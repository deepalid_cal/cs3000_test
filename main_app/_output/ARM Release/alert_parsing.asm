	.file	"alert_parsing.c"
	.text
.Ltext0:
	.section	.text.ALERTS_get_station_number_with_or_without_two_wire_indicator,"ax",%progbits
	.align	2
	.global	ALERTS_get_station_number_with_or_without_two_wire_indicator
	.type	ALERTS_get_station_number_with_or_without_two_wire_indicator, %function
ALERTS_get_station_number_with_or_without_two_wire_indicator:
.LFB0:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	sub	sp, sp, #36
.LCFI1:
	mov	r5, r0
	mov	r7, r1
	mov	r4, r2
	mov	r6, r3
	bl	FLOWSENSE_we_are_poafs
.LBB180:
	mov	r1, r7
.LBE180:
	cmp	r0, #0
.LBB181:
	mov	r0, r5
.LBE181:
	beq	.L2
.LBB182:
	add	r2, sp, #20
	mov	r3, #16
	bl	STATION_get_station_number_string
	mov	r1, r6
	ldr	r2, .L4
	add	r3, r5, #65
	str	r0, [sp, #0]
	mov	r0, r4
	bl	snprintf
	b	.L3
.L2:
.LBE182:
	add	r2, sp, #4
	mov	r3, #16
	bl	STATION_get_station_number_string
	mov	r1, r6
	ldr	r2, .L4+4
	mov	r3, r0
	mov	r0, r4
	bl	snprintf
.L3:
	mov	r0, r4
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L5:
	.align	2
.L4:
	.word	.LC7
	.word	.LC8
.LFE0:
	.size	ALERTS_get_station_number_with_or_without_two_wire_indicator, .-ALERTS_get_station_number_with_or_without_two_wire_indicator
	.section	.text.ALERTS_alert_is_visible_to_the_user,"ax",%progbits
	.align	2
	.global	ALERTS_alert_is_visible_to_the_user
	.type	ALERTS_alert_is_visible_to_the_user, %function
ALERTS_alert_is_visible_to_the_user:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #1
	mov	r3, r0
	bne	.L27
	cmp	r0, #432
	bcs	.L9
	ldr	r1, .L54
	cmp	r0, r1
	bcs	.L8
	cmp	r0, #212
	bhi	.L10
	cmp	r0, #210
	bcs	.L8
	cmp	r0, #9
	bhi	.L11
	cmp	r0, #7
	bcs	.L8
	cmp	r0, #1
	bls	.L8
	sub	r1, r0, #3
	cmp	r1, #2
	b	.L31
.L11:
	cmp	r0, #116
	bhi	.L12
	cmp	r0, #115
	bcs	.L8
	cmp	r0, #108
	beq	.L8
	cmp	r0, #113
	b	.L46
.L12:
	cmp	r0, #201
	beq	.L8
	cmp	r0, #204
	b	.L46
.L10:
	cmp	r0, #255
	bhi	.L13
	cmp	r0, #245
	bcs	.L8
	cmp	r0, #232
	beq	.L8
	subls	r1, r0, #214
	bls	.L50
	cmp	r0, #242
	b	.L46
.L13:
	ldr	r1, .L54+4
	cmp	r0, r1
	subhi	r1, r0, #420
	bhi	.L51
	sub	r1, r1, #8
	cmp	r0, r1
	bcs	.L8
	sub	r1, r0, #348
.L52:
	sub	r1, r1, #1
.L50:
	cmp	r1, #3
	b	.L31
.L9:
	ldr	r1, .L54+8
	cmp	r0, r1
	beq	.L8
	bhi	.L16
	cmp	r0, #472
	bcs	.L17
	sub	r1, r1, #12
	cmp	r0, r1
	bcs	.L8
	sub	r1, r1, #8
	cmp	r0, r1
	subhi	r1, r0, #464
	bhi	.L52
	cmp	r0, #460
	bhi	.L8
	sub	r1, r1, #29
	cmp	r0, r1
	beq	.L8
	cmp	r0, #436
	b	.L46
.L17:
	ldr	r1, .L54+12
	cmp	r0, r1
	bhi	.L19
	cmp	r0, #476
	subcc	r1, r0, #472
	bcc	.L53
	b	.L8
.L19:
	cmp	r0, #480
	b	.L46
.L16:
	cmp	r0, #528
	bcs	.L20
	cmp	r0, #524
	bhi	.L8
	cmp	r0, #516
	bcs	.L21
	ldr	r1, .L54+16
	cmp	r0, r1
	bcs	.L8
	sub	r1, r0, #484
.L53:
	sub	r1, r1, #1
.L51:
	cmp	r1, #1
	b	.L31
.L21:
	cmp	r0, #520
.L46:
	bne	.L27
	b	.L8
.L20:
	cmp	r0, #644
	bhi	.L22
	ldr	r1, .L54+20
	cmp	r0, r1
	bcs	.L8
	cmp	r0, #560
	beq	.L8
	subcs	r1, r0, #636
	bcs	.L50
	b	.L27
.L22:
	sub	r1, r0, #704
	sub	r1, r1, #1
	cmp	r1, #10
.L31:
	bhi	.L27
.L8:
	mov	r0, #1
	b	.L7
.L27:
	mov	r0, #0
.L7:
	cmp	r2, #1
	bxne	lr
	cmp	r3, #704
	bcs	.L25
	cmp	r3, #700
	bcs	.L28
	ldr	r2, .L54+24
	cmp	r3, r2
	b	.L49
.L25:
	cmp	r3, #716
	bcc	.L24
	ldr	r2, .L54+28
	cmp	r3, r2
	bls	.L28
	cmp	r3, #720
.L49:
	beq	.L28
.L24:
	cmp	r3, #49152
	movcs	r0, #1
	bx	lr
.L28:
	mov	r0, #1
	bx	lr
.L55:
	.align	2
.L54:
	.word	430
	.word	362
	.word	482
	.word	478
	.word	514
	.word	642
	.word	463
	.word	718
.LFE1:
	.size	ALERTS_alert_is_visible_to_the_user, .-ALERTS_alert_is_visible_to_the_user
	.section	.text.ALERTS_pull_object_off_pile,"ax",%progbits
	.align	2
	.global	ALERTS_pull_object_off_pile
	.type	ALERTS_pull_object_off_pile, %function
ALERTS_pull_object_off_pile:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	ldr	r8, .L59
	mov	r5, r0
	mov	r6, r1
	mov	r4, r2
	mov	sl, r3
	mov	r7, #0
	b	.L57
.L58:
	ldr	r3, [r5, #16]
	mov	r0, r5
	ldr	r2, [r8, r3, asl #3]
	ldr	r3, [r4, #0]
	mov	r1, r4
	ldrb	r3, [r2, r3]	@ zero_extendqisi2
	mov	r2, #1
	strb	r3, [r6, r7]
	bl	ALERTS_inc_index
	add	r7, r7, #1
.L57:
	cmp	r7, sl
	bne	.L58
	mov	r0, r7
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L60:
	.align	2
.L59:
	.word	alert_piles
.LFE2:
	.size	ALERTS_pull_object_off_pile, .-ALERTS_pull_object_off_pile
	.section	.text.ALERTS_comm_mngr_blocked_msg_during_idle_off_pile.constprop.9,"ax",%progbits
	.align	2
	.type	ALERTS_comm_mngr_blocked_msg_during_idle_off_pile.constprop.9, %function
ALERTS_comm_mngr_blocked_msg_during_idle_off_pile.constprop.9:
.LFB217:
	@ args = 0, pretend = 0, frame = 132
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI3:
	sub	sp, sp, #132
.LCFI4:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #128
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #129
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #130
	mov	r2, r4
	mov	r3, #1
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #131
	mov	r2, r4
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L62
	mov	r0, sp
	mov	r1, #0
	mov	r2, #128
	bl	memset
	ldrb	r3, [sp, #128]	@ zero_extendqisi2
	mov	r0, sp
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L63
.L68:
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L63
	.word	.L63
	.word	.L67
.L64:
	ldr	r1, .L74
	b	.L73
.L65:
	ldr	r1, .L74+4
	b	.L73
.L66:
	ldr	r1, .L74+8
	b	.L73
.L67:
	ldr	r1, .L74+12
	b	.L73
.L63:
	ldr	r1, .L74+16
.L73:
	mov	r2, #128
	bl	strlcpy
	ldrb	r3, [sp, #129]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L70
	mov	r0, sp
	ldr	r1, .L74+20
	mov	r2, #128
	bl	strlcat
.L70:
	ldrb	r3, [sp, #130]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L71
	mov	r0, sp
	ldr	r1, .L74+24
	mov	r2, #128
	bl	strlcat
.L71:
	ldrb	r3, [sp, #131]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L72
	mov	r0, sp
	ldr	r1, .L74+28
	mov	r2, #128
	bl	strlcat
.L72:
	mov	r0, r8
	mov	r1, #256
	ldr	r2, .L74+32
	mov	r3, sp
	bl	snprintf
.L62:
	mov	r0, r7
	add	sp, sp, #132
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L75:
	.align	2
.L74:
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
.LFE217:
	.size	ALERTS_comm_mngr_blocked_msg_during_idle_off_pile.constprop.9, .-ALERTS_comm_mngr_blocked_msg_during_idle_off_pile.constprop.9
	.section	.text.ALERTS_derate_table_update_station_count_off_pile.constprop.19,"ax",%progbits
	.align	2
	.type	ALERTS_derate_table_update_station_count_off_pile.constprop.19, %function
ALERTS_derate_table_update_station_count_off_pile.constprop.19:
.LFB207:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI5:
	sub	sp, sp, #20
.LCFI6:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #19
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #16
	mov	r2, r4
	mov	r3, #2
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #12
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L77
	ldrh	r1, [sp, #16]
	add	r2, sp, #4
	mov	r3, #8
	ldrb	r0, [sp, #19]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	ldr	r2, [sp, #12]
	mov	r1, #256
	str	r2, [sp, #0]
	ldr	r2, .L78
	mov	r3, r0
	mov	r0, r8
	bl	snprintf
.L77:
	mov	r0, r7
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L79:
	.align	2
.L78:
	.word	.LC18
.LFE207:
	.size	ALERTS_derate_table_update_station_count_off_pile.constprop.19, .-ALERTS_derate_table_update_station_count_off_pile.constprop.19
	.section	.text.ALERTS_pull_flow_error_off_pile.constprop.21,"ax",%progbits
	.align	2
	.type	ALERTS_pull_flow_error_off_pile.constprop.21, %function
ALERTS_pull_flow_error_off_pile.constprop.21:
.LFB205:
	@ args = 4, pretend = 0, frame = 68
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI7:
	sub	sp, sp, #80
.LCFI8:
	ldr	r4, [sp, #108]
	mov	sl, r1
	mov	r6, r2
	add	r1, sp, #78
	mov	r2, r4
	mov	r8, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #79
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #62
	mov	r2, r4
	mov	r3, #2
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #64
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #66
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #68
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #70
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #72
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #74
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #76
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	sl, #200
	add	r7, r7, r0
	bne	.L81
	cmp	r6, #243
	bne	.L82
	ldrh	r3, [sp, #70]
	add	r0, sp, #12
	cmp	r3, #0
	ldreq	r1, .L95
	ldrne	r1, .L95+4
	b	.L92
.L82:
	cmp	r6, #242
	bne	.L85
	ldrh	r3, [sp, #70]
	add	r0, sp, #12
	cmp	r3, #0
	ldreq	r1, .L95+8
	ldrne	r1, .L95+12
	mov	r2, #32
	bl	strlcpy
	ldrh	r3, [sp, #70]
	cmp	r3, #0
	bne	.L90
	b	.L94
.L85:
	cmp	r6, #233
	addeq	r0, sp, #12
	ldreq	r1, .L95+16
	beq	.L92
	cmp	r6, #232
	bne	.L90
	ldr	r1, .L95+20
	add	r0, sp, #12
.L92:
	mov	r2, #32
	bl	strlcpy
	b	.L90
.L94:
	ldrh	r1, [sp, #62]
	add	r2, sp, #44
	ldrb	r0, [sp, #79]	@ zero_extendqisi2
	mov	r3, #16
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, #256
	ldr	r2, .L95+24
	mov	r3, r0
	mov	r0, r8
	bl	snprintf
	b	.L81
.L90:
	ldrh	r1, [sp, #62]
	add	r2, sp, #44
	mov	r3, #16
	ldrb	r0, [sp, #79]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	ldrh	r3, [sp, #74]
	mov	r1, #256
	str	r3, [sp, #4]
	ldrh	r3, [sp, #72]
	ldr	r2, .L95+28
	str	r3, [sp, #8]
	add	r3, sp, #12
	str	r0, [sp, #0]
	mov	r0, r8
	bl	snprintf
.L81:
	mov	r0, r7
	add	sp, sp, #80
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L96:
	.align	2
.L95:
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.word	.LC24
	.word	.LC25
	.word	.LC26
.LFE205:
	.size	ALERTS_pull_flow_error_off_pile.constprop.21, .-ALERTS_pull_flow_error_off_pile.constprop.21
	.section	.text.ALERTS_pull_two_wire_station_decoder_fault_off_pile.constprop.22,"ax",%progbits
	.align	2
	.type	ALERTS_pull_two_wire_station_decoder_fault_off_pile.constprop.22, %function
ALERTS_pull_two_wire_station_decoder_fault_off_pile.constprop.22:
.LFB204:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI9:
	sub	sp, sp, #28
.LCFI10:
	mov	r5, r3
	mov	r8, r1
	mov	r4, r2
	add	r1, sp, #25
	mov	r2, r3
	mov	r3, #1
	mov	r6, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #26
	mov	r2, r5
	mov	r3, #1
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #20
	mov	r2, r5
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #27
	mov	r2, r5
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L98
	ldrb	r3, [sp, #25]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L99
	ldrb	r1, [sp, #27]	@ zero_extendqisi2
	add	r2, sp, #4
	ldrb	r0, [sp, #26]	@ zero_extendqisi2
	mov	r3, #16
	ldr	r5, [sp, #20]
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, #256
	ldr	r2, .L104
	str	r0, [sp, #0]
	mov	r0, r4
	b	.L103
.L99:
	cmp	r3, #2
	bne	.L100
	ldrb	r1, [sp, #27]	@ zero_extendqisi2
	add	r2, sp, #4
	mov	r3, #16
	ldrb	r0, [sp, #26]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	ldr	r2, [sp, #20]
	mov	r1, #256
	str	r2, [sp, #0]
	ldr	r2, .L104+4
	mov	r3, r0
	mov	r0, r4
.L102:
	bl	snprintf
	b	.L98
.L100:
	cmp	r3, #3
	bne	.L98
	ldrb	r0, [sp, #26]	@ zero_extendqisi2
	cmp	r0, #255
	beq	.L101
	ldrb	r1, [sp, #27]	@ zero_extendqisi2
	add	r2, sp, #4
	mov	r3, #16
	ldr	r5, [sp, #20]
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	ldr	r2, .L104+8
	mov	r1, #256
	str	r0, [sp, #0]
	mov	r0, r4
.L103:
	mov	r3, r5
	b	.L102
.L101:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L104+12
	ldr	r3, [sp, #20]
	bl	snprintf
.L98:
	mov	r0, r7
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L105:
	.align	2
.L104:
	.word	.LC27
	.word	.LC28
	.word	.LC29
	.word	.LC30
.LFE204:
	.size	ALERTS_pull_two_wire_station_decoder_fault_off_pile.constprop.22, .-ALERTS_pull_two_wire_station_decoder_fault_off_pile.constprop.22
	.section	.text.ALERTS_pull_hub_rcvd_data_off_pile.constprop.24,"ax",%progbits
	.align	2
	.type	ALERTS_pull_hub_rcvd_data_off_pile.constprop.24, %function
ALERTS_pull_hub_rcvd_data_off_pile.constprop.24:
.LFB202:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI11:
	sub	sp, sp, #24
.LCFI12:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #22
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #23
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #12
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #16
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L107
	ldrb	r2, [sp, #22]	@ zero_extendqisi2
	ldr	r3, .L110
	ldr	r0, [sp, #12]
	ldr	r1, .L110+4
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, .L110+8
	cmp	r0, #2000
	moveq	r2, r1
	str	r2, [sp, #0]
	ldrb	r1, [sp, #23]	@ zero_extendqisi2
	ldr	r2, .L110+12
	mov	r0, r8
	ldr	r2, [r2, r1, asl #2]
	mov	r1, #256
	str	r2, [sp, #4]
	ldr	r2, [sp, #16]
	str	r2, [sp, #8]
	ldr	r2, .L110+16
	bl	snprintf
.L107:
	mov	r0, r7
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L111:
	.align	2
.L110:
	.word	.LANCHOR0
	.word	.LC31
	.word	.LC32
	.word	port_names
	.word	.LC33
.LFE202:
	.size	ALERTS_pull_hub_rcvd_data_off_pile.constprop.24, .-ALERTS_pull_hub_rcvd_data_off_pile.constprop.24
	.section	.text.ALERTS_pull_hub_forwarding_data_off_pile.constprop.25,"ax",%progbits
	.align	2
	.type	ALERTS_pull_hub_forwarding_data_off_pile.constprop.25, %function
ALERTS_pull_hub_forwarding_data_off_pile.constprop.25:
.LFB201:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI13:
	sub	sp, sp, #24
.LCFI14:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #22
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #23
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #12
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #16
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L113
	ldrb	r2, [sp, #22]	@ zero_extendqisi2
	ldr	r3, .L116
	ldrb	r1, [sp, #23]	@ zero_extendqisi2
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, .L116+4
	ldr	ip, [sp, #12]
	ldr	r0, .L116+8
	ldr	r1, [r2, r1, asl #2]
	ldr	r2, .L116+12
	cmp	ip, #2000
	moveq	r2, r0
	stmia	sp, {r1, r2}
	ldr	r2, [sp, #16]
	mov	r0, r8
	str	r2, [sp, #8]
	mov	r1, #256
	ldr	r2, .L116+16
	bl	snprintf
.L113:
	mov	r0, r7
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L117:
	.align	2
.L116:
	.word	.LANCHOR0
	.word	port_names
	.word	.LC31
	.word	.LC32
	.word	.LC34
.LFE201:
	.size	ALERTS_pull_hub_forwarding_data_off_pile.constprop.25, .-ALERTS_pull_hub_forwarding_data_off_pile.constprop.25
	.section	.text.ALERTS_pull_router_rcvd_unexp_class_off_pile.constprop.26,"ax",%progbits
	.align	2
	.type	ALERTS_pull_router_rcvd_unexp_class_off_pile.constprop.26, %function
ALERTS_pull_router_rcvd_unexp_class_off_pile.constprop.26:
.LFB200:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI15:
	sub	sp, sp, #24
.LCFI16:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #22
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #23
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #12
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #16
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L119
	ldrb	r2, [sp, #22]	@ zero_extendqisi2
	ldr	r3, .L122
	ldrb	r1, [sp, #23]	@ zero_extendqisi2
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, .L122+4
	ldr	ip, [sp, #12]
	ldr	r0, .L122+8
	ldr	r1, [r2, r1, asl #2]
	ldr	r2, .L122+12
	cmp	ip, #2000
	moveq	r2, r0
	stmia	sp, {r1, r2}
	ldr	r2, [sp, #16]
	mov	r0, r8
	str	r2, [sp, #8]
	mov	r1, #256
	ldr	r2, .L122+16
	bl	snprintf
.L119:
	mov	r0, r7
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L123:
	.align	2
.L122:
	.word	.LANCHOR0
	.word	port_names
	.word	.LC31
	.word	.LC32
	.word	.LC35
.LFE200:
	.size	ALERTS_pull_router_rcvd_unexp_class_off_pile.constprop.26, .-ALERTS_pull_router_rcvd_unexp_class_off_pile.constprop.26
	.section	.text.ALERTS_pull_router_rcvd_packet_not_on_hub_list_with_sn_off_pile.constprop.27,"ax",%progbits
	.align	2
	.type	ALERTS_pull_router_rcvd_packet_not_on_hub_list_with_sn_off_pile.constprop.27, %function
ALERTS_pull_router_rcvd_packet_not_on_hub_list_with_sn_off_pile.constprop.27:
.LFB199:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI17:
	sub	sp, sp, #24
.LCFI18:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #22
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #23
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #12
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #16
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L125
	ldrb	r2, [sp, #22]	@ zero_extendqisi2
	ldr	r3, .L128
	ldrb	r1, [sp, #23]	@ zero_extendqisi2
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, .L128+4
	ldr	ip, [sp, #12]
	ldr	r1, [r2, r1, asl #2]
	ldr	r0, .L128+8
	ldr	r2, .L128+12
	str	r1, [sp, #0]
	ldr	r1, [sp, #16]
	cmp	ip, #2000
	moveq	r2, r0
	stmib	sp, {r1, r2}
	ldr	r2, .L128+16
	mov	r0, r8
	mov	r1, #256
	bl	snprintf
.L125:
	mov	r0, r7
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L129:
	.align	2
.L128:
	.word	.LANCHOR0
	.word	port_names
	.word	.LC31
	.word	.LC32
	.word	.LC36
.LFE199:
	.size	ALERTS_pull_router_rcvd_packet_not_on_hub_list_with_sn_off_pile.constprop.27, .-ALERTS_pull_router_rcvd_packet_not_on_hub_list_with_sn_off_pile.constprop.27
	.section	.text.ALERTS_pull_router_rcvd_packet_not_for_us_off_pile.constprop.28,"ax",%progbits
	.align	2
	.type	ALERTS_pull_router_rcvd_packet_not_for_us_off_pile.constprop.28, %function
ALERTS_pull_router_rcvd_packet_not_for_us_off_pile.constprop.28:
.LFB198:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI19:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #14
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #15
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #8
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L131
	ldrb	r2, [sp, #14]	@ zero_extendqisi2
	ldr	r3, .L134
	ldrb	r1, [sp, #15]	@ zero_extendqisi2
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, .L134+4
	ldr	ip, [sp, #8]
	ldr	r0, .L134+8
	ldr	r1, [r2, r1, asl #2]
	ldr	r2, .L134+12
	cmp	ip, #2000
	moveq	r2, r0
	stmia	sp, {r1, r2}
	ldr	r2, .L134+16
	mov	r0, r8
	mov	r1, #256
	bl	snprintf
.L131:
	mov	r0, r7
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L135:
	.align	2
.L134:
	.word	.LANCHOR0
	.word	port_names
	.word	.LC31
	.word	.LC32
	.word	.LC37
.LFE198:
	.size	ALERTS_pull_router_rcvd_packet_not_for_us_off_pile.constprop.28, .-ALERTS_pull_router_rcvd_packet_not_for_us_off_pile.constprop.28
	.section	.text.ALERTS_pull_set_no_water_days_by_station_off_pile.constprop.31,"ax",%progbits
	.align	2
	.type	ALERTS_pull_set_no_water_days_by_station_off_pile.constprop.31, %function
ALERTS_pull_set_no_water_days_by_station_off_pile.constprop.31:
.LFB195:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI20:
	sub	sp, sp, #28
.LCFI21:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #27
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #22
	mov	r2, r4
	mov	r3, #2
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #24
	mov	r2, r4
	mov	r3, #2
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L137
	ldrh	r1, [sp, #22]
	add	r2, sp, #4
	mov	r3, #16
	ldrb	r0, [sp, #27]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	ldrh	r2, [sp, #24]
	mov	r1, #256
	str	r2, [sp, #0]
	ldr	r2, .L138
	mov	r3, r0
	mov	r0, r8
	bl	snprintf
.L137:
	mov	r0, r7
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L139:
	.align	2
.L138:
	.word	.LC38
.LFE195:
	.size	ALERTS_pull_set_no_water_days_by_station_off_pile.constprop.31, .-ALERTS_pull_set_no_water_days_by_station_off_pile.constprop.31
	.section	.text.ALERTS_pull_reboot_request_off_pile.constprop.33,"ax",%progbits
	.align	2
	.type	ALERTS_pull_reboot_request_off_pile.constprop.33, %function
ALERTS_pull_reboot_request_off_pile.constprop.33:
.LFB193:
	@ args = 0, pretend = 0, frame = 68
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI22:
	sub	sp, sp, #68
.LCFI23:
	mov	r6, r1
	mov	r5, r2
	add	r1, sp, #64
	mov	r2, r3
	mov	r3, #4
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	mov	r4, r0
	bne	.L141
	ldr	r3, [sp, #64]
	mov	r0, sp
	sub	r3, r3, #27
	cmp	r3, #14
	ldrls	pc, [pc, r3, asl #2]
	b	.L142
.L155:
	.word	.L143
	.word	.L144
	.word	.L144
	.word	.L142
	.word	.L142
	.word	.L145
	.word	.L146
	.word	.L147
	.word	.L148
	.word	.L149
	.word	.L150
	.word	.L151
	.word	.L152
	.word	.L153
	.word	.L154
.L143:
	ldr	r1, .L158
	b	.L157
.L144:
	ldr	r1, .L158+4
	b	.L157
.L145:
	ldr	r1, .L158+8
	b	.L157
.L146:
	ldr	r1, .L158+12
	b	.L157
.L147:
	ldr	r1, .L158+16
	b	.L157
.L148:
	ldr	r1, .L158+20
	b	.L157
.L149:
	ldr	r1, .L158+24
	b	.L157
.L150:
	ldr	r1, .L158+28
	b	.L157
.L151:
	ldr	r1, .L158+32
	b	.L157
.L152:
	ldr	r1, .L158+36
	b	.L157
.L153:
	ldr	r1, .L158+40
	b	.L157
.L154:
	ldr	r1, .L158+44
	b	.L157
.L142:
	ldr	r1, .L158+48
.L157:
	mov	r2, #64
	bl	strlcpy
	mov	r0, r5
	mov	r1, #256
	ldr	r2, .L158+52
	mov	r3, sp
	bl	snprintf
.L141:
	mov	r0, r4
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, pc}
.L159:
	.align	2
.L158:
	.word	.LC39
	.word	.LC40
	.word	.LC41
	.word	.LC42
	.word	.LC43
	.word	.LC44
	.word	.LC45
	.word	.LC46
	.word	.LC47
	.word	.LC48
	.word	.LC49
	.word	.LC50
	.word	.LC51
	.word	.LC52
.LFE193:
	.size	ALERTS_pull_reboot_request_off_pile.constprop.33, .-ALERTS_pull_reboot_request_off_pile.constprop.33
	.section	.text.ALERTS_pull_ETGAGE_percent_full_edited_off_pile.constprop.34,"ax",%progbits
	.align	2
	.type	ALERTS_pull_ETGAGE_percent_full_edited_off_pile.constprop.34, %function
ALERTS_pull_ETGAGE_percent_full_edited_off_pile.constprop.34:
.LFB192:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI24:
	mov	r8, r1
	mov	r6, r3
	add	r1, sp, #4
	mov	r4, r2
	mov	r2, r3
	mov	r3, #2
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #6
	mov	r2, r6
	mov	r3, #2
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r0, r7
	bne	.L161
	ldr	r3, .L165+12
	mov	r2, #256
	cmp	r5, r3
	ldreq	r1, .L165+16
	ldrne	r1, .L165+20
	mov	r0, r4
	bl	strlcpy
	ldrh	r3, [sp, #4]
	flds	s11, .L165
	fldd	d7, .L165+4
	fmsr	s9, r3	@ int
	ldrh	r3, [sp, #6]
	mov	r0, r4
	fuitos	s12, s9
	fmsr	s9, r3	@ int
	mov	r1, #256
	ldr	r2, .L165+24
	fuitos	s10, s9
	fdivs	s12, s12, s11
	fdivs	s10, s10, s11
	fcvtds	d6, s12
	fmuld	d6, d6, d7
	fcvtds	d5, s10
	fmuld	d7, d5, d7
	ftouizd	s11, d7
	ftouizd	s15, d6
	fsts	s11, [sp, #0]	@ int
	fmrs	r3, s15	@ int
	bl	sp_strlcat
.L161:
	mov	r0, r7
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L166:
	.align	2
.L165:
	.word	1149861888
	.word	0
	.word	1079574528
	.word	alerts_struct_changes
	.word	.LANCHOR1
	.word	.LC54
	.word	.LC55
.LFE192:
	.size	ALERTS_pull_ETGAGE_percent_full_edited_off_pile.constprop.34, .-ALERTS_pull_ETGAGE_percent_full_edited_off_pile.constprop.34
	.section	.text.ALERTS_pull_stop_key_pressed_off_pile.constprop.35,"ax",%progbits
	.align	2
	.type	ALERTS_pull_stop_key_pressed_off_pile.constprop.35, %function
ALERTS_pull_stop_key_pressed_off_pile.constprop.35:
.LFB191:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI25:
	mov	r4, r3
	mov	r8, r1
	mov	r5, r2
	add	r1, sp, #4
	mov	r2, r3
	mov	r3, #4
	mov	r6, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #10
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #11
	mov	r2, r4
	mov	r3, #1
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L168
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	ldrb	r0, [sp, #10]	@ zero_extendqisi2
	beq	.L169
	bl	GetReasonOnStr
	ldrb	r2, [sp, #11]	@ zero_extendqisi2
	mov	r1, #256
	add	r2, r2, #65
	str	r2, [sp, #0]
	ldr	r2, .L170
	mov	r3, r0
	mov	r0, r5
	bl	snprintf
	b	.L168
.L169:
	bl	GetReasonOnStr
	mov	r1, #256
	ldr	r2, .L170+4
	mov	r3, r0
	mov	r0, r5
	bl	snprintf
.L168:
	mov	r0, r7
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, pc}
.L171:
	.align	2
.L170:
	.word	.LC56
	.word	.LC57
.LFE191:
	.size	ALERTS_pull_stop_key_pressed_off_pile.constprop.35, .-ALERTS_pull_stop_key_pressed_off_pile.constprop.35
	.section	.text.ALERTS_pull_wind_paused_or_resumed_off_pile.constprop.36,"ax",%progbits
	.align	2
	.type	ALERTS_pull_wind_paused_or_resumed_off_pile.constprop.36, %function
ALERTS_pull_wind_paused_or_resumed_off_pile.constprop.36:
.LFB190:
	@ args = 4, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI26:
	ldr	r5, [sp, #24]
	mov	r7, r1
	mov	r4, r2
	mov	r1, sp
	mov	r2, r3
	mov	r3, #4
	bl	ALERTS_pull_object_off_pile
	cmp	r7, #200
	mov	r6, r0
	bne	.L173
	ldr	r3, .L180
	cmp	r5, r3
	beq	.L175
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L177
	b	.L179
.L175:
	mov	r0, r4
	ldr	r1, .L180+4
	mov	r2, #256
	bl	strlcpy
	ldr	r3, [sp, #0]
	cmp	r3, #0
	beq	.L173
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L180+8
	bl	sp_strlcat
	b	.L173
.L179:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L180+12
	b	.L178
.L177:
	ldr	r2, .L180+16
	mov	r0, r4
	mov	r1, #256
.L178:
	ldr	r3, [sp, #0]
	bl	snprintf
.L173:
	mov	r0, r6
	ldmfd	sp!, {r3, r4, r5, r6, r7, pc}
.L181:
	.align	2
.L180:
	.word	477
	.word	.LC58
	.word	.LC59
	.word	.LC60
	.word	.LC61
.LFE190:
	.size	ALERTS_pull_wind_paused_or_resumed_off_pile.constprop.36, .-ALERTS_pull_wind_paused_or_resumed_off_pile.constprop.36
	.section	.text.ALERTS_pull_flow_not_checked_with_reason_off_pile.constprop.37,"ax",%progbits
	.align	2
	.type	ALERTS_pull_flow_not_checked_with_reason_off_pile.constprop.37, %function
ALERTS_pull_flow_not_checked_with_reason_off_pile.constprop.37:
.LFB189:
	@ args = 4, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI27:
	ldr	r5, [sp, #40]
	mov	sl, r1
	mov	r6, r3
	add	r1, sp, #10
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r8, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #11
	mov	r2, r6
	mov	r3, #1
	mov	r7, r0
	mov	r0, r8
	bl	ALERTS_pull_object_off_pile
	cmp	sl, #200
	add	r7, r0, r7
	bne	.L183
	ldrb	r1, [sp, #11]	@ zero_extendqisi2
	mov	r2, sp
	mov	r3, #8
	ldrb	r0, [sp, #10]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, #256
	ldr	r2, .L191
	mov	r3, r0
	mov	r0, r4
	bl	snprintf
	cmp	r5, #256
	beq	.L185
	ldr	r3, .L191+4
	cmp	r5, r3
	movne	r0, r4
	ldrne	r1, .L191+8
	bne	.L189
	b	.L190
.L185:
	mov	r0, r4
	ldr	r1, .L191+12
	mov	r2, r5
	b	.L188
.L190:
	ldr	r1, .L191+16
	mov	r0, r4
.L189:
	mov	r2, #256
.L188:
	bl	strlcat
.L183:
	mov	r0, r7
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, sl, pc}
.L192:
	.align	2
.L191:
	.word	.LC62
	.word	257
	.word	.LC65
	.word	.LC63
	.word	.LC64
.LFE189:
	.size	ALERTS_pull_flow_not_checked_with_reason_off_pile.constprop.37, .-ALERTS_pull_flow_not_checked_with_reason_off_pile.constprop.37
	.section	.text.ALERTS_pull_accum_rain_set_by_station_cleared_off_pile.constprop.38,"ax",%progbits
	.align	2
	.type	ALERTS_pull_accum_rain_set_by_station_cleared_off_pile.constprop.38, %function
ALERTS_pull_accum_rain_set_by_station_cleared_off_pile.constprop.38:
.LFB188:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI28:
	sub	sp, sp, #36
.LCFI29:
	mov	r4, r3
	mov	r8, r1
	mov	r6, r2
	add	r1, sp, #34
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #32
	mov	r2, r4
	mov	r3, #2
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #28
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #35
	mov	r2, r4
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L194
	ldr	r3, [sp, #28]
	add	r2, sp, #12
	cmp	r3, #0
	ldrb	r0, [sp, #34]	@ zero_extendqisi2
	ldrh	r1, [sp, #32]
	mov	r3, #16
	bne	.L195
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	ldrb	r1, [sp, #35]	@ zero_extendqisi2
	ldr	r2, .L196
	ldr	r2, [r2, r1, asl #2]
	mov	r1, #256
	str	r2, [sp, #0]
	ldr	r2, .L196+4
	mov	r3, r0
	mov	r0, r6
	bl	snprintf
	b	.L194
.L195:
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	flds	s13, [sp, #28]	@ int
	ldr	r2, .L196+8
	ldrb	r1, [sp, #35]	@ zero_extendqisi2
	fuitos	s14, s13
	flds	s15, [r2, #0]
	ldr	r2, .L196
	ldr	r2, [r2, r1, asl #2]
	mov	r1, #256
	fdivs	s14, s14, s15
	str	r2, [sp, #8]
	ldr	r2, .L196+12
	fcvtds	d7, s14
	mov	r3, r0
	mov	r0, r6
	fstd	d7, [sp, #0]
	bl	snprintf
.L194:
	mov	r0, r7
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L197:
	.align	2
.L196:
	.word	.LANCHOR2
	.word	.LC66
	.word	.LANCHOR3
	.word	.LC67
.LFE188:
	.size	ALERTS_pull_accum_rain_set_by_station_cleared_off_pile.constprop.38, .-ALERTS_pull_accum_rain_set_by_station_cleared_off_pile.constprop.38
	.section	.text.ALERTS_pull_test_station_started_off_pile.constprop.39,"ax",%progbits
	.align	2
	.type	ALERTS_pull_test_station_started_off_pile.constprop.39, %function
ALERTS_pull_test_station_started_off_pile.constprop.39:
.LFB187:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI30:
	sub	sp, sp, #24
.LCFI31:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #22
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #20
	mov	r2, r4
	mov	r3, #2
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #23
	mov	r2, r4
	mov	r3, #1
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L199
	ldrh	r1, [sp, #20]
	add	r2, sp, #4
	mov	r3, #16
	ldrb	r0, [sp, #22]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	ldrb	r1, [sp, #23]	@ zero_extendqisi2
	ldr	r2, .L200
	ldr	r2, [r2, r1, asl #2]
	mov	r1, #256
	str	r2, [sp, #0]
	ldr	r2, .L200+4
	mov	r3, r0
	mov	r0, r8
	bl	snprintf
.L199:
	mov	r0, r7
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L201:
	.align	2
.L200:
	.word	.LANCHOR2
	.word	.LC68
.LFE187:
	.size	ALERTS_pull_test_station_started_off_pile.constprop.39, .-ALERTS_pull_test_station_started_off_pile.constprop.39
	.section	.text.ALERTS_pull_manual_water_station_started_off_pile.constprop.40,"ax",%progbits
	.align	2
	.type	ALERTS_pull_manual_water_station_started_off_pile.constprop.40, %function
ALERTS_pull_manual_water_station_started_off_pile.constprop.40:
.LFB186:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI32:
	sub	sp, sp, #24
.LCFI33:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #22
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #20
	mov	r2, r4
	mov	r3, #2
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #23
	mov	r2, r4
	mov	r3, #1
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L203
	ldrh	r1, [sp, #20]
	add	r2, sp, #4
	mov	r3, #16
	ldrb	r0, [sp, #22]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	ldrb	r1, [sp, #23]	@ zero_extendqisi2
	ldr	r2, .L204
	ldr	r2, [r2, r1, asl #2]
	mov	r1, #256
	str	r2, [sp, #0]
	ldr	r2, .L204+4
	mov	r3, r0
	mov	r0, r8
	bl	snprintf
.L203:
	mov	r0, r7
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L205:
	.align	2
.L204:
	.word	.LANCHOR2
	.word	.LC69
.LFE186:
	.size	ALERTS_pull_manual_water_station_started_off_pile.constprop.40, .-ALERTS_pull_manual_water_station_started_off_pile.constprop.40
	.section	.text.ALERTS_pull_light_ID_with_text_from_pile.constprop.43,"ax",%progbits
	.align	2
	.type	ALERTS_pull_light_ID_with_text_from_pile.constprop.43, %function
ALERTS_pull_light_ID_with_text_from_pile.constprop.43:
.LFB183:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI34:
	mov	r5, r3
	mov	r8, r1
	mov	r4, r2
	add	r1, sp, #9
	mov	r2, r3
	mov	r3, #1
	mov	r6, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #10
	mov	r2, r5
	mov	r3, #1
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #11
	mov	r2, r5
	mov	r3, #1
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L207
	ldrb	r3, [sp, #11]	@ zero_extendqisi2
	cmp	r3, #5
	bhi	.L208
	bl	FLOWSENSE_we_are_poafs
	ldrb	ip, [sp, #11]	@ zero_extendqisi2
	ldr	r3, .L221
	cmp	r0, #0
	beq	.L209
	ldrb	r2, [sp, #9]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	ldrb	r2, [sp, #10]	@ zero_extendqisi2
	mov	r1, #256
	add	r2, r2, #1
	str	r2, [sp, #4]
	ldr	r3, [r3, ip, asl #2]
	ldr	r2, .L221+4
	b	.L216
.L209:
	ldrb	r2, [sp, #10]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #1
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L221+8
	ldr	r3, [r3, ip, asl #2]
	b	.L217
.L208:
	sub	r2, r3, #6
	cmp	r2, #1
	bhi	.L210
	bl	FLOWSENSE_we_are_poafs
	ldr	r2, .L221
	ldrb	r3, [sp, #10]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L211
	add	r3, r3, #1
	str	r3, [sp, #0]
	ldrb	r3, [sp, #11]	@ zero_extendqisi2
	ldrb	ip, [sp, #9]	@ zero_extendqisi2
	ldr	r3, [r2, r3, asl #2]
	ldr	r2, .L221+12
	str	r3, [sp, #4]
	mov	r0, r4
	mov	r1, #256
	add	r3, ip, #65
.L216:
	bl	snprintf
	b	.L207
.L211:
	ldrb	r1, [sp, #11]	@ zero_extendqisi2
	mov	r0, r4
	ldr	r2, [r2, r1, asl #2]
	mov	r1, #256
	str	r2, [sp, #0]
	ldr	r2, .L221+16
	b	.L220
.L210:
	cmp	r3, #16
	bne	.L212
	bl	FLOWSENSE_we_are_poafs
	ldrb	r3, [sp, #10]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L213
	add	r3, r3, #1
	ldrb	ip, [sp, #9]	@ zero_extendqisi2
	str	r3, [sp, #0]
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L221+20
	b	.L219
.L213:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L221+24
	b	.L218
.L212:
	cmp	r3, #17
	bne	.L214
	bl	FLOWSENSE_we_are_poafs
	ldrb	r3, [sp, #10]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L215
	ldrb	ip, [sp, #9]	@ zero_extendqisi2
	ldr	r2, .L221+28
	add	r3, r3, #1
	mov	r0, r4
	mov	r1, #256
	str	r3, [sp, #0]
.L219:
	add	r3, ip, #65
	b	.L217
.L215:
	ldr	r2, .L221+32
	mov	r0, r4
	mov	r1, #256
.L218:
	add	r3, r3, #1
	bl	snprintf
	b	.L207
.L214:
	ldrb	r2, [sp, #9]	@ zero_extendqisi2
	ldrb	r3, [sp, #10]	@ zero_extendqisi2
	add	r2, r2, #65
	str	r2, [sp, #0]
	ldr	r2, .L221+36
	mov	r0, r4
	mov	r1, #256
.L220:
	add	r3, r3, #1
.L217:
	bl	snprintf
.L207:
	mov	r0, r7
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, pc}
.L222:
	.align	2
.L221:
	.word	.LANCHOR4
	.word	.LC70
	.word	.LC71
	.word	.LC72
	.word	.LC73
	.word	.LC74
	.word	.LC75
	.word	.LC76
	.word	.LC77
	.word	.LC78
.LFE183:
	.size	ALERTS_pull_light_ID_with_text_from_pile.constprop.43, .-ALERTS_pull_light_ID_with_text_from_pile.constprop.43
	.section	.text.ALERTS_pull_moisture_reading_out_of_range_off_pile.constprop.44,"ax",%progbits
	.align	2
	.type	ALERTS_pull_moisture_reading_out_of_range_off_pile.constprop.44, %function
ALERTS_pull_moisture_reading_out_of_range_off_pile.constprop.44:
.LFB182:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI35:
	sub	sp, sp, #28
.LCFI36:
	mov	r4, r3
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #27
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #12
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #16
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #20
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L224
	ldr	r3, [sp, #16]
	mov	r0, r8
	str	r3, [sp, #0]
	ldr	r3, [sp, #20]
	mov	r1, #256
	str	r3, [sp, #4]
	ldrb	r3, [sp, #27]	@ zero_extendqisi2
	ldr	r2, .L225
	str	r3, [sp, #8]
	ldr	r3, [sp, #12]
	bl	snprintf
.L224:
	mov	r0, r7
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L226:
	.align	2
.L225:
	.word	.LC79
.LFE182:
	.size	ALERTS_pull_moisture_reading_out_of_range_off_pile.constprop.44, .-ALERTS_pull_moisture_reading_out_of_range_off_pile.constprop.44
	.section	.text.ALERTS_pull_soil_moisture_crossed_threshold_off_pile.constprop.45,"ax",%progbits
	.align	2
	.type	ALERTS_pull_soil_moisture_crossed_threshold_off_pile.constprop.45, %function
ALERTS_pull_soil_moisture_crossed_threshold_off_pile.constprop.45:
.LFB181:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI37:
	sub	sp, sp, #36
.LCFI38:
	mov	r4, r3
	mov	r8, r1
	mov	r6, r2
	add	r1, sp, #16
	mov	r2, r3
	mov	r3, #4
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #20
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #24
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #28
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #35
	mov	r2, r4
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L228
	flds	s14, [sp, #20]
	ldrb	r3, [sp, #35]	@ zero_extendqisi2
	fcvtds	d7, s14
	cmp	r3, #0
	fstd	d7, [sp, #0]
	bne	.L229
	flds	s14, [sp, #28]
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L232
	fcvtds	d7, s14
	fstd	d7, [sp, #8]
	b	.L231
.L229:
	cmp	r3, #1
	bne	.L230
	flds	s14, [sp, #24]
	ldr	r2, .L232+4
	mov	r0, r6
	fcvtds	d7, s14
	mov	r1, #256
	fstd	d7, [sp, #8]
.L231:
	ldr	r3, [sp, #16]
	bl	snprintf
	b	.L228
.L230:
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L232+8
	ldr	r3, [sp, #16]
	bl	snprintf
.L228:
	mov	r0, r7
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L233:
	.align	2
.L232:
	.word	.LC80
	.word	.LC81
	.word	.LC82
.LFE181:
	.size	ALERTS_pull_soil_moisture_crossed_threshold_off_pile.constprop.45, .-ALERTS_pull_soil_moisture_crossed_threshold_off_pile.constprop.45
	.section	.text.ALERTS_pull_soil_temperature_crossed_threshold_off_pile.constprop.46,"ax",%progbits
	.align	2
	.type	ALERTS_pull_soil_temperature_crossed_threshold_off_pile.constprop.46, %function
ALERTS_pull_soil_temperature_crossed_threshold_off_pile.constprop.46:
.LFB180:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI39:
	sub	sp, sp, #28
.LCFI40:
	mov	r4, r3
	mov	r8, r1
	mov	r6, r2
	add	r1, sp, #8
	mov	r2, r3
	mov	r3, #4
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #12
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #16
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #20
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #27
	mov	r2, r4
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L235
	ldrb	r2, [sp, #27]	@ zero_extendqisi2
	ldr	r3, [sp, #12]
	cmp	r2, #0
	str	r3, [sp, #0]
	bne	.L236
	ldr	r3, [sp, #20]
	mov	r0, r6
	str	r3, [sp, #4]
	mov	r1, #256
	ldr	r2, .L239
	b	.L238
.L236:
	cmp	r2, #1
	bne	.L237
	ldr	r3, [sp, #16]
	ldr	r2, .L239+4
	mov	r0, r6
	mov	r1, #256
	str	r3, [sp, #4]
.L238:
	ldr	r3, [sp, #8]
	bl	snprintf
	b	.L235
.L237:
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L239+8
	ldr	r3, [sp, #8]
	bl	snprintf
.L235:
	mov	r0, r7
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L240:
	.align	2
.L239:
	.word	.LC83
	.word	.LC84
	.word	.LC85
.LFE180:
	.size	ALERTS_pull_soil_temperature_crossed_threshold_off_pile.constprop.46, .-ALERTS_pull_soil_temperature_crossed_threshold_off_pile.constprop.46
	.section	.text.ALERTS_pull_soil_conductivity_crossed_threshold_off_pile.constprop.47,"ax",%progbits
	.align	2
	.type	ALERTS_pull_soil_conductivity_crossed_threshold_off_pile.constprop.47, %function
ALERTS_pull_soil_conductivity_crossed_threshold_off_pile.constprop.47:
.LFB179:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI41:
	sub	sp, sp, #36
.LCFI42:
	mov	r4, r3
	mov	r8, r1
	mov	r6, r2
	add	r1, sp, #16
	mov	r2, r3
	mov	r3, #4
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #20
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #24
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #28
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #35
	mov	r2, r4
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L242
	flds	s14, [sp, #20]
	ldrb	r3, [sp, #35]	@ zero_extendqisi2
	fcvtds	d7, s14
	cmp	r3, #0
	fstd	d7, [sp, #0]
	bne	.L243
	flds	s14, [sp, #28]
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L246
	fcvtds	d7, s14
	fstd	d7, [sp, #8]
	b	.L245
.L243:
	cmp	r3, #1
	bne	.L244
	flds	s14, [sp, #24]
	ldr	r2, .L246+4
	mov	r0, r6
	fcvtds	d7, s14
	mov	r1, #256
	fstd	d7, [sp, #8]
.L245:
	ldr	r3, [sp, #16]
	bl	snprintf
	b	.L242
.L244:
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L246+8
	ldr	r3, [sp, #16]
	bl	snprintf
.L242:
	mov	r0, r7
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L247:
	.align	2
.L246:
	.word	.LC86
	.word	.LC87
	.word	.LC88
.LFE179:
	.size	ALERTS_pull_soil_conductivity_crossed_threshold_off_pile.constprop.47, .-ALERTS_pull_soil_conductivity_crossed_threshold_off_pile.constprop.47
	.section	.text.ALERTS_pull_station_card_added_or_removed_off_pile.constprop.52,"ax",%progbits
	.align	2
	.type	ALERTS_pull_station_card_added_or_removed_off_pile.constprop.52, %function
ALERTS_pull_station_card_added_or_removed_off_pile.constprop.52:
.LFB174:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI43:
	mov	r4, r3
	mov	r8, r1
	mov	r5, r2
	add	r1, sp, #13
	mov	r2, r3
	mov	r3, #1
	mov	r6, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #14
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #15
	mov	r2, r4
	mov	r3, #1
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L249
	bl	FLOWSENSE_we_are_poafs
	ldr	r2, .L251
	ldrb	r1, [sp, #15]	@ zero_extendqisi2
	ldrb	r3, [sp, #14]	@ zero_extendqisi2
	ldr	r2, [r2, r1, asl #2]
	add	r3, r3, #1
	mov	r3, r3, asl #3
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	cmp	r0, #0
	beq	.L250
	ldrb	r2, [sp, #13]	@ zero_extendqisi2
	mov	r0, r5
	add	r2, r2, #65
	str	r2, [sp, #8]
	mov	r1, #256
	ldr	r2, .L251+4
	sub	r3, r3, #7
	bl	snprintf
	b	.L249
.L250:
	mov	r0, r5
	mov	r1, #256
	ldr	r2, .L251+8
	sub	r3, r3, #7
	bl	snprintf
.L249:
	mov	r0, r7
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L252:
	.align	2
.L251:
	.word	.LANCHOR5
	.word	.LC89
	.word	.LC90
.LFE174:
	.size	ALERTS_pull_station_card_added_or_removed_off_pile.constprop.52, .-ALERTS_pull_station_card_added_or_removed_off_pile.constprop.52
	.section	.text.ALERTS_pull_lights_card_added_or_removed_off_pile.constprop.53,"ax",%progbits
	.align	2
	.type	ALERTS_pull_lights_card_added_or_removed_off_pile.constprop.53, %function
ALERTS_pull_lights_card_added_or_removed_off_pile.constprop.53:
.LFB173:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI44:
	mov	r8, r1
	mov	r5, r3
	add	r1, sp, #6
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r7, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #7
	mov	r2, r5
	mov	r3, #1
	mov	r6, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r6, r0, r6
	bne	.L254
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L256
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L255
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L256+4
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L254
.L255:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L256+8
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
.L254:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L257:
	.align	2
.L256:
	.word	.LANCHOR5
	.word	.LC91
	.word	.LC92
.LFE173:
	.size	ALERTS_pull_lights_card_added_or_removed_off_pile.constprop.53, .-ALERTS_pull_lights_card_added_or_removed_off_pile.constprop.53
	.section	.text.ALERTS_pull_poc_card_added_or_removed_off_pile.constprop.54,"ax",%progbits
	.align	2
	.type	ALERTS_pull_poc_card_added_or_removed_off_pile.constprop.54, %function
ALERTS_pull_poc_card_added_or_removed_off_pile.constprop.54:
.LFB172:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI45:
	mov	r8, r1
	mov	r5, r3
	add	r1, sp, #6
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r7, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #7
	mov	r2, r5
	mov	r3, #1
	mov	r6, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r6, r0, r6
	bne	.L259
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L261
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L260
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L261+4
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L259
.L260:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L261+8
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
.L259:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L262:
	.align	2
.L261:
	.word	.LANCHOR5
	.word	.LC93
	.word	.LC94
.LFE172:
	.size	ALERTS_pull_poc_card_added_or_removed_off_pile.constprop.54, .-ALERTS_pull_poc_card_added_or_removed_off_pile.constprop.54
	.section	.text.ALERTS_pull_weather_card_added_or_removed_off_pile.constprop.55,"ax",%progbits
	.align	2
	.type	ALERTS_pull_weather_card_added_or_removed_off_pile.constprop.55, %function
ALERTS_pull_weather_card_added_or_removed_off_pile.constprop.55:
.LFB171:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI46:
	mov	r8, r1
	mov	r5, r3
	add	r1, sp, #6
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r7, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #7
	mov	r2, r5
	mov	r3, #1
	mov	r6, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r6, r0, r6
	bne	.L264
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L266
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L265
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L266+4
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L264
.L265:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L266+8
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
.L264:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L267:
	.align	2
.L266:
	.word	.LANCHOR5
	.word	.LC95
	.word	.LC96
.LFE171:
	.size	ALERTS_pull_weather_card_added_or_removed_off_pile.constprop.55, .-ALERTS_pull_weather_card_added_or_removed_off_pile.constprop.55
	.section	.text.ALERTS_pull_communication_card_added_or_removed_off_pile.constprop.56,"ax",%progbits
	.align	2
	.type	ALERTS_pull_communication_card_added_or_removed_off_pile.constprop.56, %function
ALERTS_pull_communication_card_added_or_removed_off_pile.constprop.56:
.LFB170:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI47:
	mov	r8, r1
	mov	r5, r3
	add	r1, sp, #6
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r7, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #7
	mov	r2, r5
	mov	r3, #1
	mov	r6, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r6, r0, r6
	bne	.L269
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L271
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L270
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L271+4
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L269
.L270:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L271+8
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
.L269:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L272:
	.align	2
.L271:
	.word	.LANCHOR5
	.word	.LC97
	.word	.LC98
.LFE170:
	.size	ALERTS_pull_communication_card_added_or_removed_off_pile.constprop.56, .-ALERTS_pull_communication_card_added_or_removed_off_pile.constprop.56
	.section	.text.ALERTS_pull_station_terminal_added_or_removed_off_pile.constprop.57,"ax",%progbits
	.align	2
	.type	ALERTS_pull_station_terminal_added_or_removed_off_pile.constprop.57, %function
ALERTS_pull_station_terminal_added_or_removed_off_pile.constprop.57:
.LFB169:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI48:
	mov	r4, r3
	mov	r8, r1
	mov	r5, r2
	add	r1, sp, #13
	mov	r2, r3
	mov	r3, #1
	mov	r6, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #14
	mov	r2, r4
	mov	r3, #1
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #15
	mov	r2, r4
	mov	r3, #1
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L274
	bl	FLOWSENSE_we_are_poafs
	ldr	r2, .L276
	ldrb	r1, [sp, #15]	@ zero_extendqisi2
	ldrb	r3, [sp, #14]	@ zero_extendqisi2
	ldr	r2, [r2, r1, asl #2]
	add	r3, r3, #1
	mov	r3, r3, asl #3
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	cmp	r0, #0
	beq	.L275
	ldrb	r2, [sp, #13]	@ zero_extendqisi2
	mov	r0, r5
	add	r2, r2, #65
	str	r2, [sp, #8]
	mov	r1, #256
	ldr	r2, .L276+4
	sub	r3, r3, #7
	bl	snprintf
	b	.L274
.L275:
	mov	r0, r5
	mov	r1, #256
	ldr	r2, .L276+8
	sub	r3, r3, #7
	bl	snprintf
.L274:
	mov	r0, r7
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L277:
	.align	2
.L276:
	.word	.LANCHOR5
	.word	.LC99
	.word	.LC100
.LFE169:
	.size	ALERTS_pull_station_terminal_added_or_removed_off_pile.constprop.57, .-ALERTS_pull_station_terminal_added_or_removed_off_pile.constprop.57
	.section	.text.ALERTS_pull_lights_terminal_added_or_removed_off_pile.constprop.58,"ax",%progbits
	.align	2
	.type	ALERTS_pull_lights_terminal_added_or_removed_off_pile.constprop.58, %function
ALERTS_pull_lights_terminal_added_or_removed_off_pile.constprop.58:
.LFB168:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI49:
	mov	r8, r1
	mov	r5, r3
	add	r1, sp, #6
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r7, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #7
	mov	r2, r5
	mov	r3, #1
	mov	r6, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r6, r0, r6
	bne	.L279
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L281
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L280
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L281+4
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L279
.L280:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L281+8
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
.L279:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L282:
	.align	2
.L281:
	.word	.LANCHOR5
	.word	.LC101
	.word	.LC102
.LFE168:
	.size	ALERTS_pull_lights_terminal_added_or_removed_off_pile.constprop.58, .-ALERTS_pull_lights_terminal_added_or_removed_off_pile.constprop.58
	.section	.text.ALERTS_pull_poc_terminal_added_or_removed_off_pile.constprop.59,"ax",%progbits
	.align	2
	.type	ALERTS_pull_poc_terminal_added_or_removed_off_pile.constprop.59, %function
ALERTS_pull_poc_terminal_added_or_removed_off_pile.constprop.59:
.LFB167:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI50:
	mov	r8, r1
	mov	r5, r3
	add	r1, sp, #6
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r7, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #7
	mov	r2, r5
	mov	r3, #1
	mov	r6, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r6, r0, r6
	bne	.L284
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L286
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L285
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L286+4
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L284
.L285:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L286+8
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
.L284:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L287:
	.align	2
.L286:
	.word	.LANCHOR5
	.word	.LC103
	.word	.LC104
.LFE167:
	.size	ALERTS_pull_poc_terminal_added_or_removed_off_pile.constprop.59, .-ALERTS_pull_poc_terminal_added_or_removed_off_pile.constprop.59
	.section	.text.ALERTS_pull_weather_terminal_added_or_removed_off_pile.constprop.60,"ax",%progbits
	.align	2
	.type	ALERTS_pull_weather_terminal_added_or_removed_off_pile.constprop.60, %function
ALERTS_pull_weather_terminal_added_or_removed_off_pile.constprop.60:
.LFB166:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI51:
	mov	r8, r1
	mov	r5, r3
	add	r1, sp, #6
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r7, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #7
	mov	r2, r5
	mov	r3, #1
	mov	r6, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r6, r0, r6
	bne	.L289
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L291
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L290
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L291+4
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L289
.L290:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L291+8
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
.L289:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L292:
	.align	2
.L291:
	.word	.LANCHOR5
	.word	.LC105
	.word	.LC106
.LFE166:
	.size	ALERTS_pull_weather_terminal_added_or_removed_off_pile.constprop.60, .-ALERTS_pull_weather_terminal_added_or_removed_off_pile.constprop.60
	.section	.text.ALERTS_pull_communication_terminal_added_or_removed_off_pile.constprop.61,"ax",%progbits
	.align	2
	.type	ALERTS_pull_communication_terminal_added_or_removed_off_pile.constprop.61, %function
ALERTS_pull_communication_terminal_added_or_removed_off_pile.constprop.61:
.LFB165:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI52:
	mov	r8, r1
	mov	r5, r3
	add	r1, sp, #6
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r7, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #7
	mov	r2, r5
	mov	r3, #1
	mov	r6, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r6, r0, r6
	bne	.L294
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L296
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L295
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L296+4
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L294
.L295:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L296+8
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
.L294:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L297:
	.align	2
.L296:
	.word	.LANCHOR5
	.word	.LC107
	.word	.LC108
.LFE165:
	.size	ALERTS_pull_communication_terminal_added_or_removed_off_pile.constprop.61, .-ALERTS_pull_communication_terminal_added_or_removed_off_pile.constprop.61
	.section	.text.ALERTS_pull_two_wire_terminal_added_or_removed_off_pile.constprop.62,"ax",%progbits
	.align	2
	.type	ALERTS_pull_two_wire_terminal_added_or_removed_off_pile.constprop.62, %function
ALERTS_pull_two_wire_terminal_added_or_removed_off_pile.constprop.62:
.LFB164:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI53:
	mov	r8, r1
	mov	r5, r3
	add	r1, sp, #6
	mov	r4, r2
	mov	r2, r3
	mov	r3, #1
	mov	r7, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #7
	mov	r2, r5
	mov	r3, #1
	mov	r6, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r6, r0, r6
	bne	.L299
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L301
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L300
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	mov	r0, r4
	add	r2, r2, #65
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L301+4
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L299
.L300:
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L301+8
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
.L299:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L302:
	.align	2
.L301:
	.word	.LANCHOR5
	.word	.LC109
	.word	.LC110
.LFE164:
	.size	ALERTS_pull_two_wire_terminal_added_or_removed_off_pile.constprop.62, .-ALERTS_pull_two_wire_terminal_added_or_removed_off_pile.constprop.62
	.section	.text.ALERTS_pull_string_off_pile,"ax",%progbits
	.align	2
	.global	ALERTS_pull_string_off_pile
	.type	ALERTS_pull_string_off_pile, %function
ALERTS_pull_string_off_pile:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI54:
	ldr	r9, .L307
	mov	r5, r0
	mov	r4, r1
	mov	r0, r1
	mov	r1, #0
	mov	fp, r2
	mov	r7, r3
	bl	memset
	mov	r6, #0
.L305:
	ldr	r3, [r5, #16]
	ldr	r8, [r7, #0]
	ldr	sl, [r9, r3, asl #3]
	add	r6, r6, #1
	cmp	r6, fp
	ldrccb	r3, [sl, r8]	@ zero_extendqisi2
	mov	r0, r5
	strccb	r3, [r4], #1
	mov	r1, r7
	mov	r2, #1
	bl	ALERTS_inc_index
	ldrb	r3, [sl, r8]	@ zero_extendqisi2
	adds	r3, r3, #0
	movne	r3, #1
	cmp	r6, #256
	orreq	r3, r3, #1
	cmp	r3, #0
	bne	.L305
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L308:
	.align	2
.L307:
	.word	alert_piles
.LFE3:
	.size	ALERTS_pull_string_off_pile, .-ALERTS_pull_string_off_pile
	.section	.text.ALERTS_pull_walk_thru_station_added_or_removed_off_pile.constprop.1,"ax",%progbits
	.align	2
	.type	ALERTS_pull_walk_thru_station_added_or_removed_off_pile.constprop.1, %function
ALERTS_pull_walk_thru_station_added_or_removed_off_pile.constprop.1:
.LFB225:
	@ args = 0, pretend = 0, frame = 76
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI55:
	sub	sp, sp, #84
.LCFI56:
	mov	r5, r3
	mov	r8, r1
	mov	r6, r2
	add	r1, sp, #83
	mov	r2, r3
	mov	r3, #1
	mov	r4, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #8
	mov	r2, #48
	mov	r3, r5
	mov	r7, r0
	mov	r0, r4
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #72
	mov	r2, r5
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #76
	mov	r2, r5
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #80
	mov	r2, r5
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L310
	ldr	r3, .L316
	mov	r2, #256
	cmp	r4, r3
	ldreq	r1, .L316+4
	ldrne	r1, .L316+8
	mov	r0, r6
	bl	strlcpy
	ldr	r3, [sp, #76]
	ldr	r1, [sp, #72]
	cmp	r3, #1
	add	r2, sp, #56
	ldrb	r0, [sp, #83]	@ zero_extendqisi2
	mov	r3, #16
	bne	.L313
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r4, r0
	ldrh	r0, [sp, #80]
	bl	GetChangeReasonStr
	add	r3, sp, #8
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L316+12
	str	r0, [sp, #4]
	mov	r0, r6
	b	.L315
.L313:
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r4, r0
	ldrh	r0, [sp, #80]
	bl	GetChangeReasonStr
	ldr	r2, .L316+16
	add	r3, sp, #8
	mov	r1, #256
	str	r3, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r6
.L315:
	mov	r3, r4
	bl	sp_strlcat
.L310:
	mov	r0, r7
	add	sp, sp, #84
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L317:
	.align	2
.L316:
	.word	alerts_struct_changes
	.word	.LANCHOR1
	.word	.LC54
	.word	.LC111
	.word	.LC112
.LFE225:
	.size	ALERTS_pull_walk_thru_station_added_or_removed_off_pile.constprop.1, .-ALERTS_pull_walk_thru_station_added_or_removed_off_pile.constprop.1
	.section	.text.ALERTS_pull_firmware_update_off_pile.constprop.2,"ax",%progbits
	.align	2
	.type	ALERTS_pull_firmware_update_off_pile.constprop.2, %function
ALERTS_pull_firmware_update_off_pile.constprop.2:
.LFB224:
	@ args = 0, pretend = 0, frame = 132
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI57:
	sub	sp, sp, #140
.LCFI58:
	add	r4, sp, #8
	mov	sl, r1
	mov	r8, r2
	mov	r1, r4
	mov	r2, #64
	mov	r6, r0
	mov	r5, r3
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #72
	mov	r2, #64
	mov	r3, r5
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #139
	mov	r2, r5
	mov	r3, #1
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	sl, #200
	add	r7, r7, r0
	bne	.L319
	bl	FLOWSENSE_we_are_poafs
	str	r4, [sp, #0]
	cmp	r0, #0
	beq	.L320
	ldrb	r3, [sp, #139]	@ zero_extendqisi2
	mov	r0, r8
	add	r3, r3, #65
	str	r3, [sp, #4]
	mov	r1, #256
	ldr	r2, .L321
	add	r3, sp, #72
	bl	snprintf
	b	.L319
.L320:
	mov	r0, r8
	mov	r1, #256
	ldr	r2, .L321+4
	add	r3, sp, #72
	bl	snprintf
.L319:
	mov	r0, r7
	add	sp, sp, #140
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L322:
	.align	2
.L321:
	.word	.LC113
	.word	.LC114
.LFE224:
	.size	ALERTS_pull_firmware_update_off_pile.constprop.2, .-ALERTS_pull_firmware_update_off_pile.constprop.2
	.section	.text.ALERTS_pull_battery_backed_var_init_off_pile.constprop.3,"ax",%progbits
	.align	2
	.type	ALERTS_pull_battery_backed_var_init_off_pile.constprop.3, %function
ALERTS_pull_battery_backed_var_init_off_pile.constprop.3:
.LFB223:
	@ args = 0, pretend = 0, frame = 68
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI59:
	sub	sp, sp, #68
.LCFI60:
	mov	r8, r1
	mov	r4, r2
	mov	r1, sp
	mov	r2, #64
	mov	r6, r3
	mov	r7, r0
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #67
	mov	r2, r6
	mov	r3, #1
	mov	r5, r0
	mov	r0, r7
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r5, r0, r5
	bne	.L324
	ldr	r2, .L327
	mov	r1, #256
	mov	r3, sp
	mov	r0, r4
	bl	snprintf
	ldrb	r3, [sp, #67]	@ zero_extendqisi2
	mov	r0, r4
	cmp	r3, #1
	ldrls	r2, .L327+4
	ldrhi	r1, .L327+8
	ldrls	r1, [r2, r3, asl #2]
	mov	r2, #256
	bl	strlcat
.L324:
	mov	r0, r5
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L328:
	.align	2
.L327:
	.word	.LC115
	.word	.LANCHOR6
	.word	.LC116
.LFE223:
	.size	ALERTS_pull_battery_backed_var_init_off_pile.constprop.3, .-ALERTS_pull_battery_backed_var_init_off_pile.constprop.3
	.section	.text.ALERTS_pull_item_not_on_list_off_pile.constprop.4,"ax",%progbits
	.align	2
	.type	ALERTS_pull_item_not_on_list_off_pile.constprop.4, %function
ALERTS_pull_item_not_on_list_off_pile.constprop.4:
.LFB222:
	@ args = 4, pretend = 0, frame = 196
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI61:
	sub	sp, sp, #208
.LCFI62:
	mov	sl, r1
	mov	r8, r2
	add	r1, sp, #12
	mov	r2, #64
	mov	r6, r0
	mov	r5, r3
	ldr	r9, [sp, #240]
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #76
	mov	r2, #64
	mov	r3, r5
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	cmp	r9, #41
	add	r4, r0, r4
	bne	.L330
	add	r1, sp, #140
	mov	r2, #64
	mov	r3, r5
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #204
	mov	r2, r5
	mov	r3, #4
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r0, r7, r0
	add	r4, r4, r0
.L330:
	cmp	sl, #200
	bne	.L331
	add	r3, sp, #76
	cmp	r9, #41
	str	r3, [sp, #0]
	bne	.L332
	add	r3, sp, #140
	str	r3, [sp, #4]
	ldr	r3, [sp, #204]
	mov	r0, r8
	str	r3, [sp, #8]
	mov	r1, #256
	ldr	r2, .L333
	add	r3, sp, #12
	bl	snprintf
	b	.L331
.L332:
	mov	r0, r8
	mov	r1, #256
	ldr	r2, .L333+4
	add	r3, sp, #12
	bl	snprintf
.L331:
	mov	r0, r4
	add	sp, sp, #208
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L334:
	.align	2
.L333:
	.word	.LC117
	.word	.LC118
.LFE222:
	.size	ALERTS_pull_item_not_on_list_off_pile.constprop.4, .-ALERTS_pull_item_not_on_list_off_pile.constprop.4
	.section	.text.ALERTS_pull_flash_file_found_off_pile.constprop.5,"ax",%progbits
	.align	2
	.type	ALERTS_pull_flash_file_found_off_pile.constprop.5, %function
ALERTS_pull_flash_file_found_off_pile.constprop.5:
.LFB221:
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI63:
	sub	sp, sp, #56
.LCFI64:
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #12
	mov	r2, #32
	mov	r4, r3
	mov	r5, r0
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #44
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #48
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #52
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L336
	ldr	r3, [sp, #44]
	mov	r0, r8
	str	r3, [sp, #0]
	ldr	r3, [sp, #48]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r3, [sp, #52]
	ldr	r2, .L337
	str	r3, [sp, #8]
	add	r3, sp, #12
	bl	snprintf
.L336:
	mov	r0, r7
	add	sp, sp, #56
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L338:
	.align	2
.L337:
	.word	.LC119
.LFE221:
	.size	ALERTS_pull_flash_file_found_off_pile.constprop.5, .-ALERTS_pull_flash_file_found_off_pile.constprop.5
	.section	.text.ALERTS_pull_flash_file_deleting_off_pile.constprop.6,"ax",%progbits
	.align	2
	.type	ALERTS_pull_flash_file_deleting_off_pile.constprop.6, %function
ALERTS_pull_flash_file_deleting_off_pile.constprop.6:
.LFB220:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI65:
	sub	sp, sp, #64
.LCFI66:
	mov	r4, r3
	mov	r8, r1
	mov	sl, r2
	add	r1, sp, #63
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r6, sp, #16
	mov	r1, r6
	mov	r2, #32
	mov	r3, r4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #48
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #52
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #56
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L340
	ldr	r2, [sp, #48]
	ldrb	r3, [sp, #63]	@ zero_extendqisi2
	str	r2, [sp, #4]
	ldr	r2, [sp, #52]
	mov	r0, sl
	str	r2, [sp, #8]
	ldr	r2, [sp, #56]
	mov	r1, #256
	str	r2, [sp, #12]
	ldr	r2, .L341
	str	r6, [sp, #0]
	bl	snprintf
.L340:
	mov	r0, r7
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L342:
	.align	2
.L341:
	.word	.LC120
.LFE220:
	.size	ALERTS_pull_flash_file_deleting_off_pile.constprop.6, .-ALERTS_pull_flash_file_deleting_off_pile.constprop.6
	.section	.text.ALERTS_pull_flash_writing_off_pile.constprop.7,"ax",%progbits
	.align	2
	.type	ALERTS_pull_flash_writing_off_pile.constprop.7, %function
ALERTS_pull_flash_writing_off_pile.constprop.7:
.LFB219:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI67:
	sub	sp, sp, #64
.LCFI68:
	mov	r4, r3
	mov	r8, r1
	mov	sl, r2
	add	r1, sp, #63
	mov	r2, r3
	mov	r3, #1
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	add	r6, sp, #16
	mov	r1, r6
	mov	r2, #32
	mov	r3, r4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #48
	mov	r2, r4
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #52
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #56
	mov	r2, r4
	mov	r3, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L344
	ldr	r2, [sp, #48]
	ldrb	r3, [sp, #63]	@ zero_extendqisi2
	str	r2, [sp, #4]
	ldr	r2, [sp, #52]
	mov	r0, sl
	str	r2, [sp, #8]
	ldr	r2, [sp, #56]
	mov	r1, #256
	str	r2, [sp, #12]
	ldr	r2, .L345
	str	r6, [sp, #0]
	bl	snprintf
.L344:
	mov	r0, r7
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L346:
	.align	2
.L345:
	.word	.LC121
.LFE219:
	.size	ALERTS_pull_flash_writing_off_pile.constprop.7, .-ALERTS_pull_flash_writing_off_pile.constprop.7
	.section	.text.ALERTS_pull_range_check_failed_bool_off_pile.constprop.10,"ax",%progbits
	.align	2
	.type	ALERTS_pull_range_check_failed_bool_off_pile.constprop.10, %function
ALERTS_pull_range_check_failed_bool_off_pile.constprop.10:
.LFB216:
	@ args = 8, pretend = 0, frame = 168
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI69:
	sub	sp, sp, #180
.LCFI70:
	mov	sl, r1
	mov	r6, r2
	add	r1, sp, #76
	mov	r2, #48
	ldr	r9, [sp, #216]
	mov	r5, r0
	mov	r4, r3
	ldr	fp, [sp, #220]
	bl	ALERTS_pull_string_off_pile
	cmp	r9, #1
	mov	r8, r0
	bne	.L348
	mov	r0, r5
	add	r1, sp, #124
	mov	r2, #48
	mov	r3, r4
	bl	ALERTS_pull_string_off_pile
	add	r8, r8, r0
.L348:
	add	r1, sp, #178
	mov	r2, r4
	mov	r3, #1
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #179
	mov	r2, r4
	mov	r3, #1
	add	r8, r8, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	fp, #1
	add	r8, r8, r0
	bne	.L349
	add	r1, sp, #12
	mov	r2, #64
	mov	r3, r4
	mov	r0, r5
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #172
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r0, r7, r0
	add	r8, r8, r0
.L349:
	cmp	sl, #200
	bne	.L350
	cmp	r9, #1
	ldrb	r4, [sp, #178]	@ zero_extendqisi2
	ldrb	r0, [sp, #179]	@ zero_extendqisi2
	bne	.L351
	bl	GetNoYesStr
	add	r3, sp, #124
	stmia	sp, {r3, r4}
	ldr	r2, .L353
	mov	r1, #256
	add	r3, sp, #76
	str	r0, [sp, #8]
	mov	r0, r6
	bl	snprintf
	b	.L352
.L351:
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L353+4
	add	r3, sp, #76
	str	r4, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r6
	bl	snprintf
.L352:
	cmp	fp, #1
	bne	.L350
	ldr	r3, [sp, #172]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L353+8
	add	r3, sp, #12
	bl	sp_strlcat
.L350:
	mov	r0, r8
	add	sp, sp, #180
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L354:
	.align	2
.L353:
	.word	.LC122
	.word	.LC123
	.word	.LC124
.LFE216:
	.size	ALERTS_pull_range_check_failed_bool_off_pile.constprop.10, .-ALERTS_pull_range_check_failed_bool_off_pile.constprop.10
	.section	.text.ALERTS_pull_range_check_failed_date_off_pile.constprop.11,"ax",%progbits
	.align	2
	.type	ALERTS_pull_range_check_failed_date_off_pile.constprop.11, %function
ALERTS_pull_range_check_failed_date_off_pile.constprop.11:
.LFB215:
	@ args = 8, pretend = 0, frame = 200
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI71:
	sub	sp, sp, #208
.LCFI72:
	mov	sl, r1
	mov	r6, r2
	add	r1, sp, #72
	mov	r2, #48
	ldr	r9, [sp, #244]
	mov	r5, r0
	mov	r4, r3
	ldr	fp, [sp, #248]
	bl	ALERTS_pull_string_off_pile
	cmp	r9, #1
	mov	r8, r0
	bne	.L356
	mov	r0, r5
	add	r1, sp, #120
	mov	r2, #48
	mov	r3, r4
	bl	ALERTS_pull_string_off_pile
	add	r8, r8, r0
.L356:
	mov	r0, r5
	add	r1, sp, #206
	mov	r2, r4
	mov	r3, #2
	bl	ALERTS_pull_object_off_pile
	cmp	fp, #1
	add	r8, r8, r0
	bne	.L357
	add	r1, sp, #8
	mov	r2, #64
	mov	r3, r4
	mov	r0, r5
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #200
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r0, r7, r0
	add	r8, r8, r0
.L357:
	cmp	sl, #200
	bne	.L358
	mov	r3, #250
	cmp	r9, #1
	str	r3, [sp, #0]
	ldrh	r2, [sp, #206]
	add	r0, sp, #168
	mov	r1, #32
	mov	r3, #100
	bne	.L359
	bl	GetDateStr
	add	r3, sp, #120
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L361
	add	r3, sp, #72
	str	r0, [sp, #4]
	mov	r0, r6
	bl	snprintf
	b	.L360
.L359:
	bl	GetDateStr
	mov	r1, #256
	ldr	r2, .L361+4
	add	r3, sp, #72
	str	r0, [sp, #0]
	mov	r0, r6
	bl	snprintf
.L360:
	cmp	fp, #1
	bne	.L358
	ldr	r3, [sp, #200]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L361+8
	add	r3, sp, #8
	bl	sp_strlcat
.L358:
	mov	r0, r8
	add	sp, sp, #208
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L362:
	.align	2
.L361:
	.word	.LC125
	.word	.LC126
	.word	.LC124
.LFE215:
	.size	ALERTS_pull_range_check_failed_date_off_pile.constprop.11, .-ALERTS_pull_range_check_failed_date_off_pile.constprop.11
	.section	.text.ALERTS_pull_range_check_failed_float_off_pile.constprop.12,"ax",%progbits
	.align	2
	.type	ALERTS_pull_range_check_failed_float_off_pile.constprop.12, %function
ALERTS_pull_range_check_failed_float_off_pile.constprop.12:
.LFB214:
	@ args = 8, pretend = 0, frame = 172
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI73:
	sub	sp, sp, #192
.LCFI74:
	mov	sl, r1
	mov	r6, r2
	add	r1, sp, #84
	mov	r2, #48
	ldr	r9, [sp, #228]
	mov	r5, r0
	mov	r4, r3
	ldr	fp, [sp, #232]
	bl	ALERTS_pull_string_off_pile
	cmp	r9, #1
	mov	r8, r0
	bne	.L364
	mov	r0, r5
	add	r1, sp, #132
	mov	r2, #48
	mov	r3, r4
	bl	ALERTS_pull_string_off_pile
	add	r8, r8, r0
.L364:
	add	r1, sp, #180
	mov	r2, r4
	mov	r3, #4
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #184
	mov	r2, r4
	mov	r3, #4
	add	r8, r8, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	fp, #1
	add	r8, r8, r0
	bne	.L365
	add	r1, sp, #20
	mov	r2, #64
	mov	r3, r4
	mov	r0, r5
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #188
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r0, r7, r0
	add	r8, r8, r0
.L365:
	cmp	sl, #200
	bne	.L366
	cmp	r9, #1
	flds	s14, [sp, #180]
	bne	.L367
	fcvtds	d7, s14
	add	r3, sp, #132
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L369
	add	r3, sp, #84
	fstd	d7, [sp, #4]
	flds	s14, [sp, #184]
	fcvtds	d7, s14
	fstd	d7, [sp, #12]
	bl	snprintf
	b	.L368
.L367:
	fcvtds	d7, s14
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L369+4
	add	r3, sp, #84
	fstd	d7, [sp, #0]
	flds	s14, [sp, #184]
	fcvtds	d7, s14
	fstd	d7, [sp, #8]
	bl	snprintf
.L368:
	cmp	fp, #1
	bne	.L366
	ldr	r3, [sp, #188]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L369+8
	add	r3, sp, #20
	bl	sp_strlcat
.L366:
	mov	r0, r8
	add	sp, sp, #192
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L370:
	.align	2
.L369:
	.word	.LC127
	.word	.LC128
	.word	.LC124
.LFE214:
	.size	ALERTS_pull_range_check_failed_float_off_pile.constprop.12, .-ALERTS_pull_range_check_failed_float_off_pile.constprop.12
	.section	.text.ALERTS_pull_range_check_failed_int32_off_pile.constprop.13,"ax",%progbits
	.align	2
	.type	ALERTS_pull_range_check_failed_int32_off_pile.constprop.13, %function
ALERTS_pull_range_check_failed_int32_off_pile.constprop.13:
.LFB213:
	@ args = 8, pretend = 0, frame = 172
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI75:
	sub	sp, sp, #184
.LCFI76:
	mov	sl, r1
	mov	r6, r2
	add	r1, sp, #76
	mov	r2, #48
	ldr	r9, [sp, #220]
	mov	r5, r0
	mov	r4, r3
	ldr	fp, [sp, #224]
	bl	ALERTS_pull_string_off_pile
	cmp	r9, #1
	mov	r8, r0
	bne	.L372
	mov	r0, r5
	add	r1, sp, #124
	mov	r2, #48
	mov	r3, r4
	bl	ALERTS_pull_string_off_pile
	add	r8, r8, r0
.L372:
	add	r1, sp, #172
	mov	r2, r4
	mov	r3, #4
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #176
	mov	r2, r4
	mov	r3, #4
	add	r8, r8, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	fp, #1
	add	r8, r8, r0
	bne	.L373
	add	r1, sp, #12
	mov	r2, #64
	mov	r3, r4
	mov	r0, r5
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #180
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r0, r7, r0
	add	r8, r8, r0
.L373:
	cmp	sl, #200
	bne	.L374
	cmp	r9, #1
	ldr	r3, [sp, #172]
	bne	.L375
	add	r2, sp, #124
	stmia	sp, {r2, r3}
	ldr	r3, [sp, #176]
	mov	r0, r6
	str	r3, [sp, #8]
	mov	r1, #256
	ldr	r2, .L377
	add	r3, sp, #76
	bl	snprintf
	b	.L376
.L375:
	str	r3, [sp, #0]
	ldr	r3, [sp, #176]
	mov	r0, r6
	str	r3, [sp, #4]
	mov	r1, #256
	ldr	r2, .L377+4
	add	r3, sp, #76
	bl	snprintf
.L376:
	cmp	fp, #1
	bne	.L374
	ldr	r3, [sp, #180]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L377+8
	add	r3, sp, #12
	bl	sp_strlcat
.L374:
	mov	r0, r8
	add	sp, sp, #184
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L378:
	.align	2
.L377:
	.word	.LC129
	.word	.LC130
	.word	.LC124
.LFE213:
	.size	ALERTS_pull_range_check_failed_int32_off_pile.constprop.13, .-ALERTS_pull_range_check_failed_int32_off_pile.constprop.13
	.section	.text.ALERTS_pull_range_check_failed_string_off_pile.constprop.14,"ax",%progbits
	.align	2
	.type	ALERTS_pull_range_check_failed_string_off_pile.constprop.14, %function
ALERTS_pull_range_check_failed_string_off_pile.constprop.14:
.LFB212:
	@ args = 4, pretend = 0, frame = 164
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI77:
	sub	sp, sp, #168
.LCFI78:
	mov	sl, r1
	mov	r8, r2
	add	r1, sp, #68
	mov	r2, #48
	mov	r6, r0
	mov	r5, r3
	ldr	r9, [sp, #200]
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #116
	mov	r2, #48
	mov	r3, r5
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	cmp	r9, #1
	add	r4, r0, r4
	bne	.L380
	add	r1, sp, #4
	mov	r2, #64
	mov	r3, r5
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #164
	mov	r2, r5
	mov	r3, #4
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r0, r7, r0
	add	r4, r4, r0
.L380:
	cmp	sl, #200
	bne	.L381
	mov	r0, r8
	mov	r1, #256
	ldr	r2, .L382
	add	r3, sp, #68
	bl	snprintf
	cmp	r9, #1
	bne	.L381
	ldr	r3, [sp, #164]
	mov	r0, r8
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L382+4
	add	r3, sp, #4
	bl	sp_strlcat
.L381:
	mov	r0, r4
	add	sp, sp, #168
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L383:
	.align	2
.L382:
	.word	.LC131
	.word	.LC124
.LFE212:
	.size	ALERTS_pull_range_check_failed_string_off_pile.constprop.14, .-ALERTS_pull_range_check_failed_string_off_pile.constprop.14
	.section	.text.ALERTS_pull_range_check_failed_time_off_pile.constprop.15,"ax",%progbits
	.align	2
	.type	ALERTS_pull_range_check_failed_time_off_pile.constprop.15, %function
ALERTS_pull_range_check_failed_time_off_pile.constprop.15:
.LFB211:
	@ args = 8, pretend = 0, frame = 200
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI79:
	sub	sp, sp, #208
.LCFI80:
	mov	r9, r1
	mov	r6, r2
	add	r1, sp, #72
	mov	r2, #48
	ldr	sl, [sp, #244]
	mov	r5, r0
	mov	r4, r3
	ldr	fp, [sp, #248]
	bl	ALERTS_pull_string_off_pile
	cmp	sl, #1
	mov	r8, r0
	bne	.L385
	mov	r0, r5
	add	r1, sp, #120
	mov	r2, #48
	mov	r3, r4
	bl	ALERTS_pull_string_off_pile
	add	r8, r8, r0
.L385:
	mov	r0, r5
	add	r1, sp, #200
	mov	r2, r4
	mov	r3, #4
	bl	ALERTS_pull_object_off_pile
	cmp	fp, #1
	add	r8, r8, r0
	bne	.L386
	add	r1, sp, #8
	mov	r2, #64
	mov	r3, r4
	mov	r0, r5
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #204
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r0, r7, r0
	add	r8, r8, r0
.L386:
	cmp	r9, #200
	bne	.L387
	mov	r3, #0
	cmp	sl, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	add	r0, sp, #168
	mov	r1, #32
	ldr	r2, [sp, #200]
	bne	.L388
	mov	r3, sl
	bl	TDUTILS_time_to_time_string_with_ampm
	add	r3, sp, #120
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L390
	add	r3, sp, #72
	str	r0, [sp, #4]
	mov	r0, r6
	bl	snprintf
	b	.L389
.L388:
	mov	r3, #1
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L390+4
	add	r3, sp, #72
	str	r0, [sp, #0]
	mov	r0, r6
	bl	snprintf
.L389:
	cmp	fp, #1
	bne	.L387
	ldr	r3, [sp, #204]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L390+8
	add	r3, sp, #8
	bl	sp_strlcat
.L387:
	mov	r0, r8
	add	sp, sp, #208
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L391:
	.align	2
.L390:
	.word	.LC125
	.word	.LC126
	.word	.LC124
.LFE211:
	.size	ALERTS_pull_range_check_failed_time_off_pile.constprop.15, .-ALERTS_pull_range_check_failed_time_off_pile.constprop.15
	.section	.text.ALERTS_pull_range_check_failed_uint32_off_pile.constprop.16,"ax",%progbits
	.align	2
	.type	ALERTS_pull_range_check_failed_uint32_off_pile.constprop.16, %function
ALERTS_pull_range_check_failed_uint32_off_pile.constprop.16:
.LFB210:
	@ args = 8, pretend = 0, frame = 172
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI81:
	sub	sp, sp, #184
.LCFI82:
	mov	sl, r1
	mov	r6, r2
	add	r1, sp, #76
	mov	r2, #48
	ldr	r9, [sp, #220]
	mov	r5, r0
	mov	r4, r3
	ldr	fp, [sp, #224]
	bl	ALERTS_pull_string_off_pile
	cmp	r9, #1
	mov	r8, r0
	bne	.L393
	mov	r0, r5
	add	r1, sp, #124
	mov	r2, #48
	mov	r3, r4
	bl	ALERTS_pull_string_off_pile
	add	r8, r8, r0
.L393:
	add	r1, sp, #172
	mov	r2, r4
	mov	r3, #4
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #176
	mov	r2, r4
	mov	r3, #4
	add	r8, r8, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	fp, #1
	add	r8, r8, r0
	bne	.L394
	add	r1, sp, #12
	mov	r2, #64
	mov	r3, r4
	mov	r0, r5
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #180
	mov	r2, r4
	mov	r3, #4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r0, r7, r0
	add	r8, r8, r0
.L394:
	cmp	sl, #200
	bne	.L395
	cmp	r9, #1
	ldr	r3, [sp, #172]
	bne	.L396
	add	r2, sp, #124
	stmia	sp, {r2, r3}
	ldr	r3, [sp, #176]
	mov	r0, r6
	str	r3, [sp, #8]
	mov	r1, #256
	ldr	r2, .L398
	add	r3, sp, #76
	bl	snprintf
	b	.L397
.L396:
	str	r3, [sp, #0]
	ldr	r3, [sp, #176]
	mov	r0, r6
	str	r3, [sp, #4]
	mov	r1, #256
	ldr	r2, .L398+4
	add	r3, sp, #76
	bl	snprintf
.L397:
	cmp	fp, #1
	bne	.L395
	ldr	r3, [sp, #180]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L398+8
	add	r3, sp, #12
	bl	sp_strlcat
.L395:
	mov	r0, r8
	add	sp, sp, #184
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L399:
	.align	2
.L398:
	.word	.LC129
	.word	.LC130
	.word	.LC124
.LFE210:
	.size	ALERTS_pull_range_check_failed_uint32_off_pile.constprop.16, .-ALERTS_pull_range_check_failed_uint32_off_pile.constprop.16
	.section	.text.ALERTS_derate_table_update_failed_off_pile.constprop.17,"ax",%progbits
	.align	2
	.type	ALERTS_derate_table_update_failed_off_pile.constprop.17, %function
ALERTS_derate_table_update_failed_off_pile.constprop.17:
.LFB209:
	@ args = 4, pretend = 0, frame = 132
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI83:
	sub	sp, sp, #140
.LCFI84:
	ldr	r8, [sp, #172]
	mov	r6, r1
	mov	r9, r2
	mov	sl, r0
	mov	r2, r8
	mov	r0, r1
	mov	r5, r3
	add	r1, sp, #136
	mov	r3, #2
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #138
	mov	r2, r8
	mov	r3, #2
	add	r4, sp, #8
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	mov	r1, r4
	mov	r2, #128
	mov	r3, r8
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	cmp	r9, #200
	add	r7, r7, r0
	bne	.L401
	cmp	sl, #185
	ldrh	r3, [sp, #138]
	bne	.L402
	mov	r0, r5
	mov	r1, #256
	ldr	r2, .L405
	str	r4, [sp, #0]
	bl	snprintf
	b	.L401
.L402:
	ldrh	r2, [sp, #136]
	cmp	sl, #186
	stmia	sp, {r2, r4}
	ldreq	r2, .L405+4
	mov	r0, r5
	mov	r1, #256
	ldrne	r2, .L405+8
	bl	snprintf
.L401:
	mov	r0, r7
	add	sp, sp, #140
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L406:
	.align	2
.L405:
	.word	.LC132
	.word	.LC133
	.word	.LC134
.LFE209:
	.size	ALERTS_derate_table_update_failed_off_pile.constprop.17, .-ALERTS_derate_table_update_failed_off_pile.constprop.17
	.section	.text.ALERTS_pull_mainline_break_off_pile.constprop.20,"ax",%progbits
	.align	2
	.type	ALERTS_pull_mainline_break_off_pile.constprop.20, %function
ALERTS_pull_mainline_break_off_pile.constprop.20:
.LFB206:
	@ args = 0, pretend = 0, frame = 104
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI85:
	sub	sp, sp, #120
.LCFI86:
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #64
	mov	r2, #32
	mov	r5, r0
	mov	r4, r3
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #16
	mov	r2, #48
	mov	r3, r4
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #119
	mov	r2, r4
	mov	r3, #1
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #114
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #116
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L408
	mov	r3, #0
	strb	r3, [sp, #96]
	ldrb	r3, [sp, #119]	@ zero_extendqisi2
	add	r0, sp, #96
	cmp	r3, #10
	ldreq	r1, .L414
	beq	.L413
	cmp	r3, #20
	ldreq	r1, .L414+4
	beq	.L413
	cmp	r3, #30
	ldreq	r1, .L414+8
	ldrne	r1, .L414+12
.L413:
	mov	r2, #16
	bl	strlcpy
	add	r3, sp, #16
	str	r3, [sp, #0]
	add	r3, sp, #96
	str	r3, [sp, #4]
	ldrh	r3, [sp, #114]
	mov	r0, r8
	str	r3, [sp, #8]
	ldrh	r3, [sp, #116]
	mov	r1, #256
	str	r3, [sp, #12]
	ldr	r2, .L414+16
	add	r3, sp, #64
	bl	snprintf
.L408:
	mov	r0, r7
	add	sp, sp, #120
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L415:
	.align	2
.L414:
	.word	.LC135
	.word	.LC136
	.word	.LC137
	.word	.LC138
	.word	.LC139
.LFE206:
	.size	ALERTS_pull_mainline_break_off_pile.constprop.20, .-ALERTS_pull_mainline_break_off_pile.constprop.20
	.section	.text.ALERTS_pull_two_wire_poc_decoder_fault_off_pile.constprop.23,"ax",%progbits
	.align	2
	.type	ALERTS_pull_two_wire_poc_decoder_fault_off_pile.constprop.23, %function
ALERTS_pull_two_wire_poc_decoder_fault_off_pile.constprop.23:
.LFB203:
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI87:
	sub	sp, sp, #76
.LCFI88:
	mov	r5, r3
	mov	sl, r1
	mov	r8, r2
	add	r1, sp, #74
	mov	r2, r3
	mov	r3, #1
	mov	r6, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #75
	mov	r2, r5
	mov	r3, #1
	add	r4, sp, #4
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #68
	mov	r2, r5
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	mov	r1, r4
	mov	r2, #64
	mov	r3, r5
	add	r7, r7, r0
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	cmp	sl, #200
	add	r7, r7, r0
	bne	.L417
	ldrb	r3, [sp, #74]	@ zero_extendqisi2
	cmp	r3, #1
	streq	r4, [sp, #0]
	moveq	r0, r8
	moveq	r1, #256
	ldreq	r2, .L421
	beq	.L420
.L418:
	cmp	r3, #2
	streq	r4, [sp, #0]
	moveq	r0, r8
	moveq	r1, #256
	ldreq	r2, .L421+4
	beq	.L420
.L419:
	cmp	r3, #3
	bne	.L417
	ldr	r2, .L421+8
	mov	r0, r8
	mov	r1, #256
	str	r4, [sp, #0]
.L420:
	ldr	r3, [sp, #68]
	bl	snprintf
.L417:
	mov	r0, r7
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L422:
	.align	2
.L421:
	.word	.LC140
	.word	.LC141
	.word	.LC142
.LFE203:
	.size	ALERTS_pull_two_wire_poc_decoder_fault_off_pile.constprop.23, .-ALERTS_pull_two_wire_poc_decoder_fault_off_pile.constprop.23
	.section	.text.ALERTS_pull_budget_group_reduction_off_pile.constprop.29,"ax",%progbits
	.align	2
	.type	ALERTS_pull_budget_group_reduction_off_pile.constprop.29, %function
ALERTS_pull_budget_group_reduction_off_pile.constprop.29:
.LFB197:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI89:
	sub	sp, sp, #60
.LCFI90:
	mov	r8, r1
	mov	r4, r2
	add	r1, sp, #4
	mov	r2, #48
	mov	r5, r3
	mov	r6, r0
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #52
	mov	r2, r5
	mov	r3, #4
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #56
	mov	r2, r5
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L424
	ldr	r3, [sp, #56]
	mov	r0, r4
	cmp	r3, #0
	ldr	r3, [sp, #52]
	mov	r1, #256
	str	r3, [sp, #0]
	ldreq	r2, .L427
	ldrne	r2, .L427+4
	add	r3, sp, #4
	bl	snprintf
.L424:
	mov	r0, r7
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L428:
	.align	2
.L427:
	.word	.LC143
	.word	.LC144
.LFE197:
	.size	ALERTS_pull_budget_group_reduction_off_pile.constprop.29, .-ALERTS_pull_budget_group_reduction_off_pile.constprop.29
	.global	__udivsi3
	.section	.text.ALERTS_pull_budget_period_ended_off_pile.constprop.30,"ax",%progbits
	.align	2
	.type	ALERTS_pull_budget_period_ended_off_pile.constprop.30, %function
ALERTS_pull_budget_period_ended_off_pile.constprop.30:
.LFB196:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI91:
	sub	sp, sp, #60
.LCFI92:
	mov	r8, r1
	mov	r4, r2
	add	r1, sp, #4
	mov	r2, #48
	mov	r5, r3
	mov	r6, r0
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #52
	mov	r2, r5
	mov	r3, #4
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #56
	mov	r2, r5
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L430
	ldr	r1, [sp, #52]
	cmp	r1, #0
	bne	.L431
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L434
	bl	snprintf
	b	.L430
.L431:
	ldr	r3, [sp, #56]
	mov	r2, #100
	cmp	r1, r3
	bcc	.L432
	rsb	r3, r3, r1
	mul	r0, r2, r3
	bl	__udivsi3
	mov	r1, #256
	ldr	r2, .L434+4
	str	r0, [sp, #0]
	mov	r0, r4
	b	.L433
.L432:
	rsb	r3, r1, r3
	mul	r0, r2, r3
	bl	__udivsi3
	ldr	r2, .L434+8
	mov	r1, #256
	str	r0, [sp, #0]
	mov	r0, r4
.L433:
	add	r3, sp, #4
	bl	snprintf
.L430:
	mov	r0, r7
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L435:
	.align	2
.L434:
	.word	.LC145
	.word	.LC146
	.word	.LC147
.LFE196:
	.size	ALERTS_pull_budget_period_ended_off_pile.constprop.30, .-ALERTS_pull_budget_period_ended_off_pile.constprop.30
	.section	.text.ALERTS_pull_MVOR_alert_off_pile.constprop.32,"ax",%progbits
	.align	2
	.type	ALERTS_pull_MVOR_alert_off_pile.constprop.32, %function
ALERTS_pull_MVOR_alert_off_pile.constprop.32:
.LFB194:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI93:
	sub	sp, sp, #68
.LCFI94:
	mov	r8, r1
	mov	r4, r2
	add	r1, sp, #12
	mov	r2, #48
	mov	r5, r3
	mov	r6, r0
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #66
	mov	r2, r5
	mov	r3, #1
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #60
	mov	r2, r5
	mov	r3, #4
	add	r7, r0, r7
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #67
	mov	r2, r5
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L437
	ldrb	r3, [sp, #67]	@ zero_extendqisi2
	cmp	r3, #8
	movhi	r3, #8
	strhib	r3, [sp, #67]
	ldrb	r3, [sp, #66]	@ zero_extendqisi2
	cmp	r3, #1
	beq	.L440
	bcc	.L439
	cmp	r3, #2
	bne	.L437
	b	.L444
.L439:
	flds	s13, [sp, #60]	@ int
	ldr	r3, .L445
	ldrb	r2, [sp, #67]	@ zero_extendqisi2
	fuitos	s14, s13
	flds	s15, [r3, #0]
	ldr	r3, .L445+4
	mov	r0, r4
	ldr	r3, [r3, r2, asl #2]
	mov	r1, #256
	fdivs	s14, s14, s15
	str	r3, [sp, #8]
	ldr	r2, .L445+8
	fcvtds	d7, s14
	fstd	d7, [sp, #0]
	b	.L443
.L440:
	flds	s13, [sp, #60]	@ int
	ldr	r3, .L445
	ldrb	r2, [sp, #67]	@ zero_extendqisi2
	fuitos	s14, s13
	flds	s15, [r3, #0]
	ldr	r3, .L445+4
	mov	r0, r4
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, .L445+12
	fdivs	s14, s14, s15
	mov	r1, #256
	str	r3, [sp, #8]
	fcvtds	d7, s14
	fstd	d7, [sp, #0]
.L443:
	add	r3, sp, #12
	bl	snprintf
	b	.L437
.L444:
	ldrb	r3, [sp, #67]	@ zero_extendqisi2
	cmp	r3, #4
	bne	.L442
	mov	r0, r4
	mov	r1, #256
	ldr	r2, .L445+16
	add	r3, sp, #12
	bl	snprintf
	b	.L437
.L442:
	ldr	r2, .L445+4
	mov	r0, r4
	ldr	r3, [r2, r3, asl #2]
	mov	r1, #256
	str	r3, [sp, #0]
	ldr	r2, .L445+20
	add	r3, sp, #12
	bl	snprintf
.L437:
	mov	r0, r7
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L446:
	.align	2
.L445:
	.word	.LANCHOR7
	.word	.LANCHOR2
	.word	.LC148
	.word	.LC149
	.word	.LC150
	.word	.LC151
.LFE194:
	.size	ALERTS_pull_MVOR_alert_off_pile.constprop.32, .-ALERTS_pull_MVOR_alert_off_pile.constprop.32
	.section	.text.ALERTS_pull_ETTable_Substitution_off_pile.constprop.41,"ax",%progbits
	.align	2
	.type	ALERTS_pull_ETTable_Substitution_off_pile.constprop.41, %function
ALERTS_pull_ETTable_Substitution_off_pile.constprop.41:
.LFB185:
	@ args = 0, pretend = 0, frame = 168
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI95:
	sub	sp, sp, #184
.LCFI96:
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #16
	mov	r2, #128
	mov	r4, r3
	mov	r5, r0
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #178
	mov	r2, r4
	mov	r3, #2
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #180
	mov	r2, r4
	mov	r3, #2
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #182
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L448
	mov	r3, #250
	ldrh	r2, [sp, #178]
	str	r3, [sp, #0]
	mov	r1, #32
	mov	r3, #150
	add	r0, sp, #144
	bl	GetDateStr
	ldrh	r2, [sp, #180]
	fldd	d7, .L449
	mov	r1, #256
	fmsr	s11, r2	@ int
	ldrh	r2, [sp, #182]
	fsitod	d6, s11
	fmsr	s11, r2	@ int
	ldr	r2, .L449+8
	fdivd	d6, d6, d7
	mov	r3, r0
	mov	r0, r8
	fstd	d6, [sp, #0]
	fsitod	d6, s11
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	snprintf
.L448:
	mov	r0, r7
	add	sp, sp, #184
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L450:
	.align	2
.L449:
	.word	0
	.word	1079574528
	.word	.LC152
.LFE185:
	.size	ALERTS_pull_ETTable_Substitution_off_pile.constprop.41, .-ALERTS_pull_ETTable_Substitution_off_pile.constprop.41
	.section	.text.ALERTS_pull_ETTable_LimitedEntry_off_pile.constprop.42,"ax",%progbits
	.align	2
	.type	ALERTS_pull_ETTable_LimitedEntry_off_pile.constprop.42, %function
ALERTS_pull_ETTable_LimitedEntry_off_pile.constprop.42:
.LFB184:
	@ args = 0, pretend = 0, frame = 168
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI97:
	sub	sp, sp, #184
.LCFI98:
	mov	r6, r1
	mov	r8, r2
	add	r1, sp, #16
	mov	r2, #128
	mov	r4, r3
	mov	r5, r0
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #178
	mov	r2, r4
	mov	r3, #2
	mov	r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #180
	mov	r2, r4
	mov	r3, #2
	add	r7, r0, r7
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #182
	mov	r2, r4
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r6, #200
	add	r7, r7, r0
	bne	.L452
	mov	r3, #250
	ldrh	r2, [sp, #178]
	str	r3, [sp, #0]
	mov	r1, #32
	mov	r3, #150
	add	r0, sp, #144
	bl	GetDateStr
	ldrh	r2, [sp, #180]
	fldd	d7, .L453
	mov	r1, #256
	fmsr	s11, r2	@ int
	ldrh	r2, [sp, #182]
	fsitod	d6, s11
	fmsr	s11, r2	@ int
	ldr	r2, .L453+8
	fdivd	d6, d6, d7
	mov	r3, r0
	mov	r0, r8
	fstd	d6, [sp, #0]
	fsitod	d6, s11
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	snprintf
.L452:
	mov	r0, r7
	add	sp, sp, #184
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L454:
	.align	2
.L453:
	.word	0
	.word	1079574528
	.word	.LC153
.LFE184:
	.size	ALERTS_pull_ETTable_LimitedEntry_off_pile.constprop.42, .-ALERTS_pull_ETTable_LimitedEntry_off_pile.constprop.42
	.section	.text.ALERTS_pull_station_added_to_group_off_pile.constprop.48,"ax",%progbits
	.align	2
	.type	ALERTS_pull_station_added_to_group_off_pile.constprop.48, %function
ALERTS_pull_station_added_to_group_off_pile.constprop.48:
.LFB178:
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI99:
	sub	sp, sp, #96
.LCFI100:
	mov	r5, r3
	mov	r8, r1
	mov	r6, r2
	add	r1, sp, #95
	mov	r2, r3
	mov	r3, #1
	mov	r4, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #90
	mov	r2, r5
	mov	r3, #2
	mov	r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #8
	mov	r2, #64
	mov	r3, r5
	add	r7, r0, r7
	mov	r0, r4
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #92
	mov	r2, r5
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L456
	ldr	r3, .L460
	mov	r2, #256
	cmp	r4, r3
	ldreq	r1, .L460+4
	ldrne	r1, .L460+8
	mov	r0, r6
	bl	strlcpy
	ldrh	r1, [sp, #90]
	add	r2, sp, #72
	mov	r3, #16
	ldrb	r0, [sp, #95]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r4, r0
	ldrh	r0, [sp, #92]
	bl	GetChangeReasonStr
	add	r3, sp, #8
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L460+12
	mov	r3, r4
	str	r0, [sp, #4]
	mov	r0, r6
	bl	sp_strlcat
.L456:
	mov	r0, r7
	add	sp, sp, #96
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L461:
	.align	2
.L460:
	.word	alerts_struct_changes
	.word	.LANCHOR1
	.word	.LC54
	.word	.LC154
.LFE178:
	.size	ALERTS_pull_station_added_to_group_off_pile.constprop.48, .-ALERTS_pull_station_added_to_group_off_pile.constprop.48
	.section	.text.ALERTS_pull_station_removed_from_group_off_pile.constprop.49,"ax",%progbits
	.align	2
	.type	ALERTS_pull_station_removed_from_group_off_pile.constprop.49, %function
ALERTS_pull_station_removed_from_group_off_pile.constprop.49:
.LFB177:
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI101:
	sub	sp, sp, #96
.LCFI102:
	mov	r5, r3
	mov	r8, r1
	mov	r6, r2
	add	r1, sp, #95
	mov	r2, r3
	mov	r3, #1
	mov	r4, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #90
	mov	r2, r5
	mov	r3, #2
	mov	r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #8
	mov	r2, #64
	mov	r3, r5
	add	r7, r0, r7
	mov	r0, r4
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #92
	mov	r2, r5
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L463
	ldr	r3, .L467
	mov	r2, #256
	cmp	r4, r3
	ldreq	r1, .L467+4
	ldrne	r1, .L467+8
	mov	r0, r6
	bl	strlcpy
	ldrh	r1, [sp, #90]
	add	r2, sp, #72
	mov	r3, #16
	ldrb	r0, [sp, #95]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r4, r0
	ldrh	r0, [sp, #92]
	bl	GetChangeReasonStr
	add	r3, sp, #8
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L467+12
	mov	r3, r4
	str	r0, [sp, #4]
	mov	r0, r6
	bl	sp_strlcat
.L463:
	mov	r0, r7
	add	sp, sp, #96
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L468:
	.align	2
.L467:
	.word	alerts_struct_changes
	.word	.LANCHOR1
	.word	.LC54
	.word	.LC155
.LFE177:
	.size	ALERTS_pull_station_removed_from_group_off_pile.constprop.49, .-ALERTS_pull_station_removed_from_group_off_pile.constprop.49
	.section	.text.ALERTS_pull_station_copied_off_pile.constprop.50,"ax",%progbits
	.align	2
	.type	ALERTS_pull_station_copied_off_pile.constprop.50, %function
ALERTS_pull_station_copied_off_pile.constprop.50:
.LFB176:
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI103:
	sub	sp, sp, #96
.LCFI104:
	mov	r5, r3
	mov	r8, r1
	mov	r6, r2
	add	r1, sp, #95
	mov	r2, r3
	mov	r3, #1
	mov	r4, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #90
	mov	r2, r5
	mov	r3, #2
	mov	r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #8
	mov	r2, #64
	mov	r3, r5
	add	r7, r0, r7
	mov	r0, r4
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #92
	mov	r2, r5
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L470
	ldr	r3, .L474
	mov	r2, #256
	cmp	r4, r3
	ldreq	r1, .L474+4
	ldrne	r1, .L474+8
	mov	r0, r6
	bl	strlcpy
	ldrh	r1, [sp, #90]
	add	r2, sp, #72
	mov	r3, #16
	ldrb	r0, [sp, #95]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r4, r0
	ldrh	r0, [sp, #92]
	bl	GetChangeReasonStr
	add	r3, sp, #8
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L474+12
	mov	r3, r4
	str	r0, [sp, #4]
	mov	r0, r6
	bl	sp_strlcat
.L470:
	mov	r0, r7
	add	sp, sp, #96
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L475:
	.align	2
.L474:
	.word	alerts_struct_changes
	.word	.LANCHOR1
	.word	.LC54
	.word	.LC156
.LFE176:
	.size	ALERTS_pull_station_copied_off_pile.constprop.50, .-ALERTS_pull_station_copied_off_pile.constprop.50
	.section	.text.ALERTS_pull_station_moved_from_one_group_to_another_off_pile.constprop.51,"ax",%progbits
	.align	2
	.type	ALERTS_pull_station_moved_from_one_group_to_another_off_pile.constprop.51, %function
ALERTS_pull_station_moved_from_one_group_to_another_off_pile.constprop.51:
.LFB175:
	@ args = 0, pretend = 0, frame = 152
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI105:
	sub	sp, sp, #164
.LCFI106:
	mov	r5, r3
	mov	r8, r1
	mov	r6, r2
	add	r1, sp, #163
	mov	r2, r3
	mov	r3, #1
	mov	r4, r0
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #158
	mov	r2, r5
	mov	r3, #2
	mov	r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #12
	mov	r2, #64
	mov	r3, r5
	add	r7, r0, r7
	mov	r0, r4
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #76
	mov	r2, #64
	mov	r3, r5
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #160
	mov	r2, r5
	mov	r3, #2
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L477
	ldr	r3, .L481
	mov	r2, #256
	cmp	r4, r3
	ldreq	r1, .L481+4
	ldrne	r1, .L481+8
	mov	r0, r6
	bl	strlcpy
	ldrh	r1, [sp, #158]
	add	r2, sp, #140
	mov	r3, #16
	ldrb	r0, [sp, #163]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r4, r0
	ldrh	r0, [sp, #160]
	bl	GetChangeReasonStr
	add	r3, sp, #12
	str	r3, [sp, #0]
	add	r3, sp, #76
	str	r3, [sp, #4]
	mov	r1, #256
	ldr	r2, .L481+12
	mov	r3, r4
	str	r0, [sp, #8]
	mov	r0, r6
	bl	sp_strlcat
.L477:
	mov	r0, r7
	add	sp, sp, #164
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L482:
	.align	2
.L481:
	.word	alerts_struct_changes
	.word	.LANCHOR1
	.word	.LC54
	.word	.LC157
.LFE175:
	.size	ALERTS_pull_station_moved_from_one_group_to_another_off_pile.constprop.51, .-ALERTS_pull_station_moved_from_one_group_to_another_off_pile.constprop.51
	.section	.text.ALERTS_pull_poc_assigned_to_mainline_off_pile.constprop.63,"ax",%progbits
	.align	2
	.type	ALERTS_pull_poc_assigned_to_mainline_off_pile.constprop.63, %function
ALERTS_pull_poc_assigned_to_mainline_off_pile.constprop.63:
.LFB163:
	@ args = 0, pretend = 0, frame = 100
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI107:
	sub	sp, sp, #108
.LCFI108:
	mov	r8, r1
	mov	r5, r2
	add	r1, sp, #8
	mov	r2, #48
	mov	r4, r0
	mov	r6, r3
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #56
	mov	r2, #48
	mov	r3, r6
	mov	r7, r0
	mov	r0, r4
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #104
	mov	r2, r6
	mov	r3, #2
	add	r7, r0, r7
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #107
	mov	r2, r6
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L484
	ldr	r3, .L488
	mov	r2, #256
	cmp	r4, r3
	ldreq	r1, .L488+4
	ldrne	r1, .L488+8
	mov	r0, r5
	bl	strlcpy
	ldrh	r0, [sp, #104]
	bl	GetChangeReasonStr
	add	r3, sp, #56
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L488+12
	add	r3, sp, #8
	str	r0, [sp, #4]
	mov	r0, r5
	bl	sp_strlcat
.L484:
	mov	r0, r7
	add	sp, sp, #108
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L489:
	.align	2
.L488:
	.word	alerts_struct_changes
	.word	.LANCHOR1
	.word	.LC54
	.word	.LC158
.LFE163:
	.size	ALERTS_pull_poc_assigned_to_mainline_off_pile.constprop.63, .-ALERTS_pull_poc_assigned_to_mainline_off_pile.constprop.63
	.section	.text.ALERTS_pull_station_group_assigned_to_mainline_off_pile.constprop.64,"ax",%progbits
	.align	2
	.type	ALERTS_pull_station_group_assigned_to_mainline_off_pile.constprop.64, %function
ALERTS_pull_station_group_assigned_to_mainline_off_pile.constprop.64:
.LFB162:
	@ args = 0, pretend = 0, frame = 100
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI109:
	sub	sp, sp, #108
.LCFI110:
	mov	r8, r1
	mov	r5, r2
	add	r1, sp, #8
	mov	r2, #48
	mov	r4, r0
	mov	r6, r3
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #56
	mov	r2, #48
	mov	r3, r6
	mov	r7, r0
	mov	r0, r4
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #104
	mov	r2, r6
	mov	r3, #2
	add	r7, r0, r7
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #107
	mov	r2, r6
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L491
	ldr	r3, .L495
	mov	r2, #256
	cmp	r4, r3
	ldreq	r1, .L495+4
	ldrne	r1, .L495+8
	mov	r0, r5
	bl	strlcpy
	ldrh	r0, [sp, #104]
	bl	GetChangeReasonStr
	add	r3, sp, #56
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L495+12
	add	r3, sp, #8
	str	r0, [sp, #4]
	mov	r0, r5
	bl	sp_strlcat
.L491:
	mov	r0, r7
	add	sp, sp, #108
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L496:
	.align	2
.L495:
	.word	alerts_struct_changes
	.word	.LANCHOR1
	.word	.LC54
	.word	.LC158
.LFE162:
	.size	ALERTS_pull_station_group_assigned_to_mainline_off_pile.constprop.64, .-ALERTS_pull_station_group_assigned_to_mainline_off_pile.constprop.64
	.section	.text.ALERTS_pull_station_group_assigned_to_a_moisture_sensor_off_pile.constprop.65,"ax",%progbits
	.align	2
	.type	ALERTS_pull_station_group_assigned_to_a_moisture_sensor_off_pile.constprop.65, %function
ALERTS_pull_station_group_assigned_to_a_moisture_sensor_off_pile.constprop.65:
.LFB161:
	@ args = 0, pretend = 0, frame = 100
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI111:
	sub	sp, sp, #108
.LCFI112:
	mov	r8, r1
	mov	r5, r2
	add	r1, sp, #8
	mov	r2, #48
	mov	r4, r0
	mov	r6, r3
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #56
	mov	r2, #48
	mov	r3, r6
	mov	r7, r0
	mov	r0, r4
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #104
	mov	r2, r6
	mov	r3, #2
	add	r7, r0, r7
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #107
	mov	r2, r6
	mov	r3, #1
	add	r7, r7, r0
	mov	r0, r4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r7, r7, r0
	bne	.L498
	ldr	r3, .L502
	mov	r2, #256
	cmp	r4, r3
	ldreq	r1, .L502+4
	ldrne	r1, .L502+8
	mov	r0, r5
	bl	strlcpy
	ldrh	r0, [sp, #104]
	bl	GetChangeReasonStr
	add	r3, sp, #56
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L502+12
	add	r3, sp, #8
	str	r0, [sp, #4]
	mov	r0, r5
	bl	sp_strlcat
.L498:
	mov	r0, r7
	add	sp, sp, #108
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L503:
	.align	2
.L502:
	.word	alerts_struct_changes
	.word	.LANCHOR1
	.word	.LC54
	.word	.LC159
.LFE161:
	.size	ALERTS_pull_station_group_assigned_to_a_moisture_sensor_off_pile.constprop.65, .-ALERTS_pull_station_group_assigned_to_a_moisture_sensor_off_pile.constprop.65
	.section	.text.ALERTS_parse_comm_command_string,"ax",%progbits
	.align	2
	.global	ALERTS_parse_comm_command_string
	.type	ALERTS_parse_comm_command_string, %function
ALERTS_parse_comm_command_string:
.LFB39:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	cmp	r3, #117
	mov	r0, r1
	mov	ip, r2
	beq	.L559
	bhi	.L614
	cmp	r3, #70
	beq	.L533
	bhi	.L615
	cmp	r3, #39
	beq	.L519
	bhi	.L616
	cmp	r3, #9
	beq	.L512
	bhi	.L617
	cmp	r3, #3
	beq	.L508
	bhi	.L618
	cmp	r3, #0
	beq	.L506
	cmp	r3, #2
	bne	.L505
	b	.L648
.L618:
	cmp	r3, #6
	beq	.L510
	cmp	r3, #8
	beq	.L511
	cmp	r3, #5
	bne	.L505
	b	.L649
.L617:
	cmp	r3, #14
	beq	.L515
	bhi	.L619
	cmp	r3, #11
	beq	.L513
	cmp	r3, #12
	bne	.L505
	b	.L650
.L619:
	cmp	r3, #36
	beq	.L517
	cmp	r3, #37
	beq	.L518
	cmp	r3, #34
	bne	.L505
	b	.L651
.L616:
	cmp	r3, #53
	beq	.L526
	bhi	.L620
	cmp	r3, #43
	beq	.L522
	bhi	.L621
	cmp	r3, #40
	beq	.L520
	cmp	r3, #42
	bne	.L505
	b	.L652
.L621:
	cmp	r3, #46
	beq	.L524
	cmp	r3, #48
	beq	.L525
	cmp	r3, #44
	bne	.L505
	b	.L653
.L620:
	cmp	r3, #59
	beq	.L529
	bhi	.L622
	cmp	r3, #56
	beq	.L527
	cmp	r3, #57
	bne	.L505
	b	.L654
.L622:
	cmp	r3, #67
	beq	.L531
	cmp	r3, #68
	beq	.L532
	cmp	r3, #64
	bne	.L505
	b	.L655
.L615:
	cmp	r3, #100
	beq	.L544
	bhi	.L623
	cmp	r3, #81
	beq	.L540
	bhi	.L624
	cmp	r3, #74
	beq	.L536
	bhi	.L625
	cmp	r3, #71
	beq	.L534
	cmp	r3, #73
	bne	.L505
	b	.L656
.L625:
	cmp	r3, #78
	beq	.L538
	cmp	r3, #79
	beq	.L539
	cmp	r3, #75
	bne	.L505
	b	.L657
.L624:
	cmp	r3, #85
	beq	.L540
	bhi	.L626
	cmp	r3, #82
	beq	.L541
	cmp	r3, #83
	bne	.L505
	b	.L539
.L626:
	cmp	r3, #90
	beq	.L542
	cmp	r3, #91
	beq	.L543
	cmp	r3, #86
	bne	.L505
	b	.L541
.L623:
	cmp	r3, #108
	beq	.L551
	bhi	.L627
	cmp	r3, #103
	beq	.L547
	bhi	.L628
	cmp	r3, #101
	beq	.L545
	cmp	r3, #102
	bne	.L505
	b	.L658
.L628:
	cmp	r3, #106
	beq	.L549
	bhi	.L550
	cmp	r3, #104
	bne	.L505
	b	.L659
.L627:
	cmp	r3, #112
	beq	.L555
	bhi	.L629
	cmp	r3, #110
	beq	.L553
	bhi	.L554
	b	.L660
.L629:
	cmp	r3, #114
	beq	.L557
	bcc	.L556
	cmp	r3, #115
	bne	.L505
	b	.L661
.L614:
	cmp	r3, #172
	beq	.L588
	bhi	.L630
	cmp	r3, #143
	beq	.L573
	bhi	.L631
	cmp	r3, #129
	beq	.L566
	bhi	.L632
	cmp	r3, #121
	beq	.L562
	bhi	.L633
	cmp	r3, #118
	beq	.L560
	cmp	r3, #120
	bne	.L505
	b	.L662
.L633:
	cmp	r3, #125
	beq	.L564
	cmp	r3, #127
	beq	.L565
	cmp	r3, #123
	bne	.L505
	b	.L663
.L632:
	cmp	r3, #135
	beq	.L569
	bhi	.L634
	cmp	r3, #131
	beq	.L567
	cmp	r3, #133
	bne	.L505
	b	.L664
.L634:
	cmp	r3, #139
	beq	.L571
	cmp	r3, #141
	beq	.L572
	cmp	r3, #137
	bne	.L505
	b	.L665
.L631:
	cmp	r3, #155
	beq	.L580
	bhi	.L635
	cmp	r3, #149
	beq	.L576
	bhi	.L636
	cmp	r3, #145
	beq	.L574
	cmp	r3, #147
	b	.L647
.L636:
	cmp	r3, #152
	beq	.L578
	cmp	r3, #153
	beq	.L579
	cmp	r3, #150
	bne	.L505
	b	.L666
.L635:
	cmp	r3, #165
	beq	.L584
	bhi	.L637
	cmp	r3, #162
	beq	.L582
	cmp	r3, #163
	beq	.L583
	cmp	r3, #157
	bne	.L505
	b	.L667
.L637:
	cmp	r3, #168
	beq	.L586
	cmp	r3, #171
	beq	.L587
	cmp	r3, #166
	bne	.L505
	b	.L668
.L630:
	ldr	r1, .L676
	cmp	r3, r1
	beq	.L600
	bhi	.L638
	cmp	r3, #182
	beq	.L595
	bhi	.L639
	cmp	r3, #176
	beq	.L591
	bhi	.L640
	cmp	r3, #174
	beq	.L589
	cmp	r3, #175
	bne	.L505
	b	.L669
.L640:
	cmp	r3, #180
	beq	.L593
	bhi	.L594
	cmp	r3, #177
	bne	.L505
	b	.L670
.L639:
	cmp	r3, #202
	beq	.L576
	bhi	.L641
	cmp	r3, #183
	beq	.L596
	cmp	r3, #200
.L647:
	bne	.L505
	b	.L671
.L641:
	cmp	r3, #245
	beq	.L598
	cmp	r3, #1000
	beq	.L599
	cmp	r3, #216
	bne	.L505
	b	.L672
.L638:
	ldr	r1, .L676+4
	cmp	r3, r1
	beq	.L607
	bhi	.L642
	cmp	r3, #1012
	beq	.L603
	bhi	.L643
	ldr	r2, .L676+8
	cmp	r3, r2
	beq	.L601
	add	r2, r2, #3
	cmp	r3, r2
	bne	.L505
	b	.L673
.L643:
	ldr	r2, .L676+12
	cmp	r3, r2
	beq	.L605
	add	r2, r2, #9
	cmp	r3, r2
	beq	.L606
	sub	r2, r2, #12
	cmp	r3, r2
	bne	.L505
	b	.L674
.L642:
	ldr	r1, .L676+16
	cmp	r3, r1
	beq	.L611
	bhi	.L644
	ldr	r2, .L676+20
	cmp	r3, r2
	beq	.L609
	add	r2, r2, #44
	cmp	r3, r2
	beq	.L610
	sub	r2, r2, #47
	cmp	r3, r2
	bne	.L505
	b	.L675
.L644:
	ldr	r2, .L676+24
	cmp	r3, r2
	beq	.L613
	cmp	r3, #1200
	beq	.L612
	sub	r2, r2, #3
	cmp	r3, r2
	bne	.L505
	b	.L612
.L506:
	ldr	r1, .L676+28
	b	strlcpy
.L648:
	ldr	r1, .L676+32
	b	strlcpy
.L599:
	ldr	r1, .L676+36
	b	strlcpy
.L508:
	ldr	r1, .L676+40
	b	strlcpy
.L649:
	ldr	r1, .L676+44
	b	strlcpy
.L600:
	ldr	r1, .L676+48
	b	strlcpy
.L510:
	ldr	r1, .L676+52
	b	strlcpy
.L511:
	ldr	r1, .L676+56
	b	strlcpy
.L601:
	ldr	r1, .L676+60
	mov	r2, ip
	b	strlcpy
.L520:
	ldr	r1, .L676+64
	b	strlcpy
.L652:
	ldr	r1, .L676+68
	b	strlcpy
.L522:
	ldr	r1, .L676+72
	b	strlcpy
.L653:
	ldr	r1, .L676+76
	b	strlcpy
.L593:
	ldr	r1, .L676+80
	b	strlcpy
.L596:
	ldr	r1, .L676+84
	b	strlcpy
.L595:
	ldr	r1, .L676+88
	b	strlcpy
.L594:
	ldr	r1, .L676+92
	b	strlcpy
.L512:
	ldr	r1, .L676+96
	b	strlcpy
.L513:
	ldr	r1, .L676+100
	b	strlcpy
.L673:
	ldr	r1, .L676+104
	mov	r2, ip
	b	strlcpy
.L650:
	ldr	r1, .L676+108
	b	strlcpy
.L515:
	ldr	r1, .L676+112
	b	strlcpy
.L603:
	ldr	r1, .L676+116
	b	strlcpy
.L651:
	ldr	r1, .L676+120
	b	strlcpy
.L517:
	ldr	r1, .L676+124
	b	strlcpy
.L674:
	ldr	r1, .L676+128
	mov	r2, ip
	b	strlcpy
.L666:
	ldr	r1, .L676+132
	b	strlcpy
.L578:
	ldr	r1, .L676+136
	b	strlcpy
.L613:
	ldr	r1, .L676+140
	mov	r2, ip
	b	strlcpy
.L518:
	ldr	r1, .L676+144
	b	strlcpy
.L519:
	ldr	r1, .L676+148
	b	strlcpy
.L605:
	ldr	r1, .L676+152
	mov	r2, ip
	b	strlcpy
.L661:
	ldr	r1, .L676+156
	b	strlcpy
.L559:
	ldr	r1, .L676+160
	b	strlcpy
.L610:
	ldr	r1, .L676+164
	mov	r2, ip
	b	strlcpy
.L671:
	ldr	r1, .L676+168
	mov	r2, ip
	b	strlcpy
.L576:
	ldr	r1, .L676+172
	mov	r2, ip
	b	strlcpy
.L612:
	ldr	r1, .L676+176
	mov	r2, ip
	b	strlcpy
.L524:
	ldr	r1, .L676+180
	b	strlcpy
.L525:
	ldr	r1, .L676+184
	b	strlcpy
.L606:
	ldr	r1, .L676+188
	mov	r2, ip
	b	strlcpy
.L526:
	ldr	r1, .L676+192
	b	strlcpy
.L527:
	ldr	r1, .L676+196
	b	strlcpy
.L654:
	ldr	r1, .L676+200
	b	strlcpy
.L529:
	ldr	r1, .L676+204
	b	strlcpy
.L607:
	ldr	r1, .L676+208
	b	strlcpy
.L655:
	ldr	r1, .L676+212
	b	strlcpy
.L531:
	ldr	r1, .L676+216
	b	strlcpy
.L550:
	ldr	r1, .L676+220
	b	strlcpy
.L551:
	ldr	r1, .L676+224
	b	strlcpy
.L659:
	ldr	r1, .L676+228
	b	strlcpy
.L549:
	ldr	r1, .L676+232
	b	strlcpy
.L544:
	ldr	r1, .L676+236
	b	strlcpy
.L545:
	ldr	r1, .L676+240
	b	strlcpy
.L658:
	ldr	r1, .L676+244
	b	strlcpy
.L547:
	ldr	r1, .L676+248
	b	strlcpy
.L539:
	ldr	r1, .L676+252
	mov	r2, ip
	b	strlcpy
.L540:
	ldr	r1, .L676+256
	mov	r2, ip
	b	strlcpy
.L541:
	ldr	r1, .L676+260
	mov	r2, ip
	b	strlcpy
.L532:
	ldr	r1, .L676+264
	b	strlcpy
.L533:
	ldr	r1, .L676+268
	b	strlcpy
.L675:
	ldr	r1, .L676+272
	mov	r2, ip
	b	strlcpy
.L542:
	ldr	r1, .L676+276
	b	strlcpy
.L534:
	ldr	r1, .L676+280
	b	strlcpy
.L656:
	ldr	r1, .L676+284
	b	strlcpy
.L536:
	ldr	r1, .L676+288
	b	strlcpy
.L609:
	ldr	r1, .L676+292
	mov	r2, ip
	b	strlcpy
.L657:
	ldr	r1, .L676+296
	b	strlcpy
.L538:
	ldr	r1, .L676+300
	b	strlcpy
.L543:
	ldr	r1, .L676+304
	b	strlcpy
.L660:
	ldr	r1, .L676+308
	b	strlcpy
.L677:
	.align	2
.L676:
	.word	1003
	.word	1057
	.word	1006
	.word	1037
	.word	1118
	.word	1071
	.word	1150
	.word	.LC160
	.word	.LC161
	.word	.LC162
	.word	.LC163
	.word	.LC164
	.word	.LC165
	.word	.LC166
	.word	.LC167
	.word	.LC168
	.word	.LC169
	.word	.LC170
	.word	.LC171
	.word	.LC172
	.word	.LC173
	.word	.LC174
	.word	.LC175
	.word	.LC176
	.word	.LC177
	.word	.LC178
	.word	.LC179
	.word	.LC180
	.word	.LC181
	.word	.LC182
	.word	.LC183
	.word	.LC184
	.word	.LC185
	.word	.LC186
	.word	.LC187
	.word	.LC188
	.word	.LC189
	.word	.LC190
	.word	.LC191
	.word	.LC192
	.word	.LC193
	.word	.LC194
	.word	.LC195
	.word	.LC196
	.word	.LC197
	.word	.LC198
	.word	.LC199
	.word	.LC200
	.word	.LC201
	.word	.LC202
	.word	.LC203
	.word	.LC204
	.word	.LC205
	.word	.LC206
	.word	.LC207
	.word	.LC208
	.word	.LC209
	.word	.LC210
	.word	.LC211
	.word	.LC212
	.word	.LC213
	.word	.LC214
	.word	.LC215
	.word	.LC216
	.word	.LC217
	.word	.LC218
	.word	.LC219
	.word	.LC220
	.word	.LC221
	.word	.LC222
	.word	.LC223
	.word	.LC224
	.word	.LC225
	.word	.LC226
	.word	.LC227
	.word	.LC228
	.word	.LC229
	.word	.LC230
	.word	.LC231
	.word	.LC232
	.word	.LC233
	.word	.LC234
	.word	.LC235
	.word	.LC236
	.word	.LC237
	.word	.LC238
	.word	.LC239
	.word	.LC240
	.word	.LC241
	.word	.LC242
	.word	.LC243
	.word	.LC244
	.word	.LC245
	.word	.LC246
	.word	.LC247
	.word	.LC248
	.word	.LC249
	.word	.LC250
	.word	.LC251
	.word	.LC252
	.word	.LC253
	.word	.LC254
	.word	.LC255
	.word	.LC256
	.word	.LC257
	.word	.LC258
	.word	.LC259
	.word	.LC260
	.word	.LC261
	.word	.LC262
	.word	.LC263
	.word	.LC264
	.word	.LC265
	.word	.LC266
.L553:
	ldr	r1, .L676+312
	b	strlcpy
.L554:
	ldr	r1, .L676+316
	b	strlcpy
.L555:
	ldr	r1, .L676+320
	b	strlcpy
.L556:
	ldr	r1, .L676+324
	b	strlcpy
.L557:
	ldr	r1, .L676+328
	b	strlcpy
.L560:
	ldr	r1, .L676+332
	b	strlcpy
.L662:
	ldr	r1, .L676+336
	b	strlcpy
.L611:
	ldr	r1, .L676+340
	b	strlcpy
.L562:
	ldr	r1, .L676+344
	b	strlcpy
.L663:
	ldr	r1, .L676+348
	b	strlcpy
.L564:
	ldr	r1, .L676+352
	b	strlcpy
.L565:
	ldr	r1, .L676+356
	b	strlcpy
.L566:
	ldr	r1, .L676+360
	b	strlcpy
.L574:
	ldr	r1, .L676+364
	b	strlcpy
.L567:
	ldr	r1, .L676+368
	b	strlcpy
.L664:
	ldr	r1, .L676+372
	b	strlcpy
.L569:
	ldr	r1, .L676+376
	b	strlcpy
.L665:
	ldr	r1, .L676+380
	b	strlcpy
.L571:
	ldr	r1, .L676+384
	b	strlcpy
.L572:
	ldr	r1, .L676+388
	b	strlcpy
.L573:
	ldr	r1, .L676+392
	b	strlcpy
.L579:
	ldr	r1, .L676+396
	b	strlcpy
.L580:
	ldr	r1, .L676+400
	b	strlcpy
.L667:
	ldr	r1, .L676+404
	b	strlcpy
.L582:
	ldr	r1, .L676+408
	b	strlcpy
.L583:
	ldr	r1, .L676+412
	b	strlcpy
.L584:
	ldr	r1, .L676+416
	b	strlcpy
.L668:
	ldr	r1, .L676+420
	b	strlcpy
.L586:
	ldr	r1, .L676+424
	b	strlcpy
.L589:
	ldr	r1, .L676+428
	b	strlcpy
.L669:
	ldr	r1, .L676+432
	b	strlcpy
.L587:
	ldr	r1, .L676+436
	b	strlcpy
.L588:
	ldr	r1, .L676+436
	b	strlcpy
.L591:
	ldr	r1, .L676+440
	b	strlcpy
.L670:
	ldr	r1, .L676+440
	b	strlcpy
.L672:
	ldr	r1, .L676+444
	b	strlcpy
.L598:
	ldr	r1, .L676+448
	b	strlcpy
.L505:
	ldr	r2, .L676+452
	mov	r1, ip
	b	snprintf
.LFE39:
	.size	ALERTS_parse_comm_command_string, .-ALERTS_parse_comm_command_string
	.section	.text.nm_ALERT_PARSING_parse_alert_and_return_length,"ax",%progbits
	.align	2
	.global	nm_ALERT_PARSING_parse_alert_and_return_length
	.type	nm_ALERT_PARSING_parse_alert_and_return_length, %function
nm_ALERT_PARSING_parse_alert_and_return_length:
.LFB159:
	@ args = 8, pretend = 0, frame = 468
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI113:
	sub	sp, sp, #500
.LCFI114:
	mov	r5, r0
	mov	r6, r1
	mov	r8, r2
	add	r0, sp, #32
	mov	r1, #0
	mov	r2, #256
	mov	r9, r3
	bl	memset
	cmp	r5, #247
	beq	.L784
	bhi	.L895
	cmp	r5, #109
	beq	.L732
	bhi	.L896
	cmp	r5, #38
	beq	.L706
	bhi	.L897
	cmp	r5, #16
	beq	.L692
	bhi	.L898
	cmp	r5, #7
	beq	.L686
	bhi	.L899
	cmp	r5, #2
	beq	.L682
	bhi	.L900
	cmp	r5, #0
	beq	.L680
	cmp	r5, #1
	bne	.L679
	b	.L1350
.L900:
	cmp	r5, #4
	beq	.L684
	bcc	.L683
	cmp	r5, #5
	bne	.L679
	b	.L1351
.L899:
	cmp	r5, #12
	bhi	.L901
	cmp	r5, #11
	bcs	.L689
	cmp	r5, #8
	beq	.L687
	cmp	r5, #9
	bne	.L679
	b	.L1352
.L901:
	cmp	r5, #13
	beq	.L690
	cmp	r5, #15
	bne	.L679
	b	.L1353
.L898:
	cmp	r5, #31
	beq	.L699
	bhi	.L902
	cmp	r5, #20
	beq	.L695
	bhi	.L903
	cmp	r5, #17
	beq	.L693
	cmp	r5, #18
	bne	.L679
	b	.L1354
.L903:
	cmp	r5, #25
	beq	.L697
	cmp	r5, #30
	beq	.L698
	cmp	r5, #21
	bne	.L679
	b	.L1355
.L902:
	cmp	r5, #34
	beq	.L702
	bhi	.L904
	cmp	r5, #32
	beq	.L700
	cmp	r5, #33
	bne	.L679
	b	.L1356
.L904:
	cmp	r5, #36
	beq	.L704
	bhi	.L705
	b	.L1357
.L897:
	cmp	r5, #57
	beq	.L719
	bhi	.L905
	cmp	r5, #50
	beq	.L712
	bhi	.L906
	cmp	r5, #41
	bhi	.L907
	cmp	r5, #40
	bcs	.L708
	b	.L1358
.L907:
	cmp	r5, #46
	beq	.L710
	cmp	r5, #49
	beq	.L711
	cmp	r5, #45
	bne	.L679
	b	.L1359
.L906:
	cmp	r5, #53
	beq	.L715
	bhi	.L908
	cmp	r5, #51
	beq	.L713
	cmp	r5, #52
	bne	.L679
	b	.L1360
.L908:
	cmp	r5, #55
	beq	.L717
	bhi	.L718
	b	.L1361
.L905:
	cmp	r5, #102
	bhi	.L909
	cmp	r5, #101
	bcs	.L726
	cmp	r5, #60
	beq	.L722
	bhi	.L910
	cmp	r5, #58
	beq	.L720
	cmp	r5, #59
	bne	.L679
	b	.L1362
.L910:
	cmp	r5, #62
	beq	.L724
	bcc	.L723
	cmp	r5, #100
	bne	.L679
	b	.L1363
.L909:
	cmp	r5, #106
	beq	.L729
	bhi	.L911
	cmp	r5, #103
	beq	.L727
	cmp	r5, #105
	bne	.L679
	b	.L1364
.L911:
	cmp	r5, #107
	beq	.L730
	cmp	r5, #108
	bne	.L679
	b	.L1365
.L896:
	cmp	r5, #176
	beq	.L760
	bhi	.L912
	cmp	r5, #157
	beq	.L746
	bhi	.L913
	cmp	r5, #116
	beq	.L739
	bhi	.L914
	cmp	r5, #112
	beq	.L735
	bhi	.L915
	cmp	r5, #110
	beq	.L733
	cmp	r5, #111
	bne	.L679
	b	.L1366
.L915:
	cmp	r5, #114
	beq	.L737
	bhi	.L738
	b	.L1367
.L914:
	cmp	r5, #152
	beq	.L742
	bhi	.L916
	cmp	r5, #150
	beq	.L740
	cmp	r5, #151
	bne	.L679
	b	.L1368
.L916:
	cmp	r5, #155
	beq	.L744
	bhi	.L745
	cmp	r5, #153
	bne	.L679
	b	.L1369
.L913:
	cmp	r5, #166
	beq	.L753
	bhi	.L917
	cmp	r5, #161
	beq	.L749
	bhi	.L918
	cmp	r5, #158
	beq	.L747
	cmp	r5, #160
	bne	.L679
	b	.L1370
.L918:
	cmp	r5, #163
	beq	.L751
	bcc	.L750
	cmp	r5, #165
	bne	.L679
	b	.L1371
.L917:
	cmp	r5, #170
	beq	.L756
	bhi	.L919
	cmp	r5, #167
	beq	.L754
	cmp	r5, #168
	bne	.L679
	b	.L1372
.L919:
	cmp	r5, #172
	beq	.L758
	bcc	.L757
	cmp	r5, #175
	bne	.L679
	b	.L1373
.L912:
	cmp	r5, #211
	beq	.L773
	bhi	.L920
	cmp	r5, #187
	bhi	.L921
	cmp	r5, #185
	bcs	.L767
	cmp	r5, #180
	beq	.L763
	bhi	.L922
	cmp	r5, #177
	beq	.L761
	cmp	r5, #178
	bne	.L679
	b	.L1374
.L922:
	cmp	r5, #182
	beq	.L765
	bcc	.L764
	cmp	r5, #183
	bne	.L679
	b	.L1375
.L921:
	cmp	r5, #201
	beq	.L770
	bhi	.L923
	cmp	r5, #188
	beq	.L768
	cmp	r5, #189
	bne	.L679
	b	.L1376
.L923:
	cmp	r5, #204
	beq	.L771
	cmp	r5, #210
	bne	.L679
	b	.L1377
.L920:
	cmp	r5, #223
	beq	.L779
	bhi	.L924
	cmp	r5, #215
	beq	.L776
	bhi	.L925
	cmp	r5, #212
	beq	.L774
	cmp	r5, #214
	bne	.L679
	b	.L1378
.L925:
	cmp	r5, #217
	bls	.L777
	cmp	r5, #222
	bne	.L679
	b	.L1379
.L924:
	cmp	r5, #243
	bhi	.L926
	cmp	r5, #242
	bcs	.L781
	cmp	r5, #224
	beq	.L780
	sub	r3, r5, #232
	cmp	r3, #1
	bhi	.L679
	b	.L781
.L926:
	cmp	r5, #245
	beq	.L782
	cmp	r5, #246
	bne	.L679
	b	.L1380
.L895:
	ldr	r3, .L1475
	cmp	r5, r3
	beq	.L839
	bhi	.L927
	cmp	r5, #332
	beq	.L811
	bhi	.L928
	sub	r3, r3, #152
	cmp	r5, r3
	beq	.L797
	bhi	.L929
	cmp	r5, #254
	beq	.L791
	bhi	.L930
	cmp	r5, #250
	beq	.L787
	bhi	.L931
	cmp	r5, #248
	beq	.L785
	cmp	r5, #249
	bne	.L679
	b	.L1381
.L931:
	cmp	r5, #252
	beq	.L789
	bhi	.L790
	b	.L1382
.L930:
	ldr	r3, .L1475+4
	cmp	r5, r3
	bhi	.L932
	cmp	r5, #256
	bcs	.L793
	b	.L1383
.L932:
	ldr	r3, .L1475+8
	cmp	r5, r3
	beq	.L795
	add	r3, r3, #9
	cmp	r5, r3
	beq	.L796
	cmp	r5, #300
	bne	.L679
	b	.L1384
.L929:
	ldr	r3, .L1475+12
	cmp	r5, r3
	beq	.L804
	bhi	.L933
	sub	r3, r3, #4
	cmp	r5, r3
	beq	.L800
	bhi	.L934
	cmp	r5, #312
	beq	.L798
	sub	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1385
.L934:
	cmp	r5, #316
	beq	.L802
	bhi	.L803
	b	.L1386
.L933:
	ldr	r3, .L1475+16
	cmp	r5, r3
	beq	.L807
	bhi	.L935
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L805
	cmp	r5, #320
	bne	.L679
	b	.L1387
.L935:
	ldr	r3, .L1475+20
	cmp	r5, r3
	beq	.L809
	bhi	.L810
	sub	r3, r3, #8
	cmp	r5, r3
	bne	.L679
	b	.L1388
.L928:
	ldr	r3, .L1475+24
	cmp	r5, r3
	beq	.L825
	bhi	.L936
	sub	r3, r3, #8
	cmp	r5, r3
	beq	.L818
	bhi	.L937
	sub	r3, r3, #16
	cmp	r5, r3
	beq	.L814
	bhi	.L938
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L812
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1389
.L938:
	ldr	r3, .L1475+28
	cmp	r5, r3
	beq	.L816
	bhi	.L817
	cmp	r5, #336
	bne	.L679
	b	.L1390
.L937:
	ldr	r3, .L1475+32
	cmp	r5, r3
	beq	.L821
	bhi	.L939
	cmp	r5, #352
	beq	.L819
	sub	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1391
.L939:
	ldr	r3, .L1475+36
	cmp	r5, r3
	beq	.L823
	bhi	.L824
	b	.L1392
.L936:
	ldr	r3, .L1475+40
	cmp	r5, r3
	beq	.L832
	bhi	.L940
	sub	r3, r3, #69
	cmp	r5, r3
	beq	.L828
	bhi	.L941
	cmp	r5, #360
	beq	.L826
	sub	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1393
.L941:
	ldr	r3, .L1475+44
	cmp	r5, r3
	beq	.L830
	add	r3, r3, #9
	cmp	r5, r3
	beq	.L831
	cmp	r5, #420
	bne	.L679
	b	.L1394
.L940:
	ldr	r3, .L1475+48
	cmp	r5, r3
	beq	.L835
	bhi	.L942
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L833
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1395
.L942:
	ldr	r3, .L1475+52
	cmp	r5, r3
	beq	.L837
	bhi	.L838
	cmp	r5, #436
	bne	.L679
	b	.L1396
.L927:
	cmp	r5, #636
	beq	.L866
	bhi	.L943
	ldr	r3, .L1475+56
	cmp	r5, r3
	bhi	.L944
	cmp	r5, #476
	bhi	.L852
	sub	r3, r3, #8
	cmp	r5, r3
	beq	.L845
	bhi	.L945
	sub	r3, r3, #4
	cmp	r5, r3
	beq	.L842
	bhi	.L946
	cmp	r5, #464
	beq	.L840
	sub	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1397
.L946:
	ldr	r3, .L1475+60
	cmp	r5, r3
	beq	.L843
	cmp	r5, #468
	bne	.L679
	b	.L1398
.L945:
	ldr	r3, .L1475+64
	cmp	r5, r3
	beq	.L848
	bhi	.L947
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L846
	cmp	r5, #472
	bne	.L679
	b	.L1399
.L947:
	ldr	r3, .L1475+68
	cmp	r5, r3
	beq	.L850
	bhi	.L851
	b	.L1400
.L944:
	cmp	r5, #520
	beq	.L859
	bhi	.L948
	ldr	r3, .L1475+72
	cmp	r5, r3
	beq	.L855
	bhi	.L949
	cmp	r5, #480
	beq	.L853
	sub	r3, r3, #3
	cmp	r5, r3
	bne	.L679
	b	.L1401
.L949:
	ldr	r3, .L1475+76
	cmp	r5, r3
	beq	.L857
	add	r3, r3, #1
	cmp	r5, r3
	beq	.L858
	sub	r3, r3, #29
	cmp	r5, r3
	bne	.L679
	b	.L1402
.L948:
	ldr	r3, .L1475+80
	cmp	r5, r3
	beq	.L862
	bhi	.L950
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L860
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1403
.L950:
	ldr	r3, .L1475+84
	cmp	r5, r3
	beq	.L864
	cmp	r5, #560
	beq	.L865
	cmp	r5, #552
	bne	.L679
	b	.L1404
.L943:
	ldr	r3, .L1475+88
	cmp	r5, r3
	beq	.L880
	bhi	.L951
	sub	r3, r3, #62
	cmp	r5, r3
	beq	.L873
	bhi	.L952
	sub	r3, r3, #4
	cmp	r5, r3
	beq	.L869
	bhi	.L953
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L867
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1405
.L953:
	ldr	r3, .L1475+92
	cmp	r5, r3
	beq	.L871
	bhi	.L872
	b	.L1406
.L952:
	cmp	r5, #700
	beq	.L876
	bhi	.L954
	cmp	r5, #644
	beq	.L874
	ldr	r3, .L1475+96
	cmp	r5, r3
	bne	.L679
	b	.L1407
.L954:
	ldr	r3, .L1475+100
	cmp	r5, r3
	beq	.L878
	bcc	.L877
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1408
.L951:
	cmp	r5, #712
	beq	.L887
	bhi	.L955
	cmp	r5, #708
	beq	.L883
	bhi	.L956
	ldr	r3, .L1475+104
	cmp	r5, r3
	beq	.L881
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L679
	b	.L1409
.L956:
	ldr	r3, .L1475+108
	cmp	r5, r3
	beq	.L885
	bhi	.L886
	b	.L1410
.L955:
	cmp	r5, #716
	beq	.L891
	bhi	.L957
	ldr	r3, .L1475+112
	cmp	r5, r3
	beq	.L889
	bhi	.L890
	b	.L1411
.L957:
	ldr	r3, .L1475+116
	cmp	r5, r3
	beq	.L893
	bcc	.L892
	cmp	r5, #720
	bne	.L679
	b	.L1412
.L680:
	cmp	r8, #200
	movne	r4, r5
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+120
	mov	r2, #256
	bl	strlcpy
	mov	r4, r5
	b	.L958
.L1352:
.LBB278:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L959
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+124
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L959:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+128
	bl	snprintf
	b	.L958
.L682:
.LBE278:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+132
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L683:
.LBB279:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L960
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+136
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L960:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+140
	bl	snprintf
	b	.L958
.L684:
.LBE279:
.LBB280:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L961
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+144
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L961:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+148
	bl	snprintf
	b	.L958
.L1350:
.LBE280:
.LBB281:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, r5
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L962
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+152
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L962:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+156
	bl	snprintf
	b	.L958
.L1351:
.LBE281:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_firmware_update_off_pile.constprop.2
	mov	r4, r0
	b	.L958
.L686:
.LBB282:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+160
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L687:
.LBE282:
.LBB283:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+164
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L689:
.LBE283:
	mov	r0, r6
	add	r1, sp, #32
	mov	r2, #256
	add	r3, sp, #536
	bl	ALERTS_pull_string_off_pile
	mov	r4, r0
	b	.L958
.L690:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+168
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1353:
.LBB284:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldr	r1, .L1475+172
	mov	r2, #256
	add	r0, sp, #32
	bl	strlcpy
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	cmp	r3, #1
	ldrls	r2, .L1475+176
	ldrhi	r1, .L1475+180
	ldrls	r1, [r2, r3, asl #2]
	mov	r2, #256
	bl	strlcat
	b	.L958
.L692:
.LBE284:
.LBB285:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldr	r1, .L1475+184
	mov	r2, #256
	add	r0, sp, #32
	bl	strlcpy
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	cmp	r3, #1
	ldrls	r2, .L1475+176
	ldrhi	r1, .L1475+180
	ldrls	r1, [r2, r3, asl #2]
	mov	r2, #256
	bl	strlcat
	b	.L958
.L693:
.LBE285:
.LBB286:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldr	r1, .L1475+188
	mov	r2, #256
	add	r0, sp, #32
	bl	strlcpy
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	cmp	r3, #1
	ldrls	r2, .L1475+176
	ldrhi	r1, .L1475+180
	ldrls	r1, [r2, r3, asl #2]
	mov	r2, #256
	bl	strlcat
	b	.L958
.L1354:
.LBE286:
.LBB287:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldr	r1, .L1475+192
	mov	r2, #256
	add	r0, sp, #32
	bl	strlcpy
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	cmp	r3, #1
	ldrls	r2, .L1475+176
	ldrhi	r1, .L1475+180
	ldrls	r1, [r2, r3, asl #2]
	mov	r2, #256
	bl	strlcat
	b	.L958
.L695:
.LBE287:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_battery_backed_var_init_off_pile.constprop.3
	mov	r4, r0
	b	.L958
.L1355:
.LBB288:
	mov	r0, r6
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+196
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L697:
.LBE288:
.LBB289:
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	add	r1, sp, #512
	ldrh	r3, [r1, #-20]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+200
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L698:
.LBE289:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+204
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L699:
.LBB290:
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #464]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+208
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L700:
.LBE290:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+212
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1356:
.LBB291:
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #464]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+216
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L702:
.LBE291:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+220
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1357:
.LBB292:
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #464]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+224
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L704:
.LBE292:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+228
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L705:
.LBB293:
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #464]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+232
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L706:
.LBE293:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+236
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1358:
.LBB294:
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #464]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+240
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L708:
.LBE294:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	str	r5, [sp, #0]
	bl	ALERTS_pull_item_not_on_list_off_pile.constprop.4
	mov	r4, r0
	b	.L958
.L1359:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+244
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L710:
.LBB295:
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #464]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+248
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L711:
.LBE295:
.LBB296:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+252
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	bl	snprintf
	b	.L958
.L712:
.LBE296:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_flash_file_found_off_pile.constprop.5
	mov	r4, r0
	b	.L958
.L713:
.LBB297:
	mov	r0, r6
	add	r1, sp, #288
	mov	r2, #32
	add	r3, sp, #536
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+256
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L1360:
.LBE297:
.LBB298:
	mov	r0, r6
	add	r1, sp, #288
	mov	r2, #32
	add	r3, sp, #536
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+260
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L715:
.LBE298:
.LBB299:
	mov	r0, r6
	add	r1, sp, #288
	mov	r2, #32
	add	r3, sp, #536
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+264
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L1361:
.LBE299:
.LBB300:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r5, sp, #288
	mov	r1, r5
	mov	r2, #32
	add	r3, sp, #536
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+268
	str	r5, [sp, #0]
	bl	snprintf
	b	.L958
.L717:
.LBE300:
.LBB301:
	mov	r0, r6
	add	r1, sp, #288
	mov	r2, #32
	add	r3, sp, #536
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+272
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L718:
.LBE301:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_flash_file_deleting_off_pile.constprop.6
	mov	r4, r0
	b	.L958
.L719:
.LBB302:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r5, sp, #288
	mov	r1, r5
	mov	r2, #32
	add	r3, sp, #536
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+276
	str	r5, [sp, #0]
	bl	snprintf
	b	.L958
.L720:
.LBE302:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_flash_writing_off_pile.constprop.7
	mov	r4, r0
	b	.L958
.L1362:
.LBB303:
	mov	r0, r6
	add	r1, sp, #288
	mov	r2, #32
	add	r3, sp, #536
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+280
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L722:
.LBE303:
.LBB304:
	mov	r0, r6
	add	r1, sp, #288
	mov	r2, #48
	add	r3, sp, #536
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+284
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L723:
.LBE304:
.LBB305:
	ldr	r2, .L1475+288
	add	r3, sp, #432
	ldmia	r2, {r0, r1, r2}
	stmia	r3, {r0, r1, r2}
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	cmp	r3, #2
	bhi	.L967
	add	r2, sp, #500
	add	r3, r2, r3, asl #2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+292
	ldr	r3, [r3, #-68]
	bl	snprintf
	b	.L958
.L967:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+296
	bl	snprintf
	b	.L958
.L724:
.LBE305:
.LBB306:
	ldr	r2, .L1475+300
	add	r3, sp, #432
	ldmia	r2, {r0, r1, r2}
	stmia	r3, {r0, r1, r2}
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	cmp	r3, #2
	bhi	.L968
	add	ip, sp, #500
	add	r3, ip, r3, asl #2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+304
	ldr	r3, [r3, #-68]
	bl	snprintf
	b	.L958
.L968:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+308
	bl	snprintf
	b	.L958
.L1476:
	.align	2
.L1475:
	.word	463
	.word	257
	.word	301
	.word	318
	.word	321
	.word	330
	.word	359
	.word	349
	.word	355
	.word	357
	.word	431
	.word	421
	.word	435
	.word	461
	.word	478
	.word	467
	.word	473
	.word	475
	.word	485
	.word	514
	.word	527
	.word	553
	.word	705
	.word	641
	.word	650
	.word	702
	.word	706
	.word	710
	.word	714
	.word	718
	.word	.LC271
	.word	.LC272
	.word	.LC273
	.word	.LC274
	.word	.LC275
	.word	.LC276
	.word	.LC277
	.word	.LC278
	.word	.LC279
	.word	.LC280
	.word	.LC281
	.word	.LC282
	.word	.LC283
	.word	.LC284
	.word	.LANCHOR6
	.word	.LC116
	.word	.LC285
	.word	.LC286
	.word	.LC287
	.word	.LC288
	.word	.LC289
	.word	.LC290
	.word	.LC291
	.word	.LC292
	.word	.LC293
	.word	.LC294
	.word	.LC295
	.word	.LC296
	.word	.LC297
	.word	.LC298
	.word	.LC299
	.word	.LC300
	.word	.LC301
	.word	.LC302
	.word	.LC303
	.word	.LC304
	.word	.LC305
	.word	.LC306
	.word	.LC307
	.word	.LC308
	.word	.LC309
	.word	.LC310
	.word	.LANCHOR1+4
	.word	.LC311
	.word	.LC312
	.word	.LANCHOR1+16
	.word	.LC313
	.word	.LC314
	.word	.LC315
	.word	.LC316
	.word	.LC317
	.word	.LC318
	.word	.LC319
	.word	.LC320
	.word	.LC321
	.word	.LC322
	.word	.LC323
	.word	.LANCHOR8
	.word	.LC324
	.word	.LC325
	.word	.LANCHOR9
	.word	.LC326
	.word	port_names
	.word	.LC327
	.word	.LC328
	.word	.LC329
	.word	.LC330
	.word	.LC331
	.word	.LC332
.L1363:
.LBE306:
	cmp	r8, #200
	bne	.L969
	add	r0, sp, #32
	ldr	r1, .L1475+312
	mov	r2, #256
	bl	strlcpy
.L969:
	add	r0, sp, #32
	bl	strlen
	mov	r4, r0
	add	r0, sp, #32
	bl	strlen
	add	r3, sp, #32
	add	r1, r3, r4
	add	r3, sp, #536
	rsb	r2, r0, #256
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	mov	r4, r0
	b	.L958
.L726:
.LBB307:
	mov	r0, r6
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	ip, sp, #512
	ldrh	r0, [ip, #-20]
	add	r1, sp, #32
	mov	r2, #256
	bl	ALERTS_parse_comm_command_string
	b	.L958
.L727:
.LBE307:
.LBB308:
	mov	r0, r6
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldr	r3, [sp, #468]
	cmp	r3, #105
	beq	.L973
	bhi	.L976
	cmp	r3, #100
	bne	.L971
	b	.L1413
.L976:
	cmp	r3, #106
	beq	.L974
	cmp	r3, #123
	bne	.L971
	b	.L1414
.L1413:
	add	r0, sp, #32
	ldr	r1, .L1475+316
	mov	r2, #256
	bl	strlcpy
	b	.L958
.L1414:
	add	r0, sp, #32
	ldr	r1, .L1475+320
	mov	r2, #256
	bl	strlcpy
	b	.L958
.L973:
	add	r0, sp, #32
	ldr	r1, .L1475+324
	mov	r2, #256
	bl	strlcpy
	b	.L958
.L974:
	add	r0, sp, #32
	ldr	r1, .L1475+328
	mov	r2, #256
	bl	strlcpy
	b	.L958
.L971:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+332
	bl	snprintf
.LBE308:
	b	.L958
.L732:
.LBB309:
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r1, r1, #2
	add	r2, sp, #536
	mov	r3, #2
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	add	r1, sp, #512
	ldrh	r3, [r1, #-18]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+336
	ldr	r3, [sp, #464]
	bl	snprintf
	b	.L958
.L733:
.LBE309:
.LBB310:
	add	r1, sp, #492
	add	r1, r1, #2
	add	r2, sp, #536
	mov	r3, #2
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	add	r2, sp, #512
	ldrh	r3, [r2, #-20]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+340
	ldr	r3, [sp, #464]
	bl	snprintf
	b	.L958
.L1366:
.LBE310:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+344
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L735:
.LBB311:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	add	r5, sp, #288
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	mov	r1, r5
	mov	r2, #64
	add	r3, sp, #536
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	ldrb	r1, [sp, #488]	@ zero_extendqisi2
	ldr	r2, .L1475+348
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	ldr	r2, [r2, r1, asl #2]
	cmp	r3, #1
	str	r2, [sp, #4]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+352
	movne	r3, #66
	moveq	r3, #65
	str	r5, [sp, #0]
	bl	snprintf
	b	.L958
.L1364:
.LBE311:
.LBB312:
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	ip, [sp, #488]	@ zero_extendqisi2
	ldr	r3, .L1475+368
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+356
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L958
.L729:
.LBE312:
.LBB313:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldrb	r2, [sp, #476]	@ zero_extendqisi2
	ldr	r3, .L1475+368
	ldrb	ip, [sp, #488]	@ zero_extendqisi2
	ldr	r3, [r3, r2, asl #2]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, .L1475+360
	mov	r1, #256
	ldr	r2, .L1475+364
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L958
.L730:
.LBE313:
.LBB314:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r2, [sp, #476]	@ zero_extendqisi2
	ldr	r3, .L1475+368
	add	r0, sp, #32
	ldr	r3, [r3, r2, asl #2]
	mov	r1, #256
	str	r3, [sp, #0]
	ldr	r2, .L1475+372
	mov	r3, #2080
	bl	snprintf
	b	.L958
.L1365:
.LBE314:
.LBB315:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+376
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L1367:
.LBE315:
.LBB316:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1475+380
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L737:
.LBE316:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_comm_mngr_blocked_msg_during_idle_off_pile.constprop.9
	mov	r4, r0
	b	.L958
.L738:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+384
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L739:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1475+388
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L740:
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_bool_off_pile.constprop.10
	mov	r4, r0
	b	.L958
.L1368:
	mov	r3, #0
	mov	r4, #1
	stmia	sp, {r3, r4}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_bool_off_pile.constprop.10
	mov	r4, r0
	b	.L958
.L742:
	mov	r3, #1
	mov	lr, #0
	stmia	sp, {r3, lr}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_bool_off_pile.constprop.10
	mov	r4, r0
	b	.L958
.L1369:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_bool_off_pile.constprop.10
	mov	r4, r0
	b	.L958
.L744:
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_date_off_pile.constprop.11
	mov	r4, r0
	b	.L958
.L745:
	mov	r3, #0
	mov	ip, #1
	stmia	sp, {r3, ip}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_date_off_pile.constprop.11
	mov	r4, r0
	b	.L958
.L746:
	mov	r0, #1
	mov	r3, #0
	stmia	sp, {r0, r3}
	mov	r1, r8
	mov	r0, r6
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_date_off_pile.constprop.11
	mov	r4, r0
	b	.L958
.L747:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_date_off_pile.constprop.11
	mov	r4, r0
	b	.L958
.L1370:
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_float_off_pile.constprop.12
	mov	r4, r0
	b	.L958
.L749:
	mov	r1, #0
	mov	r3, #1
	stmia	sp, {r1, r3}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_float_off_pile.constprop.12
	mov	r4, r0
	b	.L958
.L750:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_float_off_pile.constprop.12
	mov	r4, r0
	b	.L958
.L751:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_float_off_pile.constprop.12
	mov	r4, r0
	b	.L958
.L1371:
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_int32_off_pile.constprop.13
	mov	r4, r0
	b	.L958
.L753:
	mov	r3, #0
	mov	r4, #1
	stmia	sp, {r3, r4}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_int32_off_pile.constprop.13
	mov	r4, r0
	b	.L958
.L754:
	mov	r3, #1
	mov	lr, #0
	stmia	sp, {r3, lr}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_int32_off_pile.constprop.13
	mov	r4, r0
	b	.L958
.L1372:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_int32_off_pile.constprop.13
	mov	r4, r0
	b	.L958
.L756:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_string_off_pile.constprop.14
	mov	r4, r0
	b	.L958
.L757:
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_string_off_pile.constprop.14
	mov	r4, r0
	b	.L958
.L758:
.LBB317:
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #464]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1475+392
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L1373:
.LBE317:
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_time_off_pile.constprop.15
	mov	r4, r0
	b	.L958
.L760:
	mov	r3, #0
	mov	ip, #1
	stmia	sp, {r3, ip}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_time_off_pile.constprop.15
	mov	r4, r0
	b	.L958
.L761:
	mov	r0, #1
	mov	r3, #0
	stmia	sp, {r0, r3}
	mov	r1, r8
	mov	r0, r6
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_time_off_pile.constprop.15
	mov	r4, r0
	b	.L958
.L1374:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_time_off_pile.constprop.15
	mov	r4, r0
	b	.L958
.L763:
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_uint32_off_pile.constprop.16
	mov	r4, r0
	b	.L958
.L764:
	mov	r1, #0
	mov	r3, #1
	stmia	sp, {r1, r3}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_uint32_off_pile.constprop.16
	mov	r4, r0
	b	.L958
.L765:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_uint32_off_pile.constprop.16
	mov	r4, r0
	b	.L958
.L1375:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_range_check_failed_uint32_off_pile.constprop.16
	mov	r4, r0
	b	.L958
.L767:
	add	r3, sp, #536
	str	r3, [sp, #0]
	mov	r0, r5
	mov	r1, r6
	mov	r2, r8
	add	r3, sp, #32
	bl	ALERTS_derate_table_update_failed_off_pile.constprop.17
	mov	r4, r0
	b	.L958
.L768:
.LBB318:
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r1, r1, #2
	add	r2, sp, #536
	mov	r3, #2
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	add	r4, r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	add	r4, r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #496
	add	r2, sp, #536
	mov	r3, #2
	add	r4, r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #496
	add	r1, r1, #2
	add	r2, sp, #536
	mov	r3, #2
	add	r4, r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	add	ip, sp, #512
	ldrh	r3, [ip, #-20]
	fldd	d7, .L1477
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldrh	r3, [ip, #-18]
	mov	r1, #256
	str	r3, [sp, #4]
	ldrb	r3, [sp, #488]	@ zero_extendqisi2
	ldr	r2, .L1477+8
	str	r3, [sp, #8]
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	str	r3, [sp, #12]
	ldrsh	r3, [ip, #-16]
	fmsr	s10, r3	@ int
	fsitod	d6, s10
	fdivd	d6, d6, d7
	fstd	d6, [sp, #16]
	ldrsh	r3, [ip, #-14]
	fmsr	s11, r3	@ int
	ldr	r3, [sp, #468]
	fsitod	d6, s11
	fdivd	d7, d6, d7
	fstd	d7, [sp, #24]
	bl	snprintf
.LBE318:
	b	.L958
.L1376:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_derate_table_update_station_count_off_pile.constprop.19
	mov	r4, r0
	b	.L958
.L770:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_mainline_break_off_pile.constprop.20
	mov	r4, r0
	b	.L958
.L771:
.LBB319:
	mov	r0, r6
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+12
	mov	r2, #256
	bl	strlcpy
	b	.L958
.L1377:
.LBE319:
.LBB320:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	add	r2, sp, #512
	ldrh	r1, [r2, #-20]
	ldrb	r0, [sp, #476]	@ zero_extendqisi2
	add	r2, sp, #432
	mov	r3, #16
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, #256
	ldr	r2, .L1477+16
	mov	r3, r0
	add	r0, sp, #32
	bl	snprintf
	b	.L958
.L777:
.LBE320:
.LBB321:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L979
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	ldr	r2, .L1477+20
	ldr	r1, .L1477+24
	cmp	r5, #216
	movne	r5, r1
	moveq	r5, r2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+28
	add	r3, r3, #65
	str	r5, [sp, #0]
	bl	snprintf
	b	.L958
.L979:
	ldr	ip, .L1477+24
	cmp	r5, #216
	ldr	r3, .L1477+20
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+32
	movne	r3, ip
	bl	snprintf
	b	.L958
.L776:
.LBE321:
.LBB322:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	add	r3, sp, #512
	ldrh	r1, [r3, #-20]
	add	r2, sp, #432
	ldrb	r0, [sp, #476]	@ zero_extendqisi2
	mov	r3, #16
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, #256
	ldr	r2, .L1477+36
	mov	r3, r0
	add	r0, sp, #32
	bl	snprintf
	b	.L958
.L1378:
.LBE322:
.LBB323:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L982
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+40
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L982:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+44
	bl	snprintf
	b	.L958
.L773:
.LBE323:
.LBB324:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L983
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+48
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L983:
	add	r0, sp, #32
	ldr	r1, .L1477+52
	mov	r2, #256
	bl	strlcpy
	b	.L958
.L774:
.LBE324:
.LBB325:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #1
	bne	.L984
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+56
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L984:
	add	r0, sp, #32
	ldr	r1, .L1477+60
	mov	r2, #256
	bl	strlcpy
	b	.L958
.L1379:
.LBE325:
.LBB326:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	add	ip, sp, #512
	ldrh	r1, [ip, #-20]
	add	r2, sp, #432
	ldrb	r0, [sp, #476]	@ zero_extendqisi2
	mov	r3, #16
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, #256
	ldr	r2, .L1477+64
	mov	r3, r0
	add	r0, sp, #32
	bl	snprintf
	b	.L958
.L779:
.LBE326:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+68
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L780:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+72
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1478:
	.align	2
.L1477:
	.word	0
	.word	1076101120
	.word	.LC333
	.word	.LC334
	.word	.LC335
	.word	.LC267
	.word	.LC268
	.word	.LC336
	.word	.LC337
	.word	.LC338
	.word	.LC339
	.word	.LC340
	.word	.LC341
	.word	.LC342
	.word	.LC343
	.word	.LC344
	.word	.LC345
	.word	.LC346
	.word	.LC347
	.word	.LC348
	.word	.LC349
	.word	.LC350
	.word	.LC351
	.word	.LC352
	.word	.LC353
	.word	.LC354
	.word	.LC355
	.word	.LC356
	.word	.LC357
	.word	.LC358
	.word	.LC359
	.word	.LC360
	.word	.LC361
	.word	.LC362
	.word	.LC363
	.word	.LC364
	.word	.LC365
	.word	.LC31
	.word	.LC32
	.word	.LC366
	.word	.LC367
	.word	.LC368
	.word	port_names
	.word	.LANCHOR0
	.word	.LC369
	.word	.LC269
	.word	.LC270
	.word	.LC370
	.word	.LC371
	.word	.LC372
	.word	.LC373
	.word	.LC374
	.word	.LC375
	.word	.LC376
	.word	.LC377
	.word	.LC378
	.word	.LC379
	.word	.LC380
	.word	.LC381
	.word	.LC382
	.word	.LC383
	.word	.LC384
	.word	.LC386
	.word	.LC385
	.word	.LC387
	.word	.LC388
	.word	.LANCHOR10
	.word	.LC389
	.word	.LC390
	.word	.LC391
	.word	.LANCHOR11
	.word	.LC392
	.word	.LC393
	.word	.LC394
	.word	.LC395
	.word	.LC396
	.word	.LC397
	.word	.LC398
	.word	.LC399
	.word	.LC400
	.word	.LC401
	.word	.LC402
	.word	.LC403
	.word	.LC404
	.word	.LC405
	.word	.LC406
.L781:
	add	r3, sp, #536
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r1, r8
	mov	r2, r5
	add	r3, sp, #32
	bl	ALERTS_pull_flow_error_off_pile.constprop.21
	mov	r4, r0
	b	.L958
.L782:
.LBB327:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #1
	bne	.L985
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+76
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L985:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+80
	bl	snprintf
	b	.L958
.L1380:
.LBE327:
.LBB328:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #1
	bne	.L986
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+84
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L986:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+88
	bl	snprintf
	b	.L958
.L784:
.LBE328:
.LBB329:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #1
	bne	.L987
	ldrb	r3, [sp, #476]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+92
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L987:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+96
	bl	snprintf
	b	.L958
.L785:
.LBE329:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_two_wire_station_decoder_fault_off_pile.constprop.22
	mov	r4, r0
	b	.L958
.L1381:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_two_wire_poc_decoder_fault_off_pile.constprop.23
	mov	r4, r0
	b	.L958
.L787:
.LBB330:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r0, [sp, #476]	@ zero_extendqisi2
	bl	GetReasonOnStr
	mov	r1, #256
	ldr	r2, .L1477+100
	mov	r3, r0
	add	r0, sp, #32
	bl	snprintf
	b	.L958
.L1382:
.LBE330:
.LBB331:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldrb	r0, [sp, #476]	@ zero_extendqisi2
	bl	GetReasonOnStr
	ldrb	r1, [sp, #488]	@ zero_extendqisi2
	ldr	r2, .L1477+264
	ldr	r2, [r2, r1, asl #2]
	mov	r1, #256
	str	r2, [sp, #0]
	ldr	r2, .L1477+104
	mov	r3, r0
	add	r0, sp, #32
	bl	snprintf
	b	.L958
.L789:
.LBE331:
.LBB332:
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	add	r2, sp, #512
	ldrh	r1, [r2, #-20]
	ldrb	r0, [sp, #488]	@ zero_extendqisi2
	add	r2, sp, #432
	mov	r3, #16
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, #256
	ldr	r2, .L1477+108
	mov	r3, r0
	add	r0, sp, #32
	bl	snprintf
	b	.L958
.L790:
.LBE332:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+112
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L791:
.LBB333:
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r0, [sp, #488]	@ zero_extendqisi2
	bl	GetReasonOnStr
	mov	r1, #256
	ldr	r2, .L1477+116
	mov	r3, r0
	add	r0, sp, #32
	bl	snprintf
	b	.L958
.L793:
.LBE333:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	str	r5, [sp, #0]
	bl	ALERTS_pull_flow_not_checked_with_reason_off_pile.constprop.37
	mov	r4, r0
	b	.L958
.L1384:
.LBB334:
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r2, [sp, #464]
	ldrb	r3, [sp, #488]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L988
	str	r2, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+120
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L988:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+124
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L795:
.LBE334:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+128
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L796:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+132
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L797:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+136
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L798:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+140
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1385:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_hub_rcvd_data_off_pile.constprop.24
	mov	r4, r0
	b	.L958
.L800:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_hub_forwarding_data_off_pile.constprop.25
	mov	r4, r0
	b	.L958
.L1386:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_router_rcvd_unexp_class_off_pile.constprop.26
	mov	r4, r0
	b	.L958
.L802:
.LBB335:
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	ldrb	r2, [sp, #476]	@ zero_extendqisi2
	ldr	r3, .L1477+168
	ldrb	ip, [sp, #488]	@ zero_extendqisi2
	ldr	r3, [r3, r2, asl #2]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #464]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r3, .L1477+172
	ldr	r2, .L1477+144
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L958
.L803:
.LBE335:
.LBB336:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	ldrb	r2, [sp, #488]	@ zero_extendqisi2
	ldr	r3, .L1477+168
	ldr	r0, [sp, #464]
	ldr	r1, .L1477+148
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, .L1477+152
	cmp	r0, #2000
	moveq	r2, r1
	str	r2, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+156
	bl	snprintf
	b	.L958
.L804:
.LBE336:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_router_rcvd_packet_not_on_hub_list_with_sn_off_pile.constprop.27
	mov	r4, r0
	b	.L958
.L805:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_router_rcvd_packet_not_for_us_off_pile.constprop.28
	mov	r4, r0
	b	.L958
.L1387:
.LBB337:
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldrb	r2, [sp, #476]	@ zero_extendqisi2
	ldr	r3, .L1477+168
	ldrb	ip, [sp, #488]	@ zero_extendqisi2
	ldr	r3, [r3, r2, asl #2]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, .L1477+172
	mov	r1, #256
	ldr	r2, .L1477+160
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L958
.L807:
.LBE337:
.LBB338:
	mov	r0, r6
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	ip, [sp, #476]	@ zero_extendqisi2
	ldr	r3, .L1477+172
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+164
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L958
.L1388:
.LBE338:
.LBB339:
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldrb	r2, [sp, #488]	@ zero_extendqisi2
	ldr	r3, .L1477+168
	ldrb	ip, [sp, #476]	@ zero_extendqisi2
	ldr	r3, [r3, r2, asl #2]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, .L1477+172
	mov	r1, #256
	ldr	r2, .L1477+176
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L958
.L809:
.LBE339:
.LBB340:
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	lr, [sp, #464]
	ldr	r3, [sp, #468]
	ldr	ip, .L1477+180
	cmp	lr, #1
	str	r3, [sp, #0]
	add	r0, sp, #32
	ldr	r3, .L1477+184
	mov	r1, #256
	ldr	r2, .L1477+188
	moveq	r3, ip
	bl	snprintf
	b	.L958
.L810:
.LBE340:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+192
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L811:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+196
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L812:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+200
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1389:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+204
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L814:
.LBB341:
	mov	r0, r6
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+208
	ldr	r3, [sp, #468]
	bl	snprintf
	b	.L958
.L1390:
.LBE341:
.LBB342:
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldrb	r3, [sp, #488]	@ zero_extendqisi2
	add	r0, sp, #32
	cmp	r3, #1
	mov	r1, #256
	ldreq	r2, .L1477+212
	ldrne	r2, .L1477+216
	ldr	r3, [sp, #468]
	bl	snprintf
	b	.L958
.L816:
.LBE342:
.LBB343:
	add	r1, sp, #288
	mov	r2, #48
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #468]
	cmp	r3, #0
	bne	.L992
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+220
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L992:
	str	r3, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+224
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L817:
.LBE343:
.LBB344:
	add	r1, sp, #288
	mov	r2, #48
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	ldr	r3, [sp, #468]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #464]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1477+228
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L818:
.LBE344:
.LBB345:
	add	r1, sp, #288
	mov	r2, #48
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #464]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1477+232
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L819:
.LBE345:
.LBB346:
	add	r5, sp, #288
	mov	r1, r5
	mov	r2, #48
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #352
	mov	r2, #48
	add	r3, sp, #536
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+236
	add	r3, sp, #352
	str	r5, [sp, #0]
	bl	snprintf
	b	.L958
.L1391:
.LBE346:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_budget_group_reduction_off_pile.constprop.29
	mov	r4, r0
	b	.L958
.L821:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_budget_period_ended_off_pile.constprop.30
	mov	r4, r0
	b	.L958
.L1392:
.LBB347:
	add	r1, sp, #352
	mov	r2, #48
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldr	r3, [sp, #464]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1477+240
	add	r3, sp, #352
	bl	snprintf
	b	.L958
.L825:
.LBE347:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_set_no_water_days_by_station_off_pile.constprop.31
	mov	r4, r0
	b	.L958
.L826:
.LBB348:
	add	r1, sp, #352
	mov	r2, #48
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	add	ip, sp, #512
	ldrh	r3, [ip, #-20]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1477+244
	add	r3, sp, #352
	bl	snprintf
	b	.L958
.L1393:
.LBE348:
.LBB349:
	mov	r0, r6
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	ip, sp, #512
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+252
	ldrh	r3, [ip, #-20]
	bl	snprintf
	b	.L958
.L828:
.LBE349:
.LBB350:
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	add	r1, sp, #512
	ldrh	r3, [r1, #-20]
	cmp	r0, #0
	beq	.L993
	ldrb	ip, [sp, #488]	@ zero_extendqisi2
	str	r3, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+248
	add	r3, ip, #65
	bl	snprintf
	b	.L958
.L993:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+252
	bl	snprintf
	b	.L958
.L824:
.LBE350:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+256
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L823:
.LBB351:
	add	r1, sp, #352
	mov	r2, #48
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+260
	add	r3, sp, #352
	bl	snprintf
	b	.L958
.L1394:
.LBE351:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_MVOR_alert_off_pile.constprop.32
	mov	r4, r0
	b	.L958
.L830:
.LBB352:
	add	r1, sp, #352
	mov	r2, #48
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldrb	r2, [sp, #488]	@ zero_extendqisi2
	ldr	r3, .L1477+264
	add	r0, sp, #32
	ldr	r3, [r3, r2, asl #2]
	mov	r1, #256
	str	r3, [sp, #0]
	ldr	r2, .L1477+268
	add	r3, sp, #352
	bl	snprintf
	b	.L958
.L831:
.LBE352:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+272
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L832:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+276
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L833:
.LBB353:
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	ip, [sp, #488]	@ zero_extendqisi2
	ldr	r3, .L1477+280
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+284
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L958
.L1395:
.LBE353:
.LBB354:
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r3, [sp, #488]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+288
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L835:
.LBE354:
.LBB355:
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	r3, [sp, #488]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+292
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L1396:
.LBE355:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_reboot_request_off_pile.constprop.33
	mov	r4, r0
	b	.L958
.L837:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+296
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L838:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+300
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L839:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_ETGAGE_percent_full_edited_off_pile.constprop.34
	mov	r4, r0
	b	.L958
.L840:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+304
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1397:
.LBB356:
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+308
	ldrb	r3, [sp, #488]	@ zero_extendqisi2
	bl	snprintf
	b	.L958
.L842:
.LBE356:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+312
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L843:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+316
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1398:
.LBB357:
	mov	r0, r6
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	ip, sp, #512
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1477+320
	ldrh	r3, [ip, #-20]
	bl	snprintf
	b	.L958
.L845:
.LBE357:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+324
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L848:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+328
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1400:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+332
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L846:
.LBB358:
	mov	r0, r6
	add	r1, sp, #472
	add	r2, sp, #536
	mov	r3, #4
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	flds	s15, [sp, #472]
	add	r0, sp, #32
	mov	r1, #256
	fcvtds	d5, s15
	fmrrd	r2, r3, d5
	str	r3, [sp, #0]
	mov	r3, r2
	ldr	r2, .L1477+336
	bl	snprintf
	b	.L958
.L1399:
.LBE358:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1477+340
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L850:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1479+8
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L851:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_stop_key_pressed_off_pile.constprop.35
	mov	r4, r0
	b	.L958
.L852:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	str	r5, [sp, #0]
	bl	ALERTS_pull_wind_paused_or_resumed_off_pile.constprop.36
	mov	r4, r0
	b	.L958
.L853:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1479+12
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1401:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_accum_rain_set_by_station_cleared_off_pile.constprop.38
	mov	r4, r0
	b	.L958
.L855:
.LBB359:
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L994
	ldrb	r3, [sp, #488]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+16
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L994:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+20
	bl	snprintf
	b	.L958
.L1402:
.LBE359:
.LBB360:
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L995
	ldrb	r3, [sp, #488]	@ zero_extendqisi2
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+24
	add	r3, r3, #65
	bl	snprintf
	b	.L958
.L995:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+28
	bl	snprintf
	b	.L958
.L857:
.LBE360:
.LBB361:
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	add	r2, sp, #512
	ldrh	r1, [r2, #-20]
	ldrb	r0, [sp, #488]	@ zero_extendqisi2
	add	r2, sp, #432
	mov	r3, #16
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, #256
	ldr	r2, .L1479+32
	mov	r3, r0
	add	r0, sp, #32
	bl	snprintf
	b	.L958
.L858:
.LBE361:
.LBB362:
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #492
	add	r2, sp, #536
	mov	r3, #2
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	add	r3, sp, #512
	ldrh	r1, [r3, #-20]
	add	r2, sp, #432
	ldrb	r0, [sp, #488]	@ zero_extendqisi2
	mov	r3, #16
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, #256
	ldr	r2, .L1479+36
	mov	r3, r0
	add	r0, sp, #32
	bl	snprintf
	b	.L958
.L859:
.LBE362:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_test_station_started_off_pile.constprop.39
	mov	r4, r0
	b	.L958
.L1383:
.LBB363:
	mov	r0, r6
	add	r1, sp, #352
	mov	r2, #48
	add	r3, sp, #536
	bl	ALERTS_pull_string_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+40
	add	r3, sp, #352
	bl	snprintf
	b	.L958
.L860:
.LBE363:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_manual_water_station_started_off_pile.constprop.40
	mov	r4, r0
	b	.L958
.L1403:
.LBB364:
	add	r1, sp, #288
	mov	r2, #64
	add	r3, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_string_off_pile
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r0, r4
	bne	.L958
	ldrb	r2, [sp, #488]	@ zero_extendqisi2
	ldr	r3, .L1479+44
	add	r0, sp, #32
	ldr	r3, [r3, r2, asl #2]
	mov	r1, #256
	str	r3, [sp, #0]
	ldr	r2, .L1479+48
	add	r3, sp, #288
	bl	snprintf
	b	.L958
.L862:
.LBE364:
.LBB365:
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	mov	r3, #1
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	mov	r4, r0
	bne	.L958
	ldrb	ip, [sp, #488]	@ zero_extendqisi2
	ldr	r3, .L1479+44
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+52
	ldr	r3, [r3, ip, asl #2]
	bl	snprintf
	b	.L958
.L1404:
.LBE365:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_ETTable_Substitution_off_pile.constprop.41
	mov	r4, r0
	b	.L958
.L864:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_ETTable_LimitedEntry_off_pile.constprop.42
	mov	r4, r0
	b	.L958
.L865:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_light_ID_with_text_from_pile.constprop.43
	mov	r4, r0
	b	.L958
.L866:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1479+56
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L867:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1479+60
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1405:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1479+64
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L869:
	cmp	r8, #200
	movne	r4, #0
	bne	.L958
	add	r0, sp, #32
	ldr	r1, .L1479+68
	mov	r2, #256
	bl	strlcpy
	mov	r4, #0
	b	.L958
.L1406:
.LBB366:
	add	r1, sp, #472
	add	r2, sp, #536
	mov	r3, #4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	flds	s15, [sp, #472]
	ldr	r1, [sp, #464]
	add	r0, sp, #32
	fcvtds	d5, s15
	str	r1, [sp, #4]
	ldr	r1, [sp, #468]
	str	r1, [sp, #8]
	mov	r1, #256
	fmrrd	r2, r3, d5
	str	r3, [sp, #0]
	mov	r3, r2
	ldr	r2, .L1479+72
	bl	snprintf
	b	.L958
.L1407:
.LBE366:
.LBB367:
	add	r1, sp, #472
	add	r2, sp, #536
	mov	r3, #4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #476
	add	r2, sp, #536
	mov	r3, #4
	add	r4, r0, r4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	cmp	r8, #200
	add	r4, r4, r0
	bne	.L958
	flds	s15, [sp, #472]
	flds	s14, [sp, #476]
	ldr	r1, [sp, #468]
	fcvtds	d6, s15
	fcvtds	d7, s14
	str	r1, [sp, #4]
	add	r0, sp, #32
	mov	r1, #256
	fmrrd	r2, r3, d6
	fstd	d7, [sp, #8]
	str	r3, [sp, #0]
	mov	r3, r2
	ldr	r2, .L1479+76
	bl	snprintf
	b	.L958
.L871:
.LBE367:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_moisture_reading_out_of_range_off_pile.constprop.44
	mov	r4, r0
	b	.L958
.L872:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_soil_moisture_crossed_threshold_off_pile.constprop.45
	mov	r4, r0
	b	.L958
.L873:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_soil_temperature_crossed_threshold_off_pile.constprop.46
	mov	r4, r0
	b	.L958
.L874:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_soil_conductivity_crossed_threshold_off_pile.constprop.47
	mov	r4, r0
	b	.L958
.L876:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_station_added_to_group_off_pile.constprop.48
	mov	r4, r0
	b	.L958
.L877:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_station_removed_from_group_off_pile.constprop.49
	mov	r4, r0
	b	.L958
.L878:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_station_copied_off_pile.constprop.50
	mov	r4, r0
	b	.L958
.L1408:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_station_moved_from_one_group_to_another_off_pile.constprop.51
	mov	r4, r0
	b	.L958
.L880:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_station_card_added_or_removed_off_pile.constprop.52
	mov	r4, r0
	b	.L958
.L881:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_lights_card_added_or_removed_off_pile.constprop.53
	mov	r4, r0
	b	.L958
.L1409:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_poc_card_added_or_removed_off_pile.constprop.54
	mov	r4, r0
	b	.L958
.L883:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_weather_card_added_or_removed_off_pile.constprop.55
	mov	r4, r0
	b	.L958
.L1410:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_communication_card_added_or_removed_off_pile.constprop.56
	mov	r4, r0
	b	.L958
.L885:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_station_terminal_added_or_removed_off_pile.constprop.57
	mov	r4, r0
	b	.L958
.L886:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_lights_terminal_added_or_removed_off_pile.constprop.58
	mov	r4, r0
	b	.L958
.L887:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_poc_terminal_added_or_removed_off_pile.constprop.59
	mov	r4, r0
	b	.L958
.L1411:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_weather_terminal_added_or_removed_off_pile.constprop.60
	mov	r4, r0
	b	.L958
.L889:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_communication_terminal_added_or_removed_off_pile.constprop.61
	mov	r4, r0
	b	.L958
.L890:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_two_wire_terminal_added_or_removed_off_pile.constprop.62
	mov	r4, r0
	b	.L958
.L891:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_poc_assigned_to_mainline_off_pile.constprop.63
	mov	r4, r0
	b	.L958
.L892:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_station_group_assigned_to_mainline_off_pile.constprop.64
	mov	r4, r0
	b	.L958
.L893:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_station_group_assigned_to_a_moisture_sensor_off_pile.constprop.65
	mov	r4, r0
	b	.L958
.L1412:
	mov	r0, r6
	mov	r1, r8
	add	r2, sp, #32
	add	r3, sp, #536
	bl	ALERTS_pull_walk_thru_station_added_or_removed_off_pile.constprop.1
	mov	r4, r0
	b	.L958
.L679:
	cmp	r5, #49152
	bcc	.L996
.LBB368:
	add	r1, sp, #484
	add	r2, sp, #536
	mov	r3, #4
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r2, sp, #536
	mov	r3, #4
	add	r1, sp, #480
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	ldr	r3, .L1479+80
	sub	r2, r5, #49152
	cmp	r2, r3
	add	r7, r0, r7
	movls	r0, r6
	addls	r1, sp, #352
	addls	r2, sp, #536
	movls	r3, #48
	bls	.L999
.L997:
	sub	r2, r5, #53248
	cmp	r2, r3
	mov	r0, r6
	add	r1, sp, #468
	add	r2, sp, #536
	mov	r3, #4
	bhi	.L999
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #464
	add	r2, sp, #536
	mov	r3, #4
	mov	r4, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	ldr	r1, [sp, #464]
	add	r2, sp, #432
	mov	r3, #16
	add	r4, r4, r0
	ldr	r0, [sp, #468]
	add	r7, r4, r7
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	b	.L998
.L999:
	bl	ALERTS_pull_object_off_pile
	add	r7, r0, r7
.L998:
	mov	r3, #4
	mov	r0, r6
	add	r1, sp, #488
	add	r2, sp, #536
	bl	ALERTS_pull_object_off_pile
	ldr	r3, [sp, #488]
	cmp	r3, #8
	add	r4, r7, r0
	bhi	.L958
	cmp	r3, #0
	beq	.L1001
	add	r1, sp, #456
	add	r2, sp, #536
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r1, sp, #448
	add	r2, sp, #536
	ldr	r3, [sp, #488]
	mov	r7, r0
	mov	r0, r6
	bl	ALERTS_pull_object_off_pile
	add	r0, r7, r0
	add	r4, r4, r0
.L1001:
	cmp	r8, #200
	bne	.L958
	ldr	r3, .L1479+84
	add	r0, sp, #32
	cmp	r6, r3
	ldreq	r1, .L1479+88
	ldrne	r1, .L1479+92
	mov	r2, #256
	bl	strlcpy
	ldr	r3, .L1479+96
	cmp	r5, r3
	beq	.L1091
	add	r6, sp, #32
	bhi	.L1194
	sub	r3, r3, #166
	cmp	r5, r3
	beq	.L1049
	bhi	.L1195
	sub	r3, r3, #47
	cmp	r5, r3
	beq	.L1029
	bhi	.L1196
	sub	r3, r3, #20
	cmp	r5, r3
	beq	.L1017
	bhi	.L1197
	sub	r3, r3, #7
	cmp	r5, r3
	beq	.L1010
	mov	r0, r6
	bhi	.L1198
	sub	r3, r3, #3
	cmp	r5, r3
	beq	.L1007
	bhi	.L1199
	cmp	r5, #49152
	beq	.L1005
	sub	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1415
.L1199:
	ldr	r3, .L1479+100
	cmp	r5, r3
	beq	.L1008
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1416
.L1198:
	ldr	r3, .L1479+104
	cmp	r5, r3
	beq	.L1013
	bhi	.L1200
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1011
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1417
.L1200:
	ldr	r3, .L1479+108
	cmp	r5, r3
	beq	.L1015
	bhi	.L1016
	b	.L1418
.L1197:
	ldr	r3, .L1479+112
	cmp	r5, r3
	beq	.L1023
	bhi	.L1201
	sub	r3, r3, #5
	cmp	r5, r3
	beq	.L1020
	bhi	.L1202
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1018
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1419
.L1202:
	ldr	r3, .L1479+116
	cmp	r5, r3
	beq	.L1021
	add	r3, r3, #3
	cmp	r5, r3
	bne	.L1004
	b	.L1420
.L1201:
	ldr	r3, .L1479+120
	cmp	r5, r3
	beq	.L1026
	bhi	.L1203
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1024
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1421
.L1203:
	ldr	r3, .L1479+124
	cmp	r5, r3
	bls	.L1027
	b	.L1422
.L1196:
	ldr	r3, .L1479+128
	cmp	r5, r3
	bhi	.L1204
	sub	r3, r3, #3
	cmp	r5, r3
	bcs	.L1040
	sub	r3, r3, #10
	cmp	r5, r3
	beq	.L1035
	mov	r0, r6
	bhi	.L1205
	sub	r3, r3, #3
	cmp	r5, r3
	beq	.L1032
	bhi	.L1206
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1030
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1423
.L1206:
	ldr	r3, .L1479+132
	cmp	r5, r3
	beq	.L1033
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1424
.L1205:
	ldr	r3, .L1479+136
	cmp	r5, r3
	bhi	.L1039
	sub	r3, r3, #2
	cmp	r5, r3
	bcs	.L1038
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1036
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1425
.L1204:
	ldr	r3, .L1479+140
	cmp	r5, r3
	bhi	.L1207
	sub	r3, r3, #2
	cmp	r5, r3
	bcs	.L1044
	sub	r3, r3, #7
	cmp	r5, r3
	beq	.L1042
	bhi	.L1208
	sub	r3, r3, #2
	cmp	r5, r3
	bne	.L1004
	b	.L1426
.L1208:
	ldr	r3, .L1479+144
	cmp	r5, r3
	bcc	.L1004
	b	.L1427
.L1207:
	ldr	r3, .L1479+148
	mov	r0, r6
	cmp	r5, r3
	bhi	.L1209
	sub	r3, r3, #2
	cmp	r5, r3
	add	r2, sp, #352
	sub	r3, r5, #49152
	fldd	d7, .L1479
	bcs	.L1046
	b	.L1428
.L1209:
	ldr	r3, .L1479+152
	cmp	r5, r3
	beq	.L1047
	add	r3, r3, #5
	cmp	r5, r3
	bne	.L1004
	b	.L1429
.L1195:
	ldr	r3, .L1479+156
	cmp	r5, r3
	beq	.L1071
	bhi	.L1210
	sub	r3, r3, #16
	cmp	r5, r3
	bhi	.L1211
	sub	r3, r3, #6
	cmp	r5, r3
	bcs	.L1060
	sub	r3, r3, #24
	cmp	r5, r3
	beq	.L1055
	bhi	.L1212
	sub	r3, r3, #5
	cmp	r5, r3
	beq	.L1052
	bhi	.L1213
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1050
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1430
.L1213:
	ldr	r3, .L1479+160
	cmp	r5, r3
	beq	.L1053
	add	r3, r3, #3
	cmp	r5, r3
	bne	.L1004
	b	.L1431
.L1212:
	ldr	r3, .L1479+164
	cmp	r5, r3
	beq	.L1057
	bcc	.L1056
	add	r3, r3, #3
	cmp	r5, r3
	beq	.L1058
	bcc	.L1004
	add	r3, r3, #2
	cmp	r5, r3
	bcc	.L1004
	b	.L1432
.L1211:
	ldr	r3, .L1479+168
	cmp	r5, r3
	beq	.L1066
	bhi	.L1214
	sub	r3, r3, #3
	cmp	r5, r3
	beq	.L1063
	bhi	.L1215
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1061
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1433
.L1215:
	ldr	r3, .L1479+172
	cmp	r5, r3
	beq	.L1064
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1434
.L1214:
	ldr	r3, .L1479+176
	cmp	r5, r3
	bhi	.L1216
	sub	r3, r3, #2
	cmp	r5, r3
	bcs	.L1068
	sub	r3, r3, #2
	cmp	r5, r3
	bhi	.L1004
	b	.L1435
.L1216:
	ldr	r3, .L1479+180
	cmp	r5, r3
	beq	.L1069
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1436
.L1210:
	ldr	r3, .L1479+184
	cmp	r5, r3
	bhi	.L1217
	sub	r3, r3, #13
	cmp	r5, r3
	bcs	.L1082
	sub	r3, r3, #31
	cmp	r5, r3
	beq	.L1077
	bhi	.L1218
	sub	r3, r3, #5
	cmp	r5, r3
	beq	.L1074
	bhi	.L1219
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1072
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1437
.L1219:
	ldr	r3, .L1479+188
	cmp	r5, r3
	beq	.L1075
	add	r3, r3, #3
	cmp	r5, r3
	bne	.L1004
	b	.L1438
.L1218:
	ldr	r3, .L1479+192
	cmp	r5, r3
	bhi	.L1081
	sub	r3, r3, #13
	cmp	r5, r3
	bcs	.L1080
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1078
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1439
.L1217:
	ldr	r3, .L1479+196
	cmp	r5, r3
	bhi	.L1220
	sub	r3, r3, #6
	cmp	r5, r3
	sub	r0, r5, #49152
	bcs	.L1085
	sub	r3, r3, #8
	cmp	r5, r3
	bls	.L1083
	b	.L1440
.L1220:
	ldr	r3, .L1479+200
	cmp	r5, r3
	beq	.L1088
	bhi	.L1221
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1086
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1441
.L1221:
	ldr	r3, .L1479+204
	cmp	r5, r3
	bls	.L1089
	b	.L1442
.L1194:
	ldr	r3, .L1479+208
	cmp	r5, r3
	beq	.L1142
	bhi	.L1222
	ldr	r3, .L1479+212
	cmp	r5, r3
	beq	.L1115
	bhi	.L1223
	sub	r3, r3, #199
	cmp	r5, r3
	beq	.L1104
	bhi	.L1224
	sub	r3, r3, #7
	cmp	r5, r3
	beq	.L1097
	bhi	.L1225
	sub	r3, r3, #14
	cmp	r5, r3
	beq	.L1094
	bhi	.L1226
	sub	r3, r3, #4
	cmp	r5, r3
	beq	.L1092
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1443
.L1226:
	ldr	r3, .L1479+216
	cmp	r5, r3
	beq	.L1095
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1444
.L1225:
	ldr	r3, .L1479+220
	cmp	r5, r3
	beq	.L1100
	bhi	.L1227
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1098
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1445
.L1227:
	ldr	r3, .L1479+224
	cmp	r5, r3
	beq	.L1102
	bhi	.L1103
	b	.L1446
.L1224:
	ldr	r3, .L1479+228
	cmp	r5, r3
	bhi	.L1228
	sub	r3, r3, #9
	cmp	r5, r3
	bcs	.L1110
	sub	r3, r3, #26
	cmp	r5, r3
	beq	.L1107
	bhi	.L1229
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1105
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1447
.L1229:
	ldr	r3, .L1479+232
	cmp	r5, r3
	bne	.L1343
	b	.L1448
.L1228:
	ldr	r3, .L1479+236
	cmp	r5, r3
	beq	.L1112
	bhi	.L1230
	sub	r3, r5, #49408
	sub	r3, r3, #144
	cmp	r3, #23
	bhi	.L1004
	b	.L1449
.L1230:
	ldr	r3, .L1479+240
	cmp	r5, r3
	beq	.L1113
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1450
.L1480:
	.align	2
.L1479:
	.word	0
	.word	1090021888
	.word	.LC407
	.word	.LC408
	.word	.LC409
	.word	.LC410
	.word	.LC411
	.word	.LC412
	.word	.LC413
	.word	.LC414
	.word	.LC415
	.word	.LANCHOR2
	.word	.LC416
	.word	.LC417
	.word	.LC418
	.word	.LC419
	.word	.LC420
	.word	.LC421
	.word	.LC422
	.word	.LC423
	.word	4095
	.word	alerts_struct_engineering
	.word	.LC54
	.word	.LANCHOR1
	.word	49397
	.word	49155
	.word	49160
	.word	49162
	.word	49172
	.word	49168
	.word	49175
	.word	49182
	.word	49203
	.word	49188
	.word	49195
	.word	49215
	.word	49210
	.word	49221
	.word	49222
	.word	49288
	.word	49238
	.word	49255
	.word	49278
	.word	49276
	.word	49285
	.word	49286
	.word	49340
	.word	49292
	.word	49312
	.word	49368
	.word	49371
	.word	49395
	.word	53269
	.word	49622
	.word	49414
	.word	49419
	.word	49421
	.word	49461
	.word	49427
	.word	49602
	.word	49603
	.word	53252
	.word	49628
	.word	49626
	.word	49629
	.word	53250
	.word	53261
	.word	53259
	.word	53265
	.word	53267
	.word	61449
	.word	57368
	.word	53270
	.word	57347
	.word	57352
	.word	57353
	.word	61442
	.word	57373
	.word	57374
	.word	61445
	.word	61447
	.word	61472
	.word	61453
	.word	61469
	.word	61470
	.word	61481
	.word	61478
	.word	61484
	.word	61486
	.word	.LC424
	.word	.LC425
	.word	.LC426
	.word	.LC427
	.word	.LC428
	.word	.LC429
	.word	.LC430
	.word	.LC431
	.word	.LC432
	.word	.LC433
	.word	.LC434
	.word	.LC435
	.word	.LC436
	.word	.LC437
	.word	.LC438
	.word	.LC439
	.word	.LC440
	.word	.LC441
	.word	.LC442
	.word	.LC443
	.word	.LC444
	.word	.LC445
	.word	.LC446
	.word	.LC447
	.word	.LC448
	.word	.LC449
	.word	.LC450
	.word	.LC452
	.word	.LC451
	.word	.LC453
	.word	.LC454
	.word	.LC455
	.word	.LC456
	.word	.LC8
	.word	.LC458
	.word	.LC459
	.word	.LC460
	.word	.LC461
	.word	.LC462
	.word	.LC463
	.word	.LC464
	.word	.LC465
	.word	.LC466
	.word	.LC467
	.word	.LC468
	.word	.LC469
	.word	.LC470
	.word	.LC471
	.word	.LC472
	.word	.LC473
	.word	.LC474
	.word	.LC475
	.word	.LC476
	.word	.LC477
	.word	.LC478
	.word	.LC479
	.word	.LC480
	.word	.LC481
	.word	.LC457
	.word	.LC482
.L1223:
	ldr	r3, .L1479+244
	cmp	r5, r3
	beq	.L1128
	bhi	.L1231
	ldr	r3, .L1479+248
	cmp	r5, r3
	beq	.L1121
	bhi	.L1232
	sub	r3, r3, #3
	cmp	r5, r3
	beq	.L1118
	bhi	.L1233
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1116
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1451
.L1233:
	ldr	r3, .L1479+252
	cmp	r5, r3
	beq	.L1119
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1452
.L1232:
	cmp	r5, #53248
	beq	.L1124
	bhi	.L1234
	ldr	r3, .L1479+256
	cmp	r5, r3
	beq	.L1122
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1453
.L1234:
	ldr	r3, .L1479+260
	cmp	r5, r3
	ldr	r3, [sp, #456]
	beq	.L1126
	bhi	.L1127
	b	.L1454
.L1231:
	ldr	r3, .L1479+264
	cmp	r5, r3
	beq	.L1135
	mov	r0, r6
	bhi	.L1235
	sub	r3, r3, #6
	cmp	r5, r3
	beq	.L1131
	bhi	.L1236
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1129
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1455
.L1236:
	ldr	r3, .L1479+268
	cmp	r5, r3
	beq	.L1133
	bhi	.L1134
	sub	r3, r3, #3
	cmp	r5, r3
	bne	.L1004
	b	.L1456
.L1235:
	ldr	r3, .L1479+272
	cmp	r5, r3
	beq	.L1138
	bhi	.L1237
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1136
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1457
.L1237:
	ldr	r3, .L1479+276
	cmp	r5, r3
	beq	.L1140
	bhi	.L1141
	b	.L1458
.L1222:
	ldr	r3, .L1479+280
	cmp	r5, r3
	beq	.L1168
	bhi	.L1238
	ldr	r3, .L1479+284
	cmp	r5, r3
	bhi	.L1239
	sub	r3, r3, #11
	cmp	r5, r3
	bcs	.L1154
	sub	r3, r3, #8
	cmp	r5, r3
	beq	.L1148
	bhi	.L1240
	sub	r3, r3, #4
	cmp	r5, r3
	beq	.L1145
	bhi	.L1241
	ldr	r3, .L1479+288
	cmp	r5, r3
	beq	.L1143
	cmp	r5, #57344
	bne	.L1004
	b	.L1459
.L1241:
	ldr	r3, .L1479+292
	cmp	r5, r3
	beq	.L1146
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1460
.L1240:
	ldr	r3, .L1479+296
	cmp	r5, r3
	beq	.L1151
	bhi	.L1242
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1149
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1461
.L1242:
	ldr	r3, .L1479+300
	cmp	r5, r3
	beq	.L1152
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1462
.L1239:
	ldr	r3, .L1479+304
	cmp	r5, r3
	beq	.L1161
	bhi	.L1243
	ldr	r3, .L1479+308
	cmp	r5, r3
	beq	.L1157
	bhi	.L1244
	sub	r3, r3, #3
	cmp	r5, r3
	beq	.L1155
	add	r3, r3, #2
	cmp	r5, r3
	bne	.L1004
	b	.L1463
.L1244:
	cmp	r5, #61440
	beq	.L1159
	bhi	.L1160
	ldr	r3, .L1479+312
	cmp	r5, r3
	bne	.L1004
	b	.L1464
.L1243:
	ldr	r3, .L1479+316
	cmp	r5, r3
	beq	.L1164
	bhi	.L1245
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1162
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1465
.L1245:
	ldr	r3, .L1479+320
	cmp	r5, r3
	beq	.L1166
	bhi	.L1167
	b	.L1466
.L1238:
	ldr	r3, .L1479+324
	cmp	r5, r3
	beq	.L1180
	bhi	.L1246
	sub	r3, r3, #6
	cmp	r5, r3
	bhi	.L1247
	sub	r3, r3, #11
	cmp	r5, r3
	bcs	.L1174
	sub	r3, r3, #3
	cmp	r5, r3
	beq	.L1171
	bhi	.L1248
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1169
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1467
.L1248:
	ldr	r3, .L1479+328
	cmp	r5, r3
	beq	.L1172
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1468
.L1247:
	ldr	r3, .L1479+332
	cmp	r5, r3
	beq	.L1177
	bhi	.L1249
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1175
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1469
.L1249:
	ldr	r3, .L1479+336
	cmp	r5, r3
	beq	.L1178
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1470
.L1246:
	ldr	r3, .L1479+340
	cmp	r5, r3
	beq	.L1187
	bhi	.L1250
	sub	r3, r3, #5
	cmp	r5, r3
	beq	.L1183
	bhi	.L1251
	sub	r3, r3, #3
	cmp	r5, r3
	beq	.L1181
	add	r3, r3, #2
	cmp	r5, r3
	bne	.L1004
	b	.L1471
.L1251:
	ldr	r3, .L1479+344
	cmp	r5, r3
	beq	.L1185
	bcc	.L1184
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1472
.L1250:
	ldr	r3, .L1479+348
	cmp	r5, r3
	beq	.L1190
	bhi	.L1252
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L1188
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1473
.L1252:
	ldr	r3, .L1479+352
	cmp	r5, r3
	beq	.L1192
	bcc	.L1191
	add	r3, r3, #1
	cmp	r5, r3
	bne	.L1004
	b	.L1474
.L1005:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+356
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1415:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+360
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1007:
	mov	r1, #256
	ldr	r2, .L1479+364
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1008:
	ldr	r0, [sp, #456]
	bl	GetPriorityLevelStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetPriorityLevelStr
	mov	r1, #256
	ldr	r2, .L1479+368
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1416:
	ldr	r0, [sp, #456]
	bl	GetAlertActionStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetAlertActionStr
	mov	r1, #256
	ldr	r2, .L1479+372
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1010:
	ldr	r0, [sp, #456]
	bl	GetAlertActionStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetAlertActionStr
	mov	r1, #256
	ldr	r2, .L1479+376
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1011:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+380
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1417:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1479+384
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1013:
	ldr	r3, [sp, #456]
	mov	r1, #256
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	ldr	r2, .L1479+388
	str	r3, [sp, #4]
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1418:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+392
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1015:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+396
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1016:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+400
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1017:
	ldr	r3, [sp, #456]
	ldr	r2, [sp, #448]
	cmp	r3, #99
	sub	r3, r3, #100
	str	r3, [sp, #0]
	bls	.L1254
	cmp	r2, #99
	sub	r2, r2, #100
	str	r2, [sp, #4]
	mov	r0, r6
	mov	r1, #256
	ldrhi	r2, .L1479+404
	ldrls	r2, .L1479+408
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1254:
	cmp	r2, #99
	sub	r2, r2, #100
	str	r2, [sp, #4]
	mov	r0, r6
	mov	r1, #256
	ldrhi	r2, .L1479+412
	ldrls	r2, .L1479+416
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1018:
	mov	r5, #200
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	bl	GetDateStr
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	mov	r6, r0
	add	r0, sp, #400
	bl	GetDateStr
	mov	r1, #256
	ldr	r2, .L1479+420
	add	r3, sp, #352
	str	r6, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1419:
	mov	r5, #200
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	bl	GetDateStr
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	mov	r6, r0
	add	r0, sp, #400
	bl	GetDateStr
	mov	r1, #256
	ldr	r2, .L1479+424
	add	r3, sp, #352
	str	r6, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1020:
	ldr	r2, [sp, #456]
	ldr	r3, [sp, #448]
	cmn	r2, #-2147483647
	bne	.L1257
	str	r3, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+428
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1257:
	cmn	r3, #-2147483647
	str	r2, [sp, #0]
	bne	.L1258
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+432
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1258:
	str	r3, [sp, #4]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+436
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1021:
	ldr	r2, [sp, #456]
	ldr	r3, [sp, #448]
	cmn	r2, #-2147483647
	bne	.L1259
	str	r3, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+440
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1259:
	cmn	r3, #-2147483647
	str	r2, [sp, #0]
	bne	.L1260
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+444
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1260:
	str	r3, [sp, #4]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+448
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1420:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+452
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1023:
	mov	r5, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	mov	r7, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1479+456
	add	r3, sp, #352
	str	r7, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1024:
	mov	r5, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	mov	r6, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1479+460
	add	r3, sp, #352
	str	r6, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1421:
	ldr	r0, [sp, #456]
	cmp	r0, #7
	bne	.L1261
	ldr	r0, [sp, #448]
	bl	GetDayLongStr
	ldr	r3, .L1479+464
	mov	r1, #256
	str	r3, [sp, #0]
	ldr	r2, .L1479+468
	add	r3, sp, #352
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1261:
	ldr	r3, [sp, #448]
	cmp	r3, #7
	bne	.L1262
	bl	GetDayLongStr
	ldr	r3, .L1479+464
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1479+468
	add	r3, sp, #352
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1262:
	bl	GetDayLongStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetDayLongStr
	mov	r1, #256
	ldr	r2, .L1479+468
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1026:
	add	r3, sp, #352
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1479+472
	bl	sp_strlcat
	ldr	r0, [sp, #456]
	sub	r3, r0, #5
	cmp	r3, #11
	bhi	.L1263
	bl	GetScheduleTypeStr
	mov	r1, #256
	ldr	r2, .L1479+476
	mov	r3, r0
	add	r0, sp, #32
	b	.L1349
.L1263:
	bl	GetScheduleTypeStr
	ldr	r2, .L1479+480
	mov	r1, #256
	mov	r3, r0
	add	r0, sp, #32
.L1349:
	bl	sp_strlcat
	ldr	r0, [sp, #448]
	sub	r3, r0, #5
	cmp	r3, #11
	bhi	.L1265
	bl	GetScheduleTypeStr
	mov	r1, #256
	ldr	r2, .L1479+484
	mov	r3, r0
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1265:
	bl	GetScheduleTypeStr
	mov	r1, #256
	ldr	r2, .L1479+488
	mov	r3, r0
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1027:
	sub	r0, r5, #49152
	sub	r0, r0, #24
	bl	GetDayShortStr
	mov	r6, r0
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+588
	add	r3, sp, #352
	str	r6, [sp, #0]
	str	r5, [sp, #4]
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1422:
	mov	r5, #200
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	bl	GetDateStr
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	mov	r6, r0
	add	r0, sp, #400
	bl	GetDateStr
	mov	r1, #256
	ldr	r2, .L1479+492
	add	r3, sp, #352
	str	r6, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1029:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+496
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1030:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+500
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1423:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+504
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1032:
	ldr	r3, [sp, #456]
	mov	r1, #256
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	ldr	r2, .L1479+508
	str	r3, [sp, #4]
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1033:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1479+512
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1424:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1479+516
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1035:
	ldr	r3, [sp, #456]
	mov	r0, r6
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1479+520
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1036:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1479+524
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1425:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+528
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1038:
	add	r3, sp, #352
	str	r3, [sp, #0]
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #4]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #8]
	sub	r3, r5, #49152
	ldr	r2, .L1479+532
	sub	r3, r3, #40
	bl	sp_strlcat
	b	.L1253
.L1039:
	add	r3, sp, #352
	str	r3, [sp, #0]
	ldr	r3, [sp, #456]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r3, [sp, #448]
	ldr	r2, .L1479+536
	str	r3, [sp, #8]
	sub	r3, r5, #49152
	sub	r3, r3, #43
	bl	sp_strlcat
	b	.L1253
.L1040:
	add	r3, sp, #352
	str	r3, [sp, #0]
	ldr	r3, [sp, #456]
	mov	r0, r6
	str	r3, [sp, #4]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #8]
	sub	r3, r5, #49152
	ldr	r2, .L1479+540
	sub	r3, r3, #47
	bl	sp_strlcat
	b	.L1253
.L1426:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1479+544
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1058:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+548
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1042:
	ldr	r0, [sp, #456]
	bl	GetPOCUsageStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetPOCUsageStr
	mov	r1, #256
	ldr	r2, .L1479+552
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1427:
	ldr	r0, [sp, #456]
	bl	GetMasterValveStr
	mov	r6, r0
	ldr	r0, [sp, #448]
	bl	GetMasterValveStr
	add	r3, sp, #352
	stmia	sp, {r3, r6}
	ldr	r2, .L1479+556
	sub	r3, r5, #49152
	mov	r1, #256
	sub	r3, r3, #57
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1044:
	ldr	r0, [sp, #456]
	bl	GetFlowMeterStr
	mov	r7, r0
	ldr	r0, [sp, #448]
	bl	GetFlowMeterStr
	add	r3, sp, #352
	stmia	sp, {r3, r7}
	ldr	r2, .L1479+560
	sub	r3, r5, #49152
	mov	r1, #256
	sub	r3, r3, #60
	str	r0, [sp, #8]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1428:
	flds	s10, [sp, #456]	@ int
	flds	s11, [sp, #448]	@ int
	str	r2, [sp, #0]
	mov	r1, #256
	fuitod	d6, s10
	ldr	r2, .L1479+564
	sub	r3, r3, #63
	fdivd	d6, d6, d7
	fstd	d6, [sp, #4]
	fuitod	d6, s11
	fdivd	d7, d6, d7
	fstd	d7, [sp, #12]
	bl	sp_strlcat
	b	.L1253
.L1046:
	flds	s10, [sp, #456]	@ int
	flds	s11, [sp, #448]	@ int
	str	r2, [sp, #0]
	mov	r1, #256
	fsitod	d6, s10
	ldr	r2, .L1479+568
	sub	r3, r3, #66
	fdivd	d6, d6, d7
	fstd	d6, [sp, #4]
	fsitod	d6, s11
	fdivd	d7, d6, d7
	fstd	d7, [sp, #12]
	bl	sp_strlcat
	b	.L1253
.L1068:
	ldr	r0, [sp, #456]
	bl	GetReedSwitchStr
	mov	r6, r0
	ldr	r0, [sp, #448]
	bl	GetReedSwitchStr
	add	r3, sp, #352
	stmia	sp, {r3, r6}
	ldr	r2, .L1479+572
	sub	r3, r5, #49152
	mov	r1, #256
	sub	r3, r3, #130
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1047:
	ldr	r3, [sp, #456]
	mov	r1, #256
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	ldr	r2, .L1479+576
	str	r3, [sp, #4]
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1429:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1479+580
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1432:
	mov	r6, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	mov	r7, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	add	r3, sp, #352
	stmia	sp, {r3, r7}
	ldr	r2, .L1479+584
	sub	r3, r5, #49152
	mov	r1, #256
	sub	r3, r3, #107
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1060:
	sub	r0, r5, #49152
	sub	r0, r0, #114
	bl	GetDayShortStr
	mov	r7, r0
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1479+588
	add	r3, sp, #352
	str	r7, [sp, #0]
	str	r5, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1061:
	mov	r5, #200
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	bl	GetDateStr
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	mov	r6, r0
	add	r0, sp, #400
	bl	GetDateStr
	mov	r1, #256
	ldr	r2, .L1479+592
	add	r3, sp, #352
	str	r6, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1433:
	mov	r5, #200
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	bl	GetDateStr
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #100
	str	r5, [sp, #0]
	mov	r6, r0
	add	r0, sp, #400
	bl	GetDateStr
	mov	r1, #256
	ldr	r2, .L1481+20
	add	r3, sp, #352
	str	r6, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1063:
	ldr	r0, [sp, #456]
	mov	r1, #60
	bl	__udivsi3
	mov	r1, #60
	str	r0, [sp, #0]
	ldr	r0, [sp, #448]
	bl	__udivsi3
	mov	r1, #256
	ldr	r2, .L1481+24
	add	r3, sp, #352
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1135:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1481+28
	add	r3, sp, #432
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1124:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1481+32
	add	r3, sp, #432
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1454:
	fmsr	s15, r3	@ int
	flds	s10, [sp, #448]	@ int
	add	r0, sp, #32
	mov	r1, #256
	fuitod	d6, s15
	fldd	d7, .L1481
	ldr	r2, .L1481+36
	add	r3, sp, #432
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1126:
	fmsr	s11, r3	@ int
	fldd	d7, .L1481
	flds	s10, [sp, #448]	@ int
	add	r0, sp, #32
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+40
	add	r3, sp, #432
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1127:
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	str	r3, [sp, #4]
	mov	r1, #256
	ldr	r2, .L1481+44
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1128:
	ldr	r3, [sp, #456]
	mov	r0, r6
	sub	r3, r3, #100
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	sub	r3, r3, #100
	str	r3, [sp, #4]
	ldr	r2, .L1481+48
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1129:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1481+52
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1455:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1481+56
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1131:
	ldr	r3, [sp, #456]
	mov	r1, #256
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	ldr	r2, .L1481+60
	str	r3, [sp, #4]
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1456:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1481+64
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1133:
	ldr	r2, [sp, #456]
	ldr	r3, [sp, #448]
	cmp	r2, #0
	bne	.L1266
	str	r3, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1481+68
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1266:
	cmp	r3, #0
	str	r2, [sp, #0]
	bne	.L1267
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1481+72
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1267:
	str	r3, [sp, #4]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1481+76
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1134:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	add	r3, r3, #65
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1481+80
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1136:
	ldr	r0, [sp, #456]
	mov	r1, #60
	bl	__udivsi3
	mov	r1, #60
	str	r0, [sp, #0]
	ldr	r0, [sp, #448]
	bl	__udivsi3
	mov	r1, #256
	ldr	r2, .L1481+84
	add	r3, sp, #432
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1457:
	ldr	r0, [sp, #456]
	mov	r1, #60
	bl	__udivsi3
	mov	r1, #60
	str	r0, [sp, #0]
	ldr	r0, [sp, #448]
	bl	__udivsi3
	mov	r1, #256
	ldr	r2, .L1481+88
	add	r3, sp, #432
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1138:
	ldr	r3, [sp, #456]
	mov	r1, #256
	add	r3, r3, #1
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	ldr	r2, .L1481+92
	add	r3, r3, #1
	str	r3, [sp, #4]
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1458:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r3, [sp, #464]
	ldr	r2, .L1481+96
	add	r3, r3, #1
	bl	sp_strlcat
	b	.L1253
.L1140:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+8
	flds	s10, [sp, #448]	@ int
	add	r0, sp, #32
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+100
	add	r3, sp, #432
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1141:
	flds	s15, .L1481+16
	flds	s12, [sp, #456]
	flds	s14, [sp, #448]
	add	r0, sp, #32
	mov	r1, #256
	fmuls	s12, s12, s15
	fmuls	s14, s14, s15
	ldr	r2, .L1481+100
	add	r3, sp, #432
	fcvtds	d6, s12
	fcvtds	d7, s14
	fstd	d6, [sp, #0]
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1142:
	ldr	r3, [sp, #456]
	mov	r0, r6
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1481+104
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1143:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1481+108
	add	r3, sp, #432
	bl	sp_strlcat
	b	.L1253
.L1459:
	add	r3, sp, #456
	ldmia	r3, {r2, r3}
	add	r0, sp, #288
	mov	r1, #32
	bl	DATE_TIME_to_DateTimeStr_32
	add	r3, sp, #448
	ldmia	r3, {r2, r3}
	mov	r1, #32
	mov	r5, r0
	add	r0, sp, #400
	bl	DATE_TIME_to_DateTimeStr_32
	mov	r1, #256
	ldr	r2, .L1481+112
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1145:
	ldrb	r0, [sp, #456]	@ zero_extendqisi2
	bl	GetTimeZoneStr
	mov	r5, r0
	ldrb	r0, [sp, #448]	@ zero_extendqisi2
	bl	GetTimeZoneStr
	mov	r1, #256
	ldr	r2, .L1481+116
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1482:
	.align	2
.L1481:
	.word	0
	.word	1076101120
	.word	0
	.word	1083129856
	.word	1120403456
	.word	.LC483
	.word	.LC484
	.word	.LC485
	.word	.LC486
	.word	.LC487
	.word	.LC488
	.word	.LC489
	.word	.LC490
	.word	.LC491
	.word	.LC492
	.word	.LC493
	.word	.LC494
	.word	.LC495
	.word	.LC496
	.word	.LC497
	.word	.LC498
	.word	.LC499
	.word	.LC500
	.word	.LC501
	.word	.LC502
	.word	.LC503
	.word	.LC504
	.word	.LC505
	.word	.LC506
	.word	.LC507
	.word	.LC508
	.word	.LC509
	.word	.LC510
	.word	.LC511
	.word	.LC512
	.word	.LC513
	.word	.LC514
	.word	.LC515
	.word	.LC516
	.word	.LC517
	.word	.LC518
	.word	.LC519
	.word	.LC520
	.word	.LC521
	.word	.LC522
	.word	.LC523
	.word	.LC524
	.word	.LC525
	.word	.LC526
	.word	0
	.word	1090021888
	.word	0
	.word	1079574528
.L1152:
	mov	r5, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	mov	r6, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1481+120
	mov	r3, r6
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1462:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	cmp	r3, #1
	mov	r1, #256
	ldreq	r2, .L1481+124
	ldrne	r2, .L1481+128
	bl	snprintf
	b	.L1253
.L1049:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+204
	flds	s10, [sp, #448]	@ int
	mov	r0, r6
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+132
	add	r3, sp, #352
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1050:
	ldr	r0, [sp, #456]
	bl	GetHeadTypeStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetHeadTypeStr
	mov	r1, #256
	ldr	r2, .L1481+136
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1430:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+196
	flds	s10, [sp, #448]	@ int
	add	r0, sp, #32
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+140
	add	r3, sp, #352
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1052:
	ldr	r0, [sp, #456]
	bl	GetSoilTypeStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetSoilTypeStr
	mov	r1, #256
	ldr	r2, .L1481+144
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1069:
	ldr	r0, [sp, #456]
	bl	GetSlopePercentageStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetSlopePercentageStr
	mov	r1, #256
	ldr	r2, .L1481+148
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1431:
	ldr	r0, [sp, #456]
	bl	GetPlantTypeStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetPlantTypeStr
	mov	r1, #256
	ldr	r2, .L1481+152
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1056:
	sub	r0, r5, #49152
	sub	r0, r0, #91
	bl	GetMonthShortStr
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+204
	flds	s10, [sp, #448]	@ int
	add	r2, sp, #352
	fuitod	d6, s11
	str	r2, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1481+156
	fdivd	d6, d6, d7
	mov	r3, r0
	add	r0, sp, #32
	fstd	d6, [sp, #4]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #12]
	bl	sp_strlcat
	b	.L1253
.L1064:
	ldr	r0, [sp, #456]
	bl	GetExposureStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetExposureStr
	mov	r1, #256
	ldr	r2, .L1481+160
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1434:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1481+164
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1436:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1481+168
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1071:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+204
	flds	s10, [sp, #448]	@ int
	mov	r0, r6
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+172
	add	r3, sp, #352
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1055:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+204
	flds	s10, [sp, #448]	@ int
	mov	r0, r6
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+176
	add	r3, sp, #352
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1072:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+204
	flds	s10, [sp, #448]	@ int
	add	r0, sp, #32
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+180
	add	r3, sp, #352
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1437:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+204
	flds	s10, [sp, #448]	@ int
	add	r0, sp, #32
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+184
	add	r3, sp, #352
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1074:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+204
	flds	s10, [sp, #448]	@ int
	add	r0, sp, #32
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+188
	add	r3, sp, #352
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1053:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1481+204
	flds	s10, [sp, #448]	@ int
	add	r0, sp, #32
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1481+192
	add	r3, sp, #352
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1075:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1483
	flds	s10, [sp, #448]	@ int
	add	r0, sp, #32
	fuitod	d6, s11
	mov	r1, #256
	ldr	r2, .L1483+8
	add	r3, sp, #352
	fdivd	d6, d6, d7
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1438:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+12
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1077:
	ldr	r3, [sp, #456]
	mov	r0, r6
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+16
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1078:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+20
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1439:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+24
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1080:
	mov	r6, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	sub	r5, r5, #49152
	sub	r5, r5, #147
	mov	r7, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1483+28
	add	r3, sp, #352
	stmia	sp, {r5, r7}
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1081:
	mov	r6, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	sub	r5, r5, #49152
	sub	r5, r5, #161
	mov	r7, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1483+32
	add	r3, sp, #352
	stmia	sp, {r5, r7}
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1082:
	mov	r7, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r7, [sp, #0]
	str	r7, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r7, [sp, #0]
	str	r7, [sp, #4]
	sub	r5, r5, #49152
	sub	r5, r5, #175
	mov	sl, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1483+36
	add	r3, sp, #352
	stmia	sp, {r5, sl}
	str	r0, [sp, #8]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1083:
	mov	r6, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	sub	r5, r5, #49152
	sub	r5, r5, #189
	mov	r7, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1483+40
	add	r3, sp, #352
	stmia	sp, {r5, r7}
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1440:
	sub	r0, r0, #203
	bl	GetDayLongStr
	mov	r5, #0
	ldr	r2, [sp, #456]
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	mov	r6, r0
	add	r0, sp, #288
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	mov	r7, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1483+44
	add	r3, sp, #352
	stmia	sp, {r6, r7}
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1085:
	sub	r0, r0, #210
	bl	GetDayLongStr
	mov	r5, #0
	ldr	r2, [sp, #456]
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	mov	r7, r0
	add	r0, sp, #288
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	mov	sl, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1483+48
	add	r3, sp, #352
	stmia	sp, {r7, sl}
	str	r0, [sp, #8]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1086:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+52
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1441:
	mov	r5, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	mov	r6, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1483+56
	add	r3, sp, #352
	str	r6, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1484:
	.align	2
.L1483:
	.word	0
	.word	1079574528
	.word	.LC527
	.word	.LC528
	.word	.LC529
	.word	.LC530
	.word	.LC531
	.word	.LC532
	.word	.LC533
	.word	.LC534
	.word	.LC535
	.word	.LC536
	.word	.LC537
	.word	.LC538
	.word	.LC539
	.word	.LC540
	.word	.LC541
	.word	.LC542
	.word	.LC543
	.word	.LC544
	.word	.LC545
	.word	.LC546
	.word	.LC547
	.word	1144718131
	.word	.LC548
	.word	.LC549
	.word	.LC550
	.word	.LC551
	.word	.LC552
	.word	.LC553
	.word	.LC554
	.word	.LC555
	.word	.LC556
	.word	.LC557
	.word	.LC558
	.word	.LC559
	.word	.LC560
	.word	.LC561
	.word	.LC562
	.word	.LC563
	.word	.LC564
	.word	.LC565
	.word	.LC566
	.word	.LC567
	.word	.LC568
	.word	.LC569
	.word	.LC570
	.word	.LC571
	.word	.LC572
	.word	.LC573
	.word	.LC574
	.word	.LC575
	.word	.LC576
	.word	.LC577
	.word	.LC579
	.word	.LC580
	.word	.LC581
	.word	.LC582
	.word	.LC583
	.word	.LC584
	.word	.LC585
	.word	.LC586
	.word	.LC578
	.word	.LC587
	.word	.LC588
	.word	.LC589
	.word	.LC590
.L1088:
	ldr	r3, [sp, #456]
	mov	r0, r6
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+60
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1089:
	mov	r6, #225
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #100
	str	r6, [sp, #0]
	bl	GetDateStr
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #100
	str	r6, [sp, #0]
	sub	r5, r5, #49152
	sub	r5, r5, #220
	mov	r7, r0
	add	r0, sp, #400
	bl	GetDateStr
	mov	r1, #256
	ldr	r2, .L1483+64
	add	r3, sp, #352
	stmia	sp, {r5, r7}
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1442:
	ldr	r0, [sp, #456]
	bl	GetBudgetModeStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetBudgetModeStr
	mov	r1, #256
	ldr	r2, .L1483+68
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1091:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+72
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1092:
	ldr	r0, [sp, #456]
	bl	GetPOCBudgetEntryStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetPOCBudgetEntryStr
	mov	r1, #256
	ldr	r2, .L1483+76
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1443:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+80
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1110:
	sub	r0, r5, #49408
	sub	r0, r0, #44
	bl	GetFlowTypeStr
	mov	r6, r0
	ldr	r0, [sp, #456]
	bl	GetOffOnStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetOffOnStr
	mov	r1, #256
	ldr	r2, .L1483+84
	add	r3, sp, #352
	str	r6, [sp, #0]
	str	r5, [sp, #4]
	str	r0, [sp, #8]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1343:
	ldr	r3, [sp, #456]
	sub	r5, r5, #49408
	str	r3, [sp, #4]
	ldr	r3, [sp, #448]
	sub	r5, r5, #20
	str	r3, [sp, #8]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+88
	add	r3, sp, #352
	str	r5, [sp, #0]
	bl	sp_strlcat
	b	.L1253
.L1449:
.LBB369:
	flds	s11, [sp, #456]	@ int
	ldr	r2, .L1483+92
	str	r3, [sp, #0]
	fuitos	s12, s11
	str	r2, [sp, #476]	@ float
	flds	s14, [sp, #476]
	flds	s15, [sp, #476]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+96
	fdivs	s12, s12, s14
	add	r3, sp, #352
	fcvtds	d6, s12
	fstd	d6, [sp, #4]
	flds	s12, [sp, #448]	@ int
	fuitos	s14, s12
	fdivs	s14, s14, s15
	fcvtds	d7, s14
	fstd	d7, [sp, #12]
	bl	sp_strlcat
	b	.L1253
.L1094:
.LBE369:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+100
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1095:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+104
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1444:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+108
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1097:
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L1483+112
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1098:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+116
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1445:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+120
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1100:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+124
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1446:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+128
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1102:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+132
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1103:
	ldr	r0, [sp, #456]
	bl	Get_MoistureSensor_MoistureControlMode_Str
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	Get_MoistureSensor_MoistureControlMode_Str
	mov	r1, #256
	ldr	r2, .L1483+136
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1104:
	ldr	r3, [sp, #456]
	mov	r0, r6
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+140
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1105:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+144
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1447:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+148
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1107:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+152
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1448:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+156
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1115:
	flds	s14, [sp, #456]
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L1483+160
	fcvtds	d7, s14
	add	r3, sp, #352
	fstd	d7, [sp, #0]
	flds	s14, [sp, #448]
	fcvtds	d7, s14
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1116:
	flds	s14, [sp, #456]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+164
	fcvtds	d7, s14
	add	r3, sp, #352
	fstd	d7, [sp, #0]
	flds	s14, [sp, #448]
	fcvtds	d7, s14
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1451:
	ldr	r3, [sp, #456]
	add	r0, sp, #32
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r2, .L1483+168
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1118:
	ldr	r0, [sp, #456]
	bl	Get_MoistureSensor_HighTemperatureAction_Str
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	Get_MoistureSensor_HighTemperatureAction_Str
	mov	r1, #256
	ldr	r2, .L1483+172
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1119:
	ldr	r0, [sp, #456]
	bl	Get_MoistureSensor_LowTemperatureAction_Str
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	Get_MoistureSensor_LowTemperatureAction_Str
	mov	r1, #256
	ldr	r2, .L1483+176
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1452:
	flds	s14, [sp, #456]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+180
	fcvtds	d7, s14
	add	r3, sp, #352
	fstd	d7, [sp, #0]
	flds	s14, [sp, #448]
	fcvtds	d7, s14
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1121:
	flds	s14, [sp, #456]
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L1483+184
	fcvtds	d7, s14
	add	r3, sp, #352
	fstd	d7, [sp, #0]
	flds	s14, [sp, #448]
	fcvtds	d7, s14
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1122:
	ldr	r0, [sp, #456]
	bl	Get_MoistureSensor_HighECAction_Str
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	Get_MoistureSensor_HighECAction_Str
	mov	r1, #256
	ldr	r2, .L1483+188
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1453:
	ldr	r0, [sp, #456]
	bl	Get_MoistureSensor_LowECAction_Str
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	Get_MoistureSensor_LowECAction_Str
	mov	r1, #256
	ldr	r2, .L1483+192
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1057:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+196
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1066:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+200
	add	r3, sp, #352
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1435:
	ldr	r2, [sp, #456]
	ldr	r3, [sp, #448]
	cmp	r2, #0
	bne	.L1269
	str	r3, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+204
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1269:
	cmp	r3, #0
	str	r2, [sp, #0]
	bne	.L1270
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+208
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1270:
	str	r3, [sp, #4]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+212
	add	r3, sp, #352
	bl	sp_strlcat
	b	.L1253
.L1146:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+248
	bl	sp_strlcat
	b	.L1253
.L1460:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1483+216
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1148:
	ldr	r3, [sp, #448]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1483+220
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1149:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+224
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1461:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1483+228
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1151:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1483+232
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1154:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L1271
	ldr	r3, [sp, #456]
	mov	r0, r6
	str	r3, [sp, #0]
	ldr	r3, [sp, #448]
	mov	r1, #256
	str	r3, [sp, #4]
	ldr	r3, [sp, #468]
	ldr	r2, .L1483+236
	add	r3, r3, #65
	bl	sp_strlcat
	b	.L1253
.L1271:
	ldr	r3, [sp, #448]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1483+240
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1155:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+244
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1463:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1483+248
	bl	sp_strlcat
	b	.L1253
.L1157:
	ldr	r0, [sp, #456]
	bl	GetWaterUnitsStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetWaterUnitsStr
	mov	r1, #256
	ldr	r2, .L1483+252
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1464:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+256
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1159:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	add	r3, r3, #65
	str	r3, [sp, #0]
	ldr	r3, [sp, #456]
	mov	r1, #256
	ldr	r2, .L1483+260
	add	r3, r3, #65
	bl	sp_strlcat
	b	.L1253
.L1160:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1483+264
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1161:
	flds	s15, [sp, #456]	@ int
	mov	r0, r6
	mov	r1, #256
	fuitod	d6, s15
	fldd	d7, .L1485
	fdivd	d5, d6, d7
	fmrrd	r2, r3, d5
	flds	s11, [sp, #448]	@ int
	str	r3, [sp, #0]
	fuitod	d6, s11
	mov	r3, r2
	ldr	r2, .L1485+8
	fdivd	d7, d6, d7
	fstd	d7, [sp, #4]
	bl	sp_strlcat
	b	.L1253
.L1162:
	flds	s15, [sp, #456]	@ int
	add	r0, sp, #32
	mov	r1, #256
	fuitod	d6, s15
	fldd	d7, .L1485
	fdivd	d5, d6, d7
	fmrrd	r2, r3, d5
	flds	s11, [sp, #448]	@ int
	str	r3, [sp, #0]
	fuitod	d6, s11
	mov	r3, r2
	ldr	r2, .L1485+12
	fdivd	d7, d6, d7
	fstd	d7, [sp, #4]
	bl	sp_strlcat
	b	.L1253
.L1465:
	flds	s15, [sp, #456]	@ int
	add	r0, sp, #32
	mov	r1, #256
	fuitod	d6, s15
	fldd	d7, .L1485
	fdivd	d5, d6, d7
	fmrrd	r2, r3, d5
	flds	s11, [sp, #448]	@ int
	str	r3, [sp, #0]
	fuitod	d6, s11
	mov	r3, r2
	ldr	r2, .L1485+16
	fdivd	d7, d6, d7
	fstd	d7, [sp, #4]
	bl	sp_strlcat
	b	.L1253
.L1164:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	add	r3, r3, #65
	str	r3, [sp, #0]
	ldr	r3, [sp, #456]
	mov	r1, #256
	ldr	r2, .L1485+20
	add	r3, r3, #65
	bl	sp_strlcat
	b	.L1253
.L1466:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1485+24
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1166:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1485+28
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1167:
	mov	r5, #0
	ldr	r2, [sp, #456]
	add	r0, sp, #288
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r2, [sp, #448]
	mov	r1, #32
	mov	r3, #1
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	mov	r6, r0
	add	r0, sp, #400
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, #256
	ldr	r2, .L1485+32
	mov	r3, r6
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1168:
	ldr	r3, [sp, #448]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1485+36
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1169:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	add	r3, r3, #65
	str	r3, [sp, #0]
	ldr	r3, [sp, #456]
	mov	r1, #256
	ldr	r2, .L1485+40
	add	r3, r3, #65
	bl	sp_strlcat
	b	.L1253
.L1467:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1485+44
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1171:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1485+48
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1172:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1485+52
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1468:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1485+56
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1174:
	sub	r0, r5, #61440
	sub	r0, r0, #15
	bl	GetMonthLongStr
	flds	s15, [sp, #456]	@ int
	flds	s10, [sp, #448]	@ int
	mov	r1, #256
	ldr	r2, .L1485+60
	fuitod	d6, s15
	fldd	d7, .L1485
	fdivd	d6, d6, d7
	mov	r3, r0
	add	r0, sp, #32
	fstd	d6, [sp, #0]
	fuitod	d6, s10
	fdivd	d7, d6, d7
	fstd	d7, [sp, #8]
	bl	sp_strlcat
	b	.L1253
.L1175:
	ldr	ip, .L1485+64
	ldr	r2, [sp, #448]
	mov	r3, #24
	mla	r2, r3, r2, ip
	ldr	lr, [sp, #456]
	str	r2, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+68
	mla	r3, lr, r3, ip
	bl	sp_strlcat
	b	.L1253
.L1469:
	ldr	ip, .L1485+72
	ldr	r2, [sp, #448]
	mov	r3, #28
	mla	r2, r3, r2, ip
	ldr	lr, [sp, #456]
	str	r2, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+76
	mla	r3, lr, r3, ip
	bl	sp_strlcat
	b	.L1253
.L1177:
	ldr	ip, .L1485+80
	ldr	r2, [sp, #448]
	mov	r3, #76
	mla	r2, r3, r2, ip
	ldr	lr, [sp, #456]
	str	r2, [sp, #0]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+84
	mla	r3, lr, r3, ip
	bl	sp_strlcat
	b	.L1253
.L1178:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+88
	bl	sp_strlcat
	b	.L1253
.L1470:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+92
	bl	sp_strlcat
	b	.L1253
.L1180:
	mov	r0, r6
	mov	r1, #256
	ldr	r2, .L1485+96
	bl	sp_strlcat
	b	.L1253
.L1181:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+100
	bl	sp_strlcat
	b	.L1253
.L1471:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1485+104
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1486:
	.align	2
.L1485:
	.word	0
	.word	1079574528
	.word	.LC591
	.word	.LC592
	.word	.LC593
	.word	.LC594
	.word	.LC595
	.word	.LC596
	.word	.LC508
	.word	.LC597
	.word	.LC598
	.word	.LC599
	.word	.LC600
	.word	.LC601
	.word	.LC602
	.word	.LC603
	.word	States
	.word	.LC604
	.word	Counties
	.word	.LC605
	.word	ET_PredefinedData
	.word	.LC606
	.word	.LC607
	.word	.LC608
	.word	.LC609
	.word	.LC610
	.word	.LC611
	.word	.LC612
	.word	.LC613
	.word	.LC614
	.word	.LC615
	.word	.LC616
	.word	.LC617
	.word	.LC618
	.word	.LC619
	.word	.LC620
	.word	.LC621
	.word	.LC622
	.word	.LC623
	.word	.LC624
	.word	.LC625
	.word	.LC626
	.word	.LC627
	.word	.LC628
	.word	0
	.word	1086556160
	.word	0
	.word	1079574528
	.word	0
	.word	1076101120
.L1183:
	ldr	r0, [sp, #456]
	bl	GetMasterValveStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetMasterValveStr
	mov	r1, #256
	ldr	r2, .L1485+108
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1184:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1485+112
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1185:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+116
	bl	sp_strlcat
	b	.L1253
.L1472:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+120
	bl	sp_strlcat
	b	.L1253
.L1187:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1485+124
	mov	r3, r5
	str	r0, [sp, #0]
	mov	r0, r6
	bl	sp_strlcat
	b	.L1253
.L1188:
	ldr	r0, [sp, #456]
	bl	GetMasterValveStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetMasterValveStr
	mov	r1, #256
	ldr	r2, .L1485+128
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1473:
	ldr	r0, [sp, #456]
	bl	GetNoYesStr
	mov	r5, r0
	ldr	r0, [sp, #448]
	bl	GetNoYesStr
	mov	r1, #256
	ldr	r2, .L1485+132
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #32
	bl	sp_strlcat
	b	.L1253
.L1190:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+136
	bl	sp_strlcat
	b	.L1253
.L1191:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1485+140
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1192:
	add	r1, sp, #512
	ldrh	r3, [r1, #-64]
	fldd	d7, .L1485+176
	add	r0, sp, #32
	fmsr	s10, r3	@ int
	mov	r1, #256
	fsitod	d6, s10
	fdivd	d5, d6, d7
	fmrrd	r2, r3, d5
	str	r3, [sp, #0]
	mov	r3, r2
	ldr	r2, .L1485+144
	bl	sp_strlcat
	b	.L1253
.L1474:
	add	r1, sp, #512
	ldrh	r3, [r1, #-64]
	fldd	d7, .L1485+184
	add	r0, sp, #32
	fmsr	s10, r3	@ int
	mov	r1, #256
	fsitod	d6, s10
	fdivd	d5, d6, d7
	fmrrd	r2, r3, d5
	str	r3, [sp, #0]
	mov	r3, r2
	ldr	r2, .L1485+148
	bl	sp_strlcat
	b	.L1253
.L1112:
	flds	s11, [sp, #456]	@ int
	fldd	d7, .L1485+192
	add	r0, sp, #32
	mov	r1, #256
	fuitod	d6, s11
	fdivd	d5, d6, d7
	fmrrd	r2, r3, d5
	flds	s11, [sp, #448]	@ int
	str	r3, [sp, #0]
	fuitod	d6, s11
	mov	r3, r2
	ldr	r2, .L1485+152
	fdivd	d7, d6, d7
	fstd	d7, [sp, #4]
	bl	sp_strlcat
	b	.L1253
.L1113:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	str	r3, [sp, #0]
	mov	r1, #256
	ldr	r2, .L1485+156
	ldr	r3, [sp, #456]
	bl	sp_strlcat
	b	.L1253
.L1450:
	ldr	r3, [sp, #448]
	add	r0, sp, #32
	add	r3, r3, #65
	str	r3, [sp, #0]
	ldr	r3, [sp, #456]
	mov	r1, #256
	ldr	r2, .L1485+160
	add	r3, r3, #65
	bl	sp_strlcat
	b	.L1253
.L1004:
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+164
	mov	r3, r5
	bl	sp_strlcat
.L1253:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L1272
	ldr	r3, [sp, #484]
	cmp	r3, #2
	bne	.L1272
	ldr	r3, [sp, #480]
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+168
	add	r3, r3, #65
	bl	sp_strlcat
	b	.L958
.L1272:
	ldr	r0, [sp, #484]
	bl	GetChangeReasonStr
	mov	r2, #256
	mov	r1, r0
	add	r0, sp, #32
	bl	strlcat
	b	.L958
.L996:
.LBE368:
	cmp	r8, #200
	bne	.L1273
	add	r0, sp, #32
	mov	r1, #256
	ldr	r2, .L1485+172
	mov	r3, r5
	bl	snprintf
	b	.L1273
.L958:
	add	r4, r4, #1
.L1275:
	cmp	r8, #200
	bne	.L1274
	mov	r0, r9
	add	r1, sp, #32
	ldr	r2, [sp, #532]
	bl	strlcpy
.L1274:
	mov	r0, r4
	add	sp, sp, #500
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L1273:
	mov	r4, #0
	b	.L1275
.LFE159:
	.size	nm_ALERT_PARSING_parse_alert_and_return_length, .-nm_ALERT_PARSING_parse_alert_and_return_length
	.section .rodata
	.align	2
	.set	.LANCHOR1,. + 0
.LC53:
	.ascii	"\000\000"
	.space	2
.LC3:
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LC6:
	.word	.LC0
	.word	.LC4
	.word	.LC5
	.section	.rodata.init_reason_str,"a",%progbits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	init_reason_str, %object
	.size	init_reason_str, 8
init_reason_str:
	.word	.LC629
	.word	.LC630
	.section	.rodata.installed_or_removed_str,"a",%progbits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	installed_or_removed_str, %object
	.size	installed_or_removed_str, 8
installed_or_removed_str:
	.word	.LC681
	.word	.LC682
	.section	.rodata.cts_timeout_reason_text.8628,"a",%progbits
	.align	2
	.set	.LANCHOR9,. + 0
	.type	cts_timeout_reason_text.8628, %object
	.size	cts_timeout_reason_text.8628, 12
cts_timeout_reason_text.8628:
	.word	.LC633
	.word	.LC634
	.word	.LC635
	.section	.rodata.scan_start_request_str,"a",%progbits
	.align	2
	.set	.LANCHOR11,. + 0
	.type	scan_start_request_str, %object
	.size	scan_start_request_str, 16
scan_start_request_str:
	.word	.LC660
	.word	.LC661
	.word	.LC662
	.word	.LC663
	.section	.rodata.who_initiated_str,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	who_initiated_str, %object
	.size	who_initiated_str, 36
who_initiated_str:
	.word	.LC664
	.word	.LC665
	.word	.LC666
	.word	.LC667
	.word	.LC668
	.word	.LC669
	.word	.LC670
	.word	.LC671
	.word	.LC672
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC7:
	.ascii	"%c %s\000"
.LC8:
	.ascii	"%s\000"
.LC9:
	.ascii	"ISP\000"
.LC10:
	.ascii	"Scanning\000"
.LC11:
	.ascii	"Operational\000"
.LC12:
	.ascii	"Device Exch\000"
.LC13:
	.ascii	"Unk Mode\000"
.LC14:
	.ascii	" (Device Exch Pending)\000"
.LC15:
	.ascii	" (Scan Pending)\000"
.LC16:
	.ascii	" (Code Receipt Pending)\000"
.LC17:
	.ascii	"CI: During IDLE comm_mngr block msg transmission: %"
	.ascii	"s\000"
.LC18:
	.ascii	"Updating: Increment Station Count: sta %s to %u\000"
.LC19:
	.ascii	"1st NO FLOW\000"
.LC20:
	.ascii	"1st LOW FLOW\000"
.LC21:
	.ascii	"NO FLOW\000"
.LC22:
	.ascii	"LOW FLOW\000"
.LC23:
	.ascii	"1st HIGH FLOW\000"
.LC24:
	.ascii	"HIGH FLOW\000"
.LC25:
	.ascii	"NO FLOW DETECTED BY FLOW METER Station %s\000"
.LC26:
	.ascii	"%s Station %s: saw %d gpm, expected %d gpm\000"
.LC27:
	.ascii	"STATION DECODER VOLTAGE TOO LOW: Decoder S/N %07d ("
	.ascii	"Station %s)\000"
.LC28:
	.ascii	"SOLENOID SHORT: Station %s on Decoder S/N %07d\000"
.LC29:
	.ascii	"STATION DECODER NOT RESPONDING: Decoder S/N %07d (S"
	.ascii	"tation %s)\000"
.LC30:
	.ascii	"STATION DECODER NOT RESPONDING: Decoder S/N %07d (n"
	.ascii	"o stations assigned)\000"
.LC31:
	.ascii	"ET2000\000"
.LC32:
	.ascii	"CS3000\000"
.LC33:
	.ascii	"%s ROUTER: %s packet in %s %u bytes\000"
.LC34:
	.ascii	"%s ROUTER: %s, forwarding %s packet %u bytes\000"
.LC35:
	.ascii	"%s ROUTER: %s unexp %s packet of class %u\000"
.LC36:
	.ascii	"%s ROUTER: %s from %u, dropping %s packet not on hu"
	.ascii	"b list\000"
.LC37:
	.ascii	"%s ROUTER: %s, %s packet not to us\000"
.LC38:
	.ascii	"No Water Days Set: Station %s to %d days\000"
.LC39:
	.ascii	"reset to factory defaults via KEYPAD\000"
.LC40:
	.ascii	"reset to factory defaults via CLOUD\000"
.LC41:
	.ascii	"CS3000 firmware updated\000"
.LC42:
	.ascii	"TP Micro firmware updated\000"
.LC43:
	.ascii	"CS3000 firmware distributed\000"
.LC44:
	.ascii	"TP Micro firmware distributed\000"
.LC45:
	.ascii	"firmware distribution error\000"
.LC46:
	.ascii	"firmware receipt error\000"
.LC47:
	.ascii	"a FLOWSENSE slave rebooted\000"
.LC48:
	.ascii	"hub distributing TP Micro firmware\000"
.LC49:
	.ascii	"hub distributing CS3000 firmware\000"
.LC50:
	.ascii	"Hub distributing TPMicro and CS3000 firmware\000"
.LC51:
	.ascii	"reason unknown\000"
.LC52:
	.ascii	"Controller rebooted: %s\000"
.LC54:
	.ascii	"Change: \000"
.LC55:
	.ascii	"ET Gage Percent Full from %2d%% to %2d%% (keypad)\000"
.LC56:
	.ascii	"STOPPED: %s (Controller %c)\000"
.LC57:
	.ascii	"STOPPED: %s (keypad)\000"
.LC58:
	.ascii	"Wind: Paused Irrigation\000"
.LC59:
	.ascii	" at %u mph\000"
.LC60:
	.ascii	"Wind: Resumed Irrigation at %u mph\000"
.LC61:
	.ascii	"Wind: Unknown at %u mph\000"
.LC62:
	.ascii	"Unable to check flow: Sta %s (\000"
.LC63:
	.ascii	"Cycle Too Short)\000"
.LC64:
	.ascii	"Unstable Flow)\000"
.LC65:
	.ascii	"Unknown Reason)\000"
.LC66:
	.ascii	"Accumulated Rain Cleared: Station %s%s\000"
.LC67:
	.ascii	"Accumulated Rain Set: Station %s to %.1f%s\000"
.LC68:
	.ascii	"START: Test Station %s%s\000"
.LC69:
	.ascii	"START: Manual Watering Station %s%s\000"
.LC70:
	.ascii	"%s Light Output %c %d\000"
.LC71:
	.ascii	"%s Lights Output %d\000"
.LC72:
	.ascii	"SKIPPED: Lights Output %c %d: %s\000"
.LC73:
	.ascii	"SKIPPED: Lights Output %d: %s\000"
.LC74:
	.ascii	"ELECTRICAL SHORT: Lights Output %c %d\000"
.LC75:
	.ascii	"ELECTRICAL SHORT: Lights Output %d\000"
.LC76:
	.ascii	"NO ELECTRICAL CURRENT: Lights Output %c %d\000"
.LC77:
	.ascii	"NO ELECTRICAL CURRENT: Lights Output %d\000"
.LC78:
	.ascii	"Light %d at %c: Unknown Reason\000"
.LC79:
	.ascii	"Mois out of range: VWC=%u T=%d EC=%u (model=%u)\000"
.LC80:
	.ascii	"Decoder S/N %07d soil moisture level %5.3f above ac"
	.ascii	"ceptable level %5.3f\000"
.LC81:
	.ascii	"Decoder S/N %07d soil moisture level %5.3f below ac"
	.ascii	"ceptable level %5.3f\000"
.LC82:
	.ascii	"Decoder S/N %07d soil moisture level %5.3f back wit"
	.ascii	"hin range\000"
.LC83:
	.ascii	"Decoder S/N %07d soil temperature %d F above accept"
	.ascii	"able level %d\000"
.LC84:
	.ascii	"Decoder S/N %07d soil temperature %d F below accept"
	.ascii	"able level %d\000"
.LC85:
	.ascii	"Decoder S/N %07d soil temperature %d F back within "
	.ascii	"range\000"
.LC86:
	.ascii	"Decoder S/N %07d soil conductivity %5.3f above acce"
	.ascii	"ptable level %5.3f\000"
.LC87:
	.ascii	"Decoder S/N %07d soil conductivity %5.3f below acce"
	.ascii	"ptable level %5.3f\000"
.LC88:
	.ascii	"Decoder S/N %07d soil conductivity %5.3f back withi"
	.ascii	"n range\000"
.LC89:
	.ascii	"Station Card %d-%d %s: Controller %c\000"
.LC90:
	.ascii	"Station Card %d-%d %s\000"
.LC91:
	.ascii	"Lights Card %s: Controller %c\000"
.LC92:
	.ascii	"Lights Card %s\000"
.LC93:
	.ascii	"POC Card %s: Controller %c\000"
.LC94:
	.ascii	"POC Card %s\000"
.LC95:
	.ascii	"Weather Card %s: Controller %c\000"
.LC96:
	.ascii	"Weather Card %s\000"
.LC97:
	.ascii	"Communication Card %s: Controller %c\000"
.LC98:
	.ascii	"Communication Card %s\000"
.LC99:
	.ascii	"Station Terminal %d-%d %s: Controller %c\000"
.LC100:
	.ascii	"Station Terminal %d-%d %s\000"
.LC101:
	.ascii	"Lights Terminal %s: Controller %c\000"
.LC102:
	.ascii	"Lights Terminal %s\000"
.LC103:
	.ascii	"POC Terminal %s: Controller %c\000"
.LC104:
	.ascii	"POC Terminal %s\000"
.LC105:
	.ascii	"Weather Terminal %s: Controller %c\000"
.LC106:
	.ascii	"Weather Terminal %s\000"
.LC107:
	.ascii	"Communication Terminal %s: Controller %c\000"
.LC108:
	.ascii	"Communication Terminal %s\000"
.LC109:
	.ascii	"2-Wire Terminal %s: Controller %c\000"
.LC110:
	.ascii	"2-Wire Terminal %s\000"
.LC111:
	.ascii	"Station %s added to walk thru group \"%s\" %s\000"
.LC112:
	.ascii	"Station %s removed in walk thru group \"%s\" %s\000"
.LC113:
	.ascii	"Firmware Updated to \"%s\" from \"%s\": Controller "
	.ascii	"%c\000"
.LC114:
	.ascii	"Firmware Updated to \"%s\" from \"%s\"\000"
.LC115:
	.ascii	"%s initialized \000"
.LC116:
	.ascii	"(unknown reason)\000"
.LC117:
	.ascii	"%s not on %s list : %s, %d\000"
.LC118:
	.ascii	"%s not on %s list\000"
.LC119:
	.ascii	"%s: found version %ld, edit %ld, %ld bytes\000"
.LC120:
	.ascii	"DELETED: %d, %s version %ld, edit %ld, %ld bytes\000"
.LC121:
	.ascii	"WROTE: %d, %s version %ld, edit %ld, %ld bytes\000"
.LC122:
	.ascii	"%s for %s out of range (%d); reset to %s\000"
.LC123:
	.ascii	"%s out of range (%d); reset to %s\000"
.LC124:
	.ascii	": %s:%lu\000"
.LC125:
	.ascii	"%s for %s out of range; reset to %s\000"
.LC126:
	.ascii	"%s out of range; reset to %s\000"
.LC127:
	.ascii	"%s for %s out of range (%7.3f); reset to %7.3f\000"
.LC128:
	.ascii	"%s out of range (%7.3f); reset to %7.3f\000"
.LC129:
	.ascii	"%s for %s out of range (%d); reset to %d\000"
.LC130:
	.ascii	"%s out of range (%d); reset to %d\000"
.LC131:
	.ascii	"%s contains invalid characters; invalid chars repla"
	.ascii	"ced with '?'\000"
.LC132:
	.ascii	"Unable to update: measured 0 gpm, expected %u - val"
	.ascii	"ves ON are%s\000"
.LC133:
	.ascii	"Unable to update: (differ by more than 30%%) exp %u"
	.ascii	", meas %u - valves ON are%s\000"
.LC134:
	.ascii	"Unable to update: (differ by more than 50%%) exp %u"
	.ascii	", meas %u - valves ON are%s\000"
.LC135:
	.ascii	"IRRIGATING\000"
.LC136:
	.ascii	"MV OVERRIDE\000"
.LC137:
	.ascii	"NOT IRRIGATING\000"
.LC138:
	.ascii	"<parse error>\000"
.LC139:
	.ascii	"MAINLINE BREAK %s: \"%s\" while %s: saw %d gpm, all"
	.ascii	"owed %d gpm\000"
.LC140:
	.ascii	"POC DECODER VOLTAGE TOO LOW: Decoder S/N %07d (\"%s"
	.ascii	"\")\000"
.LC141:
	.ascii	"SOLENOID SHORT: Master Valve on POC Decoder S/N %07"
	.ascii	"d (\"%s\")\000"
.LC142:
	.ascii	"POC DECODER NOT RESPONDING: Decoder S/N %07d (\"%s\""
	.ascii	")\000"
.LC143:
	.ascii	"Station Group \"%s\" run times reduced by %d%% to s"
	.ascii	"tay within budget\000"
.LC144:
	.ascii	"Station Group \"%s\" run times reduced by %d%% (LIM"
	.ascii	"IT REACHED). May go over budget\000"
.LC145:
	.ascii	"Budget Period End - No budget value\000"
.LC146:
	.ascii	"Budget Period End - POC \"%s\" UNDER BUDGET by %d%%"
	.ascii	"\000"
.LC147:
	.ascii	"Budget Period End - POC \"%s\" OVER BUDGET by %d%%\000"
.LC148:
	.ascii	"Master Valve Override: Mainline \"%s\" OPENED for %"
	.ascii	".1f hours%s\000"
.LC149:
	.ascii	"Master Valve Override: Mainline \"%s\" CLOSED for %"
	.ascii	".1f hours%s\000"
.LC150:
	.ascii	"Master Valve Override: Mainline \"%s\" ended\000"
.LC151:
	.ascii	"Master Valve Override: Mainline \"%s\" cancelled%s\000"
.LC152:
	.ascii	"ET Substitution: %s replaced %.2f historical with %"
	.ascii	".2f\000"
.LC153:
	.ascii	"Historical ET Limit: %s replaced %.2f with %.2f\000"
.LC154:
	.ascii	"Station %s assigned to \"%s\"%s\000"
.LC155:
	.ascii	"Station %s removed from \"%s\"%s\000"
.LC156:
	.ascii	"Station %s settings copied to all stations in \"%s\""
	.ascii	"%s\000"
.LC157:
	.ascii	"Station %s moved from \"%s\" to \"%s\"%s\000"
.LC158:
	.ascii	"%s assigned to %s%s\000"
.LC159:
	.ascii	"Station Group \"%s\" uses Moisture Sensor \"%s\"%s\000"
.LC160:
	.ascii	"Sending Registration Data\000"
.LC161:
	.ascii	"Registration Data Sent\000"
.LC162:
	.ascii	"Registration Data: COMM_SERVER EXCEPTION\000"
.LC163:
	.ascii	"Sending Alerts\000"
.LC164:
	.ascii	"Alerts Sent\000"
.LC165:
	.ascii	"Alerts: COMM_SERVER EXCEPTION\000"
.LC166:
	.ascii	"Sending Flow Recording\000"
.LC167:
	.ascii	"Flow Recording Sent\000"
.LC168:
	.ascii	"Flow Recording: COMM_SERVER EXCEPTION\000"
.LC169:
	.ascii	"Checking for Updates\000"
.LC170:
	.ascii	"No Updates Found\000"
.LC171:
	.ascii	"One Update Found\000"
.LC172:
	.ascii	"Two Updates Found\000"
.LC173:
	.ascii	"Hub: one update, distribute both\000"
.LC174:
	.ascii	"Hub: up to date, distribute both\000"
.LC175:
	.ascii	"Hub: up to date, distribute main\000"
.LC176:
	.ascii	"Hub: up to date, distribute tpmicro\000"
.LC177:
	.ascii	"Sending Station History\000"
.LC178:
	.ascii	"Station History Sent\000"
.LC179:
	.ascii	"Station History: COMM_SERVER EXCEPTION\000"
.LC180:
	.ascii	"Sending Station Report Data\000"
.LC181:
	.ascii	"Station Report Data Sent\000"
.LC182:
	.ascii	"Station Report Data: COMM_SERVER EXCEPTION\000"
.LC183:
	.ascii	"Sending POC Report Data\000"
.LC184:
	.ascii	"POC Report Data Sent\000"
.LC185:
	.ascii	"POC Report Data: COMM_SERVER EXCEPTION\000"
.LC186:
	.ascii	"Sending Moisture Report Data\000"
.LC187:
	.ascii	"Moisture Report Data Sent\000"
.LC188:
	.ascii	"Moisture Report Data: COMM_SERVER EXCEPTION\000"
.LC189:
	.ascii	"Sending Mainline Report Data\000"
.LC190:
	.ascii	"Mainline Report Data Sent\000"
.LC191:
	.ascii	"System Report Data: COMM_SERVER EXCEPTION\000"
.LC192:
	.ascii	"Sending Lights Data\000"
.LC193:
	.ascii	"Lights Data Sent\000"
.LC194:
	.ascii	"Lights Report Data: COMM_SERVER EXCEPTION\000"
.LC195:
	.ascii	"Sending Budget Report Data\000"
.LC196:
	.ascii	"Budget Report Data Sent\000"
.LC197:
	.ascii	"Budget Report Data: COMM SERVER EXCEPTION\000"
.LC198:
	.ascii	"Sending ET and Rain\000"
.LC199:
	.ascii	"ET and Rain Sent\000"
.LC200:
	.ascii	"ET-RAIN FOR TODAY: COMM_SERVER EXCEPTION\000"
.LC201:
	.ascii	"Receiving ET and Rain\000"
.LC202:
	.ascii	"ET and Rain Received\000"
.LC203:
	.ascii	"Sending Rain Indication\000"
.LC204:
	.ascii	"Rain Indication Sent\000"
.LC205:
	.ascii	"Rain Indication: COMM_SERVER EXCEPTION\000"
.LC206:
	.ascii	"Receiving Rain Shutdown\000"
.LC207:
	.ascii	"Rain Shutdown Received\000"
.LC208:
	.ascii	"Request to begin with Mobile\000"
.LC209:
	.ascii	"Mobile Request processed\000"
.LC210:
	.ascii	"Sending Status Screen for Mobile\000"
.LC211:
	.ascii	"Status Screen for Mobile Sent\000"
.LC212:
	.ascii	"Mobile Station ON\000"
.LC213:
	.ascii	"Mobile Station ON rcvd\000"
.LC214:
	.ascii	"Mobile Station OFF\000"
.LC215:
	.ascii	"Mobile Station OFF rcvd\000"
.LC216:
	.ascii	"Checking Firmware Version\000"
.LC217:
	.ascii	"Firmware Up-to-Date\000"
.LC218:
	.ascii	"Firmware Out-of-Date\000"
.LC219:
	.ascii	"Sending Program Data\000"
.LC220:
	.ascii	"Program Data Sent\000"
.LC221:
	.ascii	"PDATA to commserver: COMM_SERVER EXCEPTION\000"
.LC222:
	.ascii	"IHSFY Program Data\000"
.LC223:
	.ascii	"Requesting Program Data\000"
.LC224:
	.ascii	"No PData Changes Coming\000"
.LC225:
	.ascii	"PData Changes Coming\000"
.LC226:
	.ascii	"PDATA REQUEST: COMM_SERVER EXCEPTION\000"
.LC227:
	.ascii	"Receiving Program Data\000"
.LC228:
	.ascii	"Program Data Received\000"
.LC229:
	.ascii	"Set Date & Time Received\000"
.LC230:
	.ascii	"New Panel Swap Factory RESET\000"
.LC231:
	.ascii	"New Panel Swap Factory RESET processed\000"
.LC232:
	.ascii	"Old Panel Swap Factory RESET\000"
.LC233:
	.ascii	"Old Panel Swap Factory RESET processed\000"
.LC234:
	.ascii	"Receiving Send All Program Data\000"
.LC235:
	.ascii	"Send All Program Data Received\000"
.LC236:
	.ascii	"Sending ET/Rain Table\000"
.LC237:
	.ascii	"ET/Rain Table Sent\000"
.LC238:
	.ascii	"ET/Rain Table: COMM_SERVER EXCEPTION\000"
.LC239:
	.ascii	"Mobile MVOR Request\000"
.LC240:
	.ascii	"Mobile Lights Request\000"
.LC241:
	.ascii	"Mobile On/Off Request\000"
.LC242:
	.ascii	"Mobile No Water Days for All Stations\000"
.LC243:
	.ascii	"Mobile No Water Days by Group\000"
.LC244:
	.ascii	"Mobile No Water Days by Controller\000"
.LC245:
	.ascii	"Mobile No Water Days by Station\000"
.LC246:
	.ascii	"Registration Request\000"
.LC247:
	.ascii	"Set CS3-FL Option\000"
.LC248:
	.ascii	"Clear Mainline Break\000"
.LC249:
	.ascii	"Stop All Irrigation\000"
.LC250:
	.ascii	"Stop Irrigation\000"
.LC251:
	.ascii	"Check for Updates\000"
.LC252:
	.ascii	"Perform 2-Wire Discovery\000"
.LC253:
	.ascii	"Mobile Acquire Expected Request\000"
.LC254:
	.ascii	"Set CS3-HUB Option\000"
.LC255:
	.ascii	"Hub Poll (send your messages)\000"
.LC256:
	.ascii	"Sending End of Poll Period (no more msgs)\000"
.LC257:
	.ascii	"End of Poll Sent\000"
.LC258:
	.ascii	"Sending Hub is Busy (no more msgs)\000"
.LC259:
	.ascii	"Hub is Busy Sent\000"
.LC260:
	.ascii	"Receiving Hub List\000"
.LC261:
	.ascii	"Hub List Received\000"
.LC262:
	.ascii	"Stop Hub Code Distribution\000"
.LC263:
	.ascii	"Start Hub Code Distribution\000"
.LC264:
	.ascii	"Request to Send Alerts\000"
.LC265:
	.ascii	"Set CS3-AQUAPONICS Option\000"
.LC266:
	.ascii	"Unknown Command (%d)\000"
.LC267:
	.ascii	"Master Valve\000"
.LC268:
	.ascii	"Pump\000"
.LC269:
	.ascii	"front\000"
.LC270:
	.ascii	"back\000"
.LC271:
	.ascii	"--------------------------------------\000"
.LC272:
	.ascii	"Firmware Restart: Controller %c\000"
.LC273:
	.ascii	"Firmware Restart\000"
.LC274:
	.ascii	"-------\000"
.LC275:
	.ascii	"POWER FAIL: Controller %c\000"
.LC276:
	.ascii	"POWER FAIL\000"
.LC277:
	.ascii	"POWER FAIL (BROWN OUT): Controller %c\000"
.LC278:
	.ascii	"POWER FAIL (BROWN OUT)\000"
.LC279:
	.ascii	"RESET TO FACTORY DEFAULTS: Controller %c\000"
.LC280:
	.ascii	"RESET TO FACTORY DEFAULTS\000"
.LC281:
	.ascii	"CS3000 firmware at Controller %c does not match Con"
	.ascii	"troller A\000"
.LC282:
	.ascii	"TP Micro firmware at Controller %c does not match C"
	.ascii	"ontroller A\000"
.LC283:
	.ascii	"WATCHDOG TIMEOUT\000"
.LC284:
	.ascii	"User Alerts cleared \000"
.LC285:
	.ascii	"Change Lines cleared \000"
.LC286:
	.ascii	"TP Micro messages cleared \000"
.LC287:
	.ascii	"Engineering Alerts cleared \000"
.LC288:
	.ascii	"%s valid\000"
.LC289:
	.ascii	"Iced Task: %s, %u ms ago\000"
.LC290:
	.ascii	"Group not found\000"
.LC291:
	.ascii	"Group not found : %s, %d\000"
.LC292:
	.ascii	"Station not found\000"
.LC293:
	.ascii	"Station not found : %s, %d\000"
.LC294:
	.ascii	"Station not in group\000"
.LC295:
	.ascii	"Station not in group : %s, %d\000"
.LC296:
	.ascii	"Function call with NULL ptr\000"
.LC297:
	.ascii	"Function call with NULL ptr : %s, %d\000"
.LC298:
	.ascii	"Index out of range\000"
.LC299:
	.ascii	"Index out of range : %s, %d\000"
.LC300:
	.ascii	"Bit set with no data\000"
.LC301:
	.ascii	"Bit set with no data : %s, %d\000"
.LC302:
	.ascii	"File System Passed : chip %d\000"
.LC303:
	.ascii	"File size error: initializing %s\000"
.LC304:
	.ascii	"Found older file version: initializing %s\000"
.LC305:
	.ascii	"File Not Found: initializing %s\000"
.LC306:
	.ascii	"DELETING OBSOLETE: %d, %s\000"
.LC307:
	.ascii	"No obsolete files found: %s\000"
.LC308:
	.ascii	"Old Version Delete: %d, %s, no old versions found\000"
.LC309:
	.ascii	"FILE: postponed save - %s\000"
.LC310:
	.ascii	"Station Group \"%s\" is not WaterSense compliant - "
	.ascii	"requires use of ET and Rain\000"
.LC311:
	.ascii	"System_Preserves: %s\000"
.LC312:
	.ascii	"System_Preserves: Unknown activity\000"
.LC313:
	.ascii	"POC_Preserves: %s\000"
.LC314:
	.ascii	"POC_Preserves: Unknown activity\000"
.LC315:
	.ascii	"Unit Communicated: \000"
.LC316:
	.ascii	"Message failed: CTS Timeout\000"
.LC317:
	.ascii	"Message failed: Unable to Connect\000"
.LC318:
	.ascii	"Message failed: No response timeout\000"
.LC319:
	.ascii	"Message failed: No response, new inbound arrived\000"
.LC320:
	.ascii	"Message failed: Unknown reason %d\000"
.LC321:
	.ascii	"Outbound Msg:  Size: %d   Packets: %d\000"
.LC322:
	.ascii	"From CommServer:  Size: %d   Packets: %d\000"
.LC323:
	.ascii	"CI: new connection detected\000"
.LC324:
	.ascii	"Port %c: %s powered %s\000"
.LC325:
	.ascii	"Data damaged during transmission (CRC failed): %s\000"
.LC326:
	.ascii	"CTS Timeout while %s: %s\000"
.LC327:
	.ascii	"Data damaged during transmission (packet > %d): %s\000"
.LC328:
	.ascii	"No response from Controller %c (status timer expire"
	.ascii	"d)\000"
.LC329:
	.ascii	"No response from Controller %c (resp timer expired)"
	.ascii	"\000"
.LC330:
	.ascii	"LR Radio not compatible with CS3-HUB-OPT. Upgrade r"
	.ascii	"adio to Raveon RV-M7.\000"
.LC331:
	.ascii	"LR Radio not compatible with CS3-HUB-OPT. Upgrade r"
	.ascii	"adio to Raveon RV-M7. (Reminder)\000"
.LC332:
	.ascii	"%s truncated to %d chars\000"
.LC333:
	.ascii	"Table Update: [%u valve(s) on, %u gpm], index: %u, "
	.ascii	"count: %u to %u, value: %5.1f to %5.1f\000"
.LC334:
	.ascii	"Mainline Break Cleared\000"
.LC335:
	.ascii	"SOLENOID SHORT: Station %s\000"
.LC336:
	.ascii	"NO ELECTRICAL CURRENT: Controller %c %s\000"
.LC337:
	.ascii	"NO ELECTRICAL CURRENT: %s\000"
.LC338:
	.ascii	"NO ELECTRICAL CURRENT: Station %s\000"
.LC339:
	.ascii	"ELECTRICAL SHORT UNKNOWN OUTPUT: Controller %c\000"
.LC340:
	.ascii	"ELECTRICAL SHORT UNKNOWN OUTPUT\000"
.LC341:
	.ascii	"SOLENOID SHORT: Master Valve at Controller %c\000"
.LC342:
	.ascii	"SOLENOID SHORT: Master Valve\000"
.LC343:
	.ascii	"ELECTRICAL SHORT: Pump at Controller %c\000"
.LC344:
	.ascii	"ELECTRICAL SHORT: Pump\000"
.LC345:
	.ascii	"Unable to check flow: Sta %s\000"
.LC346:
	.ascii	"Unable to check flow: Flow too high\000"
.LC347:
	.ascii	"Unable to check flow: Too many stations on\000"
.LC348:
	.ascii	"2-Wire Cable Excessive Electrical Current: Controll"
	.ascii	"er %c\000"
.LC349:
	.ascii	"2-Wire Cable Excessive Electrical Current\000"
.LC350:
	.ascii	"2-Wire Terminal Overheated: Controller %c\000"
.LC351:
	.ascii	"2-Wire Terminal Overheated\000"
.LC352:
	.ascii	"2-Wire Terminal Cooled Off: Controller %c\000"
.LC353:
	.ascii	"2-Wire Terminal Cooled Off\000"
.LC354:
	.ascii	"START: %s\000"
.LC355:
	.ascii	"SKIPPED: Some or All %s: %s\000"
.LC356:
	.ascii	"Irrigation not finished at next start time: Station"
	.ascii	" %s\000"
.LC357:
	.ascii	"STOPPED: Programmed Irrigation (stop time)\000"
.LC358:
	.ascii	"END: %s\000"
.LC359:
	.ascii	"Error: Decoder based POC @ %c (S/N %07u) reporting "
	.ascii	"in\000"
.LC360:
	.ascii	"Error: Terminal based POC @ %c reporting in\000"
.LC361:
	.ascii	"FOAL_COMM: token_resp extract error\000"
.LC362:
	.ascii	"ROUTER: rcvd token with no -FL\000"
.LC363:
	.ascii	"ROUTER: rcvd token - not in a FLOWSENSE chain\000"
.LC364:
	.ascii	"ROUTER: master received TOKEN or SCAN packet\000"
.LC365:
	.ascii	"%s ROUTER: %s unknown routing class BASE %u\000"
.LC366:
	.ascii	"ROUTER: %s packet to %s not on hub list\000"
.LC367:
	.ascii	"%s ROUTER: unexp TO addr from %s\000"
.LC368:
	.ascii	"%s ROUTER: UNK port\000"
.LC369:
	.ascii	"%s ROUTER: %s rcvd token/scan RESP not for us\000"
.LC370:
	.ascii	"CI queued msg to %s %u\000"
.LC371:
	.ascii	"CI: waiting for device to connect - message build s"
	.ascii	"kipped\000"
.LC372:
	.ascii	"Port A: starting the connection process\000"
.LC373:
	.ascii	"socket attempt\000"
.LC374:
	.ascii	"data connection attempt\000"
.LC375:
	.ascii	"msg transaction seconds is %u\000"
.LC376:
	.ascii	"largest token resp %u bytes\000"
.LC377:
	.ascii	"largest token %u bytes\000"
.LC378:
	.ascii	"POC \"%s\" expected to be at budget at end of perio"
	.ascii	"d\000"
.LC379:
	.ascii	"POC \"%s\" expected to be under budget by %d%% at e"
	.ascii	"nd of period\000"
.LC380:
	.ascii	"POC \"%s\" is OVER BUDGET by %d%%. Expected to be %"
	.ascii	"d%% over budget at end of period\000"
.LC381:
	.ascii	"POC \"%s\" expected to be OVER BUDGET by %d%% at en"
	.ascii	"d of period\000"
.LC382:
	.ascii	"POC \"%s\" on Mainline \"%s\" has a budget of 0 gal"
	.ascii	"lons\000"
.LC383:
	.ascii	"POC \"%s\" is OVER BUDGET by %d%% with period endin"
	.ascii	"g today\000"
.LC384:
	.ascii	"No Water Days Set: Station Group \"%s\" to %d days\000"
.LC385:
	.ascii	"No Water Days Set: All Stations to %d days\000"
.LC386:
	.ascii	"No Water Days Set: All Stations in Controller %c to"
	.ascii	" %d days\000"
.LC387:
	.ascii	"Accumulated Rain Cleared: All Stations\000"
.LC388:
	.ascii	"Accumulated Rain Cleared: Station Group \"%s\"\000"
.LC389:
	.ascii	"MASTER VALVE OVERRIDE SKIPPED: Mainline \"%s\": %s\000"
.LC390:
	.ascii	"FLOWSENSE Communication Down - IRRIGATION WILL NOT "
	.ascii	"RUN!\000"
.LC391:
	.ascii	"FLOWSENSE Scan Successful\000"
.LC392:
	.ascii	"FLOWSENSE Scan Started: %s\000"
.LC393:
	.ascii	"Foal: \"%c\" says network is the same\000"
.LC394:
	.ascii	"Foal: \"%c\" says network is new\000"
.LC395:
	.ascii	"ET Gage - 0 Pulses\000"
.LC396:
	.ascii	"ET Gage - 0 Pulses Multiple Days\000"
.LC397:
	.ascii	"ET Gage pulse received but no ET Gages in use\000"
.LC398:
	.ascii	"ET Gage Pulse %d\000"
.LC399:
	.ascii	"Runaway ET Gage\000"
.LC400:
	.ascii	"Runaway ET Gage cleared\000"
.LC401:
	.ascii	"ET Gage Low: only %2d%% full\000"
.LC402:
	.ascii	"STOPPED: Programmed Irrigation (rain)\000"
.LC403:
	.ascii	"STOPPED: Programmed Irrigation (rain switch)\000"
.LC404:
	.ascii	"STOPPED: Programmed Irrigation (freeze switch)\000"
.LC405:
	.ascii	"Midnight-to-Midnight Rainfall is %.2f inches\000"
.LC406:
	.ascii	"Rain pulse received but no Rain Buckets in use\000"
.LC407:
	.ascii	"Wind detected but no Wind Gages in use\000"
.LC408:
	.ascii	"ET Table: New historical values loaded\000"
.LC409:
	.ascii	"Blown Fuse Replaced: Controller %c\000"
.LC410:
	.ascii	"Blown Fuse Replaced\000"
.LC411:
	.ascii	"FUSE BLOWN: Controller %c\000"
.LC412:
	.ascii	"FUSE BLOWN\000"
.LC413:
	.ascii	"Mobile: Station %s ON\000"
.LC414:
	.ascii	"Mobile: Station %s OFF\000"
.LC415:
	.ascii	"START: Walk-Thru \"%s\"\000"
.LC416:
	.ascii	"START: Manual Watering Station Group \"%s\"%s\000"
.LC417:
	.ascii	"START: Manual Watering All Stations%s\000"
.LC418:
	.ascii	"Rain Switch showing rain\000"
.LC419:
	.ascii	"Rain Switch dried out\000"
.LC420:
	.ascii	"Freeze Switch showing freezing temperatures\000"
.LC421:
	.ascii	"Freeze Switch warmed up\000"
.LC422:
	.ascii	"Mois: VWC=%5.3f T=%d EC=%u\000"
.LC423:
	.ascii	"Mois: VWC=%5.3f T=%d EC=%5.3f\000"
.LC424:
	.ascii	"%s created\000"
.LC425:
	.ascii	"%s deleted\000"
.LC426:
	.ascii	"Group Name to %s\000"
.LC427:
	.ascii	"Priority Level for %s from %s to %s\000"
.LC428:
	.ascii	"High Flow Alert Action for %s from %s to %s\000"
.LC429:
	.ascii	"Low Flow Alert Action for %s from %s to %s\000"
.LC430:
	.ascii	"ET in Use for %s from %s to %s\000"
.LC431:
	.ascii	"Line Fill Time for %s from %d to %d sec\000"
.LC432:
	.ascii	"Delay Between Valves for %s from %d to %d sec\000"
.LC433:
	.ascii	"Wind In Use for %s from %s to %s\000"
.LC434:
	.ascii	"Rain In Use for %s from %s to %s\000"
.LC435:
	.ascii	"Pump In Use for %s from %s to %s\000"
.LC436:
	.ascii	"Percent Adjust for %s from +%d%% to +%d%%\000"
.LC437:
	.ascii	"Percent Adjust for %s from +%d%% to %d%%\000"
.LC438:
	.ascii	"Percent Adjust for %s from %d%% to +%d%%\000"
.LC439:
	.ascii	"Percent Adjust for %s from %d%% to %d%%\000"
.LC440:
	.ascii	"Percent Adjust Start Date for %s from %s to %s\000"
.LC441:
	.ascii	"Percent Adjust End Date for %s from %s to %s\000"
.LC442:
	.ascii	"On-at-a-Time in Group for %s from -- to %d\000"
.LC443:
	.ascii	"On-at-a-Time in Group for %s from %d to --\000"
.LC444:
	.ascii	"On-at-a-Time in Group for %s from %d to %d\000"
.LC445:
	.ascii	"On-at-a-Time in System for %s from -- to %d\000"
.LC446:
	.ascii	"On-at-a-Time in System for %s from %d to --\000"
.LC447:
	.ascii	"On-at-a-Time in System for %s from %d to %d\000"
.LC448:
	.ascii	"Schedule Enabled for %s from %s to %s\000"
.LC449:
	.ascii	"Start Time for %s from %s to %s\000"
.LC450:
	.ascii	"Stop Time for %s from %s to %s\000"
.LC451:
	.ascii	"Mow Day for %s from %s to %s\000"
.LC452:
	.ascii	"None\000"
.LC453:
	.ascii	"Days to Water for %s from \000"
.LC454:
	.ascii	"Every %s days to \000"
.LC455:
	.ascii	"%s to \000"
.LC456:
	.ascii	"Every %s days\000"
.LC457:
	.ascii	"Irrigate %s on %s from %s to %s\000"
.LC458:
	.ascii	"Next Scheduled Date for %s from %s to %s\000"
.LC459:
	.ascii	"Irrigate on 29th or 31st for %s from %s to %s\000"
.LC460:
	.ascii	"Mainline Used for Irrigation for %s from %s to %s\000"
.LC461:
	.ascii	"Mainline Capacity in Use for %s from %s to %s\000"
.LC462:
	.ascii	"Mainline Capacity with Pump for %s from %d gpm to %"
	.ascii	"d gpm\000"
.LC463:
	.ascii	"Mainline Capacity without Pump for %s from %d gpm t"
	.ascii	"o %d gpm\000"
.LC464:
	.ascii	"Mainline Break during Irrigation for %s from %d gpm"
	.ascii	" to %d gpm\000"
.LC465:
	.ascii	"Mainline Break during MVOR for %s from %d gpm to %d"
	.ascii	" gpm\000"
.LC466:
	.ascii	"Mainline Break All Other Times for %s from %d gpm t"
	.ascii	"o %d gpm\000"
.LC467:
	.ascii	"Flow Checking In Use for %s from %s to %s\000"
.LC468:
	.ascii	"Flow Checking Top of Range %d for %s from %d gpm to"
	.ascii	" %d gpm\000"
.LC469:
	.ascii	"Flow Checking Flow Window %d for %s from +%d gpm to"
	.ascii	" +%d gpm\000"
.LC470:
	.ascii	"Flow Checking Flow Window %d for %s from -%d gpm to"
	.ascii	" -%d gpm\000"
.LC471:
	.ascii	"POC Controller Index for %s from %d to %d\000"
.LC472:
	.ascii	"POC Physically Available for %s from %s to %s\000"
.LC473:
	.ascii	"POC Usage for %s from %s to %s\000"
.LC474:
	.ascii	"Master Valve %d for %s from %s to %s\000"
.LC475:
	.ascii	"Flow Meter %d Type for %s from %s to %s\000"
.LC476:
	.ascii	"K value %d for %s from %7.3f to %7.3f\000"
.LC477:
	.ascii	"Offset %d for %s from %7.3f to %7.3f\000"
.LC478:
	.ascii	"Reed Switch %d for %s from %s to %s\000"
.LC479:
	.ascii	"Bypass Manifold Stages for %s from %d to %d\000"
.LC480:
	.ascii	"Master Valve Close Delay for %s from %d to %d sec\000"
.LC481:
	.ascii	"Start Time %d for %s from %s to %s\000"
.LC482:
	.ascii	"Start Date for %s from %s to %s\000"
.LC483:
	.ascii	"End Date for %s from %s to %s\000"
.LC484:
	.ascii	"%s Run Time from %d to %d min\000"
.LC485:
	.ascii	"Station %s Installed from %s to %s\000"
.LC486:
	.ascii	"Station %s In Use from %s to %s\000"
.LC487:
	.ascii	"Station %s Total Minutes from %0.1f to %0.1f min\000"
.LC488:
	.ascii	"Station %s Cycle Time from %0.1f to %0.1f min\000"
.LC489:
	.ascii	"Station %s Soak-In Time from %d to %d min\000"
.LC490:
	.ascii	"Station %s Adjust Factor from %d%% to %d%%\000"
.LC491:
	.ascii	"Station %s Expected Flow Rate from %d to %d gpm\000"
.LC492:
	.ascii	"Station %s Distribution Uniformity from %d%% to %d%"
	.ascii	"%\000"
.LC493:
	.ascii	"Station %s No Water Days from %d to %d\000"
.LC494:
	.ascii	"Station %s Description\000"
.LC495:
	.ascii	"Station %s Assigned to Decoder S/N %07d\000"
.LC496:
	.ascii	"Station %s No Longer Assigned to Decoder S/N %07d\000"
.LC497:
	.ascii	"Station %s Decoder Assignment from S/N %d to %07d\000"
.LC498:
	.ascii	"Station %s Assigned to Decoder Output %c\000"
.LC499:
	.ascii	"Station %s Manual A Run Time from %d to %d min\000"
.LC500:
	.ascii	"Station %s Manual B Run Time from %d to %d min\000"
.LC501:
	.ascii	"Station %s Number from %d to %d\000"
.LC502:
	.ascii	"Station %d Controller Index from %d to %d\000"
.LC503:
	.ascii	"Station %s Moisture Balance from %.4f%% to %.4f%%\000"
.LC504:
	.ascii	"Station %s Square Footage from %u to %u\000"
.LC505:
	.ascii	"Station %s accumulated rain reset\000"
.LC506:
	.ascii	"Clock set from %s to %s\000"
.LC507:
	.ascii	"Time Zone from %s to %s\000"
.LC508:
	.ascii	"Start of Irrigation Day from %s to %s\000"
.LC509:
	.ascii	"SCHEDULED IRRIGATION TURNED OFF\000"
.LC510:
	.ascii	"SCHEDULED IRRIGATION TURNED ON\000"
.LC511:
	.ascii	"Soil Storage Capacity for %s from %0.2f in. to %0.2"
	.ascii	"f in.\000"
.LC512:
	.ascii	"Head Type for %s from %s to %s\000"
.LC513:
	.ascii	"Head Type Precipitation Rate for %s from %5.2f to %"
	.ascii	"5.2f in/hr\000"
.LC514:
	.ascii	"Soil Type for %s from %s to %s\000"
.LC515:
	.ascii	"Slope Percentage for %s from %s to %s\000"
.LC516:
	.ascii	"Plant Type for %s from %s to %s\000"
.LC517:
	.ascii	"%s Crop Coeffiecient for %s from %5.2f to %5.2f\000"
.LC518:
	.ascii	"Exposure for %s from %s to %s\000"
.LC519:
	.ascii	"Usable Rain for %s from %d%% to %d%%\000"
.LC520:
	.ascii	"Allowable Depletion for %s from %d to %d\000"
.LC521:
	.ascii	"Available Water for %s from %5.2f to %5.2f\000"
.LC522:
	.ascii	"Root Zone Depth for %s from %5.2f to %5.2f\000"
.LC523:
	.ascii	"Crop Species Factor for %s from %5.2f to %5.2f\000"
.LC524:
	.ascii	"Crop Density Factor for %s from %5.2f to %5.2f\000"
.LC525:
	.ascii	"Crop Microclimate Factor for %s from %5.2f to %5.2f"
	.ascii	"\000"
.LC526:
	.ascii	"Soil Intake Rate for %s from %5.2f to %5.2f\000"
.LC527:
	.ascii	"Allowable Surface Accumulation for %s from %5.2f to"
	.ascii	" %5.2f\000"
.LC528:
	.ascii	"Light Name to %s\000"
.LC529:
	.ascii	"Light Controller Address for %s from %c to %c\000"
.LC530:
	.ascii	"Light Output Index for %s from %d to %d\000"
.LC531:
	.ascii	"Light Physically Available for %s from %s to %s\000"
.LC532:
	.ascii	"Light: %s Day %d Start Time 1 from %s to %s\000"
.LC533:
	.ascii	"Light: %s Day %d Start Time 2 from %s to %s\000"
.LC534:
	.ascii	"Light: %s Day %d Stop Time 1 from %s to %s\000"
.LC535:
	.ascii	"Light: %s Day %d Stop Time 2 from %s to %s\000"
.LC536:
	.ascii	"MVOR Open Time on %s for %s from %s to %s\000"
.LC537:
	.ascii	"MVOR Close Time on %s for %s from %s to %s\000"
.LC538:
	.ascii	"Budget Use for %s from %s to %s\000"
.LC539:
	.ascii	"Budget Meter Read Time for %s from %s to %s\000"
.LC540:
	.ascii	"Budget Annual Periods to Use for %s from %d to %d\000"
.LC541:
	.ascii	"%s Budget Period %d Meter Read Date from %s to %s\000"
.LC542:
	.ascii	"Budget Mode for %s from %s to %s\000"
.LC543:
	.ascii	"Budget Reduction Limit for %s from %d to %d\000"
.LC544:
	.ascii	"Budget Entry Option for %s from %s to %s\000"
.LC545:
	.ascii	"Budget Calc Percent of ET for %s from %d to %d\000"
.LC546:
	.ascii	"%s: Budget Flow Type %s from %s to %s\000"
.LC547:
	.ascii	"%s Budget for Billing Period %d from %d gallons to "
	.ascii	"%d\000"
.LC548:
	.ascii	"%s Budget for Billing Period %d from %0.1f HCF to %"
	.ascii	"0.1f\000"
.LC549:
	.ascii	"POC %s has pump attached from %s to %s\000"
.LC550:
	.ascii	"Mainline %s deleted\000"
.LC551:
	.ascii	"Station Group %s deleted\000"
.LC552:
	.ascii	"Manual Program %s deleted\000"
.LC553:
	.ascii	"%s Mainline Assignment from %d to %d\000"
.LC554:
	.ascii	"Moisture Sensor Controller Index for %s from %d to "
	.ascii	"%d\000"
.LC555:
	.ascii	"Moisture Sensor Decoder Serial Number for %s from %"
	.ascii	"d to %d\000"
.LC556:
	.ascii	"Moisture Sensor Physically Available for %s from %s"
	.ascii	" to %s\000"
.LC557:
	.ascii	"Moisture Sensor In Use for %s from %s to %s\000"
.LC558:
	.ascii	"Moisture Sensor Irri Control Mode for %s from %s to"
	.ascii	" %s\000"
.LC559:
	.ascii	"Moisture Sensor Stop Irrigation Set Point for %s fr"
	.ascii	"om %d to %d\000"
.LC560:
	.ascii	"Moisture Sensor Allow Irrigation Set Point for %s f"
	.ascii	"rom %d to %d\000"
.LC561:
	.ascii	"Moisture Sensor Low Temperature Limit for %s from %"
	.ascii	"d to %d\000"
.LC562:
	.ascii	"Moisture Sensor Additional Soak Seconds for %s from"
	.ascii	" %d to %d\000"
.LC563:
	.ascii	"%s Moisture Sensor Used from Decoder S/N %d to %d\000"
.LC564:
	.ascii	"Moisture Sensor Stop Irrigation Set Point for %s fr"
	.ascii	"om %5.3f to %5.3f\000"
.LC565:
	.ascii	"Moisture Sensor Allow Irrigation Set Point for %s f"
	.ascii	"rom %5.3f to %5.3f\000"
.LC566:
	.ascii	"Moisture Sensor High Temperature Limit for %s from "
	.ascii	"%d to %d\000"
.LC567:
	.ascii	"Moisture Sensor High Temperature Action for %s from"
	.ascii	" %s to %s\000"
.LC568:
	.ascii	"Moisture Sensor Low Temperature Action for %s from "
	.ascii	"%s to %s\000"
.LC569:
	.ascii	"Moisture Sensor High Conductivity Limit for %s from"
	.ascii	" %5.3f to %5.3f\000"
.LC570:
	.ascii	"Moisture Sensor Low Conductivity Limit for %s from "
	.ascii	"%5.3f to %5.3f\000"
.LC571:
	.ascii	"Moisture Sensor High Conductivity Action for %s fro"
	.ascii	"m %s to %s\000"
.LC572:
	.ascii	"Moisture Sensor Low Conductivity Action for %s from"
	.ascii	" %s to %s\000"
.LC573:
	.ascii	"Use ET Averaging for %s from %s to %s\000"
.LC574:
	.ascii	"Acquire Expecteds for %s from %s to %s\000"
.LC575:
	.ascii	"POC %s Assigned to Decoder S/N %07d\000"
.LC576:
	.ascii	"POC %s No Longer Assigned to Decoder S/N %07d\000"
.LC577:
	.ascii	"POC %s Decoder Assignment from S/N %d to %07d\000"
.LC578:
	.ascii	"Controller Name\000"
.LC579:
	.ascii	"Controller Serial Number from %d to %d\000"
.LC580:
	.ascii	"Communication Address from %c to %c\000"
.LC581:
	.ascii	"Part of a FLOWSENSE Chain from %s to %s\000"
.LC582:
	.ascii	"Number of Controllers in Chain from %d to %d\000"
.LC583:
	.ascii	"Network ID from %d to %d\000"
.LC584:
	.ascii	"Controller %c Electrical Limit from %d to %d\000"
.LC585:
	.ascii	"Electrical Limit from %d to %d\000"
.LC586:
	.ascii	"Send Flow Recording from %s to %s\000"
.LC587:
	.ascii	"Water Units from %s to %s\000"
.LC588:
	.ascii	"This Controller Acts as Hub from %s to %s\000"
.LC589:
	.ascii	"Controller Connected to Rain Bucket from %c to %c\000"
.LC590:
	.ascii	"Rain Bucket Connected from %s to %s\000"
.LC591:
	.ascii	"Minimum Rain Needed to Stop Irrigation from %0.2f t"
	.ascii	"o %0.2f\000"
.LC592:
	.ascii	"Maximum Hourly Rain from %0.2f to %0.2f\000"
.LC593:
	.ascii	"Maximum 24 Hour Rain from %0.2f to %0.2f\000"
.LC594:
	.ascii	"Controller Connected to ET Gage from %c to %c\000"
.LC595:
	.ascii	"ET Gage Connected from %s to %s\000"
.LC596:
	.ascii	"Log ET Gage Pulses from %s to %s\000"
.LC597:
	.ascii	"ET Maximum Percent of Historical ET Number from %d%"
	.ascii	"% to %d%%\000"
.LC598:
	.ascii	"Controller Connected to Wind Gage from %c to %c\000"
.LC599:
	.ascii	"Wind Gage Connected from %s to %s\000"
.LC600:
	.ascii	"Wind Pause Speed from %d mph to %d mph\000"
.LC601:
	.ascii	"Wind Resume Speed from %d mph to %d mph\000"
.LC602:
	.ascii	"Use Your Own Reference ET Numbers from %s to %s\000"
.LC603:
	.ascii	"%s Reference ET Number from %0.2f to %0.2f\000"
.LC604:
	.ascii	"Reference ET State from %s to %s\000"
.LC605:
	.ascii	"Reference ET County from %s to %s\000"
.LC606:
	.ascii	"Reference ET City from %s to %s\000"
.LC607:
	.ascii	"Reference ET State\000"
.LC608:
	.ascii	"Reference ET County\000"
.LC609:
	.ascii	"Reference ET City\000"
.LC610:
	.ascii	"Rain Switch Name\000"
.LC611:
	.ascii	"Rain Switch Connected from %s to %s\000"
.LC612:
	.ascii	"Rain Switch Contact Type from %s to %s\000"
.LC613:
	.ascii	"Rain Switch Stops Irrigation from %s to %s\000"
.LC614:
	.ascii	"Rain Switch Affects\000"
.LC615:
	.ascii	"Freeze Switch Name\000"
.LC616:
	.ascii	"Freeze Switch Connected from %s to %s\000"
.LC617:
	.ascii	"Freeze Switch Contact Type from %s to %s\000"
.LC618:
	.ascii	"Freeze Switch Stops Irrigation from %s to %s\000"
.LC619:
	.ascii	"Freeze Switch Affects\000"
.LC620:
	.ascii	"ET Minimum Percent of Historical ET Number from %d%"
	.ascii	"% to %d%%\000"
.LC621:
	.ascii	"ET to %0.2f inches\000"
.LC622:
	.ascii	"Rain to %0.2f inches\000"
.LC623:
	.ascii	"Station Run Time from %0.1f min to %0.1f min\000"
.LC624:
	.ascii	"Delay Before Start from %d seconds to %d seconds\000"
.LC625:
	.ascii	"Controller used in Walk Thru from %c to %c\000"
.LC626:
	.ascii	"Unknown change line %d\000"
.LC627:
	.ascii	" (Controller %c)\000"
.LC628:
	.ascii	"UNPARSED LINE TYPE %05d\000"
.LC629:
	.ascii	"(integrity test failed)\000"
.LC630:
	.ascii	"(by request)\000"
.LC0:
	.ascii	"Clearing obsolete\000"
.LC1:
	.ascii	"System not found - registering\000"
.LC2:
	.ascii	"System found - refreshing\000"
.LC4:
	.ascii	"POC not found - registering\000"
.LC5:
	.ascii	"POC found - refreshing\000"
.LC631:
	.ascii	"down\000"
.LC632:
	.ascii	"up\000"
.LC633:
	.ascii	"idle\000"
.LC634:
	.ascii	"waiting\000"
.LC635:
	.ascii	"transmitting\000"
.LC636:
	.ascii	"Controller OFF\000"
.LC637:
	.ascii	"Mainline Break\000"
.LC638:
	.ascii	"No Water Day\000"
.LC639:
	.ascii	"Calendar No Water Day\000"
.LC640:
	.ascii	"Master Valve Closed in Effect\000"
.LC641:
	.ascii	"Rain in the Rain Table\000"
.LC642:
	.ascii	"Accumulated Rain\000"
.LC643:
	.ascii	"Rain Switch\000"
.LC644:
	.ascii	"Freeze Switch\000"
.LC645:
	.ascii	"Wind\000"
.LC646:
	.ascii	"Cycle Too Short\000"
.LC647:
	.ascii	"Stop Time SAME AS Start Time!\000"
.LC648:
	.ascii	"Station Quantity\000"
.LC649:
	.ascii	"Mow Day\000"
.LC650:
	.ascii	"2-Wire Cable Overheated\000"
.LC651:
	.ascii	"Station Not In Use\000"
.LC652:
	.ascii	"No Close Time\000"
.LC653:
	.ascii	"Open and Close Times the Same\000"
.LC654:
	.ascii	"Close Time is Before Open Time\000"
.LC655:
	.ascii	"HUB\000"
.LC656:
	.ascii	"On a Hub\000"
.LC657:
	.ascii	"Non-Hub Master\000"
.LC658:
	.ascii	"Slave\000"
.LC659:
	.ascii	"RRe\000"
.LC660:
	.ascii	"startup\000"
.LC661:
	.ascii	"keypad\000"
.LC662:
	.ascii	"rescan\000"
.LC663:
	.ascii	"slave requested\000"
.LC664:
	.ascii	" (keypad)\000"
.LC665:
	.ascii	" (comm)\000"
.LC666:
	.ascii	" (direct access)\000"
.LC667:
	.ascii	" (mv short)\000"
.LC668:
	.ascii	" (scheduled)\000"
.LC669:
	.ascii	" (mainline break)\000"
.LC670:
	.ascii	" (system timeout)\000"
.LC671:
	.ascii	" (mobile)\000"
.LC672:
	.ascii	" (UNK REASON)\000"
.LC673:
	.ascii	"START:\000"
.LC674:
	.ascii	"END:\000"
.LC675:
	.ascii	"START: Manual\000"
.LC676:
	.ascii	"END: Manual\000"
.LC677:
	.ascii	"START: Mobile\000"
.LC678:
	.ascii	"END: Mobile\000"
.LC679:
	.ascii	"No scheduled Stop Time!\000"
.LC680:
	.ascii	"\000"
.LC681:
	.ascii	"removed\000"
.LC682:
	.ascii	"installed\000"
	.section	.rodata.lights_text_str,"a",%progbits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	lights_text_str, %object
	.size	lights_text_str, 64
lights_text_str:
	.word	.LC673
	.word	.LC674
	.word	.LC675
	.word	.LC676
	.word	.LC677
	.word	.LC678
	.word	.LC679
	.word	.LC647
	.word	.LC680
	.word	.LC680
	.word	.LC680
	.word	.LC680
	.word	.LC680
	.word	.LC680
	.word	.LC680
	.word	.LC680
	.section	.rodata.on_off_str.8607,"a",%progbits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	on_off_str.8607, %object
	.size	on_off_str.8607, 8
on_off_str.8607:
	.word	.LC631
	.word	.LC632
	.section	.data.TEN.9488,"aw",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	TEN.9488, %object
	.size	TEN.9488, 4
TEN.9488:
	.word	1092616192
	.section	.rodata.irrigation_skipped_reason_str,"a",%progbits
	.align	2
	.set	.LANCHOR10,. + 0
	.type	irrigation_skipped_reason_str, %object
	.size	irrigation_skipped_reason_str, 80
irrigation_skipped_reason_str:
	.word	.LC636
	.word	.LC637
	.word	.LC638
	.word	.LC639
	.word	.LC640
	.word	.LC641
	.word	.LC642
	.word	.LC643
	.word	.LC644
	.word	.LC645
	.word	.LC646
	.word	.LC647
	.word	.LC648
	.word	.LC649
	.word	.LC650
	.word	.LC651
	.word	.LC642
	.word	.LC652
	.word	.LC653
	.word	.LC654
	.section	.data.SECONDS_PER_HOUR.9283,"aw",%progbits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	SECONDS_PER_HOUR.9283, %object
	.size	SECONDS_PER_HOUR.9283, 4
SECONDS_PER_HOUR.9283:
	.word	1163984896
	.section	.rodata.router_type_str,"a",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	router_type_str, %object
	.size	router_type_str, 20
router_type_str:
	.word	.LC655
	.word	.LC656
	.word	.LC657
	.word	.LC658
	.word	.LC659
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB217
	.4byte	.LFE217-.LFB217
	.byte	0x4
	.4byte	.LCFI3-.LFB217
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x9c
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB207
	.4byte	.LFE207-.LFB207
	.byte	0x4
	.4byte	.LCFI5-.LFB207
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB205
	.4byte	.LFE205-.LFB205
	.byte	0x4
	.4byte	.LCFI7-.LFB205
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x6c
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB204
	.4byte	.LFE204-.LFB204
	.byte	0x4
	.4byte	.LCFI9-.LFB204
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB202
	.4byte	.LFE202-.LFB202
	.byte	0x4
	.4byte	.LCFI11-.LFB202
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB201
	.4byte	.LFE201-.LFB201
	.byte	0x4
	.4byte	.LCFI13-.LFB201
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB200
	.4byte	.LFE200-.LFB200
	.byte	0x4
	.4byte	.LCFI15-.LFB200
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB199
	.4byte	.LFE199-.LFB199
	.byte	0x4
	.4byte	.LCFI17-.LFB199
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB198
	.4byte	.LFE198-.LFB198
	.byte	0x4
	.4byte	.LCFI19-.LFB198
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB195
	.4byte	.LFE195-.LFB195
	.byte	0x4
	.4byte	.LCFI20-.LFB195
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB193
	.4byte	.LFE193-.LFB193
	.byte	0x4
	.4byte	.LCFI22-.LFB193
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB192
	.4byte	.LFE192-.LFB192
	.byte	0x4
	.4byte	.LCFI24-.LFB192
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB191
	.4byte	.LFE191-.LFB191
	.byte	0x4
	.4byte	.LCFI25-.LFB191
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB190
	.4byte	.LFE190-.LFB190
	.byte	0x4
	.4byte	.LCFI26-.LFB190
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB189
	.4byte	.LFE189-.LFB189
	.byte	0x4
	.4byte	.LCFI27-.LFB189
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB188
	.4byte	.LFE188-.LFB188
	.byte	0x4
	.4byte	.LCFI28-.LFB188
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB187
	.4byte	.LFE187-.LFB187
	.byte	0x4
	.4byte	.LCFI30-.LFB187
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB186
	.4byte	.LFE186-.LFB186
	.byte	0x4
	.4byte	.LCFI32-.LFB186
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB183
	.4byte	.LFE183-.LFB183
	.byte	0x4
	.4byte	.LCFI34-.LFB183
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB182
	.4byte	.LFE182-.LFB182
	.byte	0x4
	.4byte	.LCFI35-.LFB182
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB181
	.4byte	.LFE181-.LFB181
	.byte	0x4
	.4byte	.LCFI37-.LFB181
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB180
	.4byte	.LFE180-.LFB180
	.byte	0x4
	.4byte	.LCFI39-.LFB180
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB179
	.4byte	.LFE179-.LFB179
	.byte	0x4
	.4byte	.LCFI41-.LFB179
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI42-.LCFI41
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB174
	.4byte	.LFE174-.LFB174
	.byte	0x4
	.4byte	.LCFI43-.LFB174
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB173
	.4byte	.LFE173-.LFB173
	.byte	0x4
	.4byte	.LCFI44-.LFB173
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB172
	.4byte	.LFE172-.LFB172
	.byte	0x4
	.4byte	.LCFI45-.LFB172
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB171
	.4byte	.LFE171-.LFB171
	.byte	0x4
	.4byte	.LCFI46-.LFB171
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB170
	.4byte	.LFE170-.LFB170
	.byte	0x4
	.4byte	.LCFI47-.LFB170
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB169
	.4byte	.LFE169-.LFB169
	.byte	0x4
	.4byte	.LCFI48-.LFB169
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB168
	.4byte	.LFE168-.LFB168
	.byte	0x4
	.4byte	.LCFI49-.LFB168
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB167
	.4byte	.LFE167-.LFB167
	.byte	0x4
	.4byte	.LCFI50-.LFB167
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB166
	.4byte	.LFE166-.LFB166
	.byte	0x4
	.4byte	.LCFI51-.LFB166
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB165
	.4byte	.LFE165-.LFB165
	.byte	0x4
	.4byte	.LCFI52-.LFB165
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB164
	.4byte	.LFE164-.LFB164
	.byte	0x4
	.4byte	.LCFI53-.LFB164
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI54-.LFB3
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB225
	.4byte	.LFE225-.LFB225
	.byte	0x4
	.4byte	.LCFI55-.LFB225
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI56-.LCFI55
	.byte	0xe
	.uleb128 0x6c
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB224
	.4byte	.LFE224-.LFB224
	.byte	0x4
	.4byte	.LCFI57-.LFB224
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI58-.LCFI57
	.byte	0xe
	.uleb128 0xa8
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB223
	.4byte	.LFE223-.LFB223
	.byte	0x4
	.4byte	.LCFI59-.LFB223
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI60-.LCFI59
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB222
	.4byte	.LFE222-.LFB222
	.byte	0x4
	.4byte	.LCFI61-.LFB222
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI62-.LCFI61
	.byte	0xe
	.uleb128 0xf0
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB221
	.4byte	.LFE221-.LFB221
	.byte	0x4
	.4byte	.LCFI63-.LFB221
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI64-.LCFI63
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB220
	.4byte	.LFE220-.LFB220
	.byte	0x4
	.4byte	.LCFI65-.LFB220
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI66-.LCFI65
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB219
	.4byte	.LFE219-.LFB219
	.byte	0x4
	.4byte	.LCFI67-.LFB219
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI68-.LCFI67
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB216
	.4byte	.LFE216-.LFB216
	.byte	0x4
	.4byte	.LCFI69-.LFB216
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI70-.LCFI69
	.byte	0xe
	.uleb128 0xd8
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB215
	.4byte	.LFE215-.LFB215
	.byte	0x4
	.4byte	.LCFI71-.LFB215
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI72-.LCFI71
	.byte	0xe
	.uleb128 0xf4
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB214
	.4byte	.LFE214-.LFB214
	.byte	0x4
	.4byte	.LCFI73-.LFB214
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI74-.LCFI73
	.byte	0xe
	.uleb128 0xe4
	.align	2
.LEFDE94:
.LSFDE96:
	.4byte	.LEFDE96-.LASFDE96
.LASFDE96:
	.4byte	.Lframe0
	.4byte	.LFB213
	.4byte	.LFE213-.LFB213
	.byte	0x4
	.4byte	.LCFI75-.LFB213
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI76-.LCFI75
	.byte	0xe
	.uleb128 0xdc
	.align	2
.LEFDE96:
.LSFDE98:
	.4byte	.LEFDE98-.LASFDE98
.LASFDE98:
	.4byte	.Lframe0
	.4byte	.LFB212
	.4byte	.LFE212-.LFB212
	.byte	0x4
	.4byte	.LCFI77-.LFB212
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI78-.LCFI77
	.byte	0xe
	.uleb128 0xc8
	.align	2
.LEFDE98:
.LSFDE100:
	.4byte	.LEFDE100-.LASFDE100
.LASFDE100:
	.4byte	.Lframe0
	.4byte	.LFB211
	.4byte	.LFE211-.LFB211
	.byte	0x4
	.4byte	.LCFI79-.LFB211
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI80-.LCFI79
	.byte	0xe
	.uleb128 0xf4
	.align	2
.LEFDE100:
.LSFDE102:
	.4byte	.LEFDE102-.LASFDE102
.LASFDE102:
	.4byte	.Lframe0
	.4byte	.LFB210
	.4byte	.LFE210-.LFB210
	.byte	0x4
	.4byte	.LCFI81-.LFB210
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI82-.LCFI81
	.byte	0xe
	.uleb128 0xdc
	.align	2
.LEFDE102:
.LSFDE104:
	.4byte	.LEFDE104-.LASFDE104
.LASFDE104:
	.4byte	.Lframe0
	.4byte	.LFB209
	.4byte	.LFE209-.LFB209
	.byte	0x4
	.4byte	.LCFI83-.LFB209
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI84-.LCFI83
	.byte	0xe
	.uleb128 0xac
	.align	2
.LEFDE104:
.LSFDE106:
	.4byte	.LEFDE106-.LASFDE106
.LASFDE106:
	.4byte	.Lframe0
	.4byte	.LFB206
	.4byte	.LFE206-.LFB206
	.byte	0x4
	.4byte	.LCFI85-.LFB206
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI86-.LCFI85
	.byte	0xe
	.uleb128 0x90
	.align	2
.LEFDE106:
.LSFDE108:
	.4byte	.LEFDE108-.LASFDE108
.LASFDE108:
	.4byte	.Lframe0
	.4byte	.LFB203
	.4byte	.LFE203-.LFB203
	.byte	0x4
	.4byte	.LCFI87-.LFB203
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI88-.LCFI87
	.byte	0xe
	.uleb128 0x68
	.align	2
.LEFDE108:
.LSFDE110:
	.4byte	.LEFDE110-.LASFDE110
.LASFDE110:
	.4byte	.Lframe0
	.4byte	.LFB197
	.4byte	.LFE197-.LFB197
	.byte	0x4
	.4byte	.LCFI89-.LFB197
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI90-.LCFI89
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE110:
.LSFDE112:
	.4byte	.LEFDE112-.LASFDE112
.LASFDE112:
	.4byte	.Lframe0
	.4byte	.LFB196
	.4byte	.LFE196-.LFB196
	.byte	0x4
	.4byte	.LCFI91-.LFB196
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI92-.LCFI91
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE112:
.LSFDE114:
	.4byte	.LEFDE114-.LASFDE114
.LASFDE114:
	.4byte	.Lframe0
	.4byte	.LFB194
	.4byte	.LFE194-.LFB194
	.byte	0x4
	.4byte	.LCFI93-.LFB194
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI94-.LCFI93
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE114:
.LSFDE116:
	.4byte	.LEFDE116-.LASFDE116
.LASFDE116:
	.4byte	.Lframe0
	.4byte	.LFB185
	.4byte	.LFE185-.LFB185
	.byte	0x4
	.4byte	.LCFI95-.LFB185
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI96-.LCFI95
	.byte	0xe
	.uleb128 0xd0
	.align	2
.LEFDE116:
.LSFDE118:
	.4byte	.LEFDE118-.LASFDE118
.LASFDE118:
	.4byte	.Lframe0
	.4byte	.LFB184
	.4byte	.LFE184-.LFB184
	.byte	0x4
	.4byte	.LCFI97-.LFB184
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI98-.LCFI97
	.byte	0xe
	.uleb128 0xd0
	.align	2
.LEFDE118:
.LSFDE120:
	.4byte	.LEFDE120-.LASFDE120
.LASFDE120:
	.4byte	.Lframe0
	.4byte	.LFB178
	.4byte	.LFE178-.LFB178
	.byte	0x4
	.4byte	.LCFI99-.LFB178
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI100-.LCFI99
	.byte	0xe
	.uleb128 0x78
	.align	2
.LEFDE120:
.LSFDE122:
	.4byte	.LEFDE122-.LASFDE122
.LASFDE122:
	.4byte	.Lframe0
	.4byte	.LFB177
	.4byte	.LFE177-.LFB177
	.byte	0x4
	.4byte	.LCFI101-.LFB177
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI102-.LCFI101
	.byte	0xe
	.uleb128 0x78
	.align	2
.LEFDE122:
.LSFDE124:
	.4byte	.LEFDE124-.LASFDE124
.LASFDE124:
	.4byte	.Lframe0
	.4byte	.LFB176
	.4byte	.LFE176-.LFB176
	.byte	0x4
	.4byte	.LCFI103-.LFB176
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI104-.LCFI103
	.byte	0xe
	.uleb128 0x78
	.align	2
.LEFDE124:
.LSFDE126:
	.4byte	.LEFDE126-.LASFDE126
.LASFDE126:
	.4byte	.Lframe0
	.4byte	.LFB175
	.4byte	.LFE175-.LFB175
	.byte	0x4
	.4byte	.LCFI105-.LFB175
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI106-.LCFI105
	.byte	0xe
	.uleb128 0xbc
	.align	2
.LEFDE126:
.LSFDE128:
	.4byte	.LEFDE128-.LASFDE128
.LASFDE128:
	.4byte	.Lframe0
	.4byte	.LFB163
	.4byte	.LFE163-.LFB163
	.byte	0x4
	.4byte	.LCFI107-.LFB163
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI108-.LCFI107
	.byte	0xe
	.uleb128 0x84
	.align	2
.LEFDE128:
.LSFDE130:
	.4byte	.LEFDE130-.LASFDE130
.LASFDE130:
	.4byte	.Lframe0
	.4byte	.LFB162
	.4byte	.LFE162-.LFB162
	.byte	0x4
	.4byte	.LCFI109-.LFB162
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI110-.LCFI109
	.byte	0xe
	.uleb128 0x84
	.align	2
.LEFDE130:
.LSFDE132:
	.4byte	.LEFDE132-.LASFDE132
.LASFDE132:
	.4byte	.Lframe0
	.4byte	.LFB161
	.4byte	.LFE161-.LFB161
	.byte	0x4
	.4byte	.LCFI111-.LFB161
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI112-.LCFI111
	.byte	0xe
	.uleb128 0x84
	.align	2
.LEFDE132:
.LSFDE134:
	.4byte	.LEFDE134-.LASFDE134
.LASFDE134:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.align	2
.LEFDE134:
.LSFDE136:
	.4byte	.LEFDE136-.LASFDE136
.LASFDE136:
	.4byte	.Lframe0
	.4byte	.LFB159
	.4byte	.LFE159-.LFB159
	.byte	0x4
	.4byte	.LCFI113-.LFB159
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI114-.LCFI113
	.byte	0xe
	.uleb128 0x214
	.align	2
.LEFDE136:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/alerts/alert_parsing.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa37
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF159
	.byte	0x1
	.4byte	.LASF160
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF161
	.byte	0x1
	.byte	0xcb
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x293
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x25f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x279
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x2ad
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x2e7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x2f9
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x30b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x326
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x341
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x35c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x395
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x3a7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x3bc
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x3d1
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x3e5
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x3fa
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x40f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x44d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x462
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x48f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x4a1
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x4b3
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x4c5
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x4da
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x50a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x53d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x54f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x561
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x582
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x7c4
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x805
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x81f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x839
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x859
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x86b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x887
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x899
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x8ac
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x9fc
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF39
	.byte	0x1
	.2byte	0xadd
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF40
	.byte	0x1
	.2byte	0xb79
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF41
	.byte	0x1
	.2byte	0xb47
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF42
	.byte	0x1
	.2byte	0xb62
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF43
	.byte	0x1
	.2byte	0xba4
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF44
	.byte	0x1
	.2byte	0xbbd
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF45
	.byte	0x1
	.2byte	0xbd7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF46
	.byte	0x1
	.2byte	0xbf1
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF47
	.byte	0x1
	.2byte	0xc87
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF48
	.byte	0x1
	.2byte	0xca0
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF49
	.byte	0x1
	.2byte	0xcb9
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x122c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x1252
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x126c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x1288
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF54
	.byte	0x1
	.2byte	0xd33
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF55
	.byte	0x1
	.2byte	0xda0
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF56
	.byte	0x1
	.2byte	0xdb8
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF57
	.byte	0x1
	.2byte	0xe03
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF58
	.byte	0x1
	.2byte	0xe18
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF59
	.byte	0x1
	.2byte	0xe2a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF60
	.byte	0x1
	.2byte	0xe3f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF61
	.byte	0x1
	.2byte	0xe58
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF62
	.byte	0x1
	.2byte	0xe6a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF63
	.byte	0x1
	.2byte	0xe87
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF64
	.byte	0x1
	.2byte	0xea4
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF65
	.byte	0x1
	.2byte	0xebc
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF66
	.byte	0x1
	.2byte	0xed1
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF67
	.byte	0x1
	.2byte	0xf29
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF68
	.byte	0x1
	.2byte	0xf89
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF69
	.byte	0x1
	.2byte	0xfa1
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF70
	.byte	0x1
	.2byte	0xfb3
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF71
	.byte	0x1
	.2byte	0xfd1
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x1098
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x10b1
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x10c3
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x10d5
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x1158
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x117d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x116a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x130b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x12c0
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x1328
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x1341
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x1374
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x13a0
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x13b5
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x17ca
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x17e6
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x1b10
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x1adc
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x1aa8
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x1a8c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x1a70
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x1a54
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x1a38
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x1a1c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x19fd
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x19e1
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x19c5
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x19a9
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x198d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x196e
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x193b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x190a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x18d9
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x18a8
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x187b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x184e
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x1821
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x1802
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x1456
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x143b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x1421
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x1386
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x135a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x12dd
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x129b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x11af
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x118f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x1132
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x10e7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x1058
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF122
	.byte	0x1
	.2byte	0xf71
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF123
	.byte	0x1
	.2byte	0xf06
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF124
	.byte	0x1
	.2byte	0xee7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF125
	.byte	0x1
	.2byte	0xdeb
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF126
	.byte	0x1
	.2byte	0xdd0
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF127
	.byte	0x1
	.2byte	0xd85
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF128
	.byte	0x1
	.2byte	0xd6a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF129
	.byte	0x1
	.2byte	0xd4f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF130
	.byte	0x1
	.2byte	0xd09
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF131
	.byte	0x1
	.2byte	0xcd2
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF132
	.byte	0x1
	.2byte	0xc20
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF133
	.byte	0x1
	.2byte	0xb09
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF134
	.byte	0x1
	.2byte	0xac2
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF135
	.byte	0x1
	.2byte	0xa9d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF136
	.byte	0x1
	.2byte	0xa7a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF137
	.byte	0x1
	.2byte	0xa46
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF138
	.byte	0x1
	.2byte	0xa11
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF139
	.byte	0x1
	.2byte	0x9d8
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF140
	.byte	0x1
	.2byte	0x9a4
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF141
	.byte	0x1
	.2byte	0x970
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF142
	.byte	0x1
	.2byte	0x93b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF143
	.byte	0x1
	.2byte	0x907
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF144
	.byte	0x1
	.2byte	0x8bf
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF145
	.byte	0x1
	.2byte	0x7d7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF146
	.byte	0x1
	.2byte	0x51f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF147
	.byte	0x1
	.2byte	0x4ec
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF148
	.byte	0x1
	.2byte	0x474
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF149
	.byte	0x1
	.2byte	0x424
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF150
	.byte	0x1
	.2byte	0x377
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF151
	.byte	0x1
	.2byte	0x2c7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF152
	.byte	0x1
	.2byte	0x1b44
	.byte	0x1
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF153
	.byte	0x1
	.byte	0xe7
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x1c6
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	0x53a
	.4byte	.LFB217
	.4byte	.LFE217
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	0x4e0
	.4byte	.LFB207
	.4byte	.LFE207
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	0x4ce
	.4byte	.LFB205
	.4byte	.LFE205
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	0x4c5
	.4byte	.LFB204
	.4byte	.LFE204
	.4byte	.LLST5
	.uleb128 0x4
	.4byte	0x4b3
	.4byte	.LFB202
	.4byte	.LFE202
	.4byte	.LLST6
	.uleb128 0x4
	.4byte	0x4aa
	.4byte	.LFB201
	.4byte	.LFE201
	.4byte	.LLST7
	.uleb128 0x4
	.4byte	0x4a1
	.4byte	.LFB200
	.4byte	.LFE200
	.4byte	.LLST8
	.uleb128 0x4
	.4byte	0x498
	.4byte	.LFB199
	.4byte	.LFE199
	.4byte	.LLST9
	.uleb128 0x4
	.4byte	0x48f
	.4byte	.LFB198
	.4byte	.LFE198
	.4byte	.LLST10
	.uleb128 0x4
	.4byte	0x474
	.4byte	.LFB195
	.4byte	.LFE195
	.4byte	.LLST11
	.uleb128 0x4
	.4byte	0x462
	.4byte	.LFB193
	.4byte	.LFE193
	.4byte	.LLST12
	.uleb128 0x4
	.4byte	0x459
	.4byte	.LFB192
	.4byte	.LFE192
	.4byte	.LLST13
	.uleb128 0x4
	.4byte	0x450
	.4byte	.LFB191
	.4byte	.LFE191
	.4byte	.LLST14
	.uleb128 0x4
	.4byte	0x447
	.4byte	.LFB190
	.4byte	.LFE190
	.4byte	.LLST15
	.uleb128 0x4
	.4byte	0x43e
	.4byte	.LFB189
	.4byte	.LFE189
	.4byte	.LLST16
	.uleb128 0x4
	.4byte	0x435
	.4byte	.LFB188
	.4byte	.LFE188
	.4byte	.LLST17
	.uleb128 0x4
	.4byte	0x42c
	.4byte	.LFB187
	.4byte	.LFE187
	.4byte	.LLST18
	.uleb128 0x4
	.4byte	0x423
	.4byte	.LFB186
	.4byte	.LFE186
	.4byte	.LLST19
	.uleb128 0x4
	.4byte	0x408
	.4byte	.LFB183
	.4byte	.LFE183
	.4byte	.LLST20
	.uleb128 0x4
	.4byte	0x3ff
	.4byte	.LFB182
	.4byte	.LFE182
	.4byte	.LLST21
	.uleb128 0x4
	.4byte	0x3f6
	.4byte	.LFB181
	.4byte	.LFE181
	.4byte	.LLST22
	.uleb128 0x4
	.4byte	0x3ed
	.4byte	.LFB180
	.4byte	.LFE180
	.4byte	.LLST23
	.uleb128 0x4
	.4byte	0x3e4
	.4byte	.LFB179
	.4byte	.LFE179
	.4byte	.LLST24
	.uleb128 0x4
	.4byte	0x3b7
	.4byte	.LFB174
	.4byte	.LFE174
	.4byte	.LLST25
	.uleb128 0x4
	.4byte	0x3ae
	.4byte	.LFB173
	.4byte	.LFE173
	.4byte	.LLST26
	.uleb128 0x4
	.4byte	0x3a5
	.4byte	.LFB172
	.4byte	.LFE172
	.4byte	.LLST27
	.uleb128 0x4
	.4byte	0x39c
	.4byte	.LFB171
	.4byte	.LFE171
	.4byte	.LLST28
	.uleb128 0x4
	.4byte	0x393
	.4byte	.LFB170
	.4byte	.LFE170
	.4byte	.LLST29
	.uleb128 0x4
	.4byte	0x38a
	.4byte	.LFB169
	.4byte	.LFE169
	.4byte	.LLST30
	.uleb128 0x4
	.4byte	0x381
	.4byte	.LFB168
	.4byte	.LFE168
	.4byte	.LLST31
	.uleb128 0x4
	.4byte	0x378
	.4byte	.LFB167
	.4byte	.LFE167
	.4byte	.LLST32
	.uleb128 0x4
	.4byte	0x36f
	.4byte	.LFB166
	.4byte	.LFE166
	.4byte	.LLST33
	.uleb128 0x4
	.4byte	0x366
	.4byte	.LFB165
	.4byte	.LFE165
	.4byte	.LLST34
	.uleb128 0x4
	.4byte	0x35d
	.4byte	.LFB164
	.4byte	.LFE164
	.4byte	.LLST35
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x20b
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST36
	.uleb128 0x4
	.4byte	0x582
	.4byte	.LFB225
	.4byte	.LFE225
	.4byte	.LLST37
	.uleb128 0x4
	.4byte	0x579
	.4byte	.LFB224
	.4byte	.LFE224
	.4byte	.LLST38
	.uleb128 0x4
	.4byte	0x570
	.4byte	.LFB223
	.4byte	.LFE223
	.4byte	.LLST39
	.uleb128 0x4
	.4byte	0x567
	.4byte	.LFB222
	.4byte	.LFE222
	.4byte	.LLST40
	.uleb128 0x4
	.4byte	0x55e
	.4byte	.LFB221
	.4byte	.LFE221
	.4byte	.LLST41
	.uleb128 0x4
	.4byte	0x555
	.4byte	.LFB220
	.4byte	.LFE220
	.4byte	.LLST42
	.uleb128 0x4
	.4byte	0x54c
	.4byte	.LFB219
	.4byte	.LFE219
	.4byte	.LLST43
	.uleb128 0x4
	.4byte	0x531
	.4byte	.LFB216
	.4byte	.LFE216
	.4byte	.LLST44
	.uleb128 0x4
	.4byte	0x528
	.4byte	.LFB215
	.4byte	.LFE215
	.4byte	.LLST45
	.uleb128 0x4
	.4byte	0x51f
	.4byte	.LFB214
	.4byte	.LFE214
	.4byte	.LLST46
	.uleb128 0x4
	.4byte	0x516
	.4byte	.LFB213
	.4byte	.LFE213
	.4byte	.LLST47
	.uleb128 0x4
	.4byte	0x50d
	.4byte	.LFB212
	.4byte	.LFE212
	.4byte	.LLST48
	.uleb128 0x4
	.4byte	0x504
	.4byte	.LFB211
	.4byte	.LFE211
	.4byte	.LLST49
	.uleb128 0x4
	.4byte	0x4fb
	.4byte	.LFB210
	.4byte	.LFE210
	.4byte	.LLST50
	.uleb128 0x4
	.4byte	0x4f2
	.4byte	.LFB209
	.4byte	.LFE209
	.4byte	.LLST51
	.uleb128 0x4
	.4byte	0x4d7
	.4byte	.LFB206
	.4byte	.LFE206
	.4byte	.LLST52
	.uleb128 0x4
	.4byte	0x4bc
	.4byte	.LFB203
	.4byte	.LFE203
	.4byte	.LLST53
	.uleb128 0x4
	.4byte	0x486
	.4byte	.LFB197
	.4byte	.LFE197
	.4byte	.LLST54
	.uleb128 0x4
	.4byte	0x47d
	.4byte	.LFB196
	.4byte	.LFE196
	.4byte	.LLST55
	.uleb128 0x4
	.4byte	0x46b
	.4byte	.LFB194
	.4byte	.LFE194
	.4byte	.LLST56
	.uleb128 0x4
	.4byte	0x41a
	.4byte	.LFB185
	.4byte	.LFE185
	.4byte	.LLST57
	.uleb128 0x4
	.4byte	0x411
	.4byte	.LFB184
	.4byte	.LFE184
	.4byte	.LLST58
	.uleb128 0x4
	.4byte	0x3db
	.4byte	.LFB178
	.4byte	.LFE178
	.4byte	.LLST59
	.uleb128 0x4
	.4byte	0x3d2
	.4byte	.LFB177
	.4byte	.LFE177
	.4byte	.LLST60
	.uleb128 0x4
	.4byte	0x3c9
	.4byte	.LFB176
	.4byte	.LFE176
	.4byte	.LLST61
	.uleb128 0x4
	.4byte	0x3c0
	.4byte	.LFB175
	.4byte	.LFE175
	.4byte	.LLST62
	.uleb128 0x4
	.4byte	0x354
	.4byte	.LFB163
	.4byte	.LFE163
	.4byte	.LLST63
	.uleb128 0x4
	.4byte	0x34b
	.4byte	.LFB162
	.4byte	.LFE162
	.4byte	.LLST64
	.uleb128 0x4
	.4byte	0x342
	.4byte	.LFB161
	.4byte	.LFE161
	.4byte	.LLST65
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF156
	.byte	0x1
	.2byte	0x5a3
	.4byte	.LFB39
	.4byte	.LFE39
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF157
	.byte	0x1
	.2byte	0x1b99
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF158
	.byte	0x1
	.2byte	0x20d2
	.4byte	.LFB159
	.4byte	.LFE159
	.4byte	.LLST66
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB217
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI4
	.4byte	.LFE217
	.2byte	0x3
	.byte	0x7d
	.sleb128 156
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB207
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI6
	.4byte	.LFE207
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB205
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI8
	.4byte	.LFE205
	.2byte	0x3
	.byte	0x7d
	.sleb128 108
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB204
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI10
	.4byte	.LFE204
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB202
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI12
	.4byte	.LFE202
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB201
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI14
	.4byte	.LFE201
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB200
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI16
	.4byte	.LFE200
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB199
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI18
	.4byte	.LFE199
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB198
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE198
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB195
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI21
	.4byte	.LFE195
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB193
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI23
	.4byte	.LFE193
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB192
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE192
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB191
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE191
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB190
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LFE190
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB189
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LFE189
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB188
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI29
	.4byte	.LFE188
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB187
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI31
	.4byte	.LFE187
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB186
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI33
	.4byte	.LFE186
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB183
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LFE183
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB182
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI36
	.4byte	.LFE182
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB181
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI38
	.4byte	.LFE181
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB180
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI40
	.4byte	.LFE180
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB179
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI42
	.4byte	.LFE179
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB174
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LFE174
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB173
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LFE173
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB172
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LFE172
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB171
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LFE171
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB170
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LFE170
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB169
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LFE169
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB168
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI49
	.4byte	.LFE168
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB167
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LFE167
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB166
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LFE166
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB165
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI52
	.4byte	.LFE165
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB164
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LFE164
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB3
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB225
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI55
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI56
	.4byte	.LFE225
	.2byte	0x3
	.byte	0x7d
	.sleb128 108
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB224
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI58
	.4byte	.LFE224
	.2byte	0x3
	.byte	0x7d
	.sleb128 168
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB223
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI60
	.4byte	.LFE223
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB222
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI61
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI62
	.4byte	.LFE222
	.2byte	0x3
	.byte	0x7d
	.sleb128 240
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB221
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI64
	.4byte	.LFE221
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB220
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI65
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI66
	.4byte	.LFE220
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB219
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI67
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI68
	.4byte	.LFE219
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB216
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI69
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI70
	.4byte	.LFE216
	.2byte	0x3
	.byte	0x7d
	.sleb128 216
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB215
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI71
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI72
	.4byte	.LFE215
	.2byte	0x3
	.byte	0x7d
	.sleb128 244
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB214
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI73
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI74
	.4byte	.LFE214
	.2byte	0x3
	.byte	0x7d
	.sleb128 228
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB213
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI75
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI76
	.4byte	.LFE213
	.2byte	0x3
	.byte	0x7d
	.sleb128 220
	.4byte	0
	.4byte	0
.LLST48:
	.4byte	.LFB212
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI77
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI78
	.4byte	.LFE212
	.2byte	0x3
	.byte	0x7d
	.sleb128 200
	.4byte	0
	.4byte	0
.LLST49:
	.4byte	.LFB211
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI79
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI80
	.4byte	.LFE211
	.2byte	0x3
	.byte	0x7d
	.sleb128 244
	.4byte	0
	.4byte	0
.LLST50:
	.4byte	.LFB210
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI81
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI82
	.4byte	.LFE210
	.2byte	0x3
	.byte	0x7d
	.sleb128 220
	.4byte	0
	.4byte	0
.LLST51:
	.4byte	.LFB209
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI83
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI84
	.4byte	.LFE209
	.2byte	0x3
	.byte	0x7d
	.sleb128 172
	.4byte	0
	.4byte	0
.LLST52:
	.4byte	.LFB206
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI85
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI86
	.4byte	.LFE206
	.2byte	0x3
	.byte	0x7d
	.sleb128 144
	.4byte	0
	.4byte	0
.LLST53:
	.4byte	.LFB203
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI87
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI88
	.4byte	.LFE203
	.2byte	0x3
	.byte	0x7d
	.sleb128 104
	.4byte	0
	.4byte	0
.LLST54:
	.4byte	.LFB197
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI89
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI90
	.4byte	.LFE197
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST55:
	.4byte	.LFB196
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI91
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI92
	.4byte	.LFE196
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST56:
	.4byte	.LFB194
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI93
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI94
	.4byte	.LFE194
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST57:
	.4byte	.LFB185
	.4byte	.LCFI95
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI95
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI96
	.4byte	.LFE185
	.2byte	0x3
	.byte	0x7d
	.sleb128 208
	.4byte	0
	.4byte	0
.LLST58:
	.4byte	.LFB184
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI97
	.4byte	.LCFI98
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI98
	.4byte	.LFE184
	.2byte	0x3
	.byte	0x7d
	.sleb128 208
	.4byte	0
	.4byte	0
.LLST59:
	.4byte	.LFB178
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI99
	.4byte	.LCFI100
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI100
	.4byte	.LFE178
	.2byte	0x3
	.byte	0x7d
	.sleb128 120
	.4byte	0
	.4byte	0
.LLST60:
	.4byte	.LFB177
	.4byte	.LCFI101
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI101
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI102
	.4byte	.LFE177
	.2byte	0x3
	.byte	0x7d
	.sleb128 120
	.4byte	0
	.4byte	0
.LLST61:
	.4byte	.LFB176
	.4byte	.LCFI103
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI103
	.4byte	.LCFI104
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI104
	.4byte	.LFE176
	.2byte	0x3
	.byte	0x7d
	.sleb128 120
	.4byte	0
	.4byte	0
.LLST62:
	.4byte	.LFB175
	.4byte	.LCFI105
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI105
	.4byte	.LCFI106
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI106
	.4byte	.LFE175
	.2byte	0x3
	.byte	0x7d
	.sleb128 188
	.4byte	0
	.4byte	0
.LLST63:
	.4byte	.LFB163
	.4byte	.LCFI107
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI107
	.4byte	.LCFI108
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI108
	.4byte	.LFE163
	.2byte	0x3
	.byte	0x7d
	.sleb128 132
	.4byte	0
	.4byte	0
.LLST64:
	.4byte	.LFB162
	.4byte	.LCFI109
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI109
	.4byte	.LCFI110
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI110
	.4byte	.LFE162
	.2byte	0x3
	.byte	0x7d
	.sleb128 132
	.4byte	0
	.4byte	0
.LLST65:
	.4byte	.LFB161
	.4byte	.LCFI111
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI111
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI112
	.4byte	.LFE161
	.2byte	0x3
	.byte	0x7d
	.sleb128 132
	.4byte	0
	.4byte	0
.LLST66:
	.4byte	.LFB159
	.4byte	.LCFI113
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI113
	.4byte	.LCFI114
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI114
	.4byte	.LFE159
	.2byte	0x3
	.byte	0x7d
	.sleb128 532
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x23c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB217
	.4byte	.LFE217-.LFB217
	.4byte	.LFB207
	.4byte	.LFE207-.LFB207
	.4byte	.LFB205
	.4byte	.LFE205-.LFB205
	.4byte	.LFB204
	.4byte	.LFE204-.LFB204
	.4byte	.LFB202
	.4byte	.LFE202-.LFB202
	.4byte	.LFB201
	.4byte	.LFE201-.LFB201
	.4byte	.LFB200
	.4byte	.LFE200-.LFB200
	.4byte	.LFB199
	.4byte	.LFE199-.LFB199
	.4byte	.LFB198
	.4byte	.LFE198-.LFB198
	.4byte	.LFB195
	.4byte	.LFE195-.LFB195
	.4byte	.LFB193
	.4byte	.LFE193-.LFB193
	.4byte	.LFB192
	.4byte	.LFE192-.LFB192
	.4byte	.LFB191
	.4byte	.LFE191-.LFB191
	.4byte	.LFB190
	.4byte	.LFE190-.LFB190
	.4byte	.LFB189
	.4byte	.LFE189-.LFB189
	.4byte	.LFB188
	.4byte	.LFE188-.LFB188
	.4byte	.LFB187
	.4byte	.LFE187-.LFB187
	.4byte	.LFB186
	.4byte	.LFE186-.LFB186
	.4byte	.LFB183
	.4byte	.LFE183-.LFB183
	.4byte	.LFB182
	.4byte	.LFE182-.LFB182
	.4byte	.LFB181
	.4byte	.LFE181-.LFB181
	.4byte	.LFB180
	.4byte	.LFE180-.LFB180
	.4byte	.LFB179
	.4byte	.LFE179-.LFB179
	.4byte	.LFB174
	.4byte	.LFE174-.LFB174
	.4byte	.LFB173
	.4byte	.LFE173-.LFB173
	.4byte	.LFB172
	.4byte	.LFE172-.LFB172
	.4byte	.LFB171
	.4byte	.LFE171-.LFB171
	.4byte	.LFB170
	.4byte	.LFE170-.LFB170
	.4byte	.LFB169
	.4byte	.LFE169-.LFB169
	.4byte	.LFB168
	.4byte	.LFE168-.LFB168
	.4byte	.LFB167
	.4byte	.LFE167-.LFB167
	.4byte	.LFB166
	.4byte	.LFE166-.LFB166
	.4byte	.LFB165
	.4byte	.LFE165-.LFB165
	.4byte	.LFB164
	.4byte	.LFE164-.LFB164
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB225
	.4byte	.LFE225-.LFB225
	.4byte	.LFB224
	.4byte	.LFE224-.LFB224
	.4byte	.LFB223
	.4byte	.LFE223-.LFB223
	.4byte	.LFB222
	.4byte	.LFE222-.LFB222
	.4byte	.LFB221
	.4byte	.LFE221-.LFB221
	.4byte	.LFB220
	.4byte	.LFE220-.LFB220
	.4byte	.LFB219
	.4byte	.LFE219-.LFB219
	.4byte	.LFB216
	.4byte	.LFE216-.LFB216
	.4byte	.LFB215
	.4byte	.LFE215-.LFB215
	.4byte	.LFB214
	.4byte	.LFE214-.LFB214
	.4byte	.LFB213
	.4byte	.LFE213-.LFB213
	.4byte	.LFB212
	.4byte	.LFE212-.LFB212
	.4byte	.LFB211
	.4byte	.LFE211-.LFB211
	.4byte	.LFB210
	.4byte	.LFE210-.LFB210
	.4byte	.LFB209
	.4byte	.LFE209-.LFB209
	.4byte	.LFB206
	.4byte	.LFE206-.LFB206
	.4byte	.LFB203
	.4byte	.LFE203-.LFB203
	.4byte	.LFB197
	.4byte	.LFE197-.LFB197
	.4byte	.LFB196
	.4byte	.LFE196-.LFB196
	.4byte	.LFB194
	.4byte	.LFE194-.LFB194
	.4byte	.LFB185
	.4byte	.LFE185-.LFB185
	.4byte	.LFB184
	.4byte	.LFE184-.LFB184
	.4byte	.LFB178
	.4byte	.LFE178-.LFB178
	.4byte	.LFB177
	.4byte	.LFE177-.LFB177
	.4byte	.LFB176
	.4byte	.LFE176-.LFB176
	.4byte	.LFB175
	.4byte	.LFE175-.LFB175
	.4byte	.LFB163
	.4byte	.LFE163-.LFB163
	.4byte	.LFB162
	.4byte	.LFE162-.LFB162
	.4byte	.LFB161
	.4byte	.LFE161-.LFB161
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB159
	.4byte	.LFE159-.LFB159
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB217
	.4byte	.LFE217
	.4byte	.LFB207
	.4byte	.LFE207
	.4byte	.LFB205
	.4byte	.LFE205
	.4byte	.LFB204
	.4byte	.LFE204
	.4byte	.LFB202
	.4byte	.LFE202
	.4byte	.LFB201
	.4byte	.LFE201
	.4byte	.LFB200
	.4byte	.LFE200
	.4byte	.LFB199
	.4byte	.LFE199
	.4byte	.LFB198
	.4byte	.LFE198
	.4byte	.LFB195
	.4byte	.LFE195
	.4byte	.LFB193
	.4byte	.LFE193
	.4byte	.LFB192
	.4byte	.LFE192
	.4byte	.LFB191
	.4byte	.LFE191
	.4byte	.LFB190
	.4byte	.LFE190
	.4byte	.LFB189
	.4byte	.LFE189
	.4byte	.LFB188
	.4byte	.LFE188
	.4byte	.LFB187
	.4byte	.LFE187
	.4byte	.LFB186
	.4byte	.LFE186
	.4byte	.LFB183
	.4byte	.LFE183
	.4byte	.LFB182
	.4byte	.LFE182
	.4byte	.LFB181
	.4byte	.LFE181
	.4byte	.LFB180
	.4byte	.LFE180
	.4byte	.LFB179
	.4byte	.LFE179
	.4byte	.LFB174
	.4byte	.LFE174
	.4byte	.LFB173
	.4byte	.LFE173
	.4byte	.LFB172
	.4byte	.LFE172
	.4byte	.LFB171
	.4byte	.LFE171
	.4byte	.LFB170
	.4byte	.LFE170
	.4byte	.LFB169
	.4byte	.LFE169
	.4byte	.LFB168
	.4byte	.LFE168
	.4byte	.LFB167
	.4byte	.LFE167
	.4byte	.LFB166
	.4byte	.LFE166
	.4byte	.LFB165
	.4byte	.LFE165
	.4byte	.LFB164
	.4byte	.LFE164
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB225
	.4byte	.LFE225
	.4byte	.LFB224
	.4byte	.LFE224
	.4byte	.LFB223
	.4byte	.LFE223
	.4byte	.LFB222
	.4byte	.LFE222
	.4byte	.LFB221
	.4byte	.LFE221
	.4byte	.LFB220
	.4byte	.LFE220
	.4byte	.LFB219
	.4byte	.LFE219
	.4byte	.LFB216
	.4byte	.LFE216
	.4byte	.LFB215
	.4byte	.LFE215
	.4byte	.LFB214
	.4byte	.LFE214
	.4byte	.LFB213
	.4byte	.LFE213
	.4byte	.LFB212
	.4byte	.LFE212
	.4byte	.LFB211
	.4byte	.LFE211
	.4byte	.LFB210
	.4byte	.LFE210
	.4byte	.LFB209
	.4byte	.LFE209
	.4byte	.LFB206
	.4byte	.LFE206
	.4byte	.LFB203
	.4byte	.LFE203
	.4byte	.LFB197
	.4byte	.LFE197
	.4byte	.LFB196
	.4byte	.LFE196
	.4byte	.LFB194
	.4byte	.LFE194
	.4byte	.LFB185
	.4byte	.LFE185
	.4byte	.LFB184
	.4byte	.LFE184
	.4byte	.LFB178
	.4byte	.LFE178
	.4byte	.LFB177
	.4byte	.LFE177
	.4byte	.LFB176
	.4byte	.LFE176
	.4byte	.LFB175
	.4byte	.LFE175
	.4byte	.LFB163
	.4byte	.LFE163
	.4byte	.LFB162
	.4byte	.LFE162
	.4byte	.LFB161
	.4byte	.LFE161
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB159
	.4byte	.LFE159
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF26:
	.ascii	"ALERTS_pull_group_not_watersense_complaint_off_pile"
	.ascii	"\000"
.LASF140:
	.ascii	"ALERTS_pull_range_check_failed_int32_off_pile\000"
.LASF159:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF150:
	.ascii	"ALERTS_pull_battery_backed_var_init_off_pile\000"
.LASF12:
	.ascii	"ALERTS_pull_group_not_found_off_pile\000"
.LASF38:
	.ascii	"ALERTS_pull_range_check_failed_string_too_long_off_"
	.ascii	"pile\000"
.LASF116:
	.ascii	"ALERTS_pull_flow_not_checked_with_reason_off_pile\000"
.LASF51:
	.ascii	"ALERTS_pull_some_or_all_skipped_off_pile\000"
.LASF160:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/aler"
	.ascii	"ts/alert_parsing.c\000"
.LASF156:
	.ascii	"ALERTS_parse_comm_command_string\000"
.LASF94:
	.ascii	"ALERTS_pull_poc_terminal_added_or_removed_off_pile\000"
.LASF93:
	.ascii	"ALERTS_pull_weather_terminal_added_or_removed_off_p"
	.ascii	"ile\000"
.LASF82:
	.ascii	"ALERTS_pull_mobile_station_off_from_pile\000"
.LASF41:
	.ascii	"ALERTS_pull_no_current_master_valve_or_pump_off_pil"
	.ascii	"e\000"
.LASF52:
	.ascii	"ALERTS_pull_programmed_irrigation_still_running_off"
	.ascii	"_pile\000"
.LASF63:
	.ascii	"ALERTS_pull_budget_under_budget_off_pile\000"
.LASF113:
	.ascii	"ALERTS_pull_manual_water_station_started_off_pile\000"
.LASF25:
	.ascii	"ALERTS_pull_flash_write_postponed_off_pile\000"
.LASF103:
	.ascii	"ALERTS_pull_station_copied_off_pile\000"
.LASF66:
	.ascii	"ALERTS_pull_budget_no_budget_values_off_pile\000"
.LASF68:
	.ascii	"ALERTS_pull_set_no_water_days_by_group_off_pile\000"
.LASF47:
	.ascii	"ALERTS_pull_two_wire_cable_excessive_current_off_pi"
	.ascii	"le\000"
.LASF37:
	.ascii	"ALERTS_pull_msg_response_timeout_off_pile\000"
.LASF50:
	.ascii	"ALERTS_pull_scheduled_irrigation_started_off_pile\000"
.LASF75:
	.ascii	"ALERTS_pull_chain_has_changed_off_pile\000"
.LASF33:
	.ascii	"ALERTS_pull_comm_crc_failed_off_pile\000"
.LASF24:
	.ascii	"ALERTS_pull_flash_file_old_version_not_found_off_pi"
	.ascii	"le\000"
.LASF83:
	.ascii	"ALERTS_pull_walk_thru_started_off_pile\000"
.LASF101:
	.ascii	"ALERTS_pull_station_card_added_or_removed_off_pile\000"
.LASF1:
	.ascii	"ALERTS_pull_power_fail_off_pile\000"
.LASF59:
	.ascii	"ALERTS_pull_router_rcvd_unexp_token_resp_off_pile\000"
.LASF74:
	.ascii	"ALERTS_pull_chain_is_same_off_pile\000"
.LASF133:
	.ascii	"ALERTS_pull_mainline_break_off_pile\000"
.LASF120:
	.ascii	"ALERTS_pull_reboot_request_off_pile\000"
.LASF86:
	.ascii	"nlu_ALERTS_pull_moisture_reading_obtained_off_pile\000"
.LASF153:
	.ascii	"ALERTS_alert_is_visible_to_the_user\000"
.LASF96:
	.ascii	"ALERTS_pull_station_terminal_added_or_removed_off_p"
	.ascii	"ile\000"
.LASF73:
	.ascii	"ALERTS_pull_starting_scan_with_reason_off_pile\000"
.LASF91:
	.ascii	"ALERTS_pull_two_wire_terminal_added_or_removed_off_"
	.ascii	"pile\000"
.LASF161:
	.ascii	"ALERTS_get_station_number_with_or_without_two_wire_"
	.ascii	"indicator\000"
.LASF104:
	.ascii	"ALERTS_pull_station_removed_from_group_off_pile\000"
.LASF3:
	.ascii	"ALERTS_pull_user_reset_off_pile\000"
.LASF53:
	.ascii	"ALERTS_pull_irrigation_ended_off_pile\000"
.LASF134:
	.ascii	"ALERTS_derate_table_update_station_count_off_pile\000"
.LASF61:
	.ascii	"ALERTS_pull_msg_transaction_time_off_pile\000"
.LASF115:
	.ascii	"ALERTS_pull_accum_rain_set_by_station_cleared_off_p"
	.ascii	"ile\000"
.LASF131:
	.ascii	"ALERTS_pull_two_wire_station_decoder_fault_off_pile"
	.ascii	"\000"
.LASF15:
	.ascii	"ALERTS_pull_func_call_with_null_ptr_off_pile\000"
.LASF109:
	.ascii	"ALERTS_pull_moisture_reading_out_of_range_off_pile\000"
.LASF102:
	.ascii	"ALERTS_pull_station_moved_from_one_group_to_another"
	.ascii	"_off_pile\000"
.LASF69:
	.ascii	"ALERTS_pull_SetNOWDaysByAll_off_pile\000"
.LASF117:
	.ascii	"ALERTS_pull_wind_paused_or_resumed_off_pile\000"
.LASF77:
	.ascii	"ALERTS_pull_ETGAGE_PercentFull_off_pile\000"
.LASF138:
	.ascii	"ALERTS_pull_range_check_failed_time_off_pile\000"
.LASF5:
	.ascii	"ALERTS_pull_tpmicro_code_firmware_needs_updating_of"
	.ascii	"f_pile\000"
.LASF151:
	.ascii	"ALERTS_pull_firmware_update_off_pile\000"
.LASF147:
	.ascii	"ALERTS_pull_flash_file_deleting_off_pile\000"
.LASF123:
	.ascii	"ALERTS_pull_budget_period_ended_off_pile\000"
.LASF137:
	.ascii	"ALERTS_pull_range_check_failed_uint32_off_pile\000"
.LASF7:
	.ascii	"ALERTS_pull_change_pile_init_off_pile\000"
.LASF40:
	.ascii	"ALERTS_pull_short_station_off_pile\000"
.LASF154:
	.ascii	"ALERTS_pull_object_off_pile\000"
.LASF55:
	.ascii	"ALERTS_pull_router_rcvd_unexp_base_class_off_pile\000"
.LASF64:
	.ascii	"ALERTS_pull_budget_over_budget_off_pile\000"
.LASF90:
	.ascii	"ALERTS_pull_poc_assigned_to_mainline_off_pile\000"
.LASF45:
	.ascii	"ALERTS_pull_conventional_PUMP_short_off_pile\000"
.LASF105:
	.ascii	"ALERTS_pull_station_added_to_group_off_pile\000"
.LASF129:
	.ascii	"ALERTS_pull_hub_rcvd_data_off_pile\000"
.LASF36:
	.ascii	"ALERTS_pull_comm_status_timer_expired_off_pile\000"
.LASF128:
	.ascii	"ALERTS_pull_hub_forwarding_data_off_pile\000"
.LASF27:
	.ascii	"ALERTS_pull_system_preserves_activity_off_pile\000"
.LASF57:
	.ascii	"ALERTS_pull_router_unexp_to_addr_off_pile\000"
.LASF6:
	.ascii	"ALERTS_pull_alerts_pile_init_off_pile\000"
.LASF56:
	.ascii	"ALERTS_pull_router_rcvd_packet_not_on_hub_list_off_"
	.ascii	"pile\000"
.LASF49:
	.ascii	"ALERTS_pull_two_wire_cable_cooled_off_off_pile\000"
.LASF107:
	.ascii	"ALERTS_pull_soil_temperature_crossed_threshold_off_"
	.ascii	"pile\000"
.LASF8:
	.ascii	"ALERTS_pull_tp_micro_pile_init_off_pile\000"
.LASF97:
	.ascii	"ALERTS_pull_communication_card_added_or_removed_off"
	.ascii	"_pile\000"
.LASF21:
	.ascii	"ALERTS_pull_flash_file_not_found_off_pile\000"
.LASF85:
	.ascii	"ALERTS_pull_manual_water_all_started_off_pile\000"
.LASF76:
	.ascii	"ALERTS_pull_ETGAGE_pulse_off_pile\000"
.LASF139:
	.ascii	"ALERTS_pull_range_check_failed_string_off_pile\000"
.LASF141:
	.ascii	"ALERTS_pull_range_check_failed_float_off_pile\000"
.LASF158:
	.ascii	"nm_ALERT_PARSING_parse_alert_and_return_length\000"
.LASF88:
	.ascii	"ALERTS_pull_station_group_assigned_to_a_moisture_se"
	.ascii	"nsor_off_pile\000"
.LASF142:
	.ascii	"ALERTS_pull_range_check_failed_date_off_pile\000"
.LASF70:
	.ascii	"ALERTS_pull_set_no_water_days_by_box_off_pile\000"
.LASF110:
	.ascii	"ALERTS_pull_light_ID_with_text_from_pile\000"
.LASF71:
	.ascii	"ALERTS_pull_reset_moisture_balance_by_group_off_pil"
	.ascii	"e\000"
.LASF4:
	.ascii	"ALERTS_pull_main_code_needs_updating_off_pile\000"
.LASF42:
	.ascii	"ALERTS_pull_no_current_station_off_pile\000"
.LASF145:
	.ascii	"ALERTS_pull_COMM_COMMAND_FAILED_off_pile\000"
.LASF89:
	.ascii	"ALERTS_pull_station_group_assigned_to_mainline_off_"
	.ascii	"pile\000"
.LASF157:
	.ascii	"ALERTS_pull_change_line_off_pile\000"
.LASF108:
	.ascii	"ALERTS_pull_soil_moisture_crossed_threshold_off_pil"
	.ascii	"e\000"
.LASF149:
	.ascii	"ALERTS_pull_item_not_on_list_off_pile\000"
.LASF44:
	.ascii	"ALERTS_pull_conventional_MV_short_off_pile\000"
.LASF124:
	.ascii	"ALERTS_pull_budget_group_reduction_off_pile\000"
.LASF100:
	.ascii	"ALERTS_pull_lights_card_added_or_removed_off_pile\000"
.LASF11:
	.ascii	"ALERTS_pull_task_frozen_off_pile\000"
.LASF39:
	.ascii	"ALERTS_pull_mainline_break_cleared_off_pile\000"
.LASF136:
	.ascii	"ALERTS_derate_table_update_failed_off_pile\000"
.LASF16:
	.ascii	"ALERTS_pull_index_out_of_range_off_pile\000"
.LASF81:
	.ascii	"ALERTS_pull_mobile_station_on_from_pile\000"
.LASF13:
	.ascii	"ALERTS_pull_station_not_found_off_pile\000"
.LASF92:
	.ascii	"ALERTS_pull_communication_terminal_added_or_removed"
	.ascii	"_off_pile\000"
.LASF79:
	.ascii	"ALERTS_pull_fuse_replaced_off_pile\000"
.LASF28:
	.ascii	"ALERTS_pull_poc_preserves_activity_off_pile\000"
.LASF65:
	.ascii	"ALERTS_pull_budget_will_exceed_budget_off_pile\000"
.LASF155:
	.ascii	"ALERTS_pull_string_off_pile\000"
.LASF111:
	.ascii	"ALERTS_pull_ETTable_LimitedEntry_off_pile\000"
.LASF31:
	.ascii	"ALERTS_pull_inbound_message_size_off_pile\000"
.LASF84:
	.ascii	"ALERTS_pull_manual_water_program_started_off_pile\000"
.LASF34:
	.ascii	"ALERTS_pull_cts_timeout_off_pile\000"
.LASF60:
	.ascii	"ALERTS_pull_ci_queued_msg_off_pile\000"
.LASF23:
	.ascii	"ALERTS_pull_flash_file_obsolete_not_found_pile\000"
.LASF54:
	.ascii	"ALERTS_pull_poc_not_found_off_pile\000"
.LASF29:
	.ascii	"ALERTS_pull_COMM_COMMAND_RCVD_OR_ISSUED_off_pile\000"
.LASF126:
	.ascii	"ALERTS_pull_router_rcvd_packet_not_on_hub_list_with"
	.ascii	"_sn_off_pile\000"
.LASF114:
	.ascii	"ALERTS_pull_test_station_started_off_pile\000"
.LASF20:
	.ascii	"ALERTS_pull_flash_file_found_old_version_off_pile\000"
.LASF48:
	.ascii	"ALERTS_pull_two_wire_cable_over_heated_off_pile\000"
.LASF152:
	.ascii	"ALERTS_pull_walk_thru_station_added_or_removed_off_"
	.ascii	"pile\000"
.LASF46:
	.ascii	"ALERTS_pull_flow_not_checked_no_reasons_off_pile\000"
.LASF118:
	.ascii	"ALERTS_pull_stop_key_pressed_off_pile\000"
.LASF125:
	.ascii	"ALERTS_pull_router_rcvd_packet_not_for_us_off_pile\000"
.LASF67:
	.ascii	"ALERTS_pull_budget_over_budget_with_period_ending_t"
	.ascii	"oday_off_pile\000"
.LASF14:
	.ascii	"ALERTS_pull_station_not_in_group_off_pile\000"
.LASF35:
	.ascii	"ALERTS_pull_comm_packet_greater_than_512_off_pile\000"
.LASF78:
	.ascii	"ALERTS_pull_RAIN_24HourTotal_off_pile\000"
.LASF30:
	.ascii	"ALERTS_pull_outbound_message_size_off_pile\000"
.LASF72:
	.ascii	"ALERTS_pull_MVOR_skipped_off_pile\000"
.LASF58:
	.ascii	"ALERTS_pull_router_unk_port_off_pile\000"
.LASF2:
	.ascii	"ALERTS_pull_power_fail_brownout_off_pile\000"
.LASF10:
	.ascii	"ALERTS_pull_battery_backed_var_valid_off_pile\000"
.LASF127:
	.ascii	"ALERTS_pull_router_rcvd_unexp_class_off_pile\000"
.LASF80:
	.ascii	"ALERTS_pull_fuse_blown_off_pile\000"
.LASF130:
	.ascii	"ALERTS_pull_two_wire_poc_decoder_fault_off_pile\000"
.LASF135:
	.ascii	"ALERTS_derate_table_update_successful_off_pile\000"
.LASF106:
	.ascii	"ALERTS_pull_soil_conductivity_crossed_threshold_off"
	.ascii	"_pile\000"
.LASF143:
	.ascii	"ALERTS_pull_range_check_failed_bool_off_pile\000"
.LASF62:
	.ascii	"ALERTS_pull_largest_token_size_off_pile\000"
.LASF9:
	.ascii	"ALERTS_pull_engineering_pile_init_off_pile\000"
.LASF148:
	.ascii	"ALERTS_pull_flash_file_found_off_pile\000"
.LASF132:
	.ascii	"ALERTS_pull_flow_error_off_pile\000"
.LASF121:
	.ascii	"ALERTS_pull_MVOR_alert_off_pile\000"
.LASF32:
	.ascii	"ALERTS_pull_device_powered_off_pile\000"
.LASF18:
	.ascii	"ALERTS_pull_flash_file_system_passed_off_pile\000"
.LASF95:
	.ascii	"ALERTS_pull_lights_terminal_added_or_removed_off_pi"
	.ascii	"le\000"
.LASF17:
	.ascii	"ALERTS_pull_bit_set_with_no_data_off_pile\000"
.LASF146:
	.ascii	"ALERTS_pull_flash_writing_off_pile\000"
.LASF0:
	.ascii	"ALERTS_pull_program_restart_off_pile\000"
.LASF22:
	.ascii	"ALERTS_pull_flash_file_deleting_obsolete_off_pile\000"
.LASF112:
	.ascii	"ALERTS_pull_ETTable_Substitution_off_pile\000"
.LASF99:
	.ascii	"ALERTS_pull_poc_card_added_or_removed_off_pile\000"
.LASF122:
	.ascii	"ALERTS_pull_set_no_water_days_by_station_off_pile\000"
.LASF87:
	.ascii	"ALERTS_pull_moisture_reading_obtained_off_pile\000"
.LASF144:
	.ascii	"ALERTS_comm_mngr_blocked_msg_during_idle_off_pile\000"
.LASF43:
	.ascii	"ALERTS_pull_short_station_unknown\000"
.LASF119:
	.ascii	"ALERTS_pull_ETGAGE_percent_full_edited_off_pile\000"
.LASF19:
	.ascii	"ALERTS_pull_flash_file_size_error_off_pile\000"
.LASF98:
	.ascii	"ALERTS_pull_weather_card_added_or_removed_off_pile\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
