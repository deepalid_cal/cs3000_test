	.file	"device_GR_MultiTech_MTSMC_LTE.c"
	.text
.Ltext0:
	.global	__udivsi3
	.section	.text.mt_string_exchange,"ax",%progbits
	.align	2
	.type	mt_string_exchange, %function
mt_string_exchange:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI0:
	ldr	r8, .L3
	mov	r5, r1
	mov	r4, r0
	mov	r1, #300
	mov	r0, #1
	mov	r6, r2
	mov	r2, #200
	mov	r7, r3
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r3, .L3+4
	mov	r0, r5
	str	r5, [r8, r3]
	bl	strlen
	ldr	r3, .L3+8
	str	r0, [r8, r3]
	add	r3, r3, #4
	mov	r0, r4
	str	r7, [r8, r3]
	bl	strlen
	subs	r2, r0, #0
	beq	.L2
	mov	r3, #2
	str	r3, [sp, #0]
	mov	r0, #1
	mov	r1, r4
	mov	r3, #0
	str	r0, [sp, #4]
	bl	AddCopyOfBlockToXmitList
.L2:
	mov	r0, r6
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L3+12
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #40]
	mov	r3, #0
	bl	xTimerGenericCommand
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L4:
	.align	2
.L3:
	.word	SerDrvrVars_s
	.word	8476
	.word	8480
	.word	cics
.LFE0:
	.size	mt_string_exchange, .-mt_string_exchange
	.section	.text.MTSMC_LAT_initialize_the_connection_process,"ax",%progbits
	.align	2
	.global	MTSMC_LAT_initialize_the_connection_process
	.type	MTSMC_LAT_initialize_the_connection_process, %function
MTSMC_LAT_initialize_the_connection_process:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, lr}
.LCFI1:
	beq	.L6
	ldr	r0, .L7
	bl	Alert_Message
.L6:
	mov	r0, #1
	bl	power_up_device
	ldr	r1, .L7+4
	ldr	r2, .L7+8
	ldr	r1, [r1, #0]
	mov	r3, #0
	str	r1, [r2, #4]
	mov	r1, #1
	str	r3, [r2, #8]
	str	r1, [r2, #0]
	mvn	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L7+12
	mov	r1, #2
	ldr	r0, [r2, #40]
	mov	r2, #800
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, pc}
.L8:
	.align	2
.L7:
	.word	.LC0
	.word	my_tick_count
	.word	.LANCHOR0
	.word	cics
.LFE1:
	.size	MTSMC_LAT_initialize_the_connection_process, .-MTSMC_LAT_initialize_the_connection_process
	.section	.text.MTSMC_LAT_connection_processing,"ax",%progbits
	.align	2
	.global	MTSMC_LAT_connection_processing
	.type	MTSMC_LAT_connection_processing, %function
MTSMC_LAT_connection_processing:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r3, r0, #121
	cmp	r3, #1
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	mov	r5, r0
	ldrhi	r0, .L53
	bhi	.L49
	ldr	r4, .L53+4
	ldr	r3, [r4, #0]
	sub	r3, r3, #1
	cmp	r3, #16
	ldrls	pc, [pc, r3, asl #2]
	b	.L9
.L30:
	.word	.L13
	.word	.L14
	.word	.L15
	.word	.L16
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
.L13:
	ldr	r2, .L53+8
	ldr	r1, .L53+12
	mov	r3, #20
	ldr	r0, .L53+16
	bl	mt_string_exchange
	mov	r0, #1
	bl	set_reset_ACTIVE_to_serial_port_device
	mov	r0, #50
	bl	vTaskDelay
	mov	r0, #1
	bl	set_reset_INACTIVE_to_serial_port_device
	mov	r2, #2
	b	.L51
.L14:
	cmp	r5, #121
	ldreq	r0, .L53+20
	beq	.L49
	mov	r3, #6
	ldr	r0, .L53+24
	ldr	r1, .L53+28
	mov	r2, #1000
	bl	mt_string_exchange
	mov	r3, #3
	b	.L52
.L15:
	cmp	r5, #121
	ldreq	r0, .L53+32
	beq	.L49
	mov	r3, #4
	ldr	r0, .L53+36
	ldr	r1, .L53+28
	mov	r2, #1000
	bl	mt_string_exchange
	mov	r3, #4
	b	.L52
.L16:
	cmp	r5, #121
	ldreq	r0, .L53+40
	beq	.L49
	mov	r3, #4
	ldr	r0, .L53+44
	ldr	r1, .L53+28
	mov	r2, #1000
	bl	mt_string_exchange
	mov	r3, #5
	b	.L52
.L17:
	cmp	r5, #121
	ldreq	r0, .L53+48
	beq	.L49
	mov	r3, #4
	ldr	r0, .L53+52
	ldr	r1, .L53+28
	mov	r2, #1000
	bl	mt_string_exchange
	mov	r3, #6
	b	.L52
.L18:
	cmp	r5, #121
	ldreq	r0, .L53+56
	beq	.L49
	mov	r3, #4
	ldr	r0, .L53+60
	ldr	r1, .L53+28
	mov	r2, #1000
	bl	mt_string_exchange
	mov	r3, #7
	b	.L52
.L19:
	cmp	r5, #121
	bne	.L36
	ldr	r0, .L53+64
.L49:
	bl	Alert_Message
	ldr	r3, .L53+68
	mov	r2, #1
	mov	r0, #123
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	CONTROLLER_INITIATED_post_event
.L36:
	mov	r3, #4
	ldr	r0, .L53+72
	ldr	r1, .L53+28
	mov	r2, #1000
	bl	mt_string_exchange
	mov	r3, #8
	b	.L52
.L20:
	cmp	r5, #121
	ldreq	r0, .L53+76
	beq	.L49
	ldr	r0, .L53+16
	mov	r3, #0
	mov	r1, r0
	ldr	r2, .L53+80
	bl	mt_string_exchange
	mov	r3, #9
	b	.L52
.L21:
	mov	r3, #4
	ldr	r0, .L53+84
	ldr	r1, .L53+28
	mov	r2, #1000
	bl	mt_string_exchange
	mov	r3, #10
	b	.L52
.L22:
	cmp	r5, #121
	ldreq	r0, .L53+88
	beq	.L49
	mov	r3, #4
	ldr	r0, .L53+92
	ldr	r1, .L53+96
	mov	r2, #1000
	bl	mt_string_exchange
	mov	r3, #11
	b	.L52
.L23:
	mov	r0, #25
	bl	vTaskDelay
	ldr	r3, .L53+100
	ldr	r2, .L53+104
	mov	r1, #0
	strb	r1, [r2, r3]
	ldr	r0, .L53+108
	ldr	r1, .L53+112
	bl	Alert_Message_va
	cmp	r5, #121
	ldreq	r0, .L53+116
	beq	.L49
	mov	r2, #1000
	ldr	r0, .L53+120
	ldr	r1, .L53+28
	mov	r3, #4
	bl	mt_string_exchange
	mov	r2, #12
	b	.L51
.L24:
	cmp	r5, #121
	ldreq	r0, .L53+124
	beq	.L49
	mov	r3, #4
	ldr	r0, .L53+128
	ldr	r1, .L53+132
	mov	r2, #1000
	bl	mt_string_exchange
	mov	r3, #13
	b	.L52
.L25:
	mov	r0, #25
	bl	vTaskDelay
	ldr	r3, .L53+136
	ldr	r2, .L53+104
	mov	r1, #0
	strb	r1, [r2, r3]
	ldr	r0, .L53+108
	ldr	r1, .L53+112
	bl	Alert_Message_va
	cmp	r5, #121
	ldreq	r0, .L53+140
	beq	.L49
	mov	r2, #1000
	ldr	r0, .L53+144
	ldr	r1, .L53+148
	mov	r3, #4
	bl	mt_string_exchange
	mov	r2, #14
	b	.L51
.L26:
	mov	r0, #25
	bl	vTaskDelay
	ldr	r3, .L53+152
	ldr	r2, .L53+104
	mov	r1, #0
	strb	r1, [r2, r3]
	ldr	r0, .L53+108
	ldr	r1, .L53+112
	bl	Alert_Message_va
	cmp	r5, #121
	ldreq	r0, .L53+156
	beq	.L49
	mov	r2, #1000
	ldr	r0, .L53+160
	ldr	r1, .L53+164
	mov	r3, #4
	bl	mt_string_exchange
	mov	r2, #15
.L51:
	ldr	r3, .L53+4
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, pc}
.L27:
	mov	r0, #25
	bl	vTaskDelay
	ldr	r3, .L53+168
	ldr	r2, .L53+104
	mov	r4, #0
	ldr	r0, .L53+108
	strb	r4, [r2, r3]
	ldr	r1, .L53+112
	bl	Alert_Message_va
	cmp	r5, #121
	ldreq	r0, .L53+172
	beq	.L49
	ldr	r5, .L53+4
	mov	r3, #35
	ldr	r0, .L53+176
	ldr	r1, .L53+28
	ldr	r2, .L53+180
	bl	mt_string_exchange
	str	r4, [r5, #8]
	ldr	r0, .L53+184
	mov	r1, #1
	bl	Alert_Message_va
	mov	r3, #16
	str	r3, [r5, #0]
	ldmfd	sp!, {r4, r5, pc}
.L28:
	cmp	r5, #121
	bne	.L44
	ldr	r1, [r4, #8]
	ldr	r5, .L53+4
	cmp	r1, #15
	ldrhi	r0, .L53+188
	bhi	.L49
	ldr	r0, .L53+184
	add	r1, r1, #1
	bl	Alert_Message_va
	ldr	r3, [r5, #8]
	ldr	r0, .L53+176
	add	r3, r3, #1
	str	r3, [r5, #8]
	ldr	r1, .L53+28
	ldr	r2, .L53+180
	mov	r3, #35
	b	.L50
.L44:
	ldr	r0, .L53+192
	bl	Alert_Message
	ldr	r4, .L53+4
	ldr	r0, .L53+196
	ldr	r1, .L53+200
	ldr	r2, .L53+180
	mov	r3, #4
	bl	mt_string_exchange
	mov	r3, #0
	str	r3, [r4, #8]
	ldr	r0, .L53+204
	mov	r1, #1
	bl	Alert_Message_va
	mov	r3, #17
.L52:
	str	r3, [r4, #0]
	ldmfd	sp!, {r4, r5, pc}
.L29:
	cmp	r5, #121
	bne	.L46
	ldr	r1, [r4, #8]
	ldr	r5, .L53+4
	cmp	r1, #3
	ldrhi	r0, .L53+208
	bhi	.L49
	ldr	r0, .L53+204
	add	r1, r1, #1
	bl	Alert_Message_va
	ldr	r3, [r5, #8]
	ldr	r0, .L53+196
	ldr	r1, .L53+200
	ldr	r2, .L53+180
	add	r3, r3, #1
	str	r3, [r5, #8]
	mov	r3, #4
.L50:
	ldmfd	sp!, {r4, r5, lr}
	b	mt_string_exchange
.L46:
	mov	r2, #49
	ldr	r1, .L53+212
	ldr	r0, .L53+216
	bl	strlcpy
	ldr	r3, .L53+220
	mov	r1, #200
	ldr	r0, [r3, #0]
	ldr	r3, [r4, #4]
	rsb	r0, r3, r0
	bl	__udivsi3
	mov	r1, r0
	ldr	r0, .L53+224
	bl	Alert_Message_va
	mov	r0, #1
	mov	r1, #100
	mov	r2, #0
	bl	RCVD_DATA_enable_hunting_mode
	ldmfd	sp!, {r4, r5, lr}
	b	CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities
.L9:
	ldmfd	sp!, {r4, r5, pc}
.L54:
	.align	2
.L53:
	.word	.LC1
	.word	.LANCHOR0
	.word	60000
	.word	.LANCHOR1
	.word	.LC2
	.word	.LC3
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LC4
	.word	.LANCHOR4
	.word	.LC5
	.word	.LANCHOR5
	.word	.LC6
	.word	.LANCHOR6
	.word	.LC7
	.word	.LANCHOR7
	.word	.LC8
	.word	cics
	.word	.LANCHOR8
	.word	.LC9
	.word	20000
	.word	.LANCHOR9
	.word	.LC10
	.word	.LANCHOR10
	.word	.LANCHOR11
	.word	4339
	.word	SerDrvrVars_s
	.word	.LC11
	.word	SerDrvrVars_s+4310
	.word	.LC12
	.word	.LANCHOR12
	.word	.LC13
	.word	.LANCHOR13
	.word	.LANCHOR14
	.word	4331
	.word	.LC14
	.word	.LANCHOR15
	.word	.LANCHOR16
	.word	4320
	.word	.LC15
	.word	.LANCHOR17
	.word	.LANCHOR18
	.word	4337
	.word	.LC16
	.word	.LANCHOR19
	.word	5000
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	.LANCHOR20
	.word	.LANCHOR21
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	GuiVar_CommTestStatus
	.word	my_tick_count
	.word	.LC23
.LFE2:
	.size	MTSMC_LAT_connection_processing, .-MTSMC_LAT_connection_processing
	.global	mtcs
	.global	socket_connection_str
	.global	connect_response_str
	.global	pdp_context_str
	.global	iccid_query_str
	.global	csq_query_str
	.global	cops_query_str
	.global	cops_format_str
	.global	creg_query_str
	.global	creg_format_str
	.global	socket_cfgext_str
	.global	socket_cfg_str
	.global	iccid_response_str
	.global	csq_response_str
	.global	cops_response_str
	.global	creg_response_str
	.global	apn_str
	.global	diverity_antenna_off_str
	.global	ok_response_str
	.global	at_str
	.global	turn_echo_off_str
	.global	pacsp0_str
	.section	.rodata.creg_response_str,"a",%progbits
	.set	.LANCHOR11,. + 0
	.type	creg_response_str, %object
	.size	creg_response_str, 10
creg_response_str:
	.ascii	"\015\012+CREG: \000"
	.section	.rodata.cops_response_str,"a",%progbits
	.set	.LANCHOR14,. + 0
	.type	cops_response_str, %object
	.size	cops_response_str, 10
cops_response_str:
	.ascii	"\015\012+COPS: \000"
	.section	.rodata.socket_cfgext_str,"a",%progbits
	.set	.LANCHOR8,. + 0
	.type	socket_cfgext_str, %object
	.size	socket_cfgext_str, 21
socket_cfgext_str:
	.ascii	"at#scfgext=1,0,0,25\015\000"
	.section	.bss.mtcs,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	mtcs, %object
	.size	mtcs, 12
mtcs:
	.space	12
	.section	.rodata.iccid_query_str,"a",%progbits
	.set	.LANCHOR17,. + 0
	.type	iccid_query_str, %object
	.size	iccid_query_str, 9
iccid_query_str:
	.ascii	"at#ccid\015\000"
	.section	.rodata.pdp_context_str,"a",%progbits
	.set	.LANCHOR19,. + 0
	.type	pdp_context_str, %object
	.size	pdp_context_str, 14
pdp_context_str:
	.ascii	"at#sgact=1,1\015\000"
	.section	.rodata.cops_query_str,"a",%progbits
	.set	.LANCHOR13,. + 0
	.type	cops_query_str, %object
	.size	cops_query_str, 10
cops_query_str:
	.ascii	"at+cops?\015\000"
	.section	.rodata.cops_format_str,"a",%progbits
	.set	.LANCHOR12,. + 0
	.type	cops_format_str, %object
	.size	cops_format_str, 13
cops_format_str:
	.ascii	"at+cops=3,2\015\000"
	.section	.rodata.socket_connection_str,"a",%progbits
	.set	.LANCHOR20,. + 0
	.type	socket_connection_str, %object
	.size	socket_connection_str, 32
socket_connection_str:
	.ascii	"at#sd=1,0,16001,\"64.73.242.99\"\015\000"
	.section	.rodata.csq_query_str,"a",%progbits
	.set	.LANCHOR15,. + 0
	.type	csq_query_str, %object
	.size	csq_query_str, 8
csq_query_str:
	.ascii	"at+csq\015\000"
	.section	.rodata.creg_format_str,"a",%progbits
	.set	.LANCHOR9,. + 0
	.type	creg_format_str, %object
	.size	creg_format_str, 11
creg_format_str:
	.ascii	"at+creg=2\015\000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Why is the MultiTech LTE unit not on PORT A?\000"
.LC1:
	.ascii	"Connection Process : UNXEXP EVENT\000"
.LC2:
	.ascii	"\000"
.LC3:
	.ascii	"ERROR: REMOVE and REINSTALL YOUR SIM (missing reset"
	.ascii	" string)\000"
.LC4:
	.ascii	"ERROR: waiting for echo off ok\000"
.LC5:
	.ascii	"ERROR: waiting for simple AT ok\000"
.LC6:
	.ascii	"ERROR: waiting for div ant off ok\000"
.LC7:
	.ascii	"ERROR: waiting for APN ok\000"
.LC8:
	.ascii	"ERROR: waiting for socket cfg ok\000"
.LC9:
	.ascii	"ERROR: waiting for socket cfgext ok\000"
.LC10:
	.ascii	"ERROR: waiting for creg format ok\000"
.LC11:
	.ascii	"%s\000"
.LC12:
	.ascii	"ERROR: waiting for creg response\000"
.LC13:
	.ascii	"ERROR: waiting for cops format ok\000"
.LC14:
	.ascii	"ERROR: waiting for cops response\000"
.LC15:
	.ascii	"ERROR: waiting for csq response\000"
.LC16:
	.ascii	"ERROR: waiting for iccid response string\000"
.LC17:
	.ascii	"PDP Context Activation attempt %u\000"
.LC18:
	.ascii	"ERROR: no data connection\000"
.LC19:
	.ascii	"PDP Context Activated\000"
.LC20:
	.ascii	"Socket Create attempt %u\000"
.LC21:
	.ascii	"ERROR: no socket connection\000"
.LC22:
	.ascii	"Cellular Connection\000"
.LC23:
	.ascii	"MultiTech Cell LTE 16001 Connected in %u seconds\000"
	.section	.rodata.apn_str,"a",%progbits
	.set	.LANCHOR6,. + 0
	.type	apn_str, %object
	.size	apn_str, 31
apn_str:
	.ascii	"at+cgdcont=1,\"IP\",\"10429.mcs\"\015\000"
	.section	.rodata.ok_response_str,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	ok_response_str, %object
	.size	ok_response_str, 7
ok_response_str:
	.ascii	"\015\012OK\015\012\000"
	.section	.rodata.turn_echo_off_str,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	turn_echo_off_str, %object
	.size	turn_echo_off_str, 6
turn_echo_off_str:
	.ascii	"ATE0\015\000"
	.section	.rodata.socket_cfg_str,"a",%progbits
	.set	.LANCHOR7,. + 0
	.type	socket_cfg_str, %object
	.size	socket_cfg_str, 25
socket_cfg_str:
	.ascii	"at#scfg=1,1,300,0,600,1\015\000"
	.section	.rodata.at_str,"a",%progbits
	.set	.LANCHOR4,. + 0
	.type	at_str, %object
	.size	at_str, 4
at_str:
	.ascii	"AT\015\000"
	.section	.rodata.pacsp0_str,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	pacsp0_str, %object
	.size	pacsp0_str, 12
pacsp0_str:
	.ascii	"\015\012+PACSP0\015\012\000"
	.section	.rodata.iccid_response_str,"a",%progbits
	.set	.LANCHOR18,. + 0
	.type	iccid_response_str, %object
	.size	iccid_response_str, 10
iccid_response_str:
	.ascii	"\015\012#CCID: \000"
	.section	.rodata.csq_response_str,"a",%progbits
	.set	.LANCHOR16,. + 0
	.type	csq_response_str, %object
	.size	csq_response_str, 9
csq_response_str:
	.ascii	"\015\012+CSQ: \000"
	.section	.rodata.diverity_antenna_off_str,"a",%progbits
	.set	.LANCHOR5,. + 0
	.type	diverity_antenna_off_str, %object
	.size	diverity_antenna_off_str, 14
diverity_antenna_off_str:
	.ascii	"at#rxdiv=0,1\015\000"
	.section	.rodata.creg_query_str,"a",%progbits
	.set	.LANCHOR10,. + 0
	.type	creg_query_str, %object
	.size	creg_query_str, 10
creg_query_str:
	.ascii	"at+creg?\015\000"
	.section	.rodata.connect_response_str,"a",%progbits
	.set	.LANCHOR21,. + 0
	.type	connect_response_str, %object
	.size	connect_response_str, 12
connect_response_str:
	.ascii	"\015\012CONNECT\015\012\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_GR_MultiTech_MTSMC_LTE.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0xc8
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0xeb
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x102
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_GR_MultiTech_MTSMC_LTE.c\000"
.LASF4:
	.ascii	"mt_string_exchange\000"
.LASF0:
	.ascii	"MTSMC_LAT_initialize_the_connection_process\000"
.LASF1:
	.ascii	"MTSMC_LAT_connection_processing\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
