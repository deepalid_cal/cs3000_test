	.file	"s1l_memtests.c"
	.text
.Ltext0:
	.section	.text.memtest_disp_addr,"ax",%progbits
	.align	2
	.global	memtest_disp_addr
	.type	memtest_disp_addr, %function
memtest_disp_addr:
.LFB0:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI0:
	mov	r5, r3
	ldr	r3, [r3, #0]
	mov	r6, r1
	sub	r7, r3, #1
	cmp	r3, #4
	cmpne	r7, #1
	mov	r4, r2
	movhi	r7, #0
	movls	r7, #1
	bhi	.L2
	ldr	r2, [r1, #0]
	rsb	r1, r3, #0
	and	r1, r1, r2
	cmp	r1, r2
	beq	.L3
	add	r3, r1, r3
	ldr	r1, [r4, #0]
	rsb	r2, r2, r1
	add	r2, r2, r3
	str	r2, [r4, #0]
	ldr	r1, [r5, #0]
	rsb	r1, r1, #0
	and	r2, r2, r1
	str	r2, [r4, #0]
	str	r3, [r6, #0]
.L3:
	cmp	r0, #0
	beq	.L5
	bl	term_dat_out
	ldr	r0, .L6
	bl	term_dat_out
	mov	r2, #8
	ldr	r1, [r6, #0]
	mov	r0, sp
	bl	str_makehex
	mov	r0, sp
	bl	term_dat_out_crlf
	ldr	r0, .L6+4
	bl	term_dat_out
	ldr	r1, [r4, #0]
	mov	r0, sp
	bl	str_makedec
	mov	r0, sp
	bl	term_dat_out_crlf
	ldr	r0, .L6+8
	bl	term_dat_out
	mov	r0, sp
	ldr	r1, [r5, #0]
	bl	str_makedec
	mov	r0, sp
	bl	term_dat_out_crlf
	b	.L5
.L2:
	ldr	r0, .L6+12
	bl	term_dat_out
	mov	r0, r7
	b	.L4
.L5:
	mov	r0, #1
.L4:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L7:
	.align	2
.L6:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE0:
	.size	memtest_disp_addr, .-memtest_disp_addr
	.section	.text.memtest_pf,"ax",%progbits
	.align	2
	.global	memtest_pf
	.type	memtest_pf, %function
memtest_pf:
.LFB1:
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI1:
	mov	r4, r1
	mov	r6, r2
	mov	r5, r3
	bne	.L9
.LBB4:
	mov	r4, r4, asl #1
	ldr	r0, .L11
	bl	term_dat_out
	mov	r1, r6
	mov	r2, r4
	mov	r0, sp
	bl	str_makehex
	mov	r0, sp
	bl	term_dat_out_crlf
	ldr	r0, .L11+4
	bl	term_dat_out
	ldr	r1, [sp, #32]
	mov	r2, r4
	mov	r0, sp
	bl	str_makehex
	mov	r0, sp
	bl	term_dat_out_crlf
	ldr	r0, .L11+8
	bl	term_dat_out
	mov	r0, sp
	mov	r1, r5
	mov	r2, r4
	bl	str_makehex
	mov	r0, sp
	bl	term_dat_out_crlf
	b	.L8
.L9:
.LBE4:
	ldr	r0, .L11+12
	bl	term_dat_out
.L8:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, pc}
.L12:
	.align	2
.L11:
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	.LANCHOR7
.LFE1:
	.size	memtest_pf, .-memtest_pf
	.section	.text.memtest_w10,"ax",%progbits
	.align	2
	.global	memtest_w10
	.type	memtest_w10, %function
memtest_w10:
.LFB2:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI2:
	mov	r4, r0
	str	r1, [sp, #12]
	str	r2, [sp, #8]
	str	r3, [sp, #4]
	ldreq	r0, .L49
	ldrne	r0, .L49+4
	add	r1, sp, #12
	add	r2, sp, #8
	add	r3, sp, #4
	bl	memtest_disp_addr
	cmp	r0, #0
	beq	.L13
	ldr	r3, [sp, #12]
	ldr	r5, [sp, #8]
	ldr	r0, .L49+8
	add	r5, r5, r3
	bl	term_dat_out
	ldr	r2, [sp, #12]
	mov	r3, #0
	mov	r0, #1
	b	.L17
.L26:
	ldr	r1, [sp, #4]
	cmp	r1, #2
	beq	.L20
	cmp	r1, #4
	beq	.L21
	cmp	r1, #1
	bne	.L18
	cmp	r4, #1
	moveq	r1, r4, asl r3
	mvnne	r1, r1, asl r3
	strb	r1, [r2, #0]
	b	.L18
.L20:
	cmp	r4, #1
	moveq	r1, r4, asl r3
	mvnne	r1, r0, asl r3
	strh	r1, [r2, #0]	@ movhi
	b	.L18
.L21:
	cmp	r4, #1
	moveq	r1, r4, asl r3
	mvnne	r1, r0, asl r3
	str	r1, [r2, #0]
.L18:
	ldr	r1, [sp, #4]
	add	r3, r3, #1
	cmp	r3, r1, asl #3
	movcs	r3, #0
	add	r2, r2, r1
.L17:
	cmp	r2, r5
	bcc	.L26
	ldr	r0, .L49+12
	bl	term_dat_out
	ldr	r1, [sp, #4]
	ldr	r2, [sp, #12]
	mov	r6, r1, asl #3
	mov	r0, #0
	mov	lr, #1
	b	.L27
.L36:
	cmp	r1, #2
	beq	.L30
	cmp	r1, #4
	beq	.L31
	cmp	r1, #1
	bne	.L28
	cmp	r4, #1
	bne	.L32
	ldrb	r3, [r2, #0]	@ zero_extendqisi2
	mov	ip, r4, asl r0
	and	r7, ip, #255
	cmp	r3, r7
	beq	.L28
	str	ip, [sp, #0]
	mov	r0, #0
	mov	r1, r4
	b	.L43
.L32:
	mvn	ip, r1, asl r0
	ldrb	r3, [r2, #0]	@ zero_extendqisi2
	and	ip, ip, #255
	b	.L46
.L30:
	cmp	r4, #1
	bne	.L33
	ldrh	r3, [r2, #0]
	mov	ip, r4, asl r0
	mov	r7, ip, asl #16
	cmp	r3, r7, lsr #16
	beq	.L28
	b	.L44
.L33:
	mvn	ip, lr, asl r0
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
	ldrh	r3, [r2, #0]
	b	.L46
.L31:
	cmp	r4, #1
	ldr	r3, [r2, #0]
	moveq	ip, r4, asl r0
	mvnne	ip, lr, asl r0
.L46:
	cmp	r3, ip
	beq	.L28
.L44:
	str	ip, [sp, #0]
	mov	r0, #0
	b	.L43
.L28:
	add	r0, r0, #1
	cmp	r0, r6
	movcs	r0, #0
	add	r2, r2, r1
.L27:
	cmp	r2, r5
	bcc	.L36
	mov	r2, #0
	mov	r0, #1
	mov	r3, r2
	str	r2, [sp, #0]
.L43:
	bl	memtest_pf
.L13:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, pc}
.L50:
	.align	2
.L49:
	.word	.LANCHOR8
	.word	.LANCHOR9
	.word	.LANCHOR10
	.word	.LANCHOR11
.LFE2:
	.size	memtest_w10, .-memtest_w10
	.section	.text.memtest_ia,"ax",%progbits
	.align	2
	.global	memtest_ia
	.type	memtest_ia, %function
memtest_ia:
.LFB3:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI3:
	str	r0, [sp, #12]
	str	r1, [sp, #8]
	str	r2, [sp, #4]
	ldr	r0, .L69
	add	r1, sp, #12
	add	r2, sp, #8
	add	r3, sp, #4
	bl	memtest_disp_addr
	cmp	r0, #0
	beq	.L51
	ldr	r3, [sp, #12]
	ldr	r5, [sp, #8]
	ldr	r0, .L69+4
	add	r5, r5, r3
	bl	term_dat_out
	ldr	r3, [sp, #12]
	b	.L53
.L58:
	ldr	r2, [sp, #4]
	cmp	r2, #2
	mvneq	r2, r3
	streqh	r2, [r3, #0]	@ movhi
	beq	.L54
	cmp	r2, #4
	mvneq	r2, r3
	streq	r2, [r3, #0]
	beq	.L54
	cmp	r2, #1
	mvneq	r2, r3
	streqb	r2, [r3, #0]
.L54:
	ldr	r2, [sp, #4]
	add	r3, r3, r2
.L53:
	cmp	r3, r5
	bcc	.L58
	ldr	r0, .L69+8
	bl	term_dat_out
	ldr	r4, [sp, #12]
	b	.L59
.L64:
	ldr	r1, [sp, #4]
	cmp	r1, #2
	beq	.L62
	cmp	r1, #4
	beq	.L63
	cmp	r1, #1
	mvneq	r2, r4
	andeq	r2, r2, #255
	ldreqb	r3, [r4, #0]	@ zero_extendqisi2
	bne	.L60
	b	.L67
.L62:
	mvn	r2, r4
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	ldrh	r3, [r4, #0]
	b	.L67
.L63:
	ldr	r3, [r4, #0]
	mvn	r2, r4
.L67:
	cmp	r3, r2
	beq	.L60
	str	r2, [sp, #0]
	mov	r0, #0
	mov	r2, r4
	bl	memtest_pf
.L60:
	ldr	r3, [sp, #4]
	add	r4, r4, r3
.L59:
	cmp	r4, r5
	bcc	.L64
	mov	r2, #0
	mov	r0, #1
	ldr	r1, [sp, #4]
	mov	r3, r2
	str	r2, [sp, #0]
	bl	memtest_pf
.L51:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, pc}
.L70:
	.align	2
.L69:
	.word	.LANCHOR12
	.word	.LANCHOR10
	.word	.LANCHOR11
.LFE3:
	.size	memtest_ia, .-memtest_ia
	.section	.text.memtest_pt,"ax",%progbits
	.align	2
	.global	memtest_pt
	.type	memtest_pt, %function
memtest_pt:
.LFB4:
	@ args = 4, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI4:
	ldr	r5, [sp, #40]
	str	r0, [sp, #12]
	str	r1, [sp, #8]
	str	r2, [sp, #4]
	mov	r4, r3
	ldr	r0, .L92
	add	r1, sp, #12
	add	r2, sp, #8
	add	r3, sp, #4
	bl	memtest_disp_addr
	cmp	r0, #0
	beq	.L71
	ldr	r3, [sp, #12]
	ldr	r8, [sp, #8]
	mov	r6, #0
	add	r8, r8, r3
	mov	r7, r6
	b	.L73
.L86:
	ldr	r0, .L92+4
	bl	term_dat_out
	ldr	r3, [sp, #12]
	b	.L74
.L79:
	ldr	r2, [sp, #4]
	cmp	r2, #2
	ldreq	r2, [r4, r6]
	streqh	r2, [r3, #0]	@ movhi
	beq	.L75
	cmp	r2, #4
	ldreq	r2, [r4, r6]
	streq	r2, [r3, #0]
	beq	.L75
	cmp	r2, #1
	ldreq	r2, [r4, r6]
	streqb	r2, [r3, #0]
.L75:
	ldr	r2, [sp, #4]
	add	r3, r3, r2
.L74:
	cmp	r3, r8
	bcc	.L79
	ldr	r0, .L92+8
	bl	term_dat_out
	ldr	r2, [sp, #12]
	ldr	r1, [sp, #4]
	b	.L80
.L85:
	cmp	r1, #2
	beq	.L83
	cmp	r1, #4
	beq	.L84
	cmp	r1, #1
	ldreqb	r0, [r4, r6]	@ zero_extendqisi2
	ldreqb	r3, [r2, #0]	@ zero_extendqisi2
	bne	.L81
	b	.L90
.L83:
	ldrh	r0, [r4, r6]
	ldrh	r3, [r2, #0]
	b	.L90
.L84:
	ldr	r0, [r4, r6]
	ldr	r3, [r2, #0]
.L90:
	cmp	r3, r0
	strne	r0, [sp, #0]
	movne	r0, #0
	bne	.L87
.L81:
	add	r2, r2, r1
.L80:
	cmp	r2, r8
	bcc	.L85
	add	r7, r7, #1
	add	r6, r6, #4
.L73:
	cmp	r7, r5
	blt	.L86
	ldr	r1, [sp, #4]
	mov	r2, #0
	mov	r0, #1
	mov	r3, r2
	str	r2, [sp, #0]
.L87:
	bl	memtest_pf
.L71:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, pc}
.L93:
	.align	2
.L92:
	.word	.LANCHOR13
	.word	.LANCHOR10
	.word	.LANCHOR11
.LFE4:
	.size	memtest_pt, .-memtest_pt
	.section	.text.memory_test,"ax",%progbits
	.align	2
	.global	memory_test
	.type	memory_test, %function
memory_test:
.LFB5:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI5:
	ldr	r8, [sp, #36]
	mov	r6, r0
	mov	r5, r1
	mov	r4, r2
	mov	r7, r3
	mov	sl, #2
	mov	r9, #4
	b	.L95
.L104:
	cmn	r7, #-2147483647
	bne	.L96
	ldr	r0, .L107
	bl	term_dat_out
	mov	r1, r6
	mov	r2, r5
	mov	r3, r4
	mov	r0, #1
	bl	memtest_w10
	mov	r0, #250
	bl	vTaskDelay
	ldr	r0, .L107+4
	bl	term_dat_out
	mov	r3, r4
	mov	r1, r6
	mov	r2, r5
	mov	r0, #0
	bl	memtest_w10
	mov	r0, #250
	bl	vTaskDelay
	ldr	r0, .L107+4
	bl	term_dat_out
	mov	r1, r5
	mov	r2, r4
	mov	r0, r6
	bl	memtest_ia
	mov	r0, #250
	bl	vTaskDelay
	ldr	r0, .L107+4
	bl	term_dat_out
	mov	r1, r5
	mov	r2, r4
	ldr	r3, .L107+8
	mov	r0, r6
	str	r9, [sp, #0]
	bl	memtest_pt
	mov	r0, #250
	bl	vTaskDelay
	ldr	r0, .L107+4
	bl	term_dat_out
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	ldr	r3, .L107+12
	str	sl, [sp, #0]
	bl	memtest_pt
	mov	r0, #250
	bl	vTaskDelay
	ldr	r0, .L107
	bl	term_dat_out
	b	.L97
.L96:
	cmp	r7, #4
	ldrls	pc, [pc, r7, asl #2]
	b	.L97
.L103:
	.word	.L98
	.word	.L99
	.word	.L100
	.word	.L101
	.word	.L102
.L98:
	mov	r0, #1
	b	.L105
.L99:
	mov	r0, #0
.L105:
	mov	r1, r6
	mov	r2, r5
	mov	r3, r4
	bl	memtest_w10
	b	.L97
.L100:
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	bl	memtest_ia
	b	.L97
.L101:
	str	r9, [sp, #0]
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	ldr	r3, .L107+8
	b	.L106
.L102:
	ldr	r3, .L107+12
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	str	sl, [sp, #0]
.L106:
	bl	memtest_pt
.L97:
	sub	r8, r8, #1
.L95:
	cmp	r8, #0
	bne	.L104
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, pc}
.L108:
	.align	2
.L107:
	.word	.LC0
	.word	.LC1
	.word	.LANCHOR14
	.word	.LANCHOR15
.LFE5:
	.size	memory_test, .-memory_test
	.section	.data.verify_msg,"aw",%progbits
	.set	.LANCHOR11,. + 0
	.type	verify_msg, %object
	.size	verify_msg, 19
verify_msg:
	.ascii	"Verifying data....\000"
	.section	.data.patts_00ff,"aw",%progbits
	.align	2
	.set	.LANCHOR15,. + 0
	.type	patts_00ff, %object
	.size	patts_00ff, 8
patts_00ff:
	.word	0
	.word	-1
	.section	.data.failed1_msg,"aw",%progbits
	.set	.LANCHOR4,. + 0
	.type	failed1_msg, %object
	.size	failed1_msg, 24
failed1_msg:
	.ascii	"Test failed at address \000"
	.section	.data.invwidth_msg,"aw",%progbits
	.set	.LANCHOR3,. + 0
	.type	invwidth_msg, %object
	.size	invwidth_msg, 42
invwidth_msg:
	.ascii	"Invalid width, must be 1, 2, or 4 bytes\012\015\000"
	.section	.data.pattst_msg,"aw",%progbits
	.set	.LANCHOR13,. + 0
	.type	pattst_msg, %object
	.size	pattst_msg, 15
pattst_msg:
	.ascii	"Pattern test\012\015\000"
	.section	.data.failed2_msg,"aw",%progbits
	.set	.LANCHOR5,. + 0
	.type	failed2_msg, %object
	.size	failed2_msg, 19
failed2_msg:
	.ascii	"Expected pattern: \000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"*************************\015\012\000"
.LC1:
	.ascii	"\015\012\000"
	.section	.data.start_msg,"aw",%progbits
	.set	.LANCHOR10,. + 0
	.type	start_msg, %object
	.size	start_msg, 18
start_msg:
	.ascii	"Starting test....\000"
	.section	.data.failed3_msg,"aw",%progbits
	.set	.LANCHOR6,. + 0
	.type	failed3_msg, %object
	.size	failed3_msg, 19
failed3_msg:
	.ascii	"Actual pattern  : \000"
	.section	.data.addr_msg,"aw",%progbits
	.set	.LANCHOR0,. + 0
	.type	addr_msg, %object
	.size	addr_msg, 15
addr_msg:
	.ascii	"Address      :\000"
	.section	.data.w1s_msg,"aw",%progbits
	.set	.LANCHOR8,. + 0
	.type	w1s_msg, %object
	.size	w1s_msg, 26
w1s_msg:
	.ascii	"Starting walking 1 test\012\015\000"
	.section	.data.passed_msg,"aw",%progbits
	.set	.LANCHOR7,. + 0
	.type	passed_msg, %object
	.size	passed_msg, 14
passed_msg:
	.ascii	"Test passed\012\015\000"
	.section	.data.width_msg,"aw",%progbits
	.set	.LANCHOR2,. + 0
	.type	width_msg, %object
	.size	width_msg, 15
width_msg:
	.ascii	"Width (bytes):\000"
	.section	.data.bytes_msg,"aw",%progbits
	.set	.LANCHOR1,. + 0
	.type	bytes_msg, %object
	.size	bytes_msg, 15
bytes_msg:
	.ascii	"Bytes        :\000"
	.section	.data.w0s_msg,"aw",%progbits
	.set	.LANCHOR9,. + 0
	.type	w0s_msg, %object
	.size	w0s_msg, 26
w0s_msg:
	.ascii	"Starting walking 0 test\012\015\000"
	.section	.data.invtst_msg,"aw",%progbits
	.set	.LANCHOR12,. + 0
	.type	invtst_msg, %object
	.size	invtst_msg, 23
invtst_msg:
	.ascii	"Inverse address test\012\015\000"
	.section	.data.patts_55aa,"aw",%progbits
	.align	2
	.set	.LANCHOR14,. + 0
	.type	patts_55aa, %object
	.size	patts_55aa, 16
patts_55aa:
	.word	-1431655766
	.word	1431655765
	.word	-1515870811
	.word	1515870810
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/memtest/s1l_memtests.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x9f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0x96
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x4f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xc8
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x189
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1f3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x266
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"memtest_w10\000"
.LASF0:
	.ascii	"memtest_disp_addr\000"
.LASF3:
	.ascii	"memtest_pt\000"
.LASF2:
	.ascii	"memtest_ia\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/memt"
	.ascii	"est/s1l_memtests.c\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"memory_test\000"
.LASF7:
	.ascii	"memtest_pf\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
