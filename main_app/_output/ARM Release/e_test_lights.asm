	.file	"e_test_lights.c"
	.text
.Ltext0:
	.section	.text.LIGHTS_TEST_turn_light_on_or_off,"ax",%progbits
	.align	2
	.type	LIGHTS_TEST_turn_light_on_or_off, %function
LIGHTS_TEST_turn_light_on_or_off:
.LFB0:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r2, #1
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI0:
	mov	r5, r0
	mov	r4, r1
	bne	.L2
	ldr	r7, .L10
	ldr	r6, .L10+4
	ldr	r3, [r7, #0]
	mov	r0, sp
	strh	r3, [sp, #12]	@ movhi
	ldr	r3, [r6, #0]
	str	r3, [sp, #8]
	bl	EPSON_obtain_latest_time_and_date
	mov	r0, sp
	add	r1, sp, #8
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #0
	bne	.L6
.L3:
.LBB12:
	mov	r1, r4
	mov	r0, r5
	bl	LIGHTS_get_lights_array_index
	ldr	r1, [r7, #0]
	ldr	r2, [r6, #0]
	bl	IRRI_LIGHTS_request_light_on
	b	.L9
.L2:
.LBE12:
	bl	LIGHTS_get_lights_array_index
	bl	IRRI_LIGHTS_request_light_off
.L9:
	cmp	r0, #0
	beq	.L6
	bl	good_key_beep
	b	.L1
.L6:
	bl	bad_key_beep
.L1:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, pc}
.L11:
	.align	2
.L10:
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE0:
	.size	LIGHTS_TEST_turn_light_on_or_off, .-LIGHTS_TEST_turn_light_on_or_off
	.section	.text.nm_LIGHTS_TEST_load_irri_list_stop_date_and_time.part.1,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_TEST_load_irri_list_stop_date_and_time.part.1, %function
nm_LIGHTS_TEST_load_irri_list_stop_date_and_time.part.1:
.LFB10:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI1:
	ldr	r4, .L13
	ldr	r2, .L13+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	mov	r3, #168
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L13+8
	ldr	r0, [r3, #0]
	ldr	r3, .L13+12
	ldr	r1, [r3, #0]
	bl	LIGHTS_get_lights_array_index
	mov	r1, r0
	mov	r0, sp
	bl	IRRI_LIGHTS_get_light_stop_date_and_time
	ldrh	r2, [sp, #4]
	ldr	r3, .L13+16
	ldr	r0, [r4, #0]
	str	r2, [r3, #0]
	ldr	r2, [sp, #0]
	ldr	r3, .L13+20
	str	r2, [r3, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L14:
	.align	2
.L13:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE10:
	.size	nm_LIGHTS_TEST_load_irri_list_stop_date_and_time.part.1, .-nm_LIGHTS_TEST_load_irri_list_stop_date_and_time.part.1
	.section	.text.nm_LIGHTS_TEST_update_stop_date_and_time_panels,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_TEST_update_stop_date_and_time_panels, %function
nm_LIGHTS_TEST_update_stop_date_and_time_panels:
.LFB4:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	ldr	r4, .L21
	sub	sp, sp, #64
.LCFI3:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	ldreq	r2, .L21+4
	streq	r3, [r2, #0]
	beq	.L19
	ldr	r3, .L21+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L21+12
	mov	r3, #224
	bl	xQueueTakeMutexRecursive_debug
.LBB13:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L17
	bl	nm_LIGHTS_TEST_load_irri_list_stop_date_and_time.part.1
.L17:
.LBE13:
	ldr	r3, .L21+16
	ldr	r2, [r3, #0]
	ldr	r3, .L21+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	movne	r3, #1
	bne	.L18
	ldr	r3, .L21+24
	ldr	r2, [r3, #0]
	ldr	r3, .L21+28
	ldr	r3, [r3, #0]
	subs	r3, r2, r3
	movne	r3, #1
.L18:
	ldr	r2, .L21+4
	str	r3, [r2, #0]
	ldr	r3, .L21+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L19:
	ldr	r3, .L21+28
	mov	r4, #0
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	add	r0, sp, #56
	ldr	r2, [r3, #0]
	mov	r1, #8
	mov	r3, r4
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r3, .L21+20
	mov	r5, #200
	str	r5, [sp, #0]
	mov	r1, #48
	ldr	r2, [r3, #0]
	mov	r3, #100
	mov	r6, r0
	add	r0, sp, #8
	bl	GetDateStr
	mov	r3, r6
	mov	r1, #33
	ldr	r2, .L21+32
	str	r0, [sp, #0]
	ldr	r0, .L21+36
	bl	snprintf
	ldr	r3, .L21+24
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	add	r0, sp, #56
	ldr	r2, [r3, #0]
	mov	r1, #8
	mov	r3, r4
	bl	TDUTILS_time_to_time_string_with_ampm
	ldr	r3, .L21+16
	str	r5, [sp, #0]
	mov	r1, #48
	ldr	r2, [r3, #0]
	mov	r3, #100
	mov	r4, r0
	add	r0, sp, #8
	bl	GetDateStr
	mov	r1, #33
	ldr	r2, .L21+32
	mov	r3, r4
	str	r0, [sp, #0]
	ldr	r0, .L21+40
	bl	snprintf
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, pc}
.L22:
	.align	2
.L21:
	.word	GuiVar_LightIsEnergized
	.word	GuiVar_LightsStopTimeHasBeenEdited
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	.LANCHOR1
	.word	.LANCHOR3
	.word	.LC1
	.word	GuiVar_LightIrriListStop
	.word	GuiVar_LightEditableStop
.LFE4:
	.size	nm_LIGHTS_TEST_update_stop_date_and_time_panels, .-nm_LIGHTS_TEST_update_stop_date_and_time_panels
	.global	__udivsi3
	.global	__umodsi3
	.section	.text.nm_LIGHTS_TEST_load_editable_stop_date_and_time,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_TEST_load_editable_stop_date_and_time, %function
nm_LIGHTS_TEST_load_editable_stop_date_and_time:
.LFB3:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI4:
	ldr	r4, .L30
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L24
	ldr	r3, .L30+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L30+8
	mov	r3, #192
	bl	xQueueTakeMutexRecursive_debug
.LBB19:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L25
	bl	nm_LIGHTS_TEST_load_irri_list_stop_date_and_time.part.1
.L25:
.LBE19:
	ldr	r3, .L30+12
	ldr	r2, [r3, #0]
	ldr	r3, .L30+16
	str	r2, [r3, #0]
	ldr	r3, .L30+20
	ldr	r2, [r3, #0]
	ldr	r3, .L30+24
	str	r2, [r3, #0]
	ldr	r3, .L30+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L23
.L24:
.LBB20:
	mov	r0, sp
	bl	EPSON_obtain_latest_time_and_date
	ldr	r5, [sp, #0]
	mov	r1, #600
	mov	r0, r5
	bl	__udivsi3
	mov	r1, #600
	mul	r4, r1, r0
	mov	r0, r5
	bl	__umodsi3
	ldr	r3, .L30+28
	ldr	r2, .L30+24
	cmp	r0, #300
	addcs	r4, r4, #600
	cmp	r4, r3
	subhi	r4, r4, #71680
	subhi	r4, r4, #320
	strhi	r4, [r2, #0]
	addls	r4, r4, #14400
	ldrhih	r2, [sp, #4]
	ldr	r3, .L30+16
	strls	r4, [r2, #0]
	ldrlsh	r2, [sp, #4]
	addhi	r2, r2, #1
	str	r2, [r3, #0]
.L23:
.LBE20:
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L31:
	.align	2
.L30:
	.word	GuiVar_LightIsEnergized
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	.LANCHOR1
	.word	71999
.LFE3:
	.size	nm_LIGHTS_TEST_load_editable_stop_date_and_time, .-nm_LIGHTS_TEST_load_editable_stop_date_and_time
	.section	.text.FDTO_LIGHTS_TEST_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_LIGHTS_TEST_draw_screen
	.type	FDTO_LIGHTS_TEST_draw_screen, %function
FDTO_LIGHTS_TEST_draw_screen:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L35
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI5:
	ldr	r2, .L35+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L35+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L35+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L35+4
	ldr	r3, .L35+16
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	ldreq	r3, .L35+20
	ldreqsh	r4, [r3, #0]
	beq	.L34
	bl	nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars
	bl	nm_LIGHTS_TEST_load_editable_stop_date_and_time
	mov	r4, #0
.L34:
	ldr	r6, .L35+24
	ldr	r5, .L35+28
	ldr	r0, [r6, #0]
	ldr	r1, [r5, #0]
	bl	LIGHTS_get_lights_array_index
	bl	LIGHTS_copy_light_struct_into_guivars
	ldr	r1, [r5, #0]
	ldr	r0, [r6, #0]
	bl	LIGHTS_get_lights_array_index
	mov	r1, #0
	bl	LIGHTS_populate_group_name
	bl	nm_LIGHTS_TEST_update_stop_date_and_time_panels
	ldr	r3, .L35+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L35
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #32
	mov	r1, r4
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L36:
	.align	2
.L35:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	387
	.word	irri_lights_recursive_MUTEX
	.word	390
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
.LFE6:
	.size	FDTO_LIGHTS_TEST_draw_screen, .-FDTO_LIGHTS_TEST_draw_screen
	.section	.text.FDTO_LIGHTS_TEST_update_screen,"ax",%progbits
	.align	2
	.global	FDTO_LIGHTS_TEST_update_screen
	.type	FDTO_LIGHTS_TEST_update_screen, %function
FDTO_LIGHTS_TEST_update_screen:
.LFB7:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L41
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI6:
	ldr	r5, [r3, #0]
	ldr	r3, .L41+4
	ldr	r4, .L41+8
	ldr	r8, .L41+12
	ldr	r7, .L41+16
	ldr	r2, .L41+20
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #444
	ldr	r6, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r7, #0]
	ldr	r0, [r8, #0]
	bl	LIGHTS_get_lights_array_index
	bl	IRRI_LIGHTS_light_is_energized
	sub	r3, r0, #1
	rsbs	r0, r3, #0
	adc	r0, r0, r3
	cmp	r0, #0
	str	r0, [r4, #0]
	beq	.L38
	ldr	r1, [r7, #0]
	ldr	r0, [r8, #0]
	bl	LIGHTS_get_lights_array_index
	mov	r1, r0
	mov	r0, sp
	bl	IRRI_LIGHTS_get_light_stop_date_and_time
	ldrh	r2, [sp, #4]
	ldr	r3, .L41+24
	str	r2, [r3, #0]
	ldr	r2, [sp, #0]
	ldr	r3, .L41+28
	str	r2, [r3, #0]
	bl	nm_LIGHTS_TEST_update_stop_date_and_time_panels
.L38:
	ldr	r3, .L41+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L41+8
	ldr	r3, [r3, #0]
	cmp	r6, r3
	bne	.L39
	ldr	r3, .L41
	ldr	r3, [r3, #0]
	cmp	r5, r3
	beq	.L40
.L39:
	mov	r0, #0
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	Redraw_Screen
.L40:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	Refresh_Screen
.L42:
	.align	2
.L41:
	.word	GuiVar_LightsStopTimeHasBeenEdited
	.word	irri_lights_recursive_MUTEX
	.word	GuiVar_LightIsEnergized
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	.LC0
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE7:
	.size	FDTO_LIGHTS_TEST_update_screen, .-FDTO_LIGHTS_TEST_update_screen
	.section	.text.LIGHTS_TEST_process_screen,"ax",%progbits
	.align	2
	.global	LIGHTS_TEST_process_screen
	.type	LIGHTS_TEST_process_screen, %function
LIGHTS_TEST_process_screen:
.LFB8:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	mov	r4, r0
	sub	sp, sp, #32
.LCFI8:
	beq	.L49
	bhi	.L53
	cmp	r0, #1
	beq	.L46
	bcc	.L45
	cmp	r0, #2
	beq	.L47
	cmp	r0, #3
	bne	.L44
	b	.L48
.L53:
	cmp	r0, #67
	beq	.L51
	bhi	.L54
	cmp	r0, #16
	beq	.L50
	cmp	r0, #20
	bne	.L44
	b	.L50
.L54:
	cmp	r0, #80
	beq	.L52
	cmp	r0, #84
	bne	.L44
	b	.L52
.L47:
	ldr	r3, .L99
	ldrsh	r3, [r3, #0]
	cmp	r3, #2
	beq	.L56
	cmp	r3, #3
	bne	.L91
	b	.L97
.L56:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	bne	.L58
	bl	bad_key_beep
	mov	r0, #592
	b	.L88
.L58:
	ldr	r3, .L99+4
	ldr	r4, .L99+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r3, .L99+12
	ldrne	r0, [r4, #0]
	ldrne	r1, [r3, #0]
	movne	r2, #0
	bne	.L90
.L60:
	ldr	r5, [r4, #0]
	bl	FLOWSENSE_get_controller_index
	cmp	r5, r0
	bne	.L61
	ldr	r3, .L99+16
	ldr	r3, [r3, #112]
	cmp	r3, #1
	bne	.L61
	bl	bad_key_beep
	ldr	r0, .L99+20
.L88:
	bl	DIALOG_draw_ok_dialog
	b	.L92
.L61:
	ldr	r0, [r4, #0]
	b	.L89
.L97:
	ldr	r3, .L99+8
	ldr	r0, [r3, #0]
.L89:
	ldr	r3, .L99+12
	mov	r2, #1
	ldr	r1, [r3, #0]
.L90:
	bl	LIGHTS_TEST_turn_light_on_or_off
	b	.L92
.L50:
	bl	good_key_beep
	ldr	r3, .L99+24
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L99+28
	ldr	r3, .L99+32
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L99+36
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L99+28
	ldr	r3, .L99+40
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #20
	ldr	r0, .L99+8
	ldr	r1, .L99+12
	bne	.L63
	bl	nm_LIGHTS_get_next_available_light
	b	.L64
.L63:
	bl	nm_LIGHTS_get_prev_available_light
.L64:
	ldr	r3, .L99+8
	ldr	r0, [r3, #0]
	ldr	r3, .L99+12
	ldr	r1, [r3, #0]
	bl	LIGHTS_get_lights_array_index
	bl	LIGHTS_copy_light_struct_into_guivars
	bl	nm_LIGHTS_TEST_load_editable_stop_date_and_time
	ldr	r3, .L99+36
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L94:
	ldr	r3, .L99+24
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L92:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L43
.L52:
	ldr	r3, .L99
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L66
	cmp	r3, #1
	bne	.L93
	b	.L98
.L66:
	bl	good_key_beep
	ldr	r3, .L99+24
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L99+28
	mov	r3, #568
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #84
	ldr	r0, .L99+8
	ldr	r1, .L99+12
	bne	.L68
	bl	nm_LIGHTS_get_next_available_light
	b	.L69
.L68:
	bl	nm_LIGHTS_get_prev_available_light
.L69:
	ldr	r3, .L99+8
	ldr	r0, [r3, #0]
	ldr	r3, .L99+12
	ldr	r1, [r3, #0]
	bl	LIGHTS_get_lights_array_index
	bl	LIGHTS_copy_light_struct_into_guivars
	bl	nm_LIGHTS_TEST_load_editable_stop_date_and_time
	b	.L94
.L98:
.LBB23:
	mov	r0, sp
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, .L99+44
	cmp	r4, #84
	ldrh	r2, [r3, #0]
	ldr	r3, .L99+48
	strh	r2, [sp, #12]	@ movhi
	ldr	r3, [r3, #0]
	str	r3, [sp, #8]
	bne	.L70
	ldr	r1, .L99+52
	add	r0, sp, #8
	cmp	r3, r1
	addls	r3, r3, #600
	movhi	r3, #0
	strls	r3, [sp, #8]
	strhi	r3, [sp, #8]
	ldrh	r3, [sp, #4]
	addhi	r2, r2, #1
	add	r3, r3, #1
	strh	r3, [sp, #28]	@ movhi
	ldr	r3, [sp, #0]
	strhih	r2, [sp, #12]	@ movhi
	str	r3, [sp, #24]
	add	r1, sp, #24
	b	.L96
.L70:
	cmp	r4, #80
	bne	.L92
	cmp	r3, #600
	subhi	r3, r3, #600
	movls	r3, #0
	subls	r2, r2, #1
	strhi	r3, [sp, #8]
	strls	r3, [sp, #8]
	strlsh	r2, [sp, #12]	@ movhi
	ldr	r3, .L99+56
	ldr	r2, [sp, #0]
	add	r0, sp, #16
	cmp	r2, r3
	ldrh	r3, [sp, #4]
	addls	r2, r2, #1200
	addhi	r3, r3, #1
	strhih	r3, [sp, #20]	@ movhi
	add	r1, sp, #8
	movhi	r3, #1200
	strlsh	r3, [sp, #20]	@ movhi
	strls	r2, [sp, #16]
	strhi	r3, [sp, #16]
.L96:
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #1
	bne	.L79
.L91:
	bl	bad_key_beep
	b	.L92
.L79:
	bl	good_key_beep
	ldrh	r2, [sp, #12]
	ldr	r3, .L99+44
	str	r2, [r3, #0]
	ldr	r2, [sp, #8]
	ldr	r3, .L99+48
	str	r2, [r3, #0]
	b	.L92
.L49:
.LBE23:
	ldr	r3, .L99
	mov	r0, #1
	ldrsh	r3, [r3, #0]
	cmp	r3, #3
	bne	.L46
	mov	r1, r0
	bl	CURSOR_Select
	b	.L43
.L45:
	ldr	r3, .L99
	ldrsh	r3, [r3, #0]
	cmp	r3, #2
	bne	.L48
.L93:
	bl	bad_key_beep
	b	.L43
.L46:
	bl	CURSOR_Up
	b	.L43
.L48:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L43
.L51:
	ldr	r3, .L99+60
	ldr	r2, .L99+64
	ldr	r3, [r3, #0]
	mov	r0, #36
	mla	r3, r0, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L99+68
	str	r2, [r3, #0]
.L44:
	mov	r0, r4
	bl	KEY_process_global_keys
.L43:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, pc}
.L100:
	.align	2
.L99:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_LightIsEnergized
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	tpmicro_comm
	.word	605
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	534
	.word	irri_lights_recursive_MUTEX
	.word	537
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	85799
	.word	85199
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE8:
	.size	LIGHTS_TEST_process_screen, .-LIGHTS_TEST_process_screen
	.section	.bss.g_LIGHTS_TEST_irri_list_stop_date,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_LIGHTS_TEST_irri_list_stop_date, %object
	.size	g_LIGHTS_TEST_irri_list_stop_date, 4
g_LIGHTS_TEST_irri_list_stop_date:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_test_lights.c\000"
.LC1:
	.ascii	"%s, %s\000"
	.section	.bss.g_LIGHTS_TEST_editable_stop_date,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_LIGHTS_TEST_editable_stop_date, %object
	.size	g_LIGHTS_TEST_editable_stop_date, 4
g_LIGHTS_TEST_editable_stop_date:
	.space	4
	.section	.bss.g_LIGHTS_TEST_irri_list_stop_time_in_seconds,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_LIGHTS_TEST_irri_list_stop_time_in_seconds, %object
	.size	g_LIGHTS_TEST_irri_list_stop_time_in_seconds, 4
g_LIGHTS_TEST_irri_list_stop_time_in_seconds:
	.space	4
	.section	.bss.g_LIGHTS_TEST_editable_stop_time_in_seconds,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_LIGHTS_TEST_editable_stop_time_in_seconds, %object
	.size	g_LIGHTS_TEST_editable_stop_time_in_seconds, 4
g_LIGHTS_TEST_editable_stop_time_in_seconds:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI1-.LFB10
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI6-.LFB7
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI7-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_test_lights.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xcc
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x45
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x9f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x73
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.byte	0xb9
	.byte	0x1
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	0x29
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x1
	.byte	0xd3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x3
	.4byte	0x39
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x17f
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1a9
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST5
	.uleb128 0x6
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x10e
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1db
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST6
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB10
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB7
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB8
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI8
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_test_lights.c\000"
.LASF10:
	.ascii	"nm_LIGHTS_TEST_update_stop_date_and_time_panels\000"
.LASF4:
	.ascii	"FDTO_LIGHTS_TEST_draw_screen\000"
.LASF0:
	.ascii	"LIGHTS_TEST_turn_light_on_or_off\000"
.LASF3:
	.ascii	"nm_LIGHTS_TEST_load_editable_stop_date_and_time\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"FDTO_LIGHTS_TEST_update_screen\000"
.LASF2:
	.ascii	"LIGHTS_TEST_calculate_default_off_date_and_time\000"
.LASF6:
	.ascii	"LIGHTS_TEST_process_manual_stop_date_and_time\000"
.LASF7:
	.ascii	"LIGHTS_TEST_process_screen\000"
.LASF1:
	.ascii	"nm_LIGHTS_TEST_load_irri_list_stop_date_and_time\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
