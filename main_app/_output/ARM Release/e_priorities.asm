	.file	"e_priorities.c"
	.text
.Ltext0:
	.section	.text.FDTO_PRIORITY_populate_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_PRIORITY_populate_dropdown, %function
FDTO_PRIORITY_populate_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r2, r0, asl #16
	ldr	r3, .L2
	mov	r2, r2, asr #16
	add	r2, r2, #1
	str	r2, [r3, #0]
	bx	lr
.L3:
	.align	2
.L2:
	.word	GuiVar_ComboBoxItemIndex
.LFE0:
	.size	FDTO_PRIORITY_populate_dropdown, .-FDTO_PRIORITY_populate_dropdown
	.section	.text.FDTO_PRIORITY_show_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_PRIORITY_show_dropdown, %function
FDTO_PRIORITY_show_dropdown:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L5
	ldr	r0, .L5+4
	ldr	r3, [r3, #0]
	ldr	r1, .L5+8
	ldr	r3, [r3, #0]
	mov	r2, #3
	sub	r3, r3, #1
	b	FDTO_COMBOBOX_show
.L6:
	.align	2
.L5:
	.word	.LANCHOR0
	.word	743
	.word	FDTO_PRIORITY_populate_dropdown
.LFE1:
	.size	FDTO_PRIORITY_show_dropdown, .-FDTO_PRIORITY_show_dropdown
	.section	.text.FDTO_PRIORITY_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_PRIORITY_draw_screen
	.type	FDTO_PRIORITY_draw_screen, %function
FDTO_PRIORITY_draw_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L10
	str	lr, [sp, #-4]!
.LCFI0:
	ldrnesh	r1, [r3, #0]
	bne	.L9
	bl	PRIORITY_copy_group_into_guivars
	mov	r1, #0
.L9:
	mov	r0, #49
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L11:
	.align	2
.L10:
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_PRIORITY_draw_screen, .-FDTO_PRIORITY_draw_screen
	.section	.text.PRIORITY_process_screen,"ax",%progbits
	.align	2
	.global	PRIORITY_process_screen
	.type	PRIORITY_process_screen, %function
PRIORITY_process_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L58
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L58+4
	sub	sp, sp, #44
.LCFI2:
	cmp	r2, r3
	mov	r4, r0
	mov	r5, r1
	bne	.L52
	ldr	r4, .L58+8
	ldr	r1, [r4, #0]
	bl	COMBO_BOX_key_press
	ldr	r3, [r4, #0]
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	bl	Refresh_Screen
	b	.L12
.L52:
	cmp	r0, #4
	beq	.L18
	bhi	.L22
	cmp	r0, #1
	beq	.L18
	bcc	.L17
	cmp	r0, #2
	beq	.L19
	cmp	r0, #3
	b	.L57
.L22:
	cmp	r0, #67
	beq	.L20
	bhi	.L23
	cmp	r0, #16
	beq	.L18
	cmp	r0, #20
.L57:
	bne	.L16
	b	.L17
.L23:
	cmp	r0, #80
	beq	.L21
	cmp	r0, #84
	bne	.L16
	b	.L21
.L19:
	ldr	r3, .L58+12
	ldrsh	r2, [r3, #0]
	ldr	r3, .L58+8
	cmp	r2, #9
	ldrls	pc, [pc, r2, asl #2]
	b	.L24
.L35:
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
.L25:
	ldr	r2, .L58+16
	b	.L53
.L26:
	ldr	r2, .L58+20
	b	.L53
.L27:
	ldr	r2, .L58+24
	b	.L53
.L28:
	ldr	r2, .L58+28
	b	.L53
.L29:
	ldr	r2, .L58+32
	b	.L53
.L30:
	ldr	r2, .L58+36
	b	.L53
.L31:
	ldr	r2, .L58+40
	b	.L53
.L32:
	ldr	r2, .L58+44
	b	.L53
.L33:
	ldr	r2, .L58+48
	b	.L53
.L34:
	ldr	r2, .L58+52
.L53:
	str	r2, [r3, #0]
.L24:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L36
	bl	good_key_beep
	ldr	r3, .L58+12
	ldr	r2, .L58+56
	ldrh	r3, [r3, #0]
	add	r0, sp, #8
	cmp	r3, #0
	movne	r3, r3, asl #16
	movne	r3, r3, asr #16
	addne	r3, r3, r3, asl #3
	movne	r3, r3, asl #1
	str	r3, [r2, #0]
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L58+60
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L12
.L36:
	bl	bad_key_beep
	b	.L12
.L21:
	ldr	r3, .L58+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L39
.L50:
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
.L40:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L58+16
	mov	r0, r4
	b	.L55
.L41:
	ldr	r1, .L58+20
	mov	r2, #1
	mov	r3, #0
	mov	r0, r4
	stmia	sp, {r2, r3}
.L55:
	mov	r3, #3
	bl	process_uns32
	b	.L51
.L42:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L58+24
	mov	r0, r4
	b	.L55
.L43:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L58+28
	mov	r0, r4
	b	.L55
.L44:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L58+32
	mov	r0, r4
	b	.L55
.L45:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L58+36
	mov	r0, r4
	b	.L55
.L46:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L58+40
	mov	r0, r4
	b	.L55
.L47:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L58+44
	mov	r0, r4
	b	.L55
.L48:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L58+48
	mov	r0, r4
	b	.L55
.L49:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L58+52
	mov	r0, r4
	b	.L55
.L39:
	bl	bad_key_beep
.L51:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L12
.L18:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L12
.L17:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L12
.L20:
	ldr	r3, .L58+64
	mov	r2, #1
	str	r2, [r3, #0]
	bl	PRIORITY_extract_and_store_changes_from_GuiVars
.L16:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L12:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L59:
	.align	2
.L58:
	.word	GuiLib_CurStructureNdx
	.word	743
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_ComboBox_Y1
	.word	FDTO_PRIORITY_show_dropdown
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	PRIORITY_process_screen, .-PRIORITY_process_screen
	.section	.bss.g_PRIORITY_combo_box_guivar,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_PRIORITY_combo_box_guivar, %object
	.size	g_PRIORITY_combo_box_guivar, 4
g_PRIORITY_combo_box_guivar:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_priorities.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x43
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x57
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x5d
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x7d
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"FDTO_PRIORITY_populate_dropdown\000"
.LASF3:
	.ascii	"PRIORITY_process_screen\000"
.LASF2:
	.ascii	"FDTO_PRIORITY_draw_screen\000"
.LASF1:
	.ascii	"FDTO_PRIORITY_show_dropdown\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_priorities.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
