	.file	"two_wire_utils.c"
	.text
.Ltext0:
	.section	.text.TWO_WIRE_jump_to_decoder_list,"ax",%progbits
	.align	2
	.global	TWO_WIRE_jump_to_decoder_list
	.type	TWO_WIRE_jump_to_decoder_list, %function
TWO_WIRE_jump_to_decoder_list:
.LFB0:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	mov	r4, r0
	mov	r0, #0
	sub	sp, sp, #36
.LCFI1:
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L7
	mov	r2, #2
	cmp	r4, #2
	str	r0, [r3, #0]
	mov	r3, #11
	stmia	sp, {r2, r3}
	mov	r3, #1
	str	r3, [sp, #24]
	str	r0, [sp, #32]
	beq	.L3
	cmp	r4, #8
	bne	.L1
	b	.L6
.L3:
	mov	r3, #66
	str	r3, [sp, #8]
	ldr	r3, .L7+4
	str	r3, [sp, #20]
	ldr	r3, .L7+8
	str	r3, [sp, #16]
	ldr	r3, .L7+12
	b	.L5
.L6:
	mov	r3, #65
	str	r3, [sp, #8]
	ldr	r3, .L7+16
	str	r3, [sp, #20]
	ldr	r3, .L7+20
	str	r3, [sp, #16]
	ldr	r3, .L7+24
.L5:
	mov	r0, sp
	str	r3, [sp, #12]
	bl	Change_Screen
.L1:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L8:
	.align	2
.L7:
	.word	GuiVar_MenuScreenToShow
	.word	FDTO_TWO_WIRE_STA_draw_menu
	.word	TWO_WIRE_STA_process_menu
	.word	TWO_WIRE_STA_load_decoder_serial_number_into_guivar
	.word	FDTO_TWO_WIRE_POC_draw_menu
	.word	TWO_WIRE_POC_process_menu
	.word	TWO_WIRE_POC_load_decoder_serial_number_into_guivar
.LFE0:
	.size	TWO_WIRE_jump_to_decoder_list, .-TWO_WIRE_jump_to_decoder_list
	.section	.text.TWO_WIRE_turn_cable_power_on_or_off_from_ui,"ax",%progbits
	.align	2
	.global	TWO_WIRE_turn_cable_power_on_or_off_from_ui
	.type	TWO_WIRE_turn_cable_power_on_or_off_from_ui, %function
TWO_WIRE_turn_cable_power_on_or_off_from_ui:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L11
	ldr	r2, .L11+4
	ldr	r1, [r3, r2]
	cmp	r1, #0
	bne	.L10
	mov	r1, #1
	str	r1, [r3, r2]
	add	r2, r2, #4
	str	r0, [r3, r2]
	b	good_key_beep
.L10:
	b	bad_key_beep
.L12:
	.align	2
.L11:
	.word	tpmicro_data
	.word	5020
.LFE1:
	.size	TWO_WIRE_turn_cable_power_on_or_off_from_ui, .-TWO_WIRE_turn_cable_power_on_or_off_from_ui
	.section	.text.TWO_WIRE_perform_discovery_process,"ax",%progbits
	.align	2
	.global	TWO_WIRE_perform_discovery_process
	.type	TWO_WIRE_perform_discovery_process, %function
TWO_WIRE_perform_discovery_process:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	mov	r5, r1
	mov	r4, r0
	bl	FLOWSENSE_get_controller_index
	mov	r6, r0
	bl	COMM_MNGR_network_is_available_for_normal_use
	ldr	r3, .L24
	cmp	r0, #0
	beq	.L14
	mov	r2, #92
	mla	r2, r6, r2, r3
	ldr	r2, [r2, #80]
	cmp	r2, #0
	beq	.L14
	ldr	r3, .L24+4
	ldr	r1, .L24+8
	ldr	r2, [r3, r1]
	cmp	r2, #0
	bne	.L23
	ldr	r0, .L24+12
	add	r6, r6, #24
	str	r2, [r0, r6, asl #2]
	mov	ip, #3
	mov	r0, #5056
	str	ip, [r3, r0]
	sub	r0, r0, #8
	str	r2, [r3, r0]
	cmp	r5, #0
	mov	r2, #1
	str	r2, [r3, r1]
	beq	.L16
	bl	good_key_beep
.L16:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	TWO_WIRE_init_discovery_dialog
.L14:
	mov	r2, #92
	mla	r6, r2, r6, r3
	ldr	r3, [r6, #80]
	cmp	r3, #0
	ldreq	r0, .L24+16
	ldrne	r0, .L24+20
	bl	Alert_Message
.L23:
	cmp	r5, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldmfd	sp!, {r4, r5, r6, lr}
	b	bad_key_beep
.L25:
	.align	2
.L24:
	.word	chain
	.word	tpmicro_data
	.word	5028
	.word	irri_comm
	.word	.LC0
	.word	.LC1
.LFE2:
	.size	TWO_WIRE_perform_discovery_process, .-TWO_WIRE_perform_discovery_process
	.section	.text.TWO_WIRE_clear_statistics_at_all_decoders_from_ui,"ax",%progbits
	.align	2
	.global	TWO_WIRE_clear_statistics_at_all_decoders_from_ui
	.type	TWO_WIRE_clear_statistics_at_all_decoders_from_ui, %function
TWO_WIRE_clear_statistics_at_all_decoders_from_ui:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L28
	ldr	r3, .L28+4
	ldr	r1, [r2, r3]
	cmp	r1, #0
	bne	.L27
	mov	r1, #1
	str	r1, [r2, r3]
	b	good_key_beep
.L27:
	b	bad_key_beep
.L29:
	.align	2
.L28:
	.word	tpmicro_data
	.word	5032
.LFE3:
	.size	TWO_WIRE_clear_statistics_at_all_decoders_from_ui, .-TWO_WIRE_clear_statistics_at_all_decoders_from_ui
	.section	.text.TWO_WIRE_request_statistics_from_all_decoders_from_ui,"ax",%progbits
	.align	2
	.global	TWO_WIRE_request_statistics_from_all_decoders_from_ui
	.type	TWO_WIRE_request_statistics_from_all_decoders_from_ui, %function
TWO_WIRE_request_statistics_from_all_decoders_from_ui:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L32
	ldr	r3, .L32+4
	ldr	r1, [r2, r3]
	cmp	r1, #0
	bne	.L31
	mov	r1, #1
	str	r1, [r2, r3]
	b	good_key_beep
.L31:
	b	bad_key_beep
.L33:
	.align	2
.L32:
	.word	tpmicro_data
	.word	5036
.LFE4:
	.size	TWO_WIRE_request_statistics_from_all_decoders_from_ui, .-TWO_WIRE_request_statistics_from_all_decoders_from_ui
	.section	.text.TWO_WIRE_all_decoder_loopback_test_from_ui,"ax",%progbits
	.align	2
	.global	TWO_WIRE_all_decoder_loopback_test_from_ui
	.type	TWO_WIRE_all_decoder_loopback_test_from_ui, %function
TWO_WIRE_all_decoder_loopback_test_from_ui:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r2, .L41
	ldreq	r2, .L41+4
	ldr	r3, .L41+8
	ldr	r1, [r3, r2]
	cmp	r1, #0
	bne	.L37
	mov	r1, #1
	str	r1, [r3, r2]
	b	good_key_beep
.L37:
	b	bad_key_beep
.L42:
	.align	2
.L41:
	.word	5040
	.word	5044
	.word	tpmicro_data
.LFE5:
	.size	TWO_WIRE_all_decoder_loopback_test_from_ui, .-TWO_WIRE_all_decoder_loopback_test_from_ui
	.section	.text.TWO_WIRE_decoder_solenoid_operation_from_ui,"ax",%progbits
	.align	2
	.global	TWO_WIRE_decoder_solenoid_operation_from_ui
	.type	TWO_WIRE_decoder_solenoid_operation_from_ui, %function
TWO_WIRE_decoder_solenoid_operation_from_ui:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	ldr	r3, .L47
	ldr	r4, .L47+4
	ldr	ip, [r3, r4]
	cmp	ip, #0
	bne	.L44
	mov	r5, #1
	str	r5, [r3, r4]
	add	r4, r4, #4
	str	r0, [r3, r4]
	ldr	r0, .L47+8
	cmp	r1, #0
	str	r1, [r3, r0]
	add	r0, r0, #4
	str	r2, [r3, r0]
	ldr	r3, .L47+12
	ldr	r0, .L47+16
	streq	r2, [r3, #0]
	streq	r1, [r0, #0]
	strne	r2, [r0, #0]
	strne	ip, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	good_key_beep
.L44:
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L48:
	.align	2
.L47:
	.word	tpmicro_data
	.word	5076
	.word	5084
	.word	GuiVar_TwoWireOutputOnA
	.word	GuiVar_TwoWireOutputOnB
.LFE6:
	.size	TWO_WIRE_decoder_solenoid_operation_from_ui, .-TWO_WIRE_decoder_solenoid_operation_from_ui
	.global	decoder_info_for_display
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"2-Wire Discovery Skipped - 2-Wire option missing\000"
.LC1:
	.ascii	"2-Wire Discovery Skipped - network not ready\000"
	.section	.bss.decoder_info_for_display,"aw",%nobits
	.align	2
	.type	decoder_info_for_display, %object
	.size	decoder_info_for_display, 640
decoder_info_for_display:
	.space	640
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI3-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/two_wire_utils.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa9
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x2b
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x63
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x84
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xe3
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x101
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x122
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x155
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB6
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"TWO_WIRE_clear_statistics_at_all_decoders_from_ui\000"
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/two_wire_utils.c\000"
.LASF0:
	.ascii	"TWO_WIRE_jump_to_decoder_list\000"
.LASF1:
	.ascii	"TWO_WIRE_turn_cable_power_on_or_off_from_ui\000"
.LASF4:
	.ascii	"TWO_WIRE_request_statistics_from_all_decoders_from_"
	.ascii	"ui\000"
.LASF5:
	.ascii	"TWO_WIRE_all_decoder_loopback_test_from_ui\000"
.LASF6:
	.ascii	"TWO_WIRE_decoder_solenoid_operation_from_ui\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"TWO_WIRE_perform_discovery_process\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
