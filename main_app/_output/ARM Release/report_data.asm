	.file	"report_data.c"
	.text
.Ltext0:
	.section	.text.add_amount_to_budget_record,"ax",%progbits
	.align	2
	.type	add_amount_to_budget_record, %function
add_amount_to_budget_record:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	fmdrr	d7, r0, r1
	fldd	d6, [r2, #376]
	ldr	r1, [r2, #24]
	ldr	r3, .L8
	faddd	d6, d6, d7
	ldr	r3, [r1, r3]
	tst	r3, #2
	fstd	d6, [r2, #376]
	beq	.L2
	fldd	d6, [r2, #400]
	faddd	d7, d6, d7
	fstd	d7, [r2, #400]
	bx	lr
.L2:
	tst	r3, #4
	beq	.L4
	fldd	d6, [r2, #408]
	faddd	d7, d6, d7
	fstd	d7, [r2, #408]
	bx	lr
.L4:
	tst	r3, #16
	beq	.L5
	fldd	d6, [r2, #416]
	faddd	d7, d6, d7
	fstd	d7, [r2, #416]
	bx	lr
.L5:
	tst	r3, #32
	beq	.L6
	fldd	d6, [r2, #424]
	faddd	d7, d6, d7
	fstd	d7, [r2, #424]
	bx	lr
.L6:
	tst	r3, #64
	beq	.L7
	fldd	d6, [r2, #432]
	faddd	d7, d6, d7
	fstd	d7, [r2, #432]
	bx	lr
.L7:
	tst	r3, #128
	flddne	d6, [r2, #440]
	fadddne	d7, d6, d7
	fstdne	d7, [r2, #440]
	bx	lr
.L9:
	.align	2
.L8:
	.word	14176
.LFE11:
	.size	add_amount_to_budget_record, .-add_amount_to_budget_record
	.global	__udivsi3
	.section	.text.REPORTS_extract_and_store_changes,"ax",%progbits
	.align	2
	.global	REPORTS_extract_and_store_changes
	.type	REPORTS_extract_and_store_changes, %function
REPORTS_extract_and_store_changes:
.LFB10:
	@ args = 0, pretend = 0, frame = 1520
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #38
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI0:
	mov	r4, r1
	sub	sp, sp, #1520
.LCFI1:
	mov	r6, r2
	bhi	.L22
	cmp	r0, #37
	bcs	.L16
	cmp	r0, #10
	bhi	.L23
	cmp	r0, #9
	bcs	.L13
	sub	r0, r0, #6
	cmp	r0, #1
	bhi	.L44
	b	.L52
.L23:
	cmp	r0, #12
	bcc	.L44
	cmp	r0, #13
	bls	.L14
	sub	r0, r0, #34
	cmp	r0, #1
	bhi	.L44
	b	.L53
.L22:
	cmp	r0, #148
	bhi	.L24
	cmp	r0, #147
	bcs	.L19
	cmp	r0, #115
	bcc	.L44
	cmp	r0, #116
.LBB32:
	movls	r6, r2, lsr #4
	movls	r5, #0
.LBE32:
	bls	.L35
	sub	r0, r0, #118
	cmp	r0, #1
	bhi	.L44
	b	.L54
.L24:
	cmp	r0, #150
	bcc	.L44
	cmp	r0, #151
	bls	.L20
	sub	r0, r0, #200
	cmp	r0, #1
	bhi	.L44
	b	.L55
.L52:
.LBB33:
.LBB34:
	sub	r0, r2, #4
	mov	r1, #24
	bl	__udivsi3
	mov	r1, r4
	mov	r2, #4
	add	r4, r4, #4
	mov	r5, #0
	mov	r6, r0
	add	r0, sp, #1504
	bl	memcpy
	b	.L25
.L26:
	mov	r1, r4
	add	r0, sp, #1440
	mov	r2, #24
	bl	memcpy
	add	r4, r4, #24
	add	r5, r5, #1
.L25:
	cmp	r5, r6
	bne	.L26
	mov	r3, #24
	mul	r5, r3, r5
	add	r5, r5, #4
	b	.L11
.L13:
.LBE34:
.LBE33:
.LBB35:
	mov	r0, r2
	mov	r1, #60
	bl	__udivsi3
	mov	r5, #0
	mov	r6, r0
	b	.L27
.L28:
	add	r0, sp, #1264
	mov	r1, r4
	add	r0, r0, #8
	mov	r2, #60
	bl	memcpy
	add	r4, r4, #60
	add	r5, r5, #1
.L27:
	cmp	r5, r6
	bne	.L28
	b	.L51
.L14:
.LBE35:
.LBB36:
	mov	r0, r2
	mov	r1, #48
	bl	__udivsi3
	mov	r5, #0
	mov	r6, r0
	b	.L29
.L30:
	mov	r1, r4
	add	r0, sp, #1392
	mov	r2, #48
	bl	memcpy
	add	r4, r4, #48
	add	r5, r5, #1
.L29:
	cmp	r5, r6
	bne	.L30
	mov	r3, #48
	b	.L50
.L53:
.LBE36:
.LBB37:
	mov	r0, r2
	mov	r1, #60
	bl	__udivsi3
	mov	r5, #0
	mov	r6, r0
	b	.L31
.L32:
	add	r0, sp, #1328
	mov	r1, r4
	add	r0, r0, #4
	mov	r2, #60
	bl	memcpy
	add	r4, r4, #60
	add	r5, r5, #1
.L31:
	cmp	r5, r6
	bne	.L32
.L51:
	mov	r3, #60
	b	.L50
.L16:
.LBE37:
.LBB38:
	mov	r0, r2
	mov	r1, #76
	bl	__udivsi3
	mov	r5, #0
	mov	r6, r0
	b	.L33
.L34:
	add	r0, sp, #1184
	mov	r1, r4
	add	r0, r0, #12
	mov	r2, #76
	bl	memcpy
	add	r4, r4, #76
	add	r5, r5, #1
.L33:
	cmp	r5, r6
	bne	.L34
	mov	r3, #76
	b	.L50
.L36:
.LBE38:
.LBB39:
	mov	r1, r4
	add	r0, sp, #1488
	mov	r2, #16
	bl	memcpy
	add	r4, r4, #16
	add	r5, r5, #1
.L35:
	cmp	r5, r6
	bne	.L36
	mov	r5, r5, asl #4
	b	.L11
.L54:
.LBE39:
.LBB40:
.LBB41:
	mov	r2, #4
	add	r0, sp, #1504
	bl	memcpy
	add	r0, sp, #1504
	add	r1, r4, #4
	add	r0, r0, #4
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #8
	mov	r6, #0
	mov	r5, #8
	b	.L37
.L38:
	add	r0, sp, #1504
	mov	r1, r4
	mov	r2, #4
	add	r0, r0, #8
	bl	memcpy
	add	r0, sp, #1504
	add	r1, r4, #4
	add	r0, r0, #12
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #1508]
	add	r4, r4, #8
	sub	r3, r3, #1
	add	r5, r5, #8
	add	r6, r6, #1
	str	r3, [sp, #1508]
.L37:
	ldr	r3, [sp, #1504]
	cmp	r6, r3
	bcc	.L38
	b	.L11
.L20:
.LBE41:
.LBE40:
.LBB42:
	mov	r0, r2
	mov	r1, #24
	bl	__udivsi3
	mov	r5, #0
	mov	r6, r0
	b	.L39
.L40:
	add	r0, sp, #1456
	mov	r1, r4
	add	r0, r0, #8
	mov	r2, #24
	bl	memcpy
	add	r4, r4, #24
	add	r5, r5, #1
.L39:
	cmp	r5, r6
	bne	.L40
	mov	r3, #24
	b	.L50
.L19:
.LBE42:
.LBB43:
	mov	r0, r2
	ldr	r1, .L56
	bl	__udivsi3
	mov	r5, #0
	mov	r6, r0
	b	.L41
.L42:
	mov	r1, r4
	mov	r0, sp
	ldr	r2, .L56
	bl	memcpy
	add	r4, r4, #1184
	add	r4, r4, #12
	add	r5, r5, #1
.L41:
	cmp	r5, r6
	bne	.L42
	ldr	r3, .L56
.L50:
	mul	r5, r3, r5
.LBE43:
	b	.L11
.L43:
.LBB44:
.LBB45:
	mov	r1, r4
	mov	r2, #40
	mov	r0, sp
	bl	memcpy
	add	r0, sp, #1504
	add	r1, r4, #40
	mov	r2, #4
	add	r0, r0, #4
	bl	memcpy
	ldr	r2, [sp, #1508]
	add	r4, r4, #44
	mov	r7, #96
	mov	r1, r4
	mul	r2, r7, r2
	mov	r0, r8
	bl	memcpy
	ldr	r3, [sp, #1508]
	add	r0, sp, #1184
	mul	r7, r3, r7
	add	r0, r0, #8
	add	r4, r4, r7
	mov	r1, r4
	mov	r2, #4
	add	r7, r7, #48
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, r7
	b	.L21
.L55:
.LBE45:
.LBE44:
	mov	r5, #0
.LBB47:
.LBB46:
	add	r3, sp, r5
	add	r8, r3, #40
.L21:
	cmp	r5, r6
	bcc	.L43
	b	.L11
.L44:
.LBE46:
.LBE47:
	mov	r5, #0
.L11:
	mov	r0, r5
	add	sp, sp, #1520
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L57:
	.align	2
.L56:
	.word	1196
.LFE10:
	.size	REPORTS_extract_and_store_changes, .-REPORTS_extract_and_store_changes
	.section	.text.REPORT_DATA_maintain_all_accumulators,"ax",%progbits
	.align	2
	.global	REPORT_DATA_maintain_all_accumulators
	.type	REPORT_DATA_maintain_all_accumulators, %function
REPORT_DATA_maintain_all_accumulators:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	fstmfdd	sp!, {d8}
.LCFI3:
	mov	r4, r0
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	ldrb	r2, [r0, #1]	@ zero_extendqisi2
	orr	r2, r3, r2, asl #8
	ldrb	r3, [r0, #2]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #16
	ldrb	r3, [r0, #3]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #24
	ldr	r3, .L119+12
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L59
	ldr	r3, .L119+136
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, .L119+20
	mov	r5, #0
.L61:
	mov	r0, r5, asl #7
	add	r6, r7, r0
	ldrb	r3, [r6, #141]	@ zero_extendqisi2
	add	r6, r6, #140
	tst	r3, #32
	beq	.L60
	add	r0, r0, #76
	add	r0, r7, r0
	mov	r1, r4
	bl	nm_STATION_REPORT_DATA_close_and_start_a_new_record
	ldrb	r3, [r6, #1]	@ zero_extendqisi2
	bic	r3, r3, #32
	strb	r3, [r6, #1]
.L60:
	add	r5, r5, #1
	cmp	r5, #2112
	bne	.L61
	ldr	r3, .L119+136
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L59:
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	orr	r2, r3, r2, asl #8
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #16
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #24
	ldr	r3, .L119+24
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L62
	ldr	r3, .L119+144
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+28
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, .L119+32
	mov	r6, #0
	mov	r8, #472
	mov	r7, r5
.L64:
	ldr	r3, [r5, #24]
	cmp	r3, #0
	beq	.L63
	mla	r0, r8, r6, r7
	mov	r1, r4
	add	r0, r0, #20
	bl	nm_POC_REPORT_DATA_close_and_start_a_new_record
.L63:
	add	r6, r6, #1
	cmp	r6, #12
	add	r5, r5, #472
	bne	.L64
	ldr	r3, .L119+144
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L62:
	ldr	r3, .L119+36
	ldr	r2, .L119+152
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L119+40
	bl	xQueueTakeMutexRecursive_debug
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	orr	r2, r3, r2, asl #8
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #16
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #24
	ldr	r3, .L119+44
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L65
	ldr	r3, .L119+172
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+48
	bl	xQueueTakeMutexRecursive_debug
	ldr	r6, .L119+164
	mov	r5, #0
	mov	r7, #20
.L67:
	add	r3, r6, r5
	ldrb	r3, [r3, #976]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L66
	mla	r0, r7, r5, r6
	mov	r1, r4
	add	r0, r0, #16
	bl	nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record
.L66:
	add	r5, r5, #1
	cmp	r5, #48
	bne	.L67
	ldr	r3, .L119+172
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L65:
	ldr	r3, .L119+36
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	orr	r2, r3, r2, asl #8
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #16
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #24
	ldr	r3, .L119+52
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L68
	ldr	r3, .L119+128
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+56
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, .L119+108
	ldr	r8, .L119+60
	mov	r6, #0
	mov	r7, r5
.L70:
	ldr	r3, [r5, #16]
	cmp	r3, #0
	beq	.L69
	mla	r0, r8, r6, r7
	mov	r1, r4
	add	r0, r0, #16
	bl	nm_SYSTEM_REPORT_close_and_start_a_new_record
.L69:
	add	r6, r6, #1
	add	r5, r5, #14208
	cmp	r6, #4
	add	r5, r5, #16
	bne	.L70
	ldr	r3, .L119+128
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L68:
	ldr	r3, .L119+136
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+64
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L119+132
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+68
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L119+128
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+72
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L119+108
	ldr	r1, .L119+76
	mov	r3, #0
	mov	ip, r2
	mov	r0, r3
.L72:
	ldr	lr, [r2, #16]
	add	r2, r2, #14208
	cmp	lr, #0
	addne	lr, r3, ip
	add	r3, r3, #14208
	addne	lr, lr, #14144
	add	r3, r3, #16
	strne	r0, [lr, #48]
	cmp	r3, r1
	add	r2, r2, #16
	bne	.L72
	ldr	r0, .L119+80
	ldr	r3, [r0, #44]
	cmp	r3, #0
	beq	.L73
	add	r0, r0, #36
	bl	nm_ListGetFirst
	ldr	r6, .L119+32
	mov	r5, r0
	b	.L74
.L88:
	ldr	r3, [r5, #40]
	ldr	r2, .L119+84
	ldrh	r1, [r5, #72]
	ldr	r0, [r3, r2]
	mov	r1, r1, lsr #6
	and	r1, r1, #15
	mov	ip, #1
	orr	r1, r0, ip, asl r1
	str	r1, [r3, r2]
	mov	r2, #468
	ldrh	r7, [r3, r2]
	ldrh	r2, [r5, #80]
	ands	r7, r7, #8064
	bne	.L75
	fmsr	s13, r2	@ int
	fldd	d7, .L119
	ldr	r3, .L119+144
	fsitod	d8, s13
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L119+152
	ldr	r3, .L119+88
	fdivd	d8, d8, d7
	bl	xQueueTakeMutexRecursive_debug
	ldr	r8, .L119+92
	mov	sl, #472
	fcvtsd	s16, d8
.L77:
	ldr	r3, [r8, #-364]
	cmp	r3, #0
	beq	.L76
	ldr	r2, [r8, #-344]
	ldr	r3, [r5, #40]
	ldr	r1, [r2, #0]
	ldr	r2, [r3, #0]
	cmp	r1, r2
	bne	.L76
	ldr	r3, [r3, #468]
	fldd	d6, [r8, #0]
	mla	r2, sl, r7, r6
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	fmsr	s15, r3	@ int
	add	r2, r2, #20
	fsitos	s14, s15
	fdivs	s14, s16, s14
	fcvtds	d7, s14
	faddd	d6, d6, d7
	fmrrd	r0, r1, d7
	fstd	d6, [r8, #0]
	bl	add_amount_to_budget_record
.L76:
	add	r7, r7, #1
	cmp	r7, #12
	add	r8, r8, #472
	bne	.L77
	ldr	r3, .L119+144
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L78
.L75:
	cmp	r2, #0
	beq	.L118
	ldr	r1, [r3, #100]
	cmp	r1, #0
	beq	.L118
	flds	s15, [r3, #308]
	fcmpezs	s15
	fmstat
	ble	.L118
	fmsr	s13, r2	@ int
	fuitos	s16, s13
	fmsr	s13, r1	@ int
	fuitos	s14, s13
	fdivs	s16, s16, s14
	fmuls	s16, s16, s15
	b	.L78
.L118:
	flds	s16, .L119+8
.L78:
	ldrh	r3, [r5, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	sub	r3, r3, #1
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L80
.L87:
	.word	.L81
	.word	.L82
	.word	.L80
	.word	.L83
	.word	.L84
	.word	.L85
	.word	.L86
.L81:
	ldr	r2, [r5, #44]
	ldr	r3, .L119+20
	ldrb	r1, [r4, #5]	@ zero_extendqisi2
	add	r3, r3, r2, asl #7
	flds	s15, [r3, #32]
	ldrb	r2, [r4, #4]	@ zero_extendqisi2
	fadds	s15, s15, s16
	orr	r2, r2, r1, asl #8
	strh	r2, [r3, #48]	@ movhi
	ldrb	r1, [r4, #1]	@ zero_extendqisi2
	ldrb	r2, [r4, #0]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #8
	ldrb	r1, [r4, #2]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #16
	ldrb	r1, [r4, #3]	@ zero_extendqisi2
	fsts	s15, [r3, #32]
	flds	s15, [r3, #80]
	orr	r2, r2, r1, asl #24
	str	r2, [r3, #24]
	ldr	r2, [r3, #28]
	fadds	s15, s15, s16
	add	r2, r2, #1
	str	r2, [r3, #28]
	ldr	r2, [r3, #104]
	add	r2, r2, #1
	str	r2, [r3, #104]
	fsts	s15, [r3, #80]
	ldr	r3, [r5, #40]
	flds	s15, [r3, #68]
	ldr	r2, [r3, #64]
	fadds	s16, s15, s16
	add	r2, r2, #1
	str	r2, [r3, #64]
	fsts	s16, [r3, #68]
	b	.L80
.L82:
	ldr	r2, [r5, #44]
	ldr	r3, .L119+20
	add	r3, r3, r2, asl #7
	flds	s15, [r3, #84]
	ldrh	r2, [r3, #120]
	fadds	s15, s15, s16
	add	r2, r2, #1
	strh	r2, [r3, #120]	@ movhi
	fsts	s15, [r3, #84]
	ldr	r3, [r5, #40]
	flds	s15, [r3, #60]
	ldr	r2, [r3, #56]
	fadds	s16, s15, s16
	add	r2, r2, #1
	str	r2, [r3, #56]
	fsts	s16, [r3, #60]
	b	.L80
.L83:
	ldr	r2, [r5, #44]
	ldr	r3, .L119+20
	add	r3, r3, r2, asl #7
	flds	s15, [r3, #88]
	ldrh	r1, [r3, #118]
	fadds	s15, s15, s16
	add	r1, r1, #1
	strh	r1, [r3, #118]	@ movhi
	fsts	s15, [r3, #88]
	ldr	r3, [r5, #40]
	flds	s15, [r3, #52]
	ldr	r2, [r3, #48]
	fadds	s16, s15, s16
	add	r2, r2, #1
	str	r2, [r3, #48]
	fsts	s16, [r3, #52]
	b	.L80
.L84:
	ldr	r2, [r5, #44]
	ldr	r3, .L119+20
	add	r3, r3, r2, asl #7
	flds	s15, [r3, #92]
	ldrh	r2, [r3, #116]
	fadds	s15, s15, s16
	add	r2, r2, #1
	strh	r2, [r3, #116]	@ movhi
	fsts	s15, [r3, #92]
	ldr	r3, [r5, #40]
	flds	s15, [r3, #44]
	ldr	r2, [r3, #40]
	fadds	s16, s15, s16
	add	r2, r2, #1
	str	r2, [r3, #40]
	fsts	s16, [r3, #44]
	b	.L80
.L85:
	ldr	r2, [r5, #44]
	ldr	r3, .L119+20
	add	r3, r3, r2, asl #7
	flds	s15, [r3, #96]
	ldrh	r1, [r3, #114]
	fadds	s15, s15, s16
	add	r1, r1, #1
	strh	r1, [r3, #114]	@ movhi
	fsts	s15, [r3, #96]
	ldr	r3, [r5, #40]
	flds	s15, [r3, #36]
	ldr	r2, [r3, #32]
	fadds	s16, s15, s16
	add	r2, r2, #1
	str	r2, [r3, #32]
	fsts	s16, [r3, #36]
	b	.L80
.L86:
	ldr	r2, [r5, #44]
	ldr	r3, .L119+20
	add	r3, r3, r2, asl #7
	flds	s15, [r3, #100]
	ldrh	r2, [r3, #112]
	fadds	s15, s15, s16
	add	r2, r2, #1
	strh	r2, [r3, #112]	@ movhi
	fsts	s15, [r3, #100]
	ldr	r3, [r5, #40]
	flds	s15, [r3, #28]
	ldr	r2, [r3, #24]
	fadds	s16, s15, s16
	add	r2, r2, #1
	str	r2, [r3, #24]
	fsts	s16, [r3, #28]
.L80:
	mov	r1, r5
	ldr	r0, .L119+96
	bl	nm_ListGetNext
	mov	r5, r0
.L74:
	cmp	r5, #0
	bne	.L88
	b	.L89
.L120:
	.align	2
.L119:
	.word	0
	.word	1078853632
	.word	0
	.word	station_report_data_completed
	.word	2373
	.word	station_preserves
	.word	poc_report_data_completed
	.word	2405
	.word	poc_preserves
	.word	lights_report_completed_records_recursive_MUTEX
	.word	2422
	.word	lights_report_data_completed
	.word	2428
	.word	system_report_data_completed
	.word	2451
	.word	14224
	.word	2469
	.word	2472
	.word	2475
	.word	56896
	.word	foal_irri
	.word	14176
	.word	2527
	.word	poc_preserves+388
	.word	foal_irri+36
	.word	system_preserves+324
	.word	system_preserves+57220
	.word	system_preserves
	.word	system_preserves+14548
	.word	poc_preserves+336
	.word	system_preserves+28772
	.word	system_preserves+42996
	.word	system_preserves_recursive_MUTEX
	.word	list_foal_irri_recursive_MUTEX
	.word	station_preserves_recursive_MUTEX
	.word	2708
	.word	poc_preserves_recursive_MUTEX
	.word	2796
	.word	.LC0
	.word	2798
	.word	foal_lights
	.word	lights_preserves
	.word	foal_lights+16
	.word	lights_preserves_recursive_MUTEX
	.word	list_foal_lights_recursive_MUTEX
.L73:
	ldr	r3, .L119+100
	ldr	r2, .L119+104
.L92:
	flds	s15, [r3, #-12]
	flds	s14, [r3, #-232]
	fcmpezs	s15
	flds	s15, [r3, #0]
	fmstat
	fadds	s15, s14, s15
	ldrgt	r1, [r3, #-236]
	addgt	r1, r1, #1
	strgt	r1, [r3, #-236]
	fsts	s15, [r3, #-232]
	add	r3, r3, #14208
	add	r3, r3, #16
	cmp	r3, r2
	bne	.L92
.L89:
	ldr	r3, .L119+108
	mov	r4, #0
	str	r4, [r3, #324]	@ float
	ldr	r3, .L119+112
	fmsr	s16, r4
	ldr	r5, .L119+116
	str	r4, [r3, #0]	@ float
	ldr	r3, .L119+120
	mov	r6, #0
	str	r4, [r3, #0]	@ float
	ldr	r3, .L119+124
	mov	r7, #472
	str	r4, [r3, #0]	@ float
	ldr	r3, .L119+128
	sub	r4, r5, #336
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L119+132
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L119+136
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L119+144
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+140
	bl	xQueueTakeMutexRecursive_debug
.L98:
	ldr	r3, [r5, #-312]
	cmp	r3, #0
	beq	.L93
	flds	s14, [r5, #-176]
	flds	s15, [r5, #-88]
	fadds	s14, s14, s16
	fadds	s14, s14, s15
	flds	s15, [r5, #0]
	fadds	s14, s14, s15
	fcmpezs	s14
	fmstat
	ble	.L94
	fcvtds	d7, s14
	mla	r2, r7, r6, r4
	add	r2, r2, #20
	fldd	d6, [r2, #368]
	ldr	r3, [r2, #48]
	faddd	d6, d6, d7
	add	r3, r3, #1
	str	r3, [r2, #48]
	ldr	r3, [r2, #24]
	ldr	r1, [r3, #92]
	fstd	d6, [r2, #368]
	fldd	d6, [r2, #40]
	cmp	r1, #0
	faddd	d6, d6, d7
	fstd	d6, [r2, #40]
	beq	.L96
	fldd	d6, [r2, #76]
	ldr	r3, [r2, #84]
	fmrrd	r0, r1, d7
	faddd	d6, d6, d7
	add	r3, r3, #1
	str	r3, [r2, #84]
	fstd	d6, [r2, #76]
	bl	add_amount_to_budget_record
	b	.L94
.L96:
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	tst	r3, #24
	beq	.L97
	fldd	d6, [r2, #64]
	ldr	r3, [r2, #72]
	faddd	d6, d6, d7
	add	r3, r3, #1
	str	r3, [r2, #72]
	fstd	d6, [r2, #64]
	fldd	d6, [r2, #384]
	faddd	d7, d6, d7
	fstd	d7, [r2, #384]
.L94:
	fsts	s16, [r5, #-176]
	fsts	s16, [r5, #-88]
	fsts	s16, [r5, #0]
	b	.L93
.L97:
	fldd	d6, [r2, #52]
	ldr	r3, [r2, #60]
	faddd	d6, d6, d7
	add	r3, r3, #1
	str	r3, [r2, #60]
	fstd	d6, [r2, #52]
	fldd	d6, [r2, #392]
	faddd	d7, d6, d7
	fstd	d7, [r2, #392]
	b	.L94
.L93:
	add	r6, r6, #1
	cmp	r6, #12
	add	r5, r5, #472
	bne	.L98
	ldr	r3, .L119+144
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L119+176
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+148
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L119+172
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L119+152
	ldr	r3, .L119+156
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L119+160
	ldr	r3, [r0, #24]
	cmp	r3, #0
	beq	.L99
	add	r0, r0, #16
	bl	nm_ListGetFirst
	ldr	r5, .L119+164
	mov	r6, #20
	mov	r4, r0
	b	.L100
.L103:
	ldrb	r3, [r4, #27]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L101
	ldrb	r0, [r4, #34]	@ zero_extendqisi2
	ldrb	r1, [r4, #35]	@ zero_extendqisi2
	bl	LIGHTS_get_lights_array_index
	ldrb	r3, [r4, #25]	@ zero_extendqisi2
	cmp	r3, #1
	addne	r0, r0, #1
	mlaeq	r0, r6, r0, r5
	mulne	r0, r6, r0
	ldreq	r3, [r0, #16]
	ldrne	r3, [r5, r0]
	addeq	r3, r3, #1
	addne	r3, r3, #1
	streq	r3, [r0, #16]
	strne	r3, [r5, r0]
.L101:
	mov	r1, r4
	ldr	r0, .L119+168
	bl	nm_ListGetNext
	mov	r4, r0
.L100:
	cmp	r4, #0
	bne	.L103
.L99:
	ldr	r3, .L119+172
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L119+176
	ldr	r0, [r3, #0]
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.LFE12:
	.size	REPORT_DATA_maintain_all_accumulators, .-REPORT_DATA_maintain_all_accumulators
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/report_data.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI0-.LFB10
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x608
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI2-.LFB12
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x24
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x9
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/report_data.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xb3
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF12
	.byte	0x1
	.4byte	.LASF13
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x514
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x3b5
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x273
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1a7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0xf1
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.byte	0x35
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x46b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x594
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x8ec
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x636
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x71e
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x817
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x92c
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB10
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI1
	.4byte	.LFE10
	.2byte	0x3
	.byte	0x7d
	.sleb128 1544
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB12
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI3
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"LIGHTS_REPORT_DATA_extract_and_store_changes\000"
.LASF9:
	.ascii	"BUDGET_REPORT_DATA_extract_and_store_changes2\000"
.LASF3:
	.ascii	"STATION_REPORT_DATA_extract_and_store_changes\000"
.LASF1:
	.ascii	"FLOW_RECORDING_extract_and_store_changes\000"
.LASF12:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF10:
	.ascii	"REPORTS_extract_and_store_changes\000"
.LASF13:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/report_data.c\000"
.LASF7:
	.ascii	"MOISTURE_RECORDER_extract_and_store_changes\000"
.LASF4:
	.ascii	"POC_REPORT_DATA_extract_and_store_changes\000"
.LASF14:
	.ascii	"add_amount_to_budget_record\000"
.LASF2:
	.ascii	"STATION_HISTORY_extract_and_store_changes\000"
.LASF0:
	.ascii	"WEATHER_TABLES_extract_and_store_changes\000"
.LASF5:
	.ascii	"SYSTEM_REPORT_DATA_extract_and_store_changes\000"
.LASF11:
	.ascii	"REPORT_DATA_maintain_all_accumulators\000"
.LASF8:
	.ascii	"BUDGET_REPORT_DATA_extract_and_store_changes\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
