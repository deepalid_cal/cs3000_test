	.file	"epson_rx_8025sa.c"
	.text
.Ltext0:
	.section	.text.set_the_time_and_date,"ax",%progbits
	.align	2
	.type	set_the_time_and_date, %function
set_the_time_and_date:
.LFB10:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI0:
	mov	r3, #102
	str	r3, [sp, #0]
	ldrh	r3, [r0, #16]
	mov	r2, #0
	strb	r3, [sp, #8]
	ldrh	r3, [r0, #14]
	mov	r1, sp
	strb	r3, [sp, #9]
	ldrh	r3, [r0, #12]
	strb	r3, [sp, #10]
	ldrb	r3, [r0, #18]	@ zero_extendqisi2
	strb	r3, [sp, #11]
	ldrh	r3, [r0, #6]
	strb	r3, [sp, #12]
	ldrh	r3, [r0, #8]
	strb	r3, [sp, #13]
	ldrb	r3, [r0, #10]	@ zero_extendqisi2
	add	r3, r3, #48
	strb	r3, [sp, #14]
	ldr	r3, .L3
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L2
	ldr	r0, .L3+4
	bl	Alert_Message
.L2:
	ldr	r0, .L3+8
	bl	postBackground_Calculation_Event
	add	sp, sp, #24
	ldmfd	sp!, {pc}
.L4:
	.align	2
.L3:
	.word	.LANCHOR0
	.word	.LC0
	.word	4369
.LFE10:
	.size	set_the_time_and_date, .-set_the_time_and_date
	.section	.text.epson_i2c1_read_isr,"ax",%progbits
	.align	2
	.global	epson_i2c1_read_isr
	.type	epson_i2c1_read_isr, %function
epson_i2c1_read_isr:
.LFB3:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r4, .L24
	sub	sp, sp, #28
.LCFI2:
	ldr	r3, [r4, #4]
	tst	r3, #6
	str	r3, [sp, #4]
	beq	.L6
	add	r1, sp, #28
	mov	r3, #17
	str	r3, [r1, #-28]!
	ldr	r3, .L24+4
	add	r2, sp, #24
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r3, #0
	bl	xQueueGenericSendFromISR
	mov	r3, #256
	str	r3, [r4, #8]
	ldr	r3, .L24+8
	mov	r2, #0
	str	r2, [r3, #48]
	b	.L5
.L6:
	tst	r3, #1
	ldrne	r3, .L24+8
	bne	.L20
	b	.L23
.L11:
	ldr	r2, [r3, #40]
	ldr	r0, [r4, #0]
	add	r1, r3, r2
	and	r0, r0, #255
	add	r2, r2, #1
	strb	r0, [r1, #16]
	str	r2, [r3, #40]
	ldr	r2, [r3, #36]
	sub	r2, r2, #1
	str	r2, [r3, #36]
.L20:
	ldr	r2, [r3, #36]
	cmp	r2, #0
	beq	.L10
	ldr	r2, [r4, #4]
	tst	r2, #512
	beq	.L11
.L10:
	ldr	r3, .L24
	ldr	r1, .L24+8
	mov	r2, #256
	str	r2, [r3, #8]
	mov	r3, #0
	str	r3, [r1, #48]
	ldr	r3, [r1, #52]
	add	r4, sp, #28
	str	r3, [r4, #-28]!
	add	r1, r1, #16
	add	r0, r4, #8
	mov	r2, #16
	bl	memcpy
	ldr	r3, .L24+4
	mov	r1, sp
	ldr	r0, [r3, #0]
	add	r2, sp, #24
	mov	r3, #1
	bl	xQueueGenericSendFromISR
	b	.L5
.L13:
	ldr	r2, [r3, #40]
	ldr	r0, [r4, #0]
	add	r1, r3, r2
	and	r0, r0, #255
	add	r2, r2, #1
	strb	r0, [r1, #16]
	str	r2, [r3, #40]
	ldr	r2, [r3, #36]
	sub	r2, r2, #1
	str	r2, [r3, #36]
	b	.L21
.L23:
	ldr	r3, .L24+8
.L21:
	ldr	r2, [r3, #36]
	cmp	r2, #0
	beq	.L18
	ldr	r2, [r4, #4]
	tst	r2, #512
	beq	.L13
	b	.L18
.L16:
	ldr	ip, [r3, #44]
	sub	ip, ip, #1
	str	ip, [r3, #44]
	ldr	ip, [r3, #44]
	cmp	ip, #0
	streq	r0, [r2, #0]
	strne	r1, [r2, #0]
	b	.L22
.L18:
	ldr	r3, .L24+8
	ldr	r2, .L24
	mov	r0, #512
.L22:
	ldr	r1, [r3, #44]
	cmp	r1, #0
	beq	.L5
	ldr	r1, [r2, #4]
	ands	r1, r1, #1024
	beq	.L16
.L5:
	add	sp, sp, #28
	ldmfd	sp!, {r4, pc}
.L25:
	.align	2
.L24:
	.word	1074397184
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE3:
	.size	epson_i2c1_read_isr, .-epson_i2c1_read_isr
	.section	.text.epson_periodic_isr,"ax",%progbits
	.align	2
	.global	epson_periodic_isr
	.type	epson_periodic_isr, %function
epson_periodic_isr:
.LFB1:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI3:
	mov	r3, #0
	str	r3, [sp, #24]
	add	r1, sp, #28
	mov	r3, #68
	str	r3, [r1, #-28]!
	ldr	r3, .L28
	mov	r1, sp
	ldr	r0, [r3, #0]
	add	r2, sp, #24
	mov	r3, #1
	bl	xQueueGenericSendFromISR
	ldr	r3, [sp, #24]
	cmp	r3, #1
	bne	.L26
.LBB2:
	bl	vTaskSwitchContext
.L26:
.LBE2:
	add	sp, sp, #28
	ldmfd	sp!, {pc}
.L29:
	.align	2
.L28:
	.word	.LANCHOR0
.LFE1:
	.size	epson_periodic_isr, .-epson_periodic_isr
	.section	.text.epson_i2c1_write_isr,"ax",%progbits
	.align	2
	.global	epson_i2c1_write_isr
	.type	epson_i2c1_write_isr, %function
epson_i2c1_write_isr:
.LFB2:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI4:
	ldr	r4, .L42
	sub	sp, sp, #28
.LCFI5:
	ldr	r3, [r4, #4]
	ands	r2, r3, #6
	str	r3, [sp, #4]
	beq	.L31
	add	r1, sp, #28
	mov	r3, #17
	str	r3, [r1, #-28]!
	ldr	r3, .L42+4
	add	r2, sp, #24
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r3, #0
	bl	xQueueGenericSendFromISR
	mov	r3, #256
	str	r3, [r4, #8]
	ldr	r3, .L42+8
	mov	r2, #0
	str	r2, [r3, #48]
	b	.L30
.L31:
	tst	r3, #1
.LBB5:
	ldreq	r3, .L42+8
	moveq	r2, #7
.LBE5:
	beq	.L39
	b	.L41
.L37:
.LBB6:
	ldr	r1, [r3, #32]
	sub	r1, r1, #1
	str	r1, [r3, #32]
	ldr	r1, [r3, #32]
	cmp	r1, #0
	ldrne	r1, [r3, #32]
	streq	r2, [r4, #8]
	ldrneb	r1, [r3, r1]	@ zero_extendqisi2
	ldreqb	r1, [r3, #0]	@ zero_extendqisi2
	andne	r1, r1, #255
	orreq	r1, r1, #512
	str	r1, [r4, #0]
.L39:
	ldr	r1, [r3, #32]
	cmp	r1, #0
	beq	.L30
	ldr	r1, [r4, #4]
	tst	r1, #1024
	beq	.L37
	b	.L30
.L41:
.LBE6:
	mov	r3, #256
	str	r3, [r4, #8]
	ldr	r3, .L42+8
	add	r1, sp, #28
	str	r2, [r3, #48]
	ldr	r3, [r3, #52]
	add	r2, sp, #24
	str	r3, [r1, #-28]!
	ldr	r3, .L42+4
	mov	r1, sp
	ldr	r0, [r3, #0]
	mov	r3, #1
	bl	xQueueGenericSendFromISR
.L30:
	add	sp, sp, #28
	ldmfd	sp!, {r4, pc}
.L43:
	.align	2
.L42:
	.word	1074397184
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE2:
	.size	epson_i2c1_write_isr, .-epson_i2c1_write_isr
	.global	__umodsi3
	.global	__udivsi3
	.section	.text.hextobcd,"ax",%progbits
	.align	2
	.global	hextobcd
	.type	hextobcd, %function
hextobcd:
.LFB0:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI6:
	mov	r5, #0
	add	r6, sp, #4
	mov	r4, r0
	str	r5, [r6, #-4]!
	b	.L45
.L48:
	mov	r7, r5, lsr #1
	tst	r5, #1
	ldrb	r8, [r6, r7]	@ zero_extendqisi2
	mov	r0, r4
	mov	r1, #10
	beq	.L46
	bl	__umodsi3
	mov	r0, r0, asl #4
	b	.L49
.L46:
	bl	__umodsi3
.L49:
	and	r0, r0, #255
	orr	r8, r0, r8
	mov	r1, #10
	mov	r0, r4
	strb	r8, [r6, r7]
	bl	__udivsi3
	add	r5, r5, #1
	and	r5, r5, #255
	mov	r4, r0
.L45:
	cmp	r4, #0
	bne	.L48
	ldr	r0, [sp, #0]
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, pc}
.LFE0:
	.size	hextobcd, .-hextobcd
	.section	.text._epson_read_REG_F,"ax",%progbits
	.align	2
	.global	_epson_read_REG_F
	.type	_epson_read_REG_F, %function
_epson_read_REG_F:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI7:
	mov	r0, #51
	bl	xDisable_ISR
	mov	r4, #0
	mov	r1, #14
	mov	r2, #1
	ldr	r3, .L51
	mov	r0, #51
	str	r4, [sp, #0]
	bl	xSetISR_Vector
	ldr	r2, .L51+4
	mov	r3, #256
	str	r3, [r2, #8]
	ldr	r3, .L51+8
	mov	r1, #1
	str	r1, [r3, #36]
	str	r4, [r3, #40]
	str	r4, [r3, #44]
	str	r1, [r3, #48]
	mov	r1, #34
	str	r1, [r3, #52]
	mov	r3, #111
	str	r3, [r2, #8]
	add	r3, r3, #246
	str	r3, [r2, #0]
	mov	r0, #51
	mov	r3, #512
	str	r3, [r2, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xEnable_ISR
.L52:
	.align	2
.L51:
	.word	epson_i2c1_read_isr
	.word	1074397184
	.word	.LANCHOR1
.LFE4:
	.size	_epson_read_REG_F, .-_epson_read_REG_F
	.section	.text._epson_read_the_time_and_date,"ax",%progbits
	.align	2
	.global	_epson_read_the_time_and_date
	.type	_epson_read_the_time_and_date, %function
_epson_read_the_time_and_date:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI8:
	mov	r0, #51
	bl	xDisable_ISR
	mov	r4, #0
	mov	r1, #14
	mov	r2, #1
	ldr	r3, .L54
	mov	r0, #51
	str	r4, [sp, #0]
	bl	xSetISR_Vector
	ldr	r3, .L54+4
	mov	r2, #256
	str	r2, [r3, #8]
	ldr	r2, .L54+8
	mov	r1, #8
	str	r1, [r2, #36]
	mov	r1, #6
	str	r4, [r2, #40]
	str	r1, [r2, #44]
	mov	r1, #1
	str	r1, [r2, #48]
	mov	r1, #85
	str	r1, [r2, #52]
	mov	r2, #111
	str	r2, [r3, #8]
	mov	r0, #51
	add	r2, r2, #246
	str	r2, [r3, #0]
	str	r4, [r3, #0]
	str	r4, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xEnable_ISR
.L55:
	.align	2
.L54:
	.word	epson_i2c1_read_isr
	.word	1074397184
	.word	.LANCHOR1
.LFE5:
	.size	_epson_read_the_time_and_date, .-_epson_read_the_time_and_date
	.section	.text._epson_set_the_time_and_date,"ax",%progbits
	.align	2
	.global	_epson_set_the_time_and_date
	.type	_epson_set_the_time_and_date, %function
_epson_set_the_time_and_date:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI9:
	ldr	r7, .L57
	mov	r4, r0
	mov	r0, #51
	ldr	r6, .L57+4
	bl	xDisable_ISR
	mov	r5, #0
	mov	r1, #14
	mov	r2, #1
	ldr	r3, .L57+8
	mov	r0, #51
	str	r5, [sp, #0]
	bl	xSetISR_Vector
	mov	r3, #256
	str	r3, [r7, #8]
	mov	r3, #119
	str	r3, [r6, #52]
	strb	r5, [r6, #7]
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	bl	hextobcd
	and	r0, r0, #255
	strb	r0, [r6, #6]
	ldrb	r0, [r4, #9]	@ zero_extendqisi2
	bl	hextobcd
	and	r0, r0, #255
	strb	r0, [r6, #5]
	ldrb	r0, [r4, #10]	@ zero_extendqisi2
	bl	hextobcd
	and	r0, r0, #255
	strb	r0, [r6, #4]
	ldrb	r0, [r4, #11]	@ zero_extendqisi2
	bl	hextobcd
	and	r0, r0, #255
	strb	r0, [r6, #3]
	ldrb	r0, [r4, #12]	@ zero_extendqisi2
	bl	hextobcd
	and	r0, r0, #255
	strb	r0, [r6, #2]
	ldrb	r0, [r4, #13]	@ zero_extendqisi2
	bl	hextobcd
	and	r0, r0, #255
	strb	r0, [r6, #1]
	ldrb	r0, [r4, #14]	@ zero_extendqisi2
	bl	hextobcd
	mov	r3, #8
	and	r0, r0, #255
	strb	r0, [r6, #0]
	str	r3, [r6, #32]
	mov	r3, #1
	str	r3, [r6, #48]
	mov	r3, #143
	str	r3, [r7, #8]
	mov	r0, #51
	mov	r3, #356
	str	r3, [r7, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xEnable_ISR
.L58:
	.align	2
.L57:
	.word	1074397184
	.word	.LANCHOR1
	.word	epson_i2c1_write_isr
.LFE6:
	.size	_epson_set_the_time_and_date, .-_epson_set_the_time_and_date
	.section	.text._epson_rtc_cold_start,"ax",%progbits
	.align	2
	.global	_epson_rtc_cold_start
	.type	_epson_rtc_cold_start, %function
_epson_rtc_cold_start:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI10:
	ldr	r7, .L60
	mov	r0, #51
	ldr	r5, .L60+4
	bl	xDisable_ISR
	mov	r4, #0
	mov	r1, #14
	mov	r2, #1
	ldr	r3, .L60+8
	mov	r0, #51
	str	r4, [sp, #0]
	bl	xSetISR_Vector
	mov	r3, #256
	str	r3, [r7, #8]
	mov	r6, #51
	mvn	r3, #31
	str	r6, [r5, #52]
	mov	r0, r4
	strb	r3, [r5, #9]
	mov	r3, #40
	strb	r6, [r5, #8]
	strb	r3, [r5, #7]
	bl	hextobcd
	and	r4, r0, #255
	mov	r0, #12
	strb	r4, [r5, #6]
	strb	r4, [r5, #5]
	bl	hextobcd
	and	r8, r0, #255
	mov	r0, #1
	strb	r8, [r5, #4]
	strb	r4, [r5, #3]
	bl	hextobcd
	mov	r3, #10
	and	r0, r0, #255
	strb	r0, [r5, #2]
	strb	r0, [r5, #1]
	strb	r8, [r5, #0]
	str	r3, [r5, #32]
	mov	r3, #1
	str	r3, [r5, #48]
	mov	r3, #143
	str	r3, [r7, #8]
	mov	r0, r6
	mov	r3, #356
	str	r3, [r7, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xEnable_ISR
.L61:
	.align	2
.L60:
	.word	1074397184
	.word	.LANCHOR1
	.word	epson_i2c1_write_isr
.LFE7:
	.size	_epson_rtc_cold_start, .-_epson_rtc_cold_start
	.section	.text._init_epson_i2c_channel,"ax",%progbits
	.align	2
	.global	_init_epson_i2c_channel
	.type	_init_epson_i2c_channel, %function
_init_epson_i2c_channel:
.LFB8:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI11:
	mov	r1, #1
	mov	r0, #11
	bl	clkpwr_clk_en_dis
	mov	r3, #50
	mov	r0, #4
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	bl	clkpwr_get_base_clock_rate
	str	r0, [sp, #4]
	ldr	r4, [sp, #12]
	ldr	r0, [sp, #4]
	ldr	r3, [sp, #8]
	ldr	r1, [sp, #12]
	add	r1, r1, r3
	bl	__udivsi3
	ldr	r1, .L63
	mul	r0, r4, r0
	bl	__udivsi3
	ldr	r4, .L63+4
	str	r0, [r4, #16]
	ldr	r5, [sp, #8]
	ldr	r0, [sp, #4]
	ldr	r3, [sp, #8]
	ldr	r1, [sp, #12]
	add	r1, r1, r3
	bl	__udivsi3
	ldr	r1, .L63
	mul	r0, r5, r0
	bl	__udivsi3
	str	r0, [r4, #12]
	mov	r0, #1
	mov	r1, r0
	bl	clkpwr_set_i2c_driver
	mov	r3, #256
	str	r3, [r4, #8]
	ldr	r3, .L63+8
	mov	r4, #0
	str	r4, [r3, #48]
	mov	r0, #51
	bl	xDisable_ISR
	mov	r0, #90
	bl	xDisable_ISR
	mov	r1, #15
	mov	r2, #3
	ldr	r3, .L63+12
	mov	r0, #90
	str	r4, [sp, #0]
	bl	xSetISR_Vector
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, lr}
	b	_epson_read_REG_F
.L64:
	.align	2
.L63:
	.word	200000
	.word	1074397184
	.word	.LANCHOR1
	.word	epson_periodic_isr
.LFE8:
	.size	_init_epson_i2c_channel, .-_init_epson_i2c_channel
	.section	.text.EPSON_rtc_control_task,"ax",%progbits
	.align	2
	.global	EPSON_rtc_control_task
	.type	EPSON_rtc_control_task, %function
EPSON_rtc_control_task:
.LFB9:
	@ args = 0, pretend = 0, frame = 68
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI12:
	ldr	r8, .L93
	sub	sp, sp, #68
.LCFI13:
	mov	r1, #24
	mov	r2, #0
	mov	r0, #10
	bl	xQueueGenericCreate
.LBB7:
	ldr	sl, .L93+4
.LBE7:
	mov	r5, #0
	str	r0, [r8, #0]
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L93+8
	mov	r1, #24
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #30
	bl	xQueueGenericCreate
	ldr	r3, .L93+12
	str	r0, [r3, #0]
	bl	_init_epson_i2c_channel
.L88:
	ldr	r0, [r8, #0]
	cmp	r0, #0
	beq	.L67
	mov	r1, sp
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	mov	r7, r0
	bne	.L68
.LBB8:
	ldr	r3, [sp, #0]
	cmp	r3, #51
	beq	.L72
	bhi	.L76
	cmp	r3, #17
	beq	.L70
	cmp	r3, #34
	bne	.L69
	b	.L91
.L76:
	cmp	r3, #85
	beq	.L74
	cmp	r3, #102
	beq	.L75
	cmp	r3, #68
	bne	.L69
	b	.L92
.L70:
	ldr	r0, .L93+16
	b	.L90
.L91:
	ldrb	r3, [sp, #8]	@ zero_extendqisi2
	and	r3, r3, #112
	cmp	r3, #32
	beq	.L89
	bl	_epson_rtc_cold_start
	b	.L69
.L72:
	ldr	r0, .L93+20
	bl	Alert_Message
.L89:
	mov	r0, #90
	bl	xEnable_ISR
	b	.L69
.L75:
	mov	r0, sp
	bl	_epson_set_the_time_and_date
	b	.L69
.L92:
	bl	_epson_read_the_time_and_date
	b	.L69
.L74:
	ldrb	r3, [sp, #8]	@ zero_extendqisi2
	and	r3, r3, #112
	cmp	r3, #32
	ldrne	r0, .L93+24
	bne	.L90
	ldr	r6, .L93+8
	mov	r1, #0
	mov	r3, r1
	mvn	r2, #0
	ldr	r0, [r6, #0]
	bl	xQueueGenericReceive
	ldrb	r1, [sp, #9]	@ zero_extendqisi2
	ldrb	r0, [sp, #10]	@ zero_extendqisi2
	ldrb	ip, [sp, #11]	@ zero_extendqisi2
	mov	r3, #10
	mov	r2, r1, lsr #4
	and	r1, r1, #15
	mla	r2, r3, r2, r1
	mov	r1, r0, lsr #4
	and	r0, r0, #15
	mla	r1, r3, r1, r0
	mov	r0, ip, lsr #4
	and	ip, ip, #15
	ldr	r4, .L93+28
	mla	r0, r3, r0, ip
	ldrb	ip, [sp, #12]	@ zero_extendqisi2
	strh	r2, [r4, #16]	@ movhi
	strb	ip, [r4, #18]
	ldrb	ip, [sp, #13]	@ zero_extendqisi2
	strh	r1, [r4, #14]	@ movhi
	mov	lr, ip, lsr #4
	and	ip, ip, #15
	mla	ip, r3, lr, ip
	strh	r0, [r4, #12]	@ movhi
	strh	ip, [r4, #6]	@ movhi
	ldrb	ip, [sp, #14]	@ zero_extendqisi2
	add	r5, r5, #1
	mov	lr, ip, lsr #4
	and	ip, ip, #15
	mla	ip, r3, lr, ip
	strh	ip, [r4, #8]	@ movhi
	ldrb	ip, [sp, #15]	@ zero_extendqisi2
	and	lr, ip, #15
	add	lr, lr, #2000
	mov	ip, ip, lsr #4
	mla	r3, ip, r3, lr
	strh	r3, [r4, #10]	@ movhi
	bl	HMSToTime
	ldrh	r1, [r4, #8]
	ldrh	r2, [r4, #10]
	str	r0, [r4, #0]
	ldrh	r0, [r4, #6]
	bl	DMYToDate
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	strh	r0, [r4, #4]	@ movhi
	ldr	r0, [r6, #0]
	bl	xQueueGenericSend
	ldr	r3, .L93+32
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	cmp	r5, #2
	bls	.L69
	mov	r1, #0
	mov	r3, r1
	mvn	r2, #0
	ldr	r0, [r6, #0]
	bl	xQueueGenericReceive
	ldr	r3, .L93+36
	ldrh	r2, [r4, #4]
	ldrh	r1, [r3, #16]
	ldr	r4, .L93+36
	cmp	r1, r2
	strne	r7, [r3, #20]
	strneh	r2, [r3, #16]	@ movhi
	ldr	r3, [r3, #24]
	cmp	r3, #1
	bne	.L80
	ldr	r0, .L93+28
	add	r1, r4, #28
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #1
	moveq	r3, #0
	streq	r3, [r4, #24]
.L80:
	bl	NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone
	cmp	r0, #1
	mov	r7, r0
	bne	.L81
	ldr	r9, .L93+28
	ldr	r3, .L93+40
	ldr	r2, [r9, #0]
	cmp	r2, r3
	bne	.L82
	ldrb	r6, [r9, #18]	@ zero_extendqisi2
	cmp	r6, #0
	bne	.L82
	bl	NETWORK_CONFIG_get_dls_ptr
	mov	r4, r0
	mov	r0, r6
	bl	NETWORK_CONFIG_get_dls_ptr
	ldrh	r3, [r9, #8]
	ldr	r2, [r4, #0]
	cmp	r3, r2
	bne	.L83
	ldrh	r3, [r9, #6]
	ldr	r2, [r4, #4]
	cmp	r3, r2
	bls	.L82
	ldr	r2, [r4, #8]
	cmp	r3, r2
	bcs	.L82
	ldmia	r9!, {r0, r1, r2, r3}
	add	ip, sp, #48
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [r9, #0]
	str	r3, [ip, #0]
	add	r3, r0, #3600
	str	r3, [sp, #48]
	ldrh	r3, [sp, #60]
	add	r0, sp, #48
	add	r3, r3, #1
	strh	r3, [sp, #60]	@ movhi
	bl	set_the_time_and_date
	b	.L82
.L83:
	ldr	r2, [r0, #0]
	cmp	r3, r2
	bne	.L82
	ldrh	r3, [r9, #6]
	ldr	r2, [r0, #4]
	cmp	r3, r2
	bls	.L82
	ldr	r2, [r0, #8]
	cmp	r3, r2
	bcs	.L82
	ldr	r4, .L93+36
	ldr	r3, [r4, #20]
	cmp	r3, #0
	beq	.L82
	ldmia	r9, {r0, r1}
	mov	lr, r9
	str	r0, [r4, #28]
	strh	r1, [r4, #32]	@ movhi
	ldmia	lr!, {r0, r1, r2, r3}
	add	ip, sp, #48
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	str	r3, [ip, #0]
	sub	r3, r0, #3600
	str	r3, [sp, #48]
	ldrh	r3, [sp, #60]
	add	r0, sp, #48
	sub	r3, r3, #1
	strh	r3, [sp, #60]	@ movhi
	bl	set_the_time_and_date
	str	r7, [r4, #24]
	str	r6, [r4, #20]
	b	.L82
.L81:
	ldr	r3, .L93+36
	mov	r2, #0
	str	r2, [r3, #24]
.L82:
	ldr	r3, .L93+36
	ldr	lr, .L93+28
	ldr	r3, [r3, #24]
	add	ip, sp, #28
	strb	r3, [lr, #19]
	ldr	r3, .L93+44
	str	r3, [sp, #24]
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	mov	r1, #0
	str	r3, [ip, #0]
	ldr	r3, .L93+8
	mov	r2, r1
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericSend
	ldr	r3, .L93+12
	mov	r2, #0
	ldr	r0, [r3, #0]
	add	r1, sp, #24
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L69
	ldr	r0, .L93+48
.L90:
	bl	Alert_Message
.L69:
	mov	r4, #10
	b	.L84
.L85:
	bl	vTaskDelay
	subs	r4, r4, #1
	bne	.L84
	ldr	r0, .L93+52
	bl	Alert_Message
	b	.L68
.L84:
	ldr	r0, [sl, #48]
	cmp	r0, #1
	beq	.L85
.L68:
.LBE8:
	bl	xTaskGetTickCount
	ldr	r3, .L93+56
	str	r0, [r3, #0]
	b	.L88
.L67:
	ldr	r0, .L93+60
	bl	Alert_Message
	b	.L88
.L94:
	.align	2
.L93:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LANCHOR4
	.word	startup_rtc_task_sync_semaphore
	.word	weather_preserves
	.word	7200
	.word	4369
	.word	.LC4
	.word	.LC5
	.word	rtc_task_last_x_stamp
	.word	.LC6
.LFE9:
	.size	EPSON_rtc_control_task, .-EPSON_rtc_control_task
	.section	.text.EPSON_obtain_latest_complete_time_and_date,"ax",%progbits
	.align	2
	.global	EPSON_obtain_latest_complete_time_and_date
	.type	EPSON_obtain_latest_complete_time_and_date, %function
EPSON_obtain_latest_complete_time_and_date:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI14:
	ldr	r5, .L97
	mov	r4, r0
	ldr	r6, [r5, #0]
	cmp	r6, #0
	beq	.L96
	mov	r1, #0
	mov	r0, r6
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	ip, .L97+4
	ldmia	ip!, {r0, r1, r2, r3}
	stmia	r4!, {r0, r1, r2, r3}
	ldr	r3, [ip, #0]
	ldr	r0, [r5, #0]
	mov	r1, #0
	str	r3, [r4, #0]
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGenericSend
.L96:
	mov	r0, #1
	mov	r5, #12
	mov	r1, r0
	mov	r2, r5
	strh	r0, [r4, #6]	@ movhi
	strb	r6, [r4, #18]
	strh	r5, [r4, #12]	@ movhi
	strh	r6, [r4, #14]	@ movhi
	strh	r0, [r4, #8]	@ movhi
	strh	r6, [r4, #16]	@ movhi
	strh	r5, [r4, #10]	@ movhi
	bl	DMYToDate
	mov	r1, r6
	mov	r2, r6
	strh	r0, [r4, #4]	@ movhi
	mov	r0, r5
	bl	HMSToTime
	str	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L98:
	.align	2
.L97:
	.word	.LANCHOR2
	.word	.LANCHOR4
.LFE11:
	.size	EPSON_obtain_latest_complete_time_and_date, .-EPSON_obtain_latest_complete_time_and_date
	.section	.text.EPSON_obtain_latest_time_and_date,"ax",%progbits
	.align	2
	.global	EPSON_obtain_latest_time_and_date
	.type	EPSON_obtain_latest_time_and_date, %function
EPSON_obtain_latest_time_and_date:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI15:
	ldr	r5, .L101
	mov	r4, r0
	ldr	r6, [r5, #0]
	cmp	r6, #0
	beq	.L100
	mov	r1, #0
	mov	r3, r1
	mov	r0, r6
	mvn	r2, #0
	bl	xQueueGenericReceive
	mov	r0, r4
	ldr	r1, .L101+4
	mov	r2, #6
	bl	memcpy
	ldr	r0, [r5, #0]
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGenericSend
.L100:
	mov	r0, #1
	mov	r1, r0
	mov	r2, #12
	bl	DMYToDate
	mov	r1, r6
	mov	r2, r6
	strb	r0, [r4, #4]
	mov	r0, r0, lsr #8
	strb	r0, [r4, #5]
	mov	r0, #12
	bl	HMSToTime
	mov	r3, r0, lsr #8
	strb	r0, [r4, #0]
	strb	r3, [r4, #1]
	mov	r3, r0, lsr #16
	mov	r0, r0, lsr #24
	strb	r3, [r4, #2]
	strb	r0, [r4, #3]
	ldmfd	sp!, {r4, r5, r6, pc}
.L102:
	.align	2
.L101:
	.word	.LANCHOR2
	.word	.LANCHOR4
.LFE12:
	.size	EPSON_obtain_latest_time_and_date, .-EPSON_obtain_latest_time_and_date
	.section	.text.EXCEPTION_USE_ONLY_obtain_latest_time_and_date,"ax",%progbits
	.align	2
	.global	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	.type	EXCEPTION_USE_ONLY_obtain_latest_time_and_date, %function
EXCEPTION_USE_ONLY_obtain_latest_time_and_date:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI16:
	ldr	r1, .L104
	mov	r2, #6
	bl	memcpy
	ldr	pc, [sp], #4
.L105:
	.align	2
.L104:
	.word	.LANCHOR4
.LFE13:
	.size	EXCEPTION_USE_ONLY_obtain_latest_time_and_date, .-EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	.section	.text.EPSON_set_date_time,"ax",%progbits
	.align	2
	.global	EPSON_set_date_time
	.type	EPSON_set_date_time, %function
EPSON_set_date_time:
.LFB14:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI17:
	sub	sp, sp, #76
.LCFI18:
	add	r3, sp, #12
	stmia	r3, {r0, r1}
	mov	r5, r2
	add	r0, sp, #40
	bl	EPSON_obtain_latest_time_and_date
	cmp	r5, #1
	bne	.L109
	ldr	r3, [sp, #12]
	ldr	r0, [sp, #40]
	ldrh	r6, [sp, #44]
	rsb	r0, r3, r0
	ldrh	r4, [sp, #16]
	bl	abs
	cmp	r0, #30
	bgt	.L109
	cmp	r6, r4
	moveq	r0, #0
	beq	.L108
.L109:
	add	r4, sp, #12
	ldmia	r4, {r0, r1}
	add	r3, sp, #60
	str	r0, [sp, #20]
	strh	r1, [sp, #24]	@ movhi
	ldrh	r0, [sp, #16]
	str	r3, [sp, #0]
	add	r1, sp, #48
	add	r2, sp, #52
	add	r3, sp, #56
	bl	DateToDMY
	add	r1, sp, #64
	add	r2, sp, #68
	ldr	r0, [sp, #12]
	add	r3, sp, #72
	bl	TimeToHMS
	ldr	r3, [sp, #48]
	add	r0, sp, #20
	strh	r3, [sp, #26]	@ movhi
	ldr	r3, [sp, #52]
	strh	r3, [sp, #28]	@ movhi
	ldr	r3, [sp, #56]
	strh	r3, [sp, #30]	@ movhi
	ldr	r3, [sp, #60]
	strb	r3, [sp, #38]
	ldr	r3, [sp, #64]
	strh	r3, [sp, #32]	@ movhi
	ldr	r3, [sp, #68]
	strh	r3, [sp, #34]	@ movhi
	ldr	r3, [sp, #72]
	strh	r3, [sp, #36]	@ movhi
	bl	set_the_time_and_date
	bl	FLOWSENSE_get_controller_index
	mov	r6, r0
	bl	FLOWSENSE_get_controller_index
	mov	r3, #6
	stmia	sp, {r3, r5}
	mov	r1, r6
	add	r2, sp, #40
	mov	r3, r4
	str	r0, [sp, #8]
	mov	r0, #57344
	bl	Alert_ChangeLine_Controller
	mov	r0, #1
.L108:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, pc}
.LFE14:
	.size	EPSON_set_date_time, .-EPSON_set_date_time
	.global	TD_CHECK_queue
	.global	Epson_RTC_Queue
	.section	.bss.TD_CHECK_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	TD_CHECK_queue, %object
	.size	TD_CHECK_queue, 4
TD_CHECK_queue:
	.space	4
	.section	.bss.Time_and_date_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	Time_and_date_MUTEX, %object
	.size	Time_and_date_MUTEX, 4
Time_and_date_MUTEX:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"EPSON RTC QUEUE OVERFLOW!\000"
.LC1:
	.ascii	"Epson RTC I2C: isr error detected\000"
.LC2:
	.ascii	"Epson RTC: cold start completed\000"
.LC3:
	.ascii	"RTC 1 hz error\000"
.LC4:
	.ascii	"TD_CHECK QUEUE OVERFLOW!\000"
.LC5:
	.ascii	"Unexpected EPSON RTC I2C delay!\000"
.LC6:
	.ascii	"EPSON RTC queue handle not valid!\000"
	.section	.bss.local_dt,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	local_dt, %object
	.size	local_dt, 20
local_dt:
	.space	20
	.section	.bss.Epson_RTC_Queue,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	Epson_RTC_Queue, %object
	.size	Epson_RTC_Queue, 4
Epson_RTC_Queue:
	.space	4
	.section	.bss.i2c1cs,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	i2c1cs, %object
	.size	i2c1cs, 56
i2c1cs:
	.space	56
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI0-.LFB10
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI6-.LFB0
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI7-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI8-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI9-.LFB6
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI10-.LFB7
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI11-.LFB8
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI12-.LFB9
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI14-.LFB11
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI15-.LFB12
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI16-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI17-.LFB14
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/rtc/epson_rx_8025sa.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x15b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF13
	.byte	0x1
	.4byte	.LASF14
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.byte	0xc7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x37f
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x102
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xb1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xa2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x158
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x17a
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1a0
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST7
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1d0
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST8
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x202
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST9
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x22c
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST10
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x3ac
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x3cb
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x3e3
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x3f0
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB10
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI5
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB0
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB5
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB6
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB7
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB8
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB9
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI13
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI18
	.4byte	.LFE14
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF11:
	.ascii	"EXCEPTION_USE_ONLY_obtain_latest_time_and_date\000"
.LASF1:
	.ascii	"epson_periodic_isr\000"
.LASF14:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/rtc/"
	.ascii	"epson_rx_8025sa.c\000"
.LASF10:
	.ascii	"EPSON_obtain_latest_time_and_date\000"
.LASF6:
	.ascii	"_epson_rtc_cold_start\000"
.LASF12:
	.ascii	"EPSON_set_date_time\000"
.LASF8:
	.ascii	"EPSON_rtc_control_task\000"
.LASF0:
	.ascii	"epson_i2c1_read_isr\000"
.LASF9:
	.ascii	"EPSON_obtain_latest_complete_time_and_date\000"
.LASF7:
	.ascii	"_init_epson_i2c_channel\000"
.LASF13:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"_epson_read_REG_F\000"
.LASF2:
	.ascii	"hextobcd\000"
.LASF4:
	.ascii	"_epson_read_the_time_and_date\000"
.LASF16:
	.ascii	"set_the_time_and_date\000"
.LASF5:
	.ascii	"_epson_set_the_time_and_date\000"
.LASF15:
	.ascii	"epson_i2c1_write_isr\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
