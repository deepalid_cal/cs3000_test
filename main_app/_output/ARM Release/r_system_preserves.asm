	.file	"r_system_preserves.c"
	.text
.Ltext0:
	.section	.text.FDTO_SYSTEM_PRESERVES_redraw_report,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_PRESERVES_redraw_report, %function
FDTO_SYSTEM_PRESERVES_redraw_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI0:
	ldr	r9, .L10
	ldr	sl, .L10+4
	mov	r4, #0
	sub	sp, sp, #108
.LCFI1:
	mov	r5, r4
	mov	r6, r4
	add	r8, sp, #44
.L3:
	ldr	r3, [sl, #0]
	ldr	r2, .L10+8
	mov	r1, #64
	mla	r3, r2, r3, r6
	mov	r0, r8
	add	r3, r9, r3, asl #2
	flds	s15, [r3, #232]
	mov	r7, #0
	add	r6, r6, #1
	fcvtds	d6, s15
	fmrrd	r2, r3, d6
	str	r3, [sp, #0]
	mov	r3, r2
	ldr	r2, .L10+12
	bl	snprintf
	mov	r0, #53
	mov	r1, #12
	mul	r0, r5, r0
	mul	r1, r4, r1
	mov	r3, #3
	add	r0, r0, #150
	add	r1, r1, #57
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	mov	r0, r0, asl #16
	mov	r3, #15
	mov	r1, r1, asl #16
	str	r3, [sp, #40]
	add	r5, r5, #1
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, #1
	mvn	r3, #0
	str	r8, [sp, #0]
	str	r7, [sp, #12]
	str	r7, [sp, #16]
	str	r7, [sp, #20]
	str	r7, [sp, #24]
	str	r7, [sp, #28]
	str	r7, [sp, #32]
	str	r7, [sp, #36]
	bl	GuiLib_DrawStr
	cmp	r5, #4
	addeq	r4, r4, #1
	moveq	r5, r7
	cmp	r6, #20
	bne	.L3
	ldr	sl, .L10+4
	mov	r4, #0
	mov	r5, r4
	mov	r6, r4
	add	r8, sp, #44
.L5:
	ldr	r3, [sl, #0]
	ldr	r2, .L10+8
	mov	r1, #64
	mla	r3, r2, r3, r6
	ldr	r2, .L10
	mov	r0, r8
	add	r3, r2, r3, asl #2
	flds	s15, [r3, #328]
	mov	r7, #0
	add	r6, r6, #1
	fcvtds	d6, s15
	fmrrd	r2, r3, d6
	str	r3, [sp, #0]
	mov	r3, r2
	ldr	r2, .L10+12
	bl	snprintf
	mov	r0, #53
	mov	r1, #12
	mul	r0, r5, r0
	mul	r1, r4, r1
	mov	r3, #3
	add	r0, r0, #150
	add	r1, r1, #138
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	mov	r0, r0, asl #16
	mov	r3, #15
	mov	r1, r1, asl #16
	str	r3, [sp, #40]
	add	r5, r5, #1
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, #1
	mvn	r3, #0
	str	r8, [sp, #0]
	str	r7, [sp, #12]
	str	r7, [sp, #16]
	str	r7, [sp, #20]
	str	r7, [sp, #24]
	str	r7, [sp, #28]
	str	r7, [sp, #32]
	str	r7, [sp, #36]
	bl	GuiLib_DrawStr
	cmp	r5, #4
	addeq	r4, r4, #1
	moveq	r5, r7
	cmp	r6, #30
	bne	.L5
	ldr	r4, .L10+4
	ldr	r2, .L10+16
	ldr	r5, [r4, #0]
	ldr	r3, .L10
	mla	r3, r2, r5, r3
	ldr	r2, .L10+20
	ldr	r1, [r3, #312]	@ float
	str	r1, [r2, #0]	@ float
	ldr	r1, [r3, #316]
	ldr	r2, .L10+24
	str	r1, [r2, #0]
	ldr	r1, [r3, #320]	@ float
	ldr	r2, .L10+28
	str	r1, [r2, #0]	@ float
	ldr	r2, [r3, #448]
	ldr	r3, .L10+32
	str	r2, [r3, #0]
	bl	SYSTEM_num_systems_in_use
	cmp	r5, r0
	bcs	.L6
	ldr	r0, [r4, #0]
	bl	FDTO_SYSTEM_load_system_name_into_guivar
	b	.L7
.L6:
	ldr	r3, [r4, #0]
	ldr	r0, .L10+36
	mov	r1, #65
	ldr	r2, .L10+40
	add	r3, r3, #1
	bl	snprintf
.L7:
	bl	GuiLib_Refresh
	add	sp, sp, #108
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L11:
	.align	2
.L10:
	.word	system_preserves
	.word	.LANCHOR0
	.word	3556
	.word	.LC0
	.word	14224
	.word	GuiVar_SystemPreserves5SecMostRecent
	.word	GuiVar_SystemPreserves5SecNext
	.word	GuiVar_SystemPreservesStabilityMostRecent5Sec
	.word	GuiVar_SystemPreservesStabilityNext
	.word	GuiVar_GroupName
	.word	.LC1
.LFE0:
	.size	FDTO_SYSTEM_PRESERVES_redraw_report, .-FDTO_SYSTEM_PRESERVES_redraw_report
	.section	.text.FDTO_SYSTEM_PRESERVES_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_PRESERVES_draw_report
	.type	FDTO_SYSTEM_PRESERVES_draw_report, %function
FDTO_SYSTEM_PRESERVES_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	str	lr, [sp, #-4]!
.LCFI2:
	mov	r2, r0
	bne	.L13
	mov	r0, #102
	mvn	r1, #0
	bl	GuiLib_ShowScreen
	ldr	r3, .L14
	mov	r2, #0
	str	r2, [r3, #0]
.L13:
	ldr	lr, [sp], #4
	b	FDTO_SYSTEM_PRESERVES_redraw_report
.L15:
	.align	2
.L14:
	.word	.LANCHOR0
.LFE1:
	.size	FDTO_SYSTEM_PRESERVES_draw_report, .-FDTO_SYSTEM_PRESERVES_draw_report
	.section	.text.SYSTEM_PRESERVES_process_report,"ax",%progbits
	.align	2
	.global	SYSTEM_PRESERVES_process_report
	.type	SYSTEM_PRESERVES_process_report, %function
SYSTEM_PRESERVES_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, lr}
.LCFI3:
	mov	r3, r0
	sub	sp, sp, #44
.LCFI4:
	mov	r2, r1
	beq	.L19
	bhi	.L20
	cmp	r0, #16
	beq	.L18
	cmp	r0, #20
	bne	.L17
	b	.L23
.L20:
	cmp	r0, #80
	beq	.L18
	cmp	r0, #84
	bne	.L17
.L18:
	cmp	r3, #20
	beq	.L23
	cmp	r3, #16
	movne	r0, r3
	moveq	r0, #80
	b	.L21
.L23:
	mov	r0, #84
.L21:
	mov	r4, #1
	ldr	r1, .L26
	mov	r2, #0
	mov	r3, #3
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	bl	process_uns32
	ldr	r3, .L26+4
	add	r0, sp, #8
	str	r4, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L16
.L19:
	ldr	r3, .L26+8
	mov	r2, #11
	str	r2, [r3, #0]
	b	.L25
.L17:
	mov	r0, r3
	mov	r1, r2
.L25:
	bl	KEY_process_global_keys
.L16:
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L27:
	.align	2
.L26:
	.word	.LANCHOR0
	.word	FDTO_SYSTEM_PRESERVES_redraw_report
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	SYSTEM_PRESERVES_process_report, .-SYSTEM_PRESERVES_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%7.2f\000"
.LC1:
	.ascii	"No System %d in file system\000"
	.section	.bss.g_SYSTEM_PRESERVES_index_of_system_to_show,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_SYSTEM_PRESERVES_index_of_system_to_show, %object
	.size	g_SYSTEM_PRESERVES_index_of_system_to_show, 4
g_SYSTEM_PRESERVES_index_of_system_to_show:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x8c
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_system_preserves.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x29
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x78
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x91
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 140
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"SYSTEM_PRESERVES_process_report\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_system_preserves.c\000"
.LASF0:
	.ascii	"FDTO_SYSTEM_PRESERVES_draw_report\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"FDTO_SYSTEM_PRESERVES_redraw_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
