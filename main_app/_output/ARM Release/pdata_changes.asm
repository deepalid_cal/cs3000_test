	.file	"pdata_changes.c"
	.text
.Ltext0:
	.global	__ashldi3
	.section	.text.PDATA_build_setting_to_send,"ax",%progbits
	.align	2
	.type	PDATA_build_setting_to_send, %function
PDATA_build_setting_to_send:
.LFB10:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI0:
	mov	ip, r2
	ldr	r2, [sp, #24]
	mov	r4, r1
	ldr	r1, [r2, #0]
	mov	r6, r0
	cmp	r1, #1
	movne	r5, #0
	bne	.L2
.LBB10:
	ldr	r1, [sp, #32]
	mov	r0, r3
	str	r1, [sp, #0]
	ldr	r3, [sp, #28]
	ldr	r1, [sp, #20]
	blx	ip
	subs	r5, r0, #0
	beq	.L2
.LBB11:
	mov	r2, r6
	mov	r0, #1
	mov	r1, #0
	bl	__ashldi3
	ldmia	r4, {r2-r3}
	orr	r2, r2, r0
	orr	r3, r3, r1
	stmia	r4, {r2-r3}
.LBE11:
	mov	r0, r6
	bl	CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response
.L2:
.LBE10:
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.LFE10:
	.size	PDATA_build_setting_to_send, .-PDATA_build_setting_to_send
	.global	__lshrdi3
	.section	.text.PDATA_extract_settings,"ax",%progbits
	.align	2
	.type	PDATA_extract_settings, %function
PDATA_extract_settings:
.LFB0:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI1:
	ldr	r9, [sp, #36]
	mov	r6, r0
	mov	sl, r1
	mov	r8, r2
	ldmia	r2, {r0-r1}
	ldr	r5, [sp, #44]
	mov	r2, r3
	mov	r4, r3
	bl	__lshrdi3
	ands	r7, r0, #1
	beq	.L5
.LBB14:
	mov	r1, #400
	ldr	r2, .L10
	mov	r3, #104
	mov	r0, r9
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, sl
	ldr	r2, [sp, #40]
	mov	r3, r5
	ldr	ip, [sp, #32]
	ldr	r0, [r6, #0]
	blx	ip
	mov	r7, r0
	mov	r0, r9
	bl	xQueueGiveMutexRecursive
	cmp	r7, #0
	beq	.L6
	cmp	r5, #1
	cmpne	r5, #15
	beq	.L7
	cmp	r5, #16
	bne	.L8
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L8
.L7:
	mov	r0, r4
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L8:
	mov	r0, r4
	bl	CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response
.L6:
	ldr	r3, [r6, #0]
	add	r3, r3, r7
	str	r3, [r6, #0]
	mov	r3, #1
	mvn	r4, r3, asl r4
	ldmia	r8, {r2-r3}
	mov	r3, #0
	and	r2, r2, r4
	stmia	r8, {r2-r3}
.L5:
.LBE14:
	mov	r0, r7
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L11:
	.align	2
.L10:
	.word	.LC0
.LFE0:
	.size	PDATA_extract_settings, .-PDATA_extract_settings
	.section	.text.PDATA_extract_and_store_changes,"ax",%progbits
	.align	2
	.global	PDATA_extract_and_store_changes
	.type	PDATA_extract_and_store_changes, %function
PDATA_extract_and_store_changes:
.LFB1:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	ldr	r8, .L14
	sub	sp, sp, #32
.LCFI3:
	mov	ip, r0
	mov	r6, r1
	mov	r7, r2
	mov	r1, ip
	str	r0, [sp, #16]
	mov	r2, #1
	add	r0, sp, #31
	mov	r4, r3
	bl	memcpy
	ldr	r1, [sp, #16]
	mov	r2, #8
	add	r1, r1, #1
	add	r0, sp, #20
	str	r1, [sp, #16]
	bl	memcpy
	ldr	r3, [sp, #16]
	add	r5, sp, #32
	add	r3, r3, #8
	str	r3, [r5, #-16]!
	ldr	r3, .L14+4
	mov	r1, r6
	str	r3, [sp, #0]
	ldr	r3, [r8, #0]
	add	r2, sp, #20
	stmib	sp, {r3, r7}
	mov	r0, r5
	mov	r3, #1
	str	r4, [sp, #12]
	bl	PDATA_extract_settings
	ldr	r3, .L14+8
	mov	r1, r6
	str	r3, [sp, #0]
	ldr	r3, .L14+12
	add	r2, sp, #20
	ldr	r3, [r3, #0]
	str	r4, [sp, #12]
	stmib	sp, {r3, r7}
	mov	r3, #6
	mov	sl, r0
	mov	r0, r5
	bl	PDATA_extract_settings
	ldr	r3, .L14+16
	mov	r1, r6
	str	r3, [sp, #0]
	ldr	r3, .L14+20
	add	r2, sp, #20
	ldr	r3, [r3, #0]
	str	r4, [sp, #12]
	stmib	sp, {r3, r7}
	mov	r3, #10
	add	sl, sl, r0
	mov	r0, r5
	bl	PDATA_extract_settings
	ldr	r3, .L14+24
	add	sl, sl, #9
	str	r3, [sp, #0]
	ldr	r3, [r8, #0]
	mov	r1, r6
	stmib	sp, {r3, r7}
	add	r2, sp, #20
	mov	r3, #13
	str	r4, [sp, #12]
	add	sl, sl, r0
	mov	r0, r5
	bl	PDATA_extract_settings
	ldr	r3, .L14+28
	mov	r1, r6
	str	r3, [sp, #0]
	ldr	r3, [r8, #0]
	add	r2, sp, #20
	stmib	sp, {r3, r7}
	mov	r3, #12
	str	r4, [sp, #12]
	add	sl, sl, r0
	mov	r0, r5
	bl	PDATA_extract_settings
	ldr	r3, .L14+32
	mov	r1, r6
	str	r3, [sp, #0]
	ldr	r3, [r8, #0]
	add	r2, sp, #20
	stmib	sp, {r3, r7}
	mov	r3, #9
	str	r4, [sp, #12]
	add	sl, sl, r0
	mov	r0, r5
	bl	PDATA_extract_settings
	ldr	r3, .L14+36
	mov	r1, r6
	str	r3, [sp, #0]
	ldr	r3, .L14+40
	add	r2, sp, #20
	ldr	r3, [r3, #0]
	str	r4, [sp, #12]
	stmib	sp, {r3, r7}
	mov	r3, #11
	add	r8, sl, r0
	mov	r0, r5
	bl	PDATA_extract_settings
	ldr	r3, .L14+44
	mov	r1, r6
	str	r3, [sp, #0]
	ldr	r3, .L14+48
	add	r2, sp, #20
	ldr	r3, [r3, #0]
	str	r4, [sp, #12]
	stmib	sp, {r3, r7}
	mov	r3, #16
	add	r8, r8, r0
	mov	r0, r5
	bl	PDATA_extract_settings
	ldr	r3, .L14+52
	mov	r1, r6
	str	r3, [sp, #0]
	ldr	r3, .L14+56
	add	r2, sp, #20
	ldr	r3, [r3, #0]
	str	r4, [sp, #12]
	stmib	sp, {r3, r7}
	mov	r3, #18
	add	r8, r8, r0
	mov	r0, r5
	bl	PDATA_extract_settings
	ldr	r3, .L14+60
	mov	r1, r6
	str	r3, [sp, #0]
	ldr	r3, .L14+64
	add	r2, sp, #20
	ldr	r3, [r3, #0]
	str	r4, [sp, #12]
	stmib	sp, {r3, r7}
	mov	r3, #20
	add	r8, r8, r0
	mov	r0, r5
	bl	PDATA_extract_settings
	add	r8, r8, r0
	ldr	r0, .L14+68
	bl	postBackground_Calculation_Event
	bl	FTIMES_TASK_restart_calculation
	cmp	r4, #1
	bne	.L13
	ldr	r3, .L14+72
	ldrsh	r3, [r3, #0]
	cmp	r3, #36
	beq	.L13
	bl	DIALOG_close_all_dialogs
	mov	r0, r4
	bl	Redraw_Screen
.L13:
	mov	r0, r8
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L15:
	.align	2
.L14:
	.word	list_program_data_recursive_MUTEX
	.word	NETWORK_CONFIG_extract_and_store_changes_from_comm
	.word	nm_WEATHER_extract_and_store_changes_from_comm
	.word	weather_control_recursive_MUTEX
	.word	nm_SYSTEM_extract_and_store_changes_from_comm
	.word	list_system_recursive_MUTEX
	.word	nm_STATION_GROUP_extract_and_store_changes_from_comm
	.word	nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm
	.word	nm_STATION_extract_and_store_changes_from_comm
	.word	nm_POC_extract_and_store_changes_from_comm
	.word	list_poc_recursive_MUTEX
	.word	nm_LIGHTS_extract_and_store_changes_from_comm
	.word	list_lights_recursive_MUTEX
	.word	nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm
	.word	moisture_sensor_items_recursive_MUTEX
	.word	nm_WALK_THRU_extract_and_store_changes_from_comm
	.word	walk_thru_recursive_MUTEX
	.word	4369
	.word	GuiLib_CurStructureNdx
.LFE1:
	.size	PDATA_extract_and_store_changes, .-PDATA_extract_and_store_changes
	.section	.text.PDATA_allocate_space_for_num_changed_groups_in_pucp,"ax",%progbits
	.align	2
	.global	PDATA_allocate_space_for_num_changed_groups_in_pucp
	.type	PDATA_allocate_space_for_num_changed_groups_in_pucp, %function
PDATA_allocate_space_for_num_changed_groups_in_pucp:
.LFB3:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	ip, [r3, #0]
	cmp	ip, #1
	bne	.L17
	ldr	ip, [sp, #0]
	add	r2, r2, #4
	cmp	r2, ip
	bcs	.L17
	ldr	r3, [r0, #0]
	str	r3, [r1, #0]
	ldr	r3, [r0, #0]
	add	r3, r3, #4
	str	r3, [r0, #0]
	mov	r0, #4
	bx	lr
.L17:
	mov	r0, #0
	str	r0, [r1, #0]
	str	r0, [r3, #0]
	bx	lr
.LFE3:
	.size	PDATA_allocate_space_for_num_changed_groups_in_pucp, .-PDATA_allocate_space_for_num_changed_groups_in_pucp
	.section	.text.PDATA_copy_var_into_pucp,"ax",%progbits
	.align	2
	.global	PDATA_copy_var_into_pucp
	.type	PDATA_copy_var_into_pucp, %function
PDATA_copy_var_into_pucp:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	mov	r4, r0
	ldr	r0, [r0, #0]
	mov	r5, r2
	bl	memcpy
	ldr	r3, [r4, #0]
	mov	r0, r5
	add	r3, r3, r5
	str	r3, [r4, #0]
	ldmfd	sp!, {r4, r5, pc}
.LFE4:
	.size	PDATA_copy_var_into_pucp, .-PDATA_copy_var_into_pucp
	.section	.text.PDATA_copy_bitfield_info_into_pucp,"ax",%progbits
	.align	2
	.global	PDATA_copy_bitfield_info_into_pucp
	.type	PDATA_copy_bitfield_info_into_pucp, %function
PDATA_copy_bitfield_info_into_pucp:
.LFB5:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI5:
	add	ip, sp, #4
	str	r1, [ip, #-4]!
	mov	r1, sp
	mov	r4, r0
	mov	r5, r3
	bl	PDATA_copy_var_into_pucp
	ldr	r3, [r4, #0]
	str	r3, [r5, #0]
	ldr	r3, [sp, #0]
	ldr	r2, [r4, #0]
	add	r2, r2, r3
	str	r2, [r4, #0]
	add	r0, r0, r3
	ldmfd	sp!, {r3, r4, r5, pc}
.LFE5:
	.size	PDATA_copy_bitfield_info_into_pucp, .-PDATA_copy_bitfield_info_into_pucp
	.section	.text.PDATA_allocate_memory_for_and_initialize_memory_for_sending_data,"ax",%progbits
	.align	2
	.global	PDATA_allocate_memory_for_and_initialize_memory_for_sending_data
	.type	PDATA_allocate_memory_for_and_initialize_memory_for_sending_data, %function
PDATA_allocate_memory_for_and_initialize_memory_for_sending_data:
.LFB2:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI6:
	ldr	r8, [sp, #24]
	mov	r4, r0
	mov	r7, r3
	mov	r3, #0
	str	r3, [r0, #0]
	mov	r5, r1
	mov	r6, r2
	mov	r1, r4
	ldr	r2, .L25
	mov	r3, #396
	ldr	r0, [sp, #28]
	bl	mem_oabia
	mov	r2, #0
	mov	r3, #0
	mov	r1, #8
	cmp	r0, #0
	ldrne	r0, [r4, #0]
	str	r0, [r4, #0]
	stmia	r5, {r2-r3}
	strb	r1, [r6, #0]
	ldr	r0, [r4, #0]
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, pc}
	mov	r0, r4
	mov	r2, r7
	mov	r3, r8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	PDATA_copy_bitfield_info_into_pucp
.L26:
	.align	2
.L25:
	.word	.LC0
.LFE2:
	.size	PDATA_allocate_memory_for_and_initialize_memory_for_sending_data, .-PDATA_allocate_memory_for_and_initialize_memory_for_sending_data
	.section	.text.PDATA_copy_var_into_pucp_and_set_32_bit_change_bits,"ax",%progbits
	.align	2
	.global	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	.type	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits, %function
PDATA_copy_var_into_pucp_and_set_32_bit_change_bits:
.LFB6:
	@ args = 28, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI7:
	ldr	r7, [sp, #20]
	mov	r6, r3
	mov	r5, #1
	mov	r5, r5, asl r2
	ldr	r2, [r6, #0]
	mov	r4, r1
	ands	r2, r5, r2
	ldr	r3, [sp, #28]
	beq	.L30
	ldr	r1, [sp, #32]
	ldr	r2, [sp, #40]
	add	r1, r1, r3
	cmp	r1, r2
	ldrcs	r3, [sp, #36]
	movcs	r0, #0
	strcs	r0, [r3, #0]
	ldmcsfd	sp!, {r4, r5, r6, r7, pc}
	mov	r2, r3
	ldr	r1, [sp, #24]
	bl	PDATA_copy_var_into_pucp
	ldr	r3, [r4, #0]
	orr	r3, r3, r5
	str	r3, [r4, #0]
	ldr	r3, [r6, #0]
	bic	r3, r3, r5
	str	r3, [r6, #0]
	ldr	r3, [sp, #44]
	cmp	r3, #19
	ldreq	r3, [r7, #0]
	orreq	r5, r3, r5
	streq	r5, [r7, #0]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L30:
	mov	r0, r2
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE6:
	.size	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits, .-PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	.section	.text.PDATA_copy_var_into_pucp_and_set_64_bit_change_bits,"ax",%progbits
	.align	2
	.global	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	.type	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits, %function
PDATA_copy_var_into_pucp_and_set_64_bit_change_bits:
.LFB7:
	@ args = 28, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI8:
	ldr	sl, [sp, #32]
	mov	r8, r0
	mov	r5, r1
	ldmia	r3, {r0-r1}
	ldr	r9, [sp, #40]
	mov	r7, r2
	mov	r4, r3
	bl	__lshrdi3
	ands	r6, r0, #1
	beq	.L32
	ldr	r2, [sp, #44]
	ldr	r3, [sp, #52]
	add	r2, r2, r9
	cmp	r2, r3
	ldrcs	r3, [sp, #48]
	movcs	r6, #0
	strcs	r6, [r3, #0]
	bcs	.L32
	mov	r2, r9
	mov	r0, r8
	ldr	r1, [sp, #36]
	bl	PDATA_copy_var_into_pucp
	mov	r2, r7
	mov	r1, #0
	mov	r6, r0
	mov	r0, #1
	bl	__ashldi3
	ldmia	r5, {r2-r3}
	orr	r2, r2, r0
	orr	r3, r3, r1
	stmia	r5, {r2-r3}
	ldmia	r4, {r2-r3}
	mov	r8, r0
	bic	r3, r3, r1
	bic	r2, r2, r0
	stmia	r4, {r2-r3}
	ldr	r3, [sp, #56]
	mov	r9, r1
	cmp	r3, #19
	ldmeqia	sl, {r2-r3}
	orreq	r8, r0, r2
	orreq	r9, r1, r3
	stmeqia	sl, {r8-r9}
.L32:
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.LFE7:
	.size	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits, .-PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	.section	.text.PDATA_build_data_to_send,"ax",%progbits
	.align	2
	.global	PDATA_build_data_to_send
	.type	PDATA_build_data_to_send, %function
PDATA_build_data_to_send:
.LFB11:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI9:
	str	r3, [r0, #4]
	str	r3, [r0, #0]
	sub	r3, r1, #19
	cmp	r3, #1
	sub	sp, sp, #40
.LCFI10:
	mov	r4, r0
	mov	r5, r1
	ldrls	r8, .L45
	bls	.L36
	cmp	r2, #0
	moveq	r8, #2048
	movne	r8, #24576
.L36:
	add	r3, sp, #28
	stmia	sp, {r3, r8}
	add	r0, sp, #24
	mov	r3, #1
	add	r1, sp, #16
	add	r2, sp, #39
	bl	PDATA_allocate_memory_for_and_initialize_memory_for_sending_data
	ldr	r3, [sp, #24]
	cmp	r3, #0
	mov	sl, r0
	bne	.L37
	ldr	r0, .L45+4
	bl	Alert_Message
	mov	r4, #256
	b	.L38
.L37:
	ldr	r3, .L45+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L45+12
	ldr	r3, .L45+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [sp, #24]
	add	r6, sp, #40
	rsb	r3, sl, r3
	stmia	r4, {r3, sl}
	ldr	r2, .L45+20
	mov	r0, #1
	add	r1, sp, #16
	add	r3, sp, #24
	str	r0, [r6, #-8]!
	str	sl, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
	add	r1, sp, #16
	ldr	r2, .L45+24
	add	r3, sp, #24
	add	r7, r0, sl
	str	r7, [r4, #4]
	mov	r0, #6
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
	add	r1, sp, #16
	ldr	r2, .L45+28
	add	r3, sp, #24
	add	r7, r7, r0
	str	r7, [r4, #4]
	mov	r0, #10
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
	add	r1, sp, #16
	ldr	r2, .L45+32
	add	r3, sp, #24
	add	r7, r7, r0
	str	r7, [r4, #4]
	mov	r0, #13
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
	add	r1, sp, #16
	ldr	r2, .L45+36
	add	r3, sp, #24
	add	r7, r7, r0
	str	r7, [r4, #4]
	mov	r0, #12
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
	add	r1, sp, #16
	ldr	r2, .L45+40
	add	r3, sp, #24
	add	r7, r7, r0
	str	r7, [r4, #4]
	mov	r0, #9
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
	add	r1, sp, #16
	ldr	r2, .L45+44
	add	r3, sp, #24
	add	r7, r7, r0
	str	r7, [r4, #4]
	mov	r0, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
	ldr	r2, .L45+48
	add	r3, sp, #24
	add	r7, r7, r0
	mov	r0, #16
	str	r7, [r4, #4]
	add	r1, sp, r0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
	add	r1, sp, #16
	ldr	r2, .L45+52
	add	r3, sp, #24
	add	r7, r7, r0
	str	r7, [r4, #4]
	mov	r0, #18
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
	add	r3, sp, #24
	add	r1, sp, #16
	ldr	r2, .L45+56
	add	r7, r7, r0
	str	r7, [r4, #4]
	mov	r0, #20
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r8, [sp, #12]
	bl	PDATA_build_setting_to_send
.LBB15:
	add	r1, sp, #16
	mov	r2, #8
.LBE15:
	add	r0, r7, r0
	str	r0, [r4, #4]
.LBB16:
	ldr	r0, [sp, #28]
	bl	memcpy
.LBE16:
	ldr	r3, [r4, #4]
	cmp	r3, sl
	bne	.L39
	cmp	r5, #19
	bne	.L40
	ldr	r5, .L45+60
	ldr	r3, [r5, #112]
	cmp	r3, #0
	beq	.L40
	ldr	r0, .L45+64
	bl	Alert_Message
	mov	r3, #0
	str	r3, [r5, #112]
.L40:
	ldr	r0, [r4, #0]
	ldr	r1, .L45+12
	ldr	r2, .L45+68
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [r4, #4]
	str	r3, [r4, #0]
	mov	r4, #512
	b	.L41
.L39:
	cmp	r5, #19
	ldreq	r3, .L45+60
	moveq	r2, #0
	mov	r4, #768
	streq	r2, [r3, #112]
.L41:
	ldr	r3, .L45+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L38:
	mov	r0, r4
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L46:
	.align	2
.L45:
	.word	1433600
	.word	.LC1
	.word	pending_changes_recursive_MUTEX
	.word	.LC0
	.word	770
	.word	NETWORK_CONFIG_build_data_to_send
	.word	WEATHER_build_data_to_send
	.word	SYSTEM_build_data_to_send
	.word	STATION_GROUP_build_data_to_send
	.word	MANUAL_PROGRAMS_build_data_to_send
	.word	STATION_build_data_to_send
	.word	POC_build_data_to_send
	.word	LIGHTS_build_data_to_send
	.word	MOISTURE_SENSOR_build_data_to_send
	.word	WALK_THRU_build_data_to_send
	.word	weather_preserves
	.word	.LC2
	.word	834
.LFE11:
	.size	PDATA_build_data_to_send, .-PDATA_build_data_to_send
	.section	.text.PDATA_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	PDATA_update_pending_change_bits
	.type	PDATA_update_pending_change_bits, %function
PDATA_update_pending_change_bits:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI11:
	ldr	r5, .L48
	mov	r4, r0
	mov	r1, #400
	ldr	r2, .L48+4
	ldr	r3, .L48+8
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	nm_NETWORK_CONFIG_update_pending_change_bits
	mov	r0, r4
	bl	nm_WEATHER_update_pending_change_bits
	mov	r0, r4
	bl	nm_SYSTEM_update_pending_change_bits
	mov	r0, r4
	bl	nm_STATION_GROUP_update_pending_change_bits
	mov	r0, r4
	bl	nm_MANUAL_PROGRAMS_update_pending_change_bits
	mov	r0, r4
	bl	nm_STATION_update_pending_change_bits
	mov	r0, r4
	bl	nm_POC_update_pending_change_bits
	mov	r0, r4
	bl	nm_LIGHTS_update_pending_change_bits
	mov	r0, r4
	bl	nm_MOISTURE_SENSOR_update_pending_change_bits
	mov	r0, r4
	bl	nm_WALK_THRU_update_pending_change_bits
	ldr	r0, [r5, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L49:
	.align	2
.L48:
	.word	pending_changes_recursive_MUTEX
	.word	.LC0
	.word	879
.LFE12:
	.size	PDATA_update_pending_change_bits, .-PDATA_update_pending_change_bits
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/pdata_changes.c\000"
.LC1:
	.ascii	"No memory to send program data\000"
.LC2:
	.ascii	"Pending changes flag unexpdly set\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI0-.LFB10
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI7-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI8-.LFB7
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI9-.LFB11
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI11-.LFB12
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE20:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/pdata_changes.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x11e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF13
	.byte	0x1
	.4byte	.LASF14
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x265
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x27d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x26c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0x46
	.byte	0x1
	.uleb128 0x4
	.4byte	0x2a
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x3c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xbb
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1af
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1ca
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1da
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x174
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST5
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1ef
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x229
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x2b6
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST8
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x36d
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST9
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB10
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB11
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI10
	.4byte	.LFE11
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB12
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"PDATA_copy_file_bitfield_into_pucp\000"
.LASF12:
	.ascii	"PDATA_update_pending_change_bits\000"
.LASF14:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/pdata_changes.c\000"
.LASF1:
	.ascii	"PDATA_build_setting_to_send\000"
.LASF13:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF10:
	.ascii	"PDATA_copy_var_into_pucp_and_set_64_bit_change_bits"
	.ascii	"\000"
.LASF8:
	.ascii	"PDATA_allocate_memory_for_and_initialize_memory_for"
	.ascii	"_sending_data\000"
.LASF4:
	.ascii	"PDATA_extract_and_store_changes\000"
.LASF6:
	.ascii	"PDATA_copy_var_into_pucp\000"
.LASF0:
	.ascii	"PDATA_set_bit_that_file_has_changed\000"
.LASF5:
	.ascii	"PDATA_allocate_space_for_num_changed_groups_in_pucp"
	.ascii	"\000"
.LASF11:
	.ascii	"PDATA_build_data_to_send\000"
.LASF7:
	.ascii	"PDATA_copy_bitfield_info_into_pucp\000"
.LASF9:
	.ascii	"PDATA_copy_var_into_pucp_and_set_32_bit_change_bits"
	.ascii	"\000"
.LASF3:
	.ascii	"PDATA_extract_settings\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
