	.file	"speaker.c"
	.text
.Ltext0:
	.section	.text.speaker_isr,"ax",%progbits
	.align	2
	.type	speaker_isr, %function
speaker_isr:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r2, #255
	sub	r3, r3, #196608
	str	r2, [r3, #0]
	bx	lr
.L3:
	.align	2
.L2:
	.word	1074118656
.LFE0:
	.size	speaker_isr, .-speaker_isr
	.section	.text.nm_start_speaker,"ax",%progbits
	.align	2
	.type	nm_start_speaker, %function
nm_start_speaker:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L5
	mov	r3, r0, asl #24
	mov	r3, r3, lsr #16
	orr	r3, r3, #127
	str	r3, [r2, #0]
	ldr	r3, .L5+4
	str	r1, [r3, #24]
	mov	r1, #255
	str	r1, [r3, #0]
	mov	r1, #0
	str	r1, [r3, #16]
	str	r1, [r3, #8]
	mov	r1, #1
	str	r1, [r3, #4]
	ldr	r3, [r2, #0]
	orr	r3, r3, #-2147483648
	str	r3, [r2, #0]
	bx	lr
.L6:
	.align	2
.L5:
	.word	1074118656
	.word	1073922048
.LFE1:
	.size	nm_start_speaker, .-nm_start_speaker
	.section	.text.init_speaker,"ax",%progbits
	.align	2
	.global	init_speaker
	.type	init_speaker, %function
init_speaker:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI0:
	ldr	r5, .L8
	mov	r1, #0
	mov	r3, r1
	mvn	r2, #0
	ldr	r0, [r5, #0]
	bl	xQueueGenericReceive
	mov	r0, #15
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	mov	r0, #1
	mov	r2, r0
	mov	r1, r0
	bl	clkpwr_setup_pwm
	ldr	r3, .L8+4
	mov	r4, #0
	str	r4, [r3, #0]
	mov	r1, #1
	mov	r0, #23
	bl	clkpwr_clk_en_dis
	ldr	r3, .L8+8
	mov	r2, #5
	str	r4, [r3, #4]
	str	r4, [r3, #112]
	str	r4, [r3, #40]
	str	r4, [r3, #60]
	str	r4, [r3, #8]
	str	r4, [r3, #16]
	str	r4, [r3, #24]
	str	r4, [r3, #28]
	str	r4, [r3, #32]
	str	r4, [r3, #36]
	str	r4, [r3, #12]
	str	r2, [r3, #20]
	mov	r2, #255
	str	r2, [r3, #0]
	mov	r0, #3
	bl	xDisable_ISR
	mov	r1, #1
	mov	r2, r1
	ldr	r3, .L8+12
	mov	r0, #3
	str	r4, [sp, #0]
	bl	xSetISR_Vector
	mov	r0, #3
	bl	xEnable_ISR
	ldr	r3, .L8+16
	ldr	r0, [r5, #0]
	mov	r2, #1
	str	r2, [r3, #0]
	mov	r1, r4
	mov	r2, r4
	mov	r3, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGenericSend
.L9:
	.align	2
.L8:
	.word	speaker_hardware_MUTEX
	.word	1074118656
	.word	1073922048
	.word	speaker_isr
	.word	.LANCHOR0
.LFE2:
	.size	init_speaker, .-init_speaker
	.section	.text.speaker_enable,"ax",%progbits
	.align	2
	.global	speaker_enable
	.type	speaker_enable, %function
speaker_enable:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r4, .L11
	mov	r1, #0
	mov	r3, r1
	ldr	r0, [r4, #0]
	mvn	r2, #0
	bl	xQueueGenericReceive
	ldr	r3, .L11+4
	ldr	r0, [r4, #0]
	mov	r1, #0
	mov	r2, #1
	str	r2, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, lr}
	b	xQueueGenericSend
.L12:
	.align	2
.L11:
	.word	speaker_hardware_MUTEX
	.word	.LANCHOR0
.LFE3:
	.size	speaker_enable, .-speaker_enable
	.section	.text.speaker_disable,"ax",%progbits
	.align	2
	.global	speaker_disable
	.type	speaker_disable, %function
speaker_disable:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r4, .L14
	mov	r1, #0
	mov	r3, r1
	ldr	r0, [r4, #0]
	mvn	r2, #0
	bl	xQueueGenericReceive
	ldr	r3, .L14+4
	ldr	r0, [r4, #0]
	mov	r1, #0
	str	r1, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, lr}
	b	xQueueGenericSend
.L15:
	.align	2
.L14:
	.word	speaker_hardware_MUTEX
	.word	.LANCHOR0
.LFE4:
	.size	speaker_disable, .-speaker_disable
	.section	.text.good_key_beep,"ax",%progbits
	.align	2
	.global	good_key_beep
	.type	good_key_beep, %function
good_key_beep:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI3:
	ldr	r4, .L18
	mov	r1, #0
	mov	r3, r1
	ldr	r0, [r4, #0]
	mvn	r2, #0
	bl	xQueueGenericReceive
	ldr	r3, .L18+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L17
.LBB2:
	mov	r0, #1
	mov	r1, r0
	mov	r2, r0
	bl	clkpwr_setup_pwm
	mov	r0, #40
	mov	r1, #122880
	bl	nm_start_speaker
.L17:
.LBE2:
	ldr	r0, [r4, #0]
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, lr}
	b	xQueueGenericSend
.L19:
	.align	2
.L18:
	.word	speaker_hardware_MUTEX
	.word	.LANCHOR0
.LFE5:
	.size	good_key_beep, .-good_key_beep
	.section	.text.bad_key_beep,"ax",%progbits
	.align	2
	.global	bad_key_beep
	.type	bad_key_beep, %function
bad_key_beep:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI4:
	ldr	r4, .L22
	mov	r1, #0
	mov	r3, r1
	ldr	r0, [r4, #0]
	mvn	r2, #0
	bl	xQueueGenericReceive
	ldr	r3, .L22+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L21
.LBB3:
	mov	r0, #1
	mov	r1, r0
	mov	r2, #2
	bl	clkpwr_setup_pwm
	mov	r0, #115
	ldr	r1, .L22+8
	bl	nm_start_speaker
.L21:
.LBE3:
	ldr	r0, [r4, #0]
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, lr}
	b	xQueueGenericSend
.L23:
	.align	2
.L22:
	.word	speaker_hardware_MUTEX
	.word	.LANCHOR0
	.word	910000
.LFE6:
	.size	bad_key_beep, .-bad_key_beep
	.section	.bss.speaker_is_enabled,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	speaker_is_enabled, %object
	.size	speaker_is_enabled, 4
speaker_is_enabled:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI3-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI4-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/speaker/speaker.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa7
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x26
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x35
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x55
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x9a
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xa4
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xbe
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x10d
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/spea"
	.ascii	"ker/speaker.c\000"
.LASF2:
	.ascii	"init_speaker\000"
.LASF6:
	.ascii	"bad_key_beep\000"
.LASF3:
	.ascii	"speaker_enable\000"
.LASF4:
	.ascii	"speaker_disable\000"
.LASF1:
	.ascii	"nm_start_speaker\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"good_key_beep\000"
.LASF0:
	.ascii	"speaker_isr\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
