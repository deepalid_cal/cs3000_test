	.file	"cal_list.c"
	.text
.Ltext0:
	.section	.text.nm_ListInit,"ax",%progbits
	.align	2
	.global	nm_ListInit
	.type	nm_ListInit, %function
nm_ListInit:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	str	r3, [r0, #4]
	str	r3, [r0, #0]
	str	r3, [r0, #8]
	str	r1, [r0, #12]
	str	r3, [r0, #16]
	bx	lr
.LFE0:
	.size	nm_ListInit, .-nm_ListInit
	.section	.text.nm_ListInsert,"ax",%progbits
	.align	2
	.global	nm_ListInsert
	.type	nm_ListInsert, %function
nm_ListInsert:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	mov	r4, r2
	mov	r6, r1
	mov	r5, r0
	moveq	r4, #6
	beq	.L3
.LBB4:
	ldr	r3, [r0, #16]
	cmp	r3, #1
	bne	.L4
	ldr	r0, .L13
	bl	Alert_Message
	bl	freeze_and_restart_app
.L4:
	bl	vTaskSuspendAll
	ldr	r1, [r5, #8]
	mov	r2, #1
	cmp	r1, #0
	str	r2, [r5, #16]
	ldr	r3, [r5, #12]
	bne	.L5
	str	r2, [r5, #8]
	add	r2, r6, r3
	str	r6, [r5, #4]
	str	r6, [r5, #0]
	mov	r4, r1
	str	r1, [r6, r3]
	stmib	r2, {r1, r5}
	b	.L6
.L5:
	cmp	r4, #0
	ldreq	r2, [r5, #4]
	movne	r2, r4
	add	r2, r2, r3
	ldr	r0, [r2, #8]
	cmp	r0, r5
	movne	r4, #18
	bne	.L6
	add	r0, r6, r3
	add	r1, r1, #1
	cmp	r4, #0
	str	r5, [r0, #8]
	str	r1, [r5, #8]
	bne	.L8
	str	r6, [r2, #4]
	ldr	r2, [r5, #4]
	str	r4, [r0, #4]
	str	r2, [r6, r3]
	str	r6, [r5, #4]
	b	.L6
.L8:
	ldr	r1, [r5, #0]
	cmp	r4, r1
	bne	.L9
	str	r4, [r0, #4]
	mov	r4, #0
	str	r6, [r2, #0]
	str	r4, [r6, r3]
	str	r6, [r5, #0]
	b	.L6
.L9:
	ldr	r1, [r2, #0]
	str	r6, [r2, #0]
	add	r2, r1, r3
	str	r6, [r2, #4]
	str	r4, [r0, #4]
	mov	r4, #0
	str	r1, [r6, r3]
.L6:
	mov	r3, #0
	str	r3, [r5, #16]
	bl	xTaskResumeAll
.L3:
.LBE4:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L14:
	.align	2
.L13:
	.word	.LC0
.LFE1:
	.size	nm_ListInsert, .-nm_ListInsert
	.section	.text.nm_ListInsertHead,"ax",%progbits
	.align	2
	.global	nm_ListInsertHead
	.type	nm_ListInsertHead, %function
nm_ListInsertHead:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, [r0, #0]
	b	nm_ListInsert
.LFE2:
	.size	nm_ListInsertHead, .-nm_ListInsertHead
	.section	.text.nm_ListInsertTail,"ax",%progbits
	.align	2
	.global	nm_ListInsertTail
	.type	nm_ListInsertTail, %function
nm_ListInsertTail:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r2, #0
	b	nm_ListInsert
.LFE3:
	.size	nm_ListInsertTail, .-nm_ListInsertTail
	.section	.text.nm_ListRemove_debug,"ax",%progbits
	.align	2
	.global	nm_ListRemove_debug
	.type	nm_ListRemove_debug, %function
nm_ListRemove_debug:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	mov	r6, r3
	mov	r5, r1
	mov	r4, r0
	bne	.L18
	mov	r0, r2
	bl	RemovePathFromFileName
	mov	r2, r6
	mov	r5, #6
	mov	r1, r0
	ldr	r0, .L27
	bl	Alert_Message_va
	b	.L19
.L18:
.LBB7:
	ldr	r3, [r0, #16]
	cmp	r3, #0
	beq	.L20
	mov	r0, r2
	bl	RemovePathFromFileName
	mov	r2, r6
	mov	r1, r0
	ldr	r0, .L27+4
	bl	Alert_Message_va
	bl	freeze_and_restart_app
.L20:
	bl	vTaskSuspendAll
	mov	r3, #1
	str	r3, [r4, #16]
	ldr	r3, [r4, #12]
	add	r2, r5, r3
	ldr	r1, [r2, #8]
	cmp	r1, r4
	movne	r5, #18
	bne	.L21
	mov	r1, #0
	str	r1, [r2, #8]
	ldr	r1, [r2, #4]
	ldr	r0, [r5, r3]
	cmp	r1, #0
	strne	r0, [r1, r3]
	streq	r0, [r4, #4]
	ldr	r0, [r5, r3]
	cmp	r0, #0
	addne	r0, r0, r3
	strne	r1, [r0, #4]
	streq	r1, [r4, #0]
	mov	r1, #0
	str	r1, [r5, r3]
	ldr	r3, [r4, #8]
	mov	r5, r1
	sub	r3, r3, #1
	str	r1, [r2, #4]
	str	r3, [r4, #8]
.L21:
	mov	r3, #0
	str	r3, [r4, #16]
	bl	xTaskResumeAll
.L19:
.LBE7:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L28:
	.align	2
.L27:
	.word	.LC1
	.word	.LC2
.LFE4:
	.size	nm_ListRemove_debug, .-nm_ListRemove_debug
	.section	.text.nm_ListRemoveHead_debug,"ax",%progbits
	.align	2
	.global	nm_ListRemoveHead_debug
	.type	nm_ListRemoveHead_debug, %function
nm_ListRemoveHead_debug:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	subs	r4, r0, #0
	mov	r6, r1
	mov	r5, r2
	bne	.L30
	mov	r0, r1
	bl	RemovePathFromFileName
	mov	r2, r5
	mov	r1, r0
	ldr	r0, .L34
	bl	Alert_Message_va
	b	.L31
.L30:
	ldr	r4, [r4, #0]
	cmp	r4, #0
	beq	.L31
.LBB8:
	mov	r1, r4
	mov	r2, r6
	mov	r3, r5
	bl	nm_ListRemove_debug
	cmp	r0, #18
	bne	.L32
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r1, r0
	ldr	r0, .L34+4
	b	.L33
.L32:
	cmp	r0, #0
	beq	.L31
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r1, r0
	ldr	r0, .L34+8
.L33:
	mov	r2, r5
	bl	Alert_Message_va
	mov	r4, #0
.L31:
.LBE8:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L35:
	.align	2
.L34:
	.word	.LC3
	.word	.LC4
	.word	.LC5
.LFE5:
	.size	nm_ListRemoveHead_debug, .-nm_ListRemoveHead_debug
	.section	.text.nm_ListRemoveTail_debug,"ax",%progbits
	.align	2
	.global	nm_ListRemoveTail_debug
	.type	nm_ListRemoveTail_debug, %function
nm_ListRemoveTail_debug:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI3:
	subs	r4, r0, #0
	mov	r6, r1
	mov	r5, r2
	bne	.L37
	mov	r0, r1
	bl	RemovePathFromFileName
	mov	r2, r5
	mov	r1, r0
	ldr	r0, .L41
	bl	Alert_Message_va
	b	.L38
.L37:
	ldr	r4, [r4, #4]
	cmp	r4, #0
	beq	.L38
.LBB9:
	mov	r1, r4
	mov	r2, r6
	mov	r3, r5
	bl	nm_ListRemove_debug
	cmp	r0, #18
	bne	.L39
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r1, r0
	ldr	r0, .L41+4
	b	.L40
.L39:
	cmp	r0, #0
	beq	.L38
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r1, r0
	ldr	r0, .L41+8
.L40:
	mov	r2, r5
	bl	Alert_Message_va
	mov	r4, #0
.L38:
.LBE9:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L42:
	.align	2
.L41:
	.word	.LC6
	.word	.LC7
	.word	.LC8
.LFE6:
	.size	nm_ListRemoveTail_debug, .-nm_ListRemoveTail_debug
	.section	.text.nm_ListGetFirst,"ax",%progbits
	.align	2
	.global	nm_ListGetFirst
	.type	nm_ListGetFirst, %function
nm_ListGetFirst:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #0]
	bx	lr
.LFE7:
	.size	nm_ListGetFirst, .-nm_ListGetFirst
	.section	.text.nm_ListGetLast,"ax",%progbits
	.align	2
	.global	nm_ListGetLast
	.type	nm_ListGetLast, %function
nm_ListGetLast:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #4]
	bx	lr
.LFE8:
	.size	nm_ListGetLast, .-nm_ListGetLast
	.section	.text.nm_ListGetNext,"ax",%progbits
	.align	2
	.global	nm_ListGetNext
	.type	nm_ListGetNext, %function
nm_ListGetNext:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	cmpne	r1, #0
	movne	r3, #0
	moveq	r3, #1
	moveq	r0, #0
	bxeq	lr
	ldr	r2, [r0, #12]
	add	r1, r1, r2
	ldr	r2, [r1, #8]
	cmp	r2, r0
	ldreq	r0, [r1, #4]
	movne	r0, r3
	bx	lr
.LFE9:
	.size	nm_ListGetNext, .-nm_ListGetNext
	.section	.text.nm_ListGetPrev,"ax",%progbits
	.align	2
	.global	nm_ListGetPrev
	.type	nm_ListGetPrev, %function
nm_ListGetPrev:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	cmpne	r1, #0
	movne	r2, #0
	moveq	r2, #1
	moveq	r0, #0
	bxeq	lr
	ldr	r3, [r0, #12]
	add	ip, r1, r3
	ldr	ip, [ip, #8]
	cmp	ip, r0
	ldreq	r0, [r1, r3]
	movne	r0, r2
	bx	lr
.LFE10:
	.size	nm_ListGetPrev, .-nm_ListGetPrev
	.section	.text.nm_OnList,"ax",%progbits
	.align	2
	.global	nm_OnList
	.type	nm_OnList, %function
nm_OnList:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	cmpne	r1, #0
	beq	.L59
	ldr	r3, [r0, #12]
	add	r1, r1, r3
	ldr	r3, [r1, #8]
	rsb	r3, r0, r3
	rsbs	r0, r3, #0
	adc	r0, r0, r3
	bx	lr
.L59:
	mov	r0, #0
	bx	lr
.LFE11:
	.size	nm_OnList, .-nm_OnList
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"KRNLLIST.C List Insert re-entered!\000"
.LC1:
	.ascii	"List Remove NULL : %s, %d\000"
.LC2:
	.ascii	"List Remove RE-ENTERED : %s, %d\000"
.LC3:
	.ascii	"ListRemoveHead : NULL list ptr : %s, %d\000"
.LC4:
	.ascii	"ListRemoveHead - NotONList : %s, %d\000"
.LC5:
	.ascii	"ListRemoveHead : FAILED : %s, %d\000"
.LC6:
	.ascii	"ListRemoveTail : NULL list ptr : %s, %d\000"
.LC7:
	.ascii	"ListRemoveTail - NotONList : %s, %d\000"
.LC8:
	.ascii	"ListRemoveTail : FAILED : %s, %d\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI3-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x11b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF12
	.byte	0x1
	.4byte	.LASF13
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x4c
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x126
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x2e
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xf5
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x10b
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	0x2a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1b7
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x210
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x26b
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x288
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x2a5
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x2d0
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x2fc
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB5
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB6
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF11:
	.ascii	"nm_OnList\000"
.LASF3:
	.ascii	"nm_ListInsertHead\000"
.LASF13:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/cal_list.c\000"
.LASF6:
	.ascii	"nm_ListRemoveTail_debug\000"
.LASF2:
	.ascii	"nm_ListInit\000"
.LASF5:
	.ascii	"nm_ListRemoveHead_debug\000"
.LASF12:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"nm_ListRemove_debug\000"
.LASF10:
	.ascii	"nm_ListGetPrev\000"
.LASF7:
	.ascii	"nm_ListGetFirst\000"
.LASF0:
	.ascii	"nm_ListInsert\000"
.LASF4:
	.ascii	"nm_ListInsertTail\000"
.LASF8:
	.ascii	"nm_ListGetLast\000"
.LASF9:
	.ascii	"nm_ListGetNext\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
