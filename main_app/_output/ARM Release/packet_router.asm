	.file	"packet_router.c"
	.text
.Ltext0:
	.section	.text.this_2000_packet_is_from_a_controller_on_the_hub_list,"ax",%progbits
	.align	2
	.type	this_2000_packet_is_from_a_controller_on_the_hub_list, %function
this_2000_packet_is_from_a_controller_on_the_hub_list:
.LFB4:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L9
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, sl, lr}
.LCFI0:
	ldr	r8, .L9+4
	mov	r1, #0
	mvn	r2, #0
	mov	r6, r0
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, [r8, #0]
	bl	uxQueueMessagesWaiting
	mov	r4, #0
	mov	r7, r4
	mov	r5, r4
	mov	sl, r0
	b	.L2
.L6:
	mov	r2, #0
	ldr	r0, [r8, #0]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L3
	cmp	r5, #0
	bne	.L4
	ldr	r3, [sp, #0]
	cmn	r3, #1
	moveq	r7, #1
	beq	.L4
	cmp	r7, #0
	beq	.L4
	ldrb	r2, [sp, #0]	@ zero_extendqisi2
	ldrb	r3, [r6, #2]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L4
	ldrb	r2, [sp, #1]	@ zero_extendqisi2
	ldrb	r3, [r6, #3]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L4
	ldrb	r5, [sp, #2]	@ zero_extendqisi2
	ldrb	r3, [r6, #4]	@ zero_extendqisi2
	rsb	r3, r3, r5
	rsbs	r5, r3, #0
	adc	r5, r5, r3
.L4:
	mov	r2, #0
	ldr	r0, [r8, #0]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericSend
	b	.L5
.L3:
	ldr	r0, .L9+8
	bl	Alert_Message
.L5:
	add	r4, r4, #1
.L2:
	cmp	r4, sl
	bne	.L6
	ldr	r3, .L9
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, pc}
.L10:
	.align	2
.L9:
	.word	router_hub_list_MUTEX
	.word	router_hub_list_queue
	.word	.LC0
.LFE4:
	.size	this_2000_packet_is_from_a_controller_on_the_hub_list, .-this_2000_packet_is_from_a_controller_on_the_hub_list
	.section	.text.if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task,"ax",%progbits
	.align	2
	.type	if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task, %function
if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task:
.LFB8:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	mov	r5, r2
	mov	r7, r3
	ldrb	r2, [r0, #3]	@ zero_extendqisi2
	ldrb	r3, [r0, #2]	@ zero_extendqisi2
	sub	sp, sp, #40
.LCFI2:
	orr	r2, r3, r2, asl #8
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	mov	r4, r0
	orr	r2, r2, r3, asl #16
	ldrb	r3, [r0, #5]	@ zero_extendqisi2
	mov	r6, r1
	orr	r2, r2, r3, asl #24
	ldr	r3, .L13
	ldr	r3, [r3, #48]
	cmp	r2, r3
	bne	.L11
	mov	r0, r1
	ldr	r2, .L13+4
	add	r1, sp, #32
	ldr	r3, .L13+8
	bl	mem_oabia
	cmp	r0, #0
	beq	.L11
	mov	r2, r6
	ldr	r0, [sp, #32]
	mov	r1, r4
	bl	memcpy
	str	r6, [sp, #36]
	add	r3, sp, #32
	ldmia	r3, {r2-r3}
	mov	r0, sp
	str	r7, [sp, #0]
	str	r2, [sp, #12]
	str	r3, [sp, #16]
	str	r5, [sp, #20]
	bl	RADIO_TEST_post_event_with_details
.L11:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L14:
	.align	2
.L13:
	.word	config_c
	.word	.LC1
	.word	835
.LFE8:
	.size	if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task, .-if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task
	.section	.text.handle_slave_packet_routing.isra.0,"ax",%progbits
	.align	2
	.type	handle_slave_packet_routing.isra.0, %function
handle_slave_packet_routing.isra.0:
.LFB11:
	@ args = 4, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI3:
	ldr	r4, [sp, #36]
	mov	r5, r1
	mov	r7, r0
	mov	r1, r2
	mov	r6, r2
	mov	r0, sp
	mov	r2, #12
	mov	r8, r3
	bl	memcpy
	cmp	r5, #12
	bne	.L16
	mov	r0, r7
	mov	r1, r6
	mov	r2, r8
	mov	r3, r5
	bl	CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet
	b	.L25
.L16:
	cmp	r5, #3
	bne	.L18
	ldrb	r1, [sp, #0]	@ zero_extendqisi2
	add	r0, sp, #5
	ands	r1, r1, #128
	bne	.L30
	bl	this_special_scan_addr_is_for_us
	b	.L28
.L18:
	cmp	r5, #5
	bne	.L22
	ldrb	r3, [sp, #0]	@ zero_extendqisi2
	tst	r3, #128
	addne	r0, sp, #5
	bne	.L30
	mov	r0, r7
	mov	r1, r6
	mov	r2, r8
	bl	TPL_IN_make_a_copy_and_direct_incoming_packet
	b	.L25
.L30:
	mov	r1, #0
	bl	this_communication_address_is_our_serial_number
.L28:
	cmp	r0, #1
	bne	.L25
	mov	r0, r7
	mov	r1, r6
	mov	r2, r8
	bl	TPL_IN_make_a_copy_and_direct_incoming_packet
	b	.L15
.L22:
	cmp	r5, #4
	cmpne	r5, #6
	bne	.L15
.L25:
	mov	r3, #1
	str	r3, [r4, #0]
.L15:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, pc}
.LFE11:
	.size	handle_slave_packet_routing.isra.0, .-handle_slave_packet_routing.isra.0
	.section	.text.Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy,"ax",%progbits
	.align	2
	.global	Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy
	.type	Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy, %function
Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI4:
	ldr	r6, .L32
	add	r8, r2, #8
	mov	r5, r2
	mov	fp, r0
	mov	r9, r1
	mov	r0, r8
	mov	r1, r6
	mov	r2, #117
	mov	sl, r3
	bl	mem_malloc_debug
	ldr	r3, .L32+4
	mov	r1, r9
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	strb	r2, [r0, #0]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	add	r7, r0, #4
	strb	r2, [r0, #1]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r4, r0
	strb	r2, [r0, #2]
	strb	r3, [r0, #3]
	mov	r2, r5
	mov	r0, r7
	bl	memcpy
	ldr	r3, .L32+8
	add	r2, r7, r5
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	mov	r0, fp
	strb	r1, [r7, r5]
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	strb	r1, [r2, #1]
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	strb	r1, [r2, #2]
	strb	r3, [r2, #3]
	mov	r2, #0
	mov	r3, #1
	stmia	sp, {r2, r3}
	mov	r1, r4
	mov	r2, r8
	mov	r3, sl
	bl	AddCopyOfBlockToXmitList
	mov	r0, r4
	mov	r1, r6
	mov	r2, #150
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	mem_free_debug
.L33:
	.align	2
.L32:
	.word	.LC1
	.word	preamble
	.word	postamble
.LFE0:
	.size	Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy, .-Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy
	.section	.text.attempt_to_copy_packet_and_route_out_another_port,"ax",%progbits
	.align	2
	.global	attempt_to_copy_packet_and_route_out_another_port
	.type	attempt_to_copy_packet_and_route_out_another_port, %function
attempt_to_copy_packet_and_route_out_another_port:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	bne	.L35
	ldr	r0, .L47
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message
.L35:
	cmp	r0, #1
	ldreq	ip, .L47+4
	ldreq	ip, [ip, #80]
	beq	.L46
	cmp	r0, #2
	bne	.L38
	ldr	ip, .L47+4
	ldr	ip, [ip, #84]
.L46:
	adds	ip, ip, #0
	movne	ip, #1
	b	.L37
.L38:
	cmp	r0, #5
	bne	.L39
	ldr	r4, .L47+8
	ldr	ip, [r4, #4]
	cmp	ip, #0
	movne	ip, #0
	bne	.L37
	ldr	r5, [r4, #92]
	cmp	r5, #0
	ldreq	ip, [r4, #0]
	beq	.L46
.L37:
	cmp	ip, #0
	ldmeqfd	sp!, {r4, r5, pc}
	cmp	r0, #5
	moveq	r0, #0
.L39:
	ldmfd	sp!, {r4, r5, lr}
	b	Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy
.L48:
	.align	2
.L47:
	.word	.LC2
	.word	config_c
	.word	tpmicro_comm
.LFE1:
	.size	attempt_to_copy_packet_and_route_out_another_port, .-attempt_to_copy_packet_and_route_out_another_port
	.section	.text.Route_Incoming_Packet,"ax",%progbits
	.align	2
	.global	Route_Incoming_Packet
	.type	Route_Incoming_Packet, %function
Route_Incoming_Packet:
.LFB9:
	@ args = 0, pretend = 0, frame = 68
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI6:
	mov	r3, #0
	sub	sp, sp, #72
.LCFI7:
	cmp	r0, #3
	mov	r7, r0
	mov	r4, r1
	mov	r5, r2
.LBB10:
	mov	r6, r2
.LBE10:
	str	r3, [sp, #60]
	bls	.L50
	ldr	r0, .L229
	bl	RemovePathFromFileName
	ldr	r2, .L229+4
	mov	r1, r0
	ldr	r0, .L229+8
	bl	Alert_Message_va
	b	.L61
.L172:
	ldr	r5, .L229+12
	add	r3, r7, #14
	ldrb	r3, [r5, r3, asl #2]	@ zero_extendqisi2
	tst	r3, #32
	beq	.L52
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	beq	.L53
	cmp	r7, #2
	bne	.L53
	ldr	r3, [r5, #84]
	cmp	r3, #1
	beq	.L52
.L53:
	mov	r0, r7
	bl	Alert_comm_crc_failed
.L52:
	mov	r0, #13568
	bl	RADIO_TEST_post_event
	b	.L61
.L173:
	ldr	r3, [sp, #48]
	cmp	r3, #3
	cmpne	r3, #5
	beq	.L56
	cmp	r3, #4
	beq	.L56
	cmp	r3, #6
	bne	.L57
.L56:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	bne	.L57
	ldr	r3, .L229+12
	ldrb	r3, [r3, #52]	@ zero_extendqisi2
	tst	r3, #1
	bne	.L58
	bl	Alert_token_rcvd_with_no_FL
	b	.L61
.L58:
	bl	Alert_token_rcvd_with_FL_turned_off
	b	.L61
.L174:
	ldr	r3, [sp, #48]
	cmp	r3, #3
	cmpne	r3, #5
	bne	.L60
	bl	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	cmp	r0, #0
	beq	.L60
	bl	Alert_token_rcvd_by_FL_master
	b	.L61
.L228:
	ldr	r2, [sp, #44]
	ldr	r3, .L229+16
	cmp	r2, r3
	bne	.L62
	ldr	r3, [sp, #48]
	cmp	r3, #11
	beq	.L210
.L62:
	mov	r0, #4
	mov	r1, #3
	b	.L208
.L185:
	bl	CONFIG_this_controller_is_a_configured_hub
	subs	sl, r0, #0
	beq	.L63
	cmp	r7, #1
	bne	.L64
	ldr	r2, [sp, #44]
	cmp	r2, #2000
	bne	.L65
	ldr	r3, [sp, #48]
	cmp	r3, #0
	bne	.L66
	mov	r0, #125
	bl	CONTROLLER_INITIATED_post_event
	ldr	r3, .L229+20
	ldr	r7, [r3, #0]
	cmp	r7, #0
	bne	.L61
.LBB11:
	ldr	r3, .L229+24
	ldr	sl, .L229+28
	ldr	r0, [r3, #0]
	mov	r1, r7
	mvn	r2, #0
	mov	r3, r7
	bl	xQueueGenericReceive
	ldr	r0, [sl, #0]
	bl	uxQueueMessagesWaiting
	mov	r6, r7
	mov	r8, r7
	mov	r9, r0
	b	.L67
.L71:
	mov	r2, #0
	ldr	r0, [sl, #0]
	add	r1, sp, #64
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L68
	cmp	r8, #0
	bne	.L69
	ldr	r3, [sp, #64]
	cmn	r3, #1
	beq	.L179
	cmp	r6, #0
	beq	.L69
	ldrb	r2, [sp, #64]	@ zero_extendqisi2
	ldrb	r3, [sp, #37]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L179
	ldrb	r2, [sp, #65]	@ zero_extendqisi2
	ldrb	r3, [sp, #38]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L179
	ldrb	r2, [sp, #66]	@ zero_extendqisi2
	ldrb	r3, [sp, #39]	@ zero_extendqisi2
	mov	r6, #1
	cmp	r2, r3
	moveq	r8, r6
	b	.L69
.L179:
	mov	r6, #1
.L69:
	mov	r2, #0
	ldr	r0, [sl, #0]
	add	r1, sp, #64
	mov	r3, r2
	bl	xQueueGenericSend
	b	.L70
.L68:
	ldr	r0, .L229+32
	bl	Alert_Message
.L70:
	add	r7, r7, #1
.L67:
	cmp	r7, r9
	bne	.L71
	ldr	r3, .L229+24
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
.LBE11:
	cmp	r8, #0
	bne	.L81
	ldr	r0, .L229+36
	ldrb	r1, [sp, #37]	@ zero_extendqisi2
	ldrb	r2, [sp, #38]	@ zero_extendqisi2
	ldrb	r3, [sp, #39]	@ zero_extendqisi2
	bl	Alert_Message_va
	b	.L81
.L66:
	mov	r0, #0
	mov	r1, r7
.L203:
	bl	Alert_router_rcvd_unexp_class
	b	.L61
.L65:
	ldr	r3, .L229+16
	cmp	r2, r3
	bne	.L87
	ldr	r3, [sp, #48]
	cmp	r3, #2
	bne	.L74
	mov	r0, #125
	bl	CONTROLLER_INITIATED_post_event
	ldrh	r3, [sp, #22]
	ldrh	r2, [sp, #24]
	orr	r2, r3, r2, asl #16
	ldr	r3, .L229+12
	ldr	r3, [r3, #48]
	cmp	r2, r3
	moveq	r0, r7
	moveq	r1, r4
	moveq	r2, r5
	ldreq	r3, [sp, #48]
	beq	.L207
.L75:
	ldr	r3, .L229+20
	ldr	r6, [r3, #0]
	cmp	r6, #0
	bne	.L61
.LBB12:
	ldr	r3, .L229+24
	ldr	sl, .L229+28
	ldr	r0, [r3, #0]
	mov	r1, r6
	mvn	r2, #0
	mov	r3, r6
	bl	xQueueGenericReceive
	ldr	r0, [sl, #0]
	bl	uxQueueMessagesWaiting
	mov	r8, r6
	mov	r7, r6
	mov	r9, r0
	b	.L76
.L80:
	mov	r2, #0
	ldr	r0, [sl, #0]
	add	r1, sp, #68
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L77
	cmp	r7, #0
	bne	.L78
	ldr	r3, [sp, #68]
	cmn	r3, #1
	moveq	r8, #1
	beq	.L78
	cmp	r8, #0
	bne	.L78
	ldrh	r2, [sp, #22]
	ldrh	r7, [sp, #24]
	orr	r7, r2, r7, asl #16
	rsb	r0, r7, r3
	rsbs	r7, r0, #0
	adc	r7, r7, r0
.L78:
	mov	r2, #0
	ldr	r0, [sl, #0]
	add	r1, sp, #68
	mov	r3, r2
	bl	xQueueGenericSend
	b	.L79
.L77:
	ldr	r0, .L229+32
	bl	Alert_Message
.L79:
	add	r6, r6, #1
.L76:
	cmp	r6, r9
	bne	.L80
	ldr	r3, .L229+24
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
.LBE12:
	cmp	r7, #0
	bne	.L81
	ldrh	r3, [sp, #22]
	ldrh	r1, [sp, #24]
	ldr	r0, .L229+40
	orr	r1, r3, r1, asl #16
	bl	Alert_Message_va
.L81:
	mov	r0, #2
	b	.L218
.L74:
	cmp	r3, #11
	bne	.L66
	ldrh	r3, [sp, #22]
	ldrh	r1, [sp, #24]
	orr	r1, r3, r1, asl #16
	ldr	r3, .L229+12
	ldr	r3, [r3, #48]
	cmp	r1, r3
	bne	.L83
	mov	r0, #125
	bl	CONTROLLER_INITIATED_post_event
	ldr	r3, [sp, #48]
	mov	r0, r7
	mov	r1, r4
	mov	r2, r5
.L190:
	bl	CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet
	b	.L61
.L83:
	mov	r0, #0
	mov	r1, r7
	bl	Alert_router_rcvd_packet_not_for_us
	b	.L61
.L64:
	cmp	r7, #2
	bne	.L84
	ldr	r2, [sp, #44]
	cmp	r2, #2000
	bne	.L85
	ldr	r6, [sp, #48]
	cmp	r6, #1
	bne	.L86
	add	r0, sp, #32
	bl	this_2000_packet_is_from_a_controller_on_the_hub_list
	cmp	r0, #0
	beq	.L61
	mov	r0, r6
.L218:
	mov	r1, r4
	mov	r2, r5
	b	.L201
.L86:
	sub	r6, r6, #2
	cmp	r6, #1
	bhi	.L61
	add	r0, sp, #32
	bl	this_2000_packet_is_from_a_controller_on_the_hub_list
	cmp	r0, #0
	beq	.L61
	mov	r0, r7
	mov	r1, r4
	mov	r2, r5
	bl	RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport
	b	.L61
.L85:
	ldr	r3, .L229+16
	cmp	r2, r3
	bne	.L87
	ldr	r3, [sp, #48]
	cmp	r3, #1
	bne	.L88
.LBB13:
	mov	r0, r5
	add	r1, sp, #52
	ldr	r2, .L229
	mov	r3, #332
	bl	mem_oabia
	cmp	r0, #0
	beq	.L89
	ldr	r6, [sp, #52]
	sub	r3, r5, #2
	sub	r5, r5, #6
	mov	r2, r5
	mov	r0, r6
	add	r1, r4, #2
	str	r3, [sp, #56]
	bl	memcpy
	mov	r0, r6
	mov	r1, r5
	bl	CRC_calculate_32bit_big_endian
	add	r7, r6, r5
	add	r1, sp, #72
	mov	r2, #4
	str	r0, [r1, #-4]!
	mov	r0, r7
	bl	memcpy
	ldr	r1, [sp, #52]
	ldr	r2, [sp, #56]
	mov	r0, r4
	bl	memcpy
	ldr	r0, [sp, #52]
	ldr	r1, .L229
	ldr	r2, .L229+44
	ldr	r6, [sp, #56]
	bl	mem_free_debug
.L89:
.LBE13:
	mov	r1, r4
	mov	r2, #14
	add	r0, sp, #4
	bl	memcpy
.LBB14:
	ldr	r3, .L229+24
	ldr	sl, .L229+28
	mov	r1, #0
	ldr	r0, [r3, #0]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, [sl, #0]
	bl	uxQueueMessagesWaiting
	mov	r5, #0
	mov	r8, r5
	mov	r7, r5
	mov	r9, r0
	b	.L90
.L94:
	mov	r2, #0
	ldr	r0, [sl, #0]
	add	r1, sp, #68
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L91
	cmp	r7, #0
	bne	.L92
	ldr	r3, [sp, #68]
	cmn	r3, #1
	moveq	r8, #1
	beq	.L92
	cmp	r8, #0
	bne	.L92
	ldr	r7, [sp, #4]
	rsb	r1, r7, r3
	rsbs	r7, r1, #0
	adc	r7, r7, r1
.L92:
	mov	r2, #0
	ldr	r0, [sl, #0]
	add	r1, sp, #68
	mov	r3, r2
	bl	xQueueGenericSend
	b	.L93
.L91:
	ldr	r0, .L229+32
	bl	Alert_Message
.L93:
	add	r5, r5, #1
.L90:
	cmp	r5, r9
	bne	.L94
	ldr	r3, .L229+24
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
.LBE14:
	cmp	r7, #0
	movne	r0, #1
	beq	.L61
	b	.L191
.L88:
	cmp	r3, #15
	bne	.L95
	ldrh	r2, [sp, #22]
	ldrh	r1, [sp, #24]
	orr	r1, r2, r1, asl #16
	ldr	r2, .L229+12
	ldr	r2, [r2, #48]
	cmp	r1, r2
	bne	.L61
	b	.L210
.L95:
	cmp	r3, #16
	beq	.L223
.L96:
	cmp	r3, #17
	bne	.L61
	b	.L213
.L87:
	mov	r0, #0
	b	.L193
.L84:
	cmp	r7, #0
	bne	.L97
	ldr	r2, [sp, #44]
	cmp	r2, #2000
	moveq	r0, r7
	moveq	r1, r0
	beq	.L208
	ldr	r3, .L229+16
	cmp	r2, r3
	bne	.L107
	ldr	r3, [sp, #48]
	cmp	r3, #8
	bne	.L100
	mov	r1, r7
	add	r0, sp, #37
	bl	this_communication_address_is_our_serial_number
	cmp	r0, #0
	moveq	r1, r7
	beq	.L196
	b	.L205
.L100:
	cmp	r3, #6
	cmpne	r3, #4
	bne	.L221
	add	r0, sp, #37
	mov	r1, r7
	bl	this_communication_address_is_our_serial_number
	cmp	r0, #0
	mov	r0, #0
	b	.L226
.L97:
	mov	r0, #0
	b	.L202
.L63:
	bl	CONFIG_this_controller_is_behind_a_hub
	subs	r8, r0, #0
	beq	.L104
	cmp	r7, #1
	bne	.L105
	ldr	r2, [sp, #44]
	cmp	r2, #2000
	bne	.L106
	ldr	r3, [sp, #48]
	cmp	r3, #1
	bls	.L61
.L221:
	mov	r0, r7
	mov	r1, r7
	b	.L203
.L106:
	ldr	r3, .L229+16
	cmp	r2, r3
	bne	.L107
	ldr	r3, [sp, #48]
	cmp	r3, #2
	bne	.L108
	ldrh	r2, [sp, #22]
	ldrh	r1, [sp, #24]
	orr	r1, r2, r1, asl #16
	ldr	r2, .L229+12
	ldr	r2, [r2, #48]
	cmp	r1, r2
	bne	.L61
	b	.L206
.L108:
	cmp	r3, #13
	bne	.L109
.L210:
	mov	r0, r7
	mov	r1, r4
	mov	r2, r5
	b	.L190
.L109:
	cmp	r3, #14
	bne	.L110
	ldrh	r3, [sp, #22]
	ldrh	r2, [sp, #24]
	orr	r2, r3, r2, asl #16
	ldr	r3, .L229+12
	ldr	r3, [r3, #48]
	cmp	r2, r3
	bne	.L61
	mov	r0, #1
	mov	r1, r4
	mov	r2, r5
	mov	r3, #14
	b	.L190
.L110:
	cmp	r3, #1
	beq	.L61
	cmp	r3, #16
	beq	.L222
.L111:
	cmp	r3, #17
	movne	r0, #1
	bne	.L227
	b	.L224
.L107:
	mov	r0, r7
	mov	r1, r7
	b	.L204
.L105:
	cmp	r7, #2
	bne	.L113
	ldr	r2, [sp, #44]
	cmp	r2, #2000
	bne	.L114
.L211:
	mov	r0, #1
	mov	r1, r7
.L208:
	ldr	r3, [sp, #48]
	b	.L203
.L114:
	ldr	r3, .L229+16
	cmp	r2, r3
	bne	.L122
	ldr	r3, [sp, #48]
	cmp	r3, #6
	cmpne	r3, #4
	bne	.L116
	mov	r1, sl
	add	r0, sp, #37
	bl	this_communication_address_is_our_serial_number
	cmp	r0, #0
	movne	r0, r7
	moveq	r0, #1
	moveq	r1, r7
	beq	.L192
	b	.L198
.L116:
	cmp	r3, #16
	bne	.L118
.L223:
	mov	r0, r4
	mov	r1, r5
	mov	r2, r7
	b	.L214
.L118:
	cmp	r3, #17
	movne	r0, #1
	movne	r1, #2
	beq	.L213
.L209:
	ldr	r2, .L229+16
	b	.L203
.L113:
	cmp	r7, #0
	bne	.L120
	ldr	r2, [sp, #44]
	cmp	r2, #2000
	beq	.L211
.L121:
	ldr	r3, .L229+16
	cmp	r2, r3
	bne	.L122
	ldr	r3, [sp, #48]
	cmp	r3, #8
	bne	.L123
	mov	r1, r7
	add	r0, sp, #37
	bl	this_communication_address_is_our_serial_number
	cmp	r0, #0
	moveq	r0, #1
	moveq	r1, #0
	bne	.L205
.L196:
	bl	Alert_router_unexp_to_addr_port
	b	.L61
.L123:
	cmp	r3, #6
	cmpne	r3, #4
	bne	.L125
	mov	r1, #0
	add	r0, sp, #37
	bl	this_communication_address_is_our_serial_number
	subs	r1, r0, #0
	moveq	r0, #1
	beq	.L192
	b	.L216
.L125:
	mov	r0, #1
	mov	r1, #0
	b	.L209
.L122:
	mov	r0, #1
	b	.L193
.L120:
	mov	r0, #1
	b	.L202
.L104:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	ldr	r2, [sp, #44]
	cmp	r0, #0
	beq	.L127
	cmp	r2, #2000
	ldreq	r0, .L229+48
	beq	.L200
	ldr	r3, .L229+16
	cmp	r2, r3
	bne	.L129
	cmp	r7, #1
	bne	.L130
	ldr	r3, [sp, #48]
	cmp	r3, #2
	bne	.L131
	ldrh	r2, [sp, #22]
	ldrh	r1, [sp, #24]
	orr	r1, r2, r1, asl #16
	ldr	r2, .L229+12
	ldr	r2, [r2, #48]
	cmp	r1, r2
	ldrne	r0, .L229+52
	bne	.L199
.L206:
	mov	r0, r7
	mov	r1, r4
	mov	r2, r5
.L207:
	bl	CENT_COMM_process_incoming_packet_from_comm_server
	b	.L61
.L131:
	cmp	r3, #11
	bne	.L133
	ldrh	r3, [sp, #22]
	ldrh	r2, [sp, #24]
	orr	r2, r3, r2, asl #16
	ldr	r3, .L229+12
	ldr	r3, [r3, #48]
	cmp	r2, r3
	ldrne	r0, .L229+56
	bne	.L199
	mov	r0, #1
	mov	r1, r4
	mov	r2, r5
	mov	r3, #11
	b	.L190
.L133:
	cmp	r3, #16
	bne	.L135
.L222:
	mov	r0, r4
	mov	r1, r5
	mov	r2, #1
	b	.L214
.L135:
	cmp	r3, #17
	movne	r0, #2
	movne	r1, #1
	bne	.L209
.L224:
	mov	r0, r4
	mov	r1, r5
	mov	r2, #1
	b	.L215
.L130:
	cmp	r7, #2
	bne	.L137
	ldr	r3, [sp, #48]
	cmp	r3, #6
	cmpne	r3, #4
	bne	.L138
	add	r0, sp, #37
	mov	r1, r8
	bl	this_communication_address_is_our_serial_number
	cmp	r0, #0
	mov	r0, #2
.L226:
	moveq	r1, r0
	bne	.L198
.L192:
	bl	Alert_router_received_unexp_token_resp
	b	.L61
.L138:
	cmp	r3, #16
	bne	.L140
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
.L214:
	mov	r3, #28672
	b	.L197
.L140:
	cmp	r3, #17
	movne	r0, #2
	bne	.L227
.L213:
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
.L215:
	mov	r3, #16384
.L197:
	bl	if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task
	b	.L61
.L227:
	mov	r1, r0
	b	.L209
.L137:
	cmp	r7, #0
	bne	.L142
	ldr	r3, [sp, #48]
	cmp	r3, #8
	bne	.L143
	mov	r1, #0
	add	r0, sp, #37
	bl	this_communication_address_is_our_serial_number
	subs	r1, r0, #0
	moveq	r0, #2
	beq	.L196
.L205:
	mov	r0, r4
	sub	r1, r5, #4
	bl	TPMICRO_make_a_copy_and_queue_incoming_packet
	b	.L61
.L143:
	cmp	r3, #6
	cmpne	r3, #4
	movne	r1, #0
	moveq	r1, #1
	bne	.L145
	mov	r1, #0
	add	r0, sp, #37
	bl	this_communication_address_is_our_serial_number
	subs	r1, r0, #0
	moveq	r0, #2
	beq	.L192
.L216:
	mov	r0, #0
.L198:
	mov	r1, r4
	mov	r2, r5
	bl	TPL_IN_make_a_copy_and_direct_incoming_packet
	b	.L61
.L145:
	mov	r0, #2
	b	.L209
.L142:
	mov	r0, #2
.L202:
	bl	Alert_router_unk_port
	b	.L61
.L129:
	mov	r0, #2
	b	.L193
.L127:
	cmp	r2, #2000
	bne	.L147
	ldr	r0, .L229+60
.L200:
	ldr	r3, .L229+64
	ldr	r1, [sp, #48]
	ldr	r2, [r3, r7, asl #2]
	mov	r3, r5
	bl	Alert_Message_va
	b	.L61
.L147:
	ldr	r3, .L229+16
	cmp	r2, r3
	movne	r0, #3
	bne	.L193
	cmp	r7, #1
	str	r0, [sp, #60]
	bne	.L149
	ldr	r3, [sp, #48]
	cmp	r3, #12
	cmpne	r3, #3
	beq	.L150
	cmp	r3, #5
	beq	.L150
	cmp	r3, #4
	beq	.L150
	cmp	r3, #6
	bne	.L151
.L150:
	add	r2, sp, #60
	str	r2, [sp, #0]
	mov	r0, #1
	b	.L186
.L151:
	cmp	r3, #16
	moveq	r0, r4
	moveq	r1, r5
	moveq	r2, #1
	beq	.L217
	cmp	r3, #17
	moveq	r0, r4
	moveq	r1, r5
	moveq	r2, #1
	movne	r0, #3
	movne	r1, #1
	bne	.L187
	b	.L188
.L149:
	cmp	r7, #2
	bne	.L155
	ldr	r3, [sp, #48]
	cmp	r3, #12
	cmpne	r3, #3
	beq	.L156
	cmp	r3, #5
	beq	.L156
	cmp	r3, #4
	beq	.L156
	cmp	r3, #6
	bne	.L157
.L156:
	add	r2, sp, #60
	mov	r0, #2
	str	r2, [sp, #0]
.L186:
	mov	r1, r3
	mov	r2, r4
	mov	r3, r5
	bl	handle_slave_packet_routing.isra.0
	b	.L152
.L157:
	cmp	r3, #16
	bne	.L158
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
.L217:
	mov	r3, #28672
	b	.L189
.L158:
	cmp	r3, #17
	movne	r0, #3
	movne	r1, #2
	bne	.L187
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
.L188:
	mov	r3, #16384
.L189:
	bl	if_addressed_to_us_make_a_copy_and_give_to_the_radio_test_task
	b	.L152
.L155:
	cmp	r7, #0
	bne	.L160
	ldr	r3, [sp, #48]
	cmp	r3, #8
	bne	.L161
	mov	r1, #0
	add	r0, sp, #37
	bl	this_communication_address_is_our_serial_number
	subs	r1, r0, #0
	beq	.L162
	sub	r6, r5, #4
	mov	r0, r4
	mov	r1, r6
	bl	TPMICRO_make_a_copy_and_queue_incoming_packet
	b	.L152
.L162:
	mov	r0, #3
	bl	Alert_router_unexp_to_addr_port
	b	.L152
.L161:
	cmp	r3, #12
	cmpne	r3, #3
	movne	r1, #0
	moveq	r1, #1
	beq	.L163
	cmp	r3, #5
	beq	.L163
	cmp	r3, #4
	beq	.L163
	cmp	r3, #6
	movne	r0, #3
	bne	.L187
.L163:
	add	r2, sp, #60
	str	r2, [sp, #0]
	mov	r0, #0
	b	.L186
.L187:
	ldr	r2, .L229+16
	bl	Alert_router_rcvd_unexp_class
	b	.L152
.L160:
	mov	r0, #3
	bl	Alert_router_unk_port
.L152:
	ldr	r3, [sp, #60]
	cmp	r3, #0
	beq	.L61
	ldr	r3, [sp, #48]
	cmp	r3, #12
	bhi	.L165
	mov	r5, #1
	mov	r2, r5, asl r3
	ldr	r3, .L229+68
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L165
	cmp	r7, #0
	bne	.L167
	mov	r0, r5
	bl	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets
	cmp	r0, #0
	beq	.L168
	mov	r0, r5
	mov	r1, r4
	mov	r2, r6
	mov	r3, r7
	bl	attempt_to_copy_packet_and_route_out_another_port
.L168:
	mov	r0, #2
	bl	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets
	cmp	r0, #0
	beq	.L61
	mov	r0, #2
.L191:
	mov	r1, r4
	mov	r2, r6
.L201:
	mov	r3, #0
	bl	attempt_to_copy_packet_and_route_out_another_port
	b	.L61
.L167:
	cmp	r7, #1
	bne	.L169
	mov	r0, #2
	bl	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets
	cmp	r0, #0
	movne	r0, #2
	beq	.L171
	b	.L219
.L169:
	mov	r0, r5
	bl	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets
	cmp	r0, #0
	beq	.L171
	mov	r0, r5
.L219:
	mov	r1, r4
	mov	r2, r6
	mov	r3, #0
	bl	attempt_to_copy_packet_and_route_out_another_port
.L171:
	mov	r0, #5
	b	.L191
.L165:
	ldr	r0, .L229+72
.L199:
	bl	Alert_Message
	b	.L61
.L193:
	mov	r1, r7
.L204:
	bl	Alert_router_rcvd_unexp_base_class
.L61:
	mov	r0, r4
	ldr	r1, .L229
	ldr	r2, .L229+76
	bl	mem_free_debug
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L50:
	mov	r0, r1
	mov	r1, r2
	bl	CRCIsOK
	cmp	r0, #0
	beq	.L172
	mov	r0, r4
	add	r1, sp, #44
	bl	get_this_packets_message_class
	mov	r1, r4
	mov	r2, #12
	add	r0, sp, #32
	bl	memcpy
	mov	r2, #12
	add	r0, sp, #20
	mov	r1, r4
	bl	memcpy
	ldr	r2, [sp, #44]
	ldr	r3, .L229+16
	cmp	r2, r3
	beq	.L173
.L57:
	ldr	r2, [sp, #44]
	ldr	r3, .L229+16
	cmp	r2, r3
	beq	.L174
.L60:
	cmp	r7, #3
	bne	.L185
	b	.L228
.L230:
	.align	2
.L229:
	.word	.LC1
	.word	889
	.word	.LC3
	.word	config_c
	.word	3000
	.word	in_device_exchange_hammer
	.word	router_hub_list_MUTEX
	.word	router_hub_list_queue
	.word	.LC0
	.word	.LC4
	.word	.LC5
	.word	383
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	port_names
	.word	4216
	.word	.LC10
	.word	2057
.LFE9:
	.size	Route_Incoming_Packet, .-Route_Incoming_Packet
	.section	.text.PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain,"ax",%progbits
	.align	2
	.global	PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain
	.type	PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain, %function
PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r6, r7, lr}
.LCFI8:
	mov	r6, r0
	mov	r7, r1
	mov	r4, r2
	bl	CONFIG_this_controller_is_a_configured_hub
	ldr	r3, .L233
	ldr	r3, [r3, #84]
	cmp	r3, #3
	bne	.L232
	cmp	r0, #0
	bne	.L232
	mov	r0, #2
	mov	r1, r6
	mov	r2, r7
	mov	r3, r4
	bl	attempt_to_copy_packet_and_route_out_another_port
.L232:
	mov	r0, #5
	mov	r1, r6
	mov	r2, r7
	mov	r3, r4
	ldmfd	sp!, {r4, r6, r7, lr}
	b	attempt_to_copy_packet_and_route_out_another_port
.L234:
	.align	2
.L233:
	.word	config_c
.LFE10:
	.size	PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain, .-PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"HUB: unexp hub list queue ERROR\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/packet_router.c\000"
.LC2:
	.ascii	"illogical port choice\000"
.LC3:
	.ascii	"SERIAL: uport out of range. : %s, %u\000"
.LC4:
	.ascii	"HUB: 2000 from commserver, not on list, to addr %c%"
	.ascii	"c%c\000"
.LC5:
	.ascii	"HUB: 3000 from commserver, not on list, to sn %u\000"
.LC6:
	.ascii	"non-hub master ROUTER: 3000 rcvd 2000 packet class "
	.ascii	"%u, port %s, len %u\000"
.LC7:
	.ascii	"non-hub master ROUTER: port A dropping 3000 packet "
	.ascii	"FROM_CENTRAL (error)\000"
.LC8:
	.ascii	"non-hub master ROUTER: port A dropping 3000 code pa"
	.ascii	"cket (error)\000"
.LC9:
	.ascii	"slave ROUTER: 3000 rcvd 2000 packet class %u, port "
	.ascii	"%s, len %u\000"
.LC10:
	.ascii	"ROUTER: surprise class to scatter\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI0-.LFB4
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI1-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI3-.LFB11
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI4-.LFB0
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI5-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI6-.LFB9
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x68
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI8-.LFB10
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/packet_router.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xd6
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF11
	.byte	0x1
	.4byte	.LASF12
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x2d0
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1d7
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x335
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x69
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xdd
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST4
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x186
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x228
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x138
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x27e
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x356
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x818
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST6
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB4
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB8
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI2
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB11
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB0
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB1
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB9
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI7
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 104
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB10
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Co"
	.ascii	"py\000"
.LASF12:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/packet_router.c\000"
.LASF0:
	.ascii	"this_2000_packet_is_from_a_controller_on_the_hub_li"
	.ascii	"st\000"
.LASF5:
	.ascii	"this_2000_packet_from_the_commserver_is_on_the_hub_"
	.ascii	"list\000"
.LASF3:
	.ascii	"attempt_to_copy_packet_and_route_out_another_port\000"
.LASF11:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF9:
	.ascii	"Route_Incoming_Packet\000"
.LASF8:
	.ascii	"this_3000_packet_to_the_commserver_is_on_the_hub_li"
	.ascii	"st\000"
.LASF1:
	.ascii	"if_addressed_to_us_make_a_copy_and_give_to_the_radi"
	.ascii	"o_test_task\000"
.LASF6:
	.ascii	"this_3000_packet_from_the_commserver_is_on_the_hub_"
	.ascii	"list\000"
.LASF4:
	.ascii	"handle_slave_packet_routing\000"
.LASF10:
	.ascii	"PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_o"
	.ascii	"ther_controllers_on_our_chain\000"
.LASF7:
	.ascii	"strip_hub_routing_header_from_packet\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
