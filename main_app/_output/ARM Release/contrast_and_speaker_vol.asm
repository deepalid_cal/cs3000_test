	.file	"contrast_and_speaker_vol.c"
	.text
.Ltext0:
	.section	.text.CONTRAST_AND_SPEAKER_VOL_set_default_values,"ax",%progbits
	.align	2
	.type	CONTRAST_AND_SPEAKER_VOL_set_default_values, %function
CONTRAST_AND_SPEAKER_VOL_set_default_values:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	mov	r0, #50
	mov	r1, #145
	mov	r2, #75
	mov	ip, #0
	stmia	r3, {r0, r1, r2, ip}
	bx	lr
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
.LFE9:
	.size	CONTRAST_AND_SPEAKER_VOL_set_default_values, .-CONTRAST_AND_SPEAKER_VOL_set_default_values
	.section	.text.contrast_and_speaker_volume_updater,"ax",%progbits
	.align	2
	.type	contrast_and_speaker_volume_updater, %function
contrast_and_speaker_volume_updater:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	subs	r2, r0, #0
	str	lr, [sp, #-4]!
.LCFI0:
	bne	.L5
	ldr	r0, .L6
	mov	r1, r2
	ldr	lr, [sp], #4
	b	Alert_Message_va
.L5:
	ldr	r0, .L6+4
	mov	r1, #0
	bl	Alert_Message_va
	ldr	r0, .L6+8
	ldr	lr, [sp], #4
	b	Alert_Message
.L7:
	.align	2
.L6:
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE6:
	.size	contrast_and_speaker_volume_updater, .-contrast_and_speaker_volume_updater
	.section	.text.write_command_and_value_to_dac,"ax",%progbits
	.align	2
	.type	write_command_and_value_to_dac, %function
write_command_and_value_to_dac:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	mov	r5, r0
	mov	r0, #50
	and	r4, r1, #255
	bl	xDisable_ISR
	ldr	r2, .L9
	mov	r3, #256
	str	r3, [r2, #8]
	ldr	r3, .L9+4
	orr	r5, r5, r4, lsr #4
	mov	r4, r4, asl #4
	and	r4, r4, #255
	mvn	r1, #31
	strb	r5, [r3, #2]
	strb	r4, [r3, #1]
	strb	r1, [r3, #0]
	mov	r1, #3
	str	r1, [r3, #8]
	mov	r1, #1
	str	r1, [r3, #12]
	mov	r3, #143
	str	r3, [r2, #8]
	mov	r0, #50
	mov	r3, #432
	str	r3, [r2, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xEnable_ISR
.L10:
	.align	2
.L9:
	.word	1074429952
	.word	.LANCHOR1
.LFE3:
	.size	write_command_and_value_to_dac, .-write_command_and_value_to_dac
	.section	.text.calsense_i2c2_isr,"ax",%progbits
	.align	2
	.type	calsense_i2c2_isr, %function
calsense_i2c2_isr:
.LFB0:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI2:
	ldr	r4, .L24
	ldr	r3, [r4, #4]
	ands	r2, r3, #6
	str	r3, [sp, #4]
	beq	.L12
	ldr	r3, .L24+4
	add	r1, sp, #12
	str	r3, [r1, #-12]!
	ldr	r3, .L24+8
	add	r2, sp, #8
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r3, #0
	bl	xQueueGenericSendFromISR
	mov	r3, #256
	str	r3, [r4, #8]
	mov	r2, #0
	b	.L22
.L12:
	tst	r3, #1
.LBB6:
	ldreq	r3, .L24+12
	moveq	r2, #7
.LBE6:
	beq	.L20
	b	.L23
.L18:
.LBB7:
	ldr	r1, [r3, #8]
	sub	r1, r1, #1
	str	r1, [r3, #8]
	ldr	r1, [r3, #8]
	cmp	r1, #0
	ldrne	r1, [r3, #8]
	streq	r2, [r4, #8]
	ldrneb	r1, [r3, r1]	@ zero_extendqisi2
	ldreqb	r1, [r3, #0]	@ zero_extendqisi2
	andne	r1, r1, #255
	orreq	r1, r1, #512
	str	r1, [r4, #0]
.L20:
	ldr	r1, [r3, #8]
	cmp	r1, #0
	beq	.L11
	ldr	r1, [r4, #4]
	tst	r1, #1024
	beq	.L18
	b	.L11
.L23:
.LBE7:
	mov	r3, #256
	str	r3, [r4, #8]
.L22:
	ldr	r3, .L24+12
	str	r2, [r3, #12]
.L11:
	ldmfd	sp!, {r1, r2, r3, r4, pc}
.L25:
	.align	2
.L24:
	.word	1074429952
	.word	17476
	.word	Contrast_and_Volume_Queue
	.word	.LANCHOR1
.LFE0:
	.size	calsense_i2c2_isr, .-calsense_i2c2_isr
	.global	__udivsi3
	.section	.text.contrast_and_speaker_volume_control_task,"ax",%progbits
	.align	2
	.global	contrast_and_speaker_volume_control_task
	.type	contrast_and_speaker_volume_control_task, %function
contrast_and_speaker_volume_control_task:
.LFB4:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI3:
	ldr	r6, .L42
	sub	sp, sp, #24
.LCFI4:
.LBB11:
	mov	r1, #1
.LBE11:
	rsb	r6, r6, r0
.LBB12:
	mov	r0, #10
	bl	clkpwr_clk_en_dis
	mov	r5, #50
	mov	r0, #4
	str	r5, [sp, #16]
	str	r5, [sp, #20]
	bl	clkpwr_get_base_clock_rate
.LBE12:
	mov	r6, r6, lsr #5
.LBB13:
	str	r0, [sp, #12]
	ldr	r4, [sp, #20]
	ldr	r0, [sp, #12]
	ldr	r3, [sp, #16]
	ldr	r1, [sp, #20]
	add	r1, r1, r3
	bl	__udivsi3
	ldr	r1, .L42+4
	mul	r0, r4, r0
	bl	__udivsi3
	ldr	r4, .L42+8
	str	r0, [r4, #16]
	ldr	r7, [sp, #16]
	ldr	r0, [sp, #12]
	ldr	r3, [sp, #16]
	ldr	r1, [sp, #20]
	add	r1, r1, r3
	bl	__udivsi3
	ldr	r1, .L42+4
	mul	r0, r7, r0
	bl	__udivsi3
	mov	r1, #1
	mov	r7, #0
	str	r0, [r4, #12]
	mov	r0, #2
	bl	clkpwr_set_i2c_driver
	mov	r3, #256
	str	r3, [r4, #8]
	ldr	r4, .L42+12
	mov	r0, r5
	str	r7, [r4, #12]
	bl	xDisable_ISR
	mov	r0, r5
	mov	r1, #13
	mov	r2, #1
	ldr	r3, .L42+16
	str	r7, [sp, #0]
	bl	xSetISR_Vector
.LBE13:
	ldr	r5, .L42+20
.L36:
	ldr	r0, [r5, #0]
	add	r1, sp, #4
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L28
	b	.L40
.L30:
	bl	vTaskDelay
	subs	r7, r7, #1
	bne	.L27
	ldr	r0, .L42+24
	bl	Alert_Message
	b	.L29
.L40:
	mov	r7, #10
.L27:
	ldr	r0, [r4, #12]
	cmp	r0, #1
	beq	.L30
.L29:
	ldr	r3, [sp, #4]
	ldr	r2, .L42+28
	cmp	r3, r2
	beq	.L32
	bhi	.L35
	ldr	r2, .L42+32
	cmp	r3, r2
	bne	.L28
	b	.L41
.L35:
	ldr	r2, .L42+36
	cmp	r3, r2
	beq	.L33
	ldr	r2, .L42+40
	cmp	r3, r2
	bne	.L28
	ldr	r0, .L42+44
	bl	Alert_Message
	b	.L28
.L41:
.LBB14:
	mov	r0, #50
	bl	xDisable_ISR
	ldr	r3, .L42+8
	mov	r2, #256
	str	r2, [r3, #8]
	mvn	r2, #15
	strb	r2, [r4, #1]
	mov	r2, #12
	strb	r2, [r4, #0]
	mov	r2, #2
	str	r2, [r4, #8]
	mov	r2, #1
	str	r2, [r4, #12]
	mov	r2, #143
	str	r2, [r3, #8]
	mov	r2, #432
	str	r2, [r3, #0]
	mov	r0, #50
	bl	xEnable_ISR
	b	.L28
.L32:
.LBE14:
	mov	r0, #80
	b	.L39
.L33:
	mov	r0, #64
.L39:
	ldr	r1, [sp, #8]
	bl	write_command_and_value_to_dac
.L28:
	bl	xTaskGetTickCount
	ldr	r3, .L42+48
	str	r0, [r3, r6, asl #2]
	b	.L36
.L43:
	.align	2
.L42:
	.word	Task_Table
	.word	200000
	.word	1074429952
	.word	.LANCHOR1
	.word	calsense_i2c2_isr
	.word	Contrast_and_Volume_Queue
	.word	.LC3
	.word	8738
	.word	4369
	.word	13107
	.word	17476
	.word	.LC4
	.word	task_last_execution_stamp
.LFE4:
	.size	contrast_and_speaker_volume_control_task, .-contrast_and_speaker_volume_control_task
	.section	.text.postContrast_or_Volume_Event,"ax",%progbits
	.align	2
	.global	postContrast_or_Volume_Event
	.type	postContrast_or_Volume_Event, %function
postContrast_or_Volume_Event:
.LFB5:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L46
	mov	r2, #0
	stmfd	sp!, {r0, r1, lr}
.LCFI5:
	stmia	sp, {r0, r1}
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L44
	ldr	r0, .L46+4
	bl	Alert_Message
.L44:
	ldmfd	sp!, {r2, r3, pc}
.L47:
	.align	2
.L46:
	.word	Contrast_and_Volume_Queue
	.word	.LC5
.LFE5:
	.size	postContrast_or_Volume_Event, .-postContrast_or_Volume_Event
	.section	.text.save_file_contrast_and_speaker_vol,"ax",%progbits
	.align	2
	.global	save_file_contrast_and_speaker_vol
	.type	save_file_contrast_and_speaker_vol, %function
save_file_contrast_and_speaker_vol:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #16
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI6:
	ldr	r1, .L49
	str	r3, [sp, #0]
	mov	r2, #0
	mov	r3, #15
	stmib	sp, {r2, r3}
	ldr	r3, .L49+4
	mov	r0, #1
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L50:
	.align	2
.L49:
	.word	.LANCHOR2
	.word	.LANCHOR0
.LFE8:
	.size	save_file_contrast_and_speaker_vol, .-save_file_contrast_and_speaker_vol
	.section	.text.CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars,"ax",%progbits
	.align	2
	.global	CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars
	.type	CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars, %function
CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	ldr	r4, .L52
	ldr	r3, .L52+4
	ldr	r5, [r4, #0]
	mov	r0, #12
	ldr	r2, [r3, #0]
	mul	r0, r5, r0
	ldr	r3, .L52+8
	mov	r1, #5
	str	r2, [r3, #0]
	sub	r0, r0, #180
	bl	__udivsi3
	ldr	r3, .L52+12
	mov	r1, #6
	ldr	r2, .L52+16
	str	r0, [r3, #0]
	ldr	r3, .L52+20
	str	r5, [r3, #0]
	ldr	r3, [r4, #4]
	mul	r1, r3, r1
	sub	r1, r1, #780
	str	r1, [r2, #0]
	ldr	r2, .L52+24
	ldr	r1, .L52+28
	str	r3, [r2, #0]
	ldr	r2, [r4, #8]
	mov	r3, #11
	mul	r3, r2, r3
	sub	r3, r3, #712
	sub	r3, r3, #3
	str	r3, [r1, #0]
	ldr	r3, .L52+32
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, pc}
.L53:
	.align	2
.L52:
	.word	.LANCHOR0
	.word	display_model_is
	.word	GuiVar_DisplayType
	.word	GuiVar_BacklightSliderPos
	.word	GuiVar_ContrastSliderPos
	.word	GuiVar_Backlight
	.word	GuiVar_Contrast
	.word	GuiVar_VolumeSliderPos
	.word	GuiVar_Volume
.LFE10:
	.size	CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars, .-CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars
	.section	.text.LED_backlight_brighter,"ax",%progbits
	.align	2
	.global	LED_backlight_brighter
	.type	LED_backlight_brighter, %function
LED_backlight_brighter:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L57
	str	lr, [sp, #-4]!
.LCFI8:
	ldr	r2, [r3, #0]
	cmp	r2, #89
	bhi	.L56
	add	r2, r2, #5
	str	r2, [r3, #0]
	bl	led_backlight_set_brightness
	mov	r0, #1
	ldr	pc, [sp], #4
.L56:
	mov	r0, #0
	ldr	pc, [sp], #4
.L58:
	.align	2
.L57:
	.word	.LANCHOR0
.LFE11:
	.size	LED_backlight_brighter, .-LED_backlight_brighter
	.section	.text.LED_backlight_darker,"ax",%progbits
	.align	2
	.global	LED_backlight_darker
	.type	LED_backlight_darker, %function
LED_backlight_darker:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L62
	str	lr, [sp, #-4]!
.LCFI9:
	ldr	r2, [r3, #0]
	cmp	r2, #15
	bls	.L61
	sub	r2, r2, #5
	str	r2, [r3, #0]
	bl	led_backlight_set_brightness
	mov	r0, #1
	ldr	pc, [sp], #4
.L61:
	mov	r0, #0
	ldr	pc, [sp], #4
.L63:
	.align	2
.L62:
	.word	.LANCHOR0
.LFE12:
	.size	LED_backlight_darker, .-LED_backlight_darker
	.section	.text.LCD_contrast_DARKEN,"ax",%progbits
	.align	2
	.global	LCD_contrast_DARKEN
	.type	LCD_contrast_DARKEN, %function
LCD_contrast_DARKEN:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L67
	str	lr, [sp, #-4]!
.LCFI10:
	ldr	r1, [r3, #4]
	cmp	r1, #159
	bhi	.L66
	add	r1, r1, #1
	ldr	r0, .L67+4
	str	r1, [r3, #4]
	bl	postContrast_or_Volume_Event
	mov	r0, #1
	ldr	pc, [sp], #4
.L66:
	mov	r0, #0
	ldr	pc, [sp], #4
.L68:
	.align	2
.L67:
	.word	.LANCHOR0
	.word	8738
.LFE13:
	.size	LCD_contrast_DARKEN, .-LCD_contrast_DARKEN
	.section	.text.LCD_contrast_LIGHTEN,"ax",%progbits
	.align	2
	.global	LCD_contrast_LIGHTEN
	.type	LCD_contrast_LIGHTEN, %function
LCD_contrast_LIGHTEN:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L72
	str	lr, [sp, #-4]!
.LCFI11:
	ldr	r1, [r3, #4]
	cmp	r1, #130
	bls	.L71
	sub	r1, r1, #1
	ldr	r0, .L72+4
	str	r1, [r3, #4]
	bl	postContrast_or_Volume_Event
	mov	r0, #1
	ldr	pc, [sp], #4
.L71:
	mov	r0, #0
	ldr	pc, [sp], #4
.L73:
	.align	2
.L72:
	.word	.LANCHOR0
	.word	8738
.LFE14:
	.size	LCD_contrast_LIGHTEN, .-LCD_contrast_LIGHTEN
	.section	.text.SPEAKER_vol_increase,"ax",%progbits
	.align	2
	.global	SPEAKER_vol_increase
	.type	SPEAKER_vol_increase, %function
SPEAKER_vol_increase:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L77
	str	lr, [sp, #-4]!
.LCFI12:
	ldr	r1, [r3, #8]
	cmp	r1, #81
	bhi	.L76
	add	r1, r1, #1
	ldr	r0, .L77+4
	str	r1, [r3, #8]
	bl	postContrast_or_Volume_Event
	mov	r0, #1
	ldr	pc, [sp], #4
.L76:
	mov	r0, #0
	ldr	pc, [sp], #4
.L78:
	.align	2
.L77:
	.word	.LANCHOR0
	.word	13107
.LFE15:
	.size	SPEAKER_vol_increase, .-SPEAKER_vol_increase
	.section	.text.SPEAKER_vol_decrease,"ax",%progbits
	.align	2
	.global	SPEAKER_vol_decrease
	.type	SPEAKER_vol_decrease, %function
SPEAKER_vol_decrease:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L82
	str	lr, [sp, #-4]!
.LCFI13:
	ldr	r1, [r3, #8]
	cmp	r1, #65
	bls	.L81
	sub	r1, r1, #1
	ldr	r0, .L82+4
	str	r1, [r3, #8]
	bl	postContrast_or_Volume_Event
	mov	r0, #1
	ldr	pc, [sp], #4
.L81:
	mov	r0, #0
	ldr	pc, [sp], #4
.L83:
	.align	2
.L82:
	.word	.LANCHOR0
	.word	13107
.LFE16:
	.size	SPEAKER_vol_decrease, .-SPEAKER_vol_decrease
	.section	.text.CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty,"ax",%progbits
	.align	2
	.global	CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty
	.type	CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty, %function
CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI14:
	ldr	r0, .L86
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L86+4
	mov	r1, #15
	str	r3, [sp, #4]
	ldr	r3, .L86+8
	mov	r2, #90
	str	r3, [sp, #8]
	ldr	r3, .L86+12
	str	r3, [sp, #12]
	mov	r3, #50
	bl	RC_uint32_with_filename
	cmp	r0, #0
	bne	.L85
	bl	save_file_contrast_and_speaker_vol
.L85:
	ldr	r3, .L86
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L87:
	.align	2
.L86:
	.word	.LANCHOR0
	.word	.LC6
	.word	.LC7
	.word	603
.LFE17:
	.size	CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty, .-CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty
	.section	.text.CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value,"ax",%progbits
	.align	2
	.global	CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value
	.type	CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value, %function
CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI15:
	ldr	r0, .L90
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L90+4
	mov	r1, #130
	str	r3, [sp, #4]
	ldr	r3, .L90+8
	mov	r2, #160
	str	r3, [sp, #8]
	mov	r3, #616
	str	r3, [sp, #12]
	mov	r3, #145
	bl	RC_uint32_with_filename
	cmp	r0, #0
	bne	.L89
	bl	save_file_contrast_and_speaker_vol
.L89:
	ldr	r3, .L90+12
	ldr	r0, [r3, #4]
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L91:
	.align	2
.L90:
	.word	.LANCHOR0+4
	.word	.LC8
	.word	.LC7
	.word	.LANCHOR0
.LFE18:
	.size	CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value, .-CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value
	.section	.text.CONTRAST_AND_SPEAKER_VOL_get_speaker_volume,"ax",%progbits
	.align	2
	.global	CONTRAST_AND_SPEAKER_VOL_get_speaker_volume
	.type	CONTRAST_AND_SPEAKER_VOL_get_speaker_volume, %function
CONTRAST_AND_SPEAKER_VOL_get_speaker_volume:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI16:
	ldr	r0, .L94
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L94+4
	mov	r1, #65
	str	r3, [sp, #4]
	ldr	r3, .L94+8
	mov	r2, #82
	str	r3, [sp, #8]
	mov	r3, #632
	str	r3, [sp, #12]
	mov	r3, #75
	bl	RC_uint32_with_filename
	cmp	r0, #0
	bne	.L93
	bl	save_file_contrast_and_speaker_vol
.L93:
	ldr	r3, .L94+12
	ldr	r0, [r3, #8]
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L95:
	.align	2
.L94:
	.word	.LANCHOR0+8
	.word	.LC9
	.word	.LC7
	.word	.LANCHOR0
.LFE19:
	.size	CONTRAST_AND_SPEAKER_VOL_get_speaker_volume, .-CONTRAST_AND_SPEAKER_VOL_get_speaker_volume
	.section	.text.init_file_contrast_and_speaker_vol,"ax",%progbits
	.align	2
	.global	init_file_contrast_and_speaker_vol
	.type	init_file_contrast_and_speaker_vol, %function
init_file_contrast_and_speaker_vol:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI17:
	ldr	r1, .L97
	mov	r3, #16
	str	r3, [sp, #0]
	ldr	r3, .L97+4
	mov	r2, #0
	str	r3, [sp, #8]
	ldr	r3, .L97+8
	mov	r0, #1
	str	r3, [sp, #12]
	mov	r3, #15
	str	r3, [sp, #16]
	ldr	r3, .L97+12
	str	r2, [sp, #4]
	bl	FLASH_FILE_find_or_create_variable_file
	bl	CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value
	mov	r1, r0
	ldr	r0, .L97+16
	bl	postContrast_or_Volume_Event
	bl	CONTRAST_AND_SPEAKER_VOL_get_speaker_volume
	mov	r1, r0
	ldr	r0, .L97+20
	add	sp, sp, #20
	ldr	lr, [sp], #4
	b	postContrast_or_Volume_Event
.L98:
	.align	2
.L97:
	.word	.LANCHOR2
	.word	contrast_and_speaker_volume_updater
	.word	CONTRAST_AND_SPEAKER_VOL_set_default_values
	.word	.LANCHOR0
	.word	8738
	.word	13107
.LFE7:
	.size	init_file_contrast_and_speaker_vol, .-init_file_contrast_and_speaker_vol
	.section	.text.CONTRAST_AND_SPEAKER_VOL_get_screen_language,"ax",%progbits
	.align	2
	.global	CONTRAST_AND_SPEAKER_VOL_get_screen_language
	.type	CONTRAST_AND_SPEAKER_VOL_get_screen_language, %function
CONTRAST_AND_SPEAKER_VOL_get_screen_language:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI18:
	ldr	r3, .L101
	mov	r1, #0
	str	r3, [sp, #4]
	ldr	r3, .L101+4
	ldr	r0, .L101+8
	str	r3, [sp, #8]
	ldr	r3, .L101+12
	mov	r2, #1
	str	r3, [sp, #12]
	mov	r3, r1
	str	r1, [sp, #0]
	bl	RC_uint32_with_filename
	cmp	r0, #0
	bne	.L100
	bl	save_file_contrast_and_speaker_vol
.L100:
	ldr	r3, .L101+16
	ldr	r0, [r3, #12]
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L102:
	.align	2
.L101:
	.word	.LC10
	.word	.LC7
	.word	.LANCHOR0+12
	.word	645
	.word	.LANCHOR0
.LFE20:
	.size	CONTRAST_AND_SPEAKER_VOL_get_screen_language, .-CONTRAST_AND_SPEAKER_VOL_get_screen_language
	.section	.text.CONTRAST_AND_SPEAKER_VOL_set_screen_language,"ax",%progbits
	.align	2
	.global	CONTRAST_AND_SPEAKER_VOL_set_screen_language
	.type	CONTRAST_AND_SPEAKER_VOL_set_screen_language, %function
CONTRAST_AND_SPEAKER_VOL_set_screen_language:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	ldrls	r3, .L105
	strls	r0, [r3, #12]
	bx	lr
.L106:
	.align	2
.L105:
	.word	.LANCHOR0
.LFE21:
	.size	CONTRAST_AND_SPEAKER_VOL_set_screen_language, .-CONTRAST_AND_SPEAKER_VOL_set_screen_language
	.section	.rodata.CONTRAST_AND_SPEAKER_VOL_FILENAME,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	CONTRAST_AND_SPEAKER_VOL_FILENAME, %object
	.size	CONTRAST_AND_SPEAKER_VOL_FILENAME, 24
CONTRAST_AND_SPEAKER_VOL_FILENAME:
	.ascii	"CONTRAST_AND_SPEARK_VOL\000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C & V file unexpd update %u\000"
.LC1:
	.ascii	"C & V file update : to revision %u from %u\000"
.LC2:
	.ascii	"C & V updater error\000"
.LC3:
	.ascii	"Unexpected C&V I2C delay!\000"
.LC4:
	.ascii	"C & V I2C error detected by the isr\000"
.LC5:
	.ascii	"Contrast and Volume QUEUE OVERFLOW!\000"
.LC6:
	.ascii	"LED Backlight Duty\000"
.LC7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/"
	.ascii	"contrast_and_speaker_vol.c\000"
.LC8:
	.ascii	"LCD Contrast\000"
.LC9:
	.ascii	"Speaker Volume\000"
.LC10:
	.ascii	"Screen Language\000"
	.section	.bss.c_and_sv,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	c_and_sv, %object
	.size	c_and_sv, 16
c_and_sv:
	.space	16
	.section	.bss.i2c2cs,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	i2c2cs, %object
	.size	i2c2cs, 16
i2c2cs:
	.space	16
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI0-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI2-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI6-.LFB8
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI7-.LFB10
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI8-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI9-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI10-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI12-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI13-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI14-.LFB17
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI15-.LFB18
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI16-.LFB19
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI17-.LFB7
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI18-.LFB20
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.align	2
.LEFDE38:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/contrast_and_speaker_vol.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1cf
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF22
	.byte	0x1
	.4byte	.LASF23
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x3b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x8b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x1ca
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x175
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0xa7
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x71
	.byte	0x1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xc6
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x11c
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1be
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST5
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1d6
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST6
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1e5
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST7
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1f8
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST8
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x20b
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST9
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x21e
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST10
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x231
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST11
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x245
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST12
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x259
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST13
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x266
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST14
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x273
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST15
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x1aa
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST16
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x283
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST17
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x290
	.4byte	.LFB21
	.4byte	.LFE21
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB6
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB8
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB10
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB11
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB12
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB13
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB15
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB16
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB17
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB18
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB19
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB7
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB20
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xb4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"write_command_and_value_to_dac\000"
.LASF23:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/"
	.ascii	"contrast_and_speaker_vol.c\000"
.LASF14:
	.ascii	"SPEAKER_vol_increase\000"
.LASF18:
	.ascii	"CONTRAST_AND_SPEAKER_VOL_get_speaker_volume\000"
.LASF12:
	.ascii	"LCD_contrast_DARKEN\000"
.LASF19:
	.ascii	"init_file_contrast_and_speaker_vol\000"
.LASF11:
	.ascii	"LED_backlight_darker\000"
.LASF16:
	.ascii	"CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty\000"
.LASF22:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF13:
	.ascii	"LCD_contrast_LIGHTEN\000"
.LASF3:
	.ascii	"contrast_and_speaker_volume_updater\000"
.LASF0:
	.ascii	"calsense_i2c2_isr\000"
.LASF10:
	.ascii	"LED_backlight_brighter\000"
.LASF1:
	.ascii	"powerup_both_dac_channels\000"
.LASF5:
	.ascii	"init_contrast_and_volume_i2c2_channel\000"
.LASF6:
	.ascii	"contrast_and_speaker_volume_control_task\000"
.LASF9:
	.ascii	"CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivar"
	.ascii	"s\000"
.LASF2:
	.ascii	"CONTRAST_AND_SPEAKER_VOL_set_default_values\000"
.LASF21:
	.ascii	"CONTRAST_AND_SPEAKER_VOL_set_screen_language\000"
.LASF15:
	.ascii	"SPEAKER_vol_decrease\000"
.LASF7:
	.ascii	"postContrast_or_Volume_Event\000"
.LASF20:
	.ascii	"CONTRAST_AND_SPEAKER_VOL_get_screen_language\000"
.LASF8:
	.ascii	"save_file_contrast_and_speaker_vol\000"
.LASF17:
	.ascii	"CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
