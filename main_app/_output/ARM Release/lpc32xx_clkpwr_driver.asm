	.file	"lpc32xx_clkpwr_driver.c"
	.text
.Ltext0:
	.section	.text.clkpwr_abs,"ax",%progbits
	.align	2
	.global	clkpwr_abs
	.type	clkpwr_abs, %function
clkpwr_abs:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, r1
	rsbgt	r0, r1, r0
	rsble	r0, r0, r1
	bx	lr
.LFE0:
	.size	clkpwr_abs, .-clkpwr_abs
	.global	__udivdi3
	.section	.text.clkpwr_check_pll_setup,"ax",%progbits
	.align	2
	.global	clkpwr_check_pll_setup
	.type	clkpwr_check_pll_setup, %function
clkpwr_check_pll_setup:
.LFB1:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldmib	r1, {r2, r3}
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	ldr	r7, [r1, #20]
	mov	r3, r3, asl #1
	orr	r3, r3, r2, asl #2
	ldr	r2, [r1, #12]
	mov	r4, r0
	orr	r3, r3, r2
	ldr	r0, [r1, #24]
	mov	r5, r7
	mov	r6, r7, asr #31
	ldr	r8, [r1, #16]
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L5
.L11:
	.word	.L6
	.word	.L7
	.word	.L8
	.word	.L8
	.word	.L9
	.word	.L9
	.word	.L14
	.word	.L14
.L5:
	mov	sl, #0
	mov	fp, #0
	mov	r4, sl
	mov	r5, fp
	b	.L17
.L6:
	umull	r2, r3, r4, r0
	stmia	sp, {r2-r3}
	smull	r2, r3, r7, r8
	ldmia	sp, {r0-r1}
	adds	r2, r2, r2
	adc	r3, r3, r3
	bl	__udivdi3
	mov	r2, r7
	mov	r3, r6
	mov	sl, r0
	ldmia	sp, {r0-r1}
	b	.L19
.L7:
	umull	r2, r3, r4, r0
	stmia	sp, {r2-r3}
	mov	r0, r2
	mov	r1, r3
	mov	r2, r7
	mov	r3, r6
	bl	__udivdi3
	smull	r2, r3, r7, r8
	adds	r2, r2, r2
	adc	r3, r3, r3
	mov	sl, r0
	ldmia	sp, {r0-r1}
.L19:
	bl	__udivdi3
	mov	r2, r5
	mov	r3, r6
	mov	r8, r0
	mov	r9, r1
	mov	r0, r4
	mov	r1, #0
	bl	__udivdi3
	mov	r4, r0
	mov	r5, r1
	b	.L10
.L8:
	umull	r2, r3, r4, r0
	mov	r0, r2
	mov	r1, r3
	mov	r2, r7
	mov	r3, r6
	bl	__udivdi3
	mov	r2, r7
	mov	r3, r6
	mov	sl, r0
	mov	fp, r1
	mov	r0, r4
	mov	r1, #0
	bl	__udivdi3
	mov	r4, r0
	mov	r5, r1
.L17:
	mov	r8, sl
	mov	r9, fp
	b	.L10
.L9:
	mov	r2, #2
	mov	r0, r4
	smull	r4, r5, r8, r2
	mov	r1, #0
	mov	r2, r4
	mov	r3, r5
	bl	__udivdi3
	mov	sl, r0
	b	.L18
.L14:
	mov	sl, r4
.L18:
	ldr	r4, .L20
	ldr	r8, .L20+4
	mov	r5, #0
	mov	r9, #0
.L10:
	ldr	r0, .L20+8
	ldr	r2, .L20+12
	adds	r0, r0, r8
	mvn	r1, #0
	adc	r1, r1, r9
	mov	r3, #0
	cmp	r3, r1
	cmpeq	r2, r0
	ldr	r0, .L20+16
	ldr	r2, .L20+20
	movcc	sl, #0
	movcc	fp, #0
	mvn	r1, #0
	adds	r0, r0, r4
	adc	r1, r1, r5
	mov	r3, #0
	cmp	r3, r1
	cmpeq	r2, r0
	movcc	sl, #0
	movcc	fp, #0
	mov	r0, sl
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L21:
	.align	2
.L20:
	.word	1000000
	.word	156000000
	.word	-156000000
	.word	164000000
	.word	-1000000
	.word	26000000
.LFE1:
	.size	clkpwr_check_pll_setup, .-clkpwr_check_pll_setup
	.section	.text.clkpwr_pll_rate_from_val,"ax",%progbits
	.align	2
	.global	clkpwr_pll_rate_from_val
	.type	clkpwr_pll_rate_from_val, %function
clkpwr_pll_rate_from_val:
.LFB2:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI1:
	tst	r1, #32768
	mov	r3, #0
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	movne	r3, #1
	strne	r3, [sp, #4]
	tst	r1, #16384
	movne	r3, #1
	strne	r3, [sp, #8]
	tst	r1, #8192
	movne	r3, #1
	strne	r3, [sp, #12]
	mov	r3, r1, lsr #1
	and	r3, r3, #255
	add	r3, r3, #1
	str	r3, [sp, #24]
	mov	r3, r1, lsr #9
	and	r3, r3, #3
	add	r3, r3, #1
	str	r3, [sp, #20]
	ldr	r3, .L26
	mov	r1, r1, lsr #11
	and	r1, r1, #3
	ldr	r3, [r3, r1, asl #2]
	mov	r1, sp
	str	r3, [sp, #16]
	bl	clkpwr_check_pll_setup
	add	sp, sp, #28
	ldmfd	sp!, {pc}
.L27:
	.align	2
.L26:
	.word	.LANCHOR0
.LFE2:
	.size	clkpwr_pll_rate_from_val, .-clkpwr_pll_rate_from_val
	.section	.text.clkpwr_pll_rate,"ax",%progbits
	.align	2
	.global	clkpwr_pll_rate
	.type	clkpwr_pll_rate, %function
clkpwr_pll_rate:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r1, #0]
	b	clkpwr_pll_rate_from_val
.LFE3:
	.size	clkpwr_pll_rate, .-clkpwr_pll_rate
	.section	.text.clkpwr_mask_and_set,"ax",%progbits
	.align	2
	.global	clkpwr_mask_and_set
	.type	clkpwr_mask_and_set, %function
clkpwr_mask_and_set:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	cmp	r2, #0
	bic	r3, r3, r1
	orrne	r3, r3, r1
	str	r3, [r0, #0]
	bx	lr
.LFE4:
	.size	clkpwr_mask_and_set, .-clkpwr_mask_and_set
	.section	.text.clkpwr_get_event_field,"ax",%progbits
	.align	2
	.global	clkpwr_get_event_field
	.type	clkpwr_get_event_field, %function
clkpwr_get_event_field:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #31
	bhi	.L32
	ldr	r3, .L37
	str	r3, [r1, #0]
	add	r3, r3, #8
	str	r3, [r1, #4]
	sub	r3, r3, #4
	str	r3, [r1, #8]
	add	r3, r3, #8
	str	r3, [r1, #12]
	b	.L36
.L32:
	cmp	r0, #63
	bhi	.L34
	ldr	r3, .L37+4
	rsb	r0, r0, #64
	str	r3, [r1, #0]
	add	r3, r3, #8
	str	r3, [r1, #4]
	sub	r3, r3, #4
	str	r3, [r1, #8]
	add	r3, r3, #8
	str	r3, [r1, #12]
	b	.L36
.L34:
	cmp	r0, #95
	mov	r3, #0
	bhi	.L35
.LBB16:
	ldr	ip, .L37+8
	rsb	r0, r0, #96
	str	ip, [r1, #0]
	str	r3, [r1, #4]
	str	r3, [r1, #8]
	str	r3, [r1, #12]
.L36:
	str	r0, [r2, #0]
	mov	r0, #1
	bx	lr
.L35:
.LBE16:
	mov	r0, r3
	bx	lr
.L38:
	.align	2
.L37:
	.word	1073758240
	.word	1073758256
	.word	1073758232
.LFE5:
	.size	clkpwr_get_event_field, .-clkpwr_get_event_field
	.section	.text.clkpwr_get_base_clock,"ax",%progbits
	.align	2
	.global	clkpwr_get_base_clock
	.type	clkpwr_get_base_clock, %function
clkpwr_get_base_clock:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #34
	movhi	r3, #10
	bhi	.L40
	ldr	r3, .L49
	cmp	r0, #14
	ldr	r3, [r3, r0, asl #2]
	beq	.L42
	cmp	r0, #15
	beq	.L43
	cmp	r0, #13
	bne	.L40
	ldr	r2, .L49+4
	ldr	r2, [r2, #96]
	tst	r2, #256
	b	.L48
.L42:
	ldr	r2, .L49+4
	ldr	r2, [r2, #184]
	tst	r2, #8
.L48:
	movne	r3, #5
	b	.L40
.L43:
	ldr	r2, .L49+4
	ldr	r2, [r2, #184]
	tst	r2, #2
	b	.L48
.L40:
	mov	r0, r3
	bx	lr
.L50:
	.align	2
.L49:
	.word	.LANCHOR1
	.word	1073758208
.LFE6:
	.size	clkpwr_get_base_clock, .-clkpwr_get_base_clock
	.section	.text.clkpwr_find_pll_cfg,"ax",%progbits
	.align	2
	.global	clkpwr_find_pll_cfg
	.type	clkpwr_find_pll_cfg, %function
clkpwr_find_pll_cfg:
.LFB7:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI2:
	mov	r5, r1
	mov	r6, r0
	umull	r0, r1, r2, r5
	mov	r4, r3
	mov	r3, r2, asr #31
	mla	r1, r5, r3, r1
	mov	r2, #1000
	mov	r3, #0
	bl	__udivdi3
.LBB17:
	cmp	r6, r5
	rsbgt	r3, r5, r6
	rsble	r3, r6, r5
.LBE17:
	cmp	r3, r0
	mov	r7, r0
	mov	r0, #0
	bhi	.L54
	mov	r3, #1
	str	r0, [r4, #0]
	mov	r1, r4
	mov	r0, r6
	str	r3, [r4, #4]
	str	r3, [r4, #8]
	str	r3, [r4, #12]
	str	r3, [r4, #16]
	str	r3, [r4, #20]
	str	r3, [r4, #24]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	clkpwr_check_pll_setup
.L54:
	cmp	r5, r6
	bhi	.L55
	mov	r3, #1
	str	r3, [r4, #4]
	str	r3, [r4, #12]
	str	r3, [r4, #20]
	str	r3, [r4, #24]
	mov	r3, r0
	str	r0, [r4, #0]
	str	r0, [r4, #8]
	ldr	sl, .L89
	mov	r8, r3
	b	.L56
.L59:
	ldr	r3, [sl, #4]!
	mov	r0, r6
	str	r3, [r4, #16]
	mov	r1, r4
	bl	clkpwr_check_pll_setup
	add	r8, r8, #1
.LBB18:
	cmp	r5, r0
	rsbgt	r3, r0, r5
	rsble	r3, r5, r0
	cmp	r3, r7
	movhi	r3, #0
	movls	r3, #1
.L56:
.LBE18:
	eor	r2, r3, #1
	cmp	r8, #3
	movhi	r2, #0
	andls	r2, r2, #1
	cmp	r2, #0
	bne	.L59
	cmp	r3, #0
	bne	.L80
.L55:
	mov	r8, #1
	mov	r3, #0
	str	r8, [r4, #0]
	stmib	r4, {r3, r8}
	str	r3, [r4, #12]
	str	r8, [r4, #16]
	b	.L61
.L64:
	str	sl, [r4, #20]
	str	r8, [r4, #24]
	mov	r0, r6
	mov	r1, r4
	bl	clkpwr_check_pll_setup
	add	sl, sl, #1
.LBB19:
	cmp	r5, r0
	rsbgt	r3, r0, r5
	rsble	r3, r5, r0
	cmp	r3, r7
	movhi	r3, #0
	movls	r3, #1
	b	.L65
.L83:
.LBE19:
	mov	r3, #0
	mov	sl, #1
.L65:
	eor	r2, r3, #1
	cmp	sl, #4
	movhi	r2, #0
	andls	r2, r2, #1
	cmp	r2, #0
	bne	.L64
	add	r8, r8, #1
.L61:
	eor	r2, r3, #1
	cmp	r8, #256
	movhi	r2, #0
	andls	r2, r2, #1
	cmp	r2, #0
	bne	.L83
	cmp	r3, #0
	bne	.L80
	mov	r8, #1
	str	r3, [r4, #8]
	str	r8, [r4, #12]
	ldr	fp, .L89+4
	b	.L67
.L70:
	ldr	r3, [fp, r9, asl #2]
	str	sl, [r4, #20]
	str	r3, [r4, #16]
	str	r8, [r4, #24]
	mov	r0, r6
	mov	r1, r4
	bl	clkpwr_check_pll_setup
	add	r9, r9, #1
.LBB20:
	cmp	r5, r0
	rsbgt	r3, r0, r5
	rsble	r3, r5, r0
	cmp	r3, r7
	movhi	r3, #0
	movls	r3, #1
	b	.L71
.L84:
.LBE20:
	mov	r3, #0
	mov	r9, r3
.L71:
	cmp	r9, #3
	cmpls	r3, #0
	beq	.L70
	add	sl, sl, #1
	b	.L72
.L85:
	mov	r3, #0
	mov	sl, #1
.L72:
	cmp	sl, #4
	cmpls	r3, #0
	beq	.L84
	add	r8, r8, #1
.L67:
	cmp	r8, #256
	cmpls	r3, #0
	beq	.L85
	subs	fp, r3, #0
	bne	.L73
	str	r3, [r4, #12]
	mov	r8, #1
	ldr	r2, .L89+4
	mov	r9, r7
	b	.L74
.L77:
	ldr	r3, [r2, sl, asl #2]
	str	r7, [r4, #20]
	str	r3, [r4, #16]
	str	r8, [r4, #24]
	mov	r0, r6
	mov	r1, r4
	str	r2, [sp, #0]
	bl	clkpwr_check_pll_setup
	add	sl, sl, #1
.LBB21:
	ldr	r2, [sp, #0]
	cmp	r5, r0
	rsbgt	r3, r0, r5
	rsble	r3, r5, r0
	cmp	r3, r9
	movhi	r3, #0
	movls	r3, #1
	b	.L78
.L86:
.LBE21:
	mov	r3, fp
	mov	sl, #0
.L78:
	cmp	sl, #3
	cmpls	r3, #0
	beq	.L77
	add	r7, r7, #1
	b	.L79
.L87:
	mov	r3, fp
	mov	r7, #1
.L79:
	cmp	r7, #4
	cmpls	r3, #0
	beq	.L86
	add	r8, r8, #1
.L74:
	cmp	r8, #256
	cmpls	r3, #0
	beq	.L87
.L73:
	cmp	r3, #1
	movne	r0, #0
.L80:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L90:
	.align	2
.L89:
	.word	.LANCHOR0-4
	.word	.LANCHOR0
.LFE7:
	.size	clkpwr_find_pll_cfg, .-clkpwr_find_pll_cfg
	.section	.text.clkpwr_pll397_setup,"ax",%progbits
	.align	2
	.global	clkpwr_pll397_setup
	.type	clkpwr_pll397_setup, %function
clkpwr_pll397_setup:
.LFB8:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L94
	sub	sp, sp, #4
.LCFI3:
	ldr	r3, [r3, #72]
	cmp	r0, #0
	bic	r3, r3, #960
	bic	r3, r3, #2
	str	r3, [sp, #0]
	ldrne	r3, [sp, #0]
	orrne	r3, r3, #512
	strne	r3, [sp, #0]
	ldr	r3, [sp, #0]
	cmp	r2, #0
	orr	r1, r1, r3
	str	r1, [sp, #0]
	ldreq	r3, [sp, #0]
	orreq	r3, r3, #2
	streq	r3, [sp, #0]
	ldr	r3, .L94
	ldr	r2, [sp, #0]
	str	r2, [r3, #72]
	add	sp, sp, #4
	bx	lr
.L95:
	.align	2
.L94:
	.word	1073758208
.LFE8:
	.size	clkpwr_pll397_setup, .-clkpwr_pll397_setup
	.section	.text.clkpwr_hclkpll_direct_setup,"ax",%progbits
	.align	2
	.global	clkpwr_hclkpll_direct_setup
	.type	clkpwr_hclkpll_direct_setup, %function
clkpwr_hclkpll_direct_setup:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L97
	str	r0, [r3, #88]
	bx	lr
.L98:
	.align	2
.L97:
	.word	1073758208
.LFE11:
	.size	clkpwr_hclkpll_direct_setup, .-clkpwr_hclkpll_direct_setup
	.section	.text.clkpwr_pll_dis_en,"ax",%progbits
	.align	2
	.global	clkpwr_pll_dis_en
	.type	clkpwr_pll_dis_en, %function
clkpwr_pll_dis_en:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	beq	.L102
	bcc	.L101
	cmp	r0, #2
	bxne	lr
	b	.L110
.L101:
	ldr	r3, .L111
	cmp	r1, #0
	ldr	r2, [r3, #72]
	orreq	r2, r2, #2
	bicne	r2, r2, #2
	str	r2, [r3, #72]
	bx	lr
.L102:
	ldr	r3, .L111
	cmp	r1, #0
	ldr	r2, [r3, #88]
	orrne	r2, r2, #65536
	biceq	r2, r2, #65536
	str	r2, [r3, #88]
	bx	lr
.L110:
	ldr	r3, .L111
	cmp	r1, #0
	ldr	r2, [r3, #100]
	orrne	r2, r2, #65536
	biceq	r2, r2, #65536
	str	r2, [r3, #100]
	bx	lr
.L112:
	.align	2
.L111:
	.word	1073758208
.LFE12:
	.size	clkpwr_pll_dis_en, .-clkpwr_pll_dis_en
	.section	.text.clkpwr_is_pll_locked,"ax",%progbits
	.align	2
	.global	clkpwr_is_pll_locked
	.type	clkpwr_is_pll_locked, %function
clkpwr_is_pll_locked:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	beq	.L116
	bcc	.L115
	cmp	r0, #2
	movne	r0, #0
	bxne	lr
	b	.L123
.L115:
	ldr	r3, .L124
	ldr	r0, [r3, #72]
	and	r0, r0, #1
	bx	lr
.L116:
	ldr	r3, .L124
	ldr	r2, [r3, #88]
	tst	r2, #1
	ldreq	r3, [r3, #88]
	beq	.L122
	bx	lr
.L123:
	ldr	r3, .L124
	ldr	r2, [r3, #100]
	tst	r2, #1
	bne	.L120
	ldr	r3, [r3, #100]
.L122:
	tst	r3, #65536
	movne	r0, #0
	moveq	r0, #1
	bx	lr
.L120:
	mov	r0, #1
	bx	lr
.L125:
	.align	2
.L124:
	.word	1073758208
.LFE13:
	.size	clkpwr_is_pll_locked, .-clkpwr_is_pll_locked
	.section	.text.clkpwr_mainosc_setup,"ax",%progbits
	.align	2
	.global	clkpwr_mainosc_setup
	.type	clkpwr_mainosc_setup, %function
clkpwr_mainosc_setup:
.LFB14:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L128
	sub	sp, sp, #4
.LCFI4:
	ldr	r3, [r2, #76]
	and	r0, r0, #127
	bic	r3, r3, #508
	bic	r3, r3, #3
	str	r3, [sp, #0]
	ldr	r3, [sp, #0]
	cmp	r1, #0
	orr	r3, r3, r0, asl #2
	str	r3, [sp, #0]
	ldreq	r3, [sp, #0]
	orreq	r3, r3, #1
	streq	r3, [sp, #0]
	ldr	r3, [sp, #0]
	str	r3, [r2, #76]
	add	sp, sp, #4
	bx	lr
.L129:
	.align	2
.L128:
	.word	1073758208
.LFE14:
	.size	clkpwr_mainosc_setup, .-clkpwr_mainosc_setup
	.section	.text.clkpwr_sysclk_setup,"ax",%progbits
	.align	2
	.global	clkpwr_sysclk_setup
	.type	clkpwr_sysclk_setup, %function
clkpwr_sysclk_setup:
.LFB15:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L133
	sub	sp, sp, #4
.LCFI5:
	ldr	r2, [r3, #80]
	mov	r1, r1, asl #22
	bic	r2, r2, #4080
	bic	r2, r2, #14
	str	r2, [sp, #0]
	ldr	r2, [sp, #0]
	cmp	r0, #1
	orr	r2, r2, r1, lsr #20
	str	r2, [sp, #0]
	ldreq	r2, [sp, #0]
	orreq	r2, r2, #2
	streq	r2, [sp, #0]
	ldr	r2, [sp, #0]
	str	r2, [r3, #80]
	add	sp, sp, #4
	bx	lr
.L134:
	.align	2
.L133:
	.word	1073758208
.LFE15:
	.size	clkpwr_sysclk_setup, .-clkpwr_sysclk_setup
	.section	.text.clkpwr_get_osc,"ax",%progbits
	.align	2
	.global	clkpwr_get_osc
	.type	clkpwr_get_osc, %function
clkpwr_get_osc:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L136
	ldr	r0, [r3, #80]
	and	r0, r0, #1
	bx	lr
.L137:
	.align	2
.L136:
	.word	1073758208
.LFE16:
	.size	clkpwr_get_osc, .-clkpwr_get_osc
	.section	.text.clkpwr_set_hclk_divs,"ax",%progbits
	.align	2
	.global	clkpwr_set_hclk_divs
	.type	clkpwr_set_hclk_divs, %function
clkpwr_set_hclk_divs:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	r2, r2, #1
	cmp	r2, #1
	ldrls	r3, .L141
	sub	r1, r1, #1
	ldrlsb	r3, [r3, r2]	@ zero_extendqisi2
	movhi	r3, #2
	and	r1, r1, #31
	ldr	r2, .L141+4
	orr	r0, r0, r1, asl #2
	and	r3, r3, #3
	orr	r3, r0, r3
	str	r3, [r2, #64]
	bx	lr
.L142:
	.align	2
.L141:
	.word	.LANCHOR2
	.word	1073758208
.LFE17:
	.size	clkpwr_set_hclk_divs, .-clkpwr_set_hclk_divs
	.global	__udivsi3
	.section	.text.clkpwr_get_base_clock_rate,"ax",%progbits
	.align	2
	.global	clkpwr_get_base_clock_rate
	.type	clkpwr_get_base_clock_rate, %function
clkpwr_get_base_clock_rate:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L167
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI6:
	ldr	r2, [r3, #80]
	ldr	r4, .L167+4
	tst	r2, #2
	ldr	r2, .L167+8
	mov	r8, r0
	movne	r4, r2
	ldr	r2, [r3, #104]
	tst	r2, #2
	ldr	r2, [r3, #64]
	beq	.L145
	tst	r2, #128
	movne	r7, #1
	bne	.L146
	ldr	r7, [r3, #64]
	ands	r7, r7, #256
	movne	r7, #2
	b	.L146
.L145:
	ldr	r3, .L167+12
	and	r2, r2, #3
	ldr	r7, [r3, r2, asl #2]
	sub	r7, r7, #1
.L146:
	ldr	r6, .L167
.LBB22:
	mov	r0, r4
.LBE22:
	ldr	r3, [r6, #68]
	tst	r3, #4
	moveq	r5, r4
	moveq	sl, r4
	beq	.L147
.LBB23:
	ldr	r1, [r6, #88]
	bl	clkpwr_pll_rate_from_val
.LBE23:
	ldr	r1, [r6, #64]
	mov	r1, r1, lsr #2
	and	r1, r1, #31
	add	r1, r1, #1
.LBB24:
	mov	sl, r0
.LBE24:
	bl	__udivsi3
	ldr	r2, [r6, #64]
	ldr	r3, .L167+12
	and	r2, r2, #3
	ldr	r1, [r3, r2, asl #2]
	mov	r5, r0
	mov	r0, sl
	bl	__udivsi3
.L147:
	ldr	r3, .L167
	ldr	r3, [r3, #68]
	tst	r3, #1024
	movne	r2, r5
	movne	r3, r5
	moveq	r2, sl
	moveq	r3, r0
	cmp	r8, #9
	ldrls	pc, [pc, r8, asl #2]
	b	.L149
.L160:
	.word	.L150
	.word	.L166
	.word	.L152
	.word	.L153
	.word	.L159
	.word	.L155
	.word	.L149
	.word	.L149
	.word	.L158
	.word	.L159
.L150:
	ldr	r0, .L167+4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L152:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L153:
	mov	r0, r2
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L155:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L158:
	mov	r0, sl
	add	r1, r7, #1
	bl	__udivsi3
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L159:
	mov	r0, r3
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L149:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L166:
	mov	r0, #32768
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L168:
	.align	2
.L167:
	.word	1073758208
	.word	13000000
	.word	13008896
	.word	.LANCHOR3
.LFE18:
	.size	clkpwr_get_base_clock_rate, .-clkpwr_get_base_clock_rate
	.section	.text.clkpwr_usbclkpll_setup,"ax",%progbits
	.align	2
	.global	clkpwr_usbclkpll_setup
	.type	clkpwr_usbclkpll_setup, %function
clkpwr_usbclkpll_setup:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #0]
	ldr	r2, [r0, #4]
	cmp	r3, #0
	movne	r3, #65536
	moveq	r3, #0
	cmp	r2, #0
	ldr	r2, [r0, #8]
	orrne	r3, r3, #32768
	cmp	r2, #0
	ldr	r2, [r0, #12]
	orrne	r3, r3, #16384
	cmp	r2, #0
	ldr	r2, [r0, #16]
	stmfd	sp!, {r4, lr}
.LCFI7:
	sub	r2, r2, #1
	mov	r4, r0
	orrne	r3, r3, #8192
	cmp	r2, #7
	ldrls	pc, [pc, r2, asl #2]
	b	.L174
.L179:
	.word	.L175
	.word	.L181
	.word	.L174
	.word	.L177
	.word	.L174
	.word	.L174
	.word	.L174
	.word	.L178
.L175:
	mov	r2, #0
	b	.L176
.L177:
	mov	r2, #2
	b	.L176
.L178:
	mov	r2, #3
	b	.L176
.L181:
	mov	r2, #1
.L176:
	ldr	r1, [r4, #24]
	ldr	r0, [r4, #20]
	sub	r1, r1, #1
	sub	r0, r0, #1
	and	r1, r1, #255
	and	r0, r0, #3
	mov	r1, r1, asl #1
	orr	r1, r1, r0, asl #9
	orr	r3, r1, r3
	ldr	r1, .L182
	orr	r2, r3, r2, asl #11
	str	r2, [r1, #100]
	mov	r0, #2
	bl	clkpwr_get_base_clock_rate
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	clkpwr_check_pll_setup
.L174:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L183:
	.align	2
.L182:
	.word	1073758208
.LFE10:
	.size	clkpwr_usbclkpll_setup, .-clkpwr_usbclkpll_setup
	.section	.text.clkpwr_hclkpll_setup,"ax",%progbits
	.align	2
	.global	clkpwr_hclkpll_setup
	.type	clkpwr_hclkpll_setup, %function
clkpwr_hclkpll_setup:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #0]
	ldr	r2, [r0, #4]
	cmp	r3, #0
	movne	r3, #65536
	moveq	r3, #0
	cmp	r2, #0
	ldr	r2, [r0, #8]
	orrne	r3, r3, #32768
	cmp	r2, #0
	ldr	r2, [r0, #12]
	orrne	r3, r3, #16384
	cmp	r2, #0
	ldr	r2, [r0, #16]
	stmfd	sp!, {r4, lr}
.LCFI8:
	sub	r2, r2, #1
	mov	r4, r0
	orrne	r3, r3, #8192
	cmp	r2, #7
	ldrls	pc, [pc, r2, asl #2]
	b	.L189
.L194:
	.word	.L190
	.word	.L196
	.word	.L189
	.word	.L192
	.word	.L189
	.word	.L189
	.word	.L189
	.word	.L193
.L190:
	mov	r2, #0
	b	.L191
.L192:
	mov	r2, #2
	b	.L191
.L193:
	mov	r2, #3
	b	.L191
.L196:
	mov	r2, #1
.L191:
	ldr	r1, [r4, #24]
	ldr	r0, [r4, #20]
	sub	r1, r1, #1
	sub	r0, r0, #1
	and	r1, r1, #255
	and	r0, r0, #3
	mov	r1, r1, asl #1
	orr	r1, r1, r0, asl #9
	orr	r3, r1, r3
	ldr	r1, .L197
	orr	r2, r3, r2, asl #11
	str	r2, [r1, #88]
	mov	r0, #2
	bl	clkpwr_get_base_clock_rate
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	clkpwr_check_pll_setup
.L189:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L198:
	.align	2
.L197:
	.word	1073758208
.LFE9:
	.size	clkpwr_hclkpll_setup, .-clkpwr_hclkpll_setup
	.section	.text.clkpwr_force_arm_hclk_to_pclk,"ax",%progbits
	.align	2
	.global	clkpwr_force_arm_hclk_to_pclk
	.type	clkpwr_force_arm_hclk_to_pclk, %function
clkpwr_force_arm_hclk_to_pclk:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L201
	cmp	r0, #0
	ldr	r3, [r2, #68]
	bic	r3, r3, #1024
	orrne	r3, r3, #1024
	str	r3, [r2, #68]
	bx	lr
.L202:
	.align	2
.L201:
	.word	1073758208
.LFE19:
	.size	clkpwr_force_arm_hclk_to_pclk, .-clkpwr_force_arm_hclk_to_pclk
	.section	.text.clkpwr_set_mode,"ax",%progbits
	.align	2
	.global	clkpwr_set_mode
	.type	clkpwr_set_mode, %function
clkpwr_set_mode:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	beq	.L206
	bcc	.L205
	cmp	r0, #2
	bxne	lr
	b	.L209
.L205:
	ldr	r3, .L210
	ldr	r2, [r3, #68]
	orr	r2, r2, #4
	b	.L208
.L206:
	ldr	r3, .L210
	ldr	r2, [r3, #68]
	bic	r2, r2, #4
	b	.L208
.L209:
	ldr	r3, .L210
	ldr	r2, [r3, #68]
	orr	r2, r2, #1
.L208:
	str	r2, [r3, #68]
	bx	lr
.L211:
	.align	2
.L210:
	.word	1073758208
.LFE20:
	.size	clkpwr_set_mode, .-clkpwr_set_mode
	.section	.text.clkpwr_clk_en_dis,"ax",%progbits
	.align	2
	.global	clkpwr_clk_en_dis
	.type	clkpwr_clk_en_dis, %function
clkpwr_clk_en_dis:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r0, r0, #1
	str	lr, [sp, #-4]!
.LCFI9:
	mov	r2, r1
	cmp	r0, #32
	ldrls	pc, [pc, r0, asl #2]
	b	.L212
.L247:
	.word	.L214
	.word	.L215
	.word	.L216
	.word	.L217
	.word	.L218
	.word	.L219
	.word	.L220
	.word	.L221
	.word	.L222
	.word	.L223
	.word	.L224
	.word	.L225
	.word	.L226
	.word	.L227
	.word	.L228
	.word	.L229
	.word	.L230
	.word	.L231
	.word	.L232
	.word	.L233
	.word	.L234
	.word	.L235
	.word	.L236
	.word	.L237
	.word	.L238
	.word	.L239
	.word	.L240
	.word	.L241
	.word	.L242
	.word	.L243
	.word	.L244
	.word	.L245
	.word	.L246
.L214:
	ldr	r0, .L256
	b	.L249
.L215:
	ldr	r0, .L256+4
	b	.L253
.L216:
	ldr	r0, .L256+4
	b	.L250
.L217:
	ldr	r0, .L256+8
.L253:
	mov	r1, #2
	b	.L248
.L218:
	ldr	r0, .L256+8
	b	.L250
.L219:
	ldr	r0, .L256+12
	mov	r1, #32
	bl	clkpwr_mask_and_set
	ldr	r3, .L256+16
	ldr	r2, [r3, #128]
	tst	r2, #15
	ldreq	r2, [r3, #128]
	orreq	r2, r2, #1
	streq	r2, [r3, #128]
	ldr	pc, [sp], #4
.L220:
	ldr	r0, .L256+20
	b	.L251
.L221:
	ldr	r0, .L256+20
	b	.L253
.L222:
	ldr	r0, .L256+20
	b	.L250
.L223:
	ldr	r0, .L256+24
	b	.L253
.L224:
	ldr	r0, .L256+24
	b	.L250
.L225:
	ldr	r0, .L256+28
	b	.L250
.L226:
	ldr	r0, .L256+32
	b	.L250
.L227:
	ldr	r0, .L256+36
	b	.L251
.L228:
	ldr	r0, .L256+36
	b	.L250
.L229:
	ldr	r0, .L256+40
	b	.L253
.L230:
	ldr	r0, .L256+40
	b	.L250
.L231:
	ldr	r0, .L256+44
.L249:
	mov	r1, #32
	b	.L248
.L232:
	ldr	r0, .L256+44
	b	.L254
.L233:
	ldr	r0, .L256+44
	b	.L255
.L234:
	ldr	r0, .L256+44
	b	.L251
.L235:
	ldr	r0, .L256+44
	b	.L253
.L236:
	ldr	r0, .L256+44
	b	.L250
.L237:
	ldr	r0, .L256+48
.L254:
	mov	r1, #16
	b	.L248
.L238:
	ldr	r0, .L256+48
	b	.L250
.L239:
	ldr	r0, .L256+52
	b	.L250
.L240:
	ldr	r0, .L256+52
	b	.L253
.L241:
	ldr	r0, .L256+56
.L255:
	mov	r1, #8
	b	.L248
.L242:
	ldr	r0, .L256+56
.L251:
	mov	r1, #4
	b	.L248
.L243:
	ldr	r0, .L256+56
	b	.L253
.L244:
	ldr	r0, .L256+56
	b	.L250
.L245:
	ldr	r0, .L256+60
.L250:
	mov	r1, #1
	b	.L248
.L246:
	ldr	r0, .L256+64
	rsbs	r2, r2, #1
	mov	r1, #1
	movcc	r2, #0
.L248:
	ldr	lr, [sp], #4
	b	clkpwr_mask_and_set
.L212:
	ldr	pc, [sp], #4
.L257:
	.align	2
.L256:
	.word	1073758292
	.word	1073758328
	.word	1073758332
	.word	1073758336
	.word	1073758208
	.word	1073758352
	.word	1073758380
	.word	1073758384
	.word	1073758388
	.word	1073758392
	.word	1073758396
	.word	1073758400
	.word	1073758404
	.word	1073758408
	.word	1073758436
	.word	1073758440
	.word	1073758312
.LFE21:
	.size	clkpwr_clk_en_dis, .-clkpwr_clk_en_dis
	.section	.text.clkpwr_autoclk_en_dis,"ax",%progbits
	.align	2
	.global	clkpwr_autoclk_en_dis
	.type	clkpwr_autoclk_en_dis, %function
clkpwr_autoclk_en_dis:
.LFB22:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	sub	sp, sp, #4
.LCFI10:
	beq	.L261
	bcc	.L260
	cmp	r0, #2
	bne	.L258
	b	.L267
.L260:
	ldr	r3, .L268
	cmp	r1, #0
	ldr	r2, [r3, #236]
	bic	r2, r2, #64
	str	r2, [sp, #0]
	ldrne	r2, [sp, #0]
	orrne	r2, r2, #64
	bne	.L266
	b	.L265
.L261:
	ldr	r3, .L268
	cmp	r1, #0
	ldr	r2, [r3, #236]
	bic	r2, r2, #2
	str	r2, [sp, #0]
	ldrne	r2, [sp, #0]
	orrne	r2, r2, #2
	bne	.L266
	b	.L265
.L267:
	ldr	r3, .L268
	cmp	r1, #0
	ldr	r2, [r3, #236]
	bic	r2, r2, #1
	str	r2, [sp, #0]
	beq	.L265
	ldr	r2, [sp, #0]
	orr	r2, r2, #1
.L266:
	str	r2, [sp, #0]
.L265:
	ldr	r2, [sp, #0]
	str	r2, [r3, #236]
.L258:
	add	sp, sp, #4
	bx	lr
.L269:
	.align	2
.L268:
	.word	1073758208
.LFE22:
	.size	clkpwr_autoclk_en_dis, .-clkpwr_autoclk_en_dis
	.section	.text.clkpwr_get_clock_rate,"ax",%progbits
	.align	2
	.global	clkpwr_get_clock_rate
	.type	clkpwr_get_clock_rate, %function
clkpwr_get_clock_rate:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI11:
	mov	r4, r0
	bl	clkpwr_get_base_clock
	cmp	r0, #10
	moveq	r1, #1
	moveq	r0, #0
	beq	.L271
	sub	r4, r4, #1
	bl	clkpwr_get_base_clock_rate
	cmp	r4, #14
	ldrls	pc, [pc, r4, asl #2]
	b	.L279
.L277:
	.word	.L272
	.word	.L279
	.word	.L279
	.word	.L279
	.word	.L279
	.word	.L273
	.word	.L279
	.word	.L279
	.word	.L279
	.word	.L279
	.word	.L279
	.word	.L279
	.word	.L274
	.word	.L276
	.word	.L276
.L273:
	ldr	r3, .L285
	ldr	r3, [r3, #128]
	b	.L284
.L272:
	ldr	r3, .L285
	ldr	r1, [r3, #84]
	and	r1, r1, #31
	add	r1, r1, #1
	b	.L271
.L274:
	ldr	r3, .L285
	mov	r1, #1
	ldr	r3, [r3, #96]
	tst	r3, #256
	movne	r0, #0
	b	.L271
.L276:
	ldr	r3, .L285
	ldr	r3, [r3, #184]
.L284:
	and	r3, r3, #15
	cmp	r3, #0
	movne	r1, r3
	moveq	r1, #1
	moveq	r0, r3
	b	.L271
.L279:
	mov	r1, #1
.L271:
	bl	__udivsi3
	ldmfd	sp!, {r4, pc}
.L286:
	.align	2
.L285:
	.word	1073758208
.LFE23:
	.size	clkpwr_get_clock_rate, .-clkpwr_get_clock_rate
	.section	.text.clkpwr_wk_event_en_dis,"ax",%progbits
	.align	2
	.global	clkpwr_wk_event_en_dis
	.type	clkpwr_wk_event_en_dis, %function
clkpwr_wk_event_en_dis:
.LFB24:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI12:
	mov	r4, #0
	add	r2, sp, #8
	mov	r5, r1
	str	r4, [r2, #-4]!
	mov	r1, r4
	bl	clkpwr_get_event_field
	cmp	r0, r4
	beq	.L287
	ldr	r3, [r4, #0]
	cmp	r5, r4
	ldr	r2, [r3, #0]
	mov	r1, #1
	str	r2, [sp, #0]
	ldr	r0, [sp, #0]
	ldr	r2, [sp, #4]
	orrne	r2, r0, r1, asl r2
	biceq	r2, r0, r1, asl r2
	str	r2, [sp, #0]
	ldr	r2, [sp, #0]
	str	r2, [r3, #0]
.L287:
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.LFE24:
	.size	clkpwr_wk_event_en_dis, .-clkpwr_wk_event_en_dis
	.section	.text.clkpwr_is_raw_event_active,"ax",%progbits
	.align	2
	.global	clkpwr_is_raw_event_active
	.type	clkpwr_is_raw_event_active, %function
clkpwr_is_raw_event_active:
.LFB25:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI13:
	mov	r4, #0
	add	r2, sp, #4
	str	r4, [r2, #-4]!
	mov	r1, r4
	mov	r2, sp
	bl	clkpwr_get_event_field
	cmp	r0, #0
	ldrne	r3, [r4, #8]
	ldrne	r0, [r3, #0]
	ldrne	r3, [sp, #0]
	movne	r0, r0, lsr r3
	andne	r0, r0, #1
	ldmfd	sp!, {r3, r4, pc}
.LFE25:
	.size	clkpwr_is_raw_event_active, .-clkpwr_is_raw_event_active
	.section	.text.clkpwr_is_msk_event_active,"ax",%progbits
	.align	2
	.global	clkpwr_is_msk_event_active
	.type	clkpwr_is_msk_event_active, %function
clkpwr_is_msk_event_active:
.LFB26:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI14:
	mov	r4, #0
	add	r2, sp, #4
	str	r4, [r2, #-4]!
	mov	r1, r4
	mov	r2, sp
	bl	clkpwr_get_event_field
	cmp	r0, #0
	ldreq	r3, [r0, #4]
	movne	r0, r4
	ldreq	r0, [r3, #0]
	ldreq	r3, [sp, #0]
	moveq	r0, r0, lsr r3
	andeq	r0, r0, #1
	ldmfd	sp!, {r3, r4, pc}
.LFE26:
	.size	clkpwr_is_msk_event_active, .-clkpwr_is_msk_event_active
	.section	.text.clkpwr_clear_event,"ax",%progbits
	.align	2
	.global	clkpwr_clear_event
	.type	clkpwr_clear_event, %function
clkpwr_clear_event:
.LFB27:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI15:
	mov	r4, #0
	add	r2, sp, #4
	mov	r1, r4
	str	r4, [r2, #-4]!
	mov	r2, sp
	bl	clkpwr_get_event_field
	cmp	r0, r4
	ldrne	r2, [sp, #0]
	ldrne	r3, [r4, #8]
	movne	r1, #1
	movne	r2, r1, asl r2
	strne	r2, [r3, #0]
	ldmfd	sp!, {r3, r4, pc}
.LFE27:
	.size	clkpwr_clear_event, .-clkpwr_clear_event
	.section	.text.clkpwr_set_event_pol,"ax",%progbits
	.align	2
	.global	clkpwr_set_event_pol
	.type	clkpwr_set_event_pol, %function
clkpwr_set_event_pol:
.LFB28:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI16:
	mov	r4, #0
	add	r2, sp, #8
	mov	r5, r1
	str	r4, [r2, #-4]!
	mov	r1, r4
	bl	clkpwr_get_event_field
	cmp	r0, r4
	beq	.L300
	ldr	r3, [r4, #12]
	ldr	r2, [sp, #4]
	ldr	r1, [r3, #0]
	mov	r0, #1
	mov	r2, r0, asl r2
	cmp	r5, r4
	bic	r1, r1, r2
	str	r1, [sp, #0]
	ldrne	r1, [sp, #0]
	orrne	r2, r2, r1
	strne	r2, [sp, #0]
	ldr	r2, [sp, #0]
	str	r2, [r3, #0]
.L300:
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.LFE28:
	.size	clkpwr_set_event_pol, .-clkpwr_set_event_pol
	.section	.text.clkpwr_get_uid,"ax",%progbits
	.align	2
	.global	clkpwr_get_uid
	.type	clkpwr_get_uid, %function
clkpwr_get_uid:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L304
	ldr	r2, [r3, #304]
	str	r2, [r0, #0]
	ldr	r2, [r3, #308]
	str	r2, [r0, #4]
	ldr	r2, [r3, #312]
	str	r2, [r0, #8]
	ldr	r3, [r3, #316]
	str	r3, [r0, #12]
	bx	lr
.L305:
	.align	2
.L304:
	.word	1073758208
.LFE29:
	.size	clkpwr_get_uid, .-clkpwr_get_uid
	.section	.text.clkpwr_set_mux,"ax",%progbits
	.align	2
	.global	clkpwr_set_mux
	.type	clkpwr_set_mux, %function
clkpwr_set_mux:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #7
	ldrls	pc, [pc, r0, asl #2]
	b	.L306
.L314:
	.word	.L308
	.word	.L309
	.word	.L310
	.word	.L311
	.word	.L312
	.word	.L312
	.word	.L313
	.word	.L313
.L308:
	ldr	r3, .L324
	cmp	r1, #0
	ldr	r2, [r3, #68]
	biceq	r2, r2, #2
	orrne	r2, r2, #2
	b	.L322
.L309:
	ldr	r3, .L324
	cmp	r1, #0
	ldr	r2, [r3, #68]
	biceq	r2, r2, #8
	orrne	r2, r2, #8
.L322:
	str	r2, [r3, #68]
	bx	lr
.L310:
	ldr	r3, .L324
	cmp	r1, #0
	ldr	r2, [r3, #164]
	biceq	r2, r2, #16
	orrne	r2, r2, #16
	b	.L323
.L311:
	ldr	r3, .L324
	cmp	r1, #0
	ldr	r2, [r3, #164]
	biceq	r2, r2, #1
	orrne	r2, r2, #1
.L323:
	str	r2, [r3, #164]
	bx	lr
.L312:
	ldr	r3, .L324
	cmp	r1, #0
	ldr	r2, [r3, #196]
	biceq	r2, r2, #32
	orrne	r2, r2, #32
	b	.L321
.L313:
	ldr	r3, .L324
	cmp	r1, #0
	ldr	r2, [r3, #196]
	orrne	r2, r2, #2
	strne	r2, [r3, #196]
	bne	.L306
	bic	r2, r2, #2
.L321:
	str	r2, [r3, #196]
	bx	lr
.L306:
	bx	lr
.L325:
	.align	2
.L324:
	.word	1073758208
.LFE30:
	.size	clkpwr_set_mux, .-clkpwr_set_mux
	.section	.text.clkpwr_set_mux_state,"ax",%progbits
	.align	2
	.global	clkpwr_set_mux_state
	.type	clkpwr_set_mux_state, %function
clkpwr_set_mux_state:
.LFB31:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #4
.LCFI17:
	cmp	r0, #7
	ldrls	pc, [pc, r0, asl #2]
	b	.L326
.L336:
	.word	.L328
	.word	.L329
	.word	.L330
	.word	.L331
	.word	.L332
	.word	.L333
	.word	.L334
	.word	.L335
.L328:
	ldr	r3, .L346
	cmp	r1, #0
	ldr	r2, [r3, #68]
	biceq	r2, r2, #32
	orrne	r2, r2, #32
.L344:
	str	r2, [r3, #68]
	b	.L326
.L329:
	ldr	r3, .L346
	cmp	r1, #0
	ldr	r2, [r3, #68]
	biceq	r2, r2, #16
	orrne	r2, r2, #16
	b	.L344
.L330:
	ldr	r3, .L346
	ldr	r2, [r3, #164]
	bic	r2, r2, #96
	b	.L345
.L331:
	ldr	r3, .L346
	ldr	r2, [r3, #164]
	bic	r2, r2, #14
.L345:
	orr	r1, r1, r2
	str	r1, [sp, #0]
	ldr	r2, [sp, #0]
	str	r2, [r3, #164]
	b	.L326
.L332:
	ldr	r3, .L346
	cmp	r1, #0
	ldr	r2, [r3, #196]
	biceq	r2, r2, #128
	orrne	r2, r2, #128
	b	.L343
.L333:
	ldr	r3, .L346
	cmp	r1, #0
	ldr	r2, [r3, #196]
	biceq	r2, r2, #64
	orrne	r2, r2, #64
	b	.L343
.L334:
	ldr	r3, .L346
	cmp	r1, #0
	ldr	r2, [r3, #196]
	biceq	r2, r2, #8
	orrne	r2, r2, #8
	b	.L343
.L335:
	ldr	r3, .L346
	cmp	r1, #0
	ldr	r2, [r3, #196]
	biceq	r2, r2, #4
	orrne	r2, r2, #4
.L343:
	str	r2, [r3, #196]
.L326:
	add	sp, sp, #4
	bx	lr
.L347:
	.align	2
.L346:
	.word	1073758208
.LFE31:
	.size	clkpwr_set_mux_state, .-clkpwr_set_mux_state
	.section	.text.clkpwr_setup_mcard_ctrlr,"ax",%progbits
	.align	2
	.global	clkpwr_setup_mcard_ctrlr
	.type	clkpwr_setup_mcard_ctrlr, %function
clkpwr_setup_mcard_ctrlr:
.LFB32:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	ip, .L353
	cmp	r0, #0
	ldr	ip, [ip, #128]
	and	ip, ip, #32
	orrne	ip, ip, #512
	cmp	r1, #0
	orreq	ip, ip, #256
	cmp	r2, #0
	orreq	ip, ip, #128
	cmp	r3, #0
	ldr	r3, [sp, #0]
	orreq	ip, ip, #64
	and	r3, r3, #15
	orr	ip, ip, r3
	ldr	r3, .L353
	str	ip, [r3, #128]
	bx	lr
.L354:
	.align	2
.L353:
	.word	1073758208
.LFE32:
	.size	clkpwr_setup_mcard_ctrlr, .-clkpwr_setup_mcard_ctrlr
	.section	.text.clkpwr_set_i2c_driver,"ax",%progbits
	.align	2
	.global	clkpwr_set_i2c_driver
	.type	clkpwr_set_i2c_driver, %function
clkpwr_set_i2c_driver:
.LFB33:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	sub	sp, sp, #4
.LCFI18:
	bne	.L356
	ldr	r3, .L361
	cmp	r1, #0
	ldr	r2, [r3, #172]
	bic	r2, r2, #4
	str	r2, [sp, #0]
	ldrne	r2, [sp, #0]
	orrne	r2, r2, #4
	bne	.L360
	b	.L359
.L356:
	cmp	r0, #2
	bne	.L355
	ldr	r3, .L361
	cmp	r1, #0
	ldr	r2, [r3, #172]
	bic	r2, r2, #8
	str	r2, [sp, #0]
	beq	.L359
	ldr	r2, [sp, #0]
	orr	r2, r2, #8
.L360:
	str	r2, [sp, #0]
.L359:
	ldr	r2, [sp, #0]
	str	r2, [r3, #172]
.L355:
	add	sp, sp, #4
	bx	lr
.L362:
	.align	2
.L361:
	.word	1073758208
.LFE33:
	.size	clkpwr_set_i2c_driver, .-clkpwr_set_i2c_driver
	.section	.text.clkpwr_setup_pwm,"ax",%progbits
	.align	2
	.global	clkpwr_setup_pwm
	.type	clkpwr_setup_pwm, %function
clkpwr_setup_pwm:
.LFB34:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	sub	sp, sp, #4
.LCFI19:
	bne	.L364
	ldr	r3, .L369
	cmp	r1, #0
	ldr	r3, [r3, #184]
	mov	r2, r2, asl #4
	bic	r3, r3, #242
	str	r3, [sp, #0]
	ldrne	r3, [sp, #0]
	and	r2, r2, #255
	orrne	r3, r3, #2
	strne	r3, [sp, #0]
	ldr	r3, [sp, #0]
	orr	r2, r2, r3
	b	.L368
.L364:
	cmp	r0, #2
	bne	.L363
	ldr	r3, .L369
	cmp	r1, #0
	ldr	r3, [r3, #184]
	and	r2, r2, #15
	bic	r3, r3, #3840
	bic	r3, r3, #8
	str	r3, [sp, #0]
	ldrne	r3, [sp, #0]
	orrne	r3, r3, #8
	strne	r3, [sp, #0]
	ldr	r3, [sp, #0]
	orr	r2, r3, r2, asl #8
.L368:
	str	r2, [sp, #0]
	ldr	r2, [sp, #0]
	ldr	r3, .L369
	str	r2, [r3, #184]
.L363:
	add	sp, sp, #4
	bx	lr
.L370:
	.align	2
.L369:
	.word	1073758208
.LFE34:
	.size	clkpwr_setup_pwm, .-clkpwr_setup_pwm
	.section	.text.clkpwr_setup_nand_ctrlr,"ax",%progbits
	.align	2
	.global	clkpwr_setup_nand_ctrlr
	.type	clkpwr_setup_nand_ctrlr, %function
clkpwr_setup_nand_ctrlr:
.LFB35:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	moveq	r0, #32
	movne	r0, #4
	ldr	r3, .L376
	cmp	r1, #0
	orrne	r0, r0, #16
	cmp	r2, #0
	orrne	r0, r0, #8
	str	r0, [r3, #200]
	bx	lr
.L377:
	.align	2
.L376:
	.word	1073758208
.LFE35:
	.size	clkpwr_setup_nand_ctrlr, .-clkpwr_setup_nand_ctrlr
	.section	.text.clkpwr_setup_adc_ts,"ax",%progbits
	.align	2
	.global	clkpwr_setup_adc_ts
	.type	clkpwr_setup_adc_ts, %function
clkpwr_setup_adc_ts:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L380
	and	r1, r1, #255
	cmp	r0, #0
	orrne	r1, r1, #256
	str	r1, [r3, #96]
	bx	lr
.L381:
	.align	2
.L380:
	.word	1073758208
.LFE36:
	.size	clkpwr_setup_adc_ts, .-clkpwr_setup_adc_ts
	.section	.text.clkpwr_setup_ssp,"ax",%progbits
	.align	2
	.global	clkpwr_setup_ssp
	.type	clkpwr_setup_ssp, %function
clkpwr_setup_ssp:
.LFB37:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	ip, .L387
	sub	sp, sp, #4
.LCFI20:
	ldr	ip, [ip, #120]
	cmp	r0, #0
	bic	ip, ip, #60
	str	ip, [sp, #0]
	ldrne	r0, [sp, #0]
	orrne	r0, r0, #32
	strne	r0, [sp, #0]
	cmp	r1, #0
	ldrne	r1, [sp, #0]
	orrne	r1, r1, #16
	strne	r1, [sp, #0]
	cmp	r2, #0
	ldrne	r2, [sp, #0]
	orrne	r2, r2, #8
	strne	r2, [sp, #0]
	cmp	r3, #0
	ldrne	r3, [sp, #0]
	orrne	r3, r3, #4
	strne	r3, [sp, #0]
	ldr	r3, .L387
	ldr	r2, [sp, #0]
	str	r2, [r3, #120]
	add	sp, sp, #4
	bx	lr
.L388:
	.align	2
.L387:
	.word	1073758208
.LFE37:
	.size	clkpwr_setup_ssp, .-clkpwr_setup_ssp
	.section	.text.clkpwr_setup_i2s,"ax",%progbits
	.align	2
	.global	clkpwr_setup_i2s
	.type	clkpwr_setup_i2s, %function
clkpwr_setup_i2s:
.LFB38:
	@ args = 4, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	ip, .L395
	sub	sp, sp, #4
.LCFI21:
	ldr	ip, [ip, #124]
	cmp	r0, #0
	bic	ip, ip, #124
	str	ip, [sp, #0]
	ldrne	r0, [sp, #0]
	orrne	r0, r0, #64
	strne	r0, [sp, #0]
	cmp	r1, #0
	ldrne	r1, [sp, #0]
	orrne	r1, r1, #32
	strne	r1, [sp, #0]
	cmp	r2, #0
	ldrne	r2, [sp, #0]
	orrne	r2, r2, #16
	strne	r2, [sp, #0]
	ldr	r2, [sp, #4]
	cmp	r2, #0
	ldrne	r2, [sp, #0]
	orrne	r2, r2, #8
	strne	r2, [sp, #0]
	cmp	r3, #0
	ldrne	r3, [sp, #0]
	orrne	r3, r3, #4
	strne	r3, [sp, #0]
	ldr	r3, .L395
	ldr	r2, [sp, #0]
	str	r2, [r3, #124]
	add	sp, sp, #4
	bx	lr
.L396:
	.align	2
.L395:
	.word	1073758208
.LFE38:
	.size	clkpwr_setup_i2s, .-clkpwr_setup_i2s
	.section	.text.clkpwr_setup_lcd,"ax",%progbits
	.align	2
	.global	clkpwr_setup_lcd
	.type	clkpwr_setup_lcd, %function
clkpwr_setup_lcd:
.LFB39:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L411
	sub	sp, sp, #4
.LCFI22:
	ldr	r3, [r3, #84]
	cmp	r0, #192
	bic	r3, r3, #476
	bic	r3, r3, #3
	str	r3, [sp, #0]
	beq	.L406
	bhi	.L407
	cmp	r0, #64
	beq	.L406
	cmp	r0, #128
	beq	.L406
	cmp	r0, #0
	b	.L410
.L407:
	cmp	r0, #320
	beq	.L406
	bhi	.L408
	cmp	r0, #256
	b	.L410
.L408:
	cmp	r0, #384
	beq	.L406
	cmp	r0, #448
.L410:
	bne	.L398
.L406:
	str	r0, [sp, #0]
.L398:
	ldr	r3, [sp, #0]
	sub	r1, r1, #1
	and	r1, r1, #31
	orr	r1, r1, r3
	str	r1, [sp, #0]
	ldr	r2, [sp, #0]
	ldr	r3, .L411
	str	r2, [r3, #84]
	add	sp, sp, #4
	bx	lr
.L412:
	.align	2
.L411:
	.word	1073758208
.LFE39:
	.size	clkpwr_setup_lcd, .-clkpwr_setup_lcd
	.section	.text.clkpwr_select_enet_iface,"ax",%progbits
	.align	2
	.global	clkpwr_select_enet_iface
	.type	clkpwr_select_enet_iface, %function
clkpwr_select_enet_iface:
.LFB40:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L418
	sub	sp, sp, #4
.LCFI23:
	ldr	r2, [r3, #144]
	cmp	r0, #0
	bic	r2, r2, #24
	str	r2, [sp, #0]
	ldr	r2, [sp, #0]
	beq	.L417
.L414:
	cmp	r1, #0
	orreq	r2, r2, #24
	orrne	r2, r2, #8
.L417:
	str	r2, [sp, #0]
	ldr	r2, [sp, #0]
	str	r2, [r3, #144]
	add	sp, sp, #4
	bx	lr
.L419:
	.align	2
.L418:
	.word	1073758208
.LFE40:
	.size	clkpwr_select_enet_iface, .-clkpwr_select_enet_iface
	.section	.text.clkpwr_halt_cpu,"ax",%progbits
	.align	2
	.global	clkpwr_halt_cpu
	.type	clkpwr_halt_cpu, %function
clkpwr_halt_cpu:
.LFB41:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
.LFE41:
	.size	clkpwr_halt_cpu, .-clkpwr_halt_cpu
	.global	clkpwr_base_clk
	.section	.rodata.hclkdivs,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	hclkdivs, %object
	.size	hclkdivs, 16
hclkdivs:
	.word	1
	.word	2
	.word	4
	.word	4
	.section	.rodata.pll_postdivs,"a",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	pll_postdivs, %object
	.size	pll_postdivs, 16
pll_postdivs:
	.word	1
	.word	2
	.word	4
	.word	8
	.section	.data.clkpwr_base_clk,"aw",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	clkpwr_base_clk, %object
	.size	clkpwr_base_clk, 136
clkpwr_base_clk:
	.word	4
	.word	4
	.word	4
	.word	4
	.word	4
	.word	4
	.word	3
	.word	4
	.word	4
	.word	4
	.word	4
	.word	4
	.word	1
	.word	1
	.word	1
	.word	1
	.word	5
	.word	5
	.word	5
	.word	5
	.word	5
	.word	5
	.word	5
	.word	5
	.word	4
	.word	4
	.word	4
	.word	4
	.word	5
	.word	5
	.word	5
	.word	5
	.word	4
	.word	8
	.section	.rodata.CSWTCH.114,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	CSWTCH.114, %object
	.size	CSWTCH.114, 2
CSWTCH.114:
	.byte	0
	.byte	1
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI2-.LFB7
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI3-.LFB8
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI4-.LFB14
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI5-.LFB15
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI6-.LFB18
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI7-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI8-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI9-.LFB21
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI10-.LFB22
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI11-.LFB23
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI12-.LFB24
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI13-.LFB25
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI14-.LFB26
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI15-.LFB27
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI16-.LFB28
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI17-.LFB31
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI18-.LFB33
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI19-.LFB34
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI20-.LFB37
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI21-.LFB38
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI22-.LFB39
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI23-.LFB40
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.align	2
.LEFDE82:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x38d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF42
	.byte	0x1
	.4byte	.LASF43
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x161
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x78
	.byte	0x1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x123
	.byte	0x1
	.uleb128 0x4
	.4byte	0x2b
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x96
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xf3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	0x34
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x13e
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x199
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1e6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x284
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x33f
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x358
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x399
	.4byte	.LFB13
	.4byte	.LFE13
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x3de
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x405
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x42e
	.4byte	.LFB16
	.4byte	.LFE16
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x44e
	.4byte	.LFB17
	.4byte	.LFE17
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x47e
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST6
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x2f7
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST7
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x2af
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x526
	.4byte	.LFB19
	.4byte	.LFE19
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x548
	.4byte	.LFB20
	.4byte	.LFE20
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x576
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST9
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x645
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST10
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x681
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST11
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x6ea
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST12
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x717
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST13
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x73b
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST14
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x75f
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST15
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x783
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST16
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x7ad
	.4byte	.LFB29
	.4byte	.LFE29
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x7ca
	.4byte	.LFB30
	.4byte	.LFE30
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x831
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST17
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x8aa
	.4byte	.LFB32
	.4byte	.LFE32
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x8db
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST18
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x90a
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST19
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x93d
	.4byte	.LFB35
	.4byte	.LFE35
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x96f
	.4byte	.LFB36
	.4byte	.LFE36
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x993
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST20
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x9c8
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST21
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x9ff
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST22
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.2byte	0xa45
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST23
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.2byte	0xa72
	.4byte	.LFB41
	.4byte	.LFE41
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB7
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB8
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB14
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB15
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB18
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB10
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB9
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB21
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB22
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB23
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB24
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB25
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB26
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB27
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB28
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB31
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB33
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB34
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB37
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB38
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB39
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB40
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x164
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"clkpwr_check_pll_setup\000"
.LASF14:
	.ascii	"clkpwr_get_osc\000"
.LASF32:
	.ascii	"clkpwr_setup_mcard_ctrlr\000"
.LASF42:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF31:
	.ascii	"clkpwr_set_mux_state\000"
.LASF40:
	.ascii	"clkpwr_select_enet_iface\000"
.LASF33:
	.ascii	"clkpwr_set_i2c_driver\000"
.LASF11:
	.ascii	"clkpwr_is_pll_locked\000"
.LASF29:
	.ascii	"clkpwr_get_uid\000"
.LASF39:
	.ascii	"clkpwr_setup_lcd\000"
.LASF6:
	.ascii	"clkpwr_get_base_clock\000"
.LASF19:
	.ascii	"clkpwr_force_arm_hclk_to_pclk\000"
.LASF13:
	.ascii	"clkpwr_sysclk_setup\000"
.LASF28:
	.ascii	"clkpwr_set_event_pol\000"
.LASF27:
	.ascii	"clkpwr_clear_event\000"
.LASF23:
	.ascii	"clkpwr_get_clock_rate\000"
.LASF15:
	.ascii	"clkpwr_set_hclk_divs\000"
.LASF12:
	.ascii	"clkpwr_mainosc_setup\000"
.LASF30:
	.ascii	"clkpwr_set_mux\000"
.LASF24:
	.ascii	"clkpwr_wk_event_en_dis\000"
.LASF21:
	.ascii	"clkpwr_clk_en_dis\000"
.LASF1:
	.ascii	"clkpwr_abs\000"
.LASF38:
	.ascii	"clkpwr_setup_i2s\000"
.LASF0:
	.ascii	"clkpwr_get_event_field\000"
.LASF2:
	.ascii	"clkpwr_pll_rate\000"
.LASF22:
	.ascii	"clkpwr_autoclk_en_dis\000"
.LASF5:
	.ascii	"clkpwr_mask_and_set\000"
.LASF9:
	.ascii	"clkpwr_hclkpll_direct_setup\000"
.LASF34:
	.ascii	"clkpwr_setup_pwm\000"
.LASF18:
	.ascii	"clkpwr_hclkpll_setup\000"
.LASF41:
	.ascii	"clkpwr_halt_cpu\000"
.LASF26:
	.ascii	"clkpwr_is_msk_event_active\000"
.LASF25:
	.ascii	"clkpwr_is_raw_event_active\000"
.LASF7:
	.ascii	"clkpwr_find_pll_cfg\000"
.LASF20:
	.ascii	"clkpwr_set_mode\000"
.LASF37:
	.ascii	"clkpwr_setup_ssp\000"
.LASF43:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_clkpwr_driver.c\000"
.LASF10:
	.ascii	"clkpwr_pll_dis_en\000"
.LASF35:
	.ascii	"clkpwr_setup_nand_ctrlr\000"
.LASF16:
	.ascii	"clkpwr_get_base_clock_rate\000"
.LASF8:
	.ascii	"clkpwr_pll397_setup\000"
.LASF36:
	.ascii	"clkpwr_setup_adc_ts\000"
.LASF4:
	.ascii	"clkpwr_pll_rate_from_val\000"
.LASF17:
	.ascii	"clkpwr_usbclkpll_setup\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
