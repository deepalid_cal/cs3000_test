	.file	"e_test_sequential.c"
	.text
.Ltext0:
	.section	.text.TEST_SEQ_set_test_time,"ax",%progbits
	.align	2
	.type	TEST_SEQ_set_test_time, %function
TEST_SEQ_set_test_time:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	cmp	r0, #3600
	str	r0, [r3, #0]
	ldr	r3, .L2+4
	movcs	r0, #0
	movcc	r0, #1
	str	r0, [r3, #0]
	bx	lr
.L3:
	.align	2
.L2:
	.word	GuiVar_TestStationTimeRemaining
	.word	GuiVar_TestStationTimeDisplay
.LFE0:
	.size	TEST_SEQ_set_test_time, .-TEST_SEQ_set_test_time
	.section	.text.TEST_SEQ_put_single_station_on_irri_list,"ax",%progbits
	.align	2
	.type	TEST_SEQ_put_single_station_on_irri_list, %function
TEST_SEQ_put_single_station_on_irri_list:
.LFB9:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L13
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r2, .L13+4
	mov	r1, #400
	sub	sp, sp, #24
.LCFI1:
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L13+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L13+12
	bl	nm_ListGetFirst
	ldr	r5, .L13+16
	subs	r1, r0, #0
	bne	.L5
	ldr	r3, [r5, #0]
	mov	r0, sp
	str	r3, [sp, #4]
	str	r3, [sp, #12]
	mov	r3, #6
	strb	r3, [sp, #21]
	str	r1, [sp, #0]
	str	r1, [sp, #8]
	str	r1, [sp, #16]
	strb	r4, [sp, #20]
	bl	irri_add_to_main_list_for_test_sequential
	ldr	r0, .L13+12
	bl	nm_ListGetFirst
	ldr	r3, [r5, #0]
	mov	r5, #1
	str	r3, [r0, #44]
	ldrb	r3, [r0, #53]	@ zero_extendqisi2
	orr	r3, r3, #1
	strb	r3, [r0, #53]
	b	.L6
.L5:
	ldr	r3, [r5, #0]
	strb	r4, [r1, #40]
	str	r3, [r1, #32]
	str	r3, [r1, #44]
	ldrb	r3, [r1, #53]	@ zero_extendqisi2
	ldr	r0, .L13+12
	orr	r3, r3, #1
	strb	r3, [r1, #53]
	bl	nm_ListGetNext
	subs	r4, r0, #0
	moveq	r5, #1
	beq	.L12
	ldr	r0, .L13+20
	bl	Alert_Message
	mov	r5, #0
	b	.L12
.L10:
	mov	r1, r4
	ldr	r0, .L13+12
	bl	nm_ListGetNext
	mov	r1, r4
	ldr	r2, .L13+4
	mov	r3, #640
	mov	r6, r0
	ldr	r0, .L13+12
	bl	nm_ListRemove_debug
	cmp	r0, #0
	beq	.L9
	ldr	r0, .L13+24
	bl	Alert_Message
.L9:
	mov	r0, r4
	ldr	r1, .L13+4
	ldr	r2, .L13+28
	bl	mem_free_debug
	mov	r4, r6
.L12:
	cmp	r4, #0
	bne	.L10
.L6:
	ldr	r3, .L13
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, pc}
.L14:
	.align	2
.L13:
	.word	irri_irri_recursive_MUTEX
	.word	.LC0
	.word	593
	.word	irri_irri
	.word	GuiVar_TestStationTimeRemaining
	.word	.LC1
	.word	.LC2
	.word	645
.LFE9:
	.size	TEST_SEQ_put_single_station_on_irri_list, .-TEST_SEQ_put_single_station_on_irri_list
	.section	.text.TEST_SEQ_light_control,"ax",%progbits
	.align	2
	.type	TEST_SEQ_light_control, %function
TEST_SEQ_light_control:
.LFB5:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI2:
	mov	r4, r0
	mov	r5, r1
	mov	r0, sp
	bl	EPSON_obtain_latest_time_and_date
	cmp	r5, #0
	mov	r0, r4
	bne	.L16
	bl	IRRI_LIGHTS_request_light_off
	b	.L15
.L16:
	ldr	r2, [sp, #0]
	ldrh	r1, [sp, #4]
	add	r2, r2, #60
	bl	IRRI_LIGHTS_request_light_on
.L15:
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.LFE5:
	.size	TEST_SEQ_light_control, .-TEST_SEQ_light_control
	.section	.text.TEST_SEQ_master_valve_control.isra.1,"ax",%progbits
	.align	2
	.type	TEST_SEQ_master_valve_control.isra.1, %function
TEST_SEQ_master_valve_control.isra.1:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI3:
	ldr	r4, .L19
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L19+4
	mov	r3, #144
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L20:
	.align	2
.L19:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
.LFE16:
	.size	TEST_SEQ_master_valve_control.isra.1, .-TEST_SEQ_master_valve_control.isra.1
	.section	.text.TEST_SEQ_pump_control.isra.2,"ax",%progbits
	.align	2
	.type	TEST_SEQ_pump_control.isra.2, %function
TEST_SEQ_pump_control.isra.2:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI4:
	ldr	r4, .L22
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L22+4
	mov	r3, #187
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L23:
	.align	2
.L22:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
.LFE17:
	.size	TEST_SEQ_pump_control.isra.2, .-TEST_SEQ_pump_control.isra.2
	.section	.text.TEST_SEQ_turn_off_all_stations,"ax",%progbits
	.align	2
	.type	TEST_SEQ_turn_off_all_stations, %function
TEST_SEQ_turn_off_all_stations:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI5:
	mov	r5, r0
	bl	TEST_SEQ_pump_control.isra.2
	bl	TEST_SEQ_master_valve_control.isra.1
	mov	r0, #0
	mov	r1, r0
	bl	TEST_SEQ_light_control
	mov	r0, #1
	mov	r1, #0
	bl	TEST_SEQ_light_control
	mov	r0, #2
	mov	r1, #0
	bl	TEST_SEQ_light_control
	mov	r0, #3
	mov	r1, #0
	bl	TEST_SEQ_light_control
	ldr	r3, .L29
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L29+4
	mov	r3, #516
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L29+8
	bl	nm_ListGetFirst
	mov	r4, r0
	b	.L25
.L27:
	mov	r1, r4
	ldr	r0, .L29+8
	bl	nm_ListGetNext
	mov	r1, r4
	ldr	r2, .L29+4
	mov	r3, #536
	mov	r6, r0
	ldr	r0, .L29+8
	bl	nm_ListRemove_debug
	cmp	r0, #0
	beq	.L26
	ldr	r0, .L29+12
	bl	Alert_Message
.L26:
	mov	r0, r4
	ldr	r1, .L29+4
	ldr	r2, .L29+16
	bl	mem_free_debug
	mov	r4, r6
.L25:
	cmp	r4, #0
	bne	.L27
	ldr	r3, .L29
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L29+20
	mov	r0, r4
	str	r4, [r3, #0]
	ldr	r3, .L29+24
	str	r4, [r3, #0]
	ldr	r3, .L29+28
	str	r4, [r3, #0]
	ldr	r3, .L29+32
	str	r4, [r3, #0]
	bl	Redraw_Screen
	cmp	r5, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r3, .L29+36
	ldr	r0, .L29+40
	mov	r2, #6
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	DIALOG_draw_ok_dialog
.L30:
	.align	2
.L29:
	.word	irri_irri_recursive_MUTEX
	.word	.LC0
	.word	irri_irri
	.word	.LC3
	.word	542
	.word	GuiVar_TestStationOnOff
	.word	.LANCHOR0
	.word	GuiVar_TestStationAutoMode
	.word	GuiVar_TestStationTimeDisplay
	.word	GuiVar_StopKeyReasonInList
	.word	635
.LFE8:
	.size	TEST_SEQ_turn_off_all_stations, .-TEST_SEQ_turn_off_all_stations
	.section	.text.TEST_SEQ_station_sequential_energize,"ax",%progbits
	.align	2
	.type	TEST_SEQ_station_sequential_energize, %function
TEST_SEQ_station_sequential_energize:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI6:
	ldr	r4, .L48
	ldr	r3, .L48+4
	ldr	r2, [r4, #0]
	cmp	r2, #65280
	bhi	.L32
	ldr	r3, [r3, #0]
	cmp	r3, #48
	bls	.L33
	mov	r0, #0
	bl	TEST_SEQ_turn_off_all_stations
.L33:
	ldr	r0, [r4, #0]
	sub	r0, r0, #1
	bl	TEST_SEQ_put_single_station_on_irri_list
	mov	r4, r0
	b	.L34
.L32:
	ldr	r0, [r3, #0]
	sub	r3, r0, #65280
	sub	r3, r3, #1
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L35
.L39:
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L38
	.word	.L38
	.word	.L38
.L36:
	ldr	r3, .L48+8
	cmp	r2, r3
	beq	.L35
	bl	TEST_SEQ_master_valve_control.isra.1
	b	.L35
.L37:
	bl	TEST_SEQ_pump_control.isra.2
	b	.L35
.L38:
	sub	r0, r0, #65280
	sub	r0, r0, #3
	mov	r1, #0
	bl	TEST_SEQ_light_control
.L35:
	ldr	r0, [r4, #0]
	sub	r3, r0, #65280
	sub	r3, r3, #1
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L40
.L44:
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L43
	.word	.L43
	.word	.L43
.L41:
	ldr	r3, .L48+12
	ldr	r0, [r3, #0]
	sub	r0, r0, #1
	bl	TEST_SEQ_put_single_station_on_irri_list
	mov	r4, r0
	bl	TEST_SEQ_master_valve_control.isra.1
	b	.L34
.L42:
	ldr	r3, .L48+12
	ldr	r0, [r3, #0]
	sub	r0, r0, #1
	bl	TEST_SEQ_put_single_station_on_irri_list
	mov	r4, r0
	bl	TEST_SEQ_master_valve_control.isra.1
	bl	TEST_SEQ_pump_control.isra.2
	b	.L34
.L43:
	sub	r0, r0, #65280
	sub	r0, r0, #3
	mov	r1, #1
	bl	TEST_SEQ_light_control
	mov	r4, #1
	b	.L45
.L40:
	ldr	r0, .L48+16
	bl	Alert_Message
	b	.L46
.L34:
	cmp	r4, #0
	beq	.L46
.L45:
	mov	r3, #1
	b	.L47
.L46:
	mov	r3, #0
	mov	r4, r3
.L47:
	ldr	r2, .L48+20
	mov	r0, #0
	str	r3, [r2, #0]
	ldr	r2, .L48+24
	str	r3, [r2, #0]
	bl	Redraw_Screen
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L49:
	.align	2
.L48:
	.word	GuiVar_StationInfoNumber
	.word	.LANCHOR0
	.word	65282
	.word	.LANCHOR1
	.word	.LC4
	.word	GuiVar_TestStationOnOff
	.word	GuiVar_TestStationTimeDisplay
.LFE10:
	.size	TEST_SEQ_station_sequential_energize, .-TEST_SEQ_station_sequential_energize
	.section	.text.TEST_SEQ_copy_settings_into_guivars.isra.3,"ax",%progbits
	.align	2
	.type	TEST_SEQ_copy_settings_into_guivars.isra.3, %function
TEST_SEQ_copy_settings_into_guivars.isra.3:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI7:
	ldr	r4, .L71
	ldr	r5, .L71+4
	ldr	r0, [r4, #0]
	ldr	r1, .L71+8
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	ldr	r2, [r5, #0]
	cmp	r2, #65280
	bhi	.L51
	ldr	r3, .L71+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L71+16
	ldr	r3, .L71+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r5, #0]
	ldr	r0, [r4, #0]
	sub	r1, r1, #1
	bl	nm_STATION_get_pointer_to_station
	subs	r6, r0, #0
	beq	.L52
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L71+24
	bl	strlcpy
	ldr	r1, [r5, #0]
	ldr	r2, .L71+28
	mov	r3, #4
	ldr	r0, [r4, #0]
	sub	r1, r1, #1
	bl	STATION_get_station_number_string
	ldr	r3, .L71+32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	ldr	r3, .L71+36
	moveq	r2, #2
	movne	r2, #1
	str	r2, [r3, #0]
	b	.L54
.L52:
	ldr	r1, .L71+40
	mov	r2, #48
	ldr	r0, .L71+24
	bl	strlcpy
	ldr	r1, .L71+40
	mov	r2, #4
	ldr	r0, .L71+28
	bl	strlcpy
	ldr	r0, .L71+8
	ldr	r1, .L71+40
	mov	r2, #49
	bl	strlcpy
	ldr	r3, .L71+36
	str	r6, [r3, #0]
.L54:
	ldr	r3, .L71+12
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L51:
	sub	r3, r2, #65280
	sub	r3, r3, #1
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L55
.L59:
	.word	.L56
	.word	.L57
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
.L56:
.LBB12:
	ldr	r4, .L71+44
.LBE12:
	ldr	r1, .L71+48
	mov	r2, #49
	ldr	r0, .L71+24
	bl	strlcpy
	ldr	r1, .L71+52
	mov	r2, #4
	ldr	r0, .L71+28
	bl	strlcpy
.LBB13:
	mov	r3, #125
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L71+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L71+56
	ldrb	r5, [r3, #108]	@ zero_extendqisi2
	and	r5, r5, #1
	b	.L70
.L57:
.LBE13:
.LBB14:
	ldr	r4, .L71+44
.LBE14:
	ldr	r1, .L71+60
	mov	r2, #49
	ldr	r0, .L71+24
	bl	strlcpy
	ldr	r1, .L71+64
	mov	r2, #4
	ldr	r0, .L71+28
	bl	strlcpy
.LBB15:
	mov	r3, #170
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L71+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L71+56
	ldrb	r5, [r3, #108]	@ zero_extendqisi2
	and	r5, r5, #2
.L70:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
.LBE15:
	cmp	r5, #0
	b	.L68
.L58:
	sub	r4, r2, #65280
	sub	r5, r4, #2
	mov	r1, #49
	ldr	r2, .L71+68
	mov	r3, r5
	ldr	r0, .L71+24
	bl	snprintf
	mov	r1, #4
	ldr	r2, .L71+72
	mov	r3, r5
	ldr	r0, .L71+28
	bl	snprintf
	sub	r0, r4, #3
	bl	IRRI_LIGHTS_light_is_energized
	cmp	r0, #0
.L68:
	ldr	r3, .L71+36
	moveq	r2, #1
	movne	r2, #2
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L55:
	ldr	r1, .L71+40
	mov	r2, #48
	ldr	r0, .L71+24
	bl	strlcpy
	ldr	r1, .L71+40
	mov	r2, #4
	ldr	r0, .L71+28
	bl	strlcpy
	ldr	r1, .L71+40
	mov	r2, #49
	ldr	r0, .L71+8
	bl	strlcpy
	ldr	r0, .L71+76
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message
.L72:
	.align	2
.L71:
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationInfoNumber
	.word	GuiVar_StationInfoControllerName
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	325
	.word	GuiVar_StationDescription
	.word	GuiVar_StationInfoNumber_str
	.word	GuiVar_TestStationOnOff
	.word	GuiVar_TestStationStatus
	.word	.LC5
	.word	poc_preserves_recursive_MUTEX
	.word	.LC6
	.word	.LC7
	.word	poc_preserves
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
.LFE18:
	.size	TEST_SEQ_copy_settings_into_guivars.isra.3, .-TEST_SEQ_copy_settings_into_guivars.isra.3
	.section	.text.TEST_SEQ_change_target_station_number,"ax",%progbits
	.align	2
	.type	TEST_SEQ_change_target_station_number, %function
TEST_SEQ_change_target_station_number:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI8:
	ldr	r4, .L85
	ldr	r2, .L85+4
	ldr	r3, [r4, #0]
	cmp	r0, #0
	str	r3, [r2, #0]
	beq	.L74
	cmp	r3, #65280
	bhi	.L75
	ldr	r2, .L85+8
	ldr	r2, [r2, #0]
	cmp	r3, r2
	ldrcs	r3, .L85+12
	bcs	.L82
	ldr	r5, .L85+16
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L85+20
	ldr	r3, .L85+24
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L85+28
	bl	STATION_get_next_available_station
	b	.L83
.L75:
	ldr	r2, .L85+32
	cmp	r3, r2
	addls	r3, r3, #1
	ldrhi	r3, .L85+36
	bhi	.L84
	b	.L82
.L74:
	cmp	r3, #65280
	bhi	.L79
	ldr	r2, .L85+36
	ldr	r2, [r2, #0]
	cmp	r3, r2
	ldrls	r3, .L85+40
	bls	.L82
	ldr	r5, .L85+16
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L85+20
	ldr	r3, .L85+44
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L85+28
	mov	r1, r4
	bl	STATION_get_prev_available_station
.L83:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	b	.L77
.L79:
	ldr	r2, .L85+12
	cmp	r3, r2
	subne	r3, r3, #1
	bne	.L82
	ldr	r3, .L85+8
.L84:
	ldr	r3, [r3, #0]
.L82:
	str	r3, [r4, #0]
.L77:
	ldmfd	sp!, {r4, r5, lr}
	b	TEST_SEQ_copy_settings_into_guivars.isra.3
.L86:
	.align	2
.L85:
	.word	GuiVar_StationInfoNumber
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	65281
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	787
	.word	GuiVar_StationInfoBoxIndex
	.word	65285
	.word	.LANCHOR1
	.word	65286
	.word	823
.LFE11:
	.size	TEST_SEQ_change_target_station_number, .-TEST_SEQ_change_target_station_number
	.section	.text.FDTO_TEST_SEQ_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_TEST_SEQ_draw_screen
	.type	FDTO_TEST_SEQ_draw_screen, %function
FDTO_TEST_SEQ_draw_screen:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L90
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI9:
	mov	r5, r0
	ldrnesh	r5, [r3, #0]
	bne	.L89
	bl	FLOWSENSE_get_controller_index
	ldr	r6, .L90+4
	ldr	r4, .L90+8
	mov	r2, #4
	ldr	r1, .L90+12
	ldr	r7, .L90+16
	str	r5, [r4, #0]
	str	r0, [r6, #0]
	ldr	r0, .L90+20
	bl	strlcpy
	bl	STATION_find_first_available_station_and_init_station_number_GuiVars
	ldr	r3, [r4, #0]
	mov	r1, r4
	mov	r0, r6
	str	r3, [r7, #0]
	bl	STATION_get_prev_available_station
	ldr	r2, [r4, #0]
	ldr	r3, .L90+24
	ldr	r1, .L90+12
	str	r2, [r3, #0]
	ldr	r3, [r7, #0]
	mov	r2, #49
	ldr	r0, .L90+28
	str	r3, [r4, #0]
	bl	strlcpy
	ldr	r3, .L90+32
	ldr	r2, .L90+36
	str	r5, [r3, #0]
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L90+40
	mov	r1, #3
	str	r3, [r2, #0]
	ldr	r2, .L90+44
	str	r3, [r2, #0]
	ldr	r2, .L90+48
	str	r3, [r2, #0]
	ldr	r2, .L90+52
	str	r3, [r2, #0]
	ldr	r2, .L90+56
	str	r3, [r2, #0]
	ldr	r2, .L90+60
	str	r1, [r2, #0]
	ldr	r2, .L90+64
	str	r3, [r2, #0]
.L89:
	mov	r5, r5, asl #16
	bl	TEST_SEQ_copy_settings_into_guivars.isra.3
	mov	r0, #59
	mov	r1, r5, asr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	GuiLib_Refresh
.L91:
	.align	2
.L90:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationInfoNumber
	.word	.LC5
	.word	.LANCHOR1
	.word	GuiVar_StationInfoNumber_str
	.word	.LANCHOR2
	.word	GuiVar_StationInfoControllerName
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	GuiVar_TestStationStatus
	.word	GuiVar_TestStationTimeDisplay
	.word	GuiVar_TestStationTimeRemaining
	.word	GuiVar_TestStationOnOff
	.word	GuiVar_TestStationAutoMode
	.word	GuiVar_TestStationAutoOnTime
	.word	GuiVar_TestStationAutoOffTime
.LFE12:
	.size	FDTO_TEST_SEQ_draw_screen, .-FDTO_TEST_SEQ_draw_screen
	.section	.text.FDTO_TEST_SEQ_update_screen,"ax",%progbits
	.align	2
	.global	FDTO_TEST_SEQ_update_screen
	.type	FDTO_TEST_SEQ_update_screen, %function
FDTO_TEST_SEQ_update_screen:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L101
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI10:
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L93
	ldr	r3, .L101+4
	ldr	r6, .L101+8
	ldr	r2, [r3, #0]
	cmp	r2, #0
	subne	r2, r2, #1
	strne	r2, [r3, #0]
	ldr	r2, [r6, #0]
	cmp	r2, #0
	beq	.L95
	ldr	r3, .L101+12
	ldr	r5, .L101+4
	ldr	r2, [r3, #0]
	cmp	r2, #0
	beq	.L96
	ldr	r4, [r5, #0]
	cmp	r4, #0
	bne	.L93
	str	r4, [r3, #0]
	mov	r0, #1
	bl	TEST_SEQ_change_target_station_number
	ldr	r3, .L101+16
	ldr	r2, [r3, #0]
	ldr	r3, .L101+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L97
	ldr	r3, .L101+24
	ldr	r2, [r3, #0]
	ldr	r3, .L101+28
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L97
	ldr	r3, .L101
	mov	r2, #2
	str	r2, [r3, #0]
	mov	r0, r4
	str	r4, [r6, #0]
	str	r4, [r5, #0]
	bl	TEST_SEQ_turn_off_all_stations
	b	.L93
.L97:
	ldr	r3, .L101+32
	ldr	r0, [r3, #0]
	bl	TEST_SEQ_set_test_time
	bl	TEST_SEQ_station_sequential_energize
	mov	r2, #1
	b	.L99
.L96:
	ldr	r2, [r5, #0]
	cmp	r2, #0
	bne	.L93
	mov	r2, #1
	str	r2, [r3, #0]
.L99:
	ldr	r3, .L101
	str	r2, [r3, #0]
	b	.L93
.L95:
	ldr	r3, [r3, #0]
	ldr	r2, .L101
	cmp	r3, #0
	streq	r3, [r2, #0]
	ldreq	r2, .L101+36
	movne	r3, #1
	str	r3, [r2, #0]
.L93:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Redraw_Screen
.L102:
	.align	2
.L101:
	.word	GuiVar_TestStationTimeDisplay
	.word	GuiVar_TestStationTimeRemaining
	.word	GuiVar_TestStationAutoMode
	.word	.LANCHOR4
	.word	GuiVar_StationInfoNumber
	.word	.LANCHOR5
	.word	GuiVar_StationInfoBoxIndex
	.word	.LANCHOR6
	.word	GuiVar_TestStationAutoOnTime
	.word	GuiVar_TestStationOnOff
.LFE13:
	.size	FDTO_TEST_SEQ_update_screen, .-FDTO_TEST_SEQ_update_screen
	.section	.text.TEST_SEQ_process_screen,"ax",%progbits
	.align	2
	.global	TEST_SEQ_process_screen
	.type	TEST_SEQ_process_screen, %function
TEST_SEQ_process_screen:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r4, r5, lr}
.LCFI11:
	mov	r4, r0
	mov	r5, r1
	beq	.L110
	bhi	.L114
	cmp	r0, #2
	beq	.L107
	bhi	.L115
	cmp	r0, #0
	beq	.L105
	cmp	r0, #1
	bne	.L104
	b	.L106
.L115:
	cmp	r0, #3
	beq	.L108
	cmp	r0, #4
	bne	.L104
	b	.L158
.L114:
	cmp	r0, #67
	beq	.L113
	bhi	.L116
	cmp	r0, #20
	beq	.L142
	cmp	r0, #48
	bne	.L104
	b	.L159
.L116:
	cmp	r0, #80
	beq	.L110
	cmp	r0, #84
	bne	.L104
	b	.L142
.L110:
	mov	r4, #0
	b	.L111
.L107:
	ldr	r3, .L161
	ldrsh	r3, [r3, #0]
	cmp	r3, #1
	beq	.L118
	cmp	r3, #2
	bne	.L143
	b	.L160
.L118:
	ldr	r3, .L161+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L120
	mov	r0, #60
	bl	TEST_SEQ_set_test_time
	b	.L155
.L120:
	bl	good_key_beep
	mov	r0, #0
	b	.L149
.L160:
	ldr	r4, .L161+8
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L123
	ldr	r3, .L161+12
	ldr	r0, [r3, #0]
	bl	TEST_SEQ_set_test_time
	mov	r3, #1
	str	r3, [r4, #0]
	ldr	r3, .L161+16
	ldr	r2, [r3, #0]
	ldr	r3, .L161+20
	str	r2, [r3, #0]
	ldr	r3, .L161+24
	ldr	r2, [r3, #0]
	ldr	r3, .L161+28
	str	r2, [r3, #0]
.L155:
	bl	TEST_SEQ_station_sequential_energize
	cmp	r0, #0
	beq	.L143
	bl	good_key_beep
	b	.L122
.L123:
	bl	good_key_beep
	mov	r0, #0
	str	r0, [r4, #0]
.L149:
	ldr	r3, .L161+32
	str	r0, [r3, #0]
	bl	TEST_SEQ_turn_off_all_stations
	b	.L122
.L143:
	bl	bad_key_beep
.L122:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, lr}
	b	Redraw_Screen
.L142:
	mov	r4, #1
.L111:
	ldr	r3, .L161
	ldrsh	r5, [r3, #0]
	cmp	r5, #1
	beq	.L127
	cmp	r5, #3
	beq	.L128
	cmp	r5, #0
	bne	.L144
	bl	good_key_beep
	ldr	r3, .L161+8
	mov	r0, r5
	str	r5, [r3, #0]
	bl	TEST_SEQ_turn_off_all_stations
	mov	r0, r4
	bl	TEST_SEQ_change_target_station_number
	b	.L129
.L127:
	ldr	r3, .L161+8
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r0, #60
	bl	TEST_SEQ_set_test_time
	mov	r0, r4
	bl	TEST_SEQ_change_target_station_number
	bl	TEST_SEQ_station_sequential_energize
	cmp	r0, #0
	beq	.L144
	bl	good_key_beep
	b	.L129
.L128:
	cmp	r4, #0
	ldr	r4, .L161+12
	ldr	r3, [r4, #0]
	beq	.L131
	cmp	r3, #59
	bhi	.L144
	bl	good_key_beep
	ldr	r3, [r4, #0]
	add	r3, r3, #1
	b	.L150
.L131:
	cmp	r3, #0
	beq	.L144
	bl	good_key_beep
	ldr	r3, [r4, #0]
	sub	r3, r3, #1
.L150:
	str	r3, [r4, #0]
	b	.L129
.L144:
	bl	bad_key_beep
.L129:
	ldmfd	sp!, {r4, r5, lr}
	b	Refresh_Screen
.L158:
	ldr	r3, .L161
	ldrsh	r3, [r3, #0]
	cmp	r3, #2
	moveq	r0, #0
	moveq	r1, #1
	movne	r0, #1
	bne	.L106
	b	.L151
.L105:
	ldr	r3, .L161
	ldrsh	r1, [r3, #0]
	cmp	r1, #1
	beq	.L137
	cmp	r1, #3
	b	.L157
.L137:
	mov	r0, #3
.L151:
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Select
.L106:
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Up
.L108:
	ldr	r3, .L161
	ldrsh	r3, [r3, #0]
	cmp	r3, #3
.L157:
	bne	.L147
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L147:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Down
.L159:
	ldr	r3, .L161+4
	ldr	r4, [r3, #0]
	cmp	r4, #1
	bne	.L152
	bl	good_key_beep
	mov	r0, r4
	ldmfd	sp!, {r4, r5, lr}
	b	TEST_SEQ_turn_off_all_stations
.L113:
	mov	r0, #0
	bl	TEST_SEQ_turn_off_all_stations
	ldr	r3, .L161+36
	mov	r2, #7
	str	r2, [r3, #0]
.L104:
	mov	r0, r4
	mov	r1, r5
.L152:
	ldmfd	sp!, {r4, r5, lr}
	b	KEY_process_global_keys
.L162:
	.align	2
.L161:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_TestStationOnOff
	.word	GuiVar_TestStationAutoMode
	.word	GuiVar_TestStationAutoOnTime
	.word	GuiVar_StationInfoNumber
	.word	.LANCHOR5
	.word	GuiVar_StationInfoBoxIndex
	.word	.LANCHOR6
	.word	GuiVar_TestStationTimeDisplay
	.word	GuiVar_MenuScreenToShow
.LFE14:
	.size	TEST_SEQ_process_screen, .-TEST_SEQ_process_screen
	.section	.data.g_TEST_SEQ_last_cursor_pos,"aw",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_TEST_SEQ_last_cursor_pos, %object
	.size	g_TEST_SEQ_last_cursor_pos, 4
g_TEST_SEQ_last_cursor_pos:
	.word	1
	.section	.data.g_TEST_SEQ_irrigation_station_min,"aw",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_TEST_SEQ_irrigation_station_min, %object
	.size	g_TEST_SEQ_irrigation_station_min, 4
g_TEST_SEQ_irrigation_station_min:
	.word	1
	.section	.data.g_TEST_SEQ_irrigation_station_max,"aw",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_TEST_SEQ_irrigation_station_max, %object
	.size	g_TEST_SEQ_irrigation_station_max, 4
g_TEST_SEQ_irrigation_station_max:
	.word	48
	.section	.bss.g_TEST_SEQ_previous_station,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_TEST_SEQ_previous_station, %object
	.size	g_TEST_SEQ_previous_station, 4
g_TEST_SEQ_previous_station:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_test_sequential.c\000"
.LC1:
	.ascii	"TS3\000"
.LC2:
	.ascii	"TS4\000"
.LC3:
	.ascii	"TS2\000"
.LC4:
	.ascii	"TS5\000"
.LC5:
	.ascii	"\000"
.LC6:
	.ascii	"Master Valve\000"
.LC7:
	.ascii	"MV\000"
.LC8:
	.ascii	"Pump\000"
.LC9:
	.ascii	"PMP\000"
.LC10:
	.ascii	"Light %d\000"
.LC11:
	.ascii	"L%d\000"
.LC12:
	.ascii	"TS1\000"
	.section	.bss.g_TEST_SEQ_auto_off_delay,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_TEST_SEQ_auto_off_delay, %object
	.size	g_TEST_SEQ_auto_off_delay, 4
g_TEST_SEQ_auto_off_delay:
	.space	4
	.section	.bss.g_TEST_SEQ_auto_starting_station,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	g_TEST_SEQ_auto_starting_station, %object
	.size	g_TEST_SEQ_auto_starting_station, 4
g_TEST_SEQ_auto_starting_station:
	.space	4
	.section	.bss.g_TEST_SEQ_auto_starting_box,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	g_TEST_SEQ_auto_starting_box, %object
	.size	g_TEST_SEQ_auto_starting_box, 4
g_TEST_SEQ_auto_starting_box:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI0-.LFB9
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI3-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI4-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI5-.LFB8
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI6-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI7-.LFB18
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI8-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI9-.LFB12
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI10-.LFB13
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_test_sequential.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x136
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF15
	.byte	0x1
	.4byte	.LASF16
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0xea
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x8b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0xb8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.byte	0x76
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0xa4
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x137
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.byte	0x65
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x241
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST0
	.uleb128 0x6
	.4byte	.LASF8
	.byte	0x1
	.byte	0xd2
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST1
	.uleb128 0x7
	.4byte	0x29
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST2
	.uleb128 0x7
	.4byte	0x31
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST3
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1f3
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST4
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x290
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST5
	.uleb128 0x7
	.4byte	0x49
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST6
	.uleb128 0x5
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x2fe
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST7
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x35b
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST8
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x3db
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST9
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x440
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST10
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB9
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI1
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB5
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB16
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB17
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB8
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB10
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB18
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB11
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB12
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB13
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF13:
	.ascii	"FDTO_TEST_SEQ_update_screen\000"
.LASF10:
	.ascii	"TEST_SEQ_station_sequential_energize\000"
.LASF5:
	.ascii	"TEST_SEQ_copy_settings_into_guivars\000"
.LASF0:
	.ascii	"TEST_SEQ_verify_station_status\000"
.LASF6:
	.ascii	"TEST_SEQ_set_test_time\000"
.LASF2:
	.ascii	"TEST_SEQ_pump_control\000"
.LASF15:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF9:
	.ascii	"TEST_SEQ_turn_off_all_stations\000"
.LASF12:
	.ascii	"FDTO_TEST_SEQ_draw_screen\000"
.LASF1:
	.ascii	"TEST_SEQ_master_valve_control\000"
.LASF16:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_test_sequential.c\000"
.LASF11:
	.ascii	"TEST_SEQ_change_target_station_number\000"
.LASF14:
	.ascii	"TEST_SEQ_process_screen\000"
.LASF3:
	.ascii	"TEST_SEQ_master_valve_output_is_energized\000"
.LASF4:
	.ascii	"TEST_SEQ_pump_output_is_energized\000"
.LASF7:
	.ascii	"TEST_SEQ_put_single_station_on_irri_list\000"
.LASF8:
	.ascii	"TEST_SEQ_light_control\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
