	.file	"r_orphan_two_wire_stations.c"
	.text
.Ltext0:
	.section	.text.ORPHAN_TWO_WIRE_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.type	ORPHAN_TWO_WIRE_load_guivars_for_scroll_line, %function
ORPHAN_TWO_WIRE_load_guivars_for_scroll_line:
.LFB1:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI0:
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	FLOWSENSE_get_controller_index
	mov	r3, #12
	ldr	r2, .L2
	mul	r3, r4, r3
	add	r4, r2, r3
	ldr	r1, [r2, r3]
	mov	r2, sp
	mov	r3, #16
	bl	STATION_get_station_number_string
	ldr	r2, .L2+4
	mov	r1, #4
	mov	r3, r0
	ldr	r0, .L2+8
	bl	snprintf
	ldr	r2, [r4, #4]
	ldr	r3, .L2+12
	str	r2, [r3, #0]
	ldr	r2, [r4, #8]
	ldr	r3, .L2+16
	str	r2, [r3, #0]
	ldmfd	sp!, {r0, r1, r2, r3, r4, pc}
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	.LC0
	.word	GuiVar_RptStation
	.word	GuiVar_RptDecoderSN
	.word	GuiVar_RptDecoderOutput
.LFE1:
	.size	ORPHAN_TWO_WIRE_load_guivars_for_scroll_line, .-ORPHAN_TWO_WIRE_load_guivars_for_scroll_line
	.section	.text.ORPHAN_TWO_WIRE_STATIONS_populate_list,"ax",%progbits
	.align	2
	.global	ORPHAN_TWO_WIRE_STATIONS_populate_list
	.type	ORPHAN_TWO_WIRE_STATIONS_populate_list, %function
ORPHAN_TWO_WIRE_STATIONS_populate_list:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI1:
	ldr	r6, .L15+4
	mov	r4, #0
	str	r4, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L15+8
	mov	r1, #400
	ldr	r2, .L15+12
	ldr	sl, .L15+16
	mov	r8, r0
	ldr	r0, [r3, #0]
	mov	r3, #79
	bl	xQueueTakeMutexRecursive_debug
.L10:
	ldr	r0, .L15+20
	mov	r1, r4
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	subs	r5, r0, #0
	beq	.L5
	bl	nm_STATION_get_box_index_0
	cmp	r0, r8
	bne	.L6
	mov	r0, r5
	bl	nm_STATION_get_decoder_serial_number
	subs	r7, r0, #0
	beq	.L6
	mov	r3, #108
	ldr	r0, [r6, #0]
	mov	r1, #400
	ldr	r2, .L15+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L15+24
.L8:
	ldr	r2, [r3, #220]
	cmp	r7, r2
	beq	.L7
	add	r3, r3, #60
	cmp	r3, sl
	bne	.L8
	b	.L14
.L7:
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
.L6:
	add	r4, r4, #1
	cmp	r4, #2112
	bne	.L10
.L5:
	ldr	r3, .L15+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L15
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L14:
	ldr	r9, .L15
	mov	r0, r5
	ldr	fp, [r9, #0]
	bl	nm_STATION_get_station_number_0
	mov	r2, #12
	ldr	r3, .L15+28
	mul	fp, r2, fp
	str	r0, [r3, fp]
	ldr	fp, [r9, #0]
	mov	r0, r5
	mla	fp, r2, fp, r3
	str	r7, [fp, #4]
	bl	nm_STATION_get_decoder_output
	ldr	r3, [r9, #0]
	add	r3, r3, #1
	str	r3, [r9, #0]
	str	r0, [fp, #8]
	b	.L7
.L16:
	.align	2
.L15:
	.word	.LANCHOR1
	.word	tpmicro_data_recursive_MUTEX
	.word	list_program_data_recursive_MUTEX
	.word	.LC1
	.word	tpmicro_data+4800
	.word	station_info_list_hdr
	.word	tpmicro_data
	.word	.LANCHOR0
.LFE0:
	.size	ORPHAN_TWO_WIRE_STATIONS_populate_list, .-ORPHAN_TWO_WIRE_STATIONS_populate_list
	.section	.text.FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report
	.type	FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report, %function
FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	mvn	r1, #0
	mov	r4, r0
	mov	r2, #1
	mov	r0, #89
	bl	GuiLib_ShowScreen
	ldr	r3, .L18
	ldr	r2, .L18+4
	ldr	r1, [r3, #0]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	b	FDTO_REPORTS_draw_report
.L19:
	.align	2
.L18:
	.word	.LANCHOR1
	.word	ORPHAN_TWO_WIRE_load_guivars_for_scroll_line
.LFE2:
	.size	FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report, .-FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report
	.section	.text.ORPHAN_TWO_WIRE_STATIONS_process_report,"ax",%progbits
	.align	2
	.global	ORPHAN_TWO_WIRE_STATIONS_process_report
	.type	ORPHAN_TWO_WIRE_STATIONS_process_report, %function
ORPHAN_TWO_WIRE_STATIONS_process_report:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI3:
	mov	r4, r0
	mov	r6, r1
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #2
	mov	r5, r0
	beq	.L22
	cmp	r4, #67
	bne	.L25
	b	.L26
.L22:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #0
	blt	.L24
	ldr	r6, .L27
	bl	good_key_beep
	ldr	r2, .L27+4
	mov	r3, #188
	ldr	r0, [r6, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, #12
	ldr	r2, .L27+8
	mul	r3, r0, r3
	mov	r0, r5
	ldr	r1, [r2, r3]
	bl	nm_STATION_get_pointer_to_station
	mov	r1, r4
	mov	r7, r0
	bl	STATION_get_change_bits_ptr
	str	r5, [sp, #0]
	mov	r5, #1
	mov	r1, #0
	mov	r2, r5
	mov	r3, r4
	str	r5, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r7
	bl	nm_STATION_set_decoder_serial_number
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	bl	ORPHAN_TWO_WIRE_STATIONS_populate_list
	mov	r0, r5
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Redraw_Screen
.L24:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	bad_key_beep
.L26:
	ldr	r3, .L27+12
	ldr	r2, .L27+16
	ldr	r3, [r3, #0]
	mov	r1, #36
	mla	r3, r1, r3, r2
	mov	r0, r4
	ldr	r2, [r3, #4]
	ldr	r3, .L27+20
	mov	r1, r6
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	TWO_WIRE_ASSIGNMENT_draw_dialog
.L25:
	mov	r2, #0
	mov	r0, r4
	mov	r1, r6
	mov	r3, r2
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	REPORTS_process_report
.L28:
	.align	2
.L27:
	.word	list_program_data_recursive_MUTEX
	.word	.LC1
	.word	.LANCHOR0
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	ORPHAN_TWO_WIRE_STATIONS_process_report, .-ORPHAN_TWO_WIRE_STATIONS_process_report
	.section	.bss.ORPHAN_STATIONS_list_of_orphans,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ORPHAN_STATIONS_list_of_orphans, %object
	.size	ORPHAN_STATIONS_list_of_orphans, 2112
ORPHAN_STATIONS_list_of_orphans:
	.space	2112
	.section	.bss.g_ORPHAN_TWO_WIRE_STATIONS_num_stations,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_ORPHAN_TWO_WIRE_STATIONS_num_stations, %object
	.size	g_ORPHAN_TWO_WIRE_STATIONS_num_stations, 4
g_ORPHAN_TWO_WIRE_STATIONS_num_stations:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%s\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_orphan_two_wire_stations.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_orphan_two_wire_stations.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x95
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x3b
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xa1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xa9
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_orphan_two_wire_stations.c\000"
.LASF1:
	.ascii	"FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report\000"
.LASF5:
	.ascii	"ORPHAN_TWO_WIRE_load_guivars_for_scroll_line\000"
.LASF0:
	.ascii	"ORPHAN_TWO_WIRE_STATIONS_populate_list\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"ORPHAN_TWO_WIRE_STATIONS_process_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
