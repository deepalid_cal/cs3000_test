	.file	"e_en_programming.c"
	.text
.Ltext0:
	.section	.text.FDTO_EN_PROGRAMMING_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_EN_PROGRAMMING_draw_screen
	.type	FDTO_EN_PROGRAMMING_draw_screen, %function
FDTO_EN_PROGRAMMING_draw_screen:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	mov	r5, r0
	mov	r7, r1
	bne	.L2
.LBB4:
	ldr	r3, .L6
	ldr	r6, .L6+4
	ldr	r1, .L6+8
	mov	r2, #17
	ldr	r0, .L6+12
	str	r3, [r6, #0]
	bl	strlcpy
	ldr	r1, .L6+8
	mov	r2, #49
	ldr	r0, .L6+16
	bl	strlcpy
	ldr	r3, .L6+20
	mov	r4, #0
	str	r4, [r3, #0]
	ldr	r3, .L6+24
	ldr	r1, .L6+8
	str	r4, [r3, #0]
	ldr	r3, .L6+28
	mov	r2, #49
	str	r4, [r3, #0]
	ldr	r3, .L6+32
	ldr	r0, .L6+36
	str	r4, [r3, #0]
	ldr	r3, .L6+40
	str	r4, [r3, #0]
	ldr	r3, .L6+44
	str	r4, [r3, #0]
	ldr	r3, .L6+48
	str	r4, [r3, #0]
	ldr	r3, .L6+52
	str	r4, [r3, #0]
	bl	strlcpy
	ldr	r1, .L6+8
	mov	r2, #49
	ldr	r0, .L6+56
	bl	strlcpy
	ldr	r3, .L6+60
	mov	r2, #8
	str	r2, [r3, #0]
	ldr	r3, .L6+64
	ldr	r1, .L6+8
	str	r5, [r3, #0]
	mov	r2, #49
	ldr	r0, .L6+68
	bl	strlcpy
.LBE4:
	ldr	r0, .L6+72
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L6+76
	str	r7, [r3, #0]
	str	r0, [r6, #0]
	b	.L3
.L2:
	ldr	r3, .L6+80
	ldrsh	r4, [r3, #0]
	cmn	r4, #1
	ldreq	r3, .L6+84
	ldreq	r4, [r3, #0]
.L3:
	mov	r1, r4, asl #16
	mov	r0, #17
	mov	r1, r1, asr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	cmp	r5, #1
	ldmnefd	sp!, {r4, r5, r6, r7, pc}
	bl	DEVICE_EXCHANGE_draw_dialog
	ldr	r3, .L6+76
	ldr	r2, .L6+88
	ldr	r0, [r3, #0]
	mov	r1, #82
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	e_SHARED_start_device_communication
.L7:
	.align	2
.L6:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	.LC0
	.word	GuiVar_ENDHCPName
	.word	GuiVar_ENFirmwareVer
	.word	GuiVar_ENGateway_1
	.word	GuiVar_ENGateway_2
	.word	GuiVar_ENGateway_3
	.word	GuiVar_ENGateway_4
	.word	GuiVar_ENMACAddress
	.word	GuiVar_ENIPAddress_1
	.word	GuiVar_ENIPAddress_2
	.word	GuiVar_ENIPAddress_3
	.word	GuiVar_ENIPAddress_4
	.word	GuiVar_ENModel
	.word	GuiVar_ENNetmask
	.word	GuiVar_ENObtainIPAutomatically
	.word	GuiVar_CommOptionInfoText
	.word	36867
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	.LANCHOR2
.LFE5:
	.size	FDTO_EN_PROGRAMMING_draw_screen, .-FDTO_EN_PROGRAMMING_draw_screen
	.section	.text.FDTO_EN_PROGRAMMING_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_EN_PROGRAMMING_process_device_exchange_key, %function
FDTO_EN_PROGRAMMING_process_device_exchange_key:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	mov	r4, r0
	bl	e_SHARED_get_info_text_from_programming_struct
	sub	r3, r4, #36864
	cmp	r3, #6
	ldmhifd	sp!, {r4, r5, r6, pc}
	mov	r2, #1
	mov	r3, r2, asl r3
	tst	r3, #109
	movne	r0, r4
	bne	.L20
	tst	r3, #18
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r3, .L21
	cmp	r4, r3
	bne	.L12
	ldr	r3, .L21+4
	ldr	r3, [r3, #0]
	cmp	r3, r2
.LBB9:
	ldreq	r0, .L21+8
.LBE9:
	beq	.L20
.L12:
.LBB10:
	ldr	r3, .L21+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L13
	ldr	r5, [r3, #172]
	cmp	r5, #0
	beq	.L13
	ldr	r4, [r3, #184]
	cmp	r4, #0
	beq	.L13
	ldr	r2, [r5, #4]
	ldr	r3, .L21+16
	cmp	r2, #0
	movne	r2, #1
	ldr	r6, .L21+12
	strne	r2, [r3, #0]
	ldrne	r1, .L21+20
	streq	r2, [r3, #0]
	addeq	r1, r4, #45
	ldrne	r0, .L21+24
	ldreq	r0, .L21+24
	mov	r2, #17
	bl	strlcpy
	ldr	r1, .L21+28
	ldr	r0, .L21+24
	bl	strtok
	ldr	r1, [r6, #0]
	mov	r2, #49
	add	r1, r1, #232
	ldr	r0, .L21+32
	bl	strlcpy
	ldr	r0, .L21+32
	bl	strlen
	cmp	r0, #0
	bne	.L16
	ldr	r0, .L21+32
	ldr	r1, .L21+36
	mov	r2, #49
	bl	strlcpy
.L16:
	ldr	r3, [r6, #0]
	add	r0, r4, #169
	ldr	r2, [r3, #268]
	ldr	r3, .L21+40
	adds	r2, r2, #0
	movne	r2, #1
	str	r2, [r3, #0]
	bl	atoi
	ldr	r3, .L21+44
	str	r0, [r3, #0]
	add	r0, r4, #175
	bl	atoi
	ldr	r3, .L21+48
	str	r0, [r3, #0]
	add	r0, r4, #181
	bl	atoi
	ldr	r3, .L21+52
	str	r0, [r3, #0]
	add	r0, r4, #187
	bl	atoi
	ldr	r3, .L21+56
	str	r0, [r3, #0]
	add	r0, r4, #21
	bl	atoi
	ldr	r3, .L21+60
	str	r0, [r3, #0]
	add	r0, r4, #27
	bl	atoi
	ldr	r3, .L21+64
	str	r0, [r3, #0]
	add	r0, r4, #33
	bl	atoi
	ldr	r3, .L21+68
	str	r0, [r3, #0]
	add	r0, r4, #39
	bl	atoi
	ldr	r3, .L21+72
	add	r1, r5, #38
	mov	r2, #49
	str	r0, [r3, #0]
	ldr	r0, .L21+76
	bl	strlcpy
	ldr	r0, .L21+76
	bl	strlen
	cmp	r0, #0
	bne	.L17
	ldr	r0, .L21+76
	ldr	r1, .L21+36
	mov	r2, #49
	bl	strlcpy
.L17:
	add	r1, r5, #8
	mov	r2, #49
	ldr	r0, .L21+80
	bl	strlcpy
	ldr	r0, .L21+80
	bl	strlen
	cmp	r0, #0
	bne	.L18
	ldr	r0, .L21+80
	ldr	r1, .L21+36
	mov	r2, #49
	bl	strlcpy
.L18:
	ldr	r2, [r5, #0]
	ldr	r3, .L21+84
	str	r2, [r3, #0]
.L13:
.LBE10:
	ldr	r0, .L21+88
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L21+92
	str	r0, [r3, #0]
	bl	DIALOG_close_ok_dialog
	ldr	r3, .L21+96
	mov	r0, #0
	ldr	r1, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FDTO_EN_PROGRAMMING_draw_screen
.L20:
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L21+92
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	DEVICE_EXCHANGE_draw_dialog
.L22:
	.align	2
.L21:
	.word	36868
	.word	.LANCHOR3
	.word	36867
	.word	dev_state
	.word	GuiVar_ENDHCPNameNotSet
	.word	.LC1
	.word	GuiVar_ENDHCPName
	.word	.LC2
	.word	GuiVar_ENFirmwareVer
	.word	.LC3
	.word	GuiVar_ENObtainIPAutomatically
	.word	GuiVar_ENGateway_1
	.word	GuiVar_ENGateway_2
	.word	GuiVar_ENGateway_3
	.word	GuiVar_ENGateway_4
	.word	GuiVar_ENIPAddress_1
	.word	GuiVar_ENIPAddress_2
	.word	GuiVar_ENIPAddress_3
	.word	GuiVar_ENIPAddress_4
	.word	GuiVar_ENMACAddress
	.word	GuiVar_ENModel
	.word	GuiVar_ENNetmask
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	.LANCHOR0
.LFE3:
	.size	FDTO_EN_PROGRAMMING_process_device_exchange_key, .-FDTO_EN_PROGRAMMING_process_device_exchange_key
	.section	.text.FDTO_EN_PROGRAMMING_redraw_screen,"ax",%progbits
	.align	2
	.type	FDTO_EN_PROGRAMMING_redraw_screen, %function
FDTO_EN_PROGRAMMING_redraw_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L24
	ldr	r1, [r3, #0]
	b	FDTO_EN_PROGRAMMING_draw_screen
.L25:
	.align	2
.L24:
	.word	.LANCHOR0
.LFE4:
	.size	FDTO_EN_PROGRAMMING_redraw_screen, .-FDTO_EN_PROGRAMMING_redraw_screen
	.section	.text.EN_PROGRAMMING_process_screen,"ax",%progbits
	.align	2
	.global	EN_PROGRAMMING_process_screen
	.type	EN_PROGRAMMING_process_screen, %function
EN_PROGRAMMING_process_screen:
.LFB6:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L110
	ldr	r2, .L110+4
	ldrsh	r3, [r3, #0]
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI2:
	cmp	r3, r2
	sub	sp, sp, #48
.LCFI3:
	mov	r4, r0
	mov	r5, r1
	beq	.L29
	cmp	r3, #740
	beq	.L30
	sub	r2, r2, #117
	cmp	r3, r2
	bne	.L94
	cmp	r0, #67
	beq	.L31
	cmp	r0, #2
	bne	.L32
	ldr	r3, .L110+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L32
.L31:
	ldr	r0, .L110+12
	ldr	r1, .L110+16
	mov	r2, #17
	bl	strlcpy
.L32:
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L110+20
	bl	KEYBOARD_process_key
	b	.L26
.L29:
	ldr	r1, .L110+24
	b	.L99
.L30:
	ldr	r1, .L110+28
.L99:
	bl	COMBO_BOX_key_press
	b	.L26
.L94:
	cmp	r0, #4
	beq	.L39
	bhi	.L43
	cmp	r0, #1
	beq	.L36
	bcc	.L35
	cmp	r0, #2
	beq	.L37
	cmp	r0, #3
	bne	.L34
	b	.L38
.L43:
	cmp	r0, #84
	beq	.L41
	bhi	.L44
	cmp	r0, #67
	beq	.L40
	cmp	r0, #80
	bne	.L34
	b	.L41
.L44:
	sub	r3, r0, #36864
	cmp	r3, #6
	bhi	.L34
	mov	r3, #2
	str	r3, [sp, #12]
	ldr	r3, .L110+32
	str	r0, [sp, #36]
	add	r0, sp, #12
	str	r3, [sp, #32]
	bl	Display_Post_Command
	ldr	r3, .L110+36
	cmp	r4, r3
	bne	.L26
	ldr	r3, .L110+40
	ldr	r2, [r3, #0]
	cmp	r2, #1
	moveq	r2, #0
	bne	.L26
	b	.L100
.L37:
	ldr	r4, .L110+44
	ldr	r0, .L110+48
	ldr	r5, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r5, r0
	beq	.L89
	ldr	r0, .L110+52
	ldr	r4, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r4, r0
	beq	.L89
	ldr	r4, .L110+8
	ldrsh	r3, [r4, #0]
	cmp	r3, #12
	ldrls	pc, [pc, r3, asl #2]
	b	.L89
.L52:
	.word	.L47
	.word	.L48
	.word	.L89
	.word	.L89
	.word	.L89
	.word	.L89
	.word	.L49
	.word	.L89
	.word	.L89
	.word	.L89
	.word	.L89
	.word	.L50
	.word	.L51
.L47:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L110+56
	b	.L101
.L48:
	bl	good_key_beep
	ldr	r1, .L110+12
	mov	r2, #65
	ldr	r0, .L110+16
	bl	strlcpy
	mov	r1, #0
	mov	r2, #17
	ldr	r0, .L110+12
	bl	memset
	mov	r0, #102
	mov	r1, #68
	mov	r2, #17
	mov	r3, #0
	bl	KEYBOARD_draw_keyboard
	b	.L26
.L49:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L110+60
.L101:
	add	r0, sp, #12
	str	r3, [sp, #32]
	bl	Display_Post_Command
	b	.L26
.L51:
	bl	good_key_beep
	ldr	r3, .L110+64
	ldrsh	r2, [r4, #0]
	str	r2, [r3, #0]
.LBB13:
	ldr	r3, .L110+68
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L53
	ldr	r5, [r3, #172]
	cmp	r5, #0
	beq	.L53
	ldr	r4, [r3, #184]
	cmp	r4, #0
	beq	.L53
	ldr	r0, .L110+12
	ldr	r1, .L110+72
	mov	r2, #7
	bl	strncmp
	cmp	r0, #0
	add	r0, r4, #45
	bne	.L54
	ldr	r3, [r5, #4]
	cmp	r3, #0
	addne	r1, r5, #38
	bne	.L95
.L54:
	ldr	r1, .L110+12
.L95:
	mov	r2, #18
	bl	strlcpy
	ldr	r3, .L110+24
	ldr	r2, [r3, #0]
	cmp	r2, #0
	moveq	r2, #1
	streq	r2, [r3, #0]
	ldr	r3, [r3, #0]
	str	r3, [r5, #0]
	ldr	r3, .L110+76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldr	r3, .L110+28
	bne	.L57
	ldr	r2, .L110+80
	ldr	r2, [r2, #0]
	cmp	r2, #0
	moveq	r2, #1
	streq	r2, [r3, #0]
.L57:
	ldr	r3, [r3, #0]
	ldr	r2, .L110+68
	cmp	r3, #0
	beq	.L58
	ldr	r3, [r2, #0]
	mov	r5, #0
	mov	r2, #1
	str	r2, [r3, #268]
	mov	r1, #17
	ldr	r2, .L110+84
	mov	r3, r5
	add	r0, r4, #4
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	str	r5, [sp, #8]
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	mov	r3, r5
	add	r0, r4, #21
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	mov	r3, r5
	add	r0, r4, #27
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	mov	r3, #4
	add	r0, r4, #33
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	mov	r3, r5
	add	r0, r4, #39
	bl	snprintf
	mov	r1, #17
	ldr	r2, .L110+84
	mov	r3, r5
	add	r0, r4, #152
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	str	r5, [sp, #8]
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	mov	r3, r5
	add	r0, r4, #169
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	mov	r3, r5
	add	r0, r4, #175
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	mov	r3, r5
	add	r0, r4, #181
	bl	snprintf
	add	r0, r4, #187
	mov	r1, #6
	ldr	r2, .L110+88
	mov	r3, r5
	b	.L96
.L58:
	ldr	r2, [r2, #0]
	ldr	r7, .L110+80
	str	r3, [r2, #268]
	ldr	r6, .L110+92
	ldr	r3, [r7, #0]
	ldr	r5, .L110+96
	str	r3, [sp, #0]
	ldr	r3, [r6, #0]
	ldr	r8, .L110+76
	str	r3, [sp, #4]
	ldr	r3, [r5, #0]
	mov	r1, #17
	str	r3, [sp, #8]
	ldr	r2, .L110+84
	ldr	r3, [r8, #0]
	add	r0, r4, #4
	bl	snprintf
	ldr	r3, [r8, #0]
	mov	r1, #6
	ldr	r2, .L110+88
	add	r0, r4, #21
	bl	snprintf
	ldr	r3, [r7, #0]
	mov	r1, #6
	ldr	r2, .L110+88
	ldr	r7, .L110+100
	add	r0, r4, #27
	bl	snprintf
	ldr	r3, [r6, #0]
	mov	r1, #6
	ldr	r2, .L110+88
	add	r0, r4, #33
	bl	snprintf
	ldr	r3, [r5, #0]
	mov	r1, #6
	ldr	r2, .L110+88
	add	r0, r4, #39
	bl	snprintf
	ldr	r6, .L110+104
	ldr	r3, [r7, #0]
	ldr	r5, .L110+108
	str	r3, [sp, #0]
	ldr	r3, [r6, #0]
	ldr	r8, .L110+112
	str	r3, [sp, #4]
	ldr	r3, [r5, #0]
	mov	r1, #17
	str	r3, [sp, #8]
	ldr	r2, .L110+84
	ldr	r3, [r8, #0]
	add	r0, r4, #152
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	ldr	r3, [r8, #0]
	add	r0, r4, #169
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	ldr	r3, [r7, #0]
	add	r0, r4, #175
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L110+88
	ldr	r3, [r6, #0]
	add	r0, r4, #181
	bl	snprintf
	ldr	r2, .L110+88
	ldr	r3, [r5, #0]
	add	r0, r4, #187
	mov	r1, #6
.L96:
	bl	snprintf
.L53:
.LBE13:
	ldr	r3, .L110+116
	ldr	r2, .L110+120
	ldr	r0, [r3, #0]
	mov	r1, #87
	bl	e_SHARED_start_device_communication
	ldr	r3, .L110+40
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L26
.L50:
	bl	good_key_beep
	ldrsh	r2, [r4, #0]
	ldr	r3, .L110+64
.L100:
	str	r2, [r3, #0]
	ldr	r3, .L110+116
	mov	r1, #82
	ldr	r0, [r3, #0]
	ldr	r2, .L110+120
	bl	e_SHARED_start_device_communication
	b	.L26
.L41:
	ldr	r3, .L110+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L59
.L70:
	.word	.L60
	.word	.L59
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
.L60:
	ldr	r0, .L110+28
	bl	process_bool
	mov	r0, #0
	bl	Redraw_Screen
	b	.L71
.L61:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L110+76
	b	.L97
.L62:
	ldr	r1, .L110+80
	mov	r3, #1
	mov	r0, r4
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L97:
	mov	r2, #0
	mov	r3, #255
.L98:
	bl	process_uns32
	b	.L71
.L63:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L110+92
	b	.L97
.L64:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L110+96
	b	.L97
.L65:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L110+24
	mov	r3, #24
	b	.L98
.L66:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L110+112
	b	.L97
.L67:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L110+100
	b	.L97
.L68:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L110+104
	b	.L97
.L69:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L110+108
	b	.L97
.L59:
	bl	bad_key_beep
.L71:
	bl	Refresh_Screen
	b	.L26
.L39:
	ldr	r3, .L110+8
	ldrsh	r3, [r3, #0]
	sub	r2, r3, #2
	cmp	r2, #10
	ldrls	pc, [pc, r2, asl #2]
	b	.L72
.L78:
	.word	.L73
	.word	.L73
	.word	.L73
	.word	.L73
	.word	.L91
	.word	.L86
	.word	.L86
	.word	.L86
	.word	.L86
	.word	.L76
	.word	.L77
.L73:
	ldr	r2, .L110+64
	mov	r0, #0
	str	r3, [r2, #0]
	b	.L102
.L77:
	ldr	r3, .L110+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #1
	movne	r1, r0
	beq	.L87
	b	.L103
.L76:
	ldr	r3, .L110+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L87
.L72:
	mov	r0, #1
.L36:
	bl	CURSOR_Up
	b	.L26
.L35:
	ldr	r3, .L110+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L38
.L90:
	.word	.L85
	.word	.L38
	.word	.L86
	.word	.L86
	.word	.L86
	.word	.L86
	.word	.L87
	.word	.L88
	.word	.L88
	.word	.L88
	.word	.L88
	.word	.L89
.L85:
	ldr	r3, .L110+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L38
.L91:
	ldr	r3, .L110+64
	ldr	r2, [r3, #0]
	sub	r2, r2, #2
	cmp	r2, #3
	movhi	r2, #2
	bhi	.L104
	b	.L93
.L86:
	ldr	r2, .L110+64
	mov	r0, #6
	str	r3, [r2, #0]
	b	.L102
.L87:
	ldr	r3, .L110+64
	ldr	r2, [r3, #0]
	sub	r2, r2, #7
	cmp	r2, #3
	bls	.L93
	mov	r2, #7
.L104:
	str	r2, [r3, #0]
.L93:
	ldr	r0, [r3, #0]
	b	.L102
.L88:
	ldr	r2, .L110+64
	mov	r0, #11
	str	r3, [r2, #0]
.L102:
	mov	r1, #1
.L103:
	bl	CURSOR_Select
	b	.L26
.L89:
	bl	bad_key_beep
	b	.L26
.L38:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L26
.L40:
	ldr	r3, .L110+124
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L26
.L34:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L26:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L111:
	.align	2
.L110:
	.word	GuiLib_CurStructureNdx
	.word	726
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ENDHCPName
	.word	GuiVar_GroupName
	.word	FDTO_EN_PROGRAMMING_redraw_screen
	.word	GuiVar_ENNetmask
	.word	GuiVar_ENObtainIPAutomatically
	.word	FDTO_EN_PROGRAMMING_process_device_exchange_key
	.word	36868
	.word	.LANCHOR3
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36867
	.word	36870
	.word	FDTO_e_SHARED_show_obtain_ip_automatically_dropdown
	.word	FDTO_e_SHARED_show_subnet_dropdown
	.word	.LANCHOR1
	.word	dev_state
	.word	.LC1
	.word	GuiVar_ENIPAddress_1
	.word	GuiVar_ENIPAddress_2
	.word	.LC4
	.word	.LC5
	.word	GuiVar_ENIPAddress_3
	.word	GuiVar_ENIPAddress_4
	.word	GuiVar_ENGateway_2
	.word	GuiVar_ENGateway_3
	.word	GuiVar_ENGateway_4
	.word	GuiVar_ENGateway_1
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	GuiVar_MenuScreenToShow
.LFE6:
	.size	EN_PROGRAMMING_process_screen, .-EN_PROGRAMMING_process_screen
	.section	.bss.g_EN_PROGRAMMING_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_EN_PROGRAMMING_previous_cursor_pos, %object
	.size	g_EN_PROGRAMMING_previous_cursor_pos, 4
g_EN_PROGRAMMING_previous_cursor_pos:
	.space	4
	.section	.bss.EN_PROGRAMMING_setup_mode_read_pending,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	EN_PROGRAMMING_setup_mode_read_pending, %object
	.size	EN_PROGRAMMING_setup_mode_read_pending, 4
EN_PROGRAMMING_setup_mode_read_pending:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"\000"
.LC1:
	.ascii	"not set\000"
.LC2:
	.ascii	"\015\012\000"
.LC3:
	.ascii	"unknown\000"
.LC4:
	.ascii	"%d.%d.%d.%d\000"
.LC5:
	.ascii	"%d\000"
	.section	.bss.EN_PROGRAMMING_querying_device,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	EN_PROGRAMMING_querying_device, %object
	.size	EN_PROGRAMMING_querying_device, 4
EN_PROGRAMMING_querying_device:
	.space	4
	.section	.bss.g_EN_PROGRAMMING_port,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_EN_PROGRAMMING_port, %object
	.size	g_EN_PROGRAMMING_port, 4
g_EN_PROGRAMMING_port:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI0-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI2-.LFB6
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_en_programming.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x8e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF6
	.byte	0x1
	.4byte	.LASF7
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x147
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x168
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1b4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.byte	0xc3
	.byte	0x1
	.uleb128 0x5
	.4byte	0x2a
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x6
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1a7
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.byte	0x52
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1eb
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB5
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB6
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI3
	.4byte	.LFE6
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF8:
	.ascii	"FDTO_EN_PROGRAMMING_redraw_screen\000"
.LASF0:
	.ascii	"EN_PROGRAMMING_initialize_guivars\000"
.LASF3:
	.ascii	"set_en_programming_struct_from_guivars\000"
.LASF5:
	.ascii	"EN_PROGRAMMING_process_screen\000"
.LASF1:
	.ascii	"FDTO_EN_PROGRAMMING_process_device_exchange_key\000"
.LASF4:
	.ascii	"FDTO_EN_PROGRAMMING_draw_screen\000"
.LASF7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_en_programming.c\000"
.LASF2:
	.ascii	"get_guivars_from_en_programming_struct\000"
.LASF6:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
