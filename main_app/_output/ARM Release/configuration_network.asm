	.file	"configuration_network.c"
	.text
.Ltext0:
	.section	.text.NETWORK_CONFIG_set_start_of_irri_day,"ax",%progbits
	.align	2
	.type	NETWORK_CONFIG_set_start_of_irri_day, %function
NETWORK_CONFIG_set_start_of_irri_day:
.LFB1:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	lr, .L2
	sub	sp, sp, #40
.LCFI1:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r0, .L2+4
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r3, [sp, #24]
	add	r3, r0, #144
	str	r3, [sp, #28]
	mov	r3, #1
	ldr	r1, .L2+8
	str	r3, [sp, #32]
	ldr	r3, .L2+12
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	mov	r1, ip
	mov	r2, #0
	ldr	r3, .L2+16
	add	r0, r0, #8
	str	lr, [sp, #0]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L3:
	.align	2
.L2:
	.word	72000
	.word	.LANCHOR0
	.word	57353
	.word	.LC0
	.word	85800
.LFE1:
	.size	NETWORK_CONFIG_set_start_of_irri_day, .-NETWORK_CONFIG_set_start_of_irri_day
	.section	.text.NETWORK_CONFIG_set_water_units,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_set_water_units
	.type	NETWORK_CONFIG_set_water_units, %function
NETWORK_CONFIG_set_water_units:
.LFB9:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	sub	sp, sp, #40
.LCFI3:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	lr, r0
	str	r3, [sp, #20]
	ldr	r0, .L5
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r3, [sp, #24]
	add	r3, r0, #144
	str	r3, [sp, #28]
	mov	r3, #12
	ldr	r1, .L5+4
	str	r3, [sp, #32]
	ldr	r3, .L5+8
	mov	ip, #0
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	mov	r1, lr
	mov	r2, ip
	mov	r3, #1
	add	r0, r0, #96
	str	ip, [sp, #0]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L6:
	.align	2
.L5:
	.word	.LANCHOR0
	.word	57373
	.word	.LC1
.LFE9:
	.size	NETWORK_CONFIG_set_water_units, .-NETWORK_CONFIG_set_water_units
	.section	.text.NETWORK_CONFIG_set_time_zone,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_set_time_zone
	.type	NETWORK_CONFIG_set_time_zone, %function
NETWORK_CONFIG_set_time_zone:
.LFB3:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI4:
	sub	sp, sp, #40
.LCFI5:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r0, .L8
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r3, [sp, #24]
	add	r3, r0, #144
	str	r3, [sp, #28]
	mov	r3, #3
	ldr	r1, .L8+4
	str	r3, [sp, #32]
	ldr	r3, .L8+8
	mov	lr, #2
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	mov	r1, ip
	mov	r2, #0
	mov	r3, #7
	add	r0, r0, #16
	str	lr, [sp, #0]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L9:
	.align	2
.L8:
	.word	.LANCHOR0
	.word	57345
	.word	.LC2
.LFE3:
	.size	NETWORK_CONFIG_set_time_zone, .-NETWORK_CONFIG_set_time_zone
	.section	.text.NETWORK_CONFIG_set_send_flow_recording,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_set_send_flow_recording
	.type	NETWORK_CONFIG_set_send_flow_recording, %function
NETWORK_CONFIG_set_send_flow_recording:
.LFB7:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI6:
	mov	ip, r1
	ldr	r1, .L11
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L11+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, lr
	str	r3, [sp, #16]
	add	r3, r0, #144
	str	r3, [sp, #20]
	mov	r3, #9
	str	r3, [sp, #24]
	ldr	r3, .L11+8
	mov	r2, #0
	str	r3, [sp, #28]
	add	r0, r0, #100
	mov	r3, ip
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L12:
	.align	2
.L11:
	.word	57370
	.word	.LANCHOR0
	.word	.LC3
.LFE7:
	.size	NETWORK_CONFIG_set_send_flow_recording, .-NETWORK_CONFIG_set_send_flow_recording
	.section	.text.NETWORK_CONFIG_set_electrical_limit,"ax",%progbits
	.align	2
	.type	NETWORK_CONFIG_set_electrical_limit, %function
NETWORK_CONFIG_set_electrical_limit:
.LFB4:
	@ args = 12, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #12
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI7:
	mov	r8, r0
	sub	sp, sp, #72
.LCFI8:
	mov	r4, r1
	mov	r7, r2
	mov	r6, r3
	bhi	.L14
.LBB12:
	add	r5, sp, #40
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r0, r5
	mov	r1, #32
	ldr	r2, .L16
	ldr	r3, .L16+4
	bl	snprintf
	mov	r2, #6
	stmia	sp, {r2, r7}
	ldr	r2, [sp, #96]
	ldr	r3, .L16+8
	str	r2, [sp, #16]
	ldr	r2, [sp, #100]
	add	r0, r4, #5
	str	r2, [sp, #20]
	ldr	r2, [sp, #104]
	add	r4, r4, #57344
	str	r2, [sp, #24]
	add	r2, r3, #144
	str	r2, [sp, #28]
	mov	r2, #7
	add	r4, r4, #13
	str	r2, [sp, #32]
	add	r0, r3, r0, asl #2
	mov	r1, r8
	mov	r2, #1
	mov	r3, #8
	str	r4, [sp, #8]
	str	r6, [sp, #12]
	str	r5, [sp, #36]
	bl	SHARED_set_uint32_controller
	b	.L13
.L14:
.LBE12:
	ldr	r0, .L16+12
	mov	r1, #480
	bl	Alert_index_out_of_range_with_filename
.L13:
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L17:
	.align	2
.L16:
	.word	.LC4
	.word	.LC5
	.word	.LANCHOR0
	.word	.LC6
.LFE4:
	.size	NETWORK_CONFIG_set_electrical_limit, .-NETWORK_CONFIG_set_electrical_limit
	.section	.text.NETWORK_CONFIG_set_network_id,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_set_network_id
	.type	NETWORK_CONFIG_set_network_id, %function
NETWORK_CONFIG_set_network_id:
.LFB0:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI9:
	sub	sp, sp, #40
.LCFI10:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L20
	str	r3, [sp, #24]
	ldr	r3, .L20+4
	mov	ip, #0
	str	r3, [sp, #28]
	ldr	r3, .L20+8
	mov	lr, r0
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	ldr	r0, .L20+12
	mov	r1, lr
	mov	r2, ip
	mvn	r3, #0
	str	ip, [sp, #0]
	str	ip, [sp, #32]
	bl	SHARED_set_uint32_controller
	subs	r4, r0, #0
	beq	.L19
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
.L19:
	mov	r0, r4
	add	sp, sp, #40
	ldmfd	sp!, {r4, pc}
.L21:
	.align	2
.L20:
	.word	57352
	.word	.LANCHOR0+144
	.word	.LC7
	.word	.LANCHOR0
.LFE0:
	.size	NETWORK_CONFIG_set_network_id, .-NETWORK_CONFIG_set_network_id
	.section	.text.NETWORK_CONFIG_set_controller_name,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_set_controller_name
	.type	NETWORK_CONFIG_set_controller_name, %function
NETWORK_CONFIG_set_controller_name:
.LFB8:
	@ args = 12, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #11
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI11:
	mov	r6, r0
	sub	sp, sp, #64
.LCFI12:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L23
.LBB15:
	add	r5, sp, #32
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r0, r5
	mov	r1, #32
	ldr	r2, .L25
	ldr	r3, .L25+4
	bl	snprintf
	ldr	r3, .L25+8
	ldr	r0, .L25+12
	stmia	sp, {r3, r7}
	ldr	r3, [sp, #88]
	mov	r1, #48
	str	r3, [sp, #8]
	ldr	r3, [sp, #92]
	mla	r0, r1, r4, r0
	str	r3, [sp, #12]
	ldr	r3, [sp, #96]
	mov	r2, r6
	str	r3, [sp, #16]
	ldr	r3, .L25+16
	str	r5, [sp, #28]
	str	r3, [sp, #20]
	mov	r3, #11
	str	r3, [sp, #24]
	mov	r3, r8
	bl	SHARED_set_string_controller
	b	.L24
.L23:
.LBE15:
	ldr	r0, .L25+20
	mov	r1, #644
	bl	Alert_index_out_of_range_with_filename
	mov	r0, #0
.L24:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L26:
	.align	2
.L25:
	.word	.LC4
	.word	.LC8
	.word	57372
	.word	.LANCHOR0+148
	.word	.LANCHOR0+144
	.word	.LC6
.LFE8:
	.size	NETWORK_CONFIG_set_controller_name, .-NETWORK_CONFIG_set_controller_name
	.section	.text.network_config_updater,"ax",%progbits
	.align	2
	.type	network_config_updater, %function
network_config_updater:
.LFB11:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI13:
	sub	sp, sp, #20
.LCFI14:
	mov	r4, r0
	bl	FLOWSENSE_get_controller_index
	mov	r5, r0
	add	r0, sp, #12
	bl	EPSON_obtain_latest_time_and_date
	cmp	r4, #7
	bne	.L28
	ldr	r0, .L61
	mov	r1, r4
	bl	Alert_Message_va
	b	.L27
.L28:
	ldr	r0, .L61+4
	mov	r1, #7
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L30
	ldr	r3, .L61+8
	mvn	r2, #0
	str	r2, [r3, #144]
	ldr	r3, .L61+12
	mov	r2, #1
	str	r2, [r3, #108]
	b	.L31
.L30:
	cmp	r4, #1
	bne	.L32
.L31:
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
	b	.L33
.L32:
	cmp	r4, #2
	bne	.L34
.L33:
	ldr	r0, .L61+16
	mov	r1, #0
	mov	r2, #576
	bl	memset
	ldr	r6, .L61+20
	mov	r4, #0
	mov	r7, #1
.L35:
	mov	r1, r4
	ldr	r0, .L61+24
	mov	r2, #0
	mov	r3, #11
	add	r4, r4, #1
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	NETWORK_CONFIG_set_controller_name
	cmp	r4, #12
	bne	.L35
	b	.L36
.L34:
	cmp	r4, #3
	bne	.L37
.L36:
	ldr	r3, .L61+8
	mov	r2, #0
	str	r2, [r3, #724]
	b	.L38
.L37:
	cmp	r4, #4
	bne	.L39
.L38:
	ldr	r3, .L61+8
	ldr	r3, [r3, #16]
	cmp	r3, #3
	beq	.L53
	cmp	r3, #4
	beq	.L54
	cmp	r3, #5
	moveq	r0, #6
	bne	.L43
	b	.L40
.L54:
	mov	r0, #5
	b	.L40
.L53:
	mov	r0, #4
.L40:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L61+20
	mov	r1, #0
	str	r3, [sp, #4]
	mov	r2, #11
	mov	r3, r5
	bl	NETWORK_CONFIG_set_time_zone
	b	.L43
.L39:
	cmp	r4, #5
	bne	.L44
.L43:
	ldr	r3, .L61+28
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L61+32
	mov	r3, #420
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L61+36
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L61+32
	ldr	r3, .L61+40
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, .L61+44
	ldr	r5, .L61+48
	mov	r4, #0
.L45:
	mul	r6, r7, r4
	mov	r1, #0
	add	r0, r6, #548
	ldr	r2, .L61+52
	add	r0, r5, r0
	add	r6, r6, #9536
	bl	memset
	add	r0, r6, #12
	add	r0, r5, r0
	mov	r1, #0
	ldr	r2, .L61+56
	bl	memset
	add	r4, r4, #1
	cmp	r4, #4
	bne	.L45
	ldr	r1, .L61+60
	mov	r3, #0
.L46:
	add	r2, r1, r3, asl #7
	ldrb	r0, [r2, #140]	@ zero_extendqisi2
	add	r3, r3, #1
	bic	r0, r0, #15
	cmp	r3, #2112
	strb	r0, [r2, #140]
	bne	.L46
	ldr	r4, .L61+48
	ldr	r5, .L61+64
.L48:
	ldr	r0, [r4, #16]
	cmp	r0, #0
	beq	.L47
	bl	SYSTEM_PRESERVES_request_a_resync
.L47:
	add	r4, r4, #14208
	add	r4, r4, #16
	cmp	r4, r5
	bne	.L48
	ldr	r3, .L61+36
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L61+28
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L49
.L44:
	cmp	r4, #6
	bne	.L50
.L49:
	ldr	r3, .L61+36
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L61+32
	mov	r3, #460
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, .L61+44
	ldr	r5, .L61+48
	mov	r4, #0
.L51:
	mul	r6, r7, r4
	mov	r1, #0
	add	r0, r6, #548
	ldr	r2, .L61+52
	add	r0, r5, r0
	add	r6, r6, #9536
	bl	memset
	add	r0, r6, #12
	add	r0, r5, r0
	mov	r1, #0
	ldr	r2, .L61+56
	bl	memset
	add	r4, r4, #1
	cmp	r4, #4
	bne	.L51
	ldr	r4, .L61+60
	ldr	sl, .L61+68
	mov	r5, #0
	mov	r6, r5
	mov	r7, r4
.L52:
	mov	r8, r5, asl #7
	add	r0, r8, #76
	strh	sl, [r4, #72]	@ movhi
	strh	r6, [r4, #74]	@ movhi
	add	r0, r7, r0
	bl	nm_init_station_report_data_record
	ldrh	r3, [sp, #16]
	mov	r0, #0
	strh	r3, [r4, #110]	@ movhi
	ldr	r3, [sp, #12]
	mov	r1, r0
	str	r3, [r4, #76]
	mov	r2, r0
	bl	HMSToTime
	add	r5, r5, #1
	str	r0, [r4, #124]
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	bl	HMSToTime
	ldr	r2, .L61+72
	str	r0, [r4, #128]
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	ldr	r2, .L61+72
	strh	r0, [r4, #132]	@ movhi
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	strh	r6, [r4, #136]	@ movhi
	strh	r6, [r4, #138]	@ movhi
	mov	r2, #2
	mov	r1, #0
	strh	r0, [r4, #134]	@ movhi
	add	r0, r8, #140
	add	r0, r7, r0
	add	r8, r7, r8
	bl	memset
	ldrb	r2, [r8, #140]	@ zero_extendqisi2
	cmp	r5, #2112
	bic	r2, r2, #48
	strb	r2, [r8, #140]
	ldrh	r2, [r8, #140]
	bic	r2, r2, #960
	strh	r2, [r8, #140]	@ movhi
	ldrb	r2, [r8, #141]	@ zero_extendqisi2
	bic	r2, r2, #16
	strb	r2, [r8, #141]
	strh	r6, [r4, #142]	@ movhi
	add	r4, r4, #128
	bne	.L52
	ldr	r3, .L61+36
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L27
.L50:
	ldr	r0, .L61+76
	bl	Alert_Message
.L27:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L62:
	.align	2
.L61:
	.word	.LC9
	.word	.LC10
	.word	.LANCHOR0
	.word	weather_preserves
	.word	.LANCHOR0+148
	.word	.LANCHOR0+136
	.word	.LANCHOR1
	.word	system_preserves_recursive_MUTEX
	.word	.LC11
	.word	station_preserves_recursive_MUTEX
	.word	422
	.word	14224
	.word	system_preserves
	.word	9000
	.word	4500
	.word	station_preserves
	.word	system_preserves+56896
	.word	5000
	.word	2011
	.word	.LC12
.LFE11:
	.size	network_config_updater, .-network_config_updater
	.section	.text.init_file_configuration_network,"ax",%progbits
	.align	2
	.global	init_file_configuration_network
	.type	init_file_configuration_network, %function
init_file_configuration_network:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI15:
	ldr	r1, .L64
	mov	r2, #728
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r3, .L64+4
	mov	r0, #1
	str	r3, [sp, #8]
	ldr	r3, .L64+8
	mov	r2, #7
	str	r3, [sp, #12]
	ldr	r3, .L64+12
	str	r0, [sp, #16]
	bl	FLASH_FILE_find_or_create_variable_file
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.L65:
	.align	2
.L64:
	.word	.LANCHOR2
	.word	network_config_updater
	.word	network_config_set_default_values
	.word	.LANCHOR0
.LFE12:
	.size	init_file_configuration_network, .-init_file_configuration_network
	.section	.text.save_file_configuration_network,"ax",%progbits
	.align	2
	.global	save_file_configuration_network
	.type	save_file_configuration_network, %function
save_file_configuration_network:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI16:
	ldr	r1, .L67
	mov	r0, #728
	mov	r3, #0
	stmia	sp, {r0, r3}
	ldr	r3, .L67+4
	mov	r0, #1
	mov	r2, #7
	str	r0, [sp, #8]
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L68:
	.align	2
.L67:
	.word	.LANCHOR2
	.word	.LANCHOR0
.LFE13:
	.size	save_file_configuration_network, .-save_file_configuration_network
	.section	.text.__turn_scheduled_irrigation_OFF,"ax",%progbits
	.align	2
	.global	__turn_scheduled_irrigation_OFF
	.type	__turn_scheduled_irrigation_OFF, %function
__turn_scheduled_irrigation_OFF:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI17:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	ldreq	pc, [sp], #4
	mov	r0, #113
	ldr	lr, [sp], #4
	b	FOAL_IRRI_remove_all_stations_from_the_irrigation_lists
.LFE14:
	.size	__turn_scheduled_irrigation_OFF, .-__turn_scheduled_irrigation_OFF
	.section	.text.NETWORK_CONFIG_set_scheduled_irrigation_off,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_set_scheduled_irrigation_off
	.type	NETWORK_CONFIG_set_scheduled_irrigation_off, %function
NETWORK_CONFIG_set_scheduled_irrigation_off:
.LFB2:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI18:
	mov	sl, r3
	ldr	r3, .L73
	sub	sp, sp, #32
.LCFI19:
	ldr	r3, [r3, #12]
	mov	r4, r0
	cmp	r0, #1
	cmpeq	r3, #0
	mov	r5, r1
	mov	r6, r2
	ldr	r8, [sp, #60]
	ldr	r7, [sp, #64]
	bne	.L72
	bl	__turn_scheduled_irrigation_OFF
.L72:
	ldr	r3, .L73+4
	mov	r1, r4
	stmia	sp, {r3, r6, sl}
	ldr	r3, .L73+8
	mov	r2, #1
	str	r3, [sp, #20]
	mov	r3, #2
	str	r3, [sp, #24]
	ldr	r3, .L73+12
	ldr	r0, .L73+16
	str	r3, [sp, #28]
	mov	r3, r5
	str	r8, [sp, #12]
	str	r7, [sp, #16]
	bl	SHARED_set_bool_controller
	ldr	r0, .L73+20
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	postBackground_Calculation_Event
.L74:
	.align	2
.L73:
	.word	.LANCHOR0
	.word	57354
	.word	.LANCHOR0+144
	.word	.LC13
	.word	.LANCHOR0+12
	.word	4369
.LFE2:
	.size	NETWORK_CONFIG_set_scheduled_irrigation_off, .-NETWORK_CONFIG_set_scheduled_irrigation_off
	.section	.text.network_config_set_default_values,"ax",%progbits
	.align	2
	.type	network_config_set_default_values, %function
network_config_set_default_values:
.LFB15:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI20:
	ldr	r5, .L78
	sub	sp, sp, #60
.LCFI21:
	bl	FLOWSENSE_get_controller_index
	ldr	r8, .L78+4
	mov	r4, #0
	add	r7, r5, #136
	ldr	r1, [r8, #0]
	str	r4, [r5, #4]
	str	r4, [r5, #68]
	strh	r4, [r5, #108]	@ movhi
	str	r4, [r5, #104]
	strh	r4, [r5, #110]	@ movhi
	str	r4, [r5, #112]
	str	r4, [r5, #116]
	str	r4, [r5, #120]
	str	r4, [r5, #124]
	str	r4, [r5, #128]
	str	r4, [r5, #132]
	add	sl, r5, #144
.LBB16:
.LBB17:
	add	r9, sp, #48
.LBE17:
.LBE16:
	mov	r6, r0
	mov	r0, r7
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r5, #140
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r5, #724
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	mov	r0, sl
	bl	SHARED_set_all_32_bit_change_bits
	mov	r1, r4
	mov	r2, #11
	mov	r3, r6
	mov	r0, r4
	stmia	sp, {r4, r7}
	bl	NETWORK_CONFIG_set_network_id
	ldr	r0, .L78+8
	mov	r1, r4
	mov	r2, #11
	mov	r3, r6
	stmia	sp, {r4, r7}
	bl	NETWORK_CONFIG_set_start_of_irri_day
	mov	r0, r4
	mov	r1, r4
	mov	r2, #11
	mov	r3, r6
	stmia	sp, {r4, r7}
	bl	NETWORK_CONFIG_set_scheduled_irrigation_off
	mov	r0, #2
	mov	r1, r4
	mov	r2, #11
	mov	r3, r6
	stmia	sp, {r4, r7}
	bl	NETWORK_CONFIG_set_time_zone
	mov	r0, r4
	mov	r1, r4
	mov	r2, #11
	mov	r3, r6
	stmia	sp, {r4, r7}
	bl	NETWORK_CONFIG_set_send_flow_recording
	mov	r1, #3
	mov	r2, #7
	mov	r3, #15
	str	r3, [sp, #56]
.LBB22:
.LBB18:
	stmia	sp, {r1, r2, r3, r4}
	ldr	r3, .L78+12
	mov	r8, #11
	str	r3, [sp, #16]
	mov	r3, #5
	str	r3, [sp, #40]
	ldr	r3, .L78+16
	str	r8, [sp, #20]
.LBE18:
.LBE22:
	str	r1, [sp, #48]
	str	r2, [sp, #52]
.LBB23:
.LBB19:
	str	r6, [sp, #24]
	str	r4, [sp, #28]
	str	r7, [sp, #32]
	str	sl, [sp, #36]
	str	r3, [sp, #44]
	add	r0, r5, #72
	ldmia	r9, {r1, r2, r3}
	bl	SHARED_set_DLS_controller
	mov	r3, #8
	str	r3, [sp, #56]
.LBE19:
.LBE23:
.LBB24:
.LBB25:
	str	r3, [sp, #8]
	ldr	r3, .L78+20
.LBE25:
.LBE24:
.LBB28:
.LBB20:
	str	r8, [sp, #48]
.LBE20:
.LBE28:
.LBB29:
.LBB26:
	str	r3, [sp, #16]
	mov	r3, #6
	str	r3, [sp, #40]
	ldr	r3, .L78+24
	str	r8, [sp, #0]
	str	r8, [sp, #20]
.LBE26:
.LBE29:
.LBB30:
.LBB21:
	str	r4, [sp, #52]
.LBE21:
.LBE30:
.LBB31:
.LBB27:
	str	r4, [sp, #4]
	str	r4, [sp, #12]
	str	r6, [sp, #24]
	str	r4, [sp, #28]
	str	r7, [sp, #32]
	str	sl, [sp, #36]
	str	r3, [sp, #44]
	add	r0, r5, #84
	ldmia	r9, {r1, r2, r3}
	bl	SHARED_set_DLS_controller
.LBE27:
.LBE31:
	add	r0, r5, #148
	mov	r1, r4
	mov	r2, #576
	bl	memset
	mov	r8, r4
.L76:
	mov	r5, #0
	mov	r1, r4
	mov	r0, #6
	mov	r2, r5
	mov	r3, #11
	str	r6, [sp, #0]
	str	r8, [sp, #4]
	str	r7, [sp, #8]
	bl	NETWORK_CONFIG_set_electrical_limit
	mov	r1, r4
	ldr	r0, .L78+28
	mov	r2, r5
	mov	r3, #11
	add	r4, r4, #1
	stmia	sp, {r6, r8}
	str	r7, [sp, #8]
	bl	NETWORK_CONFIG_set_controller_name
	cmp	r4, #12
	bne	.L76
	ldr	r3, .L78+32
	mov	r0, r5
	str	r3, [sp, #4]
	mov	r1, r5
	mov	r2, #11
	mov	r3, r6
	str	r5, [sp, #0]
	bl	NETWORK_CONFIG_set_water_units
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L79:
	.align	2
.L78:
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	72000
	.word	57355
	.word	.LC14
	.word	57356
	.word	.LC15
	.word	.LANCHOR1
	.word	.LANCHOR0+136
.LFE15:
	.size	network_config_set_default_values, .-network_config_set_default_values
	.section	.text.NETWORK_CONFIG_get_network_id,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_network_id
	.type	NETWORK_CONFIG_get_network_id, %function
NETWORK_CONFIG_get_network_id:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L81
	ldr	r0, [r3, #0]
	bx	lr
.L82:
	.align	2
.L81:
	.word	.LANCHOR0
.LFE17:
	.size	NETWORK_CONFIG_get_network_id, .-NETWORK_CONFIG_get_network_id
	.section	.text.NETWORK_CONFIG_get_start_of_irrigation_day,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_start_of_irrigation_day
	.type	NETWORK_CONFIG_get_start_of_irrigation_day, %function
NETWORK_CONFIG_get_start_of_irrigation_day:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI22:
	ldr	r3, .L84
	ldr	r0, .L84+4
	str	r3, [sp, #0]
	add	r3, r0, #136
	str	r3, [sp, #4]
	ldr	r3, .L84+8
	mov	r1, #0
	str	r3, [sp, #8]
	ldr	r2, .L84+12
	ldr	r3, .L84+16
	add	r0, r0, #8
	bl	SHARED_get_uint32
	ldmfd	sp!, {r1, r2, r3, pc}
.L85:
	.align	2
.L84:
	.word	NETWORK_CONFIG_set_start_of_irri_day
	.word	.LANCHOR0
	.word	.LC0
	.word	85800
	.word	72000
.LFE18:
	.size	NETWORK_CONFIG_get_start_of_irrigation_day, .-NETWORK_CONFIG_get_start_of_irrigation_day
	.section	.text.NETWORK_CONFIG_get_controller_off,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_controller_off
	.type	NETWORK_CONFIG_get_controller_off, %function
NETWORK_CONFIG_get_controller_off:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L87
	ldr	r2, .L87+4
	stmfd	sp!, {r0, lr}
.LCFI23:
	mov	r1, #0
	str	r2, [sp, #0]
	add	r0, r3, #12
	ldr	r2, .L87+8
	add	r3, r3, #136
	bl	SHARED_get_bool
	ldmfd	sp!, {r3, pc}
.L88:
	.align	2
.L87:
	.word	.LANCHOR0
	.word	.LC13
	.word	NETWORK_CONFIG_set_scheduled_irrigation_off
.LFE19:
	.size	NETWORK_CONFIG_get_controller_off, .-NETWORK_CONFIG_get_controller_off
	.section	.text.NETWORK_CONFIG_get_time_zone,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_time_zone
	.type	NETWORK_CONFIG_get_time_zone, %function
NETWORK_CONFIG_get_time_zone:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI24:
	ldr	r3, .L90
	ldr	r0, .L90+4
	str	r3, [sp, #0]
	add	r3, r0, #136
	str	r3, [sp, #4]
	ldr	r3, .L90+8
	mov	r1, #0
	str	r3, [sp, #8]
	mov	r2, #7
	mov	r3, #2
	add	r0, r0, #16
	bl	SHARED_get_uint32
	ldmfd	sp!, {r1, r2, r3, pc}
.L91:
	.align	2
.L90:
	.word	NETWORK_CONFIG_set_time_zone
	.word	.LANCHOR0
	.word	.LC2
.LFE20:
	.size	NETWORK_CONFIG_get_time_zone, .-NETWORK_CONFIG_get_time_zone
	.section	.text.NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone
	.type	NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone, %function
NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI25:
	bl	NETWORK_CONFIG_get_time_zone
	cmp	r0, #0
	cmpne	r0, #3
	beq	.L94
	subs	r0, r0, #7
	movne	r0, #1
	ldr	pc, [sp], #4
.L94:
	mov	r0, #0
	ldr	pc, [sp], #4
.LFE21:
	.size	NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone, .-NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone
	.section	.text.NETWORK_CONFIG_get_electrical_limit,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_electrical_limit
	.type	NETWORK_CONFIG_get_electrical_limit, %function
NETWORK_CONFIG_get_electrical_limit:
.LFB22:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #11
	stmfd	sp!, {r4, r5, lr}
.LCFI26:
	mov	r4, r0
	sub	sp, sp, #68
.LCFI27:
	bhi	.L96
	add	r5, sp, #20
	add	r3, r0, #1
	str	r3, [sp, #0]
	mov	r0, r5
	mov	r1, #48
	ldr	r2, .L98
	ldr	r3, .L98+4
	bl	snprintf
	mov	r2, #8
	mov	ip, #6
	ldr	r3, .L98+8
	stmia	sp, {r2, ip}
	ldr	r2, .L98+12
	add	r0, r4, #5
	str	r2, [sp, #8]
	add	r2, r3, #136
	str	r2, [sp, #12]
	add	r0, r3, r0, asl #2
	mov	r1, r4
	mov	r2, #12
	mov	r3, #1
	str	r5, [sp, #16]
	bl	SHARED_get_uint32_from_array
	b	.L97
.L96:
	ldr	r0, .L98+16
	ldr	r1, .L98+20
	bl	Alert_index_out_of_range_with_filename
	mov	r0, #6
.L97:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, pc}
.L99:
	.align	2
.L98:
	.word	.LC4
	.word	.LC5
	.word	.LANCHOR0
	.word	NETWORK_CONFIG_set_electrical_limit
	.word	.LC11
	.word	1031
.LFE22:
	.size	NETWORK_CONFIG_get_electrical_limit, .-NETWORK_CONFIG_get_electrical_limit
	.section	.text.NETWORK_CONFIG_get_dls_ptr,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_dls_ptr
	.type	NETWORK_CONFIG_get_dls_ptr, %function
NETWORK_CONFIG_get_dls_ptr:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L103
	cmp	r0, #1
	add	r2, r3, #12
	movne	r0, r2
	moveq	r0, r3
	bx	lr
.L104:
	.align	2
.L103:
	.word	.LANCHOR0+72
.LFE23:
	.size	NETWORK_CONFIG_get_dls_ptr, .-NETWORK_CONFIG_get_dls_ptr
	.section	.text.NETWORK_CONFIG_get_send_flow_recording,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_send_flow_recording
	.type	NETWORK_CONFIG_get_send_flow_recording, %function
NETWORK_CONFIG_get_send_flow_recording:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L106
	ldr	r2, .L106+4
	stmfd	sp!, {r0, lr}
.LCFI28:
	mov	r1, #0
	str	r2, [sp, #0]
	add	r0, r3, #100
	ldr	r2, .L106+8
	add	r3, r3, #136
	bl	SHARED_get_bool
	ldmfd	sp!, {r3, pc}
.L107:
	.align	2
.L106:
	.word	.LANCHOR0
	.word	.LC3
	.word	NETWORK_CONFIG_set_send_flow_recording
.LFE24:
	.size	NETWORK_CONFIG_get_send_flow_recording, .-NETWORK_CONFIG_get_send_flow_recording
	.section	.text.NETWORK_CONFIG_get_controller_name,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_controller_name
	.type	NETWORK_CONFIG_get_controller_name, %function
NETWORK_CONFIG_get_controller_name:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, r1
	ldr	r1, .L109
	mov	ip, #48
	stmfd	sp!, {r4, lr}
.LCFI29:
	mla	r1, ip, r2, r1
	mov	r4, r0
	mov	r2, r3
	bl	strlcpy
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L110:
	.align	2
.L109:
	.word	.LANCHOR0+148
.LFE25:
	.size	NETWORK_CONFIG_get_controller_name, .-NETWORK_CONFIG_get_controller_name
	.section	.text.NETWORK_CONFIG_get_controller_name_str_for_ui,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_controller_name_str_for_ui
	.type	NETWORK_CONFIG_get_controller_name_str_for_ui, %function
NETWORK_CONFIG_get_controller_name_str_for_ui:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, sl, lr}
.LCFI30:
	ldr	sl, .L117
	mov	r5, #48
	mul	r8, r5, r0
	add	r6, sl, #148
	add	r6, r6, r8
	mov	r7, r0
	mov	r4, r1
	mov	r0, r6
	ldr	r1, .L117+4
	mov	r2, r5
	bl	strncmp
	cmp	r0, #0
	beq	.L112
	add	r8, sl, r8
	ldrb	r3, [r8, #148]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L113
.L112:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #1
	bne	.L114
	mov	r1, #0
	add	r0, r0, #900
	bl	GuiLib_GetTextPtr
	add	r7, r7, #65
	mov	r1, #48
	ldr	r2, .L117+8
	str	r7, [sp, #0]
	mov	r3, r0
	mov	r0, r4
	bl	snprintf
	b	.L115
.L114:
	mov	r1, #0
	ldr	r0, .L117+12
	bl	GuiLib_GetTextPtr
	mov	r2, #48
	mov	r1, r0
	mov	r0, r4
	b	.L116
.L113:
	mov	r0, r4
	mov	r1, r6
	mov	r2, r5
.L116:
	bl	strlcpy
.L115:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, pc}
.L118:
	.align	2
.L117:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LC16
	.word	1086
.LFE26:
	.size	NETWORK_CONFIG_get_controller_name_str_for_ui, .-NETWORK_CONFIG_get_controller_name_str_for_ui
	.section	.text.NETWORK_CONFIG_get_water_units,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_water_units
	.type	NETWORK_CONFIG_get_water_units, %function
NETWORK_CONFIG_get_water_units:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI31:
	ldr	r3, .L120
	ldr	r0, .L120+4
	str	r3, [sp, #0]
	add	r3, r0, #136
	str	r3, [sp, #4]
	ldr	r3, .L120+8
	mov	r1, #0
	str	r3, [sp, #8]
	mov	r2, #1
	mov	r3, r1
	add	r0, r0, #96
	bl	SHARED_get_uint32
	ldmfd	sp!, {r1, r2, r3, pc}
.L121:
	.align	2
.L120:
	.word	NETWORK_CONFIG_set_water_units
	.word	.LANCHOR0
	.word	.LC1
.LFE27:
	.size	NETWORK_CONFIG_get_water_units, .-NETWORK_CONFIG_get_water_units
	.section	.text.NETWORK_CONFIG_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_get_change_bits_ptr
	.type	NETWORK_CONFIG_get_change_bits_ptr, %function
NETWORK_CONFIG_get_change_bits_ptr:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L123
	add	r1, r3, #136
	add	r2, r3, #140
	add	r3, r3, #144
	b	SHARED_get_32_bit_change_bits_ptr
.L124:
	.align	2
.L123:
	.word	.LANCHOR0
.LFE28:
	.size	NETWORK_CONFIG_get_change_bits_ptr, .-NETWORK_CONFIG_get_change_bits_ptr
	.section	.text.NETWORK_CONFIG_build_data_to_send,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_build_data_to_send
	.type	NETWORK_CONFIG_build_data_to_send, %function
NETWORK_CONFIG_build_data_to_send:
.LFB16:
	@ args = 4, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI32:
	mov	r4, r0
	sub	sp, sp, #40
.LCFI33:
	mov	r0, r3
	mov	r8, r1
	mov	r5, r2
	mov	sl, r3
	bl	NETWORK_CONFIG_get_change_bits_ptr
	subs	r9, r0, #0
	beq	.L126
	ldr	r2, [sp, #76]
	ldr	r3, [r5, #0]
	cmp	r8, r2
	movcs	ip, #0
	movcc	ip, #1
	cmp	r3, #1
	movne	ip, #0
	cmp	ip, #0
	moveq	r9, ip
	beq	.L126
	mov	r1, #4
	mov	ip, #0
	mov	r2, r1
	add	r3, sp, #36
	mov	r0, r4
	str	ip, [sp, #32]
	str	ip, [sp, #28]
	bl	PDATA_copy_bitfield_info_into_pucp
	ldr	r6, .L130
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #76]
	mov	r3, #4
	str	r3, [sp, #8]
	add	fp, r6, #724
	str	r2, [sp, #20]
	add	r1, sp, #32
	mov	r2, ip
	str	r6, [sp, #4]
	str	fp, [sp, #0]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	r3, r0, r8
	mov	r7, r0
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	add	r3, r6, #8
	ldr	r2, [sp, #76]
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	str	r2, [sp, #20]
	add	r1, sp, #32
	mov	r2, #1
	str	fp, [sp, #0]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	r7, r0, r7
	add	r3, r7, r8
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	add	r3, r6, #12
	ldr	r2, [sp, #76]
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	str	r2, [sp, #20]
	add	r1, sp, #32
	mov	r2, #2
	str	fp, [sp, #0]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	r7, r7, r0
	add	r3, r7, r8
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	add	r3, r6, #16
	ldr	r2, [sp, #76]
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	str	r2, [sp, #20]
	add	r1, sp, #32
	mov	r2, #3
	str	fp, [sp, #0]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	r7, r7, r0
	add	r3, r7, r8
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	add	r3, r6, #72
	str	r3, [sp, #4]
	add	r1, sp, #32
	mov	r2, #5
	str	fp, [sp, #0]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	ip, r7, r0
	add	r3, ip, r8
	str	r3, [sp, #12]
	ldr	r3, [sp, #76]
	mov	r7, #12
	str	r3, [sp, #20]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	str	r7, [sp, #8]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #76]
	add	r3, r6, #84
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #32
	mov	r2, #6
	str	fp, [sp, #0]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r8
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r6, #20
	str	r3, [sp, #4]
	mov	r3, #48
	str	r3, [sp, #8]
	add	r1, sp, #32
	mov	r2, #7
	str	fp, [sp, #0]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r8
	str	r3, [sp, #12]
	ldr	r3, [sp, #76]
	mov	r0, r4
	str	r3, [sp, #20]
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r6, #100
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #32
	mov	r2, #9
	str	fp, [sp, #0]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r8
	str	r3, [sp, #12]
	ldr	r3, [sp, #76]
	mov	r0, r4
	str	r3, [sp, #20]
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r6, #148
	ldr	r2, [sp, #76]
	str	r3, [sp, #4]
	mov	r3, #576
	str	r3, [sp, #8]
	str	r2, [sp, #20]
	add	r1, sp, #32
	mov	r2, #11
	str	fp, [sp, #0]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	r6, r6, #96
	add	ip, ip, r0
	add	r3, ip, r8
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #76]
	mov	r3, #4
	str	r3, [sp, #8]
	str	r2, [sp, #20]
	mov	r3, r9
	add	r1, sp, #32
	mov	r2, r7
	str	fp, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #16]
	str	sl, [sp, #24]
	add	ip, ip, r0
	add	r8, ip, r8
	mov	r0, r4
	str	ip, [sp, #28]
	str	r8, [sp, #12]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r3, [sp, #32]
	ldr	ip, [sp, #28]
	cmp	r3, #0
	add	r9, ip, r0
	beq	.L127
	ldr	r0, [sp, #36]
	add	r1, sp, #32
	mov	r2, #4
	bl	memcpy
	b	.L126
.L127:
	ldr	r3, [r4, #0]
	sub	r9, r9, #8
	sub	r3, r3, #8
	str	r3, [r4, #0]
.L126:
	mov	r0, r9
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L131:
	.align	2
.L130:
	.word	.LANCHOR0
.LFE16:
	.size	NETWORK_CONFIG_build_data_to_send, .-NETWORK_CONFIG_build_data_to_send
	.section	.text.NETWORK_CONFIG_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_extract_and_store_changes_from_comm
	.type	NETWORK_CONFIG_extract_and_store_changes_from_comm, %function
NETWORK_CONFIG_extract_and_store_changes_from_comm:
.LFB10:
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI34:
	mov	r5, r0
	sub	sp, sp, #136
.LCFI35:
	mov	r0, r3
	mov	fp, r3
	mov	r8, r1
	mov	r7, r2
	bl	NETWORK_CONFIG_get_change_bits_ptr
	mov	r1, r5
	mov	r2, #4
	add	r4, r5, #8
	mov	r6, r0
	add	r0, sp, #124
	bl	memcpy
	add	r1, r5, #4
	add	r0, sp, #120
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #120]
	tst	r3, #1
	moveq	r5, #8
	beq	.L133
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #128
	bl	memcpy
	ldr	r0, [sp, #128]
	mov	r1, #1
	mov	r2, r8
	mov	r3, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	bl	NETWORK_CONFIG_set_network_id
	add	r4, r5, #12
	mov	r5, #12
.L133:
	ldr	r3, [sp, #120]
	tst	r3, #2
	beq	.L134
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #128
	bl	memcpy
	ldr	r0, [sp, #128]
	mov	r1, #1
	mov	r2, r8
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	bl	NETWORK_CONFIG_set_start_of_irri_day
.L134:
	ldr	r3, [sp, #120]
	tst	r3, #4
	beq	.L135
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #132
	bl	memcpy
	ldr	r0, [sp, #132]
	mov	r1, #1
	mov	r2, r8
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	bl	NETWORK_CONFIG_set_scheduled_irrigation_off
.L135:
	ldr	r3, [sp, #120]
	tst	r3, #8
	beq	.L136
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #128
	bl	memcpy
	ldr	r0, [sp, #128]
	mov	r1, #1
	mov	r2, r8
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	bl	NETWORK_CONFIG_set_time_zone
.L136:
	ldr	r3, [sp, #120]
	tst	r3, #32
	beq	.L137
	add	sl, sp, #96
	mov	r1, r4
	mov	r2, #12
	mov	r0, sl
	bl	memcpy
	ldmia	sl, {r0, r1, r2}
	add	r3, sp, #108
	stmia	r3, {r0, r1, r2}
.LBB32:
.LBB33:
	mov	ip, #1
	mov	r0, #3
	mov	r1, #7
	mov	r2, #15
	stmia	sp, {r0, r1, r2, ip}
	ldr	r2, .L151
	str	r8, [sp, #20]
	str	r2, [sp, #16]
	mov	r2, #0
	str	r2, [sp, #24]
	ldr	r2, .L151+4
	str	r7, [sp, #28]
	str	r2, [sp, #36]
	mov	r2, #5
	str	r2, [sp, #40]
	ldr	r2, .L151+8
	str	r6, [sp, #32]
	str	r2, [sp, #44]
	ldr	r0, .L151+12
	ldmia	r3, {r1, r2, r3}
.LBE33:
.LBE32:
	add	r4, r4, #12
	add	r5, r5, #12
.LBB35:
.LBB34:
	bl	SHARED_set_DLS_controller
.L137:
.LBE34:
.LBE35:
	ldr	r3, [sp, #120]
	tst	r3, #64
	beq	.L138
	add	sl, sp, #96
	mov	r1, r4
	mov	r2, #12
	mov	r0, sl
	bl	memcpy
	ldmia	sl, {r0, r1, r2}
	add	r3, sp, #108
	stmia	r3, {r0, r1, r2}
.LBB36:
.LBB37:
	ldr	r0, .L151+16
	mov	r2, #11
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	str	r2, [sp, #24]
	ldr	r2, .L151+4
	mov	r1, #8
	str	r1, [sp, #8]
	str	r2, [sp, #36]
	mov	r1, #1
	mov	r2, #6
	str	r1, [sp, #12]
	str	r2, [sp, #40]
	ldr	r1, .L151+20
	ldr	r2, .L151+24
	str	r1, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #28]
	str	r6, [sp, #32]
	str	r2, [sp, #44]
	ldmia	r3, {r1, r2, r3}
.LBE37:
.LBE36:
	add	r4, r4, #12
	add	r5, r5, #12
.LBB39:
.LBB38:
	bl	SHARED_set_DLS_controller
.L138:
.LBE38:
.LBE39:
	ldr	r3, [sp, #120]
	tst	r3, #128
	beq	.L139
	mov	r9, r4
	mov	sl, #0
.L140:
	mov	r1, r9
	mov	r2, #4
	add	r0, sp, #128
	bl	memcpy
	mov	r3, #0
	stmia	sp, {r3, r7}
	ldr	r0, [sp, #128]
	mov	r1, sl
	mov	r2, #1
	mov	r3, r8
	add	sl, sl, #1
	str	r6, [sp, #8]
	bl	NETWORK_CONFIG_set_electrical_limit
	cmp	sl, #12
	add	r9, r9, #4
	bne	.L140
	add	r4, r4, #48
	add	r5, r5, #48
.L139:
	ldr	r3, [sp, #120]
	tst	r3, #512
	beq	.L141
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #132
	bl	memcpy
	ldr	r0, [sp, #132]
	mov	r1, #1
	mov	r2, r8
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	bl	NETWORK_CONFIG_set_send_flow_recording
.L141:
	ldr	r3, [sp, #120]
	tst	r3, #2048
	beq	.L142
	mov	r9, r4
	mov	sl, #0
.L143:
	mov	r1, r9
	mov	r2, #48
	add	r0, sp, #48
	bl	memcpy
	mov	r3, #0
	stmia	sp, {r3, r7}
	mov	r1, sl
	add	r0, sp, #48
	mov	r2, #1
	mov	r3, r8
	add	sl, sl, #1
	str	r6, [sp, #8]
	bl	NETWORK_CONFIG_set_controller_name
	cmp	sl, #12
	add	r9, r9, #48
	bne	.L143
	add	r4, r4, #576
	add	r5, r5, #576
.L142:
	ldr	r3, [sp, #120]
	tst	r3, #4096
	beq	.L144
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #128
	bl	memcpy
	ldr	r0, [sp, #128]
	mov	r1, #1
	mov	r2, r8
	mov	r3, #0
	add	r5, r5, #4
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	bl	NETWORK_CONFIG_set_water_units
.L144:
	cmp	r5, #0
	beq	.L145
	cmp	fp, #1
	cmpne	fp, #15
	beq	.L146
	cmp	fp, #16
	bne	.L147
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L147
.L146:
	mov	r0, #1
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L147
.L145:
	ldr	r0, .L151+28
	ldr	r1, .L151+32
	bl	Alert_bit_set_with_no_data_with_filename
.L147:
	mov	r0, r5
	add	sp, sp, #136
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L152:
	.align	2
.L151:
	.word	57355
	.word	.LANCHOR0+144
	.word	.LC14
	.word	.LANCHOR0+72
	.word	.LANCHOR0+84
	.word	57356
	.word	.LC15
	.word	.LC6
	.word	961
.LFE10:
	.size	NETWORK_CONFIG_extract_and_store_changes_from_comm, .-NETWORK_CONFIG_extract_and_store_changes_from_comm
	.section	.text.NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	.type	NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token, %function
NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L154
	stmfd	sp!, {r4, lr}
.LCFI36:
	ldr	r4, .L154+4
	ldr	r1, [r3, #0]
	ldr	r0, .L154+8
	bl	SHARED_set_all_32_bit_change_bits
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L154+12
	ldr	r3, .L154+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L154+20
	ldr	r0, [r4, #0]
	mov	r2, #1
	str	r2, [r3, #444]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L155:
	.align	2
.L154:
	.word	list_program_data_recursive_MUTEX
	.word	comm_mngr_recursive_MUTEX
	.word	.LANCHOR0+140
	.word	.LC11
	.word	1138
	.word	comm_mngr
.LFE29:
	.size	NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token, .-NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	.section	.text.NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits
	.type	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits, %function
NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L158
	cmp	r0, #51
	ldr	r1, [r3, #0]
	ldr	r0, .L158+4
	bne	.L157
	b	SHARED_clear_all_32_bit_change_bits
.L157:
	b	SHARED_set_all_32_bit_change_bits
.L159:
	.align	2
.L158:
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR0+144
.LFE30:
	.size	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits, .-NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits
	.section	.text.nm_NETWORK_CONFIG_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_NETWORK_CONFIG_update_pending_change_bits
	.type	nm_NETWORK_CONFIG_update_pending_change_bits, %function
nm_NETWORK_CONFIG_update_pending_change_bits:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI37:
	ldr	r4, .L164
	ldr	r3, [r4, #724]
	cmp	r3, #0
	beq	.L160
	cmp	r0, #0
	ldr	r5, .L164+4
	beq	.L162
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L164+8
	ldr	r3, .L164+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r4, #724]
	ldr	r3, [r4, #144]
	ldr	r0, [r5, #0]
	orr	r3, r2, r3
	str	r3, [r4, #144]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L164+16
	mov	r2, #1
	str	r2, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L164+20
	mov	r1, #2
	ldr	r0, [r3, #152]
	ldr	r2, .L164+24
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L163
.L162:
	add	r0, r4, #724
	ldr	r1, [r5, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L163:
	mov	r0, #1
	mov	r1, #0
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L160:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, pc}
.L165:
	.align	2
.L164:
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1177
	.word	weather_preserves
	.word	cics
	.word	60000
.LFE31:
	.size	nm_NETWORK_CONFIG_update_pending_change_bits, .-nm_NETWORK_CONFIG_update_pending_change_bits
	.section	.text.NETWORK_CONFIG_brute_force_set_network_ID_to_zero,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_brute_force_set_network_ID_to_zero
	.type	NETWORK_CONFIG_brute_force_set_network_ID_to_zero, %function
NETWORK_CONFIG_brute_force_set_network_ID_to_zero:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L167
	mov	r2, #0
	str	r2, [r3, #0]
	bx	lr
.L168:
	.align	2
.L167:
	.word	.LANCHOR0
.LFE32:
	.size	NETWORK_CONFIG_brute_force_set_network_ID_to_zero, .-NETWORK_CONFIG_brute_force_set_network_ID_to_zero
	.section	.text.NETWORK_CONFIG_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	NETWORK_CONFIG_calculate_chain_sync_crc
	.type	NETWORK_CONFIG_calculate_chain_sync_crc, %function
NETWORK_CONFIG_calculate_chain_sync_crc:
.LFB33:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI38:
	ldr	r2, .L174
	mov	r4, r0
	mov	r1, sp
	mov	r0, #728
	ldr	r3, .L174+4
	bl	mem_oabia
	subs	r5, r0, #0
	beq	.L170
.LBB40:
	ldr	r3, [sp, #0]
	add	r5, sp, #8
	str	r3, [r5, #-4]!
	ldr	r1, .L174+8
	mov	r2, #4
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L174+12
	mov	r2, #4
	ldr	r6, .L174+16
	add	r8, r6, #576
	mov	r7, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L174+20
	mov	r2, #4
	add	r7, r0, r7
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L174+24
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L174+28
	mov	r2, #48
	add	r7, r7, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L174+32
	mov	r2, #12
	add	r7, r7, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L174+36
	mov	r2, #12
	add	r7, r7, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L174+40
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L174+44
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	add	r7, r7, r0
.L171:
	mov	r0, r6
	bl	strlen
	ldr	r1, .L174+16
	add	r6, r6, #48
	mov	r2, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	cmp	r6, r8
	add	r7, r7, r0
	bne	.L171
	mov	r1, r7
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L174+48
	add	r4, r4, #43
	ldr	r1, .L174
	ldr	r2, .L174+52
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	mov	r0, #1
	b	.L172
.L170:
.LBE40:
	ldr	r3, .L174+56
	ldr	r0, .L174+60
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
	mov	r0, r5
.L172:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L175:
	.align	2
.L174:
	.word	.LC11
	.word	1255
	.word	.LANCHOR0
	.word	.LANCHOR0+8
	.word	.LANCHOR0+148
	.word	.LANCHOR0+12
	.word	.LANCHOR0+16
	.word	.LANCHOR0+20
	.word	.LANCHOR0+72
	.word	.LANCHOR0+84
	.word	.LANCHOR0+96
	.word	.LANCHOR0+100
	.word	cscs
	.word	1330
	.word	chain_sync_file_pertinants
	.word	.LC17
.LFE33:
	.size	NETWORK_CONFIG_calculate_chain_sync_crc, .-NETWORK_CONFIG_calculate_chain_sync_crc
	.global	network_config_item_sizes
	.global	CONTROLLER_DEFAULT_NAME
	.section	.rodata.network_config_item_sizes,"a",%progbits
	.align	2
	.type	network_config_item_sizes, %object
	.size	network_config_item_sizes, 32
network_config_item_sizes:
	.word	148
	.word	148
	.word	724
	.word	724
	.word	728
	.word	728
	.word	728
	.word	728
	.section	.rodata.NETWORK_CONFIG_FILENAME,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	NETWORK_CONFIG_FILENAME, %object
	.size	NETWORK_CONFIG_FILENAME, 22
NETWORK_CONFIG_FILENAME:
	.ascii	"NETWORK_CONFIGURATION\000"
	.section	.rodata.CONTROLLER_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	CONTROLLER_DEFAULT_NAME, %object
	.size	CONTROLLER_DEFAULT_NAME, 32
CONTROLLER_DEFAULT_NAME:
	.ascii	"This Controller (not yet named)\000"
	.section	.bss.config_n,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	config_n, %object
	.size	config_n, 728
config_n:
	.space	728
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"StartOfIrrigationDay\000"
.LC1:
	.ascii	"WaterUnits\000"
.LC2:
	.ascii	"TimeZone\000"
.LC3:
	.ascii	"SendFlowRecording\000"
.LC4:
	.ascii	"%s%d\000"
.LC5:
	.ascii	"ElectricalStationOnLimit\000"
.LC6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/conf"
	.ascii	"iguration/shared_configuration_network.c\000"
.LC7:
	.ascii	"NetworkID\000"
.LC8:
	.ascii	"BoxName\000"
.LC9:
	.ascii	"NET CONFIG file unexpd update %u\000"
.LC10:
	.ascii	"NET CONFIG file update : to revision %u from %u\000"
.LC11:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/conf"
	.ascii	"iguration/configuration_network.c\000"
.LC12:
	.ascii	"NET CONFIG updater error\000"
.LC13:
	.ascii	"ScheduledIrrigationIsOff\000"
.LC14:
	.ascii	"DLSSpringMonth\000"
.LC15:
	.ascii	"DLSFallBackMonth\000"
.LC16:
	.ascii	"%s %c\000"
.LC17:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI2-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI6-.LFB7
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI7-.LFB4
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x60
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI9-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI11-.LFB8
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI13-.LFB11
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI15-.LFB12
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI16-.LFB13
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI17-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI18-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI20-.LFB15
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI22-.LFB18
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI23-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI24-.LFB20
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI25-.LFB21
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI26-.LFB22
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI28-.LFB24
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI29-.LFB25
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI30-.LFB26
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI31-.LFB27
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI32-.LFB16
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI34-.LFB10
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI35-.LCFI34
	.byte	0xe
	.uleb128 0xac
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI36-.LFB29
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI37-.LFB31
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI38-.LFB33
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE62:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/shared_configuration_network.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/configuration_network.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x2d1
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF33
	.byte	0x1
	.4byte	.LASF34
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1b3
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x255
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x1e7
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x20d
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x11d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x2a3
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x18f
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x233
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST3
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xde
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST5
	.uleb128 0x6
	.4byte	0x2a
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST6
	.uleb128 0x8
	.4byte	.LASF8
	.byte	0x2
	.byte	0xff
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST7
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x2
	.2byte	0x223
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x2
	.2byte	0x231
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST9
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x2
	.2byte	0x252
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST10
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x141
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST11
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x2
	.2byte	0x25e
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST12
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF14
	.byte	0x2
	.2byte	0x3a4
	.4byte	.LFB17
	.4byte	.LFE17
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF15
	.byte	0x2
	.2byte	0x3aa
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST13
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF16
	.byte	0x2
	.2byte	0x3ba
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST14
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF17
	.byte	0x2
	.2byte	0x3c8
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST15
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF18
	.byte	0x2
	.2byte	0x3d8
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST16
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x3f1
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST17
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x410
	.4byte	.LFB23
	.4byte	.LFE23
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF21
	.byte	0x2
	.2byte	0x420
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST18
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF22
	.byte	0x2
	.2byte	0x42e
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST19
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF23
	.byte	0x2
	.2byte	0x436
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST20
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF24
	.byte	0x2
	.2byte	0x44f
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST21
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF25
	.byte	0x2
	.2byte	0x45f
	.4byte	.LFB28
	.4byte	.LFE28
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF26
	.byte	0x2
	.2byte	0x2d8
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST22
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x2c7
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST23
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x465
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST24
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF29
	.byte	0x2
	.2byte	0x47a
	.4byte	.LFB30
	.4byte	.LFE30
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x48e
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST25
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF31
	.byte	0x2
	.2byte	0x4b9
	.4byte	.LFB32
	.4byte	.LFE32
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x4c5
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST26
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB9
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI3
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB7
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI8
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 96
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB0
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB8
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI12
	.4byte	.LFE8
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB11
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI14
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB12
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB13
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB14
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB2
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI19
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB15
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI21
	.4byte	.LFE15
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB18
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB19
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB20
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB21
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB22
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI27
	.4byte	.LFE22
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB24
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB25
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB26
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB27
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB16
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI33
	.4byte	.LFE16
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB10
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI35
	.4byte	.LFE10
	.2byte	0x3
	.byte	0x7d
	.sleb128 172
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB29
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB31
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB33
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x114
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF14:
	.ascii	"NETWORK_CONFIG_get_network_id\000"
.LASF12:
	.ascii	"NETWORK_CONFIG_set_scheduled_irrigation_off\000"
.LASF33:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF18:
	.ascii	"NETWORK_CONFIG_get_is_dls_used_for_the_selected_tim"
	.ascii	"e_zone\000"
.LASF22:
	.ascii	"NETWORK_CONFIG_get_controller_name\000"
.LASF11:
	.ascii	"__turn_scheduled_irrigation_OFF\000"
.LASF34:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/conf"
	.ascii	"iguration/configuration_network.c\000"
.LASF0:
	.ascii	"NETWORK_CONFIG_set_electrical_limit\000"
.LASF8:
	.ascii	"network_config_updater\000"
.LASF21:
	.ascii	"NETWORK_CONFIG_get_send_flow_recording\000"
.LASF25:
	.ascii	"NETWORK_CONFIG_get_change_bits_ptr\000"
.LASF2:
	.ascii	"NETWORK_CONFIG_set_dls_fall_back\000"
.LASF35:
	.ascii	"NETWORK_CONFIG_set_controller_name\000"
.LASF30:
	.ascii	"nm_NETWORK_CONFIG_update_pending_change_bits\000"
.LASF3:
	.ascii	"NETWORK_CONFIG_set_water_units\000"
.LASF9:
	.ascii	"init_file_configuration_network\000"
.LASF1:
	.ascii	"NETWORK_CONFIG_set_dls_spring_ahead\000"
.LASF17:
	.ascii	"NETWORK_CONFIG_get_time_zone\000"
.LASF28:
	.ascii	"NETWORK_CONFIG_set_bits_on_all_settings_to_cause_di"
	.ascii	"stribution_in_the_next_token\000"
.LASF27:
	.ascii	"NETWORK_CONFIG_extract_and_store_changes_from_comm\000"
.LASF24:
	.ascii	"NETWORK_CONFIG_get_water_units\000"
.LASF19:
	.ascii	"NETWORK_CONFIG_get_electrical_limit\000"
.LASF15:
	.ascii	"NETWORK_CONFIG_get_start_of_irrigation_day\000"
.LASF10:
	.ascii	"save_file_configuration_network\000"
.LASF23:
	.ascii	"NETWORK_CONFIG_get_controller_name_str_for_ui\000"
.LASF4:
	.ascii	"NETWORK_CONFIG_set_time_zone\000"
.LASF29:
	.ascii	"NETWORK_CONFIG_on_all_settings_set_or_clear_commser"
	.ascii	"ver_change_bits\000"
.LASF16:
	.ascii	"NETWORK_CONFIG_get_controller_off\000"
.LASF5:
	.ascii	"NETWORK_CONFIG_set_send_flow_recording\000"
.LASF31:
	.ascii	"NETWORK_CONFIG_brute_force_set_network_ID_to_zero\000"
.LASF32:
	.ascii	"NETWORK_CONFIG_calculate_chain_sync_crc\000"
.LASF6:
	.ascii	"NETWORK_CONFIG_set_network_id\000"
.LASF26:
	.ascii	"NETWORK_CONFIG_build_data_to_send\000"
.LASF13:
	.ascii	"network_config_set_default_values\000"
.LASF20:
	.ascii	"NETWORK_CONFIG_get_dls_ptr\000"
.LASF7:
	.ascii	"NETWORK_CONFIG_set_start_of_irri_day\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
