	.file	"e_walk_thru.c"
	.text
.Ltext0:
	.section	.text.WALK_THRU_get_visible_number_in_order_sequence,"ax",%progbits
	.align	2
	.type	WALK_THRU_get_visible_number_in_order_sequence, %function
WALK_THRU_get_visible_number_in_order_sequence:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L9
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r0, [r3, #0]
	ldr	r2, .L9+4
	mov	r3, #131
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L9+8
	mov	r4, #0
	mov	r3, r4
.L5:
	ldr	r1, [r2, #4]!
	cmn	r1, #1
	bne	.L2
	mov	ip, r3
	mov	r0, #0
	mov	r1, #1
.L4:
	ldr	lr, [r2, r0]
	add	ip, ip, #1
	cmn	lr, #1
	mov	lr, ip, asl #16
	movne	r1, #0
	cmp	lr, #8388608
	add	r0, r0, #4
	mov	ip, lr, lsr #16
	bne	.L4
	cmp	r1, #0
	addne	r4, r4, #1
	movne	r4, r4, asl #16
	movne	r4, r4, lsr #16
.L2:
	add	r3, r3, #1
	mov	r1, r3, asl #16
	cmp	r1, #8388608
	mov	r3, r1, lsr #16
	bne	.L5
	ldr	r3, .L9
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	rsb	r0, r4, #129
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	cmp	r0, #128
	movge	r0, #128
	ldmfd	sp!, {r4, pc}
.L10:
	.align	2
.L9:
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	WALK_THRU_station_array_for_gui-4
.LFE1:
	.size	WALK_THRU_get_visible_number_in_order_sequence, .-WALK_THRU_get_visible_number_in_order_sequence
	.section	.text.WALK_THRU_add_new_group,"ax",%progbits
	.align	2
	.type	WALK_THRU_add_new_group, %function
WALK_THRU_add_new_group:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	ldr	r4, .L13
	ldr	r2, .L13+4
	ldr	r3, .L13+8
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L13+12
	ldr	r1, .L13+16
	bl	nm_GROUP_create_new_group_from_UI
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	bne	.L12
	ldr	r5, .L13+20
	ldr	r6, [r5, #0]
	bl	FLOWSENSE_get_controller_letter
	sub	r0, r0, #65
	cmp	r6, r0
	beq	.L12
	bl	FLOWSENSE_get_controller_letter
	sub	r0, r0, #65
	str	r0, [r5, #0]
.L12:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L14:
	.align	2
.L13:
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	362
	.word	nm_WAlK_THRU_create_new_group
	.word	WALK_THRU_copy_group_into_guivars
	.word	GuiVar_WalkThruBoxIndex
.LFE10:
	.size	WALK_THRU_add_new_group, .-WALK_THRU_add_new_group
	.section	.text.walk_thru_delay_timer_callback,"ax",%progbits
	.align	2
	.type	walk_thru_delay_timer_callback, %function
walk_thru_delay_timer_callback:
.LFB0:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L16
	mov	r3, #37120
	stmfd	sp!, {r0, r1, lr}
.LCFI2:
	ldr	r0, [r2, #0]
	str	r3, [sp, #0]
	mov	r1, sp
	mov	r3, #0
	mvn	r2, #0
	str	r3, [sp, #4]
	bl	xQueueGenericSend
	ldmfd	sp!, {r2, r3, pc}
.L17:
	.align	2
.L16:
	.word	Key_To_Process_Queue
.LFE0:
	.size	walk_thru_delay_timer_callback, .-walk_thru_delay_timer_callback
	.section	.text.WALK_THRU_update_controller_names,"ax",%progbits
	.align	2
	.type	WALK_THRU_update_controller_names, %function
WALK_THRU_update_controller_names:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	ldr	r4, .L19
	mov	r5, r0
	ldr	r2, .L19+4
	mov	r3, #284
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	ldr	r1, .L19+8
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	ldr	r3, .L19+12
	ldr	r0, [r4, #0]
	str	r5, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L20:
	.align	2
.L19:
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_WalkThruControllerName
	.word	GuiVar_WalkThruBoxIndex
.LFE6:
	.size	WALK_THRU_update_controller_names, .-WALK_THRU_update_controller_names
	.section	.text.WALK_THRU_process_key_event,"ax",%progbits
	.align	2
	.type	WALK_THRU_process_key_event, %function
WALK_THRU_process_key_event:
.LFB13:
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #37120
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI4:
	mov	r4, r0
	sub	sp, sp, #52
.LCFI5:
	mov	r5, r1
	bne	.L22
	ldr	r3, .L172
	ldr	r2, [r3, #0]
	cmp	r2, #0
	subne	r2, r2, #1
	bne	.L148
	ldr	r3, .L172+4
	mov	r2, #1
	str	r2, [r3, #232]
	ldr	r2, .L172+8
	ldr	r2, [r2, #0]
	str	r2, [r3, #236]
	bl	IRRI_DETAILS_jump_to_irrigation_details
	b	.L21
.L22:
	ldr	r3, .L172+12
	ldr	r2, .L172+16
	ldrsh	r3, [r3, #0]
	cmp	r3, r2
	beq	.L26
	add	r2, r2, #142
	cmp	r3, r2
	bne	.L131
	b	.L168
.L26:
	cmp	r0, #67
	beq	.L28
	cmp	r0, #2
	bne	.L29
	ldr	r3, .L172+20
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L29
.L28:
	bl	WALK_THRU_extract_and_store_group_name_from_GuiVars
.L29:
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L172+24
	bl	KEYBOARD_process_key
	b	.L21
.L168:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L30
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L172+28
	b	.L149
.L30:
	bl	COMBO_BOX_key_press
	b	.L21
.L131:
	cmp	r0, #16
	beq	.L37
	bhi	.L41
	cmp	r0, #2
	beq	.L34
	bhi	.L42
	cmp	r0, #0
	beq	.L32
	cmp	r0, #1
	bne	.L31
	b	.L169
.L42:
	cmp	r0, #3
	beq	.L35
	cmp	r0, #4
	bne	.L31
	b	.L170
.L41:
	cmp	r0, #67
	beq	.L39
	bhi	.L43
	cmp	r0, #20
	beq	.L37
	cmp	r0, #48
	bne	.L31
	b	.L38
.L43:
	cmp	r0, #83
	beq	.L38
	cmp	r0, #84
	beq	.L40
	cmp	r0, #80
	bne	.L31
	b	.L40
.L34:
	ldr	r3, .L172+20
	ldrsh	r3, [r3, #0]
	cmp	r3, #3
	beq	.L46
	cmp	r3, #4
	beq	.L47
	cmp	r3, #0
	bne	.L151
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	beq	.L157
.L48:
	mov	r0, #164
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	b	.L21
.L46:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	beq	.L157
.L49:
	ldr	r3, .L172+32
	ldrsh	r4, [r3, #0]
	cmp	r4, #1
	bne	.L50
	bl	good_key_beep
	ldr	r3, .L172+36
	str	r4, [sp, #8]
.L149:
	str	r3, [sp, #28]
.L153:
	add	r0, sp, #8
	bl	Display_Post_Command
	b	.L21
.L50:
	bl	bad_key_beep
	ldr	r0, .L172+40
.L152:
	bl	DIALOG_draw_ok_dialog
	b	.L21
.L47:
	ldr	r4, .L172+44
	ldr	r5, [r4, #0]
	cmp	r5, #1
	bne	.L51
	bl	good_key_beep
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L172+48
	mov	r2, #0
	ldr	r0, [r3, #0]
	mov	r1, r5
	mov	r3, r2
	bl	xTimerGenericCommand
	ldr	r3, .L172+52
	mov	r0, #0
	ldr	r2, [r3, #0]
	ldr	r3, .L172
	str	r0, [r4, #0]
	str	r2, [r3, #0]
	b	.L154
.L51:
	bl	WALK_THRU_extract_and_store_changes_from_GuiVars
	ldr	r3, .L172+56
	ldr	r0, [r3, #0]
	bl	WALK_THRU_copy_group_into_guivars
.LBB9:
	ldr	r3, .L172+60
	mvn	r2, #0
	str	r2, [r3, #0]
	ldr	r3, .L172+32
	ldrsh	r3, [r3, #0]
	cmp	r3, #1
	bne	.L52
	bl	bad_key_beep
	ldr	r0, .L172+64
	b	.L152
.L52:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	bne	.L53
	bl	bad_key_beep
	ldr	r0, .L172+68
	b	.L152
.L53:
	ldr	r3, .L172+72
	ldr	r4, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	cmp	r4, r0
	bne	.L54
	ldr	r3, .L172+76
	ldr	r3, [r3, #112]
	cmp	r3, #1
	bne	.L54
	bl	bad_key_beep
	mov	r0, #604
	b	.L152
.L54:
	bl	IRRI_COMM_if_any_2W_cable_is_over_heated
	cmp	r0, #0
	beq	.L55
	bl	bad_key_beep
	mov	r0, #640
	b	.L152
.L55:
	ldr	r3, .L172+4
	ldr	r3, [r3, #232]
	cmp	r3, #0
	bne	.L151
.L56:
	ldr	r4, .L172+48
	ldr	r2, [r4, #0]
	cmp	r2, #0
	bne	.L57
	ldr	r3, .L172+80
	mov	r1, #1
	str	r3, [sp, #0]
	mov	r3, r2
	bl	xTimerCreate
	str	r0, [r4, #0]
.L57:
	bl	good_key_beep
	ldr	r3, .L172+44
	mov	r2, #1
	str	r2, [r3, #0]
	mov	r0, #0
	bl	Redraw_Screen
	ldr	r3, .L172
	ldr	r2, [r3, #0]
	ldr	r3, .L172+52
.L148:
	str	r2, [r3, #0]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L172+48
	mov	r1, #2
	ldr	r0, [r3, #0]
	mov	r2, #200
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L21
.L40:
.LBE9:
	ldr	r3, .L172+20
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L151
.L63:
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L151
	.word	.L62
.L59:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	beq	.L157
.L64:
	mov	r0, #1
	mov	r3, #0
	stmia	sp, {r0, r3}
	ldr	r1, .L172
	mov	r0, r4
	mov	r2, #5
	mov	r3, #300
	b	.L158
.L60:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	beq	.L157
.L65:
	ldr	r1, .L172+84
	mov	r3, #0
	mov	r2, #1
	stmia	sp, {r2, r3}
	mov	r0, r4
	mov	r3, #150
.L158:
	bl	process_uns32
	b	.L156
.L61:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	bne	.L66
.L157:
	bl	bad_key_beep
	ldr	r0, .L172+88
	b	.L152
.L66:
	ldr	r3, .L172+32
	ldrsh	r5, [r3, #0]
	cmp	r5, #1
	bne	.L50
.LBB10:
	ldr	r6, .L172+92
	mov	r1, #400
	ldr	r2, .L172+96
	mov	r3, #300
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members
	ldr	r3, .L172+100
	add	r1, sp, #52
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r3, [r1, #-8]!
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	sub	r3, r0, #1
	mov	r0, r4
	bl	process_uns32
	ldr	r0, [sp, #44]
	bl	WALK_THRU_update_controller_names
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
	bl	Redraw_Screen
.L156:
.LBE10:
	bl	Refresh_Screen
	b	.L21
.L62:
.LBB11:
	ldr	r3, .L172+92
	ldr	r2, .L172+96
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L172+104
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L172+56
	mov	r6, #0
	ldr	r0, [r3, #0]
	bl	WALK_THRU_get_group_at_this_index
	mov	r1, #0
	add	r8, sp, #44
	add	r7, sp, #48
	ldr	fp, .L172+100
	mov	sl, r0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L172+108
	str	r6, [sp, #44]
	ldr	r5, [r3, r0, asl #2]
	ldr	r0, .L172+112
	bl	nm_ListGetFirst
	bl	nm_STATION_get_station_number_0
	mov	r1, r7
	add	r0, r0, #1
	str	r0, [sp, #48]
	mov	r0, r8
	bl	STATION_get_prev_available_station
	ldr	r3, [fp, #0]
	ldr	r2, [sp, #44]
	cmp	r2, r3
	mov	r9, r0
	movne	r0, r0
	bne	.L68
	b	.L145
.L70:
	mov	r1, r7
	mov	r0, r8
	bl	STATION_get_prev_available_station
	rsb	r1, r9, r0
	rsbs	r6, r1, #0
	adc	r6, r6, r1
.L68:
	ldr	r3, [fp, #0]
	ldr	r1, [sp, #44]
	eor	r2, r6, #1
	cmp	r1, r3
	moveq	r3, #0
	andne	r3, r2, #1
	cmp	r3, #0
	bne	.L70
.L145:
	bl	nm_STATION_get_station_number_0
	add	r9, sp, #44
	add	r8, sp, #48
	mov	r1, r8
	ldr	fp, .L172+100
	add	r3, r0, #1
	mov	r7, r0
	mov	r0, r9
	str	r3, [sp, #48]
	bl	STATION_get_next_available_station
	ldr	r2, [fp, #0]
	ldr	r1, [sp, #44]
	cmp	r1, r2
	mov	r3, r0
	bne	.L134
	b	.L146
.L74:
	mov	r0, r9
	mov	r1, r8
	bl	STATION_get_next_available_station
	mov	r3, r0
.L134:
	ldr	r2, [fp, #0]
	ldr	r0, [sp, #44]
	eor	r1, r6, #1
	cmp	r0, r2
	moveq	r2, #0
	andne	r2, r1, #1
	cmp	r2, #0
	bne	.L74
	mov	r0, r3
.L146:
	bl	nm_STATION_get_station_number_0
	mov	r8, r0
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	bne	.L75
	bl	bad_key_beep
	ldr	r0, .L172+88
	b	.L147
.L75:
	cmp	r6, #0
	beq	.L77
	bl	bad_key_beep
	mov	r0, #648
.L147:
	bl	DIALOG_draw_ok_dialog
	b	.L76
.L77:
	bl	good_key_beep
	cmp	r4, #84
	bne	.L78
	cmn	r5, #1
	moveq	r4, r8
	beq	.L135
	add	r3, r5, #1
	add	r1, sp, #48
	add	r0, sp, #44
	str	r3, [sp, #48]
	bl	STATION_get_next_available_station
	bl	nm_STATION_get_station_number_0
	cmp	r0, r5
	mov	r4, r0
	mvnlt	r4, #0
	blt	.L135
	ldr	r3, .L172+100
	ldr	r2, [sp, #44]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	mvnne	r4, #0
	b	.L135
.L83:
	add	r4, r4, #1
	add	r1, sp, #48
	add	r0, sp, #44
	str	r4, [sp, #48]
	bl	STATION_get_next_available_station
	bl	nm_STATION_get_station_number_0
	cmp	r0, r8
	mvneq	r4, #0
	beq	.L136
	ldr	r3, [r5, #0]
	ldr	r2, [sp, #44]
	cmp	r2, r3
	moveq	r4, r0
	mvnne	r4, #0
	b	.L136
.L135:
	ldr	r5, .L172+100
.L136:
	mov	r0, sl
	mov	r1, r4
	bl	WALK_THRU_check_for_station_in_walk_thru
	cmp	r0, #0
	bne	.L83
	b	.L84
.L78:
	cmp	r5, r8
	mvneq	r4, #0
	beq	.L137
	cmn	r5, #1
	moveq	r4, r7
	beq	.L137
	add	r1, sp, #48
	add	r5, r5, #1
	add	r0, sp, #44
	str	r5, [sp, #48]
	bl	STATION_get_prev_available_station
	bl	nm_STATION_get_station_number_0
	mov	r4, r0
	b	.L137
.L89:
	add	r4, r4, #1
	add	r1, sp, #48
	add	r0, sp, #44
	str	r4, [sp, #48]
	bl	STATION_get_prev_available_station
	bl	nm_STATION_get_station_number_0
	cmp	r0, r7
	mvncs	r4, #0
	bcs	.L138
	ldr	r3, [r5, #0]
	ldr	r2, [sp, #44]
	cmp	r2, r3
	moveq	r4, r0
	mvnne	r4, #0
	b	.L138
.L137:
	ldr	r5, .L172+100
.L138:
	mov	r0, sl
	mov	r1, r4
	bl	WALK_THRU_check_for_station_in_walk_thru
	cmp	r0, #0
	bne	.L89
.L84:
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L172+108
	str	r4, [r3, r0, asl #2]
.L76:
	ldr	r3, .L172+92
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE11:
	mov	r1, #0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L172+60
	str	r0, [r3, #0]
	bl	WALK_THRU_get_visible_number_in_order_sequence
	ldr	r3, .L172+32
	ldrsh	r3, [r3, #0]
	cmp	r3, r0
	beq	.L90
.L159:
	mov	r0, #0
.L154:
	bl	Redraw_Screen
	b	.L21
.L90:
	mov	r0, #1
	bl	SCROLL_BOX_redraw
	b	.L21
.L37:
	ldr	r3, .L172+20
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	bne	.L139
	ldr	r3, .L172+44
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L151
.L139:
	mov	r1, #0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L172+60
	str	r0, [r3, #0]
	bl	WALK_THRU_get_num_groups_in_use
	ldr	r3, .L172+116
	ldr	r2, .L172+120
	str	r3, [sp, #0]
	ldr	r3, .L172+124
	mov	r1, r0
	mov	r0, r4
	bl	GROUP_process_NEXT_and_PREV
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L172+128
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L159
.L170:
	ldr	r3, .L172+20
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	beq	.L108
	cmp	r3, #5
	beq	.L109
	cmp	r3, #0
	b	.L163
.L32:
	ldr	r3, .L172+20
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	beq	.L114
	cmp	r3, #5
	bne	.L143
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #0
	bge	.L118
	b	.L151
.L169:
	ldr	r3, .L172+20
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	beq	.L108
	cmp	r3, #5
	beq	.L109
	cmp	r3, #0
	bne	.L142
	b	.L119
.L109:
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #0
	blt	.L151
	movne	r0, #1
	movne	r1, #4
	bne	.L155
	ldr	r3, .L172+60
	mvn	r2, #0
	str	r2, [r3, #0]
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L172+132
	b	.L149
.L108:
	ldr	r3, .L172+44
	ldr	r3, [r3, #0]
	cmp	r3, #1
.L163:
	beq	.L151
.L142:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L21
.L35:
	ldr	r3, .L172+20
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	beq	.L114
	cmp	r3, #5
	bne	.L143
	b	.L171
.L114:
	ldr	r3, .L172+44
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L151
.L116:
	bl	good_key_beep
	mov	r3, #2
	str	r3, [sp, #8]
	ldr	r3, .L172+136
	str	r3, [sp, #28]
	mov	r3, #1
	str	r3, [sp, #32]
	b	.L153
.L171:
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #0
	blt	.L151
	ldr	r3, .L172+32
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r0, r3
	beq	.L151
.L118:
	mov	r0, #1
	mov	r1, #0
.L155:
	bl	SCROLL_BOX_up_or_down
	b	.L21
.L143:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L21
.L39:
	ldr	r3, .L172+44
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L119
.L151:
	bl	bad_key_beep
	b	.L21
.L119:
	ldr	r0, .L172+140
	bl	KEY_process_BACK_from_editing_screen
	b	.L21
.L38:
	ldr	r3, .L172+20
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	bne	.L31
	ldr	r6, .L172+44
	ldr	r7, [r6, #0]
	cmp	r7, #1
	bne	.L31
	bl	good_key_beep
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L172+48
	mov	r2, #0
	ldr	r0, [r3, #0]
	mov	r1, r7
	mov	r3, r2
	bl	xTimerGenericCommand
	ldr	r3, .L172+52
	mov	r0, #0
	ldr	r2, [r3, #0]
	ldr	r3, .L172
	str	r0, [r6, #0]
	str	r2, [r3, #0]
	bl	Redraw_Screen
.L31:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L21:
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L173:
	.align	2
.L172:
	.word	GuiVar_WalkThruDelay
	.word	irri_comm
	.word	g_GROUP_ID
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	WALK_THRU_draw_menu
	.word	WALK_THRU_close_controller_name_dropdown
	.word	.LANCHOR1
	.word	WALK_THRU_show_controller_name_dropdown
	.word	649
	.word	GuiVar_WalkThruCountingDown
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	g_GROUP_list_item_index
	.word	.LANCHOR3
	.word	647
	.word	591
	.word	GuiVar_StationInfoBoxIndex
	.word	tpmicro_comm
	.word	walk_thru_delay_timer_callback
	.word	GuiVar_WalkThruRunTime
	.word	646
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_WalkThruBoxIndex
	.word	415
	.word	WALK_THRU_station_array_for_gui
	.word	station_info_list_hdr
	.word	WALK_THRU_copy_group_into_guivars
	.word	WALK_THRU_extract_and_store_changes_from_GuiVars
	.word	WALK_THRU_get_group_at_this_index
	.word	WALK_THRU_draw_order_scrollbox
	.word	WALK_THRU_leave_order_scrollbox
	.word	WALK_THRU_enter_order_scrollbox
	.word	WALK_THRU_return_to_menu
.LFE13:
	.size	WALK_THRU_process_key_event, .-WALK_THRU_process_key_event
	.section	.text.WALK_THRU_enter_order_scrollbox,"ax",%progbits
	.align	2
	.type	WALK_THRU_enter_order_scrollbox, %function
WALK_THRU_enter_order_scrollbox:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI6:
	ldr	r5, .L177
	mov	r4, r0
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r2, .L177+4
	mov	r3, #245
	mov	r1, #400
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	WALK_THRU_get_visible_number_in_order_sequence
	ldr	r3, .L177+8
	cmp	r4, #1
	mov	r2, r0
	strh	r0, [r3, #0]	@ movhi
	bne	.L175
	mov	r0, r4
	ldr	r1, .L177+12
	mov	r3, #0
	bl	GuiLib_ScrollBox_Init
	b	.L176
.L175:
	str	r0, [sp, #0]
	ldr	r1, .L177+12
	mov	r0, #1
	mov	r3, r2
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L176:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #5
	bl	GuiLib_Cursor_Select
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L178:
	.align	2
.L177:
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
	.word	WALK_THRU_populate_order_scrollbox
.LFE4:
	.size	WALK_THRU_enter_order_scrollbox, .-WALK_THRU_enter_order_scrollbox
	.section	.text.WALK_THRU_draw_order_scrollbox,"ax",%progbits
	.align	2
	.type	WALK_THRU_draw_order_scrollbox, %function
WALK_THRU_draw_order_scrollbox:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	ldr	r4, .L180
	mov	r1, #400
	ldr	r2, .L180+4
	mov	r3, #228
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	WALK_THRU_get_visible_number_in_order_sequence
	ldr	r5, .L180+8
	strh	r0, [r5, #0]	@ movhi
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r3, .L180+12
	ldr	r1, .L180+16
	ldrsh	r2, [r5, #0]
	ldrsh	r3, [r3, #0]
	mov	r0, #1
	bl	GuiLib_ScrollBox_Init
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L181:
	.align	2
.L180:
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
	.word	.LANCHOR3
	.word	WALK_THRU_populate_order_scrollbox
.LFE3:
	.size	WALK_THRU_draw_order_scrollbox, .-WALK_THRU_draw_order_scrollbox
	.section	.text.WALK_THRU_return_to_menu,"ax",%progbits
	.align	2
	.type	WALK_THRU_return_to_menu, %function
WALK_THRU_return_to_menu:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI8:
	ldr	r0, .L183
	ldr	r1, .L183+4
	bl	FDTO_GROUP_return_to_menu
	ldr	r3, .L183+8
	mvn	r2, #0
	str	r2, [r3, #0]
	ldr	lr, [sp], #4
	b	WALK_THRU_draw_order_scrollbox
.L184:
	.align	2
.L183:
	.word	.LANCHOR4
	.word	WALK_THRU_extract_and_store_changes_from_GuiVars
	.word	.LANCHOR3
.LFE14:
	.size	WALK_THRU_return_to_menu, .-WALK_THRU_return_to_menu
	.section	.text.WALK_THRU_populate_order_scrollbox,"ax",%progbits
	.align	2
	.type	WALK_THRU_populate_order_scrollbox, %function
WALK_THRU_populate_order_scrollbox:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L188
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI9:
	ldr	r2, .L188+4
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #202
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L188+8
	ldr	r0, [r3, #0]
	bl	WALK_THRU_get_group_at_this_index
	ldr	r3, .L188+12
	ldr	r5, [r3, r4, asl #2]
	ldr	r3, .L188+16
	add	r4, r4, #1
	cmn	r5, #1
	str	r4, [r3, #0]
	bne	.L186
	mov	r1, #4
	ldr	r2, .L188+20
	ldr	r0, .L188+24
	bl	snprintf
	mov	r1, #49
	ldr	r2, .L188+28
	ldr	r0, .L188+32
	bl	snprintf
	b	.L187
.L186:
	ldr	r6, .L188+36
	mov	r1, r5
	ldr	r0, [r6, #0]
	bl	nm_STATION_get_pointer_to_station
	mov	r1, r5
	ldr	r2, .L188+24
	mov	r3, #4
	mov	r4, r0
	ldr	r0, [r6, #0]
	bl	STATION_get_station_number_string
	mov	r0, r4
	bl	nm_STATION_get_description
	mov	r1, #49
	ldr	r2, .L188+40
	mov	r3, r0
	ldr	r0, .L188+32
	bl	snprintf
.L187:
	ldr	r3, .L188
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L189:
	.align	2
.L188:
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	g_GROUP_list_item_index
	.word	WALK_THRU_station_array_for_gui
	.word	GuiVar_WalkThruOrder
	.word	.LC1
	.word	GuiVar_WalkThruStation_Str
	.word	.LC2
	.word	GuiVar_WalkThruStationDesc
	.word	GuiVar_WalkThruBoxIndex
	.word	.LC3
.LFE2:
	.size	WALK_THRU_populate_order_scrollbox, .-WALK_THRU_populate_order_scrollbox
	.section	.text.WALK_THRU_leave_order_scrollbox,"ax",%progbits
	.align	2
	.type	WALK_THRU_leave_order_scrollbox, %function
WALK_THRU_leave_order_scrollbox:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI10:
	ldr	r4, .L191
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	mov	r1, #400
	ldr	r2, .L191+4
	ldr	r3, .L191+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	WALK_THRU_get_visible_number_in_order_sequence
	ldr	r3, .L191+12
	ldr	r1, .L191+16
	mov	r2, r0
	strh	r0, [r3, #0]	@ movhi
	mvn	r3, #0
	mov	r0, #1
	bl	GuiLib_ScrollBox_Init
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #4
	mov	r1, #1
	ldmfd	sp!, {r4, lr}
	b	FDTO_Cursor_Select
.L192:
	.align	2
.L191:
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	269
	.word	.LANCHOR1
	.word	WALK_THRU_populate_order_scrollbox
.LFE5:
	.size	WALK_THRU_leave_order_scrollbox, .-WALK_THRU_leave_order_scrollbox
	.section	.text.WALK_THRU_show_controller_name_dropdown,"ax",%progbits
	.align	2
	.type	WALK_THRU_show_controller_name_dropdown, %function
WALK_THRU_show_controller_name_dropdown:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI11:
	bl	FLOWSENSE_get_controller_index
	mov	r4, r0
	bl	COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members
	ldr	r1, .L194
	mov	r3, r4
	mov	r2, r0
	ldr	r0, .L194+4
	ldmfd	sp!, {r4, lr}
	b	FDTO_COMBOBOX_show
.L195:
	.align	2
.L194:
	.word	WALK_THRU_load_controller_name_into_scroll_box_guivar
	.word	751
.LFE8:
	.size	WALK_THRU_show_controller_name_dropdown, .-WALK_THRU_show_controller_name_dropdown
	.section	.text.WALK_THRU_close_controller_name_dropdown,"ax",%progbits
	.align	2
	.type	WALK_THRU_close_controller_name_dropdown, %function
WALK_THRU_close_controller_name_dropdown:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, #0
	str	lr, [sp, #-4]!
.LCFI12:
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	bl	WALK_THRU_update_controller_names
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_hide
.LFE9:
	.size	WALK_THRU_close_controller_name_dropdown, .-WALK_THRU_close_controller_name_dropdown
	.section	.text.WALK_THRU_draw_menu,"ax",%progbits
	.align	2
	.global	WALK_THRU_draw_menu
	.type	WALK_THRU_draw_menu, %function
WALK_THRU_draw_menu:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI13:
	subs	r5, r0, #0
	beq	.L198
	ldr	r3, .L199
	mvn	r2, #0
	str	r2, [r3, #0]
	ldr	r3, .L199+4
	mov	r2, #0
	str	r2, [r3, #0]
.L198:
	ldr	r4, .L199+8
	mov	r1, #400
	ldr	r2, .L199+12
	ldr	r3, .L199+16
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	WALK_THRU_get_num_groups_in_use
	ldr	r3, .L199+20
	ldr	r1, .L199+24
	str	r3, [sp, #0]
	ldr	r3, .L199+28
	str	r3, [sp, #4]
	mov	r3, #1
	str	r3, [sp, #8]
	mov	r3, #71
	mov	r2, r0
	mov	r0, r5
	bl	FDTO_GROUP_draw_menu
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	b	WALK_THRU_draw_order_scrollbox
.L200:
	.align	2
.L199:
	.word	.LANCHOR3
	.word	GuiVar_WalkThruCountingDown
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	1260
	.word	WALK_THRU_load_group_name_into_guivar
	.word	.LANCHOR4
	.word	WALK_THRU_copy_group_into_guivars
.LFE15:
	.size	WALK_THRU_draw_menu, .-WALK_THRU_draw_menu
	.section	.text.WALK_THRU_process_menu,"ax",%progbits
	.align	2
	.global	WALK_THRU_process_menu
	.type	WALK_THRU_process_menu, %function
WALK_THRU_process_menu:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI14:
	ldr	r3, .L203
	mov	r5, r0
	ldr	r2, .L203+4
	mov	r6, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L203+8
	bl	xQueueTakeMutexRecursive_debug
	bl	WALK_THRU_get_num_groups_in_use
	ldr	r2, .L203+12
	ldr	r4, .L203+16
	str	r2, [sp, #0]
	ldr	r2, .L203+20
	mov	r1, r6
	str	r2, [sp, #4]
	ldr	r2, .L203+24
	str	r2, [sp, #8]
	ldr	r2, .L203+28
	str	r2, [sp, #12]
	mov	r2, r4
	mov	r3, r0
	mov	r0, r5
	bl	GROUP_process_menu
	cmp	r5, #67
	beq	.L202
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r5, r0
	bl	WALK_THRU_get_num_groups_in_use
	cmp	r5, r0
	bcs	.L202
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L202
	mov	r0, #1
	bl	SCROLL_BOX_redraw
.L202:
	ldr	r3, .L203
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L204:
	.align	2
.L203:
	.word	walk_thru_recursive_MUTEX
	.word	.LC0
	.word	1288
	.word	WALK_THRU_process_key_event
	.word	.LANCHOR4
	.word	WALK_THRU_add_new_group
	.word	WALK_THRU_get_group_at_this_index
	.word	WALK_THRU_copy_group_into_guivars
.LFE16:
	.size	WALK_THRU_process_menu, .-WALK_THRU_process_menu
	.section	.bss.wt_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	wt_editing_group, %object
	.size	wt_editing_group, 4
wt_editing_group:
	.space	4
	.section	.bss.wt_delay_timer,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	wt_delay_timer, %object
	.size	wt_delay_timer, 4
wt_delay_timer:
	.space	4
	.section	.bss.wt_delay_copy,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	wt_delay_copy, %object
	.size	wt_delay_copy, 4
wt_delay_copy:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_walk_thru.c\000"
.LC1:
	.ascii	"--\000"
.LC2:
	.ascii	"\000"
.LC3:
	.ascii	"%s\000"
	.section	.bss.wt_visible_number_in_order_sequence,"aw",%nobits
	.align	1
	.set	.LANCHOR1,. + 0
	.type	wt_visible_number_in_order_sequence, %object
	.size	wt_visible_number_in_order_sequence, 2
wt_visible_number_in_order_sequence:
	.space	2
	.section	.bss.wt_index_of_order_when_dialog_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	wt_index_of_order_when_dialog_displayed, %object
	.size	wt_index_of_order_when_dialog_displayed, 4
wt_index_of_order_when_dialog_displayed:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI1-.LFB10
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI2-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI3-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI4-.LFB13
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI6-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI8-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI9-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI10-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI11-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI12-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI13-.LFB15
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI14-.LFB16
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE26:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_walk_thru.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x14e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF17
	.byte	0x1
	.4byte	.LASF18
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x125
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.byte	0x77
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x168
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x55
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x119
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST3
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x25c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x189
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x2b3
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST4
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x1
	.byte	0xf1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x1
	.byte	0xe2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST6
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x4c4
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST7
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x1
	.byte	0xc1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST8
	.uleb128 0x4
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x109
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST9
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x13b
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST10
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x144
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST11
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x4e2
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST12
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x506
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST13
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB10
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB6
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB13
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI5
	.4byte	.LFE13
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB14
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB2
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB5
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB8
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB9
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB15
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB16
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x84
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF18:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_walk_thru.c\000"
.LASF4:
	.ascii	"WALK_THRU_process_controller_names\000"
.LASF7:
	.ascii	"WALK_THRU_process_key_event\000"
.LASF11:
	.ascii	"WALK_THRU_populate_order_scrollbox\000"
.LASF15:
	.ascii	"WALK_THRU_draw_menu\000"
.LASF10:
	.ascii	"WALK_THRU_return_to_menu\000"
.LASF17:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"WALK_THRU_process_start_key\000"
.LASF16:
	.ascii	"WALK_THRU_process_menu\000"
.LASF1:
	.ascii	"WALK_THRU_add_new_group\000"
.LASF12:
	.ascii	"WALK_THRU_leave_order_scrollbox\000"
.LASF3:
	.ascii	"WALK_THRU_update_controller_names\000"
.LASF8:
	.ascii	"WALK_THRU_enter_order_scrollbox\000"
.LASF2:
	.ascii	"walk_thru_delay_timer_callback\000"
.LASF9:
	.ascii	"WALK_THRU_draw_order_scrollbox\000"
.LASF6:
	.ascii	"WALK_THRU_process_station_plus_minus\000"
.LASF13:
	.ascii	"WALK_THRU_show_controller_name_dropdown\000"
.LASF14:
	.ascii	"WALK_THRU_close_controller_name_dropdown\000"
.LASF0:
	.ascii	"WALK_THRU_get_visible_number_in_order_sequence\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
