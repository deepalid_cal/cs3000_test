	.file	"battery_backed_vars.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	alerts_pile_engineering
	.section	.battery_backed_vars,"aw",%progbits
	.align	2
	.type	alerts_pile_engineering, %object
	.size	alerts_pile_engineering, 8192
alerts_pile_engineering:
	.space	8192
	.global	alerts_pile_tp_micro
	.align	2
	.type	alerts_pile_tp_micro, %object
	.size	alerts_pile_tp_micro, 8192
alerts_pile_tp_micro:
	.space	8192
	.global	alerts_pile_changes
	.align	2
	.type	alerts_pile_changes, %object
	.size	alerts_pile_changes, 4096
alerts_pile_changes:
	.space	4096
	.global	alerts_pile_user
	.align	2
	.type	alerts_pile_user, %object
	.size	alerts_pile_user, 4096
alerts_pile_user:
	.space	4096
	.global	alerts_struct_engineering
	.align	2
	.type	alerts_struct_engineering, %object
	.size	alerts_struct_engineering, 52
alerts_struct_engineering:
	.space	52
	.global	alerts_struct_tp_micro
	.align	2
	.type	alerts_struct_tp_micro, %object
	.size	alerts_struct_tp_micro, 52
alerts_struct_tp_micro:
	.space	52
	.global	alerts_struct_changes
	.align	2
	.type	alerts_struct_changes, %object
	.size	alerts_struct_changes, 52
alerts_struct_changes:
	.space	52
	.global	alerts_struct_user
	.align	2
	.type	alerts_struct_user, %object
	.size	alerts_struct_user, 52
alerts_struct_user:
	.space	52
	.global	restart_info
	.align	2
	.type	restart_info, %object
	.size	restart_info, 300
restart_info:
	.space	300
	.global	foal_irri
	.align	2
	.type	foal_irri, %object
	.size	foal_irri, 74048
foal_irri:
	.space	74048
	.global	battery_backed_general_use
	.align	2
	.type	battery_backed_general_use, %object
	.size	battery_backed_general_use, 120
battery_backed_general_use:
	.space	120
	.global	weather_preserves
	.align	2
	.type	weather_preserves, %object
	.size	weather_preserves, 236
weather_preserves:
	.space	236
	.global	poc_preserves
	.align	2
	.type	poc_preserves, %object
	.size	poc_preserves, 5684
poc_preserves:
	.space	5684
	.global	system_preserves
	.align	2
	.type	system_preserves, %object
	.size	system_preserves, 56912
system_preserves:
	.space	56912
	.global	station_preserves
	.align	2
	.type	station_preserves, %object
	.size	station_preserves, 270352
station_preserves:
	.space	270352
	.global	chain
	.align	2
	.type	chain, %object
	.size	chain, 1120
chain:
	.space	1120
	.global	lights_preserves
	.align	2
	.type	lights_preserves, %object
	.size	lights_preserves, 1024
lights_preserves:
	.space	1024
	.global	foal_lights
	.align	2
	.type	foal_lights, %object
	.size	foal_lights, 1784
foal_lights:
	.space	1784
	.text
.Letext0:
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
