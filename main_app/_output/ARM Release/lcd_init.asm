	.file	"lcd_init.c"
	.text
.Ltext0:
	.global	__udivsi3
	.section	.text.led_backlight_set_brightness,"ax",%progbits
	.align	2
	.global	led_backlight_set_brightness
	.type	led_backlight_set_brightness, %function
led_backlight_set_brightness:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	bl	vTaskSuspendAll
	bl	CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty
	mov	r1, #100
	rsb	r3, r0, #100
	mov	r3, r3, asl #8
	rsb	r0, r0, r3
	bl	__udivsi3
	ldr	r3, .L2
	and	r0, r0, #255
	orr	r0, r0, #-2147483648
	orr	r0, r0, #9472
	str	r0, [r3, #4]
	ldr	lr, [sp], #4
	b	xTaskResumeAll
.L3:
	.align	2
.L2:
	.word	1074118656
.LFE0:
	.size	led_backlight_set_brightness, .-led_backlight_set_brightness
	.section	.text.init_lcd_1,"ax",%progbits
	.align	2
	.global	init_lcd_1
	.type	init_lcd_1, %function
init_lcd_1:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r4, .L8
	mov	r0, #256
	ldr	r3, [r4, #24]
	mov	r1, #4
	bic	r3, r3, #2048
	bic	r3, r3, #1
	str	r3, [r4, #24]
	bl	clkpwr_setup_lcd
	mov	r0, #1
	mov	r1, r0
	bl	clkpwr_clk_en_dis
	mov	r1, #255
	mov	r2, #38400
	ldr	r0, .L8+4
	bl	memset
	mov	r2, #245760
	mov	r1, #255
	ldr	r0, .L8+8
	bl	memset
	ldr	r1, .L8+12
	mov	r0, r4
	bl	calsense_lcd_open
	mov	r1, #3
	ldr	r2, .L8+4
	bl	lcd_ioctl
	ldr	r3, .L8+16
	ldr	r3, [r3, #0]
	cmp	r3, #16
	bne	.L5
	mov	r3, #131072
	str	r3, [r4, #512]
	ldr	r3, .L8+20
	str	r3, [r4, #516]
	ldr	r3, .L8+24
	str	r3, [r4, #520]
	ldr	r3, .L8+28
	str	r3, [r4, #524]
	ldr	r3, .L8+32
	str	r3, [r4, #528]
	ldr	r3, .L8+36
	str	r3, [r4, #532]
	ldr	r3, .L8+40
	str	r3, [r4, #536]
	ldr	r3, .L8+44
	b	.L7
.L5:
	ldr	r3, .L8+48
	str	r3, [r4, #512]
	str	r3, [r4, #516]
	str	r3, [r4, #520]
	str	r3, [r4, #524]
	mov	r3, #0
	str	r3, [r4, #528]
	str	r3, [r4, #532]
	str	r3, [r4, #536]
.L7:
	str	r3, [r4, #540]
	ldr	r3, .L8
	ldr	r2, [r3, #24]
	orr	r2, r2, #2048
	str	r2, [r3, #24]
	ldr	r2, [r3, #24]
	orr	r2, r2, #1
	str	r2, [r3, #24]
	ldmfd	sp!, {r4, pc}
.L9:
	.align	2
.L8:
	.word	822345728
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	calsense_4bit_grey16
	.word	display_model_is
	.word	393220
	.word	655368
	.word	917516
	.word	1179664
	.word	1441812
	.word	1703960
	.word	1966108
	.word	1966110
.LFE2:
	.size	init_lcd_1, .-init_lcd_1
	.section	.text.init_lcd_2,"ax",%progbits
	.align	2
	.global	init_lcd_2
	.type	init_lcd_2, %function
init_lcd_2:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	bl	init_lcd_1
	ldr	r0, .L11
	mov	r1, #0
	bl	postContrast_or_Volume_Event
.LBB8:
	bl	vTaskSuspendAll
	mov	r0, #14
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	mov	r0, #2
	mov	r2, r0
	mov	r1, #1
	bl	clkpwr_setup_pwm
	bl	xTaskResumeAll
	bl	led_backlight_set_brightness
.LBE8:
	ldr	r3, .L11+4
	mov	r2, #16384
	str	r2, [r3, #4]
	ldr	pc, [sp], #4
.L12:
	.align	2
.L11:
	.word	4369
	.word	1073905664
.LFE3:
	.size	init_lcd_2, .-init_lcd_2
	.section	.text.FDTO_set_guilib_to_write_to_the_primary_display_buffer,"ax",%progbits
	.align	2
	.global	FDTO_set_guilib_to_write_to_the_primary_display_buffer
	.type	FDTO_set_guilib_to_write_to_the_primary_display_buffer, %function
FDTO_set_guilib_to_write_to_the_primary_display_buffer:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L14
	ldr	r3, .L14+4
	str	r2, [r3, #0]
	bx	lr
.L15:
	.align	2
.L14:
	.word	.LANCHOR3
	.word	.LANCHOR2
.LFE5:
	.size	FDTO_set_guilib_to_write_to_the_primary_display_buffer, .-FDTO_set_guilib_to_write_to_the_primary_display_buffer
	.section	.text.CS_DrawStr,"ax",%progbits
	.align	2
	.global	CS_DrawStr
	.type	CS_DrawStr, %function
CS_DrawStr:
.LFB6:
	@ args = 48, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	ip, r1
.LBB9:
	sub	r1, r3, #1
	cmp	r1, #5
.LBE9:
	stmfd	sp!, {r4, lr}
.LCFI3:
	sub	sp, sp, #44
.LCFI4:
.LBB10:
	bhi	.L17
	ldr	lr, .L21
	ldrb	r1, [lr, r1]	@ zero_extendqisi2
.LBE10:
	cmp	r1, #0
	beq	.L17
	add	lr, r0, #1
	add	r1, lr, r1
	cmp	r1, #1536
	ldrcs	r0, .L21+4
	movcs	r2, #1536
	bcs	.L20
.LBB11:
	ldr	r1, .L21+8
	mov	lr, #160
	mla	r0, lr, r0, r1
	ldr	r4, .L21+12
.LBE11:
	mov	r1, r2, asl #16
.LBB12:
	str	r0, [r4, #0]
.LBE12:
	mov	r0, ip, asl #16
	ldr	ip, [sp, #56]
	mov	r2, r3, asl #16
	str	ip, [sp, #0]
	ldr	ip, [sp, #60]
	ldr	r3, [sp, #52]
	and	ip, ip, #255
	str	ip, [sp, #4]
	ldr	ip, [sp, #64]
	mov	r3, r3, asl #24
	and	ip, ip, #255
	str	ip, [sp, #8]
	ldr	ip, [sp, #68]
	mov	r0, r0, asr #16
	and	ip, ip, #255
	str	ip, [sp, #12]
	ldr	ip, [sp, #72]
	mov	r3, r3, asr #24
	and	ip, ip, #255
	str	ip, [sp, #16]
	ldr	ip, [sp, #76]
	mov	r1, r1, asr #16
	mov	ip, ip, asl #16
	mov	ip, ip, asr #16
	str	ip, [sp, #20]
	ldr	ip, [sp, #80]
	mov	r2, r2, lsr #16
	mov	ip, ip, asl #16
	mov	ip, ip, asr #16
	str	ip, [sp, #24]
	ldr	ip, [sp, #84]
	mov	ip, ip, asl #16
	mov	ip, ip, asr #16
	str	ip, [sp, #28]
	ldr	ip, [sp, #88]
	and	ip, ip, #255
	str	ip, [sp, #32]
	ldr	ip, [sp, #92]
	and	ip, ip, #255
	str	ip, [sp, #36]
	ldr	ip, [sp, #96]
	and	ip, ip, #255
	str	ip, [sp, #40]
	bl	GuiLib_DrawStr
.LBB13:
	ldr	r3, .L21+16
	mov	r0, #1
	str	r3, [r4, #0]
	b	.L19
.L17:
.LBE13:
	ldr	r0, .L21+20
	bl	RemovePathFromFileName
	ldr	r2, .L21+24
	mov	r1, r0
	ldr	r0, .L21+28
.L20:
	bl	Alert_Message_va
	mov	r0, #0
.L19:
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L22:
	.align	2
.L21:
	.word	.LANCHOR4
	.word	.LC0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LC1
	.word	407
	.word	.LC2
.LFE6:
	.size	CS_DrawStr, .-CS_DrawStr
	.global	utility_display_buf
	.global	primary_display_buf
	.global	gui_lib_display_buf
	.global	guilib_display_ptr
	.section	.rodata.CSWTCH.5,"a",%progbits
	.set	.LANCHOR4,. + 0
	.type	CSWTCH.5, %object
	.size	CSWTCH.5, 6
CSWTCH.5:
	.byte	11
	.byte	11
	.byte	0
	.byte	0
	.byte	17
	.byte	16
	.section	.bss.gui_lib_display_buf,"aw",%nobits
	.align	3
	.set	.LANCHOR3,. + 0
	.type	gui_lib_display_buf, %object
	.size	gui_lib_display_buf, 38400
gui_lib_display_buf:
	.space	38400
	.section	.bss.utility_display_buf,"aw",%nobits
	.align	3
	.set	.LANCHOR1,. + 0
	.type	utility_display_buf, %object
	.size	utility_display_buf, 245760
utility_display_buf:
	.space	245760
	.section	.bss.primary_display_buf,"aw",%nobits
	.align	3
	.set	.LANCHOR0,. + 0
	.type	primary_display_buf, %object
	.size	primary_display_buf, 38400
primary_display_buf:
	.space	38400
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Utility buffer too small: needs %lu; has %lu\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/"
	.ascii	"lcd_init.c\000"
.LC2:
	.ascii	"Function call with invalid font : %s, %u\000"
	.section	.bss.guilib_display_ptr,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	guilib_display_ptr, %object
	.size	guilib_display_ptr, 4
guilib_display_ptr:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI3-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/lcd_init.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x99
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF6
	.byte	0x1
	.4byte	.LASF7
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x6b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0xf8
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x10a
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x49
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x89
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xda
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	0x31
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x149
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB6
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"FDTO_set_guilib_to_write_to_the_utility_display_buf"
	.ascii	"fer\000"
.LASF2:
	.ascii	"led_backlight_set_brightness\000"
.LASF8:
	.ascii	"FDTO_set_guilib_to_write_to_the_primary_display_buf"
	.ascii	"fer\000"
.LASF3:
	.ascii	"init_lcd_1\000"
.LASF4:
	.ascii	"init_lcd_2\000"
.LASF5:
	.ascii	"CS_DrawStr\000"
.LASF7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/"
	.ascii	"lcd_init.c\000"
.LASF6:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"init_backlight_pwm\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
