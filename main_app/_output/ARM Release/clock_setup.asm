	.file	"clock_setup.c"
	.text
.Ltext0:
	.section	.text.clock_setup,"ax",%progbits
	.align	2
	.global	clock_setup
	.type	clock_setup, %function
clock_setup:
.LFB0:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	mov	r6, r0
	sub	sp, sp, #28
.LCFI1:
	mov	r0, #0
	mov	r4, r1
	mov	r5, r2
	mov	r1, #1
	mov	r2, #2
	bl	clkpwr_set_hclk_divs
	mov	r0, #1
	bl	clkpwr_set_mode
	mov	r0, #1
	mov	r1, #0
	bl	clkpwr_pll_dis_en
	mov	r1, #2
	ldr	r0, .L7
	bl	timer_wait_ms
	bl	clkpwr_get_osc
	mov	r1, r0
	cmp	r1, #1
	mov	r0, #0
	bne	.L2
	bl	clkpwr_mainosc_setup
	ldr	r0, .L7
	mov	r1, #100
	bl	timer_wait_ms
	mov	r0, #0
	mov	r1, #80
	bl	clkpwr_sysclk_setup
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	bl	clkpwr_pll397_setup
	b	.L3
.L2:
	mov	r1, #80
	bl	clkpwr_sysclk_setup
.L3:
	ldr	r0, .L7+4
	mov	r1, r6
	mov	r2, #5
	mov	r3, sp
	bl	clkpwr_find_pll_cfg
	cmp	r0, #0
	beq	.L1
	mov	r0, sp
	bl	clkpwr_hclkpll_setup
.L5:
	mov	r0, #1
	bl	clkpwr_is_pll_locked
	cmp	r0, #0
	beq	.L5
	mov	r0, #0
	mov	r1, r5
	mov	r2, r4
	bl	clkpwr_set_hclk_divs
	mov	r0, #0
	bl	clkpwr_force_arm_hclk_to_pclk
	mov	r0, #0
	bl	clkpwr_set_mode
.L1:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, pc}
.L8:
	.align	2
.L7:
	.word	1074020352
	.word	13000000
.LFE0:
	.size	clock_setup, .-clock_setup
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE0:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/clock_setup.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x32
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF0
	.byte	0x1
	.4byte	.LASF1
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x32
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/clock_setup.c\000"
.LASF2:
	.ascii	"clock_setup\000"
.LASF0:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
