	.file	"r_rt_bypass_flow.c"
	.text
.Ltext0:
	.section	.text.REAL_TIME_BYPASS_FLOW_draw_scroll_line,"ax",%progbits
	.align	2
	.type	REAL_TIME_BYPASS_FLOW_draw_scroll_line, %function
REAL_TIME_BYPASS_FLOW_draw_scroll_line:
.LFB0:
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	ldr	r3, .L8
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	ldr	r2, .L8+4
	mov	r4, r0, asr #16
	add	r5, r4, #1
	str	r5, [r3, #0]
	ldr	r3, .L8+8
	sub	sp, sp, #52
.LCFI1:
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #56
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L8+12
	mov	r1, #400
	ldr	r2, .L8+4
	ldr	r0, [r3, #0]
	mov	r3, #58
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_index
	mov	r1, #1
	bl	nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0
	subs	r6, r0, #0
	beq	.L2
	bl	nm_GROUP_get_group_ID
	add	r1, sp, #48
	mov	r7, r0
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	cmp	r0, #0
	beq	.L3
	mov	r3, #88
	mul	r5, r3, r5
	ldrb	r2, [r0, r5]	@ zero_extendqisi2
	and	r2, r2, #1
	cmp	r2, #1
	bne	.L4
	mla	r3, r4, r3, r0
	ldr	r3, [r3, #96]
	sub	r2, r3, #1
	rsbs	r3, r2, #0
	adc	r3, r3, r2
	b	.L5
.L4:
	cmp	r2, #0
	movne	r3, #0
	bne	.L5
	mla	r3, r4, r3, r0
	ldr	r3, [r3, #96]
	rsbs	r3, r3, #1
	movcc	r3, #0
.L5:
	ldr	r2, .L8+16
	str	r3, [r2, #0]
	mov	r3, #88
	mla	r0, r3, r4, r0
	ldr	r2, .L8+20
	flds	s15, [r0, #148]
	mov	r0, r6
	ftouizs	s15, s15
	fsts	s15, [r2, #0]	@ int
	ldr	r2, .L8+24
	fsts	s15, [r2, #0]	@ int
	bl	POC_get_bypass_number_of_levels
	ldr	r3, .L8+28
	mov	r1, sp
	sub	r0, r0, #1
	cmp	r4, r0
	movcs	r0, #0
	movcc	r0, #1
	str	r0, [r3, #0]
	mov	r0, r7
	bl	POC_get_fm_choice_and_rate_details
	add	r3, sp, #52
	add	r4, r3, r4, asl #4
	ldr	r0, [r4, #-52]
	bl	BYPASS_get_transition_rate_for_this_flow_meter_type
	ldr	r3, .L8+32
	str	r0, [r3, #0]
	mov	r0, r6
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L8+36
	bl	strlcpy
	b	.L6
.L3:
	ldr	r3, .L8+16
	str	r0, [r3, #0]
	ldr	r3, .L8+20
	str	r0, [r3, #0]
	ldr	r3, .L8+24
	str	r0, [r3, #0]
	ldr	r3, .L8+32
	str	r0, [r3, #0]
	ldr	r3, .L8+36
	strb	r0, [r3, #0]
	b	.L6
.L2:
	ldr	r0, .L8+4
	mov	r1, #116
	bl	Alert_group_not_found_with_filename
.L6:
	ldr	r3, .L8+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L8+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L9:
	.align	2
.L8:
	.word	GuiVar_LiveScreensBypassFlowLevel
	.word	.LC0
	.word	poc_preserves_recursive_MUTEX
	.word	list_poc_recursive_MUTEX
	.word	GuiVar_LiveScreensBypassFlowMVOpen
	.word	GuiVar_LiveScreensBypassFlowFlow
	.word	GuiVar_LiveScreensBypassFlowAvgFlow
	.word	GuiVar_LiveScreensBypassFlowShowMaxFlow
	.word	GuiVar_LiveScreensBypassFlowMaxFlow
	.word	GuiVar_GroupName
.LFE0:
	.size	REAL_TIME_BYPASS_FLOW_draw_scroll_line, .-REAL_TIME_BYPASS_FLOW_draw_scroll_line
	.section	.text.FDTO_REAL_TIME_BYPASS_FLOW_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_BYPASS_FLOW_draw_report
	.type	FDTO_REAL_TIME_BYPASS_FLOW_draw_report, %function
FDTO_REAL_TIME_BYPASS_FLOW_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI2:
	mov	r4, r0
	bne	.L11
	mov	r2, r4
	mvn	r1, #0
	mov	r0, #92
	bl	GuiLib_ShowScreen
	bl	FLOWSENSE_get_controller_index
	mov	r1, r4
	bl	nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0
	subs	r4, r0, #0
	beq	.L12
	bl	POC_get_bypass_number_of_levels
	mov	r4, r0
.L12:
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	mov	r2, r4, asl #16
	mov	r0, #0
	ldr	r1, .L15
	mov	r2, r2, asr #16
	mov	r3, r0
	bl	GuiLib_ScrollBox_Init
	b	.L13
.L11:
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
.L13:
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L16:
	.align	2
.L15:
	.word	REAL_TIME_BYPASS_FLOW_draw_scroll_line
.LFE1:
	.size	FDTO_REAL_TIME_BYPASS_FLOW_draw_report, .-FDTO_REAL_TIME_BYPASS_FLOW_draw_report
	.section	.text.REAL_TIME_BYPASS_FLOW_process_report,"ax",%progbits
	.align	2
	.global	REAL_TIME_BYPASS_FLOW_process_report
	.type	REAL_TIME_BYPASS_FLOW_process_report, %function
REAL_TIME_BYPASS_FLOW_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, lr}
.LCFI3:
	bne	.L21
	ldr	r3, .L22
	ldr	r2, .L22+4
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r4, .L22+8
	ldr	r3, [r3, #4]
	str	r3, [r4, #0]
	bl	KEY_process_global_keys
	ldr	r3, [r4, #0]
	cmp	r3, #10
	ldmnefd	sp!, {r4, pc}
	mov	r0, #1
	ldmfd	sp!, {r4, lr}
	b	LIVE_SCREENS_draw_dialog
.L21:
	mov	r2, #0
	mov	r3, r2
	ldmfd	sp!, {r4, lr}
	b	REPORTS_process_report
.L23:
	.align	2
.L22:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	REAL_TIME_BYPASS_FLOW_process_report, .-REAL_TIME_BYPASS_FLOW_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_bypass_flow.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_rt_bypass_flow.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x20
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x7d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x9f
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_bypass_flow.c\000"
.LASF4:
	.ascii	"REAL_TIME_BYPASS_FLOW_draw_scroll_line\000"
.LASF1:
	.ascii	"REAL_TIME_BYPASS_FLOW_process_report\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_REAL_TIME_BYPASS_FLOW_draw_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
