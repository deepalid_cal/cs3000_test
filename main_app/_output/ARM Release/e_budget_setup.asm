	.file	"e_budget_setup.c"
	.text
.Ltext0:
	.section	.text.BUDGET_pre_process_periods_per_year_changed,"ax",%progbits
	.align	2
	.type	BUDGET_pre_process_periods_per_year_changed, %function
BUDGET_pre_process_periods_per_year_changed:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L3
	ldr	r2, [r3, #0]
	ldr	r3, .L3+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bxeq	lr
	mov	r0, #584
	b	DIALOG_draw_yes_no_cancel_dialog
.L4:
	.align	2
.L3:
	.word	.LANCHOR0
	.word	GuiVar_BudgetPeriodsPerYearIdx
.LFE2:
	.size	BUDGET_pre_process_periods_per_year_changed, .-BUDGET_pre_process_periods_per_year_changed
	.section	.text.BUDGET_pre_entry_option_changed,"ax",%progbits
	.align	2
	.type	BUDGET_pre_entry_option_changed, %function
BUDGET_pre_entry_option_changed:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L7
	ldr	r2, [r3, #0]
	ldr	r3, .L7+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bxeq	lr
	ldr	r0, .L7+8
	b	DIALOG_draw_yes_no_cancel_dialog
.L8:
	.align	2
.L7:
	.word	.LANCHOR1
	.word	GuiVar_BudgetEntryOptionIdx
	.word	585
.LFE3:
	.size	BUDGET_pre_entry_option_changed, .-BUDGET_pre_entry_option_changed
	.section	.text.FDTO_BUDGET_SETUP_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_BUDGET_SETUP_return_to_menu, %function
FDTO_BUDGET_SETUP_return_to_menu:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L10
	ldr	r1, .L10+4
	b	FDTO_GROUP_return_to_menu
.L11:
	.align	2
.L10:
	.word	.LANCHOR2
	.word	BUDGET_SETUP_extract_and_store_changes_from_GuiVars
.LFE14:
	.size	FDTO_BUDGET_SETUP_return_to_menu, .-FDTO_BUDGET_SETUP_return_to_menu
	.section	.text.FDTO_POC_show_budget_periods_per_year_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_periods_per_year_dropdown, %function
FDTO_POC_show_budget_periods_per_year_dropdown:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L13
	ldr	r1, .L13+4
	ldr	r3, [r3, #0]
	mov	r0, #724
	mov	r2, #2
	b	FDTO_COMBOBOX_show
.L14:
	.align	2
.L13:
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	FDTO_COMBOBOX_add_items
.LFE7:
	.size	FDTO_POC_show_budget_periods_per_year_dropdown, .-FDTO_POC_show_budget_periods_per_year_dropdown
	.section	.text.FDTO_POC_show_budget_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_in_use_dropdown, %function
FDTO_POC_show_budget_in_use_dropdown:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L16
	mov	r0, #213
	ldr	r2, [r3, #0]
	mov	r1, #38
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L17:
	.align	2
.L16:
	.word	GuiVar_BudgetInUse
.LFE6:
	.size	FDTO_POC_show_budget_in_use_dropdown, .-FDTO_POC_show_budget_in_use_dropdown
	.section	.text.BUDGET_SETUP_process_entry_option_changed_dialog,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_process_entry_option_changed_dialog, %function
BUDGET_SETUP_process_entry_option_changed_dialog:
.LFB5:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L28
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r3, [r3, #0]
	sub	sp, sp, #20
.LCFI1:
	cmp	r3, #6
	beq	.L21
	cmp	r3, #7
	beq	.L20
	cmp	r3, #2
	bne	.L19
	b	.L20
.L21:
.LBB6:
	ldr	r3, .L28+4
	ldr	r6, .L28+8
	ldr	r2, [r3, #0]
	ldr	r3, [r6, #0]
	cmp	r2, r3
	beq	.L22
	mov	r0, sp
	bl	EPSON_obtain_latest_complete_time_and_date
	ldr	r3, .L28+12
	ldrh	r5, [sp, #8]
	ldr	r0, [r3, #0]
	ldrh	r4, [sp, #10]
	bl	SYSTEM_get_group_at_this_index
	ldr	r2, [r6, #0]
	cmp	r2, #0
	bne	.L23
	ldr	r2, .L28+16
	ldr	r2, [r2, #0]
	cmp	r2, #0
	bne	.L23
	tst	r5, #1
	subeq	r5, r5, #1
	mov	r1, r5
	mov	r2, r4
	mov	r3, #2
	b	.L26
.L23:
	mov	r1, r5
	mov	r2, r4
	mov	r3, #1
.L26:
	bl	BUDGET_reset_budget_values
.L22:
	ldr	r3, .L28+8
	ldr	r3, [r3, #0]
	cmp	r3, #2
	moveq	r2, #0
	ldreq	r3, .L28+20
	bne	.L19
	b	.L27
.L20:
.LBE6:
	ldr	r3, .L28+4
	ldr	r2, [r3, #0]
	ldr	r3, .L28+8
.L27:
	str	r2, [r3, #0]
.L19:
	bl	Refresh_Screen
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, pc}
.L29:
	.align	2
.L28:
	.word	g_DIALOG_modal_result
	.word	.LANCHOR1
	.word	GuiVar_BudgetEntryOptionIdx
	.word	g_GROUP_list_item_index
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	GuiVar_BudgetModeIdx
.LFE5:
	.size	BUDGET_SETUP_process_entry_option_changed_dialog, .-BUDGET_SETUP_process_entry_option_changed_dialog
	.section	.text.BUDGET_SETUP_process_number_annual_periods_changed_dialog,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_process_number_annual_periods_changed_dialog, %function
BUDGET_SETUP_process_number_annual_periods_changed_dialog:
.LFB4:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L37
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	ldr	r3, [r3, #0]
	sub	sp, sp, #20
.LCFI3:
	cmp	r3, #6
	beq	.L33
	cmp	r3, #7
	beq	.L32
	cmp	r3, #2
	bne	.L31
	b	.L32
.L33:
.LBB7:
	ldr	r3, .L37+4
	ldr	r6, .L37+8
	ldr	r2, [r3, #0]
	ldr	r3, [r6, #0]
	cmp	r2, r3
	beq	.L31
	mov	r0, sp
	bl	EPSON_obtain_latest_complete_time_and_date
	ldr	r3, .L37+12
	ldrh	r5, [sp, #8]
	ldr	r0, [r3, #0]
	ldrh	r4, [sp, #10]
	bl	SYSTEM_get_group_at_this_index
	ldr	r2, [r6, #0]
	cmp	r2, #0
	movne	r1, r5
	movne	r2, r4
	mov	r3, r0
	movne	r3, #1
	bne	.L36
	tst	r5, #1
	subeq	r5, r5, #1
	mov	r0, r3
	mov	r1, r5
	mov	r2, r4
	mov	r3, #2
.L36:
	bl	BUDGET_reset_budget_values
	b	.L31
.L32:
.LBE7:
	ldr	r3, .L37+4
	ldr	r2, [r3, #0]
	ldr	r3, .L37+8
	str	r2, [r3, #0]
.L31:
	bl	Refresh_Screen
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, pc}
.L38:
	.align	2
.L37:
	.word	g_DIALOG_modal_result
	.word	.LANCHOR0
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	g_GROUP_list_item_index
.LFE4:
	.size	BUDGET_SETUP_process_number_annual_periods_changed_dialog, .-BUDGET_SETUP_process_number_annual_periods_changed_dialog
	.section	.text.BUDGET_SETUP_process_group,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_process_group, %function
BUDGET_SETUP_process_group:
.LFB13:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L119
	stmfd	sp!, {r4, lr}
.LCFI4:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #44
.LCFI5:
	cmp	r3, #720
	mov	r4, r0
	mov	r2, r1
	bgt	.L48
	ldr	ip, .L119+4
	cmp	r3, ip
	bge	.L44
	sub	ip, ip, #134
	cmp	r3, ip
	beq	.L42
	cmp	r3, #616
	beq	.L43
	cmp	r3, #584
	bne	.L40
	b	.L117
.L48:
	cmp	r3, #724
	beq	.L46
	bgt	.L49
	ldr	r1, .L119+8
	cmp	r3, r1
	bgt	.L40
	b	.L118
.L49:
	cmp	r3, #740
	ldreq	r1, .L119+12
	bne	.L40
	b	.L53
.L46:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L53
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L119+16
	b	.L110
.L43:
	ldr	r2, .L119+20
	bl	NUMERIC_KEYPAD_process_key
	b	.L39
.L118:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L53
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L119+24
.L110:
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L39
.L44:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L53
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L119+28
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	bl	BUDGET_SETUP_process_number_annual_periods_changed_dialog
	b	.L39
.L53:
	bl	COMBO_BOX_key_press
	b	.L39
.L117:
	ldr	r2, .L119+32
	b	.L113
.L42:
	ldr	r2, .L119+36
.L113:
	bl	DIALOG_process_yes_no_cancel_dialog
	b	.L39
.L40:
	cmp	r4, #4
	beq	.L59
	bhi	.L63
	cmp	r4, #1
	beq	.L56
	bcc	.L58
	cmp	r4, #2
	beq	.L57
	cmp	r4, #3
	bne	.L54
	b	.L58
.L63:
	cmp	r4, #67
	beq	.L61
	bhi	.L64
	cmp	r4, #16
	beq	.L60
	cmp	r4, #20
	bne	.L54
	b	.L60
.L64:
	cmp	r4, #80
	beq	.L62
	cmp	r4, #84
	bne	.L54
	b	.L62
.L57:
	ldr	r3, .L119+40
	ldrsh	r3, [r3, #0]
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L78
.L73:
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L70
	.word	.L71
	.word	.L72
.L66:
	ldr	r3, .L119+44
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r3, [r0, #468]
	tst	r3, #122880
	beq	.L78
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L119+48
	b	.L107
.L67:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L119+52
.L107:
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L86
.L68:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L119+56
	b	.L107
.L69:
	bl	good_key_beep
	ldr	r0, .L119+60
	mov	r1, #10
	mov	r2, #300
	bl	NUMERIC_KEYPAD_draw_uint32_keypad
	b	.L86
.L70:
	ldr	r3, .L119+64
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L78
.L76:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L119+68
	b	.L107
.L71:
	bl	BUDGET_SETUP_extract_and_store_changes_from_GuiVars
	ldr	r2, .L119+72
	mov	r3, #1
	str	r3, [r2, #0]
	ldr	r2, .L119+44
	str	r3, [sp, #32]
	ldr	r1, [r2, #0]
	ldr	r2, .L119+76
	ldr	r3, .L119+80
	str	r1, [r2, #0]
	mov	r2, #2
	str	r2, [sp, #8]
	mov	r2, #9
	str	r2, [sp, #16]
	ldr	r2, .L119+84
	ldr	r3, [r3, #0]
	str	r2, [sp, #28]
	ldr	r2, .L119+88
	add	r0, sp, #8
	str	r2, [sp, #24]
	str	r3, [sp, #40]
	bl	Change_Screen
	b	.L86
.L72:
	ldr	r4, .L119+44
	bl	good_key_beep
	ldr	r0, [r4, #0]
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L119+92
	bl	strlcpy
	ldr	r0, [r4, #0]
	bl	BUDGET_calculate_square_footage
	cmp	r0, #1
	addeq	r0, r0, #620
	ldrne	r0, .L119+96
	bl	DIALOG_draw_ok_dialog
	b	.L86
.L62:
	ldr	r3, .L119+40
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L78
.L84:
	.word	.L79
	.word	.L80
	.word	.L81
	.word	.L82
	.word	.L83
.L79:
	ldr	r3, .L119+44
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r3, [r0, #468]
	tst	r3, #122880
	beq	.L78
	ldr	r0, .L119+12
	bl	process_bool
	b	.L109
.L80:
	ldr	r1, .L119+100
	ldr	r3, .L119+104
	ldr	r2, [r1, #0]
	mov	r0, r4
	str	r2, [r3, #0]
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	bl	process_uns32
	bl	BUDGET_pre_process_periods_per_year_changed
	b	.L86
.L81:
	ldr	r1, .L119+64
	ldr	r3, .L119+108
	ldr	r2, [r1, #0]
	mov	r0, r4
	str	r2, [r3, #0]
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	bl	BUDGET_pre_entry_option_changed
.L109:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L86
.L82:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L119+60
	mov	r0, r4
	mov	r2, #10
	mov	r3, #300
	bl	process_uns32
	b	.L86
.L83:
	ldr	r3, .L119+64
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L78
.L87:
	mov	r3, #1
	mov	r0, r4
	ldr	r1, .L119+112
	mov	r2, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	bl	process_uns32
	b	.L109
.L78:
	bl	bad_key_beep
.L86:
	bl	Refresh_Screen
	b	.L39
.L60:
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L119+116
	ldr	r2, .L119+120
	str	r3, [sp, #0]
	ldr	r3, .L119+124
	mov	r1, r0
	mov	r0, r4
	bl	GROUP_process_NEXT_and_PREV
	b	.L39
.L59:
	ldr	r3, .L119+40
	ldrsh	r2, [r3, #0]
	cmp	r2, #0
	beq	.L61
	cmp	r2, #2
	bne	.L103
	ldr	r2, .L119+64
	ldr	r0, [r2, #0]
	cmp	r0, #1
	streqh	r0, [r3, #0]	@ movhi
	beq	.L112
.L103:
	mov	r0, #1
	b	.L112
.L56:
	ldr	r3, .L119+40
	ldrsh	r2, [r3, #0]
	cmp	r2, #0
	beq	.L61
	cmp	r2, #2
	bne	.L104
	ldr	r2, .L119+64
	ldr	r2, [r2, #0]
	cmp	r2, #1
	streqh	r4, [r3, #0]	@ movhi
.L104:
	mov	r0, r4
.L112:
	bl	CURSOR_Up
	b	.L39
.L58:
	ldr	r3, .L119+40
	ldrsh	r3, [r3, #0]
	cmp	r3, #5
	bne	.L106
	ldr	r3, .L119+64
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L106
	bl	bad_key_beep
	b	.L39
.L106:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L39
.L61:
	ldr	r0, .L119+128
	bl	KEY_process_BACK_from_editing_screen
	b	.L39
.L54:
	mov	r0, r4
	mov	r1, r2
	bl	KEY_process_global_keys
.L39:
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L120:
	.align	2
.L119:
	.word	GuiLib_CurStructureNdx
	.word	719
	.word	722
	.word	GuiVar_BudgetInUse
	.word	FDTO_POC_close_budget_periods_per_year_dropdown
	.word	FDTO_BUDGET_SETUP_draw_menu
	.word	FDTO_POC_close_budget_option_dropdown
	.word	FDTO_POC_close_budget_mode_dropdown
	.word	BUDGET_SETUP_process_number_annual_periods_changed_dialog
	.word	BUDGET_SETUP_process_entry_option_changed_dialog
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_GROUP_list_item_index
	.word	FDTO_POC_show_budget_in_use_dropdown
	.word	FDTO_POC_show_budget_periods_per_year_dropdown
	.word	FDTO_POC_show_budget_option_dropdown
	.word	GuiVar_BudgetETPercentageUsed
	.word	GuiVar_BudgetEntryOptionIdx
	.word	FDTO_POC_show_budget_mode_dropdown
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	GuiVar_MenuScreenToShow
	.word	FDTO_BUDGETS_draw_menu
	.word	BUDGETS_process_menu
	.word	GuiVar_BudgetMainlineName
	.word	622
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	GuiVar_BudgetModeIdx
	.word	BUDGET_SETUP_copy_group_into_guivars
	.word	BUDGET_SETUP_extract_and_store_changes_from_GuiVars
	.word	SYSTEM_get_group_at_this_index
	.word	FDTO_BUDGET_SETUP_return_to_menu
.LFE13:
	.size	BUDGET_SETUP_process_group, .-BUDGET_SETUP_process_group
	.section	.text.FDTO_POC_close_budget_option_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_budget_option_dropdown, %function
FDTO_POC_close_budget_option_dropdown:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI6:
	ldr	r4, .L122
	ldr	r3, .L122+4
	ldr	r2, [r4, #0]
	mov	r0, #0
	str	r2, [r3, #0]
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [r4, #0]
	bl	FDTO_COMBOBOX_hide
	ldmfd	sp!, {r4, lr}
	b	BUDGET_pre_entry_option_changed
.L123:
	.align	2
.L122:
	.word	GuiVar_BudgetEntryOptionIdx
	.word	.LANCHOR1
.LFE10:
	.size	FDTO_POC_close_budget_option_dropdown, .-FDTO_POC_close_budget_option_dropdown
	.section	.text.FDTO_POC_close_budget_periods_per_year_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_budget_periods_per_year_dropdown, %function
FDTO_POC_close_budget_periods_per_year_dropdown:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI7:
	ldr	r4, .L125
	ldr	r3, .L125+4
	ldr	r2, [r4, #0]
	mov	r0, #0
	str	r2, [r3, #0]
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [r4, #0]
	bl	FDTO_COMBOBOX_hide
	ldmfd	sp!, {r4, lr}
	b	BUDGET_pre_process_periods_per_year_changed
.L126:
	.align	2
.L125:
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	.LANCHOR0
.LFE8:
	.size	FDTO_POC_close_budget_periods_per_year_dropdown, .-FDTO_POC_close_budget_periods_per_year_dropdown
	.section	.text.FDTO_POC_close_budget_mode_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_budget_mode_dropdown, %function
FDTO_POC_close_budget_mode_dropdown:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, #0
	str	lr, [sp, #-4]!
.LCFI8:
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L128
	str	r0, [r3, #0]
	bl	FDTO_COMBOBOX_hide
	mov	r0, #4
	ldr	lr, [sp], #4
	b	GuiLib_Cursor_Select
.L129:
	.align	2
.L128:
	.word	GuiVar_BudgetModeIdx
.LFE12:
	.size	FDTO_POC_close_budget_mode_dropdown, .-FDTO_POC_close_budget_mode_dropdown
	.section	.text.FDTO_BUDGET_SETUP_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_BUDGET_SETUP_draw_menu
	.type	FDTO_BUDGET_SETUP_draw_menu, %function
FDTO_BUDGET_SETUP_draw_menu:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L133
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI9:
	ldr	r2, .L133+4
	mov	r1, #400
	mov	r6, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L133+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L133+12
	ldr	r5, .L133+16
	ldr	r3, [r3, #0]
	mov	r4, #0
	cmp	r3, #1
	ldreq	r3, .L133+20
	ldreq	r2, [r3, #0]
	ldreq	r3, .L133+24
	streq	r2, [r3, #0]
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L133+28
	mov	r1, r5
	str	r3, [sp, #0]
	ldr	r3, .L133+32
	str	r4, [sp, #8]
	str	r3, [sp, #4]
	mov	r3, #12
	mov	r2, r0
	mov	r0, r6
	bl	FDTO_GROUP_draw_menu
	ldr	r2, .L133+12
	ldr	r3, [r2, #0]
	cmp	r3, #1
	bne	.L132
	str	r3, [r5, #0]
	ldr	r3, .L133+36
	str	r4, [r2, #0]
	mov	r2, #5
	strh	r2, [r3, #0]	@ movhi
	mov	r0, r4
	bl	FDTO_Redraw_Screen
.L132:
	ldr	r3, .L133
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L134:
	.align	2
.L133:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	719
	.word	.LANCHOR3
	.word	.LANCHOR2
	.word	.LANCHOR4
	.word	g_GROUP_list_item_index
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	BUDGET_SETUP_copy_group_into_guivars
	.word	GuiLib_ActiveCursorFieldNo
.LFE15:
	.size	FDTO_BUDGET_SETUP_draw_menu, .-FDTO_BUDGET_SETUP_draw_menu
	.section	.text.FDTO_POC_show_budget_mode_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_mode_dropdown, %function
FDTO_POC_show_budget_mode_dropdown:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L138
	ldr	r1, .L138+4
	ldr	r3, [r3, #0]
	mov	r2, #2
	cmp	r3, #0
	ldr	r3, .L138+8
.LBB10:
	ldreq	r0, .L138+12
.LBE10:
	ldr	r3, [r3, #0]
	movne	r0, #720
	b	FDTO_COMBOBOX_show
.L139:
	.align	2
.L138:
	.word	GuiVar_BudgetEntryOptionIdx
	.word	FDTO_COMBOBOX_add_items
	.word	GuiVar_BudgetModeIdx
	.word	719
.LFE11:
	.size	FDTO_POC_show_budget_mode_dropdown, .-FDTO_POC_show_budget_mode_dropdown
	.section	.text.FDTO_POC_show_budget_option_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_option_dropdown, %function
FDTO_POC_show_budget_option_dropdown:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L143
	ldr	r1, .L143+4
	ldr	r3, [r3, #0]
	mov	r2, #3
	cmp	r3, #1
.LBB13:
	ldreq	r0, .L143+8
.LBE13:
	ldrne	r0, .L143+12
	b	FDTO_COMBOBOX_show
.L144:
	.align	2
.L143:
	.word	GuiVar_BudgetEntryOptionIdx
	.word	FDTO_COMBOBOX_add_items
	.word	722
	.word	721
.LFE9:
	.size	FDTO_POC_show_budget_option_dropdown, .-FDTO_POC_show_budget_option_dropdown
	.section	.text.BUDGET_SETUP_process_menu,"ax",%progbits
	.align	2
	.global	BUDGET_SETUP_process_menu
	.type	BUDGET_SETUP_process_menu, %function
BUDGET_SETUP_process_menu:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r6, r7, lr}
.LCFI10:
	ldr	r4, .L146
	mov	r6, r0
	mov	r7, r1
	ldr	r2, .L146+4
	mov	r1, #400
	mov	r3, #748
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	ldr	r2, .L146+8
	mov	r1, r7
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	ldr	r2, .L146+12
	str	r2, [sp, #8]
	ldr	r2, .L146+16
	str	r2, [sp, #12]
	ldr	r2, .L146+20
	mov	r3, r0
	mov	r0, r6
	bl	GROUP_process_menu
	ldr	r0, [r4, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L147:
	.align	2
.L146:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	BUDGET_SETUP_process_group
	.word	SYSTEM_get_group_at_this_index
	.word	BUDGET_SETUP_copy_group_into_guivars
	.word	.LANCHOR2
.LFE16:
	.size	BUDGET_SETUP_process_menu, .-BUDGET_SETUP_process_menu
	.section	.bss.g_BUDGET_SETUP_entry_option,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_BUDGET_SETUP_entry_option, %object
	.size	g_BUDGET_SETUP_entry_option, 4
g_BUDGET_SETUP_entry_option:
	.space	4
	.section	.bss.g_BUDGET_SETUP_number_annual_periods,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_BUDGET_SETUP_number_annual_periods, %object
	.size	g_BUDGET_SETUP_number_annual_periods, 4
g_BUDGET_SETUP_number_annual_periods:
	.space	4
	.section	.bss.g_BUDGET_SETUP_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_BUDGET_SETUP_editing_group, %object
	.size	g_BUDGET_SETUP_editing_group, 4
g_BUDGET_SETUP_editing_group:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budget_setup.c\000"
	.section	.bss.g_BUDGET_setup_selected_group,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_BUDGET_setup_selected_group, %object
	.size	g_BUDGET_setup_selected_group, 4
g_BUDGET_setup_selected_group:
	.space	4
	.section	.bss.g_BUDGET_setup_button_pressed,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_BUDGET_setup_button_pressed, %object
	.size	g_BUDGET_setup_button_pressed, 4
g_BUDGET_setup_button_pressed:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI0-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI4-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI6-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI7-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI8-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI9-.LFB15
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI10-.LFB16
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_budget_setup.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x15d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF17
	.byte	0x1
	.4byte	.LASF18
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x155
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x12b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x4f
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.byte	0xae
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.byte	0xc8
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x2c7
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x10a
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x104
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x1
	.byte	0x79
	.byte	0x1
	.uleb128 0x6
	.4byte	.LASF9
	.byte	0x1
	.byte	0xf2
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST0
	.uleb128 0x6
	.4byte	.LASF10
	.byte	0x1
	.byte	0xd1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x7
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x16c
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST2
	.uleb128 0x7
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x149
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST3
	.uleb128 0x7
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x110
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST4
	.uleb128 0x7
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x163
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST5
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x2cd
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST6
	.uleb128 0x9
	.4byte	0x21
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.4byte	0x2a
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x2ea
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST7
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB5
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI1
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB13
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI5
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB10
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB8
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB12
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB15
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB16
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF16:
	.ascii	"BUDGET_SETUP_process_menu\000"
.LASF6:
	.ascii	"FDTO_POC_show_budget_periods_per_year_dropdown\000"
.LASF11:
	.ascii	"BUDGET_SETUP_process_group\000"
.LASF9:
	.ascii	"BUDGET_SETUP_process_entry_option_changed_dialog\000"
.LASF5:
	.ascii	"FDTO_BUDGET_SETUP_return_to_menu\000"
.LASF0:
	.ascii	"FDTO_POC_show_budget_mode_dropdown\000"
.LASF18:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budget_setup.c\000"
.LASF17:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FDTO_POC_show_budget_option_dropdown\000"
.LASF10:
	.ascii	"BUDGET_SETUP_process_number_annual_periods_changed_"
	.ascii	"dialog\000"
.LASF2:
	.ascii	"BUDGET_SETUP_process_annual_periods_changed\000"
.LASF12:
	.ascii	"FDTO_POC_close_budget_option_dropdown\000"
.LASF8:
	.ascii	"BUDGET_SETUP_process_entry_option_changed\000"
.LASF4:
	.ascii	"BUDGET_pre_entry_option_changed\000"
.LASF13:
	.ascii	"FDTO_POC_close_budget_periods_per_year_dropdown\000"
.LASF3:
	.ascii	"BUDGET_pre_process_periods_per_year_changed\000"
.LASF7:
	.ascii	"FDTO_POC_show_budget_in_use_dropdown\000"
.LASF15:
	.ascii	"FDTO_BUDGET_SETUP_draw_menu\000"
.LASF14:
	.ascii	"FDTO_POC_close_budget_mode_dropdown\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
