	.file	"d_live_screens.c"
	.text
.Ltext0:
	.section	.text.FDTO_LIVE_SCREENS_draw_dialog,"ax",%progbits
	.align	2
	.type	FDTO_LIVE_SCREENS_draw_dialog, %function
FDTO_LIVE_SCREENS_draw_dialog:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L8
	cmp	r0, #1
	ldrsh	r1, [r3, #0]
	ldr	r3, .L8+4
	str	lr, [sp, #-4]!
.LCFI0:
	str	r1, [r3, #0]
	bne	.L2
	ldr	r3, .L8+8
	ldr	r2, [r3, #0]
	cmp	r2, #0
	bne	.L3
	ldr	r2, .L8+12
	ldr	r2, [r2, #0]
	cmp	r2, #0
	movne	r2, #50
	bne	.L7
	ldr	r2, .L8+16
	ldr	r2, [r2, #0]
	cmp	r2, #1
	moveq	r2, #51
	beq	.L7
	ldr	r2, .L8+20
	ldr	r2, [r2, #0]
	cmp	r2, #1
	moveq	r2, #52
	movne	r2, #53
.L7:
	str	r2, [r3, #0]
.L3:
	ldr	r1, [r3, #0]
.L2:
	ldr	r3, .L8+24
	mov	r2, #1
	mov	r1, r1, asl #16
	str	r2, [r3, #0]
	ldr	r0, .L8+28
	mov	r1, r1, asr #16
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L9:
	.align	2
.L8:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	GuiVar_MainMenuMainLinesAvailable
	.word	GuiVar_MainMenuPOCsExist
	.word	GuiVar_MainMenuBypassExists
	.word	.LANCHOR2
	.word	610
.LFE0:
	.size	FDTO_LIVE_SCREENS_draw_dialog, .-FDTO_LIVE_SCREENS_draw_dialog
	.section	.text.LIVE_SCREENS_change_screen.constprop.0,"ax",%progbits
	.align	2
	.type	LIVE_SCREENS_change_screen.constprop.0, %function
LIVE_SCREENS_change_screen.constprop.0:
.LFB4:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L11
	str	lr, [sp, #-4]!
.LCFI1:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #36
.LCFI2:
	ldr	ip, .L11+4
	str	r2, [sp, #16]
	ldr	r2, .L11+8
	str	r3, [ip, #0]
	str	r3, [sp, #32]
	mov	r3, #0
	mov	ip, #2
	mov	lr, #10
	str	r0, [sp, #8]
	str	r1, [sp, #20]
	str	r3, [r2, #0]
	mov	r1, #1
	mov	r0, sp
	stmia	sp, {ip, lr}
	str	r1, [sp, #24]
	str	r3, [sp, #12]
	bl	Change_Screen
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L12:
	.align	2
.L11:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_MenuScreenToShow
	.word	.LANCHOR2
.LFE4:
	.size	LIVE_SCREENS_change_screen.constprop.0, .-LIVE_SCREENS_change_screen.constprop.0
	.section	.text.LIVE_SCREENS_draw_dialog,"ax",%progbits
	.align	2
	.global	LIVE_SCREENS_draw_dialog
	.type	LIVE_SCREENS_draw_dialog, %function
LIVE_SCREENS_draw_dialog:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI3:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI4:
	str	r3, [sp, #0]
	ldr	r3, .L14
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L15:
	.align	2
.L14:
	.word	FDTO_LIVE_SCREENS_draw_dialog
.LFE2:
	.size	LIVE_SCREENS_draw_dialog, .-LIVE_SCREENS_draw_dialog
	.section	.text.LIVE_SCREENS_process_dialog,"ax",%progbits
	.align	2
	.global	LIVE_SCREENS_process_dialog
	.type	LIVE_SCREENS_process_dialog, %function
LIVE_SCREENS_process_dialog:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	ldr	r4, .L34
	ldr	r5, .L34+4
	ldrsh	r3, [r4, #0]
	cmp	r0, #2
	str	r3, [r5, #0]
	beq	.L19
	bhi	.L22
	cmp	r0, #0
	beq	.L18
	b	.L17
.L22:
	cmp	r0, #4
	beq	.L20
	cmp	r0, #67
	bne	.L17
	b	.L33
.L19:
	sub	r3, r3, #50
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L23
.L31:
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
.L24:
	mov	r0, #97
	ldr	r1, .L34+8
	ldr	r2, .L34+12
	b	.L32
.L25:
	ldr	r1, .L34+16
	ldr	r2, .L34+20
	mov	r0, #96
.L32:
	ldmfd	sp!, {r4, r5, lr}
	b	LIVE_SCREENS_change_screen.constprop.0
.L26:
	mov	r0, #92
	ldr	r1, .L34+24
	ldr	r2, .L34+28
	b	.L32
.L27:
	mov	r0, #94
	ldr	r1, .L34+32
	ldr	r2, .L34+36
	b	.L32
.L28:
	mov	r0, #93
	ldr	r1, .L34+40
	ldr	r2, .L34+44
	b	.L32
.L29:
	mov	r0, #98
	ldr	r1, .L34+48
	ldr	r2, .L34+52
	b	.L32
.L30:
	mov	r0, #95
	ldr	r1, .L34+56
	ldr	r2, .L34+60
	b	.L32
.L23:
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L20:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Up
.L18:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Down
.L33:
	bl	good_key_beep
	ldrsh	r3, [r4, #0]
	mov	r0, #0
	str	r3, [r5, #0]
	ldr	r3, .L34+64
	str	r0, [r3, #0]
	ldr	r3, .L34+68
	ldr	r3, [r3, #0]
	strh	r3, [r4, #0]	@ movhi
	ldmfd	sp!, {r4, r5, lr}
	b	Redraw_Screen
.L17:
	ldmfd	sp!, {r4, r5, lr}
	b	KEY_process_global_keys
.L35:
	.align	2
.L34:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	FDTO_REAL_TIME_SYSTEM_FLOW_draw_report
	.word	REAL_TIME_SYSTEM_FLOW_process_report
	.word	FDTO_REAL_TIME_POC_FLOW_draw_report
	.word	REAL_TIME_POC_FLOW_process_report
	.word	FDTO_REAL_TIME_BYPASS_FLOW_draw_report
	.word	REAL_TIME_BYPASS_FLOW_process_report
	.word	FDTO_REAL_TIME_ELECTRICAL_draw_report
	.word	REAL_TIME_ELECTRICAL_process_report
	.word	FDTO_REAL_TIME_COMMUNICATIONS_draw_report
	.word	REAL_TIME_COMMUNICATIONS_process_report
	.word	FDTO_REAL_TIME_WEATHER_draw_report
	.word	REAL_TIME_WEATHER_process_report
	.word	FDTO_REAL_TIME_LIGHTS_draw_report
	.word	REAL_TIME_LIGHTS_process_report
	.word	.LANCHOR2
	.word	.LANCHOR0
.LFE3:
	.size	LIVE_SCREENS_process_dialog, .-LIVE_SCREENS_process_dialog
	.global	g_LIVE_SCREENS_dialog_visible
	.global	g_LIVE_SCREENS_cursor_position_when_dialog_displayed
	.section	.bss.g_LIVE_SCREENS_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_LIVE_SCREENS_cursor_position_when_dialog_displayed, %object
	.size	g_LIVE_SCREENS_cursor_position_when_dialog_displayed, 4
g_LIVE_SCREENS_cursor_position_when_dialog_displayed:
	.space	4
	.section	.bss.g_LIVE_SCREENS_last_cursor_position,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_LIVE_SCREENS_last_cursor_position, %object
	.size	g_LIVE_SCREENS_last_cursor_position, 4
g_LIVE_SCREENS_last_cursor_position:
	.space	4
	.section	.bss.g_LIVE_SCREENS_dialog_visible,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_LIVE_SCREENS_dialog_visible, %object
	.size	g_LIVE_SCREENS_dialog_visible, 4
g_LIVE_SCREENS_dialog_visible:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI5-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_live_screens.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x72
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x60
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.byte	0x30
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x75
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x80
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI2
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"FDTO_LIVE_SCREENS_draw_dialog\000"
.LASF0:
	.ascii	"LIVE_SCREENS_draw_dialog\000"
.LASF4:
	.ascii	"LIVE_SCREENS_change_screen\000"
.LASF1:
	.ascii	"LIVE_SCREENS_process_dialog\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_live_screens.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
