	.file	"ftimes_funcs.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.text.add_duplicate_ft_station,"ax",%progbits
	.align	2
	.type	add_duplicate_ft_station, %function
add_duplicate_ft_station:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ftimes/ftimes_funcs.c"
	.loc 1 25 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #16
.LCFI2:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 38 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 40 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-8]
	.loc 1 45 0
	b	.L2
.L4:
	.loc 1 47 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 50 0
	ldr	r1, .L5
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, r1, r3
	ldrb	r3, [r3, #0]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L2
	.loc 1 53 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	mov	r2, r3
	ldr	r3, .L5
	add	r1, r2, r3
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	mov	r2, r3
	ldr	r3, .L5
	add	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	mov	r2, #112
	bl	memcpy
	.loc 1 56 0
	ldr	r1, .L5
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r2, r1, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #2
	strb	r3, [r2, #0]
	.loc 1 60 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r2, r3, #4
	ldr	r3, .L5
	add	r3, r2, r3
	mov	r0, r3
	mov	r1, #0
	mov	r2, #12
	bl	memset
	.loc 1 63 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r2, r3, #16
	ldr	r3, .L5
	add	r3, r2, r3
	mov	r0, r3
	mov	r1, #0
	mov	r2, #12
	bl	memset
	.loc 1 66 0
	ldr	r1, .L5
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r2, r1, r3
	ldrb	r3, [r2, #0]
	bic	r3, r3, #4
	strb	r3, [r2, #0]
	.loc 1 67 0
	ldr	r0, .L5
	ldr	r2, [fp, #-8]
	mov	r1, #56
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 68 0
	ldr	r0, .L5
	ldr	r2, [fp, #-8]
	mov	r1, #84
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 69 0
	ldr	r0, .L5
	ldr	r2, [fp, #-8]
	mov	r1, #104
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, r0, r3
	add	r2, r3, r1
	ldrb	r3, [r2, #2]
	bic	r3, r3, #64
	strb	r3, [r2, #2]
	.loc 1 70 0
	ldr	r3, [fp, #-20]
	and	r3, r3, #255
	and	r3, r3, #15
	and	r0, r3, #255
	ldr	ip, .L5
	ldr	r2, [fp, #-8]
	mov	r1, #104
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, ip, r3
	add	r1, r3, r1
	ldrh	r3, [r1, #0]	@ movhi
	and	r2, r0, #15
	bic	r3, r3, #960
	mov	r2, r2, asl #6
	orr	r3, r2, r3
	strh	r3, [r1, #0]	@ movhi
	.loc 1 72 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	mov	r2, r3
	ldr	r3, .L5
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 74 0
	b	.L3
.L2:
	.loc 1 45 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, .L5+4
	cmp	r2, r3
	bls	.L4
.L3:
	.loc 1 80 0
	ldr	r3, [fp, #-12]
	.loc 1 81 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	ft_stations
	.word	766
.LFE0:
	.size	add_duplicate_ft_station, .-add_duplicate_ft_station
	.section	.text.find_a_duplicate_ahead_of_this_station,"ax",%progbits
	.align	2
	.type	find_a_duplicate_ahead_of_this_station, %function
find_a_duplicate_ahead_of_this_station:
.LFB1:
	.loc 1 85 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #12
.LCFI5:
	str	r0, [fp, #-12]
	.loc 1 97 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 99 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-4]
	.loc 1 104 0
	b	.L8
.L10:
	.loc 1 106 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
	.loc 1 110 0
	ldr	r1, .L11
	ldr	r2, [fp, #-4]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, r1, r3
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r3, r3, lsr #1
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L8
	.loc 1 112 0
	ldr	r0, .L11
	ldr	r2, [fp, #-4]
	mov	r1, #48
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L11
	ldr	r2, [fp, #-12]
	mov	r0, #48
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	cmp	r1, r3
	bne	.L8
	.loc 1 112 0 is_stmt 0 discriminator 1
	ldr	r0, .L11
	ldr	r2, [fp, #-4]
	mov	r1, #44
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L11
	ldr	r2, [fp, #-12]
	mov	r0, #44
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	cmp	r1, r3
	bne	.L8
	.loc 1 115 0 is_stmt 1
	ldr	r2, [fp, #-4]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	mov	r2, r3
	ldr	r3, .L11
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 117 0
	b	.L9
.L8:
	.loc 1 104 0 discriminator 1
	ldr	r2, [fp, #-4]
	ldr	r3, .L11+4
	cmp	r2, r3
	bls	.L10
.L9:
	.loc 1 124 0
	ldr	r3, [fp, #-8]
	.loc 1 125 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L12:
	.align	2
.L11:
	.word	ft_stations
	.word	766
.LFE1:
	.size	find_a_duplicate_ahead_of_this_station, .-find_a_duplicate_ahead_of_this_station
	.section	.text.FTIMES_FUNCS_load_run_time_calculation_support,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_load_run_time_calculation_support
	.type	FTIMES_FUNCS_load_run_time_calculation_support, %function
FTIMES_FUNCS_load_run_time_calculation_support:
.LFB2:
	.loc 1 129 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #12
.LCFI8:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 132 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 134 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L14
	.loc 1 136 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 138 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #84]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 140 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #88]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #4]
	.loc 1 142 0
	ldr	r3, [fp, #-16]
	add	r2, r3, #8
	ldr	r3, [fp, #-12]
	add	r3, r3, #104
	mov	r0, r2
	mov	r1, r3
	mov	r2, #28
	bl	memcpy
	.loc 1 144 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #132]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #36]
	.loc 1 146 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #144]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #40]
	.loc 1 149 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #92]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #44]
.L14:
	.loc 1 152 0
	ldr	r3, [fp, #-8]
	.loc 1 153 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE2:
	.size	FTIMES_FUNCS_load_run_time_calculation_support, .-FTIMES_FUNCS_load_run_time_calculation_support
	.section	.text.FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID
	.type	FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID, %function
FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID:
.LFB3:
	.loc 1 157 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #12
.LCFI11:
	str	r0, [fp, #-16]
	.loc 1 170 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 172 0
	ldr	r0, .L20
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 174 0
	b	.L16
.L19:
	.loc 1 176 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L17
	.loc 1 178 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 180 0
	b	.L18
.L17:
	.loc 1 183 0
	ldr	r0, .L20
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L16:
	.loc 1 174 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L19
.L18:
	.loc 1 186 0
	ldr	r3, [fp, #-8]
	.loc 1 187 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L21:
	.align	2
.L20:
	.word	ft_station_groups_list_hdr
.LFE3:
	.size	FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID, .-FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID
	.section	.text.FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID
	.type	FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID, %function
FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID:
.LFB4:
	.loc 1 191 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #12
.LCFI14:
	str	r0, [fp, #-16]
	.loc 1 201 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 203 0
	ldr	r0, .L27
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 205 0
	b	.L23
.L26:
	.loc 1 207 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L24
	.loc 1 209 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 211 0
	b	.L25
.L24:
	.loc 1 214 0
	ldr	r0, .L27
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L23:
	.loc 1 205 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L26
.L25:
	.loc 1 217 0
	ldr	r3, [fp, #-8]
	.loc 1 218 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L28:
	.align	2
.L27:
	.word	ft_system_groups_list_hdr
.LFE4:
	.size	FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID, .-FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ftim"
	.ascii	"es/ftimes_funcs.c\000"
	.align	2
.LC1:
	.ascii	"FTIMES: pftstation not on irrigation list! : %s, %u"
	.ascii	"\000"
	.align	2
.LC2:
	.ascii	"FTIMES: unexp ON count : %s, %u\000"
	.align	2
.LC3:
	.ascii	"FTIMES: should been on the ON list : %s, %u\000"
	.align	2
.LC4:
	.ascii	"FTIMES: system number ON 0\000"
	.align	2
.LC5:
	.ascii	"FTIMES: unexp system expected flow rate\000"
	.align	2
.LC6:
	.ascii	"FTIMES: unexp flow group count\000"
	.align	2
.LC7:
	.ascii	"FTIMES: on ON list! : %s, %u\000"
	.section	.text.ftimes_if_station_is_ON_turn_it_OFF,"ax",%progbits
	.align	2
	.global	ftimes_if_station_is_ON_turn_it_OFF
	.type	ftimes_if_station_is_ON_turn_it_OFF, %function
ftimes_if_station_is_ON_turn_it_OFF:
.LFB5:
	.loc 1 222 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #16
.LCFI17:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 229 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 234 0
	ldr	r0, .L49
	ldr	r1, [fp, #-20]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	bne	.L30
	.loc 1 236 0
	ldr	r0, .L49+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L49+8
	mov	r1, r3
	mov	r2, #236
	bl	Alert_Message_va
	b	.L31
.L30:
	.loc 1 250 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L32
	.loc 1 252 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-20]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L32:
	.loc 1 261 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #106]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L33
	.loc 1 263 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #48]
	ldr	r3, .L49+12
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	bne	.L34
	.loc 1 265 0
	ldr	r0, .L49+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L49+16
	mov	r1, r3
	ldr	r2, .L49+20
	bl	Alert_Message_va
	b	.L35
.L34:
	.loc 1 269 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #48]
	ldr	r3, [fp, #-20]
	ldr	r1, [r3, #48]
	ldr	r3, .L49+12
	ldr	r3, [r3, r1, asl #2]
	sub	r1, r3, #1
	ldr	r3, .L49+12
	str	r1, [r3, r2, asl #2]
.L35:
	.loc 1 277 0
	ldr	r0, .L49+24
	ldr	r1, [fp, #-20]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L36
	.loc 1 279 0
	ldr	r0, .L49+24
	ldr	r1, [fp, #-20]
	ldr	r2, .L49+4
	ldr	r3, .L49+28
	bl	nm_ListRemove_debug
	b	.L37
.L36:
	.loc 1 283 0
	ldr	r0, .L49+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L49+32
	mov	r1, r3
	ldr	r2, .L49+36
	bl	Alert_Message_va
.L37:
	.loc 1 290 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L38
	.loc 1 292 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-20]
	ldr	r2, [r2, #28]
	ldr	r2, [r2, #36]
	sub	r2, r2, #1
	str	r2, [r3, #36]
	b	.L39
.L38:
	.loc 1 297 0
	ldr	r0, .L49+40
	bl	Alert_Message
.L39:
	.loc 1 302 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	ldr	r2, [r3, #44]
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #102]
	cmp	r2, r3
	bcc	.L40
	.loc 1 304 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-20]
	ldr	r2, [r2, #28]
	ldr	r1, [r2, #44]
	ldr	r2, [fp, #-20]
	ldrh	r2, [r2, #102]
	rsb	r2, r2, r1
	str	r2, [r3, #44]
	b	.L41
.L40:
	.loc 1 308 0
	ldr	r0, .L49+44
	bl	Alert_Message
	.loc 1 311 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	mov	r2, #0
	str	r2, [r3, #44]
.L41:
	.loc 1 316 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-20]
	ldrb	r2, [r2, #106]	@ zero_extendqisi2
	mov	r2, r2, lsr #2
	and	r2, r2, #3
	and	r2, r2, #255
	add	r2, r2, #29
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L42
	.loc 1 318 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-20]
	ldrb	r2, [r2, #106]	@ zero_extendqisi2
	mov	r2, r2, lsr #2
	and	r2, r2, #3
	and	r2, r2, #255
	ldr	r1, [fp, #-20]
	ldr	r1, [r1, #28]
	ldr	r0, [fp, #-20]
	ldrb	r0, [r0, #106]	@ zero_extendqisi2
	mov	r0, r0, lsr #2
	and	r0, r0, #3
	and	r0, r0, #255
	add	r0, r0, #29
	ldr	r1, [r1, r0, asl #2]
	sub	r1, r1, #1
	add	r2, r2, #29
	str	r1, [r3, r2, asl #2]
	b	.L43
.L42:
	.loc 1 323 0
	ldr	r0, .L49+48
	bl	Alert_Message
.L43:
	.loc 1 333 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 342 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L44
	.loc 1 346 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	bne	.L44
	.loc 1 348 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L44:
	.loc 1 354 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L45
	.loc 1 356 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	ldr	r2, [r3, #132]
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #168]
	cmp	r2, r3
	bcs	.L46
	.loc 1 358 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-20]
	ldr	r2, [r2, #32]
	ldr	r2, [r2, #168]
	str	r2, [r3, #132]
	b	.L46
.L45:
	.loc 1 376 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	mov	r2, #0
	str	r2, [r3, #132]
.L46:
	.loc 1 383 0
	ldr	r2, [fp, #-20]
	ldrb	r3, [r2, #106]
	bic	r3, r3, #64
	strb	r3, [r2, #106]
	.loc 1 390 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	str	r2, [r3, #64]
	.loc 1 402 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	beq	.L47
	.loc 1 404 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #80]
	ldr	r3, [fp, #-20]
	str	r2, [r3, #84]
	b	.L48
.L47:
	.loc 1 407 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	str	r2, [r3, #84]
.L48:
	.loc 1 419 0
	mov	r0, #1
	ldr	r1, [fp, #-20]
	bl	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	b	.L31
.L33:
	.loc 1 425 0
	ldr	r0, .L49+24
	ldr	r1, [fp, #-20]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L31
	.loc 1 427 0
	ldr	r0, .L49+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L49+52
	mov	r1, r3
	ldr	r2, .L49+56
	bl	Alert_Message_va
	.loc 1 429 0
	ldr	r0, .L49+24
	ldr	r1, [fp, #-20]
	ldr	r2, .L49+4
	ldr	r3, .L49+60
	bl	nm_ListRemove_debug
.L31:
	.loc 1 437 0
	ldr	r3, [fp, #-8]
	.loc 1 438 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L50:
	.align	2
.L49:
	.word	ft_irrigating_stations_list_hdr
	.word	.LC0
	.word	.LC1
	.word	ft_stations_ON_by_controller
	.word	.LC2
	.word	265
	.word	ft_stations_ON_list_hdr
	.word	279
	.word	.LC3
	.word	283
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	427
	.word	429
.LFE5:
	.size	ftimes_if_station_is_ON_turn_it_OFF, .-ftimes_if_station_is_ON_turn_it_OFF
	.section	.text.ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists,"ax",%progbits
	.align	2
	.global	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists
	.type	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists, %function
ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists:
.LFB6:
	.loc 1 442 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #12
.LCFI20:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 460 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	bl	ftimes_if_station_is_ON_turn_it_OFF
	str	r0, [fp, #-8]
	.loc 1 466 0
	ldr	r0, .L53
	ldr	r1, [fp, #-16]
	ldr	r2, .L53+4
	ldr	r3, .L53+8
	bl	nm_ListRemove_debug
	.loc 1 470 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #2
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L52
	.loc 1 473 0
	ldr	r0, [fp, #-16]
	mov	r1, #0
	mov	r2, #112
	bl	memset
.L52:
	.loc 1 478 0
	ldr	r3, [fp, #-8]
	.loc 1 479 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L54:
	.align	2
.L53:
	.word	ft_irrigating_stations_list_hdr
	.word	.LC0
	.word	466
.LFE6:
	.size	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists, .-ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists
	.section	.text.ftimes_complete_the_turn_ON,"ax",%progbits
	.align	2
	.global	ftimes_complete_the_turn_ON
	.type	ftimes_complete_the_turn_ON, %function
ftimes_complete_the_turn_ON:
.LFB7:
	.loc 1 483 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #4
.LCFI23:
	str	r0, [fp, #-8]
	.loc 1 486 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #106]
	orr	r3, r3, #64
	strb	r3, [r2, #106]
	.loc 1 491 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #48]
	ldr	r3, [fp, #-8]
	ldr	r1, [r3, #48]
	ldr	r3, .L57
	ldr	r3, [r3, r1, asl #2]
	add	r1, r3, #1
	ldr	r3, .L57
	str	r1, [r3, r2, asl #2]
	.loc 1 497 0
	ldr	r0, .L57+4
	ldr	r1, [fp, #-8]
	bl	nm_ListInsertTail
	.loc 1 503 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #28]
	ldrb	r3, [r2, #137]
	bic	r3, r3, #64
	strb	r3, [r2, #137]
	.loc 1 505 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #28]
	ldrb	r3, [r2, #136]
	bic	r3, r3, #128
	strb	r3, [r2, #136]
	.loc 1 506 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #28]
	ldrb	r3, [r2, #137]
	bic	r3, r3, #1
	strb	r3, [r2, #137]
	.loc 1 507 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #28]
	ldrb	r3, [r2, #137]
	bic	r3, r3, #2
	strb	r3, [r2, #137]
	.loc 1 508 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #28]
	ldrb	r3, [r2, #137]
	bic	r3, r3, #4
	strb	r3, [r2, #137]
	.loc 1 509 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #28]
	ldrb	r3, [r2, #137]
	bic	r3, r3, #8
	strb	r3, [r2, #137]
	.loc 1 510 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #28]
	ldrb	r3, [r2, #137]
	bic	r3, r3, #16
	strb	r3, [r2, #137]
	.loc 1 511 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #28]
	ldrb	r3, [r2, #137]
	bic	r3, r3, #32
	strb	r3, [r2, #137]
	.loc 1 515 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L55
	.loc 1 517 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #0]
	bic	r3, r3, #4
	strb	r3, [r2, #0]
.L55:
	.loc 1 519 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L58:
	.align	2
.L57:
	.word	ft_stations_ON_by_controller
	.word	ft_stations_ON_list_hdr
.LFE7:
	.size	ftimes_complete_the_turn_ON, .-ftimes_complete_the_turn_ON
	.section	.text.ftimes_get_percent_adjust_100u,"ax",%progbits
	.align	2
	.global	ftimes_get_percent_adjust_100u
	.type	ftimes_get_percent_adjust_100u, %function
ftimes_get_percent_adjust_100u:
.LFB8:
	.loc 1 523 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI24:
	add	fp, sp, #0
.LCFI25:
	sub	sp, sp, #12
.LCFI26:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 529 0
	mov	r3, #100
	str	r3, [fp, #-4]
	.loc 1 533 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L60
	.loc 1 535 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L60
	.loc 1 535 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #80]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bcc	.L60
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #80]
	cmp	r2, r3
	beq	.L60
	.loc 1 537 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #72]
	str	r3, [fp, #-4]
.L60:
	.loc 1 541 0
	ldr	r3, [fp, #-4]
	.loc 1 542 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE8:
	.size	ftimes_get_percent_adjust_100u, .-ftimes_get_percent_adjust_100u
	.section	.text.ftimes_number_of_starts_for_this_manual_program,"ax",%progbits
	.align	2
	.global	ftimes_number_of_starts_for_this_manual_program
	.type	ftimes_number_of_starts_for_this_manual_program, %function
ftimes_number_of_starts_for_this_manual_program:
.LFB9:
	.loc 1 546 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #12
.LCFI29:
	str	r0, [fp, #-16]
	.loc 1 553 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 557 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L62
	.loc 1 560 0
	ldr	r3, .L67
	ldrh	r3, [r3, #12]
	mov	r2, r3
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #68]
	cmp	r2, r3
	bcc	.L62
	.loc 1 560 0 is_stmt 0 discriminator 1
	ldr	r3, .L67
	ldrh	r3, [r3, #12]
	mov	r2, r3
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #72]
	cmp	r2, r3
	bhi	.L62
	.loc 1 563 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L63
.L66:
	.loc 1 565 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-12]
	add	r2, r2, #4
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L67
	ldr	r3, [r3, #8]
	cmp	r2, r3
	bne	.L64
	.loc 1 567 0
	ldr	r3, .L67
	ldr	r3, [r3, #28]
	cmp	r3, #0
	bne	.L65
	.loc 1 571 0
	ldr	r3, .L67
	ldrh	r3, [r3, #12]
	mov	r2, r3
	ldr	r3, .L67
	ldr	r3, [r3, #8]
	mov	r0, r2
	mov	r1, r3
	ldr	r2, .L67+4
	bl	DateAndTimeToDTCS
	.loc 1 573 0
	ldr	r3, .L67
	mov	r2, #1
	str	r2, [r3, #28]
.L65:
	.loc 1 576 0
	ldr	r3, .L67
	ldrb	r3, [r3, #26]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	add	r2, r2, #10
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L64
	.loc 1 579 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L64:
	.loc 1 563 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L63:
	.loc 1 563 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #5
	bls	.L66
.L62:
	.loc 1 588 0 is_stmt 1
	ldr	r3, [fp, #-8]
	.loc 1 589 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L68:
	.align	2
.L67:
	.word	ftcs
	.word	ftcs+8
.LFE9:
	.size	ftimes_number_of_starts_for_this_manual_program, .-ftimes_number_of_starts_for_this_manual_program
	.section	.text.ftimes_add_to_the_irrigation_list,"ax",%progbits
	.align	2
	.global	ftimes_add_to_the_irrigation_list
	.type	ftimes_add_to_the_irrigation_list, %function
ftimes_add_to_the_irrigation_list:
.LFB10:
	.loc 1 593 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #4
.LCFI32:
	str	r0, [fp, #-8]
	.loc 1 601 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #106]
	bic	r3, r3, #64
	strb	r3, [r2, #106]
	.loc 1 611 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L70
	.loc 1 613 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #80]
	b	.L71
.L70:
	.loc 1 617 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #80]
.L71:
	.loc 1 620 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #84]
	.loc 1 622 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #64]
	.loc 1 639 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r0, r3
	bl	FOAL_IRRI_we_test_flow_when_ON_for_this_reason
	mov	r3, r0
	and	r3, r3, #255
	and	r3, r3, #1
	and	r1, r3, #255
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #107]
	and	r1, r1, #1
	bic	r3, r3, #1
	orr	r3, r1, r3
	strb	r3, [r2, #107]
	.loc 1 652 0
	ldr	r0, .L72
	ldr	r1, [fp, #-8]
	bl	nm_ListInsertTail
	.loc 1 655 0
	mov	r0, #1
	ldr	r1, [fp, #-8]
	bl	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	.loc 1 656 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L73:
	.align	2
.L72:
	.word	ft_irrigating_stations_list_hdr
.LFE10:
	.size	ftimes_add_to_the_irrigation_list, .-ftimes_add_to_the_irrigation_list
	.global	__umodsi3
	.section .rodata
	.align	2
.LC8:
	.ascii	"Invalid schedule type\000"
	.section	.text.start_time_check_does_it_irrigate_on_this_date,"ax",%progbits
	.align	2
	.type	start_time_check_does_it_irrigate_on_this_date, %function
start_time_check_does_it_irrigate_on_this_date:
.LFB11:
	.loc 1 674 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #16
.LCFI35:
	str	r0, [fp, #-20]
	.loc 1 679 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 689 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #88]
	cmp	r3, #18
	ldrls	pc, [pc, r3, asl #2]
	b	.L75
.L81:
	.word	.L76
	.word	.L77
	.word	.L78
	.word	.L79
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
.L76:
	.loc 1 692 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 693 0
	b	.L82
.L77:
	.loc 1 696 0
	ldr	r3, .L99
	ldrh	r3, [r3, #14]
	and	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #0
	bne	.L96
	.loc 1 698 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 700 0
	b	.L96
.L78:
	.loc 1 703 0
	ldr	r3, .L99
	ldrh	r3, [r3, #14]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L97
	.loc 1 707 0
	ldr	r3, .L99
	ldrh	r3, [r3, #14]
	cmp	r3, #1
	bne	.L85
	.loc 1 708 0 discriminator 1
	ldr	r3, .L99
	ldrh	r3, [r3, #16]
	.loc 1 707 0 discriminator 1
	cmp	r3, #1
	beq	.L86
	.loc 1 708 0
	ldr	r3, .L99
	ldrh	r3, [r3, #16]
	cmp	r3, #2
	beq	.L86
	.loc 1 709 0 discriminator 1
	ldr	r3, .L99
	ldrh	r3, [r3, #16]
	.loc 1 708 0 discriminator 1
	cmp	r3, #4
	beq	.L86
	.loc 1 709 0
	ldr	r3, .L99
	ldrh	r3, [r3, #16]
	cmp	r3, #6
	beq	.L86
	.loc 1 710 0 discriminator 1
	ldr	r3, .L99
	ldrh	r3, [r3, #16]
	.loc 1 709 0 discriminator 1
	cmp	r3, #8
	beq	.L86
	.loc 1 710 0
	ldr	r3, .L99
	ldrh	r3, [r3, #16]
	cmp	r3, #9
	beq	.L86
	.loc 1 711 0 discriminator 1
	ldr	r3, .L99
	ldrh	r3, [r3, #16]
	.loc 1 710 0 discriminator 1
	cmp	r3, #11
	bne	.L85
.L86:
	.loc 1 713 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #132]
	cmp	r3, #0
	beq	.L84
	.loc 1 715 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 713 0
	b	.L84
.L85:
	.loc 1 719 0
	ldr	r3, .L99
	ldrh	r3, [r3, #18]
	mov	r0, r3
	bl	IsLeapYear
	mov	r3, r0
	cmp	r3, #1
	bne	.L88
	.loc 1 719 0 is_stmt 0 discriminator 1
	ldr	r3, .L99
	ldrh	r3, [r3, #14]
	cmp	r3, #1
	bne	.L88
	ldr	r3, .L99
	ldrh	r3, [r3, #16]
	cmp	r3, #3
	bne	.L88
	.loc 1 721 0 is_stmt 1
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #132]
	cmp	r3, #0
	beq	.L84
	.loc 1 723 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 721 0
	b	.L84
.L88:
	.loc 1 728 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 731 0
	b	.L97
.L84:
	b	.L97
.L79:
	.loc 1 734 0
	ldr	r3, .L99
	ldrb	r3, [r3, #26]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-20]
	add	r2, r2, #26
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L98
	.loc 1 736 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 738 0
	b	.L98
.L80:
	.loc 1 755 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #88]
	cmp	r3, #17
	bne	.L91
	.loc 1 757 0
	mov	r3, #21
	str	r3, [fp, #-12]
	b	.L92
.L91:
	.loc 1 759 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #88]
	cmp	r3, #18
	bne	.L93
	.loc 1 761 0
	mov	r3, #28
	str	r3, [fp, #-12]
	b	.L92
.L93:
	.loc 1 765 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #88]
	sub	r3, r3, #2
	str	r3, [fp, #-12]
.L92:
	.loc 1 776 0
	ldr	r3, .L99
	ldrh	r3, [r3, #12]
	mov	r2, r3
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #100]
	rsb	r3, r3, r2
	str	r3, [fp, #-16]
	.loc 1 778 0
	ldr	r3, [fp, #-16]
	mov	r0, r3
	ldr	r1, [fp, #-12]
	bl	__umodsi3
	mov	r3, r0
	cmp	r3, #0
	bne	.L94
	.loc 1 780 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 786 0
	b	.L82
.L94:
	.loc 1 784 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 786 0
	b	.L82
.L75:
	.loc 1 789 0
	ldr	r0, .L99+4
	bl	Alert_Message
	b	.L82
.L96:
	.loc 1 700 0
	mov	r0, r0	@ nop
	b	.L82
.L97:
	.loc 1 731 0
	mov	r0, r0	@ nop
	b	.L82
.L98:
	.loc 1 738 0
	mov	r0, r0	@ nop
.L82:
	.loc 1 792 0
	ldr	r3, [fp, #-8]
	.loc 1 793 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L100:
	.align	2
.L99:
	.word	ftcs
	.word	.LC8
.LFE11:
	.size	start_time_check_does_it_irrigate_on_this_date, .-start_time_check_does_it_irrigate_on_this_date
	.section	.text.we_are_at_a_start_time_and_a_water_day,"ax",%progbits
	.align	2
	.type	we_are_at_a_start_time_and_a_water_day, %function
we_are_at_a_start_time_and_a_water_day:
.LFB12:
	.loc 1 797 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #8
.LCFI38:
	.loc 1 812 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 816 0
	ldr	r0, .L107
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 818 0
	b	.L102
.L106:
	.loc 1 828 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #84]
	cmp	r3, #0
	beq	.L103
	.loc 1 828 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #92]
	ldr	r3, .L107+4
	ldr	r3, [r3, #8]
	cmp	r2, r3
	bne	.L103
	.loc 1 830 0 is_stmt 1
	ldr	r3, .L107+4
	ldr	r3, [r3, #28]
	cmp	r3, #0
	bne	.L104
	.loc 1 832 0
	ldr	r3, .L107+4
	ldrh	r3, [r3, #12]
	mov	r2, r3
	ldr	r3, .L107+4
	ldr	r3, [r3, #8]
	mov	r0, r2
	mov	r1, r3
	ldr	r2, .L107+8
	bl	DateAndTimeToDTCS
	.loc 1 834 0
	ldr	r3, .L107+4
	mov	r2, #1
	str	r2, [r3, #28]
.L104:
	.loc 1 841 0
	ldr	r0, [fp, #-8]
	bl	start_time_check_does_it_irrigate_on_this_date
	mov	r3, r0
	cmp	r3, #0
	beq	.L103
	.loc 1 843 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 847 0
	b	.L105
.L103:
	.loc 1 851 0
	ldr	r0, .L107
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L102:
	.loc 1 818 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L106
.L105:
	.loc 1 854 0
	ldr	r3, [fp, #-12]
	.loc 1 855 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L108:
	.align	2
.L107:
	.word	ft_station_groups_list_hdr
	.word	ftcs
	.word	ftcs+8
.LFE12:
	.size	we_are_at_a_start_time_and_a_water_day, .-we_are_at_a_start_time_and_a_water_day
	.section	.text.a_manual_program_wants_to_start,"ax",%progbits
	.align	2
	.type	a_manual_program_wants_to_start, %function
a_manual_program_wants_to_start:
.LFB13:
	.loc 1 859 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI39:
	add	fp, sp, #4
.LCFI40:
	sub	sp, sp, #8
.LCFI41:
	.loc 1 871 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 873 0
	ldr	r0, .L114
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 875 0
	b	.L110
.L113:
	.loc 1 877 0
	ldr	r0, [fp, #-8]
	bl	ftimes_number_of_starts_for_this_manual_program
	mov	r3, r0
	cmp	r3, #0
	beq	.L111
	.loc 1 879 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 883 0
	b	.L112
.L111:
	.loc 1 886 0
	ldr	r0, .L114
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L110:
	.loc 1 875 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L113
.L112:
	.loc 1 889 0
	ldr	r3, [fp, #-12]
	.loc 1 890 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L115:
	.align	2
.L114:
	.word	ft_manual_programs_list_hdr
.LFE13:
	.size	a_manual_program_wants_to_start, .-a_manual_program_wants_to_start
	.section .rodata
	.align	2
.LC9:
	.ascii	"FTIMES ERROR: duplicate problem : %s, %u\000"
	.align	2
.LC10:
	.ascii	"FTIMES ERROR: no room for duplicate : %s, %u\000"
	.align	2
.LC11:
	.ascii	"FTIMES ERROR: station in irri list for UNK reason :"
	.ascii	" %s, %u\000"
	.section	.text.FTIMES_FUNCS_check_for_a_start,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_check_for_a_start
	.type	FTIMES_FUNCS_check_for_a_start, %function
FTIMES_FUNCS_check_for_a_start:
.LFB14:
	.loc 1 894 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI42:
	fstmfdd	sp!, {d8}
.LCFI43:
	add	fp, sp, #16
.LCFI44:
	sub	sp, sp, #48
.LCFI45:
	.loc 1 915 0
	ldr	r3, .L149	@ float
	str	r3, [fp, #-56]	@ float
	.loc 1 921 0
	mov	r3, #0
	str	r3, [fp, #-44]
	.loc 1 923 0
	bl	we_are_at_a_start_time_and_a_water_day
	mov	r3, r0
	cmp	r3, #0
	beq	.L117
	.loc 1 925 0
	mov	r3, #1
	str	r3, [fp, #-44]
.L117:
	.loc 1 928 0
	bl	a_manual_program_wants_to_start
	mov	r3, r0
	cmp	r3, #0
	beq	.L118
	.loc 1 930 0
	mov	r3, #1
	str	r3, [fp, #-44]
.L118:
	.loc 1 935 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L116
	.loc 1 937 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 939 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 943 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L120
.L148:
	.loc 1 945 0
	ldr	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #4
	mov	r2, r3
	ldr	r3, .L149+4
	add	r3, r2, r3
	str	r3, [fp, #-48]
	.loc 1 950 0
	ldr	r3, [fp, #-48]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L121
	.loc 1 958 0
	ldr	r3, [fp, #-48]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #2
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L122
	.loc 1 958 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-48]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #128
	beq	.L123
.L122:
	.loc 1 963 0 is_stmt 1
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	cmp	r3, #0
	beq	.L123
	.loc 1 967 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #84]
	cmp	r3, #0
	beq	.L123
	.loc 1 967 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	ldr	r2, [r3, #92]
	ldr	r3, .L149+8
	ldr	r3, [r3, #8]
	cmp	r2, r3
	bne	.L123
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	ldr	r2, [r3, #96]
	ldr	r3, .L149+8
	ldr	r3, [r3, #8]
	cmp	r2, r3
	beq	.L123
	.loc 1 969 0 is_stmt 1
	ldr	r3, .L149+8
	ldr	r3, [r3, #28]
	cmp	r3, #0
	bne	.L124
	.loc 1 975 0
	ldr	r3, .L149+8
	ldrh	r3, [r3, #12]
	mov	r2, r3
	ldr	r3, .L149+8
	ldr	r3, [r3, #8]
	mov	r0, r2
	mov	r1, r3
	ldr	r2, .L149+12
	bl	DateAndTimeToDTCS
	.loc 1 977 0
	ldr	r3, .L149+8
	mov	r2, #1
	str	r2, [r3, #28]
.L124:
	.loc 1 985 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	mov	r0, r3
	bl	start_time_check_does_it_irrigate_on_this_date
	mov	r3, r0
	cmp	r3, #0
	beq	.L123
	.loc 1 985 0 is_stmt 0 discriminator 1
	mov	r0, #1
	mov	r1, #0
	ldr	r2, [fp, #-48]
	ldr	r3, .L149+12
	bl	STATION_GROUPS_skip_irrigation_due_to_a_mow_day
	mov	r3, r0
	cmp	r3, #0
	bne	.L123
	.loc 1 987 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 992 0
	ldr	r0, .L149+16
	ldr	r1, [fp, #-48]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L125
	.loc 1 996 0
	ldr	r3, [fp, #-48]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L126
	.loc 1 1002 0
	mov	r0, #0
	ldr	r1, [fp, #-48]
	bl	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists
	b	.L125
.L126:
	.loc 1 1005 0
	ldr	r3, [fp, #-48]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #128
	bne	.L127
	.loc 1 1009 0
	mov	r3, #1
	str	r3, [fp, #-28]
	.loc 1 1014 0
	ldr	r3, [fp, #-48]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #2
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L125
	.loc 1 1025 0
	ldr	r0, [fp, #-20]
	bl	find_a_duplicate_ahead_of_this_station
	str	r0, [fp, #-52]
	.loc 1 1027 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	beq	.L128
	.loc 1 1031 0
	ldr	r0, .L149+16
	ldr	r1, [fp, #-52]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L129
	.loc 1 1031 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-52]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	beq	.L125
.L129:
	.loc 1 1041 0 is_stmt 1
	ldr	r0, .L149+20
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L149+24
	mov	r1, r3
	ldr	r2, .L149+28
	bl	Alert_Message_va
	.loc 1 1043 0
	mov	r3, #1
	str	r3, [fp, #-24]
	b	.L125
.L128:
	.loc 1 1048 0
	ldr	r0, [fp, #-20]
	mov	r1, #1
	bl	add_duplicate_ft_station
	str	r0, [fp, #-52]
	.loc 1 1050 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	bne	.L125
	.loc 1 1055 0
	ldr	r0, .L149+20
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L149+32
	mov	r1, r3
	ldr	r2, .L149+36
	bl	Alert_Message_va
	b	.L125
.L127:
	.loc 1 1066 0
	ldr	r0, .L149+20
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L149+40
	mov	r1, r3
	ldr	r2, .L149+44
	bl	Alert_Message_va
	.loc 1 1068 0
	mov	r3, #1
	str	r3, [fp, #-24]
.L125:
	.loc 1 1074 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L131
	.loc 1 1076 0
	bl	FTIMES_TASK_restart_calculation
	.loc 1 1079 0
	b	.L116
.L131:
	.loc 1 1084 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L123
	.loc 1 1091 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #92]
	.loc 1 1086 0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	ldr	r3, .L149+12
	str	r3, [sp, #0]
	fsts	s15, [sp, #4]
	mov	r0, #1
	mov	r1, #0
	mov	r2, #0
	ldr	r3, [fp, #-48]
	bl	nm_WATERSENSE_calculate_run_minutes_for_this_station
	fmsr	s14, r0
	flds	s15, [fp, #-56]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	ldr	r3, [fp, #-48]
	str	r2, [r3, #60]
	.loc 1 1096 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #140]
	cmp	r3, #0
	beq	.L132
	.loc 1 1098 0
	ldr	r3, [fp, #-48]
	ldr	r2, [r3, #60]
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #56]
	add	r2, r2, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #60]
	.loc 1 1100 0
	ldr	r3, [fp, #-48]
	mov	r2, #0
	str	r2, [r3, #56]
	.loc 1 1102 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	beq	.L132
	.loc 1 1109 0
	ldr	r3, [fp, #-48]
	add	r2, r3, #60
	.loc 1 1111 0
	ldr	r3, [fp, #-48]
	.loc 1 1109 0
	ldr	r3, [r3, #68]
	mov	r0, #1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #0
	bl	WATERSENSE_make_min_cycle_adjustment
	mov	r2, r0
	ldr	r3, [fp, #-48]
	str	r2, [r3, #56]
.L132:
	.loc 1 1125 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #60]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	.loc 1 1122 0
	flds	s15, [fp, #-56]
	fdivs	s16, s14, s15
	.loc 1 1126 0
	ldr	r3, [fp, #-48]
	.loc 1 1122 0
	ldr	r4, [r3, #88]
	.loc 1 1127 0
	ldr	r3, [fp, #-48]
	.loc 1 1122 0
	ldr	r2, [r3, #32]
	.loc 1 1127 0
	ldr	r3, .L149+8
	ldrh	r3, [r3, #12]
	.loc 1 1122 0
	mov	r0, r2
	mov	r1, r3
	bl	ftimes_get_percent_adjust_100u
	mov	r3, r0
	str	r4, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, #1
	mov	r1, #0
	ldr	r2, [fp, #-48]
	fmrs	r3, s16
	bl	nm_WATERSENSE_calculate_adjusted_run_time
	fmsr	s14, r0
	flds	s15, [fp, #-56]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	ldr	r3, [fp, #-48]
	str	r2, [r3, #60]
	.loc 1 1138 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #140]
	cmp	r3, #0
	beq	.L133
	.loc 1 1140 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	beq	.L133
	.loc 1 1143 0
	ldr	r3, [fp, #-48]
	add	r2, r3, #60
	.loc 1 1145 0
	ldr	r3, [fp, #-48]
	.loc 1 1143 0
	ldr	r3, [r3, #68]
	mov	r0, #1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #0
	bl	WATERSENSE_make_min_cycle_adjustment
.L133:
	.loc 1 1155 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	ldr	r2, [r3, #96]
	ldr	r3, [fp, #-48]
	str	r2, [r3, #96]
	.loc 1 1157 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #96]
	ldr	r0, .L149+12
	mov	r1, r3
	bl	FOAL_IRRI_return_stop_date_according_to_stop_time
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-48]
	strh	r2, [r3, #100]	@ movhi
	.loc 1 1172 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	beq	.L123
	.loc 1 1174 0
	ldr	r2, [fp, #-48]
	ldrh	r3, [r2, #104]	@ movhi
	bic	r3, r3, #896
	orr	r3, r3, #64
	strh	r3, [r2, #104]	@ movhi
	.loc 1 1176 0
	ldr	r2, [fp, #-48]
	ldrb	r3, [r2, #105]
	bic	r3, r3, #4
	strb	r3, [r2, #105]
	.loc 1 1178 0
	ldr	r3, [fp, #-48]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #1
	and	r2, r3, #255
	ldr	r1, [fp, #-48]
	ldrb	r3, [r1, #105]
	and	r2, r2, #1
	bic	r3, r3, #16
	mov	r2, r2, asl #4
	orr	r3, r2, r3
	strb	r3, [r1, #105]
	.loc 1 1182 0
	ldr	r3, [fp, #-48]
	ldr	r2, [r3, #68]
	ldr	r3, [fp, #-48]
	str	r2, [r3, #72]
	.loc 1 1186 0
	ldr	r0, [fp, #-48]
	bl	ftimes_add_to_the_irrigation_list
	.loc 1 1191 0
	ldr	r2, [fp, #-48]
	ldrb	r3, [r2, #0]
	orr	r3, r3, #4
	strb	r3, [r2, #0]
	.loc 1 1198 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	mov	r2, #0
	str	r2, [r3, #216]
	.loc 1 1200 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	ldr	r2, .L149+8
	add	r3, r3, #192
	add	r2, r2, #8
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 1202 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	mov	r2, #0
	str	r2, [r3, #200]
	.loc 1 1204 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	mov	r2, #0
	str	r2, [r3, #204]
	.loc 1 1207 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #32]
	mov	r2, #0
	str	r2, [r3, #212]
.L123:
	.loc 1 1231 0
	ldr	r3, [fp, #-48]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #2
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L134
	.loc 1 1231 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-48]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	beq	.L121
.L134:
	.loc 1 1237 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-36]
	.loc 1 1239 0
	mov	r3, #0
	str	r3, [fp, #-32]
	b	.L135
.L136:
	.loc 1 1241 0 discriminator 2
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-32]
	add	r2, r2, #9
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	ftimes_number_of_starts_for_this_manual_program
	mov	r3, r0
	ldr	r2, [fp, #-36]
	add	r3, r2, r3
	str	r3, [fp, #-36]
	.loc 1 1239 0 discriminator 2
	ldr	r3, [fp, #-32]
	add	r3, r3, #1
	str	r3, [fp, #-32]
.L135:
	.loc 1 1239 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-32]
	cmp	r3, #1
	bls	.L136
	.loc 1 1244 0 is_stmt 1
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L121
	.loc 1 1246 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 1 1248 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 1253 0
	ldr	r0, .L149+16
	ldr	r1, [fp, #-48]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L137
	.loc 1 1257 0
	ldr	r3, [fp, #-48]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #128
	bne	.L138
	.loc 1 1262 0
	mov	r3, #1
	str	r3, [fp, #-40]
	b	.L137
.L138:
	.loc 1 1265 0
	ldr	r3, [fp, #-48]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L139
	.loc 1 1269 0
	mov	r3, #1
	str	r3, [fp, #-28]
	.loc 1 1274 0
	ldr	r3, [fp, #-48]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #2
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L137
	.loc 1 1285 0
	ldr	r0, [fp, #-20]
	bl	find_a_duplicate_ahead_of_this_station
	str	r0, [fp, #-52]
	.loc 1 1287 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	beq	.L140
	.loc 1 1291 0
	ldr	r0, .L149+16
	ldr	r1, [fp, #-52]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L141
	.loc 1 1291 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-52]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #128
	beq	.L137
.L141:
	.loc 1 1301 0 is_stmt 1
	ldr	r0, .L149+20
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L149+24
	mov	r1, r3
	ldr	r2, .L149+48
	bl	Alert_Message_va
	.loc 1 1303 0
	mov	r3, #1
	str	r3, [fp, #-24]
	b	.L137
.L140:
	.loc 1 1308 0
	ldr	r0, [fp, #-20]
	mov	r1, #2
	bl	add_duplicate_ft_station
	str	r0, [fp, #-52]
	.loc 1 1310 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	bne	.L137
	.loc 1 1315 0
	ldr	r0, .L149+20
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L149+32
	mov	r1, r3
	ldr	r2, .L149+52
	bl	Alert_Message_va
	b	.L137
.L139:
	.loc 1 1330 0
	ldr	r0, .L149+20
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L149+40
	mov	r1, r3
	ldr	r2, .L149+56
	bl	Alert_Message_va
	.loc 1 1332 0
	mov	r3, #1
	str	r3, [fp, #-24]
.L137:
	.loc 1 1338 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L143
	.loc 1 1340 0
	bl	FTIMES_TASK_restart_calculation
	.loc 1 1343 0
	b	.L116
.L143:
	.loc 1 1346 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L121
	.loc 1 1348 0
	mov	r3, #0
	str	r3, [fp, #-32]
	b	.L144
.L147:
	.loc 1 1350 0
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-32]
	add	r2, r2, #9
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	ftimes_number_of_starts_for_this_manual_program
	str	r0, [fp, #-36]
	.loc 1 1352 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L145
	.loc 1 1356 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L146
	.loc 1 1358 0
	ldr	r3, [fp, #-48]
	ldr	r2, [r3, #60]
	ldr	r3, [fp, #-48]
	ldr	r1, [fp, #-32]
	add	r1, r1, #9
	ldr	r3, [r3, r1, asl #2]
	ldr	r3, [r3, #76]
	ldr	r1, [fp, #-36]
	mul	r3, r1, r3
	add	r2, r2, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #60]
	b	.L145
.L146:
	.loc 1 1362 0
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-32]
	add	r2, r2, #9
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-48]
	str	r2, [r3, #72]
	.loc 1 1364 0
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-32]
	add	r2, r2, #9
	ldr	r3, [r3, r2, asl #2]
	ldr	r3, [r3, #76]
	ldr	r2, [fp, #-36]
	mul	r2, r3, r2
	ldr	r3, [fp, #-48]
	str	r2, [r3, #60]
	.loc 1 1366 0
	ldr	r2, [fp, #-48]
	ldrh	r3, [r2, #104]	@ movhi
	bic	r3, r3, #832
	orr	r3, r3, #128
	strh	r3, [r2, #104]	@ movhi
	.loc 1 1370 0
	ldr	r0, [fp, #-48]
	bl	ftimes_add_to_the_irrigation_list
	.loc 1 1378 0
	mov	r3, #1
	str	r3, [fp, #-40]
.L145:
	.loc 1 1348 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #1
	str	r3, [fp, #-32]
.L144:
	.loc 1 1348 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-32]
	cmp	r3, #1
	bls	.L147
.L121:
	.loc 1 943 0 is_stmt 1
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L120:
	.loc 1 943 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-20]
	ldr	r3, .L149+60
	cmp	r2, r3
	bls	.L148
.L116:
	.loc 1 1397 0 is_stmt 1
	sub	sp, fp, #16
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, fp, pc}
.L150:
	.align	2
.L149:
	.word	1114636288
	.word	ft_stations
	.word	ftcs
	.word	ftcs+8
	.word	ft_irrigating_stations_list_hdr
	.word	.LC0
	.word	.LC9
	.word	1041
	.word	.LC10
	.word	1055
	.word	.LC11
	.word	1066
	.word	1301
	.word	1315
	.word	1330
	.word	767
.LFE14:
	.size	FTIMES_FUNCS_check_for_a_start, .-FTIMES_FUNCS_check_for_a_start
	.section	.text.ftimes_will_exceed_system_capacity,"ax",%progbits
	.align	2
	.global	ftimes_will_exceed_system_capacity
	.type	ftimes_will_exceed_system_capacity, %function
ftimes_will_exceed_system_capacity:
.LFB15:
	.loc 1 1401 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI46:
	add	fp, sp, #0
.LCFI47:
	sub	sp, sp, #16
.LCFI48:
	str	r0, [fp, #-16]
	.loc 1 1415 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 1419 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L152
	.loc 1 1427 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #96]
	cmp	r3, #1
	beq	.L153
	.loc 1 1427 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L154
.L153:
	.loc 1 1429 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #20]
	str	r3, [fp, #-8]
	.loc 1 1431 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L155
.L154:
	.loc 1 1435 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #24]
	str	r3, [fp, #-8]
	.loc 1 1437 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L155:
	.loc 1 1440 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	bne	.L156
	.loc 1 1444 0
	ldr	r3, [fp, #-16]
	ldrh	r3, [r3, #102]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bls	.L156
	.loc 1 1465 0
	ldr	r3, [fp, #-16]
	ldrh	r3, [r3, #102]
	str	r3, [fp, #-8]
.L156:
	.loc 1 1469 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #28]
	ldr	r2, [r3, #44]
	ldr	r3, [fp, #-16]
	ldrh	r3, [r3, #102]
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bls	.L152
	.loc 1 1471 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L152:
	.loc 1 1475 0
	ldr	r3, [fp, #-4]
	.loc 1 1476 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE15:
	.size	ftimes_will_exceed_system_capacity, .-ftimes_will_exceed_system_capacity
	.section	.text.ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON,"ax",%progbits
	.align	2
	.global	ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON
	.type	ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON, %function
ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON:
.LFB16:
	.loc 1 1480 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI49:
	add	fp, sp, #0
.LCFI50:
	sub	sp, sp, #8
.LCFI51:
	str	r0, [fp, #-8]
	.loc 1 1485 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 1490 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [r3, #36]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #32]
	cmp	r2, r3
	bcs	.L158
	.loc 1 1494 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	ldr	r2, [r3, #152]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #36]
	cmp	r2, r3
	bls	.L158
	.loc 1 1498 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L158:
	.loc 1 1504 0
	ldr	r3, [fp, #-4]
	.loc 1 1505 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE16:
	.size	ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON, .-ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON
	.section	.text.ftimes_can_another_valve_come_ON_within_this_station_group,"ax",%progbits
	.align	2
	.global	ftimes_can_another_valve_come_ON_within_this_station_group
	.type	ftimes_can_another_valve_come_ON_within_this_station_group, %function
ftimes_can_another_valve_come_ON_within_this_station_group:
.LFB17:
	.loc 1 1509 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI52:
	add	fp, sp, #0
.LCFI53:
	sub	sp, sp, #8
.LCFI54:
	str	r0, [fp, #-8]
	.loc 1 1519 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 1523 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	ldr	r2, [r3, #156]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #148]
	cmp	r2, r3
	bcs	.L160
	.loc 1 1526 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L160:
	.loc 1 1531 0
	ldr	r3, [fp, #-4]
	.loc 1 1532 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE17:
	.size	ftimes_can_another_valve_come_ON_within_this_station_group, .-ftimes_can_another_valve_come_ON_within_this_station_group
	.section .rodata
	.align	2
.LC12:
	.ascii	"FTIMES: too many valves ON in mainline\000"
	.section	.text.ftimes_merge_new_mainline_allowed_ON_into_the_limit,"ax",%progbits
	.align	2
	.global	ftimes_merge_new_mainline_allowed_ON_into_the_limit
	.type	ftimes_merge_new_mainline_allowed_ON_into_the_limit, %function
ftimes_merge_new_mainline_allowed_ON_into_the_limit:
.LFB18:
	.loc 1 1536 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI55:
	add	fp, sp, #4
.LCFI56:
	sub	sp, sp, #8
.LCFI57:
	str	r0, [fp, #-12]
	.loc 1 1541 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #152]
	str	r3, [fp, #-8]
	.loc 1 1543 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #28]
	ldr	r2, [r3, #32]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bls	.L162
	.loc 1 1546 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #32]
.L162:
	.loc 1 1552 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #28]
	ldr	r2, [r3, #36]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #32]
	cmp	r2, r3
	bls	.L161
	.loc 1 1554 0
	ldr	r0, .L164
	bl	Alert_Message
.L161:
	.loc 1 1556 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L165:
	.align	2
.L164:
	.word	.LC12
.LFE18:
	.size	ftimes_merge_new_mainline_allowed_ON_into_the_limit, .-ftimes_merge_new_mainline_allowed_ON_into_the_limit
	.section .rodata
	.align	2
.LC13:
	.ascii	"FTIMES: too many valves ON in station group\000"
	.section	.text.ftimes_bump_this_station_groups_on_at_a_time_ON_count,"ax",%progbits
	.align	2
	.global	ftimes_bump_this_station_groups_on_at_a_time_ON_count
	.type	ftimes_bump_this_station_groups_on_at_a_time_ON_count, %function
ftimes_bump_this_station_groups_on_at_a_time_ON_count:
.LFB19:
	.loc 1 1559 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI58:
	add	fp, sp, #4
.LCFI59:
	sub	sp, sp, #4
.LCFI60:
	str	r0, [fp, #-8]
	.loc 1 1562 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #32]
	ldr	r2, [r2, #156]
	add	r2, r2, #1
	str	r2, [r3, #156]
	.loc 1 1564 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	ldr	r2, [r3, #156]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #148]
	cmp	r2, r3
	bls	.L166
	.loc 1 1566 0
	ldr	r0, .L168
	bl	Alert_Message
.L166:
	.loc 1 1568 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L169:
	.align	2
.L168:
	.word	.LC13
.LFE19:
	.size	ftimes_bump_this_station_groups_on_at_a_time_ON_count, .-ftimes_bump_this_station_groups_on_at_a_time_ON_count
	.section	.text.ftimes_set_remaining_seconds_on,"ax",%progbits
	.align	2
	.global	ftimes_set_remaining_seconds_on
	.type	ftimes_set_remaining_seconds_on, %function
ftimes_set_remaining_seconds_on:
.LFB20:
	.loc 1 1572 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI61:
	add	fp, sp, #0
.LCFI62:
	sub	sp, sp, #4
.LCFI63:
	str	r0, [fp, #-4]
	.loc 1 1575 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #64]
	.loc 1 1578 0
	ldr	r3, [fp, #-4]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #128
	beq	.L171
	.loc 1 1578 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L170
.L171:
	.loc 1 1585 0 is_stmt 1
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #60]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #72]
	cmp	r2, r3
	bls	.L173
	.loc 1 1587 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #72]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #64]
	.loc 1 1589 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #60]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #72]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-4]
	str	r2, [r3, #60]
	b	.L170
.L173:
	.loc 1 1593 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #60]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #64]
	.loc 1 1595 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #60]
.L170:
	.loc 1 1598 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE20:
	.size	ftimes_set_remaining_seconds_on, .-ftimes_set_remaining_seconds_on
	.section	.text.ftimes_init_ufim_variables_for_all_systems,"ax",%progbits
	.align	2
	.global	ftimes_init_ufim_variables_for_all_systems
	.type	ftimes_init_ufim_variables_for_all_systems, %function
ftimes_init_ufim_variables_for_all_systems:
.LFB21:
	.loc 1 1602 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI64:
	add	fp, sp, #4
.LCFI65:
	sub	sp, sp, #4
.LCFI66:
	.loc 1 1605 0
	ldr	r0, .L177
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 1607 0
	b	.L175
.L176:
	.loc 1 1619 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 1622 0
	ldr	r3, [fp, #-8]
	mvn	r2, #0
	str	r2, [r3, #32]
	.loc 1 1625 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #48]
	.loc 1 1628 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #52]
	.loc 1 1630 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #56]
	.loc 1 1633 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #60]
	.loc 1 1635 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #64]
	.loc 1 1637 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #68]
	.loc 1 1640 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #72]
	.loc 1 1643 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 1645 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #80]
	.loc 1 1648 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #84]
	.loc 1 1650 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #88]
	.loc 1 1658 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #96]
	.loc 1 1659 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #100]
	.loc 1 1663 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #104]
	.loc 1 1664 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #108]
	.loc 1 1665 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #112]
	.loc 1 1669 0
	ldr	r0, .L177
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L175:
	.loc 1 1607 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L176
	.loc 1 1676 0
	ldr	r3, .L177+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1677 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L178:
	.align	2
.L177:
	.word	ft_system_groups_list_hdr
	.word	ufim_one_or_more_in_the_list_for_manual_program
.LFE21:
	.size	ftimes_init_ufim_variables_for_all_systems, .-ftimes_init_ufim_variables_for_all_systems
	.section	.text.ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups,"ax",%progbits
	.align	2
	.global	ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups
	.type	ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups, %function
ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups:
.LFB22:
	.loc 1 1681 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI67:
	add	fp, sp, #4
.LCFI68:
	sub	sp, sp, #4
.LCFI69:
	.loc 1 1684 0
	ldr	r0, .L182
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 1686 0
	b	.L180
.L181:
	.loc 1 1688 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #156]
	.loc 1 1694 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #184]
	.loc 1 1696 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #188]
	.loc 1 1698 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #208]
	.loc 1 1702 0
	ldr	r0, .L182
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L180:
	.loc 1 1686 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L181
	.loc 1 1704 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L183:
	.align	2
.L182:
	.word	ft_station_groups_list_hdr
.LFE22:
	.size	ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups, .-ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups
	.section	.text.ftimes_for_each_station_group_check_if_irrigation_just_completed,"ax",%progbits
	.align	2
	.global	ftimes_for_each_station_group_check_if_irrigation_just_completed
	.type	ftimes_for_each_station_group_check_if_irrigation_just_completed, %function
ftimes_for_each_station_group_check_if_irrigation_just_completed:
.LFB23:
	.loc 1 1708 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI70:
	add	fp, sp, #4
.LCFI71:
	sub	sp, sp, #4
.LCFI72:
	.loc 1 1715 0
	ldr	r0, .L191
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 1717 0
	b	.L185
.L190:
	.loc 1 1720 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #184]
	cmp	r3, #0
	beq	.L186
	.loc 1 1722 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #188]
	cmp	r3, #0
	beq	.L187
	.loc 1 1728 0
	ldr	r3, .L191+4
	ldrh	r2, [r3, #12]
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #196]
	cmp	r2, r3
	bne	.L188
	.loc 1 1728 0 is_stmt 0 discriminator 1
	ldr	r3, .L191+4
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #192]
	cmp	r2, r3
	beq	.L189
.L188:
	.loc 1 1730 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #216]
	ldr	r3, .L191+4
	ldr	r3, [r3, #64]
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #216]
.L189:
	.loc 1 1733 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L191+4
	add	r3, r3, #236
	add	r2, r2, #8
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	b	.L186
.L187:
	.loc 1 1739 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #216]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #220]
	cmp	r2, r3
	bls	.L186
	.loc 1 1742 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #216]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #220]
	.loc 1 1745 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-8]
	add	r1, r2, #230
	add	r2, r3, #236
	mov	r3, #6
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	memcpy
	.loc 1 1748 0
	ldr	r3, [fp, #-8]
	ldrh	r2, [r3, #230]
	ldrh	r3, [r3, #232]
	mov	r3, r3, asl #16
	orr	r3, r3, r2
	add	r1, r3, #30
	ldr	r2, [fp, #-8]
	mov	r3, r1, asl #16
	mov	r3, r3, lsr #16
	mov	r0, #0
	orr	r3, r0, r3
	strh	r3, [r2, #230]	@ movhi
	mov	r3, r1, lsr #16
	mov	r1, #0
	orr	r3, r1, r3
	strh	r3, [r2, #232]	@ movhi
	.loc 1 1749 0
	ldr	r3, [fp, #-8]
	ldrh	r2, [r3, #230]
	ldrh	r3, [r3, #232]
	mov	r3, r3, asl #16
	orr	r3, r3, r2
	mov	r0, r3
	ldr	r3, [fp, #-8]
	ldrh	r2, [r3, #230]
	ldrh	r3, [r3, #232]
	mov	r3, r3, asl #16
	orr	r3, r3, r2
	mov	r1, r3
	ldr	r3, .L191+8
	umull	r2, r3, r1, r3
	mov	r2, r3, lsr #5
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	rsb	r2, r3, r1
	rsb	r1, r2, r0
	ldr	r2, [fp, #-8]
	mov	r3, r1, asl #16
	mov	r3, r3, lsr #16
	mov	r0, #0
	orr	r3, r0, r3
	strh	r3, [r2, #230]	@ movhi
	mov	r3, r1, lsr #16
	mov	r1, #0
	orr	r3, r1, r3
	strh	r3, [r2, #232]	@ movhi
	.loc 1 1752 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-8]
	add	r3, r3, #224
	add	r2, r2, #192
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 1755 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #200]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #244]
	.loc 1 1757 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #204]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #248]
	.loc 1 1759 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #212]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #252]
.L186:
	.loc 1 1766 0
	ldr	r0, .L191
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L185:
	.loc 1 1717 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L190
	.loc 1 1768 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L192:
	.align	2
.L191:
	.word	ft_station_groups_list_hdr
	.word	ftcs
	.word	-2004318071
.LFE23:
	.size	ftimes_for_each_station_group_check_if_irrigation_just_completed, .-ftimes_for_each_station_group_check_if_irrigation_just_completed
	.section	.text.ftimes_decrement_system_timers,"ax",%progbits
	.align	2
	.type	ftimes_decrement_system_timers, %function
ftimes_decrement_system_timers:
.LFB24:
	.loc 1 1772 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI73:
	add	fp, sp, #4
.LCFI74:
	sub	sp, sp, #4
.LCFI75:
	.loc 1 1775 0
	ldr	r0, .L197
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 1777 0
	b	.L194
.L196:
	.loc 1 1784 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #132]
	cmp	r3, #0
	beq	.L195
	.loc 1 1786 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #132]
	sub	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #132]
.L195:
	.loc 1 1791 0
	ldr	r0, .L197
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L194:
	.loc 1 1777 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L196
	.loc 1 1793 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L198:
	.align	2
.L197:
	.word	ft_system_groups_list_hdr
.LFE24:
	.size	ftimes_decrement_system_timers, .-ftimes_decrement_system_timers
	.section	.text.ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list,"ax",%progbits
	.align	2
	.type	ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list, %function
ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list:
.LFB25:
	.loc 1 1797 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI76:
	add	fp, sp, #0
.LCFI77:
	sub	sp, sp, #8
.LCFI78:
	str	r0, [fp, #-8]
	.loc 1 1809 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #106]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L200
	.loc 1 1809 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	bne	.L200
	.loc 1 1811 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L201
.L200:
	.loc 1 1815 0
	mov	r3, #0
	str	r3, [fp, #-4]
.L201:
	.loc 1 1820 0
	ldr	r3, [fp, #-4]
	.loc 1 1821 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE25:
	.size	ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list, .-ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list
	.section .rodata
	.align	2
.LC14:
	.ascii	"FTIMES: not on ON list!\000"
	.align	2
.LC15:
	.ascii	"FTIMES: -ve remaining seconds!!\000"
	.align	2
.LC16:
	.ascii	"FTIMES: is OFF but on the ON list\000"
	.section	.text.ftimes_irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations,"ax",%progbits
	.align	2
	.type	ftimes_irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations, %function
ftimes_irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations:
.LFB26:
	.loc 1 1825 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI79:
	add	fp, sp, #4
.LCFI80:
	sub	sp, sp, #20
.LCFI81:
	.loc 1 1841 0
	ldr	r0, .L228
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 1846 0
	b	.L203
.L227:
	.loc 1 1850 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L204
	.loc 1 1855 0
	ldr	r0, .L228
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
	.loc 1 1856 0
	b	.L203
.L204:
	.loc 1 1861 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #0]
	orr	r3, r3, #8
	strb	r3, [r2, #0]
	.loc 1 1872 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 1874 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1879 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L205
	.loc 1 1881 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #100]
	strh	r3, [fp, #-20]	@ movhi
	.loc 1 1882 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #96]
	str	r3, [fp, #-24]
	.loc 1 1884 0
	sub	r3, fp, #24
	ldr	r0, .L228+4
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L206
	.loc 1 1888 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #200]
	cmp	r3, #0
	bne	.L207
	.loc 1 1892 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	mov	r2, #0
	str	r2, [r3, #208]
.L207:
	.loc 1 1898 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	mov	r2, #1
	str	r2, [r3, #200]
	.loc 1 1902 0
	mov	r3, #1
	str	r3, [fp, #-16]
.L206:
	.loc 1 1910 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	mov	r2, #1
	str	r2, [r3, #184]
.L205:
	.loc 1 1918 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #28]
	ldr	r2, [r2, #28]
	add	r2, r2, #1
	str	r2, [r3, #28]
	.loc 1 1925 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #128
	bne	.L208
	.loc 1 1927 0
	ldr	r3, .L228+8
	mov	r2, #1
	str	r2, [r3, #0]
.L208:
	.loc 1 1941 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #84]
	cmp	r3, #0
	bne	.L209
	.loc 1 1946 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #104]
	cmp	r2, r3
	bls	.L210
	.loc 1 1949 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #104]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	and	r2, r2, #255
	str	r2, [r3, #104]
.L210:
	.loc 1 1953 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L211
	.loc 1 1953 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #108]
	cmp	r2, r3
	bls	.L211
	.loc 1 1955 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #104]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	and	r2, r2, #255
	str	r2, [r3, #108]
.L211:
	.loc 1 1958 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L209
	.loc 1 1958 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #112]
	cmp	r2, r3
	bls	.L209
	.loc 1 1960 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #104]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	and	r2, r2, #255
	str	r2, [r3, #112]
.L209:
	.loc 1 1975 0
	ldr	r0, [fp, #-8]
	bl	ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list
	mov	r3, r0
	cmp	r3, #0
	bne	.L212
	.loc 1 1977 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	ldr	r3, [r3, #208]
	cmp	r3, #0
	beq	.L212
	.loc 1 1979 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L212
	.loc 1 1981 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #32]
	ldr	r1, [r2, #204]
	ldr	r2, .L228+12
	ldr	r2, [r2, #64]
	add	r2, r1, r2
	str	r2, [r3, #204]
	.loc 1 1984 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	mov	r2, #0
	str	r2, [r3, #208]
.L212:
	.loc 1 1991 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #106]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L213
	.loc 1 1995 0
	ldr	r0, .L228+16
	ldr	r1, [fp, #-8]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	bne	.L214
	.loc 1 1997 0
	ldr	r0, .L228+20
	bl	Alert_Message
.L214:
	.loc 1 2003 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	mov	r2, r3
	ldr	r3, .L228+12
	ldr	r3, [r3, #64]
	rsb	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #64]
	.loc 1 2005 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	cmp	r3, #0
	bge	.L215
	.loc 1 2007 0
	ldr	r0, .L228+24
	bl	Alert_Message
.L215:
	.loc 1 2016 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	cmp	r3, #0
	bgt	.L216
	.loc 1 2021 0
	ldr	r0, .L228
	ldr	r1, [fp, #-8]
	bl	ftimes_if_station_is_ON_turn_it_OFF
	str	r0, [fp, #-8]
	.loc 1 2023 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L217
.L216:
	.loc 1 2029 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #107]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L218
	.loc 1 2031 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	mov	r2, #1
	str	r2, [r3, #88]
.L218:
	.loc 1 2037 0
	ldr	r0, [fp, #-8]
	bl	ftimes_merge_new_mainline_allowed_ON_into_the_limit
	.loc 1 2039 0
	ldr	r0, [fp, #-8]
	bl	ftimes_bump_this_station_groups_on_at_a_time_ON_count
	.loc 1 2044 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L219
	.loc 1 2046 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	mov	r2, #1
	str	r2, [r3, #96]
	b	.L217
.L219:
	.loc 1 2050 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	mov	r2, #1
	str	r2, [r3, #100]
	b	.L217
.L213:
	.loc 1 2064 0
	ldr	r0, .L228+16
	ldr	r1, [fp, #-8]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L220
	.loc 1 2066 0
	ldr	r0, .L228+28
	bl	Alert_Message
.L220:
	.loc 1 2072 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #84]
	cmp	r3, #0
	beq	.L221
	.loc 1 2075 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #84]
	ldr	r3, .L228+12
	ldr	r3, [r3, #64]
	cmp	r2, r3
	bls	.L222
	.loc 1 2077 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #84]
	ldr	r3, .L228+12
	ldr	r3, [r3, #64]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-8]
	str	r2, [r3, #84]
	b	.L221
.L222:
	.loc 1 2081 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #84]
.L221:
	.loc 1 2089 0
	ldr	r0, [fp, #-8]
	bl	ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list
	mov	r3, r0
	cmp	r3, #0
	beq	.L223
	.loc 1 2094 0
	ldr	r0, .L228
	ldr	r1, [fp, #-8]
	bl	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-8]
	.loc 1 2096 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 2101 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L217
.L223:
	.loc 1 2108 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L224
	.loc 1 2112 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #84]
	cmp	r3, #0
	bne	.L224
	.loc 1 2112 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	beq	.L224
	.loc 1 2116 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #104]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #3
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #52]
	cmp	r2, r3
	bls	.L225
	.loc 1 2116 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L225
	.loc 1 2118 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	ldrb	r2, [r2, #104]	@ zero_extendqisi2
	mov	r2, r2, lsr #4
	and	r2, r2, #3
	and	r2, r2, #255
	str	r2, [r3, #52]
.L225:
	.loc 1 2121 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #104]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #3
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #56]
	cmp	r2, r3
	bls	.L224
	.loc 1 2121 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L224
	.loc 1 2123 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	ldrb	r2, [r2, #104]	@ zero_extendqisi2
	mov	r2, r2, lsr #4
	and	r2, r2, #3
	and	r2, r2, #255
	str	r2, [r3, #56]
.L224:
	.loc 1 2135 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #84]
	cmp	r3, #0
	bne	.L217
	.loc 1 2135 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	beq	.L217
	.loc 1 2137 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L226
	.loc 1 2139 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	mov	r2, #1
	str	r2, [r3, #64]
	b	.L217
.L226:
	.loc 1 2143 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	mov	r2, #1
	str	r2, [r3, #68]
.L217:
	.loc 1 2155 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L203
	.loc 1 2157 0
	ldr	r0, .L228
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L203:
	.loc 1 1846 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L227
	.loc 1 2161 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L229:
	.align	2
.L228:
	.word	ft_irrigating_stations_list_hdr
	.word	ftcs+8
	.word	ufim_one_or_more_in_the_list_for_manual_program
	.word	ftcs
	.word	ft_stations_ON_list_hdr
	.word	.LC14
	.word	.LC15
	.word	.LC16
.LFE26:
	.size	ftimes_irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations, .-ftimes_irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations
	.section .rodata
	.align	2
.LC17:
	.ascii	"FOAL_IRRI: expected flow rate should be zero.\000"
	.align	2
.LC18:
	.ascii	"FTIMES: mix of flow groups ON.\000"
	.section	.text.ftimes_irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars,"ax",%progbits
	.align	2
	.type	ftimes_irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars, %function
ftimes_irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars:
.LFB27:
	.loc 1 2165 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI82:
	add	fp, sp, #4
.LCFI83:
	sub	sp, sp, #4
.LCFI84:
	.loc 1 2170 0
	ldr	r0, .L253
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 2172 0
	b	.L231
.L252:
	.loc 1 2178 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	bne	.L232
	.loc 1 2180 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #44]
	cmp	r3, #0
	beq	.L232
	.loc 1 2182 0
	ldr	r0, .L253+4
	bl	Alert_Message
	.loc 1 2184 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #44]
.L232:
	.loc 1 2199 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	cmp	r3, #0
	bne	.L233
	.loc 1 2201 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #40]
	b	.L234
.L233:
	.loc 1 2208 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #56]
	cmp	r2, r3
	bls	.L235
	.loc 1 2209 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #104]
	.loc 1 2208 0 discriminator 1
	cmp	r3, #1
	bne	.L235
	.loc 1 2211 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #40]
	b	.L234
.L235:
	.loc 1 2213 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #56]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #52]
	cmp	r2, r3
	bls	.L236
	.loc 1 2214 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #104]
	.loc 1 2213 0 discriminator 1
	cmp	r3, #1
	bne	.L236
	.loc 1 2216 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #40]
	b	.L234
.L236:
	.loc 1 2220 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #96]
	cmp	r3, #1
	bne	.L237
	.loc 1 2222 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #40]
	b	.L234
.L237:
	.loc 1 2225 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #100]
	cmp	r3, #1
	bne	.L238
	.loc 1 2227 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #40]
	b	.L234
.L238:
	.loc 1 2234 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #104]
	cmp	r3, #2
	bls	.L239
	.loc 1 2236 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #108]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #112]
	cmp	r2, r3
	bls	.L240
	.loc 1 2238 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #40]
	b	.L234
.L240:
	.loc 1 2240 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #108]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #112]
	cmp	r2, r3
	bcs	.L234
	.loc 1 2242 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #40]
	b	.L234
.L239:
	.loc 1 2261 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #72]
	cmp	r3, #1
	bne	.L241
	.loc 1 2263 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	cmp	r3, #1
	bne	.L242
	.loc 1 2264 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #76]
	.loc 1 2263 0 discriminator 1
	cmp	r3, #1
	bne	.L242
	.loc 1 2266 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #40]
	b	.L234
.L242:
	.loc 1 2268 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	cmp	r3, #0
	bne	.L244
	.loc 1 2269 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #80]
	.loc 1 2268 0 discriminator 1
	cmp	r3, #1
	bne	.L244
	.loc 1 2271 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #40]
	b	.L234
.L244:
	.loc 1 2275 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	cmp	r3, #1
	bne	.L245
	.loc 1 2277 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #40]
	.loc 1 2281 0
	b	.L234
.L245:
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #40]
	b	.L234
.L241:
	.loc 1 2289 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	cmp	r3, #1
	bne	.L246
	.loc 1 2290 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	.loc 1 2289 0 discriminator 1
	cmp	r3, #1
	bne	.L246
	.loc 1 2292 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #40]
	b	.L234
.L246:
	.loc 1 2294 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	cmp	r3, #0
	bne	.L247
	.loc 1 2295 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #68]
	.loc 1 2294 0 discriminator 1
	cmp	r3, #1
	bne	.L247
	.loc 1 2297 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #40]
	b	.L234
.L247:
	.loc 1 2301 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	cmp	r3, #1
	bne	.L248
	.loc 1 2303 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #40]
	b	.L234
.L248:
	.loc 1 2307 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #40]
.L234:
	.loc 1 2327 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L249
	.loc 1 2327 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #92]
	cmp	r3, #1
	bhi	.L249
	.loc 1 2329 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r3, r3, #116
	mov	r0, r3
	bl	FOAL_IRRI_there_is_more_than_one_flow_group_ON
	mov	r3, r0
	cmp	r3, #0
	beq	.L249
	.loc 1 2331 0
	ldr	r0, .L253+8
	bl	Alert_Message
.L249:
	.loc 1 2344 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #100]
	cmp	r3, #1
	bne	.L250
	.loc 1 2344 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #96]
	cmp	r3, #1
	bne	.L250
	.loc 1 2346 0 is_stmt 1
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #84]
	b	.L251
.L250:
	.loc 1 2350 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #84]
.L251:
	.loc 1 2355 0
	ldr	r0, .L253
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L231:
	.loc 1 2172 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L252
	.loc 1 2359 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L254:
	.align	2
.L253:
	.word	ft_system_groups_list_hdr
	.word	.LC17
	.word	.LC18
.LFE27:
	.size	ftimes_irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars, .-ftimes_irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars
	.section	.text.ftimes_irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON,"ax",%progbits
	.align	2
	.type	ftimes_irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON, %function
ftimes_irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON:
.LFB28:
	.loc 1 2363 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI85:
	add	fp, sp, #4
.LCFI86:
	sub	sp, sp, #4
.LCFI87:
	.loc 1 2368 0
	ldr	r0, .L259
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 2370 0
	b	.L256
.L258:
	.loc 1 2375 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L257
	.loc 1 2377 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	mov	r2, #1
	str	r2, [r3, #188]
.L257:
	.loc 1 2382 0
	ldr	r0, .L259
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L256:
	.loc 1 2370 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L258
	.loc 1 2389 0
	bl	ftimes_for_each_station_group_check_if_irrigation_just_completed
	.loc 1 2390 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L260:
	.align	2
.L259:
	.word	ft_irrigating_stations_list_hdr
.LFE28:
	.size	ftimes_irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON, .-ftimes_irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON
	.section .rodata
	.align	2
.LC19:
	.ascii	"FTIMES: run time 0\000"
	.section	.text.ftimes_irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON,"ax",%progbits
	.align	2
	.type	ftimes_irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON, %function
ftimes_irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON:
.LFB29:
	.loc 1 2394 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI88:
	add	fp, sp, #4
.LCFI89:
	sub	sp, sp, #16
.LCFI90:
	.loc 1 2422 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 2424 0
	ldr	r0, .L306
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
.L289:
.LBB2:
	.loc 1 2430 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L262
	.loc 1 2432 0
	ldr	r0, .L306
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L262:
	.loc 1 2435 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 2438 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L291
.L263:
	.loc 1 2443 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L265
	.loc 1 2449 0
	ldr	r3, .L306+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L265
	.loc 1 2451 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	mov	r2, #1
	str	r2, [r3, #212]
.L265:
	.loc 1 2465 0
	ldr	r0, [fp, #-8]
	bl	ftimes_station_is_eligible_to_be_removed_from_the_irrigating_list
	mov	r3, r0
	cmp	r3, #0
	bne	.L292
.L266:
	.loc 1 2474 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #106]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L293
.L268:
	.loc 1 2477 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #84]
	cmp	r3, #0
	bne	.L294
.L269:
	.loc 1 2486 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #132]
	cmp	r3, #0
	bne	.L295
.L270:
	.loc 1 2494 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #107]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L271
	.loc 1 2494 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #104]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	beq	.L271
	.loc 1 2496 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r2, r3, #104
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	add	r3, r3, #116
	mov	r0, r2
	mov	r1, r3
	bl	FOAL_IRRI_there_are_other_flow_check_groups_already_ON
	mov	r3, r0
	cmp	r3, #0
	bne	.L296
.L271:
	.loc 1 2506 0
	ldr	r0, [fp, #-8]
	bl	ftimes_will_exceed_system_capacity
	mov	r3, r0
	cmp	r3, #0
	bne	.L297
.L272:
	.loc 1 2512 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #84]
	cmp	r3, #1
	beq	.L298
.L273:
	.loc 1 2515 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L274
	.loc 1 2521 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #40]
	cmp	r3, #0
	beq	.L299
.L275:
	.loc 1 2527 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L276
	.loc 1 2527 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #96]
	cmp	r3, #0
	bne	.L276
	.loc 1 2527 0
	b	.L267
.L274:
	.loc 1 2531 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #40]
	cmp	r3, #1
	beq	.L300
.L277:
	.loc 1 2535 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #96]
	cmp	r3, #1
	beq	.L301
.L276:
	.loc 1 2541 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #48]
	ldr	r3, .L306+8
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-8]
	ldr	r1, [r3, #48]
	ldr	r3, .L306+12
	ldr	r3, [r3, r1, asl #2]
	cmp	r2, r3
	bcs	.L302
.L278:
	.loc 1 2550 0
	ldr	r0, [fp, #-8]
	bl	ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON
	mov	r3, r0
	cmp	r3, #0
	beq	.L303
.L279:
	.loc 1 2555 0
	ldr	r0, [fp, #-8]
	bl	ftimes_can_another_valve_come_ON_within_this_station_group
	mov	r3, r0
	cmp	r3, #0
	beq	.L304
.L280:
	.loc 1 2568 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 2573 0
	ldr	r0, .L306+16
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 2575 0
	b	.L281
.L283:
	.loc 1 2577 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #48]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #48]
	cmp	r2, r3
	bne	.L282
	.loc 1 2577 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #44]
	cmp	r2, r3
	bne	.L282
	.loc 1 2579 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-20]
.L282:
	.loc 1 2582 0
	ldr	r0, .L306+16
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L281:
	.loc 1 2575 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L283
	.loc 1 2585 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	beq	.L305
.L284:
	.loc 1 2589 0
	ldr	r0, [fp, #-8]
	bl	ftimes_set_remaining_seconds_on
	.loc 1 2591 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	cmp	r3, #0
	ble	.L285
	.loc 1 2599 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L286
	.loc 1 2601 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r3, [r3, #96]
	cmp	r3, #0
	bne	.L287
	.loc 1 2603 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L286
	.loc 1 2607 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	mov	r2, #1
	str	r2, [r3, #84]
	b	.L286
.L287:
	.loc 1 2612 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L286
	.loc 1 2616 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	mov	r2, #1
	str	r2, [r3, #84]
.L286:
	.loc 1 2623 0
	ldr	r0, [fp, #-8]
	bl	ftimes_complete_the_turn_ON
	.loc 1 2628 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #28]
	ldr	r1, [r2, #44]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #102]
	add	r2, r1, r2
	str	r2, [r3, #44]
	.loc 1 2631 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	ldrb	r2, [r2, #106]	@ zero_extendqisi2
	mov	r2, r2, lsr #2
	and	r2, r2, #3
	and	r2, r2, #255
	ldr	r1, [fp, #-8]
	ldr	r1, [r1, #28]
	ldr	r0, [fp, #-8]
	ldrb	r0, [r0, #106]	@ zero_extendqisi2
	mov	r0, r0, lsr #2
	and	r0, r0, #3
	and	r0, r0, #255
	add	r0, r0, #29
	ldr	r1, [r1, r0, asl #2]
	add	r1, r1, #1
	add	r2, r2, #29
	str	r1, [r3, r2, asl #2]
	.loc 1 2635 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #28]
	ldr	r2, [r2, #36]
	add	r2, r2, #1
	str	r2, [r3, #36]
	.loc 1 2640 0
	ldr	r0, [fp, #-8]
	bl	ftimes_merge_new_mainline_allowed_ON_into_the_limit
	.loc 1 2643 0
	ldr	r0, [fp, #-8]
	bl	ftimes_bump_this_station_groups_on_at_a_time_ON_count
	.loc 1 2648 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #105]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L288
	.loc 1 2650 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	mov	r2, #1
	str	r2, [r3, #96]
.LBE2:
	.loc 1 2664 0
	b	.L289
.L288:
.LBB3:
	.loc 1 2654 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	mov	r2, #1
	str	r2, [r3, #100]
.LBE3:
	.loc 1 2664 0
	b	.L289
.L285:
.LBB4:
	.loc 1 2661 0
	ldr	r0, .L306+20
	bl	Alert_Message
.LBE4:
	.loc 1 2664 0
	b	.L289
.L292:
.LBB5:
	.loc 1 2468 0
	mov	r0, r0	@ nop
	b	.L267
.L293:
	.loc 1 2474 0
	mov	r0, r0	@ nop
	b	.L267
.L294:
	.loc 1 2477 0
	mov	r0, r0	@ nop
	b	.L267
.L295:
	.loc 1 2486 0
	mov	r0, r0	@ nop
	b	.L267
.L296:
	.loc 1 2499 0
	mov	r0, r0	@ nop
	b	.L267
.L297:
	.loc 1 2506 0
	mov	r0, r0	@ nop
	b	.L267
.L298:
	.loc 1 2512 0
	mov	r0, r0	@ nop
	b	.L267
.L299:
	.loc 1 2521 0
	mov	r0, r0	@ nop
	b	.L267
.L300:
	.loc 1 2531 0
	mov	r0, r0	@ nop
	b	.L267
.L301:
	.loc 1 2535 0
	mov	r0, r0	@ nop
	b	.L267
.L302:
	.loc 1 2544 0
	mov	r0, r0	@ nop
	b	.L267
.L303:
	.loc 1 2552 0
	mov	r0, r0	@ nop
	b	.L267
.L304:
	.loc 1 2557 0
	mov	r0, r0	@ nop
	b	.L267
.L305:
	.loc 1 2585 0
	mov	r0, r0	@ nop
.L267:
.LBE5:
	.loc 1 2664 0
	b	.L289
.L291:
.LBB6:
	.loc 1 2438 0
	mov	r0, r0	@ nop
.L290:
.LBE6:
	.loc 1 2665 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L307:
	.align	2
.L306:
	.word	ft_irrigating_stations_list_hdr
	.word	ufim_one_or_more_in_the_list_for_manual_program
	.word	ft_stations_ON_by_controller
	.word	ft_electrical_limits
	.word	ft_stations_ON_list_hdr
	.word	.LC19
.LFE29:
	.size	ftimes_irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON, .-ftimes_irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON
	.section	.text.FTIMES_FUNCS_maintain_irrigation_list,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_maintain_irrigation_list
	.type	FTIMES_FUNCS_maintain_irrigation_list, %function
FTIMES_FUNCS_maintain_irrigation_list:
.LFB30:
	.loc 1 2669 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI91:
	add	fp, sp, #4
.LCFI92:
	.loc 1 2679 0
	bl	ftimes_decrement_system_timers
	.loc 1 2683 0
	bl	ftimes_init_ufim_variables_for_all_systems
	.loc 1 2685 0
	bl	ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups
	.loc 1 2691 0
	bl	ftimes_irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations
	.loc 1 2695 0
	bl	ftimes_irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars
	.loc 1 2697 0
	bl	ftimes_irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON
	.loc 1 2701 0
	bl	ftimes_irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON
	.loc 1 2702 0
	ldmfd	sp!, {fp, pc}
.LFE30:
	.size	FTIMES_FUNCS_maintain_irrigation_list, .-FTIMES_FUNCS_maintain_irrigation_list
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI44-.LCFI43
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI46-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI47-.LCFI46
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI49-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI50-.LCFI49
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI52-.LFB17
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI53-.LCFI52
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI55-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI56-.LCFI55
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI58-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI59-.LCFI58
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI61-.LFB20
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI62-.LCFI61
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI64-.LFB21
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI65-.LCFI64
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI67-.LFB22
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI68-.LCFI67
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI70-.LFB23
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI71-.LCFI70
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI73-.LFB24
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI74-.LCFI73
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI76-.LFB25
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI77-.LCFI76
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI79-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI80-.LCFI79
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI82-.LFB27
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI83-.LCFI82
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI85-.LFB28
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI86-.LCFI85
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI88-.LFB29
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI89-.LCFI88
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI91-.LFB30
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI92-.LCFI91
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE60:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_defs.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ftimes/ftimes_vars.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/station_groups.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ftimes/ftimes_task.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x19b5
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF320
	.byte	0x1
	.4byte	.LASF321
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0x30
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x4
	.byte	0x4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x5
	.4byte	0x51
	.4byte	0x76
	.uleb128 0x6
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF8
	.uleb128 0x7
	.4byte	.LASF9
	.byte	0x2
	.byte	0x3a
	.4byte	0x51
	.uleb128 0x7
	.4byte	.LASF10
	.byte	0x2
	.byte	0x4c
	.4byte	0x33
	.uleb128 0x7
	.4byte	.LASF11
	.byte	0x2
	.byte	0x5e
	.4byte	0x9e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x7
	.4byte	.LASF13
	.byte	0x2
	.byte	0x67
	.4byte	0x2c
	.uleb128 0x7
	.4byte	.LASF14
	.byte	0x2
	.byte	0x70
	.4byte	0x5f
	.uleb128 0x7
	.4byte	.LASF15
	.byte	0x2
	.byte	0x99
	.4byte	0x9e
	.uleb128 0x7
	.4byte	.LASF16
	.byte	0x2
	.byte	0x9d
	.4byte	0x9e
	.uleb128 0x5
	.4byte	0x93
	.4byte	0xe1
	.uleb128 0x6
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.byte	0x3
	.byte	0xba
	.4byte	0x30c
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x3
	.byte	0xbc
	.4byte	0x93
	.byte	0x4
	.byte	0x4
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x3
	.byte	0xc6
	.4byte	0x93
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x3
	.byte	0xc9
	.4byte	0x93
	.byte	0x4
	.byte	0x4
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x3
	.byte	0xcd
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x3
	.byte	0xcf
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x3
	.byte	0xd1
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x3
	.byte	0xd7
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x3
	.byte	0xdd
	.4byte	0x93
	.byte	0x4
	.byte	0x2
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x3
	.byte	0xdf
	.4byte	0x93
	.byte	0x4
	.byte	0x2
	.byte	0xe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x3
	.byte	0xf5
	.4byte	0x93
	.byte	0x4
	.byte	0x2
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x3
	.byte	0xfb
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x3
	.byte	0xff
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0xa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF29
	.byte	0x3
	.2byte	0x102
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x3
	.2byte	0x106
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF31
	.byte	0x3
	.2byte	0x10c
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF32
	.byte	0x3
	.2byte	0x111
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x3
	.2byte	0x117
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF34
	.byte	0x3
	.2byte	0x11e
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x3
	.2byte	0x120
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x3
	.2byte	0x128
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF37
	.byte	0x3
	.2byte	0x12a
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x3
	.2byte	0x12c
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x3
	.2byte	0x130
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x3
	.2byte	0x136
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF41
	.byte	0x3
	.2byte	0x13d
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF42
	.byte	0x3
	.2byte	0x13f
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x3
	.2byte	0x141
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF44
	.byte	0x3
	.2byte	0x143
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0x3
	.2byte	0x145
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF46
	.byte	0x3
	.2byte	0x147
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF47
	.byte	0x3
	.2byte	0x149
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x3
	.byte	0xb6
	.4byte	0x325
	.uleb128 0xc
	.4byte	.LASF85
	.byte	0x3
	.byte	0xb8
	.4byte	0xb0
	.uleb128 0xd
	.4byte	0xe1
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.byte	0x3
	.byte	0xb4
	.4byte	0x336
	.uleb128 0xe
	.4byte	0x30c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF48
	.byte	0x3
	.2byte	0x156
	.4byte	0x325
	.uleb128 0x10
	.byte	0x8
	.byte	0x3
	.2byte	0x163
	.4byte	0x5f8
	.uleb128 0xa
	.4byte	.LASF49
	.byte	0x3
	.2byte	0x16b
	.4byte	0x93
	.byte	0x4
	.byte	0x4
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0x3
	.2byte	0x171
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0x3
	.2byte	0x17c
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF52
	.byte	0x3
	.2byte	0x185
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF53
	.byte	0x3
	.2byte	0x19b
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF54
	.byte	0x3
	.2byte	0x19d
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF55
	.byte	0x3
	.2byte	0x19f
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF56
	.byte	0x3
	.2byte	0x1a1
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0x3
	.2byte	0x1a3
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF58
	.byte	0x3
	.2byte	0x1a5
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF59
	.byte	0x3
	.2byte	0x1a7
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF60
	.byte	0x3
	.2byte	0x1b1
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF61
	.byte	0x3
	.2byte	0x1b6
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF62
	.byte	0x3
	.2byte	0x1bb
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF63
	.byte	0x3
	.2byte	0x1c7
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0xe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF64
	.byte	0x3
	.2byte	0x1cd
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0xd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF65
	.byte	0x3
	.2byte	0x1d6
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF66
	.byte	0x3
	.2byte	0x1d8
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF67
	.byte	0x3
	.2byte	0x1e6
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0xa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF68
	.byte	0x3
	.2byte	0x1e7
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF17
	.byte	0x3
	.2byte	0x1e8
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF69
	.byte	0x3
	.2byte	0x1e9
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF70
	.byte	0x3
	.2byte	0x1ea
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF71
	.byte	0x3
	.2byte	0x1eb
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF72
	.byte	0x3
	.2byte	0x1ec
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF73
	.byte	0x3
	.2byte	0x1f6
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF74
	.byte	0x3
	.2byte	0x1f7
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x3
	.2byte	0x1f8
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF75
	.byte	0x3
	.2byte	0x1f9
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF76
	.byte	0x3
	.2byte	0x1fa
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF77
	.byte	0x3
	.2byte	0x1fb
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF78
	.byte	0x3
	.2byte	0x1fc
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF79
	.byte	0x3
	.2byte	0x206
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF80
	.byte	0x3
	.2byte	0x20d
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF81
	.byte	0x3
	.2byte	0x214
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF82
	.byte	0x3
	.2byte	0x216
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF83
	.byte	0x3
	.2byte	0x223
	.4byte	0x93
	.byte	0x4
	.byte	0x6
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF84
	.byte	0x3
	.2byte	0x227
	.4byte	0x93
	.byte	0x4
	.byte	0x4
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x11
	.byte	0x8
	.byte	0x3
	.2byte	0x15f
	.4byte	0x613
	.uleb128 0x12
	.4byte	.LASF86
	.byte	0x3
	.2byte	0x161
	.4byte	0xb0
	.uleb128 0xd
	.4byte	0x342
	.byte	0
	.uleb128 0x10
	.byte	0x8
	.byte	0x3
	.2byte	0x15d
	.4byte	0x625
	.uleb128 0xe
	.4byte	0x5f8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF87
	.byte	0x3
	.2byte	0x230
	.4byte	0x613
	.uleb128 0x8
	.byte	0x6
	.byte	0x4
	.byte	0x22
	.4byte	0x652
	.uleb128 0x13
	.ascii	"T\000"
	.byte	0x4
	.byte	0x24
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.ascii	"D\000"
	.byte	0x4
	.byte	0x26
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x7
	.4byte	.LASF88
	.byte	0x4
	.byte	0x28
	.4byte	0x631
	.uleb128 0x8
	.byte	0x14
	.byte	0x4
	.byte	0x31
	.4byte	0x6e4
	.uleb128 0x14
	.4byte	.LASF89
	.byte	0x4
	.byte	0x33
	.4byte	0x652
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF90
	.byte	0x4
	.byte	0x35
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x14
	.4byte	.LASF91
	.byte	0x4
	.byte	0x35
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x14
	.4byte	.LASF92
	.byte	0x4
	.byte	0x35
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x14
	.4byte	.LASF93
	.byte	0x4
	.byte	0x37
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x14
	.4byte	.LASF94
	.byte	0x4
	.byte	0x37
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x14
	.4byte	.LASF95
	.byte	0x4
	.byte	0x37
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x14
	.4byte	.LASF96
	.byte	0x4
	.byte	0x39
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0x14
	.4byte	.LASF97
	.byte	0x4
	.byte	0x3b
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x7
	.4byte	.LASF98
	.byte	0x4
	.byte	0x3d
	.4byte	0x65d
	.uleb128 0x8
	.byte	0x14
	.byte	0x5
	.byte	0x18
	.4byte	0x73e
	.uleb128 0x14
	.4byte	.LASF99
	.byte	0x5
	.byte	0x1a
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF100
	.byte	0x5
	.byte	0x1c
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x14
	.4byte	.LASF101
	.byte	0x5
	.byte	0x1e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x14
	.4byte	.LASF102
	.byte	0x5
	.byte	0x20
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x14
	.4byte	.LASF103
	.byte	0x5
	.byte	0x23
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x7
	.4byte	.LASF104
	.byte	0x5
	.byte	0x26
	.4byte	0x6ef
	.uleb128 0x7
	.4byte	.LASF105
	.byte	0x5
	.byte	0x26
	.4byte	0x754
	.uleb128 0x15
	.byte	0x4
	.4byte	0x6ef
	.uleb128 0x8
	.byte	0xc
	.byte	0x5
	.byte	0x2a
	.4byte	0x78d
	.uleb128 0x14
	.4byte	.LASF106
	.byte	0x5
	.byte	0x2c
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF107
	.byte	0x5
	.byte	0x2e
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x14
	.4byte	.LASF108
	.byte	0x5
	.byte	0x30
	.4byte	0x78d
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x15
	.byte	0x4
	.4byte	0x73e
	.uleb128 0x7
	.4byte	.LASF109
	.byte	0x5
	.byte	0x32
	.4byte	0x75a
	.uleb128 0x16
	.2byte	0x100
	.byte	0x6
	.byte	0x4c
	.4byte	0xa12
	.uleb128 0x14
	.4byte	.LASF110
	.byte	0x6
	.byte	0x4e
	.4byte	0x793
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF111
	.byte	0x6
	.byte	0x50
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x14
	.4byte	.LASF112
	.byte	0x6
	.byte	0x53
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x14
	.4byte	.LASF113
	.byte	0x6
	.byte	0x55
	.4byte	0xa12
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x14
	.4byte	.LASF114
	.byte	0x6
	.byte	0x5a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x14
	.4byte	.LASF115
	.byte	0x6
	.byte	0x5f
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x14
	.4byte	.LASF116
	.byte	0x6
	.byte	0x62
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x14
	.4byte	.LASF117
	.byte	0x6
	.byte	0x65
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x14
	.4byte	.LASF118
	.byte	0x6
	.byte	0x6a
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x14
	.4byte	.LASF119
	.byte	0x6
	.byte	0x6c
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x14
	.4byte	.LASF120
	.byte	0x6
	.byte	0x6e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x14
	.4byte	.LASF121
	.byte	0x6
	.byte	0x70
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x14
	.4byte	.LASF122
	.byte	0x6
	.byte	0x77
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x14
	.4byte	.LASF123
	.byte	0x6
	.byte	0x79
	.4byte	0xa22
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x14
	.4byte	.LASF124
	.byte	0x6
	.byte	0x80
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x14
	.4byte	.LASF125
	.byte	0x6
	.byte	0x82
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x14
	.4byte	.LASF126
	.byte	0x6
	.byte	0x84
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x14
	.4byte	.LASF127
	.byte	0x6
	.byte	0x8f
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x14
	.4byte	.LASF128
	.byte	0x6
	.byte	0x94
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x14
	.4byte	.LASF129
	.byte	0x6
	.byte	0x98
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x14
	.4byte	.LASF130
	.byte	0x6
	.byte	0xa8
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x14
	.4byte	.LASF131
	.byte	0x6
	.byte	0xac
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x14
	.4byte	.LASF132
	.byte	0x6
	.byte	0xb0
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x14
	.4byte	.LASF133
	.byte	0x6
	.byte	0xb3
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x14
	.4byte	.LASF134
	.byte	0x6
	.byte	0xb7
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x14
	.4byte	.LASF135
	.byte	0x6
	.byte	0xb9
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x14
	.4byte	.LASF136
	.byte	0x6
	.byte	0xc1
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0x14
	.4byte	.LASF137
	.byte	0x6
	.byte	0xc6
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0x14
	.4byte	.LASF138
	.byte	0x6
	.byte	0xc8
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0xbc
	.uleb128 0x14
	.4byte	.LASF139
	.byte	0x6
	.byte	0xd3
	.4byte	0x652
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x14
	.4byte	.LASF140
	.byte	0x6
	.byte	0xd7
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0x14
	.4byte	.LASF141
	.byte	0x6
	.byte	0xda
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x14
	.4byte	.LASF142
	.byte	0x6
	.byte	0xe0
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x14
	.4byte	.LASF143
	.byte	0x6
	.byte	0xe4
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0x14
	.4byte	.LASF144
	.byte	0x6
	.byte	0xeb
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0x14
	.4byte	.LASF145
	.byte	0x6
	.byte	0xed
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xdc
	.uleb128 0x14
	.4byte	.LASF146
	.byte	0x6
	.byte	0xf3
	.4byte	0x652
	.byte	0x3
	.byte	0x23
	.uleb128 0xe0
	.uleb128 0x14
	.4byte	.LASF147
	.byte	0x6
	.byte	0xf6
	.4byte	0x652
	.byte	0x3
	.byte	0x23
	.uleb128 0xe6
	.uleb128 0x14
	.4byte	.LASF148
	.byte	0x6
	.byte	0xf8
	.4byte	0x652
	.byte	0x3
	.byte	0x23
	.uleb128 0xec
	.uleb128 0x14
	.4byte	.LASF149
	.byte	0x6
	.byte	0xfc
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0xf4
	.uleb128 0x17
	.4byte	.LASF150
	.byte	0x6
	.2byte	0x100
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xf8
	.uleb128 0x17
	.4byte	.LASF151
	.byte	0x6
	.2byte	0x104
	.4byte	0xbb
	.byte	0x3
	.byte	0x23
	.uleb128 0xfc
	.byte	0
	.uleb128 0x5
	.4byte	0x93
	.4byte	0xa22
	.uleb128 0x6
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x5
	.4byte	0xbb
	.4byte	0xa32
	.uleb128 0x6
	.4byte	0x25
	.byte	0x6
	.byte	0
	.uleb128 0xf
	.4byte	.LASF152
	.byte	0x6
	.2byte	0x106
	.4byte	0x79e
	.uleb128 0x10
	.byte	0x50
	.byte	0x6
	.2byte	0x10f
	.4byte	0xab1
	.uleb128 0x17
	.4byte	.LASF153
	.byte	0x6
	.2byte	0x111
	.4byte	0x793
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x17
	.4byte	.LASF111
	.byte	0x6
	.2byte	0x113
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x17
	.4byte	.LASF154
	.byte	0x6
	.2byte	0x116
	.4byte	0xab1
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x17
	.4byte	.LASF155
	.byte	0x6
	.2byte	0x118
	.4byte	0xa22
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x17
	.4byte	.LASF156
	.byte	0x6
	.2byte	0x11a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x17
	.4byte	.LASF157
	.byte	0x6
	.2byte	0x11c
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x17
	.4byte	.LASF158
	.byte	0x6
	.2byte	0x11e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.byte	0
	.uleb128 0x5
	.4byte	0x93
	.4byte	0xac1
	.uleb128 0x6
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0xf
	.4byte	.LASF159
	.byte	0x6
	.2byte	0x120
	.4byte	0xa3e
	.uleb128 0x10
	.byte	0x90
	.byte	0x6
	.2byte	0x129
	.4byte	0xc9b
	.uleb128 0x17
	.4byte	.LASF160
	.byte	0x6
	.2byte	0x131
	.4byte	0x793
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x17
	.4byte	.LASF161
	.byte	0x6
	.2byte	0x139
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x17
	.4byte	.LASF162
	.byte	0x6
	.2byte	0x13e
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x17
	.4byte	.LASF163
	.byte	0x6
	.2byte	0x140
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x17
	.4byte	.LASF164
	.byte	0x6
	.2byte	0x142
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x17
	.4byte	.LASF165
	.byte	0x6
	.2byte	0x149
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x17
	.4byte	.LASF166
	.byte	0x6
	.2byte	0x151
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x17
	.4byte	.LASF167
	.byte	0x6
	.2byte	0x158
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x17
	.4byte	.LASF168
	.byte	0x6
	.2byte	0x173
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x17
	.4byte	.LASF169
	.byte	0x6
	.2byte	0x17d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x17
	.4byte	.LASF170
	.byte	0x6
	.2byte	0x199
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x17
	.4byte	.LASF171
	.byte	0x6
	.2byte	0x19d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x17
	.4byte	.LASF172
	.byte	0x6
	.2byte	0x19f
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x17
	.4byte	.LASF173
	.byte	0x6
	.2byte	0x1a9
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x17
	.4byte	.LASF174
	.byte	0x6
	.2byte	0x1ad
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x17
	.4byte	.LASF175
	.byte	0x6
	.2byte	0x1af
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x17
	.4byte	.LASF176
	.byte	0x6
	.2byte	0x1b7
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x17
	.4byte	.LASF177
	.byte	0x6
	.2byte	0x1c1
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x17
	.4byte	.LASF178
	.byte	0x6
	.2byte	0x1c3
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x17
	.4byte	.LASF179
	.byte	0x6
	.2byte	0x1cc
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x17
	.4byte	.LASF180
	.byte	0x6
	.2byte	0x1d1
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x17
	.4byte	.LASF181
	.byte	0x6
	.2byte	0x1d9
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x17
	.4byte	.LASF182
	.byte	0x6
	.2byte	0x1e3
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x17
	.4byte	.LASF183
	.byte	0x6
	.2byte	0x1e5
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x17
	.4byte	.LASF184
	.byte	0x6
	.2byte	0x1e9
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x17
	.4byte	.LASF185
	.byte	0x6
	.2byte	0x1eb
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x17
	.4byte	.LASF186
	.byte	0x6
	.2byte	0x1ed
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x17
	.4byte	.LASF187
	.byte	0x6
	.2byte	0x1f4
	.4byte	0xc9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x17
	.4byte	.LASF188
	.byte	0x6
	.2byte	0x1fc
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x18
	.ascii	"sbf\000"
	.byte	0x6
	.2byte	0x203
	.4byte	0x625
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.byte	0
	.uleb128 0x5
	.4byte	0x93
	.4byte	0xcab
	.uleb128 0x6
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0xf
	.4byte	.LASF189
	.byte	0x6
	.2byte	0x205
	.4byte	0xacd
	.uleb128 0x10
	.byte	0x4
	.byte	0x6
	.2byte	0x213
	.4byte	0xd09
	.uleb128 0xa
	.4byte	.LASF190
	.byte	0x6
	.2byte	0x215
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF191
	.byte	0x6
	.2byte	0x21d
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF192
	.byte	0x6
	.2byte	0x227
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF193
	.byte	0x6
	.2byte	0x233
	.4byte	0xc6
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x11
	.byte	0x4
	.byte	0x6
	.2byte	0x20f
	.4byte	0xd24
	.uleb128 0x12
	.4byte	.LASF85
	.byte	0x6
	.2byte	0x211
	.4byte	0x93
	.uleb128 0xd
	.4byte	0xcb7
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0x6
	.2byte	0x20d
	.4byte	0xd36
	.uleb128 0xe
	.4byte	0xd09
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF194
	.byte	0x6
	.2byte	0x23b
	.4byte	0xd24
	.uleb128 0x10
	.byte	0x70
	.byte	0x6
	.2byte	0x23e
	.4byte	0xea5
	.uleb128 0x17
	.4byte	.LASF195
	.byte	0x6
	.2byte	0x249
	.4byte	0xd36
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x17
	.4byte	.LASF196
	.byte	0x6
	.2byte	0x24b
	.4byte	0x793
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x17
	.4byte	.LASF197
	.byte	0x6
	.2byte	0x24d
	.4byte	0x793
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x17
	.4byte	.LASF198
	.byte	0x6
	.2byte	0x251
	.4byte	0xea5
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x17
	.4byte	.LASF199
	.byte	0x6
	.2byte	0x253
	.4byte	0xeab
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x17
	.4byte	.LASF200
	.byte	0x6
	.2byte	0x258
	.4byte	0xeb1
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x17
	.4byte	.LASF201
	.byte	0x6
	.2byte	0x25c
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x17
	.4byte	.LASF202
	.byte	0x6
	.2byte	0x25e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x17
	.4byte	.LASF203
	.byte	0x6
	.2byte	0x266
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x17
	.4byte	.LASF204
	.byte	0x6
	.2byte	0x26a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x17
	.4byte	.LASF205
	.byte	0x6
	.2byte	0x26e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x17
	.4byte	.LASF206
	.byte	0x6
	.2byte	0x272
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x17
	.4byte	.LASF207
	.byte	0x6
	.2byte	0x277
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x17
	.4byte	.LASF208
	.byte	0x6
	.2byte	0x27c
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x17
	.4byte	.LASF209
	.byte	0x6
	.2byte	0x281
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x17
	.4byte	.LASF210
	.byte	0x6
	.2byte	0x285
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x17
	.4byte	.LASF211
	.byte	0x6
	.2byte	0x288
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x17
	.4byte	.LASF212
	.byte	0x6
	.2byte	0x28c
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x17
	.4byte	.LASF213
	.byte	0x6
	.2byte	0x28e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x17
	.4byte	.LASF214
	.byte	0x6
	.2byte	0x298
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x17
	.4byte	.LASF215
	.byte	0x6
	.2byte	0x29a
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x17
	.4byte	.LASF216
	.byte	0x6
	.2byte	0x29f
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0x66
	.uleb128 0x18
	.ascii	"bbf\000"
	.byte	0x6
	.2byte	0x2a3
	.4byte	0x336
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.byte	0
	.uleb128 0x15
	.byte	0x4
	.4byte	0xcab
	.uleb128 0x15
	.byte	0x4
	.4byte	0xa32
	.uleb128 0x5
	.4byte	0xec1
	.4byte	0xec1
	.uleb128 0x6
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x15
	.byte	0x4
	.4byte	0xac1
	.uleb128 0xf
	.4byte	.LASF217
	.byte	0x6
	.2byte	0x2a5
	.4byte	0xd42
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF218
	.uleb128 0x10
	.byte	0x30
	.byte	0x7
	.2byte	0x1af
	.4byte	0xf3d
	.uleb128 0x17
	.4byte	.LASF219
	.byte	0x7
	.2byte	0x1b1
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x17
	.4byte	.LASF119
	.byte	0x7
	.2byte	0x1b3
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x17
	.4byte	.LASF220
	.byte	0x7
	.2byte	0x1b5
	.4byte	0xa22
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x17
	.4byte	.LASF221
	.byte	0x7
	.2byte	0x1b7
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x17
	.4byte	.LASF222
	.byte	0x7
	.2byte	0x1b9
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x18
	.ascii	"sx\000"
	.byte	0x7
	.2byte	0x1bb
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.byte	0
	.uleb128 0xf
	.4byte	.LASF223
	.byte	0x7
	.2byte	0x1bd
	.4byte	0xeda
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF224
	.uleb128 0x8
	.byte	0x44
	.byte	0x8
	.byte	0x3d
	.4byte	0x1001
	.uleb128 0x14
	.4byte	.LASF225
	.byte	0x8
	.byte	0x40
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF226
	.byte	0x8
	.byte	0x46
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x14
	.4byte	.LASF227
	.byte	0x8
	.byte	0x4d
	.4byte	0x6e4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x14
	.4byte	.LASF228
	.byte	0x8
	.byte	0x51
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x14
	.4byte	.LASF229
	.byte	0x8
	.byte	0x56
	.4byte	0x652
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x14
	.4byte	.LASF230
	.byte	0x8
	.byte	0x5c
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.4byte	.LASF231
	.byte	0x8
	.byte	0x5e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x14
	.4byte	.LASF232
	.byte	0x8
	.byte	0x60
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x14
	.4byte	.LASF233
	.byte	0x8
	.byte	0x63
	.4byte	0xed3
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x14
	.4byte	.LASF234
	.byte	0x8
	.byte	0x68
	.4byte	0xed3
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x14
	.4byte	.LASF235
	.byte	0x8
	.byte	0x6a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x14
	.4byte	.LASF236
	.byte	0x8
	.byte	0x6d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.byte	0
	.uleb128 0x7
	.4byte	.LASF237
	.byte	0x8
	.byte	0x6f
	.4byte	0xf50
	.uleb128 0x19
	.4byte	.LASF240
	.byte	0x1
	.byte	0x18
	.byte	0x1
	.4byte	0x1060
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1060
	.uleb128 0x1a
	.4byte	.LASF238
	.byte	0x1
	.byte	0x18
	.4byte	0x1066
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF239
	.byte	0x1
	.byte	0x18
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1b
	.ascii	"iii\000"
	.byte	0x1
	.byte	0x20
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1b
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x22
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x15
	.byte	0x4
	.4byte	0xec7
	.uleb128 0x1c
	.4byte	0x93
	.uleb128 0x19
	.4byte	.LASF241
	.byte	0x1
	.byte	0x54
	.byte	0x1
	.4byte	0x1060
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x10b1
	.uleb128 0x1a
	.4byte	.LASF238
	.byte	0x1
	.byte	0x54
	.4byte	0x1066
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.ascii	"iii\000"
	.byte	0x1
	.byte	0x5b
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x1b
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x5d
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF244
	.byte	0x1
	.byte	0x80
	.byte	0x1
	.4byte	0xbb
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x10f8
	.uleb128 0x1a
	.4byte	.LASF242
	.byte	0x1
	.byte	0x80
	.4byte	0xeab
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1a
	.4byte	.LASF243
	.byte	0x1
	.byte	0x80
	.4byte	0x10f8
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x82
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.byte	0x4
	.4byte	0xf3d
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF245
	.byte	0x1
	.byte	0x9c
	.byte	0x1
	.4byte	0xeab
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x1145
	.uleb128 0x1a
	.4byte	.LASF246
	.byte	0x1
	.byte	0x9c
	.4byte	0x1066
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xa6
	.4byte	0xeab
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1e
	.4byte	.LASF247
	.byte	0x1
	.byte	0xa8
	.4byte	0xeab
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF248
	.byte	0x1
	.byte	0xbe
	.byte	0x1
	.4byte	0xea5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x118c
	.uleb128 0x1a
	.4byte	.LASF246
	.byte	0x1
	.byte	0xbe
	.4byte	0x1066
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xc5
	.4byte	0xea5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1e
	.4byte	.LASF247
	.byte	0x1
	.byte	0xc7
	.4byte	0xea5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF249
	.byte	0x1
	.byte	0xdd
	.byte	0x1
	.4byte	0x1060
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x11e1
	.uleb128 0x1a
	.4byte	.LASF250
	.byte	0x1
	.byte	0xdd
	.4byte	0x11e1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF251
	.byte	0x1
	.byte	0xdd
	.4byte	0x11e6
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1b
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xdf
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1e
	.4byte	.LASF252
	.byte	0x1
	.byte	0xe1
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1c
	.4byte	0x749
	.uleb128 0x1c
	.4byte	0x1060
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF253
	.byte	0x1
	.2byte	0x1b9
	.byte	0x1
	.4byte	0x1060
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x1236
	.uleb128 0x20
	.4byte	.LASF250
	.byte	0x1
	.2byte	0x1b9
	.4byte	0x11e1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x1b9
	.4byte	0x11e6
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1c7
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x1e2
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x1260
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x1e2
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x20a
	.byte	0x1
	.4byte	0x93
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x12ab
	.uleb128 0x20
	.4byte	.LASF255
	.byte	0x1
	.2byte	0x20a
	.4byte	0xeab
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x20a
	.4byte	0x1066
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x20c
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x221
	.byte	0x1
	.4byte	0x93
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x12f4
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x221
	.4byte	0x12f4
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x223
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x225
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1c
	.4byte	0x12f9
	.uleb128 0x15
	.byte	0x4
	.4byte	0x12ff
	.uleb128 0x1c
	.4byte	0xac1
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x250
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x132e
	.uleb128 0x20
	.4byte	.LASF261
	.byte	0x1
	.2byte	0x250
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.4byte	.LASF262
	.byte	0x1
	.2byte	0x2a1
	.byte	0x1
	.4byte	0xbb
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x1387
	.uleb128 0x20
	.4byte	.LASF263
	.byte	0x1
	.2byte	0x2a1
	.4byte	0x1387
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x2a3
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x24
	.4byte	.LASF264
	.byte	0x1
	.2byte	0x2ab
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.4byte	.LASF265
	.byte	0x1
	.2byte	0x2ad
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1c
	.4byte	0xeab
	.uleb128 0x23
	.4byte	.LASF266
	.byte	0x1
	.2byte	0x31c
	.byte	0x1
	.4byte	0xbb
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x13c7
	.uleb128 0x24
	.4byte	.LASF199
	.byte	0x1
	.2byte	0x326
	.4byte	0xeab
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x328
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.4byte	.LASF267
	.byte	0x1
	.2byte	0x35a
	.byte	0x1
	.4byte	0xbb
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x1402
	.uleb128 0x24
	.4byte	.LASF247
	.byte	0x1
	.2byte	0x361
	.4byte	0xec1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x363
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF268
	.byte	0x1
	.2byte	0x37d
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x14b2
	.uleb128 0x24
	.4byte	.LASF269
	.byte	0x1
	.2byte	0x37f
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x24
	.4byte	.LASF270
	.byte	0x1
	.2byte	0x381
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x24
	.4byte	.LASF271
	.byte	0x1
	.2byte	0x383
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x24
	.4byte	.LASF272
	.byte	0x1
	.2byte	0x385
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x24
	.4byte	.LASF273
	.byte	0x1
	.2byte	0x387
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x21
	.ascii	"mp\000"
	.byte	0x1
	.2byte	0x389
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x24
	.4byte	.LASF274
	.byte	0x1
	.2byte	0x38b
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x24
	.4byte	.LASF275
	.byte	0x1
	.2byte	0x38d
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x24
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x38f
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x24
	.4byte	.LASF277
	.byte	0x1
	.2byte	0x393
	.4byte	0x14b2
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.byte	0
	.uleb128 0x25
	.4byte	0x14b7
	.uleb128 0x1c
	.4byte	0xed3
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF278
	.byte	0x1
	.2byte	0x578
	.byte	0x1
	.4byte	0xbb
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x1516
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x578
	.4byte	0x11e6
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x57f
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x24
	.4byte	.LASF279
	.byte	0x1
	.2byte	0x581
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x24
	.4byte	.LASF280
	.byte	0x1
	.2byte	0x583
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF281
	.byte	0x1
	.2byte	0x5c7
	.byte	0x1
	.4byte	0xbb
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x1552
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x5c7
	.4byte	0x11e6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x5c9
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF282
	.byte	0x1
	.2byte	0x5e4
	.byte	0x1
	.4byte	0xbb
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x158e
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x5e4
	.4byte	0x11e6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x5e6
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF283
	.byte	0x1
	.2byte	0x5ff
	.byte	0x1
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0x15c7
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x5ff
	.4byte	0x11e6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x24
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x603
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF285
	.byte	0x1
	.2byte	0x616
	.byte	0x1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0x15f1
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x616
	.4byte	0x11e6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF286
	.byte	0x1
	.2byte	0x623
	.byte	0x1
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.4byte	0x161b
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x623
	.4byte	0x11e6
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF287
	.byte	0x1
	.2byte	0x641
	.byte	0x1
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0x1645
	.uleb128 0x24
	.4byte	.LASF288
	.byte	0x1
	.2byte	0x643
	.4byte	0xea5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF289
	.byte	0x1
	.2byte	0x690
	.byte	0x1
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0x166f
	.uleb128 0x24
	.4byte	.LASF290
	.byte	0x1
	.2byte	0x692
	.4byte	0xeab
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF291
	.byte	0x1
	.2byte	0x6ab
	.byte	0x1
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST23
	.4byte	0x1699
	.uleb128 0x24
	.4byte	.LASF290
	.byte	0x1
	.2byte	0x6ad
	.4byte	0xeab
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x26
	.4byte	.LASF293
	.byte	0x1
	.2byte	0x6eb
	.byte	0x1
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST24
	.4byte	0x16c2
	.uleb128 0x24
	.4byte	.LASF288
	.byte	0x1
	.2byte	0x6ed
	.4byte	0xea5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.4byte	.LASF292
	.byte	0x1
	.2byte	0x704
	.byte	0x1
	.4byte	0xbb
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST25
	.4byte	0x16fd
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x704
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x709
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x26
	.4byte	.LASF294
	.byte	0x1
	.2byte	0x720
	.byte	0x1
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST26
	.4byte	0x1753
	.uleb128 0x24
	.4byte	.LASF270
	.byte	0x1
	.2byte	0x727
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x24
	.4byte	.LASF295
	.byte	0x1
	.2byte	0x729
	.4byte	0x652
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x24
	.4byte	.LASF296
	.byte	0x1
	.2byte	0x72b
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x24
	.4byte	.LASF297
	.byte	0x1
	.2byte	0x72d
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x26
	.4byte	.LASF298
	.byte	0x1
	.2byte	0x874
	.byte	0x1
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST27
	.4byte	0x177c
	.uleb128 0x24
	.4byte	.LASF288
	.byte	0x1
	.2byte	0x876
	.4byte	0xea5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x26
	.4byte	.LASF299
	.byte	0x1
	.2byte	0x93a
	.byte	0x1
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST28
	.4byte	0x17a5
	.uleb128 0x24
	.4byte	.LASF270
	.byte	0x1
	.2byte	0x93c
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x26
	.4byte	.LASF300
	.byte	0x1
	.2byte	0x959
	.byte	0x1
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST29
	.4byte	0x1801
	.uleb128 0x24
	.4byte	.LASF270
	.byte	0x1
	.2byte	0x95b
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x24
	.4byte	.LASF301
	.byte	0x1
	.2byte	0x95d
	.4byte	0x1060
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x24
	.4byte	.LASF302
	.byte	0x1
	.2byte	0x95f
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x27
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x24
	.4byte	.LASF303
	.byte	0x1
	.2byte	0xa06
	.4byte	0xbb
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.byte	0
	.uleb128 0x28
	.byte	0x1
	.4byte	.LASF322
	.byte	0x1
	.2byte	0xa6c
	.byte	0x1
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST30
	.uleb128 0x1e
	.4byte	.LASF304
	.byte	0x9
	.byte	0x30
	.4byte	0x1828
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x1c
	.4byte	0x66
	.uleb128 0x1e
	.4byte	.LASF305
	.byte	0x9
	.byte	0x34
	.4byte	0x183e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x1c
	.4byte	0x66
	.uleb128 0x1e
	.4byte	.LASF306
	.byte	0x9
	.byte	0x36
	.4byte	0x1854
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x1c
	.4byte	0x66
	.uleb128 0x1e
	.4byte	.LASF307
	.byte	0x9
	.byte	0x38
	.4byte	0x186a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x1c
	.4byte	0x66
	.uleb128 0x29
	.4byte	.LASF308
	.byte	0x6
	.byte	0x2b
	.4byte	0xa12
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF309
	.byte	0x6
	.byte	0x2d
	.4byte	0xa12
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF310
	.byte	0x6
	.byte	0x31
	.4byte	0xbb
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF311
	.byte	0x6
	.byte	0x42
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF312
	.byte	0x6
	.byte	0x45
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF313
	.byte	0x6
	.2byte	0x109
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF314
	.byte	0x6
	.2byte	0x124
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF315
	.byte	0x6
	.2byte	0x208
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	0xec7
	.4byte	0x18eb
	.uleb128 0x2b
	.4byte	0x25
	.2byte	0x2ff
	.byte	0
	.uleb128 0x2a
	.4byte	.LASF316
	.byte	0x6
	.2byte	0x2b1
	.4byte	0x18da
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF317
	.byte	0xa
	.byte	0x33
	.4byte	0x190a
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x1c
	.4byte	0xd1
	.uleb128 0x1e
	.4byte	.LASF318
	.byte	0xa
	.byte	0x3f
	.4byte	0x1920
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x1c
	.4byte	0xc9b
	.uleb128 0x29
	.4byte	.LASF319
	.byte	0x8
	.byte	0x72
	.4byte	0x1001
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF308
	.byte	0x6
	.byte	0x2b
	.4byte	0xa12
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF309
	.byte	0x6
	.byte	0x2d
	.4byte	0xa12
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF310
	.byte	0x6
	.byte	0x31
	.4byte	0xbb
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF311
	.byte	0x6
	.byte	0x42
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF312
	.byte	0x6
	.byte	0x45
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF313
	.byte	0x6
	.2byte	0x109
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF314
	.byte	0x6
	.2byte	0x124
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF315
	.byte	0x6
	.2byte	0x208
	.4byte	0x73e
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF316
	.byte	0x6
	.2byte	0x2b1
	.4byte	0x18da
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF319
	.byte	0x8
	.byte	0x72
	.4byte	0x1001
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI43
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI44
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI47
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI49
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI50
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI52
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI53
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI55
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI56
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI58
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI59
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI61
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI62
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI64
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI65
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI67
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI68
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB23
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI70
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI71
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB24
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI73
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI74
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB25
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI76
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI77
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB26
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI79
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI80
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB27
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI82
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI83
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB28
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI85
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI86
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB29
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI88
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI89
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB30
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI91
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI92
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x10c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LBB2
	.4byte	.LBE2
	.4byte	.LBB3
	.4byte	.LBE3
	.4byte	.LBB4
	.4byte	.LBE4
	.4byte	.LBB5
	.4byte	.LBE5
	.4byte	.LBB6
	.4byte	.LBE6
	.4byte	0
	.4byte	0
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF94:
	.ascii	"__minutes\000"
.LASF244:
	.ascii	"FTIMES_FUNCS_load_run_time_calculation_support\000"
.LASF97:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF231:
	.ascii	"percent_complete_completed_in_calculation_seconds\000"
.LASF61:
	.ascii	"flow_checking_enabled_and_allowed\000"
.LASF20:
	.ascii	"w_to_set_expected\000"
.LASF143:
	.ascii	"results_manual_programs_and_programmed_irrigation_c"
	.ascii	"lashed_holding\000"
.LASF106:
	.ascii	"pPrev\000"
.LASF75:
	.ascii	"ufim_one_or_more_in_list_for_manual\000"
.LASF319:
	.ascii	"ftcs\000"
.LASF208:
	.ascii	"cycle_seconds_used_during_irrigation\000"
.LASF278:
	.ascii	"ftimes_will_exceed_system_capacity\000"
.LASF280:
	.ascii	"capacity\000"
.LASF247:
	.ascii	"working_ptr\000"
.LASF79:
	.ascii	"due_to_edit_resync_to_the_system_list\000"
.LASF261:
	.ascii	"pft_station_ptr\000"
.LASF171:
	.ascii	"ufim_highest_priority_pump_waiting\000"
.LASF59:
	.ascii	"system_level_valves_are_ON_and_waiting_to_acquire_e"
	.ascii	"xpected\000"
.LASF229:
	.ascii	"calculation_END_date_and_time\000"
.LASF295:
	.ascii	"stop_dt\000"
.LASF54:
	.ascii	"system_level_valves_are_ON_but_will_not_be_checking"
	.ascii	"_flow\000"
.LASF216:
	.ascii	"expected_flow_rate_gpm_u16\000"
.LASF42:
	.ascii	"directions_honor_controller_set_to_OFF\000"
.LASF264:
	.ascii	"num_of_days\000"
.LASF162:
	.ascii	"capacity_in_use_bool\000"
.LASF36:
	.ascii	"rre_on_sxr_to_pause\000"
.LASF127:
	.ascii	"use_et_averaging_bool\000"
.LASF27:
	.ascii	"responds_to_wind\000"
.LASF108:
	.ascii	"pListHdr\000"
.LASF316:
	.ascii	"ft_stations\000"
.LASF67:
	.ascii	"one_or_more_in_list_for_programmed_irrigation\000"
.LASF282:
	.ascii	"ftimes_can_another_valve_come_ON_within_this_statio"
	.ascii	"n_group\000"
.LASF179:
	.ascii	"ufim_there_is_a_PUMP_mix_condition_b\000"
.LASF243:
	.ascii	"psupport_struct_ptr\000"
.LASF71:
	.ascii	"one_or_more_in_list_for_test\000"
.LASF182:
	.ascii	"ufim_stations_ON_with_the_pump_b\000"
.LASF217:
	.ascii	"FT_STATION_STRUCT\000"
.LASF279:
	.ascii	"with_pump\000"
.LASF253:
	.ascii	"ftimes_turn_OFF_this_station_if_ON_and_remove_from_"
	.ascii	"all_irrigation_lists\000"
.LASF224:
	.ascii	"double\000"
.LASF74:
	.ascii	"ufim_one_or_more_in_list_for_manual_program\000"
.LASF294:
	.ascii	"ftimes_irrigation_maintenance_FIRST__full_list_pass"
	.ascii	"_to_develop_ufim_vars_and_remove_completed_stations"
	.ascii	"\000"
.LASF86:
	.ascii	"overall_size\000"
.LASF154:
	.ascii	"start_times\000"
.LASF95:
	.ascii	"__seconds\000"
.LASF308:
	.ascii	"ft_stations_ON_by_controller\000"
.LASF152:
	.ascii	"FT_STATION_GROUP_STRUCT\000"
.LASF225:
	.ascii	"mode\000"
.LASF307:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF169:
	.ascii	"ufim_expected_flow_rate_for_those_ON\000"
.LASF64:
	.ascii	"stable_flow\000"
.LASF254:
	.ascii	"ftimes_get_percent_adjust_100u\000"
.LASF144:
	.ascii	"results_elapsed_irrigation_time_holding\000"
.LASF131:
	.ascii	"pump_in_use\000"
.LASF240:
	.ascii	"add_duplicate_ft_station\000"
.LASF140:
	.ascii	"results_hit_the_stop_time_holding\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF43:
	.ascii	"directions_honor_MANUAL_NOW\000"
.LASF194:
	.ascii	"FTIMES_STATION_BIT_FIELD_STRUCT\000"
.LASF146:
	.ascii	"results_start_date_and_time_associated_with_the_lat"
	.ascii	"est_finish_date_and_time\000"
.LASF17:
	.ascii	"no_longer_used_01\000"
.LASF30:
	.ascii	"no_longer_used_02\000"
.LASF78:
	.ascii	"ufim_one_or_more_in_list_for_mobile\000"
.LASF173:
	.ascii	"ufim_list_contains_waiting_programmed_irrigation_b\000"
.LASF249:
	.ascii	"ftimes_if_station_is_ON_turn_it_OFF\000"
.LASF234:
	.ascii	"duration_calculation_float_seconds\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF52:
	.ascii	"pump_activate_for_irrigation\000"
.LASF281:
	.ascii	"ftimes_the_on_at_a_time_rules_in_the_mainline_allow"
	.ascii	"_this_valve_to_come_ON\000"
.LASF213:
	.ascii	"distribution_uniformity_100u\000"
.LASF126:
	.ascii	"et_in_use\000"
.LASF209:
	.ascii	"soak_seconds_original_from_station_info\000"
.LASF200:
	.ascii	"manual_program_ptrs\000"
.LASF299:
	.ascii	"ftimes_irrigation_maintenance_THIRD__full_station_l"
	.ascii	"ist_pass_to_turn_off_stations_that_should_not_be_ON"
	.ascii	"_based_upon_those_trying_to_come_ON\000"
.LASF218:
	.ascii	"float\000"
.LASF99:
	.ascii	"phead\000"
.LASF296:
	.ascii	"ilc_still_in_the_list_and_need_to_get_the_next_ilc\000"
.LASF92:
	.ascii	"__year\000"
.LASF88:
	.ascii	"DATE_TIME\000"
.LASF101:
	.ascii	"count\000"
.LASF196:
	.ascii	"list_of_irrigating_stations\000"
.LASF289:
	.ascii	"ftimes_init_on_at_a_time_and_completion_variables_i"
	.ascii	"n_all_station_groups\000"
.LASF277:
	.ascii	"SIXTY_POINT_ZERO\000"
.LASF285:
	.ascii	"ftimes_bump_this_station_groups_on_at_a_time_ON_cou"
	.ascii	"nt\000"
.LASF70:
	.ascii	"one_or_more_in_list_for_walk_thru\000"
.LASF102:
	.ascii	"offset\000"
.LASF114:
	.ascii	"priority_level\000"
.LASF184:
	.ascii	"ufim_highest_reason_in_list_available_to_turn_ON\000"
.LASF220:
	.ascii	"wday\000"
.LASF136:
	.ascii	"GID_irrigation_system\000"
.LASF272:
	.ascii	"restart_calculation_due_to_error\000"
.LASF187:
	.ascii	"ufim_flow_check_group_count_of_ON\000"
.LASF302:
	.ascii	"need_to_get_the_next_ilc\000"
.LASF303:
	.ascii	"lthis_station_is_already_on_b\000"
.LASF177:
	.ascii	"ufim_list_contains_some_pump_to_setex_that_are_not_"
	.ascii	"ON_b\000"
.LASF139:
	.ascii	"results_start_date_and_time_holding\000"
.LASF322:
	.ascii	"FTIMES_FUNCS_maintain_irrigation_list\000"
.LASF85:
	.ascii	"whole_thing\000"
.LASF100:
	.ascii	"ptail\000"
.LASF72:
	.ascii	"one_or_more_in_list_for_rre\000"
.LASF210:
	.ascii	"soak_seconds_used_during_irrigation\000"
.LASF48:
	.ascii	"BIG_BIT_FIELD_FOR_ILC_STRUCT\000"
.LASF83:
	.ascii	"number_of_flow_meters_in_this_sys\000"
.LASF141:
	.ascii	"results_exceeded_stop_time_by_seconds_holding\000"
.LASF267:
	.ascii	"a_manual_program_wants_to_start\000"
.LASF160:
	.ascii	"list_support_systems\000"
.LASF91:
	.ascii	"__month\000"
.LASF251:
	.ascii	"pftstation_ptr\000"
.LASF273:
	.ascii	"done_considering_this_station\000"
.LASF260:
	.ascii	"ftimes_add_to_the_irrigation_list\000"
.LASF103:
	.ascii	"InUse\000"
.LASF320:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF181:
	.ascii	"ufim_number_ON_during_test\000"
.LASF257:
	.ascii	"ftimes_number_of_starts_for_this_manual_program\000"
.LASF286:
	.ascii	"ftimes_set_remaining_seconds_on\000"
.LASF82:
	.ascii	"delivered_MVOR_in_effect_closed\000"
.LASF188:
	.ascii	"inhibit_next_turn_ON_remaining_seconds\000"
.LASF158:
	.ascii	"run_time_seconds\000"
.LASF269:
	.ascii	"lstation_count\000"
.LASF44:
	.ascii	"directions_honor_CALENDAR_NOW\000"
.LASF258:
	.ascii	"pmanual_program_ptr\000"
.LASF199:
	.ascii	"station_group_ptr\000"
.LASF149:
	.ascii	"results_hit_the_stop_time\000"
.LASF321:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ftim"
	.ascii	"es/ftimes_funcs.c\000"
.LASF270:
	.ascii	"lftstation_ptr\000"
.LASF310:
	.ascii	"ufim_one_or_more_in_the_list_for_manual_program\000"
.LASF151:
	.ascii	"results_manual_programs_and_programmed_irrigation_c"
	.ascii	"lashed\000"
.LASF110:
	.ascii	"list_support_station_groups\000"
.LASF18:
	.ascii	"station_priority\000"
.LASF116:
	.ascii	"percent_adjust_start_date\000"
.LASF13:
	.ascii	"INT_32\000"
.LASF178:
	.ascii	"ufim_list_contains_some_non_pump_to_setex_that_are_"
	.ascii	"not_ON_b\000"
.LASF35:
	.ascii	"at_some_point_flow_was_checked\000"
.LASF288:
	.ascii	"ftsystem_ptr\000"
.LASF142:
	.ascii	"for_this_SECOND_add_to_exceeded_stop_time_by_second"
	.ascii	"s_holding\000"
.LASF207:
	.ascii	"cycle_seconds_original_from_station_info\000"
.LASF8:
	.ascii	"char\000"
.LASF80:
	.ascii	"accounted_for\000"
.LASF250:
	.ascii	"plist_hdr\000"
.LASF262:
	.ascii	"start_time_check_does_it_irrigate_on_this_date\000"
.LASF133:
	.ascii	"delay_between_valve_time_sec\000"
.LASF212:
	.ascii	"et_factor_100u\000"
.LASF112:
	.ascii	"precip_rate_in_100000u\000"
.LASF263:
	.ascii	"pft_sgs\000"
.LASF73:
	.ascii	"ufim_one_or_more_in_list_for_programmed_irrigation\000"
.LASF24:
	.ascii	"flow_check_hi_action\000"
.LASF176:
	.ascii	"ufim_list_contains_some_to_setex_that_are_not_ON_b\000"
.LASF96:
	.ascii	"__dayofweek\000"
.LASF195:
	.ascii	"ftimes_support\000"
.LASF81:
	.ascii	"delivered_MVOR_in_effect_opened\000"
.LASF6:
	.ascii	"long long int\000"
.LASF291:
	.ascii	"ftimes_for_each_station_group_check_if_irrigation_j"
	.ascii	"ust_completed\000"
.LASF318:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF239:
	.ascii	"preason_in_list\000"
.LASF26:
	.ascii	"flow_check_group\000"
.LASF39:
	.ascii	"no_longer_used_03\000"
.LASF161:
	.ascii	"system_gid\000"
.LASF301:
	.ascii	"ltmp_ftstation_ptr\000"
.LASF271:
	.ascii	"duplicate_ahead_ft_station_ptr\000"
.LASF317:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF76:
	.ascii	"ufim_one_or_more_in_list_for_walk_thru\000"
.LASF89:
	.ascii	"date_time\000"
.LASF15:
	.ascii	"BOOL_32\000"
.LASF109:
	.ascii	"MIST_DLINK_TYPE\000"
.LASF287:
	.ascii	"ftimes_init_ufim_variables_for_all_systems\000"
.LASF111:
	.ascii	"group_identity_number\000"
.LASF255:
	.ascii	"pft_group_ptr\000"
.LASF190:
	.ascii	"slot_loaded\000"
.LASF130:
	.ascii	"on_at_a_time__presently_ON_in_station_group_count\000"
.LASF137:
	.ascii	"at_start_of_second_one_or_more_in_the_list_for_prog"
	.ascii	"rammed_irrigation\000"
.LASF57:
	.ascii	"system_level_valves_are_ON_and_waiting_to_update_de"
	.ascii	"rate_table\000"
.LASF46:
	.ascii	"directions_honor_FREEZE_SWITCH\000"
.LASF9:
	.ascii	"UNS_8\000"
.LASF266:
	.ascii	"we_are_at_a_start_time_and_a_water_day\000"
.LASF49:
	.ascii	"unused_four_bits\000"
.LASF66:
	.ascii	"MVOR_in_effect_closed\000"
.LASF292:
	.ascii	"ftimes_station_is_eligible_to_be_removed_from_the_i"
	.ascii	"rrigating_list\000"
.LASF29:
	.ascii	"station_is_ON\000"
.LASF245:
	.ascii	"FTIMES_FUNCS_find_station_group_in_ftimes_list_with"
	.ascii	"_this_GID\000"
.LASF120:
	.ascii	"start_time\000"
.LASF237:
	.ascii	"FTIMES_CONTROL_STRUCT\000"
.LASF230:
	.ascii	"percent_complete_total_in_calculation_seconds\000"
.LASF10:
	.ascii	"UNS_16\000"
.LASF98:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF50:
	.ascii	"master_valve_has_at_least_one_normally_closed\000"
.LASF138:
	.ascii	"after_removal_opportunity_one_or_more_in_the_list_f"
	.ascii	"or_programmed_irrigation\000"
.LASF56:
	.ascii	"system_level_valves_are_ON_and_actively_checking\000"
.LASF290:
	.ascii	"lgroup\000"
.LASF202:
	.ascii	"box_index_0\000"
.LASF148:
	.ascii	"results_latest_finish_date_and_time_holding\000"
.LASF166:
	.ascii	"ufim_on_at_a_time__presently_allowed_ON_in_mainline"
	.ascii	"__learned\000"
.LASF238:
	.ascii	"pindex\000"
.LASF315:
	.ascii	"ft_system_groups_list_hdr\000"
.LASF119:
	.ascii	"schedule_type\000"
.LASF58:
	.ascii	"system_level_valves_are_ON_and_has_updated_the_dera"
	.ascii	"te_table\000"
.LASF121:
	.ascii	"stop_time\000"
.LASF276:
	.ascii	"loop_through_the_stations\000"
.LASF298:
	.ascii	"ftimes_irrigation_maintenance_SECOND__pass_through_"
	.ascii	"all_systems_to_develop_more_ufim_vars\000"
.LASF132:
	.ascii	"line_fill_time_sec\000"
.LASF236:
	.ascii	"seconds_to_advance\000"
.LASF167:
	.ascii	"system_master_number_of_valves_ON\000"
.LASF156:
	.ascii	"start_date\000"
.LASF123:
	.ascii	"water_days_bool\000"
.LASF170:
	.ascii	"ufim_one_ON_from_the_problem_list_b\000"
.LASF122:
	.ascii	"a_scheduled_irrigation_date_in_the_past\000"
.LASF248:
	.ascii	"FTIMES_FUNCS_find_system_group_in_ftimes_list_with_"
	.ascii	"this_GID\000"
.LASF174:
	.ascii	"ufim_list_contains_waiting_pump_valves_b\000"
.LASF192:
	.ascii	"did_not_irrigate_last_time_holding_copy\000"
.LASF115:
	.ascii	"percent_adjust_100u\000"
.LASF51:
	.ascii	"mv_open_for_irrigation\000"
.LASF157:
	.ascii	"stop_date\000"
.LASF124:
	.ascii	"irrigate_on_29th_or_31st_bool\000"
.LASF153:
	.ascii	"list_support_manual_program\000"
.LASF117:
	.ascii	"percent_adjust_end_date\000"
.LASF314:
	.ascii	"ft_manual_programs_list_hdr\000"
.LASF312:
	.ascii	"ft_stations_ON_list_hdr\000"
.LASF134:
	.ascii	"high_flow_action\000"
.LASF206:
	.ascii	"remaining_seconds_ON\000"
.LASF3:
	.ascii	"short int\000"
.LASF147:
	.ascii	"results_latest_finish_date_and_time\000"
.LASF165:
	.ascii	"ufim_valves_in_the_list_for_this_system\000"
.LASF293:
	.ascii	"ftimes_decrement_system_timers\000"
.LASF211:
	.ascii	"soak_seconds_remaining_ul\000"
.LASF4:
	.ascii	"long int\000"
.LASF87:
	.ascii	"SYSTEM_BIT_FIELD_STRUCT\000"
.LASF69:
	.ascii	"one_or_more_in_list_for_manual\000"
.LASF55:
	.ascii	"system_level_valves_are_ON_and_waiting_to_check_flo"
	.ascii	"w\000"
.LASF40:
	.ascii	"xfer_to_irri_machines\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF275:
	.ascii	"add_the_time_to_the_remaining_balance\000"
.LASF38:
	.ascii	"rre_in_process_to_turn_ON\000"
.LASF172:
	.ascii	"ufim_highest_priority_non_pump_waiting\000"
.LASF193:
	.ascii	"done_with_runtime_cycle\000"
.LASF28:
	.ascii	"responds_to_rain\000"
.LASF175:
	.ascii	"ufim_list_contains_waiting_non_pump_valves_b\000"
.LASF33:
	.ascii	"rre_station_is_paused\000"
.LASF25:
	.ascii	"flow_check_lo_action\000"
.LASF233:
	.ascii	"percent_complete\000"
.LASF41:
	.ascii	"directions_honor_RAIN_TABLE\000"
.LASF93:
	.ascii	"__hours\000"
.LASF241:
	.ascii	"find_a_duplicate_ahead_of_this_station\000"
.LASF45:
	.ascii	"directions_honor_RAIN_SWITCH\000"
.LASF65:
	.ascii	"MVOR_in_effect_opened\000"
.LASF104:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF300:
	.ascii	"ftimes_irrigation_maintenance_FOURTH__full_station_"
	.ascii	"list_pass_to_hunt_for_stations_to_turn_ON\000"
.LASF183:
	.ascii	"ufim_stations_ON_without_the_pump_b\000"
.LASF219:
	.ascii	"schedule_enabled\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF125:
	.ascii	"mow_day\000"
.LASF198:
	.ascii	"system_ptr\000"
.LASF180:
	.ascii	"ufim_based_on_reason_in_list_at_least_one_valve_is_"
	.ascii	"ON_that_wants_to_check_flow\000"
.LASF265:
	.ascii	"schedule_type_days\000"
.LASF23:
	.ascii	"w_involved_in_a_flow_problem\000"
.LASF274:
	.ascii	"number_of_mp_starts\000"
.LASF155:
	.ascii	"days\000"
.LASF145:
	.ascii	"results_elapsed_irrigation_time\000"
.LASF283:
	.ascii	"ftimes_merge_new_mainline_allowed_ON_into_the_limit"
	.ascii	"\000"
.LASF163:
	.ascii	"capacity_with_pump_gpm\000"
.LASF22:
	.ascii	"w_did_not_irrigate_last_time\000"
.LASF256:
	.ascii	"pdate\000"
.LASF215:
	.ascii	"stop_datetime_d\000"
.LASF90:
	.ascii	"__day\000"
.LASF305:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF252:
	.ascii	"inhibit_next_turn_on\000"
.LASF214:
	.ascii	"stop_datetime_t\000"
.LASF77:
	.ascii	"ufim_one_or_more_in_list_for_test\000"
.LASF203:
	.ascii	"user_programmed_seconds\000"
.LASF164:
	.ascii	"capacity_without_pump_gpm\000"
.LASF297:
	.ascii	"for_this_STATION_add_to_exceeded_stop_time_by_secon"
	.ascii	"ds_holding\000"
.LASF60:
	.ascii	"checked_or_updated_and_made_flow_recording_lines\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF197:
	.ascii	"list_of_stations_ON\000"
.LASF19:
	.ascii	"w_reason_in_list\000"
.LASF62:
	.ascii	"flow_checking_enabled_by_user_setting\000"
.LASF32:
	.ascii	"flow_check_to_be_excluded_from_future_checking\000"
.LASF34:
	.ascii	"at_some_point_should_check_flow\000"
.LASF150:
	.ascii	"results_exceeded_stop_time_by_seconds\000"
.LASF268:
	.ascii	"FTIMES_FUNCS_check_for_a_start\000"
.LASF191:
	.ascii	"is_a_duplicate_in_the_array\000"
.LASF226:
	.ascii	"calculation_has_run_since_reboot\000"
.LASF311:
	.ascii	"ft_irrigating_stations_list_hdr\000"
.LASF129:
	.ascii	"on_at_a_time__allowed_ON_in_mainline__user_setting\000"
.LASF31:
	.ascii	"flow_check_when_possible_based_on_reason_in_list\000"
.LASF21:
	.ascii	"w_uses_the_pump\000"
.LASF113:
	.ascii	"crop_coefficient_100u\000"
.LASF227:
	.ascii	"calculation_date_and_time\000"
.LASF221:
	.ascii	"irrigates_after_29th_or_31st\000"
.LASF16:
	.ascii	"BITFIELD_BOOL\000"
.LASF223:
	.ascii	"RUN_TIME_CALC_SUPPORT_STRUCT\000"
.LASF309:
	.ascii	"ft_electrical_limits\000"
.LASF205:
	.ascii	"requested_irrigation_seconds_balance_ul\000"
.LASF47:
	.ascii	"directions_honor_WIND_PAUSE\000"
.LASF37:
	.ascii	"rre_on_sxr_to_turn_OFF\000"
.LASF84:
	.ascii	"number_of_pocs_in_this_system\000"
.LASF14:
	.ascii	"UNS_64\000"
.LASF68:
	.ascii	"one_or_more_in_list_for_manual_program\000"
.LASF53:
	.ascii	"system_level_no_valves_ON_therefore_no_flow_checkin"
	.ascii	"g\000"
.LASF105:
	.ascii	"MIST_LIST_HDR_TYPE_PTR\000"
.LASF2:
	.ascii	"signed char\000"
.LASF284:
	.ascii	"allowed_ON_within_mainline\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF304:
	.ascii	"GuiFont_LanguageActive\000"
.LASF246:
	.ascii	"psg_gid\000"
.LASF235:
	.ascii	"duration_start_time_stamp\000"
.LASF222:
	.ascii	"uses_et_averaging\000"
.LASF201:
	.ascii	"station_number_0\000"
.LASF186:
	.ascii	"ufim_highest_non_pump_reason_in_list_available_to_t"
	.ascii	"urn_ON\000"
.LASF168:
	.ascii	"ufim_what_are_we_turning_on_b\000"
.LASF228:
	.ascii	"dtcs_data_is_current\000"
.LASF189:
	.ascii	"FT_SYSTEM_GROUP_STRUCT\000"
.LASF135:
	.ascii	"low_flow_action\000"
.LASF313:
	.ascii	"ft_station_groups_list_hdr\000"
.LASF118:
	.ascii	"schedule_enabled_bool\000"
.LASF242:
	.ascii	"pft_station_group_ptr\000"
.LASF185:
	.ascii	"ufim_highest_pump_reason_in_list_available_to_turn_"
	.ascii	"ON\000"
.LASF107:
	.ascii	"pNext\000"
.LASF63:
	.ascii	"there_are_pocs_without_flow_meters\000"
.LASF128:
	.ascii	"on_at_a_time__allowed_ON_in_station_group__user_set"
	.ascii	"ting\000"
.LASF159:
	.ascii	"FT_MANUAL_PROGRAM_STRUCT\000"
.LASF259:
	.ascii	"ftimes_complete_the_turn_ON\000"
.LASF204:
	.ascii	"water_sense_deferred_seconds\000"
.LASF306:
	.ascii	"GuiFont_DecimalChar\000"
.LASF232:
	.ascii	"percent_complete_next_calculation_boundary\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
