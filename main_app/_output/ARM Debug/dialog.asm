	.file	"dialog.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_DIALOG_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.type	g_DIALOG_cursor_position_when_dialog_displayed, %object
	.size	g_DIALOG_cursor_position_when_dialog_displayed, 4
g_DIALOG_cursor_position_when_dialog_displayed:
	.space	4
	.global	g_DIALOG_modal_result
	.section	.bss.g_DIALOG_modal_result,"aw",%nobits
	.align	2
	.type	g_DIALOG_modal_result, %object
	.size	g_DIALOG_modal_result, 4
g_DIALOG_modal_result:
	.space	4
	.global	g_DIALOG_ok_dialog_visible
	.section	.bss.g_DIALOG_ok_dialog_visible,"aw",%nobits
	.align	2
	.type	g_DIALOG_ok_dialog_visible, %object
	.size	g_DIALOG_ok_dialog_visible, 4
g_DIALOG_ok_dialog_visible:
	.space	4
	.section	.bss.g_DIALOG_yes_no_cancel_dialog_visible,"aw",%nobits
	.align	2
	.type	g_DIALOG_yes_no_cancel_dialog_visible, %object
	.size	g_DIALOG_yes_no_cancel_dialog_visible, 4
g_DIALOG_yes_no_cancel_dialog_visible:
	.space	4
	.section	.bss.g_DIALOG_yes_no_dialog_visible,"aw",%nobits
	.align	2
	.type	g_DIALOG_yes_no_dialog_visible, %object
	.size	g_DIALOG_yes_no_dialog_visible, 4
g_DIALOG_yes_no_dialog_visible:
	.space	4
	.section	.text.DIALOG_a_dialog_is_visible,"ax",%progbits
	.align	2
	.global	DIALOG_a_dialog_is_visible
	.type	DIALOG_a_dialog_is_visible, %function
DIALOG_a_dialog_is_visible:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/dialog.c"
	.loc 1 68 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	.loc 1 71 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 75 0
	ldr	r3, .L4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L2
	.loc 1 75 0 is_stmt 0 discriminator 1
	ldr	r3, .L4+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L2
	.loc 1 76 0 is_stmt 1
	ldr	r3, .L4+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L3
.L2:
	.loc 1 79 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L3:
	.loc 1 84 0
	ldr	r3, [fp, #-4]
	.loc 1 85 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L5:
	.align	2
.L4:
	.word	g_DIALOG_ok_dialog_visible
	.word	g_DIALOG_yes_no_cancel_dialog_visible
	.word	g_DIALOG_yes_no_dialog_visible
.LFE0:
	.size	DIALOG_a_dialog_is_visible, .-DIALOG_a_dialog_is_visible
	.section	.text.FDTO_DIALOG_redraw_ok_dialog,"ax",%progbits
	.align	2
	.global	FDTO_DIALOG_redraw_ok_dialog
	.type	FDTO_DIALOG_redraw_ok_dialog, %function
FDTO_DIALOG_redraw_ok_dialog:
.LFB1:
	.loc 1 97 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 98 0
	ldr	r3, .L7
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r3
	mov	r1, #99
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 99 0
	bl	GuiLib_Refresh
	.loc 1 100 0
	ldmfd	sp!, {fp, pc}
.L8:
	.align	2
.L7:
	.word	GuiLib_CurStructureNdx
.LFE1:
	.size	FDTO_DIALOG_redraw_ok_dialog, .-FDTO_DIALOG_redraw_ok_dialog
	.section	.text.FDTO_DIALOG_draw_ok_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_draw_ok_dialog, %function
FDTO_DIALOG_draw_ok_dialog:
.LFB2:
	.loc 1 120 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #4
.LCFI7:
	str	r0, [fp, #-8]
	.loc 1 121 0
	bl	DIALOG_close_all_dialogs
	.loc 1 123 0
	ldr	r3, .L10
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 125 0
	ldr	r3, .L10+4
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L10+8
	str	r2, [r3, #0]
	.loc 1 127 0
	ldr	r3, .L10+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 129 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r3
	mov	r1, #99
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 130 0
	bl	GuiLib_Refresh
	.loc 1 131 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L11:
	.align	2
.L10:
	.word	g_DIALOG_ok_dialog_visible
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_DIALOG_cursor_position_when_dialog_displayed
	.word	g_DIALOG_modal_result
.LFE2:
	.size	FDTO_DIALOG_draw_ok_dialog, .-FDTO_DIALOG_draw_ok_dialog
	.section	.text.DIALOG_draw_ok_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_draw_ok_dialog
	.type	DIALOG_draw_ok_dialog, %function
DIALOG_draw_ok_dialog:
.LFB3:
	.loc 1 135 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	sub	sp, sp, #40
.LCFI10:
	str	r0, [fp, #-44]
	.loc 1 138 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 139 0
	ldr	r3, .L13
	str	r3, [fp, #-20]
	.loc 1 140 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 142 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 143 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L14:
	.align	2
.L13:
	.word	FDTO_DIALOG_draw_ok_dialog
.LFE3:
	.size	DIALOG_draw_ok_dialog, .-DIALOG_draw_ok_dialog
	.section	.text.DIALOG_close_ok_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_close_ok_dialog
	.type	DIALOG_close_ok_dialog, %function
DIALOG_close_ok_dialog:
.LFB4:
	.loc 1 147 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	.loc 1 148 0
	ldr	r3, .L16
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 152 0
	ldr	r3, .L16+4
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L16+8
	strh	r2, [r3, #0]	@ movhi
	.loc 1 154 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 155 0
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	g_DIALOG_ok_dialog_visible
	.word	g_DIALOG_cursor_position_when_dialog_displayed
	.word	GuiLib_ActiveCursorFieldNo
.LFE4:
	.size	DIALOG_close_ok_dialog, .-DIALOG_close_ok_dialog
	.section	.text.DIALOG_process_ok_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_process_ok_dialog
	.type	DIALOG_process_ok_dialog, %function
DIALOG_process_ok_dialog:
.LFB5:
	.loc 1 175 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI13:
	add	fp, sp, #4
.LCFI14:
	sub	sp, sp, #44
.LCFI15:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	.loc 1 178 0
	ldr	r3, [fp, #-48]
	cmp	r3, #83
	beq	.L21
	cmp	r3, #83
	bhi	.L23
	cmp	r3, #2
	beq	.L20
	cmp	r3, #67
	beq	.L20
	b	.L19
.L23:
	sub	r3, r3, #36864
	cmp	r3, #6
	bhi	.L19
	b	.L32
.L21:
	.loc 1 181 0
	bl	KEY_process_ENG_SPA
	.loc 1 183 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 184 0
	ldr	r3, .L33
	str	r3, [fp, #-20]
	.loc 1 185 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 186 0
	b	.L18
.L20:
	.loc 1 192 0
	ldr	r3, .L33+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L25
	.loc 1 194 0
	bl	bad_key_beep
	.loc 1 242 0
	b	.L18
.L25:
	.loc 1 198 0
	ldr	r3, .L33+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L33+12
	cmp	r2, r3
	bne	.L27
	.loc 1 198 0 is_stmt 0 discriminator 1
	ldr	r3, .L33+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L27
	.loc 1 200 0 is_stmt 1
	bl	bad_key_beep
	.loc 1 242 0
	b	.L18
.L27:
	.loc 1 202 0
	ldr	r3, .L33+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L33+20
	cmp	r2, r3
	bne	.L28
	.loc 1 204 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	ldr	r2, .L33+24
	bl	FDTO_TWO_WIRE_close_discovery_dialog
	.loc 1 242 0
	b	.L18
.L28:
	.loc 1 206 0
	ldr	r3, .L33+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L33+28
	cmp	r2, r3
	bne	.L29
	.loc 1 208 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	FDTO_DEVICE_EXCHANGE_close_dialog
	.loc 1 242 0
	b	.L18
.L29:
	.loc 1 212 0
	ldr	r3, .L33+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L33+32
	cmp	r2, r3
	bne	.L30
	.loc 1 214 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	bne	.L31
	.loc 1 216 0
	bl	bad_key_beep
	.loc 1 242 0
	b	.L18
.L31:
	.loc 1 223 0
	ldr	r3, .L33+36
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 225 0
	ldr	r3, .L33+24
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 227 0
	ldr	r3, .L33+40
	mov	r2, #26
	strh	r2, [r3, #0]	@ movhi
	.loc 1 229 0
	ldr	r3, .L33+44
	ldr	r2, [r3, #0]
	ldr	r0, .L33+48
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L33+52
	str	r2, [r3, #0]
	.loc 1 231 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 242 0
	b	.L18
.L30:
	.loc 1 236 0
	bl	good_key_beep
	.loc 1 238 0
	ldr	r3, .L33+36
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 240 0
	bl	DIALOG_close_ok_dialog
	.loc 1 242 0
	b	.L18
.L32:
	.loc 1 253 0
	ldr	r3, .L33+44
	ldr	r2, [r3, #0]
	ldr	r0, .L33+48
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	blx	r3
	.loc 1 254 0
	b	.L18
.L19:
	.loc 1 257 0
	bl	bad_key_beep
.L18:
	.loc 1 259 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L34:
	.align	2
.L33:
	.word	FDTO_DIALOG_redraw_ok_dialog
	.word	GuiVar_CodeDownloadState
	.word	GuiLib_CurStructureNdx
	.word	635
	.word	GuiVar_StopKeyPending
	.word	599
	.word	g_DIALOG_ok_dialog_visible
	.word	598
	.word	603
	.word	g_DIALOG_modal_result
	.word	GuiLib_ActiveCursorFieldNo
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE5:
	.size	DIALOG_process_ok_dialog, .-DIALOG_process_ok_dialog
	.section	.text.FDTO_DIALOG_redraw_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_redraw_yes_no_cancel_dialog, %function
FDTO_DIALOG_redraw_yes_no_cancel_dialog:
.LFB6:
	.loc 1 275 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI16:
	add	fp, sp, #4
.LCFI17:
	.loc 1 276 0
	ldr	r3, .L36
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r3
	mov	r1, #97
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 277 0
	bl	GuiLib_Refresh
	.loc 1 278 0
	ldmfd	sp!, {fp, pc}
.L37:
	.align	2
.L36:
	.word	GuiLib_CurStructureNdx
.LFE6:
	.size	FDTO_DIALOG_redraw_yes_no_cancel_dialog, .-FDTO_DIALOG_redraw_yes_no_cancel_dialog
	.section	.text.FDTO_DIALOG_draw_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_draw_yes_no_cancel_dialog, %function
FDTO_DIALOG_draw_yes_no_cancel_dialog:
.LFB7:
	.loc 1 282 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #4
.LCFI20:
	str	r0, [fp, #-8]
	.loc 1 283 0
	bl	DIALOG_close_all_dialogs
	.loc 1 285 0
	ldr	r3, .L39
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 287 0
	ldr	r3, .L39+4
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L39+8
	str	r2, [r3, #0]
	.loc 1 289 0
	ldr	r3, .L39+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 291 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r3
	mov	r1, #97
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 292 0
	bl	GuiLib_Refresh
	.loc 1 293 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L40:
	.align	2
.L39:
	.word	g_DIALOG_yes_no_cancel_dialog_visible
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_DIALOG_cursor_position_when_dialog_displayed
	.word	g_DIALOG_modal_result
.LFE7:
	.size	FDTO_DIALOG_draw_yes_no_cancel_dialog, .-FDTO_DIALOG_draw_yes_no_cancel_dialog
	.section	.text.DIALOG_draw_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_draw_yes_no_cancel_dialog
	.type	DIALOG_draw_yes_no_cancel_dialog, %function
DIALOG_draw_yes_no_cancel_dialog:
.LFB8:
	.loc 1 297 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #40
.LCFI23:
	str	r0, [fp, #-44]
	.loc 1 300 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 301 0
	ldr	r3, .L42
	str	r3, [fp, #-20]
	.loc 1 302 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 304 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 305 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L43:
	.align	2
.L42:
	.word	FDTO_DIALOG_draw_yes_no_cancel_dialog
.LFE8:
	.size	DIALOG_draw_yes_no_cancel_dialog, .-DIALOG_draw_yes_no_cancel_dialog
	.section	.text.DIALOG_close_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.type	DIALOG_close_yes_no_cancel_dialog, %function
DIALOG_close_yes_no_cancel_dialog:
.LFB9:
	.loc 1 309 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	.loc 1 310 0
	ldr	r3, .L45
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 314 0
	ldr	r3, .L45+4
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L45+8
	strh	r2, [r3, #0]	@ movhi
	.loc 1 316 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 317 0
	ldmfd	sp!, {fp, pc}
.L46:
	.align	2
.L45:
	.word	g_DIALOG_yes_no_cancel_dialog_visible
	.word	g_DIALOG_cursor_position_when_dialog_displayed
	.word	GuiLib_ActiveCursorFieldNo
.LFE9:
	.size	DIALOG_close_yes_no_cancel_dialog, .-DIALOG_close_yes_no_cancel_dialog
	.section	.text.DIALOG_process_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_process_yes_no_cancel_dialog
	.type	DIALOG_process_yes_no_cancel_dialog, %function
DIALOG_process_yes_no_cancel_dialog:
.LFB10:
	.loc 1 337 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	sub	sp, sp, #48
.LCFI28:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	str	r2, [fp, #-52]
	.loc 1 342 0
	ldr	r3, [fp, #-48]
	cmp	r3, #3
	beq	.L51
	cmp	r3, #3
	bhi	.L54
	cmp	r3, #1
	beq	.L49
	cmp	r3, #2
	beq	.L50
	b	.L48
.L54:
	cmp	r3, #67
	beq	.L52
	cmp	r3, #83
	bne	.L48
.L53:
	.loc 1 345 0
	bl	KEY_process_ENG_SPA
	.loc 1 347 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 348 0
	ldr	r3, .L64
	str	r3, [fp, #-20]
	.loc 1 349 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 350 0
	b	.L47
.L50:
	.loc 1 353 0
	ldr	r3, .L64+4
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #98
	beq	.L58
	cmp	r3, #99
	beq	.L59
	cmp	r3, #97
	bne	.L56
.L57:
	.loc 1 356 0
	ldr	r3, .L64+8
	mov	r2, #6
	str	r2, [r3, #0]
	.loc 1 357 0
	b	.L56
.L58:
	.loc 1 360 0
	ldr	r3, .L64+8
	mov	r2, #7
	str	r2, [r3, #0]
	.loc 1 361 0
	b	.L56
.L59:
	.loc 1 364 0
	ldr	r3, .L64+8
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 365 0
	mov	r0, r0	@ nop
.L56:
	.loc 1 368 0
	ldr	r3, .L64+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L60
	.loc 1 370 0
	bl	good_key_beep
	.loc 1 372 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	beq	.L61
	.loc 1 374 0
	ldr	r3, [fp, #-52]
	blx	r3
.L61:
	.loc 1 377 0
	bl	DIALOG_close_yes_no_cancel_dialog
	.loc 1 383 0
	b	.L47
.L60:
	.loc 1 381 0
	bl	bad_key_beep
	.loc 1 383 0
	b	.L47
.L52:
	.loc 1 386 0
	bl	good_key_beep
	.loc 1 388 0
	ldr	r3, .L64+8
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 390 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	beq	.L63
	.loc 1 392 0
	ldr	r3, [fp, #-52]
	blx	r3
.L63:
	.loc 1 395 0
	bl	DIALOG_close_yes_no_cancel_dialog
	.loc 1 396 0
	b	.L47
.L49:
	.loc 1 399 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 400 0
	b	.L47
.L51:
	.loc 1 403 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 404 0
	b	.L47
.L48:
	.loc 1 407 0
	bl	bad_key_beep
.L47:
	.loc 1 409 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L65:
	.align	2
.L64:
	.word	FDTO_DIALOG_redraw_yes_no_cancel_dialog
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_DIALOG_modal_result
.LFE10:
	.size	DIALOG_process_yes_no_cancel_dialog, .-DIALOG_process_yes_no_cancel_dialog
	.section	.text.FDTO_DIALOG_redraw_yes_no_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_redraw_yes_no_dialog, %function
FDTO_DIALOG_redraw_yes_no_dialog:
.LFB11:
	.loc 1 424 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI29:
	add	fp, sp, #4
.LCFI30:
	.loc 1 425 0
	ldr	r3, .L67
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r3
	mov	r1, #97
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 426 0
	bl	GuiLib_Refresh
	.loc 1 427 0
	ldmfd	sp!, {fp, pc}
.L68:
	.align	2
.L67:
	.word	GuiLib_CurStructureNdx
.LFE11:
	.size	FDTO_DIALOG_redraw_yes_no_dialog, .-FDTO_DIALOG_redraw_yes_no_dialog
	.section	.text.FDTO_DIALOG_draw_yes_no_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_draw_yes_no_dialog, %function
FDTO_DIALOG_draw_yes_no_dialog:
.LFB12:
	.loc 1 431 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI31:
	add	fp, sp, #4
.LCFI32:
	sub	sp, sp, #4
.LCFI33:
	str	r0, [fp, #-8]
	.loc 1 432 0
	bl	DIALOG_close_all_dialogs
	.loc 1 434 0
	ldr	r3, .L70
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 436 0
	ldr	r3, .L70+4
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L70+8
	str	r2, [r3, #0]
	.loc 1 438 0
	ldr	r3, .L70+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 440 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r3
	mov	r1, #97
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 441 0
	bl	GuiLib_Refresh
	.loc 1 442 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L71:
	.align	2
.L70:
	.word	g_DIALOG_yes_no_dialog_visible
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_DIALOG_cursor_position_when_dialog_displayed
	.word	g_DIALOG_modal_result
.LFE12:
	.size	FDTO_DIALOG_draw_yes_no_dialog, .-FDTO_DIALOG_draw_yes_no_dialog
	.section	.text.DIALOG_draw_yes_no_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_draw_yes_no_dialog
	.type	DIALOG_draw_yes_no_dialog, %function
DIALOG_draw_yes_no_dialog:
.LFB13:
	.loc 1 446 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI34:
	add	fp, sp, #4
.LCFI35:
	sub	sp, sp, #40
.LCFI36:
	str	r0, [fp, #-44]
	.loc 1 449 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 450 0
	ldr	r3, .L73
	str	r3, [fp, #-20]
	.loc 1 451 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 453 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 454 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L74:
	.align	2
.L73:
	.word	FDTO_DIALOG_draw_yes_no_dialog
.LFE13:
	.size	DIALOG_draw_yes_no_dialog, .-DIALOG_draw_yes_no_dialog
	.section	.text.DIALOG_close_yes_no_dialog,"ax",%progbits
	.align	2
	.type	DIALOG_close_yes_no_dialog, %function
DIALOG_close_yes_no_dialog:
.LFB14:
	.loc 1 458 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI37:
	add	fp, sp, #4
.LCFI38:
	.loc 1 459 0
	ldr	r3, .L76
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 463 0
	ldr	r3, .L76+4
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L76+8
	strh	r2, [r3, #0]	@ movhi
	.loc 1 465 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 466 0
	ldmfd	sp!, {fp, pc}
.L77:
	.align	2
.L76:
	.word	g_DIALOG_yes_no_dialog_visible
	.word	g_DIALOG_cursor_position_when_dialog_displayed
	.word	GuiLib_ActiveCursorFieldNo
.LFE14:
	.size	DIALOG_close_yes_no_dialog, .-DIALOG_close_yes_no_dialog
	.section	.text.DIALOG_process_yes_no_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_process_yes_no_dialog
	.type	DIALOG_process_yes_no_dialog, %function
DIALOG_process_yes_no_dialog:
.LFB15:
	.loc 1 486 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI39:
	add	fp, sp, #4
.LCFI40:
	sub	sp, sp, #48
.LCFI41:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	str	r2, [fp, #-52]
	.loc 1 491 0
	ldr	r3, [fp, #-48]
	cmp	r3, #3
	beq	.L82
	cmp	r3, #3
	bhi	.L85
	cmp	r3, #1
	beq	.L80
	cmp	r3, #2
	beq	.L81
	b	.L79
.L85:
	cmp	r3, #67
	beq	.L83
	cmp	r3, #83
	bne	.L79
.L84:
	.loc 1 494 0
	bl	KEY_process_ENG_SPA
	.loc 1 496 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 497 0
	ldr	r3, .L94
	str	r3, [fp, #-20]
	.loc 1 498 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 499 0
	b	.L78
.L81:
	.loc 1 502 0
	ldr	r3, .L94+4
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #97
	beq	.L88
	cmp	r3, #98
	beq	.L89
	b	.L87
.L88:
	.loc 1 505 0
	ldr	r3, .L94+8
	mov	r2, #6
	str	r2, [r3, #0]
	.loc 1 506 0
	b	.L87
.L89:
	.loc 1 509 0
	ldr	r3, .L94+8
	mov	r2, #7
	str	r2, [r3, #0]
	.loc 1 510 0
	mov	r0, r0	@ nop
.L87:
	.loc 1 513 0
	ldr	r3, .L94+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L90
	.loc 1 515 0
	bl	good_key_beep
	.loc 1 517 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	beq	.L91
	.loc 1 519 0
	ldr	r3, [fp, #-52]
	blx	r3
.L91:
	.loc 1 522 0
	bl	DIALOG_close_yes_no_dialog
	.loc 1 528 0
	b	.L78
.L90:
	.loc 1 526 0
	bl	bad_key_beep
	.loc 1 528 0
	b	.L78
.L83:
	.loc 1 531 0
	bl	good_key_beep
	.loc 1 533 0
	ldr	r3, .L94+8
	mov	r2, #7
	str	r2, [r3, #0]
	.loc 1 535 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	beq	.L93
	.loc 1 537 0
	ldr	r3, [fp, #-52]
	blx	r3
.L93:
	.loc 1 540 0
	bl	DIALOG_close_yes_no_dialog
	.loc 1 541 0
	b	.L78
.L80:
	.loc 1 544 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 545 0
	b	.L78
.L82:
	.loc 1 548 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 549 0
	b	.L78
.L79:
	.loc 1 552 0
	bl	bad_key_beep
.L78:
	.loc 1 554 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L95:
	.align	2
.L94:
	.word	FDTO_DIALOG_redraw_yes_no_dialog
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_DIALOG_modal_result
.LFE15:
	.size	DIALOG_process_yes_no_dialog, .-DIALOG_process_yes_no_dialog
	.section	.text.DIALOG_close_all_dialogs,"ax",%progbits
	.align	2
	.global	DIALOG_close_all_dialogs
	.type	DIALOG_close_all_dialogs, %function
DIALOG_close_all_dialogs:
.LFB16:
	.loc 1 563 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI42:
	add	fp, sp, #4
.LCFI43:
	.loc 1 564 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L97
	.loc 1 566 0
	bl	DIALOG_close_ok_dialog
	b	.L96
.L97:
	.loc 1 568 0
	ldr	r3, .L107+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L99
	.loc 1 570 0
	bl	DIALOG_close_yes_no_cancel_dialog
	b	.L96
.L99:
	.loc 1 572 0
	ldr	r3, .L107+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L100
	.loc 1 574 0
	bl	DIALOG_close_yes_no_dialog
	b	.L96
.L100:
	.loc 1 576 0
	ldr	r3, .L107+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L101
	.loc 1 578 0
	ldr	r3, .L107+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 583 0
	ldr	r3, .L107+16
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L107+20
	strh	r2, [r3, #0]	@ movhi
	.loc 1 585 0
	mov	r0, #0
	bl	Redraw_Screen
	b	.L96
.L101:
	.loc 1 587 0
	ldr	r3, .L107+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L102
	.loc 1 589 0
	ldr	r3, .L107+24
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 591 0
	mov	r0, #0
	bl	Redraw_Screen
	b	.L96
.L102:
	.loc 1 593 0
	ldr	r3, .L107+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L103
	.loc 1 595 0
	ldr	r3, .L107+28
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 599 0
	ldr	r3, .L107+32
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L107+20
	strh	r2, [r3, #0]	@ movhi
	.loc 1 601 0
	mov	r0, #0
	bl	Redraw_Screen
	b	.L96
.L103:
	.loc 1 603 0
	ldr	r3, .L107+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L104
	.loc 1 605 0
	ldr	r3, .L107+36
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 610 0
	ldr	r3, .L107+40
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L107+20
	strh	r2, [r3, #0]	@ movhi
	.loc 1 612 0
	mov	r0, #0
	bl	Redraw_Screen
	b	.L96
.L104:
	.loc 1 614 0
	ldr	r3, .L107+44
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L105
	.loc 1 616 0
	ldr	r3, .L107+44
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 620 0
	ldr	r3, .L107+48
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L107+20
	strh	r2, [r3, #0]	@ movhi
	.loc 1 622 0
	mov	r0, #0
	bl	Redraw_Screen
	b	.L96
.L105:
	.loc 1 624 0
	ldr	r3, .L107+52
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L106
	.loc 1 626 0
	ldr	r3, .L107+52
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 630 0
	ldr	r3, .L107+56
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L107+20
	strh	r2, [r3, #0]	@ movhi
	.loc 1 632 0
	mov	r0, #0
	bl	Redraw_Screen
	b	.L96
.L106:
	.loc 1 634 0
	ldr	r3, .L107+60
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L96
	.loc 1 636 0
	ldr	r3, .L107+60
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 640 0
	ldr	r3, .L107+64
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L107+20
	strh	r2, [r3, #0]	@ movhi
	.loc 1 642 0
	mov	r0, #0
	bl	Redraw_Screen
.L96:
	.loc 1 644 0
	ldmfd	sp!, {fp, pc}
.L108:
	.align	2
.L107:
	.word	g_DIALOG_ok_dialog_visible
	.word	g_DIALOG_yes_no_cancel_dialog_visible
	.word	g_DIALOG_yes_no_dialog_visible
	.word	g_COMM_OPTIONS_dialog_visible
	.word	g_COMM_OPTIONS_cursor_position_when_dialog_displayed
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_COPY_DIALOG_visible
	.word	g_LIVE_SCREENS_dialog_visible
	.word	g_LIVE_SCREENS_cursor_position_when_dialog_displayed
	.word	g_TWO_WIRE_dialog_visible
	.word	g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed
	.word	g_TWO_WIRE_DEBUG_dialog_visible
	.word	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed
	.word	g_TECH_SUPPORT_dialog_visible
	.word	g_TECH_SUPPORT_cursor_position_when_dialog_displayed
	.word	g_HUB_dialog_visible
	.word	g_HUB_cursor_position_when_dialog_displayed
.LFE16:
	.size	DIALOG_close_all_dialogs, .-DIALOG_close_all_dialogs
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI13-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI16-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI18-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI21-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI24-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI26-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI29-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI31-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI32-.LCFI31
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI34-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI35-.LCFI34
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI37-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI39-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI42-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/dialog.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_comm_options.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_copy_dialog.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_live_screens.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_tech_support.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_two_wire_assignment.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_two_wire_debug.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_hub.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x74d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF76
	.byte	0x1
	.4byte	.LASF77
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x45
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x69
	.uleb128 0x5
	.byte	0x4
	.4byte	0x96
	.uleb128 0x6
	.4byte	0x9d
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xbb
	.uleb128 0x9
	.4byte	0x9d
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xe0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x7e
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF15
	.byte	0x3
	.byte	0x80
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x82
	.4byte	0xbb
	.uleb128 0xc
	.byte	0x1
	.uleb128 0x5
	.byte	0x4
	.4byte	0xeb
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF17
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x181
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x7b
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x83
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x86
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x88
	.4byte	0x192
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x8d
	.4byte	0x1a4
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x92
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x96
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9a
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x4
	.byte	0x9c
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	0x18d
	.uleb128 0xe
	.4byte	0x18d
	.byte	0
	.uleb128 0xf
	.4byte	0x4c
	.uleb128 0x5
	.byte	0x4
	.4byte	0x181
	.uleb128 0xd
	.byte	0x1
	.4byte	0x1a4
	.uleb128 0xe
	.4byte	0xe0
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x198
	.uleb128 0x3
	.4byte	.LASF27
	.byte	0x4
	.byte	0x9e
	.4byte	0xfa
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF78
	.byte	0x1
	.byte	0x43
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1e0
	.uleb128 0x11
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x45
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.byte	0x60
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x13
	.4byte	.LASF34
	.byte	0x1
	.byte	0x77
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x21c
	.uleb128 0x14
	.4byte	.LASF28
	.byte	0x1
	.byte	0x77
	.4byte	0x21c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.4byte	0x5e
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.byte	0x86
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x257
	.uleb128 0x14
	.4byte	.LASF28
	.byte	0x1
	.byte	0x86
	.4byte	0x21c
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x11
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x88
	.4byte	0x1aa
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.byte	0x92
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.byte	0xae
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x2a2
	.uleb128 0x14
	.4byte	.LASF33
	.byte	0x1
	.byte	0xae
	.4byte	0x2a2
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x11
	.ascii	"lde\000"
	.byte	0x1
	.byte	0xb0
	.4byte	0x1aa
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xf
	.4byte	0xe0
	.uleb128 0x16
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x112
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x17
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x119
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x2e5
	.uleb128 0x18
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x119
	.4byte	0x21c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x128
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x31e
	.uleb128 0x18
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x128
	.4byte	0x21c
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1a
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x12a
	.4byte	0x1aa
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x16
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x134
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x150
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x37b
	.uleb128 0x18
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x150
	.4byte	0x2a2
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x18
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x150
	.4byte	0xed
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x1a
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x152
	.4byte	0x1aa
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x16
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x1a7
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.uleb128 0x17
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x1ae
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x3b9
	.uleb128 0x18
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x1ae
	.4byte	0x21c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x1bd
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x3f2
	.uleb128 0x18
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x1bd
	.4byte	0x21c
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1a
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x1bf
	.4byte	0x1aa
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x16
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x1c9
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x1e5
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x44f
	.uleb128 0x18
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x1e5
	.4byte	0x2a2
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x18
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x1e5
	.4byte	0xed
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x1a
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x1e7
	.4byte	0x1aa
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x232
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.uleb128 0x1c
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x165
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x45a
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF50
	.byte	0x6
	.2byte	0x127
	.4byte	0x57
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF51
	.byte	0x6
	.2byte	0x132
	.4byte	0x57
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF52
	.byte	0x7
	.byte	0x30
	.4byte	0x4bc
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xf
	.4byte	0xab
	.uleb128 0x1d
	.4byte	.LASF53
	.byte	0x7
	.byte	0x34
	.4byte	0x4d2
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xf
	.4byte	0xab
	.uleb128 0x1d
	.4byte	.LASF54
	.byte	0x7
	.byte	0x36
	.4byte	0x4e8
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xf
	.4byte	0xab
	.uleb128 0x1d
	.4byte	.LASF55
	.byte	0x7
	.byte	0x38
	.4byte	0x4fe
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xf
	.4byte	0xab
	.uleb128 0x1e
	.4byte	.LASF56
	.byte	0x8
	.byte	0x2f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF57
	.byte	0x8
	.byte	0x33
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF58
	.byte	0x9
	.byte	0x22
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF59
	.byte	0x9
	.byte	0x24
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF60
	.byte	0xa
	.byte	0x14
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF61
	.byte	0xb
	.byte	0x1f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF62
	.byte	0xb
	.byte	0x21
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x8
	.4byte	0x1aa
	.4byte	0x56e
	.uleb128 0x9
	.4byte	0x9d
	.byte	0x31
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF63
	.byte	0x4
	.byte	0xac
	.4byte	0x55e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF64
	.byte	0x4
	.byte	0xae
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF65
	.byte	0xc
	.byte	0x15
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF66
	.byte	0xc
	.byte	0x17
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF67
	.byte	0xd
	.byte	0x15
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF68
	.byte	0xd
	.byte	0x17
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF69
	.byte	0xe
	.byte	0x15
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF70
	.byte	0xe
	.byte	0x17
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF71
	.byte	0xf
	.byte	0x14
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF72
	.byte	0xf
	.byte	0x16
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF73
	.byte	0x1
	.byte	0x28
	.4byte	0x5e
	.byte	0x5
	.byte	0x3
	.4byte	g_DIALOG_cursor_position_when_dialog_displayed
	.uleb128 0x1d
	.4byte	.LASF74
	.byte	0x1
	.byte	0x31
	.4byte	0x5e
	.byte	0x5
	.byte	0x3
	.4byte	g_DIALOG_yes_no_cancel_dialog_visible
	.uleb128 0x1d
	.4byte	.LASF75
	.byte	0x1
	.byte	0x33
	.4byte	0x5e
	.byte	0x5
	.byte	0x3
	.4byte	g_DIALOG_yes_no_dialog_visible
	.uleb128 0x1c
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x165
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x45a
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF50
	.byte	0x6
	.2byte	0x127
	.4byte	0x57
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF51
	.byte	0x6
	.2byte	0x132
	.4byte	0x57
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF56
	.byte	0x1
	.byte	0x2a
	.4byte	0x5e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_DIALOG_modal_result
	.uleb128 0x1f
	.4byte	.LASF57
	.byte	0x1
	.byte	0x2f
	.4byte	0x5e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_DIALOG_ok_dialog_visible
	.uleb128 0x1e
	.4byte	.LASF58
	.byte	0x9
	.byte	0x22
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF59
	.byte	0x9
	.byte	0x24
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF60
	.byte	0xa
	.byte	0x14
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF61
	.byte	0xb
	.byte	0x1f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF62
	.byte	0xb
	.byte	0x21
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF63
	.byte	0x4
	.byte	0xac
	.4byte	0x55e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF64
	.byte	0x4
	.byte	0xae
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF65
	.byte	0xc
	.byte	0x15
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF66
	.byte	0xc
	.byte	0x17
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF67
	.byte	0xd
	.byte	0x15
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF68
	.byte	0xd
	.byte	0x17
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF69
	.byte	0xe
	.byte	0x15
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF70
	.byte	0xe
	.byte	0x17
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF71
	.byte	0xf
	.byte	0x14
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF72
	.byte	0xf
	.byte	0x16
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI14
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI17
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI32
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI35
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI38
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI40
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI43
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x9c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF76:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF17:
	.ascii	"float\000"
.LASF34:
	.ascii	"FDTO_DIALOG_draw_ok_dialog\000"
.LASF75:
	.ascii	"g_DIALOG_yes_no_dialog_visible\000"
.LASF71:
	.ascii	"g_HUB_cursor_position_when_dialog_displayed\000"
.LASF74:
	.ascii	"g_DIALOG_yes_no_cancel_dialog_visible\000"
.LASF59:
	.ascii	"g_COMM_OPTIONS_dialog_visible\000"
.LASF30:
	.ascii	"DIALOG_close_ok_dialog\000"
.LASF67:
	.ascii	"g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_d"
	.ascii	"isplayed\000"
.LASF6:
	.ascii	"short int\000"
.LASF57:
	.ascii	"g_DIALOG_ok_dialog_visible\000"
.LASF21:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF18:
	.ascii	"_01_command\000"
.LASF62:
	.ascii	"g_LIVE_SCREENS_dialog_visible\000"
.LASF48:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF38:
	.ascii	"DIALOG_close_yes_no_cancel_dialog\000"
.LASF14:
	.ascii	"keycode\000"
.LASF19:
	.ascii	"_02_menu\000"
.LASF64:
	.ascii	"screen_history_index\000"
.LASF56:
	.ascii	"g_DIALOG_modal_result\000"
.LASF78:
	.ascii	"DIALOG_a_dialog_is_visible\000"
.LASF69:
	.ascii	"g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displa"
	.ascii	"yed\000"
.LASF47:
	.ascii	"GuiVar_CodeDownloadState\000"
.LASF10:
	.ascii	"long long int\000"
.LASF54:
	.ascii	"GuiFont_DecimalChar\000"
.LASF72:
	.ascii	"g_HUB_dialog_visible\000"
.LASF13:
	.ascii	"long int\000"
.LASF61:
	.ascii	"g_LIVE_SCREENS_cursor_position_when_dialog_displaye"
	.ascii	"d\000"
.LASF68:
	.ascii	"g_TWO_WIRE_dialog_visible\000"
.LASF66:
	.ascii	"g_TECH_SUPPORT_dialog_visible\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF24:
	.ascii	"_06_u32_argument1\000"
.LASF22:
	.ascii	"key_process_func_ptr\000"
.LASF26:
	.ascii	"_08_screen_to_draw\000"
.LASF39:
	.ascii	"DIALOG_process_yes_no_cancel_dialog\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF2:
	.ascii	"signed char\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF42:
	.ascii	"FDTO_DIALOG_draw_yes_no_dialog\000"
.LASF58:
	.ascii	"g_COMM_OPTIONS_cursor_position_when_dialog_displaye"
	.ascii	"d\000"
.LASF44:
	.ascii	"DIALOG_close_yes_no_dialog\000"
.LASF45:
	.ascii	"DIALOG_process_yes_no_dialog\000"
.LASF20:
	.ascii	"_03_structure_to_draw\000"
.LASF50:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF41:
	.ascii	"FDTO_DIALOG_redraw_yes_no_dialog\000"
.LASF0:
	.ascii	"char\000"
.LASF51:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF55:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF31:
	.ascii	"DIALOG_draw_ok_dialog\000"
.LASF60:
	.ascii	"g_COPY_DIALOG_visible\000"
.LASF25:
	.ascii	"_07_u32_argument2\000"
.LASF36:
	.ascii	"DIALOG_draw_yes_no_cancel_dialog\000"
.LASF46:
	.ascii	"DIALOG_close_all_dialogs\000"
.LASF29:
	.ascii	"FDTO_DIALOG_redraw_ok_dialog\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF12:
	.ascii	"long unsigned int\000"
.LASF65:
	.ascii	"g_TECH_SUPPORT_cursor_position_when_dialog_displaye"
	.ascii	"d\000"
.LASF23:
	.ascii	"_04_func_ptr\000"
.LASF16:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF32:
	.ascii	"DIALOG_process_ok_dialog\000"
.LASF77:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/d"
	.ascii	"ialog.c\000"
.LASF15:
	.ascii	"repeats\000"
.LASF52:
	.ascii	"GuiFont_LanguageActive\000"
.LASF35:
	.ascii	"FDTO_DIALOG_draw_yes_no_cancel_dialog\000"
.LASF53:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF70:
	.ascii	"g_TWO_WIRE_DEBUG_dialog_visible\000"
.LASF49:
	.ascii	"GuiVar_StopKeyPending\000"
.LASF43:
	.ascii	"DIALOG_draw_yes_no_dialog\000"
.LASF33:
	.ascii	"pkey_event\000"
.LASF40:
	.ascii	"callback_func_ptr\000"
.LASF73:
	.ascii	"g_DIALOG_cursor_position_when_dialog_displayed\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF27:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF28:
	.ascii	"pstructure_to_draw\000"
.LASF37:
	.ascii	"FDTO_DIALOG_redraw_yes_no_cancel_dialog\000"
.LASF63:
	.ascii	"ScreenHistory\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
