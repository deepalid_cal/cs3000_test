	.file	"GuiDisplay.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.text.GuiDisplay_Refresh,"ax",%progbits
	.align	2
	.global	GuiDisplay_Refresh
	.type	GuiDisplay_Refresh, %function
GuiDisplay_Refresh:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiDisplay.c"
	.loc 1 12189 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, fp}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	.loc 1 12197 0
	mov	r3, #0
	strh	r3, [fp, #-6]	@ movhi
	b	.L2
.L7:
	.loc 1 12199 0
	ldrsh	r2, [fp, #-6]
	ldr	r1, .L8
	mov	r3, #2
	mov	r2, r2, asl #2
	add	r2, r1, r2
	add	r3, r2, r3
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L3
	.loc 1 12203 0
	mov	r3, #159
	strh	r3, [fp, #-10]	@ movhi
	.loc 1 12204 0
	ldrsh	r2, [fp, #-6]
	ldr	r1, .L8
	mov	r3, #2
	mov	r2, r2, asl #2
	add	r2, r1, r2
	add	r3, r2, r3
	ldrh	r3, [r3, #0]
	ldrsh	r2, [fp, #-10]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r2, r3
	ble	.L4
	.loc 1 12205 0
	ldrsh	r2, [fp, #-6]
	ldr	r1, .L8
	mov	r3, #2
	mov	r2, r2, asl #2
	add	r2, r1, r2
	add	r3, r2, r3
	ldrh	r3, [r3, #0]	@ movhi
	strh	r3, [fp, #-10]	@ movhi
.L4:
	.loc 1 12207 0
	ldrsh	r3, [fp, #-6]
	ldr	r2, .L8
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldrh	r3, [r3, #0]	@ movhi
	strh	r3, [fp, #-8]	@ movhi
	b	.L5
.L6:
	.loc 1 12210 0 discriminator 2
	ldrsh	r2, [fp, #-6]
	ldrsh	ip, [fp, #-8]
	ldrsh	r1, [fp, #-6]
	ldrsh	r4, [fp, #-8]
	ldr	r0, .L8+4
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #5
	add	r3, r3, r4
	add	r3, r0, r3
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldr	r0, .L8+8
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #5
	add	r3, r3, ip
	add	r3, r0, r3
	mov	r2, r1
	strb	r2, [r3, #0]
	.loc 1 12207 0 discriminator 2
	ldrh	r3, [fp, #-8]	@ movhi
	add	r3, r3, #1
	strh	r3, [fp, #-8]	@ movhi
.L5:
	.loc 1 12207 0 is_stmt 0 discriminator 1
	ldrsh	r2, [fp, #-8]
	ldrsh	r3, [fp, #-10]
	cmp	r2, r3
	ble	.L6
	.loc 1 12217 0 is_stmt 1
	ldrsh	r2, [fp, #-6]
	ldr	r1, .L8
	mov	r3, #2
	mov	r2, r2, asl #2
	add	r2, r1, r2
	add	r3, r2, r3
	mvn	r2, #0
	strh	r2, [r3, #0]	@ movhi
.L3:
	.loc 1 12197 0
	ldrh	r3, [fp, #-6]	@ movhi
	add	r3, r3, #1
	strh	r3, [fp, #-6]	@ movhi
.L2:
	.loc 1 12197 0 is_stmt 0 discriminator 1
	ldrsh	r3, [fp, #-6]
	cmp	r3, #239
	ble	.L7
	.loc 1 12223 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {r4, fp}
	bx	lr
.L9:
	.align	2
.L8:
	.word	GuiLib_DisplayRepaint
	.word	gui_lib_display_buf
	.word	primary_display_buf
.LFE0:
	.size	GuiDisplay_Refresh, .-GuiDisplay_Refresh
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8b
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/lcd_init.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1cc
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF22
	.byte	0x1
	.4byte	.LASF23
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x2
	.byte	0xac
	.4byte	0x4a
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x2
	.byte	0xae
	.4byte	0x4a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x2
	.byte	0xaf
	.4byte	0x4a
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x2
	.byte	0xb1
	.4byte	0x25
	.uleb128 0x6
	.4byte	0x73
	.4byte	0x6c
	.uleb128 0x7
	.4byte	0x6c
	.byte	0x1
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x8
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.4byte	.LASF8
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x3
	.byte	0x3a
	.4byte	0x73
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.4byte	.LASF11
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x6
	.4byte	0x9d
	.4byte	0xd3
	.uleb128 0x7
	.4byte	0x6c
	.byte	0xef
	.uleb128 0x7
	.4byte	0x6c
	.byte	0x9f
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x2f9c
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x117
	.uleb128 0xa
	.ascii	"Y\000"
	.byte	0x1
	.2byte	0x2f9e
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -10
	.uleb128 0xa
	.ascii	"X\000"
	.byte	0x1
	.2byte	0x2f9f
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x2fa0
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -14
	.byte	0
	.uleb128 0x6
	.4byte	0x51
	.4byte	0x127
	.uleb128 0x7
	.4byte	0x6c
	.byte	0xef
	.byte	0
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x125
	.4byte	0x117
	.byte	0x1
	.byte	0x1
	.uleb128 0xd
	.4byte	.LASF15
	.byte	0x4
	.byte	0x30
	.4byte	0x146
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0x5c
	.uleb128 0xd
	.4byte	.LASF16
	.byte	0x4
	.byte	0x34
	.4byte	0x15c
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0x5c
	.uleb128 0xd
	.4byte	.LASF17
	.byte	0x4
	.byte	0x36
	.4byte	0x172
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0x5c
	.uleb128 0xd
	.4byte	.LASF18
	.byte	0x4
	.byte	0x38
	.4byte	0x188
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0x5c
	.uleb128 0xf
	.4byte	.LASF20
	.byte	0x5
	.byte	0x37
	.4byte	0xbd
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF21
	.byte	0x5
	.byte	0x3b
	.4byte	0xbd
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x125
	.4byte	0x117
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF20
	.byte	0x5
	.byte	0x37
	.4byte	0xbd
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF21
	.byte	0x5
	.byte	0x3b
	.4byte	0xbd
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF15:
	.ascii	"GuiFont_LanguageActive\000"
.LASF0:
	.ascii	"ByteBegin\000"
.LASF18:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF9:
	.ascii	"GuiLib_DisplayLineRec\000"
.LASF20:
	.ascii	"gui_lib_display_buf\000"
.LASF14:
	.ascii	"LastByte\000"
.LASF23:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/"
	.ascii	"library_src/GuiDisplay.c\000"
.LASF13:
	.ascii	"long long unsigned int\000"
.LASF4:
	.ascii	"unsigned char\000"
.LASF3:
	.ascii	"long unsigned int\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF24:
	.ascii	"GuiDisplay_Refresh\000"
.LASF16:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF10:
	.ascii	"UNS_8\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF8:
	.ascii	"char\000"
.LASF21:
	.ascii	"primary_display_buf\000"
.LASF7:
	.ascii	"long long int\000"
.LASF1:
	.ascii	"ByteEnd\000"
.LASF2:
	.ascii	"short int\000"
.LASF22:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF6:
	.ascii	"long int\000"
.LASF11:
	.ascii	"signed char\000"
.LASF19:
	.ascii	"GuiLib_DisplayRepaint\000"
.LASF17:
	.ascii	"GuiFont_DecimalChar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
