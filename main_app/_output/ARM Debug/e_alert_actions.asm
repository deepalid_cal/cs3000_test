	.file	"e_alert_actions.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_ALERT_ACTIONS_combo_box_guivar,"aw",%nobits
	.align	2
	.type	g_ALERT_ACTIONS_combo_box_guivar, %object
	.size	g_ALERT_ACTIONS_combo_box_guivar, 4
g_ALERT_ACTIONS_combo_box_guivar:
	.space	4
	.section	.text.FDTO_ALERT_ACTIONS_show_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_ALERT_ACTIONS_show_dropdown, %function
FDTO_ALERT_ACTIONS_show_dropdown:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_alert_actions.c"
	.loc 1 75 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	.loc 1 76 0
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	ldr	r0, .L2+4
	ldr	r1, .L2+8
	mov	r2, #3
	bl	FDTO_COMBOBOX_show
	.loc 1 77 0
	ldmfd	sp!, {fp, pc}
.L3:
	.align	2
.L2:
	.word	g_ALERT_ACTIONS_combo_box_guivar
	.word	718
	.word	FDTO_COMBOBOX_add_items
.LFE0:
	.size	FDTO_ALERT_ACTIONS_show_dropdown, .-FDTO_ALERT_ACTIONS_show_dropdown
	.section	.text.FDTO_ALERT_ACTIONS_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_ALERT_ACTIONS_draw_screen
	.type	FDTO_ALERT_ACTIONS_draw_screen, %function
FDTO_ALERT_ACTIONS_draw_screen:
.LFB1:
	.loc 1 81 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI2:
	add	fp, sp, #4
.LCFI3:
	sub	sp, sp, #8
.LCFI4:
	str	r0, [fp, #-12]
	.loc 1 84 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L5
	.loc 1 86 0
	bl	ALERT_ACTIONS_copy_group_into_guivars
	.loc 1 88 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L6
.L5:
	.loc 1 92 0
	ldr	r3, .L7
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L6:
	.loc 1 95 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #8
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 96 0
	bl	GuiLib_Refresh
	.loc 1 97 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L8:
	.align	2
.L7:
	.word	GuiLib_ActiveCursorFieldNo
.LFE1:
	.size	FDTO_ALERT_ACTIONS_draw_screen, .-FDTO_ALERT_ACTIONS_draw_screen
	.section	.text.ALERT_ACTIONS_process_screen,"ax",%progbits
	.align	2
	.global	ALERT_ACTIONS_process_screen
	.type	ALERT_ACTIONS_process_screen, %function
ALERT_ACTIONS_process_screen:
.LFB2:
	.loc 1 116 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI5:
	add	fp, sp, #8
.LCFI6:
	sub	sp, sp, #52
.LCFI7:
	str	r0, [fp, #-52]
	str	r1, [fp, #-48]
	.loc 1 119 0
	ldr	r3, .L85
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L85+4
	cmp	r2, r3
	bne	.L84
.L11:
	.loc 1 122 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L85+8
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	COMBO_BOX_key_press
	.loc 1 123 0
	b	.L9
.L84:
	.loc 1 126 0
	ldr	r3, [fp, #-52]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L13
.L21:
	.word	.L14
	.word	.L15
	.word	.L16
	.word	.L17
	.word	.L18
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L18
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L14
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L19
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L20
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L20
.L16:
	.loc 1 129 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #19
	ldrls	pc, [pc, r3, asl #2]
	b	.L22
.L43:
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
.L23:
	.loc 1 132 0
	ldr	r3, .L85+8
	ldr	r2, .L85+16
	str	r2, [r3, #0]
	.loc 1 133 0
	b	.L22
.L24:
	.loc 1 136 0
	ldr	r3, .L85+8
	ldr	r2, .L85+20
	str	r2, [r3, #0]
	.loc 1 137 0
	b	.L22
.L25:
	.loc 1 140 0
	ldr	r3, .L85+8
	ldr	r2, .L85+24
	str	r2, [r3, #0]
	.loc 1 141 0
	b	.L22
.L26:
	.loc 1 144 0
	ldr	r3, .L85+8
	ldr	r2, .L85+28
	str	r2, [r3, #0]
	.loc 1 145 0
	b	.L22
.L27:
	.loc 1 148 0
	ldr	r3, .L85+8
	ldr	r2, .L85+32
	str	r2, [r3, #0]
	.loc 1 149 0
	b	.L22
.L28:
	.loc 1 152 0
	ldr	r3, .L85+8
	ldr	r2, .L85+36
	str	r2, [r3, #0]
	.loc 1 153 0
	b	.L22
.L29:
	.loc 1 156 0
	ldr	r3, .L85+8
	ldr	r2, .L85+40
	str	r2, [r3, #0]
	.loc 1 157 0
	b	.L22
.L30:
	.loc 1 160 0
	ldr	r3, .L85+8
	ldr	r2, .L85+44
	str	r2, [r3, #0]
	.loc 1 161 0
	b	.L22
.L31:
	.loc 1 164 0
	ldr	r3, .L85+8
	ldr	r2, .L85+48
	str	r2, [r3, #0]
	.loc 1 165 0
	b	.L22
.L32:
	.loc 1 168 0
	ldr	r3, .L85+8
	ldr	r2, .L85+52
	str	r2, [r3, #0]
	.loc 1 169 0
	b	.L22
.L33:
	.loc 1 172 0
	ldr	r3, .L85+8
	ldr	r2, .L85+56
	str	r2, [r3, #0]
	.loc 1 173 0
	b	.L22
.L34:
	.loc 1 176 0
	ldr	r3, .L85+8
	ldr	r2, .L85+60
	str	r2, [r3, #0]
	.loc 1 177 0
	b	.L22
.L35:
	.loc 1 180 0
	ldr	r3, .L85+8
	ldr	r2, .L85+64
	str	r2, [r3, #0]
	.loc 1 181 0
	b	.L22
.L36:
	.loc 1 184 0
	ldr	r3, .L85+8
	ldr	r2, .L85+68
	str	r2, [r3, #0]
	.loc 1 185 0
	b	.L22
.L37:
	.loc 1 188 0
	ldr	r3, .L85+8
	ldr	r2, .L85+72
	str	r2, [r3, #0]
	.loc 1 189 0
	b	.L22
.L38:
	.loc 1 192 0
	ldr	r3, .L85+8
	ldr	r2, .L85+76
	str	r2, [r3, #0]
	.loc 1 193 0
	b	.L22
.L39:
	.loc 1 196 0
	ldr	r3, .L85+8
	ldr	r2, .L85+80
	str	r2, [r3, #0]
	.loc 1 197 0
	b	.L22
.L40:
	.loc 1 200 0
	ldr	r3, .L85+8
	ldr	r2, .L85+84
	str	r2, [r3, #0]
	.loc 1 201 0
	b	.L22
.L41:
	.loc 1 204 0
	ldr	r3, .L85+8
	ldr	r2, .L85+88
	str	r2, [r3, #0]
	.loc 1 205 0
	b	.L22
.L42:
	.loc 1 208 0
	ldr	r3, .L85+8
	ldr	r2, .L85+92
	str	r2, [r3, #0]
	.loc 1 209 0
	mov	r0, r0	@ nop
.L22:
	.loc 1 212 0
	ldr	r3, .L85+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L44
	.loc 1 214 0
	bl	good_key_beep
	.loc 1 216 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L45
	.loc 1 218 0
	ldr	r3, .L85+96
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 219 0
	ldr	r3, .L85+100
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L46
.L45:
	.loc 1 221 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #10
	bne	.L47
	.loc 1 223 0
	ldr	r3, .L85+96
	mov	r2, #94
	str	r2, [r3, #0]
	.loc 1 224 0
	ldr	r3, .L85+100
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L46
.L47:
	.loc 1 228 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	bgt	.L48
	.loc 1 228 0 is_stmt 0 discriminator 1
	mov	r3, #0
	b	.L49
.L48:
	.loc 1 228 0 discriminator 2
	mov	r3, #94
.L49:
	.loc 1 228 0 discriminator 3
	ldr	r2, .L85+96
	str	r3, [r2, #0]
	.loc 1 229 0 is_stmt 1 discriminator 3
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L85+104
	smull	r1, r3, r2, r3
	mov	r1, r3, asr #2
	mov	r3, r2, asr #31
	rsb	r1, r3, r1
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #1
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #1
	mov	r2, r3
	ldr	r3, .L85+100
	str	r2, [r3, #0]
.L46:
	.loc 1 232 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 233 0
	ldr	r3, .L85+108
	str	r3, [fp, #-24]
	.loc 1 234 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 240 0
	b	.L9
.L44:
	.loc 1 238 0
	bl	bad_key_beep
	.loc 1 240 0
	b	.L9
.L20:
	.loc 1 244 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #19
	ldrls	pc, [pc, r3, asl #2]
	b	.L51
.L72:
	.word	.L52
	.word	.L53
	.word	.L54
	.word	.L55
	.word	.L56
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L70
	.word	.L71
.L52:
	.loc 1 247 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+16
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 248 0
	b	.L73
.L53:
	.loc 1 251 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+20
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 252 0
	b	.L73
.L54:
	.loc 1 255 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+24
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 256 0
	b	.L73
.L55:
	.loc 1 259 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+28
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 260 0
	b	.L73
.L56:
	.loc 1 263 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+32
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 264 0
	b	.L73
.L57:
	.loc 1 267 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+36
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 268 0
	b	.L73
.L58:
	.loc 1 271 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+40
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 272 0
	b	.L73
.L59:
	.loc 1 275 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+44
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 276 0
	b	.L73
.L60:
	.loc 1 279 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+48
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 280 0
	b	.L73
.L61:
	.loc 1 283 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+52
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 284 0
	b	.L73
.L62:
	.loc 1 287 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+56
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 288 0
	b	.L73
.L63:
	.loc 1 291 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+60
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 292 0
	b	.L73
.L64:
	.loc 1 295 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+64
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 296 0
	b	.L73
.L65:
	.loc 1 299 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+68
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 300 0
	b	.L73
.L66:
	.loc 1 303 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+72
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 304 0
	b	.L73
.L67:
	.loc 1 307 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+76
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 308 0
	b	.L73
.L68:
	.loc 1 311 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+80
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 312 0
	b	.L73
.L69:
	.loc 1 315 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+84
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 316 0
	b	.L73
.L70:
	.loc 1 319 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+88
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 320 0
	b	.L73
.L71:
	.loc 1 323 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L85+92
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 324 0
	b	.L73
.L51:
	.loc 1 327 0
	bl	bad_key_beep
.L73:
	.loc 1 329 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 330 0
	b	.L9
.L18:
	.loc 1 334 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L85+104
	smull	r1, r3, r2, r3
	mov	r1, r3, asr #2
	mov	r3, r2, asr #31
	rsb	r1, r3, r1
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #1
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #0
	bne	.L74
	.loc 1 336 0
	bl	bad_key_beep
	.loc 1 342 0
	b	.L9
.L74:
	.loc 1 340 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 342 0
	b	.L9
.L14:
	.loc 1 346 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L85+104
	smull	r1, r3, r2, r3
	mov	r1, r3, asr #2
	mov	r3, r2, asr #31
	rsb	r1, r3, r1
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #1
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r4, r3, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r3, r0
	sub	r3, r3, #1
	cmp	r4, r3
	bne	.L76
	.loc 1 348 0
	bl	bad_key_beep
	.loc 1 354 0
	b	.L9
.L76:
	.loc 1 352 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 354 0
	b	.L9
.L15:
	.loc 1 357 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L78
	.loc 1 357 0 is_stmt 0 discriminator 1
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	bgt	.L78
	.loc 1 359 0 is_stmt 1
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #9
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L79
.L78:
	.loc 1 361 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ble	.L80
	.loc 1 361 0 is_stmt 0 discriminator 1
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #19
	bgt	.L80
	.loc 1 363 0 is_stmt 1
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #10
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L79
.L80:
	.loc 1 367 0
	bl	bad_key_beep
	.loc 1 369 0
	b	.L9
.L79:
	b	.L9
.L17:
	.loc 1 372 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L81
	.loc 1 372 0 is_stmt 0 discriminator 1
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	bgt	.L81
	.loc 1 374 0 is_stmt 1
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #10
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L82
.L81:
	.loc 1 376 0
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ble	.L83
	.loc 1 376 0 is_stmt 0 discriminator 1
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #19
	bgt	.L83
	.loc 1 378 0 is_stmt 1
	ldr	r3, .L85+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #9
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L82
.L83:
	.loc 1 382 0
	bl	bad_key_beep
	.loc 1 384 0
	b	.L9
.L82:
	b	.L9
.L19:
	.loc 1 387 0
	ldr	r3, .L85+112
	mov	r2, #3
	str	r2, [r3, #0]
	.loc 1 388 0
	bl	ALERT_ACTIONS_extract_and_store_changes_from_GuiVars
.L13:
	.loc 1 393 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L9:
	.loc 1 396 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L86:
	.align	2
.L85:
	.word	GuiLib_CurStructureNdx
	.word	718
	.word	g_ALERT_ACTIONS_combo_box_guivar
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_ComboBox_X1
	.word	GuiVar_ComboBox_Y1
	.word	1717986919
	.word	FDTO_ALERT_ACTIONS_show_dropdown
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	ALERT_ACTIONS_process_screen, .-ALERT_ACTIONS_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x554
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF62
	.byte	0x1
	.4byte	.LASF63
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8b
	.uleb128 0x6
	.4byte	0x92
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xb0
	.uleb128 0x9
	.4byte	0x92
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd5
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x82
	.4byte	0xb0
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x167
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x88
	.4byte	0x178
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x8d
	.4byte	0x18a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x92
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x173
	.uleb128 0xd
	.4byte	0x173
	.byte	0
	.uleb128 0xe
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x167
	.uleb128 0xc
	.byte	0x1
	.4byte	0x18a
	.uleb128 0xd
	.4byte	0xd5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x17e
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9e
	.4byte	0xe0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF26
	.uleb128 0xf
	.4byte	.LASF64
	.byte	0x1
	.byte	0x4a
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.byte	0x50
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x1ec
	.uleb128 0x11
	.4byte	.LASF29
	.byte	0x1
	.byte	0x50
	.4byte	0x1ec
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF31
	.byte	0x1
	.byte	0x52
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x7a
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.byte	0x73
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x227
	.uleb128 0x11
	.4byte	.LASF30
	.byte	0x1
	.byte	0x73
	.4byte	0x227
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x75
	.4byte	0x190
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0xe
	.4byte	0xd5
	.uleb128 0x14
	.4byte	.LASF32
	.byte	0x5
	.2byte	0x167
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF33
	.byte	0x5
	.2byte	0x168
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF34
	.byte	0x5
	.2byte	0x211
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF35
	.byte	0x5
	.2byte	0x212
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x213
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x214
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x215
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x216
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x217
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x218
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x219
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x21a
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x21b
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x21c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x21d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x21e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x21f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x220
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF50
	.byte	0x5
	.2byte	0x221
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF51
	.byte	0x5
	.2byte	0x222
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF52
	.byte	0x5
	.2byte	0x223
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF53
	.byte	0x5
	.2byte	0x224
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF54
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF55
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF56
	.byte	0x6
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x7
	.byte	0x30
	.4byte	0x39b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF58
	.byte	0x7
	.byte	0x34
	.4byte	0x3b1
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF59
	.byte	0x7
	.byte	0x36
	.4byte	0x3c7
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF60
	.byte	0x7
	.byte	0x38
	.4byte	0x3dd
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF61
	.byte	0x1
	.byte	0x36
	.4byte	0x3f3
	.byte	0x5
	.byte	0x3
	.4byte	g_ALERT_ACTIONS_combo_box_guivar
	.uleb128 0x5
	.byte	0x4
	.4byte	0x53
	.uleb128 0x14
	.4byte	.LASF32
	.byte	0x5
	.2byte	0x167
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF33
	.byte	0x5
	.2byte	0x168
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF34
	.byte	0x5
	.2byte	0x211
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF35
	.byte	0x5
	.2byte	0x212
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x213
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x214
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x215
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x216
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x217
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x218
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x219
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x21a
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x21b
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x21c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x21d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x21e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x21f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x220
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF50
	.byte	0x5
	.2byte	0x221
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF51
	.byte	0x5
	.2byte	0x222
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF52
	.byte	0x5
	.2byte	0x223
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF53
	.byte	0x5
	.2byte	0x224
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF54
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF55
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF56
	.byte	0x6
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF32:
	.ascii	"GuiVar_ComboBox_X1\000"
.LASF28:
	.ascii	"ALERT_ACTIONS_process_screen\000"
.LASF4:
	.ascii	"short int\000"
.LASF61:
	.ascii	"g_ALERT_ACTIONS_combo_box_guivar\000"
.LASF16:
	.ascii	"_01_command\000"
.LASF45:
	.ascii	"GuiVar_GroupSettingB_1\000"
.LASF54:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF50:
	.ascii	"GuiVar_GroupSettingB_6\000"
.LASF13:
	.ascii	"keycode\000"
.LASF52:
	.ascii	"GuiVar_GroupSettingB_8\000"
.LASF17:
	.ascii	"_02_menu\000"
.LASF38:
	.ascii	"GuiVar_GroupSettingA_4\000"
.LASF33:
	.ascii	"GuiVar_ComboBox_Y1\000"
.LASF2:
	.ascii	"signed char\000"
.LASF26:
	.ascii	"float\000"
.LASF9:
	.ascii	"long long int\000"
.LASF59:
	.ascii	"GuiFont_DecimalChar\000"
.LASF12:
	.ascii	"long int\000"
.LASF63:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_alert_actions.c\000"
.LASF22:
	.ascii	"_06_u32_argument1\000"
.LASF20:
	.ascii	"key_process_func_ptr\000"
.LASF24:
	.ascii	"_08_screen_to_draw\000"
.LASF29:
	.ascii	"pcomplete_redraw\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF44:
	.ascii	"GuiVar_GroupSettingB_0\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF46:
	.ascii	"GuiVar_GroupSettingB_2\000"
.LASF47:
	.ascii	"GuiVar_GroupSettingB_3\000"
.LASF48:
	.ascii	"GuiVar_GroupSettingB_4\000"
.LASF49:
	.ascii	"GuiVar_GroupSettingB_5\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF51:
	.ascii	"GuiVar_GroupSettingB_7\000"
.LASF58:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF53:
	.ascii	"GuiVar_GroupSettingB_9\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF18:
	.ascii	"_03_structure_to_draw\000"
.LASF55:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF34:
	.ascii	"GuiVar_GroupSettingA_0\000"
.LASF35:
	.ascii	"GuiVar_GroupSettingA_1\000"
.LASF36:
	.ascii	"GuiVar_GroupSettingA_2\000"
.LASF37:
	.ascii	"GuiVar_GroupSettingA_3\000"
.LASF0:
	.ascii	"char\000"
.LASF39:
	.ascii	"GuiVar_GroupSettingA_5\000"
.LASF40:
	.ascii	"GuiVar_GroupSettingA_6\000"
.LASF41:
	.ascii	"GuiVar_GroupSettingA_7\000"
.LASF42:
	.ascii	"GuiVar_GroupSettingA_8\000"
.LASF43:
	.ascii	"GuiVar_GroupSettingA_9\000"
.LASF60:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF19:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF23:
	.ascii	"_07_u32_argument2\000"
.LASF27:
	.ascii	"FDTO_ALERT_ACTIONS_draw_screen\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF62:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF21:
	.ascii	"_04_func_ptr\000"
.LASF15:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF56:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF14:
	.ascii	"repeats\000"
.LASF57:
	.ascii	"GuiFont_LanguageActive\000"
.LASF31:
	.ascii	"lcursor_to_select\000"
.LASF64:
	.ascii	"FDTO_ALERT_ACTIONS_show_dropdown\000"
.LASF30:
	.ascii	"pkey_event\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF25:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
