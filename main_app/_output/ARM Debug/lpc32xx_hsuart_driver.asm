	.file	"lpc32xx_hsuart_driver.c"
	.text
.Ltext0:
	.section	.bss.hsuartdat,"aw",%nobits
	.align	2
	.type	hsuartdat, %object
	.size	hsuartdat, 96
hsuartdat:
	.space	96
	.section	.text.hsuart_gen_int_handler,"ax",%progbits
	.align	2
	.global	hsuart_gen_int_handler
	.type	hsuart_gen_int_handler, %function
hsuart_gen_int_handler:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_hsuart_driver.c"
	.loc 1 63 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-12]
	.loc 1 67 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #8]
	str	r3, [fp, #-8]
	.loc 1 69 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #6
	cmp	r3, #0
	beq	.L2
	.loc 1 72 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L3
	.loc 1 74 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	sub	r2, fp, #12
	mov	r0, r2
	blx	r3
	b	.L2
.L3:
	.loc 1 79 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	bic	r2, r2, #64
	str	r2, [r3, #12]
.L2:
	.loc 1 83 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L4
	.loc 1 86 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L5
	.loc 1 88 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #8]
	sub	r2, fp, #12
	mov	r0, r2
	blx	r3
	b	.L4
.L5:
	.loc 1 93 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	bic	r2, r2, #32
	str	r2, [r3, #12]
.L4:
	.loc 1 97 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #56
	cmp	r3, #0
	beq	.L6
	.loc 1 100 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	cmp	r3, #0
	beq	.L7
	.loc 1 102 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	sub	r2, fp, #12
	mov	r0, r2
	blx	r3
	b	.L6
.L7:
	.loc 1 107 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	bic	r2, r2, #128
	str	r2, [r3, #12]
.L6:
	.loc 1 112 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #8]
	.loc 1 113 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE0:
	.size	hsuart_gen_int_handler, .-hsuart_gen_int_handler
	.section	.text.uart1_int_handler,"ax",%progbits
	.align	2
	.global	uart1_int_handler
	.type	uart1_int_handler, %function
uart1_int_handler:
.LFB1:
	.loc 1 135 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 136 0
	ldr	r0, .L9
	bl	hsuart_gen_int_handler
	.loc 1 137 0
	ldmfd	sp!, {fp, pc}
.L10:
	.align	2
.L9:
	.word	hsuartdat
.LFE1:
	.size	uart1_int_handler, .-uart1_int_handler
	.section	.text.uart2_int_handler,"ax",%progbits
	.align	2
	.global	uart2_int_handler
	.type	uart2_int_handler, %function
uart2_int_handler:
.LFB2:
	.loc 1 159 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	.loc 1 160 0
	ldr	r0, .L12
	bl	hsuart_gen_int_handler
	.loc 1 161 0
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	hsuartdat+32
.LFE2:
	.size	uart2_int_handler, .-uart2_int_handler
	.section	.text.uart7_int_handler,"ax",%progbits
	.align	2
	.global	uart7_int_handler
	.type	uart7_int_handler, %function
uart7_int_handler:
.LFB3:
	.loc 1 183 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI7:
	add	fp, sp, #4
.LCFI8:
	.loc 1 184 0
	ldr	r0, .L15
	bl	hsuart_gen_int_handler
	.loc 1 185 0
	ldmfd	sp!, {fp, pc}
.L16:
	.align	2
.L15:
	.word	hsuartdat+64
.LFE3:
	.size	uart7_int_handler, .-uart7_int_handler
	.section	.text.hsuart_abs,"ax",%progbits
	.align	2
	.global	hsuart_abs
	.type	hsuart_abs, %function
hsuart_abs:
.LFB4:
	.loc 1 208 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 209 0
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	ble	.L18
	.loc 1 211 0
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-8]
	rsb	r3, r3, r2
	b	.L19
.L18:
	.loc 1 214 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-4]
	rsb	r3, r3, r2
.L19:
	.loc 1 215 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE4:
	.size	hsuart_abs, .-hsuart_abs
	.section	.text.hsuart_ptr_to_hsuart_num,"ax",%progbits
	.align	2
	.global	hsuart_ptr_to_hsuart_num
	.type	hsuart_ptr_to_hsuart_num, %function
hsuart_ptr_to_hsuart_num:
.LFB5:
	.loc 1 235 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI12:
	add	fp, sp, #0
.LCFI13:
	sub	sp, sp, #8
.LCFI14:
	str	r0, [fp, #-8]
	.loc 1 236 0
	mvn	r3, #0
	str	r3, [fp, #-4]
	.loc 1 238 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L24
	cmp	r2, r3
	bne	.L21
	.loc 1 240 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L22
.L21:
	.loc 1 242 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L24+4
	cmp	r2, r3
	bne	.L23
	.loc 1 244 0
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L22
.L23:
	.loc 1 246 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L24+8
	cmp	r2, r3
	bne	.L22
	.loc 1 248 0
	mov	r3, #2
	str	r3, [fp, #-4]
.L22:
	.loc 1 251 0
	ldr	r3, [fp, #-4]
	.loc 1 252 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L25:
	.align	2
.L24:
	.word	1073823744
	.word	1073840128
	.word	1073856512
.LFE5:
	.size	hsuart_ptr_to_hsuart_num, .-hsuart_ptr_to_hsuart_num
	.section	.text.hsuart_flush_fifos,"ax",%progbits
	.align	2
	.global	hsuart_flush_fifos
	.type	hsuart_flush_fifos, %function
hsuart_flush_fifos:
.LFB6:
	.loc 1 276 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI15:
	add	fp, sp, #0
.LCFI16:
	sub	sp, sp, #12
.LCFI17:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 281 0
	mov	r0, r0	@ nop
.L27:
	.loc 1 281 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	and	r3, r3, #65280
	cmp	r3, #0
	bne	.L27
	.loc 1 282 0 is_stmt 1
	b	.L28
.L29:
	.loc 1 284 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	strb	r3, [fp, #-1]
.L28:
	.loc 1 282 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L29
	.loc 1 286 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE6:
	.size	hsuart_flush_fifos, .-hsuart_flush_fifos
	.global	__udivsi3
	.section	.text.hsuart_find_clk,"ax",%progbits
	.align	2
	.global	hsuart_find_clk
	.type	hsuart_find_clk, %function
hsuart_find_clk:
.LFB7:
	.loc 1 311 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #32
.LCFI20:
	str	r0, [fp, #-32]
	str	r1, [fp, #-36]
	.loc 1 316 0
	mov	r0, #5
	bl	clkpwr_get_base_clock_rate
	str	r0, [fp, #-24]
	.loc 1 319 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 320 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 321 0
	mvn	r3, #0
	str	r3, [fp, #-12]
	.loc 1 322 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L31
.L33:
	.loc 1 324 0
	ldr	r3, [fp, #-16]
	add	r2, r3, #1
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	ldr	r0, [fp, #-24]
	mov	r1, r3
	bl	__udivsi3
	mov	r3, r0
	str	r3, [fp, #-28]
	.loc 1 325 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-32]
	mov	r0, r2
	mov	r1, r3
	bl	hsuart_abs
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bcs	.L32
	.loc 1 327 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-32]
	mov	r0, r2
	mov	r1, r3
	bl	hsuart_abs
	mov	r3, r0
	str	r3, [fp, #-12]
	.loc 1 328 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-8]
	.loc 1 329 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-20]
.L32:
	.loc 1 322 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L31:
	.loc 1 322 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #255
	ble	.L33
	.loc 1 334 0 is_stmt 1
	ldr	r3, [fp, #-36]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #24]
	.loc 1 336 0
	ldr	r3, [fp, #-8]
	.loc 1 337 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE7:
	.size	hsuart_find_clk, .-hsuart_find_clk
	.section	.text.hsuart_setup_trans_mode,"ax",%progbits
	.align	2
	.global	hsuart_setup_trans_mode
	.type	hsuart_setup_trans_mode, %function
hsuart_setup_trans_mode:
.LFB8:
	.loc 1 360 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #12
.LCFI23:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 361 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 364 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, [fp, #-12]
	bl	hsuart_find_clk
	mov	r2, r0
	ldr	r3, [fp, #-12]
	str	r2, [r3, #20]
	.loc 1 367 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #16]
	cmp	r3, #1
	beq	.L37
	cmp	r3, #2
	beq	.L38
	cmp	r3, #0
	bne	.L46
.L36:
	.loc 1 370 0
	ldr	r3, .L47
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #24]
	str	r2, [r3, #16]
	.loc 1 371 0
	b	.L39
.L37:
	.loc 1 374 0
	ldr	r3, .L47+4
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #24]
	str	r2, [r3, #16]
	.loc 1 375 0
	b	.L39
.L38:
	.loc 1 378 0
	ldr	r3, .L47+8
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #24]
	str	r2, [r3, #16]
	.loc 1 379 0
	b	.L39
.L46:
	.loc 1 382 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	.loc 1 383 0
	mov	r0, r0	@ nop
.L39:
	.loc 1 386 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L40
	.loc 1 389 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L41
	.loc 1 391 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	orr	r2, r2, #16384
	str	r2, [r3, #12]
	.loc 1 393 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L42
	.loc 1 395 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	orr	r2, r2, #32768
	str	r2, [r3, #12]
	b	.L43
.L42:
	.loc 1 399 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	bic	r2, r2, #32768
	str	r2, [r3, #12]
	b	.L43
.L41:
	.loc 1 404 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	bic	r2, r2, #16384
	str	r2, [r3, #12]
.L43:
	.loc 1 408 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #12]
	cmp	r3, #0
	beq	.L44
	.loc 1 410 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	orr	r2, r2, #262144
	str	r2, [r3, #12]
	.loc 1 412 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L45
	.loc 1 414 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	orr	r2, r2, #2097152
	str	r2, [r3, #12]
	b	.L40
.L45:
	.loc 1 418 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	bic	r2, r2, #2097152
	str	r2, [r3, #12]
	b	.L40
.L44:
	.loc 1 423 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #12]
	bic	r2, r2, #262144
	str	r2, [r3, #12]
.L40:
	.loc 1 427 0
	ldr	r3, [fp, #-8]
	.loc 1 428 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L48:
	.align	2
.L47:
	.word	1073823744
	.word	1073840128
	.word	1073856512
.LFE8:
	.size	hsuart_setup_trans_mode, .-hsuart_setup_trans_mode
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI9-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI12-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI15-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI18-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI21-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_hsuart.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_hsuart_driver.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x4ad
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF69
	.byte	0x1
	.4byte	.LASF70
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x3a
	.4byte	0x45
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x6c
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x67
	.4byte	0x7e
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x6c
	.uleb128 0x5
	.ascii	"PFV\000"
	.byte	0x2
	.byte	0xb9
	.4byte	0xa9
	.uleb128 0x6
	.byte	0x4
	.4byte	0xaf
	.uleb128 0x7
	.4byte	0xb6
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x2
	.byte	0xfd
	.4byte	0x73
	.uleb128 0x9
	.4byte	0x61
	.uleb128 0xa
	.byte	0x14
	.byte	0x3
	.byte	0x27
	.4byte	0x115
	.uleb128 0xb
	.4byte	.LASF15
	.byte	0x3
	.byte	0x28
	.4byte	0xc1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x3
	.byte	0x29
	.4byte	0xc1
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.ascii	"iir\000"
	.byte	0x3
	.byte	0x2a
	.4byte	0xc1
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x3
	.byte	0x2b
	.4byte	0xc1
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x3
	.byte	0x2c
	.4byte	0xc1
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x3
	.byte	0x2d
	.4byte	0xc6
	.uleb128 0xa
	.byte	0x14
	.byte	0x4
	.byte	0x22
	.4byte	0x16f
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x24
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x25
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x26
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x27
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x28
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x4
	.byte	0x29
	.4byte	0x120
	.uleb128 0xa
	.byte	0xc
	.byte	0x4
	.byte	0x31
	.4byte	0x1ad
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x4
	.byte	0x35
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x4
	.byte	0x38
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x4
	.byte	0x3b
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x4
	.byte	0x3c
	.4byte	0x17a
	.uleb128 0xa
	.byte	0x20
	.byte	0x4
	.byte	0x3f
	.4byte	0x215
	.uleb128 0xb
	.4byte	.LASF30
	.byte	0x4
	.byte	0x41
	.4byte	0x215
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.ascii	"cbs\000"
	.byte	0x4
	.byte	0x42
	.4byte	0x1ad
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF31
	.byte	0x4
	.byte	0x43
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x4
	.byte	0x44
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF33
	.byte	0x4
	.byte	0x45
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF34
	.byte	0x4
	.byte	0x46
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x115
	.uleb128 0x3
	.4byte	.LASF35
	.byte	0x4
	.byte	0x47
	.4byte	0x1b8
	.uleb128 0xd
	.byte	0x4
	.byte	0x5
	.byte	0x4d
	.4byte	0x271
	.uleb128 0xe
	.4byte	.LASF36
	.sleb128 0
	.uleb128 0xe
	.4byte	.LASF37
	.sleb128 1
	.uleb128 0xe
	.4byte	.LASF38
	.sleb128 2
	.uleb128 0xe
	.4byte	.LASF39
	.sleb128 3
	.uleb128 0xe
	.4byte	.LASF40
	.sleb128 4
	.uleb128 0xe
	.4byte	.LASF41
	.sleb128 5
	.uleb128 0xe
	.4byte	.LASF42
	.sleb128 6
	.uleb128 0xe
	.4byte	.LASF43
	.sleb128 7
	.uleb128 0xe
	.4byte	.LASF44
	.sleb128 8
	.uleb128 0xe
	.4byte	.LASF45
	.sleb128 9
	.uleb128 0xe
	.4byte	.LASF46
	.sleb128 10
	.byte	0
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF54
	.byte	0x1
	.byte	0x3e
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x2a7
	.uleb128 0x10
	.4byte	.LASF50
	.byte	0x1
	.byte	0x3e
	.4byte	0x2a7
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.ascii	"tmp\000"
	.byte	0x1
	.byte	0x40
	.4byte	0xc1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x21b
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF47
	.byte	0x1
	.byte	0x86
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF48
	.byte	0x1
	.byte	0x9e
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF49
	.byte	0x1
	.byte	0xb6
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF51
	.byte	0x1
	.byte	0xcf
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x324
	.uleb128 0x14
	.ascii	"v1\000"
	.byte	0x1
	.byte	0xcf
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x14
	.ascii	"v2\000"
	.byte	0x1
	.byte	0xcf
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF52
	.byte	0x1
	.byte	0xea
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x35e
	.uleb128 0x10
	.4byte	.LASF53
	.byte	0x1
	.byte	0xea
	.4byte	0x215
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x15
	.4byte	.LASF31
	.byte	0x1
	.byte	0xec
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x112
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x3a6
	.uleb128 0x17
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x112
	.4byte	0x215
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x113
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x115
	.4byte	0x3a
	.byte	0x2
	.byte	0x91
	.sleb128 -5
	.byte	0
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x135
	.byte	0x1
	.4byte	0x61
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x43d
	.uleb128 0x17
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x135
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x17
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x136
	.4byte	0x2a7
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x18
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x138
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x18
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x138
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x138
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x138
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x139
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x139
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x167
	.byte	0x1
	.4byte	0xb6
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x489
	.uleb128 0x17
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x167
	.4byte	0x2a7
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x167
	.4byte	0x489
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.ascii	"err\000"
	.byte	0x1
	.2byte	0x169
	.4byte	0xb6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x16f
	.uleb128 0x1b
	.4byte	0x21b
	.4byte	0x49f
	.uleb128 0x1c
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x15
	.4byte	.LASF68
	.byte	0x1
	.byte	0x23
	.4byte	0x48f
	.byte	0x5
	.byte	0x3
	.4byte	hsuartdat
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI8
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF50:
	.ascii	"phsuartcfg\000"
.LASF31:
	.ascii	"hsuartnum\000"
.LASF64:
	.ascii	"basepclk\000"
.LASF70:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_hsuart_driver.c\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF20:
	.ascii	"baud_rate\000"
.LASF63:
	.ascii	"diff\000"
.LASF21:
	.ascii	"cts_en\000"
.LASF39:
	.ascii	"CLKPWR_ARM_CLK\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF49:
	.ascii	"uart7_int_handler\000"
.LASF12:
	.ascii	"long long int\000"
.LASF4:
	.ascii	"signed char\000"
.LASF60:
	.ascii	"freq\000"
.LASF18:
	.ascii	"rate\000"
.LASF32:
	.ascii	"baudrate\000"
.LASF1:
	.ascii	"long int\000"
.LASF68:
	.ascii	"hsuartdat\000"
.LASF38:
	.ascii	"CLKPWR_SYSCLK\000"
.LASF42:
	.ascii	"CLKPWR_USB_HCLK_SYS\000"
.LASF30:
	.ascii	"regptr\000"
.LASF28:
	.ascii	"rxerrcb\000"
.LASF67:
	.ascii	"phsuartsetup\000"
.LASF66:
	.ascii	"hsuart_setup_trans_mode\000"
.LASF48:
	.ascii	"uart2_int_handler\000"
.LASF58:
	.ascii	"dummy\000"
.LASF25:
	.ascii	"HSUART_CONTROL_T\000"
.LASF62:
	.ascii	"savedclkrate\000"
.LASF57:
	.ascii	"flushword\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF53:
	.ascii	"phsuart\000"
.LASF10:
	.ascii	"INT_32\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF16:
	.ascii	"level\000"
.LASF54:
	.ascii	"hsuart_gen_int_handler\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF69:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF46:
	.ascii	"CLKPWR_BASE_INVALID\000"
.LASF37:
	.ascii	"CLKPWR_RTC_CLK\000"
.LASF23:
	.ascii	"rts_en\000"
.LASF22:
	.ascii	"cts_inv\000"
.LASF24:
	.ascii	"rts_inv\000"
.LASF15:
	.ascii	"txrx_fifo\000"
.LASF35:
	.ascii	"HSUART_CFG_T\000"
.LASF41:
	.ascii	"CLKPWR_PERIPH_CLK\000"
.LASF27:
	.ascii	"txcb\000"
.LASF43:
	.ascii	"CLKPWR_48M_CLK\000"
.LASF34:
	.ascii	"hsuart_init\000"
.LASF45:
	.ascii	"CLKPWR_MSSD_CLK\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF61:
	.ascii	"clkrate\000"
.LASF6:
	.ascii	"short int\000"
.LASF51:
	.ascii	"hsuart_abs\000"
.LASF59:
	.ascii	"hsuart_find_clk\000"
.LASF47:
	.ascii	"uart1_int_handler\000"
.LASF19:
	.ascii	"HSUART_REGS_T\000"
.LASF26:
	.ascii	"rxcb\000"
.LASF2:
	.ascii	"char\000"
.LASF29:
	.ascii	"HSUART_CBS_T\000"
.LASF36:
	.ascii	"CLKPWR_MAINOSC_CLK\000"
.LASF40:
	.ascii	"CLKPWR_HCLK\000"
.LASF56:
	.ascii	"pregs\000"
.LASF17:
	.ascii	"ctrl\000"
.LASF55:
	.ascii	"hsuart_flush_fifos\000"
.LASF14:
	.ascii	"STATUS\000"
.LASF65:
	.ascii	"idiv\000"
.LASF7:
	.ascii	"UNS_8\000"
.LASF44:
	.ascii	"CLKPWR_DDR_CLK\000"
.LASF33:
	.ascii	"divider\000"
.LASF52:
	.ascii	"hsuart_ptr_to_hsuart_num\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
