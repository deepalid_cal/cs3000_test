	.file	"analog_420ma_sensors.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.analog_420ma_sensor_group_list_hdr,"aw",%nobits
	.align	2
	.type	analog_420ma_sensor_group_list_hdr, %object
	.size	analog_420ma_sensor_group_list_hdr, 20
analog_420ma_sensor_group_list_hdr:
	.space	20
	.section .rodata
	.align	2
.LC0:
	.ascii	"Name\000"
	.align	2
.LC1:
	.ascii	"BoxIndex\000"
	.align	2
.LC2:
	.ascii	"InUse\000"
	.section	.rodata.ANALOG_420MA_SENSOR_database_field_names,"a",%progbits
	.align	2
	.type	ANALOG_420MA_SENSOR_database_field_names, %object
	.size	ANALOG_420MA_SENSOR_database_field_names, 12
ANALOG_420MA_SENSOR_database_field_names:
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.section	.text.nm_ANALOG_420MA_SENSOR_set_name,"ax",%progbits
	.align	2
	.global	nm_ANALOG_420MA_SENSOR_set_name
	.type	nm_ANALOG_420MA_SENSOR_set_name, %function
nm_ANALOG_420MA_SENSOR_set_name:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_analog_420ma_sensors.c"
	.loc 1 146 0
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #36
.LCFI2:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 147 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #80
	ldr	r2, [fp, #4]
	str	r2, [sp, #0]
	ldr	r2, [fp, #8]
	str	r2, [sp, #4]
	ldr	r2, [fp, #12]
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	mov	r3, #0
	str	r3, [sp, #16]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	bl	SHARED_set_name_32_bit_change_bits
	.loc 1 163 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE0:
	.size	nm_ANALOG_420MA_SENSOR_set_name, .-nm_ANALOG_420MA_SENSOR_set_name
	.section	.text.nm_ANALOG_420MA_SENSOR_set_box_index,"ax",%progbits
	.align	2
	.global	nm_ANALOG_420MA_SENSOR_set_box_index
	.type	nm_ANALOG_420MA_SENSOR_set_box_index, %function
nm_ANALOG_420MA_SENSOR_set_box_index:
.LFB1:
	.loc 1 179 0
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #60
.LCFI5:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 180 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #88
	ldr	r2, [fp, #-8]
	add	r1, r2, #80
	.loc 1 194 0
	ldr	r2, .L3
	ldr	r2, [r2, #4]
	.loc 1 180 0
	mov	r0, #11
	str	r0, [sp, #0]
	mov	r0, #0
	str	r0, [sp, #4]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #8]
	ldr	r0, .L3+4
	str	r0, [sp, #12]
	ldr	r0, [fp, #-20]
	str	r0, [sp, #16]
	ldr	r0, [fp, #4]
	str	r0, [sp, #20]
	ldr	r0, [fp, #8]
	str	r0, [sp, #24]
	ldr	r0, [fp, #12]
	str	r0, [sp, #28]
	str	r1, [sp, #32]
	mov	r1, #1
	str	r1, [sp, #36]
	str	r2, [sp, #40]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, [fp, #-12]
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	.loc 1 201 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L4:
	.align	2
.L3:
	.word	ANALOG_420MA_SENSOR_database_field_names
	.word	49627
.LFE1:
	.size	nm_ANALOG_420MA_SENSOR_set_box_index, .-nm_ANALOG_420MA_SENSOR_set_box_index
	.section	.text.nm_ANALOG_420MA_SENSOR_set_in_use,"ax",%progbits
	.align	2
	.global	nm_ANALOG_420MA_SENSOR_set_in_use
	.type	nm_ANALOG_420MA_SENSOR_set_in_use, %function
nm_ANALOG_420MA_SENSOR_set_in_use:
.LFB2:
	.loc 1 218 0
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #52
.LCFI8:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 219 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #96
	ldr	r2, [fp, #-8]
	add	r1, r2, #80
	.loc 1 231 0
	ldr	r2, .L6
	ldr	r2, [r2, #8]
	.loc 1 219 0
	ldr	r0, [fp, #-16]
	str	r0, [sp, #0]
	ldr	r0, .L6+4
	str	r0, [sp, #4]
	ldr	r0, [fp, #-20]
	str	r0, [sp, #8]
	ldr	r0, [fp, #4]
	str	r0, [sp, #12]
	ldr	r0, [fp, #8]
	str	r0, [sp, #16]
	ldr	r0, [fp, #12]
	str	r0, [sp, #20]
	str	r1, [sp, #24]
	mov	r1, #2
	str	r1, [sp, #28]
	str	r2, [sp, #32]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, [fp, #-12]
	mov	r3, #0
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	.loc 1 238 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L7:
	.align	2
.L6:
	.word	ANALOG_420MA_SENSOR_database_field_names
	.word	49629
.LFE2:
	.size	nm_ANALOG_420MA_SENSOR_set_in_use, .-nm_ANALOG_420MA_SENSOR_set_in_use
	.section .rodata
	.align	2
.LC3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_analog_420ma_sensors.c\000"
	.section	.text.nm_ANALOG_420MA_SENSOR_store_changes,"ax",%progbits
	.align	2
	.type	nm_ANALOG_420MA_SENSOR_store_changes, %function
nm_ANALOG_420MA_SENSOR_store_changes:
.LFB3:
	.loc 1 254 0
	@ args = 12, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #36
.LCFI11:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 269 0
	sub	r3, fp, #12
	ldr	r0, .L16
	ldr	r1, [fp, #-16]
	mov	r2, r3
	bl	nm_GROUP_find_this_group_in_list
	.loc 1 271 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L9
	.loc 1 280 0
	ldr	r3, [fp, #-12]
	mov	r0, r3
	ldr	r1, [fp, #8]
	bl	ANALOG_420MA_SENSOR_get_change_bits_ptr
	str	r0, [fp, #-8]
	.loc 1 292 0
	ldr	r3, [fp, #-24]
	cmp	r3, #2
	beq	.L10
	.loc 1 292 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #12]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L11
.L10:
	.loc 1 294 0 is_stmt 1
	ldr	r1, [fp, #-12]
	ldr	r3, [fp, #-16]
	add	r2, r3, #20
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-28]
	str	r0, [sp, #0]
	ldr	r0, [fp, #4]
	str	r0, [sp, #4]
	ldr	r0, [fp, #-8]
	str	r0, [sp, #8]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	ldr	r3, [fp, #-24]
	bl	nm_ANALOG_420MA_SENSOR_set_name
.L11:
	.loc 1 301 0
	ldr	r3, [fp, #-24]
	cmp	r3, #2
	beq	.L12
	.loc 1 301 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #12]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L13
.L12:
	.loc 1 303 0 is_stmt 1
	ldr	r1, [fp, #-12]
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #88]
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-28]
	str	r0, [sp, #0]
	ldr	r0, [fp, #4]
	str	r0, [sp, #4]
	ldr	r0, [fp, #-8]
	str	r0, [sp, #8]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	ldr	r3, [fp, #-24]
	bl	nm_ANALOG_420MA_SENSOR_set_box_index
.L13:
	.loc 1 310 0
	ldr	r3, [fp, #-24]
	cmp	r3, #2
	beq	.L14
	.loc 1 310 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #12]
	and	r3, r3, #4
	cmp	r3, #0
	beq	.L8
.L14:
	.loc 1 312 0 is_stmt 1
	ldr	r1, [fp, #-12]
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #96]
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-28]
	str	r0, [sp, #0]
	ldr	r0, [fp, #4]
	str	r0, [sp, #4]
	ldr	r0, [fp, #-8]
	str	r0, [sp, #8]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	ldr	r3, [fp, #-24]
	bl	nm_ANALOG_420MA_SENSOR_set_in_use
	b	.L8
.L9:
	.loc 1 323 0
	ldr	r0, .L16+4
	ldr	r1, .L16+8
	bl	Alert_group_not_found_with_filename
.L8:
	.loc 1 326 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	analog_420ma_sensor_group_list_hdr
	.word	.LC3
	.word	323
.LFE3:
	.size	nm_ANALOG_420MA_SENSOR_store_changes, .-nm_ANALOG_420MA_SENSOR_store_changes
	.section	.text.nm_ANALOG_420MA_SENSOR_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_ANALOG_420MA_SENSOR_extract_and_store_changes_from_comm
	.type	nm_ANALOG_420MA_SENSOR_extract_and_store_changes_from_comm, %function
nm_ANALOG_420MA_SENSOR_extract_and_store_changes_from_comm:
.LFB4:
	.loc 1 365 0
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #64
.LCFI14:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	str	r2, [fp, #-52]
	str	r3, [fp, #-56]
	.loc 1 397 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 399 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 404 0
	sub	r3, fp, #40
	mov	r0, r3
	ldr	r1, [fp, #-44]
	mov	r2, #4
	bl	memcpy
	.loc 1 405 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #4
	str	r3, [fp, #-44]
	.loc 1 406 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #4
	str	r3, [fp, #-20]
	.loc 1 426 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L19
.L28:
	.loc 1 429 0
	sub	r3, fp, #28
	mov	r0, r3
	ldr	r1, [fp, #-44]
	mov	r2, #4
	bl	memcpy
	.loc 1 430 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #4
	str	r3, [fp, #-44]
	.loc 1 431 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #4
	str	r3, [fp, #-20]
	.loc 1 433 0
	sub	r3, fp, #32
	mov	r0, r3
	ldr	r1, [fp, #-44]
	mov	r2, #4
	bl	memcpy
	.loc 1 434 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #4
	str	r3, [fp, #-44]
	.loc 1 435 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #4
	str	r3, [fp, #-20]
	.loc 1 437 0
	sub	r3, fp, #36
	mov	r0, r3
	ldr	r1, [fp, #-44]
	mov	r2, #4
	bl	memcpy
	.loc 1 438 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #4
	str	r3, [fp, #-44]
	.loc 1 439 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #4
	str	r3, [fp, #-20]
	.loc 1 468 0
	ldr	r3, [fp, #-28]
	ldr	r0, .L32
	mov	r1, r3
	mov	r2, #1
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	str	r0, [fp, #-8]
	.loc 1 472 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L20
	.loc 1 479 0
	ldr	r0, .L32
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 483 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L21
	.loc 1 483 0 is_stmt 0 discriminator 1
	ldr	r3, .L32
	ldr	r3, [r3, #8]
	cmp	r3, #1
	bne	.L21
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #16]
	cmp	r3, #0
	bne	.L21
	.loc 1 488 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r2, r3, #80
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_set_all_32_bit_change_bits
	b	.L22
.L21:
	.loc 1 494 0
	bl	nm_ANALOG_420MA_SENSOR_create_new_group
	str	r0, [fp, #-8]
	.loc 1 496 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L22:
	.loc 1 501 0
	ldr	r3, [fp, #-56]
	cmp	r3, #16
	bne	.L20
	.loc 1 507 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #72
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
.L20:
	.loc 1 522 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L23
	.loc 1 528 0
	mov	r0, #120
	ldr	r1, .L32+8
	mov	r2, #528
	bl	mem_malloc_debug
	str	r0, [fp, #-24]
	.loc 1 533 0
	ldr	r0, [fp, #-24]
	mov	r1, #0
	mov	r2, #120
	bl	memset
	.loc 1 540 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-8]
	mov	r2, #120
	bl	memcpy
	.loc 1 548 0
	ldr	r3, [fp, #-36]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L24
	.loc 1 550 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #20
	mov	r0, r3
	ldr	r1, [fp, #-44]
	mov	r2, #48
	bl	memcpy
	.loc 1 551 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #48
	str	r3, [fp, #-44]
	.loc 1 552 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #48
	str	r3, [fp, #-20]
.L24:
	.loc 1 555 0
	ldr	r3, [fp, #-36]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L25
	.loc 1 557 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #88
	mov	r0, r3
	ldr	r1, [fp, #-44]
	mov	r2, #4
	bl	memcpy
	.loc 1 558 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #4
	str	r3, [fp, #-44]
	.loc 1 559 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #4
	str	r3, [fp, #-20]
.L25:
	.loc 1 562 0
	ldr	r3, [fp, #-36]
	and	r3, r3, #4
	cmp	r3, #0
	beq	.L26
	.loc 1 564 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #96
	mov	r0, r3
	ldr	r1, [fp, #-44]
	mov	r2, #4
	bl	memcpy
	.loc 1 565 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #4
	str	r3, [fp, #-44]
	.loc 1 566 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #4
	str	r3, [fp, #-20]
.L26:
	.loc 1 571 0
	ldr	r3, [fp, #-36]
	ldr	r2, [fp, #-52]
	str	r2, [sp, #0]
	ldr	r2, [fp, #-56]
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-48]
	mov	r3, #0
	bl	nm_ANALOG_420MA_SENSOR_store_changes
	.loc 1 581 0
	ldr	r0, [fp, #-24]
	ldr	r1, .L32+8
	ldr	r2, .L32+12
	bl	mem_free_debug
	b	.L27
.L23:
	.loc 1 600 0
	ldr	r0, .L32+8
	mov	r1, #600
	bl	Alert_group_not_found_with_filename
.L27:
	.loc 1 426 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L19:
	.loc 1 426 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-40]
	ldr	r2, [fp, #-16]
	cmp	r2, r3
	bcc	.L28
	.loc 1 609 0 is_stmt 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L29
	.loc 1 622 0
	ldr	r3, [fp, #-56]
	cmp	r3, #1
	beq	.L30
	.loc 1 622 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-56]
	cmp	r3, #15
	beq	.L30
	.loc 1 623 0 is_stmt 1
	ldr	r3, [fp, #-56]
	cmp	r3, #16
	bne	.L31
	.loc 1 624 0
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	mov	r3, r0
	cmp	r3, #0
	bne	.L31
.L30:
	.loc 1 629 0
	mov	r0, #21
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L31
.L29:
	.loc 1 634 0
	ldr	r0, .L32+8
	ldr	r1, .L32+16
	bl	Alert_bit_set_with_no_data_with_filename
.L31:
	.loc 1 639 0
	ldr	r3, [fp, #-20]
	.loc 1 640 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L33:
	.align	2
.L32:
	.word	analog_420ma_sensor_group_list_hdr
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC3
	.word	581
	.word	634
.LFE4:
	.size	nm_ANALOG_420MA_SENSOR_extract_and_store_changes_from_comm, .-nm_ANALOG_420MA_SENSOR_extract_and_store_changes_from_comm
	.section	.rodata.ANALOG_420MA_SENSOR_FILENAME,"a",%progbits
	.align	2
	.type	ANALOG_420MA_SENSOR_FILENAME, %object
	.size	ANALOG_420MA_SENSOR_FILENAME, 21
ANALOG_420MA_SENSOR_FILENAME:
	.ascii	"ANALOG_420MA_SENSORS\000"
	.global	ANALOG_420MA_SENSOR_DEFAULT_GROUP_NAME
	.section	.rodata.ANALOG_420MA_SENSOR_DEFAULT_GROUP_NAME,"a",%progbits
	.align	2
	.type	ANALOG_420MA_SENSOR_DEFAULT_GROUP_NAME, %object
	.size	ANALOG_420MA_SENSOR_DEFAULT_GROUP_NAME, 20
ANALOG_420MA_SENSOR_DEFAULT_GROUP_NAME:
	.ascii	"ANALOG 420MA SENSOR\000"
	.global	analog_420ma_sensor_list_item_sizes
	.section	.rodata.analog_420ma_sensor_list_item_sizes,"a",%progbits
	.align	2
	.type	analog_420ma_sensor_list_item_sizes, %object
	.size	analog_420ma_sensor_list_item_sizes, 8
analog_420ma_sensor_list_item_sizes:
	.word	120
	.word	120
	.section .rodata
	.align	2
.LC4:
	.ascii	"ANALOG_420MA_SENSOR updater error\000"
	.section	.text.nm_analog_420ma_sensor_structure_updater,"ax",%progbits
	.align	2
	.type	nm_analog_420ma_sensor_structure_updater, %function
nm_analog_420ma_sensor_structure_updater:
.LFB5:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/analog_420ma_sensors.c"
	.loc 2 110 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #4
.LCFI17:
	str	r0, [fp, #-8]
	.loc 2 119 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	beq	.L34
	.loc 2 121 0
	ldr	r0, .L36
	bl	Alert_Message
.L34:
	.loc 2 123 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L37:
	.align	2
.L36:
	.word	.LC4
.LFE5:
	.size	nm_analog_420ma_sensor_structure_updater, .-nm_analog_420ma_sensor_structure_updater
	.section .rodata
	.align	2
.LC5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/analog_420ma_sensors.c\000"
	.section	.text.init_file_analog_420ma_sensor,"ax",%progbits
	.align	2
	.global	init_file_analog_420ma_sensor
	.type	init_file_analog_420ma_sensor, %function
init_file_analog_420ma_sensor:
.LFB6:
	.loc 2 127 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #28
.LCFI20:
	.loc 2 132 0
	ldr	r3, .L41
	ldr	r3, [r3, #0]
	ldr	r2, .L41+4
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	ldr	r3, .L41+8
	str	r3, [sp, #8]
	ldr	r3, .L41+12
	str	r3, [sp, #12]
	ldr	r3, .L41+16
	str	r3, [sp, #16]
	mov	r3, #21
	str	r3, [sp, #20]
	mov	r0, #1
	ldr	r1, .L41+20
	mov	r2, #1
	ldr	r3, .L41+24
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	.loc 2 149 0
	ldr	r3, .L41
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L41+28
	mov	r3, #149
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 151 0
	ldr	r0, .L41+24
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 153 0
	b	.L39
.L40:
	.loc 2 155 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #112]
	.loc 2 157 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #116]
	.loc 2 160 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #108]
	.loc 2 164 0
	ldr	r0, .L41+24
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L39:
	.loc 2 153 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L40
	.loc 2 167 0
	ldr	r3, .L41
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 168 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L42:
	.align	2
.L41:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	analog_420ma_sensor_list_item_sizes
	.word	nm_analog_420ma_sensor_structure_updater
	.word	nm_ANALOG_420MA_SENSOR_set_default_values
	.word	ANALOG_420MA_SENSOR_DEFAULT_GROUP_NAME
	.word	ANALOG_420MA_SENSOR_FILENAME
	.word	analog_420ma_sensor_group_list_hdr
	.word	.LC5
.LFE6:
	.size	init_file_analog_420ma_sensor, .-init_file_analog_420ma_sensor
	.section	.text.save_file_analog_420ma_sensor,"ax",%progbits
	.align	2
	.global	save_file_analog_420ma_sensor
	.type	save_file_analog_420ma_sensor, %function
save_file_analog_420ma_sensor:
.LFB7:
	.loc 2 172 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #12
.LCFI23:
	.loc 2 173 0
	ldr	r3, .L44
	ldr	r3, [r3, #0]
	mov	r2, #120
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	mov	r3, #21
	str	r3, [sp, #8]
	mov	r0, #1
	ldr	r1, .L44+4
	mov	r2, #1
	ldr	r3, .L44+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	.loc 2 180 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L45:
	.align	2
.L44:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	ANALOG_420MA_SENSOR_FILENAME
	.word	analog_420ma_sensor_group_list_hdr
.LFE7:
	.size	save_file_analog_420ma_sensor, .-save_file_analog_420ma_sensor
	.section	.text.nm_ANALOG_420MA_SENSOR_set_default_values,"ax",%progbits
	.align	2
	.type	nm_ANALOG_420MA_SENSOR_set_default_values, %function
nm_ANALOG_420MA_SENSOR_set_default_values:
.LFB8:
	.loc 2 184 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #28
.LCFI26:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 2 191 0
	bl	FLOWSENSE_get_controller_index
	str	r0, [fp, #-8]
	.loc 2 195 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L47
	.loc 2 199 0
	ldr	r3, [fp, #-16]
	add	r2, r3, #72
	ldr	r3, .L50
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
	.loc 2 200 0
	ldr	r3, [fp, #-16]
	add	r2, r3, #76
	ldr	r3, .L50
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
	.loc 2 201 0
	ldr	r3, [fp, #-16]
	add	r2, r3, #84
	ldr	r3, .L50
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
	.loc 2 211 0
	ldr	r3, .L50+4
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L48
	.loc 2 213 0
	ldr	r3, [fp, #-16]
	add	r2, r3, #80
	ldr	r3, .L50
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_set_all_32_bit_change_bits
.L48:
	.loc 2 221 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #72
	str	r3, [fp, #-12]
	.loc 2 225 0
	ldr	r3, [fp, #-8]
	str	r3, [sp, #0]
	ldr	r3, [fp, #-20]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-12]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-8]
	mov	r2, #0
	mov	r3, #11
	bl	nm_ANALOG_420MA_SENSOR_set_box_index
	.loc 2 227 0
	ldr	r3, [fp, #-8]
	str	r3, [sp, #0]
	ldr	r3, [fp, #-20]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-12]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	mov	r1, #0
	mov	r2, #0
	mov	r3, #11
	bl	nm_ANALOG_420MA_SENSOR_set_in_use
	b	.L46
.L47:
	.loc 2 231 0
	ldr	r0, .L50+8
	mov	r1, #231
	bl	Alert_func_call_with_null_ptr_with_filename
.L46:
	.loc 2 233 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L51:
	.align	2
.L50:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	analog_420ma_sensor_group_list_hdr
	.word	.LC5
.LFE8:
	.size	nm_ANALOG_420MA_SENSOR_set_default_values, .-nm_ANALOG_420MA_SENSOR_set_default_values
	.section	.text.nm_ANLAOG_420MA_SENSOR_create_new_group,"ax",%progbits
	.align	2
	.global	nm_ANLAOG_420MA_SENSOR_create_new_group
	.type	nm_ANLAOG_420MA_SENSOR_create_new_group, %function
nm_ANLAOG_420MA_SENSOR_create_new_group:
.LFB9:
	.loc 2 255 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #8
.LCFI29:
	.loc 2 258 0
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r0, .L53
	ldr	r1, .L53+4
	ldr	r2, .L53+8
	mov	r3, #120
	bl	nm_GROUP_create_new_group
	mov	r3, r0
	.loc 2 259 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L54:
	.align	2
.L53:
	.word	analog_420ma_sensor_group_list_hdr
	.word	ANALOG_420MA_SENSOR_DEFAULT_GROUP_NAME
	.word	nm_ANALOG_420MA_SENSOR_set_default_values
.LFE9:
	.size	nm_ANLAOG_420MA_SENSOR_create_new_group, .-nm_ANLAOG_420MA_SENSOR_create_new_group
	.section	.text.ANALOG_420MA_SENSOR_build_data_to_send,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_build_data_to_send
	.type	ANALOG_420MA_SENSOR_build_data_to_send, %function
ANALOG_420MA_SENSOR_build_data_to_send:
.LFB10:
	.loc 2 294 0
	@ args = 4, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #80
.LCFI32:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	str	r2, [fp, #-52]
	str	r3, [fp, #-56]
	.loc 2 320 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 2 322 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 2 327 0
	ldr	r3, .L70
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L70+4
	ldr	r3, .L70+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 331 0
	ldr	r0, .L70+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 335 0
	b	.L56
.L59:
	.loc 2 337 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-56]
	bl	ANALOG_420MA_SENSOR_get_change_bits_ptr
	mov	r3, r0
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L57
	.loc 2 339 0
	ldr	r3, [fp, #-40]
	add	r3, r3, #1
	str	r3, [fp, #-40]
	.loc 2 343 0
	b	.L58
.L57:
	.loc 2 346 0
	ldr	r0, .L70+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L56:
	.loc 2 335 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L59
.L58:
	.loc 2 351 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L60
	.loc 2 356 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 2 364 0
	sub	r3, fp, #32
	ldr	r2, [fp, #4]
	str	r2, [sp, #0]
	ldr	r0, [fp, #-44]
	mov	r1, r3
	ldr	r2, [fp, #-48]
	ldr	r3, [fp, #-52]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	str	r0, [fp, #-12]
	.loc 2 370 0
	ldr	r3, [fp, #-52]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L60
	.loc 2 372 0
	ldr	r0, .L70+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 374 0
	b	.L61
.L67:
	.loc 2 376 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-56]
	bl	ANALOG_420MA_SENSOR_get_change_bits_ptr
	str	r0, [fp, #-16]
	.loc 2 378 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L62
	.loc 2 383 0
	mov	r3, #12
	str	r3, [fp, #-20]
	.loc 2 388 0
	ldr	r3, [fp, #-52]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L63
	.loc 2 388 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-48]
	ldr	r3, [fp, #-12]
	add	r2, r2, r3
	ldr	r3, [fp, #4]
	cmp	r2, r3
	bcs	.L63
	.loc 2 390 0 is_stmt 1
	sub	r3, fp, #36
	ldr	r0, [fp, #-44]
	mov	r1, #4
	mov	r2, #4
	bl	PDATA_copy_bitfield_info_into_pucp
	.loc 2 403 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 2 408 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 2 410 0
	ldr	r3, [fp, #-8]
	add	r0, r3, #84
	.loc 2 415 0
	ldr	r3, [fp, #-8]
	add	r1, r3, #88
	.loc 2 417 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-12]
	add	r2, r2, r3
	.loc 2 410 0
	ldr	r3, [fp, #-48]
	add	r2, r2, r3
	sub	r3, fp, #28
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	mov	r1, #4
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	ldr	r2, [fp, #-52]
	str	r2, [sp, #16]
	ldr	r2, [fp, #4]
	str	r2, [sp, #20]
	ldr	r2, [fp, #-56]
	str	r2, [sp, #24]
	ldr	r0, [fp, #-44]
	mov	r1, r3
	mov	r2, #1
	ldr	r3, [fp, #-16]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	mov	r3, r0
	ldr	r2, [fp, #-24]
	add	r3, r2, r3
	str	r3, [fp, #-24]
	.loc 2 422 0
	ldr	r3, [fp, #-8]
	add	r0, r3, #84
	.loc 2 427 0
	ldr	r3, [fp, #-8]
	add	r1, r3, #96
	.loc 2 429 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-12]
	add	r2, r2, r3
	.loc 2 422 0
	ldr	r3, [fp, #-48]
	add	r2, r2, r3
	sub	r3, fp, #28
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	mov	r1, #4
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	ldr	r2, [fp, #-52]
	str	r2, [sp, #16]
	ldr	r2, [fp, #4]
	str	r2, [sp, #20]
	ldr	r2, [fp, #-56]
	str	r2, [sp, #24]
	ldr	r0, [fp, #-44]
	mov	r1, r3
	mov	r2, #2
	ldr	r3, [fp, #-16]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	mov	r3, r0
	ldr	r2, [fp, #-24]
	add	r3, r2, r3
	str	r3, [fp, #-24]
	.loc 2 440 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bhi	.L64
	b	.L69
.L63:
	.loc 2 396 0
	ldr	r3, [fp, #-52]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 2 400 0
	b	.L66
.L64:
	.loc 2 444 0
	ldr	r3, [fp, #-40]
	add	r3, r3, #1
	str	r3, [fp, #-40]
	.loc 2 446 0
	ldr	r2, [fp, #-36]
	sub	r3, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	memcpy
	.loc 2 448 0
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-24]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	b	.L62
.L69:
	.loc 2 453 0
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	rsb	r3, r3, #0
	add	r2, r2, r3
	ldr	r3, [fp, #-44]
	str	r2, [r3, #0]
.L62:
	.loc 2 458 0
	ldr	r0, .L70+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L61:
	.loc 2 374 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L67
.L66:
	.loc 2 465 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L68
	.loc 2 467 0
	ldr	r2, [fp, #-32]
	sub	r3, fp, #40
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	memcpy
	b	.L60
.L68:
	.loc 2 474 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	sub	r2, r3, #4
	ldr	r3, [fp, #-44]
	str	r2, [r3, #0]
	.loc 2 476 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L60:
	.loc 2 489 0
	ldr	r3, .L70
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 493 0
	ldr	r3, [fp, #-12]
	.loc 2 494 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L71:
	.align	2
.L70:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	327
	.word	analog_420ma_sensor_group_list_hdr
.LFE10:
	.size	ANALOG_420MA_SENSOR_build_data_to_send, .-ANALOG_420MA_SENSOR_build_data_to_send
	.section	.text.ANALOG_420MA_SENSOR_get_pointer_to_sensor_with_this_box_index,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_get_pointer_to_sensor_with_this_box_index
	.type	ANALOG_420MA_SENSOR_get_pointer_to_sensor_with_this_box_index, %function
ANALOG_420MA_SENSOR_get_pointer_to_sensor_with_this_box_index:
.LFB11:
	.loc 2 498 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #8
.LCFI35:
	str	r0, [fp, #-12]
	.loc 2 506 0
	ldr	r0, .L78
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 508 0
	b	.L73
.L76:
	.loc 2 510 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #88]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	beq	.L77
.L74:
	.loc 2 515 0
	ldr	r0, .L78
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L73:
	.loc 2 508 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L76
	b	.L75
.L77:
	.loc 2 512 0
	mov	r0, r0	@ nop
.L75:
	.loc 2 518 0
	ldr	r3, [fp, #-8]
	.loc 2 519 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L79:
	.align	2
.L78:
	.word	analog_420ma_sensor_group_list_hdr
.LFE11:
	.size	ANALOG_420MA_SENSOR_get_pointer_to_sensor_with_this_box_index, .-ANALOG_420MA_SENSOR_get_pointer_to_sensor_with_this_box_index
	.section	.text.ANALOG_420MA_SENSOR_get_group_with_this_GID,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_get_group_with_this_GID
	.type	ANALOG_420MA_SENSOR_get_group_with_this_GID, %function
ANALOG_420MA_SENSOR_get_group_with_this_GID:
.LFB12:
	.loc 2 523 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #8
.LCFI38:
	str	r0, [fp, #-12]
	.loc 2 526 0
	ldr	r3, .L81
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L81+4
	ldr	r3, .L81+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 528 0
	ldr	r0, .L81+12
	ldr	r1, [fp, #-12]
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	str	r0, [fp, #-8]
	.loc 2 530 0
	ldr	r3, .L81
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 532 0
	ldr	r3, [fp, #-8]
	.loc 2 533 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L82:
	.align	2
.L81:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	526
	.word	analog_420ma_sensor_group_list_hdr
.LFE12:
	.size	ANALOG_420MA_SENSOR_get_group_with_this_GID, .-ANALOG_420MA_SENSOR_get_group_with_this_GID
	.section	.text.ANALOG_420MA_SENSOR_get_list_count,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_get_list_count
	.type	ANALOG_420MA_SENSOR_get_list_count, %function
ANALOG_420MA_SENSOR_get_list_count:
.LFB13:
	.loc 2 559 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI39:
	add	fp, sp, #4
.LCFI40:
	sub	sp, sp, #4
.LCFI41:
	.loc 2 562 0
	ldr	r3, .L84
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L84+4
	ldr	r3, .L84+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 564 0
	ldr	r3, .L84+12
	ldr	r3, [r3, #8]
	str	r3, [fp, #-8]
	.loc 2 566 0
	ldr	r3, .L84
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 568 0
	ldr	r3, [fp, #-8]
	.loc 2 569 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L85:
	.align	2
.L84:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	562
	.word	analog_420ma_sensor_group_list_hdr
.LFE13:
	.size	ANALOG_420MA_SENSOR_get_list_count, .-ANALOG_420MA_SENSOR_get_list_count
	.section .rodata
	.align	2
.LC6:
	.ascii	"420ma Report: Sensor not on list!\000"
	.section	.text.ANALOG_420MA_SENSOR_fill_out_recorder_record,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_fill_out_recorder_record
	.type	ANALOG_420MA_SENSOR_fill_out_recorder_record, %function
ANALOG_420MA_SENSOR_fill_out_recorder_record:
.LFB14:
	.loc 2 573 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI42:
	add	fp, sp, #4
.LCFI43:
	sub	sp, sp, #12
.LCFI44:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 2 581 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 2 586 0
	ldr	r3, .L89
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L89+4
	ldr	r3, .L89+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 589 0
	ldr	r3, .L89+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L89+4
	ldr	r3, .L89+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 593 0
	ldr	r0, .L89+20
	ldr	r1, [fp, #-12]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L87
	.loc 2 596 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #108]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	ldr	r3, [fp, #-16]
	fsts	s15, [r3, #8]
	.loc 2 598 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L88
.L87:
	.loc 2 602 0
	ldr	r0, .L89+24
	bl	Alert_Message
.L88:
	.loc 2 607 0
	ldr	r3, .L89+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 609 0
	ldr	r3, .L89
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 613 0
	ldr	r3, [fp, #-8]
	.loc 2 614 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L90:
	.align	2
.L89:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	586
	.word	analog_420ma_sensor_recorder_recursive_MUTEX
	.word	589
	.word	analog_420ma_sensor_group_list_hdr
	.word	.LC6
.LFE14:
	.size	ANALOG_420MA_SENSOR_fill_out_recorder_record, .-ANALOG_420MA_SENSOR_fill_out_recorder_record
	.section	.text.ANALOG_420MA_SENSOR_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_get_change_bits_ptr
	.type	ANALOG_420MA_SENSOR_get_change_bits_ptr, %function
ANALOG_420MA_SENSOR_get_change_bits_ptr:
.LFB15:
	.loc 2 618 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI45:
	add	fp, sp, #4
.LCFI46:
	sub	sp, sp, #8
.LCFI47:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 2 619 0
	ldr	r3, [fp, #-8]
	add	r1, r3, #72
	ldr	r3, [fp, #-8]
	add	r2, r3, #76
	ldr	r3, [fp, #-8]
	add	r3, r3, #80
	ldr	r0, [fp, #-12]
	bl	SHARED_get_32_bit_change_bits_ptr
	mov	r3, r0
	.loc 2 620 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE15:
	.size	ANALOG_420MA_SENSOR_get_change_bits_ptr, .-ANALOG_420MA_SENSOR_get_change_bits_ptr
	.section	.text.ANALOG_420MA_SENSOR_clean_house_processing,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_clean_house_processing
	.type	ANALOG_420MA_SENSOR_clean_house_processing, %function
ANALOG_420MA_SENSOR_clean_house_processing:
.LFB16:
	.loc 2 624 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI48:
	add	fp, sp, #4
.LCFI49:
	sub	sp, sp, #16
.LCFI50:
	.loc 2 637 0
	ldr	r3, .L96
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L96+4
	ldr	r3, .L96+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 639 0
	ldr	r0, .L96+12
	ldr	r1, .L96+4
	ldr	r2, .L96+16
	bl	nm_ListRemoveHead_debug
	str	r0, [fp, #-8]
	.loc 2 641 0
	b	.L93
.L94:
	.loc 2 643 0
	ldr	r0, [fp, #-8]
	ldr	r1, .L96+4
	ldr	r2, .L96+20
	bl	mem_free_debug
	.loc 2 645 0
	ldr	r0, .L96+12
	ldr	r1, .L96+4
	ldr	r2, .L96+24
	bl	nm_ListRemoveHead_debug
	str	r0, [fp, #-8]
.L93:
	.loc 2 641 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L94
	.loc 2 648 0
	ldr	r3, .L96
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 654 0
	ldr	r3, .L96
	ldr	r3, [r3, #0]
	mov	r2, #120
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	mov	r3, #21
	str	r3, [sp, #8]
	mov	r0, #1
	ldr	r1, .L96+28
	mov	r2, #1
	ldr	r3, .L96+12
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	.loc 2 660 0
	ldr	r3, .L96+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L96+4
	mov	r3, #660
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 662 0
	ldr	r3, .L96+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L95
	.loc 2 664 0
	ldr	r3, .L96+36
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, .L96+4
	mov	r2, #664
	bl	mem_free_debug
.L95:
	.loc 2 667 0
	ldr	r0, .L96+36
	mov	r1, #0
	mov	r2, #28
	bl	memset
	.loc 2 669 0
	ldr	r3, .L96+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 670 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L97:
	.align	2
.L96:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	637
	.word	analog_420ma_sensor_group_list_hdr
	.word	639
	.word	643
	.word	645
	.word	ANALOG_420MA_SENSOR_FILENAME
	.word	analog_420ma_sensor_recorder_recursive_MUTEX
	.word	a420_srcs
.LFE16:
	.size	ANALOG_420MA_SENSOR_clean_house_processing, .-ANALOG_420MA_SENSOR_clean_house_processing
	.section	.text.ANALOG_420MA_SENSOR_set_bits_on_420mA_sensors_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_set_bits_on_420mA_sensors_to_cause_distribution_in_the_next_token
	.type	ANALOG_420MA_SENSOR_set_bits_on_420mA_sensors_to_cause_distribution_in_the_next_token, %function
ANALOG_420MA_SENSOR_set_bits_on_420mA_sensors_to_cause_distribution_in_the_next_token:
.LFB17:
	.loc 2 674 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI51:
	add	fp, sp, #4
.LCFI52:
	sub	sp, sp, #4
.LCFI53:
	.loc 2 679 0
	ldr	r3, .L101
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L101+4
	ldr	r3, .L101+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 681 0
	ldr	r0, .L101+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 683 0
	b	.L99
.L100:
	.loc 2 688 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #76
	ldr	r3, .L101
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_set_all_32_bit_change_bits
	.loc 2 690 0
	ldr	r0, .L101+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L99:
	.loc 2 683 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L100
	.loc 2 693 0
	ldr	r3, .L101
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 701 0
	ldr	r3, .L101+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L101+4
	ldr	r3, .L101+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 703 0
	ldr	r3, .L101+24
	mov	r2, #1
	str	r2, [r3, #444]
	.loc 2 705 0
	ldr	r3, .L101+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 706 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L102:
	.align	2
.L101:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	679
	.word	analog_420ma_sensor_group_list_hdr
	.word	comm_mngr_recursive_MUTEX
	.word	701
	.word	comm_mngr
.LFE17:
	.size	ANALOG_420MA_SENSOR_set_bits_on_420mA_sensors_to_cause_distribution_in_the_next_token, .-ANALOG_420MA_SENSOR_set_bits_on_420mA_sensors_to_cause_distribution_in_the_next_token
	.section	.text.ANALOG_420MA_SENSOR_on_all_sensors_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_on_all_sensors_set_or_clear_commserver_change_bits
	.type	ANALOG_420MA_SENSOR_on_all_sensors_set_or_clear_commserver_change_bits, %function
ANALOG_420MA_SENSOR_on_all_sensors_set_or_clear_commserver_change_bits:
.LFB18:
	.loc 2 710 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI54:
	add	fp, sp, #4
.LCFI55:
	sub	sp, sp, #8
.LCFI56:
	str	r0, [fp, #-12]
	.loc 2 713 0
	ldr	r3, .L108
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L108+4
	ldr	r3, .L108+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 715 0
	ldr	r0, .L108+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 717 0
	b	.L104
.L107:
	.loc 2 719 0
	ldr	r3, [fp, #-12]
	cmp	r3, #51
	bne	.L105
	.loc 2 721 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #80
	ldr	r3, .L108
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L106
.L105:
	.loc 2 725 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #80
	ldr	r3, .L108
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_set_all_32_bit_change_bits
.L106:
	.loc 2 728 0
	ldr	r0, .L108+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L104:
	.loc 2 717 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L107
	.loc 2 731 0
	ldr	r3, .L108
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 732 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L109:
	.align	2
.L108:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	713
	.word	analog_420ma_sensor_group_list_hdr
.LFE18:
	.size	ANALOG_420MA_SENSOR_on_all_sensors_set_or_clear_commserver_change_bits, .-ANALOG_420MA_SENSOR_on_all_sensors_set_or_clear_commserver_change_bits
	.section	.text.nm_ANALOG_420MA_SENSOR_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_ANALOG_420MA_SENSOR_update_pending_change_bits
	.type	nm_ANALOG_420MA_SENSOR_update_pending_change_bits, %function
nm_ANALOG_420MA_SENSOR_update_pending_change_bits:
.LFB19:
	.loc 2 743 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI57:
	add	fp, sp, #4
.LCFI58:
	sub	sp, sp, #16
.LCFI59:
	str	r0, [fp, #-16]
	.loc 2 748 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 2 750 0
	ldr	r3, .L117
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L117+4
	ldr	r3, .L117+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 752 0
	ldr	r0, .L117+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 754 0
	b	.L111
.L115:
	.loc 2 758 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #84]
	cmp	r3, #0
	beq	.L112
	.loc 2 763 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L113
	.loc 2 765 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #80]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #84]
	orr	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #80]
	.loc 2 769 0
	ldr	r3, .L117+16
	mov	r2, #1
	str	r2, [r3, #112]
	.loc 2 777 0
	ldr	r3, .L117+20
	ldr	r3, [r3, #152]
	mvn	r2, #0
	str	r2, [sp, #0]
	mov	r0, r3
	mov	r1, #2
	ldr	r2, .L117+24
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L114
.L113:
	.loc 2 781 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #84
	ldr	r3, .L117+28
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
.L114:
	.loc 2 785 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L112:
	.loc 2 788 0
	ldr	r0, .L117+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L111:
	.loc 2 754 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L115
	.loc 2 791 0
	ldr	r3, .L117
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 793 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L110
	.loc 2 799 0
	mov	r0, #21
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L110:
	.loc 2 801 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L118:
	.align	2
.L117:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	750
	.word	analog_420ma_sensor_group_list_hdr
	.word	weather_preserves
	.word	cics
	.word	60000
	.word	list_program_data_recursive_MUTEX
.LFE19:
	.size	nm_ANALOG_420MA_SENSOR_update_pending_change_bits, .-nm_ANALOG_420MA_SENSOR_update_pending_change_bits
	.section	.text.nm_ANALOG_420MA_SENSOR_set_new_reading_slave_side_variables,"ax",%progbits
	.align	2
	.global	nm_ANALOG_420MA_SENSOR_set_new_reading_slave_side_variables
	.type	nm_ANALOG_420MA_SENSOR_set_new_reading_slave_side_variables, %function
nm_ANALOG_420MA_SENSOR_set_new_reading_slave_side_variables:
.LFB20:
	.loc 2 805 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI60:
	add	fp, sp, #4
.LCFI61:
	sub	sp, sp, #8
.LCFI62:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 2 806 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L119
	.loc 2 806 0 is_stmt 0 discriminator 1
	ldr	r0, .L121
	ldr	r1, [fp, #-12]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L119
	.loc 2 808 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #100]
	.loc 2 810 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #104]
.L119:
	.loc 2 812 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L122:
	.align	2
.L121:
	.word	analog_420ma_sensor_group_list_hdr
.LFE20:
	.size	nm_ANALOG_420MA_SENSOR_set_new_reading_slave_side_variables, .-nm_ANALOG_420MA_SENSOR_set_new_reading_slave_side_variables
	.section	.text.ANALOG_420MA_SENSOR_find_a_sensor_reading_to_send_to_the_master,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_find_a_sensor_reading_to_send_to_the_master
	.type	ANALOG_420MA_SENSOR_find_a_sensor_reading_to_send_to_the_master, %function
ANALOG_420MA_SENSOR_find_a_sensor_reading_to_send_to_the_master:
.LFB21:
	.loc 2 816 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI63:
	add	fp, sp, #4
.LCFI64:
	sub	sp, sp, #16
.LCFI65:
	str	r0, [fp, #-20]
	.loc 2 828 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 2 830 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 2 834 0
	ldr	r3, .L127
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L127+4
	ldr	r3, .L127+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 836 0
	ldr	r0, .L127+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 838 0
	b	.L124
.L126:
	.loc 2 840 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #104]
	cmp	r3, #0
	beq	.L125
	.loc 2 847 0
	sub	r3, fp, #16
	mov	r0, #4
	mov	r1, r3
	ldr	r2, .L127+4
	ldr	r3, .L127+16
	bl	mem_oabia
	mov	r3, r0
	cmp	r3, #0
	beq	.L125
	.loc 2 849 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-12]
	.loc 2 853 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-8]
	add	r3, r3, #100
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	memcpy
	.loc 2 855 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	add	r2, r3, #4
	ldr	r3, [fp, #-20]
	str	r2, [r3, #0]
	.loc 2 860 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #104]
.L125:
	.loc 2 874 0
	ldr	r0, .L127+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L124:
	.loc 2 838 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L126
	.loc 2 877 0
	ldr	r3, .L127
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 881 0
	ldr	r3, [fp, #-12]
	.loc 2 882 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L128:
	.align	2
.L127:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	834
	.word	analog_420ma_sensor_group_list_hdr
	.word	847
.LFE21:
	.size	ANALOG_420MA_SENSOR_find_a_sensor_reading_to_send_to_the_master, .-ANALOG_420MA_SENSOR_find_a_sensor_reading_to_send_to_the_master
	.section .rodata
	.align	2
.LC7:
	.ascii	" Reading received from the token = %d\000"
	.align	2
.LC8:
	.ascii	"4-20mA sensor: list item not found!\000"
	.section	.text.ANALOG_420MA_SENSOR_extract_analog_420ma_reading_from_token_response,"ax",%progbits
	.align	2
	.global	ANALOG_420MA_SENSOR_extract_analog_420ma_reading_from_token_response
	.type	ANALOG_420MA_SENSOR_extract_analog_420ma_reading_from_token_response, %function
ANALOG_420MA_SENSOR_extract_analog_420ma_reading_from_token_response:
.LFB22:
	.loc 2 902 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI66:
	add	fp, sp, #4
.LCFI67:
	sub	sp, sp, #20
.LCFI68:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 2 923 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 2 927 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	sub	r2, fp, #16
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	memcpy
	.loc 2 929 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	add	r2, r3, #4
	ldr	r3, [fp, #-20]
	str	r2, [r3, #0]
	.loc 2 943 0
	ldr	r3, .L132
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L132+4
	ldr	r3, .L132+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 945 0
	ldr	r0, [fp, #-24]
	bl	ANALOG_420MA_SENSOR_get_pointer_to_sensor_with_this_box_index
	str	r0, [fp, #-12]
	.loc 2 947 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L130
	.loc 2 958 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #108]
	.loc 2 964 0
	ldr	r3, .L132+12
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L132+12
	str	r2, [r3, #0]
	.loc 2 969 0
	ldr	r3, .L132+12
	ldr	r1, [r3, #0]
	ldr	r3, .L132+16
	umull	r2, r3, r1, r3
	mov	r2, r3, lsr #3
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	cmp	r2, #0
	bne	.L131
	.loc 2 973 0
	ldr	r0, [fp, #-12]
	bl	ANALOG_420MA_SENSOR_RECORDER_add_a_record
	.loc 2 978 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #108]
	ldr	r0, .L132+20
	mov	r1, r3
	bl	Alert_Message_va
	.loc 2 982 0
	ldr	r3, .L132+12
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L131
.L130:
	.loc 2 988 0
	ldr	r0, .L132+24
	bl	Alert_Message
	.loc 2 991 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L131:
	.loc 2 994 0
	ldr	r3, .L132
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 998 0
	ldr	r3, [fp, #-8]
	.loc 2 999 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L133:
	.align	2
.L132:
	.word	analog_420ma_sensor_items_recursive_MUTEX
	.word	.LC5
	.word	943
	.word	record_count.9180
	.word	-858993459
	.word	.LC7
	.word	.LC8
.LFE22:
	.size	ANALOG_420MA_SENSOR_extract_analog_420ma_reading_from_token_response, .-ANALOG_420MA_SENSOR_extract_analog_420ma_reading_from_token_response
	.section	.bss.record_count.9180,"aw",%nobits
	.align	2
	.type	record_count.9180, %object
	.size	record_count.9180, 4
record_count.9180:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI45-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI48-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI51-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI52-.LCFI51
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI54-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI55-.LCFI54
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI57-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI58-.LCFI57
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI60-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI61-.LCFI60
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI63-.LFB21
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI64-.LCFI63
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI66-.LFB22
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI67-.LCFI66
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE44:
	.text
.Letext0:
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_comm_server_common.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/flow_recorder.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/report_data.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/analog_420ma_sensor_recorder.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/analog_420ma_sensors.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/comm_mngr.h"
	.file 19 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/controller_initiated.h"
	.file 20 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 21 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 22 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x17de
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF296
	.byte	0x1
	.4byte	.LASF297
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF8
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x3
	.byte	0x3a
	.4byte	0x4f
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x3
	.byte	0x4c
	.4byte	0x56
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x3
	.byte	0x5e
	.4byte	0x8c
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x3
	.byte	0x99
	.4byte	0x8c
	.uleb128 0x5
	.byte	0x4
	.byte	0x4
	.byte	0x4c
	.4byte	0xc3
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x4
	.byte	0x55
	.4byte	0x76
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x4
	.byte	0x57
	.4byte	0x76
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x4
	.byte	0x59
	.4byte	0x9e
	.uleb128 0x5
	.byte	0x4
	.byte	0x4
	.byte	0x65
	.4byte	0xf3
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x4
	.byte	0x67
	.4byte	0x76
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x4
	.byte	0x69
	.4byte	0x76
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x4
	.byte	0x6b
	.4byte	0xce
	.uleb128 0x5
	.byte	0x8
	.byte	0x5
	.byte	0x14
	.4byte	0x123
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x5
	.byte	0x17
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF20
	.byte	0x5
	.byte	0x1a
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x6b
	.uleb128 0x4
	.4byte	.LASF21
	.byte	0x5
	.byte	0x1c
	.4byte	0xfe
	.uleb128 0x5
	.byte	0x6
	.byte	0x6
	.byte	0x22
	.4byte	0x155
	.uleb128 0x8
	.ascii	"T\000"
	.byte	0x6
	.byte	0x24
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.ascii	"D\000"
	.byte	0x6
	.byte	0x26
	.4byte	0x76
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF22
	.byte	0x6
	.byte	0x28
	.4byte	0x134
	.uleb128 0x5
	.byte	0x14
	.byte	0x7
	.byte	0x18
	.4byte	0x1af
	.uleb128 0x6
	.4byte	.LASF23
	.byte	0x7
	.byte	0x1a
	.4byte	0x1af
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF24
	.byte	0x7
	.byte	0x1c
	.4byte	0x1af
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x7
	.byte	0x1e
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x7
	.byte	0x20
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x7
	.byte	0x23
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x9
	.byte	0x4
	.uleb128 0x4
	.4byte	.LASF28
	.byte	0x7
	.byte	0x26
	.4byte	0x160
	.uleb128 0x5
	.byte	0xc
	.byte	0x7
	.byte	0x2a
	.4byte	0x1ef
	.uleb128 0x6
	.4byte	.LASF29
	.byte	0x7
	.byte	0x2c
	.4byte	0x1af
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF30
	.byte	0x7
	.byte	0x2e
	.4byte	0x1af
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF31
	.byte	0x7
	.byte	0x30
	.4byte	0x1ef
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x1b1
	.uleb128 0x4
	.4byte	.LASF32
	.byte	0x7
	.byte	0x32
	.4byte	0x1bc
	.uleb128 0x4
	.4byte	.LASF33
	.byte	0x8
	.byte	0x35
	.4byte	0x25
	.uleb128 0x4
	.4byte	.LASF34
	.byte	0x9
	.byte	0x57
	.4byte	0x1af
	.uleb128 0x4
	.4byte	.LASF35
	.byte	0xa
	.byte	0x4c
	.4byte	0x20b
	.uleb128 0x4
	.4byte	.LASF36
	.byte	0xb
	.byte	0x65
	.4byte	0x1af
	.uleb128 0xa
	.4byte	0x4f
	.4byte	0x23c
	.uleb128 0xb
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.4byte	0x81
	.4byte	0x24c
	.uleb128 0xb
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xa
	.4byte	0x64
	.4byte	0x25c
	.uleb128 0xb
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0xa
	.4byte	0x64
	.4byte	0x26c
	.uleb128 0xb
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x64
	.uleb128 0x5
	.byte	0x48
	.byte	0xc
	.byte	0x3a
	.4byte	0x2c1
	.uleb128 0x6
	.4byte	.LASF37
	.byte	0xc
	.byte	0x3e
	.4byte	0x1f5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF38
	.byte	0xc
	.byte	0x46
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF39
	.byte	0xc
	.byte	0x4d
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF40
	.byte	0xc
	.byte	0x50
	.4byte	0x24c
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF41
	.byte	0xc
	.byte	0x5a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.byte	0
	.uleb128 0x4
	.4byte	.LASF42
	.byte	0xc
	.byte	0x5c
	.4byte	0x272
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF43
	.uleb128 0xa
	.4byte	0x81
	.4byte	0x2e3
	.uleb128 0xb
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0xa
	.4byte	0x81
	.4byte	0x2f3
	.uleb128 0xb
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x5
	.byte	0x1c
	.byte	0xd
	.byte	0x8f
	.4byte	0x35e
	.uleb128 0x6
	.4byte	.LASF44
	.byte	0xd
	.byte	0x94
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF45
	.byte	0xd
	.byte	0x99
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF46
	.byte	0xd
	.byte	0x9e
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF47
	.byte	0xd
	.byte	0xa3
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF48
	.byte	0xd
	.byte	0xad
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF49
	.byte	0xd
	.byte	0xb8
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF50
	.byte	0xd
	.byte	0xbe
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x4
	.4byte	.LASF51
	.byte	0xd
	.byte	0xc2
	.4byte	0x2f3
	.uleb128 0x5
	.byte	0x14
	.byte	0xe
	.byte	0xb5
	.4byte	0x3b7
	.uleb128 0x8
	.ascii	"dt\000"
	.byte	0xe
	.byte	0xbc
	.4byte	0x155
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF52
	.byte	0xe
	.byte	0xbe
	.4byte	0x2cc
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF53
	.byte	0xe
	.byte	0xc1
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF54
	.byte	0xe
	.byte	0xc3
	.4byte	0x6b
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF55
	.byte	0xe
	.byte	0xc8
	.4byte	0x6b
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.byte	0
	.uleb128 0x4
	.4byte	.LASF56
	.byte	0xe
	.byte	0xd0
	.4byte	0x369
	.uleb128 0xc
	.byte	0x1c
	.byte	0xf
	.2byte	0x10c
	.4byte	0x435
	.uleb128 0xd
	.ascii	"rip\000"
	.byte	0xf
	.2byte	0x112
	.4byte	0xf3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF57
	.byte	0xf
	.2byte	0x11b
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF58
	.byte	0xf
	.2byte	0x122
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF59
	.byte	0xf
	.2byte	0x127
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF60
	.byte	0xf
	.2byte	0x138
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF61
	.byte	0xf
	.2byte	0x144
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.4byte	.LASF62
	.byte	0xf
	.2byte	0x14b
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0xf
	.4byte	.LASF63
	.byte	0xf
	.2byte	0x14d
	.4byte	0x3c2
	.uleb128 0xc
	.byte	0xec
	.byte	0xf
	.2byte	0x150
	.4byte	0x615
	.uleb128 0xe
	.4byte	.LASF64
	.byte	0xf
	.2byte	0x157
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF65
	.byte	0xf
	.2byte	0x162
	.4byte	0x76
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF66
	.byte	0xf
	.2byte	0x164
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.4byte	.LASF67
	.byte	0xf
	.2byte	0x166
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.4byte	.LASF68
	.byte	0xf
	.2byte	0x168
	.4byte	0x155
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xe
	.4byte	.LASF69
	.byte	0xf
	.2byte	0x16e
	.4byte	0xc3
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xe
	.4byte	.LASF70
	.byte	0xf
	.2byte	0x174
	.4byte	0x435
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xe
	.4byte	.LASF71
	.byte	0xf
	.2byte	0x17b
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xe
	.4byte	.LASF72
	.byte	0xf
	.2byte	0x17d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xe
	.4byte	.LASF73
	.byte	0xf
	.2byte	0x185
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0xe
	.4byte	.LASF74
	.byte	0xf
	.2byte	0x18d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xe
	.4byte	.LASF75
	.byte	0xf
	.2byte	0x191
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xe
	.4byte	.LASF76
	.byte	0xf
	.2byte	0x195
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xe
	.4byte	.LASF77
	.byte	0xf
	.2byte	0x199
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0xe
	.4byte	.LASF78
	.byte	0xf
	.2byte	0x19e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0xe
	.4byte	.LASF79
	.byte	0xf
	.2byte	0x1a2
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0xe
	.4byte	.LASF80
	.byte	0xf
	.2byte	0x1a6
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0xe
	.4byte	.LASF81
	.byte	0xf
	.2byte	0x1b4
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0xe
	.4byte	.LASF82
	.byte	0xf
	.2byte	0x1ba
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0xe
	.4byte	.LASF83
	.byte	0xf
	.2byte	0x1c2
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0xe
	.4byte	.LASF84
	.byte	0xf
	.2byte	0x1c4
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0xe
	.4byte	.LASF85
	.byte	0xf
	.2byte	0x1c6
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0xe
	.4byte	.LASF86
	.byte	0xf
	.2byte	0x1d5
	.4byte	0x6b
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0xe
	.4byte	.LASF87
	.byte	0xf
	.2byte	0x1d7
	.4byte	0x6b
	.byte	0x3
	.byte	0x23
	.uleb128 0x81
	.uleb128 0xe
	.4byte	.LASF88
	.byte	0xf
	.2byte	0x1dd
	.4byte	0x6b
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0xe
	.4byte	.LASF89
	.byte	0xf
	.2byte	0x1e7
	.4byte	0x6b
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0xe
	.4byte	.LASF90
	.byte	0xf
	.2byte	0x1f0
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0xe
	.4byte	.LASF91
	.byte	0xf
	.2byte	0x1f7
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0xe
	.4byte	.LASF92
	.byte	0xf
	.2byte	0x1f9
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0xe
	.4byte	.LASF93
	.byte	0xf
	.2byte	0x1fd
	.4byte	0x615
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0xa
	.4byte	0x81
	.4byte	0x625
	.uleb128 0xb
	.4byte	0x25
	.byte	0x16
	.byte	0
	.uleb128 0xf
	.4byte	.LASF94
	.byte	0xf
	.2byte	0x204
	.4byte	0x441
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF95
	.uleb128 0x5
	.byte	0x1c
	.byte	0x10
	.byte	0x34
	.4byte	0x6a3
	.uleb128 0x6
	.4byte	.LASF44
	.byte	0x10
	.byte	0x39
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF45
	.byte	0x10
	.byte	0x3e
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF46
	.byte	0x10
	.byte	0x43
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF47
	.byte	0x10
	.byte	0x48
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF48
	.byte	0x10
	.byte	0x52
	.4byte	0x123
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF49
	.byte	0x10
	.byte	0x5d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF50
	.byte	0x10
	.byte	0x63
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x4
	.4byte	.LASF96
	.byte	0x10
	.byte	0x67
	.4byte	0x638
	.uleb128 0x4
	.4byte	.LASF97
	.byte	0x11
	.byte	0x2c
	.4byte	0x6b9
	.uleb128 0x10
	.4byte	.LASF97
	.byte	0x78
	.byte	0x1
	.byte	0x3d
	.4byte	0x77c
	.uleb128 0x6
	.4byte	.LASF98
	.byte	0x1
	.byte	0x3f
	.4byte	0x2c1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF99
	.byte	0x1
	.byte	0x43
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x6
	.4byte	.LASF100
	.byte	0x1
	.byte	0x44
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x6
	.4byte	.LASF101
	.byte	0x1
	.byte	0x45
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x6
	.4byte	.LASF102
	.byte	0x1
	.byte	0x46
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x6
	.4byte	.LASF103
	.byte	0x1
	.byte	0x4d
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x6
	.4byte	.LASF53
	.byte	0x1
	.byte	0x50
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x6
	.4byte	.LASF104
	.byte	0x1
	.byte	0x59
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x6
	.4byte	.LASF105
	.byte	0x1
	.byte	0x67
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x6
	.4byte	.LASF106
	.byte	0x1
	.byte	0x6a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x6
	.4byte	.LASF107
	.byte	0x1
	.byte	0x6e
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x6
	.4byte	.LASF108
	.byte	0x1
	.byte	0x77
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x6
	.4byte	.LASF109
	.byte	0x1
	.byte	0x7e
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.byte	0
	.uleb128 0x5
	.byte	0x8
	.byte	0x12
	.byte	0xe5
	.4byte	0x7a1
	.uleb128 0x6
	.4byte	.LASF110
	.byte	0x12
	.byte	0xf4
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF111
	.byte	0x12
	.byte	0xfc
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF112
	.byte	0x12
	.byte	0xfe
	.4byte	0x77c
	.uleb128 0xc
	.byte	0xc
	.byte	0x12
	.2byte	0x103
	.4byte	0x7d3
	.uleb128 0xd
	.ascii	"dt\000"
	.byte	0x12
	.2byte	0x105
	.4byte	0x155
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF113
	.byte	0x12
	.2byte	0x106
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xf
	.4byte	.LASF114
	.byte	0x12
	.2byte	0x107
	.4byte	0x7ac
	.uleb128 0x11
	.2byte	0x1e4
	.byte	0x12
	.2byte	0x10b
	.4byte	0xa9d
	.uleb128 0xe
	.4byte	.LASF115
	.byte	0x12
	.2byte	0x110
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF116
	.byte	0x12
	.2byte	0x114
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF117
	.byte	0x12
	.2byte	0x11d
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF118
	.byte	0x12
	.2byte	0x124
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF119
	.byte	0x12
	.2byte	0x128
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF120
	.byte	0x12
	.2byte	0x12c
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.4byte	.LASF121
	.byte	0x12
	.2byte	0x131
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.4byte	.LASF122
	.byte	0x12
	.2byte	0x136
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xe
	.4byte	.LASF123
	.byte	0x12
	.2byte	0x13a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xe
	.4byte	.LASF124
	.byte	0x12
	.2byte	0x141
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xe
	.4byte	.LASF125
	.byte	0x12
	.2byte	0x14a
	.4byte	0xa9d
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xe
	.4byte	.LASF126
	.byte	0x12
	.2byte	0x154
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xe
	.4byte	.LASF127
	.byte	0x12
	.2byte	0x156
	.4byte	0x2d3
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0xe
	.4byte	.LASF128
	.byte	0x12
	.2byte	0x158
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0xe
	.4byte	.LASF129
	.byte	0x12
	.2byte	0x15a
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0xe
	.4byte	.LASF130
	.byte	0x12
	.2byte	0x172
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0xe
	.4byte	.LASF131
	.byte	0x12
	.2byte	0x174
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0xe
	.4byte	.LASF132
	.byte	0x12
	.2byte	0x17e
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0xe
	.4byte	.LASF133
	.byte	0x12
	.2byte	0x180
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0xe
	.4byte	.LASF134
	.byte	0x12
	.2byte	0x184
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0xe
	.4byte	.LASF135
	.byte	0x12
	.2byte	0x193
	.4byte	0x2d3
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0xe
	.4byte	.LASF136
	.byte	0x12
	.2byte	0x195
	.4byte	0x2d3
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0xe
	.4byte	.LASF137
	.byte	0x12
	.2byte	0x199
	.4byte	0xa9d
	.byte	0x3
	.byte	0x23
	.uleb128 0x108
	.uleb128 0xe
	.4byte	.LASF138
	.byte	0x12
	.2byte	0x19b
	.4byte	0xa9d
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.uleb128 0xe
	.4byte	.LASF139
	.byte	0x12
	.2byte	0x1a0
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x168
	.uleb128 0xe
	.4byte	.LASF140
	.byte	0x12
	.2byte	0x1a7
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x16c
	.uleb128 0xe
	.4byte	.LASF141
	.byte	0x12
	.2byte	0x1a9
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x170
	.uleb128 0xe
	.4byte	.LASF142
	.byte	0x12
	.2byte	0x1ab
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x174
	.uleb128 0xe
	.4byte	.LASF143
	.byte	0x12
	.2byte	0x1ad
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x178
	.uleb128 0xe
	.4byte	.LASF144
	.byte	0x12
	.2byte	0x1b3
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x17c
	.uleb128 0xe
	.4byte	.LASF145
	.byte	0x12
	.2byte	0x1b5
	.4byte	0x221
	.byte	0x3
	.byte	0x23
	.uleb128 0x180
	.uleb128 0xe
	.4byte	.LASF146
	.byte	0x12
	.2byte	0x1bc
	.4byte	0x221
	.byte	0x3
	.byte	0x23
	.uleb128 0x184
	.uleb128 0xe
	.4byte	.LASF147
	.byte	0x12
	.2byte	0x1be
	.4byte	0x221
	.byte	0x3
	.byte	0x23
	.uleb128 0x188
	.uleb128 0xe
	.4byte	.LASF148
	.byte	0x12
	.2byte	0x1c2
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x18c
	.uleb128 0xe
	.4byte	.LASF149
	.byte	0x12
	.2byte	0x1c4
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x190
	.uleb128 0xe
	.4byte	.LASF150
	.byte	0x12
	.2byte	0x1ca
	.4byte	0x1b1
	.byte	0x3
	.byte	0x23
	.uleb128 0x194
	.uleb128 0xe
	.4byte	.LASF151
	.byte	0x12
	.2byte	0x1ce
	.4byte	0x1b1
	.byte	0x3
	.byte	0x23
	.uleb128 0x1a8
	.uleb128 0xe
	.4byte	.LASF152
	.byte	0x12
	.2byte	0x1d4
	.4byte	0x7a1
	.byte	0x3
	.byte	0x23
	.uleb128 0x1bc
	.uleb128 0xe
	.4byte	.LASF153
	.byte	0x12
	.2byte	0x1da
	.4byte	0x221
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c4
	.uleb128 0xe
	.4byte	.LASF154
	.byte	0x12
	.2byte	0x1e0
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c8
	.uleb128 0xe
	.4byte	.LASF155
	.byte	0x12
	.2byte	0x1e3
	.4byte	0x7d3
	.byte	0x3
	.byte	0x23
	.uleb128 0x1cc
	.uleb128 0xe
	.4byte	.LASF156
	.byte	0x12
	.2byte	0x1e9
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d8
	.uleb128 0xe
	.4byte	.LASF157
	.byte	0x12
	.2byte	0x1f0
	.4byte	0x221
	.byte	0x3
	.byte	0x23
	.uleb128 0x1dc
	.uleb128 0xe
	.4byte	.LASF158
	.byte	0x12
	.2byte	0x1f2
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x1e0
	.byte	0
	.uleb128 0xa
	.4byte	0x93
	.4byte	0xaad
	.uleb128 0xb
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0xf
	.4byte	.LASF159
	.byte	0x12
	.2byte	0x1f4
	.4byte	0x7df
	.uleb128 0xc
	.byte	0x18
	.byte	0x13
	.2byte	0x154
	.4byte	0xb0e
	.uleb128 0xe
	.4byte	.LASF160
	.byte	0x13
	.2byte	0x159
	.4byte	0x129
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF161
	.byte	0x13
	.2byte	0x160
	.4byte	0xb0e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF162
	.byte	0x13
	.2byte	0x162
	.4byte	0xb14
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF163
	.byte	0x13
	.2byte	0x164
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF164
	.byte	0x13
	.2byte	0x166
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x93
	.uleb128 0x7
	.byte	0x4
	.4byte	0x35e
	.uleb128 0xf
	.4byte	.LASF165
	.byte	0x13
	.2byte	0x168
	.4byte	0xab9
	.uleb128 0xc
	.byte	0xbc
	.byte	0x13
	.2byte	0x16c
	.4byte	0xdb5
	.uleb128 0xe
	.4byte	.LASF115
	.byte	0x13
	.2byte	0x16e
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF116
	.byte	0x13
	.2byte	0x170
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF166
	.byte	0x13
	.2byte	0x175
	.4byte	0xb1a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF167
	.byte	0x13
	.2byte	0x17c
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xe
	.4byte	.LASF168
	.byte	0x13
	.2byte	0x182
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xe
	.4byte	.LASF169
	.byte	0x13
	.2byte	0x187
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xe
	.4byte	.LASF170
	.byte	0x13
	.2byte	0x18d
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xe
	.4byte	.LASF171
	.byte	0x13
	.2byte	0x193
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xe
	.4byte	.LASF172
	.byte	0x13
	.2byte	0x195
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xe
	.4byte	.LASF173
	.byte	0x13
	.2byte	0x19a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0xe
	.4byte	.LASF174
	.byte	0x13
	.2byte	0x1a0
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xe
	.4byte	.LASF175
	.byte	0x13
	.2byte	0x1a9
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xe
	.4byte	.LASF176
	.byte	0x13
	.2byte	0x1b1
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xe
	.4byte	.LASF177
	.byte	0x13
	.2byte	0x1bb
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xe
	.4byte	.LASF178
	.byte	0x13
	.2byte	0x1c1
	.4byte	0xb14
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0xe
	.4byte	.LASF179
	.byte	0x13
	.2byte	0x1cb
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xe
	.4byte	.LASF180
	.byte	0x13
	.2byte	0x1d1
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xe
	.4byte	.LASF181
	.byte	0x13
	.2byte	0x1d5
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xe
	.4byte	.LASF182
	.byte	0x13
	.2byte	0x1d9
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0xe
	.4byte	.LASF183
	.byte	0x13
	.2byte	0x1dd
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0xe
	.4byte	.LASF184
	.byte	0x13
	.2byte	0x1e1
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0xe
	.4byte	.LASF185
	.byte	0x13
	.2byte	0x1e5
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0xe
	.4byte	.LASF186
	.byte	0x13
	.2byte	0x1e9
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0xe
	.4byte	.LASF187
	.byte	0x13
	.2byte	0x1ef
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0xe
	.4byte	.LASF188
	.byte	0x13
	.2byte	0x1f1
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0xe
	.4byte	.LASF189
	.byte	0x13
	.2byte	0x1f8
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0xe
	.4byte	.LASF190
	.byte	0x13
	.2byte	0x1fa
	.4byte	0x221
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0xe
	.4byte	.LASF191
	.byte	0x13
	.2byte	0x202
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0xe
	.4byte	.LASF192
	.byte	0x13
	.2byte	0x204
	.4byte	0x221
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0xe
	.4byte	.LASF193
	.byte	0x13
	.2byte	0x206
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0xe
	.4byte	.LASF194
	.byte	0x13
	.2byte	0x20c
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0xe
	.4byte	.LASF195
	.byte	0x13
	.2byte	0x216
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0xe
	.4byte	.LASF196
	.byte	0x13
	.2byte	0x218
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0xe
	.4byte	.LASF197
	.byte	0x13
	.2byte	0x21e
	.4byte	0x221
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0xe
	.4byte	.LASF198
	.byte	0x13
	.2byte	0x225
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0xe
	.4byte	.LASF199
	.byte	0x13
	.2byte	0x227
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0xe
	.4byte	.LASF200
	.byte	0x13
	.2byte	0x22b
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0xe
	.4byte	.LASF201
	.byte	0x13
	.2byte	0x22f
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0xe
	.4byte	.LASF202
	.byte	0x13
	.2byte	0x231
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0xe
	.4byte	.LASF203
	.byte	0x13
	.2byte	0x240
	.4byte	0x20b
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0xe
	.4byte	.LASF204
	.byte	0x13
	.2byte	0x248
	.4byte	0x221
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0xe
	.4byte	.LASF205
	.byte	0x13
	.2byte	0x252
	.4byte	0x221
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.byte	0
	.uleb128 0xf
	.4byte	.LASF206
	.byte	0x13
	.2byte	0x254
	.4byte	0xb26
	.uleb128 0x12
	.4byte	0x81
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF214
	.byte	0x1
	.byte	0x85
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0xe42
	.uleb128 0x14
	.4byte	.LASF207
	.byte	0x1
	.byte	0x85
	.4byte	0xe42
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF208
	.byte	0x1
	.byte	0x86
	.4byte	0x26c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF209
	.byte	0x1
	.byte	0x87
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF210
	.byte	0x1
	.byte	0x88
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF211
	.byte	0x1
	.byte	0x89
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x14
	.4byte	.LASF212
	.byte	0x1
	.byte	0x8a
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x14
	.4byte	.LASF213
	.byte	0x1
	.byte	0x8b
	.4byte	0xe52
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.byte	0
	.uleb128 0x12
	.4byte	0xe47
	.uleb128 0x7
	.byte	0x4
	.4byte	0x6ae
	.uleb128 0x12
	.4byte	0x93
	.uleb128 0x7
	.byte	0x4
	.4byte	0x81
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF215
	.byte	0x1
	.byte	0xa6
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0xed4
	.uleb128 0x14
	.4byte	.LASF207
	.byte	0x1
	.byte	0xa6
	.4byte	0xed4
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF216
	.byte	0x1
	.byte	0xa7
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF209
	.byte	0x1
	.byte	0xa8
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF210
	.byte	0x1
	.byte	0xa9
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF211
	.byte	0x1
	.byte	0xaa
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x14
	.4byte	.LASF212
	.byte	0x1
	.byte	0xab
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x14
	.4byte	.LASF213
	.byte	0x1
	.byte	0xac
	.4byte	0xe52
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.byte	0
	.uleb128 0x12
	.4byte	0x1af
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF217
	.byte	0x1
	.byte	0xcd
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0xf55
	.uleb128 0x14
	.4byte	.LASF207
	.byte	0x1
	.byte	0xcd
	.4byte	0xed4
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF218
	.byte	0x1
	.byte	0xce
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF209
	.byte	0x1
	.byte	0xcf
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF210
	.byte	0x1
	.byte	0xd0
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF211
	.byte	0x1
	.byte	0xd1
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x14
	.4byte	.LASF212
	.byte	0x1
	.byte	0xd2
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x14
	.4byte	.LASF213
	.byte	0x1
	.byte	0xd3
	.4byte	0xe52
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.byte	0
	.uleb128 0x15
	.4byte	.LASF233
	.byte	0x1
	.byte	0xf1
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0xfee
	.uleb128 0x14
	.4byte	.LASF219
	.byte	0x1
	.byte	0xf1
	.4byte	0xe42
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF220
	.byte	0x1
	.byte	0xf2
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF210
	.byte	0x1
	.byte	0xf3
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x14
	.4byte	.LASF211
	.byte	0x1
	.byte	0xf4
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x14
	.4byte	.LASF212
	.byte	0x1
	.byte	0xf5
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x14
	.4byte	.LASF221
	.byte	0x1
	.byte	0xf6
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x14
	.4byte	.LASF222
	.byte	0x1
	.byte	0xf7
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x16
	.4byte	.LASF223
	.byte	0x1
	.2byte	0x104
	.4byte	0xe52
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF224
	.byte	0x1
	.2byte	0x106
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF239
	.byte	0x1
	.2byte	0x15f
	.byte	0x1
	.4byte	0x81
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x10cd
	.uleb128 0x18
	.4byte	.LASF225
	.byte	0x1
	.2byte	0x15f
	.4byte	0x10cd
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x18
	.4byte	.LASF210
	.byte	0x1
	.2byte	0x160
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x18
	.4byte	.LASF212
	.byte	0x1
	.2byte	0x161
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x18
	.4byte	.LASF221
	.byte	0x1
	.2byte	0x162
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x16
	.4byte	.LASF226
	.byte	0x1
	.2byte	0x16e
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x16
	.4byte	.LASF227
	.byte	0x1
	.2byte	0x170
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x16
	.4byte	.LASF228
	.byte	0x1
	.2byte	0x172
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x16
	.4byte	.LASF229
	.byte	0x1
	.2byte	0x174
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x16
	.4byte	.LASF230
	.byte	0x1
	.2byte	0x178
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF231
	.byte	0x1
	.2byte	0x17c
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x16
	.4byte	.LASF232
	.byte	0x1
	.2byte	0x17f
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x181
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x183
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x10d3
	.uleb128 0x12
	.4byte	0x4f
	.uleb128 0x15
	.4byte	.LASF234
	.byte	0x2
	.byte	0x6d
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x10ff
	.uleb128 0x14
	.4byte	.LASF235
	.byte	0x2
	.byte	0x6d
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF236
	.byte	0x2
	.byte	0x7e
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x1127
	.uleb128 0x1a
	.4byte	.LASF237
	.byte	0x2
	.byte	0x80
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF298
	.byte	0x2
	.byte	0xab
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x15
	.4byte	.LASF238
	.byte	0x2
	.byte	0xb7
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x118d
	.uleb128 0x14
	.4byte	.LASF207
	.byte	0x2
	.byte	0xb7
	.4byte	0xed4
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF212
	.byte	0x2
	.byte	0xb7
	.4byte	0xe4d
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1a
	.4byte	.LASF223
	.byte	0x2
	.byte	0xb9
	.4byte	0xe52
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1a
	.4byte	.LASF103
	.byte	0x2
	.byte	0xbb
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.byte	0x1
	.4byte	.LASF299
	.byte	0x2
	.byte	0xfe
	.byte	0x1
	.4byte	0x1af
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF240
	.byte	0x2
	.2byte	0x121
	.byte	0x1
	.4byte	0x81
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x1296
	.uleb128 0x18
	.4byte	.LASF225
	.byte	0x2
	.2byte	0x121
	.4byte	0x1296
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x18
	.4byte	.LASF241
	.byte	0x2
	.2byte	0x122
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x18
	.4byte	.LASF242
	.byte	0x2
	.2byte	0x123
	.4byte	0xb0e
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x18
	.4byte	.LASF243
	.byte	0x2
	.2byte	0x124
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x18
	.4byte	.LASF244
	.byte	0x2
	.2byte	0x125
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x16
	.4byte	.LASF245
	.byte	0x2
	.2byte	0x127
	.4byte	0xe52
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF246
	.byte	0x2
	.2byte	0x129
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x16
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x12b
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF247
	.byte	0x2
	.2byte	0x12d
	.4byte	0x123
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x16
	.4byte	.LASF248
	.byte	0x2
	.2byte	0x12f
	.4byte	0x123
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x16
	.4byte	.LASF249
	.byte	0x2
	.2byte	0x131
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x16
	.4byte	.LASF250
	.byte	0x2
	.2byte	0x136
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x16
	.4byte	.LASF251
	.byte	0x2
	.2byte	0x13a
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x13c
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x123
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF252
	.byte	0x2
	.2byte	0x1f1
	.byte	0x1
	.4byte	0xe47
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x12d9
	.uleb128 0x18
	.4byte	.LASF253
	.byte	0x2
	.2byte	0x1f1
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x1f8
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x20a
	.byte	0x1
	.4byte	0x1af
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x1316
	.uleb128 0x18
	.4byte	.LASF255
	.byte	0x2
	.2byte	0x20a
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x20c
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF256
	.byte	0x2
	.2byte	0x22e
	.byte	0x1
	.4byte	0x81
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x1343
	.uleb128 0x19
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x230
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF257
	.byte	0x2
	.2byte	0x23c
	.byte	0x1
	.4byte	0x93
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x138e
	.uleb128 0x18
	.4byte	.LASF207
	.byte	0x2
	.2byte	0x23c
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF258
	.byte	0x2
	.2byte	0x23c
	.4byte	0x138e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x243
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x3b7
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF259
	.byte	0x2
	.2byte	0x269
	.byte	0x1
	.4byte	0xe52
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x13d1
	.uleb128 0x18
	.4byte	.LASF207
	.byte	0x2
	.2byte	0x269
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF260
	.byte	0x2
	.2byte	0x269
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF261
	.byte	0x2
	.2byte	0x26f
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x13fb
	.uleb128 0x16
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x27b
	.4byte	0x1af
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF262
	.byte	0x2
	.2byte	0x2a1
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x1425
	.uleb128 0x16
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x2a3
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF263
	.byte	0x2
	.2byte	0x2c5
	.byte	0x1
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0x145e
	.uleb128 0x18
	.4byte	.LASF264
	.byte	0x2
	.2byte	0x2c5
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x2c7
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF265
	.byte	0x2
	.2byte	0x2e6
	.byte	0x1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0x14a6
	.uleb128 0x18
	.4byte	.LASF266
	.byte	0x2
	.2byte	0x2e6
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x2e8
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF267
	.byte	0x2
	.2byte	0x2ea
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF268
	.byte	0x2
	.2byte	0x324
	.byte	0x1
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.4byte	0x14df
	.uleb128 0x18
	.4byte	.LASF269
	.byte	0x2
	.2byte	0x324
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x324
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF270
	.byte	0x2
	.2byte	0x32f
	.byte	0x1
	.4byte	0x123
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0x1539
	.uleb128 0x18
	.4byte	.LASF271
	.byte	0x2
	.2byte	0x32f
	.4byte	0xe52
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x334
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x336
	.4byte	0x123
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"ucp\000"
	.byte	0x2
	.2byte	0x338
	.4byte	0x123
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF272
	.byte	0x2
	.2byte	0x384
	.byte	0x1
	.4byte	0x93
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0x15b4
	.uleb128 0x18
	.4byte	.LASF273
	.byte	0x2
	.2byte	0x384
	.4byte	0x1296
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x18
	.4byte	.LASF274
	.byte	0x2
	.2byte	0x385
	.4byte	0xdc1
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x16
	.4byte	.LASF275
	.byte	0x2
	.2byte	0x38f
	.4byte	0x81
	.byte	0x5
	.byte	0x3
	.4byte	record_count.9180
	.uleb128 0x19
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x393
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x395
	.4byte	0xe47
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF276
	.byte	0x2
	.2byte	0x397
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF277
	.byte	0x14
	.byte	0x30
	.4byte	0x15c5
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x12
	.4byte	0x22c
	.uleb128 0x1a
	.4byte	.LASF278
	.byte	0x14
	.byte	0x34
	.4byte	0x15db
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x12
	.4byte	0x22c
	.uleb128 0x1a
	.4byte	.LASF279
	.byte	0x14
	.byte	0x36
	.4byte	0x15f1
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x12
	.4byte	0x22c
	.uleb128 0x1a
	.4byte	.LASF280
	.byte	0x14
	.byte	0x38
	.4byte	0x1607
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x12
	.4byte	0x22c
	.uleb128 0x1a
	.4byte	.LASF281
	.byte	0x15
	.byte	0x33
	.4byte	0x161d
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x12
	.4byte	0x23c
	.uleb128 0x1a
	.4byte	.LASF282
	.byte	0x15
	.byte	0x3f
	.4byte	0x1633
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x12
	.4byte	0x2e3
	.uleb128 0x1e
	.4byte	.LASF283
	.byte	0xf
	.2byte	0x206
	.4byte	0x625
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF284
	.byte	0x10
	.byte	0x6a
	.4byte	0x6a3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF285
	.byte	0x12
	.2byte	0x20a
	.4byte	0xaad
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF286
	.byte	0x16
	.byte	0x78
	.4byte	0x216
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF287
	.byte	0x16
	.byte	0x9f
	.4byte	0x216
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF288
	.byte	0x16
	.2byte	0x11b
	.4byte	0x216
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF289
	.byte	0x16
	.2byte	0x11d
	.4byte	0x216
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF290
	.byte	0x13
	.2byte	0x258
	.4byte	0xdb5
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF291
	.byte	0x2
	.byte	0x3d
	.4byte	0x1b1
	.byte	0x5
	.byte	0x3
	.4byte	analog_420ma_sensor_group_list_hdr
	.uleb128 0xa
	.4byte	0x26c
	.4byte	0x16c6
	.uleb128 0xb
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF292
	.byte	0x1
	.byte	0x34
	.4byte	0x16d7
	.byte	0x5
	.byte	0x3
	.4byte	ANALOG_420MA_SENSOR_database_field_names
	.uleb128 0x12
	.4byte	0x16b6
	.uleb128 0xa
	.4byte	0x64
	.4byte	0x16ec
	.uleb128 0xb
	.4byte	0x25
	.byte	0x14
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF293
	.byte	0x2
	.byte	0x46
	.4byte	0x16fd
	.byte	0x5
	.byte	0x3
	.4byte	ANALOG_420MA_SENSOR_FILENAME
	.uleb128 0x12
	.4byte	0x16dc
	.uleb128 0xa
	.4byte	0x64
	.4byte	0x1712
	.uleb128 0xb
	.4byte	0x25
	.byte	0x13
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF294
	.byte	0x2
	.byte	0x48
	.4byte	0x171f
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	0x1702
	.uleb128 0xa
	.4byte	0x81
	.4byte	0x1734
	.uleb128 0xb
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF295
	.byte	0x2
	.byte	0x5e
	.4byte	0x1741
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	0x1724
	.uleb128 0x1e
	.4byte	.LASF283
	.byte	0xf
	.2byte	0x206
	.4byte	0x625
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF284
	.byte	0x10
	.byte	0x6a
	.4byte	0x6a3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF285
	.byte	0x12
	.2byte	0x20a
	.4byte	0xaad
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF286
	.byte	0x16
	.byte	0x78
	.4byte	0x216
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF287
	.byte	0x16
	.byte	0x9f
	.4byte	0x216
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF288
	.byte	0x16
	.2byte	0x11b
	.4byte	0x216
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF289
	.byte	0x16
	.2byte	0x11d
	.4byte	0x216
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF290
	.byte	0x13
	.2byte	0x258
	.4byte	0xdb5
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF294
	.byte	0x2
	.byte	0x48
	.4byte	0x17c5
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	ANALOG_420MA_SENSOR_DEFAULT_GROUP_NAME
	.uleb128 0x12
	.4byte	0x1702
	.uleb128 0x20
	.4byte	.LASF295
	.byte	0x2
	.byte	0x5e
	.4byte	0x17dc
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	analog_420ma_sensor_list_item_sizes
	.uleb128 0x12
	.4byte	0x1724
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI43
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI46
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI49
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI52
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI55
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI58
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI61
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI64
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI67
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xcc
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF188:
	.ascii	"send_weather_data_timer\000"
.LASF258:
	.ascii	"pmsrr\000"
.LASF264:
	.ascii	"pset_or_clear\000"
.LASF136:
	.ascii	"failures_to_respond_to_the_token_today\000"
.LASF249:
	.ascii	"lchanged_sensors_in_the_message\000"
.LASF170:
	.ascii	"connection_process_failures\000"
.LASF88:
	.ascii	"perform_a_discovery_following_a_FACTORY_reset\000"
.LASF29:
	.ascii	"pPrev\000"
.LASF211:
	.ascii	"pinitiator_box_index_0\000"
.LASF175:
	.ascii	"alerts_timer\000"
.LASF160:
	.ascii	"message\000"
.LASF42:
	.ascii	"GROUP_BASE_DEFINITION_STRUCT\000"
.LASF200:
	.ascii	"waiting_for_et_rain_tables_response\000"
.LASF141:
	.ascii	"device_exchange_port\000"
.LASF156:
	.ascii	"perform_two_wire_discovery\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF59:
	.ascii	"midnight_to_midnight_raw_pulse_count\000"
.LASF158:
	.ascii	"flowsense_devices_are_connected\000"
.LASF268:
	.ascii	"nm_ANALOG_420MA_SENSOR_set_new_reading_slave_side_v"
	.ascii	"ariables\000"
.LASF44:
	.ascii	"original_allocation\000"
.LASF130:
	.ascii	"request_a_scan_in_the_next_token_RESP\000"
.LASF37:
	.ascii	"list_support\000"
.LASF172:
	.ascii	"last_message_concluded_with_a_new_inbound_message\000"
.LASF208:
	.ascii	"pname\000"
.LASF133:
	.ascii	"i_am_the_master_and_i_have_rebooted\000"
.LASF101:
	.ascii	"changes_to_upload_to_comm_server\000"
.LASF168:
	.ascii	"waiting_to_start_the_connection_process_timer\000"
.LASF147:
	.ascii	"timer_token_rate_timer\000"
.LASF33:
	.ascii	"portTickType\000"
.LASF163:
	.ascii	"init_packet_message_id\000"
.LASF4:
	.ascii	"short int\000"
.LASF145:
	.ascii	"timer_device_exchange\000"
.LASF16:
	.ascii	"ET_TABLE_ENTRY\000"
.LASF83:
	.ascii	"commserver_monthly_rcvd_bytes\000"
.LASF106:
	.ascii	"reading_send_to_master__slave\000"
.LASF115:
	.ascii	"mode\000"
.LASF51:
	.ascii	"FLOW_RECORDING_CONTROL_STRUCT\000"
.LASF169:
	.ascii	"process_timer\000"
.LASF218:
	.ascii	"psensor_in_use\000"
.LASF280:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF105:
	.ascii	"reading_value_storage__slave\000"
.LASF52:
	.ascii	"analog_420ma_value\000"
.LASF99:
	.ascii	"changes_to_send_to_master\000"
.LASF186:
	.ascii	"waiting_for_lights_report_data_response\000"
.LASF118:
	.ascii	"flag_chain_down_at_the_next_opportunity\000"
.LASF248:
	.ascii	"lptr_where_to_put_the_bitfield\000"
.LASF142:
	.ascii	"device_exchange_state\000"
.LASF21:
	.ascii	"DATA_HANDLE\000"
.LASF58:
	.ascii	"roll_to_roll_raw_pulse_count\000"
.LASF46:
	.ascii	"first_to_display\000"
.LASF125:
	.ascii	"is_a_NEW_member_in_the_chain\000"
.LASF76:
	.ascii	"yesterdays_ET_was_zero\000"
.LASF216:
	.ascii	"pnew_box_index\000"
.LASF232:
	.ascii	"lgroup_created\000"
.LASF225:
	.ascii	"pucp\000"
.LASF297:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/analog_420ma_sensors.c\000"
.LASF69:
	.ascii	"et_rip\000"
.LASF75:
	.ascii	"run_away_gage\000"
.LASF262:
	.ascii	"ANALOG_420MA_SENSOR_set_bits_on_420mA_sensors_to_ca"
	.ascii	"use_distribution_in_the_next_token\000"
.LASF108:
	.ascii	"latest_reading_valid\000"
.LASF295:
	.ascii	"analog_420ma_sensor_list_item_sizes\000"
.LASF220:
	.ascii	"psensor_created\000"
.LASF43:
	.ascii	"float\000"
.LASF129:
	.ascii	"since_the_scan__slave__number_of_clean_token_respon"
	.ascii	"ses_made\000"
.LASF64:
	.ascii	"verify_string_pre\000"
.LASF22:
	.ascii	"DATE_TIME\000"
.LASF98:
	.ascii	"base\000"
.LASF25:
	.ascii	"count\000"
.LASF38:
	.ascii	"number_of_groups_ever_created\000"
.LASF45:
	.ascii	"next_available\000"
.LASF202:
	.ascii	"waiting_for_hub_is_busy_msg_response\000"
.LASF273:
	.ascii	"pucp_ptr\000"
.LASF68:
	.ascii	"dls_after_fall_when_to_clear_ignore_sxs\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF26:
	.ascii	"offset\000"
.LASF113:
	.ascii	"reason\000"
.LASF276:
	.ascii	"lreading\000"
.LASF239:
	.ascii	"nm_ANALOG_420MA_SENSOR_extract_and_store_changes_fr"
	.ascii	"om_comm\000"
.LASF87:
	.ascii	"factory_reset_registration_to_tell_commserver_to_cl"
	.ascii	"ear_pdata\000"
.LASF57:
	.ascii	"hourly_total_inches_100u\000"
.LASF291:
	.ascii	"analog_420ma_sensor_group_list_hdr\000"
.LASF85:
	.ascii	"commserver_monthly_code_receipt_bytes\000"
.LASF233:
	.ascii	"nm_ANALOG_420MA_SENSOR_store_changes\000"
.LASF210:
	.ascii	"preason_for_change\000"
.LASF149:
	.ascii	"token_in_transit\000"
.LASF294:
	.ascii	"ANALOG_420MA_SENSOR_DEFAULT_GROUP_NAME\000"
.LASF140:
	.ascii	"device_exchange_initial_event\000"
.LASF102:
	.ascii	"changes_uploaded_to_comm_server_awaiting_ACK\000"
.LASF283:
	.ascii	"weather_preserves\000"
.LASF254:
	.ascii	"ANALOG_420MA_SENSOR_get_group_with_this_GID\000"
.LASF36:
	.ascii	"xTimerHandle\000"
.LASF18:
	.ascii	"RAIN_TABLE_ENTRY\000"
.LASF24:
	.ascii	"ptail\000"
.LASF53:
	.ascii	"sensor_serial_number\000"
.LASF82:
	.ascii	"pending_changes_to_send_to_comm_server\000"
.LASF242:
	.ascii	"pmem_used_so_far_is_less_than_allocated_memory\000"
.LASF62:
	.ascii	"needs_to_be_broadcast\000"
.LASF256:
	.ascii	"ANALOG_420MA_SENSOR_get_list_count\000"
.LASF9:
	.ascii	"UNS_8\000"
.LASF292:
	.ascii	"ANALOG_420MA_SENSOR_database_field_names\000"
.LASF281:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF17:
	.ascii	"rain_inches_u16_100u\000"
.LASF27:
	.ascii	"InUse\000"
.LASF79:
	.ascii	"rain_switch_active\000"
.LASF135:
	.ascii	"failures_to_respond_to_the_token\000"
.LASF234:
	.ascii	"nm_analog_420ma_sensor_structure_updater\000"
.LASF132:
	.ascii	"i_am_a_slave_and_i_have_rebooted\000"
.LASF47:
	.ascii	"first_to_send\000"
.LASF230:
	.ascii	"lmatching\000"
.LASF65:
	.ascii	"dls_saved_date\000"
.LASF166:
	.ascii	"now_xmitting\000"
.LASF250:
	.ascii	"lmem_used_by_this_group\000"
.LASF72:
	.ascii	"nlu_old_rain_table_sync_variable\000"
.LASF266:
	.ascii	"pcomm_error_occurred\000"
.LASF195:
	.ascii	"a_pdata_message_is_on_the_list\000"
.LASF223:
	.ascii	"lchange_bitfield_to_set\000"
.LASF19:
	.ascii	"dptr\000"
.LASF8:
	.ascii	"char\000"
.LASF80:
	.ascii	"freeze_switch_active\000"
.LASF183:
	.ascii	"waiting_for_poc_report_data_response\000"
.LASF171:
	.ascii	"last_message_concluded_with_a_response_timeout\000"
.LASF184:
	.ascii	"waiting_for_system_report_data_response\000"
.LASF73:
	.ascii	"et_table_update_all_historical_values\000"
.LASF55:
	.ascii	"unused_01\000"
.LASF23:
	.ascii	"phead\000"
.LASF178:
	.ascii	"current_msg_frcs_ptr\000"
.LASF2:
	.ascii	"long long int\000"
.LASF174:
	.ascii	"a_registration_message_is_on_the_list\000"
.LASF282:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF15:
	.ascii	"status\000"
.LASF212:
	.ascii	"pset_change_bits\000"
.LASF92:
	.ascii	"hub_needs_to_distribute_main_binary\000"
.LASF86:
	.ascii	"factory_reset_do_not_allow_pdata_to_be_sent\000"
.LASF139:
	.ascii	"broadcast_chain_members_array\000"
.LASF244:
	.ascii	"pallocated_memory\000"
.LASF252:
	.ascii	"ANALOG_420MA_SENSOR_get_pointer_to_sensor_with_this"
	.ascii	"_box_index\000"
.LASF122:
	.ascii	"we_have_crossed_midnight_so_clear_the_error_counts\000"
.LASF240:
	.ascii	"ANALOG_420MA_SENSOR_build_data_to_send\000"
.LASF96:
	.ascii	"ANALOG_420MA_SENSOR_RECORDER_CONTROL_STRUCT\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF32:
	.ascii	"MIST_DLINK_TYPE\000"
.LASF127:
	.ascii	"since_the_scan__master__number_of_clean_token_respo"
	.ascii	"nses_rcvd\000"
.LASF39:
	.ascii	"group_identity_number\000"
.LASF40:
	.ascii	"description\000"
.LASF289:
	.ascii	"analog_420ma_sensor_items_recursive_MUTEX\000"
.LASF89:
	.ascii	"ununsed_uns8_1\000"
.LASF228:
	.ascii	"lbitfield_of_changes\000"
.LASF155:
	.ascii	"token_date_time\000"
.LASF66:
	.ascii	"dls_eligible_to_fall_back\000"
.LASF185:
	.ascii	"waiting_for_budget_report_data_response\000"
.LASF109:
	.ascii	"latest_reading_time_stamp\000"
.LASF150:
	.ascii	"packets_waiting_for_token\000"
.LASF286:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF189:
	.ascii	"waiting_for_rain_indication_response\000"
.LASF81:
	.ascii	"write_tpmicro_file_in_new_file_format\000"
.LASF229:
	.ascii	"ltemporary\000"
.LASF296:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF154:
	.ascii	"flag_update_date_time\000"
.LASF112:
	.ascii	"CHANGE_TRACKING_FOR_COMMUNICATIONS_STRUCT\000"
.LASF274:
	.ascii	"pcontroller_index\000"
.LASF224:
	.ascii	"lgroup\000"
.LASF205:
	.ascii	"hub_packet_activity_timer\000"
.LASF194:
	.ascii	"waiting_for_firmware_version_check_response\000"
.LASF74:
	.ascii	"dont_use_et_gage_today\000"
.LASF165:
	.ascii	"CONTROLLER_INITIATED_MESSAGE_TRANSMITTING\000"
.LASF77:
	.ascii	"remaining_gage_pulses\000"
.LASF10:
	.ascii	"UNS_16\000"
.LASF237:
	.ascii	"lsensor\000"
.LASF104:
	.ascii	"in_use\000"
.LASF196:
	.ascii	"waiting_for_pdata_response\000"
.LASF182:
	.ascii	"waiting_for_station_report_data_response\000"
.LASF103:
	.ascii	"box_index_0\000"
.LASF134:
	.ascii	"pending_device_exchange_request\000"
.LASF199:
	.ascii	"waiting_for_asked_commserver_if_there_is_pdata_for_"
	.ascii	"us_response\000"
.LASF272:
	.ascii	"ANALOG_420MA_SENSOR_extract_analog_420ma_reading_fr"
	.ascii	"om_token_response\000"
.LASF14:
	.ascii	"et_inches_u16_10000u\000"
.LASF180:
	.ascii	"waiting_for_check_for_updates_response\000"
.LASF78:
	.ascii	"clear_runaway_gage\000"
.LASF235:
	.ascii	"pfrom_revision\000"
.LASF48:
	.ascii	"pending_first_to_send\000"
.LASF157:
	.ascii	"flowsense_device_establish_connection_timer\000"
.LASF173:
	.ascii	"waiting_for_registration_response\000"
.LASF238:
	.ascii	"nm_ANALOG_420MA_SENSOR_set_default_values\000"
.LASF260:
	.ascii	"pchange_reason\000"
.LASF84:
	.ascii	"commserver_monthly_xmit_bytes\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF114:
	.ascii	"DATE_TIME_TOKEN_STRUCT\000"
.LASF203:
	.ascii	"msgs_to_send_queue\000"
.LASF190:
	.ascii	"send_rain_indication_timer\000"
.LASF146:
	.ascii	"timer_message_resp\000"
.LASF117:
	.ascii	"chain_is_down\000"
.LASF71:
	.ascii	"sync_the_et_rain_tables\000"
.LASF269:
	.ascii	"preading\000"
.LASF241:
	.ascii	"pmem_used_so_far\000"
.LASF191:
	.ascii	"waiting_for_mobile_status_response\000"
.LASF226:
	.ascii	"lgroup_id\000"
.LASF298:
	.ascii	"save_file_analog_420ma_sensor\000"
.LASF299:
	.ascii	"nm_ANLAOG_420MA_SENSOR_create_new_group\000"
.LASF247:
	.ascii	"lptr_to_num_changed_sensors\000"
.LASF255:
	.ascii	"pgroup_ID\000"
.LASF35:
	.ascii	"xSemaphoreHandle\000"
.LASF1:
	.ascii	"long int\000"
.LASF209:
	.ascii	"pgenerate_change_line_bool\000"
.LASF159:
	.ascii	"COMM_MNGR_STRUCT\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF70:
	.ascii	"rain\000"
.LASF91:
	.ascii	"hub_needs_to_distribute_tpmicro_binary\000"
.LASF214:
	.ascii	"nm_ANALOG_420MA_SENSOR_set_name\000"
.LASF261:
	.ascii	"ANALOG_420MA_SENSOR_clean_house_processing\000"
.LASF126:
	.ascii	"since_the_scan__master__number_of_clean_tokens_made"
	.ascii	"\000"
.LASF110:
	.ascii	"distribute_changes_to_slave\000"
.LASF231:
	.ascii	"lnum_changed_groups\000"
.LASF246:
	.ascii	"lbitfield_of_changes_in_the_msg\000"
.LASF111:
	.ascii	"send_changes_to_master\000"
.LASF151:
	.ascii	"incoming_messages_or_packets\000"
.LASF123:
	.ascii	"start_a_scan_at_the_next_opportunity\000"
.LASF49:
	.ascii	"pending_first_to_send_in_use\000"
.LASF153:
	.ascii	"timer_commserver_msg_receipt_error\000"
.LASF28:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF120:
	.ascii	"timer_token_arrival\000"
.LASF285:
	.ascii	"comm_mngr\000"
.LASF217:
	.ascii	"nm_ANALOG_420MA_SENSOR_set_in_use\000"
.LASF293:
	.ascii	"ANALOG_420MA_SENSOR_FILENAME\000"
.LASF148:
	.ascii	"token_rate_timer_has_timed_out\000"
.LASF31:
	.ascii	"pListHdr\000"
.LASF284:
	.ascii	"a420_srcs\000"
.LASF198:
	.ascii	"waiting_for_firmware_version_check_before_asking_fo"
	.ascii	"r_pdata_response\000"
.LASF119:
	.ascii	"timer_rescan\000"
.LASF144:
	.ascii	"device_exchange_saved_comm_mngr_mode\000"
.LASF192:
	.ascii	"send_mobile_status_timer\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF278:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF60:
	.ascii	"rain_shutdown_rcvd_from_commserver_uns32\000"
.LASF197:
	.ascii	"pdata_timer\000"
.LASF107:
	.ascii	"reading_value_storage__master\000"
.LASF54:
	.ascii	"sensor_type\000"
.LASF56:
	.ascii	"ANALOG_420MA_SENSOR_RECORDER_RECORD\000"
.LASF167:
	.ascii	"response_timer\000"
.LASF50:
	.ascii	"when_to_send_timer\000"
.LASF93:
	.ascii	"expansion\000"
.LASF267:
	.ascii	"lfile_save_necessary\000"
.LASF34:
	.ascii	"xQueueHandle\000"
.LASF207:
	.ascii	"psensor\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF20:
	.ascii	"dlen\000"
.LASF41:
	.ascii	"deleted\000"
.LASF187:
	.ascii	"waiting_for_weather_data_receipt_response\000"
.LASF63:
	.ascii	"RAIN_STATE\000"
.LASF162:
	.ascii	"frcs_ptr\000"
.LASF121:
	.ascii	"scans_while_chain_is_down\000"
.LASF94:
	.ascii	"WEATHER_PRESERVES_STRUCT\000"
.LASF257:
	.ascii	"ANALOG_420MA_SENSOR_fill_out_recorder_record\000"
.LASF116:
	.ascii	"state\000"
.LASF152:
	.ascii	"changes\000"
.LASF263:
	.ascii	"ANALOG_420MA_SENSOR_on_all_sensors_set_or_clear_com"
	.ascii	"mserver_change_bits\000"
.LASF271:
	.ascii	"phow_many_bytes_ptr\000"
.LASF100:
	.ascii	"changes_to_distribute_to_slaves\000"
.LASF243:
	.ascii	"preason_data_is_being_built\000"
.LASF137:
	.ascii	"main_app_firmware_up_to_date\000"
.LASF201:
	.ascii	"waiting_for_the_no_more_messages_msg_response\000"
.LASF164:
	.ascii	"data_packet_message_id\000"
.LASF259:
	.ascii	"ANALOG_420MA_SENSOR_get_change_bits_ptr\000"
.LASF288:
	.ascii	"analog_420ma_sensor_recorder_recursive_MUTEX\000"
.LASF161:
	.ascii	"activity_flag_ptr\000"
.LASF3:
	.ascii	"signed char\000"
.LASF124:
	.ascii	"start_a_scan_captured_reason\000"
.LASF287:
	.ascii	"comm_mngr_recursive_MUTEX\000"
.LASF193:
	.ascii	"mobile_seconds_since_last_command\000"
.LASF236:
	.ascii	"init_file_analog_420ma_sensor\000"
.LASF277:
	.ascii	"GuiFont_LanguageActive\000"
.LASF181:
	.ascii	"waiting_for_station_history_response\000"
.LASF253:
	.ascii	"pbox_index\000"
.LASF213:
	.ascii	"pchange_bitfield_to_set\000"
.LASF67:
	.ascii	"dls_after_fall_back_ignore_sxs\000"
.LASF219:
	.ascii	"ptemporary_group\000"
.LASF179:
	.ascii	"waiting_for_moisture_sensor_recording_response\000"
.LASF177:
	.ascii	"waiting_for_flow_recording_response\000"
.LASF290:
	.ascii	"cics\000"
.LASF61:
	.ascii	"inhibit_irrigation_have_crossed_the_minimum\000"
.LASF95:
	.ascii	"double\000"
.LASF251:
	.ascii	"lmem_overhead_per_group\000"
.LASF206:
	.ascii	"CONTROLLER_INITIATED_CONTROL_STRUCT\000"
.LASF215:
	.ascii	"nm_ANALOG_420MA_SENSOR_set_box_index\000"
.LASF128:
	.ascii	"since_the_scan__slave__number_of_clean_tokens_rcvd\000"
.LASF222:
	.ascii	"pbitfield_of_changes\000"
.LASF227:
	.ascii	"lsize_of_bitfield\000"
.LASF245:
	.ascii	"lptr_to_bitfield_of_changes_to_send\000"
.LASF131:
	.ascii	"flag_myself_as_NEW_in_the_scan_RESP\000"
.LASF265:
	.ascii	"nm_ANALOG_420MA_SENSOR_update_pending_change_bits\000"
.LASF221:
	.ascii	"pchanges_received_from\000"
.LASF176:
	.ascii	"waiting_for_alerts_response\000"
.LASF30:
	.ascii	"pNext\000"
.LASF90:
	.ascii	"commserver_monthly_mobile_status_updates_bytes\000"
.LASF270:
	.ascii	"ANALOG_420MA_SENSOR_find_a_sensor_reading_to_send_t"
	.ascii	"o_the_master\000"
.LASF143:
	.ascii	"device_exchange_device_index\000"
.LASF204:
	.ascii	"queued_msgs_polling_timer\000"
.LASF97:
	.ascii	"ANALOG_420MA_SENSOR_GROUP_STRUCT\000"
.LASF138:
	.ascii	"tpmicro_firmware_up_to_date\000"
.LASF275:
	.ascii	"record_count\000"
.LASF279:
	.ascii	"GuiFont_DecimalChar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
