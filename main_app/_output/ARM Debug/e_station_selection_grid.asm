	.file	"e_station_selection_grid.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	g_STATION_SELECTION_GRID_user_pressed_back
	.section	.bss.g_STATION_SELECTION_GRID_user_pressed_back,"aw",%nobits
	.align	2
	.type	g_STATION_SELECTION_GRID_user_pressed_back, %object
	.size	g_STATION_SELECTION_GRID_user_pressed_back, 4
g_STATION_SELECTION_GRID_user_pressed_back:
	.space	4
	.section	.bss.g_STATION_SELECTION_GRID_dialog_displayed,"aw",%nobits
	.align	2
	.type	g_STATION_SELECTION_GRID_dialog_displayed, %object
	.size	g_STATION_SELECTION_GRID_dialog_displayed, 4
g_STATION_SELECTION_GRID_dialog_displayed:
	.space	4
	.section	.bss.g_STATION_SELECTION_GRID_user_toggled_display,"aw",%nobits
	.align	2
	.type	g_STATION_SELECTION_GRID_user_toggled_display, %object
	.size	g_STATION_SELECTION_GRID_user_toggled_display, 4
g_STATION_SELECTION_GRID_user_toggled_display:
	.space	4
	.section	.bss.CURSOR_statistics,"aw",%nobits
	.align	2
	.type	CURSOR_statistics, %object
	.size	CURSOR_statistics, 20
CURSOR_statistics:
	.space	20
	.section	.bss.CURSOR_current_cursor_ptr,"aw",%nobits
	.align	2
	.type	CURSOR_current_cursor_ptr, %object
	.size	CURSOR_current_cursor_ptr, 4
CURSOR_current_cursor_ptr:
	.space	4
	.section	.bss.Group_CP,"aw",%nobits
	.align	2
	.type	Group_CP, %object
	.size	Group_CP, 18432
Group_CP:
	.space	18432
	.section	.bss.g_CURSOR_mgmt_add_on,"aw",%nobits
	.align	2
	.type	g_CURSOR_mgmt_add_on, %object
	.size	g_CURSOR_mgmt_add_on, 9216
g_CURSOR_mgmt_add_on:
	.space	9216
	.section	.bss.station_selection_struct,"aw",%nobits
	.align	2
	.type	station_selection_struct, %object
	.size	station_selection_struct, 15360
station_selection_struct:
	.space	15360
	.section	.bss.CURSOR_station_when_display_was_toggled,"aw",%nobits
	.align	2
	.type	CURSOR_station_when_display_was_toggled, %object
	.size	CURSOR_station_when_display_was_toggled, 20
CURSOR_station_when_display_was_toggled:
	.space	20
	.section	.bss.g_CURSOR_last_station_selected,"aw",%nobits
	.align	2
	.type	g_CURSOR_last_station_selected, %object
	.size	g_CURSOR_last_station_selected, 4
g_CURSOR_last_station_selected:
	.space	4
	.global	__udivsi3
	.section	.text.__resize_scroll_box_marker,"ax",%progbits
	.align	2
	.type	__resize_scroll_box_marker, %function
__resize_scroll_box_marker:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_station_selection_grid.c"
	.loc 1 203 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	.loc 1 204 0
	ldr	r3, .L4
	ldr	r2, [r3, #16]
	ldr	r3, .L4
	ldr	r3, [r3, #8]
	cmp	r2, r3
	bhi	.L2
	.loc 1 206 0
	ldr	r3, .L4+4
	mov	r2, #175
	str	r2, [r3, #0]
	b	.L1
.L2:
	.loc 1 214 0
	ldr	r3, .L4
	ldr	r2, [r3, #16]
	ldr	r3, .L4+8
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #3
	mov	r0, #175
	mov	r1, r3
	bl	__udivsi3
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fmrs	r0, s15
	bl	__round_UNS16
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L4+4
	str	r2, [r3, #0]
.L1:
	.loc 1 216 0
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	CURSOR_statistics
	.word	GuiVar_StationSelectionGridMarkerSize
	.word	-1431655765
.LFE0:
	.size	__resize_scroll_box_marker, .-__resize_scroll_box_marker
	.section	.text.__move_scroll_box_marker,"ax",%progbits
	.align	2
	.type	__move_scroll_box_marker, %function
__move_scroll_box_marker:
.LFB1:
	.loc 1 220 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI2:
	add	fp, sp, #4
.LCFI3:
	sub	sp, sp, #4
.LCFI4:
	str	r0, [fp, #-8]
	.loc 1 221 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L7
	.loc 1 223 0
	ldr	r3, .L9
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L6
.L7:
	.loc 1 227 0
	ldr	r3, .L9+4
	ldr	r2, [r3, #16]
	ldr	r3, .L9+4
	ldr	r3, [r3, #8]
	cmp	r2, r3
	bls	.L6
	.loc 1 233 0
	ldr	r3, .L9+8
	ldr	r3, [r3, #0]
	ldr	r0, .L9+12
	mov	r1, r3
	bl	__udivsi3
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fmrs	r0, s15
	bl	__round_UNS16
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L9
	str	r2, [r3, #0]
.L6:
	.loc 1 236 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L10:
	.align	2
.L9:
	.word	GuiVar_StationSelectionGridMarkerPos
	.word	CURSOR_statistics
	.word	GuiVar_StationSelectionGridMarkerSize
	.word	2100
.LFE1:
	.size	__move_scroll_box_marker, .-__move_scroll_box_marker
	.section	.text.__draw_group_name_and_controller_name,"ax",%progbits
	.align	2
	.type	__draw_group_name_and_controller_name, %function
__draw_group_name_and_controller_name:
.LFB2:
	.loc 1 252 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #4
.LCFI7:
	.loc 1 260 0
	ldr	r3, .L14
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 262 0
	b	.L12
.L13:
	.loc 1 264 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #24
	str	r3, [fp, #-8]
.L12:
	.loc 1 262 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #8]
	ldr	r3, .L14+4
	ldr	r3, [r3, #4]
	cmp	r2, r3
	bhi	.L13
	.loc 1 271 0
	ldr	r0, .L14+8
	ldr	r1, .L14+12
	mov	r2, #49
	bl	strlcpy
	.loc 1 273 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #4]
	ldr	r1, .L14+16
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, .L14+20
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	.loc 1 274 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L15:
	.align	2
.L14:
	.word	CURSOR_current_cursor_ptr
	.word	CURSOR_statistics
	.word	GuiVar_StationSelectionGridGroupName
	.word	GuiVar_GroupName
	.word	station_selection_struct
	.word	GuiVar_StationSelectionGridControllerName
.LFE2:
	.size	__draw_group_name_and_controller_name, .-__draw_group_name_and_controller_name
	.section .rodata
	.align	2
.LC0:
	.ascii	"%s %c %s: %s\000"
	.align	2
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_station_selection_grid.c\000"
	.section	.text.__draw_station_description_for_selected_cursor,"ax",%progbits
	.align	2
	.type	__draw_station_description_for_selected_cursor, %function
__draw_station_description_for_selected_cursor:
.LFB3:
	.loc 1 298 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI8:
	add	fp, sp, #12
.LCFI9:
	sub	sp, sp, #32
.LCFI10:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	.loc 1 303 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 307 0
	ldr	r2, [fp, #-32]
	ldr	r3, .L21
	cmp	r2, r3
	bhi	.L17
	.loc 1 309 0
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L18
	.loc 1 313 0
	ldr	r0, .L21+4
	ldr	r2, [fp, #-32]
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L19
	.loc 1 315 0
	ldr	r0, .L21+8
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r4, r0
	ldr	r1, .L21+4
	ldr	r2, [fp, #-32]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	add	r5, r3, #65
	ldr	r1, .L21+4
	ldr	r2, [fp, #-32]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r1, [r3, #0]
	ldr	ip, .L21+4
	ldr	r2, [fp, #-32]
	mov	r0, #4
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r2, [r3, #0]
	sub	r3, fp, #24
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #8
	bl	STATION_get_station_number_string
	mov	r1, r0
	ldr	ip, .L21+4
	ldr	r2, [fp, #-32]
	mov	r0, #16
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	str	r5, [sp, #0]
	str	r1, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, .L21+12
	mov	r1, #48
	ldr	r2, .L21+16
	mov	r3, r4
	bl	snprintf
	str	r0, [fp, #-16]
.L19:
	.loc 1 320 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L16
	.loc 1 322 0
	ldr	r3, .L21+12
	mov	r2, #0
	strb	r2, [r3, #0]
	b	.L16
.L18:
	.loc 1 329 0
	ldr	r3, .L21+12
	mov	r2, #0
	strb	r2, [r3, #0]
	b	.L16
.L17:
	.loc 1 334 0
	ldr	r0, .L21+20
	ldr	r1, .L21+24
	bl	Alert_index_out_of_range_with_filename
	.loc 1 336 0
	ldr	r3, .L21+12
	mov	r2, #0
	strb	r2, [r3, #0]
.L16:
	.loc 1 338 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L22:
	.align	2
.L21:
	.word	767
	.word	station_selection_struct
	.word	1058
	.word	GuiVar_StationDescription
	.word	.LC0
	.word	.LC1
	.word	334
.LFE3:
	.size	__draw_station_description_for_selected_cursor, .-__draw_station_description_for_selected_cursor
	.section .rodata
	.align	2
.LC2:
	.ascii	"---\000"
	.align	2
.LC3:
	.ascii	" \000"
	.section	.text.__draw_station_number_for_current_cursor,"ax",%progbits
	.align	2
	.type	__draw_station_number_for_current_cursor, %function
__draw_station_number_for_current_cursor:
.LFB4:
	.loc 1 362 0
	@ args = 0, pretend = 0, frame = 300
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	sub	sp, sp, #348
.LCFI13:
	str	r0, [fp, #-304]
	.loc 1 363 0
	mov	r3, #3
	str	r3, [fp, #-24]
	.loc 1 379 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 383 0
	ldr	r3, [fp, #-304]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L23
	.loc 1 383 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-304]
	ldr	r3, [r3, #12]
	cmp	r3, #0
	beq	.L23
	.loc 1 391 0 is_stmt 1
	ldr	r3, .L36
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-304]
	cmp	r2, r3
	bne	.L25
	.loc 1 393 0
	ldr	r3, [fp, #-304]
	ldr	r3, [r3, #4]
	mov	r0, #1
	mov	r1, r3
	bl	__draw_station_description_for_selected_cursor
	b	.L26
.L25:
	.loc 1 395 0
	ldr	r3, .L36
	ldr	r2, [r3, #0]
	ldr	r3, .L36+4
	cmp	r2, r3
	bne	.L26
	.loc 1 397 0
	mov	r0, #1
	mov	r1, #0
	bl	__draw_station_description_for_selected_cursor
.L26:
	.loc 1 401 0
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #4]
	ldr	r0, .L36+8
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L27
	.loc 1 403 0
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #4]
	ldr	r1, .L36+8
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r1, [r3, #0]
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #4]
	ldr	ip, .L36+8
	mov	r0, #4
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r2, [r3, #0]
	sub	r3, fp, #300
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #256
	bl	STATION_get_station_number_string
	b	.L28
.L27:
	.loc 1 407 0
	sub	r3, fp, #300
	mov	r0, r3
	ldr	r1, .L36+12
	mov	r2, #256
	bl	strlcpy
.L28:
	.loc 1 412 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 413 0
	mov	r3, #2
	str	r3, [fp, #-32]
	.loc 1 418 0
	ldr	r3, .L36+16
	ldr	r2, [fp, #-32]
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #300
	mov	r0, r2
	mov	r1, r3
	mov	r2, #3
	bl	GuiLib_GetTextWidth
	mov	r3, r0
	str	r3, [fp, #-36]
	.loc 1 420 0
	ldr	r3, [fp, #-304]
	ldr	r1, [r3, #16]
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #0]
	ldr	r0, .L36+20
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	ldr	r3, [r3, #0]
	add	r3, r1, r3
	str	r3, [fp, #-40]
	.loc 1 425 0
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #0]
	ldr	r0, .L36+20
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	beq	.L29
	.loc 1 429 0
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #0]
	ldr	r0, .L36+20
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	add	r1, r2, r3
	.loc 1 431 0
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #0]
	ldr	ip, .L36+20
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	.loc 1 430 0
	add	r3, r1, r3
	.loc 1 429 0
	str	r3, [fp, #-44]
	.loc 1 438 0
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	sub	r3, r3, #14
	mvn	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L36+24
	str	r2, [sp, #4]
	mov	r2, #3
	str	r2, [sp, #8]
	mov	r2, #3
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	mov	r2, #0
	str	r2, [sp, #20]
	ldr	r2, [fp, #-44]
	str	r2, [sp, #24]
	mov	r2, #0
	str	r2, [sp, #28]
	mov	r2, #0
	str	r2, [sp, #32]
	mov	r2, #0
	str	r2, [sp, #36]
	mov	r2, #15
	str	r2, [sp, #40]
	mov	r2, #15
	str	r2, [sp, #44]
	mov	r0, r3
	ldr	r1, [fp, #-40]
	mov	r2, #14
	ldr	r3, [fp, #-32]
	bl	CS_DrawStr
	str	r0, [fp, #-20]
.L29:
	.loc 1 447 0
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #0]
	ldr	r0, .L36+20
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [fp, #-36]
	str	r2, [r3, #0]
	.loc 1 451 0
	ldr	r3, .L36
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-304]
	cmp	r2, r3
	bne	.L30
	.loc 1 453 0
	mov	r3, #15
	str	r3, [fp, #-8]
	.loc 1 454 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-12]
	b	.L31
.L30:
	.loc 1 458 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-8]
	.loc 1 459 0
	mov	r3, #15
	str	r3, [fp, #-12]
.L31:
	.loc 1 464 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L32
	.loc 1 466 0
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	sub	r3, r3, #14
	mvn	r2, #0
	str	r2, [sp, #0]
	.loc 1 468 0
	sub	r2, fp, #300
	.loc 1 466 0
	str	r2, [sp, #4]
	mov	r2, #3
	str	r2, [sp, #8]
	mov	r2, #3
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	mov	r2, #0
	str	r2, [sp, #20]
	mov	r2, #0
	str	r2, [sp, #24]
	mov	r2, #0
	str	r2, [sp, #28]
	mov	r2, #0
	str	r2, [sp, #32]
	mov	r2, #0
	str	r2, [sp, #36]
	ldr	r2, [fp, #-8]
	str	r2, [sp, #40]
	ldr	r2, [fp, #-12]
	str	r2, [sp, #44]
	mov	r0, r3
	ldr	r1, [fp, #-40]
	mov	r2, #14
	ldr	r3, [fp, #-32]
	bl	CS_DrawStr
	str	r0, [fp, #-20]
.L32:
	.loc 1 474 0
	ldr	r3, [fp, #-304]
	ldr	r2, [r3, #0]
	ldr	r0, .L36+20
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 476 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L33
	.loc 1 478 0
	ldr	r3, .L36+28
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	b	.L34
.L35:
	.loc 1 480 0 discriminator 2
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #5
	mov	r2, r3
	ldr	r3, .L36+32
	add	r1, r2, r3
	ldr	r3, .L36+28
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-16]
	rsb	r0, r3, r2
	ldr	r3, .L36+28
	ldr	r2, [r3, #4]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	add	r2, r0, r3
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #5
	sub	r2, r3, #1760
	ldr	r3, .L36+36
	add	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	mov	r2, #155
	bl	memcpy
	.loc 1 478 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L34:
	.loc 1 478 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #212
	bls	.L35
	.loc 1 478 0
	b	.L23
.L33:
	.loc 1 485 0 is_stmt 1
	ldr	r3, .L36+28
	mov	r2, #0
	str	r2, [r3, #12]
.L23:
	.loc 1 488 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L37:
	.align	2
.L36:
	.word	CURSOR_current_cursor_ptr
	.word	Group_CP
	.word	station_selection_struct
	.word	.LC2
	.word	GuiFont_FontList
	.word	g_CURSOR_mgmt_add_on
	.word	.LC3
	.word	CURSOR_statistics
	.word	primary_display_buf
	.word	utility_display_buf
.LFE4:
	.size	__draw_station_number_for_current_cursor, .-__draw_station_number_for_current_cursor
	.section	.text.__show_cursor,"ax",%progbits
	.align	2
	.type	__show_cursor, %function
__show_cursor:
.LFB5:
	.loc 1 510 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI14:
	add	fp, sp, #4
.LCFI15:
	sub	sp, sp, #12
.LCFI16:
	str	r0, [fp, #-16]
	.loc 1 515 0
	ldr	r3, .L48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L38
	.loc 1 517 0
	ldr	r3, [fp, #-16]
	cmp	r3, #1
	bne	.L40
	.loc 1 519 0
	mov	r0, #1
	bl	__move_scroll_box_marker
	.loc 1 521 0
	ldr	r3, .L48+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 523 0
	b	.L41
.L43:
	.loc 1 525 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L41:
	.loc 1 523 0 discriminator 1
	ldr	r0, .L48+8
	ldr	r2, [fp, #-8]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L42
	.loc 1 523 0 is_stmt 0 discriminator 2
	ldr	r0, .L48+8
	ldr	r2, [fp, #-8]
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L43
.L42:
	.loc 1 528 0 is_stmt 1
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L48+8
	add	r2, r2, r3
	ldr	r3, .L48
	str	r2, [r3, #0]
	.loc 1 530 0
	ldr	r3, .L48+12
	mov	r2, #1
	str	r2, [r3, #4]
	.loc 1 532 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-12]
	b	.L44
.L40:
	.loc 1 536 0
	ldr	r3, .L48+12
	ldr	r3, [r3, #16]
	sub	r2, r3, #1
	ldr	r3, .L48+12
	ldr	r3, [r3, #8]
	cmp	r2, r3
	bhi	.L45
	.loc 1 538 0
	ldr	r3, .L48+12
	mov	r2, #1
	str	r2, [r3, #4]
	b	.L46
.L45:
	.loc 1 542 0
	ldr	r3, .L48+12
	ldr	r2, [r3, #16]
	ldr	r3, .L48+12
	ldr	r3, [r3, #8]
	rsb	r3, r3, r2
	sub	r2, r3, #1
	ldr	r3, .L48+12
	str	r2, [r3, #4]
.L46:
	.loc 1 545 0
	ldr	r3, .L48+4
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L48+8
	add	r2, r2, r3
	ldr	r3, .L48
	str	r2, [r3, #0]
	.loc 1 547 0
	mov	r0, #0
	bl	__move_scroll_box_marker
	.loc 1 549 0
	ldr	r3, .L48+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
.L44:
	.loc 1 552 0
	bl	__draw_group_name_and_controller_name
	.loc 1 556 0
	ldr	r0, .L48+8
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L47
	.loc 1 556 0 is_stmt 0 discriminator 1
	ldr	r0, .L48+8
	ldr	r2, [fp, #-12]
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L47
	.loc 1 558 0 is_stmt 1
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L48+8
	add	r3, r2, r3
	mov	r0, r3
	bl	__draw_station_number_for_current_cursor
.L47:
	.loc 1 561 0
	ldr	r3, .L48
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #4]
	mov	r0, #1
	mov	r1, r3
	bl	__draw_station_description_for_selected_cursor
.L38:
	.loc 1 563 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L49:
	.align	2
.L48:
	.word	CURSOR_current_cursor_ptr
	.word	g_CURSOR_last_station_selected
	.word	Group_CP
	.word	CURSOR_statistics
.LFE5:
	.size	__show_cursor, .-__show_cursor
	.section	.text.__FDTO_draw_all_cursor_positions,"ax",%progbits
	.align	2
	.type	__FDTO_draw_all_cursor_positions, %function
__FDTO_draw_all_cursor_positions:
.LFB6:
	.loc 1 608 0
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI17:
	add	fp, sp, #4
.LCFI18:
	sub	sp, sp, #144
.LCFI19:
	str	r0, [fp, #-96]
	str	r1, [fp, #-100]
	.loc 1 627 0
	mov	r3, #0
	str	r3, [fp, #-36]
	.loc 1 629 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 630 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 632 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 634 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 636 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 638 0
	mov	r3, #1
	str	r3, [fp, #-28]
	.loc 1 640 0
	mov	r3, #0
	str	r3, [fp, #-32]
	.loc 1 643 0
	mov	r0, #1
	bl	__move_scroll_box_marker
	.loc 1 646 0
	ldr	r3, .L82
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 649 0
	ldr	r0, .L82+4
	mov	r1, #0
	mov	r2, #18432
	bl	memset
	.loc 1 652 0
	ldr	r3, [fp, #-100]
	sub	r2, r3, #1
	ldr	r3, .L82+8
	str	r2, [r3, #0]
	.loc 1 653 0
	ldr	r3, [fp, #-100]
	add	r3, r3, #10
	str	r3, [fp, #-100]
	.loc 1 657 0
	ldr	r3, .L82+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L51
	.loc 1 657 0 is_stmt 0 discriminator 1
	ldr	r3, .L82+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L51
	.loc 1 659 0 is_stmt 1
	ldr	r3, .L82+8
	mov	r2, #1
	str	r2, [r3, #4]
.L51:
	.loc 1 667 0
	ldr	r3, .L82+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L52
	.loc 1 669 0
	ldr	r3, .L82+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L53
	.loc 1 673 0
	mov	r3, #0
	str	r3, [fp, #-40]
	b	.L54
.L57:
	.loc 1 675 0
	ldr	r1, .L82+24
	ldr	r2, [fp, #-40]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L82+28
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L55
	.loc 1 676 0 discriminator 1
	ldr	r0, .L82+24
	ldr	r2, [fp, #-40]
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L82+28
	ldr	r3, [r3, #4]
	.loc 1 675 0 discriminator 1
	cmp	r2, r3
	bne	.L55
	.loc 1 678 0
	ldr	r3, [fp, #-40]
	str	r3, [fp, #-36]
	.loc 1 680 0
	mov	r0, r0	@ nop
	b	.L52
.L55:
	.loc 1 673 0
	ldr	r3, [fp, #-40]
	add	r3, r3, #1
	str	r3, [fp, #-40]
.L54:
	.loc 1 673 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-40]
	ldr	r3, .L82+32
	cmp	r2, r3
	bls	.L57
	.loc 1 673 0
	b	.L52
.L53:
	.loc 1 689 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-40]
	b	.L58
.L61:
	.loc 1 694 0
	ldr	r1, .L82+24
	ldr	r2, [fp, #-40]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L82+28
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcc	.L59
	.loc 1 695 0 discriminator 1
	ldr	r1, .L82+24
	ldr	r2, [fp, #-40]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L82+28
	ldr	r3, [r3, #0]
	.loc 1 694 0 discriminator 1
	cmp	r2, r3
	bne	.L52
	.loc 1 696 0
	ldr	r0, .L82+24
	ldr	r2, [fp, #-40]
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L82+28
	ldr	r3, [r3, #4]
	.loc 1 695 0
	cmp	r2, r3
	bhi	.L52
.L59:
	.loc 1 698 0
	ldr	r0, .L82+24
	ldr	r2, [fp, #-40]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L60
	.loc 1 700 0
	ldr	r3, [fp, #-40]
	str	r3, [fp, #-36]
.L60:
	.loc 1 689 0
	ldr	r3, [fp, #-40]
	add	r3, r3, #1
	str	r3, [fp, #-40]
.L58:
	.loc 1 689 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-40]
	ldr	r3, .L82+32
	cmp	r2, r3
	bls	.L61
.L52:
	.loc 1 712 0 is_stmt 1
	ldr	r3, .L82+8
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 714 0
	ldr	r3, .L82+8
	ldr	r3, [r3, #0]
	rsb	r3, r3, #212
	mov	r2, r3, lsr #1
	ldr	r3, .L82+36
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #2
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fmrs	r0, s15
	bl	floorf
	fmsr	s15, r0
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	ldr	r3, .L82+8
	str	r2, [r3, #8]
	.loc 1 716 0
	ldr	r0, .L82+40
	mov	r1, #255
	mov	r2, #245760
	bl	memset
	.loc 1 718 0
	mov	r3, #0
	str	r3, [fp, #-40]
	b	.L62
.L73:
	.loc 1 720 0
	ldr	r0, .L82+24
	ldr	r2, [fp, #-40]
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L63
	.loc 1 722 0
	ldr	r3, [fp, #-24]
	cmp	r3, #1
	bne	.L64
	.loc 1 728 0
	ldr	r3, .L82+44
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L82+48
	mov	r3, #728
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 731 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #1
	str	r3, [fp, #-32]
	.loc 1 733 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L65
	.loc 1 733 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L65
	.loc 1 735 0 is_stmt 1
	ldr	r1, .L82+24
	ldr	r2, [fp, #-40]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, .L82+52
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	.loc 1 737 0
	ldr	r1, .L82+24
	ldr	r2, [fp, #-40]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	.loc 1 741 0
	bl	GuiLib_Refresh
	b	.L66
.L65:
	.loc 1 745 0
	ldr	r1, .L82+24
	ldr	r2, [fp, #-40]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r2, [r3, #0]
	sub	r3, fp, #92
	mov	r0, r2
	mov	r1, r3
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	.loc 1 747 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	sub	r3, r3, #14
	mvn	r2, #0
	str	r2, [sp, #0]
	sub	r2, fp, #92
	str	r2, [sp, #4]
	mov	r2, #1
	str	r2, [sp, #8]
	mov	r2, #2
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	mov	r2, #0
	str	r2, [sp, #20]
	mov	r2, #300
	str	r2, [sp, #24]
	mov	r2, #0
	str	r2, [sp, #28]
	mov	r2, #0
	str	r2, [sp, #32]
	mov	r2, #0
	str	r2, [sp, #36]
	mov	r2, #0
	str	r2, [sp, #40]
	mov	r2, #12
	str	r2, [sp, #44]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #14
	mov	r3, #1
	bl	CS_DrawStr
	str	r0, [fp, #-28]
	.loc 1 749 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L66:
	.loc 1 754 0
	ldr	r3, .L82+44
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 758 0
	mov	r3, #0
	str	r3, [fp, #-24]
.L64:
	.loc 1 765 0
	ldr	r3, .L82+8
	ldr	r2, [r3, #12]
	ldr	r3, .L82+8
	ldr	r1, [r3, #12]
	ldr	r0, .L82+4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	str	r1, [r3, #0]
	.loc 1 766 0
	ldr	r3, .L82+8
	ldr	r2, [r3, #12]
	ldr	r0, .L82+4
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [fp, #-40]
	str	r2, [r3, #0]
	.loc 1 767 0
	ldr	r3, .L82+8
	ldr	r2, [r3, #12]
	ldr	r0, .L82+4
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 768 0
	ldr	r3, .L82+8
	ldr	r2, [r3, #12]
	ldr	r0, .L82+4
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 769 0
	ldr	r3, .L82+8
	ldr	r2, [r3, #12]
	ldr	r1, [fp, #-12]
	mov	r3, r1
	mov	r3, r3, asl #5
	rsb	r1, r1, r3
	ldr	r3, [fp, #-96]
	add	r3, r1, r3
	sub	r1, r3, #31
	ldr	ip, .L82+4
	mov	r0, #16
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, ip, r3
	add	r3, r3, r0
	str	r1, [r3, #0]
	.loc 1 770 0
	ldr	r3, .L82+8
	ldr	r2, [r3, #12]
	ldr	r1, [fp, #-8]
	mov	r3, r1
	mov	r3, r3, asl #3
	rsb	r3, r1, r3
	mov	r3, r3, asl #1
	mov	r1, r3
	ldr	r3, [fp, #-100]
	add	r3, r1, r3
	sub	r1, r3, #1
	ldr	ip, .L82+4
	mov	r0, #20
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, ip, r3
	add	r3, r3, r0
	str	r1, [r3, #0]
	.loc 1 775 0
	ldr	r3, .L82+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L67
	.loc 1 775 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bne	.L67
	.loc 1 777 0 is_stmt 1
	ldr	r3, .L82+8
	ldr	r2, [r3, #12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L82+4
	add	r2, r2, r3
	ldr	r3, .L82+56
	str	r2, [r3, #0]
.L67:
	.loc 1 780 0
	ldr	r3, .L82+8
	ldr	r3, [r3, #12]
	add	r2, r3, #1
	ldr	r3, .L82+8
	str	r2, [r3, #12]
	.loc 1 784 0
	ldr	r2, [fp, #-40]
	ldr	r3, .L82+32
	cmp	r2, r3
	bhi	.L63
	.loc 1 786 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 788 0
	ldr	r3, [fp, #-40]
	add	r3, r3, #1
	str	r3, [fp, #-44]
	b	.L68
.L71:
	.loc 1 790 0
	ldr	r0, .L82+24
	ldr	r2, [fp, #-44]
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L69
	.loc 1 792 0
	ldr	r1, .L82+24
	ldr	r2, [fp, #-44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 793 0
	b	.L70
.L69:
	.loc 1 788 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #1
	str	r3, [fp, #-44]
.L68:
	.loc 1 788 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-44]
	ldr	r3, .L82+32
	cmp	r2, r3
	bls	.L71
.L70:
	.loc 1 797 0 is_stmt 1
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L72
	.loc 1 799 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 803 0
	ldr	r3, [fp, #-12]
	cmp	r3, #10
	bls	.L63
	.loc 1 805 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 806 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	b	.L63
.L72:
	.loc 1 811 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 813 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 817 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 819 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-16]
.L63:
	.loc 1 718 0
	ldr	r3, [fp, #-40]
	add	r3, r3, #1
	str	r3, [fp, #-40]
.L62:
	.loc 1 718 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-40]
	ldr	r3, .L82+32
	cmp	r2, r3
	bls	.L73
	.loc 1 829 0 is_stmt 1
	ldr	r3, .L82+8
	ldr	r2, [fp, #-8]
	str	r2, [r3, #16]
	.loc 1 831 0
	bl	__resize_scroll_box_marker
	.loc 1 833 0
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L74
	.loc 1 835 0
	ldr	r3, .L82+8
	ldr	r3, [r3, #0]
	str	r3, [fp, #-40]
	b	.L75
.L76:
	.loc 1 837 0 discriminator 2
	ldr	r2, [fp, #-40]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #5
	mov	r2, r3
	ldr	r3, .L82+60
	add	r1, r2, r3
	ldr	r3, .L82+8
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-40]
	rsb	r2, r3, r2
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #5
	add	r2, r3, #480
	ldr	r3, .L82+40
	add	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	mov	r2, #155
	bl	memcpy
	.loc 1 835 0 discriminator 2
	ldr	r3, [fp, #-40]
	add	r3, r3, #1
	str	r3, [fp, #-40]
.L75:
	.loc 1 835 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-40]
	cmp	r3, #212
	bls	.L76
	.loc 1 835 0
	b	.L77
.L74:
	.loc 1 842 0 is_stmt 1
	ldr	r3, .L82+8
	mov	r2, #0
	str	r2, [r3, #12]
.L77:
	.loc 1 845 0
	ldr	r3, .L82+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L78
	.loc 1 845 0 is_stmt 0 discriminator 1
	ldr	r3, .L82+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L78
	.loc 1 847 0 is_stmt 1
	ldr	r3, .L82+56
	mov	r2, #0
	str	r2, [r3, #0]
.L78:
	.loc 1 850 0
	mov	r3, #0
	str	r3, [fp, #-40]
	b	.L79
.L81:
	.loc 1 852 0
	ldr	r0, .L82+4
	ldr	r2, [fp, #-40]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L80
	.loc 1 852 0 is_stmt 0 discriminator 1
	ldr	r0, .L82+4
	ldr	r2, [fp, #-40]
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L80
	.loc 1 854 0 is_stmt 1
	ldr	r2, [fp, #-40]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L82+4
	add	r3, r2, r3
	mov	r0, r3
	bl	__draw_station_number_for_current_cursor
.L80:
	.loc 1 850 0
	ldr	r3, [fp, #-40]
	add	r3, r3, #1
	str	r3, [fp, #-40]
.L79:
	.loc 1 850 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-40]
	ldr	r3, .L82+32
	cmp	r2, r3
	bls	.L81
	.loc 1 858 0 is_stmt 1
	mov	r0, #0
	bl	__show_cursor
	.loc 1 860 0
	bl	GuiLib_Refresh
	.loc 1 861 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L83:
	.align	2
.L82:
	.word	g_CURSOR_last_station_selected
	.word	Group_CP
	.word	CURSOR_statistics
	.word	g_STATION_SELECTION_GRID_dialog_displayed
	.word	g_STATION_SELECTION_GRID_user_toggled_display
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.word	station_selection_struct
	.word	CURSOR_station_when_display_was_toggled
	.word	767
	.word	-1840700269
	.word	utility_display_buf
	.word	chain_members_recursive_MUTEX
	.word	.LC1
	.word	GuiVar_StationSelectionGridControllerName
	.word	CURSOR_current_cursor_ptr
	.word	primary_display_buf
.LFE6:
	.size	__FDTO_draw_all_cursor_positions, .-__FDTO_draw_all_cursor_positions
	.section .rodata
	.align	2
.LC4:
	.ascii	"CUR: move failed - NULL\000"
	.align	2
.LC5:
	.ascii	"CUR: move failed - out-of-range @ start\000"
	.align	2
.LC6:
	.ascii	"CUR: move failed - out-of-range @ end\000"
	.section	.text.__FDTO_move_cursor,"ax",%progbits
	.align	2
	.type	__FDTO_move_cursor, %function
__FDTO_move_cursor:
.LFB7:
	.loc 1 883 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI20:
	add	fp, sp, #4
.LCFI21:
	sub	sp, sp, #20
.LCFI22:
	str	r0, [fp, #-24]
	.loc 1 894 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 896 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 901 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L85
	.loc 1 903 0
	ldr	r3, [fp, #-24]
	cmp	r3, #4
	bne	.L86
	.loc 1 905 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #8]
	cmp	r3, #1
	beq	.L87
	.loc 1 907 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-16]
	.loc 1 910 0
	b	.L88
.L89:
	.loc 1 912 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #24
	str	r3, [fp, #-16]
	.loc 1 914 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
.L88:
	.loc 1 910 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #8]
	sub	r3, r3, #1
	cmp	r2, r3
	bhi	.L89
	.loc 1 919 0
	b	.L90
.L91:
	.loc 1 921 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #24
	str	r3, [fp, #-16]
	.loc 1 923 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
.L90:
	.loc 1 919 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	cmp	r2, r3
	bhi	.L91
	.loc 1 919 0 is_stmt 0
	b	.L87
.L86:
	.loc 1 927 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L92
	.loc 1 929 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #8]
	ldr	r3, .L107+4
	ldr	r3, [r3, #16]
	sub	r3, r3, #1
	cmp	r2, r3
	bhi	.L87
	.loc 1 931 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-16]
	.loc 1 934 0
	b	.L93
.L94:
	.loc 1 936 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #24
	str	r3, [fp, #-16]
	.loc 1 938 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L93:
	.loc 1 934 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #8]
	add	r3, r3, #1
	cmp	r2, r3
	bcc	.L94
	.loc 1 943 0
	b	.L95
.L96:
	.loc 1 945 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #24
	str	r3, [fp, #-16]
	.loc 1 947 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L95:
	.loc 1 943 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	cmp	r2, r3
	bcs	.L87
	.loc 1 943 0 is_stmt 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #24
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #8]
	cmp	r2, r3
	beq	.L96
	.loc 1 943 0
	b	.L87
.L92:
	.loc 1 951 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #1
	bne	.L97
	.loc 1 953 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L87
	.loc 1 955 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	b	.L87
.L97:
	.loc 1 960 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L107+4
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L87
	.loc 1 960 0 is_stmt 0 discriminator 1
	ldr	r3, .L107+4
	ldr	r3, [r3, #12]
	cmp	r3, #0
	beq	.L87
	.loc 1 962 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
.L87:
	.loc 1 967 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L98
	.loc 1 974 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	b	.L85
.L98:
	.loc 1 978 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 982 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L99
	.loc 1 982 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r3, .L107+8
	cmp	r2, r3
	bcc	.L99
	ldr	r2, [fp, #-12]
	ldr	r3, .L107+12
	cmp	r2, r3
	bcc	.L85
.L99:
	.loc 1 985 0 is_stmt 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L100
	.loc 1 987 0
	ldr	r0, .L107+16
	bl	Alert_Message
	b	.L101
.L100:
	.loc 1 989 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L107+8
	cmp	r2, r3
	bcs	.L102
	.loc 1 991 0
	ldr	r0, .L107+20
	bl	Alert_Message
	b	.L101
.L102:
	.loc 1 993 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L107+12
	cmp	r2, r3
	bcc	.L101
	.loc 1 995 0
	ldr	r0, .L107+24
	bl	Alert_Message
.L101:
	.loc 1 998 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
.L85:
	.loc 1 1004 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	cmp	r2, r3
	beq	.L103
	.loc 1 1006 0
	bl	good_key_beep
	.loc 1 1010 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 1011 0
	ldr	r3, .L107
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 1013 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #8]
	ldr	r3, .L107+4
	ldr	r1, [r3, #4]
	ldr	r3, .L107+4
	ldr	r3, [r3, #8]
	add	r3, r1, r3
	sub	r3, r3, #1
	cmp	r2, r3
	bls	.L104
	.loc 1 1016 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #8]
	ldr	r3, .L107+4
	ldr	r3, [r3, #8]
	rsb	r3, r3, r2
	add	r2, r3, #1
	ldr	r3, .L107+4
	str	r2, [r3, #4]
	b	.L105
.L104:
	.loc 1 1018 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #8]
	ldr	r3, .L107+4
	ldr	r3, [r3, #4]
	cmp	r2, r3
	bcs	.L105
	.loc 1 1021 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #8]
	ldr	r3, .L107+4
	str	r2, [r3, #4]
.L105:
	.loc 1 1026 0
	bl	__draw_group_name_and_controller_name
	.loc 1 1030 0
	ldr	r0, [fp, #-20]
	bl	__draw_station_number_for_current_cursor
	.loc 1 1031 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	__draw_station_number_for_current_cursor
	.loc 1 1033 0
	mov	r0, #0
	bl	__move_scroll_box_marker
	.loc 1 1035 0
	ldr	r3, .L107+28
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r0, .L107+32
	mov	r1, r3
	mov	r2, #0
	bl	GuiLib_ShowScreen
	.loc 1 1036 0
	bl	GuiLib_Refresh
	b	.L84
.L103:
	.loc 1 1040 0
	bl	bad_key_beep
.L84:
	.loc 1 1042 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L108:
	.align	2
.L107:
	.word	CURSOR_current_cursor_ptr
	.word	CURSOR_statistics
	.word	Group_CP
	.word	Group_CP+18432
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	GuiLib_ActiveCursorFieldNo
	.word	858
.LFE7:
	.size	__FDTO_move_cursor, .-__FDTO_move_cursor
	.section	.text.__FDTO_process_cursor_position,"ax",%progbits
	.align	2
	.type	__FDTO_process_cursor_position, %function
__FDTO_process_cursor_position:
.LFB8:
	.loc 1 1067 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI23:
	add	fp, sp, #4
.LCFI24:
	sub	sp, sp, #8
.LCFI25:
	str	r0, [fp, #-12]
	.loc 1 1073 0
	ldr	r3, .L128
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L110
	.loc 1 1075 0
	ldr	r3, .L128+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L128+8
	ldr	r3, .L128+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1077 0
	ldr	r3, .L128
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #4]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L128+16
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 1079 0
	ldr	r3, [fp, #-12]
	cmp	r3, #80
	beq	.L113
	cmp	r3, #84
	beq	.L114
	cmp	r3, #2
	bne	.L127
.L112:
	.loc 1 1082 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #8]
	.loc 1 1084 0
	ldr	r3, .L128
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L128+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L115
	.loc 1 1086 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1092 0
	b	.L117
.L115:
	.loc 1 1090 0
	bl	good_key_beep
	.loc 1 1092 0
	b	.L117
.L114:
	.loc 1 1095 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L118
	.loc 1 1097 0
	ldr	r3, .L128
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L128+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L119
	.loc 1 1099 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1121 0
	b	.L117
.L119:
	.loc 1 1104 0
	bl	bad_key_beep
	.loc 1 1121 0
	b	.L117
.L118:
	.loc 1 1110 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #8]
	.loc 1 1112 0
	ldr	r3, .L128
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L128+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L121
	.loc 1 1114 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1121 0
	b	.L117
.L121:
	.loc 1 1118 0
	bl	good_key_beep
	.loc 1 1121 0
	b	.L117
.L113:
	.loc 1 1125 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 1127 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	bne	.L122
	.loc 1 1129 0
	ldr	r3, .L128
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L128+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L123
	.loc 1 1131 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1150 0
	b	.L117
.L123:
	.loc 1 1136 0
	bl	bad_key_beep
	.loc 1 1150 0
	b	.L117
.L122:
	.loc 1 1141 0
	ldr	r3, .L128
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L128+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L125
	.loc 1 1143 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1150 0
	b	.L117
.L125:
	.loc 1 1147 0
	bl	good_key_beep
	.loc 1 1150 0
	b	.L117
.L127:
	.loc 1 1153 0
	bl	bad_key_beep
.L117:
	.loc 1 1157 0
	ldr	r3, .L128
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	__draw_station_number_for_current_cursor
	.loc 1 1159 0
	ldr	r3, .L128+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L109
.L110:
	.loc 1 1163 0
	bl	bad_key_beep
.L109:
	.loc 1 1165 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L129:
	.align	2
.L128:
	.word	CURSOR_current_cursor_ptr
	.word	list_program_data_recursive_MUTEX
	.word	.LC1
	.word	1075
	.word	station_selection_struct
	.word	CURSOR_statistics
.LFE8:
	.size	__FDTO_process_cursor_position, .-__FDTO_process_cursor_position
	.section	.text.__FDTO_process_manual_program_cursor_position,"ax",%progbits
	.align	2
	.type	__FDTO_process_manual_program_cursor_position, %function
__FDTO_process_manual_program_cursor_position:
.LFB9:
	.loc 1 1169 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	sub	sp, sp, #12
.LCFI28:
	str	r0, [fp, #-16]
	.loc 1 1177 0
	ldr	r3, .L153
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L131
	.loc 1 1179 0
	ldr	r3, .L153+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L153+8
	ldr	r3, .L153+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1181 0
	ldr	r3, .L153
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #4]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L153+16
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 1183 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	mov	r0, r2
	mov	r1, r3
	bl	nm_STATION_get_pointer_to_station
	str	r0, [fp, #-12]
	.loc 1 1185 0
	ldr	r3, [fp, #-16]
	cmp	r3, #80
	beq	.L134
	cmp	r3, #84
	beq	.L135
	cmp	r3, #2
	bne	.L152
.L133:
	.loc 1 1188 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L136
	.loc 1 1190 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 1192 0
	ldr	r3, .L153
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L153+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L137
	.loc 1 1194 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1228 0
	b	.L141
.L137:
	.loc 1 1198 0
	bl	good_key_beep
	.loc 1 1228 0
	b	.L141
.L136:
	.loc 1 1203 0
	ldr	r0, [fp, #-12]
	bl	nm_STATION_get_number_of_manual_programs_station_is_assigned_to
	mov	r3, r0
	cmp	r3, #1
	bls	.L139
	.loc 1 1204 0 discriminator 1
	ldr	r0, [fp, #-12]
	bl	STATION_get_GID_manual_program_A
	mov	r2, r0
	ldr	r3, .L153+24
	ldr	r3, [r3, #0]
	.loc 1 1203 0 discriminator 1
	cmp	r2, r3
	beq	.L139
	.loc 1 1205 0
	ldr	r0, [fp, #-12]
	bl	STATION_get_GID_manual_program_B
	mov	r2, r0
	ldr	r3, .L153+24
	ldr	r3, [r3, #0]
	.loc 1 1204 0
	cmp	r2, r3
	beq	.L139
	.loc 1 1207 0
	bl	bad_key_beep
	.loc 1 1209 0
	ldr	r0, .L153+28
	bl	DIALOG_draw_ok_dialog
	.loc 1 1211 0
	ldr	r3, .L153+32
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1228 0
	b	.L141
.L139:
	.loc 1 1216 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #8]
	.loc 1 1218 0
	ldr	r3, .L153
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L153+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L140
	.loc 1 1220 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1228 0
	b	.L141
.L140:
	.loc 1 1224 0
	bl	good_key_beep
	.loc 1 1228 0
	b	.L141
.L135:
	.loc 1 1231 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	cmp	r3, #1
	bne	.L142
	.loc 1 1233 0
	ldr	r3, .L153
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L153+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L143
	.loc 1 1235 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1267 0
	b	.L141
.L143:
	.loc 1 1240 0
	bl	bad_key_beep
	.loc 1 1267 0
	b	.L141
.L142:
	.loc 1 1243 0
	ldr	r0, [fp, #-12]
	bl	nm_STATION_get_number_of_manual_programs_station_is_assigned_to
	mov	r3, r0
	cmp	r3, #1
	bls	.L145
	.loc 1 1244 0 discriminator 1
	ldr	r0, [fp, #-12]
	bl	STATION_get_GID_manual_program_A
	mov	r2, r0
	ldr	r3, .L153+24
	ldr	r3, [r3, #0]
	.loc 1 1243 0 discriminator 1
	cmp	r2, r3
	beq	.L145
	.loc 1 1245 0
	ldr	r0, [fp, #-12]
	bl	STATION_get_GID_manual_program_B
	mov	r2, r0
	ldr	r3, .L153+24
	ldr	r3, [r3, #0]
	.loc 1 1244 0
	cmp	r2, r3
	beq	.L145
	.loc 1 1247 0
	bl	bad_key_beep
	.loc 1 1249 0
	ldr	r0, .L153+28
	bl	DIALOG_draw_ok_dialog
	.loc 1 1251 0
	ldr	r3, .L153+32
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1267 0
	b	.L141
.L145:
	.loc 1 1256 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #8]
	.loc 1 1258 0
	ldr	r3, .L153
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L153+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L146
	.loc 1 1260 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1267 0
	b	.L141
.L146:
	.loc 1 1264 0
	bl	good_key_beep
	.loc 1 1267 0
	b	.L141
.L134:
	.loc 1 1270 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	bne	.L147
	.loc 1 1272 0
	ldr	r3, .L153
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L153+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L148
	.loc 1 1274 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1296 0
	b	.L141
.L148:
	.loc 1 1279 0
	bl	bad_key_beep
	.loc 1 1296 0
	b	.L141
.L147:
	.loc 1 1285 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 1287 0
	ldr	r3, .L153
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L153+20
	ldr	r3, [r3, #12]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L150
	.loc 1 1289 0
	mov	r0, #3
	bl	__FDTO_move_cursor
	.loc 1 1296 0
	b	.L141
.L150:
	.loc 1 1293 0
	bl	good_key_beep
	.loc 1 1296 0
	b	.L141
.L152:
	.loc 1 1299 0
	bl	bad_key_beep
.L141:
	.loc 1 1303 0
	ldr	r3, .L153
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	__draw_station_number_for_current_cursor
	.loc 1 1305 0
	ldr	r3, .L153+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L130
.L131:
	.loc 1 1309 0
	bl	bad_key_beep
.L130:
	.loc 1 1311 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L154:
	.align	2
.L153:
	.word	CURSOR_current_cursor_ptr
	.word	list_program_data_recursive_MUTEX
	.word	.LC1
	.word	1179
	.word	station_selection_struct
	.word	CURSOR_statistics
	.word	g_GROUP_ID
	.word	623
	.word	g_STATION_SELECTION_GRID_dialog_displayed
.LFE9:
	.size	__FDTO_process_manual_program_cursor_position, .-__FDTO_process_manual_program_cursor_position
	.section .rodata
	.align	2
.LC7:
	.ascii	"Station Selected\000"
	.section	.text.STATION_SELECTION_GRID_station_is_selected,"ax",%progbits
	.align	2
	.global	STATION_SELECTION_GRID_station_is_selected
	.type	STATION_SELECTION_GRID_station_is_selected, %function
STATION_SELECTION_GRID_station_is_selected:
.LFB10:
	.loc 1 1337 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI29:
	add	fp, sp, #4
.LCFI30:
	sub	sp, sp, #16
.LCFI31:
	str	r0, [fp, #-12]
	.loc 1 1340 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1342 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L158
	cmp	r2, r3
	bhi	.L156
	.loc 1 1344 0
	ldr	r0, .L158+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L157
.L156:
	.loc 1 1348 0
	ldr	r0, .L158+8
	ldr	r1, .L158+12
	bl	Alert_index_out_of_range_with_filename
.L157:
	.loc 1 1351 0
	sub	r3, fp, #8
	ldr	r2, .L158+8
	str	r2, [sp, #0]
	ldr	r2, .L158+16
	str	r2, [sp, #4]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	ldr	r3, .L158+20
	bl	RC_bool_with_filename
	.loc 1 1353 0
	ldr	r3, [fp, #-8]
	.loc 1 1354 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L159:
	.align	2
.L158:
	.word	767
	.word	station_selection_struct
	.word	.LC1
	.word	1348
	.word	1351
	.word	.LC7
.LFE10:
	.size	STATION_SELECTION_GRID_station_is_selected, .-STATION_SELECTION_GRID_station_is_selected
	.section	.text.STATION_SELECTION_GRID_get_count_of_selected_stations,"ax",%progbits
	.align	2
	.global	STATION_SELECTION_GRID_get_count_of_selected_stations
	.type	STATION_SELECTION_GRID_get_count_of_selected_stations, %function
STATION_SELECTION_GRID_get_count_of_selected_stations:
.LFB11:
	.loc 1 1358 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI32:
	add	fp, sp, #0
.LCFI33:
	sub	sp, sp, #8
.LCFI34:
	.loc 1 1363 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 1365 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L161
.L163:
	.loc 1 1367 0
	ldr	r0, .L164
	ldr	r2, [fp, #-8]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L162
	.loc 1 1369 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L162:
	.loc 1 1365 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L161:
	.loc 1 1365 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, .L164+4
	cmp	r2, r3
	bls	.L163
	.loc 1 1373 0 is_stmt 1
	ldr	r3, [fp, #-4]
	.loc 1 1374 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L165:
	.align	2
.L164:
	.word	station_selection_struct
	.word	767
.LFE11:
	.size	STATION_SELECTION_GRID_get_count_of_selected_stations, .-STATION_SELECTION_GRID_get_count_of_selected_stations
	.section	.text.STATION_SELECTION_GRID_populate_cursors,"ax",%progbits
	.align	2
	.global	STATION_SELECTION_GRID_populate_cursors
	.type	STATION_SELECTION_GRID_populate_cursors, %function
STATION_SELECTION_GRID_populate_cursors:
.LFB12:
	.loc 1 1406 0
	@ args = 8, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI35:
	add	fp, sp, #4
.LCFI36:
	sub	sp, sp, #28
.LCFI37:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	str	r3, [fp, #-32]
	.loc 1 1417 0
	ldr	r3, [fp, #-32]
	cmp	r3, #1
	bne	.L167
	.loc 1 1421 0
	ldr	r0, .L187
	mov	r1, #0
	mov	r2, #768
	bl	memset
.L167:
	.loc 1 1426 0
	ldr	r3, .L187+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L187+8
	ldr	r3, .L187+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1428 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L168
	.loc 1 1430 0
	ldr	r0, .L187+16
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 1432 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L169
.L183:
	.loc 1 1434 0
	ldr	r0, [fp, #-8]
	bl	nm_STATION_get_box_index_0
	mov	r1, r0
	ldr	r0, .L187
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	str	r1, [r3, #0]
	.loc 1 1436 0
	ldr	r0, [fp, #-8]
	bl	nm_STATION_get_station_number_0
	mov	r1, r0
	ldr	ip, .L187
	ldr	r2, [fp, #-16]
	mov	r0, #4
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	str	r1, [r3, #0]
	.loc 1 1438 0
	ldr	r0, [fp, #-8]
	bl	nm_STATION_get_description
	mov	r1, r0
	ldr	ip, .L187
	ldr	r2, [fp, #-16]
	mov	r0, #16
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	str	r1, [r3, #0]
	.loc 1 1441 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L170
	.loc 1 1441 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	ldr	r0, [fp, #-8]
	blx	r3
	mov	r2, r0
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bne	.L170
	.loc 1 1443 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L171
.L170:
	.loc 1 1445 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L172
	.loc 1 1445 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldr	r0, [fp, #-8]
	blx	r3
	mov	r2, r0
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bne	.L172
	.loc 1 1447 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L171
.L172:
	.loc 1 1451 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L171:
	.loc 1 1457 0
	ldr	r3, [fp, #-32]
	cmp	r3, #1
	bne	.L173
	.loc 1 1459 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L174
	.loc 1 1459 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-8]
	bl	STATION_station_is_available_for_use
	mov	r3, r0
	cmp	r3, #0
	beq	.L174
	mov	r3, #1
	b	.L175
.L174:
	.loc 1 1459 0 discriminator 2
	mov	r3, #0
.L175:
	.loc 1 1459 0 discriminator 3
	mov	r1, r3
	ldr	ip, .L187
	ldr	r2, [fp, #-16]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	str	r1, [r3, #0]
.L173:
	.loc 1 1464 0 is_stmt 1
	ldr	r3, [fp, #4]
	cmp	r3, #0
	bne	.L176
	.loc 1 1464 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-8]
	bl	nm_STATION_get_physically_available
	mov	r3, r0
	cmp	r3, #1
	beq	.L177
.L176:
	.loc 1 1464 0 discriminator 2
	ldr	r3, [fp, #4]
	cmp	r3, #1
	bne	.L178
	.loc 1 1465 0 is_stmt 1
	ldr	r0, [fp, #-8]
	bl	STATION_station_is_available_for_use
	mov	r3, r0
	cmp	r3, #1
	bne	.L178
.L177:
	.loc 1 1467 0
	ldr	r3, [fp, #8]
	cmp	r3, #0
	beq	.L179
	.loc 1 1467 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #8]
	cmp	r3, #1
	bne	.L180
	.loc 1 1468 0 is_stmt 1
	ldr	r0, .L187
	ldr	r2, [fp, #-16]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L180
.L179:
	.loc 1 1470 0
	ldr	r0, .L187
	ldr	r2, [fp, #-16]
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1467 0
	b	.L182
.L180:
	.loc 1 1474 0
	ldr	r0, .L187
	ldr	r2, [fp, #-16]
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1467 0
	b	.L182
.L178:
	.loc 1 1479 0
	ldr	r0, .L187
	ldr	r2, [fp, #-16]
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
.L182:
	.loc 1 1482 0
	ldr	r0, .L187+16
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
	.loc 1 1432 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L169:
	.loc 1 1432 0 is_stmt 0 discriminator 1
	ldr	r3, .L187+16
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bhi	.L183
	.loc 1 1487 0 is_stmt 1
	b	.L184
.L185:
	.loc 1 1489 0 discriminator 2
	ldr	r0, .L187
	ldr	r2, [fp, #-16]
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1487 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L184:
	.loc 1 1487 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-16]
	ldr	r3, .L187+20
	cmp	r2, r3
	bls	.L185
	.loc 1 1487 0
	b	.L186
.L168:
	.loc 1 1494 0 is_stmt 1
	ldr	r0, .L187+8
	ldr	r1, .L187+24
	bl	Alert_func_call_with_null_ptr_with_filename
.L186:
	.loc 1 1497 0
	ldr	r3, .L187+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1498 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L188:
	.align	2
.L187:
	.word	station_selection_struct
	.word	list_program_data_recursive_MUTEX
	.word	.LC1
	.word	1426
	.word	station_info_list_hdr
	.word	767
	.word	1494
.LFE12:
	.size	STATION_SELECTION_GRID_populate_cursors, .-STATION_SELECTION_GRID_populate_cursors
	.section	.text.FDTO_STATION_SELECTION_GRID_redraw_calling_screen,"ax",%progbits
	.align	2
	.global	FDTO_STATION_SELECTION_GRID_redraw_calling_screen
	.type	FDTO_STATION_SELECTION_GRID_redraw_calling_screen, %function
FDTO_STATION_SELECTION_GRID_redraw_calling_screen:
.LFB13:
	.loc 1 1502 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI38:
	add	fp, sp, #4
.LCFI39:
	sub	sp, sp, #8
.LCFI40:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1503 0
	ldr	r3, .L190
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1505 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1507 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L190+4
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1509 0
	mov	r0, #0
	bl	FDTO_Redraw_Screen
	.loc 1 1510 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L191:
	.align	2
.L190:
	.word	g_STATION_SELECTION_GRID_user_pressed_back
	.word	GuiLib_ActiveCursorFieldNo
.LFE13:
	.size	FDTO_STATION_SELECTION_GRID_redraw_calling_screen, .-FDTO_STATION_SELECTION_GRID_redraw_calling_screen
	.section .rodata
	.align	2
.LC8:
	.ascii	"%s %s %s %s %s\000"
	.align	2
.LC9:
	.ascii	"%s %s %s\000"
	.section	.text.draw_station_selection_grid_title,"ax",%progbits
	.align	2
	.type	draw_station_selection_grid_title, %function
draw_station_selection_grid_title:
.LFB14:
	.loc 1 1514 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, fp, lr}
.LCFI41:
	add	fp, sp, #20
.LCFI42:
	sub	sp, sp, #16
.LCFI43:
	.loc 1 1515 0
	ldr	r3, .L210
	ldr	r3, [r3, #0]
	cmp	r3, #7
	beq	.L195
	cmp	r3, #11
	beq	.L196
	cmp	r3, #1
	bne	.L192
.L194:
	.loc 1 1518 0
	ldr	r3, .L210+4
	ldr	r3, [r3, #0]
	cmp	r3, #20
	beq	.L198
	cmp	r3, #25
	beq	.L199
	b	.L197
.L198:
	.loc 1 1521 0
	ldr	r0, .L210+8
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r4, r0
	ldr	r0, .L210+12
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r7, r0
	ldr	r0, .L210+16
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r6, r0
	ldr	r0, .L210+12
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r5, r0
	.loc 1 1524 0
	ldr	r3, .L210+20
	ldr	r3, [r3, #0]
	.loc 1 1521 0
	cmp	r3, #0
	beq	.L200
	.loc 1 1521 0 is_stmt 0 discriminator 1
	ldr	r0, .L210+24
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	b	.L201
.L200:
	.loc 1 1521 0 discriminator 2
	ldr	r0, .L210+28
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
.L201:
	.loc 1 1521 0 discriminator 3
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r3, [sp, #12]
	ldr	r0, .L210+32
	mov	r1, #129
	ldr	r2, .L210+36
	mov	r3, r4
	bl	snprintf
	.loc 1 1525 0 is_stmt 1 discriminator 3
	b	.L197
.L199:
	.loc 1 1528 0
	ldr	r0, .L210+8
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r4, r0
	ldr	r0, .L210+12
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r7, r0
	mov	r0, #980
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r6, r0
	ldr	r0, .L210+12
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r5, r0
	.loc 1 1531 0
	ldr	r3, .L210+20
	ldr	r3, [r3, #0]
	.loc 1 1528 0
	cmp	r3, #0
	beq	.L202
	.loc 1 1528 0 is_stmt 0 discriminator 1
	ldr	r0, .L210+24
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	b	.L203
.L202:
	.loc 1 1528 0 discriminator 2
	ldr	r0, .L210+28
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
.L203:
	.loc 1 1528 0 discriminator 3
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r3, [sp, #12]
	ldr	r0, .L210+32
	mov	r1, #129
	ldr	r2, .L210+36
	mov	r3, r4
	bl	snprintf
	.loc 1 1532 0 is_stmt 1
	mov	r0, r0	@ nop
.L197:
	.loc 1 1534 0
	b	.L192
.L195:
	.loc 1 1537 0
	ldr	r3, .L210+4
	ldr	r3, [r3, #0]
	cmp	r3, #22
	bne	.L204
.L205:
	.loc 1 1540 0
	ldr	r0, .L210+8
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r4, r0
	ldr	r0, .L210+12
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r7, r0
	mov	r0, #980
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r6, r0
	ldr	r0, .L210+12
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r5, r0
	.loc 1 1543 0
	ldr	r3, .L210+20
	ldr	r3, [r3, #0]
	.loc 1 1540 0
	cmp	r3, #0
	beq	.L206
	.loc 1 1540 0 is_stmt 0 discriminator 1
	ldr	r0, .L210+24
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	b	.L207
.L206:
	.loc 1 1540 0 discriminator 2
	ldr	r0, .L210+28
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
.L207:
	.loc 1 1540 0 discriminator 3
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r3, [sp, #12]
	ldr	r0, .L210+32
	mov	r1, #129
	ldr	r2, .L210+36
	mov	r3, r4
	bl	snprintf
	.loc 1 1544 0 is_stmt 1
	mov	r0, r0	@ nop
.L204:
	.loc 1 1546 0
	b	.L192
.L196:
	.loc 1 1549 0
	ldr	r3, .L210+4
	ldr	r3, [r3, #0]
	cmp	r3, #20
	bne	.L208
.L209:
	.loc 1 1552 0
	ldr	r0, .L210+40
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r4, r0
	ldr	r0, .L210+12
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r5, r0
	ldr	r0, .L210+44
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	str	r5, [sp, #0]
	str	r3, [sp, #4]
	ldr	r0, .L210+32
	mov	r1, #129
	ldr	r2, .L210+48
	mov	r3, r4
	bl	snprintf
	.loc 1 1555 0
	mov	r0, r0	@ nop
.L208:
	.loc 1 1557 0
	mov	r0, r0	@ nop
.L192:
	.loc 1 1560 0
	sub	sp, fp, #20
	ldmfd	sp!, {r4, r5, r6, r7, fp, pc}
.L211:
	.align	2
.L210:
	.word	g_MAIN_MENU_active_menu_item
	.word	GuiVar_MenuScreenToShow
	.word	1045
	.word	865
	.word	1066
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.word	1102
	.word	875
	.word	GuiVar_StationSelectionGridScreenTitle
	.word	.LC8
	.word	1051
	.word	1070
	.word	.LC9
.LFE14:
	.size	draw_station_selection_grid_title, .-draw_station_selection_grid_title
	.section	.text.STATION_SELECTION_GRID_save_changes,"ax",%progbits
	.align	2
	.type	STATION_SELECTION_GRID_save_changes, %function
STATION_SELECTION_GRID_save_changes:
.LFB15:
	.loc 1 1564 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI44:
	add	fp, sp, #4
.LCFI45:
	sub	sp, sp, #20
.LCFI46:
	.loc 1 1571 0
	bl	FLOWSENSE_get_controller_index
	str	r0, [fp, #-8]
	.loc 1 1576 0
	ldr	r3, .L228
	ldr	r3, [r3, #0]
	cmp	r3, #7
	beq	.L215
	cmp	r3, #11
	beq	.L216
	cmp	r3, #1
	bne	.L212
.L214:
	.loc 1 1579 0
	ldr	r3, .L228+4
	ldr	r3, [r3, #0]
	cmp	r3, #20
	beq	.L218
	cmp	r3, #25
	beq	.L219
	b	.L217
.L218:
	.loc 1 1582 0
	ldr	r3, .L228+8
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r2, #2
	str	r2, [sp, #8]
	ldr	r0, .L228+12
	ldr	r1, .L228+16
	mov	r2, r3
	mov	r3, #2
	bl	STATION_store_group_assignment_changes
	.loc 1 1583 0
	b	.L217
.L219:
	.loc 1 1586 0
	ldr	r3, .L228+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	str	r0, [fp, #-12]
	.loc 1 1588 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L226
	.loc 1 1590 0
	ldr	r3, .L228+8
	ldr	r3, [r3, #0]
	mov	r2, #2
	str	r2, [sp, #0]
	mov	r0, r3
	mov	r1, #2
	ldr	r2, [fp, #-8]
	mov	r3, #1
	bl	STATION_store_manual_programs_assignment_changes
.L226:
	.loc 1 1592 0
	mov	r0, r0	@ nop
.L217:
	.loc 1 1594 0
	b	.L212
.L215:
	.loc 1 1597 0
	ldr	r3, .L228+4
	ldr	r3, [r3, #0]
	cmp	r3, #22
	bne	.L221
.L222:
	.loc 1 1600 0
	ldr	r3, .L228+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	str	r0, [fp, #-12]
	.loc 1 1602 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L227
	.loc 1 1604 0
	ldr	r3, .L228+8
	ldr	r3, [r3, #0]
	mov	r2, #2
	str	r2, [sp, #0]
	mov	r0, r3
	mov	r1, #2
	ldr	r2, [fp, #-8]
	mov	r3, #1
	bl	STATION_store_manual_programs_assignment_changes
.L227:
	.loc 1 1606 0
	mov	r0, r0	@ nop
.L221:
	.loc 1 1608 0
	b	.L212
.L216:
	.loc 1 1611 0
	ldr	r3, .L228+4
	ldr	r3, [r3, #0]
	cmp	r3, #20
	bne	.L224
.L225:
	.loc 1 1614 0
	mov	r0, #2
	ldr	r1, [fp, #-8]
	mov	r2, #2
	bl	STATION_store_stations_in_use
	.loc 1 1615 0
	mov	r0, r0	@ nop
.L224:
	.loc 1 1617 0
	mov	r0, r0	@ nop
.L212:
	.loc 1 1619 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L229:
	.align	2
.L228:
	.word	g_MAIN_MENU_active_menu_item
	.word	GuiVar_MenuScreenToShow
	.word	g_GROUP_ID
	.word	STATION_get_GID_station_group
	.word	nm_STATION_set_GID_station_group
.LFE15:
	.size	STATION_SELECTION_GRID_save_changes, .-STATION_SELECTION_GRID_save_changes
	.section	.text.FDTO_STATION_SELECTION_GRID_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_STATION_SELECTION_GRID_draw_screen
	.type	FDTO_STATION_SELECTION_GRID_draw_screen, %function
FDTO_STATION_SELECTION_GRID_draw_screen:
.LFB16:
	.loc 1 1623 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI47:
	add	fp, sp, #4
.LCFI48:
	sub	sp, sp, #16
.LCFI49:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1628 0
	ldr	r3, .L245
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1630 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L231
	.loc 1 1632 0
	ldr	r3, .L245+4
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 1634 0
	ldr	r3, .L245+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1636 0
	ldr	r3, .L245+12
	mov	r2, #0
	str	r2, [r3, #0]
.L231:
	.loc 1 1639 0
	ldr	r3, .L245+16
	ldr	r3, [r3, #0]
	cmp	r3, #7
	beq	.L234
	cmp	r3, #11
	beq	.L235
	cmp	r3, #1
	bne	.L232
.L233:
	.loc 1 1642 0
	ldr	r3, .L245+20
	ldr	r3, [r3, #0]
	cmp	r3, #20
	beq	.L237
	cmp	r3, #25
	beq	.L238
	b	.L236
.L237:
	.loc 1 1645 0
	bl	draw_station_selection_grid_title
	.loc 1 1647 0
	ldr	r3, .L245+24
	ldr	r3, [r3, #0]
	ldr	r2, .L245+4
	ldr	r2, [r2, #0]
	mov	r1, #1
	str	r1, [sp, #0]
	str	r2, [sp, #4]
	ldr	r0, .L245+28
	mov	r1, #0
	mov	r2, r3
	ldr	r3, [fp, #-8]
	bl	STATION_SELECTION_GRID_populate_cursors
	.loc 1 1648 0
	b	.L236
.L238:
	.loc 1 1651 0
	bl	draw_station_selection_grid_title
	.loc 1 1653 0
	ldr	r3, .L245+24
	ldr	r3, [r3, #0]
	ldr	r2, .L245+4
	ldr	r2, [r2, #0]
	mov	r1, #1
	str	r1, [sp, #0]
	str	r2, [sp, #4]
	ldr	r0, .L245+32
	ldr	r1, .L245+36
	mov	r2, r3
	ldr	r3, [fp, #-8]
	bl	STATION_SELECTION_GRID_populate_cursors
	.loc 1 1654 0
	mov	r0, r0	@ nop
.L236:
	.loc 1 1656 0
	b	.L232
.L234:
	.loc 1 1659 0
	ldr	r3, .L245+20
	ldr	r3, [r3, #0]
	cmp	r3, #22
	bne	.L239
.L240:
	.loc 1 1662 0
	bl	draw_station_selection_grid_title
	.loc 1 1664 0
	ldr	r3, .L245+24
	ldr	r3, [r3, #0]
	ldr	r2, .L245+4
	ldr	r2, [r2, #0]
	mov	r1, #1
	str	r1, [sp, #0]
	str	r2, [sp, #4]
	ldr	r0, .L245+32
	ldr	r1, .L245+36
	mov	r2, r3
	ldr	r3, [fp, #-8]
	bl	STATION_SELECTION_GRID_populate_cursors
	.loc 1 1665 0
	mov	r0, r0	@ nop
.L239:
	.loc 1 1667 0
	b	.L232
.L235:
	.loc 1 1670 0
	ldr	r3, .L245+20
	ldr	r3, [r3, #0]
	cmp	r3, #20
	bne	.L241
.L242:
	.loc 1 1678 0
	ldr	r3, .L245+24
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1682 0
	ldr	r3, .L245+40
	mov	r2, #0
	strb	r2, [r3, #0]
	.loc 1 1684 0
	bl	draw_station_selection_grid_title
	.loc 1 1686 0
	ldr	r3, .L245+24
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	ldr	r0, .L245+44
	mov	r1, #0
	mov	r2, r3
	ldr	r3, [fp, #-8]
	bl	STATION_SELECTION_GRID_populate_cursors
	.loc 1 1687 0
	mov	r0, r0	@ nop
.L241:
	.loc 1 1689 0
	mov	r0, r0	@ nop
.L232:
	.loc 1 1695 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	beq	.L243
	.loc 1 1695 0 is_stmt 0 discriminator 1
	ldr	r3, .L245+12
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L244
.L243:
	.loc 1 1697 0 is_stmt 1
	mov	r0, #67
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
.L244:
	.loc 1 1700 0
	mov	r0, #24
	mov	r1, #48
	bl	__FDTO_draw_all_cursor_positions
	.loc 1 1702 0
	ldr	r3, .L245+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1704 0
	ldr	r3, .L245+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1705 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L246:
	.align	2
.L245:
	.word	g_STATION_SELECTION_GRID_user_pressed_back
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.word	g_STATION_SELECTION_GRID_user_toggled_display
	.word	g_STATION_SELECTION_GRID_dialog_displayed
	.word	g_MAIN_MENU_active_menu_item
	.word	GuiVar_MenuScreenToShow
	.word	g_GROUP_ID
	.word	STATION_get_GID_station_group
	.word	STATION_get_GID_manual_program_A
	.word	STATION_get_GID_manual_program_B
	.word	GuiVar_GroupName
	.word	STATION_station_is_available_for_use
.LFE16:
	.size	FDTO_STATION_SELECTION_GRID_draw_screen, .-FDTO_STATION_SELECTION_GRID_draw_screen
	.section	.text.STATION_SELECTION_GRID_process_screen,"ax",%progbits
	.align	2
	.global	STATION_SELECTION_GRID_process_screen
	.type	STATION_SELECTION_GRID_process_screen, %function
STATION_SELECTION_GRID_process_screen:
.LFB17:
	.loc 1 1709 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI50:
	add	fp, sp, #8
.LCFI51:
	sub	sp, sp, #48
.LCFI52:
	str	r0, [fp, #-52]
	str	r1, [fp, #-48]
	.loc 1 1714 0
	ldr	r3, [fp, #-52]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L248
.L253:
	.word	.L249
	.word	.L249
	.word	.L250
	.word	.L249
	.word	.L249
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L251
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L251
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L252
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L250
	.word	.L248
	.word	.L248
	.word	.L248
	.word	.L250
.L250:
	.loc 1 1719 0
	ldr	r3, .L274
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L254
	.loc 1 1721 0
	bl	bad_key_beep
	.loc 1 1738 0
	b	.L247
.L254:
	.loc 1 1723 0
	ldr	r3, .L274+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L256
	.loc 1 1723 0 is_stmt 0 discriminator 1
	ldr	r3, .L274+8
	ldr	r3, [r3, #0]
	cmp	r3, #25
	beq	.L257
.L256:
	.loc 1 1724 0 is_stmt 1 discriminator 2
	ldr	r3, .L274+4
	ldr	r3, [r3, #0]
	.loc 1 1723 0 discriminator 2
	cmp	r3, #7
	bne	.L258
	.loc 1 1724 0
	ldr	r3, .L274+8
	ldr	r3, [r3, #0]
	cmp	r3, #22
	bne	.L258
.L257:
	.loc 1 1726 0
	mov	r3, #2
	str	r3, [fp, #-44]
	.loc 1 1727 0
	ldr	r3, .L274+12
	str	r3, [fp, #-24]
	.loc 1 1728 0
	ldr	r3, [fp, #-52]
	str	r3, [fp, #-20]
	.loc 1 1729 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 1738 0
	b	.L247
.L258:
	.loc 1 1733 0
	mov	r3, #2
	str	r3, [fp, #-44]
	.loc 1 1734 0
	ldr	r3, .L274+16
	str	r3, [fp, #-24]
	.loc 1 1735 0
	ldr	r3, [fp, #-52]
	str	r3, [fp, #-20]
	.loc 1 1736 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 1738 0
	b	.L247
.L249:
	.loc 1 1744 0
	mov	r3, #2
	str	r3, [fp, #-44]
	.loc 1 1745 0
	ldr	r3, .L274+20
	str	r3, [fp, #-24]
	.loc 1 1746 0
	ldr	r3, [fp, #-52]
	str	r3, [fp, #-20]
	.loc 1 1747 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 1748 0
	b	.L247
.L251:
	.loc 1 1752 0
	bl	STATION_SELECTION_GRID_save_changes
	.loc 1 1754 0
	ldr	r3, .L274+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L261
	cmp	r3, #7
	beq	.L262
	b	.L272
.L261:
	.loc 1 1757 0
	ldr	r3, .L274+8
	ldr	r3, [r3, #0]
	cmp	r3, #20
	beq	.L264
	cmp	r3, #25
	beq	.L265
	b	.L263
.L264:
	.loc 1 1760 0
	ldr	r4, [fp, #-52]
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r2, r0
	ldr	r3, .L274+24
	ldr	r1, .L274+28
	str	r1, [sp, #0]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	ldr	r3, .L274+32
	bl	GROUP_process_NEXT_and_PREV
	.loc 1 1761 0
	b	.L263
.L265:
	.loc 1 1764 0
	ldr	r4, [fp, #-52]
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	mov	r2, r0
	ldr	r3, .L274+36
	ldr	r1, .L274+40
	str	r1, [sp, #0]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	ldr	r3, .L274+44
	bl	GROUP_process_NEXT_and_PREV
	.loc 1 1765 0
	mov	r0, r0	@ nop
.L263:
	.loc 1 1767 0
	b	.L266
.L262:
	.loc 1 1770 0
	ldr	r3, .L274+8
	ldr	r3, [r3, #0]
	cmp	r3, #22
	bne	.L267
.L268:
	.loc 1 1773 0
	ldr	r4, [fp, #-52]
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	mov	r2, r0
	ldr	r3, .L274+36
	ldr	r1, .L274+40
	str	r1, [sp, #0]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	ldr	r3, .L274+44
	bl	GROUP_process_NEXT_and_PREV
	.loc 1 1774 0
	mov	r0, r0	@ nop
.L267:
	.loc 1 1776 0
	b	.L266
.L272:
	.loc 1 1780 0
	bl	bad_key_beep
	.loc 1 1782 0
	b	.L247
.L266:
	b	.L247
.L252:
	.loc 1 1785 0
	ldr	r3, .L274+48
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1787 0
	bl	STATION_SELECTION_GRID_save_changes
	.loc 1 1792 0
	ldr	r3, .L274+4
	ldr	r3, [r3, #0]
	cmp	r3, #11
	bne	.L269
	.loc 1 1792 0 is_stmt 0 discriminator 1
	ldr	r3, .L274+8
	ldr	r3, [r3, #0]
	cmp	r3, #20
	bne	.L269
	.loc 1 1794 0 is_stmt 1
	ldr	r3, .L274+48
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1796 0
	ldr	r3, .L274+52
	ldr	r2, [r3, #0]
	ldr	r0, .L274+56
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L273
.L271:
	.loc 1 1799 0
	ldr	r3, .L274+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1800 0
	b	.L269
.L273:
	.loc 1 1803 0
	ldr	r3, .L274+8
	mov	r2, #11
	str	r2, [r3, #0]
.L269:
	.loc 1 1807 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 1808 0
	b	.L247
.L248:
	.loc 1 1811 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L247:
	.loc 1 1813 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L275:
	.align	2
.L274:
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.word	g_MAIN_MENU_active_menu_item
	.word	GuiVar_MenuScreenToShow
	.word	__FDTO_process_manual_program_cursor_position
	.word	__FDTO_process_cursor_position
	.word	__FDTO_move_cursor
	.word	STATION_GROUP_extract_and_store_changes_from_GuiVars
	.word	STATION_GROUP_copy_group_into_guivars
	.word	STATION_GROUP_get_group_at_this_index
	.word	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
	.word	MANUAL_PROGRAMS_get_group_at_this_index
	.word	g_STATION_SELECTION_GRID_user_pressed_back
	.word	screen_history_index
	.word	ScreenHistory
.LFE17:
	.size	STATION_SELECTION_GRID_process_screen, .-STATION_SELECTION_GRID_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI14-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI17-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI20-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI23-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI26-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI29-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI32-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI35-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI38-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI41-.LFB14
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI42-.LCFI41
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI44-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI45-.LCFI44
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI47-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI48-.LCFI47
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI50-.LFB17
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI51-.LCFI50
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/stations.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/manual_programs.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_station_selection_grid.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/m_main.h"
	.file 19 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/lcd_init.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xf2f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF179
	.byte	0x1
	.4byte	.LASF180
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x3a
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x55
	.4byte	0x6c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x5e
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x67
	.4byte	0x90
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x2
	.byte	0x99
	.4byte	0x7e
	.uleb128 0x5
	.byte	0x4
	.4byte	0xb6
	.uleb128 0x6
	.4byte	0xbd
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF16
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x3
	.byte	0x35
	.4byte	0x33
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x4
	.byte	0x57
	.4byte	0xbd
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x5
	.byte	0x4c
	.4byte	0xd1
	.uleb128 0x9
	.byte	0x18
	.byte	0x6
	.byte	0xb6
	.4byte	0x1de
	.uleb128 0xa
	.4byte	.LASF20
	.byte	0x6
	.byte	0xba
	.4byte	0x1de
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x6
	.byte	0xbb
	.4byte	0x1de
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x6
	.byte	0xbc
	.4byte	0x1ee
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x6
	.byte	0xbd
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x6
	.byte	0xc2
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xa
	.4byte	.LASF25
	.byte	0x6
	.byte	0xc2
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xb
	.uleb128 0xa
	.4byte	.LASF26
	.byte	0x6
	.byte	0xc6
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x6
	.byte	0xc6
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0xa
	.4byte	.LASF28
	.byte	0x6
	.byte	0xc6
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0xa
	.4byte	.LASF29
	.byte	0x6
	.byte	0xc7
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xf
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x6
	.byte	0xc7
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF31
	.byte	0x6
	.byte	0xc8
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.uleb128 0xa
	.4byte	.LASF32
	.byte	0x6
	.byte	0xc8
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x6
	.byte	0xc9
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.uleb128 0xa
	.4byte	.LASF34
	.byte	0x6
	.byte	0xca
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x6
	.byte	0xcb
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x15
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x6
	.byte	0xcc
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x16
	.byte	0
	.uleb128 0xb
	.4byte	0x4c
	.4byte	0x1ee
	.uleb128 0xc
	.4byte	0x33
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.4byte	0x5a
	.4byte	0x1fe
	.uleb128 0xc
	.4byte	0x33
	.byte	0x1
	.byte	0
	.uleb128 0x3
	.4byte	.LASF37
	.byte	0x6
	.byte	0xce
	.4byte	0xe7
	.uleb128 0x3
	.4byte	.LASF38
	.byte	0x6
	.byte	0xd8
	.4byte	0x214
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1fe
	.uleb128 0x5
	.byte	0x4
	.4byte	0x3a
	.uleb128 0x9
	.byte	0x8
	.byte	0x7
	.byte	0x7c
	.4byte	0x245
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x7
	.byte	0x7e
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x7
	.byte	0x80
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF41
	.byte	0x7
	.byte	0x82
	.4byte	0x220
	.uleb128 0xb
	.4byte	0x73
	.4byte	0x260
	.uleb128 0xc
	.4byte	0x33
	.byte	0x2
	.byte	0
	.uleb128 0xb
	.4byte	0x3a
	.4byte	0x270
	.uleb128 0xc
	.4byte	0x33
	.byte	0x2f
	.byte	0
	.uleb128 0xb
	.4byte	0x3a
	.4byte	0x280
	.uleb128 0xc
	.4byte	0x33
	.byte	0x7
	.byte	0
	.uleb128 0x9
	.byte	0x14
	.byte	0x8
	.byte	0x18
	.4byte	0x2cf
	.uleb128 0xa
	.4byte	.LASF42
	.byte	0x8
	.byte	0x1a
	.4byte	0xbd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x8
	.byte	0x1c
	.4byte	0xbd
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF44
	.byte	0x8
	.byte	0x1e
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0x8
	.byte	0x20
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF46
	.byte	0x8
	.byte	0x23
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF47
	.byte	0x8
	.byte	0x26
	.4byte	0x280
	.uleb128 0x3
	.4byte	.LASF48
	.byte	0x9
	.byte	0x69
	.4byte	0x2e5
	.uleb128 0xd
	.4byte	.LASF48
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF49
	.byte	0xa
	.byte	0x31
	.4byte	0x2f6
	.uleb128 0xd
	.4byte	.LASF49
	.byte	0x1
	.uleb128 0xb
	.4byte	0x73
	.4byte	0x30c
	.uleb128 0xc
	.4byte	0x33
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.byte	0x24
	.byte	0xb
	.byte	0x78
	.4byte	0x393
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0xb
	.byte	0x7b
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0xb
	.byte	0x83
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF52
	.byte	0xb
	.byte	0x86
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF53
	.byte	0xb
	.byte	0x88
	.4byte	0x3a4
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF54
	.byte	0xb
	.byte	0x8d
	.4byte	0x3b6
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF55
	.byte	0xb
	.byte	0x92
	.4byte	0xb0
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xa
	.4byte	.LASF56
	.byte	0xb
	.byte	0x96
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0xb
	.byte	0x9a
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xa
	.4byte	.LASF58
	.byte	0xb
	.byte	0x9c
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	0x39f
	.uleb128 0xf
	.4byte	0x39f
	.byte	0
	.uleb128 0x10
	.4byte	0x61
	.uleb128 0x5
	.byte	0x4
	.4byte	0x393
	.uleb128 0xe
	.byte	0x1
	.4byte	0x3b6
	.uleb128 0xf
	.4byte	0x245
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x3aa
	.uleb128 0x3
	.4byte	.LASF59
	.byte	0xb
	.byte	0x9e
	.4byte	0x30c
	.uleb128 0xb
	.4byte	0x41
	.4byte	0x3dd
	.uleb128 0xc
	.4byte	0x33
	.byte	0xef
	.uleb128 0xc
	.4byte	0x33
	.byte	0x9f
	.byte	0
	.uleb128 0x9
	.byte	0x14
	.byte	0x1
	.byte	0x4f
	.4byte	0x42c
	.uleb128 0xa
	.4byte	.LASF60
	.byte	0x1
	.byte	0x55
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF61
	.byte	0x1
	.byte	0x59
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF62
	.byte	0x1
	.byte	0x5e
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF63
	.byte	0x1
	.byte	0x60
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF64
	.byte	0x1
	.byte	0x62
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF65
	.byte	0x1
	.byte	0x64
	.4byte	0x3dd
	.uleb128 0x9
	.byte	0x18
	.byte	0x1
	.byte	0x69
	.4byte	0x490
	.uleb128 0xa
	.4byte	.LASF66
	.byte	0x1
	.byte	0x6c
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF67
	.byte	0x1
	.byte	0x6f
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x11
	.ascii	"row\000"
	.byte	0x1
	.byte	0x73
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x11
	.ascii	"col\000"
	.byte	0x1
	.byte	0x77
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x11
	.ascii	"X\000"
	.byte	0x1
	.byte	0x7a
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x11
	.ascii	"Y\000"
	.byte	0x1
	.byte	0x7d
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x3
	.4byte	.LASF68
	.byte	0x1
	.byte	0x7f
	.4byte	0x437
	.uleb128 0x9
	.byte	0xc
	.byte	0x1
	.byte	0x8b
	.4byte	0x4ce
	.uleb128 0xa
	.4byte	.LASF69
	.byte	0x1
	.byte	0x8d
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF70
	.byte	0x1
	.byte	0x8f
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF71
	.byte	0x1
	.byte	0x91
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF72
	.byte	0x1
	.byte	0x93
	.4byte	0x49b
	.uleb128 0x9
	.byte	0x14
	.byte	0x1
	.byte	0x98
	.4byte	0x528
	.uleb128 0xa
	.4byte	.LASF73
	.byte	0x1
	.byte	0x9c
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF74
	.byte	0x1
	.byte	0x9f
	.4byte	0x73
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF75
	.byte	0x1
	.byte	0xa5
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF76
	.byte	0x1
	.byte	0xab
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF77
	.byte	0x1
	.byte	0xaf
	.4byte	0x21a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF78
	.byte	0x1
	.byte	0xb1
	.4byte	0x4d9
	.uleb128 0x12
	.4byte	.LASF135
	.byte	0x1
	.byte	0xca
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x13
	.4byte	.LASF79
	.byte	0x1
	.byte	0xdb
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x56e
	.uleb128 0x14
	.4byte	.LASF82
	.byte	0x1
	.byte	0xdb
	.4byte	0x56e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.4byte	0xa5
	.uleb128 0x13
	.4byte	.LASF80
	.byte	0x1
	.byte	0xfb
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x59a
	.uleb128 0x15
	.4byte	.LASF85
	.byte	0x1
	.byte	0xfd
	.4byte	0x59a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x490
	.uleb128 0x16
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x129
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x5f6
	.uleb128 0x17
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x129
	.4byte	0x56e
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x17
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x129
	.4byte	0x5f6
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x18
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x12b
	.4byte	0x270
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x12d
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x10
	.4byte	0x73
	.uleb128 0x16
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x169
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x6c9
	.uleb128 0x17
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x169
	.4byte	0x6c9
	.byte	0x3
	.byte	0x91
	.sleb128 -308
	.uleb128 0x18
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x16b
	.4byte	0x5f6
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x16d
	.4byte	0x6d9
	.byte	0x3
	.byte	0x91
	.sleb128 -304
	.uleb128 0x18
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x16f
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x16f
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x18
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x16f
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x171
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x18
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x171
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x18
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x173
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x18
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x175
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x19
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x177
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x179
	.4byte	0xa5
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x10
	.4byte	0x6ce
	.uleb128 0x5
	.byte	0x4
	.4byte	0x6d4
	.uleb128 0x10
	.4byte	0x490
	.uleb128 0xb
	.4byte	0x3a
	.4byte	0x6e9
	.uleb128 0xc
	.4byte	0x33
	.byte	0xff
	.byte	0
	.uleb128 0x16
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x1fd
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x730
	.uleb128 0x17
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x1fd
	.4byte	0x5f6
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x1ff
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x201
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x25e
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x80c
	.uleb128 0x17
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x25e
	.4byte	0x5f6
	.byte	0x3
	.byte	0x91
	.sleb128 -100
	.uleb128 0x17
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x25f
	.4byte	0x73
	.byte	0x3
	.byte	0x91
	.sleb128 -104
	.uleb128 0x19
	.ascii	"row\000"
	.byte	0x1
	.2byte	0x261
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"col\000"
	.byte	0x1
	.2byte	0x262
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x263
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x264
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x18
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x266
	.4byte	0xa5
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x267
	.4byte	0xa5
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x18
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x269
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x18
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x26b
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x19
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x26d
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x19
	.ascii	"j\000"
	.byte	0x1
	.2byte	0x26d
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x18
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x26f
	.4byte	0x260
	.byte	0x3
	.byte	0x91
	.sleb128 -96
	.byte	0
	.uleb128 0x16
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x372
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x871
	.uleb128 0x17
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x372
	.4byte	0x5f6
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x374
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x376
	.4byte	0x59a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x37a
	.4byte	0x59a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x37c
	.4byte	0x59a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x16
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x42a
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x8a9
	.uleb128 0x17
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x42a
	.4byte	0x5f6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x42c
	.4byte	0x8a9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x528
	.uleb128 0x16
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x490
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x8f6
	.uleb128 0x17
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x490
	.4byte	0x5f6
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x492
	.4byte	0x8a9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x494
	.4byte	0x8f6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2da
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF122
	.byte	0x1
	.2byte	0x538
	.byte	0x1
	.4byte	0xa5
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x938
	.uleb128 0x17
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x538
	.4byte	0x5f6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x53a
	.4byte	0xa5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF123
	.byte	0x1
	.2byte	0x54d
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x972
	.uleb128 0x19
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x54f
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x19
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x551
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF131
	.byte	0x1
	.2byte	0x57d
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0xa12
	.uleb128 0x17
	.4byte	.LASF124
	.byte	0x1
	.2byte	0x57d
	.4byte	0xa27
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x17
	.4byte	.LASF125
	.byte	0x1
	.2byte	0x57d
	.4byte	0xa27
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x17
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x57d
	.4byte	0x5f6
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x17
	.4byte	.LASF127
	.byte	0x1
	.2byte	0x57d
	.4byte	0x56e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x17
	.4byte	.LASF128
	.byte	0x1
	.2byte	0x57d
	.4byte	0x56e
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x17
	.4byte	.LASF129
	.byte	0x1
	.2byte	0x57d
	.4byte	0x56e
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x18
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x57f
	.4byte	0x8f6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF130
	.byte	0x1
	.2byte	0x581
	.4byte	0xa5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x583
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x1c
	.byte	0x1
	.4byte	0x73
	.4byte	0xa22
	.uleb128 0xf
	.4byte	0xa22
	.byte	0
	.uleb128 0x10
	.4byte	0xbd
	.uleb128 0x5
	.byte	0x4
	.4byte	0xa12
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF132
	.byte	0x1
	.2byte	0x5dd
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0xa66
	.uleb128 0x17
	.4byte	.LASF133
	.byte	0x1
	.2byte	0x5dd
	.4byte	0xa66
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF134
	.byte	0x1
	.2byte	0x5dd
	.4byte	0x5f6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0xa5
	.uleb128 0x1d
	.4byte	.LASF136
	.byte	0x1
	.2byte	0x5e9
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.uleb128 0x16
	.4byte	.LASF137
	.byte	0x1
	.2byte	0x61b
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0xab9
	.uleb128 0x18
	.4byte	.LASF138
	.byte	0x1
	.2byte	0x61d
	.4byte	0xab9
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF139
	.byte	0x1
	.2byte	0x61f
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2eb
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF140
	.byte	0x1
	.2byte	0x656
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0xaf8
	.uleb128 0x17
	.4byte	.LASF127
	.byte	0x1
	.2byte	0x656
	.4byte	0x56e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF129
	.byte	0x1
	.2byte	0x656
	.4byte	0x56e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF141
	.byte	0x1
	.2byte	0x6ac
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0xb31
	.uleb128 0x17
	.4byte	.LASF142
	.byte	0x1
	.2byte	0x6ac
	.4byte	0xb31
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x19
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x6ae
	.4byte	0x3bc
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x10
	.4byte	0x245
	.uleb128 0xb
	.4byte	0x3a
	.4byte	0xb46
	.uleb128 0xc
	.4byte	0x33
	.byte	0x40
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF143
	.byte	0xc
	.2byte	0x1fc
	.4byte	0xb36
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF144
	.byte	0xc
	.2byte	0x2ec
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	0x3a
	.4byte	0xb72
	.uleb128 0xc
	.4byte	0x33
	.byte	0x30
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF145
	.byte	0xc
	.2byte	0x3f3
	.4byte	0xb62
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF146
	.byte	0xc
	.2byte	0x42c
	.4byte	0xb62
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF147
	.byte	0xc
	.2byte	0x42d
	.4byte	0xb62
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF148
	.byte	0xc
	.2byte	0x42e
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF149
	.byte	0xc
	.2byte	0x42f
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	0x3a
	.4byte	0xbc8
	.uleb128 0xc
	.4byte	0x33
	.byte	0x80
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF150
	.byte	0xc
	.2byte	0x430
	.4byte	0xbb8
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF151
	.byte	0xc
	.2byte	0x431
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF152
	.byte	0x6
	.2byte	0x127
	.4byte	0x6c
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	0x209
	.4byte	0xc02
	.uleb128 0xc
	.4byte	0x33
	.byte	0x6
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF153
	.byte	0xd
	.byte	0x2e
	.4byte	0xc0f
	.byte	0x1
	.byte	0x1
	.uleb128 0x10
	.4byte	0xbf2
	.uleb128 0x15
	.4byte	.LASF154
	.byte	0xd
	.byte	0x30
	.4byte	0xc25
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x10
	.4byte	0x1de
	.uleb128 0x15
	.4byte	.LASF155
	.byte	0xd
	.byte	0x34
	.4byte	0xc3b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x10
	.4byte	0x1de
	.uleb128 0x15
	.4byte	.LASF156
	.byte	0xd
	.byte	0x36
	.4byte	0xc51
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x10
	.4byte	0x1de
	.uleb128 0x15
	.4byte	.LASF157
	.byte	0xd
	.byte	0x38
	.4byte	0xc67
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x10
	.4byte	0x1de
	.uleb128 0x1f
	.4byte	.LASF158
	.byte	0xe
	.byte	0x14
	.4byte	0xa5
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF159
	.byte	0xf
	.byte	0x63
	.4byte	0x73
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF160
	.byte	0x9
	.byte	0x64
	.4byte	0x2cf
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF161
	.byte	0x10
	.byte	0x33
	.4byte	0xca4
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x10
	.4byte	0x250
	.uleb128 0x15
	.4byte	.LASF162
	.byte	0x10
	.byte	0x3f
	.4byte	0xcba
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x10
	.4byte	0x2fc
	.uleb128 0x1f
	.4byte	.LASF163
	.byte	0x11
	.byte	0x78
	.4byte	0xdc
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF164
	.byte	0x11
	.byte	0xa5
	.4byte	0xdc
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF165
	.byte	0x12
	.byte	0x7a
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	0x3bc
	.4byte	0xcf6
	.uleb128 0xc
	.4byte	0x33
	.byte	0x31
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF166
	.byte	0xb
	.byte	0xac
	.4byte	0xce6
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF167
	.byte	0xb
	.byte	0xae
	.4byte	0x73
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF168
	.byte	0x13
	.byte	0x3b
	.4byte	0x3c7
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	0x41
	.4byte	0xd34
	.uleb128 0x20
	.4byte	0x33
	.2byte	0x5ff
	.uleb128 0xc
	.4byte	0x33
	.byte	0x9f
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF169
	.byte	0x13
	.byte	0x3d
	.4byte	0xd1d
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF170
	.byte	0x1
	.byte	0x39
	.4byte	0xa5
	.byte	0x5
	.byte	0x3
	.4byte	g_STATION_SELECTION_GRID_dialog_displayed
	.uleb128 0x15
	.4byte	.LASF171
	.byte	0x1
	.byte	0x3b
	.4byte	0xa5
	.byte	0x5
	.byte	0x3
	.4byte	g_STATION_SELECTION_GRID_user_toggled_display
	.uleb128 0x15
	.4byte	.LASF172
	.byte	0x1
	.byte	0xb6
	.4byte	0x42c
	.byte	0x5
	.byte	0x3
	.4byte	CURSOR_statistics
	.uleb128 0x15
	.4byte	.LASF173
	.byte	0x1
	.byte	0xba
	.4byte	0x59a
	.byte	0x5
	.byte	0x3
	.4byte	CURSOR_current_cursor_ptr
	.uleb128 0xb
	.4byte	0x490
	.4byte	0xd96
	.uleb128 0x20
	.4byte	0x33
	.2byte	0x2ff
	.byte	0
	.uleb128 0x15
	.4byte	.LASF174
	.byte	0x1
	.byte	0xbc
	.4byte	0xd85
	.byte	0x5
	.byte	0x3
	.4byte	Group_CP
	.uleb128 0xb
	.4byte	0x4ce
	.4byte	0xdb8
	.uleb128 0x20
	.4byte	0x33
	.2byte	0x2ff
	.byte	0
	.uleb128 0x15
	.4byte	.LASF175
	.byte	0x1
	.byte	0xbe
	.4byte	0xda7
	.byte	0x5
	.byte	0x3
	.4byte	g_CURSOR_mgmt_add_on
	.uleb128 0xb
	.4byte	0x528
	.4byte	0xdda
	.uleb128 0x20
	.4byte	0x33
	.2byte	0x2ff
	.byte	0
	.uleb128 0x15
	.4byte	.LASF176
	.byte	0x1
	.byte	0xc0
	.4byte	0xdc9
	.byte	0x5
	.byte	0x3
	.4byte	station_selection_struct
	.uleb128 0x15
	.4byte	.LASF177
	.byte	0x1
	.byte	0xc2
	.4byte	0x528
	.byte	0x5
	.byte	0x3
	.4byte	CURSOR_station_when_display_was_toggled
	.uleb128 0x15
	.4byte	.LASF178
	.byte	0x1
	.byte	0xc4
	.4byte	0x73
	.byte	0x5
	.byte	0x3
	.4byte	g_CURSOR_last_station_selected
	.uleb128 0x1e
	.4byte	.LASF143
	.byte	0xc
	.2byte	0x1fc
	.4byte	0xb36
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF144
	.byte	0xc
	.2byte	0x2ec
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF145
	.byte	0xc
	.2byte	0x3f3
	.4byte	0xb62
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF146
	.byte	0xc
	.2byte	0x42c
	.4byte	0xb62
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF147
	.byte	0xc
	.2byte	0x42d
	.4byte	0xb62
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF148
	.byte	0xc
	.2byte	0x42e
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF149
	.byte	0xc
	.2byte	0x42f
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF150
	.byte	0xc
	.2byte	0x430
	.4byte	0xbb8
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF151
	.byte	0xc
	.2byte	0x431
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF152
	.byte	0x6
	.2byte	0x127
	.4byte	0x6c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF153
	.byte	0xd
	.byte	0x2e
	.4byte	0xea6
	.byte	0x1
	.byte	0x1
	.uleb128 0x10
	.4byte	0xbf2
	.uleb128 0x21
	.4byte	.LASF158
	.byte	0x1
	.byte	0x37
	.4byte	0xa5
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_STATION_SELECTION_GRID_user_pressed_back
	.uleb128 0x1f
	.4byte	.LASF159
	.byte	0xf
	.byte	0x63
	.4byte	0x73
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF160
	.byte	0x9
	.byte	0x64
	.4byte	0x2cf
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF163
	.byte	0x11
	.byte	0x78
	.4byte	0xdc
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF164
	.byte	0x11
	.byte	0xa5
	.4byte	0xdc
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF165
	.byte	0x12
	.byte	0x7a
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF166
	.byte	0xb
	.byte	0xac
	.4byte	0xce6
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF167
	.byte	0xb
	.byte	0xae
	.4byte	0x73
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF168
	.byte	0x13
	.byte	0x3b
	.4byte	0x3c7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF169
	.byte	0x13
	.byte	0x3d
	.4byte	0xd1d
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI18
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI21
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI24
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI33
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI36
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI39
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI42
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI45
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI48
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI51
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xa4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF44:
	.ascii	"count\000"
.LASF113:
	.ascii	"pkeycode\000"
.LASF49:
	.ascii	"MANUAL_PROGRAMS_GROUP_STRUCT\000"
.LASF124:
	.ascii	"pget_group_ID_A_func_ptr\000"
.LASF75:
	.ascii	"assigned_to_this_group\000"
.LASF146:
	.ascii	"GuiVar_StationSelectionGridControllerName\000"
.LASF52:
	.ascii	"_03_structure_to_draw\000"
.LASF153:
	.ascii	"GuiFont_FontList\000"
.LASF51:
	.ascii	"_02_menu\000"
.LASF156:
	.ascii	"GuiFont_DecimalChar\000"
.LASF64:
	.ascii	"number_of_rows\000"
.LASF126:
	.ascii	"pgroup_ID\000"
.LASF116:
	.ascii	"ltemp_cursor\000"
.LASF107:
	.ascii	"next_box_index_0\000"
.LASF13:
	.ascii	"long long unsigned int\000"
.LASF145:
	.ascii	"GuiVar_StationDescription\000"
.LASF15:
	.ascii	"BOOL_32\000"
.LASF42:
	.ascii	"phead\000"
.LASF134:
	.ascii	"pcursor_pos\000"
.LASF112:
	.ascii	"__FDTO_move_cursor\000"
.LASF10:
	.ascii	"UNS_32\000"
.LASF39:
	.ascii	"keycode\000"
.LASF14:
	.ascii	"long long int\000"
.LASF5:
	.ascii	"signed char\000"
.LASF108:
	.ascii	"start_of_new_controller\000"
.LASF105:
	.ascii	"py_coordinate_start\000"
.LASF92:
	.ascii	"ltext_color\000"
.LASF21:
	.ascii	"LastChar\000"
.LASF165:
	.ascii	"g_MAIN_MENU_active_menu_item\000"
.LASF8:
	.ascii	"INT_16\000"
.LASF138:
	.ascii	"lman_program\000"
.LASF46:
	.ascii	"InUse\000"
.LASF98:
	.ascii	"lwidth_us\000"
.LASF95:
	.ascii	"ltarget_x_i32\000"
.LASF16:
	.ascii	"long int\000"
.LASF91:
	.ascii	"ldisplay_buf\000"
.LASF90:
	.ascii	"OFFSET_TARGET_UNITS_FROM_END_OF_TARGET\000"
.LASF59:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF142:
	.ascii	"pkey_event\000"
.LASF9:
	.ascii	"short int\000"
.LASF167:
	.ascii	"screen_history_index\000"
.LASF122:
	.ascii	"STATION_SELECTION_GRID_station_is_selected\000"
.LASF1:
	.ascii	"double\000"
.LASF135:
	.ascii	"__resize_scroll_box_marker\000"
.LASF47:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF158:
	.ascii	"g_STATION_SELECTION_GRID_user_pressed_back\000"
.LASF157:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF36:
	.ascii	"LineSize\000"
.LASF62:
	.ascii	"visible_rows\000"
.LASF169:
	.ascii	"utility_display_buf\000"
.LASF176:
	.ascii	"station_selection_struct\000"
.LASF163:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF106:
	.ascii	"prev_controller_box_index_0\000"
.LASF123:
	.ascii	"STATION_SELECTION_GRID_get_count_of_selected_statio"
	.ascii	"ns\000"
.LASF81:
	.ascii	"__draw_station_description_for_selected_cursor\000"
.LASF178:
	.ascii	"g_CURSOR_last_station_selected\000"
.LASF103:
	.ascii	"__FDTO_draw_all_cursor_positions\000"
.LASF82:
	.ascii	"pmove_to_top\000"
.LASF151:
	.ascii	"GuiVar_StationSelectionGridShowOnlyStationsInThisGr"
	.ascii	"oup\000"
.LASF11:
	.ascii	"unsigned int\000"
.LASF144:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF177:
	.ascii	"CURSOR_station_when_display_was_toggled\000"
.LASF12:
	.ascii	"INT_32\000"
.LASF2:
	.ascii	"long unsigned int\000"
.LASF170:
	.ascii	"g_STATION_SELECTION_GRID_dialog_displayed\000"
.LASF101:
	.ascii	"pstart_at_top_bool\000"
.LASF54:
	.ascii	"key_process_func_ptr\000"
.LASF150:
	.ascii	"GuiVar_StationSelectionGridScreenTitle\000"
.LASF93:
	.ascii	"lforeground_color\000"
.LASF55:
	.ascii	"_04_func_ptr\000"
.LASF66:
	.ascii	"cursor_index\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF26:
	.ascii	"TopLine\000"
.LASF179:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF133:
	.ascii	"pediting_group\000"
.LASF56:
	.ascii	"_06_u32_argument1\000"
.LASF100:
	.ascii	"__show_cursor\000"
.LASF94:
	.ascii	"lbackground_color\000"
.LASF139:
	.ascii	"lbox_index_0\000"
.LASF77:
	.ascii	"description_ptr\000"
.LASF53:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF60:
	.ascii	"pixels_from_top\000"
.LASF18:
	.ascii	"xQueueHandle\000"
.LASF155:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF58:
	.ascii	"_08_screen_to_draw\000"
.LASF125:
	.ascii	"pget_group_ID_B_func_ptr\000"
.LASF71:
	.ascii	"target_units_width\000"
.LASF99:
	.ascii	"lwrote_to_utility_buffer\000"
.LASF140:
	.ascii	"FDTO_STATION_SELECTION_GRID_draw_screen\000"
.LASF33:
	.ascii	"BoxWidth\000"
.LASF172:
	.ascii	"CURSOR_statistics\000"
.LASF67:
	.ascii	"station_array_index\000"
.LASF96:
	.ascii	"lcleanup_width_i32\000"
.LASF118:
	.ascii	"__FDTO_process_cursor_position\000"
.LASF25:
	.ascii	"YSize\000"
.LASF120:
	.ascii	"__FDTO_process_manual_program_cursor_position\000"
.LASF41:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF50:
	.ascii	"_01_command\000"
.LASF80:
	.ascii	"__draw_group_name_and_controller_name\000"
.LASF147:
	.ascii	"GuiVar_StationSelectionGridGroupName\000"
.LASF109:
	.ascii	"lnumber_of_controllers\000"
.LASF87:
	.ascii	"llen\000"
.LASF168:
	.ascii	"primary_display_buf\000"
.LASF131:
	.ascii	"STATION_SELECTION_GRID_populate_cursors\000"
.LASF43:
	.ascii	"ptail\000"
.LASF65:
	.ascii	"CURSOR_STATS\000"
.LASF0:
	.ascii	"float\000"
.LASF154:
	.ascii	"GuiFont_LanguageActive\000"
.LASF88:
	.ascii	"__draw_station_number_for_current_cursor\000"
.LASF74:
	.ascii	"station_number_0\000"
.LASF19:
	.ascii	"xSemaphoreHandle\000"
.LASF152:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF72:
	.ascii	"CURSOR_MANAGEMENT_STRUCT\000"
.LASF4:
	.ascii	"unsigned char\000"
.LASF143:
	.ascii	"GuiVar_GroupName\000"
.LASF85:
	.ascii	"lcursor_pos_on_top_row\000"
.LASF84:
	.ascii	"pindex\000"
.LASF28:
	.ascii	"BaseLine\000"
.LASF173:
	.ascii	"CURSOR_current_cursor_ptr\000"
.LASF34:
	.ascii	"PsNumWidth\000"
.LASF17:
	.ascii	"portTickType\000"
.LASF104:
	.ascii	"px_coordinate_start\000"
.LASF119:
	.ascii	"lstation\000"
.LASF149:
	.ascii	"GuiVar_StationSelectionGridMarkerSize\000"
.LASF166:
	.ascii	"ScreenHistory\000"
.LASF136:
	.ascii	"draw_station_selection_grid_title\000"
.LASF102:
	.ascii	"cursor_to_draw\000"
.LASF114:
	.ascii	"lamount_to_move\000"
.LASF97:
	.ascii	"lfont\000"
.LASF128:
	.ascii	"phide_stations_not_in_use\000"
.LASF127:
	.ascii	"pcomplete_redraw\000"
.LASF3:
	.ascii	"char\000"
.LASF117:
	.ascii	"lsaved_cursor\000"
.LASF159:
	.ascii	"g_GROUP_ID\000"
.LASF180:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_station_selection_grid.c\000"
.LASF79:
	.ascii	"__move_scroll_box_marker\000"
.LASF22:
	.ascii	"FirstCharNdx\000"
.LASF83:
	.ascii	"pshow_description_bool\000"
.LASF171:
	.ascii	"g_STATION_SELECTION_GRID_user_toggled_display\000"
.LASF45:
	.ascii	"offset\000"
.LASF175:
	.ascii	"g_CURSOR_mgmt_add_on\000"
.LASF40:
	.ascii	"repeats\000"
.LASF137:
	.ascii	"STATION_SELECTION_GRID_save_changes\000"
.LASF132:
	.ascii	"FDTO_STATION_SELECTION_GRID_redraw_calling_screen\000"
.LASF162:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF23:
	.ascii	"IllegalCharNdx\000"
.LASF174:
	.ascii	"Group_CP\000"
.LASF70:
	.ascii	"target_width\000"
.LASF68:
	.ascii	"CURSOR_OBJ\000"
.LASF160:
	.ascii	"station_info_list_hdr\000"
.LASF31:
	.ascii	"Underline1\000"
.LASF38:
	.ascii	"GuiLib_FontRecPtr\000"
.LASF7:
	.ascii	"UNS_8\000"
.LASF24:
	.ascii	"XSize\000"
.LASF86:
	.ascii	"str_8\000"
.LASF148:
	.ascii	"GuiVar_StationSelectionGridMarkerPos\000"
.LASF32:
	.ascii	"Underline2\000"
.LASF111:
	.ascii	"str_48\000"
.LASF57:
	.ascii	"_07_u32_argument2\000"
.LASF121:
	.ascii	"lstation_struct_ptr\000"
.LASF27:
	.ascii	"MidLine\000"
.LASF76:
	.ascii	"station_visible\000"
.LASF48:
	.ascii	"STATION_STRUCT\000"
.LASF61:
	.ascii	"top_row\000"
.LASF78:
	.ascii	"CURSOR_STATION_LIST_STRUCT\000"
.LASF20:
	.ascii	"FirstChar\000"
.LASF35:
	.ascii	"PsSpace\000"
.LASF89:
	.ascii	"pcursor_to_draw\000"
.LASF129:
	.ascii	"ponly_show_stations_in_this_group\000"
.LASF130:
	.ascii	"lassigned_to_this_group\000"
.LASF141:
	.ascii	"STATION_SELECTION_GRID_process_screen\000"
.LASF73:
	.ascii	"box_index_0\000"
.LASF164:
	.ascii	"chain_members_recursive_MUTEX\000"
.LASF63:
	.ascii	"number_of_stations\000"
.LASF161:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF29:
	.ascii	"Cursor1\000"
.LASF30:
	.ascii	"Cursor2\000"
.LASF115:
	.ascii	"lcursor\000"
.LASF37:
	.ascii	"GuiLib_FontRec\000"
.LASF110:
	.ascii	"lindex_of_selected_station_when_display_was_toggled"
	.ascii	"\000"
.LASF69:
	.ascii	"prompt_width\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
