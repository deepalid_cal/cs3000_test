	.file	"lpc32xx_timer_driver.c"
	.text
.Ltext0:
	.section	.bss.tmr_init,"aw",%nobits
	.align	2
	.type	tmr_init, %object
	.size	tmr_init, 24
tmr_init:
	.space	24
	.section	.bss.tmrdat,"aw",%nobits
	.align	2
	.type	tmrdat, %object
	.size	tmrdat, 24
tmrdat:
	.space	24
	.section	.rodata.timer_num_to_clk_enum,"a",%progbits
	.align	2
	.type	timer_num_to_clk_enum, %object
	.size	timer_num_to_clk_enum, 24
timer_num_to_clk_enum:
	.word	21
	.word	20
	.word	19
	.word	18
	.word	23
	.word	22
	.global	__udivdi3
	.section	.text.timer_usec_to_val,"ax",%progbits
	.align	2
	.global	timer_usec_to_val
	.type	timer_usec_to_val, %function
timer_usec_to_val:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer_driver.c"
	.loc 1 83 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI0:
	add	fp, sp, #8
.LCFI1:
	sub	sp, sp, #24
.LCFI2:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	.loc 1 89 0
	ldr	r0, [fp, #-28]
	bl	clkpwr_get_clock_rate
	mov	r2, r0
	mov	r3, r2
	mov	r4, #0
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	.loc 1 90 0
	sub	r2, fp, #16
	ldmia	r2, {r1-r2}
	mov	r3, #1000
	mov	r4, #0
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-24]
	str	r4, [fp, #-20]
	.loc 1 91 0
	ldr	r2, [fp, #-32]
	mov	r3, r2
	mov	r4, #0
	ldr	r2, [fp, #-24]
	mul	r1, r4, r2
	ldr	r2, [fp, #-20]
	mul	r2, r3, r2
	add	r0, r1, r2
	ldr	ip, [fp, #-24]
	umull	r1, r2, ip, r3
	add	r3, r0, r2
	mov	r2, r3
	mov	r3, #1000
	mov	r4, #0
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-24]
	str	r4, [fp, #-20]
	.loc 1 93 0
	ldr	r3, [fp, #-24]
	.loc 1 94 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE0:
	.size	timer_usec_to_val, .-timer_usec_to_val
	.section	.text.timer_ptr_to_timer_num,"ax",%progbits
	.align	2
	.global	timer_ptr_to_timer_num
	.type	timer_ptr_to_timer_num, %function
timer_ptr_to_timer_num:
.LFB1:
	.loc 1 116 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 117 0
	mvn	r3, #0
	str	r3, [fp, #-4]
	.loc 1 119 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L9
	cmp	r2, r3
	bne	.L3
	.loc 1 121 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L4
.L3:
	.loc 1 123 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L9+4
	cmp	r2, r3
	bne	.L5
	.loc 1 125 0
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L4
.L5:
	.loc 1 127 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L9+8
	cmp	r2, r3
	bne	.L6
	.loc 1 129 0
	mov	r3, #2
	str	r3, [fp, #-4]
	b	.L4
.L6:
	.loc 1 131 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L9+12
	cmp	r2, r3
	bne	.L7
	.loc 1 133 0
	mov	r3, #3
	str	r3, [fp, #-4]
	b	.L4
.L7:
	.loc 1 135 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L9+16
	cmp	r2, r3
	bne	.L8
	.loc 1 137 0
	mov	r3, #4
	str	r3, [fp, #-4]
	b	.L4
.L8:
	.loc 1 139 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L9+20
	cmp	r2, r3
	bne	.L4
	.loc 1 141 0
	mov	r3, #5
	str	r3, [fp, #-4]
.L4:
	.loc 1 144 0
	ldr	r3, [fp, #-4]
	.loc 1 145 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L10:
	.align	2
.L9:
	.word	1074020352
	.word	1074053120
	.word	1074102272
	.word	1074135040
	.word	1073922048
	.word	1073938432
.LFE1:
	.size	timer_ptr_to_timer_num, .-timer_ptr_to_timer_num
	.section	.text.timer_cfg_to_timer_num,"ax",%progbits
	.align	2
	.global	timer_cfg_to_timer_num
	.type	timer_cfg_to_timer_num, %function
timer_cfg_to_timer_num:
.LFB2:
	.loc 1 167 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-8]
	.loc 1 168 0
	mvn	r3, #0
	str	r3, [fp, #-4]
	.loc 1 170 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L18
	cmp	r2, r3
	bne	.L12
	.loc 1 172 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L13
.L12:
	.loc 1 174 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L18+4
	cmp	r2, r3
	bne	.L14
	.loc 1 176 0
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L13
.L14:
	.loc 1 178 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L18+8
	cmp	r2, r3
	bne	.L15
	.loc 1 180 0
	mov	r3, #2
	str	r3, [fp, #-4]
	b	.L13
.L15:
	.loc 1 182 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L18+12
	cmp	r2, r3
	bne	.L16
	.loc 1 184 0
	mov	r3, #3
	str	r3, [fp, #-4]
	b	.L13
.L16:
	.loc 1 186 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L18+16
	cmp	r2, r3
	bne	.L17
	.loc 1 188 0
	mov	r3, #4
	str	r3, [fp, #-4]
	b	.L13
.L17:
	.loc 1 190 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L18+20
	cmp	r2, r3
	bne	.L13
	.loc 1 192 0
	mov	r3, #5
	str	r3, [fp, #-4]
.L13:
	.loc 1 195 0
	ldr	r3, [fp, #-4]
	.loc 1 196 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L19:
	.align	2
.L18:
	.word	tmrdat
	.word	tmrdat+4
	.word	tmrdat+8
	.word	tmrdat+12
	.word	tmrdat+16
	.word	tmrdat+20
.LFE2:
	.size	timer_cfg_to_timer_num, .-timer_cfg_to_timer_num
	.section	.text.timer_delay_cmn,"ax",%progbits
	.align	2
	.global	timer_delay_cmn
	.type	timer_delay_cmn, %function
timer_delay_cmn:
.LFB3:
	.loc 1 219 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #12
.LCFI11:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 223 0
	ldr	r0, [fp, #-12]
	bl	timer_ptr_to_timer_num
	str	r0, [fp, #-8]
	.loc 1 224 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	blt	.L24
.L21:
	.loc 1 230 0
	ldr	r3, .L25
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 233 0
	ldr	r3, [fp, #-12]
	mov	r2, #2
	str	r2, [r3, #4]
	.loc 1 234 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 237 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 240 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #112]
	.loc 1 243 0
	ldr	r3, .L25
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	mov	r1, #1
	bl	timer_usec_to_val
	mov	r2, r0
	ldr	r3, [fp, #-12]
	str	r2, [r3, #12]
	.loc 1 247 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #24]
	.loc 1 250 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #20]
	.loc 1 253 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #4]
	.loc 1 256 0
	mov	r0, r0	@ nop
.L23:
	.loc 1 256 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	and	r3, r3, #1
	cmp	r3, #0
	beq	.L23
	.loc 1 259 0 is_stmt 1
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 262 0
	ldr	r3, .L25
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	mov	r1, #0
	bl	clkpwr_clk_en_dis
	b	.L20
.L24:
	.loc 1 226 0
	mov	r0, r0	@ nop
.L20:
	.loc 1 263 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L26:
	.align	2
.L25:
	.word	timer_num_to_clk_enum
.LFE3:
	.size	timer_delay_cmn, .-timer_delay_cmn
	.section	.text.timer_open,"ax",%progbits
	.align	2
	.global	timer_open
	.type	timer_open, %function
timer_open:
.LFB4:
	.loc 1 291 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #20
.LCFI14:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 1 293 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 296 0
	ldr	r0, [fp, #-20]
	bl	timer_ptr_to_timer_num
	str	r0, [fp, #-12]
	.loc 1 297 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	blt	.L28
	.loc 1 300 0
	ldr	r3, .L29
	ldr	r2, [fp, #-12]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	bne	.L28
	.loc 1 303 0
	ldr	r3, .L29
	ldr	r2, [fp, #-12]
	mov	r1, #1
	str	r1, [r3, r2, asl #2]
	.loc 1 304 0
	ldr	r3, [fp, #-12]
	mov	r2, r3, asl #2
	ldr	r3, .L29+4
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 305 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 308 0
	ldr	r3, .L29+8
	ldr	r2, [fp, #-12]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 312 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 313 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #112]
	.loc 1 315 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #20]
	.loc 1 316 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #40]
	.loc 1 317 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #60]
	.loc 1 324 0
	ldr	r3, [fp, #-12]
	mov	r2, r3, asl #2
	ldr	r3, .L29+4
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 327 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 328 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 329 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #16]
	.loc 1 330 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #24]
	.loc 1 331 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 332 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 333 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #36]
	.loc 1 334 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #255
	str	r2, [r3, #0]
.L28:
	.loc 1 342 0
	ldr	r3, [fp, #-8]
	.loc 1 343 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L30:
	.align	2
.L29:
	.word	tmr_init
	.word	tmrdat
	.word	timer_num_to_clk_enum
.LFE4:
	.size	timer_open, .-timer_open
	.section	.text.timer_close,"ax",%progbits
	.align	2
	.global	timer_close
	.type	timer_close, %function
timer_close:
.LFB5:
	.loc 1 367 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #16
.LCFI17:
	str	r0, [fp, #-20]
	.loc 1 370 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	.loc 1 373 0
	ldr	r3, [fp, #-20]
	mov	r0, r3
	bl	timer_cfg_to_timer_num
	str	r0, [fp, #-12]
	.loc 1 374 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	blt	.L32
	.loc 1 376 0
	ldr	r3, .L33
	ldr	r2, [fp, #-12]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #1
	bne	.L32
	.loc 1 378 0
	ldr	r3, [fp, #-12]
	mov	r2, r3, asl #2
	ldr	r3, .L33+4
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 379 0
	ldr	r3, .L33
	ldr	r2, [fp, #-12]
	mov	r1, #0
	str	r1, [r3, r2, asl #2]
	.loc 1 382 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 383 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #112]
	.loc 1 385 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #20]
	.loc 1 386 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #40]
	.loc 1 387 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #60]
	.loc 1 394 0
	ldr	r3, .L33+8
	ldr	r2, [fp, #-12]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	mov	r1, #0
	bl	clkpwr_clk_en_dis
.L32:
	.loc 1 398 0
	ldr	r3, [fp, #-8]
	.loc 1 399 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L34:
	.align	2
.L33:
	.word	tmr_init
	.word	tmrdat
	.word	timer_num_to_clk_enum
.LFE5:
	.size	timer_close, .-timer_close
	.section	.text.timer_ioctl,"ax",%progbits
	.align	2
	.global	timer_ioctl
	.type	timer_ioctl, %function
timer_ioctl:
.LFB6:
	.loc 1 426 0
	@ args = 0, pretend = 0, frame = 60
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #60
.LCFI20:
	str	r0, [fp, #-56]
	str	r1, [fp, #-60]
	str	r2, [fp, #-64]
	.loc 1 437 0
	mvn	r3, #0
	str	r3, [fp, #-12]
	.loc 1 440 0
	ldr	r3, [fp, #-56]
	mov	r0, r3
	bl	timer_cfg_to_timer_num
	str	r0, [fp, #-16]
	.loc 1 441 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	blt	.L36
	.loc 1 443 0
	ldr	r3, .L80
	ldr	r2, [fp, #-16]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #1
	bne	.L36
	.loc 1 445 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 446 0
	ldr	r3, [fp, #-16]
	mov	r2, r3, asl #2
	ldr	r3, .L80+4
	add	r3, r2, r3
	str	r3, [fp, #-20]
	.loc 1 447 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-24]
	.loc 1 449 0
	ldr	r3, [fp, #-60]
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L37
.L48:
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
.L38:
	.loc 1 452 0
	ldr	r3, [fp, #-64]
	cmp	r3, #0
	beq	.L49
	.loc 1 455 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	orr	r2, r3, #1
	ldr	r3, [fp, #-24]
	str	r2, [r3, #4]
	.loc 1 462 0
	b	.L36
.L49:
	.loc 1 460 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	bic	r2, r3, #1
	ldr	r3, [fp, #-24]
	str	r2, [r3, #4]
	.loc 1 462 0
	b	.L36
.L39:
	.loc 1 465 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	orr	r2, r3, #2
	ldr	r3, [fp, #-24]
	str	r2, [r3, #4]
	.loc 1 466 0
	mov	r0, r0	@ nop
.L51:
	.loc 1 466 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	bne	.L51
	.loc 1 467 0 is_stmt 1
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	bic	r2, r3, #2
	ldr	r3, [fp, #-24]
	str	r2, [r3, #4]
	.loc 1 468 0
	b	.L36
.L40:
	.loc 1 471 0
	ldr	r3, [fp, #-64]
	str	r3, [fp, #-28]
	.loc 1 472 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L52
	.loc 1 474 0
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	str	r2, [r3, #12]
	.loc 1 482 0
	b	.L36
.L52:
	.loc 1 479 0
	ldr	r3, .L80+8
	ldr	r2, [fp, #-16]
	ldr	r2, [r3, r2, asl #2]
	.loc 1 480 0
	ldr	r3, [fp, #-28]
	.loc 1 479 0
	ldr	r3, [r3, #4]
	mov	r0, r2
	mov	r1, r3
	bl	timer_usec_to_val
	mov	r2, r0
	.loc 1 478 0
	ldr	r3, [fp, #-24]
	str	r2, [r3, #12]
	.loc 1 482 0
	b	.L36
.L41:
	.loc 1 485 0
	ldr	r3, [fp, #-64]
	str	r3, [fp, #-32]
	.loc 1 486 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L54
	.loc 1 488 0
	ldr	r3, [fp, #-24]
	mov	r2, #0
	str	r2, [r3, #112]
	.loc 1 523 0
	b	.L36
.L54:
	.loc 1 493 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 494 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #4]
	cmp	r3, #1
	bne	.L56
	.loc 1 495 0 discriminator 1
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #8]
	.loc 1 494 0 discriminator 1
	cmp	r3, #1
	bne	.L56
	.loc 1 497 0
	mov	r3, #3
	str	r3, [fp, #-8]
	b	.L57
.L56:
	.loc 1 500 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #4]
	cmp	r3, #1
	bne	.L58
	.loc 1 502 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L57
.L58:
	.loc 1 505 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #8]
	cmp	r3, #1
	bne	.L57
	.loc 1 507 0
	mov	r3, #2
	str	r3, [fp, #-8]
.L57:
	.loc 1 512 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #12]
	cmp	r3, #3
	bhi	.L59
	.loc 1 514 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #12]
	and	r3, r3, #3
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 516 0
	ldr	r3, [fp, #-24]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #112]
	.loc 1 523 0
	b	.L36
.L59:
	.loc 1 520 0
	mvn	r3, #6
	str	r3, [fp, #-12]
	.loc 1 523 0
	b	.L36
.L42:
	.loc 1 526 0
	ldr	r3, [fp, #-64]
	str	r3, [fp, #-36]
	.loc 1 528 0
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bhi	.L60
	.loc 1 531 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r2, #1
	mov	r1, r2, asl r3
	.loc 1 532 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r3, r3, #1
	mov	r2, #1
	mov	r3, r2, asl r3
	orr	r1, r1, r3
	.loc 1 533 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r3, r3, #2
	mov	r2, #1
	mov	r3, r2, asl r3
	orr	r3, r1, r3
	.loc 1 531 0
	mvn	r3, r3
	str	r3, [fp, #-40]
	.loc 1 536 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	.loc 1 537 0
	ldr	r3, [fp, #-36]
	ldr	r1, [r3, #16]
	.loc 1 536 0
	ldr	r3, [fp, #-24]
	add	r2, r2, #6
	str	r1, [r3, r2, asl #2]
	.loc 1 539 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 540 0
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #4]
	cmp	r3, #1
	bne	.L61
	.loc 1 543 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r2, #1
	mov	r3, r2, asl r3
	.loc 1 542 0
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
.L61:
	.loc 1 545 0
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #8]
	cmp	r3, #1
	bne	.L62
	.loc 1 548 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r3, r3, #2
	mov	r2, #1
	mov	r3, r2, asl r3
	.loc 1 547 0
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
.L62:
	.loc 1 550 0
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #12]
	cmp	r3, #1
	bne	.L63
	.loc 1 553 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r3, r3, #1
	mov	r2, #1
	mov	r3, r2, asl r3
	.loc 1 552 0
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
.L63:
	.loc 1 555 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #20]
	ldr	r3, [fp, #-40]
	and	r2, r2, r3
	ldr	r3, [fp, #-8]
	orr	r2, r2, r3
	ldr	r3, [fp, #-24]
	str	r2, [r3, #20]
	.loc 1 561 0
	b	.L36
.L60:
	.loc 1 559 0
	mvn	r3, #6
	str	r3, [fp, #-12]
	.loc 1 561 0
	b	.L36
.L43:
	.loc 1 564 0
	ldr	r3, [fp, #-64]
	str	r3, [fp, #-44]
	.loc 1 565 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bhi	.L65
	.loc 1 568 0
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r3, r3, #2
	mov	r2, #1
	mov	r1, r2, asl r3
	.loc 1 570 0
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r3, r3, #1
	mov	r2, #1
	mov	r3, r2, asl r3
	.loc 1 569 0
	orr	r1, r1, r3
	.loc 1 571 0
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r2, #1
	mov	r3, r2, asl r3
	orr	r3, r1, r3
	.loc 1 568 0
	mvn	r3, r3
	str	r3, [fp, #-40]
	.loc 1 575 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 576 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #8]
	cmp	r3, #1
	bne	.L66
	.loc 1 578 0
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r2, #1
	mov	r3, r2, asl r3
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
.L66:
	.loc 1 581 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #12]
	cmp	r3, #1
	bne	.L67
	.loc 1 583 0
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r3, r3, #1
	mov	r2, #1
	mov	r3, r2, asl r3
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
.L67:
	.loc 1 586 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #4]
	cmp	r3, #1
	bne	.L68
	.loc 1 588 0
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r3, r3, #2
	mov	r2, #1
	mov	r3, r2, asl r3
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
.L68:
	.loc 1 591 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	ldr	r3, [fp, #-40]
	and	r2, r2, r3
	ldr	r3, [fp, #-8]
	orr	r2, r2, r3
	ldr	r3, [fp, #-24]
	str	r2, [r3, #40]
	.loc 1 597 0
	b	.L36
.L65:
	.loc 1 595 0
	mvn	r3, #6
	str	r3, [fp, #-12]
	.loc 1 597 0
	b	.L36
.L44:
	.loc 1 600 0
	ldr	r3, [fp, #-64]
	str	r3, [fp, #-48]
	.loc 1 601 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bhi	.L70
	.loc 1 604 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #0]
	mov	r2, #1
	mov	r2, r2, asl r3
	.loc 1 605 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #0]
	add	r3, r3, #2
	mov	r3, r3, asl #1
	mov	r1, #3
	mov	r3, r1, asl r3
	orr	r3, r2, r3
	.loc 1 604 0
	mvn	r3, r3
	str	r3, [fp, #-40]
	.loc 1 607 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #4]
	and	r2, r3, #1
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #0]
	mov	r2, r2, asl r3
	.loc 1 609 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #8]
	and	r1, r3, #3
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #0]
	add	r3, r3, #2
	mov	r3, r3, asl #1
	mov	r3, r1, asl r3
	.loc 1 607 0
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 611 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #60]
	ldr	r3, [fp, #-40]
	and	r2, r2, r3
	ldr	r3, [fp, #-8]
	orr	r2, r2, r3
	ldr	r3, [fp, #-24]
	str	r2, [r3, #60]
	.loc 1 617 0
	b	.L36
.L70:
	.loc 1 615 0
	mvn	r3, #6
	str	r3, [fp, #-12]
	.loc 1 617 0
	b	.L36
.L45:
	.loc 1 620 0
	ldr	r2, [fp, #-64]
	ldr	r3, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 621 0
	b	.L36
.L46:
	.loc 1 624 0
	ldr	r3, [fp, #-64]
	str	r3, [fp, #-52]
	.loc 1 625 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L72
.L73:
	.loc 1 628 0 discriminator 2
	ldr	r3, [fp, #-24]
	ldr	r2, [fp, #-8]
	add	r2, r2, #11
	ldr	r1, [r3, r2, asl #2]
	.loc 1 627 0 discriminator 2
	ldr	r3, [fp, #-52]
	ldr	r2, [fp, #-8]
	add	r2, r2, #2
	str	r1, [r3, r2, asl #2]
	.loc 1 625 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L72:
	.loc 1 625 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bls	.L73
	.loc 1 630 0 is_stmt 1
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #16]
	ldr	r3, [fp, #-52]
	str	r2, [r3, #4]
	.loc 1 631 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-52]
	str	r2, [r3, #0]
	.loc 1 632 0
	b	.L36
.L47:
	.loc 1 636 0
	ldr	r3, [fp, #-64]
	cmp	r3, #1
	beq	.L76
	cmp	r3, #2
	beq	.L77
	cmp	r3, #0
	bne	.L79
.L75:
	.loc 1 640 0
	ldr	r3, .L80+8
	ldr	r2, [fp, #-16]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	clkpwr_get_clock_rate
	mov	r3, r0
	.loc 1 639 0
	str	r3, [fp, #-12]
	.loc 1 641 0
	b	.L78
.L76:
	.loc 1 644 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	str	r3, [fp, #-12]
	.loc 1 645 0
	b	.L78
.L77:
	.loc 1 648 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 649 0
	b	.L78
.L79:
	.loc 1 653 0
	mvn	r3, #6
	str	r3, [fp, #-12]
	.loc 1 654 0
	mov	r0, r0	@ nop
.L78:
	.loc 1 656 0
	b	.L36
.L37:
	.loc 1 660 0
	mvn	r3, #6
	str	r3, [fp, #-12]
.L36:
	.loc 1 665 0
	ldr	r3, [fp, #-12]
	.loc 1 666 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L81:
	.align	2
.L80:
	.word	tmr_init
	.word	tmrdat
	.word	timer_num_to_clk_enum
.LFE6:
	.size	timer_ioctl, .-timer_ioctl
	.section	.text.timer_read,"ax",%progbits
	.align	2
	.global	timer_read
	.type	timer_read, %function
timer_read:
.LFB7:
	.loc 1 692 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI21:
	add	fp, sp, #0
.LCFI22:
	sub	sp, sp, #12
.LCFI23:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	str	r2, [fp, #-12]
	.loc 1 693 0
	mov	r3, #0
	.loc 1 694 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE7:
	.size	timer_read, .-timer_read
	.section	.text.timer_write,"ax",%progbits
	.align	2
	.global	timer_write
	.type	timer_write, %function
timer_write:
.LFB8:
	.loc 1 720 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI24:
	add	fp, sp, #0
.LCFI25:
	sub	sp, sp, #12
.LCFI26:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	str	r2, [fp, #-12]
	.loc 1 721 0
	mov	r3, #0
	.loc 1 722 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE8:
	.size	timer_write, .-timer_write
	.section	.text.timer_wait_ms,"ax",%progbits
	.align	2
	.global	timer_wait_ms
	.type	timer_wait_ms, %function
timer_wait_ms:
.LFB9:
	.loc 1 748 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #8
.LCFI29:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 749 0
	ldr	r3, [fp, #-12]
	mov	r2, #1000
	mul	r3, r2, r3
	ldr	r0, [fp, #-8]
	mov	r1, r3
	bl	timer_delay_cmn
	.loc 1 750 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE9:
	.size	timer_wait_ms, .-timer_wait_ms
	.section	.text.timer_wait_us,"ax",%progbits
	.align	2
	.global	timer_wait_us
	.type	timer_wait_us, %function
timer_wait_us:
.LFB10:
	.loc 1 776 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #8
.LCFI32:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 777 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	timer_delay_cmn
	.loc 1 778 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE10:
	.size	timer_wait_us, .-timer_wait_us
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer_driver.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x914
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF131
	.byte	0x1
	.4byte	.LASF132
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x61
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x67
	.4byte	0x73
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x70
	.4byte	0x85
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x61
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x2
	.byte	0xfd
	.4byte	0x68
	.uleb128 0x5
	.byte	0x74
	.byte	0x3
	.byte	0x28
	.4byte	0x154
	.uleb128 0x6
	.ascii	"ir\000"
	.byte	0x3
	.byte	0x2a
	.4byte	0x154
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.ascii	"tcr\000"
	.byte	0x3
	.byte	0x2b
	.4byte	0x154
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.ascii	"tc\000"
	.byte	0x3
	.byte	0x2c
	.4byte	0x154
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.ascii	"pr\000"
	.byte	0x3
	.byte	0x2d
	.4byte	0x154
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.ascii	"pc\000"
	.byte	0x3
	.byte	0x2e
	.4byte	0x154
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.ascii	"mcr\000"
	.byte	0x3
	.byte	0x2f
	.4byte	0x154
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.ascii	"mr\000"
	.byte	0x3
	.byte	0x30
	.4byte	0x169
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.ascii	"ccr\000"
	.byte	0x3
	.byte	0x31
	.4byte	0x154
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x6
	.ascii	"cr\000"
	.byte	0x3
	.byte	0x32
	.4byte	0x16e
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x6
	.ascii	"emr\000"
	.byte	0x3
	.byte	0x33
	.4byte	0x154
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x7
	.4byte	.LASF15
	.byte	0x3
	.byte	0x34
	.4byte	0x183
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x7
	.4byte	.LASF16
	.byte	0x3
	.byte	0x35
	.4byte	0x154
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.byte	0
	.uleb128 0x8
	.4byte	0x56
	.uleb128 0x9
	.4byte	0x56
	.4byte	0x169
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.4byte	0x159
	.uleb128 0x8
	.4byte	0x159
	.uleb128 0x9
	.4byte	0x56
	.4byte	0x183
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x8
	.4byte	0x173
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x3
	.byte	0x36
	.4byte	0xa9
	.uleb128 0x5
	.byte	0x8
	.byte	0x4
	.byte	0x26
	.4byte	0x1b8
	.uleb128 0x7
	.4byte	.LASF18
	.byte	0x4
	.byte	0x2b
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF19
	.byte	0x4
	.byte	0x2f
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x4
	.byte	0x30
	.4byte	0x193
	.uleb128 0x5
	.byte	0x10
	.byte	0x4
	.byte	0x34
	.4byte	0x204
	.uleb128 0x7
	.4byte	.LASF21
	.byte	0x4
	.byte	0x3a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF22
	.byte	0x4
	.byte	0x3f
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF23
	.byte	0x4
	.byte	0x44
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF24
	.byte	0x4
	.byte	0x47
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x4
	.byte	0x48
	.4byte	0x1c3
	.uleb128 0x5
	.byte	0x14
	.byte	0x4
	.byte	0x4b
	.4byte	0x25e
	.uleb128 0x7
	.4byte	.LASF26
	.byte	0x4
	.byte	0x4e
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF27
	.byte	0x4
	.byte	0x51
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF28
	.byte	0x4
	.byte	0x55
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF29
	.byte	0x4
	.byte	0x58
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF30
	.byte	0x4
	.byte	0x5b
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF31
	.byte	0x4
	.byte	0x5c
	.4byte	0x20f
	.uleb128 0x5
	.byte	0x10
	.byte	0x4
	.byte	0x5f
	.4byte	0x2aa
	.uleb128 0x7
	.4byte	.LASF26
	.byte	0x4
	.byte	0x62
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF32
	.byte	0x4
	.byte	0x65
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF33
	.byte	0x4
	.byte	0x69
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF34
	.byte	0x4
	.byte	0x6d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x3
	.4byte	.LASF35
	.byte	0x4
	.byte	0x6e
	.4byte	0x269
	.uleb128 0x5
	.byte	0xc
	.byte	0x4
	.byte	0x71
	.4byte	0x2e8
	.uleb128 0x7
	.4byte	.LASF26
	.byte	0x4
	.byte	0x75
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF36
	.byte	0x4
	.byte	0x79
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF37
	.byte	0x4
	.byte	0x7e
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF38
	.byte	0x4
	.byte	0x7f
	.4byte	0x2b5
	.uleb128 0x5
	.byte	0x18
	.byte	0x4
	.byte	0x82
	.4byte	0x326
	.uleb128 0x7
	.4byte	.LASF39
	.byte	0x4
	.byte	0x85
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF40
	.byte	0x4
	.byte	0x87
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF41
	.byte	0x4
	.byte	0x89
	.4byte	0x159
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF42
	.byte	0x4
	.byte	0x8a
	.4byte	0x2f3
	.uleb128 0xb
	.byte	0x4
	.byte	0x4
	.byte	0x8e
	.4byte	0x376
	.uleb128 0xc
	.4byte	.LASF43
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF44
	.sleb128 1
	.uleb128 0xc
	.4byte	.LASF45
	.sleb128 2
	.uleb128 0xc
	.4byte	.LASF46
	.sleb128 3
	.uleb128 0xc
	.4byte	.LASF47
	.sleb128 4
	.uleb128 0xc
	.4byte	.LASF48
	.sleb128 5
	.uleb128 0xc
	.4byte	.LASF49
	.sleb128 6
	.uleb128 0xc
	.4byte	.LASF50
	.sleb128 7
	.uleb128 0xc
	.4byte	.LASF51
	.sleb128 8
	.uleb128 0xc
	.4byte	.LASF52
	.sleb128 9
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.byte	0x4
	.byte	0xb3
	.4byte	0x391
	.uleb128 0xc
	.4byte	.LASF53
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF54
	.sleb128 1
	.uleb128 0xc
	.4byte	.LASF55
	.sleb128 2
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.byte	0x5
	.byte	0x24
	.4byte	0x472
	.uleb128 0xc
	.4byte	.LASF56
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF57
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF58
	.sleb128 1
	.uleb128 0xc
	.4byte	.LASF59
	.sleb128 2
	.uleb128 0xc
	.4byte	.LASF60
	.sleb128 3
	.uleb128 0xc
	.4byte	.LASF61
	.sleb128 4
	.uleb128 0xc
	.4byte	.LASF62
	.sleb128 5
	.uleb128 0xc
	.4byte	.LASF63
	.sleb128 6
	.uleb128 0xc
	.4byte	.LASF64
	.sleb128 7
	.uleb128 0xc
	.4byte	.LASF65
	.sleb128 8
	.uleb128 0xc
	.4byte	.LASF66
	.sleb128 9
	.uleb128 0xc
	.4byte	.LASF67
	.sleb128 10
	.uleb128 0xc
	.4byte	.LASF68
	.sleb128 11
	.uleb128 0xc
	.4byte	.LASF69
	.sleb128 12
	.uleb128 0xc
	.4byte	.LASF70
	.sleb128 13
	.uleb128 0xc
	.4byte	.LASF71
	.sleb128 14
	.uleb128 0xc
	.4byte	.LASF72
	.sleb128 15
	.uleb128 0xc
	.4byte	.LASF73
	.sleb128 16
	.uleb128 0xc
	.4byte	.LASF74
	.sleb128 17
	.uleb128 0xc
	.4byte	.LASF75
	.sleb128 18
	.uleb128 0xc
	.4byte	.LASF76
	.sleb128 19
	.uleb128 0xc
	.4byte	.LASF77
	.sleb128 20
	.uleb128 0xc
	.4byte	.LASF78
	.sleb128 21
	.uleb128 0xc
	.4byte	.LASF79
	.sleb128 22
	.uleb128 0xc
	.4byte	.LASF80
	.sleb128 23
	.uleb128 0xc
	.4byte	.LASF81
	.sleb128 24
	.uleb128 0xc
	.4byte	.LASF82
	.sleb128 25
	.uleb128 0xc
	.4byte	.LASF83
	.sleb128 26
	.uleb128 0xc
	.4byte	.LASF84
	.sleb128 27
	.uleb128 0xc
	.4byte	.LASF85
	.sleb128 28
	.uleb128 0xc
	.4byte	.LASF86
	.sleb128 29
	.uleb128 0xc
	.4byte	.LASF87
	.sleb128 30
	.uleb128 0xc
	.4byte	.LASF88
	.sleb128 31
	.uleb128 0xc
	.4byte	.LASF89
	.sleb128 32
	.uleb128 0xc
	.4byte	.LASF90
	.sleb128 33
	.uleb128 0xc
	.4byte	.LASF91
	.sleb128 34
	.byte	0
	.uleb128 0x3
	.4byte	.LASF92
	.byte	0x5
	.byte	0x49
	.4byte	0x391
	.uleb128 0x5
	.byte	0x4
	.byte	0x1
	.byte	0x23
	.4byte	0x494
	.uleb128 0x7
	.4byte	.LASF93
	.byte	0x1
	.byte	0x25
	.4byte	0x494
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xd
	.byte	0x4
	.4byte	0x188
	.uleb128 0x3
	.4byte	.LASF94
	.byte	0x1
	.byte	0x26
	.4byte	0x47d
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF99
	.byte	0x1
	.byte	0x51
	.byte	0x1
	.4byte	0x56
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x4fb
	.uleb128 0xf
	.4byte	.LASF95
	.byte	0x1
	.byte	0x51
	.4byte	0x472
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xf
	.4byte	.LASF96
	.byte	0x1
	.byte	0x52
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x10
	.4byte	.LASF97
	.byte	0x1
	.byte	0x54
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x10
	.4byte	.LASF98
	.byte	0x1
	.byte	0x55
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF100
	.byte	0x1
	.byte	0x73
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x535
	.uleb128 0xf
	.4byte	.LASF101
	.byte	0x1
	.byte	0x73
	.4byte	0x494
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LASF102
	.byte	0x1
	.byte	0x75
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF103
	.byte	0x1
	.byte	0xa6
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x56f
	.uleb128 0xf
	.4byte	.LASF104
	.byte	0x1
	.byte	0xa6
	.4byte	0x56f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LASF102
	.byte	0x1
	.byte	0xa8
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xd
	.byte	0x4
	.4byte	0x49a
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF124
	.byte	0x1
	.byte	0xda
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x5b9
	.uleb128 0xf
	.4byte	.LASF101
	.byte	0x1
	.byte	0xda
	.4byte	0x494
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xf
	.4byte	.LASF96
	.byte	0x1
	.byte	0xda
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x10
	.4byte	.LASF105
	.byte	0x1
	.byte	0xdc
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x121
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x623
	.uleb128 0x13
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x121
	.4byte	0x623
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.ascii	"arg\000"
	.byte	0x1
	.2byte	0x122
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x15
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x124
	.4byte	0x56f
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x125
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x15
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x125
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x16e
	.byte	0x1
	.4byte	0x9e
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x680
	.uleb128 0x13
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x16e
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x15
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x170
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x15
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x171
	.4byte	0x56f
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x172
	.4byte	0x9e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x1a7
	.byte	0x1
	.4byte	0x9e
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x781
	.uleb128 0x13
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x14
	.ascii	"cmd\000"
	.byte	0x1
	.2byte	0x1a8
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x14
	.ascii	"arg\000"
	.byte	0x1
	.2byte	0x1a9
	.4byte	0x68
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x17
	.ascii	"msk\000"
	.byte	0x1
	.2byte	0x1ab
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x17
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x1ab
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x15
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x1ac
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x1ad
	.4byte	0x56f
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x15
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x1ae
	.4byte	0x494
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x15
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x1af
	.4byte	0x781
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x15
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x1b0
	.4byte	0x787
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x15
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x1b1
	.4byte	0x78d
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x15
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x1b2
	.4byte	0x793
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x15
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x1b3
	.4byte	0x799
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x15
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x1b4
	.4byte	0x79f
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x15
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x1b5
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xd
	.byte	0x4
	.4byte	0x1b8
	.uleb128 0xd
	.byte	0x4
	.4byte	0x204
	.uleb128 0xd
	.byte	0x4
	.4byte	0x25e
	.uleb128 0xd
	.byte	0x4
	.4byte	0x2aa
	.uleb128 0xd
	.byte	0x4
	.4byte	0x2e8
	.uleb128 0xd
	.byte	0x4
	.4byte	0x326
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x2b1
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x7f1
	.uleb128 0x13
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x2b1
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x13
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x2b2
	.4byte	0x623
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x2b3
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF122
	.byte	0x1
	.2byte	0x2cd
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x83d
	.uleb128 0x13
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x2cd
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x13
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x2ce
	.4byte	0x623
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF123
	.byte	0x1
	.2byte	0x2cf
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF125
	.byte	0x1
	.2byte	0x2eb
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x876
	.uleb128 0x13
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x2eb
	.4byte	0x494
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x2eb
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF127
	.byte	0x1
	.2byte	0x307
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x8af
	.uleb128 0x13
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x307
	.4byte	0x494
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x307
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x9
	.4byte	0x93
	.4byte	0x8bf
	.uleb128 0xa
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x10
	.4byte	.LASF128
	.byte	0x1
	.byte	0x29
	.4byte	0x8af
	.byte	0x5
	.byte	0x3
	.4byte	tmr_init
	.uleb128 0x9
	.4byte	0x49a
	.4byte	0x8e0
	.uleb128 0xa
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x10
	.4byte	.LASF129
	.byte	0x1
	.byte	0x2c
	.4byte	0x8d0
	.byte	0x5
	.byte	0x3
	.4byte	tmrdat
	.uleb128 0x9
	.4byte	0x472
	.4byte	0x901
	.uleb128 0xa
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x10
	.4byte	.LASF130
	.byte	0x1
	.byte	0x2f
	.4byte	0x912
	.byte	0x5
	.byte	0x3
	.4byte	timer_num_to_clk_enum
	.uleb128 0x19
	.4byte	0x8f1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF54:
	.ascii	"TMR_VALUE_ST\000"
.LASF105:
	.ascii	"timernum\000"
.LASF61:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF123:
	.ascii	"n_bytes\000"
.LASF121:
	.ascii	"max_bytes\000"
.LASF73:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF77:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF103:
	.ascii	"timer_cfg_to_timer_num\000"
.LASF85:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF58:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF31:
	.ascii	"TMR_MATCH_SETUP_T\000"
.LASF46:
	.ascii	"TMR_SETUP_CLKIN\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF63:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF12:
	.ascii	"long long int\000"
.LASF4:
	.ascii	"signed char\000"
.LASF116:
	.ascii	"pstpcap\000"
.LASF49:
	.ascii	"TMR_SETUP_MATCHOUT\000"
.LASF56:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF126:
	.ascii	"msec\000"
.LASF38:
	.ascii	"TMR_MATCH_GEN_T\000"
.LASF47:
	.ascii	"TMR_SETUP_MATCH\000"
.LASF1:
	.ascii	"long int\000"
.LASF109:
	.ascii	"timer_close\000"
.LASF82:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF89:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF113:
	.ascii	"ppstp\000"
.LASF128:
	.ascii	"tmr_init\000"
.LASF69:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF98:
	.ascii	"longcnt\000"
.LASF108:
	.ascii	"tptr\000"
.LASF93:
	.ascii	"regptr\000"
.LASF107:
	.ascii	"ipbase\000"
.LASF124:
	.ascii	"timer_delay_cmn\000"
.LASF33:
	.ascii	"cap_on_rising\000"
.LASF10:
	.ascii	"UNS_64\000"
.LASF27:
	.ascii	"use_match_int\000"
.LASF75:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF66:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF122:
	.ascii	"timer_write\000"
.LASF9:
	.ascii	"INT_32\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF95:
	.ascii	"clknum\000"
.LASF101:
	.ascii	"pTimer\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF131:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF104:
	.ascii	"pTimerCfg\000"
.LASF99:
	.ascii	"timer_usec_to_val\000"
.LASF91:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF78:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF71:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF57:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF86:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF50:
	.ascii	"TMR_CLEAR_INTS\000"
.LASF67:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF106:
	.ascii	"timer_open\000"
.LASF100:
	.ascii	"timer_ptr_to_timer_num\000"
.LASF26:
	.ascii	"timer_num\000"
.LASF37:
	.ascii	"matn_state_ctrl\000"
.LASF59:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF87:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF18:
	.ascii	"ps_tick_val\000"
.LASF36:
	.ascii	"matn_state\000"
.LASF132:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_timer_driver.c\000"
.LASF53:
	.ascii	"TMR_GET_CLOCK\000"
.LASF115:
	.ascii	"pmstp\000"
.LASF96:
	.ascii	"usec\000"
.LASF45:
	.ascii	"TMR_SETUP_PSCALE\000"
.LASF97:
	.ascii	"clkdlycnt\000"
.LASF125:
	.ascii	"timer_wait_ms\000"
.LASF17:
	.ascii	"TIMER_CNTR_REGS_T\000"
.LASF28:
	.ascii	"stop_on_match\000"
.LASF42:
	.ascii	"TMR_COUNTS_T\000"
.LASF41:
	.ascii	"cap_count_val\000"
.LASF127:
	.ascii	"timer_wait_us\000"
.LASF22:
	.ascii	"use_cap0_pos\000"
.LASF24:
	.ascii	"cap_input\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF23:
	.ascii	"use_cap0_neg\000"
.LASF130:
	.ascii	"timer_num_to_clk_enum\000"
.LASF52:
	.ascii	"TMR_GET_STATUS\000"
.LASF6:
	.ascii	"short int\000"
.LASF15:
	.ascii	"rsvd2\000"
.LASF62:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF19:
	.ascii	"ps_us_val\000"
.LASF76:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF44:
	.ascii	"TMR_RESET\000"
.LASF51:
	.ascii	"TMR_GET_COUNTS\000"
.LASF83:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF29:
	.ascii	"reset_on_match\000"
.LASF102:
	.ascii	"tnum\000"
.LASF34:
	.ascii	"cap_on_falling\000"
.LASF39:
	.ascii	"count_val\000"
.LASF2:
	.ascii	"char\000"
.LASF120:
	.ascii	"buffer\000"
.LASF64:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF21:
	.ascii	"use_pclk\000"
.LASF117:
	.ascii	"pmgen\000"
.LASF72:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF32:
	.ascii	"use_capture_int\000"
.LASF40:
	.ascii	"ps_count_val\000"
.LASF92:
	.ascii	"CLKPWR_CLK_T\000"
.LASF25:
	.ascii	"TMR_INPUT_CLK_T\000"
.LASF81:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF90:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF114:
	.ascii	"pclkins\000"
.LASF55:
	.ascii	"TMR_INT_PEND\000"
.LASF68:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF16:
	.ascii	"ctcr\000"
.LASF84:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF110:
	.ascii	"devid\000"
.LASF14:
	.ascii	"STATUS\000"
.LASF94:
	.ascii	"TIMER_CFG_T\000"
.LASF111:
	.ascii	"status\000"
.LASF79:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF60:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF88:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF70:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF43:
	.ascii	"TMR_ENABLE\000"
.LASF118:
	.ascii	"ptcnts\000"
.LASF48:
	.ascii	"TMR_SETUP_CAPTURE\000"
.LASF80:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF112:
	.ascii	"timer_ioctl\000"
.LASF35:
	.ascii	"TMR_CAP_CLOCK_CTRL_T\000"
.LASF30:
	.ascii	"match_tick_val\000"
.LASF119:
	.ascii	"timer_read\000"
.LASF65:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF74:
	.ascii	"CLKPWR_WDOG_CLK\000"
.LASF129:
	.ascii	"tmrdat\000"
.LASF20:
	.ascii	"TMR_PSCALE_SETUP_T\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
