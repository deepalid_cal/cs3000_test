	.file	"change.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.text.Alert_ChangeLine,"ax",%progbits
	.align	2
	.type	Alert_ChangeLine, %function
Alert_ChangeLine:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/alerts/change.c"
	.loc 1 69 0
	@ args = 20, pretend = 0, frame = 128
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #128
.LCFI2:
	str	r0, [fp, #-120]
	str	r1, [fp, #-124]
	str	r2, [fp, #-128]
	str	r3, [fp, #-132]
	.loc 1 79 0
	sub	r2, fp, #112
	sub	r3, fp, #16
	mov	r0, r2
	ldr	r1, [fp, #-120]
	mov	r2, r3
	bl	ALERTS_load_common
	str	r0, [fp, #-8]
	.loc 1 81 0
	add	r2, fp, #16
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	mov	r1, r2
	mov	r2, #4
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
	.loc 1 82 0
	add	r2, fp, #20
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	mov	r1, r2
	mov	r2, #4
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
	.loc 1 84 0
	ldr	r2, [fp, #-120]
	ldr	r3, .L7
	cmp	r2, r3
	bls	.L2
	.loc 1 84 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-120]
	ldr	r3, .L7+4
	cmp	r2, r3
	bhi	.L2
	.loc 1 86 0 is_stmt 1
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-124]
	mov	r2, #48
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
	b	.L3
.L2:
	.loc 1 88 0
	ldr	r2, [fp, #-120]
	ldr	r3, .L7+4
	cmp	r2, r3
	bls	.L4
	.loc 1 88 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-120]
	ldr	r3, .L7+8
	cmp	r2, r3
	bhi	.L4
	.loc 1 90 0 is_stmt 1
	sub	r2, fp, #128
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	mov	r1, r2
	mov	r2, #4
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
	.loc 1 91 0
	sub	r2, fp, #132
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	mov	r1, r2
	mov	r2, #4
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
	b	.L3
.L4:
	.loc 1 95 0
	sub	r2, fp, #128
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	mov	r1, r2
	mov	r2, #4
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
.L3:
	.loc 1 103 0
	ldr	r3, [fp, #12]
	cmp	r3, #8
	bhi	.L5
	.loc 1 105 0
	add	r2, fp, #12
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	mov	r1, r2
	mov	r2, #4
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
	.loc 1 108 0
	ldr	r2, [fp, #12]
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #4]
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
	.loc 1 111 0
	ldr	r2, [fp, #12]
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #8]
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
	b	.L6
.L5:
	.loc 1 116 0
	mov	r3, #0
	str	r3, [fp, #-116]
	.loc 1 117 0
	sub	r2, fp, #116
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	mov	r1, r2
	mov	r2, #4
	bl	ALERTS_load_object
	str	r0, [fp, #-8]
.L6:
	.loc 1 120 0
	ldr	r0, [fp, #-120]
	sub	r2, fp, #16
	ldmia	r2, {r1-r2}
	bl	ALERTS_add_alert
	.loc 1 121 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L8:
	.align	2
.L7:
	.word	49151
	.word	53247
	.word	57343
.LFE0:
	.size	Alert_ChangeLine, .-Alert_ChangeLine
	.section	.text.Alert_ChangeLine_Controller,"ax",%progbits
	.align	2
	.global	Alert_ChangeLine_Controller
	.type	Alert_ChangeLine_Controller, %function
Alert_ChangeLine_Controller:
.LFB1:
	.loc 1 154 0
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #36
.LCFI5:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 155 0
	ldr	r3, [fp, #-16]
	str	r3, [sp, #0]
	ldr	r3, [fp, #-20]
	str	r3, [sp, #4]
	ldr	r3, [fp, #4]
	str	r3, [sp, #8]
	ldr	r3, [fp, #8]
	str	r3, [sp, #12]
	ldr	r3, [fp, #12]
	str	r3, [sp, #16]
	ldr	r0, [fp, #-8]
	mov	r1, #0
	ldr	r2, [fp, #-12]
	mov	r3, #0
	bl	Alert_ChangeLine
	.loc 1 156 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	Alert_ChangeLine_Controller, .-Alert_ChangeLine_Controller
	.section	.text.Alert_ChangeLine_Group,"ax",%progbits
	.align	2
	.global	Alert_ChangeLine_Group
	.type	Alert_ChangeLine_Group, %function
Alert_ChangeLine_Group:
.LFB2:
	.loc 1 184 0
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #36
.LCFI8:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 185 0
	ldr	r3, [fp, #-16]
	str	r3, [sp, #0]
	ldr	r3, [fp, #-20]
	str	r3, [sp, #4]
	ldr	r3, [fp, #4]
	str	r3, [sp, #8]
	ldr	r3, [fp, #8]
	str	r3, [sp, #12]
	ldr	r3, [fp, #12]
	str	r3, [sp, #16]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, #0
	mov	r3, #0
	bl	Alert_ChangeLine
	.loc 1 186 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE2:
	.size	Alert_ChangeLine_Group, .-Alert_ChangeLine_Group
	.section	.text.Alert_ChangeLine_Station,"ax",%progbits
	.align	2
	.global	Alert_ChangeLine_Station
	.type	Alert_ChangeLine_Station, %function
Alert_ChangeLine_Station:
.LFB3:
	.loc 1 217 0
	@ args = 16, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #36
.LCFI11:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 218 0
	ldr	r3, [fp, #-20]
	str	r3, [sp, #0]
	ldr	r3, [fp, #4]
	str	r3, [sp, #4]
	ldr	r3, [fp, #8]
	str	r3, [sp, #8]
	ldr	r3, [fp, #12]
	str	r3, [sp, #12]
	ldr	r3, [fp, #16]
	str	r3, [sp, #16]
	ldr	r0, [fp, #-8]
	mov	r1, #0
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-16]
	bl	Alert_ChangeLine
	.loc 1 219 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE3:
	.size	Alert_ChangeLine_Station, .-Alert_ChangeLine_Station
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x400
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF37
	.byte	0x1
	.4byte	.LASF38
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x5
	.4byte	0x37
	.4byte	0x98
	.uleb128 0x6
	.4byte	0x7a
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0x53
	.4byte	0xa8
	.uleb128 0x6
	.4byte	0x7a
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.byte	0x3
	.byte	0x14
	.4byte	0xcd
	.uleb128 0x8
	.4byte	.LASF12
	.byte	0x3
	.byte	0x17
	.4byte	0xcd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF13
	.byte	0x3
	.byte	0x1a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x9
	.byte	0x4
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x1c
	.4byte	0xa8
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF15
	.uleb128 0x5
	.4byte	0x53
	.4byte	0xf5
	.uleb128 0x6
	.4byte	0x7a
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF16
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x1
	.byte	0x3c
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1d1
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x1
	.byte	0x3c
	.4byte	0x1d1
	.byte	0x3
	.byte	0x91
	.sleb128 -124
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x1
	.byte	0x3d
	.4byte	0x1d6
	.byte	0x3
	.byte	0x91
	.sleb128 -128
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x1
	.byte	0x3e
	.4byte	0x1d1
	.byte	0x3
	.byte	0x91
	.sleb128 -132
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x1
	.byte	0x3f
	.4byte	0x1d1
	.byte	0x3
	.byte	0x91
	.sleb128 -136
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x1
	.byte	0x40
	.4byte	0x1e1
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x1
	.byte	0x41
	.4byte	0x1e1
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x1
	.byte	0x42
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x1
	.byte	0x43
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x1
	.byte	0x44
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 16
	.uleb128 0xc
	.ascii	"ldh\000"
	.byte	0x1
	.byte	0x46
	.4byte	0xd3
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xd
	.4byte	.LASF26
	.byte	0x1
	.byte	0x48
	.4byte	0x1ed
	.byte	0x3
	.byte	0x91
	.sleb128 -116
	.uleb128 0xc
	.ascii	"ucp\000"
	.byte	0x1
	.byte	0x4a
	.4byte	0xcd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xd
	.4byte	.LASF27
	.byte	0x1
	.byte	0x4c
	.4byte	0x53
	.byte	0x3
	.byte	0x91
	.sleb128 -120
	.byte	0
	.uleb128 0xe
	.4byte	0x53
	.uleb128 0x9
	.byte	0x4
	.4byte	0x1dc
	.uleb128 0xe
	.4byte	0x25
	.uleb128 0xe
	.4byte	0x1e6
	.uleb128 0x9
	.byte	0x4
	.4byte	0x1ec
	.uleb128 0xf
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0x1fd
	.uleb128 0x6
	.4byte	0x7a
	.byte	0x5f
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.byte	0x99
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x279
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x1
	.byte	0x99
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x1
	.byte	0x99
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x1
	.byte	0x99
	.4byte	0x1e1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x1
	.byte	0x99
	.4byte	0x1e1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x1
	.byte	0x99
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x1
	.byte	0x99
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x1
	.byte	0x99
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.byte	0xb7
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x2f5
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x1
	.byte	0xb7
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x1
	.byte	0xb7
	.4byte	0x1d6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x1
	.byte	0xb7
	.4byte	0x1e1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x1
	.byte	0xb7
	.4byte	0x1e1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x1
	.byte	0xb7
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x1
	.byte	0xb7
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x1
	.byte	0xb7
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.byte	0xd8
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x37f
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x1
	.byte	0xd8
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x1
	.byte	0xd8
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x1
	.byte	0xd8
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x1
	.byte	0xd8
	.4byte	0x1e1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x1
	.byte	0xd8
	.4byte	0x1e1
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x1
	.byte	0xd8
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x1
	.byte	0xd8
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x1
	.byte	0xd8
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.byte	0
	.uleb128 0xd
	.4byte	.LASF31
	.byte	0x4
	.byte	0x30
	.4byte	0x390
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0x88
	.uleb128 0xd
	.4byte	.LASF32
	.byte	0x4
	.byte	0x34
	.4byte	0x3a6
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0x88
	.uleb128 0xd
	.4byte	.LASF33
	.byte	0x4
	.byte	0x36
	.4byte	0x3bc
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0x88
	.uleb128 0xd
	.4byte	.LASF34
	.byte	0x4
	.byte	0x38
	.4byte	0x3d2
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0x88
	.uleb128 0xd
	.4byte	.LASF35
	.byte	0x5
	.byte	0x33
	.4byte	0x3e8
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xe
	.4byte	0x98
	.uleb128 0xd
	.4byte	.LASF36
	.byte	0x5
	.byte	0x3f
	.4byte	0x3fe
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xe
	.4byte	0xe5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF31:
	.ascii	"GuiFont_LanguageActive\000"
.LASF17:
	.ascii	"paid\000"
.LASF37:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF39:
	.ascii	"Alert_ChangeLine\000"
.LASF18:
	.ascii	"pgroup_name\000"
.LASF27:
	.ascii	"lsize\000"
.LASF23:
	.ascii	"psize\000"
.LASF15:
	.ascii	"float\000"
.LASF38:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/aler"
	.ascii	"ts/change.c\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF14:
	.ascii	"DATA_HANDLE\000"
.LASF13:
	.ascii	"dlen\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF10:
	.ascii	"long unsigned int\000"
.LASF34:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF35:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF32:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF16:
	.ascii	"double\000"
.LASF5:
	.ascii	"UNS_8\000"
.LASF25:
	.ascii	"pbox_index_of_initiator_0\000"
.LASF24:
	.ascii	"preason\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF19:
	.ascii	"pbox_index_0\000"
.LASF12:
	.ascii	"dptr\000"
.LASF36:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF30:
	.ascii	"Alert_ChangeLine_Station\000"
.LASF20:
	.ascii	"pstation_number_0\000"
.LASF9:
	.ascii	"long long int\000"
.LASF0:
	.ascii	"char\000"
.LASF29:
	.ascii	"Alert_ChangeLine_Group\000"
.LASF33:
	.ascii	"GuiFont_DecimalChar\000"
.LASF4:
	.ascii	"short int\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF11:
	.ascii	"long int\000"
.LASF28:
	.ascii	"Alert_ChangeLine_Controller\000"
.LASF22:
	.ascii	"pto_ptr\000"
.LASF26:
	.ascii	"lalert\000"
.LASF2:
	.ascii	"signed char\000"
.LASF21:
	.ascii	"pfrom_ptr\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
