	.file	"lpc_lcd_params.c"
	.text
.Ltext0:
	.global	calsense_4bit_grey16
	.section	.rodata.calsense_4bit_grey16,"a",%progbits
	.align	2
	.type	calsense_4bit_grey16, %object
	.size	calsense_4bit_grey16, 40
calsense_4bit_grey16:
	.byte	1
	.byte	1
	.byte	1
	.space	1
	.short	320
	.byte	0
	.byte	0
	.byte	1
	.space	1
	.short	240
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.space	2
	.word	1450000
	.word	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.space	1
	.short	0
	.space	2
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/lpc_lcd_params.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/lpc_lcd_params.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x231
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF42
	.byte	0x1
	.4byte	.LASF43
	.4byte	.Ltext0
	.4byte	.Letext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0x3a
	.4byte	0x33
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0x4c
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x1
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x5
	.byte	0x4
	.byte	0x2
	.byte	0x2f
	.4byte	0xae
	.uleb128 0x6
	.ascii	"TFT\000"
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF11
	.sleb128 1
	.uleb128 0x7
	.4byte	.LASF12
	.sleb128 2
	.uleb128 0x7
	.4byte	.LASF13
	.sleb128 3
	.uleb128 0x7
	.4byte	.LASF14
	.sleb128 4
	.uleb128 0x7
	.4byte	.LASF15
	.sleb128 5
	.byte	0
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x2
	.byte	0x36
	.4byte	0x81
	.uleb128 0x8
	.byte	0x28
	.byte	0x2
	.byte	0x39
	.4byte	0x212
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x2
	.byte	0x3b
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x2
	.byte	0x3d
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x2
	.byte	0x3f
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x2
	.byte	0x41
	.4byte	0x41
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x2
	.byte	0x43
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x2
	.byte	0x45
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x7
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x2
	.byte	0x47
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x2
	.byte	0x49
	.4byte	0x41
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x2
	.byte	0x4b
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x2
	.byte	0x4d
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x2
	.byte	0x4f
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x2
	.byte	0x50
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0xf
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x2
	.byte	0x51
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x2
	.byte	0x53
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x2
	.byte	0x55
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x2
	.byte	0x56
	.4byte	0xae
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x9
	.4byte	.LASF33
	.byte	0x2
	.byte	0x57
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x9
	.4byte	.LASF34
	.byte	0x2
	.byte	0x5c
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x1d
	.uleb128 0x9
	.4byte	.LASF35
	.byte	0x2
	.byte	0x5e
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x1e
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x2
	.byte	0x60
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x1f
	.uleb128 0x9
	.4byte	.LASF37
	.byte	0x2
	.byte	0x62
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x9
	.4byte	.LASF38
	.byte	0x2
	.byte	0x64
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x21
	.uleb128 0x9
	.4byte	.LASF39
	.byte	0x2
	.byte	0x65
	.4byte	0x28
	.byte	0x2
	.byte	0x23
	.uleb128 0x22
	.uleb128 0x9
	.4byte	.LASF40
	.byte	0x2
	.byte	0x68
	.4byte	0x41
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x3
	.4byte	.LASF41
	.byte	0x2
	.byte	0x69
	.4byte	0xb9
	.uleb128 0xa
	.4byte	.LASF44
	.byte	0x3
	.byte	0x17
	.4byte	0x22f
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	calsense_4bit_grey16
	.uleb128 0xb
	.4byte	0x212
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF29:
	.ascii	"ac_bias_frequency\000"
.LASF44:
	.ascii	"calsense_4bit_grey16\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF20:
	.ascii	"pixels_per_line\000"
.LASF34:
	.ascii	"hrtft_cls_enable\000"
.LASF22:
	.ascii	"v_front_porch\000"
.LASF31:
	.ascii	"optimal_clock\000"
.LASF26:
	.ascii	"invert_panel_clock\000"
.LASF25:
	.ascii	"invert_output_enable\000"
.LASF17:
	.ascii	"h_back_porch\000"
.LASF33:
	.ascii	"dual_panel\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF12:
	.ascii	"HRTFT\000"
.LASF28:
	.ascii	"invert_vsync\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF16:
	.ascii	"LCD_PANEL_T\000"
.LASF39:
	.ascii	"hrtft_spl_delay\000"
.LASF36:
	.ascii	"hrtft_lp_to_ps_delay\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF15:
	.ascii	"CSTN\000"
.LASF21:
	.ascii	"v_back_porch\000"
.LASF40:
	.ascii	"hrtft_spl_to_cls_delay\000"
.LASF24:
	.ascii	"lines_per_panel\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF35:
	.ascii	"hrtft_sps_enable\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF0:
	.ascii	"char\000"
.LASF38:
	.ascii	"hrtft_lp_delay\000"
.LASF14:
	.ascii	"MONO_8BIT\000"
.LASF41:
	.ascii	"LCD_PARAM_T\000"
.LASF30:
	.ascii	"bits_per_pixel\000"
.LASF10:
	.ascii	"long long int\000"
.LASF27:
	.ascii	"invert_hsync\000"
.LASF23:
	.ascii	"v_sync_pulse_width\000"
.LASF37:
	.ascii	"hrtft_polarity_delay\000"
.LASF18:
	.ascii	"h_front_porch\000"
.LASF6:
	.ascii	"short int\000"
.LASF13:
	.ascii	"MONO_4BIT\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF42:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF19:
	.ascii	"h_sync_pulse_width\000"
.LASF11:
	.ascii	"ADTFT\000"
.LASF2:
	.ascii	"signed char\000"
.LASF32:
	.ascii	"lcd_panel_type\000"
.LASF43:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/"
	.ascii	"lpc_lcd_params.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
