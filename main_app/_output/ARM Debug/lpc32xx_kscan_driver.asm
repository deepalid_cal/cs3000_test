	.file	"lpc32xx_kscan_driver.c"
	.text
.Ltext0:
	.section	.bss.kscandat,"aw",%nobits
	.align	2
	.type	kscandat, %object
	.size	kscandat, 8
kscandat:
	.space	8
	.section	.text.kscan_open,"ax",%progbits
	.align	2
	.global	kscan_open
	.type	kscan_open, %function
kscan_open:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_kscan_driver.c"
	.loc 1 68 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #12
.LCFI2:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 69 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 71 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L3
	cmp	r2, r3
	bne	.L2
	.loc 1 74 0
	ldr	r3, .L3+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L2
	.loc 1 78 0
	ldr	r3, .L3+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 81 0
	ldr	r3, .L3+4
	ldr	r2, .L3
	str	r2, [r3, #4]
	.loc 1 84 0
	mov	r0, #12
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 90 0
	ldr	r3, .L3+4
	ldr	r3, [r3, #4]
	mov	r2, #255
	str	r2, [r3, #0]
	.loc 1 91 0
	ldr	r3, .L3+4
	ldr	r3, [r3, #4]
	mov	r2, #255
	str	r2, [r3, #12]
	.loc 1 92 0
	ldr	r3, .L3+4
	ldr	r3, [r3, #4]
	mov	r2, #2
	str	r2, [r3, #16]
	.loc 1 93 0
	ldr	r3, .L3+4
	ldr	r3, [r3, #4]
	mov	r2, #1
	str	r2, [r3, #20]
	.loc 1 94 0
	ldr	r3, .L3+4
	ldr	r3, [r3, #4]
	mov	r2, #1
	str	r2, [r3, #8]
	.loc 1 98 0
	ldr	r3, .L3+4
	ldr	r3, [r3, #4]
	ldr	r2, .L3+4
	ldr	r2, [r2, #4]
	ldr	r2, [r2, #16]
	orr	r2, r2, #2
	str	r2, [r3, #16]
	.loc 1 100 0
	ldr	r3, .L3+4
	str	r3, [fp, #-8]
.L2:
	.loc 1 104 0
	ldr	r3, [fp, #-8]
	.loc 1 105 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L4:
	.align	2
.L3:
	.word	1074069504
	.word	kscandat
.LFE0:
	.size	kscan_open, .-kscan_open
	.section	.text.kscan_close,"ax",%progbits
	.align	2
	.global	kscan_close
	.type	kscan_close, %function
kscan_close:
.LFB1:
	.loc 1 129 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-12]
	.loc 1 130 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	.loc 1 132 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L7
	cmp	r2, r3
	bne	.L6
	.loc 1 134 0
	ldr	r3, .L7
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L6
	.loc 1 137 0
	ldr	r3, .L7
	ldr	r3, [r3, #4]
	mov	r2, #1
	str	r2, [r3, #8]
	.loc 1 140 0
	ldr	r3, .L7
	ldr	r3, [r3, #4]
	ldr	r2, .L7
	ldr	r2, [r2, #4]
	ldr	r2, [r2, #16]
	bic	r2, r2, #2
	str	r2, [r3, #16]
	.loc 1 141 0
	mov	r0, #12
	mov	r1, #0
	bl	clkpwr_clk_en_dis
	.loc 1 144 0
	ldr	r3, .L7
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 147 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L6:
	.loc 1 151 0
	ldr	r3, [fp, #-8]
	.loc 1 152 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L8:
	.align	2
.L7:
	.word	kscandat
.LFE1:
	.size	kscan_close, .-kscan_close
	.section	.text.kscan_ioctl,"ax",%progbits
	.align	2
	.global	kscan_ioctl
	.type	kscan_ioctl, %function
kscan_ioctl:
.LFB2:
	.loc 1 179 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #20
.LCFI8:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	.loc 1 181 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	.loc 1 183 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L32
	cmp	r2, r3
	bne	.L10
	.loc 1 185 0
	ldr	r3, .L32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L10
	.loc 1 187 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 189 0
	ldr	r3, [fp, #-20]
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L11
.L16:
	.word	.L12
	.word	.L13
	.word	.L14
	.word	.L15
.L12:
	.loc 1 192 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-12]
	.loc 1 193 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	cmp	r3, #8
	bhi	.L17
	.loc 1 195 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bhi	.L18
	.loc 1 197 0
	ldr	r3, [fp, #-12]
	mov	r2, #2
	str	r2, [r3, #0]
.L18:
	.loc 1 200 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	and	r2, r2, #255
	str	r2, [r3, #0]
	.loc 1 202 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #4]
	and	r2, r2, #255
	str	r2, [r3, #12]
	.loc 1 204 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #12]
	and	r2, r2, #15
	str	r2, [r3, #20]
	.loc 1 216 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	mov	r2, #2
	str	r2, [r3, #16]
	.loc 1 223 0
	b	.L10
.L17:
	.loc 1 221 0
	mvn	r3, #6
	str	r3, [fp, #-8]
	.loc 1 223 0
	b	.L10
.L13:
	.loc 1 226 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	mov	r2, #1
	str	r2, [r3, #8]
	.loc 1 227 0
	b	.L10
.L14:
	.loc 1 230 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L20
	.loc 1 232 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	ldr	r2, .L32
	ldr	r2, [r2, #4]
	ldr	r2, [r2, #16]
	orr	r2, r2, #1
	str	r2, [r3, #16]
	.loc 1 240 0
	b	.L10
.L20:
	.loc 1 237 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	ldr	r2, .L32
	ldr	r2, [r2, #4]
	ldr	r2, [r2, #16]
	bic	r2, r2, #1
	str	r2, [r3, #16]
	.loc 1 240 0
	b	.L10
.L15:
	.loc 1 244 0
	ldr	r3, [fp, #-24]
	cmp	r3, #1
	beq	.L24
	cmp	r3, #2
	beq	.L25
	cmp	r3, #0
	bne	.L31
.L23:
	.loc 1 247 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #16]
	and	r3, r3, #2
	cmp	r3, #0
	bne	.L26
	.loc 1 251 0
	mov	r0, #5
	bl	clkpwr_get_base_clock_rate
	mov	r3, r0
	str	r3, [fp, #-8]
	.loc 1 258 0
	b	.L28
.L26:
	.loc 1 256 0
	mov	r3, #32768
	str	r3, [fp, #-8]
	.loc 1 258 0
	b	.L28
.L24:
	.loc 1 261 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #8]
	.loc 1 262 0
	and	r3, r3, #1
	and	r3, r3, #255
	.loc 1 261 0
	cmp	r3, #0
	beq	.L29
	.loc 1 264 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 270 0
	b	.L28
.L29:
	.loc 1 268 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 270 0
	b	.L28
.L25:
	.loc 1 274 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #4]
	.loc 1 273 0
	str	r3, [fp, #-8]
	.loc 1 275 0
	b	.L28
.L31:
	.loc 1 279 0
	mvn	r3, #6
	str	r3, [fp, #-8]
	.loc 1 280 0
	mov	r0, r0	@ nop
.L28:
	.loc 1 282 0
	b	.L10
.L11:
	.loc 1 286 0
	mvn	r3, #6
	str	r3, [fp, #-8]
.L10:
	.loc 1 291 0
	ldr	r3, [fp, #-8]
	.loc 1 292 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L33:
	.align	2
.L32:
	.word	kscandat
.LFE2:
	.size	kscan_ioctl, .-kscan_ioctl
	.section	.text.kscan_read,"ax",%progbits
	.align	2
	.global	kscan_read
	.type	kscan_read, %function
kscan_read:
.LFB3:
	.loc 1 318 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #24
.LCFI11:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	.loc 1 319 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 320 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-8]
	.loc 1 323 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L39
	cmp	r2, r3
	bne	.L35
	.loc 1 325 0
	ldr	r3, .L39
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L35
	.loc 1 327 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 330 0
	ldr	r3, [fp, #-24]
	cmp	r3, #8
	ble	.L36
	.loc 1 332 0
	mov	r3, #8
	str	r3, [fp, #-24]
.L36:
	.loc 1 336 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L37
.L38:
	.loc 1 338 0 discriminator 2
	ldr	r3, .L39
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-12]
	add	r2, r2, #16
	ldr	r3, [r3, r2, asl #2]
	and	r2, r3, #255
	ldr	r3, [fp, #-8]
	strb	r2, [r3, #0]
	.loc 1 339 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 336 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L37:
	.loc 1 336 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	blt	.L38
	.loc 1 342 0 is_stmt 1
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-4]
.L35:
	.loc 1 346 0
	ldr	r3, [fp, #-4]
	.loc 1 347 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L40:
	.align	2
.L39:
	.word	kscandat
.LFE3:
	.size	kscan_read, .-kscan_read
	.section	.text.kscan_write,"ax",%progbits
	.align	2
	.global	kscan_write
	.type	kscan_write, %function
kscan_write:
.LFB4:
	.loc 1 373 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI12:
	add	fp, sp, #0
.LCFI13:
	sub	sp, sp, #12
.LCFI14:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	str	r2, [fp, #-12]
	.loc 1 374 0
	mov	r3, #0
	.loc 1 375 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE4:
	.size	kscan_write, .-kscan_write
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_kscan.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_kscan_driver.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x50d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF101
	.byte	0x1
	.4byte	.LASF102
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x3a
	.4byte	0x45
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x6c
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x67
	.4byte	0x7e
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x6c
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x2
	.byte	0xfd
	.4byte	0x73
	.uleb128 0x5
	.byte	0x60
	.byte	0x3
	.byte	0x28
	.4byte	0x122
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x2a
	.4byte	0x122
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x3
	.byte	0x2b
	.4byte	0x122
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x3
	.byte	0x2c
	.4byte	0x122
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x3
	.byte	0x2d
	.4byte	0x122
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x3
	.byte	0x2e
	.4byte	0x122
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF20
	.byte	0x3
	.byte	0x2f
	.4byte	0x122
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF21
	.byte	0x3
	.byte	0x30
	.4byte	0x137
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF22
	.byte	0x3
	.byte	0x31
	.4byte	0x14c
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.byte	0
	.uleb128 0x7
	.4byte	0x61
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x137
	.uleb128 0x9
	.4byte	0x25
	.byte	0x9
	.byte	0
	.uleb128 0x7
	.4byte	0x127
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x14c
	.uleb128 0x9
	.4byte	0x25
	.byte	0x7
	.byte	0
	.uleb128 0x7
	.4byte	0x13c
	.uleb128 0x3
	.4byte	.LASF23
	.byte	0x3
	.byte	0x32
	.4byte	0xa9
	.uleb128 0x5
	.byte	0x10
	.byte	0x4
	.byte	0x2b
	.4byte	0x19d
	.uleb128 0x6
	.4byte	.LASF24
	.byte	0x4
	.byte	0x31
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x4
	.byte	0x36
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x4
	.byte	0x39
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x4
	.byte	0x3c
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x4
	.byte	0x3d
	.4byte	0x15c
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0x4a
	.4byte	0x1c9
	.uleb128 0xb
	.4byte	.LASF29
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF30
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF31
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF32
	.sleb128 3
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0x5b
	.4byte	0x1e4
	.uleb128 0xb
	.4byte	.LASF33
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF34
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF35
	.sleb128 2
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.byte	0x5
	.byte	0x24
	.4byte	0x2c5
	.uleb128 0xb
	.4byte	.LASF36
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF37
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF38
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF39
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF40
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF41
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF42
	.sleb128 5
	.uleb128 0xb
	.4byte	.LASF43
	.sleb128 6
	.uleb128 0xb
	.4byte	.LASF44
	.sleb128 7
	.uleb128 0xb
	.4byte	.LASF45
	.sleb128 8
	.uleb128 0xb
	.4byte	.LASF46
	.sleb128 9
	.uleb128 0xb
	.4byte	.LASF47
	.sleb128 10
	.uleb128 0xb
	.4byte	.LASF48
	.sleb128 11
	.uleb128 0xb
	.4byte	.LASF49
	.sleb128 12
	.uleb128 0xb
	.4byte	.LASF50
	.sleb128 13
	.uleb128 0xb
	.4byte	.LASF51
	.sleb128 14
	.uleb128 0xb
	.4byte	.LASF52
	.sleb128 15
	.uleb128 0xb
	.4byte	.LASF53
	.sleb128 16
	.uleb128 0xb
	.4byte	.LASF54
	.sleb128 17
	.uleb128 0xb
	.4byte	.LASF55
	.sleb128 18
	.uleb128 0xb
	.4byte	.LASF56
	.sleb128 19
	.uleb128 0xb
	.4byte	.LASF57
	.sleb128 20
	.uleb128 0xb
	.4byte	.LASF58
	.sleb128 21
	.uleb128 0xb
	.4byte	.LASF59
	.sleb128 22
	.uleb128 0xb
	.4byte	.LASF60
	.sleb128 23
	.uleb128 0xb
	.4byte	.LASF61
	.sleb128 24
	.uleb128 0xb
	.4byte	.LASF62
	.sleb128 25
	.uleb128 0xb
	.4byte	.LASF63
	.sleb128 26
	.uleb128 0xb
	.4byte	.LASF64
	.sleb128 27
	.uleb128 0xb
	.4byte	.LASF65
	.sleb128 28
	.uleb128 0xb
	.4byte	.LASF66
	.sleb128 29
	.uleb128 0xb
	.4byte	.LASF67
	.sleb128 30
	.uleb128 0xb
	.4byte	.LASF68
	.sleb128 31
	.uleb128 0xb
	.4byte	.LASF69
	.sleb128 32
	.uleb128 0xb
	.4byte	.LASF70
	.sleb128 33
	.uleb128 0xb
	.4byte	.LASF71
	.sleb128 34
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.byte	0x5
	.byte	0x4d
	.4byte	0x310
	.uleb128 0xb
	.4byte	.LASF72
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF73
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF74
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF75
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF76
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF77
	.sleb128 5
	.uleb128 0xb
	.4byte	.LASF78
	.sleb128 6
	.uleb128 0xb
	.4byte	.LASF79
	.sleb128 7
	.uleb128 0xb
	.4byte	.LASF80
	.sleb128 8
	.uleb128 0xb
	.4byte	.LASF81
	.sleb128 9
	.uleb128 0xb
	.4byte	.LASF82
	.sleb128 10
	.byte	0
	.uleb128 0x5
	.byte	0x8
	.byte	0x1
	.byte	0x21
	.4byte	0x335
	.uleb128 0x6
	.4byte	.LASF83
	.byte	0x1
	.byte	0x23
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF84
	.byte	0x1
	.byte	0x24
	.4byte	0x335
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xc
	.byte	0x4
	.4byte	0x151
	.uleb128 0x3
	.4byte	.LASF85
	.byte	0x1
	.byte	0x25
	.4byte	0x310
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF87
	.byte	0x1
	.byte	0x42
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x38e
	.uleb128 0xe
	.4byte	.LASF86
	.byte	0x1
	.byte	0x42
	.4byte	0x38e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xf
	.ascii	"arg\000"
	.byte	0x1
	.byte	0x43
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x10
	.4byte	.LASF90
	.byte	0x1
	.byte	0x45
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x4
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF88
	.byte	0x1
	.byte	0x80
	.byte	0x1
	.4byte	0x9e
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x3ca
	.uleb128 0xe
	.4byte	.LASF89
	.byte	0x1
	.byte	0x80
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x10
	.4byte	.LASF91
	.byte	0x1
	.byte	0x82
	.4byte	0x9e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF92
	.byte	0x1
	.byte	0xb0
	.byte	0x1
	.4byte	0x9e
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x42e
	.uleb128 0xe
	.4byte	.LASF89
	.byte	0x1
	.byte	0xb0
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xf
	.ascii	"cmd\000"
	.byte	0x1
	.byte	0xb1
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xf
	.ascii	"arg\000"
	.byte	0x1
	.byte	0xb2
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x10
	.4byte	.LASF93
	.byte	0x1
	.byte	0xb4
	.4byte	0x42e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x10
	.4byte	.LASF91
	.byte	0x1
	.byte	0xb5
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xc
	.byte	0x4
	.4byte	0x19d
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x13b
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x4ad
	.uleb128 0x13
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x13b
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x13c
	.4byte	0x38e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x13d
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x14
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x13f
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x14
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x140
	.4byte	0x4ad
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x15
	.ascii	"idx\000"
	.byte	0x1
	.2byte	0x141
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xc
	.byte	0x4
	.4byte	0x3a
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x172
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x4ff
	.uleb128 0x13
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x172
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x13
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x173
	.4byte	0x38e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x174
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x10
	.4byte	.LASF100
	.byte	0x1
	.byte	0x28
	.4byte	0x33b
	.byte	0x5
	.byte	0x3
	.4byte	kscandat
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF55:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF102:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_kscan_driver.c\000"
.LASF30:
	.ascii	"KSCAN_CLEAR_INT\000"
.LASF53:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF20:
	.ascii	"ks_matrix_dim\000"
.LASF57:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF65:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF34:
	.ascii	"KSCAN_IRQ_PENDING\000"
.LASF38:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF74:
	.ascii	"CLKPWR_SYSCLK\000"
.LASF75:
	.ascii	"CLKPWR_ARM_CLK\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF43:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF12:
	.ascii	"long long int\000"
.LASF4:
	.ascii	"signed char\000"
.LASF36:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF99:
	.ascii	"n_bytes\000"
.LASF70:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF1:
	.ascii	"long int\000"
.LASF87:
	.ascii	"kscan_open\000"
.LASF62:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF69:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF49:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF78:
	.ascii	"CLKPWR_USB_HCLK_SYS\000"
.LASF95:
	.ascii	"buffer\000"
.LASF90:
	.ascii	"tptr\000"
.LASF84:
	.ascii	"regptr\000"
.LASF35:
	.ascii	"KSCAN_GET_STATE\000"
.LASF86:
	.ascii	"ipbase\000"
.LASF21:
	.ascii	"reserved\000"
.LASF41:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF33:
	.ascii	"KSCAN_GET_CLOCK\000"
.LASF85:
	.ascii	"KSCAN_CFG_T\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF46:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF64:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF10:
	.ascii	"INT_32\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF101:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF27:
	.ascii	"matrix_size\000"
.LASF92:
	.ascii	"kscan_ioctl\000"
.LASF82:
	.ascii	"CLKPWR_BASE_INVALID\000"
.LASF71:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF73:
	.ascii	"CLKPWR_RTC_CLK\000"
.LASF58:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF51:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF37:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF66:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF19:
	.ascii	"ks_fast_tst\000"
.LASF18:
	.ascii	"ks_scan_ctl\000"
.LASF47:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF17:
	.ascii	"ks_irq\000"
.LASF16:
	.ascii	"ks_state_cond\000"
.LASF31:
	.ascii	"KSCAN_SCAN_ONCE\000"
.LASF93:
	.ascii	"pksctp\000"
.LASF39:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF67:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF77:
	.ascii	"CLKPWR_PERIPH_CLK\000"
.LASF22:
	.ascii	"ks_data\000"
.LASF79:
	.ascii	"CLKPWR_48M_CLK\000"
.LASF59:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF83:
	.ascii	"init\000"
.LASF94:
	.ascii	"kscan_read\000"
.LASF81:
	.ascii	"CLKPWR_MSSD_CLK\000"
.LASF15:
	.ascii	"ks_deb\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF88:
	.ascii	"kscan_close\000"
.LASF6:
	.ascii	"short int\000"
.LASF42:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF56:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF63:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF28:
	.ascii	"KSCAN_SETUP_T\000"
.LASF2:
	.ascii	"char\000"
.LASF98:
	.ascii	"kscan_write\000"
.LASF72:
	.ascii	"CLKPWR_MAINOSC_CLK\000"
.LASF76:
	.ascii	"CLKPWR_HCLK\000"
.LASF44:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF96:
	.ascii	"max_bytes\000"
.LASF52:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF29:
	.ascii	"KSCAN_SETUP\000"
.LASF61:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF26:
	.ascii	"pclk_sel\000"
.LASF100:
	.ascii	"kscandat\000"
.LASF48:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF24:
	.ascii	"deb_clks\000"
.LASF97:
	.ascii	"buff8\000"
.LASF89:
	.ascii	"devid\000"
.LASF14:
	.ascii	"STATUS\000"
.LASF91:
	.ascii	"status\000"
.LASF40:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF68:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF7:
	.ascii	"UNS_8\000"
.LASF80:
	.ascii	"CLKPWR_DDR_CLK\000"
.LASF23:
	.ascii	"KSCAN_REGS_T\000"
.LASF50:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF32:
	.ascii	"KSCAN_GET_STATUS\000"
.LASF60:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF25:
	.ascii	"scan_delay_clks\000"
.LASF45:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF54:
	.ascii	"CLKPWR_WDOG_CLK\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
