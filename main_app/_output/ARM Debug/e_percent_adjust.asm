	.file	"e_percent_adjust.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.text.PERCENT_ADJUST_process_percentage,"ax",%progbits
	.align	2
	.type	PERCENT_ADJUST_process_percentage, %function
PERCENT_ADJUST_process_percentage:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_percent_adjust.c"
	.loc 1 70 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #28
.LCFI2:
	str	r0, [fp, #-16]
	str	r1, [fp, #-12]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 73 0
	ldr	r3, [fp, #-12]
	cmp	r3, #72
	bls	.L2
	.loc 1 75 0
	mov	r3, #4
	str	r3, [fp, #-8]
	b	.L3
.L2:
	.loc 1 77 0
	ldr	r3, [fp, #-12]
	cmp	r3, #48
	bls	.L4
	.loc 1 79 0
	mov	r3, #2
	str	r3, [fp, #-8]
	b	.L3
.L4:
	.loc 1 83 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L3:
	.loc 1 86 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, [fp, #-20]
	mvn	r2, #79
	mov	r3, #100
	bl	process_int32
	.loc 1 88 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L5
	.loc 1 90 0
	ldr	r3, [fp, #-24]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 92 0
	mov	r0, #0
	bl	Redraw_Screen
	b	.L1
.L5:
	.loc 1 94 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L7
	.loc 1 96 0
	bl	Refresh_Screen
	.loc 1 98 0
	ldr	r3, [fp, #-24]
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L1
.L7:
	.loc 1 104 0
	bl	Refresh_Screen
.L1:
	.loc 1 106 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE0:
	.size	PERCENT_ADJUST_process_percentage, .-PERCENT_ADJUST_process_percentage
	.section	.text.PERCENT_ADJUST_process_days,"ax",%progbits
	.align	2
	.type	PERCENT_ADJUST_process_days, %function
PERCENT_ADJUST_process_days:
.LFB1:
	.loc 1 110 0
	@ args = 0, pretend = 0, frame = 76
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #84
.LCFI5:
	str	r0, [fp, #-72]
	str	r1, [fp, #-68]
	str	r2, [fp, #-76]
	str	r3, [fp, #-80]
	.loc 1 113 0
	ldr	r3, [fp, #-68]
	cmp	r3, #72
	bls	.L9
	.loc 1 115 0
	mov	r3, #4
	str	r3, [fp, #-8]
	b	.L10
.L9:
	.loc 1 117 0
	ldr	r3, [fp, #-68]
	cmp	r3, #48
	bls	.L11
	.loc 1 119 0
	mov	r3, #2
	str	r3, [fp, #-8]
	b	.L10
.L11:
	.loc 1 123 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L10:
	.loc 1 126 0
	ldr	r3, [fp, #-72]
	ldr	r2, [fp, #-8]
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, [fp, #-76]
	mov	r2, #0
	ldr	r3, .L12
	bl	process_uns32
	.loc 1 134 0
	sub	r3, fp, #64
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 136 0
	ldrh	r3, [fp, #-60]
	mov	r2, r3
	ldr	r3, [fp, #-76]
	ldr	r3, [r3, #0]
	add	r3, r2, r3
	sub	r2, fp, #56
	mov	r1, #225
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #125
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, [fp, #-80]
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 140 0
	bl	Refresh_Screen
	.loc 1 141 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	366
.LFE1:
	.size	PERCENT_ADJUST_process_days, .-PERCENT_ADJUST_process_days
	.section	.text.FDTO_PERCENT_ADJUST_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_PERCENT_ADJUST_draw_screen
	.type	FDTO_PERCENT_ADJUST_draw_screen, %function
FDTO_PERCENT_ADJUST_draw_screen:
.LFB2:
	.loc 1 145 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-12]
	.loc 1 148 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L15
	.loc 1 150 0
	bl	PERCENT_ADJUST_copy_group_into_guivars
	.loc 1 152 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L16
.L15:
	.loc 1 156 0
	ldr	r3, .L17
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L16:
	.loc 1 159 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #48
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 160 0
	bl	GuiLib_Refresh
	.loc 1 161 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L18:
	.align	2
.L17:
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_PERCENT_ADJUST_draw_screen, .-FDTO_PERCENT_ADJUST_draw_screen
	.section	.text.PERCENT_ADJUST_process_screen,"ax",%progbits
	.align	2
	.global	PERCENT_ADJUST_process_screen
	.type	PERCENT_ADJUST_process_screen, %function
PERCENT_ADJUST_process_screen:
.LFB3:
	.loc 1 165 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI9:
	add	fp, sp, #8
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-16]
	str	r1, [fp, #-12]
	.loc 1 167 0
	ldr	r3, [fp, #-16]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L20
.L27:
	.word	.L21
	.word	.L22
	.word	.L20
	.word	.L23
	.word	.L24
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L24
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L21
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L25
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L26
	.word	.L20
	.word	.L20
	.word	.L20
	.word	.L26
.L26:
	.loc 1 171 0
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #19
	ldrls	pc, [pc, r3, asl #2]
	b	.L28
.L49:
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
.L29:
	.loc 1 174 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+4
	ldr	r3, .L62+8
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 175 0
	b	.L50
.L30:
	.loc 1 178 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+12
	ldr	r3, .L62+16
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 179 0
	b	.L50
.L31:
	.loc 1 182 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+20
	ldr	r3, .L62+24
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 183 0
	b	.L50
.L32:
	.loc 1 186 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+28
	ldr	r3, .L62+32
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 187 0
	b	.L50
.L33:
	.loc 1 190 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+36
	ldr	r3, .L62+40
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 191 0
	b	.L50
.L34:
	.loc 1 194 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+44
	ldr	r3, .L62+48
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 195 0
	b	.L50
.L35:
	.loc 1 198 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+52
	ldr	r3, .L62+56
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 199 0
	b	.L50
.L36:
	.loc 1 202 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+60
	ldr	r3, .L62+64
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 203 0
	b	.L50
.L37:
	.loc 1 206 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+68
	ldr	r3, .L62+72
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 207 0
	b	.L50
.L38:
	.loc 1 210 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+76
	ldr	r3, .L62+80
	bl	PERCENT_ADJUST_process_percentage
	.loc 1 211 0
	b	.L50
.L39:
	.loc 1 214 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+84
	ldr	r3, .L62+88
	bl	PERCENT_ADJUST_process_days
	.loc 1 215 0
	b	.L50
.L40:
	.loc 1 218 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+92
	ldr	r3, .L62+96
	bl	PERCENT_ADJUST_process_days
	.loc 1 219 0
	b	.L50
.L41:
	.loc 1 222 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+100
	ldr	r3, .L62+104
	bl	PERCENT_ADJUST_process_days
	.loc 1 223 0
	b	.L50
.L42:
	.loc 1 226 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+108
	ldr	r3, .L62+112
	bl	PERCENT_ADJUST_process_days
	.loc 1 227 0
	b	.L50
.L43:
	.loc 1 230 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+116
	ldr	r3, .L62+120
	bl	PERCENT_ADJUST_process_days
	.loc 1 231 0
	b	.L50
.L44:
	.loc 1 234 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+124
	ldr	r3, .L62+128
	bl	PERCENT_ADJUST_process_days
	.loc 1 235 0
	b	.L50
.L45:
	.loc 1 238 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+132
	ldr	r3, .L62+136
	bl	PERCENT_ADJUST_process_days
	.loc 1 239 0
	b	.L50
.L46:
	.loc 1 242 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+140
	ldr	r3, .L62+144
	bl	PERCENT_ADJUST_process_days
	.loc 1 243 0
	b	.L50
.L47:
	.loc 1 246 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+148
	ldr	r3, .L62+152
	bl	PERCENT_ADJUST_process_days
	.loc 1 247 0
	b	.L50
.L48:
	.loc 1 250 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L62+156
	ldr	r3, .L62+160
	bl	PERCENT_ADJUST_process_days
	.loc 1 251 0
	b	.L50
.L28:
	.loc 1 254 0
	bl	bad_key_beep
	.loc 1 256 0
	b	.L19
.L50:
	b	.L19
.L24:
	.loc 1 260 0
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L62+164
	smull	r1, r3, r2, r3
	mov	r1, r3, asr #2
	mov	r3, r2, asr #31
	rsb	r1, r3, r1
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #1
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #0
	bne	.L52
	.loc 1 262 0
	bl	bad_key_beep
	.loc 1 268 0
	b	.L19
.L52:
	.loc 1 266 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 268 0
	b	.L19
.L21:
	.loc 1 272 0
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L62+164
	smull	r1, r3, r2, r3
	mov	r1, r3, asr #2
	mov	r3, r2, asr #31
	rsb	r1, r3, r1
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #1
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r4, r3, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r3, r0
	sub	r3, r3, #1
	cmp	r4, r3
	bne	.L54
	.loc 1 274 0
	bl	bad_key_beep
	.loc 1 280 0
	b	.L19
.L54:
	.loc 1 278 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 280 0
	b	.L19
.L22:
	.loc 1 283 0
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L56
	.loc 1 283 0 is_stmt 0 discriminator 1
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	bgt	.L56
	.loc 1 285 0 is_stmt 1
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #9
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L57
.L56:
	.loc 1 287 0
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ble	.L58
	.loc 1 287 0 is_stmt 0 discriminator 1
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #19
	bgt	.L58
	.loc 1 289 0 is_stmt 1
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #10
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L57
.L58:
	.loc 1 293 0
	bl	bad_key_beep
	.loc 1 295 0
	b	.L19
.L57:
	b	.L19
.L23:
	.loc 1 298 0
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L59
	.loc 1 298 0 is_stmt 0 discriminator 1
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	bgt	.L59
	.loc 1 300 0 is_stmt 1
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #10
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L60
.L59:
	.loc 1 302 0
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ble	.L61
	.loc 1 302 0 is_stmt 0 discriminator 1
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #19
	bgt	.L61
	.loc 1 304 0 is_stmt 1
	ldr	r3, .L62
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #9
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L60
.L61:
	.loc 1 308 0
	bl	bad_key_beep
	.loc 1 310 0
	b	.L19
.L60:
	b	.L19
.L25:
	.loc 1 313 0
	ldr	r3, .L62+168
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 315 0
	bl	PERCENT_ADJUST_extract_and_store_changes_from_GuiVars
.L20:
	.loc 1 320 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L19:
	.loc 1 322 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L63:
	.align	2
.L62:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_PercentAdjustPercent_0
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_PercentAdjustPercent_1
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_PercentAdjustPercent_2
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_PercentAdjustPercent_3
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_PercentAdjustPercent_4
	.word	GuiVar_GroupSettingC_4
	.word	GuiVar_PercentAdjustPercent_5
	.word	GuiVar_GroupSettingC_5
	.word	GuiVar_PercentAdjustPercent_6
	.word	GuiVar_GroupSettingC_6
	.word	GuiVar_PercentAdjustPercent_7
	.word	GuiVar_GroupSettingC_7
	.word	GuiVar_PercentAdjustPercent_8
	.word	GuiVar_GroupSettingC_8
	.word	GuiVar_PercentAdjustPercent_9
	.word	GuiVar_GroupSettingC_9
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_PercentAdjustEndDate_0
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_PercentAdjustEndDate_1
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_PercentAdjustEndDate_2
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_PercentAdjustEndDate_3
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_PercentAdjustEndDate_4
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_PercentAdjustEndDate_5
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_PercentAdjustEndDate_6
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_PercentAdjustEndDate_7
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_PercentAdjustEndDate_8
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_PercentAdjustEndDate_9
	.word	1717986919
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	PERCENT_ADJUST_process_screen, .-PERCENT_ADJUST_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x76b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF79
	.byte	0x1
	.4byte	.LASF80
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x67
	.4byte	0x82
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x5
	.4byte	0x33
	.4byte	0xb9
	.uleb128 0x6
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x2c
	.uleb128 0x8
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xe4
	.uleb128 0x9
	.4byte	.LASF15
	.byte	0x3
	.byte	0x7e
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x3
	.byte	0x80
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x3
	.byte	0x82
	.4byte	0xbf
	.uleb128 0x8
	.byte	0x6
	.byte	0x4
	.byte	0x22
	.4byte	0x110
	.uleb128 0xa
	.ascii	"T\000"
	.byte	0x4
	.byte	0x24
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.ascii	"D\000"
	.byte	0x4
	.byte	0x26
	.4byte	0x41
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x4
	.byte	0x28
	.4byte	0xef
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0x12b
	.uleb128 0x6
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF19
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x1
	.byte	0x45
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x183
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x1
	.byte	0x45
	.4byte	0x183
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x1
	.byte	0x45
	.4byte	0x188
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x1
	.byte	0x45
	.4byte	0x18e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xd
	.4byte	.LASF27
	.byte	0x1
	.byte	0x47
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0xe4
	.uleb128 0x7
	.byte	0x4
	.4byte	0x77
	.uleb128 0x7
	.byte	0x4
	.4byte	0x97
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x1
	.byte	0x6d
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x205
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x1
	.byte	0x6d
	.4byte	0x183
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x1
	.byte	0x6d
	.4byte	0x205
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x1
	.byte	0x6d
	.4byte	0xb9
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0xd
	.4byte	.LASF27
	.byte	0x1
	.byte	0x6f
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xd
	.4byte	.LASF28
	.byte	0x1
	.byte	0x82
	.4byte	0x11b
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0xf
	.ascii	"ldt\000"
	.byte	0x1
	.byte	0x84
	.4byte	0x110
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x65
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.byte	0x90
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x241
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x1
	.byte	0x90
	.4byte	0x241
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xd
	.4byte	.LASF30
	.byte	0x1
	.byte	0x92
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x97
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.byte	0xa4
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x26e
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x1
	.byte	0xa4
	.4byte	0x183
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x11
	.4byte	.LASF33
	.byte	0x5
	.2byte	0x21b
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF34
	.byte	0x5
	.2byte	0x21c
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF35
	.byte	0x5
	.2byte	0x21d
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x21e
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x21f
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x220
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x221
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x222
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x223
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x224
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x225
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x226
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x227
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x228
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x229
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x22a
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x22b
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF50
	.byte	0x5
	.2byte	0x22c
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF51
	.byte	0x5
	.2byte	0x22d
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0x5
	.2byte	0x22e
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF53
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0x3a4
	.uleb128 0x6
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x11
	.4byte	.LASF54
	.byte	0x5
	.2byte	0x32a
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF55
	.byte	0x5
	.2byte	0x32b
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF56
	.byte	0x5
	.2byte	0x32c
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF57
	.byte	0x5
	.2byte	0x32d
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF58
	.byte	0x5
	.2byte	0x32e
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF59
	.byte	0x5
	.2byte	0x32f
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF60
	.byte	0x5
	.2byte	0x330
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF61
	.byte	0x5
	.2byte	0x331
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF62
	.byte	0x5
	.2byte	0x332
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF63
	.byte	0x5
	.2byte	0x333
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF64
	.byte	0x5
	.2byte	0x336
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF65
	.byte	0x5
	.2byte	0x337
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF66
	.byte	0x5
	.2byte	0x338
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF67
	.byte	0x5
	.2byte	0x339
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF68
	.byte	0x5
	.2byte	0x33a
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF69
	.byte	0x5
	.2byte	0x33b
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF70
	.byte	0x5
	.2byte	0x33c
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF71
	.byte	0x5
	.2byte	0x33d
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF72
	.byte	0x5
	.2byte	0x33e
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF73
	.byte	0x5
	.2byte	0x33f
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF74
	.byte	0x6
	.2byte	0x127
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0xd
	.4byte	.LASF75
	.byte	0x7
	.byte	0x30
	.4byte	0x4db
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xa9
	.uleb128 0xd
	.4byte	.LASF76
	.byte	0x7
	.byte	0x34
	.4byte	0x4f1
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xa9
	.uleb128 0xd
	.4byte	.LASF77
	.byte	0x7
	.byte	0x36
	.4byte	0x507
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xa9
	.uleb128 0xd
	.4byte	.LASF78
	.byte	0x7
	.byte	0x38
	.4byte	0x51d
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xa9
	.uleb128 0x11
	.4byte	.LASF33
	.byte	0x5
	.2byte	0x21b
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF34
	.byte	0x5
	.2byte	0x21c
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF35
	.byte	0x5
	.2byte	0x21d
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x21e
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x21f
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x220
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x221
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x222
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x223
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x224
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x225
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x226
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x227
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x228
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x229
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x22a
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x22b
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF50
	.byte	0x5
	.2byte	0x22c
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF51
	.byte	0x5
	.2byte	0x22d
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0x5
	.2byte	0x22e
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF53
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF54
	.byte	0x5
	.2byte	0x32a
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF55
	.byte	0x5
	.2byte	0x32b
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF56
	.byte	0x5
	.2byte	0x32c
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF57
	.byte	0x5
	.2byte	0x32d
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF58
	.byte	0x5
	.2byte	0x32e
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF59
	.byte	0x5
	.2byte	0x32f
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF60
	.byte	0x5
	.2byte	0x330
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF61
	.byte	0x5
	.2byte	0x331
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF62
	.byte	0x5
	.2byte	0x332
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF63
	.byte	0x5
	.2byte	0x333
	.4byte	0x394
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF64
	.byte	0x5
	.2byte	0x336
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF65
	.byte	0x5
	.2byte	0x337
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF66
	.byte	0x5
	.2byte	0x338
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF67
	.byte	0x5
	.2byte	0x339
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF68
	.byte	0x5
	.2byte	0x33a
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF69
	.byte	0x5
	.2byte	0x33b
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF70
	.byte	0x5
	.2byte	0x33c
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF71
	.byte	0x5
	.2byte	0x33d
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF72
	.byte	0x5
	.2byte	0x33e
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF73
	.byte	0x5
	.2byte	0x33f
	.4byte	0x82
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF74
	.byte	0x6
	.2byte	0x127
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF79:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF19:
	.ascii	"float\000"
.LASF7:
	.ascii	"short int\000"
.LASF15:
	.ascii	"keycode\000"
.LASF33:
	.ascii	"GuiVar_GroupSettingB_0\000"
.LASF23:
	.ascii	"PERCENT_ADJUST_process_percentage\000"
.LASF24:
	.ascii	"PERCENT_ADJUST_process_days\000"
.LASF53:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF64:
	.ascii	"GuiVar_PercentAdjustPercent_0\000"
.LASF65:
	.ascii	"GuiVar_PercentAdjustPercent_1\000"
.LASF66:
	.ascii	"GuiVar_PercentAdjustPercent_2\000"
.LASF67:
	.ascii	"GuiVar_PercentAdjustPercent_3\000"
.LASF68:
	.ascii	"GuiVar_PercentAdjustPercent_4\000"
.LASF69:
	.ascii	"GuiVar_PercentAdjustPercent_5\000"
.LASF70:
	.ascii	"GuiVar_PercentAdjustPercent_6\000"
.LASF71:
	.ascii	"GuiVar_PercentAdjustPercent_7\000"
.LASF72:
	.ascii	"GuiVar_PercentAdjustPercent_8\000"
.LASF17:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF10:
	.ascii	"INT_32\000"
.LASF3:
	.ascii	"signed char\000"
.LASF12:
	.ascii	"long long int\000"
.LASF77:
	.ascii	"GuiFont_DecimalChar\000"
.LASF32:
	.ascii	"PERCENT_ADJUST_process_screen\000"
.LASF14:
	.ascii	"long int\000"
.LASF80:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_percent_adjust.c\000"
.LASF43:
	.ascii	"GuiVar_GroupSettingC_0\000"
.LASF44:
	.ascii	"GuiVar_GroupSettingC_1\000"
.LASF45:
	.ascii	"GuiVar_GroupSettingC_2\000"
.LASF46:
	.ascii	"GuiVar_GroupSettingC_3\000"
.LASF47:
	.ascii	"GuiVar_GroupSettingC_4\000"
.LASF48:
	.ascii	"GuiVar_GroupSettingC_5\000"
.LASF49:
	.ascii	"GuiVar_GroupSettingC_6\000"
.LASF50:
	.ascii	"GuiVar_GroupSettingC_7\000"
.LASF51:
	.ascii	"GuiVar_GroupSettingC_8\000"
.LASF52:
	.ascii	"GuiVar_GroupSettingC_9\000"
.LASF21:
	.ascii	"ppercentage\000"
.LASF18:
	.ascii	"DATE_TIME\000"
.LASF29:
	.ascii	"pcomplete_redraw\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF31:
	.ascii	"FDTO_PERCENT_ADJUST_draw_screen\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF35:
	.ascii	"GuiVar_GroupSettingB_2\000"
.LASF36:
	.ascii	"GuiVar_GroupSettingB_3\000"
.LASF37:
	.ascii	"GuiVar_GroupSettingB_4\000"
.LASF38:
	.ascii	"GuiVar_GroupSettingB_5\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF40:
	.ascii	"GuiVar_GroupSettingB_7\000"
.LASF76:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF42:
	.ascii	"GuiVar_GroupSettingB_9\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF20:
	.ascii	"pkey_event\000"
.LASF41:
	.ascii	"GuiVar_GroupSettingB_8\000"
.LASF74:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF1:
	.ascii	"char\000"
.LASF78:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF22:
	.ascii	"pshow_days\000"
.LASF25:
	.ascii	"pdays\000"
.LASF26:
	.ascii	"pend_date_str\000"
.LASF34:
	.ascii	"GuiVar_GroupSettingB_1\000"
.LASF16:
	.ascii	"repeats\000"
.LASF75:
	.ascii	"GuiFont_LanguageActive\000"
.LASF30:
	.ascii	"lcursor_to_select\000"
.LASF28:
	.ascii	"str_48\000"
.LASF39:
	.ascii	"GuiVar_GroupSettingB_6\000"
.LASF73:
	.ascii	"GuiVar_PercentAdjustPercent_9\000"
.LASF54:
	.ascii	"GuiVar_PercentAdjustEndDate_0\000"
.LASF55:
	.ascii	"GuiVar_PercentAdjustEndDate_1\000"
.LASF56:
	.ascii	"GuiVar_PercentAdjustEndDate_2\000"
.LASF57:
	.ascii	"GuiVar_PercentAdjustEndDate_3\000"
.LASF58:
	.ascii	"GuiVar_PercentAdjustEndDate_4\000"
.LASF59:
	.ascii	"GuiVar_PercentAdjustEndDate_5\000"
.LASF60:
	.ascii	"GuiVar_PercentAdjustEndDate_6\000"
.LASF61:
	.ascii	"GuiVar_PercentAdjustEndDate_7\000"
.LASF62:
	.ascii	"GuiVar_PercentAdjustEndDate_8\000"
.LASF63:
	.ascii	"GuiVar_PercentAdjustEndDate_9\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF27:
	.ascii	"inc_by\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
