	.file	"lpc_arm922t_cp15_driver.c"
	.text
.Ltext0:
	.global	virtual_tlb_addr
	.section	.bss.virtual_tlb_addr,"aw",%nobits
	.align	2
	.type	virtual_tlb_addr, %object
	.size	virtual_tlb_addr, 4
virtual_tlb_addr:
	.space	4
	.section	.text.cp15_decode_level2,"ax",%progbits
	.align	2
	.type	cp15_decode_level2, %function
cp15_decode_level2:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c"
	.loc 1 150 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #12
.LCFI2:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 151 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 153 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #3
	cmp	r3, #2
	beq	.L4
	cmp	r3, #3
	beq	.L5
	cmp	r3, #1
	bne	.L9
.L3:
	.loc 1 157 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	eor	r3, r2, r3
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	cmp	r3, #0
	bne	.L10
	.loc 1 160 0
	ldr	r3, [fp, #-8]
	mov	r2, r3, lsr #16
	mov	r2, r2, asl #16
	.loc 1 161 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 160 0
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 163 0
	b	.L10
.L4:
	.loc 1 167 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	eor	r3, r2, r3
	bic	r3, r3, #4080
	bic	r3, r3, #15
	cmp	r3, #0
	bne	.L11
	.loc 1 170 0
	ldr	r3, [fp, #-8]
	bic	r2, r3, #4080
	bic	r2, r2, #15
	.loc 1 171 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #20
	mov	r3, r3, lsr #20
	.loc 1 170 0
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 173 0
	b	.L11
.L5:
	.loc 1 177 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	eor	r3, r2, r3
	bic	r3, r3, #1020
	bic	r3, r3, #3
	cmp	r3, #0
	bne	.L12
	.loc 1 180 0
	ldr	r3, [fp, #-8]
	bic	r2, r3, #1020
	bic	r2, r2, #3
	.loc 1 181 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	.loc 1 180 0
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 185 0
	b	.L12
.L9:
	b	.L12
.L10:
	.loc 1 163 0
	mov	r0, r0	@ nop
	b	.L7
.L11:
	.loc 1 173 0
	mov	r0, r0	@ nop
	b	.L7
.L12:
	.loc 1 185 0
	mov	r0, r0	@ nop
.L7:
	.loc 1 188 0
	ldr	r3, [fp, #-4]
	.loc 1 189 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE0:
	.size	cp15_decode_level2, .-cp15_decode_level2
	.section	.text.cp15_map_virtual_to_physical,"ax",%progbits
	.align	2
	.global	cp15_map_virtual_to_physical
	.type	cp15_map_virtual_to_physical, %function
cp15_map_virtual_to_physical:
.LFB1:
	.loc 1 246 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, fp}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #28
.LCFI5:
	str	r0, [fp, #-32]
	.loc 1 251 0
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-12]
	.loc 1 255 0
@ 255 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r4, c1, c0, 0
@ 0 "" 2
	.loc 1 272 0
	and	r3, r4, #1
	cmp	r3, #0
	bne	.L14
	.loc 1 275 0
	ldr	r3, [fp, #-12]
	b	.L15
.L14:
	.loc 1 279 0
	ldr	r3, .L28
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	.loc 1 285 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-20]
	.loc 1 286 0
	ldr	r3, [fp, #-20]
	mov	r3, r3, lsr #20
	str	r3, [fp, #-20]
	.loc 1 287 0
	ldr	r3, [fp, #-20]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-24]
	.loc 1 289 0
	ldr	r3, [fp, #-24]
	and	r3, r3, #3
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L16
.L21:
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
.L17:
	.loc 1 293 0
	mov	r3, #0
	b	.L15
.L18:
	.loc 1 297 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, lsr #12
	and	r3, r3, #255
	str	r3, [fp, #-20]
	.loc 1 298 0
	ldr	r3, [fp, #-24]
	bic	r3, r3, #1020
	bic	r3, r3, #3
	str	r3, [fp, #-28]
	.loc 1 300 0
	ldr	r3, [fp, #-20]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-28]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 301 0
	b	.L22
.L19:
	.loc 1 310 0
	ldr	r3, [fp, #-24]
	mov	r2, r3, lsr #20
	mov	r2, r2, asl #20
	.loc 1 311 0
	ldr	r3, [fp, #-12]
	bic	r3, r3, #-16777216
	bic	r3, r3, #15728640
	.loc 1 310 0
	orr	r3, r2, r3
	b	.L15
.L20:
	.loc 1 315 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, lsr #10
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	str	r3, [fp, #-20]
	.loc 1 316 0
	ldr	r3, [fp, #-24]
	bic	r3, r3, #4080
	bic	r3, r3, #15
	str	r3, [fp, #-28]
	.loc 1 318 0
	ldr	r3, [fp, #-20]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-28]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 319 0
	b	.L22
.L16:
	.loc 1 323 0
	mov	r3, #0
	b	.L15
.L22:
	.loc 1 326 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #3
	cmp	r3, #2
	beq	.L25
	cmp	r3, #3
	beq	.L26
	cmp	r3, #1
	bne	.L27
.L24:
	.loc 1 330 0
	ldr	r3, [fp, #-8]
	mov	r2, r3, lsr #16
	mov	r2, r2, asl #16
	.loc 1 331 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 330 0
	orr	r3, r2, r3
	b	.L15
.L25:
	.loc 1 335 0
	ldr	r3, [fp, #-8]
	bic	r2, r3, #4080
	bic	r2, r2, #15
	.loc 1 336 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #20
	mov	r3, r3, lsr #20
	.loc 1 335 0
	orr	r3, r2, r3
	b	.L15
.L26:
	.loc 1 340 0
	ldr	r3, [fp, #-8]
	bic	r2, r3, #1020
	bic	r2, r2, #3
	.loc 1 341 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	.loc 1 340 0
	orr	r3, r2, r3
	b	.L15
.L27:
	.loc 1 344 0
	mov	r0, r0	@ nop
	.loc 1 347 0
	mov	r3, #0
.L15:
	.loc 1 348 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {r4, fp}
	bx	lr
.L29:
	.align	2
.L28:
	.word	virtual_tlb_addr
.LFE1:
	.size	cp15_map_virtual_to_physical, .-cp15_map_virtual_to_physical
	.section	.text.cp15_map_physical_to_virtual,"ax",%progbits
	.align	2
	.global	cp15_map_physical_to_virtual
	.type	cp15_map_physical_to_virtual, %function
cp15_map_physical_to_virtual:
.LFB2:
	.loc 1 373 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI6:
	add	fp, sp, #8
.LCFI7:
	sub	sp, sp, #32
.LCFI8:
	str	r0, [fp, #-40]
	.loc 1 386 0
@ 386 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r4, c1, c0, 0
@ 0 "" 2
	.loc 1 403 0
	and	r3, r4, #1
	cmp	r3, #0
	bne	.L31
	.loc 1 406 0
	ldr	r3, [fp, #-40]
	b	.L32
.L31:
	.loc 1 410 0
	ldr	r3, .L52
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 416 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L33
.L47:
	.loc 1 418 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-20]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-24]
	.loc 1 419 0
	ldr	r3, [fp, #-24]
	and	r3, r3, #3
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L48
.L39:
	.word	.L48
	.word	.L36
	.word	.L37
	.word	.L38
.L36:
	.loc 1 427 0
	ldr	r3, [fp, #-24]
	bic	r3, r3, #1020
	bic	r3, r3, #3
	str	r3, [fp, #-28]
	.loc 1 429 0
	mov	r3, #0
	str	r3, [fp, #-16]
	ldr	r3, [fp, #-16]
	cmp	r3, #255
	bhi	.L49
	.loc 1 431 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-28]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-32]
	.loc 1 432 0
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	beq	.L50
	.loc 1 435 0
	ldr	r0, [fp, #-32]
	ldr	r1, [fp, #-40]
	bl	cp15_decode_level2
	mov	r3, r0
	.loc 1 434 0
	str	r3, [fp, #-36]
	.loc 1 437 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L50
	.loc 1 439 0
	ldr	r3, [fp, #-36]
	b	.L32
.L50:
	.loc 1 442 0
	mov	r0, r0	@ nop
.L41:
	.loc 1 444 0
	b	.L49
.L37:
	.loc 1 454 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-40]
	eor	r3, r2, r3
	mov	r3, r3, lsr #20
	mov	r3, r3, asl #20
	.loc 1 453 0
	cmp	r3, #0
	bne	.L51
	.loc 1 456 0
	ldr	r3, [fp, #-12]
	mov	r2, r3, asl #20
	.loc 1 457 0
	ldr	r3, [fp, #-40]
	bic	r3, r3, #-16777216
	bic	r3, r3, #15728640
	.loc 1 456 0
	orr	r3, r2, r3
	b	.L32
.L38:
	.loc 1 463 0
	ldr	r3, [fp, #-24]
	bic	r3, r3, #4080
	bic	r3, r3, #15
	str	r3, [fp, #-28]
	.loc 1 465 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L44
.L46:
	.loc 1 467 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-28]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-32]
	.loc 1 468 0
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	beq	.L45
	.loc 1 471 0
	ldr	r0, [fp, #-32]
	ldr	r1, [fp, #-40]
	bl	cp15_decode_level2
	mov	r3, r0
	.loc 1 470 0
	str	r3, [fp, #-36]
	.loc 1 473 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L45
	.loc 1 475 0
	ldr	r3, [fp, #-36]
	b	.L32
.L45:
	.loc 1 465 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L44:
	.loc 1 465 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-16]
	ldr	r3, .L52+4
	cmp	r2, r3
	bls	.L46
	.loc 1 479 0 is_stmt 1
	b	.L40
.L48:
	.loc 1 482 0
	mov	r0, r0	@ nop
	b	.L40
.L49:
	.loc 1 444 0
	mov	r0, r0	@ nop
	b	.L40
.L51:
	.loc 1 459 0
	mov	r0, r0	@ nop
.L40:
	.loc 1 416 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L33:
	.loc 1 416 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r3, .L52+8
	cmp	r2, r3
	bls	.L47
	.loc 1 486 0 is_stmt 1
	mov	r3, #0
.L32:
	.loc 1 487 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L53:
	.align	2
.L52:
	.word	virtual_tlb_addr
	.word	1023
	.word	4095
.LFE2:
	.size	cp15_map_physical_to_virtual, .-cp15_map_physical_to_virtual
	.section	.text.cp15_force_cache_coherence,"ax",%progbits
	.align	2
	.global	cp15_force_cache_coherence
	.type	cp15_force_cache_coherence, %function
cp15_force_cache_coherence:
.LFB3:
	.loc 1 519 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, fp}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 527 0
	ldr	r3, [fp, #-8]
	bic	r3, r3, #31
	mov	r4, r3
	b	.L55
.L56:
	.loc 1 533 0
@ 533 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MOV r0, r4
@ 0 "" 2
	.loc 1 535 0
@ 535 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r0, c7, c14, 1
@ 0 "" 2
	.loc 1 537 0
@ 537 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r0, c7, c5, 1
@ 0 "" 2
	.loc 1 529 0
	add	r4, r4, #32
.L55:
	.loc 1 527 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r4, r3
	bcc	.L56
	.loc 1 566 0
	ldr	r3, [fp, #-8]
	bic	r3, r3, #1020
	bic	r3, r3, #3
	mov	r4, r3
	b	.L57
.L58:
	.loc 1 571 0
@ 571 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MOV r0, r4
@ 0 "" 2
	.loc 1 573 0
@ 573 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r0, c8, c5, 1
@ 0 "" 2
	.loc 1 574 0
@ 574 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	NOP
@ 0 "" 2
	.loc 1 575 0
@ 575 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	NOP
@ 0 "" 2
	.loc 1 568 0
	add	r4, r4, #1024
.L57:
	.loc 1 566 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r4, r3
	bcc	.L58
	.loc 1 600 0
	sub	sp, fp, #4
	ldmfd	sp!, {r4, fp}
	bx	lr
.LFE3:
	.size	cp15_force_cache_coherence, .-cp15_force_cache_coherence
	.section	.text.cp15_init_mmu_trans_table,"ax",%progbits
	.align	2
	.global	cp15_init_mmu_trans_table
	.type	cp15_init_mmu_trans_table, %function
cp15_init_mmu_trans_table:
.LFB4:
	.loc 1 632 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, fp}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #28
.LCFI14:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	.loc 1 638 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 649 0
@ 649 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r4, c1, c0, 0
@ 0 "" 2
	.loc 1 666 0
	mov	r3, r4
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L60
	.loc 1 668 0
	mvn	r3, #0
	b	.L61
.L60:
	.loc 1 672 0
	ldr	r3, [fp, #-28]
	mov	r3, r3, asl #18
	mov	r3, r3, lsr #18
	cmp	r3, #0
	beq	.L62
	.loc 1 674 0
	mvn	r3, #0
	b	.L61
.L62:
	.loc 1 682 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-20]
	.loc 1 683 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L63
.L64:
	.loc 1 684 0 discriminator 2
	ldr	r3, [fp, #-20]
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	add	r3, r3, #4
	str	r3, [fp, #-20]
	.loc 1 683 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L63:
	.loc 1 683 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, .L76
	cmp	r2, r3
	bls	.L64
	.loc 1 688 0 is_stmt 1
	b	.L65
.L74:
	.loc 1 690 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #12]
	and	r3, r3, #3
	cmp	r3, #1
	beq	.L67
	cmp	r3, #2
	bne	.L75
.L68:
	.loc 1 693 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #4]
	mov	r3, r3, lsr #20
	str	r3, [fp, #-12]
	.loc 1 694 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #8]
	mov	r3, r3, lsr #20
	mov	r3, r3, asl #20
	str	r3, [fp, #-16]
	.loc 1 695 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L69
.L70:
	.loc 1 697 0 discriminator 2
	ldr	r3, [fp, #-32]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-16]
	orr	r3, r2, r3
	orr	r1, r3, #16
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-12]
	str	r1, [r3, r2, asl #2]
	.loc 1 698 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 699 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1048576
	str	r3, [fp, #-16]
	.loc 1 695 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L69:
	.loc 1 695 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-32]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bhi	.L70
	.loc 1 701 0 is_stmt 1
	b	.L71
.L67:
	.loc 1 704 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #4]
	mov	r3, r3, lsr #20
	str	r3, [fp, #-12]
	.loc 1 705 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #8]
	bic	r3, r3, #1020
	bic	r3, r3, #3
	str	r3, [fp, #-16]
	.loc 1 706 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L72
.L73:
	.loc 1 708 0 discriminator 2
	ldr	r3, [fp, #-32]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-16]
	orr	r1, r2, r3
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-12]
	str	r1, [r3, r2, asl #2]
	.loc 1 709 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 710 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1048576
	str	r3, [fp, #-16]
	.loc 1 706 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L72:
	.loc 1 706 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-32]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bhi	.L73
	.loc 1 712 0 is_stmt 1
	b	.L71
.L75:
	.loc 1 716 0
	mov	r0, r0	@ nop
.L71:
	.loc 1 719 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #16
	str	r3, [fp, #-32]
.L65:
	.loc 1 688 0 discriminator 1
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L74
	.loc 1 722 0
	ldr	r3, [fp, #-24]
.L61:
	.loc 1 723 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {r4, fp}
	bx	lr
.L77:
	.align	2
.L76:
	.word	4095
.LFE4:
	.size	cp15_init_mmu_trans_table, .-cp15_init_mmu_trans_table
	.section	.text.cp15_set_vmmu_addr,"ax",%progbits
	.align	2
	.global	cp15_set_vmmu_addr
	.type	cp15_set_vmmu_addr, %function
cp15_set_vmmu_addr:
.LFB5:
	.loc 1 748 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI15:
	add	fp, sp, #0
.LCFI16:
	sub	sp, sp, #4
.LCFI17:
	str	r0, [fp, #-4]
	.loc 1 749 0
	ldr	r3, .L79
	ldr	r2, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 750 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L80:
	.align	2
.L79:
	.word	virtual_tlb_addr
.LFE5:
	.size	cp15_set_vmmu_addr, .-cp15_set_vmmu_addr
	.section	.text.cp15_get_ttb,"ax",%progbits
	.align	2
	.global	cp15_get_ttb
	.type	cp15_get_ttb, %function
cp15_get_ttb:
.LFB6:
	.loc 1 772 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, fp}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	.loc 1 776 0
@ 776 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r4, c2, c0, 0
@ 0 "" 2
	.loc 1 792 0
	mov	r3, r4
	.loc 1 793 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {r4, fp}
	bx	lr
.LFE6:
	.size	cp15_get_ttb, .-cp15_get_ttb
	.section	.text.cp15_dcache_flush,"ax",%progbits
	.align	2
	.global	cp15_dcache_flush
	.type	cp15_dcache_flush, %function
cp15_dcache_flush:
.LFB7:
	.loc 1 814 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI20:
	add	fp, sp, #0
.LCFI21:
	.loc 1 849 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE7:
	.size	cp15_dcache_flush, .-cp15_dcache_flush
	.section	.text.cp15_write_buffer_flush,"ax",%progbits
	.align	2
	.global	cp15_write_buffer_flush
	.type	cp15_write_buffer_flush, %function
cp15_write_buffer_flush:
.LFB8:
	.loc 1 870 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI22:
	add	fp, sp, #0
.LCFI23:
	.loc 1 888 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE8:
	.size	cp15_write_buffer_flush, .-cp15_write_buffer_flush
	.section	.text.cp15_mmu_enabled,"ax",%progbits
	.align	2
	.global	cp15_mmu_enabled
	.type	cp15_mmu_enabled, %function
cp15_mmu_enabled:
.LFB9:
	.loc 1 913 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, fp}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #4
.LCFI26:
	.loc 1 917 0
@ 917 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r4, c1, c0, 0
@ 0 "" 2
	str	r4, [fp, #-8]
	.loc 1 937 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #1
	.loc 1 938 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {r4, fp}
	bx	lr
.LFE9:
	.size	cp15_mmu_enabled, .-cp15_mmu_enabled
	.section	.text.cp15_get_mmu_control_reg,"ax",%progbits
	.align	2
	.global	cp15_get_mmu_control_reg
	.type	cp15_get_mmu_control_reg, %function
cp15_get_mmu_control_reg:
.LFB10:
	.loc 1 962 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, fp}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	.loc 1 966 0
@ 966 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r4, c1, c0, 0
@ 0 "" 2
	.loc 1 984 0
	mov	r3, r4
	.loc 1 985 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {r4, fp}
	bx	lr
.LFE10:
	.size	cp15_get_mmu_control_reg, .-cp15_get_mmu_control_reg
	.section	.text.cp15_set_mmu_control_reg,"ax",%progbits
	.align	2
	.global	cp15_set_mmu_control_reg
	.type	cp15_set_mmu_control_reg, %function
cp15_set_mmu_control_reg:
.LFB11:
	.loc 1 1008 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI29:
	add	fp, sp, #0
.LCFI30:
	sub	sp, sp, #8
.LCFI31:
	str	r0, [fp, #-8]
	.loc 1 1010 0
@ 1010 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r3, c1, c0, 0
@ 0 "" 2
	str	r3, [fp, #-4]
	.loc 1 1028 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE11:
	.size	cp15_set_mmu_control_reg, .-cp15_set_mmu_control_reg
	.section	.text.cp15_set_mmu,"ax",%progbits
	.align	2
	.global	cp15_set_mmu
	.type	cp15_set_mmu, %function
cp15_set_mmu:
.LFB12:
	.loc 1 1054 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI32:
	add	fp, sp, #8
.LCFI33:
	sub	sp, sp, #4
.LCFI34:
	str	r0, [fp, #-12]
	.loc 1 1058 0
	bl	cp15_get_mmu_control_reg
	.loc 1 1060 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	.loc 1 1070 0
@ 1070 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r4, c1, c0, 0
@ 0 "" 2
	.loc 1 1089 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE12:
	.size	cp15_set_mmu, .-cp15_set_mmu
	.section	.text.cp15_invalidate_cache,"ax",%progbits
	.align	2
	.global	cp15_invalidate_cache
	.type	cp15_invalidate_cache, %function
cp15_invalidate_cache:
.LFB13:
	.loc 1 1115 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI35:
	add	fp, sp, #0
.LCFI36:
	.loc 1 1118 0
@ 1118 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MOV r0, #0
@ 0 "" 2
	.loc 1 1119 0
@ 1119 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r0, c7, c7, 0
@ 0 "" 2
	.loc 1 1141 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE13:
	.size	cp15_invalidate_cache, .-cp15_invalidate_cache
	.section	.text.cp15_invalidate_tlb,"ax",%progbits
	.align	2
	.global	cp15_invalidate_tlb
	.type	cp15_invalidate_tlb, %function
cp15_invalidate_tlb:
.LFB14:
	.loc 1 1165 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI37:
	add	fp, sp, #0
.LCFI38:
	.loc 1 1167 0
@ 1167 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MOV r0, #0
	MCR p15, 0, r0, c8, c7, 0
	NOP
	NOP
	NOP
	
@ 0 "" 2
	.loc 1 1203 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE14:
	.size	cp15_invalidate_tlb, .-cp15_invalidate_tlb
	.section	.text.cp15_set_transtable_base,"ax",%progbits
	.align	2
	.global	cp15_set_transtable_base
	.type	cp15_set_transtable_base, %function
cp15_set_transtable_base:
.LFB15:
	.loc 1 1228 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI39:
	add	fp, sp, #0
.LCFI40:
	sub	sp, sp, #4
.LCFI41:
	str	r0, [fp, #-4]
	.loc 1 1229 0
	ldr	r3, [fp, #-4]
	bic	r3, r3, #16320
	bic	r3, r3, #63
	str	r3, [fp, #-4]
	.loc 1 1232 0
@ 1232 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r3, c2, c0, 0
@ 0 "" 2
	str	r3, [fp, #-4]
	.loc 1 1250 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE15:
	.size	cp15_set_transtable_base, .-cp15_set_transtable_base
	.section	.text.cp15_set_icache,"ax",%progbits
	.align	2
	.global	cp15_set_icache
	.type	cp15_set_icache, %function
cp15_set_icache:
.LFB16:
	.loc 1 1276 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI42:
	add	fp, sp, #8
.LCFI43:
	sub	sp, sp, #8
.LCFI44:
	str	r0, [fp, #-16]
	.loc 1 1279 0
	bl	cp15_get_mmu_control_reg
	str	r0, [fp, #-12]
	.loc 1 1281 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L94
	.loc 1 1282 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #4096
	str	r3, [fp, #-12]
	b	.L95
.L94:
	.loc 1 1284 0
	ldr	r3, [fp, #-12]
	bic	r3, r3, #4096
	str	r3, [fp, #-12]
.L95:
	.loc 1 1287 0
@ 1287 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r4, c1, c0, 0
@ 0 "" 2
	str	r4, [fp, #-12]
	.loc 1 1304 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE16:
	.size	cp15_set_icache, .-cp15_set_icache
	.section	.text.cp15_set_dcache,"ax",%progbits
	.align	2
	.global	cp15_set_dcache
	.type	cp15_set_dcache, %function
cp15_set_dcache:
.LFB17:
	.loc 1 1330 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI45:
	add	fp, sp, #8
.LCFI46:
	sub	sp, sp, #8
.LCFI47:
	str	r0, [fp, #-16]
	.loc 1 1333 0
	bl	cp15_get_mmu_control_reg
	str	r0, [fp, #-12]
	.loc 1 1335 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L97
	.loc 1 1336 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #4
	str	r3, [fp, #-12]
	b	.L98
.L97:
	.loc 1 1338 0
	ldr	r3, [fp, #-12]
	bic	r3, r3, #4
	str	r3, [fp, #-12]
.L98:
	.loc 1 1341 0
@ 1341 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r4, c1, c0, 0
@ 0 "" 2
	str	r4, [fp, #-12]
	.loc 1 1358 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE17:
	.size	cp15_set_dcache, .-cp15_set_dcache
	.section	.text.cp15_set_domain_access,"ax",%progbits
	.align	2
	.global	cp15_set_domain_access
	.type	cp15_set_domain_access, %function
cp15_set_domain_access:
.LFB18:
	.loc 1 1393 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI48:
	add	fp, sp, #0
.LCFI49:
	sub	sp, sp, #8
.LCFI50:
	str	r0, [fp, #-8]
	.loc 1 1395 0
@ 1395 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r3, c3, c0, 0
@ 0 "" 2
	str	r3, [fp, #-4]
	.loc 1 1411 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE18:
	.size	cp15_set_domain_access, .-cp15_set_domain_access
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8b
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8b
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8b
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8b
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI20-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI22-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI24-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8b
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI27-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8b
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI29-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI32-.LFB12
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI35-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI37-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI39-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI42-.LFB16
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI45-.LFB17
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI48-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE36:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5bb
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF54
	.byte	0x1
	.4byte	.LASF55
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x67
	.4byte	0x65
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x53
	.uleb128 0x5
	.2byte	0x4000
	.byte	0x3
	.byte	0x27
	.4byte	0x9d
	.uleb128 0x6
	.4byte	.LASF13
	.byte	0x3
	.byte	0x29
	.4byte	0x9d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x7
	.4byte	0x48
	.4byte	0xae
	.uleb128 0x8
	.4byte	0xae
	.2byte	0xfff
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x3
	.byte	0x2a
	.4byte	0x85
	.uleb128 0x9
	.byte	0x10
	.byte	0x3
	.byte	0x44
	.4byte	0x101
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x3
	.byte	0x46
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x47
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x3
	.byte	0x48
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x3
	.byte	0x4c
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0xa
	.4byte	0xc0
	.uleb128 0xb
	.4byte	.LASF56
	.byte	0x1
	.byte	0x94
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x14d
	.uleb128 0xc
	.4byte	.LASF18
	.byte	0x1
	.byte	0x94
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x1
	.byte	0x95
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xd
	.ascii	"dcd\000"
	.byte	0x1
	.byte	0x97
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.byte	0xf5
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x1da
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x1
	.byte	0xf5
	.4byte	0x1da
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xf
	.4byte	.LASF20
	.byte	0x1
	.byte	0xf7
	.4byte	0x48
	.byte	0x1
	.byte	0x54
	.uleb128 0xd
	.ascii	"tlb\000"
	.byte	0x1
	.byte	0xf8
	.4byte	0x1dc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xf
	.4byte	.LASF21
	.byte	0x1
	.byte	0xf8
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xf
	.4byte	.LASF22
	.byte	0x1
	.byte	0xf8
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xf
	.4byte	.LASF23
	.byte	0x1
	.byte	0xf8
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x1
	.byte	0xf9
	.4byte	0x1dc
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xf
	.4byte	.LASF18
	.byte	0x1
	.byte	0xf9
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.uleb128 0x11
	.byte	0x4
	.4byte	0x48
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x174
	.byte	0x1
	.4byte	0x1da
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x287
	.uleb128 0x13
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x174
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x14
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x176
	.4byte	0x48
	.byte	0x1
	.byte	0x54
	.uleb128 0x15
	.ascii	"tlb\000"
	.byte	0x1
	.2byte	0x177
	.4byte	0x1dc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x177
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x14
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x177
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x178
	.4byte	0x1dc
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x14
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x178
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x14
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x178
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x179
	.4byte	0x1da
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x205
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x2ce
	.uleb128 0x13
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x205
	.4byte	0x1dc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x206
	.4byte	0x1dc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x208
	.4byte	0x1dc
	.byte	0x1
	.byte	0x54
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x276
	.byte	0x1
	.4byte	0x7a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x363
	.uleb128 0x17
	.ascii	"tt\000"
	.byte	0x1
	.2byte	0x276
	.4byte	0x363
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x13
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x277
	.4byte	0x369
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x14
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x279
	.4byte	0x48
	.byte	0x1
	.byte	0x54
	.uleb128 0x15
	.ascii	"idx\000"
	.byte	0x1
	.2byte	0x27a
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x27b
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x27c
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x27d
	.4byte	0x1dc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x15
	.ascii	"ret\000"
	.byte	0x1
	.2byte	0x27e
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x11
	.byte	0x4
	.4byte	0xb5
	.uleb128 0x11
	.byte	0x4
	.4byte	0x101
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x2eb
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x399
	.uleb128 0x13
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x2eb
	.4byte	0x1dc
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x303
	.byte	0x1
	.4byte	0x1dc
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x3c6
	.uleb128 0x15
	.ascii	"ttb\000"
	.byte	0x1
	.2byte	0x305
	.4byte	0x48
	.byte	0x1
	.byte	0x54
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x32d
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x365
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x390
	.byte	0x1
	.4byte	0x7a
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x420
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x392
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x3c1
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x44d
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x3c3
	.4byte	0x48
	.byte	0x1
	.byte	0x54
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x3ef
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x477
	.uleb128 0x13
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x3ef
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x41d
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x4ad
	.uleb128 0x13
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x41d
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x41f
	.4byte	0x48
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x45a
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x48c
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x4cb
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x503
	.uleb128 0x13
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x4cb
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x4fb
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x53c
	.uleb128 0x13
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x4fb
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x4fd
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x531
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x575
	.uleb128 0x13
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x531
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x533
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x570
	.byte	0x1
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0x59f
	.uleb128 0x17
	.ascii	"dac\000"
	.byte	0x1
	.2byte	0x570
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF53
	.byte	0x1
	.byte	0x23
	.4byte	0x1dc
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF53
	.byte	0x1
	.byte	0x23
	.4byte	0x1dc
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	virtual_tlb_addr
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI21
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI23
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI30
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI33
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI36
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI38
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI40
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI43
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI46
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI49
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xac
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF37:
	.ascii	"cp15_set_vmmu_addr\000"
.LASF14:
	.ascii	"num_sections\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF38:
	.ascii	"cp15_get_ttb\000"
.LASF27:
	.ascii	"index2\000"
.LASF45:
	.ascii	"cp15_set_mmu\000"
.LASF43:
	.ascii	"cp15_get_mmu_control_reg\000"
.LASF31:
	.ascii	"ttsbp\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF49:
	.ascii	"cp15_set_transtable_base\000"
.LASF17:
	.ascii	"entry\000"
.LASF20:
	.ascii	"status\000"
.LASF19:
	.ascii	"addr\000"
.LASF51:
	.ascii	"cp15_set_dcache\000"
.LASF46:
	.ascii	"enable\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF21:
	.ascii	"tlb_entry\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF7:
	.ascii	"INT_32\000"
.LASF18:
	.ascii	"level2\000"
.LASF29:
	.ascii	"end_adr\000"
.LASF26:
	.ascii	"cp15_map_physical_to_virtual\000"
.LASF40:
	.ascii	"cp15_write_buffer_flush\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF55:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/lpc_arm922t_cp15_driver.c\000"
.LASF32:
	.ascii	"control\000"
.LASF53:
	.ascii	"virtual_tlb_addr\000"
.LASF15:
	.ascii	"virt_addr\000"
.LASF30:
	.ascii	"cp15_init_mmu_trans_table\000"
.LASF13:
	.ascii	"vidx\000"
.LASF35:
	.ascii	"uiptr\000"
.LASF44:
	.ascii	"cp15_set_mmu_control_reg\000"
.LASF5:
	.ascii	"unsigned int\000"
.LASF39:
	.ascii	"cp15_dcache_flush\000"
.LASF0:
	.ascii	"char\000"
.LASF24:
	.ascii	"page_table\000"
.LASF16:
	.ascii	"phys_addr\000"
.LASF12:
	.ascii	"TRANSTABLE_T\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF33:
	.ascii	"va_idx\000"
.LASF47:
	.ascii	"cp15_invalidate_cache\000"
.LASF36:
	.ascii	"cp15_force_cache_coherence\000"
.LASF9:
	.ascii	"long long int\000"
.LASF28:
	.ascii	"start_adr\000"
.LASF41:
	.ascii	"cp15_mmu_enabled\000"
.LASF22:
	.ascii	"index\000"
.LASF42:
	.ascii	"mmu_reg\000"
.LASF25:
	.ascii	"cp15_map_virtual_to_physical\000"
.LASF4:
	.ascii	"short int\000"
.LASF34:
	.ascii	"pa_addr\000"
.LASF54:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF23:
	.ascii	"virtual_addr\000"
.LASF50:
	.ascii	"cp15_set_icache\000"
.LASF56:
	.ascii	"cp15_decode_level2\000"
.LASF2:
	.ascii	"signed char\000"
.LASF48:
	.ascii	"cp15_invalidate_tlb\000"
.LASF52:
	.ascii	"cp15_set_domain_access\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
