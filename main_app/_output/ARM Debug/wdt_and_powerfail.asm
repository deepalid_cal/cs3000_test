	.file	"wdt_and_powerfail.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.rodata.RESTART_INFO_VERIFY_STRING_PRE,"a",%progbits
	.align	2
	.type	RESTART_INFO_VERIFY_STRING_PRE, %object
	.size	RESTART_INFO_VERIFY_STRING_PRE, 12
RESTART_INFO_VERIFY_STRING_PRE:
	.ascii	"RESTART v03\000"
	.section .rodata
	.align	2
.LC0:
	.ascii	"Restart Info\000"
	.align	2
.LC1:
	.ascii	"EXCEPTION: %s (%s)\000"
	.align	2
.LC2:
	.ascii	"%s (%s)\000"
	.align	2
.LC3:
	.ascii	"Factory Reset for NEW panel\000"
	.align	2
.LC4:
	.ascii	"Factory Reset for OLD panel\000"
	.align	2
.LC5:
	.ascii	"Factory Reset by user KEYPAD\000"
	.align	2
.LC6:
	.ascii	"Reset: HUB to distribute tpmicro binary\000"
	.align	2
.LC7:
	.ascii	"Reset: HUB to distribute main binary\000"
	.align	2
.LC8:
	.ascii	"Reset: HUB to distribute both binaries\000"
	.align	2
.LC9:
	.ascii	"DIRTY POWER or RESET BUTTON PUSHED\000"
	.section	.text.init_restart_info,"ax",%progbits
	.align	2
	.global	init_restart_info
	.type	init_restart_info, %function
init_restart_info:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_and_powerfail/wdt_and_powerfail.c"
	.loc 1 57 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #16
.LCFI2:
	str	r0, [fp, #-20]
	.loc 1 67 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 72 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	beq	.L2
	.loc 1 72 0 is_stmt 0 discriminator 1
	ldr	r0, .L22
	ldr	r1, .L22+4
	mov	r2, #12
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	beq	.L3
.L2:
	.loc 1 74 0 is_stmt 1
	ldr	r0, .L22+8
	ldr	r1, [fp, #-20]
	bl	Alert_battery_backed_var_initialized
	.loc 1 80 0
	ldr	r0, .L22
	mov	r1, #0
	mov	r2, #300
	bl	memset
	.loc 1 95 0
	ldr	r0, .L22
	ldr	r1, .L22+4
	mov	r2, #16
	bl	strlcpy
	b	.L4
.L3:
.LBB2:
	.loc 1 105 0
	ldr	r3, .L22
	ldr	r3, [r3, #92]
	cmp	r3, #0
	beq	.L5
	.loc 1 107 0
	ldr	r0, .L22+12
	ldr	r1, .L22+16
	ldr	r2, .L22+20
	bl	Alert_Message_va
	.loc 1 111 0
	ldr	r3, .L22
	mov	r2, #0
	str	r2, [r3, #92]
	.loc 1 113 0
	ldr	r0, .L22+16
	mov	r1, #0
	mov	r2, #48
	bl	memset
	.loc 1 115 0
	ldr	r0, .L22+20
	mov	r1, #0
	mov	r2, #48
	bl	memset
.L5:
	.loc 1 120 0
	ldr	r3, .L22
	ldr	r3, [r3, #200]
	cmp	r3, #0
	beq	.L6
	.loc 1 122 0
	ldr	r0, .L22+24
	ldr	r1, .L22+28
	ldr	r2, .L22+32
	bl	Alert_Message_va
	.loc 1 126 0
	ldr	r3, .L22
	mov	r2, #0
	str	r2, [r3, #200]
	.loc 1 128 0
	ldr	r0, .L22+28
	mov	r1, #0
	mov	r2, #64
	bl	memset
	.loc 1 130 0
	ldr	r0, .L22+32
	mov	r1, #0
	mov	r2, #24
	bl	memset
.L6:
	.loc 1 137 0
	ldr	r3, .L22+36
	str	r3, [fp, #-12]
	.loc 1 143 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #28]
	and	r3, r3, #1
	str	r3, [fp, #-16]
	.loc 1 147 0
	ldr	r3, .L22
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L7
	.loc 1 149 0
	ldr	r3, .L22
	ldr	r3, [r3, #80]
	cmp	r3, #26
	bne	.L8
	.loc 1 153 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L9
	.loc 1 157 0
	ldr	r3, .L22
	ldr	r3, [r3, #64]
	ldr	r0, .L22+40
	mov	r1, r3
	bl	Alert_power_fail_brownout_idx
	.loc 1 258 0
	b	.L21
.L9:
	.loc 1 163 0
	ldr	r3, .L22
	ldr	r3, [r3, #64]
	ldr	r0, .L22+40
	mov	r1, r3
	bl	Alert_power_fail_idx
	.loc 1 258 0
	b	.L21
.L8:
	.loc 1 168 0
	ldr	r3, .L22
	ldr	r3, [r3, #80]
	mov	r0, r3
	bl	Alert_reboot_request
	.loc 1 174 0
	ldr	r3, .L22
	ldr	r3, [r3, #80]
	sub	r3, r3, #27
	cmp	r3, #14
	ldrls	pc, [pc, r3, asl #2]
	b	.L21
.L17:
	.word	.L11
	.word	.L12
	.word	.L13
	.word	.L21
	.word	.L21
	.word	.L21
	.word	.L21
	.word	.L21
	.word	.L21
	.word	.L21
	.word	.L21
	.word	.L21
	.word	.L14
	.word	.L15
	.word	.L16
.L12:
	.loc 1 180 0
	ldr	r0, .L22+44
	bl	Alert_Message
	.loc 1 182 0
	ldr	r3, .L22+48
	mov	r2, #0
	strb	r2, [r3, #129]
	.loc 1 184 0
	ldr	r3, .L22+48
	mov	r2, #1
	strb	r2, [r3, #128]
	.loc 1 185 0
	b	.L18
.L13:
	.loc 1 192 0
	ldr	r0, .L22+52
	bl	Alert_Message
	.loc 1 194 0
	ldr	r3, .L22+48
	mov	r2, #1
	strb	r2, [r3, #129]
	.loc 1 196 0
	ldr	r3, .L22+48
	mov	r2, #0
	strb	r2, [r3, #128]
	.loc 1 197 0
	b	.L18
.L11:
	.loc 1 203 0
	ldr	r0, .L22+56
	bl	Alert_Message
	.loc 1 205 0
	ldr	r3, .L22+48
	mov	r2, #1
	strb	r2, [r3, #129]
	.loc 1 207 0
	ldr	r3, .L22+48
	mov	r2, #0
	strb	r2, [r3, #128]
	.loc 1 208 0
	b	.L18
.L14:
	.loc 1 216 0
	ldr	r0, .L22+60
	bl	Alert_Message
	.loc 1 218 0
	ldr	r3, .L22+48
	mov	r2, #1
	str	r2, [r3, #136]
	.loc 1 224 0
	b	.L18
.L15:
	.loc 1 232 0
	ldr	r0, .L22+64
	bl	Alert_Message
	.loc 1 234 0
	ldr	r3, .L22+48
	mov	r2, #1
	str	r2, [r3, #140]
	.loc 1 240 0
	mov	r0, r0	@ nop
	b	.L18
.L16:
	.loc 1 248 0
	ldr	r0, .L22+68
	bl	Alert_Message
	.loc 1 250 0
	ldr	r3, .L22+48
	mov	r2, #1
	str	r2, [r3, #136]
	.loc 1 252 0
	ldr	r3, .L22+48
	mov	r2, #1
	str	r2, [r3, #140]
	.loc 1 258 0
	b	.L21
.L7:
	.loc 1 264 0
	ldr	r3, [fp, #-16]
	cmp	r3, #1
	bne	.L19
	.loc 1 266 0
	bl	Alert_watchdog_timeout
	b	.L18
.L19:
	.loc 1 275 0
	ldr	r0, .L22+72
	bl	Alert_Message
	b	.L18
.L21:
	.loc 1 258 0
	mov	r0, r0	@ nop
.L18:
	.loc 1 282 0
	bl	Alert_divider_large
	.loc 1 288 0
	ldr	r3, .L22
	mov	r2, #0
	str	r2, [r3, #68]
	.loc 1 292 0
	ldr	r3, .L22
	mov	r2, #0
	str	r2, [r3, #80]
	.loc 1 294 0
	ldr	r3, .L22
	mov	r2, #0
	str	r2, [r3, #88]
	.loc 1 296 0
	ldr	r3, .L22
	mov	r2, #0
	str	r2, [r3, #84]
	.loc 1 300 0
	ldr	r0, .L22+76
	bl	strlen
	mov	r3, r0
	ldr	r0, .L22+76
	ldr	r1, .L22+80
	mov	r2, r3
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	beq	.L20
	.loc 1 302 0
	ldr	r3, .L22
	ldr	r3, [r3, #64]
	ldr	r0, .L22+76
	ldr	r1, .L22+80
	mov	r2, r3
	bl	Alert_firmware_update_idx
	b	.L4
.L20:
	.loc 1 314 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L4:
.LBE2:
	.loc 1 318 0
	ldr	r3, [fp, #-8]
	.loc 1 319 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L23:
	.align	2
.L22:
	.word	restart_info
	.word	RESTART_INFO_VERIFY_STRING_PRE
	.word	.LC0
	.word	.LC1
	.word	restart_info+150
	.word	restart_info+102
	.word	.LC2
	.word	restart_info+234
	.word	restart_info+210
	.word	1073987584
	.word	restart_info+72
	.word	.LC3
	.word	weather_preserves
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	restart_info+16
	.word	-2147483584
.LFE0:
	.size	init_restart_info, .-init_restart_info
	.section	.bss.SYSTEM_shutdown_event_queue,"aw",%nobits
	.align	2
	.type	SYSTEM_shutdown_event_queue, %object
	.size	SYSTEM_shutdown_event_queue, 4
SYSTEM_shutdown_event_queue:
	.space	4
	.section	.text.powerfail_isr,"ax",%progbits
	.align	2
	.type	powerfail_isr, %function
powerfail_isr:
.LFB1:
	.loc 1 329 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	.loc 1 342 0
	ldr	r3, .L26
	ldr	r2, .L26
	ldr	r2, [r2, #0]
	bic	r2, r2, #8388608
	str	r2, [r3, #0]
	.loc 1 348 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 352 0
	mov	r3, #26
	str	r3, [fp, #-8]
	.loc 1 354 0
	ldr	r3, .L26+4
	ldr	r1, [r3, #0]
	sub	r2, fp, #8
	sub	r3, fp, #12
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #0
	bl	xQueueGenericSendFromISR
	.loc 1 356 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L24
.LBB3:
	.loc 1 358 0
	bl	vTaskSwitchContext
.L24:
.LBE3:
	.loc 1 369 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L27:
	.align	2
.L26:
	.word	1073807360
	.word	SYSTEM_shutdown_event_queue
.LFE1:
	.size	powerfail_isr, .-powerfail_isr
	.section	.text.init_powerfail_isr,"ax",%progbits
	.align	2
	.type	init_powerfail_isr, %function
init_powerfail_isr:
.LFB2:
	.loc 1 373 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #4
.LCFI8:
	.loc 1 374 0
	mov	r0, #87
	bl	xDisable_ISR
	.loc 1 377 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #87
	mov	r1, #0
	mov	r2, #1
	ldr	r3, .L29
	bl	xSetISR_Vector
	.loc 1 379 0
	mov	r0, #87
	bl	xEnable_ISR
	.loc 1 380 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L30:
	.align	2
.L29:
	.word	powerfail_isr
.LFE2:
	.size	init_powerfail_isr, .-init_powerfail_isr
	.section	.text.SYSTEM_application_requested_restart,"ax",%progbits
	.align	2
	.global	SYSTEM_application_requested_restart
	.type	SYSTEM_application_requested_restart, %function
SYSTEM_application_requested_restart:
.LFB3:
	.loc 1 384 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-12]
	.loc 1 387 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 389 0
	ldr	r3, .L33
	ldr	r2, [r3, #0]
	sub	r3, fp, #8
	mov	r0, r2
	mov	r1, r3
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
.L32:
	.loc 1 394 0 discriminator 1
	b	.L32
.L34:
	.align	2
.L33:
	.word	SYSTEM_shutdown_event_queue
.LFE3:
	.size	SYSTEM_application_requested_restart, .-SYSTEM_application_requested_restart
	.section	.text.system_shutdown_task,"ax",%progbits
	.align	2
	.global	system_shutdown_task
	.type	system_shutdown_task, %function
system_shutdown_task:
.LFB4:
	.loc 1 399 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #16
.LCFI14:
	str	r0, [fp, #-20]
	.loc 1 413 0
	mov	r0, #10
	mov	r1, #4
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L47
	str	r2, [r3, #0]
	.loc 1 418 0
	bl	init_powerfail_isr
.L46:
	.loc 1 425 0
	ldr	r3, .L47
	ldr	r2, [r3, #0]
	sub	r3, fp, #16
	mov	r0, r2
	mov	r1, r3
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	mov	r3, r0
	cmp	r3, #1
	bne	.L36
.LBB4:
	.loc 1 435 0
	ldr	r3, .L47+4
	str	r3, [fp, #-12]
	.loc 1 448 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 454 0
	ldr	r3, .L47+8
	mov	r2, #16384
	str	r2, [r3, #0]
	.loc 1 462 0
	ldr	r3, .L47+8
	mov	r2, #524288
	str	r2, [r3, #0]
	.loc 1 464 0
	ldr	r3, .L47+8
	mov	r2, #4194304
	str	r2, [r3, #0]
	.loc 1 474 0
	mov	r0, #1
	bl	SERIAL_add_daily_stats_to_monthly_battery_backed
	.loc 1 485 0
	ldr	r3, .L47+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L37
	.loc 1 490 0
	ldr	r3, .L47+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	vTaskSuspend
.L37:
	.loc 1 493 0
	ldr	r3, .L47+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L38
	.loc 1 498 0
	ldr	r3, .L47+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	vTaskSuspend
.L38:
	.loc 1 501 0
	ldr	r3, .L47+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L39
	.loc 1 506 0
	ldr	r3, .L47+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	vTaskSuspend
.L39:
	.loc 1 514 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L40
.L42:
	.loc 1 516 0
	ldr	r3, .L47+24
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L41
	.loc 1 518 0
	ldr	r3, .L47+24
	ldr	r2, [fp, #-8]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L47+28
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L41
	.loc 1 518 0 is_stmt 0 discriminator 1
	ldr	r3, .L47+24
	ldr	r2, [fp, #-8]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L47+32
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L41
	.loc 1 523 0 is_stmt 1
	ldr	r3, .L47+24
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	vTaskSuspend
.L41:
	.loc 1 514 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L40:
	.loc 1 514 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #23
	bls	.L42
	.loc 1 544 0 is_stmt 1
	ldr	r3, .L47+36
	mov	r2, #1
	str	r2, [r3, #68]
	.loc 1 547 0
	ldr	r0, .L47+40
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 553 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L47+36
	str	r2, [r3, #80]
	.loc 1 585 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L43
.L44:
	.loc 1 590 0 discriminator 2
	mov	r0, #2
	bl	vTaskDelay
	.loc 1 585 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L43:
	.loc 1 585 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #29
	bls	.L44
	.loc 1 599 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #16]
	sub	r2, r3, #2
	ldr	r3, [fp, #-12]
	str	r2, [r3, #8]
.L45:
	.loc 1 605 0 discriminator 1
	b	.L45
.L36:
.LBE4:
	.loc 1 613 0
	bl	xTaskGetTickCount
	mov	r2, r0
	ldr	r3, .L47+44
	str	r2, [r3, #0]
	.loc 1 617 0
	b	.L46
.L48:
	.align	2
.L47:
	.word	SYSTEM_shutdown_event_queue
	.word	1073987584
	.word	1073905672
	.word	timer_task_handle
	.word	epson_rtc_task_handle
	.word	wdt_task_handle
	.word	created_task_handles
	.word	flash_storage_0_task_handle
	.word	flash_storage_1_task_handle
	.word	restart_info
	.word	restart_info+72
	.word	pwrfail_task_last_x_stamp
.LFE4:
	.size	system_shutdown_task, .-system_shutdown_task
	.section .rodata
	.align	2
.LC10:
	.ascii	"New Monitoring Delta: %u ms\000"
	.section	.text.wdt_monitoring_task,"ax",%progbits
	.align	2
	.global	wdt_monitoring_task
	.type	wdt_monitoring_task, %function
wdt_monitoring_task:
.LFB5:
	.loc 1 623 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #28
.LCFI17:
	str	r0, [fp, #-32]
	.loc 1 626 0
	mov	r0, #17
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 633 0
	ldr	r3, .L65
	str	r3, [fp, #-20]
	.loc 1 646 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 654 0
	mov	r0, #5
	bl	clkpwr_get_base_clock_rate
	mov	r2, r0
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #1
	mov	r2, r3
	ldr	r3, [fp, #-20]
	str	r2, [r3, #16]
	.loc 1 664 0
	ldr	r3, [fp, #-20]
	mov	r2, #20
	str	r2, [r3, #12]
	.loc 1 669 0
	ldr	r3, [fp, #-20]
	ldr	r2, .L65+4
	str	r2, [r3, #24]
	.loc 1 672 0
	ldr	r3, [fp, #-20]
	mov	r2, #32
	str	r2, [r3, #20]
	.loc 1 676 0
	ldr	r3, [fp, #-20]
	mov	r2, #5
	str	r2, [r3, #4]
	.loc 1 683 0
	bl	xTaskGetTickCount
	mov	r2, r0
	ldr	r3, .L65+8
	str	r2, [r3, #0]
	.loc 1 685 0
	bl	xTaskGetTickCount
	mov	r2, r0
	ldr	r3, .L65+12
	str	r2, [r3, #0]
	.loc 1 689 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L50
.L51:
	.loc 1 691 0 discriminator 2
	bl	xTaskGetTickCount
	mov	r1, r0
	ldr	r3, .L65+16
	ldr	r2, [fp, #-8]
	str	r1, [r3, r2, asl #2]
	.loc 1 689 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L50:
	.loc 1 689 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #23
	bls	.L51
	.loc 1 705 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 713 0
	bl	xTaskGetTickCount
	mov	r2, r0
	ldr	r3, .L65+20
	str	r2, [r3, #0]
	.loc 1 715 0
	ldr	r3, .L65+24
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L63
.L64:
	.loc 1 941 0
	mov	r0, r0	@ nop
.L63:
.LBB5:
	.loc 1 726 0
	mov	r0, #100
	bl	vTaskDelay
	.loc 1 732 0
	bl	xTaskGetTickCount
	str	r0, [fp, #-24]
	.loc 1 738 0
	ldr	r3, .L65+20
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-24]
	cmp	r2, r3
	bls	.L52
	.loc 1 740 0
	ldr	r3, .L65+20
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-24]
	rsb	r2, r3, r2
	ldr	r3, .L65+24
	ldr	r1, [r3, #0]
	ldr	r3, .L65+28
	umull	r0, r3, r1, r3
	mov	r3, r3, lsr #2
	cmp	r2, r3
	bls	.L52
	.loc 1 742 0
	ldr	r3, .L65+20
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-24]
	rsb	r2, r3, r2
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r2, r3, r2
	ldr	r3, .L65+24
	str	r2, [r3, #0]
	.loc 1 748 0
	ldr	r3, .L65+24
	ldr	r2, [r3, #0]
	ldr	r3, .L65+32
	cmp	r2, r3
	bls	.L52
	.loc 1 750 0
	ldr	r3, .L65+24
	ldr	r3, [r3, #0]
	ldr	r0, .L65+36
	mov	r1, r3
	bl	Alert_Message_va
.L52:
	.loc 1 755 0
	ldr	r3, .L65+20
	ldr	r2, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 759 0
	ldr	r3, [fp, #-12]
	cmp	r3, #5
	bhi	.L53
	.loc 1 761 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 763 0
	ldr	r3, [fp, #-12]
	cmp	r3, #6
	bne	.L53
	.loc 1 772 0
	mov	r0, #5
	bl	clkpwr_get_base_clock_rate
	mov	r3, r0
	mov	r2, r3, asl #3
	ldr	r3, [fp, #-20]
	str	r2, [r3, #16]
.L53:
	.loc 1 780 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 784 0
	ldr	r3, [fp, #-12]
	cmp	r3, #5
	bls	.L54
.LBB6:
	.loc 1 805 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L55
.L58:
	.loc 1 809 0
	ldr	r3, .L65+40
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #5]
	cmp	r3, #0
	beq	.L56
	.loc 1 811 0
	ldr	r3, .L65+44
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L56
	.loc 1 813 0
	ldr	r1, .L65+40
	ldr	r2, [fp, #-8]
	mov	r3, #4
	mov	r2, r2, asl #5
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L56
	.loc 1 816 0
	ldr	r3, .L65+16
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	str	r3, [fp, #-28]
	.loc 1 821 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bcc	.L57
	.loc 1 825 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-28]
	rsb	r2, r3, r2
	ldr	r0, .L65+40
	ldr	r1, [fp, #-8]
	mov	r3, #8
	mov	r1, r1, asl #5
	add	r1, r0, r1
	add	r3, r1, r3
	ldr	r1, [r3, #0]
	ldr	r3, .L65+28
	umull	r0, r3, r1, r3
	mov	r3, r3, lsr #2
	cmp	r2, r3
	bls	.L56
	.loc 1 828 0
	ldr	r3, .L65+44
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	pcTaskGetTaskName
	mov	r1, r0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-28]
	rsb	r2, r3, r2
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r0, r1
	mov	r1, r3
	bl	Alert_task_frozen
	.loc 1 833 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L56
.L57:
	.loc 1 850 0
	ldr	r3, .L65+16
	ldr	r2, [fp, #-8]
	ldr	r1, [fp, #-24]
	str	r1, [r3, r2, asl #2]
.L56:
	.loc 1 805 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L55:
	.loc 1 805 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #23
	bls	.L58
	.loc 1 879 0 is_stmt 1
	ldr	r3, .L65+8
	ldr	r3, [r3, #0]
	str	r3, [fp, #-28]
	.loc 1 882 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bcc	.L59
	.loc 1 885 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-28]
	rsb	r3, r3, r2
	cmp	r3, #200
	bls	.L60
	.loc 1 888 0
	ldr	r3, .L65+48
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	pcTaskGetTaskName
	mov	r1, r0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-28]
	rsb	r2, r3, r2
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r0, r1
	mov	r1, r3
	bl	Alert_task_frozen
	.loc 1 893 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L60
.L59:
	.loc 1 898 0
	ldr	r3, .L65+8
	ldr	r2, [fp, #-24]
	str	r2, [r3, #0]
.L60:
	.loc 1 907 0
	ldr	r3, .L65+12
	ldr	r3, [r3, #0]
	str	r3, [fp, #-28]
	.loc 1 910 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bcc	.L61
	.loc 1 913 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-28]
	rsb	r3, r3, r2
	cmp	r3, #200
	bls	.L54
	.loc 1 916 0
	ldr	r3, .L65+52
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	pcTaskGetTaskName
	mov	r1, r0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-28]
	rsb	r2, r3, r2
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r0, r1
	mov	r1, r3
	bl	Alert_task_frozen
	.loc 1 921 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L54
.L61:
	.loc 1 926 0
	ldr	r3, .L65+12
	ldr	r2, [fp, #-24]
	str	r2, [r3, #0]
.L54:
.LBE6:
	.loc 1 933 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L64
	.loc 1 936 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	str	r2, [r3, #8]
.LBE5:
	.loc 1 941 0
	b	.L64
.L66:
	.align	2
.L65:
	.word	1073987584
	.word	65535
	.word	rtc_task_last_x_stamp
	.word	pwrfail_task_last_x_stamp
	.word	task_last_execution_stamp
	.word	last_x_stamp.8135
	.word	greatest_diff_ms.8136
	.word	-858993459
	.word	750
	.word	.LC10
	.word	Task_Table
	.word	created_task_handles
	.word	epson_rtc_task_handle
	.word	powerfail_task_handle
.LFE5:
	.size	wdt_monitoring_task, .-wdt_monitoring_task
	.section	.text.freeze_and_restart_app,"ax",%progbits
	.align	2
	.global	freeze_and_restart_app
	.type	freeze_and_restart_app, %function
freeze_and_restart_app:
.LFB6:
	.loc 1 1024 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI18:
	add	fp, sp, #0
.LCFI19:
.L68:
	.loc 1 1025 0 discriminator 1
	b	.L68
.LFE6:
	.size	freeze_and_restart_app, .-freeze_and_restart_app
	.section .rodata
	.align	2
.LC11:
	.ascii	"Undef Instr at 0x%08lX 0x%08lX\000"
	.align	2
.LC12:
	.ascii	"%s\000"
	.section	.text.undefined_instruction_handler,"ax",%progbits
	.align	2
	.global	undefined_instruction_handler
	.type	undefined_instruction_handler, %function
undefined_instruction_handler:
.LFB7:
	.loc 1 1031 0
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	.loc 1 1034 0
@ 1034 "C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_and_powerfail/wdt_and_powerfail.c" 1
	sub lr, lr, #8
mov r4, lr
@ 0 "" 2
	.loc 1 1052 0
	ldr	r3, .L71
	mov	r2, #1
	str	r2, [r3, #92]
	.loc 1 1054 0
	ldr	r0, .L71+4
	bl	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	.loc 1 1058 0
	ldr	r3, [r4, #0]
	str	r3, [sp, #0]
	ldr	r0, .L71+8
	mov	r1, #48
	ldr	r2, .L71+12
	mov	r3, r4
	bl	snprintf
	.loc 1 1060 0
	mov	r0, #0
	bl	pcTaskGetTaskName
	mov	r3, r0
	ldr	r0, .L71+16
	mov	r1, #48
	ldr	r2, .L71+20
	bl	snprintf
.L70:
	.loc 1 1068 0 discriminator 1
	b	.L70
.L72:
	.align	2
.L71:
	.word	restart_info
	.word	restart_info+96
	.word	restart_info+150
	.word	.LC11
	.word	restart_info+102
	.word	.LC12
.LFE7:
	.size	undefined_instruction_handler, .-undefined_instruction_handler
	.section .rodata
	.align	2
.LC13:
	.ascii	"Prefetch Abort at 0x%08lX 0x%08lX\000"
	.section	.text.prefetch_abort_handler,"ax",%progbits
	.align	2
	.global	prefetch_abort_handler
	.type	prefetch_abort_handler, %function
prefetch_abort_handler:
.LFB8:
	.loc 1 1074 0
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	.loc 1 1077 0
@ 1077 "C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_and_powerfail/wdt_and_powerfail.c" 1
	sub lr, lr, #8
mov r4, lr
@ 0 "" 2
	.loc 1 1095 0
	ldr	r3, .L75
	mov	r2, #1
	str	r2, [r3, #92]
	.loc 1 1097 0
	ldr	r0, .L75+4
	bl	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	.loc 1 1101 0
	ldr	r3, [r4, #0]
	str	r3, [sp, #0]
	ldr	r0, .L75+8
	mov	r1, #48
	ldr	r2, .L75+12
	mov	r3, r4
	bl	snprintf
	.loc 1 1103 0
	mov	r0, #0
	bl	pcTaskGetTaskName
	mov	r3, r0
	ldr	r0, .L75+16
	mov	r1, #48
	ldr	r2, .L75+20
	bl	snprintf
.L74:
	.loc 1 1111 0 discriminator 1
	b	.L74
.L76:
	.align	2
.L75:
	.word	restart_info
	.word	restart_info+96
	.word	restart_info+150
	.word	.LC13
	.word	restart_info+102
	.word	.LC12
.LFE8:
	.size	prefetch_abort_handler, .-prefetch_abort_handler
	.section .rodata
	.align	2
.LC14:
	.ascii	"Data Abort at 0x%08lX 0x%08lX\000"
	.section	.text.data_abort_handler,"ax",%progbits
	.align	2
	.global	data_abort_handler
	.type	data_abort_handler, %function
data_abort_handler:
.LFB9:
	.loc 1 1117 0
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	.loc 1 1120 0
@ 1120 "C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_and_powerfail/wdt_and_powerfail.c" 1
	sub lr, lr, #8
mov r4, lr
@ 0 "" 2
	.loc 1 1138 0
	ldr	r3, .L79
	mov	r2, #1
	str	r2, [r3, #92]
	.loc 1 1140 0
	ldr	r0, .L79+4
	bl	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	.loc 1 1144 0
	ldr	r3, [r4, #0]
	str	r3, [sp, #0]
	ldr	r0, .L79+8
	mov	r1, #48
	ldr	r2, .L79+12
	mov	r3, r4
	bl	snprintf
	.loc 1 1146 0
	mov	r0, #0
	bl	pcTaskGetTaskName
	mov	r3, r0
	ldr	r0, .L79+16
	mov	r1, #48
	ldr	r2, .L79+20
	bl	snprintf
.L78:
	.loc 1 1154 0 discriminator 1
	b	.L78
.L80:
	.align	2
.L79:
	.word	restart_info
	.word	restart_info+96
	.word	restart_info+150
	.word	.LC14
	.word	restart_info+102
	.word	.LC12
.LFE9:
	.size	data_abort_handler, .-data_abort_handler
	.section .rodata
	.align	2
.LC15:
	.ascii	"ASSERT: %s, %s, %d\000"
	.section	.text.__assert,"ax",%progbits
	.align	2
	.global	__assert
	.type	__assert, %function
__assert:
.LFB10:
	.loc 1 1163 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI20:
	add	fp, sp, #4
.LCFI21:
	sub	sp, sp, #20
.LCFI22:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 1166 0
	ldr	r3, .L83
	mov	r2, #1
	str	r2, [r3, #200]
	.loc 1 1168 0
	ldr	r0, .L83+4
	bl	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	.loc 1 1170 0
	ldr	r0, [fp, #-12]
	bl	RemovePathFromFileName
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r3, [fp, #-16]
	str	r3, [sp, #4]
	ldr	r0, .L83+8
	mov	r1, #64
	ldr	r2, .L83+12
	ldr	r3, [fp, #-8]
	bl	snprintf
	.loc 1 1172 0
	mov	r0, #0
	bl	pcTaskGetTaskName
	mov	r3, r0
	ldr	r0, .L83+16
	mov	r1, #24
	ldr	r2, .L83+20
	bl	snprintf
.L82:
	.loc 1 1180 0 discriminator 1
	b	.L82
.L84:
	.align	2
.L83:
	.word	restart_info
	.word	restart_info+204
	.word	restart_info+234
	.word	.LC15
	.word	restart_info+210
	.word	.LC12
.LFE10:
	.size	__assert, .-__assert
	.section	.bss.last_x_stamp.8135,"aw",%nobits
	.align	2
	.type	last_x_stamp.8135, %object
	.size	last_x_stamp.8135, 4
last_x_stamp.8135:
	.space	4
	.section	.bss.greatest_diff_ms.8136,"aw",%nobits
	.align	2
	.type	greatest_diff_ms.8136, %object
	.size	greatest_diff_ms.8136, 4
greatest_diff_ms.8136:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI20-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_comm_server_common.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/projdefs.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/task.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_intc_driver.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_wdt.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xd2d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF197
	.byte	0x1
	.4byte	.LASF198
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x70
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x4c
	.4byte	0xbc
	.uleb128 0x6
	.4byte	.LASF13
	.byte	0x3
	.byte	0x55
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x3
	.byte	0x57
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x59
	.4byte	0x97
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x65
	.4byte	0xec
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x3
	.byte	0x67
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x3
	.byte	0x69
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x3
	.byte	0x6b
	.4byte	0xc7
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x4
	.byte	0x47
	.4byte	0x102
	.uleb128 0x7
	.byte	0x4
	.4byte	0x108
	.uleb128 0x8
	.byte	0x1
	.4byte	0x114
	.uleb128 0x9
	.4byte	0x114
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF19
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x5
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x6
	.byte	0x63
	.4byte	0x114
	.uleb128 0x3
	.4byte	.LASF22
	.byte	0x7
	.byte	0x57
	.4byte	0x114
	.uleb128 0xb
	.4byte	0x3e
	.4byte	0x14e
	.uleb128 0xc
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xd
	.4byte	0x65
	.uleb128 0xb
	.4byte	0x65
	.4byte	0x163
	.uleb128 0xc
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0x8
	.byte	0x1d
	.4byte	0x190
	.uleb128 0xf
	.4byte	.LASF23
	.sleb128 0
	.uleb128 0xf
	.4byte	.LASF24
	.sleb128 1
	.uleb128 0xf
	.4byte	.LASF25
	.sleb128 2
	.uleb128 0xf
	.4byte	.LASF26
	.sleb128 3
	.uleb128 0xf
	.4byte	.LASF27
	.sleb128 4
	.uleb128 0xf
	.4byte	.LASF28
	.sleb128 5
	.byte	0
	.uleb128 0xb
	.4byte	0x65
	.4byte	0x1a0
	.uleb128 0xc
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0x9
	.byte	0x24
	.4byte	0x281
	.uleb128 0xf
	.4byte	.LASF29
	.sleb128 0
	.uleb128 0xf
	.4byte	.LASF30
	.sleb128 0
	.uleb128 0xf
	.4byte	.LASF31
	.sleb128 1
	.uleb128 0xf
	.4byte	.LASF32
	.sleb128 2
	.uleb128 0xf
	.4byte	.LASF33
	.sleb128 3
	.uleb128 0xf
	.4byte	.LASF34
	.sleb128 4
	.uleb128 0xf
	.4byte	.LASF35
	.sleb128 5
	.uleb128 0xf
	.4byte	.LASF36
	.sleb128 6
	.uleb128 0xf
	.4byte	.LASF37
	.sleb128 7
	.uleb128 0xf
	.4byte	.LASF38
	.sleb128 8
	.uleb128 0xf
	.4byte	.LASF39
	.sleb128 9
	.uleb128 0xf
	.4byte	.LASF40
	.sleb128 10
	.uleb128 0xf
	.4byte	.LASF41
	.sleb128 11
	.uleb128 0xf
	.4byte	.LASF42
	.sleb128 12
	.uleb128 0xf
	.4byte	.LASF43
	.sleb128 13
	.uleb128 0xf
	.4byte	.LASF44
	.sleb128 14
	.uleb128 0xf
	.4byte	.LASF45
	.sleb128 15
	.uleb128 0xf
	.4byte	.LASF46
	.sleb128 16
	.uleb128 0xf
	.4byte	.LASF47
	.sleb128 17
	.uleb128 0xf
	.4byte	.LASF48
	.sleb128 18
	.uleb128 0xf
	.4byte	.LASF49
	.sleb128 19
	.uleb128 0xf
	.4byte	.LASF50
	.sleb128 20
	.uleb128 0xf
	.4byte	.LASF51
	.sleb128 21
	.uleb128 0xf
	.4byte	.LASF52
	.sleb128 22
	.uleb128 0xf
	.4byte	.LASF53
	.sleb128 23
	.uleb128 0xf
	.4byte	.LASF54
	.sleb128 24
	.uleb128 0xf
	.4byte	.LASF55
	.sleb128 25
	.uleb128 0xf
	.4byte	.LASF56
	.sleb128 26
	.uleb128 0xf
	.4byte	.LASF57
	.sleb128 27
	.uleb128 0xf
	.4byte	.LASF58
	.sleb128 28
	.uleb128 0xf
	.4byte	.LASF59
	.sleb128 29
	.uleb128 0xf
	.4byte	.LASF60
	.sleb128 30
	.uleb128 0xf
	.4byte	.LASF61
	.sleb128 31
	.uleb128 0xf
	.4byte	.LASF62
	.sleb128 32
	.uleb128 0xf
	.4byte	.LASF63
	.sleb128 33
	.uleb128 0xf
	.4byte	.LASF64
	.sleb128 34
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0x9
	.byte	0x4d
	.4byte	0x2cc
	.uleb128 0xf
	.4byte	.LASF65
	.sleb128 0
	.uleb128 0xf
	.4byte	.LASF66
	.sleb128 1
	.uleb128 0xf
	.4byte	.LASF67
	.sleb128 2
	.uleb128 0xf
	.4byte	.LASF68
	.sleb128 3
	.uleb128 0xf
	.4byte	.LASF69
	.sleb128 4
	.uleb128 0xf
	.4byte	.LASF70
	.sleb128 5
	.uleb128 0xf
	.4byte	.LASF71
	.sleb128 6
	.uleb128 0xf
	.4byte	.LASF72
	.sleb128 7
	.uleb128 0xf
	.4byte	.LASF73
	.sleb128 8
	.uleb128 0xf
	.4byte	.LASF74
	.sleb128 9
	.uleb128 0xf
	.4byte	.LASF75
	.sleb128 10
	.byte	0
	.uleb128 0x5
	.byte	0x6
	.byte	0xa
	.byte	0x22
	.4byte	0x2ed
	.uleb128 0x10
	.ascii	"T\000"
	.byte	0xa
	.byte	0x24
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.ascii	"D\000"
	.byte	0xa
	.byte	0x26
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF76
	.byte	0xa
	.byte	0x28
	.4byte	0x2cc
	.uleb128 0x5
	.byte	0x20
	.byte	0xb
	.byte	0x1e
	.4byte	0x371
	.uleb128 0x6
	.4byte	.LASF77
	.byte	0xb
	.byte	0x20
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF78
	.byte	0xb
	.byte	0x22
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF79
	.byte	0xb
	.byte	0x24
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF80
	.byte	0xb
	.byte	0x26
	.4byte	0xf7
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF81
	.byte	0xb
	.byte	0x28
	.4byte	0x371
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF82
	.byte	0xb
	.byte	0x2a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF83
	.byte	0xb
	.byte	0x2c
	.4byte	0x114
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF84
	.byte	0xb
	.byte	0x2e
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x11
	.4byte	0x376
	.uleb128 0x7
	.byte	0x4
	.4byte	0x37c
	.uleb128 0x11
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF85
	.byte	0xb
	.byte	0x30
	.4byte	0x2f8
	.uleb128 0xb
	.4byte	0x2c
	.4byte	0x39c
	.uleb128 0xc
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0xb
	.4byte	0x2c
	.4byte	0x3ac
	.uleb128 0xc
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF86
	.uleb128 0xb
	.4byte	0x65
	.4byte	0x3c3
	.uleb128 0xc
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x12
	.2byte	0x12c
	.byte	0xc
	.byte	0xb5
	.4byte	0x4b2
	.uleb128 0x6
	.4byte	.LASF87
	.byte	0xc
	.byte	0xbc
	.4byte	0x39c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF88
	.byte	0xc
	.byte	0xc1
	.4byte	0x38c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF89
	.byte	0xc
	.byte	0xcd
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x6
	.4byte	.LASF90
	.byte	0xc
	.byte	0xd5
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x6
	.4byte	.LASF91
	.byte	0xc
	.byte	0xd7
	.4byte	0x2ed
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x6
	.4byte	.LASF92
	.byte	0xc
	.byte	0xdb
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x6
	.4byte	.LASF93
	.byte	0xc
	.byte	0xe1
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x6
	.4byte	.LASF94
	.byte	0xc
	.byte	0xe3
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x6
	.4byte	.LASF95
	.byte	0xc
	.byte	0xea
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x6
	.4byte	.LASF96
	.byte	0xc
	.byte	0xec
	.4byte	0x2ed
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x6
	.4byte	.LASF97
	.byte	0xc
	.byte	0xee
	.4byte	0x38c
	.byte	0x2
	.byte	0x23
	.uleb128 0x66
	.uleb128 0x6
	.4byte	.LASF98
	.byte	0xc
	.byte	0xf0
	.4byte	0x38c
	.byte	0x3
	.byte	0x23
	.uleb128 0x96
	.uleb128 0x6
	.4byte	.LASF99
	.byte	0xc
	.byte	0xf5
	.4byte	0x8c
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0x6
	.4byte	.LASF100
	.byte	0xc
	.byte	0xf7
	.4byte	0x2ed
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x6
	.4byte	.LASF101
	.byte	0xc
	.byte	0xfa
	.4byte	0x4b2
	.byte	0x3
	.byte	0x23
	.uleb128 0xd2
	.uleb128 0x6
	.4byte	.LASF102
	.byte	0xc
	.byte	0xfe
	.4byte	0x4c2
	.byte	0x3
	.byte	0x23
	.uleb128 0xea
	.byte	0
	.uleb128 0xb
	.4byte	0x2c
	.4byte	0x4c2
	.uleb128 0xc
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0xb
	.4byte	0x2c
	.4byte	0x4d2
	.uleb128 0xc
	.4byte	0x25
	.byte	0x3f
	.byte	0
	.uleb128 0x13
	.4byte	.LASF103
	.byte	0xc
	.2byte	0x105
	.4byte	0x3c3
	.uleb128 0x14
	.byte	0x1c
	.byte	0xc
	.2byte	0x10c
	.4byte	0x551
	.uleb128 0x15
	.ascii	"rip\000"
	.byte	0xc
	.2byte	0x112
	.4byte	0xec
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF104
	.byte	0xc
	.2byte	0x11b
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF105
	.byte	0xc
	.2byte	0x122
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x16
	.4byte	.LASF106
	.byte	0xc
	.2byte	0x127
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x16
	.4byte	.LASF107
	.byte	0xc
	.2byte	0x138
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x16
	.4byte	.LASF108
	.byte	0xc
	.2byte	0x144
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x16
	.4byte	.LASF109
	.byte	0xc
	.2byte	0x14b
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x13
	.4byte	.LASF110
	.byte	0xc
	.2byte	0x14d
	.4byte	0x4de
	.uleb128 0x14
	.byte	0xec
	.byte	0xc
	.2byte	0x150
	.4byte	0x731
	.uleb128 0x16
	.4byte	.LASF87
	.byte	0xc
	.2byte	0x157
	.4byte	0x39c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF111
	.byte	0xc
	.2byte	0x162
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x16
	.4byte	.LASF112
	.byte	0xc
	.2byte	0x164
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x16
	.4byte	.LASF113
	.byte	0xc
	.2byte	0x166
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x16
	.4byte	.LASF114
	.byte	0xc
	.2byte	0x168
	.4byte	0x2ed
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x16
	.4byte	.LASF115
	.byte	0xc
	.2byte	0x16e
	.4byte	0xbc
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x16
	.4byte	.LASF116
	.byte	0xc
	.2byte	0x174
	.4byte	0x551
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x16
	.4byte	.LASF117
	.byte	0xc
	.2byte	0x17b
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x16
	.4byte	.LASF118
	.byte	0xc
	.2byte	0x17d
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x16
	.4byte	.LASF119
	.byte	0xc
	.2byte	0x185
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x16
	.4byte	.LASF120
	.byte	0xc
	.2byte	0x18d
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x16
	.4byte	.LASF121
	.byte	0xc
	.2byte	0x191
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x16
	.4byte	.LASF122
	.byte	0xc
	.2byte	0x195
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x16
	.4byte	.LASF123
	.byte	0xc
	.2byte	0x199
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x16
	.4byte	.LASF124
	.byte	0xc
	.2byte	0x19e
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x16
	.4byte	.LASF125
	.byte	0xc
	.2byte	0x1a2
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x16
	.4byte	.LASF126
	.byte	0xc
	.2byte	0x1a6
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x16
	.4byte	.LASF127
	.byte	0xc
	.2byte	0x1b4
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x16
	.4byte	.LASF128
	.byte	0xc
	.2byte	0x1ba
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x16
	.4byte	.LASF129
	.byte	0xc
	.2byte	0x1c2
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x16
	.4byte	.LASF130
	.byte	0xc
	.2byte	0x1c4
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x16
	.4byte	.LASF131
	.byte	0xc
	.2byte	0x1c6
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x16
	.4byte	.LASF132
	.byte	0xc
	.2byte	0x1d5
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x16
	.4byte	.LASF133
	.byte	0xc
	.2byte	0x1d7
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x81
	.uleb128 0x16
	.4byte	.LASF134
	.byte	0xc
	.2byte	0x1dd
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0x16
	.4byte	.LASF135
	.byte	0xc
	.2byte	0x1e7
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0x16
	.4byte	.LASF136
	.byte	0xc
	.2byte	0x1f0
	.4byte	0x65
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x16
	.4byte	.LASF137
	.byte	0xc
	.2byte	0x1f7
	.4byte	0x8c
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x16
	.4byte	.LASF138
	.byte	0xc
	.2byte	0x1f9
	.4byte	0x8c
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x16
	.4byte	.LASF139
	.byte	0xc
	.2byte	0x1fd
	.4byte	0x731
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0xb
	.4byte	0x65
	.4byte	0x741
	.uleb128 0xc
	.4byte	0x25
	.byte	0x16
	.byte	0
	.uleb128 0x13
	.4byte	.LASF140
	.byte	0xc
	.2byte	0x204
	.4byte	0x55d
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF141
	.uleb128 0x5
	.byte	0x20
	.byte	0xd
	.byte	0x28
	.4byte	0x7cd
	.uleb128 0x6
	.4byte	.LASF142
	.byte	0xd
	.byte	0x2a
	.4byte	0x14e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF143
	.byte	0xd
	.byte	0x2b
	.4byte	0x14e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF144
	.byte	0xd
	.byte	0x2c
	.4byte	0x14e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF145
	.byte	0xd
	.byte	0x2d
	.4byte	0x14e
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF146
	.byte	0xd
	.byte	0x2e
	.4byte	0x14e
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF147
	.byte	0xd
	.byte	0x2f
	.4byte	0x14e
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF148
	.byte	0xd
	.byte	0x30
	.4byte	0x14e
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF149
	.byte	0xd
	.byte	0x31
	.4byte	0x14e
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x3
	.4byte	.LASF150
	.byte	0xd
	.byte	0x32
	.4byte	0x754
	.uleb128 0x5
	.byte	0x4
	.byte	0x1
	.byte	0x25
	.4byte	0x7ef
	.uleb128 0x6
	.4byte	.LASF151
	.byte	0x1
	.byte	0x29
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF152
	.byte	0x1
	.byte	0x2b
	.4byte	0x7d8
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF199
	.byte	0x1
	.byte	0x38
	.byte	0x1
	.4byte	0x8c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x859
	.uleb128 0x18
	.4byte	.LASF156
	.byte	0x1
	.byte	0x38
	.4byte	0x859
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x3a
	.4byte	0x8c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1a
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x19
	.ascii	"wdt\000"
	.byte	0x1
	.byte	0x87
	.4byte	0x85e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.4byte	.LASF153
	.byte	0x1
	.byte	0x8b
	.4byte	0x8c
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x11
	.4byte	0x8c
	.uleb128 0x7
	.byte	0x4
	.4byte	0x7cd
	.uleb128 0x1c
	.4byte	.LASF200
	.byte	0x1
	.2byte	0x148
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x8b1
	.uleb128 0x1d
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x14a
	.4byte	0x7ef
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1d
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x15a
	.4byte	0x116
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1a
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF201
	.byte	0x1
	.2byte	0x166
	.byte	0x1
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF202
	.byte	0x1
	.2byte	0x174
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF158
	.byte	0x1
	.2byte	0x17f
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x8ff
	.uleb128 0x21
	.4byte	.LASF157
	.byte	0x1
	.2byte	0x17f
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1d
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x181
	.4byte	0x7ef
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF159
	.byte	0x1
	.2byte	0x18e
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x95e
	.uleb128 0x21
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x18e
	.4byte	0x114
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1d
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x196
	.4byte	0x7ef
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LBB4
	.4byte	.LBE4
	.uleb128 0x22
	.ascii	"wdt\000"
	.byte	0x1
	.2byte	0x1b1
	.4byte	0x85e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x22
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x1ff
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x26e
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0xa18
	.uleb128 0x21
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x26e
	.4byte	0x114
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x22
	.ascii	"wdt\000"
	.byte	0x1
	.2byte	0x277
	.4byte	0x85e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x22
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x2af
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1d
	.4byte	.LASF162
	.byte	0x1
	.2byte	0x2bf
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1d
	.4byte	.LASF163
	.byte	0x1
	.2byte	0x2c5
	.4byte	0x65
	.byte	0x5
	.byte	0x3
	.4byte	last_x_stamp.8135
	.uleb128 0x1d
	.4byte	.LASF164
	.byte	0x1
	.2byte	0x2c5
	.4byte	0x65
	.byte	0x5
	.byte	0x3
	.4byte	greatest_diff_ms.8136
	.uleb128 0x1d
	.4byte	.LASF165
	.byte	0x1
	.2byte	0x2c7
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1a
	.4byte	.LBB5
	.4byte	.LBE5
	.uleb128 0x1d
	.4byte	.LASF166
	.byte	0x1
	.2byte	0x30a
	.4byte	0x8c
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LBB6
	.4byte	.LBE6
	.uleb128 0x1d
	.4byte	.LASF167
	.byte	0x1
	.2byte	0x321
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF203
	.byte	0x1
	.2byte	0x3ff
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x406
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	0xa56
	.uleb128 0x1d
	.4byte	.LASF169
	.byte	0x1
	.2byte	0x408
	.4byte	0xa56
	.byte	0x1
	.byte	0x54
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x65
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF170
	.byte	0x1
	.2byte	0x431
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	0xa84
	.uleb128 0x1d
	.4byte	.LASF169
	.byte	0x1
	.2byte	0x433
	.4byte	0xa56
	.byte	0x1
	.byte	0x54
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF171
	.byte	0x1
	.2byte	0x45c
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	0xaac
	.uleb128 0x1d
	.4byte	.LASF169
	.byte	0x1
	.2byte	0x45e
	.4byte	0xa56
	.byte	0x1
	.byte	0x54
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF172
	.byte	0x1
	.2byte	0x48a
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST7
	.4byte	0xaf4
	.uleb128 0x21
	.4byte	.LASF173
	.byte	0x1
	.2byte	0x48a
	.4byte	0x376
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.4byte	.LASF174
	.byte	0x1
	.2byte	0x48a
	.4byte	0x376
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LASF175
	.byte	0x1
	.2byte	0x48a
	.4byte	0x77
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF176
	.byte	0xe
	.byte	0x30
	.4byte	0xb05
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x11
	.4byte	0x13e
	.uleb128 0x1b
	.4byte	.LASF177
	.byte	0xe
	.byte	0x34
	.4byte	0xb1b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x11
	.4byte	0x13e
	.uleb128 0x1b
	.4byte	.LASF178
	.byte	0xe
	.byte	0x36
	.4byte	0xb31
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x11
	.4byte	0x13e
	.uleb128 0x1b
	.4byte	.LASF179
	.byte	0xe
	.byte	0x38
	.4byte	0xb47
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x11
	.4byte	0x13e
	.uleb128 0xb
	.4byte	0x381
	.4byte	0xb5c
	.uleb128 0xc
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x25
	.4byte	.LASF180
	.byte	0xb
	.byte	0x38
	.4byte	0xb69
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	0xb4c
	.uleb128 0x25
	.4byte	.LASF181
	.byte	0xb
	.byte	0x48
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF182
	.byte	0xb
	.byte	0x48
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF183
	.byte	0xb
	.byte	0x48
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF184
	.byte	0xb
	.byte	0x4b
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF185
	.byte	0xb
	.byte	0x50
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF186
	.byte	0xb
	.byte	0x52
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	0x128
	.4byte	0xbcc
	.uleb128 0xc
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x25
	.4byte	.LASF187
	.byte	0xb
	.byte	0x55
	.4byte	0xbbc
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF188
	.byte	0xb
	.byte	0x58
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF189
	.byte	0xb
	.byte	0x58
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF190
	.byte	0xb
	.byte	0x5a
	.4byte	0x3b3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF191
	.byte	0xf
	.byte	0x33
	.4byte	0xc11
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x11
	.4byte	0x153
	.uleb128 0x1b
	.4byte	.LASF192
	.byte	0xf
	.byte	0x3f
	.4byte	0xc27
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x11
	.4byte	0x190
	.uleb128 0x26
	.4byte	.LASF193
	.byte	0xc
	.2byte	0x108
	.4byte	0x4d2
	.byte	0x1
	.byte	0x1
	.uleb128 0x26
	.4byte	.LASF194
	.byte	0xc
	.2byte	0x206
	.4byte	0x741
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	0x2c
	.4byte	0xc58
	.uleb128 0xc
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF195
	.byte	0x1
	.byte	0x30
	.4byte	0xc69
	.byte	0x5
	.byte	0x3
	.4byte	RESTART_INFO_VERIFY_STRING_PRE
	.uleb128 0x11
	.4byte	0xc48
	.uleb128 0x1d
	.4byte	.LASF196
	.byte	0x1
	.2byte	0x144
	.4byte	0x133
	.byte	0x5
	.byte	0x3
	.4byte	SYSTEM_shutdown_event_queue
	.uleb128 0x25
	.4byte	.LASF180
	.byte	0xb
	.byte	0x38
	.4byte	0xc8d
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	0xb4c
	.uleb128 0x25
	.4byte	.LASF181
	.byte	0xb
	.byte	0x48
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF182
	.byte	0xb
	.byte	0x48
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF183
	.byte	0xb
	.byte	0x48
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF184
	.byte	0xb
	.byte	0x4b
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF185
	.byte	0xb
	.byte	0x50
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF186
	.byte	0xb
	.byte	0x52
	.4byte	0x128
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF187
	.byte	0xb
	.byte	0x55
	.4byte	0xbbc
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF188
	.byte	0xb
	.byte	0x58
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF189
	.byte	0xb
	.byte	0x58
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF190
	.byte	0xb
	.byte	0x5a
	.4byte	0x3b3
	.byte	0x1
	.byte	0x1
	.uleb128 0x26
	.4byte	.LASF193
	.byte	0xc
	.2byte	0x108
	.4byte	0x4d2
	.byte	0x1
	.byte	0x1
	.uleb128 0x26
	.4byte	.LASF194
	.byte	0xc
	.2byte	0x206
	.4byte	0x741
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB10
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI21
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF193:
	.ascii	"restart_info\000"
.LASF154:
	.ascii	"sseqs\000"
.LASF130:
	.ascii	"commserver_monthly_xmit_bytes\000"
.LASF135:
	.ascii	"ununsed_uns8_1\000"
.LASF147:
	.ascii	"external_match_control\000"
.LASF51:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF189:
	.ascii	"pwrfail_task_last_x_stamp\000"
.LASF109:
	.ascii	"needs_to_be_broadcast\000"
.LASF46:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF178:
	.ascii	"GuiFont_DecimalChar\000"
.LASF85:
	.ascii	"TASK_ENTRY_STRUCT\000"
.LASF137:
	.ascii	"hub_needs_to_distribute_tpmicro_binary\000"
.LASF122:
	.ascii	"yesterdays_ET_was_zero\000"
.LASF50:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF175:
	.ascii	"__line\000"
.LASF58:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF19:
	.ascii	"long int\000"
.LASF31:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF26:
	.ascii	"ISR_TRIGGER_NEGATIVE_EDGE\000"
.LASF181:
	.ascii	"epson_rtc_task_handle\000"
.LASF162:
	.ascii	"unblock_count\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF160:
	.ascii	"pvParameters\000"
.LASF89:
	.ascii	"controller_index\000"
.LASF67:
	.ascii	"CLKPWR_SYSCLK\000"
.LASF111:
	.ascii	"dls_saved_date\000"
.LASF108:
	.ascii	"inhibit_irrigation_have_crossed_the_minimum\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF140:
	.ascii	"WEATHER_PRESERVES_STRUCT\000"
.LASF76:
	.ascii	"DATE_TIME\000"
.LASF20:
	.ascii	"portTickType\000"
.LASF3:
	.ascii	"signed char\000"
.LASF29:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF21:
	.ascii	"xTaskHandle\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF117:
	.ascii	"sync_the_et_rain_tables\000"
.LASF158:
	.ascii	"SYSTEM_application_requested_restart\000"
.LASF59:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF157:
	.ascii	"preason\000"
.LASF132:
	.ascii	"factory_reset_do_not_allow_pdata_to_be_sent\000"
.LASF164:
	.ascii	"greatest_diff_ms\000"
.LASF134:
	.ascii	"perform_a_discovery_following_a_FACTORY_reset\000"
.LASF114:
	.ascii	"dls_after_fall_when_to_clear_ignore_sxs\000"
.LASF55:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF62:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF42:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF187:
	.ascii	"created_task_handles\000"
.LASF201:
	.ascii	"vTaskSwitchContext\000"
.LASF68:
	.ascii	"CLKPWR_ARM_CLK\000"
.LASF11:
	.ascii	"long long int\000"
.LASF25:
	.ascii	"ISR_TRIGGER_HIGH_LEVEL\000"
.LASF183:
	.ascii	"wdt_task_handle\000"
.LASF80:
	.ascii	"pTaskFunc\000"
.LASF171:
	.ascii	"data_abort_handler\000"
.LASF169:
	.ascii	"lnk_ptr\000"
.LASF203:
	.ascii	"freeze_and_restart_app\000"
.LASF168:
	.ascii	"undefined_instruction_handler\000"
.LASF152:
	.ascii	"SYSTEM_SHUTDOWN_EVENT_QUEUE_STRUCT\000"
.LASF179:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF92:
	.ascii	"shutdown_reason\000"
.LASF195:
	.ascii	"RESTART_INFO_VERIFY_STRING_PRE\000"
.LASF182:
	.ascii	"powerfail_task_handle\000"
.LASF107:
	.ascii	"rain_shutdown_rcvd_from_commserver_uns32\000"
.LASF148:
	.ascii	"reset_pulse_length\000"
.LASF100:
	.ascii	"assert_td\000"
.LASF139:
	.ascii	"expansion\000"
.LASF34:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF159:
	.ascii	"system_shutdown_task\000"
.LASF106:
	.ascii	"midnight_to_midnight_raw_pulse_count\000"
.LASF48:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF39:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF91:
	.ascii	"shutdown_td\000"
.LASF57:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF77:
	.ascii	"bCreateTask\000"
.LASF97:
	.ascii	"exception_current_task\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF113:
	.ascii	"dls_after_fall_back_ignore_sxs\000"
.LASF124:
	.ascii	"clear_runaway_gage\000"
.LASF142:
	.ascii	"interrupt_status\000"
.LASF174:
	.ascii	"__filename\000"
.LASF118:
	.ascii	"nlu_old_rain_table_sync_variable\000"
.LASF119:
	.ascii	"et_table_update_all_historical_values\000"
.LASF96:
	.ascii	"exception_td\000"
.LASF151:
	.ascii	"event\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF197:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF131:
	.ascii	"commserver_monthly_code_receipt_bytes\000"
.LASF23:
	.ascii	"ISR_TRIGGER_FIXED\000"
.LASF170:
	.ascii	"prefetch_abort_handler\000"
.LASF196:
	.ascii	"SYSTEM_shutdown_event_queue\000"
.LASF173:
	.ascii	"__expression\000"
.LASF153:
	.ascii	"watchdog_initiated_reset\000"
.LASF84:
	.ascii	"priority\000"
.LASF75:
	.ascii	"CLKPWR_BASE_INVALID\000"
.LASF64:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF66:
	.ascii	"CLKPWR_RTC_CLK\000"
.LASF145:
	.ascii	"match_control\000"
.LASF138:
	.ascii	"hub_needs_to_distribute_main_binary\000"
.LASF44:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF30:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF22:
	.ascii	"xQueueHandle\000"
.LASF79:
	.ascii	"execution_limit_ms\000"
.LASF177:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF33:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF90:
	.ascii	"SHUTDOWN_impending\000"
.LASF40:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF188:
	.ascii	"rtc_task_last_x_stamp\000"
.LASF126:
	.ascii	"freeze_switch_active\000"
.LASF180:
	.ascii	"Task_Table\000"
.LASF81:
	.ascii	"TaskName\000"
.LASF32:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF60:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF199:
	.ascii	"init_restart_info\000"
.LASF70:
	.ascii	"CLKPWR_PERIPH_CLK\000"
.LASF128:
	.ascii	"pending_changes_to_send_to_comm_server\000"
.LASF165:
	.ascii	"now_stamp\000"
.LASF98:
	.ascii	"exception_description_string\000"
.LASF103:
	.ascii	"RESTART_INFORMATION_STRUCT\000"
.LASF71:
	.ascii	"CLKPWR_USB_HCLK_SYS\000"
.LASF72:
	.ascii	"CLKPWR_48M_CLK\000"
.LASF202:
	.ascii	"init_powerfail_isr\000"
.LASF52:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF27:
	.ascii	"ISR_TRIGGER_POSITIVE_EDGE\000"
.LASF149:
	.ascii	"reset_source\000"
.LASF35:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF86:
	.ascii	"float\000"
.LASF176:
	.ascii	"GuiFont_LanguageActive\000"
.LASF133:
	.ascii	"factory_reset_registration_to_tell_commserver_to_cl"
	.ascii	"ear_pdata\000"
.LASF136:
	.ascii	"commserver_monthly_mobile_status_updates_bytes\000"
.LASF94:
	.ascii	"we_were_reading_spi_flash_data\000"
.LASF28:
	.ascii	"ISR_TRIGGER_DUAL_EDGE\000"
.LASF129:
	.ascii	"commserver_monthly_rcvd_bytes\000"
.LASF105:
	.ascii	"roll_to_roll_raw_pulse_count\000"
.LASF74:
	.ascii	"CLKPWR_MSSD_CLK\000"
.LASF24:
	.ascii	"ISR_TRIGGER_LOW_LEVEL\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF88:
	.ascii	"main_app_code_revision_string\000"
.LASF7:
	.ascii	"short int\000"
.LASF155:
	.ascii	"higher_priority_woken\000"
.LASF17:
	.ascii	"RAIN_TABLE_ENTRY\000"
.LASF78:
	.ascii	"include_in_wdt\000"
.LASF186:
	.ascii	"flash_storage_1_task_handle\000"
.LASF101:
	.ascii	"assert_current_task\000"
.LASF49:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF95:
	.ascii	"exception_noted\000"
.LASF166:
	.ascii	"care_for_the_dog\000"
.LASF116:
	.ascii	"rain\000"
.LASF184:
	.ascii	"timer_task_handle\000"
.LASF56:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF45:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF93:
	.ascii	"we_were_writing_spi_flash_data\000"
.LASF38:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF18:
	.ascii	"pdTASK_CODE\000"
.LASF82:
	.ascii	"stack_depth\000"
.LASF1:
	.ascii	"char\000"
.LASF194:
	.ascii	"weather_preserves\000"
.LASF127:
	.ascii	"write_tpmicro_file_in_new_file_format\000"
.LASF121:
	.ascii	"run_away_gage\000"
.LASF120:
	.ascii	"dont_use_et_gage_today\000"
.LASF65:
	.ascii	"CLKPWR_MAINOSC_CLK\000"
.LASF125:
	.ascii	"rain_switch_active\000"
.LASF69:
	.ascii	"CLKPWR_HCLK\000"
.LASF143:
	.ascii	"main_control\000"
.LASF37:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF13:
	.ascii	"et_inches_u16_10000u\000"
.LASF161:
	.ascii	"wdt_monitoring_task\000"
.LASF104:
	.ascii	"hourly_total_inches_100u\000"
.LASF36:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF83:
	.ascii	"parameter\000"
.LASF54:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF63:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF185:
	.ascii	"flash_storage_0_task_handle\000"
.LASF192:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF41:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF172:
	.ascii	"__assert\000"
.LASF102:
	.ascii	"assert_description_string\000"
.LASF200:
	.ascii	"powerfail_isr\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF14:
	.ascii	"status\000"
.LASF123:
	.ascii	"remaining_gage_pulses\000"
.LASF61:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF4:
	.ascii	"UNS_8\000"
.LASF110:
	.ascii	"RAIN_STATE\000"
.LASF73:
	.ascii	"CLKPWR_DDR_CLK\000"
.LASF146:
	.ascii	"match_value\000"
.LASF99:
	.ascii	"assert_noted\000"
.LASF115:
	.ascii	"et_rip\000"
.LASF43:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF112:
	.ascii	"dls_eligible_to_fall_back\000"
.LASF150:
	.ascii	"WDT_REGS_T\000"
.LASF16:
	.ascii	"rain_inches_u16_100u\000"
.LASF15:
	.ascii	"ET_TABLE_ENTRY\000"
.LASF53:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF167:
	.ascii	"task_x_stamp\000"
.LASF198:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_"
	.ascii	"and_powerfail/wdt_and_powerfail.c\000"
.LASF87:
	.ascii	"verify_string_pre\000"
.LASF191:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF144:
	.ascii	"counter_value\000"
.LASF163:
	.ascii	"last_x_stamp\000"
.LASF141:
	.ascii	"double\000"
.LASF190:
	.ascii	"task_last_execution_stamp\000"
.LASF47:
	.ascii	"CLKPWR_WDOG_CLK\000"
.LASF156:
	.ascii	"pforce_the_init\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
