	.file	"d_comm_options.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.global	g_COMM_OPTIONS_cursor_position_when_dialog_displayed
	.section	.bss.g_COMM_OPTIONS_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.type	g_COMM_OPTIONS_cursor_position_when_dialog_displayed, %object
	.size	g_COMM_OPTIONS_cursor_position_when_dialog_displayed, 4
g_COMM_OPTIONS_cursor_position_when_dialog_displayed:
	.space	4
	.global	g_COMM_OPTIONS_dialog_visible
	.section	.bss.g_COMM_OPTIONS_dialog_visible,"aw",%nobits
	.align	2
	.type	g_COMM_OPTIONS_dialog_visible, %object
	.size	g_COMM_OPTIONS_dialog_visible, 4
g_COMM_OPTIONS_dialog_visible:
	.space	4
	.section	.bss.g_COMM_OPTIONS_last_cursor_position,"aw",%nobits
	.align	2
	.type	g_COMM_OPTIONS_last_cursor_position, %object
	.size	g_COMM_OPTIONS_last_cursor_position, 4
g_COMM_OPTIONS_last_cursor_position:
	.space	4
	.section	.text.FDTO_COMM_OPTIONS_draw_dialog,"ax",%progbits
	.align	2
	.type	FDTO_COMM_OPTIONS_draw_dialog, %function
FDTO_COMM_OPTIONS_draw_dialog:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_comm_options.c"
	.loc 1 59 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-12]
	.loc 1 60 0
	ldr	r3, .L15
	ldr	r2, [r3, #80]
	ldr	r3, .L15+4
	str	r2, [r3, #0]
	.loc 1 61 0
	ldr	r3, .L15
	ldr	r2, [r3, #84]
	ldr	r3, .L15+8
	str	r2, [r3, #0]
	.loc 1 70 0
	ldr	r3, .L15+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L2
	.loc 1 71 0 discriminator 1
	ldr	r3, .L15+4
	ldr	r3, [r3, #0]
	.loc 1 70 0 discriminator 1
	cmp	r3, #5
	beq	.L2
	.loc 1 72 0
	ldr	r3, .L15+4
	ldr	r3, [r3, #0]
	.loc 1 71 0
	cmp	r3, #7
	beq	.L2
	.loc 1 73 0
	ldr	r3, .L15+4
	ldr	r3, [r3, #0]
	.loc 1 72 0
	cmp	r3, #10
	beq	.L2
	.loc 1 70 0
	mov	r3, #1
	b	.L3
.L2:
	.loc 1 70 0 is_stmt 0 discriminator 2
	mov	r3, #0
.L3:
	.loc 1 70 0 discriminator 3
	mov	r2, r3
	ldr	r3, .L15+12
	str	r2, [r3, #0]
	.loc 1 76 0 is_stmt 1 discriminator 3
	ldr	r3, .L15+8
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L4
	.loc 1 77 0 discriminator 2
	ldr	r3, .L15+8
	ldr	r3, [r3, #0]
	.loc 1 76 0 discriminator 2
	cmp	r3, #1
	beq	.L4
	.loc 1 78 0
	ldr	r3, .L15+8
	ldr	r3, [r3, #0]
	.loc 1 77 0
	cmp	r3, #3
	bne	.L5
.L4:
	.loc 1 76 0 discriminator 1
	mov	r3, #1
	b	.L6
.L5:
	.loc 1 76 0 is_stmt 0
	mov	r3, #0
.L6:
	.loc 1 76 0 discriminator 3
	mov	r2, r3
	ldr	r3, .L15+16
	str	r2, [r3, #0]
	.loc 1 84 0 is_stmt 1 discriminator 3
	ldr	r3, .L15
	ldr	r2, [r3, #80]
	ldr	r1, .L15+20
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #3
	add	r3, r1, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L15+24
	str	r2, [r3, #0]
	.loc 1 88 0 discriminator 3
	bl	RADIO_TEST_there_is_a_port_device_to_test
	mov	r2, r0
	ldr	r3, .L15+28
	str	r2, [r3, #0]
	.loc 1 92 0 discriminator 3
	ldr	r3, .L15+32
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L15+36
	str	r2, [r3, #0]
	.loc 1 98 0 discriminator 3
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L7
	.loc 1 102 0
	ldr	r3, .L15+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L8
	.loc 1 104 0
	ldr	r3, .L15+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L9
	.loc 1 106 0
	ldr	r3, .L15+40
	mov	r2, #50
	str	r2, [r3, #0]
	b	.L8
.L9:
	.loc 1 108 0
	ldr	r3, .L15+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L10
	.loc 1 110 0
	ldr	r3, .L15+40
	mov	r2, #51
	str	r2, [r3, #0]
	b	.L8
.L10:
	.loc 1 112 0
	ldr	r3, .L15+12
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L11
	.loc 1 114 0
	ldr	r3, .L15+40
	mov	r2, #52
	str	r2, [r3, #0]
	b	.L8
.L11:
	.loc 1 116 0
	ldr	r3, .L15+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L12
	.loc 1 118 0
	ldr	r3, .L15+40
	mov	r2, #53
	str	r2, [r3, #0]
	b	.L8
.L12:
	.loc 1 120 0
	ldr	r3, .L15+44
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L13
	.loc 1 122 0
	ldr	r3, .L15+40
	mov	r2, #54
	str	r2, [r3, #0]
	b	.L8
.L13:
	.loc 1 126 0
	ldr	r3, .L15+40
	mov	r2, #55
	str	r2, [r3, #0]
.L8:
	.loc 1 130 0
	ldr	r3, .L15+40
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L14
.L7:
	.loc 1 134 0
	ldr	r3, .L15+32
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L14:
	.loc 1 137 0
	ldr	r3, .L15+48
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 139 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r0, .L15+52
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 140 0
	bl	GuiLib_Refresh
	.loc 1 141 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L16:
	.align	2
.L15:
	.word	config_c
	.word	GuiVar_CommOptionPortAIndex
	.word	GuiVar_CommOptionPortBIndex
	.word	GuiVar_CommOptionPortAInstalled
	.word	GuiVar_CommOptionPortBInstalled
	.word	port_device_table
	.word	GuiVar_CommOptionCloudCommTestAvailable
	.word	GuiVar_CommOptionRadioTestAvailable
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_COMM_OPTIONS_cursor_position_when_dialog_displayed
	.word	g_COMM_OPTIONS_last_cursor_position
	.word	GuiVar_IsAHub
	.word	g_COMM_OPTIONS_dialog_visible
	.word	594
.LFE0:
	.size	FDTO_COMM_OPTIONS_draw_dialog, .-FDTO_COMM_OPTIONS_draw_dialog
	.section	.text.COMM_OPTIONS_draw_dialog,"ax",%progbits
	.align	2
	.global	COMM_OPTIONS_draw_dialog
	.type	COMM_OPTIONS_draw_dialog, %function
COMM_OPTIONS_draw_dialog:
.LFB1:
	.loc 1 145 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #40
.LCFI5:
	str	r0, [fp, #-44]
	.loc 1 152 0
	ldr	r3, .L19
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L18
	.loc 1 154 0
	bl	task_control_EXIT_device_exchange_hammer
.L18:
	.loc 1 159 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 160 0
	ldr	r3, .L19+4
	str	r3, [fp, #-20]
	.loc 1 161 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 162 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 163 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L20:
	.align	2
.L19:
	.word	in_device_exchange_hammer
	.word	FDTO_COMM_OPTIONS_draw_dialog
.LFE1:
	.size	COMM_OPTIONS_draw_dialog, .-COMM_OPTIONS_draw_dialog
	.section	.text.COMM_OPTIONS_process_dialog,"ax",%progbits
	.align	2
	.global	COMM_OPTIONS_process_dialog
	.type	COMM_OPTIONS_process_dialog, %function
COMM_OPTIONS_process_dialog:
.LFB2:
	.loc 1 167 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #44
.LCFI8:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	.loc 1 171 0
	ldr	r3, [fp, #-48]
	cmp	r3, #4
	beq	.L25
	cmp	r3, #4
	bhi	.L28
	cmp	r3, #0
	beq	.L23
	cmp	r3, #2
	beq	.L24
	b	.L22
.L28:
	cmp	r3, #67
	beq	.L26
	cmp	r3, #81
	bne	.L22
.L27:
	.loc 1 174 0
	ldr	r3, .L59
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #50
	bne	.L57
.L30:
	.loc 1 179 0
	ldr	r3, .L59
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L59+4
	str	r2, [r3, #0]
	.loc 1 181 0
	ldr	r3, .L59+8
	mov	r2, #50
	str	r2, [r3, #0]
	.loc 1 183 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 184 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 185 0
	mov	r3, #13
	str	r3, [fp, #-32]
	.loc 1 186 0
	ldr	r3, .L59+12
	str	r3, [fp, #-20]
	.loc 1 187 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 188 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-12]
	.loc 1 189 0
	ldr	r3, .L59+16
	str	r3, [fp, #-24]
	.loc 1 190 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 192 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 194 0
	mov	r0, r0	@ nop
	.loc 1 199 0
	b	.L21
.L57:
	.loc 1 197 0
	bl	bad_key_beep
	.loc 1 199 0
	b	.L21
.L24:
	.loc 1 204 0
	ldr	r3, .L59
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L59+4
	str	r2, [r3, #0]
	.loc 1 206 0
	ldr	r3, .L59
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #50
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L33
.L41:
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
.L34:
	.loc 1 209 0
	ldr	r3, .L59+8
	mov	r2, #50
	str	r2, [r3, #0]
	.loc 1 211 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 212 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 213 0
	mov	r3, #13
	str	r3, [fp, #-32]
	.loc 1 214 0
	ldr	r3, .L59+12
	str	r3, [fp, #-20]
	.loc 1 215 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 216 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-12]
	.loc 1 217 0
	ldr	r3, .L59+16
	str	r3, [fp, #-24]
	.loc 1 218 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 220 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 222 0
	b	.L42
.L35:
	.loc 1 225 0
	ldr	r3, .L59+8
	mov	r2, #51
	str	r2, [r3, #0]
	.loc 1 227 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 228 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 229 0
	mov	r3, #51
	str	r3, [fp, #-32]
	.loc 1 230 0
	ldr	r3, .L59+24
	str	r3, [fp, #-20]
	.loc 1 231 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 232 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 233 0
	ldr	r3, .L59+28
	str	r3, [fp, #-24]
	.loc 1 234 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 236 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 238 0
	b	.L42
.L36:
	.loc 1 242 0
	ldr	r3, .L59+32
	ldr	r3, [r3, #80]
	sub	r3, r3, #1
	cmp	r3, #8
	ldrls	pc, [pc, r3, asl #2]
	b	.L43
.L50:
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L43
	.word	.L48
	.word	.L43
	.word	.L43
	.word	.L49
.L47:
	.loc 1 245 0
	bl	task_control_ENTER_device_exchange_hammer
	.loc 1 247 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 248 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 249 0
	mov	r3, #17
	str	r3, [fp, #-32]
	.loc 1 250 0
	ldr	r3, .L59+36
	str	r3, [fp, #-20]
	.loc 1 251 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 252 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 253 0
	ldr	r3, .L59+40
	str	r3, [fp, #-24]
	.loc 1 254 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 256 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 258 0
	b	.L51
.L49:
	.loc 1 261 0
	bl	task_control_ENTER_device_exchange_hammer
	.loc 1 263 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 264 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 265 0
	mov	r3, #68
	str	r3, [fp, #-32]
	.loc 1 266 0
	ldr	r3, .L59+44
	str	r3, [fp, #-20]
	.loc 1 267 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 268 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 269 0
	ldr	r3, .L59+48
	str	r3, [fp, #-24]
	.loc 1 270 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 272 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 273 0
	b	.L51
.L48:
	.loc 1 276 0
	bl	task_control_ENTER_device_exchange_hammer
	.loc 1 278 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 279 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 280 0
	mov	r3, #68
	str	r3, [fp, #-32]
	.loc 1 281 0
	ldr	r3, .L59+44
	str	r3, [fp, #-20]
	.loc 1 282 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 283 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 284 0
	ldr	r3, .L59+48
	str	r3, [fp, #-24]
	.loc 1 285 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 287 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 288 0
	b	.L51
.L45:
	.loc 1 291 0
	bl	task_control_ENTER_device_exchange_hammer
	.loc 1 293 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 294 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 295 0
	mov	r3, #29
	str	r3, [fp, #-32]
	.loc 1 296 0
	ldr	r3, .L59+52
	str	r3, [fp, #-20]
	.loc 1 297 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 298 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 299 0
	ldr	r3, .L59+56
	str	r3, [fp, #-24]
	.loc 1 300 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 302 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 303 0
	b	.L51
.L44:
	.loc 1 306 0
	bl	task_control_ENTER_device_exchange_hammer
	.loc 1 308 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 309 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 310 0
	mov	r3, #30
	str	r3, [fp, #-32]
	.loc 1 311 0
	ldr	r3, .L59+60
	str	r3, [fp, #-20]
	.loc 1 312 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 313 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 315 0
	ldr	r3, .L59+64
	str	r3, [fp, #-24]
	.loc 1 316 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 318 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 319 0
	b	.L51
.L46:
	.loc 1 322 0
	bl	task_control_ENTER_device_exchange_hammer
	.loc 1 325 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 326 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 327 0
	mov	r3, #53
	str	r3, [fp, #-32]
	.loc 1 328 0
	ldr	r3, .L59+68
	str	r3, [fp, #-20]
	.loc 1 329 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 330 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 331 0
	ldr	r3, .L59+72
	str	r3, [fp, #-24]
	.loc 1 332 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 334 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 335 0
	b	.L51
.L43:
	.loc 1 340 0
	bl	bad_key_beep
	.loc 1 346 0
	bl	task_control_EXIT_device_exchange_hammer
	.loc 1 349 0
	b	.L42
.L51:
	b	.L42
.L37:
	.loc 1 353 0
	ldr	r3, .L59+32
	ldr	r3, [r3, #84]
	cmp	r3, #2
	beq	.L54
	cmp	r3, #3
	beq	.L55
	cmp	r3, #1
	beq	.L53
	b	.L58
.L54:
	.loc 1 356 0
	bl	task_control_ENTER_device_exchange_hammer
	.loc 1 358 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 359 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 360 0
	mov	r3, #29
	str	r3, [fp, #-32]
	.loc 1 361 0
	ldr	r3, .L59+52
	str	r3, [fp, #-20]
	.loc 1 362 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 363 0
	mov	r3, #2
	str	r3, [fp, #-12]
	.loc 1 365 0
	ldr	r3, .L59+56
	str	r3, [fp, #-24]
	.loc 1 366 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 368 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 369 0
	b	.L56
.L53:
	.loc 1 372 0
	bl	task_control_ENTER_device_exchange_hammer
	.loc 1 374 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 375 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 376 0
	mov	r3, #30
	str	r3, [fp, #-32]
	.loc 1 377 0
	ldr	r3, .L59+60
	str	r3, [fp, #-20]
	.loc 1 378 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 379 0
	mov	r3, #2
	str	r3, [fp, #-12]
	.loc 1 380 0
	ldr	r3, .L59+64
	str	r3, [fp, #-24]
	.loc 1 381 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 383 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 384 0
	b	.L56
.L55:
	.loc 1 387 0
	bl	task_control_ENTER_device_exchange_hammer
	.loc 1 390 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 391 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 392 0
	mov	r3, #53
	str	r3, [fp, #-32]
	.loc 1 393 0
	ldr	r3, .L59+68
	str	r3, [fp, #-20]
	.loc 1 394 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 395 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 396 0
	ldr	r3, .L59+72
	str	r3, [fp, #-24]
	.loc 1 397 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 399 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 400 0
	b	.L56
.L58:
	.loc 1 405 0
	bl	bad_key_beep
	.loc 1 411 0
	bl	task_control_EXIT_device_exchange_hammer
	.loc 1 415 0
	b	.L42
.L56:
	b	.L42
.L38:
	.loc 1 418 0
	ldr	r3, .L59+8
	mov	r2, #54
	str	r2, [r3, #0]
	.loc 1 420 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 421 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 422 0
	mov	r3, #28
	str	r3, [fp, #-32]
	.loc 1 423 0
	ldr	r3, .L59+76
	str	r3, [fp, #-20]
	.loc 1 424 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 425 0
	ldr	r3, .L59+80
	str	r3, [fp, #-24]
	.loc 1 426 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 428 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 430 0
	b	.L42
.L39:
	.loc 1 433 0
	ldr	r3, .L59+8
	mov	r2, #55
	str	r2, [r3, #0]
	.loc 1 435 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 436 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 437 0
	mov	r3, #99
	str	r3, [fp, #-32]
	.loc 1 438 0
	ldr	r3, .L59+84
	str	r3, [fp, #-20]
	.loc 1 439 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 440 0
	ldr	r3, .L59+88
	str	r3, [fp, #-24]
	.loc 1 441 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 443 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 445 0
	b	.L42
.L40:
	.loc 1 448 0
	ldr	r3, .L59+8
	mov	r2, #56
	str	r2, [r3, #0]
	.loc 1 450 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 451 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 452 0
	mov	r3, #3
	str	r3, [fp, #-32]
	.loc 1 453 0
	ldr	r3, .L59+92
	str	r3, [fp, #-20]
	.loc 1 454 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 455 0
	ldr	r3, .L59+96
	str	r3, [fp, #-24]
	.loc 1 456 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 458 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 460 0
	b	.L42
.L33:
	.loc 1 463 0
	bl	bad_key_beep
	.loc 1 465 0
	b	.L21
.L42:
	b	.L21
.L25:
	.loc 1 468 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 469 0
	b	.L21
.L23:
	.loc 1 472 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 473 0
	b	.L21
.L26:
	.loc 1 476 0
	bl	good_key_beep
	.loc 1 480 0
	ldr	r3, .L59
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L59+4
	str	r2, [r3, #0]
	.loc 1 482 0
	ldr	r3, .L59+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 487 0
	ldr	r3, .L59+100
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L59
	strh	r2, [r3, #0]	@ movhi
	.loc 1 489 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 490 0
	b	.L21
.L22:
	.loc 1 493 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L21:
	.loc 1 495 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L60:
	.align	2
.L59:
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_COMM_OPTIONS_last_cursor_position
	.word	GuiVar_MenuScreenToShow
	.word	FDTO_COMM_TEST_draw_screen
	.word	COMM_TEST_process_screen
	.word	g_COMM_OPTIONS_dialog_visible
	.word	FDTO_RADIO_TEST_draw_radio_comm_test_screen
	.word	RADIO_TEST_process_radio_comm_test_screen
	.word	config_c
	.word	FDTO_EN_PROGRAMMING_draw_screen
	.word	EN_PROGRAMMING_process_screen
	.word	FDTO_WEN_PROGRAMMING_draw_screen
	.word	WEN_PROGRAMMING_process_screen
	.word	FDTO_LR_PROGRAMMING_lrs_draw_screen
	.word	LR_PROGRAMMING_lrs_process_screen
	.word	FDTO_LR_PROGRAMMING_raveon_draw_screen
	.word	LR_PROGRAMMING_raveon_process_screen
	.word	FDTO_SR_PROGRAMMING_draw_screen
	.word	SR_PROGRAMMING_process_screen
	.word	FDTO_HUB_draw_screen
	.word	HUB_process_screen
	.word	FDTO_SERIAL_PORT_INFO_draw_report
	.word	SERIAL_PORT_INFO_process_report
	.word	FDTO_ACTIVATE_OPTIONS_draw_screen
	.word	ACTIVATE_OPTIONS_process_screen
	.word	g_COMM_OPTIONS_cursor_position_when_dialog_displayed
.LFE2:
	.size	COMM_OPTIONS_process_dialog, .-COMM_OPTIONS_process_dialog
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/configuration_controller.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_comm_options.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/comm_mngr.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x95d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF114
	.byte	0x1
	.4byte	.LASF115
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x4
	.byte	0x4
	.4byte	0x40
	.uleb128 0x5
	.byte	0x1
	.4byte	0x4c
	.uleb128 0x6
	.4byte	0x4c
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x8
	.4byte	0x63
	.4byte	0x88
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF8
	.uleb128 0xa
	.4byte	.LASF9
	.byte	0x2
	.byte	0x4c
	.4byte	0x33
	.uleb128 0xa
	.4byte	.LASF10
	.byte	0x2
	.byte	0x55
	.4byte	0x55
	.uleb128 0xa
	.4byte	.LASF11
	.byte	0x2
	.byte	0x5e
	.4byte	0xb0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0xa
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0xb0
	.uleb128 0xa
	.4byte	.LASF14
	.byte	0x2
	.byte	0x9d
	.4byte	0xb0
	.uleb128 0x4
	.byte	0x4
	.4byte	0xd3
	.uleb128 0xb
	.4byte	0xda
	.uleb128 0xc
	.byte	0
	.uleb128 0xd
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xff
	.uleb128 0xe
	.4byte	.LASF15
	.byte	0x3
	.byte	0x7e
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF16
	.byte	0x3
	.byte	0x80
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xa
	.4byte	.LASF17
	.byte	0x3
	.byte	0x82
	.4byte	0xda
	.uleb128 0xd
	.byte	0x4
	.byte	0x4
	.byte	0x2f
	.4byte	0x201
	.uleb128 0xf
	.4byte	.LASF18
	.byte	0x4
	.byte	0x35
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF19
	.byte	0x4
	.byte	0x3e
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF20
	.byte	0x4
	.byte	0x3f
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF21
	.byte	0x4
	.byte	0x46
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF22
	.byte	0x4
	.byte	0x4e
	.4byte	0xa5
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF23
	.byte	0x4
	.byte	0x4f
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x4
	.byte	0x50
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF25
	.byte	0x4
	.byte	0x52
	.4byte	0xa5
	.byte	0x4
	.byte	0x2
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF26
	.byte	0x4
	.byte	0x53
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF27
	.byte	0x4
	.byte	0x54
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x4
	.byte	0x58
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF29
	.byte	0x4
	.byte	0x59
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x4
	.byte	0x5a
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x4
	.byte	0x5b
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0x4
	.byte	0x2b
	.4byte	0x21a
	.uleb128 0x11
	.4byte	.LASF37
	.byte	0x4
	.byte	0x2d
	.4byte	0x8f
	.uleb128 0x12
	.4byte	0x10a
	.byte	0
	.uleb128 0xd
	.byte	0x4
	.byte	0x4
	.byte	0x29
	.4byte	0x22b
	.uleb128 0x13
	.4byte	0x201
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xa
	.4byte	.LASF32
	.byte	0x4
	.byte	0x61
	.4byte	0x21a
	.uleb128 0xd
	.byte	0x4
	.byte	0x4
	.byte	0x6c
	.4byte	0x283
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x4
	.byte	0x70
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF34
	.byte	0x4
	.byte	0x76
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF35
	.byte	0x4
	.byte	0x7a
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x4
	.byte	0x7c
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0x4
	.byte	0x68
	.4byte	0x29c
	.uleb128 0x11
	.4byte	.LASF37
	.byte	0x4
	.byte	0x6a
	.4byte	0x8f
	.uleb128 0x12
	.4byte	0x236
	.byte	0
	.uleb128 0xd
	.byte	0x4
	.byte	0x4
	.byte	0x66
	.4byte	0x2ad
	.uleb128 0x13
	.4byte	0x283
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x4
	.byte	0x82
	.4byte	0x29c
	.uleb128 0xd
	.byte	0x38
	.byte	0x4
	.byte	0xd2
	.4byte	0x38b
	.uleb128 0xe
	.4byte	.LASF39
	.byte	0x4
	.byte	0xdc
	.4byte	0xb7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF40
	.byte	0x4
	.byte	0xe0
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF41
	.byte	0x4
	.byte	0xe9
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF42
	.byte	0x4
	.byte	0xed
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF43
	.byte	0x4
	.byte	0xef
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0x4
	.byte	0xf7
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.4byte	.LASF45
	.byte	0x4
	.byte	0xf9
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0x4
	.byte	0xfc
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x102
	.4byte	0x39c
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x107
	.4byte	0x3ae
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x14
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x10a
	.4byte	0x3ae
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x10f
	.4byte	0x3c4
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x14
	.4byte	.LASF51
	.byte	0x4
	.2byte	0x115
	.4byte	0x3cc
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x14
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x119
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x5
	.byte	0x1
	.4byte	0x39c
	.uleb128 0x6
	.4byte	0xa5
	.uleb128 0x6
	.4byte	0xb7
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.4byte	0x38b
	.uleb128 0x5
	.byte	0x1
	.4byte	0x3ae
	.uleb128 0x6
	.4byte	0xa5
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.4byte	0x3a2
	.uleb128 0x15
	.byte	0x1
	.4byte	0xb7
	.4byte	0x3c4
	.uleb128 0x6
	.4byte	0xa5
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.4byte	0x3b4
	.uleb128 0x16
	.byte	0x1
	.uleb128 0x4
	.byte	0x4
	.4byte	0x3ca
	.uleb128 0x17
	.4byte	.LASF53
	.byte	0x4
	.2byte	0x11b
	.4byte	0x2b8
	.uleb128 0x18
	.byte	0x4
	.byte	0x4
	.2byte	0x126
	.4byte	0x454
	.uleb128 0x19
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x12a
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x19
	.4byte	.LASF55
	.byte	0x4
	.2byte	0x12b
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x19
	.4byte	.LASF56
	.byte	0x4
	.2byte	0x12c
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x19
	.4byte	.LASF57
	.byte	0x4
	.2byte	0x12d
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x19
	.4byte	.LASF58
	.byte	0x4
	.2byte	0x12e
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x19
	.4byte	.LASF59
	.byte	0x4
	.2byte	0x135
	.4byte	0xc2
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x1a
	.byte	0x4
	.byte	0x4
	.2byte	0x122
	.4byte	0x46f
	.uleb128 0x1b
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x124
	.4byte	0xa5
	.uleb128 0x12
	.4byte	0x3de
	.byte	0
	.uleb128 0x18
	.byte	0x4
	.byte	0x4
	.2byte	0x120
	.4byte	0x481
	.uleb128 0x13
	.4byte	0x454
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x17
	.4byte	.LASF60
	.byte	0x4
	.2byte	0x13a
	.4byte	0x46f
	.uleb128 0x18
	.byte	0x94
	.byte	0x4
	.2byte	0x13e
	.4byte	0x59b
	.uleb128 0x14
	.4byte	.LASF61
	.byte	0x4
	.2byte	0x14b
	.4byte	0x59b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF62
	.byte	0x4
	.2byte	0x150
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x14
	.4byte	.LASF63
	.byte	0x4
	.2byte	0x153
	.4byte	0x22b
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x14
	.4byte	.LASF64
	.byte	0x4
	.2byte	0x158
	.4byte	0x5ab
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x14
	.4byte	.LASF65
	.byte	0x4
	.2byte	0x15e
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x14
	.4byte	.LASF66
	.byte	0x4
	.2byte	0x160
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x14
	.4byte	.LASF67
	.byte	0x4
	.2byte	0x16a
	.4byte	0x5bb
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x14
	.4byte	.LASF68
	.byte	0x4
	.2byte	0x170
	.4byte	0x5cb
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x14
	.4byte	.LASF69
	.byte	0x4
	.2byte	0x17a
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x14
	.4byte	.LASF70
	.byte	0x4
	.2byte	0x17e
	.4byte	0x2ad
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x14
	.4byte	.LASF71
	.byte	0x4
	.2byte	0x186
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x14
	.4byte	.LASF72
	.byte	0x4
	.2byte	0x191
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x14
	.4byte	.LASF73
	.byte	0x4
	.2byte	0x1b1
	.4byte	0xa5
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x14
	.4byte	.LASF74
	.byte	0x4
	.2byte	0x1b3
	.4byte	0xa5
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x14
	.4byte	.LASF75
	.byte	0x4
	.2byte	0x1b9
	.4byte	0xa5
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x14
	.4byte	.LASF76
	.byte	0x4
	.2byte	0x1c1
	.4byte	0xa5
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x14
	.4byte	.LASF77
	.byte	0x4
	.2byte	0x1d0
	.4byte	0xb7
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0x8
	.4byte	0x88
	.4byte	0x5ab
	.uleb128 0x9
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0x8
	.4byte	0x481
	.4byte	0x5bb
	.uleb128 0x9
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x8
	.4byte	0x88
	.4byte	0x5cb
	.uleb128 0x9
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x8
	.4byte	0x88
	.4byte	0x5db
	.uleb128 0x9
	.4byte	0x25
	.byte	0x7
	.byte	0
	.uleb128 0x17
	.4byte	.LASF78
	.byte	0x4
	.2byte	0x1d6
	.4byte	0x48d
	.uleb128 0xd
	.byte	0x24
	.byte	0x5
	.byte	0x78
	.4byte	0x66e
	.uleb128 0xe
	.4byte	.LASF79
	.byte	0x5
	.byte	0x7b
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF80
	.byte	0x5
	.byte	0x83
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF81
	.byte	0x5
	.byte	0x86
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF82
	.byte	0x5
	.byte	0x88
	.4byte	0x67f
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF83
	.byte	0x5
	.byte	0x8d
	.4byte	0x691
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF84
	.byte	0x5
	.byte	0x92
	.4byte	0xcd
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.4byte	.LASF85
	.byte	0x5
	.byte	0x96
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.4byte	.LASF86
	.byte	0x5
	.byte	0x9a
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xe
	.4byte	.LASF87
	.byte	0x5
	.byte	0x9c
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0x5
	.byte	0x1
	.4byte	0x67a
	.uleb128 0x6
	.4byte	0x67a
	.byte	0
	.uleb128 0x1c
	.4byte	0x9a
	.uleb128 0x4
	.byte	0x4
	.4byte	0x66e
	.uleb128 0x5
	.byte	0x1
	.4byte	0x691
	.uleb128 0x6
	.4byte	0xff
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.4byte	0x685
	.uleb128 0xa
	.4byte	.LASF88
	.byte	0x5
	.byte	0x9e
	.4byte	0x5e7
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF89
	.uleb128 0x1d
	.4byte	.LASF116
	.byte	0x1
	.byte	0x3a
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x6de
	.uleb128 0x1e
	.4byte	.LASF90
	.byte	0x1
	.byte	0x3a
	.4byte	0x6de
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1f
	.4byte	.LASF91
	.byte	0x1
	.byte	0x60
	.4byte	0xa5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.4byte	0xb7
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF92
	.byte	0x1
	.byte	0x90
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x719
	.uleb128 0x1e
	.4byte	.LASF90
	.byte	0x1
	.byte	0x90
	.4byte	0x6de
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x21
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x92
	.4byte	0x697
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF93
	.byte	0x1
	.byte	0xa6
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x74f
	.uleb128 0x1e
	.4byte	.LASF94
	.byte	0x1
	.byte	0xa6
	.4byte	0x74f
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x21
	.ascii	"lde\000"
	.byte	0x1
	.byte	0xa8
	.4byte	0x697
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x1c
	.4byte	0xff
	.uleb128 0x22
	.4byte	.LASF95
	.byte	0x6
	.2byte	0x16b
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF96
	.byte	0x6
	.2byte	0x16f
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF97
	.byte	0x6
	.2byte	0x170
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF98
	.byte	0x6
	.2byte	0x171
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF99
	.byte	0x6
	.2byte	0x172
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF100
	.byte	0x6
	.2byte	0x173
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF101
	.byte	0x6
	.2byte	0x267
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF102
	.byte	0x6
	.2byte	0x2ec
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF103
	.byte	0x7
	.2byte	0x127
	.4byte	0x55
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF104
	.byte	0x8
	.byte	0x30
	.4byte	0x7e3
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x1c
	.4byte	0x78
	.uleb128 0x1f
	.4byte	.LASF105
	.byte	0x8
	.byte	0x34
	.4byte	0x7f9
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x1c
	.4byte	0x78
	.uleb128 0x1f
	.4byte	.LASF106
	.byte	0x8
	.byte	0x36
	.4byte	0x80f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x1c
	.4byte	0x78
	.uleb128 0x1f
	.4byte	.LASF107
	.byte	0x8
	.byte	0x38
	.4byte	0x825
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x1c
	.4byte	0x78
	.uleb128 0x23
	.4byte	.LASF108
	.byte	0x9
	.byte	0x22
	.4byte	0xa5
	.byte	0x1
	.byte	0x1
	.uleb128 0x23
	.4byte	.LASF109
	.byte	0x9
	.byte	0x24
	.4byte	0xb7
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF110
	.byte	0x4
	.2byte	0x1d9
	.4byte	0x5db
	.byte	0x1
	.byte	0x1
	.uleb128 0x8
	.4byte	0x3d2
	.4byte	0x85d
	.uleb128 0x24
	.byte	0
	.uleb128 0x22
	.4byte	.LASF111
	.byte	0x4
	.2byte	0x1e0
	.4byte	0x86b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	0x852
	.uleb128 0x22
	.4byte	.LASF112
	.byte	0xa
	.2byte	0x212
	.4byte	0xb7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF113
	.byte	0x1
	.byte	0x34
	.4byte	0xa5
	.byte	0x5
	.byte	0x3
	.4byte	g_COMM_OPTIONS_last_cursor_position
	.uleb128 0x22
	.4byte	.LASF95
	.byte	0x6
	.2byte	0x16b
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF96
	.byte	0x6
	.2byte	0x16f
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF97
	.byte	0x6
	.2byte	0x170
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF98
	.byte	0x6
	.2byte	0x171
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF99
	.byte	0x6
	.2byte	0x172
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF100
	.byte	0x6
	.2byte	0x173
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF101
	.byte	0x6
	.2byte	0x267
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF102
	.byte	0x6
	.2byte	0x2ec
	.4byte	0xb0
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF103
	.byte	0x7
	.2byte	0x127
	.4byte	0x55
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF108
	.byte	0x1
	.byte	0x2e
	.4byte	0xa5
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_COMM_OPTIONS_cursor_position_when_dialog_displayed
	.uleb128 0x25
	.4byte	.LASF109
	.byte	0x1
	.byte	0x30
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_COMM_OPTIONS_dialog_visible
	.uleb128 0x22
	.4byte	.LASF110
	.byte	0x4
	.2byte	0x1d9
	.4byte	0x5db
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF111
	.byte	0x4
	.2byte	0x1e0
	.4byte	0x94d
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	0x852
	.uleb128 0x22
	.4byte	.LASF112
	.byte	0xa
	.2byte	0x212
	.4byte	0xb7
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF87:
	.ascii	"_08_screen_to_draw\000"
.LASF37:
	.ascii	"size_of_the_union\000"
.LASF114:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF116:
	.ascii	"FDTO_COMM_OPTIONS_draw_dialog\000"
.LASF35:
	.ascii	"use_new_k_and_offset_numbers\000"
.LASF73:
	.ascii	"OM_Seconds_for_Status_FOAL\000"
.LASF14:
	.ascii	"BITFIELD_BOOL\000"
.LASF72:
	.ascii	"OM_Originator_Retries\000"
.LASF32:
	.ascii	"PURCHASED_OPTIONS_STRUCT\000"
.LASF109:
	.ascii	"g_COMM_OPTIONS_dialog_visible\000"
.LASF39:
	.ascii	"on_port_A_enables_controller_to_make_CI_messages\000"
.LASF67:
	.ascii	"comm_server_ip_address\000"
.LASF47:
	.ascii	"__power_device_ptr\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF75:
	.ascii	"test_seconds\000"
.LASF60:
	.ascii	"CONFIGURATION_PORT_CONTROL_STRUCT\000"
.LASF92:
	.ascii	"COMM_OPTIONS_draw_dialog\000"
.LASF79:
	.ascii	"_01_command\000"
.LASF62:
	.ascii	"serial_number\000"
.LASF101:
	.ascii	"GuiVar_IsAHub\000"
.LASF102:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF3:
	.ascii	"short int\000"
.LASF41:
	.ascii	"cts_when_OK_to_send\000"
.LASF15:
	.ascii	"keycode\000"
.LASF54:
	.ascii	"nlu_bit_0\000"
.LASF55:
	.ascii	"nlu_bit_1\000"
.LASF56:
	.ascii	"nlu_bit_2\000"
.LASF80:
	.ascii	"_02_menu\000"
.LASF58:
	.ascii	"nlu_bit_4\000"
.LASF66:
	.ascii	"port_B_device_index\000"
.LASF43:
	.ascii	"ri_polarity\000"
.LASF40:
	.ascii	"baud_rate\000"
.LASF42:
	.ascii	"cd_when_connected\000"
.LASF113:
	.ascii	"g_COMM_OPTIONS_last_cursor_position\000"
.LASF68:
	.ascii	"comm_server_port\000"
.LASF99:
	.ascii	"GuiVar_CommOptionPortBInstalled\000"
.LASF96:
	.ascii	"GuiVar_CommOptionPortAIndex\000"
.LASF89:
	.ascii	"float\000"
.LASF57:
	.ascii	"nlu_bit_3\000"
.LASF61:
	.ascii	"nlu_controller_name\000"
.LASF6:
	.ascii	"long long int\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF106:
	.ascii	"GuiFont_DecimalChar\000"
.LASF17:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF4:
	.ascii	"long int\000"
.LASF65:
	.ascii	"port_A_device_index\000"
.LASF71:
	.ascii	"dummy\000"
.LASF33:
	.ascii	"transport_om_show_unexpected_behavior_alerts\000"
.LASF9:
	.ascii	"UNS_16\000"
.LASF85:
	.ascii	"_06_u32_argument1\000"
.LASF111:
	.ascii	"port_device_table\000"
.LASF29:
	.ascii	"unused_13\000"
.LASF30:
	.ascii	"unused_14\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF23:
	.ascii	"port_a_freewave_lr_set_for_repeater\000"
.LASF83:
	.ascii	"key_process_func_ptr\000"
.LASF100:
	.ascii	"GuiVar_CommOptionRadioTestAvailable\000"
.LASF90:
	.ascii	"pcomplete_redraw\000"
.LASF53:
	.ascii	"PORT_DEVICE_SETTINGS_STRUCT\000"
.LASF110:
	.ascii	"config_c\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF2:
	.ascii	"signed char\000"
.LASF69:
	.ascii	"nlu_prevent_automated_CI_transmissions\000"
.LASF38:
	.ascii	"DEBUG_BITS_STRUCT\000"
.LASF105:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF22:
	.ascii	"port_a_raveon_radio_type\000"
.LASF76:
	.ascii	"last_assigned_decoder_serial_number\000"
.LASF108:
	.ascii	"g_COMM_OPTIONS_cursor_position_when_dialog_displaye"
	.ascii	"d\000"
.LASF70:
	.ascii	"debug\000"
.LASF48:
	.ascii	"__initialize_the_connection_process\000"
.LASF115:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_comm_options.c\000"
.LASF81:
	.ascii	"_03_structure_to_draw\000"
.LASF98:
	.ascii	"GuiVar_CommOptionPortBIndex\000"
.LASF103:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF27:
	.ascii	"port_b_freewave_sr_set_for_repeater\000"
.LASF28:
	.ascii	"option_AQUAPONICS\000"
.LASF8:
	.ascii	"char\000"
.LASF59:
	.ascii	"alert_about_crc_errors\000"
.LASF25:
	.ascii	"port_b_raveon_radio_type\000"
.LASF107:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF51:
	.ascii	"__initialize_device_exchange\000"
.LASF82:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF95:
	.ascii	"GuiVar_CommOptionCloudCommTestAvailable\000"
.LASF46:
	.ascii	"reset_active_level\000"
.LASF49:
	.ascii	"__connection_processing\000"
.LASF24:
	.ascii	"port_a_freewave_sr_set_for_repeater\000"
.LASF21:
	.ascii	"option_HUB\000"
.LASF74:
	.ascii	"OM_Minutes_To_Exist\000"
.LASF18:
	.ascii	"option_FL\000"
.LASF10:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF77:
	.ascii	"hub_enabled_user_setting\000"
.LASF34:
	.ascii	"transport_om_show_status_timer_expired_alert\000"
.LASF84:
	.ascii	"_04_func_ptr\000"
.LASF45:
	.ascii	"dtr_level_to_connect\000"
.LASF52:
	.ascii	"__device_exchange_processing\000"
.LASF86:
	.ascii	"_07_u32_argument2\000"
.LASF78:
	.ascii	"CONTROLLER_CONFIGURATION_STRUCT\000"
.LASF16:
	.ascii	"repeats\000"
.LASF104:
	.ascii	"GuiFont_LanguageActive\000"
.LASF91:
	.ascii	"lcursor_to_select\000"
.LASF63:
	.ascii	"purchased_options\000"
.LASF26:
	.ascii	"port_b_freewave_lr_set_for_repeater\000"
.LASF50:
	.ascii	"__is_connected\000"
.LASF97:
	.ascii	"GuiVar_CommOptionPortAInstalled\000"
.LASF94:
	.ascii	"pkey_event\000"
.LASF36:
	.ascii	"show_flow_table_interaction\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF19:
	.ascii	"option_SSE\000"
.LASF44:
	.ascii	"rts_level_to_cause_device_to_send\000"
.LASF88:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF112:
	.ascii	"in_device_exchange_hammer\000"
.LASF93:
	.ascii	"COMM_OPTIONS_process_dialog\000"
.LASF64:
	.ascii	"port_settings\000"
.LASF20:
	.ascii	"option_SSE_D\000"
.LASF31:
	.ascii	"unused_15\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
