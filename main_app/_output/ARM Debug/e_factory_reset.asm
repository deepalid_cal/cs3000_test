	.file	"e_factory_reset.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.text.FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart,"ax",%progbits
	.align	2
	.global	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart
	.type	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart, %function
FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_factory_reset.c"
	.loc 1 48 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 66 0
	ldr	r3, .L4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #10
	bl	vTaskPrioritySet
	.loc 1 83 0
	mov	r0, #1
	bl	FLASH_STORAGE_delete_all_files
	.loc 1 104 0
	mov	r0, #34
	bl	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits
	.loc 1 106 0
	ldr	r3, [fp, #-8]
	cmp	r3, #28
	beq	.L2
	.loc 1 106 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #29
	bne	.L3
.L2:
	.loc 1 111 0 is_stmt 1
	bl	NETWORK_CONFIG_brute_force_set_network_ID_to_zero
.L3:
	.loc 1 114 0
	bl	save_file_configuration_network
	.loc 1 126 0
	mov	r0, #200
	bl	vTaskDelay
	.loc 1 134 0
	mov	r0, #1
	bl	init_battery_backed_weather_preserves
	.loc 1 136 0
	mov	r0, #1
	bl	init_battery_backed_station_preserves
	.loc 1 138 0
	mov	r0, #1
	bl	init_battery_backed_system_preserves
	.loc 1 140 0
	mov	r0, #1
	bl	init_battery_backed_poc_preserves
	.loc 1 142 0
	mov	r0, #1
	bl	init_battery_backed_foal_irri
	.loc 1 144 0
	mov	r0, #1
	bl	init_battery_backed_chain_members
	.loc 1 146 0
	mov	r0, #1
	bl	init_battery_backed_general_use
	.loc 1 148 0
	mov	r0, #1
	bl	init_battery_backed_lights_preserves
	.loc 1 150 0
	mov	r0, #1
	bl	init_battery_backed_foal_lights
	.loc 1 157 0
	bl	ALERTS_restart_all_piles
	.loc 1 167 0
	ldr	r3, .L4+4
	mov	r2, #1
	strb	r2, [r3, #130]
	.loc 1 172 0
	ldr	r0, [fp, #-8]
	bl	SYSTEM_application_requested_restart
	.loc 1 173 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	flash_storage_1_task_handle
	.word	weather_preserves
.LFE0:
	.size	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart, .-FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart
	.section	.text.FDTO_FACTORY_RESET_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_FACTORY_RESET_draw_screen
	.type	FDTO_FACTORY_RESET_draw_screen, %function
FDTO_FACTORY_RESET_draw_screen:
.LFB1:
	.loc 1 177 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 178 0
	mov	r0, #20
	mov	r1, #1
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 179 0
	bl	GuiLib_Refresh
	.loc 1 180 0
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	FDTO_FACTORY_RESET_draw_screen, .-FDTO_FACTORY_RESET_draw_screen
	.section	.text.FACTORY_RESET_process_screen,"ax",%progbits
	.align	2
	.global	FACTORY_RESET_process_screen
	.type	FACTORY_RESET_process_screen, %function
FACTORY_RESET_process_screen:
.LFB2:
	.loc 1 184 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #8
.LCFI7:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 185 0
	ldr	r3, [fp, #-12]
	cmp	r3, #2
	beq	.L10
	cmp	r3, #2
	bhi	.L13
	cmp	r3, #1
	beq	.L9
	b	.L8
.L13:
	cmp	r3, #3
	beq	.L11
	cmp	r3, #67
	beq	.L12
	b	.L8
.L10:
	.loc 1 188 0
	bl	good_key_beep
	.loc 1 190 0
	ldr	r3, .L19
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #1
	bne	.L14
	.loc 1 192 0
	ldr	r0, .L19+4
	bl	DIALOG_draw_ok_dialog
	.loc 1 196 0
	mov	r0, #27
	bl	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart
	.loc 1 203 0
	b	.L7
.L14:
	.loc 1 200 0
	bl	ALERTS_restart_all_piles
	.loc 1 203 0
	b	.L7
.L9:
	.loc 1 206 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 207 0
	b	.L7
.L11:
	.loc 1 210 0
	ldr	r3, .L19
	ldrh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L17
	.loc 1 212 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 218 0
	b	.L7
.L17:
	.loc 1 216 0
	bl	bad_key_beep
	.loc 1 218 0
	b	.L7
.L12:
	.loc 1 221 0
	ldr	r3, .L19+8
	mov	r2, #11
	str	r2, [r3, #0]
	.loc 1 223 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 227 0
	mov	r0, #1
	bl	TECH_SUPPORT_draw_dialog
	.loc 1 228 0
	b	.L7
.L8:
	.loc 1 231 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L7:
	.loc 1 233 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L20:
	.align	2
.L19:
	.word	GuiLib_ActiveCursorFieldNo
	.word	618
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	FACTORY_RESET_process_screen, .-FACTORY_RESET_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/task.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_comm_server_common.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x588
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF79
	.byte	0x1
	.4byte	.LASF80
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x5
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x35
	.4byte	0x90
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x4
	.byte	0x63
	.4byte	0x97
	.uleb128 0x6
	.4byte	0x37
	.4byte	0xc6
	.uleb128 0x7
	.4byte	0x90
	.byte	0x1
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.byte	0x5
	.byte	0x7c
	.4byte	0xeb
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x5
	.byte	0x7e
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x5
	.byte	0x80
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x5
	.byte	0x82
	.4byte	0xc6
	.uleb128 0x8
	.byte	0x4
	.byte	0x6
	.byte	0x4c
	.4byte	0x11b
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x6
	.byte	0x55
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x6
	.byte	0x57
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x6
	.byte	0x59
	.4byte	0xf6
	.uleb128 0x8
	.byte	0x4
	.byte	0x6
	.byte	0x65
	.4byte	0x14b
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x6
	.byte	0x67
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x6
	.byte	0x69
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF23
	.byte	0x6
	.byte	0x6b
	.4byte	0x126
	.uleb128 0x8
	.byte	0x6
	.byte	0x7
	.byte	0x22
	.4byte	0x177
	.uleb128 0xa
	.ascii	"T\000"
	.byte	0x7
	.byte	0x24
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.ascii	"D\000"
	.byte	0x7
	.byte	0x26
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x7
	.byte	0x28
	.4byte	0x156
	.uleb128 0x6
	.4byte	0x5e
	.4byte	0x192
	.uleb128 0x7
	.4byte	0x90
	.byte	0x2
	.byte	0
	.uleb128 0x6
	.4byte	0x25
	.4byte	0x1a2
	.uleb128 0x7
	.4byte	0x90
	.byte	0xf
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF25
	.uleb128 0x6
	.4byte	0x5e
	.4byte	0x1b9
	.uleb128 0x7
	.4byte	0x90
	.byte	0x3
	.byte	0
	.uleb128 0xb
	.byte	0x1c
	.byte	0x8
	.2byte	0x10c
	.4byte	0x22c
	.uleb128 0xc
	.ascii	"rip\000"
	.byte	0x8
	.2byte	0x112
	.4byte	0x14b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF26
	.byte	0x8
	.2byte	0x11b
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF27
	.byte	0x8
	.2byte	0x122
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF28
	.byte	0x8
	.2byte	0x127
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xd
	.4byte	.LASF29
	.byte	0x8
	.2byte	0x138
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF30
	.byte	0x8
	.2byte	0x144
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xd
	.4byte	.LASF31
	.byte	0x8
	.2byte	0x14b
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0xe
	.4byte	.LASF32
	.byte	0x8
	.2byte	0x14d
	.4byte	0x1b9
	.uleb128 0xb
	.byte	0xec
	.byte	0x8
	.2byte	0x150
	.4byte	0x40c
	.uleb128 0xd
	.4byte	.LASF33
	.byte	0x8
	.2byte	0x157
	.4byte	0x192
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF34
	.byte	0x8
	.2byte	0x162
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF35
	.byte	0x8
	.2byte	0x164
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xd
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x166
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xd
	.4byte	.LASF37
	.byte	0x8
	.2byte	0x168
	.4byte	0x177
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xd
	.4byte	.LASF38
	.byte	0x8
	.2byte	0x16e
	.4byte	0x11b
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xd
	.4byte	.LASF39
	.byte	0x8
	.2byte	0x174
	.4byte	0x22c
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xd
	.4byte	.LASF40
	.byte	0x8
	.2byte	0x17b
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xd
	.4byte	.LASF41
	.byte	0x8
	.2byte	0x17d
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xd
	.4byte	.LASF42
	.byte	0x8
	.2byte	0x185
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0xd
	.4byte	.LASF43
	.byte	0x8
	.2byte	0x18d
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xd
	.4byte	.LASF44
	.byte	0x8
	.2byte	0x191
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xd
	.4byte	.LASF45
	.byte	0x8
	.2byte	0x195
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xd
	.4byte	.LASF46
	.byte	0x8
	.2byte	0x199
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0xd
	.4byte	.LASF47
	.byte	0x8
	.2byte	0x19e
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0xd
	.4byte	.LASF48
	.byte	0x8
	.2byte	0x1a2
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0xd
	.4byte	.LASF49
	.byte	0x8
	.2byte	0x1a6
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0xd
	.4byte	.LASF50
	.byte	0x8
	.2byte	0x1b4
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0xd
	.4byte	.LASF51
	.byte	0x8
	.2byte	0x1ba
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0xd
	.4byte	.LASF52
	.byte	0x8
	.2byte	0x1c2
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0xd
	.4byte	.LASF53
	.byte	0x8
	.2byte	0x1c4
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0xd
	.4byte	.LASF54
	.byte	0x8
	.2byte	0x1c6
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0xd
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x1d5
	.4byte	0x2c
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0xd
	.4byte	.LASF56
	.byte	0x8
	.2byte	0x1d7
	.4byte	0x2c
	.byte	0x3
	.byte	0x23
	.uleb128 0x81
	.uleb128 0xd
	.4byte	.LASF57
	.byte	0x8
	.2byte	0x1dd
	.4byte	0x2c
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0xd
	.4byte	.LASF58
	.byte	0x8
	.2byte	0x1e7
	.4byte	0x2c
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0xd
	.4byte	.LASF59
	.byte	0x8
	.2byte	0x1f0
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0xd
	.4byte	.LASF60
	.byte	0x8
	.2byte	0x1f7
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0xd
	.4byte	.LASF61
	.byte	0x8
	.2byte	0x1f9
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0xd
	.4byte	.LASF62
	.byte	0x8
	.2byte	0x1fd
	.4byte	0x40c
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0x6
	.4byte	0x5e
	.4byte	0x41c
	.uleb128 0x7
	.4byte	0x90
	.byte	0x16
	.byte	0
	.uleb128 0xe
	.4byte	.LASF63
	.byte	0x8
	.2byte	0x204
	.4byte	0x238
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF64
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF65
	.byte	0x1
	.byte	0x2f
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x457
	.uleb128 0x10
	.4byte	.LASF67
	.byte	0x1
	.byte	0x2f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF81
	.byte	0x1
	.byte	0xb0
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF66
	.byte	0x1
	.byte	0xb7
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x494
	.uleb128 0x10
	.4byte	.LASF68
	.byte	0x1
	.byte	0xb7
	.4byte	0x494
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x12
	.4byte	0xeb
	.uleb128 0x13
	.4byte	.LASF69
	.byte	0x9
	.2byte	0x2ec
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF70
	.byte	0xa
	.2byte	0x127
	.4byte	0x57
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF71
	.byte	0xb
	.byte	0x30
	.4byte	0x4c6
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x12
	.4byte	0xb6
	.uleb128 0x14
	.4byte	.LASF72
	.byte	0xb
	.byte	0x34
	.4byte	0x4dc
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x12
	.4byte	0xb6
	.uleb128 0x14
	.4byte	.LASF73
	.byte	0xb
	.byte	0x36
	.4byte	0x4f2
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x12
	.4byte	0xb6
	.uleb128 0x14
	.4byte	.LASF74
	.byte	0xb
	.byte	0x38
	.4byte	0x508
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x12
	.4byte	0xb6
	.uleb128 0x14
	.4byte	.LASF75
	.byte	0xc
	.byte	0x33
	.4byte	0x51e
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x12
	.4byte	0x182
	.uleb128 0x14
	.4byte	.LASF76
	.byte	0xc
	.byte	0x3f
	.4byte	0x534
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x12
	.4byte	0x1a9
	.uleb128 0x13
	.4byte	.LASF77
	.byte	0x8
	.2byte	0x206
	.4byte	0x41c
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF78
	.byte	0xd
	.byte	0x52
	.4byte	0xab
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF69
	.byte	0x9
	.2byte	0x2ec
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF70
	.byte	0xa
	.2byte	0x127
	.4byte	0x57
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF77
	.byte	0x8
	.2byte	0x206
	.4byte	0x41c
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF78
	.byte	0xd
	.byte	0x52
	.4byte	0xab
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF79:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF31:
	.ascii	"needs_to_be_broadcast\000"
.LASF65:
	.ascii	"FACTORY_RESET_init_all_battery_backed_delete_all_fi"
	.ascii	"les_and_restart\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF34:
	.ascii	"dls_saved_date\000"
.LASF78:
	.ascii	"flash_storage_1_task_handle\000"
.LASF51:
	.ascii	"pending_changes_to_send_to_comm_server\000"
.LASF6:
	.ascii	"short int\000"
.LASF14:
	.ascii	"portTickType\000"
.LASF30:
	.ascii	"inhibit_irrigation_have_crossed_the_minimum\000"
.LASF27:
	.ascii	"roll_to_roll_raw_pulse_count\000"
.LASF38:
	.ascii	"et_rip\000"
.LASF46:
	.ascii	"remaining_gage_pulses\000"
.LASF58:
	.ascii	"ununsed_uns8_1\000"
.LASF29:
	.ascii	"rain_shutdown_rcvd_from_commserver_uns32\000"
.LASF50:
	.ascii	"write_tpmicro_file_in_new_file_format\000"
.LASF69:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF43:
	.ascii	"dont_use_et_gage_today\000"
.LASF59:
	.ascii	"commserver_monthly_mobile_status_updates_bytes\000"
.LASF61:
	.ascii	"hub_needs_to_distribute_main_binary\000"
.LASF40:
	.ascii	"sync_the_et_rain_tables\000"
.LASF41:
	.ascii	"nlu_old_rain_table_sync_variable\000"
.LASF80:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_factory_reset.c\000"
.LASF32:
	.ascii	"RAIN_STATE\000"
.LASF25:
	.ascii	"float\000"
.LASF22:
	.ascii	"rain_inches_u16_100u\000"
.LASF10:
	.ascii	"long long int\000"
.LASF73:
	.ascii	"GuiFont_DecimalChar\000"
.LASF66:
	.ascii	"FACTORY_RESET_process_screen\000"
.LASF13:
	.ascii	"long int\000"
.LASF35:
	.ascii	"dls_eligible_to_fall_back\000"
.LASF67:
	.ascii	"psystem_shutdown_event\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF54:
	.ascii	"commserver_monthly_code_receipt_bytes\000"
.LASF36:
	.ascii	"dls_after_fall_back_ignore_sxs\000"
.LASF52:
	.ascii	"commserver_monthly_rcvd_bytes\000"
.LASF55:
	.ascii	"factory_reset_do_not_allow_pdata_to_be_sent\000"
.LASF44:
	.ascii	"run_away_gage\000"
.LASF47:
	.ascii	"clear_runaway_gage\000"
.LASF24:
	.ascii	"DATE_TIME\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF2:
	.ascii	"signed char\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF23:
	.ascii	"RAIN_TABLE_ENTRY\000"
.LASF72:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF15:
	.ascii	"xTaskHandle\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF60:
	.ascii	"hub_needs_to_distribute_tpmicro_binary\000"
.LASF77:
	.ascii	"weather_preserves\000"
.LASF42:
	.ascii	"et_table_update_all_historical_values\000"
.LASF19:
	.ascii	"et_inches_u16_10000u\000"
.LASF20:
	.ascii	"status\000"
.LASF37:
	.ascii	"dls_after_fall_when_to_clear_ignore_sxs\000"
.LASF70:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF0:
	.ascii	"char\000"
.LASF28:
	.ascii	"midnight_to_midnight_raw_pulse_count\000"
.LASF74:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF71:
	.ascii	"GuiFont_LanguageActive\000"
.LASF45:
	.ascii	"yesterdays_ET_was_zero\000"
.LASF33:
	.ascii	"verify_string_pre\000"
.LASF12:
	.ascii	"long unsigned int\000"
.LASF76:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF18:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF49:
	.ascii	"freeze_switch_active\000"
.LASF16:
	.ascii	"keycode\000"
.LASF17:
	.ascii	"repeats\000"
.LASF57:
	.ascii	"perform_a_discovery_following_a_FACTORY_reset\000"
.LASF75:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF62:
	.ascii	"expansion\000"
.LASF63:
	.ascii	"WEATHER_PRESERVES_STRUCT\000"
.LASF68:
	.ascii	"pkey_event\000"
.LASF64:
	.ascii	"double\000"
.LASF48:
	.ascii	"rain_switch_active\000"
.LASF53:
	.ascii	"commserver_monthly_xmit_bytes\000"
.LASF21:
	.ascii	"ET_TABLE_ENTRY\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF56:
	.ascii	"factory_reset_registration_to_tell_commserver_to_cl"
	.ascii	"ear_pdata\000"
.LASF81:
	.ascii	"FDTO_FACTORY_RESET_draw_screen\000"
.LASF39:
	.ascii	"rain\000"
.LASF26:
	.ascii	"hourly_total_inches_100u\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
