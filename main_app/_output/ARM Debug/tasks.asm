	.file	"tasks.c"
	.text
.Ltext0:
	.global	pxCurrentTCB
	.section	.bss.pxCurrentTCB,"aw",%nobits
	.align	2
	.type	pxCurrentTCB, %object
	.size	pxCurrentTCB, 4
pxCurrentTCB:
	.space	4
	.section	.bss.pxReadyTasksLists,"aw",%nobits
	.align	2
	.type	pxReadyTasksLists, %object
	.size	pxReadyTasksLists, 320
pxReadyTasksLists:
	.space	320
	.section	.bss.xDelayedTaskList1,"aw",%nobits
	.align	2
	.type	xDelayedTaskList1, %object
	.size	xDelayedTaskList1, 20
xDelayedTaskList1:
	.space	20
	.section	.bss.xDelayedTaskList2,"aw",%nobits
	.align	2
	.type	xDelayedTaskList2, %object
	.size	xDelayedTaskList2, 20
xDelayedTaskList2:
	.space	20
	.section	.bss.pxDelayedTaskList,"aw",%nobits
	.align	2
	.type	pxDelayedTaskList, %object
	.size	pxDelayedTaskList, 4
pxDelayedTaskList:
	.space	4
	.section	.bss.pxOverflowDelayedTaskList,"aw",%nobits
	.align	2
	.type	pxOverflowDelayedTaskList, %object
	.size	pxOverflowDelayedTaskList, 4
pxOverflowDelayedTaskList:
	.space	4
	.section	.bss.xPendingReadyList,"aw",%nobits
	.align	2
	.type	xPendingReadyList, %object
	.size	xPendingReadyList, 20
xPendingReadyList:
	.space	20
	.section	.bss.xTasksWaitingTermination,"aw",%nobits
	.align	2
	.type	xTasksWaitingTermination, %object
	.size	xTasksWaitingTermination, 20
xTasksWaitingTermination:
	.space	20
	.section	.bss.uxTasksDeleted,"aw",%nobits
	.align	2
	.type	uxTasksDeleted, %object
	.size	uxTasksDeleted, 4
uxTasksDeleted:
	.space	4
	.section	.bss.xSuspendedTaskList,"aw",%nobits
	.align	2
	.type	xSuspendedTaskList, %object
	.size	xSuspendedTaskList, 20
xSuspendedTaskList:
	.space	20
	.section	.bss.uxCurrentNumberOfTasks,"aw",%nobits
	.align	2
	.type	uxCurrentNumberOfTasks, %object
	.size	uxCurrentNumberOfTasks, 4
uxCurrentNumberOfTasks:
	.space	4
	.section	.bss.xTickCount,"aw",%nobits
	.align	2
	.type	xTickCount, %object
	.size	xTickCount, 4
xTickCount:
	.space	4
	.section	.bss.uxTopUsedPriority,"aw",%nobits
	.align	2
	.type	uxTopUsedPriority, %object
	.size	uxTopUsedPriority, 4
uxTopUsedPriority:
	.space	4
	.section	.bss.uxTopReadyPriority,"aw",%nobits
	.align	2
	.type	uxTopReadyPriority, %object
	.size	uxTopReadyPriority, 4
uxTopReadyPriority:
	.space	4
	.section	.bss.xSchedulerRunning,"aw",%nobits
	.align	2
	.type	xSchedulerRunning, %object
	.size	xSchedulerRunning, 4
xSchedulerRunning:
	.space	4
	.section	.bss.uxSchedulerSuspended,"aw",%nobits
	.align	2
	.type	uxSchedulerSuspended, %object
	.size	uxSchedulerSuspended, 4
uxSchedulerSuspended:
	.space	4
	.section	.bss.uxMissedTicks,"aw",%nobits
	.align	2
	.type	uxMissedTicks, %object
	.size	uxMissedTicks, 4
uxMissedTicks:
	.space	4
	.section	.bss.xMissedYield,"aw",%nobits
	.align	2
	.type	xMissedYield, %object
	.size	xMissedYield, 4
xMissedYield:
	.space	4
	.section	.bss.xNumOfOverflows,"aw",%nobits
	.align	2
	.type	xNumOfOverflows, %object
	.size	xNumOfOverflows, 4
xNumOfOverflows:
	.space	4
	.section	.bss.uxTaskNumber,"aw",%nobits
	.align	2
	.type	uxTaskNumber, %object
	.size	uxTaskNumber, 4
uxTaskNumber:
	.space	4
	.section	.data.xNextTaskUnblockTime,"aw",%progbits
	.align	2
	.type	xNextTaskUnblockTime, %object
	.size	xNextTaskUnblockTime, 4
xNextTaskUnblockTime:
	.word	-1
	.section	.bss.ulTaskSwitchedInTime,"aw",%nobits
	.align	2
	.type	ulTaskSwitchedInTime, %object
	.size	ulTaskSwitchedInTime, 4
ulTaskSwitchedInTime:
	.space	4
	.section .rodata
	.align	2
.LC0:
	.ascii	"pxTaskCode\000"
	.align	2
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/tasks.c\000"
	.align	2
.LC2:
	.ascii	"( ( uxPriority & ( ~( ( unsigned long ) 0x00 ) ) ) "
	.ascii	"< 16 )\000"
	.align	2
.LC3:
	.ascii	"( ( ( unsigned long ) pxTopOfStack & ( unsigned lon"
	.ascii	"g ) ( 0x0007 ) ) == 0UL )\000"
	.align	2
.LC4:
	.ascii	"( ( ( unsigned long ) pxNewTCB->pxTopOfStack & ( un"
	.ascii	"signed long ) ( 0x0007 ) ) == 0UL )\000"
	.section	.text.xTaskGenericCreate,"ax",%progbits
	.align	2
	.global	xTaskGenericCreate
	.type	xTaskGenericCreate, %function
xTaskGenericCreate:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c"
	.loc 1 400 0
	@ args = 16, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #32
.LCFI2:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r3, [fp, #-32]
	strh	r2, [fp, #-28]	@ movhi
	.loc 1 404 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L2
	.loc 1 404 0 is_stmt 0 discriminator 1
	ldr	r0, .L14
	ldr	r1, .L14+4
	mov	r2, #404
	bl	__assert
.L2:
	.loc 1 405 0 is_stmt 1
	ldr	r3, [fp, #4]
	cmp	r3, #15
	bls	.L3
	.loc 1 405 0 is_stmt 0 discriminator 1
	ldr	r0, .L14+8
	ldr	r1, .L14+4
	ldr	r2, .L14+12
	bl	__assert
.L3:
	.loc 1 409 0 is_stmt 1
	ldrh	r3, [fp, #-28]
	mov	r0, r3
	ldr	r1, [fp, #12]
	bl	prvAllocateTCBAndStack
	str	r0, [fp, #-12]
	.loc 1 411 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L4
.LBB2:
	.loc 1 435 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #48]
	ldrh	r3, [fp, #-28]
	sub	r3, r3, #1
	mov	r3, r3, asl #2
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 436 0
	ldr	r3, [fp, #-16]
	bic	r3, r3, #7
	str	r3, [fp, #-16]
	.loc 1 439 0
	ldr	r3, [fp, #-16]
	and	r3, r3, #7
	cmp	r3, #0
	beq	.L5
	.loc 1 439 0 is_stmt 0 discriminator 1
	ldr	r0, .L14+16
	ldr	r1, .L14+4
	ldr	r2, .L14+20
	bl	__assert
.L5:
	.loc 1 456 0 is_stmt 1
	ldrh	r3, [fp, #-28]
	str	r3, [sp, #0]
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-24]
	ldr	r2, [fp, #4]
	ldr	r3, [fp, #16]
	bl	prvInitialiseTCBVariables
	.loc 1 468 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-20]
	ldr	r2, [fp, #-32]
	bl	pxPortInitialiseStack
	mov	r2, r0
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 473 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	and	r3, r3, #7
	cmp	r3, #0
	beq	.L6
	.loc 1 473 0 is_stmt 0 discriminator 1
	ldr	r0, .L14+24
	ldr	r1, .L14+4
	ldr	r2, .L14+28
	bl	__assert
.L6:
	.loc 1 475 0 is_stmt 1
	ldr	r3, [fp, #8]
	cmp	r3, #0
	beq	.L7
	.loc 1 480 0
	ldr	r3, [fp, #8]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
.L7:
	.loc 1 485 0
	bl	vPortEnterCritical
	.loc 1 487 0
	ldr	r3, .L14+32
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L14+32
	str	r2, [r3, #0]
	.loc 1 488 0
	ldr	r3, .L14+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L8
	.loc 1 492 0
	ldr	r3, .L14+36
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 494 0
	ldr	r3, .L14+32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L9
	.loc 1 499 0
	bl	prvInitialiseTaskLists
	b	.L9
.L8:
	.loc 1 507 0
	ldr	r3, .L14+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	.loc 1 509 0
	ldr	r3, .L14+36
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #44]
	ldr	r3, [fp, #4]
	cmp	r2, r3
	bhi	.L9
	.loc 1 511 0
	ldr	r3, .L14+36
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
.L9:
	.loc 1 518 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L14+44
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L10
	.loc 1 520 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L14+44
	str	r2, [r3, #0]
.L10:
	.loc 1 529 0
	ldr	r3, .L14+48
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L14+48
	str	r2, [r3, #0]
	.loc 1 531 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L14+52
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L11
	.loc 1 531 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L14+52
	str	r2, [r3, #0]
.L11:
	.loc 1 531 0 discriminator 2
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L14+56
	add	r2, r2, r3
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsertEnd
	.loc 1 533 0 is_stmt 1 discriminator 2
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 537 0 discriminator 2
	bl	vPortExitCritical
	b	.L12
.L4:
.LBE2:
	.loc 1 541 0
	mvn	r3, #0
	str	r3, [fp, #-8]
.L12:
	.loc 1 545 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L13
	.loc 1 547 0
	ldr	r3, .L14+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L13
	.loc 1 551 0
	ldr	r3, .L14+36
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #44]
	ldr	r3, [fp, #4]
	cmp	r2, r3
	bcs	.L13
	.loc 1 553 0
@ 553 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
.L13:
	.loc 1 558 0
	ldr	r3, [fp, #-8]
	.loc 1 559 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L15:
	.align	2
.L14:
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	405
	.word	.LC3
	.word	439
	.word	.LC4
	.word	473
	.word	uxCurrentNumberOfTasks
	.word	pxCurrentTCB
	.word	xSchedulerRunning
	.word	uxTopUsedPriority
	.word	uxTaskNumber
	.word	uxTopReadyPriority
	.word	pxReadyTasksLists
.LFE0:
	.size	xTaskGenericCreate, .-xTaskGenericCreate
	.section	.text.vTaskDelete,"ax",%progbits
	.align	2
	.global	vTaskDelete
	.type	vTaskDelete, %function
vTaskDelete:
.LFB1:
	.loc 1 565 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-12]
	.loc 1 568 0
	bl	vPortEnterCritical
	.loc 1 572 0
	ldr	r3, .L22
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	cmp	r2, r3
	bne	.L17
	.loc 1 574 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L17:
	.loc 1 578 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L18
	.loc 1 578 0 is_stmt 0 discriminator 1
	ldr	r3, .L22
	ldr	r3, [r3, #0]
	b	.L19
.L18:
	.loc 1 578 0 discriminator 2
	ldr	r3, [fp, #-12]
.L19:
	.loc 1 578 0 discriminator 3
	str	r3, [fp, #-8]
	.loc 1 584 0 is_stmt 1 discriminator 3
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 587 0 discriminator 3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	cmp	r3, #0
	beq	.L20
	.loc 1 589 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #24
	mov	r0, r3
	bl	vListRemove
.L20:
	.loc 1 592 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	ldr	r0, .L22+4
	mov	r1, r3
	bl	vListInsertEnd
	.loc 1 597 0
	ldr	r3, .L22+8
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L22+8
	str	r2, [r3, #0]
	.loc 1 601 0
	ldr	r3, .L22+12
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L22+12
	str	r2, [r3, #0]
	.loc 1 605 0
	bl	vPortExitCritical
	.loc 1 608 0
	ldr	r3, .L22+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L16
	.loc 1 610 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L16
	.loc 1 612 0
@ 612 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
.L16:
	.loc 1 615 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L23:
	.align	2
.L22:
	.word	pxCurrentTCB
	.word	xTasksWaitingTermination
	.word	uxTasksDeleted
	.word	uxTaskNumber
	.word	xSchedulerRunning
.LFE1:
	.size	vTaskDelete, .-vTaskDelete
	.section .rodata
	.align	2
.LC5:
	.ascii	"pxPreviousWakeTime\000"
	.align	2
.LC6:
	.ascii	"( xTimeIncrement > 0U )\000"
	.section	.text.vTaskDelayUntil,"ax",%progbits
	.align	2
	.global	vTaskDelayUntil
	.type	vTaskDelayUntil, %function
vTaskDelayUntil:
.LFB2:
	.loc 1 631 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #20
.LCFI8:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 1 633 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 635 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L25
	.loc 1 635 0 is_stmt 0 discriminator 1
	ldr	r0, .L32
	ldr	r1, .L32+4
	ldr	r2, .L32+8
	bl	__assert
.L25:
	.loc 1 636 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L26
	.loc 1 636 0 is_stmt 0 discriminator 1
	ldr	r0, .L32+12
	ldr	r1, .L32+4
	mov	r2, #636
	bl	__assert
.L26:
	.loc 1 638 0 is_stmt 1
	bl	vTaskSuspendAll
	.loc 1 641 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 643 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #0]
	ldr	r3, .L32+16
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L27
	.loc 1 650 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bls	.L28
	.loc 1 650 0 is_stmt 0 discriminator 1
	ldr	r3, .L32+16
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	cmp	r2, r3
	bls	.L28
	.loc 1 652 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L28
.L27:
	.loc 1 660 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L29
	.loc 1 660 0 is_stmt 0 discriminator 1
	ldr	r3, .L32+16
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	cmp	r2, r3
	bls	.L28
.L29:
	.loc 1 662 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
.L28:
	.loc 1 667 0
	ldr	r3, [fp, #-20]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 669 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L30
	.loc 1 676 0
	ldr	r3, .L32+20
	ldr	r3, [r3, #0]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 677 0
	ldr	r0, [fp, #-12]
	bl	prvAddCurrentTaskToDelayedList
.L30:
	.loc 1 680 0
	bl	xTaskResumeAll
	str	r0, [fp, #-16]
	.loc 1 684 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L24
	.loc 1 686 0
@ 686 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
.L24:
	.loc 1 688 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L33:
	.align	2
.L32:
	.word	.LC5
	.word	.LC1
	.word	635
	.word	.LC6
	.word	xTickCount
	.word	pxCurrentTCB
.LFE2:
	.size	vTaskDelayUntil, .-vTaskDelayUntil
	.section	.text.vTaskDelay,"ax",%progbits
	.align	2
	.global	vTaskDelay
	.type	vTaskDelay, %function
vTaskDelay:
.LFB3:
	.loc 1 696 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #12
.LCFI11:
	str	r0, [fp, #-16]
	.loc 1 698 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 701 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L35
	.loc 1 703 0
	bl	vTaskSuspendAll
	.loc 1 717 0
	ldr	r3, .L37
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 722 0
	ldr	r3, .L37+4
	ldr	r3, [r3, #0]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 723 0
	ldr	r0, [fp, #-12]
	bl	prvAddCurrentTaskToDelayedList
	.loc 1 725 0
	bl	xTaskResumeAll
	str	r0, [fp, #-8]
.L35:
	.loc 1 730 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L34
	.loc 1 732 0
@ 732 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
.L34:
	.loc 1 734 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L38:
	.align	2
.L37:
	.word	xTickCount
	.word	pxCurrentTCB
.LFE3:
	.size	vTaskDelay, .-vTaskDelay
	.section	.text.uxTaskPriorityGet,"ax",%progbits
	.align	2
	.global	uxTaskPriorityGet
	.type	uxTaskPriorityGet, %function
uxTaskPriorityGet:
.LFB4:
	.loc 1 742 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #12
.LCFI14:
	str	r0, [fp, #-16]
	.loc 1 746 0
	bl	vPortEnterCritical
	.loc 1 750 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L40
	.loc 1 750 0 is_stmt 0 discriminator 1
	ldr	r3, .L42
	ldr	r3, [r3, #0]
	b	.L41
.L40:
	.loc 1 750 0 discriminator 2
	ldr	r3, [fp, #-16]
.L41:
	.loc 1 750 0 discriminator 3
	str	r3, [fp, #-8]
	.loc 1 751 0 is_stmt 1 discriminator 3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #44]
	str	r3, [fp, #-12]
	.loc 1 753 0 discriminator 3
	bl	vPortExitCritical
	.loc 1 755 0 discriminator 3
	ldr	r3, [fp, #-12]
	.loc 1 756 0 discriminator 3
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L43:
	.align	2
.L42:
	.word	pxCurrentTCB
.LFE4:
	.size	uxTaskPriorityGet, .-uxTaskPriorityGet
	.section .rodata
	.align	2
.LC7:
	.ascii	"( uxNewPriority < 16 )\000"
	.section	.text.vTaskPrioritySet,"ax",%progbits
	.align	2
	.global	vTaskPrioritySet
	.type	vTaskPrioritySet, %function
vTaskPrioritySet:
.LFB5:
	.loc 1 764 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #20
.LCFI17:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 1 767 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 769 0
	ldr	r3, [fp, #-24]
	cmp	r3, #15
	bls	.L45
	.loc 1 769 0 is_stmt 0 discriminator 1
	ldr	r0, .L56
	ldr	r1, .L56+4
	ldr	r2, .L56+8
	bl	__assert
.L45:
	.loc 1 772 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #15
	bls	.L46
	.loc 1 774 0
	mov	r3, #15
	str	r3, [fp, #-24]
.L46:
	.loc 1 777 0
	bl	vPortEnterCritical
	.loc 1 779 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-20]
	cmp	r2, r3
	bne	.L47
	.loc 1 781 0
	mov	r3, #0
	str	r3, [fp, #-20]
.L47:
	.loc 1 786 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L48
	.loc 1 786 0 is_stmt 0 discriminator 1
	ldr	r3, .L56+12
	ldr	r3, [r3, #0]
	b	.L49
.L48:
	.loc 1 786 0 discriminator 2
	ldr	r3, [fp, #-20]
.L49:
	.loc 1 786 0 discriminator 3
	str	r3, [fp, #-12]
	.loc 1 792 0 is_stmt 1 discriminator 3
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #84]
	str	r3, [fp, #-16]
	.loc 1 800 0 discriminator 3
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	beq	.L50
	.loc 1 804 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bls	.L51
	.loc 1 806 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L52
	.loc 1 812 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L52
.L51:
	.loc 1 815 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L52
	.loc 1 819 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L52:
	.loc 1 828 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #84]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #44]
	cmp	r2, r3
	bne	.L53
	.loc 1 830 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-24]
	str	r2, [r3, #44]
.L53:
	.loc 1 834 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-24]
	str	r2, [r3, #84]
	.loc 1 842 0
	ldr	r3, [fp, #-24]
	rsb	r2, r3, #16
	ldr	r3, [fp, #-12]
	str	r2, [r3, #24]
	.loc 1 848 0
	ldr	r3, [fp, #-12]
	ldr	r1, [r3, #20]
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L56+16
	add	r3, r2, r3
	cmp	r1, r3
	bne	.L54
	.loc 1 853 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 854 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L56+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L55
	.loc 1 854 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L56+20
	str	r2, [r3, #0]
.L55:
	.loc 1 854 0 discriminator 2
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L56+16
	add	r2, r2, r3
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsertEnd
.L54:
	.loc 1 857 0 is_stmt 1
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L50
	.loc 1 859 0
@ 859 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
.L50:
	.loc 1 863 0
	bl	vPortExitCritical
	.loc 1 864 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L57:
	.align	2
.L56:
	.word	.LC7
	.word	.LC1
	.word	769
	.word	pxCurrentTCB
	.word	pxReadyTasksLists
	.word	uxTopReadyPriority
.LFE5:
	.size	vTaskPrioritySet, .-vTaskPrioritySet
	.section	.text.vTaskSuspend,"ax",%progbits
	.align	2
	.global	vTaskSuspend
	.type	vTaskSuspend, %function
vTaskSuspend:
.LFB6:
	.loc 1 872 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #8
.LCFI20:
	str	r0, [fp, #-12]
	.loc 1 875 0
	bl	vPortEnterCritical
	.loc 1 879 0
	ldr	r3, .L66
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	cmp	r2, r3
	bne	.L59
	.loc 1 881 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L59:
	.loc 1 885 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L60
	.loc 1 885 0 is_stmt 0 discriminator 1
	ldr	r3, .L66
	ldr	r3, [r3, #0]
	b	.L61
.L60:
	.loc 1 885 0 discriminator 2
	ldr	r3, [fp, #-12]
.L61:
	.loc 1 885 0 discriminator 3
	str	r3, [fp, #-8]
	.loc 1 890 0 is_stmt 1 discriminator 3
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 893 0 discriminator 3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	cmp	r3, #0
	beq	.L62
	.loc 1 895 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #24
	mov	r0, r3
	bl	vListRemove
.L62:
	.loc 1 898 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	ldr	r0, .L66+4
	mov	r1, r3
	bl	vListInsertEnd
	.loc 1 900 0
	bl	vPortExitCritical
	.loc 1 902 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L58
	.loc 1 904 0
	ldr	r3, .L66+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L64
	.loc 1 907 0
@ 907 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
	b	.L58
.L64:
	.loc 1 914 0
	ldr	r3, .L66+4
	ldr	r2, [r3, #0]
	ldr	r3, .L66+12
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L65
	.loc 1 920 0
	ldr	r3, .L66
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L58
.L65:
	.loc 1 924 0
	bl	vTaskSwitchContext
.L58:
	.loc 1 928 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L67:
	.align	2
.L66:
	.word	pxCurrentTCB
	.word	xSuspendedTaskList
	.word	xSchedulerRunning
	.word	uxCurrentNumberOfTasks
.LFE6:
	.size	vTaskSuspend, .-vTaskSuspend
	.section .rodata
	.align	2
.LC8:
	.ascii	"xTask\000"
	.section	.text.xTaskIsTaskSuspended,"ax",%progbits
	.align	2
	.global	xTaskIsTaskSuspended
	.type	xTaskIsTaskSuspended, %function
xTaskIsTaskSuspended:
.LFB7:
	.loc 1 936 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #12
.LCFI23:
	str	r0, [fp, #-16]
	.loc 1 937 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 938 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-12]
	.loc 1 941 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L69
	.loc 1 941 0 is_stmt 0 discriminator 1
	ldr	r0, .L71
	ldr	r1, .L71+4
	ldr	r2, .L71+8
	bl	__assert
.L69:
	.loc 1 945 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #20]
	ldr	r3, .L71+12
	cmp	r2, r3
	bne	.L70
	.loc 1 948 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #40]
	ldr	r3, .L71+16
	cmp	r2, r3
	beq	.L70
	.loc 1 954 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	cmp	r3, #0
	bne	.L70
	.loc 1 956 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L70:
	.loc 1 961 0
	ldr	r3, [fp, #-8]
	.loc 1 962 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L72:
	.align	2
.L71:
	.word	.LC8
	.word	.LC1
	.word	941
	.word	xSuspendedTaskList
	.word	xPendingReadyList
.LFE7:
	.size	xTaskIsTaskSuspended, .-xTaskIsTaskSuspended
	.section .rodata
	.align	2
.LC9:
	.ascii	"pxTaskToResume\000"
	.section	.text.vTaskResume,"ax",%progbits
	.align	2
	.global	vTaskResume
	.type	vTaskResume, %function
vTaskResume:
.LFB8:
	.loc 1 970 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #8
.LCFI26:
	str	r0, [fp, #-12]
	.loc 1 974 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L74
	.loc 1 974 0 is_stmt 0 discriminator 1
	ldr	r0, .L78
	ldr	r1, .L78+4
	ldr	r2, .L78+8
	bl	__assert
.L74:
	.loc 1 978 0 is_stmt 1
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 982 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L73
	.loc 1 982 0 is_stmt 0 discriminator 1
	ldr	r3, .L78+12
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	beq	.L73
	.loc 1 984 0 is_stmt 1
	bl	vPortEnterCritical
	.loc 1 986 0
	ldr	r0, [fp, #-8]
	bl	xTaskIsTaskSuspended
	mov	r3, r0
	cmp	r3, #1
	bne	.L76
	.loc 1 992 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 993 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r3, .L78+16
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L77
	.loc 1 993 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r3, .L78+16
	str	r2, [r3, #0]
.L77:
	.loc 1 993 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L78+20
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsertEnd
	.loc 1 996 0 is_stmt 1 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r3, .L78+12
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	cmp	r2, r3
	bcc	.L76
	.loc 1 1000 0
@ 1000 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
.L76:
	.loc 1 1004 0
	bl	vPortExitCritical
.L73:
	.loc 1 1006 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L79:
	.align	2
.L78:
	.word	.LC9
	.word	.LC1
	.word	974
	.word	pxCurrentTCB
	.word	uxTopReadyPriority
	.word	pxReadyTasksLists
.LFE8:
	.size	vTaskResume, .-vTaskResume
	.section	.text.xTaskResumeFromISR,"ax",%progbits
	.align	2
	.global	xTaskResumeFromISR
	.type	xTaskResumeFromISR, %function
xTaskResumeFromISR:
.LFB9:
	.loc 1 1015 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #16
.LCFI29:
	str	r0, [fp, #-20]
	.loc 1 1016 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1020 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L81
	.loc 1 1020 0 is_stmt 0 discriminator 1
	ldr	r0, .L85
	ldr	r1, .L85+4
	mov	r2, #1020
	bl	__assert
.L81:
	.loc 1 1022 0 is_stmt 1
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-12]
	.loc 1 1024 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1026 0
	ldr	r0, [fp, #-12]
	bl	xTaskIsTaskSuspended
	mov	r3, r0
	cmp	r3, #1
	bne	.L82
	.loc 1 1030 0
	ldr	r3, .L85+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L83
	.loc 1 1032 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L85+12
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	cmp	r2, r3
	movcc	r3, #0
	movcs	r3, #1
	str	r3, [fp, #-8]
	.loc 1 1033 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 1034 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L85+16
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L84
	.loc 1 1034 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L85+16
	str	r2, [r3, #0]
.L84:
	.loc 1 1034 0 discriminator 2
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L85+20
	add	r2, r2, r3
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsertEnd
	b	.L82
.L83:
	.loc 1 1041 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #24
	ldr	r0, .L85+24
	mov	r1, r3
	bl	vListInsertEnd
.L82:
	.loc 1 1047 0
	ldr	r3, [fp, #-8]
	.loc 1 1048 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L86:
	.align	2
.L85:
	.word	.LC9
	.word	.LC1
	.word	uxSchedulerSuspended
	.word	pxCurrentTCB
	.word	uxTopReadyPriority
	.word	pxReadyTasksLists
	.word	xPendingReadyList
.LFE9:
	.size	xTaskResumeFromISR, .-xTaskResumeFromISR
	.section .rodata
	.align	2
.LC10:
	.ascii	"IDLE\000"
	.align	2
.LC11:
	.ascii	"xReturn\000"
	.section	.text.vTaskStartScheduler,"ax",%progbits
	.align	2
	.global	vTaskStartScheduler
	.type	vTaskStartScheduler, %function
vTaskStartScheduler:
.LFB10:
	.loc 1 1061 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #20
.LCFI32:
	.loc 1 1074 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r3, #0
	str	r3, [sp, #12]
	ldr	r0, .L91
	ldr	r1, .L91+4
	mov	r2, #256
	mov	r3, #0
	bl	xTaskGenericCreate
	str	r0, [fp, #-8]
	.loc 1 1080 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L88
	.loc 1 1082 0
	bl	xTimerCreateTimerTask
	str	r0, [fp, #-8]
.L88:
	.loc 1 1087 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L89
	.loc 1 1097 0
@ 1097 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	STMDB	SP!, {R0}		
	MRS	R0, CPSR		
	ORR	R0, R0, #0xC0	
	MSR	CPSR, R0		
	LDMIA	SP!, {R0}			
@ 0 "" 2
	.loc 1 1099 0
	ldr	r3, .L91+8
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1100 0
	ldr	r3, .L91+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1105 0
	bl	vConfigureTimerForRunTimeStats
	.loc 1 1109 0
	bl	xPortStartScheduler
.L89:
	.loc 1 1121 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L87
	.loc 1 1121 0 is_stmt 0 discriminator 1
	ldr	r0, .L91+16
	ldr	r1, .L91+20
	ldr	r2, .L91+24
	bl	__assert
.L87:
	.loc 1 1122 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L92:
	.align	2
.L91:
	.word	prvIdleTask
	.word	.LC10
	.word	xSchedulerRunning
	.word	xTickCount
	.word	.LC11
	.word	.LC1
	.word	1121
.LFE10:
	.size	vTaskStartScheduler, .-vTaskStartScheduler
	.section	.text.vTaskEndScheduler,"ax",%progbits
	.align	2
	.global	vTaskEndScheduler
	.type	vTaskEndScheduler, %function
vTaskEndScheduler:
.LFB11:
	.loc 1 1126 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	.loc 1 1130 0
@ 1130 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	STMDB	SP!, {R0}		
	MRS	R0, CPSR		
	ORR	R0, R0, #0xC0	
	MSR	CPSR, R0		
	LDMIA	SP!, {R0}			
@ 0 "" 2
	.loc 1 1131 0
	ldr	r3, .L94
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1132 0
	bl	vPortEndScheduler
	.loc 1 1133 0
	ldmfd	sp!, {fp, pc}
.L95:
	.align	2
.L94:
	.word	xSchedulerRunning
.LFE11:
	.size	vTaskEndScheduler, .-vTaskEndScheduler
	.section	.text.vTaskSuspendAll,"ax",%progbits
	.align	2
	.global	vTaskSuspendAll
	.type	vTaskSuspendAll, %function
vTaskSuspendAll:
.LFB12:
	.loc 1 1137 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI35:
	add	fp, sp, #0
.LCFI36:
	.loc 1 1140 0
	ldr	r3, .L97
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L97
	str	r2, [r3, #0]
	.loc 1 1141 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L98:
	.align	2
.L97:
	.word	uxSchedulerSuspended
.LFE12:
	.size	vTaskSuspendAll, .-vTaskSuspendAll
	.section .rodata
	.align	2
.LC12:
	.ascii	"uxSchedulerSuspended\000"
	.section	.text.xTaskResumeAll,"ax",%progbits
	.align	2
	.global	xTaskResumeAll
	.type	xTaskResumeAll, %function
xTaskResumeAll:
.LFB13:
	.loc 1 1145 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI37:
	add	fp, sp, #8
.LCFI38:
	sub	sp, sp, #8
.LCFI39:
	.loc 1 1147 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 1151 0
	ldr	r3, .L109
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L100
	.loc 1 1151 0 is_stmt 0 discriminator 1
	ldr	r0, .L109+4
	ldr	r1, .L109+8
	ldr	r2, .L109+12
	bl	__assert
.L100:
	.loc 1 1158 0 is_stmt 1
	bl	vPortEnterCritical
	.loc 1 1160 0
	ldr	r3, .L109
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L109
	str	r2, [r3, #0]
	.loc 1 1162 0
	ldr	r3, .L109
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L101
	.loc 1 1164 0
	ldr	r3, .L109+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L101
.LBB3:
	.loc 1 1166 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1170 0
	b	.L102
.L104:
	.loc 1 1172 0
	ldr	r3, .L109+20
	ldr	r3, [r3, #12]
	ldr	r4, [r3, #12]
	.loc 1 1173 0
	add	r3, r4, #24
	mov	r0, r3
	bl	vListRemove
	.loc 1 1174 0
	add	r3, r4, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 1175 0
	ldr	r2, [r4, #44]
	ldr	r3, .L109+24
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L103
	.loc 1 1175 0 is_stmt 0 discriminator 1
	ldr	r2, [r4, #44]
	ldr	r3, .L109+24
	str	r2, [r3, #0]
.L103:
	.loc 1 1175 0 discriminator 2
	ldr	r2, [r4, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L109+28
	add	r2, r2, r3
	add	r3, r4, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsertEnd
	.loc 1 1179 0 is_stmt 1 discriminator 2
	ldr	r2, [r4, #44]
	ldr	r3, .L109+32
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	cmp	r2, r3
	bcc	.L102
	.loc 1 1181 0
	mov	r3, #1
	str	r3, [fp, #-16]
.L102:
	.loc 1 1170 0 discriminator 1
	ldr	r3, .L109+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L104
	.loc 1 1188 0
	ldr	r3, .L109+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L105
	.loc 1 1190 0
	b	.L106
.L107:
	.loc 1 1192 0
	bl	vTaskIncrementTick
	.loc 1 1193 0
	ldr	r3, .L109+36
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L109+36
	str	r2, [r3, #0]
.L106:
	.loc 1 1190 0 discriminator 1
	ldr	r3, .L109+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L107
	.loc 1 1201 0
	mov	r3, #1
	str	r3, [fp, #-16]
.L105:
	.loc 1 1206 0
	ldr	r3, [fp, #-16]
	cmp	r3, #1
	beq	.L108
	.loc 1 1206 0 is_stmt 0 discriminator 1
	ldr	r3, .L109+40
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L101
.L108:
	.loc 1 1208 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 1209 0
	ldr	r3, .L109+40
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1210 0
@ 1210 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
.L101:
.LBE3:
	.loc 1 1215 0
	bl	vPortExitCritical
	.loc 1 1217 0
	ldr	r3, [fp, #-12]
	.loc 1 1218 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L110:
	.align	2
.L109:
	.word	uxSchedulerSuspended
	.word	.LC12
	.word	.LC1
	.word	1151
	.word	uxCurrentNumberOfTasks
	.word	xPendingReadyList
	.word	uxTopReadyPriority
	.word	pxReadyTasksLists
	.word	pxCurrentTCB
	.word	uxMissedTicks
	.word	xMissedYield
.LFE13:
	.size	xTaskResumeAll, .-xTaskResumeAll
	.section	.text.xTaskGetTickCount,"ax",%progbits
	.align	2
	.global	xTaskGetTickCount
	.type	xTaskGetTickCount, %function
xTaskGetTickCount:
.LFB14:
	.loc 1 1232 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI40:
	add	fp, sp, #4
.LCFI41:
	sub	sp, sp, #4
.LCFI42:
	.loc 1 1236 0
	bl	vPortEnterCritical
	.loc 1 1238 0
	ldr	r3, .L112
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 1240 0
	bl	vPortExitCritical
	.loc 1 1242 0
	ldr	r3, [fp, #-8]
	.loc 1 1243 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L113:
	.align	2
.L112:
	.word	xTickCount
.LFE14:
	.size	xTaskGetTickCount, .-xTaskGetTickCount
	.section	.text.xTaskGetTickCountFromISR,"ax",%progbits
	.align	2
	.global	xTaskGetTickCountFromISR
	.type	xTaskGetTickCountFromISR, %function
xTaskGetTickCountFromISR:
.LFB15:
	.loc 1 1247 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI43:
	add	fp, sp, #0
.LCFI44:
	sub	sp, sp, #8
.LCFI45:
	.loc 1 1251 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 1252 0
	ldr	r3, .L115
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 1255 0
	ldr	r3, [fp, #-8]
	.loc 1 1256 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L116:
	.align	2
.L115:
	.word	xTickCount
.LFE15:
	.size	xTaskGetTickCountFromISR, .-xTaskGetTickCountFromISR
	.section	.text.uxTaskGetNumberOfTasks,"ax",%progbits
	.align	2
	.global	uxTaskGetNumberOfTasks
	.type	uxTaskGetNumberOfTasks, %function
uxTaskGetNumberOfTasks:
.LFB16:
	.loc 1 1260 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI46:
	add	fp, sp, #0
.LCFI47:
	.loc 1 1263 0
	ldr	r3, .L118
	ldr	r3, [r3, #0]
	.loc 1 1264 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L119:
	.align	2
.L118:
	.word	uxCurrentNumberOfTasks
.LFE16:
	.size	uxTaskGetNumberOfTasks, .-uxTaskGetNumberOfTasks
	.section .rodata
	.align	2
.LC13:
	.ascii	"pxTCB\000"
	.section	.text.pcTaskGetTaskName,"ax",%progbits
	.align	2
	.global	pcTaskGetTaskName
	.type	pcTaskGetTaskName, %function
pcTaskGetTaskName:
.LFB17:
	.loc 1 1270 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI48:
	add	fp, sp, #4
.LCFI49:
	sub	sp, sp, #8
.LCFI50:
	str	r0, [fp, #-12]
	.loc 1 1274 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L121
	.loc 1 1274 0 is_stmt 0 discriminator 1
	ldr	r3, .L124
	ldr	r3, [r3, #0]
	b	.L122
.L121:
	.loc 1 1274 0 discriminator 2
	ldr	r3, [fp, #-12]
.L122:
	.loc 1 1274 0 discriminator 3
	str	r3, [fp, #-8]
	.loc 1 1275 0 is_stmt 1 discriminator 3
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L123
	.loc 1 1275 0 is_stmt 0 discriminator 1
	ldr	r0, .L124+4
	ldr	r1, .L124+8
	ldr	r2, .L124+12
	bl	__assert
.L123:
	.loc 1 1276 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r3, r3, #52
	.loc 1 1277 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L125:
	.align	2
.L124:
	.word	pxCurrentTCB
	.word	.LC13
	.word	.LC1
	.word	1275
.LFE17:
	.size	pcTaskGetTaskName, .-pcTaskGetTaskName
	.section	.text.vTaskGetRunTimeStats,"ax",%progbits
	.align	2
	.global	vTaskGetRunTimeStats
	.type	vTaskGetRunTimeStats, %function
vTaskGetRunTimeStats:
.LFB18:
	.loc 1 1351 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI51:
	add	fp, sp, #4
.LCFI52:
	sub	sp, sp, #12
.LCFI53:
	str	r0, [fp, #-16]
	.loc 1 1358 0
	bl	vTaskSuspendAll
	.loc 1 1363 0
	bl	return_run_timer_counter_value
	str	r0, [fp, #-12]
	.loc 1 1368 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L133
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #5
	str	r3, [fp, #-12]
	.loc 1 1374 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	strb	r2, [r3, #0]
	.loc 1 1377 0
	ldr	r3, .L133+4
	ldr	r3, [r3, #0]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L128:
	.loc 1 1381 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 1383 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L133+8
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L127
	.loc 1 1385 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L133+8
	add	r3, r2, r3
	ldr	r0, [fp, #-16]
	mov	r1, r3
	ldr	r2, [fp, #-12]
	bl	prvGenerateRunTimeStatsForTasksInList
.L127:
	.loc 1 1387 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L128
	.loc 1 1389 0
	ldr	r3, .L133+12
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L129
	.loc 1 1391 0
	ldr	r3, .L133+12
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-16]
	mov	r1, r3
	ldr	r2, [fp, #-12]
	bl	prvGenerateRunTimeStatsForTasksInList
.L129:
	.loc 1 1394 0
	ldr	r3, .L133+16
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L130
	.loc 1 1396 0
	ldr	r3, .L133+16
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-16]
	mov	r1, r3
	ldr	r2, [fp, #-12]
	bl	prvGenerateRunTimeStatsForTasksInList
.L130:
	.loc 1 1401 0
	ldr	r3, .L133+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L131
	.loc 1 1403 0
	ldr	r0, [fp, #-16]
	ldr	r1, .L133+20
	ldr	r2, [fp, #-12]
	bl	prvGenerateRunTimeStatsForTasksInList
.L131:
	.loc 1 1410 0
	ldr	r3, .L133+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L132
	.loc 1 1412 0
	ldr	r0, [fp, #-16]
	ldr	r1, .L133+24
	ldr	r2, [fp, #-12]
	bl	prvGenerateRunTimeStatsForTasksInList
.L132:
	.loc 1 1417 0
	bl	xTaskResumeAll
	.loc 1 1418 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L134:
	.align	2
.L133:
	.word	1374389535
	.word	uxTopUsedPriority
	.word	pxReadyTasksLists
	.word	pxDelayedTaskList
	.word	pxOverflowDelayedTaskList
	.word	xTasksWaitingTermination
	.word	xSuspendedTaskList
.LFE18:
	.size	vTaskGetRunTimeStats, .-vTaskGetRunTimeStats
	.section .rodata
	.align	2
.LC14:
	.ascii	"( ( ( pxDelayedTaskList )->uxNumberOfItems == ( uns"
	.ascii	"igned long ) 0 ) )\000"
	.section	.text.vTaskIncrementTick,"ax",%progbits
	.align	2
	.global	vTaskIncrementTick
	.type	vTaskIncrementTick, %function
vTaskIncrementTick:
.LFB19:
	.loc 1 1599 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI54:
	add	fp, sp, #4
.LCFI55:
	sub	sp, sp, #12
.LCFI56:
	.loc 1 1605 0
	ldr	r3, .L147
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L136
	.loc 1 1607 0
	ldr	r3, .L147+4
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L147+4
	str	r2, [r3, #0]
	.loc 1 1608 0
	ldr	r3, .L147+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L137
.LBB4:
	.loc 1 1615 0
	ldr	r3, .L147+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L138
	.loc 1 1615 0 is_stmt 0 discriminator 1
	ldr	r0, .L147+12
	ldr	r1, .L147+16
	ldr	r2, .L147+20
	bl	__assert
.L138:
	.loc 1 1617 0 is_stmt 1
	ldr	r3, .L147+8
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 1618 0
	ldr	r3, .L147+24
	ldr	r2, [r3, #0]
	ldr	r3, .L147+8
	str	r2, [r3, #0]
	.loc 1 1619 0
	ldr	r3, .L147+24
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 1620 0
	ldr	r3, .L147+28
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L147+28
	str	r2, [r3, #0]
	.loc 1 1622 0
	ldr	r3, .L147+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L139
	.loc 1 1629 0
	ldr	r3, .L147+32
	mvn	r2, #0
	str	r2, [r3, #0]
	b	.L137
.L139:
	.loc 1 1637 0
	ldr	r3, .L147+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #12]
	ldr	r3, [r3, #12]
	str	r3, [fp, #-12]
	.loc 1 1638 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #4]
	ldr	r3, .L147+32
	str	r2, [r3, #0]
.L137:
.LBE4:
.LBB5:
	.loc 1 1643 0
	ldr	r3, .L147+4
	ldr	r2, [r3, #0]
	ldr	r3, .L147+32
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcc	.L135
.L145:
	.loc 1 1643 0 is_stmt 0 discriminator 1
	ldr	r3, .L147+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L141
	.loc 1 1643 0 discriminator 2
	ldr	r3, .L147+32
	mvn	r2, #0
	str	r2, [r3, #0]
	b	.L135
.L141:
	.loc 1 1643 0 discriminator 3
	ldr	r3, .L147+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #12]
	ldr	r3, [r3, #12]
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-16]
	ldr	r3, .L147+4
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcs	.L142
	.loc 1 1643 0 discriminator 4
	ldr	r3, .L147+32
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	b	.L135
.L142:
	.loc 1 1643 0 discriminator 5
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	cmp	r3, #0
	beq	.L143
	.loc 1 1643 0 discriminator 6
	ldr	r3, [fp, #-12]
	add	r3, r3, #24
	mov	r0, r3
	bl	vListRemove
.L143:
	.loc 1 1643 0 discriminator 7
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L147+36
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L144
	.loc 1 1643 0 discriminator 8
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L147+36
	str	r2, [r3, #0]
.L144:
	.loc 1 1643 0 discriminator 9
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L147+40
	add	r2, r2, r3
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsertEnd
	b	.L145
.L136:
.LBE5:
	.loc 1 1647 0 is_stmt 1
	ldr	r3, .L147+44
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L147+44
	str	r2, [r3, #0]
.L135:
	.loc 1 1670 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L148:
	.align	2
.L147:
	.word	uxSchedulerSuspended
	.word	xTickCount
	.word	pxDelayedTaskList
	.word	.LC14
	.word	.LC1
	.word	1615
	.word	pxOverflowDelayedTaskList
	.word	xNumOfOverflows
	.word	xNextTaskUnblockTime
	.word	uxTopReadyPriority
	.word	pxReadyTasksLists
	.word	uxMissedTicks
.LFE19:
	.size	vTaskIncrementTick, .-vTaskIncrementTick
	.section	.text.vTaskSetApplicationTaskTag,"ax",%progbits
	.align	2
	.global	vTaskSetApplicationTaskTag
	.type	vTaskSetApplicationTaskTag, %function
vTaskSetApplicationTaskTag:
.LFB20:
	.loc 1 1676 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI57:
	add	fp, sp, #4
.LCFI58:
	sub	sp, sp, #12
.LCFI59:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 1680 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L150
	.loc 1 1682 0
	ldr	r3, .L152
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L151
.L150:
	.loc 1 1686 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
.L151:
	.loc 1 1691 0
	bl	vPortEnterCritical
	.loc 1 1692 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #88]
	.loc 1 1693 0
	bl	vPortExitCritical
	.loc 1 1694 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L153:
	.align	2
.L152:
	.word	pxCurrentTCB
.LFE20:
	.size	vTaskSetApplicationTaskTag, .-vTaskSetApplicationTaskTag
	.section	.text.xTaskGetApplicationTaskTag,"ax",%progbits
	.align	2
	.global	xTaskGetApplicationTaskTag
	.type	xTaskGetApplicationTaskTag, %function
xTaskGetApplicationTaskTag:
.LFB21:
	.loc 1 1702 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI60:
	add	fp, sp, #4
.LCFI61:
	sub	sp, sp, #12
.LCFI62:
	str	r0, [fp, #-16]
	.loc 1 1707 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L155
	.loc 1 1709 0
	ldr	r3, .L157
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L156
.L155:
	.loc 1 1713 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-8]
.L156:
	.loc 1 1718 0
	bl	vPortEnterCritical
	.loc 1 1719 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #88]
	str	r3, [fp, #-12]
	.loc 1 1720 0
	bl	vPortExitCritical
	.loc 1 1722 0
	ldr	r3, [fp, #-12]
	.loc 1 1723 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L158:
	.align	2
.L157:
	.word	pxCurrentTCB
.LFE21:
	.size	xTaskGetApplicationTaskTag, .-xTaskGetApplicationTaskTag
	.section	.text.xTaskCallApplicationTaskHook,"ax",%progbits
	.align	2
	.global	xTaskCallApplicationTaskHook
	.type	xTaskCallApplicationTaskHook, %function
xTaskCallApplicationTaskHook:
.LFB22:
	.loc 1 1731 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI63:
	add	fp, sp, #4
.LCFI64:
	sub	sp, sp, #16
.LCFI65:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 1736 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L160
	.loc 1 1738 0
	ldr	r3, .L164
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L161
.L160:
	.loc 1 1742 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-8]
.L161:
	.loc 1 1745 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L162
	.loc 1 1747 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #88]
	ldr	r0, [fp, #-20]
	blx	r3
	str	r0, [fp, #-12]
	b	.L163
.L162:
	.loc 1 1751 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L163:
	.loc 1 1754 0
	ldr	r3, [fp, #-12]
	.loc 1 1755 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L165:
	.align	2
.L164:
	.word	pxCurrentTCB
.LFE22:
	.size	xTaskCallApplicationTaskHook, .-xTaskCallApplicationTaskHook
	.section .rodata
	.align	2
.LC15:
	.ascii	"uxTopReadyPriority\000"
	.section	.text.vTaskSwitchContext,"ax",%progbits
	.align	2
	.global	vTaskSwitchContext
	.type	vTaskSwitchContext, %function
vTaskSwitchContext:
.LFB23:
	.loc 1 1761 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI66:
	add	fp, sp, #4
.LCFI67:
	sub	sp, sp, #8
.LCFI68:
	.loc 1 1762 0
	ldr	r3, .L177
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L167
	.loc 1 1766 0
	ldr	r3, .L177+4
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L166
.L167:
	.loc 1 1770 0
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L169
.LBB6:
	.loc 1 1770 0 is_stmt 0 discriminator 1
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #88]
	mov	r0, r3
	bl	vPortSaveVFPRegisters
.L169:
.LBE6:
.LBB7:
	.loc 1 1779 0 is_stmt 1
	bl	return_run_timer_counter_value
	str	r0, [fp, #-8]
	.loc 1 1787 0
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	ldr	r1, [r3, #92]
	ldr	r2, .L177+12
	ldr	r2, [r2, #0]
	ldr	r0, [fp, #-8]
	rsb	r2, r2, r0
	add	r2, r1, r2
	str	r2, [r3, #92]
	.loc 1 1788 0
	ldr	r3, .L177+12
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
.LBE7:
	.loc 1 1792 0
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #48]
	cmp	r2, r3
	bhi	.L170
	.loc 1 1792 0 is_stmt 0 discriminator 1
	ldr	r3, .L177+8
	ldr	r2, [r3, #0]
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	add	r3, r3, #52
	mov	r0, r2
	mov	r1, r3
	bl	vApplicationStackOverflowHook
.L170:
.LBB8:
	.loc 1 1793 0 is_stmt 1
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #48]
	mov	r0, r3
	ldr	r1, .L177+16
	mov	r2, #20
	bl	memcmp
	mov	r3, r0
	cmp	r3, #0
	beq	.L176
	.loc 1 1793 0 is_stmt 0 discriminator 1
	ldr	r3, .L177+8
	ldr	r2, [r3, #0]
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	add	r3, r3, #52
	mov	r0, r2
	mov	r1, r3
	bl	vApplicationStackOverflowHook
.LBE8:
	.loc 1 1796 0 is_stmt 1 discriminator 1
	b	.L176
.L174:
	.loc 1 1798 0
	ldr	r3, .L177+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L173
	.loc 1 1798 0 is_stmt 0 discriminator 1
	ldr	r0, .L177+24
	ldr	r1, .L177+28
	ldr	r2, .L177+32
	bl	__assert
.L173:
	.loc 1 1799 0 is_stmt 1
	ldr	r3, .L177+20
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L177+20
	str	r2, [r3, #0]
	b	.L172
.L176:
	.loc 1 1796 0
	mov	r0, r0	@ nop
.L172:
	.loc 1 1796 0 is_stmt 0 discriminator 1
	ldr	r3, .L177+20
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L177+36
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L174
.LBB9:
	.loc 1 1804 0 is_stmt 1
	ldr	r3, .L177+20
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L177+36
	add	r3, r2, r3
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #4]
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-12]
	add	r3, r3, #8
	cmp	r2, r3
	bne	.L175
	.loc 1 1804 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #4]
.L175:
	.loc 1 1804 0 discriminator 2
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #12]
	ldr	r3, .L177+8
	str	r2, [r3, #0]
.LBE9:
	.loc 1 1806 0 is_stmt 1 discriminator 2
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L166
.LBB10:
	.loc 1 1806 0 is_stmt 0 discriminator 1
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #88]
	mov	r0, r3
	bl	vPortRestoreVFPRegisters
.L166:
.LBE10:
	.loc 1 1808 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L178:
	.align	2
.L177:
	.word	uxSchedulerSuspended
	.word	xMissedYield
	.word	pxCurrentTCB
	.word	ulTaskSwitchedInTime
	.word	ucExpectedStackBytes.1602
	.word	uxTopReadyPriority
	.word	.LC15
	.word	.LC1
	.word	1798
	.word	pxReadyTasksLists
.LFE23:
	.size	vTaskSwitchContext, .-vTaskSwitchContext
	.section .rodata
	.align	2
.LC16:
	.ascii	"pxEventList\000"
	.section	.text.vTaskPlaceOnEventList,"ax",%progbits
	.align	2
	.global	vTaskPlaceOnEventList
	.type	vTaskPlaceOnEventList, %function
vTaskPlaceOnEventList:
.LFB24:
	.loc 1 1812 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI69:
	add	fp, sp, #4
.LCFI70:
	sub	sp, sp, #12
.LCFI71:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 1815 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L180
	.loc 1 1815 0 is_stmt 0 discriminator 1
	ldr	r0, .L183
	ldr	r1, .L183+4
	ldr	r2, .L183+8
	bl	__assert
.L180:
	.loc 1 1823 0 is_stmt 1
	ldr	r3, .L183+12
	ldr	r3, [r3, #0]
	add	r3, r3, #24
	ldr	r0, [fp, #-12]
	mov	r1, r3
	bl	vListInsert
	.loc 1 1828 0
	ldr	r3, .L183+12
	ldr	r3, [r3, #0]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 1833 0
	ldr	r3, [fp, #-16]
	cmn	r3, #1
	bne	.L181
	.loc 1 1838 0
	ldr	r3, .L183+12
	ldr	r3, [r3, #0]
	add	r3, r3, #4
	ldr	r0, .L183+16
	mov	r1, r3
	bl	vListInsertEnd
	b	.L179
.L181:
	.loc 1 1844 0
	ldr	r3, .L183+20
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 1845 0
	ldr	r0, [fp, #-8]
	bl	prvAddCurrentTaskToDelayedList
.L179:
	.loc 1 1856 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L184:
	.align	2
.L183:
	.word	.LC16
	.word	.LC1
	.word	1815
	.word	pxCurrentTCB
	.word	xSuspendedTaskList
	.word	xTickCount
.LFE24:
	.size	vTaskPlaceOnEventList, .-vTaskPlaceOnEventList
	.section	.text.vTaskPlaceOnEventListRestricted,"ax",%progbits
	.align	2
	.global	vTaskPlaceOnEventListRestricted
	.type	vTaskPlaceOnEventListRestricted, %function
vTaskPlaceOnEventListRestricted:
.LFB25:
	.loc 1 1862 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI72:
	add	fp, sp, #4
.LCFI73:
	sub	sp, sp, #12
.LCFI74:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 1865 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L186
	.loc 1 1865 0 is_stmt 0 discriminator 1
	ldr	r0, .L187
	ldr	r1, .L187+4
	ldr	r2, .L187+8
	bl	__assert
.L186:
	.loc 1 1877 0 is_stmt 1
	ldr	r3, .L187+12
	ldr	r3, [r3, #0]
	add	r3, r3, #24
	ldr	r0, [fp, #-12]
	mov	r1, r3
	bl	vListInsertEnd
	.loc 1 1882 0
	ldr	r3, .L187+12
	ldr	r3, [r3, #0]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 1886 0
	ldr	r3, .L187+16
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 1887 0
	ldr	r0, [fp, #-8]
	bl	prvAddCurrentTaskToDelayedList
	.loc 1 1888 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L188:
	.align	2
.L187:
	.word	.LC16
	.word	.LC1
	.word	1865
	.word	pxCurrentTCB
	.word	xTickCount
.LFE25:
	.size	vTaskPlaceOnEventListRestricted, .-vTaskPlaceOnEventListRestricted
	.section .rodata
	.align	2
.LC17:
	.ascii	"pxUnblockedTCB\000"
	.section	.text.xTaskRemoveFromEventList,"ax",%progbits
	.align	2
	.global	xTaskRemoveFromEventList
	.type	xTaskRemoveFromEventList, %function
xTaskRemoveFromEventList:
.LFB26:
	.loc 1 1894 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI75:
	add	fp, sp, #4
.LCFI76:
	sub	sp, sp, #12
.LCFI77:
	str	r0, [fp, #-16]
	.loc 1 1911 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #12]
	ldr	r3, [r3, #12]
	str	r3, [fp, #-12]
	.loc 1 1912 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L190
	.loc 1 1912 0 is_stmt 0 discriminator 1
	ldr	r0, .L196
	ldr	r1, .L196+4
	ldr	r2, .L196+8
	bl	__assert
.L190:
	.loc 1 1913 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #24
	mov	r0, r3
	bl	vListRemove
	.loc 1 1915 0
	ldr	r3, .L196+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L191
	.loc 1 1917 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 1918 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L196+16
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L192
	.loc 1 1918 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L196+16
	str	r2, [r3, #0]
.L192:
	.loc 1 1918 0 discriminator 2
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L196+20
	add	r2, r2, r3
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsertEnd
	b	.L193
.L191:
	.loc 1 1924 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #24
	ldr	r0, .L196+24
	mov	r1, r3
	bl	vListInsertEnd
.L193:
	.loc 1 1927 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r3, .L196+28
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	cmp	r2, r3
	bcc	.L194
	.loc 1 1933 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L195
.L194:
	.loc 1 1937 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L195:
	.loc 1 1940 0
	ldr	r3, [fp, #-8]
	.loc 1 1941 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L197:
	.align	2
.L196:
	.word	.LC17
	.word	.LC1
	.word	1912
	.word	uxSchedulerSuspended
	.word	uxTopReadyPriority
	.word	pxReadyTasksLists
	.word	xPendingReadyList
	.word	pxCurrentTCB
.LFE26:
	.size	xTaskRemoveFromEventList, .-xTaskRemoveFromEventList
	.section .rodata
	.align	2
.LC18:
	.ascii	"pxTimeOut\000"
	.section	.text.vTaskSetTimeOutState,"ax",%progbits
	.align	2
	.global	vTaskSetTimeOutState
	.type	vTaskSetTimeOutState, %function
vTaskSetTimeOutState:
.LFB27:
	.loc 1 1945 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI78:
	add	fp, sp, #4
.LCFI79:
	sub	sp, sp, #4
.LCFI80:
	str	r0, [fp, #-8]
	.loc 1 1946 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L199
	.loc 1 1946 0 is_stmt 0 discriminator 1
	ldr	r0, .L200
	ldr	r1, .L200+4
	ldr	r2, .L200+8
	bl	__assert
.L199:
	.loc 1 1947 0 is_stmt 1
	ldr	r3, .L200+12
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 1948 0
	ldr	r3, .L200+16
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #4]
	.loc 1 1949 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L201:
	.align	2
.L200:
	.word	.LC18
	.word	.LC1
	.word	1946
	.word	xNumOfOverflows
	.word	xTickCount
.LFE27:
	.size	vTaskSetTimeOutState, .-vTaskSetTimeOutState
	.section .rodata
	.align	2
.LC19:
	.ascii	"pxTicksToWait\000"
	.section	.text.xTaskCheckForTimeOut,"ax",%progbits
	.align	2
	.global	xTaskCheckForTimeOut
	.type	xTaskCheckForTimeOut, %function
xTaskCheckForTimeOut:
.LFB28:
	.loc 1 1953 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI81:
	add	fp, sp, #4
.LCFI82:
	sub	sp, sp, #12
.LCFI83:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 1956 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L203
	.loc 1 1956 0 is_stmt 0 discriminator 1
	ldr	r0, .L209
	ldr	r1, .L209+4
	ldr	r2, .L209+8
	bl	__assert
.L203:
	.loc 1 1957 0 is_stmt 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L204
	.loc 1 1957 0 is_stmt 0 discriminator 1
	ldr	r0, .L209+12
	ldr	r1, .L209+4
	ldr	r2, .L209+16
	bl	__assert
.L204:
	.loc 1 1959 0 is_stmt 1
	bl	vPortEnterCritical
	.loc 1 1965 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmn	r3, #1
	bne	.L205
	.loc 1 1967 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L206
.L205:
	.loc 1 1972 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, .L209+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L207
	.loc 1 1972 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #4]
	ldr	r3, .L209+24
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bhi	.L207
	.loc 1 1978 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L206
.L207:
	.loc 1 1980 0
	ldr	r3, .L209+24
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcs	.L208
	.loc 1 1983 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	ldr	r1, [r3, #4]
	ldr	r3, .L209+24
	ldr	r3, [r3, #0]
	rsb	r3, r3, r1
	add	r2, r2, r3
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 1984 0
	ldr	r0, [fp, #-12]
	bl	vTaskSetTimeOutState
	.loc 1 1985 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L206
.L208:
	.loc 1 1989 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L206:
	.loc 1 1992 0
	bl	vPortExitCritical
	.loc 1 1994 0
	ldr	r3, [fp, #-8]
	.loc 1 1995 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L210:
	.align	2
.L209:
	.word	.LC18
	.word	.LC1
	.word	1956
	.word	.LC19
	.word	1957
	.word	xNumOfOverflows
	.word	xTickCount
.LFE28:
	.size	xTaskCheckForTimeOut, .-xTaskCheckForTimeOut
	.section	.text.vTaskMissedYield,"ax",%progbits
	.align	2
	.global	vTaskMissedYield
	.type	vTaskMissedYield, %function
vTaskMissedYield:
.LFB29:
	.loc 1 1999 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI84:
	add	fp, sp, #0
.LCFI85:
	.loc 1 2000 0
	ldr	r3, .L212
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 2001 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L213:
	.align	2
.L212:
	.word	xMissedYield
.LFE29:
	.size	vTaskMissedYield, .-vTaskMissedYield
	.section	.text.prvIdleTask,"ax",%progbits
	.align	2
	.type	prvIdleTask, %function
prvIdleTask:
.LFB30:
	.loc 1 2051 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI86:
	add	fp, sp, #4
.LCFI87:
	sub	sp, sp, #4
.LCFI88:
	str	r0, [fp, #-8]
	b	.L216
.L217:
	.loc 1 2100 0
	mov	r0, r0	@ nop
.L216:
	.loc 1 2058 0
	bl	prvCheckTasksWaitingTermination
	.loc 1 2081 0
	ldr	r3, .L218
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bls	.L217
	.loc 1 2083 0
@ 2083 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
	.loc 1 2100 0
	b	.L217
.L219:
	.align	2
.L218:
	.word	pxReadyTasksLists
.LFE30:
	.size	prvIdleTask, .-prvIdleTask
	.section	.text.prvInitialiseTCBVariables,"ax",%progbits
	.align	2
	.type	prvInitialiseTCBVariables, %function
prvInitialiseTCBVariables:
.LFB31:
	.loc 1 2116 0
	@ args = 4, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI89:
	add	fp, sp, #4
.LCFI90:
	sub	sp, sp, #20
.LCFI91:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	ldr	r3, [fp, #4]
	strh	r3, [fp, #-24]	@ movhi
	.loc 1 2121 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #52
	mov	r0, r3
	ldr	r1, [fp, #-12]
	mov	r2, #32
	bl	strncpy
	.loc 1 2124 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	strb	r2, [r3, #83]
	.loc 1 2128 0
	ldr	r3, [fp, #-16]
	cmp	r3, #15
	bls	.L221
	.loc 1 2130 0
	mov	r3, #15
	str	r3, [fp, #-16]
.L221:
	.loc 1 2133 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #44]
	.loc 1 2136 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #84]
	.loc 1 2140 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListInitialiseItem
	.loc 1 2141 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #24
	mov	r0, r3
	bl	vListInitialiseItem
	.loc 1 2145 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #16]
	.loc 1 2148 0
	ldr	r3, [fp, #-16]
	rsb	r2, r3, #16
	ldr	r3, [fp, #-8]
	str	r2, [r3, #24]
	.loc 1 2149 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #36]
	.loc 1 2159 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #88]
	.loc 1 2165 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #92]
	.loc 1 2179 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE31:
	.size	prvInitialiseTCBVariables, .-prvInitialiseTCBVariables
	.section	.text.prvInitialiseTaskLists,"ax",%progbits
	.align	2
	.type	prvInitialiseTaskLists, %function
prvInitialiseTaskLists:
.LFB32:
	.loc 1 2202 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI92:
	add	fp, sp, #4
.LCFI93:
	sub	sp, sp, #4
.LCFI94:
	.loc 1 2205 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L223
.L224:
	.loc 1 2207 0 discriminator 2
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L225
	add	r3, r2, r3
	mov	r0, r3
	bl	vListInitialise
	.loc 1 2205 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L223:
	.loc 1 2205 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #15
	bls	.L224
	.loc 1 2210 0 is_stmt 1
	ldr	r0, .L225+4
	bl	vListInitialise
	.loc 1 2211 0
	ldr	r0, .L225+8
	bl	vListInitialise
	.loc 1 2212 0
	ldr	r0, .L225+12
	bl	vListInitialise
	.loc 1 2216 0
	ldr	r0, .L225+16
	bl	vListInitialise
	.loc 1 2222 0
	ldr	r0, .L225+20
	bl	vListInitialise
	.loc 1 2228 0
	ldr	r3, .L225+24
	ldr	r2, .L225+4
	str	r2, [r3, #0]
	.loc 1 2229 0
	ldr	r3, .L225+28
	ldr	r2, .L225+8
	str	r2, [r3, #0]
	.loc 1 2230 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L226:
	.align	2
.L225:
	.word	pxReadyTasksLists
	.word	xDelayedTaskList1
	.word	xDelayedTaskList2
	.word	xPendingReadyList
	.word	xTasksWaitingTermination
	.word	xSuspendedTaskList
	.word	pxDelayedTaskList
	.word	pxOverflowDelayedTaskList
.LFE32:
	.size	prvInitialiseTaskLists, .-prvInitialiseTaskLists
	.section	.text.prvCheckTasksWaitingTermination,"ax",%progbits
	.align	2
	.type	prvCheckTasksWaitingTermination, %function
prvCheckTasksWaitingTermination:
.LFB33:
	.loc 1 2234 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI95:
	add	fp, sp, #4
.LCFI96:
	sub	sp, sp, #8
.LCFI97:
.LBB11:
	.loc 1 2241 0
	ldr	r3, .L229
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L227
	.loc 1 2243 0
	bl	vTaskSuspendAll
	.loc 1 2244 0
	ldr	r3, .L229+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	str	r3, [fp, #-8]
	.loc 1 2245 0
	bl	xTaskResumeAll
	.loc 1 2247 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L227
.LBB12:
	.loc 1 2251 0
	bl	vPortEnterCritical
	.loc 1 2253 0
	ldr	r3, .L229+4
	ldr	r3, [r3, #12]
	ldr	r3, [r3, #12]
	str	r3, [fp, #-12]
	.loc 1 2254 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 2255 0
	ldr	r3, .L229+8
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L229+8
	str	r2, [r3, #0]
	.loc 1 2256 0
	ldr	r3, .L229
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L229
	str	r2, [r3, #0]
	.loc 1 2258 0
	bl	vPortExitCritical
	.loc 1 2260 0
	ldr	r0, [fp, #-12]
	bl	prvDeleteTCB
.L227:
.LBE12:
.LBE11:
	.loc 1 2265 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L230:
	.align	2
.L229:
	.word	uxTasksDeleted
	.word	xTasksWaitingTermination
	.word	uxCurrentNumberOfTasks
.LFE33:
	.size	prvCheckTasksWaitingTermination, .-prvCheckTasksWaitingTermination
	.section	.text.prvAddCurrentTaskToDelayedList,"ax",%progbits
	.align	2
	.type	prvAddCurrentTaskToDelayedList, %function
prvAddCurrentTaskToDelayedList:
.LFB34:
	.loc 1 2269 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI98:
	add	fp, sp, #4
.LCFI99:
	sub	sp, sp, #4
.LCFI100:
	str	r0, [fp, #-8]
	.loc 1 2271 0
	ldr	r3, .L234
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #4]
	.loc 1 2273 0
	ldr	r3, .L234+4
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	bcs	.L232
	.loc 1 2276 0
	ldr	r3, .L234+8
	ldr	r2, [r3, #0]
	ldr	r3, .L234
	ldr	r3, [r3, #0]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsert
	b	.L231
.L232:
	.loc 1 2281 0
	ldr	r3, .L234+12
	ldr	r2, [r3, #0]
	ldr	r3, .L234
	ldr	r3, [r3, #0]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsert
	.loc 1 2286 0
	ldr	r3, .L234+16
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	bcs	.L231
	.loc 1 2288 0
	ldr	r3, .L234+16
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
.L231:
	.loc 1 2291 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L235:
	.align	2
.L234:
	.word	pxCurrentTCB
	.word	xTickCount
	.word	pxOverflowDelayedTaskList
	.word	pxDelayedTaskList
	.word	xNextTaskUnblockTime
.LFE34:
	.size	prvAddCurrentTaskToDelayedList, .-prvAddCurrentTaskToDelayedList
	.section	.text.prvAllocateTCBAndStack,"ax",%progbits
	.align	2
	.type	prvAllocateTCBAndStack, %function
prvAllocateTCBAndStack:
.LFB35:
	.loc 1 2295 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI101:
	add	fp, sp, #4
.LCFI102:
	sub	sp, sp, #12
.LCFI103:
	mov	r3, r0
	str	r1, [fp, #-16]
	strh	r3, [fp, #-12]	@ movhi
	.loc 1 2300 0
	mov	r0, #96
	bl	pvPortMalloc
	str	r0, [fp, #-8]
	.loc 1 2302 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L237
	.loc 1 2307 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L238
	.loc 1 2307 0 is_stmt 0 discriminator 1
	ldrh	r3, [fp, #-12]
	mov	r3, r3, asl #2
	mov	r0, r3
	bl	pvPortMalloc
	mov	r3, r0
	b	.L239
.L238:
	.loc 1 2307 0 discriminator 2
	ldr	r3, [fp, #-16]
.L239:
	.loc 1 2307 0 discriminator 3
	ldr	r2, [fp, #-8]
	str	r3, [r2, #48]
	.loc 1 2309 0 is_stmt 1 discriminator 3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #48]
	cmp	r3, #0
	bne	.L240
	.loc 1 2312 0
	ldr	r0, [fp, #-8]
	bl	vPortFree
	.loc 1 2313 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L237
.L240:
	.loc 1 2318 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #48]
	ldrh	r3, [fp, #-12]
	mov	r3, r3, asl #2
	mov	r0, r2
	mov	r1, #165
	mov	r2, r3
	bl	memset
.L237:
	.loc 1 2322 0
	ldr	r3, [fp, #-8]
	.loc 1 2323 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE35:
	.size	prvAllocateTCBAndStack, .-prvAllocateTCBAndStack
	.section .rodata
	.align	2
.LC20:
	.ascii	"%15s  0x%08lX%10.3f%%%6u\012\000"
	.align	2
.LC21:
	.ascii	"%15s   %23s    12u\012\000"
	.align	2
.LC22:
	.ascii	"(task hasn't run @ all)\000"
	.section	.text.prvGenerateRunTimeStatsForTasksInList,"ax",%progbits
	.align	2
	.type	prvGenerateRunTimeStatsForTasksInList, %function
prvGenerateRunTimeStatsForTasksInList:
.LFB36:
	.loc 1 2361 0
	@ args = 0, pretend = 0, frame = 160
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI104:
	fstmfdd	sp!, {d8}
.LCFI105:
	add	fp, sp, #20
.LCFI106:
	sub	sp, sp, #172
.LCFI107:
	str	r0, [fp, #-172]
	str	r1, [fp, #-176]
	str	r2, [fp, #-180]
.LBB13:
	.loc 1 2373 0
	ldr	r3, [fp, #-176]
	str	r3, [fp, #-24]
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-24]
	str	r2, [r3, #4]
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-24]
	add	r3, r3, #8
	cmp	r2, r3
	bne	.L242
	.loc 1 2373 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-24]
	str	r2, [r3, #4]
.L242:
	.loc 1 2373 0 discriminator 2
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #12]
	str	r3, [fp, #-28]
.L246:
.LBE13:
.LBB14:
	.loc 1 2377 0 is_stmt 1
	ldr	r3, [fp, #-176]
	str	r3, [fp, #-32]
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-32]
	str	r2, [r3, #4]
	ldr	r3, [fp, #-32]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-32]
	add	r3, r3, #8
	cmp	r2, r3
	bne	.L243
	.loc 1 2377 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-32]
	str	r2, [r3, #4]
.L243:
	.loc 1 2377 0 discriminator 2
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #12]
	str	r3, [fp, #-36]
.LBE14:
	.loc 1 2380 0 is_stmt 1 discriminator 2
	ldr	r3, [fp, #-180]
	cmp	r3, #0
	beq	.L244
	.loc 1 2383 0
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #92]
	fmsr	s14, r3	@ int
	fuitod	d6, s14
	fldd	d7, .L247
	fmuld	d6, d6, d7
	ldr	r3, [fp, #-180]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fcvtds	d7, s15
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	fsts	s15, [fp, #-40]
	.loc 1 2384 0
	ldr	r3, [fp, #-36]
	add	r5, r3, #52
	ldr	r3, [fp, #-36]
	ldr	r4, [r3, #92]
	flds	s15, [fp, #-40]
	fcvtds	d6, s15
	fldd	d7, .L247
	fdivd	d8, d6, d7
	ldr	r0, [fp, #-36]
	bl	uxTaskGetStackHighWaterMark
	mov	r3, r0
	mov	r2, r3, asl #2
	sub	r3, fp, #168
	fstd	d8, [sp, #0]
	str	r2, [sp, #8]
	mov	r0, r3
	ldr	r1, .L247+8
	mov	r2, r5
	mov	r3, r4
	bl	sprintf
	b	.L245
.L244:
	.loc 1 2390 0
	ldr	r3, [fp, #-36]
	add	r4, r3, #52
	ldr	r0, [fp, #-36]
	bl	uxTaskGetStackHighWaterMark
	mov	r3, r0
	mov	r2, r3, asl #2
	sub	r3, fp, #168
	str	r2, [sp, #0]
	mov	r0, r3
	ldr	r1, .L247+12
	mov	r2, r4
	ldr	r3, .L247+16
	bl	sprintf
.L245:
	.loc 1 2395 0
	sub	r3, fp, #168
	ldr	r0, [fp, #-172]
	mov	r1, r3
	bl	strcat
	.loc 1 2397 0
	ldr	r2, [fp, #-36]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bne	.L246
	.loc 1 2399 0
	sub	sp, fp, #20
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, fp, pc}
.L248:
	.align	2
.L247:
	.word	0
	.word	1079574528
	.word	.LC20
	.word	.LC21
	.word	.LC22
.LFE36:
	.size	prvGenerateRunTimeStatsForTasksInList, .-prvGenerateRunTimeStatsForTasksInList
	.section	.text.usTaskCheckFreeStackSpace,"ax",%progbits
	.align	2
	.type	usTaskCheckFreeStackSpace, %function
usTaskCheckFreeStackSpace:
.LFB37:
	.loc 1 2478 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, fp}
.LCFI108:
	add	fp, sp, #4
.LCFI109:
	sub	sp, sp, #4
.LCFI110:
	str	r0, [fp, #-8]
	.loc 1 2479 0
	mov	r4, #0
	.loc 1 2481 0
	b	.L250
.L251:
	.loc 1 2483 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 2484 0
	add	r3, r4, #1
	mov	r3, r3, asl #16
	mov	r4, r3, lsr #16
.L250:
	.loc 1 2481 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #165
	beq	.L251
	.loc 1 2487 0
	mov	r3, r4, lsr #2
	mov	r3, r3, asl #16
	mov	r4, r3, lsr #16
	.loc 1 2489 0
	mov	r3, r4
	.loc 1 2490 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {r4, fp}
	bx	lr
.LFE37:
	.size	usTaskCheckFreeStackSpace, .-usTaskCheckFreeStackSpace
	.section	.text.uxTaskGetStackHighWaterMark,"ax",%progbits
	.align	2
	.global	uxTaskGetStackHighWaterMark
	.type	uxTaskGetStackHighWaterMark, %function
uxTaskGetStackHighWaterMark:
.LFB38:
	.loc 1 2498 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI111:
	add	fp, sp, #4
.LCFI112:
	sub	sp, sp, #16
.LCFI113:
	str	r0, [fp, #-20]
	.loc 1 2503 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L253
	.loc 1 2503 0 is_stmt 0 discriminator 1
	ldr	r3, .L255
	ldr	r3, [r3, #0]
	b	.L254
.L253:
	.loc 1 2503 0 discriminator 2
	ldr	r3, [fp, #-20]
.L254:
	.loc 1 2503 0 discriminator 3
	str	r3, [fp, #-8]
	.loc 1 2507 0 is_stmt 1 discriminator 3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #48]
	str	r3, [fp, #-12]
	.loc 1 2515 0 discriminator 3
	ldr	r0, [fp, #-12]
	bl	usTaskCheckFreeStackSpace
	mov	r3, r0
	str	r3, [fp, #-16]
	.loc 1 2517 0 discriminator 3
	ldr	r3, [fp, #-16]
	.loc 1 2518 0 discriminator 3
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L256:
	.align	2
.L255:
	.word	pxCurrentTCB
.LFE38:
	.size	uxTaskGetStackHighWaterMark, .-uxTaskGetStackHighWaterMark
	.section	.text.prvDeleteTCB,"ax",%progbits
	.align	2
	.type	prvDeleteTCB, %function
prvDeleteTCB:
.LFB39:
	.loc 1 2526 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI114:
	add	fp, sp, #4
.LCFI115:
	sub	sp, sp, #4
.LCFI116:
	str	r0, [fp, #-8]
	.loc 1 2534 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #48]
	mov	r0, r3
	bl	vPortFree
	.loc 1 2535 0
	ldr	r0, [fp, #-8]
	bl	vPortFree
	.loc 1 2536 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE39:
	.size	prvDeleteTCB, .-prvDeleteTCB
	.section	.text.xTaskGetCurrentTaskHandle,"ax",%progbits
	.align	2
	.global	xTaskGetCurrentTaskHandle
	.type	xTaskGetCurrentTaskHandle, %function
xTaskGetCurrentTaskHandle:
.LFB40:
	.loc 1 2546 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI117:
	add	fp, sp, #0
.LCFI118:
	sub	sp, sp, #4
.LCFI119:
	.loc 1 2552 0
	ldr	r3, .L259
	ldr	r3, [r3, #0]
	str	r3, [fp, #-4]
	.loc 1 2554 0
	ldr	r3, [fp, #-4]
	.loc 1 2555 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L260:
	.align	2
.L259:
	.word	pxCurrentTCB
.LFE40:
	.size	xTaskGetCurrentTaskHandle, .-xTaskGetCurrentTaskHandle
	.section	.text.xTaskGetSchedulerState,"ax",%progbits
	.align	2
	.global	xTaskGetSchedulerState
	.type	xTaskGetSchedulerState, %function
xTaskGetSchedulerState:
.LFB41:
	.loc 1 2564 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI120:
	add	fp, sp, #0
.LCFI121:
	sub	sp, sp, #4
.LCFI122:
	.loc 1 2567 0
	ldr	r3, .L265
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L262
	.loc 1 2569 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L263
.L262:
	.loc 1 2573 0
	ldr	r3, .L265+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L264
	.loc 1 2575 0
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L263
.L264:
	.loc 1 2579 0
	mov	r3, #2
	str	r3, [fp, #-4]
.L263:
	.loc 1 2583 0
	ldr	r3, [fp, #-4]
	.loc 1 2584 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L266:
	.align	2
.L265:
	.word	xSchedulerRunning
	.word	uxSchedulerSuspended
.LFE41:
	.size	xTaskGetSchedulerState, .-xTaskGetSchedulerState
	.section .rodata
	.align	2
.LC23:
	.ascii	"pxMutexHolder\000"
	.section	.text.vTaskPriorityInherit,"ax",%progbits
	.align	2
	.global	vTaskPriorityInherit
	.type	vTaskPriorityInherit, %function
vTaskPriorityInherit:
.LFB42:
	.loc 1 2592 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI123:
	add	fp, sp, #4
.LCFI124:
	sub	sp, sp, #8
.LCFI125:
	str	r0, [fp, #-12]
	.loc 1 2593 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 2595 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L268
	.loc 1 2595 0 is_stmt 0 discriminator 1
	ldr	r0, .L272
	ldr	r1, .L272+4
	ldr	r2, .L272+8
	bl	__assert
.L268:
	.loc 1 2597 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r3, .L272+12
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	cmp	r2, r3
	bcs	.L267
	.loc 1 2600 0
	ldr	r3, .L272+12
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	rsb	r2, r3, #16
	ldr	r3, [fp, #-8]
	str	r2, [r3, #24]
	.loc 1 2604 0
	ldr	r3, [fp, #-8]
	ldr	r1, [r3, #20]
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L272+16
	add	r3, r2, r3
	cmp	r1, r3
	bne	.L270
	.loc 1 2606 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 2609 0
	ldr	r3, .L272+12
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #44]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #44]
	.loc 1 2610 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r3, .L272+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L271
	.loc 1 2610 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r3, .L272+20
	str	r2, [r3, #0]
.L271:
	.loc 1 2610 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L272+16
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsertEnd
	b	.L267
.L270:
	.loc 1 2615 0 is_stmt 1
	ldr	r3, .L272+12
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #44]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #44]
.L267:
	.loc 1 2620 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L273:
	.align	2
.L272:
	.word	.LC23
	.word	.LC1
	.word	2595
	.word	pxCurrentTCB
	.word	pxReadyTasksLists
	.word	uxTopReadyPriority
.LFE42:
	.size	vTaskPriorityInherit, .-vTaskPriorityInherit
	.section	.text.vTaskPriorityDisinherit,"ax",%progbits
	.align	2
	.global	vTaskPriorityDisinherit
	.type	vTaskPriorityDisinherit, %function
vTaskPriorityDisinherit:
.LFB43:
	.loc 1 2628 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI126:
	add	fp, sp, #4
.LCFI127:
	sub	sp, sp, #8
.LCFI128:
	str	r0, [fp, #-12]
	.loc 1 2629 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 2631 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L274
	.loc 1 2633 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #84]
	cmp	r2, r3
	beq	.L274
	.loc 1 2637 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 2642 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #84]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #44]
	.loc 1 2643 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #44]
	rsb	r2, r3, #16
	ldr	r3, [fp, #-8]
	str	r2, [r3, #24]
	.loc 1 2644 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r3, .L277
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L276
	.loc 1 2644 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r3, .L277
	str	r2, [r3, #0]
.L276:
	.loc 1 2644 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L277+4
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsertEnd
.L274:
	.loc 1 2647 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L278:
	.align	2
.L277:
	.word	uxTopReadyPriority
	.word	pxReadyTasksLists
.LFE43:
	.size	vTaskPriorityDisinherit, .-vTaskPriorityDisinherit
	.section	.rodata.ucExpectedStackBytes.1602,"a",%progbits
	.align	2
	.type	ucExpectedStackBytes.1602, %object
	.size	ucExpectedStackBytes.1602, 20
ucExpectedStackBytes.1602:
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI35-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI37-.LFB13
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI40-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI43-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI44-.LCFI43
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI46-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI47-.LCFI46
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI48-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI51-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI52-.LCFI51
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI54-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI55-.LCFI54
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI57-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI58-.LCFI57
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI60-.LFB21
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI61-.LCFI60
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI63-.LFB22
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI64-.LCFI63
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI66-.LFB23
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI67-.LCFI66
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI69-.LFB24
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI70-.LCFI69
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI72-.LFB25
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI73-.LCFI72
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI75-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI76-.LCFI75
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI78-.LFB27
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI79-.LCFI78
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI81-.LFB28
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI82-.LCFI81
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI84-.LFB29
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI85-.LCFI84
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI86-.LFB30
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI87-.LCFI86
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI89-.LFB31
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI90-.LCFI89
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI92-.LFB32
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI93-.LCFI92
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI95-.LFB33
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI96-.LCFI95
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI98-.LFB34
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI99-.LCFI98
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI101-.LFB35
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI102-.LCFI101
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI104-.LFB36
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI105-.LCFI104
	.byte	0xe
	.uleb128 0x18
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI106-.LCFI105
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI108-.LFB37
	.byte	0xe
	.uleb128 0x8
	.byte	0x8b
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI109-.LCFI108
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI111-.LFB38
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI112-.LCFI111
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI114-.LFB39
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI115-.LCFI114
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI117-.LFB40
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI118-.LCFI117
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI120-.LFB41
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI121-.LCFI120
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI123-.LFB42
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI124-.LCFI123
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI126-.LFB43
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI127-.LCFI126
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE86:
	.text
.Letext0:
	.file 2 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/stdio.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/projdefs.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/FreeRTOS.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/list.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/task.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x11bc
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF169
	.byte	0x1
	.4byte	.LASF170
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x2
	.byte	0x21
	.4byte	0x30
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x3
	.byte	0x47
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x64
	.uleb128 0x6
	.byte	0x1
	.4byte	0x70
	.uleb128 0x7
	.4byte	0x70
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.4byte	.LASF6
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF8
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x4
	.byte	0x35
	.4byte	0x30
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x5
	.byte	0x58
	.4byte	0xa4
	.uleb128 0x5
	.byte	0x4
	.4byte	0xaa
	.uleb128 0x9
	.byte	0x1
	.4byte	0x37
	.4byte	0xba
	.uleb128 0x7
	.4byte	0x70
	.byte	0
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x14
	.byte	0x6
	.byte	0x69
	.4byte	0x10d
	.uleb128 0xb
	.4byte	.LASF12
	.byte	0x6
	.byte	0x6b
	.4byte	0x8e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x6
	.byte	0x6c
	.4byte	0x10d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x6
	.byte	0x6d
	.4byte	0x10d
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF15
	.byte	0x6
	.byte	0x6e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x6
	.byte	0x6f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x113
	.uleb128 0xc
	.4byte	0xba
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x6
	.byte	0x71
	.4byte	0xba
	.uleb128 0xa
	.4byte	.LASF19
	.byte	0xc
	.byte	0x6
	.byte	0x73
	.4byte	0x15a
	.uleb128 0xb
	.4byte	.LASF12
	.byte	0x6
	.byte	0x75
	.4byte	0x8e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x6
	.byte	0x76
	.4byte	0x10d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x6
	.byte	0x77
	.4byte	0x10d
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x6
	.byte	0x79
	.4byte	0x123
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x14
	.byte	0x6
	.byte	0x7e
	.4byte	0x19c
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x6
	.byte	0x80
	.4byte	0x19c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x6
	.byte	0x81
	.4byte	0x1a1
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x6
	.byte	0x82
	.4byte	0x1ac
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xc
	.4byte	0x30
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1a7
	.uleb128 0xc
	.4byte	0x118
	.uleb128 0xc
	.4byte	0x15a
	.uleb128 0x2
	.4byte	.LASF25
	.byte	0x6
	.byte	0x83
	.4byte	0x165
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x7
	.byte	0x63
	.4byte	0x70
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x8
	.byte	0x7
	.byte	0x68
	.4byte	0x1f0
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x7
	.byte	0x6a
	.4byte	0x37
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x7
	.byte	0x6b
	.4byte	0x8e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x2
	.4byte	.LASF30
	.byte	0x7
	.byte	0x6c
	.4byte	0x1c7
	.uleb128 0xa
	.4byte	.LASF31
	.byte	0xc
	.byte	0x7
	.byte	0x71
	.4byte	0x232
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x7
	.byte	0x73
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF33
	.byte	0x7
	.byte	0x74
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF34
	.byte	0x7
	.byte	0x75
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x2
	.4byte	.LASF35
	.byte	0x7
	.byte	0x76
	.4byte	0x1fb
	.uleb128 0xd
	.4byte	0x242
	.uleb128 0x5
	.byte	0x4
	.4byte	0x248
	.uleb128 0xd
	.4byte	0x72
	.uleb128 0x5
	.byte	0x4
	.4byte	0x30
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x60
	.byte	0x1
	.byte	0x5d
	.4byte	0x2de
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x1
	.byte	0x5f
	.4byte	0x2de
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF38
	.byte	0x1
	.byte	0x65
	.4byte	0x118
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF39
	.byte	0x1
	.byte	0x66
	.4byte	0x118
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF40
	.byte	0x1
	.byte	0x67
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xb
	.4byte	.LASF41
	.byte	0x1
	.byte	0x68
	.4byte	0x24d
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xb
	.4byte	.LASF42
	.byte	0x1
	.byte	0x69
	.4byte	0x2e4
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xb
	.4byte	.LASF43
	.byte	0x1
	.byte	0x79
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xb
	.4byte	.LASF44
	.byte	0x1
	.byte	0x7d
	.4byte	0x99
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xb
	.4byte	.LASF45
	.byte	0x1
	.byte	0x81
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x19c
	.uleb128 0xe
	.4byte	0x72
	.4byte	0x2f4
	.uleb128 0xf
	.4byte	0x30
	.byte	0x1f
	.byte	0
	.uleb128 0x2
	.4byte	.LASF46
	.byte	0x1
	.byte	0x84
	.4byte	0x253
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x18f
	.byte	0x1
	.4byte	0x37
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x3cd
	.uleb128 0x11
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x18f
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x11
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x18f
	.4byte	0x23d
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x11
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x18f
	.4byte	0x4c
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x11
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x18f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x11
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x18f
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x11
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x18f
	.4byte	0x3cd
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x18f
	.4byte	0x24d
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x11
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x18f
	.4byte	0x3d3
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x191
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x192
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x12
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x19d
	.4byte	0x24d
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1bc
	.uleb128 0xd
	.4byte	0x3d8
	.uleb128 0x5
	.byte	0x4
	.4byte	0x3de
	.uleb128 0xd
	.4byte	0x232
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2f4
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x234
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x422
	.uleb128 0x11
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x234
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x236
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x276
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x488
	.uleb128 0x11
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x276
	.4byte	0x488
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x11
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x276
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x278
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x279
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x279
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xd
	.4byte	0x48d
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8e
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x2b7
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x4db
	.uleb128 0x11
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x2b7
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x2b9
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x2ba
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x2e5
	.byte	0x1
	.4byte	0x30
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x527
	.uleb128 0x11
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x2e5
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x2e7
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x2e8
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x2fb
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x58d
	.uleb128 0x11
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x2fb
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x11
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x2fb
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x2fd
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x2fe
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x2ff
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x367
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x5c6
	.uleb128 0x11
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x367
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x369
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x3a7
	.byte	0x1
	.4byte	0x37
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x612
	.uleb128 0x11
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x3a7
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x3a9
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x3aa
	.4byte	0x612
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xd
	.4byte	0x617
	.uleb128 0x5
	.byte	0x4
	.4byte	0x61d
	.uleb128 0xd
	.4byte	0x2f4
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x3c9
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x65b
	.uleb128 0x11
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x3c9
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x3cb
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x3f6
	.byte	0x1
	.4byte	0x37
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x6b6
	.uleb128 0x11
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x3f6
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x3f8
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x3f9
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x3fa
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x424
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x6e0
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x426
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x465
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x470
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x478
	.byte	0x1
	.4byte	0x37
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x761
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x47a
	.4byte	0x3e3
	.byte	0x1
	.byte	0x54
	.uleb128 0x12
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x47b
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0x12
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x48e
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x4cf
	.byte	0x1
	.4byte	0x8e
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x78f
	.uleb128 0x12
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x4d1
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x4de
	.byte	0x1
	.4byte	0x8e
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x7cc
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x4e0
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x4e1
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF171
	.byte	0x1
	.2byte	0x4eb
	.byte	0x1
	.4byte	0x30
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x4f5
	.byte	0x1
	.4byte	0x823
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x823
	.uleb128 0x11
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x4f5
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x4f7
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x72
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x546
	.byte	0x1
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0x871
	.uleb128 0x11
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x546
	.4byte	0x823
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x548
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x549
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x63e
	.byte	0x1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0x8d1
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x640
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LBB4
	.4byte	.LBE4
	.4byte	0x8b7
	.uleb128 0x12
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x64a
	.4byte	0x8d1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x13
	.4byte	.LBB5
	.4byte	.LBE5
	.uleb128 0x12
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x66b
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1b1
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x68b
	.byte	0x1
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.4byte	0x91f
	.uleb128 0x11
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x68b
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x68b
	.4byte	0x99
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x68d
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x6a5
	.byte	0x1
	.4byte	0x99
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0x96b
	.uleb128 0x11
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x6a5
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x6a7
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x6a8
	.4byte	0x99
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x6c2
	.byte	0x1
	.4byte	0x37
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0x9c6
	.uleb128 0x11
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x6c2
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x6c2
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x6c4
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x6c5
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x6e0
	.byte	0x1
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST23
	.4byte	0xa75
	.uleb128 0x17
	.4byte	.LBB6
	.4byte	.LBE6
	.4byte	0x9ff
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x6ea
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	0x70
	.byte	0
	.byte	0
	.uleb128 0x17
	.4byte	.LBB7
	.4byte	.LBE7
	.4byte	0xa1c
	.uleb128 0x12
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x6ee
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.4byte	.LBB8
	.4byte	.LBE8
	.4byte	0xa3c
	.uleb128 0x12
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x701
	.4byte	0xa85
	.byte	0x5
	.byte	0x3
	.4byte	ucExpectedStackBytes.1602
	.byte	0
	.uleb128 0x17
	.4byte	.LBB9
	.4byte	.LBE9
	.4byte	0xa59
	.uleb128 0x12
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x70c
	.4byte	0xa8a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x13
	.4byte	.LBB10
	.4byte	.LBE10
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x70e
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	0x70
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0xe
	.4byte	0x80
	.4byte	0xa85
	.uleb128 0xf
	.4byte	0x30
	.byte	0x13
	.byte	0
	.uleb128 0xd
	.4byte	0xa75
	.uleb128 0xd
	.4byte	0x8d1
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x713
	.byte	0x1
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST24
	.4byte	0xad7
	.uleb128 0x11
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x713
	.4byte	0xad7
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x713
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x715
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xd
	.4byte	0xadc
	.uleb128 0x5
	.byte	0x4
	.4byte	0xae2
	.uleb128 0xd
	.4byte	0x1b1
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x745
	.byte	0x1
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST25
	.4byte	0xb2f
	.uleb128 0x11
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x745
	.4byte	0xad7
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x745
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x747
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x765
	.byte	0x1
	.4byte	0x37
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST26
	.4byte	0xb7b
	.uleb128 0x11
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x765
	.4byte	0xad7
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x767
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x768
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x798
	.byte	0x1
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST27
	.4byte	0xba5
	.uleb128 0x11
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x798
	.4byte	0xba5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xd
	.4byte	0xbaa
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1f0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x7a0
	.byte	0x1
	.4byte	0x37
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST28
	.4byte	0xbfc
	.uleb128 0x11
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x7a0
	.4byte	0xba5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x7a0
	.4byte	0x488
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x7a2
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x7ce
	.byte	0x1
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST29
	.uleb128 0x19
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x802
	.byte	0x1
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST30
	.4byte	0xc3b
	.uleb128 0x11
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x802
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF122
	.byte	0x1
	.2byte	0x843
	.byte	0x1
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST31
	.4byte	0xca0
	.uleb128 0x11
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x843
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x11
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x843
	.4byte	0x23d
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x843
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x843
	.4byte	0x3d3
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x11
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x843
	.4byte	0x4c
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x19
	.4byte	.LASF123
	.byte	0x1
	.2byte	0x899
	.byte	0x1
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST32
	.4byte	0xcc9
	.uleb128 0x12
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x89b
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF124
	.byte	0x1
	.2byte	0x8b9
	.byte	0x1
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST33
	.4byte	0xd15
	.uleb128 0x13
	.4byte	.LBB11
	.4byte	.LBE11
	.uleb128 0x12
	.4byte	.LASF125
	.byte	0x1
	.2byte	0x8bd
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LBB12
	.4byte	.LBE12
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x8c9
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x19
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x8dc
	.byte	0x1
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST34
	.4byte	0xd3e
	.uleb128 0x11
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x8dc
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF135
	.byte	0x1
	.2byte	0x8f6
	.byte	0x1
	.4byte	0x3e3
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST35
	.4byte	0xd89
	.uleb128 0x11
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x8f6
	.4byte	0x4c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x8f6
	.4byte	0x24d
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x8f8
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF127
	.byte	0x1
	.2byte	0x938
	.byte	0x1
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST36
	.4byte	0xe46
	.uleb128 0x11
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x938
	.4byte	0x242
	.byte	0x3
	.byte	0x91
	.sleb128 -176
	.uleb128 0x11
	.4byte	.LASF128
	.byte	0x1
	.2byte	0x938
	.4byte	0x8d1
	.byte	0x3
	.byte	0x91
	.sleb128 -180
	.uleb128 0x11
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x938
	.4byte	0x30
	.byte	0x3
	.byte	0x91
	.sleb128 -184
	.uleb128 0x12
	.4byte	.LASF129
	.byte	0x1
	.2byte	0x93d
	.4byte	0xe46
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x12
	.4byte	.LASF130
	.byte	0x1
	.2byte	0x93d
	.4byte	0xe46
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.4byte	.LASF131
	.byte	0x1
	.2byte	0x93f
	.4byte	0xe51
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x12
	.4byte	.LASF132
	.byte	0x1
	.2byte	0x941
	.4byte	0xe58
	.byte	0x3
	.byte	0x91
	.sleb128 -172
	.uleb128 0x17
	.4byte	.LBB13
	.4byte	.LBE13
	.4byte	0xe2c
	.uleb128 0x12
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x945
	.4byte	0xa8a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x13
	.4byte	.LBB14
	.4byte	.LBE14
	.uleb128 0x12
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x949
	.4byte	0xa8a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0xe4c
	.uleb128 0xc
	.4byte	0x2f4
	.uleb128 0x3
	.byte	0x4
	.byte	0x4
	.4byte	.LASF133
	.uleb128 0xe
	.4byte	0xe68
	.4byte	0xe68
	.uleb128 0xf
	.4byte	0x30
	.byte	0x7f
	.byte	0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF134
	.uleb128 0x1a
	.4byte	.LASF136
	.byte	0x1
	.2byte	0x9ad
	.byte	0x1
	.4byte	0x4c
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST37
	.4byte	0xeaa
	.uleb128 0x11
	.4byte	.LASF137
	.byte	0x1
	.2byte	0x9ad
	.4byte	0xeaa
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF138
	.byte	0x1
	.2byte	0x9af
	.4byte	0x4c
	.byte	0x1
	.byte	0x54
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0xeb0
	.uleb128 0xd
	.4byte	0x80
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF139
	.byte	0x1
	.2byte	0x9c1
	.byte	0x1
	.4byte	0x30
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST38
	.4byte	0xf10
	.uleb128 0x11
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x9c1
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x9c3
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF140
	.byte	0x1
	.2byte	0x9c4
	.4byte	0xf10
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x9c5
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x80
	.uleb128 0x19
	.4byte	.LASF141
	.byte	0x1
	.2byte	0x9dd
	.byte	0x1
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST39
	.4byte	0xf3f
	.uleb128 0x11
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x9dd
	.4byte	0x3e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF142
	.byte	0x1
	.2byte	0x9f1
	.byte	0x1
	.4byte	0x1bc
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST40
	.4byte	0xf6d
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x9f3
	.4byte	0x1bc
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF143
	.byte	0x1
	.2byte	0xa03
	.byte	0x1
	.4byte	0x37
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST41
	.4byte	0xf9b
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0xa05
	.4byte	0x37
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF144
	.byte	0x1
	.2byte	0xa1f
	.byte	0x1
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST42
	.4byte	0xfd4
	.uleb128 0x11
	.4byte	.LASF145
	.byte	0x1
	.2byte	0xa1f
	.4byte	0xfd4
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0xa21
	.4byte	0xfd9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xd
	.4byte	0x3cd
	.uleb128 0xd
	.4byte	0x3e3
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF146
	.byte	0x1
	.2byte	0xa43
	.byte	0x1
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST43
	.4byte	0x1017
	.uleb128 0x11
	.4byte	.LASF145
	.byte	0x1
	.2byte	0xa43
	.4byte	0xfd4
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0xa45
	.4byte	0xfd9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF168
	.byte	0x1
	.byte	0x90
	.4byte	0x1024
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	0x3e3
	.uleb128 0xe
	.4byte	0x1b1
	.4byte	0x1039
	.uleb128 0xf
	.4byte	0x30
	.byte	0xf
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF147
	.byte	0x1
	.byte	0x94
	.4byte	0x1029
	.byte	0x5
	.byte	0x3
	.4byte	pxReadyTasksLists
	.uleb128 0x1c
	.4byte	.LASF148
	.byte	0x1
	.byte	0x95
	.4byte	0x1b1
	.byte	0x5
	.byte	0x3
	.4byte	xDelayedTaskList1
	.uleb128 0x1c
	.4byte	.LASF149
	.byte	0x1
	.byte	0x96
	.4byte	0x1b1
	.byte	0x5
	.byte	0x3
	.4byte	xDelayedTaskList2
	.uleb128 0x1c
	.4byte	.LASF150
	.byte	0x1
	.byte	0x97
	.4byte	0x107d
	.byte	0x5
	.byte	0x3
	.4byte	pxDelayedTaskList
	.uleb128 0xc
	.4byte	0x8d1
	.uleb128 0x1c
	.4byte	.LASF151
	.byte	0x1
	.byte	0x98
	.4byte	0x107d
	.byte	0x5
	.byte	0x3
	.4byte	pxOverflowDelayedTaskList
	.uleb128 0x1c
	.4byte	.LASF152
	.byte	0x1
	.byte	0x99
	.4byte	0x1b1
	.byte	0x5
	.byte	0x3
	.4byte	xPendingReadyList
	.uleb128 0x1c
	.4byte	.LASF153
	.byte	0x1
	.byte	0x9d
	.4byte	0x1b1
	.byte	0x5
	.byte	0x3
	.4byte	xTasksWaitingTermination
	.uleb128 0x1c
	.4byte	.LASF154
	.byte	0x1
	.byte	0x9e
	.4byte	0x19c
	.byte	0x5
	.byte	0x3
	.4byte	uxTasksDeleted
	.uleb128 0x1c
	.4byte	.LASF155
	.byte	0x1
	.byte	0xa4
	.4byte	0x1b1
	.byte	0x5
	.byte	0x3
	.4byte	xSuspendedTaskList
	.uleb128 0x1c
	.4byte	.LASF156
	.byte	0x1
	.byte	0xaf
	.4byte	0x19c
	.byte	0x5
	.byte	0x3
	.4byte	uxCurrentNumberOfTasks
	.uleb128 0x1c
	.4byte	.LASF157
	.byte	0x1
	.byte	0xb0
	.4byte	0x10f9
	.byte	0x5
	.byte	0x3
	.4byte	xTickCount
	.uleb128 0xc
	.4byte	0x8e
	.uleb128 0x1c
	.4byte	.LASF158
	.byte	0x1
	.byte	0xb1
	.4byte	0x30
	.byte	0x5
	.byte	0x3
	.4byte	uxTopUsedPriority
	.uleb128 0x1c
	.4byte	.LASF159
	.byte	0x1
	.byte	0xb2
	.4byte	0x19c
	.byte	0x5
	.byte	0x3
	.4byte	uxTopReadyPriority
	.uleb128 0x1c
	.4byte	.LASF160
	.byte	0x1
	.byte	0xb3
	.4byte	0x1131
	.byte	0x5
	.byte	0x3
	.4byte	xSchedulerRunning
	.uleb128 0xc
	.4byte	0x37
	.uleb128 0x1c
	.4byte	.LASF161
	.byte	0x1
	.byte	0xb4
	.4byte	0x19c
	.byte	0x5
	.byte	0x3
	.4byte	uxSchedulerSuspended
	.uleb128 0x1c
	.4byte	.LASF162
	.byte	0x1
	.byte	0xb5
	.4byte	0x19c
	.byte	0x5
	.byte	0x3
	.4byte	uxMissedTicks
	.uleb128 0x1c
	.4byte	.LASF163
	.byte	0x1
	.byte	0xb6
	.4byte	0x1131
	.byte	0x5
	.byte	0x3
	.4byte	xMissedYield
	.uleb128 0x1c
	.4byte	.LASF164
	.byte	0x1
	.byte	0xb7
	.4byte	0x1131
	.byte	0x5
	.byte	0x3
	.4byte	xNumOfOverflows
	.uleb128 0x1c
	.4byte	.LASF165
	.byte	0x1
	.byte	0xb8
	.4byte	0x30
	.byte	0x5
	.byte	0x3
	.4byte	uxTaskNumber
	.uleb128 0x1c
	.4byte	.LASF166
	.byte	0x1
	.byte	0xb9
	.4byte	0x8e
	.byte	0x5
	.byte	0x3
	.4byte	xNextTaskUnblockTime
	.uleb128 0x1c
	.4byte	.LASF167
	.byte	0x1
	.byte	0xc0
	.4byte	0x30
	.byte	0x5
	.byte	0x3
	.4byte	ulTaskSwitchedInTime
	.uleb128 0x1d
	.4byte	.LASF168
	.byte	0x1
	.byte	0x90
	.4byte	0x1024
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	pxCurrentTCB
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI36
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI38
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI41
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI44
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI47
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI49
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI52
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI55
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI58
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI61
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI64
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB23
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI67
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB24
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI69
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI70
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB25
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI73
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB26
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI75
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI76
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB27
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI78
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI79
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB28
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI81
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI82
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB29
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI85
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB30
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI86
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI87
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB31
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI89
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI90
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB32
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI92
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI93
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB33
	.4byte	.LCFI95
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI95
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI96
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB34
	.4byte	.LCFI98
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI98
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI99
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB35
	.4byte	.LCFI101
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI101
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI102
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB36
	.4byte	.LCFI104
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI104
	.4byte	.LCFI105
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI105
	.4byte	.LCFI106
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI106
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB37
	.4byte	.LCFI108
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI108
	.4byte	.LCFI109
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI109
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB38
	.4byte	.LCFI111
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI111
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI112
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB39
	.4byte	.LCFI114
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI114
	.4byte	.LCFI115
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI115
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB40
	.4byte	.LCFI117
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI117
	.4byte	.LCFI118
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI118
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB41
	.4byte	.LCFI120
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI120
	.4byte	.LCFI121
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI121
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB42
	.4byte	.LCFI123
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI123
	.4byte	.LCFI124
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI124
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB43
	.4byte	.LCFI126
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI126
	.4byte	.LCFI127
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI127
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x174
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF122:
	.ascii	"prvInitialiseTCBVariables\000"
.LASF17:
	.ascii	"xListItem\000"
.LASF111:
	.ascii	"pxEventList\000"
.LASF75:
	.ascii	"vTaskSuspend\000"
.LASF4:
	.ascii	"size_t\000"
.LASF156:
	.ascii	"uxCurrentNumberOfTasks\000"
.LASF45:
	.ascii	"ulRunTimeCounter\000"
.LASF154:
	.ascii	"uxTasksDeleted\000"
.LASF99:
	.ascii	"pxHookFunction\000"
.LASF155:
	.ascii	"xSuspendedTaskList\000"
.LASF44:
	.ascii	"pxTaskTag\000"
.LASF67:
	.ascii	"xTaskGenericCreate\000"
.LASF57:
	.ascii	"pxTCB\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF63:
	.ascii	"xAlreadyYielded\000"
.LASF117:
	.ascii	"pxTimeOut\000"
.LASF24:
	.ascii	"xListEnd\000"
.LASF27:
	.ascii	"xTIME_OUT\000"
.LASF19:
	.ascii	"xMINI_LIST_ITEM\000"
.LASF125:
	.ascii	"xListIsEmpty\000"
.LASF170:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/tasks.c\000"
.LASF144:
	.ascii	"vTaskPriorityInherit\000"
.LASF80:
	.ascii	"pxTaskToResume\000"
.LASF2:
	.ascii	"long long int\000"
.LASF6:
	.ascii	"signed char\000"
.LASF108:
	.ascii	"vPortSaveVFPRegisters\000"
.LASF65:
	.ascii	"vTaskDelay\000"
.LASF26:
	.ascii	"xTaskHandle\000"
.LASF102:
	.ascii	"xTaskCallApplicationTaskHook\000"
.LASF20:
	.ascii	"xMiniListItem\000"
.LASF25:
	.ascii	"xList\000"
.LASF1:
	.ascii	"long int\000"
.LASF36:
	.ascii	"tskTaskControlBlock\000"
.LASF55:
	.ascii	"pxNewTCB\000"
.LASF89:
	.ascii	"xTaskGetTickCountFromISR\000"
.LASF135:
	.ascii	"prvAllocateTCBAndStack\000"
.LASF10:
	.ascii	"portTickType\000"
.LASF130:
	.ascii	"pxFirstTCB\000"
.LASF49:
	.ascii	"usStackDepth\000"
.LASF91:
	.ascii	"xTaskToQuery\000"
.LASF52:
	.ascii	"puxStackBuffer\000"
.LASF71:
	.ascii	"vTaskPrioritySet\000"
.LASF28:
	.ascii	"xOverflowCount\000"
.LASF83:
	.ascii	"vTaskStartScheduler\000"
.LASF132:
	.ascii	"str_128\000"
.LASF110:
	.ascii	"vTaskPlaceOnEventList\000"
.LASF69:
	.ascii	"pxTask\000"
.LASF103:
	.ascii	"pvParameter\000"
.LASF161:
	.ascii	"uxSchedulerSuspended\000"
.LASF100:
	.ascii	"xTCB\000"
.LASF15:
	.ascii	"pvOwner\000"
.LASF54:
	.ascii	"xReturn\000"
.LASF29:
	.ascii	"xTimeOnEntering\000"
.LASF167:
	.ascii	"ulTaskSwitchedInTime\000"
.LASF51:
	.ascii	"pxCreatedTask\000"
.LASF22:
	.ascii	"uxNumberOfItems\000"
.LASF77:
	.ascii	"xTaskIsTaskSuspended\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF37:
	.ascii	"pxTopOfStack\000"
.LASF50:
	.ascii	"pvParameters\000"
.LASF157:
	.ascii	"xTickCount\000"
.LASF46:
	.ascii	"tskTCB\000"
.LASF107:
	.ascii	"pxConstList\000"
.LASF11:
	.ascii	"pdTASK_HOOK_CODE\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF169:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF74:
	.ascii	"xYieldRequired\000"
.LASF30:
	.ascii	"xTimeOutType\000"
.LASF79:
	.ascii	"vTaskResume\000"
.LASF168:
	.ascii	"pxCurrentTCB\000"
.LASF70:
	.ascii	"uxReturn\000"
.LASF92:
	.ascii	"vTaskGetRunTimeStats\000"
.LASF136:
	.ascii	"usTaskCheckFreeStackSpace\000"
.LASF150:
	.ascii	"pxDelayedTaskList\000"
.LASF31:
	.ascii	"xMEMORY_REGION\000"
.LASF98:
	.ascii	"vTaskSetApplicationTaskTag\000"
.LASF127:
	.ascii	"prvGenerateRunTimeStatsForTasksInList\000"
.LASF137:
	.ascii	"pucStackByte\000"
.LASF141:
	.ascii	"prvDeleteTCB\000"
.LASF163:
	.ascii	"xMissedYield\000"
.LASF106:
	.ascii	"ucExpectedStackBytes\000"
.LASF121:
	.ascii	"prvIdleTask\000"
.LASF104:
	.ascii	"vTaskSwitchContext\000"
.LASF145:
	.ascii	"pxMutexHolder\000"
.LASF41:
	.ascii	"pxStack\000"
.LASF138:
	.ascii	"usCount\000"
.LASF151:
	.ascii	"pxOverflowDelayedTaskList\000"
.LASF34:
	.ascii	"ulParameters\000"
.LASF56:
	.ascii	"pxTaskToDelete\000"
.LASF53:
	.ascii	"xRegions\000"
.LASF146:
	.ascii	"vTaskPriorityDisinherit\000"
.LASF152:
	.ascii	"xPendingReadyList\000"
.LASF153:
	.ascii	"xTasksWaitingTermination\000"
.LASF142:
	.ascii	"xTaskGetCurrentTaskHandle\000"
.LASF47:
	.ascii	"pxTaskCode\000"
.LASF39:
	.ascii	"xEventListItem\000"
.LASF166:
	.ascii	"xNextTaskUnblockTime\000"
.LASF109:
	.ascii	"vPortRestoreVFPRegisters\000"
.LASF140:
	.ascii	"pcEndOfStack\000"
.LASF159:
	.ascii	"uxTopReadyPriority\000"
.LASF60:
	.ascii	"pxPreviousWakeTime\000"
.LASF33:
	.ascii	"ulLengthInBytes\000"
.LASF14:
	.ascii	"pxPrevious\000"
.LASF72:
	.ascii	"uxNewPriority\000"
.LASF133:
	.ascii	"float\000"
.LASF112:
	.ascii	"xTicksToWait\000"
.LASF129:
	.ascii	"pxNextTCB\000"
.LASF23:
	.ascii	"pxIndex\000"
.LASF105:
	.ascii	"ulTempCounter\000"
.LASF16:
	.ascii	"pvContainer\000"
.LASF115:
	.ascii	"pxUnblockedTCB\000"
.LASF59:
	.ascii	"vTaskDelayUntil\000"
.LASF94:
	.ascii	"uxQueue\000"
.LASF62:
	.ascii	"xTimeToWake\000"
.LASF8:
	.ascii	"unsigned char\000"
.LASF7:
	.ascii	"short int\000"
.LASF96:
	.ascii	"vTaskIncrementTick\000"
.LASF160:
	.ascii	"xSchedulerRunning\000"
.LASF84:
	.ascii	"vTaskEndScheduler\000"
.LASF86:
	.ascii	"xTaskResumeAll\000"
.LASF21:
	.ascii	"xLIST\000"
.LASF120:
	.ascii	"vTaskMissedYield\000"
.LASF38:
	.ascii	"xGenericListItem\000"
.LASF43:
	.ascii	"uxBasePriority\000"
.LASF81:
	.ascii	"xTaskResumeFromISR\000"
.LASF143:
	.ascii	"xTaskGetSchedulerState\000"
.LASF82:
	.ascii	"uxSavedInterruptStatus\000"
.LASF64:
	.ascii	"xShouldDelay\000"
.LASF124:
	.ascii	"prvCheckTasksWaitingTermination\000"
.LASF78:
	.ascii	"xTask\000"
.LASF35:
	.ascii	"xMemoryRegion\000"
.LASF126:
	.ascii	"prvAddCurrentTaskToDelayedList\000"
.LASF139:
	.ascii	"uxTaskGetStackHighWaterMark\000"
.LASF134:
	.ascii	"char\000"
.LASF128:
	.ascii	"pxList\000"
.LASF114:
	.ascii	"xTaskRemoveFromEventList\000"
.LASF5:
	.ascii	"pdTASK_CODE\000"
.LASF93:
	.ascii	"pcWriteBuffer\000"
.LASF58:
	.ascii	"vTaskDelete\000"
.LASF90:
	.ascii	"pcTaskGetTaskName\000"
.LASF87:
	.ascii	"xTaskGetTickCount\000"
.LASF118:
	.ascii	"xTaskCheckForTimeOut\000"
.LASF165:
	.ascii	"uxTaskNumber\000"
.LASF97:
	.ascii	"pxTemp\000"
.LASF147:
	.ascii	"pxReadyTasksLists\000"
.LASF95:
	.ascii	"ulTotalRunTime\000"
.LASF171:
	.ascii	"uxTaskGetNumberOfTasks\000"
.LASF148:
	.ascii	"xDelayedTaskList1\000"
.LASF149:
	.ascii	"xDelayedTaskList2\000"
.LASF164:
	.ascii	"xNumOfOverflows\000"
.LASF88:
	.ascii	"xTicks\000"
.LASF85:
	.ascii	"vTaskSuspendAll\000"
.LASF12:
	.ascii	"xItemValue\000"
.LASF162:
	.ascii	"uxMissedTicks\000"
.LASF66:
	.ascii	"xTicksToDelay\000"
.LASF61:
	.ascii	"xTimeIncrement\000"
.LASF42:
	.ascii	"pcTaskName\000"
.LASF101:
	.ascii	"xTaskGetApplicationTaskTag\000"
.LASF119:
	.ascii	"pxTicksToWait\000"
.LASF73:
	.ascii	"uxCurrentPriority\000"
.LASF13:
	.ascii	"pxNext\000"
.LASF113:
	.ascii	"vTaskPlaceOnEventListRestricted\000"
.LASF158:
	.ascii	"uxTopUsedPriority\000"
.LASF18:
	.ascii	"xLIST_ITEM\000"
.LASF40:
	.ascii	"uxPriority\000"
.LASF131:
	.ascii	"percentage_fl\000"
.LASF48:
	.ascii	"pcName\000"
.LASF68:
	.ascii	"uxTaskPriorityGet\000"
.LASF116:
	.ascii	"vTaskSetTimeOutState\000"
.LASF76:
	.ascii	"pxTaskToSuspend\000"
.LASF32:
	.ascii	"pvBaseAddress\000"
.LASF123:
	.ascii	"prvInitialiseTaskLists\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
