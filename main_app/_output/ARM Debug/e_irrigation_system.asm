	.file	"e_irrigation_system.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.g_SYSTEM_is_watering,"aw",%nobits
	.align	2
	.type	g_SYSTEM_is_watering, %object
	.size	g_SYSTEM_is_watering, 4
g_SYSTEM_is_watering:
	.space	4
	.section	.bss.g_SYSTEM_editing_group,"aw",%nobits
	.align	2
	.type	g_SYSTEM_editing_group, %object
	.size	g_SYSTEM_editing_group, 4
g_SYSTEM_editing_group:
	.space	4
	.section	.bss.g_SYSTEM_index_of_poc_when_dialog_displayed,"aw",%nobits
	.align	2
	.type	g_SYSTEM_index_of_poc_when_dialog_displayed, %object
	.size	g_SYSTEM_index_of_poc_when_dialog_displayed, 4
g_SYSTEM_index_of_poc_when_dialog_displayed:
	.space	4
	.section	.bss.g_SYSTEM_num_pocs_physically_available,"aw",%nobits
	.align	2
	.type	g_SYSTEM_num_pocs_physically_available, %object
	.size	g_SYSTEM_num_pocs_physically_available, 4
g_SYSTEM_num_pocs_physically_available:
	.space	4
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_irrigation_system.c\000"
	.section	.text.SYSTEM_populate_poc_scrollbox,"ax",%progbits
	.align	2
	.type	SYSTEM_populate_poc_scrollbox, %function
SYSTEM_populate_poc_scrollbox:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_irrigation_system.c"
	.loc 1 81 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	mov	r3, r0
	strh	r3, [fp, #-12]	@ movhi
	.loc 1 84 0
	ldr	r3, .L4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L4+4
	mov	r3, #84
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 86 0
	ldrsh	r2, [fp, #-12]
	ldr	r3, .L4+8
	ldr	r3, [r3, r2, asl #2]
	str	r3, [fp, #-8]
	.loc 1 88 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L2
	.loc 1 98 0
	ldr	r0, [fp, #-8]
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r0, .L4+12
	mov	r1, r3
	mov	r2, #24
	bl	strlcpy
	.loc 1 100 0
	ldr	r0, [fp, #-8]
	bl	POC_get_GID_irrigation_system
	mov	r2, r0
	ldr	r3, .L4+16
	ldr	r3, [r3, #0]
	cmp	r2, r3
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, .L4+20
	str	r2, [r3, #0]
	b	.L3
.L2:
	.loc 1 104 0
	ldr	r0, .L4+4
	mov	r1, #104
	bl	Alert_group_not_found_with_filename
.L3:
	.loc 1 107 0
	ldr	r3, .L4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 108 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	POC_MENU_items
	.word	GuiVar_itmPOC
	.word	g_GROUP_ID
	.word	GuiVar_SystemPOCSelected
.LFE0:
	.size	SYSTEM_populate_poc_scrollbox, .-SYSTEM_populate_poc_scrollbox
	.section	.text.FDTO_SYSTEM_draw_poc_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_draw_poc_scrollbox, %function
FDTO_SYSTEM_draw_poc_scrollbox:
.LFB1:
	.loc 1 112 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 113 0
	ldr	r3, .L7
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L7+4
	mov	r3, #113
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 115 0
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	mov	r2, r0
	ldr	r3, .L7+8
	str	r2, [r3, #0]
	.loc 1 117 0
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	.loc 1 118 0
	ldr	r3, .L7+8
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L7+12
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #1
	ldr	r1, .L7+16
	bl	GuiLib_ScrollBox_Init
	.loc 1 120 0
	ldr	r3, .L7
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 122 0
	bl	GuiLib_Refresh
	.loc 1 123 0
	ldmfd	sp!, {fp, pc}
.L8:
	.align	2
.L7:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	g_SYSTEM_num_pocs_physically_available
	.word	g_SYSTEM_index_of_poc_when_dialog_displayed
	.word	SYSTEM_populate_poc_scrollbox
.LFE1:
	.size	FDTO_SYSTEM_draw_poc_scrollbox, .-FDTO_SYSTEM_draw_poc_scrollbox
	.section	.text.FDTO_SYSTEM_enter_poc_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_enter_poc_scrollbox, %function
FDTO_SYSTEM_enter_poc_scrollbox:
.LFB2:
	.loc 1 127 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #8
.LCFI7:
	str	r0, [fp, #-8]
	.loc 1 128 0
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	.loc 1 130 0
	ldr	r3, .L12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L12+4
	mov	r3, #130
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 132 0
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	mov	r2, r0
	ldr	r3, .L12+8
	str	r2, [r3, #0]
	.loc 1 134 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L10
	.loc 1 136 0
	ldr	r3, .L12+8
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #1
	ldr	r1, .L12+12
	mov	r2, r3
	mov	r3, #0
	bl	GuiLib_ScrollBox_Init
	b	.L11
.L10:
	.loc 1 140 0
	ldr	r3, .L12+8
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L12+8
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r1, .L12+8
	ldr	r1, [r1, #0]
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r1, r1, asl #16
	mov	r1, r1, asr #16
	str	r1, [sp, #0]
	mov	r0, #1
	ldr	r1, .L12+12
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L11:
	.loc 1 143 0
	ldr	r3, .L12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 145 0
	mov	r0, #3
	bl	GuiLib_Cursor_Select
	.loc 1 146 0
	bl	GuiLib_Refresh
	.loc 1 147 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	g_SYSTEM_num_pocs_physically_available
	.word	SYSTEM_populate_poc_scrollbox
.LFE2:
	.size	FDTO_SYSTEM_enter_poc_scrollbox, .-FDTO_SYSTEM_enter_poc_scrollbox
	.section	.text.FDTO_SYSTEM_leave_poc_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_leave_poc_scrollbox, %function
FDTO_SYSTEM_leave_poc_scrollbox:
.LFB3:
	.loc 1 151 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	.loc 1 152 0
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	.loc 1 154 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L15+4
	mov	r3, #154
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 156 0
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	mov	r2, r0
	ldr	r3, .L15+8
	str	r2, [r3, #0]
	.loc 1 158 0
	ldr	r3, .L15+8
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #1
	ldr	r1, .L15+12
	mov	r2, r3
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init
	.loc 1 160 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 162 0
	mov	r0, #0
	mov	r1, #1
	bl	FDTO_Cursor_Select
	.loc 1 163 0
	ldmfd	sp!, {fp, pc}
.L16:
	.align	2
.L15:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	g_SYSTEM_num_pocs_physically_available
	.word	SYSTEM_populate_poc_scrollbox
.LFE3:
	.size	FDTO_SYSTEM_leave_poc_scrollbox, .-FDTO_SYSTEM_leave_poc_scrollbox
	.section	.text.SYSTEM_add_new_group,"ax",%progbits
	.align	2
	.type	SYSTEM_add_new_group, %function
SYSTEM_add_new_group:
.LFB4:
	.loc 1 195 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI10:
	add	fp, sp, #4
.LCFI11:
	.loc 1 196 0
	ldr	r3, .L18
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L18+4
	mov	r3, #196
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 198 0
	ldr	r2, .L18+8
	ldr	r3, .L18+12
	mov	r0, r2
	mov	r1, r3
	bl	nm_GROUP_create_new_group_from_UI
	.loc 1 200 0
	ldr	r3, .L18
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 201 0
	ldmfd	sp!, {fp, pc}
.L19:
	.align	2
.L18:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	nm_SYSTEM_create_new_group
	.word	SYSTEM_copy_group_into_guivars
.LFE4:
	.size	SYSTEM_add_new_group, .-SYSTEM_add_new_group
	.section	.text.SYSTEM_process_group,"ax",%progbits
	.align	2
	.type	SYSTEM_process_group, %function
SYSTEM_process_group:
.LFB5:
	.loc 1 218 0
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI12:
	add	fp, sp, #12
.LCFI13:
	sub	sp, sp, #64
.LCFI14:
	str	r0, [fp, #-64]
	str	r1, [fp, #-60]
	.loc 1 225 0
	ldr	r3, .L90
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L90+4
	cmp	r2, r3
	bne	.L82
.L22:
	.loc 1 228 0
	ldr	r3, [fp, #-64]
	cmp	r3, #67
	beq	.L23
	.loc 1 228 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-64]
	cmp	r3, #2
	bne	.L24
	ldr	r3, .L90+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #49
	bne	.L24
.L23:
	.loc 1 230 0 is_stmt 1
	bl	SYSTEM_extract_and_store_group_name_from_GuiVars
.L24:
	.loc 1 233 0
	sub	r1, fp, #64
	ldmia	r1, {r0-r1}
	ldr	r2, .L90+12
	bl	KEYBOARD_process_key
	.loc 1 234 0
	b	.L20
.L82:
	.loc 1 237 0
	ldr	r3, [fp, #-64]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L26
.L35:
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L32
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L32
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L33
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L34
	.word	.L26
	.word	.L26
	.word	.L26
	.word	.L34
.L29:
	.loc 1 240 0
	ldr	r3, .L90+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	beq	.L37
	cmp	r3, #3
	beq	.L38
	b	.L83
.L37:
	.loc 1 243 0
	mov	r0, #164
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	.loc 1 244 0
	b	.L39
.L38:
	.loc 1 247 0
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L90+16
	ldr	r3, [r3, r2, asl #2]
	str	r3, [fp, #-16]
	.loc 1 253 0
	ldr	r0, [fp, #-16]
	bl	POC_get_GID_irrigation_system
	mov	r2, r0
	ldr	r3, .L90+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L40
	.loc 1 255 0
	bl	bad_key_beep
	.loc 1 257 0
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L90+24
	str	r2, [r3, #0]
	.loc 1 261 0
	ldr	r0, .L90+28
	bl	DIALOG_draw_ok_dialog
	.loc 1 271 0
	b	.L39
.L40:
	.loc 1 265 0
	bl	good_key_beep
	.loc 1 268 0
	ldr	r3, .L90+20
	ldr	r4, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r5, r0
	ldr	r0, [fp, #-16]
	mov	r1, #2
	bl	POC_get_change_bits_ptr
	mov	r3, r0
	str	r5, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	mov	r1, r4
	mov	r2, #1
	mov	r3, #2
	bl	nm_POC_set_GID_irrigation_system
	.loc 1 269 0
	mov	r0, #1
	bl	SCROLL_BOX_redraw
	.loc 1 271 0
	b	.L39
.L83:
	.loc 1 274 0
	bl	bad_key_beep
	.loc 1 276 0
	b	.L20
.L39:
	b	.L20
.L34:
	.loc 1 280 0
	ldr	r3, .L90+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #3
	bne	.L84
.L43:
	.loc 1 283 0
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L90+16
	ldr	r3, [r3, r2, asl #2]
	str	r3, [fp, #-16]
	.loc 1 285 0
	ldr	r3, [fp, #-64]
	cmp	r3, #84
	bne	.L44
	.loc 1 287 0
	ldr	r0, [fp, #-16]
	bl	POC_get_GID_irrigation_system
	mov	r2, r0
	ldr	r3, .L90+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L45
	.loc 1 289 0
	bl	good_key_beep
	.loc 1 292 0
	ldr	r3, .L90+20
	ldr	r4, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r5, r0
	ldr	r0, [fp, #-16]
	mov	r1, #2
	bl	POC_get_change_bits_ptr
	mov	r3, r0
	str	r5, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	mov	r1, r4
	mov	r2, #1
	mov	r3, #2
	bl	nm_POC_set_GID_irrigation_system
	.loc 1 293 0
	mov	r0, #1
	bl	SCROLL_BOX_redraw
	.loc 1 318 0
	b	.L48
.L45:
	.loc 1 297 0
	bl	bad_key_beep
	.loc 1 318 0
	b	.L48
.L44:
	.loc 1 302 0
	bl	bad_key_beep
	.loc 1 308 0
	ldr	r0, [fp, #-16]
	bl	POC_get_GID_irrigation_system
	mov	r2, r0
	ldr	r3, .L90+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L89
	.loc 1 310 0
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L90+24
	str	r2, [r3, #0]
	.loc 1 314 0
	ldr	r0, .L90+28
	bl	DIALOG_draw_ok_dialog
	.loc 1 316 0
	b	.L89
.L84:
	.loc 1 321 0
	bl	bad_key_beep
	.loc 1 323 0
	b	.L20
.L89:
	.loc 1 316 0
	mov	r0, r0	@ nop
.L48:
	.loc 1 323 0
	b	.L20
.L32:
	.loc 1 329 0
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L90+24
	str	r2, [r3, #0]
	.loc 1 331 0
	ldr	r4, [fp, #-64]
	bl	SYSTEM_num_systems_in_use
	mov	r2, r0
	ldr	r3, .L90+32
	ldr	r1, .L90+36
	str	r1, [sp, #0]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	ldr	r3, .L90+40
	bl	GROUP_process_NEXT_and_PREV
	.loc 1 334 0
	mov	r3, #1
	str	r3, [fp, #-56]
	.loc 1 335 0
	ldr	r3, .L90+44
	str	r3, [fp, #-36]
	.loc 1 336 0
	sub	r3, fp, #56
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 337 0
	b	.L20
.L31:
	.loc 1 340 0
	ldr	r3, .L90+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #3
	bne	.L85
.L50:
	.loc 1 343 0
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	str	r3, [fp, #-20]
	.loc 1 345 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	blt	.L51
	.loc 1 347 0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetTopLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L52
	.loc 1 349 0
	mov	r3, #1
	str	r3, [fp, #-56]
	.loc 1 350 0
	ldr	r3, .L90+48
	str	r3, [fp, #-36]
	.loc 1 351 0
	sub	r3, fp, #56
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 362 0
	b	.L54
.L52:
	.loc 1 355 0
	ldr	r3, [fp, #-64]
	mov	r0, #1
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	.loc 1 362 0
	b	.L54
.L51:
	.loc 1 360 0
	bl	bad_key_beep
	.loc 1 362 0
	b	.L54
.L85:
	.loc 1 365 0
	bl	bad_key_beep
	.loc 1 367 0
	b	.L20
.L54:
	b	.L20
.L27:
	.loc 1 370 0
	ldr	r3, .L90+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	beq	.L56
	cmp	r3, #3
	beq	.L57
	b	.L86
.L56:
	.loc 1 373 0
	ldr	r3, .L90+52
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L58
	.loc 1 375 0
	ldr	r3, .L90+56
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L59
	.loc 1 377 0
	bl	good_key_beep
	.loc 1 379 0
	mov	r3, #2
	str	r3, [fp, #-56]
	.loc 1 380 0
	ldr	r3, .L90+60
	str	r3, [fp, #-36]
	.loc 1 381 0
	mov	r3, #1
	str	r3, [fp, #-32]
	.loc 1 382 0
	sub	r3, fp, #56
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 398 0
	b	.L61
.L59:
	.loc 1 388 0
	bl	bad_key_beep
	.loc 1 398 0
	b	.L61
.L58:
	.loc 1 396 0
	ldr	r0, .L90+64
	bl	DIALOG_draw_ok_dialog
	.loc 1 398 0
	b	.L61
.L57:
	.loc 1 401 0
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	str	r3, [fp, #-20]
	.loc 1 403 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	blt	.L62
	.loc 1 405 0
	ldr	r3, .L90+56
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	mov	r2, r3
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L63
	.loc 1 407 0
	bl	bad_key_beep
	.loc 1 418 0
	b	.L61
.L63:
	.loc 1 411 0
	ldr	r3, [fp, #-64]
	mov	r0, #1
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	.loc 1 418 0
	b	.L61
.L62:
	.loc 1 416 0
	bl	bad_key_beep
	.loc 1 418 0
	b	.L61
.L86:
	.loc 1 421 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 423 0
	b	.L20
.L61:
	b	.L20
.L28:
	.loc 1 426 0
	ldr	r3, .L90+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	beq	.L66
	cmp	r3, #3
	beq	.L67
	b	.L87
.L66:
	.loc 1 429 0
	ldr	r0, .L90+68
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 430 0
	b	.L68
.L67:
	.loc 1 433 0
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	str	r3, [fp, #-20]
	.loc 1 435 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	blt	.L69
	.loc 1 437 0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetTopLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L70
	.loc 1 439 0
	mov	r3, #1
	str	r3, [fp, #-56]
	.loc 1 440 0
	ldr	r3, .L90+48
	str	r3, [fp, #-36]
	.loc 1 441 0
	sub	r3, fp, #56
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 456 0
	b	.L68
.L70:
	.loc 1 447 0
	mov	r3, #4
	str	r3, [fp, #-64]
	.loc 1 449 0
	ldr	r3, [fp, #-64]
	mov	r0, #1
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	.loc 1 456 0
	b	.L68
.L69:
	.loc 1 454 0
	bl	bad_key_beep
	.loc 1 456 0
	b	.L68
.L87:
	.loc 1 459 0
	bl	bad_key_beep
	.loc 1 461 0
	b	.L20
.L68:
	b	.L20
.L30:
	.loc 1 464 0
	ldr	r3, .L90+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	beq	.L73
	cmp	r3, #3
	beq	.L74
	b	.L88
.L73:
	.loc 1 467 0
	ldr	r3, .L90+52
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L75
	.loc 1 469 0
	ldr	r3, .L90+56
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L76
	.loc 1 471 0
	bl	good_key_beep
	.loc 1 473 0
	mov	r3, #2
	str	r3, [fp, #-56]
	.loc 1 474 0
	ldr	r3, .L90+60
	str	r3, [fp, #-36]
	.loc 1 475 0
	mov	r3, #1
	str	r3, [fp, #-32]
	.loc 1 476 0
	sub	r3, fp, #56
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 492 0
	b	.L78
.L76:
	.loc 1 482 0
	bl	bad_key_beep
	.loc 1 492 0
	b	.L78
.L75:
	.loc 1 490 0
	ldr	r0, .L90+64
	bl	DIALOG_draw_ok_dialog
	.loc 1 492 0
	b	.L78
.L74:
	.loc 1 495 0
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	str	r3, [fp, #-20]
	.loc 1 497 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	blt	.L79
	.loc 1 499 0
	ldr	r3, .L90+56
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	mov	r2, r3
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L80
	.loc 1 501 0
	bl	bad_key_beep
	.loc 1 516 0
	b	.L78
.L80:
	.loc 1 507 0
	mov	r3, #0
	str	r3, [fp, #-64]
	.loc 1 509 0
	ldr	r3, [fp, #-64]
	mov	r0, #1
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	.loc 1 516 0
	b	.L78
.L79:
	.loc 1 514 0
	bl	bad_key_beep
	.loc 1 516 0
	b	.L78
.L88:
	.loc 1 519 0
	bl	bad_key_beep
	.loc 1 521 0
	b	.L20
.L78:
	b	.L20
.L33:
	.loc 1 524 0
	ldr	r0, .L90+68
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 525 0
	b	.L20
.L26:
	.loc 1 528 0
	sub	r1, fp, #64
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L20:
	.loc 1 531 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L91:
	.align	2
.L90:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_SYSTEM_draw_menu
	.word	POC_MENU_items
	.word	g_GROUP_ID
	.word	g_SYSTEM_index_of_poc_when_dialog_displayed
	.word	590
	.word	SYSTEM_extract_and_store_changes_from_GuiVars
	.word	SYSTEM_copy_group_into_guivars
	.word	SYSTEM_get_group_at_this_index
	.word	FDTO_SYSTEM_draw_poc_scrollbox
	.word	FDTO_SYSTEM_leave_poc_scrollbox
	.word	g_SYSTEM_is_watering
	.word	g_SYSTEM_num_pocs_physically_available
	.word	FDTO_SYSTEM_enter_poc_scrollbox
	.word	613
	.word	FDTO_SYSTEM_return_to_menu
.LFE5:
	.size	SYSTEM_process_group, .-SYSTEM_process_group
	.section	.text.FDTO_SYSTEM_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_return_to_menu, %function
FDTO_SYSTEM_return_to_menu:
.LFB6:
	.loc 1 555 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	.loc 1 556 0
	ldr	r3, .L93
	ldr	r0, .L93+4
	mov	r1, r3
	bl	FDTO_GROUP_return_to_menu
	.loc 1 558 0
	ldr	r3, .L93+8
	mvn	r2, #0
	str	r2, [r3, #0]
	.loc 1 562 0
	bl	FDTO_SYSTEM_draw_poc_scrollbox
	.loc 1 563 0
	ldmfd	sp!, {fp, pc}
.L94:
	.align	2
.L93:
	.word	SYSTEM_extract_and_store_changes_from_GuiVars
	.word	g_SYSTEM_editing_group
	.word	g_SYSTEM_index_of_poc_when_dialog_displayed
.LFE6:
	.size	FDTO_SYSTEM_return_to_menu, .-FDTO_SYSTEM_return_to_menu
	.section	.text.FDTO_SYSTEM_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_draw_menu
	.type	FDTO_SYSTEM_draw_menu, %function
FDTO_SYSTEM_draw_menu:
.LFB7:
	.loc 1 585 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI17:
	add	fp, sp, #12
.LCFI18:
	sub	sp, sp, #16
.LCFI19:
	str	r0, [fp, #-16]
	.loc 1 586 0
	ldr	r3, .L97
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L97+4
	ldr	r3, .L97+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 588 0
	bl	SYSTEM_num_systems_in_use
	mov	r4, r0
	ldr	r5, .L97+12
	bl	SYSTEM_num_systems_in_use
	mov	r3, r0
	cmp	r3, #3
	movhi	r3, #0
	movls	r3, #1
	ldr	r2, .L97+16
	str	r2, [sp, #0]
	str	r5, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	ldr	r1, .L97+20
	mov	r2, r4
	mov	r3, #35
	bl	FDTO_GROUP_draw_menu
	.loc 1 590 0
	ldr	r3, .L97
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 592 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L96
	.loc 1 594 0
	ldr	r3, .L97+24
	mvn	r2, #0
	str	r2, [r3, #0]
.L96:
	.loc 1 598 0
	bl	FDTO_SYSTEM_draw_poc_scrollbox
	.loc 1 599 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L98:
	.align	2
.L97:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	586
	.word	SYSTEM_copy_group_into_guivars
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	g_SYSTEM_editing_group
	.word	g_SYSTEM_index_of_poc_when_dialog_displayed
.LFE7:
	.size	FDTO_SYSTEM_draw_menu, .-FDTO_SYSTEM_draw_menu
	.section	.text.SYSTEM_process_menu,"ax",%progbits
	.align	2
	.global	SYSTEM_process_menu
	.type	SYSTEM_process_menu, %function
SYSTEM_process_menu:
.LFB8:
	.loc 1 618 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI20:
	add	fp, sp, #8
.LCFI21:
	sub	sp, sp, #24
.LCFI22:
	str	r0, [fp, #-16]
	str	r1, [fp, #-12]
	.loc 1 619 0
	ldr	r3, .L101
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L101+4
	ldr	r3, .L101+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 621 0
	bl	SYSTEM_num_systems_in_use
	mov	r3, r0
	ldr	r0, .L101+12
	ldr	r1, .L101+16
	ldr	r2, .L101+20
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, .L101+24
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L101+28
	bl	GROUP_process_menu
	.loc 1 624 0
	ldr	r3, [fp, #-16]
	cmp	r3, #67
	beq	.L100
	.loc 1 624 0 is_stmt 0 discriminator 1
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r4, r3
	bl	SYSTEM_num_systems_in_use
	mov	r3, r0
	cmp	r4, r3
	bcs	.L100
	ldr	r3, .L101+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L100
	.loc 1 626 0 is_stmt 1
	mov	r0, #1
	bl	SCROLL_BOX_redraw
.L100:
	.loc 1 629 0
	ldr	r3, .L101
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 630 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L102:
	.align	2
.L101:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	619
	.word	SYSTEM_process_group
	.word	SYSTEM_add_new_group
	.word	SYSTEM_copy_group_into_guivars
	.word	SYSTEM_get_group_at_this_index
	.word	g_SYSTEM_editing_group
.LFE8:
	.size	SYSTEM_process_menu, .-SYSTEM_process_menu
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI10-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI12-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI15-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI17-.LFB7
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI20-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/poc.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_poc.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x54a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF68
	.byte	0x1
	.4byte	.LASF69
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x53
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x67
	.4byte	0x77
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x5
	.byte	0x4
	.4byte	0x9d
	.uleb128 0x6
	.4byte	0xa4
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x4
	.byte	0x57
	.4byte	0xa4
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x5
	.byte	0x4c
	.4byte	0xb8
	.uleb128 0x9
	.4byte	0x33
	.4byte	0xde
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0x103
	.uleb128 0xc
	.4byte	.LASF17
	.byte	0x6
	.byte	0x7e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF18
	.byte	0x6
	.byte	0x80
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x6
	.byte	0x82
	.4byte	0xde
	.uleb128 0x9
	.4byte	0x5a
	.4byte	0x11e
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x7
	.byte	0xda
	.4byte	0x129
	.uleb128 0xd
	.4byte	.LASF20
	.byte	0x1
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF21
	.uleb128 0x9
	.4byte	0x5a
	.4byte	0x146
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF22
	.uleb128 0xb
	.byte	0x24
	.byte	0x8
	.byte	0x78
	.4byte	0x1d4
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x8
	.byte	0x7b
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x8
	.byte	0x83
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x8
	.byte	0x86
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x8
	.byte	0x88
	.4byte	0x1e5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x8
	.byte	0x8d
	.4byte	0x1f7
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x8
	.byte	0x92
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x8
	.byte	0x96
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x8
	.byte	0x9a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x8
	.byte	0x9c
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	0x1e0
	.uleb128 0xf
	.4byte	0x1e0
	.byte	0
	.uleb128 0x10
	.4byte	0x48
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1d4
	.uleb128 0xe
	.byte	0x1
	.4byte	0x1f7
	.uleb128 0xf
	.4byte	0x103
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1eb
	.uleb128 0x3
	.4byte	.LASF32
	.byte	0x8
	.byte	0x9e
	.4byte	0x14d
	.uleb128 0xb
	.byte	0x4
	.byte	0x9
	.byte	0x16
	.4byte	0x21f
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x9
	.byte	0x18
	.4byte	0xa4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF34
	.byte	0x9
	.byte	0x1a
	.4byte	0x208
	.uleb128 0x11
	.4byte	.LASF35
	.byte	0x1
	.byte	0x50
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x25f
	.uleb128 0x12
	.4byte	.LASF37
	.byte	0x1
	.byte	0x50
	.4byte	0x1e0
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF44
	.byte	0x1
	.byte	0x52
	.4byte	0x25f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x11e
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x1
	.byte	0x6f
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x11
	.4byte	.LASF36
	.byte	0x1
	.byte	0x7e
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x2a0
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.byte	0x7e
	.4byte	0x2a0
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.4byte	0x8c
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x1
	.byte	0x96
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x1
	.byte	0xc2
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x11
	.4byte	.LASF42
	.byte	0x1
	.byte	0xd9
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x31f
	.uleb128 0x12
	.4byte	.LASF43
	.byte	0x1
	.byte	0xd9
	.4byte	0x103
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x15
	.ascii	"lde\000"
	.byte	0x1
	.byte	0xdb
	.4byte	0x1fd
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x13
	.4byte	.LASF44
	.byte	0x1
	.byte	0xdd
	.4byte	0x25f
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF45
	.byte	0x1
	.byte	0xdf
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x16
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x22a
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x248
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x35e
	.uleb128 0x18
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x248
	.4byte	0x2a0
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x269
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x388
	.uleb128 0x18
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x269
	.4byte	0x388
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x10
	.4byte	0x103
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x39d
	.uleb128 0xa
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x19
	.4byte	.LASF50
	.byte	0xa
	.2byte	0x26d
	.4byte	0x38d
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF51
	.byte	0xa
	.2byte	0x46a
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF52
	.byte	0xb
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF53
	.byte	0xb
	.2byte	0x132
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF54
	.byte	0xc
	.byte	0x30
	.4byte	0x3e6
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF55
	.byte	0xc
	.byte	0x34
	.4byte	0x3fc
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF56
	.byte	0xc
	.byte	0x36
	.4byte	0x412
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF57
	.byte	0xc
	.byte	0x38
	.4byte	0x428
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x1a
	.4byte	.LASF58
	.byte	0xd
	.byte	0x63
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF59
	.byte	0xe
	.byte	0x33
	.4byte	0x44b
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x10
	.4byte	0x10e
	.uleb128 0x13
	.4byte	.LASF60
	.byte	0xe
	.byte	0x3f
	.4byte	0x461
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x10
	.4byte	0x136
	.uleb128 0x1a
	.4byte	.LASF61
	.byte	0xf
	.byte	0xc6
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF62
	.byte	0xf
	.byte	0xc9
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x21f
	.4byte	0x490
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF63
	.byte	0x9
	.byte	0x1c
	.4byte	0x480
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF64
	.byte	0x1
	.byte	0x3b
	.4byte	0x8c
	.byte	0x5
	.byte	0x3
	.4byte	g_SYSTEM_is_watering
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.byte	0x3d
	.4byte	0x8c
	.byte	0x5
	.byte	0x3
	.4byte	g_SYSTEM_editing_group
	.uleb128 0x13
	.4byte	.LASF66
	.byte	0x1
	.byte	0x3f
	.4byte	0x5a
	.byte	0x5
	.byte	0x3
	.4byte	g_SYSTEM_index_of_poc_when_dialog_displayed
	.uleb128 0x13
	.4byte	.LASF67
	.byte	0x1
	.byte	0x45
	.4byte	0x5a
	.byte	0x5
	.byte	0x3
	.4byte	g_SYSTEM_num_pocs_physically_available
	.uleb128 0x19
	.4byte	.LASF50
	.byte	0xa
	.2byte	0x26d
	.4byte	0x38d
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF51
	.byte	0xa
	.2byte	0x46a
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF52
	.byte	0xb
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF53
	.byte	0xb
	.2byte	0x132
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF58
	.byte	0xd
	.byte	0x63
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF61
	.byte	0xf
	.byte	0xc6
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF62
	.byte	0xf
	.byte	0xc9
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF63
	.byte	0x9
	.byte	0x1c
	.4byte	0x480
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI11
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI13
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI18
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI21
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF68:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF36:
	.ascii	"FDTO_SYSTEM_enter_poc_scrollbox\000"
.LASF5:
	.ascii	"short int\000"
.LASF14:
	.ascii	"portTickType\000"
.LASF23:
	.ascii	"_01_command\000"
.LASF37:
	.ascii	"pline_index_0\000"
.LASF62:
	.ascii	"list_poc_recursive_MUTEX\000"
.LASF46:
	.ascii	"FDTO_SYSTEM_return_to_menu\000"
.LASF17:
	.ascii	"keycode\000"
.LASF24:
	.ascii	"_02_menu\000"
.LASF38:
	.ascii	"pmove_to_top_line\000"
.LASF9:
	.ascii	"INT_32\000"
.LASF65:
	.ascii	"g_SYSTEM_editing_group\000"
.LASF21:
	.ascii	"float\000"
.LASF33:
	.ascii	"poc_ptr\000"
.LASF34:
	.ascii	"POC_MENU_SCROLL_BOX_ITEM\000"
.LASF11:
	.ascii	"long long int\000"
.LASF39:
	.ascii	"FDTO_SYSTEM_draw_poc_scrollbox\000"
.LASF51:
	.ascii	"GuiVar_SystemPOCSelected\000"
.LASF64:
	.ascii	"g_SYSTEM_is_watering\000"
.LASF45:
	.ascii	"lactive_line\000"
.LASF16:
	.ascii	"xSemaphoreHandle\000"
.LASF15:
	.ascii	"xQueueHandle\000"
.LASF35:
	.ascii	"SYSTEM_populate_poc_scrollbox\000"
.LASF49:
	.ascii	"SYSTEM_process_menu\000"
.LASF29:
	.ascii	"_06_u32_argument1\000"
.LASF27:
	.ascii	"key_process_func_ptr\000"
.LASF31:
	.ascii	"_08_screen_to_draw\000"
.LASF47:
	.ascii	"pcomplete_redraw\000"
.LASF67:
	.ascii	"g_SYSTEM_num_pocs_physically_available\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF50:
	.ascii	"GuiVar_itmPOC\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF3:
	.ascii	"signed char\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF55:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF61:
	.ascii	"list_system_recursive_MUTEX\000"
.LASF63:
	.ascii	"POC_MENU_items\000"
.LASF66:
	.ascii	"g_SYSTEM_index_of_poc_when_dialog_displayed\000"
.LASF25:
	.ascii	"_03_structure_to_draw\000"
.LASF52:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF69:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_irrigation_system.c\000"
.LASF1:
	.ascii	"char\000"
.LASF13:
	.ascii	"long int\000"
.LASF42:
	.ascii	"SYSTEM_process_group\000"
.LASF57:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF58:
	.ascii	"g_GROUP_ID\000"
.LASF26:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF48:
	.ascii	"FDTO_SYSTEM_draw_menu\000"
.LASF44:
	.ascii	"lpoc\000"
.LASF56:
	.ascii	"GuiFont_DecimalChar\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF60:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF28:
	.ascii	"_04_func_ptr\000"
.LASF19:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF30:
	.ascii	"_07_u32_argument2\000"
.LASF53:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF41:
	.ascii	"SYSTEM_add_new_group\000"
.LASF18:
	.ascii	"repeats\000"
.LASF54:
	.ascii	"GuiFont_LanguageActive\000"
.LASF59:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF20:
	.ascii	"POC_GROUP_STRUCT\000"
.LASF43:
	.ascii	"pkey_event\000"
.LASF22:
	.ascii	"double\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF32:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF40:
	.ascii	"FDTO_SYSTEM_leave_poc_scrollbox\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
