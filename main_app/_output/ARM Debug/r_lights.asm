	.file	"r_lights.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_LIGHTS_REPORT_line_count,"aw",%nobits
	.align	2
	.type	g_LIGHTS_REPORT_line_count, %object
	.size	g_LIGHTS_REPORT_line_count, 4
g_LIGHTS_REPORT_line_count:
	.space	4
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_lights.c\000"
	.section	.text.FDTO_LIGHTS_REPORT_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_LIGHTS_REPORT_redraw_scrollbox
	.type	FDTO_LIGHTS_REPORT_redraw_scrollbox, %function
FDTO_LIGHTS_REPORT_redraw_scrollbox:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_lights.c"
	.loc 1 58 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	.loc 1 62 0
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L2+4
	mov	r3, #62
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 65 0
	ldr	r3, .L2+8
	ldr	r2, [r3, #0]
	ldr	r3, .L2+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_light_struct_into_guivars
	.loc 1 69 0
	ldr	r3, .L2+8
	ldr	r2, [r3, #0]
	ldr	r3, .L2+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	mov	r1, #1
	bl	LIGHTS_populate_group_name
	.loc 1 71 0
	ldr	r3, .L2+8
	ldr	r2, [r3, #0]
	ldr	r3, .L2+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines
	str	r0, [fp, #-8]
	.loc 1 75 0
	ldr	r3, .L2+16
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	moveq	r3, #0
	movne	r3, #1
	mov	r0, #0
	ldr	r1, [fp, #-8]
	mov	r2, r3
	bl	FDTO_SCROLL_BOX_redraw_retaining_topline
	.loc 1 77 0
	ldr	r3, .L2+16
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 79 0
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 81 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L3:
	.align	2
.L2:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	g_LIGHTS_REPORT_line_count
.LFE0:
	.size	FDTO_LIGHTS_REPORT_redraw_scrollbox, .-FDTO_LIGHTS_REPORT_redraw_scrollbox
	.section	.text.FDTO_LIGHTS_REPORT_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_LIGHTS_REPORT_draw_report
	.type	FDTO_LIGHTS_REPORT_draw_report, %function
FDTO_LIGHTS_REPORT_draw_report:
.LFB1:
	.loc 1 96 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 97 0
	mov	r0, #86
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 100 0
	ldr	r3, .L6
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L6+4
	mov	r3, #100
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 102 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L5
	.loc 1 104 0
	bl	nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars
.L5:
	.loc 1 108 0
	ldr	r3, .L6+8
	ldr	r2, [r3, #0]
	ldr	r3, .L6+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_light_struct_into_guivars
	.loc 1 112 0
	ldr	r3, .L6+8
	ldr	r2, [r3, #0]
	ldr	r3, .L6+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	mov	r1, #1
	bl	LIGHTS_populate_group_name
	.loc 1 116 0
	ldr	r3, .L6+8
	ldr	r2, [r3, #0]
	ldr	r3, .L6+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines
	mov	r2, r0
	ldr	r3, .L6+16
	str	r2, [r3, #0]
	.loc 1 120 0
	ldr	r3, .L6+16
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, .L6+20
	bl	FDTO_REPORTS_draw_report
	.loc 1 122 0
	ldr	r3, .L6
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 124 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L7:
	.align	2
.L6:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	g_LIGHTS_REPORT_line_count
	.word	LIGHTS_REPORT_draw_scroll_line
.LFE1:
	.size	FDTO_LIGHTS_REPORT_draw_report, .-FDTO_LIGHTS_REPORT_draw_report
	.section	.text.LIGHTS_REPORT_process_report,"ax",%progbits
	.align	2
	.global	LIGHTS_REPORT_process_report
	.type	LIGHTS_REPORT_process_report, %function
LIGHTS_REPORT_process_report:
.LFB2:
	.loc 1 140 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #44
.LCFI8:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	.loc 1 143 0
	ldr	r3, [fp, #-48]
	cmp	r3, #16
	beq	.L10
	cmp	r3, #20
	bne	.L14
.L10:
	.loc 1 147 0
	bl	good_key_beep
	.loc 1 149 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L15+4
	mov	r3, #149
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 151 0
	ldr	r3, [fp, #-48]
	cmp	r3, #20
	bne	.L11
	.loc 1 153 0
	ldr	r0, .L15+8
	ldr	r1, .L15+12
	bl	nm_LIGHTS_get_next_available_light
	b	.L12
.L11:
	.loc 1 157 0
	ldr	r0, .L15+8
	ldr	r1, .L15+12
	bl	nm_LIGHTS_get_prev_available_light
.L12:
	.loc 1 160 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 162 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 163 0
	ldr	r3, .L15+16
	str	r3, [fp, #-20]
	.loc 1 164 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 165 0
	b	.L8
.L14:
	.loc 1 171 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #0
	bl	REPORTS_process_report
.L8:
	.loc 1 173 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L16:
	.align	2
.L15:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	FDTO_LIGHTS_REPORT_redraw_scrollbox
.LFE2:
	.size	LIGHTS_REPORT_process_report, .-LIGHTS_REPORT_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x30d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF44
	.byte	0x1
	.4byte	.LASF45
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8b
	.uleb128 0x6
	.4byte	0x92
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x35
	.4byte	0x92
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x4
	.byte	0x57
	.4byte	0x99
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x5
	.byte	0x4c
	.4byte	0xad
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0xd3
	.uleb128 0xa
	.4byte	0x92
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0xf8
	.uleb128 0xc
	.4byte	.LASF16
	.byte	0x6
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF17
	.byte	0x6
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x6
	.byte	0x82
	.4byte	0xd3
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF19
	.uleb128 0xb
	.byte	0x24
	.byte	0x7
	.byte	0x78
	.4byte	0x191
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x7
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x7
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x7
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x7
	.byte	0x88
	.4byte	0x1a2
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x7
	.byte	0x8d
	.4byte	0x1b4
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x7
	.byte	0x92
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x7
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x7
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x7
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	0x19d
	.uleb128 0xe
	.4byte	0x19d
	.byte	0
	.uleb128 0xf
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x191
	.uleb128 0xd
	.byte	0x1
	.4byte	0x1b4
	.uleb128 0xe
	.4byte	0xf8
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1a8
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x7
	.byte	0x9e
	.4byte	0x10a
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.byte	0x39
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1ed
	.uleb128 0x11
	.4byte	.LASF35
	.byte	0x1
	.byte	0x3b
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.byte	0x5f
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x215
	.uleb128 0x12
	.4byte	.LASF33
	.byte	0x1
	.byte	0x5f
	.4byte	0x215
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.4byte	0x7a
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.byte	0x8b
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x250
	.uleb128 0x12
	.4byte	.LASF34
	.byte	0x1
	.byte	0x8b
	.4byte	0x250
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x8d
	.4byte	0x1ba
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xf
	.4byte	0xf8
	.uleb128 0x14
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x289
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x8
	.2byte	0x291
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF38
	.byte	0x9
	.byte	0x30
	.4byte	0x282
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x11
	.4byte	.LASF39
	.byte	0x9
	.byte	0x34
	.4byte	0x298
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x11
	.4byte	.LASF40
	.byte	0x9
	.byte	0x36
	.4byte	0x2ae
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x11
	.4byte	.LASF41
	.byte	0x9
	.byte	0x38
	.4byte	0x2c4
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x15
	.4byte	.LASF42
	.byte	0xa
	.byte	0xfe
	.4byte	0xb8
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x1
	.byte	0x28
	.4byte	0x53
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_REPORT_line_count
	.uleb128 0x14
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x289
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x8
	.2byte	0x291
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF42
	.byte	0xa
	.byte	0xfe
	.4byte	0xb8
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF38:
	.ascii	"GuiFont_LanguageActive\000"
.LASF25:
	.ascii	"_04_func_ptr\000"
.LASF13:
	.ascii	"portTickType\000"
.LASF21:
	.ascii	"_02_menu\000"
.LASF24:
	.ascii	"key_process_func_ptr\000"
.LASF42:
	.ascii	"list_lights_recursive_MUTEX\000"
.LASF33:
	.ascii	"pcomplete_redraw\000"
.LASF30:
	.ascii	"FDTO_LIGHTS_REPORT_redraw_scrollbox\000"
.LASF15:
	.ascii	"xSemaphoreHandle\000"
.LASF18:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF23:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF28:
	.ascii	"_08_screen_to_draw\000"
.LASF37:
	.ascii	"GuiVar_LightsOutputIndex_0\000"
.LASF35:
	.ascii	"lcurrent_line_count\000"
.LASF19:
	.ascii	"float\000"
.LASF14:
	.ascii	"xQueueHandle\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF16:
	.ascii	"keycode\000"
.LASF20:
	.ascii	"_01_command\000"
.LASF29:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF44:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF39:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF43:
	.ascii	"g_LIGHTS_REPORT_line_count\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF34:
	.ascii	"pkey_event\000"
.LASF0:
	.ascii	"char\000"
.LASF17:
	.ascii	"repeats\000"
.LASF31:
	.ascii	"FDTO_LIGHTS_REPORT_draw_report\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF45:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_lights.c\000"
.LASF36:
	.ascii	"GuiVar_LightsBoxIndex_0\000"
.LASF9:
	.ascii	"long long int\000"
.LASF27:
	.ascii	"_07_u32_argument2\000"
.LASF41:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF40:
	.ascii	"GuiFont_DecimalChar\000"
.LASF4:
	.ascii	"short int\000"
.LASF22:
	.ascii	"_03_structure_to_draw\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF12:
	.ascii	"long int\000"
.LASF26:
	.ascii	"_06_u32_argument1\000"
.LASF2:
	.ascii	"signed char\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF32:
	.ascii	"LIGHTS_REPORT_process_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
