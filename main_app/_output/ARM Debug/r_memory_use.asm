	.file	"r_memory_use.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section .rodata
	.align	2
.LC0:
	.ascii	"%6lu\000"
	.align	2
.LC1:
	.ascii	"%4u\000"
	.align	2
.LC2:
	.ascii	"%4lu\000"
	.align	2
.LC3:
	.ascii	"%ld bytes in %ld allocs\000"
	.align	2
.LC4:
	.ascii	"(max %ld out of %ld)\000"
	.section	.text.FDTO_MEM_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_MEM_draw_report
	.type	FDTO_MEM_draw_report, %function
FDTO_MEM_draw_report:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_memory_use.c"
	.loc 1 41 0
	@ args = 0, pretend = 0, frame = 80
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI0:
	add	fp, sp, #8
.LCFI1:
	sub	sp, sp, #124
.LCFI2:
	str	r0, [fp, #-88]
	.loc 1 54 0
	mov	r3, #0
	mov	r4, #0
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	.loc 1 56 0
	ldr	r3, [fp, #-88]
	cmp	r3, #1
	bne	.L2
	.loc 1 58 0
	mov	r0, #87
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
.L2:
	.loc 1 67 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L3
.L4:
	.loc 1 69 0 discriminator 2
	ldr	r3, .L5
	ldr	r2, [fp, #-20]
	ldr	r3, [r3, r2, asl #3]
	ldr	r0, .L5
	ldr	r1, [fp, #-20]
	mov	r2, #4
	mov	r1, r1, asl #3
	add	r1, r0, r1
	add	r2, r1, r2
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	mov	r3, r2
	mov	r4, #0
	sub	r2, fp, #16
	ldmia	r2, {r1-r2}
	adds	r3, r3, r1
	adc	r4, r4, r2
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	.loc 1 72 0 discriminator 2
	ldr	r3, .L5
	ldr	r2, [fp, #-20]
	ldr	r3, [r3, r2, asl #3]
	sub	r2, fp, #84
	mov	r0, r2
	mov	r1, #64
	ldr	r2, .L5+4
	bl	snprintf
	.loc 1 73 0 discriminator 2
	ldr	r3, [fp, #-20]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #4
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r3, #38
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r2, fp, #84
	str	r2, [sp, #0]
	mov	r2, #3
	str	r2, [sp, #4]
	mov	r2, #3
	str	r2, [sp, #8]
	mov	r2, #0
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	mov	r2, #0
	str	r2, [sp, #20]
	mov	r2, #0
	str	r2, [sp, #24]
	mov	r2, #0
	str	r2, [sp, #28]
	mov	r2, #0
	str	r2, [sp, #32]
	mov	r2, #0
	str	r2, [sp, #36]
	mov	r2, #15
	str	r2, [sp, #40]
	mov	r0, #105
	mov	r1, r3
	mov	r2, #1
	mvn	r3, #0
	bl	GuiLib_DrawStr
	.loc 1 76 0 discriminator 2
	ldr	r3, .L5+8
	ldr	r2, [fp, #-20]
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #84
	mov	r0, r2
	mov	r1, #64
	ldr	r2, .L5+12
	bl	snprintf
	.loc 1 77 0 discriminator 2
	ldr	r3, [fp, #-20]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #4
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r3, #38
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r2, fp, #84
	str	r2, [sp, #0]
	mov	r2, #3
	str	r2, [sp, #4]
	mov	r2, #3
	str	r2, [sp, #8]
	mov	r2, #0
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	mov	r2, #0
	str	r2, [sp, #20]
	mov	r2, #0
	str	r2, [sp, #24]
	mov	r2, #0
	str	r2, [sp, #28]
	mov	r2, #0
	str	r2, [sp, #32]
	mov	r2, #0
	str	r2, [sp, #36]
	mov	r2, #15
	str	r2, [sp, #40]
	mov	r0, #143
	mov	r1, r3
	mov	r2, #1
	mvn	r3, #0
	bl	GuiLib_DrawStr
	.loc 1 80 0 discriminator 2
	ldr	r3, .L5+16
	ldr	r2, [fp, #-20]
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #84
	mov	r0, r2
	mov	r1, #64
	ldr	r2, .L5+12
	bl	snprintf
	.loc 1 81 0 discriminator 2
	ldr	r3, [fp, #-20]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #4
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r3, #38
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r2, fp, #84
	str	r2, [sp, #0]
	mov	r2, #3
	str	r2, [sp, #4]
	mov	r2, #3
	str	r2, [sp, #8]
	mov	r2, #0
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	mov	r2, #0
	str	r2, [sp, #20]
	mov	r2, #0
	str	r2, [sp, #24]
	mov	r2, #0
	str	r2, [sp, #28]
	mov	r2, #0
	str	r2, [sp, #32]
	mov	r2, #0
	str	r2, [sp, #36]
	mov	r2, #15
	str	r2, [sp, #40]
	mov	r0, #201
	mov	r1, r3
	mov	r2, #1
	mvn	r3, #0
	bl	GuiLib_DrawStr
	.loc 1 84 0 discriminator 2
	ldr	r1, .L5
	ldr	r2, [fp, #-20]
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	sub	r2, fp, #84
	mov	r0, r2
	mov	r1, #64
	ldr	r2, .L5+20
	bl	snprintf
	.loc 1 85 0 discriminator 2
	ldr	r3, [fp, #-20]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #4
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r3, #38
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r2, fp, #84
	str	r2, [sp, #0]
	mov	r2, #3
	str	r2, [sp, #4]
	mov	r2, #3
	str	r2, [sp, #8]
	mov	r2, #0
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	mov	r2, #0
	str	r2, [sp, #20]
	mov	r2, #0
	str	r2, [sp, #24]
	mov	r2, #0
	str	r2, [sp, #28]
	mov	r2, #0
	str	r2, [sp, #32]
	mov	r2, #0
	str	r2, [sp, #36]
	mov	r2, #15
	str	r2, [sp, #40]
	ldr	r0, .L5+24
	mov	r1, r3
	mov	r2, #1
	mvn	r3, #0
	bl	GuiLib_DrawStr
	.loc 1 67 0 discriminator 2
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L3:
	.loc 1 67 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #10
	bls	.L4
	.loc 1 88 0 is_stmt 1
	ldr	r3, .L5+28
	ldr	r3, [r3, #0]
	ldr	r2, .L5+32
	ldr	r1, [r2, #0]
	sub	r2, fp, #84
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #64
	ldr	r2, .L5+36
	bl	snprintf
	.loc 1 89 0
	sub	r3, fp, #84
	mov	r0, r3
	bl	strlen
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #1
	add	r3, r2, r3
	mov	r3, r3, asl #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r2, fp, #84
	str	r2, [sp, #0]
	mov	r2, #2
	str	r2, [sp, #4]
	mov	r2, #3
	str	r2, [sp, #8]
	mov	r2, #0
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
	mov	r3, #0
	str	r3, [sp, #28]
	mov	r3, #0
	str	r3, [sp, #32]
	mov	r3, #0
	str	r3, [sp, #36]
	mov	r3, #15
	str	r3, [sp, #40]
	mov	r0, #160
	mov	r1, #207
	mov	r2, #1
	mvn	r3, #0
	bl	GuiLib_DrawStr
	.loc 1 91 0
	ldr	r3, .L5+40
	ldr	ip, [r3, #0]
	sub	r2, fp, #84
	sub	r4, fp, #16
	ldmia	r4, {r3-r4}
	stmia	sp, {r3-r4}
	mov	r0, r2
	mov	r1, #64
	ldr	r2, .L5+44
	mov	r3, ip
	bl	snprintf
	.loc 1 92 0
	sub	r3, fp, #84
	mov	r0, r3
	bl	strlen
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #1
	add	r3, r2, r3
	mov	r3, r3, asl #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r2, fp, #84
	str	r2, [sp, #0]
	mov	r2, #2
	str	r2, [sp, #4]
	mov	r2, #3
	str	r2, [sp, #8]
	mov	r2, #0
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
	mov	r3, #0
	str	r3, [sp, #28]
	mov	r3, #0
	str	r3, [sp, #32]
	mov	r3, #0
	str	r3, [sp, #36]
	mov	r3, #15
	str	r3, [sp, #40]
	mov	r0, #160
	mov	r1, #219
	mov	r2, #1
	mvn	r3, #0
	bl	GuiLib_DrawStr
	.loc 1 94 0
	bl	GuiLib_Refresh
	.loc 1 95 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L6:
	.align	2
.L5:
	.word	partitions
	.word	.LC0
	.word	mem_current_allocations_of
	.word	.LC1
	.word	mem_max_allocations_of
	.word	.LC2
	.word	270
	.word	mem_numalloc
	.word	mem_count
	.word	.LC3
	.word	mem_maxalloc
	.word	.LC4
.LFE0:
	.size	FDTO_MEM_draw_report, .-FDTO_MEM_draw_report
	.section	.text.MEM_process_report,"ax",%progbits
	.align	2
	.global	MEM_process_report
	.type	MEM_process_report, %function
MEM_process_report:
.LFB1:
	.loc 1 99 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 100 0
	ldr	r3, [fp, #-12]
	cmp	r3, #67
	bne	.L11
.L9:
	.loc 1 103 0
	ldr	r3, .L12
	mov	r2, #11
	str	r2, [r3, #0]
	.loc 1 105 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 109 0
	mov	r0, #1
	bl	TECH_SUPPORT_draw_dialog
	.loc 1 110 0
	b	.L7
.L11:
	.loc 1 113 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L7:
	.loc 1 115 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	MEM_process_report, .-MEM_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cs_mem_part.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cs_mem.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x2d9
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF37
	.byte	0x1
	.4byte	.LASF38
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x53
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x70
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x5
	.4byte	0x33
	.4byte	0xae
	.uleb128 0x6
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd3
	.uleb128 0x8
	.4byte	.LASF14
	.byte	0x3
	.byte	0x7e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF15
	.byte	0x3
	.byte	0x80
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x82
	.4byte	0xae
	.uleb128 0x7
	.byte	0x8
	.byte	0x4
	.byte	0x17
	.4byte	0x103
	.uleb128 0x8
	.4byte	.LASF17
	.byte	0x4
	.byte	0x19
	.4byte	0x25
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF18
	.byte	0x4
	.byte	0x1b
	.4byte	0x25
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x4
	.byte	0x1d
	.4byte	0xde
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.byte	0x28
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x160
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x1
	.byte	0x28
	.4byte	0x160
	.byte	0x3
	.byte	0x91
	.sleb128 -92
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x1
	.byte	0x30
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xc
	.ascii	"i\000"
	.byte	0x1
	.byte	0x32
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x1
	.byte	0x34
	.4byte	0x165
	.byte	0x3
	.byte	0x91
	.sleb128 -88
	.byte	0
	.uleb128 0xd
	.4byte	0x8c
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0x175
	.uleb128 0x6
	.4byte	0x25
	.byte	0x3f
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.byte	0x62
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x19d
	.uleb128 0xa
	.4byte	.LASF25
	.byte	0x1
	.byte	0x62
	.4byte	0x19d
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xd
	.4byte	0xd3
	.uleb128 0xe
	.4byte	.LASF30
	.byte	0x6
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x5
	.byte	0x30
	.4byte	0x1c1
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xd
	.4byte	0x9e
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x5
	.byte	0x34
	.4byte	0x1d7
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xd
	.4byte	0x9e
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x5
	.byte	0x36
	.4byte	0x1ed
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xd
	.4byte	0x9e
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x5
	.byte	0x38
	.4byte	0x203
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xd
	.4byte	0x9e
	.uleb128 0x5
	.4byte	0x103
	.4byte	0x218
	.uleb128 0x6
	.4byte	0x25
	.byte	0xa
	.byte	0
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x4
	.byte	0x20
	.4byte	0x225
	.byte	0x1
	.byte	0x1
	.uleb128 0xd
	.4byte	0x208
	.uleb128 0xf
	.4byte	.LASF32
	.byte	0x7
	.byte	0x6f
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x7
	.byte	0x71
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF34
	.byte	0x7
	.byte	0x73
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	0x5a
	.4byte	0x261
	.uleb128 0x6
	.4byte	0x25
	.byte	0xa
	.byte	0
	.uleb128 0xf
	.4byte	.LASF35
	.byte	0x7
	.byte	0x76
	.4byte	0x251
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x7
	.byte	0x78
	.4byte	0x251
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF30
	.byte	0x6
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x4
	.byte	0x20
	.4byte	0x296
	.byte	0x1
	.byte	0x1
	.uleb128 0xd
	.4byte	0x208
	.uleb128 0xf
	.4byte	.LASF32
	.byte	0x7
	.byte	0x6f
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x7
	.byte	0x71
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF34
	.byte	0x7
	.byte	0x73
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF35
	.byte	0x7
	.byte	0x76
	.4byte	0x251
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x7
	.byte	0x78
	.4byte	0x251
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF26:
	.ascii	"GuiFont_LanguageActive\000"
.LASF20:
	.ascii	"total_mem\000"
.LASF22:
	.ascii	"FDTO_MEM_draw_report\000"
.LASF24:
	.ascii	"pcomplete_redraw\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF16:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF30:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF33:
	.ascii	"mem_maxalloc\000"
.LASF9:
	.ascii	"UNS_64\000"
.LASF38:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_memory_use.c\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF34:
	.ascii	"mem_numalloc\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF14:
	.ascii	"keycode\000"
.LASF17:
	.ascii	"block_size\000"
.LASF35:
	.ascii	"mem_current_allocations_of\000"
.LASF31:
	.ascii	"partitions\000"
.LASF37:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF27:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF23:
	.ascii	"MEM_process_report\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF25:
	.ascii	"pkey_event\000"
.LASF28:
	.ascii	"GuiFont_DecimalChar\000"
.LASF15:
	.ascii	"repeats\000"
.LASF36:
	.ascii	"mem_max_allocations_of\000"
.LASF19:
	.ascii	"PARTITION_DEFINITION\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF11:
	.ascii	"long long int\000"
.LASF1:
	.ascii	"char\000"
.LASF29:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF5:
	.ascii	"short int\000"
.LASF21:
	.ascii	"str_64\000"
.LASF32:
	.ascii	"mem_count\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF13:
	.ascii	"long int\000"
.LASF3:
	.ascii	"signed char\000"
.LASF18:
	.ascii	"block_count\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
