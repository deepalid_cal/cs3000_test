	.file	"e_sr_programming.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.SR_PROGRAMMING_querying_device,"aw",%nobits
	.align	2
	.type	SR_PROGRAMMING_querying_device, %object
	.size	SR_PROGRAMMING_querying_device, 4
SR_PROGRAMMING_querying_device:
	.space	4
	.global	CurrentSubnetIDRcvPtr
	.section	.bss.CurrentSubnetIDRcvPtr,"aw",%nobits
	.align	2
	.type	CurrentSubnetIDRcvPtr, %object
	.size	CurrentSubnetIDRcvPtr, 4
CurrentSubnetIDRcvPtr:
	.space	4
	.global	CurrentSubnetIDXmtPtr
	.section	.bss.CurrentSubnetIDXmtPtr,"aw",%nobits
	.align	2
	.type	CurrentSubnetIDXmtPtr, %object
	.size	CurrentSubnetIDXmtPtr, 4
CurrentSubnetIDXmtPtr:
	.space	4
	.section	.bss.g_SR_PROGRAMMING_previous_cursor_pos,"aw",%nobits
	.align	2
	.type	g_SR_PROGRAMMING_previous_cursor_pos, %object
	.size	g_SR_PROGRAMMING_previous_cursor_pos, 4
g_SR_PROGRAMMING_previous_cursor_pos:
	.space	4
	.section	.rodata.sr_mode_dropdown_lookup,"a",%progbits
	.align	2
	.type	sr_mode_dropdown_lookup, %object
	.size	sr_mode_dropdown_lookup, 12
sr_mode_dropdown_lookup:
	.word	2
	.word	3
	.word	7
	.section	.text.display_warning_if_repeater_links_to_itself,"ax",%progbits
	.align	2
	.type	display_warning_if_repeater_links_to_itself, %function
display_warning_if_repeater_links_to_itself:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_sr_programming.c"
	.loc 1 174 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	.loc 1 177 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 179 0
	ldr	r3, .L5+4
	ldr	r3, [r3, #0]
	cmp	r3, #7
	bne	.L2
	.loc 1 183 0
	ldr	r3, .L5+8
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L5+12
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, .L5
	str	r2, [r3, #0]
	b	.L3
.L2:
	.loc 1 187 0
	ldr	r3, .L5
	mov	r2, #0
	str	r2, [r3, #0]
.L3:
	.loc 1 191 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	beq	.L1
	.loc 1 193 0
	mov	r0, #0
	bl	Redraw_Screen
.L1:
	.loc 1 195 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	GuiVar_SRRepeaterLinkedToItself
	.word	GuiVar_SRMode
	.word	CurrentSubnetIDRcvPtr
	.word	CurrentSubnetIDXmtPtr
.LFE0:
	.size	display_warning_if_repeater_links_to_itself, .-display_warning_if_repeater_links_to_itself
	.section	.text.set_sr_mode,"ax",%progbits
	.align	2
	.type	set_sr_mode, %function
set_sr_mode:
.LFB1:
	.loc 1 203 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 204 0
	ldr	r3, [fp, #-8]
	cmp	r3, #7
	beq	.L9
	cmp	r3, #7
	bhi	.L12
	sub	r3, r3, #2
	cmp	r3, #1
	bhi	.L8
	b	.L9
.L12:
	cmp	r3, #1000
	beq	.L10
	ldr	r2, .L32
	cmp	r3, r2
	beq	.L11
	b	.L8
.L10:
	.loc 1 207 0
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L14
	cmp	r3, #3
	beq	.L15
	b	.L28
.L14:
	.loc 1 210 0
	ldr	r3, .L32+4
	mov	r2, #3
	str	r2, [r3, #0]
	.loc 1 211 0
	b	.L16
.L15:
	.loc 1 214 0
	ldr	r3, .L32+4
	mov	r2, #7
	str	r2, [r3, #0]
	.loc 1 215 0
	b	.L16
.L28:
	.loc 1 220 0
	ldr	r3, .L32+4
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 221 0
	mov	r0, r0	@ nop
.L16:
	.loc 1 223 0
	b	.L17
.L11:
	.loc 1 226 0
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L19
	cmp	r3, #3
	beq	.L20
	b	.L29
.L19:
	.loc 1 230 0
	ldr	r3, .L32+4
	mov	r2, #7
	str	r2, [r3, #0]
	.loc 1 231 0
	b	.L21
.L20:
	.loc 1 234 0
	ldr	r3, .L32+4
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 235 0
	b	.L21
.L29:
	.loc 1 239 0
	ldr	r3, .L32+4
	mov	r2, #3
	str	r2, [r3, #0]
	.loc 1 240 0
	mov	r0, r0	@ nop
.L21:
	.loc 1 242 0
	b	.L17
.L9:
	.loc 1 248 0
	ldr	r3, .L32+4
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 249 0
	b	.L17
.L8:
	.loc 1 253 0
	ldr	r3, .L32+4
	mov	r2, #99
	str	r2, [r3, #0]
	.loc 1 254 0
	mov	r0, r0	@ nop
.L17:
	.loc 1 260 0
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	cmp	r3, #3
	beq	.L23
	cmp	r3, #7
	bne	.L30
.L24:
	.loc 1 263 0
	ldr	r3, .L32+8
	ldr	r2, .L32+12
	str	r2, [r3, #0]
	.loc 1 264 0
	ldr	r3, .L32+16
	ldr	r2, .L32+20
	str	r2, [r3, #0]
	.loc 1 267 0
	ldr	r3, .L32+24
	ldr	r3, [r3, #0]
	cmp	r3, #4
	bls	.L25
	.loc 1 269 0
	ldr	r3, .L32+24
	mov	r2, #0
	str	r2, [r3, #0]
.L25:
	.loc 1 274 0
	ldr	r3, .L32+16
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L31
	.loc 1 276 0
	ldr	r3, .L32+16
	ldr	r3, [r3, #0]
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 278 0
	b	.L31
.L23:
	.loc 1 281 0
	ldr	r3, .L32+8
	ldr	r2, .L32+28
	str	r2, [r3, #0]
	.loc 1 282 0
	ldr	r3, .L32+16
	ldr	r2, .L32+32
	str	r2, [r3, #0]
	.loc 1 283 0
	b	.L27
.L30:
	.loc 1 287 0
	ldr	r3, .L32+8
	ldr	r2, .L32+36
	str	r2, [r3, #0]
	.loc 1 288 0
	ldr	r3, .L32+16
	ldr	r2, .L32+40
	str	r2, [r3, #0]
	.loc 1 289 0
	b	.L27
.L31:
	.loc 1 278 0
	mov	r0, r0	@ nop
.L27:
	.loc 1 292 0
	bl	display_warning_if_repeater_links_to_itself
	.loc 1 293 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L33:
	.align	2
.L32:
	.word	1001
	.word	GuiVar_SRMode
	.word	CurrentSubnetIDRcvPtr
	.word	GuiVar_SRSubnetRcvRepeater
	.word	CurrentSubnetIDXmtPtr
	.word	GuiVar_SRSubnetXmtRepeater
	.word	GuiVar_SRRepeater
	.word	GuiVar_SRSubnetRcvSlave
	.word	GuiVar_SRSubnetXmtSlave
	.word	GuiVar_SRSubnetRcvMaster
	.word	GuiVar_SRSubnetXmtMaster
.LFE1:
	.size	set_sr_mode, .-set_sr_mode
	.section	.text.navigate_around_subnet_rcv_field,"ax",%progbits
	.align	2
	.type	navigate_around_subnet_rcv_field, %function
navigate_around_subnet_rcv_field:
.LFB2:
	.loc 1 299 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #4
.LCFI8:
	str	r0, [fp, #-8]
	.loc 1 300 0
	ldr	r3, .L37
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L35
	.loc 1 302 0
	mov	r0, #4
	mov	r1, #1
	bl	CURSOR_Select
	b	.L34
.L35:
	.loc 1 306 0
	ldr	r0, [fp, #-8]
	mov	r1, #1
	bl	CURSOR_Select
.L34:
	.loc 1 308 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L38:
	.align	2
.L37:
	.word	GuiVar_SRMode
.LFE2:
	.size	navigate_around_subnet_rcv_field, .-navigate_around_subnet_rcv_field
	.section	.text.start_sr_device_communication,"ax",%progbits
	.align	2
	.type	start_sr_device_communication, %function
start_sr_device_communication:
.LFB3:
	.loc 1 316 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #48
.LCFI11:
	str	r0, [fp, #-48]
	str	r1, [fp, #-52]
	.loc 1 317 0
	ldr	r3, .L44
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 324 0
	ldr	r3, [fp, #-52]
	cmp	r3, #87
	bne	.L40
	.loc 1 326 0
	mov	r3, #4608
	str	r3, [fp, #-44]
	b	.L41
.L40:
	.loc 1 332 0
	mov	r3, #4352
	str	r3, [fp, #-44]
.L41:
	.loc 1 338 0
	ldr	r3, [fp, #-48]
	cmp	r3, #1
	bne	.L42
	.loc 1 338 0 is_stmt 0 discriminator 1
	mov	r3, #2
	b	.L43
.L42:
	.loc 1 338 0 discriminator 2
	mov	r3, #1
.L43:
	.loc 1 338 0 discriminator 3
	str	r3, [fp, #-12]
	.loc 1 340 0 is_stmt 1 discriminator 3
	sub	r3, fp, #44
	mov	r0, r3
	bl	COMM_MNGR_post_event_with_details
	.loc 1 341 0 discriminator 3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L45:
	.align	2
.L44:
	.word	SR_PROGRAMMING_querying_device
.LFE3:
	.size	start_sr_device_communication, .-start_sr_device_communication
	.section .rodata
	.align	2
.LC0:
	.ascii	"ERROR: Invalid SubnetID(1): %d\000"
	.section	.text.get_subnet_id_from_string,"ax",%progbits
	.align	2
	.type	get_subnet_id_from_string, %function
get_subnet_id_from_string:
.LFB4:
	.loc 1 345 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #8
.LCFI14:
	str	r0, [fp, #-12]
	.loc 1 350 0
	ldr	r0, [fp, #-12]
	bl	atoi
	mov	r3, r0
	str	r3, [fp, #-8]
	.loc 1 352 0
	ldr	r3, [fp, #-8]
	cmp	r3, #9
	bls	.L52
	cmp	r3, #15
	bne	.L51
.L49:
	.loc 1 368 0
	mov	r3, #10
	str	r3, [fp, #-8]
	.loc 1 371 0
	b	.L50
.L51:
	.loc 1 374 0
	ldr	r0, .L53
	ldr	r1, [fp, #-8]
	bl	Alert_Message_va
	.loc 1 375 0
	mov	r3, #10
	str	r3, [fp, #-8]
	.loc 1 376 0
	b	.L50
.L52:
	.loc 1 365 0
	mov	r0, r0	@ nop
.L50:
	.loc 1 379 0
	ldr	r3, [fp, #-8]
	.loc 1 380 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L54:
	.align	2
.L53:
	.word	.LC0
.LFE4:
	.size	get_subnet_id_from_string, .-get_subnet_id_from_string
	.section .rodata
	.align	2
.LC1:
	.ascii	"%d\000"
	.align	2
.LC2:
	.ascii	"F\000"
	.align	2
.LC3:
	.ascii	"-\000"
	.align	2
.LC4:
	.ascii	"ERROR: Invalid SubnetID(2): %d\000"
	.section	.text.get_subnet_string_from_id,"ax",%progbits
	.align	2
	.type	get_subnet_string_from_id, %function
get_subnet_string_from_id:
.LFB5:
	.loc 1 384 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #12
.LCFI17:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 391 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #9
	bhi	.L56
	.loc 1 393 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	sub	r2, fp, #8
	mov	r0, r2
	mov	r1, #2
	ldr	r2, .L59
	bl	snprintf
	b	.L57
.L56:
	.loc 1 395 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #10
	bne	.L58
	.loc 1 397 0
	sub	r3, fp, #8
	mov	r0, r3
	mov	r1, #2
	ldr	r2, .L59+4
	bl	snprintf
	b	.L57
.L58:
	.loc 1 401 0
	sub	r3, fp, #8
	mov	r0, r3
	mov	r1, #2
	ldr	r2, .L59+8
	bl	snprintf
	.loc 1 402 0
	ldr	r0, .L59+12
	ldr	r1, [fp, #-12]
	bl	Alert_Message_va
.L57:
	.loc 1 406 0
	sub	r3, fp, #8
	ldr	r0, [fp, #-16]
	mov	r1, r3
	mov	r2, #2
	bl	memcpy
	.loc 1 408 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L60:
	.align	2
.L59:
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
.LFE5:
	.size	get_subnet_string_from_id, .-get_subnet_string_from_id
	.section	.text.get_info_text_from_sr_programming_struct,"ax",%progbits
	.align	2
	.type	get_info_text_from_sr_programming_struct, %function
get_info_text_from_sr_programming_struct:
.LFB6:
	.loc 1 412 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	.loc 1 415 0
	ldr	r3, .L62
	ldr	r3, [r3, #0]
	add	r3, r3, #14
	ldr	r0, .L62+4
	mov	r1, #49
	mov	r2, r3
	bl	snprintf
	.loc 1 417 0
	ldmfd	sp!, {fp, pc}
.L63:
	.align	2
.L62:
	.word	sr_state
	.word	GuiVar_CommOptionInfoText
.LFE6:
	.size	get_info_text_from_sr_programming_struct, .-get_info_text_from_sr_programming_struct
	.section .rodata
	.align	2
.LC5:
	.ascii	"\000"
	.section	.text.clear_info_text,"ax",%progbits
	.align	2
	.type	clear_info_text, %function
clear_info_text:
.LFB7:
	.loc 1 421 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI20:
	add	fp, sp, #4
.LCFI21:
	.loc 1 424 0
	ldr	r0, .L65
	ldr	r1, .L65+4
	mov	r2, #49
	bl	strlcpy
	.loc 1 426 0
	ldmfd	sp!, {fp, pc}
.L66:
	.align	2
.L65:
	.word	GuiVar_CommOptionInfoText
	.word	.LC5
.LFE7:
	.size	clear_info_text, .-clear_info_text
	.section .rodata
	.align	2
.LC6:
	.ascii	"%2d\000"
	.align	2
.LC7:
	.ascii	"%c\000"
	.align	2
.LC8:
	.ascii	"0\000"
	.section	.text.set_sr_programming_struct_from_guivars,"ax",%progbits
	.align	2
	.type	set_sr_programming_struct_from_guivars, %function
set_sr_programming_struct_from_guivars:
.LFB8:
	.loc 1 430 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI22:
	add	fp, sp, #4
.LCFI23:
	.loc 1 433 0
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L67
	.loc 1 435 0
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r2, r3, #4
	ldr	r3, .L73+4
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, #3
	ldr	r2, .L73+8
	bl	snprintf
	.loc 1 443 0
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	add	r2, r3, #176
	ldr	r3, .L73+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, #5
	ldr	r2, .L73+16
	bl	snprintf
	.loc 1 446 0
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	add	r2, r3, #181
	ldr	r3, .L73+20
	ldr	r3, [r3, #0]
	and	r3, r3, #255
	add	r3, r3, #65
	and	r3, r3, #255
	mov	r0, r2
	mov	r1, #3
	ldr	r2, .L73+24
	bl	snprintf
	.loc 1 449 0
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r2, r3, #24
	ldr	r3, .L73+28
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, #5
	ldr	r2, .L73+16
	bl	snprintf
	.loc 1 453 0
	ldr	r3, .L73+4
	ldr	r3, [r3, #0]
	cmp	r3, #3
	beq	.L70
	cmp	r3, #7
	bne	.L72
.L71:
	.loc 1 456 0
	ldr	r3, .L73+32
	ldr	r2, [r3, #0]
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #65
	mov	r0, r2
	mov	r1, r3
	bl	get_subnet_string_from_id
	.loc 1 457 0
	ldr	r3, .L73+36
	ldr	r2, [r3, #0]
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #68
	mov	r0, r2
	mov	r1, r3
	bl	get_subnet_string_from_id
	.loc 1 458 0
	b	.L67
.L70:
	.loc 1 461 0
	ldr	r3, .L73+32
	ldr	r2, [r3, #0]
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #65
	mov	r0, r2
	mov	r1, r3
	bl	get_subnet_string_from_id
	.loc 1 462 0
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #68
	mov	r0, r3
	mov	r1, #3
	ldr	r2, .L73+40
	bl	snprintf
	.loc 1 463 0
	b	.L67
.L72:
	.loc 1 467 0
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #65
	mov	r0, r3
	mov	r1, #3
	ldr	r2, .L73+44
	bl	snprintf
	.loc 1 468 0
	ldr	r3, .L73
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #68
	mov	r0, r3
	mov	r1, #3
	ldr	r2, .L73+44
	bl	snprintf
	.loc 1 469 0
	mov	r0, r0	@ nop
.L67:
	.loc 1 476 0
	ldmfd	sp!, {fp, pc}
.L74:
	.align	2
.L73:
	.word	sr_state
	.word	GuiVar_SRMode
	.word	.LC1
	.word	GuiVar_SRGroup
	.word	.LC6
	.word	GuiVar_SRRepeater
	.word	.LC7
	.word	GuiVar_SRTransmitPower
	.word	CurrentSubnetIDRcvPtr
	.word	CurrentSubnetIDXmtPtr
	.word	.LC2
	.word	.LC8
.LFE8:
	.size	set_sr_programming_struct_from_guivars, .-set_sr_programming_struct_from_guivars
	.section	.text.get_guivars_from_sr_programming_struct,"ax",%progbits
	.align	2
	.type	get_guivars_from_sr_programming_struct, %function
get_guivars_from_sr_programming_struct:
.LFB9:
	.loc 1 480 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI24:
	add	fp, sp, #8
.LCFI25:
	.loc 1 483 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L75
	.loc 1 491 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	add	r3, r3, #157
	ldr	r0, .L77+4
	mov	r1, r3
	mov	r2, #9
	bl	strlcpy
	.loc 1 493 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	add	r3, r3, #144
	ldr	r0, .L77+8
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 495 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #4
	mov	r0, r3
	bl	atoi
	mov	r3, r0
	mov	r0, r3
	bl	set_sr_mode
	.loc 1 500 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	add	r3, r3, #176
	mov	r0, r3
	bl	atoi
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L77+12
	str	r2, [r3, #0]
	.loc 1 503 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	ldrb	r3, [r3, #181]	@ zero_extendqisi2
	sub	r3, r3, #65
	mov	r2, r3
	ldr	r3, .L77+16
	str	r2, [r3, #0]
	.loc 1 506 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #24
	mov	r0, r3
	bl	atoi
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L77+20
	str	r2, [r3, #0]
	.loc 1 510 0
	ldr	r3, .L77+24
	ldr	r4, [r3, #0]
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #65
	mov	r0, r3
	bl	get_subnet_id_from_string
	mov	r3, r0
	mov	r0, r4
	mov	r1, r3
	mov	r2, #1
	bl	memset
	.loc 1 511 0
	ldr	r3, .L77+28
	ldr	r4, [r3, #0]
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #132]
	add	r3, r3, #68
	mov	r0, r3
	bl	get_subnet_id_from_string
	mov	r3, r0
	mov	r0, r4
	mov	r1, r3
	mov	r2, #1
	bl	memset
.L75:
	.loc 1 515 0
	ldmfd	sp!, {r4, fp, pc}
.L78:
	.align	2
.L77:
	.word	sr_state
	.word	GuiVar_SRSerialNumber
	.word	GuiVar_SRFirmwareVer
	.word	GuiVar_SRGroup
	.word	GuiVar_SRRepeater
	.word	GuiVar_SRTransmitPower
	.word	CurrentSubnetIDRcvPtr
	.word	CurrentSubnetIDXmtPtr
.LFE9:
	.size	get_guivars_from_sr_programming_struct, .-get_guivars_from_sr_programming_struct
	.section	.text.sr_values_in_range,"ax",%progbits
	.align	2
	.type	sr_values_in_range, %function
sr_values_in_range:
.LFB10:
	.loc 1 519 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	sub	sp, sp, #4
.LCFI28:
	.loc 1 520 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 526 0
	ldr	r3, .L87
	ldr	r3, [r3, #0]
	cmp	r3, #7
	bls	.L80
	.loc 1 529 0
	mov	r0, #1
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 530 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L81
.L80:
	.loc 1 532 0
	ldr	r3, .L87+4
	ldr	r3, [r3, #0]
	cmp	r3, #10
	bls	.L82
	.loc 1 535 0
	mov	r0, #2
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 536 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L81
.L82:
	.loc 1 539 0
	ldr	r3, .L87
	ldr	r3, [r3, #0]
	cmp	r3, #7
	bne	.L83
	.loc 1 539 0 is_stmt 0 discriminator 1
	ldr	r3, .L87+8
	ldr	r3, [r3, #0]
	cmp	r3, #4
	bls	.L83
	.loc 1 542 0 is_stmt 1
	mov	r0, #3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 543 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L81
.L83:
	.loc 1 546 0
	ldr	r3, .L87
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bne	.L84
	.loc 1 546 0 is_stmt 0 discriminator 1
	ldr	r3, .L87+12
	ldr	r3, [r3, #0]
	cmp	r3, #9
	bls	.L84
	.loc 1 549 0 is_stmt 1
	mov	r0, #4
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 550 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L81
.L84:
	.loc 1 553 0
	ldr	r3, .L87
	ldr	r3, [r3, #0]
	cmp	r3, #7
	bne	.L85
	.loc 1 553 0 is_stmt 0 discriminator 1
	ldr	r3, .L87+16
	ldr	r3, [r3, #0]
	cmp	r3, #9
	bls	.L85
	.loc 1 556 0 is_stmt 1
	mov	r0, #4
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 557 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L81
.L85:
	.loc 1 560 0
	ldr	r3, .L87
	ldr	r3, [r3, #0]
	cmp	r3, #7
	bne	.L86
	.loc 1 560 0 is_stmt 0 discriminator 1
	ldr	r3, .L87+20
	ldr	r3, [r3, #0]
	cmp	r3, #9
	bls	.L86
	.loc 1 563 0 is_stmt 1
	mov	r0, #5
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 564 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L81
.L86:
	.loc 1 566 0
	ldr	r3, .L87+24
	ldr	r3, [r3, #0]
	cmp	r3, #10
	bls	.L81
	.loc 1 569 0
	mov	r0, #6
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 570 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L81:
	.loc 1 573 0
	ldr	r3, [fp, #-8]
	.loc 1 574 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L88:
	.align	2
.L87:
	.word	GuiVar_SRMode
	.word	GuiVar_SRGroup
	.word	GuiVar_SRRepeater
	.word	GuiVar_SRSubnetRcvSlave
	.word	GuiVar_SRSubnetRcvRepeater
	.word	GuiVar_SRSubnetXmtRepeater
	.word	GuiVar_SRTransmitPower
.LFE10:
	.size	sr_values_in_range, .-sr_values_in_range
	.section .rodata
	.align	2
.LC9:
	.ascii	"--------\000"
	.section	.text.SR_PROGRAMMING_initialize_guivars,"ax",%progbits
	.align	2
	.type	SR_PROGRAMMING_initialize_guivars, %function
SR_PROGRAMMING_initialize_guivars:
.LFB11:
	.loc 1 578 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI29:
	add	fp, sp, #4
.LCFI30:
	sub	sp, sp, #4
.LCFI31:
	str	r0, [fp, #-8]
	.loc 1 580 0
	ldr	r3, .L90
	ldr	r2, .L90+4
	str	r2, [r3, #0]
	.loc 1 583 0
	ldr	r0, .L90+8
	ldr	r1, .L90+12
	mov	r2, #9
	bl	strlcpy
	.loc 1 585 0
	mov	r0, #2
	bl	set_sr_mode
	.loc 1 587 0
	ldr	r3, .L90+16
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 589 0
	ldr	r3, .L90+20
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 593 0
	ldr	r3, .L90+24
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 595 0
	ldr	r3, .L90+28
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 596 0
	ldr	r3, .L90+32
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 597 0
	ldr	r3, .L90+36
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 599 0
	ldr	r3, .L90+40
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 600 0
	ldr	r3, .L90+44
	mov	r2, #10
	str	r2, [r3, #0]
	.loc 1 601 0
	ldr	r3, .L90+48
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 603 0
	ldr	r3, .L90+52
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 605 0
	bl	clear_info_text
	.loc 1 607 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L91:
	.align	2
.L90:
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36865
	.word	GuiVar_SRSerialNumber
	.word	.LC9
	.word	GuiVar_SRPort
	.word	GuiVar_SRGroup
	.word	GuiVar_SRRepeater
	.word	GuiVar_SRSubnetRcvMaster
	.word	GuiVar_SRSubnetRcvSlave
	.word	GuiVar_SRSubnetRcvRepeater
	.word	GuiVar_SRSubnetXmtMaster
	.word	GuiVar_SRSubnetXmtSlave
	.word	GuiVar_SRSubnetXmtRepeater
	.word	GuiVar_SRTransmitPower
.LFE11:
	.size	SR_PROGRAMMING_initialize_guivars, .-SR_PROGRAMMING_initialize_guivars
	.section	.text.FDTO_SR_PROGRAMMING_populate_mode_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_SR_PROGRAMMING_populate_mode_dropdown, %function
FDTO_SR_PROGRAMMING_populate_mode_dropdown:
.LFB12:
	.loc 1 611 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI32:
	add	fp, sp, #0
.LCFI33:
	sub	sp, sp, #4
.LCFI34:
	mov	r3, r0
	strh	r3, [fp, #-4]	@ movhi
	.loc 1 612 0
	ldrsh	r2, [fp, #-4]
	ldr	r3, .L93
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L93+4
	str	r2, [r3, #0]
	.loc 1 613 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L94:
	.align	2
.L93:
	.word	sr_mode_dropdown_lookup
	.word	GuiVar_ComboBoxItemIndex
.LFE12:
	.size	FDTO_SR_PROGRAMMING_populate_mode_dropdown, .-FDTO_SR_PROGRAMMING_populate_mode_dropdown
	.section	.text.FDTO_SR_PROGRAMMING_show_mode_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_SR_PROGRAMMING_show_mode_dropdown, %function
FDTO_SR_PROGRAMMING_show_mode_dropdown:
.LFB13:
	.loc 1 617 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI35:
	add	fp, sp, #4
.LCFI36:
	sub	sp, sp, #8
.LCFI37:
	.loc 1 622 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 625 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L96
.L99:
	.loc 1 627 0
	ldr	r3, .L100
	ldr	r2, [fp, #-8]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L100+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L97
	.loc 1 629 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-12]
	.loc 1 631 0
	b	.L98
.L97:
	.loc 1 625 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L96:
	.loc 1 625 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	bls	.L99
.L98:
	.loc 1 635 0 is_stmt 1
	mov	r0, #744
	ldr	r1, .L100+8
	mov	r2, #3
	ldr	r3, [fp, #-12]
	bl	FDTO_COMBOBOX_show
	.loc 1 636 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L101:
	.align	2
.L100:
	.word	sr_mode_dropdown_lookup
	.word	GuiVar_SRMode
	.word	FDTO_SR_PROGRAMMING_populate_mode_dropdown
.LFE13:
	.size	FDTO_SR_PROGRAMMING_show_mode_dropdown, .-FDTO_SR_PROGRAMMING_show_mode_dropdown
	.section	.text.FDTO_SR_PROGRAMMING_show_receives_from_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_SR_PROGRAMMING_show_receives_from_dropdown, %function
FDTO_SR_PROGRAMMING_show_receives_from_dropdown:
.LFB14:
	.loc 1 640 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI38:
	add	fp, sp, #4
.LCFI39:
	sub	sp, sp, #4
.LCFI40:
	.loc 1 645 0
	ldr	r3, .L105
	ldr	r3, [r3, #0]
	cmp	r3, #7
	bne	.L103
	.loc 1 647 0
	ldr	r3, .L105+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L104
.L103:
	.loc 1 651 0
	ldr	r3, .L105+8
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
.L104:
	.loc 1 654 0
	ldr	r0, .L105+12
	ldr	r1, .L105+16
	mov	r2, #10
	ldr	r3, [fp, #-8]
	bl	FDTO_COMBOBOX_show
	.loc 1 655 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L106:
	.align	2
.L105:
	.word	GuiVar_SRMode
	.word	GuiVar_SRSubnetRcvRepeater
	.word	GuiVar_SRSubnetRcvSlave
	.word	745
	.word	FDTO_COMBOBOX_add_items
.LFE14:
	.size	FDTO_SR_PROGRAMMING_show_receives_from_dropdown, .-FDTO_SR_PROGRAMMING_show_receives_from_dropdown
	.section	.text.FDTO_SR_PROGRAMMING_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_SR_PROGRAMMING_process_device_exchange_key, %function
FDTO_SR_PROGRAMMING_process_device_exchange_key:
.LFB15:
	.loc 1 659 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI41:
	add	fp, sp, #4
.LCFI42:
	sub	sp, sp, #8
.LCFI43:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 660 0
	bl	get_info_text_from_sr_programming_struct
	.loc 1 662 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #36864
	cmp	r3, #6
	bhi	.L113
	mov	r2, #1
	mov	r3, r2, asl r3
	and	r2, r3, #109
	cmp	r2, #0
	bne	.L109
	and	r3, r3, #18
	cmp	r3, #0
	beq	.L112
.L110:
	.loc 1 667 0
	bl	get_guivars_from_sr_programming_struct
	.loc 1 670 0
	ldr	r0, .L114
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L114+4
	str	r2, [r3, #0]
	.loc 1 673 0
	bl	DIALOG_close_ok_dialog
	.loc 1 675 0
	mov	r0, #0
	ldr	r1, [fp, #-12]
	bl	FDTO_SR_PROGRAMMING_draw_screen
	.loc 1 676 0
	b	.L107
.L109:
	.loc 1 684 0
	ldr	r0, [fp, #-8]
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L114+4
	str	r2, [r3, #0]
	.loc 1 687 0
	bl	DEVICE_EXCHANGE_draw_dialog
	.loc 1 688 0
	b	.L107
.L112:
	.loc 1 692 0
	mov	r0, r0	@ nop
.L113:
	mov	r0, r0	@ nop
.L107:
	.loc 1 694 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L115:
	.align	2
.L114:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
.LFE15:
	.size	FDTO_SR_PROGRAMMING_process_device_exchange_key, .-FDTO_SR_PROGRAMMING_process_device_exchange_key
	.section	.text.FDTO_SR_PROGRAMMING_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_SR_PROGRAMMING_draw_screen
	.type	FDTO_SR_PROGRAMMING_draw_screen, %function
FDTO_SR_PROGRAMMING_draw_screen:
.LFB16:
	.loc 1 698 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI44:
	add	fp, sp, #4
.LCFI45:
	sub	sp, sp, #16
.LCFI46:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 702 0
	ldr	r3, [fp, #-16]
	cmp	r3, #1
	bne	.L117
	.loc 1 705 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	movne	r3, #0
	moveq	r3, #1
	str	r3, [fp, #-12]
	.loc 1 707 0
	ldr	r0, [fp, #-12]
	bl	SR_PROGRAMMING_initialize_guivars
	.loc 1 713 0
	ldr	r0, .L121
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L121+4
	str	r2, [r3, #0]
	.loc 1 717 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L118
.L117:
	.loc 1 721 0
	ldr	r3, .L121+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmn	r3, #1
	bne	.L119
	.loc 1 723 0
	ldr	r3, .L121+12
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L118
.L119:
	.loc 1 727 0
	ldr	r3, .L121+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L118:
	.loc 1 731 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #53
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 732 0
	bl	GuiLib_Refresh
	.loc 1 734 0
	ldr	r3, [fp, #-16]
	cmp	r3, #1
	bne	.L116
	.loc 1 738 0
	bl	DEVICE_EXCHANGE_draw_dialog
	.loc 1 743 0
	ldr	r0, [fp, #-12]
	mov	r1, #82
	bl	start_sr_device_communication
.L116:
	.loc 1 745 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L122:
	.align	2
.L121:
	.word	36867
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_SR_PROGRAMMING_previous_cursor_pos
.LFE16:
	.size	FDTO_SR_PROGRAMMING_draw_screen, .-FDTO_SR_PROGRAMMING_draw_screen
	.section	.text.SR_PROGRAMMING_process_screen,"ax",%progbits
	.align	2
	.global	SR_PROGRAMMING_process_screen
	.type	SR_PROGRAMMING_process_screen, %function
SR_PROGRAMMING_process_screen:
.LFB17:
	.loc 1 749 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI47:
	add	fp, sp, #4
.LCFI48:
	sub	sp, sp, #56
.LCFI49:
	str	r0, [fp, #-52]
	str	r1, [fp, #-48]
	.loc 1 754 0
	ldr	r3, .L194
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #744
	beq	.L125
	ldr	r2, .L194+4
	cmp	r3, r2
	beq	.L126
	b	.L190
.L125:
	.loc 1 757 0
	ldr	r2, [fp, #-52]
	sub	r3, fp, #44
	mov	r0, r2
	mov	r1, r3
	bl	COMBO_BOX_key_press
	.loc 1 761 0
	ldr	r3, [fp, #-52]
	cmp	r3, #2
	beq	.L127
	.loc 1 761 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-52]
	cmp	r3, #67
	bne	.L191
.L127:
	.loc 1 763 0 is_stmt 1
	ldr	r2, [fp, #-44]
	ldr	r3, .L194+8
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	set_sr_mode
	.loc 1 764 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 766 0
	b	.L191
.L126:
	.loc 1 769 0
	ldr	r2, [fp, #-52]
	sub	r3, fp, #44
	mov	r0, r2
	mov	r1, r3
	bl	COMBO_BOX_key_press
	.loc 1 773 0
	ldr	r3, [fp, #-52]
	cmp	r3, #2
	beq	.L130
	.loc 1 773 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-52]
	cmp	r3, #67
	bne	.L192
.L130:
	.loc 1 777 0 is_stmt 1
	ldr	r3, .L194+12
	ldr	r3, [r3, #0]
	cmp	r3, #7
	bne	.L132
	.loc 1 779 0
	ldr	r2, [fp, #-44]
	ldr	r3, .L194+16
	str	r2, [r3, #0]
	b	.L133
.L132:
	.loc 1 783 0
	ldr	r2, [fp, #-44]
	ldr	r3, .L194+20
	str	r2, [r3, #0]
.L133:
	.loc 1 786 0
	bl	display_warning_if_repeater_links_to_itself
	.loc 1 788 0
	bl	Refresh_Screen
	.loc 1 790 0
	b	.L192
.L190:
	.loc 1 793 0
	ldr	r3, [fp, #-52]
	cmp	r3, #16
	beq	.L140
	cmp	r3, #16
	bhi	.L144
	cmp	r3, #2
	beq	.L137
	cmp	r3, #2
	bhi	.L145
	cmp	r3, #0
	beq	.L135
	cmp	r3, #1
	beq	.L136
	b	.L134
.L145:
	cmp	r3, #3
	beq	.L138
	cmp	r3, #4
	beq	.L139
	b	.L134
.L144:
	cmp	r3, #80
	beq	.L142
	cmp	r3, #80
	bhi	.L146
	cmp	r3, #20
	beq	.L140
	cmp	r3, #67
	beq	.L141
	b	.L134
.L146:
	cmp	r3, #84
	beq	.L142
	cmp	r3, #84
	bcc	.L134
	sub	r3, r3, #36864
	cmp	r3, #6
	bhi	.L134
	.loc 1 804 0
	ldr	r3, .L194+24
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 808 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 809 0
	ldr	r3, .L194+28
	str	r3, [fp, #-20]
	.loc 1 810 0
	ldr	r3, [fp, #-52]
	str	r3, [fp, #-16]
	.loc 1 813 0
	ldr	r3, .L194+32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L147
	.loc 1 815 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L148
.L147:
	.loc 1 819 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L148:
	.loc 1 822 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 823 0
	b	.L123
.L137:
	.loc 1 828 0
	ldr	r0, .L194+36
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L194+40
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L149
	.loc 1 829 0 discriminator 1
	ldr	r0, .L194+44
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L194+40
	ldr	r3, [r3, #0]
	.loc 1 828 0 discriminator 1
	cmp	r2, r3
	beq	.L149
	.loc 1 832 0
	bl	clear_info_text
	.loc 1 834 0
	ldr	r3, .L194+48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #1
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L150
.L155:
	.word	.L151
	.word	.L150
	.word	.L150
	.word	.L152
	.word	.L150
	.word	.L150
	.word	.L153
	.word	.L154
.L154:
	.loc 1 837 0
	bl	sr_values_in_range
	mov	r3, r0
	cmp	r3, #0
	beq	.L156
	.loc 1 839 0
	bl	good_key_beep
	.loc 1 843 0
	ldr	r3, .L194+48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L194+52
	str	r2, [r3, #0]
	.loc 1 846 0
	bl	set_sr_programming_struct_from_guivars
	.loc 1 848 0
	ldr	r3, .L194+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #87
	bl	start_sr_device_communication
	.loc 1 855 0
	b	.L158
.L156:
	.loc 1 852 0
	ldr	r0, .L194+56
	bl	DIALOG_draw_ok_dialog
	.loc 1 853 0
	bl	bad_key_beep
	.loc 1 855 0
	b	.L158
.L153:
	.loc 1 858 0
	bl	good_key_beep
	.loc 1 862 0
	ldr	r3, .L194+48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L194+52
	str	r2, [r3, #0]
	.loc 1 864 0
	ldr	r3, .L194+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #82
	bl	start_sr_device_communication
	.loc 1 865 0
	b	.L158
.L151:
	.loc 1 868 0
	bl	good_key_beep
	.loc 1 869 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 870 0
	ldr	r3, .L194+60
	str	r3, [fp, #-20]
	.loc 1 871 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 872 0
	b	.L158
.L152:
	.loc 1 875 0
	bl	good_key_beep
	.loc 1 876 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 877 0
	ldr	r3, .L194+64
	str	r3, [fp, #-20]
	.loc 1 878 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 879 0
	b	.L158
.L150:
	.loc 1 887 0
	bl	bad_key_beep
	.loc 1 888 0
	mov	r0, r0	@ nop
.L158:
	.loc 1 890 0
	mov	r0, r0	@ nop
	.loc 1 895 0
	b	.L123
.L149:
	.loc 1 893 0
	bl	bad_key_beep
	.loc 1 895 0
	b	.L123
.L139:
	.loc 1 899 0
	bl	clear_info_text
	.loc 1 904 0
	ldr	r3, .L194+48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #3
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L160
.L166:
	.word	.L161
	.word	.L162
	.word	.L163
	.word	.L164
	.word	.L160
	.word	.L165
.L161:
	.loc 1 907 0
	mov	r0, #1
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 908 0
	b	.L167
.L165:
	.loc 1 911 0
	mov	r0, #6
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 912 0
	b	.L167
.L163:
	.loc 1 915 0
	mov	r0, #3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 916 0
	b	.L167
.L164:
	.loc 1 919 0
	mov	r0, #2
	bl	navigate_around_subnet_rcv_field
	.loc 1 920 0
	b	.L167
.L162:
	.loc 1 923 0
	mov	r0, #2
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 924 0
	b	.L167
.L160:
	.loc 1 931 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 932 0
	mov	r0, r0	@ nop
.L167:
	.loc 1 934 0
	b	.L123
.L136:
	.loc 1 938 0
	bl	clear_info_text
	.loc 1 940 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 941 0
	b	.L123
.L135:
	.loc 1 945 0
	bl	clear_info_text
	.loc 1 950 0
	ldr	r3, .L194+48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #2
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L168
.L173:
	.word	.L169
	.word	.L170
	.word	.L171
	.word	.L168
	.word	.L168
	.word	.L172
.L169:
	.loc 1 953 0
	mov	r0, #6
	bl	navigate_around_subnet_rcv_field
	.loc 1 954 0
	b	.L174
.L171:
	.loc 1 957 0
	mov	r0, #6
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 958 0
	b	.L174
.L170:
	.loc 1 961 0
	mov	r0, #5
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 962 0
	b	.L174
.L172:
	.loc 1 965 0
	bl	bad_key_beep
	.loc 1 966 0
	b	.L174
.L168:
	.loc 1 974 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 975 0
	mov	r0, r0	@ nop
.L174:
	.loc 1 977 0
	b	.L123
.L138:
	.loc 1 981 0
	bl	clear_info_text
	.loc 1 983 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 984 0
	b	.L123
.L140:
	.loc 1 989 0
	bl	clear_info_text
	.loc 1 991 0
	bl	bad_key_beep
	.loc 1 992 0
	b	.L123
.L142:
	.loc 1 997 0
	bl	clear_info_text
	.loc 1 999 0
	ldr	r3, .L194+48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L175
.L183:
	.word	.L176
	.word	.L177
	.word	.L178
	.word	.L179
	.word	.L180
	.word	.L181
	.word	.L182
.L176:
	.loc 1 1004 0
	ldr	r3, .L194+68
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bne	.L184
	.loc 1 1004 0 is_stmt 0 discriminator 1
	ldr	r3, .L194+72
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bne	.L184
	.loc 1 1006 0 is_stmt 1
	bl	good_key_beep
	.loc 1 1008 0
	ldr	r3, .L194+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, .L194+32
	str	r2, [r3, #0]
	.loc 1 1012 0
	ldr	r3, .L194+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #82
	bl	start_sr_device_communication
	.loc 1 1018 0
	b	.L186
.L184:
	.loc 1 1016 0
	bl	bad_key_beep
	.loc 1 1018 0
	b	.L186
.L177:
	.loc 1 1021 0
	bl	good_key_beep
	.loc 1 1023 0
	ldr	r3, [fp, #-52]
	cmp	r3, #84
	bne	.L187
	.loc 1 1025 0
	mov	r0, #1000
	bl	set_sr_mode
	b	.L188
.L187:
	.loc 1 1029 0
	ldr	r0, .L194+76
	bl	set_sr_mode
.L188:
	.loc 1 1032 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 1033 0
	b	.L186
.L178:
	.loc 1 1036 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L194+80
	mov	r2, #1
	mov	r3, #10
	bl	process_uns32
	.loc 1 1040 0
	b	.L186
.L179:
	.loc 1 1043 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L194+84
	mov	r2, #0
	mov	r3, #4
	bl	process_uns32
	.loc 1 1047 0
	b	.L186
.L180:
	.loc 1 1050 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L194+88
	ldr	r3, [r3, #0]
	mov	r1, #1
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	mov	r3, #9
	bl	process_uns32
	.loc 1 1052 0
	bl	display_warning_if_repeater_links_to_itself
	.loc 1 1056 0
	b	.L186
.L181:
	.loc 1 1059 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L194+92
	ldr	r3, [r3, #0]
	mov	r1, #1
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, r3
	mov	r2, #1
	mov	r3, #10
	bl	process_uns32
	.loc 1 1061 0
	bl	display_warning_if_repeater_links_to_itself
	.loc 1 1065 0
	b	.L186
.L182:
	.loc 1 1068 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L194+96
	mov	r2, #0
	mov	r3, #10
	bl	process_uns32
	.loc 1 1070 0
	b	.L186
.L175:
	.loc 1 1075 0
	bl	bad_key_beep
	.loc 1 1077 0
	mov	r0, r0	@ nop
.L186:
	.loc 1 1081 0
	ldr	r3, .L194+48
	ldrh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L193
	.loc 1 1081 0 is_stmt 0 discriminator 1
	ldr	r3, .L194+48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #1
	beq	.L193
	.loc 1 1083 0 is_stmt 1
	bl	Refresh_Screen
	.loc 1 1085 0
	b	.L193
.L141:
	.loc 1 1088 0
	ldr	r3, .L194+100
	mov	r2, #11
	str	r2, [r3, #0]
	.loc 1 1090 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 1094 0
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	.loc 1 1095 0
	b	.L123
.L134:
	.loc 1 1098 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	b	.L123
.L191:
	.loc 1 766 0
	mov	r0, r0	@ nop
	b	.L123
.L192:
	.loc 1 790 0
	mov	r0, r0	@ nop
	b	.L123
.L193:
	.loc 1 1085 0
	mov	r0, r0	@ nop
.L123:
	.loc 1 1101 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L195:
	.align	2
.L194:
	.word	GuiLib_CurStructureNdx
	.word	745
	.word	sr_mode_dropdown_lookup
	.word	GuiVar_SRMode
	.word	GuiVar_SRSubnetRcvRepeater
	.word	GuiVar_SRSubnetRcvSlave
	.word	SR_PROGRAMMING_querying_device
	.word	FDTO_SR_PROGRAMMING_process_device_exchange_key
	.word	GuiVar_SRPort
	.word	36867
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36870
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_SR_PROGRAMMING_previous_cursor_pos
	.word	589
	.word	FDTO_SR_PROGRAMMING_show_mode_dropdown
	.word	FDTO_SR_PROGRAMMING_show_receives_from_dropdown
	.word	GuiVar_CommOptionPortAIndex
	.word	GuiVar_CommOptionPortBIndex
	.word	1001
	.word	GuiVar_SRGroup
	.word	GuiVar_SRRepeater
	.word	CurrentSubnetIDRcvPtr
	.word	CurrentSubnetIDXmtPtr
	.word	GuiVar_SRTransmitPower
	.word	GuiVar_MenuScreenToShow
.LFE17:
	.size	SR_PROGRAMMING_process_screen, .-SR_PROGRAMMING_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI20-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI22-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI24-.LFB9
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI26-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI29-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI32-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI35-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI38-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI41-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI42-.LCFI41
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI44-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI45-.LCFI44
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI47-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI48-.LCFI47
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/packet_definitions.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/comm_mngr.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_SR_FREEWAVE.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xd1e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF154
	.byte	0x1
	.4byte	.LASF155
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x2
	.byte	0x3a
	.4byte	0x53
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF5
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x2
	.byte	0x4c
	.4byte	0x6c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x2
	.byte	0x55
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x2
	.byte	0x5e
	.4byte	0x90
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x2
	.byte	0x99
	.4byte	0x90
	.uleb128 0x5
	.byte	0x4
	.4byte	0xaf
	.uleb128 0x6
	.4byte	0xb6
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.4byte	0x53
	.4byte	0xc6
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x6
	.byte	0x3
	.byte	0x3c
	.4byte	0xea
	.uleb128 0xb
	.4byte	.LASF15
	.byte	0x3
	.byte	0x3e
	.4byte	0xea
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.ascii	"to\000"
	.byte	0x3
	.byte	0x40
	.4byte	0xea
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.byte	0
	.uleb128 0x8
	.4byte	0x48
	.4byte	0xfa
	.uleb128 0x9
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x3
	.byte	0x42
	.4byte	0xc6
	.uleb128 0x8
	.4byte	0x85
	.4byte	0x115
	.uleb128 0x9
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x4
	.byte	0x14
	.4byte	0x13a
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x17
	.4byte	0x13a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x1a
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x48
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x4
	.byte	0x1c
	.4byte	0x115
	.uleb128 0x5
	.byte	0x4
	.4byte	0x41
	.uleb128 0xa
	.byte	0x8
	.byte	0x5
	.byte	0x7c
	.4byte	0x176
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x5
	.byte	0x7e
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x5
	.byte	0x80
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF22
	.byte	0x5
	.byte	0x82
	.4byte	0x151
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF23
	.uleb128 0x8
	.4byte	0x85
	.4byte	0x198
	.uleb128 0x9
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0xa
	.byte	0x28
	.byte	0x6
	.byte	0x74
	.4byte	0x210
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x6
	.byte	0x77
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x6
	.byte	0x7a
	.4byte	0xfa
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x6
	.byte	0x7d
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.ascii	"dh\000"
	.byte	0x6
	.byte	0x81
	.4byte	0x140
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x6
	.byte	0x85
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x6
	.byte	0x87
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x6
	.byte	0x8a
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xb
	.4byte	.LASF30
	.byte	0x6
	.byte	0x8c
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x4
	.4byte	.LASF31
	.byte	0x6
	.byte	0x8e
	.4byte	0x198
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x22b
	.uleb128 0x9
	.4byte	0x25
	.byte	0x27
	.byte	0
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x23b
	.uleb128 0x9
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0xd
	.4byte	0x85
	.uleb128 0xa
	.byte	0x24
	.byte	0x7
	.byte	0x78
	.4byte	0x2c7
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x7
	.byte	0x7b
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF33
	.byte	0x7
	.byte	0x83
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF34
	.byte	0x7
	.byte	0x86
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF35
	.byte	0x7
	.byte	0x88
	.4byte	0x2d8
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF36
	.byte	0x7
	.byte	0x8d
	.4byte	0x2ea
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x7
	.byte	0x92
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF38
	.byte	0x7
	.byte	0x96
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF39
	.byte	0x7
	.byte	0x9a
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF40
	.byte	0x7
	.byte	0x9c
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	0x2d3
	.uleb128 0xf
	.4byte	0x2d3
	.byte	0
	.uleb128 0xd
	.4byte	0x73
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2c7
	.uleb128 0xe
	.byte	0x1
	.4byte	0x2ea
	.uleb128 0xf
	.4byte	0x176
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2de
	.uleb128 0x4
	.4byte	.LASF41
	.byte	0x7
	.byte	0x9e
	.4byte	0x240
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF42
	.uleb128 0x10
	.byte	0x48
	.byte	0x8
	.2byte	0x2d2
	.4byte	0x438
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x8
	.2byte	0x2d4
	.4byte	0x438
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x11
	.4byte	.LASF44
	.byte	0x8
	.2byte	0x2d7
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x8
	.2byte	0x2da
	.4byte	0x458
	.byte	0x2
	.byte	0x23
	.uleb128 0x7
	.uleb128 0x11
	.4byte	.LASF46
	.byte	0x8
	.2byte	0x2e1
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x11
	.4byte	.LASF47
	.byte	0x8
	.2byte	0x2e2
	.4byte	0x468
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.uleb128 0x11
	.4byte	.LASF48
	.byte	0x8
	.2byte	0x2e3
	.4byte	0x468
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.uleb128 0x11
	.4byte	.LASF49
	.byte	0x8
	.2byte	0x2e4
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x15
	.uleb128 0x11
	.4byte	.LASF50
	.byte	0x8
	.2byte	0x2e5
	.4byte	0x478
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x11
	.4byte	.LASF51
	.byte	0x8
	.2byte	0x2e6
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x1d
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0x8
	.2byte	0x2e9
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x11
	.4byte	.LASF53
	.byte	0x8
	.2byte	0x2ea
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x23
	.uleb128 0x11
	.4byte	.LASF54
	.byte	0x8
	.2byte	0x2eb
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x26
	.uleb128 0x11
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x2ec
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x29
	.uleb128 0x11
	.4byte	.LASF56
	.byte	0x8
	.2byte	0x2ed
	.4byte	0x468
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x11
	.4byte	.LASF57
	.byte	0x8
	.2byte	0x2ee
	.4byte	0x478
	.byte	0x2
	.byte	0x23
	.uleb128 0x2e
	.uleb128 0x11
	.4byte	.LASF58
	.byte	0x8
	.2byte	0x2ef
	.4byte	0x468
	.byte	0x2
	.byte	0x23
	.uleb128 0x33
	.uleb128 0x11
	.4byte	.LASF59
	.byte	0x8
	.2byte	0x2f0
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x35
	.uleb128 0x11
	.4byte	.LASF60
	.byte	0x8
	.2byte	0x2f1
	.4byte	0x488
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x11
	.4byte	.LASF61
	.byte	0x8
	.2byte	0x2f2
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x41
	.uleb128 0x11
	.4byte	.LASF62
	.byte	0x8
	.2byte	0x2f3
	.4byte	0x448
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.byte	0
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x448
	.uleb128 0x9
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x458
	.uleb128 0x9
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x468
	.uleb128 0x9
	.4byte	0x25
	.byte	0x6
	.byte	0
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x478
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x488
	.uleb128 0x9
	.4byte	0x25
	.byte	0x4
	.byte	0
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x498
	.uleb128 0x9
	.4byte	0x25
	.byte	0x8
	.byte	0
	.uleb128 0x12
	.4byte	.LASF63
	.byte	0x8
	.2byte	0x2f9
	.4byte	0x302
	.uleb128 0x10
	.byte	0xb8
	.byte	0x8
	.2byte	0x2fe
	.4byte	0x611
	.uleb128 0x11
	.4byte	.LASF64
	.byte	0x8
	.2byte	0x300
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x11
	.4byte	.LASF65
	.byte	0x8
	.2byte	0x301
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x11
	.4byte	.LASF66
	.byte	0x8
	.2byte	0x302
	.4byte	0x22b
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x11
	.4byte	.LASF67
	.byte	0x8
	.2byte	0x303
	.4byte	0x21b
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x11
	.4byte	.LASF68
	.byte	0x8
	.2byte	0x304
	.4byte	0x21b
	.byte	0x2
	.byte	0x23
	.uleb128 0x36
	.uleb128 0x11
	.4byte	.LASF69
	.byte	0x8
	.2byte	0x305
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x11
	.4byte	.LASF70
	.byte	0x8
	.2byte	0x306
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x11
	.4byte	.LASF71
	.byte	0x8
	.2byte	0x307
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x11
	.4byte	.LASF72
	.byte	0x8
	.2byte	0x308
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x11
	.4byte	.LASF73
	.byte	0x8
	.2byte	0x309
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x11
	.4byte	.LASF74
	.byte	0x8
	.2byte	0x30a
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x11
	.4byte	.LASF75
	.byte	0x8
	.2byte	0x30b
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x11
	.4byte	.LASF76
	.byte	0x8
	.2byte	0x30e
	.4byte	0x14b
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x11
	.4byte	.LASF77
	.byte	0x8
	.2byte	0x30f
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x11
	.4byte	.LASF78
	.byte	0x8
	.2byte	0x312
	.4byte	0x611
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x11
	.4byte	.LASF79
	.byte	0x8
	.2byte	0x313
	.4byte	0x611
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x11
	.4byte	.LASF80
	.byte	0x8
	.2byte	0x318
	.4byte	0x611
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x11
	.4byte	.LASF81
	.byte	0x8
	.2byte	0x31b
	.4byte	0x617
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x11
	.4byte	.LASF82
	.byte	0x8
	.2byte	0x320
	.4byte	0x488
	.byte	0x3
	.byte	0x23
	.uleb128 0x9d
	.uleb128 0x11
	.4byte	.LASF83
	.byte	0x8
	.2byte	0x321
	.4byte	0x478
	.byte	0x3
	.byte	0x23
	.uleb128 0xa6
	.uleb128 0x11
	.4byte	.LASF84
	.byte	0x8
	.2byte	0x324
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x11
	.4byte	.LASF85
	.byte	0x8
	.2byte	0x325
	.4byte	0x478
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x11
	.4byte	.LASF86
	.byte	0x8
	.2byte	0x326
	.4byte	0x448
	.byte	0x3
	.byte	0x23
	.uleb128 0xb5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x498
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x627
	.uleb128 0x9
	.4byte	0x25
	.byte	0xc
	.byte	0
	.uleb128 0x12
	.4byte	.LASF87
	.byte	0x8
	.2byte	0x328
	.4byte	0x4a4
	.uleb128 0x13
	.4byte	.LASF88
	.byte	0x1
	.byte	0xad
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x65a
	.uleb128 0x14
	.4byte	.LASF94
	.byte	0x1
	.byte	0xaf
	.4byte	0x9e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x13
	.4byte	.LASF89
	.byte	0x1
	.byte	0xca
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x681
	.uleb128 0x15
	.4byte	.LASF91
	.byte	0x1
	.byte	0xca
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x12a
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x6aa
	.uleb128 0x17
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x12a
	.4byte	0x23b
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x13b
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x6f1
	.uleb128 0x17
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x13b
	.4byte	0x23b
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x17
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x13b
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x18
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x142
	.4byte	0x210
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x19
	.4byte	.LASF156
	.byte	0x1
	.2byte	0x158
	.byte	0x1
	.4byte	0x90
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x72d
	.uleb128 0x17
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x158
	.4byte	0x14b
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x15a
	.4byte	0x90
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x17f
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x774
	.uleb128 0x17
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x17f
	.4byte	0x774
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x17f
	.4byte	0x14b
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x182
	.4byte	0x468
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x90
	.uleb128 0x1a
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x19b
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x1a
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x1a4
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x1a
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x1ad
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x1a
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x1df
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x1b
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x206
	.4byte	0x9e
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x7f5
	.uleb128 0x1c
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x208
	.4byte	0x9e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x241
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x81e
	.uleb128 0x17
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x241
	.4byte	0x23b
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x262
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x847
	.uleb128 0x17
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x262
	.4byte	0x2d3
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x16
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x268
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x87d
	.uleb128 0x1c
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x26a
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x26c
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x27f
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x8a6
	.uleb128 0x18
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x281
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x292
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x8de
	.uleb128 0x17
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x292
	.4byte	0x23b
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x292
	.4byte	0x23b
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x2b9
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x935
	.uleb128 0x17
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x2b9
	.4byte	0x935
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x2b9
	.4byte	0x23b
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x18
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x2bb
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x2bc
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xd
	.4byte	0x9e
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x2ec
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x982
	.uleb128 0x17
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x2ec
	.4byte	0x982
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x1c
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x2ee
	.4byte	0x2f0
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x18
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x2f0
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0xd
	.4byte	0x176
	.uleb128 0x1e
	.4byte	.LASF120
	.byte	0x9
	.2byte	0x169
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF121
	.byte	0x9
	.2byte	0x16c
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x9b3
	.uleb128 0x9
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF122
	.byte	0x9
	.2byte	0x16d
	.4byte	0x9a3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF123
	.byte	0x9
	.2byte	0x16f
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF124
	.byte	0x9
	.2byte	0x171
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF125
	.byte	0x9
	.2byte	0x2ec
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF126
	.byte	0x9
	.2byte	0x3e0
	.4byte	0x9a3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF127
	.byte	0x9
	.2byte	0x3e1
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF128
	.byte	0x9
	.2byte	0x3e2
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF129
	.byte	0x9
	.2byte	0x3e3
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF130
	.byte	0x9
	.2byte	0x3e4
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF131
	.byte	0x9
	.2byte	0x3e5
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF132
	.byte	0x9
	.2byte	0x3e6
	.4byte	0x488
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF133
	.byte	0x9
	.2byte	0x3e7
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF134
	.byte	0x9
	.2byte	0x3e8
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF135
	.byte	0x9
	.2byte	0x3e9
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF136
	.byte	0x9
	.2byte	0x3ea
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF137
	.byte	0x9
	.2byte	0x3eb
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF138
	.byte	0x9
	.2byte	0x3ec
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF139
	.byte	0x9
	.2byte	0x3ed
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF140
	.byte	0xa
	.2byte	0x127
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF141
	.byte	0xa
	.2byte	0x132
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF142
	.byte	0xb
	.byte	0x30
	.4byte	0xadc
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xd
	.4byte	0xb6
	.uleb128 0x14
	.4byte	.LASF143
	.byte	0xb
	.byte	0x34
	.4byte	0xaf2
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xd
	.4byte	0xb6
	.uleb128 0x14
	.4byte	.LASF144
	.byte	0xb
	.byte	0x36
	.4byte	0xb08
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xd
	.4byte	0xb6
	.uleb128 0x14
	.4byte	.LASF145
	.byte	0xb
	.byte	0x38
	.4byte	0xb1e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xd
	.4byte	0xb6
	.uleb128 0x14
	.4byte	.LASF146
	.byte	0xc
	.byte	0x33
	.4byte	0xb34
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xd
	.4byte	0x105
	.uleb128 0x14
	.4byte	.LASF147
	.byte	0xc
	.byte	0x3f
	.4byte	0xb4a
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xd
	.4byte	0x188
	.uleb128 0x1e
	.4byte	.LASF148
	.byte	0x8
	.2byte	0x342
	.4byte	0xb5d
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.byte	0x4
	.4byte	0x627
	.uleb128 0x14
	.4byte	.LASF149
	.byte	0x1
	.byte	0x9a
	.4byte	0x9e
	.byte	0x5
	.byte	0x3
	.4byte	SR_PROGRAMMING_querying_device
	.uleb128 0x1f
	.4byte	.LASF150
	.byte	0x1
	.byte	0x9b
	.4byte	0xb81
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.byte	0x4
	.4byte	0x85
	.uleb128 0x1f
	.4byte	.LASF151
	.byte	0x1
	.byte	0x9c
	.4byte	0xb81
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF152
	.byte	0x1
	.byte	0x9e
	.4byte	0x85
	.byte	0x5
	.byte	0x3
	.4byte	g_SR_PROGRAMMING_previous_cursor_pos
	.uleb128 0x14
	.4byte	.LASF153
	.byte	0x1
	.byte	0xa2
	.4byte	0xbb6
	.byte	0x5
	.byte	0x3
	.4byte	sr_mode_dropdown_lookup
	.uleb128 0xd
	.4byte	0x105
	.uleb128 0x1e
	.4byte	.LASF120
	.byte	0x9
	.2byte	0x169
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF121
	.byte	0x9
	.2byte	0x16c
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF122
	.byte	0x9
	.2byte	0x16d
	.4byte	0x9a3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF123
	.byte	0x9
	.2byte	0x16f
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF124
	.byte	0x9
	.2byte	0x171
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF125
	.byte	0x9
	.2byte	0x2ec
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF126
	.byte	0x9
	.2byte	0x3e0
	.4byte	0x9a3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF127
	.byte	0x9
	.2byte	0x3e1
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF128
	.byte	0x9
	.2byte	0x3e2
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF129
	.byte	0x9
	.2byte	0x3e3
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF130
	.byte	0x9
	.2byte	0x3e4
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF131
	.byte	0x9
	.2byte	0x3e5
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF132
	.byte	0x9
	.2byte	0x3e6
	.4byte	0x488
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF133
	.byte	0x9
	.2byte	0x3e7
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF134
	.byte	0x9
	.2byte	0x3e8
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF135
	.byte	0x9
	.2byte	0x3e9
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF136
	.byte	0x9
	.2byte	0x3ea
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF137
	.byte	0x9
	.2byte	0x3eb
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF138
	.byte	0x9
	.2byte	0x3ec
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF139
	.byte	0x9
	.2byte	0x3ed
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF140
	.byte	0xa
	.2byte	0x127
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF141
	.byte	0xa
	.2byte	0x132
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF148
	.byte	0x8
	.2byte	0x342
	.4byte	0xb5d
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF150
	.byte	0x1
	.byte	0x9b
	.4byte	0xb81
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	CurrentSubnetIDRcvPtr
	.uleb128 0x20
	.4byte	.LASF151
	.byte	0x1
	.byte	0x9c
	.4byte	0xb81
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	CurrentSubnetIDXmtPtr
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI21
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI23
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI25
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI33
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI36
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI39
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI42
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI45
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI48
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xa4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF88:
	.ascii	"display_warning_if_repeater_links_to_itself\000"
.LASF10:
	.ascii	"short int\000"
.LASF111:
	.ascii	"FDTO_SR_PROGRAMMING_process_device_exchange_key\000"
.LASF34:
	.ascii	"_03_structure_to_draw\000"
.LASF64:
	.ascii	"error_code\000"
.LASF100:
	.ascii	"clear_info_text\000"
.LASF33:
	.ascii	"_02_menu\000"
.LASF144:
	.ascii	"GuiFont_DecimalChar\000"
.LASF43:
	.ascii	"pvs_token\000"
.LASF114:
	.ascii	"pcomplete_redraw\000"
.LASF46:
	.ascii	"freq_key\000"
.LASF127:
	.ascii	"GuiVar_SRGroup\000"
.LASF61:
	.ascii	"subnet_rcv_id\000"
.LASF68:
	.ascii	"progress_text\000"
.LASF118:
	.ascii	"SR_PROGRAMMING_process_screen\000"
.LASF98:
	.ascii	"temp_string\000"
.LASF148:
	.ascii	"sr_state\000"
.LASF14:
	.ascii	"BOOL_32\000"
.LASF28:
	.ascii	"code_time\000"
.LASF79:
	.ascii	"write_pvs\000"
.LASF102:
	.ascii	"get_guivars_from_sr_programming_struct\000"
.LASF124:
	.ascii	"GuiVar_CommOptionPortBIndex\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF20:
	.ascii	"keycode\000"
.LASF2:
	.ascii	"long long int\000"
.LASF58:
	.ascii	"slave_and_repeater\000"
.LASF116:
	.ascii	"lport\000"
.LASF9:
	.ascii	"INT_16\000"
.LASF101:
	.ascii	"set_sr_programming_struct_from_guivars\000"
.LASF110:
	.ascii	"subnet_rcv\000"
.LASF126:
	.ascii	"GuiVar_SRFirmwareVer\000"
.LASF1:
	.ascii	"long int\000"
.LASF156:
	.ascii	"get_subnet_id_from_string\000"
.LASF90:
	.ascii	"navigate_around_subnet_rcv_field\000"
.LASF63:
	.ascii	"SR_PROGRAMMABLE_VALUES_STRUCT\000"
.LASF41:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF119:
	.ascii	"pkey_event\000"
.LASF27:
	.ascii	"code_date\000"
.LASF72:
	.ascii	"group_is_valid\000"
.LASF81:
	.ascii	"sw_version\000"
.LASF62:
	.ascii	"subnet_xmt_id\000"
.LASF129:
	.ascii	"GuiVar_SRPort\000"
.LASF145:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF53:
	.ascii	"master_packet_repeat\000"
.LASF36:
	.ascii	"key_process_func_ptr\000"
.LASF93:
	.ascii	"start_sr_device_communication\000"
.LASF60:
	.ascii	"subnet_id\000"
.LASF113:
	.ascii	"pport\000"
.LASF149:
	.ascii	"SR_PROGRAMMING_querying_device\000"
.LASF48:
	.ascii	"min_packet_size\000"
.LASF30:
	.ascii	"reason_for_scan\000"
.LASF29:
	.ascii	"port\000"
.LASF32:
	.ascii	"_01_command\000"
.LASF80:
	.ascii	"active_pvs\000"
.LASF31:
	.ascii	"COMM_MNGR_TASK_QUEUE_STRUCT\000"
.LASF125:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF121:
	.ascii	"GuiVar_CommOptionDeviceExchangeResult\000"
.LASF131:
	.ascii	"GuiVar_SRRepeaterLinkedToItself\000"
.LASF103:
	.ascii	"sr_values_in_range\000"
.LASF95:
	.ascii	"cmqs\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF19:
	.ascii	"DATA_HANDLE\000"
.LASF73:
	.ascii	"repeater_is_valid\000"
.LASF71:
	.ascii	"mode_is_valid\000"
.LASF13:
	.ascii	"long long unsigned int\000"
.LASF24:
	.ascii	"event\000"
.LASF8:
	.ascii	"short unsigned int\000"
.LASF154:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF38:
	.ascii	"_06_u32_argument1\000"
.LASF82:
	.ascii	"serial_number\000"
.LASF151:
	.ascii	"CurrentSubnetIDXmtPtr\000"
.LASF35:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF65:
	.ascii	"operation\000"
.LASF112:
	.ascii	"pkeycode\000"
.LASF143:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF15:
	.ascii	"from\000"
.LASF40:
	.ascii	"_08_screen_to_draw\000"
.LASF132:
	.ascii	"GuiVar_SRSerialNumber\000"
.LASF75:
	.ascii	"programming_successful\000"
.LASF99:
	.ascii	"get_info_text_from_sr_programming_struct\000"
.LASF104:
	.ascii	"SR_PROGRAMMING_initialize_guivars\000"
.LASF139:
	.ascii	"GuiVar_SRTransmitPower\000"
.LASF44:
	.ascii	"modem_mode\000"
.LASF135:
	.ascii	"GuiVar_SRSubnetRcvSlave\000"
.LASF94:
	.ascii	"lprev_value\000"
.LASF107:
	.ascii	"FDTO_SR_PROGRAMMING_show_mode_dropdown\000"
.LASF47:
	.ascii	"max_packet_size\000"
.LASF105:
	.ascii	"FDTO_SR_PROGRAMMING_populate_mode_dropdown\000"
.LASF22:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF122:
	.ascii	"GuiVar_CommOptionInfoText\000"
.LASF70:
	.ascii	"write_list_index\000"
.LASF78:
	.ascii	"dynamic_pvs\000"
.LASF150:
	.ascii	"CurrentSubnetIDRcvPtr\000"
.LASF26:
	.ascii	"message_class\000"
.LASF138:
	.ascii	"GuiVar_SRSubnetXmtSlave\000"
.LASF115:
	.ascii	"lcursor_to_select\000"
.LASF133:
	.ascii	"GuiVar_SRSubnetRcvMaster\000"
.LASF23:
	.ascii	"float\000"
.LASF25:
	.ascii	"who_the_message_was_to\000"
.LASF142:
	.ascii	"GuiFont_LanguageActive\000"
.LASF18:
	.ascii	"dlen\000"
.LASF153:
	.ascii	"sr_mode_dropdown_lookup\000"
.LASF50:
	.ascii	"rf_xmit_power\000"
.LASF4:
	.ascii	"unsigned char\000"
.LASF141:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF67:
	.ascii	"info_text\000"
.LASF66:
	.ascii	"operation_text\000"
.LASF84:
	.ascii	"group_index\000"
.LASF97:
	.ascii	"get_subnet_string_from_id\000"
.LASF89:
	.ascii	"set_sr_mode\000"
.LASF45:
	.ascii	"baud_rate\000"
.LASF54:
	.ascii	"max_slave_retry\000"
.LASF120:
	.ascii	"GuiVar_ComboBoxItemIndex\000"
.LASF76:
	.ascii	"resp_ptr\000"
.LASF57:
	.ascii	"network_id\000"
.LASF123:
	.ascii	"GuiVar_CommOptionPortAIndex\000"
.LASF17:
	.ascii	"dptr\000"
.LASF83:
	.ascii	"model\000"
.LASF59:
	.ascii	"diagnostics\000"
.LASF136:
	.ascii	"GuiVar_SRSubnetXmtMaster\000"
.LASF74:
	.ascii	"radio_settings_are_valid\000"
.LASF155:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_sr_programming.c\000"
.LASF109:
	.ascii	"FDTO_SR_PROGRAMMING_show_receives_from_dropdown\000"
.LASF3:
	.ascii	"char\000"
.LASF91:
	.ascii	"mode\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF108:
	.ascii	"lactive_line\000"
.LASF96:
	.ascii	"subnet_string\000"
.LASF106:
	.ascii	"pindex_0\000"
.LASF56:
	.ascii	"repeater_frequency\000"
.LASF21:
	.ascii	"repeats\000"
.LASF152:
	.ascii	"g_SR_PROGRAMMING_previous_cursor_pos\000"
.LASF16:
	.ascii	"ADDR_TYPE\000"
.LASF147:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF69:
	.ascii	"read_list_index\000"
.LASF51:
	.ascii	"low_power_mode\000"
.LASF137:
	.ascii	"GuiVar_SRSubnetXmtRepeater\000"
.LASF140:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF130:
	.ascii	"GuiVar_SRRepeater\000"
.LASF7:
	.ascii	"UNS_16\000"
.LASF52:
	.ascii	"number_of_repeators\000"
.LASF6:
	.ascii	"UNS_8\000"
.LASF39:
	.ascii	"_07_u32_argument2\000"
.LASF77:
	.ascii	"resp_len\000"
.LASF37:
	.ascii	"_04_func_ptr\000"
.LASF117:
	.ascii	"FDTO_SR_PROGRAMMING_draw_screen\000"
.LASF85:
	.ascii	"network_group_id\000"
.LASF55:
	.ascii	"retry_odds\000"
.LASF5:
	.ascii	"signed char\000"
.LASF87:
	.ascii	"SR_STATE_STRUCT\000"
.LASF146:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF92:
	.ascii	"alternate_field\000"
.LASF49:
	.ascii	"rf_data_rate\000"
.LASF42:
	.ascii	"double\000"
.LASF86:
	.ascii	"repeater_group_id\000"
.LASF134:
	.ascii	"GuiVar_SRSubnetRcvRepeater\000"
.LASF128:
	.ascii	"GuiVar_SRMode\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
