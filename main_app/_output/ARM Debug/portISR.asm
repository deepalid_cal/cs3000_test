	.file	"portISR.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.global	ulCriticalNesting
	.section	.data.ulCriticalNesting,"aw",%progbits
	.align	2
	.type	ulCriticalNesting, %object
	.size	ulCriticalNesting, 4
ulCriticalNesting:
	.word	9999
	.section	.text.vPortISRStartFirstTask,"ax",%progbits
	.align	2
	.global	vPortISRStartFirstTask
	.type	vPortISRStartFirstTask, %function
vPortISRStartFirstTask:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c"
	.loc 1 69 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
.LBB2:
	.loc 1 72 0
@ 72 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	LDR		R0, =pxCurrentTCB								
	LDR		R0, [R0]										
	LDR		LR, [R0]										
	LDR		R0, =ulCriticalNesting							
	LDMFD	LR!, {R1}											
	STR		R1, [R0]										
	LDMFD	LR!, {R0}											
	MSR		SPSR, R0										
	LDMFD	LR, {R0-R14}^										
	NOP														
	LDR		LR, [LR, #+60]									
	SUBS	PC, LR, #4											
	
@ 0 "" 2
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	ldr	r3, .L2+4
	ldr	r3, [r3, #0]
.LBE2:
	.loc 1 73 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L3:
	.align	2
.L2:
	.word	ulCriticalNesting
	.word	pxCurrentTCB
.LFE0:
	.size	vPortISRStartFirstTask, .-vPortISRStartFirstTask
	.section	.text.vPortYieldProcessor,"ax",%progbits
	.align	2
	.global	vPortYieldProcessor
	.type	vPortYieldProcessor, %function
vPortYieldProcessor:
.LFB1:
	.loc 1 85 0
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	.loc 1 89 0
@ 89 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	ADD		LR, LR, #4
@ 0 "" 2
.LBB3:
	.loc 1 92 0
@ 92 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	STMDB	SP!, {R0}											
	STMDB	SP,{SP}^											
	NOP														
	SUB	SP, SP, #4											
	LDMIA	SP!,{R0}											
	STMDB	R0!, {LR}											
	MOV	LR, R0												
	LDMIA	SP!, {R0}											
	STMDB	LR,{R0-LR}^											
	NOP														
	SUB	LR, LR, #60											
	MRS	R0, SPSR											
	STMDB	LR!, {R0}											
	LDR	R0, =ulCriticalNesting								
	LDR	R0, [R0]											
	STMDB	LR!, {R0}											
	LDR	R0, =pxCurrentTCB									
	LDR	R0, [R0]											
	STR	LR, [R0]											
	
@ 0 "" 2
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	ldr	r3, .L5+4
	ldr	r3, [r3, #0]
.LBE3:
	.loc 1 95 0
@ 95 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	bl			vTaskSwitchContext
@ 0 "" 2
.LBB4:
	.loc 1 98 0
@ 98 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	LDR		R0, =pxCurrentTCB								
	LDR		R0, [R0]										
	LDR		LR, [R0]										
	LDR		R0, =ulCriticalNesting							
	LDMFD	LR!, {R1}											
	STR		R1, [R0]										
	LDMFD	LR!, {R0}											
	MSR		SPSR, R0										
	LDMFD	LR, {R0-R14}^										
	NOP														
	LDR		LR, [LR, #+60]									
	SUBS	PC, LR, #4											
	
@ 0 "" 2
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	ldr	r3, .L5+4
	ldr	r3, [r3, #0]
.LBE4:
	.loc 1 99 0
.L6:
	.align	2
.L5:
	.word	ulCriticalNesting
	.word	pxCurrentTCB
.LFE1:
	.size	vPortYieldProcessor, .-vPortYieldProcessor
	.global	my_tick_count
	.section	.bss.my_tick_count,"aw",%nobits
	.align	2
	.type	my_tick_count, %object
	.size	my_tick_count, 4
my_tick_count:
	.space	4
	.section	.text.vPreemptiveTick,"ax",%progbits
	.align	2
	.global	vPreemptiveTick
	.type	vPreemptiveTick, %function
vPreemptiveTick:
.LFB2:
	.loc 1 134 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI2:
	add	fp, sp, #4
.LCFI3:
	.loc 1 137 0
	ldr	r3, .L8
	mov	r2, #255
	str	r2, [r3, #0]
	.loc 1 141 0
	ldr	r3, .L8+4
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L8+4
	str	r2, [r3, #0]
	.loc 1 160 0
	bl	vTaskIncrementTick
	.loc 1 162 0
	bl	vTaskSwitchContext
	.loc 1 163 0
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	1073938432
	.word	my_tick_count
.LFE2:
	.size	vPreemptiveTick, .-vPreemptiveTick
	.section	.text.vPortEnterCritical,"ax",%progbits
	.align	2
	.global	vPortEnterCritical
	.type	vPortEnterCritical, %function
vPortEnterCritical:
.LFB3:
	.loc 1 210 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI4:
	add	fp, sp, #0
.LCFI5:
	.loc 1 211 0
@ 211 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	STMDB	SP!, {R0}		
	MRS	R0, CPSR		
	ORR	R0, R0, #0xC0	
	MSR	CPSR, R0		
	LDMIA	SP!, {R0}			
@ 0 "" 2
	.loc 1 216 0
	ldr	r3, .L11
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L11
	str	r2, [r3, #0]
	.loc 1 217 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L12:
	.align	2
.L11:
	.word	ulCriticalNesting
.LFE3:
	.size	vPortEnterCritical, .-vPortEnterCritical
	.section	.text.vPortExitCritical,"ax",%progbits
	.align	2
	.global	vPortExitCritical
	.type	vPortExitCritical, %function
vPortExitCritical:
.LFB4:
	.loc 1 220 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	.loc 1 221 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L13
	.loc 1 224 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L15
	str	r2, [r3, #0]
	.loc 1 228 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L13
	.loc 1 230 0
@ 230 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	STMDB	SP!, {R0}		
	MRS	R0, CPSR		
	BIC	R0, R0, #0xC0	
	MSR	CPSR, R0		
	LDMIA	SP!, {R0}			
@ 0 "" 2
.L13:
	.loc 1 233 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L16:
	.align	2
.L15:
	.word	ulCriticalNesting
.LFE4:
	.size	vPortExitCritical, .-vPortExitCritical
	.section	.text.vPortSaveVFPRegisters,"ax",%progbits
	.align	2
	.global	vPortSaveVFPRegisters
	.type	vPortSaveVFPRegisters, %function
vPortSaveVFPRegisters:
.LFB5:
	.loc 1 244 0
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	.loc 1 247 0
@ 247 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	FSTD	D0, [ r0, #+0 * 8 ]         
	FSTD	D1, [ r0, #+1 * 8 ]         
	FSTD	D2, [ r0, #+2 * 8 ]         
	FSTD	D3, [ r0, #+3 * 8 ]         
	FSTD	D4, [ r0, #+4 * 8 ]         
	FSTD	D5, [ r0, #+5 * 8 ]         
	FSTD	D6, [ r0, #+6 * 8 ]         
	FSTD	D7, [ r0, #+7 * 8 ]         
	FSTD	D8, [ r0, #+8 * 8 ]         
	FSTD	D9, [ r0, #+9 * 8 ]         
	FSTD	D10, [ r0, #+10 * 8 ]       
	FSTD	D11, [ r0, #+11 * 8 ]       
	FSTD	D12, [ r0, #+12 * 8 ]       
	FSTD	D13, [ r0, #+13 * 8 ]       
	FSTD	D14, [ r0, #+14 * 8 ]       
	FSTD	D15, [ r0, #+15 * 8 ]       
	FMRX	R2, FPSCR					
	str    R2, [ r0, #+16 * 8 ]        
	BLX R14
@ 0 "" 2
	.loc 1 317 0
.LFE5:
	.size	vPortSaveVFPRegisters, .-vPortSaveVFPRegisters
	.section	.text.vPortRestoreVFPRegisters,"ax",%progbits
	.align	2
	.global	vPortRestoreVFPRegisters
	.type	vPortRestoreVFPRegisters, %function
vPortRestoreVFPRegisters:
.LFB6:
	.loc 1 327 0
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	.loc 1 330 0
@ 330 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	LDR 	R2, [ r0, #+16 * 8 ]        
	FLDD	D0, [ r0, #+0 * 8 ]         
	FLDD	D1, [ r0, #+1 * 8 ]         
	FLDD	D2, [ r0, #+2 * 8 ]         
	FLDD	D3, [ r0, #+3 * 8 ]         
	FLDD	D4, [ r0, #+4 * 8 ]         
	FLDD	D5, [ r0, #+5 * 8 ]         
	FLDD	D6, [ r0, #+6 * 8 ]         
	FLDD	D7, [ r0, #+7 * 8 ]         
	FLDD	D8, [ r0, #+8 * 8 ]         
	FLDD	D9, [ r0, #+9 * 8 ]         
	FLDD	D10, [ r0, #+10 * 8 ]       
	FLDD	D11, [ r0, #+11 * 8 ]       
	FLDD	D12, [ r0, #+12 * 8 ]       
	FLDD	D13, [ r0, #+13 * 8 ]       
	FLDD	D14, [ r0, #+14 * 8 ]       
	FLDD	D15, [ r0, #+15 * 8 ]       
	FMXR 	FPSCR, R2					
	BLX R14
@ 0 "" 2
	.loc 1 401 0
.LFE6:
	.size	vPortRestoreVFPRegisters, .-vPortRestoreVFPRegisters
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI6-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/FreeRTOSConfig.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x35a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF29
	.byte	0x1
	.4byte	.LASF30
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x4
	.byte	0x4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x5
	.4byte	0x25
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF8
	.uleb128 0x6
	.4byte	.LASF12
	.byte	0x3
	.byte	0x5e
	.4byte	0x7d
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x7
	.byte	0x74
	.byte	0x2
	.byte	0x28
	.4byte	0x12f
	.uleb128 0x8
	.ascii	"ir\000"
	.byte	0x2
	.byte	0x2a
	.4byte	0x12f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.ascii	"tcr\000"
	.byte	0x2
	.byte	0x2b
	.4byte	0x12f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x8
	.ascii	"tc\000"
	.byte	0x2
	.byte	0x2c
	.4byte	0x12f
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x8
	.ascii	"pr\000"
	.byte	0x2
	.byte	0x2d
	.4byte	0x12f
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x8
	.ascii	"pc\000"
	.byte	0x2
	.byte	0x2e
	.4byte	0x12f
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x8
	.ascii	"mcr\000"
	.byte	0x2
	.byte	0x2f
	.4byte	0x12f
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x8
	.ascii	"mr\000"
	.byte	0x2
	.byte	0x30
	.4byte	0x144
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x8
	.ascii	"ccr\000"
	.byte	0x2
	.byte	0x31
	.4byte	0x12f
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x8
	.ascii	"cr\000"
	.byte	0x2
	.byte	0x32
	.4byte	0x149
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x8
	.ascii	"emr\000"
	.byte	0x2
	.byte	0x33
	.4byte	0x12f
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x9
	.4byte	.LASF10
	.byte	0x2
	.byte	0x34
	.4byte	0x15e
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x9
	.4byte	.LASF11
	.byte	0x2
	.byte	0x35
	.4byte	0x12f
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.byte	0
	.uleb128 0x5
	.4byte	0x72
	.uleb128 0xa
	.4byte	0x72
	.4byte	0x144
	.uleb128 0xb
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x5
	.4byte	0x134
	.uleb128 0x5
	.4byte	0x134
	.uleb128 0xa
	.4byte	0x72
	.4byte	0x15e
	.uleb128 0xb
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x5
	.4byte	0x14e
	.uleb128 0x6
	.4byte	.LASF13
	.byte	0x2
	.byte	0x36
	.4byte	0x84
	.uleb128 0xa
	.4byte	0x51
	.4byte	0x17e
	.uleb128 0xb
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.byte	0x44
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1bc
	.uleb128 0xd
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0xe
	.4byte	.LASF14
	.byte	0x1
	.byte	0x48
	.4byte	0x1bc
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF15
	.byte	0x1
	.byte	0x48
	.4byte	0x66
	.byte	0x1
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x5
	.4byte	0x1c1
	.uleb128 0xf
	.byte	0x4
	.4byte	0x1c7
	.uleb128 0x10
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.byte	0x54
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	0x22d
	.uleb128 0x12
	.4byte	.LBB3
	.4byte	.LBE3
	.4byte	0x208
	.uleb128 0xe
	.4byte	.LASF14
	.byte	0x1
	.byte	0x5c
	.4byte	0x1bc
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF15
	.byte	0x1
	.byte	0x5c
	.4byte	0x66
	.byte	0x1
	.byte	0x1
	.byte	0
	.uleb128 0xd
	.4byte	.LBB4
	.4byte	.LBE4
	.uleb128 0xe
	.4byte	.LASF14
	.byte	0x1
	.byte	0x62
	.4byte	0x1bc
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF15
	.byte	0x1
	.byte	0x62
	.4byte	0x66
	.byte	0x1
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.byte	0x85
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.byte	0xd1
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.byte	0xdb
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.byte	0xf3
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	0x292
	.uleb128 0x14
	.4byte	.LASF23
	.byte	0x1
	.byte	0xf3
	.4byte	0x3a
	.byte	0x1
	.byte	0x50
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x146
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	0x2ba
	.uleb128 0x16
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x146
	.4byte	0x3a
	.byte	0x1
	.byte	0x50
	.byte	0
	.uleb128 0xe
	.4byte	.LASF24
	.byte	0x4
	.byte	0x7c
	.4byte	0x25
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF25
	.byte	0x5
	.byte	0x30
	.4byte	0x2d8
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x18
	.4byte	0x16e
	.uleb128 0x17
	.4byte	.LASF26
	.byte	0x5
	.byte	0x34
	.4byte	0x2ee
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x18
	.4byte	0x16e
	.uleb128 0x17
	.4byte	.LASF27
	.byte	0x5
	.byte	0x36
	.4byte	0x304
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x18
	.4byte	0x16e
	.uleb128 0x17
	.4byte	.LASF28
	.byte	0x5
	.byte	0x38
	.4byte	0x31a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x18
	.4byte	0x16e
	.uleb128 0xe
	.4byte	.LASF15
	.byte	0x1
	.byte	0x33
	.4byte	0x66
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF24
	.byte	0x1
	.byte	0x6b
	.4byte	0x25
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	my_tick_count
	.uleb128 0x19
	.4byte	.LASF15
	.byte	0x1
	.byte	0x33
	.4byte	0x66
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	ulCriticalNesting
	.uleb128 0xe
	.4byte	.LASF14
	.byte	0x1
	.byte	0x62
	.4byte	0x1bc
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x35
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF29:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF16:
	.ascii	"vPortISRStartFirstTask\000"
.LASF3:
	.ascii	"short int\000"
.LASF30:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/portable/GCC/ARM9_LPC32xx/portISR.c\000"
.LASF15:
	.ascii	"ulCriticalNesting\000"
.LASF6:
	.ascii	"long long int\000"
.LASF22:
	.ascii	"vPortRestoreVFPRegisters\000"
.LASF4:
	.ascii	"long int\000"
.LASF17:
	.ascii	"vPortYieldProcessor\000"
.LASF21:
	.ascii	"vPortSaveVFPRegisters\000"
.LASF27:
	.ascii	"GuiFont_DecimalChar\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF2:
	.ascii	"signed char\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF14:
	.ascii	"pxCurrentTCB\000"
.LASF26:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF10:
	.ascii	"rsvd2\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF19:
	.ascii	"vPortEnterCritical\000"
.LASF8:
	.ascii	"char\000"
.LASF28:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF20:
	.ascii	"vPortExitCritical\000"
.LASF24:
	.ascii	"my_tick_count\000"
.LASF23:
	.ascii	"pvTaskTag\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF11:
	.ascii	"ctcr\000"
.LASF25:
	.ascii	"GuiFont_LanguageActive\000"
.LASF13:
	.ascii	"TIMER_CNTR_REGS_T\000"
.LASF12:
	.ascii	"UNS_32\000"
.LASF18:
	.ascii	"vPreemptiveTick\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
