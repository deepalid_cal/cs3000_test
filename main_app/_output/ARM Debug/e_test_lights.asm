	.file	"e_test_lights.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.g_LIGHTS_TEST_irri_list_stop_date,"aw",%nobits
	.align	2
	.type	g_LIGHTS_TEST_irri_list_stop_date, %object
	.size	g_LIGHTS_TEST_irri_list_stop_date, 4
g_LIGHTS_TEST_irri_list_stop_date:
	.space	4
	.section	.bss.g_LIGHTS_TEST_irri_list_stop_time_in_seconds,"aw",%nobits
	.align	2
	.type	g_LIGHTS_TEST_irri_list_stop_time_in_seconds, %object
	.size	g_LIGHTS_TEST_irri_list_stop_time_in_seconds, 4
g_LIGHTS_TEST_irri_list_stop_time_in_seconds:
	.space	4
	.section	.bss.g_LIGHTS_TEST_editable_stop_date,"aw",%nobits
	.align	2
	.type	g_LIGHTS_TEST_editable_stop_date, %object
	.size	g_LIGHTS_TEST_editable_stop_date, 4
g_LIGHTS_TEST_editable_stop_date:
	.space	4
	.section	.bss.g_LIGHTS_TEST_editable_stop_time_in_seconds,"aw",%nobits
	.align	2
	.type	g_LIGHTS_TEST_editable_stop_time_in_seconds, %object
	.size	g_LIGHTS_TEST_editable_stop_time_in_seconds, 4
g_LIGHTS_TEST_editable_stop_time_in_seconds:
	.space	4
	.section	.text.LIGHTS_TEST_turn_light_on_or_off,"ax",%progbits
	.align	2
	.type	LIGHTS_TEST_turn_light_on_or_off, %function
LIGHTS_TEST_turn_light_on_or_off:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_test_lights.c"
	.loc 1 70 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #28
.LCFI2:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	.loc 1 75 0
	ldr	r3, [fp, #-32]
	cmp	r3, #1
	bne	.L2
	.loc 1 78 0
	ldr	r3, .L7
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-16]	@ movhi
	.loc 1 79 0
	ldr	r3, .L7+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 81 0
	sub	r3, fp, #12
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 84 0
	sub	r2, fp, #12
	sub	r3, fp, #20
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #0
	beq	.L3
	.loc 1 86 0
	bl	bad_key_beep
	b	.L1
.L3:
	.loc 1 90 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	bl	LIGHTS_get_lights_array_index
	mov	r1, r0
	ldr	r3, .L7
	ldr	r2, [r3, #0]
	ldr	r3, .L7+4
	ldr	r3, [r3, #0]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	IRRI_LIGHTS_request_light_on
	mov	r3, r0
	cmp	r3, #0
	beq	.L5
	.loc 1 92 0
	bl	good_key_beep
	b	.L1
.L5:
	.loc 1 96 0
	bl	bad_key_beep
	b	.L1
.L2:
	.loc 1 102 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	bl	IRRI_LIGHTS_request_light_off
	mov	r3, r0
	cmp	r3, #0
	beq	.L6
	.loc 1 104 0
	bl	good_key_beep
	b	.L1
.L6:
	.loc 1 108 0
	bl	bad_key_beep
.L1:
	.loc 1 112 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L8:
	.align	2
.L7:
	.word	g_LIGHTS_TEST_editable_stop_date
	.word	g_LIGHTS_TEST_editable_stop_time_in_seconds
.LFE0:
	.size	LIGHTS_TEST_turn_light_on_or_off, .-LIGHTS_TEST_turn_light_on_or_off
	.section	.text.LIGHTS_TEST_calculate_default_off_date_and_time,"ax",%progbits
	.align	2
	.type	LIGHTS_TEST_calculate_default_off_date_and_time, %function
LIGHTS_TEST_calculate_default_off_date_and_time:
.LFB1:
	.loc 1 116 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #16
.LCFI5:
	.loc 1 129 0
	sub	r3, fp, #20
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 132 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L13
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #6
	mov	r2, #600
	mul	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 135 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L13
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #6
	mov	r1, #600
	mul	r3, r1, r3
	rsb	r3, r3, r2
	ldr	r2, .L13+4
	cmp	r3, r2
	bls	.L10
	.loc 1 137 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #600
	str	r3, [fp, #-8]
.L10:
	.loc 1 142 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L13+8
	cmp	r2, r3
	bhi	.L11
	.loc 1 144 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #14400
	ldr	r3, .L13+12
	str	r2, [r3, #0]
	.loc 1 145 0
	ldrh	r3, [fp, #-16]
	mov	r2, r3
	ldr	r3, .L13+16
	str	r2, [r3, #0]
	b	.L9
.L11:
	.loc 1 151 0
	ldr	r3, [fp, #-8]
	rsb	r3, r3, #86016
	add	r3, r3, #384
	str	r3, [fp, #-12]
	.loc 1 152 0
	ldr	r3, [fp, #-12]
	rsb	r2, r3, #14400
	ldr	r3, .L13+12
	str	r2, [r3, #0]
	.loc 1 153 0
	ldrh	r3, [fp, #-16]
	add	r3, r3, #1
	mov	r2, r3
	ldr	r3, .L13+16
	str	r2, [r3, #0]
.L9:
	.loc 1 156 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L14:
	.align	2
.L13:
	.word	458129845
	.word	299
	.word	71999
	.word	g_LIGHTS_TEST_editable_stop_time_in_seconds
	.word	g_LIGHTS_TEST_editable_stop_date
.LFE1:
	.size	LIGHTS_TEST_calculate_default_off_date_and_time, .-LIGHTS_TEST_calculate_default_off_date_and_time
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_test_lights.c\000"
	.section	.text.nm_LIGHTS_TEST_load_irri_list_stop_date_and_time,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_TEST_load_irri_list_stop_date_and_time, %function
nm_LIGHTS_TEST_load_irri_list_stop_date_and_time:
.LFB2:
	.loc 1 160 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	.loc 1 166 0
	ldr	r3, .L17
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L15
	.loc 1 168 0
	ldr	r3, .L17+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L17+8
	mov	r3, #168
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 171 0
	ldr	r3, .L17+12
	ldr	r2, [r3, #0]
	ldr	r3, .L17+16
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	sub	r2, fp, #12
	mov	r0, r2
	mov	r1, r3
	bl	IRRI_LIGHTS_get_light_stop_date_and_time
	.loc 1 174 0
	ldrh	r3, [fp, #-8]
	mov	r2, r3
	ldr	r3, .L17+20
	str	r2, [r3, #0]
	.loc 1 175 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L17+24
	str	r2, [r3, #0]
	.loc 1 180 0
	ldr	r3, .L17+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L15:
	.loc 1 182 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L18:
	.align	2
.L17:
	.word	GuiVar_LightIsEnergized
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	g_LIGHTS_TEST_irri_list_stop_date
	.word	g_LIGHTS_TEST_irri_list_stop_time_in_seconds
.LFE2:
	.size	nm_LIGHTS_TEST_load_irri_list_stop_date_and_time, .-nm_LIGHTS_TEST_load_irri_list_stop_date_and_time
	.section	.text.nm_LIGHTS_TEST_load_editable_stop_date_and_time,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_TEST_load_editable_stop_date_and_time, %function
nm_LIGHTS_TEST_load_editable_stop_date_and_time:
.LFB3:
	.loc 1 186 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	.loc 1 190 0
	ldr	r3, .L22
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L20
	.loc 1 192 0
	ldr	r3, .L22+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L22+8
	mov	r3, #192
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 195 0
	bl	nm_LIGHTS_TEST_load_irri_list_stop_date_and_time
	.loc 1 197 0
	ldr	r3, .L22+12
	ldr	r2, [r3, #0]
	ldr	r3, .L22+16
	str	r2, [r3, #0]
	.loc 1 198 0
	ldr	r3, .L22+20
	ldr	r2, [r3, #0]
	ldr	r3, .L22+24
	str	r2, [r3, #0]
	.loc 1 200 0
	ldr	r3, .L22+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L19
.L20:
	.loc 1 206 0
	bl	LIGHTS_TEST_calculate_default_off_date_and_time
.L19:
	.loc 1 208 0
	ldmfd	sp!, {fp, pc}
.L23:
	.align	2
.L22:
	.word	GuiVar_LightIsEnergized
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	g_LIGHTS_TEST_irri_list_stop_date
	.word	g_LIGHTS_TEST_editable_stop_date
	.word	g_LIGHTS_TEST_irri_list_stop_time_in_seconds
	.word	g_LIGHTS_TEST_editable_stop_time_in_seconds
.LFE3:
	.size	nm_LIGHTS_TEST_load_editable_stop_date_and_time, .-nm_LIGHTS_TEST_load_editable_stop_date_and_time
	.section .rodata
	.align	2
.LC1:
	.ascii	"%s, %s\000"
	.section	.text.nm_LIGHTS_TEST_update_stop_date_and_time_panels,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_TEST_update_stop_date_and_time_panels, %function
nm_LIGHTS_TEST_update_stop_date_and_time_panels:
.LFB4:
	.loc 1 212 0
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI11:
	add	fp, sp, #8
.LCFI12:
	sub	sp, sp, #64
.LCFI13:
	.loc 1 222 0
	ldr	r3, .L30
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L25
	.loc 1 224 0
	ldr	r3, .L30+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L30+8
	mov	r3, #224
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 227 0
	bl	nm_LIGHTS_TEST_load_irri_list_stop_date_and_time
	.loc 1 230 0
	ldr	r3, .L30+12
	ldr	r2, [r3, #0]
	ldr	r3, .L30+16
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L26
	.loc 1 231 0 discriminator 1
	ldr	r3, .L30+20
	ldr	r2, [r3, #0]
	ldr	r3, .L30+24
	ldr	r3, [r3, #0]
	.loc 1 230 0 discriminator 1
	cmp	r2, r3
	beq	.L27
.L26:
	.loc 1 234 0
	ldr	r3, .L30+28
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L28
.L27:
	.loc 1 239 0
	ldr	r3, .L30+28
	mov	r2, #0
	str	r2, [r3, #0]
.L28:
	.loc 1 242 0
	ldr	r3, .L30+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L29
.L25:
	.loc 1 247 0
	ldr	r3, .L30+28
	mov	r2, #0
	str	r2, [r3, #0]
.L29:
	.loc 1 252 0
	ldr	r3, .L30+24
	ldr	r3, [r3, #0]
	sub	r2, fp, #64
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, #8
	mov	r2, r3
	mov	r3, #0
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r4, r0
	ldr	r3, .L30+16
	ldr	r3, [r3, #0]
	sub	r2, fp, #56
	mov	r1, #200
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #100
	bl	GetDateStr
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r0, .L30+32
	mov	r1, #33
	ldr	r2, .L30+36
	mov	r3, r4
	bl	snprintf
	.loc 1 261 0
	ldr	r3, .L30+20
	ldr	r3, [r3, #0]
	sub	r2, fp, #64
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, #8
	mov	r2, r3
	mov	r3, #0
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r4, r0
	ldr	r3, .L30+12
	ldr	r3, [r3, #0]
	sub	r2, fp, #56
	mov	r1, #200
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #100
	bl	GetDateStr
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r0, .L30+40
	mov	r1, #33
	ldr	r2, .L30+36
	mov	r3, r4
	bl	snprintf
	.loc 1 267 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L31:
	.align	2
.L30:
	.word	GuiVar_LightIsEnergized
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	g_LIGHTS_TEST_editable_stop_date
	.word	g_LIGHTS_TEST_irri_list_stop_date
	.word	g_LIGHTS_TEST_editable_stop_time_in_seconds
	.word	g_LIGHTS_TEST_irri_list_stop_time_in_seconds
	.word	GuiVar_LightsStopTimeHasBeenEdited
	.word	GuiVar_LightIrriListStop
	.word	.LC1
	.word	GuiVar_LightEditableStop
.LFE4:
	.size	nm_LIGHTS_TEST_update_stop_date_and_time_panels, .-nm_LIGHTS_TEST_update_stop_date_and_time_panels
	.section	.text.LIGHTS_TEST_process_manual_stop_date_and_time,"ax",%progbits
	.align	2
	.type	LIGHTS_TEST_process_manual_stop_date_and_time, %function
LIGHTS_TEST_process_manual_stop_date_and_time:
.LFB5:
	.loc 1 271 0
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI14:
	add	fp, sp, #4
.LCFI15:
	sub	sp, sp, #36
.LCFI16:
	str	r0, [fp, #-40]
	.loc 1 291 0
	sub	r3, fp, #12
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 297 0
	ldr	r3, .L43
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-16]	@ movhi
	.loc 1 298 0
	ldr	r3, .L43+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 300 0
	ldr	r3, [fp, #-40]
	cmp	r3, #84
	bne	.L33
	.loc 1 303 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L43+8
	cmp	r2, r3
	bhi	.L34
	.loc 1 306 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #600
	str	r3, [fp, #-20]
	b	.L35
.L34:
	.loc 1 311 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 312 0
	ldrh	r3, [fp, #-16]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-16]	@ movhi
.L35:
	.loc 1 316 0
	ldrh	r3, [fp, #-8]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-32]	@ movhi
	.loc 1 317 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-36]
	.loc 1 319 0
	sub	r2, fp, #20
	sub	r3, fp, #36
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L36
	.loc 1 322 0
	bl	bad_key_beep
	b	.L37
.L36:
	.loc 1 327 0
	bl	good_key_beep
	.loc 1 328 0
	ldrh	r3, [fp, #-16]
	mov	r2, r3
	ldr	r3, .L43
	str	r2, [r3, #0]
	.loc 1 329 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L43+4
	str	r2, [r3, #0]
	b	.L37
.L33:
	.loc 1 332 0
	ldr	r3, [fp, #-40]
	cmp	r3, #80
	bne	.L37
	.loc 1 335 0
	ldr	r3, [fp, #-20]
	cmp	r3, #600
	bls	.L38
	.loc 1 337 0
	ldr	r3, [fp, #-20]
	sub	r3, r3, #600
	str	r3, [fp, #-20]
	b	.L39
.L38:
	.loc 1 342 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 343 0
	ldrh	r3, [fp, #-16]
	sub	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-16]	@ movhi
.L39:
	.loc 1 347 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L43+12
	cmp	r2, r3
	bhi	.L40
	.loc 1 350 0
	ldrh	r3, [fp, #-8]
	strh	r3, [fp, #-24]	@ movhi
	.loc 1 351 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1200
	str	r3, [fp, #-28]
	b	.L41
.L40:
	.loc 1 356 0
	ldrh	r3, [fp, #-8]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-24]	@ movhi
	.loc 1 359 0
	mov	r3, #1200
	str	r3, [fp, #-28]
.L41:
	.loc 1 363 0
	sub	r2, fp, #28
	sub	r3, fp, #20
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L42
	.loc 1 366 0
	bl	bad_key_beep
	b	.L37
.L42:
	.loc 1 371 0
	bl	good_key_beep
	.loc 1 372 0
	ldrh	r3, [fp, #-16]
	mov	r2, r3
	ldr	r3, .L43
	str	r2, [r3, #0]
	.loc 1 373 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L43+4
	str	r2, [r3, #0]
.L37:
	.loc 1 379 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 380 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L44:
	.align	2
.L43:
	.word	g_LIGHTS_TEST_editable_stop_date
	.word	g_LIGHTS_TEST_editable_stop_time_in_seconds
	.word	85799
	.word	85199
.LFE5:
	.size	LIGHTS_TEST_process_manual_stop_date_and_time, .-LIGHTS_TEST_process_manual_stop_date_and_time
	.section	.text.FDTO_LIGHTS_TEST_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_LIGHTS_TEST_draw_screen
	.type	FDTO_LIGHTS_TEST_draw_screen, %function
FDTO_LIGHTS_TEST_draw_screen:
.LFB6:
	.loc 1 384 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI17:
	add	fp, sp, #4
.LCFI18:
	sub	sp, sp, #8
.LCFI19:
	str	r0, [fp, #-12]
	.loc 1 387 0
	ldr	r3, .L48
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L48+4
	ldr	r3, .L48+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 390 0
	ldr	r3, .L48+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L48+4
	ldr	r3, .L48+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 392 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L46
	.loc 1 394 0
	bl	nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars
	.loc 1 397 0
	bl	nm_LIGHTS_TEST_load_editable_stop_date_and_time
	.loc 1 399 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L47
.L46:
	.loc 1 404 0
	ldr	r3, .L48+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L47:
	.loc 1 408 0
	ldr	r3, .L48+24
	ldr	r2, [r3, #0]
	ldr	r3, .L48+28
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_light_struct_into_guivars
	.loc 1 411 0
	ldr	r3, .L48+24
	ldr	r2, [r3, #0]
	ldr	r3, .L48+28
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	mov	r1, #0
	bl	LIGHTS_populate_group_name
	.loc 1 414 0
	bl	nm_LIGHTS_TEST_update_stop_date_and_time_panels
	.loc 1 416 0
	ldr	r3, .L48+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 418 0
	ldr	r3, .L48
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 420 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #32
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 421 0
	bl	GuiLib_Refresh
	.loc 1 422 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L49:
	.align	2
.L48:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	387
	.word	irri_lights_recursive_MUTEX
	.word	390
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
.LFE6:
	.size	FDTO_LIGHTS_TEST_draw_screen, .-FDTO_LIGHTS_TEST_draw_screen
	.section	.text.FDTO_LIGHTS_TEST_update_screen,"ax",%progbits
	.align	2
	.global	FDTO_LIGHTS_TEST_update_screen
	.type	FDTO_LIGHTS_TEST_update_screen, %function
FDTO_LIGHTS_TEST_update_screen:
.LFB7:
	.loc 1 426 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI20:
	add	fp, sp, #4
.LCFI21:
	sub	sp, sp, #16
.LCFI22:
	.loc 1 437 0
	ldr	r3, .L55
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 439 0
	ldr	r3, .L55+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 444 0
	ldr	r3, .L55+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L55+12
	mov	r3, #444
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 447 0
	ldr	r3, .L55+16
	ldr	r2, [r3, #0]
	ldr	r3, .L55+20
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	bl	IRRI_LIGHTS_light_is_energized
	mov	r3, r0
	cmp	r3, #1
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, .L55
	str	r2, [r3, #0]
	ldr	r3, .L55
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L51
	.loc 1 450 0
	ldr	r3, .L55+16
	ldr	r2, [r3, #0]
	ldr	r3, .L55+20
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	sub	r2, fp, #20
	mov	r0, r2
	mov	r1, r3
	bl	IRRI_LIGHTS_get_light_stop_date_and_time
	.loc 1 452 0
	ldrh	r3, [fp, #-16]
	mov	r2, r3
	ldr	r3, .L55+24
	str	r2, [r3, #0]
	.loc 1 453 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L55+28
	str	r2, [r3, #0]
	.loc 1 456 0
	bl	nm_LIGHTS_TEST_update_stop_date_and_time_panels
.L51:
	.loc 1 460 0
	ldr	r3, .L55+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 462 0
	ldr	r3, .L55
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	bne	.L52
	.loc 1 462 0 is_stmt 0 discriminator 1
	ldr	r3, .L55+4
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	cmp	r2, r3
	beq	.L53
.L52:
	.loc 1 466 0 is_stmt 1
	mov	r0, #0
	bl	Redraw_Screen
	b	.L50
.L53:
	.loc 1 470 0
	bl	Refresh_Screen
.L50:
	.loc 1 472 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L56:
	.align	2
.L55:
	.word	GuiVar_LightIsEnergized
	.word	GuiVar_LightsStopTimeHasBeenEdited
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	g_LIGHTS_TEST_irri_list_stop_date
	.word	g_LIGHTS_TEST_irri_list_stop_time_in_seconds
.LFE7:
	.size	FDTO_LIGHTS_TEST_update_screen, .-FDTO_LIGHTS_TEST_update_screen
	.section	.text.LIGHTS_TEST_process_screen,"ax",%progbits
	.align	2
	.global	LIGHTS_TEST_process_screen
	.type	LIGHTS_TEST_process_screen, %function
LIGHTS_TEST_process_screen:
.LFB8:
	.loc 1 476 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI23:
	add	fp, sp, #4
.LCFI24:
	sub	sp, sp, #8
.LCFI25:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 477 0
	ldr	r3, [fp, #-12]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L58
.L67:
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L64
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L64
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L65
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L66
	.word	.L58
	.word	.L58
	.word	.L58
	.word	.L66
.L61:
	.loc 1 480 0
	ldr	r3, .L95
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #2
	beq	.L69
	cmp	r3, #3
	beq	.L70
	b	.L91
.L69:
	.loc 1 484 0
	bl	COMM_MNGR_network_is_available_for_normal_use
	mov	r3, r0
	cmp	r3, #0
	bne	.L71
	.loc 1 486 0
	bl	bad_key_beep
	.loc 1 489 0
	mov	r0, #592
	bl	DIALOG_draw_ok_dialog
	.loc 1 513 0
	b	.L75
.L71:
	.loc 1 493 0
	ldr	r3, .L95+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L73
	.loc 1 495 0
	ldr	r3, .L95+8
	ldr	r2, [r3, #0]
	ldr	r3, .L95+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	bl	LIGHTS_TEST_turn_light_on_or_off
	.loc 1 513 0
	b	.L75
.L73:
	.loc 1 501 0
	bl	FLOWSENSE_get_controller_index
	mov	r2, r0
	ldr	r3, .L95+8
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L74
	.loc 1 501 0 is_stmt 0 discriminator 1
	ldr	r3, .L95+16
	ldr	r3, [r3, #112]
	cmp	r3, #1
	bne	.L74
	.loc 1 503 0 is_stmt 1
	bl	bad_key_beep
	.loc 1 505 0
	ldr	r0, .L95+20
	bl	DIALOG_draw_ok_dialog
	.loc 1 513 0
	b	.L75
.L74:
	.loc 1 509 0
	ldr	r3, .L95+8
	ldr	r2, [r3, #0]
	ldr	r3, .L95+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	mov	r2, #1
	bl	LIGHTS_TEST_turn_light_on_or_off
	.loc 1 513 0
	b	.L75
.L70:
	.loc 1 520 0
	ldr	r3, .L95+8
	ldr	r2, [r3, #0]
	ldr	r3, .L95+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	mov	r2, #1
	bl	LIGHTS_TEST_turn_light_on_or_off
	.loc 1 521 0
	b	.L75
.L91:
	.loc 1 524 0
	bl	bad_key_beep
.L75:
	.loc 1 527 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 528 0
	b	.L57
.L64:
	.loc 1 532 0
	bl	good_key_beep
	.loc 1 534 0
	ldr	r3, .L95+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L95+28
	ldr	r3, .L95+32
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 537 0
	ldr	r3, .L95+36
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L95+28
	ldr	r3, .L95+40
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 539 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	bne	.L77
	.loc 1 541 0
	ldr	r0, .L95+8
	ldr	r1, .L95+12
	bl	nm_LIGHTS_get_next_available_light
	b	.L78
.L77:
	.loc 1 545 0
	ldr	r0, .L95+8
	ldr	r1, .L95+12
	bl	nm_LIGHTS_get_prev_available_light
.L78:
	.loc 1 549 0
	ldr	r3, .L95+8
	ldr	r2, [r3, #0]
	ldr	r3, .L95+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_light_struct_into_guivars
	.loc 1 552 0
	bl	nm_LIGHTS_TEST_load_editable_stop_date_and_time
	.loc 1 554 0
	ldr	r3, .L95+36
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 556 0
	ldr	r3, .L95+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 558 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 559 0
	b	.L57
.L66:
	.loc 1 563 0
	ldr	r3, .L95
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	beq	.L80
	cmp	r3, #1
	beq	.L81
	b	.L92
.L80:
	.loc 1 566 0
	bl	good_key_beep
	.loc 1 568 0
	ldr	r3, .L95+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L95+28
	mov	r3, #568
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 570 0
	ldr	r3, [fp, #-12]
	cmp	r3, #84
	bne	.L82
	.loc 1 572 0
	ldr	r0, .L95+8
	ldr	r1, .L95+12
	bl	nm_LIGHTS_get_next_available_light
	b	.L83
.L82:
	.loc 1 576 0
	ldr	r0, .L95+8
	ldr	r1, .L95+12
	bl	nm_LIGHTS_get_prev_available_light
.L83:
	.loc 1 580 0
	ldr	r3, .L95+8
	ldr	r2, [r3, #0]
	ldr	r3, .L95+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_light_struct_into_guivars
	.loc 1 583 0
	bl	nm_LIGHTS_TEST_load_editable_stop_date_and_time
	.loc 1 585 0
	ldr	r3, .L95+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 587 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 588 0
	b	.L84
.L81:
	.loc 1 591 0
	ldr	r3, [fp, #-12]
	mov	r0, r3
	bl	LIGHTS_TEST_process_manual_stop_date_and_time
	.loc 1 592 0
	b	.L84
.L92:
	.loc 1 595 0
	bl	bad_key_beep
	.loc 1 597 0
	b	.L57
.L84:
	b	.L57
.L63:
	.loc 1 600 0
	ldr	r3, .L95
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #3
	bne	.L93
.L86:
	.loc 1 603 0
	mov	r0, #1
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 604 0
	mov	r0, r0	@ nop
	.loc 1 609 0
	b	.L57
.L93:
	.loc 1 607 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 609 0
	b	.L57
.L59:
	.loc 1 612 0
	ldr	r3, .L95
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #2
	bne	.L94
.L89:
	.loc 1 615 0
	bl	bad_key_beep
	.loc 1 616 0
	mov	r0, r0	@ nop
	.loc 1 621 0
	b	.L57
.L94:
	.loc 1 619 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 621 0
	b	.L57
.L60:
	.loc 1 624 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 625 0
	b	.L57
.L62:
	.loc 1 628 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 629 0
	b	.L57
.L65:
	.loc 1 632 0
	ldr	r3, .L95+44
	ldr	r2, [r3, #0]
	ldr	r0, .L95+48
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L95+52
	str	r2, [r3, #0]
.L58:
	.loc 1 638 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L57:
	.loc 1 641 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L96:
	.align	2
.L95:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_LightIsEnergized
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	tpmicro_comm
	.word	605
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	534
	.word	irri_lights_recursive_MUTEX
	.word	537
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE8:
	.size	LIGHTS_TEST_process_screen, .-LIGHTS_TEST_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI14-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI17-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI20-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI23-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_tpmicro_common.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/tpmicro_comm.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x9d3
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF130
	.byte	0x1
	.4byte	.LASF131
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x2
	.byte	0x3a
	.4byte	0x53
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF5
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x2
	.byte	0x4c
	.4byte	0x6c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x2
	.byte	0x55
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x2
	.byte	0x5e
	.4byte	0x90
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x2
	.byte	0x99
	.4byte	0x90
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x2
	.byte	0x9d
	.4byte	0x90
	.uleb128 0x5
	.byte	0x4
	.4byte	0xba
	.uleb128 0x6
	.4byte	0xc1
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x4
	.byte	0x57
	.4byte	0xc1
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x5
	.byte	0x4c
	.4byte	0xce
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x6
	.byte	0x65
	.4byte	0xc1
	.uleb128 0x9
	.4byte	0x53
	.4byte	0xff
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x7
	.byte	0x7c
	.4byte	0x124
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x7
	.byte	0x7e
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x7
	.byte	0x80
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF22
	.byte	0x7
	.byte	0x82
	.4byte	0xff
	.uleb128 0xb
	.byte	0x6
	.byte	0x8
	.byte	0x22
	.4byte	0x150
	.uleb128 0xd
	.ascii	"T\000"
	.byte	0x8
	.byte	0x24
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"D\000"
	.byte	0x8
	.byte	0x26
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF23
	.byte	0x8
	.byte	0x28
	.4byte	0x12f
	.uleb128 0x9
	.4byte	0x85
	.4byte	0x16b
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x9
	.4byte	0x41
	.4byte	0x17b
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0x9
	.4byte	0x41
	.4byte	0x18b
	.uleb128 0xa
	.4byte	0x25
	.byte	0x7
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0x9
	.2byte	0x235
	.4byte	0x1b9
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x9
	.2byte	0x237
	.4byte	0xa9
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF25
	.byte	0x9
	.2byte	0x239
	.4byte	0xa9
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0x9
	.2byte	0x231
	.4byte	0x1d4
	.uleb128 0x11
	.4byte	.LASF132
	.byte	0x9
	.2byte	0x233
	.4byte	0x85
	.uleb128 0x12
	.4byte	0x18b
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0x9
	.2byte	0x22f
	.4byte	0x1e6
	.uleb128 0x13
	.4byte	0x1b9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x14
	.4byte	.LASF26
	.byte	0x9
	.2byte	0x23e
	.4byte	0x1d4
	.uleb128 0xe
	.byte	0x38
	.byte	0x9
	.2byte	0x241
	.4byte	0x283
	.uleb128 0x15
	.4byte	.LASF27
	.byte	0x9
	.2byte	0x245
	.4byte	0x283
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.ascii	"poc\000"
	.byte	0x9
	.2byte	0x247
	.4byte	0x1e6
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x15
	.4byte	.LASF28
	.byte	0x9
	.2byte	0x249
	.4byte	0x1e6
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x15
	.4byte	.LASF29
	.byte	0x9
	.2byte	0x24f
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x15
	.4byte	.LASF30
	.byte	0x9
	.2byte	0x250
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x15
	.4byte	.LASF31
	.byte	0x9
	.2byte	0x252
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF32
	.byte	0x9
	.2byte	0x253
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x15
	.4byte	.LASF33
	.byte	0x9
	.2byte	0x254
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x15
	.4byte	.LASF34
	.byte	0x9
	.2byte	0x256
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x9
	.4byte	0x1e6
	.4byte	0x293
	.uleb128 0xa
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x14
	.4byte	.LASF35
	.byte	0x9
	.2byte	0x258
	.4byte	0x1f2
	.uleb128 0xb
	.byte	0x8
	.byte	0xa
	.byte	0x14
	.4byte	0x2c4
	.uleb128 0xc
	.4byte	.LASF36
	.byte	0xa
	.byte	0x17
	.4byte	0x2c4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF37
	.byte	0xa
	.byte	0x1a
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x48
	.uleb128 0x4
	.4byte	.LASF38
	.byte	0xa
	.byte	0x1c
	.4byte	0x29f
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF39
	.uleb128 0x9
	.4byte	0x85
	.4byte	0x2ec
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF40
	.uleb128 0xb
	.byte	0x24
	.byte	0xb
	.byte	0x78
	.4byte	0x37a
	.uleb128 0xc
	.4byte	.LASF41
	.byte	0xb
	.byte	0x7b
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF42
	.byte	0xb
	.byte	0x83
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF43
	.byte	0xb
	.byte	0x86
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF44
	.byte	0xb
	.byte	0x88
	.4byte	0x38b
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF45
	.byte	0xb
	.byte	0x8d
	.4byte	0x39d
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF46
	.byte	0xb
	.byte	0x92
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF47
	.byte	0xb
	.byte	0x96
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF48
	.byte	0xb
	.byte	0x9a
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF49
	.byte	0xb
	.byte	0x9c
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	0x386
	.uleb128 0x18
	.4byte	0x386
	.byte	0
	.uleb128 0x19
	.4byte	0x73
	.uleb128 0x5
	.byte	0x4
	.4byte	0x37a
	.uleb128 0x17
	.byte	0x1
	.4byte	0x39d
	.uleb128 0x18
	.4byte	0x124
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x391
	.uleb128 0x4
	.4byte	.LASF50
	.byte	0xb
	.byte	0x9e
	.4byte	0x2f3
	.uleb128 0xb
	.byte	0xac
	.byte	0xc
	.byte	0x33
	.4byte	0x54d
	.uleb128 0xc
	.4byte	.LASF51
	.byte	0xc
	.byte	0x3b
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF52
	.byte	0xc
	.byte	0x41
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF53
	.byte	0xc
	.byte	0x46
	.4byte	0xe4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF54
	.byte	0xc
	.byte	0x4e
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF55
	.byte	0xc
	.byte	0x52
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF56
	.byte	0xc
	.byte	0x56
	.4byte	0x2ca
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF57
	.byte	0xc
	.byte	0x5a
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF58
	.byte	0xc
	.byte	0x5e
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xc
	.4byte	.LASF59
	.byte	0xc
	.byte	0x60
	.4byte	0x2c4
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xc
	.4byte	.LASF60
	.byte	0xc
	.byte	0x64
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.4byte	.LASF61
	.byte	0xc
	.byte	0x66
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xc
	.4byte	.LASF62
	.byte	0xc
	.byte	0x68
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xc
	.4byte	.LASF63
	.byte	0xc
	.byte	0x6a
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xc
	.4byte	.LASF64
	.byte	0xc
	.byte	0x6c
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0xc
	.4byte	.LASF65
	.byte	0xc
	.byte	0x77
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xc
	.4byte	.LASF66
	.byte	0xc
	.byte	0x7d
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xc
	.4byte	.LASF67
	.byte	0xc
	.byte	0x7f
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xc
	.4byte	.LASF68
	.byte	0xc
	.byte	0x81
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xc
	.4byte	.LASF69
	.byte	0xc
	.byte	0x83
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0xc
	.4byte	.LASF70
	.byte	0xc
	.byte	0x87
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xc
	.4byte	.LASF71
	.byte	0xc
	.byte	0x89
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xc
	.4byte	.LASF72
	.byte	0xc
	.byte	0x90
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xc
	.4byte	.LASF73
	.byte	0xc
	.byte	0x97
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0xc
	.4byte	.LASF74
	.byte	0xc
	.byte	0x9d
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0xc
	.4byte	.LASF75
	.byte	0xc
	.byte	0xa8
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0xc
	.4byte	.LASF76
	.byte	0xc
	.byte	0xaa
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0xc
	.4byte	.LASF77
	.byte	0xc
	.byte	0xac
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0xc
	.4byte	.LASF78
	.byte	0xc
	.byte	0xb4
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0xc
	.4byte	.LASF79
	.byte	0xc
	.byte	0xbe
	.4byte	0x293
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.byte	0
	.uleb128 0x4
	.4byte	.LASF80
	.byte	0xc
	.byte	0xc0
	.4byte	0x3ae
	.uleb128 0x1a
	.4byte	.LASF85
	.byte	0x1
	.byte	0x45
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x5b7
	.uleb128 0x1b
	.4byte	.LASF81
	.byte	0x1
	.byte	0x45
	.4byte	0x5b7
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1b
	.4byte	.LASF82
	.byte	0x1
	.byte	0x45
	.4byte	0x5b7
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1b
	.4byte	.LASF83
	.byte	0x1
	.byte	0x45
	.4byte	0x5bc
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1c
	.ascii	"ldt\000"
	.byte	0x1
	.byte	0x47
	.4byte	0x150
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1d
	.4byte	.LASF84
	.byte	0x1
	.byte	0x47
	.4byte	0x150
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x19
	.4byte	0x85
	.uleb128 0x19
	.4byte	0x9e
	.uleb128 0x1a
	.4byte	.LASF86
	.byte	0x1
	.byte	0x73
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x604
	.uleb128 0x1d
	.4byte	.LASF87
	.byte	0x1
	.byte	0x75
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1c
	.ascii	"ldt\000"
	.byte	0x1
	.byte	0x77
	.4byte	0x150
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1d
	.4byte	.LASF88
	.byte	0x1
	.byte	0x79
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF89
	.byte	0x1
	.byte	0x9f
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x62b
	.uleb128 0x1d
	.4byte	.LASF90
	.byte	0x1
	.byte	0xa4
	.4byte	0x150
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF133
	.byte	0x1
	.byte	0xb9
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x1a
	.4byte	.LASF91
	.byte	0x1
	.byte	0xd3
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x675
	.uleb128 0x1d
	.4byte	.LASF92
	.byte	0x1
	.byte	0xd5
	.4byte	0x16b
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x1d
	.4byte	.LASF93
	.byte	0x1
	.byte	0xd7
	.4byte	0x17b
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x10e
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x6da
	.uleb128 0x20
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x10e
	.4byte	0x5b7
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x21
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x110
	.4byte	0x150
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x22
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x110
	.4byte	0x150
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x22
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x110
	.4byte	0x150
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x22
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x110
	.4byte	0x150
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x17f
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x713
	.uleb128 0x20
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x17f
	.4byte	0x5bc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x22
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x181
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x1a9
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x75b
	.uleb128 0x22
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x1ab
	.4byte	0x150
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x22
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x1ad
	.4byte	0x9e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x22
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x1af
	.4byte	0x9e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x1db
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x785
	.uleb128 0x20
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x1db
	.4byte	0x785
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x19
	.4byte	0x124
	.uleb128 0x9
	.4byte	0x41
	.4byte	0x79a
	.uleb128 0xa
	.4byte	0x25
	.byte	0x20
	.byte	0
	.uleb128 0x24
	.4byte	.LASF107
	.byte	0xd
	.2byte	0x283
	.4byte	0x78a
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF108
	.byte	0xd
	.2byte	0x284
	.4byte	0x78a
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF109
	.byte	0xd
	.2byte	0x285
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF110
	.byte	0xd
	.2byte	0x289
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF111
	.byte	0xd
	.2byte	0x291
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF112
	.byte	0xd
	.2byte	0x29b
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF113
	.byte	0xd
	.2byte	0x2ec
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF114
	.byte	0xe
	.2byte	0x127
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF115
	.byte	0xf
	.byte	0x30
	.4byte	0x81b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x19
	.4byte	0xef
	.uleb128 0x1d
	.4byte	.LASF116
	.byte	0xf
	.byte	0x34
	.4byte	0x831
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x19
	.4byte	0xef
	.uleb128 0x1d
	.4byte	.LASF117
	.byte	0xf
	.byte	0x36
	.4byte	0x847
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x19
	.4byte	0xef
	.uleb128 0x1d
	.4byte	.LASF118
	.byte	0xf
	.byte	0x38
	.4byte	0x85d
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x19
	.4byte	0xef
	.uleb128 0x1d
	.4byte	.LASF119
	.byte	0x10
	.byte	0x33
	.4byte	0x873
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x19
	.4byte	0x15b
	.uleb128 0x1d
	.4byte	.LASF120
	.byte	0x10
	.byte	0x3f
	.4byte	0x889
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x19
	.4byte	0x2dc
	.uleb128 0x25
	.4byte	.LASF121
	.byte	0x11
	.byte	0xfe
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF122
	.byte	0x11
	.2byte	0x104
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x3a3
	.4byte	0x8b9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x31
	.byte	0
	.uleb128 0x25
	.4byte	.LASF123
	.byte	0xb
	.byte	0xac
	.4byte	0x8a9
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF124
	.byte	0xb
	.byte	0xae
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF125
	.byte	0xc
	.byte	0xc7
	.4byte	0x54d
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF126
	.byte	0x1
	.byte	0x3c
	.4byte	0x85
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_TEST_irri_list_stop_date
	.uleb128 0x1d
	.4byte	.LASF127
	.byte	0x1
	.byte	0x3d
	.4byte	0x85
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_TEST_irri_list_stop_time_in_seconds
	.uleb128 0x1d
	.4byte	.LASF128
	.byte	0x1
	.byte	0x3e
	.4byte	0x85
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_TEST_editable_stop_date
	.uleb128 0x1d
	.4byte	.LASF129
	.byte	0x1
	.byte	0x3f
	.4byte	0x85
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_TEST_editable_stop_time_in_seconds
	.uleb128 0x24
	.4byte	.LASF107
	.byte	0xd
	.2byte	0x283
	.4byte	0x78a
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF108
	.byte	0xd
	.2byte	0x284
	.4byte	0x78a
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF109
	.byte	0xd
	.2byte	0x285
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF110
	.byte	0xd
	.2byte	0x289
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF111
	.byte	0xd
	.2byte	0x291
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF112
	.byte	0xd
	.2byte	0x29b
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF113
	.byte	0xd
	.2byte	0x2ec
	.4byte	0x90
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF114
	.byte	0xe
	.2byte	0x127
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF121
	.byte	0x11
	.byte	0xfe
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF122
	.byte	0x11
	.2byte	0x104
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF123
	.byte	0xb
	.byte	0xac
	.4byte	0x8a9
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF124
	.byte	0xb
	.byte	0xae
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF125
	.byte	0xc
	.byte	0xc7
	.4byte	0x54d
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI18
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI21
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI24
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF67:
	.ascii	"tpmicro_executing_code_time\000"
.LASF84:
	.ascii	"temp_dt\000"
.LASF71:
	.ascii	"file_system_code_time\000"
.LASF57:
	.ascii	"isp_after_prepare_state\000"
.LASF78:
	.ascii	"fuse_blown\000"
.LASF112:
	.ascii	"GuiVar_LightsStopTimeHasBeenEdited\000"
.LASF88:
	.ascii	"rounded_time\000"
.LASF53:
	.ascii	"timer_message_rate\000"
.LASF42:
	.ascii	"_02_menu\000"
.LASF117:
	.ascii	"GuiFont_DecimalChar\000"
.LASF125:
	.ascii	"tpmicro_comm\000"
.LASF13:
	.ascii	"long long unsigned int\000"
.LASF14:
	.ascii	"BOOL_32\000"
.LASF34:
	.ascii	"two_wire_terminal_present\000"
.LASF122:
	.ascii	"irri_lights_recursive_MUTEX\000"
.LASF94:
	.ascii	"LIGHTS_TEST_process_manual_stop_date_and_time\000"
.LASF43:
	.ascii	"_03_structure_to_draw\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF20:
	.ascii	"keycode\000"
.LASF66:
	.ascii	"tpmicro_executing_code_date\000"
.LASF23:
	.ascii	"DATE_TIME\000"
.LASF2:
	.ascii	"long long int\000"
.LASF65:
	.ascii	"isp_sync_fault\000"
.LASF100:
	.ascii	"FDTO_LIGHTS_TEST_draw_screen\000"
.LASF70:
	.ascii	"file_system_code_date\000"
.LASF51:
	.ascii	"up_and_running\000"
.LASF103:
	.ascii	"llight_was_energized\000"
.LASF9:
	.ascii	"INT_16\000"
.LASF119:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF127:
	.ascii	"g_LIGHTS_TEST_irri_list_stop_time_in_seconds\000"
.LASF101:
	.ascii	"FDTO_LIGHTS_TEST_update_screen\000"
.LASF1:
	.ascii	"long int\000"
.LASF27:
	.ascii	"stations\000"
.LASF50:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF106:
	.ascii	"pkey_event\000"
.LASF18:
	.ascii	"xSemaphoreHandle\000"
.LASF124:
	.ascii	"screen_history_index\000"
.LASF40:
	.ascii	"double\000"
.LASF118:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF80:
	.ascii	"TPMICRO_COMM_STRUCT\000"
.LASF31:
	.ascii	"dash_m_card_present\000"
.LASF83:
	.ascii	"action\000"
.LASF107:
	.ascii	"GuiVar_LightEditableStop\000"
.LASF110:
	.ascii	"GuiVar_LightsBoxIndex_0\000"
.LASF41:
	.ascii	"_01_command\000"
.LASF64:
	.ascii	"uuencode_first_1K_block_sent\000"
.LASF54:
	.ascii	"number_of_outgoing_since_last_incoming\000"
.LASF113:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF128:
	.ascii	"g_LIGHTS_TEST_editable_stop_date\000"
.LASF24:
	.ascii	"card_present\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF93:
	.ascii	"time_str\000"
.LASF25:
	.ascii	"tb_present\000"
.LASF38:
	.ascii	"DATA_HANDLE\000"
.LASF45:
	.ascii	"key_process_func_ptr\000"
.LASF58:
	.ascii	"isp_where_to_in_flash\000"
.LASF8:
	.ascii	"short unsigned int\000"
.LASF130:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF47:
	.ascii	"_06_u32_argument1\000"
.LASF91:
	.ascii	"nm_LIGHTS_TEST_update_stop_date_and_time_panels\000"
.LASF52:
	.ascii	"in_ISP\000"
.LASF44:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF95:
	.ascii	"pkeycode\000"
.LASF30:
	.ascii	"weather_terminal_present\000"
.LASF17:
	.ascii	"xQueueHandle\000"
.LASF116:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF49:
	.ascii	"_08_screen_to_draw\000"
.LASF109:
	.ascii	"GuiVar_LightIsEnergized\000"
.LASF19:
	.ascii	"xTimerHandle\000"
.LASF76:
	.ascii	"freeze_switch_active\000"
.LASF79:
	.ascii	"wi_holding\000"
.LASF22:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF82:
	.ascii	"output_index_0\000"
.LASF105:
	.ascii	"LIGHTS_TEST_process_screen\000"
.LASF108:
	.ascii	"GuiVar_LightIrriListStop\000"
.LASF90:
	.ascii	"irri_stop_dt\000"
.LASF99:
	.ascii	"lcursor_to_select\000"
.LASF39:
	.ascii	"float\000"
.LASF115:
	.ascii	"GuiFont_LanguageActive\000"
.LASF37:
	.ascii	"dlen\000"
.LASF60:
	.ascii	"uuencode_checksum_line_count_20\000"
.LASF111:
	.ascii	"GuiVar_LightsOutputIndex_0\000"
.LASF29:
	.ascii	"weather_card_present\000"
.LASF97:
	.ascii	"max_dt\000"
.LASF33:
	.ascii	"dash_m_card_type\000"
.LASF4:
	.ascii	"unsigned char\000"
.LASF86:
	.ascii	"LIGHTS_TEST_calculate_default_off_date_and_time\000"
.LASF32:
	.ascii	"dash_m_terminal_present\000"
.LASF10:
	.ascii	"short int\000"
.LASF102:
	.ascii	"irri_list_stop_dt\000"
.LASF16:
	.ascii	"portTickType\000"
.LASF69:
	.ascii	"tpmicro_request_executing_code_date_and_time\000"
.LASF123:
	.ascii	"ScreenHistory\000"
.LASF36:
	.ascii	"dptr\000"
.LASF133:
	.ascii	"nm_LIGHTS_TEST_load_editable_stop_date_and_time\000"
.LASF98:
	.ascii	"pcomplete_redraw\000"
.LASF96:
	.ascii	"min_dt\000"
.LASF3:
	.ascii	"char\000"
.LASF129:
	.ascii	"g_LIGHTS_TEST_editable_stop_time_in_seconds\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF87:
	.ascii	"seconds_left_today\000"
.LASF35:
	.ascii	"WHATS_INSTALLED_STRUCT\000"
.LASF74:
	.ascii	"wind_mph\000"
.LASF121:
	.ascii	"list_lights_recursive_MUTEX\000"
.LASF59:
	.ascii	"isp_where_from_in_file\000"
.LASF75:
	.ascii	"rain_switch_active\000"
.LASF68:
	.ascii	"tpmicro_executing_code_date_and_time_valid\000"
.LASF21:
	.ascii	"repeats\000"
.LASF62:
	.ascii	"uuencode_bytes_left_in_file_to_send\000"
.LASF72:
	.ascii	"file_system_code_date_and_time_valid\000"
.LASF120:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF89:
	.ascii	"nm_LIGHTS_TEST_load_irri_list_stop_date_and_time\000"
.LASF126:
	.ascii	"g_LIGHTS_TEST_irri_list_stop_date\000"
.LASF26:
	.ascii	"I2C_CARD_DETAILS_STRUCT\000"
.LASF114:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF55:
	.ascii	"isp_state\000"
.LASF104:
	.ascii	"llight_was_edited\000"
.LASF7:
	.ascii	"UNS_16\000"
.LASF28:
	.ascii	"lights\000"
.LASF6:
	.ascii	"UNS_8\000"
.LASF56:
	.ascii	"isp_tpmicro_file\000"
.LASF85:
	.ascii	"LIGHTS_TEST_turn_light_on_or_off\000"
.LASF48:
	.ascii	"_07_u32_argument2\000"
.LASF46:
	.ascii	"_04_func_ptr\000"
.LASF73:
	.ascii	"code_version_test_pending\000"
.LASF92:
	.ascii	"date_str\000"
.LASF132:
	.ascii	"sizer\000"
.LASF81:
	.ascii	"box_index_0\000"
.LASF5:
	.ascii	"signed char\000"
.LASF15:
	.ascii	"BITFIELD_BOOL\000"
.LASF63:
	.ascii	"uuencode_bytes_in_this_1K_block_sent\000"
.LASF61:
	.ascii	"uuencode_running_checksum_20\000"
.LASF131:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_test_lights.c\000"
.LASF77:
	.ascii	"wind_paused\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
