	.file	"list.c"
	.text
.Ltext0:
	.section	.text.vListInitialise,"ax",%progbits
	.align	2
	.global	vListInitialise
	.type	vListInitialise, %function
vListInitialise:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/list.c"
	.loc 1 77 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-4]
	.loc 1 81 0
	ldr	r3, [fp, #-4]
	add	r2, r3, #8
	ldr	r3, [fp, #-4]
	str	r2, [r3, #4]
	.loc 1 85 0
	ldr	r3, [fp, #-4]
	mvn	r2, #0
	str	r2, [r3, #8]
	.loc 1 89 0
	ldr	r3, [fp, #-4]
	add	r2, r3, #8
	ldr	r3, [fp, #-4]
	str	r2, [r3, #12]
	.loc 1 90 0
	ldr	r3, [fp, #-4]
	add	r2, r3, #8
	ldr	r3, [fp, #-4]
	str	r2, [r3, #16]
	.loc 1 92 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 93 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE0:
	.size	vListInitialise, .-vListInitialise
	.section	.text.vListInitialiseItem,"ax",%progbits
	.align	2
	.global	vListInitialiseItem
	.type	vListInitialiseItem, %function
vListInitialiseItem:
.LFB1:
	.loc 1 97 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-4]
	.loc 1 99 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #16]
	.loc 1 100 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE1:
	.size	vListInitialiseItem, .-vListInitialiseItem
	.section	.text.vListInsertEnd,"ax",%progbits
	.align	2
	.global	vListInsertEnd
	.type	vListInsertEnd, %function
vListInsertEnd:
.LFB2:
	.loc 1 104 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #12
.LCFI8:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 111 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-4]
	.loc 1 113 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #4]
	.loc 1 114 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #8]
	.loc 1 115 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #8]
	.loc 1 116 0
	ldr	r3, [fp, #-4]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #4]
	.loc 1 117 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #4]
	.loc 1 120 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #16]
	.loc 1 122 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 123 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE2:
	.size	vListInsertEnd, .-vListInsertEnd
	.section	.text.vListInsert,"ax",%progbits
	.align	2
	.global	vListInsert
	.type	vListInsert, %function
vListInsert:
.LFB3:
	.loc 1 127 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #16
.LCFI11:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 132 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 141 0
	ldr	r3, [fp, #-8]
	cmn	r3, #1
	bne	.L5
	.loc 1 143 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #16]
	str	r3, [fp, #-4]
	b	.L6
.L5:
	.loc 1 163 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #8
	str	r3, [fp, #-4]
	b	.L7
.L8:
	.loc 1 163 0 is_stmt 0 discriminator 2
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-4]
.L7:
	.loc 1 163 0 discriminator 1
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bls	.L8
.L6:
	.loc 1 170 0 is_stmt 1
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #4]
	.loc 1 171 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #8]
	.loc 1 172 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-4]
	str	r2, [r3, #8]
	.loc 1 173 0
	ldr	r3, [fp, #-4]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #4]
	.loc 1 177 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #16]
	.loc 1 179 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 180 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE3:
	.size	vListInsert, .-vListInsert
	.section	.text.vListRemove,"ax",%progbits
	.align	2
	.global	vListRemove
	.type	vListRemove, %function
vListRemove:
.LFB4:
	.loc 1 184 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI12:
	add	fp, sp, #0
.LCFI13:
	sub	sp, sp, #8
.LCFI14:
	str	r0, [fp, #-8]
	.loc 1 187 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #8]
	str	r2, [r3, #8]
	.loc 1 188 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #4]
	str	r2, [r3, #4]
	.loc 1 192 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #16]
	str	r3, [fp, #-4]
	.loc 1 195 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L10
	.loc 1 197 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-4]
	str	r2, [r3, #4]
.L10:
	.loc 1 200 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #16]
	.loc 1 201 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 202 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE4:
	.size	vListRemove, .-vListRemove
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/list.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x294
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF33
	.byte	0x1
	.4byte	.LASF34
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x4
	.byte	0x4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x5
	.4byte	.LASF13
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x14
	.byte	0x2
	.byte	0x69
	.4byte	0xc4
	.uleb128 0x7
	.4byte	.LASF8
	.byte	0x2
	.byte	0x6b
	.4byte	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF9
	.byte	0x2
	.byte	0x6c
	.4byte	0xc4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF10
	.byte	0x2
	.byte	0x6d
	.4byte	0xc4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF11
	.byte	0x2
	.byte	0x6e
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF12
	.byte	0x2
	.byte	0x6f
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0xca
	.uleb128 0x9
	.4byte	0x71
	.uleb128 0x5
	.4byte	.LASF14
	.byte	0x2
	.byte	0x71
	.4byte	0x71
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0xc
	.byte	0x2
	.byte	0x73
	.4byte	0x111
	.uleb128 0x7
	.4byte	.LASF8
	.byte	0x2
	.byte	0x75
	.4byte	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF9
	.byte	0x2
	.byte	0x76
	.4byte	0xc4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF10
	.byte	0x2
	.byte	0x77
	.4byte	0xc4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x5
	.4byte	.LASF17
	.byte	0x2
	.byte	0x79
	.4byte	0xda
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x14
	.byte	0x2
	.byte	0x7e
	.4byte	0x153
	.uleb128 0x7
	.4byte	.LASF19
	.byte	0x2
	.byte	0x80
	.4byte	0x153
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF20
	.byte	0x2
	.byte	0x81
	.4byte	0x158
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF21
	.byte	0x2
	.byte	0x82
	.4byte	0x163
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x9
	.4byte	0x25
	.uleb128 0x8
	.byte	0x4
	.4byte	0x15e
	.uleb128 0x9
	.4byte	0xcf
	.uleb128 0x9
	.4byte	0x111
	.uleb128 0x5
	.4byte	.LASF22
	.byte	0x2
	.byte	0x83
	.4byte	0x11c
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.byte	0x4c
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x19b
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x1
	.byte	0x4c
	.4byte	0x19b
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x168
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.byte	0x60
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x1c9
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x1
	.byte	0x60
	.4byte	0x1c9
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0xcf
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.byte	0x67
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x213
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x1
	.byte	0x67
	.4byte	0x19b
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x1
	.byte	0x67
	.4byte	0x1c9
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x1
	.byte	0x69
	.4byte	0x158
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.byte	0x7e
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x265
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x1
	.byte	0x7e
	.4byte	0x19b
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x1
	.byte	0x7e
	.4byte	0x1c9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x1
	.byte	0x80
	.4byte	0x158
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x1
	.byte	0x81
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.byte	0xb7
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x1
	.byte	0xb7
	.4byte	0x1c9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x1
	.byte	0xb9
	.4byte	0x19b
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF12:
	.ascii	"pvContainer\000"
.LASF9:
	.ascii	"pxNext\000"
.LASF13:
	.ascii	"portTickType\000"
.LASF8:
	.ascii	"xItemValue\000"
.LASF22:
	.ascii	"xList\000"
.LASF21:
	.ascii	"xListEnd\000"
.LASF11:
	.ascii	"pvOwner\000"
.LASF15:
	.ascii	"xLIST_ITEM\000"
.LASF34:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/list.c\000"
.LASF6:
	.ascii	"unsigned char\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF32:
	.ascii	"pxItemToRemove\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF24:
	.ascii	"vListInitialiseItem\000"
.LASF17:
	.ascii	"xMiniListItem\000"
.LASF19:
	.ascii	"uxNumberOfItems\000"
.LASF27:
	.ascii	"vListInsertEnd\000"
.LASF23:
	.ascii	"vListInitialise\000"
.LASF31:
	.ascii	"xValueOfInsertion\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF26:
	.ascii	"pxItem\000"
.LASF2:
	.ascii	"long long int\000"
.LASF14:
	.ascii	"xListItem\000"
.LASF35:
	.ascii	"vListRemove\000"
.LASF30:
	.ascii	"pxIterator\000"
.LASF5:
	.ascii	"short int\000"
.LASF20:
	.ascii	"pxIndex\000"
.LASF10:
	.ascii	"pxPrevious\000"
.LASF33:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"long int\000"
.LASF16:
	.ascii	"xMINI_LIST_ITEM\000"
.LASF4:
	.ascii	"signed char\000"
.LASF25:
	.ascii	"pxList\000"
.LASF18:
	.ascii	"xLIST\000"
.LASF28:
	.ascii	"pxNewListItem\000"
.LASF29:
	.ascii	"vListInsert\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
