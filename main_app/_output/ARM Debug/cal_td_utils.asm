	.file	"cal_td_utils.c"
	.text
.Ltext0:
	.section	.text.DMYToDate,"ax",%progbits
	.align	2
	.global	DMYToDate
	.type	DMYToDate, %function
DMYToDate:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.c"
	.loc 1 36 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #16
.LCFI2:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 41 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #1888
	sub	r3, r3, #12
	str	r3, [fp, #-16]
	.loc 1 43 0
	ldr	r3, [fp, #-12]
	cmp	r3, #2
	bls	.L2
	.loc 1 45 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #3
	str	r3, [fp, #-12]
	b	.L3
.L2:
	.loc 1 49 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #9
	str	r3, [fp, #-12]
	.loc 1 51 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #1
	str	r3, [fp, #-16]
.L3:
	.loc 1 54 0
	ldr	r3, [fp, #-16]
	ldr	r2, .L4
	mul	r3, r2, r3
	mov	r1, r3, lsr #2
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r2, r3, asl #4
	add	r3, r3, r2
	add	r2, r3, #2
	ldr	r3, .L4+4
	umull	r0, r3, r2, r3
	mov	r3, r3, lsr #2
	add	r2, r1, r3
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
	add	r3, r3, #58
	str	r3, [fp, #-4]
	.loc 1 56 0
	ldr	r3, [fp, #-4]
	.loc 1 57 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L5:
	.align	2
.L4:
	.word	1461
	.word	-858993459
.LFE0:
	.size	DMYToDate, .-DMYToDate
	.section	.text.DateAndTimeToDTCS,"ax",%progbits
	.align	2
	.global	DateAndTimeToDTCS
	.type	DateAndTimeToDTCS, %function
DateAndTimeToDTCS:
.LFB1:
	.loc 1 62 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #44
.LCFI5:
	str	r0, [fp, #-36]
	str	r1, [fp, #-40]
	str	r2, [fp, #-44]
	.loc 1 66 0
	ldr	r3, [fp, #-36]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-44]
	strh	r2, [r3, #4]	@ movhi
	.loc 1 69 0
	ldr	r3, [fp, #-44]
	ldrh	r3, [r3, #4]
	mov	r0, r3
	sub	r1, fp, #8
	sub	r2, fp, #12
	sub	r3, fp, #16
	sub	ip, fp, #20
	str	ip, [sp, #0]
	bl	DateToDMY
	.loc 1 71 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-44]
	strh	r2, [r3, #8]	@ movhi
	.loc 1 73 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-44]
	strh	r2, [r3, #6]	@ movhi
	.loc 1 75 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-44]
	strh	r2, [r3, #10]	@ movhi
	.loc 1 77 0
	ldr	r3, [fp, #-20]
	and	r2, r3, #255
	ldr	r3, [fp, #-44]
	strb	r2, [r3, #18]
	.loc 1 81 0
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-40]
	str	r2, [r3, #0]
	.loc 1 83 0
	sub	r1, fp, #24
	sub	r2, fp, #28
	sub	r3, fp, #32
	ldr	r0, [fp, #-40]
	bl	TimeToHMS
	.loc 1 85 0
	ldr	r3, [fp, #-24]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-44]
	strh	r2, [r3, #12]	@ movhi
	.loc 1 87 0
	ldr	r3, [fp, #-28]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-44]
	strh	r2, [r3, #14]	@ movhi
	.loc 1 89 0
	ldr	r3, [fp, #-32]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-44]
	strh	r2, [r3, #16]	@ movhi
	.loc 1 90 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	DateAndTimeToDTCS, .-DateAndTimeToDTCS
	.section	.text.HMSToTime,"ax",%progbits
	.align	2
	.global	HMSToTime
	.type	HMSToTime, %function
HMSToTime:
.LFB2:
	.loc 1 100 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #16
.LCFI8:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 103 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L8
	umull	r1, r3, r2, r3
	mov	r1, r3, lsr #4
	mov	r3, r1
	mov	r3, r3, asl #1
	add	r3, r3, r1
	mov	r3, r3, asl #3
	rsb	r3, r3, r2
	str	r3, [fp, #-8]
	.loc 1 105 0
	ldr	r3, [fp, #-8]
	mov	r2, #3600
	mul	r1, r2, r3
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r2, r1, r3
	ldr	r3, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 107 0
	ldr	r3, [fp, #-4]
	ldr	r2, .L8+4
	umull	r1, r2, r3, r2
	mov	r2, r2, lsr #16
	ldr	r1, .L8+8
	mul	r2, r1, r2
	rsb	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 109 0
	ldr	r3, [fp, #-4]
	.loc 1 110 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L9:
	.align	2
.L8:
	.word	-1431655765
	.word	-1037155065
	.word	86400
.LFE2:
	.size	HMSToTime, .-HMSToTime
	.section	.text.DayOfWeek,"ax",%progbits
	.align	2
	.global	DayOfWeek
	.type	DayOfWeek, %function
DayOfWeek:
.LFB3:
	.loc 1 124 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-8]
	.loc 1 127 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #1
	ldr	r3, .L11
	umull	r1, r3, r2, r3
	rsb	r1, r3, r2
	mov	r1, r1, lsr #1
	add	r3, r3, r1
	mov	r1, r3, lsr #2
	mov	r3, r1
	mov	r3, r3, asl #3
	rsb	r3, r1, r3
	rsb	r3, r3, r2
	str	r3, [fp, #-4]
	.loc 1 129 0
	ldr	r3, [fp, #-4]
	.loc 1 130 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L12:
	.align	2
.L11:
	.word	613566757
.LFE3:
	.size	DayOfWeek, .-DayOfWeek
	.section	.text.DateToDMY,"ax",%progbits
	.align	2
	.global	DateToDMY
	.type	DateToDMY, %function
DateToDMY:
.LFB4:
	.loc 1 134 0
	@ args = 4, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #20
.LCFI14:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 137 0
	ldr	r0, [fp, #-12]
	bl	DayOfWeek
	mov	r2, r0
	ldr	r3, [fp, #4]
	str	r2, [r3, #0]
	.loc 1 139 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #2
	sub	r3, r3, #233
	str	r3, [fp, #-8]
	.loc 1 140 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L16
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #7
	ldr	r3, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 142 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L16
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #7
	ldr	r1, .L16+4
	mul	r3, r1, r3
	rsb	r3, r3, r2
	mov	r2, r3, lsr #2
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	add	r3, r3, #2
	str	r3, [fp, #-8]
	.loc 1 144 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L16+8
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #7
	ldr	r3, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 146 0
	ldr	r1, [fp, #-8]
	ldr	r3, .L16+8
	umull	r2, r3, r1, r3
	mov	r2, r3, lsr #7
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r2, r3, asl #4
	add	r3, r3, r2
	rsb	r2, r3, r1
	add	r2, r2, #5
	ldr	r3, .L16+12
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #2
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 148 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	cmp	r3, #9
	bhi	.L14
	.loc 1 150 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	add	r2, r3, #3
	ldr	r3, [fp, #-20]
	str	r2, [r3, #0]
	b	.L15
.L14:
	.loc 1 154 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	sub	r2, r3, #9
	ldr	r3, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 155 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, [fp, #-24]
	str	r2, [r3, #0]
.L15:
	.loc 1 158 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	add	r3, r3, #1888
	add	r3, r3, #12
	ldr	r2, [fp, #-24]
	str	r3, [r2, #0]
	.loc 1 159 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	376287347
	.word	1461
	.word	-701792041
	.word	-858993459
.LFE4:
	.size	DateToDMY, .-DateToDMY
	.section	.text.IsLeapYear,"ax",%progbits
	.align	2
	.global	IsLeapYear
	.type	IsLeapYear, %function
IsLeapYear:
.LFB5:
	.loc 1 163 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI15:
	add	fp, sp, #0
.LCFI16:
	sub	sp, sp, #4
.LCFI17:
	str	r0, [fp, #-4]
	.loc 1 165 0
	ldr	r3, [fp, #-4]
	and	r3, r3, #3
	cmp	r3, #0
	bne	.L19
	.loc 1 165 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-4]
	ldr	r3, .L23
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #5
	mov	r1, #100
	mul	r3, r1, r3
	rsb	r3, r3, r2
	cmp	r3, #0
	bne	.L20
.L19:
	.loc 1 165 0 discriminator 2
	ldr	r2, [fp, #-4]
	ldr	r3, .L23
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #7
	mov	r1, #400
	mul	r3, r1, r3
	rsb	r3, r3, r2
	cmp	r3, #0
	bne	.L21
.L20:
	.loc 1 165 0 discriminator 1
	mov	r3, #1
	b	.L22
.L21:
	mov	r3, #0
.L22:
	.loc 1 166 0 is_stmt 1 discriminator 3
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L24:
	.align	2
.L23:
	.word	1374389535
.LFE5:
	.size	IsLeapYear, .-IsLeapYear
	.section	.text.NumberOfDaysInMonth,"ax",%progbits
	.align	2
	.global	NumberOfDaysInMonth
	.type	NumberOfDaysInMonth, %function
NumberOfDaysInMonth:
.LFB6:
	.loc 1 170 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #12
.LCFI20:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 173 0
	ldr	r3, [fp, #-12]
	cmp	r3, #11
	bhi	.L26
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	and	r2, r3, #2640
	cmp	r2, #0
	bne	.L28
	and	r3, r3, #4
	cmp	r3, #0
	bne	.L27
	b	.L26
.L28:
	.loc 1 179 0
	mov	r3, #30
	str	r3, [fp, #-8]
	.loc 1 180 0
	b	.L29
.L27:
	.loc 1 183 0
	ldr	r0, [fp, #-16]
	bl	IsLeapYear
	mov	r3, r0
	cmp	r3, #1
	bne	.L30
	.loc 1 185 0
	mov	r3, #29
	str	r3, [fp, #-8]
	.loc 1 191 0
	b	.L29
.L30:
	.loc 1 189 0
	mov	r3, #28
	str	r3, [fp, #-8]
	.loc 1 191 0
	b	.L29
.L26:
	.loc 1 194 0
	mov	r3, #31
	str	r3, [fp, #-8]
	.loc 1 195 0
	mov	r0, r0	@ nop
.L29:
	.loc 1 198 0
	ldr	r3, [fp, #-8]
	.loc 1 199 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE6:
	.size	NumberOfDaysInMonth, .-NumberOfDaysInMonth
	.section	.text.TimeToHMS,"ax",%progbits
	.align	2
	.global	TimeToHMS
	.type	TimeToHMS, %function
TimeToHMS:
.LFB7:
	.loc 1 203 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI21:
	add	fp, sp, #0
.LCFI22:
	sub	sp, sp, #16
.LCFI23:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	str	r2, [fp, #-12]
	str	r3, [fp, #-16]
	.loc 1 204 0
	ldr	r2, [fp, #-4]
	ldr	r3, .L33
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #11
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 205 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	mov	r2, #3600
	mul	r3, r2, r3
	ldr	r2, [fp, #-4]
	rsb	r3, r3, r2
	str	r3, [fp, #-4]
	.loc 1 207 0
	ldr	r2, [fp, #-4]
	ldr	r3, .L33+4
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 208 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-4]
	rsb	r3, r3, r2
	str	r3, [fp, #-4]
	.loc 1 210 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 211 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L34:
	.align	2
.L33:
	.word	-1851608123
	.word	-2004318071
.LFE7:
	.size	TimeToHMS, .-TimeToHMS
	.section .rodata
	.align	2
.LC0:
	.ascii	"%s\000"
	.align	2
.LC1:
	.ascii	"%2u:%02u\000"
	.align	2
.LC2:
	.ascii	":%02u\000"
	.align	2
.LC3:
	.ascii	"PM\000"
	.align	2
.LC4:
	.ascii	"AM\000"
	.section	.text.TDUTILS_time_to_time_string_with_ampm,"ax",%progbits
	.align	2
	.global	TDUTILS_time_to_time_string_with_ampm
	.type	TDUTILS_time_to_time_string_with_ampm, %function
TDUTILS_time_to_time_string_with_ampm:
.LFB8:
	.loc 1 220 0
	@ args = 8, pretend = 0, frame = 64
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #68
.LCFI26:
	str	r0, [fp, #-56]
	str	r1, [fp, #-60]
	str	r2, [fp, #-64]
	str	r3, [fp, #-68]
	.loc 1 228 0
	ldr	r3, [fp, #-68]
	cmp	r3, #1
	bne	.L36
	.loc 1 228 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-64]
	ldr	r3, .L44
	cmp	r2, r3
	bne	.L36
	.loc 1 230 0 is_stmt 1
	mov	r0, #0
	bl	GetOffOnStr
	mov	r3, r0
	sub	r2, fp, #40
	mov	r0, r2
	mov	r1, #32
	ldr	r2, .L44+4
	bl	snprintf
	b	.L37
.L36:
	.loc 1 234 0
	sub	r1, fp, #44
	sub	r2, fp, #48
	sub	r3, fp, #52
	ldr	r0, [fp, #-64]
	bl	TimeToHMS
	.loc 1 236 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 238 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	bne	.L38
	.loc 1 240 0
	mov	r3, #12
	str	r3, [fp, #-44]
	b	.L39
.L38:
	.loc 1 242 0
	ldr	r3, [fp, #-44]
	cmp	r3, #12
	bne	.L40
	.loc 1 244 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L39
.L40:
	.loc 1 246 0
	ldr	r3, [fp, #-44]
	cmp	r3, #12
	bls	.L39
	.loc 1 248 0
	ldr	r3, [fp, #-44]
	sub	r3, r3, #12
	str	r3, [fp, #-44]
	.loc 1 249 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L39:
	.loc 1 253 0
	ldr	r3, [fp, #-44]
	ldr	r1, [fp, #-48]
	sub	r2, fp, #40
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #32
	ldr	r2, .L44+8
	bl	snprintf
	.loc 1 255 0
	ldr	r3, [fp, #4]
	cmp	r3, #1
	bne	.L41
	.loc 1 257 0
	ldr	r3, [fp, #-52]
	sub	r2, fp, #40
	mov	r0, r2
	mov	r1, #32
	ldr	r2, .L44+12
	bl	sp_strlcat
.L41:
	.loc 1 260 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L42
	.loc 1 262 0
	sub	r3, fp, #40
	mov	r0, r3
	ldr	r1, .L44+16
	mov	r2, #32
	bl	strlcat
	b	.L37
.L42:
	.loc 1 266 0
	sub	r3, fp, #40
	mov	r0, r3
	ldr	r1, .L44+20
	mov	r2, #32
	bl	strlcat
.L37:
	.loc 1 272 0
	ldr	r3, [fp, #8]
	cmp	r3, #0
	bne	.L43
	.loc 1 274 0
	ldr	r3, [fp, #-56]
	mov	r2, #0
	strb	r2, [r3, #0]
.L43:
	.loc 1 277 0
	sub	r3, fp, #40
	ldr	r0, [fp, #-56]
	mov	r1, r3
	ldr	r2, [fp, #-60]
	bl	strlcat
	.loc 1 279 0
	ldr	r3, [fp, #-56]
	.loc 1 280 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L45:
	.align	2
.L44:
	.word	86400
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
.LFE8:
	.size	TDUTILS_time_to_time_string_with_ampm, .-TDUTILS_time_to_time_string_with_ampm
	.section .rodata
	.align	2
.LC5:
	.ascii	"%s \000"
	.align	2
.LC6:
	.ascii	"%02d/%02d\000"
	.align	2
.LC7:
	.ascii	"/%02d\000"
	.align	2
.LC8:
	.ascii	"/%04d\000"
	.section	.text.GetDateStr,"ax",%progbits
	.align	2
	.global	GetDateStr
	.type	GetDateStr, %function
GetDateStr:
.LFB9:
	.loc 1 284 0
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #36
.LCFI29:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	str	r3, [fp, #-36]
	.loc 1 289 0
	sub	r1, fp, #8
	sub	r2, fp, #12
	sub	r3, fp, #16
	sub	r0, fp, #20
	str	r0, [sp, #0]
	ldr	r0, [fp, #-32]
	bl	DateToDMY
	.loc 1 291 0
	ldr	r3, [fp, #-24]
	mov	r2, #0
	strb	r2, [r3, #0]
	.loc 1 293 0
	ldr	r3, [fp, #4]
	cmp	r3, #225
	bne	.L47
	.loc 1 295 0
	ldr	r3, [fp, #-20]
	mov	r0, r3
	bl	GetDayShortStr
	mov	r3, r0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	ldr	r2, .L53
	bl	sp_strlcat
	b	.L48
.L47:
	.loc 1 298 0
	ldr	r3, [fp, #4]
	cmp	r3, #200
	bne	.L48
	.loc 1 300 0
	ldr	r3, [fp, #-20]
	mov	r0, r3
	bl	GetDayLongStr
	mov	r3, r0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	ldr	r2, .L53
	bl	sp_strlcat
.L48:
	.loc 1 303 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-8]
	str	r2, [sp, #0]
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	ldr	r2, .L53+4
	bl	sp_strlcat
	.loc 1 305 0
	ldr	r3, [fp, #-36]
	cmp	r3, #125
	bne	.L49
	.loc 1 307 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L53+8
	cmp	r2, r3
	bls	.L50
	.loc 1 309 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #2000
	str	r3, [fp, #-16]
	b	.L51
.L50:
	.loc 1 313 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #1984
	sub	r3, r3, #15
	str	r3, [fp, #-16]
.L51:
	.loc 1 316 0
	ldr	r3, [fp, #-16]
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	ldr	r2, .L53+12
	bl	sp_strlcat
	b	.L52
.L49:
	.loc 1 318 0
	ldr	r3, [fp, #-36]
	cmp	r3, #100
	bne	.L52
	.loc 1 320 0
	ldr	r3, [fp, #-16]
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	ldr	r2, .L53+16
	bl	sp_strlcat
.L52:
	.loc 1 323 0
	ldr	r3, [fp, #-24]
	.loc 1 324 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L54:
	.align	2
.L53:
	.word	.LC5
	.word	.LC6
	.word	1999
	.word	.LC7
	.word	.LC8
.LFE9:
	.size	GetDateStr, .-GetDateStr
	.section .rodata
	.align	2
.LC9:
	.ascii	"%s %s\000"
	.section	.text.DATE_TIME_to_DateTimeStr_32,"ax",%progbits
	.align	2
	.global	DATE_TIME_to_DateTimeStr_32
	.type	DATE_TIME_to_DateTimeStr_32, %function
DATE_TIME_to_DateTimeStr_32:
.LFB10:
	.loc 1 331 0
	@ args = 0, pretend = 0, frame = 80
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI30:
	add	fp, sp, #8
.LCFI31:
	sub	sp, sp, #88
.LCFI32:
	str	r0, [fp, #-76]
	str	r1, [fp, #-80]
	sub	r1, fp, #88
	stmia	r1, {r2, r3}
	.loc 1 337 0
	ldrh	r3, [fp, #-84]
	.loc 1 336 0
	sub	r2, fp, #72
	mov	r1, #250
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #32
	mov	r2, r3
	mov	r3, #100
	bl	GetDateStr
	mov	r4, r0
	.loc 1 338 0
	ldr	r3, [fp, #-88]
	sub	r2, fp, #40
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, #32
	mov	r2, r3
	mov	r3, #0
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r3, r0
	.loc 1 336 0
	sub	r2, fp, #40
	mov	r0, r2
	mov	r1, r3
	bl	ShaveLeftPad
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r0, [fp, #-76]
	ldr	r1, [fp, #-80]
	ldr	r2, .L56
	mov	r3, r4
	bl	snprintf
	.loc 1 341 0
	ldr	r3, [fp, #-76]
	.loc 1 342 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L57:
	.align	2
.L56:
	.word	.LC9
.LFE10:
	.size	DATE_TIME_to_DateTimeStr_32, .-DATE_TIME_to_DateTimeStr_32
	.section	.text.DT1_IsBiggerThan_DT2,"ax",%progbits
	.align	2
	.global	DT1_IsBiggerThan_DT2
	.type	DT1_IsBiggerThan_DT2, %function
DT1_IsBiggerThan_DT2:
.LFB11:
	.loc 1 346 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI33:
	add	fp, sp, #0
.LCFI34:
	sub	sp, sp, #12
.LCFI35:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 349 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 351 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-12]
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r2, r3
	bls	.L59
	.loc 1 353 0
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L60
.L59:
	.loc 1 355 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-12]
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r2, r3
	bne	.L60
	.loc 1 357 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	mov	r0, r0, asl #8
	orr	r1, r0, r1
	ldrb	r0, [r3, #2]	@ zero_extendqisi2
	mov	r0, r0, asl #16
	orr	r1, r0, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r1
	cmp	r2, r3
	movls	r3, #0
	movhi	r3, #1
	str	r3, [fp, #-4]
.L60:
	.loc 1 360 0
	ldr	r3, [fp, #-4]
	.loc 1 361 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE11:
	.size	DT1_IsBiggerThan_DT2, .-DT1_IsBiggerThan_DT2
	.section	.text.DT1_IsBiggerThanOrEqualTo_DT2,"ax",%progbits
	.align	2
	.global	DT1_IsBiggerThanOrEqualTo_DT2
	.type	DT1_IsBiggerThanOrEqualTo_DT2, %function
DT1_IsBiggerThanOrEqualTo_DT2:
.LFB12:
	.loc 1 365 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI36:
	add	fp, sp, #0
.LCFI37:
	sub	sp, sp, #12
.LCFI38:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 368 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 370 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-12]
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r2, r3
	bls	.L62
	.loc 1 372 0
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L63
.L62:
	.loc 1 374 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-12]
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r2, r3
	bne	.L63
	.loc 1 376 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	mov	r0, r0, asl #8
	orr	r1, r0, r1
	ldrb	r0, [r3, #2]	@ zero_extendqisi2
	mov	r0, r0, asl #16
	orr	r1, r0, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r1
	cmp	r2, r3
	movcc	r3, #0
	movcs	r3, #1
	str	r3, [fp, #-4]
.L63:
	.loc 1 379 0
	ldr	r3, [fp, #-4]
	.loc 1 380 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE12:
	.size	DT1_IsBiggerThanOrEqualTo_DT2, .-DT1_IsBiggerThanOrEqualTo_DT2
	.section	.text.DT1_IsEqualTo_DT2,"ax",%progbits
	.align	2
	.global	DT1_IsEqualTo_DT2
	.type	DT1_IsEqualTo_DT2, %function
DT1_IsEqualTo_DT2:
.LFB13:
	.loc 1 384 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI39:
	add	fp, sp, #0
.LCFI40:
	sub	sp, sp, #12
.LCFI41:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 387 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 389 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-12]
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r2, r3
	bne	.L65
	.loc 1 389 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	mov	r0, r0, asl #8
	orr	r1, r0, r1
	ldrb	r0, [r3, #2]	@ zero_extendqisi2
	mov	r0, r0, asl #16
	orr	r1, r0, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r1
	cmp	r2, r3
	bne	.L65
	.loc 1 391 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-4]
.L65:
	.loc 1 394 0
	ldr	r3, [fp, #-4]
	.loc 1 395 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE13:
	.size	DT1_IsEqualTo_DT2, .-DT1_IsEqualTo_DT2
	.section	.text.TDUTILS_add_seconds_to_passed_DT_ptr,"ax",%progbits
	.align	2
	.global	TDUTILS_add_seconds_to_passed_DT_ptr
	.type	TDUTILS_add_seconds_to_passed_DT_ptr, %function
TDUTILS_add_seconds_to_passed_DT_ptr:
.LFB14:
	.loc 1 481 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI42:
	add	fp, sp, #0
.LCFI43:
	sub	sp, sp, #20
.LCFI44:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 488 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L68
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #16
	str	r3, [fp, #-4]
	.loc 1 490 0
	ldr	r3, [fp, #-20]
	ldr	r2, .L68
	umull	r1, r2, r3, r2
	mov	r2, r2, lsr #16
	ldr	r1, .L68+4
	mul	r2, r1, r2
	rsb	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 493 0
	ldr	r3, [fp, #-16]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	rsb	r3, r3, #86016
	add	r3, r3, #384
	str	r3, [fp, #-12]
	.loc 1 495 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bcc	.L67
	.loc 1 497 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	rsb	r3, r3, r2
	str	r3, [fp, #-8]
	.loc 1 499 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
	.loc 1 503 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	strb	r2, [r3, #0]
	mov	r2, #0
	strb	r2, [r3, #1]
	mov	r2, #0
	strb	r2, [r3, #2]
	mov	r2, #0
	strb	r2, [r3, #3]
.L67:
	.loc 1 508 0
	ldr	r3, [fp, #-16]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-4]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-16]
	and	r1, r2, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #4]
	mov	r2, r2, lsr #8
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	mov	r1, #0
	orr	r2, r1, r2
	strb	r2, [r3, #5]
	.loc 1 510 0
	ldr	r3, [fp, #-16]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-8]
	add	r2, r2, r3
	ldr	r3, [fp, #-16]
	and	r1, r2, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #0]
	mov	r1, r2, lsr #8
	and	r1, r1, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #1]
	mov	r1, r2, lsr #16
	and	r1, r1, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #2]
	mov	r2, r2, lsr #24
	mov	r1, #0
	orr	r2, r1, r2
	strb	r2, [r3, #3]
	.loc 1 511 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L69:
	.align	2
.L68:
	.word	-1037155065
	.word	86400
.LFE14:
	.size	TDUTILS_add_seconds_to_passed_DT_ptr, .-TDUTILS_add_seconds_to_passed_DT_ptr
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x77b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF84
	.byte	0x1
	.4byte	.LASF85
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x5
	.byte	0x6
	.byte	0x3
	.byte	0x22
	.4byte	0xbf
	.uleb128 0x6
	.ascii	"T\000"
	.byte	0x3
	.byte	0x24
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.ascii	"D\000"
	.byte	0x3
	.byte	0x26
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x28
	.4byte	0x9e
	.uleb128 0x5
	.byte	0x14
	.byte	0x3
	.byte	0x31
	.4byte	0x151
	.uleb128 0x7
	.4byte	.LASF15
	.byte	0x3
	.byte	0x33
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF16
	.byte	0x3
	.byte	0x35
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x7
	.4byte	.LASF17
	.byte	0x3
	.byte	0x35
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF18
	.byte	0x3
	.byte	0x35
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x7
	.4byte	.LASF19
	.byte	0x3
	.byte	0x37
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF20
	.byte	0x3
	.byte	0x37
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x7
	.4byte	.LASF21
	.byte	0x3
	.byte	0x37
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x7
	.4byte	.LASF22
	.byte	0x3
	.byte	0x39
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0x7
	.4byte	.LASF23
	.byte	0x3
	.byte	0x3b
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x3
	.byte	0x3d
	.4byte	0xca
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.byte	0x23
	.byte	0x1
	.4byte	0x5e
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1b1
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x1
	.byte	0x23
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x1
	.byte	0x23
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x1
	.byte	0x23
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x27
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xb
	.4byte	0x5e
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF45
	.byte	0x1
	.byte	0x3d
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x25c
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x1
	.byte	0x3d
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x1
	.byte	0x3d
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x1
	.byte	0x3d
	.4byte	0x25c
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0xd
	.4byte	.LASF31
	.byte	0x1
	.byte	0x3f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xd
	.4byte	.LASF32
	.byte	0x1
	.byte	0x3f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xd
	.4byte	.LASF33
	.byte	0x1
	.byte	0x3f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xd
	.4byte	.LASF34
	.byte	0x1
	.byte	0x3f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xd
	.4byte	.LASF35
	.byte	0x1
	.byte	0x40
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xd
	.4byte	.LASF36
	.byte	0x1
	.byte	0x40
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xd
	.4byte	.LASF37
	.byte	0x1
	.byte	0x40
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.4byte	0x151
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.byte	0x63
	.byte	0x1
	.4byte	0x5e
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x2b8
	.uleb128 0x9
	.4byte	.LASF40
	.byte	0x1
	.byte	0x63
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x9
	.4byte	.LASF41
	.byte	0x1
	.byte	0x63
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x9
	.4byte	.LASF42
	.byte	0x1
	.byte	0x63
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xd
	.4byte	.LASF43
	.byte	0x1
	.byte	0x65
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.byte	0x7b
	.byte	0x1
	.4byte	0x5e
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x2f1
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x1
	.byte	0x7b
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x7d
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF46
	.byte	0x1
	.byte	0x85
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x35e
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x1
	.byte	0x85
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x9
	.4byte	.LASF47
	.byte	0x1
	.byte	0x85
	.4byte	0x35e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x9
	.4byte	.LASF48
	.byte	0x1
	.byte	0x85
	.4byte	0x35e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x9
	.4byte	.LASF49
	.byte	0x1
	.byte	0x85
	.4byte	0x35e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x9
	.4byte	.LASF50
	.byte	0x1
	.byte	0x85
	.4byte	0x35e
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xa
	.ascii	"II\000"
	.byte	0x1
	.byte	0x87
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xb
	.4byte	0x363
	.uleb128 0xe
	.byte	0x4
	.4byte	0x5e
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF51
	.byte	0x1
	.byte	0xa2
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x395
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x1
	.byte	0xa2
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF52
	.byte	0x1
	.byte	0xa9
	.byte	0x1
	.4byte	0x5e
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x3dc
	.uleb128 0x9
	.4byte	.LASF53
	.byte	0x1
	.byte	0xa9
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x1
	.byte	0xa9
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xab
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF54
	.byte	0x1
	.byte	0xca
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x42e
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x1
	.byte	0xca
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x9
	.4byte	.LASF55
	.byte	0x1
	.byte	0xca
	.4byte	0x35e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x9
	.4byte	.LASF56
	.byte	0x1
	.byte	0xca
	.4byte	0x35e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x9
	.4byte	.LASF57
	.byte	0x1
	.byte	0xca
	.4byte	0x35e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF58
	.byte	0x1
	.byte	0xd6
	.byte	0x1
	.4byte	0x4e8
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x4e8
	.uleb128 0x9
	.4byte	.LASF59
	.byte	0x1
	.byte	0xd6
	.4byte	0x4ee
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x9
	.4byte	.LASF60
	.byte	0x1
	.byte	0xd7
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x1
	.byte	0xd8
	.4byte	0x1b1
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x9
	.4byte	.LASF61
	.byte	0x1
	.byte	0xd9
	.4byte	0x4f3
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x9
	.4byte	.LASF62
	.byte	0x1
	.byte	0xda
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF63
	.byte	0x1
	.byte	0xdb
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0xd
	.4byte	.LASF64
	.byte	0x1
	.byte	0xdd
	.4byte	0x4f8
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0xd
	.4byte	.LASF35
	.byte	0x1
	.byte	0xdf
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0xd
	.4byte	.LASF36
	.byte	0x1
	.byte	0xdf
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0xd
	.4byte	.LASF37
	.byte	0x1
	.byte	0xdf
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0xd
	.4byte	.LASF65
	.byte	0x1
	.byte	0xe1
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.4byte	0x25
	.uleb128 0xb
	.4byte	0x4e8
	.uleb128 0xb
	.4byte	0x85
	.uleb128 0xf
	.4byte	0x25
	.4byte	0x508
	.uleb128 0x10
	.4byte	0x90
	.byte	0x1f
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x11b
	.byte	0x1
	.4byte	0x4e8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x5ae
	.uleb128 0x12
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x11b
	.4byte	0x4ee
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x11b
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x11b
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x12
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x11b
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x12
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x11b
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x13
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x11d
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x11d
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x11d
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x11f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x14a
	.byte	0x1
	.4byte	0x4e8
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x61c
	.uleb128 0x12
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x14a
	.4byte	0x4ee
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x12
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x14a
	.4byte	0x1b1
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x12
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x14a
	.4byte	0x61c
	.byte	0x3
	.byte	0x91
	.sleb128 -92
	.uleb128 0x13
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x14c
	.4byte	0x4f8
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x13
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x14c
	.4byte	0x4f8
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.byte	0
	.uleb128 0xb
	.4byte	0xbf
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x159
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x66c
	.uleb128 0x14
	.ascii	"DT1\000"
	.byte	0x1
	.2byte	0x159
	.4byte	0x66c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.ascii	"DT2\000"
	.byte	0x1
	.2byte	0x159
	.4byte	0x66c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x15
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x15b
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xb
	.4byte	0x671
	.uleb128 0xe
	.byte	0x4
	.4byte	0x61c
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x16c
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x6c2
	.uleb128 0x14
	.ascii	"DT1\000"
	.byte	0x1
	.2byte	0x16c
	.4byte	0x66c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.ascii	"DT2\000"
	.byte	0x1
	.2byte	0x16c
	.4byte	0x66c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x15
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x16e
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x17f
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x70d
	.uleb128 0x14
	.ascii	"DT1\000"
	.byte	0x1
	.2byte	0x17f
	.4byte	0x66c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.ascii	"DT2\000"
	.byte	0x1
	.2byte	0x17f
	.4byte	0x66c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x15
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x181
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x1e0
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x773
	.uleb128 0x14
	.ascii	"pdt\000"
	.byte	0x1
	.2byte	0x1e0
	.4byte	0x773
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x1e0
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x1e2
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x13
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x1e4
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x1e4
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xb
	.4byte	0x778
	.uleb128 0xe
	.byte	0x4
	.4byte	0xbf
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI43
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF18:
	.ascii	"__year\000"
.LASF84:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF15:
	.ascii	"date_time\000"
.LASF33:
	.ascii	"lyear\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF17:
	.ascii	"__month\000"
.LASF59:
	.ascii	"dest\000"
.LASF83:
	.ascii	"lseconds_to_start_of_next_day\000"
.LASF6:
	.ascii	"short int\000"
.LASF79:
	.ascii	"TDUTILS_add_seconds_to_passed_DT_ptr\000"
.LASF40:
	.ascii	"phour\000"
.LASF82:
	.ascii	"lseconds_to_add\000"
.LASF39:
	.ascii	"HMSToTime\000"
.LASF57:
	.ascii	"psec_ptr\000"
.LASF30:
	.ascii	"pdtcs_ptr\000"
.LASF31:
	.ascii	"lday_of_month\000"
.LASF34:
	.ascii	"ldow\000"
.LASF26:
	.ascii	"pmonth\000"
.LASF27:
	.ascii	"pyear_4digit\000"
.LASF22:
	.ascii	"__dayofweek\000"
.LASF56:
	.ascii	"pmin_ptr\000"
.LASF81:
	.ascii	"ldays_to_add\000"
.LASF80:
	.ascii	"pseconds\000"
.LASF32:
	.ascii	"lmonth_1_12\000"
.LASF43:
	.ascii	"ltime\000"
.LASF10:
	.ascii	"long long int\000"
.LASF74:
	.ascii	"t_str_32\000"
.LASF24:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF13:
	.ascii	"long int\000"
.LASF46:
	.ascii	"DateToDMY\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF45:
	.ascii	"DateAndTimeToDTCS\000"
.LASF38:
	.ascii	"DMYToDate\000"
.LASF36:
	.ascii	"lmin\000"
.LASF70:
	.ascii	"lday\000"
.LASF14:
	.ascii	"DATE_TIME\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF25:
	.ascii	"pday\000"
.LASF76:
	.ascii	"DT1_IsBiggerThan_DT2\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF55:
	.ascii	"phour_ptr\000"
.LASF44:
	.ascii	"DayOfWeek\000"
.LASF2:
	.ascii	"signed char\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF60:
	.ascii	"pdest_size\000"
.LASF41:
	.ascii	"pmin\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF64:
	.ascii	"str_32\000"
.LASF58:
	.ascii	"TDUTILS_time_to_time_string_with_ampm\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF68:
	.ascii	"pshow_year\000"
.LASF63:
	.ascii	"pconcat_to_pstr\000"
.LASF0:
	.ascii	"char\000"
.LASF28:
	.ascii	"pdate\000"
.LASF66:
	.ascii	"GetDateStr\000"
.LASF53:
	.ascii	"pmonth_1\000"
.LASF77:
	.ascii	"DT1_IsBiggerThanOrEqualTo_DT2\000"
.LASF37:
	.ascii	"lsec\000"
.LASF35:
	.ascii	"lhour\000"
.LASF23:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF48:
	.ascii	"pmonth_ptr\000"
.LASF71:
	.ascii	"lmonth\000"
.LASF29:
	.ascii	"ptime\000"
.LASF42:
	.ascii	"psec\000"
.LASF12:
	.ascii	"long unsigned int\000"
.LASF47:
	.ascii	"pday_ptr\000"
.LASF16:
	.ascii	"__day\000"
.LASF21:
	.ascii	"__seconds\000"
.LASF62:
	.ascii	"pinclude_seconds\000"
.LASF73:
	.ascii	"pdate_time\000"
.LASF78:
	.ascii	"DT1_IsEqualTo_DT2\000"
.LASF75:
	.ascii	"d_str_32\000"
.LASF69:
	.ascii	"pshow_dow\000"
.LASF85:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/../commo"
	.ascii	"n_includes/cal_td_utils.c\000"
.LASF54:
	.ascii	"TimeToHMS\000"
.LASF67:
	.ascii	"pdest\000"
.LASF72:
	.ascii	"DATE_TIME_to_DateTimeStr_32\000"
.LASF65:
	.ascii	"lshow_PM\000"
.LASF61:
	.ascii	"ptreat_as_a_start_time\000"
.LASF49:
	.ascii	"pyear_4digit_ptr\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF52:
	.ascii	"NumberOfDaysInMonth\000"
.LASF51:
	.ascii	"IsLeapYear\000"
.LASF50:
	.ascii	"pdow_ptr\000"
.LASF20:
	.ascii	"__minutes\000"
.LASF19:
	.ascii	"__hours\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
