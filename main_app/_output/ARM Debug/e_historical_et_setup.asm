	.file	"e_historical_et_setup.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_state_display_order,"aw",%nobits
	.align	2
	.type	g_state_display_order, %object
	.size	g_state_display_order, 52
g_state_display_order:
	.space	52
	.section	.bss.g_county_display_order,"aw",%nobits
	.align	2
	.type	g_county_display_order, %object
	.size	g_county_display_order, 260
g_county_display_order:
	.space	260
	.section	.bss.g_city_display_order,"aw",%nobits
	.align	2
	.type	g_city_display_order, %object
	.size	g_city_display_order, 616
g_city_display_order:
	.space	616
	.section	.bss.g_state_display_count,"aw",%nobits
	.align	2
	.type	g_state_display_count, %object
	.size	g_state_display_count, 4
g_state_display_count:
	.space	4
	.section	.bss.g_county_display_count,"aw",%nobits
	.align	2
	.type	g_county_display_count, %object
	.size	g_county_display_count, 4
g_county_display_count:
	.space	4
	.section	.bss.g_city_display_count,"aw",%nobits
	.align	2
	.type	g_city_display_count, %object
	.size	g_city_display_count, 4
g_city_display_count:
	.space	4
	.section	.bss.g_counties_sorted,"aw",%nobits
	.align	2
	.type	g_counties_sorted, %object
	.size	g_counties_sorted, 4
g_counties_sorted:
	.space	4
	.section	.bss.g_cities_sorted,"aw",%nobits
	.align	2
	.type	g_cities_sorted, %object
	.size	g_cities_sorted, 4
g_cities_sorted:
	.space	4
	.section	.bss.g_cursor_pos_of_direct_entry_field,"aw",%nobits
	.align	2
	.type	g_cursor_pos_of_direct_entry_field, %object
	.size	g_cursor_pos_of_direct_entry_field, 4
g_cursor_pos_of_direct_entry_field:
	.space	4
	.global	g_HISTORICAL_ET_state_index
	.section	.bss.g_HISTORICAL_ET_state_index,"aw",%nobits
	.align	2
	.type	g_HISTORICAL_ET_state_index, %object
	.size	g_HISTORICAL_ET_state_index, 4
g_HISTORICAL_ET_state_index:
	.space	4
	.global	g_HISTORICAL_ET_county_index
	.section	.bss.g_HISTORICAL_ET_county_index,"aw",%nobits
	.align	2
	.type	g_HISTORICAL_ET_county_index, %object
	.size	g_HISTORICAL_ET_county_index, 4
g_HISTORICAL_ET_county_index:
	.space	4
	.global	g_HISTORICAL_ET_city_index
	.section	.bss.g_HISTORICAL_ET_city_index,"aw",%nobits
	.align	2
	.type	g_HISTORICAL_ET_city_index, %object
	.size	g_HISTORICAL_ET_city_index, 4
g_HISTORICAL_ET_city_index:
	.space	4
	.section	.text.HISTORICAL_ET_qsort_partition,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_qsort_partition, %function
HISTORICAL_ET_qsort_partition:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_historical_et_setup.c"
	.loc 1 92 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #20
.LCFI2:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	.loc 1 100 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 101 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-12]
	.loc 1 103 0
	ldr	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	ldr	r0, .L10
	mov	r1, r3
	mov	r2, #24
	bl	strlcpy
	.loc 1 105 0
	b	.L2
.L5:
	.loc 1 109 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L3:
	.loc 1 107 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bgt	.L6
	.loc 1 107 0 is_stmt 0 discriminator 2
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, .L10
	mov	r2, #24
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	ble	.L5
	.loc 1 112 0 is_stmt 1
	b	.L6
.L8:
	.loc 1 114 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
.L6:
	.loc 1 112 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bgt	.L7
	.loc 1 112 0 is_stmt 0 discriminator 2
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, .L10
	mov	r2, #24
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	bgt	.L8
.L7:
	.loc 1 117 0 is_stmt 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bge	.L2
	.loc 1 119 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	ldr	r0, .L10+4
	mov	r1, r3
	mov	r2, #24
	bl	strlcpy
	.loc 1 120 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r1, r2, r3
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	mov	r2, #24
	bl	strlcpy
	.loc 1 121 0
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, .L10+4
	mov	r2, #24
	bl	strlcpy
	.loc 1 123 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 124 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
.L2:
	.loc 1 105 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	ble	.L3
	.loc 1 128 0
	ldr	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	ldr	r0, .L10+4
	mov	r1, r3
	mov	r2, #24
	bl	strlcpy
	.loc 1 129 0
	ldr	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r1, r2, r3
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	mov	r2, #24
	bl	strlcpy
	.loc 1 130 0
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, .L10+4
	mov	r2, #24
	bl	strlcpy
	.loc 1 132 0
	ldr	r3, [fp, #-12]
	.loc 1 133 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L11:
	.align	2
.L10:
	.word	pivot.3825
	.word	swapArray.3824
.LFE0:
	.size	HISTORICAL_ET_qsort_partition, .-HISTORICAL_ET_qsort_partition
	.section	.text.HISTORICAL_ET_qsort,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_qsort, %function
HISTORICAL_ET_qsort:
.LFB1:
	.loc 1 136 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #16
.LCFI5:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	.loc 1 139 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bge	.L12
	.loc 1 141 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-20]
	bl	HISTORICAL_ET_qsort_partition
	str	r0, [fp, #-8]
	.loc 1 143 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	mov	r2, r3
	bl	HISTORICAL_ET_qsort
	.loc 1 144 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	ldr	r0, [fp, #-12]
	mov	r1, r3
	ldr	r2, [fp, #-20]
	bl	HISTORICAL_ET_qsort
.L12:
	.loc 1 146 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	HISTORICAL_ET_qsort, .-HISTORICAL_ET_qsort
	.section	.text.HISTORICAL_ET_update_et_numbers,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_update_et_numbers, %function
HISTORICAL_ET_update_et_numbers:
.LFB2:
	.loc 1 149 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	.loc 1 150 0
	ldr	r3, .L16+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L14
	.loc 1 152 0
	ldr	r3, .L16+12
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L16+16
	add	r3, r2, r3
	ldr	r0, .L16+20
	mov	r1, r3
	mov	r2, #25
	bl	strlcpy
	.loc 1 153 0
	ldr	r3, .L16+24
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	ldr	r2, .L16+28
	add	r3, r3, r2
	ldr	r0, .L16+32
	mov	r1, r3
	mov	r2, #25
	bl	strlcpy
	.loc 1 154 0
	ldr	r3, .L16+36
	ldr	r3, [r3, #0]
	mov	r2, #76
	mul	r2, r3, r2
	ldr	r3, .L16+40
	add	r3, r2, r3
	ldr	r0, .L16+44
	mov	r1, r3
	mov	r2, #25
	bl	strlcpy
	.loc 1 156 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #28
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+48
	fsts	s15, [r3, #0]
	.loc 1 157 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #32
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+52
	fsts	s15, [r3, #0]
	.loc 1 158 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #36
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+56
	fsts	s15, [r3, #0]
	.loc 1 159 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #40
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+60
	fsts	s15, [r3, #0]
	.loc 1 160 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #44
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+64
	fsts	s15, [r3, #0]
	.loc 1 161 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #48
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+68
	fsts	s15, [r3, #0]
	.loc 1 162 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #52
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+72
	fsts	s15, [r3, #0]
	.loc 1 163 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #56
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+76
	fsts	s15, [r3, #0]
	.loc 1 164 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #60
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+80
	fsts	s15, [r3, #0]
	.loc 1 165 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #64
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+84
	fsts	s15, [r3, #0]
	.loc 1 166 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #68
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+88
	fsts	s15, [r3, #0]
	.loc 1 167 0
	ldr	r3, .L16+36
	ldr	r2, [r3, #0]
	ldr	r1, .L16+40
	mov	r3, #72
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L16
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L16+92
	fsts	s15, [r3, #0]
	.loc 1 169 0
	bl	Refresh_Screen
.L14:
	.loc 1 171 0
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	0
	.word	1079574528
	.word	GuiVar_HistoricalETUseYourOwn
	.word	g_HISTORICAL_ET_state_index
	.word	States
	.word	GuiVar_HistoricalETState
	.word	g_HISTORICAL_ET_county_index
	.word	Counties
	.word	GuiVar_HistoricalETCounty
	.word	g_HISTORICAL_ET_city_index
	.word	ET_PredefinedData
	.word	GuiVar_HistoricalETCity
	.word	GuiVar_HistoricalET1
	.word	GuiVar_HistoricalET2
	.word	GuiVar_HistoricalET3
	.word	GuiVar_HistoricalET4
	.word	GuiVar_HistoricalET5
	.word	GuiVar_HistoricalET6
	.word	GuiVar_HistoricalET7
	.word	GuiVar_HistoricalET8
	.word	GuiVar_HistoricalET9
	.word	GuiVar_HistoricalET10
	.word	GuiVar_HistoricalET11
	.word	GuiVar_HistoricalET12
.LFE2:
	.size	HISTORICAL_ET_update_et_numbers, .-HISTORICAL_ET_update_et_numbers
	.section	.text.HISTORICAL_ET_sort_states,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_sort_states, %function
HISTORICAL_ET_sort_states:
.LFB3:
	.loc 1 174 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	sub	sp, sp, #8
.LCFI10:
	.loc 1 179 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L19
.L20:
	.loc 1 181 0 discriminator 2
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L26
	add	r1, r2, r3
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L26+4
	add	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	mov	r2, #24
	bl	strlcpy
	.loc 1 179 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L19:
	.loc 1 179 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #12
	bls	.L20
	.loc 1 184 0 is_stmt 1
	ldr	r0, .L26
	mov	r1, #0
	mov	r2, #12
	bl	HISTORICAL_ET_qsort
	.loc 1 186 0
	mov	r3, #0
	str	r3, [fp, #-8]
	ldr	r3, .L26+8
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L21
.L25:
	.loc 1 188 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L22
.L24:
	.loc 1 190 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L26
	add	r1, r2, r3
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L26+4
	add	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	mov	r2, #24
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	bne	.L23
	.loc 1 192 0
	ldr	r3, .L26+8
	ldr	r2, [r3, #0]
	ldr	r3, .L26+12
	ldr	r1, [fp, #-12]
	str	r1, [r3, r2, asl #2]
	.loc 1 194 0
	ldr	r3, .L26+8
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L26+8
	str	r2, [r3, #0]
.L23:
	.loc 1 188 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L22:
	.loc 1 188 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #12
	bls	.L24
	.loc 1 186 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L21:
	.loc 1 186 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #12
	bls	.L25
	.loc 1 199 0 is_stmt 1
	ldr	r3, .L26+16
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 200 0
	ldr	r3, .L26+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 201 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L27:
	.align	2
.L26:
	.word	StatesCopy.3849
	.word	States
	.word	g_state_display_count
	.word	g_state_display_order
	.word	g_counties_sorted
	.word	g_cities_sorted
.LFE3:
	.size	HISTORICAL_ET_sort_states, .-HISTORICAL_ET_sort_states
	.section	.text.HISTORICAL_ET_sort_counties,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_sort_counties, %function
HISTORICAL_ET_sort_counties:
.LFB4:
	.loc 1 204 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	sub	sp, sp, #12
.LCFI13:
	.loc 1 211 0
	mov	r3, #0
	str	r3, [fp, #-12]
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L29
.L31:
	.loc 1 213 0
	ldr	r0, .L37
	ldr	r2, [fp, #-12]
	mov	r1, #24
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L37+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L30
	.loc 1 215 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L37+8
	add	r1, r2, r3
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	ldr	r2, .L37
	add	r3, r3, r2
	mov	r0, r1
	mov	r1, r3
	mov	r2, #24
	bl	strlcpy
	.loc 1 217 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L30:
	.loc 1 211 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L29:
	.loc 1 211 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #64
	bls	.L31
	.loc 1 221 0 is_stmt 1
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	ldr	r0, .L37+8
	mov	r1, #0
	mov	r2, r3
	bl	HISTORICAL_ET_qsort
	.loc 1 223 0
	mov	r3, #0
	str	r3, [fp, #-12]
	ldr	r3, .L37+12
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L32
.L36:
	.loc 1 225 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L33
.L35:
	.loc 1 227 0
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L37+8
	add	r1, r2, r3
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	ldr	r2, .L37
	add	r3, r3, r2
	mov	r0, r1
	mov	r1, r3
	mov	r2, #24
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	bne	.L34
	.loc 1 228 0 discriminator 1
	ldr	r0, .L37
	ldr	r2, [fp, #-16]
	mov	r1, #24
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L37+4
	ldr	r3, [r3, #0]
	.loc 1 227 0 discriminator 1
	cmp	r2, r3
	bne	.L34
	.loc 1 230 0
	ldr	r3, .L37+12
	ldr	r2, [r3, #0]
	ldr	r3, .L37+16
	ldr	r1, [fp, #-16]
	str	r1, [r3, r2, asl #2]
	.loc 1 232 0
	ldr	r3, .L37+12
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L37+12
	str	r2, [r3, #0]
.L34:
	.loc 1 225 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L33:
	.loc 1 225 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #64
	bls	.L35
	.loc 1 223 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L32:
	.loc 1 223 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L36
	.loc 1 237 0 is_stmt 1
	ldr	r3, .L37+20
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 238 0
	ldr	r3, .L37+24
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 239 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L38:
	.align	2
.L37:
	.word	Counties
	.word	g_HISTORICAL_ET_state_index
	.word	CountiesCopy.3864
	.word	g_county_display_count
	.word	g_county_display_order
	.word	g_counties_sorted
	.word	g_cities_sorted
.LFE4:
	.size	HISTORICAL_ET_sort_counties, .-HISTORICAL_ET_sort_counties
	.section	.text.HISTORICAL_ET_sort_cities,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_sort_cities, %function
HISTORICAL_ET_sort_cities:
.LFB5:
	.loc 1 242 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI14:
	add	fp, sp, #4
.LCFI15:
	sub	sp, sp, #12
.LCFI16:
	.loc 1 249 0
	mov	r3, #0
	str	r3, [fp, #-12]
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L40
.L42:
	.loc 1 251 0
	ldr	r1, .L48
	ldr	r2, [fp, #-12]
	mov	r3, #24
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L48+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L41
	.loc 1 253 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L48+8
	add	r2, r2, r3
	ldr	r3, [fp, #-12]
	mov	r1, #76
	mul	r1, r3, r1
	ldr	r3, .L48
	add	r3, r1, r3
	mov	r0, r2
	mov	r1, r3
	mov	r2, #24
	bl	strlcpy
	.loc 1 255 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L41:
	.loc 1 249 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L40:
	.loc 1 249 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #153
	bls	.L42
	.loc 1 259 0 is_stmt 1
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	ldr	r0, .L48+8
	mov	r1, #0
	mov	r2, r3
	bl	HISTORICAL_ET_qsort
	.loc 1 261 0
	mov	r3, #0
	str	r3, [fp, #-12]
	ldr	r3, .L48+12
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L43
.L47:
	.loc 1 263 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L44
.L46:
	.loc 1 265 0
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L48+8
	add	r2, r2, r3
	ldr	r3, [fp, #-16]
	mov	r1, #76
	mul	r1, r3, r1
	ldr	r3, .L48
	add	r3, r1, r3
	mov	r0, r2
	mov	r1, r3
	mov	r2, #24
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	bne	.L45
	.loc 1 266 0 discriminator 1
	ldr	r1, .L48
	ldr	r2, [fp, #-16]
	mov	r3, #24
	mov	r0, #76
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L48+4
	ldr	r3, [r3, #0]
	.loc 1 265 0 discriminator 1
	cmp	r2, r3
	bne	.L45
	.loc 1 268 0
	ldr	r3, .L48+12
	ldr	r2, [r3, #0]
	ldr	r3, .L48+16
	ldr	r1, [fp, #-16]
	str	r1, [r3, r2, asl #2]
	.loc 1 270 0
	ldr	r3, .L48+12
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L48+12
	str	r2, [r3, #0]
.L45:
	.loc 1 263 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L44:
	.loc 1 263 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #153
	bls	.L46
	.loc 1 261 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L43:
	.loc 1 261 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L47
	.loc 1 275 0 is_stmt 1
	ldr	r3, .L48+20
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 276 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L49:
	.align	2
.L48:
	.word	ET_PredefinedData
	.word	g_HISTORICAL_ET_county_index
	.word	CitiesCopy.3880
	.word	g_city_display_count
	.word	g_city_display_order
	.word	g_cities_sorted
.LFE5:
	.size	HISTORICAL_ET_sort_cities, .-HISTORICAL_ET_sort_cities
	.section	.text.HISTORICAL_ET_process_reference_et_location,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_process_reference_et_location, %function
HISTORICAL_ET_process_reference_et_location:
.LFB6:
	.loc 1 279 0
	@ args = 4, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI17:
	add	fp, sp, #4
.LCFI18:
	sub	sp, sp, #24
.LCFI19:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 284 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 286 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L51
.L53:
	.loc 1 288 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #2
	ldr	r1, [fp, #-24]
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L52
	.loc 1 290 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
.L52:
	.loc 1 286 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L51:
	.loc 1 286 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #4]
	cmp	r2, r3
	bls	.L53
	.loc 1 294 0 is_stmt 1
	ldr	r3, [fp, #-16]
	cmp	r3, #80
	beq	.L55
	cmp	r3, #84
	bne	.L54
.L56:
	.loc 1 297 0
	bl	good_key_beep
	.loc 1 299 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #4]
	cmp	r2, r3
	bne	.L57
	.loc 1 301 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-8]
	.loc 1 310 0
	b	.L61
.L57:
	.loc 1 305 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #4]
	cmp	r2, r3
	bcs	.L61
	.loc 1 307 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 310 0
	b	.L61
.L55:
	.loc 1 313 0
	bl	good_key_beep
	.loc 1 315 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bne	.L59
	.loc 1 317 0
	ldr	r3, [fp, #4]
	str	r3, [fp, #-8]
	.loc 1 326 0
	b	.L62
.L59:
	.loc 1 321 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L62
	.loc 1 323 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 326 0
	b	.L62
.L61:
	.loc 1 310 0
	mov	r0, r0	@ nop
	b	.L54
.L62:
	.loc 1 326 0
	mov	r0, r0	@ nop
.L54:
	.loc 1 329 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-24]
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 330 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE6:
	.size	HISTORICAL_ET_process_reference_et_location, .-HISTORICAL_ET_process_reference_et_location
	.section	.text.HISTORICAL_ET_process_state_index,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_process_state_index, %function
HISTORICAL_ET_process_state_index:
.LFB7:
	.loc 1 333 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI20:
	add	fp, sp, #4
.LCFI21:
	sub	sp, sp, #8
.LCFI22:
	str	r0, [fp, #-8]
	.loc 1 334 0
	bl	HISTORICAL_ET_sort_states
	.loc 1 336 0
	ldr	r3, .L64
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	ldr	r1, .L64+4
	ldr	r2, .L64+8
	mov	r3, #0
	bl	HISTORICAL_ET_process_reference_et_location
	.loc 1 338 0
	bl	HISTORICAL_ET_sort_counties
	.loc 1 340 0
	ldr	r3, .L64+12
	ldr	r2, [r3, #0]
	ldr	r3, .L64+16
	str	r2, [r3, #0]
	.loc 1 342 0
	bl	HISTORICAL_ET_sort_cities
	.loc 1 344 0
	ldr	r3, .L64+20
	ldr	r2, [r3, #0]
	ldr	r3, .L64+24
	str	r2, [r3, #0]
	.loc 1 346 0
	bl	HISTORICAL_ET_update_et_numbers
	.loc 1 347 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L65:
	.align	2
.L64:
	.word	g_state_display_count
	.word	g_HISTORICAL_ET_state_index
	.word	g_state_display_order
	.word	g_county_display_order
	.word	g_HISTORICAL_ET_county_index
	.word	g_city_display_order
	.word	g_HISTORICAL_ET_city_index
.LFE7:
	.size	HISTORICAL_ET_process_state_index, .-HISTORICAL_ET_process_state_index
	.section	.text.HISTORICAL_ET_process_county_index,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_process_county_index, %function
HISTORICAL_ET_process_county_index:
.LFB8:
	.loc 1 350 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI23:
	add	fp, sp, #4
.LCFI24:
	sub	sp, sp, #8
.LCFI25:
	str	r0, [fp, #-8]
	.loc 1 351 0
	ldr	r3, .L70
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L67
	.loc 1 353 0
	bl	HISTORICAL_ET_sort_counties
.L67:
	.loc 1 356 0
	ldr	r3, .L70+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L68
	.loc 1 358 0
	bl	bad_key_beep
	b	.L66
.L68:
	.loc 1 362 0
	ldr	r3, .L70+4
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	ldr	r1, .L70+8
	ldr	r2, .L70+12
	mov	r3, #0
	bl	HISTORICAL_ET_process_reference_et_location
	.loc 1 364 0
	bl	HISTORICAL_ET_sort_cities
	.loc 1 366 0
	ldr	r3, .L70+16
	ldr	r2, [r3, #0]
	ldr	r3, .L70+20
	str	r2, [r3, #0]
	.loc 1 368 0
	bl	HISTORICAL_ET_update_et_numbers
.L66:
	.loc 1 370 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L71:
	.align	2
.L70:
	.word	g_counties_sorted
	.word	g_county_display_count
	.word	g_HISTORICAL_ET_county_index
	.word	g_county_display_order
	.word	g_city_display_order
	.word	g_HISTORICAL_ET_city_index
.LFE8:
	.size	HISTORICAL_ET_process_county_index, .-HISTORICAL_ET_process_county_index
	.section	.text.HISTORICAL_ET_process_city_index,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_process_city_index, %function
HISTORICAL_ET_process_city_index:
.LFB9:
	.loc 1 373 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	sub	sp, sp, #8
.LCFI28:
	str	r0, [fp, #-8]
	.loc 1 374 0
	ldr	r3, .L76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L73
	.loc 1 376 0
	bl	HISTORICAL_ET_sort_cities
.L73:
	.loc 1 379 0
	ldr	r3, .L76+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L74
	.loc 1 381 0
	bl	bad_key_beep
	b	.L72
.L74:
	.loc 1 385 0
	ldr	r3, .L76+4
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	ldr	r1, .L76+8
	ldr	r2, .L76+12
	mov	r3, #0
	bl	HISTORICAL_ET_process_reference_et_location
	.loc 1 387 0
	bl	HISTORICAL_ET_update_et_numbers
.L72:
	.loc 1 389 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L77:
	.align	2
.L76:
	.word	g_cities_sorted
	.word	g_city_display_count
	.word	g_HISTORICAL_ET_city_index
	.word	g_city_display_order
.LFE9:
	.size	HISTORICAL_ET_process_city_index, .-HISTORICAL_ET_process_city_index
	.section	.text.HISTORICAL_ET_process_user_et,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_process_user_et, %function
HISTORICAL_ET_process_user_et:
.LFB10:
	.loc 1 392 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI29:
	add	fp, sp, #4
.LCFI30:
	sub	sp, sp, #16
.LCFI31:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 393 0
	ldr	r3, .L79	@ float
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, .L79+4	@ float
	ldr	r3, .L79+8	@ float
	bl	process_fl
	.loc 1 394 0
	bl	Refresh_Screen
	.loc 1 395 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L80:
	.align	2
.L79:
	.word	1008981770
	.word	0
	.word	1101004800
.LFE10:
	.size	HISTORICAL_ET_process_user_et, .-HISTORICAL_ET_process_user_et
	.section	.text.FDTO_HISTORICAL_ET_show_source_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_show_source_dropdown, %function
FDTO_HISTORICAL_ET_show_source_dropdown:
.LFB11:
	.loc 1 398 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI32:
	add	fp, sp, #4
.LCFI33:
	.loc 1 399 0
	ldr	r3, .L82
	ldr	r3, [r3, #0]
	mov	r0, #732
	ldr	r1, .L82+4
	mov	r2, #2
	bl	FDTO_COMBOBOX_show
	.loc 1 400 0
	ldmfd	sp!, {fp, pc}
.L83:
	.align	2
.L82:
	.word	GuiVar_HistoricalETUseYourOwn
	.word	FDTO_COMBOBOX_add_items
.LFE11:
	.size	FDTO_HISTORICAL_ET_show_source_dropdown, .-FDTO_HISTORICAL_ET_show_source_dropdown
	.section	.text.FDTO_HISTORICAL_ET_populate_state_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_populate_state_dropdown, %function
FDTO_HISTORICAL_ET_populate_state_dropdown:
.LFB12:
	.loc 1 403 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI34:
	add	fp, sp, #4
.LCFI35:
	sub	sp, sp, #8
.LCFI36:
	mov	r3, r0
	strh	r3, [fp, #-12]	@ movhi
	.loc 1 406 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L85
.L86:
	.loc 1 408 0 discriminator 2
	ldrsh	r2, [fp, #-12]
	ldr	r3, .L87
	ldr	r2, [r3, r2, asl #2]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L87+4
	add	r3, r2, r3
	ldr	r0, .L87+8
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 406 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L85:
	.loc 1 406 0 is_stmt 0 discriminator 1
	ldr	r3, .L87+12
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	bcc	.L86
	.loc 1 410 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L88:
	.align	2
.L87:
	.word	g_state_display_order
	.word	States
	.word	GuiVar_ComboBoxItemString
	.word	g_state_display_count
.LFE12:
	.size	FDTO_HISTORICAL_ET_populate_state_dropdown, .-FDTO_HISTORICAL_ET_populate_state_dropdown
	.section	.text.FDTO_HISTORICAL_ET_show_state_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_show_state_dropdown, %function
FDTO_HISTORICAL_ET_show_state_dropdown:
.LFB13:
	.loc 1 413 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI37:
	add	fp, sp, #4
.LCFI38:
	sub	sp, sp, #4
.LCFI39:
	.loc 1 414 0
	bl	HISTORICAL_ET_sort_states
	.loc 1 418 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 420 0
	b	.L90
.L91:
	.loc 1 422 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L90:
	.loc 1 420 0 discriminator 1
	ldr	r3, .L92
	ldr	r2, [fp, #-8]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L92+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L91
	.loc 1 425 0
	ldr	r3, .L92+8
	ldr	r3, [r3, #0]
	ldr	r0, .L92+12
	ldr	r1, .L92+16
	mov	r2, r3
	ldr	r3, [fp, #-8]
	bl	FDTO_COMBOBOX_show
	.loc 1 426 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L93:
	.align	2
.L92:
	.word	g_state_display_order
	.word	g_HISTORICAL_ET_state_index
	.word	g_state_display_count
	.word	733
	.word	FDTO_HISTORICAL_ET_populate_state_dropdown
.LFE13:
	.size	FDTO_HISTORICAL_ET_show_state_dropdown, .-FDTO_HISTORICAL_ET_show_state_dropdown
	.section	.text.FDTO_HISTORICAL_ET_close_state_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_close_state_dropdown, %function
FDTO_HISTORICAL_ET_close_state_dropdown:
.LFB14:
	.loc 1 429 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI40:
	add	fp, sp, #4
.LCFI41:
	.loc 1 430 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L96
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L96+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L95
	.loc 1 432 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L96
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L96+4
	str	r2, [r3, #0]
	.loc 1 434 0
	bl	HISTORICAL_ET_sort_counties
	.loc 1 435 0
	ldr	r3, .L96+8
	ldr	r2, [r3, #0]
	ldr	r3, .L96+12
	str	r2, [r3, #0]
	.loc 1 437 0
	bl	HISTORICAL_ET_sort_cities
	.loc 1 438 0
	ldr	r3, .L96+16
	ldr	r2, [r3, #0]
	ldr	r3, .L96+20
	str	r2, [r3, #0]
	.loc 1 440 0
	bl	HISTORICAL_ET_update_et_numbers
.L95:
	.loc 1 443 0
	bl	FDTO_COMBOBOX_hide
	.loc 1 444 0
	ldmfd	sp!, {fp, pc}
.L97:
	.align	2
.L96:
	.word	g_state_display_order
	.word	g_HISTORICAL_ET_state_index
	.word	g_county_display_order
	.word	g_HISTORICAL_ET_county_index
	.word	g_city_display_order
	.word	g_HISTORICAL_ET_city_index
.LFE14:
	.size	FDTO_HISTORICAL_ET_close_state_dropdown, .-FDTO_HISTORICAL_ET_close_state_dropdown
	.section	.text.FDTO_HISTORICAL_ET_populate_county_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_populate_county_dropdown, %function
FDTO_HISTORICAL_ET_populate_county_dropdown:
.LFB15:
	.loc 1 447 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI42:
	add	fp, sp, #4
.LCFI43:
	sub	sp, sp, #8
.LCFI44:
	mov	r3, r0
	strh	r3, [fp, #-12]	@ movhi
	.loc 1 450 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L99
.L100:
	.loc 1 452 0 discriminator 2
	ldrsh	r2, [fp, #-12]
	ldr	r3, .L101
	ldr	r2, [r3, r2, asl #2]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	ldr	r2, .L101+4
	add	r3, r3, r2
	ldr	r0, .L101+8
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 450 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L99:
	.loc 1 450 0 is_stmt 0 discriminator 1
	ldr	r3, .L101+12
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	bcc	.L100
	.loc 1 454 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L102:
	.align	2
.L101:
	.word	g_county_display_order
	.word	Counties
	.word	GuiVar_ComboBoxItemString
	.word	g_county_display_count
.LFE15:
	.size	FDTO_HISTORICAL_ET_populate_county_dropdown, .-FDTO_HISTORICAL_ET_populate_county_dropdown
	.section	.text.FDTO_HISTORICAL_ET_show_county_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_show_county_dropdown, %function
FDTO_HISTORICAL_ET_show_county_dropdown:
.LFB16:
	.loc 1 457 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI45:
	add	fp, sp, #4
.LCFI46:
	sub	sp, sp, #4
.LCFI47:
	.loc 1 458 0
	bl	HISTORICAL_ET_sort_counties
	.loc 1 462 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 464 0
	b	.L104
.L105:
	.loc 1 466 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L104:
	.loc 1 464 0 discriminator 1
	ldr	r3, .L106
	ldr	r2, [fp, #-8]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L106+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L105
	.loc 1 469 0
	ldr	r3, .L106+8
	ldr	r3, [r3, #0]
	ldr	r0, .L106+12
	ldr	r1, .L106+16
	mov	r2, r3
	ldr	r3, [fp, #-8]
	bl	FDTO_COMBOBOX_show
	.loc 1 470 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L107:
	.align	2
.L106:
	.word	g_county_display_order
	.word	g_HISTORICAL_ET_county_index
	.word	g_county_display_count
	.word	731
	.word	FDTO_HISTORICAL_ET_populate_county_dropdown
.LFE16:
	.size	FDTO_HISTORICAL_ET_show_county_dropdown, .-FDTO_HISTORICAL_ET_show_county_dropdown
	.section	.text.FDTO_HISTORICAL_ET_close_county_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_close_county_dropdown, %function
FDTO_HISTORICAL_ET_close_county_dropdown:
.LFB17:
	.loc 1 473 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI48:
	add	fp, sp, #4
.LCFI49:
	.loc 1 474 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L110
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L110+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L109
	.loc 1 476 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L110
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L110+4
	str	r2, [r3, #0]
	.loc 1 478 0
	bl	HISTORICAL_ET_sort_cities
	.loc 1 479 0
	ldr	r3, .L110+8
	ldr	r2, [r3, #0]
	ldr	r3, .L110+12
	str	r2, [r3, #0]
	.loc 1 481 0
	bl	HISTORICAL_ET_update_et_numbers
.L109:
	.loc 1 484 0
	bl	FDTO_COMBOBOX_hide
	.loc 1 485 0
	ldmfd	sp!, {fp, pc}
.L111:
	.align	2
.L110:
	.word	g_county_display_order
	.word	g_HISTORICAL_ET_county_index
	.word	g_city_display_order
	.word	g_HISTORICAL_ET_city_index
.LFE17:
	.size	FDTO_HISTORICAL_ET_close_county_dropdown, .-FDTO_HISTORICAL_ET_close_county_dropdown
	.section	.text.FDTO_HISTORICAL_ET_populate_city_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_populate_city_dropdown, %function
FDTO_HISTORICAL_ET_populate_city_dropdown:
.LFB18:
	.loc 1 488 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI50:
	add	fp, sp, #4
.LCFI51:
	sub	sp, sp, #8
.LCFI52:
	mov	r3, r0
	strh	r3, [fp, #-12]	@ movhi
	.loc 1 491 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L113
.L114:
	.loc 1 493 0 discriminator 2
	ldrsh	r2, [fp, #-12]
	ldr	r3, .L115
	ldr	r3, [r3, r2, asl #2]
	mov	r2, #76
	mul	r2, r3, r2
	ldr	r3, .L115+4
	add	r3, r2, r3
	ldr	r0, .L115+8
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 491 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L113:
	.loc 1 491 0 is_stmt 0 discriminator 1
	ldr	r3, .L115+12
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	bcc	.L114
	.loc 1 495 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L116:
	.align	2
.L115:
	.word	g_city_display_order
	.word	ET_PredefinedData
	.word	GuiVar_ComboBoxItemString
	.word	g_city_display_count
.LFE18:
	.size	FDTO_HISTORICAL_ET_populate_city_dropdown, .-FDTO_HISTORICAL_ET_populate_city_dropdown
	.section	.text.FDTO_HISTORICAL_ET_show_city_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_show_city_dropdown, %function
FDTO_HISTORICAL_ET_show_city_dropdown:
.LFB19:
	.loc 1 498 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI53:
	add	fp, sp, #4
.LCFI54:
	sub	sp, sp, #4
.LCFI55:
	.loc 1 499 0
	bl	HISTORICAL_ET_sort_cities
	.loc 1 503 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 505 0
	b	.L118
.L119:
	.loc 1 507 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L118:
	.loc 1 505 0 discriminator 1
	ldr	r3, .L120
	ldr	r2, [fp, #-8]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L120+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L119
	.loc 1 510 0
	ldr	r3, .L120+8
	ldr	r3, [r3, #0]
	ldr	r0, .L120+12
	ldr	r1, .L120+16
	mov	r2, r3
	ldr	r3, [fp, #-8]
	bl	FDTO_COMBOBOX_show
	.loc 1 511 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L121:
	.align	2
.L120:
	.word	g_city_display_order
	.word	g_HISTORICAL_ET_city_index
	.word	g_city_display_count
	.word	730
	.word	FDTO_HISTORICAL_ET_populate_city_dropdown
.LFE19:
	.size	FDTO_HISTORICAL_ET_show_city_dropdown, .-FDTO_HISTORICAL_ET_show_city_dropdown
	.section	.text.FDTO_HISTORICAL_ET_close_city_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_close_city_dropdown, %function
FDTO_HISTORICAL_ET_close_city_dropdown:
.LFB20:
	.loc 1 514 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI56:
	add	fp, sp, #4
.LCFI57:
	.loc 1 515 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L123
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L123+4
	str	r2, [r3, #0]
	.loc 1 517 0
	bl	HISTORICAL_ET_update_et_numbers
	.loc 1 519 0
	bl	FDTO_COMBOBOX_hide
	.loc 1 520 0
	ldmfd	sp!, {fp, pc}
.L124:
	.align	2
.L123:
	.word	g_city_display_order
	.word	g_HISTORICAL_ET_city_index
.LFE20:
	.size	FDTO_HISTORICAL_ET_close_city_dropdown, .-FDTO_HISTORICAL_ET_close_city_dropdown
	.section	.text.FDTO_HISTORICAL_ET_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_HISTORICAL_ET_draw_screen
	.type	FDTO_HISTORICAL_ET_draw_screen, %function
FDTO_HISTORICAL_ET_draw_screen:
.LFB21:
	.loc 1 524 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI58:
	add	fp, sp, #4
.LCFI59:
	sub	sp, sp, #8
.LCFI60:
	str	r0, [fp, #-12]
	.loc 1 527 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L126
	.loc 1 529 0
	bl	WEATHER_copy_historical_et_settings_into_GuiVars
	.loc 1 531 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L127
.L126:
	.loc 1 535 0
	ldr	r3, .L128
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L127:
	.loc 1 538 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #27
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 539 0
	bl	GuiLib_Refresh
	.loc 1 540 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L129:
	.align	2
.L128:
	.word	GuiLib_ActiveCursorFieldNo
.LFE21:
	.size	FDTO_HISTORICAL_ET_draw_screen, .-FDTO_HISTORICAL_ET_draw_screen
	.section	.text.HISTORICAL_ET_process_screen,"ax",%progbits
	.align	2
	.global	HISTORICAL_ET_process_screen
	.type	HISTORICAL_ET_process_screen, %function
HISTORICAL_ET_process_screen:
.LFB22:
	.loc 1 543 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI61:
	add	fp, sp, #4
.LCFI62:
	sub	sp, sp, #52
.LCFI63:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	.loc 1 546 0
	ldr	r3, .L213
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r2, .L213+4
	cmp	r3, r2
	beq	.L134
	ldr	r2, .L213+4
	cmp	r3, r2
	bgt	.L137
	ldr	r2, .L213+8
	cmp	r3, r2
	beq	.L132
	ldr	r2, .L213+12
	cmp	r3, r2
	beq	.L133
	b	.L131
.L137:
	cmp	r3, #732
	beq	.L135
	ldr	r2, .L213+16
	cmp	r3, r2
	beq	.L136
	b	.L131
.L132:
	.loc 1 549 0
	ldr	r3, [fp, #-48]
	cmp	r3, #67
	beq	.L138
	.loc 1 549 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	bne	.L139
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #49
	bne	.L139
.L138:
	.loc 1 551 0 is_stmt 1
	ldr	r3, .L213+24
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L213+20
	strh	r2, [r3, #0]	@ movhi
	.loc 1 553 0
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #1
	bne	.L140
	.loc 1 555 0
	ldr	r0, .L213+28
	ldr	r1, .L213+32
	mov	r2, #24
	bl	strlcpy
	b	.L139
.L140:
	.loc 1 557 0
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #2
	bne	.L141
	.loc 1 559 0
	ldr	r0, .L213+36
	ldr	r1, .L213+32
	mov	r2, #24
	bl	strlcpy
	b	.L139
.L141:
	.loc 1 561 0
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #3
	bne	.L139
	.loc 1 563 0
	ldr	r0, .L213+40
	ldr	r1, .L213+32
	mov	r2, #24
	bl	strlcpy
.L139:
	.loc 1 569 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	ldr	r2, .L213+44
	bl	KEYBOARD_process_key
	.loc 1 570 0
	b	.L130
.L135:
	.loc 1 573 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+48
	bl	COMBO_BOX_key_press
	.loc 1 574 0
	b	.L130
.L136:
	.loc 1 577 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	beq	.L143
	.loc 1 577 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-48]
	cmp	r3, #67
	bne	.L144
.L143:
	.loc 1 579 0 is_stmt 1
	bl	good_key_beep
	.loc 1 581 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 582 0
	ldr	r3, .L213+52
	str	r3, [fp, #-20]
	.loc 1 583 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 589 0
	b	.L130
.L144:
	.loc 1 587 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	mov	r1, #0
	bl	COMBO_BOX_key_press
	.loc 1 589 0
	b	.L130
.L134:
	.loc 1 592 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	beq	.L146
	.loc 1 592 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-48]
	cmp	r3, #67
	bne	.L147
.L146:
	.loc 1 594 0 is_stmt 1
	bl	good_key_beep
	.loc 1 596 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 597 0
	ldr	r3, .L213+56
	str	r3, [fp, #-20]
	.loc 1 598 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 604 0
	b	.L130
.L147:
	.loc 1 602 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	mov	r1, #0
	bl	COMBO_BOX_key_press
	.loc 1 604 0
	b	.L130
.L133:
	.loc 1 607 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	beq	.L149
	.loc 1 607 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-48]
	cmp	r3, #67
	bne	.L150
.L149:
	.loc 1 609 0 is_stmt 1
	bl	good_key_beep
	.loc 1 611 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 612 0
	ldr	r3, .L213+60
	str	r3, [fp, #-20]
	.loc 1 613 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 619 0
	b	.L130
.L150:
	.loc 1 617 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	mov	r1, #0
	bl	COMBO_BOX_key_press
	.loc 1 619 0
	b	.L130
.L131:
	.loc 1 622 0
	ldr	r3, [fp, #-48]
	cmp	r3, #3
	beq	.L156
	cmp	r3, #3
	bhi	.L159
	cmp	r3, #1
	beq	.L154
	cmp	r3, #1
	bhi	.L155
	b	.L211
.L159:
	cmp	r3, #80
	beq	.L158
	cmp	r3, #84
	beq	.L158
	cmp	r3, #4
	beq	.L157
	b	.L212
.L155:
	.loc 1 625 0
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L160
.L165:
	.word	.L161
	.word	.L162
	.word	.L163
	.word	.L164
.L161:
	.loc 1 628 0
	bl	good_key_beep
	.loc 1 629 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 630 0
	ldr	r3, .L213+64
	str	r3, [fp, #-20]
	.loc 1 631 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 632 0
	b	.L166
.L162:
	.loc 1 635 0
	ldr	r3, .L213+48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L167
	.loc 1 637 0
	bl	good_key_beep
	.loc 1 638 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 639 0
	ldr	r3, .L213+68
	str	r3, [fp, #-20]
	.loc 1 640 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 652 0
	b	.L166
.L167:
	.loc 1 644 0
	bl	good_key_beep
	.loc 1 646 0
	ldr	r0, .L213+32
	ldr	r1, .L213+28
	mov	r2, #65
	bl	strlcpy
	.loc 1 648 0
	ldr	r3, .L213+24
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 650 0
	mov	r0, #96
	mov	r1, #44
	mov	r2, #24
	mov	r3, #0
	bl	KEYBOARD_draw_keyboard
	.loc 1 652 0
	b	.L166
.L163:
	.loc 1 655 0
	ldr	r3, .L213+48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L169
	.loc 1 657 0
	bl	good_key_beep
	.loc 1 658 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 659 0
	ldr	r3, .L213+72
	str	r3, [fp, #-20]
	.loc 1 660 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 672 0
	b	.L166
.L169:
	.loc 1 664 0
	bl	good_key_beep
	.loc 1 666 0
	ldr	r0, .L213+32
	ldr	r1, .L213+36
	mov	r2, #65
	bl	strlcpy
	.loc 1 668 0
	ldr	r3, .L213+24
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 670 0
	mov	r0, #96
	mov	r1, #58
	mov	r2, #24
	mov	r3, #0
	bl	KEYBOARD_draw_keyboard
	.loc 1 672 0
	b	.L166
.L164:
	.loc 1 675 0
	ldr	r3, .L213+48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L171
	.loc 1 677 0
	bl	good_key_beep
	.loc 1 678 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 679 0
	ldr	r3, .L213+76
	str	r3, [fp, #-20]
	.loc 1 680 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 692 0
	b	.L166
.L171:
	.loc 1 684 0
	bl	good_key_beep
	.loc 1 686 0
	ldr	r0, .L213+32
	ldr	r1, .L213+40
	mov	r2, #65
	bl	strlcpy
	.loc 1 688 0
	ldr	r3, .L213+24
	mov	r2, #3
	str	r2, [r3, #0]
	.loc 1 690 0
	mov	r0, #96
	mov	r1, #72
	mov	r2, #24
	mov	r3, #0
	bl	KEYBOARD_draw_keyboard
	.loc 1 692 0
	b	.L166
.L160:
	.loc 1 695 0
	bl	bad_key_beep
	.loc 1 697 0
	b	.L130
.L166:
	b	.L130
.L158:
	.loc 1 701 0
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #16
	ldrls	pc, [pc, r3, asl #2]
	b	.L173
.L191:
	.word	.L174
	.word	.L175
	.word	.L176
	.word	.L177
	.word	.L178
	.word	.L179
	.word	.L180
	.word	.L181
	.word	.L182
	.word	.L183
	.word	.L184
	.word	.L185
	.word	.L186
	.word	.L187
	.word	.L188
	.word	.L189
	.word	.L190
.L174:
	.loc 1 704 0
	ldr	r0, .L213+48
	bl	process_bool
	.loc 1 706 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 707 0
	b	.L192
.L175:
	.loc 1 710 0
	ldr	r3, .L213+48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L193
	.loc 1 712 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	HISTORICAL_ET_process_state_index
	.loc 1 718 0
	b	.L192
.L193:
	.loc 1 716 0
	bl	bad_key_beep
	.loc 1 718 0
	b	.L192
.L176:
	.loc 1 721 0
	ldr	r3, .L213+48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L195
	.loc 1 723 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	HISTORICAL_ET_process_county_index
	.loc 1 729 0
	b	.L192
.L195:
	.loc 1 727 0
	bl	bad_key_beep
	.loc 1 729 0
	b	.L192
.L177:
	.loc 1 732 0
	ldr	r3, .L213+48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L197
	.loc 1 734 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	HISTORICAL_ET_process_city_index
	.loc 1 740 0
	b	.L192
.L197:
	.loc 1 738 0
	bl	bad_key_beep
	.loc 1 740 0
	b	.L192
.L178:
	.loc 1 743 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L213+80
	mov	r2, #10
	mov	r3, #500
	bl	process_uns32
	.loc 1 744 0
	bl	Refresh_Screen
	.loc 1 745 0
	b	.L192
.L179:
	.loc 1 748 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+84
	bl	HISTORICAL_ET_process_user_et
	.loc 1 749 0
	b	.L192
.L180:
	.loc 1 752 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+88
	bl	HISTORICAL_ET_process_user_et
	.loc 1 753 0
	b	.L192
.L181:
	.loc 1 756 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+92
	bl	HISTORICAL_ET_process_user_et
	.loc 1 757 0
	b	.L192
.L182:
	.loc 1 760 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+96
	bl	HISTORICAL_ET_process_user_et
	.loc 1 761 0
	b	.L192
.L183:
	.loc 1 764 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+100
	bl	HISTORICAL_ET_process_user_et
	.loc 1 765 0
	b	.L192
.L184:
	.loc 1 768 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+104
	bl	HISTORICAL_ET_process_user_et
	.loc 1 769 0
	b	.L192
.L185:
	.loc 1 772 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+108
	bl	HISTORICAL_ET_process_user_et
	.loc 1 773 0
	b	.L192
.L186:
	.loc 1 776 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+112
	bl	HISTORICAL_ET_process_user_et
	.loc 1 777 0
	b	.L192
.L187:
	.loc 1 780 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+116
	bl	HISTORICAL_ET_process_user_et
	.loc 1 781 0
	b	.L192
.L188:
	.loc 1 784 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+120
	bl	HISTORICAL_ET_process_user_et
	.loc 1 785 0
	b	.L192
.L189:
	.loc 1 788 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+124
	bl	HISTORICAL_ET_process_user_et
	.loc 1 789 0
	b	.L192
.L190:
	.loc 1 792 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L213+128
	bl	HISTORICAL_ET_process_user_et
	.loc 1 793 0
	b	.L192
.L173:
	.loc 1 796 0
	bl	bad_key_beep
	.loc 1 798 0
	b	.L130
.L192:
	b	.L130
.L157:
	.loc 1 801 0
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #16
	ldrls	pc, [pc, r3, asl #2]
	b	.L199
.L203:
	.word	.L200
	.word	.L199
	.word	.L199
	.word	.L199
	.word	.L199
	.word	.L201
	.word	.L201
	.word	.L201
	.word	.L201
	.word	.L201
	.word	.L201
	.word	.L202
	.word	.L202
	.word	.L202
	.word	.L202
	.word	.L202
	.word	.L202
.L200:
	.loc 1 804 0
	bl	bad_key_beep
	.loc 1 805 0
	b	.L204
.L201:
	.loc 1 813 0
	mov	r0, #4
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 814 0
	b	.L204
.L202:
	.loc 1 822 0
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #6
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 823 0
	b	.L204
.L199:
	.loc 1 826 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 828 0
	b	.L130
.L204:
	b	.L130
.L211:
	.loc 1 831 0
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #5
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L205
.L208:
	.word	.L206
	.word	.L206
	.word	.L206
	.word	.L206
	.word	.L206
	.word	.L206
	.word	.L207
	.word	.L207
	.word	.L207
	.word	.L207
	.word	.L207
	.word	.L207
.L206:
	.loc 1 839 0
	ldr	r3, .L213+20
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #6
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 840 0
	b	.L209
.L207:
	.loc 1 848 0
	bl	bad_key_beep
	.loc 1 849 0
	b	.L209
.L205:
	.loc 1 852 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 854 0
	b	.L130
.L209:
	b	.L130
.L154:
	.loc 1 857 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 858 0
	b	.L130
.L156:
	.loc 1 861 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 862 0
	b	.L130
.L212:
	.loc 1 865 0
	ldr	r3, [fp, #-48]
	cmp	r3, #67
	bne	.L210
	.loc 1 867 0
	bl	WEATHER_extract_and_store_historical_et_changes_from_GuiVars
	.loc 1 869 0
	ldr	r3, .L213+132
	mov	r2, #4
	str	r2, [r3, #0]
.L210:
	.loc 1 872 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L130:
	.loc 1 875 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L214:
	.align	2
.L213:
	.word	GuiLib_CurStructureNdx
	.word	731
	.word	609
	.word	730
	.word	733
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_cursor_pos_of_direct_entry_field
	.word	GuiVar_HistoricalETUserState
	.word	GuiVar_GroupName
	.word	GuiVar_HistoricalETUserCounty
	.word	GuiVar_HistoricalETUserCity
	.word	FDTO_HISTORICAL_ET_draw_screen
	.word	GuiVar_HistoricalETUseYourOwn
	.word	FDTO_HISTORICAL_ET_close_state_dropdown
	.word	FDTO_HISTORICAL_ET_close_county_dropdown
	.word	FDTO_HISTORICAL_ET_close_city_dropdown
	.word	FDTO_HISTORICAL_ET_show_source_dropdown
	.word	FDTO_HISTORICAL_ET_show_state_dropdown
	.word	FDTO_HISTORICAL_ET_show_county_dropdown
	.word	FDTO_HISTORICAL_ET_show_city_dropdown
	.word	GuiVar_ETGageMaxPercent
	.word	GuiVar_HistoricalETUser1
	.word	GuiVar_HistoricalETUser2
	.word	GuiVar_HistoricalETUser3
	.word	GuiVar_HistoricalETUser4
	.word	GuiVar_HistoricalETUser5
	.word	GuiVar_HistoricalETUser6
	.word	GuiVar_HistoricalETUser7
	.word	GuiVar_HistoricalETUser8
	.word	GuiVar_HistoricalETUser9
	.word	GuiVar_HistoricalETUser10
	.word	GuiVar_HistoricalETUser11
	.word	GuiVar_HistoricalETUser12
	.word	GuiVar_MenuScreenToShow
.LFE22:
	.size	HISTORICAL_ET_process_screen, .-HISTORICAL_ET_process_screen
	.section	.bss.CitiesCopy.3880,"aw",%nobits
	.align	2
	.type	CitiesCopy.3880, %object
	.size	CitiesCopy.3880, 3696
CitiesCopy.3880:
	.space	3696
	.section	.bss.CountiesCopy.3864,"aw",%nobits
	.align	2
	.type	CountiesCopy.3864, %object
	.size	CountiesCopy.3864, 480
CountiesCopy.3864:
	.space	480
	.section	.bss.StatesCopy.3849,"aw",%nobits
	.align	2
	.type	StatesCopy.3849, %object
	.size	StatesCopy.3849, 312
StatesCopy.3849:
	.space	312
	.section	.bss.pivot.3825,"aw",%nobits
	.align	2
	.type	pivot.3825, %object
	.size	pivot.3825, 24
pivot.3825:
	.space	24
	.section	.bss.swapArray.3824,"aw",%nobits
	.align	2
	.type	swapArray.3824, %object
	.size	swapArray.3824, 24
swapArray.3824:
	.space	24
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI14-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI17-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI20-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI23-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI26-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI29-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI32-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI34-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI35-.LCFI34
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI37-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI40-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI42-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI45-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI48-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI50-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI51-.LCFI50
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI53-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI54-.LCFI53
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI56-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI57-.LCFI56
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI58-.LFB21
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI59-.LCFI58
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI61-.LFB22
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI62-.LCFI61
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE44:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/et_data.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_historical_et_setup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xdca
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF139
	.byte	0x1
	.4byte	.LASF140
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x53
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x67
	.4byte	0x77
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x5
	.byte	0x4
	.4byte	0x9d
	.uleb128 0x6
	.4byte	0xa4
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x8
	.4byte	0x33
	.4byte	0xbb
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xe0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x7e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF15
	.byte	0x3
	.byte	0x80
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x82
	.4byte	0xbb
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x172
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x7b
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x83
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x86
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x88
	.4byte	0x183
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x8d
	.4byte	0x195
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x92
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x96
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9c
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x17e
	.uleb128 0xd
	.4byte	0x17e
	.byte	0
	.uleb128 0xe
	.4byte	0x48
	.uleb128 0x5
	.byte	0x4
	.4byte	0x172
	.uleb128 0xc
	.byte	0x1
	.4byte	0x195
	.uleb128 0xd
	.4byte	0xe0
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x189
	.uleb128 0x3
	.4byte	.LASF26
	.byte	0x4
	.byte	0x9e
	.4byte	0xeb
	.uleb128 0xa
	.byte	0x1c
	.byte	0x5
	.byte	0x2d
	.4byte	0x1cb
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x5
	.byte	0x2f
	.4byte	0x1cb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x5
	.byte	0x31
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x1db
	.uleb128 0x9
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x5
	.byte	0x33
	.4byte	0x1a6
	.uleb128 0xa
	.byte	0x4c
	.byte	0x5
	.byte	0x35
	.4byte	0x219
	.uleb128 0xb
	.4byte	.LASF30
	.byte	0x5
	.byte	0x37
	.4byte	0x1cb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF31
	.byte	0x5
	.byte	0x39
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x5
	.byte	0x3c
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x8
	.4byte	0x5a
	.4byte	0x229
	.uleb128 0x9
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x3
	.4byte	.LASF33
	.byte	0x5
	.byte	0x3e
	.4byte	0x1e6
	.uleb128 0xf
	.4byte	.LASF141
	.byte	0x1
	.byte	0x5b
	.byte	0x1
	.4byte	0x6c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x2b9
	.uleb128 0x10
	.4byte	.LASF34
	.byte	0x1
	.byte	0x5b
	.4byte	0x2b9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x10
	.4byte	.LASF35
	.byte	0x1
	.byte	0x5b
	.4byte	0x2bf
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x10
	.4byte	.LASF36
	.byte	0x1
	.byte	0x5b
	.4byte	0x2bf
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x11
	.4byte	.LASF37
	.byte	0x1
	.byte	0x5d
	.4byte	0x1cb
	.byte	0x5
	.byte	0x3
	.4byte	swapArray.3824
	.uleb128 0x11
	.4byte	.LASF38
	.byte	0x1
	.byte	0x5f
	.4byte	0x1cb
	.byte	0x5
	.byte	0x3
	.4byte	pivot.3825
	.uleb128 0x11
	.4byte	.LASF39
	.byte	0x1
	.byte	0x61
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x11
	.4byte	.LASF40
	.byte	0x1
	.byte	0x62
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1cb
	.uleb128 0xe
	.4byte	0x6c
	.uleb128 0x12
	.4byte	.LASF42
	.byte	0x1
	.byte	0x87
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x315
	.uleb128 0x10
	.4byte	.LASF34
	.byte	0x1
	.byte	0x87
	.4byte	0x2b9
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x10
	.4byte	.LASF35
	.byte	0x1
	.byte	0x87
	.4byte	0x2bf
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x10
	.4byte	.LASF36
	.byte	0x1
	.byte	0x87
	.4byte	0x2bf
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x11
	.4byte	.LASF41
	.byte	0x1
	.byte	0x89
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x13
	.4byte	.LASF63
	.byte	0x1
	.byte	0x94
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x12
	.4byte	.LASF43
	.byte	0x1
	.byte	0xad
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x36b
	.uleb128 0x11
	.4byte	.LASF44
	.byte	0x1
	.byte	0xaf
	.4byte	0x36b
	.byte	0x5
	.byte	0x3
	.4byte	StatesCopy.3849
	.uleb128 0x14
	.ascii	"i\000"
	.byte	0x1
	.byte	0xb1
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.ascii	"j\000"
	.byte	0x1
	.byte	0xb1
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x381
	.uleb128 0x9
	.4byte	0x25
	.byte	0xc
	.uleb128 0x9
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x12
	.4byte	.LASF45
	.byte	0x1
	.byte	0xcb
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x3d1
	.uleb128 0x11
	.4byte	.LASF46
	.byte	0x1
	.byte	0xcd
	.4byte	0x3d1
	.byte	0x5
	.byte	0x3
	.4byte	CountiesCopy.3864
	.uleb128 0x11
	.4byte	.LASF47
	.byte	0x1
	.byte	0xcf
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.ascii	"i\000"
	.byte	0x1
	.byte	0xd1
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.ascii	"j\000"
	.byte	0x1
	.byte	0xd1
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x3e7
	.uleb128 0x9
	.4byte	0x25
	.byte	0x13
	.uleb128 0x9
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x12
	.4byte	.LASF48
	.byte	0x1
	.byte	0xf1
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x437
	.uleb128 0x11
	.4byte	.LASF49
	.byte	0x1
	.byte	0xf3
	.4byte	0x437
	.byte	0x5
	.byte	0x3
	.4byte	CitiesCopy.3880
	.uleb128 0x11
	.4byte	.LASF47
	.byte	0x1
	.byte	0xf5
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.ascii	"i\000"
	.byte	0x1
	.byte	0xf7
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.ascii	"j\000"
	.byte	0x1
	.byte	0xf7
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x44d
	.uleb128 0x9
	.4byte	0x25
	.byte	0x99
	.uleb128 0x9
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x15
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x116
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x4ce
	.uleb128 0x16
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x116
	.4byte	0x4ce
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x116
	.4byte	0x4d3
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x116
	.4byte	0x4d9
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x16
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x116
	.4byte	0x4ce
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x16
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x116
	.4byte	0x4ce
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x17
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x118
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x11a
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xe
	.4byte	0x5a
	.uleb128 0x5
	.byte	0x4
	.4byte	0x5a
	.uleb128 0x5
	.byte	0x4
	.4byte	0x4ce
	.uleb128 0x15
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x14c
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x508
	.uleb128 0x16
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x14c
	.4byte	0x4ce
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x15d
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x531
	.uleb128 0x16
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x15d
	.4byte	0x4ce
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x174
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x55a
	.uleb128 0x16
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x174
	.4byte	0x4ce
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x187
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x592
	.uleb128 0x16
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x187
	.4byte	0x4ce
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x187
	.4byte	0x592
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x598
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF62
	.uleb128 0x19
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x18d
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.uleb128 0x15
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x192
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x5ea
	.uleb128 0x16
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x192
	.4byte	0x17e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x194
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x19c
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x613
	.uleb128 0x17
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x1a0
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x1ac
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.uleb128 0x15
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x1be
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x65e
	.uleb128 0x16
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x1be
	.4byte	0x17e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x1c0
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x1c8
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x687
	.uleb128 0x17
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x1cc
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x1d8
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.uleb128 0x15
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x1e7
	.byte	0x1
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0x6d2
	.uleb128 0x16
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x1e7
	.4byte	0x17e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x1e9
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x1f1
	.byte	0x1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0x6fb
	.uleb128 0x17
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x1f5
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x201
	.byte	0x1
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x20b
	.byte	0x1
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0x749
	.uleb128 0x16
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x20b
	.4byte	0x749
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x20d
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x8c
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x21e
	.byte	0x1
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0x787
	.uleb128 0x16
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x21e
	.4byte	0xe0
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x18
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x220
	.4byte	0x19b
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x797
	.uleb128 0x9
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF83
	.byte	0x6
	.2byte	0x16a
	.4byte	0x787
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF84
	.byte	0x6
	.2byte	0x1a5
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x7c3
	.uleb128 0x9
	.4byte	0x25
	.byte	0x40
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF85
	.byte	0x6
	.2byte	0x1fc
	.4byte	0x7b3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF86
	.byte	0x6
	.2byte	0x235
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF87
	.byte	0x6
	.2byte	0x236
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF88
	.byte	0x6
	.2byte	0x237
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF89
	.byte	0x6
	.2byte	0x238
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF90
	.byte	0x6
	.2byte	0x239
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF91
	.byte	0x6
	.2byte	0x23a
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF92
	.byte	0x6
	.2byte	0x23b
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF93
	.byte	0x6
	.2byte	0x23c
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF94
	.byte	0x6
	.2byte	0x23d
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF95
	.byte	0x6
	.2byte	0x23e
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF96
	.byte	0x6
	.2byte	0x23f
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF97
	.byte	0x6
	.2byte	0x240
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x889
	.uleb128 0x9
	.4byte	0x25
	.byte	0x18
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF98
	.byte	0x6
	.2byte	0x241
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF99
	.byte	0x6
	.2byte	0x242
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF100
	.byte	0x6
	.2byte	0x243
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF101
	.byte	0x6
	.2byte	0x244
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF102
	.byte	0x6
	.2byte	0x245
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF103
	.byte	0x6
	.2byte	0x246
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF104
	.byte	0x6
	.2byte	0x247
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF105
	.byte	0x6
	.2byte	0x248
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF106
	.byte	0x6
	.2byte	0x249
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF107
	.byte	0x6
	.2byte	0x24a
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF108
	.byte	0x6
	.2byte	0x24b
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF109
	.byte	0x6
	.2byte	0x24c
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF110
	.byte	0x6
	.2byte	0x24d
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF111
	.byte	0x6
	.2byte	0x24e
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF112
	.byte	0x6
	.2byte	0x24f
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF113
	.byte	0x6
	.2byte	0x250
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF114
	.byte	0x6
	.2byte	0x251
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF115
	.byte	0x6
	.2byte	0x252
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF116
	.byte	0x6
	.2byte	0x253
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF117
	.byte	0x6
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF118
	.byte	0x7
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF119
	.byte	0x7
	.2byte	0x132
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF120
	.byte	0x8
	.byte	0x30
	.4byte	0x9ce
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xab
	.uleb128 0x11
	.4byte	.LASF121
	.byte	0x8
	.byte	0x34
	.4byte	0x9e4
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xab
	.uleb128 0x11
	.4byte	.LASF122
	.byte	0x8
	.byte	0x36
	.4byte	0x9fa
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xab
	.uleb128 0x11
	.4byte	.LASF123
	.byte	0x8
	.byte	0x38
	.4byte	0xa10
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xab
	.uleb128 0x1c
	.4byte	.LASF124
	.byte	0x9
	.byte	0x14
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF125
	.byte	0x9
	.byte	0x16
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF126
	.byte	0x9
	.byte	0x18
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF127
	.byte	0x5
	.byte	0x43
	.4byte	0xa49
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0x36b
	.uleb128 0x8
	.4byte	0x1db
	.4byte	0xa5e
	.uleb128 0x9
	.4byte	0x25
	.byte	0x40
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF128
	.byte	0x5
	.byte	0x45
	.4byte	0xa6b
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0xa4e
	.uleb128 0x8
	.4byte	0x229
	.4byte	0xa80
	.uleb128 0x9
	.4byte	0x25
	.byte	0x99
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF129
	.byte	0x5
	.byte	0x47
	.4byte	0xa8d
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0xa70
	.uleb128 0x8
	.4byte	0x5a
	.4byte	0xaa2
	.uleb128 0x9
	.4byte	0x25
	.byte	0xc
	.byte	0
	.uleb128 0x11
	.4byte	.LASF130
	.byte	0x1
	.byte	0x40
	.4byte	0xa92
	.byte	0x5
	.byte	0x3
	.4byte	g_state_display_order
	.uleb128 0x8
	.4byte	0x5a
	.4byte	0xac3
	.uleb128 0x9
	.4byte	0x25
	.byte	0x40
	.byte	0
	.uleb128 0x11
	.4byte	.LASF131
	.byte	0x1
	.byte	0x41
	.4byte	0xab3
	.byte	0x5
	.byte	0x3
	.4byte	g_county_display_order
	.uleb128 0x8
	.4byte	0x5a
	.4byte	0xae4
	.uleb128 0x9
	.4byte	0x25
	.byte	0x99
	.byte	0
	.uleb128 0x11
	.4byte	.LASF132
	.byte	0x1
	.byte	0x42
	.4byte	0xad4
	.byte	0x5
	.byte	0x3
	.4byte	g_city_display_order
	.uleb128 0x11
	.4byte	.LASF133
	.byte	0x1
	.byte	0x44
	.4byte	0x5a
	.byte	0x5
	.byte	0x3
	.4byte	g_state_display_count
	.uleb128 0x11
	.4byte	.LASF134
	.byte	0x1
	.byte	0x45
	.4byte	0x5a
	.byte	0x5
	.byte	0x3
	.4byte	g_county_display_count
	.uleb128 0x11
	.4byte	.LASF135
	.byte	0x1
	.byte	0x46
	.4byte	0x5a
	.byte	0x5
	.byte	0x3
	.4byte	g_city_display_count
	.uleb128 0x11
	.4byte	.LASF136
	.byte	0x1
	.byte	0x48
	.4byte	0x8c
	.byte	0x5
	.byte	0x3
	.4byte	g_counties_sorted
	.uleb128 0x11
	.4byte	.LASF137
	.byte	0x1
	.byte	0x49
	.4byte	0x8c
	.byte	0x5
	.byte	0x3
	.4byte	g_cities_sorted
	.uleb128 0x11
	.4byte	.LASF138
	.byte	0x1
	.byte	0x50
	.4byte	0x5a
	.byte	0x5
	.byte	0x3
	.4byte	g_cursor_pos_of_direct_entry_field
	.uleb128 0x1b
	.4byte	.LASF83
	.byte	0x6
	.2byte	0x16a
	.4byte	0x787
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF84
	.byte	0x6
	.2byte	0x1a5
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF85
	.byte	0x6
	.2byte	0x1fc
	.4byte	0x7b3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF86
	.byte	0x6
	.2byte	0x235
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF87
	.byte	0x6
	.2byte	0x236
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF88
	.byte	0x6
	.2byte	0x237
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF89
	.byte	0x6
	.2byte	0x238
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF90
	.byte	0x6
	.2byte	0x239
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF91
	.byte	0x6
	.2byte	0x23a
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF92
	.byte	0x6
	.2byte	0x23b
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF93
	.byte	0x6
	.2byte	0x23c
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF94
	.byte	0x6
	.2byte	0x23d
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF95
	.byte	0x6
	.2byte	0x23e
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF96
	.byte	0x6
	.2byte	0x23f
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF97
	.byte	0x6
	.2byte	0x240
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF98
	.byte	0x6
	.2byte	0x241
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF99
	.byte	0x6
	.2byte	0x242
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF100
	.byte	0x6
	.2byte	0x243
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF101
	.byte	0x6
	.2byte	0x244
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF102
	.byte	0x6
	.2byte	0x245
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF103
	.byte	0x6
	.2byte	0x246
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF104
	.byte	0x6
	.2byte	0x247
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF105
	.byte	0x6
	.2byte	0x248
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF106
	.byte	0x6
	.2byte	0x249
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF107
	.byte	0x6
	.2byte	0x24a
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF108
	.byte	0x6
	.2byte	0x24b
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF109
	.byte	0x6
	.2byte	0x24c
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF110
	.byte	0x6
	.2byte	0x24d
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF111
	.byte	0x6
	.2byte	0x24e
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF112
	.byte	0x6
	.2byte	0x24f
	.4byte	0x598
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF113
	.byte	0x6
	.2byte	0x250
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF114
	.byte	0x6
	.2byte	0x251
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF115
	.byte	0x6
	.2byte	0x252
	.4byte	0x879
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF116
	.byte	0x6
	.2byte	0x253
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF117
	.byte	0x6
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF118
	.byte	0x7
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF119
	.byte	0x7
	.2byte	0x132
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF124
	.byte	0x1
	.byte	0x52
	.4byte	0x5a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_HISTORICAL_ET_state_index
	.uleb128 0x1d
	.4byte	.LASF125
	.byte	0x1
	.byte	0x54
	.4byte	0x5a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_HISTORICAL_ET_county_index
	.uleb128 0x1d
	.4byte	.LASF126
	.byte	0x1
	.byte	0x56
	.4byte	0x5a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_HISTORICAL_ET_city_index
	.uleb128 0x1c
	.4byte	.LASF127
	.byte	0x5
	.byte	0x43
	.4byte	0xda4
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0x36b
	.uleb128 0x1c
	.4byte	.LASF128
	.byte	0x5
	.byte	0x45
	.4byte	0xdb6
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0xa4e
	.uleb128 0x1c
	.4byte	.LASF129
	.byte	0x5
	.byte	0x47
	.4byte	0xdc8
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0xa70
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI18
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI21
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI24
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI33
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI35
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI38
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI41
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI43
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI46
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI49
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI51
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI54
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI57
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI58
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI59
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI61
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI62
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xcc
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF29:
	.ascii	"COUNTY_INFO\000"
.LASF127:
	.ascii	"States\000"
.LASF9:
	.ascii	"INT_32\000"
.LASF77:
	.ascii	"FDTO_HISTORICAL_ET_close_city_dropdown\000"
.LASF30:
	.ascii	"city\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF135:
	.ascii	"g_city_display_count\000"
.LASF99:
	.ascii	"GuiVar_HistoricalETCounty\000"
.LASF49:
	.ascii	"CitiesCopy\000"
.LASF61:
	.ascii	"et_ptr\000"
.LASF56:
	.ascii	"lindex\000"
.LASF123:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF33:
	.ascii	"CITY_INFO\000"
.LASF124:
	.ascii	"g_HISTORICAL_ET_state_index\000"
.LASF52:
	.ascii	"var_ptr\000"
.LASF116:
	.ascii	"GuiVar_HistoricalETUseYourOwn\000"
.LASF22:
	.ascii	"_04_func_ptr\000"
.LASF62:
	.ascii	"float\000"
.LASF18:
	.ascii	"_02_menu\000"
.LASF41:
	.ascii	"split\000"
.LASF98:
	.ascii	"GuiVar_HistoricalETCity\000"
.LASF69:
	.ascii	"FDTO_HISTORICAL_ET_close_state_dropdown\000"
.LASF53:
	.ascii	"pdisplay_order_array\000"
.LASF32:
	.ascii	"ET_100u\000"
.LASF16:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF36:
	.ascii	"pupper_bound\000"
.LASF19:
	.ascii	"_03_structure_to_draw\000"
.LASF126:
	.ascii	"g_HISTORICAL_ET_city_index\000"
.LASF86:
	.ascii	"GuiVar_HistoricalET1\000"
.LASF90:
	.ascii	"GuiVar_HistoricalET2\000"
.LASF91:
	.ascii	"GuiVar_HistoricalET3\000"
.LASF92:
	.ascii	"GuiVar_HistoricalET4\000"
.LASF93:
	.ascii	"GuiVar_HistoricalET5\000"
.LASF94:
	.ascii	"GuiVar_HistoricalET6\000"
.LASF95:
	.ascii	"GuiVar_HistoricalET7\000"
.LASF96:
	.ascii	"GuiVar_HistoricalET8\000"
.LASF97:
	.ascii	"GuiVar_HistoricalET9\000"
.LASF132:
	.ascii	"g_city_display_order\000"
.LASF44:
	.ascii	"StatesCopy\000"
.LASF31:
	.ascii	"county_index\000"
.LASF24:
	.ascii	"_07_u32_argument2\000"
.LASF57:
	.ascii	"HISTORICAL_ET_process_state_index\000"
.LASF84:
	.ascii	"GuiVar_ETGageMaxPercent\000"
.LASF23:
	.ascii	"_06_u32_argument1\000"
.LASF119:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF113:
	.ascii	"GuiVar_HistoricalETUserCity\000"
.LASF64:
	.ascii	"FDTO_HISTORICAL_ET_show_source_dropdown\000"
.LASF67:
	.ascii	"FDTO_HISTORICAL_ET_show_state_dropdown\000"
.LASF70:
	.ascii	"FDTO_HISTORICAL_ET_populate_county_dropdown\000"
.LASF76:
	.ascii	"lcity_to_display\000"
.LASF25:
	.ascii	"_08_screen_to_draw\000"
.LASF1:
	.ascii	"char\000"
.LASF114:
	.ascii	"GuiVar_HistoricalETUserCounty\000"
.LASF54:
	.ascii	"pmin\000"
.LASF117:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF82:
	.ascii	"pkey_event\000"
.LASF28:
	.ascii	"state_index\000"
.LASF17:
	.ascii	"_01_command\000"
.LASF78:
	.ascii	"pcomplete_redraw\000"
.LASF72:
	.ascii	"lcounty_to_display\000"
.LASF102:
	.ascii	"GuiVar_HistoricalETUser10\000"
.LASF103:
	.ascii	"GuiVar_HistoricalETUser11\000"
.LASF104:
	.ascii	"GuiVar_HistoricalETUser12\000"
.LASF40:
	.ascii	"right\000"
.LASF11:
	.ascii	"long long int\000"
.LASF48:
	.ascii	"HISTORICAL_ET_sort_cities\000"
.LASF134:
	.ascii	"g_county_display_count\000"
.LASF128:
	.ascii	"Counties\000"
.LASF100:
	.ascii	"GuiVar_HistoricalETState\000"
.LASF20:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF101:
	.ascii	"GuiVar_HistoricalETUser1\000"
.LASF81:
	.ascii	"HISTORICAL_ET_process_screen\000"
.LASF66:
	.ascii	"pindex_0\000"
.LASF63:
	.ascii	"HISTORICAL_ET_update_et_numbers\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF47:
	.ascii	"sortCount\000"
.LASF74:
	.ascii	"FDTO_HISTORICAL_ET_populate_city_dropdown\000"
.LASF15:
	.ascii	"repeats\000"
.LASF27:
	.ascii	"county\000"
.LASF35:
	.ascii	"plower_bound\000"
.LASF85:
	.ascii	"GuiVar_GroupName\000"
.LASF51:
	.ascii	"pkeycode\000"
.LASF46:
	.ascii	"CountiesCopy\000"
.LASF136:
	.ascii	"g_counties_sorted\000"
.LASF71:
	.ascii	"FDTO_HISTORICAL_ET_show_county_dropdown\000"
.LASF129:
	.ascii	"ET_PredefinedData\000"
.LASF118:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF42:
	.ascii	"HISTORICAL_ET_qsort\000"
.LASF105:
	.ascii	"GuiVar_HistoricalETUser2\000"
.LASF106:
	.ascii	"GuiVar_HistoricalETUser3\000"
.LASF107:
	.ascii	"GuiVar_HistoricalETUser4\000"
.LASF108:
	.ascii	"GuiVar_HistoricalETUser5\000"
.LASF109:
	.ascii	"GuiVar_HistoricalETUser6\000"
.LASF110:
	.ascii	"GuiVar_HistoricalETUser7\000"
.LASF111:
	.ascii	"GuiVar_HistoricalETUser8\000"
.LASF112:
	.ascii	"GuiVar_HistoricalETUser9\000"
.LASF5:
	.ascii	"short int\000"
.LASF115:
	.ascii	"GuiVar_HistoricalETUserState\000"
.LASF13:
	.ascii	"long int\000"
.LASF38:
	.ascii	"pivot\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF133:
	.ascii	"g_state_display_count\000"
.LASF75:
	.ascii	"FDTO_HISTORICAL_ET_show_city_dropdown\000"
.LASF131:
	.ascii	"g_county_display_order\000"
.LASF39:
	.ascii	"left\000"
.LASF139:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF34:
	.ascii	"strArray\000"
.LASF43:
	.ascii	"HISTORICAL_ET_sort_states\000"
.LASF50:
	.ascii	"HISTORICAL_ET_process_reference_et_location\000"
.LASF80:
	.ascii	"FDTO_HISTORICAL_ET_draw_screen\000"
.LASF140:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_historical_et_setup.c\000"
.LASF45:
	.ascii	"HISTORICAL_ET_sort_counties\000"
.LASF65:
	.ascii	"FDTO_HISTORICAL_ET_populate_state_dropdown\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF121:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF60:
	.ascii	"HISTORICAL_ET_process_user_et\000"
.LASF87:
	.ascii	"GuiVar_HistoricalET10\000"
.LASF88:
	.ascii	"GuiVar_HistoricalET11\000"
.LASF89:
	.ascii	"GuiVar_HistoricalET12\000"
.LASF125:
	.ascii	"g_HISTORICAL_ET_county_index\000"
.LASF141:
	.ascii	"HISTORICAL_ET_qsort_partition\000"
.LASF59:
	.ascii	"HISTORICAL_ET_process_city_index\000"
.LASF55:
	.ascii	"pmax\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF3:
	.ascii	"signed char\000"
.LASF130:
	.ascii	"g_state_display_order\000"
.LASF137:
	.ascii	"g_cities_sorted\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF83:
	.ascii	"GuiVar_ComboBoxItemString\000"
.LASF79:
	.ascii	"lcursor_to_select\000"
.LASF120:
	.ascii	"GuiFont_LanguageActive\000"
.LASF138:
	.ascii	"g_cursor_pos_of_direct_entry_field\000"
.LASF26:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF37:
	.ascii	"swapArray\000"
.LASF58:
	.ascii	"HISTORICAL_ET_process_county_index\000"
.LASF21:
	.ascii	"key_process_func_ptr\000"
.LASF14:
	.ascii	"keycode\000"
.LASF73:
	.ascii	"FDTO_HISTORICAL_ET_close_county_dropdown\000"
.LASF68:
	.ascii	"lstate_to_display\000"
.LASF122:
	.ascii	"GuiFont_DecimalChar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
