	.file	"budgets.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	g_BUDGET_using_et_calculate_budget
	.section	.bss.g_BUDGET_using_et_calculate_budget,"aw",%nobits
	.align	2
	.type	g_BUDGET_using_et_calculate_budget, %object
	.size	g_BUDGET_using_et_calculate_budget, 4
g_BUDGET_using_et_calculate_budget:
	.space	4
	.section	.rodata.SQUARE_FOOTAGE_CONSTANT,"a",%progbits
	.align	2
	.type	SQUARE_FOOTAGE_CONSTANT, %object
	.size	SQUARE_FOOTAGE_CONSTANT, 4
SQUARE_FOOTAGE_CONSTANT:
	.word	1119911936
	.section	.rodata.MINUTES_PER_HOUR,"a",%progbits
	.align	2
	.type	MINUTES_PER_HOUR, %object
	.size	MINUTES_PER_HOUR, 4
MINUTES_PER_HOUR:
	.word	1114636288
	.section	.rodata.percip_100000u,"a",%progbits
	.align	2
	.type	percip_100000u, %object
	.size	percip_100000u, 4
percip_100000u:
	.word	1203982336
	.section	.text.update_running_dtcs,"ax",%progbits
	.align	2
	.type	update_running_dtcs, %function
update_running_dtcs:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/budgets/budgets.c"
	.loc 1 75 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #28
.LCFI2:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	.loc 1 78 0
	ldr	r3, [fp, #-28]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-24]
	strh	r2, [r3, #4]	@ movhi
	.loc 1 81 0
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #4]
	mov	r0, r3
	sub	r1, fp, #8
	sub	r2, fp, #12
	sub	r3, fp, #16
	sub	ip, fp, #20
	str	ip, [sp, #0]
	bl	DateToDMY
	.loc 1 83 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-24]
	strh	r2, [r3, #8]	@ movhi
	.loc 1 85 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-24]
	strh	r2, [r3, #6]	@ movhi
	.loc 1 87 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-24]
	strh	r2, [r3, #10]	@ movhi
	.loc 1 89 0
	ldr	r3, [fp, #-20]
	and	r2, r3, #255
	ldr	r3, [fp, #-24]
	strb	r2, [r3, #18]
	.loc 1 90 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE0:
	.size	update_running_dtcs, .-update_running_dtcs
	.section	.text.nm_handle_budget_reset_record,"ax",%progbits
	.align	2
	.type	nm_handle_budget_reset_record, %function
nm_handle_budget_reset_record:
.LFB1:
	.loc 1 101 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #12
.LCFI5:
	str	r0, [fp, #-16]
	.loc 1 108 0
	ldr	r1, .L3
	ldr	r2, [fp, #-16]
	mov	r3, #44
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	add	r3, r3, #14080
	add	r3, r3, #48
	str	r3, [fp, #-8]
	.loc 1 110 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #24]
	.loc 1 111 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 112 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #36]
	.loc 1 116 0
	ldr	r3, [fp, #-16]
	mov	r2, #472
	mul	r3, r2, r3
	add	r2, r3, #380
	ldr	r3, .L3
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 118 0
	ldr	r0, [fp, #-12]
	mov	r1, #0
	mov	r2, #96
	bl	memset
	.loc 1 119 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L4:
	.align	2
.L3:
	.word	poc_preserves
.LFE1:
	.size	nm_handle_budget_reset_record, .-nm_handle_budget_reset_record
	.section	.text.nm_reset_meter_read_dates,"ax",%progbits
	.align	2
	.type	nm_reset_meter_read_dates, %function
nm_reset_meter_read_dates:
.LFB2:
	.loc 1 125 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #44
.LCFI8:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	str	r3, [fp, #-32]
	.loc 1 132 0
	bl	FLOWSENSE_get_controller_index
	str	r0, [fp, #-12]
	.loc 1 133 0
	ldr	r0, [fp, #-20]
	mov	r1, #2
	bl	SYSTEM_get_change_bits_ptr
	str	r0, [fp, #-16]
	.loc 1 135 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L6
.L8:
	.loc 1 137 0
	mov	r0, #1
	ldr	r1, [fp, #-24]
	ldr	r2, [fp, #-28]
	bl	DMYToDate
	mov	r3, r0
	mov	r2, #2
	str	r2, [sp, #0]
	ldr	r2, [fp, #-12]
	str	r2, [sp, #4]
	mov	r2, #1
	str	r2, [sp, #8]
	ldr	r2, [fp, #-16]
	str	r2, [sp, #12]
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-8]
	mov	r2, r3
	mov	r3, #1
	bl	nm_SYSTEM_set_budget_period
	.loc 1 145 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-32]
	add	r3, r2, r3
	str	r3, [fp, #-24]
	.loc 1 146 0
	ldr	r3, [fp, #-24]
	cmp	r3, #12
	bls	.L7
	.loc 1 148 0
	ldr	r3, [fp, #-24]
	sub	r3, r3, #12
	str	r3, [fp, #-24]
	.loc 1 149 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
.L7:
	.loc 1 135 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L6:
	.loc 1 135 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #23
	bls	.L8
	.loc 1 152 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE2:
	.size	nm_reset_meter_read_dates, .-nm_reset_meter_read_dates
	.section	.text.nm_reset_budget_values,"ax",%progbits
	.align	2
	.type	nm_reset_budget_values, %function
nm_reset_budget_values:
.LFB3:
	.loc 1 157 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #24
.LCFI11:
	str	r0, [fp, #-28]
	.loc 1 165 0
	ldr	r0, [fp, #-28]
	bl	nm_GROUP_get_group_ID
	str	r0, [fp, #-16]
	.loc 1 170 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L10
.L14:
.LBB2:
	.loc 1 172 0
	ldr	r3, [fp, #-8]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L15
	add	r3, r2, r3
	str	r3, [fp, #-20]
	.loc 1 174 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L11
	.loc 1 175 0 discriminator 1
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #24]
	ldr	r2, [r3, #0]
	.loc 1 174 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L11
	.loc 1 177 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	mov	r0, r3
	bl	POC_get_index_for_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-24]
	.loc 1 181 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L12
.L13:
	.loc 1 183 0 discriminator 2
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-12]
	mov	r2, #0
	bl	POC_set_budget
	.loc 1 181 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L12:
	.loc 1 181 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #23
	bls	.L13
	.loc 1 187 0 is_stmt 1
	ldr	r0, [fp, #-8]
	bl	nm_handle_budget_reset_record
.L11:
.LBE2:
	.loc 1 170 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L10:
	.loc 1 170 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L14
	.loc 1 190 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L16:
	.align	2
.L15:
	.word	poc_preserves+20
.LFE3:
	.size	nm_reset_budget_values, .-nm_reset_budget_values
	.section	.text.getNumStarts,"ax",%progbits
	.align	2
	.type	getNumStarts, %function
getNumStarts:
.LFB4:
	.loc 1 205 0
	@ args = 4, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #44
.LCFI14:
	str	r0, [fp, #-36]
	str	r1, [fp, #-40]
	str	r2, [fp, #-44]
	str	r3, [fp, #-48]
	.loc 1 214 0
	ldr	r3, .L29+4	@ float
	str	r3, [fp, #-12]	@ float
	.loc 1 216 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	str	r3, [fp, #-8]
	b	.L18
.L24:
	.loc 1 220 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #44]
	ldr	r2, [fp, #-8]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	cmp	r2, r3
	bcc	.L25
.L19:
	.loc 1 227 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L21
	.loc 1 227 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-44]
	cmp	r2, r3
	bhi	.L26
.L21:
	.loc 1 233 0 is_stmt 1
	ldr	r3, [fp, #-40]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L22
	.loc 1 233 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-44]
	cmp	r2, r3
	bcc	.L27
.L22:
	.loc 1 240 0 is_stmt 1
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L23
	.loc 1 240 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-44]
	cmp	r2, r3
	bhi	.L28
.L23:
	.loc 1 247 0 is_stmt 1
	sub	r3, fp, #32
	mov	r0, r3
	ldr	r1, [fp, #-8]
	bl	update_running_dtcs
	.loc 1 249 0
	sub	r3, fp, #32
	ldr	r0, [fp, #4]
	mov	r1, r3
	mov	r2, #0
	bl	SCHEDULE_does_it_irrigate_on_this_date
	mov	r3, r0
	cmp	r3, #0
	beq	.L20
	.loc 1 251 0
	flds	s14, [fp, #-12]
	flds	s15, .L29
	fadds	s15, s14, s15
	fsts	s15, [fp, #-12]
	b	.L20
.L25:
	.loc 1 222 0
	mov	r0, r0	@ nop
	b	.L20
.L26:
	.loc 1 229 0
	mov	r0, r0	@ nop
	b	.L20
.L27:
	.loc 1 235 0
	mov	r0, r0	@ nop
	b	.L20
.L28:
	.loc 1 242 0
	mov	r0, r0	@ nop
.L20:
	.loc 1 216 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L18:
	.loc 1 216 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-40]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bcs	.L24
	.loc 1 255 0 is_stmt 1
	ldr	r3, [fp, #-12]	@ float
	.loc 1 256 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L30:
	.align	2
.L29:
	.word	1065353216
	.word	0
.LFE4:
	.size	getNumStarts, .-getNumStarts
	.section	.text.getVolume,"ax",%progbits
	.align	2
	.type	getVolume, %function
getVolume:
.LFB5:
	.loc 1 268 0
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	fstmfdd	sp!, {d8}
.LCFI16:
	add	fp, sp, #12
.LCFI17:
	sub	sp, sp, #52
.LCFI18:
	str	r0, [fp, #-56]
	str	r1, [fp, #-60]	@ float
	str	r2, [fp, #-64]
	.loc 1 269 0
	ldr	r3, .L36+8	@ float
	str	r3, [fp, #-48]	@ float
	.loc 1 289 0
	ldr	r3, [fp, #-64]
	ldr	r3, [r3, #20]
	cmp	r3, #0
	beq	.L32
.LBB3:
	.loc 1 304 0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #6]
	cmp	r3, #1
	bne	.L33
	.loc 1 307 0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #8]
	sub	r3, r3, #1
	mov	r0, r3
	bl	ET_DATA_et_number_for_the_month
	fmsr	s16, r0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #8]
	sub	r3, r3, #1
	mov	r2, r3
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #10]
	mov	r0, r2
	mov	r1, r3
	bl	NumberOfDaysInMonth
	mov	r3, r0
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s16, s15
	fsts	s15, [fp, #-16]
	b	.L34
.L33:
	.loc 1 311 0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #8]
	mov	r0, r3
	bl	ET_DATA_et_number_for_the_month
	fmsr	s16, r0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #8]
	mov	r2, r3
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #10]
	mov	r0, r2
	mov	r1, r3
	bl	NumberOfDaysInMonth
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fdivs	s15, s16, s15
	fsts	s15, [fp, #-16]
.L34:
	.loc 1 314 0
	bl	WEATHER_TABLES_get_et_ratio
	str	r0, [fp, #-24]	@ float
	.loc 1 316 0
	ldr	r3, [fp, #-64]
	ldr	r3, [r3, #40]	@ float
	str	r3, [fp, #-28]	@ float
	.loc 1 318 0
	ldr	r3, [fp, #-64]
	ldr	r3, [r3, #28]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fsts	s15, [fp, #-32]
	.loc 1 320 0
	ldr	r3, [fp, #-64]
	ldr	r2, [r3, #48]
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #8]
	mov	r0, r2
	mov	r1, r3
	bl	STATION_GROUP_get_crop_coefficient_100u
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fsts	s15, [fp, #-36]
	.loc 1 322 0
	ldr	r3, [fp, #-64]
	ldr	r3, [r3, #24]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fsts	s15, [fp, #-40]
	.loc 1 325 0
	flds	s14, [fp, #-36]
	flds	s15, .L36
	fmuls	s14, s14, s15
	flds	s15, [fp, #-16]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-24]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-40]
	fmuls	s14, s14, s15
	flds	s13, [fp, #-32]
	flds	s15, [fp, #-28]
	fmuls	s15, s13, s15
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-44]
	.loc 1 327 0
	flds	s14, [fp, #-60]
	flds	s15, [fp, #-44]
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-64]
	ldr	r3, [r3, #36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-20]
	.loc 1 331 0
	ldr	r3, [fp, #-64]
	ldr	r1, [r3, #0]
	.loc 1 332 0
	ldr	r3, [fp, #-64]
	.loc 1 331 0
	ldr	r2, [r3, #4]
	sub	r3, fp, #52
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	.loc 1 334 0
	ldr	r2, [fp, #-52]
	ldr	r1, .L36+12
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrh	r3, [r3, #0]
	ldr	r2, [fp, #-64]
	ldr	r2, [r2, #36]
	mul	r3, r2, r3
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, .L36+4
	fdivs	s15, s14, s15
	flds	s14, [fp, #-20]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-20]
	b	.L35
.L32:
.LBE3:
	.loc 1 342 0
	ldr	r3, [fp, #-64]
	ldr	r3, [r3, #16]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, [fp, #-48]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-44]
	.loc 1 344 0
	flds	s14, [fp, #-60]
	flds	s15, [fp, #-44]
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-64]
	ldr	r3, [r3, #36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-20]
.L35:
	.loc 1 347 0
	ldr	r3, [fp, #-20]	@ float
	.loc 1 348 0
	mov	r0, r3	@ float
	sub	sp, fp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {fp, pc}
.L37:
	.align	2
.L36:
	.word	1198153728
	.word	1114636288
	.word	1092616192
	.word	station_preserves
.LFE5:
	.size	getVolume, .-getVolume
	.section	.text.getNumManualStarts,"ax",%progbits
	.align	2
	.type	getNumManualStarts, %function
getNumManualStarts:
.LFB6:
	.loc 1 353 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI19:
	add	fp, sp, #4
.LCFI20:
	sub	sp, sp, #48
.LCFI21:
	str	r0, [fp, #-36]
	str	r1, [fp, #-40]
	str	r2, [fp, #-44]
	str	r3, [fp, #-48]
	.loc 1 368 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 370 0
	ldr	r0, [fp, #-48]
	bl	MANUAL_PROGRAMS_get_start_date
	str	r0, [fp, #-20]
	.loc 1 372 0
	ldr	r0, [fp, #-48]
	bl	MANUAL_PROGRAMS_get_end_date
	str	r0, [fp, #-24]
	.loc 1 377 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bhi	.L39
	.loc 1 378 0 discriminator 1
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #16]
	.loc 1 377 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcc	.L39
	.loc 1 381 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #44]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	b	.L40
.L51:
.LBB4:
	.loc 1 384 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bhi	.L52
	.loc 1 384 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcc	.L52
.L42:
	.loc 1 391 0 is_stmt 1
	sub	r3, fp, #32
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	DateToDMY
	.loc 1 393 0
	ldr	r3, [fp, #-32]
	ldr	r0, [fp, #-48]
	mov	r1, r3
	bl	MANUAL_PROGRAMS_today_is_a_water_day
	mov	r3, r0
	cmp	r3, #0
	beq	.L43
	.loc 1 396 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L44
.L50:
	.loc 1 398 0
	ldr	r0, [fp, #-48]
	ldr	r1, [fp, #-12]
	bl	MANUAL_PROGRAMS_get_start_time
	str	r0, [fp, #-28]
	.loc 1 401 0
	ldr	r2, [fp, #-28]
	ldr	r3, .L57
	cmp	r2, r3
	beq	.L53
.L45:
	.loc 1 407 0
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L47
	.loc 1 407 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bhi	.L54
.L47:
	.loc 1 413 0 is_stmt 1
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #16]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L48
	.loc 1 413 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bcc	.L55
.L48:
	.loc 1 419 0 is_stmt 1
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L49
	.loc 1 419 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bhi	.L56
.L49:
	.loc 1 424 0 is_stmt 1
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
	b	.L46
.L53:
	.loc 1 403 0
	mov	r0, r0	@ nop
	b	.L46
.L54:
	.loc 1 409 0
	mov	r0, r0	@ nop
	b	.L46
.L55:
	.loc 1 415 0
	mov	r0, r0	@ nop
	b	.L46
.L56:
	.loc 1 421 0
	mov	r0, r0	@ nop
.L46:
	.loc 1 396 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L44:
	.loc 1 396 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #5
	bls	.L50
	b	.L43
.L52:
	.loc 1 386 0 is_stmt 1
	mov	r0, r0	@ nop
.L43:
.LBE4:
	.loc 1 381 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L40:
	.loc 1 381 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #16]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bcs	.L51
.L39:
	.loc 1 430 0 is_stmt 1
	ldr	r3, [fp, #-16]
	.loc 1 431 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L58:
	.align	2
.L57:
	.word	86400
.LFE6:
	.size	getNumManualStarts, .-getNumManualStarts
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/budg"
	.ascii	"ets/budgets.c\000"
	.section	.text.getIrrigationListVp,"ax",%progbits
	.align	2
	.type	getIrrigationListVp, %function
getIrrigationListVp:
.LFB7:
	.loc 1 437 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI22:
	add	fp, sp, #4
.LCFI23:
	sub	sp, sp, #24
.LCFI24:
	str	r0, [fp, #-28]
	.loc 1 452 0
	ldr	r3, .L63	@ float
	str	r3, [fp, #-12]	@ float
	.loc 1 454 0
	ldr	r3, .L63+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L63+8
	ldr	r3, .L63+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 456 0
	ldr	r0, .L63+16
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 458 0
	b	.L60
.L62:
	.loc 1 461 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bne	.L61
	.loc 1 464 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	bl	nm_STATION_get_pointer_to_station
	str	r0, [fp, #-16]
	.loc 1 466 0
	ldr	r0, [fp, #-16]
	bl	nm_STATION_get_expected_flow_rate_gpm
	str	r0, [fp, #-20]
	.loc 1 468 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #48]
	add	r3, r2, r3
	ldr	r2, [fp, #-20]
	mul	r3, r2, r3
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	ldr	r3, .L63+20
	flds	s15, [r3, #0]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-24]
	.loc 1 470 0
	flds	s14, [fp, #-12]
	flds	s15, [fp, #-24]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-12]
.L61:
	.loc 1 473 0
	ldr	r0, .L63+16
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L60:
	.loc 1 458 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L62
	.loc 1 476 0
	ldr	r3, .L63+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 478 0
	ldr	r3, [fp, #-12]	@ float
	.loc 1 479 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L64:
	.align	2
.L63:
	.word	0
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	454
	.word	foal_irri+16
	.word	SIXTY.8002
.LFE7:
	.size	getIrrigationListVp, .-getIrrigationListVp
	.section	.text.calc_percent_adjust,"ax",%progbits
	.align	2
	.global	calc_percent_adjust
	.type	calc_percent_adjust, %function
calc_percent_adjust:
.LFB8:
	.loc 1 484 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI25:
	add	fp, sp, #0
.LCFI26:
	sub	sp, sp, #20
.LCFI27:
	str	r0, [fp, #-8]	@ float
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]	@ float
	str	r3, [fp, #-20]
	.loc 1 489 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ble	.L66
	.loc 1 491 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s15, [fp, #-8]
	fcmpes	s14, s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L67
	.loc 1 493 0
	flds	s14, [fp, #-8]
	flds	s15, [fp, #-16]
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-8]
	b	.L66
.L67:
	.loc 1 498 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s13, [fp, #-16]
	flds	s15, .L69
	fsubs	s15, s13, s15
	fmuls	s15, s14, s15
	flds	s14, [fp, #-8]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-8]
.L66:
	.loc 1 503 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	mov	r2, r3
	ldr	r3, [fp, #-20]
	rsb	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 504 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bge	.L68
	.loc 1 506 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #0]
.L68:
	.loc 1 509 0
	ldr	r3, [fp, #-8]	@ float
	str	r3, [fp, #-4]	@ float
	.loc 1 510 0
	ldr	r3, [fp, #-4]	@ float
	.loc 1 511 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L70:
	.align	2
.L69:
	.word	1065353216
.LFE8:
	.size	calc_percent_adjust, .-calc_percent_adjust
	.section	.text.getScheduledVp,"ax",%progbits
	.align	2
	.type	getScheduledVp, %function
getScheduledVp:
.LFB9:
	.loc 1 530 0
	@ args = 12, pretend = 4, frame = 192
	@ frame_needed = 1, uses_anonymous_args = 0
	sub	sp, sp, #4
.LCFI28:
	stmfd	sp!, {fp, lr}
.LCFI29:
	fstmfdd	sp!, {d8}
.LCFI30:
	add	fp, sp, #12
.LCFI31:
	sub	sp, sp, #196
.LCFI32:
	str	r0, [fp, #-196]
	str	r1, [fp, #-200]
	str	r2, [fp, #-204]
	str	r3, [fp, #4]
	.loc 1 537 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 1 576 0
	ldr	r3, .L92+4	@ float
	str	r3, [fp, #-36]	@ float
	.loc 1 577 0
	ldr	r3, .L92+8	@ float
	str	r3, [fp, #-16]	@ float
	.loc 1 580 0
	ldr	r3, [fp, #-204]
	ldr	r0, [fp, #-200]
	ldmia	r3, {r1, r2}
	bl	SYSTEM_get_budget_period_index
	str	r0, [fp, #-44]
	.loc 1 586 0
	ldr	r0, .L92+20
	bl	nm_ListGetFirst
	str	r0, [fp, #-20]
	.loc 1 588 0
	b	.L72
.L90:
	.loc 1 590 0
	ldr	r3, .L92+8	@ float
	str	r3, [fp, #-24]	@ float
	.loc 1 591 0
	ldr	r3, .L92+8	@ float
	str	r3, [fp, #-48]	@ float
	.loc 1 594 0
	sub	r3, fp, #148
	ldr	r0, [fp, #-20]
	mov	r1, r3
	bl	nm_STATION_get_Budget_data
	.loc 1 597 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L73
	.loc 1 601 0
	ldr	r2, [fp, #-104]
	ldr	r3, [fp, #-200]
	ldr	r1, [fp, #-44]
	add	r1, r1, #3
	ldr	r1, [r3, r1, asl #2]
	ldrh	r3, [fp, #8]
	rsb	r3, r3, r1
	add	r3, r3, #1
	cmp	r2, r3
	bls	.L73
	.loc 1 603 0
	ldr	r2, [fp, #-104]
	ldr	r3, [fp, #-200]
	ldr	r1, [fp, #-44]
	add	r1, r1, #3
	ldr	r1, [r3, r1, asl #2]
	ldrh	r3, [fp, #8]
	rsb	r3, r3, r1
	mvn	r3, r3
	add	r3, r2, r3
	str	r3, [fp, #-104]
.L73:
	.loc 1 611 0
	ldr	r3, [fp, #-100]
	cmp	r3, #0
	beq	.L74
	.loc 1 612 0 discriminator 1
	ldr	r2, [fp, #-96]
	.loc 1 611 0 discriminator 1
	ldr	r3, [fp, #-196]
	cmp	r2, r3
	bne	.L74
	.loc 1 613 0
	ldr	r3, [fp, #-136]
	.loc 1 612 0
	cmp	r3, #0
	beq	.L74
	.loc 1 613 0
	ldr	r3, [fp, #-140]
	cmp	r3, #0
	beq	.L74
	.loc 1 614 0 discriminator 1
	ldr	r3, [fp, #-44]
	add	r2, r3, #1
	ldr	r3, [fp, #-200]
	add	r2, r2, #3
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-204]
	ldrh	r3, [r3, #4]
	mov	r1, r3
	ldr	r3, [fp, #-104]
	add	r3, r1, r3
	.loc 1 613 0 discriminator 1
	cmp	r2, r3
	bcc	.L74
	.loc 1 617 0
	ldr	r3, [fp, #-100]
	mov	r0, r3
	bl	STATION_GROUP_get_group_with_this_GID
	str	r0, [fp, #-52]
	.loc 1 619 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	beq	.L74
	.loc 1 623 0
	ldr	r0, [fp, #-52]
	bl	SCHEDULE_get_start_time
	str	r0, [fp, #-56]
	.loc 1 625 0
	ldr	r0, [fp, #-52]
	bl	SCHEDULE_get_stop_time
	mov	r2, r0
	ldr	r3, [fp, #-56]
	cmp	r2, r3
	beq	.L75
	.loc 1 630 0
	ldr	r3, [fp, #12]
	cmp	r3, #0
	beq	.L76
.LBB5:
	.loc 1 632 0
	ldr	r0, [fp, #-196]
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-60]
	.loc 1 633 0
	ldr	r0, [fp, #-60]
	bl	nm_SYSTEM_get_poc_ratio
	fmsr	s16, r0
	ldr	r0, [fp, #-60]
	bl	nm_SYSTEM_get_Vp
	fmsr	s15, r0
	fdivs	s15, s16, s15
	fsts	s15, [fp, #-64]
	.loc 1 634 0
	ldr	r0, [fp, #-52]
	bl	STATION_GROUP_get_budget_reduction_limit
	mov	r3, r0
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, .L92
	fdivs	s15, s14, s15
	flds	s14, .L92+4
	fsubs	s15, s14, s15
	fsts	s15, [fp, #-68]
	.loc 1 636 0
	flds	s14, [fp, #-64]
	flds	s15, .L92+4
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L77
	.loc 1 638 0
	ldr	r3, .L92+4	@ float
	str	r3, [fp, #-36]	@ float
	b	.L76
.L77:
	.loc 1 640 0
	flds	s15, [fp, #-64]
	fcmpezs	s15
	fmstat
	movhi	r3, #0
	movls	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L78
	.loc 1 642 0
	ldr	r3, [fp, #-68]	@ float
	str	r3, [fp, #-36]	@ float
	b	.L76
.L78:
	.loc 1 644 0
	flds	s14, [fp, #-64]
	flds	s15, [fp, #-68]
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L79
	.loc 1 646 0
	ldr	r3, [fp, #-64]	@ float
	str	r3, [fp, #-36]	@ float
	b	.L76
.L79:
	.loc 1 650 0
	ldr	r3, [fp, #-68]	@ float
	str	r3, [fp, #-36]	@ float
.L76:
.LBE5:
	.loc 1 656 0
	ldr	r3, [fp, #-204]
	sub	ip, fp, #168
	mov	lr, r3
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	str	r3, [ip, #0]
	.loc 1 658 0
	ldrh	r2, [fp, #-164]
	ldr	r3, [fp, #-104]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-164]	@ movhi
	.loc 1 660 0
	ldrh	r3, [fp, #-164]
	sub	r2, fp, #168
	mov	r0, r2
	mov	r1, r3
	bl	update_running_dtcs
	.loc 1 663 0
	ldr	r2, [fp, #-100]
	ldrh	r3, [fp, #-164]
	mov	r0, r2
	mov	r1, r3
	bl	PERCENT_ADJUST_get_percentage_100u_for_this_gid
	mov	r3, r0
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	ldr	r3, .L92+12
	flds	s15, [r3, #0]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-72]
	.loc 1 665 0
	ldr	r2, [fp, #-100]
	ldrh	r3, [fp, #-164]
	mov	r0, r2
	mov	r1, r3
	bl	PERCENT_ADJUST_get_remaining_days_for_this_gid
	mov	r3, r0
	str	r3, [fp, #-192]
	.loc 1 677 0
	mov	r3, #0
	str	r3, [fp, #-104]
	.loc 1 688 0
	ldr	r3, [fp, #-128]
	cmp	r3, #0
	beq	.L80
.L88:
	.loc 1 692 0
	ldrh	r3, [fp, #-160]
	mov	r2, r3
	ldrh	r3, [fp, #-158]
	mov	r0, r2
	mov	r1, r3
	bl	NumberOfDaysInMonth
	mov	r2, r0
	ldrh	r3, [fp, #-162]
	rsb	r3, r3, r2
	add	r3, r3, #1
	str	r3, [fp, #-76]
	.loc 1 693 0
	ldr	r3, [fp, #-76]
	str	r3, [fp, #-28]
	.loc 1 694 0
	ldr	r3, [fp, #-44]
	add	r2, r3, #1
	ldr	r3, [fp, #-200]
	add	r2, r2, #3
	ldr	r2, [r3, r2, asl #2]
	ldrh	r3, [fp, #-164]
	rsb	r3, r3, r2
	str	r3, [fp, #-32]
	.loc 1 697 0
	ldr	r2, [fp, #-168]
	ldr	r3, [fp, #-56]
	cmp	r2, r3
	bhi	.L81
	.loc 1 699 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
	.loc 1 700 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #1
	str	r3, [fp, #-32]
.L81:
	.loc 1 704 0
	ldrh	r3, [fp, #-162]
	cmp	r3, #1
	bne	.L82
	.loc 1 707 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-76]
	rsb	r3, r3, r2
	str	r3, [fp, #-28]
.L82:
	.loc 1 711 0
	ldr	r2, [fp, #-32]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bhi	.L83
	.loc 1 715 0
	ldr	r3, [fp, #-32]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	sub	r3, fp, #192
	fmrs	r0, s15
	mov	r1, r3
	ldr	r2, [fp, #-72]	@ float
	ldr	r3, [fp, #-28]
	bl	calc_percent_adjust
	str	r0, [fp, #-48]	@ float
	.loc 1 719 0
	flds	s15, [fp, #-48]
	fcmpezs	s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L91
	.loc 1 721 0
	sub	r2, fp, #168
	sub	r3, fp, #148
	mov	r0, r2
	ldr	r1, [fp, #-48]	@ float
	mov	r2, r3
	bl	getVolume
	str	r0, [fp, #-80]	@ float
	.loc 1 722 0
	flds	s14, [fp, #-24]
	flds	s15, [fp, #-80]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-24]
	.loc 1 725 0
	mov	r0, r0	@ nop
	b	.L91
.L83:
	.loc 1 731 0
	ldr	r3, [fp, #-28]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	sub	r3, fp, #192
	fmrs	r0, s15
	mov	r1, r3
	ldr	r2, [fp, #-72]	@ float
	ldr	r3, [fp, #-28]
	bl	calc_percent_adjust
	str	r0, [fp, #-48]	@ float
	.loc 1 733 0
	flds	s15, [fp, #-48]
	fcmpezs	s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L86
	.loc 1 735 0
	sub	r2, fp, #168
	sub	r3, fp, #148
	mov	r0, r2
	ldr	r1, [fp, #-48]	@ float
	mov	r2, r3
	bl	getVolume
	str	r0, [fp, #-80]	@ float
	.loc 1 736 0
	flds	s14, [fp, #-24]
	flds	s15, [fp, #-80]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-24]
.L86:
	.loc 1 739 0
	ldrh	r2, [fp, #-164]
	ldr	r3, [fp, #-28]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-164]	@ movhi
	.loc 1 740 0
	ldr	r2, [fp, #-168]
	ldr	r3, [fp, #-56]
	cmp	r2, r3
	bls	.L87
	.loc 1 742 0
	ldrh	r3, [fp, #-164]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-164]	@ movhi
.L87:
	.loc 1 744 0
	mov	r3, #0
	str	r3, [fp, #-168]
	.loc 1 745 0
	ldrh	r3, [fp, #-164]
	sub	r2, fp, #168
	mov	r0, r2
	mov	r1, r3
	bl	update_running_dtcs
	.loc 1 747 0
	ldr	r2, [fp, #-32]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bhi	.L88
	b	.L75
.L93:
	.align	2
.L92:
	.word	1120403456
	.word	1065353216
	.word	0
	.word	HUNDRED.8026
	.word	SIXTY.8025
	.word	station_info_list_hdr
.L80:
	.loc 1 751 0
	ldrh	r3, [fp, #-164]
	sub	r2, fp, #168
	mov	r0, r2
	mov	r1, r3
	bl	update_running_dtcs
	.loc 1 753 0
	ldr	r3, [fp, #-44]
	add	r2, r3, #1
	ldr	r3, [fp, #-200]
	add	r2, r2, #3
	ldr	r3, [r3, r2, asl #2]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-184]	@ movhi
	.loc 1 754 0
	ldr	r3, [fp, #-200]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-188]
	.loc 1 756 0
	sub	r1, fp, #168
	sub	r2, fp, #188
	sub	r3, fp, #148
	ldr	r0, [fp, #-52]
	str	r0, [sp, #0]
	mov	r0, r1
	mov	r1, r2
	ldr	r2, [fp, #-56]
	bl	getNumStarts
	str	r0, [fp, #-48]	@ float
	.loc 1 759 0
	ldrh	r3, [fp, #-184]
	mov	r2, r3
	ldrh	r3, [fp, #-164]
	rsb	r3, r3, r2
	str	r3, [fp, #-32]
	.loc 1 761 0
	sub	r3, fp, #192
	ldr	r0, [fp, #-48]	@ float
	mov	r1, r3
	ldr	r2, [fp, #-72]	@ float
	ldr	r3, [fp, #-32]
	bl	calc_percent_adjust
	str	r0, [fp, #-48]	@ float
	.loc 1 764 0
	sub	r2, fp, #168
	sub	r3, fp, #148
	mov	r0, r2
	ldr	r1, [fp, #-48]	@ float
	mov	r2, r3
	bl	getVolume
	str	r0, [fp, #-80]	@ float
	.loc 1 766 0
	flds	s14, [fp, #-24]
	flds	s15, [fp, #-80]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-24]
	b	.L75
.L91:
	.loc 1 725 0
	mov	r0, r0	@ nop
.L75:
	.loc 1 772 0
	flds	s14, [fp, #-36]
	flds	s15, [fp, #-24]
	fmuls	s15, s14, s15
	flds	s14, [fp, #-16]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-16]
	.loc 1 777 0
	ldr	r3, [fp, #-92]
	cmp	r3, #0
	beq	.L89
	.loc 1 780 0
	ldr	r3, [fp, #-92]
	mov	r0, r3
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	str	r0, [fp, #-40]
	.loc 1 782 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L89
	.loc 1 785 0
	sub	r3, fp, #148
	ldr	r0, [fp, #-204]
	ldr	r1, [fp, #-200]
	mov	r2, r3
	ldr	r3, [fp, #-40]
	bl	getNumManualStarts
	str	r0, [fp, #-84]
	.loc 1 787 0
	ldr	r0, [fp, #-40]
	bl	MANUAL_PROGRAMS_get_run_time
	mov	r3, r0
	ldr	r2, [fp, #-84]
	mul	r3, r2, r3
	ldr	r2, [fp, #-112]
	mul	r3, r2, r3
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	ldr	r3, .L92+16
	flds	s15, [r3, #0]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-80]
	.loc 1 789 0
	flds	s14, [fp, #-16]
	flds	s15, [fp, #-80]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-16]
.L89:
	.loc 1 793 0
	ldr	r3, [fp, #-88]
	cmp	r3, #0
	beq	.L74
	.loc 1 796 0
	ldr	r3, [fp, #-88]
	mov	r0, r3
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	str	r0, [fp, #-40]
	.loc 1 798 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L74
	.loc 1 801 0
	sub	r3, fp, #148
	ldr	r0, [fp, #-204]
	ldr	r1, [fp, #-200]
	mov	r2, r3
	ldr	r3, [fp, #-40]
	bl	getNumManualStarts
	str	r0, [fp, #-84]
	.loc 1 803 0
	ldr	r0, [fp, #-40]
	bl	MANUAL_PROGRAMS_get_run_time
	mov	r3, r0
	ldr	r2, [fp, #-84]
	mul	r3, r2, r3
	ldr	r2, [fp, #-112]
	mul	r3, r2, r3
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	ldr	r3, .L92+16
	flds	s15, [r3, #0]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-80]
	.loc 1 805 0
	flds	s14, [fp, #-16]
	flds	s15, [fp, #-80]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-16]
.L74:
	.loc 1 812 0
	ldr	r0, .L92+20
	ldr	r1, [fp, #-20]
	bl	nm_ListGetNext
	str	r0, [fp, #-20]
.L72:
	.loc 1 588 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L90
	.loc 1 815 0
	ldr	r3, [fp, #-16]	@ float
	.loc 1 816 0
	mov	r0, r3	@ float
	sub	sp, fp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {fp, lr}
	add	sp, sp, #4
	bx	lr
.LFE9:
	.size	getScheduledVp, .-getScheduledVp
	.section	.text.nm_BUDGET_get_poc_usage,"ax",%progbits
	.align	2
	.type	nm_BUDGET_get_poc_usage, %function
nm_BUDGET_get_poc_usage:
.LFB10:
	.loc 1 827 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #20
.LCFI35:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 1 836 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 838 0
	ldr	r3, .L98
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L98+4
	ldr	r3, .L98+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 842 0
	bl	nm_POC_REPORT_RECORDS_get_most_recently_completed_record
	str	r0, [fp, #-8]
	.loc 1 844 0
	b	.L95
.L97:
	.loc 1 847 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_GID_irrigation_system_using_POC_gid
	mov	r2, r0
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L96
	.loc 1 854 0
	ldr	r3, [fp, #-12]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	ldr	r3, [fp, #-8]
	fldd	d7, [r3, #48]
	faddd	d7, d6, d7
	ftouizd	s13, d7
	fmrs	r3, s13	@ int
	str	r3, [fp, #-12]
	.loc 1 857 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	sub	r3, fp, #16
	mov	r0, r2
	mov	r1, r3
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	.loc 1 860 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-24]
	add	r3, r2, r3
	ldr	r2, [fp, #-16]
	mov	r2, r2, asl #2
	ldr	r1, [fp, #-24]
	add	r2, r1, r2
	ldr	r2, [r2, #0]
	fmsr	s15, r2	@ int
	fuitod	d6, s15
	ldr	r2, [fp, #-8]
	fldd	d7, [r2, #48]
	faddd	d7, d6, d7
	ftouizd	s13, d7
	fmrs	r2, s13	@ int
	str	r2, [r3, #0]
.L96:
	.loc 1 863 0
	ldr	r0, [fp, #-8]
	bl	nm_POC_REPORT_RECORDS_get_previous_completed_record
	str	r0, [fp, #-8]
.L95:
	.loc 1 844 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L97
	.loc 1 866 0
	ldr	r3, .L98
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 870 0
	ldr	r3, [fp, #-12]
	.loc 1 871 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L99:
	.align	2
.L98:
	.word	poc_report_completed_records_recursive_MUTEX
	.word	.LC0
	.word	838
.LFE10:
	.size	nm_BUDGET_get_poc_usage, .-nm_BUDGET_get_poc_usage
	.section .rodata
	.align	2
.LC1:
	.ascii	"BUDGET %d/%d - No measured volume\000"
	.section	.text.nm_BUDGET_calc_rpoc,"ax",%progbits
	.align	2
	.global	nm_BUDGET_calc_rpoc
	.type	nm_BUDGET_calc_rpoc, %function
nm_BUDGET_calc_rpoc:
.LFB11:
	.loc 1 880 0
	@ args = 0, pretend = 0, frame = 80
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #80
.LCFI38:
	str	r0, [fp, #-80]
	str	r1, [fp, #-84]
	.loc 1 889 0
	sub	r3, fp, #76
	mov	r0, r3
	mov	r1, #0
	mov	r2, #48
	bl	memset
	.loc 1 892 0
	sub	r3, fp, #76
	ldr	r0, [fp, #-80]
	mov	r1, r3
	bl	nm_BUDGET_get_poc_usage
	str	r0, [fp, #-16]
	.loc 1 894 0
	ldr	r3, .L117+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L117+8
	ldr	r3, .L117+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 901 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 902 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L101
.L103:
	.loc 1 904 0
	ldr	r1, .L117+16
	ldr	r2, [fp, #-8]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 905 0
	ldr	r1, .L117+16
	ldr	r2, [fp, #-8]
	mov	r3, #44
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-24]
	.loc 1 908 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L102
	.loc 1 909 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #0]
	.loc 1 908 0 discriminator 1
	ldr	r3, [fp, #-80]
	cmp	r2, r3
	bne	.L102
	.loc 1 910 0
	ldr	r0, [fp, #-8]
	bl	POC_use_for_budget
	mov	r3, r0
	.loc 1 909 0
	cmp	r3, #0
	beq	.L102
	.loc 1 913 0
	ldr	r0, [fp, #-20]
	bl	POC_get_box_index_0
	mov	r3, r0
	ldr	r0, [fp, #-20]
	mov	r1, r3
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	str	r0, [fp, #-28]
	.loc 1 914 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	beq	.L102
	.loc 1 914 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-28]
	mov	r1, #0
	bl	POC_get_budget
	mov	r3, r0
	cmp	r3, #0
	beq	.L102
	.loc 1 916 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L102:
	.loc 1 902 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L101:
	.loc 1 902 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L103
	.loc 1 924 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L104
.L114:
	.loc 1 928 0
	ldr	r1, .L117+16
	ldr	r2, [fp, #-8]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 930 0
	ldr	r1, .L117+16
	ldr	r2, [fp, #-8]
	mov	r3, #44
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-24]
	.loc 1 934 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L115
	.loc 1 935 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #0]
	.loc 1 934 0 discriminator 1
	ldr	r3, [fp, #-80]
	cmp	r2, r3
	bne	.L115
	.loc 1 936 0
	ldr	r0, [fp, #-8]
	bl	POC_use_for_budget
	mov	r3, r0
	.loc 1 935 0
	cmp	r3, #0
	beq	.L115
.L106:
	.loc 1 941 0
	ldr	r0, [fp, #-20]
	bl	POC_get_box_index_0
	mov	r3, r0
	ldr	r0, [fp, #-20]
	mov	r1, r3
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	str	r0, [fp, #-28]
	.loc 1 942 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	beq	.L116
	.loc 1 942 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-28]
	mov	r1, #0
	bl	POC_get_budget
	mov	r3, r0
	cmp	r3, #0
	beq	.L116
.L109:
	.loc 1 952 0 is_stmt 1
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L110
	.loc 1 954 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-84]
	add	r3, r2, r3
	ldr	r2, .L117	@ float
	str	r2, [r3, #0]	@ float
	b	.L107
.L110:
	.loc 1 956 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bls	.L107
	.loc 1 958 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L111
	.loc 1 960 0
	ldr	r2, [fp, #-24]
	mov	r3, #468
	ldrh	r3, [r2, r3]
	mov	r3, r3, lsr #7
	and	r3, r3, #63
	and	r3, r3, #255
	cmp	r3, #0
	ble	.L112
	.loc 1 963 0
	ldr	r0, .L117+20
	ldr	r1, [fp, #-80]
	ldr	r2, [fp, #-20]
	bl	Alert_Message_va
.L112:
	.loc 1 968 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-84]
	add	r3, r2, r3
	ldr	r2, [fp, #-12]
	fmsr	s13, r2	@ int
	fuitos	s15, s13
	flds	s14, .L117
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	b	.L107
.L111:
	.loc 1 972 0
	ldr	r2, [fp, #-8]
	mvn	r3, #71
	mov	r2, r2, asl #2
	sub	r0, fp, #4
	add	r2, r0, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L113
	.loc 1 974 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-84]
	add	r3, r2, r3
	ldr	r2, .L117+24	@ float
	str	r2, [r3, #0]	@ float
	b	.L107
.L113:
	.loc 1 978 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-84]
	add	r3, r2, r3
	ldr	r1, [fp, #-8]
	mvn	r2, #71
	mov	r1, r1, asl #2
	sub	r0, fp, #4
	add	r1, r0, r1
	add	r2, r1, r2
	ldr	r2, [r2, #0]
	fmsr	s13, r2	@ int
	fuitos	s14, s13
	ldr	r2, [fp, #-16]
	fmsr	s13, r2	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	b	.L107
.L115:
	.loc 1 938 0
	mov	r0, r0	@ nop
	b	.L107
.L116:
	.loc 1 944 0
	mov	r0, r0	@ nop
.L107:
	.loc 1 924 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L104:
	.loc 1 924 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L114
	.loc 1 984 0 is_stmt 1
	ldr	r3, .L117+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 985 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L118:
	.align	2
.L117:
	.word	1065353216
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	894
	.word	poc_preserves
	.word	.LC1
	.word	0
.LFE11:
	.size	nm_BUDGET_calc_rpoc, .-nm_BUDGET_calc_rpoc
	.section	.text.nm_calc_POC_ratios,"ax",%progbits
	.align	2
	.type	nm_calc_POC_ratios, %function
nm_calc_POC_ratios:
.LFB12:
	.loc 1 993 0
	@ args = 0, pretend = 0, frame = 76
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI39:
	add	fp, sp, #4
.LCFI40:
	sub	sp, sp, #76
.LCFI41:
	str	r0, [fp, #-80]
	.loc 1 1010 0
	sub	r3, fp, #76
	mov	r0, r3
	mov	r1, #0
	mov	r2, #48
	bl	memset
	.loc 1 1013 0
	ldr	r3, .L128+8
	ldr	r3, [r3, #0]	@ float
	ldr	r0, [fp, #-80]
	mov	r1, r3	@ float
	bl	nm_SYSTEM_set_poc_ratio
	.loc 1 1015 0
	ldr	r0, [fp, #-80]
	bl	nm_GROUP_get_group_ID
	str	r0, [fp, #-12]
	.loc 1 1018 0
	sub	r3, fp, #76
	ldr	r0, [fp, #-12]
	mov	r1, r3
	bl	nm_BUDGET_calc_rpoc
	.loc 1 1023 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L120
.L125:
	.loc 1 1025 0
	ldr	r1, .L128+12
	ldr	r2, [fp, #-8]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	.loc 1 1028 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L126
	.loc 1 1029 0 discriminator 1
	ldr	r1, .L128+12
	ldr	r2, [fp, #-8]
	mov	r3, #44
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	.loc 1 1028 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bne	.L126
.L122:
	.loc 1 1035 0
	ldr	r0, [fp, #-8]
	bl	POC_use_for_budget
	mov	r3, r0
	cmp	r3, #0
	beq	.L127
.L124:
	.loc 1 1042 0
	ldr	r0, [fp, #-16]
	bl	POC_get_index_for_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_group_at_this_index
	mov	r3, r0
	mov	r0, r3
	mov	r1, #0
	bl	POC_get_budget
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fsts	s15, [fp, #-20]
	.loc 1 1044 0
	ldr	r0, [fp, #-8]
	bl	nm_BUDGET_get_used
	fmdrr	d7, r0, r1
	fcvtsd	s15, d7
	fsts	s15, [fp, #-24]
	.loc 1 1048 0
	flds	s15, [fp, #-20]
	fcvtds	d6, s15
	fldd	d7, .L128
	fmuld	d6, d6, d7
	flds	s15, [fp, #-24]
	fcvtds	d7, s15
	fsubd	d6, d6, d7
	ldr	r2, [fp, #-8]
	mvn	r3, #71
	mov	r2, r2, asl #2
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	flds	s15, [r3, #0]
	fcvtds	d7, s15
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	fsts	s15, [fp, #-28]
	.loc 1 1073 0
	flds	s15, [fp, #-20]
	fcmpezs	s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L123
	.loc 1 1075 0
	ldr	r0, [fp, #-80]
	bl	nm_SYSTEM_get_poc_ratio
	fmsr	s14, r0
	flds	s15, [fp, #-28]
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L123
	.loc 1 1077 0
	ldr	r0, [fp, #-80]
	ldr	r1, [fp, #-28]	@ float
	bl	nm_SYSTEM_set_poc_ratio
	b	.L123
.L126:
	.loc 1 1031 0
	mov	r0, r0	@ nop
	b	.L123
.L127:
	.loc 1 1039 0
	mov	r0, r0	@ nop
.L123:
	.loc 1 1023 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L120:
	.loc 1 1023 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L125
	.loc 1 1081 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L129:
	.align	2
.L128:
	.word	-343597384
	.word	1072609361
	.word	__float32_infinity
	.word	poc_preserves
.LFE12:
	.size	nm_calc_POC_ratios, .-nm_calc_POC_ratios
	.section	.text.calc_time_adjustment_low_level,"ax",%progbits
	.align	2
	.type	calc_time_adjustment_low_level, %function
calc_time_adjustment_low_level:
.LFB13:
	.loc 1 1088 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI42:
	add	fp, sp, #4
.LCFI43:
	sub	sp, sp, #40
.LCFI44:
	str	r0, [fp, #-32]	@ float
	str	r1, [fp, #-36]
	str	r2, [fp, #-40]
	str	r3, [fp, #-44]
	.loc 1 1103 0
	mov	r3, #0
	strb	r3, [fp, #-9]
	.loc 1 1106 0
	ldr	r3, [fp, #-32]	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 1108 0
	ldr	r0, [fp, #-40]
	bl	nm_SYSTEM_get_Vp
	str	r0, [fp, #-16]	@ float
	.loc 1 1110 0
	ldr	r0, [fp, #-40]
	bl	nm_SYSTEM_get_poc_ratio
	str	r0, [fp, #-20]	@ float
	.loc 1 1116 0
	flds	s15, [fp, #-16]
	fcmpzs	s15
	fmstat
	beq	.L131
	.loc 1 1118 0
	flds	s14, [fp, #-32]
	flds	s15, [fp, #-20]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-16]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-8]
.L131:
	.loc 1 1141 0
	ldr	r0, [fp, #-44]
	bl	STATION_GROUP_get_budget_reduction_limit_for_this_station
	str	r0, [fp, #-24]
	.loc 1 1142 0
	ldr	r3, [fp, #-24]
	rsb	r3, r3, #100
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-32]
	fmuls	s14, s14, s15
	flds	s15, .L138
	fmuls	s14, s14, s15
	flds	s15, [fp, #-8]
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L132
	.loc 1 1144 0
	ldr	r3, [fp, #-24]
	rsb	r3, r3, #100
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-32]
	fmuls	s14, s14, s15
	flds	s15, .L138
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-8]
	.loc 1 1148 0
	ldrb	r3, [fp, #-9]
	orr	r3, r3, #2
	strb	r3, [fp, #-9]
.L132:
	.loc 1 1152 0
	flds	s15, [fp, #-8]
	fcmpezs	s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L133
	.loc 1 1154 0
	ldr	r3, .L138+4	@ float
	str	r3, [fp, #-8]	@ float
.L133:
	.loc 1 1159 0
	flds	s14, [fp, #-32]
	flds	s15, [fp, #-8]
	fcmpes	s14, s15
	fmstat
	movhi	r3, #0
	movls	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L134
	.loc 1 1161 0
	ldr	r3, [fp, #-32]	@ float
	str	r3, [fp, #-8]	@ float
	b	.L135
.L134:
	.loc 1 1166 0
	ldrb	r3, [fp, #-9]	@ zero_extendqisi2
	and	r3, r3, #2
	cmp	r3, #0
	bne	.L135
	.loc 1 1168 0
	ldrb	r3, [fp, #-9]
	orr	r3, r3, #1
	strb	r3, [fp, #-9]
.L135:
	.loc 1 1176 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L136
	.loc 1 1178 0
	sub	r3, fp, #28
	ldr	r0, [fp, #-44]
	mov	r1, r3
	bl	STATION_PRESERVES_get_index_using_ptr_to_station_struct
	mov	r3, r0
	cmp	r3, #0
	beq	.L136
	.loc 1 1180 0
	ldr	r3, .L138+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L138+12
	ldr	r3, .L138+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1183 0
	ldr	r1, [fp, #-28]
	ldr	r2, [fp, #-28]
	ldr	r0, .L138+20
	mov	r3, #68
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	bic	r3, r3, #1
	and	r2, r3, #255
	ldr	r0, .L138+20
	mov	r3, #68
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strb	r2, [r3, #3]
	.loc 1 1184 0
	ldr	r1, [fp, #-28]
	ldr	r2, [fp, #-28]
	ldr	r0, .L138+20
	mov	r3, #68
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	bic	r3, r3, #2
	and	r2, r3, #255
	ldr	r0, .L138+20
	mov	r3, #68
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strb	r2, [r3, #3]
	.loc 1 1187 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L137
	.loc 1 1191 0
	ldr	r1, [fp, #-28]
	ldr	r2, [fp, #-28]
	ldr	r0, .L138+20
	mov	r3, #68
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	ldrb	r3, [fp, #-9]
	orr	r3, r2, r3
	and	r2, r3, #255
	ldr	r0, .L138+20
	mov	r3, #68
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strb	r2, [r3, #3]
.L137:
	.loc 1 1194 0
	ldr	r3, .L138+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L136:
	.loc 1 1204 0
	ldr	r3, [fp, #-8]	@ float
	.loc 1 1205 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L139:
	.align	2
.L138:
	.word	1008981770
	.word	0
	.word	station_preserves_recursive_MUTEX
	.word	.LC0
	.word	1180
	.word	station_preserves
.LFE13:
	.size	calc_time_adjustment_low_level, .-calc_time_adjustment_low_level
	.section	.text.nm_handle_sending_budget_record,"ax",%progbits
	.align	2
	.type	nm_handle_sending_budget_record, %function
nm_handle_sending_budget_record:
.LFB14:
	.loc 1 1218 0
	@ args = 20, pretend = 8, frame = 1344
	@ frame_needed = 1, uses_anonymous_args = 0
	sub	sp, sp, #8
.LCFI45:
	stmfd	sp!, {fp, lr}
.LCFI46:
	add	fp, sp, #4
.LCFI47:
	sub	sp, sp, #1344
.LCFI48:
	sub	sp, sp, #8
.LCFI49:
	str	r0, [fp, #-1344]
	str	r1, [fp, #-1348]
	add	r1, fp, #4
	stmia	r1, {r2, r3}
	.loc 1 1228 0
	sub	r3, fp, #1216
	sub	r3, r3, #4
	sub	r3, r3, #8
	ldr	r2, .L146+4
	mov	r0, r3
	mov	r1, #0
	bl	memset
	.loc 1 1240 0
	ldr	r3, .L146+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L146+12
	ldr	r3, .L146+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1242 0
	ldr	r0, [fp, #-1344]
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-12]
	.loc 1 1251 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L141
.L144:
	.loc 1 1254 0
	ldr	r1, .L146+20
	ldr	r2, [fp, #-8]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L145
.L142:
	.loc 1 1259 0
	ldr	r1, .L146+20
	ldr	r2, [fp, #-8]
	mov	r3, #44
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	.loc 1 1261 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-1344]
	cmp	r2, r3
	bne	.L143
	.loc 1 1263 0
	ldr	r3, [fp, #-8]
	mov	r2, #472
	mul	r3, r2, r3
	add	r2, r3, #380
	ldr	r3, .L146+20
	add	r3, r2, r3
	str	r3, [fp, #-20]
	.loc 1 1265 0
	ldr	r1, .L146+20
	ldr	r2, [fp, #-8]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 1267 0
	ldr	r3, .L146+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L146+12
	ldr	r3, .L146+28
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1268 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_index_for_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_group_at_this_index
	mov	r3, r0
	mov	r0, r3
	mov	r1, #0
	bl	POC_get_budget
	mov	r2, r0
	ldr	r3, [fp, #-20]
	str	r2, [r3, #4]
	.loc 1 1269 0
	ldr	r3, .L146+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1277 0
	ldr	r2, [fp, #-8]
	ldr	r1, .L146+32
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #5
	sub	r2, fp, #4
	add	r3, r2, r3
	add	r2, r3, r1
	ldr	r3, [fp, #-20]
	mov	r1, r2
	mov	r2, r3
	mov	r3, #96
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	memcpy
	b	.L143
.L145:
	.loc 1 1256 0
	mov	r0, r0	@ nop
.L143:
	.loc 1 1251 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L141:
	.loc 1 1251 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L144
	.loc 1 1283 0 is_stmt 1
	ldrh	r3, [fp, #8]
	str	r3, [fp, #-36]
	.loc 1 1286 0
	sub	r3, fp, #1328
	sub	r3, r3, #4
	sub	r3, r3, #8
	ldr	r0, [fp, #-12]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 1288 0
	sub	r2, fp, #1328
	sub	r2, r2, #4
	sub	r2, r2, #8
	mov	r3, #0
	str	r3, [sp, #4]
	ldrh	r3, [fp, #8]	@ movhi
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [fp, #4]
	ldr	r0, [fp, #-1344]
	mov	r1, r2
	add	r2, fp, #4
	bl	nm_BUDGET_predicted_volume
	str	r0, [fp, #-24]	@ float
	.loc 1 1289 0
	ldr	r0, [fp, #-12]
	bl	nm_SYSTEM_get_used
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fsts	s15, [fp, #-28]
	.loc 1 1291 0
	ldr	r3, .L146+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1295 0
	ldr	r0, [fp, #-1344]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	mov	r3, r0
	add	r3, r3, #14080
	add	r3, r3, #48
	str	r3, [fp, #-32]
	.loc 1 1297 0
	ldr	r3, [fp, #-32]
	ldr	r2, [fp, #-1344]
	str	r2, [r3, #0]
	.loc 1 1299 0
	ldr	r3, [fp, #-1348]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-32]
	str	r2, [r3, #4]
	.loc 1 1301 0
	ldr	r3, [fp, #-1348]
	ldr	r2, [r3, #108]
	ldr	r3, [fp, #-32]
	str	r2, [r3, #8]
	.loc 1 1303 0
	ldr	r3, [fp, #-1348]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-32]
	str	r2, [r3, #12]
	.loc 1 1305 0
	ldr	r3, [fp, #-1348]
	ldr	r2, [r3, #16]
	ldr	r3, [fp, #-32]
	str	r2, [r3, #16]
	.loc 1 1307 0
	ldr	r3, [fp, #-1348]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-32]
	str	r2, [r3, #20]
	.loc 1 1309 0
	flds	s14, [fp, #-24]
	flds	s15, [fp, #-28]
	fadds	s14, s14, s15
	flds	s15, .L146
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	ldr	r3, [fp, #-32]
	str	r2, [r3, #24]
	.loc 1 1317 0
	ldr	r3, [fp, #-32]
	sub	ip, fp, #1216
	sub	ip, ip, #4
	sub	ip, ip, #8
	mov	lr, r3
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr, {r0, r1}
	stmia	ip, {r0, r1}
	.loc 1 1319 0
	sub	r3, fp, #1216
	sub	r3, r3, #4
	sub	r3, r3, #8
	mov	r0, r3
	bl	nm_BUDGET_REPORT_DATA_close_and_start_a_new_record
	.loc 1 1320 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, lr}
	add	sp, sp, #8
	bx	lr
.L147:
	.align	2
.L146:
	.word	1056964608
	.word	1196
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	1240
	.word	poc_preserves
	.word	list_poc_recursive_MUTEX
	.word	1267
	.word	-1184
.LFE14:
	.size	nm_handle_sending_budget_record, .-nm_handle_sending_budget_record
	.section	.text.nm_handle_budget_value_rollback,"ax",%progbits
	.align	2
	.type	nm_handle_budget_value_rollback, %function
nm_handle_budget_value_rollback:
.LFB15:
	.loc 1 1328 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI50:
	add	fp, sp, #8
.LCFI51:
	sub	sp, sp, #20
.LCFI52:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	.loc 1 1337 0
	ldr	r3, .L151
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L151+4
	ldr	r3, .L151+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1339 0
	ldr	r1, .L151+12
	ldr	r2, [fp, #-24]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_index_for_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-16]
	.loc 1 1345 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #8]
	sub	r3, r3, #1
	ldr	r0, [fp, #-16]
	mov	r1, r3
	bl	POC_get_budget
	str	r0, [fp, #-20]
	.loc 1 1348 0
	mov	r3, #22
	str	r3, [fp, #-12]
	b	.L149
.L150:
	.loc 1 1350 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	mov	r4, r3
	ldr	r3, [fp, #-12]
	ldr	r0, [fp, #-16]
	mov	r1, r3
	bl	POC_get_budget
	mov	r3, r0
	ldr	r0, [fp, #-16]
	mov	r1, r4
	mov	r2, r3
	bl	POC_set_budget
	.loc 1 1348 0 discriminator 2
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
.L149:
	.loc 1 1348 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bge	.L150
	.loc 1 1354 0 is_stmt 1
	ldr	r0, [fp, #-16]
	mov	r1, #0
	ldr	r2, [fp, #-20]
	bl	POC_set_budget
	.loc 1 1356 0
	ldr	r3, .L151
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1357 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L152:
	.align	2
.L151:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	1337
	.word	poc_preserves
.LFE15:
	.size	nm_handle_budget_value_rollback, .-nm_handle_budget_value_rollback
	.section	.text.nm_handle_budget_value_updates,"ax",%progbits
	.align	2
	.type	nm_handle_budget_value_updates, %function
nm_handle_budget_value_updates:
.LFB16:
	.loc 1 1364 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI53:
	add	fp, sp, #8
.LCFI54:
	sub	sp, sp, #20
.LCFI55:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	.loc 1 1373 0
	ldr	r3, .L156
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L156+4
	ldr	r3, .L156+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1375 0
	ldr	r1, .L156+12
	ldr	r2, [fp, #-24]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_index_for_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-16]
	.loc 1 1381 0
	ldr	r0, [fp, #-16]
	mov	r1, #0
	bl	POC_get_budget
	str	r0, [fp, #-20]
	.loc 1 1384 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L154
.L155:
	.loc 1 1386 0 discriminator 2
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	mov	r4, r3
	ldr	r3, [fp, #-12]
	ldr	r0, [fp, #-16]
	mov	r1, r3
	bl	POC_get_budget
	mov	r3, r0
	ldr	r0, [fp, #-16]
	mov	r1, r4
	mov	r2, r3
	bl	POC_set_budget
	.loc 1 1384 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L154:
	.loc 1 1384 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #23
	ble	.L155
	.loc 1 1390 0 is_stmt 1
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #8]
	sub	r3, r3, #1
	ldr	r0, [fp, #-16]
	mov	r1, r3
	ldr	r2, [fp, #-20]
	bl	POC_set_budget
	.loc 1 1392 0
	ldr	r3, .L156
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1393 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L157:
	.align	2
.L156:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	1373
	.word	poc_preserves
.LFE16:
	.size	nm_handle_budget_value_updates, .-nm_handle_budget_value_updates
	.section	.text.nm_handle_budget_period_rollback,"ax",%progbits
	.align	2
	.type	nm_handle_budget_period_rollback, %function
nm_handle_budget_period_rollback:
.LFB17:
	.loc 1 1399 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, fp, lr}
.LCFI56:
	add	fp, sp, #16
.LCFI57:
	sub	sp, sp, #48
.LCFI58:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	.loc 1 1409 0
	mov	r3, #22
	str	r3, [fp, #-20]
	b	.L159
.L160:
	.loc 1 1411 0 discriminator 2
	ldr	r3, [fp, #-20]
	add	r2, r3, #1
	ldr	r3, [fp, #-48]
	ldr	r1, [fp, #-20]
	add	r1, r1, #3
	ldr	r1, [r3, r1, asl #2]
	ldr	r3, [fp, #-48]
	add	r2, r2, #3
	str	r1, [r3, r2, asl #2]
	.loc 1 1409 0 discriminator 2
	ldr	r3, [fp, #-20]
	sub	r3, r3, #1
	str	r3, [fp, #-20]
.L159:
	.loc 1 1409 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bge	.L160
	.loc 1 1415 0 is_stmt 1
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #16]
	str	r3, [fp, #-24]
	.loc 1 1416 0
	sub	r1, fp, #28
	sub	r2, fp, #32
	sub	r3, fp, #36
	sub	r0, fp, #40
	str	r0, [sp, #0]
	ldr	r0, [fp, #-24]
	bl	DateToDMY
	.loc 1 1417 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #8]
	cmp	r3, #12
	bne	.L161
	.loc 1 1419 0
	ldr	r3, [fp, #-32]
	sub	r3, r3, #1
	str	r3, [fp, #-32]
	b	.L162
.L161:
	.loc 1 1423 0
	ldr	r3, [fp, #-32]
	sub	r3, r3, #2
	str	r3, [fp, #-32]
.L162:
	.loc 1 1427 0
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	bge	.L163
	.loc 1 1429 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #11
	str	r3, [fp, #-32]
	.loc 1 1430 0
	ldr	r3, [fp, #-36]
	sub	r3, r3, #1
	str	r3, [fp, #-36]
.L163:
	.loc 1 1432 0
	ldr	r1, [fp, #-28]
	ldr	r2, [fp, #-32]
	ldr	r3, [fp, #-36]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	DMYToDate
	str	r0, [fp, #-24]
	.loc 1 1435 0
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-24]
	str	r2, [r3, #12]
	.loc 1 1440 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L164
.L165:
	.loc 1 1442 0 discriminator 2
	ldr	r5, [fp, #-20]
	.loc 1 1444 0 discriminator 2
	ldr	r3, [fp, #-48]
	.loc 1 1442 0 discriminator 2
	ldr	r2, [fp, #-20]
	add	r2, r2, #3
	ldr	r4, [r3, r2, asl #2]
	bl	FLOWSENSE_get_controller_index
	mov	r6, r0
	ldr	r0, [fp, #-44]
	mov	r1, #9
	bl	SYSTEM_get_change_bits_ptr
	mov	r3, r0
	mov	r2, #9
	str	r2, [sp, #0]
	str	r6, [sp, #4]
	mov	r2, #1
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	ldr	r0, [fp, #-44]
	mov	r1, r5
	mov	r2, r4
	mov	r3, #0
	bl	nm_SYSTEM_set_budget_period
	.loc 1 1440 0 discriminator 2
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L164:
	.loc 1 1440 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #23
	ble	.L165
	.loc 1 1451 0 is_stmt 1
	sub	sp, fp, #16
	ldmfd	sp!, {r4, r5, r6, fp, pc}
.LFE17:
	.size	nm_handle_budget_period_rollback, .-nm_handle_budget_period_rollback
	.section	.text.nm_handle_budget_period_updates,"ax",%progbits
	.align	2
	.type	nm_handle_budget_period_updates, %function
nm_handle_budget_period_updates:
.LFB18:
	.loc 1 1457 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI59:
	add	fp, sp, #12
.LCFI60:
	sub	sp, sp, #48
.LCFI61:
	str	r0, [fp, #-40]
	str	r1, [fp, #-44]
	.loc 1 1468 0
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L167
.L168:
	.loc 1 1470 0 discriminator 2
	ldr	r3, [fp, #-16]
	sub	r2, r3, #1
	ldr	r3, [fp, #-44]
	ldr	r1, [fp, #-16]
	add	r1, r1, #3
	ldr	r1, [r3, r1, asl #2]
	ldr	r3, [fp, #-44]
	add	r2, r2, #3
	str	r1, [r3, r2, asl #2]
	.loc 1 1468 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L167:
	.loc 1 1468 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #23
	bls	.L168
	.loc 1 1475 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L169
.L170:
	.loc 1 1477 0 discriminator 2
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-16]
	add	r2, r2, #3
	ldr	r3, [r3, r2, asl #2]
	str	r3, [fp, #-20]
	.loc 1 1478 0 discriminator 2
	sub	r1, fp, #24
	sub	r2, fp, #28
	sub	r3, fp, #32
	sub	r0, fp, #36
	str	r0, [sp, #0]
	ldr	r0, [fp, #-20]
	bl	DateToDMY
	.loc 1 1479 0 discriminator 2
	ldr	r3, [fp, #-32]
	add	r3, r3, #1
	str	r3, [fp, #-32]
	.loc 1 1480 0 discriminator 2
	ldr	r1, [fp, #-24]
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-32]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	DMYToDate
	str	r0, [fp, #-20]
	.loc 1 1483 0 discriminator 2
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-16]
	add	r2, r2, r3
	ldr	r3, [fp, #-44]
	add	r2, r2, #3
	ldr	r1, [fp, #-20]
	str	r1, [r3, r2, asl #2]
	.loc 1 1475 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L169:
	.loc 1 1475 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bhi	.L170
	.loc 1 1487 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L171
.L172:
	.loc 1 1491 0 discriminator 2
	ldr	r3, [fp, #-44]
	.loc 1 1489 0 discriminator 2
	ldr	r2, [fp, #-16]
	add	r2, r2, #3
	ldr	r4, [r3, r2, asl #2]
	bl	FLOWSENSE_get_controller_index
	mov	r5, r0
	ldr	r0, [fp, #-40]
	mov	r1, #9
	bl	SYSTEM_get_change_bits_ptr
	mov	r3, r0
	mov	r2, #9
	str	r2, [sp, #0]
	str	r5, [sp, #4]
	mov	r2, #1
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	ldr	r0, [fp, #-40]
	ldr	r1, [fp, #-16]
	mov	r2, r4
	mov	r3, #0
	bl	nm_SYSTEM_set_budget_period
	.loc 1 1487 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L171:
	.loc 1 1487 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #23
	bls	.L172
	.loc 1 1498 0 is_stmt 1
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.LFE18:
	.size	nm_handle_budget_period_updates, .-nm_handle_budget_period_updates
	.section	.text.nm_BUDGET_increment_off_at_start_time,"ax",%progbits
	.align	2
	.type	nm_BUDGET_increment_off_at_start_time, %function
nm_BUDGET_increment_off_at_start_time:
.LFB19:
	.loc 1 1507 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI62:
	add	fp, sp, #4
.LCFI63:
	sub	sp, sp, #16
.LCFI64:
	str	r0, [fp, #-20]
	.loc 1 1514 0
	ldr	r0, [fp, #-20]
	bl	nm_GROUP_get_group_ID
	str	r0, [fp, #-12]
	.loc 1 1516 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L174
.L176:
.LBB6:
	.loc 1 1518 0
	ldr	r3, [fp, #-8]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L177
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 1521 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L175
	.loc 1 1522 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #24]
	ldr	r2, [r3, #0]
	.loc 1 1521 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bne	.L175
	.loc 1 1524 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #452]
	add	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #452]
.L175:
.LBE6:
	.loc 1 1516 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L174:
	.loc 1 1516 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L176
	.loc 1 1527 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L178:
	.align	2
.L177:
	.word	poc_preserves+20
.LFE19:
	.size	nm_BUDGET_increment_off_at_start_time, .-nm_BUDGET_increment_off_at_start_time
	.section	.text.nm_BUDGET_increment_on_at_start_time,"ax",%progbits
	.align	2
	.type	nm_BUDGET_increment_on_at_start_time, %function
nm_BUDGET_increment_on_at_start_time:
.LFB20:
	.loc 1 1530 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI65:
	add	fp, sp, #4
.LCFI66:
	sub	sp, sp, #16
.LCFI67:
	str	r0, [fp, #-20]
	.loc 1 1537 0
	ldr	r0, [fp, #-20]
	bl	nm_GROUP_get_group_ID
	str	r0, [fp, #-12]
	.loc 1 1539 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L180
.L182:
.LBB7:
	.loc 1 1541 0
	ldr	r3, [fp, #-8]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L183
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 1544 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L181
	.loc 1 1545 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #24]
	ldr	r2, [r3, #0]
	.loc 1 1544 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bne	.L181
	.loc 1 1547 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #448]
	add	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #448]
.L181:
.LBE7:
	.loc 1 1539 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L180:
	.loc 1 1539 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L182
	.loc 1 1550 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L184:
	.align	2
.L183:
	.word	poc_preserves+20
.LFE20:
	.size	nm_BUDGET_increment_on_at_start_time, .-nm_BUDGET_increment_on_at_start_time
	.section .rodata
	.align	2
.LC2:
	.ascii	"Invalid budget mode: %d, POC gid: %d\000"
	.align	2
.LC3:
	.ascii	"Calc_time (%d): M: %d R: %0.2f Vp: %0.0f\000"
	.section	.text.nm_BUDGET_calc_time_adjustment,"ax",%progbits
	.align	2
	.global	nm_BUDGET_calc_time_adjustment
	.type	nm_BUDGET_calc_time_adjustment, %function
nm_BUDGET_calc_time_adjustment:
.LFB21:
	.loc 1 1573 0
	@ args = 0, pretend = 0, frame = 156
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI68:
	fstmfdd	sp!, {d8}
.LCFI69:
	add	fp, sp, #16
.LCFI70:
	sub	sp, sp, #168
.LCFI71:
	str	r0, [fp, #-160]	@ float
	str	r1, [fp, #-164]
	sub	r1, fp, #172
	stmia	r1, {r2, r3}
	.loc 1 1594 0
	ldr	r3, .L202+4	@ float
	str	r3, [fp, #-24]	@ float
	.loc 1 1596 0
	ldr	r3, [fp, #-160]	@ float
	str	r3, [fp, #-20]	@ float
	.loc 1 1599 0
	ldr	r0, [fp, #-164]
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	str	r0, [fp, #-28]
	.loc 1 1601 0
	ldr	r0, [fp, #-28]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [fp, #-32]
	.loc 1 1603 0
	ldr	r0, [fp, #-28]
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-36]
	.loc 1 1605 0
	sub	r3, fp, #156
	ldr	r0, [fp, #-36]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 1611 0
	ldr	r3, [fp, #-156]
	cmp	r3, #1
	bne	.L186
	.loc 1 1612 0 discriminator 1
	sub	r3, fp, #156
	mov	r0, r3
	sub	r3, fp, #172
	ldmia	r3, {r1, r2}
	bl	SYSTEM_get_budget_period_index
	mov	r3, r0
	.loc 1 1611 0 discriminator 1
	cmp	r3, #0
	bne	.L186
	.loc 1 1613 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #468]
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	and	r3, r3, #255
	.loc 1 1612 0
	cmp	r3, #0
	ble	.L186
	.loc 1 1614 0
	ldr	r0, [fp, #-36]
	mov	r1, #0
	bl	nm_SYSTEM_get_budget
	mov	r3, r0
	.loc 1 1613 0
	cmp	r3, #0
	beq	.L186
	.loc 1 1616 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L188
	cmp	r3, #1
	beq	.L189
	b	.L199
.L188:
	.loc 1 1620 0
	ldr	r3, [fp, #-48]
	ldr	r0, [fp, #-160]	@ float
	mov	r1, r3
	ldr	r2, [fp, #-36]
	ldr	r3, [fp, #-164]
	bl	calc_time_adjustment_low_level
	str	r0, [fp, #-20]	@ float
	.loc 1 1622 0
	flds	s14, [fp, #-20]
	flds	s15, [fp, #-160]
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L190
	.loc 1 1625 0
	ldr	r3, .L202+8
	ldrh	r3, [r3, #4]
	cmp	r3, #0
	beq	.L191
	.loc 1 1626 0 discriminator 1
	sub	r3, fp, #172
	mov	r0, r3
	ldr	r1, .L202+8
	bl	DT1_IsBiggerThan_DT2
	mov	r3, r0
	.loc 1 1625 0 discriminator 1
	cmp	r3, #0
	bne	.L191
	.loc 1 1627 0
	ldr	r3, .L202+12
	ldr	r3, [r3, #0]
	.loc 1 1626 0
	ldr	r2, [fp, #-28]
	cmp	r2, r3
	beq	.L192
.L191:
	.loc 1 1633 0
	ldr	r3, .L202+8
	sub	r2, fp, #172
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 1634 0
	ldr	r3, .L202+12
	ldr	r2, [fp, #-28]
	str	r2, [r3, #0]
.L192:
	.loc 1 1637 0
	ldr	r3, [fp, #-160]	@ float
	str	r3, [fp, #-20]	@ float
.L190:
	.loc 1 1640 0
	ldr	r3, .L202+4	@ float
	str	r3, [fp, #-24]	@ float
	.loc 1 1642 0
	b	.L193
.L189:
	.loc 1 1646 0
	ldr	r3, [fp, #-48]
	ldr	r0, [fp, #-160]	@ float
	mov	r1, r3
	ldr	r2, [fp, #-36]
	ldr	r3, [fp, #-164]
	bl	calc_time_adjustment_low_level
	str	r0, [fp, #-20]	@ float
	.loc 1 1649 0
	flds	s14, [fp, #-20]
	flds	s15, [fp, #-160]
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L200
	.loc 1 1651 0
	flds	s14, [fp, #-160]
	flds	s15, [fp, #-20]
	fsubs	s16, s14, s15
	ldr	r0, [fp, #-164]
	bl	nm_STATION_get_expected_flow_rate_gpm
	mov	r3, r0
	fmsr	s12, r3	@ int
	fuitos	s15, s12
	fmuls	s14, s16, s15
	flds	s15, .L202
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-24]
	.loc 1 1653 0
	b	.L200
.L199:
	.loc 1 1692 0
	ldr	r3, [fp, #-48]
	ldr	r0, .L202+16
	mov	r1, r3
	ldr	r2, [fp, #-28]
	bl	Alert_Message_va
	b	.L193
.L200:
	.loc 1 1653 0
	mov	r0, r0	@ nop
.L193:
	.loc 1 1696 0
	ldr	r3, .L202+8
	ldrh	r3, [r3, #4]
	cmp	r3, #0
	beq	.L195
	.loc 1 1697 0 discriminator 1
	sub	r3, fp, #172
	mov	r0, r3
	ldr	r1, .L202+8
	bl	DT1_IsBiggerThan_DT2
	mov	r3, r0
	.loc 1 1696 0 discriminator 1
	cmp	r3, #0
	bne	.L195
	.loc 1 1698 0
	ldr	r3, .L202+12
	ldr	r3, [r3, #0]
	.loc 1 1697 0
	ldr	r2, [fp, #-28]
	cmp	r2, r3
	beq	.L201
.L195:
	.loc 1 1701 0
	ldr	r0, [fp, #-36]
	bl	nm_SYSTEM_get_Vp
	str	r0, [fp, #-40]	@ float
	.loc 1 1702 0
	ldr	r0, [fp, #-36]
	bl	nm_SYSTEM_get_poc_ratio
	str	r0, [fp, #-44]	@ float
	.loc 1 1704 0
	ldr	r2, [fp, #-48]
	flds	s15, [fp, #-44]
	fcvtds	d6, s15
	fmrrd	r3, r4, d6
	flds	s15, [fp, #-40]
	fcvtds	d7, s15
	fstd	d7, [sp, #4]
	mov	r1, r4
	str	r1, [sp, #0]
	ldr	r0, .L202+20
	ldr	r1, [fp, #-28]
	bl	Alert_Message_va
	.loc 1 1708 0
	ldr	r3, .L202+8
	sub	r2, fp, #172
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 1709 0
	ldr	r3, .L202+12
	ldr	r2, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 1696 0
	b	.L201
.L186:
	.loc 1 1716 0
	ldr	r3, [fp, #-160]	@ float
	str	r3, [fp, #-20]	@ float
	.loc 1 1717 0
	ldr	r3, .L202+4	@ float
	str	r3, [fp, #-24]	@ float
	b	.L197
.L201:
	.loc 1 1696 0
	mov	r0, r0	@ nop
.L197:
	.loc 1 1724 0
	ldr	r2, [fp, #-32]
	ldr	r3, .L202+24
	ldr	r3, [r2, r3]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, [fp, #-24]
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	ldr	r2, [fp, #-32]
	ldr	r3, .L202+24
	str	r1, [r2, r3]
	.loc 1 1727 0
	flds	s14, [fp, #-20]
	flds	s15, [fp, #-160]
	fdivs	s15, s14, s15
	ldr	r2, [fp, #-32]
	ldr	r3, .L202+28
	add	r3, r2, r3
	fsts	s15, [r3, #0]
	.loc 1 1730 0
	ldr	r2, [fp, #-32]
	ldr	r3, .L202+28
	add	r3, r2, r3
	ldr	r3, [r3, #0]	@ float
	mov	r0, r3	@ float
	bl	__float32_isnormal
	mov	r3, r0
	cmp	r3, #0
	bne	.L198
	.loc 1 1732 0
	ldr	r2, [fp, #-32]
	ldr	r3, .L202+28
	add	r3, r2, r3
	ldr	r2, .L202+4	@ float
	str	r2, [r3, #0]	@ float
.L198:
	.loc 1 1735 0
	ldr	r3, [fp, #-20]	@ float
	.loc 1 1736 0
	mov	r0, r3	@ float
	sub	sp, fp, #16
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, fp, pc}
.L203:
	.align	2
.L202:
	.word	1114636288
	.word	0
	.word	dt_flag.8209
	.word	saved_gid.8210
	.word	.LC2
	.word	.LC3
	.word	14156
	.word	14160
.LFE21:
	.size	nm_BUDGET_calc_time_adjustment, .-nm_BUDGET_calc_time_adjustment
	.section	.text.nm_BUDGET_predicted_volume,"ax",%progbits
	.align	2
	.global	nm_BUDGET_predicted_volume
	.type	nm_BUDGET_predicted_volume, %function
nm_BUDGET_predicted_volume:
.LFB22:
	.loc 1 1749 0
	@ args = 12, pretend = 4, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	sub	sp, sp, #4
.LCFI72:
	stmfd	sp!, {fp, lr}
.LCFI73:
	add	fp, sp, #4
.LCFI74:
	sub	sp, sp, #24
.LCFI75:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	str	r3, [fp, #4]
	.loc 1 1754 0
	ldr	r3, .L206	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 1756 0
	bl	NETWORK_CONFIG_get_controller_off
	mov	r3, r0
	cmp	r3, #0
	bne	.L205
	.loc 1 1759 0
	ldr	r3, [fp, #12]
	str	r3, [sp, #4]
	ldrh	r3, [fp, #8]	@ movhi
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [fp, #4]
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-20]
	bl	getScheduledVp
	fmsr	s14, r0
	flds	s15, [fp, #-8]
	fadds	s15, s15, s14
	fsts	s15, [fp, #-8]
	.loc 1 1762 0
	ldr	r0, [fp, #-12]
	bl	getIrrigationListVp
	fmsr	s14, r0
	flds	s15, [fp, #-8]
	fadds	s15, s15, s14
	fsts	s15, [fp, #-8]
.L205:
	.loc 1 1765 0
	ldr	r3, [fp, #-8]	@ float
	.loc 1 1766 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, lr}
	add	sp, sp, #4
	bx	lr
.L207:
	.align	2
.L206:
	.word	0
.LFE22:
	.size	nm_BUDGET_predicted_volume, .-nm_BUDGET_predicted_volume
	.section	.text.BUDGET_calculate_ratios,"ax",%progbits
	.align	2
	.global	BUDGET_calculate_ratios
	.type	BUDGET_calculate_ratios, %function
BUDGET_calculate_ratios:
.LFB23:
	.loc 1 1787 0
	@ args = 0, pretend = 0, frame = 148
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI76:
	add	fp, sp, #4
.LCFI77:
	sub	sp, sp, #156
.LCFI78:
	str	r0, [fp, #-148]
	str	r1, [fp, #-152]
	.loc 1 1806 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 1808 0
	ldr	r3, .L217
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L217+4
	mov	r3, #1808
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1810 0
	ldr	r3, .L217+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L217+4
	ldr	r3, .L217+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1812 0
	bl	SYSTEM_num_systems_in_use
	str	r0, [fp, #-16]
	.loc 1 1814 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L209
.L215:
	.loc 1 1816 0
	ldr	r0, [fp, #-8]
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-20]
	.loc 1 1819 0
	ldr	r0, [fp, #-20]
	bl	nm_GROUP_get_group_ID
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [fp, #-24]
	.loc 1 1820 0
	ldr	r3, .L217+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L217+4
	ldr	r3, .L217+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1821 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #468]
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	and	r3, r3, #255
	str	r3, [fp, #-28]
	.loc 1 1822 0
	ldr	r3, .L217+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1824 0
	sub	r3, fp, #144
	ldr	r0, [fp, #-20]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 1828 0
	ldr	r3, [fp, #-144]
	cmp	r3, #0
	beq	.L210
	.loc 1 1829 0 discriminator 1
	sub	r2, fp, #144
	ldr	r3, [fp, #-148]
	mov	r0, r2
	ldmia	r3, {r1, r2}
	bl	SYSTEM_get_budget_period_index
	mov	r3, r0
	.loc 1 1828 0 discriminator 1
	cmp	r3, #0
	bne	.L210
	.loc 1 1829 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L211
.L210:
	.loc 1 1832 0
	ldr	r0, [fp, #-20]
	ldr	r1, .L217+24	@ float
	bl	nm_SYSTEM_set_Vp
	.loc 1 1834 0
	ldr	r0, [fp, #-20]
	ldr	r1, .L217+24	@ float
	bl	nm_SYSTEM_set_poc_ratio
	.loc 1 1837 0
	ldr	r3, [fp, #-152]
	cmp	r3, #0
	beq	.L213
	.loc 1 1839 0
	ldr	r3, .L217+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L217+4
	ldr	r3, .L217+32
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1842 0
	ldr	r0, [fp, #-20]
	bl	nm_BUDGET_increment_off_at_start_time
	.loc 1 1844 0
	ldr	r3, .L217+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1837 0
	b	.L213
.L211:
	.loc 1 1850 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 1852 0
	ldr	r3, .L217+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L217+4
	ldr	r3, .L217+36
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1855 0
	ldr	r3, [fp, #-152]
	cmp	r3, #0
	beq	.L214
	.loc 1 1858 0
	ldr	r0, [fp, #-20]
	bl	nm_BUDGET_increment_on_at_start_time
.L214:
	.loc 1 1862 0
	ldr	r0, [fp, #-20]
	bl	nm_calc_POC_ratios
	.loc 1 1864 0
	ldr	r3, .L217+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1867 0
	ldr	r0, [fp, #-20]
	bl	nm_GROUP_get_group_ID
	mov	r1, r0
	sub	r2, fp, #144
	ldr	r3, [fp, #-148]
	mov	r0, #0
	str	r0, [sp, #4]
	ldrh	r0, [r3, #4]	@ movhi
	strh	r0, [sp, #0]	@ movhi
	ldr	r3, [r3, #0]
	mov	r0, r1
	mov	r1, r2
	ldr	r2, [fp, #-148]
	bl	nm_BUDGET_predicted_volume
	str	r0, [fp, #-32]	@ float
	.loc 1 1869 0
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-32]	@ float
	bl	nm_SYSTEM_set_Vp
.L213:
	.loc 1 1814 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L209:
	.loc 1 1814 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcc	.L215
	.loc 1 1876 0 is_stmt 1
	ldr	r3, .L217+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1878 0
	ldr	r3, .L217
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1881 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L208
	.loc 1 1883 0
	mov	r0, #20
	bl	WEATHER_TABLES_set_et_ratio
.L208:
	.loc 1 1885 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L218:
	.align	2
.L217:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	list_program_data_recursive_MUTEX
	.word	1810
	.word	system_preserves_recursive_MUTEX
	.word	1820
	.word	0
	.word	poc_preserves_recursive_MUTEX
	.word	1839
	.word	1852
.LFE23:
	.size	BUDGET_calculate_ratios, .-BUDGET_calculate_ratios
	.section	.text.BUDGET_in_effect_for_any_system,"ax",%progbits
	.align	2
	.global	BUDGET_in_effect_for_any_system
	.type	BUDGET_in_effect_for_any_system, %function
BUDGET_in_effect_for_any_system:
.LFB24:
	.loc 1 1896 0
	@ args = 0, pretend = 0, frame = 136
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI79:
	fstmfdd	sp!, {d8}
.LCFI80:
	add	fp, sp, #12
.LCFI81:
	sub	sp, sp, #136
.LCFI82:
	.loc 1 1913 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 1915 0
	ldr	r3, .L224+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L224+8
	ldr	r3, .L224+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1917 0
	bl	SYSTEM_num_systems_in_use
	str	r0, [fp, #-24]
	.loc 1 1918 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L220
.L223:
	.loc 1 1920 0
	ldr	r0, [fp, #-16]
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-28]
	.loc 1 1923 0
	ldr	r0, [fp, #-28]
	bl	nm_GROUP_get_group_ID
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [fp, #-32]
	.loc 1 1924 0
	ldr	r3, [fp, #-32]
	ldr	r3, [r3, #468]
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	and	r3, r3, #255
	cmp	r3, #0
	ble	.L221
	.loc 1 1926 0
	sub	r3, fp, #148
	ldr	r0, [fp, #-28]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 1929 0
	ldr	r3, [fp, #-148]
	cmp	r3, #0
	beq	.L221
	.loc 1 1930 0 discriminator 1
	ldr	r3, [fp, #-40]
	.loc 1 1929 0 discriminator 1
	cmp	r3, #0
	beq	.L221
	.loc 1 1932 0
	ldr	r0, [fp, #-28]
	bl	nm_SYSTEM_get_Vp
	fmsr	s15, r0
	fcmpezs	s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L221
	.loc 1 1934 0
	ldr	r0, [fp, #-28]
	bl	nm_SYSTEM_get_poc_ratio
	fmsr	s16, r0
	ldr	r0, [fp, #-28]
	bl	nm_SYSTEM_get_Vp
	fmsr	s15, r0
	fdivs	s15, s16, s15
	fsts	s15, [fp, #-36]
	.loc 1 1936 0
	flds	s14, [fp, #-36]
	flds	s15, .L224
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L221
	.loc 1 1938 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 1940 0
	b	.L222
.L221:
	.loc 1 1918 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L220:
	.loc 1 1918 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bcc	.L223
.L222:
	.loc 1 1946 0 is_stmt 1
	ldr	r3, .L224+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1948 0
	ldr	r3, [fp, #-20]
	.loc 1 1949 0
	mov	r0, r3
	sub	sp, fp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {fp, pc}
.L225:
	.align	2
.L224:
	.word	1065353216
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	1915
.LFE24:
	.size	BUDGET_in_effect_for_any_system, .-BUDGET_in_effect_for_any_system
	.section	.text.BUDGET_handle_alerts_at_midnight,"ax",%progbits
	.align	2
	.global	BUDGET_handle_alerts_at_midnight
	.type	BUDGET_handle_alerts_at_midnight, %function
BUDGET_handle_alerts_at_midnight:
.LFB25:
	.loc 1 1961 0
	@ args = 20, pretend = 12, frame = 240
	@ frame_needed = 1, uses_anonymous_args = 0
	sub	sp, sp, #12
.LCFI83:
	stmfd	sp!, {r4, fp, lr}
.LCFI84:
	add	fp, sp, #8
.LCFI85:
	sub	sp, sp, #248
.LCFI86:
	str	r0, [fp, #-248]
	add	r0, fp, #4
	stmia	r0, {r1, r2, r3}
	.loc 1 1981 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 1984 0
	ldr	r0, [fp, #-248]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [fp, #-36]
	.loc 1 1986 0
	ldr	r3, .L244+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L244+12
	ldr	r3, .L244+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1989 0
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #468]
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	and	r3, r3, #255
	str	r3, [fp, #-40]
	.loc 1 1990 0
	ldr	r3, .L244+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1992 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L227
	.loc 1 1994 0
	ldr	r3, .L244+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L244+12
	ldr	r3, .L244+24
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1996 0
	ldr	r0, [fp, #-248]
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-44]
	.loc 1 1998 0
	sub	r3, fp, #196
	ldr	r0, [fp, #-44]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 2002 0
	ldr	r3, [fp, #-196]
	cmp	r3, #0
	beq	.L227
	.loc 1 2005 0
	sub	r3, fp, #244
	mov	r0, r3
	mov	r1, #0
	mov	r2, #48
	bl	memset
	.loc 1 2006 0
	sub	r3, fp, #244
	ldr	r0, [fp, #-248]
	mov	r1, r3
	bl	nm_BUDGET_calc_rpoc
	.loc 1 2011 0
	ldr	r3, [fp, #-88]
	.loc 1 2010 0
	cmp	r3, #0
	moveq	r3, #0
	movne	r3, #1
	sub	r2, fp, #196
	str	r3, [sp, #4]
	ldrh	r3, [fp, #8]	@ movhi
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [fp, #4]
	ldr	r0, [fp, #-248]
	mov	r1, r2
	add	r2, fp, #4
	bl	nm_BUDGET_predicted_volume
	str	r0, [fp, #-48]	@ float
	.loc 1 2013 0
	ldr	r3, .L244+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L244+12
	ldr	r3, .L244+32
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2015 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L228
.L242:
.LBB8:
	.loc 1 2017 0
	ldr	r3, [fp, #-12]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L244+36
	add	r3, r2, r3
	str	r3, [fp, #-52]
	.loc 1 2020 0
	ldr	r3, [fp, #-52]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L229
	.loc 1 2021 0 discriminator 1
	ldr	r3, [fp, #-52]
	ldr	r3, [r3, #24]
	ldr	r2, [r3, #0]
	.loc 1 2020 0 discriminator 1
	ldr	r3, [fp, #-248]
	cmp	r2, r3
	bne	.L229
	.loc 1 2021 0
	ldr	r3, [fp, #-248]
	cmp	r3, #0
	beq	.L229
	.loc 1 2023 0
	ldr	r0, [fp, #-12]
	bl	POC_use_for_budget
	mov	r3, r0
	.loc 1 2022 0
	cmp	r3, #0
	beq	.L229
	.loc 1 2025 0
	ldr	r3, .L244+40
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L244+12
	ldr	r3, .L244+44
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2026 0
	ldr	r3, [fp, #-52]
	ldr	r3, [r3, #4]
	mov	r0, r3
	bl	POC_get_index_for_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-56]
	.loc 1 2029 0
	ldr	r0, [fp, #-56]
	bl	POC_get_budget_gallons_entry_option
	str	r0, [fp, #-20]
	.loc 1 2032 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 2033 0
	ldr	r0, [fp, #-56]
	mov	r1, #0
	bl	POC_get_budget
	mov	r3, r0
	cmp	r3, #0
	beq	.L230
	.loc 1 2036 0
	mov	r3, #0
	str	r3, [fp, #-16]
.L230:
	.loc 1 2038 0
	ldr	r3, .L244+40
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2040 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L231
	.loc 1 2042 0
	ldr	r0, [fp, #-44]
	bl	nm_GROUP_get_name
	mov	r4, r0
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_name
	mov	r3, r0
	mov	r0, r4
	mov	r1, r3
	bl	Alert_budget_values_not_set
	b	.L229
.L231:
.LBB9:
	.loc 1 2046 0
	ldr	r3, [fp, #-52]
	ldr	r3, [r3, #4]
	mov	r0, r3
	bl	POC_get_index_for_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_group_at_this_index
	mov	r3, r0
	mov	r0, r3
	mov	r1, #0
	bl	POC_get_budget
	str	r0, [fp, #-60]
	.loc 1 2047 0
	ldr	r0, [fp, #-12]
	bl	nm_BUDGET_get_used
	fmdrr	d7, r0, r1
	fcvtsd	s15, d7
	fsts	s15, [fp, #-64]
	.loc 1 2048 0
	ldr	r3, [fp, #-60]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, [fp, #-64]
	fsubs	s14, s14, s15
	ldr	r2, [fp, #-12]
	mvn	r3, #235
	mov	r2, r2, asl #2
	sub	r1, fp, #8
	add	r2, r1, r2
	add	r3, r2, r3
	flds	s13, [r3, #0]
	flds	s15, [fp, #-48]
	fmuls	s15, s13, s15
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-68]
	.loc 1 2052 0
	flds	s14, .L244
	flds	s15, [fp, #-68]
	fsubs	s14, s14, s15
	flds	s15, .L244+4
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-24]
	.loc 1 2053 0
	flds	s14, [fp, #-24]
	flds	s15, .L244+4
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L232
	.loc 1 2055 0
	ldr	r3, .L244+4	@ float
	str	r3, [fp, #-24]	@ float
.L232:
	.loc 1 2064 0
	mov	r3, #1
	str	r3, [fp, #-28]
	.loc 1 2065 0
	bl	STATION_GROUP_get_num_groups_in_use
	str	r0, [fp, #-72]
.LBB10:
	.loc 1 2066 0
	mov	r3, #0
	str	r3, [fp, #-32]
	b	.L233
.L236:
.LBB11:
	.loc 1 2068 0
	ldr	r0, [fp, #-32]
	bl	STATION_GROUP_get_group_at_this_index
	str	r0, [fp, #-76]
	.loc 1 2069 0
	ldr	r0, [fp, #-76]
	bl	STATION_GROUP_get_GID_irrigation_system
	mov	r2, r0
	ldr	r3, [fp, #-248]
	cmp	r2, r3
	bne	.L234
.LBB12:
	.loc 1 2072 0
	ldr	r0, [fp, #-76]
	bl	STATION_GROUP_get_budget_reduction_limit
	str	r0, [fp, #-80]
	.loc 1 2073 0
	ldr	r3, [fp, #-80]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, [fp, #-24]
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L234
	.loc 1 2073 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-80]
	cmp	r3, #99
	bhi	.L234
	.loc 1 2075 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 2076 0
	b	.L235
.L234:
.LBE12:
.LBE11:
	.loc 1 2066 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #1
	str	r3, [fp, #-32]
.L233:
	.loc 1 2066 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-32]
	ldr	r3, [fp, #-72]
	cmp	r2, r3
	bcc	.L236
.L235:
.LBE10:
	.loc 1 2086 0 is_stmt 1
	flds	s14, [fp, #-68]
	flds	s15, .L244
	fcmpes	s14, s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L237
	.loc 1 2088 0
	ldr	r2, [fp, #-12]
	mvn	r3, #235
	mov	r2, r2, asl #2
	sub	r1, fp, #8
	add	r2, r1, r2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-48]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-64]
	fadds	s14, s14, s15
	flds	s15, .L244+4
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-60]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	flds	s14, .L244+4
	fsubs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-84]
	.loc 1 2089 0
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_name
	mov	r3, r0
	mov	r0, r3
	ldr	r1, [fp, #-84]
	bl	Alert_budget_under_budget
	b	.L229
.L237:
	.loc 1 2091 0
	flds	s15, [fp, #-68]
	fcmpezs	s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L238
	.loc 1 2095 0
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L239
	.loc 1 2095 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-88]
	cmp	r3, #0
	beq	.L239
	.loc 1 2097 0 is_stmt 1
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_name
	mov	r3, r0
	mov	r0, r3
	mov	r1, #0
	bl	Alert_budget_under_budget
	b	.L229
.L239:
	.loc 1 2102 0
	ldr	r2, [fp, #-12]
	mvn	r3, #235
	mov	r2, r2, asl #2
	sub	r1, fp, #8
	add	r2, r1, r2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-48]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-64]
	fadds	s14, s14, s15
	ldr	r3, [fp, #-60]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fsubs	s14, s14, s15
	flds	s15, .L244+4
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-60]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-84]
	.loc 1 2103 0
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_name
	mov	r3, r0
	mov	r0, r3
	ldr	r1, [fp, #-84]
	bl	Alert_budget_will_exceed_budget
	b	.L229
.L238:
	.loc 1 2111 0
	ldr	r2, [fp, #-184]
	ldrh	r3, [fp, #8]
	cmp	r2, r3
	bne	.L241
	.loc 1 2113 0
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r2, [fp, #-60]
	fmsr	s14, r2	@ int
	fuitos	s15, s14
	flds	s14, [fp, #-64]
	fsubs	s14, s14, s15
	flds	s15, .L244+4
	fmuls	s14, s14, s15
	ldr	r2, [fp, #-60]
	fmsr	s13, r2	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	mov	r0, r3
	mov	r1, r2
	bl	Alert_budget_over_budget_period_ending_today
	b	.L229
.L241:
	.loc 1 2118 0
	ldr	r2, [fp, #-12]
	mvn	r3, #235
	mov	r2, r2, asl #2
	sub	r1, fp, #8
	add	r2, r1, r2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-48]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-64]
	fadds	s14, s14, s15
	ldr	r3, [fp, #-60]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fsubs	s14, s14, s15
	flds	s15, .L244+4
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-60]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-84]
	.loc 1 2120 0
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r2, [fp, #-60]
	fmsr	s14, r2	@ int
	fuitos	s15, s14
	flds	s14, [fp, #-64]
	fsubs	s14, s14, s15
	flds	s15, .L244+4
	fmuls	s14, s14, s15
	ldr	r2, [fp, #-60]
	fmsr	s13, r2	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	mov	r0, r3
	mov	r1, r2
	ldr	r2, [fp, #-84]
	bl	Alert_budget_over_budget
.L229:
.LBE9:
.LBE8:
	.loc 1 2015 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L228:
	.loc 1 2015 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #11
	bls	.L242
	.loc 1 2130 0 is_stmt 1
	ldr	r3, [fp, #-20]
	cmp	r3, #2
	bne	.L243
	.loc 1 2132 0
	ldr	r0, [fp, #-44]
	mov	r1, #0
	bl	BUDGET_calc_POC_monthly_budget_using_et
.L243:
	.loc 1 2135 0
	ldr	r3, .L244+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L227:
	.loc 1 2140 0
	ldr	r3, .L244+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2141 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, lr}
	add	sp, sp, #12
	bx	lr
.L245:
	.align	2
.L244:
	.word	1065353216
	.word	1120403456
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	1986
	.word	list_system_recursive_MUTEX
	.word	1994
	.word	poc_preserves_recursive_MUTEX
	.word	2013
	.word	poc_preserves+20
	.word	list_poc_recursive_MUTEX
	.word	2025
.LFE25:
	.size	BUDGET_handle_alerts_at_midnight, .-BUDGET_handle_alerts_at_midnight
	.section	.text.BUDGET_handle_alerts_at_start_time,"ax",%progbits
	.align	2
	.global	BUDGET_handle_alerts_at_start_time
	.type	BUDGET_handle_alerts_at_start_time, %function
BUDGET_handle_alerts_at_start_time:
.LFB26:
	.loc 1 2146 0
	@ args = 0, pretend = 0, frame = 148
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI87:
	fstmfdd	sp!, {d8}
.LCFI88:
	add	fp, sp, #12
.LCFI89:
	sub	sp, sp, #148
.LCFI90:
	str	r0, [fp, #-156]
	str	r1, [fp, #-160]
	.loc 1 2158 0
	ldr	r0, [fp, #-156]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [fp, #-24]
	.loc 1 2160 0
	ldr	r3, .L251+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L251+12
	mov	r3, #2160
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2163 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #468]
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	and	r3, r3, #255
	str	r3, [fp, #-28]
	.loc 1 2164 0
	ldr	r3, .L251+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2166 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	beq	.L246
	.loc 1 2168 0
	ldr	r3, .L251+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L251+12
	ldr	r3, .L251+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2170 0
	ldr	r0, [fp, #-156]
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-32]
	.loc 1 2172 0
	sub	r3, fp, #152
	ldr	r0, [fp, #-32]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 2176 0
	ldr	r3, [fp, #-152]
	cmp	r3, #0
	beq	.L248
	.loc 1 2176 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L248
	.loc 1 2179 0 is_stmt 1
	ldr	r0, [fp, #-32]
	bl	nm_SYSTEM_get_poc_ratio
	fmsr	s16, r0
	ldr	r0, [fp, #-32]
	bl	nm_SYSTEM_get_Vp
	fmsr	s15, r0
	fdivs	s15, s16, s15
	fsts	s15, [fp, #-36]
	.loc 1 2180 0
	flds	s14, [fp, #-36]
	flds	s15, .L251
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L248
.LBB13:
	.loc 1 2184 0
	ldr	r0, [fp, #-160]
	bl	STATION_GROUP_get_budget_reduction_limit
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	flds	s14, .L251+4
	fsubs	s15, s14, s15
	fsts	s15, [fp, #-40]
	.loc 1 2185 0
	flds	s14, [fp, #-40]
	flds	s15, .L251+4
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-40]
	.loc 1 2187 0
	flds	s14, [fp, #-36]
	flds	s15, [fp, #-40]
	fcmpes	s14, s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L249
	.loc 1 2190 0
	flds	s14, .L251
	flds	s15, [fp, #-36]
	fsubs	s14, s14, s15
	flds	s15, .L251+4
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-16]
	.loc 1 2191 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L250
.L249:
	.loc 1 2196 0
	flds	s14, .L251
	flds	s15, [fp, #-40]
	fsubs	s14, s14, s15
	flds	s15, .L251+4
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-16]
	.loc 1 2200 0
	mov	r3, #1
	str	r3, [fp, #-20]
.L250:
	.loc 1 2202 0
	ldr	r0, [fp, #-160]
	bl	nm_GROUP_get_name
	mov	r3, r0
	mov	r0, r3
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-20]
	bl	Alert_budget_group_reduction
.L248:
.LBE13:
	.loc 1 2206 0
	ldr	r3, .L251+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L246:
	.loc 1 2209 0
	sub	sp, fp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {fp, pc}
.L252:
	.align	2
.L251:
	.word	1065353216
	.word	1120403456
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	list_system_recursive_MUTEX
	.word	2168
.LFE26:
	.size	BUDGET_handle_alerts_at_start_time, .-BUDGET_handle_alerts_at_start_time
	.section .rodata
	.align	2
.LC4:
	.ascii	"caught BUG DATE: %d/%d/%d\000"
	.align	2
.LC5:
	.ascii	"Budget Rollback Check: Too far back\000"
	.align	2
.LC6:
	.ascii	"Budget Rollover Check: BAD DATE DATA\000"
	.section	.text.BUDGET_timer_upkeep,"ax",%progbits
	.align	2
	.global	BUDGET_timer_upkeep
	.type	BUDGET_timer_upkeep, %function
BUDGET_timer_upkeep:
.LFB27:
	.loc 1 2225 0
	@ args = 20, pretend = 16, frame = 184
	@ frame_needed = 1, uses_anonymous_args = 0
	sub	sp, sp, #16
.LCFI91:
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI92:
	add	fp, sp, #12
.LCFI93:
	sub	sp, sp, #196
.LCFI94:
	add	ip, fp, #4
	stmia	ip, {r0, r1, r2, r3}
	.loc 1 2255 0
	sub	r3, fp, #68
	add	r2, fp, #4
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 2260 0
	ldr	r3, .L293
	ldrh	r3, [r3, #4]
	cmp	r3, #0
	beq	.L254
	.loc 1 2260 0 is_stmt 0 discriminator 1
	ldrh	r3, [fp, #-64]
	mov	r2, r3
	ldr	r3, .L293
	ldrh	r3, [r3, #4]
	rsb	r3, r3, r2
	mov	r0, r3
	bl	abs
	mov	r2, r0
	ldr	r3, .L293+4
	cmp	r2, r3
	ble	.L254
	.loc 1 2263 0 is_stmt 1
	ldrh	r3, [fp, #12]
	mov	r1, r3
	ldrh	r3, [fp, #10]
	mov	r2, r3
	ldrh	r3, [fp, #14]
	ldr	r0, .L293+8
	bl	Alert_Message_va
	.loc 1 2264 0
	ldr	r3, .L293
	sub	r2, fp, #68
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 2265 0
	b	.L253
.L254:
	.loc 1 2268 0
	ldr	r3, .L293
	sub	r2, fp, #68
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 2270 0
	ldr	r3, .L293+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L293+16
	ldr	r3, .L293+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2271 0
	ldr	r3, .L293+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L293+16
	ldr	r3, .L293+28
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2272 0
	ldr	r3, .L293+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L293+16
	mov	r3, #2272
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2275 0
	bl	SYSTEM_num_systems_in_use
	str	r0, [fp, #-28]
	.loc 1 2280 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L256
.L287:
	.loc 1 2282 0
	ldr	r0, [fp, #-16]
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-32]
	.loc 1 2284 0
	ldr	r0, [fp, #-32]
	bl	nm_GROUP_get_group_ID
	str	r0, [fp, #-36]
	.loc 1 2290 0
	ldr	r0, [fp, #-36]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [fp, #-40]
	.loc 1 2291 0
	ldr	r3, [fp, #-40]
	ldr	r3, [r3, #468]
	and	r3, r3, #122880
	cmp	r3, #0
	beq	.L288
.L257:
	.loc 1 2298 0
	sub	r3, fp, #196
	ldr	r0, [fp, #-32]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 2300 0
	ldr	r3, [fp, #-184]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-72]	@ movhi
	.loc 1 2301 0
	ldr	r3, [fp, #-192]
	str	r3, [fp, #-76]
	.loc 1 2303 0
	ldr	r3, [fp, #-180]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-80]	@ movhi
	.loc 1 2304 0
	ldr	r3, [fp, #-192]
	str	r3, [fp, #-84]
	.loc 1 2307 0
	sub	r2, fp, #76
	sub	r3, fp, #68
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThan_DT2
	mov	r3, r0
	cmp	r3, #0
	beq	.L259
	.loc 1 2310 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 2311 0
	b	.L260
.L267:
	.loc 1 2314 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
	.loc 1 2317 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L261
.L265:
.LBB14:
	.loc 1 2320 0
	ldr	r3, [fp, #-20]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L293+36
	add	r3, r2, r3
	str	r3, [fp, #-44]
	.loc 1 2323 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L289
.L262:
	.loc 1 2328 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #24]
	str	r3, [fp, #-40]
	.loc 1 2331 0
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bne	.L264
	.loc 1 2332 0 discriminator 1
	ldr	r2, [fp, #-40]
	ldr	r3, .L293+40
	ldr	r3, [r2, r3]
	.loc 1 2331 0 discriminator 1
	cmn	r3, #1
	beq	.L264
	.loc 1 2335 0
	ldr	r0, [fp, #-20]
	bl	nm_handle_budget_reset_record
	.loc 1 2337 0
	ldr	r2, [fp, #-40]
	ldr	r3, .L293+40
	mvn	r1, #0
	str	r1, [r2, r3]
.L264:
	.loc 1 2340 0
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bne	.L263
	.loc 1 2343 0
	sub	r3, fp, #196
	ldr	r0, [fp, #-20]
	mov	r1, r3
	bl	nm_handle_budget_value_rollback
	b	.L263
.L289:
	.loc 1 2325 0
	mov	r0, r0	@ nop
.L263:
.LBE14:
	.loc 1 2317 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L261:
	.loc 1 2317 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #11
	bls	.L265
	.loc 1 2347 0 is_stmt 1
	sub	r3, fp, #196
	ldr	r0, [fp, #-32]
	mov	r1, r3
	bl	nm_handle_budget_period_rollback
	.loc 1 2352 0
	ldr	r3, [fp, #-184]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-72]	@ movhi
	.loc 1 2353 0
	ldr	r3, [fp, #-192]
	str	r3, [fp, #-76]
.L260:
	.loc 1 2311 0 discriminator 1
	sub	r2, fp, #76
	sub	r3, fp, #68
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #0
	beq	.L266
	.loc 1 2311 0 is_stmt 0 discriminator 2
	ldr	r3, [fp, #-24]
	cmp	r3, #23
	bls	.L267
.L266:
	.loc 1 2356 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #24
	bne	.L258
	.loc 1 2358 0
	ldr	r0, .L293+44
	bl	Alert_Message
	b	.L258
.L259:
	.loc 1 2362 0
	sub	r2, fp, #84
	sub	r3, fp, #68
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThan_DT2
	mov	r3, r0
	cmp	r3, #0
	beq	.L268
	.loc 1 2364 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L269
.L273:
.LBB15:
	.loc 1 2367 0
	ldr	r3, [fp, #-20]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L293+36
	add	r3, r2, r3
	str	r3, [fp, #-48]
	.loc 1 2370 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L290
.L270:
	.loc 1 2375 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #24]
	str	r3, [fp, #-40]
	.loc 1 2378 0
	ldr	r2, [fp, #-40]
	ldr	r3, .L293+40
	ldr	r3, [r2, r3]
	cmp	r3, #0
	beq	.L272
	.loc 1 2379 0 discriminator 1
	ldr	r2, [fp, #-40]
	ldr	r3, .L293+40
	ldr	r3, [r2, r3]
	.loc 1 2378 0 discriminator 1
	cmn	r3, #1
	bne	.L271
.L272:
	.loc 1 2380 0
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #0]
	.loc 1 2379 0
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bne	.L271
	.loc 1 2383 0
	ldr	r0, [fp, #-20]
	bl	nm_handle_budget_reset_record
	.loc 1 2385 0
	ldrh	r3, [fp, #-64]
	mov	r1, r3
	ldr	r2, [fp, #-40]
	ldr	r3, .L293+40
	str	r1, [r2, r3]
	b	.L271
.L290:
	.loc 1 2372 0
	mov	r0, r0	@ nop
.L271:
.LBE15:
	.loc 1 2364 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L269:
	.loc 1 2364 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #11
	bls	.L273
	.loc 1 2390 0 is_stmt 1
	ldr	r3, [fp, #-196]
	cmp	r3, #0
	beq	.L258
	.loc 1 2393 0
	ldr	r2, [fp, #-68]
	ldr	r3, [fp, #-192]
	cmp	r2, r3
	bne	.L274
	.loc 1 2395 0
	sub	ip, fp, #196
	mov	r3, sp
	add	r2, fp, #12
	ldmia	r2, {r0, r1, r2}
	stmia	r3, {r0, r1, r2}
	add	r3, fp, #4
	ldmia	r3, {r2, r3}
	ldr	r0, [fp, #-36]
	mov	r1, ip
	bl	nm_handle_sending_budget_record
.L274:
	.loc 1 2399 0
	ldr	r3, [fp, #-68]
	cmp	r3, #0
	bne	.L258
	.loc 1 2401 0
	mov	r3, sp
	add	r2, fp, #16
	ldmia	r2, {r0, r1}
	stmia	r3, {r0, r1}
	add	r3, fp, #4
	ldmia	r3, {r1, r2, r3}
	ldr	r0, [fp, #-36]
	bl	BUDGET_handle_alerts_at_midnight
	b	.L258
.L268:
	.loc 1 2410 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 2411 0
	b	.L275
.L286:
	.loc 1 2413 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
	.loc 1 2420 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L276
.L279:
.LBB16:
	.loc 1 2423 0
	ldr	r3, [fp, #-20]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L293+36
	add	r3, r2, r3
	str	r3, [fp, #-52]
	.loc 1 2426 0
	ldr	r3, [fp, #-52]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L291
.L277:
	.loc 1 2431 0
	ldr	r3, [fp, #-52]
	ldr	r3, [r3, #24]
	str	r3, [fp, #-40]
	.loc 1 2433 0
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bne	.L278
	.loc 1 2435 0
	ldr	r2, [fp, #-40]
	ldr	r3, .L293+48
	mov	r1, #1
	str	r1, [r2, r3]
	.loc 1 2438 0
	ldr	r0, [fp, #-20]
	bl	POC_use_for_budget
	mov	r3, r0
	cmp	r3, #0
	beq	.L278
	.loc 1 2438 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-196]
	cmp	r3, #0
	beq	.L278
.LBB17:
	.loc 1 2441 0 is_stmt 1
	ldr	r3, .L293+52
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L293+16
	ldr	r3, .L293+56
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2442 0
	ldr	r3, [fp, #-52]
	ldr	r3, [r3, #4]
	mov	r0, r3
	bl	POC_get_index_for_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-56]
	.loc 1 2444 0
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_name
	mov	r5, r0
	ldr	r0, [fp, #-56]
	mov	r1, #0
	bl	POC_get_budget
	mov	r4, r0
	.loc 1 2446 0
	ldr	r0, [fp, #-20]
	bl	nm_BUDGET_get_used
	fmdrr	d7, r0, r1
	.loc 1 2444 0
	ftouizd	s13, d7
	fmrs	r3, s13	@ int
	mov	r0, r5
	mov	r1, r4
	mov	r2, r3
	bl	Alert_budget_period_ended
	.loc 1 2447 0
	ldr	r3, .L293+52
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L278
.L291:
.LBE17:
	.loc 1 2428 0
	mov	r0, r0	@ nop
.L278:
.LBE16:
	.loc 1 2420 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L276:
	.loc 1 2420 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #11
	bls	.L279
	.loc 1 2455 0 is_stmt 1
	ldr	r3, [fp, #-196]
	cmp	r3, #0
	beq	.L280
	.loc 1 2457 0
	sub	ip, fp, #196
	mov	r3, sp
	add	r2, fp, #12
	ldmia	r2, {r0, r1, r2}
	stmia	r3, {r0, r1, r2}
	add	r3, fp, #4
	ldmia	r3, {r2, r3}
	ldr	r0, [fp, #-36]
	mov	r1, ip
	bl	nm_handle_sending_budget_record
.L280:
	.loc 1 2461 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L281
.L284:
.LBB18:
	.loc 1 2464 0
	ldr	r3, [fp, #-20]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L293+36
	add	r3, r2, r3
	str	r3, [fp, #-60]
	.loc 1 2467 0
	ldr	r3, [fp, #-60]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L292
.L282:
	.loc 1 2472 0
	ldr	r1, .L293+60
	ldr	r2, [fp, #-20]
	mov	r3, #44
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-40]
	.loc 1 2474 0
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bne	.L283
	.loc 1 2477 0
	ldr	r0, [fp, #-20]
	bl	nm_handle_budget_reset_record
	.loc 1 2479 0
	ldr	r2, [fp, #-40]
	ldr	r3, .L293+40
	mov	r1, #0
	str	r1, [r2, r3]
	.loc 1 2482 0
	sub	r3, fp, #196
	ldr	r0, [fp, #-20]
	mov	r1, r3
	bl	nm_handle_budget_value_updates
	b	.L283
.L292:
	.loc 1 2469 0
	mov	r0, r0	@ nop
.L283:
.LBE18:
	.loc 1 2461 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L281:
	.loc 1 2461 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #11
	bls	.L284
	.loc 1 2487 0 is_stmt 1
	sub	r3, fp, #196
	ldr	r0, [fp, #-32]
	mov	r1, r3
	bl	nm_handle_budget_period_updates
	.loc 1 2492 0
	ldr	r3, [fp, #-180]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-80]	@ movhi
	.loc 1 2493 0
	ldr	r3, [fp, #-192]
	str	r3, [fp, #-84]
.L275:
	.loc 1 2411 0 discriminator 1
	sub	r2, fp, #68
	sub	r3, fp, #84
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #0
	beq	.L285
	.loc 1 2411 0 is_stmt 0 discriminator 2
	ldr	r3, [fp, #-24]
	cmp	r3, #23
	bls	.L286
.L285:
	.loc 1 2496 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #24
	bne	.L258
	.loc 1 2498 0
	ldr	r0, .L293+64
	bl	Alert_Message
	b	.L258
.L288:
	.loc 1 2293 0
	mov	r0, r0	@ nop
.L258:
	.loc 1 2280 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L256:
	.loc 1 2280 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bcc	.L287
	.loc 1 2504 0 is_stmt 1
	ldr	r3, .L293+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2505 0
	ldr	r3, .L293+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2506 0
	ldr	r3, .L293+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L253:
	.loc 1 2507 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, lr}
	add	sp, sp, #16
	bx	lr
.L294:
	.align	2
.L293:
	.word	dt_bug_check.8299
	.word	365
	.word	.LC4
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	2270
	.word	system_preserves_recursive_MUTEX
	.word	2271
	.word	poc_preserves_recursive_MUTEX
	.word	poc_preserves+20
	.word	14172
	.word	.LC5
	.word	14164
	.word	list_poc_recursive_MUTEX
	.word	2441
	.word	poc_preserves
	.word	.LC6
.LFE27:
	.size	BUDGET_timer_upkeep, .-BUDGET_timer_upkeep
	.section	.text.nm_BUDGET_get_used,"ax",%progbits
	.align	2
	.global	nm_BUDGET_get_used
	.type	nm_BUDGET_get_used, %function
nm_BUDGET_get_used:
.LFB28:
	.loc 1 2514 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI95:
	add	fp, sp, #8
.LCFI96:
	sub	sp, sp, #20
.LCFI97:
	str	r0, [fp, #-28]
	.loc 1 2519 0
	ldr	r3, [fp, #-28]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L304+8
	add	r3, r2, r3
	str	r3, [fp, #-20]
	.loc 1 2523 0
	adr	r4, .L304
	ldmia	r4, {r3-r4}
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	.loc 1 2525 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #24]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-24]
	.loc 1 2529 0
	ldr	r0, [fp, #-24]
	mov	r1, #1
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r3, r0
	cmp	r3, #0
	beq	.L296
	.loc 1 2531 0
	ldr	r3, [fp, #-20]
	fldd	d7, [r3, #400]
	fldd	d6, [fp, #-16]
	faddd	d7, d6, d7
	fstd	d7, [fp, #-16]
.L296:
	.loc 1 2533 0
	ldr	r0, [fp, #-24]
	mov	r1, #2
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r3, r0
	cmp	r3, #0
	beq	.L297
	.loc 1 2535 0
	ldr	r3, [fp, #-20]
	fldd	d7, [r3, #408]
	fldd	d6, [fp, #-16]
	faddd	d7, d6, d7
	fstd	d7, [fp, #-16]
.L297:
	.loc 1 2537 0
	ldr	r0, [fp, #-24]
	mov	r1, #4
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r3, r0
	cmp	r3, #0
	beq	.L298
	.loc 1 2539 0
	ldr	r3, [fp, #-20]
	fldd	d7, [r3, #416]
	fldd	d6, [fp, #-16]
	faddd	d7, d6, d7
	fstd	d7, [fp, #-16]
.L298:
	.loc 1 2541 0
	ldr	r0, [fp, #-24]
	mov	r1, #5
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r3, r0
	cmp	r3, #0
	beq	.L299
	.loc 1 2543 0
	ldr	r3, [fp, #-20]
	fldd	d7, [r3, #424]
	fldd	d6, [fp, #-16]
	faddd	d7, d6, d7
	fstd	d7, [fp, #-16]
.L299:
	.loc 1 2545 0
	ldr	r0, [fp, #-24]
	mov	r1, #6
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r3, r0
	cmp	r3, #0
	beq	.L300
	.loc 1 2547 0
	ldr	r3, [fp, #-20]
	fldd	d7, [r3, #432]
	fldd	d6, [fp, #-16]
	faddd	d7, d6, d7
	fstd	d7, [fp, #-16]
.L300:
	.loc 1 2549 0
	ldr	r0, [fp, #-24]
	mov	r1, #7
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r3, r0
	cmp	r3, #0
	beq	.L301
	.loc 1 2551 0
	ldr	r3, [fp, #-20]
	fldd	d7, [r3, #440]
	fldd	d6, [fp, #-16]
	faddd	d7, d6, d7
	fstd	d7, [fp, #-16]
.L301:
	.loc 1 2553 0
	ldr	r0, [fp, #-24]
	mov	r1, #8
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r3, r0
	cmp	r3, #0
	beq	.L302
	.loc 1 2555 0
	ldr	r3, [fp, #-20]
	fldd	d7, [r3, #384]
	fldd	d6, [fp, #-16]
	faddd	d7, d6, d7
	fstd	d7, [fp, #-16]
.L302:
	.loc 1 2557 0
	ldr	r0, [fp, #-24]
	mov	r1, #9
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r3, r0
	cmp	r3, #0
	beq	.L303
	.loc 1 2559 0
	ldr	r3, [fp, #-20]
	fldd	d7, [r3, #392]
	fldd	d6, [fp, #-16]
	faddd	d7, d6, d7
	fstd	d7, [fp, #-16]
.L303:
	.loc 1 2562 0
	sub	r4, fp, #16
	ldmia	r4, {r3-r4}
	.loc 1 2563 0
	mov	r0, r3
	mov	r1, r4
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L305:
	.align	2
.L304:
	.word	0
	.word	0
	.word	poc_preserves+20
.LFE28:
	.size	nm_BUDGET_get_used, .-nm_BUDGET_get_used
	.section	.text.BUDGET_reset_budget_values,"ax",%progbits
	.align	2
	.global	BUDGET_reset_budget_values
	.type	BUDGET_reset_budget_values, %function
BUDGET_reset_budget_values:
.LFB29:
	.loc 1 2572 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI98:
	add	fp, sp, #4
.LCFI99:
	sub	sp, sp, #16
.LCFI100:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 2573 0
	ldr	r3, .L307
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L307+4
	ldr	r3, .L307+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2574 0
	ldr	r3, .L307+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L307+4
	ldr	r3, .L307+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2575 0
	ldr	r3, .L307+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L307+4
	ldr	r3, .L307+24
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2577 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	bl	nm_reset_meter_read_dates
	.loc 1 2578 0
	ldr	r0, [fp, #-8]
	bl	nm_reset_budget_values
	.loc 1 2580 0
	ldr	r3, .L307+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2581 0
	ldr	r3, .L307+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2582 0
	ldr	r3, .L307
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2583 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L308:
	.align	2
.L307:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	2573
	.word	system_preserves_recursive_MUTEX
	.word	2574
	.word	poc_preserves_recursive_MUTEX
	.word	2575
.LFE29:
	.size	BUDGET_reset_budget_values, .-BUDGET_reset_budget_values
	.section	.text.BUDGET_calculate_square_footage,"ax",%progbits
	.align	2
	.global	BUDGET_calculate_square_footage
	.type	BUDGET_calculate_square_footage, %function
BUDGET_calculate_square_footage:
.LFB30:
	.loc 1 2604 0
	@ args = 0, pretend = 0, frame = 84
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI101:
	add	fp, sp, #12
.LCFI102:
	sub	sp, sp, #96
.LCFI103:
	str	r0, [fp, #-96]
	.loc 1 2617 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 2619 0
	ldr	r3, .L313
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L313+4
	ldr	r3, .L313+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2621 0
	ldr	r0, [fp, #-96]
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-24]
	.loc 1 2623 0
	ldr	r0, .L313+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-16]
	.loc 1 2627 0
	b	.L310
.L312:
	.loc 1 2633 0
	ldr	r0, [fp, #-16]
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_get_index_for_group_with_this_GID
	mov	r2, r0
	ldr	r3, [fp, #-96]
	cmp	r2, r3
	bne	.L311
	.loc 1 2633 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-16]
	bl	STATION_get_GID_station_group
	mov	r3, r0
	cmp	r3, #0
	beq	.L311
	.loc 1 2635 0 is_stmt 1
	ldr	r0, [fp, #-16]
	bl	STATION_get_square_footage
	mov	r3, r0
	cmp	r3, #1000
	bne	.L311
	.loc 1 2637 0
	sub	r3, fp, #92
	ldr	r0, [fp, #-16]
	mov	r1, r3
	bl	nm_STATION_get_Budget_data
	.loc 1 2642 0
	ldr	r3, .L313+16
	flds	s14, [r3, #0]
	ldr	r3, .L313+20
	flds	s15, [r3, #0]
	fdivs	s14, s14, s15
	ldr	r3, [fp, #-56]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fmuls	s14, s14, s15
	ldr	r3, .L313+24
	flds	s15, [r3, #0]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-52]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-28]
	.loc 1 2644 0
	ldr	r0, [fp, #-28]	@ float
	mov	r1, #0
	bl	__round_float
	fmsr	s15, r0
	ftouizs	s15, s15
	fmrs	r5, s15	@ int
	ldr	r4, [fp, #-92]
	ldr	r0, [fp, #-16]
	mov	r1, #2
	bl	STATION_get_change_bits_ptr
	mov	r3, r0
	str	r4, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	mov	r1, r5
	mov	r2, #1
	mov	r3, #2
	bl	nm_STATION_set_square_footage
	.loc 1 2647 0
	mov	r3, #1
	str	r3, [fp, #-20]
.L311:
	.loc 1 2651 0
	ldr	r0, .L313+12
	ldr	r1, [fp, #-16]
	bl	nm_ListGetNext
	str	r0, [fp, #-16]
.L310:
	.loc 1 2627 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L312
	.loc 1 2654 0
	ldr	r3, .L313
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2656 0
	ldr	r3, [fp, #-20]
	.loc 1 2657 0
	mov	r0, r3
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L314:
	.align	2
.L313:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	2619
	.word	station_info_list_hdr
	.word	SQUARE_FOOTAGE_CONSTANT
	.word	MINUTES_PER_HOUR
	.word	percip_100000u
.LFE30:
	.size	BUDGET_calculate_square_footage, .-BUDGET_calculate_square_footage
	.section	.text.BUDGET_total_square_footage,"ax",%progbits
	.align	2
	.type	BUDGET_total_square_footage, %function
BUDGET_total_square_footage:
.LFB31:
	.loc 1 2679 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI104:
	add	fp, sp, #4
.LCFI105:
	sub	sp, sp, #16
.LCFI106:
	str	r0, [fp, #-20]
	.loc 1 2688 0
	ldr	r3, .L319	@ float
	str	r3, [fp, #-12]	@ float
	.loc 1 2692 0
	ldr	r3, .L319+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L319+8
	ldr	r3, .L319+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2694 0
	ldr	r0, [fp, #-20]
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-16]
	.loc 1 2696 0
	ldr	r0, .L319+16
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 2700 0
	b	.L316
.L318:
	.loc 1 2705 0
	ldr	r0, [fp, #-8]
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_get_index_for_group_with_this_GID
	mov	r2, r0
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L317
	.loc 1 2705 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-8]
	bl	STATION_get_GID_station_group
	mov	r3, r0
	cmp	r3, #0
	beq	.L317
	.loc 1 2707 0 is_stmt 1
	ldr	r0, [fp, #-8]
	bl	STATION_get_square_footage
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	flds	s14, [fp, #-12]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-12]
.L317:
	.loc 1 2710 0
	ldr	r0, .L319+16
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L316:
	.loc 1 2700 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L318
	.loc 1 2713 0
	ldr	r3, .L319+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2715 0
	ldr	r3, [fp, #-12]	@ float
	.loc 1 2716 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L320:
	.align	2
.L319:
	.word	0
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	2692
	.word	station_info_list_hdr
.LFE31:
	.size	BUDGET_total_square_footage, .-BUDGET_total_square_footage
	.section	.text.BUDGET_calculate_gallons_per_inch_for_square_footage_of_mainline,"ax",%progbits
	.align	2
	.type	BUDGET_calculate_gallons_per_inch_for_square_footage_of_mainline, %function
BUDGET_calculate_gallons_per_inch_for_square_footage_of_mainline:
.LFB32:
	.loc 1 2740 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI107:
	add	fp, sp, #4
.LCFI108:
	sub	sp, sp, #20
.LCFI109:
	str	r0, [fp, #-24]
	.loc 1 2746 0
	ldr	r3, .L322	@ float
	str	r3, [fp, #-16]	@ float
	.loc 1 2748 0
	ldr	r3, .L322+4	@ float
	str	r3, [fp, #-20]	@ float
	.loc 1 2752 0
	ldr	r0, [fp, #-24]
	bl	BUDGET_total_square_footage
	fmsr	s15, r0
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-8]
	.loc 1 2754 0
	ldr	r3, [fp, #-8]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-16]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-20]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-12]
	.loc 1 2756 0
	ldr	r3, [fp, #-12]	@ float
	.loc 1 2757 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L323:
	.align	2
.L322:
	.word	1188308096
	.word	1193945088
.LFE32:
	.size	BUDGET_calculate_gallons_per_inch_for_square_footage_of_mainline, .-BUDGET_calculate_gallons_per_inch_for_square_footage_of_mainline
	.section	.text.BUDGET_calc_POC_monthly_budget_using_et,"ax",%progbits
	.align	2
	.global	BUDGET_calc_POC_monthly_budget_using_et
	.type	BUDGET_calc_POC_monthly_budget_using_et, %function
BUDGET_calc_POC_monthly_budget_using_et:
.LFB33:
	.loc 1 2782 0
	@ args = 0, pretend = 0, frame = 284
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI110:
	fstmfdd	sp!, {d8}
.LCFI111:
	add	fp, sp, #16
.LCFI112:
	sub	sp, sp, #284
.LCFI113:
	str	r0, [fp, #-296]
	str	r1, [fp, #-300]
	.loc 1 2813 0
	ldr	r3, .L347+16	@ float
	str	r3, [fp, #-292]	@ float
	.loc 1 2824 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 2827 0
	sub	r3, fp, #288
	mov	r0, r3
	mov	r1, #0
	mov	r2, #48
	bl	memset
	.loc 1 2829 0
	ldr	r3, .L347+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L347+24
	ldr	r3, .L347+28
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2831 0
	ldr	r0, [fp, #-296]
	bl	nm_GROUP_get_group_ID
	str	r0, [fp, #-40]
	.loc 1 2833 0
	ldr	r3, .L347+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2835 0
	ldr	r3, .L347+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L347+24
	ldr	r3, .L347+36
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2837 0
	sub	r3, fp, #288
	ldr	r0, [fp, #-40]
	mov	r1, r3
	bl	nm_BUDGET_get_poc_usage
	str	r0, [fp, #-44]
	.loc 1 2839 0
	ldr	r3, .L347+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2842 0
	sub	r3, fp, #220
	ldr	r0, [fp, #-296]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 2844 0
	mov	r3, #0
	str	r3, [fp, #-32]
	.loc 1 2849 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L325
.L327:
	.loc 1 2851 0
	ldr	r1, .L347+40
	ldr	r2, [fp, #-20]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-48]
	.loc 1 2854 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L326
	.loc 1 2855 0 discriminator 1
	ldr	r1, .L347+40
	ldr	r2, [fp, #-20]
	mov	r3, #44
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	.loc 1 2854 0 discriminator 1
	ldr	r3, [fp, #-40]
	cmp	r2, r3
	bne	.L326
	.loc 1 2856 0
	ldr	r0, [fp, #-20]
	bl	POC_use_for_budget
	mov	r3, r0
	.loc 1 2855 0
	cmp	r3, #0
	beq	.L326
	.loc 1 2858 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #1
	str	r3, [fp, #-32]
.L326:
	.loc 1 2849 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L325:
	.loc 1 2849 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #11
	bls	.L327
	.loc 1 2863 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L328
.L344:
	.loc 1 2865 0
	ldr	r1, .L347+40
	ldr	r2, [fp, #-20]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-48]
	.loc 1 2866 0
	ldr	r0, [fp, #-20]
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-52]
	.loc 1 2869 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L345
	.loc 1 2869 0 is_stmt 0 discriminator 1
	ldr	r1, .L347+40
	ldr	r2, [fp, #-20]
	mov	r3, #44
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-40]
	cmp	r2, r3
	bne	.L345
.L330:
	.loc 1 2875 0 is_stmt 1
	ldr	r0, [fp, #-20]
	bl	POC_use_for_budget
	mov	r3, r0
	cmp	r3, #0
	beq	.L346
.L332:
	.loc 1 2882 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L333
	.loc 1 2884 0
	ldr	r0, [fp, #-40]
	bl	SYSTEM_get_index_for_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	BUDGET_calculate_gallons_per_inch_for_square_footage_of_mainline
	str	r0, [fp, #-56]	@ float
	.loc 1 2886 0
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L334
.L335:
	.loc 1 2888 0 discriminator 2
	ldr	r3, [fp, #-28]
	sub	r4, r3, #1
	ldr	r0, [fp, #-28]
	bl	ET_DATA_et_number_for_the_month
	fmsr	s14, r0
	flds	s15, [fp, #-56]
	fmuls	s16, s14, s15
	ldr	r0, [fp, #-52]
	bl	POC_get_budget_percent_ET
	mov	r3, r0
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fmuls	s14, s16, s15
	flds	s15, [fp, #-292]
	fdivs	s15, s14, s15
	mvn	r3, #91
	mov	r2, r4, asl #2
	sub	r1, fp, #16
	add	r2, r1, r2
	add	r3, r2, r3
	fsts	s15, [r3, #0]
	.loc 1 2886 0 discriminator 2
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
.L334:
	.loc 1 2886 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #12
	bls	.L335
	.loc 1 2891 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-24]
.L333:
	.loc 1 2895 0
	ldr	r3, [fp, #-32]
	cmp	r3, #1
	bne	.L336
	.loc 1 2897 0
	ldr	r3, .L347	@ float
	str	r3, [fp, #-36]	@ float
	b	.L337
.L336:
	.loc 1 2899 0
	ldr	r3, [fp, #-32]
	cmp	r3, #1
	bls	.L337
	.loc 1 2901 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	bne	.L338
	.loc 1 2905 0
	ldr	r3, [fp, #-32]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	flds	s14, .L347
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-36]
	b	.L337
.L338:
	.loc 1 2909 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L347+44
	mov	r2, r2, asl #2
	sub	r1, fp, #16
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L339
	.loc 1 2911 0
	ldr	r3, .L347+48	@ float
	str	r3, [fp, #-36]	@ float
	b	.L337
.L339:
	.loc 1 2915 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L347+44
	mov	r2, r2, asl #2
	sub	r1, fp, #16
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	ldr	r3, [fp, #-44]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-36]
.L337:
	.loc 1 2921 0
	mov	r3, #0
	str	r3, [fp, #-28]
	b	.L340
.L343:
	.loc 1 2923 0
	ldr	r3, [fp, #-28]
	add	r2, r3, #3
	mvn	r3, #203
	mov	r2, r2, asl #2
	sub	r1, fp, #16
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	sub	r3, fp, #240
	mov	r0, r2
	mov	r1, #0
	mov	r2, r3
	bl	DateAndTimeToDTCS
	.loc 1 2927 0
	ldrh	r3, [fp, #-232]
	sub	r2, r3, #1
	mvn	r3, #91
	mov	r2, r2, asl #2
	sub	r1, fp, #16
	add	r2, r1, r2
	add	r3, r2, r3
	flds	s15, [r3, #0]
	fcvtds	d6, s15
	fldd	d7, .L347+4
	fmuld	d6, d6, d7
	flds	s15, [fp, #-36]
	fcvtds	d7, s15
	fmuld	d7, d6, d7
	fcvtsd	s15, d7
	fsts	s15, [fp, #-60]
	.loc 1 2931 0
	ldr	r0, [fp, #-52]
	ldr	r1, [fp, #-28]
	bl	POC_get_budget
	mov	r3, r0
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	flds	s14, [fp, #-60]
	fsubs	s15, s14, s15
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
	mov	r0, r3
	bl	abs
	mov	r3, r0
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s15, [fp, #-292]
	fmuls	s16, s14, s15
	ldr	r0, [fp, #-52]
	ldr	r1, [fp, #-28]
	bl	POC_get_budget
	mov	r3, r0
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s14, s16, s15
	flds	s15, .L347+12
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L341
	.loc 1 2931 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-300]
	cmp	r3, #1
	bne	.L342
.L341:
	.loc 1 2933 0 is_stmt 1
	flds	s15, [fp, #-60]
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	ldr	r0, [fp, #-52]
	ldr	r1, [fp, #-28]
	mov	r2, r3
	bl	POC_set_budget
.L342:
	.loc 1 2921 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
.L340:
	.loc 1 2921 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #11
	bls	.L343
	b	.L331
.L345:
	.loc 1 2871 0 is_stmt 1
	mov	r0, r0	@ nop
	b	.L331
.L346:
	.loc 1 2877 0
	mov	r0, r0	@ nop
.L331:
	.loc 1 2863 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L328:
	.loc 1 2863 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #11
	bls	.L344
	.loc 1 2937 0 is_stmt 1
	sub	sp, fp, #16
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, fp, pc}
.L348:
	.align	2
.L347:
	.word	1065353216
	.word	-343597384
	.word	1072609361
	.word	1082130432
	.word	1120403456
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	2829
	.word	poc_preserves_recursive_MUTEX
	.word	2835
	.word	poc_preserves
	.word	-272
	.word	0
.LFE33:
	.size	BUDGET_calc_POC_monthly_budget_using_et, .-BUDGET_calc_POC_monthly_budget_using_et
	.section	.bss.dt_bug_check.8299,"aw",%nobits
	.align	2
	.type	dt_bug_check.8299, %object
	.size	dt_bug_check.8299, 6
dt_bug_check.8299:
	.space	6
	.section	.bss.dt_flag.8209,"aw",%nobits
	.align	2
	.type	dt_flag.8209, %object
	.size	dt_flag.8209, 6
dt_flag.8209:
	.space	6
	.section	.bss.saved_gid.8210,"aw",%nobits
	.align	2
	.type	saved_gid.8210, %object
	.size	saved_gid.8210, 4
saved_gid.8210:
	.space	4
	.section	.data.HUNDRED.8026,"aw",%progbits
	.align	2
	.type	HUNDRED.8026, %object
	.size	HUNDRED.8026, 4
HUNDRED.8026:
	.word	1120403456
	.section	.data.SIXTY.8025,"aw",%progbits
	.align	2
	.type	SIXTY.8025, %object
	.size	SIXTY.8025, 4
SIXTY.8025:
	.word	1114636288
	.section	.data.SIXTY.8002,"aw",%progbits
	.align	2
	.type	SIXTY.8002, %object
	.size	SIXTY.8002, 4
SIXTY.8002:
	.word	1114636288
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x10
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI19-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI22-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI25-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI26-.LCFI25
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI28-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x2
	.byte	0x8b
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x8
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI33-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI36-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI39-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI42-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI45-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x3
	.byte	0x8b
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI47-.LCFI46
	.byte	0xc
	.uleb128 0xb
	.uleb128 0xc
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI50-.LFB15
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI51-.LCFI50
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI53-.LFB16
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI54-.LCFI53
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI56-.LFB17
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI57-.LCFI56
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI59-.LFB18
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI60-.LCFI59
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI62-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI63-.LCFI62
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI65-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI66-.LCFI65
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI68-.LFB21
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI69-.LCFI68
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI70-.LCFI69
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI72-.LFB22
	.byte	0xe
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI73-.LCFI72
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x2
	.byte	0x8b
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI74-.LCFI73
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x8
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI76-.LFB23
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI77-.LCFI76
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI79-.LFB24
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI80-.LCFI79
	.byte	0xe
	.uleb128 0x10
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI81-.LCFI80
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI83-.LFB25
	.byte	0xe
	.uleb128 0xc
	.byte	0x4
	.4byte	.LCFI84-.LCFI83
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI85-.LCFI84
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x10
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI87-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI88-.LCFI87
	.byte	0xe
	.uleb128 0x10
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI89-.LCFI88
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI91-.LFB27
	.byte	0xe
	.uleb128 0x10
	.byte	0x4
	.4byte	.LCFI92-.LCFI91
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x5
	.byte	0x8b
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI93-.LCFI92
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x14
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI95-.LFB28
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI96-.LCFI95
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI98-.LFB29
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI99-.LCFI98
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI101-.LFB30
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI102-.LCFI101
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI104-.LFB31
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI105-.LCFI104
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI107-.LFB32
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI108-.LCFI107
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI110-.LFB33
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI111-.LCFI110
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI112-.LCFI111
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE66:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_tpmicro_common.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_defs.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/poc.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/stations.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/manual_programs.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/station_groups.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/flow_recorder.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/station_history_data.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/station_report_data.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 19 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 20 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/budget_report_data.h"
	.file 21 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 22 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 23 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/math.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x3972
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF631
	.byte	0x1
	.4byte	.LASF632
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x55
	.4byte	0x69
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x5e
	.4byte	0x7b
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x67
	.4byte	0x8d
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x70
	.4byte	0x9f
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x2
	.byte	0x99
	.4byte	0x7b
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x2
	.byte	0x9d
	.4byte	0x7b
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF17
	.uleb128 0x5
	.byte	0x6
	.byte	0x3
	.byte	0x22
	.4byte	0xeb
	.uleb128 0x6
	.ascii	"T\000"
	.byte	0x3
	.byte	0x24
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.ascii	"D\000"
	.byte	0x3
	.byte	0x26
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x3
	.byte	0x28
	.4byte	0xca
	.uleb128 0x5
	.byte	0x14
	.byte	0x3
	.byte	0x31
	.4byte	0x17d
	.uleb128 0x7
	.4byte	.LASF19
	.byte	0x3
	.byte	0x33
	.4byte	0xeb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF20
	.byte	0x3
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x7
	.4byte	.LASF21
	.byte	0x3
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF22
	.byte	0x3
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x7
	.4byte	.LASF23
	.byte	0x3
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF24
	.byte	0x3
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x7
	.4byte	.LASF25
	.byte	0x3
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x7
	.4byte	.LASF26
	.byte	0x3
	.byte	0x39
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0x7
	.4byte	.LASF27
	.byte	0x3
	.byte	0x3b
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x3
	.byte	0x3d
	.4byte	0xf6
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x4
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF30
	.byte	0x5
	.byte	0x57
	.4byte	0x188
	.uleb128 0x3
	.4byte	.LASF31
	.byte	0x6
	.byte	0x4c
	.4byte	0x195
	.uleb128 0x3
	.4byte	.LASF32
	.byte	0x7
	.byte	0x65
	.4byte	0x188
	.uleb128 0x9
	.4byte	0x3e
	.4byte	0x1c6
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x1d6
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x1e6
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1f
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x1f6
	.uleb128 0xa
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.byte	0x8
	.2byte	0x1c3
	.4byte	0x20f
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x8
	.2byte	0x1ca
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xd
	.4byte	.LASF34
	.byte	0x8
	.2byte	0x1d0
	.4byte	0x1f6
	.uleb128 0x5
	.byte	0x8
	.byte	0x9
	.byte	0xba
	.4byte	0x446
	.uleb128 0xe
	.4byte	.LASF35
	.byte	0x9
	.byte	0xbc
	.4byte	0x70
	.byte	0x4
	.byte	0x4
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF36
	.byte	0x9
	.byte	0xc6
	.4byte	0x70
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF37
	.byte	0x9
	.byte	0xc9
	.4byte	0x70
	.byte	0x4
	.byte	0x4
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF38
	.byte	0x9
	.byte	0xcd
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF39
	.byte	0x9
	.byte	0xcf
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF40
	.byte	0x9
	.byte	0xd1
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF41
	.byte	0x9
	.byte	0xd7
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF42
	.byte	0x9
	.byte	0xdd
	.4byte	0x70
	.byte	0x4
	.byte	0x2
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF43
	.byte	0x9
	.byte	0xdf
	.4byte	0x70
	.byte	0x4
	.byte	0x2
	.byte	0xe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0x9
	.byte	0xf5
	.4byte	0x70
	.byte	0x4
	.byte	0x2
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF45
	.byte	0x9
	.byte	0xfb
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0x9
	.byte	0xff
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF47
	.byte	0x9
	.2byte	0x102
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF48
	.byte	0x9
	.2byte	0x106
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF49
	.byte	0x9
	.2byte	0x10c
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF50
	.byte	0x9
	.2byte	0x111
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF51
	.byte	0x9
	.2byte	0x117
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF52
	.byte	0x9
	.2byte	0x11e
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF53
	.byte	0x9
	.2byte	0x120
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF54
	.byte	0x9
	.2byte	0x128
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF55
	.byte	0x9
	.2byte	0x12a
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF56
	.byte	0x9
	.2byte	0x12c
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF57
	.byte	0x9
	.2byte	0x130
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF58
	.byte	0x9
	.2byte	0x136
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF59
	.byte	0x9
	.2byte	0x13d
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF60
	.byte	0x9
	.2byte	0x13f
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF61
	.byte	0x9
	.2byte	0x141
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF62
	.byte	0x9
	.2byte	0x143
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF63
	.byte	0x9
	.2byte	0x145
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF64
	.byte	0x9
	.2byte	0x147
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF65
	.byte	0x9
	.2byte	0x149
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x10
	.byte	0x8
	.byte	0x9
	.byte	0xb6
	.4byte	0x45f
	.uleb128 0x11
	.4byte	.LASF103
	.byte	0x9
	.byte	0xb8
	.4byte	0x94
	.uleb128 0x12
	.4byte	0x21b
	.byte	0
	.uleb128 0x5
	.byte	0x8
	.byte	0x9
	.byte	0xb4
	.4byte	0x470
	.uleb128 0x13
	.4byte	0x446
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xd
	.4byte	.LASF66
	.byte	0x9
	.2byte	0x156
	.4byte	0x45f
	.uleb128 0xb
	.byte	0x8
	.byte	0x9
	.2byte	0x163
	.4byte	0x732
	.uleb128 0xf
	.4byte	.LASF67
	.byte	0x9
	.2byte	0x16b
	.4byte	0x70
	.byte	0x4
	.byte	0x4
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF68
	.byte	0x9
	.2byte	0x171
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF69
	.byte	0x9
	.2byte	0x17c
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF70
	.byte	0x9
	.2byte	0x185
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF71
	.byte	0x9
	.2byte	0x19b
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF72
	.byte	0x9
	.2byte	0x19d
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF73
	.byte	0x9
	.2byte	0x19f
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF74
	.byte	0x9
	.2byte	0x1a1
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF75
	.byte	0x9
	.2byte	0x1a3
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF76
	.byte	0x9
	.2byte	0x1a5
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF77
	.byte	0x9
	.2byte	0x1a7
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF78
	.byte	0x9
	.2byte	0x1b1
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF79
	.byte	0x9
	.2byte	0x1b6
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF80
	.byte	0x9
	.2byte	0x1bb
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF81
	.byte	0x9
	.2byte	0x1c7
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF82
	.byte	0x9
	.2byte	0x1cd
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF83
	.byte	0x9
	.2byte	0x1d6
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF84
	.byte	0x9
	.2byte	0x1d8
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF85
	.byte	0x9
	.2byte	0x1e6
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF86
	.byte	0x9
	.2byte	0x1e7
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF35
	.byte	0x9
	.2byte	0x1e8
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF87
	.byte	0x9
	.2byte	0x1e9
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF88
	.byte	0x9
	.2byte	0x1ea
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF89
	.byte	0x9
	.2byte	0x1eb
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF90
	.byte	0x9
	.2byte	0x1ec
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF91
	.byte	0x9
	.2byte	0x1f6
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF92
	.byte	0x9
	.2byte	0x1f7
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF48
	.byte	0x9
	.2byte	0x1f8
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF93
	.byte	0x9
	.2byte	0x1f9
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF94
	.byte	0x9
	.2byte	0x1fa
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF95
	.byte	0x9
	.2byte	0x1fb
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF96
	.byte	0x9
	.2byte	0x1fc
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF97
	.byte	0x9
	.2byte	0x206
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF98
	.byte	0x9
	.2byte	0x20d
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF99
	.byte	0x9
	.2byte	0x214
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF100
	.byte	0x9
	.2byte	0x216
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF101
	.byte	0x9
	.2byte	0x223
	.4byte	0x70
	.byte	0x4
	.byte	0x6
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF102
	.byte	0x9
	.2byte	0x227
	.4byte	0x70
	.byte	0x4
	.byte	0x4
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x14
	.byte	0x8
	.byte	0x9
	.2byte	0x15f
	.4byte	0x74d
	.uleb128 0x15
	.4byte	.LASF104
	.byte	0x9
	.2byte	0x161
	.4byte	0x94
	.uleb128 0x12
	.4byte	0x47c
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x9
	.2byte	0x15d
	.4byte	0x75f
	.uleb128 0x13
	.4byte	0x732
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xd
	.4byte	.LASF105
	.byte	0x9
	.2byte	0x230
	.4byte	0x74d
	.uleb128 0x5
	.byte	0x14
	.byte	0xa
	.byte	0x18
	.4byte	0x7ba
	.uleb128 0x7
	.4byte	.LASF106
	.byte	0xa
	.byte	0x1a
	.4byte	0x188
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF107
	.byte	0xa
	.byte	0x1c
	.4byte	0x188
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF108
	.byte	0xa
	.byte	0x1e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF109
	.byte	0xa
	.byte	0x20
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF110
	.byte	0xa
	.byte	0x23
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF111
	.byte	0xa
	.byte	0x26
	.4byte	0x76b
	.uleb128 0x5
	.byte	0xc
	.byte	0xa
	.byte	0x2a
	.4byte	0x7f8
	.uleb128 0x7
	.4byte	.LASF112
	.byte	0xa
	.byte	0x2c
	.4byte	0x188
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF113
	.byte	0xa
	.byte	0x2e
	.4byte	0x188
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF114
	.byte	0xa
	.byte	0x30
	.4byte	0x7f8
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0x7ba
	.uleb128 0x3
	.4byte	.LASF115
	.byte	0xa
	.byte	0x32
	.4byte	0x7c5
	.uleb128 0x16
	.byte	0x4
	.4byte	0x33
	.uleb128 0x3
	.4byte	.LASF116
	.byte	0xb
	.byte	0xda
	.4byte	0x81a
	.uleb128 0x17
	.4byte	.LASF116
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF117
	.byte	0xc
	.byte	0x69
	.4byte	0x82b
	.uleb128 0x17
	.4byte	.LASF117
	.byte	0x1
	.uleb128 0x5
	.byte	0x40
	.byte	0xc
	.byte	0x6e
	.4byte	0x91a
	.uleb128 0x7
	.4byte	.LASF118
	.byte	0xc
	.byte	0x70
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF119
	.byte	0xc
	.byte	0x71
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF120
	.byte	0xc
	.byte	0x72
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF121
	.byte	0xc
	.byte	0x73
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF122
	.byte	0xc
	.byte	0x74
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x7
	.4byte	.LASF123
	.byte	0xc
	.byte	0x75
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x7
	.4byte	.LASF124
	.byte	0xc
	.byte	0x76
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x7
	.4byte	.LASF125
	.byte	0xc
	.byte	0x77
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x7
	.4byte	.LASF126
	.byte	0xc
	.byte	0x78
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x7
	.4byte	.LASF127
	.byte	0xc
	.byte	0x79
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x7
	.4byte	.LASF128
	.byte	0xc
	.byte	0x7a
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x7
	.4byte	.LASF129
	.byte	0xc
	.byte	0x7b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x7
	.4byte	.LASF130
	.byte	0xc
	.byte	0x7c
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x7
	.4byte	.LASF131
	.byte	0xc
	.byte	0x7d
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x7
	.4byte	.LASF132
	.byte	0xc
	.byte	0x7e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x7
	.4byte	.LASF133
	.byte	0xc
	.byte	0x7f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF134
	.uleb128 0x3
	.4byte	.LASF135
	.byte	0xc
	.byte	0x82
	.4byte	0x831
	.uleb128 0x3
	.4byte	.LASF136
	.byte	0xd
	.byte	0x31
	.4byte	0x937
	.uleb128 0x17
	.4byte	.LASF136
	.byte	0x1
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x94d
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x95d
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0xd
	.4byte	.LASF137
	.byte	0xe
	.2byte	0x1a2
	.4byte	0x969
	.uleb128 0x17
	.4byte	.LASF137
	.byte	0x1
	.uleb128 0x5
	.byte	0x1c
	.byte	0xf
	.byte	0x8f
	.4byte	0x9da
	.uleb128 0x7
	.4byte	.LASF138
	.byte	0xf
	.byte	0x94
	.4byte	0x809
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF139
	.byte	0xf
	.byte	0x99
	.4byte	0x809
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF140
	.byte	0xf
	.byte	0x9e
	.4byte	0x809
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF141
	.byte	0xf
	.byte	0xa3
	.4byte	0x809
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF142
	.byte	0xf
	.byte	0xad
	.4byte	0x809
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x7
	.4byte	.LASF143
	.byte	0xf
	.byte	0xb8
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x7
	.4byte	.LASF144
	.byte	0xf
	.byte	0xbe
	.4byte	0x1ab
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x3
	.4byte	.LASF145
	.byte	0xf
	.byte	0xc2
	.4byte	0x96f
	.uleb128 0x5
	.byte	0x4
	.byte	0x10
	.byte	0x24
	.4byte	0xc0e
	.uleb128 0xe
	.4byte	.LASF146
	.byte	0x10
	.byte	0x31
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF147
	.byte	0x10
	.byte	0x35
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF148
	.byte	0x10
	.byte	0x37
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF149
	.byte	0x10
	.byte	0x39
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF150
	.byte	0x10
	.byte	0x3b
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF151
	.byte	0x10
	.byte	0x3c
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF152
	.byte	0x10
	.byte	0x3d
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF153
	.byte	0x10
	.byte	0x3e
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF154
	.byte	0x10
	.byte	0x40
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF155
	.byte	0x10
	.byte	0x44
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF156
	.byte	0x10
	.byte	0x46
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF157
	.byte	0x10
	.byte	0x47
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF158
	.byte	0x10
	.byte	0x4d
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF159
	.byte	0x10
	.byte	0x4f
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF160
	.byte	0x10
	.byte	0x50
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF161
	.byte	0x10
	.byte	0x52
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF162
	.byte	0x10
	.byte	0x53
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF163
	.byte	0x10
	.byte	0x55
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF164
	.byte	0x10
	.byte	0x56
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF165
	.byte	0x10
	.byte	0x5b
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF166
	.byte	0x10
	.byte	0x5d
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF167
	.byte	0x10
	.byte	0x5e
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF168
	.byte	0x10
	.byte	0x5f
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF169
	.byte	0x10
	.byte	0x61
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF170
	.byte	0x10
	.byte	0x62
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF171
	.byte	0x10
	.byte	0x68
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF172
	.byte	0x10
	.byte	0x6a
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF173
	.byte	0x10
	.byte	0x70
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF174
	.byte	0x10
	.byte	0x78
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF175
	.byte	0x10
	.byte	0x7c
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF176
	.byte	0x10
	.byte	0x7e
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF177
	.byte	0x10
	.byte	0x82
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0x10
	.byte	0x20
	.4byte	0xc27
	.uleb128 0x11
	.4byte	.LASF104
	.byte	0x10
	.byte	0x22
	.4byte	0x70
	.uleb128 0x12
	.4byte	0x9e5
	.byte	0
	.uleb128 0x3
	.4byte	.LASF178
	.byte	0x10
	.byte	0x8d
	.4byte	0xc0e
	.uleb128 0x5
	.byte	0x3c
	.byte	0x10
	.byte	0xa5
	.4byte	0xda4
	.uleb128 0x7
	.4byte	.LASF179
	.byte	0x10
	.byte	0xb0
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF180
	.byte	0x10
	.byte	0xb5
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF181
	.byte	0x10
	.byte	0xb8
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF182
	.byte	0x10
	.byte	0xbd
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF183
	.byte	0x10
	.byte	0xc3
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x7
	.4byte	.LASF184
	.byte	0x10
	.byte	0xd0
	.4byte	0xc27
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x7
	.4byte	.LASF131
	.byte	0x10
	.byte	0xdb
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x7
	.4byte	.LASF185
	.byte	0x10
	.byte	0xdd
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1a
	.uleb128 0x7
	.4byte	.LASF186
	.byte	0x10
	.byte	0xe4
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x7
	.4byte	.LASF187
	.byte	0x10
	.byte	0xe8
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1e
	.uleb128 0x7
	.4byte	.LASF188
	.byte	0x10
	.byte	0xea
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x7
	.4byte	.LASF189
	.byte	0x10
	.byte	0xf0
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x22
	.uleb128 0x7
	.4byte	.LASF190
	.byte	0x10
	.byte	0xf9
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x7
	.4byte	.LASF191
	.byte	0x10
	.byte	0xff
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.4byte	.LASF192
	.byte	0x10
	.2byte	0x101
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2a
	.uleb128 0xc
	.4byte	.LASF193
	.byte	0x10
	.2byte	0x109
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xc
	.4byte	.LASF194
	.byte	0x10
	.2byte	0x10f
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2e
	.uleb128 0xc
	.4byte	.LASF195
	.byte	0x10
	.2byte	0x111
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xc
	.4byte	.LASF196
	.byte	0x10
	.2byte	0x113
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x32
	.uleb128 0xc
	.4byte	.LASF197
	.byte	0x10
	.2byte	0x118
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xc
	.4byte	.LASF198
	.byte	0x10
	.2byte	0x11a
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x35
	.uleb128 0xc
	.4byte	.LASF118
	.byte	0x10
	.2byte	0x11d
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x36
	.uleb128 0xc
	.4byte	.LASF199
	.byte	0x10
	.2byte	0x121
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x37
	.uleb128 0xc
	.4byte	.LASF200
	.byte	0x10
	.2byte	0x12c
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0xc
	.4byte	.LASF201
	.byte	0x10
	.2byte	0x12e
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x3a
	.byte	0
	.uleb128 0xd
	.4byte	.LASF202
	.byte	0x10
	.2byte	0x13a
	.4byte	0xc32
	.uleb128 0x5
	.byte	0x30
	.byte	0x11
	.byte	0x22
	.4byte	0xea7
	.uleb128 0x7
	.4byte	.LASF179
	.byte	0x11
	.byte	0x24
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF203
	.byte	0x11
	.byte	0x2a
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF204
	.byte	0x11
	.byte	0x2c
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF205
	.byte	0x11
	.byte	0x2e
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF206
	.byte	0x11
	.byte	0x30
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x7
	.4byte	.LASF207
	.byte	0x11
	.byte	0x32
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x7
	.4byte	.LASF208
	.byte	0x11
	.byte	0x34
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x7
	.4byte	.LASF209
	.byte	0x11
	.byte	0x39
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x7
	.4byte	.LASF130
	.byte	0x11
	.byte	0x44
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x7
	.4byte	.LASF186
	.byte	0x11
	.byte	0x48
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x22
	.uleb128 0x7
	.4byte	.LASF210
	.byte	0x11
	.byte	0x4c
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x7
	.4byte	.LASF211
	.byte	0x11
	.byte	0x4e
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x26
	.uleb128 0x7
	.4byte	.LASF212
	.byte	0x11
	.byte	0x50
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x7
	.4byte	.LASF213
	.byte	0x11
	.byte	0x52
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2a
	.uleb128 0x7
	.4byte	.LASF214
	.byte	0x11
	.byte	0x54
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x7
	.4byte	.LASF197
	.byte	0x11
	.byte	0x59
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x2e
	.uleb128 0x7
	.4byte	.LASF118
	.byte	0x11
	.byte	0x5c
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x2f
	.byte	0
	.uleb128 0x3
	.4byte	.LASF215
	.byte	0x11
	.byte	0x66
	.4byte	0xdb0
	.uleb128 0x3
	.4byte	.LASF216
	.byte	0x12
	.byte	0xd0
	.4byte	0xebd
	.uleb128 0x17
	.4byte	.LASF216
	.byte	0x1
	.uleb128 0xb
	.byte	0x70
	.byte	0x12
	.2byte	0x19b
	.4byte	0xf18
	.uleb128 0xc
	.4byte	.LASF217
	.byte	0x12
	.2byte	0x19d
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF218
	.byte	0x12
	.2byte	0x19e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF219
	.byte	0x12
	.2byte	0x19f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF220
	.byte	0x12
	.2byte	0x1a0
	.4byte	0xf18
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF221
	.byte	0x12
	.2byte	0x1a1
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.byte	0
	.uleb128 0x9
	.4byte	0x70
	.4byte	0xf28
	.uleb128 0xa
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0xd
	.4byte	.LASF222
	.byte	0x12
	.2byte	0x1a2
	.4byte	0xec3
	.uleb128 0xb
	.byte	0x2
	.byte	0x13
	.2byte	0x249
	.4byte	0xfe0
	.uleb128 0xf
	.4byte	.LASF223
	.byte	0x13
	.2byte	0x25d
	.4byte	0x70
	.byte	0x4
	.byte	0x4
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF224
	.byte	0x13
	.2byte	0x264
	.4byte	0x70
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF225
	.byte	0x13
	.2byte	0x26d
	.4byte	0x70
	.byte	0x4
	.byte	0x4
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF226
	.byte	0x13
	.2byte	0x271
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF227
	.byte	0x13
	.2byte	0x273
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF228
	.byte	0x13
	.2byte	0x277
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF229
	.byte	0x13
	.2byte	0x281
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF230
	.byte	0x13
	.2byte	0x289
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF231
	.byte	0x13
	.2byte	0x290
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x14
	.byte	0x2
	.byte	0x13
	.2byte	0x243
	.4byte	0xffb
	.uleb128 0x15
	.4byte	.LASF104
	.byte	0x13
	.2byte	0x247
	.4byte	0x4c
	.uleb128 0x12
	.4byte	0xf34
	.byte	0
	.uleb128 0xd
	.4byte	.LASF232
	.byte	0x13
	.2byte	0x296
	.4byte	0xfe0
	.uleb128 0xb
	.byte	0x80
	.byte	0x13
	.2byte	0x2aa
	.4byte	0x10a7
	.uleb128 0xc
	.4byte	.LASF233
	.byte	0x13
	.2byte	0x2b5
	.4byte	0xda4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF234
	.byte	0x13
	.2byte	0x2b9
	.4byte	0xea7
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xc
	.4byte	.LASF235
	.byte	0x13
	.2byte	0x2bf
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0xc
	.4byte	.LASF236
	.byte	0x13
	.2byte	0x2c3
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0xc
	.4byte	.LASF237
	.byte	0x13
	.2byte	0x2c9
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0xc
	.4byte	.LASF238
	.byte	0x13
	.2byte	0x2cd
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x76
	.uleb128 0xc
	.4byte	.LASF239
	.byte	0x13
	.2byte	0x2d4
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0xc
	.4byte	.LASF240
	.byte	0x13
	.2byte	0x2d8
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x7a
	.uleb128 0xc
	.4byte	.LASF241
	.byte	0x13
	.2byte	0x2dd
	.4byte	0xffb
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0xc
	.4byte	.LASF242
	.byte	0x13
	.2byte	0x2e5
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x7e
	.byte	0
	.uleb128 0xd
	.4byte	.LASF243
	.byte	0x13
	.2byte	0x2ff
	.4byte	0x1007
	.uleb128 0x18
	.4byte	0x42010
	.byte	0x13
	.2byte	0x309
	.4byte	0x10de
	.uleb128 0xc
	.4byte	.LASF244
	.byte	0x13
	.2byte	0x310
	.4byte	0x1e6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x19
	.ascii	"sps\000"
	.byte	0x13
	.2byte	0x314
	.4byte	0x10de
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x9
	.4byte	0x10a7
	.4byte	0x10ef
	.uleb128 0x1a
	.4byte	0x25
	.2byte	0x83f
	.byte	0
	.uleb128 0xd
	.4byte	.LASF245
	.byte	0x13
	.2byte	0x31b
	.4byte	0x10b3
	.uleb128 0xb
	.byte	0x10
	.byte	0x13
	.2byte	0x366
	.4byte	0x119b
	.uleb128 0xc
	.4byte	.LASF246
	.byte	0x13
	.2byte	0x379
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF247
	.byte	0x13
	.2byte	0x37b
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0xc
	.4byte	.LASF248
	.byte	0x13
	.2byte	0x37d
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xc
	.4byte	.LASF249
	.byte	0x13
	.2byte	0x381
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.uleb128 0xc
	.4byte	.LASF250
	.byte	0x13
	.2byte	0x387
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF251
	.byte	0x13
	.2byte	0x388
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xc
	.4byte	.LASF252
	.byte	0x13
	.2byte	0x38a
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF253
	.byte	0x13
	.2byte	0x38b
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xc
	.4byte	.LASF254
	.byte	0x13
	.2byte	0x38d
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF255
	.byte	0x13
	.2byte	0x38e
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.byte	0
	.uleb128 0xd
	.4byte	.LASF256
	.byte	0x13
	.2byte	0x390
	.4byte	0x10fb
	.uleb128 0xb
	.byte	0x4c
	.byte	0x13
	.2byte	0x39b
	.4byte	0x12bf
	.uleb128 0xc
	.4byte	.LASF257
	.byte	0x13
	.2byte	0x39f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF258
	.byte	0x13
	.2byte	0x3a8
	.4byte	0xeb
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF259
	.byte	0x13
	.2byte	0x3aa
	.4byte	0xeb
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xc
	.4byte	.LASF260
	.byte	0x13
	.2byte	0x3b1
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF261
	.byte	0x13
	.2byte	0x3b7
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF262
	.byte	0x13
	.2byte	0x3b8
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF263
	.byte	0x13
	.2byte	0x3ba
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF207
	.byte	0x13
	.2byte	0x3bb
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xc
	.4byte	.LASF264
	.byte	0x13
	.2byte	0x3bd
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xc
	.4byte	.LASF206
	.byte	0x13
	.2byte	0x3be
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.4byte	.LASF265
	.byte	0x13
	.2byte	0x3c0
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xc
	.4byte	.LASF205
	.byte	0x13
	.2byte	0x3c1
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xc
	.4byte	.LASF266
	.byte	0x13
	.2byte	0x3c3
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xc
	.4byte	.LASF204
	.byte	0x13
	.2byte	0x3c4
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0xc
	.4byte	.LASF267
	.byte	0x13
	.2byte	0x3c6
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xc
	.4byte	.LASF268
	.byte	0x13
	.2byte	0x3c7
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xc
	.4byte	.LASF269
	.byte	0x13
	.2byte	0x3c9
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xc
	.4byte	.LASF270
	.byte	0x13
	.2byte	0x3ca
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.byte	0
	.uleb128 0xd
	.4byte	.LASF271
	.byte	0x13
	.2byte	0x3d1
	.4byte	0x11a7
	.uleb128 0xb
	.byte	0x28
	.byte	0x13
	.2byte	0x3d4
	.4byte	0x136b
	.uleb128 0xc
	.4byte	.LASF257
	.byte	0x13
	.2byte	0x3d6
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF217
	.byte	0x13
	.2byte	0x3d8
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF221
	.byte	0x13
	.2byte	0x3d9
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF272
	.byte	0x13
	.2byte	0x3db
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF273
	.byte	0x13
	.2byte	0x3dc
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF218
	.byte	0x13
	.2byte	0x3dd
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF274
	.byte	0x13
	.2byte	0x3e0
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF275
	.byte	0x13
	.2byte	0x3e3
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF276
	.byte	0x13
	.2byte	0x3f5
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xc
	.4byte	.LASF277
	.byte	0x13
	.2byte	0x3fa
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0xd
	.4byte	.LASF278
	.byte	0x13
	.2byte	0x401
	.4byte	0x12cb
	.uleb128 0xb
	.byte	0x30
	.byte	0x13
	.2byte	0x404
	.4byte	0x13ae
	.uleb128 0x19
	.ascii	"rip\000"
	.byte	0x13
	.2byte	0x406
	.4byte	0x136b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF279
	.byte	0x13
	.2byte	0x409
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.4byte	.LASF280
	.byte	0x13
	.2byte	0x40c
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.byte	0
	.uleb128 0xd
	.4byte	.LASF281
	.byte	0x13
	.2byte	0x40e
	.4byte	0x1377
	.uleb128 0x1b
	.2byte	0x3790
	.byte	0x13
	.2byte	0x418
	.4byte	0x1837
	.uleb128 0xc
	.4byte	.LASF257
	.byte	0x13
	.2byte	0x420
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x19
	.ascii	"rip\000"
	.byte	0x13
	.2byte	0x425
	.4byte	0x12bf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF282
	.byte	0x13
	.2byte	0x42f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xc
	.4byte	.LASF283
	.byte	0x13
	.2byte	0x442
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xc
	.4byte	.LASF284
	.byte	0x13
	.2byte	0x44e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xc
	.4byte	.LASF285
	.byte	0x13
	.2byte	0x458
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0xc
	.4byte	.LASF286
	.byte	0x13
	.2byte	0x473
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0xc
	.4byte	.LASF287
	.byte	0x13
	.2byte	0x47d
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0xc
	.4byte	.LASF288
	.byte	0x13
	.2byte	0x499
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0xc
	.4byte	.LASF289
	.byte	0x13
	.2byte	0x49d
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0xc
	.4byte	.LASF290
	.byte	0x13
	.2byte	0x49f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0xc
	.4byte	.LASF291
	.byte	0x13
	.2byte	0x4a9
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0xc
	.4byte	.LASF292
	.byte	0x13
	.2byte	0x4ad
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0xc
	.4byte	.LASF293
	.byte	0x13
	.2byte	0x4af
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0xc
	.4byte	.LASF294
	.byte	0x13
	.2byte	0x4b3
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0xc
	.4byte	.LASF295
	.byte	0x13
	.2byte	0x4b5
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0xc
	.4byte	.LASF296
	.byte	0x13
	.2byte	0x4b7
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0xc
	.4byte	.LASF297
	.byte	0x13
	.2byte	0x4bc
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0xc
	.4byte	.LASF298
	.byte	0x13
	.2byte	0x4be
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0xc
	.4byte	.LASF299
	.byte	0x13
	.2byte	0x4c1
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0xc
	.4byte	.LASF300
	.byte	0x13
	.2byte	0x4c3
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0xc
	.4byte	.LASF301
	.byte	0x13
	.2byte	0x4cc
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0xc
	.4byte	.LASF302
	.byte	0x13
	.2byte	0x4cf
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0xc
	.4byte	.LASF303
	.byte	0x13
	.2byte	0x4d1
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0xc
	.4byte	.LASF304
	.byte	0x13
	.2byte	0x4d9
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0xc
	.4byte	.LASF305
	.byte	0x13
	.2byte	0x4e3
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0xc
	.4byte	.LASF306
	.byte	0x13
	.2byte	0x4e5
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0xc
	.4byte	.LASF307
	.byte	0x13
	.2byte	0x4e9
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0xc
	.4byte	.LASF308
	.byte	0x13
	.2byte	0x4eb
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0xc
	.4byte	.LASF309
	.byte	0x13
	.2byte	0x4ed
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xbc
	.uleb128 0xc
	.4byte	.LASF310
	.byte	0x13
	.2byte	0x4f4
	.4byte	0x94d
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0xc
	.4byte	.LASF311
	.byte	0x13
	.2byte	0x4fe
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0xc
	.4byte	.LASF312
	.byte	0x13
	.2byte	0x504
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0xc
	.4byte	.LASF313
	.byte	0x13
	.2byte	0x50c
	.4byte	0x1837
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0xc
	.4byte	.LASF314
	.byte	0x13
	.2byte	0x512
	.4byte	0x91a
	.byte	0x3
	.byte	0x23
	.uleb128 0x128
	.uleb128 0xc
	.4byte	.LASF315
	.byte	0x13
	.2byte	0x515
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x12c
	.uleb128 0xc
	.4byte	.LASF316
	.byte	0x13
	.2byte	0x519
	.4byte	0x91a
	.byte	0x3
	.byte	0x23
	.uleb128 0x130
	.uleb128 0xc
	.4byte	.LASF317
	.byte	0x13
	.2byte	0x51e
	.4byte	0x91a
	.byte	0x3
	.byte	0x23
	.uleb128 0x134
	.uleb128 0xc
	.4byte	.LASF318
	.byte	0x13
	.2byte	0x524
	.4byte	0x1847
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.uleb128 0xc
	.4byte	.LASF319
	.byte	0x13
	.2byte	0x52b
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1b0
	.uleb128 0xc
	.4byte	.LASF320
	.byte	0x13
	.2byte	0x536
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1b4
	.uleb128 0xc
	.4byte	.LASF321
	.byte	0x13
	.2byte	0x538
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1b8
	.uleb128 0xc
	.4byte	.LASF322
	.byte	0x13
	.2byte	0x53e
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1bc
	.uleb128 0xc
	.4byte	.LASF323
	.byte	0x13
	.2byte	0x54a
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c0
	.uleb128 0xc
	.4byte	.LASF324
	.byte	0x13
	.2byte	0x54c
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c4
	.uleb128 0xc
	.4byte	.LASF325
	.byte	0x13
	.2byte	0x555
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c8
	.uleb128 0xc
	.4byte	.LASF326
	.byte	0x13
	.2byte	0x55f
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1cc
	.uleb128 0x19
	.ascii	"sbf\000"
	.byte	0x13
	.2byte	0x566
	.4byte	0x75f
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d0
	.uleb128 0xc
	.4byte	.LASF327
	.byte	0x13
	.2byte	0x573
	.4byte	0x9da
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d8
	.uleb128 0xc
	.4byte	.LASF328
	.byte	0x13
	.2byte	0x578
	.4byte	0x119b
	.byte	0x3
	.byte	0x23
	.uleb128 0x1f4
	.uleb128 0xc
	.4byte	.LASF329
	.byte	0x13
	.2byte	0x57b
	.4byte	0x119b
	.byte	0x3
	.byte	0x23
	.uleb128 0x204
	.uleb128 0xc
	.4byte	.LASF330
	.byte	0x13
	.2byte	0x57f
	.4byte	0x1857
	.byte	0x3
	.byte	0x23
	.uleb128 0x214
	.uleb128 0xc
	.4byte	.LASF331
	.byte	0x13
	.2byte	0x581
	.4byte	0x1868
	.byte	0x3
	.byte	0x23
	.uleb128 0x253c
	.uleb128 0xc
	.4byte	.LASF332
	.byte	0x13
	.2byte	0x588
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36d0
	.uleb128 0xc
	.4byte	.LASF333
	.byte	0x13
	.2byte	0x58a
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36d4
	.uleb128 0xc
	.4byte	.LASF334
	.byte	0x13
	.2byte	0x58c
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x36d8
	.uleb128 0xc
	.4byte	.LASF335
	.byte	0x13
	.2byte	0x58e
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36dc
	.uleb128 0xc
	.4byte	.LASF336
	.byte	0x13
	.2byte	0x590
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36e0
	.uleb128 0xc
	.4byte	.LASF337
	.byte	0x13
	.2byte	0x592
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36e4
	.uleb128 0xc
	.4byte	.LASF338
	.byte	0x13
	.2byte	0x597
	.4byte	0x1c6
	.byte	0x3
	.byte	0x23
	.uleb128 0x36e8
	.uleb128 0xc
	.4byte	.LASF339
	.byte	0x13
	.2byte	0x599
	.4byte	0x94d
	.byte	0x3
	.byte	0x23
	.uleb128 0x36f4
	.uleb128 0xc
	.4byte	.LASF340
	.byte	0x13
	.2byte	0x59b
	.4byte	0x94d
	.byte	0x3
	.byte	0x23
	.uleb128 0x3704
	.uleb128 0xc
	.4byte	.LASF341
	.byte	0x13
	.2byte	0x5a0
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3714
	.uleb128 0xc
	.4byte	.LASF342
	.byte	0x13
	.2byte	0x5a2
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3718
	.uleb128 0xc
	.4byte	.LASF343
	.byte	0x13
	.2byte	0x5a4
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x371c
	.uleb128 0xc
	.4byte	.LASF344
	.byte	0x13
	.2byte	0x5aa
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3720
	.uleb128 0xc
	.4byte	.LASF345
	.byte	0x13
	.2byte	0x5b1
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3724
	.uleb128 0xc
	.4byte	.LASF346
	.byte	0x13
	.2byte	0x5b3
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3728
	.uleb128 0xc
	.4byte	.LASF347
	.byte	0x13
	.2byte	0x5b7
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x372c
	.uleb128 0xc
	.4byte	.LASF348
	.byte	0x13
	.2byte	0x5be
	.4byte	0x13ae
	.byte	0x3
	.byte	0x23
	.uleb128 0x3730
	.uleb128 0xc
	.4byte	.LASF349
	.byte	0x13
	.2byte	0x5c8
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3760
	.uleb128 0xc
	.4byte	.LASF350
	.byte	0x13
	.2byte	0x5cf
	.4byte	0x1879
	.byte	0x3
	.byte	0x23
	.uleb128 0x3764
	.byte	0
	.uleb128 0x9
	.4byte	0x91a
	.4byte	0x1847
	.uleb128 0xa
	.4byte	0x25
	.byte	0x13
	.byte	0
	.uleb128 0x9
	.4byte	0x91a
	.4byte	0x1857
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1d
	.byte	0
	.uleb128 0x9
	.4byte	0x5e
	.4byte	0x1868
	.uleb128 0x1a
	.4byte	0x25
	.2byte	0x1193
	.byte	0
	.uleb128 0x9
	.4byte	0x33
	.4byte	0x1879
	.uleb128 0x1a
	.4byte	0x25
	.2byte	0x1193
	.byte	0
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x1889
	.uleb128 0xa
	.4byte	0x25
	.byte	0xa
	.byte	0
	.uleb128 0xd
	.4byte	.LASF351
	.byte	0x13
	.2byte	0x5d6
	.4byte	0x13ba
	.uleb128 0xb
	.byte	0x3c
	.byte	0x13
	.2byte	0x60b
	.4byte	0x1953
	.uleb128 0xc
	.4byte	.LASF352
	.byte	0x13
	.2byte	0x60f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF353
	.byte	0x13
	.2byte	0x616
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF272
	.byte	0x13
	.2byte	0x618
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF354
	.byte	0x13
	.2byte	0x61d
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xc
	.4byte	.LASF355
	.byte	0x13
	.2byte	0x626
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF356
	.byte	0x13
	.2byte	0x62f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF357
	.byte	0x13
	.2byte	0x631
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF358
	.byte	0x13
	.2byte	0x63a
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xc
	.4byte	.LASF359
	.byte	0x13
	.2byte	0x63c
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xc
	.4byte	.LASF360
	.byte	0x13
	.2byte	0x645
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xc
	.4byte	.LASF361
	.byte	0x13
	.2byte	0x647
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xc
	.4byte	.LASF362
	.byte	0x13
	.2byte	0x650
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF363
	.uleb128 0xd
	.4byte	.LASF364
	.byte	0x13
	.2byte	0x652
	.4byte	0x1895
	.uleb128 0xb
	.byte	0x60
	.byte	0x13
	.2byte	0x655
	.4byte	0x1a42
	.uleb128 0xc
	.4byte	.LASF352
	.byte	0x13
	.2byte	0x657
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF348
	.byte	0x13
	.2byte	0x65b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF365
	.byte	0x13
	.2byte	0x65f
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF366
	.byte	0x13
	.2byte	0x660
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF367
	.byte	0x13
	.2byte	0x661
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF368
	.byte	0x13
	.2byte	0x662
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xc
	.4byte	.LASF369
	.byte	0x13
	.2byte	0x668
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.4byte	.LASF370
	.byte	0x13
	.2byte	0x669
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xc
	.4byte	.LASF371
	.byte	0x13
	.2byte	0x66a
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0xc
	.4byte	.LASF372
	.byte	0x13
	.2byte	0x66b
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xc
	.4byte	.LASF373
	.byte	0x13
	.2byte	0x66c
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xc
	.4byte	.LASF374
	.byte	0x13
	.2byte	0x66d
	.4byte	0x1953
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xc
	.4byte	.LASF375
	.byte	0x13
	.2byte	0x671
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xc
	.4byte	.LASF376
	.byte	0x13
	.2byte	0x672
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.byte	0
	.uleb128 0xd
	.4byte	.LASF377
	.byte	0x13
	.2byte	0x674
	.4byte	0x1966
	.uleb128 0xb
	.byte	0x4
	.byte	0x13
	.2byte	0x687
	.4byte	0x1ae8
	.uleb128 0xf
	.4byte	.LASF378
	.byte	0x13
	.2byte	0x692
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF379
	.byte	0x13
	.2byte	0x696
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF380
	.byte	0x13
	.2byte	0x6a2
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF381
	.byte	0x13
	.2byte	0x6a9
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF382
	.byte	0x13
	.2byte	0x6af
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF383
	.byte	0x13
	.2byte	0x6b1
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF384
	.byte	0x13
	.2byte	0x6b3
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF385
	.byte	0x13
	.2byte	0x6b5
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x14
	.byte	0x4
	.byte	0x13
	.2byte	0x681
	.4byte	0x1b03
	.uleb128 0x15
	.4byte	.LASF104
	.byte	0x13
	.2byte	0x685
	.4byte	0x70
	.uleb128 0x12
	.4byte	0x1a4e
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.byte	0x13
	.2byte	0x67f
	.4byte	0x1b15
	.uleb128 0x13
	.4byte	0x1ae8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xd
	.4byte	.LASF386
	.byte	0x13
	.2byte	0x6be
	.4byte	0x1b03
	.uleb128 0xb
	.byte	0x58
	.byte	0x13
	.2byte	0x6c1
	.4byte	0x1c75
	.uleb128 0x19
	.ascii	"pbf\000"
	.byte	0x13
	.2byte	0x6c3
	.4byte	0x1b15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF387
	.byte	0x13
	.2byte	0x6c8
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF388
	.byte	0x13
	.2byte	0x6cd
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF389
	.byte	0x13
	.2byte	0x6d4
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF390
	.byte	0x13
	.2byte	0x6d6
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF391
	.byte	0x13
	.2byte	0x6d8
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF392
	.byte	0x13
	.2byte	0x6da
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF393
	.byte	0x13
	.2byte	0x6dc
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF394
	.byte	0x13
	.2byte	0x6e3
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xc
	.4byte	.LASF395
	.byte	0x13
	.2byte	0x6e5
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xc
	.4byte	.LASF396
	.byte	0x13
	.2byte	0x6e7
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.4byte	.LASF397
	.byte	0x13
	.2byte	0x6e9
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xc
	.4byte	.LASF398
	.byte	0x13
	.2byte	0x6ef
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xc
	.4byte	.LASF317
	.byte	0x13
	.2byte	0x6fa
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xc
	.4byte	.LASF399
	.byte	0x13
	.2byte	0x705
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0xc
	.4byte	.LASF400
	.byte	0x13
	.2byte	0x70c
	.4byte	0x91a
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xc
	.4byte	.LASF401
	.byte	0x13
	.2byte	0x718
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xc
	.4byte	.LASF402
	.byte	0x13
	.2byte	0x71a
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xc
	.4byte	.LASF403
	.byte	0x13
	.2byte	0x720
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xc
	.4byte	.LASF404
	.byte	0x13
	.2byte	0x722
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0xc
	.4byte	.LASF405
	.byte	0x13
	.2byte	0x72a
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xc
	.4byte	.LASF406
	.byte	0x13
	.2byte	0x72c
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.byte	0
	.uleb128 0xd
	.4byte	.LASF407
	.byte	0x13
	.2byte	0x72e
	.4byte	0x1b21
	.uleb128 0x1b
	.2byte	0x1d8
	.byte	0x13
	.2byte	0x731
	.4byte	0x1d52
	.uleb128 0xc
	.4byte	.LASF98
	.byte	0x13
	.2byte	0x736
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF352
	.byte	0x13
	.2byte	0x73f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF408
	.byte	0x13
	.2byte	0x749
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF409
	.byte	0x13
	.2byte	0x752
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF410
	.byte	0x13
	.2byte	0x756
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF411
	.byte	0x13
	.2byte	0x75b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF412
	.byte	0x13
	.2byte	0x762
	.4byte	0x1d52
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x19
	.ascii	"rip\000"
	.byte	0x13
	.2byte	0x76b
	.4byte	0x195a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x19
	.ascii	"ws\000"
	.byte	0x13
	.2byte	0x772
	.4byte	0x1d58
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xc
	.4byte	.LASF413
	.byte	0x13
	.2byte	0x77a
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x160
	.uleb128 0xc
	.4byte	.LASF414
	.byte	0x13
	.2byte	0x787
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x164
	.uleb128 0xc
	.4byte	.LASF348
	.byte	0x13
	.2byte	0x78e
	.4byte	0x1a42
	.byte	0x3
	.byte	0x23
	.uleb128 0x168
	.uleb128 0xc
	.4byte	.LASF350
	.byte	0x13
	.2byte	0x797
	.4byte	0x94d
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c8
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0x1889
	.uleb128 0x9
	.4byte	0x1c75
	.4byte	0x1d68
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xd
	.4byte	.LASF415
	.byte	0x13
	.2byte	0x79e
	.4byte	0x1c81
	.uleb128 0x1b
	.2byte	0x1634
	.byte	0x13
	.2byte	0x7a0
	.4byte	0x1dac
	.uleb128 0xc
	.4byte	.LASF244
	.byte	0x13
	.2byte	0x7a7
	.4byte	0x1e6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF416
	.byte	0x13
	.2byte	0x7ad
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x19
	.ascii	"poc\000"
	.byte	0x13
	.2byte	0x7b0
	.4byte	0x1dac
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x9
	.4byte	0x1d68
	.4byte	0x1dbc
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0xd
	.4byte	.LASF417
	.byte	0x13
	.2byte	0x7ba
	.4byte	0x1d74
	.uleb128 0xb
	.byte	0x60
	.byte	0x13
	.2byte	0x812
	.4byte	0x1f0d
	.uleb128 0xc
	.4byte	.LASF418
	.byte	0x13
	.2byte	0x814
	.4byte	0x7fe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF419
	.byte	0x13
	.2byte	0x816
	.4byte	0x7fe
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF420
	.byte	0x13
	.2byte	0x820
	.4byte	0x7fe
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF421
	.byte	0x13
	.2byte	0x823
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x19
	.ascii	"bsr\000"
	.byte	0x13
	.2byte	0x82d
	.4byte	0x1d52
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.4byte	.LASF422
	.byte	0x13
	.2byte	0x839
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xc
	.4byte	.LASF423
	.byte	0x13
	.2byte	0x841
	.4byte	0x82
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xc
	.4byte	.LASF424
	.byte	0x13
	.2byte	0x847
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xc
	.4byte	.LASF425
	.byte	0x13
	.2byte	0x849
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0xc
	.4byte	.LASF426
	.byte	0x13
	.2byte	0x84b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xc
	.4byte	.LASF427
	.byte	0x13
	.2byte	0x84e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xc
	.4byte	.LASF428
	.byte	0x13
	.2byte	0x854
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x19
	.ascii	"bbf\000"
	.byte	0x13
	.2byte	0x85a
	.4byte	0x470
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xc
	.4byte	.LASF429
	.byte	0x13
	.2byte	0x85c
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xc
	.4byte	.LASF430
	.byte	0x13
	.2byte	0x85f
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x52
	.uleb128 0xc
	.4byte	.LASF431
	.byte	0x13
	.2byte	0x863
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xc
	.4byte	.LASF432
	.byte	0x13
	.2byte	0x86b
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x56
	.uleb128 0xc
	.4byte	.LASF433
	.byte	0x13
	.2byte	0x872
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xc
	.4byte	.LASF434
	.byte	0x13
	.2byte	0x875
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x5a
	.uleb128 0xc
	.4byte	.LASF118
	.byte	0x13
	.2byte	0x87d
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x5b
	.uleb128 0xc
	.4byte	.LASF435
	.byte	0x13
	.2byte	0x886
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.byte	0
	.uleb128 0xd
	.4byte	.LASF436
	.byte	0x13
	.2byte	0x88d
	.4byte	0x1dc8
	.uleb128 0x18
	.4byte	0x12140
	.byte	0x13
	.2byte	0x892
	.4byte	0x1ff3
	.uleb128 0xc
	.4byte	.LASF244
	.byte	0x13
	.2byte	0x899
	.4byte	0x1e6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF437
	.byte	0x13
	.2byte	0x8a0
	.4byte	0x7ba
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF438
	.byte	0x13
	.2byte	0x8a6
	.4byte	0x7ba
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xc
	.4byte	.LASF439
	.byte	0x13
	.2byte	0x8b0
	.4byte	0x7ba
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0xc
	.4byte	.LASF440
	.byte	0x13
	.2byte	0x8be
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0xc
	.4byte	.LASF441
	.byte	0x13
	.2byte	0x8c8
	.4byte	0x93d
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xc
	.4byte	.LASF442
	.byte	0x13
	.2byte	0x8cc
	.4byte	0x1ab
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0xc
	.4byte	.LASF443
	.byte	0x13
	.2byte	0x8ce
	.4byte	0x1ab
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0xc
	.4byte	.LASF444
	.byte	0x13
	.2byte	0x8d4
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0xc
	.4byte	.LASF445
	.byte	0x13
	.2byte	0x8de
	.4byte	0x1ff3
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0xc
	.4byte	.LASF446
	.byte	0x13
	.2byte	0x8e2
	.4byte	0xad
	.byte	0x4
	.byte	0x23
	.uleb128 0x1208c
	.uleb128 0xc
	.4byte	.LASF447
	.byte	0x13
	.2byte	0x8e4
	.4byte	0x2004
	.byte	0x4
	.byte	0x23
	.uleb128 0x12090
	.uleb128 0xc
	.4byte	.LASF350
	.byte	0x13
	.2byte	0x8ed
	.4byte	0x1d6
	.byte	0x4
	.byte	0x23
	.uleb128 0x120c0
	.byte	0
	.uleb128 0x9
	.4byte	0x1f0d
	.4byte	0x2004
	.uleb128 0x1a
	.4byte	0x25
	.2byte	0x2ff
	.byte	0
	.uleb128 0x9
	.4byte	0x20f
	.4byte	0x2014
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0xd
	.4byte	.LASF448
	.byte	0x13
	.2byte	0x8f4
	.4byte	0x1f19
	.uleb128 0x1c
	.2byte	0x4ac
	.byte	0x14
	.byte	0x1a
	.4byte	0x2055
	.uleb128 0x7
	.4byte	.LASF449
	.byte	0x14
	.byte	0x1c
	.4byte	0x136b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF450
	.byte	0x14
	.byte	0x1e
	.4byte	0x2055
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x7
	.4byte	.LASF451
	.byte	0x14
	.byte	0x20
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x4a8
	.byte	0
	.uleb128 0x9
	.4byte	0x1a42
	.4byte	0x2065
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x3
	.4byte	.LASF452
	.byte	0x14
	.byte	0x22
	.4byte	0x2020
	.uleb128 0x1d
	.4byte	.LASF458
	.byte	0x1
	.byte	0x4a
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x20db
	.uleb128 0x1e
	.4byte	.LASF453
	.byte	0x1
	.byte	0x4a
	.4byte	0x20db
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1f
	.ascii	"d\000"
	.byte	0x1
	.byte	0x4a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x20
	.4byte	.LASF454
	.byte	0x1
	.byte	0x4c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF455
	.byte	0x1
	.byte	0x4c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF456
	.byte	0x1
	.byte	0x4c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x20
	.4byte	.LASF457
	.byte	0x1
	.byte	0x4c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0x17d
	.uleb128 0x1d
	.4byte	.LASF459
	.byte	0x1
	.byte	0x64
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x2124
	.uleb128 0x1e
	.4byte	.LASF460
	.byte	0x1
	.byte	0x64
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x20
	.4byte	.LASF461
	.byte	0x1
	.byte	0x66
	.4byte	0x2124
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF462
	.byte	0x1
	.byte	0x68
	.4byte	0x212a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0x1a42
	.uleb128 0x16
	.byte	0x4
	.4byte	0x136b
	.uleb128 0x1d
	.4byte	.LASF463
	.byte	0x1
	.byte	0x7c
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x21a9
	.uleb128 0x1e
	.4byte	.LASF464
	.byte	0x1
	.byte	0x7c
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1e
	.4byte	.LASF465
	.byte	0x1
	.byte	0x7c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1e
	.4byte	.LASF466
	.byte	0x1
	.byte	0x7c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1e
	.4byte	.LASF467
	.byte	0x1
	.byte	0x7c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x21
	.ascii	"i\000"
	.byte	0x1
	.byte	0x7e
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF408
	.byte	0x1
	.byte	0x7f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF468
	.byte	0x1
	.byte	0x80
	.4byte	0x21af
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0xeb2
	.uleb128 0x16
	.byte	0x4
	.4byte	0x70
	.uleb128 0x1d
	.4byte	.LASF469
	.byte	0x1
	.byte	0x9c
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x2228
	.uleb128 0x1e
	.4byte	.LASF464
	.byte	0x1
	.byte	0x9c
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x21
	.ascii	"i\000"
	.byte	0x1
	.byte	0x9e
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.ascii	"j\000"
	.byte	0x1
	.byte	0x9f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.byte	0xa0
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x20
	.4byte	.LASF470
	.byte	0x1
	.byte	0xa1
	.4byte	0x2228
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x22
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x20
	.4byte	.LASF471
	.byte	0x1
	.byte	0xac
	.4byte	0x222e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0x80f
	.uleb128 0x23
	.4byte	0x2233
	.uleb128 0x16
	.byte	0x4
	.4byte	0x2239
	.uleb128 0x23
	.4byte	0x1d68
	.uleb128 0x24
	.4byte	.LASF476
	.byte	0x1
	.byte	0xc8
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x22c8
	.uleb128 0x1e
	.4byte	.LASF472
	.byte	0x1
	.byte	0xc8
	.4byte	0x22c8
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1f
	.ascii	"end\000"
	.byte	0x1
	.byte	0xc9
	.4byte	0x22c8
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x1e
	.4byte	.LASF353
	.byte	0x1
	.byte	0xca
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1e
	.4byte	.LASF473
	.byte	0x1
	.byte	0xcb
	.4byte	0x22d8
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x1e
	.4byte	.LASF474
	.byte	0x1
	.byte	0xcc
	.4byte	0x22e8
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x21
	.ascii	"d\000"
	.byte	0x1
	.byte	0xce
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF475
	.byte	0x1
	.byte	0xd0
	.4byte	0x17d
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xd2
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.4byte	0x22cd
	.uleb128 0x16
	.byte	0x4
	.4byte	0x22d3
	.uleb128 0x23
	.4byte	0x17d
	.uleb128 0x23
	.4byte	0x22dd
	.uleb128 0x16
	.byte	0x4
	.4byte	0x22e3
	.uleb128 0x23
	.4byte	0x921
	.uleb128 0x23
	.4byte	0x22ed
	.uleb128 0x16
	.byte	0x4
	.4byte	0x95d
	.uleb128 0x25
	.4byte	.LASF477
	.byte	0x1
	.2byte	0x108
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x23dc
	.uleb128 0x26
	.4byte	.LASF478
	.byte	0x1
	.2byte	0x108
	.4byte	0x22c8
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x26
	.4byte	.LASF479
	.byte	0x1
	.2byte	0x109
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x26
	.4byte	.LASF473
	.byte	0x1
	.2byte	0x10a
	.4byte	0x22d8
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x27
	.ascii	"TEN\000"
	.byte	0x1
	.2byte	0x10d
	.4byte	0x23dc
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x28
	.4byte	.LASF480
	.byte	0x1
	.2byte	0x10f
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF481
	.byte	0x1
	.2byte	0x111
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF128
	.byte	0x1
	.2byte	0x113
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF482
	.byte	0x1
	.2byte	0x115
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x27
	.ascii	"cc\000"
	.byte	0x1
	.2byte	0x117
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x28
	.4byte	.LASF483
	.byte	0x1
	.2byte	0x119
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x27
	.ascii	"rt\000"
	.byte	0x1
	.2byte	0x11b
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x27
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x11d
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x22
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0x27
	.ascii	"idx\000"
	.byte	0x1
	.2byte	0x14a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.byte	0
	.byte	0
	.uleb128 0x29
	.4byte	0x23e1
	.uleb128 0x23
	.4byte	0x91a
	.uleb128 0x25
	.4byte	.LASF484
	.byte	0x1
	.2byte	0x160
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x24b0
	.uleb128 0x26
	.4byte	.LASF485
	.byte	0x1
	.2byte	0x160
	.4byte	0x22c8
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x26
	.4byte	.LASF486
	.byte	0x1
	.2byte	0x160
	.4byte	0x24b0
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x26
	.4byte	.LASF473
	.byte	0x1
	.2byte	0x160
	.4byte	0x22d8
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x26
	.4byte	.LASF487
	.byte	0x1
	.2byte	0x160
	.4byte	0x24c0
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x27
	.ascii	"d\000"
	.byte	0x1
	.2byte	0x162
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF488
	.byte	0x1
	.2byte	0x164
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF353
	.byte	0x1
	.2byte	0x166
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF489
	.byte	0x1
	.2byte	0x168
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF490
	.byte	0x1
	.2byte	0x16a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x27
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x16c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x22
	.4byte	.LBB4
	.4byte	.LBE4
	.uleb128 0x27
	.ascii	"dow\000"
	.byte	0x1
	.2byte	0x186
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.byte	0
	.uleb128 0x23
	.4byte	0x24b5
	.uleb128 0x16
	.byte	0x4
	.4byte	0x24bb
	.uleb128 0x23
	.4byte	0xf28
	.uleb128 0x16
	.byte	0x4
	.4byte	0x92c
	.uleb128 0x25
	.4byte	.LASF491
	.byte	0x1
	.2byte	0x1b4
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x254f
	.uleb128 0x26
	.4byte	.LASF492
	.byte	0x1
	.2byte	0x1b4
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF493
	.byte	0x1
	.2byte	0x1b6
	.4byte	0x23dc
	.byte	0x5
	.byte	0x3
	.4byte	SIXTY.8002
	.uleb128 0x28
	.4byte	.LASF494
	.byte	0x1
	.2byte	0x1b8
	.4byte	0x254f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF495
	.byte	0x1
	.2byte	0x1ba
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF467
	.byte	0x1
	.2byte	0x1bc
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x27
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1be
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF496
	.byte	0x1
	.2byte	0x1c0
	.4byte	0x2555
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0x1f0d
	.uleb128 0x16
	.byte	0x4
	.4byte	0x820
	.uleb128 0x2a
	.byte	0x1
	.4byte	.LASF550
	.byte	0x1
	.2byte	0x1e3
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x25c4
	.uleb128 0x26
	.4byte	.LASF479
	.byte	0x1
	.2byte	0x1e3
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x26
	.4byte	.LASF497
	.byte	0x1
	.2byte	0x1e3
	.4byte	0x25c4
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x26
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x1e3
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x26
	.4byte	.LASF498
	.byte	0x1
	.2byte	0x1e3
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x27
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1e5
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0x82
	.uleb128 0x25
	.4byte	.LASF499
	.byte	0x1
	.2byte	0x20d
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x27b6
	.uleb128 0x26
	.4byte	.LASF492
	.byte	0x1
	.2byte	0x20d
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -204
	.uleb128 0x2b
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x20e
	.4byte	0x27b6
	.byte	0x3
	.byte	0x91
	.sleb128 -208
	.uleb128 0x26
	.4byte	.LASF485
	.byte	0x1
	.2byte	0x20f
	.4byte	0x22c8
	.byte	0x3
	.byte	0x91
	.sleb128 -212
	.uleb128 0x26
	.4byte	.LASF500
	.byte	0x1
	.2byte	0x210
	.4byte	0x27c1
	.byte	0x2
	.byte	0x91
	.sleb128 -4
	.uleb128 0x26
	.4byte	.LASF501
	.byte	0x1
	.2byte	0x211
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x28
	.4byte	.LASF493
	.byte	0x1
	.2byte	0x213
	.4byte	0x23dc
	.byte	0x5
	.byte	0x3
	.4byte	SIXTY.8025
	.uleb128 0x28
	.4byte	.LASF502
	.byte	0x1
	.2byte	0x215
	.4byte	0x23dc
	.byte	0x5
	.byte	0x3
	.4byte	HUNDRED.8026
	.uleb128 0x27
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x217
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF503
	.byte	0x1
	.2byte	0x219
	.4byte	0x24c0
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x28
	.4byte	.LASF504
	.byte	0x1
	.2byte	0x21b
	.4byte	0x22ed
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x28
	.4byte	.LASF496
	.byte	0x1
	.2byte	0x21d
	.4byte	0x2555
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF505
	.byte	0x1
	.2byte	0x21f
	.4byte	0x921
	.byte	0x3
	.byte	0x91
	.sleb128 -156
	.uleb128 0x28
	.4byte	.LASF353
	.byte	0x1
	.2byte	0x221
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x28
	.4byte	.LASF479
	.byte	0x1
	.2byte	0x224
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x28
	.4byte	.LASF506
	.byte	0x1
	.2byte	0x226
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -92
	.uleb128 0x28
	.4byte	.LASF507
	.byte	0x1
	.2byte	0x228
	.4byte	0x91a
	.byte	0x3
	.byte	0x91
	.sleb128 -88
	.uleb128 0x28
	.4byte	.LASF508
	.byte	0x1
	.2byte	0x22a
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF498
	.byte	0x1
	.2byte	0x22c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x28
	.4byte	.LASF509
	.byte	0x1
	.2byte	0x22e
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x28
	.4byte	.LASF510
	.byte	0x1
	.2byte	0x230
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x28
	.4byte	.LASF478
	.byte	0x1
	.2byte	0x232
	.4byte	0x17d
	.byte	0x3
	.byte	0x91
	.sleb128 -176
	.uleb128 0x28
	.4byte	.LASF511
	.byte	0x1
	.2byte	0x234
	.4byte	0x17d
	.byte	0x3
	.byte	0x91
	.sleb128 -196
	.uleb128 0x28
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x236
	.4byte	0x91a
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x28
	.4byte	.LASF497
	.byte	0x1
	.2byte	0x238
	.4byte	0x82
	.byte	0x3
	.byte	0x91
	.sleb128 -200
	.uleb128 0x28
	.4byte	.LASF512
	.byte	0x1
	.2byte	0x23a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x27
	.ascii	"sf\000"
	.byte	0x1
	.2byte	0x23c
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x22
	.4byte	.LBB5
	.4byte	.LBE5
	.uleb128 0x28
	.4byte	.LASF464
	.byte	0x1
	.2byte	0x278
	.4byte	0x21a9
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x28
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x279
	.4byte	0x91a
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x27
	.ascii	"rl\000"
	.byte	0x1
	.2byte	0x27a
	.4byte	0x91a
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.byte	0
	.byte	0
	.uleb128 0x23
	.4byte	0x27bb
	.uleb128 0x16
	.byte	0x4
	.4byte	0xf28
	.uleb128 0x23
	.4byte	0xeb
	.uleb128 0x25
	.4byte	.LASF513
	.byte	0x1
	.2byte	0x33a
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x282f
	.uleb128 0x26
	.4byte	.LASF514
	.byte	0x1
	.2byte	0x33a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x26
	.4byte	.LASF515
	.byte	0x1
	.2byte	0x33a
	.4byte	0x21af
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF516
	.byte	0x1
	.2byte	0x33c
	.4byte	0x282f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF517
	.byte	0x1
	.2byte	0x33e
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF518
	.byte	0x1
	.2byte	0x340
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0x195a
	.uleb128 0x2c
	.byte	0x1
	.4byte	.LASF557
	.byte	0x1
	.2byte	0x36e
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x28da
	.uleb128 0x26
	.4byte	.LASF514
	.byte	0x1
	.2byte	0x36e
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x26
	.4byte	.LASF519
	.byte	0x1
	.2byte	0x36f
	.4byte	0x28da
	.byte	0x3
	.byte	0x91
	.sleb128 -88
	.uleb128 0x28
	.4byte	.LASF517
	.byte	0x1
	.2byte	0x371
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF520
	.byte	0x1
	.2byte	0x372
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF521
	.byte	0x1
	.2byte	0x373
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF522
	.byte	0x1
	.2byte	0x374
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF515
	.byte	0x1
	.2byte	0x375
	.4byte	0x93d
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x27
	.ascii	"bsr\000"
	.byte	0x1
	.2byte	0x376
	.4byte	0x1d52
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x27
	.ascii	"pgs\000"
	.byte	0x1
	.2byte	0x377
	.4byte	0x2228
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.uleb128 0x16
	.byte	0x4
	.4byte	0x91a
	.uleb128 0x2d
	.4byte	.LASF523
	.byte	0x1
	.2byte	0x3e0
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x2974
	.uleb128 0x26
	.4byte	.LASF524
	.byte	0x1
	.2byte	0x3e0
	.4byte	0x21a9
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x28
	.4byte	.LASF517
	.byte	0x1
	.2byte	0x3e2
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF514
	.byte	0x1
	.2byte	0x3e4
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF520
	.byte	0x1
	.2byte	0x3e6
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF525
	.byte	0x1
	.2byte	0x3e8
	.4byte	0x2974
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x28
	.4byte	.LASF348
	.byte	0x1
	.2byte	0x3ea
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF526
	.byte	0x1
	.2byte	0x3ec
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF527
	.byte	0x1
	.2byte	0x3ee
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.uleb128 0x9
	.4byte	0x91a
	.4byte	0x2984
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x25
	.4byte	.LASF528
	.byte	0x1
	.2byte	0x43f
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x2a37
	.uleb128 0x26
	.4byte	.LASF529
	.byte	0x1
	.2byte	0x43f
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x26
	.4byte	.LASF221
	.byte	0x1
	.2byte	0x43f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x26
	.4byte	.LASF530
	.byte	0x1
	.2byte	0x43f
	.4byte	0x2a37
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x26
	.4byte	.LASF531
	.byte	0x1
	.2byte	0x43f
	.4byte	0x2a3c
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x28
	.4byte	.LASF532
	.byte	0x1
	.2byte	0x441
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x27
	.ascii	"Vp\000"
	.byte	0x1
	.2byte	0x443
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x445
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF533
	.byte	0x1
	.2byte	0x447
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF422
	.byte	0x1
	.2byte	0x449
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF199
	.byte	0x1
	.2byte	0x44b
	.4byte	0x33
	.byte	0x2
	.byte	0x91
	.sleb128 -13
	.byte	0
	.uleb128 0x23
	.4byte	0x21a9
	.uleb128 0x23
	.4byte	0x2555
	.uleb128 0x2d
	.4byte	.LASF534
	.byte	0x1
	.2byte	0x4c1
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x2b11
	.uleb128 0x26
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x4c1
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -1356
	.uleb128 0x26
	.4byte	.LASF486
	.byte	0x1
	.2byte	0x4c1
	.4byte	0x24b0
	.byte	0x3
	.byte	0x91
	.sleb128 -1360
	.uleb128 0x26
	.4byte	.LASF485
	.byte	0x1
	.2byte	0x4c1
	.4byte	0x17d
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x28
	.4byte	.LASF535
	.byte	0x1
	.2byte	0x4c3
	.4byte	0x2124
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF449
	.byte	0x1
	.2byte	0x4c5
	.4byte	0x212a
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x28
	.4byte	.LASF464
	.byte	0x1
	.2byte	0x4c7
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x27
	.ascii	"bsr\000"
	.byte	0x1
	.2byte	0x4c9
	.4byte	0x1d52
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF536
	.byte	0x1
	.2byte	0x4cc
	.4byte	0x2065
	.byte	0x3
	.byte	0x91
	.sleb128 -1240
	.uleb128 0x28
	.4byte	.LASF537
	.byte	0x1
	.2byte	0x4ce
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x28
	.4byte	.LASF538
	.byte	0x1
	.2byte	0x4d0
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x27
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x4d2
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x27
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x4d4
	.4byte	0xf28
	.byte	0x3
	.byte	0x91
	.sleb128 -1352
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF539
	.byte	0x1
	.2byte	0x52f
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x2b74
	.uleb128 0x26
	.4byte	.LASF540
	.byte	0x1
	.2byte	0x52f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x26
	.4byte	.LASF486
	.byte	0x1
	.2byte	0x52f
	.4byte	0x24b0
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x27
	.ascii	"j\000"
	.byte	0x1
	.2byte	0x531
	.4byte	0x82
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF470
	.byte	0x1
	.2byte	0x533
	.4byte	0x2228
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF541
	.byte	0x1
	.2byte	0x535
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF542
	.byte	0x1
	.2byte	0x553
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x2bd7
	.uleb128 0x26
	.4byte	.LASF540
	.byte	0x1
	.2byte	0x553
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x26
	.4byte	.LASF486
	.byte	0x1
	.2byte	0x553
	.4byte	0x24b0
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x27
	.ascii	"j\000"
	.byte	0x1
	.2byte	0x555
	.4byte	0x8d
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF470
	.byte	0x1
	.2byte	0x557
	.4byte	0x2228
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF541
	.byte	0x1
	.2byte	0x559
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF543
	.byte	0x1
	.2byte	0x576
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x2c67
	.uleb128 0x26
	.4byte	.LASF544
	.byte	0x1
	.2byte	0x576
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x26
	.4byte	.LASF486
	.byte	0x1
	.2byte	0x576
	.4byte	0x27bb
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x27
	.ascii	"j\000"
	.byte	0x1
	.2byte	0x578
	.4byte	0x82
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF545
	.byte	0x1
	.2byte	0x57a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x27
	.ascii	"day\000"
	.byte	0x1
	.2byte	0x57a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF465
	.byte	0x1
	.2byte	0x57a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x28
	.4byte	.LASF466
	.byte	0x1
	.2byte	0x57a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x27
	.ascii	"dow\000"
	.byte	0x1
	.2byte	0x57a
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF546
	.byte	0x1
	.2byte	0x5b0
	.byte	0x1
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0x2cf7
	.uleb128 0x26
	.4byte	.LASF544
	.byte	0x1
	.2byte	0x5b0
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x26
	.4byte	.LASF486
	.byte	0x1
	.2byte	0x5b0
	.4byte	0x27bb
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x27
	.ascii	"j\000"
	.byte	0x1
	.2byte	0x5b2
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF545
	.byte	0x1
	.2byte	0x5b4
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x27
	.ascii	"day\000"
	.byte	0x1
	.2byte	0x5b4
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF465
	.byte	0x1
	.2byte	0x5b4
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF466
	.byte	0x1
	.2byte	0x5b4
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x27
	.ascii	"dow\000"
	.byte	0x1
	.2byte	0x5b4
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF547
	.byte	0x1
	.2byte	0x5e2
	.byte	0x1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0x2d55
	.uleb128 0x26
	.4byte	.LASF524
	.byte	0x1
	.2byte	0x5e2
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x27
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x5e4
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF548
	.byte	0x1
	.2byte	0x5e6
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x22
	.4byte	.LBB6
	.4byte	.LBE6
	.uleb128 0x28
	.4byte	.LASF471
	.byte	0x1
	.2byte	0x5ee
	.4byte	0x2d55
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x23
	.4byte	0x2d5a
	.uleb128 0x16
	.byte	0x4
	.4byte	0x1d68
	.uleb128 0x2d
	.4byte	.LASF549
	.byte	0x1
	.2byte	0x5f9
	.byte	0x1
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.4byte	0x2dbe
	.uleb128 0x26
	.4byte	.LASF524
	.byte	0x1
	.2byte	0x5f9
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x27
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x5fb
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF548
	.byte	0x1
	.2byte	0x5fd
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x22
	.4byte	.LBB7
	.4byte	.LBE7
	.uleb128 0x28
	.4byte	.LASF471
	.byte	0x1
	.2byte	0x605
	.4byte	0x2d55
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x2a
	.byte	0x1
	.4byte	.LASF551
	.byte	0x1
	.2byte	0x624
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0x2ea8
	.uleb128 0x26
	.4byte	.LASF529
	.byte	0x1
	.2byte	0x624
	.4byte	0x91a
	.byte	0x3
	.byte	0x91
	.sleb128 -164
	.uleb128 0x26
	.4byte	.LASF531
	.byte	0x1
	.2byte	0x624
	.4byte	0x2555
	.byte	0x3
	.byte	0x91
	.sleb128 -168
	.uleb128 0x2b
	.ascii	"dt\000"
	.byte	0x1
	.2byte	0x624
	.4byte	0xeb
	.byte	0x3
	.byte	0x91
	.sleb128 -176
	.uleb128 0x28
	.4byte	.LASF532
	.byte	0x1
	.2byte	0x626
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF552
	.byte	0x1
	.2byte	0x628
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF530
	.byte	0x1
	.2byte	0x62a
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x27
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x62c
	.4byte	0xf28
	.byte	0x3
	.byte	0x91
	.sleb128 -160
	.uleb128 0x27
	.ascii	"bsr\000"
	.byte	0x1
	.2byte	0x62e
	.4byte	0x1d52
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x28
	.4byte	.LASF553
	.byte	0x1
	.2byte	0x631
	.4byte	0xeb
	.byte	0x5
	.byte	0x3
	.4byte	dt_flag.8209
	.uleb128 0x28
	.4byte	.LASF554
	.byte	0x1
	.2byte	0x632
	.4byte	0x70
	.byte	0x5
	.byte	0x3
	.4byte	saved_gid.8210
	.uleb128 0x28
	.4byte	.LASF555
	.byte	0x1
	.2byte	0x634
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x27
	.ascii	"Vp\000"
	.byte	0x1
	.2byte	0x636
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x28
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x637
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x2a
	.byte	0x1
	.4byte	.LASF556
	.byte	0x1
	.2byte	0x6d0
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0x2f20
	.uleb128 0x26
	.4byte	.LASF492
	.byte	0x1
	.2byte	0x6d0
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x2b
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x6d1
	.4byte	0x27b6
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x26
	.4byte	.LASF485
	.byte	0x1
	.2byte	0x6d2
	.4byte	0x22c8
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x26
	.4byte	.LASF500
	.byte	0x1
	.2byte	0x6d3
	.4byte	0x27c1
	.byte	0x2
	.byte	0x91
	.sleb128 -4
	.uleb128 0x26
	.4byte	.LASF501
	.byte	0x1
	.2byte	0x6d4
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x27
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x6d6
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x2c
	.byte	0x1
	.4byte	.LASF558
	.byte	0x1
	.2byte	0x6fa
	.byte	0x1
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST23
	.4byte	0x2fd1
	.uleb128 0x26
	.4byte	.LASF559
	.byte	0x1
	.2byte	0x6fa
	.4byte	0x22c8
	.byte	0x3
	.byte	0x91
	.sleb128 -152
	.uleb128 0x26
	.4byte	.LASF560
	.byte	0x1
	.2byte	0x6fa
	.4byte	0xad
	.byte	0x3
	.byte	0x91
	.sleb128 -156
	.uleb128 0x27
	.ascii	"Vp\000"
	.byte	0x1
	.2byte	0x6fc
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x28
	.4byte	.LASF524
	.byte	0x1
	.2byte	0x6fe
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x27
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x700
	.4byte	0xf28
	.byte	0x3
	.byte	0x91
	.sleb128 -148
	.uleb128 0x28
	.4byte	.LASF561
	.byte	0x1
	.2byte	0x702
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x27
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x704
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x27
	.ascii	"bsr\000"
	.byte	0x1
	.2byte	0x706
	.4byte	0x1d52
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF562
	.byte	0x1
	.2byte	0x707
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF563
	.byte	0x1
	.2byte	0x70a
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x2e
	.byte	0x1
	.4byte	.LASF564
	.byte	0x1
	.2byte	0x767
	.4byte	0x70
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST24
	.4byte	0x3056
	.uleb128 0x27
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x769
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF524
	.byte	0x1
	.2byte	0x76b
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x27
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x76d
	.4byte	0xf28
	.byte	0x3
	.byte	0x91
	.sleb128 -152
	.uleb128 0x27
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x76f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x771
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x28
	.4byte	.LASF561
	.byte	0x1
	.2byte	0x773
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x27
	.ascii	"bsr\000"
	.byte	0x1
	.2byte	0x775
	.4byte	0x1d52
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.uleb128 0x2c
	.byte	0x1
	.4byte	.LASF565
	.byte	0x1
	.2byte	0x7a8
	.byte	0x1
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST25
	.4byte	0x3206
	.uleb128 0x26
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x7a8
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -264
	.uleb128 0x26
	.4byte	.LASF485
	.byte	0x1
	.2byte	0x7a8
	.4byte	0x17d
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF464
	.byte	0x1
	.2byte	0x7aa
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x27
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x7ab
	.4byte	0xf28
	.byte	0x3
	.byte	0x91
	.sleb128 -212
	.uleb128 0x28
	.4byte	.LASF348
	.byte	0x1
	.2byte	0x7ac
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.uleb128 0x28
	.4byte	.LASF537
	.byte	0x1
	.2byte	0x7ad
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x28
	.4byte	.LASF526
	.byte	0x1
	.2byte	0x7ae
	.4byte	0x91a
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x28
	.4byte	.LASF517
	.byte	0x1
	.2byte	0x7af
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF470
	.byte	0x1
	.2byte	0x7b0
	.4byte	0x2228
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x28
	.4byte	.LASF566
	.byte	0x1
	.2byte	0x7b1
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x27
	.ascii	"bsr\000"
	.byte	0x1
	.2byte	0x7b2
	.4byte	0x1d52
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x28
	.4byte	.LASF562
	.byte	0x1
	.2byte	0x7b3
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x28
	.4byte	.LASF525
	.byte	0x1
	.2byte	0x7b4
	.4byte	0x2974
	.byte	0x3
	.byte	0x91
	.sleb128 -260
	.uleb128 0x28
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x7b5
	.4byte	0x91a
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x28
	.4byte	.LASF567
	.byte	0x1
	.2byte	0x7b6
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -100
	.uleb128 0x28
	.4byte	.LASF568
	.byte	0x1
	.2byte	0x7b7
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x22
	.4byte	.LBB8
	.4byte	.LBE8
	.uleb128 0x28
	.4byte	.LASF471
	.byte	0x1
	.2byte	0x7e1
	.4byte	0x222e
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x22
	.4byte	.LBB9
	.4byte	.LBE9
	.uleb128 0x28
	.4byte	.LASF569
	.byte	0x1
	.2byte	0x804
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x28
	.4byte	.LASF570
	.byte	0x1
	.2byte	0x810
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x28
	.4byte	.LASF571
	.byte	0x1
	.2byte	0x811
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -88
	.uleb128 0x22
	.4byte	.LBB10
	.4byte	.LBE10
	.uleb128 0x27
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x812
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x22
	.4byte	.LBB11
	.4byte	.LBE11
	.uleb128 0x28
	.4byte	.LASF504
	.byte	0x1
	.2byte	0x814
	.4byte	0x22ed
	.byte	0x3
	.byte	0x91
	.sleb128 -92
	.uleb128 0x22
	.4byte	.LBB12
	.4byte	.LBE12
	.uleb128 0x28
	.4byte	.LASF572
	.byte	0x1
	.2byte	0x818
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -96
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2c
	.byte	0x1
	.4byte	.LASF573
	.byte	0x1
	.2byte	0x861
	.byte	0x1
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST26
	.4byte	0x32c4
	.uleb128 0x26
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x861
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -160
	.uleb128 0x2b
	.ascii	"psg\000"
	.byte	0x1
	.2byte	0x861
	.4byte	0x22ed
	.byte	0x3
	.byte	0x91
	.sleb128 -164
	.uleb128 0x28
	.4byte	.LASF464
	.byte	0x1
	.2byte	0x863
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x27
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x864
	.4byte	0xf28
	.byte	0x3
	.byte	0x91
	.sleb128 -156
	.uleb128 0x27
	.ascii	"bsr\000"
	.byte	0x1
	.2byte	0x865
	.4byte	0x1d52
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF562
	.byte	0x1
	.2byte	0x866
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x867
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x28
	.4byte	.LASF574
	.byte	0x1
	.2byte	0x868
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF575
	.byte	0x1
	.2byte	0x869
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x22
	.4byte	.LBB13
	.4byte	.LBE13
	.uleb128 0x28
	.4byte	.LASF533
	.byte	0x1
	.2byte	0x888
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.byte	0
	.uleb128 0x2c
	.byte	0x1
	.4byte	.LASF576
	.byte	0x1
	.2byte	0x8b0
	.byte	0x1
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST27
	.4byte	0x3432
	.uleb128 0x26
	.4byte	.LASF577
	.byte	0x1
	.2byte	0x8b0
	.4byte	0x17d
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF578
	.byte	0x1
	.2byte	0x8b5
	.4byte	0xeb
	.byte	0x5
	.byte	0x3
	.4byte	dt_bug_check.8299
	.uleb128 0x27
	.ascii	"dt\000"
	.byte	0x1
	.2byte	0x8b7
	.4byte	0xeb
	.byte	0x3
	.byte	0x91
	.sleb128 -88
	.uleb128 0x28
	.4byte	.LASF579
	.byte	0x1
	.2byte	0x8b9
	.4byte	0xeb
	.byte	0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x28
	.4byte	.LASF580
	.byte	0x1
	.2byte	0x8bb
	.4byte	0xeb
	.byte	0x3
	.byte	0x91
	.sleb128 -104
	.uleb128 0x28
	.4byte	.LASF561
	.byte	0x1
	.2byte	0x8bd
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x27
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x8bf
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x28
	.4byte	.LASF464
	.byte	0x1
	.2byte	0x8c1
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x28
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x8c3
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x27
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x8c5
	.4byte	0xf28
	.byte	0x3
	.byte	0x91
	.sleb128 -216
	.uleb128 0x27
	.ascii	"bp\000"
	.byte	0x1
	.2byte	0x8c7
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x27
	.ascii	"bsr\000"
	.byte	0x1
	.2byte	0x8c9
	.4byte	0x1d52
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x28
	.4byte	.LASF581
	.byte	0x1
	.2byte	0x8cb
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x2f
	.4byte	.LBB14
	.4byte	.LBE14
	.4byte	0x33c1
	.uleb128 0x28
	.4byte	.LASF471
	.byte	0x1
	.2byte	0x910
	.4byte	0x222e
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.byte	0
	.uleb128 0x2f
	.4byte	.LBB15
	.4byte	.LBE15
	.4byte	0x33df
	.uleb128 0x28
	.4byte	.LASF471
	.byte	0x1
	.2byte	0x93f
	.4byte	0x222e
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.byte	0
	.uleb128 0x2f
	.4byte	.LBB16
	.4byte	.LBE16
	.4byte	0x3417
	.uleb128 0x28
	.4byte	.LASF471
	.byte	0x1
	.2byte	0x977
	.4byte	0x222e
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x22
	.4byte	.LBB17
	.4byte	.LBE17
	.uleb128 0x28
	.4byte	.LASF582
	.byte	0x1
	.2byte	0x988
	.4byte	0x2228
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.byte	0
	.byte	0
	.uleb128 0x22
	.4byte	.LBB18
	.4byte	.LBE18
	.uleb128 0x28
	.4byte	.LASF471
	.byte	0x1
	.2byte	0x9a0
	.4byte	0x222e
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.byte	0
	.byte	0
	.uleb128 0x2a
	.byte	0x1
	.4byte	.LASF583
	.byte	0x1
	.2byte	0x9d1
	.byte	0x1
	.4byte	0x1953
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST28
	.4byte	0x348c
	.uleb128 0x26
	.4byte	.LASF584
	.byte	0x1
	.2byte	0x9d1
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x27
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x9d3
	.4byte	0x1953
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF464
	.byte	0x1
	.2byte	0x9d5
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF471
	.byte	0x1
	.2byte	0x9d7
	.4byte	0x222e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x2c
	.byte	0x1
	.4byte	.LASF585
	.byte	0x1
	.2byte	0xa0b
	.byte	0x1
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST29
	.4byte	0x34e3
	.uleb128 0x26
	.4byte	.LASF464
	.byte	0x1
	.2byte	0xa0b
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x26
	.4byte	.LASF465
	.byte	0x1
	.2byte	0xa0b
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x26
	.4byte	.LASF466
	.byte	0x1
	.2byte	0xa0b
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x26
	.4byte	.LASF467
	.byte	0x1
	.2byte	0xa0b
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x2a
	.byte	0x1
	.4byte	.LASF586
	.byte	0x1
	.2byte	0xa2b
	.byte	0x1
	.4byte	0xad
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST30
	.4byte	0x355e
	.uleb128 0x26
	.4byte	.LASF587
	.byte	0x1
	.2byte	0xa2b
	.4byte	0x355e
	.byte	0x3
	.byte	0x91
	.sleb128 -100
	.uleb128 0x28
	.4byte	.LASF588
	.byte	0x1
	.2byte	0xa2d
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF589
	.byte	0x1
	.2byte	0xa2f
	.4byte	0x2555
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF590
	.byte	0x1
	.2byte	0xa31
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF505
	.byte	0x1
	.2byte	0xa33
	.4byte	0x921
	.byte	0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x28
	.4byte	.LASF591
	.byte	0x1
	.2byte	0xa35
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x23
	.4byte	0x70
	.uleb128 0x25
	.4byte	.LASF592
	.byte	0x1
	.2byte	0xa76
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST31
	.4byte	0x35bd
	.uleb128 0x26
	.4byte	.LASF587
	.byte	0x1
	.2byte	0xa76
	.4byte	0x355e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF588
	.byte	0x1
	.2byte	0xa78
	.4byte	0x21a9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF589
	.byte	0x1
	.2byte	0xa7a
	.4byte	0x2555
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF590
	.byte	0x1
	.2byte	0xa7c
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x25
	.4byte	.LASF593
	.byte	0x1
	.2byte	0xab3
	.byte	0x1
	.4byte	0x91a
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST32
	.4byte	0x3626
	.uleb128 0x26
	.4byte	.LASF587
	.byte	0x1
	.2byte	0xab3
	.4byte	0x355e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF594
	.byte	0x1
	.2byte	0xab6
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.4byte	.LASF595
	.byte	0x1
	.2byte	0xab8
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF596
	.byte	0x1
	.2byte	0xaba
	.4byte	0x23dc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF597
	.byte	0x1
	.2byte	0xabc
	.4byte	0x23dc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x2c
	.byte	0x1
	.4byte	.LASF598
	.byte	0x1
	.2byte	0xadd
	.byte	0x1
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST33
	.4byte	0x3754
	.uleb128 0x26
	.4byte	.LASF524
	.byte	0x1
	.2byte	0xadd
	.4byte	0x21a9
	.byte	0x3
	.byte	0x91
	.sleb128 -300
	.uleb128 0x26
	.4byte	.LASF599
	.byte	0x1
	.2byte	0xadd
	.4byte	0xad
	.byte	0x3
	.byte	0x91
	.sleb128 -304
	.uleb128 0x28
	.4byte	.LASF517
	.byte	0x1
	.2byte	0xadf
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF514
	.byte	0x1
	.2byte	0xae1
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x28
	.4byte	.LASF520
	.byte	0x1
	.2byte	0xae3
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x28
	.4byte	.LASF595
	.byte	0x1
	.2byte	0xae5
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x28
	.4byte	.LASF600
	.byte	0x1
	.2byte	0xae7
	.4byte	0x2974
	.byte	0x3
	.byte	0x91
	.sleb128 -112
	.uleb128 0x28
	.4byte	.LASF601
	.byte	0x1
	.2byte	0xae9
	.4byte	0xf28
	.byte	0x3
	.byte	0x91
	.sleb128 -224
	.uleb128 0x28
	.4byte	.LASF602
	.byte	0x1
	.2byte	0xaeb
	.4byte	0x17d
	.byte	0x3
	.byte	0x91
	.sleb128 -244
	.uleb128 0x28
	.4byte	.LASF603
	.byte	0x1
	.2byte	0xaed
	.4byte	0x2228
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x28
	.4byte	.LASF604
	.byte	0x1
	.2byte	0xaef
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x27
	.ascii	"i\000"
	.byte	0x1
	.2byte	0xaf1
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.4byte	.LASF521
	.byte	0x1
	.2byte	0xaf3
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x28
	.4byte	.LASF515
	.byte	0x1
	.2byte	0xaf5
	.4byte	0x93d
	.byte	0x3
	.byte	0x91
	.sleb128 -292
	.uleb128 0x28
	.4byte	.LASF522
	.byte	0x1
	.2byte	0xaf7
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x28
	.4byte	.LASF605
	.byte	0x1
	.2byte	0xaf9
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x28
	.4byte	.LASF606
	.byte	0x1
	.2byte	0xafb
	.4byte	0x91a
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x28
	.4byte	.LASF607
	.byte	0x1
	.2byte	0xafd
	.4byte	0x23dc
	.byte	0x3
	.byte	0x91
	.sleb128 -296
	.byte	0
	.uleb128 0x20
	.4byte	.LASF608
	.byte	0x15
	.byte	0x30
	.4byte	0x3765
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x23
	.4byte	0x1b6
	.uleb128 0x20
	.4byte	.LASF609
	.byte	0x15
	.byte	0x34
	.4byte	0x377b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x23
	.4byte	0x1b6
	.uleb128 0x20
	.4byte	.LASF610
	.byte	0x15
	.byte	0x36
	.4byte	0x3791
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x23
	.4byte	0x1b6
	.uleb128 0x20
	.4byte	.LASF611
	.byte	0x15
	.byte	0x38
	.4byte	0x37a7
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x23
	.4byte	0x1b6
	.uleb128 0x30
	.4byte	.LASF614
	.byte	0xc
	.byte	0x64
	.4byte	0x7ba
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF612
	.byte	0x12
	.byte	0x33
	.4byte	0x37ca
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x23
	.4byte	0x1c6
	.uleb128 0x20
	.4byte	.LASF613
	.byte	0x12
	.byte	0x3f
	.4byte	0x37e0
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x23
	.4byte	0x94d
	.uleb128 0x31
	.4byte	.LASF615
	.byte	0x13
	.2byte	0x31e
	.4byte	0x10ef
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF616
	.byte	0x13
	.2byte	0x7bd
	.4byte	0x1dbc
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF617
	.byte	0x13
	.2byte	0x8ff
	.4byte	0x2014
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF618
	.byte	0x16
	.byte	0x78
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF619
	.byte	0x16
	.byte	0x98
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF620
	.byte	0x16
	.byte	0xb1
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF621
	.byte	0x16
	.byte	0xbd
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF622
	.byte	0x16
	.byte	0xc0
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF623
	.byte	0x16
	.byte	0xc3
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF624
	.byte	0x16
	.byte	0xc6
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF625
	.byte	0x16
	.byte	0xc9
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF626
	.byte	0x17
	.byte	0x1b
	.4byte	0x23e1
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF627
	.byte	0x1
	.byte	0x41
	.4byte	0x23e1
	.byte	0x5
	.byte	0x3
	.4byte	SQUARE_FOOTAGE_CONSTANT
	.uleb128 0x20
	.4byte	.LASF628
	.byte	0x1
	.byte	0x42
	.4byte	0x23e1
	.byte	0x5
	.byte	0x3
	.4byte	MINUTES_PER_HOUR
	.uleb128 0x20
	.4byte	.LASF629
	.byte	0x1
	.byte	0x43
	.4byte	0x23e1
	.byte	0x5
	.byte	0x3
	.4byte	percip_100000u
	.uleb128 0x30
	.4byte	.LASF614
	.byte	0xc
	.byte	0x64
	.4byte	0x7ba
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF615
	.byte	0x13
	.2byte	0x31e
	.4byte	0x10ef
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF616
	.byte	0x13
	.2byte	0x7bd
	.4byte	0x1dbc
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF617
	.byte	0x13
	.2byte	0x8ff
	.4byte	0x2014
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF618
	.byte	0x16
	.byte	0x78
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF619
	.byte	0x16
	.byte	0x98
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF620
	.byte	0x16
	.byte	0xb1
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF621
	.byte	0x16
	.byte	0xbd
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF622
	.byte	0x16
	.byte	0xc0
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF623
	.byte	0x16
	.byte	0xc3
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF624
	.byte	0x16
	.byte	0xc6
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF625
	.byte	0x16
	.byte	0xc9
	.4byte	0x1a0
	.byte	0x1
	.byte	0x1
	.uleb128 0x32
	.4byte	.LASF630
	.byte	0x1
	.byte	0x3e
	.4byte	0xad
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_BUDGET_using_et_calculate_budget
	.uleb128 0x30
	.4byte	.LASF626
	.byte	0x17
	.byte	0x1b
	.4byte	0x23e1
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x6
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI17
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI20
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI23
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI26
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI31
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI40
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI43
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI46
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI47
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI51
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI54
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI57
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI60
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI62
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI63
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI65
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI66
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI68
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI69
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI70
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI73
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI74
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB23
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI76
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI77
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB24
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI79
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI80
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI81
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB25
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI83
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI84
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI85
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7b
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB26
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI87
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI88
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI89
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB27
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI91
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI92
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI93
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7b
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB28
	.4byte	.LCFI95
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI95
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI96
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB29
	.4byte	.LCFI98
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI98
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI99
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB30
	.4byte	.LCFI101
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI101
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI102
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB31
	.4byte	.LCFI104
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI104
	.4byte	.LCFI105
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI105
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB32
	.4byte	.LCFI107
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI107
	.4byte	.LCFI108
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI108
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB33
	.4byte	.LCFI110
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI110
	.4byte	.LCFI111
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI111
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI112
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x124
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF379:
	.ascii	"pump_energized_irri\000"
.LASF315:
	.ascii	"system_master_5_sec_avgs_next_index\000"
.LASF252:
	.ascii	"mlb_measured_during_mvor_closed_gpm\000"
.LASF88:
	.ascii	"one_or_more_in_list_for_walk_thru\000"
.LASF107:
	.ascii	"ptail\000"
.LASF603:
	.ascii	"lpoc\000"
.LASF220:
	.ascii	"meter_read_date\000"
.LASF614:
	.ascii	"station_info_list_hdr\000"
.LASF250:
	.ascii	"mlb_measured_during_irrigation_gpm\000"
.LASF124:
	.ascii	"et_factor_100u\000"
.LASF368:
	.ascii	"used_idle_gallons\000"
.LASF176:
	.ascii	"two_wire_poc_decoder_inoperative\000"
.LASF518:
	.ascii	"rv_sys_usage\000"
.LASF502:
	.ascii	"HUNDRED\000"
.LASF336:
	.ascii	"flow_check_derate_table_max_stations_ON\000"
.LASF115:
	.ascii	"MIST_DLINK_TYPE\000"
.LASF446:
	.ascii	"need_to_distribute_twci\000"
.LASF105:
	.ascii	"SYSTEM_BIT_FIELD_STRUCT\000"
.LASF289:
	.ascii	"ufim_highest_priority_pump_waiting\000"
.LASF589:
	.ascii	"lstation\000"
.LASF232:
	.ascii	"STATION_PRESERVES_BIT_FIELD\000"
.LASF365:
	.ascii	"used_total_gallons\000"
.LASF577:
	.ascii	"pdtcs\000"
.LASF160:
	.ascii	"no_water_by_calendar_prevented\000"
.LASF204:
	.ascii	"manual_program_gallons_fl\000"
.LASF396:
	.ascii	"fm_latest_5_second_pulse_count_foal\000"
.LASF540:
	.ascii	"bp_idx\000"
.LASF298:
	.ascii	"ufim_list_contains_some_RRE_to_setex_that_are_not_O"
	.ascii	"N_b\000"
.LASF83:
	.ascii	"MVOR_in_effect_opened\000"
.LASF132:
	.ascii	"GID_manual_program_A\000"
.LASF133:
	.ascii	"GID_manual_program_B\000"
.LASF546:
	.ascii	"nm_handle_budget_period_updates\000"
.LASF610:
	.ascii	"GuiFont_DecimalChar\000"
.LASF560:
	.ascii	"is_start_time\000"
.LASF259:
	.ascii	"no_longer_used_end_dt\000"
.LASF60:
	.ascii	"directions_honor_controller_set_to_OFF\000"
.LASF82:
	.ascii	"stable_flow\000"
.LASF516:
	.ascii	"lprr\000"
.LASF96:
	.ascii	"ufim_one_or_more_in_list_for_mobile\000"
.LASF127:
	.ascii	"expected_flow_rate_gpm\000"
.LASF498:
	.ascii	"days_to_end_of_month\000"
.LASF322:
	.ascii	"MVOR_remaining_seconds\000"
.LASF592:
	.ascii	"BUDGET_total_square_footage\000"
.LASF400:
	.ascii	"delivered_5_second_average_gpm_irri\000"
.LASF80:
	.ascii	"flow_checking_enabled_by_user_setting\000"
.LASF432:
	.ascii	"GID_on_at_a_time\000"
.LASF201:
	.ascii	"expansion_u16\000"
.LASF378:
	.ascii	"master_valve_energized_irri\000"
.LASF585:
	.ascii	"BUDGET_reset_budget_values\000"
.LASF156:
	.ascii	"flow_low\000"
.LASF613:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF15:
	.ascii	"BOOL_32\000"
.LASF123:
	.ascii	"uses_et\000"
.LASF371:
	.ascii	"used_manual_gallons\000"
.LASF200:
	.ascii	"pi_moisture_balance_percentage_after_schedule_compl"
	.ascii	"etes_100u\000"
.LASF543:
	.ascii	"nm_handle_budget_period_rollback\000"
.LASF600:
	.ascii	"lbudget_per_month\000"
.LASF279:
	.ascii	"unused_0\000"
.LASF465:
	.ascii	"month\000"
.LASF235:
	.ascii	"skip_irrigation_till_due_to_manual_NOW_time\000"
.LASF44:
	.ascii	"flow_check_group\000"
.LASF581:
	.ascii	"safety_count\000"
.LASF563:
	.ascii	"sys_has_poc_flag\000"
.LASF195:
	.ascii	"pi_flow_check_share_of_hi_limit_gpm\000"
.LASF142:
	.ascii	"pending_first_to_send\000"
.LASF495:
	.ascii	"flow_rate\000"
.LASF190:
	.ascii	"pi_watersense_requested_seconds\000"
.LASF369:
	.ascii	"used_programmed_gallons\000"
.LASF566:
	.ascii	"flag_no_budget\000"
.LASF419:
	.ascii	"list_support_foal_stations_ON\000"
.LASF151:
	.ascii	"current_none\000"
.LASF141:
	.ascii	"first_to_send\000"
.LASF504:
	.ascii	"ptr_group\000"
.LASF236:
	.ascii	"skip_irrigation_till_due_to_calendar_NOW_time\000"
.LASF417:
	.ascii	"POC_BB_STRUCT\000"
.LASF541:
	.ascii	"saved_budget\000"
.LASF7:
	.ascii	"INT_16\000"
.LASF300:
	.ascii	"ufim_list_contains_some_non_pump_to_setex_that_are_"
	.ascii	"not_ON_b\000"
.LASF523:
	.ascii	"nm_calc_POC_ratios\000"
.LASF202:
	.ascii	"STATION_HISTORY_RECORD\000"
.LASF66:
	.ascii	"BIG_BIT_FIELD_FOR_ILC_STRUCT\000"
.LASF512:
	.ascii	"budget_idx\000"
.LASF595:
	.ascii	"lgallons_per_inch_for_square_footage\000"
.LASF424:
	.ascii	"requested_irrigation_seconds_ul\000"
.LASF89:
	.ascii	"one_or_more_in_list_for_test\000"
.LASF101:
	.ascii	"number_of_flow_meters_in_this_sys\000"
.LASF601:
	.ascii	"lbds_ptr\000"
.LASF374:
	.ascii	"used_mobile_gallons\000"
.LASF67:
	.ascii	"unused_four_bits\000"
.LASF521:
	.ascii	"poc_count\000"
.LASF137:
	.ascii	"STATION_GROUP_STRUCT\000"
.LASF296:
	.ascii	"ufim_list_contains_some_to_setex_that_are_not_ON_b\000"
.LASF484:
	.ascii	"getNumManualStarts\000"
.LASF429:
	.ascii	"expected_flow_rate_gpm_u16\000"
.LASF74:
	.ascii	"system_level_valves_are_ON_and_actively_checking\000"
.LASF539:
	.ascii	"nm_handle_budget_value_rollback\000"
.LASF599:
	.ascii	"percent_et_changed\000"
.LASF33:
	.ascii	"current_percentage_of_max\000"
.LASF630:
	.ascii	"g_BUDGET_using_et_calculate_budget\000"
.LASF11:
	.ascii	"INT_32\000"
.LASF350:
	.ascii	"expansion\000"
.LASF418:
	.ascii	"list_support_foal_all_irrigation\000"
.LASF26:
	.ascii	"__dayofweek\000"
.LASF304:
	.ascii	"ufim_number_ON_during_test\000"
.LASF422:
	.ascii	"station_preserves_index\000"
.LASF16:
	.ascii	"BITFIELD_BOOL\000"
.LASF522:
	.ascii	"sys_usage\000"
.LASF476:
	.ascii	"getNumStarts\000"
.LASF130:
	.ascii	"GID_station_group\000"
.LASF415:
	.ascii	"BY_POC_RECORD\000"
.LASF612:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF262:
	.ascii	"rre_gallons_fl\000"
.LASF594:
	.ascii	"ltotal_square_footage\000"
.LASF103:
	.ascii	"whole_thing\000"
.LASF440:
	.ascii	"flow_recording_group_made_during_this_turn_OFF_loop"
	.ascii	"\000"
.LASF244:
	.ascii	"verify_string_pre\000"
.LASF206:
	.ascii	"walk_thru_gallons_fl\000"
.LASF164:
	.ascii	"rain_as_negative_time_reduced_irrigation\000"
.LASF352:
	.ascii	"poc_gid\000"
.LASF166:
	.ascii	"switch_rain_prevented_or_curtailed\000"
.LASF270:
	.ascii	"non_controller_gallons_fl\000"
.LASF305:
	.ascii	"ufim_stations_ON_with_the_pump_b\000"
.LASF462:
	.ascii	"psbrr\000"
.LASF260:
	.ascii	"rainfall_raw_total_100u\000"
.LASF494:
	.ascii	"pilc\000"
.LASF586:
	.ascii	"BUDGET_calculate_square_footage\000"
.LASF545:
	.ascii	"newday\000"
.LASF473:
	.ascii	"pssfb\000"
.LASF571:
	.ascii	"num_groups\000"
.LASF221:
	.ascii	"mode\000"
.LASF52:
	.ascii	"at_some_point_should_check_flow\000"
.LASF290:
	.ascii	"ufim_highest_priority_non_pump_waiting\000"
.LASF154:
	.ascii	"watersense_min_cycle_eliminated_a_cycle\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF112:
	.ascii	"pPrev\000"
.LASF420:
	.ascii	"list_support_foal_action_needed\000"
.LASF167:
	.ascii	"switch_freeze_prevented_or_curtailed\000"
.LASF61:
	.ascii	"directions_honor_MANUAL_NOW\000"
.LASF441:
	.ascii	"stations_ON_by_controller\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF470:
	.ascii	"ptr_poc\000"
.LASF203:
	.ascii	"programmed_irrigation_gallons_irrigated_fl\000"
.LASF273:
	.ascii	"end_date\000"
.LASF447:
	.ascii	"twccm\000"
.LASF134:
	.ascii	"float\000"
.LASF463:
	.ascii	"nm_reset_meter_read_dates\000"
.LASF561:
	.ascii	"numsys\000"
.LASF46:
	.ascii	"responds_to_rain\000"
.LASF210:
	.ascii	"mobile_seconds_us\000"
.LASF317:
	.ascii	"accumulated_gallons_for_accumulators_foal\000"
.LASF467:
	.ascii	"delta\000"
.LASF433:
	.ascii	"stop_datetime_d\000"
.LASF9:
	.ascii	"UNS_32\000"
.LASF488:
	.ascii	"mpstart\000"
.LASF428:
	.ascii	"stop_datetime_t\000"
.LASF257:
	.ascii	"system_gid\000"
.LASF353:
	.ascii	"start_time\000"
.LASF174:
	.ascii	"rip_valid_to_show\000"
.LASF455:
	.ascii	"lmonth_1_12\000"
.LASF395:
	.ascii	"fm_accumulated_ms_foal\000"
.LASF333:
	.ascii	"flow_check_required_cell_iteration\000"
.LASF165:
	.ascii	"rain_table_R_M_or_Poll_prevented_or_curtailed\000"
.LASF183:
	.ascii	"pi_gallons_irrigated_fl\000"
.LASF597:
	.ascii	"CONVERSION_RATE_SQUARE_FEET_PER_ACRE\000"
.LASF205:
	.ascii	"manual_gallons_fl\000"
.LASF129:
	.ascii	"no_water_days\000"
.LASF285:
	.ascii	"system_master_number_of_valves_ON\000"
.LASF569:
	.ascii	"reduction\000"
.LASF312:
	.ascii	"inhibit_next_turn_ON_remaining_seconds\000"
.LASF390:
	.ascii	"fm_accumulated_ms_irri\000"
.LASF375:
	.ascii	"on_at_start_time\000"
.LASF551:
	.ascii	"nm_BUDGET_calc_time_adjustment\000"
.LASF335:
	.ascii	"flow_check_derate_table_gpm_slot_size\000"
.LASF537:
	.ascii	"predicted\000"
.LASF620:
	.ascii	"poc_report_completed_records_recursive_MUTEX\000"
.LASF619:
	.ascii	"list_foal_irri_recursive_MUTEX\000"
.LASF344:
	.ascii	"system_rcvd_most_recent_number_of_valves_ON\000"
.LASF58:
	.ascii	"xfer_to_irri_machines\000"
.LASF256:
	.ascii	"SYSTEM_MAINLINE_BREAK_RECORD\000"
.LASF196:
	.ascii	"pi_flow_check_share_of_lo_limit_gpm\000"
.LASF505:
	.ascii	"budget_data\000"
.LASF261:
	.ascii	"rre_seconds\000"
.LASF372:
	.ascii	"used_walkthru_gallons\000"
.LASF359:
	.ascii	"gallons_during_mvor\000"
.LASF556:
	.ascii	"nm_BUDGET_predicted_volume\000"
.LASF313:
	.ascii	"system_master_5_second_averages_ring\000"
.LASF309:
	.ascii	"ufim_highest_non_pump_reason_in_list_available_to_t"
	.ascii	"urn_ON\000"
.LASF568:
	.ascii	"budget_entry_option\000"
.LASF329:
	.ascii	"delivered_mlb_record\000"
.LASF385:
	.ascii	"no_current_pump\000"
.LASF12:
	.ascii	"UNS_64\000"
.LASF529:
	.ascii	"t_orig\000"
.LASF189:
	.ascii	"pi_total_requested_minutes_us_10u\000"
.LASF591:
	.ascii	"square_footage_calculation_occured\000"
.LASF547:
	.ascii	"nm_BUDGET_increment_off_at_start_time\000"
.LASF524:
	.ascii	"psystem\000"
.LASF113:
	.ascii	"pNext\000"
.LASF481:
	.ascii	"et_ratio\000"
.LASF29:
	.ascii	"portTickType\000"
.LASF549:
	.ascii	"nm_BUDGET_increment_on_at_start_time\000"
.LASF534:
	.ascii	"nm_handle_sending_budget_record\000"
.LASF558:
	.ascii	"BUDGET_calculate_ratios\000"
.LASF108:
	.ascii	"count\000"
.LASF85:
	.ascii	"one_or_more_in_list_for_programmed_irrigation\000"
.LASF490:
	.ascii	"end_day\000"
.LASF131:
	.ascii	"GID_irrigation_system\000"
.LASF77:
	.ascii	"system_level_valves_are_ON_and_waiting_to_acquire_e"
	.ascii	"xpected\000"
.LASF28:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF519:
	.ascii	"rpoc\000"
.LASF150:
	.ascii	"current_short\000"
.LASF307:
	.ascii	"ufim_highest_reason_in_list_available_to_turn_ON\000"
.LASF520:
	.ascii	"gid_poc\000"
.LASF243:
	.ascii	"STATION_PRESERVES_RECORD\000"
.LASF475:
	.ascii	"dtcs\000"
.LASF295:
	.ascii	"ufim_one_ON_to_set_expected_b\000"
.LASF55:
	.ascii	"rre_on_sxr_to_turn_OFF\000"
.LASF277:
	.ascii	"closing_record_for_the_period\000"
.LASF86:
	.ascii	"one_or_more_in_list_for_manual_program\000"
.LASF136:
	.ascii	"MANUAL_PROGRAMS_GROUP_STRUCT\000"
.LASF163:
	.ascii	"rain_as_negative_time_prevented_irrigation\000"
.LASF593:
	.ascii	"BUDGET_calculate_gallons_per_inch_for_square_footag"
	.ascii	"e_of_mainline\000"
.LASF616:
	.ascii	"poc_preserves\000"
.LASF30:
	.ascii	"xQueueHandle\000"
.LASF550:
	.ascii	"calc_percent_adjust\000"
.LASF609:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF34:
	.ascii	"TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT\000"
.LASF306:
	.ascii	"ufim_stations_ON_without_the_pump_b\000"
.LASF302:
	.ascii	"ufim_based_on_reason_in_list_at_least_one_valve_is_"
	.ascii	"ON_that_wants_to_check_flow\000"
.LASF386:
	.ascii	"POC_BIT_FIELD_STRUCT\000"
.LASF187:
	.ascii	"pi_first_cycle_start_date\000"
.LASF175:
	.ascii	"two_wire_station_decoder_inoperative\000"
.LASF399:
	.ascii	"latest_5_second_average_gpm_foal\000"
.LASF349:
	.ascii	"reason_in_running_list\000"
.LASF299:
	.ascii	"ufim_list_contains_some_pump_to_setex_that_are_not_"
	.ascii	"ON_b\000"
.LASF294:
	.ascii	"ufim_highest_reason_of_OFF_valve_to_set_expected\000"
.LASF218:
	.ascii	"meter_read_time\000"
.LASF552:
	.ascii	"sys_gid\000"
.LASF208:
	.ascii	"mobile_gallons_fl\000"
.LASF559:
	.ascii	"ptoday_ptr\000"
.LASF345:
	.ascii	"mvor_stop_date\000"
.LASF606:
	.ascii	"new_calculated_budget\000"
.LASF435:
	.ascii	"moisture_sensor_decoder_serial_number\000"
.LASF442:
	.ascii	"timer_rain_switch\000"
.LASF544:
	.ascii	"psys\000"
.LASF337:
	.ascii	"flow_check_derate_table_number_of_gpm_slots\000"
.LASF554:
	.ascii	"saved_gid\000"
.LASF104:
	.ascii	"overall_size\000"
.LASF464:
	.ascii	"ptr_sys\000"
.LASF266:
	.ascii	"manual_program_seconds\000"
.LASF31:
	.ascii	"xSemaphoreHandle\000"
.LASF91:
	.ascii	"ufim_one_or_more_in_list_for_programmed_irrigation\000"
.LASF373:
	.ascii	"used_test_gallons\000"
.LASF323:
	.ascii	"transition_timer_all_stations_are_OFF\000"
.LASF286:
	.ascii	"ufim_what_are_we_turning_on_b\000"
.LASF172:
	.ascii	"mow_day\000"
.LASF148:
	.ascii	"hit_stop_time\000"
.LASF94:
	.ascii	"ufim_one_or_more_in_list_for_walk_thru\000"
.LASF59:
	.ascii	"directions_honor_RAIN_TABLE\000"
.LASF469:
	.ascii	"nm_reset_budget_values\000"
.LASF343:
	.ascii	"flow_check_lo_limit\000"
.LASF562:
	.ascii	"num_pocs\000"
.LASF43:
	.ascii	"flow_check_lo_action\000"
.LASF75:
	.ascii	"system_level_valves_are_ON_and_waiting_to_update_de"
	.ascii	"rate_table\000"
.LASF574:
	.ascii	"value\000"
.LASF140:
	.ascii	"first_to_display\000"
.LASF487:
	.ascii	"pman\000"
.LASF456:
	.ascii	"lyear\000"
.LASF22:
	.ascii	"__year\000"
.LASF466:
	.ascii	"year\000"
.LASF624:
	.ascii	"list_system_recursive_MUTEX\000"
.LASF246:
	.ascii	"there_was_a_MLB_during_irrigation\000"
.LASF451:
	.ascii	"record_date\000"
.LASF564:
	.ascii	"BUDGET_in_effect_for_any_system\000"
.LASF412:
	.ascii	"this_pocs_system_preserves_ptr\000"
.LASF404:
	.ascii	"pump_current_for_distribution_in_the_token_ma\000"
.LASF39:
	.ascii	"w_uses_the_pump\000"
.LASF452:
	.ascii	"BUDGET_REPORT_RECORD\000"
.LASF240:
	.ascii	"rain_minutes_10u\000"
.LASF227:
	.ascii	"skip_irrigation_due_to_calendar_NOW\000"
.LASF416:
	.ascii	"perform_a_full_resync\000"
.LASF99:
	.ascii	"delivered_MVOR_in_effect_opened\000"
.LASF485:
	.ascii	"today\000"
.LASF93:
	.ascii	"ufim_one_or_more_in_list_for_manual\000"
.LASF402:
	.ascii	"pump_last_measured_current_from_the_tpmicro_ma\000"
.LASF427:
	.ascii	"soak_seconds_ul\000"
.LASF230:
	.ascii	"station_history_rip_needs_to_be_saved\000"
.LASF143:
	.ascii	"pending_first_to_send_in_use\000"
.LASF407:
	.ascii	"POC_DECODER_OR_TERMINAL_WORKING_STRUCT\000"
.LASF588:
	.ascii	"lsystem\000"
.LASF439:
	.ascii	"list_of_foal_stations_with_action_needed\000"
.LASF72:
	.ascii	"system_level_valves_are_ON_but_will_not_be_checking"
	.ascii	"_flow\000"
.LASF542:
	.ascii	"nm_handle_budget_value_updates\000"
.LASF454:
	.ascii	"lday_of_month\000"
.LASF525:
	.ascii	"Rpoc\000"
.LASF191:
	.ascii	"pi_rain_at_start_time_before_working_down__minutes_"
	.ascii	"10u\000"
.LASF122:
	.ascii	"total_run_minutes_10u\000"
.LASF106:
	.ascii	"phead\000"
.LASF508:
	.ascii	"volume_scheduled\000"
.LASF251:
	.ascii	"mlb_limit_during_irrigation_gpm\000"
.LASF139:
	.ascii	"next_available\000"
.LASF506:
	.ascii	"times_on\000"
.LASF596:
	.ascii	"CONVERSION_RATE_GALLONS_PER_ACRE\000"
.LASF553:
	.ascii	"dt_flag\000"
.LASF157:
	.ascii	"flow_high\000"
.LASF411:
	.ascii	"usage\000"
.LASF482:
	.ascii	"unif\000"
.LASF264:
	.ascii	"walk_thru_seconds\000"
.LASF530:
	.ascii	"system_ptr\000"
.LASF575:
	.ascii	"flag\000"
.LASF608:
	.ascii	"GuiFont_LanguageActive\000"
.LASF381:
	.ascii	"there_is_flow_meter_count_data_to_send_to_the_maste"
	.ascii	"r\000"
.LASF45:
	.ascii	"responds_to_wind\000"
.LASF185:
	.ascii	"GID_irrigation_schedule\000"
.LASF388:
	.ascii	"master_valve_type\000"
.LASF69:
	.ascii	"mv_open_for_irrigation\000"
.LASF326:
	.ascii	"timer_MLB_just_stopped_irrigating_blockout_seconds_"
	.ascii	"remaining\000"
.LASF449:
	.ascii	"sbrr\000"
.LASF366:
	.ascii	"used_irrigation_gallons\000"
.LASF194:
	.ascii	"pi_flow_check_share_of_actual_gpm\000"
.LASF51:
	.ascii	"rre_station_is_paused\000"
.LASF479:
	.ascii	"days_on\000"
.LASF222:
	.ascii	"BUDGET_DETAILS_STRUCT\000"
.LASF258:
	.ascii	"start_dt\000"
.LASF468:
	.ascii	"lchange_bitfield_to_set\000"
.LASF40:
	.ascii	"w_did_not_irrigate_last_time\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF3:
	.ascii	"signed char\000"
.LASF118:
	.ascii	"box_index_0\000"
.LASF234:
	.ascii	"station_report_data_rip\000"
.LASF211:
	.ascii	"test_seconds_us\000"
.LASF153:
	.ascii	"current_high\000"
.LASF18:
	.ascii	"DATE_TIME\000"
.LASF79:
	.ascii	"flow_checking_enabled_and_allowed\000"
.LASF579:
	.ascii	"dt_budget0\000"
.LASF580:
	.ascii	"dt_budget1\000"
.LASF97:
	.ascii	"due_to_edit_resync_to_the_system_list\000"
.LASF192:
	.ascii	"pi_rain_at_start_time_after_working_down__minutes_1"
	.ascii	"0u\000"
.LASF528:
	.ascii	"calc_time_adjustment_low_level\000"
.LASF625:
	.ascii	"list_poc_recursive_MUTEX\000"
.LASF510:
	.ascii	"days_in_month\000"
.LASF587:
	.ascii	"system_index\000"
.LASF426:
	.ascii	"soak_seconds_remaining_ul\000"
.LASF36:
	.ascii	"station_priority\000"
.LASF444:
	.ascii	"wind_paused\000"
.LASF458:
	.ascii	"update_running_dtcs\000"
.LASF125:
	.ascii	"uniformity\000"
.LASF536:
	.ascii	"report\000"
.LASF278:
	.ascii	"SYSTEM_BUDGET_REPORT_RECORD\000"
.LASF632:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/budg"
	.ascii	"ets/budgets.c\000"
.LASF197:
	.ascii	"station_number\000"
.LASF73:
	.ascii	"system_level_valves_are_ON_and_waiting_to_check_flo"
	.ascii	"w\000"
.LASF198:
	.ascii	"pi_number_of_repeats\000"
.LASF500:
	.ascii	"right_now\000"
.LASF274:
	.ascii	"predicted_use_to_end_of_period\000"
.LASF330:
	.ascii	"derate_table_10u\000"
.LASF347:
	.ascii	"delivered_MVOR_remaining_seconds\000"
.LASF513:
	.ascii	"nm_BUDGET_get_poc_usage\000"
.LASF357:
	.ascii	"gallons_during_idle\000"
.LASF65:
	.ascii	"directions_honor_WIND_PAUSE\000"
.LASF265:
	.ascii	"manual_seconds\000"
.LASF223:
	.ascii	"flow_check_station_cycles_count\000"
.LASF188:
	.ascii	"pi_last_cycle_end_date\000"
.LASF460:
	.ascii	"pp_idx\000"
.LASF397:
	.ascii	"fm_delta_between_last_two_fm_pulses_4khz_foal\000"
.LASF217:
	.ascii	"in_use\000"
.LASF186:
	.ascii	"record_start_date\000"
.LASF626:
	.ascii	"__float32_infinity\000"
.LASF168:
	.ascii	"wind_conditions_prevented_or_curtailed\000"
.LASF245:
	.ascii	"STATION_PRESERVES_STRUCT\000"
.LASF17:
	.ascii	"long int\000"
.LASF434:
	.ascii	"station_number_0_u8\000"
.LASF231:
	.ascii	"distribute_last_measured_current_ma\000"
.LASF471:
	.ascii	"pbpr\000"
.LASF392:
	.ascii	"fm_delta_between_last_two_fm_pulses_4khz_irri\000"
.LASF247:
	.ascii	"there_was_a_MLB_during_mvor_closed\000"
.LASF409:
	.ascii	"poc_type__file_type\000"
.LASF383:
	.ascii	"shorted_pump\000"
.LASF241:
	.ascii	"spbf\000"
.LASF342:
	.ascii	"flow_check_hi_limit\000"
.LASF338:
	.ascii	"flow_check_ranges_gpm\000"
.LASF53:
	.ascii	"at_some_point_flow_was_checked\000"
.LASF572:
	.ascii	"limit\000"
.LASF242:
	.ascii	"last_measured_current_ma\000"
.LASF178:
	.ascii	"STATION_HISTORY_BITFIELD\000"
.LASF382:
	.ascii	"shorted_mv\000"
.LASF555:
	.ascii	"saved\000"
.LASF414:
	.ascii	"msgs_to_tpmicro_with_no_valves_ON\000"
.LASF212:
	.ascii	"walk_thru_seconds_us\000"
.LASF391:
	.ascii	"fm_latest_5_second_pulse_count_irri\000"
.LASF578:
	.ascii	"dt_bug_check\000"
.LASF408:
	.ascii	"box_index\000"
.LASF477:
	.ascii	"getVolume\000"
.LASF507:
	.ascii	"volume\000"
.LASF535:
	.ascii	"pbrr\000"
.LASF254:
	.ascii	"mlb_measured_during_all_other_times_gpm\000"
.LASF37:
	.ascii	"w_reason_in_list\000"
.LASF380:
	.ascii	"send_mv_pump_milli_amp_measurements_to_the_master\000"
.LASF436:
	.ascii	"IRRIGATION_LIST_COMPONENT\000"
.LASF111:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF531:
	.ascii	"pss_ptr\000"
.LASF98:
	.ascii	"accounted_for\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF225:
	.ascii	"i_status\000"
.LASF526:
	.ascii	"usedpoc\000"
.LASF423:
	.ascii	"remaining_seconds_ON\000"
.LASF62:
	.ascii	"directions_honor_CALENDAR_NOW\000"
.LASF135:
	.ascii	"STATION_STRUCT_for_budgets\000"
.LASF483:
	.ascii	"et_factor\000"
.LASF117:
	.ascii	"STATION_STRUCT\000"
.LASF138:
	.ascii	"original_allocation\000"
.LASF316:
	.ascii	"system_rcvd_most_recent_token_5_second_average\000"
.LASF25:
	.ascii	"__seconds\000"
.LASF398:
	.ascii	"fm_seconds_since_last_pulse_foal\000"
.LASF532:
	.ascii	"rv_t\000"
.LASF515:
	.ascii	"poc_usage\000"
.LASF623:
	.ascii	"poc_preserves_recursive_MUTEX\000"
.LASF461:
	.ascii	"pbpbr\000"
.LASF147:
	.ascii	"controller_turned_off\000"
.LASF92:
	.ascii	"ufim_one_or_more_in_list_for_manual_program\000"
.LASF517:
	.ascii	"idx_poc\000"
.LASF340:
	.ascii	"flow_check_tolerance_minus_gpm\000"
.LASF1:
	.ascii	"char\000"
.LASF207:
	.ascii	"test_gallons_fl\000"
.LASF268:
	.ascii	"programmed_irrigation_gallons_fl\000"
.LASF54:
	.ascii	"rre_on_sxr_to_pause\000"
.LASF480:
	.ascii	"et_hist\000"
.LASF145:
	.ascii	"FLOW_RECORDING_CONTROL_STRUCT\000"
.LASF360:
	.ascii	"seconds_of_flow_during_mvor\000"
.LASF393:
	.ascii	"fm_seconds_since_last_pulse_irri\000"
.LASF325:
	.ascii	"timer_MVJO_flow_checking_blockout_seconds_remaining"
	.ascii	"\000"
.LASF233:
	.ascii	"station_history_rip\000"
.LASF249:
	.ascii	"dummy_byte\000"
.LASF499:
	.ascii	"getScheduledVp\000"
.LASF169:
	.ascii	"mois_cause_cycle_skip\000"
.LASF511:
	.ascii	"endday\000"
.LASF618:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF448:
	.ascii	"FOAL_IRRI_BB_STRUCT\000"
.LASF403:
	.ascii	"mv_current_for_distribution_in_the_token_ma\000"
.LASF4:
	.ascii	"UNS_8\000"
.LASF119:
	.ascii	"station_number_0\000"
.LASF425:
	.ascii	"cycle_seconds_ul\000"
.LASF81:
	.ascii	"there_are_pocs_without_flow_meters\000"
.LASF24:
	.ascii	"__minutes\000"
.LASF604:
	.ascii	"monthy_budget_for_system_calculated\000"
.LASF213:
	.ascii	"manual_seconds_us\000"
.LASF548:
	.ascii	"gid_system\000"
.LASF297:
	.ascii	"ufim_one_RRE_ON_to_set_expected_b\000"
.LASF319:
	.ascii	"stability_avgs_index_of_last_computed\000"
.LASF355:
	.ascii	"gallons_total\000"
.LASF453:
	.ascii	"running_dtcs\000"
.LASF20:
	.ascii	"__day\000"
.LASF474:
	.ascii	"pgroup_ptr\000"
.LASF63:
	.ascii	"directions_honor_RAIN_SWITCH\000"
.LASF237:
	.ascii	"skip_irrigation_till_due_to_manual_NOW_date\000"
.LASF570:
	.ascii	"reduction_flag\000"
.LASF583:
	.ascii	"nm_BUDGET_get_used\000"
.LASF152:
	.ascii	"current_low\000"
.LASF128:
	.ascii	"precip_rate\000"
.LASF430:
	.ascii	"line_fill_seconds\000"
.LASF38:
	.ascii	"w_to_set_expected\000"
.LASF293:
	.ascii	"ufim_list_contains_waiting_non_pump_valves_b\000"
.LASF281:
	.ascii	"BY_SYSTEM_BUDGET_RECORD\000"
.LASF356:
	.ascii	"seconds_of_flow_total\000"
.LASF615:
	.ascii	"station_preserves\000"
.LASF95:
	.ascii	"ufim_one_or_more_in_list_for_test\000"
.LASF239:
	.ascii	"left_over_irrigation_seconds\000"
.LASF276:
	.ascii	"ratio\000"
.LASF248:
	.ascii	"there_was_a_MLB_during_all_other_times\000"
.LASF215:
	.ascii	"STATION_REPORT_DATA_RECORD\000"
.LASF19:
	.ascii	"date_time\000"
.LASF238:
	.ascii	"skip_irrigation_till_due_to_calendar_NOW_date\000"
.LASF180:
	.ascii	"pi_first_cycle_start_time\000"
.LASF219:
	.ascii	"number_of_annual_periods\000"
.LASF501:
	.ascii	"use_limits\000"
.LASF339:
	.ascii	"flow_check_tolerance_plus_gpm\000"
.LASF406:
	.ascii	"pump_current_as_delivered_from_the_master_ma\000"
.LASF363:
	.ascii	"double\000"
.LASF149:
	.ascii	"stop_key_pressed\000"
.LASF308:
	.ascii	"ufim_highest_pump_reason_in_list_available_to_turn_"
	.ascii	"ON\000"
.LASF224:
	.ascii	"flow_status\000"
.LASF292:
	.ascii	"ufim_list_contains_waiting_pump_valves_b\000"
.LASF346:
	.ascii	"mvor_stop_time\000"
.LASF161:
	.ascii	"mlb_prevented_or_curtailed\000"
.LASF116:
	.ascii	"POC_GROUP_STRUCT\000"
.LASF64:
	.ascii	"directions_honor_FREEZE_SWITCH\000"
.LASF503:
	.ascii	"ptr_manprog\000"
.LASF209:
	.ascii	"programmed_irrigation_seconds_irrigated_ul\000"
.LASF582:
	.ascii	"group\000"
.LASF573:
	.ascii	"BUDGET_handle_alerts_at_start_time\000"
.LASF565:
	.ascii	"BUDGET_handle_alerts_at_midnight\000"
.LASF310:
	.ascii	"ufim_flow_check_group_count_of_ON\000"
.LASF120:
	.ascii	"physically_available\000"
.LASF590:
	.ascii	"square_footage\000"
.LASF486:
	.ascii	"pbds\000"
.LASF621:
	.ascii	"station_preserves_recursive_MUTEX\000"
.LASF567:
	.ascii	"predicted_percent_for_alert\000"
.LASF49:
	.ascii	"flow_check_when_possible_based_on_reason_in_list\000"
.LASF288:
	.ascii	"ufim_one_ON_from_the_problem_list_b\000"
.LASF162:
	.ascii	"mvor_closed_prevented_or_curtailed\000"
.LASF384:
	.ascii	"no_current_mv\000"
.LASF68:
	.ascii	"master_valve_has_at_least_one_normally_closed\000"
.LASF173:
	.ascii	"two_wire_cable_problem\000"
.LASF514:
	.ascii	"gid_sys\000"
.LASF602:
	.ascii	"ldtcs_ptr\000"
.LASF159:
	.ascii	"no_water_by_manual_prevented\000"
.LASF170:
	.ascii	"mois_max_water_day\000"
.LASF387:
	.ascii	"decoder_serial_number\000"
.LASF607:
	.ascii	"ONE_HUNDRED\000"
.LASF611:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF182:
	.ascii	"pi_seconds_irrigated_ul\000"
.LASF413:
	.ascii	"bypass_activate\000"
.LASF492:
	.ascii	"system_GID\000"
.LASF283:
	.ascii	"ufim_valves_in_the_list_for_this_system\000"
.LASF328:
	.ascii	"latest_mlb_record\000"
.LASF76:
	.ascii	"system_level_valves_are_ON_and_has_updated_the_dera"
	.ascii	"te_table\000"
.LASF496:
	.ascii	"ptr_station\000"
.LASF47:
	.ascii	"station_is_ON\000"
.LASF367:
	.ascii	"used_mvor_gallons\000"
.LASF497:
	.ascii	"percent_adjust_days\000"
.LASF13:
	.ascii	"long long unsigned int\000"
.LASF109:
	.ascii	"offset\000"
.LASF377:
	.ascii	"BY_POC_BUDGET_RECORD\000"
.LASF321:
	.ascii	"last_off__reason_in_list\000"
.LASF226:
	.ascii	"skip_irrigation_due_to_manual_NOW\000"
.LASF42:
	.ascii	"flow_check_hi_action\000"
.LASF121:
	.ascii	"in_use_bool\000"
.LASF491:
	.ascii	"getIrrigationListVp\000"
.LASF171:
	.ascii	"poc_short_cancelled_irrigation\000"
.LASF348:
	.ascii	"budget\000"
.LASF320:
	.ascii	"last_off__station_number_0\000"
.LASF291:
	.ascii	"ufim_list_contains_waiting_programmed_irrigation_b\000"
.LASF327:
	.ascii	"frcs\000"
.LASF598:
	.ascii	"BUDGET_calc_POC_monthly_budget_using_et\000"
.LASF457:
	.ascii	"ldow\000"
.LASF70:
	.ascii	"pump_activate_for_irrigation\000"
.LASF341:
	.ascii	"flow_check_derated_expected\000"
.LASF275:
	.ascii	"reduction_gallons\000"
.LASF282:
	.ascii	"highest_reason_in_list\000"
.LASF284:
	.ascii	"ufim_maximum_valves_in_system_we_can_have_ON_now\000"
.LASF269:
	.ascii	"non_controller_seconds\000"
.LASF438:
	.ascii	"list_of_foal_stations_ON\000"
.LASF421:
	.ascii	"action_reason\000"
.LASF271:
	.ascii	"SYSTEM_REPORT_RECORD\000"
.LASF362:
	.ascii	"seconds_of_flow_during_irrigation\000"
.LASF253:
	.ascii	"mlb_limit_during_mvor_closed_gpm\000"
.LASF631:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF199:
	.ascii	"pi_flag2\000"
.LASF303:
	.ascii	"ufim_the_valves_ON_meet_the_flow_checking_cycles_re"
	.ascii	"quirement\000"
.LASF401:
	.ascii	"mv_last_measured_current_from_the_tpmicro_ma\000"
.LASF617:
	.ascii	"foal_irri\000"
.LASF35:
	.ascii	"no_longer_used_01\000"
.LASF48:
	.ascii	"no_longer_used_02\000"
.LASF57:
	.ascii	"no_longer_used_03\000"
.LASF78:
	.ascii	"checked_or_updated_and_made_flow_recording_lines\000"
.LASF394:
	.ascii	"fm_accumulated_pulses_foal\000"
.LASF272:
	.ascii	"start_date\000"
.LASF364:
	.ascii	"POC_REPORT_RECORD\000"
.LASF193:
	.ascii	"pi_last_measured_current_ma\000"
.LASF576:
	.ascii	"BUDGET_timer_upkeep\000"
.LASF184:
	.ascii	"pi_flag\000"
.LASF14:
	.ascii	"long long int\000"
.LASF144:
	.ascii	"when_to_send_timer\000"
.LASF21:
	.ascii	"__month\000"
.LASF229:
	.ascii	"station_report_data_record_is_in_use\000"
.LASF443:
	.ascii	"timer_freeze_switch\000"
.LASF56:
	.ascii	"rre_in_process_to_turn_ON\000"
.LASF459:
	.ascii	"nm_handle_budget_reset_record\000"
.LASF110:
	.ascii	"InUse\000"
.LASF389:
	.ascii	"fm_accumulated_pulses_irri\000"
.LASF405:
	.ascii	"mv_current_as_delivered_from_the_master_ma\000"
.LASF431:
	.ascii	"slow_closing_valve_seconds\000"
.LASF584:
	.ascii	"idx_poc_preserves\000"
.LASF287:
	.ascii	"ufim_expected_flow_rate_for_those_ON\000"
.LASF376:
	.ascii	"off_at_start_time\000"
.LASF214:
	.ascii	"manual_program_seconds_us\000"
.LASF267:
	.ascii	"programmed_irrigation_seconds\000"
.LASF155:
	.ascii	"watersense_min_cycle_zeroed_the_irrigation_time\000"
.LASF410:
	.ascii	"unused_01\000"
.LASF114:
	.ascii	"pListHdr\000"
.LASF102:
	.ascii	"number_of_pocs_in_this_system\000"
.LASF538:
	.ascii	"used\000"
.LASF437:
	.ascii	"list_of_foal_all_irrigation\000"
.LASF605:
	.ascii	"poc_ratio\000"
.LASF263:
	.ascii	"test_seconds\000"
.LASF228:
	.ascii	"did_not_irrigate_last_time\000"
.LASF334:
	.ascii	"flow_check_allow_table_to_lock\000"
.LASF314:
	.ascii	"system_master_most_recent_5_second_average\000"
.LASF629:
	.ascii	"percip_100000u\000"
.LASF354:
	.ascii	"pad_bytes_avaiable_for_use\000"
.LASF50:
	.ascii	"flow_check_to_be_excluded_from_future_checking\000"
.LASF100:
	.ascii	"delivered_MVOR_in_effect_closed\000"
.LASF332:
	.ascii	"flow_check_required_station_cycles\000"
.LASF370:
	.ascii	"used_manual_programmed_gallons\000"
.LASF41:
	.ascii	"w_involved_in_a_flow_problem\000"
.LASF177:
	.ascii	"moisture_balance_prevented_irrigation\000"
.LASF331:
	.ascii	"derate_cell_iterations\000"
.LASF311:
	.ascii	"flow_checking_block_out_remaining_seconds\000"
.LASF628:
	.ascii	"MINUTES_PER_HOUR\000"
.LASF27:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF489:
	.ascii	"start_day\000"
.LASF301:
	.ascii	"ufim_there_is_a_PUMP_mix_condition_b\000"
.LASF255:
	.ascii	"mlb_limit_during_all_other_times_gpm\000"
.LASF158:
	.ascii	"flow_never_checked\000"
.LASF71:
	.ascii	"system_level_no_valves_ON_therefore_no_flow_checkin"
	.ascii	"g\000"
.LASF527:
	.ascii	"newval\000"
.LASF280:
	.ascii	"last_rollover_day\000"
.LASF358:
	.ascii	"seconds_of_flow_during_idle\000"
.LASF533:
	.ascii	"reduction_limit\000"
.LASF90:
	.ascii	"one_or_more_in_list_for_rre\000"
.LASF87:
	.ascii	"one_or_more_in_list_for_manual\000"
.LASF10:
	.ascii	"unsigned int\000"
.LASF32:
	.ascii	"xTimerHandle\000"
.LASF84:
	.ascii	"MVOR_in_effect_closed\000"
.LASF557:
	.ascii	"nm_BUDGET_calc_rpoc\000"
.LASF478:
	.ascii	"startday\000"
.LASF181:
	.ascii	"pi_last_cycle_end_time\000"
.LASF351:
	.ascii	"BY_SYSTEM_RECORD\000"
.LASF627:
	.ascii	"SQUARE_FOOTAGE_CONSTANT\000"
.LASF493:
	.ascii	"SIXTY\000"
.LASF8:
	.ascii	"short int\000"
.LASF179:
	.ascii	"record_start_time\000"
.LASF361:
	.ascii	"gallons_during_irrigation\000"
.LASF126:
	.ascii	"percent_adjust\000"
.LASF216:
	.ascii	"IRRIGATION_SYSTEM_GROUP_STRUCT\000"
.LASF509:
	.ascii	"days_to_end_of_period\000"
.LASF622:
	.ascii	"system_preserves_recursive_MUTEX\000"
.LASF23:
	.ascii	"__hours\000"
.LASF450:
	.ascii	"bpbr\000"
.LASF324:
	.ascii	"transition_timer_all_pump_valves_are_OFF\000"
.LASF445:
	.ascii	"ilcs\000"
.LASF318:
	.ascii	"system_stability_averages_ring\000"
.LASF146:
	.ascii	"pi_flow_data_has_been_stamped\000"
.LASF472:
	.ascii	"start\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
