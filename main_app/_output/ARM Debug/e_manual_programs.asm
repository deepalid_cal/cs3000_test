	.file	"e_manual_programs.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_MANUAL_PROGRAMS_editing_group,"aw",%nobits
	.align	2
	.type	g_MANUAL_PROGRAMS_editing_group, %object
	.size	g_MANUAL_PROGRAMS_editing_group, 4
g_MANUAL_PROGRAMS_editing_group:
	.space	4
	.section	.text.MANUAL_PROGRAMS_update_will_run,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_update_will_run, %function
MANUAL_PROGRAMS_update_will_run:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_manual_programs.c"
	.loc 1 81 0
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #68
.LCFI2:
	.loc 1 115 0
	ldr	r3, .L59
	ldr	r3, [r3, #0]
	str	r3, [fp, #-40]
	.loc 1 117 0
	ldr	r3, .L59+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-44]
	.loc 1 122 0
	sub	r3, fp, #52
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 124 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 126 0
	ldrh	r3, [fp, #-48]
	mov	r2, r3
	ldr	r3, .L59+8
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bhi	.L2
	.loc 1 126 0 is_stmt 0 discriminator 1
	ldrh	r3, [fp, #-48]
	mov	r2, r3
	ldr	r3, .L59+12
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcc	.L2
	.loc 1 128 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
.L2:
	.loc 1 135 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 137 0
	ldrh	r3, [fp, #-48]
	add	r3, r3, #1
	mov	r2, r3
	ldr	r3, .L59+8
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bhi	.L3
	.loc 1 137 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+12
	ldr	r2, [r3, #0]
	ldr	r3, .L59+8
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bhi	.L3
	.loc 1 139 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-12]
.L3:
	.loc 1 146 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 148 0
	ldr	r3, .L59+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L4
	.loc 1 148 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L4
	ldr	r3, .L59+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L4
	ldr	r3, .L59+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L4
	ldr	r3, .L59+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L4
	ldr	r3, .L59+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L5
.L4:
	.loc 1 150 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-20]
.L5:
	.loc 1 157 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 159 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L6
	.loc 1 161 0
	ldr	r3, .L59+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L7
	.loc 1 161 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-52]
	ldr	r3, .L59+40
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L59+44
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcc	.L8
.L7:
	.loc 1 162 0 is_stmt 1 discriminator 2
	ldr	r3, .L59+20
	ldr	r3, [r3, #0]
	.loc 1 161 0 discriminator 2
	cmp	r3, #0
	beq	.L9
	.loc 1 162 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L59+40
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L59+48
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcc	.L8
.L9:
	.loc 1 163 0 discriminator 1
	ldr	r3, .L59+24
	ldr	r3, [r3, #0]
	.loc 1 162 0 discriminator 1
	cmp	r3, #0
	beq	.L10
	.loc 1 163 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L59+40
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L59+52
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcc	.L8
.L10:
	.loc 1 164 0 discriminator 1
	ldr	r3, .L59+28
	ldr	r3, [r3, #0]
	.loc 1 163 0 discriminator 1
	cmp	r3, #0
	beq	.L11
	.loc 1 164 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L59+40
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L59+56
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcc	.L8
.L11:
	.loc 1 165 0 discriminator 1
	ldr	r3, .L59+32
	ldr	r3, [r3, #0]
	.loc 1 164 0 discriminator 1
	cmp	r3, #0
	beq	.L12
	.loc 1 165 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L59+40
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L59+60
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcc	.L8
.L12:
	.loc 1 166 0 discriminator 1
	ldr	r3, .L59+36
	ldr	r3, [r3, #0]
	.loc 1 165 0 discriminator 1
	cmp	r3, #0
	beq	.L6
	.loc 1 166 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L59+40
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L59+64
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcs	.L6
.L8:
	.loc 1 168 0
	mov	r3, #1
	str	r3, [fp, #-24]
.L6:
	.loc 1 176 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 178 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L13
	.loc 1 180 0
	ldrh	r3, [fp, #-48]
	mov	r0, r3
	sub	r1, fp, #56
	sub	r2, fp, #60
	sub	r3, fp, #64
	sub	ip, fp, #68
	str	ip, [sp, #0]
	bl	DateToDMY
	.loc 1 182 0
	ldr	r3, [fp, #-68]
	cmp	r3, #0
	bne	.L14
	.loc 1 182 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+68
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L14
	.loc 1 184 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L13
.L14:
	.loc 1 186 0
	ldr	r3, [fp, #-68]
	cmp	r3, #1
	bne	.L15
	.loc 1 186 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+72
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L15
	.loc 1 188 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L13
.L15:
	.loc 1 190 0
	ldr	r3, [fp, #-68]
	cmp	r3, #2
	bne	.L16
	.loc 1 190 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L16
	.loc 1 192 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L13
.L16:
	.loc 1 194 0
	ldr	r3, [fp, #-68]
	cmp	r3, #3
	bne	.L17
	.loc 1 194 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+80
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L17
	.loc 1 196 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L13
.L17:
	.loc 1 198 0
	ldr	r3, [fp, #-68]
	cmp	r3, #4
	bne	.L18
	.loc 1 198 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+84
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L18
	.loc 1 200 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L13
.L18:
	.loc 1 202 0
	ldr	r3, [fp, #-68]
	cmp	r3, #5
	bne	.L19
	.loc 1 202 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+88
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L19
	.loc 1 204 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L13
.L19:
	.loc 1 206 0
	ldr	r3, [fp, #-68]
	cmp	r3, #6
	bne	.L13
	.loc 1 206 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+92
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L13
	.loc 1 208 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-28]
.L13:
	.loc 1 216 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 218 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L20
	.loc 1 223 0
	ldrh	r3, [fp, #-48]
	mov	r2, r3
	ldr	r3, .L59+12
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcc	.L21
	.loc 1 227 0
	ldrh	r3, [fp, #-48]
	add	r3, r3, #1
	str	r3, [fp, #-32]
	b	.L22
.L21:
	.loc 1 231 0
	ldr	r3, .L59+12
	ldr	r3, [r3, #0]
	str	r3, [fp, #-32]
.L22:
	.loc 1 236 0
	mov	r3, #0
	str	r3, [fp, #-36]
	b	.L23
.L32:
	.loc 1 238 0
	ldr	r2, [fp, #-32]
	ldr	r3, [fp, #-36]
	add	r0, r2, r3
	sub	r1, fp, #56
	sub	r2, fp, #60
	sub	r3, fp, #64
	sub	ip, fp, #68
	str	ip, [sp, #0]
	bl	DateToDMY
	.loc 1 240 0
	ldr	r3, [fp, #-68]
	cmp	r3, #0
	bne	.L24
	.loc 1 240 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+68
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L24
	.loc 1 242 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L25
.L24:
	.loc 1 244 0
	ldr	r3, [fp, #-68]
	cmp	r3, #1
	bne	.L26
	.loc 1 244 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+72
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L26
	.loc 1 246 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L25
.L26:
	.loc 1 248 0
	ldr	r3, [fp, #-68]
	cmp	r3, #2
	bne	.L27
	.loc 1 248 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L27
	.loc 1 250 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L25
.L27:
	.loc 1 252 0
	ldr	r3, [fp, #-68]
	cmp	r3, #3
	bne	.L28
	.loc 1 252 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+80
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L28
	.loc 1 254 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L25
.L28:
	.loc 1 256 0
	ldr	r3, [fp, #-68]
	cmp	r3, #4
	bne	.L29
	.loc 1 256 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+84
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L29
	.loc 1 258 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L25
.L29:
	.loc 1 260 0
	ldr	r3, [fp, #-68]
	cmp	r3, #5
	bne	.L30
	.loc 1 260 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+88
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L30
	.loc 1 262 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L25
.L30:
	.loc 1 264 0
	ldr	r3, [fp, #-68]
	cmp	r3, #6
	bne	.L25
	.loc 1 264 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+92
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L25
	.loc 1 266 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-16]
.L25:
	.loc 1 269 0
	ldr	r2, [fp, #-32]
	ldr	r3, [fp, #-36]
	add	r2, r2, r3
	ldr	r3, .L59+8
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcs	.L57
.L31:
	.loc 1 236 0
	ldr	r3, [fp, #-36]
	add	r3, r3, #1
	str	r3, [fp, #-36]
.L23:
	.loc 1 236 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	cmp	r3, #6
	bls	.L32
	b	.L20
.L57:
	.loc 1 272 0 is_stmt 1
	mov	r0, r0	@ nop
.L20:
	.loc 1 282 0
	ldr	r3, .L59+96
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L33
	.loc 1 282 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+100
	flds	s15, [r3, #0]
	fcmpezs	s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L33
	.loc 1 284 0 is_stmt 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L34
	.loc 1 284 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L34
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L35
.L34:
	.loc 1 284 0 discriminator 2
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L36
	.loc 1 285 0 is_stmt 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L36
	.loc 1 285 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L36
.L35:
	.loc 1 287 0 is_stmt 1
	ldr	r3, .L59
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 284 0
	b	.L38
.L36:
	.loc 1 291 0
	ldr	r3, .L59
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 284 0
	b	.L38
.L33:
	.loc 1 296 0
	ldr	r3, .L59
	mov	r2, #0
	str	r2, [r3, #0]
.L38:
	.loc 1 302 0
	ldr	r3, .L59
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L39
	.loc 1 309 0
	ldr	r3, .L59+100
	flds	s15, [r3, #0]
	fcmpzs	s15
	fmstat
	bne	.L40
	.loc 1 311 0
	ldr	r3, .L59+4
	mov	r2, #3
	str	r2, [r3, #0]
	b	.L39
.L40:
	.loc 1 313 0
	ldr	r3, .L59+96
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L41
	.loc 1 315 0
	ldr	r3, .L59+4
	mov	r2, #4
	str	r2, [r3, #0]
	b	.L39
.L41:
	.loc 1 317 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L42
	.loc 1 319 0
	ldr	r3, .L59+4
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L39
.L42:
	.loc 1 321 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L43
	.loc 1 321 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L43
	.loc 1 323 0 is_stmt 1
	ldr	r3, .L59+4
	mov	r2, #2
	str	r2, [r3, #0]
	b	.L39
.L43:
	.loc 1 325 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L44
	.loc 1 325 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L44
	.loc 1 327 0 is_stmt 1
	ldr	r3, .L59+4
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L39
.L44:
	.loc 1 329 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L45
	.loc 1 329 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L45
	.loc 1 331 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L46
	.loc 1 333 0
	ldr	r3, .L59+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 331 0
	b	.L58
.L46:
	.loc 1 335 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L58
	.loc 1 337 0
	ldr	r3, .L59+4
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 331 0
	b	.L58
.L45:
	.loc 1 340 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L48
	.loc 1 340 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L48
	.loc 1 342 0 is_stmt 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L49
	.loc 1 344 0
	ldr	r3, .L59+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 342 0
	b	.L39
.L49:
	.loc 1 349 0
	ldr	r3, .L59+4
	mov	r2, #5
	str	r2, [r3, #0]
	.loc 1 342 0
	b	.L39
.L48:
	.loc 1 352 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L51
	.loc 1 352 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L51
	.loc 1 356 0 is_stmt 1
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	beq	.L52
	.loc 1 356 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L52
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L52
	.loc 1 360 0 is_stmt 1
	ldr	r3, .L59+4
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L53
.L52:
	.loc 1 362 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L54
	.loc 1 362 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L54
	.loc 1 364 0 is_stmt 1
	ldr	r3, .L59+4
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L53
.L54:
	.loc 1 369 0
	ldr	r3, .L59+4
	mov	r2, #5
	str	r2, [r3, #0]
	.loc 1 356 0
	b	.L39
.L53:
	b	.L39
.L51:
	.loc 1 375 0
	ldr	r3, .L59+4
	mov	r2, #5
	str	r2, [r3, #0]
	b	.L39
.L58:
	.loc 1 331 0
	mov	r0, r0	@ nop
.L39:
	.loc 1 383 0
	ldr	r3, .L59
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-40]
	cmp	r2, r3
	bne	.L55
	.loc 1 383 0 is_stmt 0 discriminator 1
	ldr	r3, .L59+4
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-44]
	cmp	r2, r3
	beq	.L1
.L55:
	.loc 1 385 0 is_stmt 1
	mov	r0, #0
	bl	Redraw_Screen
.L1:
	.loc 1 387 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L60:
	.align	2
.L59:
	.word	GuiVar_ManualPWillRun
	.word	GuiVar_ManualPWillNotRunReason
	.word	g_MANUAL_PROGRAMS_stop_date
	.word	g_MANUAL_PROGRAMS_start_date
	.word	GuiVar_ManualPStartTime1Enabled
	.word	GuiVar_ManualPStartTime2Enabled
	.word	GuiVar_ManualPStartTime3Enabled
	.word	GuiVar_ManualPStartTime4Enabled
	.word	GuiVar_ManualPStartTime5Enabled
	.word	GuiVar_ManualPStartTime6Enabled
	.word	-2004318071
	.word	GuiVar_ManualPStartTime1
	.word	GuiVar_ManualPStartTime2
	.word	GuiVar_ManualPStartTime3
	.word	GuiVar_ManualPStartTime4
	.word	GuiVar_ManualPStartTime5
	.word	GuiVar_ManualPStartTime6
	.word	GuiVar_ManualPWaterDay_Sun
	.word	GuiVar_ManualPWaterDay_Mon
	.word	GuiVar_ManualPWaterDay_Tue
	.word	GuiVar_ManualPWaterDay_Wed
	.word	GuiVar_ManualPWaterDay_Thu
	.word	GuiVar_ManualPWaterDay_Fri
	.word	GuiVar_ManualPWaterDay_Sat
	.word	GuiVar_StationSelectionGridStationCount
	.word	GuiVar_ManualPRunTime
.LFE0:
	.size	MANUAL_PROGRAMS_update_will_run, .-MANUAL_PROGRAMS_update_will_run
	.section	.text.MANUAL_PROGRAMS_process_water_day,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_process_water_day, %function
MANUAL_PROGRAMS_process_water_day:
.LFB1:
	.loc 1 391 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 392 0
	ldr	r3, .L73
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #1
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L62
.L70:
	.word	.L63
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
.L63:
	.loc 1 395 0
	ldr	r0, .L73+4
	bl	process_bool
	.loc 1 396 0
	b	.L71
.L64:
	.loc 1 399 0
	ldr	r0, .L73+8
	bl	process_bool
	.loc 1 400 0
	b	.L71
.L65:
	.loc 1 403 0
	ldr	r0, .L73+12
	bl	process_bool
	.loc 1 404 0
	b	.L71
.L66:
	.loc 1 407 0
	ldr	r0, .L73+16
	bl	process_bool
	.loc 1 408 0
	b	.L71
.L67:
	.loc 1 411 0
	ldr	r0, .L73+20
	bl	process_bool
	.loc 1 412 0
	b	.L71
.L68:
	.loc 1 415 0
	ldr	r0, .L73+24
	bl	process_bool
	.loc 1 416 0
	b	.L71
.L69:
	.loc 1 419 0
	ldr	r0, .L73+28
	bl	process_bool
	.loc 1 420 0
	b	.L71
.L62:
	.loc 1 423 0
	bl	bad_key_beep
.L71:
	.loc 1 426 0
	bl	Refresh_Screen
	.loc 1 428 0
	ldr	r3, .L73
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #6
	bgt	.L61
	.loc 1 430 0
	mov	r0, #0
	bl	CURSOR_Down
.L61:
	.loc 1 432 0
	ldmfd	sp!, {fp, pc}
.L74:
	.align	2
.L73:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ManualPWaterDay_Sun
	.word	GuiVar_ManualPWaterDay_Mon
	.word	GuiVar_ManualPWaterDay_Tue
	.word	GuiVar_ManualPWaterDay_Wed
	.word	GuiVar_ManualPWaterDay_Thu
	.word	GuiVar_ManualPWaterDay_Fri
	.word	GuiVar_ManualPWaterDay_Sat
.LFE1:
	.size	MANUAL_PROGRAMS_process_water_day, .-MANUAL_PROGRAMS_process_water_day
	.section	.text.MANUAL_PROGRAMS_process_start_time,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_process_start_time, %function
MANUAL_PROGRAMS_process_start_time:
.LFB2:
	.loc 1 436 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #20
.LCFI7:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 437 0
	mov	r3, #10
	str	r3, [sp, #0]
	mov	r3, #1
	str	r3, [sp, #4]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, #0
	mov	r3, #1440
	bl	process_uns32
	.loc 1 439 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, .L76
	cmp	r2, r3
	movhi	r2, #0
	movls	r2, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 440 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L77:
	.align	2
.L76:
	.word	1439
.LFE2:
	.size	MANUAL_PROGRAMS_process_start_time, .-MANUAL_PROGRAMS_process_start_time
	.section	.text.MANUAL_PROGRAMS_process_start_date,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_process_start_date, %function
MANUAL_PROGRAMS_process_start_date:
.LFB3:
	.loc 1 444 0
	@ args = 0, pretend = 0, frame = 60
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	sub	sp, sp, #68
.LCFI10:
	str	r0, [fp, #-64]
	.loc 1 449 0
	sub	r3, fp, #60
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 453 0
	ldrh	r3, [fp, #-56]
	mov	r2, r3
	ldr	r3, .L82
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L79
	.loc 1 455 0
	bl	good_key_beep
	.loc 1 457 0
	ldrh	r3, [fp, #-56]
	mov	r2, r3
	ldr	r3, .L82
	str	r2, [r3, #0]
	b	.L80
.L79:
	.loc 1 461 0
	ldrh	r3, [fp, #-56]
	mov	r2, r3
	ldr	r3, .L82+4
	ldr	r3, [r3, #0]
	mov	r1, #1
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	ldr	r0, [fp, #-64]
	ldr	r1, .L82
	bl	process_uns32
.L80:
	.loc 1 464 0
	ldr	r3, .L82
	ldr	r3, [r3, #0]
	sub	r2, fp, #52
	mov	r1, #225
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #125
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L82+8
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 466 0
	ldr	r3, .L82+4
	ldr	r2, [r3, #0]
	ldr	r3, .L82
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcs	.L78
	.loc 1 468 0
	ldr	r3, .L82
	ldr	r2, [r3, #0]
	ldr	r3, .L82+4
	str	r2, [r3, #0]
	.loc 1 470 0
	ldr	r3, .L82+4
	ldr	r3, [r3, #0]
	sub	r2, fp, #52
	mov	r1, #225
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #125
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L82+12
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
.L78:
	.loc 1 472 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L83:
	.align	2
.L82:
	.word	g_MANUAL_PROGRAMS_start_date
	.word	g_MANUAL_PROGRAMS_stop_date
	.word	GuiVar_ManualPStartDateStr
	.word	GuiVar_ManualPEndDateStr
.LFE3:
	.size	MANUAL_PROGRAMS_process_start_date, .-MANUAL_PROGRAMS_process_start_date
	.section	.text.MANUAL_PROGRAMS_process_end_date,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_process_end_date, %function
MANUAL_PROGRAMS_process_end_date:
.LFB4:
	.loc 1 476 0
	@ args = 0, pretend = 0, frame = 60
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI11:
	add	fp, sp, #8
.LCFI12:
	sub	sp, sp, #68
.LCFI13:
	str	r0, [fp, #-68]
	.loc 1 481 0
	sub	r3, fp, #64
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 483 0
	ldr	r3, .L85
	ldr	r4, [r3, #0]
	mov	r0, #31
	mov	r1, #12
	ldr	r2, .L85+4
	bl	DMYToDate
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	ldr	r0, [fp, #-68]
	ldr	r1, .L85+8
	mov	r2, r4
	bl	process_uns32
	.loc 1 485 0
	ldr	r3, .L85+8
	ldr	r3, [r3, #0]
	sub	r2, fp, #56
	mov	r1, #225
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #125
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L85+12
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 486 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L86:
	.align	2
.L85:
	.word	g_MANUAL_PROGRAMS_start_date
	.word	2042
	.word	g_MANUAL_PROGRAMS_stop_date
	.word	GuiVar_ManualPEndDateStr
.LFE4:
	.size	MANUAL_PROGRAMS_process_end_date, .-MANUAL_PROGRAMS_process_end_date
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_manual_programs.c\000"
	.section	.text.MANUAL_PROGRAMS_add_new_group,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_add_new_group, %function
MANUAL_PROGRAMS_add_new_group:
.LFB5:
	.loc 1 490 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI14:
	add	fp, sp, #4
.LCFI15:
	.loc 1 491 0
	ldr	r3, .L88
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L88+4
	ldr	r3, .L88+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 493 0
	ldr	r2, .L88+12
	ldr	r3, .L88+16
	mov	r0, r2
	mov	r1, r3
	bl	nm_GROUP_create_new_group_from_UI
	.loc 1 495 0
	ldr	r3, .L88
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 496 0
	ldmfd	sp!, {fp, pc}
.L89:
	.align	2
.L88:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	491
	.word	nm_MANUAL_PROGRAMS_create_new_group
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
.LFE5:
	.size	MANUAL_PROGRAMS_add_new_group, .-MANUAL_PROGRAMS_add_new_group
	.section	.text.MANUAL_PROGRAMS_process_group,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_process_group, %function
MANUAL_PROGRAMS_process_group:
.LFB6:
	.loc 1 516 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI16:
	add	fp, sp, #8
.LCFI17:
	sub	sp, sp, #52
.LCFI18:
	str	r0, [fp, #-52]
	str	r1, [fp, #-48]
	.loc 1 519 0
	ldr	r3, .L147
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L147+4
	cmp	r2, r3
	bne	.L145
.L92:
	.loc 1 522 0
	ldr	r3, [fp, #-52]
	cmp	r3, #67
	beq	.L93
	.loc 1 522 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-52]
	cmp	r3, #2
	bne	.L94
	ldr	r3, .L147+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #49
	bne	.L94
.L93:
	.loc 1 524 0 is_stmt 1
	bl	MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars
.L94:
	.loc 1 527 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	ldr	r2, .L147+12
	bl	KEYBOARD_process_key
	.loc 1 528 0
	b	.L90
.L145:
	.loc 1 531 0
	ldr	r3, [fp, #-52]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L96
.L105:
	.word	.L97
	.word	.L98
	.word	.L99
	.word	.L100
	.word	.L101
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L102
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L102
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L103
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L104
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L104
.L99:
	.loc 1 534 0
	ldr	r3, .L147+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #18
	ldrls	pc, [pc, r3, asl #2]
	b	.L106
.L110:
	.word	.L107
	.word	.L108
	.word	.L108
	.word	.L108
	.word	.L108
	.word	.L108
	.word	.L108
	.word	.L108
	.word	.L106
	.word	.L106
	.word	.L106
	.word	.L106
	.word	.L106
	.word	.L106
	.word	.L106
	.word	.L106
	.word	.L106
	.word	.L109
	.word	.L109
.L107:
	.loc 1 537 0
	mov	r0, #168
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	.loc 1 538 0
	b	.L111
.L108:
	.loc 1 547 0
	bl	MANUAL_PROGRAMS_process_water_day
	.loc 1 549 0
	bl	Refresh_Screen
	.loc 1 551 0
	bl	MANUAL_PROGRAMS_update_will_run
	.loc 1 552 0
	b	.L111
.L109:
	.loc 1 558 0
	bl	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
	.loc 1 560 0
	mov	r3, #3
	str	r3, [fp, #-44]
	.loc 1 561 0
	mov	r3, #67
	str	r3, [fp, #-36]
	.loc 1 562 0
	ldr	r3, .L147+16
	str	r3, [fp, #-24]
	.loc 1 563 0
	ldr	r3, .L147+20
	str	r3, [fp, #-28]
	.loc 1 564 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 565 0
	ldr	r3, .L147+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #17
	movne	r3, #0
	moveq	r3, #1
	str	r3, [fp, #-16]
	.loc 1 566 0
	ldr	r3, .L147+24
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 567 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Change_Screen
	.loc 1 568 0
	b	.L111
.L106:
	.loc 1 571 0
	bl	bad_key_beep
	.loc 1 573 0
	b	.L90
.L111:
	b	.L90
.L104:
	.loc 1 577 0
	ldr	r3, .L147+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #1
	cmp	r3, #15
	ldrls	pc, [pc, r3, asl #2]
	b	.L112
.L123:
	.word	.L113
	.word	.L113
	.word	.L113
	.word	.L113
	.word	.L113
	.word	.L113
	.word	.L113
	.word	.L114
	.word	.L115
	.word	.L116
	.word	.L117
	.word	.L118
	.word	.L119
	.word	.L120
	.word	.L121
	.word	.L122
.L113:
	.loc 1 586 0
	bl	MANUAL_PROGRAMS_process_water_day
	.loc 1 587 0
	b	.L124
.L114:
	.loc 1 590 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L147+28
	ldr	r2, .L147+32
	bl	MANUAL_PROGRAMS_process_start_time
	.loc 1 591 0
	b	.L124
.L115:
	.loc 1 594 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L147+36
	ldr	r2, .L147+40
	bl	MANUAL_PROGRAMS_process_start_time
	.loc 1 595 0
	b	.L124
.L116:
	.loc 1 598 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L147+44
	ldr	r2, .L147+48
	bl	MANUAL_PROGRAMS_process_start_time
	.loc 1 599 0
	b	.L124
.L117:
	.loc 1 602 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L147+52
	ldr	r2, .L147+56
	bl	MANUAL_PROGRAMS_process_start_time
	.loc 1 603 0
	b	.L124
.L118:
	.loc 1 606 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L147+60
	ldr	r2, .L147+64
	bl	MANUAL_PROGRAMS_process_start_time
	.loc 1 607 0
	b	.L124
.L119:
	.loc 1 610 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L147+68
	ldr	r2, .L147+72
	bl	MANUAL_PROGRAMS_process_start_time
	.loc 1 611 0
	b	.L124
.L120:
	.loc 1 614 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	MANUAL_PROGRAMS_process_start_date
	.loc 1 615 0
	b	.L124
.L121:
	.loc 1 618 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	MANUAL_PROGRAMS_process_end_date
	.loc 1 619 0
	b	.L124
.L122:
	.loc 1 622 0
	ldr	r3, [fp, #-52]
	ldr	r2, .L147+76	@ float
	str	r2, [sp, #0]	@ float
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L147+80
	ldr	r2, .L147+84	@ float
	ldr	r3, .L147+88	@ float
	bl	process_fl
	.loc 1 623 0
	b	.L124
.L112:
	.loc 1 626 0
	bl	bad_key_beep
.L124:
	.loc 1 629 0
	bl	Refresh_Screen
	.loc 1 631 0
	bl	MANUAL_PROGRAMS_update_will_run
	.loc 1 632 0
	b	.L90
.L102:
	.loc 1 636 0
	ldr	r4, [fp, #-52]
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	mov	r2, r0
	ldr	r3, .L147+92
	ldr	r1, .L147+96
	str	r1, [sp, #0]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	ldr	r3, .L147+100
	bl	GROUP_process_NEXT_and_PREV
	.loc 1 637 0
	b	.L90
.L101:
	.loc 1 640 0
	ldr	r3, .L147+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #1
	cmp	r3, #17
	ldrls	pc, [pc, r3, asl #2]
	b	.L125
.L132:
	.word	.L126
	.word	.L126
	.word	.L126
	.word	.L126
	.word	.L126
	.word	.L126
	.word	.L126
	.word	.L127
	.word	.L127
	.word	.L127
	.word	.L128
	.word	.L128
	.word	.L128
	.word	.L129
	.word	.L129
	.word	.L130
	.word	.L131
	.word	.L131
.L126:
	.loc 1 649 0
	mov	r0, #0
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 650 0
	b	.L133
.L127:
	.loc 1 655 0
	mov	r0, #1
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 656 0
	b	.L133
.L128:
	.loc 1 661 0
	ldr	r3, .L147+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #3
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 662 0
	b	.L133
.L129:
	.loc 1 666 0
	mov	r0, #11
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 667 0
	b	.L133
.L130:
	.loc 1 670 0
	mov	r0, #14
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 671 0
	b	.L133
.L131:
	.loc 1 675 0
	mov	r0, #16
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 676 0
	b	.L133
.L125:
	.loc 1 679 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 681 0
	b	.L90
.L133:
	b	.L90
.L97:
	.loc 1 684 0
	ldr	r3, .L147+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #1
	cmp	r3, #17
	ldrls	pc, [pc, r3, asl #2]
	b	.L134
.L140:
	.word	.L135
	.word	.L135
	.word	.L135
	.word	.L135
	.word	.L135
	.word	.L135
	.word	.L135
	.word	.L136
	.word	.L136
	.word	.L136
	.word	.L137
	.word	.L137
	.word	.L137
	.word	.L138
	.word	.L138
	.word	.L134
	.word	.L139
	.word	.L139
.L135:
	.loc 1 693 0
	mov	r0, #8
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 694 0
	b	.L141
.L136:
	.loc 1 699 0
	ldr	r3, .L147+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #3
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 700 0
	b	.L141
.L137:
	.loc 1 705 0
	mov	r0, #14
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 706 0
	b	.L141
.L138:
	.loc 1 710 0
	mov	r0, #16
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 711 0
	b	.L141
.L139:
	.loc 1 715 0
	bl	bad_key_beep
	.loc 1 716 0
	b	.L141
.L134:
	.loc 1 719 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 721 0
	b	.L90
.L141:
	b	.L90
.L98:
	.loc 1 724 0
	ldr	r3, .L147+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L146
.L143:
	.loc 1 727 0
	ldr	r0, .L147+104
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 728 0
	mov	r0, r0	@ nop
	.loc 1 733 0
	b	.L90
.L146:
	.loc 1 731 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 733 0
	b	.L90
.L100:
	.loc 1 736 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 737 0
	b	.L90
.L103:
	.loc 1 740 0
	ldr	r0, .L147+104
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 741 0
	b	.L90
.L96:
	.loc 1 744 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L90:
	.loc 1 747 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L148:
	.align	2
.L147:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_MANUAL_PROGRAMS_draw_menu
	.word	FDTO_STATION_SELECTION_GRID_draw_screen
	.word	STATION_SELECTION_GRID_process_screen
	.word	GuiVar_MenuScreenToShow
	.word	GuiVar_ManualPStartTime1
	.word	GuiVar_ManualPStartTime1Enabled
	.word	GuiVar_ManualPStartTime2
	.word	GuiVar_ManualPStartTime2Enabled
	.word	GuiVar_ManualPStartTime3
	.word	GuiVar_ManualPStartTime3Enabled
	.word	GuiVar_ManualPStartTime4
	.word	GuiVar_ManualPStartTime4Enabled
	.word	GuiVar_ManualPStartTime5
	.word	GuiVar_ManualPStartTime5Enabled
	.word	GuiVar_ManualPStartTime6
	.word	GuiVar_ManualPStartTime6Enabled
	.word	1065353216
	.word	GuiVar_ManualPRunTime
	.word	0
	.word	1139802112
	.word	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
	.word	MANUAL_PROGRAMS_get_group_at_this_index
	.word	FDTO_MANUAL_PROGRAMS_return_to_menu
.LFE6:
	.size	MANUAL_PROGRAMS_process_group, .-MANUAL_PROGRAMS_process_group
	.section	.text.FDTO_MANUAL_PROGRAMS_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_MANUAL_PROGRAMS_return_to_menu, %function
FDTO_MANUAL_PROGRAMS_return_to_menu:
.LFB7:
	.loc 1 771 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI19:
	add	fp, sp, #4
.LCFI20:
	.loc 1 772 0
	ldr	r3, .L150
	ldr	r0, .L150+4
	mov	r1, r3
	bl	FDTO_GROUP_return_to_menu
	.loc 1 773 0
	ldmfd	sp!, {fp, pc}
.L151:
	.align	2
.L150:
	.word	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
	.word	g_MANUAL_PROGRAMS_editing_group
.LFE7:
	.size	FDTO_MANUAL_PROGRAMS_return_to_menu, .-FDTO_MANUAL_PROGRAMS_return_to_menu
	.section	.text.FDTO_MANUAL_PROGRAMS_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_MANUAL_PROGRAMS_draw_menu
	.type	FDTO_MANUAL_PROGRAMS_draw_menu, %function
FDTO_MANUAL_PROGRAMS_draw_menu:
.LFB8:
	.loc 1 795 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #16
.LCFI23:
	str	r0, [fp, #-8]
	.loc 1 796 0
	ldr	r3, .L156
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L156+4
	mov	r3, #796
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 798 0
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	mov	r3, r0
	ldr	r2, .L156+8
	ldr	r1, .L156+12
	str	r1, [sp, #0]
	str	r2, [sp, #4]
	mov	r2, #1
	str	r2, [sp, #8]
	ldr	r0, [fp, #-8]
	ldr	r1, .L156+16
	mov	r2, r3
	mov	r3, #39
	bl	FDTO_GROUP_draw_menu
	.loc 1 800 0
	bl	MANUAL_PROGRAMS_update_will_run
	.loc 1 802 0
	ldr	r3, .L156+20
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L153
	.loc 1 804 0
	ldr	r3, .L156+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L154
	.loc 1 804 0 is_stmt 0 discriminator 1
	mov	r3, #18
	b	.L155
.L154:
	.loc 1 804 0 discriminator 2
	mov	r3, #17
.L155:
	.loc 1 804 0 discriminator 3
	ldr	r0, .L156+16
	mov	r1, r3
	bl	FDTO_STATION_SELECTION_GRID_redraw_calling_screen
.L153:
	.loc 1 807 0 is_stmt 1
	ldr	r3, .L156
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 808 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L157:
	.align	2
.L156:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
	.word	MANUAL_PROGRAMS_load_group_name_into_guivar
	.word	g_MANUAL_PROGRAMS_editing_group
	.word	g_STATION_SELECTION_GRID_user_pressed_back
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
.LFE8:
	.size	FDTO_MANUAL_PROGRAMS_draw_menu, .-FDTO_MANUAL_PROGRAMS_draw_menu
	.section	.text.MANUAL_PROGRAMS_process_menu,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_process_menu
	.type	MANUAL_PROGRAMS_process_menu, %function
MANUAL_PROGRAMS_process_menu:
.LFB9:
	.loc 1 827 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #24
.LCFI26:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 828 0
	ldr	r3, .L159
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L159+4
	mov	r3, #828
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 830 0
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	mov	r3, r0
	ldr	r0, .L159+8
	ldr	r1, .L159+12
	ldr	r2, .L159+16
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, .L159+20
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	ldr	r2, .L159+24
	bl	GROUP_process_menu
	.loc 1 832 0
	ldr	r3, .L159
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 833 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L160:
	.align	2
.L159:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	MANUAL_PROGRAMS_process_group
	.word	MANUAL_PROGRAMS_add_new_group
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
	.word	MANUAL_PROGRAMS_get_group_at_this_index
	.word	g_MANUAL_PROGRAMS_editing_group
.LFE9:
	.size	MANUAL_PROGRAMS_process_menu, .-MANUAL_PROGRAMS_process_menu
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI14-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI16-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI19-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI21-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI24-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_station_selection_grid.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/manual_programs.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x8b8
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF96
	.byte	0x1
	.4byte	.LASF97
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x70
	.uleb128 0x5
	.byte	0x4
	.4byte	0x9d
	.uleb128 0x6
	.4byte	0xa4
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x4
	.byte	0x57
	.4byte	0xa4
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x5
	.byte	0x4c
	.4byte	0xb8
	.uleb128 0x9
	.4byte	0x33
	.4byte	0xde
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0x103
	.uleb128 0xc
	.4byte	.LASF17
	.byte	0x6
	.byte	0x7e
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF18
	.byte	0x6
	.byte	0x80
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x6
	.byte	0x82
	.4byte	0xde
	.uleb128 0xb
	.byte	0x6
	.byte	0x7
	.byte	0x22
	.4byte	0x12f
	.uleb128 0xd
	.ascii	"T\000"
	.byte	0x7
	.byte	0x24
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"D\000"
	.byte	0x7
	.byte	0x26
	.4byte	0x41
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x7
	.byte	0x28
	.4byte	0x10e
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x14a
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0xb
	.byte	0x24
	.byte	0x8
	.byte	0x78
	.4byte	0x1d1
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x8
	.byte	0x7b
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x8
	.byte	0x83
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x8
	.byte	0x86
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x8
	.byte	0x88
	.4byte	0x1e2
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x8
	.byte	0x8d
	.4byte	0x1f4
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x8
	.byte	0x92
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x8
	.byte	0x96
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x8
	.byte	0x9a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x8
	.byte	0x9c
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	0x1dd
	.uleb128 0xf
	.4byte	0x1dd
	.byte	0
	.uleb128 0x10
	.4byte	0x53
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1d1
	.uleb128 0xe
	.byte	0x1
	.4byte	0x1f4
	.uleb128 0xf
	.4byte	0x103
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1e8
	.uleb128 0x3
	.4byte	.LASF30
	.byte	0x8
	.byte	0x9e
	.4byte	0x14a
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF31
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x1
	.byte	0x50
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x2f7
	.uleb128 0x12
	.ascii	"ldt\000"
	.byte	0x1
	.byte	0x59
	.4byte	0x12f
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x13
	.4byte	.LASF32
	.byte	0x1
	.byte	0x5b
	.4byte	0x8c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF33
	.byte	0x1
	.byte	0x5d
	.4byte	0x8c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF34
	.byte	0x1
	.byte	0x5f
	.4byte	0x8c
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF35
	.byte	0x1
	.byte	0x61
	.4byte	0x8c
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.4byte	.LASF36
	.byte	0x1
	.byte	0x63
	.4byte	0x8c
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.ascii	"day\000"
	.byte	0x1
	.byte	0x65
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x13
	.4byte	.LASF37
	.byte	0x1
	.byte	0x65
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x13
	.4byte	.LASF38
	.byte	0x1
	.byte	0x65
	.4byte	0x65
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x12
	.ascii	"dow\000"
	.byte	0x1
	.byte	0x65
	.4byte	0x65
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x13
	.4byte	.LASF39
	.byte	0x1
	.byte	0x67
	.4byte	0x8c
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x13
	.4byte	.LASF40
	.byte	0x1
	.byte	0x69
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0x6b
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x13
	.4byte	.LASF41
	.byte	0x1
	.byte	0x6d
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x13
	.4byte	.LASF42
	.byte	0x1
	.byte	0x6f
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x14
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x186
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x15
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x1b3
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x352
	.uleb128 0x16
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x1b3
	.4byte	0x352
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x1b3
	.4byte	0x357
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x1b3
	.4byte	0x357
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x10
	.4byte	0x65
	.uleb128 0x5
	.byte	0x4
	.4byte	0x65
	.uleb128 0x15
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x1bb
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x3a5
	.uleb128 0x16
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x1bb
	.4byte	0x352
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x17
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x1bd
	.4byte	0x13a
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x18
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x1bf
	.4byte	0x12f
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.byte	0
	.uleb128 0x15
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x1db
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x3ee
	.uleb128 0x16
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x1db
	.4byte	0x352
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x17
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x1dd
	.4byte	0x13a
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x18
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x1df
	.4byte	0x12f
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.byte	0
	.uleb128 0x19
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x1e9
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x15
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x203
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x43b
	.uleb128 0x16
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x203
	.4byte	0x43b
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x18
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x205
	.4byte	0x1fa
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x10
	.4byte	0x103
	.uleb128 0x19
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x302
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x31a
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x47f
	.uleb128 0x16
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x31a
	.4byte	0x47f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.4byte	0x8c
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x33a
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x4ae
	.uleb128 0x16
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x33a
	.4byte	0x43b
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x4be
	.uleb128 0xa
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF58
	.byte	0x9
	.2byte	0x2cd
	.4byte	0x4ae
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF59
	.byte	0x9
	.2byte	0x2cf
	.4byte	0x205
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF60
	.byte	0x9
	.2byte	0x2d1
	.4byte	0x4ae
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF61
	.byte	0x9
	.2byte	0x2d2
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF62
	.byte	0x9
	.2byte	0x2d3
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF63
	.byte	0x9
	.2byte	0x2d4
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF64
	.byte	0x9
	.2byte	0x2d5
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF65
	.byte	0x9
	.2byte	0x2d6
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF66
	.byte	0x9
	.2byte	0x2d7
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF67
	.byte	0x9
	.2byte	0x2d8
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF68
	.byte	0x9
	.2byte	0x2d9
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF69
	.byte	0x9
	.2byte	0x2da
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF70
	.byte	0x9
	.2byte	0x2db
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF71
	.byte	0x9
	.2byte	0x2dc
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF72
	.byte	0x9
	.2byte	0x2dd
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF73
	.byte	0x9
	.2byte	0x2e1
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF74
	.byte	0x9
	.2byte	0x2e2
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF75
	.byte	0x9
	.2byte	0x2e3
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF76
	.byte	0x9
	.2byte	0x2e4
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF77
	.byte	0x9
	.2byte	0x2e5
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF78
	.byte	0x9
	.2byte	0x2e6
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF79
	.byte	0x9
	.2byte	0x2e7
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF80
	.byte	0x9
	.2byte	0x2e8
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF81
	.byte	0x9
	.2byte	0x2e9
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF82
	.byte	0x9
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF83
	.byte	0x9
	.2byte	0x431
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF84
	.byte	0x9
	.2byte	0x432
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF85
	.byte	0xa
	.2byte	0x127
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF86
	.byte	0xa
	.2byte	0x132
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF87
	.byte	0xb
	.byte	0x30
	.4byte	0x665
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF88
	.byte	0xb
	.byte	0x34
	.4byte	0x67b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF89
	.byte	0xb
	.byte	0x36
	.4byte	0x691
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF90
	.byte	0xb
	.byte	0x38
	.4byte	0x6a7
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x1c
	.4byte	.LASF91
	.byte	0xc
	.byte	0x78
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF92
	.byte	0xd
	.byte	0x14
	.4byte	0x8c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF93
	.byte	0xe
	.byte	0x38
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF94
	.byte	0xe
	.byte	0x3a
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF95
	.byte	0x1
	.byte	0x45
	.4byte	0x8c
	.byte	0x5
	.byte	0x3
	.4byte	g_MANUAL_PROGRAMS_editing_group
	.uleb128 0x1b
	.4byte	.LASF58
	.byte	0x9
	.2byte	0x2cd
	.4byte	0x4ae
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF59
	.byte	0x9
	.2byte	0x2cf
	.4byte	0x205
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF60
	.byte	0x9
	.2byte	0x2d1
	.4byte	0x4ae
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF61
	.byte	0x9
	.2byte	0x2d2
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF62
	.byte	0x9
	.2byte	0x2d3
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF63
	.byte	0x9
	.2byte	0x2d4
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF64
	.byte	0x9
	.2byte	0x2d5
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF65
	.byte	0x9
	.2byte	0x2d6
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF66
	.byte	0x9
	.2byte	0x2d7
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF67
	.byte	0x9
	.2byte	0x2d8
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF68
	.byte	0x9
	.2byte	0x2d9
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF69
	.byte	0x9
	.2byte	0x2da
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF70
	.byte	0x9
	.2byte	0x2db
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF71
	.byte	0x9
	.2byte	0x2dc
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF72
	.byte	0x9
	.2byte	0x2dd
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF73
	.byte	0x9
	.2byte	0x2e1
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF74
	.byte	0x9
	.2byte	0x2e2
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF75
	.byte	0x9
	.2byte	0x2e3
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF76
	.byte	0x9
	.2byte	0x2e4
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF77
	.byte	0x9
	.2byte	0x2e5
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF78
	.byte	0x9
	.2byte	0x2e6
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF79
	.byte	0x9
	.2byte	0x2e7
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF80
	.byte	0x9
	.2byte	0x2e8
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF81
	.byte	0x9
	.2byte	0x2e9
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF82
	.byte	0x9
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF83
	.byte	0x9
	.2byte	0x431
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF84
	.byte	0x9
	.2byte	0x432
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF85
	.byte	0xa
	.2byte	0x127
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF86
	.byte	0xa
	.2byte	0x132
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF91
	.byte	0xc
	.byte	0x78
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF92
	.byte	0xd
	.byte	0x14
	.4byte	0x8c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF93
	.byte	0xe
	.byte	0x38
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF94
	.byte	0xe
	.byte	0x3a
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI17
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI20
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x64
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF29:
	.ascii	"_08_screen_to_draw\000"
.LASF96:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF42:
	.ascii	"lprevious_reason\000"
.LASF31:
	.ascii	"float\000"
.LASF66:
	.ascii	"GuiVar_ManualPStartTime3Enabled\000"
.LASF34:
	.ascii	"a_future_day_is_a_water_day\000"
.LASF94:
	.ascii	"g_MANUAL_PROGRAMS_stop_date\000"
.LASF77:
	.ascii	"GuiVar_ManualPWaterDay_Thu\000"
.LASF7:
	.ascii	"short int\000"
.LASF14:
	.ascii	"portTickType\000"
.LASF51:
	.ascii	"MANUAL_PROGRAMS_process_group\000"
.LASF21:
	.ascii	"_01_command\000"
.LASF43:
	.ascii	"MANUAL_PROGRAMS_update_will_run\000"
.LASF47:
	.ascii	"pstart_time_enabled_ptr\000"
.LASF41:
	.ascii	"lprevious_state\000"
.LASF59:
	.ascii	"GuiVar_ManualPRunTime\000"
.LASF83:
	.ascii	"GuiVar_StationSelectionGridShowOnlyStationsInThisGr"
	.ascii	"oup\000"
.LASF53:
	.ascii	"MANUAL_PROGRAMS_add_new_group\000"
.LASF82:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF38:
	.ascii	"year\000"
.LASF17:
	.ascii	"keycode\000"
.LASF97:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_manual_programs.c\000"
.LASF69:
	.ascii	"GuiVar_ManualPStartTime5\000"
.LASF22:
	.ascii	"_02_menu\000"
.LASF71:
	.ascii	"GuiVar_ManualPStartTime6\000"
.LASF70:
	.ascii	"GuiVar_ManualPStartTime5Enabled\000"
.LASF35:
	.ascii	"there_are_start_times\000"
.LASF84:
	.ascii	"GuiVar_StationSelectionGridStationCount\000"
.LASF32:
	.ascii	"today_is_in_the_schedule\000"
.LASF73:
	.ascii	"GuiVar_ManualPWaterDay_Fri\000"
.LASF75:
	.ascii	"GuiVar_ManualPWaterDay_Sat\000"
.LASF11:
	.ascii	"long long int\000"
.LASF89:
	.ascii	"GuiFont_DecimalChar\000"
.LASF40:
	.ascii	"start_date\000"
.LASF81:
	.ascii	"GuiVar_ManualPWillRun\000"
.LASF36:
	.ascii	"there_is_a_start_time_later_today\000"
.LASF13:
	.ascii	"long int\000"
.LASF15:
	.ascii	"xQueueHandle\000"
.LASF39:
	.ascii	"today_is_a_water_day\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF27:
	.ascii	"_06_u32_argument1\000"
.LASF48:
	.ascii	"MANUAL_PROGRAMS_process_start_date\000"
.LASF80:
	.ascii	"GuiVar_ManualPWillNotRunReason\000"
.LASF37:
	.ascii	"month\000"
.LASF20:
	.ascii	"DATE_TIME\000"
.LASF55:
	.ascii	"pcomplete_redraw\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF78:
	.ascii	"GuiVar_ManualPWaterDay_Tue\000"
.LASF67:
	.ascii	"GuiVar_ManualPStartTime4\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF91:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF3:
	.ascii	"signed char\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF45:
	.ascii	"pkeycode\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF68:
	.ascii	"GuiVar_ManualPStartTime4Enabled\000"
.LASF93:
	.ascii	"g_MANUAL_PROGRAMS_start_date\000"
.LASF95:
	.ascii	"g_MANUAL_PROGRAMS_editing_group\000"
.LASF72:
	.ascii	"GuiVar_ManualPStartTime6Enabled\000"
.LASF58:
	.ascii	"GuiVar_ManualPEndDateStr\000"
.LASF23:
	.ascii	"_03_structure_to_draw\000"
.LASF85:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF1:
	.ascii	"char\000"
.LASF76:
	.ascii	"GuiVar_ManualPWaterDay_Sun\000"
.LASF90:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF24:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF33:
	.ascii	"a_future_day_is_in_the_schedule\000"
.LASF28:
	.ascii	"_07_u32_argument2\000"
.LASF56:
	.ascii	"FDTO_MANUAL_PROGRAMS_draw_menu\000"
.LASF64:
	.ascii	"GuiVar_ManualPStartTime2Enabled\000"
.LASF92:
	.ascii	"g_STATION_SELECTION_GRID_user_pressed_back\000"
.LASF50:
	.ascii	"MANUAL_PROGRAMS_process_end_date\000"
.LASF74:
	.ascii	"GuiVar_ManualPWaterDay_Mon\000"
.LASF54:
	.ascii	"FDTO_MANUAL_PROGRAMS_return_to_menu\000"
.LASF60:
	.ascii	"GuiVar_ManualPStartDateStr\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF25:
	.ascii	"key_process_func_ptr\000"
.LASF61:
	.ascii	"GuiVar_ManualPStartTime1\000"
.LASF63:
	.ascii	"GuiVar_ManualPStartTime2\000"
.LASF65:
	.ascii	"GuiVar_ManualPStartTime3\000"
.LASF26:
	.ascii	"_04_func_ptr\000"
.LASF62:
	.ascii	"GuiVar_ManualPStartTime1Enabled\000"
.LASF44:
	.ascii	"MANUAL_PROGRAMS_process_start_time\000"
.LASF19:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF86:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF18:
	.ascii	"repeats\000"
.LASF87:
	.ascii	"GuiFont_LanguageActive\000"
.LASF88:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF49:
	.ascii	"str_48\000"
.LASF79:
	.ascii	"GuiVar_ManualPWaterDay_Wed\000"
.LASF57:
	.ascii	"MANUAL_PROGRAMS_process_menu\000"
.LASF52:
	.ascii	"pkey_event\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF30:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF46:
	.ascii	"pstart_time_ptr\000"
.LASF98:
	.ascii	"MANUAL_PROGRAMS_process_water_day\000"
.LASF16:
	.ascii	"xSemaphoreHandle\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
