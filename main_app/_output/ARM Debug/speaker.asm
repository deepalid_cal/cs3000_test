	.file	"speaker.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.speaker_is_enabled,"aw",%nobits
	.align	2
	.type	speaker_is_enabled, %object
	.size	speaker_is_enabled, 4
speaker_is_enabled:
	.space	4
	.section	.text.speaker_isr,"ax",%progbits
	.align	2
	.type	speaker_isr, %function
speaker_isr:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/speaker/speaker.c"
	.loc 1 39 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	.loc 1 44 0
	ldr	r3, .L2
	str	r3, [fp, #-4]
	.loc 1 45 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 48 0
	ldr	r3, .L2+4
	str	r3, [fp, #-8]
	.loc 1 49 0
	ldr	r3, [fp, #-8]
	mov	r2, #255
	str	r2, [r3, #0]
	.loc 1 50 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L3:
	.align	2
.L2:
	.word	1074118656
	.word	1073922048
.LFE0:
	.size	speaker_isr, .-speaker_isr
	.section	.text.nm_start_speaker,"ax",%progbits
	.align	2
	.type	nm_start_speaker, %function
nm_start_speaker:
.LFB1:
	.loc 1 54 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #20
.LCFI5:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 61 0
	ldr	r3, .L5
	str	r3, [fp, #-4]
	.loc 1 63 0
	mov	r3, #50
	str	r3, [fp, #-8]
	.loc 1 65 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #8
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	ldr	r2, [fp, #-8]
	rsb	r2, r2, #100
	mov	r1, r2, asl #8
	ldr	r2, [fp, #-8]
	rsb	r1, r2, r1
	ldr	r2, .L5+4
	umull	r0, r2, r1, r2
	mov	r2, r2, lsr #5
	and	r2, r2, #255
	orr	r2, r3, r2
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 69 0
	ldr	r3, .L5+8
	str	r3, [fp, #-12]
	.loc 1 71 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #24]
	.loc 1 73 0
	ldr	r3, [fp, #-12]
	mov	r2, #255
	str	r2, [r3, #0]
	.loc 1 75 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #16]
	.loc 1 76 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 79 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #4]
	.loc 1 81 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #0]
	orr	r2, r3, #-2147483648
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 82 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L6:
	.align	2
.L5:
	.word	1074118656
	.word	1374389535
	.word	1073922048
.LFE1:
	.size	nm_start_speaker, .-nm_start_speaker
	.section	.text.init_speaker,"ax",%progbits
	.align	2
	.global	init_speaker
	.type	init_speaker, %function
init_speaker:
.LFB2:
	.loc 1 86 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #12
.LCFI8:
	.loc 1 89 0
	ldr	r3, .L8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 98 0
	mov	r0, #15
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 101 0
	mov	r0, #1
	mov	r1, #1
	mov	r2, #1
	bl	clkpwr_setup_pwm
	.loc 1 105 0
	ldr	r3, .L8+4
	str	r3, [fp, #-8]
	.loc 1 107 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 114 0
	ldr	r3, .L8+8
	str	r3, [fp, #-12]
	.loc 1 117 0
	mov	r0, #23
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 120 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 121 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #112]
	.loc 1 122 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #40]
	.loc 1 123 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #60]
	.loc 1 126 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 127 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #16]
	.loc 1 129 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #24]
	.loc 1 130 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 131 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 132 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #36]
	.loc 1 134 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 135 0
	ldr	r3, [fp, #-12]
	mov	r2, #5
	str	r2, [r3, #20]
	.loc 1 137 0
	ldr	r3, [fp, #-12]
	mov	r2, #255
	str	r2, [r3, #0]
	.loc 1 139 0
	mov	r0, #3
	bl	xDisable_ISR
	.loc 1 140 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #3
	mov	r1, #1
	mov	r2, #1
	ldr	r3, .L8+12
	bl	xSetISR_Vector
	.loc 1 141 0
	mov	r0, #3
	bl	xEnable_ISR
	.loc 1 146 0
	ldr	r3, .L8+16
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 150 0
	ldr	r3, .L8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	.loc 1 151 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	speaker_hardware_MUTEX
	.word	1074118656
	.word	1073922048
	.word	speaker_isr
	.word	speaker_is_enabled
.LFE2:
	.size	init_speaker, .-init_speaker
	.section	.text.speaker_enable,"ax",%progbits
	.align	2
	.global	speaker_enable
	.type	speaker_enable, %function
speaker_enable:
.LFB3:
	.loc 1 155 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	.loc 1 156 0
	ldr	r3, .L11
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 158 0
	ldr	r3, .L11+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 160 0
	ldr	r3, .L11
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	.loc 1 161 0
	ldmfd	sp!, {fp, pc}
.L12:
	.align	2
.L11:
	.word	speaker_hardware_MUTEX
	.word	speaker_is_enabled
.LFE3:
	.size	speaker_enable, .-speaker_enable
	.section	.text.speaker_disable,"ax",%progbits
	.align	2
	.global	speaker_disable
	.type	speaker_disable, %function
speaker_disable:
.LFB4:
	.loc 1 165 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	.loc 1 166 0
	ldr	r3, .L14
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 168 0
	ldr	r3, .L14+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 170 0
	ldr	r3, .L14
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	.loc 1 171 0
	ldmfd	sp!, {fp, pc}
.L15:
	.align	2
.L14:
	.word	speaker_hardware_MUTEX
	.word	speaker_is_enabled
.LFE4:
	.size	speaker_disable, .-speaker_disable
	.section	.text.good_key_beep,"ax",%progbits
	.align	2
	.global	good_key_beep
	.type	good_key_beep, %function
good_key_beep:
.LFB5:
	.loc 1 191 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI13:
	add	fp, sp, #4
.LCFI14:
	sub	sp, sp, #8
.LCFI15:
	.loc 1 194 0
	ldr	r3, .L18
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 197 0
	ldr	r3, .L18+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L17
.LBB2:
	.loc 1 228 0
	mov	r0, #1
	mov	r1, #1
	mov	r2, #1
	bl	clkpwr_setup_pwm
	.loc 1 233 0
	mov	r3, #40
	str	r3, [fp, #-8]
	.loc 1 237 0
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #10
	str	r3, [fp, #-12]
	.loc 1 246 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	nm_start_speaker
.L17:
.LBE2:
	.loc 1 249 0
	ldr	r3, .L18
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	.loc 1 250 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L19:
	.align	2
.L18:
	.word	speaker_hardware_MUTEX
	.word	speaker_is_enabled
.LFE5:
	.size	good_key_beep, .-good_key_beep
	.section	.text.bad_key_beep,"ax",%progbits
	.align	2
	.global	bad_key_beep
	.type	bad_key_beep, %function
bad_key_beep:
.LFB6:
	.loc 1 270 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI16:
	add	fp, sp, #4
.LCFI17:
	sub	sp, sp, #8
.LCFI18:
	.loc 1 273 0
	ldr	r3, .L22
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 276 0
	ldr	r3, .L22+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L21
.LBB3:
	.loc 1 281 0
	mov	r0, #1
	mov	r1, #1
	mov	r2, #2
	bl	clkpwr_setup_pwm
	.loc 1 285 0
	mov	r3, #115
	str	r3, [fp, #-8]
	.loc 1 287 0
	ldr	r3, .L22+8
	str	r3, [fp, #-12]
	.loc 1 291 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	nm_start_speaker
.L21:
.LBE3:
	.loc 1 294 0
	ldr	r3, .L22
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	.loc 1 295 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L23:
	.align	2
.L22:
	.word	speaker_hardware_MUTEX
	.word	speaker_is_enabled
	.word	910000
.LFE6:
	.size	bad_key_beep, .-bad_key_beep
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI13-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI16-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc3xxx_spwm.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_intc_driver.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x4dd
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF80
	.byte	0x1
	.4byte	.LASF81
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x99
	.4byte	0x53
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x28
	.4byte	0x91
	.uleb128 0x6
	.4byte	.LASF48
	.byte	0x3
	.byte	0x2a
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x7
	.4byte	0x48
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x3
	.byte	0x2b
	.4byte	0x7a
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x8
	.4byte	0x48
	.4byte	0xb8
	.uleb128 0x9
	.4byte	0xa1
	.byte	0x3
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.byte	0x5
	.byte	0x24
	.4byte	0x199
	.uleb128 0xb
	.4byte	.LASF12
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF13
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF15
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF16
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF17
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF18
	.sleb128 5
	.uleb128 0xb
	.4byte	.LASF19
	.sleb128 6
	.uleb128 0xb
	.4byte	.LASF20
	.sleb128 7
	.uleb128 0xb
	.4byte	.LASF21
	.sleb128 8
	.uleb128 0xb
	.4byte	.LASF22
	.sleb128 9
	.uleb128 0xb
	.4byte	.LASF23
	.sleb128 10
	.uleb128 0xb
	.4byte	.LASF24
	.sleb128 11
	.uleb128 0xb
	.4byte	.LASF25
	.sleb128 12
	.uleb128 0xb
	.4byte	.LASF26
	.sleb128 13
	.uleb128 0xb
	.4byte	.LASF27
	.sleb128 14
	.uleb128 0xb
	.4byte	.LASF28
	.sleb128 15
	.uleb128 0xb
	.4byte	.LASF29
	.sleb128 16
	.uleb128 0xb
	.4byte	.LASF30
	.sleb128 17
	.uleb128 0xb
	.4byte	.LASF31
	.sleb128 18
	.uleb128 0xb
	.4byte	.LASF32
	.sleb128 19
	.uleb128 0xb
	.4byte	.LASF33
	.sleb128 20
	.uleb128 0xb
	.4byte	.LASF34
	.sleb128 21
	.uleb128 0xb
	.4byte	.LASF35
	.sleb128 22
	.uleb128 0xb
	.4byte	.LASF36
	.sleb128 23
	.uleb128 0xb
	.4byte	.LASF37
	.sleb128 24
	.uleb128 0xb
	.4byte	.LASF38
	.sleb128 25
	.uleb128 0xb
	.4byte	.LASF39
	.sleb128 26
	.uleb128 0xb
	.4byte	.LASF40
	.sleb128 27
	.uleb128 0xb
	.4byte	.LASF41
	.sleb128 28
	.uleb128 0xb
	.4byte	.LASF42
	.sleb128 29
	.uleb128 0xb
	.4byte	.LASF43
	.sleb128 30
	.uleb128 0xb
	.4byte	.LASF44
	.sleb128 31
	.uleb128 0xb
	.4byte	.LASF45
	.sleb128 32
	.uleb128 0xb
	.4byte	.LASF46
	.sleb128 33
	.uleb128 0xb
	.4byte	.LASF47
	.sleb128 34
	.byte	0
	.uleb128 0x5
	.byte	0x74
	.byte	0x4
	.byte	0x28
	.4byte	0x244
	.uleb128 0xc
	.ascii	"ir\000"
	.byte	0x4
	.byte	0x2a
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.ascii	"tcr\000"
	.byte	0x4
	.byte	0x2b
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.ascii	"tc\000"
	.byte	0x4
	.byte	0x2c
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.ascii	"pr\000"
	.byte	0x4
	.byte	0x2d
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.ascii	"pc\000"
	.byte	0x4
	.byte	0x2e
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.ascii	"mcr\000"
	.byte	0x4
	.byte	0x2f
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.ascii	"mr\000"
	.byte	0x4
	.byte	0x30
	.4byte	0x244
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.ascii	"ccr\000"
	.byte	0x4
	.byte	0x31
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.ascii	"cr\000"
	.byte	0x4
	.byte	0x32
	.4byte	0x249
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xc
	.ascii	"emr\000"
	.byte	0x4
	.byte	0x33
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x6
	.4byte	.LASF49
	.byte	0x4
	.byte	0x34
	.4byte	0x25e
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x6
	.4byte	.LASF50
	.byte	0x4
	.byte	0x35
	.4byte	0x91
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.byte	0
	.uleb128 0x7
	.4byte	0xa8
	.uleb128 0x7
	.4byte	0xa8
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x25e
	.uleb128 0x9
	.4byte	0xa1
	.byte	0xb
	.byte	0
	.uleb128 0x7
	.4byte	0x24e
	.uleb128 0x3
	.4byte	.LASF51
	.byte	0x4
	.byte	0x36
	.4byte	0x199
	.uleb128 0xa
	.byte	0x4
	.byte	0x6
	.byte	0x1d
	.4byte	0x29b
	.uleb128 0xb
	.4byte	.LASF52
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF53
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF54
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF55
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF56
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF57
	.sleb128 5
	.byte	0
	.uleb128 0xd
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF58
	.uleb128 0x3
	.4byte	.LASF59
	.byte	0x7
	.byte	0x35
	.4byte	0xa1
	.uleb128 0x3
	.4byte	.LASF60
	.byte	0x8
	.byte	0x57
	.4byte	0x29b
	.uleb128 0x3
	.4byte	.LASF61
	.byte	0x9
	.byte	0x4c
	.4byte	0x2af
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x2d5
	.uleb128 0x9
	.4byte	0xa1
	.byte	0x1
	.byte	0
	.uleb128 0xe
	.4byte	.LASF64
	.byte	0x1
	.byte	0x26
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x30a
	.uleb128 0xf
	.4byte	.LASF62
	.byte	0x1
	.byte	0x28
	.4byte	0x30a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xf
	.4byte	.LASF63
	.byte	0x1
	.byte	0x29
	.4byte	0x310
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.4byte	0x263
	.uleb128 0x10
	.byte	0x4
	.4byte	0x96
	.uleb128 0xe
	.4byte	.LASF65
	.byte	0x1
	.byte	0x35
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x375
	.uleb128 0x11
	.4byte	.LASF66
	.byte	0x1
	.byte	0x35
	.4byte	0x375
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF67
	.byte	0x1
	.byte	0x35
	.4byte	0x375
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xf
	.4byte	.LASF62
	.byte	0x1
	.byte	0x37
	.4byte	0x30a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xf
	.4byte	.LASF63
	.byte	0x1
	.byte	0x39
	.4byte	0x310
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xf
	.4byte	.LASF68
	.byte	0x1
	.byte	0x3f
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.4byte	0x48
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF71
	.byte	0x1
	.byte	0x55
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x3b0
	.uleb128 0xf
	.4byte	.LASF63
	.byte	0x1
	.byte	0x67
	.4byte	0x310
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xf
	.4byte	.LASF62
	.byte	0x1
	.byte	0x6f
	.4byte	0x30a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF69
	.byte	0x1
	.byte	0x9a
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF70
	.byte	0x1
	.byte	0xa4
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF72
	.byte	0x1
	.byte	0xbe
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x41a
	.uleb128 0x15
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0xf
	.4byte	.LASF66
	.byte	0x1
	.byte	0xe6
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xf
	.4byte	.LASF67
	.byte	0x1
	.byte	0xe6
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x10d
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x45d
	.uleb128 0x15
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0x17
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x11b
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x11b
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF74
	.byte	0xa
	.byte	0x30
	.4byte	0x46e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x12
	.4byte	0x2c5
	.uleb128 0xf
	.4byte	.LASF75
	.byte	0xa
	.byte	0x34
	.4byte	0x484
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x12
	.4byte	0x2c5
	.uleb128 0xf
	.4byte	.LASF76
	.byte	0xa
	.byte	0x36
	.4byte	0x49a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x12
	.4byte	0x2c5
	.uleb128 0xf
	.4byte	.LASF77
	.byte	0xa
	.byte	0x38
	.4byte	0x4b0
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x12
	.4byte	0x2c5
	.uleb128 0x18
	.4byte	.LASF79
	.byte	0xb
	.byte	0xd2
	.4byte	0x2ba
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF78
	.byte	0x1
	.byte	0x22
	.4byte	0x6f
	.byte	0x5
	.byte	0x3
	.4byte	speaker_is_enabled
	.uleb128 0x18
	.4byte	.LASF79
	.byte	0xb
	.byte	0xd2
	.4byte	0x2ba
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI14
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI17
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF80:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF13:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF14:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF30:
	.ascii	"CLKPWR_WDOG_CLK\000"
.LASF59:
	.ascii	"portTickType\000"
.LASF15:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF45:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF43:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF55:
	.ascii	"ISR_TRIGGER_NEGATIVE_EDGE\000"
.LASF78:
	.ascii	"speaker_is_enabled\000"
.LASF54:
	.ascii	"ISR_TRIGGER_HIGH_LEVEL\000"
.LASF32:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF4:
	.ascii	"short int\000"
.LASF67:
	.ascii	"timer_match\000"
.LASF23:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF20:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF31:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF17:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF2:
	.ascii	"signed char\000"
.LASF28:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF7:
	.ascii	"long long int\000"
.LASF76:
	.ascii	"GuiFont_DecimalChar\000"
.LASF41:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF0:
	.ascii	"char\000"
.LASF21:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF58:
	.ascii	"long int\000"
.LASF60:
	.ascii	"xQueueHandle\000"
.LASF29:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF64:
	.ascii	"speaker_isr\000"
.LASF71:
	.ascii	"init_speaker\000"
.LASF57:
	.ascii	"ISR_TRIGGER_DUAL_EDGE\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF68:
	.ascii	"duty\000"
.LASF40:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF10:
	.ascii	"SPWM_REGS_T\000"
.LASF73:
	.ascii	"bad_key_beep\000"
.LASF6:
	.ascii	"long long unsigned int\000"
.LASF75:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF24:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF5:
	.ascii	"unsigned int\000"
.LASF38:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF65:
	.ascii	"nm_start_speaker\000"
.LASF66:
	.ascii	"pwm_reload\000"
.LASF33:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF12:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF22:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF69:
	.ascii	"speaker_enable\000"
.LASF39:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF36:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF46:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF27:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF52:
	.ascii	"ISR_TRIGGER_FIXED\000"
.LASF77:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF19:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF56:
	.ascii	"ISR_TRIGGER_POSITIVE_EDGE\000"
.LASF61:
	.ascii	"xSemaphoreHandle\000"
.LASF25:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF53:
	.ascii	"ISR_TRIGGER_LOW_LEVEL\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF62:
	.ascii	"ltimer\000"
.LASF44:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF50:
	.ascii	"ctcr\000"
.LASF16:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF74:
	.ascii	"GuiFont_LanguageActive\000"
.LASF51:
	.ascii	"TIMER_CNTR_REGS_T\000"
.LASF26:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF9:
	.ascii	"BOOL_32\000"
.LASF48:
	.ascii	"pwm_ctrl\000"
.LASF63:
	.ascii	"lpwm\000"
.LASF37:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF79:
	.ascii	"speaker_hardware_MUTEX\000"
.LASF34:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF49:
	.ascii	"rsvd2\000"
.LASF70:
	.ascii	"speaker_disable\000"
.LASF18:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF72:
	.ascii	"good_key_beep\000"
.LASF35:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF81:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/spea"
	.ascii	"ker/speaker.c\000"
.LASF47:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF42:
	.ascii	"CLKPWR_UART5_CLK\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
