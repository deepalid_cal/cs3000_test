	.file	"irri_lights.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	irri_lights
	.section	.bss.irri_lights,"aw",%nobits
	.align	2
	.type	irri_lights, %object
	.size	irri_lights, 1940
irri_lights:
	.space	1940
	.global	IRRI_LIGHTS_seconds_since_chain_went_down
	.section	.bss.IRRI_LIGHTS_seconds_since_chain_went_down,"aw",%nobits
	.align	2
	.type	IRRI_LIGHTS_seconds_since_chain_went_down, %object
	.size	IRRI_LIGHTS_seconds_since_chain_went_down, 4
IRRI_LIGHTS_seconds_since_chain_went_down:
	.space	4
	.section	.bss.IRRI_LIGHTS_MENU_items,"aw",%nobits
	.align	2
	.type	IRRI_LIGHTS_MENU_items, %object
	.size	IRRI_LIGHTS_MENU_items, 192
IRRI_LIGHTS_MENU_items:
	.space	192
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/irri_lights.c\000"
	.section	.text.init_irri_lights,"ax",%progbits
	.align	2
	.global	init_irri_lights
	.type	init_irri_lights, %function
init_irri_lights:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/irri_lights.c"
	.loc 1 64 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #12
.LCFI2:
	.loc 1 69 0
	ldr	r3, .L6
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L6+4
	mov	r3, #69
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 72 0
	ldr	r0, .L6+8
	mov	r1, #0
	bl	nm_ListInit
	.loc 1 76 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L2
.L5:
	.loc 1 78 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L3
.L4:
	.loc 1 80 0 discriminator 2
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	LIGHTS_get_lights_array_index
	str	r0, [fp, #-16]
	.loc 1 83 0 discriminator 2
	ldr	r0, .L6+8
	ldr	r2, [fp, #-16]
	mov	r1, #28
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 84 0 discriminator 2
	ldr	r0, .L6+8
	ldr	r2, [fp, #-16]
	mov	r1, #24
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 85 0 discriminator 2
	ldr	r0, .L6+8
	ldr	r2, [fp, #-16]
	mov	r1, #20
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 86 0 discriminator 2
	ldr	r0, .L6+8
	ldr	r2, [fp, #-16]
	mov	r1, #52
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 87 0 discriminator 2
	ldr	r0, .L6+8
	ldr	r2, [fp, #-16]
	mov	r1, #56
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 88 0 discriminator 2
	ldr	r1, .L6+8
	ldr	r3, [fp, #-16]
	add	r2, r3, #1
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r1, r3
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 89 0 discriminator 2
	ldr	r0, .L6+8
	ldr	r2, [fp, #-16]
	mov	r1, #32
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 90 0 discriminator 2
	ldr	r0, .L6+8
	ldr	r2, [fp, #-16]
	mov	r1, #48
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 91 0 discriminator 2
	ldr	r0, .L6+8
	ldr	r2, [fp, #-16]
	mov	r1, #44
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 78 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L3:
	.loc 1 78 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #3
	bls	.L4
	.loc 1 76 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L2:
	.loc 1 76 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L5
	.loc 1 95 0 is_stmt 1
	ldr	r3, .L6
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 97 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L7:
	.align	2
.L6:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	irri_lights
.LFE0:
	.size	init_irri_lights, .-init_irri_lights
	.section	.text.IRRI_LIGHTS_restart_all_lights,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_restart_all_lights
	.type	IRRI_LIGHTS_restart_all_lights, %function
IRRI_LIGHTS_restart_all_lights:
.LFB1:
	.loc 1 120 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	.loc 1 124 0
	ldr	r3, .L13
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L13+4
	mov	r3, #124
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 133 0
	ldr	r0, .L13+8
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 135 0
	b	.L9
.L10:
	.loc 1 138 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #20]
	.loc 1 139 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 140 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #24]
	.loc 1 142 0
	ldr	r0, .L13+8
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L9:
	.loc 1 135 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L10
	.loc 1 148 0
	ldr	r0, .L13+8
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 150 0
	b	.L11
.L12:
	.loc 1 152 0
	ldr	r0, .L13+8
	ldr	r1, .L13+4
	mov	r2, #152
	bl	nm_ListRemoveHead_debug
	str	r0, [fp, #-8]
.L11:
	.loc 1 150 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L12
	.loc 1 157 0
	ldr	r3, .L13
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 158 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L14:
	.align	2
.L13:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	irri_lights
.LFE1:
	.size	IRRI_LIGHTS_restart_all_lights, .-IRRI_LIGHTS_restart_all_lights
	.section	.text.IRRI_LIGHTS_light_is_energized,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_light_is_energized
	.type	IRRI_LIGHTS_light_is_energized, %function
IRRI_LIGHTS_light_is_energized:
.LFB2:
	.loc 1 162 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-12]
	.loc 1 163 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 165 0
	ldr	r3, .L16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L16+4
	mov	r3, #165
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 167 0
	ldr	r1, .L16+8
	ldr	r3, [fp, #-12]
	add	r2, r3, #1
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 169 0
	ldr	r3, .L16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 171 0
	ldr	r3, [fp, #-8]
	.loc 1 173 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	irri_lights
.LFE2:
	.size	IRRI_LIGHTS_light_is_energized, .-IRRI_LIGHTS_light_is_energized
	.section	.text.IRRI_LIGHTS_get_light_at_this_index,"ax",%progbits
	.align	2
	.type	IRRI_LIGHTS_get_light_at_this_index, %function
IRRI_LIGHTS_get_light_at_this_index:
.LFB3:
	.loc 1 199 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-12]
	.loc 1 202 0
	ldr	r3, .L19
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L19+4
	mov	r3, #202
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 204 0
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L19+8
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 206 0
	ldr	r3, .L19
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 208 0
	ldr	r3, [fp, #-8]
	.loc 1 209 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L20:
	.align	2
.L19:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	irri_lights+20
.LFE3:
	.size	IRRI_LIGHTS_get_light_at_this_index, .-IRRI_LIGHTS_get_light_at_this_index
	.section	.text.IRRI_LIGHTS_get_number_of_energized_lights,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_number_of_energized_lights
	.type	IRRI_LIGHTS_get_number_of_energized_lights, %function
IRRI_LIGHTS_get_number_of_energized_lights:
.LFB4:
	.loc 1 223 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #4
.LCFI14:
	.loc 1 226 0
	ldr	r3, .L22
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L22+4
	mov	r3, #226
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 228 0
	ldr	r3, .L22+8
	ldr	r3, [r3, #8]
	str	r3, [fp, #-8]
	.loc 1 230 0
	ldr	r3, .L22
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 232 0
	ldr	r3, [fp, #-8]
	.loc 1 234 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L23:
	.align	2
.L22:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	irri_lights
.LFE4:
	.size	IRRI_LIGHTS_get_number_of_energized_lights, .-IRRI_LIGHTS_get_number_of_energized_lights
	.section .rodata
	.align	2
.LC1:
	.ascii	"Num of Energized Lights Mismatch\000"
	.section	.text.IRRI_LIGHTS_populate_pointers_of_energized_lights,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_populate_pointers_of_energized_lights
	.type	IRRI_LIGHTS_populate_pointers_of_energized_lights, %function
IRRI_LIGHTS_populate_pointers_of_energized_lights:
.LFB5:
	.loc 1 253 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #12
.LCFI17:
	.loc 1 260 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 262 0
	ldr	r0, .L29
	mov	r1, #0
	mov	r2, #192
	bl	memset
	.loc 1 264 0
	ldr	r3, .L29+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L29+8
	mov	r3, #264
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 266 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L25
.L27:
	.loc 1 268 0
	ldr	r0, [fp, #-12]
	bl	IRRI_LIGHTS_get_light_at_this_index
	str	r0, [fp, #-16]
	.loc 1 270 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L26
	.loc 1 270 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-12]
	bl	IRRI_LIGHTS_light_is_energized
	mov	r3, r0
	cmp	r3, #0
	beq	.L26
	.loc 1 272 0 is_stmt 1
	ldr	r3, .L29
	ldr	r2, [fp, #-8]
	ldr	r1, [fp, #-16]
	str	r1, [r3, r2, asl #2]
	.loc 1 274 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L26:
	.loc 1 266 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L25:
	.loc 1 266 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #47
	bls	.L27
	.loc 1 278 0 is_stmt 1
	bl	IRRI_LIGHTS_get_number_of_energized_lights
	mov	r2, r0
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	beq	.L28
	.loc 1 280 0
	ldr	r0, .L29+12
	bl	Alert_Message
.L28:
	.loc 1 283 0
	ldr	r3, .L29+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 285 0
	ldr	r3, [fp, #-8]
	.loc 1 287 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L30:
	.align	2
.L29:
	.word	IRRI_LIGHTS_MENU_items
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LC1
.LFE5:
	.size	IRRI_LIGHTS_populate_pointers_of_energized_lights, .-IRRI_LIGHTS_populate_pointers_of_energized_lights
	.section	.text.IRRI_LIGHTS_get_ptr_to_energized_light,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_ptr_to_energized_light
	.type	IRRI_LIGHTS_get_ptr_to_energized_light, %function
IRRI_LIGHTS_get_ptr_to_energized_light:
.LFB6:
	.loc 1 306 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI18:
	add	fp, sp, #0
.LCFI19:
	sub	sp, sp, #4
.LCFI20:
	str	r0, [fp, #-4]
	.loc 1 307 0
	ldr	r3, .L32
	ldr	r2, [fp, #-4]
	ldr	r3, [r3, r2, asl #2]
	.loc 1 308 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L33:
	.align	2
.L32:
	.word	IRRI_LIGHTS_MENU_items
.LFE6:
	.size	IRRI_LIGHTS_get_ptr_to_energized_light, .-IRRI_LIGHTS_get_ptr_to_energized_light
	.section	.text.IRRI_LIGHTS_get_box_index,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_box_index
	.type	IRRI_LIGHTS_get_box_index, %function
IRRI_LIGHTS_get_box_index:
.LFB7:
	.loc 1 322 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #12
.LCFI23:
	str	r0, [fp, #-16]
	.loc 1 327 0
	ldr	r3, .L35
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L35+4
	ldr	r3, .L35+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 329 0
	ldr	r0, [fp, #-16]
	bl	IRRI_LIGHTS_get_ptr_to_energized_light
	str	r0, [fp, #-8]
	.loc 1 331 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #32]
	str	r3, [fp, #-12]
	.loc 1 333 0
	ldr	r3, .L35
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 335 0
	ldr	r3, [fp, #-12]
	.loc 1 337 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L36:
	.align	2
.L35:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	327
.LFE7:
	.size	IRRI_LIGHTS_get_box_index, .-IRRI_LIGHTS_get_box_index
	.section	.text.IRRI_LIGHTS_get_output_index,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_output_index
	.type	IRRI_LIGHTS_get_output_index, %function
IRRI_LIGHTS_get_output_index:
.LFB8:
	.loc 1 351 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #12
.LCFI26:
	str	r0, [fp, #-16]
	.loc 1 356 0
	ldr	r3, .L38
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L38+4
	mov	r3, #356
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 358 0
	ldr	r0, [fp, #-16]
	bl	IRRI_LIGHTS_get_ptr_to_energized_light
	str	r0, [fp, #-8]
	.loc 1 360 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #36]
	str	r3, [fp, #-12]
	.loc 1 362 0
	ldr	r3, .L38
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 364 0
	ldr	r3, [fp, #-12]
	.loc 1 366 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L39:
	.align	2
.L38:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
.LFE8:
	.size	IRRI_LIGHTS_get_output_index, .-IRRI_LIGHTS_get_output_index
	.section	.text.IRRI_LIGHTS_get_reason_on_list,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_reason_on_list
	.type	IRRI_LIGHTS_get_reason_on_list, %function
IRRI_LIGHTS_get_reason_on_list:
.LFB9:
	.loc 1 380 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #12
.LCFI29:
	str	r0, [fp, #-16]
	.loc 1 385 0
	ldr	r3, .L41
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L41+4
	ldr	r3, .L41+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 387 0
	ldr	r0, [fp, #-16]
	bl	IRRI_LIGHTS_get_ptr_to_energized_light
	str	r0, [fp, #-8]
	.loc 1 389 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #16]
	str	r3, [fp, #-12]
	.loc 1 391 0
	ldr	r3, .L41
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 393 0
	ldr	r3, [fp, #-12]
	.loc 1 395 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L42:
	.align	2
.L41:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	385
.LFE9:
	.size	IRRI_LIGHTS_get_reason_on_list, .-IRRI_LIGHTS_get_reason_on_list
	.section	.text.IRRI_LIGHTS_get_stop_date,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_stop_date
	.type	IRRI_LIGHTS_get_stop_date, %function
IRRI_LIGHTS_get_stop_date:
.LFB10:
	.loc 1 409 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #12
.LCFI32:
	str	r0, [fp, #-16]
	.loc 1 414 0
	ldr	r3, .L44
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L44+4
	ldr	r3, .L44+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 416 0
	ldr	r0, [fp, #-16]
	bl	IRRI_LIGHTS_get_ptr_to_energized_light
	str	r0, [fp, #-8]
	.loc 1 418 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	str	r3, [fp, #-12]
	.loc 1 420 0
	ldr	r3, .L44
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 422 0
	ldr	r3, [fp, #-12]
	.loc 1 424 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L45:
	.align	2
.L44:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	414
.LFE10:
	.size	IRRI_LIGHTS_get_stop_date, .-IRRI_LIGHTS_get_stop_date
	.section	.text.IRRI_LIGHTS_get_stop_time,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_stop_time
	.type	IRRI_LIGHTS_get_stop_time, %function
IRRI_LIGHTS_get_stop_time:
.LFB11:
	.loc 1 438 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #12
.LCFI35:
	str	r0, [fp, #-16]
	.loc 1 443 0
	ldr	r3, .L47
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L47+4
	ldr	r3, .L47+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 445 0
	ldr	r0, [fp, #-16]
	bl	IRRI_LIGHTS_get_ptr_to_energized_light
	str	r0, [fp, #-8]
	.loc 1 447 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	str	r3, [fp, #-12]
	.loc 1 449 0
	ldr	r3, .L47
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 451 0
	ldr	r3, [fp, #-12]
	.loc 1 453 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L48:
	.align	2
.L47:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	443
.LFE11:
	.size	IRRI_LIGHTS_get_stop_time, .-IRRI_LIGHTS_get_stop_time
	.section	.text.IRRI_LIGHTS_get_light_stop_date_and_time,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_light_stop_date_and_time
	.type	IRRI_LIGHTS_get_light_stop_date_and_time, %function
IRRI_LIGHTS_get_light_stop_date_and_time:
.LFB12:
	.loc 1 466 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #16
.LCFI38:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 469 0
	mov	r3, #0
	strh	r3, [fp, #-8]	@ movhi
	.loc 1 470 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 472 0
	ldr	r3, .L51
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L51+4
	mov	r3, #472
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 474 0
	ldr	r1, .L51+8
	ldr	r3, [fp, #-20]
	add	r2, r3, #1
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L50
	.loc 1 476 0
	ldr	r0, .L51+8
	ldr	r2, [fp, #-20]
	mov	r1, #48
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-8]	@ movhi
	.loc 1 477 0
	ldr	r0, .L51+8
	ldr	r2, [fp, #-20]
	mov	r1, #44
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
.L50:
	.loc 1 480 0
	ldr	r3, .L51
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 482 0
	ldr	r3, [fp, #-16]
	mov	r1, r3
	sub	r2, fp, #12
	mov	r3, #6
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	memcpy
	.loc 1 484 0
	ldr	r0, [fp, #-16]
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L52:
	.align	2
.L51:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	irri_lights
.LFE12:
	.size	IRRI_LIGHTS_get_light_stop_date_and_time, .-IRRI_LIGHTS_get_light_stop_date_and_time
	.section	.text.IRRI_LIGHTS_maintain_lights_list,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_maintain_lights_list
	.type	IRRI_LIGHTS_maintain_lights_list, %function
IRRI_LIGHTS_maintain_lights_list:
.LFB13:
	.loc 1 506 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI39:
	add	fp, sp, #8
.LCFI40:
	sub	sp, sp, #24
.LCFI41:
	str	r0, [fp, #-32]
	.loc 1 513 0
	ldr	r3, .L67
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L67+4
	ldr	r3, .L67+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 518 0
	bl	FLOWSENSE_we_are_poafs
	mov	r3, r0
	cmp	r3, #0
	beq	.L54
	.loc 1 518 0 is_stmt 0 discriminator 1
	ldr	r3, .L67+12
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L54
	.loc 1 521 0 is_stmt 1
	ldr	r3, .L67+16
	ldr	r2, [r3, #0]
	ldr	r3, .L67+20
	cmp	r2, r3
	bhi	.L56
	.loc 1 523 0
	ldr	r3, .L67+16
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L67+16
	str	r2, [r3, #0]
	.loc 1 521 0
	b	.L56
.L54:
	.loc 1 528 0
	ldr	r3, .L67+16
	mov	r2, #0
	str	r2, [r3, #0]
.L56:
	.loc 1 533 0
	ldr	r0, .L67+24
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 535 0
	b	.L57
.L66:
.LBB2:
	.loc 1 537 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 541 0
	bl	FLOWSENSE_we_are_poafs
	mov	r3, r0
	cmp	r3, #0
	beq	.L58
	.loc 1 541 0 is_stmt 0 discriminator 1
	ldr	r3, .L67+12
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L59
.L58:
	.loc 1 543 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r4, [r3, #32]
	bl	FLOWSENSE_get_controller_index
	mov	r3, r0
	cmp	r4, r3
	beq	.L59
	.loc 1 545 0
	mov	r3, #1
	str	r3, [fp, #-16]
.L59:
	.loc 1 551 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L60
	.loc 1 554 0
	ldr	r3, [fp, #-12]
	ldr	r4, [r3, #32]
	bl	FLOWSENSE_get_controller_index
	mov	r3, r0
	cmp	r4, r3
	bne	.L61
	.loc 1 556 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #20]
	cmp	r3, #1
	bne	.L61
	.loc 1 561 0
	ldr	r3, .L67+16
	ldr	r3, [r3, #0]
	cmp	r3, #120
	bls	.L62
	.loc 1 564 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #20]
	.loc 1 566 0
	mov	r3, #1
	str	r3, [fp, #-16]
.L62:
	.loc 1 575 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #28]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-24]	@ movhi
	.loc 1 576 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #24]
	add	r3, r3, #120
	str	r3, [fp, #-28]
	.loc 1 579 0
	ldr	r2, [fp, #-28]
	ldr	r3, .L67+20
	cmp	r2, r3
	bls	.L63
	.loc 1 581 0
	ldrh	r3, [fp, #-24]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-24]	@ movhi
	.loc 1 582 0
	ldr	r3, [fp, #-28]
	sub	r3, r3, #86016
	sub	r3, r3, #384
	str	r3, [fp, #-28]
.L63:
	.loc 1 587 0
	sub	r3, fp, #28
	ldr	r0, [fp, #-32]
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L61
	.loc 1 590 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #20]
	.loc 1 592 0
	mov	r3, #1
	str	r3, [fp, #-16]
.L61:
	.loc 1 602 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L60
	.loc 1 602 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #20]
	cmp	r3, #0
	bne	.L60
	.loc 1 605 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	cmp	r3, #1
	beq	.L64
	.loc 1 606 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	.loc 1 605 0 discriminator 1
	cmp	r3, #2
	beq	.L64
	.loc 1 607 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	.loc 1 606 0
	cmp	r3, #3
	beq	.L64
	.loc 1 608 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	.loc 1 607 0
	cmp	r3, #4
	bne	.L60
.L64:
	.loc 1 611 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #20]
	.loc 1 617 0
	ldr	r3, .L67+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L67+4
	ldr	r3, .L67+32
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 619 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #32]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #36]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r2, r0
	ldr	r1, .L67+36
	mov	r3, #976
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	bic	r3, r3, #4
	strb	r3, [r2, #0]
	.loc 1 621 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #32]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #36]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r2, r0
	ldr	r1, .L67+36
	mov	r3, #976
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	bic	r3, r3, #8
	strb	r3, [r2, #0]
	.loc 1 623 0
	ldr	r3, .L67+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 628 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #12]
.L60:
	.loc 1 636 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L65
.LBB3:
	.loc 1 640 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-20]
	.loc 1 643 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #20]
	.loc 1 644 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 645 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #24]
	.loc 1 647 0
	ldr	r0, .L67+24
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
	.loc 1 650 0
	ldr	r0, .L67+24
	ldr	r1, [fp, #-20]
	ldr	r2, .L67+4
	ldr	r3, .L67+40
	bl	nm_ListRemove_debug
	b	.L57
.L65:
.LBE3:
	.loc 1 657 0
	ldr	r0, .L67+24
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L57:
.LBE2:
	.loc 1 535 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L66
	.loc 1 664 0
	ldr	r3, .L67
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 666 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L68:
	.align	2
.L67:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	513
	.word	comm_mngr
	.word	IRRI_LIGHTS_seconds_since_chain_went_down
	.word	86399
	.word	irri_lights
	.word	lights_preserves_recursive_MUTEX
	.word	617
	.word	lights_preserves
	.word	650
.LFE13:
	.size	IRRI_LIGHTS_maintain_lights_list, .-IRRI_LIGHTS_maintain_lights_list
	.section .rodata
	.align	2
.LC2:
	.ascii	"I_L: Bad action (1)\000"
	.align	2
.LC3:
	.ascii	"I_L: Bad light index (1)\000"
	.align	2
.LC4:
	.ascii	"I_L: Bad illc pointer (1)\000"
	.align	2
.LC5:
	.ascii	"I_L: ON received wih no time left\000"
	.align	2
.LC6:
	.ascii	"I_L: list insert fault\000"
	.align	2
.LC7:
	.ascii	"I_L: light index fault (1)\000"
	.section	.text.IRRI_LIGHTS_process_rcvd_lights_on_list_record,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_process_rcvd_lights_on_list_record
	.type	IRRI_LIGHTS_process_rcvd_lights_on_list_record, %function
IRRI_LIGHTS_process_rcvd_lights_on_list_record:
.LFB14:
	.loc 1 684 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI42:
	add	fp, sp, #4
.LCFI43:
	sub	sp, sp, #32
.LCFI44:
	str	r0, [fp, #-36]
	.loc 1 687 0
	ldr	r3, [fp, #-36]
	ldrb	r3, [r3, #6]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #255
	str	r3, [fp, #-16]
	.loc 1 689 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 698 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	strh	r3, [fp, #-28]	@ movhi
	.loc 1 699 0
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-32]
	.loc 1 701 0
	sub	r3, fp, #24
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 704 0
	ldr	r3, [fp, #-36]
	ldrb	r3, [r3, #7]	@ zero_extendqisi2
	cmp	r3, #1
	beq	.L70
	.loc 1 705 0 discriminator 1
	ldr	r3, [fp, #-36]
	ldrb	r3, [r3, #7]	@ zero_extendqisi2
	.loc 1 704 0 discriminator 1
	cmp	r3, #2
	beq	.L70
	.loc 1 706 0
	ldr	r3, [fp, #-36]
	ldrb	r3, [r3, #7]	@ zero_extendqisi2
	.loc 1 705 0
	cmp	r3, #3
	beq	.L70
	.loc 1 707 0
	ldr	r3, [fp, #-36]
	ldrb	r3, [r3, #7]	@ zero_extendqisi2
	.loc 1 706 0
	cmp	r3, #4
	beq	.L70
	.loc 1 709 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 710 0
	ldr	r0, .L86
	bl	Alert_Message
.L70:
	.loc 1 714 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L71
	.loc 1 714 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	ldrb	r3, [r3, #6]	@ zero_extendqisi2
	cmp	r3, #47
	bls	.L71
	.loc 1 716 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 717 0
	ldr	r0, .L86+4
	bl	Alert_Message
.L71:
	.loc 1 721 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L72
	.loc 1 721 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	ldrb	r3, [r3, #6]	@ zero_extendqisi2
	mov	r2, r3
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L86+8
	add	r3, r2, r3
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L72
	.loc 1 723 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 724 0
	ldr	r0, .L86+12
	bl	Alert_Message
.L72:
	.loc 1 728 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L73
	.loc 1 728 0 is_stmt 0 discriminator 1
	sub	r2, fp, #24
	sub	r3, fp, #32
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L73
	.loc 1 730 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 738 0
	bl	FLOWSENSE_we_are_poafs
	mov	r3, r0
	cmp	r3, #0
	beq	.L74
	.loc 1 738 0 is_stmt 0 discriminator 1
	bl	FLOWSENSE_get_controller_index
	mov	r2, r0
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L73
.L74:
	.loc 1 740 0 is_stmt 1
	ldr	r0, .L86+16
	bl	Alert_Message
.L73:
	.loc 1 744 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L75
	.loc 1 747 0
	ldr	r3, .L86+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L86+24
	ldr	r3, .L86+28
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 750 0
	ldr	r0, .L86+32
	ldr	r1, [fp, #-12]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	bne	.L76
	.loc 1 753 0
	ldr	r0, .L86+32
	ldr	r1, [fp, #-12]
	bl	nm_ListInsertTail
	mov	r3, r0
	cmp	r3, #0
	beq	.L76
	.loc 1 755 0
	ldr	r0, .L86+36
	bl	Alert_Message
.L76:
	.loc 1 760 0
	ldr	r3, [fp, #-36]
	ldrb	r3, [r3, #7]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	str	r2, [r3, #12]
	.loc 1 764 0
	ldr	r3, [fp, #-36]
	ldrb	r3, [r3, #7]	@ zero_extendqisi2
	sub	r3, r3, #1
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L85
.L82:
	.word	.L78
	.word	.L79
	.word	.L80
	.word	.L81
.L78:
	.loc 1 767 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #16]
	.loc 1 768 0
	b	.L83
.L79:
	.loc 1 771 0
	ldr	r3, [fp, #-12]
	mov	r2, #4
	str	r2, [r3, #16]
	.loc 1 772 0
	b	.L83
.L80:
	.loc 1 775 0
	ldr	r3, [fp, #-12]
	mov	r2, #7
	str	r2, [r3, #16]
	.loc 1 776 0
	b	.L83
.L81:
	.loc 1 779 0
	ldr	r3, [fp, #-12]
	mov	r2, #6
	str	r2, [r3, #16]
	.loc 1 780 0
	b	.L83
.L85:
	.loc 1 783 0
	mov	r0, r0	@ nop
.L83:
	.loc 1 788 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-12]
	str	r2, [r3, #28]
	.loc 1 789 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #24]
	.loc 1 798 0
	ldr	r3, .L86+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L69
.L75:
	.loc 1 802 0
	ldr	r0, .L86+40
	bl	Alert_Message
.L69:
	.loc 1 805 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L87:
	.align	2
.L86:
	.word	.LC2
	.word	.LC3
	.word	irri_lights+20
	.word	.LC4
	.word	.LC5
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	747
	.word	irri_lights
	.word	.LC6
	.word	.LC7
.LFE14:
	.size	IRRI_LIGHTS_process_rcvd_lights_on_list_record, .-IRRI_LIGHTS_process_rcvd_lights_on_list_record
	.section .rodata
	.align	2
.LC8:
	.ascii	"I_L: Bad light index (2)\000"
	.align	2
.LC9:
	.ascii	"I_L: Bad illc pointer (2)\000"
	.align	2
.LC10:
	.ascii	"I_L: Bad action (2)\000"
	.align	2
.LC11:
	.ascii	"I_L: light index fault (2)\000"
	.section	.text.IRRI_LIGHTS_process_rcvd_lights_action_needed_record,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_process_rcvd_lights_action_needed_record
	.type	IRRI_LIGHTS_process_rcvd_lights_action_needed_record, %function
IRRI_LIGHTS_process_rcvd_lights_action_needed_record:
.LFB15:
	.loc 1 825 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI45:
	add	fp, sp, #4
.LCFI46:
	sub	sp, sp, #12
.LCFI47:
	str	r0, [fp, #-16]
	.loc 1 826 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 831 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L89
	.loc 1 831 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #47
	bls	.L89
	.loc 1 833 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 834 0
	ldr	r0, .L97
	bl	Alert_Message
.L89:
	.loc 1 838 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L90
	.loc 1 838 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #3
	mov	r2, r3
	ldr	r3, .L97+4
	add	r3, r2, r3
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L90
	.loc 1 840 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 841 0
	ldr	r0, .L97+8
	bl	Alert_Message
.L90:
	.loc 1 844 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L91
	.loc 1 847 0
	ldr	r3, .L97+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L97+16
	ldr	r3, .L97+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 849 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	sub	r3, r3, #5
	cmp	r3, #6
	bhi	.L92
	.loc 1 864 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	cmp	r3, #5
	bne	.L94
	.loc 1 866 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r1, .L97+24
	mov	r3, #976
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #4
	strb	r3, [r2, #0]
.L94:
	.loc 1 872 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #20]
	.loc 1 873 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 874 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #24]
	.loc 1 877 0
	ldr	r0, .L97+28
	ldr	r1, [fp, #-12]
	ldr	r2, .L97+16
	ldr	r3, .L97+32
	bl	nm_ListRemove_debug
	.loc 1 878 0
	b	.L95
.L92:
	.loc 1 883 0
	ldr	r0, .L97+36
	bl	Alert_Message
	.loc 1 884 0
	mov	r0, r0	@ nop
.L95:
	.loc 1 888 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 890 0
	ldr	r3, .L97+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L88
.L91:
	.loc 1 895 0
	ldr	r0, .L97+40
	bl	Alert_Message
.L88:
	.loc 1 898 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L98:
	.align	2
.L97:
	.word	.LC8
	.word	irri_lights+20
	.word	.LC9
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	847
	.word	lights_preserves
	.word	irri_lights
	.word	877
	.word	.LC10
	.word	.LC11
.LFE15:
	.size	IRRI_LIGHTS_process_rcvd_lights_action_needed_record, .-IRRI_LIGHTS_process_rcvd_lights_action_needed_record
	.section .rodata
	.align	2
.LC12:
	.ascii	"I_L: Bad light index (3)\000"
	.align	2
.LC13:
	.ascii	"I_L: ON received wih no time left (2)\000"
	.section	.text.IRRI_LIGHTS_request_light_on,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_request_light_on
	.type	IRRI_LIGHTS_request_light_on, %function
IRRI_LIGHTS_request_light_on:
.LFB16:
	.loc 1 902 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI48:
	add	fp, sp, #4
.LCFI49:
	sub	sp, sp, #32
.LCFI50:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	str	r2, [fp, #-36]
	.loc 1 903 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 910 0
	ldr	r3, .L104
	ldr	r3, [r3, #44]
	cmp	r3, #0
	bne	.L100
	.loc 1 913 0
	ldr	r3, [fp, #-32]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-20]	@ movhi
	.loc 1 914 0
	ldr	r3, [fp, #-36]
	str	r3, [fp, #-24]
	.loc 1 916 0
	sub	r3, fp, #16
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 919 0
	ldr	r3, [fp, #-28]
	cmp	r3, #47
	bls	.L101
	.loc 1 921 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 922 0
	ldr	r0, .L104+4
	bl	Alert_Message
.L101:
	.loc 1 926 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L102
	.loc 1 926 0 is_stmt 0 discriminator 1
	sub	r2, fp, #16
	sub	r3, fp, #24
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L102
	.loc 1 928 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 929 0
	ldr	r0, .L104+8
	bl	Alert_Message
.L102:
	.loc 1 932 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L103
	.loc 1 935 0
	ldr	r3, .L104
	mov	r2, #1
	str	r2, [r3, #44]
	.loc 1 938 0
	ldr	r3, .L104
	ldr	r2, [fp, #-28]
	str	r2, [r3, #48]
	.loc 1 939 0
	ldr	r3, .L104
	ldr	r2, [fp, #-32]
	str	r2, [r3, #52]
	.loc 1 940 0
	ldr	r3, .L104
	ldr	r2, [fp, #-36]
	str	r2, [r3, #56]
	b	.L103
.L100:
	.loc 1 949 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L103:
	.loc 1 952 0
	ldr	r3, [fp, #-8]
	.loc 1 954 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L105:
	.align	2
.L104:
	.word	irri_comm
	.word	.LC12
	.word	.LC13
.LFE16:
	.size	IRRI_LIGHTS_request_light_on, .-IRRI_LIGHTS_request_light_on
	.section	.text.IRRI_LIGHTS_request_light_off,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_request_light_off
	.type	IRRI_LIGHTS_request_light_off, %function
IRRI_LIGHTS_request_light_off:
.LFB17:
	.loc 1 958 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI51:
	add	fp, sp, #4
.LCFI52:
	sub	sp, sp, #8
.LCFI53:
	str	r0, [fp, #-12]
	.loc 1 963 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 968 0
	ldr	r3, .L109
	ldr	r3, [r3, #44]
	cmp	r3, #0
	bne	.L107
	.loc 1 971 0
	ldr	r3, [fp, #-12]
	cmp	r3, #47
	bls	.L108
	.loc 1 973 0
	ldr	r0, .L109+4
	ldr	r1, .L109+8
	bl	Alert_index_out_of_range_with_filename
	b	.L107
.L108:
	.loc 1 978 0
	ldr	r3, .L109
	mov	r2, #1
	str	r2, [r3, #60]
	.loc 1 981 0
	ldr	r3, .L109
	ldr	r2, [fp, #-12]
	str	r2, [r3, #64]
	.loc 1 983 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L107:
	.loc 1 989 0
	ldr	r3, [fp, #-8]
	.loc 1 990 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L110:
	.align	2
.L109:
	.word	irri_comm
	.word	.LC0
	.word	973
.LFE17:
	.size	IRRI_LIGHTS_request_light_off, .-IRRI_LIGHTS_request_light_off
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI45-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI48-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI51-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI52-.LCFI51
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
	.text
.Letext0:
	.file 2 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/string.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/foal_comm.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/irri_lights.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/lights_report_data.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/comm_mngr.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_irri.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/irri_comm.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 19 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1239
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF203
	.byte	0x1
	.4byte	.LASF204
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x2
	.byte	0x16
	.4byte	0x30
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x5
	.byte	0x4
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x3
	.byte	0x35
	.4byte	0x30
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x4
	.byte	0x57
	.4byte	0x45
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x5
	.byte	0x4c
	.4byte	0x7c
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x6
	.byte	0x65
	.4byte	0x45
	.uleb128 0x6
	.4byte	0x5c
	.4byte	0xad
	.uleb128 0x7
	.4byte	0x30
	.byte	0x1
	.byte	0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF13
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x7
	.byte	0x3a
	.4byte	0x5c
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x7
	.byte	0x4c
	.4byte	0x3e
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x7
	.byte	0x5e
	.4byte	0xd5
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF17
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x7
	.byte	0x99
	.4byte	0xd5
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x7
	.byte	0x9d
	.4byte	0xd5
	.uleb128 0x6
	.4byte	0xca
	.4byte	0x102
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2
	.byte	0
	.uleb128 0x6
	.4byte	0xad
	.4byte	0x112
	.uleb128 0x7
	.4byte	0x30
	.byte	0xf
	.byte	0
	.uleb128 0x8
	.byte	0x6
	.byte	0x8
	.byte	0x22
	.4byte	0x133
	.uleb128 0x9
	.ascii	"T\000"
	.byte	0x8
	.byte	0x24
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.ascii	"D\000"
	.byte	0x8
	.byte	0x26
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x8
	.byte	0x28
	.4byte	0x112
	.uleb128 0x8
	.byte	0x14
	.byte	0x9
	.byte	0x18
	.4byte	0x18d
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x9
	.byte	0x1a
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x9
	.byte	0x1c
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x9
	.byte	0x1e
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x9
	.byte	0x20
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF25
	.byte	0x9
	.byte	0x23
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x9
	.byte	0x26
	.4byte	0x13e
	.uleb128 0x8
	.byte	0xc
	.byte	0x9
	.byte	0x2a
	.4byte	0x1cb
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x9
	.byte	0x2c
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF28
	.byte	0x9
	.byte	0x2e
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF29
	.byte	0x9
	.byte	0x30
	.4byte	0x1cb
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x18d
	.uleb128 0x2
	.4byte	.LASF30
	.byte	0x9
	.byte	0x32
	.4byte	0x198
	.uleb128 0x3
	.byte	0x4
	.byte	0x4
	.4byte	.LASF31
	.uleb128 0x6
	.4byte	0xca
	.4byte	0x1f3
	.uleb128 0x7
	.4byte	0x30
	.byte	0xb
	.byte	0
	.uleb128 0x6
	.4byte	0xca
	.4byte	0x203
	.uleb128 0x7
	.4byte	0x30
	.byte	0x3
	.byte	0
	.uleb128 0xc
	.byte	0x18
	.byte	0xa
	.2byte	0x210
	.4byte	0x267
	.uleb128 0xd
	.4byte	.LASF32
	.byte	0xa
	.2byte	0x215
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF33
	.byte	0xa
	.2byte	0x217
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF34
	.byte	0xa
	.2byte	0x21e
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF35
	.byte	0xa
	.2byte	0x220
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xd
	.4byte	.LASF36
	.byte	0xa
	.2byte	0x224
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF37
	.byte	0xa
	.2byte	0x22d
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0xe
	.4byte	.LASF38
	.byte	0xa
	.2byte	0x22f
	.4byte	0x203
	.uleb128 0xc
	.byte	0x8
	.byte	0xa
	.2byte	0x234
	.4byte	0x2b9
	.uleb128 0xd
	.4byte	.LASF39
	.byte	0xa
	.2byte	0x236
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF40
	.byte	0xa
	.2byte	0x238
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF41
	.byte	0xa
	.2byte	0x23b
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xd
	.4byte	.LASF42
	.byte	0xa
	.2byte	0x23d
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x7
	.byte	0
	.uleb128 0xe
	.4byte	.LASF43
	.byte	0xa
	.2byte	0x241
	.4byte	0x273
	.uleb128 0xc
	.byte	0x8
	.byte	0xa
	.2byte	0x245
	.4byte	0x2ed
	.uleb128 0xd
	.4byte	.LASF41
	.byte	0xa
	.2byte	0x248
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF42
	.byte	0xa
	.2byte	0x24a
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0xa
	.2byte	0x24e
	.4byte	0x2c5
	.uleb128 0xc
	.byte	0x10
	.byte	0xa
	.2byte	0x253
	.4byte	0x33f
	.uleb128 0xd
	.4byte	.LASF45
	.byte	0xa
	.2byte	0x258
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF41
	.byte	0xa
	.2byte	0x25a
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF40
	.byte	0xa
	.2byte	0x260
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF39
	.byte	0xa
	.2byte	0x263
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0xa
	.2byte	0x268
	.4byte	0x2f9
	.uleb128 0xc
	.byte	0x8
	.byte	0xa
	.2byte	0x26c
	.4byte	0x373
	.uleb128 0xd
	.4byte	.LASF45
	.byte	0xa
	.2byte	0x271
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF41
	.byte	0xa
	.2byte	0x273
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xe
	.4byte	.LASF47
	.byte	0xa
	.2byte	0x27c
	.4byte	0x34b
	.uleb128 0x8
	.byte	0x28
	.byte	0xb
	.byte	0x48
	.4byte	0x3f8
	.uleb128 0xa
	.4byte	.LASF48
	.byte	0xb
	.byte	0x4d
	.4byte	0x1d1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF42
	.byte	0xb
	.byte	0x57
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF49
	.byte	0xb
	.byte	0x5b
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0xb
	.byte	0x5d
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0xb
	.byte	0x61
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0xb
	.byte	0x63
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0xb
	.byte	0x6a
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xa
	.4byte	.LASF52
	.byte	0xb
	.byte	0x6c
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x2
	.4byte	.LASF53
	.byte	0xb
	.byte	0x70
	.4byte	0x37f
	.uleb128 0xf
	.2byte	0x794
	.byte	0xb
	.byte	0x74
	.4byte	0x429
	.uleb128 0xa
	.4byte	.LASF54
	.byte	0xb
	.byte	0x84
	.4byte	0x18d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF55
	.byte	0xb
	.byte	0x89
	.4byte	0x429
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x6
	.4byte	0x3f8
	.4byte	0x439
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2f
	.byte	0
	.uleb128 0x2
	.4byte	.LASF56
	.byte	0xb
	.byte	0x8d
	.4byte	0x403
	.uleb128 0x8
	.byte	0x10
	.byte	0xc
	.byte	0x2b
	.4byte	0x4af
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0xc
	.byte	0x32
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF58
	.byte	0xc
	.byte	0x35
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF59
	.byte	0xc
	.byte	0x38
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF60
	.byte	0xc
	.byte	0x3b
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0xc
	.byte	0x3e
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF52
	.byte	0xc
	.byte	0x41
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0xa
	.4byte	.LASF61
	.byte	0xc
	.byte	0x44
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.byte	0
	.uleb128 0x2
	.4byte	.LASF62
	.byte	0xc
	.byte	0x4a
	.4byte	0x444
	.uleb128 0x3
	.byte	0x8
	.byte	0x4
	.4byte	.LASF63
	.uleb128 0xc
	.byte	0x1
	.byte	0xd
	.2byte	0x944
	.4byte	0x525
	.uleb128 0x10
	.4byte	.LASF64
	.byte	0xd
	.2byte	0x94c
	.4byte	0xe7
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF50
	.byte	0xd
	.2byte	0x94f
	.4byte	0xe7
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF65
	.byte	0xd
	.2byte	0x953
	.4byte	0xe7
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF66
	.byte	0xd
	.2byte	0x958
	.4byte	0xe7
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF67
	.byte	0xd
	.2byte	0x95c
	.4byte	0xe7
	.byte	0x4
	.byte	0x4
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.byte	0xd
	.2byte	0x940
	.4byte	0x540
	.uleb128 0x12
	.4byte	.LASF205
	.byte	0xd
	.2byte	0x942
	.4byte	0xb4
	.uleb128 0x13
	.4byte	0x4c1
	.byte	0
	.uleb128 0xe
	.4byte	.LASF68
	.byte	0xd
	.2byte	0x962
	.4byte	0x525
	.uleb128 0xc
	.byte	0x14
	.byte	0xd
	.2byte	0x966
	.4byte	0x583
	.uleb128 0x14
	.ascii	"lrr\000"
	.byte	0xd
	.2byte	0x96a
	.4byte	0x4af
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF69
	.byte	0xd
	.2byte	0x970
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF70
	.byte	0xd
	.2byte	0x972
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.byte	0
	.uleb128 0xe
	.4byte	.LASF71
	.byte	0xd
	.2byte	0x978
	.4byte	0x54c
	.uleb128 0x15
	.2byte	0x400
	.byte	0xd
	.2byte	0x97c
	.4byte	0x5c8
	.uleb128 0xd
	.4byte	.LASF72
	.byte	0xd
	.2byte	0x983
	.4byte	0x102
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF73
	.byte	0xd
	.2byte	0x988
	.4byte	0x5c8
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x14
	.ascii	"lbf\000"
	.byte	0xd
	.2byte	0x98d
	.4byte	0x5d8
	.byte	0x3
	.byte	0x23
	.uleb128 0x3d0
	.byte	0
	.uleb128 0x6
	.4byte	0x583
	.4byte	0x5d8
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2f
	.byte	0
	.uleb128 0x6
	.4byte	0x540
	.4byte	0x5e8
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2f
	.byte	0
	.uleb128 0xe
	.4byte	.LASF74
	.byte	0xd
	.2byte	0x996
	.4byte	0x58f
	.uleb128 0x8
	.byte	0x8
	.byte	0xe
	.byte	0xe7
	.4byte	0x619
	.uleb128 0xa
	.4byte	.LASF75
	.byte	0xe
	.byte	0xf6
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF76
	.byte	0xe
	.byte	0xfe
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xe
	.4byte	.LASF77
	.byte	0xe
	.2byte	0x100
	.4byte	0x5f4
	.uleb128 0xc
	.byte	0xc
	.byte	0xe
	.2byte	0x105
	.4byte	0x64c
	.uleb128 0x14
	.ascii	"dt\000"
	.byte	0xe
	.2byte	0x107
	.4byte	0x133
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF67
	.byte	0xe
	.2byte	0x108
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xe
	.4byte	.LASF78
	.byte	0xe
	.2byte	0x109
	.4byte	0x625
	.uleb128 0x15
	.2byte	0x1e4
	.byte	0xe
	.2byte	0x10d
	.4byte	0x916
	.uleb128 0xd
	.4byte	.LASF79
	.byte	0xe
	.2byte	0x112
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF80
	.byte	0xe
	.2byte	0x116
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF81
	.byte	0xe
	.2byte	0x11f
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF82
	.byte	0xe
	.2byte	0x126
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xd
	.4byte	.LASF83
	.byte	0xe
	.2byte	0x12a
	.4byte	0x92
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF84
	.byte	0xe
	.2byte	0x12e
	.4byte	0x92
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xd
	.4byte	.LASF85
	.byte	0xe
	.2byte	0x133
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xd
	.4byte	.LASF86
	.byte	0xe
	.2byte	0x138
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xd
	.4byte	.LASF87
	.byte	0xe
	.2byte	0x13c
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xd
	.4byte	.LASF88
	.byte	0xe
	.2byte	0x143
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xd
	.4byte	.LASF89
	.byte	0xe
	.2byte	0x14c
	.4byte	0x916
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xd
	.4byte	.LASF90
	.byte	0xe
	.2byte	0x156
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xd
	.4byte	.LASF91
	.byte	0xe
	.2byte	0x158
	.4byte	0x1e3
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0xd
	.4byte	.LASF92
	.byte	0xe
	.2byte	0x15a
	.4byte	0xca
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0xd
	.4byte	.LASF93
	.byte	0xe
	.2byte	0x15c
	.4byte	0xca
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0xd
	.4byte	.LASF94
	.byte	0xe
	.2byte	0x174
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0xd
	.4byte	.LASF95
	.byte	0xe
	.2byte	0x176
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0xd
	.4byte	.LASF96
	.byte	0xe
	.2byte	0x180
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0xd
	.4byte	.LASF97
	.byte	0xe
	.2byte	0x182
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0xd
	.4byte	.LASF98
	.byte	0xe
	.2byte	0x186
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0xd
	.4byte	.LASF99
	.byte	0xe
	.2byte	0x195
	.4byte	0x1e3
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0xd
	.4byte	.LASF100
	.byte	0xe
	.2byte	0x197
	.4byte	0x1e3
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0xd
	.4byte	.LASF101
	.byte	0xe
	.2byte	0x19b
	.4byte	0x916
	.byte	0x3
	.byte	0x23
	.uleb128 0x108
	.uleb128 0xd
	.4byte	.LASF102
	.byte	0xe
	.2byte	0x19d
	.4byte	0x916
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.uleb128 0xd
	.4byte	.LASF103
	.byte	0xe
	.2byte	0x1a2
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0x168
	.uleb128 0xd
	.4byte	.LASF104
	.byte	0xe
	.2byte	0x1a9
	.4byte	0xca
	.byte	0x3
	.byte	0x23
	.uleb128 0x16c
	.uleb128 0xd
	.4byte	.LASF105
	.byte	0xe
	.2byte	0x1ab
	.4byte	0xca
	.byte	0x3
	.byte	0x23
	.uleb128 0x170
	.uleb128 0xd
	.4byte	.LASF106
	.byte	0xe
	.2byte	0x1ad
	.4byte	0xca
	.byte	0x3
	.byte	0x23
	.uleb128 0x174
	.uleb128 0xd
	.4byte	.LASF107
	.byte	0xe
	.2byte	0x1af
	.4byte	0xca
	.byte	0x3
	.byte	0x23
	.uleb128 0x178
	.uleb128 0xd
	.4byte	.LASF108
	.byte	0xe
	.2byte	0x1b5
	.4byte	0xca
	.byte	0x3
	.byte	0x23
	.uleb128 0x17c
	.uleb128 0xd
	.4byte	.LASF109
	.byte	0xe
	.2byte	0x1b7
	.4byte	0x92
	.byte	0x3
	.byte	0x23
	.uleb128 0x180
	.uleb128 0xd
	.4byte	.LASF110
	.byte	0xe
	.2byte	0x1be
	.4byte	0x92
	.byte	0x3
	.byte	0x23
	.uleb128 0x184
	.uleb128 0xd
	.4byte	.LASF111
	.byte	0xe
	.2byte	0x1c0
	.4byte	0x92
	.byte	0x3
	.byte	0x23
	.uleb128 0x188
	.uleb128 0xd
	.4byte	.LASF112
	.byte	0xe
	.2byte	0x1c4
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0x18c
	.uleb128 0xd
	.4byte	.LASF113
	.byte	0xe
	.2byte	0x1c6
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0x190
	.uleb128 0xd
	.4byte	.LASF114
	.byte	0xe
	.2byte	0x1cc
	.4byte	0x18d
	.byte	0x3
	.byte	0x23
	.uleb128 0x194
	.uleb128 0xd
	.4byte	.LASF115
	.byte	0xe
	.2byte	0x1d0
	.4byte	0x18d
	.byte	0x3
	.byte	0x23
	.uleb128 0x1a8
	.uleb128 0xd
	.4byte	.LASF116
	.byte	0xe
	.2byte	0x1d6
	.4byte	0x619
	.byte	0x3
	.byte	0x23
	.uleb128 0x1bc
	.uleb128 0xd
	.4byte	.LASF117
	.byte	0xe
	.2byte	0x1dc
	.4byte	0x92
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c4
	.uleb128 0xd
	.4byte	.LASF118
	.byte	0xe
	.2byte	0x1e2
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c8
	.uleb128 0xd
	.4byte	.LASF119
	.byte	0xe
	.2byte	0x1e5
	.4byte	0x64c
	.byte	0x3
	.byte	0x23
	.uleb128 0x1cc
	.uleb128 0xd
	.4byte	.LASF120
	.byte	0xe
	.2byte	0x1eb
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d8
	.uleb128 0xd
	.4byte	.LASF121
	.byte	0xe
	.2byte	0x1f2
	.4byte	0x92
	.byte	0x3
	.byte	0x23
	.uleb128 0x1dc
	.uleb128 0xd
	.4byte	.LASF122
	.byte	0xe
	.2byte	0x1f4
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0x1e0
	.byte	0
	.uleb128 0x6
	.4byte	0xdc
	.4byte	0x926
	.uleb128 0x7
	.4byte	0x30
	.byte	0xb
	.byte	0
	.uleb128 0xe
	.4byte	.LASF123
	.byte	0xe
	.2byte	0x1f6
	.4byte	0x658
	.uleb128 0x8
	.byte	0x14
	.byte	0xf
	.byte	0x9c
	.4byte	0x981
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0xf
	.byte	0xa2
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0xf
	.byte	0xa8
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF124
	.byte	0xf
	.byte	0xaa
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF125
	.byte	0xf
	.byte	0xac
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF126
	.byte	0xf
	.byte	0xae
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x2
	.4byte	.LASF127
	.byte	0xf
	.byte	0xb0
	.4byte	0x932
	.uleb128 0x8
	.byte	0x18
	.byte	0xf
	.byte	0xbe
	.4byte	0x9e9
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0xf
	.byte	0xc0
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF128
	.byte	0xf
	.byte	0xc4
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0xf
	.byte	0xc8
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF124
	.byte	0xf
	.byte	0xca
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF58
	.byte	0xf
	.byte	0xcc
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF129
	.byte	0xf
	.byte	0xd0
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x2
	.4byte	.LASF130
	.byte	0xf
	.byte	0xd2
	.4byte	0x98c
	.uleb128 0x8
	.byte	0x14
	.byte	0xf
	.byte	0xd8
	.4byte	0xa43
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0xf
	.byte	0xda
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF131
	.byte	0xf
	.byte	0xde
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF132
	.byte	0xf
	.byte	0xe0
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF133
	.byte	0xf
	.byte	0xe4
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF134
	.byte	0xf
	.byte	0xe8
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x2
	.4byte	.LASF135
	.byte	0xf
	.byte	0xea
	.4byte	0x9f4
	.uleb128 0x8
	.byte	0xf0
	.byte	0x10
	.byte	0x21
	.4byte	0xb31
	.uleb128 0xa
	.4byte	.LASF136
	.byte	0x10
	.byte	0x27
	.4byte	0x981
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF137
	.byte	0x10
	.byte	0x2e
	.4byte	0x9e9
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xa
	.4byte	.LASF138
	.byte	0x10
	.byte	0x32
	.4byte	0x33f
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xa
	.4byte	.LASF139
	.byte	0x10
	.byte	0x3d
	.4byte	0x373
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xa
	.4byte	.LASF140
	.byte	0x10
	.byte	0x43
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xa
	.4byte	.LASF141
	.byte	0x10
	.byte	0x45
	.4byte	0x267
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xa
	.4byte	.LASF142
	.byte	0x10
	.byte	0x50
	.4byte	0x916
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0xa
	.4byte	.LASF143
	.byte	0x10
	.byte	0x58
	.4byte	0x916
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0xa
	.4byte	.LASF144
	.byte	0x10
	.byte	0x60
	.4byte	0xb31
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0xa
	.4byte	.LASF145
	.byte	0x10
	.byte	0x67
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0xa
	.4byte	.LASF146
	.byte	0x10
	.byte	0x6c
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0xa
	.4byte	.LASF147
	.byte	0x10
	.byte	0x72
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0xa
	.4byte	.LASF148
	.byte	0x10
	.byte	0x76
	.4byte	0xa43
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0xa
	.4byte	.LASF149
	.byte	0x10
	.byte	0x7c
	.4byte	0xdc
	.byte	0x3
	.byte	0x23
	.uleb128 0xe8
	.uleb128 0xa
	.4byte	.LASF150
	.byte	0x10
	.byte	0x7e
	.4byte	0xca
	.byte	0x3
	.byte	0x23
	.uleb128 0xec
	.byte	0
	.uleb128 0x6
	.4byte	0xbf
	.4byte	0xb41
	.uleb128 0x7
	.4byte	0x30
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.4byte	.LASF151
	.byte	0x10
	.byte	0x80
	.4byte	0xa4e
	.uleb128 0x8
	.byte	0x4
	.byte	0x1
	.byte	0x36
	.4byte	0xb63
	.uleb128 0xa
	.4byte	.LASF152
	.byte	0x1
	.byte	0x38
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x2
	.4byte	.LASF153
	.byte	0x1
	.byte	0x3a
	.4byte	0xb4c
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF157
	.byte	0x1
	.byte	0x3f
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0xbb2
	.uleb128 0x17
	.4byte	.LASF154
	.byte	0x1
	.byte	0x41
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF155
	.byte	0x1
	.byte	0x42
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF156
	.byte	0x1
	.byte	0x43
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF158
	.byte	0x1
	.byte	0x77
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0xbda
	.uleb128 0x17
	.4byte	.LASF159
	.byte	0x1
	.byte	0x79
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x3f8
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF162
	.byte	0x1
	.byte	0xa1
	.byte	0x1
	.4byte	0xdc
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0xc1a
	.uleb128 0x19
	.4byte	.LASF41
	.byte	0x1
	.byte	0xa1
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF50
	.byte	0x1
	.byte	0xa3
	.4byte	0xdc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.4byte	0xca
	.uleb128 0x1b
	.4byte	.LASF206
	.byte	0x1
	.byte	0xc6
	.byte	0x1
	.4byte	0x45
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0xc58
	.uleb128 0x19
	.4byte	.LASF160
	.byte	0x1
	.byte	0xc6
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF161
	.byte	0x1
	.byte	0xc8
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF163
	.byte	0x1
	.byte	0xde
	.byte	0x1
	.4byte	0xca
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0xc84
	.uleb128 0x17
	.4byte	.LASF164
	.byte	0x1
	.byte	0xe0
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF165
	.byte	0x1
	.byte	0xfc
	.byte	0x1
	.4byte	0xca
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0xccc
	.uleb128 0x17
	.4byte	.LASF159
	.byte	0x1
	.byte	0xfe
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF166
	.byte	0x1
	.2byte	0x100
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1d
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x102
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF167
	.byte	0x1
	.2byte	0x131
	.byte	0x1
	.4byte	0xbda
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0xcfa
	.uleb128 0x1f
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x131
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF169
	.byte	0x1
	.2byte	0x141
	.byte	0x1
	.4byte	0xca
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0xd46
	.uleb128 0x1f
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x141
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x143
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x145
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF170
	.byte	0x1
	.2byte	0x15e
	.byte	0x1
	.4byte	0xca
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0xd92
	.uleb128 0x1f
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x15e
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x160
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x162
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF171
	.byte	0x1
	.2byte	0x17b
	.byte	0x1
	.4byte	0xca
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0xdde
	.uleb128 0x1f
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x17b
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x17d
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x17f
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF172
	.byte	0x1
	.2byte	0x198
	.byte	0x1
	.4byte	0xca
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0xe2a
	.uleb128 0x1f
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x198
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x19a
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x19c
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF173
	.byte	0x1
	.2byte	0x1b5
	.byte	0x1
	.4byte	0xca
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0xe76
	.uleb128 0x1f
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x1b5
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x1b7
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x1b9
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF174
	.byte	0x1
	.2byte	0x1d1
	.byte	0x1
	.4byte	0x133
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0xeb3
	.uleb128 0x1f
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x1d1
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1c
	.4byte	.LASF175
	.byte	0x1
	.2byte	0x1d3
	.4byte	0x133
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF176
	.byte	0x1
	.2byte	0x1f9
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0xf2d
	.uleb128 0x1f
	.4byte	.LASF177
	.byte	0x1
	.2byte	0x1f9
	.4byte	0xf2d
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1c
	.4byte	.LASF178
	.byte	0x1
	.2byte	0x1fb
	.4byte	0x133
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1c
	.4byte	.LASF179
	.byte	0x1
	.2byte	0x1fd
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x1c
	.4byte	.LASF180
	.byte	0x1
	.2byte	0x219
	.4byte	0xdc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0x1c
	.4byte	.LASF181
	.byte	0x1
	.2byte	0x27e
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1a
	.4byte	0xf32
	.uleb128 0xb
	.byte	0x4
	.4byte	0xf38
	.uleb128 0x1a
	.4byte	0x133
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF182
	.byte	0x1
	.2byte	0x2ab
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0xfb2
	.uleb128 0x1f
	.4byte	.LASF183
	.byte	0x1
	.2byte	0x2ab
	.4byte	0xfb2
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1c
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x2af
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF184
	.byte	0x1
	.2byte	0x2b1
	.4byte	0xdc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF179
	.byte	0x1
	.2byte	0x2b3
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1d
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x2b5
	.4byte	0x133
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1c
	.4byte	.LASF185
	.byte	0x1
	.2byte	0x2b5
	.4byte	0x133
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.uleb128 0x1a
	.4byte	0xfb7
	.uleb128 0xb
	.byte	0x4
	.4byte	0xfbd
	.uleb128 0x1a
	.4byte	0x2b9
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF186
	.byte	0x1
	.2byte	0x338
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x100a
	.uleb128 0x1f
	.4byte	.LASF183
	.byte	0x1
	.2byte	0x338
	.4byte	0x100a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF184
	.byte	0x1
	.2byte	0x33a
	.4byte	0xdc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF179
	.byte	0x1
	.2byte	0x33c
	.4byte	0xbda
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1a
	.4byte	0x100f
	.uleb128 0xb
	.byte	0x4
	.4byte	0x1015
	.uleb128 0x1a
	.4byte	0x2ed
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF187
	.byte	0x1
	.2byte	0x385
	.byte	0x1
	.4byte	0xdc
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x1093
	.uleb128 0x1f
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x385
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1f
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x385
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1f
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x385
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1c
	.4byte	.LASF184
	.byte	0x1
	.2byte	0x387
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1d
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x389
	.4byte	0x133
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF185
	.byte	0x1
	.2byte	0x389
	.4byte	0x133
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x1e
	.byte	0x1
	.4byte	.LASF188
	.byte	0x1
	.2byte	0x3bd
	.byte	0x1
	.4byte	0xdc
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x10cf
	.uleb128 0x1f
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x3bd
	.4byte	0xc1a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1d
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x3bf
	.4byte	0xdc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.4byte	.LASF189
	.byte	0x11
	.byte	0x30
	.4byte	0x10e0
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x1a
	.4byte	0x9d
	.uleb128 0x17
	.4byte	.LASF190
	.byte	0x11
	.byte	0x34
	.4byte	0x10f6
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x1a
	.4byte	0x9d
	.uleb128 0x17
	.4byte	.LASF191
	.byte	0x11
	.byte	0x36
	.4byte	0x110c
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x1a
	.4byte	0x9d
	.uleb128 0x17
	.4byte	.LASF192
	.byte	0x11
	.byte	0x38
	.4byte	0x1122
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x1a
	.4byte	0x9d
	.uleb128 0x22
	.4byte	.LASF195
	.byte	0xb
	.byte	0x96
	.4byte	0x439
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF193
	.byte	0x12
	.byte	0x33
	.4byte	0x1145
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x1a
	.4byte	0xf2
	.uleb128 0x17
	.4byte	.LASF194
	.byte	0x12
	.byte	0x3f
	.4byte	0x115b
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x1a
	.4byte	0x1f3
	.uleb128 0x23
	.4byte	.LASF196
	.byte	0xd
	.2byte	0x998
	.4byte	0x5e8
	.byte	0x1
	.byte	0x1
	.uleb128 0x23
	.4byte	.LASF197
	.byte	0xe
	.2byte	0x20c
	.4byte	0x926
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF198
	.byte	0x10
	.byte	0x83
	.4byte	0xb41
	.byte	0x1
	.byte	0x1
	.uleb128 0x23
	.4byte	.LASF199
	.byte	0x13
	.2byte	0x104
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x23
	.4byte	.LASF200
	.byte	0x13
	.2byte	0x107
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF201
	.byte	0x1
	.byte	0x33
	.4byte	0xca
	.byte	0x1
	.byte	0x1
	.uleb128 0x6
	.4byte	0xb63
	.4byte	0x11c2
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2f
	.byte	0
	.uleb128 0x17
	.4byte	.LASF202
	.byte	0x1
	.byte	0x3c
	.4byte	0x11b2
	.byte	0x5
	.byte	0x3
	.4byte	IRRI_LIGHTS_MENU_items
	.uleb128 0x24
	.4byte	.LASF195
	.byte	0x1
	.byte	0x2f
	.4byte	0x439
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	irri_lights
	.uleb128 0x23
	.4byte	.LASF196
	.byte	0xd
	.2byte	0x998
	.4byte	0x5e8
	.byte	0x1
	.byte	0x1
	.uleb128 0x23
	.4byte	.LASF197
	.byte	0xe
	.2byte	0x20c
	.4byte	0x926
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF198
	.byte	0x10
	.byte	0x83
	.4byte	0xb41
	.byte	0x1
	.byte	0x1
	.uleb128 0x23
	.4byte	.LASF199
	.byte	0x13
	.2byte	0x104
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x23
	.4byte	.LASF200
	.byte	0x13
	.2byte	0x107
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF201
	.byte	0x1
	.byte	0x33
	.4byte	0xca
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	IRRI_LIGHTS_seconds_since_chain_went_down
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI43
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI46
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI49
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI52
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xa4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF23:
	.ascii	"count\000"
.LASF99:
	.ascii	"failures_to_respond_to_the_token\000"
.LASF54:
	.ascii	"header_for_irri_lights_ON_list\000"
.LASF28:
	.ascii	"pNext\000"
.LASF181:
	.ascii	"list_item_to_delete\000"
.LASF142:
	.ascii	"two_wire_cable_excessive_current\000"
.LASF50:
	.ascii	"light_is_energized\000"
.LASF205:
	.ascii	"overall_size\000"
.LASF201:
	.ascii	"IRRI_LIGHTS_seconds_since_chain_went_down\000"
.LASF174:
	.ascii	"IRRI_LIGHTS_get_light_stop_date_and_time\000"
.LASF154:
	.ascii	"box_index\000"
.LASF2:
	.ascii	"signed char\000"
.LASF88:
	.ascii	"start_a_scan_captured_reason\000"
.LASF191:
	.ascii	"GuiFont_DecimalChar\000"
.LASF34:
	.ascii	"stop_for_the_highest_reason_in_all_systems\000"
.LASF38:
	.ascii	"STOP_KEY_RECORD_s\000"
.LASF149:
	.ascii	"walk_thru_need_to_send_gid_to_master\000"
.LASF118:
	.ascii	"flag_update_date_time\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF18:
	.ascii	"BOOL_32\000"
.LASF21:
	.ascii	"phead\000"
.LASF53:
	.ascii	"IRRI_LIGHTS_LIST_COMPONENT\000"
.LASF117:
	.ascii	"timer_commserver_msg_receipt_error\000"
.LASF59:
	.ascii	"record_start_date\000"
.LASF164:
	.ascii	"number_of_energized_lights\000"
.LASF170:
	.ascii	"IRRI_LIGHTS_get_output_index\000"
.LASF95:
	.ascii	"flag_myself_as_NEW_in_the_scan_RESP\000"
.LASF40:
	.ascii	"stop_datetime_d\000"
.LASF204:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/irri_lights.c\000"
.LASF16:
	.ascii	"UNS_32\000"
.LASF20:
	.ascii	"DATE_TIME\000"
.LASF6:
	.ascii	"long long int\000"
.LASF41:
	.ascii	"light_index_0_47\000"
.LASF150:
	.ascii	"walk_thru_gid_to_send\000"
.LASF161:
	.ascii	"llight\000"
.LASF39:
	.ascii	"stop_datetime_t\000"
.LASF147:
	.ascii	"send_crc_list\000"
.LASF112:
	.ascii	"token_rate_timer_has_timed_out\000"
.LASF175:
	.ascii	"stop_datetime\000"
.LASF198:
	.ascii	"irri_comm\000"
.LASF75:
	.ascii	"distribute_changes_to_slave\000"
.LASF80:
	.ascii	"state\000"
.LASF4:
	.ascii	"long int\000"
.LASF56:
	.ascii	"IRRI_LIGHTS\000"
.LASF93:
	.ascii	"since_the_scan__slave__number_of_clean_token_respon"
	.ascii	"ses_made\000"
.LASF148:
	.ascii	"mvor_request\000"
.LASF110:
	.ascii	"timer_message_resp\000"
.LASF25:
	.ascii	"InUse\000"
.LASF137:
	.ascii	"manual_water_request\000"
.LASF103:
	.ascii	"broadcast_chain_members_array\000"
.LASF153:
	.ascii	"IRRI_LIGHTS_MENU_SCROLL_BOX_ITEM\000"
.LASF9:
	.ascii	"portTickType\000"
.LASF26:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF125:
	.ascii	"time_seconds\000"
.LASF192:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF49:
	.ascii	"reason_on_list\000"
.LASF131:
	.ascii	"mvor_action_to_take\000"
.LASF61:
	.ascii	"lrr_pad_bytes_avaiable_for_use\000"
.LASF121:
	.ascii	"flowsense_device_establish_connection_timer\000"
.LASF178:
	.ascii	"watchdog_dt\000"
.LASF77:
	.ascii	"CHANGE_TRACKING_FOR_COMMUNICATIONS_STRUCT\000"
.LASF91:
	.ascii	"since_the_scan__master__number_of_clean_token_respo"
	.ascii	"nses_rcvd\000"
.LASF47:
	.ascii	"LIGHTS_OFF_XFER_RECORD\000"
.LASF8:
	.ascii	"size_t\000"
.LASF32:
	.ascii	"stop_for_this_reason\000"
.LASF48:
	.ascii	"list_linkage_for_irri_lights_ON_list\000"
.LASF17:
	.ascii	"unsigned int\000"
.LASF69:
	.ascii	"last_measured_current_ma\000"
.LASF136:
	.ascii	"test_request\000"
.LASF177:
	.ascii	"dt_ptr\000"
.LASF139:
	.ascii	"light_off_request\000"
.LASF43:
	.ascii	"LIGHTS_LIST_XFER_RECORD\000"
.LASF135:
	.ascii	"MVOR_KICK_OFF_STRUCT\000"
.LASF152:
	.ascii	"light_ptr\000"
.LASF57:
	.ascii	"programmed_seconds\000"
.LASF124:
	.ascii	"station_number\000"
.LASF157:
	.ascii	"init_irri_lights\000"
.LASF162:
	.ascii	"IRRI_LIGHTS_light_is_energized\000"
.LASF58:
	.ascii	"manual_seconds\000"
.LASF109:
	.ascii	"timer_device_exchange\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF203:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF29:
	.ascii	"pListHdr\000"
.LASF81:
	.ascii	"chain_is_down\000"
.LASF185:
	.ascii	"temp_dt\000"
.LASF100:
	.ascii	"failures_to_respond_to_the_token_today\000"
.LASF184:
	.ascii	"still_sane\000"
.LASF92:
	.ascii	"since_the_scan__slave__number_of_clean_tokens_rcvd\000"
.LASF187:
	.ascii	"IRRI_LIGHTS_request_light_on\000"
.LASF129:
	.ascii	"program_GID\000"
.LASF120:
	.ascii	"perform_two_wire_discovery\000"
.LASF10:
	.ascii	"xQueueHandle\000"
.LASF190:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF70:
	.ascii	"expansion_bytes\000"
.LASF45:
	.ascii	"in_use\000"
.LASF12:
	.ascii	"xTimerHandle\000"
.LASF63:
	.ascii	"double\000"
.LASF182:
	.ascii	"IRRI_LIGHTS_process_rcvd_lights_on_list_record\000"
.LASF105:
	.ascii	"device_exchange_port\000"
.LASF111:
	.ascii	"timer_token_rate_timer\000"
.LASF107:
	.ascii	"device_exchange_device_index\000"
.LASF60:
	.ascii	"number_of_on_cycles\000"
.LASF108:
	.ascii	"device_exchange_saved_comm_mngr_mode\000"
.LASF188:
	.ascii	"IRRI_LIGHTS_request_light_off\000"
.LASF82:
	.ascii	"flag_chain_down_at_the_next_opportunity\000"
.LASF42:
	.ascii	"action_needed\000"
.LASF186:
	.ascii	"IRRI_LIGHTS_process_rcvd_lights_action_needed_recor"
	.ascii	"d\000"
.LASF145:
	.ascii	"clear_runaway_gage_pressed\000"
.LASF76:
	.ascii	"send_changes_to_master\000"
.LASF119:
	.ascii	"token_date_time\000"
.LASF64:
	.ascii	"light_is_available\000"
.LASF166:
	.ascii	"ladded_LIGHTs\000"
.LASF52:
	.ascii	"output_index_0\000"
.LASF163:
	.ascii	"IRRI_LIGHTS_get_number_of_energized_lights\000"
.LASF196:
	.ascii	"lights_preserves\000"
.LASF123:
	.ascii	"COMM_MNGR_STRUCT\000"
.LASF22:
	.ascii	"ptail\000"
.LASF96:
	.ascii	"i_am_a_slave_and_i_have_rebooted\000"
.LASF200:
	.ascii	"lights_preserves_recursive_MUTEX\000"
.LASF98:
	.ascii	"pending_device_exchange_request\000"
.LASF173:
	.ascii	"IRRI_LIGHTS_get_stop_time\000"
.LASF62:
	.ascii	"LIGHTS_REPORT_RECORD\000"
.LASF169:
	.ascii	"IRRI_LIGHTS_get_box_index\000"
.LASF31:
	.ascii	"float\000"
.LASF138:
	.ascii	"light_on_request\000"
.LASF83:
	.ascii	"timer_rescan\000"
.LASF189:
	.ascii	"GuiFont_LanguageActive\000"
.LASF65:
	.ascii	"shorted_output\000"
.LASF176:
	.ascii	"IRRI_LIGHTS_maintain_lights_list\000"
.LASF102:
	.ascii	"tpmicro_firmware_up_to_date\000"
.LASF97:
	.ascii	"i_am_the_master_and_i_have_rebooted\000"
.LASF183:
	.ascii	"plxr\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF158:
	.ascii	"IRRI_LIGHTS_restart_all_lights\000"
.LASF159:
	.ascii	"lillc\000"
.LASF46:
	.ascii	"LIGHTS_ON_XFER_RECORD\000"
.LASF36:
	.ascii	"stop_for_all_reasons\000"
.LASF130:
	.ascii	"MANUAL_WATER_KICK_OFF_STRUCT\000"
.LASF202:
	.ascii	"IRRI_LIGHTS_MENU_items\000"
.LASF134:
	.ascii	"system_gid\000"
.LASF3:
	.ascii	"short int\000"
.LASF85:
	.ascii	"scans_while_chain_is_down\000"
.LASF78:
	.ascii	"DATE_TIME_TOKEN_STRUCT\000"
.LASF94:
	.ascii	"request_a_scan_in_the_next_token_RESP\000"
.LASF30:
	.ascii	"MIST_DLINK_TYPE\000"
.LASF165:
	.ascii	"IRRI_LIGHTS_populate_pointers_of_energized_lights\000"
.LASF67:
	.ascii	"reason\000"
.LASF197:
	.ascii	"comm_mngr\000"
.LASF114:
	.ascii	"packets_waiting_for_token\000"
.LASF171:
	.ascii	"IRRI_LIGHTS_get_reason_on_list\000"
.LASF84:
	.ascii	"timer_token_arrival\000"
.LASF144:
	.ascii	"system_gids_to_clear_mlbs_for\000"
.LASF180:
	.ascii	"flagged_for_removal\000"
.LASF140:
	.ascii	"send_stop_key_record\000"
.LASF71:
	.ascii	"BY_LIGHTS_RECORD\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF13:
	.ascii	"char\000"
.LASF79:
	.ascii	"mode\000"
.LASF143:
	.ascii	"two_wire_cable_overheated\000"
.LASF87:
	.ascii	"start_a_scan_at_the_next_opportunity\000"
.LASF122:
	.ascii	"flowsense_devices_are_connected\000"
.LASF101:
	.ascii	"main_app_firmware_up_to_date\000"
.LASF35:
	.ascii	"stop_in_all_systems\000"
.LASF160:
	.ascii	"pindex_0\000"
.LASF55:
	.ascii	"illcs\000"
.LASF89:
	.ascii	"is_a_NEW_member_in_the_chain\000"
.LASF24:
	.ascii	"offset\000"
.LASF126:
	.ascii	"set_expected\000"
.LASF179:
	.ascii	"ills\000"
.LASF86:
	.ascii	"we_have_crossed_midnight_so_clear_the_error_counts\000"
.LASF68:
	.ascii	"LIGHTS_BIT_FIELD\000"
.LASF168:
	.ascii	"pline_index\000"
.LASF194:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF44:
	.ascii	"LIGHTS_ACTION_XFER_RECORD\000"
.LASF206:
	.ascii	"IRRI_LIGHTS_get_light_at_this_index\000"
.LASF172:
	.ascii	"IRRI_LIGHTS_get_stop_date\000"
.LASF104:
	.ascii	"device_exchange_initial_event\000"
.LASF115:
	.ascii	"incoming_messages_or_packets\000"
.LASF15:
	.ascii	"UNS_16\000"
.LASF74:
	.ascii	"LIGHTS_BB_STRUCT\000"
.LASF141:
	.ascii	"stop_key_record\000"
.LASF73:
	.ascii	"lights\000"
.LASF14:
	.ascii	"UNS_8\000"
.LASF199:
	.ascii	"irri_lights_recursive_MUTEX\000"
.LASF156:
	.ascii	"array_index\000"
.LASF167:
	.ascii	"IRRI_LIGHTS_get_ptr_to_energized_light\000"
.LASF151:
	.ascii	"IRRI_COMM\000"
.LASF106:
	.ascii	"device_exchange_state\000"
.LASF66:
	.ascii	"no_current_output\000"
.LASF11:
	.ascii	"xSemaphoreHandle\000"
.LASF195:
	.ascii	"irri_lights\000"
.LASF127:
	.ascii	"TEST_AND_MOBILE_KICK_OFF_STRUCT\000"
.LASF155:
	.ascii	"output_index\000"
.LASF90:
	.ascii	"since_the_scan__master__number_of_clean_tokens_made"
	.ascii	"\000"
.LASF51:
	.ascii	"box_index_0\000"
.LASF27:
	.ascii	"pPrev\000"
.LASF37:
	.ascii	"stations_removed_for_this_reason\000"
.LASF72:
	.ascii	"verify_string_pre\000"
.LASF19:
	.ascii	"BITFIELD_BOOL\000"
.LASF193:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF128:
	.ascii	"manual_how\000"
.LASF146:
	.ascii	"send_box_configuration_to_master\000"
.LASF132:
	.ascii	"mvor_seconds\000"
.LASF113:
	.ascii	"token_in_transit\000"
.LASF116:
	.ascii	"changes\000"
.LASF33:
	.ascii	"stop_in_this_system_gid\000"
.LASF133:
	.ascii	"initiated_by\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
