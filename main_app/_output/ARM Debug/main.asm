	.file	"main.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.lcounter,"aw",%nobits
	.align	2
	.type	lcounter, %object
	.size	lcounter, 4
lcounter:
	.space	4
	.section	.text.toggle_LED,"ax",%progbits
	.align	2
	.type	toggle_LED, %function
toggle_LED:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/main.c"
	.loc 1 373 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-4]
	.loc 1 374 0
	ldr	r3, [fp, #-4]
	cmp	r3, #1
	bne	.L2
	.loc 1 376 0
	ldr	r3, .L7
	ldr	r3, [r3, #0]
	and	r3, r3, #4
	cmp	r3, #0
	beq	.L3
	.loc 1 378 0
	ldr	r3, .L7+4
	mov	r2, #4
	str	r2, [r3, #0]
	b	.L4
.L3:
	.loc 1 382 0
	ldr	r3, .L7+8
	mov	r2, #4
	str	r2, [r3, #0]
.L4:
	.loc 1 410 0
	ldr	r3, .L7+12
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L7+12
	str	r2, [r3, #0]
	b	.L1
.L2:
	.loc 1 415 0
	ldr	r3, [fp, #-4]
	cmp	r3, #2
	bne	.L1
	.loc 1 417 0
	ldr	r3, .L7
	ldr	r3, [r3, #0]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L6
	.loc 1 419 0
	ldr	r3, .L7+4
	mov	r2, #2
	str	r2, [r3, #0]
	b	.L1
.L6:
	.loc 1 423 0
	ldr	r3, .L7+8
	mov	r2, #2
	str	r2, [r3, #0]
.L1:
	.loc 1 431 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L8:
	.align	2
.L7:
	.word	1073905740
	.word	1073905736
	.word	1073905732
	.word	lcounter
.LFE0:
	.size	toggle_LED, .-toggle_LED
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/main"
	.ascii	".c\000"
	.section	.text.vled_TASK,"ax",%progbits
	.align	2
	.global	vled_TASK
	.type	vled_TASK, %function
vled_TASK:
.LFB1:
	.loc 1 441 0
	@ args = 0, pretend = 0, frame = 664
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI3:
	add	fp, sp, #8
.LCFI4:
	sub	sp, sp, #664
.LCFI5:
	str	r0, [fp, #-672]
	.loc 1 444 0
	ldr	r3, [fp, #-672]
	str	r3, [fp, #-48]
	b	.L52
.L53:
	.loc 1 773 0
	mov	r0, r0	@ nop
.L52:
	.loc 1 539 0
	mov	r0, #2
	bl	toggle_LED
	.loc 1 543 0
	sub	r3, fp, #204
	str	r3, [fp, #-12]
	.loc 1 545 0
	mov	r3, #256
	str	r3, [fp, #-36]
	.loc 1 547 0
	mov	r3, #24
	str	r3, [fp, #-40]
	.loc 1 549 0
	mov	r3, #256
	str	r3, [fp, #-44]
	.loc 1 552 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	bne	.L10
	.loc 1 554 0
	sub	r3, fp, #332
	str	r3, [fp, #-12]
	.loc 1 556 0
	mov	r3, #320
	str	r3, [fp, #-36]
	.loc 1 558 0
	mov	r3, #16
	str	r3, [fp, #-40]
	.loc 1 560 0
	mov	r3, #320
	str	r3, [fp, #-44]
	b	.L11
.L10:
	.loc 1 563 0
	ldr	r3, [fp, #-48]
	cmp	r3, #3
	bne	.L12
	.loc 1 565 0
	sub	r3, fp, #460
	str	r3, [fp, #-12]
	.loc 1 567 0
	mov	r3, #384
	str	r3, [fp, #-36]
	.loc 1 569 0
	mov	r3, #32
	str	r3, [fp, #-40]
	.loc 1 571 0
	mov	r3, #896
	str	r3, [fp, #-44]
	b	.L11
.L12:
	.loc 1 574 0
	ldr	r3, [fp, #-48]
	cmp	r3, #4
	bne	.L11
	.loc 1 576 0
	sub	r3, fp, #588
	str	r3, [fp, #-12]
	.loc 1 578 0
	mov	r3, #512
	str	r3, [fp, #-36]
	.loc 1 580 0
	mov	r3, #64
	str	r3, [fp, #-40]
	.loc 1 582 0
	mov	r3, #1104
	str	r3, [fp, #-44]
.L11:
	.loc 1 589 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L13
.L18:
	.loc 1 591 0
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	sub	r2, fp, #668
	add	r3, r2, r3
	mov	r0, r3
	mov	r1, #0
	bl	nm_ListInit
	.loc 1 593 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L14
.L17:
	.loc 1 595 0
	ldr	r0, [fp, #-44]
	ldr	r1, .L54+32
	ldr	r2, .L54+36
	bl	mem_malloc_debug
	str	r0, [fp, #-28]
	.loc 1 597 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #12
	str	r3, [fp, #-32]
	.loc 1 599 0
	mov	r3, #0
	str	r3, [fp, #-24]
	b	.L15
.L16:
	.loc 1 602 0 discriminator 2
	ldr	r2, [fp, #-32]
	ldr	r3, [fp, #-48]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-32]
	str	r2, [r3, #0]
	.loc 1 604 0 discriminator 2
	ldr	r3, [fp, #-32]
	add	r3, r3, #4
	str	r3, [fp, #-32]
	.loc 1 599 0 discriminator 2
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
.L15:
	.loc 1 599 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-44]
	sub	r3, r3, #12
	mov	r2, r3, lsr #2
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bhi	.L16
	.loc 1 607 0 is_stmt 1
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	sub	r2, fp, #668
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, [fp, #-28]
	bl	nm_ListInsertTail
	.loc 1 593 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L14:
	.loc 1 593 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-40]
	cmp	r2, r3
	bcc	.L17
	.loc 1 589 0 is_stmt 1
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L13:
	.loc 1 589 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #3
	bls	.L18
	.loc 1 611 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L19
.L28:
	.loc 1 613 0
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	sub	r2, fp, #668
	add	r3, r2, r3
	mov	r0, r3
	bl	nm_ListGetFirst
	str	r0, [fp, #-28]
	.loc 1 615 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 617 0
	b	.L20
.L25:
	.loc 1 619 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
	.loc 1 621 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #12
	str	r3, [fp, #-32]
	.loc 1 623 0
	mov	r3, #0
	str	r3, [fp, #-24]
	b	.L21
.L24:
	.loc 1 626 0
	ldr	r3, [fp, #-32]
	ldr	r2, [r3, #0]
	ldr	r1, [fp, #-32]
	ldr	r3, [fp, #-48]
	rsb	r3, r3, r1
	cmp	r2, r3
	beq	.L22
.L23:
	.loc 1 628 0 discriminator 1
	b	.L23
.L22:
	.loc 1 631 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #4
	str	r3, [fp, #-32]
	.loc 1 623 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
.L21:
	.loc 1 623 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-44]
	sub	r3, r3, #12
	mov	r2, r3, lsr #2
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bhi	.L24
	.loc 1 634 0 is_stmt 1
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-52]
	.loc 1 636 0
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	sub	r2, fp, #668
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, [fp, #-28]
	bl	nm_ListGetNext
	str	r0, [fp, #-28]
	.loc 1 638 0
	ldr	r2, [fp, #-16]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	sub	r2, fp, #668
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, [fp, #-52]
	ldr	r2, .L54+32
	ldr	r3, .L54+40
	bl	nm_ListRemove_debug
	.loc 1 640 0
	ldr	r0, [fp, #-52]
	ldr	r1, .L54+32
	mov	r2, #640
	bl	mem_free_debug
.L20:
	.loc 1 617 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L25
	.loc 1 643 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-40]
	cmp	r2, r3
	beq	.L26
.L27:
	.loc 1 645 0 discriminator 1
	b	.L27
.L26:
	.loc 1 611 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L19:
	.loc 1 611 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #3
	bls	.L28
	.loc 1 654 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L29
.L32:
	.loc 1 656 0
	ldr	r0, [fp, #-36]
	ldr	r1, .L54+32
	mov	r2, #656
	bl	mem_malloc_debug
	mov	r2, r0
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 658 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-28]
	.loc 1 660 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L30
.L31:
	.loc 1 663 0 discriminator 2
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-48]
	add	r2, r2, r3
	ldr	r3, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 665 0 discriminator 2
	ldr	r3, [fp, #-28]
	add	r3, r3, #4
	str	r3, [fp, #-28]
	.loc 1 660 0 discriminator 2
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L30:
	.loc 1 660 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	mov	r2, r3, lsr #2
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bhi	.L31
	.loc 1 668 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #8
	str	r3, [fp, #-12]
	.loc 1 654 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L29:
	.loc 1 654 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #15
	bls	.L32
	.loc 1 672 0 is_stmt 1
	ldr	r3, [fp, #-48]
	cmp	r3, #1
	bne	.L33
	.loc 1 674 0
	sub	r3, fp, #204
	str	r3, [fp, #-12]
	b	.L34
.L33:
	.loc 1 677 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	bne	.L35
	.loc 1 679 0
	sub	r3, fp, #332
	str	r3, [fp, #-12]
	b	.L34
.L35:
	.loc 1 682 0
	ldr	r3, [fp, #-48]
	cmp	r3, #3
	bne	.L36
	.loc 1 684 0
	sub	r3, fp, #460
	str	r3, [fp, #-12]
	b	.L34
.L36:
	.loc 1 687 0
	ldr	r3, [fp, #-48]
	cmp	r3, #4
	bne	.L34
	.loc 1 689 0
	sub	r3, fp, #588
	str	r3, [fp, #-12]
.L34:
	.loc 1 692 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L37
.L42:
	.loc 1 694 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-28]
	.loc 1 696 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L38
.L41:
	.loc 1 699 0
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #0]
	ldr	r1, [fp, #-28]
	ldr	r3, [fp, #-48]
	add	r3, r1, r3
	cmp	r2, r3
	beq	.L39
.L40:
	.loc 1 701 0 discriminator 1
	b	.L40
.L39:
	.loc 1 704 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #4
	str	r3, [fp, #-28]
	.loc 1 696 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L38:
	.loc 1 696 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	mov	r2, r3, lsr #2
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bhi	.L41
	.loc 1 707 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, .L54+32
	ldr	r2, .L54+44
	bl	mem_free_debug
	.loc 1 709 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #8
	str	r3, [fp, #-12]
	.loc 1 692 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L37:
	.loc 1 692 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #15
	bls	.L42
	.loc 1 718 0 is_stmt 1
	ldr	r3, [fp, #-48]
	cmp	r3, #1
	bne	.L43
	.loc 1 720 0
	mov	r0, #1
	bl	toggle_LED
	.loc 1 722 0
	ldr	r3, .L54+48	@ float
	str	r3, [fp, #-56]	@ float
	.loc 1 724 0
	ldr	r3, .L54+52	@ float
	str	r3, [fp, #-60]	@ float
	.loc 1 726 0
	flds	s14, [fp, #-56]
	flds	s15, [fp, #-60]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-60]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-60]
	.loc 1 729 0
	adr	r4, .L54
	ldmia	r4, {r3-r4}
	str	r3, [fp, #-68]
	str	r4, [fp, #-64]
	.loc 1 731 0
	adr	r4, .L54+8
	ldmia	r4, {r3-r4}
	str	r3, [fp, #-76]
	str	r4, [fp, #-72]
	.loc 1 733 0
	fldd	d6, [fp, #-68]
	fldd	d7, [fp, #-76]
	fmuld	d6, d6, d7
	fldd	d7, [fp, #-76]
	fdivd	d7, d6, d7
	fstd	d7, [fp, #-76]
	.loc 1 735 0
	flds	s14, [fp, #-60]
	flds	s15, [fp, #-56]
	fcmps	s14, s15
	fmstat
	beq	.L44
.L45:
	.loc 1 737 0 discriminator 1
	b	.L45
.L44:
	.loc 1 740 0
	fldd	d6, [fp, #-76]
	fldd	d7, [fp, #-68]
	fcmpd	d6, d7
	fmstat
	beq	.L46
.L47:
	.loc 1 742 0 discriminator 1
	b	.L47
.L46:
	.loc 1 745 0
	mov	r0, #1
	bl	toggle_LED
	.loc 1 773 0
	b	.L53
.L43:
	.loc 1 748 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	bne	.L53
	.loc 1 750 0
	ldr	r3, .L54+56	@ float
	str	r3, [fp, #-56]	@ float
	.loc 1 752 0
	ldr	r3, .L54+60	@ float
	str	r3, [fp, #-60]	@ float
	.loc 1 754 0
	flds	s14, [fp, #-56]
	flds	s15, [fp, #-60]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-60]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-60]
	.loc 1 757 0
	adr	r4, .L54+16
	ldmia	r4, {r3-r4}
	str	r3, [fp, #-68]
	str	r4, [fp, #-64]
	.loc 1 759 0
	adr	r4, .L54+24
	ldmia	r4, {r3-r4}
	str	r3, [fp, #-76]
	str	r4, [fp, #-72]
	.loc 1 761 0
	fldd	d6, [fp, #-68]
	fldd	d7, [fp, #-76]
	fmuld	d6, d6, d7
	fldd	d7, [fp, #-76]
	fdivd	d7, d6, d7
	fstd	d7, [fp, #-76]
	.loc 1 763 0
	flds	s14, [fp, #-60]
	flds	s15, [fp, #-56]
	fcmps	s14, s15
	fmstat
	beq	.L49
.L50:
	.loc 1 765 0 discriminator 1
	b	.L50
.L49:
	.loc 1 768 0
	fldd	d6, [fp, #-76]
	fldd	d7, [fp, #-68]
	fcmpd	d6, d7
	fmstat
	beq	.L53
.L51:
	.loc 1 770 0 discriminator 1
	b	.L51
.L55:
	.align	2
.L54:
	.word	-1431655766
	.word	1075882666
	.word	0
	.word	1073479680
	.word	-1431655766
	.word	1076013738
	.word	0
	.word	1074135040
	.word	.LC0
	.word	595
	.word	638
	.word	707
	.word	1090868566
	.word	1071644672
	.word	1091917142
	.word	1076887552
.LFE1:
	.size	vled_TASK, .-vled_TASK
	.section .rodata
	.align	2
.LC1:
	.ascii	"Startup\000"
	.section	.text.main,"ax",%progbits
	.align	2
	.global	main
	.type	main, %function
main:
.LFB2:
	.loc 1 1071 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #16
.LCFI8:
	.loc 1 1222 0
	ldr	r3, .L58
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 1224 0
	ldr	r3, .L58+4
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 1226 0
	ldr	r3, .L58+8
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 1228 0
	ldr	r3, .L58+12
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 1239 0
	bl	init_mem_partitioning
	.loc 1 1241 0
	bl	init_mem_debug
	.loc 1 1257 0
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L58+16
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r3, #0
	str	r3, [sp, #12]
	ldr	r0, .L58+20
	ldr	r1, .L58+24
	mov	r2, #1024
	mov	r3, #0
	bl	xTaskGenericCreate
	.loc 1 1259 0
	ldr	r0, .L58+28
	bl	vPortSaveVFPRegisters
	ldr	r3, .L58+16
	ldr	r2, [r3, #0]
	ldr	r3, .L58+28
	mov	r0, r2
	mov	r1, r3
	bl	vTaskSetApplicationTaskTag
	.loc 1 1263 0
	bl	vTaskStartScheduler
.L57:
	.loc 1 1268 0 discriminator 1
	b	.L57
.L59:
	.align	2
.L58:
	.word	alerts_struct_user
	.word	alerts_struct_changes
	.word	alerts_struct_tp_micro
	.word	alerts_struct_engineering
	.word	startup_task_handle
	.word	system_startup_task
	.word	.LC1
	.word	startup_task_FLOP_registers
.LFE2:
	.size	main, .-main
	.section	.text.vApplicationStackOverflowHook,"ax",%progbits
	.align	2
	.global	vApplicationStackOverflowHook
	.type	vApplicationStackOverflowHook, %function
vApplicationStackOverflowHook:
.LFB3:
	.loc 1 1278 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1293 0
	bl	vTaskSuspendAll
.L61:
	.loc 1 1296 0 discriminator 1
	b	.L61
.LFE3:
	.size	vApplicationStackOverflowHook, .-vApplicationStackOverflowHook
	.section	.text.vApplicationMallocFailedHook,"ax",%progbits
	.align	2
	.global	vApplicationMallocFailedHook
	.type	vApplicationMallocFailedHook, %function
vApplicationMallocFailedHook:
.LFB4:
	.loc 1 1331 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI12:
	add	fp, sp, #0
.LCFI13:
.L63:
	.loc 1 1332 0 discriminator 1
	b	.L63
.LFE4:
	.size	vApplicationMallocFailedHook, .-vApplicationMallocFailedHook
	.section	.text.vConfigureTimerForRunTimeStats,"ax",%progbits
	.align	2
	.global	vConfigureTimerForRunTimeStats
	.type	vConfigureTimerForRunTimeStats, %function
vConfigureTimerForRunTimeStats:
.LFB5:
	.loc 1 1338 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI14:
	add	fp, sp, #4
.LCFI15:
	sub	sp, sp, #4
.LCFI16:
	.loc 1 1341 0
	ldr	r3, .L65
	str	r3, [fp, #-8]
	.loc 1 1344 0
	mov	r0, #18
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 1347 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 1348 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #112]
	.loc 1 1349 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #40]
	.loc 1 1350 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #60]
	.loc 1 1353 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 1354 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #16]
	.loc 1 1356 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #24]
	.loc 1 1357 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 1358 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 1359 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #36]
	.loc 1 1361 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L65+4
	str	r2, [r3, #12]
	.loc 1 1362 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #20]
	.loc 1 1364 0
	ldr	r3, [fp, #-8]
	mov	r2, #255
	str	r2, [r3, #0]
	.loc 1 1366 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #4]
	.loc 1 1367 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L66:
	.align	2
.L65:
	.word	1074135040
	.word	650
.LFE5:
	.size	vConfigureTimerForRunTimeStats, .-vConfigureTimerForRunTimeStats
	.section	.text.return_run_timer_counter_value,"ax",%progbits
	.align	2
	.global	return_run_timer_counter_value
	.type	return_run_timer_counter_value, %function
return_run_timer_counter_value:
.LFB6:
	.loc 1 1370 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI17:
	add	fp, sp, #0
.LCFI18:
	sub	sp, sp, #4
.LCFI19:
	.loc 1 1373 0
	ldr	r3, .L68
	str	r3, [fp, #-4]
	.loc 1 1375 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #8]
	.loc 1 1376 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L69:
	.align	2
.L68:
	.word	1074135040
.LFE6:
	.size	return_run_timer_counter_value, .-return_run_timer_counter_value
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI14-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI17-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/task.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7b8
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF105
	.byte	0x1
	.4byte	.LASF106
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x4
	.byte	0x4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x2
	.byte	0x63
	.4byte	0x3a
	.uleb128 0x6
	.4byte	0x51
	.4byte	0x81
	.uleb128 0x7
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF8
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x3
	.byte	0x3a
	.4byte	0x51
	.uleb128 0x5
	.4byte	.LASF11
	.byte	0x3
	.byte	0x5e
	.4byte	0x9e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x5
	.4byte	.LASF13
	.byte	0x3
	.byte	0x99
	.4byte	0x9e
	.uleb128 0x8
	.byte	0x14
	.byte	0x4
	.byte	0x18
	.4byte	0xff
	.uleb128 0x9
	.4byte	.LASF14
	.byte	0x4
	.byte	0x1a
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF15
	.byte	0x4
	.byte	0x1c
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x4
	.byte	0x1e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x4
	.byte	0x20
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x4
	.byte	0x23
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x5
	.4byte	.LASF19
	.byte	0x4
	.byte	0x26
	.4byte	0xb0
	.uleb128 0xa
	.4byte	0x93
	.uleb128 0x6
	.4byte	0x93
	.4byte	0x11f
	.uleb128 0x7
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.byte	0x5
	.byte	0x14
	.4byte	0x144
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x5
	.byte	0x17
	.4byte	0x144
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x5
	.byte	0x1a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x88
	.uleb128 0x5
	.4byte	.LASF22
	.byte	0x5
	.byte	0x1c
	.4byte	0x11f
	.uleb128 0x6
	.4byte	0x81
	.4byte	0x165
	.uleb128 0x7
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF23
	.uleb128 0x6
	.4byte	0x93
	.4byte	0x17c
	.uleb128 0x7
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x6
	.4byte	0x93
	.4byte	0x18c
	.uleb128 0x7
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.byte	0x34
	.byte	0x6
	.byte	0x58
	.4byte	0x221
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x6
	.byte	0x68
	.4byte	0x155
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x6
	.byte	0x6d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x6
	.byte	0x74
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x6
	.byte	0x77
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x6
	.byte	0x7b
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x6
	.byte	0x81
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x6
	.byte	0x8a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x6
	.byte	0x8f
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x6
	.byte	0x9b
	.4byte	0xa5
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x6
	.byte	0xa2
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.byte	0
	.uleb128 0x5
	.4byte	.LASF33
	.byte	0x6
	.byte	0xaa
	.4byte	0x18c
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF34
	.uleb128 0xc
	.byte	0x4
	.byte	0xb
	.byte	0x24
	.4byte	0x314
	.uleb128 0xd
	.4byte	.LASF35
	.sleb128 0
	.uleb128 0xd
	.4byte	.LASF36
	.sleb128 0
	.uleb128 0xd
	.4byte	.LASF37
	.sleb128 1
	.uleb128 0xd
	.4byte	.LASF38
	.sleb128 2
	.uleb128 0xd
	.4byte	.LASF39
	.sleb128 3
	.uleb128 0xd
	.4byte	.LASF40
	.sleb128 4
	.uleb128 0xd
	.4byte	.LASF41
	.sleb128 5
	.uleb128 0xd
	.4byte	.LASF42
	.sleb128 6
	.uleb128 0xd
	.4byte	.LASF43
	.sleb128 7
	.uleb128 0xd
	.4byte	.LASF44
	.sleb128 8
	.uleb128 0xd
	.4byte	.LASF45
	.sleb128 9
	.uleb128 0xd
	.4byte	.LASF46
	.sleb128 10
	.uleb128 0xd
	.4byte	.LASF47
	.sleb128 11
	.uleb128 0xd
	.4byte	.LASF48
	.sleb128 12
	.uleb128 0xd
	.4byte	.LASF49
	.sleb128 13
	.uleb128 0xd
	.4byte	.LASF50
	.sleb128 14
	.uleb128 0xd
	.4byte	.LASF51
	.sleb128 15
	.uleb128 0xd
	.4byte	.LASF52
	.sleb128 16
	.uleb128 0xd
	.4byte	.LASF53
	.sleb128 17
	.uleb128 0xd
	.4byte	.LASF54
	.sleb128 18
	.uleb128 0xd
	.4byte	.LASF55
	.sleb128 19
	.uleb128 0xd
	.4byte	.LASF56
	.sleb128 20
	.uleb128 0xd
	.4byte	.LASF57
	.sleb128 21
	.uleb128 0xd
	.4byte	.LASF58
	.sleb128 22
	.uleb128 0xd
	.4byte	.LASF59
	.sleb128 23
	.uleb128 0xd
	.4byte	.LASF60
	.sleb128 24
	.uleb128 0xd
	.4byte	.LASF61
	.sleb128 25
	.uleb128 0xd
	.4byte	.LASF62
	.sleb128 26
	.uleb128 0xd
	.4byte	.LASF63
	.sleb128 27
	.uleb128 0xd
	.4byte	.LASF64
	.sleb128 28
	.uleb128 0xd
	.4byte	.LASF65
	.sleb128 29
	.uleb128 0xd
	.4byte	.LASF66
	.sleb128 30
	.uleb128 0xd
	.4byte	.LASF67
	.sleb128 31
	.uleb128 0xd
	.4byte	.LASF68
	.sleb128 32
	.uleb128 0xd
	.4byte	.LASF69
	.sleb128 33
	.uleb128 0xd
	.4byte	.LASF70
	.sleb128 34
	.byte	0
	.uleb128 0x8
	.byte	0x74
	.byte	0x7
	.byte	0x28
	.4byte	0x3bf
	.uleb128 0xe
	.ascii	"ir\000"
	.byte	0x7
	.byte	0x2a
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.ascii	"tcr\000"
	.byte	0x7
	.byte	0x2b
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.ascii	"tc\000"
	.byte	0x7
	.byte	0x2c
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.ascii	"pr\000"
	.byte	0x7
	.byte	0x2d
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.ascii	"pc\000"
	.byte	0x7
	.byte	0x2e
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.ascii	"mcr\000"
	.byte	0x7
	.byte	0x2f
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.ascii	"mr\000"
	.byte	0x7
	.byte	0x30
	.4byte	0x3bf
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.ascii	"ccr\000"
	.byte	0x7
	.byte	0x31
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xe
	.ascii	"cr\000"
	.byte	0x7
	.byte	0x32
	.4byte	0x3c4
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xe
	.ascii	"emr\000"
	.byte	0x7
	.byte	0x33
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x9
	.4byte	.LASF71
	.byte	0x7
	.byte	0x34
	.4byte	0x3c9
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x9
	.4byte	.LASF72
	.byte	0x7
	.byte	0x35
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.byte	0
	.uleb128 0xa
	.4byte	0x17c
	.uleb128 0xa
	.4byte	0x17c
	.uleb128 0xa
	.4byte	0x16c
	.uleb128 0x5
	.4byte	.LASF73
	.byte	0x7
	.byte	0x36
	.4byte	0x314
	.uleb128 0xf
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x174
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x402
	.uleb128 0x10
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x174
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x1b8
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x55a
	.uleb128 0x10
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x1b8
	.4byte	0x3a
	.byte	0x3
	.byte	0x91
	.sleb128 -676
	.uleb128 0x12
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x1ba
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x13
	.ascii	"sf1\000"
	.byte	0x1
	.2byte	0x1f1
	.4byte	0x55a
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x13
	.ascii	"sf2\000"
	.byte	0x1
	.2byte	0x1f1
	.4byte	0x55a
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x13
	.ascii	"df1\000"
	.byte	0x1
	.2byte	0x1f3
	.4byte	0x55f
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x13
	.ascii	"df2\000"
	.byte	0x1
	.2byte	0x1f3
	.4byte	0x55f
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x13
	.ascii	"dh1\000"
	.byte	0x1
	.2byte	0x1fd
	.4byte	0x564
	.byte	0x3
	.byte	0x91
	.sleb128 -208
	.uleb128 0x13
	.ascii	"dh2\000"
	.byte	0x1
	.2byte	0x1fe
	.4byte	0x564
	.byte	0x3
	.byte	0x91
	.sleb128 -336
	.uleb128 0x13
	.ascii	"dh3\000"
	.byte	0x1
	.2byte	0x1ff
	.4byte	0x564
	.byte	0x3
	.byte	0x91
	.sleb128 -464
	.uleb128 0x13
	.ascii	"dh4\000"
	.byte	0x1
	.2byte	0x200
	.4byte	0x564
	.byte	0x3
	.byte	0x91
	.sleb128 -592
	.uleb128 0x12
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x202
	.4byte	0x574
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x204
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.ascii	"j\000"
	.byte	0x1
	.2byte	0x204
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.ascii	"k\000"
	.byte	0x1
	.2byte	0x204
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x204
	.4byte	0x57a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x204
	.4byte	0x57a
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x12
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x204
	.4byte	0x57a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x12
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x204
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x12
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x204
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x12
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x204
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x12
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x215
	.4byte	0x580
	.byte	0x3
	.byte	0x91
	.sleb128 -672
	.byte	0
	.uleb128 0xa
	.4byte	0x165
	.uleb128 0xa
	.4byte	0x22c
	.uleb128 0x6
	.4byte	0x14a
	.4byte	0x574
	.uleb128 0x7
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x14a
	.uleb128 0xb
	.byte	0x4
	.4byte	0x93
	.uleb128 0x6
	.4byte	0xff
	.4byte	0x590
	.uleb128 0x7
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x42e
	.byte	0x1
	.4byte	0x2c
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x5c0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x4eb
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	0x3a
	.byte	0
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x4fd
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x5f9
	.uleb128 0x10
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x4fd
	.4byte	0x5f9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x4fd
	.4byte	0x5ff
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x66
	.uleb128 0xb
	.byte	0x4
	.4byte	0x3c
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x532
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x539
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x645
	.uleb128 0x12
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x53b
	.4byte	0x645
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x3ce
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x559
	.byte	0x1
	.4byte	0x25
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x679
	.uleb128 0x12
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x55b
	.4byte	0x645
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x18
	.4byte	.LASF92
	.byte	0x8
	.byte	0x30
	.4byte	0x68a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x19
	.4byte	0x71
	.uleb128 0x18
	.4byte	.LASF93
	.byte	0x8
	.byte	0x34
	.4byte	0x6a0
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x19
	.4byte	0x71
	.uleb128 0x18
	.4byte	.LASF94
	.byte	0x8
	.byte	0x36
	.4byte	0x6b6
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x19
	.4byte	0x71
	.uleb128 0x18
	.4byte	.LASF95
	.byte	0x8
	.byte	0x38
	.4byte	0x6cc
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x19
	.4byte	0x71
	.uleb128 0x6
	.4byte	0x93
	.4byte	0x6e1
	.uleb128 0x7
	.4byte	0x25
	.byte	0x20
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF96
	.byte	0x9
	.byte	0x3d
	.4byte	0x6d1
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF97
	.byte	0x9
	.byte	0x48
	.4byte	0x66
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF98
	.byte	0xa
	.byte	0x33
	.4byte	0x70c
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x19
	.4byte	0x10f
	.uleb128 0x18
	.4byte	.LASF99
	.byte	0xa
	.byte	0x3f
	.4byte	0x722
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x19
	.4byte	0x17c
	.uleb128 0x1a
	.4byte	.LASF100
	.byte	0x6
	.byte	0xad
	.4byte	0x221
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF101
	.byte	0x6
	.byte	0xae
	.4byte	0x221
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF102
	.byte	0x6
	.byte	0xaf
	.4byte	0x221
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF103
	.byte	0x6
	.byte	0xb0
	.4byte	0x221
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x172
	.4byte	0x93
	.byte	0x5
	.byte	0x3
	.4byte	lcounter
	.uleb128 0x1a
	.4byte	.LASF96
	.byte	0x9
	.byte	0x3d
	.4byte	0x6d1
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF97
	.byte	0x9
	.byte	0x48
	.4byte	0x66
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF100
	.byte	0x6
	.byte	0xad
	.4byte	0x221
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF101
	.byte	0x6
	.byte	0xae
	.4byte	0x221
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF102
	.byte	0x6
	.byte	0xaf
	.4byte	0x221
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF103
	.byte	0x6
	.byte	0xb0
	.4byte	0x221
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI18
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF16:
	.ascii	"count\000"
.LASF81:
	.ascii	"list_length\000"
.LASF102:
	.ascii	"alerts_struct_tp_micro\000"
.LASF107:
	.ascii	"toggle_LED\000"
.LASF40:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF52:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF94:
	.ascii	"GuiFont_DecimalChar\000"
.LASF56:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF64:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF108:
	.ascii	"vPortSaveVFPRegisters\000"
.LASF100:
	.ascii	"alerts_struct_user\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF14:
	.ascii	"phead\000"
.LASF74:
	.ascii	"pled\000"
.LASF27:
	.ascii	"next\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF42:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF6:
	.ascii	"long long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF35:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF9:
	.ascii	"xTaskHandle\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF18:
	.ascii	"InUse\000"
.LASF79:
	.ascii	"tmp32_ptr\000"
.LASF4:
	.ascii	"long int\000"
.LASF25:
	.ascii	"alerts_pile_index\000"
.LASF61:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF68:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF84:
	.ascii	"vled_TASK\000"
.LASF48:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF33:
	.ascii	"ALERTS_PILE_STRUCT\000"
.LASF30:
	.ascii	"pending_first_to_send\000"
.LASF34:
	.ascii	"double\000"
.LASF19:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF80:
	.ascii	"test_size\000"
.LASF78:
	.ascii	"uns32_ptr\000"
.LASF95:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF88:
	.ascii	"vConfigureTimerForRunTimeStats\000"
.LASF86:
	.ascii	"pxTask\000"
.LASF91:
	.ascii	"return_run_timer_counter_value\000"
.LASF54:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF45:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF63:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF22:
	.ascii	"DATA_HANDLE\000"
.LASF75:
	.ascii	"pvParameters\000"
.LASF85:
	.ascii	"vApplicationStackOverflowHook\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF105:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF37:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF70:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF57:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF50:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF36:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF65:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF93:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF76:
	.ascii	"task_param\000"
.LASF46:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF83:
	.ascii	"tlist_hdr\000"
.LASF38:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF66:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF15:
	.ascii	"ptail\000"
.LASF58:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF41:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF82:
	.ascii	"list_element_size\000"
.LASF73:
	.ascii	"TIMER_CNTR_REGS_T\000"
.LASF23:
	.ascii	"float\000"
.LASF92:
	.ascii	"GuiFont_LanguageActive\000"
.LASF21:
	.ascii	"dlen\000"
.LASF96:
	.ascii	"startup_task_FLOP_registers\000"
.LASF32:
	.ascii	"ci_duration_ms\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF3:
	.ascii	"short int\000"
.LASF106:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/main"
	.ascii	".c\000"
.LASF71:
	.ascii	"rsvd2\000"
.LASF97:
	.ascii	"startup_task_handle\000"
.LASF55:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF89:
	.ascii	"ltimer\000"
.LASF29:
	.ascii	"first_to_send\000"
.LASF20:
	.ascii	"dptr\000"
.LASF103:
	.ascii	"alerts_struct_engineering\000"
.LASF62:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF26:
	.ascii	"first\000"
.LASF8:
	.ascii	"char\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF31:
	.ascii	"pending_first_to_send_in_use\000"
.LASF43:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF77:
	.ascii	"dh_ptr\000"
.LASF51:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF17:
	.ascii	"offset\000"
.LASF60:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF69:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF99:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF47:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF72:
	.ascii	"ctcr\000"
.LASF104:
	.ascii	"lcounter\000"
.LASF39:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF67:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF10:
	.ascii	"UNS_8\000"
.LASF87:
	.ascii	"pcTaskName\000"
.LASF101:
	.ascii	"alerts_struct_changes\000"
.LASF28:
	.ascii	"ready_to_use_bool\000"
.LASF49:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF59:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF109:
	.ascii	"vApplicationMallocFailedHook\000"
.LASF24:
	.ascii	"verify_string_pre\000"
.LASF98:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF90:
	.ascii	"main\000"
.LASF44:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF53:
	.ascii	"CLKPWR_WDOG_CLK\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
