	.file	"lcd_init.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	guilib_display_ptr
	.section	.bss.guilib_display_ptr,"aw",%nobits
	.align	2
	.type	guilib_display_ptr, %object
	.size	guilib_display_ptr, 4
guilib_display_ptr:
	.space	4
	.global	gui_lib_display_buf
	.section	.bss.gui_lib_display_buf,"aw",%nobits
	.align	3
	.type	gui_lib_display_buf, %object
	.size	gui_lib_display_buf, 38400
gui_lib_display_buf:
	.space	38400
	.global	primary_display_buf
	.section	.bss.primary_display_buf,"aw",%nobits
	.align	3
	.type	primary_display_buf, %object
	.size	primary_display_buf, 38400
primary_display_buf:
	.space	38400
	.global	utility_display_buf
	.section	.bss.utility_display_buf,"aw",%nobits
	.align	3
	.type	utility_display_buf, %object
	.size	utility_display_buf, 245760
utility_display_buf:
	.space	245760
	.section	.text.led_backlight_set_brightness,"ax",%progbits
	.align	2
	.global	led_backlight_set_brightness
	.type	led_backlight_set_brightness, %function
led_backlight_set_brightness:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/lcd_init.c"
	.loc 1 74 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	.loc 1 84 0
	ldr	r3, .L2
	str	r3, [fp, #-8]
	.loc 1 89 0
	bl	vTaskSuspendAll
	.loc 1 95 0
	bl	CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty
	str	r0, [fp, #-12]
	.loc 1 97 0
	ldr	r3, [fp, #-12]
	rsb	r3, r3, #100
	mov	r2, r3, asl #8
	ldr	r3, [fp, #-12]
	rsb	r2, r3, r2
	ldr	r3, .L2+4
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #5
	and	r3, r3, #255
	orr	r3, r3, #-2147483648
	orr	r3, r3, #9472
	ldr	r2, [fp, #-8]
	str	r3, [r2, #0]
	.loc 1 101 0
	bl	xTaskResumeAll
	.loc 1 104 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L3:
	.align	2
.L2:
	.word	1074118660
	.word	1374389535
.LFE0:
	.size	led_backlight_set_brightness, .-led_backlight_set_brightness
	.section	.text.init_backlight_pwm,"ax",%progbits
	.align	2
	.type	init_backlight_pwm, %function
init_backlight_pwm:
.LFB1:
	.loc 1 108 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 115 0
	bl	vTaskSuspendAll
	.loc 1 120 0
	mov	r0, #14
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 123 0
	mov	r0, #2
	mov	r1, #1
	mov	r2, #2
	bl	clkpwr_setup_pwm
	.loc 1 127 0
	bl	xTaskResumeAll
	.loc 1 133 0
	bl	led_backlight_set_brightness
	.loc 1 134 0
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	init_backlight_pwm, .-init_backlight_pwm
	.section	.text.init_lcd_1,"ax",%progbits
	.align	2
	.global	init_lcd_1
	.type	init_lcd_1, %function
init_lcd_1:
.LFB2:
	.loc 1 138 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #4
.LCFI7:
	.loc 1 142 0
	ldr	r2, .L8
	ldr	r3, .L8
	ldr	r3, [r3, #24]
	bic	r3, r3, #2048
	bic	r3, r3, #1
	str	r3, [r2, #24]
	.loc 1 145 0
	mov	r0, #256
	mov	r1, #4
	bl	clkpwr_setup_lcd
	.loc 1 148 0
	mov	r0, #1
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 151 0
	ldr	r0, .L8+4
	mov	r1, #255
	mov	r2, #38400
	bl	memset
	.loc 1 152 0
	ldr	r0, .L8+8
	mov	r1, #255
	mov	r2, #245760
	bl	memset
	.loc 1 157 0
	ldr	r3, .L8+12
	ldr	r0, .L8
	mov	r1, r3
	bl	calsense_lcd_open
	str	r0, [fp, #-8]
	.loc 1 160 0
	ldr	r3, .L8+4
	ldr	r0, [fp, #-8]
	mov	r1, #3
	mov	r2, r3
	bl	lcd_ioctl
	.loc 1 163 0
	ldr	r3, .L8+16
	ldr	r3, [r3, #0]
	cmp	r3, #16
	bne	.L6
	.loc 1 166 0
	ldr	r3, .L8
	mov	r2, #131072
	str	r2, [r3, #512]
	.loc 1 167 0
	ldr	r3, .L8
	ldr	r2, .L8+20
	str	r2, [r3, #516]
	.loc 1 168 0
	ldr	r3, .L8
	ldr	r2, .L8+24
	str	r2, [r3, #520]
	.loc 1 169 0
	ldr	r3, .L8
	ldr	r2, .L8+28
	str	r2, [r3, #524]
	.loc 1 170 0
	ldr	r3, .L8
	ldr	r2, .L8+32
	str	r2, [r3, #528]
	.loc 1 171 0
	ldr	r3, .L8
	ldr	r2, .L8+36
	str	r2, [r3, #532]
	.loc 1 172 0
	ldr	r3, .L8
	ldr	r2, .L8+40
	str	r2, [r3, #536]
	.loc 1 173 0
	ldr	r3, .L8
	ldr	r2, .L8+44
	str	r2, [r3, #540]
	b	.L7
.L6:
	.loc 1 178 0
	ldr	r3, .L8
	ldr	r2, .L8+48
	str	r2, [r3, #512]
	.loc 1 179 0
	ldr	r3, .L8
	ldr	r2, .L8+48
	str	r2, [r3, #516]
	.loc 1 180 0
	ldr	r3, .L8
	ldr	r2, .L8+48
	str	r2, [r3, #520]
	.loc 1 181 0
	ldr	r3, .L8
	ldr	r2, .L8+48
	str	r2, [r3, #524]
	.loc 1 182 0
	ldr	r3, .L8
	mov	r2, #0
	str	r2, [r3, #528]
	.loc 1 183 0
	ldr	r3, .L8
	mov	r2, #0
	str	r2, [r3, #532]
	.loc 1 184 0
	ldr	r3, .L8
	mov	r2, #0
	str	r2, [r3, #536]
	.loc 1 185 0
	ldr	r3, .L8
	mov	r2, #0
	str	r2, [r3, #540]
.L7:
	.loc 1 192 0
	ldr	r3, .L8
	ldr	r2, .L8
	ldr	r2, [r2, #24]
	orr	r2, r2, #2048
	str	r2, [r3, #24]
	.loc 1 195 0
	ldr	r3, .L8
	ldr	r2, .L8
	ldr	r2, [r2, #24]
	orr	r2, r2, #1
	str	r2, [r3, #24]
	.loc 1 215 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	822345728
	.word	primary_display_buf
	.word	utility_display_buf
	.word	calsense_4bit_grey16
	.word	display_model_is
	.word	393220
	.word	655368
	.word	917516
	.word	1179664
	.word	1441812
	.word	1703960
	.word	1966108
	.word	1966110
.LFE2:
	.size	init_lcd_1, .-init_lcd_1
	.section	.text.init_lcd_2,"ax",%progbits
	.align	2
	.global	init_lcd_2
	.type	init_lcd_2, %function
init_lcd_2:
.LFB3:
	.loc 1 219 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	.loc 1 220 0
	bl	init_lcd_1
	.loc 1 224 0
	ldr	r0, .L11
	mov	r1, #0
	bl	postContrast_or_Volume_Event
	.loc 1 226 0
	bl	init_backlight_pwm
	.loc 1 228 0
	ldr	r3, .L11+4
	mov	r2, #16384
	str	r2, [r3, #0]
	.loc 1 229 0
	ldmfd	sp!, {fp, pc}
.L12:
	.align	2
.L11:
	.word	4369
	.word	1073905668
.LFE3:
	.size	init_lcd_2, .-init_lcd_2
	.section	.text.FDTO_set_guilib_to_write_to_the_utility_display_buffer,"ax",%progbits
	.align	2
	.type	FDTO_set_guilib_to_write_to_the_utility_display_buffer, %function
FDTO_set_guilib_to_write_to_the_utility_display_buffer:
.LFB4:
	.loc 1 249 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI10:
	add	fp, sp, #0
.LCFI11:
	sub	sp, sp, #4
.LCFI12:
	str	r0, [fp, #-4]
	.loc 1 250 0
	ldr	r2, [fp, #-4]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #5
	mov	r2, r3
	ldr	r3, .L14
	add	r2, r2, r3
	ldr	r3, .L14+4
	str	r2, [r3, #0]
	.loc 1 251 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L15:
	.align	2
.L14:
	.word	utility_display_buf
	.word	guilib_display_ptr
.LFE4:
	.size	FDTO_set_guilib_to_write_to_the_utility_display_buffer, .-FDTO_set_guilib_to_write_to_the_utility_display_buffer
	.section	.text.FDTO_set_guilib_to_write_to_the_primary_display_buffer,"ax",%progbits
	.align	2
	.global	FDTO_set_guilib_to_write_to_the_primary_display_buffer
	.type	FDTO_set_guilib_to_write_to_the_primary_display_buffer, %function
FDTO_set_guilib_to_write_to_the_primary_display_buffer:
.LFB5:
	.loc 1 267 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI13:
	add	fp, sp, #0
.LCFI14:
	.loc 1 270 0
	ldr	r3, .L17
	ldr	r2, .L17+4
	str	r2, [r3, #0]
	.loc 1 277 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L18:
	.align	2
.L17:
	.word	guilib_display_ptr
	.word	gui_lib_display_buf
.LFE5:
	.size	FDTO_set_guilib_to_write_to_the_primary_display_buffer, .-FDTO_set_guilib_to_write_to_the_primary_display_buffer
	.section .rodata
	.align	2
.LC0:
	.ascii	"Utility buffer too small: needs %lu; has %lu\000"
	.align	2
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/"
	.ascii	"lcd_init.c\000"
	.align	2
.LC2:
	.ascii	"Function call with invalid font : %s, %u\000"
	.section	.text.CS_DrawStr,"ax",%progbits
	.align	2
	.global	CS_DrawStr
	.type	CS_DrawStr, %function
CS_DrawStr:
.LFB6:
	.loc 1 345 0
	@ args = 48, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI15:
	add	fp, sp, #32
.LCFI16:
	sub	sp, sp, #76
.LCFI17:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	str	r2, [fp, #-52]
	str	r3, [fp, #-56]
	.loc 1 350 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 1 352 0
	ldr	r3, [fp, #-56]
	sub	r3, r3, #1
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L20
.L25:
	.word	.L21
	.word	.L22
	.word	.L20
	.word	.L20
	.word	.L23
	.word	.L24
.L21:
	.loc 1 355 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 356 0
	b	.L26
.L22:
	.loc 1 359 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 360 0
	b	.L26
.L23:
	.loc 1 363 0
	mov	r3, #17
	str	r3, [fp, #-36]
	.loc 1 364 0
	b	.L26
.L24:
	.loc 1 367 0
	mov	r3, #16
	str	r3, [fp, #-36]
	.loc 1 368 0
	b	.L26
.L20:
	.loc 1 371 0
	mov	r3, #0
	str	r3, [fp, #-36]
.L26:
	.loc 1 374 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L27
	.loc 1 376 0
	ldr	r2, [fp, #-36]
	ldr	r3, [fp, #-44]
	add	r3, r2, r3
	add	r2, r3, #1
	ldr	r3, .L30
	cmp	r2, r3
	bhi	.L28
	.loc 1 378 0
	ldr	r0, [fp, #-44]
	bl	FDTO_set_guilib_to_write_to_the_utility_display_buffer
	.loc 1 380 0
	ldr	r3, [fp, #-48]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r0, r3, asr #16
	.loc 1 381 0
	ldr	r3, [fp, #-52]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 380 0
	mov	r3, r3, asl #16
	mov	r1, r3, asr #16
	.loc 1 382 0
	ldr	r3, [fp, #-56]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 380 0
	mov	lr, r3
	.loc 1 383 0
	ldr	r3, [fp, #4]
	and	r3, r3, #255
	.loc 1 380 0
	mov	r3, r3, asl #24
	mov	ip, r3, asr #24
	.loc 1 385 0
	ldr	r3, [fp, #12]
	and	r3, r3, #255
	.loc 1 380 0
	str	r3, [fp, #-60]
	.loc 1 386 0
	ldr	r3, [fp, #16]
	and	r3, r3, #255
	.loc 1 380 0
	str	r3, [fp, #-64]
	.loc 1 387 0
	ldr	r3, [fp, #20]
	and	r3, r3, #255
	.loc 1 380 0
	mov	r9, r3
	.loc 1 388 0
	ldr	r3, [fp, #24]
	and	r3, r3, #255
	.loc 1 380 0
	mov	sl, r3
	.loc 1 389 0
	ldr	r3, [fp, #28]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 380 0
	mov	r3, r3, asl #16
	mov	r8, r3, asr #16
	.loc 1 390 0
	ldr	r3, [fp, #32]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 380 0
	mov	r3, r3, asl #16
	mov	r7, r3, asr #16
	.loc 1 391 0
	ldr	r3, [fp, #36]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 380 0
	mov	r3, r3, asl #16
	mov	r6, r3, asr #16
	.loc 1 392 0
	ldr	r3, [fp, #40]
	and	r3, r3, #255
	.loc 1 380 0
	mov	r5, r3
	.loc 1 393 0
	ldr	r3, [fp, #44]
	and	r3, r3, #255
	.loc 1 380 0
	mov	r4, r3
	.loc 1 394 0
	ldr	r3, [fp, #48]
	and	r3, r3, #255
	.loc 1 380 0
	mov	r2, r3
	ldr	r3, [fp, #8]
	str	r3, [sp, #0]
	ldr	r3, [fp, #-60]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-64]
	str	r3, [sp, #8]
	str	r9, [sp, #12]
	str	sl, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	str	r6, [sp, #28]
	str	r5, [sp, #32]
	str	r4, [sp, #36]
	str	r2, [sp, #40]
	mov	r2, lr
	mov	r3, ip
	bl	GuiLib_DrawStr
	.loc 1 396 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 398 0
	bl	FDTO_set_guilib_to_write_to_the_primary_display_buffer
	b	.L29
.L28:
	.loc 1 402 0
	ldr	r2, [fp, #-36]
	ldr	r3, [fp, #-44]
	add	r3, r2, r3
	add	r3, r3, #1
	ldr	r0, .L30+4
	mov	r1, r3
	mov	r2, #1536
	bl	Alert_Message_va
	b	.L29
.L27:
	.loc 1 407 0
	ldr	r0, .L30+8
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L30+12
	mov	r1, r3
	ldr	r2, .L30+16
	bl	Alert_Message_va
.L29:
	.loc 1 410 0
	ldr	r3, [fp, #-40]
	.loc 1 411 0
	mov	r0, r3
	sub	sp, fp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L31:
	.align	2
.L30:
	.word	1535
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	407
.LFE6:
	.size	CS_DrawStr, .-CS_DrawStr
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI10-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI13-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI15-.LFB6
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/lpc_lcd_params.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clcdc.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clcdc_driver.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc3xxx_spwm.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/lcd_init.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/gpio_setup.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x9da
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF181
	.byte	0x1
	.4byte	.LASF182
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x43
	.4byte	0x50
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x4c
	.4byte	0x62
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x55
	.4byte	0x74
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x5e
	.4byte	0x86
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x67
	.4byte	0x98
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x2
	.byte	0x99
	.4byte	0x86
	.uleb128 0x5
	.4byte	0x3e
	.4byte	0xc8
	.uleb128 0x6
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0x33
	.4byte	0xde
	.uleb128 0x6
	.4byte	0x25
	.byte	0xef
	.uleb128 0x6
	.4byte	0x25
	.byte	0x9f
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.byte	0x3
	.byte	0x2f
	.4byte	0x10b
	.uleb128 0x8
	.ascii	"TFT\000"
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF16
	.sleb128 1
	.uleb128 0x9
	.4byte	.LASF17
	.sleb128 2
	.uleb128 0x9
	.4byte	.LASF18
	.sleb128 3
	.uleb128 0x9
	.4byte	.LASF19
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF20
	.sleb128 5
	.byte	0
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x3
	.byte	0x36
	.4byte	0xde
	.uleb128 0xa
	.byte	0x28
	.byte	0x3
	.byte	0x39
	.4byte	0x26f
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x3
	.byte	0x3b
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x3
	.byte	0x3d
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x3
	.byte	0x3f
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x3
	.byte	0x41
	.4byte	0x57
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x3
	.byte	0x43
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x3
	.byte	0x45
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x7
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x3
	.byte	0x47
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x3
	.byte	0x49
	.4byte	0x57
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xb
	.4byte	.LASF30
	.byte	0x3
	.byte	0x4b
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF31
	.byte	0x3
	.byte	0x4d
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x3
	.byte	0x4f
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0xb
	.4byte	.LASF33
	.byte	0x3
	.byte	0x50
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0xf
	.uleb128 0xb
	.4byte	.LASF34
	.byte	0x3
	.byte	0x51
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF35
	.byte	0x3
	.byte	0x53
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.uleb128 0xb
	.4byte	.LASF36
	.byte	0x3
	.byte	0x55
	.4byte	0x7b
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x3
	.byte	0x56
	.4byte	0x10b
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF38
	.byte	0x3
	.byte	0x57
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF39
	.byte	0x3
	.byte	0x5c
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x1d
	.uleb128 0xb
	.4byte	.LASF40
	.byte	0x3
	.byte	0x5e
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x1e
	.uleb128 0xb
	.4byte	.LASF41
	.byte	0x3
	.byte	0x60
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x1f
	.uleb128 0xb
	.4byte	.LASF42
	.byte	0x3
	.byte	0x62
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xb
	.4byte	.LASF43
	.byte	0x3
	.byte	0x64
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x21
	.uleb128 0xb
	.4byte	.LASF44
	.byte	0x3
	.byte	0x65
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x22
	.uleb128 0xb
	.4byte	.LASF45
	.byte	0x3
	.byte	0x68
	.4byte	0x57
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x3
	.4byte	.LASF46
	.byte	0x3
	.byte	0x69
	.4byte	0x116
	.uleb128 0xc
	.2byte	0xc30
	.byte	0x4
	.byte	0x23
	.4byte	0x41a
	.uleb128 0xb
	.4byte	.LASF47
	.byte	0x4
	.byte	0x25
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF48
	.byte	0x4
	.byte	0x26
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF49
	.byte	0x4
	.byte	0x27
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF50
	.byte	0x4
	.byte	0x28
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF51
	.byte	0x4
	.byte	0x29
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF52
	.byte	0x4
	.byte	0x2b
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF53
	.byte	0x4
	.byte	0x2e
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF54
	.byte	0x4
	.byte	0x30
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF55
	.byte	0x4
	.byte	0x31
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xb
	.4byte	.LASF56
	.byte	0x4
	.byte	0x32
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xb
	.4byte	.LASF57
	.byte	0x4
	.byte	0x33
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xb
	.4byte	.LASF58
	.byte	0x4
	.byte	0x35
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xb
	.4byte	.LASF59
	.byte	0x4
	.byte	0x37
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xb
	.4byte	.LASF60
	.byte	0x4
	.byte	0x3a
	.4byte	0x42f
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xb
	.4byte	.LASF61
	.byte	0x4
	.byte	0x3b
	.4byte	0x444
	.byte	0x3
	.byte	0x23
	.uleb128 0x200
	.uleb128 0xb
	.4byte	.LASF62
	.byte	0x4
	.byte	0x3c
	.4byte	0x459
	.byte	0x3
	.byte	0x23
	.uleb128 0x400
	.uleb128 0xb
	.4byte	.LASF63
	.byte	0x4
	.byte	0x3d
	.4byte	0x45e
	.byte	0x3
	.byte	0x23
	.uleb128 0x800
	.uleb128 0xb
	.4byte	.LASF64
	.byte	0x4
	.byte	0x3e
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc00
	.uleb128 0xb
	.4byte	.LASF65
	.byte	0x4
	.byte	0x40
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc04
	.uleb128 0xb
	.4byte	.LASF66
	.byte	0x4
	.byte	0x43
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc08
	.uleb128 0xb
	.4byte	.LASF67
	.byte	0x4
	.byte	0x45
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0c
	.uleb128 0xb
	.4byte	.LASF68
	.byte	0x4
	.byte	0x47
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc10
	.uleb128 0xb
	.4byte	.LASF69
	.byte	0x4
	.byte	0x48
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc14
	.uleb128 0xb
	.4byte	.LASF70
	.byte	0x4
	.byte	0x4a
	.4byte	0x473
	.byte	0x3
	.byte	0x23
	.uleb128 0xc18
	.uleb128 0xb
	.4byte	.LASF71
	.byte	0x4
	.byte	0x4c
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc20
	.uleb128 0xb
	.4byte	.LASF72
	.byte	0x4
	.byte	0x4e
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc24
	.uleb128 0xb
	.4byte	.LASF73
	.byte	0x4
	.byte	0x50
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc28
	.uleb128 0xb
	.4byte	.LASF74
	.byte	0x4
	.byte	0x52
	.4byte	0x41a
	.byte	0x3
	.byte	0x23
	.uleb128 0xc2c
	.byte	0
	.uleb128 0xd
	.4byte	0x7b
	.uleb128 0x5
	.4byte	0x7b
	.4byte	0x42f
	.uleb128 0x6
	.4byte	0x25
	.byte	0x72
	.byte	0
	.uleb128 0xd
	.4byte	0x41f
	.uleb128 0x5
	.4byte	0x7b
	.4byte	0x444
	.uleb128 0x6
	.4byte	0x25
	.byte	0x7f
	.byte	0
	.uleb128 0xd
	.4byte	0x434
	.uleb128 0x5
	.4byte	0x7b
	.4byte	0x459
	.uleb128 0x6
	.4byte	0x25
	.byte	0xff
	.byte	0
	.uleb128 0xd
	.4byte	0x449
	.uleb128 0xd
	.4byte	0x449
	.uleb128 0x5
	.4byte	0x7b
	.4byte	0x473
	.uleb128 0x6
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xd
	.4byte	0x463
	.uleb128 0x3
	.4byte	.LASF75
	.byte	0x4
	.byte	0x54
	.4byte	0x27a
	.uleb128 0x7
	.byte	0x4
	.byte	0x5
	.byte	0x28
	.4byte	0x510
	.uleb128 0x9
	.4byte	.LASF76
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF77
	.sleb128 1
	.uleb128 0x9
	.4byte	.LASF78
	.sleb128 2
	.uleb128 0x9
	.4byte	.LASF79
	.sleb128 3
	.uleb128 0x9
	.4byte	.LASF80
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF81
	.sleb128 5
	.uleb128 0x9
	.4byte	.LASF82
	.sleb128 6
	.uleb128 0x9
	.4byte	.LASF83
	.sleb128 7
	.uleb128 0x9
	.4byte	.LASF84
	.sleb128 8
	.uleb128 0x9
	.4byte	.LASF85
	.sleb128 9
	.uleb128 0x9
	.4byte	.LASF86
	.sleb128 10
	.uleb128 0x9
	.4byte	.LASF87
	.sleb128 11
	.uleb128 0x9
	.4byte	.LASF88
	.sleb128 12
	.uleb128 0x9
	.4byte	.LASF89
	.sleb128 13
	.uleb128 0x9
	.4byte	.LASF90
	.sleb128 14
	.uleb128 0x9
	.4byte	.LASF91
	.sleb128 15
	.uleb128 0x9
	.4byte	.LASF92
	.sleb128 16
	.uleb128 0x9
	.4byte	.LASF93
	.sleb128 17
	.uleb128 0x9
	.4byte	.LASF94
	.sleb128 18
	.uleb128 0x9
	.4byte	.LASF95
	.sleb128 19
	.uleb128 0x9
	.4byte	.LASF96
	.sleb128 20
	.uleb128 0x9
	.4byte	.LASF97
	.sleb128 21
	.byte	0
	.uleb128 0x5
	.4byte	0x7b
	.4byte	0x520
	.uleb128 0x6
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.4byte	0x7b
	.4byte	0x530
	.uleb128 0x6
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.byte	0x6
	.byte	0x24
	.4byte	0x611
	.uleb128 0x9
	.4byte	.LASF98
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF99
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF100
	.sleb128 1
	.uleb128 0x9
	.4byte	.LASF101
	.sleb128 2
	.uleb128 0x9
	.4byte	.LASF102
	.sleb128 3
	.uleb128 0x9
	.4byte	.LASF103
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF104
	.sleb128 5
	.uleb128 0x9
	.4byte	.LASF105
	.sleb128 6
	.uleb128 0x9
	.4byte	.LASF106
	.sleb128 7
	.uleb128 0x9
	.4byte	.LASF107
	.sleb128 8
	.uleb128 0x9
	.4byte	.LASF108
	.sleb128 9
	.uleb128 0x9
	.4byte	.LASF109
	.sleb128 10
	.uleb128 0x9
	.4byte	.LASF110
	.sleb128 11
	.uleb128 0x9
	.4byte	.LASF111
	.sleb128 12
	.uleb128 0x9
	.4byte	.LASF112
	.sleb128 13
	.uleb128 0x9
	.4byte	.LASF113
	.sleb128 14
	.uleb128 0x9
	.4byte	.LASF114
	.sleb128 15
	.uleb128 0x9
	.4byte	.LASF115
	.sleb128 16
	.uleb128 0x9
	.4byte	.LASF116
	.sleb128 17
	.uleb128 0x9
	.4byte	.LASF117
	.sleb128 18
	.uleb128 0x9
	.4byte	.LASF118
	.sleb128 19
	.uleb128 0x9
	.4byte	.LASF119
	.sleb128 20
	.uleb128 0x9
	.4byte	.LASF120
	.sleb128 21
	.uleb128 0x9
	.4byte	.LASF121
	.sleb128 22
	.uleb128 0x9
	.4byte	.LASF122
	.sleb128 23
	.uleb128 0x9
	.4byte	.LASF123
	.sleb128 24
	.uleb128 0x9
	.4byte	.LASF124
	.sleb128 25
	.uleb128 0x9
	.4byte	.LASF125
	.sleb128 26
	.uleb128 0x9
	.4byte	.LASF126
	.sleb128 27
	.uleb128 0x9
	.4byte	.LASF127
	.sleb128 28
	.uleb128 0x9
	.4byte	.LASF128
	.sleb128 29
	.uleb128 0x9
	.4byte	.LASF129
	.sleb128 30
	.uleb128 0x9
	.4byte	.LASF130
	.sleb128 31
	.uleb128 0x9
	.4byte	.LASF131
	.sleb128 32
	.uleb128 0x9
	.4byte	.LASF132
	.sleb128 33
	.uleb128 0x9
	.4byte	.LASF133
	.sleb128 34
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0x6
	.2byte	0x19f
	.4byte	0x652
	.uleb128 0x9
	.4byte	.LASF134
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF135
	.sleb128 64
	.uleb128 0x9
	.4byte	.LASF136
	.sleb128 128
	.uleb128 0x9
	.4byte	.LASF137
	.sleb128 192
	.uleb128 0x9
	.4byte	.LASF138
	.sleb128 256
	.uleb128 0x9
	.4byte	.LASF139
	.sleb128 320
	.uleb128 0x9
	.4byte	.LASF140
	.sleb128 384
	.uleb128 0x9
	.4byte	.LASF141
	.sleb128 448
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.byte	0x7
	.byte	0x28
	.4byte	0x669
	.uleb128 0xb
	.4byte	.LASF142
	.byte	0x7
	.byte	0x2a
	.4byte	0x41a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF143
	.byte	0x7
	.byte	0x2b
	.4byte	0x652
	.uleb128 0xf
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF144
	.uleb128 0x10
	.byte	0x4
	.4byte	0x2c
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF145
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF146
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF149
	.byte	0x1
	.byte	0x49
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x6c7
	.uleb128 0x12
	.4byte	.LASF147
	.byte	0x1
	.byte	0x52
	.4byte	0x6c7
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF148
	.byte	0x1
	.byte	0x5d
	.4byte	0x7b
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.4byte	0x669
	.uleb128 0x13
	.4byte	.LASF183
	.byte	0x1
	.byte	0x6b
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF150
	.byte	0x1
	.byte	0x89
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x709
	.uleb128 0x12
	.4byte	.LASF151
	.byte	0x1
	.byte	0x8b
	.4byte	0x8d
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF152
	.byte	0x1
	.byte	0xda
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x15
	.4byte	.LASF184
	.byte	0x1
	.byte	0xf8
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x745
	.uleb128 0x16
	.4byte	.LASF154
	.byte	0x1
	.byte	0xf8
	.4byte	0x7b
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF153
	.byte	0x1
	.2byte	0x10a
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF185
	.byte	0x1
	.2byte	0x149
	.byte	0x1
	.4byte	0xad
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x883
	.uleb128 0x19
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x149
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1a
	.ascii	"X\000"
	.byte	0x1
	.2byte	0x14a
	.4byte	0x888
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x1a
	.ascii	"Y\000"
	.byte	0x1
	.2byte	0x14b
	.4byte	0x888
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x19
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x14c
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x19
	.4byte	.LASF156
	.byte	0x1
	.2byte	0x14d
	.4byte	0x888
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x19
	.4byte	.LASF157
	.byte	0x1
	.2byte	0x14e
	.4byte	0x67d
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x19
	.4byte	.LASF158
	.byte	0x1
	.2byte	0x14f
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x19
	.4byte	.LASF159
	.byte	0x1
	.2byte	0x150
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x19
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x151
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 16
	.uleb128 0x19
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x152
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 20
	.uleb128 0x19
	.4byte	.LASF162
	.byte	0x1
	.2byte	0x153
	.4byte	0x888
	.byte	0x2
	.byte	0x91
	.sleb128 24
	.uleb128 0x19
	.4byte	.LASF163
	.byte	0x1
	.2byte	0x154
	.4byte	0x888
	.byte	0x2
	.byte	0x91
	.sleb128 28
	.uleb128 0x19
	.4byte	.LASF164
	.byte	0x1
	.2byte	0x155
	.4byte	0x888
	.byte	0x2
	.byte	0x91
	.sleb128 32
	.uleb128 0x19
	.4byte	.LASF165
	.byte	0x1
	.2byte	0x156
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 36
	.uleb128 0x19
	.4byte	.LASF166
	.byte	0x1
	.2byte	0x157
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 40
	.uleb128 0x19
	.4byte	.LASF167
	.byte	0x1
	.2byte	0x158
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 44
	.uleb128 0x1b
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x15a
	.4byte	0x7b
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1c
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x15c
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x1d
	.4byte	0x7b
	.uleb128 0x1d
	.4byte	0x8d
	.uleb128 0x12
	.4byte	.LASF169
	.byte	0x8
	.byte	0x30
	.4byte	0x89e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x1d
	.4byte	0xb8
	.uleb128 0x12
	.4byte	.LASF170
	.byte	0x8
	.byte	0x34
	.4byte	0x8b4
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x1d
	.4byte	0xb8
	.uleb128 0x12
	.4byte	.LASF171
	.byte	0x8
	.byte	0x36
	.4byte	0x8ca
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x1d
	.4byte	0xb8
	.uleb128 0x12
	.4byte	.LASF172
	.byte	0x8
	.byte	0x38
	.4byte	0x8e0
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x1d
	.4byte	0xb8
	.uleb128 0x1e
	.4byte	.LASF173
	.byte	0x9
	.byte	0x30
	.4byte	0x674
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF174
	.byte	0x9
	.byte	0x37
	.4byte	0xc8
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF175
	.byte	0x9
	.byte	0x3b
	.4byte	0xc8
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	0x33
	.4byte	0x923
	.uleb128 0x1f
	.4byte	0x25
	.2byte	0x5ff
	.uleb128 0x6
	.4byte	0x25
	.byte	0x9f
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF176
	.byte	0x9
	.byte	0x3d
	.4byte	0x90c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF177
	.byte	0x3
	.byte	0x70
	.4byte	0x93d
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	0x26f
	.uleb128 0x1e
	.4byte	.LASF178
	.byte	0xa
	.byte	0x3d
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF179
	.byte	0xb
	.byte	0x33
	.4byte	0x960
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x1d
	.4byte	0x510
	.uleb128 0x12
	.4byte	.LASF180
	.byte	0xb
	.byte	0x3f
	.4byte	0x976
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x1d
	.4byte	0x520
	.uleb128 0x20
	.4byte	.LASF173
	.byte	0x1
	.byte	0x29
	.4byte	0x674
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	guilib_display_ptr
	.uleb128 0x20
	.4byte	.LASF174
	.byte	0x1
	.byte	0x2e
	.4byte	0xc8
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	gui_lib_display_buf
	.uleb128 0x20
	.4byte	.LASF175
	.byte	0x1
	.byte	0x36
	.4byte	0xc8
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	primary_display_buf
	.uleb128 0x20
	.4byte	.LASF176
	.byte	0x1
	.byte	0x3b
	.4byte	0x90c
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	utility_display_buf
	.uleb128 0x1e
	.4byte	.LASF177
	.byte	0x3
	.byte	0x70
	.4byte	0x93d
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF178
	.byte	0xa
	.byte	0x3d
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI11
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI14
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI16
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF67:
	.ascii	"lcdcrsrpal1\000"
.LASF136:
	.ascii	"CLKPWR_LCDMUX_TFT15\000"
.LASF135:
	.ascii	"CLKPWR_LCDMUX_TFT16\000"
.LASF75:
	.ascii	"CLCDC_REGS_T\000"
.LASF117:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF160:
	.ascii	"Transparent\000"
.LASF153:
	.ascii	"FDTO_set_guilib_to_write_to_the_primary_display_buf"
	.ascii	"fer\000"
.LASF27:
	.ascii	"v_front_porch\000"
.LASF115:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF24:
	.ascii	"h_sync_pulse_width\000"
.LASF23:
	.ascii	"h_front_porch\000"
.LASF119:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF36:
	.ascii	"optimal_clock\000"
.LASF21:
	.ascii	"LCD_PANEL_T\000"
.LASF56:
	.ascii	"lcdstatus\000"
.LASF127:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF144:
	.ascii	"long int\000"
.LASF168:
	.ascii	"lFontHeight\000"
.LASF100:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF13:
	.ascii	"long long unsigned int\000"
.LASF15:
	.ascii	"BOOL_32\000"
.LASF71:
	.ascii	"lcdcrsrintmsk\000"
.LASF142:
	.ascii	"pwm_ctrl\000"
.LASF78:
	.ascii	"LCD_PWENABLE\000"
.LASF68:
	.ascii	"lcdcrsrxy\000"
.LASF88:
	.ascii	"LCD_SET_BPP\000"
.LASF29:
	.ascii	"lines_per_panel\000"
.LASF162:
	.ascii	"BackBoxSizeX\000"
.LASF95:
	.ascii	"LCD_CRSR_INTMSK\000"
.LASF148:
	.ascii	"led_backlight_duty\000"
.LASF157:
	.ascii	"String\000"
.LASF137:
	.ascii	"CLKPWR_LCDMUX_TFT24\000"
.LASF105:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF14:
	.ascii	"long long int\000"
.LASF5:
	.ascii	"signed char\000"
.LASF98:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF53:
	.ascii	"lcdctrl\000"
.LASF4:
	.ascii	"INT_8\000"
.LASF43:
	.ascii	"hrtft_lp_delay\000"
.LASF8:
	.ascii	"INT_16\000"
.LASF69:
	.ascii	"lcdcrsrclip\000"
.LASF18:
	.ascii	"MONO_4BIT\000"
.LASF76:
	.ascii	"LCD_CONFIG\000"
.LASF151:
	.ascii	"lcddev\000"
.LASF84:
	.ascii	"LCD_PICK_INT\000"
.LASF55:
	.ascii	"lcdinterrupt\000"
.LASF124:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF131:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF147:
	.ascii	"lpwm\000"
.LASF72:
	.ascii	"lcdcrsrintclr\000"
.LASF111:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF9:
	.ascii	"short int\000"
.LASF141:
	.ascii	"CLKPWR_LCDMUX_DSTN8C\000"
.LASF146:
	.ascii	"double\000"
.LASF96:
	.ascii	"LCD_CRSR_INIT_IMG\000"
.LASF61:
	.ascii	"lcdpalette\000"
.LASF64:
	.ascii	"lcdcrsrcntl\000"
.LASF172:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF176:
	.ascii	"utility_display_buf\000"
.LASF159:
	.ascii	"PsWriting\000"
.LASF70:
	.ascii	"rsrved3clcdc\000"
.LASF20:
	.ascii	"CSTN\000"
.LASF89:
	.ascii	"LCD_SET_CLOCK\000"
.LASF103:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF167:
	.ascii	"BackColor\000"
.LASF49:
	.ascii	"lcdpol\000"
.LASF11:
	.ascii	"unsigned int\000"
.LASF108:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF126:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF77:
	.ascii	"LCD_ENABLE\000"
.LASF12:
	.ascii	"INT_32\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF154:
	.ascii	"dot_row_origin\000"
.LASF138:
	.ascii	"CLKPWR_LCDMUX_STN4M\000"
.LASF22:
	.ascii	"h_back_porch\000"
.LASF7:
	.ascii	"short unsigned int\000"
.LASF181:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF65:
	.ascii	"lcdcrsrcnfg\000"
.LASF74:
	.ascii	"lcdcrsrintstat\000"
.LASF93:
	.ascii	"LCD_CRSR_PAL0\000"
.LASF94:
	.ascii	"LCD_CRSR_PAL1\000"
.LASF177:
	.ascii	"calsense_4bit_grey16\000"
.LASF39:
	.ascii	"hrtft_cls_enable\000"
.LASF133:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF120:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF113:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF99:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF128:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF155:
	.ascii	"FontNo\000"
.LASF32:
	.ascii	"invert_hsync\000"
.LASF163:
	.ascii	"BackBoxSizeY1\000"
.LASF164:
	.ascii	"BackBoxSizeY2\000"
.LASF82:
	.ascii	"LCD_ENABLE_INTS\000"
.LASF109:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF40:
	.ascii	"hrtft_sps_enable\000"
.LASF86:
	.ascii	"LCD_DMA_ON_4MT\000"
.LASF62:
	.ascii	"rsrved2clcdc\000"
.LASF91:
	.ascii	"LCD_CRSR_EN\000"
.LASF101:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF129:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF174:
	.ascii	"gui_lib_display_buf\000"
.LASF81:
	.ascii	"LCD_SET_OV_FB\000"
.LASF80:
	.ascii	"LCD_SET_LW_FB\000"
.LASF185:
	.ascii	"CS_DrawStr\000"
.LASF165:
	.ascii	"BackBorderPixels\000"
.LASF110:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF25:
	.ascii	"pixels_per_line\000"
.LASF173:
	.ascii	"guilib_display_ptr\000"
.LASF54:
	.ascii	"lcdintrenable\000"
.LASF121:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF175:
	.ascii	"primary_display_buf\000"
.LASF26:
	.ascii	"v_back_porch\000"
.LASF38:
	.ascii	"dual_panel\000"
.LASF145:
	.ascii	"float\000"
.LASF169:
	.ascii	"GuiFont_LanguageActive\000"
.LASF90:
	.ascii	"LCD_GET_STATUS\000"
.LASF47:
	.ascii	"lcdtimh\000"
.LASF42:
	.ascii	"hrtft_polarity_delay\000"
.LASF35:
	.ascii	"bits_per_pixel\000"
.LASF48:
	.ascii	"lcdtimv\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF184:
	.ascii	"FDTO_set_guilib_to_write_to_the_utility_display_buf"
	.ascii	"fer\000"
.LASF44:
	.ascii	"hrtft_spl_delay\000"
.LASF149:
	.ascii	"led_backlight_set_brightness\000"
.LASF166:
	.ascii	"ForeColor\000"
.LASF134:
	.ascii	"CLKPWR_LCDMUX_TFT12\000"
.LASF104:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF118:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF143:
	.ascii	"SPWM_REGS_T\000"
.LASF57:
	.ascii	"lcdintclr\000"
.LASF28:
	.ascii	"v_sync_pulse_width\000"
.LASF30:
	.ascii	"invert_output_enable\000"
.LASF125:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF140:
	.ascii	"CLKPWR_LCDMUX_DSTN4M\000"
.LASF178:
	.ascii	"display_model_is\000"
.LASF97:
	.ascii	"LCD_INIT_IMG_PALL\000"
.LASF170:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF60:
	.ascii	"rsrved1clcdc\000"
.LASF19:
	.ascii	"MONO_8BIT\000"
.LASF34:
	.ascii	"ac_bias_frequency\000"
.LASF161:
	.ascii	"Underlining\000"
.LASF150:
	.ascii	"init_lcd_1\000"
.LASF152:
	.ascii	"init_lcd_2\000"
.LASF1:
	.ascii	"char\000"
.LASF50:
	.ascii	"lcdle\000"
.LASF183:
	.ascii	"init_backlight_pwm\000"
.LASF171:
	.ascii	"GuiFont_DecimalChar\000"
.LASF106:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF114:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF87:
	.ascii	"LCD_SWAP_RGB\000"
.LASF45:
	.ascii	"hrtft_spl_to_cls_delay\000"
.LASF52:
	.ascii	"lcdlpbase\000"
.LASF158:
	.ascii	"Alignment\000"
.LASF51:
	.ascii	"lcdupbase\000"
.LASF123:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF132:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF46:
	.ascii	"LCD_PARAM_T\000"
.LASF180:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF73:
	.ascii	"lcdcrsrintraw\000"
.LASF17:
	.ascii	"HRTFT\000"
.LASF139:
	.ascii	"CLKPWR_LCDMUX_STN8C\000"
.LASF156:
	.ascii	"CharSetSelector\000"
.LASF6:
	.ascii	"UNS_16\000"
.LASF102:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF41:
	.ascii	"hrtft_lp_to_ps_delay\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF83:
	.ascii	"LCD_DISABLE_INTS\000"
.LASF85:
	.ascii	"LCD_CLEAR_INT\000"
.LASF10:
	.ascii	"UNS_32\000"
.LASF33:
	.ascii	"invert_vsync\000"
.LASF58:
	.ascii	"lcdupcurr\000"
.LASF112:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF130:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF37:
	.ascii	"lcd_panel_type\000"
.LASF122:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF31:
	.ascii	"invert_panel_clock\000"
.LASF66:
	.ascii	"lcdcrsrpal0\000"
.LASF179:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF63:
	.ascii	"lcdcrsrimage\000"
.LASF79:
	.ascii	"LCD_SET_UP_FB\000"
.LASF182:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/"
	.ascii	"lcd_init.c\000"
.LASF107:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF116:
	.ascii	"CLKPWR_WDOG_CLK\000"
.LASF92:
	.ascii	"LCD_CRSR_XY\000"
.LASF16:
	.ascii	"ADTFT\000"
.LASF59:
	.ascii	"lcdlpcurr\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
