	.file	"e_budget_setup.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.g_BUDGET_SETUP_editing_group,"aw",%nobits
	.align	2
	.type	g_BUDGET_SETUP_editing_group, %object
	.size	g_BUDGET_SETUP_editing_group, 4
g_BUDGET_SETUP_editing_group:
	.space	4
	.section	.bss.g_BUDGET_SETUP_number_annual_periods,"aw",%nobits
	.align	2
	.type	g_BUDGET_SETUP_number_annual_periods, %object
	.size	g_BUDGET_SETUP_number_annual_periods, 4
g_BUDGET_SETUP_number_annual_periods:
	.space	4
	.section	.bss.g_BUDGET_SETUP_entry_option,"aw",%nobits
	.align	2
	.type	g_BUDGET_SETUP_entry_option, %object
	.size	g_BUDGET_SETUP_entry_option, 4
g_BUDGET_SETUP_entry_option:
	.space	4
	.section	.bss.g_BUDGET_setup_button_pressed,"aw",%nobits
	.align	2
	.type	g_BUDGET_setup_button_pressed, %object
	.size	g_BUDGET_setup_button_pressed, 4
g_BUDGET_setup_button_pressed:
	.space	4
	.section	.bss.g_BUDGET_setup_selected_group,"aw",%nobits
	.align	2
	.type	g_BUDGET_setup_selected_group, %object
	.size	g_BUDGET_setup_selected_group, 4
g_BUDGET_setup_selected_group:
	.space	4
	.section	.text.BUDGET_SETUP_process_annual_periods_changed,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_process_annual_periods_changed, %function
BUDGET_SETUP_process_annual_periods_changed:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_budget_setup.c"
	.loc 1 80 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #32
.LCFI2:
	.loc 1 88 0
	ldr	r3, .L5
	ldr	r2, [r3, #0]
	ldr	r3, .L5+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L1
	.loc 1 90 0
	sub	r3, fp, #36
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 91 0
	ldrh	r3, [fp, #-28]
	str	r3, [fp, #-8]
	.loc 1 92 0
	ldrh	r3, [fp, #-26]
	str	r3, [fp, #-12]
	.loc 1 94 0
	ldr	r3, .L5+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-16]
	.loc 1 99 0
	ldr	r3, .L5+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L3
	.loc 1 103 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #1
	cmp	r3, #0
	bne	.L4
	.loc 1 105 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
.L4:
	.loc 1 109 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	mov	r3, #2
	bl	BUDGET_reset_budget_values
	b	.L1
.L3:
	.loc 1 115 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	mov	r3, #1
	bl	BUDGET_reset_budget_values
.L1:
	.loc 1 118 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	g_BUDGET_SETUP_number_annual_periods
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	g_GROUP_list_item_index
.LFE0:
	.size	BUDGET_SETUP_process_annual_periods_changed, .-BUDGET_SETUP_process_annual_periods_changed
	.section	.text.BUDGET_SETUP_process_entry_option_changed,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_process_entry_option_changed, %function
BUDGET_SETUP_process_entry_option_changed:
.LFB1:
	.loc 1 122 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #32
.LCFI5:
	.loc 1 131 0
	ldr	r3, .L13
	ldr	r2, [r3, #0]
	ldr	r3, .L13+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L8
	.loc 1 133 0
	sub	r3, fp, #36
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 134 0
	ldrh	r3, [fp, #-28]
	str	r3, [fp, #-8]
	.loc 1 135 0
	ldrh	r3, [fp, #-26]
	str	r3, [fp, #-12]
	.loc 1 137 0
	ldr	r3, .L13+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-16]
	.loc 1 140 0
	ldr	r3, .L13+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	.loc 1 142 0
	ldr	r3, .L13+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L10
	.loc 1 146 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #1
	cmp	r3, #0
	bne	.L11
	.loc 1 148 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
.L11:
	.loc 1 152 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	mov	r3, #2
	bl	BUDGET_reset_budget_values
	b	.L8
.L10:
	.loc 1 158 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	mov	r3, #1
	bl	BUDGET_reset_budget_values
	b	.L8
.L9:
	.loc 1 163 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	mov	r3, #1
	bl	BUDGET_reset_budget_values
.L8:
	.loc 1 168 0
	ldr	r3, .L13+4
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L7
	.loc 1 170 0
	ldr	r3, .L13+16
	mov	r2, #0
	str	r2, [r3, #0]
.L7:
	.loc 1 172 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L14:
	.align	2
.L13:
	.word	g_BUDGET_SETUP_entry_option
	.word	GuiVar_BudgetEntryOptionIdx
	.word	g_GROUP_list_item_index
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	GuiVar_BudgetModeIdx
.LFE1:
	.size	BUDGET_SETUP_process_entry_option_changed, .-BUDGET_SETUP_process_entry_option_changed
	.section	.text.BUDGET_pre_process_periods_per_year_changed,"ax",%progbits
	.align	2
	.type	BUDGET_pre_process_periods_per_year_changed, %function
BUDGET_pre_process_periods_per_year_changed:
.LFB2:
	.loc 1 175 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	.loc 1 176 0
	ldr	r3, .L17
	ldr	r2, [r3, #0]
	ldr	r3, .L17+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L15
	.loc 1 178 0
	mov	r0, #584
	bl	DIALOG_draw_yes_no_cancel_dialog
.L15:
	.loc 1 182 0
	ldmfd	sp!, {fp, pc}
.L18:
	.align	2
.L17:
	.word	g_BUDGET_SETUP_number_annual_periods
	.word	GuiVar_BudgetPeriodsPerYearIdx
.LFE2:
	.size	BUDGET_pre_process_periods_per_year_changed, .-BUDGET_pre_process_periods_per_year_changed
	.section	.text.BUDGET_pre_entry_option_changed,"ax",%progbits
	.align	2
	.type	BUDGET_pre_entry_option_changed, %function
BUDGET_pre_entry_option_changed:
.LFB3:
	.loc 1 201 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	.loc 1 202 0
	ldr	r3, .L21
	ldr	r2, [r3, #0]
	ldr	r3, .L21+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L19
	.loc 1 204 0
	ldr	r0, .L21+8
	bl	DIALOG_draw_yes_no_cancel_dialog
.L19:
	.loc 1 206 0
	ldmfd	sp!, {fp, pc}
.L22:
	.align	2
.L21:
	.word	g_BUDGET_SETUP_entry_option
	.word	GuiVar_BudgetEntryOptionIdx
	.word	585
.LFE3:
	.size	BUDGET_pre_entry_option_changed, .-BUDGET_pre_entry_option_changed
	.section	.text.BUDGET_SETUP_process_number_annual_periods_changed_dialog,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_process_number_annual_periods_changed_dialog, %function
BUDGET_SETUP_process_number_annual_periods_changed_dialog:
.LFB4:
	.loc 1 210 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI10:
	add	fp, sp, #4
.LCFI11:
	.loc 1 211 0
	ldr	r3, .L27
	ldr	r3, [r3, #0]
	cmp	r3, #6
	beq	.L26
	cmp	r3, #7
	beq	.L25
	cmp	r3, #2
	beq	.L25
	b	.L24
.L26:
	.loc 1 214 0
	bl	BUDGET_SETUP_process_annual_periods_changed
	.loc 1 215 0
	b	.L24
.L25:
	.loc 1 219 0
	ldr	r3, .L27+4
	ldr	r2, [r3, #0]
	ldr	r3, .L27+8
	str	r2, [r3, #0]
	.loc 1 220 0
	mov	r0, r0	@ nop
.L24:
	.loc 1 223 0
	bl	Refresh_Screen
	.loc 1 224 0
	ldmfd	sp!, {fp, pc}
.L28:
	.align	2
.L27:
	.word	g_DIALOG_modal_result
	.word	g_BUDGET_SETUP_number_annual_periods
	.word	GuiVar_BudgetPeriodsPerYearIdx
.LFE4:
	.size	BUDGET_SETUP_process_number_annual_periods_changed_dialog, .-BUDGET_SETUP_process_number_annual_periods_changed_dialog
	.section	.text.BUDGET_SETUP_process_entry_option_changed_dialog,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_process_entry_option_changed_dialog, %function
BUDGET_SETUP_process_entry_option_changed_dialog:
.LFB5:
	.loc 1 243 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	.loc 1 244 0
	ldr	r3, .L33
	ldr	r3, [r3, #0]
	cmp	r3, #6
	beq	.L32
	cmp	r3, #7
	beq	.L31
	cmp	r3, #2
	beq	.L31
	b	.L30
.L32:
	.loc 1 247 0
	bl	BUDGET_SETUP_process_entry_option_changed
	.loc 1 248 0
	b	.L30
.L31:
	.loc 1 252 0
	ldr	r3, .L33+4
	ldr	r2, [r3, #0]
	ldr	r3, .L33+8
	str	r2, [r3, #0]
	.loc 1 253 0
	mov	r0, r0	@ nop
.L30:
	.loc 1 256 0
	bl	Refresh_Screen
	.loc 1 257 0
	ldmfd	sp!, {fp, pc}
.L34:
	.align	2
.L33:
	.word	g_DIALOG_modal_result
	.word	g_BUDGET_SETUP_entry_option
	.word	GuiVar_BudgetEntryOptionIdx
.LFE5:
	.size	BUDGET_SETUP_process_entry_option_changed_dialog, .-BUDGET_SETUP_process_entry_option_changed_dialog
	.section	.text.FDTO_POC_show_budget_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_in_use_dropdown, %function
FDTO_POC_show_budget_in_use_dropdown:
.LFB6:
	.loc 1 261 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI14:
	add	fp, sp, #4
.LCFI15:
	.loc 1 262 0
	ldr	r3, .L36
	ldr	r3, [r3, #0]
	mov	r0, #213
	mov	r1, #38
	mov	r2, r3
	bl	FDTO_COMBO_BOX_show_no_yes_dropdown
	.loc 1 263 0
	ldmfd	sp!, {fp, pc}
.L37:
	.align	2
.L36:
	.word	GuiVar_BudgetInUse
.LFE6:
	.size	FDTO_POC_show_budget_in_use_dropdown, .-FDTO_POC_show_budget_in_use_dropdown
	.section	.text.FDTO_POC_show_budget_periods_per_year_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_periods_per_year_dropdown, %function
FDTO_POC_show_budget_periods_per_year_dropdown:
.LFB7:
	.loc 1 267 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI16:
	add	fp, sp, #4
.LCFI17:
	.loc 1 268 0
	ldr	r3, .L39
	ldr	r3, [r3, #0]
	mov	r0, #724
	ldr	r1, .L39+4
	mov	r2, #2
	bl	FDTO_COMBOBOX_show
	.loc 1 269 0
	ldmfd	sp!, {fp, pc}
.L40:
	.align	2
.L39:
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	FDTO_COMBOBOX_add_items
.LFE7:
	.size	FDTO_POC_show_budget_periods_per_year_dropdown, .-FDTO_POC_show_budget_periods_per_year_dropdown
	.section	.text.FDTO_POC_close_budget_periods_per_year_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_budget_periods_per_year_dropdown, %function
FDTO_POC_close_budget_periods_per_year_dropdown:
.LFB8:
	.loc 1 273 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	.loc 1 274 0
	ldr	r3, .L42
	ldr	r2, [r3, #0]
	ldr	r3, .L42+4
	str	r2, [r3, #0]
	.loc 1 276 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L42
	str	r2, [r3, #0]
	.loc 1 278 0
	bl	FDTO_COMBOBOX_hide
	.loc 1 280 0
	bl	BUDGET_pre_process_periods_per_year_changed
	.loc 1 281 0
	ldmfd	sp!, {fp, pc}
.L43:
	.align	2
.L42:
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	g_BUDGET_SETUP_number_annual_periods
.LFE8:
	.size	FDTO_POC_close_budget_periods_per_year_dropdown, .-FDTO_POC_close_budget_periods_per_year_dropdown
	.section	.text.FDTO_POC_show_budget_option_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_option_dropdown, %function
FDTO_POC_show_budget_option_dropdown:
.LFB9:
	.loc 1 300 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI20:
	add	fp, sp, #4
.LCFI21:
	.loc 1 303 0
	ldr	r3, .L47
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L45
	.loc 1 305 0
	ldr	r3, .L47
	ldr	r3, [r3, #0]
	ldr	r0, .L47+4
	ldr	r1, .L47+8
	mov	r2, #3
	bl	FDTO_COMBOBOX_show
	b	.L44
.L45:
	.loc 1 309 0
	ldr	r3, .L47
	ldr	r3, [r3, #0]
	ldr	r0, .L47+12
	ldr	r1, .L47+8
	mov	r2, #3
	bl	FDTO_COMBOBOX_show
.L44:
	.loc 1 311 0
	ldmfd	sp!, {fp, pc}
.L48:
	.align	2
.L47:
	.word	GuiVar_BudgetEntryOptionIdx
	.word	722
	.word	FDTO_COMBOBOX_add_items
	.word	721
.LFE9:
	.size	FDTO_POC_show_budget_option_dropdown, .-FDTO_POC_show_budget_option_dropdown
	.section	.text.FDTO_POC_close_budget_option_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_budget_option_dropdown, %function
FDTO_POC_close_budget_option_dropdown:
.LFB10:
	.loc 1 330 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI22:
	add	fp, sp, #4
.LCFI23:
	.loc 1 331 0
	ldr	r3, .L50
	ldr	r2, [r3, #0]
	ldr	r3, .L50+4
	str	r2, [r3, #0]
	.loc 1 333 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L50
	str	r2, [r3, #0]
	.loc 1 335 0
	bl	FDTO_COMBOBOX_hide
	.loc 1 337 0
	bl	BUDGET_pre_entry_option_changed
	.loc 1 338 0
	ldmfd	sp!, {fp, pc}
.L51:
	.align	2
.L50:
	.word	GuiVar_BudgetEntryOptionIdx
	.word	g_BUDGET_SETUP_entry_option
.LFE10:
	.size	FDTO_POC_close_budget_option_dropdown, .-FDTO_POC_close_budget_option_dropdown
	.section	.text.FDTO_POC_show_budget_mode_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_mode_dropdown, %function
FDTO_POC_show_budget_mode_dropdown:
.LFB11:
	.loc 1 342 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	.loc 1 345 0
	ldr	r3, .L55
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L53
	.loc 1 347 0
	ldr	r3, .L55+4
	ldr	r3, [r3, #0]
	ldr	r0, .L55+8
	ldr	r1, .L55+12
	mov	r2, #2
	bl	FDTO_COMBOBOX_show
	b	.L52
.L53:
	.loc 1 351 0
	ldr	r3, .L55+4
	ldr	r3, [r3, #0]
	mov	r0, #720
	ldr	r1, .L55+12
	mov	r2, #2
	bl	FDTO_COMBOBOX_show
.L52:
	.loc 1 353 0
	ldmfd	sp!, {fp, pc}
.L56:
	.align	2
.L55:
	.word	GuiVar_BudgetEntryOptionIdx
	.word	GuiVar_BudgetModeIdx
	.word	719
	.word	FDTO_COMBOBOX_add_items
.LFE11:
	.size	FDTO_POC_show_budget_mode_dropdown, .-FDTO_POC_show_budget_mode_dropdown
	.section	.text.FDTO_POC_close_budget_mode_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_budget_mode_dropdown, %function
FDTO_POC_close_budget_mode_dropdown:
.LFB12:
	.loc 1 356 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	.loc 1 357 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L58
	str	r2, [r3, #0]
	.loc 1 358 0
	bl	FDTO_COMBOBOX_hide
	.loc 1 360 0
	mov	r0, #4
	bl	GuiLib_Cursor_Select
	.loc 1 361 0
	ldmfd	sp!, {fp, pc}
.L59:
	.align	2
.L58:
	.word	GuiVar_BudgetModeIdx
.LFE12:
	.size	FDTO_POC_close_budget_mode_dropdown, .-FDTO_POC_close_budget_mode_dropdown
	.section	.text.BUDGET_SETUP_process_group,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_process_group, %function
BUDGET_SETUP_process_group:
.LFB13:
	.loc 1 365 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI28:
	add	fp, sp, #8
.LCFI29:
	sub	sp, sp, #56
.LCFI30:
	str	r0, [fp, #-56]
	str	r1, [fp, #-52]
	.loc 1 371 0
	ldr	r3, .L146
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #720
	bgt	.L69
	ldr	r2, .L146+4
	cmp	r3, r2
	bge	.L65
	ldr	r2, .L146+8
	cmp	r3, r2
	beq	.L63
	cmp	r3, #616
	beq	.L64
	cmp	r3, #584
	beq	.L62
	b	.L61
.L69:
	cmp	r3, #724
	beq	.L67
	cmp	r3, #724
	bgt	.L70
	ldr	r2, .L146+12
	cmp	r3, r2
	bgt	.L61
	b	.L141
.L70:
	cmp	r3, #740
	bne	.L61
.L68:
	.loc 1 374 0
	ldr	r3, [fp, #-56]
	mov	r0, r3
	ldr	r1, .L146+16
	bl	COMBO_BOX_key_press
	.loc 1 375 0
	b	.L60
.L67:
	.loc 1 378 0
	ldr	r3, [fp, #-56]
	cmp	r3, #2
	beq	.L72
	.loc 1 378 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-56]
	cmp	r3, #67
	bne	.L73
.L72:
	.loc 1 380 0 is_stmt 1
	bl	good_key_beep
	.loc 1 381 0
	mov	r3, #1
	str	r3, [fp, #-48]
	.loc 1 382 0
	ldr	r3, .L146+20
	str	r3, [fp, #-28]
	.loc 1 383 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 389 0
	b	.L60
.L73:
	.loc 1 387 0
	ldr	r3, [fp, #-56]
	mov	r0, r3
	mov	r1, #0
	bl	COMBO_BOX_key_press
	.loc 1 389 0
	b	.L60
.L64:
	.loc 1 393 0
	sub	r1, fp, #56
	ldmia	r1, {r0-r1}
	ldr	r2, .L146+24
	bl	NUMERIC_KEYPAD_process_key
	.loc 1 394 0
	b	.L60
.L141:
	.loc 1 399 0
	ldr	r3, [fp, #-56]
	cmp	r3, #2
	beq	.L75
	.loc 1 399 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-56]
	cmp	r3, #67
	bne	.L76
.L75:
	.loc 1 401 0 is_stmt 1
	bl	good_key_beep
	.loc 1 402 0
	mov	r3, #1
	str	r3, [fp, #-48]
	.loc 1 403 0
	ldr	r3, .L146+28
	str	r3, [fp, #-28]
	.loc 1 404 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 410 0
	b	.L60
.L76:
	.loc 1 408 0
	ldr	r3, [fp, #-56]
	mov	r0, r3
	mov	r1, #0
	bl	COMBO_BOX_key_press
	.loc 1 410 0
	b	.L60
.L65:
	.loc 1 414 0
	ldr	r3, [fp, #-56]
	cmp	r3, #2
	beq	.L78
	.loc 1 414 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-56]
	cmp	r3, #67
	bne	.L79
.L78:
	.loc 1 416 0 is_stmt 1
	bl	good_key_beep
	.loc 1 417 0
	mov	r3, #1
	str	r3, [fp, #-48]
	.loc 1 418 0
	ldr	r3, .L146+32
	str	r3, [fp, #-28]
	.loc 1 419 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 420 0
	bl	BUDGET_SETUP_process_number_annual_periods_changed_dialog
	.loc 1 426 0
	b	.L60
.L79:
	.loc 1 424 0
	ldr	r3, [fp, #-56]
	mov	r0, r3
	mov	r1, #0
	bl	COMBO_BOX_key_press
	.loc 1 426 0
	b	.L60
.L62:
	.loc 1 429 0
	sub	r1, fp, #56
	ldmia	r1, {r0-r1}
	ldr	r2, .L146+36
	bl	DIALOG_process_yes_no_cancel_dialog
	.loc 1 430 0
	b	.L60
.L63:
	.loc 1 433 0
	sub	r1, fp, #56
	ldmia	r1, {r0-r1}
	ldr	r2, .L146+40
	bl	DIALOG_process_yes_no_cancel_dialog
	.loc 1 434 0
	b	.L60
.L61:
	.loc 1 437 0
	ldr	r3, [fp, #-56]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L81
.L90:
	.word	.L82
	.word	.L83
	.word	.L84
	.word	.L85
	.word	.L86
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L87
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L87
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L88
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L89
	.word	.L81
	.word	.L81
	.word	.L81
	.word	.L89
.L84:
	.loc 1 440 0
	ldr	r3, .L146+44
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L91
.L99:
	.word	.L92
	.word	.L93
	.word	.L94
	.word	.L95
	.word	.L96
	.word	.L97
	.word	.L98
.L92:
	.loc 1 444 0
	ldr	r3, .L146+48
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-12]
	.loc 1 445 0
	ldr	r0, [fp, #-12]
	bl	nm_GROUP_get_group_ID
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	mov	r3, r0
	ldr	r3, [r3, #468]
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	and	r3, r3, #255
	cmp	r3, #0
	ble	.L100
	.loc 1 447 0
	bl	good_key_beep
	.loc 1 448 0
	mov	r3, #1
	str	r3, [fp, #-48]
	.loc 1 449 0
	ldr	r3, .L146+52
	str	r3, [fp, #-28]
	.loc 1 450 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 456 0
	b	.L102
.L100:
	.loc 1 454 0
	bl	bad_key_beep
	.loc 1 456 0
	b	.L102
.L93:
	.loc 1 459 0
	bl	good_key_beep
	.loc 1 460 0
	mov	r3, #1
	str	r3, [fp, #-48]
	.loc 1 461 0
	ldr	r3, .L146+56
	str	r3, [fp, #-28]
	.loc 1 462 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 463 0
	b	.L102
.L94:
	.loc 1 466 0
	bl	good_key_beep
	.loc 1 467 0
	mov	r3, #1
	str	r3, [fp, #-48]
	.loc 1 468 0
	ldr	r3, .L146+60
	str	r3, [fp, #-28]
	.loc 1 469 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 470 0
	b	.L102
.L95:
	.loc 1 474 0
	bl	good_key_beep
	.loc 1 475 0
	ldr	r0, .L146+64
	mov	r1, #10
	mov	r2, #300
	bl	NUMERIC_KEYPAD_draw_uint32_keypad
	.loc 1 476 0
	b	.L102
.L96:
	.loc 1 480 0
	ldr	r3, .L146+68
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L103
	.loc 1 482 0
	bl	bad_key_beep
	.loc 1 491 0
	b	.L102
.L103:
	.loc 1 486 0
	bl	good_key_beep
	.loc 1 487 0
	mov	r3, #1
	str	r3, [fp, #-48]
	.loc 1 488 0
	ldr	r3, .L146+72
	str	r3, [fp, #-28]
	.loc 1 489 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 491 0
	b	.L102
.L97:
	.loc 1 494 0
	bl	BUDGET_SETUP_extract_and_store_changes_from_GuiVars
	.loc 1 497 0
	ldr	r3, .L146+76
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 500 0
	ldr	r3, .L146+48
	ldr	r2, [r3, #0]
	ldr	r3, .L146+80
	str	r2, [r3, #0]
	.loc 1 502 0
	mov	r3, #2
	str	r3, [fp, #-48]
	.loc 1 503 0
	mov	r3, #9
	str	r3, [fp, #-40]
	.loc 1 504 0
	ldr	r3, .L146+84
	str	r3, [fp, #-28]
	.loc 1 505 0
	ldr	r3, .L146+88
	str	r3, [fp, #-32]
	.loc 1 506 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 507 0
	ldr	r3, .L146+92
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	.loc 1 508 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	Change_Screen
	.loc 1 510 0
	b	.L102
.L98:
	.loc 1 513 0
	bl	good_key_beep
	.loc 1 516 0
	ldr	r3, .L146+48
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-12]
	.loc 1 517 0
	ldr	r0, [fp, #-12]
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r0, .L146+96
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 521 0
	ldr	r3, .L146+48
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	BUDGET_calculate_square_footage
	mov	r3, r0
	cmp	r3, #1
	bne	.L105
	.loc 1 523 0
	ldr	r0, .L146+100
	bl	DIALOG_draw_ok_dialog
	.loc 1 529 0
	b	.L102
.L105:
	.loc 1 527 0
	ldr	r0, .L146+104
	bl	DIALOG_draw_ok_dialog
	.loc 1 529 0
	b	.L102
.L91:
	.loc 1 532 0
	bl	bad_key_beep
.L102:
	.loc 1 534 0
	bl	Refresh_Screen
	.loc 1 535 0
	b	.L60
.L89:
	.loc 1 539 0
	ldr	r3, .L146+44
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L107
.L113:
	.word	.L108
	.word	.L109
	.word	.L110
	.word	.L111
	.word	.L112
.L108:
	.loc 1 543 0
	ldr	r3, .L146+48
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SYSTEM_get_group_at_this_index
	str	r0, [fp, #-12]
	.loc 1 544 0
	ldr	r0, [fp, #-12]
	bl	nm_GROUP_get_group_ID
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	mov	r3, r0
	ldr	r3, [r3, #468]
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	and	r3, r3, #255
	cmp	r3, #0
	ble	.L114
	.loc 1 546 0
	ldr	r0, .L146+16
	bl	process_bool
	.loc 1 547 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 553 0
	b	.L116
.L114:
	.loc 1 551 0
	bl	bad_key_beep
	.loc 1 553 0
	b	.L116
.L109:
	.loc 1 556 0
	ldr	r3, .L146+108
	ldr	r2, [r3, #0]
	ldr	r3, .L146+112
	str	r2, [r3, #0]
	.loc 1 557 0
	ldr	r3, [fp, #-56]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L146+108
	mov	r2, #0
	mov	r3, #1
	bl	process_uns32
	.loc 1 559 0
	bl	BUDGET_pre_process_periods_per_year_changed
	.loc 1 560 0
	b	.L116
.L110:
	.loc 1 564 0
	ldr	r3, .L146+68
	ldr	r2, [r3, #0]
	ldr	r3, .L146+116
	str	r2, [r3, #0]
	.loc 1 565 0
	ldr	r3, [fp, #-56]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L146+68
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 568 0
	bl	BUDGET_pre_entry_option_changed
	.loc 1 569 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 570 0
	b	.L116
.L111:
	.loc 1 574 0
	ldr	r3, [fp, #-56]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L146+64
	mov	r2, #10
	mov	r3, #300
	bl	process_uns32
	.loc 1 575 0
	b	.L116
.L112:
	.loc 1 579 0
	ldr	r3, .L146+68
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L117
	.loc 1 581 0
	bl	bad_key_beep
	.loc 1 591 0
	b	.L116
.L117:
	.loc 1 585 0
	ldr	r3, [fp, #-56]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L146+120
	mov	r2, #0
	mov	r3, #1
	bl	process_uns32
	.loc 1 589 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 591 0
	b	.L116
.L107:
	.loc 1 599 0
	bl	bad_key_beep
.L116:
	.loc 1 601 0
	bl	Refresh_Screen
	.loc 1 602 0
	b	.L60
.L87:
	.loc 1 606 0
	ldr	r4, [fp, #-56]
	bl	SYSTEM_num_systems_in_use
	mov	r2, r0
	ldr	r3, .L146+124
	ldr	r1, .L146+128
	str	r1, [sp, #0]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	ldr	r3, .L146+132
	bl	GROUP_process_NEXT_and_PREV
	.loc 1 607 0
	b	.L60
.L86:
	.loc 1 610 0
	ldr	r3, .L146+44
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	beq	.L120
	cmp	r3, #2
	beq	.L121
	b	.L142
.L120:
	.loc 1 613 0
	ldr	r0, .L146+136
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 614 0
	b	.L122
.L121:
	.loc 1 618 0
	ldr	r3, .L146+68
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L123
	.loc 1 620 0
	ldr	r3, .L146+44
	mov	r2, #1
	strh	r2, [r3, #0]	@ movhi
	.loc 1 621 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 627 0
	b	.L122
.L123:
	.loc 1 625 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 627 0
	b	.L122
.L142:
	.loc 1 629 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 631 0
	b	.L60
.L122:
	b	.L60
.L83:
	.loc 1 634 0
	ldr	r3, .L146+44
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	beq	.L126
	cmp	r3, #2
	beq	.L127
	b	.L143
.L126:
	.loc 1 637 0
	ldr	r0, .L146+136
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 638 0
	b	.L128
.L127:
	.loc 1 642 0
	ldr	r3, .L146+68
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L129
	.loc 1 644 0
	ldr	r3, .L146+44
	mov	r2, #1
	strh	r2, [r3, #0]	@ movhi
	.loc 1 645 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 651 0
	b	.L128
.L129:
	.loc 1 649 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 651 0
	b	.L128
.L143:
	.loc 1 653 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 655 0
	b	.L60
.L128:
	b	.L60
.L82:
	.loc 1 658 0
	ldr	r3, .L146+44
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #5
	bne	.L144
.L132:
	.loc 1 663 0
	ldr	r3, .L146+68
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L133
	.loc 1 665 0
	bl	bad_key_beep
	.loc 1 671 0
	b	.L135
.L133:
	.loc 1 669 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 671 0
	b	.L135
.L144:
	.loc 1 673 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 675 0
	b	.L60
.L135:
	b	.L60
.L85:
	.loc 1 678 0
	ldr	r3, .L146+44
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #5
	bne	.L145
.L137:
	.loc 1 683 0
	ldr	r3, .L146+68
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L138
	.loc 1 685 0
	bl	bad_key_beep
	.loc 1 691 0
	b	.L140
.L138:
	.loc 1 689 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 691 0
	b	.L140
.L145:
	.loc 1 693 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 695 0
	b	.L60
.L140:
	b	.L60
.L88:
	.loc 1 698 0
	ldr	r0, .L146+136
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 699 0
	b	.L60
.L81:
	.loc 1 702 0
	sub	r1, fp, #56
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L60:
	.loc 1 705 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L147:
	.align	2
.L146:
	.word	GuiLib_CurStructureNdx
	.word	719
	.word	585
	.word	722
	.word	GuiVar_BudgetInUse
	.word	FDTO_POC_close_budget_periods_per_year_dropdown
	.word	FDTO_BUDGET_SETUP_draw_menu
	.word	FDTO_POC_close_budget_option_dropdown
	.word	FDTO_POC_close_budget_mode_dropdown
	.word	BUDGET_SETUP_process_number_annual_periods_changed_dialog
	.word	BUDGET_SETUP_process_entry_option_changed_dialog
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_GROUP_list_item_index
	.word	FDTO_POC_show_budget_in_use_dropdown
	.word	FDTO_POC_show_budget_periods_per_year_dropdown
	.word	FDTO_POC_show_budget_option_dropdown
	.word	GuiVar_BudgetETPercentageUsed
	.word	GuiVar_BudgetEntryOptionIdx
	.word	FDTO_POC_show_budget_mode_dropdown
	.word	g_BUDGET_setup_button_pressed
	.word	g_BUDGET_setup_selected_group
	.word	FDTO_BUDGETS_draw_menu
	.word	BUDGETS_process_menu
	.word	GuiVar_MenuScreenToShow
	.word	GuiVar_BudgetMainlineName
	.word	621
	.word	622
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	g_BUDGET_SETUP_number_annual_periods
	.word	g_BUDGET_SETUP_entry_option
	.word	GuiVar_BudgetModeIdx
	.word	BUDGET_SETUP_extract_and_store_changes_from_GuiVars
	.word	BUDGET_SETUP_copy_group_into_guivars
	.word	SYSTEM_get_group_at_this_index
	.word	FDTO_BUDGET_SETUP_return_to_menu
.LFE13:
	.size	BUDGET_SETUP_process_group, .-BUDGET_SETUP_process_group
	.section	.text.FDTO_BUDGET_SETUP_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_BUDGET_SETUP_return_to_menu, %function
FDTO_BUDGET_SETUP_return_to_menu:
.LFB14:
	.loc 1 712 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI31:
	add	fp, sp, #4
.LCFI32:
	.loc 1 713 0
	ldr	r3, .L149
	ldr	r0, .L149+4
	mov	r1, r3
	bl	FDTO_GROUP_return_to_menu
	.loc 1 714 0
	ldmfd	sp!, {fp, pc}
.L150:
	.align	2
.L149:
	.word	BUDGET_SETUP_extract_and_store_changes_from_GuiVars
	.word	g_BUDGET_SETUP_editing_group
.LFE14:
	.size	FDTO_BUDGET_SETUP_return_to_menu, .-FDTO_BUDGET_SETUP_return_to_menu
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budget_setup.c\000"
	.section	.text.FDTO_BUDGET_SETUP_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_BUDGET_SETUP_draw_menu
	.type	FDTO_BUDGET_SETUP_draw_menu, %function
FDTO_BUDGET_SETUP_draw_menu:
.LFB15:
	.loc 1 718 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #16
.LCFI35:
	str	r0, [fp, #-8]
	.loc 1 719 0
	ldr	r3, .L154
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L154+4
	ldr	r3, .L154+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 724 0
	ldr	r3, .L154+12
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L152
	.loc 1 726 0
	ldr	r3, .L154+16
	ldr	r2, [r3, #0]
	ldr	r3, .L154+20
	str	r2, [r3, #0]
.L152:
	.loc 1 729 0
	bl	SYSTEM_num_systems_in_use
	mov	r3, r0
	ldr	r2, .L154+24
	ldr	r1, .L154+28
	str	r1, [sp, #0]
	str	r2, [sp, #4]
	mov	r2, #0
	str	r2, [sp, #8]
	ldr	r0, [fp, #-8]
	ldr	r1, .L154+32
	mov	r2, r3
	mov	r3, #12
	bl	FDTO_GROUP_draw_menu
	.loc 1 734 0
	ldr	r3, .L154+12
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L153
	.loc 1 736 0
	ldr	r3, .L154+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 737 0
	ldr	r3, .L154+32
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 738 0
	ldr	r3, .L154+36
	mov	r2, #5
	strh	r2, [r3, #0]	@ movhi
	.loc 1 739 0
	mov	r0, #0
	bl	FDTO_Redraw_Screen
.L153:
	.loc 1 742 0
	ldr	r3, .L154
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 743 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L155:
	.align	2
.L154:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	719
	.word	g_BUDGET_setup_button_pressed
	.word	g_BUDGET_setup_selected_group
	.word	g_GROUP_list_item_index
	.word	BUDGET_SETUP_copy_group_into_guivars
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	g_BUDGET_SETUP_editing_group
	.word	GuiLib_ActiveCursorFieldNo
.LFE15:
	.size	FDTO_BUDGET_SETUP_draw_menu, .-FDTO_BUDGET_SETUP_draw_menu
	.section	.text.BUDGET_SETUP_process_menu,"ax",%progbits
	.align	2
	.global	BUDGET_SETUP_process_menu
	.type	BUDGET_SETUP_process_menu, %function
BUDGET_SETUP_process_menu:
.LFB16:
	.loc 1 747 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #24
.LCFI38:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 748 0
	ldr	r3, .L157
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L157+4
	mov	r3, #748
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 750 0
	bl	SYSTEM_num_systems_in_use
	mov	r3, r0
	ldr	r1, .L157+8
	ldr	r2, .L157+12
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	ldr	r1, .L157+16
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	ldr	r2, .L157+20
	bl	GROUP_process_menu
	.loc 1 752 0
	ldr	r3, .L157
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 753 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L158:
	.align	2
.L157:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	BUDGET_SETUP_process_group
	.word	BUDGET_SETUP_copy_group_into_guivars
	.word	SYSTEM_get_group_at_this_index
	.word	g_BUDGET_SETUP_editing_group
.LFE16:
	.size	BUDGET_SETUP_process_menu, .-BUDGET_SETUP_process_menu
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI10-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI12-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI14-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI16-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI18-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI20-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI22-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI24-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI26-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI28-.LFB13
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI31-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI32-.LCFI31
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI33-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI36-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/dialog.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x742
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF90
	.byte	0x1
	.4byte	.LASF91
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x55
	.4byte	0x69
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x5e
	.4byte	0x7b
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x7b
	.uleb128 0x5
	.byte	0x4
	.4byte	0xa8
	.uleb128 0x6
	.4byte	0xaf
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x8
	.byte	0x6
	.byte	0x3
	.byte	0x22
	.4byte	0xd7
	.uleb128 0x9
	.ascii	"T\000"
	.byte	0x3
	.byte	0x24
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.ascii	"D\000"
	.byte	0x3
	.byte	0x26
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x28
	.4byte	0xb6
	.uleb128 0x8
	.byte	0x14
	.byte	0x3
	.byte	0x31
	.4byte	0x169
	.uleb128 0xa
	.4byte	.LASF16
	.byte	0x3
	.byte	0x33
	.4byte	0xd7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF17
	.byte	0x3
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x3
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF19
	.byte	0x3
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xa
	.4byte	.LASF20
	.byte	0x3
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x3
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x3
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x3
	.byte	0x39
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x3
	.byte	0x3b
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x3
	.byte	0x3d
	.4byte	0xe2
	.uleb128 0xb
	.byte	0x4
	.uleb128 0x3
	.4byte	.LASF26
	.byte	0x4
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF27
	.byte	0x5
	.byte	0x57
	.4byte	0x174
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x6
	.byte	0x4c
	.4byte	0x181
	.uleb128 0xc
	.4byte	0x3e
	.4byte	0x1a7
	.uleb128 0xd
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xc
	.4byte	0x70
	.4byte	0x1b7
	.uleb128 0xd
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.byte	0x7
	.byte	0x7c
	.4byte	0x1dc
	.uleb128 0xa
	.4byte	.LASF29
	.byte	0x7
	.byte	0x7e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x7
	.byte	0x80
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF31
	.byte	0x7
	.byte	0x82
	.4byte	0x1b7
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF32
	.uleb128 0xc
	.4byte	0x70
	.4byte	0x1fe
	.uleb128 0xd
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x3
	.4byte	.LASF33
	.byte	0x8
	.byte	0xd0
	.4byte	0x209
	.uleb128 0xe
	.4byte	.LASF33
	.byte	0x1
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF34
	.uleb128 0x8
	.byte	0x24
	.byte	0x9
	.byte	0x78
	.4byte	0x29d
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x9
	.byte	0x7b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x9
	.byte	0x83
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF37
	.byte	0x9
	.byte	0x86
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x9
	.byte	0x88
	.4byte	0x2ae
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x9
	.byte	0x8d
	.4byte	0x2c0
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x9
	.byte	0x92
	.4byte	0xa2
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xa
	.4byte	.LASF41
	.byte	0x9
	.byte	0x96
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xa
	.4byte	.LASF42
	.byte	0x9
	.byte	0x9a
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x9
	.byte	0x9c
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xf
	.byte	0x1
	.4byte	0x2a9
	.uleb128 0x10
	.4byte	0x2a9
	.byte	0
	.uleb128 0x11
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x29d
	.uleb128 0xf
	.byte	0x1
	.4byte	0x2c0
	.uleb128 0x10
	.4byte	0x1dc
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2b4
	.uleb128 0x3
	.4byte	.LASF44
	.byte	0x9
	.byte	0x9e
	.4byte	0x216
	.uleb128 0x12
	.4byte	.LASF49
	.byte	0x1
	.byte	0x4f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x321
	.uleb128 0x13
	.4byte	.LASF45
	.byte	0x1
	.byte	0x51
	.4byte	0x321
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF46
	.byte	0x1
	.byte	0x52
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF47
	.byte	0x1
	.byte	0x53
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF48
	.byte	0x1
	.byte	0x54
	.4byte	0x169
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1fe
	.uleb128 0x12
	.4byte	.LASF50
	.byte	0x1
	.byte	0x79
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x377
	.uleb128 0x13
	.4byte	.LASF45
	.byte	0x1
	.byte	0x7b
	.4byte	0x321
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF46
	.byte	0x1
	.byte	0x7c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF47
	.byte	0x1
	.byte	0x7d
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF48
	.byte	0x1
	.byte	0x7e
	.4byte	0x169
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.uleb128 0x14
	.4byte	.LASF51
	.byte	0x1
	.byte	0xae
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x14
	.4byte	.LASF52
	.byte	0x1
	.byte	0xc8
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x15
	.4byte	.LASF53
	.byte	0x1
	.byte	0xd1
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x15
	.4byte	.LASF54
	.byte	0x1
	.byte	0xf2
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x16
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x104
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x16
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x10a
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x16
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x110
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x16
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x12b
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x16
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x149
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.uleb128 0x16
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x155
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.uleb128 0x16
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x163
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.uleb128 0x17
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x16c
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x49f
	.uleb128 0x18
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x16c
	.4byte	0x49f
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x19
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x16e
	.4byte	0x2c6
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x1a
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x16f
	.4byte	0x321
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x11
	.4byte	0x1dc
	.uleb128 0x16
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x2c7
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x2cd
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x4e3
	.uleb128 0x18
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x2cd
	.4byte	0x4e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.4byte	0x97
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x2ea
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x512
	.uleb128 0x18
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x2ea
	.4byte	0x49f
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF67
	.byte	0xa
	.2byte	0x116
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF68
	.byte	0xa
	.2byte	0x118
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF69
	.byte	0xa
	.2byte	0x11f
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	0x2c
	.4byte	0x54c
	.uleb128 0xd
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF70
	.byte	0xa
	.2byte	0x120
	.4byte	0x53c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF71
	.byte	0xa
	.2byte	0x121
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF72
	.byte	0xa
	.2byte	0x126
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF73
	.byte	0xa
	.2byte	0x2ec
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF74
	.byte	0xb
	.2byte	0x127
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF75
	.byte	0xb
	.2byte	0x132
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF76
	.byte	0xc
	.byte	0x30
	.4byte	0x5b1
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x11
	.4byte	0x197
	.uleb128 0x13
	.4byte	.LASF77
	.byte	0xc
	.byte	0x34
	.4byte	0x5c7
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x11
	.4byte	0x197
	.uleb128 0x13
	.4byte	.LASF78
	.byte	0xc
	.byte	0x36
	.4byte	0x5dd
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x11
	.4byte	0x197
	.uleb128 0x13
	.4byte	.LASF79
	.byte	0xc
	.byte	0x38
	.4byte	0x5f3
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x11
	.4byte	0x197
	.uleb128 0x1d
	.4byte	.LASF80
	.byte	0xd
	.byte	0x65
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF81
	.byte	0x8
	.byte	0x33
	.4byte	0x616
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x11
	.4byte	0x1a7
	.uleb128 0x13
	.4byte	.LASF82
	.byte	0x8
	.byte	0x3f
	.4byte	0x62c
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x11
	.4byte	0x1ee
	.uleb128 0x1d
	.4byte	.LASF83
	.byte	0xe
	.byte	0xc6
	.4byte	0x18c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF84
	.byte	0xf
	.byte	0x2f
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF85
	.byte	0x1
	.byte	0x36
	.4byte	0x97
	.byte	0x5
	.byte	0x3
	.4byte	g_BUDGET_SETUP_editing_group
	.uleb128 0x13
	.4byte	.LASF86
	.byte	0x1
	.byte	0x3a
	.4byte	0x70
	.byte	0x5
	.byte	0x3
	.4byte	g_BUDGET_SETUP_number_annual_periods
	.uleb128 0x13
	.4byte	.LASF87
	.byte	0x1
	.byte	0x3e
	.4byte	0x70
	.byte	0x5
	.byte	0x3
	.4byte	g_BUDGET_SETUP_entry_option
	.uleb128 0x13
	.4byte	.LASF88
	.byte	0x1
	.byte	0x42
	.4byte	0x97
	.byte	0x5
	.byte	0x3
	.4byte	g_BUDGET_setup_button_pressed
	.uleb128 0x13
	.4byte	.LASF89
	.byte	0x1
	.byte	0x46
	.4byte	0x70
	.byte	0x5
	.byte	0x3
	.4byte	g_BUDGET_setup_selected_group
	.uleb128 0x1c
	.4byte	.LASF67
	.byte	0xa
	.2byte	0x116
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF68
	.byte	0xa
	.2byte	0x118
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF69
	.byte	0xa
	.2byte	0x11f
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF70
	.byte	0xa
	.2byte	0x120
	.4byte	0x53c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF71
	.byte	0xa
	.2byte	0x121
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF72
	.byte	0xa
	.2byte	0x126
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF73
	.byte	0xa
	.2byte	0x2ec
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF74
	.byte	0xb
	.2byte	0x127
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF75
	.byte	0xb
	.2byte	0x132
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF80
	.byte	0xd
	.byte	0x65
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF83
	.byte	0xe
	.byte	0xc6
	.4byte	0x18c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF84
	.byte	0xf
	.byte	0x2f
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI11
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI17
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI21
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI23
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI29
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI32
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x9c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF19:
	.ascii	"__year\000"
.LASF37:
	.ascii	"_03_structure_to_draw\000"
.LASF16:
	.ascii	"date_time\000"
.LASF36:
	.ascii	"_02_menu\000"
.LASF15:
	.ascii	"DATE_TIME\000"
.LASF56:
	.ascii	"FDTO_POC_show_budget_periods_per_year_dropdown\000"
.LASF67:
	.ascii	"GuiVar_BudgetEntryOptionIdx\000"
.LASF4:
	.ascii	"UNS_8\000"
.LASF48:
	.ascii	"dtcs\000"
.LASF86:
	.ascii	"g_BUDGET_SETUP_number_annual_periods\000"
.LASF62:
	.ascii	"FDTO_BUDGET_SETUP_return_to_menu\000"
.LASF59:
	.ascii	"FDTO_POC_close_budget_option_dropdown\000"
.LASF52:
	.ascii	"BUDGET_pre_entry_option_changed\000"
.LASF66:
	.ascii	"BUDGET_SETUP_process_menu\000"
.LASF53:
	.ascii	"BUDGET_SETUP_process_number_annual_periods_changed_"
	.ascii	"dialog\000"
.LASF26:
	.ascii	"portTickType\000"
.LASF35:
	.ascii	"_01_command\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF85:
	.ascii	"g_BUDGET_SETUP_editing_group\000"
.LASF73:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF47:
	.ascii	"year\000"
.LASF71:
	.ascii	"GuiVar_BudgetModeIdx\000"
.LASF29:
	.ascii	"keycode\000"
.LASF58:
	.ascii	"FDTO_POC_show_budget_option_dropdown\000"
.LASF92:
	.ascii	"BUDGET_SETUP_process_group\000"
.LASF31:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF23:
	.ascii	"__dayofweek\000"
.LASF90:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"short int\000"
.LASF69:
	.ascii	"GuiVar_BudgetInUse\000"
.LASF84:
	.ascii	"g_DIALOG_modal_result\000"
.LASF32:
	.ascii	"float\000"
.LASF12:
	.ascii	"long long int\000"
.LASF72:
	.ascii	"GuiVar_BudgetPeriodsPerYearIdx\000"
.LASF17:
	.ascii	"__day\000"
.LASF57:
	.ascii	"FDTO_POC_close_budget_periods_per_year_dropdown\000"
.LASF25:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF14:
	.ascii	"long int\000"
.LASF28:
	.ascii	"xSemaphoreHandle\000"
.LASF27:
	.ascii	"xQueueHandle\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF89:
	.ascii	"g_BUDGET_setup_selected_group\000"
.LASF41:
	.ascii	"_06_u32_argument1\000"
.LASF18:
	.ascii	"__month\000"
.LASF68:
	.ascii	"GuiVar_BudgetETPercentageUsed\000"
.LASF39:
	.ascii	"key_process_func_ptr\000"
.LASF43:
	.ascii	"_08_screen_to_draw\000"
.LASF64:
	.ascii	"pcomplete_redraw\000"
.LASF78:
	.ascii	"GuiFont_DecimalChar\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF3:
	.ascii	"signed char\000"
.LASF45:
	.ascii	"ptr_sys\000"
.LASF77:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF83:
	.ascii	"list_system_recursive_MUTEX\000"
.LASF63:
	.ascii	"pkey_event\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF74:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF65:
	.ascii	"FDTO_BUDGET_SETUP_draw_menu\000"
.LASF1:
	.ascii	"char\000"
.LASF50:
	.ascii	"BUDGET_SETUP_process_entry_option_changed\000"
.LASF79:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF88:
	.ascii	"g_BUDGET_setup_button_pressed\000"
.LASF87:
	.ascii	"g_BUDGET_SETUP_entry_option\000"
.LASF38:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF46:
	.ascii	"month\000"
.LASF42:
	.ascii	"_07_u32_argument2\000"
.LASF54:
	.ascii	"BUDGET_SETUP_process_entry_option_changed_dialog\000"
.LASF24:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF49:
	.ascii	"BUDGET_SETUP_process_annual_periods_changed\000"
.LASF7:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF82:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF40:
	.ascii	"_04_func_ptr\000"
.LASF61:
	.ascii	"FDTO_POC_close_budget_mode_dropdown\000"
.LASF55:
	.ascii	"FDTO_POC_show_budget_in_use_dropdown\000"
.LASF22:
	.ascii	"__seconds\000"
.LASF75:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF60:
	.ascii	"FDTO_POC_show_budget_mode_dropdown\000"
.LASF33:
	.ascii	"IRRIGATION_SYSTEM_GROUP_STRUCT\000"
.LASF30:
	.ascii	"repeats\000"
.LASF76:
	.ascii	"GuiFont_LanguageActive\000"
.LASF81:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF91:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budget_setup.c\000"
.LASF51:
	.ascii	"BUDGET_pre_process_periods_per_year_changed\000"
.LASF34:
	.ascii	"double\000"
.LASF9:
	.ascii	"UNS_32\000"
.LASF10:
	.ascii	"unsigned int\000"
.LASF44:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF80:
	.ascii	"g_GROUP_list_item_index\000"
.LASF70:
	.ascii	"GuiVar_BudgetMainlineName\000"
.LASF21:
	.ascii	"__minutes\000"
.LASF20:
	.ascii	"__hours\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
