	.file	"e_lights.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.g_LIGHTS_top_line,"aw",%nobits
	.align	2
	.type	g_LIGHTS_top_line, %object
	.size	g_LIGHTS_top_line, 4
g_LIGHTS_top_line:
	.space	4
	.section	.bss.g_LIGHTS_editing_group,"aw",%nobits
	.align	2
	.type	g_LIGHTS_editing_group, %object
	.size	g_LIGHTS_editing_group, 4
g_LIGHTS_editing_group:
	.space	4
	.section	.bss.g_LIGHTS_prev_cursor_pos,"aw",%nobits
	.align	2
	.type	g_LIGHTS_prev_cursor_pos, %object
	.size	g_LIGHTS_prev_cursor_pos, 4
g_LIGHTS_prev_cursor_pos:
	.space	4
	.section	.bss.g_LIGHTS_current_list_item_index,"aw",%nobits
	.align	2
	.type	g_LIGHTS_current_list_item_index, %object
	.size	g_LIGHTS_current_list_item_index, 4
g_LIGHTS_current_list_item_index:
	.space	4
	.section	.bss.g_LIGHTS_first_sundays_date,"aw",%nobits
	.align	2
	.type	g_LIGHTS_first_sundays_date, %object
	.size	g_LIGHTS_first_sundays_date, 4
g_LIGHTS_first_sundays_date:
	.space	4
	.global	g_LIGHTS_numeric_date
	.section	.bss.g_LIGHTS_numeric_date,"aw",%nobits
	.align	2
	.type	g_LIGHTS_numeric_date, %object
	.size	g_LIGHTS_numeric_date, 4
g_LIGHTS_numeric_date:
	.space	4
	.global	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.section	.bss.g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY,"aw",%nobits
	.align	2
	.type	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY, %object
	.size	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY, 224
g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY:
	.space	224
	.section	.bss.LIGHTS_MENU_items,"aw",%nobits
	.align	2
	.type	LIGHTS_MENU_items, %object
	.size	LIGHTS_MENU_items, 192
LIGHTS_MENU_items:
	.space	192
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_lights.c\000"
	.section	.text.LIGHTS_get_menu_index_for_displayed_light,"ax",%progbits
	.align	2
	.type	LIGHTS_get_menu_index_for_displayed_light, %function
LIGHTS_get_menu_index_for_displayed_light:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_lights.c"
	.loc 1 130 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #12
.LCFI2:
	.loc 1 139 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 143 0
	ldr	r3, .L6
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L6+4
	mov	r3, #143
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 145 0
	ldr	r3, .L6+8
	ldr	r2, [r3, #0]
	ldr	r3, .L6+12
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	str	r0, [fp, #-16]
	.loc 1 147 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L2
.L5:
	.loc 1 149 0
	ldr	r3, .L6+16
	ldr	r2, [fp, #-12]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L3
	.loc 1 151 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 153 0
	b	.L4
.L3:
	.loc 1 147 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L2:
	.loc 1 147 0 is_stmt 0 discriminator 1
	bl	LIGHTS_get_list_count
	mov	r2, r0
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L5
.L4:
	.loc 1 157 0 is_stmt 1
	ldr	r3, .L6
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 161 0
	ldr	r3, [fp, #-8]
	.loc 1 162 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L7:
	.align	2
.L6:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	LIGHTS_MENU_items
.LFE0:
	.size	LIGHTS_get_menu_index_for_displayed_light, .-LIGHTS_get_menu_index_for_displayed_light
	.section	.text.LIGHTS_start_time_exists_in_GUI_struct,"ax",%progbits
	.align	2
	.type	LIGHTS_start_time_exists_in_GUI_struct, %function
LIGHTS_start_time_exists_in_GUI_struct:
.LFB1:
	.loc 1 179 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	.loc 1 186 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 190 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L9
.L13:
	.loc 1 192 0
	ldr	r3, .L14
	ldr	r2, [fp, #-8]
	ldr	r2, [r3, r2, asl #4]
	ldr	r3, .L14+4
	cmp	r2, r3
	bne	.L10
	.loc 1 193 0 discriminator 1
	ldr	r1, .L14
	ldr	r2, [fp, #-8]
	mov	r3, #4
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	.loc 1 192 0 discriminator 1
	ldr	r3, .L14+4
	cmp	r2, r3
	beq	.L11
.L10:
	.loc 1 195 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 197 0
	b	.L12
.L11:
	.loc 1 190 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L9:
	.loc 1 190 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #13
	bls	.L13
.L12:
	.loc 1 203 0 is_stmt 1
	ldr	r3, [fp, #-4]
	.loc 1 204 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L15:
	.align	2
.L14:
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.word	86400
.LFE1:
	.size	LIGHTS_start_time_exists_in_GUI_struct, .-LIGHTS_start_time_exists_in_GUI_struct
	.section	.text.LIGHTS_stop_time_exists_in_GUI_struct,"ax",%progbits
	.align	2
	.type	LIGHTS_stop_time_exists_in_GUI_struct, %function
LIGHTS_stop_time_exists_in_GUI_struct:
.LFB2:
	.loc 1 221 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	.loc 1 228 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 232 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L17
.L21:
	.loc 1 234 0
	ldr	r1, .L22
	ldr	r2, [fp, #-8]
	mov	r3, #8
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L22+4
	cmp	r2, r3
	bne	.L18
	.loc 1 235 0 discriminator 1
	ldr	r1, .L22
	ldr	r2, [fp, #-8]
	mov	r3, #12
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	.loc 1 234 0 discriminator 1
	ldr	r3, .L22+4
	cmp	r2, r3
	beq	.L19
.L18:
	.loc 1 237 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 239 0
	b	.L20
.L19:
	.loc 1 232 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L17:
	.loc 1 232 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #13
	bls	.L21
.L20:
	.loc 1 245 0 is_stmt 1
	ldr	r3, [fp, #-4]
	.loc 1 246 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L23:
	.align	2
.L22:
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.word	86400
.LFE2:
	.size	LIGHTS_stop_time_exists_in_GUI_struct, .-LIGHTS_stop_time_exists_in_GUI_struct
	.section	.text.LIGHTS_get_next_scheduled_time,"ax",%progbits
	.align	2
	.type	LIGHTS_get_next_scheduled_time, %function
LIGHTS_get_next_scheduled_time:
.LFB3:
	.loc 1 274 0
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #52
.LCFI11:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	str	r2, [fp, #-52]
	str	r3, [fp, #-56]
	.loc 1 285 0
	mvn	r3, #0
	strh	r3, [fp, #-36]	@ movhi
	.loc 1 286 0
	mvn	r3, #0
	str	r3, [fp, #-40]
	.loc 1 288 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 299 0
	ldr	r3, .L61
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L61+4
	ldr	r3, .L61+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 301 0
	ldr	r0, [fp, #-52]
	bl	LIGHTS_get_light_at_this_index
	str	r0, [fp, #-24]
	.loc 1 304 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r3
	bl	LIGHTS_get_day_index
	str	r0, [fp, #-28]
	.loc 1 307 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L25
	.loc 1 307 0 is_stmt 0 discriminator 1
	bl	LIGHTS_start_time_exists_in_GUI_struct
	mov	r3, r0
	cmp	r3, #0
	bne	.L26
.L25:
	.loc 1 307 0 discriminator 2
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	bne	.L27
	.loc 1 308 0 is_stmt 1
	bl	LIGHTS_stop_time_exists_in_GUI_struct
	mov	r3, r0
	cmp	r3, #0
	beq	.L27
.L26:
	.loc 1 311 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L28
	.loc 1 315 0
	ldr	r3, .L61+12
	ldr	r2, [fp, #-28]
	ldr	r3, [r3, r2, asl #4]
	str	r3, [fp, #-16]
	b	.L29
.L28:
	.loc 1 319 0
	ldr	r1, .L61+12
	ldr	r2, [fp, #-28]
	mov	r3, #8
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
.L29:
	.loc 1 322 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L61+16
	cmp	r2, r3
	bhi	.L30
	.loc 1 325 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bhi	.L30
	.loc 1 328 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 329 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-36]	@ movhi
	.loc 1 330 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-40]
.L30:
	.loc 1 335 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L31
	.loc 1 337 0
	ldr	r1, .L61+12
	ldr	r2, [fp, #-28]
	mov	r3, #4
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	b	.L32
.L31:
	.loc 1 341 0
	ldr	r1, .L61+12
	ldr	r2, [fp, #-28]
	mov	r3, #12
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
.L32:
	.loc 1 344 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L61+16
	cmp	r2, r3
	bhi	.L33
	.loc 1 347 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bhi	.L33
	.loc 1 350 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 353 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bls	.L33
	.loc 1 355 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-36]	@ movhi
	.loc 1 356 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-40]
.L33:
	.loc 1 361 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L34
	.loc 1 366 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L35
.L43:
	.loc 1 370 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L36
	.loc 1 372 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-8]
	add	r1, r2, r3
	mov	r2, r1, lsr #1
	ldr	r3, .L61+20
	umull	r0, r3, r2, r3
	mov	r2, r3, lsr #2
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	ldr	r3, .L61+12
	ldr	r3, [r3, r2, asl #4]
	str	r3, [fp, #-16]
	b	.L37
.L36:
	.loc 1 376 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-8]
	add	r1, r2, r3
	mov	r2, r1, lsr #1
	ldr	r3, .L61+20
	umull	r0, r3, r2, r3
	mov	r2, r3, lsr #2
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	ldr	r1, .L61+12
	mov	r3, #8
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
.L37:
	.loc 1 379 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L61+16
	cmp	r2, r3
	bhi	.L38
	.loc 1 381 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 384 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-36]	@ movhi
	.loc 1 385 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-40]
.L38:
	.loc 1 388 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L39
	.loc 1 390 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-8]
	add	r1, r2, r3
	mov	r2, r1, lsr #1
	ldr	r3, .L61+20
	umull	r0, r3, r2, r3
	mov	r2, r3, lsr #2
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	ldr	r1, .L61+12
	mov	r3, #4
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	b	.L40
.L39:
	.loc 1 394 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-8]
	add	r1, r2, r3
	mov	r2, r1, lsr #1
	ldr	r3, .L61+20
	umull	r0, r3, r2, r3
	mov	r2, r3, lsr #2
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	ldr	r1, .L61+12
	mov	r3, #12
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
.L40:
	.loc 1 397 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L61+16
	cmp	r2, r3
	bhi	.L41
	.loc 1 400 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bls	.L41
	.loc 1 402 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 404 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-36]	@ movhi
	.loc 1 405 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-40]
.L41:
	.loc 1 409 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L59
.L42:
	.loc 1 366 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L35:
	.loc 1 366 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #14
	bls	.L43
	b	.L34
.L59:
	.loc 1 412 0 is_stmt 1
	mov	r0, r0	@ nop
.L34:
	.loc 1 421 0
	ldr	r3, .L61+24
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #31
	bne	.L60
	.loc 1 424 0
	ldr	r3, .L61+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_day_index
	mov	r2, r0
	ldr	r3, .L61+32
	ldr	r3, [r3, #0]
	add	r3, r2, r3
	str	r3, [fp, #-32]
	.loc 1 427 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L45
	.loc 1 429 0
	ldr	r3, .L61+36
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	str	r3, [fp, #-12]
	b	.L46
.L45:
	.loc 1 433 0
	ldr	r3, .L61+40
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	str	r3, [fp, #-12]
.L46:
	.loc 1 437 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L61+16
	cmp	r2, r3
	bhi	.L47
	.loc 1 442 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bne	.L48
	.loc 1 442 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bcc	.L49
.L48:
	.loc 1 443 0 is_stmt 1 discriminator 2
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3
	.loc 1 442 0 discriminator 2
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bcs	.L47
.L49:
	.loc 1 446 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 449 0
	ldrh	r3, [fp, #-36]
	mov	r2, r3
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bcc	.L47
	.loc 1 453 0
	ldrh	r3, [fp, #-36]
	mov	r2, r3
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bhi	.L50
	.loc 1 453 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bls	.L47
.L50:
	.loc 1 456 0 is_stmt 1
	ldr	r3, [fp, #-32]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-36]	@ movhi
	.loc 1 457 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-40]
.L47:
	.loc 1 466 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L51
	.loc 1 468 0
	ldr	r3, .L61+44
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	str	r3, [fp, #-12]
	b	.L52
.L51:
	.loc 1 472 0
	ldr	r3, .L61+48
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	str	r3, [fp, #-12]
.L52:
	.loc 1 476 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L61+16
	cmp	r2, r3
	bhi	.L60
	.loc 1 481 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bne	.L53
	.loc 1 481 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bcc	.L54
.L53:
	.loc 1 482 0 is_stmt 1 discriminator 2
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3
	.loc 1 481 0 discriminator 2
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bcs	.L60
.L54:
	.loc 1 485 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 488 0
	ldrh	r3, [fp, #-36]
	mov	r2, r3
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bcc	.L60
	.loc 1 492 0
	ldrh	r3, [fp, #-36]
	mov	r2, r3
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bhi	.L55
	.loc 1 492 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bls	.L60
.L55:
	.loc 1 495 0 is_stmt 1
	ldr	r3, [fp, #-32]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-36]	@ movhi
	.loc 1 496 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-40]
	.loc 1 421 0
	b	.L60
.L27:
	.loc 1 511 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L56
.L60:
	.loc 1 421 0
	mov	r0, r0	@ nop
.L56:
	.loc 1 514 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L57
	.loc 1 517 0
	ldrh	r2, [fp, #-36]
	ldr	r3, [fp, #-56]
	and	r1, r2, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #4]
	mov	r2, r2, lsr #8
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	mov	r1, #0
	orr	r2, r1, r2
	strb	r2, [r3, #5]
	.loc 1 518 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-56]
	and	r1, r2, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #0]
	mov	r1, r2, lsr #8
	and	r1, r1, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #1]
	mov	r1, r2, lsr #16
	and	r1, r1, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #2]
	mov	r2, r2, lsr #24
	mov	r1, #0
	orr	r2, r1, r2
	strb	r2, [r3, #3]
	b	.L58
.L57:
	.loc 1 523 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-56]
	and	r1, r2, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #4]
	mov	r2, r2, lsr #8
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	mov	r1, #0
	orr	r2, r1, r2
	strb	r2, [r3, #5]
	.loc 1 524 0
	ldr	r3, [fp, #-44]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-56]
	and	r1, r2, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #0]
	mov	r1, r2, lsr #8
	and	r1, r1, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #1]
	mov	r1, r2, lsr #16
	and	r1, r1, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #2]
	mov	r2, r2, lsr #24
	mov	r1, #0
	orr	r2, r1, r2
	strb	r2, [r3, #3]
.L58:
	.loc 1 527 0
	ldr	r3, .L61
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 529 0
	ldr	r3, [fp, #-20]
	.loc 1 531 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L62:
	.align	2
.L61:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	299
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.word	86399
	.word	-1840700269
	.word	GuiLib_CurStructureNdx
	.word	g_LIGHTS_numeric_date
	.word	g_LIGHTS_first_sundays_date
	.word	GuiVar_LightsStartTime1InMinutes
	.word	GuiVar_LightsStopTime1InMinutes
	.word	GuiVar_LightsStartTime2InMinutes
	.word	GuiVar_LightsStopTime2InMinutes
.LFE3:
	.size	LIGHTS_get_next_scheduled_time, .-LIGHTS_get_next_scheduled_time
	.section .rodata
	.align	2
.LC1:
	.ascii	"%s, %s\000"
	.section	.text.LIGHTS_update_next_scheduled_date_and_time_panel,"ax",%progbits
	.align	2
	.global	LIGHTS_update_next_scheduled_date_and_time_panel
	.type	LIGHTS_update_next_scheduled_date_and_time_panel, %function
LIGHTS_update_next_scheduled_date_and_time_panel:
.LFB4:
	.loc 1 548 0
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI12:
	add	fp, sp, #8
.LCFI13:
	sub	sp, sp, #96
.LCFI14:
	.loc 1 563 0
	bl	LIGHTS_start_time_exists_in_GUI_struct
	str	r0, [fp, #-12]
	.loc 1 564 0
	bl	LIGHTS_stop_time_exists_in_GUI_struct
	str	r0, [fp, #-16]
	.loc 1 568 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L64
	.loc 1 568 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L64
	.loc 1 570 0 is_stmt 1
	ldr	r3, .L67
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 574 0
	sub	r3, fp, #96
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 577 0
	ldr	r3, .L67+4
	ldr	r2, [r3, #0]
	ldr	r3, .L67+8
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r2, r0
	sub	r1, fp, #96
	sub	r3, fp, #80
	mov	r0, r1
	mov	r1, #1
	bl	LIGHTS_get_next_scheduled_time
	.loc 1 579 0
	ldr	r3, [fp, #-80]
	sub	r2, fp, #72
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, #8
	mov	r2, r3
	mov	r3, #0
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r4, r0
	.loc 1 583 0
	ldrh	r3, [fp, #-76]
	.loc 1 579 0
	sub	r2, fp, #64
	mov	r1, #200
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #100
	bl	GetDateStr
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r0, .L67+12
	mov	r1, #33
	ldr	r2, .L67+16
	mov	r3, r4
	bl	snprintf
	.loc 1 591 0
	ldr	r3, .L67+4
	ldr	r2, [r3, #0]
	ldr	r3, .L67+8
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r2, r0
	sub	r1, fp, #96
	sub	r3, fp, #88
	mov	r0, r1
	mov	r1, #0
	bl	LIGHTS_get_next_scheduled_time
	.loc 1 593 0
	ldr	r3, [fp, #-88]
	sub	r2, fp, #72
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, #8
	mov	r2, r3
	mov	r3, #0
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r4, r0
	.loc 1 597 0
	ldrh	r3, [fp, #-84]
	.loc 1 593 0
	sub	r2, fp, #64
	mov	r1, #200
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #100
	bl	GetDateStr
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r0, .L67+20
	mov	r1, #33
	ldr	r2, .L67+16
	mov	r3, r4
	bl	snprintf
	b	.L63
.L64:
	.loc 1 599 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L66
	.loc 1 599 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L66
	.loc 1 601 0 is_stmt 1
	ldr	r3, .L67
	mov	r2, #2
	str	r2, [r3, #0]
	b	.L63
.L66:
	.loc 1 606 0
	ldr	r3, .L67
	mov	r2, #1
	str	r2, [r3, #0]
.L63:
	.loc 1 608 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L68:
	.align	2
.L67:
	.word	GuiVar_LightScheduleState
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	GuiVar_LightScheduledStart
	.word	.LC1
	.word	GuiVar_LightScheduledStop
.LFE4:
	.size	LIGHTS_update_next_scheduled_date_and_time_panel, .-LIGHTS_update_next_scheduled_date_and_time_panel
	.section	.text.LIGHTS_copy_group_into_guivars,"ax",%progbits
	.align	2
	.type	LIGHTS_copy_group_into_guivars, %function
LIGHTS_copy_group_into_guivars:
.LFB5:
	.loc 1 629 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #4
.LCFI17:
	str	r0, [fp, #-8]
	.loc 1 633 0
	ldr	r0, [fp, #-8]
	bl	LIGHTS_copy_light_struct_into_guivars
	.loc 1 636 0
	ldr	r0, [fp, #-8]
	bl	LIGHTS_copy_file_schedule_into_global_daily_schedule
	.loc 1 639 0
	ldr	r3, .L70
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_day_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_daily_schedule_to_guivars
	.loc 1 641 0
	ldr	r3, .L70+4
	ldr	r2, [r3, #0]
	ldr	r3, .L70+8
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	mov	r1, #0
	bl	LIGHTS_populate_group_name
	.loc 1 642 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L71:
	.align	2
.L70:
	.word	g_LIGHTS_numeric_date
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
.LFE5:
	.size	LIGHTS_copy_group_into_guivars, .-LIGHTS_copy_group_into_guivars
	.section	.text.LIGHTS_process_NEXT_and_PREV,"ax",%progbits
	.align	2
	.type	LIGHTS_process_NEXT_and_PREV, %function
LIGHTS_process_NEXT_and_PREV:
.LFB6:
	.loc 1 815 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #8
.LCFI20:
	str	r0, [fp, #-12]
	.loc 1 818 0
	bl	LIGHTS_get_menu_index_for_displayed_light
	str	r0, [fp, #-8]
	.loc 1 820 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	beq	.L73
	.loc 1 820 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #84
	bne	.L74
.L73:
	.loc 1 824 0 is_stmt 1
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.loc 1 826 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_to_line
	.loc 1 828 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 830 0
	ldr	r3, .L80
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L75
	.loc 1 832 0
	mov	r0, #0
	mov	r1, #0
	bl	SCROLL_BOX_up_or_down
.L75:
	.loc 1 837 0
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	b	.L76
.L74:
	.loc 1 843 0
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.loc 1 845 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_to_line
	.loc 1 847 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 849 0
	ldr	r3, .L80
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L77
	.loc 1 851 0
	mov	r0, #0
	mov	r1, #4
	bl	SCROLL_BOX_up_or_down
.L77:
	.loc 1 856 0
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
.L76:
	.loc 1 861 0
	ldr	r3, .L80
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L78
	.loc 1 863 0
	bl	LIGHTS_extract_and_store_changes_from_GuiVars
	.loc 1 865 0
	ldr	r3, .L80
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	LIGHTS_get_index_using_ptr_to_light_struct
	mov	r2, r0
	ldr	r3, .L80+4
	str	r2, [r3, #0]
	.loc 1 869 0
	ldr	r3, .L80+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_copy_group_into_guivars
	b	.L79
.L78:
	.loc 1 873 0
	bl	bad_key_beep
.L79:
	.loc 1 881 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 882 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L81:
	.align	2
.L80:
	.word	LIGHTS_MENU_items
	.word	g_GROUP_list_item_index
.LFE6:
	.size	LIGHTS_process_NEXT_and_PREV, .-LIGHTS_process_NEXT_and_PREV
	.section	.text.LIGHTS_set_start_of_14_day_schedule,"ax",%progbits
	.align	2
	.type	LIGHTS_set_start_of_14_day_schedule, %function
LIGHTS_set_start_of_14_day_schedule:
.LFB7:
	.loc 1 902 0
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI21:
	add	fp, sp, #8
.LCFI22:
	sub	sp, sp, #60
.LCFI23:
	.loc 1 910 0
	sub	r3, fp, #64
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 913 0
	ldrh	r3, [fp, #-60]
	mov	r2, r3
	ldr	r3, .L83
	str	r2, [r3, #0]
	.loc 1 915 0
	ldr	r3, .L83
	ldr	r3, [r3, #0]
	sub	r2, fp, #56
	mov	r1, #200
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #100
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L83+4
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 918 0
	ldrh	r3, [fp, #-60]
	mov	r4, r3
	ldr	r3, .L83
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_day_index
	mov	r3, r0
	rsb	r2, r3, r4
	ldr	r3, .L83+8
	str	r2, [r3, #0]
	.loc 1 919 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L84:
	.align	2
.L83:
	.word	g_LIGHTS_numeric_date
	.word	GuiVar_LightsDate
	.word	g_LIGHTS_first_sundays_date
.LFE7:
	.size	LIGHTS_set_start_of_14_day_schedule, .-LIGHTS_set_start_of_14_day_schedule
	.section	.text.LIGHTS_process_daily_schedule_change,"ax",%progbits
	.align	2
	.type	LIGHTS_process_daily_schedule_change, %function
LIGHTS_process_daily_schedule_change:
.LFB8:
	.loc 1 938 0
	@ args = 0, pretend = 0, frame = 60
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #68
.LCFI26:
	str	r0, [fp, #-64]
	.loc 1 945 0
	sub	r3, fp, #60
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 951 0
	ldr	r3, .L86
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_day_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_guivars_to_daily_schedule
	.loc 1 957 0
	ldrh	r3, [fp, #-56]
	mov	r2, r3
	ldrh	r3, [fp, #-56]
	add	r3, r3, #13
	mov	r1, #1
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	ldr	r0, [fp, #-64]
	ldr	r1, .L86
	bl	process_uns32
	.loc 1 959 0
	ldr	r3, .L86
	ldr	r3, [r3, #0]
	sub	r2, fp, #52
	mov	r1, #200
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #48
	mov	r2, r3
	mov	r3, #100
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L86+4
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 962 0
	ldr	r3, .L86
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_day_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_daily_schedule_to_guivars
	.loc 1 964 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 965 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L87:
	.align	2
.L86:
	.word	g_LIGHTS_numeric_date
	.word	GuiVar_LightsDate
.LFE8:
	.size	LIGHTS_process_daily_schedule_change, .-LIGHTS_process_daily_schedule_change
	.section	.text.LIGHTS_process_this_time,"ax",%progbits
	.align	2
	.type	LIGHTS_process_this_time, %function
LIGHTS_process_this_time:
.LFB9:
	.loc 1 989 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #24
.LCFI29:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 995 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L89
	.loc 1 995 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #80
	bne	.L89
	.loc 1 997 0 is_stmt 1
	bl	good_key_beep
	.loc 1 1000 0
	ldr	r3, [fp, #-12]
	mov	r2, #1440
	str	r2, [r3, #0]
	b	.L90
.L89:
	.loc 1 1002 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #1440
	bne	.L91
	.loc 1 1002 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #84
	bne	.L91
	.loc 1 1004 0 is_stmt 1
	bl	good_key_beep
	.loc 1 1007 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L90
.L91:
	.loc 1 1013 0
	mov	r3, #10
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, #0
	mov	r3, #1440
	bl	process_uns32
.L90:
	.loc 1 1017 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #1440
	moveq	r2, #0
	movne	r2, #1
	ldr	r3, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 1023 0
	ldr	r3, .L92
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_day_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_guivars_to_daily_schedule
	.loc 1 1027 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 1028 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L93:
	.align	2
.L92:
	.word	g_LIGHTS_numeric_date
.LFE9:
	.size	LIGHTS_process_this_time, .-LIGHTS_process_this_time
	.section	.text.LIGHTS_process_group,"ax",%progbits
	.align	2
	.type	LIGHTS_process_group, %function
LIGHTS_process_group:
.LFB10:
	.loc 1 1046 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #8
.LCFI32:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 1047 0
	ldr	r3, .L139
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L139+4
	cmp	r2, r3
	bne	.L135
.L96:
	.loc 1 1050 0
	ldr	r3, [fp, #-12]
	cmp	r3, #67
	beq	.L97
	.loc 1 1050 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #2
	bne	.L98
	ldr	r3, .L139+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #49
	bne	.L98
.L97:
	.loc 1 1052 0 is_stmt 1
	bl	LIGHTS_extract_and_store_group_name_from_GuiVars
.L98:
	.loc 1 1055 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	ldr	r2, .L139+12
	bl	KEYBOARD_process_key
	.loc 1 1056 0
	b	.L94
.L135:
	.loc 1 1059 0
	ldr	r3, [fp, #-12]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L100
.L109:
	.word	.L101
	.word	.L102
	.word	.L103
	.word	.L104
	.word	.L105
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L106
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L106
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L107
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L108
	.word	.L100
	.word	.L100
	.word	.L100
	.word	.L108
.L103:
	.loc 1 1062 0
	ldr	r3, .L139+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L136
.L111:
	.loc 1 1065 0
	mov	r0, #158
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	.loc 1 1066 0
	mov	r0, r0	@ nop
	.loc 1 1071 0
	b	.L94
.L136:
	.loc 1 1069 0
	bl	bad_key_beep
	.loc 1 1071 0
	b	.L94
.L106:
	.loc 1 1075 0
	ldr	r3, [fp, #-12]
	mov	r0, r3
	bl	LIGHTS_process_NEXT_and_PREV
	.loc 1 1076 0
	b	.L94
.L108:
	.loc 1 1080 0
	ldr	r3, .L139+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #1
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L113
.L119:
	.word	.L114
	.word	.L115
	.word	.L116
	.word	.L117
	.word	.L118
.L114:
	.loc 1 1083 0
	ldr	r3, [fp, #-12]
	mov	r0, r3
	bl	LIGHTS_process_daily_schedule_change
	.loc 1 1084 0
	b	.L120
.L115:
	.loc 1 1087 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L139+16
	ldr	r3, [r3, #0]
	mov	r0, r2
	ldr	r1, .L139+20
	mov	r2, r3
	ldr	r3, .L139+24
	bl	LIGHTS_process_this_time
	.loc 1 1088 0
	b	.L120
.L116:
	.loc 1 1091 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L139+28
	ldr	r3, [r3, #0]
	mov	r0, r2
	ldr	r1, .L139+32
	mov	r2, r3
	ldr	r3, .L139+36
	bl	LIGHTS_process_this_time
	.loc 1 1092 0
	b	.L120
.L117:
	.loc 1 1095 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L139+20
	ldr	r3, [r3, #0]
	mov	r0, r2
	ldr	r1, .L139+16
	mov	r2, r3
	ldr	r3, .L139+40
	bl	LIGHTS_process_this_time
	.loc 1 1096 0
	b	.L120
.L118:
	.loc 1 1099 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L139+32
	ldr	r3, [r3, #0]
	mov	r0, r2
	ldr	r1, .L139+28
	mov	r2, r3
	ldr	r3, .L139+44
	bl	LIGHTS_process_this_time
	.loc 1 1100 0
	b	.L120
.L113:
	.loc 1 1103 0
	bl	bad_key_beep
	.loc 1 1105 0
	b	.L94
.L120:
	b	.L94
.L105:
	.loc 1 1108 0
	ldr	r3, .L139+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #4
	beq	.L123
	cmp	r3, #5
	beq	.L124
	cmp	r3, #3
	bne	.L137
.L122:
	.loc 1 1111 0
	mov	r0, #1
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 1112 0
	b	.L125
.L123:
	.loc 1 1115 0
	mov	r0, #2
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 1116 0
	b	.L125
.L124:
	.loc 1 1119 0
	mov	r0, #3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 1120 0
	b	.L125
.L137:
	.loc 1 1123 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 1125 0
	b	.L94
.L125:
	b	.L94
.L101:
	.loc 1 1128 0
	ldr	r3, .L139+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #2
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L126
.L130:
	.word	.L127
	.word	.L128
	.word	.L129
	.word	.L129
.L127:
	.loc 1 1131 0
	mov	r0, #4
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 1132 0
	b	.L131
.L128:
	.loc 1 1135 0
	mov	r0, #5
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 1136 0
	b	.L131
.L129:
	.loc 1 1140 0
	bl	bad_key_beep
	.loc 1 1141 0
	b	.L131
.L126:
	.loc 1 1144 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 1146 0
	b	.L94
.L131:
	b	.L94
.L102:
	.loc 1 1149 0
	ldr	r3, .L139+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L138
.L133:
	.loc 1 1152 0
	ldr	r0, .L139+48
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 1153 0
	mov	r0, r0	@ nop
	.loc 1 1158 0
	b	.L94
.L138:
	.loc 1 1156 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 1158 0
	b	.L94
.L104:
	.loc 1 1161 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 1162 0
	b	.L94
.L107:
	.loc 1 1165 0
	ldr	r0, .L139+48
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 1166 0
	b	.L94
.L100:
	.loc 1 1169 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L94:
	.loc 1 1172 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L140:
	.align	2
.L139:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_LIGHTS_draw_menu
	.word	GuiVar_LightsStopTime1InMinutes
	.word	GuiVar_LightsStartTime1InMinutes
	.word	GuiVar_LightsStartTimeEnabled1
	.word	GuiVar_LightsStopTime2InMinutes
	.word	GuiVar_LightsStartTime2InMinutes
	.word	GuiVar_LightsStartTimeEnabled2
	.word	GuiVar_LightsStopTimeEnabled1
	.word	GuiVar_LightsStopTimeEnabled2
	.word	FDTO_LIGHTS_return_to_menu
.LFE10:
	.size	LIGHTS_process_group, .-LIGHTS_process_group
	.section	.text.LIGHTS_populate_pointers_of_physically_available_lights_for_display,"ax",%progbits
	.align	2
	.type	LIGHTS_populate_pointers_of_physically_available_lights_for_display, %function
LIGHTS_populate_pointers_of_physically_available_lights_for_display:
.LFB11:
	.loc 1 1193 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #12
.LCFI35:
	.loc 1 1202 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1204 0
	ldr	r0, .L145
	mov	r1, #0
	mov	r2, #192
	bl	memset
	.loc 1 1208 0
	ldr	r3, .L145+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L145+8
	ldr	r3, .L145+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1210 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L142
.L144:
	.loc 1 1212 0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_get_light_at_this_index
	str	r0, [fp, #-16]
	.loc 1 1214 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L143
	.loc 1 1214 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-16]
	bl	LIGHTS_get_physically_available
	mov	r3, r0
	cmp	r3, #0
	beq	.L143
	.loc 1 1216 0 is_stmt 1
	ldr	r3, .L145
	ldr	r2, [fp, #-8]
	ldr	r1, [fp, #-16]
	str	r1, [r3, r2, asl #2]
	.loc 1 1218 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L143:
	.loc 1 1210 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L142:
	.loc 1 1210 0 is_stmt 0 discriminator 1
	bl	LIGHTS_get_list_count
	mov	r2, r0
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L144
	.loc 1 1222 0 is_stmt 1
	ldr	r3, .L145+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1226 0
	ldr	r3, [fp, #-8]
	.loc 1 1227 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L146:
	.align	2
.L145:
	.word	LIGHTS_MENU_items
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	1208
.LFE11:
	.size	LIGHTS_populate_pointers_of_physically_available_lights_for_display, .-LIGHTS_populate_pointers_of_physically_available_lights_for_display
	.section .rodata
	.align	2
.LC2:
	.ascii	"Lights L%d\000"
	.section	.text.LIGHTS_load_light_name_into_guivar,"ax",%progbits
	.align	2
	.type	LIGHTS_load_light_name_into_guivar, %function
LIGHTS_load_light_name_into_guivar:
.LFB12:
	.loc 1 1245 0
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI36:
	add	fp, sp, #8
.LCFI37:
	sub	sp, sp, #52
.LCFI38:
	mov	r3, r0
	strh	r3, [fp, #-60]	@ movhi
	.loc 1 1250 0
	ldr	r3, .L151
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L151+4
	ldr	r3, .L151+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1252 0
	ldrsh	r4, [fp, #-60]
	bl	LIGHTS_get_list_count
	mov	r3, r0
	cmp	r4, r3
	bcs	.L148
	.loc 1 1254 0
	ldrsh	r2, [fp, #-60]
	ldr	r3, .L151+12
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	nm_GROUP_get_name
	mov	r3, r0
	sub	r2, fp, #56
	mov	r0, r2
	mov	r1, r3
	mov	r2, #48
	bl	strlcpy
	.loc 1 1256 0
	ldr	r0, .L151+16
	bl	strlen
	mov	r3, r0
	sub	r2, fp, #56
	mov	r0, r2
	ldr	r1, .L151+16
	mov	r2, r3
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	bne	.L149
	.loc 1 1258 0
	ldrsh	r2, [fp, #-60]
	ldr	r3, .L151+12
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	LIGHTS_get_output_index
	mov	r3, r0
	add	r3, r3, #1
	ldr	r0, .L151+20
	mov	r1, #49
	ldr	r2, .L151+24
	bl	snprintf
	b	.L150
.L149:
	.loc 1 1262 0
	sub	r3, fp, #56
	ldr	r0, .L151+20
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	b	.L150
.L148:
	.loc 1 1267 0
	ldr	r0, .L151+4
	ldr	r1, .L151+28
	bl	Alert_group_not_found_with_filename
.L150:
	.loc 1 1270 0
	ldr	r3, .L151
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1271 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L152:
	.align	2
.L151:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	1250
	.word	LIGHTS_MENU_items
	.word	LIGHTS_DEFAULT_NAME
	.word	GuiVar_itmGroupName
	.word	.LC2
	.word	1267
.LFE12:
	.size	LIGHTS_load_light_name_into_guivar, .-LIGHTS_load_light_name_into_guivar
	.section	.text.LIGHTS_get_ptr_to_physically_available_light,"ax",%progbits
	.align	2
	.type	LIGHTS_get_ptr_to_physically_available_light, %function
LIGHTS_get_ptr_to_physically_available_light:
.LFB13:
	.loc 1 1289 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI39:
	add	fp, sp, #0
.LCFI40:
	sub	sp, sp, #4
.LCFI41:
	str	r0, [fp, #-4]
	.loc 1 1292 0
	ldr	r3, .L154
	ldr	r2, [fp, #-4]
	ldr	r3, [r3, r2, asl #2]
	.loc 1 1293 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L155:
	.align	2
.L154:
	.word	LIGHTS_MENU_items
.LFE13:
	.size	LIGHTS_get_ptr_to_physically_available_light, .-LIGHTS_get_ptr_to_physically_available_light
	.section	.text.FDTO_LIGHTS_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_LIGHTS_return_to_menu, %function
FDTO_LIGHTS_return_to_menu:
.LFB14:
	.loc 1 1317 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI42:
	add	fp, sp, #4
.LCFI43:
	.loc 1 1318 0
	ldr	r3, .L157
	ldr	r0, .L157+4
	mov	r1, r3
	bl	FDTO_GROUP_return_to_menu
	.loc 1 1319 0
	ldmfd	sp!, {fp, pc}
.L158:
	.align	2
.L157:
	.word	LIGHTS_extract_and_store_changes_from_GuiVars
	.word	g_LIGHTS_editing_group
.LFE14:
	.size	FDTO_LIGHTS_return_to_menu, .-FDTO_LIGHTS_return_to_menu
	.section	.text.FDTO_LIGHTS_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_LIGHTS_draw_menu
	.type	FDTO_LIGHTS_draw_menu, %function
FDTO_LIGHTS_draw_menu:
.LFB15:
	.loc 1 1344 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI44:
	add	fp, sp, #4
.LCFI45:
	sub	sp, sp, #20
.LCFI46:
	str	r0, [fp, #-20]
	.loc 1 1351 0
	ldr	r3, .L165
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L165+4
	ldr	r3, .L165+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1354 0
	bl	LIGHTS_populate_pointers_of_physically_available_lights_for_display
	str	r0, [fp, #-12]
	.loc 1 1356 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L160
	.loc 1 1358 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1360 0
	ldr	r3, .L165+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1362 0
	ldr	r3, .L165+16
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1364 0
	ldr	r3, .L165+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1366 0
	ldr	r3, .L165+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_ptr_to_physically_available_light
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_get_index_using_ptr_to_light_struct
	mov	r2, r0
	ldr	r3, .L165+24
	str	r2, [r3, #0]
	.loc 1 1369 0
	bl	LIGHTS_set_start_of_14_day_schedule
	.loc 1 1372 0
	ldr	r3, .L165+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_copy_group_into_guivars
	.loc 1 1374 0
	ldr	r3, .L165+28
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L161
.L160:
	.loc 1 1378 0
	ldr	r3, .L165+32
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
	.loc 1 1380 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_GetTopLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L165+12
	str	r2, [r3, #0]
	.loc 1 1383 0
	ldr	r3, .L165+36
	ldr	r2, [r3, #0]
	ldr	r3, .L165+40
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_light_struct_into_guivars
.L161:
	.loc 1 1386 0
	bl	LIGHTS_update_next_scheduled_date_and_time_panel
	.loc 1 1394 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #31
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 1397 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	.loc 1 1399 0
	bl	LIGHTS_get_menu_index_for_displayed_light
	str	r0, [fp, #-16]
	.loc 1 1401 0
	ldr	r3, .L165+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L162
	.loc 1 1405 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r2, .L165+12
	ldr	r2, [r2, #0]
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	mov	r2, r2, asl #16
	mov	r2, r2, asr #16
	str	r2, [sp, #0]
	mov	r0, #0
	ldr	r1, .L165+44
	mov	r2, r3
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	.loc 1 1406 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	mov	r1, r3
	bl	GuiLib_ScrollBox_SetIndicator
	.loc 1 1407 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
	b	.L163
.L162:
	.loc 1 1414 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L164
	.loc 1 1414 0 is_stmt 0 discriminator 1
	ldr	r3, .L165+24
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bls	.L164
	.loc 1 1416 0 is_stmt 1
	ldr	r3, .L165+24
	ldr	r3, [r3, #0]
	sub	r2, r3, #11
	ldr	r3, .L165+12
	str	r2, [r3, #0]
.L164:
	.loc 1 1423 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r1, .L165+12
	ldr	r1, [r1, #0]
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r1, r1, asl #16
	mov	r1, r1, asr #16
	str	r1, [sp, #0]
	mov	r0, #0
	ldr	r1, .L165+44
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L163:
	.loc 1 1426 0
	ldr	r3, .L165
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1428 0
	bl	GuiLib_Refresh
	.loc 1 1429 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L166:
	.align	2
.L165:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	1351
	.word	g_LIGHTS_top_line
	.word	g_LIGHTS_editing_group
	.word	g_LIGHTS_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	g_LIGHTS_prev_cursor_pos
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	LIGHTS_load_light_name_into_guivar
.LFE15:
	.size	FDTO_LIGHTS_draw_menu, .-FDTO_LIGHTS_draw_menu
	.section	.text.LIGHTS_process_menu,"ax",%progbits
	.align	2
	.global	LIGHTS_process_menu
	.type	LIGHTS_process_menu, %function
LIGHTS_process_menu:
.LFB16:
	.loc 1 1451 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI47:
	add	fp, sp, #4
.LCFI48:
	sub	sp, sp, #8
.LCFI49:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 1452 0
	ldr	r3, .L177
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L168
	.loc 1 1454 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	LIGHTS_process_group
	b	.L167
.L168:
	.loc 1 1458 0
	ldr	r3, [fp, #-12]
	cmp	r3, #67
	ldrls	pc, [pc, r3, asl #2]
	b	.L170
.L175:
	.word	.L171
	.word	.L170
	.word	.L172
	.word	.L172
	.word	.L171
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L173
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L173
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L170
	.word	.L174
.L172:
	.loc 1 1462 0
	bl	good_key_beep
	.loc 1 1464 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L177+4
	str	r2, [r3, #0]
	.loc 1 1466 0
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L177+12
	ldr	r3, .L177+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1468 0
	ldr	r3, .L177+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_ptr_to_physically_available_light
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_get_index_using_ptr_to_light_struct
	mov	r2, r0
	ldr	r3, .L177+20
	str	r2, [r3, #0]
	.loc 1 1470 0
	ldr	r3, .L177
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1472 0
	ldr	r3, .L177+24
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1474 0
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1476 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 1477 0
	b	.L167
.L173:
	.loc 1 1483 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	bne	.L176
	.loc 1 1485 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L171
.L176:
	.loc 1 1489 0
	mov	r3, #4
	str	r3, [fp, #-12]
.L171:
	.loc 1 1496 0
	ldr	r3, [fp, #-12]
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	.loc 1 1498 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L177+4
	str	r2, [r3, #0]
	.loc 1 1500 0
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L177+12
	ldr	r3, .L177+28
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1502 0
	ldr	r3, .L177+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_ptr_to_physically_available_light
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_get_index_using_ptr_to_light_struct
	mov	r2, r0
	ldr	r3, .L177+20
	str	r2, [r3, #0]
	.loc 1 1504 0
	ldr	r3, .L177+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_copy_group_into_guivars
	.loc 1 1506 0
	ldr	r3, .L177+32
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1508 0
	ldr	r3, .L177+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1510 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 1511 0
	b	.L167
.L174:
	.loc 1 1514 0
	ldr	r3, .L177+36
	ldr	r2, [r3, #0]
	ldr	r0, .L177+40
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L177+44
	str	r2, [r3, #0]
.L170:
	.loc 1 1520 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L167:
	.loc 1 1523 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L178:
	.align	2
.L177:
	.word	g_LIGHTS_editing_group
	.word	g_LIGHTS_current_list_item_index
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	1466
	.word	g_GROUP_list_item_index
	.word	GuiLib_ActiveCursorFieldNo
	.word	1500
	.word	g_LIGHTS_prev_cursor_pos
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE16:
	.size	LIGHTS_process_menu, .-LIGHTS_process_menu
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI44-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI45-.LCFI44
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI47-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI48-.LCFI47
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/lights.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_lights.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xbbc
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF130
	.byte	0x1
	.4byte	.LASF131
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x67
	.4byte	0x82
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x70
	.uleb128 0x5
	.byte	0x4
	.4byte	0xa8
	.uleb128 0x6
	.4byte	0xaf
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x4
	.byte	0x57
	.4byte	0xaf
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x5
	.byte	0x4c
	.4byte	0xc3
	.uleb128 0x9
	.4byte	0x33
	.4byte	0xe9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0x10e
	.uleb128 0xc
	.4byte	.LASF18
	.byte	0x6
	.byte	0x7e
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x6
	.byte	0x80
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x6
	.byte	0x82
	.4byte	0xe9
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x129
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0xb
	.byte	0x6
	.byte	0x7
	.byte	0x22
	.4byte	0x14a
	.uleb128 0xd
	.ascii	"T\000"
	.byte	0x7
	.byte	0x24
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"D\000"
	.byte	0x7
	.byte	0x26
	.4byte	0x41
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x7
	.byte	0x28
	.4byte	0x129
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF22
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x16c
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x17c
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x3
	.4byte	.LASF23
	.byte	0x8
	.byte	0x5d
	.4byte	0x187
	.uleb128 0xe
	.4byte	.LASF23
	.byte	0x1
	.uleb128 0xb
	.byte	0x10
	.byte	0x8
	.byte	0x6b
	.4byte	0x1ce
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x8
	.byte	0x70
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x8
	.byte	0x72
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x8
	.byte	0x74
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x8
	.byte	0x76
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x8
	.byte	0x78
	.4byte	0x18d
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x1e9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x7
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF29
	.uleb128 0xb
	.byte	0x24
	.byte	0x9
	.byte	0x78
	.4byte	0x277
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x9
	.byte	0x7b
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x9
	.byte	0x83
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF32
	.byte	0x9
	.byte	0x86
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x9
	.byte	0x88
	.4byte	0x288
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF34
	.byte	0x9
	.byte	0x8d
	.4byte	0x29a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF35
	.byte	0x9
	.byte	0x92
	.4byte	0xa2
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF36
	.byte	0x9
	.byte	0x96
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF37
	.byte	0x9
	.byte	0x9a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF38
	.byte	0x9
	.byte	0x9c
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xf
	.byte	0x1
	.4byte	0x283
	.uleb128 0x10
	.4byte	0x283
	.byte	0
	.uleb128 0x11
	.4byte	0x53
	.uleb128 0x5
	.byte	0x4
	.4byte	0x277
	.uleb128 0xf
	.byte	0x1
	.4byte	0x29a
	.uleb128 0x10
	.4byte	0x10e
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x28e
	.uleb128 0x3
	.4byte	.LASF39
	.byte	0x9
	.byte	0x9e
	.4byte	0x1f0
	.uleb128 0xb
	.byte	0x4
	.byte	0x1
	.byte	0x62
	.4byte	0x2c2
	.uleb128 0xc
	.4byte	.LASF40
	.byte	0x1
	.byte	0x64
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF41
	.byte	0x1
	.byte	0x66
	.4byte	0x2ab
	.uleb128 0x12
	.4byte	.LASF43
	.byte	0x1
	.byte	0x81
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x311
	.uleb128 0x13
	.4byte	.LASF42
	.byte	0x1
	.byte	0x83
	.4byte	0x311
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x85
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.ascii	"i\000"
	.byte	0x1
	.byte	0x87
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x17c
	.uleb128 0x12
	.4byte	.LASF44
	.byte	0x1
	.byte	0xb2
	.byte	0x1
	.4byte	0x97
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x34f
	.uleb128 0x14
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xb4
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x13
	.4byte	.LASF45
	.byte	0x1
	.byte	0xb6
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.4byte	.LASF46
	.byte	0x1
	.byte	0xdc
	.byte	0x1
	.4byte	0x97
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x387
	.uleb128 0x14
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xde
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x13
	.4byte	.LASF45
	.byte	0x1
	.byte	0xe0
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x111
	.byte	0x1
	.4byte	0x97
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x459
	.uleb128 0x16
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x111
	.4byte	0x459
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x16
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x111
	.4byte	0x469
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x16
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x111
	.4byte	0x46e
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x16
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x111
	.4byte	0x473
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x17
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x113
	.4byte	0x311
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x17
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x115
	.4byte	0x14a
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x17
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x117
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x117
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x17
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x118
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x17
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x119
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x11a
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x11b
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x11
	.4byte	0x45e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x464
	.uleb128 0x11
	.4byte	0x14a
	.uleb128 0x11
	.4byte	0x97
	.uleb128 0x11
	.4byte	0x65
	.uleb128 0x5
	.byte	0x4
	.4byte	0x14a
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x223
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x502
	.uleb128 0x17
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x225
	.4byte	0x119
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x17
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x227
	.4byte	0x1d9
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.uleb128 0x17
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x229
	.4byte	0x14a
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x17
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x22a
	.4byte	0x14a
	.byte	0x3
	.byte	0x91
	.sleb128 -92
	.uleb128 0x17
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x22c
	.4byte	0x14a
	.byte	0x3
	.byte	0x91
	.sleb128 -100
	.uleb128 0x17
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x22e
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x22f
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x274
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x52b
	.uleb128 0x16
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x274
	.4byte	0x46e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x32e
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x563
	.uleb128 0x16
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x32e
	.4byte	0x46e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x330
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x385
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x59c
	.uleb128 0x17
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x387
	.4byte	0x119
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x1a
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x389
	.4byte	0x14a
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.byte	0
	.uleb128 0x19
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x3a9
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x5e4
	.uleb128 0x16
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x3a9
	.4byte	0x46e
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x17
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x3ab
	.4byte	0x119
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x1a
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x3ad
	.4byte	0x14a
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.byte	0
	.uleb128 0x19
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x3dc
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x63a
	.uleb128 0x16
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x3dc
	.4byte	0x46e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x3dc
	.4byte	0x63a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x3dc
	.4byte	0x46e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x3dc
	.4byte	0x640
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x65
	.uleb128 0x5
	.byte	0x4
	.4byte	0x97
	.uleb128 0x19
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x415
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x66f
	.uleb128 0x16
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x415
	.4byte	0x66f
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x11
	.4byte	0x10e
	.uleb128 0x15
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x4a8
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x6bd
	.uleb128 0x17
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x4aa
	.4byte	0x311
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x4ac
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1a
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x4ae
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x19
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x4dc
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x6f5
	.uleb128 0x16
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x4dc
	.4byte	0x283
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x17
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x4de
	.4byte	0x119
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.byte	0
	.uleb128 0x15
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x508
	.byte	0x1
	.4byte	0x311
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x722
	.uleb128 0x16
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x508
	.4byte	0x46e
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF132
	.byte	0x1
	.2byte	0x524
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x53f
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x78e
	.uleb128 0x16
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x53f
	.4byte	0x469
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x17
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x541
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x543
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x545
	.4byte	0x77
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x5aa
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x7b8
	.uleb128 0x16
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x5aa
	.4byte	0x10e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x7c8
	.uleb128 0xa
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF93
	.byte	0xa
	.2byte	0x26a
	.4byte	0x7b8
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF94
	.byte	0xa
	.2byte	0x289
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x7f4
	.uleb128 0xa
	.4byte	0x25
	.byte	0x20
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF95
	.byte	0xa
	.2byte	0x28c
	.4byte	0x7e4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF96
	.byte	0xa
	.2byte	0x28d
	.4byte	0x7e4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF97
	.byte	0xa
	.2byte	0x28e
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF98
	.byte	0xa
	.2byte	0x28f
	.4byte	0x7b8
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF99
	.byte	0xa
	.2byte	0x291
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF100
	.byte	0xa
	.2byte	0x293
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF101
	.byte	0xa
	.2byte	0x294
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF102
	.byte	0xa
	.2byte	0x295
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF103
	.byte	0xa
	.2byte	0x296
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF104
	.byte	0xa
	.2byte	0x297
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF105
	.byte	0xa
	.2byte	0x298
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF106
	.byte	0xa
	.2byte	0x299
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF107
	.byte	0xa
	.2byte	0x29a
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF108
	.byte	0xa
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0xb
	.2byte	0x127
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF110
	.byte	0xb
	.2byte	0x132
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF111
	.byte	0xc
	.byte	0x30
	.4byte	0x8e5
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x11
	.4byte	0xd9
	.uleb128 0x13
	.4byte	.LASF112
	.byte	0xc
	.byte	0x34
	.4byte	0x8fb
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x11
	.4byte	0xd9
	.uleb128 0x13
	.4byte	.LASF113
	.byte	0xc
	.byte	0x36
	.4byte	0x911
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x11
	.4byte	0xd9
	.uleb128 0x13
	.4byte	.LASF114
	.byte	0xc
	.byte	0x38
	.4byte	0x927
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x11
	.4byte	0xd9
	.uleb128 0x1d
	.4byte	.LASF115
	.byte	0xd
	.byte	0x65
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x944
	.uleb128 0x1e
	.byte	0
	.uleb128 0x1d
	.4byte	.LASF116
	.byte	0x8
	.byte	0x64
	.4byte	0x951
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	0x939
	.uleb128 0x1d
	.4byte	.LASF117
	.byte	0xe
	.byte	0x16
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x1ce
	.4byte	0x973
	.uleb128 0xa
	.4byte	0x25
	.byte	0xd
	.byte	0
	.uleb128 0x1d
	.4byte	.LASF118
	.byte	0xe
	.byte	0x1a
	.4byte	0x963
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF119
	.byte	0xf
	.byte	0x33
	.4byte	0x991
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x11
	.4byte	0x15c
	.uleb128 0x13
	.4byte	.LASF120
	.byte	0xf
	.byte	0x3f
	.4byte	0x9a7
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x11
	.4byte	0x16c
	.uleb128 0x1d
	.4byte	.LASF121
	.byte	0x10
	.byte	0xfe
	.4byte	0xce
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x2a0
	.4byte	0x9c9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x31
	.byte	0
	.uleb128 0x1d
	.4byte	.LASF122
	.byte	0x9
	.byte	0xac
	.4byte	0x9b9
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF123
	.byte	0x9
	.byte	0xae
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF124
	.byte	0x1
	.byte	0x35
	.4byte	0x65
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_top_line
	.uleb128 0x13
	.4byte	.LASF125
	.byte	0x1
	.byte	0x37
	.4byte	0x97
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_editing_group
	.uleb128 0x13
	.4byte	.LASF126
	.byte	0x1
	.byte	0x39
	.4byte	0x65
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_prev_cursor_pos
	.uleb128 0x13
	.4byte	.LASF127
	.byte	0x1
	.byte	0x3f
	.4byte	0x65
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_current_list_item_index
	.uleb128 0x13
	.4byte	.LASF128
	.byte	0x1
	.byte	0x45
	.4byte	0x65
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_first_sundays_date
	.uleb128 0x9
	.4byte	0x2c2
	.4byte	0xa48
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0x13
	.4byte	.LASF129
	.byte	0x1
	.byte	0x68
	.4byte	0xa38
	.byte	0x5
	.byte	0x3
	.4byte	LIGHTS_MENU_items
	.uleb128 0x1c
	.4byte	.LASF93
	.byte	0xa
	.2byte	0x26a
	.4byte	0x7b8
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF94
	.byte	0xa
	.2byte	0x289
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF95
	.byte	0xa
	.2byte	0x28c
	.4byte	0x7e4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF96
	.byte	0xa
	.2byte	0x28d
	.4byte	0x7e4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF97
	.byte	0xa
	.2byte	0x28e
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF98
	.byte	0xa
	.2byte	0x28f
	.4byte	0x7b8
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF99
	.byte	0xa
	.2byte	0x291
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF100
	.byte	0xa
	.2byte	0x293
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF101
	.byte	0xa
	.2byte	0x294
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF102
	.byte	0xa
	.2byte	0x295
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF103
	.byte	0xa
	.2byte	0x296
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF104
	.byte	0xa
	.2byte	0x297
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF105
	.byte	0xa
	.2byte	0x298
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF106
	.byte	0xa
	.2byte	0x299
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF107
	.byte	0xa
	.2byte	0x29a
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF108
	.byte	0xa
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0xb
	.2byte	0x127
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF110
	.byte	0xb
	.2byte	0x132
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF115
	.byte	0xd
	.byte	0x65
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF116
	.byte	0x8
	.byte	0x64
	.4byte	0xb6f
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	0x939
	.uleb128 0x1f
	.4byte	.LASF117
	.byte	0x1
	.byte	0x49
	.4byte	0x65
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_numeric_date
	.uleb128 0x1f
	.4byte	.LASF118
	.byte	0x1
	.byte	0x50
	.4byte	0x963
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.uleb128 0x1d
	.4byte	.LASF121
	.byte	0x10
	.byte	0xfe
	.4byte	0xce
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF122
	.byte	0x9
	.byte	0xac
	.4byte	0x9b9
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	.LASF123
	.byte	0x9
	.byte	0xae
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI43
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI45
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI48
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x9c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF124:
	.ascii	"g_LIGHTS_top_line\000"
.LASF52:
	.ascii	"lls_ptr\000"
.LASF71:
	.ascii	"LIGHTS_set_start_of_14_day_schedule\000"
.LASF32:
	.ascii	"_03_structure_to_draw\000"
.LASF95:
	.ascii	"GuiVar_LightScheduledStart\000"
.LASF31:
	.ascii	"_02_menu\000"
.LASF113:
	.ascii	"GuiFont_DecimalChar\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF36:
	.ascii	"_06_u32_argument1\000"
.LASF72:
	.ascii	"LIGHTS_process_daily_schedule_change\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF18:
	.ascii	"keycode\000"
.LASF21:
	.ascii	"DATE_TIME\000"
.LASF12:
	.ascii	"long long int\000"
.LASF3:
	.ascii	"signed char\000"
.LASF42:
	.ascii	"llight\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF125:
	.ascii	"g_LIGHTS_editing_group\000"
.LASF84:
	.ascii	"LIGHTS_get_ptr_to_physically_available_light\000"
.LASF118:
	.ascii	"g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY\000"
.LASF79:
	.ascii	"LIGHTS_populate_pointers_of_physically_available_li"
	.ascii	"ghts_for_display\000"
.LASF102:
	.ascii	"GuiVar_LightsStartTimeEnabled1\000"
.LASF103:
	.ascii	"GuiVar_LightsStartTimeEnabled2\000"
.LASF14:
	.ascii	"long int\000"
.LASF47:
	.ascii	"LIGHTS_get_next_scheduled_time\000"
.LASF49:
	.ascii	"look_for_start\000"
.LASF65:
	.ascii	"light_has_start_time\000"
.LASF128:
	.ascii	"g_LIGHTS_first_sundays_date\000"
.LASF39:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF78:
	.ascii	"pkey_event\000"
.LASF7:
	.ascii	"short int\000"
.LASF123:
	.ascii	"screen_history_index\000"
.LASF29:
	.ascii	"double\000"
.LASF97:
	.ascii	"GuiVar_LightScheduleState\000"
.LASF114:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF24:
	.ascii	"start_time_1_in_seconds\000"
.LASF73:
	.ascii	"LIGHTS_process_this_time\000"
.LASF94:
	.ascii	"GuiVar_LightsBoxIndex_0\000"
.LASF30:
	.ascii	"_01_command\000"
.LASF129:
	.ascii	"LIGHTS_MENU_items\000"
.LASF27:
	.ascii	"stop_time_2_in_seconds\000"
.LASF77:
	.ascii	"LIGHTS_process_group\000"
.LASF131:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_lights.c\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF108:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF115:
	.ascii	"g_GROUP_list_item_index\000"
.LASF48:
	.ascii	"dt_ptr\000"
.LASF10:
	.ascii	"INT_32\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF61:
	.ascii	"time_str\000"
.LASF43:
	.ascii	"LIGHTS_get_menu_index_for_displayed_light\000"
.LASF40:
	.ascii	"light_ptr\000"
.LASF34:
	.ascii	"key_process_func_ptr\000"
.LASF53:
	.ascii	"temp_date\000"
.LASF126:
	.ascii	"g_LIGHTS_prev_cursor_pos\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF130:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF51:
	.ascii	"next_dt\000"
.LASF23:
	.ascii	"LIGHT_STRUCT\000"
.LASF33:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF69:
	.ascii	"pkeycode\000"
.LASF82:
	.ascii	"pindex_0_i16\000"
.LASF16:
	.ascii	"xQueueHandle\000"
.LASF112:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF50:
	.ascii	"plight_index_0\000"
.LASF38:
	.ascii	"_08_screen_to_draw\000"
.LASF25:
	.ascii	"start_time_2_in_seconds\000"
.LASF92:
	.ascii	"LIGHTS_process_menu\000"
.LASF45:
	.ascii	"day_index\000"
.LASF127:
	.ascii	"g_LIGHTS_current_list_item_index\000"
.LASF96:
	.ascii	"GuiVar_LightScheduledStop\000"
.LASF57:
	.ascii	"displayed_time_in_seconds\000"
.LASF83:
	.ascii	"str_48\000"
.LASF132:
	.ascii	"FDTO_LIGHTS_return_to_menu\000"
.LASF20:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF98:
	.ascii	"GuiVar_LightsDate\000"
.LASF111:
	.ascii	"GuiFont_LanguageActive\000"
.LASF75:
	.ascii	"the_other_time_in_minutes\000"
.LASF56:
	.ascii	"displayed_date\000"
.LASF28:
	.ascii	"LIGHTS_DAY_STRUCT\000"
.LASF80:
	.ascii	"lavailable_lights\000"
.LASF91:
	.ascii	"lcursor_to_select\000"
.LASF100:
	.ascii	"GuiVar_LightsStartTime1InMinutes\000"
.LASF22:
	.ascii	"float\000"
.LASF101:
	.ascii	"GuiVar_LightsStartTime2InMinutes\000"
.LASF85:
	.ascii	"pindex_0\000"
.LASF99:
	.ascii	"GuiVar_LightsOutputIndex_0\000"
.LASF66:
	.ascii	"light_has_stop_time\000"
.LASF17:
	.ascii	"xSemaphoreHandle\000"
.LASF62:
	.ascii	"lnext_start_dt\000"
.LASF93:
	.ascii	"GuiVar_itmGroupName\000"
.LASF46:
	.ascii	"LIGHTS_stop_time_exists_in_GUI_struct\000"
.LASF44:
	.ascii	"LIGHTS_start_time_exists_in_GUI_struct\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF110:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF70:
	.ascii	"lmenu_index_for_displayed_light\000"
.LASF59:
	.ascii	"found_a_usable_time\000"
.LASF67:
	.ascii	"LIGHTS_copy_group_into_guivars\000"
.LASF15:
	.ascii	"portTickType\000"
.LASF122:
	.ascii	"ScreenHistory\000"
.LASF64:
	.ascii	"current_dt\000"
.LASF81:
	.ascii	"LIGHTS_load_light_name_into_guivar\000"
.LASF55:
	.ascii	"todays_index\000"
.LASF68:
	.ascii	"LIGHTS_process_NEXT_and_PREV\000"
.LASF88:
	.ascii	"pcomplete_redraw\000"
.LASF1:
	.ascii	"char\000"
.LASF90:
	.ascii	"lactive_line\000"
.LASF106:
	.ascii	"GuiVar_LightsStopTimeEnabled1\000"
.LASF107:
	.ascii	"GuiVar_LightsStopTimeEnabled2\000"
.LASF121:
	.ascii	"list_lights_recursive_MUTEX\000"
.LASF89:
	.ascii	"llights_in_use\000"
.LASF19:
	.ascii	"repeats\000"
.LASF76:
	.ascii	"time_is_enabled\000"
.LASF120:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF41:
	.ascii	"LIGHTS_MENU_SCROLL_BOX_ITEM\000"
.LASF109:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF86:
	.ascii	"LIGHTS_update_next_scheduled_date_and_time_panel\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF63:
	.ascii	"lnext_stop_dt\000"
.LASF87:
	.ascii	"FDTO_LIGHTS_draw_menu\000"
.LASF37:
	.ascii	"_07_u32_argument2\000"
.LASF54:
	.ascii	"day_counter\000"
.LASF35:
	.ascii	"_04_func_ptr\000"
.LASF116:
	.ascii	"LIGHTS_DEFAULT_NAME\000"
.LASF60:
	.ascii	"date_str\000"
.LASF74:
	.ascii	"this_time_in_minutes\000"
.LASF26:
	.ascii	"stop_time_1_in_seconds\000"
.LASF104:
	.ascii	"GuiVar_LightsStopTime1InMinutes\000"
.LASF105:
	.ascii	"GuiVar_LightsStopTime2InMinutes\000"
.LASF119:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF58:
	.ascii	"next_time_in_schedule_in_seconds\000"
.LASF117:
	.ascii	"g_LIGHTS_numeric_date\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
