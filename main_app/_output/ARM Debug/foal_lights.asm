	.file	"foal_lights.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.rodata.FOAL_LIGHTS_PRE_TEST_STRING,"a",%progbits
	.align	2
	.type	FOAL_LIGHTS_PRE_TEST_STRING, %object
	.size	FOAL_LIGHTS_PRE_TEST_STRING, 16
FOAL_LIGHTS_PRE_TEST_STRING:
	.ascii	"f lights v03\000"
	.space	3
	.global	__largest_lights_start_time_delta
	.section	.bss.__largest_lights_start_time_delta,"aw",%nobits
	.align	2
	.type	__largest_lights_start_time_delta, %object
	.size	__largest_lights_start_time_delta, 4
__largest_lights_start_time_delta:
	.space	4
	.section .rodata
	.align	2
.LC0:
	.ascii	"F_L: invalid list pointer\000"
	.align	2
.LC1:
	.ascii	"F_L: Bad inputs to find_on_a_list\000"
	.section	.text.nm_FOAL_LIGHTS_find_this_light_on_the_list,"ax",%progbits
	.align	2
	.type	nm_FOAL_LIGHTS_find_this_light_on_the_list, %function
nm_FOAL_LIGHTS_find_this_light_on_the_list:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_lights.c"
	.loc 1 209 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #16
.LCFI2:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	.loc 1 210 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 217 0
	ldr	r3, [fp, #-16]
	cmp	r3, #11
	bhi	.L2
	.loc 1 217 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #3
	bhi	.L2
	.loc 1 220 0 is_stmt 1
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-20]
	bl	LIGHTS_get_lights_array_index
	mov	r2, r0
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L8
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 222 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L7
	.loc 1 227 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L8+4
	cmp	r2, r3
	bne	.L4
	.loc 1 229 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	bne	.L7
	.loc 1 231 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 222 0
	b	.L7
.L4:
	.loc 1 235 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L8+8
	cmp	r2, r3
	bne	.L5
	.loc 1 237 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #20]
	cmp	r3, #0
	bne	.L7
	.loc 1 239 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 222 0
	b	.L7
.L5:
	.loc 1 244 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 247 0
	ldr	r0, .L8+12
	bl	Alert_Message
	.loc 1 222 0
	b	.L7
.L2:
	.loc 1 254 0
	ldr	r0, .L8+16
	bl	Alert_Message
	b	.L6
.L7:
	.loc 1 222 0
	mov	r0, r0	@ nop
.L6:
	.loc 1 257 0
	ldr	r3, [fp, #-8]
	.loc 1 259 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	foal_lights+56
	.word	foal_lights+16
	.word	foal_lights+36
	.word	.LC0
	.word	.LC1
.LFE0:
	.size	nm_FOAL_LIGHTS_find_this_light_on_the_list, .-nm_FOAL_LIGHTS_find_this_light_on_the_list
	.section .rodata
	.align	2
.LC2:
	.ascii	"F_L: llc pointer failure\000"
	.align	2
.LC3:
	.ascii	"F_L: Bad inputs to return llc pointer\000"
	.section	.text.nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light,"ax",%progbits
	.align	2
	.type	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light, %function
nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light:
.LFB1:
	.loc 1 278 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #16
.LCFI5:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	.loc 1 279 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 286 0
	ldr	r3, [fp, #-12]
	cmp	r3, #11
	bhi	.L11
	.loc 1 286 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #3
	bhi	.L11
	.loc 1 289 0 is_stmt 1
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	bl	LIGHTS_get_lights_array_index
	mov	r2, r0
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L16
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 291 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L12
	.loc 1 293 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L15
	.loc 1 297 0
	ldr	r0, [fp, #-8]
	mov	r1, #0
	mov	r2, #36
	bl	memset
	.loc 1 300 0
	ldr	r3, [fp, #-12]
	and	r2, r3, #255
	ldr	r3, [fp, #-8]
	strb	r2, [r3, #34]
	.loc 1 301 0
	ldr	r3, [fp, #-16]
	and	r2, r3, #255
	ldr	r3, [fp, #-8]
	strb	r2, [r3, #35]
	.loc 1 291 0
	b	.L15
.L12:
	.loc 1 307 0
	ldr	r0, .L16+4
	bl	Alert_Message
	.loc 1 291 0
	b	.L15
.L11:
	.loc 1 313 0
	ldr	r0, .L16+8
	bl	Alert_Message
	b	.L14
.L15:
	.loc 1 291 0
	mov	r0, r0	@ nop
.L14:
	.loc 1 316 0
	ldr	r3, [fp, #-8]
	.loc 1 318 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	foal_lights+56
	.word	.LC2
	.word	.LC3
.LFE1:
	.size	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light, .-nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light
	.section	.text.__foal_lights_maintenance_may_run,"ax",%progbits
	.align	2
	.type	__foal_lights_maintenance_may_run, %function
__foal_lights_maintenance_may_run:
.LFB2:
	.loc 1 333 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #4
.LCFI8:
	.loc 1 336 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 341 0
	ldr	r3, .L20
	ldr	r3, [r3, #44]
	cmp	r3, #0
	beq	.L19
	.loc 1 343 0
	mov	r3, #0
	str	r3, [fp, #-4]
.L19:
	.loc 1 346 0
	ldr	r3, [fp, #-4]
	.loc 1 348 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L21:
	.align	2
.L20:
	.word	foal_lights
.LFE2:
	.size	__foal_lights_maintenance_may_run, .-__foal_lights_maintenance_may_run
	.section	.text.FOAL_LIGHTS_set_stop_date_and_time_to_furthest,"ax",%progbits
	.align	2
	.type	FOAL_LIGHTS_set_stop_date_and_time_to_furthest, %function
FOAL_LIGHTS_set_stop_date_and_time_to_furthest:
.LFB3:
	.loc 1 357 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #20
.LCFI11:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 360 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 371 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #0]
	mov	r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L23
	.loc 1 375 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcs	.L24
	.loc 1 375 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-20]
	ldr	r3, .L25
	cmp	r2, r3
	bhi	.L24
	.loc 1 377 0 is_stmt 1
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-8]
	strh	r2, [r3, #0]	@ movhi
	.loc 1 378 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 380 0
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L24
.L23:
	.loc 1 383 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #0]
	mov	r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcs	.L24
	.loc 1 387 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L25
	cmp	r2, r3
	bhi	.L24
	.loc 1 389 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-8]
	strh	r2, [r3, #0]	@ movhi
	.loc 1 390 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 392 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L24:
	.loc 1 396 0
	ldr	r3, [fp, #-4]
	.loc 1 397 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L26:
	.align	2
.L25:
	.word	86399
.LFE3:
	.size	FOAL_LIGHTS_set_stop_date_and_time_to_furthest, .-FOAL_LIGHTS_set_stop_date_and_time_to_furthest
	.section .rodata
	.align	2
.LC4:
	.ascii	"F_L: No Stop Time\000"
	.section	.text.nm_FOAL_LIGHTS_get_soonest_stop_time,"ax",%progbits
	.align	2
	.type	nm_FOAL_LIGHTS_get_soonest_stop_time, %function
nm_FOAL_LIGHTS_get_soonest_stop_time:
.LFB4:
	.loc 1 421 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #44
.LCFI14:
	str	r0, [fp, #-36]
	str	r1, [fp, #-40]
	str	r2, [fp, #-44]
	str	r3, [fp, #-48]
	.loc 1 423 0
	mvn	r3, #-268435456
	str	r3, [fp, #-12]
	.loc 1 424 0
	mvn	r3, #-268435456
	str	r3, [fp, #-16]
	.loc 1 426 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 427 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 434 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r0, r3
	bl	LIGHTS_get_day_index
	str	r0, [fp, #-28]
	.loc 1 437 0
	ldr	r0, [fp, #-40]
	ldr	r1, [fp, #-28]
	bl	LIGHTS_get_stop_time_1
	str	r0, [fp, #-32]
	.loc 1 439 0
	ldr	r2, [fp, #-32]
	ldr	r3, .L41
	cmp	r2, r3
	bhi	.L28
	.loc 1 442 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bhi	.L28
	.loc 1 445 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 446 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	str	r3, [fp, #-12]
	.loc 1 447 0
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-16]
.L28:
	.loc 1 452 0
	ldr	r0, [fp, #-40]
	ldr	r1, [fp, #-28]
	bl	LIGHTS_get_stop_time_2
	str	r0, [fp, #-32]
	.loc 1 454 0
	ldr	r2, [fp, #-32]
	ldr	r3, .L41
	cmp	r2, r3
	bhi	.L29
	.loc 1 457 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bhi	.L29
	.loc 1 460 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 463 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bls	.L29
	.loc 1 465 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	str	r3, [fp, #-12]
	.loc 1 466 0
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-16]
.L29:
	.loc 1 471 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L30
	.loc 1 471 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L30
	.loc 1 476 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L31
.L34:
	.loc 1 480 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-8]
	add	r1, r2, r3
	mov	r2, r1, lsr #1
	ldr	r3, .L41+4
	umull	r0, r3, r2, r3
	mov	r2, r3, lsr #2
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	ldr	r0, [fp, #-40]
	mov	r1, r2
	bl	LIGHTS_get_stop_time_1
	str	r0, [fp, #-32]
	.loc 1 482 0
	ldr	r2, [fp, #-32]
	ldr	r3, .L41
	cmp	r2, r3
	bhi	.L32
	.loc 1 484 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 487 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 488 0
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-16]
.L32:
	.loc 1 491 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-8]
	add	r1, r2, r3
	mov	r2, r1, lsr #1
	ldr	r3, .L41+4
	umull	r0, r3, r2, r3
	mov	r2, r3, lsr #2
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	ldr	r0, [fp, #-40]
	mov	r1, r2
	bl	LIGHTS_get_stop_time_2
	str	r0, [fp, #-32]
	.loc 1 493 0
	ldr	r2, [fp, #-32]
	ldr	r3, .L41
	cmp	r2, r3
	bhi	.L33
	.loc 1 496 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bls	.L33
	.loc 1 498 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 500 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 501 0
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-16]
.L33:
	.loc 1 505 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L30
	.loc 1 505 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L30
	.loc 1 476 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L31:
	.loc 1 476 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #14
	bls	.L34
.L30:
	.loc 1 513 0 is_stmt 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L35
	.loc 1 513 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L36
.L35:
	.loc 1 516 0 is_stmt 1
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 517 0
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	b	.L37
.L36:
	.loc 1 522 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	ldr	r3, [fp, #-44]
	str	r2, [r3, #0]
	.loc 1 523 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-48]
	str	r2, [r3, #0]
	.loc 1 524 0
	ldr	r0, .L41+8
	bl	Alert_Message
.L37:
	.loc 1 527 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L38
	.loc 1 527 0 is_stmt 0 discriminator 2
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L39
.L38:
	.loc 1 527 0 discriminator 1
	mov	r3, #1
	b	.L40
.L39:
	mov	r3, #0
.L40:
	.loc 1 529 0 is_stmt 1 discriminator 3
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L42:
	.align	2
.L41:
	.word	86399
	.word	-1840700269
	.word	.LC4
.LFE4:
	.size	nm_FOAL_LIGHTS_get_soonest_stop_time, .-nm_FOAL_LIGHTS_get_soonest_stop_time
	.section .rodata
	.align	2
.LC5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_lights.c\000"
	.section	.text.FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down,"ax",%progbits
	.align	2
	.type	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down, %function
FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down:
.LFB5:
	.loc 1 565 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #16
.LCFI17:
	str	r0, [fp, #-20]
	.loc 1 582 0
	ldr	r3, .L51
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L51+4
	ldr	r3, .L51+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 584 0
	ldr	r3, .L51+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L51+4
	mov	r3, #584
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 589 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L44
	.loc 1 597 0
	ldr	r0, .L51+16
	mov	r1, #0
	bl	nm_ListInit
	.loc 1 600 0
	ldr	r0, .L51+20
	mov	r1, #12
	bl	nm_ListInit
	.loc 1 605 0
	ldr	r3, .L51+24
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 606 0
	ldr	r3, .L51+24
	mov	r2, #0
	str	r2, [r3, #52]
	.loc 1 609 0
	ldr	r0, .L51+28
	mov	r1, #0
	mov	r2, #1728
	bl	memset
	b	.L45
.L44:
	.loc 1 627 0
	ldr	r3, .L51+24
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 629 0
	ldr	r3, .L51+24
	mov	r2, #0
	str	r2, [r3, #52]
.L45:
	.loc 1 635 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L46
.L50:
	.loc 1 637 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L47
.L49:
	.loc 1 640 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	LIGHTS_get_lights_array_index
	mov	r2, r0
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L51+28
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 643 0
	ldr	r3, [fp, #-8]
	and	r2, r3, #255
	ldr	r3, [fp, #-16]
	strb	r2, [r3, #34]
	.loc 1 644 0
	ldr	r3, [fp, #-12]
	and	r2, r3, #255
	ldr	r3, [fp, #-16]
	strb	r2, [r3, #35]
	.loc 1 647 0
	ldr	r0, .L51+16
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list
	mov	r3, r0
	cmp	r3, #0
	beq	.L48
	.loc 1 650 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	strb	r2, [r3, #26]
.L48:
	.loc 1 637 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L47:
	.loc 1 637 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #3
	bls	.L49
	.loc 1 635 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L46:
	.loc 1 635 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L50
	.loc 1 657 0 is_stmt 1
	ldr	r3, .L51+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 659 0
	ldr	r3, .L51
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 661 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L52:
	.align	2
.L51:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC5
	.word	582
	.word	system_preserves_recursive_MUTEX
	.word	foal_lights+16
	.word	foal_lights+36
	.word	foal_lights
	.word	foal_lights+56
.LFE5:
	.size	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down, .-FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down
	.section	.text.FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights
	.type	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights, %function
FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights:
.LFB6:
	.loc 1 679 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	.loc 1 680 0
	mov	r0, #1
	bl	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down
	.loc 1 682 0
	bl	IRRI_LIGHTS_restart_all_lights
	.loc 1 684 0
	ldmfd	sp!, {fp, pc}
.LFE6:
	.size	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights, .-FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights
	.section .rodata
	.align	2
.LC6:
	.ascii	"Foal Lights\000"
	.section	.text.init_battery_backed_foal_lights,"ax",%progbits
	.align	2
	.global	init_battery_backed_foal_lights
	.type	init_battery_backed_foal_lights, %function
init_battery_backed_foal_lights:
.LFB7:
	.loc 1 700 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI20:
	add	fp, sp, #4
.LCFI21:
	sub	sp, sp, #4
.LCFI22:
	str	r0, [fp, #-8]
	.loc 1 701 0
	ldr	r3, .L58
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L58+4
	ldr	r3, .L58+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 707 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L55
	.loc 1 707 0 is_stmt 0 discriminator 1
	ldr	r0, .L58+12
	ldr	r1, .L58+16
	mov	r2, #16
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	beq	.L56
.L55:
	.loc 1 709 0 is_stmt 1
	ldr	r0, .L58+20
	ldr	r1, [fp, #-8]
	bl	Alert_battery_backed_var_initialized
	.loc 1 714 0
	ldr	r0, .L58+12
	mov	r1, #0
	ldr	r2, .L58+24
	bl	memset
	.loc 1 718 0
	mov	r0, #1
	bl	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down
	.loc 1 722 0
	ldr	r0, .L58+12
	ldr	r1, .L58+16
	mov	r2, #16
	bl	strlcpy
	b	.L57
.L56:
	.loc 1 733 0
	mov	r0, #0
	bl	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down
.L57:
	.loc 1 738 0
	ldr	r3, .L58
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 740 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L59:
	.align	2
.L58:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC5
	.word	701
	.word	foal_lights
	.word	FOAL_LIGHTS_PRE_TEST_STRING
	.word	.LC6
	.word	1784
.LFE7:
	.size	init_battery_backed_foal_lights, .-init_battery_backed_foal_lights
	.section .rodata
	.align	2
.LC7:
	.ascii	"F_L: Non-off sent to add to A N list\000"
	.section	.text.nm_FOAL_LIGHTS_add_to_action_needed_list,"ax",%progbits
	.align	2
	.type	nm_FOAL_LIGHTS_add_to_action_needed_list, %function
nm_FOAL_LIGHTS_add_to_action_needed_list:
.LFB8:
	.loc 1 766 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI23:
	add	fp, sp, #4
.LCFI24:
	sub	sp, sp, #4
.LCFI25:
	str	r0, [fp, #-8]
	.loc 1 772 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #24]	@ zero_extendqisi2
	cmp	r3, #1
	beq	.L61
	.loc 1 773 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #24]	@ zero_extendqisi2
	.loc 1 772 0 discriminator 1
	cmp	r3, #2
	beq	.L61
	.loc 1 774 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #24]	@ zero_extendqisi2
	.loc 1 773 0
	cmp	r3, #3
	beq	.L61
	.loc 1 775 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #24]	@ zero_extendqisi2
	.loc 1 774 0
	cmp	r3, #4
	beq	.L61
	.loc 1 776 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #24]	@ zero_extendqisi2
	.loc 1 775 0
	cmp	r3, #0
	beq	.L61
	.loc 1 779 0
	ldr	r0, .L64
	ldr	r1, [fp, #-8]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	bne	.L60
	.loc 1 782 0
	ldr	r0, .L64
	ldr	r1, [fp, #-8]
	bl	nm_ListInsertTail
	.loc 1 779 0
	b	.L60
.L61:
	.loc 1 797 0
	ldr	r0, .L64+4
	bl	Alert_Message
.L60:
	.loc 1 800 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L65:
	.align	2
.L64:
	.word	foal_lights+36
	.word	.LC7
.LFE8:
	.size	nm_FOAL_LIGHTS_add_to_action_needed_list, .-nm_FOAL_LIGHTS_add_to_action_needed_list
	.section .rodata
	.align	2
.LC8:
	.ascii	"F_L: Error removing from ON list : %s, %u\000"
	.section	.text.nm_remove_from_the_lights_ON_list,"ax",%progbits
	.align	2
	.type	nm_remove_from_the_lights_ON_list, %function
nm_remove_from_the_lights_ON_list:
.LFB9:
	.loc 1 804 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	sub	sp, sp, #8
.LCFI28:
	str	r0, [fp, #-12]
	.loc 1 809 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 814 0
	ldr	r0, .L69
	ldr	r1, [fp, #-12]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #1
	bne	.L67
	.loc 1 817 0
	ldr	r0, .L69
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
	.loc 1 819 0
	ldr	r0, .L69
	ldr	r1, [fp, #-12]
	ldr	r2, .L69+4
	ldr	r3, .L69+8
	bl	nm_ListRemove_debug
	mov	r3, r0
	cmp	r3, #0
	beq	.L68
	.loc 1 821 0
	ldr	r0, .L69+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L69+12
	mov	r1, r3
	ldr	r2, .L69+16
	bl	Alert_Message_va
.L68:
	.loc 1 825 0
	ldr	r0, [fp, #-12]
	bl	nm_FOAL_LIGHTS_add_to_action_needed_list
.L67:
	.loc 1 831 0
	ldr	r3, [fp, #-8]
	.loc 1 832 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L70:
	.align	2
.L69:
	.word	foal_lights+16
	.word	.LC5
	.word	819
	.word	.LC8
	.word	821
.LFE9:
	.size	nm_remove_from_the_lights_ON_list, .-nm_remove_from_the_lights_ON_list
	.section	.text.nm_turn_OFF_this_light,"ax",%progbits
	.align	2
	.type	nm_turn_OFF_this_light, %function
nm_turn_OFF_this_light:
.LFB10:
	.loc 1 836 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI29:
	add	fp, sp, #4
.LCFI30:
	sub	sp, sp, #8
.LCFI31:
	str	r0, [fp, #-12]
	.loc 1 842 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	strb	r2, [r3, #27]
	.loc 1 844 0
	ldr	r0, [fp, #-12]
	bl	nm_remove_from_the_lights_ON_list
	str	r0, [fp, #-8]
	.loc 1 850 0
	ldr	r0, [fp, #-12]
	bl	nm_FOAL_LIGHTS_add_to_action_needed_list
	.loc 1 856 0
	ldr	r0, .L72
	ldr	r1, [fp, #-12]
	ldr	r2, .L72+4
	mov	r3, #856
	bl	nm_ListRemove_debug
	.loc 1 860 0
	ldr	r3, [fp, #-8]
	.loc 1 861 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L73:
	.align	2
.L72:
	.word	foal_lights+16
	.word	.LC5
.LFE10:
	.size	nm_turn_OFF_this_light, .-nm_turn_OFF_this_light
	.section	.text._nm_nm_nm_add_to_the_lights_ON_list,"ax",%progbits
	.align	2
	.type	_nm_nm_nm_add_to_the_lights_ON_list, %function
_nm_nm_nm_add_to_the_lights_ON_list:
.LFB11:
	.loc 1 904 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI32:
	add	fp, sp, #4
.LCFI33:
	sub	sp, sp, #16
.LCFI34:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 905 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 915 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L77+4
	ldr	r3, .L77+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 921 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #34]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #35]	@ zero_extendqisi2
	ldr	r0, .L77+12
	mov	r1, r2
	mov	r2, r3
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list
	str	r0, [fp, #-12]
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L75
	.loc 1 924 0
	ldr	r0, .L77+12
	ldr	r1, [fp, #-16]
	bl	nm_ListInsertTail
	mov	r3, r0
	cmp	r3, #0
	bne	.L76
	.loc 1 927 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 930 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	strb	r2, [r3, #27]
	.loc 1 934 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	strb	r2, [r3, #26]
	b	.L76
.L75:
	.loc 1 941 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 949 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	strb	r2, [r3, #26]
.L76:
	.loc 1 955 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 957 0
	ldr	r3, [fp, #-8]
	.loc 1 959 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L78:
	.align	2
.L77:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC5
	.word	915
	.word	foal_lights+16
.LFE11:
	.size	_nm_nm_nm_add_to_the_lights_ON_list, .-_nm_nm_nm_add_to_the_lights_ON_list
	.section .rodata
	.align	2
.LC9:
	.ascii	"pllc_ptr\000"
	.align	2
.LC10:
	.ascii	"foal_lights.header_for_foal_lights_ON_list\000"
	.section	.text._nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_ON,"ax",%progbits
	.align	2
	.type	_nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_ON, %function
_nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_ON:
.LFB12:
	.loc 1 981 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI35:
	add	fp, sp, #4
.LCFI36:
	sub	sp, sp, #8
.LCFI37:
	str	r0, [fp, #-12]
	.loc 1 982 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #34]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #35]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	str	r0, [fp, #-8]
	.loc 1 984 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #24]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L79
	.loc 1 989 0
	ldr	r1, .L82
	mov	r3, #976
	ldr	r2, [fp, #-8]
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	bic	r3, r3, #4
	strb	r3, [r2, #0]
	.loc 1 994 0
	ldr	r1, .L82
	mov	r3, #976
	ldr	r2, [fp, #-8]
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	bic	r3, r3, #8
	strb	r3, [r2, #0]
	.loc 1 999 0
	ldr	r0, .L82
	ldr	r2, [fp, #-8]
	mov	r1, #24
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrh	r3, [r3, #2]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r1, r3, lsr #16
	ldr	ip, .L82
	ldr	r2, [fp, #-8]
	mov	r0, #24
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	strh	r1, [r3, #2]	@ movhi
	.loc 1 1001 0
	ldr	r0, .L82+4
	ldr	r1, [fp, #-12]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	bne	.L81
	.loc 1 1003 0
	ldr	r0, .L82+8
	ldr	r1, .L82+12
	ldr	r2, .L82+16
	ldr	r3, .L82+20
	bl	Alert_item_not_on_list_with_filename
	b	.L79
.L81:
	.loc 1 1010 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	strb	r2, [r3, #27]
.L79:
	.loc 1 1016 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L83:
	.align	2
.L82:
	.word	lights_preserves
	.word	foal_lights+16
	.word	.LC9
	.word	.LC10
	.word	.LC5
	.word	1003
.LFE12:
	.size	_nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_ON, .-_nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_ON
	.section .rodata
	.align	2
.LC11:
	.ascii	"F_L: lls not found : %s, %u\000"
	.align	2
.LC12:
	.ascii	"F_L: llc not available : %s, %u\000"
	.align	2
.LC13:
	.ascii	"F_L: Bad index in mobile on\000"
	.section	.text.FOAL_LIGHTS_initiate_a_mobile_light_ON,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_initiate_a_mobile_light_ON
	.type	FOAL_LIGHTS_initiate_a_mobile_light_ON, %function
FOAL_LIGHTS_initiate_a_mobile_light_ON:
.LFB13:
	.loc 1 1039 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI38:
	add	fp, sp, #4
.LCFI39:
	sub	sp, sp, #20
.LCFI40:
	str	r0, [fp, #-24]
	.loc 1 1040 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	mov	r3, r3, lsr #2
	str	r3, [fp, #-8]
	.loc 1 1041 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	and	r3, r3, #3
	str	r3, [fp, #-12]
	.loc 1 1047 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	cmp	r3, #47
	bhi	.L85
	.loc 1 1049 0
	ldr	r3, .L92
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L92+4
	ldr	r3, .L92+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1051 0
	ldr	r3, .L92+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L92+4
	ldr	r3, .L92+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1053 0
	ldr	r3, .L92+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L92+4
	ldr	r3, .L92+24
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1061 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	str	r0, [fp, #-16]
	.loc 1 1063 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L86
	.loc 1 1065 0
	ldr	r0, .L92+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L92+28
	mov	r1, r3
	ldr	r2, .L92+32
	bl	Alert_Message_va
	b	.L87
.L86:
.LBB2:
	.loc 1 1074 0
	ldr	r0, .L92+36
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list
	str	r0, [fp, #-20]
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L88
	.loc 1 1076 0
	ldr	r3, [fp, #-20]
	mov	r2, #7
	strb	r2, [r3, #25]
	.loc 1 1077 0
	ldr	r3, [fp, #-20]
	mov	r2, #3
	strb	r2, [r3, #24]
	.loc 1 1081 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-20]
	strh	r2, [r3, #32]	@ movhi
	.loc 1 1082 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-20]
	str	r2, [r3, #28]
	.loc 1 1086 0
	ldr	r3, [fp, #-20]
	mov	r2, #1
	strb	r2, [r3, #26]
	b	.L87
.L88:
	.loc 1 1091 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, #0
	bl	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light
	str	r0, [fp, #-20]
	.loc 1 1093 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L89
	.loc 1 1095 0
	ldr	r0, .L92+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L92+40
	mov	r1, r3
	ldr	r2, .L92+44
	bl	Alert_Message_va
	b	.L87
.L89:
	.loc 1 1099 0
	ldr	r3, [fp, #-20]
	mov	r2, #7
	strb	r2, [r3, #25]
	.loc 1 1100 0
	ldr	r3, [fp, #-20]
	mov	r2, #3
	strb	r2, [r3, #24]
	.loc 1 1103 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-20]
	strh	r2, [r3, #32]	@ movhi
	.loc 1 1104 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-20]
	str	r2, [r3, #28]
	.loc 1 1107 0
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-16]
	bl	_nm_nm_nm_add_to_the_lights_ON_list
	mov	r3, r0
	cmp	r3, #0
	beq	.L90
	.loc 1 1109 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, #4
	bl	Alert_light_ID_with_text
.L90:
	.loc 1 1114 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	strb	r2, [r3, #27]
	.loc 1 1118 0
	ldr	r3, [fp, #-20]
	mov	r2, #1
	strb	r2, [r3, #26]
.L87:
.LBE2:
	.loc 1 1123 0
	ldr	r3, .L92+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1125 0
	ldr	r3, .L92+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1127 0
	ldr	r3, .L92
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L84
.L85:
	.loc 1 1131 0
	ldr	r0, .L92+48
	bl	Alert_Message
.L84:
	.loc 1 1134 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L93:
	.align	2
.L92:
	.word	list_lights_recursive_MUTEX
	.word	.LC5
	.word	1049
	.word	lights_preserves_recursive_MUTEX
	.word	1051
	.word	list_foal_lights_recursive_MUTEX
	.word	1053
	.word	.LC11
	.word	1065
	.word	foal_lights+16
	.word	.LC12
	.word	1095
	.word	.LC13
.LFE13:
	.size	FOAL_LIGHTS_initiate_a_mobile_light_ON, .-FOAL_LIGHTS_initiate_a_mobile_light_ON
	.section .rodata
	.align	2
.LC14:
	.ascii	"F_L: Bad index in mobile off\000"
	.section	.text.FOAL_LIGHTS_initiate_a_mobile_light_OFF,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_initiate_a_mobile_light_OFF
	.type	FOAL_LIGHTS_initiate_a_mobile_light_OFF, %function
FOAL_LIGHTS_initiate_a_mobile_light_OFF:
.LFB14:
	.loc 1 1157 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI41:
	add	fp, sp, #4
.LCFI42:
	sub	sp, sp, #16
.LCFI43:
	str	r0, [fp, #-20]
	.loc 1 1158 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	mov	r3, r3, lsr #2
	str	r3, [fp, #-8]
	.loc 1 1159 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	and	r3, r3, #3
	str	r3, [fp, #-12]
	.loc 1 1165 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	cmp	r3, #47
	bhi	.L95
	.loc 1 1167 0
	ldr	r3, .L98
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L98+4
	ldr	r3, .L98+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1169 0
	ldr	r3, .L98+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L98+4
	ldr	r3, .L98+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1171 0
	ldr	r3, .L98+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L98+4
	ldr	r3, .L98+24
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1176 0
	ldr	r0, .L98+28
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list
	str	r0, [fp, #-16]
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L96
	.loc 1 1179 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	strb	r2, [r3, #25]
	.loc 1 1181 0
	ldr	r3, [fp, #-16]
	mov	r2, #8
	strb	r2, [r3, #24]
	.loc 1 1187 0
	ldr	r0, [fp, #-16]
	bl	nm_turn_OFF_this_light
	str	r0, [fp, #-16]
	.loc 1 1189 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, #5
	bl	Alert_light_ID_with_text
.L96:
	.loc 1 1197 0
	ldr	r3, .L98+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1199 0
	ldr	r3, .L98+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1201 0
	ldr	r3, .L98
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L94
.L95:
	.loc 1 1205 0
	ldr	r0, .L98+32
	bl	Alert_Message
.L94:
	.loc 1 1208 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L99:
	.align	2
.L98:
	.word	list_lights_recursive_MUTEX
	.word	.LC5
	.word	1167
	.word	lights_preserves_recursive_MUTEX
	.word	1169
	.word	list_foal_lights_recursive_MUTEX
	.word	1171
	.word	foal_lights+16
	.word	.LC14
.LFE14:
	.size	FOAL_LIGHTS_initiate_a_mobile_light_OFF, .-FOAL_LIGHTS_initiate_a_mobile_light_OFF
	.section .rodata
	.align	2
.LC15:
	.ascii	"FOAL light list must have changed : %s, %u\000"
	.align	2
.LC16:
	.ascii	"F_L: ON list token error : %s, %u\000"
	.section	.text.FOAL_LIGHTS_load_lights_on_list_into_outgoing_token,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_load_lights_on_list_into_outgoing_token
	.type	FOAL_LIGHTS_load_lights_on_list_into_outgoing_token, %function
FOAL_LIGHTS_load_lights_on_list_into_outgoing_token:
.LFB15:
	.loc 1 1240 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI44:
	add	fp, sp, #4
.LCFI45:
	sub	sp, sp, #32
.LCFI46:
	str	r0, [fp, #-36]
	.loc 1 1247 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1249 0
	ldr	r3, [fp, #-36]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1253 0
	ldr	r3, .L111
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L111+4
	ldr	r3, .L111+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1256 0
	mov	r3, #0
	strh	r3, [fp, #-22]	@ movhi
	.loc 1 1261 0
	ldr	r0, .L111+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 1263 0
	b	.L101
.L103:
	.loc 1 1266 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #26]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L102
	.loc 1 1268 0
	ldrh	r3, [fp, #-22]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-22]	@ movhi
.L102:
	.loc 1 1279 0
	ldr	r0, .L111+12
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L101:
	.loc 1 1263 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L103
	.loc 1 1282 0
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	beq	.L104
.LBB3:
	.loc 1 1291 0
	ldrh	r3, [fp, #-22]
	mov	r3, r3, asl #3
	add	r3, r3, #2
	str	r3, [fp, #-16]
	.loc 1 1293 0
	ldr	r0, [fp, #-16]
	ldr	r1, .L111+4
	ldr	r2, .L111+16
	bl	mem_malloc_debug
	str	r0, [fp, #-20]
	.loc 1 1295 0
	ldr	r3, [fp, #-36]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 1297 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-8]
	.loc 1 1299 0
	sub	r3, fp, #22
	ldr	r0, [fp, #-20]
	mov	r1, r3
	mov	r2, #2
	bl	memcpy
	.loc 1 1301 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #2
	str	r3, [fp, #-20]
	.loc 1 1303 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #2
	str	r3, [fp, #-16]
	.loc 1 1305 0
	ldr	r0, .L111+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 1307 0
	b	.L105
.L109:
	.loc 1 1309 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #26]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L106
.LBB4:
	.loc 1 1311 0
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	bne	.L107
	.loc 1 1314 0
	ldr	r0, .L111+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L111+20
	mov	r1, r3
	ldr	r2, .L111+24
	bl	Alert_Message_va
	.loc 1 1316 0
	b	.L108
.L107:
	.loc 1 1319 0
	ldrh	r3, [fp, #-22]
	sub	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-22]	@ movhi
	.loc 1 1323 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #32]
	strh	r3, [fp, #-28]	@ movhi
	.loc 1 1324 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #28]
	str	r3, [fp, #-32]
	.loc 1 1325 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #34]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #35]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	and	r3, r3, #255
	strb	r3, [fp, #-26]
	.loc 1 1326 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #24]	@ zero_extendqisi2
	strb	r3, [fp, #-25]
	.loc 1 1328 0
	sub	r3, fp, #32
	ldr	r0, [fp, #-20]
	mov	r1, r3
	mov	r2, #8
	bl	memcpy
	.loc 1 1330 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #8
	str	r3, [fp, #-20]
	.loc 1 1333 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	strb	r2, [r3, #26]
	.loc 1 1336 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #8
	str	r3, [fp, #-16]
.L106:
.LBE4:
	.loc 1 1339 0
	ldr	r0, .L111+12
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L105:
	.loc 1 1307 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L109
.L108:
	.loc 1 1343 0
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	bne	.L110
	.loc 1 1343 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L104
.L110:
	.loc 1 1345 0 is_stmt 1
	ldr	r0, .L111+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L111+28
	mov	r1, r3
	ldr	r2, .L111+32
	bl	Alert_Message_va
.L104:
.LBE3:
	.loc 1 1350 0
	ldr	r3, .L111
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1352 0
	ldr	r3, [fp, #-8]
	.loc 1 1354 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L112:
	.align	2
.L111:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC5
	.word	1253
	.word	foal_lights+16
	.word	1293
	.word	.LC15
	.word	1314
	.word	.LC16
	.word	1345
.LFE15:
	.size	FOAL_LIGHTS_load_lights_on_list_into_outgoing_token, .-FOAL_LIGHTS_load_lights_on_list_into_outgoing_token
	.section .rodata
	.align	2
.LC17:
	.ascii	"F_L: A_N token error : %s, %u\000"
	.section	.text.FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token
	.type	FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token, %function
FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token:
.LFB16:
	.loc 1 1358 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI47:
	add	fp, sp, #4
.LCFI48:
	sub	sp, sp, #32
.LCFI49:
	str	r0, [fp, #-36]
	.loc 1 1362 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1364 0
	ldr	r3, [fp, #-36]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1367 0
	ldr	r3, .L118
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L118+4
	ldr	r3, .L118+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1375 0
	ldr	r3, .L118+12
	ldr	r3, [r3, #44]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-22]	@ movhi
	.loc 1 1377 0
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	beq	.L114
.LBB5:
	.loc 1 1386 0
	ldrh	r3, [fp, #-22]
	mov	r3, r3, asl #3
	add	r3, r3, #2
	str	r3, [fp, #-12]
	.loc 1 1388 0
	ldr	r0, [fp, #-12]
	ldr	r1, .L118+4
	ldr	r2, .L118+16
	bl	mem_malloc_debug
	str	r0, [fp, #-16]
	.loc 1 1390 0
	ldr	r3, [fp, #-36]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 1392 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 1395 0
	sub	r3, fp, #22
	ldr	r0, [fp, #-16]
	mov	r1, r3
	mov	r2, #2
	bl	memcpy
	.loc 1 1396 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #2
	str	r3, [fp, #-16]
	.loc 1 1397 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #2
	str	r3, [fp, #-12]
	.loc 1 1402 0
	ldr	r0, .L118+20
	ldr	r1, .L118+4
	ldr	r2, .L118+24
	bl	nm_ListRemoveHead_debug
	str	r0, [fp, #-20]
	.loc 1 1404 0
	b	.L115
.L116:
.LBB6:
	.loc 1 1409 0
	sub	r3, fp, #32
	mov	r0, r3
	mov	r1, #0
	mov	r2, #8
	bl	memset
	.loc 1 1411 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #34]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #35]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	bl	LIGHTS_get_lights_array_index
	mov	r3, r0
	str	r3, [fp, #-32]
	.loc 1 1412 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #24]	@ zero_extendqisi2
	str	r3, [fp, #-28]
	.loc 1 1417 0
	sub	r3, fp, #32
	ldr	r0, [fp, #-16]
	mov	r1, r3
	mov	r2, #8
	bl	memcpy
	.loc 1 1418 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #8
	str	r3, [fp, #-16]
	.loc 1 1419 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #8
	str	r3, [fp, #-12]
	.loc 1 1420 0
	ldrh	r3, [fp, #-22]
	sub	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-22]	@ movhi
	.loc 1 1425 0
	ldr	r0, .L118+20
	ldr	r1, .L118+4
	ldr	r2, .L118+28
	bl	nm_ListRemoveHead_debug
	str	r0, [fp, #-20]
.L115:
.LBE6:
	.loc 1 1404 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L116
	.loc 1 1430 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L117
	.loc 1 1430 0 is_stmt 0 discriminator 1
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	beq	.L114
.L117:
	.loc 1 1432 0 is_stmt 1
	ldr	r0, .L118+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L118+32
	mov	r1, r3
	ldr	r2, .L118+36
	bl	Alert_Message_va
.L114:
.LBE5:
	.loc 1 1437 0
	ldr	r3, .L118
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1439 0
	ldr	r3, [fp, #-8]
	.loc 1 1441 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L119:
	.align	2
.L118:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC5
	.word	1367
	.word	foal_lights
	.word	1388
	.word	foal_lights+36
	.word	1402
	.word	1425
	.word	.LC17
	.word	1432
.LFE16:
	.size	FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token, .-FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token
	.section .rodata
	.align	2
.LC18:
	.ascii	"F_L: light not found : %s, %u\000"
	.section	.text.FOAL_LIGHTS_process_light_on_from_token_response,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_process_light_on_from_token_response
	.type	FOAL_LIGHTS_process_light_on_from_token_response, %function
FOAL_LIGHTS_process_light_on_from_token_response:
.LFB17:
	.loc 1 1449 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI50:
	add	fp, sp, #4
.LCFI51:
	sub	sp, sp, #20
.LCFI52:
	str	r0, [fp, #-24]
	.loc 1 1450 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	mov	r3, r3, lsr #2
	str	r3, [fp, #-8]
	.loc 1 1451 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	and	r3, r3, #3
	str	r3, [fp, #-12]
	.loc 1 1459 0
	ldr	r3, .L126
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L126+4
	ldr	r3, .L126+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1461 0
	ldr	r3, .L126+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L126+4
	ldr	r3, .L126+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1463 0
	ldr	r3, .L126+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L126+4
	ldr	r3, .L126+24
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1467 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	str	r0, [fp, #-16]
	.loc 1 1469 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L121
	.loc 1 1471 0
	ldr	r0, .L126+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L126+28
	mov	r1, r3
	ldr	r2, .L126+32
	bl	Alert_Message_va
	b	.L122
.L121:
.LBB7:
	.loc 1 1480 0
	ldr	r0, .L126+36
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list
	str	r0, [fp, #-20]
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L123
	.loc 1 1484 0
	ldr	r3, [fp, #-20]
	mov	r2, #4
	strb	r2, [r3, #25]
	.loc 1 1485 0
	ldr	r3, [fp, #-20]
	mov	r2, #2
	strb	r2, [r3, #24]
	.loc 1 1489 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-20]
	strh	r2, [r3, #32]	@ movhi
	.loc 1 1490 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-20]
	str	r2, [r3, #28]
	.loc 1 1493 0
	ldr	r3, [fp, #-20]
	mov	r2, #1
	strb	r2, [r3, #26]
	b	.L122
.L123:
	.loc 1 1498 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, #0
	bl	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light
	str	r0, [fp, #-20]
	.loc 1 1500 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L124
	.loc 1 1502 0
	ldr	r0, .L126+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L126+40
	mov	r1, r3
	ldr	r2, .L126+44
	bl	Alert_Message_va
	b	.L122
.L124:
	.loc 1 1508 0
	ldr	r3, [fp, #-20]
	mov	r2, #4
	strb	r2, [r3, #25]
	.loc 1 1509 0
	ldr	r3, [fp, #-20]
	mov	r2, #2
	strb	r2, [r3, #24]
	.loc 1 1512 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-20]
	strh	r2, [r3, #32]	@ movhi
	.loc 1 1513 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-20]
	str	r2, [r3, #28]
	.loc 1 1516 0
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-16]
	bl	_nm_nm_nm_add_to_the_lights_ON_list
	mov	r3, r0
	cmp	r3, #0
	beq	.L125
	.loc 1 1518 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, #2
	bl	Alert_light_ID_with_text
.L125:
	.loc 1 1522 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	strb	r2, [r3, #27]
	.loc 1 1526 0
	ldr	r3, [fp, #-20]
	mov	r2, #1
	strb	r2, [r3, #26]
.L122:
.LBE7:
	.loc 1 1531 0
	ldr	r3, .L126+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1533 0
	ldr	r3, .L126+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1535 0
	ldr	r3, .L126
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1537 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L127:
	.align	2
.L126:
	.word	list_lights_recursive_MUTEX
	.word	.LC5
	.word	1459
	.word	lights_preserves_recursive_MUTEX
	.word	1461
	.word	list_foal_lights_recursive_MUTEX
	.word	1463
	.word	.LC18
	.word	1471
	.word	foal_lights+16
	.word	.LC12
	.word	1502
.LFE17:
	.size	FOAL_LIGHTS_process_light_on_from_token_response, .-FOAL_LIGHTS_process_light_on_from_token_response
	.section	.text.FOAL_LIGHTS_turn_off_this_lights_output,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_turn_off_this_lights_output
	.type	FOAL_LIGHTS_turn_off_this_lights_output, %function
FOAL_LIGHTS_turn_off_this_lights_output:
.LFB18:
	.loc 1 1541 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI53:
	add	fp, sp, #4
.LCFI54:
	sub	sp, sp, #20
.LCFI55:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 1554 0
	ldr	r3, .L130
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L130+4
	ldr	r3, .L130+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1556 0
	ldr	r3, .L130+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L130+4
	ldr	r3, .L130+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1561 0
	ldr	r0, .L130+20
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list
	str	r0, [fp, #-8]
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L129
	.loc 1 1564 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	strb	r2, [r3, #25]
	.loc 1 1566 0
	ldr	r3, [fp, #-20]
	and	r2, r3, #255
	ldr	r3, [fp, #-8]
	strb	r2, [r3, #24]
	.loc 1 1572 0
	ldr	r0, [fp, #-8]
	bl	nm_turn_OFF_this_light
	str	r0, [fp, #-8]
	.loc 1 1574 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-24]
	bl	Alert_light_ID_with_text
.L129:
	.loc 1 1582 0
	ldr	r3, .L130+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1584 0
	ldr	r3, .L130
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1585 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L131:
	.align	2
.L130:
	.word	lights_preserves_recursive_MUTEX
	.word	.LC5
	.word	1554
	.word	list_foal_lights_recursive_MUTEX
	.word	1556
	.word	foal_lights+16
.LFE18:
	.size	FOAL_LIGHTS_turn_off_this_lights_output, .-FOAL_LIGHTS_turn_off_this_lights_output
	.section	.text.FOAL_LIGHTS_turn_off_all_lights_at_this_box,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_turn_off_all_lights_at_this_box
	.type	FOAL_LIGHTS_turn_off_all_lights_at_this_box, %function
FOAL_LIGHTS_turn_off_all_lights_at_this_box:
.LFB19:
	.loc 1 1589 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI56:
	add	fp, sp, #4
.LCFI57:
	sub	sp, sp, #16
.LCFI58:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	.loc 1 1592 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L133
.L134:
	.loc 1 1594 0 discriminator 2
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	bl	FOAL_LIGHTS_turn_off_this_lights_output
	.loc 1 1592 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L133:
	.loc 1 1592 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bls	.L134
	.loc 1 1596 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE19:
	.size	FOAL_LIGHTS_turn_off_all_lights_at_this_box, .-FOAL_LIGHTS_turn_off_all_lights_at_this_box
	.section	.text.FOAL_LIGHTS_process_light_off_from_token_response,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_process_light_off_from_token_response
	.type	FOAL_LIGHTS_process_light_off_from_token_response, %function
FOAL_LIGHTS_process_light_off_from_token_response:
.LFB20:
	.loc 1 1600 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI59:
	add	fp, sp, #4
.LCFI60:
	sub	sp, sp, #12
.LCFI61:
	str	r0, [fp, #-16]
	.loc 1 1607 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	mov	r3, r3, lsr #2
	str	r3, [fp, #-8]
	.loc 1 1609 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	and	r3, r3, #3
	str	r3, [fp, #-12]
	.loc 1 1611 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, #7
	mov	r3, #3
	bl	FOAL_LIGHTS_turn_off_this_lights_output
	.loc 1 1612 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE20:
	.size	FOAL_LIGHTS_process_light_off_from_token_response, .-FOAL_LIGHTS_process_light_off_from_token_response
	.section .rodata
	.align	2
.LC19:
	.ascii	"F_L: ON add failed\000"
	.align	2
.LC20:
	.ascii	"F_L: Too many lights\000"
	.align	2
.LC21:
	.ascii	"F_L: no stop time error\000"
	.align	2
.LC22:
	.ascii	"F_L: unexpected date/time\000"
	.align	2
.LC23:
	.ascii	"F_L: no ON list pointer\000"
	.section	.text.TDCHECK_at_1Hz_rate_check_for_lights_schedule_start,"ax",%progbits
	.align	2
	.global	TDCHECK_at_1Hz_rate_check_for_lights_schedule_start
	.type	TDCHECK_at_1Hz_rate_check_for_lights_schedule_start, %function
TDCHECK_at_1Hz_rate_check_for_lights_schedule_start:
.LFB21:
	.loc 1 1638 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI62:
	add	fp, sp, #8
.LCFI63:
	sub	sp, sp, #48
.LCFI64:
	str	r0, [fp, #-56]
	.loc 1 1639 0
	mvn	r3, #-268435456
	str	r3, [fp, #-40]
	.loc 1 1641 0
	mvn	r3, #-268435456
	str	r3, [fp, #-44]
	.loc 1 1660 0
	ldr	r3, .L151
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L151+4
	ldr	r3, .L151+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1663 0
	ldr	r3, .L151+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L151+4
	ldr	r3, .L151+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1665 0
	ldr	r3, .L151+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L151+4
	ldr	r3, .L151+24
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1673 0
	ldr	r3, .L151+28
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 1677 0
	ldr	r0, .L151+32
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 1679 0
	b	.L137
.L149:
	.loc 1 1681 0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_get_box_index
	str	r0, [fp, #-24]
	.loc 1 1683 0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_get_output_index
	str	r0, [fp, #-28]
	.loc 1 1685 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	bl	LIGHTS_get_lights_array_index
	str	r0, [fp, #-32]
	.loc 1 1696 0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #4]
	mov	r0, r3
	bl	LIGHTS_get_day_index
	str	r0, [fp, #-36]
	.loc 1 1705 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-36]
	bl	LIGHTS_get_start_time_1
	mov	r2, r0
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L138
	.loc 1 1706 0 discriminator 1
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-36]
	bl	LIGHTS_get_start_time_2
	mov	r2, r0
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #0]
	.loc 1 1705 0 discriminator 1
	cmp	r2, r3
	bne	.L139
.L138:
	.loc 1 1707 0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_get_physically_available
	mov	r3, r0
	.loc 1 1706 0
	cmp	r3, #0
	beq	.L139
	.loc 1 1708 0
	ldr	r3, [fp, #-56]
	ldrb	r3, [r3, #19]	@ zero_extendqisi2
	.loc 1 1707 0
	cmp	r3, #0
	bne	.L139
	.loc 1 1730 0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_has_a_stop_time
	mov	r3, r0
	cmp	r3, #0
	beq	.L140
	.loc 1 1733 0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_get_box_index
	mov	r4, r0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_get_output_index
	mov	r3, r0
	ldr	r0, .L151+36
	mov	r1, r4
	mov	r2, r3
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list
	str	r0, [fp, #-16]
	.loc 1 1736 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L141
	.loc 1 1739 0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_get_box_index
	mov	r4, r0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_get_output_index
	mov	r3, r0
	mov	r0, r4
	mov	r1, r3
	mov	r2, #0
	bl	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light
	str	r0, [fp, #-16]
	.loc 1 1741 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L142
	.loc 1 1743 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	strb	r2, [r3, #25]
	.loc 1 1744 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	strb	r2, [r3, #24]
	.loc 1 1748 0
	ldr	r3, [fp, #-56]
	ldrh	r2, [r3, #4]
	ldr	r3, [fp, #-16]
	strh	r2, [r3, #32]	@ movhi
	.loc 1 1749 0
	ldr	r3, [fp, #-56]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #28]
	.loc 1 1755 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-12]
	bl	_nm_nm_nm_add_to_the_lights_ON_list
	mov	r3, r0
	cmp	r3, #0
	bne	.L141
	.loc 1 1764 0
	ldr	r0, .L151+40
	bl	Alert_Message
	b	.L141
.L142:
	.loc 1 1770 0
	ldr	r0, .L151+44
	bl	Alert_Message
.L141:
	.loc 1 1774 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L143
	.loc 1 1787 0
	sub	r2, fp, #40
	sub	r3, fp, #44
	ldr	r0, [fp, #-56]
	ldr	r1, [fp, #-12]
	bl	nm_FOAL_LIGHTS_get_soonest_stop_time
	mov	r3, r0
	cmp	r3, #0
	beq	.L144
	.loc 1 1791 0
	ldr	r3, [fp, #-16]
	add	r0, r3, #32
	ldr	r3, [fp, #-16]
	add	r1, r3, #28
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-44]
	bl	FOAL_LIGHTS_set_stop_date_and_time_to_furthest
	mov	r3, r0
	cmp	r3, #0
	beq	.L145
	.loc 1 1795 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	strb	r2, [r3, #25]
	b	.L145
.L144:
	.loc 1 1801 0
	ldr	r0, .L151+48
	bl	Alert_Message
.L145:
	.loc 1 1806 0
	ldr	r3, [fp, #-16]
	ldrh	r3, [r3, #32]
	mov	r2, r3
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #4]
	add	r3, r3, #14
	cmp	r2, r3
	bgt	.L146
	.loc 1 1807 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #28]
	.loc 1 1806 0 discriminator 1
	ldr	r3, .L151+52
	cmp	r2, r3
	bls	.L147
.L146:
	.loc 1 1809 0
	ldr	r0, .L151+56
	bl	Alert_Message
	.loc 1 1810 0
	ldr	r3, [fp, #-56]
	ldrh	r2, [r3, #4]
	ldr	r3, [fp, #-16]
	strh	r2, [r3, #32]	@ movhi
	.loc 1 1811 0
	ldr	r3, [fp, #-56]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #28]
.L147:
	.loc 1 1815 0
	ldr	r3, [fp, #-16]
	ldrh	r3, [r3, #32]
	strh	r3, [fp, #-48]	@ movhi
	.loc 1 1816 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #28]
	str	r3, [fp, #-52]
	.loc 1 1820 0
	ldr	r3, [fp, #-56]
	sub	r2, fp, #52
	mov	r0, r2
	mov	r1, r3
	bl	DT1_IsBiggerThan_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L148
	.loc 1 1822 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	mov	r2, #0
	bl	Alert_light_ID_with_text
	.loc 1 1834 0
	ldr	r0, .L151+60
	ldr	r2, [fp, #-32]
	mov	r1, #32
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1836 0
	ldr	r1, .L151+60
	mov	r3, #976
	ldr	r2, [fp, #-32]
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #1
	strb	r3, [r2, #0]
	.loc 1 1837 0
	ldr	r1, .L151+60
	mov	r3, #976
	ldr	r2, [fp, #-32]
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #2
	strb	r3, [r2, #0]
	b	.L139
.L148:
	.loc 1 1856 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	mov	r2, #7
	bl	Alert_light_ID_with_text
	b	.L139
.L143:
	.loc 1 1862 0
	ldr	r0, .L151+64
	bl	Alert_Message
	b	.L139
.L140:
	.loc 1 1870 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	mov	r2, #6
	bl	Alert_light_ID_with_text
.L139:
	.loc 1 1879 0
	ldr	r0, .L151+32
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L137:
	.loc 1 1679 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L149
	.loc 1 1883 0
	ldr	r3, .L151+28
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	rsb	r3, r3, r2
	str	r3, [fp, #-20]
	.loc 1 1885 0
	ldr	r3, .L151+68
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-20]
	cmp	r2, r3
	bls	.L150
	.loc 1 1887 0
	ldr	r3, .L151+68
	ldr	r2, [fp, #-20]
	str	r2, [r3, #0]
.L150:
	.loc 1 1893 0
	ldr	r3, .L151+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1895 0
	ldr	r3, .L151+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1897 0
	ldr	r3, .L151
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1899 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L152:
	.align	2
.L151:
	.word	list_lights_recursive_MUTEX
	.word	.LC5
	.word	1660
	.word	lights_preserves_recursive_MUTEX
	.word	1663
	.word	list_foal_lights_recursive_MUTEX
	.word	1665
	.word	my_tick_count
	.word	light_list_hdr
	.word	foal_lights+16
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	86399
	.word	.LC22
	.word	lights_preserves
	.word	.LC23
	.word	__largest_lights_start_time_delta
.LFE21:
	.size	TDCHECK_at_1Hz_rate_check_for_lights_schedule_start, .-TDCHECK_at_1Hz_rate_check_for_lights_schedule_start
	.section .rodata
	.align	2
.LC24:
	.ascii	"Box Index\000"
	.align	2
.LC25:
	.ascii	"Output Index\000"
	.section	.text.FOAL_LIGHTS_maintain_lights_list,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_maintain_lights_list
	.type	FOAL_LIGHTS_maintain_lights_list, %function
FOAL_LIGHTS_maintain_lights_list:
.LFB22:
	.loc 1 1916 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI65:
	add	fp, sp, #4
.LCFI66:
	sub	sp, sp, #40
.LCFI67:
	str	r0, [fp, #-28]
	.loc 1 1921 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 1926 0
	ldr	r3, .L160
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L160+4
	ldr	r3, .L160+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1934 0
	bl	__foal_lights_maintenance_may_run
	mov	r3, r0
	cmp	r3, #1
	bne	.L154
	.loc 1 1945 0
	ldr	r0, .L160+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 1952 0
	b	.L155
.L159:
	.loc 1 1955 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #34
	mov	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L160+16
	str	r2, [sp, #4]
	ldr	r2, .L160+4
	str	r2, [sp, #8]
	ldr	r2, .L160+20
	str	r2, [sp, #12]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #11
	mov	r3, #0
	bl	RC_uns8_with_filename
	.loc 1 1956 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #35
	mov	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L160+24
	str	r2, [sp, #4]
	ldr	r2, .L160+4
	str	r2, [sp, #8]
	ldr	r2, .L160+28
	str	r2, [sp, #12]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #3
	mov	r3, #0
	bl	RC_uns8_with_filename
	.loc 1 1959 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #32]
	strh	r3, [fp, #-20]	@ movhi
	.loc 1 1960 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	str	r3, [fp, #-24]
	.loc 1 1963 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 1966 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-16]
	.loc 1 1970 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #27]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L156
	.loc 1 1982 0
	sub	r3, fp, #24
	ldr	r0, [fp, #-28]
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L157
	.loc 1 1996 0
	ldr	r3, [fp, #-8]
	mov	r2, #6
	strb	r2, [r3, #24]
	.loc 1 2002 0
	ldr	r0, [fp, #-8]
	bl	nm_turn_OFF_this_light
	str	r0, [fp, #-8]
	.loc 1 2011 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #34]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #35]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	mov	r2, #1
	bl	Alert_light_ID_with_text
	.loc 1 2014 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L157
	.loc 1 2017 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L157
.L156:
	.loc 1 2028 0
	sub	r3, fp, #24
	mov	r0, r3
	ldr	r1, [fp, #-28]
	bl	DT1_IsBiggerThan_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L158
	.loc 1 2040 0
	ldr	r0, [fp, #-8]
	bl	_nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_ON
	b	.L157
.L158:
	.loc 1 2057 0
	ldr	r3, [fp, #-8]
	mov	r2, #11
	strb	r2, [r3, #24]
	.loc 1 2060 0
	ldr	r0, [fp, #-8]
	bl	nm_turn_OFF_this_light
	str	r0, [fp, #-8]
	.loc 1 2066 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #34]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #35]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	mov	r2, #7
	bl	Alert_light_ID_with_text
	.loc 1 2069 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L157
	.loc 1 2072 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L157:
	.loc 1 2080 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L155
	.loc 1 2082 0
	ldr	r0, .L160+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L155:
	.loc 1 1952 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L159
.L154:
	.loc 1 2091 0
	ldr	r3, .L160
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2092 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L161:
	.align	2
.L160:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC5
	.word	1926
	.word	foal_lights+16
	.word	.LC24
	.word	1955
	.word	.LC25
	.word	1956
.LFE22:
	.size	FOAL_LIGHTS_maintain_lights_list, .-FOAL_LIGHTS_maintain_lights_list
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI20-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI23-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI26-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI29-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI32-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI35-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI38-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI41-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI42-.LCFI41
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI44-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI45-.LCFI44
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI47-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI48-.LCFI47
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI50-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI51-.LCFI50
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI53-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI54-.LCFI53
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI56-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI57-.LCFI56
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI59-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI60-.LCFI59
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI62-.LFB21
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI63-.LCFI62
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI65-.LFB22
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI66-.LCFI65
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE44:
	.text
.Letext0:
	.file 2 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/string.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/lights.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/foal_comm.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/lights_report_data.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/FreeRTOSConfig.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x10c2
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF172
	.byte	0x1
	.4byte	.LASF173
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x2
	.byte	0x16
	.4byte	0x30
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x5
	.byte	0x4
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x3
	.byte	0x35
	.4byte	0x30
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x4
	.byte	0x57
	.4byte	0x45
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x5
	.byte	0x4c
	.4byte	0x7c
	.uleb128 0x6
	.4byte	0x5c
	.4byte	0xa2
	.uleb128 0x7
	.4byte	0x30
	.byte	0x1
	.byte	0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF12
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x6
	.byte	0x3a
	.4byte	0x5c
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x6
	.byte	0x4c
	.4byte	0x3e
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x6
	.byte	0x5e
	.4byte	0xca
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF16
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x6
	.byte	0x99
	.4byte	0xca
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x6
	.byte	0x9d
	.4byte	0xca
	.uleb128 0x8
	.byte	0x6
	.byte	0x7
	.byte	0x22
	.4byte	0x108
	.uleb128 0x9
	.ascii	"T\000"
	.byte	0x7
	.byte	0x24
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.ascii	"D\000"
	.byte	0x7
	.byte	0x26
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x7
	.byte	0x28
	.4byte	0xe7
	.uleb128 0x8
	.byte	0x14
	.byte	0x7
	.byte	0x31
	.4byte	0x19a
	.uleb128 0xa
	.4byte	.LASF20
	.byte	0x7
	.byte	0x33
	.4byte	0x108
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x7
	.byte	0x35
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x7
	.byte	0x35
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x7
	.byte	0x35
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x7
	.byte	0x37
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF25
	.byte	0x7
	.byte	0x37
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0xa
	.4byte	.LASF26
	.byte	0x7
	.byte	0x37
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x7
	.byte	0x39
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0xa
	.4byte	.LASF28
	.byte	0x7
	.byte	0x3b
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x2
	.4byte	.LASF29
	.byte	0x7
	.byte	0x3d
	.4byte	0x113
	.uleb128 0x6
	.4byte	0xbf
	.4byte	0x1b5
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2
	.byte	0
	.uleb128 0x6
	.4byte	0xa2
	.4byte	0x1c5
	.uleb128 0x7
	.4byte	0x30
	.byte	0xf
	.byte	0
	.uleb128 0x8
	.byte	0x14
	.byte	0x8
	.byte	0x18
	.4byte	0x214
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x8
	.byte	0x1a
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF31
	.byte	0x8
	.byte	0x1c
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF32
	.byte	0x8
	.byte	0x1e
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x8
	.byte	0x20
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF34
	.byte	0x8
	.byte	0x23
	.4byte	0xd1
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x2
	.4byte	.LASF35
	.byte	0x8
	.byte	0x26
	.4byte	0x1c5
	.uleb128 0x2
	.4byte	.LASF36
	.byte	0x8
	.byte	0x26
	.4byte	0x22a
	.uleb128 0xb
	.byte	0x4
	.4byte	0x1c5
	.uleb128 0x8
	.byte	0xc
	.byte	0x8
	.byte	0x2a
	.4byte	0x263
	.uleb128 0xa
	.4byte	.LASF37
	.byte	0x8
	.byte	0x2c
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x8
	.byte	0x2e
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x8
	.byte	0x30
	.4byte	0x263
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x214
	.uleb128 0x2
	.4byte	.LASF40
	.byte	0x8
	.byte	0x32
	.4byte	0x230
	.uleb128 0xb
	.byte	0x4
	.4byte	0xa9
	.uleb128 0x3
	.byte	0x4
	.byte	0x4
	.4byte	.LASF41
	.uleb128 0x6
	.4byte	0xbf
	.4byte	0x291
	.uleb128 0x7
	.4byte	0x30
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.4byte	.LASF42
	.byte	0x9
	.byte	0x5d
	.4byte	0x29c
	.uleb128 0xc
	.4byte	.LASF42
	.byte	0x1
	.uleb128 0xd
	.byte	0x8
	.byte	0xa
	.2byte	0x234
	.4byte	0x2e8
	.uleb128 0xe
	.4byte	.LASF43
	.byte	0xa
	.2byte	0x236
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0xa
	.2byte	0x238
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF45
	.byte	0xa
	.2byte	0x23b
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0xa
	.2byte	0x23d
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x7
	.byte	0
	.uleb128 0xf
	.4byte	.LASF47
	.byte	0xa
	.2byte	0x241
	.4byte	0x2a2
	.uleb128 0xd
	.byte	0x8
	.byte	0xa
	.2byte	0x245
	.4byte	0x31c
	.uleb128 0xe
	.4byte	.LASF45
	.byte	0xa
	.2byte	0x248
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0xa
	.2byte	0x24a
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xf
	.4byte	.LASF48
	.byte	0xa
	.2byte	0x24e
	.4byte	0x2f4
	.uleb128 0xd
	.byte	0x10
	.byte	0xa
	.2byte	0x253
	.4byte	0x36e
	.uleb128 0xe
	.4byte	.LASF49
	.byte	0xa
	.2byte	0x258
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF45
	.byte	0xa
	.2byte	0x25a
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0xa
	.2byte	0x260
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF43
	.byte	0xa
	.2byte	0x263
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0xf
	.4byte	.LASF50
	.byte	0xa
	.2byte	0x268
	.4byte	0x328
	.uleb128 0xd
	.byte	0x8
	.byte	0xa
	.2byte	0x26c
	.4byte	0x3a2
	.uleb128 0xe
	.4byte	.LASF49
	.byte	0xa
	.2byte	0x271
	.4byte	0xd1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF45
	.byte	0xa
	.2byte	0x273
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xf
	.4byte	.LASF51
	.byte	0xa
	.2byte	0x27c
	.4byte	0x37a
	.uleb128 0x8
	.byte	0x10
	.byte	0xb
	.byte	0x2b
	.4byte	0x419
	.uleb128 0xa
	.4byte	.LASF52
	.byte	0xb
	.byte	0x32
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF53
	.byte	0xb
	.byte	0x35
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF54
	.byte	0xb
	.byte	0x38
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF55
	.byte	0xb
	.byte	0x3b
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xa
	.4byte	.LASF56
	.byte	0xb
	.byte	0x3e
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0xb
	.byte	0x41
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0xa
	.4byte	.LASF58
	.byte	0xb
	.byte	0x44
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.byte	0
	.uleb128 0x2
	.4byte	.LASF59
	.byte	0xb
	.byte	0x4a
	.4byte	0x3ae
	.uleb128 0x3
	.byte	0x8
	.byte	0x4
	.4byte	.LASF60
	.uleb128 0xd
	.byte	0x1
	.byte	0xc
	.2byte	0x944
	.4byte	0x48f
	.uleb128 0x10
	.4byte	.LASF61
	.byte	0xc
	.2byte	0x94c
	.4byte	0xdc
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF62
	.byte	0xc
	.2byte	0x94f
	.4byte	0xdc
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF63
	.byte	0xc
	.2byte	0x953
	.4byte	0xdc
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF64
	.byte	0xc
	.2byte	0x958
	.4byte	0xdc
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF65
	.byte	0xc
	.2byte	0x95c
	.4byte	0xdc
	.byte	0x4
	.byte	0x4
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.byte	0xc
	.2byte	0x940
	.4byte	0x4aa
	.uleb128 0x12
	.4byte	.LASF174
	.byte	0xc
	.2byte	0x942
	.4byte	0xa9
	.uleb128 0x13
	.4byte	0x42b
	.byte	0
	.uleb128 0xf
	.4byte	.LASF66
	.byte	0xc
	.2byte	0x962
	.4byte	0x48f
	.uleb128 0xd
	.byte	0x14
	.byte	0xc
	.2byte	0x966
	.4byte	0x4ed
	.uleb128 0x14
	.ascii	"lrr\000"
	.byte	0xc
	.2byte	0x96a
	.4byte	0x419
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF67
	.byte	0xc
	.2byte	0x970
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF68
	.byte	0xc
	.2byte	0x972
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.byte	0
	.uleb128 0xf
	.4byte	.LASF69
	.byte	0xc
	.2byte	0x978
	.4byte	0x4b6
	.uleb128 0x15
	.2byte	0x400
	.byte	0xc
	.2byte	0x97c
	.4byte	0x532
	.uleb128 0xe
	.4byte	.LASF70
	.byte	0xc
	.2byte	0x983
	.4byte	0x1b5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF71
	.byte	0xc
	.2byte	0x988
	.4byte	0x532
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x14
	.ascii	"lbf\000"
	.byte	0xc
	.2byte	0x98d
	.4byte	0x542
	.byte	0x3
	.byte	0x23
	.uleb128 0x3d0
	.byte	0
	.uleb128 0x6
	.4byte	0x4ed
	.4byte	0x542
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2f
	.byte	0
	.uleb128 0x6
	.4byte	0x4aa
	.4byte	0x552
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2f
	.byte	0
	.uleb128 0xf
	.4byte	.LASF72
	.byte	0xc
	.2byte	0x996
	.4byte	0x4f9
	.uleb128 0xd
	.byte	0x24
	.byte	0xc
	.2byte	0x99d
	.4byte	0x5fe
	.uleb128 0xe
	.4byte	.LASF73
	.byte	0xc
	.2byte	0x9a2
	.4byte	0x269
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF74
	.byte	0xc
	.2byte	0x9a6
	.4byte	0x269
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0xc
	.2byte	0x9ac
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.4byte	.LASF75
	.byte	0xc
	.2byte	0x9ae
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x19
	.uleb128 0xe
	.4byte	.LASF76
	.byte	0xc
	.2byte	0x9b0
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x1a
	.uleb128 0xe
	.4byte	.LASF62
	.byte	0xc
	.2byte	0x9b2
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x1b
	.uleb128 0xe
	.4byte	.LASF43
	.byte	0xc
	.2byte	0x9b6
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0xc
	.2byte	0x9b8
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xe
	.4byte	.LASF56
	.byte	0xc
	.2byte	0x9c0
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x22
	.uleb128 0xe
	.4byte	.LASF57
	.byte	0xc
	.2byte	0x9c2
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x23
	.byte	0
	.uleb128 0xf
	.4byte	.LASF77
	.byte	0xc
	.2byte	0x9c7
	.4byte	0x55e
	.uleb128 0x15
	.2byte	0x6f8
	.byte	0xc
	.2byte	0x9cb
	.4byte	0x651
	.uleb128 0xe
	.4byte	.LASF70
	.byte	0xc
	.2byte	0x9cd
	.4byte	0x1b5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF78
	.byte	0xc
	.2byte	0x9d5
	.4byte	0x214
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF79
	.byte	0xc
	.2byte	0x9df
	.4byte	0x214
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xe
	.4byte	.LASF80
	.byte	0xc
	.2byte	0x9e7
	.4byte	0x651
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.byte	0
	.uleb128 0x6
	.4byte	0x5fe
	.4byte	0x661
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2f
	.byte	0
	.uleb128 0xf
	.4byte	.LASF81
	.byte	0xc
	.2byte	0x9ed
	.4byte	0x60a
	.uleb128 0x16
	.4byte	.LASF85
	.byte	0x1
	.byte	0xd0
	.byte	0x1
	.4byte	0x6c2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x6c2
	.uleb128 0x17
	.4byte	.LASF82
	.byte	0x1
	.byte	0xd0
	.4byte	0x21f
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF83
	.byte	0x1
	.byte	0xd0
	.4byte	0x6c8
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF84
	.byte	0x1
	.byte	0xd0
	.4byte	0x6c8
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x18
	.4byte	.LASF88
	.byte	0x1
	.byte	0xd2
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x5fe
	.uleb128 0x19
	.4byte	0xbf
	.uleb128 0x1a
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x115
	.byte	0x1
	.4byte	0x6c2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x727
	.uleb128 0x1b
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x115
	.4byte	0x6c8
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x115
	.4byte	0x6c8
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x115
	.4byte	0xd1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1c
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x117
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x14c
	.byte	0x1
	.4byte	0xd1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x753
	.uleb128 0x1d
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x14e
	.4byte	0xd1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x164
	.byte	0x1
	.4byte	0xd1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x7bc
	.uleb128 0x1b
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x164
	.4byte	0x7bc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1b
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x164
	.4byte	0x7c2
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x164
	.4byte	0x6c8
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x164
	.4byte	0x6c8
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1c
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x166
	.4byte	0xd1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0xb4
	.uleb128 0xb
	.byte	0x4
	.4byte	0xbf
	.uleb128 0x1a
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x1a4
	.byte	0x1
	.4byte	0xd1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x88b
	.uleb128 0x1b
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x1a4
	.4byte	0x88b
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1b
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x1a4
	.4byte	0x89b
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x1b
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x1a4
	.4byte	0x7c2
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1b
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x1a4
	.4byte	0x7c2
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x1c
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x1a6
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x1a6
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1c
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x1a7
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1c
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x1a8
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x1a9
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1c
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x1aa
	.4byte	0xd1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1c
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x1ab
	.4byte	0xd1
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x19
	.4byte	0x890
	.uleb128 0xb
	.byte	0x4
	.4byte	0x896
	.uleb128 0x19
	.4byte	0x19a
	.uleb128 0x19
	.4byte	0x8a0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x291
	.uleb128 0x1e
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x234
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x8fc
	.uleb128 0x1b
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x234
	.4byte	0xd1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x23f
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x241
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x241
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF175
	.byte	0x1
	.2byte	0x2a6
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF125
	.byte	0x1
	.2byte	0x2bb
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x93c
	.uleb128 0x1b
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x2bb
	.4byte	0x93c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	0xd1
	.uleb128 0x1e
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x2fd
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x96a
	.uleb128 0x1b
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x2fd
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x323
	.byte	0x1
	.4byte	0x6c2
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x9a6
	.uleb128 0x1b
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x323
	.4byte	0x9a6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1c
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x325
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	0x6c2
	.uleb128 0x1a
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x343
	.byte	0x1
	.4byte	0x6c2
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x9e7
	.uleb128 0x1b
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x343
	.4byte	0x9a6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1c
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x345
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x387
	.byte	0x1
	.4byte	0xd1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0xa41
	.uleb128 0x1b
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x387
	.4byte	0x9a6
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x387
	.4byte	0x89b
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1c
	.4byte	.LASF122
	.byte	0x1
	.2byte	0x389
	.4byte	0xd1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x395
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF123
	.byte	0x1
	.2byte	0x3d4
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0xa79
	.uleb128 0x1b
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x3d4
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1c
	.4byte	.LASF124
	.byte	0x1
	.2byte	0x3d6
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x40e
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0xae9
	.uleb128 0x21
	.ascii	"lxr\000"
	.byte	0x1
	.2byte	0x40e
	.4byte	0xae9
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1c
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x410
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x411
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1c
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x413
	.4byte	0x8a0
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x22
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x42f
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x36e
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF127
	.byte	0x1
	.2byte	0x484
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0xb46
	.uleb128 0x21
	.ascii	"lxr\000"
	.byte	0x1
	.2byte	0x484
	.4byte	0xb46
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1c
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x486
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x487
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x489
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x3a2
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF132
	.byte	0x1
	.2byte	0x4d7
	.byte	0x1
	.4byte	0xbf
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0xbe8
	.uleb128 0x1b
	.4byte	.LASF128
	.byte	0x1
	.2byte	0x4d7
	.4byte	0xbe8
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1c
	.4byte	.LASF129
	.byte	0x1
	.2byte	0x4d9
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF130
	.byte	0x1
	.2byte	0x4e8
	.4byte	0xb4
	.byte	0x2
	.byte	0x91
	.sleb128 -26
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x4ea
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x22
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0x1c
	.4byte	.LASF131
	.byte	0x1
	.2byte	0x505
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1d
	.ascii	"ucp\000"
	.byte	0x1
	.2byte	0x507
	.4byte	0x274
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x22
	.4byte	.LBB4
	.4byte	.LBE4
	.uleb128 0x1d
	.ascii	"lxr\000"
	.byte	0x1
	.2byte	0x529
	.4byte	0x2e8
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x274
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF133
	.byte	0x1
	.2byte	0x54d
	.byte	0x1
	.4byte	0xbf
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0xc89
	.uleb128 0x1b
	.4byte	.LASF128
	.byte	0x1
	.2byte	0x54d
	.4byte	0xbe8
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1d
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x54f
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF134
	.byte	0x1
	.2byte	0x55b
	.4byte	0xb4
	.byte	0x2
	.byte	0x91
	.sleb128 -26
	.uleb128 0x22
	.4byte	.LBB5
	.4byte	.LBE5
	.uleb128 0x1c
	.4byte	.LASF131
	.byte	0x1
	.2byte	0x564
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1d
	.ascii	"ucp\000"
	.byte	0x1
	.2byte	0x566
	.4byte	0x274
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF135
	.byte	0x1
	.2byte	0x578
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x22
	.4byte	.LBB6
	.4byte	.LBE6
	.uleb128 0x1c
	.4byte	.LASF136
	.byte	0x1
	.2byte	0x57e
	.4byte	0x31c
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF137
	.byte	0x1
	.2byte	0x5a8
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0xcf9
	.uleb128 0x1b
	.4byte	.LASF138
	.byte	0x1
	.2byte	0x5a8
	.4byte	0xae9
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1c
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x5aa
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x5ab
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1c
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x5ad
	.4byte	0x8a0
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x22
	.4byte	.LBB7
	.4byte	.LBE7
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x5c5
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF139
	.byte	0x1
	.2byte	0x604
	.byte	0x1
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0xd5f
	.uleb128 0x1b
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x604
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x604
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.4byte	.LASF140
	.byte	0x1
	.2byte	0x604
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1b
	.4byte	.LASF141
	.byte	0x1
	.2byte	0x604
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x60e
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF142
	.byte	0x1
	.2byte	0x634
	.byte	0x1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0xdb6
	.uleb128 0x1b
	.4byte	.LASF143
	.byte	0x1
	.2byte	0x634
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.4byte	.LASF140
	.byte	0x1
	.2byte	0x634
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.4byte	.LASF141
	.byte	0x1
	.2byte	0x634
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1d
	.ascii	"iii\000"
	.byte	0x1
	.2byte	0x636
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF144
	.byte	0x1
	.2byte	0x63f
	.byte	0x1
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.4byte	0xdfe
	.uleb128 0x1b
	.4byte	.LASF138
	.byte	0x1
	.2byte	0x63f
	.4byte	0xb46
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x641
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x643
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF145
	.byte	0x1
	.2byte	0x665
	.byte	0x1
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0xebe
	.uleb128 0x1b
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x665
	.4byte	0x88b
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x1c
	.4byte	.LASF146
	.byte	0x1
	.2byte	0x667
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x1c
	.4byte	.LASF147
	.byte	0x1
	.2byte	0x669
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1c
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x66b
	.4byte	0x8a0
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1c
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x66d
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1c
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x66f
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1c
	.4byte	.LASF148
	.byte	0x1
	.2byte	0x671
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1c
	.4byte	.LASF149
	.byte	0x1
	.2byte	0x673
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x675
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF150
	.byte	0x1
	.2byte	0x677
	.4byte	0x108
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x1c
	.4byte	.LASF151
	.byte	0x1
	.2byte	0x687
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF152
	.byte	0x1
	.2byte	0x77b
	.byte	0x1
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0xf24
	.uleb128 0x1b
	.4byte	.LASF153
	.byte	0x1
	.2byte	0x77b
	.4byte	0xf24
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1c
	.4byte	.LASF135
	.byte	0x1
	.2byte	0x77d
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1c
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x77d
	.4byte	0x6c2
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.4byte	.LASF150
	.byte	0x1
	.2byte	0x77f
	.4byte	0x108
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1c
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x781
	.4byte	0xd1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x19
	.4byte	0xf29
	.uleb128 0xb
	.byte	0x4
	.4byte	0xf2f
	.uleb128 0x19
	.4byte	0x108
	.uleb128 0x24
	.4byte	.LASF160
	.byte	0xe
	.byte	0x7c
	.4byte	0x30
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF156
	.byte	0xd
	.byte	0x30
	.4byte	0xf52
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x19
	.4byte	0x92
	.uleb128 0x18
	.4byte	.LASF157
	.byte	0xd
	.byte	0x34
	.4byte	0xf68
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x19
	.4byte	0x92
	.uleb128 0x18
	.4byte	.LASF158
	.byte	0xd
	.byte	0x36
	.4byte	0xf7e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x19
	.4byte	0x92
	.uleb128 0x18
	.4byte	.LASF159
	.byte	0xd
	.byte	0x38
	.4byte	0xf94
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x19
	.4byte	0x92
	.uleb128 0x24
	.4byte	.LASF161
	.byte	0x9
	.byte	0x62
	.4byte	0x214
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF162
	.byte	0xf
	.byte	0x33
	.4byte	0xfb7
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x19
	.4byte	0x1a5
	.uleb128 0x18
	.4byte	.LASF163
	.byte	0xf
	.byte	0x3f
	.4byte	0xfcd
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x19
	.4byte	0x281
	.uleb128 0x25
	.4byte	.LASF164
	.byte	0xc
	.2byte	0x998
	.4byte	0x552
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF165
	.byte	0xc
	.2byte	0x9f7
	.4byte	0x661
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF166
	.byte	0x10
	.byte	0xc0
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF167
	.byte	0x10
	.byte	0xfe
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF168
	.byte	0x10
	.2byte	0x101
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF169
	.byte	0x10
	.2byte	0x107
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF170
	.byte	0x1
	.byte	0x32
	.4byte	0x1035
	.byte	0x5
	.byte	0x3
	.4byte	FOAL_LIGHTS_PRE_TEST_STRING
	.uleb128 0x19
	.4byte	0x1b5
	.uleb128 0x24
	.4byte	.LASF171
	.byte	0x1
	.byte	0x39
	.4byte	0xbf
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF160
	.byte	0xe
	.byte	0x7c
	.4byte	0x30
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF161
	.byte	0x9
	.byte	0x62
	.4byte	0x214
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF164
	.byte	0xc
	.2byte	0x998
	.4byte	0x552
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF165
	.byte	0xc
	.2byte	0x9f7
	.4byte	0x661
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF166
	.byte	0x10
	.byte	0xc0
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF167
	.byte	0x10
	.byte	0xfe
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF168
	.byte	0x10
	.2byte	0x101
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	.LASF169
	.byte	0x10
	.2byte	0x107
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x26
	.4byte	.LASF171
	.byte	0x1
	.byte	0x39
	.4byte	0xbf
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	__largest_lights_start_time_delta
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI21
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI24
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI33
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI36
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI39
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI42
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI45
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI48
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI51
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI54
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI57
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI60
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI62
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI63
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI65
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI66
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xcc
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF32:
	.ascii	"count\000"
.LASF38:
	.ascii	"pNext\000"
.LASF81:
	.ascii	"FOAL_LIGHTS_BATTERY_BACKED_STRUCT\000"
.LASF98:
	.ascii	"lls_ptr\000"
.LASF62:
	.ascii	"light_is_energized\000"
.LASF174:
	.ascii	"overall_size\000"
.LASF110:
	.ascii	"box_index\000"
.LASF84:
	.ascii	"poutput_index_0\000"
.LASF2:
	.ascii	"signed char\000"
.LASF158:
	.ascii	"GuiFont_DecimalChar\000"
.LASF35:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF171:
	.ascii	"__largest_lights_start_time_delta\000"
.LASF77:
	.ascii	"LIGHTS_LIST_COMPONENT\000"
.LASF133:
	.ascii	"FOAL_LIGHTS_load_lights_action_needed_list_into_out"
	.ascii	"going_token\000"
.LASF89:
	.ascii	"__foal_lights_maintenance_may_run\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF17:
	.ascii	"BOOL_32\000"
.LASF30:
	.ascii	"phead\000"
.LASF150:
	.ascii	"stop_dt\000"
.LASF54:
	.ascii	"record_start_date\000"
.LASF121:
	.ascii	"plgs_ptr\000"
.LASF44:
	.ascii	"stop_datetime_d\000"
.LASF82:
	.ascii	"plist_hdr\000"
.LASF15:
	.ascii	"UNS_32\000"
.LASF151:
	.ascii	"__this_time_delta\000"
.LASF19:
	.ascii	"DATE_TIME\000"
.LASF6:
	.ascii	"long long int\000"
.LASF45:
	.ascii	"light_index_0_47\000"
.LASF115:
	.ascii	"pllc\000"
.LASF43:
	.ascii	"stop_datetime_t\000"
.LASF132:
	.ascii	"FOAL_LIGHTS_load_lights_on_list_into_outgoing_token"
	.ascii	"\000"
.LASF109:
	.ascii	"lllc_ptr\000"
.LASF123:
	.ascii	"_nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_"
	.ascii	"ON\000"
.LASF131:
	.ascii	"llength\000"
.LASF94:
	.ascii	"stop_time\000"
.LASF4:
	.ascii	"long int\000"
.LASF92:
	.ascii	"target_time\000"
.LASF85:
	.ascii	"nm_FOAL_LIGHTS_find_this_light_on_the_list\000"
.LASF34:
	.ascii	"InUse\000"
.LASF25:
	.ascii	"__minutes\000"
.LASF113:
	.ascii	"FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain"
	.ascii	"_goes_down\000"
.LASF9:
	.ascii	"portTickType\000"
.LASF46:
	.ascii	"action_needed\000"
.LASF147:
	.ascii	"temp_time\000"
.LASF126:
	.ascii	"FOAL_LIGHTS_initiate_a_mobile_light_ON\000"
.LASF159:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF86:
	.ascii	"nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light"
	.ascii	"\000"
.LASF175:
	.ascii	"FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri"
	.ascii	"_side_lights\000"
.LASF58:
	.ascii	"lrr_pad_bytes_avaiable_for_use\000"
.LASF104:
	.ascii	"temp_stop_time\000"
.LASF51:
	.ascii	"LIGHTS_OFF_XFER_RECORD\000"
.LASF8:
	.ascii	"size_t\000"
.LASF52:
	.ascii	"programmed_seconds\000"
.LASF134:
	.ascii	"action_records_to_xfer\000"
.LASF93:
	.ascii	"stop_date\000"
.LASF91:
	.ascii	"target_date\000"
.LASF16:
	.ascii	"unsigned int\000"
.LASF67:
	.ascii	"last_measured_current_ma\000"
.LASF125:
	.ascii	"init_battery_backed_foal_lights\000"
.LASF129:
	.ascii	"bytes_to_send\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF95:
	.ascii	"target_was_modified\000"
.LASF154:
	.ascii	"current_llc\000"
.LASF149:
	.ascii	"llights_preserves_index\000"
.LASF146:
	.ascii	"temp_date\000"
.LASF53:
	.ascii	"manual_seconds\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF172:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF39:
	.ascii	"pListHdr\000"
.LASF166:
	.ascii	"system_preserves_recursive_MUTEX\000"
.LASF36:
	.ascii	"MIST_LIST_HDR_TYPE_PTR\000"
.LASF79:
	.ascii	"header_for_foal_lights_with_action_needed_list\000"
.LASF153:
	.ascii	"pdate_time\000"
.LASF42:
	.ascii	"LIGHT_STRUCT\000"
.LASF112:
	.ascii	"pforce_the_initialization\000"
.LASF10:
	.ascii	"xQueueHandle\000"
.LASF143:
	.ascii	"pcontroller_index\000"
.LASF157:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF68:
	.ascii	"expansion_bytes\000"
.LASF107:
	.ascii	"stop_2_is_usable\000"
.LASF135:
	.ascii	"lllc\000"
.LASF100:
	.ascii	"soonest_stop_time\000"
.LASF144:
	.ascii	"FOAL_LIGHTS_process_light_off_from_token_response\000"
.LASF75:
	.ascii	"reason_in_list\000"
.LASF148:
	.ascii	"day_index\000"
.LASF55:
	.ascii	"number_of_on_cycles\000"
.LASF78:
	.ascii	"header_for_foal_lights_ON_list\000"
.LASF24:
	.ascii	"__hours\000"
.LASF136:
	.ascii	"llxr\000"
.LASF27:
	.ascii	"__dayofweek\000"
.LASF74:
	.ascii	"list_linkage_for_foal_lights_action_needed_list\000"
.LASF61:
	.ascii	"light_is_available\000"
.LASF57:
	.ascii	"output_index_0\000"
.LASF76:
	.ascii	"xfer_to_irri_machines\000"
.LASF141:
	.ascii	"palerts_reason\000"
.LASF164:
	.ascii	"lights_preserves\000"
.LASF31:
	.ascii	"ptail\000"
.LASF169:
	.ascii	"lights_preserves_recursive_MUTEX\000"
.LASF90:
	.ascii	"FOAL_LIGHTS_set_stop_date_and_time_to_furthest\000"
.LASF59:
	.ascii	"LIGHTS_REPORT_RECORD\000"
.LASF41:
	.ascii	"float\000"
.LASF22:
	.ascii	"__month\000"
.LASF156:
	.ascii	"GuiFont_LanguageActive\000"
.LASF99:
	.ascii	"soonest_stop_date\000"
.LASF118:
	.ascii	"next_llc_ptr\000"
.LASF63:
	.ascii	"shorted_output\000"
.LASF145:
	.ascii	"TDCHECK_at_1Hz_rate_check_for_lights_schedule_start"
	.ascii	"\000"
.LASF97:
	.ascii	"pdtcs_ptr\000"
.LASF11:
	.ascii	"xSemaphoreHandle\000"
.LASF127:
	.ascii	"FOAL_LIGHTS_initiate_a_mobile_light_OFF\000"
.LASF120:
	.ascii	"_nm_nm_nm_add_to_the_lights_ON_list\000"
.LASF161:
	.ascii	"light_list_hdr\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF130:
	.ascii	"lnumber_of_lights\000"
.LASF50:
	.ascii	"LIGHTS_ON_XFER_RECORD\000"
.LASF165:
	.ascii	"foal_lights\000"
.LASF28:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF3:
	.ascii	"short int\000"
.LASF140:
	.ascii	"paction_needed\000"
.LASF47:
	.ascii	"LIGHTS_LIST_XFER_RECORD\000"
.LASF87:
	.ascii	"initialize\000"
.LASF40:
	.ascii	"MIST_DLINK_TYPE\000"
.LASF65:
	.ascii	"reason\000"
.LASF106:
	.ascii	"stop_1_is_usable\000"
.LASF72:
	.ascii	"LIGHTS_BB_STRUCT\000"
.LASF49:
	.ascii	"in_use\000"
.LASF48:
	.ascii	"LIGHTS_ACTION_XFER_RECORD\000"
.LASF138:
	.ascii	"lxr_ptr\000"
.LASF102:
	.ascii	"todays_index\000"
.LASF69:
	.ascii	"BY_LIGHTS_RECORD\000"
.LASF137:
	.ascii	"FOAL_LIGHTS_process_light_on_from_token_response\000"
.LASF12:
	.ascii	"char\000"
.LASF26:
	.ascii	"__seconds\000"
.LASF119:
	.ascii	"nm_turn_OFF_this_light\000"
.LASF167:
	.ascii	"list_lights_recursive_MUTEX\000"
.LASF160:
	.ascii	"my_tick_count\000"
.LASF128:
	.ascii	"pdata_ptr\000"
.LASF33:
	.ascii	"offset\000"
.LASF170:
	.ascii	"FOAL_LIGHTS_PRE_TEST_STRING\000"
.LASF73:
	.ascii	"list_linkage_for_foal_lights_ON_list\000"
.LASF168:
	.ascii	"list_foal_lights_recursive_MUTEX\000"
.LASF66:
	.ascii	"LIGHTS_BIT_FIELD\000"
.LASF152:
	.ascii	"FOAL_LIGHTS_maintain_lights_list\000"
.LASF163:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF23:
	.ascii	"__year\000"
.LASF20:
	.ascii	"date_time\000"
.LASF173:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_lights.c\000"
.LASF14:
	.ascii	"UNS_16\000"
.LASF139:
	.ascii	"FOAL_LIGHTS_turn_off_this_lights_output\000"
.LASF83:
	.ascii	"pbox_index_0\000"
.LASF71:
	.ascii	"lights\000"
.LASF117:
	.ascii	"pllc_ptr\000"
.LASF13:
	.ascii	"UNS_8\000"
.LASF105:
	.ascii	"stop_time_in_schedule\000"
.LASF124:
	.ascii	"array_index\000"
.LASF108:
	.ascii	"pcompletely_wipe\000"
.LASF101:
	.ascii	"day_counter\000"
.LASF21:
	.ascii	"__day\000"
.LASF64:
	.ascii	"no_current_output\000"
.LASF111:
	.ascii	"output_index\000"
.LASF114:
	.ascii	"nm_FOAL_LIGHTS_add_to_action_needed_list\000"
.LASF56:
	.ascii	"box_index_0\000"
.LASF37:
	.ascii	"pPrev\000"
.LASF96:
	.ascii	"nm_FOAL_LIGHTS_get_soonest_stop_time\000"
.LASF155:
	.ascii	"get_the_next_llc\000"
.LASF70:
	.ascii	"verify_string_pre\000"
.LASF142:
	.ascii	"FOAL_LIGHTS_turn_off_all_lights_at_this_box\000"
.LASF18:
	.ascii	"BITFIELD_BOOL\000"
.LASF162:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF88:
	.ascii	"llc_ptr\000"
.LASF116:
	.ascii	"nm_remove_from_the_lights_ON_list\000"
.LASF80:
	.ascii	"llcs\000"
.LASF60:
	.ascii	"double\000"
.LASF29:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF122:
	.ascii	"success\000"
.LASF103:
	.ascii	"temp_stop_date\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
