	.file	"e_flow_checking.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.g_FLOW_CHECKING_editing_group,"aw",%nobits
	.align	2
	.type	g_FLOW_CHECKING_editing_group, %object
	.size	g_FLOW_CHECKING_editing_group, 4
g_FLOW_CHECKING_editing_group:
	.space	4
	.section	.bss.g_FLOW_CHECKING_prev_cursor_pos,"aw",%nobits
	.align	2
	.type	g_FLOW_CHECKING_prev_cursor_pos, %object
	.size	g_FLOW_CHECKING_prev_cursor_pos, 4
g_FLOW_CHECKING_prev_cursor_pos:
	.space	4
	.section	.text.FDTO_FLOW_CHECKING_show_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_FLOW_CHECKING_show_dropdown, %function
FDTO_FLOW_CHECKING_show_dropdown:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_flow_checking.c"
	.loc 1 56 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	.loc 1 57 0
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	mov	r0, #272
	mov	r1, #34
	mov	r2, r3
	bl	FDTO_COMBO_BOX_show_no_yes_dropdown
	.loc 1 58 0
	ldmfd	sp!, {fp, pc}
.L3:
	.align	2
.L2:
	.word	GuiVar_SystemFlowCheckingInUse
.LFE0:
	.size	FDTO_FLOW_CHECKING_show_dropdown, .-FDTO_FLOW_CHECKING_show_dropdown
	.section	.text.FLOW_CHECKING_process_group,"ax",%progbits
	.align	2
	.type	FLOW_CHECKING_process_group, %function
FLOW_CHECKING_process_group:
.LFB1:
	.loc 1 62 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI2:
	add	fp, sp, #8
.LCFI3:
	sub	sp, sp, #52
.LCFI4:
	str	r0, [fp, #-52]
	str	r1, [fp, #-48]
	.loc 1 65 0
	ldr	r3, .L54
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #740
	bne	.L51
.L6:
	.loc 1 68 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L54+4
	bl	COMBO_BOX_key_press
	.loc 1 69 0
	b	.L4
.L51:
	.loc 1 72 0
	ldr	r3, [fp, #-52]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L8
.L17:
	.word	.L9
	.word	.L10
	.word	.L11
	.word	.L12
	.word	.L13
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L14
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L14
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L15
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L16
	.word	.L8
	.word	.L8
	.word	.L8
	.word	.L16
.L11:
	.loc 1 75 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L52
.L19:
	.loc 1 78 0
	bl	good_key_beep
	.loc 1 79 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 80 0
	ldr	r3, .L54+12
	str	r3, [fp, #-24]
	.loc 1 81 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 82 0
	mov	r0, r0	@ nop
	.loc 1 87 0
	b	.L4
.L52:
	.loc 1 85 0
	bl	bad_key_beep
	.loc 1 87 0
	b	.L4
.L16:
	.loc 1 91 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L21
.L34:
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
.L22:
	.loc 1 94 0
	ldr	r0, .L54+4
	bl	process_bool
	.loc 1 95 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 96 0
	b	.L35
.L23:
	.loc 1 99 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L54+16
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	mov	r1, #1
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	ldr	r1, .L54+20
	mov	r2, #10
	bl	process_uns32
	.loc 1 100 0
	b	.L35
.L26:
	.loc 1 103 0
	ldr	r1, [fp, #-52]
	ldr	r3, .L54+20
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L54+24
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	mov	r0, #1
	str	r0, [sp, #0]
	mov	r0, #0
	str	r0, [sp, #4]
	mov	r0, r1
	ldr	r1, .L54+16
	bl	process_uns32
	.loc 1 104 0
	b	.L35
.L29:
	.loc 1 107 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L54+16
	ldr	r3, [r3, #0]
	add	r3, r3, #1
	mov	r1, #1
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	ldr	r1, .L54+24
	mov	r2, r3
	mov	r3, #4000
	bl	process_uns32
	.loc 1 108 0
	b	.L35
.L24:
	.loc 1 111 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L54+28
	mov	r2, #1
	mov	r3, #200
	bl	process_uns32
	.loc 1 112 0
	b	.L35
.L27:
	.loc 1 115 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L54+32
	mov	r2, #1
	mov	r3, #200
	bl	process_uns32
	.loc 1 116 0
	b	.L35
.L30:
	.loc 1 119 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L54+36
	mov	r2, #1
	mov	r3, #200
	bl	process_uns32
	.loc 1 120 0
	b	.L35
.L32:
	.loc 1 123 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L54+40
	mov	r2, #1
	mov	r3, #200
	bl	process_uns32
	.loc 1 124 0
	b	.L35
.L25:
	.loc 1 127 0
	ldr	r3, .L54+44
	ldr	r3, [r3, #0]
	rsb	r2, r3, #0
	ldr	r3, .L54+44
	str	r2, [r3, #0]
	.loc 1 128 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L54+44
	mov	r2, #1
	mov	r3, #200
	bl	process_int32
	.loc 1 129 0
	ldr	r3, .L54+44
	ldr	r3, [r3, #0]
	rsb	r2, r3, #0
	ldr	r3, .L54+44
	str	r2, [r3, #0]
	.loc 1 130 0
	b	.L35
.L28:
	.loc 1 133 0
	ldr	r3, .L54+48
	ldr	r3, [r3, #0]
	rsb	r2, r3, #0
	ldr	r3, .L54+48
	str	r2, [r3, #0]
	.loc 1 134 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L54+48
	mov	r2, #1
	mov	r3, #200
	bl	process_int32
	.loc 1 135 0
	ldr	r3, .L54+48
	ldr	r3, [r3, #0]
	rsb	r2, r3, #0
	ldr	r3, .L54+48
	str	r2, [r3, #0]
	.loc 1 136 0
	b	.L35
.L31:
	.loc 1 139 0
	ldr	r3, .L54+52
	ldr	r3, [r3, #0]
	rsb	r2, r3, #0
	ldr	r3, .L54+52
	str	r2, [r3, #0]
	.loc 1 140 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L54+52
	mov	r2, #1
	mov	r3, #200
	bl	process_int32
	.loc 1 141 0
	ldr	r3, .L54+52
	ldr	r3, [r3, #0]
	rsb	r2, r3, #0
	ldr	r3, .L54+52
	str	r2, [r3, #0]
	.loc 1 142 0
	b	.L35
.L33:
	.loc 1 145 0
	ldr	r3, .L54+56
	ldr	r3, [r3, #0]
	rsb	r2, r3, #0
	ldr	r3, .L54+56
	str	r2, [r3, #0]
	.loc 1 146 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L54+56
	mov	r2, #1
	mov	r3, #200
	bl	process_int32
	.loc 1 147 0
	ldr	r3, .L54+56
	ldr	r3, [r3, #0]
	rsb	r2, r3, #0
	ldr	r3, .L54+56
	str	r2, [r3, #0]
	.loc 1 148 0
	b	.L35
.L21:
	.loc 1 151 0
	bl	bad_key_beep
.L35:
	.loc 1 153 0
	bl	Refresh_Screen
	.loc 1 154 0
	b	.L4
.L14:
	.loc 1 158 0
	ldr	r4, [fp, #-52]
	bl	SYSTEM_num_systems_in_use
	mov	r2, r0
	ldr	r3, .L54+60
	ldr	r1, .L54+64
	str	r1, [sp, #0]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	ldr	r3, .L54+68
	bl	GROUP_process_NEXT_and_PREV
	.loc 1 159 0
	b	.L4
.L13:
	.loc 1 162 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #11
	bhi	.L36
	mov	r2, #1
	mov	r3, r2, asl r3
	and	r2, r3, #3072
	cmp	r2, #0
	bne	.L39
	and	r2, r3, #1008
	cmp	r2, #0
	bne	.L38
	and	r3, r3, #14
	cmp	r3, #0
	beq	.L36
.L37:
	.loc 1 167 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L54+72
	str	r2, [r3, #0]
	.loc 1 169 0
	mov	r0, #0
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 170 0
	b	.L40
.L38:
	.loc 1 178 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #3
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 179 0
	b	.L40
.L39:
	.loc 1 183 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #2
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 184 0
	b	.L40
.L36:
	.loc 1 187 0
	bl	bad_key_beep
	.loc 1 189 0
	b	.L4
.L40:
	b	.L4
.L9:
	.loc 1 192 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L41
.L45:
	.word	.L42
	.word	.L43
	.word	.L43
	.word	.L43
	.word	.L43
	.word	.L43
	.word	.L43
	.word	.L41
	.word	.L44
	.word	.L44
.L42:
	.loc 1 195 0
	ldr	r3, .L54+72
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L46
	.loc 1 197 0
	ldr	r3, .L54+72
	mov	r2, #1
	str	r2, [r3, #0]
.L46:
	.loc 1 200 0
	ldr	r3, .L54+72
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 201 0
	b	.L47
.L43:
	.loc 1 209 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #3
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 210 0
	b	.L47
.L44:
	.loc 1 214 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #2
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 215 0
	b	.L47
.L41:
	.loc 1 218 0
	bl	bad_key_beep
	.loc 1 220 0
	b	.L4
.L47:
	b	.L4
.L10:
	.loc 1 223 0
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L53
.L49:
	.loc 1 226 0
	ldr	r0, .L54+76
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 227 0
	mov	r0, r0	@ nop
	.loc 1 232 0
	b	.L4
.L53:
	.loc 1 230 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 232 0
	b	.L4
.L12:
	.loc 1 235 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 236 0
	b	.L4
.L15:
	.loc 1 239 0
	ldr	r0, .L54+76
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 240 0
	b	.L4
.L8:
	.loc 1 243 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L4:
	.loc 1 246 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L55:
	.align	2
.L54:
	.word	GuiLib_CurStructureNdx
	.word	GuiVar_SystemFlowCheckingInUse
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_FLOW_CHECKING_show_dropdown
	.word	GuiVar_SystemFlowCheckingRange2
	.word	GuiVar_SystemFlowCheckingRange1
	.word	GuiVar_SystemFlowCheckingRange3
	.word	GuiVar_SystemFlowCheckingTolerancePlus1
	.word	GuiVar_SystemFlowCheckingTolerancePlus2
	.word	GuiVar_SystemFlowCheckingTolerancePlus3
	.word	GuiVar_SystemFlowCheckingTolerancePlus4
	.word	GuiVar_SystemFlowCheckingToleranceMinus1
	.word	GuiVar_SystemFlowCheckingToleranceMinus2
	.word	GuiVar_SystemFlowCheckingToleranceMinus3
	.word	GuiVar_SystemFlowCheckingToleranceMinus4
	.word	FLOW_CHECKING_extract_and_store_changes_from_GuiVars
	.word	FLOW_CHECKING_copy_group_into_guivars
	.word	SYSTEM_get_group_at_this_index
	.word	g_FLOW_CHECKING_prev_cursor_pos
	.word	FDTO_FLOW_CHECKING_return_to_menu
.LFE1:
	.size	FLOW_CHECKING_process_group, .-FLOW_CHECKING_process_group
	.section	.text.FDTO_FLOW_CHECKING_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_FLOW_CHECKING_return_to_menu, %function
FDTO_FLOW_CHECKING_return_to_menu:
.LFB2:
	.loc 1 253 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	.loc 1 254 0
	ldr	r3, .L57
	ldr	r0, .L57+4
	mov	r1, r3
	bl	FDTO_GROUP_return_to_menu
	.loc 1 255 0
	ldmfd	sp!, {fp, pc}
.L58:
	.align	2
.L57:
	.word	FLOW_CHECKING_extract_and_store_changes_from_GuiVars
	.word	g_FLOW_CHECKING_editing_group
.LFE2:
	.size	FDTO_FLOW_CHECKING_return_to_menu, .-FDTO_FLOW_CHECKING_return_to_menu
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_flow_checking.c\000"
	.section	.text.FDTO_FLOW_CHECKING_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_FLOW_CHECKING_draw_menu
	.type	FDTO_FLOW_CHECKING_draw_menu, %function
FDTO_FLOW_CHECKING_draw_menu:
.LFB3:
	.loc 1 259 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI7:
	add	fp, sp, #4
.LCFI8:
	sub	sp, sp, #16
.LCFI9:
	str	r0, [fp, #-8]
	.loc 1 260 0
	ldr	r3, .L60
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L60+4
	mov	r3, #260
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 262 0
	bl	SYSTEM_num_systems_in_use
	mov	r3, r0
	ldr	r2, .L60+8
	ldr	r1, .L60+12
	str	r1, [sp, #0]
	str	r2, [sp, #4]
	mov	r2, #0
	str	r2, [sp, #8]
	ldr	r0, [fp, #-8]
	ldr	r1, .L60+16
	mov	r2, r3
	mov	r3, #21
	bl	FDTO_GROUP_draw_menu
	.loc 1 264 0
	ldr	r3, .L60
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 265 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L61:
	.align	2
.L60:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	FLOW_CHECKING_copy_group_into_guivars
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	g_FLOW_CHECKING_editing_group
.LFE3:
	.size	FDTO_FLOW_CHECKING_draw_menu, .-FDTO_FLOW_CHECKING_draw_menu
	.section	.text.FLOW_CHECKING_process_menu,"ax",%progbits
	.align	2
	.global	FLOW_CHECKING_process_menu
	.type	FLOW_CHECKING_process_menu, %function
FLOW_CHECKING_process_menu:
.LFB4:
	.loc 1 269 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI10:
	add	fp, sp, #4
.LCFI11:
	sub	sp, sp, #24
.LCFI12:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 270 0
	ldr	r3, .L63
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L63+4
	ldr	r3, .L63+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 272 0
	bl	SYSTEM_num_systems_in_use
	mov	r3, r0
	ldr	r1, .L63+12
	ldr	r2, .L63+16
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	ldr	r1, .L63+20
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	ldr	r2, .L63+24
	bl	GROUP_process_menu
	.loc 1 274 0
	ldr	r3, .L63
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 275 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L64:
	.align	2
.L63:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	270
	.word	FLOW_CHECKING_process_group
	.word	FLOW_CHECKING_copy_group_into_guivars
	.word	SYSTEM_get_group_at_this_index
	.word	g_FLOW_CHECKING_editing_group
.LFE4:
	.size	FLOW_CHECKING_process_menu, .-FLOW_CHECKING_process_menu
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI10-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x4e5
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF59
	.byte	0x1
	.4byte	.LASF60
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8b
	.uleb128 0x6
	.4byte	0x92
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x35
	.4byte	0x92
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x4
	.byte	0x57
	.4byte	0x99
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x5
	.byte	0x4c
	.4byte	0xad
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0xd3
	.uleb128 0xa
	.4byte	0x92
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0xf8
	.uleb128 0xc
	.4byte	.LASF16
	.byte	0x6
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF17
	.byte	0x6
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x6
	.byte	0x82
	.4byte	0xd3
	.uleb128 0x9
	.4byte	0x53
	.4byte	0x113
	.uleb128 0xa
	.4byte	0x92
	.byte	0x2
	.byte	0
	.uleb128 0xb
	.byte	0x24
	.byte	0x7
	.byte	0x78
	.4byte	0x19a
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x7
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x7
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x7
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x7
	.byte	0x88
	.4byte	0x1ab
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x7
	.byte	0x8d
	.4byte	0x1bd
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x7
	.byte	0x92
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x7
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x7
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x7
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	0x1a6
	.uleb128 0xe
	.4byte	0x1a6
	.byte	0
	.uleb128 0xf
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x19a
	.uleb128 0xd
	.byte	0x1
	.4byte	0x1bd
	.uleb128 0xe
	.4byte	0xf8
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1b1
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x7
	.byte	0x9e
	.4byte	0x113
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF29
	.uleb128 0x9
	.4byte	0x53
	.4byte	0x1e5
	.uleb128 0xa
	.4byte	0x92
	.byte	0x3
	.byte	0
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x1
	.byte	0x37
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x11
	.4byte	.LASF61
	.byte	0x1
	.byte	0x3d
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x22e
	.uleb128 0x12
	.4byte	.LASF32
	.byte	0x1
	.byte	0x3d
	.4byte	0x22e
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x3f
	.4byte	0x1c3
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0xf
	.4byte	0xf8
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0xfc
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x102
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x271
	.uleb128 0x15
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x102
	.4byte	0x271
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.4byte	0x7a
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x10c
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x2a0
	.uleb128 0x15
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x10c
	.4byte	0x22e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x45c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF37
	.byte	0x8
	.2byte	0x45d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF38
	.byte	0x8
	.2byte	0x45e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF39
	.byte	0x8
	.2byte	0x45f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF40
	.byte	0x8
	.2byte	0x460
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF41
	.byte	0x8
	.2byte	0x461
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF42
	.byte	0x8
	.2byte	0x462
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF43
	.byte	0x8
	.2byte	0x463
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF44
	.byte	0x8
	.2byte	0x464
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF45
	.byte	0x8
	.2byte	0x465
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF46
	.byte	0x8
	.2byte	0x466
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF47
	.byte	0x8
	.2byte	0x467
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF48
	.byte	0x9
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF49
	.byte	0x9
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF50
	.byte	0xa
	.byte	0x30
	.4byte	0x375
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x17
	.4byte	.LASF51
	.byte	0xa
	.byte	0x34
	.4byte	0x38b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x17
	.4byte	.LASF52
	.byte	0xa
	.byte	0x36
	.4byte	0x3a1
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x17
	.4byte	.LASF53
	.byte	0xa
	.byte	0x38
	.4byte	0x3b7
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x18
	.4byte	.LASF54
	.byte	0xb
	.byte	0xc6
	.4byte	0xb8
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF55
	.byte	0xc
	.byte	0x33
	.4byte	0x3da
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xf
	.4byte	0x103
	.uleb128 0x17
	.4byte	.LASF56
	.byte	0xc
	.byte	0x3f
	.4byte	0x3f0
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xf
	.4byte	0x1d5
	.uleb128 0x17
	.4byte	.LASF57
	.byte	0x1
	.byte	0x2a
	.4byte	0x7a
	.byte	0x5
	.byte	0x3
	.4byte	g_FLOW_CHECKING_editing_group
	.uleb128 0x17
	.4byte	.LASF58
	.byte	0x1
	.byte	0x2c
	.4byte	0x53
	.byte	0x5
	.byte	0x3
	.4byte	g_FLOW_CHECKING_prev_cursor_pos
	.uleb128 0x16
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x45c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF37
	.byte	0x8
	.2byte	0x45d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF38
	.byte	0x8
	.2byte	0x45e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF39
	.byte	0x8
	.2byte	0x45f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF40
	.byte	0x8
	.2byte	0x460
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF41
	.byte	0x8
	.2byte	0x461
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF42
	.byte	0x8
	.2byte	0x462
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF43
	.byte	0x8
	.2byte	0x463
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF44
	.byte	0x8
	.2byte	0x464
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF45
	.byte	0x8
	.2byte	0x465
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF46
	.byte	0x8
	.2byte	0x466
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF47
	.byte	0x8
	.2byte	0x467
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF48
	.byte	0x9
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF49
	.byte	0x9
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF54
	.byte	0xb
	.byte	0xc6
	.4byte	0xb8
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI8
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI11
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF59:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF46:
	.ascii	"GuiVar_SystemFlowCheckingTolerancePlus3\000"
.LASF58:
	.ascii	"g_FLOW_CHECKING_prev_cursor_pos\000"
.LASF4:
	.ascii	"short int\000"
.LASF13:
	.ascii	"portTickType\000"
.LASF19:
	.ascii	"_01_command\000"
.LASF55:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF39:
	.ascii	"GuiVar_SystemFlowCheckingRange3\000"
.LASF36:
	.ascii	"GuiVar_SystemFlowCheckingInUse\000"
.LASF16:
	.ascii	"keycode\000"
.LASF43:
	.ascii	"GuiVar_SystemFlowCheckingToleranceMinus4\000"
.LASF20:
	.ascii	"_02_menu\000"
.LASF35:
	.ascii	"FLOW_CHECKING_process_menu\000"
.LASF2:
	.ascii	"signed char\000"
.LASF29:
	.ascii	"float\000"
.LASF9:
	.ascii	"long long int\000"
.LASF52:
	.ascii	"GuiFont_DecimalChar\000"
.LASF12:
	.ascii	"long int\000"
.LASF15:
	.ascii	"xSemaphoreHandle\000"
.LASF14:
	.ascii	"xQueueHandle\000"
.LASF57:
	.ascii	"g_FLOW_CHECKING_editing_group\000"
.LASF25:
	.ascii	"_06_u32_argument1\000"
.LASF23:
	.ascii	"key_process_func_ptr\000"
.LASF27:
	.ascii	"_08_screen_to_draw\000"
.LASF33:
	.ascii	"pcomplete_redraw\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF40:
	.ascii	"GuiVar_SystemFlowCheckingToleranceMinus1\000"
.LASF41:
	.ascii	"GuiVar_SystemFlowCheckingToleranceMinus2\000"
.LASF42:
	.ascii	"GuiVar_SystemFlowCheckingToleranceMinus3\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF51:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF44:
	.ascii	"GuiVar_SystemFlowCheckingTolerancePlus1\000"
.LASF45:
	.ascii	"GuiVar_SystemFlowCheckingTolerancePlus2\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF47:
	.ascii	"GuiVar_SystemFlowCheckingTolerancePlus4\000"
.LASF31:
	.ascii	"FDTO_FLOW_CHECKING_return_to_menu\000"
.LASF30:
	.ascii	"FDTO_FLOW_CHECKING_show_dropdown\000"
.LASF60:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_flow_checking.c\000"
.LASF21:
	.ascii	"_03_structure_to_draw\000"
.LASF48:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF0:
	.ascii	"char\000"
.LASF53:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF22:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF26:
	.ascii	"_07_u32_argument2\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF56:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF24:
	.ascii	"_04_func_ptr\000"
.LASF18:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF49:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF17:
	.ascii	"repeats\000"
.LASF50:
	.ascii	"GuiFont_LanguageActive\000"
.LASF54:
	.ascii	"list_system_recursive_MUTEX\000"
.LASF32:
	.ascii	"pkey_event\000"
.LASF34:
	.ascii	"FDTO_FLOW_CHECKING_draw_menu\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF28:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF37:
	.ascii	"GuiVar_SystemFlowCheckingRange1\000"
.LASF38:
	.ascii	"GuiVar_SystemFlowCheckingRange2\000"
.LASF61:
	.ascii	"FLOW_CHECKING_process_group\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
