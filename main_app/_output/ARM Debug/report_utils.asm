	.file	"report_utils.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_REPORT_active_line,"aw",%nobits
	.align	2
	.type	g_REPORT_active_line, %object
	.size	g_REPORT_active_line, 4
g_REPORT_active_line:
	.space	4
	.section	.bss.g_REPORT_top_line,"aw",%nobits
	.align	2
	.type	g_REPORT_top_line, %object
	.size	g_REPORT_top_line, 4
g_REPORT_top_line:
	.space	4
	.section	.text.FDTO_REPORTS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REPORTS_draw_report
	.type	FDTO_REPORTS_draw_report, %function
FDTO_REPORTS_draw_report:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/report_utils.c"
	.loc 1 33 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #16
.LCFI2:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 34 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	.loc 1 36 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L2
	.loc 1 38 0
	ldr	r3, .L5
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 40 0
	ldr	r3, .L5+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 42 0
	ldr	r3, .L5+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 44 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L3
	.loc 1 46 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	ldr	r1, [fp, #-16]
	mov	r2, r3
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init
	b	.L4
.L3:
	.loc 1 50 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L5+8
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	ldr	r1, [fp, #-16]
	bl	GuiLib_ScrollBox_Init
	b	.L4
.L2:
	.loc 1 55 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L5+4
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r1, .L5+8
	ldr	r1, [r1, #0]
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r1, r1, asl #16
	mov	r1, r1, asr #16
	str	r1, [sp, #0]
	mov	r0, #0
	ldr	r1, [fp, #-16]
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L4:
	.loc 1 58 0
	bl	GuiLib_Refresh
	.loc 1 59 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	g_REPORT_active_line
	.word	g_REPORT_top_line
.LFE0:
	.size	FDTO_REPORTS_draw_report, .-FDTO_REPORTS_draw_report
	.section	.text.REPORTS_process_report,"ax",%progbits
	.align	2
	.global	REPORTS_process_report
	.type	REPORTS_process_report, %function
REPORTS_process_report:
.LFB1:
	.loc 1 63 0
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #52
.LCFI5:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	str	r2, [fp, #-52]
	str	r3, [fp, #-56]
	.loc 1 66 0
	ldr	r3, [fp, #-48]
	cmp	r3, #3
	beq	.L10
	cmp	r3, #3
	bhi	.L12
	cmp	r3, #0
	beq	.L9
	cmp	r3, #1
	beq	.L10
	b	.L8
.L12:
	cmp	r3, #4
	beq	.L9
	cmp	r3, #67
	beq	.L11
	b	.L8
.L9:
	.loc 1 70 0
	ldr	r3, [fp, #-48]
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	.loc 1 71 0
	b	.L7
.L10:
	.loc 1 75 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L16
	str	r2, [r3, #0]
	.loc 1 77 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_GetTopLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L16+4
	str	r2, [r3, #0]
	.loc 1 79 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 81 0
	ldr	r3, [fp, #-48]
	cmp	r3, #3
	bne	.L14
	.loc 1 83 0
	ldr	r3, .L16+8
	str	r3, [fp, #-20]
	b	.L15
.L14:
	.loc 1 87 0
	ldr	r3, .L16+12
	str	r3, [fp, #-20]
.L15:
	.loc 1 90 0
	ldr	r3, [fp, #-56]
	str	r3, [fp, #-16]
	.loc 1 91 0
	ldr	r3, [fp, #-52]
	str	r3, [fp, #-12]
	.loc 1 92 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 93 0
	b	.L7
.L11:
	.loc 1 96 0
	ldr	r3, .L16+16
	ldr	r2, [r3, #0]
	ldr	r0, .L16+20
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L16+24
	str	r2, [r3, #0]
.L8:
	.loc 1 101 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L7:
	.loc 1 103 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	g_REPORT_active_line
	.word	g_REPORT_top_line
	.word	FDTO_scroll_right
	.word	FDTO_scroll_left
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	REPORTS_process_report, .-REPORTS_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x345
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF44
	.byte	0x1
	.4byte	.LASF45
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8b
	.uleb128 0x6
	.4byte	0x92
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xb0
	.uleb128 0x9
	.4byte	0x92
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd5
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x82
	.4byte	0xb0
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x167
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x88
	.4byte	0x178
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x8d
	.4byte	0x18a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x92
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x173
	.uleb128 0xd
	.4byte	0x173
	.byte	0
	.uleb128 0xe
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x167
	.uleb128 0xc
	.byte	0x1
	.4byte	0x18a
	.uleb128 0xd
	.4byte	0xd5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x17e
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9e
	.4byte	0xe0
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.byte	0x20
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1df
	.uleb128 0x10
	.4byte	.LASF26
	.byte	0x1
	.byte	0x20
	.4byte	0x1df
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LASF27
	.byte	0x1
	.byte	0x20
	.4byte	0x1e4
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x10
	.4byte	.LASF28
	.byte	0x1
	.byte	0x20
	.4byte	0x1f5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xe
	.4byte	0x7a
	.uleb128 0xe
	.4byte	0x53
	.uleb128 0xc
	.byte	0x1
	.4byte	0x1f5
	.uleb128 0xd
	.4byte	0x41
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1e9
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.byte	0x3e
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x24d
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0x3e
	.4byte	0x24d
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x10
	.4byte	.LASF32
	.byte	0x1
	.byte	0x3e
	.4byte	0x1e4
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x10
	.4byte	.LASF33
	.byte	0x1
	.byte	0x3e
	.4byte	0x1e4
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x11
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x40
	.4byte	0x190
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xe
	.4byte	0xd5
	.uleb128 0x12
	.4byte	.LASF34
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF35
	.byte	0x5
	.2byte	0x3ca
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF36
	.byte	0x6
	.byte	0x30
	.4byte	0x27f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x13
	.4byte	.LASF37
	.byte	0x6
	.byte	0x34
	.4byte	0x295
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x13
	.4byte	.LASF38
	.byte	0x6
	.byte	0x36
	.4byte	0x2ab
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x13
	.4byte	.LASF39
	.byte	0x6
	.byte	0x38
	.4byte	0x2c1
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x8
	.4byte	0x190
	.4byte	0x2d6
	.uleb128 0x9
	.4byte	0x92
	.byte	0x31
	.byte	0
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x4
	.byte	0xac
	.4byte	0x2c6
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x4
	.byte	0xae
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF42
	.byte	0x1
	.byte	0x18
	.4byte	0x53
	.byte	0x5
	.byte	0x3
	.4byte	g_REPORT_active_line
	.uleb128 0x13
	.4byte	.LASF43
	.byte	0x1
	.byte	0x1a
	.4byte	0x53
	.byte	0x5
	.byte	0x3
	.4byte	g_REPORT_top_line
	.uleb128 0x12
	.4byte	.LASF34
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF35
	.byte	0x5
	.2byte	0x3ca
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x4
	.byte	0xac
	.4byte	0x2c6
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x4
	.byte	0xae
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF44:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF17:
	.ascii	"_02_menu\000"
.LASF4:
	.ascii	"short int\000"
.LASF28:
	.ascii	"DataFuncPtr\000"
.LASF16:
	.ascii	"_01_command\000"
.LASF34:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF35:
	.ascii	"GuiVar_ScrollBoxHorizScrollPos\000"
.LASF29:
	.ascii	"FDTO_REPORTS_draw_report\000"
.LASF27:
	.ascii	"pline_count\000"
.LASF13:
	.ascii	"keycode\000"
.LASF15:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF41:
	.ascii	"screen_history_index\000"
.LASF9:
	.ascii	"long long int\000"
.LASF38:
	.ascii	"GuiFont_DecimalChar\000"
.LASF33:
	.ascii	"pmax_pixels_to_scroll\000"
.LASF12:
	.ascii	"long int\000"
.LASF45:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/r"
	.ascii	"eport_utils.c\000"
.LASF22:
	.ascii	"_06_u32_argument1\000"
.LASF42:
	.ascii	"g_REPORT_active_line\000"
.LASF20:
	.ascii	"key_process_func_ptr\000"
.LASF24:
	.ascii	"_08_screen_to_draw\000"
.LASF26:
	.ascii	"pcomplete_redraw\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF2:
	.ascii	"signed char\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF37:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF32:
	.ascii	"pamount_to_scroll\000"
.LASF43:
	.ascii	"g_REPORT_top_line\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF0:
	.ascii	"char\000"
.LASF39:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF19:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF23:
	.ascii	"_07_u32_argument2\000"
.LASF30:
	.ascii	"REPORTS_process_report\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF21:
	.ascii	"_04_func_ptr\000"
.LASF18:
	.ascii	"_03_structure_to_draw\000"
.LASF14:
	.ascii	"repeats\000"
.LASF36:
	.ascii	"GuiFont_LanguageActive\000"
.LASF31:
	.ascii	"pkey_event\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF25:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF40:
	.ascii	"ScreenHistory\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
