	.file	"lpc3250_otg_i2c.c"
	.text
.Ltext0:
	.section	.text.otg_i2c_init,"ax",%progbits
	.align	2
	.global	otg_i2c_init
	.type	otg_i2c_init, %function
otg_i2c_init:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc3250_otg_i2c.c"
	.loc 1 6 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	.loc 1 7 0
	ldr	r3, .L3
	ldr	r2, .L3
	ldr	r2, [r2, #0]
	orr	r2, r2, #30
	str	r2, [r3, #0]
	.loc 1 8 0
	mov	r0, r0	@ nop
.L2:
	.loc 1 8 0 is_stmt 0 discriminator 1
	ldr	r3, .L3+4
	ldr	r3, [r3, #0]
	and	r3, r3, #30
	cmp	r3, #30
	bne	.L2
	.loc 1 11 0 is_stmt 1
	ldr	r3, .L3+8
	ldr	r2, .L3+8
	ldr	r2, [r2, #0]
	orr	r2, r2, #256
	str	r2, [r3, #0]
	.loc 1 13 0
	ldr	r3, .L3+12
	mov	r2, #65
	str	r2, [r3, #0]
	.loc 1 14 0
	ldr	r3, .L3+16
	mov	r2, #65
	str	r2, [r3, #0]
	.loc 1 16 0
	ldr	r3, .L3+8
	mov	r2, #256
	str	r2, [r3, #0]
	.loc 1 17 0
	mov	r3, #0
	.loc 1 18 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L4:
	.align	2
.L3:
	.word	822218740
	.word	822218744
	.word	822215432
	.word	822215440
	.word	822215436
.LFE0:
	.size	otg_i2c_init, .-otg_i2c_init
	.section	.text.otg_i2c_start,"ax",%progbits
	.align	2
	.global	otg_i2c_start
	.type	otg_i2c_start, %function
otg_i2c_start:
.LFB1:
	.loc 1 21 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI2:
	add	fp, sp, #0
.LCFI3:
	.loc 1 23 0
	mov	r3, #0
	.loc 1 24 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE1:
	.size	otg_i2c_start, .-otg_i2c_start
	.section	.text.otg_i2c_stop,"ax",%progbits
	.align	2
	.global	otg_i2c_stop
	.type	otg_i2c_stop, %function
otg_i2c_stop:
.LFB2:
	.loc 1 27 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI4:
	add	fp, sp, #0
.LCFI5:
	.loc 1 28 0
	ldr	r3, .L8
	ldr	r2, .L8
	ldr	r2, [r2, #0]
	bic	r2, r2, #12
	str	r2, [r3, #0]
	.loc 1 29 0
	mov	r0, r0	@ nop
.L7:
	.loc 1 29 0 is_stmt 0 discriminator 1
	ldr	r3, .L8+4
	ldr	r3, [r3, #0]
	and	r3, r3, #28
	cmp	r3, #12
	bne	.L7
	.loc 1 31 0 is_stmt 1
	mov	r3, #0
	.loc 1 32 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L9:
	.align	2
.L8:
	.word	822218740
	.word	822218744
.LFE2:
	.size	otg_i2c_stop, .-otg_i2c_stop
	.section	.text.otg_i2c_delete,"ax",%progbits
	.align	2
	.global	otg_i2c_delete
	.type	otg_i2c_delete, %function
otg_i2c_delete:
.LFB3:
	.loc 1 35 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	.loc 1 37 0
	mov	r3, #0
	.loc 1 38 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE3:
	.size	otg_i2c_delete, .-otg_i2c_delete
	.section	.text.otg_i2c_read,"ax",%progbits
	.align	2
	.global	otg_i2c_read
	.type	otg_i2c_read, %function
otg_i2c_read:
.LFB4:
	.loc 1 41 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI8:
	add	fp, sp, #0
.LCFI9:
	sub	sp, sp, #8
.LCFI10:
	mov	r3, r0
	strb	r3, [fp, #-8]
	.loc 1 44 0
	ldr	r3, .L18
	mov	r2, #344
	str	r2, [r3, #0]
	.loc 1 45 0
	mov	r0, r0	@ nop
.L12:
	.loc 1 45 0 is_stmt 0 discriminator 1
	ldr	r3, .L18+4
	ldr	r3, [r3, #0]
	and	r3, r3, #2048
	cmp	r3, #0
	beq	.L12
	.loc 1 47 0 is_stmt 1
	ldr	r3, .L18
	ldrb	r2, [fp, #-8]	@ zero_extendqisi2
	str	r2, [r3, #0]
	.loc 1 48 0
	mov	r0, r0	@ nop
.L13:
	.loc 1 48 0 is_stmt 0 discriminator 1
	ldr	r3, .L18+4
	ldr	r3, [r3, #0]
	and	r3, r3, #2048
	cmp	r3, #0
	beq	.L13
	.loc 1 50 0 is_stmt 1
	ldr	r3, .L18
	ldr	r2, .L18+8
	str	r2, [r3, #0]
	.loc 1 51 0
	mov	r0, r0	@ nop
.L14:
	.loc 1 51 0 is_stmt 0 discriminator 1
	ldr	r3, .L18+4
	ldr	r3, [r3, #0]
	and	r3, r3, #2048
	cmp	r3, #0
	beq	.L14
	.loc 1 53 0 is_stmt 1
	ldr	r3, .L18
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 54 0
	mov	r0, r0	@ nop
.L15:
	.loc 1 54 0 is_stmt 0 discriminator 1
	ldr	r3, .L18+4
	ldr	r3, [r3, #0]
	and	r3, r3, #2048
	cmp	r3, #0
	beq	.L15
	.loc 1 57 0 is_stmt 1
	ldr	r3, .L18
	mov	r2, #512
	str	r2, [r3, #0]
	.loc 1 58 0
	mov	r0, r0	@ nop
.L16:
	.loc 1 58 0 is_stmt 0 discriminator 1
	ldr	r3, .L18+4
	ldr	r3, [r3, #0]
	and	r3, r3, #2048
	cmp	r3, #0
	beq	.L16
	.loc 1 61 0 is_stmt 1
	mov	r0, r0	@ nop
.L17:
	.loc 1 61 0 is_stmt 0 discriminator 1
	ldr	r3, .L18+4
	ldr	r3, [r3, #0]
	and	r3, r3, #512
	cmp	r3, #0
	bne	.L17
	.loc 1 63 0 is_stmt 1
	ldr	r3, .L18
	ldr	r3, [r3, #0]
	.loc 1 64 0
	ldr	r3, .L18
	ldr	r3, [r3, #0]
	strb	r3, [fp, #-1]
	.loc 1 65 0
	ldrb	r3, [fp, #-1]	@ zero_extendqisi2
	.loc 1 66 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L19:
	.align	2
.L18:
	.word	822215424
	.word	822215428
	.word	345
.LFE4:
	.size	otg_i2c_read, .-otg_i2c_read
	.section	.text.otg_i2c_write,"ax",%progbits
	.align	2
	.global	otg_i2c_write
	.type	otg_i2c_write, %function
otg_i2c_write:
.LFB5:
	.loc 1 69 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI11:
	add	fp, sp, #0
.LCFI12:
	sub	sp, sp, #8
.LCFI13:
	mov	r2, r0
	mov	r3, r1
	strb	r2, [fp, #-4]
	strb	r3, [fp, #-8]
	.loc 1 70 0
	ldr	r3, .L24
	mov	r2, #344
	str	r2, [r3, #0]
	.loc 1 71 0
	mov	r0, r0	@ nop
.L21:
	.loc 1 71 0 is_stmt 0 discriminator 1
	ldr	r3, .L24+4
	ldr	r3, [r3, #0]
	and	r3, r3, #2048
	cmp	r3, #0
	beq	.L21
	.loc 1 73 0 is_stmt 1
	ldr	r3, .L24
	ldrb	r2, [fp, #-4]	@ zero_extendqisi2
	str	r2, [r3, #0]
	.loc 1 74 0
	mov	r0, r0	@ nop
.L22:
	.loc 1 74 0 is_stmt 0 discriminator 1
	ldr	r3, .L24+4
	ldr	r3, [r3, #0]
	and	r3, r3, #2048
	cmp	r3, #0
	beq	.L22
	.loc 1 76 0 is_stmt 1
	ldr	r3, .L24
	ldrb	r2, [fp, #-8]	@ zero_extendqisi2
	orr	r2, r2, #512
	str	r2, [r3, #0]
	.loc 1 77 0
	mov	r0, r0	@ nop
.L23:
	.loc 1 77 0 is_stmt 0 discriminator 1
	ldr	r3, .L24+4
	ldr	r3, [r3, #0]
	and	r3, r3, #2048
	cmp	r3, #0
	beq	.L23
	.loc 1 79 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L25:
	.align	2
.L24:
	.word	822215424
	.word	822215428
.LFE5:
	.size	otg_i2c_write, .-otg_i2c_write
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI6-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI8-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI11-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/usb-device/portable/hcc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/usb-device/portable/os_common.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x16e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF20
	.byte	0x1
	.4byte	.LASF21
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x2
	.byte	0x24
	.4byte	0x30
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.4byte	.LASF1
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.4byte	.LASF2
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x2
	.byte	0x28
	.4byte	0x57
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x26
	.4byte	0xa2
	.uleb128 0x6
	.4byte	.LASF11
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF12
	.sleb128 1
	.uleb128 0x6
	.4byte	.LASF13
	.sleb128 2
	.uleb128 0x6
	.4byte	.LASF14
	.sleb128 3
	.byte	0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.byte	0x5
	.byte	0x1
	.4byte	0x6c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.byte	0x14
	.byte	0x1
	.4byte	0x6c
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.byte	0x1a
	.byte	0x1
	.4byte	0x6c
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.byte	0x22
	.byte	0x1
	.4byte	0x6c
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.byte	0x28
	.byte	0x1
	.4byte	0x25
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x13f
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x1
	.byte	0x28
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xa
	.ascii	"rc\000"
	.byte	0x1
	.byte	0x2a
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -5
	.byte	0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.byte	0x44
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x1
	.byte	0x44
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xc
	.ascii	"val\000"
	.byte	0x1
	.byte	0x44
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI5
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI9
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI12
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF9:
	.ascii	"long long int\000"
.LASF12:
	.ascii	"OS_ERR_MUTEX\000"
.LASF4:
	.ascii	"hcc_u8\000"
.LASF20:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF21:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc3250_otg_i2c.c\000"
.LASF18:
	.ascii	"otg_i2c_delete\000"
.LASF17:
	.ascii	"otg_i2c_stop\000"
.LASF19:
	.ascii	"addr\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF0:
	.ascii	"unsigned char\000"
.LASF14:
	.ascii	"OS_ERR\000"
.LASF16:
	.ascii	"otg_i2c_start\000"
.LASF7:
	.ascii	"long int\000"
.LASF11:
	.ascii	"OS_SUCCESS\000"
.LASF13:
	.ascii	"OS_ERR_EVENT\000"
.LASF2:
	.ascii	"short unsigned int\000"
.LASF1:
	.ascii	"signed char\000"
.LASF15:
	.ascii	"otg_i2c_init\000"
.LASF22:
	.ascii	"otg_i2c_read\000"
.LASF23:
	.ascii	"otg_i2c_write\000"
.LASF3:
	.ascii	"short int\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF5:
	.ascii	"hcc_u32\000"
.LASF6:
	.ascii	"long unsigned int\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
