	.file	"lpc32xx_ssp_driver.c"
	.text
.Ltext0:
	.section	.bss.sspdrv,"aw",%nobits
	.align	2
	.type	sspdrv, %object
	.size	sspdrv, 48
sspdrv:
	.space	48
	.section	.rodata.sspclks,"a",%progbits
	.align	2
	.type	sspclks, %object
	.size	sspclks, 8
sspclks:
	.word	3
	.word	2
	.global	__udivsi3
	.section	.text.ssp_set_clock,"ax",%progbits
	.align	2
	.type	ssp_set_clock, %function
ssp_set_clock:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_ssp_driver.c"
	.loc 1 78 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #28
.LCFI2:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	.loc 1 83 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #16]
	ldr	r2, .L4
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	clkpwr_get_clock_rate
	str	r0, [fp, #-20]
	.loc 1 88 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 89 0
	mvn	r3, #0
	str	r3, [fp, #-16]
	.loc 1 90 0
	mov	r3, #2
	str	r3, [fp, #-8]
	.loc 1 91 0
	b	.L2
.L3:
	.loc 1 93 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	ldr	r2, [fp, #-8]
	mul	r3, r2, r3
	ldr	r0, [fp, #-20]
	mov	r1, r3
	bl	__udivsi3
	mov	r3, r0
	str	r3, [fp, #-16]
	.loc 1 94 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bls	.L2
	.loc 1 96 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 97 0
	ldr	r3, [fp, #-12]
	cmp	r3, #255
	bls	.L2
	.loc 1 99 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 100 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #2
	str	r3, [fp, #-8]
.L2:
	.loc 1 91 0 discriminator 1
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bhi	.L3
	.loc 1 106 0
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #0]
	bic	r3, r3, #65280
	str	r3, [r2, #0]
	str	r3, [fp, #-24]
	.loc 1 107 0
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	mov	r3, r3, asl #8
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	ldr	r1, [fp, #-24]
	orr	r3, r3, r1
	str	r3, [r2, #0]
	.loc 1 108 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #16]
	.loc 1 110 0
	mov	r3, #0
	.loc 1 111 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	sspclks
.LFE0:
	.size	ssp_set_clock, .-ssp_set_clock
	.section	.text.ssp_configure,"ax",%progbits
	.align	2
	.type	ssp_configure, %function
ssp_configure:
.LFB1:
	.loc 1 136 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #24
.LCFI5:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	.loc 1 138 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 139 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-20]
	.loc 1 142 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 143 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bls	.L7
	.loc 1 143 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r3, #16
	bhi	.L7
	.loc 1 145 0 is_stmt 1
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	and	r3, r3, #15
	str	r3, [fp, #-8]
	b	.L8
.L7:
	.loc 1 149 0
	mvn	r3, #0
	str	r3, [fp, #-16]
.L8:
	.loc 1 151 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r3, #8
	bhi	.L9
	.loc 1 153 0
	ldr	r3, [fp, #-28]
	mov	r2, #1
	str	r2, [r3, #20]
	b	.L10
.L9:
	.loc 1 157 0
	ldr	r3, [fp, #-28]
	mov	r2, #2
	str	r2, [r3, #20]
.L10:
	.loc 1 161 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	bic	r3, r3, #48
	cmp	r3, #0
	bne	.L11
	.loc 1 162 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	.loc 1 161 0 discriminator 1
	cmp	r3, #48
	bne	.L12
.L11:
	.loc 1 164 0
	mvn	r3, #0
	str	r3, [fp, #-16]
.L12:
	.loc 1 166 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 169 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	cmp	r3, #1
	bne	.L13
	.loc 1 171 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #64
	str	r3, [fp, #-8]
.L13:
	.loc 1 173 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #12]
	cmp	r3, #1
	bne	.L14
	.loc 1 175 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #128
	str	r3, [fp, #-8]
.L14:
	.loc 1 179 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 180 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #20]
	cmp	r3, #0
	bne	.L15
	.loc 1 182 0
	mov	r3, #4
	str	r3, [fp, #-12]
.L15:
	.loc 1 186 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L16
	.loc 1 188 0
	ldr	r3, [fp, #-20]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 189 0
	ldr	r3, [fp, #-20]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #4]
	.loc 1 190 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #16]
	ldr	r0, [fp, #-28]
	mov	r1, r3
	bl	ssp_set_clock
	str	r0, [fp, #-16]
.L16:
	.loc 1 193 0
	ldr	r3, [fp, #-16]
	.loc 1 194 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	ssp_configure, .-ssp_configure
	.section	.text.ssp_standard_interrupt,"ax",%progbits
	.align	2
	.type	ssp_standard_interrupt, %function
ssp_standard_interrupt:
.LFB2:
	.loc 1 218 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-12]
	.loc 1 219 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-8]
	.loc 1 222 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	and	r3, r3, #7
	cmp	r3, #0
	beq	.L18
	.loc 1 225 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	cmp	r3, #0
	bne	.L19
	.loc 1 228 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #20]
	bic	r2, r3, #6
	ldr	r3, [fp, #-8]
	str	r2, [r3, #20]
	b	.L18
.L19:
	.loc 1 233 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #12]
	blx	r3
	.loc 1 234 0
	ldr	r3, [fp, #-8]
	mov	r2, #3
	str	r2, [r3, #32]
.L18:
	.loc 1 239 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	and	r3, r3, #8
	cmp	r3, #0
	beq	.L17
	.loc 1 241 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	bne	.L21
	.loc 1 244 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #20]
	bic	r2, r3, #8
	ldr	r3, [fp, #-8]
	str	r2, [r3, #20]
	b	.L17
.L21:
	.loc 1 249 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #8]
	blx	r3
.L17:
	.loc 1 252 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE2:
	.size	ssp_standard_interrupt, .-ssp_standard_interrupt
	.section	.text.ssp_open,"ax",%progbits
	.align	2
	.global	ssp_open
	.type	ssp_open, %function
ssp_open:
.LFB3:
	.loc 1 280 0
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #52
.LCFI11:
	str	r0, [fp, #-52]
	str	r1, [fp, #-56]
	.loc 1 283 0
	ldr	r3, [fp, #-52]
	str	r3, [fp, #-20]
	.loc 1 284 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 285 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 288 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L32
	cmp	r2, r3
	bne	.L23
	.loc 1 290 0
	ldr	r3, .L32+4
	str	r3, [fp, #-12]
	.loc 1 291 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #16]
	b	.L24
.L23:
	.loc 1 293 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L32+8
	cmp	r2, r3
	bne	.L24
	.loc 1 295 0
	ldr	r3, .L32+12
	str	r3, [fp, #-12]
	.loc 1 296 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #16]
.L24:
	.loc 1 299 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L25
	.loc 1 301 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L25
	.loc 1 304 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-52]
	str	r2, [r3, #4]
	.loc 1 307 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #16]
	ldr	r2, .L32+16
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 310 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 311 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 314 0
	ldr	r3, [fp, #-56]
	cmp	r3, #0
	bne	.L26
	.loc 1 317 0
	mov	r3, #8
	str	r3, [fp, #-44]
	.loc 1 318 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 1 319 0
	mov	r3, #1
	str	r3, [fp, #-36]
	.loc 1 320 0
	mov	r3, #0
	str	r3, [fp, #-32]
	.loc 1 321 0
	ldr	r3, .L32+20
	str	r3, [fp, #-28]
	.loc 1 322 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 323 0
	sub	r3, fp, #44
	str	r3, [fp, #-8]
	b	.L27
.L26:
	.loc 1 327 0
	ldr	r3, [fp, #-56]
	str	r3, [fp, #-8]
.L27:
	.loc 1 329 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	ssp_configure
	mov	r3, r0
	cmn	r3, #1
	beq	.L28
	.loc 1 332 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 333 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-16]
	.loc 1 341 0
	b	.L30
.L28:
	.loc 1 337 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #16]
	ldr	r2, .L32+16
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	bl	clkpwr_clk_en_dis
	.loc 1 341 0
	b	.L30
.L31:
	.loc 1 343 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #8]
	str	r3, [fp, #-48]
.L30:
	.loc 1 341 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #12]
	and	r3, r3, #4
	cmp	r3, #0
	bne	.L31
	.loc 1 347 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	mov	r2, #3
	str	r2, [r3, #32]
	.loc 1 350 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	mov	r2, #7
	str	r2, [r3, #20]
.L25:
	.loc 1 355 0
	ldr	r3, [fp, #-16]
	.loc 1 356 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L33:
	.align	2
.L32:
	.word	537411584
	.word	sspdrv
	.word	537444352
	.word	sspdrv+24
	.word	sspclks
	.word	1000000
.LFE3:
	.size	ssp_open, .-ssp_open
	.section	.text.ssp_close,"ax",%progbits
	.align	2
	.global	ssp_close
	.type	ssp_close, %function
ssp_close:
.LFB4:
	.loc 1 378 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #12
.LCFI14:
	str	r0, [fp, #-16]
	.loc 1 379 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-12]
	.loc 1 380 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	.loc 1 382 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L35
	.loc 1 385 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 386 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 389 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #4]
	ldr	r2, [r2, #4]
	bic	r2, r2, #2
	str	r2, [r3, #4]
	.loc 1 392 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #16]
	ldr	r2, .L36
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	bl	clkpwr_clk_en_dis
.L35:
	.loc 1 395 0
	ldr	r3, [fp, #-8]
	.loc 1 396 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L37:
	.align	2
.L36:
	.word	sspclks
.LFE4:
	.size	ssp_close, .-ssp_close
	.section	.text.ssp_ioctl,"ax",%progbits
	.align	2
	.global	ssp_ioctl
	.type	ssp_ioctl, %function
ssp_ioctl:
.LFB5:
	.loc 1 423 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #40
.LCFI17:
	str	r0, [fp, #-36]
	str	r1, [fp, #-40]
	str	r2, [fp, #-44]
	.loc 1 427 0
	ldr	r3, [fp, #-36]
	str	r3, [fp, #-16]
	.loc 1 428 0
	mvn	r3, #0
	str	r3, [fp, #-12]
	.loc 1 430 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L39
	.loc 1 432 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 433 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-20]
	.loc 1 435 0
	ldr	r3, [fp, #-40]
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L40
.L48:
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
.L41:
	.loc 1 438 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L49
	.loc 1 441 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	orr	r2, r3, #2
	ldr	r3, [fp, #-20]
	str	r2, [r3, #4]
	.loc 1 448 0
	b	.L39
.L49:
	.loc 1 446 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	bic	r2, r3, #2
	ldr	r3, [fp, #-20]
	str	r2, [r3, #4]
	.loc 1 448 0
	b	.L39
.L42:
	.loc 1 451 0
	ldr	r3, [fp, #-44]
	mov	r0, r3
	ldr	r1, [fp, #-16]
	bl	ssp_configure
	str	r0, [fp, #-12]
	.loc 1 453 0
	b	.L39
.L43:
	.loc 1 457 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L51
	.loc 1 460 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	orr	r2, r3, #1
	ldr	r3, [fp, #-20]
	str	r2, [r3, #4]
	.loc 1 467 0
	b	.L39
.L51:
	.loc 1 465 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	bic	r2, r3, #1
	ldr	r3, [fp, #-20]
	str	r2, [r3, #4]
	.loc 1 467 0
	b	.L39
.L44:
	.loc 1 471 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L53
	.loc 1 473 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	orr	r2, r3, #8
	ldr	r3, [fp, #-20]
	str	r2, [r3, #4]
	.loc 1 479 0
	b	.L39
.L53:
	.loc 1 477 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	bic	r2, r3, #8
	ldr	r3, [fp, #-20]
	str	r2, [r3, #4]
	.loc 1 479 0
	b	.L39
.L45:
	.loc 1 482 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-24]
	.loc 1 483 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #8]
	.loc 1 484 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #12]
	.loc 1 485 0
	b	.L39
.L46:
	.loc 1 488 0
	ldr	r3, [fp, #-44]
	and	r2, r3, #3
	ldr	r3, [fp, #-20]
	str	r2, [r3, #32]
	.loc 1 490 0
	b	.L39
.L47:
	.loc 1 494 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	beq	.L57
	cmp	r3, #2
	beq	.L58
	cmp	r3, #0
	bne	.L61
.L56:
	.loc 1 498 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	and	r3, r3, #65280
	mov	r3, r3, lsr #8
	str	r3, [fp, #-28]
	.loc 1 499 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #16]
	str	r3, [fp, #-8]
	.loc 1 500 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L59
	.loc 1 503 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L59:
	.loc 1 508 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #16]
	.loc 1 507 0
	ldr	r2, .L62
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	clkpwr_get_clock_rate
	str	r0, [fp, #-32]
	.loc 1 509 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	ldr	r2, [fp, #-8]
	mul	r3, r2, r3
	ldr	r0, [fp, #-32]
	mov	r1, r3
	bl	__udivsi3
	mov	r3, r0
	str	r3, [fp, #-12]
	.loc 1 510 0
	b	.L60
.L57:
	.loc 1 513 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	str	r3, [fp, #-12]
	.loc 1 514 0
	b	.L60
.L58:
	.loc 1 517 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #24]
	str	r3, [fp, #-12]
	.loc 1 518 0
	b	.L60
.L61:
	.loc 1 522 0
	mvn	r3, #6
	str	r3, [fp, #-12]
	.loc 1 523 0
	mov	r0, r0	@ nop
.L60:
	.loc 1 525 0
	b	.L39
.L40:
	.loc 1 529 0
	mvn	r3, #6
	str	r3, [fp, #-12]
.L39:
	.loc 1 533 0
	ldr	r3, [fp, #-12]
	.loc 1 534 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L63:
	.align	2
.L62:
	.word	sspclks
.LFE5:
	.size	ssp_ioctl, .-ssp_ioctl
	.section	.text.ssp_read,"ax",%progbits
	.align	2
	.global	ssp_read
	.type	ssp_read, %function
ssp_read:
.LFB6:
	.loc 1 560 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI18:
	add	fp, sp, #0
.LCFI19:
	sub	sp, sp, #32
.LCFI20:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	.loc 1 562 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 563 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-16]
	.loc 1 564 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-8]
	.loc 1 565 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-12]
	.loc 1 567 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L65
	.loc 1 569 0
	b	.L66
.L69:
	.loc 1 572 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #8]
	str	r3, [fp, #-20]
	.loc 1 573 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #20]
	cmp	r3, #1
	bne	.L67
	.loc 1 575 0
	ldr	r3, [fp, #-20]
	and	r2, r3, #255
	ldr	r3, [fp, #-12]
	strb	r2, [r3, #0]
	.loc 1 576 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	b	.L68
.L67:
	.loc 1 580 0
	ldr	r3, [fp, #-20]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-8]
	strh	r2, [r3, #0]	@ movhi
	.loc 1 581 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #2
	str	r3, [fp, #-8]
.L68:
	.loc 1 585 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
	.loc 1 586 0
	ldr	r3, [fp, #-32]
	sub	r3, r3, #1
	str	r3, [fp, #-32]
.L66:
	.loc 1 569 0 discriminator 1
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	ble	.L65
	.loc 1 570 0 discriminator 2
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #12]
	and	r3, r3, #4
	.loc 1 569 0 discriminator 2
	cmp	r3, #0
	bne	.L69
.L65:
	.loc 1 590 0
	ldr	r3, [fp, #-4]
	.loc 1 591 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE6:
	.size	ssp_read, .-ssp_read
	.section	.text.ssp_write,"ax",%progbits
	.align	2
	.global	ssp_write
	.type	ssp_write, %function
ssp_write:
.LFB7:
	.loc 1 617 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI21:
	add	fp, sp, #0
.LCFI22:
	sub	sp, sp, #28
.LCFI23:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	.loc 1 618 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 619 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-16]
	.loc 1 620 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-8]
	.loc 1 621 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-12]
	.loc 1 623 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L71
	.loc 1 627 0
	b	.L72
.L76:
	.loc 1 630 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #20]
	cmp	r3, #1
	bne	.L73
	.loc 1 632 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-12]
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	str	r2, [r3, #8]
	.loc 1 633 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	b	.L74
.L73:
	.loc 1 637 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #0]
	str	r2, [r3, #8]
	.loc 1 638 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #2
	str	r3, [fp, #-8]
.L74:
	.loc 1 642 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
	.loc 1 643 0
	ldr	r3, [fp, #-28]
	sub	r3, r3, #1
	str	r3, [fp, #-28]
.L72:
	.loc 1 627 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	ble	.L75
	.loc 1 628 0 discriminator 2
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #12]
	and	r3, r3, #2
	.loc 1 627 0 discriminator 2
	cmp	r3, #0
	bne	.L76
.L75:
	.loc 1 647 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	ble	.L71
	.loc 1 649 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-16]
	ldr	r2, [r2, #4]
	ldr	r2, [r2, #20]
	orr	r2, r2, #8
	str	r2, [r3, #20]
.L71:
	.loc 1 653 0
	ldr	r3, [fp, #-4]
	.loc 1 654 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE7:
	.size	ssp_write, .-ssp_write
	.section	.text.ssp0_int,"ax",%progbits
	.align	2
	.global	ssp0_int
	.type	ssp0_int, %function
ssp0_int:
.LFB8:
	.loc 1 675 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	.loc 1 676 0
	ldr	r0, .L78
	bl	ssp_standard_interrupt
	.loc 1 677 0
	ldmfd	sp!, {fp, pc}
.L79:
	.align	2
.L78:
	.word	sspdrv
.LFE8:
	.size	ssp0_int, .-ssp0_int
	.section	.text.ssp1_int,"ax",%progbits
	.align	2
	.global	ssp1_int
	.type	ssp1_int, %function
ssp1_int:
.LFB9:
	.loc 1 698 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	.loc 1 699 0
	ldr	r0, .L81
	bl	ssp_standard_interrupt
	.loc 1 700 0
	ldmfd	sp!, {fp, pc}
.L82:
	.align	2
.L81:
	.word	sspdrv+24
.LFE9:
	.size	ssp1_int, .-ssp1_int
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI26-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_ssp.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_ssp_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x80e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF121
	.byte	0x1
	.4byte	.LASF122
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x3a
	.4byte	0x45
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x4c
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x5e
	.4byte	0x77
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x67
	.4byte	0x89
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x2
	.byte	0x99
	.4byte	0x77
	.uleb128 0x5
	.ascii	"PFV\000"
	.byte	0x2
	.byte	0xb9
	.4byte	0xb4
	.uleb128 0x6
	.byte	0x4
	.4byte	0xba
	.uleb128 0x7
	.4byte	0xc1
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x2
	.byte	0xfd
	.4byte	0x7e
	.uleb128 0x9
	.4byte	0x6c
	.uleb128 0xa
	.byte	0x4
	.byte	0x3
	.byte	0x24
	.4byte	0x1b2
	.uleb128 0xb
	.4byte	.LASF16
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF18
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF19
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF20
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF21
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF22
	.sleb128 5
	.uleb128 0xb
	.4byte	.LASF23
	.sleb128 6
	.uleb128 0xb
	.4byte	.LASF24
	.sleb128 7
	.uleb128 0xb
	.4byte	.LASF25
	.sleb128 8
	.uleb128 0xb
	.4byte	.LASF26
	.sleb128 9
	.uleb128 0xb
	.4byte	.LASF27
	.sleb128 10
	.uleb128 0xb
	.4byte	.LASF28
	.sleb128 11
	.uleb128 0xb
	.4byte	.LASF29
	.sleb128 12
	.uleb128 0xb
	.4byte	.LASF30
	.sleb128 13
	.uleb128 0xb
	.4byte	.LASF31
	.sleb128 14
	.uleb128 0xb
	.4byte	.LASF32
	.sleb128 15
	.uleb128 0xb
	.4byte	.LASF33
	.sleb128 16
	.uleb128 0xb
	.4byte	.LASF34
	.sleb128 17
	.uleb128 0xb
	.4byte	.LASF35
	.sleb128 18
	.uleb128 0xb
	.4byte	.LASF36
	.sleb128 19
	.uleb128 0xb
	.4byte	.LASF37
	.sleb128 20
	.uleb128 0xb
	.4byte	.LASF38
	.sleb128 21
	.uleb128 0xb
	.4byte	.LASF39
	.sleb128 22
	.uleb128 0xb
	.4byte	.LASF40
	.sleb128 23
	.uleb128 0xb
	.4byte	.LASF41
	.sleb128 24
	.uleb128 0xb
	.4byte	.LASF42
	.sleb128 25
	.uleb128 0xb
	.4byte	.LASF43
	.sleb128 26
	.uleb128 0xb
	.4byte	.LASF44
	.sleb128 27
	.uleb128 0xb
	.4byte	.LASF45
	.sleb128 28
	.uleb128 0xb
	.4byte	.LASF46
	.sleb128 29
	.uleb128 0xb
	.4byte	.LASF47
	.sleb128 30
	.uleb128 0xb
	.4byte	.LASF48
	.sleb128 31
	.uleb128 0xb
	.4byte	.LASF49
	.sleb128 32
	.uleb128 0xb
	.4byte	.LASF50
	.sleb128 33
	.uleb128 0xb
	.4byte	.LASF51
	.sleb128 34
	.byte	0
	.uleb128 0x3
	.4byte	.LASF52
	.byte	0x3
	.byte	0x49
	.4byte	0xd1
	.uleb128 0xc
	.byte	0x28
	.byte	0x4
	.byte	0x23
	.4byte	0x251
	.uleb128 0xd
	.ascii	"cr0\000"
	.byte	0x4
	.byte	0x25
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"cr1\000"
	.byte	0x4
	.byte	0x26
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF53
	.byte	0x4
	.byte	0x27
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.ascii	"sr\000"
	.byte	0x4
	.byte	0x28
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF54
	.byte	0x4
	.byte	0x29
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF55
	.byte	0x4
	.byte	0x2a
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xd
	.ascii	"ris\000"
	.byte	0x4
	.byte	0x2b
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xd
	.ascii	"mis\000"
	.byte	0x4
	.byte	0x2c
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xd
	.ascii	"icr\000"
	.byte	0x4
	.byte	0x2d
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xe
	.4byte	.LASF56
	.byte	0x4
	.byte	0x2e
	.4byte	0xcc
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x3
	.4byte	.LASF57
	.byte	0x4
	.byte	0x2f
	.4byte	0x1bd
	.uleb128 0xc
	.byte	0x18
	.byte	0x5
	.byte	0x26
	.4byte	0x2b9
	.uleb128 0xe
	.4byte	.LASF58
	.byte	0x5
	.byte	0x29
	.4byte	0x6c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF59
	.byte	0x5
	.byte	0x2d
	.4byte	0x6c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF60
	.byte	0x5
	.byte	0x30
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF61
	.byte	0x5
	.byte	0x34
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF62
	.byte	0x5
	.byte	0x36
	.4byte	0x6c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF63
	.byte	0x5
	.byte	0x38
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x3
	.4byte	.LASF64
	.byte	0x5
	.byte	0x39
	.4byte	0x25c
	.uleb128 0xc
	.byte	0x8
	.byte	0x5
	.byte	0x3c
	.4byte	0x2e9
	.uleb128 0xe
	.4byte	.LASF65
	.byte	0x5
	.byte	0x3f
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF66
	.byte	0x5
	.byte	0x41
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF67
	.byte	0x5
	.byte	0x42
	.4byte	0x2c4
	.uleb128 0xa
	.byte	0x4
	.byte	0x5
	.byte	0x46
	.4byte	0x327
	.uleb128 0xb
	.4byte	.LASF68
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF69
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF70
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF71
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF72
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF73
	.sleb128 5
	.uleb128 0xb
	.4byte	.LASF74
	.sleb128 6
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.byte	0x5
	.byte	0x5f
	.4byte	0x342
	.uleb128 0xb
	.4byte	.LASF75
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF76
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF77
	.sleb128 2
	.byte	0
	.uleb128 0xc
	.byte	0x18
	.byte	0x1
	.byte	0x22
	.4byte	0x391
	.uleb128 0xe
	.4byte	.LASF78
	.byte	0x1
	.byte	0x24
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF79
	.byte	0x1
	.byte	0x25
	.4byte	0x391
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.ascii	"cbs\000"
	.byte	0x1
	.byte	0x26
	.4byte	0x2e9
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF80
	.byte	0x1
	.byte	0x27
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF81
	.byte	0x1
	.byte	0x28
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x251
	.uleb128 0x3
	.4byte	.LASF82
	.byte	0x1
	.byte	0x29
	.4byte	0x342
	.uleb128 0xf
	.4byte	.LASF89
	.byte	0x1
	.byte	0x4c
	.byte	0x1
	.4byte	0xc1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x421
	.uleb128 0x10
	.4byte	.LASF83
	.byte	0x1
	.byte	0x4c
	.4byte	0x421
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x10
	.4byte	.LASF84
	.byte	0x1
	.byte	0x4d
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x11
	.4byte	.LASF85
	.byte	0x1
	.byte	0x4f
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x11
	.4byte	.LASF86
	.byte	0x1
	.byte	0x4f
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x11
	.4byte	.LASF87
	.byte	0x1
	.byte	0x4f
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF88
	.byte	0x1
	.byte	0x4f
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF62
	.byte	0x1
	.byte	0x4f
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x397
	.uleb128 0xf
	.4byte	.LASF90
	.byte	0x1
	.byte	0x86
	.byte	0x1
	.4byte	0xc1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x498
	.uleb128 0x10
	.4byte	.LASF91
	.byte	0x1
	.byte	0x86
	.4byte	0x498
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x10
	.4byte	.LASF83
	.byte	0x1
	.byte	0x87
	.4byte	0x421
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x11
	.4byte	.LASF92
	.byte	0x1
	.byte	0x89
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x11
	.4byte	.LASF93
	.byte	0x1
	.byte	0x89
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF94
	.byte	0x1
	.byte	0x8a
	.4byte	0xc1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF95
	.byte	0x1
	.byte	0x8b
	.4byte	0x391
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x2b9
	.uleb128 0x12
	.4byte	.LASF123
	.byte	0x1
	.byte	0xd9
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x4d3
	.uleb128 0x10
	.4byte	.LASF83
	.byte	0x1
	.byte	0xd9
	.4byte	0x421
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF95
	.byte	0x1
	.byte	0xdb
	.4byte	0x391
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x116
	.byte	0x1
	.4byte	0x7e
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x56a
	.uleb128 0x14
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x116
	.4byte	0x56a
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x15
	.ascii	"arg\000"
	.byte	0x1
	.2byte	0x117
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x16
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x119
	.4byte	0x2b9
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x16
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x119
	.4byte	0x498
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x11a
	.4byte	0xcc
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x16
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x11b
	.4byte	0x391
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x11c
	.4byte	0x421
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x11d
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x18
	.byte	0x4
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x179
	.byte	0x1
	.4byte	0xc1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x5b8
	.uleb128 0x14
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x179
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x17b
	.4byte	0x421
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x17c
	.4byte	0xc1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x1a4
	.byte	0x1
	.4byte	0xc1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x66d
	.uleb128 0x14
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x1a4
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x15
	.ascii	"cmd\000"
	.byte	0x1
	.2byte	0x1a5
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x15
	.ascii	"arg\000"
	.byte	0x1
	.2byte	0x1a6
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x16
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x1a8
	.4byte	0x391
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x1a9
	.4byte	0x66d
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x16
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x1aa
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x17
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x1aa
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x16
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x1aa
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x1ab
	.4byte	0x421
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x1ac
	.4byte	0xc1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x2e9
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x22d
	.byte	0x1
	.4byte	0x7e
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x70a
	.uleb128 0x14
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x22d
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x14
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x22e
	.4byte	0x56a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x14
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x22f
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x16
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x231
	.4byte	0xcc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x232
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x16
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x233
	.4byte	0x421
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x234
	.4byte	0x70a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x235
	.4byte	0x710
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x53
	.uleb128 0x6
	.byte	0x4
	.4byte	0x3a
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x266
	.byte	0x1
	.4byte	0x7e
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x79e
	.uleb128 0x14
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x266
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x267
	.4byte	0x56a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x14
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x268
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x16
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x26a
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x16
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x26b
	.4byte	0x421
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x26c
	.4byte	0x70a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x26d
	.4byte	0x710
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x2a2
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x2b9
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x1a
	.4byte	0x397
	.4byte	0x7da
	.uleb128 0x1b
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x11
	.4byte	.LASF119
	.byte	0x1
	.byte	0x2c
	.4byte	0x7ca
	.byte	0x5
	.byte	0x3
	.4byte	sspdrv
	.uleb128 0x1a
	.4byte	0x1b2
	.4byte	0x7fb
	.uleb128 0x1b
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x11
	.4byte	.LASF120
	.byte	0x1
	.byte	0x2f
	.4byte	0x80c
	.byte	0x5
	.byte	0x3
	.4byte	sspclks
	.uleb128 0x1c
	.4byte	0x7eb
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x64
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF77:
	.ascii	"SSP_RAW_INTS_ST\000"
.LASF111:
	.ascii	"count\000"
.LASF113:
	.ascii	"data16\000"
.LASF56:
	.ascii	"dmacr\000"
.LASF67:
	.ascii	"SSP_CBS_T\000"
.LASF33:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF37:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF80:
	.ascii	"thisdev\000"
.LASF45:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF18:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF12:
	.ascii	"long long unsigned int\000"
.LASF14:
	.ascii	"BOOL_32\000"
.LASF119:
	.ascii	"sspdrv\000"
.LASF9:
	.ascii	"UNS_32\000"
.LASF92:
	.ascii	"tmp0\000"
.LASF93:
	.ascii	"tmp1\000"
.LASF107:
	.ascii	"tmp2\000"
.LASF23:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF13:
	.ascii	"long long int\000"
.LASF4:
	.ascii	"signed char\000"
.LASF16:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF86:
	.ascii	"prescale\000"
.LASF91:
	.ascii	"psspcfg\000"
.LASF117:
	.ascii	"ssp0_int\000"
.LASF95:
	.ascii	"psspregs\000"
.LASF1:
	.ascii	"long int\000"
.LASF42:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF49:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF29:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF122:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_ssp_driver.c\000"
.LASF79:
	.ascii	"regptr\000"
.LASF73:
	.ascii	"SSP_CLEAR_INTS\000"
.LASF96:
	.ascii	"ipbase\000"
.LASF85:
	.ascii	"control\000"
.LASF103:
	.ascii	"ssp_ioctl\000"
.LASF21:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF35:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF10:
	.ascii	"unsigned int\000"
.LASF26:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF44:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF11:
	.ascii	"INT_32\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF57:
	.ascii	"SSP_REGS_T\000"
.LASF53:
	.ascii	"data\000"
.LASF7:
	.ascii	"short unsigned int\000"
.LASF104:
	.ascii	"sspregs\000"
.LASF121:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF61:
	.ascii	"usesecond_clk_spi\000"
.LASF64:
	.ascii	"SSP_CONFIG_T\000"
.LASF89:
	.ascii	"ssp_set_clock\000"
.LASF51:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF68:
	.ascii	"SSP_ENABLE\000"
.LASF38:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF31:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF17:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF46:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF108:
	.ascii	"ssp_read\000"
.LASF106:
	.ascii	"sspclk\000"
.LASF71:
	.ascii	"SSP_SO_DISABLE\000"
.LASF27:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF74:
	.ascii	"SSP_GET_STATUS\000"
.LASF62:
	.ascii	"ssp_clk\000"
.LASF99:
	.ascii	"ssp_open\000"
.LASF19:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF47:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF100:
	.ascii	"ssp_close\000"
.LASF114:
	.ascii	"data8\000"
.LASF65:
	.ascii	"txcb\000"
.LASF55:
	.ascii	"imsc\000"
.LASF87:
	.ascii	"cr0_div\000"
.LASF94:
	.ascii	"setup\000"
.LASF76:
	.ascii	"SSP_PENDING_INTS_ST\000"
.LASF39:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF78:
	.ascii	"init\000"
.LASF116:
	.ascii	"n_fifo\000"
.LASF88:
	.ascii	"cmp_clk\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF115:
	.ascii	"ssp_write\000"
.LASF90:
	.ascii	"ssp_configure\000"
.LASF8:
	.ascii	"short int\000"
.LASF22:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF54:
	.ascii	"cpsr\000"
.LASF97:
	.ascii	"ssp_cfg\000"
.LASF70:
	.ascii	"SSP_ENABLE_LOOPB\000"
.LASF36:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF83:
	.ascii	"psspdrvdat\000"
.LASF102:
	.ascii	"sspdrvdat\000"
.LASF110:
	.ascii	"max_fifo\000"
.LASF112:
	.ascii	"sspcfgptr\000"
.LASF60:
	.ascii	"highclk_spi_frames\000"
.LASF43:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF84:
	.ascii	"target_clock\000"
.LASF66:
	.ascii	"rxcb\000"
.LASF2:
	.ascii	"char\000"
.LASF59:
	.ascii	"mode\000"
.LASF120:
	.ascii	"sspclks\000"
.LASF109:
	.ascii	"buffer\000"
.LASF24:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF118:
	.ascii	"ssp1_int\000"
.LASF32:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF82:
	.ascii	"SSP_DRVDAT_T\000"
.LASF52:
	.ascii	"CLKPWR_CLK_T\000"
.LASF41:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF50:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF28:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF101:
	.ascii	"devid\000"
.LASF15:
	.ascii	"STATUS\000"
.LASF6:
	.ascii	"UNS_16\000"
.LASF98:
	.ascii	"status\000"
.LASF20:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF48:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF5:
	.ascii	"UNS_8\000"
.LASF58:
	.ascii	"databits\000"
.LASF72:
	.ascii	"SSP_SET_CALLBACKS\000"
.LASF81:
	.ascii	"dsize\000"
.LASF30:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF105:
	.ascii	"psspcb\000"
.LASF63:
	.ascii	"master_mode\000"
.LASF123:
	.ascii	"ssp_standard_interrupt\000"
.LASF40:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF75:
	.ascii	"SSP_CLOCK_ST\000"
.LASF69:
	.ascii	"SSP_CONFIG\000"
.LASF25:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF34:
	.ascii	"CLKPWR_WDOG_CLK\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
