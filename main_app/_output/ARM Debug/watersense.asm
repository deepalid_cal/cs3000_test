	.file	"watersense.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	ASA
	.section	.rodata.ASA,"a",%progbits
	.align	2
	.type	ASA, %object
	.size	ASA, 112
ASA:
	.word	1045220557
	.word	1041865114
	.word	1036831949
	.word	1036831949
	.word	1047233823
	.word	1044549468
	.word	1042536202
	.word	1040522936
	.word	1048911544
	.word	1046562734
	.word	1043878380
	.word	1041865114
	.word	1050253722
	.word	1048576000
	.word	1045891645
	.word	1043207291
	.word	1051260355
	.word	1049918177
	.word	1047904911
	.word	1045220557
	.word	1052266988
	.word	1050253722
	.word	1048911544
	.word	1046562734
	.word	1053609165
	.word	1051931443
	.word	1050253722
	.word	1048576000
	.global	Ks
	.section	.rodata.Ks,"a",%progbits
	.align	2
	.type	Ks, %object
	.size	Ks, 56
Ks:
	.word	1061997773
	.word	1058642330
	.word	1061158912
	.word	1056964608
	.word	1056964608
	.word	1056964608
	.word	1060320051
	.word	1056964608
	.word	1045220557
	.word	1063675494
	.word	1056964608
	.word	1045220557
	.word	1056964608
	.word	1060320051
	.global	Kd
	.section	.rodata.Kd,"a",%progbits
	.align	2
	.type	Kd, %object
	.size	Kd, 168
Kd:
	.word	1058642330
	.word	1065353216
	.word	1065353216
	.word	1058642330
	.word	1065353216
	.word	1065353216
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1058642330
	.word	1066192077
	.word	1067869798
	.word	1058642330
	.word	1066192077
	.word	1067869798
	.word	1058642330
	.word	1066192077
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1058642330
	.word	1065353216
	.word	1065353216
	.global	Kmc
	.section	.rodata.Kmc,"a",%progbits
	.align	2
	.type	Kmc, %object
	.size	Kmc, 168
Kmc:
	.word	1061997773
	.word	1065353216
	.word	1067030938
	.word	1061997773
	.word	1065353216
	.word	1067030938
	.word	1061997773
	.word	1065353216
	.word	1067030938
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1068708659
	.word	1056964608
	.word	1065353216
	.word	1067030938
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1068708659
	.word	1056964608
	.word	1065353216
	.word	1068708659
	.word	1056964608
	.word	1065353216
	.word	1068708659
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1061997773
	.word	1065353216
	.word	1067030938
	.global	ROOT_ZONE_DEPTH
	.section	.rodata.ROOT_ZONE_DEPTH,"a",%progbits
	.align	2
	.type	ROOT_ZONE_DEPTH, %object
	.size	ROOT_ZONE_DEPTH, 392
ROOT_ZONE_DEPTH:
	.word	1082130432
	.word	1082130432
	.word	1082130432
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1082130432
	.word	1082130432
	.word	1082130432
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1082130432
	.word	1082130432
	.word	1082130432
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1082130432
	.word	1082130432
	.word	1082130432
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.global	MAD
	.section	.rodata.MAD,"a",%progbits
	.align	2
	.type	MAD, %object
	.size	MAD, 28
MAD:
	.word	1051931443
	.word	1053609165
	.word	1056964608
	.word	1056964608
	.word	1057803469
	.word	1056964608
	.word	1056964608
	.global	AW
	.section	.rodata.AW,"a",%progbits
	.align	2
	.type	AW, %object
	.size	AW, 28
AW:
	.word	1043207291
	.word	1043207291
	.word	1043878380
	.word	1043207291
	.word	1040522936
	.word	1035489772
	.word	1031127695
	.global	IR
	.section	.rodata.IR,"a",%progbits
	.align	2
	.type	IR, %object
	.size	IR, 28
IR:
	.word	1036831949
	.word	1041865114
	.word	1045220557
	.word	1051931443
	.word	1053609165
	.word	1056964608
	.word	1058642330
	.global	PR
	.section	.rodata.PR,"a",%progbits
	.align	2
	.type	PR, %object
	.size	PR, 52
PR:
	.word	1071225242
	.word	1061158912
	.word	1056964608
	.word	1056964608
	.word	1065353216
	.word	1061158912
	.word	1059481190
	.word	1056964608
	.word	1065353216
	.word	1061158912
	.word	1076048691
	.word	1045220557
	.word	1065353216
	.section	.data.MINUTES_PER_HOUR,"aw",%progbits
	.align	2
	.type	MINUTES_PER_HOUR, %object
	.size	MINUTES_PER_HOUR, 4
MINUTES_PER_HOUR:
	.word	1114636288
	.section	.text.WATERSENSE_get_Kmc_index_based_on_exposure,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_Kmc_index_based_on_exposure
	.type	WATERSENSE_get_Kmc_index_based_on_exposure, %function
WATERSENSE_get_Kmc_index_based_on_exposure:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/watersense.c"
	.loc 1 275 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 285 0
	ldr	r3, [fp, #-8]
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L2
.L6:
	.word	.L3
	.word	.L4
	.word	.L4
	.word	.L4
	.word	.L5
.L3:
	.loc 1 288 0
	mov	r3, #2
	str	r3, [fp, #-4]
	.loc 1 289 0
	b	.L7
.L4:
	.loc 1 294 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 295 0
	b	.L7
.L5:
	.loc 1 298 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 299 0
	b	.L7
.L2:
	.loc 1 302 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L7:
	.loc 1 305 0
	ldr	r3, [fp, #-4]
	.loc 1 306 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE0:
	.size	WATERSENSE_get_Kmc_index_based_on_exposure, .-WATERSENSE_get_Kmc_index_based_on_exposure
	.section	.text.WATERSENSE_get_RZWWS,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_RZWWS
	.type	WATERSENSE_get_RZWWS, %function
WATERSENSE_get_RZWWS:
.LFB1:
	.loc 1 327 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #16
.LCFI5:
	str	r0, [fp, #-8]	@ float
	str	r1, [fp, #-12]	@ float
	str	r2, [fp, #-16]	@ float
	.loc 1 330 0
	flds	s14, [fp, #-8]
	flds	s15, [fp, #-12]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-16]
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-4]
	.loc 1 332 0
	ldr	r3, [fp, #-4]	@ float
	.loc 1 333 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE1:
	.size	WATERSENSE_get_RZWWS, .-WATERSENSE_get_RZWWS
	.section	.text.WATERSENSE_get_Kl,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_Kl
	.type	WATERSENSE_get_Kl, %function
WATERSENSE_get_Kl:
.LFB2:
	.loc 1 356 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #16
.LCFI8:
	str	r0, [fp, #-8]	@ float
	str	r1, [fp, #-12]	@ float
	str	r2, [fp, #-16]	@ float
	.loc 1 359 0
	flds	s14, [fp, #-8]
	flds	s15, [fp, #-12]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-16]
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-4]
	.loc 1 361 0
	ldr	r3, [fp, #-4]	@ float
	.loc 1 362 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE2:
	.size	WATERSENSE_get_Kl, .-WATERSENSE_get_Kl
	.section	.text.WATERSENSE_get_Rn,"ax",%progbits
	.align	2
	.type	WATERSENSE_get_Rn, %function
WATERSENSE_get_Rn:
.LFB3:
	.loc 1 384 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #12
.LCFI11:
	str	r0, [fp, #-8]	@ float
	str	r1, [fp, #-12]	@ float
	.loc 1 387 0
	flds	s14, [fp, #-12]
	flds	s15, [fp, #-8]
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-4]
	.loc 1 389 0
	ldr	r3, [fp, #-4]	@ float
	.loc 1 390 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE3:
	.size	WATERSENSE_get_Rn, .-WATERSENSE_get_Rn
	.section	.text.WATERSENSE_get_St,"ax",%progbits
	.align	2
	.type	WATERSENSE_get_St, %function
WATERSENSE_get_St:
.LFB4:
	.loc 1 412 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI12:
	add	fp, sp, #0
.LCFI13:
	sub	sp, sp, #16
.LCFI14:
	str	r0, [fp, #-12]	@ float
	str	r1, [fp, #-16]	@ float
	.loc 1 417 0
	ldr	r3, .L12	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 421 0
	ldr	r3, .L12+4
	flds	s14, [r3, #0]
	flds	s15, [fp, #-12]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-16]
	fdivs	s14, s14, s15
	flds	s15, [fp, #-8]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-4]
	.loc 1 423 0
	ldr	r3, [fp, #-4]	@ float
	.loc 1 424 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L13:
	.align	2
.L12:
	.word	1120403456
	.word	MINUTES_PER_HOUR
.LFE4:
	.size	WATERSENSE_get_St, .-WATERSENSE_get_St
	.section .rodata
	.align	2
.LC0:
	.ascii	"Rt Max - PR > IR\000"
	.section	.text.WATERSENSE_get_Rt_max,"ax",%progbits
	.align	2
	.type	WATERSENSE_get_Rt_max, %function
WATERSENSE_get_Rt_max:
.LFB5:
	.loc 1 490 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #16
.LCFI17:
	str	r0, [fp, #-12]	@ float
	str	r1, [fp, #-16]	@ float
	str	r2, [fp, #-20]	@ float
	.loc 1 493 0
	flds	s14, [fp, #-20]
	flds	s15, [fp, #-16]
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L15
	.loc 1 495 0
	ldr	r3, .L17
	flds	s14, [r3, #0]
	flds	s15, [fp, #-12]
	fmuls	s14, s14, s15
	flds	s13, [fp, #-20]
	flds	s15, [fp, #-16]
	fsubs	s15, s13, s15
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-8]
	b	.L16
.L15:
	.loc 1 507 0
	ldr	r3, .L17+4	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 511 0
	ldr	r0, .L17+8
	bl	Alert_Message
.L16:
	.loc 1 514 0
	ldr	r3, [fp, #-8]	@ float
	.loc 1 515 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L18:
	.align	2
.L17:
	.word	MINUTES_PER_HOUR
	.word	1133903872
	.word	.LC0
.LFE5:
	.size	WATERSENSE_get_Rt_max, .-WATERSENSE_get_Rt_max
	.section	.text.WATERSENSE_get_I,"ax",%progbits
	.align	2
	.type	WATERSENSE_get_I, %function
WATERSENSE_get_I:
.LFB6:
	.loc 1 536 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI18:
	add	fp, sp, #0
.LCFI19:
	sub	sp, sp, #12
.LCFI20:
	str	r0, [fp, #-8]	@ float
	str	r1, [fp, #-12]	@ float
	.loc 1 539 0
	flds	s14, [fp, #-8]
	flds	s15, [fp, #-12]
	fmuls	s14, s14, s15
	ldr	r3, .L20
	flds	s15, [r3, #0]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-4]
	.loc 1 541 0
	ldr	r3, [fp, #-4]	@ float
	.loc 1 542 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L21:
	.align	2
.L20:
	.word	MINUTES_PER_HOUR
.LFE6:
	.size	WATERSENSE_get_I, .-WATERSENSE_get_I
	.section	.text.WATERSENSE_get_Da,"ax",%progbits
	.align	2
	.type	WATERSENSE_get_Da, %function
WATERSENSE_get_Da:
.LFB7:
	.loc 1 563 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI21:
	add	fp, sp, #0
.LCFI22:
	sub	sp, sp, #12
.LCFI23:
	str	r0, [fp, #-8]	@ float
	str	r1, [fp, #-12]	@ float
	.loc 1 566 0
	flds	s14, [fp, #-8]
	flds	s15, [fp, #-12]
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-4]
	.loc 1 568 0
	ldr	r3, [fp, #-4]	@ float
	.loc 1 569 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE7:
	.size	WATERSENSE_get_Da, .-WATERSENSE_get_Da
	.section	.text.WATERSENSE_get_cycle_time,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_cycle_time
	.type	WATERSENSE_get_cycle_time, %function
WATERSENSE_get_cycle_time:
.LFB8:
	.loc 1 601 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #16
.LCFI26:
	str	r0, [fp, #-12]	@ float
	str	r1, [fp, #-16]	@ float
	str	r2, [fp, #-20]	@ float
	.loc 1 604 0
	flds	s14, [fp, #-20]
	flds	s15, [fp, #-16]
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L24
	.loc 1 606 0
	ldr	r0, [fp, #-12]	@ float
	ldr	r1, [fp, #-16]	@ float
	ldr	r2, [fp, #-20]	@ float
	bl	WATERSENSE_get_Rt_max
	str	r0, [fp, #-8]	@ float
	.loc 1 612 0
	flds	s14, [fp, #-8]
	flds	s15, .L26
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L25
	.loc 1 614 0
	ldr	r3, .L26	@ float
	str	r3, [fp, #-8]	@ float
	b	.L25
.L24:
	.loc 1 621 0
	ldr	r3, .L26	@ float
	str	r3, [fp, #-8]	@ float
.L25:
	.loc 1 624 0
	ldr	r3, [fp, #-8]	@ float
	.loc 1 625 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L27:
	.align	2
.L26:
	.word	1133903872
.LFE8:
	.size	WATERSENSE_get_cycle_time, .-WATERSENSE_get_cycle_time
	.section	.text.WATERSENSE_get_soak_time__cycle_end_to_cycle_start,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_soak_time__cycle_end_to_cycle_start
	.type	WATERSENSE_get_soak_time__cycle_end_to_cycle_start, %function
WATERSENSE_get_soak_time__cycle_end_to_cycle_start:
.LFB9:
	.loc 1 650 0
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #36
.LCFI29:
	str	r0, [fp, #-28]	@ float
	str	r1, [fp, #-32]	@ float
	str	r2, [fp, #-36]	@ float
	str	r3, [fp, #-40]	@ float
	.loc 1 669 0
	flds	s14, [fp, #-36]
	flds	s15, [fp, #-32]
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L29
	.loc 1 671 0
	ldr	r0, [fp, #-28]	@ float
	ldr	r1, [fp, #-32]	@ float
	ldr	r2, [fp, #-36]	@ float
	bl	WATERSENSE_get_Rt_max
	str	r0, [fp, #-12]	@ float
	.loc 1 673 0
	ldr	r0, [fp, #-12]	@ float
	ldr	r1, [fp, #-36]	@ float
	bl	WATERSENSE_get_I
	str	r0, [fp, #-16]	@ float
	.loc 1 675 0
	ldr	r0, [fp, #-16]	@ float
	ldr	r1, [fp, #-40]	@ float
	bl	WATERSENSE_get_Da
	str	r0, [fp, #-20]	@ float
	.loc 1 677 0
	ldr	r0, [fp, #-20]	@ float
	ldr	r1, [fp, #-32]	@ float
	bl	WATERSENSE_get_St
	str	r0, [fp, #-24]	@ float
	.loc 1 684 0
	flds	s14, [fp, #-24]
	flds	s15, [fp, #-12]
	fsubs	s15, s14, s15
	fsts	s15, [fp, #-8]
	b	.L30
.L29:
	.loc 1 690 0
	ldr	r3, .L31	@ float
	str	r3, [fp, #-8]	@ float
.L30:
	.loc 1 695 0
	ldr	r3, [fp, #-8]	@ float
	.loc 1 696 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L32:
	.align	2
.L31:
	.word	1123024896
.LFE9:
	.size	WATERSENSE_get_soak_time__cycle_end_to_cycle_start, .-WATERSENSE_get_soak_time__cycle_end_to_cycle_start
	.section	.text.WATERSENSE_reduce_moisture_balance_by_ETc,"ax",%progbits
	.align	2
	.global	WATERSENSE_reduce_moisture_balance_by_ETc
	.type	WATERSENSE_reduce_moisture_balance_by_ETc, %function
WATERSENSE_reduce_moisture_balance_by_ETc:
.LFB10:
	.loc 1 711 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI30:
	add	fp, sp, #0
.LCFI31:
	sub	sp, sp, #20
.LCFI32:
	str	r0, [fp, #-12]	@ float
	str	r1, [fp, #-16]	@ float
	str	r2, [fp, #-20]	@ float
	.loc 1 720 0
	flds	s14, [fp, #-12]
	flds	s15, [fp, #-16]
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-4]
	.loc 1 722 0
	flds	s14, [fp, #-20]
	flds	s15, [fp, #-4]
	fsubs	s15, s14, s15
	fsts	s15, [fp, #-8]
	.loc 1 726 0
	ldr	r3, [fp, #-8]	@ float
	.loc 1 727 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE10:
	.size	WATERSENSE_reduce_moisture_balance_by_ETc, .-WATERSENSE_reduce_moisture_balance_by_ETc
	.section	.text.WATERSENSE_increase_moisture_balance_by_irrigation,"ax",%progbits
	.align	2
	.global	WATERSENSE_increase_moisture_balance_by_irrigation
	.type	WATERSENSE_increase_moisture_balance_by_irrigation, %function
WATERSENSE_increase_moisture_balance_by_irrigation:
.LFB11:
	.loc 1 746 0
	@ args = 4, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #28
.LCFI35:
	str	r0, [fp, #-20]	@ float
	str	r1, [fp, #-24]	@ float
	str	r2, [fp, #-28]	@ float
	str	r3, [fp, #-32]	@ float
	.loc 1 764 0
	flds	s14, [fp, #-32]
	flds	s15, [fp, #4]
	fsubs	s15, s14, s15
	fsts	s15, [fp, #-12]
	.loc 1 767 0
	ldr	r0, [fp, #-20]	@ float
	ldr	r1, [fp, #-28]	@ float
	bl	WATERSENSE_get_I
	str	r0, [fp, #-8]	@ float
	.loc 1 771 0
	flds	s14, [fp, #-8]
	flds	s15, [fp, #-12]
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L35
	.loc 1 773 0
	ldr	r3, [fp, #-12]	@ float
	str	r3, [fp, #-8]	@ float
.L35:
	.loc 1 778 0
	flds	s14, [fp, #-8]
	flds	s15, [fp, #-24]
	fmuls	s14, s14, s15
	flds	s15, [fp, #4]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-16]
	.loc 1 782 0
	ldr	r3, [fp, #-16]	@ float
	.loc 1 783 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE11:
	.size	WATERSENSE_increase_moisture_balance_by_irrigation, .-WATERSENSE_increase_moisture_balance_by_irrigation
	.section .rodata
	.align	2
.LC1:
	.ascii	"MB out of range (%0.2f); reset to %0.2f\000"
	.section	.text.WATERSENSE_increase_moisture_balance_by_rain,"ax",%progbits
	.align	2
	.global	WATERSENSE_increase_moisture_balance_by_rain
	.type	WATERSENSE_increase_moisture_balance_by_rain, %function
WATERSENSE_increase_moisture_balance_by_rain:
.LFB12:
	.loc 1 798 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #32
.LCFI38:
	str	r0, [fp, #-20]	@ float
	str	r1, [fp, #-24]	@ float
	str	r2, [fp, #-28]	@ float
	str	r3, [fp, #-32]	@ float
	.loc 1 817 0
	ldr	r0, [fp, #-20]	@ float
	ldr	r1, [fp, #-28]	@ float
	bl	WATERSENSE_get_Rn
	str	r0, [fp, #-12]	@ float
	.loc 1 841 0
	flds	s14, [fp, #-24]
	flds	s15, [fp, #-32]
	fsubs	s15, s14, s15
	fsts	s15, [fp, #-16]
	.loc 1 845 0
	flds	s14, [fp, #-12]
	flds	s15, [fp, #-16]
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L37
	.loc 1 847 0
	ldr	r3, [fp, #-16]	@ float
	str	r3, [fp, #-12]	@ float
.L37:
	.loc 1 852 0
	flds	s14, [fp, #-32]
	flds	s15, [fp, #-12]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-8]
	.loc 1 859 0
	flds	s14, [fp, #-8]
	flds	s15, .L39
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L38
	.loc 1 861 0
	flds	s15, [fp, #-8]
	fcvtds	d7, s15
	ldr	r3, .L39+4
	str	r3, [sp, #0]
	mov	r3, #0
	ldr	r0, .L39+8
	fmrrd	r1, r2, d7
	bl	Alert_Message_va
	.loc 1 863 0
	ldr	r3, .L39+12	@ float
	str	r3, [fp, #-8]	@ float
.L38:
	.loc 1 868 0
	ldr	r3, [fp, #-8]	@ float
	.loc 1 869 0
	mov	r0, r3	@ float
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L40:
	.align	2
.L39:
	.word	1065353216
	.word	1071644672
	.word	.LC1
	.word	1056964608
.LFE12:
	.size	WATERSENSE_increase_moisture_balance_by_rain, .-WATERSENSE_increase_moisture_balance_by_rain
	.section	.text.WATERSENSE_verify_watersense_compliance,"ax",%progbits
	.align	2
	.global	WATERSENSE_verify_watersense_compliance
	.type	WATERSENSE_verify_watersense_compliance, %function
WATERSENSE_verify_watersense_compliance:
.LFB13:
	.loc 1 878 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI39:
	add	fp, sp, #4
.LCFI40:
	sub	sp, sp, #8
.LCFI41:
	.loc 1 885 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L42
.L45:
	.loc 1 887 0
	ldr	r0, [fp, #-8]
	bl	STATION_GROUP_get_group_at_this_index
	str	r0, [fp, #-12]
	.loc 1 889 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L43
	.loc 1 893 0
	ldr	r0, [fp, #-12]
	bl	SCHEDULE_get_enabled
	mov	r3, r0
	cmp	r3, #0
	beq	.L43
	.loc 1 893 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-12]
	bl	DAILY_ET_get_et_in_use
	mov	r3, r0
	cmp	r3, #0
	beq	.L44
	ldr	r0, [fp, #-12]
	bl	RAIN_get_rain_in_use
	mov	r3, r0
	cmp	r3, #0
	bne	.L43
.L44:
	.loc 1 895 0 is_stmt 1
	ldr	r0, [fp, #-12]
	bl	nm_GROUP_get_name
	mov	r3, r0
	mov	r0, r3
	bl	Alert_group_not_watersense_compliant
.L43:
	.loc 1 885 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L42:
	.loc 1 885 0 is_stmt 0 discriminator 1
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r2, r0
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bhi	.L45
	.loc 1 899 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE13:
	.size	WATERSENSE_verify_watersense_compliance, .-WATERSENSE_verify_watersense_compliance
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/station_groups.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7d8
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF53
	.byte	0x1
	.4byte	.LASF54
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x5e
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF10
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0x94
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0x48
	.4byte	0xa4
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.4byte	0x48
	.4byte	0xb4
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x3
	.byte	0
	.uleb128 0x7
	.4byte	.LASF12
	.byte	0x3
	.2byte	0x1a2
	.4byte	0xc0
	.uleb128 0x8
	.4byte	.LASF12
	.byte	0x1
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF13
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x112
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x10a
	.uleb128 0xa
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x112
	.4byte	0x10a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x114
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xc
	.4byte	0x48
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x146
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x16a
	.uleb128 0xa
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x146
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x146
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xa
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x146
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x148
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xc
	.4byte	0x7d
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x163
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x1c9
	.uleb128 0xd
	.ascii	"pKs\000"
	.byte	0x1
	.2byte	0x163
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xd
	.ascii	"pKd\000"
	.byte	0x1
	.2byte	0x163
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x163
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xe
	.ascii	"Kl\000"
	.byte	0x1
	.2byte	0x165
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xf
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x17f
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x211
	.uleb128 0xd
	.ascii	"R\000"
	.byte	0x1
	.2byte	0x17f
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x17f
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xe
	.ascii	"Rn\000"
	.byte	0x1
	.2byte	0x181
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xf
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x19b
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x268
	.uleb128 0xd
	.ascii	"Da\000"
	.byte	0x1
	.2byte	0x19b
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xd
	.ascii	"IR\000"
	.byte	0x1
	.2byte	0x19b
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x1a1
	.4byte	0x268
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xe
	.ascii	"St\000"
	.byte	0x1
	.2byte	0x1a3
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x10
	.4byte	0x16a
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x1e9
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x2c5
	.uleb128 0xd
	.ascii	"ASA\000"
	.byte	0x1
	.2byte	0x1e9
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xd
	.ascii	"IR\000"
	.byte	0x1
	.2byte	0x1e9
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xd
	.ascii	"PR\000"
	.byte	0x1
	.2byte	0x1e9
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x1eb
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x217
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x30c
	.uleb128 0xd
	.ascii	"Rt\000"
	.byte	0x1
	.2byte	0x217
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xd
	.ascii	"PR\000"
	.byte	0x1
	.2byte	0x217
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xe
	.ascii	"I\000"
	.byte	0x1
	.2byte	0x219
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x232
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x352
	.uleb128 0xd
	.ascii	"I\000"
	.byte	0x1
	.2byte	0x232
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xd
	.ascii	"E\000"
	.byte	0x1
	.2byte	0x232
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xe
	.ascii	"Da\000"
	.byte	0x1
	.2byte	0x234
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x258
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x3ad
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x258
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xa
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x258
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x258
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x25a
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x289
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x450
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x289
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xa
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x289
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x289
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0xd
	.ascii	"E\000"
	.byte	0x1
	.2byte	0x289
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x28c
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x28f
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xb
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x292
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xe
	.ascii	"St\000"
	.byte	0x1
	.2byte	0x295
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xb
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x299
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x2c6
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x4b8
	.uleb128 0xd
	.ascii	"Kl\000"
	.byte	0x1
	.2byte	0x2c6
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xd
	.ascii	"ETo\000"
	.byte	0x1
	.2byte	0x2c6
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xd
	.ascii	"MBo\000"
	.byte	0x1
	.2byte	0x2c6
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xe
	.ascii	"MB\000"
	.byte	0x1
	.2byte	0x2c9
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xe
	.ascii	"ETc\000"
	.byte	0x1
	.2byte	0x2cc
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x2e9
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x548
	.uleb128 0xd
	.ascii	"Rt\000"
	.byte	0x1
	.2byte	0x2e9
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xd
	.ascii	"E\000"
	.byte	0x1
	.2byte	0x2e9
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xd
	.ascii	"PR\000"
	.byte	0x1
	.2byte	0x2e9
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x2e9
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xd
	.ascii	"MBo\000"
	.byte	0x1
	.2byte	0x2e9
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xe
	.ascii	"MB\000"
	.byte	0x1
	.2byte	0x2ec
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xe
	.ascii	"I\000"
	.byte	0x1
	.2byte	0x2ef
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x2f2
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x31d
	.byte	0x1
	.4byte	0x7d
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x5cc
	.uleb128 0xd
	.ascii	"R\000"
	.byte	0x1
	.2byte	0x31d
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x31d
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x31d
	.4byte	0x16a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xd
	.ascii	"MBo\000"
	.byte	0x1
	.2byte	0x31d
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xe
	.ascii	"MB\000"
	.byte	0x1
	.2byte	0x320
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xe
	.ascii	"Rn\000"
	.byte	0x1
	.2byte	0x323
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xb
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x328
	.4byte	0x7d
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x36d
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x603
	.uleb128 0xb
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x36f
	.4byte	0x603
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xe
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x371
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.byte	0x4
	.4byte	0xb4
	.uleb128 0x13
	.4byte	.LASF45
	.byte	0x4
	.byte	0x30
	.4byte	0x61a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xc
	.4byte	0x84
	.uleb128 0x13
	.4byte	.LASF46
	.byte	0x4
	.byte	0x34
	.4byte	0x630
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xc
	.4byte	0x84
	.uleb128 0x13
	.4byte	.LASF47
	.byte	0x4
	.byte	0x36
	.4byte	0x646
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xc
	.4byte	0x84
	.uleb128 0x13
	.4byte	.LASF48
	.byte	0x4
	.byte	0x38
	.4byte	0x65c
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xc
	.4byte	0x84
	.uleb128 0x13
	.4byte	.LASF49
	.byte	0x5
	.byte	0x33
	.4byte	0x672
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xc
	.4byte	0x94
	.uleb128 0x13
	.4byte	.LASF50
	.byte	0x5
	.byte	0x3f
	.4byte	0x688
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xc
	.4byte	0xa4
	.uleb128 0xb
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x10d
	.4byte	0x268
	.byte	0x5
	.byte	0x3
	.4byte	MINUTES_PER_HOUR
	.uleb128 0x5
	.4byte	0x7d
	.4byte	0x6af
	.uleb128 0x6
	.4byte	0x6f
	.byte	0xd
	.byte	0
	.uleb128 0x14
	.ascii	"Ks\000"
	.byte	0x1
	.byte	0x49
	.4byte	0x6c0
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Ks
	.uleb128 0xc
	.4byte	0x69f
	.uleb128 0x5
	.4byte	0x7d
	.4byte	0x6db
	.uleb128 0x6
	.4byte	0x6f
	.byte	0xd
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x2
	.byte	0
	.uleb128 0x14
	.ascii	"Kd\000"
	.byte	0x1
	.byte	0x60
	.4byte	0x6ec
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Kd
	.uleb128 0xc
	.4byte	0x6c5
	.uleb128 0x14
	.ascii	"Kmc\000"
	.byte	0x1
	.byte	0x78
	.4byte	0x703
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Kmc
	.uleb128 0xc
	.4byte	0x6c5
	.uleb128 0x5
	.4byte	0x7d
	.4byte	0x71e
	.uleb128 0x6
	.4byte	0x6f
	.byte	0xd
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x6
	.byte	0
	.uleb128 0x15
	.4byte	.LASF52
	.byte	0x1
	.byte	0x92
	.4byte	0x730
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	ROOT_ZONE_DEPTH
	.uleb128 0xc
	.4byte	0x708
	.uleb128 0x5
	.4byte	0x7d
	.4byte	0x745
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x6
	.byte	0
	.uleb128 0x14
	.ascii	"MAD\000"
	.byte	0x1
	.byte	0xb7
	.4byte	0x757
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	MAD
	.uleb128 0xc
	.4byte	0x735
	.uleb128 0x14
	.ascii	"AW\000"
	.byte	0x1
	.byte	0xd3
	.4byte	0x76d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	AW
	.uleb128 0xc
	.4byte	0x735
	.uleb128 0x14
	.ascii	"IR\000"
	.byte	0x1
	.byte	0xe6
	.4byte	0x783
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	IR
	.uleb128 0xc
	.4byte	0x735
	.uleb128 0x5
	.4byte	0x7d
	.4byte	0x79e
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x6
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x3
	.byte	0
	.uleb128 0x14
	.ascii	"ASA\000"
	.byte	0x1
	.byte	0x37
	.4byte	0x7b0
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	ASA
	.uleb128 0xc
	.4byte	0x788
	.uleb128 0x5
	.4byte	0x7d
	.4byte	0x7c5
	.uleb128 0x6
	.4byte	0x6f
	.byte	0xc
	.byte	0
	.uleb128 0x14
	.ascii	"PR\000"
	.byte	0x1
	.byte	0xf4
	.4byte	0x7d6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	PR
	.uleb128 0xc
	.4byte	0x7b5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x84
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF53:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF37:
	.ascii	"I_max\000"
.LASF35:
	.ascii	"precip_rate\000"
.LASF41:
	.ascii	"WATERSENSE_increase_moisture_balance_by_irrigation\000"
.LASF4:
	.ascii	"short int\000"
.LASF14:
	.ascii	"WATERSENSE_get_Kmc_index_based_on_exposure\000"
.LASF55:
	.ascii	"WATERSENSE_verify_watersense_compliance\000"
.LASF27:
	.ascii	"ONE_HUNDRED\000"
.LASF43:
	.ascii	"R_max\000"
.LASF19:
	.ascii	"root_zone_depth\000"
.LASF24:
	.ascii	"usable_rain\000"
.LASF38:
	.ascii	"Da_max\000"
.LASF40:
	.ascii	"WATERSENSE_reduce_moisture_balance_by_ETc\000"
.LASF10:
	.ascii	"float\000"
.LASF29:
	.ascii	"Rt_max\000"
.LASF7:
	.ascii	"long long int\000"
.LASF30:
	.ascii	"WATERSENSE_get_I\000"
.LASF54:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/watersense.c\000"
.LASF21:
	.ascii	"RZWWS\000"
.LASF9:
	.ascii	"long int\000"
.LASF25:
	.ascii	"WATERSENSE_get_Rn\000"
.LASF47:
	.ascii	"GuiFont_DecimalChar\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF23:
	.ascii	"pKmc\000"
.LASF2:
	.ascii	"signed char\000"
.LASF6:
	.ascii	"long long unsigned int\000"
.LASF46:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF17:
	.ascii	"allowable_depletion\000"
.LASF5:
	.ascii	"unsigned int\000"
.LASF39:
	.ascii	"cycle_end_to_cycle_start_soak_time\000"
.LASF44:
	.ascii	"lgroup\000"
.LASF31:
	.ascii	"WATERSENSE_get_Da\000"
.LASF0:
	.ascii	"char\000"
.LASF51:
	.ascii	"MINUTES_PER_HOUR\000"
.LASF36:
	.ascii	"WATERSENSE_get_soak_time__cycle_end_to_cycle_start\000"
.LASF32:
	.ascii	"WATERSENSE_get_cycle_time\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF26:
	.ascii	"WATERSENSE_get_St\000"
.LASF16:
	.ascii	"pexposure\000"
.LASF52:
	.ascii	"ROOT_ZONE_DEPTH\000"
.LASF8:
	.ascii	"long unsigned int\000"
.LASF15:
	.ascii	"WATERSENSE_get_RZWWS\000"
.LASF50:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF20:
	.ascii	"lmicroclimate_factor_index\000"
.LASF12:
	.ascii	"STATION_GROUP_STRUCT\000"
.LASF45:
	.ascii	"GuiFont_LanguageActive\000"
.LASF42:
	.ascii	"WATERSENSE_increase_moisture_balance_by_rain\000"
.LASF49:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF33:
	.ascii	"allowable_surface_accum\000"
.LASF22:
	.ascii	"WATERSENSE_get_Kl\000"
.LASF13:
	.ascii	"double\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF28:
	.ascii	"WATERSENSE_get_Rt_max\000"
.LASF34:
	.ascii	"soil_intake_rate\000"
.LASF48:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF18:
	.ascii	"available_water\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
