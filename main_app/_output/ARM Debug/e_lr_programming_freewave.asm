	.file	"e_lr_programming_freewave.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_LR_PROGRAMMING_previous_cursor_pos,"aw",%nobits
	.align	2
	.type	g_LR_PROGRAMMING_previous_cursor_pos, %object
	.size	g_LR_PROGRAMMING_previous_cursor_pos, 4
g_LR_PROGRAMMING_previous_cursor_pos:
	.space	4
	.section	.rodata.LR_mode_dropdown_lookup,"a",%progbits
	.align	2
	.type	LR_mode_dropdown_lookup, %object
	.size	LR_mode_dropdown_lookup, 12
LR_mode_dropdown_lookup:
	.word	2
	.word	3
	.word	7
	.section	.text.LR_PROGRAMMING_set_LR_mode,"ax",%progbits
	.align	2
	.type	LR_PROGRAMMING_set_LR_mode, %function
LR_PROGRAMMING_set_LR_mode:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_lr_programming_freewave.c"
	.loc 1 70 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-4]
	.loc 1 71 0
	ldr	r3, [fp, #-4]
	cmp	r3, #80
	beq	.L3
	cmp	r3, #84
	bne	.L1
.L4:
	.loc 1 74 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L6
	cmp	r3, #3
	beq	.L7
	b	.L13
.L6:
	.loc 1 77 0
	ldr	r3, .L15
	mov	r2, #3
	str	r2, [r3, #0]
	.loc 1 78 0
	b	.L8
.L7:
	.loc 1 81 0
	ldr	r3, .L15
	mov	r2, #7
	str	r2, [r3, #0]
	.loc 1 82 0
	b	.L8
.L13:
	.loc 1 86 0
	ldr	r3, .L15
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 87 0
	mov	r0, r0	@ nop
.L8:
	.loc 1 89 0
	b	.L1
.L3:
	.loc 1 92 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L10
	cmp	r3, #3
	beq	.L11
	b	.L14
.L10:
	.loc 1 95 0
	ldr	r3, .L15
	mov	r2, #7
	str	r2, [r3, #0]
	.loc 1 96 0
	b	.L12
.L11:
	.loc 1 99 0
	ldr	r3, .L15
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 100 0
	b	.L12
.L14:
	.loc 1 104 0
	ldr	r3, .L15
	mov	r2, #3
	str	r2, [r3, #0]
	.loc 1 105 0
	mov	r0, r0	@ nop
.L12:
	.loc 1 107 0
	mov	r0, r0	@ nop
.L1:
	.loc 1 109 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L16:
	.align	2
.L15:
	.word	GuiVar_LRMode
.LFE0:
	.size	LR_PROGRAMMING_set_LR_mode, .-LR_PROGRAMMING_set_LR_mode
	.section .rodata
	.align	2
.LC0:
	.ascii	"--------\000"
	.section	.text.LR_PROGRAMMING_initialize_guivars,"ax",%progbits
	.align	2
	.type	LR_PROGRAMMING_initialize_guivars, %function
LR_PROGRAMMING_initialize_guivars:
.LFB1:
	.loc 1 116 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 119 0
	ldr	r3, .L18
	ldr	r2, .L18+4
	str	r2, [r3, #0]
	.loc 1 123 0
	ldr	r0, .L18+8
	ldr	r1, .L18+12
	mov	r2, #9
	bl	strlcpy
	.loc 1 125 0
	ldr	r3, .L18+16
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 129 0
	ldr	r3, .L18+20
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 131 0
	ldr	r3, .L18+24
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 133 0
	ldr	r3, .L18+28
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 135 0
	ldr	r3, .L18+32
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 137 0
	ldr	r3, .L18+36
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 139 0
	ldr	r3, .L18+40
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 140 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L19:
	.align	2
.L18:
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36865
	.word	GuiVar_LRSerialNumber
	.word	.LC0
	.word	GuiVar_LRMode
	.word	GuiVar_LRPort
	.word	GuiVar_LRGroup
	.word	GuiVar_LRBeaconRate
	.word	GuiVar_LRSlaveLinksToRepeater
	.word	GuiVar_LRRepeaterInUse
	.word	GuiVar_LRTransmitPower
.LFE1:
	.size	LR_PROGRAMMING_initialize_guivars, .-LR_PROGRAMMING_initialize_guivars
	.section	.text.FDTO_LR_PROGRAMMING_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_LR_PROGRAMMING_process_device_exchange_key, %function
FDTO_LR_PROGRAMMING_process_device_exchange_key:
.LFB2:
	.loc 1 144 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 145 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #36864
	cmp	r3, #6
	bhi	.L26
	mov	r2, #1
	mov	r3, r2, asl r3
	and	r2, r3, #109
	cmp	r2, #0
	bne	.L22
	and	r3, r3, #18
	cmp	r3, #0
	beq	.L25
.L23:
	.loc 1 150 0
	bl	LR_FREEWAVE_copy_settings_into_guivars
	.loc 1 153 0
	ldr	r0, .L27
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L27+4
	str	r2, [r3, #0]
	.loc 1 156 0
	bl	DIALOG_close_ok_dialog
	.loc 1 158 0
	mov	r0, #0
	ldr	r1, [fp, #-12]
	bl	FDTO_LR_PROGRAMMING_lrs_draw_screen
	.loc 1 159 0
	b	.L20
.L22:
	.loc 1 167 0
	ldr	r0, [fp, #-8]
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L27+4
	str	r2, [r3, #0]
	.loc 1 170 0
	bl	DEVICE_EXCHANGE_draw_dialog
	.loc 1 171 0
	b	.L20
.L25:
	.loc 1 175 0
	mov	r0, r0	@ nop
.L26:
	mov	r0, r0	@ nop
.L20:
	.loc 1 177 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L28:
	.align	2
.L27:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
.LFE2:
	.size	FDTO_LR_PROGRAMMING_process_device_exchange_key, .-FDTO_LR_PROGRAMMING_process_device_exchange_key
	.section	.text.FDTO_LR_PROGRAMMING_lrs_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_LR_PROGRAMMING_lrs_draw_screen
	.type	FDTO_LR_PROGRAMMING_lrs_draw_screen, %function
FDTO_LR_PROGRAMMING_lrs_draw_screen:
.LFB3:
	.loc 1 181 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #12
.LCFI11:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 184 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L30
	.loc 1 186 0
	ldr	r0, [fp, #-16]
	bl	LR_PROGRAMMING_initialize_guivars
	.loc 1 189 0
	ldr	r0, .L34
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L34+4
	str	r2, [r3, #0]
	.loc 1 193 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L31
.L30:
	.loc 1 197 0
	ldr	r3, .L34+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmn	r3, #1
	bne	.L32
	.loc 1 199 0
	ldr	r3, .L34+12
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L31
.L32:
	.loc 1 203 0
	ldr	r3, .L34+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L31:
	.loc 1 207 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #29
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 209 0
	bl	GuiLib_Refresh
	.loc 1 211 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L29
	.loc 1 215 0
	bl	DEVICE_EXCHANGE_draw_dialog
	.loc 1 220 0
	ldr	r0, [fp, #-16]
	mov	r1, #82
	bl	LR_FREEWAVE_post_device_query_to_queue
.L29:
	.loc 1 222 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L35:
	.align	2
.L34:
	.word	36867
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_LR_PROGRAMMING_previous_cursor_pos
.LFE3:
	.size	FDTO_LR_PROGRAMMING_lrs_draw_screen, .-FDTO_LR_PROGRAMMING_lrs_draw_screen
	.section	.text.LR_PROGRAMMING_lrs_process_screen,"ax",%progbits
	.align	2
	.global	LR_PROGRAMMING_lrs_process_screen
	.type	LR_PROGRAMMING_lrs_process_screen, %function
LR_PROGRAMMING_lrs_process_screen:
.LFB4:
	.loc 1 226 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #52
.LCFI14:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	.loc 1 232 0
	ldr	r3, [fp, #-48]
	cmp	r3, #4
	beq	.L42
	cmp	r3, #4
	bhi	.L46
	cmp	r3, #1
	beq	.L39
	cmp	r3, #1
	bcc	.L38
	cmp	r3, #2
	beq	.L40
	cmp	r3, #3
	beq	.L41
	b	.L37
.L46:
	cmp	r3, #84
	beq	.L44
	cmp	r3, #84
	bhi	.L47
	cmp	r3, #67
	beq	.L43
	cmp	r3, #80
	beq	.L44
	b	.L37
.L47:
	sub	r3, r3, #36864
	cmp	r3, #6
	bhi	.L37
	.loc 1 243 0
	ldr	r3, .L95
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 247 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 248 0
	ldr	r3, .L95+4
	str	r3, [fp, #-20]
	.loc 1 249 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-16]
	.loc 1 250 0
	ldr	r3, .L95+8
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 252 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 253 0
	b	.L36
.L40:
	.loc 1 258 0
	ldr	r0, .L95+12
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L95+16
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L49
	.loc 1 259 0 discriminator 1
	ldr	r0, .L95+20
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L95+16
	ldr	r3, [r3, #0]
	.loc 1 258 0 discriminator 1
	cmp	r2, r3
	beq	.L49
	.loc 1 261 0
	ldr	r3, .L95+24
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #8
	beq	.L51
	cmp	r3, #9
	beq	.L52
	b	.L91
.L51:
	.loc 1 264 0
	bl	good_key_beep
	.loc 1 268 0
	ldr	r3, .L95+24
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L95+28
	str	r2, [r3, #0]
	.loc 1 270 0
	ldr	r3, .L95+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #82
	bl	LR_FREEWAVE_post_device_query_to_queue
	.loc 1 271 0
	b	.L53
.L52:
	.loc 1 274 0
	bl	good_key_beep
	.loc 1 278 0
	ldr	r3, .L95+24
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L95+28
	str	r2, [r3, #0]
	.loc 1 281 0
	bl	LR_FREEWAVE_extract_changes_from_guivars
	.loc 1 283 0
	ldr	r3, .L95+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #87
	bl	LR_FREEWAVE_post_device_query_to_queue
	.loc 1 284 0
	b	.L53
.L91:
	.loc 1 287 0
	bl	bad_key_beep
	.loc 1 289 0
	b	.L54
.L53:
	b	.L54
.L49:
	.loc 1 292 0
	bl	bad_key_beep
	.loc 1 294 0
	b	.L36
.L54:
	b	.L36
.L42:
	.loc 1 297 0
	ldr	r3, .L95+24
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #2
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L55
.L59:
	.word	.L56
	.word	.L56
	.word	.L57
	.word	.L55
	.word	.L55
	.word	.L55
	.word	.L55
	.word	.L58
.L56:
	.loc 1 301 0
	mov	r0, #0
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 302 0
	b	.L60
.L57:
	.loc 1 305 0
	mov	r0, #2
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 306 0
	b	.L60
.L58:
	.loc 1 309 0
	ldr	r3, .L95+32
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L62
	cmp	r3, #3
	bne	.L92
.L63:
	.loc 1 312 0
	mov	r0, #6
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 313 0
	b	.L64
.L62:
	.loc 1 316 0
	mov	r0, #7
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 317 0
	b	.L64
.L92:
	.loc 1 320 0
	mov	r0, #5
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 322 0
	b	.L60
.L64:
	b	.L60
.L55:
	.loc 1 325 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 327 0
	b	.L36
.L60:
	b	.L36
.L39:
	.loc 1 330 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 331 0
	b	.L36
.L38:
	.loc 1 334 0
	ldr	r3, .L95+24
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #8
	ldrls	pc, [pc, r3, asl #2]
	b	.L65
.L71:
	.word	.L66
	.word	.L66
	.word	.L67
	.word	.L67
	.word	.L65
	.word	.L68
	.word	.L69
	.word	.L69
	.word	.L70
.L66:
	.loc 1 338 0
	mov	r0, #2
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 339 0
	b	.L72
.L67:
	.loc 1 343 0
	mov	r0, #4
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 344 0
	b	.L72
.L68:
	.loc 1 347 0
	ldr	r3, .L95+32
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L74
	cmp	r3, #3
	bne	.L93
.L75:
	.loc 1 350 0
	mov	r0, #6
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 351 0
	b	.L76
.L74:
	.loc 1 354 0
	mov	r0, #7
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 355 0
	b	.L76
.L93:
	.loc 1 358 0
	mov	r0, #8
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 360 0
	b	.L72
.L76:
	b	.L72
.L69:
	.loc 1 364 0
	mov	r0, #8
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 365 0
	b	.L72
.L70:
	.loc 1 368 0
	bl	bad_key_beep
	.loc 1 369 0
	b	.L72
.L65:
	.loc 1 372 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 374 0
	b	.L36
.L72:
	b	.L36
.L41:
	.loc 1 377 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 378 0
	b	.L36
.L44:
	.loc 1 382 0
	ldr	r3, .L95+24
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L77
.L86:
	.word	.L78
	.word	.L79
	.word	.L80
	.word	.L81
	.word	.L82
	.word	.L83
	.word	.L84
	.word	.L85
.L78:
	.loc 1 385 0
	bl	good_key_beep
	.loc 1 387 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	LR_PROGRAMMING_set_LR_mode
	.loc 1 389 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 390 0
	b	.L87
.L80:
	.loc 1 393 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L95+36
	ldr	r2, .L95+40
	ldr	r3, .L95+44
	bl	process_uns32
	.loc 1 395 0
	ldr	r3, .L95+36
	ldr	r2, [r3, #0]
	ldr	r3, .L95+44
	cmp	r2, r3
	bne	.L94
	.loc 1 397 0
	ldr	r3, .L95+48
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 399 0
	b	.L94
.L81:
	.loc 1 402 0
	ldr	r3, .L95+36
	ldr	r2, [r3, #0]
	ldr	r3, .L95+52
	cmp	r2, r3
	bhi	.L89
	.loc 1 404 0
	ldr	r3, [fp, #-48]
	mov	r2, #125
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L95+48
	mov	r2, #0
	ldr	r3, .L95+56
	bl	process_uns32
	.loc 1 410 0
	b	.L87
.L89:
	.loc 1 408 0
	bl	bad_key_beep
	.loc 1 410 0
	b	.L87
.L82:
	.loc 1 413 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L95+60
	mov	r2, #1
	mov	r3, #6
	bl	process_uns32
	.loc 1 414 0
	b	.L87
.L79:
	.loc 1 417 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L95+64
	mov	r2, #0
	mov	r3, #9
	bl	process_uns32
	.loc 1 418 0
	b	.L87
.L83:
	.loc 1 421 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L95+68
	mov	r2, #0
	mov	r3, #10
	bl	process_uns32
	.loc 1 422 0
	b	.L87
.L84:
	.loc 1 425 0
	ldr	r0, .L95+72
	bl	process_bool
	.loc 1 426 0
	b	.L87
.L85:
	.loc 1 429 0
	ldr	r0, .L95+76
	bl	process_bool
	.loc 1 430 0
	b	.L87
.L77:
	.loc 1 433 0
	bl	bad_key_beep
	b	.L87
.L94:
	.loc 1 399 0
	mov	r0, r0	@ nop
.L87:
	.loc 1 436 0
	bl	Refresh_Screen
	.loc 1 437 0
	b	.L36
.L43:
	.loc 1 440 0
	ldr	r3, .L95+80
	mov	r2, #11
	str	r2, [r3, #0]
	.loc 1 442 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 446 0
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	.loc 1 447 0
	b	.L36
.L37:
	.loc 1 450 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L36:
	.loc 1 453 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L96:
	.align	2
.L95:
	.word	LR_PROGRAMMING_querying_device
	.word	FDTO_LR_PROGRAMMING_process_device_exchange_key
	.word	GuiVar_LRPort
	.word	36867
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36870
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_LR_PROGRAMMING_previous_cursor_pos
	.word	GuiVar_LRMode
	.word	GuiVar_LRFrequency_WholeNum
	.word	435
	.word	470
	.word	GuiVar_LRFrequency_Decimal
	.word	469
	.word	9875
	.word	GuiVar_LRGroup
	.word	GuiVar_LRBeaconRate
	.word	GuiVar_LRTransmitPower
	.word	GuiVar_LRSlaveLinksToRepeater
	.word	GuiVar_LRRepeaterInUse
	.word	GuiVar_MenuScreenToShow
.LFE4:
	.size	LR_PROGRAMMING_lrs_process_screen, .-LR_PROGRAMMING_lrs_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_LR_FREEWAVE.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x4ee
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF58
	.byte	0x1
	.4byte	.LASF59
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x2
	.byte	0x55
	.4byte	0x68
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x2
	.byte	0x5e
	.4byte	0x7a
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x4
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x7a
	.uleb128 0x5
	.byte	0x4
	.4byte	0x99
	.uleb128 0x6
	.4byte	0xa0
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.4byte	0x48
	.4byte	0xb0
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x8
	.4byte	0x6f
	.4byte	0xc0
	.uleb128 0x9
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xe5
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x7e
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x80
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x3
	.byte	0x82
	.4byte	0xc0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF16
	.uleb128 0xc
	.4byte	0x6f
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x183
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x7b
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x83
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x86
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x88
	.4byte	0x194
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x8d
	.4byte	0x1a6
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x92
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x96
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9a
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9c
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	0x18f
	.uleb128 0xe
	.4byte	0x18f
	.byte	0
	.uleb128 0xc
	.4byte	0x5d
	.uleb128 0x5
	.byte	0x4
	.4byte	0x183
	.uleb128 0xd
	.byte	0x1
	.4byte	0x1a6
	.uleb128 0xe
	.4byte	0xe5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x19a
	.uleb128 0x4
	.4byte	.LASF26
	.byte	0x4
	.byte	0x9e
	.4byte	0xfc
	.uleb128 0x8
	.4byte	0x41
	.4byte	0x1c7
	.uleb128 0x9
	.4byte	0x25
	.byte	0x8
	.byte	0
	.uleb128 0xf
	.4byte	.LASF27
	.byte	0x1
	.byte	0x45
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1ee
	.uleb128 0x10
	.4byte	.LASF29
	.byte	0x1
	.byte	0x45
	.4byte	0xf7
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x1
	.byte	0x73
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x215
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x1
	.byte	0x73
	.4byte	0xf7
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x1
	.byte	0x8f
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x24a
	.uleb128 0x10
	.4byte	.LASF32
	.byte	0x1
	.byte	0x8f
	.4byte	0xf7
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x1
	.byte	0x8f
	.4byte	0xf7
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.byte	0xb4
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x28e
	.uleb128 0x10
	.4byte	.LASF33
	.byte	0x1
	.byte	0xb4
	.4byte	0x28e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x1
	.byte	0xb4
	.4byte	0xf7
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF36
	.byte	0x1
	.byte	0xb6
	.4byte	0x6f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xc
	.4byte	0x88
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.byte	0xe1
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x2c9
	.uleb128 0x10
	.4byte	.LASF29
	.byte	0x1
	.byte	0xe1
	.4byte	0x2c9
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0xe3
	.4byte	0x1ac
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xc
	.4byte	0xe5
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x16c
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x2ae
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x2b0
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x2b1
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x2b2
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x2b4
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x2b5
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x2b7
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x2b8
	.4byte	0x1b7
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x2b9
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x2bb
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF49
	.byte	0x6
	.2byte	0x127
	.4byte	0x68
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF50
	.byte	0x6
	.2byte	0x132
	.4byte	0x68
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF51
	.byte	0x7
	.byte	0x30
	.4byte	0x3a3
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xc
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF52
	.byte	0x7
	.byte	0x34
	.4byte	0x3b9
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xc
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF53
	.byte	0x7
	.byte	0x36
	.4byte	0x3cf
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xc
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x7
	.byte	0x38
	.4byte	0x3e5
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xc
	.4byte	0xa0
	.uleb128 0x14
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x18d
	.4byte	0x88
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF56
	.byte	0x1
	.byte	0x34
	.4byte	0x6f
	.byte	0x5
	.byte	0x3
	.4byte	g_LR_PROGRAMMING_previous_cursor_pos
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.byte	0x38
	.4byte	0x41a
	.byte	0x5
	.byte	0x3
	.4byte	LR_mode_dropdown_lookup
	.uleb128 0xc
	.4byte	0xb0
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x16c
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x2ae
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x2b0
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x2b1
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x2b2
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x2b4
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x2b5
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x2b7
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x2b8
	.4byte	0x1b7
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x2b9
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x2bb
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF49
	.byte	0x6
	.2byte	0x127
	.4byte	0x68
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF50
	.byte	0x6
	.2byte	0x132
	.4byte	0x68
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x18d
	.4byte	0x88
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF51:
	.ascii	"GuiFont_LanguageActive\000"
.LASF22:
	.ascii	"_04_func_ptr\000"
.LASF57:
	.ascii	"LR_mode_dropdown_lookup\000"
.LASF42:
	.ascii	"GuiVar_LRMode\000"
.LASF38:
	.ascii	"GuiVar_LRBeaconRate\000"
.LASF34:
	.ascii	"FDTO_LR_PROGRAMMING_lrs_draw_screen\000"
.LASF18:
	.ascii	"_02_menu\000"
.LASF21:
	.ascii	"key_process_func_ptr\000"
.LASF1:
	.ascii	"long int\000"
.LASF33:
	.ascii	"pcomplete_redraw\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF20:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF40:
	.ascii	"GuiVar_LRFrequency_WholeNum\000"
.LASF15:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF43:
	.ascii	"GuiVar_LRPort\000"
.LASF45:
	.ascii	"GuiVar_LRSerialNumber\000"
.LASF48:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF27:
	.ascii	"LR_PROGRAMMING_set_LR_mode\000"
.LASF25:
	.ascii	"_08_screen_to_draw\000"
.LASF16:
	.ascii	"float\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF8:
	.ascii	"INT_16\000"
.LASF35:
	.ascii	"LR_PROGRAMMING_lrs_process_screen\000"
.LASF4:
	.ascii	"unsigned char\000"
.LASF32:
	.ascii	"pkeycode\000"
.LASF41:
	.ascii	"GuiVar_LRGroup\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF13:
	.ascii	"keycode\000"
.LASF39:
	.ascii	"GuiVar_LRFrequency_Decimal\000"
.LASF17:
	.ascii	"_01_command\000"
.LASF44:
	.ascii	"GuiVar_LRRepeaterInUse\000"
.LASF26:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF50:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF58:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF52:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF47:
	.ascii	"GuiVar_LRTransmitPower\000"
.LASF30:
	.ascii	"pport\000"
.LASF10:
	.ascii	"unsigned int\000"
.LASF29:
	.ascii	"pkey_event\000"
.LASF3:
	.ascii	"char\000"
.LASF14:
	.ascii	"repeats\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF59:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_lr_programming_freewave.c\000"
.LASF46:
	.ascii	"GuiVar_LRSlaveLinksToRepeater\000"
.LASF2:
	.ascii	"long long int\000"
.LASF56:
	.ascii	"g_LR_PROGRAMMING_previous_cursor_pos\000"
.LASF36:
	.ascii	"lcursor_to_select\000"
.LASF28:
	.ascii	"LR_PROGRAMMING_initialize_guivars\000"
.LASF54:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF53:
	.ascii	"GuiFont_DecimalChar\000"
.LASF7:
	.ascii	"short int\000"
.LASF19:
	.ascii	"_03_structure_to_draw\000"
.LASF9:
	.ascii	"UNS_32\000"
.LASF24:
	.ascii	"_07_u32_argument2\000"
.LASF31:
	.ascii	"FDTO_LR_PROGRAMMING_process_device_exchange_key\000"
.LASF23:
	.ascii	"_06_u32_argument1\000"
.LASF5:
	.ascii	"signed char\000"
.LASF37:
	.ascii	"GuiVar_CommOptionDeviceExchangeResult\000"
.LASF55:
	.ascii	"LR_PROGRAMMING_querying_device\000"
.LASF49:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
