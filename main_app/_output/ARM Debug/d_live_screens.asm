	.file	"d_live_screens.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.global	g_LIVE_SCREENS_cursor_position_when_dialog_displayed
	.section	.bss.g_LIVE_SCREENS_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.type	g_LIVE_SCREENS_cursor_position_when_dialog_displayed, %object
	.size	g_LIVE_SCREENS_cursor_position_when_dialog_displayed, 4
g_LIVE_SCREENS_cursor_position_when_dialog_displayed:
	.space	4
	.global	g_LIVE_SCREENS_dialog_visible
	.section	.bss.g_LIVE_SCREENS_dialog_visible,"aw",%nobits
	.align	2
	.type	g_LIVE_SCREENS_dialog_visible, %object
	.size	g_LIVE_SCREENS_dialog_visible, 4
g_LIVE_SCREENS_dialog_visible:
	.space	4
	.section	.bss.g_LIVE_SCREENS_last_cursor_position,"aw",%nobits
	.align	2
	.type	g_LIVE_SCREENS_last_cursor_position, %object
	.size	g_LIVE_SCREENS_last_cursor_position, 4
g_LIVE_SCREENS_last_cursor_position:
	.space	4
	.section	.text.FDTO_LIVE_SCREENS_draw_dialog,"ax",%progbits
	.align	2
	.type	FDTO_LIVE_SCREENS_draw_dialog, %function
FDTO_LIVE_SCREENS_draw_dialog:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_live_screens.c"
	.loc 1 49 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-12]
	.loc 1 52 0
	ldr	r3, .L8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L8+4
	str	r2, [r3, #0]
	.loc 1 58 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L2
	.loc 1 62 0
	ldr	r3, .L8+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L3
	.loc 1 64 0
	ldr	r3, .L8+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L4
	.loc 1 66 0
	ldr	r3, .L8+8
	mov	r2, #50
	str	r2, [r3, #0]
	b	.L3
.L4:
	.loc 1 68 0
	ldr	r3, .L8+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L5
	.loc 1 70 0
	ldr	r3, .L8+8
	mov	r2, #51
	str	r2, [r3, #0]
	b	.L3
.L5:
	.loc 1 72 0
	ldr	r3, .L8+20
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L6
	.loc 1 74 0
	ldr	r3, .L8+8
	mov	r2, #52
	str	r2, [r3, #0]
	b	.L3
.L6:
	.loc 1 78 0
	ldr	r3, .L8+8
	mov	r2, #53
	str	r2, [r3, #0]
.L3:
	.loc 1 82 0
	ldr	r3, .L8+8
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L7
.L2:
	.loc 1 86 0
	ldr	r3, .L8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L7:
	.loc 1 89 0
	ldr	r3, .L8+24
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 91 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r0, .L8+28
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 92 0
	bl	GuiLib_Refresh
	.loc 1 93 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_LIVE_SCREENS_cursor_position_when_dialog_displayed
	.word	g_LIVE_SCREENS_last_cursor_position
	.word	GuiVar_MainMenuMainLinesAvailable
	.word	GuiVar_MainMenuPOCsExist
	.word	GuiVar_MainMenuBypassExists
	.word	g_LIVE_SCREENS_dialog_visible
	.word	610
.LFE0:
	.size	FDTO_LIVE_SCREENS_draw_dialog, .-FDTO_LIVE_SCREENS_draw_dialog
	.section	.text.LIVE_SCREENS_change_screen,"ax",%progbits
	.align	2
	.type	LIVE_SCREENS_change_screen, %function
LIVE_SCREENS_change_screen:
.LFB1:
	.loc 1 97 0
	@ args = 4, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #52
.LCFI5:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	str	r2, [fp, #-52]
	str	r3, [fp, #-56]
	.loc 1 100 0
	ldr	r3, .L11
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L11+4
	str	r2, [r3, #0]
	.loc 1 102 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 103 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-36]
	.loc 1 104 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-32]
	.loc 1 105 0
	ldr	r3, [fp, #-52]
	str	r3, [fp, #-20]
	.loc 1 106 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 107 0
	ldr	r3, .L11+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 108 0
	ldr	r3, [fp, #-56]
	str	r3, [fp, #-24]
	.loc 1 109 0
	ldr	r3, [fp, #4]
	str	r3, [fp, #-28]
	.loc 1 111 0
	ldr	r3, .L11+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 113 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 114 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L12:
	.align	2
.L11:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_MenuScreenToShow
	.word	g_LIVE_SCREENS_dialog_visible
.LFE1:
	.size	LIVE_SCREENS_change_screen, .-LIVE_SCREENS_change_screen
	.section	.text.LIVE_SCREENS_draw_dialog,"ax",%progbits
	.align	2
	.global	LIVE_SCREENS_draw_dialog
	.type	LIVE_SCREENS_draw_dialog, %function
LIVE_SCREENS_draw_dialog:
.LFB2:
	.loc 1 118 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #40
.LCFI8:
	str	r0, [fp, #-44]
	.loc 1 121 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 122 0
	ldr	r3, .L14
	str	r3, [fp, #-20]
	.loc 1 123 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 124 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 125 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L15:
	.align	2
.L14:
	.word	FDTO_LIVE_SCREENS_draw_dialog
.LFE2:
	.size	LIVE_SCREENS_draw_dialog, .-LIVE_SCREENS_draw_dialog
	.section	.text.LIVE_SCREENS_process_dialog,"ax",%progbits
	.align	2
	.global	LIVE_SCREENS_process_dialog
	.type	LIVE_SCREENS_process_dialog, %function
LIVE_SCREENS_process_dialog:
.LFB3:
	.loc 1 129 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #12
.LCFI11:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 132 0
	ldr	r3, .L34
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L34+4
	str	r2, [r3, #0]
	.loc 1 134 0
	ldr	r3, [fp, #-12]
	cmp	r3, #2
	beq	.L19
	cmp	r3, #2
	bhi	.L22
	cmp	r3, #0
	beq	.L18
	b	.L17
.L22:
	cmp	r3, #4
	beq	.L20
	cmp	r3, #67
	beq	.L21
	b	.L17
.L19:
	.loc 1 137 0
	ldr	r3, .L34
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #50
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L23
.L31:
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
.L24:
	.loc 1 140 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #97
	ldr	r2, .L34+8
	ldr	r3, .L34+12
	bl	LIVE_SCREENS_change_screen
	.loc 1 145 0
	b	.L32
.L25:
	.loc 1 148 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #96
	ldr	r2, .L34+16
	ldr	r3, .L34+20
	bl	LIVE_SCREENS_change_screen
	.loc 1 153 0
	b	.L32
.L26:
	.loc 1 156 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #92
	ldr	r2, .L34+24
	ldr	r3, .L34+28
	bl	LIVE_SCREENS_change_screen
	.loc 1 161 0
	b	.L32
.L27:
	.loc 1 164 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #94
	ldr	r2, .L34+32
	ldr	r3, .L34+36
	bl	LIVE_SCREENS_change_screen
	.loc 1 169 0
	b	.L32
.L28:
	.loc 1 172 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #93
	ldr	r2, .L34+40
	ldr	r3, .L34+44
	bl	LIVE_SCREENS_change_screen
	.loc 1 177 0
	b	.L32
.L29:
	.loc 1 180 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #98
	ldr	r2, .L34+48
	ldr	r3, .L34+52
	bl	LIVE_SCREENS_change_screen
	.loc 1 185 0
	b	.L32
.L30:
	.loc 1 188 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #95
	ldr	r2, .L34+56
	ldr	r3, .L34+60
	bl	LIVE_SCREENS_change_screen
	.loc 1 193 0
	b	.L32
.L23:
	.loc 1 196 0
	bl	bad_key_beep
	.loc 1 198 0
	b	.L16
.L32:
	b	.L16
.L20:
	.loc 1 201 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 202 0
	b	.L16
.L18:
	.loc 1 205 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 206 0
	b	.L16
.L21:
	.loc 1 209 0
	bl	good_key_beep
	.loc 1 213 0
	ldr	r3, .L34
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L34+4
	str	r2, [r3, #0]
	.loc 1 215 0
	ldr	r3, .L34+64
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 219 0
	ldr	r3, .L34+68
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L34
	strh	r2, [r3, #0]	@ movhi
	.loc 1 221 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 222 0
	b	.L16
.L17:
	.loc 1 225 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L16:
	.loc 1 227 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L35:
	.align	2
.L34:
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_LIVE_SCREENS_last_cursor_position
	.word	FDTO_REAL_TIME_SYSTEM_FLOW_draw_report
	.word	REAL_TIME_SYSTEM_FLOW_process_report
	.word	FDTO_REAL_TIME_POC_FLOW_draw_report
	.word	REAL_TIME_POC_FLOW_process_report
	.word	FDTO_REAL_TIME_BYPASS_FLOW_draw_report
	.word	REAL_TIME_BYPASS_FLOW_process_report
	.word	FDTO_REAL_TIME_ELECTRICAL_draw_report
	.word	REAL_TIME_ELECTRICAL_process_report
	.word	FDTO_REAL_TIME_COMMUNICATIONS_draw_report
	.word	REAL_TIME_COMMUNICATIONS_process_report
	.word	FDTO_REAL_TIME_WEATHER_draw_report
	.word	REAL_TIME_WEATHER_process_report
	.word	FDTO_REAL_TIME_LIGHTS_draw_report
	.word	REAL_TIME_LIGHTS_process_report
	.word	g_LIVE_SCREENS_dialog_visible
	.word	g_LIVE_SCREENS_cursor_position_when_dialog_displayed
.LFE3:
	.size	LIVE_SCREENS_process_dialog, .-LIVE_SCREENS_process_dialog
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_live_screens.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x3fe
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF49
	.byte	0x1
	.4byte	.LASF50
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8b
	.uleb128 0x6
	.4byte	0x92
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xb0
	.uleb128 0x9
	.4byte	0x92
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd5
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x82
	.4byte	0xb0
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x167
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x88
	.4byte	0x178
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x8d
	.4byte	0x18a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x92
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x173
	.uleb128 0xd
	.4byte	0x173
	.byte	0
	.uleb128 0xe
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x167
	.uleb128 0xc
	.byte	0x1
	.4byte	0x18a
	.uleb128 0xd
	.4byte	0xd5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x17e
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9e
	.4byte	0xe0
	.uleb128 0xf
	.4byte	.LASF26
	.byte	0x1
	.byte	0x30
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1d0
	.uleb128 0x10
	.4byte	.LASF28
	.byte	0x1
	.byte	0x30
	.4byte	0x1d0
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF33
	.byte	0x1
	.byte	0x38
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x7a
	.uleb128 0xf
	.4byte	.LASF27
	.byte	0x1
	.byte	0x60
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x242
	.uleb128 0x10
	.4byte	.LASF29
	.byte	0x1
	.byte	0x60
	.4byte	0x242
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x1
	.byte	0x60
	.4byte	0x242
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0x60
	.4byte	0x253
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x10
	.4byte	.LASF32
	.byte	0x1
	.byte	0x60
	.4byte	0x26a
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x10
	.4byte	.LASF19
	.byte	0x1
	.byte	0x60
	.4byte	0x178
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x12
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x62
	.4byte	0x190
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xe
	.4byte	0x53
	.uleb128 0xc
	.byte	0x1
	.4byte	0x253
	.uleb128 0xd
	.4byte	0x1d0
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x247
	.uleb128 0xc
	.byte	0x1
	.4byte	0x265
	.uleb128 0xd
	.4byte	0x265
	.byte	0
	.uleb128 0xe
	.4byte	0xd5
	.uleb128 0x5
	.byte	0x4
	.4byte	0x259
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.byte	0x75
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x2a6
	.uleb128 0x10
	.4byte	.LASF28
	.byte	0x1
	.byte	0x75
	.4byte	0x1d0
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x12
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x77
	.4byte	0x190
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.byte	0x80
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x2ce
	.uleb128 0x10
	.4byte	.LASF36
	.byte	0x1
	.byte	0x80
	.4byte	0x265
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x2bf
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x2c6
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x2c9
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF42
	.byte	0x7
	.byte	0x30
	.4byte	0x325
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x7
	.byte	0x34
	.4byte	0x33b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF44
	.byte	0x7
	.byte	0x36
	.4byte	0x351
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x7
	.byte	0x38
	.4byte	0x367
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x15
	.4byte	.LASF46
	.byte	0x8
	.byte	0x1f
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF47
	.byte	0x8
	.byte	0x21
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF48
	.byte	0x1
	.byte	0x2a
	.4byte	0x53
	.byte	0x5
	.byte	0x3
	.4byte	g_LIVE_SCREENS_last_cursor_position
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x2bf
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x2c6
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x2c9
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF46
	.byte	0x1
	.byte	0x24
	.4byte	0x53
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_LIVE_SCREENS_cursor_position_when_dialog_displayed
	.uleb128 0x16
	.4byte	.LASF47
	.byte	0x1
	.byte	0x26
	.4byte	0x7a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_LIVE_SCREENS_dialog_visible
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF42:
	.ascii	"GuiFont_LanguageActive\000"
.LASF21:
	.ascii	"_04_func_ptr\000"
.LASF17:
	.ascii	"_02_menu\000"
.LASF32:
	.ascii	"pprocess_funct_ptr\000"
.LASF20:
	.ascii	"key_process_func_ptr\000"
.LASF30:
	.ascii	"pstructure\000"
.LASF38:
	.ascii	"GuiVar_MainMenuMainLinesAvailable\000"
.LASF28:
	.ascii	"pcomplete_redraw\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF15:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF37:
	.ascii	"GuiVar_MainMenuBypassExists\000"
.LASF19:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF24:
	.ascii	"_08_screen_to_draw\000"
.LASF48:
	.ascii	"g_LIVE_SCREENS_last_cursor_position\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF35:
	.ascii	"LIVE_SCREENS_process_dialog\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF27:
	.ascii	"LIVE_SCREENS_change_screen\000"
.LASF13:
	.ascii	"keycode\000"
.LASF16:
	.ascii	"_01_command\000"
.LASF25:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF49:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF43:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF46:
	.ascii	"g_LIVE_SCREENS_cursor_position_when_dialog_displaye"
	.ascii	"d\000"
.LASF47:
	.ascii	"g_LIVE_SCREENS_dialog_visible\000"
.LASF50:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_live_screens.c\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF26:
	.ascii	"FDTO_LIVE_SCREENS_draw_dialog\000"
.LASF36:
	.ascii	"pkey_event\000"
.LASF44:
	.ascii	"GuiFont_DecimalChar\000"
.LASF14:
	.ascii	"repeats\000"
.LASF40:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF34:
	.ascii	"LIVE_SCREENS_draw_dialog\000"
.LASF41:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF9:
	.ascii	"long long int\000"
.LASF39:
	.ascii	"GuiVar_MainMenuPOCsExist\000"
.LASF0:
	.ascii	"char\000"
.LASF33:
	.ascii	"lcursor_to_select\000"
.LASF45:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF29:
	.ascii	"pmenu\000"
.LASF4:
	.ascii	"short int\000"
.LASF18:
	.ascii	"_03_structure_to_draw\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF23:
	.ascii	"_07_u32_argument2\000"
.LASF12:
	.ascii	"long int\000"
.LASF22:
	.ascii	"_06_u32_argument1\000"
.LASF2:
	.ascii	"signed char\000"
.LASF31:
	.ascii	"pdraw_func_ptr\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
