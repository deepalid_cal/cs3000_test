	.file	"cs_mem.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	mem_inited
	.section	.bss.mem_inited,"aw",%nobits
	.align	2
	.type	mem_inited, %object
	.size	mem_inited, 4
mem_inited:
	.space	4
	.global	mem_count
	.section	.bss.mem_count,"aw",%nobits
	.align	2
	.type	mem_count, %object
	.size	mem_count, 4
mem_count:
	.space	4
	.global	mem_maxalloc
	.section	.bss.mem_maxalloc,"aw",%nobits
	.align	2
	.type	mem_maxalloc, %object
	.size	mem_maxalloc, 4
mem_maxalloc:
	.space	4
	.global	mem_numalloc
	.section	.bss.mem_numalloc,"aw",%nobits
	.align	2
	.type	mem_numalloc, %object
	.size	mem_numalloc, 4
mem_numalloc:
	.space	4
	.global	mem_current_allocations_of
	.section	.bss.mem_current_allocations_of,"aw",%nobits
	.align	2
	.type	mem_current_allocations_of, %object
	.size	mem_current_allocations_of, 44
mem_current_allocations_of:
	.space	44
	.global	mem_max_allocations_of
	.section	.bss.mem_max_allocations_of,"aw",%nobits
	.align	2
	.type	mem_max_allocations_of, %object
	.size	mem_max_allocations_of, 44
mem_max_allocations_of:
	.space	44
	.global	mem_alloclist
	.section	.bss.mem_alloclist,"aw",%nobits
	.align	2
	.type	mem_alloclist, %object
	.size	mem_alloclist, 28
mem_alloclist:
	.space	28
	.section	.text.init_mem_debug,"ax",%progbits
	.align	2
	.global	init_mem_debug
	.type	init_mem_debug, %function
init_mem_debug:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cs_mem.c"
	.loc 1 95 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	.loc 1 102 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L2
	.loc 1 104 0
	ldr	r3, .L5+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 107 0
	ldr	r3, .L5+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 108 0
	ldr	r3, .L5+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 109 0
	ldr	r3, .L5+16
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 112 0
	mov	r3, #0
	strh	r3, [fp, #-2]	@ movhi
	b	.L3
.L4:
	.loc 1 114 0 discriminator 2
	ldrh	r2, [fp, #-2]
	ldr	r3, .L5+20
	mov	r1, #0
	str	r1, [r3, r2, asl #2]
	.loc 1 116 0 discriminator 2
	ldrh	r2, [fp, #-2]
	ldr	r3, .L5+24
	mov	r1, #0
	str	r1, [r3, r2, asl #2]
	.loc 1 112 0 discriminator 2
	ldrh	r3, [fp, #-2]	@ movhi
	add	r3, r3, #1
	strh	r3, [fp, #-2]	@ movhi
.L3:
	.loc 1 112 0 is_stmt 0 discriminator 1
	ldrh	r3, [fp, #-2]
	cmp	r3, #10
	bls	.L4
	.loc 1 121 0 is_stmt 1
	ldr	r3, .L5
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 130 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L2:
	.loc 1 128 0 discriminator 1
	b	.L2
.L6:
	.align	2
.L5:
	.word	mem_inited
	.word	mem_count
	.word	mem_numalloc
	.word	mem_maxalloc
	.word	mem_alloclist
	.word	mem_current_allocations_of
	.word	mem_max_allocations_of
.LFE0:
	.size	init_mem_debug, .-init_mem_debug
	.section	.text.__adjust_block_size,"ax",%progbits
	.align	2
	.type	__adjust_block_size, %function
__adjust_block_size:
.LFB1:
	.loc 1 134 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-4]
	.loc 1 139 0
	b	.L8
.L9:
	.loc 1 139 0 is_stmt 0 discriminator 2
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
.L8:
	.loc 1 139 0 discriminator 1
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #0]
	and	r3, r3, #3
	cmp	r3, #0
	bne	.L9
	.loc 1 142 0 is_stmt 1
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #0]
	add	r2, r3, #28
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 143 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE1:
	.size	__adjust_block_size, .-__adjust_block_size
	.section .rodata
	.align	2
.LC0:
	.ascii	"Partition %u Out of memory (rqstd %u bytes) : %s %u"
	.ascii	"\000"
	.align	2
.LC1:
	.ascii	"OSMemGet returned success with NULL ptr? : %s %u\000"
	.align	2
.LC2:
	.ascii	"MEM REQUEST TOO BIG : %u bytes : %s, %u\000"
	.align	2
.LC3:
	.ascii	"NO MEMORY : rqstd %u bytes : %s, %u\000"
	.section	.text.mem_calloc_debug,"ax",%progbits
	.align	2
	.type	mem_calloc_debug, %function
mem_calloc_debug:
.LFB2:
	.loc 1 147 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI6:
	add	fp, sp, #12
.LCFI7:
	sub	sp, sp, #36
.LCFI8:
	str	r0, [fp, #-36]
	str	r1, [fp, #-40]
	str	r2, [fp, #-44]
	.loc 1 154 0
	mov	r3, #0
	str	r3, [fp, #-16]
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 156 0
	sub	r3, fp, #36
	mov	r0, r3
	bl	__adjust_block_size
	.loc 1 160 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L11
.L19:
	.loc 1 162 0
	ldr	r3, .L23
	ldr	r2, [fp, #-20]
	ldr	r2, [r3, r2, asl #3]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bcc	.L12
	.loc 1 164 0
	bl	vTaskSuspendAll
	.loc 1 166 0
	ldr	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L23+4
	add	r2, r2, r3
	sub	r3, fp, #28
	mov	r0, r2
	mov	r1, r3
	bl	OSMemGet
	str	r0, [fp, #-16]
	.loc 1 175 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L13
	.loc 1 175 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L13
	.loc 1 178 0 is_stmt 1
	ldr	r3, .L23
	ldr	r2, [fp, #-20]
	ldr	r3, [r3, r2, asl #3]
	str	r3, [fp, #-36]
	.loc 1 181 0
	ldr	r3, .L23+8
	ldr	r2, [fp, #-20]
	ldr	r3, [r3, r2, asl #2]
	add	r1, r3, #1
	ldr	r3, .L23+8
	ldr	r2, [fp, #-20]
	str	r1, [r3, r2, asl #2]
	.loc 1 183 0
	ldr	r3, .L23+8
	ldr	r2, [fp, #-20]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, .L23+12
	ldr	r1, [fp, #-20]
	ldr	r3, [r3, r1, asl #2]
	cmp	r2, r3
	bls	.L14
	.loc 1 185 0
	ldr	r3, .L23+8
	ldr	r2, [fp, #-20]
	ldr	r1, [r3, r2, asl #2]
	ldr	r3, .L23+12
	ldr	r2, [fp, #-20]
	str	r1, [r3, r2, asl #2]
.L14:
	.loc 1 195 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-40]
	str	r2, [r3, #8]
	.loc 1 197 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-44]
	str	r2, [r3, #12]
	.loc 1 199 0
	ldr	r2, [fp, #-36]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #16]
	.loc 1 201 0
	ldr	r3, [fp, #-16]
	ldr	r2, .L23+16
	str	r2, [r3, #20]
	.loc 1 205 0
	ldr	r3, .L23+20
	str	r3, [fp, #-32]
	.loc 1 206 0
	ldr	r3, [fp, #-16]
	add	r2, r3, #24
	ldr	r3, [fp, #-36]
	sub	r3, r3, #28
	add	r2, r2, r3
	sub	r3, fp, #32
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	memcpy
	.loc 1 210 0
	ldr	r3, .L23+24
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 211 0
	ldr	r3, [fp, #-16]
	ldr	r2, .L23+24
	str	r2, [r3, #4]
	.loc 1 213 0
	ldr	r3, .L23+24
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 215 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L15
	.loc 1 215 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #4]
.L15:
	.loc 1 218 0 is_stmt 1
	ldr	r3, .L23+28
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L23+28
	str	r2, [r3, #0]
	.loc 1 220 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #16]
	ldr	r3, .L23+32
	ldr	r3, [r3, #0]
	add	r2, r2, r3
	ldr	r3, .L23+32
	str	r2, [r3, #0]
	.loc 1 222 0
	ldr	r3, .L23+32
	ldr	r2, [r3, #0]
	ldr	r3, .L23+36
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L16
	.loc 1 222 0 is_stmt 0 discriminator 1
	ldr	r3, .L23+32
	ldr	r2, [r3, #0]
	ldr	r3, .L23+36
	str	r2, [r3, #0]
.L16:
	.loc 1 224 0 is_stmt 1
	ldr	r3, [fp, #-16]
	add	r3, r3, #24
	str	r3, [fp, #-24]
	.loc 1 229 0
	bl	xTaskResumeAll
	.loc 1 232 0
	b	.L17
.L13:
	.loc 1 235 0
	bl	xTaskResumeAll
	.loc 1 240 0
	ldr	r3, [fp, #-28]
	cmp	r3, #72
	bne	.L18
	.loc 1 244 0
	ldr	r3, [fp, #-20]
	add	r5, r3, #1
	ldr	r4, [fp, #-36]
	ldr	r0, [fp, #-40]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r2, [fp, #-44]
	str	r2, [sp, #0]
	ldr	r0, .L23+40
	mov	r1, r5
	mov	r2, r4
	bl	Alert_Message_va
	b	.L12
.L18:
	.loc 1 247 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L12
	.loc 1 251 0
	ldr	r0, [fp, #-40]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L23+44
	mov	r1, r3
	ldr	r2, [fp, #-44]
	bl	Alert_Message_va
	.loc 1 253 0
	bl	freeze_and_restart_app
.L12:
	.loc 1 160 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L11:
	.loc 1 160 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #10
	bls	.L19
.L17:
	.loc 1 261 0 is_stmt 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L20
	.loc 1 264 0
	ldr	r3, .L23
	ldr	r2, [r3, #80]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bcs	.L21
	.loc 1 266 0
	ldr	r4, [fp, #-36]
	ldr	r0, [fp, #-40]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L23+48
	mov	r1, r4
	mov	r2, r3
	ldr	r3, [fp, #-44]
	bl	Alert_Message_va
	b	.L22
.L21:
	.loc 1 271 0
	ldr	r4, [fp, #-36]
	ldr	r0, [fp, #-40]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L23+52
	mov	r1, r4
	mov	r2, r3
	ldr	r3, [fp, #-44]
	bl	Alert_Message_va
.L22:
	.loc 1 274 0
	bl	freeze_and_restart_app
.L20:
	.loc 1 283 0
	ldr	r3, [fp, #-24]
	.loc 1 284 0
	mov	r0, r3
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L24:
	.align	2
.L23:
	.word	partitions
	.word	OSMemTbl
	.word	mem_current_allocations_of
	.word	mem_max_allocations_of
	.word	305419896
	.word	-2023406815
	.word	mem_alloclist
	.word	mem_count
	.word	mem_numalloc
	.word	mem_maxalloc
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	.LC3
.LFE2:
	.size	mem_calloc_debug, .-mem_calloc_debug
	.section	.text.mem_malloc_debug,"ax",%progbits
	.align	2
	.global	mem_malloc_debug
	.type	mem_malloc_debug, %function
mem_malloc_debug:
.LFB3:
	.loc 1 288 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #16
.LCFI11:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	.loc 1 291 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-20]
	bl	mem_calloc_debug
	str	r0, [fp, #-8]
	.loc 1 293 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L26
	.loc 1 295 0
	ldr	r0, [fp, #-8]
	mov	r1, #221
	ldr	r2, [fp, #-12]
	bl	memset
.L26:
	.loc 1 298 0
	ldr	r3, [fp, #-8]
	.loc 1 299 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE3:
	.size	mem_malloc_debug, .-mem_malloc_debug
	.section	.text.mem_oabia,"ax",%progbits
	.align	2
	.global	mem_oabia
	.type	mem_oabia, %function
mem_oabia:
.LFB4:
	.loc 1 320 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #28
.LCFI14:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	str	r3, [fp, #-32]
	.loc 1 326 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 328 0
	ldr	r3, [fp, #-24]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 336 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-16]
	.loc 1 338 0
	sub	r3, fp, #16
	mov	r0, r3
	bl	__adjust_block_size
	.loc 1 345 0
	bl	vTaskSuspendAll
	.loc 1 351 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L28
.L31:
	.loc 1 353 0
	ldr	r3, .L32
	ldr	r2, [fp, #-8]
	ldr	r2, [r3, r2, asl #3]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcc	.L29
	.loc 1 355 0
	ldr	r0, .L32+4
	ldr	r2, [fp, #-8]
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L29
	.loc 1 357 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 359 0
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-28]
	ldr	r2, [fp, #-32]
	bl	mem_malloc_debug
	mov	r2, r0
	ldr	r3, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 361 0
	b	.L30
.L29:
	.loc 1 351 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L28:
	.loc 1 351 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #10
	bls	.L31
.L30:
	.loc 1 368 0 is_stmt 1
	bl	xTaskResumeAll
	.loc 1 372 0
	ldr	r3, [fp, #-12]
	.loc 1 373 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L33:
	.align	2
.L32:
	.word	partitions
	.word	OSMemTbl
.LFE4:
	.size	mem_oabia, .-mem_oabia
	.section .rodata
	.align	2
.LC4:
	.ascii	"MEM_FREE: freeing null ptr : %s, %d\000"
	.align	2
.LC5:
	.ascii	"MEM_FREE : freeing with no count : %s, %d\000"
	.align	2
.LC6:
	.ascii	"MEM_FREE : pointer under run : %s, %d\000"
	.align	2
.LC7:
	.ascii	"MEM_FREE : pointer over run : %s, %d\000"
	.align	2
.LC8:
	.ascii	"MEM_FREE : no more to release : %s, %d\000"
	.align	2
.LC9:
	.ascii	"MEM_FREE : OSMemPut error : %s, %d\000"
	.section	.text.mem_free_debug,"ax",%progbits
	.align	2
	.global	mem_free_debug
	.type	mem_free_debug, %function
mem_free_debug:
.LFB5:
	.loc 1 377 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #28
.LCFI17:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	.loc 1 386 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L35
	.loc 1 388 0
	ldr	r0, [fp, #-28]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L47
	mov	r1, r3
	ldr	r2, [fp, #-32]
	bl	Alert_Message_va
	.loc 1 390 0
	bl	freeze_and_restart_app
.L35:
	.loc 1 393 0
	ldr	r3, .L47+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L36
	.loc 1 395 0
	ldr	r0, [fp, #-28]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L47+8
	mov	r1, r3
	ldr	r2, [fp, #-32]
	bl	Alert_Message_va
	.loc 1 397 0
	bl	freeze_and_restart_app
.L36:
	.loc 1 401 0
	ldr	r3, [fp, #-24]
	sub	r3, r3, #24
	str	r3, [fp, #-12]
	.loc 1 403 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #20]
	ldr	r3, .L47+12
	cmp	r2, r3
	beq	.L37
	.loc 1 405 0
	ldr	r0, [fp, #-28]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L47+16
	mov	r1, r3
	ldr	r2, [fp, #-32]
	bl	Alert_Message_va
	.loc 1 407 0
	bl	freeze_and_restart_app
.L37:
	.loc 1 411 0
	ldr	r3, [fp, #-12]
	add	r2, r3, #24
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #16]
	sub	r3, r3, #28
	add	r3, r2, r3
	sub	r2, fp, #20
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	memcpy
	.loc 1 413 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L47+20
	cmp	r2, r3
	beq	.L38
	.loc 1 415 0
	ldr	r0, [fp, #-28]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L47+24
	mov	r1, r3
	ldr	r2, [fp, #-32]
	bl	Alert_Message_va
	.loc 1 417 0
	bl	freeze_and_restart_app
.L38:
	.loc 1 420 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #16]
	ldr	r3, .L47+28
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L39
	.loc 1 422 0
	ldr	r0, [fp, #-28]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L47+32
	mov	r1, r3
	ldr	r2, [fp, #-32]
	bl	Alert_Message_va
	.loc 1 424 0
	bl	freeze_and_restart_app
.L39:
	.loc 1 438 0
	bl	vTaskSuspendAll
	.loc 1 441 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L40
	.loc 1 443 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #0]
	str	r2, [r3, #0]
.L40:
	.loc 1 446 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L41
	.loc 1 448 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #4]
	str	r2, [r3, #4]
.L41:
	.loc 1 451 0
	ldr	r3, .L47+28
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #16]
	rsb	r2, r3, r2
	ldr	r3, .L47+28
	str	r2, [r3, #0]
	.loc 1 453 0
	ldr	r3, .L47+4
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L47+4
	str	r2, [r3, #0]
	.loc 1 461 0
	ldr	r3, [fp, #-12]
	add	r2, r3, #24
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #16]
	sub	r3, r3, #24
	mov	r0, r2
	mov	r1, #238
	mov	r2, r3
	bl	memset
	.loc 1 464 0
	mov	r3, #0
	strh	r3, [fp, #-6]	@ movhi
	b	.L42
.L46:
	.loc 1 466 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #16]
	ldrh	r1, [fp, #-6]
	ldr	r3, .L47+36
	ldr	r3, [r3, r1, asl #3]
	cmp	r2, r3
	bne	.L43
	.loc 1 469 0
	ldrh	r3, [fp, #-6]
	mov	r0, r3
	ldr	r1, [fp, #-12]
	bl	OSMemPut
	mov	r3, r0
	strb	r3, [fp, #-13]
	.loc 1 471 0
	ldrb	r3, [fp, #-13]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L44
	.loc 1 473 0
	ldrh	r3, [fp, #-6]
	ldr	r2, .L47+40
	ldr	r2, [r2, r3, asl #2]
	sub	r1, r2, #1
	ldr	r2, .L47+40
	str	r1, [r2, r3, asl #2]
	.loc 1 475 0
	b	.L45
.L44:
	.loc 1 479 0
	ldr	r0, [fp, #-28]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L47+44
	mov	r1, r3
	ldr	r2, [fp, #-32]
	bl	Alert_Message_va
	.loc 1 481 0
	bl	freeze_and_restart_app
.L43:
	.loc 1 464 0
	ldrh	r3, [fp, #-6]	@ movhi
	add	r3, r3, #1
	strh	r3, [fp, #-6]	@ movhi
.L42:
	.loc 1 464 0 is_stmt 0 discriminator 1
	ldrh	r3, [fp, #-6]
	cmp	r3, #10
	bls	.L46
.L45:
	.loc 1 486 0 is_stmt 1
	bl	xTaskResumeAll
	.loc 1 487 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L48:
	.align	2
.L47:
	.word	.LC4
	.word	mem_count
	.word	.LC5
	.word	305419896
	.word	.LC6
	.word	-2023406815
	.word	.LC7
	.word	mem_numalloc
	.word	.LC8
	.word	partitions
	.word	mem_current_allocations_of
	.word	.LC9
.LFE5:
	.size	mem_free_debug, .-mem_free_debug
	.section .rodata
	.align	2
.LC10:
	.ascii	"size %lu : %s , %lu\000"
	.align	2
.LC11:
	.ascii	"\012\015\000"
	.section	.text.mem_show_allocations,"ax",%progbits
	.align	2
	.global	mem_show_allocations
	.type	mem_show_allocations, %function
mem_show_allocations:
.LFB6:
	.loc 1 491 0
	@ args = 0, pretend = 0, frame = 1296
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI18:
	add	fp, sp, #12
.LCFI19:
	sub	sp, sp, #1296
.LCFI20:
	sub	sp, sp, #8
.LCFI21:
	.loc 1 502 0
	mov	r3, #0
	str	r3, [fp, #-20]
	ldr	r3, .L55
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	b	.L50
.L52:
	.loc 1 504 0 discriminator 1
	ldr	r3, [fp, #-20]
	mov	r2, r3, asl #6
	sub	r3, fp, #1280
	sub	r3, r3, #12
	sub	r3, r3, #8
	add	r5, r3, r2
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #16]
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #8]
	mov	r0, r3
	bl	RemovePathFromFileName
	mov	r2, r0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #12]
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r5
	mov	r1, #64
	ldr	r2, .L55+4
	mov	r3, r4
	bl	snprintf
	.loc 1 506 0 discriminator 1
	ldr	r3, [fp, #-20]
	mov	r2, r3, asl #6
	sub	r3, fp, #1280
	sub	r3, r3, #12
	sub	r3, r3, #8
	add	r3, r3, r2
	mov	r0, r3
	ldr	r1, .L55+8
	mov	r2, #64
	bl	strlcat
	.loc 1 502 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L50:
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L51
	.loc 1 502 0 is_stmt 0 discriminator 2
	ldr	r3, [fp, #-20]
	cmp	r3, #19
	bls	.L52
.L51:
	.loc 1 509 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L53
.L54:
	.loc 1 511 0 discriminator 2
	ldr	r3, [fp, #-20]
	mov	r2, r3, asl #6
	sub	r3, fp, #1280
	sub	r3, r3, #12
	sub	r3, r3, #8
	add	r3, r3, r2
	str	r3, [fp, #-1308]
	.loc 1 513 0 discriminator 2
	ldr	r3, [fp, #-20]
	mov	r2, r3, asl #6
	sub	r3, fp, #1280
	sub	r3, r3, #12
	sub	r3, r3, #8
	add	r3, r3, r2
	mov	r0, r3
	bl	strlen
	mov	r3, r0
	str	r3, [fp, #-1304]
	.loc 1 515 0 discriminator 2
	mov	r3, #2
	str	r3, [sp, #0]
	mov	r3, #1
	str	r3, [sp, #4]
	mov	r0, #3
	ldr	r1, [fp, #-1308]
	ldr	r2, [fp, #-1304]
	mov	r3, #0
	bl	AddCopyOfBlockToXmitList
	.loc 1 509 0 discriminator 2
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L53:
	.loc 1 509 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #19
	bls	.L54
	.loc 1 517 0 is_stmt 1
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L56:
	.align	2
.L55:
	.word	mem_alloclist
	.word	.LC10
	.word	.LC11
.LFE6:
	.size	mem_show_allocations, .-mem_show_allocations
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 2 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/string.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cs_mem_part.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cs_mem.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6a6
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF61
	.byte	0x1
	.4byte	.LASF62
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x2
	.byte	0x16
	.4byte	0x30
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x5
	.byte	0x4
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x6
	.4byte	0x5c
	.4byte	0x81
	.uleb128 0x7
	.4byte	0x30
	.byte	0x1
	.byte	0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF8
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x3
	.byte	0x3a
	.4byte	0x5c
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x3
	.byte	0x5e
	.4byte	0x9e
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x3
	.byte	0x99
	.4byte	0x9e
	.uleb128 0x8
	.byte	0x8
	.byte	0x4
	.byte	0x17
	.4byte	0xd5
	.uleb128 0x9
	.4byte	.LASF14
	.byte	0x4
	.byte	0x19
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF15
	.byte	0x4
	.byte	0x1b
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x4
	.byte	0x1d
	.4byte	0xb0
	.uleb128 0x8
	.byte	0x14
	.byte	0x4
	.byte	0x49
	.4byte	0x12f
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x4
	.byte	0x4a
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x4
	.byte	0x4b
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x4
	.byte	0x4c
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x4
	.byte	0x4d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x4
	.byte	0x4e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x2
	.4byte	.LASF22
	.byte	0x4
	.byte	0x4f
	.4byte	0xe0
	.uleb128 0x8
	.byte	0x1c
	.byte	0x5
	.byte	0x5b
	.4byte	0x1a5
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x5
	.byte	0x5d
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x5
	.byte	0x5e
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x5
	.byte	0x5f
	.4byte	0x1a5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x5
	.byte	0x60
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x5
	.byte	0x61
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x5
	.byte	0x62
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x5
	.byte	0x66
	.4byte	0x1ab
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0x81
	.uleb128 0x6
	.4byte	0x81
	.4byte	0x1bb
	.uleb128 0x7
	.4byte	0x30
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.4byte	.LASF30
	.byte	0x5
	.byte	0x68
	.4byte	0x13a
	.uleb128 0x6
	.4byte	0x93
	.4byte	0x1d6
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.byte	0x6
	.byte	0x14
	.4byte	0x1fb
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x6
	.byte	0x17
	.4byte	0x1fb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x6
	.byte	0x1a
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0x88
	.uleb128 0x2
	.4byte	.LASF33
	.byte	0x6
	.byte	0x1c
	.4byte	0x1d6
	.uleb128 0x3
	.byte	0x4
	.byte	0x4
	.4byte	.LASF34
	.uleb128 0x6
	.4byte	0x93
	.4byte	0x223
	.uleb128 0x7
	.4byte	0x30
	.byte	0x3
	.byte	0
	.uleb128 0x6
	.4byte	0x93
	.4byte	0x233
	.uleb128 0x7
	.4byte	0x30
	.byte	0xa
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.byte	0x4
	.4byte	.LASF35
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.byte	0x5f
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x260
	.uleb128 0xc
	.ascii	"i\000"
	.byte	0x1
	.byte	0x61
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -6
	.byte	0
	.uleb128 0xd
	.4byte	.LASF63
	.byte	0x1
	.byte	0x85
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x285
	.uleb128 0xe
	.ascii	"n\000"
	.byte	0x1
	.byte	0x85
	.4byte	0x285
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0x93
	.uleb128 0xf
	.4byte	.LASF64
	.byte	0x1
	.byte	0x92
	.byte	0x1
	.4byte	0x45
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x313
	.uleb128 0x10
	.4byte	.LASF36
	.byte	0x1
	.byte	0x92
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0xe
	.ascii	"fil\000"
	.byte	0x1
	.byte	0x92
	.4byte	0x1a5
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0xe
	.ascii	"lin\000"
	.byte	0x1
	.byte	0x92
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0xc
	.ascii	"dl\000"
	.byte	0x1
	.byte	0x94
	.4byte	0x313
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xc
	.ascii	"i\000"
	.byte	0x1
	.byte	0x96
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xc
	.ascii	"err\000"
	.byte	0x1
	.byte	0x96
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xc
	.ascii	"ul\000"
	.byte	0x1
	.byte	0x96
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xc
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x98
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0x1bb
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x11f
	.byte	0x1
	.4byte	0x45
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x371
	.uleb128 0x12
	.ascii	"n\000"
	.byte	0x1
	.2byte	0x11f
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.ascii	"fil\000"
	.byte	0x1
	.2byte	0x11f
	.4byte	0x1a5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.ascii	"lin\000"
	.byte	0x1
	.2byte	0x11f
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x121
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x13f
	.byte	0x1
	.4byte	0xa5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x3f6
	.uleb128 0x14
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x13f
	.4byte	0x3f6
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x13f
	.4byte	0x3fb
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.ascii	"fil\000"
	.byte	0x1
	.2byte	0x13f
	.4byte	0x1a5
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.ascii	"lin\000"
	.byte	0x1
	.2byte	0x13f
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x13
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x141
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x143
	.4byte	0xa5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x15
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x14e
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x16
	.4byte	0x93
	.uleb128 0xa
	.byte	0x4
	.4byte	0x45
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x178
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x482
	.uleb128 0x12
	.ascii	"ptr\000"
	.byte	0x1
	.2byte	0x178
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.ascii	"fil\000"
	.byte	0x1
	.2byte	0x178
	.4byte	0x1a5
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.ascii	"lin\000"
	.byte	0x1
	.2byte	0x178
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x13
	.ascii	"dl\000"
	.byte	0x1
	.2byte	0x17a
	.4byte	0x313
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x17c
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -10
	.uleb128 0x15
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x17e
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.ascii	"err\000"
	.byte	0x1
	.2byte	0x180
	.4byte	0x5c
	.byte	0x2
	.byte	0x91
	.sleb128 -17
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x1ea
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x4d8
	.uleb128 0x13
	.ascii	"dl\000"
	.byte	0x1
	.2byte	0x1ee
	.4byte	0x313
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x1f0
	.4byte	0x4d8
	.byte	0x3
	.byte	0x91
	.sleb128 -1304
	.uleb128 0x13
	.ascii	"ldh\000"
	.byte	0x1
	.2byte	0x1f2
	.4byte	0x201
	.byte	0x3
	.byte	0x91
	.sleb128 -1312
	.uleb128 0x13
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x1f4
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x6
	.4byte	0x81
	.4byte	0x4ee
	.uleb128 0x7
	.4byte	0x30
	.byte	0x13
	.uleb128 0x7
	.4byte	0x30
	.byte	0x3f
	.byte	0
	.uleb128 0x18
	.4byte	.LASF46
	.byte	0x7
	.byte	0x30
	.4byte	0x4ff
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x16
	.4byte	0x71
	.uleb128 0x18
	.4byte	.LASF47
	.byte	0x7
	.byte	0x34
	.4byte	0x515
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x16
	.4byte	0x71
	.uleb128 0x18
	.4byte	.LASF48
	.byte	0x7
	.byte	0x36
	.4byte	0x52b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x16
	.4byte	0x71
	.uleb128 0x18
	.4byte	.LASF49
	.byte	0x7
	.byte	0x38
	.4byte	0x541
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x16
	.4byte	0x71
	.uleb128 0x6
	.4byte	0xd5
	.4byte	0x556
	.uleb128 0x7
	.4byte	0x30
	.byte	0xa
	.byte	0
	.uleb128 0x19
	.4byte	.LASF50
	.byte	0x4
	.byte	0x20
	.4byte	0x563
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	0x546
	.uleb128 0x6
	.4byte	0x12f
	.4byte	0x578
	.uleb128 0x7
	.4byte	0x30
	.byte	0xa
	.byte	0
	.uleb128 0x19
	.4byte	.LASF51
	.byte	0x4
	.byte	0x51
	.4byte	0x568
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF52
	.byte	0x5
	.byte	0x6f
	.4byte	0x93
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF53
	.byte	0x5
	.byte	0x71
	.4byte	0x93
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF54
	.byte	0x5
	.byte	0x73
	.4byte	0x93
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF55
	.byte	0x5
	.byte	0x76
	.4byte	0x223
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF56
	.byte	0x5
	.byte	0x78
	.4byte	0x223
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF57
	.byte	0x5
	.byte	0x7a
	.4byte	0x1bb
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF58
	.byte	0x8
	.byte	0x33
	.4byte	0x5e4
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x16
	.4byte	0x1c6
	.uleb128 0x18
	.4byte	.LASF59
	.byte	0x8
	.byte	0x3f
	.4byte	0x5fa
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x16
	.4byte	0x213
	.uleb128 0x19
	.4byte	.LASF60
	.byte	0x1
	.byte	0x28
	.4byte	0x93
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF50
	.byte	0x4
	.byte	0x20
	.4byte	0x619
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	0x546
	.uleb128 0x19
	.4byte	.LASF51
	.byte	0x4
	.byte	0x51
	.4byte	0x568
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF52
	.byte	0x1
	.byte	0x2b
	.4byte	0x93
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	mem_count
	.uleb128 0x1a
	.4byte	.LASF53
	.byte	0x1
	.byte	0x38
	.4byte	0x93
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	mem_maxalloc
	.uleb128 0x1a
	.4byte	.LASF54
	.byte	0x1
	.byte	0x3a
	.4byte	0x93
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	mem_numalloc
	.uleb128 0x1a
	.4byte	.LASF55
	.byte	0x1
	.byte	0x3d
	.4byte	0x223
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	mem_current_allocations_of
	.uleb128 0x1a
	.4byte	.LASF56
	.byte	0x1
	.byte	0x3f
	.4byte	0x223
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	mem_max_allocations_of
	.uleb128 0x1a
	.4byte	.LASF57
	.byte	0x1
	.byte	0x51
	.4byte	0x1bb
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	mem_alloclist
	.uleb128 0x1a
	.4byte	.LASF60
	.byte	0x1
	.byte	0x28
	.4byte	0x93
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	mem_inited
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF61:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF37:
	.ascii	"mem_malloc_debug\000"
.LASF14:
	.ascii	"block_size\000"
.LASF31:
	.ascii	"dptr\000"
.LASF63:
	.ascii	"__adjust_block_size\000"
.LASF3:
	.ascii	"short int\000"
.LASF9:
	.ascii	"size_t\000"
.LASF44:
	.ascii	"mem_show_allocations\000"
.LASF60:
	.ascii	"mem_inited\000"
.LASF42:
	.ascii	"mem_free_debug\000"
.LASF25:
	.ascii	"file\000"
.LASF17:
	.ascii	"OSMemAddr\000"
.LASF53:
	.ascii	"mem_maxalloc\000"
.LASF28:
	.ascii	"beforeval\000"
.LASF32:
	.ascii	"dlen\000"
.LASF26:
	.ascii	"line\000"
.LASF34:
	.ascii	"float\000"
.LASF6:
	.ascii	"long long int\000"
.LASF10:
	.ascii	"UNS_8\000"
.LASF51:
	.ascii	"OSMemTbl\000"
.LASF4:
	.ascii	"long int\000"
.LASF45:
	.ascii	"str_64\000"
.LASF19:
	.ascii	"OSMemBlkSize\000"
.LASF56:
	.ascii	"mem_max_allocations_of\000"
.LASF52:
	.ascii	"mem_count\000"
.LASF57:
	.ascii	"mem_alloclist\000"
.LASF30:
	.ascii	"MEM_DEBUG_STRUCT\000"
.LASF55:
	.ascii	"mem_current_allocations_of\000"
.LASF48:
	.ascii	"GuiFont_DecimalChar\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF20:
	.ascii	"OSMemNBlks\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF2:
	.ascii	"signed char\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF40:
	.ascii	"lsize\000"
.LASF47:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF38:
	.ascii	"mem_oabia\000"
.LASF21:
	.ascii	"OSMemNFree\000"
.LASF8:
	.ascii	"char\000"
.LASF49:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF43:
	.ascii	"after_val\000"
.LASF64:
	.ascii	"mem_calloc_debug\000"
.LASF29:
	.ascii	"data\000"
.LASF16:
	.ascii	"PARTITION_DEFINITION\000"
.LASF15:
	.ascii	"block_count\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF36:
	.ascii	"palloc_size\000"
.LASF59:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF27:
	.ascii	"nbytes\000"
.LASF33:
	.ascii	"DATA_HANDLE\000"
.LASF22:
	.ascii	"OS_MEM\000"
.LASF46:
	.ascii	"GuiFont_LanguageActive\000"
.LASF58:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF41:
	.ascii	"init_mem_debug\000"
.LASF39:
	.ascii	"block_ptr\000"
.LASF35:
	.ascii	"double\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF24:
	.ascii	"prev\000"
.LASF18:
	.ascii	"OSMemFreeList\000"
.LASF54:
	.ascii	"mem_numalloc\000"
.LASF50:
	.ascii	"partitions\000"
.LASF62:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/cs_mem.c\000"
.LASF23:
	.ascii	"next\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
