	.file	"e_poc.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	g_POC_top_line
	.section	.bss.g_POC_top_line,"aw",%nobits
	.align	2
	.type	g_POC_top_line, %object
	.size	g_POC_top_line, 4
g_POC_top_line:
	.space	4
	.global	g_POC_editing_group
	.section	.bss.g_POC_editing_group,"aw",%nobits
	.align	2
	.type	g_POC_editing_group, %object
	.size	g_POC_editing_group, 4
g_POC_editing_group:
	.space	4
	.global	g_POC_current_list_item_index
	.section	.bss.g_POC_current_list_item_index,"aw",%nobits
	.align	2
	.type	g_POC_current_list_item_index, %object
	.size	g_POC_current_list_item_index, 4
g_POC_current_list_item_index:
	.space	4
	.global	POC_MENU_items
	.section	.bss.POC_MENU_items,"aw",%nobits
	.align	2
	.type	POC_MENU_items, %object
	.size	POC_MENU_items, 48
POC_MENU_items:
	.space	48
	.section	.bss.g_POC_prev_cursor_pos,"aw",%nobits
	.align	2
	.type	g_POC_prev_cursor_pos, %object
	.size	g_POC_prev_cursor_pos, 4
g_POC_prev_cursor_pos:
	.space	4
	.section	.text.POC_get_inc_by_for_K_value,"ax",%progbits
	.align	2
	.type	POC_get_inc_by_for_K_value, %function
POC_get_inc_by_for_K_value:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_poc.c"
	.loc 1 110 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 113 0
	ldr	r3, [fp, #-8]
	cmp	r3, #96
	bls	.L2
	.loc 1 115 0
	ldr	r3, .L6	@ float
	str	r3, [fp, #-4]	@ float
	b	.L3
.L2:
	.loc 1 117 0
	ldr	r3, [fp, #-8]
	cmp	r3, #64
	bls	.L4
	.loc 1 119 0
	ldr	r3, .L6+4	@ float
	str	r3, [fp, #-4]	@ float
	b	.L3
.L4:
	.loc 1 121 0
	ldr	r3, [fp, #-8]
	cmp	r3, #32
	bls	.L5
	.loc 1 123 0
	ldr	r3, .L6+8	@ float
	str	r3, [fp, #-4]	@ float
	b	.L3
.L5:
	.loc 1 127 0
	ldr	r3, .L6+12	@ float
	str	r3, [fp, #-4]	@ float
.L3:
	.loc 1 130 0
	ldr	r3, [fp, #-4]	@ float
	.loc 1 131 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L7:
	.align	2
.L6:
	.word	1041865114
	.word	1015222895
	.word	1006834287
	.word	981668463
.LFE0:
	.size	POC_get_inc_by_for_K_value, .-POC_get_inc_by_for_K_value
	.section	.text.POC_get_inc_by_for_offset,"ax",%progbits
	.align	2
	.type	POC_get_inc_by_for_offset, %function
POC_get_inc_by_for_offset:
.LFB1:
	.loc 1 135 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 138 0
	ldr	r3, [fp, #-8]
	cmp	r3, #64
	bls	.L9
	.loc 1 140 0
	ldr	r3, .L12	@ float
	str	r3, [fp, #-4]	@ float
	b	.L10
.L9:
	.loc 1 142 0
	ldr	r3, [fp, #-8]
	cmp	r3, #32
	bls	.L11
	.loc 1 144 0
	ldr	r3, .L12+4	@ float
	str	r3, [fp, #-4]	@ float
	b	.L10
.L11:
	.loc 1 148 0
	ldr	r3, .L12+8	@ float
	str	r3, [fp, #-4]	@ float
.L10:
	.loc 1 151 0
	ldr	r3, [fp, #-4]	@ float
	.loc 1 152 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L13:
	.align	2
.L12:
	.word	1006834287
	.word	998445679
	.word	981668463
.LFE1:
	.size	POC_get_inc_by_for_offset, .-POC_get_inc_by_for_offset
	.section	.text.POC_process_decoder_serial_number,"ax",%progbits
	.align	2
	.type	POC_process_decoder_serial_number, %function
POC_process_decoder_serial_number:
.LFB2:
	.loc 1 156 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI6:
	add	fp, sp, #8
.LCFI7:
	sub	sp, sp, #28
.LCFI8:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	.loc 1 165 0
	ldr	r3, .L21
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-16]
	.loc 1 169 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 171 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L15
.L18:
	.loc 1 173 0
	ldr	r3, [fp, #-28]
	ldr	r4, [r3, #0]
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-12]
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	mov	r3, r0
	cmp	r4, r3
	bne	.L16
	.loc 1 175 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-20]
	.loc 1 179 0
	b	.L17
.L16:
	.loc 1 171 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L15:
	.loc 1 171 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #2
	bls	.L18
.L17:
	.loc 1 185 0 is_stmt 1
	ldr	r3, .L21+4
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	sub	r2, fp, #20
	mov	r1, #1
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	ldr	r0, [fp, #-24]
	mov	r1, r2
	mov	r2, #0
	bl	process_uns32
	.loc 1 187 0
	ldr	r3, [fp, #-20]
	ldr	r0, [fp, #-16]
	mov	r1, r3
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	mov	r3, r0
	cmp	r3, #0
	beq	.L19
	.loc 1 189 0
	ldr	r3, [fp, #-20]
	ldr	r0, [fp, #-16]
	mov	r1, r3
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	mov	r2, r0
	ldr	r3, [fp, #-28]
	str	r2, [r3, #0]
	b	.L14
.L19:
	.loc 1 193 0
	bl	bad_key_beep
.L14:
	.loc 1 195 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L22:
	.align	2
.L21:
	.word	g_GROUP_list_item_index
	.word	GuiVar_POCBypassStages
.LFE2:
	.size	POC_process_decoder_serial_number, .-POC_process_decoder_serial_number
	.section	.text.FDTO_POC_show_poc_usage_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_poc_usage_dropdown, %function
FDTO_POC_show_poc_usage_dropdown:
.LFB3:
	.loc 1 199 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	.loc 1 200 0
	ldr	r3, .L24
	ldr	r3, [r3, #0]
	ldr	r0, .L24+4
	ldr	r1, .L24+8
	mov	r2, #3
	bl	FDTO_COMBOBOX_show
	.loc 1 201 0
	ldmfd	sp!, {fp, pc}
.L25:
	.align	2
.L24:
	.word	GuiVar_POCUsage
	.word	741
	.word	FDTO_COMBOBOX_add_items
.LFE3:
	.size	FDTO_POC_show_poc_usage_dropdown, .-FDTO_POC_show_poc_usage_dropdown
	.section	.text.FDTO_POC_show_master_valve_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_master_valve_dropdown, %function
FDTO_POC_show_master_valve_dropdown:
.LFB4:
	.loc 1 205 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	.loc 1 209 0
	ldr	r3, .L31
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L27
	.loc 1 211 0
	ldr	r3, .L31+4
	mov	r2, #42
	str	r2, [r3, #0]
	b	.L28
.L27:
	.loc 1 215 0
	ldr	r3, .L31+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L29
	.loc 1 217 0
	ldr	r3, .L31+4
	mov	r2, #12
	str	r2, [r3, #0]
	b	.L28
.L29:
	.loc 1 219 0
	ldr	r3, .L31+12
	ldr	r3, [r3, #0]
	cmp	r3, #8
	bne	.L30
	.loc 1 221 0
	ldr	r3, .L31+4
	mov	r2, #24
	str	r2, [r3, #0]
	b	.L28
.L30:
	.loc 1 225 0
	ldr	r3, .L31+4
	mov	r2, #0
	str	r2, [r3, #0]
.L28:
	.loc 1 229 0
	ldr	r3, .L31+16
	ldr	r3, [r3, #0]
	mov	r0, #736
	ldr	r1, .L31+20
	mov	r2, #2
	bl	FDTO_COMBOBOX_show
	.loc 1 230 0
	ldmfd	sp!, {fp, pc}
.L32:
	.align	2
.L31:
	.word	GuiVar_POCType
	.word	GuiVar_ComboBox_Y1
	.word	GuiVar_POCFMType1IsBermad
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_POCMVType1
	.word	FDTO_COMBOBOX_add_items
.LFE4:
	.size	FDTO_POC_show_master_valve_dropdown, .-FDTO_POC_show_master_valve_dropdown
	.section	.text.FDTO_POC_show_flow_meter_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_flow_meter_dropdown, %function
FDTO_POC_show_flow_meter_dropdown:
.LFB5:
	.loc 1 234 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI13:
	add	fp, sp, #4
.LCFI14:
	sub	sp, sp, #4
.LCFI15:
	.loc 1 237 0
	ldr	r3, .L38
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L34
	.loc 1 239 0
	ldr	r3, .L38+4
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #4
	bne	.L35
	.loc 1 241 0
	ldr	r3, .L38+8
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 243 0
	ldr	r3, .L38+12
	mov	r2, #44
	str	r2, [r3, #0]
	b	.L36
.L35:
	.loc 1 245 0
	ldr	r3, .L38+4
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #10
	bne	.L37
	.loc 1 247 0
	ldr	r3, .L38+16
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 249 0
	ldr	r3, .L38+12
	mov	r2, #102
	str	r2, [r3, #0]
	b	.L36
.L37:
	.loc 1 253 0
	ldr	r3, .L38+20
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 255 0
	ldr	r3, .L38+12
	mov	r2, #107
	str	r2, [r3, #0]
	b	.L36
.L34:
	.loc 1 260 0
	ldr	r3, .L38+8
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 262 0
	ldr	r3, .L38+12
	mov	r2, #0
	str	r2, [r3, #0]
.L36:
	.loc 1 265 0
	mov	r0, #728
	ldr	r1, .L38+24
	mov	r2, #18
	ldr	r3, [fp, #-8]
	bl	FDTO_COMBOBOX_show
	.loc 1 266 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L39:
	.align	2
.L38:
	.word	GuiVar_POCType
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_ComboBox_Y1
	.word	GuiVar_POCFlowMeterType2
	.word	GuiVar_POCFlowMeterType3
	.word	FDTO_COMBOBOX_add_items
.LFE5:
	.size	FDTO_POC_show_flow_meter_dropdown, .-FDTO_POC_show_flow_meter_dropdown
	.section	.text.FDTO_POC_close_flow_meter_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_flow_meter_dropdown, %function
FDTO_POC_close_flow_meter_dropdown:
.LFB6:
	.loc 1 270 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI16:
	add	fp, sp, #4
.LCFI17:
	sub	sp, sp, #16
.LCFI18:
	.loc 1 281 0
	ldr	r3, .L46
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L41
	.loc 1 281 0 is_stmt 0 discriminator 1
	ldr	r3, .L46
	ldr	r3, [r3, #0]
	cmp	r3, #44
	bne	.L42
.L41:
	.loc 1 283 0 is_stmt 1
	ldr	r3, .L46+4
	str	r3, [fp, #-8]
	.loc 1 285 0
	ldr	r3, .L46+8
	str	r3, [fp, #-12]
	.loc 1 287 0
	ldr	r3, .L46+12
	str	r3, [fp, #-16]
	b	.L43
.L42:
	.loc 1 289 0
	ldr	r3, .L46
	ldr	r3, [r3, #0]
	cmp	r3, #102
	bne	.L44
	.loc 1 291 0
	ldr	r3, .L46+16
	str	r3, [fp, #-8]
	.loc 1 293 0
	ldr	r3, .L46+20
	str	r3, [fp, #-12]
	.loc 1 295 0
	ldr	r3, .L46+24
	str	r3, [fp, #-16]
	b	.L43
.L44:
	.loc 1 299 0
	ldr	r3, .L46+28
	str	r3, [fp, #-8]
	.loc 1 301 0
	ldr	r3, .L46+32
	str	r3, [fp, #-12]
	.loc 1 303 0
	ldr	r3, .L46+36
	str	r3, [fp, #-16]
.L43:
	.loc 1 310 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 312 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 317 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	cmp	r3, #8
	movls	r2, #0
	movhi	r2, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 321 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	cmp	r3, #15
	bne	.L45
	.loc 1 321 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #14
	bne	.L45
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L45
	.loc 1 325 0 is_stmt 1
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #0]
.L45:
	.loc 1 340 0
	bl	FDTO_COMBOBOX_hide
	.loc 1 341 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L47:
	.align	2
.L46:
	.word	GuiVar_ComboBox_Y1
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_POCReedSwitchType1
	.word	GuiVar_POCFMType1IsBermad
	.word	GuiVar_POCFlowMeterType2
	.word	GuiVar_POCReedSwitchType2
	.word	GuiVar_POCFMType2IsBermad
	.word	GuiVar_POCFlowMeterType3
	.word	GuiVar_POCReedSwitchType3
	.word	GuiVar_POCFMType3IsBermad
.LFE6:
	.size	FDTO_POC_close_flow_meter_dropdown, .-FDTO_POC_close_flow_meter_dropdown
	.section	.text.POC_process_fm_type,"ax",%progbits
	.align	2
	.type	POC_process_fm_type, %function
POC_process_fm_type:
.LFB7:
	.loc 1 360 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI19:
	add	fp, sp, #4
.LCFI20:
	sub	sp, sp, #28
.LCFI21:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 366 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 371 0
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	mov	r2, #0
	mov	r3, #17
	bl	process_uns32
	.loc 1 373 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #15
	bne	.L49
	.loc 1 373 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #14
	bne	.L49
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L49
	.loc 1 377 0 is_stmt 1
	ldr	r3, [fp, #-24]
	mov	r2, #1
	str	r2, [r3, #0]
.L49:
	.loc 1 390 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L50
	.loc 1 390 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #8
	bhi	.L51
.L50:
	.loc 1 391 0 is_stmt 1 discriminator 2
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	.loc 1 390 0 discriminator 2
	cmp	r3, #1
	bne	.L52
	.loc 1 391 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #8
	bhi	.L52
.L51:
	.loc 1 393 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 397 0
	mov	r0, #0
	bl	Redraw_Screen
	b	.L48
.L52:
	.loc 1 399 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #8
	beq	.L54
	.loc 1 399 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #8
	bne	.L48
.L54:
	.loc 1 403 0 is_stmt 1
	mov	r0, #0
	bl	Redraw_Screen
.L48:
	.loc 1 405 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE7:
	.size	POC_process_fm_type, .-POC_process_fm_type
	.section	.text.POC_process_reed_switch_type,"ax",%progbits
	.align	2
	.type	POC_process_reed_switch_type, %function
POC_process_reed_switch_type:
.LFB8:
	.loc 1 409 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI22:
	add	fp, sp, #4
.LCFI23:
	sub	sp, sp, #20
.LCFI24:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 410 0
	ldr	r3, [fp, #-12]
	cmp	r3, #8
	bls	.L56
	.loc 1 410 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #14
	bhi	.L56
	.loc 1 412 0 is_stmt 1
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-16]
	mov	r2, #0
	mov	r3, #1
	bl	process_uns32
	b	.L55
.L56:
	.loc 1 414 0
	ldr	r3, [fp, #-12]
	cmp	r3, #14
	bls	.L58
	.loc 1 414 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #17
	bhi	.L58
	.loc 1 416 0 is_stmt 1
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-16]
	mov	r2, #1
	mov	r3, #1
	bl	process_uns32
	b	.L55
.L58:
	.loc 1 420 0
	bl	bad_key_beep
.L55:
	.loc 1 422 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE8:
	.size	POC_process_reed_switch_type, .-POC_process_reed_switch_type
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_poc.c\000"
	.section	.text.POC_get_menu_index_for_displayed_poc,"ax",%progbits
	.align	2
	.global	POC_get_menu_index_for_displayed_poc
	.type	POC_get_menu_index_for_displayed_poc, %function
POC_get_menu_index_for_displayed_poc:
.LFB9:
	.loc 1 426 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI25:
	add	fp, sp, #4
.LCFI26:
	sub	sp, sp, #12
.LCFI27:
	.loc 1 435 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 437 0
	ldr	r3, .L64
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L64+4
	ldr	r3, .L64+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 439 0
	ldr	r3, .L64+12
	ldr	r2, [r3, #0]
	ldr	r3, .L64+16
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	str	r0, [fp, #-16]
	.loc 1 441 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L60
.L63:
	.loc 1 443 0
	ldr	r3, .L64+20
	ldr	r2, [fp, #-12]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L61
	.loc 1 445 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 447 0
	b	.L62
.L61:
	.loc 1 441 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L60:
	.loc 1 441 0 is_stmt 0 discriminator 1
	bl	POC_get_list_count
	mov	r2, r0
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L63
.L62:
	.loc 1 451 0 is_stmt 1
	ldr	r3, .L64
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 453 0
	ldr	r3, [fp, #-8]
	.loc 1 454 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L65:
	.align	2
.L64:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	437
	.word	g_GROUP_ID
	.word	GuiVar_POCBoxIndex
	.word	POC_MENU_items
.LFE9:
	.size	POC_get_menu_index_for_displayed_poc, .-POC_get_menu_index_for_displayed_poc
	.section	.text.POC_process_NEXT_and_PREV,"ax",%progbits
	.align	2
	.type	POC_process_NEXT_and_PREV, %function
POC_process_NEXT_and_PREV:
.LFB10:
	.loc 1 458 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI28:
	add	fp, sp, #4
.LCFI29:
	sub	sp, sp, #8
.LCFI30:
	str	r0, [fp, #-12]
	.loc 1 461 0
	bl	POC_get_menu_index_for_displayed_poc
	str	r0, [fp, #-8]
	.loc 1 463 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	beq	.L67
	.loc 1 463 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #84
	bne	.L68
.L67:
	.loc 1 467 0 is_stmt 1
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.loc 1 469 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_to_line
	.loc 1 473 0
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bhi	.L69
	.loc 1 475 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 478 0
	ldr	r3, .L76
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L70
	.loc 1 480 0
	mov	r0, #0
	mov	r1, #0
	bl	SCROLL_BOX_up_or_down
	b	.L70
.L69:
	.loc 1 485 0
	bl	bad_key_beep
.L70:
	.loc 1 490 0
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	b	.L71
.L68:
	.loc 1 496 0
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.loc 1 498 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_to_line
	.loc 1 501 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L72
	.loc 1 503 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 506 0
	ldr	r3, .L76
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L73
	.loc 1 508 0
	mov	r0, #0
	mov	r1, #4
	bl	SCROLL_BOX_up_or_down
	b	.L73
.L72:
	.loc 1 513 0
	bl	bad_key_beep
.L73:
	.loc 1 518 0
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
.L71:
	.loc 1 525 0
	ldr	r3, .L76
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L74
	.loc 1 527 0
	bl	POC_extract_and_store_changes_from_GuiVars
	.loc 1 529 0
	ldr	r3, .L76
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L76+4
	str	r2, [r3, #0]
	.loc 1 531 0
	ldr	r3, .L76+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L76+12
	ldr	r3, .L76+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 533 0
	ldr	r3, .L76+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_copy_group_into_guivars
	.loc 1 535 0
	ldr	r3, .L76+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L75
.L74:
	.loc 1 539 0
	bl	bad_key_beep
.L75:
	.loc 1 547 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 548 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L77:
	.align	2
.L76:
	.word	POC_MENU_items
	.word	g_GROUP_list_item_index
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	531
.LFE10:
	.size	POC_process_NEXT_and_PREV, .-POC_process_NEXT_and_PREV
	.section	.text.POC_process_group,"ax",%progbits
	.align	2
	.type	POC_process_group, %function
POC_process_group:
.LFB11:
	.loc 1 567 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI31:
	add	fp, sp, #8
.LCFI32:
	sub	sp, sp, #52
.LCFI33:
	str	r0, [fp, #-52]
	str	r1, [fp, #-48]
	.loc 1 570 0
	ldr	r3, .L161
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #728
	beq	.L82
	cmp	r3, #728
	bgt	.L85
	ldr	r2, .L161+4
	cmp	r3, r2
	beq	.L80
	cmp	r3, #616
	beq	.L81
	b	.L79
.L85:
	cmp	r3, #736
	beq	.L83
	ldr	r2, .L161+8
	cmp	r3, r2
	beq	.L84
	b	.L79
.L80:
	.loc 1 573 0
	ldr	r3, [fp, #-52]
	cmp	r3, #67
	beq	.L86
	.loc 1 573 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-52]
	cmp	r3, #2
	bne	.L87
	ldr	r3, .L161+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #49
	bne	.L87
.L86:
	.loc 1 575 0 is_stmt 1
	bl	POC_extract_and_store_group_name_from_GuiVars
.L87:
	.loc 1 578 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	ldr	r2, .L161+16
	bl	KEYBOARD_process_key
	.loc 1 579 0
	b	.L78
.L81:
	.loc 1 582 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	ldr	r2, .L161+16
	bl	NUMERIC_KEYPAD_process_key
	.loc 1 583 0
	b	.L78
.L84:
	.loc 1 586 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L161+20
	bl	COMBO_BOX_key_press
	.loc 1 587 0
	b	.L78
.L83:
	.loc 1 590 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L161+24
	bl	COMBO_BOX_key_press
	.loc 1 591 0
	b	.L78
.L82:
	.loc 1 594 0
	ldr	r3, [fp, #-52]
	cmp	r3, #2
	beq	.L89
	.loc 1 594 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-52]
	cmp	r3, #67
	bne	.L90
.L89:
	.loc 1 596 0 is_stmt 1
	bl	good_key_beep
	.loc 1 598 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 599 0
	ldr	r3, .L161+28
	str	r3, [fp, #-24]
	.loc 1 600 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 606 0
	b	.L78
.L90:
	.loc 1 604 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	mov	r1, #0
	bl	COMBO_BOX_key_press
	.loc 1 606 0
	b	.L78
.L79:
	.loc 1 609 0
	ldr	r3, [fp, #-52]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L92
.L101:
	.word	.L93
	.word	.L94
	.word	.L95
	.word	.L96
	.word	.L97
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L98
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L98
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L99
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L100
	.word	.L92
	.word	.L92
	.word	.L92
	.word	.L100
.L95:
	.loc 1 612 0
	ldr	r3, .L161+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #17
	ldrls	pc, [pc, r3, asl #2]
	b	.L102
.L113:
	.word	.L103
	.word	.L104
	.word	.L102
	.word	.L102
	.word	.L105
	.word	.L106
	.word	.L107
	.word	.L102
	.word	.L108
	.word	.L102
	.word	.L105
	.word	.L109
	.word	.L110
	.word	.L102
	.word	.L102
	.word	.L105
	.word	.L111
	.word	.L112
.L103:
	.loc 1 615 0
	mov	r0, #150
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	.loc 1 616 0
	b	.L114
.L104:
	.loc 1 619 0
	bl	good_key_beep
	.loc 1 620 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 621 0
	ldr	r3, .L161+32
	str	r3, [fp, #-24]
	.loc 1 622 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 623 0
	b	.L114
.L105:
	.loc 1 628 0
	bl	good_key_beep
	.loc 1 629 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 630 0
	ldr	r3, .L161+36
	str	r3, [fp, #-24]
	.loc 1 631 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 632 0
	b	.L114
.L108:
	.loc 1 635 0
	bl	good_key_beep
	.loc 1 636 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 637 0
	ldr	r3, .L161+40
	str	r3, [fp, #-24]
	.loc 1 638 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 639 0
	b	.L114
.L106:
	.loc 1 642 0
	bl	good_key_beep
	.loc 1 644 0
	ldr	r0, .L161+44
	ldr	r1, .L161+48	@ float
	ldr	r2, .L161+52	@ float
	mov	r3, #3
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 645 0
	b	.L114
.L109:
	.loc 1 648 0
	bl	good_key_beep
	.loc 1 650 0
	ldr	r0, .L161+56
	ldr	r1, .L161+48	@ float
	ldr	r2, .L161+52	@ float
	mov	r3, #3
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 651 0
	b	.L114
.L111:
	.loc 1 654 0
	bl	good_key_beep
	.loc 1 656 0
	ldr	r0, .L161+60
	ldr	r1, .L161+48	@ float
	ldr	r2, .L161+52	@ float
	mov	r3, #3
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 657 0
	b	.L114
.L107:
	.loc 1 660 0
	bl	good_key_beep
	.loc 1 662 0
	ldr	r0, .L161+64
	ldr	r1, .L161+68	@ float
	ldr	r2, .L161+72	@ float
	mov	r3, #3
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 663 0
	b	.L114
.L110:
	.loc 1 666 0
	bl	good_key_beep
	.loc 1 668 0
	ldr	r0, .L161+76
	ldr	r1, .L161+68	@ float
	ldr	r2, .L161+72	@ float
	mov	r3, #3
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 669 0
	b	.L114
.L112:
	.loc 1 672 0
	bl	good_key_beep
	.loc 1 674 0
	ldr	r0, .L161+80
	ldr	r1, .L161+68	@ float
	ldr	r2, .L161+72	@ float
	mov	r3, #3
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 675 0
	b	.L114
.L102:
	.loc 1 678 0
	bl	bad_key_beep
.L114:
	.loc 1 680 0
	bl	Refresh_Screen
	.loc 1 681 0
	b	.L78
.L100:
	.loc 1 685 0
	ldr	r3, .L161+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #1
	cmp	r3, #17
	ldrls	pc, [pc, r3, asl #2]
	b	.L115
.L134:
	.word	.L116
	.word	.L117
	.word	.L118
	.word	.L119
	.word	.L120
	.word	.L121
	.word	.L122
	.word	.L123
	.word	.L124
	.word	.L125
	.word	.L126
	.word	.L127
	.word	.L128
	.word	.L129
	.word	.L130
	.word	.L131
	.word	.L132
	.word	.L133
.L116:
	.loc 1 688 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L161+20
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	.loc 1 689 0
	b	.L135
.L117:
	.loc 1 692 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L161+84
	mov	r2, #2
	mov	r3, #3
	bl	process_uns32
	.loc 1 698 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 699 0
	b	.L135
.L118:
	.loc 1 702 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L161+88
	bl	POC_process_decoder_serial_number
	.loc 1 703 0
	b	.L135
.L119:
	.loc 1 706 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L161+92
	ldr	r2, .L161+96
	ldr	r3, .L161+100
	bl	POC_process_fm_type
	.loc 1 707 0
	b	.L135
.L120:
	.loc 1 710 0
	ldr	r4, [fp, #-52]
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	POC_get_inc_by_for_K_value
	mov	r3, r0	@ float
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L161+44
	ldr	r2, .L161+48	@ float
	ldr	r3, .L161+52	@ float
	bl	process_fl
	.loc 1 711 0
	b	.L135
.L121:
	.loc 1 714 0
	ldr	r4, [fp, #-52]
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	POC_get_inc_by_for_offset
	mov	r3, r0	@ float
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L161+64
	ldr	r2, .L161+68	@ float
	ldr	r3, .L161+72	@ float
	bl	process_fl
	.loc 1 715 0
	b	.L135
.L122:
	.loc 1 718 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L161+92
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	ldr	r2, .L161+100
	bl	POC_process_reed_switch_type
	.loc 1 719 0
	b	.L135
.L123:
	.loc 1 722 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L161+24
	mov	r2, #0
	mov	r3, #1
	bl	process_uns32
	.loc 1 723 0
	b	.L135
.L124:
	.loc 1 726 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L161+104
	bl	POC_process_decoder_serial_number
	.loc 1 727 0
	b	.L135
.L125:
	.loc 1 730 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L161+108
	ldr	r2, .L161+112
	ldr	r3, .L161+116
	bl	POC_process_fm_type
	.loc 1 731 0
	b	.L135
.L126:
	.loc 1 734 0
	ldr	r4, [fp, #-52]
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	POC_get_inc_by_for_K_value
	mov	r3, r0	@ float
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L161+56
	ldr	r2, .L161+48	@ float
	ldr	r3, .L161+52	@ float
	bl	process_fl
	.loc 1 735 0
	b	.L135
.L127:
	.loc 1 738 0
	ldr	r4, [fp, #-52]
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	POC_get_inc_by_for_offset
	mov	r3, r0	@ float
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L161+76
	ldr	r2, .L161+68	@ float
	ldr	r3, .L161+72	@ float
	bl	process_fl
	.loc 1 739 0
	b	.L135
.L128:
	.loc 1 742 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L161+108
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	ldr	r2, .L161+116
	bl	POC_process_reed_switch_type
	.loc 1 743 0
	b	.L135
.L129:
	.loc 1 746 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L161+120
	bl	POC_process_decoder_serial_number
	.loc 1 747 0
	b	.L135
.L130:
	.loc 1 750 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L161+124
	ldr	r2, .L161+128
	ldr	r3, .L161+132
	bl	POC_process_fm_type
	.loc 1 751 0
	b	.L135
.L131:
	.loc 1 754 0
	ldr	r4, [fp, #-52]
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	POC_get_inc_by_for_K_value
	mov	r3, r0	@ float
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L161+60
	ldr	r2, .L161+48	@ float
	ldr	r3, .L161+52	@ float
	bl	process_fl
	.loc 1 755 0
	b	.L135
.L132:
	.loc 1 758 0
	ldr	r4, [fp, #-52]
	ldr	r3, [fp, #-48]
	mov	r0, r3
	bl	POC_get_inc_by_for_offset
	mov	r3, r0	@ float
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L161+80
	ldr	r2, .L161+68	@ float
	ldr	r3, .L161+72	@ float
	bl	process_fl
	.loc 1 759 0
	b	.L135
.L133:
	.loc 1 762 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L161+124
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	ldr	r2, .L161+132
	bl	POC_process_reed_switch_type
	.loc 1 763 0
	b	.L135
.L115:
	.loc 1 766 0
	bl	bad_key_beep
.L135:
	.loc 1 768 0
	bl	Refresh_Screen
	.loc 1 769 0
	b	.L78
.L98:
	.loc 1 773 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	POC_process_NEXT_and_PREV
	.loc 1 774 0
	b	.L78
.L97:
	.loc 1 777 0
	ldr	r3, .L161+136
	ldr	r3, [r3, #0]
	cmp	r3, #12
	beq	.L136
	.loc 1 779 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 825 0
	b	.L78
.L136:
	.loc 1 783 0
	ldr	r3, .L161+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #7
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L138
.L147:
	.word	.L139
	.word	.L140
	.word	.L141
	.word	.L138
	.word	.L142
	.word	.L138
	.word	.L143
	.word	.L144
	.word	.L138
	.word	.L145
	.word	.L138
	.word	.L146
.L139:
	.loc 1 786 0
	mov	r0, #3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 787 0
	b	.L137
.L140:
	.loc 1 790 0
	mov	r0, #4
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 791 0
	b	.L137
.L141:
	.loc 1 794 0
	ldr	r3, .L161+140
	mov	r2, #9
	str	r2, [r3, #0]
	.loc 1 796 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 797 0
	b	.L137
.L143:
	.loc 1 800 0
	mov	r0, #9
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 801 0
	b	.L137
.L144:
	.loc 1 804 0
	mov	r0, #10
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 805 0
	b	.L137
.L142:
	.loc 1 808 0
	ldr	r3, .L161+140
	mov	r2, #11
	str	r2, [r3, #0]
	.loc 1 810 0
	mov	r0, #8
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 811 0
	b	.L137
.L145:
	.loc 1 814 0
	mov	r0, #12
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 815 0
	b	.L137
.L146:
	.loc 1 818 0
	mov	r0, #14
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 819 0
	b	.L137
.L138:
	.loc 1 822 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 825 0
	b	.L78
.L137:
	b	.L78
.L93:
	.loc 1 828 0
	ldr	r3, .L161+136
	ldr	r3, [r3, #0]
	cmp	r3, #12
	beq	.L148
	.loc 1 830 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 860 0
	b	.L78
.L148:
	.loc 1 834 0
	ldr	r3, .L161+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #4
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L150
.L156:
	.word	.L151
	.word	.L150
	.word	.L150
	.word	.L150
	.word	.L152
	.word	.L150
	.word	.L153
	.word	.L150
	.word	.L154
	.word	.L150
	.word	.L150
	.word	.L155
.L151:
	.loc 1 837 0
	mov	r0, #8
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 838 0
	b	.L149
.L152:
	.loc 1 841 0
	ldr	r3, .L161+140
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 842 0
	b	.L149
.L153:
	.loc 1 845 0
	mov	r0, #14
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 846 0
	b	.L149
.L154:
	.loc 1 849 0
	mov	r0, #16
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 850 0
	b	.L149
.L155:
	.loc 1 853 0
	bl	bad_key_beep
	.loc 1 854 0
	b	.L149
.L150:
	.loc 1 857 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 860 0
	b	.L78
.L149:
	b	.L78
.L94:
	.loc 1 863 0
	ldr	r3, .L161+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L160
.L158:
	.loc 1 866 0
	ldr	r0, .L161+144
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 867 0
	mov	r0, r0	@ nop
	.loc 1 872 0
	b	.L78
.L160:
	.loc 1 870 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 872 0
	b	.L78
.L96:
	.loc 1 875 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 876 0
	b	.L78
.L99:
	.loc 1 879 0
	ldr	r0, .L161+144
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 880 0
	b	.L78
.L92:
	.loc 1 883 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L78:
	.loc 1 886 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L162:
	.align	2
.L161:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	741
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_POC_draw_menu
	.word	GuiVar_POCUsage
	.word	GuiVar_POCMVType1
	.word	FDTO_POC_close_flow_meter_dropdown
	.word	FDTO_POC_show_poc_usage_dropdown
	.word	FDTO_POC_show_flow_meter_dropdown
	.word	FDTO_POC_show_master_valve_dropdown
	.word	GuiVar_POCFlowMeterK1
	.word	0
	.word	1145569280
	.word	GuiVar_POCFlowMeterK2
	.word	GuiVar_POCFlowMeterK3
	.word	GuiVar_POCFlowMeterOffset1
	.word	-1054867456
	.word	1092616192
	.word	GuiVar_POCFlowMeterOffset2
	.word	GuiVar_POCFlowMeterOffset3
	.word	GuiVar_POCBypassStages
	.word	GuiVar_POCDecoderSN1
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_POCFMType1IsBermad
	.word	GuiVar_POCReedSwitchType1
	.word	GuiVar_POCDecoderSN2
	.word	GuiVar_POCFlowMeterType2
	.word	GuiVar_POCFMType2IsBermad
	.word	GuiVar_POCReedSwitchType2
	.word	GuiVar_POCDecoderSN3
	.word	GuiVar_POCFlowMeterType3
	.word	GuiVar_POCFMType3IsBermad
	.word	GuiVar_POCReedSwitchType3
	.word	GuiVar_POCType
	.word	g_POC_prev_cursor_pos
	.word	FDTO_POC_return_to_menu
.LFE11:
	.size	POC_process_group, .-POC_process_group
	.section	.text.POC_populate_pointers_of_POCs_for_display,"ax",%progbits
	.align	2
	.global	POC_populate_pointers_of_POCs_for_display
	.type	POC_populate_pointers_of_POCs_for_display, %function
POC_populate_pointers_of_POCs_for_display:
.LFB12:
	.loc 1 892 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI34:
	add	fp, sp, #4
.LCFI35:
	sub	sp, sp, #16
.LCFI36:
	str	r0, [fp, #-20]
	.loc 1 901 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 903 0
	ldr	r0, .L168
	mov	r1, #0
	mov	r2, #48
	bl	memset
	.loc 1 907 0
	ldr	r3, .L168+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L168+8
	ldr	r3, .L168+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 909 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L164
.L167:
	.loc 1 911 0
	ldr	r0, [fp, #-12]
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-16]
	.loc 1 913 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L165
	.loc 1 913 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-16]
	bl	POC_get_show_for_this_poc
	mov	r3, r0
	cmp	r3, #0
	beq	.L165
	.loc 1 916 0 is_stmt 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L166
	.loc 1 920 0
	ldr	r0, [fp, #-16]
	bl	nm_GROUP_get_group_ID
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_usage
	mov	r3, r0
	cmp	r3, #0
	beq	.L165
	.loc 1 921 0 discriminator 1
	ldr	r0, [fp, #-12]
	bl	POC_use_for_budget
	mov	r3, r0
	.loc 1 920 0 discriminator 1
	cmp	r3, #0
	beq	.L165
	.loc 1 923 0
	ldr	r3, .L168
	ldr	r2, [fp, #-8]
	ldr	r1, [fp, #-16]
	str	r1, [r3, r2, asl #2]
	.loc 1 924 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	b	.L165
.L166:
	.loc 1 929 0
	ldr	r3, .L168
	ldr	r2, [fp, #-8]
	ldr	r1, [fp, #-16]
	str	r1, [r3, r2, asl #2]
	.loc 1 930 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L165:
	.loc 1 909 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L164:
	.loc 1 909 0 is_stmt 0 discriminator 1
	bl	POC_get_list_count
	mov	r2, r0
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L167
	.loc 1 935 0 is_stmt 1
	ldr	r3, .L168+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 939 0
	ldr	r3, [fp, #-8]
	.loc 1 940 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L169:
	.align	2
.L168:
	.word	POC_MENU_items
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	907
.LFE12:
	.size	POC_populate_pointers_of_POCs_for_display, .-POC_populate_pointers_of_POCs_for_display
	.section	.text.POC_load_poc_name_into_guivar,"ax",%progbits
	.align	2
	.global	POC_load_poc_name_into_guivar
	.type	POC_load_poc_name_into_guivar, %function
POC_load_poc_name_into_guivar:
.LFB13:
	.loc 1 944 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI37:
	add	fp, sp, #8
.LCFI38:
	sub	sp, sp, #4
.LCFI39:
	mov	r3, r0
	strh	r3, [fp, #-12]	@ movhi
	.loc 1 945 0
	ldr	r3, .L173
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L173+4
	ldr	r3, .L173+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 947 0
	ldrsh	r4, [fp, #-12]
	bl	POC_get_list_count
	mov	r3, r0
	cmp	r4, r3
	bcs	.L171
	.loc 1 949 0
	ldrsh	r2, [fp, #-12]
	ldr	r3, .L173+12
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r0, .L173+16
	mov	r1, r3
	mov	r2, #48
	bl	strlcpy
	b	.L172
.L171:
	.loc 1 953 0
	ldr	r0, .L173+4
	ldr	r1, .L173+20
	bl	Alert_group_not_found_with_filename
.L172:
	.loc 1 956 0
	ldr	r3, .L173
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 957 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L174:
	.align	2
.L173:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	945
	.word	POC_MENU_items
	.word	GuiVar_itmGroupName
	.word	953
.LFE13:
	.size	POC_load_poc_name_into_guivar, .-POC_load_poc_name_into_guivar
	.section	.text.POC_get_ptr_to_physically_available_poc,"ax",%progbits
	.align	2
	.global	POC_get_ptr_to_physically_available_poc
	.type	POC_get_ptr_to_physically_available_poc, %function
POC_get_ptr_to_physically_available_poc:
.LFB14:
	.loc 1 961 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI40:
	add	fp, sp, #0
.LCFI41:
	sub	sp, sp, #4
.LCFI42:
	str	r0, [fp, #-4]
	.loc 1 962 0
	ldr	r3, .L176
	ldr	r2, [fp, #-4]
	ldr	r3, [r3, r2, asl #2]
	.loc 1 963 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L177:
	.align	2
.L176:
	.word	POC_MENU_items
.LFE14:
	.size	POC_get_ptr_to_physically_available_poc, .-POC_get_ptr_to_physically_available_poc
	.section	.text.FDTO_POC_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_POC_return_to_menu, %function
FDTO_POC_return_to_menu:
.LFB15:
	.loc 1 984 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI43:
	add	fp, sp, #4
.LCFI44:
	.loc 1 985 0
	ldr	r3, .L179
	ldr	r0, .L179+4
	mov	r1, r3
	bl	FDTO_GROUP_return_to_menu
	.loc 1 986 0
	ldmfd	sp!, {fp, pc}
.L180:
	.align	2
.L179:
	.word	POC_extract_and_store_changes_from_GuiVars
	.word	g_POC_editing_group
.LFE15:
	.size	FDTO_POC_return_to_menu, .-FDTO_POC_return_to_menu
	.section	.text.FDTO_POC_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_POC_draw_menu
	.type	FDTO_POC_draw_menu, %function
FDTO_POC_draw_menu:
.LFB16:
	.loc 1 1008 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI45:
	add	fp, sp, #4
.LCFI46:
	sub	sp, sp, #20
.LCFI47:
	str	r0, [fp, #-20]
	.loc 1 1017 0
	ldr	r3, .L186
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L186+4
	ldr	r3, .L186+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1020 0
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	str	r0, [fp, #-12]
	.loc 1 1022 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L182
	.loc 1 1024 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1026 0
	ldr	r3, .L186+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1028 0
	ldr	r3, .L186+16
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1030 0
	ldr	r3, .L186+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1032 0
	ldr	r3, .L186+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_ptr_to_physically_available_poc
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L186+24
	str	r2, [r3, #0]
	.loc 1 1034 0
	ldr	r3, .L186+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_copy_group_into_guivars
	.loc 1 1040 0
	ldr	r3, .L186+28
	mov	r2, #9
	str	r2, [r3, #0]
	b	.L183
.L182:
	.loc 1 1044 0
	ldr	r3, .L186+32
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
	.loc 1 1046 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_GetTopLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L186+12
	str	r2, [r3, #0]
.L183:
	.loc 1 1049 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #47
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 1053 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	.loc 1 1055 0
	bl	POC_get_menu_index_for_displayed_poc
	str	r0, [fp, #-16]
	.loc 1 1057 0
	ldr	r3, .L186+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L184
	.loc 1 1062 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r2, .L186+12
	ldr	r2, [r2, #0]
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	mov	r2, r2, asl #16
	mov	r2, r2, asr #16
	str	r2, [sp, #0]
	mov	r0, #0
	ldr	r1, .L186+36
	mov	r2, r3
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	.loc 1 1063 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	mov	r1, r3
	bl	GuiLib_ScrollBox_SetIndicator
	.loc 1 1064 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
	b	.L185
.L184:
	.loc 1 1068 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r1, .L186+12
	ldr	r1, [r1, #0]
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r1, r1, asl #16
	mov	r1, r1, asr #16
	str	r1, [sp, #0]
	mov	r0, #0
	ldr	r1, .L186+36
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L185:
	.loc 1 1071 0
	ldr	r3, .L186
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1073 0
	bl	GuiLib_Refresh
	.loc 1 1074 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L187:
	.align	2
.L186:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	1017
	.word	g_POC_top_line
	.word	g_POC_editing_group
	.word	g_POC_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	g_POC_prev_cursor_pos
	.word	GuiLib_ActiveCursorFieldNo
	.word	POC_load_poc_name_into_guivar
.LFE16:
	.size	FDTO_POC_draw_menu, .-FDTO_POC_draw_menu
	.section	.text.POC_process_menu,"ax",%progbits
	.align	2
	.global	POC_process_menu
	.type	POC_process_menu, %function
POC_process_menu:
.LFB17:
	.loc 1 1093 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI48:
	add	fp, sp, #4
.LCFI49:
	sub	sp, sp, #8
.LCFI50:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 1094 0
	ldr	r3, .L197
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L189
	.loc 1 1096 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	POC_process_group
	b	.L188
.L189:
	.loc 1 1100 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	bhi	.L191
	mov	r2, #1
	mov	r3, r2, asl r3
	and	r2, r3, #1114112
	cmp	r2, #0
	bne	.L194
	and	r2, r3, #17
	cmp	r2, #0
	bne	.L192
	and	r3, r3, #12
	cmp	r3, #0
	beq	.L191
.L193:
	.loc 1 1104 0
	bl	good_key_beep
	.loc 1 1106 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L197+4
	str	r2, [r3, #0]
	.loc 1 1108 0
	ldr	r3, .L197+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_ptr_to_physically_available_poc
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L197+8
	str	r2, [r3, #0]
	.loc 1 1110 0
	ldr	r3, .L197
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1112 0
	ldr	r3, .L197+12
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1114 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 1115 0
	b	.L188
.L194:
	.loc 1 1121 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	bne	.L195
	.loc 1 1123 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L192
.L195:
	.loc 1 1127 0
	mov	r3, #4
	str	r3, [fp, #-12]
.L192:
	.loc 1 1135 0
	ldr	r3, [fp, #-12]
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	.loc 1 1137 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L197+4
	str	r2, [r3, #0]
	.loc 1 1139 0
	ldr	r3, .L197+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_ptr_to_physically_available_poc
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L197+8
	str	r2, [r3, #0]
	.loc 1 1141 0
	ldr	r3, .L197+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_copy_group_into_guivars
	.loc 1 1147 0
	ldr	r3, .L197+16
	mov	r2, #9
	str	r2, [r3, #0]
	.loc 1 1153 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 1154 0
	b	.L188
.L191:
	.loc 1 1157 0
	ldr	r3, [fp, #-12]
	cmp	r3, #67
	bne	.L196
	.loc 1 1159 0
	ldr	r3, .L197+20
	ldr	r2, [r3, #0]
	ldr	r0, .L197+24
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L197+28
	str	r2, [r3, #0]
.L196:
	.loc 1 1162 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L188:
	.loc 1 1165 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L198:
	.align	2
.L197:
	.word	g_POC_editing_group
	.word	g_POC_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_POC_prev_cursor_pos
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE17:
	.size	POC_process_menu, .-POC_process_menu
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI13-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI16-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI19-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI22-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI25-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI26-.LCFI25
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI28-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI31-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI32-.LCFI31
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI34-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI35-.LCFI34
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI37-.LFB13
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI40-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI43-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI44-.LCFI43
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI45-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI48-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/poc.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_poc.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xb4d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF122
	.byte	0x1
	.4byte	.LASF123
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x67
	.4byte	0x70
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x96
	.uleb128 0x6
	.4byte	0x9d
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x35
	.4byte	0x9d
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x4
	.byte	0x57
	.4byte	0xa4
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x5
	.byte	0x4c
	.4byte	0xb8
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0xde
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0x103
	.uleb128 0xc
	.4byte	.LASF17
	.byte	0x6
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF18
	.byte	0x6
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x6
	.byte	0x82
	.4byte	0xde
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x7
	.byte	0xda
	.4byte	0x119
	.uleb128 0xd
	.4byte	.LASF20
	.byte	0x1
	.uleb128 0x9
	.4byte	0x53
	.4byte	0x12f
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x2
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.byte	0x8
	.byte	0x16
	.4byte	0x146
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x8
	.byte	0x18
	.4byte	0xa4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF22
	.byte	0x8
	.byte	0x1a
	.4byte	0x12f
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF23
	.uleb128 0x9
	.4byte	0x53
	.4byte	0x168
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF24
	.uleb128 0xb
	.byte	0x24
	.byte	0x9
	.byte	0x78
	.4byte	0x1f6
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x9
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x9
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x9
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x9
	.byte	0x88
	.4byte	0x207
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x9
	.byte	0x8d
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x9
	.byte	0x92
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x9
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF32
	.byte	0x9
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x9
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	0x202
	.uleb128 0xf
	.4byte	0x202
	.byte	0
	.uleb128 0x10
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1f6
	.uleb128 0xe
	.byte	0x1
	.4byte	0x219
	.uleb128 0xf
	.4byte	0x103
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x20d
	.uleb128 0x3
	.4byte	.LASF34
	.byte	0x9
	.byte	0x9e
	.4byte	0x16f
	.uleb128 0x11
	.4byte	.LASF35
	.byte	0x1
	.byte	0x6d
	.byte	0x1
	.4byte	0x151
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x262
	.uleb128 0x12
	.4byte	.LASF37
	.byte	0x1
	.byte	0x6d
	.4byte	0x262
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x6f
	.4byte	0x151
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x10
	.4byte	0x53
	.uleb128 0x11
	.4byte	.LASF36
	.byte	0x1
	.byte	0x86
	.byte	0x1
	.4byte	0x151
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x29f
	.uleb128 0x12
	.4byte	.LASF37
	.byte	0x1
	.byte	0x86
	.4byte	0x262
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x88
	.4byte	0x151
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x14
	.4byte	.LASF44
	.byte	0x1
	.byte	0x9b
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x2fc
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.byte	0x9b
	.4byte	0x262
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF39
	.byte	0x1
	.byte	0x9b
	.4byte	0x2fc
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x15
	.4byte	.LASF40
	.byte	0x1
	.byte	0x9d
	.4byte	0x302
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.4byte	.LASF41
	.byte	0x1
	.byte	0x9f
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.ascii	"i\000"
	.byte	0x1
	.byte	0xa1
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x53
	.uleb128 0x5
	.byte	0x4
	.4byte	0x10e
	.uleb128 0x16
	.4byte	.LASF42
	.byte	0x1
	.byte	0xc6
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x16
	.4byte	.LASF43
	.byte	0x1
	.byte	0xcc
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0x1
	.byte	0xe9
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x357
	.uleb128 0x15
	.4byte	.LASF46
	.byte	0x1
	.byte	0xeb
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x10d
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x3ad
	.uleb128 0x18
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x10f
	.4byte	0x2fc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x111
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x18
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x113
	.4byte	0x2fc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x115
	.4byte	0x2fc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x17
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x167
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x412
	.uleb128 0x19
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x167
	.4byte	0x262
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x167
	.4byte	0x2fc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x167
	.4byte	0x2fc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x167
	.4byte	0x2fc
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x169
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x198
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x459
	.uleb128 0x19
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x198
	.4byte	0x262
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x198
	.4byte	0x262
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x198
	.4byte	0x2fc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x1a9
	.byte	0x1
	.4byte	0x53
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x4a2
	.uleb128 0x18
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x1ad
	.4byte	0x302
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1af
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1b
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x1b1
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x17
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x1c9
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x4da
	.uleb128 0x19
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x1c9
	.4byte	0x262
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x1cb
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x236
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x512
	.uleb128 0x19
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x236
	.4byte	0x512
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x1b
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x238
	.4byte	0x21f
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x10
	.4byte	0x103
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x37b
	.byte	0x1
	.4byte	0x53
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x570
	.uleb128 0x19
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x37b
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x18
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x37d
	.4byte	0x302
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x37f
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1b
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x381
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1c
	.byte	0x1
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x3af
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x59a
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x3af
	.4byte	0x202
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x3c0
	.byte	0x1
	.4byte	0x302
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x5c8
	.uleb128 0x19
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x3c0
	.4byte	0x262
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1d
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x3d7
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.uleb128 0x1c
	.byte	0x1
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x3ef
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x634
	.uleb128 0x19
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x3ef
	.4byte	0x634
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x18
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x3f1
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x3f3
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x3f5
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.4byte	0x85
	.uleb128 0x1c
	.byte	0x1
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x444
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x663
	.uleb128 0x19
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x444
	.4byte	0x103
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF77
	.byte	0xa
	.2byte	0x168
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x25
	.4byte	0x681
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x30
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF78
	.byte	0xa
	.2byte	0x26a
	.4byte	0x671
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF79
	.byte	0xa
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF80
	.byte	0xa
	.2byte	0x341
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF81
	.byte	0xa
	.2byte	0x351
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF82
	.byte	0xa
	.2byte	0x352
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF83
	.byte	0xa
	.2byte	0x353
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF84
	.byte	0xa
	.2byte	0x354
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF85
	.byte	0xa
	.2byte	0x355
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF86
	.byte	0xa
	.2byte	0x356
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF87
	.byte	0xa
	.2byte	0x357
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF88
	.byte	0xa
	.2byte	0x358
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF89
	.byte	0xa
	.2byte	0x359
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF90
	.byte	0xa
	.2byte	0x35a
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF91
	.byte	0xa
	.2byte	0x35b
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF92
	.byte	0xa
	.2byte	0x35c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF93
	.byte	0xa
	.2byte	0x35d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF94
	.byte	0xa
	.2byte	0x35e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF95
	.byte	0xa
	.2byte	0x35f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF96
	.byte	0xa
	.2byte	0x360
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF97
	.byte	0xa
	.2byte	0x362
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF98
	.byte	0xa
	.2byte	0x37d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF99
	.byte	0xa
	.2byte	0x37e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF100
	.byte	0xa
	.2byte	0x37f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF101
	.byte	0xa
	.2byte	0x380
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF102
	.byte	0xa
	.2byte	0x381
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF103
	.byte	0xb
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF104
	.byte	0xb
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF105
	.byte	0xc
	.byte	0x30
	.4byte	0x80c
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x15
	.4byte	.LASF106
	.byte	0xc
	.byte	0x34
	.4byte	0x822
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x15
	.4byte	.LASF107
	.byte	0xc
	.byte	0x36
	.4byte	0x838
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x15
	.4byte	.LASF108
	.byte	0xc
	.byte	0x38
	.4byte	0x84e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x9
	.4byte	0x146
	.4byte	0x863
	.uleb128 0xa
	.4byte	0x9d
	.byte	0xb
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF109
	.byte	0x8
	.byte	0x1c
	.4byte	0x853
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF110
	.byte	0x8
	.byte	0x1e
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF111
	.byte	0x8
	.byte	0x20
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF112
	.byte	0x8
	.byte	0x22
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF113
	.byte	0xd
	.byte	0x63
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF114
	.byte	0xd
	.byte	0x65
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF115
	.byte	0xe
	.byte	0x33
	.4byte	0x8c2
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x10
	.4byte	0x11f
	.uleb128 0x15
	.4byte	.LASF116
	.byte	0xe
	.byte	0x3f
	.4byte	0x8d8
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x10
	.4byte	0x158
	.uleb128 0x1f
	.4byte	.LASF117
	.byte	0xf
	.byte	0x78
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF118
	.byte	0xf
	.byte	0xc9
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x21f
	.4byte	0x907
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x31
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF119
	.byte	0x9
	.byte	0xac
	.4byte	0x8f7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF120
	.byte	0x9
	.byte	0xae
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF121
	.byte	0x1
	.byte	0x62
	.4byte	0x53
	.byte	0x5
	.byte	0x3
	.4byte	g_POC_prev_cursor_pos
	.uleb128 0x1e
	.4byte	.LASF77
	.byte	0xa
	.2byte	0x168
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF78
	.byte	0xa
	.2byte	0x26a
	.4byte	0x671
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF79
	.byte	0xa
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF80
	.byte	0xa
	.2byte	0x341
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF81
	.byte	0xa
	.2byte	0x351
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF82
	.byte	0xa
	.2byte	0x352
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF83
	.byte	0xa
	.2byte	0x353
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF84
	.byte	0xa
	.2byte	0x354
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF85
	.byte	0xa
	.2byte	0x355
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF86
	.byte	0xa
	.2byte	0x356
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF87
	.byte	0xa
	.2byte	0x357
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF88
	.byte	0xa
	.2byte	0x358
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF89
	.byte	0xa
	.2byte	0x359
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF90
	.byte	0xa
	.2byte	0x35a
	.4byte	0x151
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF91
	.byte	0xa
	.2byte	0x35b
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF92
	.byte	0xa
	.2byte	0x35c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF93
	.byte	0xa
	.2byte	0x35d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF94
	.byte	0xa
	.2byte	0x35e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF95
	.byte	0xa
	.2byte	0x35f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF96
	.byte	0xa
	.2byte	0x360
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF97
	.byte	0xa
	.2byte	0x362
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF98
	.byte	0xa
	.2byte	0x37d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF99
	.byte	0xa
	.2byte	0x37e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF100
	.byte	0xa
	.2byte	0x37f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF101
	.byte	0xa
	.2byte	0x380
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF102
	.byte	0xa
	.2byte	0x381
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF103
	.byte	0xb
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF104
	.byte	0xb
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF109
	.byte	0x1
	.byte	0x5e
	.4byte	0x853
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	POC_MENU_items
	.uleb128 0x20
	.4byte	.LASF110
	.byte	0x1
	.byte	0x53
	.4byte	0x53
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_POC_top_line
	.uleb128 0x20
	.4byte	.LASF111
	.byte	0x1
	.byte	0x55
	.4byte	0x85
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_POC_editing_group
	.uleb128 0x20
	.4byte	.LASF112
	.byte	0x1
	.byte	0x5c
	.4byte	0x53
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_POC_current_list_item_index
	.uleb128 0x1f
	.4byte	.LASF113
	.byte	0xd
	.byte	0x63
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF114
	.byte	0xd
	.byte	0x65
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF117
	.byte	0xf
	.byte	0x78
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF118
	.byte	0xf
	.byte	0xc9
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF119
	.byte	0x9
	.byte	0xac
	.4byte	0x8f7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF120
	.byte	0x9
	.byte	0xae
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI14
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI17
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI20
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI23
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI26
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI29
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI32
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI35
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI38
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI41
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI44
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI46
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI49
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xa4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF109:
	.ascii	"POC_MENU_items\000"
.LASF117:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF27:
	.ascii	"_03_structure_to_draw\000"
.LASF26:
	.ascii	"_02_menu\000"
.LASF94:
	.ascii	"GuiVar_POCFMType1IsBermad\000"
.LASF70:
	.ascii	"FDTO_POC_return_to_menu\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF37:
	.ascii	"prepeats\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF17:
	.ascii	"keycode\000"
.LASF40:
	.ascii	"lpoc\000"
.LASF10:
	.ascii	"long long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF72:
	.ascii	"FDTO_POC_draw_menu\000"
.LASF33:
	.ascii	"_08_screen_to_draw\000"
.LASF101:
	.ascii	"GuiVar_POCType\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF22:
	.ascii	"POC_MENU_SCROLL_BOX_ITEM\000"
.LASF13:
	.ascii	"long int\000"
.LASF95:
	.ascii	"GuiVar_POCFMType2IsBermad\000"
.LASF44:
	.ascii	"POC_process_decoder_serial_number\000"
.LASF34:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF62:
	.ascii	"pkey_event\000"
.LASF120:
	.ascii	"screen_history_index\000"
.LASF24:
	.ascii	"double\000"
.LASF63:
	.ascii	"POC_get_menu_index_for_displayed_poc\000"
.LASF91:
	.ascii	"GuiVar_POCFlowMeterType1\000"
.LASF92:
	.ascii	"GuiVar_POCFlowMeterType2\000"
.LASF108:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF82:
	.ascii	"GuiVar_POCDecoderSN1\000"
.LASF83:
	.ascii	"GuiVar_POCDecoderSN2\000"
.LASF84:
	.ascii	"GuiVar_POCDecoderSN3\000"
.LASF55:
	.ascii	"prs_type_guivar_ptr\000"
.LASF60:
	.ascii	"lmenu_index_for_displayed_poc\000"
.LASF39:
	.ascii	"var_ptr\000"
.LASF54:
	.ascii	"pfm_type_is_bermad_guivar_ptr\000"
.LASF25:
	.ascii	"_01_command\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF79:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF53:
	.ascii	"pfm_type_guivar_ptr\000"
.LASF114:
	.ascii	"g_GROUP_list_item_index\000"
.LASF8:
	.ascii	"INT_32\000"
.LASF12:
	.ascii	"long unsigned int\000"
.LASF96:
	.ascii	"GuiVar_POCFMType3IsBermad\000"
.LASF29:
	.ascii	"key_process_func_ptr\000"
.LASF97:
	.ascii	"GuiVar_POCMVType1\000"
.LASF43:
	.ascii	"FDTO_POC_show_master_valve_dropdown\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF122:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF56:
	.ascii	"lprevious_fm_type\000"
.LASF31:
	.ascii	"_06_u32_argument1\000"
.LASF103:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF68:
	.ascii	"POC_get_ptr_to_physically_available_poc\000"
.LASF28:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF38:
	.ascii	"pkeycode\000"
.LASF67:
	.ascii	"pindex_0_i16\000"
.LASF15:
	.ascii	"xQueueHandle\000"
.LASF106:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF81:
	.ascii	"GuiVar_POCBypassStages\000"
.LASF85:
	.ascii	"GuiVar_POCFlowMeterK1\000"
.LASF123:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_poc.c\000"
.LASF74:
	.ascii	"lpocs_in_use\000"
.LASF121:
	.ascii	"g_POC_prev_cursor_pos\000"
.LASF111:
	.ascii	"g_POC_editing_group\000"
.LASF19:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF105:
	.ascii	"GuiFont_LanguageActive\000"
.LASF48:
	.ascii	"fm_type_ptr\000"
.LASF80:
	.ascii	"GuiVar_POCBoxIndex\000"
.LASF58:
	.ascii	"fm_type_guivar\000"
.LASF59:
	.ascii	"POC_process_NEXT_and_PREV\000"
.LASF75:
	.ascii	"lcursor_to_select\000"
.LASF23:
	.ascii	"float\000"
.LASF69:
	.ascii	"pindex_0\000"
.LASF110:
	.ascii	"g_POC_top_line\000"
.LASF16:
	.ascii	"xSemaphoreHandle\000"
.LASF78:
	.ascii	"GuiVar_itmGroupName\000"
.LASF36:
	.ascii	"POC_get_inc_by_for_offset\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF49:
	.ascii	"previous_fm_type\000"
.LASF104:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF93:
	.ascii	"GuiVar_POCFlowMeterType3\000"
.LASF45:
	.ascii	"FDTO_POC_show_flow_meter_dropdown\000"
.LASF4:
	.ascii	"short int\000"
.LASF61:
	.ascii	"POC_process_group\000"
.LASF66:
	.ascii	"lavailable_POCs\000"
.LASF57:
	.ascii	"POC_process_reed_switch_type\000"
.LASF42:
	.ascii	"FDTO_POC_show_poc_usage_dropdown\000"
.LASF14:
	.ascii	"portTickType\000"
.LASF21:
	.ascii	"poc_ptr\000"
.LASF119:
	.ascii	"ScreenHistory\000"
.LASF102:
	.ascii	"GuiVar_POCUsage\000"
.LASF47:
	.ascii	"FDTO_POC_close_flow_meter_dropdown\000"
.LASF71:
	.ascii	"POC_load_poc_name_into_guivar\000"
.LASF64:
	.ascii	"POC_populate_pointers_of_POCs_for_display\000"
.LASF65:
	.ascii	"flag_not_used\000"
.LASF73:
	.ascii	"pcomplete_redraw\000"
.LASF76:
	.ascii	"POC_process_menu\000"
.LASF0:
	.ascii	"char\000"
.LASF113:
	.ascii	"g_GROUP_ID\000"
.LASF88:
	.ascii	"GuiVar_POCFlowMeterOffset1\000"
.LASF89:
	.ascii	"GuiVar_POCFlowMeterOffset2\000"
.LASF90:
	.ascii	"GuiVar_POCFlowMeterOffset3\000"
.LASF46:
	.ascii	"lactive_line\000"
.LASF35:
	.ascii	"POC_get_inc_by_for_K_value\000"
.LASF107:
	.ascii	"GuiFont_DecimalChar\000"
.LASF18:
	.ascii	"repeats\000"
.LASF118:
	.ascii	"list_poc_recursive_MUTEX\000"
.LASF116:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF41:
	.ascii	"lindex\000"
.LASF77:
	.ascii	"GuiVar_ComboBox_Y1\000"
.LASF32:
	.ascii	"_07_u32_argument2\000"
.LASF30:
	.ascii	"_04_func_ptr\000"
.LASF86:
	.ascii	"GuiVar_POCFlowMeterK2\000"
.LASF87:
	.ascii	"GuiVar_POCFlowMeterK3\000"
.LASF51:
	.ascii	"fm_type_is_bermad_guivar_ptr\000"
.LASF52:
	.ascii	"POC_process_fm_type\000"
.LASF112:
	.ascii	"g_POC_current_list_item_index\000"
.LASF50:
	.ascii	"rs_type_ptr\000"
.LASF115:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF98:
	.ascii	"GuiVar_POCReedSwitchType1\000"
.LASF99:
	.ascii	"GuiVar_POCReedSwitchType2\000"
.LASF100:
	.ascii	"GuiVar_POCReedSwitchType3\000"
.LASF20:
	.ascii	"POC_GROUP_STRUCT\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
