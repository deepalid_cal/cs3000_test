	.file	"e_numeric_keypad.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed,"aw",%nobits
	.align	2
	.type	g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed, %object
	.size	g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed, 4
g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed:
	.space	4
	.section	.bss.g_NUMERIC_KEYPAD_type,"aw",%nobits
	.align	2
	.type	g_NUMERIC_KEYPAD_type, %object
	.size	g_NUMERIC_KEYPAD_type, 4
g_NUMERIC_KEYPAD_type:
	.space	4
	.section	.bss.g_NUMERIC_KEYPAD_num_decimals_to_display,"aw",%nobits
	.align	2
	.type	g_NUMERIC_KEYPAD_num_decimals_to_display, %object
	.size	g_NUMERIC_KEYPAD_num_decimals_to_display, 4
g_NUMERIC_KEYPAD_num_decimals_to_display:
	.space	4
	.section	.bss.ptr_to_uint32_value,"aw",%nobits
	.align	2
	.type	ptr_to_uint32_value, %object
	.size	ptr_to_uint32_value, 4
ptr_to_uint32_value:
	.space	4
	.section	.bss.min_uint32_value,"aw",%nobits
	.align	2
	.type	min_uint32_value, %object
	.size	min_uint32_value, 4
min_uint32_value:
	.space	4
	.section	.bss.max_uint32_value,"aw",%nobits
	.align	2
	.type	max_uint32_value, %object
	.size	max_uint32_value, 4
max_uint32_value:
	.space	4
	.section	.bss.ptr_to_int32_value,"aw",%nobits
	.align	2
	.type	ptr_to_int32_value, %object
	.size	ptr_to_int32_value, 4
ptr_to_int32_value:
	.space	4
	.section	.bss.min_int32_value,"aw",%nobits
	.align	2
	.type	min_int32_value, %object
	.size	min_int32_value, 4
min_int32_value:
	.space	4
	.section	.bss.max_int32_value,"aw",%nobits
	.align	2
	.type	max_int32_value, %object
	.size	max_int32_value, 4
max_int32_value:
	.space	4
	.section	.bss.ptr_to_float_value,"aw",%nobits
	.align	2
	.type	ptr_to_float_value, %object
	.size	ptr_to_float_value, 4
ptr_to_float_value:
	.space	4
	.section	.bss.min_float_value,"aw",%nobits
	.align	2
	.type	min_float_value, %object
	.size	min_float_value, 4
min_float_value:
	.space	4
	.section	.bss.max_float_value,"aw",%nobits
	.align	2
	.type	max_float_value, %object
	.size	max_float_value, 4
max_float_value:
	.space	4
	.section	.bss.ptr_to_double_value,"aw",%nobits
	.align	2
	.type	ptr_to_double_value, %object
	.size	ptr_to_double_value, 4
ptr_to_double_value:
	.space	4
	.section	.bss.min_double_value,"aw",%nobits
	.align	2
	.type	min_double_value, %object
	.size	min_double_value, 8
min_double_value:
	.space	8
	.section	.bss.max_double_value,"aw",%nobits
	.align	2
	.type	max_double_value, %object
	.size	max_double_value, 8
max_double_value:
	.space	8
	.section .rodata
	.align	2
.LC0:
	.ascii	"%d\000"
	.align	2
.LC1:
	.ascii	"-\000"
	.align	2
.LC2:
	.ascii	"%*.f\000"
	.section	.text.validate_upper_range,"ax",%progbits
	.align	2
	.type	validate_upper_range, %function
validate_upper_range:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_numeric_keypad.c"
	.loc 1 117 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI0:
	add	fp, sp, #8
.LCFI1:
	sub	sp, sp, #16
.LCFI2:
	str	r0, [fp, #-16]
	.loc 1 123 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 127 0
	ldr	r3, .L11
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L2
	.loc 1 129 0
	ldr	r0, .L11+4
	bl	atoi
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L11+8
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L3
	.loc 1 131 0
	ldr	r3, .L11+8
	ldr	r3, [r3, #0]
	ldr	r0, .L11+4
	mov	r1, #11
	ldr	r2, .L11+12
	bl	snprintf
	.loc 1 133 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L3
.L2:
	.loc 1 136 0
	ldr	r3, .L11
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L4
	.loc 1 138 0
	ldr	r0, .L11+4
	bl	atoi
	mov	r2, r0
	ldr	r3, .L11+16
	ldr	r3, [r3, #0]
	cmp	r2, r3
	ble	.L5
	.loc 1 140 0
	ldr	r3, .L11+16
	ldr	r3, [r3, #0]
	ldr	r0, .L11+4
	mov	r1, #11
	ldr	r2, .L11+12
	bl	snprintf
	.loc 1 142 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L3
.L5:
	.loc 1 144 0
	ldr	r0, .L11+4
	ldr	r1, .L11+20
	bl	strstr
	mov	r3, r0
	cmp	r3, #0
	beq	.L3
	.loc 1 144 0 is_stmt 0 discriminator 1
	ldr	r0, .L11+4
	bl	atoi
	mov	r2, r0
	ldr	r3, .L11+24
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bge	.L3
	.loc 1 146 0 is_stmt 1
	ldr	r3, .L11+24
	ldr	r3, [r3, #0]
	ldr	r0, .L11+4
	mov	r1, #11
	ldr	r2, .L11+12
	bl	snprintf
	.loc 1 148 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L3
.L4:
	.loc 1 151 0
	ldr	r3, .L11
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L6
	.loc 1 153 0
	ldr	r0, .L11+4
	bl	atof
	fmdrr	d6, r0, r1
	ldr	r3, .L11+28
	flds	s15, [r3, #0]
	fcvtds	d7, s15
	fcmped	d6, d7
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L7
	.loc 1 155 0
	ldr	r3, .L11+32
	ldr	r3, [r3, #0]
	ldr	r2, .L11+28
	flds	s15, [r2, #0]
	fcvtds	d7, s15
	fstd	d7, [sp, #0]
	ldr	r0, .L11+4
	mov	r1, #11
	ldr	r2, .L11+36
	bl	snprintf
	.loc 1 157 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L3
.L7:
	.loc 1 159 0
	ldr	r0, .L11+4
	ldr	r1, .L11+20
	bl	strstr
	mov	r3, r0
	cmp	r3, #0
	beq	.L3
	.loc 1 159 0 is_stmt 0 discriminator 1
	ldr	r0, .L11+4
	bl	atof
	fmdrr	d6, r0, r1
	ldr	r3, .L11+40
	flds	s15, [r3, #0]
	fcvtds	d7, s15
	fcmped	d6, d7
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L3
	.loc 1 161 0 is_stmt 1
	ldr	r3, .L11+32
	ldr	r3, [r3, #0]
	ldr	r2, .L11+40
	flds	s15, [r2, #0]
	fcvtds	d7, s15
	fstd	d7, [sp, #0]
	ldr	r0, .L11+4
	mov	r1, #11
	ldr	r2, .L11+36
	bl	snprintf
	.loc 1 163 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L3
.L6:
	.loc 1 168 0
	ldr	r0, .L11+4
	bl	atof
	fmdrr	d6, r0, r1
	ldr	r3, .L11+44
	fldd	d7, [r3, #0]
	fcmped	d6, d7
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L8
	.loc 1 170 0
	ldr	r3, .L11+32
	ldr	ip, [r3, #0]
	ldr	r3, .L11+44
	ldmia	r3, {r3-r4}
	stmia	sp, {r3-r4}
	ldr	r0, .L11+4
	mov	r1, #11
	ldr	r2, .L11+36
	mov	r3, ip
	bl	snprintf
	.loc 1 172 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L3
.L8:
	.loc 1 174 0
	ldr	r0, .L11+4
	ldr	r1, .L11+20
	bl	strstr
	mov	r3, r0
	cmp	r3, #0
	beq	.L3
	.loc 1 174 0 is_stmt 0 discriminator 1
	ldr	r0, .L11+4
	bl	atof
	fmdrr	d6, r0, r1
	ldr	r3, .L11+48
	fldd	d7, [r3, #0]
	fcmped	d6, d7
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L3
	.loc 1 176 0 is_stmt 1
	ldr	r3, .L11+32
	ldr	ip, [r3, #0]
	ldr	r3, .L11+48
	ldmia	r3, {r3-r4}
	stmia	sp, {r3-r4}
	ldr	r0, .L11+4
	mov	r1, #11
	ldr	r2, .L11+36
	mov	r3, ip
	bl	snprintf
	.loc 1 178 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L3:
	.loc 1 184 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L1
	.loc 1 186 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L10
	.loc 1 188 0
	bl	bad_key_beep
	b	.L1
.L10:
	.loc 1 192 0
	bl	good_key_beep
.L1:
	.loc 1 195 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L12:
	.align	2
.L11:
	.word	g_NUMERIC_KEYPAD_type
	.word	GuiVar_NumericKeypadValue
	.word	max_uint32_value
	.word	.LC0
	.word	max_int32_value
	.word	.LC1
	.word	min_int32_value
	.word	max_float_value
	.word	g_NUMERIC_KEYPAD_num_decimals_to_display
	.word	.LC2
	.word	min_float_value
	.word	max_double_value
	.word	min_double_value
.LFE0:
	.size	validate_upper_range, .-validate_upper_range
	.section	.text.validate_lower_range,"ax",%progbits
	.align	2
	.type	validate_lower_range, %function
validate_lower_range:
.LFB1:
	.loc 1 212 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI3:
	add	fp, sp, #12
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-20]
	.loc 1 217 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 221 0
	ldr	r3, .L20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L14
	.loc 1 223 0
	ldr	r3, .L20+4
	ldr	r4, [r3, #0]
	ldr	r0, .L20+8
	bl	atoi
	mov	r3, r0
	str	r3, [r4, #0]
	.loc 1 225 0
	ldr	r3, .L20+4
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L20+12
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcs	.L15
	.loc 1 227 0
	ldr	r3, .L20+4
	ldr	r3, [r3, #0]
	ldr	r2, .L20+12
	ldr	r2, [r2, #0]
	str	r2, [r3, #0]
	.loc 1 229 0
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L15
.L14:
	.loc 1 232 0
	ldr	r3, .L20
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L16
	.loc 1 234 0
	ldr	r3, .L20+16
	ldr	r4, [r3, #0]
	ldr	r0, .L20+8
	bl	atoi
	mov	r3, r0
	str	r3, [r4, #0]
	.loc 1 236 0
	ldr	r3, .L20+16
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldr	r3, .L20+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bge	.L15
	.loc 1 238 0
	ldr	r3, .L20+16
	ldr	r3, [r3, #0]
	ldr	r2, .L20+20
	ldr	r2, [r2, #0]
	str	r2, [r3, #0]
	.loc 1 240 0
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L15
.L16:
	.loc 1 243 0
	ldr	r3, .L20
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L17
	.loc 1 245 0
	ldr	r3, .L20+24
	ldr	r4, [r3, #0]
	ldr	r0, .L20+8
	bl	atof
	fmdrr	d7, r0, r1
	fcvtsd	s15, d7
	fsts	s15, [r4, #0]
	.loc 1 247 0
	ldr	r3, .L20+24
	ldr	r3, [r3, #0]
	flds	s14, [r3, #0]
	ldr	r3, .L20+28
	flds	s15, [r3, #0]
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L15
	.loc 1 249 0
	ldr	r3, .L20+24
	ldr	r3, [r3, #0]
	ldr	r2, .L20+28
	ldr	r2, [r2, #0]	@ float
	str	r2, [r3, #0]	@ float
	.loc 1 251 0
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L15
.L17:
	.loc 1 256 0
	ldr	r3, .L20+32
	ldr	r5, [r3, #0]
	ldr	r0, .L20+8
	bl	atof
	mov	r3, r0
	mov	r4, r1
	stmia	r5, {r3-r4}
	.loc 1 258 0
	ldr	r3, .L20+32
	ldr	r3, [r3, #0]
	fldd	d6, [r3, #0]
	ldr	r3, .L20+36
	fldd	d7, [r3, #0]
	fcmped	d6, d7
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L15
	.loc 1 260 0
	ldr	r3, .L20+32
	ldr	r2, [r3, #0]
	ldr	r3, .L20+36
	ldmia	r3, {r3-r4}
	stmia	r2, {r3-r4}
	.loc 1 262 0
	mov	r3, #1
	str	r3, [fp, #-16]
.L15:
	.loc 1 268 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L18
	.loc 1 270 0
	bl	bad_key_beep
	b	.L13
.L18:
	.loc 1 274 0
	bl	good_key_beep
.L13:
	.loc 1 276 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L21:
	.align	2
.L20:
	.word	g_NUMERIC_KEYPAD_type
	.word	ptr_to_uint32_value
	.word	GuiVar_NumericKeypadValue
	.word	min_uint32_value
	.word	ptr_to_int32_value
	.word	min_int32_value
	.word	ptr_to_float_value
	.word	min_float_value
	.word	ptr_to_double_value
	.word	min_double_value
.LFE1:
	.size	validate_lower_range, .-validate_lower_range
	.section	.text.FDTO_show_keypad,"ax",%progbits
	.align	2
	.type	FDTO_show_keypad, %function
FDTO_show_keypad:
.LFB2:
	.loc 1 292 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #4
.LCFI8:
	str	r0, [fp, #-8]
	.loc 1 293 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L23
	.loc 1 295 0
	ldr	r3, .L24
	mov	r2, #98
	strh	r2, [r3, #0]	@ movhi
.L23:
	.loc 1 298 0
	ldr	r3, .L24
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #616
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 299 0
	bl	GuiLib_Refresh
	.loc 1 300 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L25:
	.align	2
.L24:
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_show_keypad, .-FDTO_show_keypad
	.section	.text.show_keypad,"ax",%progbits
	.align	2
	.type	show_keypad, %function
show_keypad:
.LFB3:
	.loc 1 316 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #40
.LCFI11:
	str	r0, [fp, #-44]
	.loc 1 319 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 320 0
	ldr	r3, .L27
	str	r3, [fp, #-20]
	.loc 1 321 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 322 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 323 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L28:
	.align	2
.L27:
	.word	FDTO_show_keypad
.LFE3:
	.size	show_keypad, .-show_keypad
	.section	.text.hide_keypad,"ax",%progbits
	.align	2
	.type	hide_keypad, %function
hide_keypad:
.LFB4:
	.loc 1 339 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #40
.LCFI14:
	str	r0, [fp, #-44]
	.loc 1 346 0
	ldr	r3, .L30
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L30+4
	strh	r2, [r3, #0]	@ movhi
	.loc 1 350 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 351 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-20]
	.loc 1 352 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 353 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 354 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L31:
	.align	2
.L30:
	.word	g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed
	.word	GuiLib_ActiveCursorFieldNo
.LFE4:
	.size	hide_keypad, .-hide_keypad
	.section	.text.process_up_arrow,"ax",%progbits
	.align	2
	.type	process_up_arrow, %function
process_up_arrow:
.LFB5:
	.loc 1 358 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	.loc 1 359 0
	ldr	r3, .L37
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #98
	beq	.L33
	.loc 1 360 0 discriminator 1
	ldr	r3, .L37
	ldrh	r3, [r3, #0]
	.loc 1 359 0 discriminator 1
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #99
	bne	.L34
.L33:
	.loc 1 362 0
	mov	r0, #60
	mov	r1, #1
	bl	CURSOR_Select
	b	.L32
.L34:
	.loc 1 364 0
	ldr	r3, .L37
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #52
	ble	.L36
	.loc 1 365 0 discriminator 1
	ldr	r3, .L37
	ldrh	r3, [r3, #0]
	.loc 1 364 0 discriminator 1
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #61
	bgt	.L36
	.loc 1 367 0
	ldr	r3, .L37
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #3
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L32
.L36:
	.loc 1 371 0
	bl	bad_key_beep
.L32:
	.loc 1 373 0
	ldmfd	sp!, {fp, pc}
.L38:
	.align	2
.L37:
	.word	GuiLib_ActiveCursorFieldNo
.LFE5:
	.size	process_up_arrow, .-process_up_arrow
	.section	.text.process_down_arrow,"ax",%progbits
	.align	2
	.type	process_down_arrow, %function
process_down_arrow:
.LFB6:
	.loc 1 377 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI17:
	add	fp, sp, #4
.LCFI18:
	.loc 1 378 0
	ldr	r3, .L48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #49
	ble	.L40
	.loc 1 379 0 discriminator 1
	ldr	r3, .L48
	ldrh	r3, [r3, #0]
	.loc 1 378 0 discriminator 1
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #55
	bgt	.L40
	.loc 1 381 0
	ldr	r3, .L48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #3
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L39
.L40:
	.loc 1 383 0
	ldr	r3, .L48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #56
	bne	.L42
	.loc 1 385 0
	ldr	r3, .L48+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L43
	.loc 1 387 0
	mov	r0, #59
	mov	r1, #1
	bl	CURSOR_Select
	b	.L39
.L43:
	.loc 1 391 0
	mov	r0, #60
	mov	r1, #1
	bl	CURSOR_Select
	b	.L39
.L42:
	.loc 1 394 0
	ldr	r3, .L48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #57
	bne	.L44
	.loc 1 396 0
	mov	r0, #60
	mov	r1, #1
	bl	CURSOR_Select
	b	.L39
.L44:
	.loc 1 398 0
	ldr	r3, .L48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #58
	bne	.L45
	.loc 1 400 0
	ldr	r3, .L48+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L46
	.loc 1 402 0
	mov	r0, #61
	mov	r1, #1
	bl	CURSOR_Select
	b	.L39
.L46:
	.loc 1 406 0
	mov	r0, #60
	mov	r1, #1
	bl	CURSOR_Select
	b	.L39
.L45:
	.loc 1 409 0
	ldr	r3, .L48
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #58
	ble	.L47
	.loc 1 410 0 discriminator 1
	ldr	r3, .L48
	ldrh	r3, [r3, #0]
	.loc 1 409 0 discriminator 1
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #61
	bgt	.L47
	.loc 1 412 0
	mov	r0, #98
	mov	r1, #1
	bl	CURSOR_Select
	b	.L39
.L47:
	.loc 1 416 0
	bl	bad_key_beep
.L39:
	.loc 1 418 0
	ldmfd	sp!, {fp, pc}
.L49:
	.align	2
.L48:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_NumericKeypadShowPlusMinus
	.word	GuiVar_NumericKeypadShowDecimalPoint
.LFE6:
	.size	process_down_arrow, .-process_down_arrow
	.section	.text.process_left_arrow,"ax",%progbits
	.align	2
	.type	process_left_arrow, %function
process_left_arrow:
.LFB7:
	.loc 1 422 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI19:
	add	fp, sp, #4
.LCFI20:
	.loc 1 423 0
	ldr	r3, .L57
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #99
	bne	.L51
	.loc 1 425 0
	mov	r0, #98
	mov	r1, #1
	bl	CURSOR_Select
	b	.L50
.L51:
	.loc 1 427 0
	ldr	r3, .L57
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #51
	beq	.L53
	.loc 1 428 0 discriminator 1
	ldr	r3, .L57
	ldrh	r3, [r3, #0]
	.loc 1 427 0 discriminator 1
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #52
	beq	.L53
	.loc 1 429 0
	ldr	r3, .L57
	ldrh	r3, [r3, #0]
	.loc 1 428 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #54
	beq	.L53
	.loc 1 430 0
	ldr	r3, .L57
	ldrh	r3, [r3, #0]
	.loc 1 429 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #55
	beq	.L53
	.loc 1 431 0
	ldr	r3, .L57
	ldrh	r3, [r3, #0]
	.loc 1 430 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #57
	beq	.L53
	.loc 1 432 0
	ldr	r3, .L57
	ldrh	r3, [r3, #0]
	.loc 1 431 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #58
	beq	.L53
	.loc 1 433 0
	ldr	r3, .L57
	ldrh	r3, [r3, #0]
	.loc 1 432 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #61
	bne	.L54
.L53:
	.loc 1 435 0
	mov	r0, #1
	bl	CURSOR_Up
	b	.L50
.L54:
	.loc 1 437 0
	ldr	r3, .L57
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #60
	bne	.L55
	.loc 1 439 0
	ldr	r3, .L57+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L56
	.loc 1 441 0
	mov	r0, #59
	mov	r1, #1
	bl	CURSOR_Select
	b	.L50
.L56:
	.loc 1 445 0
	bl	bad_key_beep
	b	.L50
.L55:
	.loc 1 450 0
	bl	bad_key_beep
.L50:
	.loc 1 452 0
	ldmfd	sp!, {fp, pc}
.L58:
	.align	2
.L57:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_NumericKeypadShowPlusMinus
.LFE7:
	.size	process_left_arrow, .-process_left_arrow
	.section	.text.process_right_arrow,"ax",%progbits
	.align	2
	.type	process_right_arrow, %function
process_right_arrow:
.LFB8:
	.loc 1 456 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	.loc 1 457 0
	ldr	r3, .L66
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #98
	bne	.L60
	.loc 1 459 0
	mov	r0, #99
	mov	r1, #1
	bl	CURSOR_Select
	b	.L59
.L60:
	.loc 1 461 0
	ldr	r3, .L66
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #50
	beq	.L62
	.loc 1 462 0 discriminator 1
	ldr	r3, .L66
	ldrh	r3, [r3, #0]
	.loc 1 461 0 discriminator 1
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #51
	beq	.L62
	.loc 1 463 0
	ldr	r3, .L66
	ldrh	r3, [r3, #0]
	.loc 1 462 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #53
	beq	.L62
	.loc 1 464 0
	ldr	r3, .L66
	ldrh	r3, [r3, #0]
	.loc 1 463 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #54
	beq	.L62
	.loc 1 465 0
	ldr	r3, .L66
	ldrh	r3, [r3, #0]
	.loc 1 464 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #56
	beq	.L62
	.loc 1 466 0
	ldr	r3, .L66
	ldrh	r3, [r3, #0]
	.loc 1 465 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #57
	beq	.L62
	.loc 1 467 0
	ldr	r3, .L66
	ldrh	r3, [r3, #0]
	.loc 1 466 0
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #59
	bne	.L63
.L62:
	.loc 1 469 0
	mov	r0, #1
	bl	CURSOR_Down
	b	.L59
.L63:
	.loc 1 471 0
	ldr	r3, .L66
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #60
	bne	.L64
	.loc 1 473 0
	ldr	r3, .L66+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L65
	.loc 1 475 0
	mov	r0, #61
	mov	r1, #1
	bl	CURSOR_Select
	b	.L59
.L65:
	.loc 1 479 0
	bl	bad_key_beep
	b	.L59
.L64:
	.loc 1 484 0
	bl	bad_key_beep
.L59:
	.loc 1 486 0
	ldmfd	sp!, {fp, pc}
.L67:
	.align	2
.L66:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_NumericKeypadShowDecimalPoint
.LFE8:
	.size	process_right_arrow, .-process_right_arrow
	.section	.text.process_OK_button,"ax",%progbits
	.align	2
	.type	process_OK_button, %function
process_OK_button:
.LFB9:
	.loc 1 490 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI23:
	add	fp, sp, #4
.LCFI24:
	sub	sp, sp, #4
.LCFI25:
	str	r0, [fp, #-8]
	.loc 1 491 0
	mov	r0, #1
	bl	validate_lower_range
	.loc 1 495 0
	ldr	r0, [fp, #-8]
	bl	hide_keypad
	.loc 1 496 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE9:
	.size	process_OK_button, .-process_OK_button
	.section .rodata
	.align	2
.LC3:
	.ascii	"0\000"
	.align	2
.LC4:
	.ascii	".\000"
	.align	2
.LC5:
	.ascii	"%c\000"
	.section	.text.process_key_press,"ax",%progbits
	.align	2
	.type	process_key_press, %function
process_key_press:
.LFB10:
	.loc 1 500 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	sub	sp, sp, #16
.LCFI28:
	str	r0, [fp, #-20]
	.loc 1 507 0
	ldr	r3, [fp, #-20]
	cmp	r3, #99
	bne	.L70
	.loc 1 509 0
	ldr	r0, .L95
	ldr	r1, .L95+4
	mov	r2, #11
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	bne	.L71
	.loc 1 511 0
	bl	bad_key_beep
	b	.L72
.L71:
	.loc 1 515 0
	bl	good_key_beep
	.loc 1 517 0
	ldr	r0, .L95
	bl	strlen
	mov	r3, r0
	cmp	r3, #1
	bls	.L73
	.loc 1 519 0
	ldr	r0, .L95
	bl	strlen
	mov	r3, r0
	ldr	r0, .L95
	ldr	r1, .L95
	mov	r2, r3
	bl	strlcpy
	b	.L72
.L73:
	.loc 1 523 0
	ldr	r0, .L95
	ldr	r1, .L95+4
	mov	r2, #11
	bl	strlcpy
	b	.L72
.L70:
	.loc 1 527 0
	ldr	r3, [fp, #-20]
	cmp	r3, #59
	bne	.L74
	.loc 1 529 0
	ldr	r0, .L95
	ldr	r1, .L95+8
	bl	strstr
	mov	r3, r0
	cmp	r3, #0
	bne	.L75
	.loc 1 531 0
	ldr	r0, .L95
	bl	strlen
	mov	r3, r0
	add	r3, r3, #1
	ldr	r0, .L95+12
	ldr	r1, .L95
	mov	r2, r3
	bl	memmove
	.loc 1 533 0
	ldr	r3, .L95
	mov	r2, #45
	strb	r2, [r3, #0]
	b	.L76
.L75:
	.loc 1 537 0
	ldr	r0, .L95
	ldr	r1, .L95+12
	mov	r2, #11
	bl	memmove
.L76:
	.loc 1 540 0
	mov	r0, #1
	bl	validate_upper_range
	b	.L72
.L74:
	.loc 1 542 0
	ldr	r3, [fp, #-20]
	cmp	r3, #61
	bne	.L77
	.loc 1 544 0
	ldr	r0, .L95
	ldr	r1, .L95+16
	bl	strstr
	mov	r3, r0
	cmp	r3, #0
	bne	.L78
	.loc 1 546 0
	bl	good_key_beep
	.loc 1 548 0
	ldr	r0, .L95
	ldr	r1, .L95+16
	mov	r2, #11
	bl	strlcat
	b	.L72
.L78:
	.loc 1 552 0
	bl	bad_key_beep
	b	.L72
.L77:
	.loc 1 557 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 561 0
	ldr	r0, .L95
	ldr	r1, .L95+16
	bl	strstr
	mov	r3, r0
	cmp	r3, #0
	beq	.L79
.LBB2:
	.loc 1 565 0
	ldr	r0, .L95
	ldr	r1, .L95+16
	bl	strstr
	str	r0, [fp, #-16]
	.loc 1 567 0
	ldr	r0, [fp, #-16]
	bl	strlen
	mov	r3, r0
	sub	r2, r3, #1
	ldr	r3, .L95+20
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L79
	.loc 1 569 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L79:
.LBE2:
	.loc 1 573 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L80
	.loc 1 575 0
	mov	r3, #0
	strb	r3, [fp, #-9]
	.loc 1 577 0
	ldr	r3, [fp, #-20]
	sub	r3, r3, #50
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L81
.L92:
	.word	.L82
	.word	.L83
	.word	.L84
	.word	.L85
	.word	.L86
	.word	.L87
	.word	.L88
	.word	.L89
	.word	.L90
	.word	.L81
	.word	.L91
.L82:
	.loc 1 579 0
	mov	r3, #49
	strb	r3, [fp, #-9]
	b	.L81
.L83:
	.loc 1 580 0
	mov	r3, #50
	strb	r3, [fp, #-9]
	b	.L81
.L84:
	.loc 1 581 0
	mov	r3, #51
	strb	r3, [fp, #-9]
	b	.L81
.L85:
	.loc 1 582 0
	mov	r3, #52
	strb	r3, [fp, #-9]
	b	.L81
.L86:
	.loc 1 583 0
	mov	r3, #53
	strb	r3, [fp, #-9]
	b	.L81
.L87:
	.loc 1 584 0
	mov	r3, #54
	strb	r3, [fp, #-9]
	b	.L81
.L88:
	.loc 1 585 0
	mov	r3, #55
	strb	r3, [fp, #-9]
	b	.L81
.L89:
	.loc 1 586 0
	mov	r3, #56
	strb	r3, [fp, #-9]
	b	.L81
.L90:
	.loc 1 587 0
	mov	r3, #57
	strb	r3, [fp, #-9]
	b	.L81
.L91:
	.loc 1 588 0
	mov	r3, #48
	strb	r3, [fp, #-9]
	mov	r0, r0	@ nop
.L81:
	.loc 1 591 0
	ldr	r0, .L95
	ldr	r1, .L95+4
	mov	r2, #11
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	bne	.L93
	.loc 1 593 0
	ldrb	r3, [fp, #-9]	@ zero_extendqisi2
	ldr	r0, .L95
	mov	r1, #11
	ldr	r2, .L95+24
	bl	snprintf
	b	.L94
.L93:
	.loc 1 597 0
	ldrb	r3, [fp, #-9]	@ zero_extendqisi2
	ldr	r0, .L95
	mov	r1, #11
	ldr	r2, .L95+24
	bl	sp_strlcat
.L94:
	.loc 1 600 0
	mov	r0, #1
	bl	validate_upper_range
	b	.L72
.L80:
	.loc 1 604 0
	bl	bad_key_beep
.L72:
	.loc 1 612 0
	mov	r0, #0
	bl	show_keypad
	.loc 1 613 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L96:
	.align	2
.L95:
	.word	GuiVar_NumericKeypadValue
	.word	.LC3
	.word	.LC1
	.word	GuiVar_NumericKeypadValue+1
	.word	.LC4
	.word	g_NUMERIC_KEYPAD_num_decimals_to_display
	.word	.LC5
.LFE10:
	.size	process_key_press, .-process_key_press
	.section	.text.init_common_globals,"ax",%progbits
	.align	2
	.type	init_common_globals, %function
init_common_globals:
.LFB11:
	.loc 1 632 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI29:
	add	fp, sp, #0
.LCFI30:
	sub	sp, sp, #16
.LCFI31:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	str	r2, [fp, #-12]
	str	r3, [fp, #-16]
	.loc 1 634 0
	ldr	r3, .L98
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L98+4
	str	r2, [r3, #0]
	.loc 1 638 0
	ldr	r3, .L98+8
	ldr	r2, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 640 0
	ldr	r3, .L98+12
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 644 0
	ldr	r3, .L98+16
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 646 0
	ldr	r3, .L98+20
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 647 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L99:
	.align	2
.L98:
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed
	.word	g_NUMERIC_KEYPAD_type
	.word	g_NUMERIC_KEYPAD_num_decimals_to_display
	.word	GuiVar_NumericKeypadShowPlusMinus
	.word	GuiVar_NumericKeypadShowDecimalPoint
.LFE11:
	.size	init_common_globals, .-init_common_globals
	.section .rodata
	.align	2
.LC6:
	.ascii	"%u\000"
	.section	.text.NUMERIC_KEYPAD_draw_uint32_keypad,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_draw_uint32_keypad
	.type	NUMERIC_KEYPAD_draw_uint32_keypad, %function
NUMERIC_KEYPAD_draw_uint32_keypad:
.LFB12:
	.loc 1 665 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI32:
	add	fp, sp, #4
.LCFI33:
	sub	sp, sp, #12
.LCFI34:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 666 0
	mov	r0, #0
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	init_common_globals
	.loc 1 670 0
	ldr	r3, .L101
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 672 0
	ldr	r3, .L101+4
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 674 0
	ldr	r3, .L101+8
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 678 0
	ldr	r3, .L101
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	ldr	r0, .L101+12
	mov	r1, #11
	ldr	r2, .L101+16
	bl	snprintf
	.loc 1 682 0
	mov	r0, #1
	bl	show_keypad
	.loc 1 683 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L102:
	.align	2
.L101:
	.word	ptr_to_uint32_value
	.word	min_uint32_value
	.word	max_uint32_value
	.word	GuiVar_NumericKeypadValue
	.word	.LC6
.LFE12:
	.size	NUMERIC_KEYPAD_draw_uint32_keypad, .-NUMERIC_KEYPAD_draw_uint32_keypad
	.section	.text.NUMERIC_KEYPAD_draw_int32_keypad,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_draw_int32_keypad
	.type	NUMERIC_KEYPAD_draw_int32_keypad, %function
NUMERIC_KEYPAD_draw_int32_keypad:
.LFB13:
	.loc 1 704 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI35:
	add	fp, sp, #4
.LCFI36:
	sub	sp, sp, #12
.LCFI37:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 705 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, lsr #31
	mov	r0, #1
	mov	r1, #0
	mov	r2, #0
	bl	init_common_globals
	.loc 1 709 0
	ldr	r3, .L104
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 711 0
	ldr	r3, .L104+4
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 713 0
	ldr	r3, .L104+8
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 717 0
	ldr	r3, .L104
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	ldr	r0, .L104+12
	mov	r1, #11
	ldr	r2, .L104+16
	bl	snprintf
	.loc 1 721 0
	mov	r0, #1
	bl	show_keypad
	.loc 1 722 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L105:
	.align	2
.L104:
	.word	ptr_to_int32_value
	.word	min_int32_value
	.word	max_int32_value
	.word	GuiVar_NumericKeypadValue
	.word	.LC0
.LFE13:
	.size	NUMERIC_KEYPAD_draw_int32_keypad, .-NUMERIC_KEYPAD_draw_int32_keypad
	.section .rodata
	.align	2
.LC7:
	.ascii	"%.*f\000"
	.section	.text.NUMERIC_KEYPAD_draw_float_keypad,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_draw_float_keypad
	.type	NUMERIC_KEYPAD_draw_float_keypad, %function
NUMERIC_KEYPAD_draw_float_keypad:
.LFB14:
	.loc 1 740 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI38:
	add	fp, sp, #4
.LCFI39:
	sub	sp, sp, #24
.LCFI40:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]	@ float
	str	r2, [fp, #-16]	@ float
	str	r3, [fp, #-20]
	.loc 1 741 0
	flds	s15, [fp, #-12]
	fcmpezs	s15
	fmstat
	movpl	r2, #0
	movmi	r2, #1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	moveq	r3, #0
	movne	r3, #1
	mov	r0, #2
	ldr	r1, [fp, #-20]
	bl	init_common_globals
	.loc 1 745 0
	ldr	r3, .L107
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 747 0
	ldr	r3, .L107+4
	ldr	r2, [fp, #-12]	@ float
	str	r2, [r3, #0]	@ float
	.loc 1 749 0
	ldr	r3, .L107+8
	ldr	r2, [fp, #-16]	@ float
	str	r2, [r3, #0]	@ float
	.loc 1 753 0
	ldr	r3, .L107
	ldr	r3, [r3, #0]
	flds	s15, [r3, #0]
	fcvtds	d7, s15
	fstd	d7, [sp, #0]
	ldr	r0, .L107+12
	mov	r1, #11
	ldr	r2, .L107+16
	ldr	r3, [fp, #-20]
	bl	snprintf
	.loc 1 757 0
	mov	r0, #1
	bl	show_keypad
	.loc 1 758 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L108:
	.align	2
.L107:
	.word	ptr_to_float_value
	.word	min_float_value
	.word	max_float_value
	.word	GuiVar_NumericKeypadValue
	.word	.LC7
.LFE14:
	.size	NUMERIC_KEYPAD_draw_float_keypad, .-NUMERIC_KEYPAD_draw_float_keypad
	.section	.text.NUMERIC_KEYPAD_draw_double_keypad,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_draw_double_keypad
	.type	NUMERIC_KEYPAD_draw_double_keypad, %function
NUMERIC_KEYPAD_draw_double_keypad:
.LFB15:
	.loc 1 780 0
	@ args = 12, pretend = 4, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	sub	sp, sp, #4
.LCFI41:
	stmfd	sp!, {r4, fp, lr}
.LCFI42:
	add	fp, sp, #8
.LCFI43:
	sub	sp, sp, #20
.LCFI44:
	str	r0, [fp, #-12]
	str	r1, [fp, #-20]
	str	r2, [fp, #-16]
	str	r3, [fp, #4]
	.loc 1 781 0
	fldd	d7, [fp, #-20]
	fcmpezd	d7
	fmstat
	movpl	r2, #0
	movmi	r2, #1
	ldr	r3, [fp, #12]
	cmp	r3, #0
	moveq	r3, #0
	movne	r3, #1
	mov	r0, #3
	ldr	r1, [fp, #12]
	bl	init_common_globals
	.loc 1 785 0
	ldr	r3, .L110
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 787 0
	ldr	r2, .L110+4
	sub	r4, fp, #20
	ldmia	r4, {r3-r4}
	stmia	r2, {r3-r4}
	.loc 1 789 0
	ldr	r2, .L110+8
	ldmib	fp, {r3-r4}
	stmia	r2, {r3-r4}
	.loc 1 793 0
	ldr	r3, .L110
	ldr	r3, [r3, #0]
	ldmia	r3, {r3-r4}
	stmia	sp, {r3-r4}
	ldr	r0, .L110+12
	mov	r1, #11
	ldr	r2, .L110+16
	ldr	r3, [fp, #12]
	bl	snprintf
	.loc 1 797 0
	mov	r0, #1
	bl	show_keypad
	.loc 1 798 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, lr}
	add	sp, sp, #4
	bx	lr
.L111:
	.align	2
.L110:
	.word	ptr_to_double_value
	.word	min_double_value
	.word	max_double_value
	.word	GuiVar_NumericKeypadValue
	.word	.LC7
.LFE15:
	.size	NUMERIC_KEYPAD_draw_double_keypad, .-NUMERIC_KEYPAD_draw_double_keypad
	.section	.text.NUMERIC_KEYPAD_process_key,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_process_key
	.type	NUMERIC_KEYPAD_process_key, %function
NUMERIC_KEYPAD_process_key:
.LFB16:
	.loc 1 803 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI45:
	add	fp, sp, #4
.LCFI46:
	sub	sp, sp, #12
.LCFI47:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	str	r2, [fp, #-16]
	.loc 1 804 0
	ldr	r3, [fp, #-12]
	cmp	r3, #3
	beq	.L117
	cmp	r3, #3
	bhi	.L121
	cmp	r3, #1
	beq	.L115
	cmp	r3, #1
	bhi	.L116
	b	.L128
.L121:
	cmp	r3, #67
	beq	.L119
	cmp	r3, #67
	bhi	.L122
	cmp	r3, #4
	beq	.L118
	b	.L113
.L122:
	cmp	r3, #80
	beq	.L120
	cmp	r3, #84
	beq	.L120
	b	.L113
.L116:
	.loc 1 807 0
	ldr	r3, .L129
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #98
	bne	.L123
	.loc 1 809 0
	ldr	r0, [fp, #-16]
	bl	process_OK_button
	.loc 1 815 0
	b	.L112
.L123:
	.loc 1 813 0
	ldr	r3, .L129
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, r3
	bl	process_key_press
	.loc 1 815 0
	b	.L112
.L118:
	.loc 1 818 0
	bl	process_up_arrow
	.loc 1 819 0
	b	.L112
.L128:
	.loc 1 822 0
	bl	process_down_arrow
	.loc 1 823 0
	b	.L112
.L115:
	.loc 1 826 0
	bl	process_left_arrow
	.loc 1 827 0
	b	.L112
.L117:
	.loc 1 830 0
	bl	process_right_arrow
	.loc 1 831 0
	b	.L112
.L120:
	.loc 1 836 0
	ldr	r3, .L129+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L126
	.loc 1 838 0
	mov	r0, #59
	bl	process_key_press
	.loc 1 844 0
	b	.L112
.L126:
	.loc 1 842 0
	bl	bad_key_beep
	.loc 1 844 0
	b	.L112
.L119:
	.loc 1 847 0
	ldr	r0, [fp, #-16]
	bl	process_OK_button
	.loc 1 848 0
	b	.L112
.L113:
	.loc 1 851 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L112:
	.loc 1 853 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L130:
	.align	2
.L129:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_NumericKeypadShowPlusMinus
.LFE16:
	.size	NUMERIC_KEYPAD_process_key, .-NUMERIC_KEYPAD_process_key
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI17-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI19-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI21-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI23-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI26-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI29-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI32-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI35-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI38-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI41-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI42-.LCFI41
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x2
	.byte	0x8b
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x8
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI45-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x77e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF85
	.byte	0x1
	.4byte	.LASF86
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF2
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x2
	.byte	0x55
	.4byte	0x76
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x2
	.byte	0x5e
	.4byte	0x88
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x2
	.byte	0x67
	.4byte	0x3a
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF14
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x2
	.byte	0x99
	.4byte	0x88
	.uleb128 0x5
	.byte	0x4
	.4byte	0xb2
	.uleb128 0x6
	.4byte	0xb9
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.4byte	0x56
	.4byte	0xc9
	.uleb128 0x9
	.4byte	0x33
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x4f
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xf4
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x3
	.byte	0x7e
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x3
	.byte	0x80
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x3
	.byte	0x82
	.4byte	0xcf
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x186
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x7b
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x83
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x86
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x88
	.4byte	0x197
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x8d
	.4byte	0x1a9
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x92
	.4byte	0xac
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x4
	.byte	0x96
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x4
	.byte	0x9a
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x4
	.byte	0x9c
	.4byte	0x7d
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x192
	.uleb128 0xd
	.4byte	0x192
	.byte	0
	.uleb128 0xe
	.4byte	0x6b
	.uleb128 0x5
	.byte	0x4
	.4byte	0x186
	.uleb128 0xc
	.byte	0x1
	.4byte	0x1a9
	.uleb128 0xd
	.4byte	0xf4
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x19d
	.uleb128 0x4
	.4byte	.LASF28
	.byte	0x4
	.byte	0x9e
	.4byte	0xff
	.uleb128 0xf
	.4byte	.LASF29
	.byte	0x1
	.byte	0x74
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1ef
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0x74
	.4byte	0x1ef
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF32
	.byte	0x1
	.byte	0x76
	.4byte	0xa1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xe
	.4byte	0xa1
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x1
	.byte	0xd3
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x229
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0xd3
	.4byte	0x1ef
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x11
	.4byte	.LASF32
	.byte	0x1
	.byte	0xd7
	.4byte	0xa1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x12
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x123
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x252
	.uleb128 0x13
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x123
	.4byte	0x1ef
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x13b
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x28a
	.uleb128 0x13
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x13b
	.4byte	0x1ef
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x14
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x13d
	.4byte	0x1af
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x12
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x152
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x2c2
	.uleb128 0x13
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x152
	.4byte	0x2ce
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x14
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x154
	.4byte	0x1af
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x2ce
	.uleb128 0xd
	.4byte	0x1ef
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2c2
	.uleb128 0x15
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x165
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x15
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x178
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x15
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x1a5
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x15
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x1c7
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x12
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x1e9
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x351
	.uleb128 0x13
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x1e9
	.4byte	0x2ce
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x1f3
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x3b1
	.uleb128 0x13
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x1f3
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x1f5
	.4byte	0xa1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x1f7
	.4byte	0x4f
	.byte	0x2
	.byte	0x91
	.sleb128 -13
	.uleb128 0x17
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x16
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x233
	.4byte	0xc9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0xe
	.4byte	0x7d
	.uleb128 0x12
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x277
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x40c
	.uleb128 0x13
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x277
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x13
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x277
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x277
	.4byte	0x1ef
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x277
	.4byte	0x1ef
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x298
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x454
	.uleb128 0x13
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x298
	.4byte	0x454
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x298
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x298
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x7d
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x2bf
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x4a2
	.uleb128 0x13
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x2bf
	.4byte	0x4a2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x2bf
	.4byte	0x4a8
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x2bf
	.4byte	0x4a8
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8f
	.uleb128 0xe
	.4byte	0x8f
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x2e3
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x504
	.uleb128 0x13
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x2e3
	.4byte	0x504
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x2e3
	.4byte	0x50a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x2e3
	.4byte	0x50a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x2e3
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x25
	.uleb128 0xe
	.4byte	0x25
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x30b
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x566
	.uleb128 0x13
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x30b
	.4byte	0x566
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x30b
	.4byte	0x56c
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x13
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x30b
	.4byte	0x56c
	.byte	0x2
	.byte	0x91
	.sleb128 -4
	.uleb128 0x13
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x30b
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2c
	.uleb128 0xe
	.4byte	0x2c
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x321
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x5aa
	.uleb128 0x13
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x321
	.4byte	0xf4
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x322
	.4byte	0x2ce
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x19
	.4byte	.LASF62
	.byte	0x5
	.2byte	0x326
	.4byte	0x88
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF63
	.byte	0x5
	.2byte	0x327
	.4byte	0x88
	.byte	0x1
	.byte	0x1
	.uleb128 0x8
	.4byte	0x4f
	.4byte	0x5d6
	.uleb128 0x9
	.4byte	0x33
	.byte	0xa
	.byte	0
	.uleb128 0x19
	.4byte	.LASF64
	.byte	0x5
	.2byte	0x328
	.4byte	0x5c6
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF65
	.byte	0x6
	.2byte	0x127
	.4byte	0x76
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF66
	.byte	0x7
	.byte	0x30
	.4byte	0x603
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xb9
	.uleb128 0x11
	.4byte	.LASF67
	.byte	0x7
	.byte	0x34
	.4byte	0x619
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xb9
	.uleb128 0x11
	.4byte	.LASF68
	.byte	0x7
	.byte	0x36
	.4byte	0x62f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xb9
	.uleb128 0x11
	.4byte	.LASF69
	.byte	0x7
	.byte	0x38
	.4byte	0x645
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xb9
	.uleb128 0x11
	.4byte	.LASF70
	.byte	0x1
	.byte	0x48
	.4byte	0x7d
	.byte	0x5
	.byte	0x3
	.4byte	g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed
	.uleb128 0x11
	.4byte	.LASF71
	.byte	0x1
	.byte	0x4b
	.4byte	0x7d
	.byte	0x5
	.byte	0x3
	.4byte	g_NUMERIC_KEYPAD_type
	.uleb128 0x11
	.4byte	.LASF72
	.byte	0x1
	.byte	0x4f
	.4byte	0xa1
	.byte	0x5
	.byte	0x3
	.4byte	g_NUMERIC_KEYPAD_num_decimals_to_display
	.uleb128 0x11
	.4byte	.LASF73
	.byte	0x1
	.byte	0x53
	.4byte	0x454
	.byte	0x5
	.byte	0x3
	.4byte	ptr_to_uint32_value
	.uleb128 0x11
	.4byte	.LASF74
	.byte	0x1
	.byte	0x54
	.4byte	0x7d
	.byte	0x5
	.byte	0x3
	.4byte	min_uint32_value
	.uleb128 0x11
	.4byte	.LASF75
	.byte	0x1
	.byte	0x55
	.4byte	0x7d
	.byte	0x5
	.byte	0x3
	.4byte	max_uint32_value
	.uleb128 0x11
	.4byte	.LASF76
	.byte	0x1
	.byte	0x57
	.4byte	0x4a2
	.byte	0x5
	.byte	0x3
	.4byte	ptr_to_int32_value
	.uleb128 0x11
	.4byte	.LASF77
	.byte	0x1
	.byte	0x58
	.4byte	0x8f
	.byte	0x5
	.byte	0x3
	.4byte	min_int32_value
	.uleb128 0x11
	.4byte	.LASF78
	.byte	0x1
	.byte	0x59
	.4byte	0x8f
	.byte	0x5
	.byte	0x3
	.4byte	max_int32_value
	.uleb128 0x11
	.4byte	.LASF79
	.byte	0x1
	.byte	0x5b
	.4byte	0x504
	.byte	0x5
	.byte	0x3
	.4byte	ptr_to_float_value
	.uleb128 0x11
	.4byte	.LASF80
	.byte	0x1
	.byte	0x5c
	.4byte	0x25
	.byte	0x5
	.byte	0x3
	.4byte	min_float_value
	.uleb128 0x11
	.4byte	.LASF81
	.byte	0x1
	.byte	0x5d
	.4byte	0x25
	.byte	0x5
	.byte	0x3
	.4byte	max_float_value
	.uleb128 0x11
	.4byte	.LASF82
	.byte	0x1
	.byte	0x5f
	.4byte	0x566
	.byte	0x5
	.byte	0x3
	.4byte	ptr_to_double_value
	.uleb128 0x11
	.4byte	.LASF83
	.byte	0x1
	.byte	0x60
	.4byte	0x2c
	.byte	0x5
	.byte	0x3
	.4byte	min_double_value
	.uleb128 0x11
	.4byte	.LASF84
	.byte	0x1
	.byte	0x61
	.4byte	0x2c
	.byte	0x5
	.byte	0x3
	.4byte	max_double_value
	.uleb128 0x19
	.4byte	.LASF62
	.byte	0x5
	.2byte	0x326
	.4byte	0x88
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF63
	.byte	0x5
	.2byte	0x327
	.4byte	0x88
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF64
	.byte	0x5
	.2byte	0x328
	.4byte	0x5c6
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF65
	.byte	0x6
	.2byte	0x127
	.4byte	0x76
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI18
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI20
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI24
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI30
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI33
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI36
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI39
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI43
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI46
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x9c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF40:
	.ascii	"process_left_arrow\000"
.LASF21:
	.ascii	"_03_structure_to_draw\000"
.LASF20:
	.ascii	"_02_menu\000"
.LASF68:
	.ascii	"GuiFont_DecimalChar\000"
.LASF57:
	.ascii	"NUMERIC_KEYPAD_draw_int32_keypad\000"
.LASF52:
	.ascii	"pallow_decimal\000"
.LASF37:
	.ascii	"pDrawFunc\000"
.LASF34:
	.ascii	"pcomplete_redraw\000"
.LASF35:
	.ascii	"show_keypad\000"
.LASF77:
	.ascii	"min_int32_value\000"
.LASF64:
	.ascii	"GuiVar_NumericKeypadValue\000"
.LASF14:
	.ascii	"long long unsigned int\000"
.LASF10:
	.ascii	"INT_16\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF16:
	.ascii	"keycode\000"
.LASF36:
	.ascii	"hide_keypad\000"
.LASF4:
	.ascii	"long long int\000"
.LASF7:
	.ascii	"signed char\000"
.LASF76:
	.ascii	"ptr_to_int32_value\000"
.LASF72:
	.ascii	"g_NUMERIC_KEYPAD_num_decimals_to_display\000"
.LASF3:
	.ascii	"long int\000"
.LASF75:
	.ascii	"max_uint32_value\000"
.LASF70:
	.ascii	"g_NUMERIC_KEYPAD_cursor_position_when_keypad_displa"
	.ascii	"yed\000"
.LASF28:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF61:
	.ascii	"pkey_event\000"
.LASF1:
	.ascii	"double\000"
.LASF69:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF54:
	.ascii	"pmin_value\000"
.LASF78:
	.ascii	"max_int32_value\000"
.LASF19:
	.ascii	"_01_command\000"
.LASF51:
	.ascii	"pallow_negative\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF13:
	.ascii	"INT_32\000"
.LASF2:
	.ascii	"long unsigned int\000"
.LASF48:
	.ascii	"init_common_globals\000"
.LASF23:
	.ascii	"key_process_func_ptr\000"
.LASF45:
	.ascii	"lallowed_to_add_digit\000"
.LASF58:
	.ascii	"NUMERIC_KEYPAD_draw_float_keypad\000"
.LASF50:
	.ascii	"pnum_decimals_to_display\000"
.LASF15:
	.ascii	"BOOL_32\000"
.LASF8:
	.ascii	"short unsigned int\000"
.LASF62:
	.ascii	"GuiVar_NumericKeypadShowDecimalPoint\000"
.LASF85:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF25:
	.ascii	"_06_u32_argument1\000"
.LASF22:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF67:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF27:
	.ascii	"_08_screen_to_draw\000"
.LASF73:
	.ascii	"ptr_to_uint32_value\000"
.LASF33:
	.ascii	"FDTO_show_keypad\000"
.LASF43:
	.ascii	"process_key_press\000"
.LASF82:
	.ascii	"ptr_to_double_value\000"
.LASF44:
	.ascii	"pkey_pressed\000"
.LASF63:
	.ascii	"GuiVar_NumericKeypadShowPlusMinus\000"
.LASF18:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF55:
	.ascii	"pmax_value\000"
.LASF84:
	.ascii	"max_double_value\000"
.LASF46:
	.ascii	"lnum_to_append\000"
.LASF31:
	.ascii	"pplay_key_beep\000"
.LASF83:
	.ascii	"min_double_value\000"
.LASF0:
	.ascii	"float\000"
.LASF66:
	.ascii	"GuiFont_LanguageActive\000"
.LASF65:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF6:
	.ascii	"unsigned char\000"
.LASF47:
	.ascii	"decimal_value\000"
.LASF32:
	.ascii	"lval_out_of_range\000"
.LASF9:
	.ascii	"short int\000"
.LASF74:
	.ascii	"min_uint32_value\000"
.LASF71:
	.ascii	"g_NUMERIC_KEYPAD_type\000"
.LASF80:
	.ascii	"min_float_value\000"
.LASF29:
	.ascii	"validate_upper_range\000"
.LASF38:
	.ascii	"process_up_arrow\000"
.LASF5:
	.ascii	"char\000"
.LASF41:
	.ascii	"process_right_arrow\000"
.LASF56:
	.ascii	"NUMERIC_KEYPAD_draw_uint32_keypad\000"
.LASF49:
	.ascii	"pvalue_type\000"
.LASF59:
	.ascii	"NUMERIC_KEYPAD_draw_double_keypad\000"
.LASF17:
	.ascii	"repeats\000"
.LASF30:
	.ascii	"validate_lower_range\000"
.LASF79:
	.ascii	"ptr_to_float_value\000"
.LASF39:
	.ascii	"process_down_arrow\000"
.LASF60:
	.ascii	"NUMERIC_KEYPAD_process_key\000"
.LASF53:
	.ascii	"pvalue\000"
.LASF26:
	.ascii	"_07_u32_argument2\000"
.LASF42:
	.ascii	"process_OK_button\000"
.LASF24:
	.ascii	"_04_func_ptr\000"
.LASF86:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_numeric_keypad.c\000"
.LASF81:
	.ascii	"max_float_value\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
