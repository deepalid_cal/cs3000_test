	.file	"r_task_list.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.text.FDTO_TASK_LIST_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_TASK_LIST_draw_report
	.type	FDTO_TASK_LIST_draw_report, %function
FDTO_TASK_LIST_draw_report:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_task_list.c"
	.loc 1 42 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 43 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L1
	.loc 1 47 0
	ldr	r0, .L3
	bl	vTaskGetRunTimeStats
	.loc 1 55 0
	mov	r0, #105
	mov	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 56 0
	bl	GuiLib_Refresh
.L1:
	.loc 1 58 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L4:
	.align	2
.L3:
	.word	GuiVar_TaskList
.LFE0:
	.size	FDTO_TASK_LIST_draw_report, .-FDTO_TASK_LIST_draw_report
	.section	.text.TASK_LIST_process_report,"ax",%progbits
	.align	2
	.global	TASK_LIST_process_report
	.type	TASK_LIST_process_report, %function
TASK_LIST_process_report:
.LFB1:
	.loc 1 75 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 76 0
	ldr	r3, [fp, #-12]
	cmp	r3, #2
	beq	.L8
	cmp	r3, #2
	bhi	.L10
	cmp	r3, #0
	beq	.L7
	b	.L6
.L10:
	cmp	r3, #4
	beq	.L7
	cmp	r3, #67
	beq	.L9
	b	.L6
.L8:
	.loc 1 81 0
	bl	good_key_beep
	.loc 1 82 0
	mov	r0, #1
	bl	Redraw_Screen
	.loc 1 83 0
	b	.L5
.L7:
	.loc 1 87 0
	ldr	r3, [fp, #-12]
	mov	r0, #0
	mov	r1, r3
	bl	TEXT_BOX_up_or_down
	.loc 1 88 0
	b	.L5
.L9:
	.loc 1 92 0
	ldr	r3, .L12
	mov	r2, #11
	str	r2, [r3, #0]
	.loc 1 94 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 98 0
	mov	r0, #1
	bl	TECH_SUPPORT_draw_dialog
	.loc 1 99 0
	b	.L5
.L6:
	.loc 1 102 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L5:
	.loc 1 104 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	TASK_LIST_process_report, .-TASK_LIST_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1c0
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF25
	.byte	0x1
	.4byte	.LASF26
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x99
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0x98
	.uleb128 0x6
	.4byte	0x7a
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xbd
	.uleb128 0x8
	.4byte	.LASF12
	.byte	0x3
	.byte	0x7e
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF13
	.byte	0x3
	.byte	0x80
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x82
	.4byte	0x98
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.byte	0x29
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0xf0
	.uleb128 0xa
	.4byte	.LASF17
	.byte	0x1
	.byte	0x29
	.4byte	0xf0
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xb
	.4byte	0x6f
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.byte	0x4a
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x11d
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x1
	.byte	0x4a
	.4byte	0x11d
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xb
	.4byte	0xbd
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x2ec
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	0x25
	.4byte	0x141
	.uleb128 0xd
	.4byte	0x7a
	.2byte	0x7d0
	.byte	0
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x46f
	.4byte	0x130
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF21
	.byte	0x5
	.byte	0x30
	.4byte	0x160
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xb
	.4byte	0x88
	.uleb128 0xe
	.4byte	.LASF22
	.byte	0x5
	.byte	0x34
	.4byte	0x176
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xb
	.4byte	0x88
	.uleb128 0xe
	.4byte	.LASF23
	.byte	0x5
	.byte	0x36
	.4byte	0x18c
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xb
	.4byte	0x88
	.uleb128 0xe
	.4byte	.LASF24
	.byte	0x5
	.byte	0x38
	.4byte	0x1a2
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xb
	.4byte	0x88
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x2ec
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x46f
	.4byte	0x130
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF9:
	.ascii	"BOOL_32\000"
.LASF14:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF5:
	.ascii	"unsigned int\000"
.LASF25:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF26:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_task_list.c\000"
.LASF18:
	.ascii	"pkey_event\000"
.LASF10:
	.ascii	"long unsigned int\000"
.LASF6:
	.ascii	"long long unsigned int\000"
.LASF23:
	.ascii	"GuiFont_DecimalChar\000"
.LASF12:
	.ascii	"keycode\000"
.LASF16:
	.ascii	"TASK_LIST_process_report\000"
.LASF13:
	.ascii	"repeats\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF0:
	.ascii	"char\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF11:
	.ascii	"long int\000"
.LASF15:
	.ascii	"FDTO_TASK_LIST_draw_report\000"
.LASF17:
	.ascii	"pcomplete_redraw\000"
.LASF7:
	.ascii	"long long int\000"
.LASF21:
	.ascii	"GuiFont_LanguageActive\000"
.LASF24:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF22:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF19:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF4:
	.ascii	"short int\000"
.LASF20:
	.ascii	"GuiVar_TaskList\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
