	.file	"LPC32xx_intc_driver.c"
	.text
.Ltext0:
	.section	.bss.xISRVectorTable,"aw",%nobits
	.align	2
	.type	xISRVectorTable, %object
	.size	xISRVectorTable, 576
xISRVectorTable:
	.space	576
	.section	.text.prvExecuteHighestPriorityISR,"ax",%progbits
	.align	2
	.global	prvExecuteHighestPriorityISR
	.type	prvExecuteHighestPriorityISR, %function
prvExecuteHighestPriorityISR:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/LPC32xx_intc_driver.c"
	.loc 1 36 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	.loc 1 39 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L2
.L12:
	.loc 1 41 0
	ldr	r1, .L16
	ldr	r2, [fp, #-8]
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 43 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L3
	.loc 1 46 0
	ldr	r3, [fp, #-12]
	cmp	r3, #31
	bhi	.L4
	.loc 1 48 0
	ldr	r3, .L16+4
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	mov	r1, #1
	mov	r3, r1, asl r3
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L3
	.loc 1 50 0
	ldr	r3, .L16
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #3]
	blx	r3
	.loc 1 53 0
	ldr	r3, .L16+8
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	mov	r1, #1
	mov	r3, r1, asl r3
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L13
	.loc 1 55 0
	ldr	r3, .L16+12
	ldr	r2, .L16+12
	ldr	r1, [r2, #0]
	ldr	r2, [fp, #-12]
	mov	r0, #1
	mov	r2, r0, asl r2
	orr	r2, r1, r2
	str	r2, [r3, #0]
	.loc 1 58 0
	b	.L13
.L4:
	.loc 1 62 0
	ldr	r3, [fp, #-12]
	cmp	r3, #63
	bhi	.L7
	.loc 1 64 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #32
	str	r3, [fp, #-12]
	.loc 1 66 0
	ldr	r3, .L16+4
	ldr	r3, [r3, #0]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L8
	.loc 1 66 0 is_stmt 0 discriminator 2
	ldr	r3, .L16+4
	ldr	r3, [r3, #0]
	and	r3, r3, #1073741824
	cmp	r3, #0
	beq	.L3
.L8:
	.loc 1 66 0 discriminator 1
	ldr	r3, .L16+16
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	mov	r1, #1
	mov	r3, r1, asl r3
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L3
	.loc 1 68 0 is_stmt 1
	ldr	r3, .L16
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #3]
	blx	r3
	.loc 1 71 0
	ldr	r3, .L16+20
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	mov	r1, #1
	mov	r3, r1, asl r3
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L14
	.loc 1 73 0
	ldr	r3, .L16+24
	ldr	r2, .L16+24
	ldr	r1, [r2, #0]
	ldr	r2, [fp, #-12]
	mov	r0, #1
	mov	r2, r0, asl r2
	orr	r2, r1, r2
	str	r2, [r3, #0]
	.loc 1 76 0
	b	.L14
.L7:
	.loc 1 82 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #64
	str	r3, [fp, #-12]
	.loc 1 84 0
	ldr	r3, .L16+4
	ldr	r3, [r3, #0]
	and	r3, r3, #2
	cmp	r3, #0
	bne	.L10
	.loc 1 84 0 is_stmt 0 discriminator 2
	ldr	r3, .L16+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bge	.L3
.L10:
	.loc 1 84 0 discriminator 1
	ldr	r3, .L16+28
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	mov	r1, #1
	mov	r3, r1, asl r3
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L3
	.loc 1 86 0 is_stmt 1
	ldr	r3, .L16
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #3]
	blx	r3
	.loc 1 89 0
	ldr	r3, .L16+32
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	mov	r1, #1
	mov	r3, r1, asl r3
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L15
	.loc 1 91 0
	ldr	r3, .L16+36
	ldr	r2, .L16+36
	ldr	r1, [r2, #0]
	ldr	r2, [fp, #-12]
	mov	r0, #1
	mov	r2, r0, asl r2
	orr	r2, r1, r2
	str	r2, [r3, #0]
	.loc 1 94 0
	b	.L15
.L3:
	.loc 1 39 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L2:
	.loc 1 39 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #71
	bls	.L12
	b	.L1
.L13:
	.loc 1 58 0 is_stmt 1
	mov	r0, r0	@ nop
	b	.L1
.L14:
	.loc 1 76 0
	mov	r0, r0	@ nop
	b	.L1
.L15:
	.loc 1 94 0
	mov	r0, r0	@ nop
.L1:
	.loc 1 99 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	xISRVectorTable
	.word	1073774600
	.word	1073774608
	.word	1073774596
	.word	1073790984
	.word	1073790992
	.word	1073790980
	.word	1073807368
	.word	1073807376
	.word	1073807364
.LFE0:
	.size	prvExecuteHighestPriorityISR, .-prvExecuteHighestPriorityISR
	.section	.text.xIRQ_Handler,"ax",%progbits
	.align	2
	.global	xIRQ_Handler
	.type	xIRQ_Handler, %function
xIRQ_Handler:
.LFB1:
	.loc 1 107 0
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
.LBB2:
	.loc 1 108 0
@ 108 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/LPC32xx_intc_driver.c" 1
	STMDB	SP!, {R0}											
	STMDB	SP,{SP}^											
	NOP														
	SUB	SP, SP, #4											
	LDMIA	SP!,{R0}											
	STMDB	R0!, {LR}											
	MOV	LR, R0												
	LDMIA	SP!, {R0}											
	STMDB	LR,{R0-LR}^											
	NOP														
	SUB	LR, LR, #60											
	MRS	R0, SPSR											
	STMDB	LR!, {R0}											
	LDR	R0, =ulCriticalNesting								
	LDR	R0, [R0]											
	STMDB	LR!, {R0}											
	LDR	R0, =pxCurrentTCB									
	LDR	R0, [R0]											
	STR	LR, [R0]											
	
@ 0 "" 2
	ldr	r3, .L19
	ldr	r3, [r3, #0]
	ldr	r3, .L19+4
	ldr	r3, [r3, #0]
.LBE2:
	.loc 1 109 0
@ 109 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/LPC32xx_intc_driver.c" 1
	bl   prvExecuteHighestPriorityISR
@ 0 "" 2
.LBB3:
	.loc 1 110 0
@ 110 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/LPC32xx_intc_driver.c" 1
	LDR		R0, =pxCurrentTCB								
	LDR		R0, [R0]										
	LDR		LR, [R0]										
	LDR		R0, =ulCriticalNesting							
	LDMFD	LR!, {R1}											
	STR		R1, [R0]										
	LDMFD	LR!, {R0}											
	MSR		SPSR, R0										
	LDMFD	LR, {R0-R14}^										
	NOP														
	LDR		LR, [LR, #+60]									
	SUBS	PC, LR, #4											
	
@ 0 "" 2
	ldr	r3, .L19
	ldr	r3, [r3, #0]
	ldr	r3, .L19+4
	ldr	r3, [r3, #0]
.LBE3:
	.loc 1 111 0
.L20:
	.align	2
.L19:
	.word	ulCriticalNesting
	.word	pxCurrentTCB
.LFE1:
	.size	xIRQ_Handler, .-xIRQ_Handler
	.section	.text.xFIQ_Handler,"ax",%progbits
	.align	2
	.global	xFIQ_Handler
	.type	xFIQ_Handler, %function
xFIQ_Handler:
.LFB2:
	.loc 1 119 0
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
.LBB4:
	.loc 1 120 0
@ 120 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/LPC32xx_intc_driver.c" 1
	STMDB	SP!, {R0}											
	STMDB	SP,{SP}^											
	NOP														
	SUB	SP, SP, #4											
	LDMIA	SP!,{R0}											
	STMDB	R0!, {LR}											
	MOV	LR, R0												
	LDMIA	SP!, {R0}											
	STMDB	LR,{R0-LR}^											
	NOP														
	SUB	LR, LR, #60											
	MRS	R0, SPSR											
	STMDB	LR!, {R0}											
	LDR	R0, =ulCriticalNesting								
	LDR	R0, [R0]											
	STMDB	LR!, {R0}											
	LDR	R0, =pxCurrentTCB									
	LDR	R0, [R0]											
	STR	LR, [R0]											
	
@ 0 "" 2
	ldr	r3, .L22
	ldr	r3, [r3, #0]
	ldr	r3, .L22+4
	ldr	r3, [r3, #0]
.LBE4:
	.loc 1 121 0
@ 121 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/LPC32xx_intc_driver.c" 1
	bl   prvExecuteHighestPriorityISR
@ 0 "" 2
.LBB5:
	.loc 1 122 0
@ 122 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/LPC32xx_intc_driver.c" 1
	LDR		R0, =pxCurrentTCB								
	LDR		R0, [R0]										
	LDR		LR, [R0]										
	LDR		R0, =ulCriticalNesting							
	LDMFD	LR!, {R1}											
	STR		R1, [R0]										
	LDMFD	LR!, {R0}											
	MSR		SPSR, R0										
	LDMFD	LR, {R0-R14}^										
	NOP														
	LDR		LR, [LR, #+60]									
	SUBS	PC, LR, #4											
	
@ 0 "" 2
	ldr	r3, .L22
	ldr	r3, [r3, #0]
	ldr	r3, .L22+4
	ldr	r3, [r3, #0]
.LBE5:
	.loc 1 123 0
.L23:
	.align	2
.L22:
	.word	ulCriticalNesting
	.word	pxCurrentTCB
.LFE2:
	.size	xFIQ_Handler, .-xFIQ_Handler
	.section	.text.xSetISR_Vector,"ax",%progbits
	.align	2
	.global	xSetISR_Vector
	.type	xSetISR_Vector, %function
xSetISR_Vector:
.LFB3:
	.loc 1 130 0
	@ args = 4, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #36
.LCFI5:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	str	r3, [fp, #-36]
	.loc 1 132 0
	ldr	r2, [fp, #-28]
	ldr	r3, .L47
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #4
	str	r3, [fp, #-20]
	.loc 1 135 0
	ldr	r2, [fp, #-28]
	ldr	r3, .L47
	umull	r1, r3, r2, r3
	mov	r1, r3, lsr #4
	mov	r3, r1
	mov	r3, r3, asl #3
	add	r3, r3, r1
	mov	r3, r3, asl #3
	rsb	r3, r3, r2
	str	r3, [fp, #-28]
	.loc 1 137 0
	ldr	r3, [fp, #-24]
	sub	r3, r3, #3
	cmp	r3, #92
	ldrls	pc, [pc, r3, asl #2]
	b	.L25
.L27:
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L25
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L25
	.word	.L25
	.word	.L25
	.word	.L46
	.word	.L46
	.word	.L25
	.word	.L46
	.word	.L25
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L25
	.word	.L25
	.word	.L25
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L25
	.word	.L25
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L25
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L25
	.word	.L25
	.word	.L46
	.word	.L25
	.word	.L25
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L25
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L46
	.word	.L25
	.word	.L25
	.word	.L46
.L25:
	.loc 1 216 0
	mov	r3, #0
	b	.L29
.L46:
	.loc 1 213 0
	mov	r0, r0	@ nop
.L28:
	.loc 1 220 0
	ldr	r3, [fp, #4]
	cmp	r3, #0
	beq	.L30
	.loc 1 222 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L31
.L33:
	.loc 1 224 0
	ldr	r1, .L47+4
	ldr	r2, [fp, #-4]
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bne	.L32
	.loc 1 226 0
	ldr	r3, .L47+4
	ldr	r2, [fp, #-4]
	ldr	r2, [r3, r2, asl #3]
	ldr	r3, [fp, #4]
	str	r2, [r3, #0]
	.loc 1 227 0
	ldr	r1, .L47+4
	ldr	r2, [fp, #-4]
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 228 0
	b	.L30
.L32:
	.loc 1 222 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L31:
	.loc 1 222 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #71
	bls	.L33
.L30:
	.loc 1 234 0 is_stmt 1
	ldr	r1, .L47+4
	ldr	r2, [fp, #-28]
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 235 0
	ldr	r3, .L47+4
	ldr	r2, [fp, #-28]
	ldr	r1, [fp, #-36]
	str	r1, [r3, r2, asl #3]
	.loc 1 238 0
	ldr	r3, [fp, #-24]
	cmp	r3, #31
	bhi	.L34
	.loc 1 240 0
	ldr	r3, .L47+8
	str	r3, [fp, #-8]
	.loc 1 241 0
	ldr	r3, .L47+12
	str	r3, [fp, #-12]
	.loc 1 242 0
	ldr	r3, .L47+16
	str	r3, [fp, #-16]
	b	.L35
.L34:
	.loc 1 245 0
	ldr	r3, [fp, #-24]
	cmp	r3, #63
	bhi	.L36
	.loc 1 247 0
	ldr	r3, .L47+20
	str	r3, [fp, #-8]
	.loc 1 248 0
	ldr	r3, .L47+24
	str	r3, [fp, #-12]
	.loc 1 249 0
	ldr	r3, .L47+28
	str	r3, [fp, #-16]
	.loc 1 250 0
	ldr	r3, [fp, #-24]
	sub	r3, r3, #32
	str	r3, [fp, #-24]
	.loc 1 252 0
	ldr	r3, .L47+32
	ldr	r2, .L47+32
	ldr	r2, [r2, #0]
	orr	r2, r2, #1073741825
	str	r2, [r3, #0]
	.loc 1 254 0
	ldr	r3, .L47+16
	ldr	r2, .L47+16
	ldr	r2, [r2, #0]
	orr	r2, r2, #1073741824
	str	r2, [r3, #0]
	.loc 1 256 0
	ldr	r3, .L47+8
	ldr	r2, .L47+8
	ldr	r2, [r2, #0]
	bic	r2, r2, #1
	str	r2, [r3, #0]
	.loc 1 258 0
	ldr	r3, .L47+12
	ldr	r2, .L47+12
	ldr	r2, [r2, #0]
	bic	r2, r2, #1
	str	r2, [r3, #0]
	b	.L35
.L36:
	.loc 1 263 0
	ldr	r3, .L47+36
	str	r3, [fp, #-8]
	.loc 1 264 0
	ldr	r3, .L47+40
	str	r3, [fp, #-12]
	.loc 1 265 0
	ldr	r3, .L47+44
	str	r3, [fp, #-16]
	.loc 1 266 0
	ldr	r3, [fp, #-24]
	sub	r3, r3, #64
	str	r3, [fp, #-24]
	.loc 1 268 0
	ldr	r3, .L47+32
	ldr	r2, .L47+32
	ldr	r2, [r2, #0]
	orr	r2, r2, #-2147483646
	str	r2, [r3, #0]
	.loc 1 270 0
	ldr	r3, .L47+16
	ldr	r2, .L47+16
	ldr	r2, [r2, #0]
	orr	r2, r2, #-2147483648
	str	r2, [r3, #0]
	.loc 1 272 0
	ldr	r3, .L47+8
	ldr	r2, .L47+8
	ldr	r2, [r2, #0]
	bic	r2, r2, #2
	str	r2, [r3, #0]
	.loc 1 274 0
	ldr	r3, .L47+12
	ldr	r2, .L47+12
	ldr	r2, [r2, #0]
	bic	r2, r2, #2
	str	r2, [r3, #0]
.L35:
	.loc 1 276 0
	ldr	r3, [fp, #-32]
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L37
.L43:
	.word	.L38
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
.L38:
	.loc 1 280 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	mvn	r3, r3
	and	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 281 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	mvn	r3, r3
	and	r2, r2, r3
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 282 0
	b	.L37
.L39:
	.loc 1 284 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	orr	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 285 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	mvn	r3, r3
	and	r2, r2, r3
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 286 0
	b	.L37
.L40:
	.loc 1 288 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	mvn	r3, r3
	and	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 289 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	orr	r2, r2, r3
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 290 0
	b	.L37
.L41:
	.loc 1 292 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	orr	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 293 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	orr	r2, r2, r3
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 294 0
	b	.L37
.L42:
	.loc 1 296 0
	mov	r3, #0
	b	.L29
.L37:
	.loc 1 299 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L44
	.loc 1 301 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	orr	r2, r2, r3
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	b	.L45
.L44:
	.loc 1 305 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r1, #1
	mov	r3, r1, asl r3
	mvn	r3, r3
	and	r2, r2, r3
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
.L45:
	.loc 1 308 0
	mov	r3, #1
.L29:
	.loc 1 309 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L48:
	.align	2
.L47:
	.word	954437177
	.word	xISRVectorTable
	.word	1073774604
	.word	1073774608
	.word	1073774612
	.word	1073790988
	.word	1073790992
	.word	1073790996
	.word	1073774592
	.word	1073807372
	.word	1073807376
	.word	1073807380
.LFE3:
	.size	xSetISR_Vector, .-xSetISR_Vector
	.section	.text.xEnable_ISR,"ax",%progbits
	.align	2
	.global	xEnable_ISR
	.type	xEnable_ISR, %function
xEnable_ISR:
.LFB4:
	.loc 1 316 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-8]
	.loc 1 319 0
	ldr	r3, [fp, #-8]
	cmp	r3, #31
	bhi	.L50
	.loc 1 321 0
	ldr	r3, .L53
	str	r3, [fp, #-4]
	b	.L51
.L50:
	.loc 1 324 0
	ldr	r3, [fp, #-8]
	cmp	r3, #63
	bhi	.L52
	.loc 1 326 0
	ldr	r3, .L53+4
	str	r3, [fp, #-4]
	.loc 1 327 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #32
	str	r3, [fp, #-8]
	b	.L51
.L52:
	.loc 1 332 0
	ldr	r3, .L53+8
	str	r3, [fp, #-4]
	.loc 1 333 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #64
	str	r3, [fp, #-8]
.L51:
	.loc 1 336 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	mov	r1, #1
	mov	r3, r1, asl r3
	orr	r2, r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 338 0
	mov	r3, #0
	.loc 1 339 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L54:
	.align	2
.L53:
	.word	1073774592
	.word	1073790976
	.word	1073807360
.LFE4:
	.size	xEnable_ISR, .-xEnable_ISR
	.section	.text.xDisable_ISR,"ax",%progbits
	.align	2
	.global	xDisable_ISR
	.type	xDisable_ISR, %function
xDisable_ISR:
.LFB5:
	.loc 1 346 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-8]
	.loc 1 349 0
	ldr	r3, [fp, #-8]
	cmp	r3, #31
	bhi	.L56
	.loc 1 351 0
	ldr	r3, .L59
	str	r3, [fp, #-4]
	b	.L57
.L56:
	.loc 1 354 0
	ldr	r3, [fp, #-8]
	cmp	r3, #63
	bhi	.L58
	.loc 1 356 0
	ldr	r3, .L59+4
	str	r3, [fp, #-4]
	.loc 1 357 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #32
	str	r3, [fp, #-8]
	b	.L57
.L58:
	.loc 1 362 0
	ldr	r3, .L59+8
	str	r3, [fp, #-4]
	.loc 1 363 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #64
	str	r3, [fp, #-8]
.L57:
	.loc 1 366 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	mov	r1, #1
	mov	r3, r1, asl r3
	mvn	r3, r3
	and	r2, r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 368 0
	mov	r3, #0
	.loc 1 369 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L60:
	.align	2
.L59:
	.word	1073774592
	.word	1073790976
	.word	1073807360
.LFE5:
	.size	xDisable_ISR, .-xDisable_ISR
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI6-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI9-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/LPC32xx_intc_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x359
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF39
	.byte	0x1
	.4byte	.LASF40
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x2
	.byte	0x1d
	.4byte	0x91
	.uleb128 0x5
	.4byte	.LASF8
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF9
	.sleb128 1
	.uleb128 0x5
	.4byte	.LASF10
	.sleb128 2
	.uleb128 0x5
	.4byte	.LASF11
	.sleb128 3
	.uleb128 0x5
	.4byte	.LASF12
	.sleb128 4
	.uleb128 0x5
	.4byte	.LASF13
	.sleb128 5
	.byte	0
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x2
	.byte	0x25
	.4byte	0x64
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x2
	.byte	0x27
	.4byte	0xa7
	.uleb128 0x7
	.byte	0x4
	.4byte	0xad
	.uleb128 0x8
	.byte	0x1
	.uleb128 0x9
	.byte	0x8
	.byte	0x1
	.byte	0x19
	.4byte	0xd4
	.uleb128 0xa
	.4byte	.LASF16
	.byte	0x1
	.byte	0x1b
	.4byte	0x9c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF17
	.byte	0x1
	.byte	0x1c
	.4byte	0xd4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF18
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.byte	0x23
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x111
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x1
	.byte	0x25
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x1
	.byte	0x25
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.byte	0x6a
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	0x176
	.uleb128 0xe
	.4byte	.LBB2
	.4byte	.LBE2
	.4byte	0x151
	.uleb128 0xf
	.4byte	.LASF23
	.byte	0x1
	.byte	0x6c
	.4byte	0x176
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x1
	.byte	0x6c
	.4byte	0x182
	.byte	0x1
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0xf
	.4byte	.LASF23
	.byte	0x1
	.byte	0x6e
	.4byte	0x176
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x1
	.byte	0x6e
	.4byte	0x182
	.byte	0x1
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x11
	.4byte	0x17b
	.uleb128 0x7
	.byte	0x4
	.4byte	0x181
	.uleb128 0x12
	.uleb128 0x11
	.4byte	0x25
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.byte	0x76
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	0x1ec
	.uleb128 0xe
	.4byte	.LBB4
	.4byte	.LBE4
	.4byte	0x1c7
	.uleb128 0xf
	.4byte	.LASF23
	.byte	0x1
	.byte	0x78
	.4byte	0x176
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x1
	.byte	0x78
	.4byte	0x182
	.byte	0x1
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.4byte	.LBB5
	.4byte	.LBE5
	.uleb128 0xf
	.4byte	.LASF23
	.byte	0x1
	.byte	0x7a
	.4byte	0x176
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x1
	.byte	0x7a
	.4byte	0x182
	.byte	0x1
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.byte	0x81
	.byte	0x1
	.4byte	0x2c
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.4byte	0x296
	.uleb128 0x14
	.4byte	.LASF17
	.byte	0x1
	.byte	0x81
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x14
	.4byte	.LASF26
	.byte	0x1
	.byte	0x81
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x14
	.4byte	.LASF27
	.byte	0x1
	.byte	0x81
	.4byte	0x91
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x14
	.4byte	.LASF16
	.byte	0x1
	.byte	0x81
	.4byte	0x9c
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x14
	.4byte	.LASF28
	.byte	0x1
	.byte	0x81
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x1
	.byte	0x83
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x1
	.byte	0x84
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x1
	.byte	0x85
	.4byte	0x29c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xc
	.4byte	.LASF32
	.byte	0x1
	.byte	0x85
	.4byte	0x29c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x1
	.byte	0x85
	.4byte	0x29c
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x9c
	.uleb128 0x7
	.byte	0x4
	.4byte	0x2a2
	.uleb128 0x11
	.4byte	0xd4
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x13b
	.byte	0x1
	.4byte	0x2c
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.4byte	0x2e4
	.uleb128 0x16
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x13b
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x13d
	.4byte	0x29c
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x159
	.byte	0x1
	.4byte	0x2c
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.4byte	0x321
	.uleb128 0x16
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x159
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x15b
	.4byte	0x29c
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x18
	.4byte	0xaf
	.4byte	0x331
	.uleb128 0x19
	.4byte	0x25
	.byte	0x47
	.byte	0
	.uleb128 0xc
	.4byte	.LASF38
	.byte	0x1
	.byte	0x1d
	.4byte	0x321
	.byte	0x5
	.byte	0x3
	.4byte	xISRVectorTable
	.uleb128 0xf
	.4byte	.LASF23
	.byte	0x1
	.byte	0x7a
	.4byte	0x176
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x1
	.byte	0x7a
	.4byte	0x182
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x35
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF20:
	.ascii	"ISR_number\000"
.LASF29:
	.ascii	"ulISRindex\000"
.LASF34:
	.ascii	"xSetISR_Vector\000"
.LASF11:
	.ascii	"ISR_TRIGGER_NEGATIVE_EDGE\000"
.LASF16:
	.ascii	"pISR_Function\000"
.LASF15:
	.ascii	"ISR_FUNCTION\000"
.LASF12:
	.ascii	"ISR_TRIGGER_POSITIVE_EDGE\000"
.LASF21:
	.ascii	"prvExecuteHighestPriorityISR\000"
.LASF19:
	.ascii	"ISR_index\000"
.LASF35:
	.ascii	"xEnable_ISR\000"
.LASF38:
	.ascii	"xISRVectorTable\000"
.LASF28:
	.ascii	"pOldISR_Function\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF9:
	.ascii	"ISR_TRIGGER_LOW_LEVEL\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF14:
	.ascii	"ISR_TRIGGER_TYPE\000"
.LASF33:
	.ascii	"ulITR\000"
.LASF13:
	.ascii	"ISR_TRIGGER_DUAL_EDGE\000"
.LASF18:
	.ascii	"unsigned int\000"
.LASF22:
	.ascii	"xIRQ_Handler\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF17:
	.ascii	"ulISRNumber\000"
.LASF31:
	.ascii	"ulAPR\000"
.LASF30:
	.ascii	"ulFiq\000"
.LASF36:
	.ascii	"ulER\000"
.LASF37:
	.ascii	"xDisable_ISR\000"
.LASF6:
	.ascii	"long long int\000"
.LASF32:
	.ascii	"ulATR\000"
.LASF23:
	.ascii	"pxCurrentTCB\000"
.LASF3:
	.ascii	"short int\000"
.LASF39:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF25:
	.ascii	"xFIQ_Handler\000"
.LASF4:
	.ascii	"long int\000"
.LASF24:
	.ascii	"ulCriticalNesting\000"
.LASF2:
	.ascii	"signed char\000"
.LASF27:
	.ascii	"eTriggerType\000"
.LASF26:
	.ascii	"ulPriority\000"
.LASF10:
	.ascii	"ISR_TRIGGER_HIGH_LEVEL\000"
.LASF40:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"LPC32xx_intc_driver.c\000"
.LASF8:
	.ascii	"ISR_TRIGGER_FIXED\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
