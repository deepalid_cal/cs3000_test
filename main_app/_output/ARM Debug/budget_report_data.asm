	.file	"budget_report_data.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	budget_report_data_completed
	.section	.bss.budget_report_data_completed,"aw",%nobits
	.align	2
	.type	budget_report_data_completed, %object
	.size	budget_report_data_completed, 229692
budget_report_data_completed:
	.space	229692
	.section	.bss.budget_report_ptrs,"aw",%nobits
	.align	2
	.type	budget_report_ptrs, %object
	.size	budget_report_ptrs, 4
budget_report_ptrs:
	.space	4
	.section	.bss.budget_report_data_ci_timer,"aw",%nobits
	.align	2
	.type	budget_report_data_ci_timer, %object
	.size	budget_report_data_ci_timer, 4
budget_report_data_ci_timer:
	.space	4
	.section	.text.nm_init_budget_report_record,"ax",%progbits
	.align	2
	.type	nm_init_budget_report_record, %function
nm_init_budget_report_record:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/budget_report_data.c"
	.loc 1 69 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 72 0
	ldr	r0, [fp, #-8]
	mov	r1, #0
	ldr	r2, .L2
	bl	memset
	.loc 1 73 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L3:
	.align	2
.L2:
	.word	1196
.LFE0:
	.size	nm_init_budget_report_record, .-nm_init_budget_report_record
	.section	.text.nm_init_budget_report_records,"ax",%progbits
	.align	2
	.type	nm_init_budget_report_records, %function
nm_init_budget_report_records:
.LFB1:
	.loc 1 78 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	.loc 1 83 0
	ldr	r0, .L7
	mov	r1, #0
	mov	r2, #60
	bl	memset
	.loc 1 85 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L5
.L6:
	.loc 1 87 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r2, .L7+4
	mul	r2, r3, r2
	ldr	r3, .L7+8
	add	r3, r2, r3
	mov	r0, r3
	bl	nm_init_budget_report_record
	.loc 1 85 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L5:
	.loc 1 85 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #191
	bls	.L6
	.loc 1 93 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L8:
	.align	2
.L7:
	.word	budget_report_data_completed
	.word	1196
	.word	budget_report_data_completed+60
.LFE1:
	.size	nm_init_budget_report_records, .-nm_init_budget_report_records
	.global	BUDGET_REPORT_RECORDS_FILENAME
	.section	.rodata.BUDGET_REPORT_RECORDS_FILENAME,"a",%progbits
	.align	2
	.type	BUDGET_REPORT_RECORDS_FILENAME, %object
	.size	BUDGET_REPORT_RECORDS_FILENAME, 22
BUDGET_REPORT_RECORDS_FILENAME:
	.ascii	"BUDGET_REPORT_RECORDS\000"
	.global	budget_revision_record_sizes
	.section	.rodata.budget_revision_record_sizes,"a",%progbits
	.align	2
	.type	budget_revision_record_sizes, %object
	.size	budget_revision_record_sizes, 4
budget_revision_record_sizes:
	.word	1196
	.global	budget_revision_record_counts
	.section	.rodata.budget_revision_record_counts,"a",%progbits
	.align	2
	.type	budget_revision_record_counts, %object
	.size	budget_revision_record_counts, 4
budget_revision_record_counts:
	.word	192
	.section .rodata
	.align	2
.LC0:
	.ascii	"BUDGET RPRT file unexpd update %u\000"
	.align	2
.LC1:
	.ascii	"BUDGET RPRT file update : to revision %u from %u\000"
	.align	2
.LC2:
	.ascii	"BUDGET RPRT updater error\000"
	.section	.text.nm_budget_report_data_updater,"ax",%progbits
	.align	2
	.type	nm_budget_report_data_updater, %function
nm_budget_report_data_updater:
.LFB2:
	.loc 1 123 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #4
.LCFI8:
	str	r0, [fp, #-8]
	.loc 1 128 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L10
	.loc 1 130 0
	ldr	r0, .L13
	ldr	r1, [fp, #-8]
	bl	Alert_Message_va
	b	.L11
.L10:
	.loc 1 134 0
	ldr	r0, .L13+4
	mov	r1, #0
	ldr	r2, [fp, #-8]
	bl	Alert_Message_va
	.loc 1 136 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L11
	.loc 1 139 0
	bl	nm_init_budget_report_records
	.loc 1 140 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L11:
	.loc 1 148 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L9
	.loc 1 150 0
	ldr	r0, .L13+8
	bl	Alert_Message
.L9:
	.loc 1 152 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L14:
	.align	2
.L13:
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE2:
	.size	nm_budget_report_data_updater, .-nm_budget_report_data_updater
	.section	.text.init_file_budget_report_records,"ax",%progbits
	.align	2
	.global	init_file_budget_report_records
	.type	init_file_budget_report_records, %function
init_file_budget_report_records:
.LFB3:
	.loc 1 156 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #28
.LCFI11:
	.loc 1 160 0
	ldr	r3, .L16
	ldr	r3, [r3, #0]
	ldr	r2, .L16+4
	str	r2, [sp, #0]
	ldr	r2, .L16+8
	str	r2, [sp, #4]
	ldr	r2, .L16+12
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	ldr	r3, .L16+16
	str	r3, [sp, #16]
	ldr	r3, .L16+20
	str	r3, [sp, #20]
	mov	r3, #19
	str	r3, [sp, #24]
	mov	r0, #1
	ldr	r1, .L16+24
	mov	r2, #0
	ldr	r3, .L16+28
	bl	FLASH_FILE_find_or_create_reports_file
	.loc 1 183 0
	ldr	r3, .L16+28
	mov	r2, #0
	str	r2, [r3, #24]
	.loc 1 184 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	budget_report_completed_records_recursive_MUTEX
	.word	budget_revision_record_sizes
	.word	budget_revision_record_counts
	.word	229692
	.word	nm_budget_report_data_updater
	.word	nm_init_budget_report_records
	.word	BUDGET_REPORT_RECORDS_FILENAME
	.word	budget_report_data_completed
.LFE3:
	.size	init_file_budget_report_records, .-init_file_budget_report_records
	.section	.text.save_file_budget_report_records,"ax",%progbits
	.align	2
	.global	save_file_budget_report_records
	.type	save_file_budget_report_records, %function
save_file_budget_report_records:
.LFB4:
	.loc 1 188 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #12
.LCFI14:
	.loc 1 189 0
	ldr	r3, .L19
	ldr	r3, [r3, #0]
	ldr	r2, .L19+4
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	mov	r3, #19
	str	r3, [sp, #8]
	mov	r0, #1
	ldr	r1, .L19+8
	mov	r2, #0
	ldr	r3, .L19+12
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	.loc 1 196 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L20:
	.align	2
.L19:
	.word	budget_report_completed_records_recursive_MUTEX
	.word	229692
	.word	BUDGET_REPORT_RECORDS_FILENAME
	.word	budget_report_data_completed
.LFE4:
	.size	save_file_budget_report_records, .-save_file_budget_report_records
	.section	.text.budget_report_data_ci_timer_callback,"ax",%progbits
	.align	2
	.type	budget_report_data_ci_timer_callback, %function
budget_report_data_ci_timer_callback:
.LFB5:
	.loc 1 200 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #4
.LCFI17:
	str	r0, [fp, #-8]
	.loc 1 204 0
	ldr	r0, .L22
	mov	r1, #0
	mov	r2, #512
	mov	r3, #0
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	.loc 1 205 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L23:
	.align	2
.L22:
	.word	407
.LFE5:
	.size	budget_report_data_ci_timer_callback, .-budget_report_data_ci_timer_callback
	.section .rodata
	.align	2
.LC3:
	.ascii	"\000"
	.align	2
.LC4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/budget_report_data.c\000"
	.align	2
.LC5:
	.ascii	"Timer NOT CREATED : %s, %u\000"
	.section	.text.BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running,"ax",%progbits
	.align	2
	.global	BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.type	BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, %function
BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running:
.LFB6:
	.loc 1 208 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI18:
	add	fp, sp, #8
.LCFI19:
	sub	sp, sp, #4
.LCFI20:
	.loc 1 211 0
	ldr	r3, .L27
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L25
	.loc 1 215 0
	ldr	r3, .L27+4
	str	r3, [sp, #0]
	ldr	r0, .L27+8
	mov	r1, #200
	mov	r2, #0
	mov	r3, #0
	bl	xTimerCreate
	mov	r2, r0
	ldr	r3, .L27
	str	r2, [r3, #0]
	.loc 1 217 0
	ldr	r3, .L27
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L25
	.loc 1 219 0
	ldr	r0, .L27+12
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L27+16
	mov	r1, r3
	mov	r2, #219
	bl	Alert_Message_va
.L25:
	.loc 1 226 0
	ldr	r3, .L27
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L24
	.loc 1 230 0
	ldr	r3, .L27
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xTimerIsTimerActive
	mov	r3, r0
	cmp	r3, #0
	bne	.L24
	.loc 1 233 0
	ldr	r3, .L27
	ldr	r4, [r3, #0]
	ldr	r3, .L27+20
	ldr	r3, [r3, #48]
	mov	r0, r3
	bl	CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records
	mov	r2, r0
	ldr	r3, .L27+24
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #2
	mvn	r2, #0
	str	r2, [sp, #0]
	mov	r0, r4
	mov	r1, #2
	mov	r2, r3
	mov	r3, #0
	bl	xTimerGenericCommand
.L24:
	.loc 1 236 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L28:
	.align	2
.L27:
	.word	budget_report_data_ci_timer
	.word	budget_report_data_ci_timer_callback
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	config_c
	.word	-858993459
.LFE6:
	.size	BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, .-BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.section	.text.nm_BUDGET_REPORT_DATA_inc_index,"ax",%progbits
	.align	2
	.global	nm_BUDGET_REPORT_DATA_inc_index
	.type	nm_BUDGET_REPORT_DATA_inc_index, %function
nm_BUDGET_REPORT_DATA_inc_index:
.LFB7:
	.loc 1 240 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI21:
	add	fp, sp, #0
.LCFI22:
	sub	sp, sp, #4
.LCFI23:
	str	r0, [fp, #-4]
	.loc 1 244 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 246 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #0]
	cmp	r3, #191
	bls	.L29
	.loc 1 249 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #0]
.L29:
	.loc 1 251 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE7:
	.size	nm_BUDGET_REPORT_DATA_inc_index, .-nm_BUDGET_REPORT_DATA_inc_index
	.section	.text.nm_BUDGET_increment_next_avail_ptr,"ax",%progbits
	.align	2
	.type	nm_BUDGET_increment_next_avail_ptr, %function
nm_BUDGET_increment_next_avail_ptr:
.LFB8:
	.loc 1 274 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	.loc 1 275 0
	ldr	r0, .L35
	bl	nm_BUDGET_REPORT_DATA_inc_index
	.loc 1 277 0
	ldr	r3, .L35+4
	ldr	r3, [r3, #4]
	cmp	r3, #0
	bne	.L32
	.loc 1 281 0
	ldr	r3, .L35+4
	mov	r2, #1
	str	r2, [r3, #8]
.L32:
	.loc 1 288 0
	ldr	r3, .L35+4
	ldr	r2, [r3, #4]
	ldr	r3, .L35+4
	ldr	r3, [r3, #16]
	cmp	r2, r3
	bne	.L33
	.loc 1 290 0
	ldr	r0, .L35+8
	bl	nm_BUDGET_REPORT_DATA_inc_index
.L33:
	.loc 1 298 0
	ldr	r3, .L35+4
	ldr	r2, [r3, #4]
	ldr	r3, .L35+4
	ldr	r3, [r3, #20]
	cmp	r2, r3
	bne	.L31
	.loc 1 300 0
	ldr	r0, .L35+12
	bl	nm_BUDGET_REPORT_DATA_inc_index
.L31:
	.loc 1 302 0
	ldmfd	sp!, {fp, pc}
.L36:
	.align	2
.L35:
	.word	budget_report_data_completed+4
	.word	budget_report_data_completed
	.word	budget_report_data_completed+16
	.word	budget_report_data_completed+20
.LFE8:
	.size	nm_BUDGET_increment_next_avail_ptr, .-nm_BUDGET_increment_next_avail_ptr
	.section	.text.nm_BUDGET_REPORT_RECORDS_get_previous_completed_record,"ax",%progbits
	.align	2
	.global	nm_BUDGET_REPORT_RECORDS_get_previous_completed_record
	.type	nm_BUDGET_REPORT_RECORDS_get_previous_completed_record, %function
nm_BUDGET_REPORT_RECORDS_get_previous_completed_record:
.LFB9:
	.loc 1 331 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI26:
	add	fp, sp, #0
.LCFI27:
	sub	sp, sp, #8
.LCFI28:
	str	r0, [fp, #-8]
	.loc 1 334 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L45
	cmp	r2, r3
	bcc	.L38
	.loc 1 334 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, .L45+4
	cmp	r2, r3
	bcc	.L39
.L38:
	.loc 1 337 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L40
.L39:
	.loc 1 339 0
	ldr	r3, .L45+8
	ldr	r3, [r3, #12]
	cmp	r3, #1
	bne	.L41
	.loc 1 343 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L40
.L41:
	.loc 1 347 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L45
	cmp	r2, r3
	bne	.L42
	.loc 1 349 0
	ldr	r3, .L45+8
	ldr	r3, [r3, #8]
	cmp	r3, #1
	bne	.L43
	.loc 1 352 0
	ldr	r3, .L45+12
	str	r3, [fp, #-4]
	b	.L40
.L43:
	.loc 1 356 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L40
.L42:
	.loc 1 361 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-4]
	.loc 1 363 0
	ldr	r3, [fp, #-4]
	sub	r3, r3, #1184
	sub	r3, r3, #12
	str	r3, [fp, #-4]
.L40:
	.loc 1 367 0
	ldr	r3, .L45+8
	ldr	r3, [r3, #4]
	ldr	r2, .L45+16
	mul	r2, r3, r2
	ldr	r3, .L45
	add	r2, r2, r3
	ldr	r3, [fp, #-4]
	cmp	r2, r3
	bne	.L44
	.loc 1 371 0
	ldr	r3, .L45+8
	mov	r2, #1
	str	r2, [r3, #12]
.L44:
	.loc 1 374 0
	ldr	r3, [fp, #-4]
	.loc 1 375 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L46:
	.align	2
.L45:
	.word	budget_report_data_completed+60
	.word	budget_report_data_completed+229692
	.word	budget_report_data_completed
	.word	budget_report_data_completed+228496
	.word	1196
.LFE9:
	.size	nm_BUDGET_REPORT_RECORDS_get_previous_completed_record, .-nm_BUDGET_REPORT_RECORDS_get_previous_completed_record
	.section	.text.nm_BUDGET_REPORT_RECORDS_get_most_recently_completed_record,"ax",%progbits
	.align	2
	.global	nm_BUDGET_REPORT_RECORDS_get_most_recently_completed_record
	.type	nm_BUDGET_REPORT_RECORDS_get_most_recently_completed_record, %function
nm_BUDGET_REPORT_RECORDS_get_most_recently_completed_record:
.LFB10:
	.loc 1 402 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI29:
	add	fp, sp, #4
.LCFI30:
	sub	sp, sp, #4
.LCFI31:
	.loc 1 406 0
	ldr	r3, .L48
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 408 0
	ldr	r3, .L48
	ldr	r3, [r3, #4]
	ldr	r2, .L48+4
	mul	r2, r3, r2
	ldr	r3, .L48+8
	add	r3, r2, r3
	mov	r0, r3
	bl	nm_BUDGET_REPORT_RECORDS_get_previous_completed_record
	str	r0, [fp, #-8]
	.loc 1 409 0
	ldr	r3, [fp, #-8]
	.loc 1 410 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L49:
	.align	2
.L48:
	.word	budget_report_data_completed
	.word	1196
	.word	budget_report_data_completed+60
.LFE10:
	.size	nm_BUDGET_REPORT_RECORDS_get_most_recently_completed_record, .-nm_BUDGET_REPORT_RECORDS_get_most_recently_completed_record
	.section	.text.nm_BUDGET_REPORT_DATA_close_and_start_a_new_record,"ax",%progbits
	.align	2
	.global	nm_BUDGET_REPORT_DATA_close_and_start_a_new_record
	.type	nm_BUDGET_REPORT_DATA_close_and_start_a_new_record, %function
nm_BUDGET_REPORT_DATA_close_and_start_a_new_record:
.LFB11:
	.loc 1 567 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI32:
	add	fp, sp, #4
.LCFI33:
	sub	sp, sp, #4
.LCFI34:
	str	r0, [fp, #-8]
	.loc 1 569 0
	ldr	r3, .L51
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L51+4
	ldr	r3, .L51+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 571 0
	ldr	r3, .L51+12
	ldr	r2, [r3, #4]
	ldr	r1, .L51+12
	mov	r3, #60
	ldr	r0, .L51+16
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	mov	r1, r2
	mov	r2, r3
	ldr	r3, .L51+16
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	memcpy
	.loc 1 574 0
	bl	nm_BUDGET_increment_next_avail_ptr
	.loc 1 576 0
	ldr	r3, .L51
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 581 0
	bl	BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.loc 1 587 0
	mov	r0, #19
	mov	r1, #2
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	.loc 1 588 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L52:
	.align	2
.L51:
	.word	budget_report_completed_records_recursive_MUTEX
	.word	.LC4
	.word	569
	.word	budget_report_data_completed
	.word	1196
.LFE11:
	.size	nm_BUDGET_REPORT_DATA_close_and_start_a_new_record, .-nm_BUDGET_REPORT_DATA_close_and_start_a_new_record
	.section	.text.BUDGET_REPORT_free_report_support,"ax",%progbits
	.align	2
	.global	BUDGET_REPORT_free_report_support
	.type	BUDGET_REPORT_free_report_support, %function
BUDGET_REPORT_free_report_support:
.LFB12:
	.loc 1 647 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI35:
	add	fp, sp, #4
.LCFI36:
	.loc 1 652 0
	ldr	r3, .L55
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L55+4
	mov	r3, #652
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 654 0
	ldr	r3, .L55+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L54
	.loc 1 656 0
	ldr	r3, .L55+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, .L55+4
	mov	r2, #656
	bl	mem_free_debug
	.loc 1 660 0
	ldr	r3, .L55+8
	mov	r2, #0
	str	r2, [r3, #0]
.L54:
	.loc 1 663 0
	ldr	r3, .L55
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 664 0
	ldmfd	sp!, {fp, pc}
.L56:
	.align	2
.L55:
	.word	budget_report_completed_records_recursive_MUTEX
	.word	.LC4
	.word	budget_report_ptrs
.LFE12:
	.size	BUDGET_REPORT_free_report_support, .-BUDGET_REPORT_free_report_support
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI26-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI29-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI32-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI35-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/configuration_controller.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/report_data.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/budget_report_data.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xaed
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF137
	.byte	0x1
	.4byte	.LASF138
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x4c
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x9d
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x5
	.byte	0x4
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x4
	.byte	0x57
	.4byte	0x9e
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x5
	.byte	0x4c
	.4byte	0xab
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x6
	.byte	0x65
	.4byte	0x9e
	.uleb128 0x6
	.4byte	0x33
	.4byte	0xdc
	.uleb128 0x7
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x6
	.4byte	0x5a
	.4byte	0xec
	.uleb128 0x7
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.byte	0x7
	.byte	0x2f
	.4byte	0x1e3
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x7
	.byte	0x35
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x7
	.byte	0x3e
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x7
	.byte	0x3f
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x7
	.byte	0x46
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x7
	.byte	0x4e
	.4byte	0x5a
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x7
	.byte	0x4f
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x7
	.byte	0x50
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x7
	.byte	0x52
	.4byte	0x5a
	.byte	0x4
	.byte	0x2
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x7
	.byte	0x53
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x7
	.byte	0x54
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x7
	.byte	0x58
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x7
	.byte	0x59
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x7
	.byte	0x5a
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x7
	.byte	0x5b
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.byte	0x7
	.byte	0x2b
	.4byte	0x1fc
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x7
	.byte	0x2d
	.4byte	0x41
	.uleb128 0xc
	.4byte	0xec
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.byte	0x7
	.byte	0x29
	.4byte	0x20d
	.uleb128 0xd
	.4byte	0x1e3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF32
	.byte	0x7
	.byte	0x61
	.4byte	0x1fc
	.uleb128 0x8
	.byte	0x4
	.byte	0x7
	.byte	0x6c
	.4byte	0x265
	.uleb128 0x9
	.4byte	.LASF33
	.byte	0x7
	.byte	0x70
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF34
	.byte	0x7
	.byte	0x76
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF35
	.byte	0x7
	.byte	0x7a
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x7
	.byte	0x7c
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.byte	0x7
	.byte	0x68
	.4byte	0x27e
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x7
	.byte	0x6a
	.4byte	0x41
	.uleb128 0xc
	.4byte	0x218
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.byte	0x7
	.byte	0x66
	.4byte	0x28f
	.uleb128 0xd
	.4byte	0x265
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF38
	.byte	0x7
	.byte	0x82
	.4byte	0x27e
	.uleb128 0xe
	.byte	0x4
	.byte	0x7
	.2byte	0x126
	.4byte	0x310
	.uleb128 0xf
	.4byte	.LASF39
	.byte	0x7
	.2byte	0x12a
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF40
	.byte	0x7
	.2byte	0x12b
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF41
	.byte	0x7
	.2byte	0x12c
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF42
	.byte	0x7
	.2byte	0x12d
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF43
	.byte	0x7
	.2byte	0x12e
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF44
	.byte	0x7
	.2byte	0x135
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0x7
	.2byte	0x122
	.4byte	0x32b
	.uleb128 0x11
	.4byte	.LASF37
	.byte	0x7
	.2byte	0x124
	.4byte	0x5a
	.uleb128 0xc
	.4byte	0x29a
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0x7
	.2byte	0x120
	.4byte	0x33d
	.uleb128 0xd
	.4byte	0x310
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x12
	.4byte	.LASF45
	.byte	0x7
	.2byte	0x13a
	.4byte	0x32b
	.uleb128 0xe
	.byte	0x94
	.byte	0x7
	.2byte	0x13e
	.4byte	0x457
	.uleb128 0x13
	.4byte	.LASF46
	.byte	0x7
	.2byte	0x14b
	.4byte	0x457
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF47
	.byte	0x7
	.2byte	0x150
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x13
	.4byte	.LASF48
	.byte	0x7
	.2byte	0x153
	.4byte	0x20d
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x13
	.4byte	.LASF49
	.byte	0x7
	.2byte	0x158
	.4byte	0x467
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x13
	.4byte	.LASF50
	.byte	0x7
	.2byte	0x15e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x13
	.4byte	.LASF51
	.byte	0x7
	.2byte	0x160
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x13
	.4byte	.LASF52
	.byte	0x7
	.2byte	0x16a
	.4byte	0x477
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x13
	.4byte	.LASF53
	.byte	0x7
	.2byte	0x170
	.4byte	0x487
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x13
	.4byte	.LASF54
	.byte	0x7
	.2byte	0x17a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x13
	.4byte	.LASF55
	.byte	0x7
	.2byte	0x17e
	.4byte	0x28f
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x13
	.4byte	.LASF56
	.byte	0x7
	.2byte	0x186
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x13
	.4byte	.LASF57
	.byte	0x7
	.2byte	0x191
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x13
	.4byte	.LASF58
	.byte	0x7
	.2byte	0x1b1
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x13
	.4byte	.LASF59
	.byte	0x7
	.2byte	0x1b3
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x13
	.4byte	.LASF60
	.byte	0x7
	.2byte	0x1b9
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x13
	.4byte	.LASF61
	.byte	0x7
	.2byte	0x1c1
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x13
	.4byte	.LASF62
	.byte	0x7
	.2byte	0x1d0
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0x6
	.4byte	0x2c
	.4byte	0x467
	.uleb128 0x7
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0x6
	.4byte	0x33d
	.4byte	0x477
	.uleb128 0x7
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x6
	.4byte	0x2c
	.4byte	0x487
	.uleb128 0x7
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x6
	.4byte	0x2c
	.4byte	0x497
	.uleb128 0x7
	.4byte	0x25
	.byte	0x7
	.byte	0
	.uleb128 0x12
	.4byte	.LASF63
	.byte	0x7
	.2byte	0x1d6
	.4byte	0x349
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF64
	.uleb128 0x6
	.4byte	0x5a
	.4byte	0x4ba
	.uleb128 0x7
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.byte	0x3c
	.byte	0x8
	.byte	0x21
	.4byte	0x533
	.uleb128 0x14
	.4byte	.LASF65
	.byte	0x8
	.byte	0x26
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF66
	.byte	0x8
	.byte	0x2e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x14
	.4byte	.LASF67
	.byte	0x8
	.byte	0x32
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x14
	.4byte	.LASF68
	.byte	0x8
	.byte	0x3a
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x14
	.4byte	.LASF69
	.byte	0x8
	.byte	0x49
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x14
	.4byte	.LASF70
	.byte	0x8
	.byte	0x4b
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x14
	.4byte	.LASF71
	.byte	0x8
	.byte	0x4d
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x14
	.4byte	.LASF72
	.byte	0x8
	.byte	0x53
	.4byte	0x533
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x6
	.4byte	0x5a
	.4byte	0x543
	.uleb128 0x7
	.4byte	0x25
	.byte	0x7
	.byte	0
	.uleb128 0x3
	.4byte	.LASF73
	.byte	0x8
	.byte	0x5a
	.4byte	0x4ba
	.uleb128 0xe
	.byte	0x28
	.byte	0x9
	.2byte	0x3d4
	.4byte	0x5ee
	.uleb128 0x13
	.4byte	.LASF74
	.byte	0x9
	.2byte	0x3d6
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF75
	.byte	0x9
	.2byte	0x3d8
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF76
	.byte	0x9
	.2byte	0x3d9
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF77
	.byte	0x9
	.2byte	0x3db
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF78
	.byte	0x9
	.2byte	0x3dc
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x13
	.4byte	.LASF79
	.byte	0x9
	.2byte	0x3dd
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x13
	.4byte	.LASF80
	.byte	0x9
	.2byte	0x3e0
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x13
	.4byte	.LASF81
	.byte	0x9
	.2byte	0x3e3
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x13
	.4byte	.LASF82
	.byte	0x9
	.2byte	0x3f5
	.4byte	0x4a3
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x13
	.4byte	.LASF83
	.byte	0x9
	.2byte	0x3fa
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x12
	.4byte	.LASF84
	.byte	0x9
	.2byte	0x401
	.4byte	0x54e
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF85
	.uleb128 0xe
	.byte	0x60
	.byte	0x9
	.2byte	0x655
	.4byte	0x6dd
	.uleb128 0x13
	.4byte	.LASF86
	.byte	0x9
	.2byte	0x657
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF87
	.byte	0x9
	.2byte	0x65b
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF88
	.byte	0x9
	.2byte	0x65f
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF89
	.byte	0x9
	.2byte	0x660
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x13
	.4byte	.LASF90
	.byte	0x9
	.2byte	0x661
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x13
	.4byte	.LASF91
	.byte	0x9
	.2byte	0x662
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x13
	.4byte	.LASF92
	.byte	0x9
	.2byte	0x668
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x13
	.4byte	.LASF93
	.byte	0x9
	.2byte	0x669
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x13
	.4byte	.LASF94
	.byte	0x9
	.2byte	0x66a
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x13
	.4byte	.LASF95
	.byte	0x9
	.2byte	0x66b
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x13
	.4byte	.LASF96
	.byte	0x9
	.2byte	0x66c
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x13
	.4byte	.LASF97
	.byte	0x9
	.2byte	0x66d
	.4byte	0x5fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x13
	.4byte	.LASF98
	.byte	0x9
	.2byte	0x671
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x13
	.4byte	.LASF99
	.byte	0x9
	.2byte	0x672
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.byte	0
	.uleb128 0x12
	.4byte	.LASF100
	.byte	0x9
	.2byte	0x674
	.4byte	0x601
	.uleb128 0x15
	.2byte	0x4ac
	.byte	0xa
	.byte	0x1a
	.4byte	0x71e
	.uleb128 0x14
	.4byte	.LASF101
	.byte	0xa
	.byte	0x1c
	.4byte	0x5ee
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF102
	.byte	0xa
	.byte	0x1e
	.4byte	0x71e
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.4byte	.LASF103
	.byte	0xa
	.byte	0x20
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0x4a8
	.byte	0
	.uleb128 0x6
	.4byte	0x6dd
	.4byte	0x72e
	.uleb128 0x7
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x3
	.4byte	.LASF104
	.byte	0xa
	.byte	0x22
	.4byte	0x6e9
	.uleb128 0x16
	.4byte	0x3813c
	.byte	0xa
	.byte	0x24
	.4byte	0x761
	.uleb128 0x14
	.4byte	.LASF105
	.byte	0xa
	.byte	0x26
	.4byte	0x543
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x17
	.ascii	"brr\000"
	.byte	0xa
	.byte	0x2a
	.4byte	0x761
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.byte	0
	.uleb128 0x6
	.4byte	0x72e
	.4byte	0x771
	.uleb128 0x7
	.4byte	0x25
	.byte	0xbf
	.byte	0
	.uleb128 0x3
	.4byte	.LASF106
	.byte	0xa
	.byte	0x2c
	.4byte	0x739
	.uleb128 0x18
	.4byte	.LASF107
	.byte	0x1
	.byte	0x44
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x7a3
	.uleb128 0x19
	.4byte	.LASF110
	.byte	0x1
	.byte	0x44
	.4byte	0x7a3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.byte	0x4
	.4byte	0x72e
	.uleb128 0x18
	.4byte	.LASF108
	.byte	0x1
	.byte	0x4d
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x7ce
	.uleb128 0x1b
	.ascii	"i\000"
	.byte	0x1
	.byte	0x4f
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.4byte	.LASF109
	.byte	0x1
	.byte	0x7a
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x7f5
	.uleb128 0x19
	.4byte	.LASF111
	.byte	0x1
	.byte	0x7a
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.byte	0x1
	.4byte	.LASF112
	.byte	0x1
	.byte	0x9b
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x1c
	.byte	0x1
	.4byte	.LASF113
	.byte	0x1
	.byte	0xbb
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x18
	.4byte	.LASF114
	.byte	0x1
	.byte	0xc7
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x846
	.uleb128 0x19
	.4byte	.LASF115
	.byte	0x1
	.byte	0xc7
	.4byte	0xc1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.byte	0x1
	.4byte	.LASF116
	.byte	0x1
	.byte	0xcf
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF120
	.byte	0x1
	.byte	0xef
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x883
	.uleb128 0x19
	.4byte	.LASF117
	.byte	0x1
	.byte	0xef
	.4byte	0x883
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1a
	.byte	0x4
	.4byte	0x5a
	.uleb128 0x1e
	.4byte	.LASF139
	.byte	0x1
	.2byte	0x111
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x14a
	.byte	0x1
	.4byte	0x7a3
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x8da
	.uleb128 0x20
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x14a
	.4byte	0x7a3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x14c
	.4byte	0x7a3
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x191
	.byte	0x1
	.4byte	0x7a3
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x907
	.uleb128 0x21
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x193
	.4byte	0x7a3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x236
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x931
	.uleb128 0x20
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x236
	.4byte	0x7a3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF122
	.byte	0x1
	.2byte	0x286
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.uleb128 0x24
	.4byte	.LASF123
	.byte	0xb
	.byte	0x30
	.4byte	0x958
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x25
	.4byte	0xcc
	.uleb128 0x24
	.4byte	.LASF124
	.byte	0xb
	.byte	0x34
	.4byte	0x96e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x25
	.4byte	0xcc
	.uleb128 0x24
	.4byte	.LASF125
	.byte	0xb
	.byte	0x36
	.4byte	0x984
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x25
	.4byte	0xcc
	.uleb128 0x24
	.4byte	.LASF126
	.byte	0xb
	.byte	0x38
	.4byte	0x99a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x25
	.4byte	0xcc
	.uleb128 0x26
	.4byte	.LASF129
	.byte	0x7
	.2byte	0x1d9
	.4byte	0x497
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF127
	.byte	0xc
	.byte	0x33
	.4byte	0x9be
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x25
	.4byte	0xdc
	.uleb128 0x24
	.4byte	.LASF128
	.byte	0xc
	.byte	0x3f
	.4byte	0x9d4
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x25
	.4byte	0x4aa
	.uleb128 0x26
	.4byte	.LASF130
	.byte	0xd
	.2byte	0x11b
	.4byte	0xb6
	.byte	0x1
	.byte	0x1
	.uleb128 0x27
	.4byte	.LASF131
	.byte	0xa
	.byte	0x2f
	.4byte	0x771
	.byte	0x1
	.byte	0x1
	.uleb128 0x6
	.4byte	0x7a3
	.4byte	0x9ff
	.uleb128 0x28
	.byte	0
	.uleb128 0x24
	.4byte	.LASF132
	.byte	0x1
	.byte	0x24
	.4byte	0xa10
	.byte	0x5
	.byte	0x3
	.4byte	budget_report_ptrs
	.uleb128 0x1a
	.byte	0x4
	.4byte	0x9f4
	.uleb128 0x24
	.4byte	.LASF133
	.byte	0x1
	.byte	0x2b
	.4byte	0xc1
	.byte	0x5
	.byte	0x3
	.4byte	budget_report_data_ci_timer
	.uleb128 0x6
	.4byte	0x2c
	.4byte	0xa37
	.uleb128 0x7
	.4byte	0x25
	.byte	0x15
	.byte	0
	.uleb128 0x27
	.4byte	.LASF134
	.byte	0x1
	.byte	0x63
	.4byte	0xa44
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	0xa27
	.uleb128 0x6
	.4byte	0x5a
	.4byte	0xa59
	.uleb128 0x7
	.4byte	0x25
	.byte	0
	.byte	0
	.uleb128 0x27
	.4byte	.LASF135
	.byte	0x1
	.byte	0x6c
	.4byte	0xa66
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	0xa49
	.uleb128 0x27
	.4byte	.LASF136
	.byte	0x1
	.byte	0x72
	.4byte	0xa78
	.byte	0x1
	.byte	0x1
	.uleb128 0x25
	.4byte	0xa49
	.uleb128 0x26
	.4byte	.LASF129
	.byte	0x7
	.2byte	0x1d9
	.4byte	0x497
	.byte	0x1
	.byte	0x1
	.uleb128 0x26
	.4byte	.LASF130
	.byte	0xd
	.2byte	0x11b
	.4byte	0xb6
	.byte	0x1
	.byte	0x1
	.uleb128 0x29
	.4byte	.LASF131
	.byte	0x1
	.byte	0x1b
	.4byte	0x771
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	budget_report_data_completed
	.uleb128 0x29
	.4byte	.LASF134
	.byte	0x1
	.byte	0x63
	.4byte	0xabd
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	BUDGET_REPORT_RECORDS_FILENAME
	.uleb128 0x25
	.4byte	0xa27
	.uleb128 0x29
	.4byte	.LASF135
	.byte	0x1
	.byte	0x6c
	.4byte	0xad4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	budget_revision_record_sizes
	.uleb128 0x25
	.4byte	0xa49
	.uleb128 0x29
	.4byte	.LASF136
	.byte	0x1
	.byte	0x72
	.4byte	0xaeb
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	budget_revision_record_counts
	.uleb128 0x25
	.4byte	0xa49
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x6
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI27
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI33
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI36
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x7c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF46:
	.ascii	"nlu_controller_name\000"
.LASF83:
	.ascii	"closing_record_for_the_period\000"
.LASF37:
	.ascii	"size_of_the_union\000"
.LASF103:
	.ascii	"record_date\000"
.LASF131:
	.ascii	"budget_report_data_completed\000"
.LASF51:
	.ascii	"port_B_device_index\000"
.LASF97:
	.ascii	"used_mobile_gallons\000"
.LASF109:
	.ascii	"nm_budget_report_data_updater\000"
.LASF111:
	.ascii	"pfrom_revision\000"
.LASF93:
	.ascii	"used_manual_programmed_gallons\000"
.LASF125:
	.ascii	"GuiFont_DecimalChar\000"
.LASF63:
	.ascii	"CONTROLLER_CONFIGURATION_STRUCT\000"
.LASF78:
	.ascii	"end_date\000"
.LASF68:
	.ascii	"have_returned_next_available_record\000"
.LASF106:
	.ascii	"COMPLETED_BUDGET_REPORT_RECORDS_STRUCT\000"
.LASF13:
	.ascii	"long int\000"
.LASF96:
	.ascii	"used_test_gallons\000"
.LASF81:
	.ascii	"reduction_gallons\000"
.LASF66:
	.ascii	"index_of_next_available\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF24:
	.ascii	"port_a_freewave_sr_set_for_repeater\000"
.LASF60:
	.ascii	"test_seconds\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF112:
	.ascii	"init_file_budget_report_records\000"
.LASF10:
	.ascii	"long long int\000"
.LASF3:
	.ascii	"signed char\000"
.LASF92:
	.ascii	"used_programmed_gallons\000"
.LASF28:
	.ascii	"option_AQUAPONICS\000"
.LASF50:
	.ascii	"port_A_device_index\000"
.LASF98:
	.ascii	"on_at_start_time\000"
.LASF132:
	.ascii	"budget_report_ptrs\000"
.LASF84:
	.ascii	"SYSTEM_BUDGET_REPORT_RECORD\000"
.LASF39:
	.ascii	"nlu_bit_0\000"
.LASF70:
	.ascii	"pending_first_to_send\000"
.LASF41:
	.ascii	"nlu_bit_2\000"
.LASF42:
	.ascii	"nlu_bit_3\000"
.LASF43:
	.ascii	"nlu_bit_4\000"
.LASF14:
	.ascii	"portTickType\000"
.LASF44:
	.ascii	"alert_about_crc_errors\000"
.LASF67:
	.ascii	"have_wrapped\000"
.LASF107:
	.ascii	"nm_init_budget_report_record\000"
.LASF126:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF18:
	.ascii	"option_FL\000"
.LASF53:
	.ascii	"comm_server_port\000"
.LASF57:
	.ascii	"OM_Originator_Retries\000"
.LASF33:
	.ascii	"transport_om_show_unexpected_behavior_alerts\000"
.LASF56:
	.ascii	"dummy\000"
.LASF62:
	.ascii	"hub_enabled_user_setting\000"
.LASF129:
	.ascii	"config_c\000"
.LASF136:
	.ascii	"budget_revision_record_counts\000"
.LASF139:
	.ascii	"nm_BUDGET_increment_next_avail_ptr\000"
.LASF20:
	.ascii	"option_SSE_D\000"
.LASF130:
	.ascii	"budget_report_completed_records_recursive_MUTEX\000"
.LASF117:
	.ascii	"pindex_ptr\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF35:
	.ascii	"use_new_k_and_offset_numbers\000"
.LASF89:
	.ascii	"used_irrigation_gallons\000"
.LASF99:
	.ascii	"off_at_start_time\000"
.LASF49:
	.ascii	"port_settings\000"
.LASF80:
	.ascii	"predicted_use_to_end_of_period\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF137:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF138:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/budget_report_data.c\000"
.LASF29:
	.ascii	"unused_13\000"
.LASF30:
	.ascii	"unused_14\000"
.LASF31:
	.ascii	"unused_15\000"
.LASF72:
	.ascii	"unused_array\000"
.LASF61:
	.ascii	"last_assigned_decoder_serial_number\000"
.LASF94:
	.ascii	"used_manual_gallons\000"
.LASF114:
	.ascii	"budget_report_data_ci_timer_callback\000"
.LASF100:
	.ascii	"BY_POC_BUDGET_RECORD\000"
.LASF15:
	.ascii	"xQueueHandle\000"
.LASF124:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF36:
	.ascii	"show_flow_table_interaction\000"
.LASF120:
	.ascii	"nm_BUDGET_REPORT_DATA_inc_index\000"
.LASF88:
	.ascii	"used_total_gallons\000"
.LASF17:
	.ascii	"xTimerHandle\000"
.LASF133:
	.ascii	"budget_report_data_ci_timer\000"
.LASF95:
	.ascii	"used_walkthru_gallons\000"
.LASF79:
	.ascii	"meter_read_time\000"
.LASF104:
	.ascii	"BUDGET_REPORT_RECORD\000"
.LASF90:
	.ascii	"used_mvor_gallons\000"
.LASF91:
	.ascii	"used_idle_gallons\000"
.LASF121:
	.ascii	"nm_BUDGET_REPORT_DATA_close_and_start_a_new_record\000"
.LASF54:
	.ascii	"nlu_prevent_automated_CI_transmissions\000"
.LASF23:
	.ascii	"port_a_freewave_lr_set_for_repeater\000"
.LASF123:
	.ascii	"GuiFont_LanguageActive\000"
.LASF102:
	.ascii	"bpbr\000"
.LASF86:
	.ascii	"poc_gid\000"
.LASF22:
	.ascii	"port_a_raveon_radio_type\000"
.LASF27:
	.ascii	"port_b_freewave_sr_set_for_repeater\000"
.LASF135:
	.ascii	"budget_revision_record_sizes\000"
.LASF110:
	.ascii	"pbrr_ptr\000"
.LASF55:
	.ascii	"debug\000"
.LASF64:
	.ascii	"float\000"
.LASF118:
	.ascii	"nm_BUDGET_REPORT_RECORDS_get_previous_completed_rec"
	.ascii	"ord\000"
.LASF47:
	.ascii	"serial_number\000"
.LASF101:
	.ascii	"sbrr\000"
.LASF16:
	.ascii	"xSemaphoreHandle\000"
.LASF48:
	.ascii	"purchased_options\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF40:
	.ascii	"nlu_bit_1\000"
.LASF122:
	.ascii	"BUDGET_REPORT_free_report_support\000"
.LASF74:
	.ascii	"system_gid\000"
.LASF5:
	.ascii	"short int\000"
.LASF73:
	.ascii	"REPORT_DATA_FILE_BASE_STRUCT\000"
.LASF113:
	.ascii	"save_file_budget_report_records\000"
.LASF69:
	.ascii	"first_to_send\000"
.LASF108:
	.ascii	"nm_init_budget_report_records\000"
.LASF75:
	.ascii	"in_use\000"
.LASF115:
	.ascii	"pxTimer\000"
.LASF19:
	.ascii	"option_SSE\000"
.LASF1:
	.ascii	"char\000"
.LASF105:
	.ascii	"rdfb\000"
.LASF76:
	.ascii	"mode\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF82:
	.ascii	"ratio\000"
.LASF71:
	.ascii	"pending_first_to_send_in_use\000"
.LASF119:
	.ascii	"nm_BUDGET_REPORT_RECORDS_get_most_recently_complete"
	.ascii	"d_record\000"
.LASF26:
	.ascii	"port_b_freewave_lr_set_for_repeater\000"
.LASF25:
	.ascii	"port_b_raveon_radio_type\000"
.LASF32:
	.ascii	"PURCHASED_OPTIONS_STRUCT\000"
.LASF87:
	.ascii	"budget\000"
.LASF128:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF116:
	.ascii	"BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_"
	.ascii	"running\000"
.LASF6:
	.ascii	"UNS_16\000"
.LASF58:
	.ascii	"OM_Seconds_for_Status_FOAL\000"
.LASF65:
	.ascii	"roll_time\000"
.LASF59:
	.ascii	"OM_Minutes_To_Exist\000"
.LASF38:
	.ascii	"DEBUG_BITS_STRUCT\000"
.LASF77:
	.ascii	"start_date\000"
.LASF34:
	.ascii	"transport_om_show_status_timer_expired_alert\000"
.LASF52:
	.ascii	"comm_server_ip_address\000"
.LASF12:
	.ascii	"BITFIELD_BOOL\000"
.LASF127:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF134:
	.ascii	"BUDGET_REPORT_RECORDS_FILENAME\000"
.LASF45:
	.ascii	"CONFIGURATION_PORT_CONTROL_STRUCT\000"
.LASF85:
	.ascii	"double\000"
.LASF21:
	.ascii	"option_HUB\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
