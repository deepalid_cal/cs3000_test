	.file	"chain_sync_vars.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	cscs
	.section	.bss.cscs,"aw",%nobits
	.align	2
	.type	cscs, %object
	.size	cscs, 256
cscs:
	.space	256
	.global	chain_sync_file_pertinants
	.section .rodata
	.align	2
.LC0:
	.ascii	"\000"
	.align	2
.LC1:
	.ascii	"Network Config\000"
	.align	2
.LC2:
	.ascii	"Weather Control\000"
	.align	2
.LC3:
	.ascii	"Stations\000"
	.align	2
.LC4:
	.ascii	"Mainline\000"
	.align	2
.LC5:
	.ascii	"POC\000"
	.align	2
.LC6:
	.ascii	"Manual Programs\000"
	.align	2
.LC7:
	.ascii	"Station Group\000"
	.align	2
.LC8:
	.ascii	"Lights\000"
	.align	2
.LC9:
	.ascii	"Moisture Sensor\000"
	.align	2
.LC10:
	.ascii	"Walk Thru\000"
	.section	.rodata.chain_sync_file_pertinants,"a",%progbits
	.align	2
	.type	chain_sync_file_pertinants, %object
	.size	chain_sync_file_pertinants, 336
chain_sync_file_pertinants:
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC1
	.word	NETWORK_CONFIG_calculate_chain_sync_crc
	.word	NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	.word	0
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC2
	.word	WEATHER_CONTROL_calculate_chain_sync_crc
	.word	WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	.word	0
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC3
	.word	STATION_calculate_chain_sync_crc
	.word	STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token
	.word	STATION_clean_house_processing
	.word	.LC4
	.word	IRRIGATION_SYSTEM_calculate_chain_sync_crc
	.word	IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token
	.word	SYSTEM_clean_house_processing
	.word	.LC5
	.word	POC_calculate_chain_sync_crc
	.word	POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token
	.word	POC_clean_house_processing
	.word	.LC6
	.word	MANUAL_PROGRAMS_calculate_chain_sync_crc
	.word	MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.word	MANUAL_PROGRAMS_clean_house_processing
	.word	.LC7
	.word	STATION_GROUPS_calculate_chain_sync_crc
	.word	STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.word	STATION_GROUP_clean_house_processing
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC8
	.word	LIGHTS_calculate_chain_sync_crc
	.word	LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token
	.word	LIGHTS_clean_house_processing
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC9
	.word	MOISTURE_SENSOR_calculate_chain_sync_crc
	.word	MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token
	.word	MOISTURE_SENSOR_clean_house_processing
	.word	.LC0
	.word	0
	.word	0
	.word	0
	.word	.LC10
	.word	WALK_THRU_calculate_chain_sync_crc
	.word	WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.word	WALK_THRU_clean_house_processing
	.section .rodata
	.align	2
.LC11:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/flas"
	.ascii	"h_storage/chain_sync_vars.c\000"
	.section	.text.init_chain_sync_vars,"ax",%progbits
	.align	2
	.global	init_chain_sync_vars
	.type	init_chain_sync_vars, %function
init_chain_sync_vars:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/flash_storage/chain_sync_vars.c"
	.loc 1 314 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	.loc 1 322 0
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L2+4
	ldr	r3, .L2+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 326 0
	ldr	r3, .L2+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 328 0
	ldr	r0, .L2+16
	mov	r1, #0
	mov	r2, #84
	bl	memset
	.loc 1 330 0
	ldr	r0, .L2+20
	mov	r1, #0
	mov	r2, #84
	bl	memset
	.loc 1 332 0
	ldr	r0, .L2+24
	mov	r1, #0
	mov	r2, #84
	bl	memset
	.loc 1 336 0
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 337 0
	ldmfd	sp!, {fp, pc}
.L3:
	.align	2
.L2:
	.word	chain_sync_control_structure_recursive_MUTEX
	.word	.LC11
	.word	322
	.word	cscs
	.word	cscs+4
	.word	cscs+88
	.word	cscs+172
.LFE0:
	.size	init_chain_sync_vars, .-init_chain_sync_vars
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/flash_storage/chain_sync_vars.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x2cf
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF36
	.byte	0x1
	.4byte	.LASF37
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x4
	.byte	0x4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x5
	.4byte	.LASF8
	.byte	0x2
	.byte	0x35
	.4byte	0x25
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x3
	.byte	0x57
	.4byte	0x3a
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x4
	.byte	0x4c
	.4byte	0x71
	.uleb128 0x6
	.4byte	0x51
	.4byte	0x97
	.uleb128 0x7
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF11
	.uleb128 0x5
	.4byte	.LASF12
	.byte	0x5
	.byte	0x5e
	.4byte	0xa9
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x5
	.4byte	.LASF14
	.byte	0x5
	.byte	0x99
	.4byte	0xa9
	.uleb128 0x8
	.2byte	0x100
	.byte	0x6
	.byte	0x2f
	.4byte	0xfe
	.uleb128 0x9
	.4byte	.LASF15
	.byte	0x6
	.byte	0x34
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x6
	.byte	0x3b
	.4byte	0xfe
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x6
	.byte	0x46
	.4byte	0x10e
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x6
	.byte	0x50
	.4byte	0xfe
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.byte	0
	.uleb128 0x6
	.4byte	0x9e
	.4byte	0x10e
	.uleb128 0x7
	.4byte	0x25
	.byte	0x14
	.byte	0
	.uleb128 0x6
	.4byte	0xb0
	.4byte	0x11e
	.uleb128 0x7
	.4byte	0x25
	.byte	0x14
	.byte	0
	.uleb128 0x5
	.4byte	.LASF19
	.byte	0x6
	.byte	0x52
	.4byte	0xbb
	.uleb128 0xa
	.byte	0x10
	.byte	0x6
	.byte	0x5a
	.4byte	0x16a
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x6
	.byte	0x61
	.4byte	0x16a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x6
	.byte	0x63
	.4byte	0x185
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x6
	.byte	0x65
	.4byte	0x192
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x6
	.byte	0x6a
	.4byte	0x192
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x97
	.uleb128 0xc
	.byte	0x1
	.4byte	0xb0
	.4byte	0x180
	.uleb128 0xd
	.4byte	0x180
	.byte	0
	.uleb128 0xe
	.4byte	0x9e
	.uleb128 0xe
	.4byte	0x18a
	.uleb128 0xb
	.byte	0x4
	.4byte	0x170
	.uleb128 0xf
	.byte	0x1
	.uleb128 0xe
	.4byte	0x197
	.uleb128 0xb
	.byte	0x4
	.4byte	0x190
	.uleb128 0x5
	.4byte	.LASF24
	.byte	0x6
	.byte	0x6c
	.4byte	0x129
	.uleb128 0x6
	.4byte	0x9e
	.4byte	0x1b8
	.uleb128 0x7
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF25
	.uleb128 0x6
	.4byte	0x9e
	.4byte	0x1cf
	.uleb128 0x7
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF26
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x139
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x11
	.4byte	.LASF27
	.byte	0x7
	.byte	0x30
	.4byte	0x1fd
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0x87
	.uleb128 0x11
	.4byte	.LASF28
	.byte	0x7
	.byte	0x34
	.4byte	0x213
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0x87
	.uleb128 0x11
	.4byte	.LASF29
	.byte	0x7
	.byte	0x36
	.4byte	0x229
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0x87
	.uleb128 0x11
	.4byte	.LASF30
	.byte	0x7
	.byte	0x38
	.4byte	0x23f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0x87
	.uleb128 0x12
	.4byte	.LASF31
	.byte	0x6
	.byte	0x55
	.4byte	0x11e
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF32
	.byte	0x8
	.2byte	0x122
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF33
	.byte	0x9
	.byte	0x33
	.4byte	0x270
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xe
	.4byte	0x1a8
	.uleb128 0x11
	.4byte	.LASF34
	.byte	0x9
	.byte	0x3f
	.4byte	0x286
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xe
	.4byte	0x1bf
	.uleb128 0x14
	.4byte	.LASF31
	.byte	0x1
	.byte	0x23
	.4byte	0x11e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	cscs
	.uleb128 0x6
	.4byte	0x19d
	.4byte	0x2ad
	.uleb128 0x7
	.4byte	0x25
	.byte	0x14
	.byte	0
	.uleb128 0x14
	.4byte	.LASF35
	.byte	0x1
	.byte	0x44
	.4byte	0x2bf
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	chain_sync_file_pertinants
	.uleb128 0xe
	.4byte	0x29d
	.uleb128 0x13
	.4byte	.LASF32
	.byte	0x8
	.2byte	0x122
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF27:
	.ascii	"GuiFont_LanguageActive\000"
.LASF8:
	.ascii	"portTickType\000"
.LASF20:
	.ascii	"file_name_string\000"
.LASF36:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF10:
	.ascii	"xSemaphoreHandle\000"
.LASF15:
	.ascii	"ff_next_file_crc_to_send_0\000"
.LASF25:
	.ascii	"float\000"
.LASF9:
	.ascii	"xQueueHandle\000"
.LASF18:
	.ascii	"the_crc\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF19:
	.ascii	"CHAIN_SYNC_CONTROL_STRUCTURE\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF30:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF17:
	.ascii	"crc_is_valid\000"
.LASF24:
	.ascii	"CHAIN_SYNC_FILE_PERTINANTS\000"
.LASF33:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF21:
	.ascii	"__crc_calculation_function_ptr\000"
.LASF28:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF26:
	.ascii	"double\000"
.LASF31:
	.ascii	"cscs\000"
.LASF37:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/flas"
	.ascii	"h_storage/chain_sync_vars.c\000"
.LASF13:
	.ascii	"unsigned int\000"
.LASF11:
	.ascii	"char\000"
.LASF35:
	.ascii	"chain_sync_file_pertinants\000"
.LASF34:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF14:
	.ascii	"BOOL_32\000"
.LASF6:
	.ascii	"long long int\000"
.LASF32:
	.ascii	"chain_sync_control_structure_recursive_MUTEX\000"
.LASF22:
	.ascii	"__set_bits_on_all_groups_to_cause_distribution_in_t"
	.ascii	"he_next_token\000"
.LASF16:
	.ascii	"clean_tokens_since_change_detected_or_file_save\000"
.LASF29:
	.ascii	"GuiFont_DecimalChar\000"
.LASF3:
	.ascii	"short int\000"
.LASF23:
	.ascii	"__clean_house_processing\000"
.LASF12:
	.ascii	"UNS_32\000"
.LASF38:
	.ascii	"init_chain_sync_vars\000"
.LASF4:
	.ascii	"long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF7:
	.ascii	"long long unsigned int\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
