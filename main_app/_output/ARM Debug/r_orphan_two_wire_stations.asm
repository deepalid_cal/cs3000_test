	.file	"r_orphan_two_wire_stations.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.ORPHAN_STATIONS_list_of_orphans,"aw",%nobits
	.align	2
	.type	ORPHAN_STATIONS_list_of_orphans, %object
	.size	ORPHAN_STATIONS_list_of_orphans, 2112
ORPHAN_STATIONS_list_of_orphans:
	.space	2112
	.section	.bss.g_ORPHAN_TWO_WIRE_STATIONS_num_stations,"aw",%nobits
	.align	2
	.type	g_ORPHAN_TWO_WIRE_STATIONS_num_stations, %object
	.size	g_ORPHAN_TWO_WIRE_STATIONS_num_stations, 4
g_ORPHAN_TWO_WIRE_STATIONS_num_stations:
	.space	4
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_orphan_two_wire_stations.c\000"
	.section	.text.ORPHAN_TWO_WIRE_STATIONS_populate_list,"ax",%progbits
	.align	2
	.global	ORPHAN_TWO_WIRE_STATIONS_populate_list
	.type	ORPHAN_TWO_WIRE_STATIONS_populate_list, %function
ORPHAN_TWO_WIRE_STATIONS_populate_list:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_orphan_two_wire_stations.c"
	.loc 1 60 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI0:
	add	fp, sp, #8
.LCFI1:
	sub	sp, sp, #24
.LCFI2:
	.loc 1 73 0
	ldr	r3, .L13
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 75 0
	bl	FLOWSENSE_get_controller_index
	str	r0, [fp, #-24]
	.loc 1 79 0
	ldr	r3, .L13+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L13+8
	mov	r3, #79
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 87 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L2
.L11:
	.loc 1 89 0
	ldr	r0, .L13+12
	ldr	r1, [fp, #-16]
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	str	r0, [fp, #-28]
	.loc 1 91 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	beq	.L12
.L3:
	.loc 1 100 0
	ldr	r0, [fp, #-28]
	bl	nm_STATION_get_box_index_0
	mov	r2, r0
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bne	.L5
	.loc 1 102 0
	ldr	r0, [fp, #-28]
	bl	nm_STATION_get_decoder_serial_number
	str	r0, [fp, #-32]
	.loc 1 104 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 106 0
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	beq	.L5
	.loc 1 108 0
	ldr	r3, .L13+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L13+8
	mov	r3, #108
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 110 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L6
.L9:
	.loc 1 112 0
	ldr	r0, .L13+20
	ldr	r2, [fp, #-20]
	mov	r1, #220
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bne	.L7
	.loc 1 114 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 119 0
	b	.L8
.L7:
	.loc 1 110 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L6:
	.loc 1 110 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #79
	bls	.L9
.L8:
	.loc 1 127 0 is_stmt 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L10
	.loc 1 129 0
	ldr	r3, .L13
	ldr	r4, [r3, #0]
	ldr	r0, [fp, #-28]
	bl	nm_STATION_get_station_number_0
	mov	r2, r0
	ldr	r1, .L13+24
	mov	r3, r4
	mov	r3, r3, asl #1
	add	r3, r3, r4
	mov	r3, r3, asl #2
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 1 131 0
	ldr	r3, .L13
	ldr	r2, [r3, #0]
	ldr	r0, .L13+24
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [fp, #-32]
	str	r2, [r3, #0]
	.loc 1 133 0
	ldr	r3, .L13
	ldr	r4, [r3, #0]
	ldr	r0, [fp, #-28]
	bl	nm_STATION_get_decoder_output
	mov	r2, r0
	ldr	r0, .L13+24
	mov	r1, #8
	mov	r3, r4
	mov	r3, r3, asl #1
	add	r3, r3, r4
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	str	r2, [r3, #0]
	.loc 1 135 0
	ldr	r3, .L13
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L13
	str	r2, [r3, #0]
.L10:
	.loc 1 138 0
	ldr	r3, .L13+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L5:
	.loc 1 87 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L2:
	.loc 1 87 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-16]
	ldr	r3, .L13+28
	cmp	r2, r3
	bls	.L11
	b	.L4
.L12:
	.loc 1 95 0 is_stmt 1
	mov	r0, r0	@ nop
.L4:
	.loc 1 143 0
	ldr	r3, .L13+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 145 0
	ldr	r3, .L13
	ldr	r3, [r3, #0]
	.loc 1 146 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L14:
	.align	2
.L13:
	.word	g_ORPHAN_TWO_WIRE_STATIONS_num_stations
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	station_info_list_hdr
	.word	tpmicro_data_recursive_MUTEX
	.word	tpmicro_data
	.word	ORPHAN_STATIONS_list_of_orphans
	.word	2111
.LFE0:
	.size	ORPHAN_TWO_WIRE_STATIONS_populate_list, .-ORPHAN_TWO_WIRE_STATIONS_populate_list
	.section .rodata
	.align	2
.LC1:
	.ascii	"%s\000"
	.section	.text.ORPHAN_TWO_WIRE_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.type	ORPHAN_TWO_WIRE_load_guivars_for_scroll_line, %function
ORPHAN_TWO_WIRE_load_guivars_for_scroll_line:
.LFB1:
	.loc 1 150 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #20
.LCFI5:
	mov	r3, r0
	strh	r3, [fp, #-24]	@ movhi
	.loc 1 153 0
	bl	FLOWSENSE_get_controller_index
	mov	r1, r0
	ldrsh	r2, [fp, #-24]
	ldr	r0, .L16
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	ldr	r2, [r3, #0]
	sub	r3, fp, #20
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #16
	bl	STATION_get_station_number_string
	mov	r3, r0
	ldr	r0, .L16+4
	mov	r1, #4
	ldr	r2, .L16+8
	bl	snprintf
	.loc 1 155 0
	ldrsh	r2, [fp, #-24]
	ldr	r0, .L16
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L16+12
	str	r2, [r3, #0]
	.loc 1 157 0
	ldrsh	r2, [fp, #-24]
	ldr	r0, .L16
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L16+16
	str	r2, [r3, #0]
	.loc 1 158 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	ORPHAN_STATIONS_list_of_orphans
	.word	GuiVar_RptStation
	.word	.LC1
	.word	GuiVar_RptDecoderSN
	.word	GuiVar_RptDecoderOutput
.LFE1:
	.size	ORPHAN_TWO_WIRE_load_guivars_for_scroll_line, .-ORPHAN_TWO_WIRE_load_guivars_for_scroll_line
	.section	.text.FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report
	.type	FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report, %function
FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report:
.LFB2:
	.loc 1 162 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #4
.LCFI8:
	str	r0, [fp, #-8]
	.loc 1 163 0
	mov	r0, #89
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 165 0
	ldr	r3, .L19
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, .L19+4
	bl	FDTO_REPORTS_draw_report
	.loc 1 166 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L20:
	.align	2
.L19:
	.word	g_ORPHAN_TWO_WIRE_STATIONS_num_stations
	.word	ORPHAN_TWO_WIRE_load_guivars_for_scroll_line
.LFE2:
	.size	FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report, .-FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report
	.section	.text.ORPHAN_TWO_WIRE_STATIONS_process_report,"ax",%progbits
	.align	2
	.global	ORPHAN_TWO_WIRE_STATIONS_process_report
	.type	ORPHAN_TWO_WIRE_STATIONS_process_report, %function
ORPHAN_TWO_WIRE_STATIONS_process_report:
.LFB3:
	.loc 1 170 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #28
.LCFI11:
	str	r0, [fp, #-20]
	str	r1, [fp, #-16]
	.loc 1 177 0
	bl	FLOWSENSE_get_controller_index
	str	r0, [fp, #-8]
	.loc 1 181 0
	ldr	r3, [fp, #-20]
	cmp	r3, #2
	beq	.L23
	cmp	r3, #67
	beq	.L24
	b	.L28
.L23:
	.loc 1 184 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	cmp	r3, #0
	blt	.L25
	.loc 1 186 0
	bl	good_key_beep
	.loc 1 188 0
	ldr	r3, .L29
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L29+4
	mov	r3, #188
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 190 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r1, .L29+8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	bl	nm_STATION_get_pointer_to_station
	str	r0, [fp, #-12]
	.loc 1 192 0
	ldr	r0, [fp, #-12]
	mov	r1, #2
	bl	STATION_get_change_bits_ptr
	mov	r3, r0
	ldr	r2, [fp, #-8]
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-12]
	mov	r1, #0
	mov	r2, #1
	mov	r3, #2
	bl	nm_STATION_set_decoder_serial_number
	.loc 1 194 0
	ldr	r3, .L29
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 198 0
	bl	ORPHAN_TWO_WIRE_STATIONS_populate_list
	.loc 1 200 0
	mov	r0, #1
	bl	Redraw_Screen
	.loc 1 206 0
	b	.L21
.L25:
	.loc 1 204 0
	bl	bad_key_beep
	.loc 1 206 0
	b	.L21
.L24:
	.loc 1 209 0
	ldr	r3, .L29+12
	ldr	r2, [r3, #0]
	ldr	r0, .L29+16
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L29+20
	str	r2, [r3, #0]
	.loc 1 211 0
	sub	r1, fp, #20
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 215 0
	mov	r0, #1
	bl	TWO_WIRE_ASSIGNMENT_draw_dialog
	.loc 1 216 0
	b	.L21
.L28:
	.loc 1 219 0
	sub	r1, fp, #20
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #0
	bl	REPORTS_process_report
.L21:
	.loc 1 221 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L30:
	.align	2
.L29:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	ORPHAN_STATIONS_list_of_orphans
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	ORPHAN_TWO_WIRE_STATIONS_process_report, .-ORPHAN_TWO_WIRE_STATIONS_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/stdint.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/stations.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/../decoder/_common_decoder_files/shared_to_cs3000/data_types.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/../decoder/_common_decoder_files/shared_to_cs3000/eeprom.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/../decoder/_common_decoder_files/shared_to_cs3000/protocolmsgs.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_tpmicro_common.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/tp_board/tpmicro_data.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xcc0
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF180
	.byte	0x1
	.4byte	.LASF181
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x45
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x69
	.uleb128 0x5
	.byte	0x4
	.4byte	0x96
	.uleb128 0x6
	.4byte	0x9d
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x11
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x12
	.4byte	0x45
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x4
	.byte	0x35
	.4byte	0x9d
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x5
	.byte	0x57
	.4byte	0xa4
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x6
	.byte	0x4c
	.4byte	0xce
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0xf4
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x7
	.byte	0x7c
	.4byte	0x119
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x7
	.byte	0x7e
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x7
	.byte	0x80
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x7
	.byte	0x82
	.4byte	0xf4
	.uleb128 0xb
	.byte	0x14
	.byte	0x8
	.byte	0x18
	.4byte	0x173
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x8
	.byte	0x1a
	.4byte	0xa4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x8
	.byte	0x1c
	.4byte	0xa4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x8
	.byte	0x1e
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x8
	.byte	0x20
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x8
	.byte	0x23
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF27
	.byte	0x8
	.byte	0x26
	.4byte	0x124
	.uleb128 0xb
	.byte	0x24
	.byte	0x9
	.byte	0x78
	.4byte	0x205
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x9
	.byte	0x7b
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x9
	.byte	0x83
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x9
	.byte	0x86
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x9
	.byte	0x88
	.4byte	0x216
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF32
	.byte	0x9
	.byte	0x8d
	.4byte	0x228
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x9
	.byte	0x92
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF34
	.byte	0x9
	.byte	0x96
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF35
	.byte	0x9
	.byte	0x9a
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF36
	.byte	0x9
	.byte	0x9c
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	0x211
	.uleb128 0xe
	.4byte	0x211
	.byte	0
	.uleb128 0xf
	.4byte	0x4c
	.uleb128 0x5
	.byte	0x4
	.4byte	0x205
	.uleb128 0xd
	.byte	0x1
	.4byte	0x228
	.uleb128 0xe
	.4byte	0x119
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x21c
	.uleb128 0x3
	.4byte	.LASF37
	.byte	0x9
	.byte	0x9e
	.4byte	0x17e
	.uleb128 0x3
	.4byte	.LASF38
	.byte	0xa
	.byte	0x69
	.4byte	0x244
	.uleb128 0x10
	.4byte	.LASF38
	.byte	0x1
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF39
	.uleb128 0x11
	.ascii	"U16\000"
	.byte	0xb
	.byte	0xb
	.4byte	0xb8
	.uleb128 0x11
	.ascii	"U8\000"
	.byte	0xb
	.byte	0xc
	.4byte	0xad
	.uleb128 0xb
	.byte	0x1d
	.byte	0xc
	.byte	0x9b
	.4byte	0x3e9
	.uleb128 0xc
	.4byte	.LASF40
	.byte	0xc
	.byte	0x9d
	.4byte	0x251
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF41
	.byte	0xc
	.byte	0x9e
	.4byte	0x251
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xc
	.4byte	.LASF42
	.byte	0xc
	.byte	0x9f
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF43
	.byte	0xc
	.byte	0xa0
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x5
	.uleb128 0xc
	.4byte	.LASF44
	.byte	0xc
	.byte	0xa1
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xc
	.4byte	.LASF45
	.byte	0xc
	.byte	0xa2
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x7
	.uleb128 0xc
	.4byte	.LASF46
	.byte	0xc
	.byte	0xa3
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF47
	.byte	0xc
	.byte	0xa4
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x9
	.uleb128 0xc
	.4byte	.LASF48
	.byte	0xc
	.byte	0xa5
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xc
	.4byte	.LASF49
	.byte	0xc
	.byte	0xa6
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0xb
	.uleb128 0xc
	.4byte	.LASF50
	.byte	0xc
	.byte	0xa7
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF51
	.byte	0xc
	.byte	0xa8
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0xc
	.4byte	.LASF52
	.byte	0xc
	.byte	0xa9
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0xc
	.4byte	.LASF53
	.byte	0xc
	.byte	0xaa
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0xf
	.uleb128 0xc
	.4byte	.LASF54
	.byte	0xc
	.byte	0xab
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF55
	.byte	0xc
	.byte	0xac
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.uleb128 0xc
	.4byte	.LASF56
	.byte	0xc
	.byte	0xad
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0xc
	.4byte	.LASF57
	.byte	0xc
	.byte	0xae
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.uleb128 0xc
	.4byte	.LASF58
	.byte	0xc
	.byte	0xaf
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF59
	.byte	0xc
	.byte	0xb0
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x15
	.uleb128 0xc
	.4byte	.LASF60
	.byte	0xc
	.byte	0xb1
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x16
	.uleb128 0xc
	.4byte	.LASF61
	.byte	0xc
	.byte	0xb2
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x17
	.uleb128 0xc
	.4byte	.LASF62
	.byte	0xc
	.byte	0xb3
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF63
	.byte	0xc
	.byte	0xb4
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x19
	.uleb128 0xc
	.4byte	.LASF64
	.byte	0xc
	.byte	0xb5
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1a
	.uleb128 0xc
	.4byte	.LASF65
	.byte	0xc
	.byte	0xb6
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1b
	.uleb128 0xc
	.4byte	.LASF66
	.byte	0xc
	.byte	0xb7
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x3
	.4byte	.LASF67
	.byte	0xc
	.byte	0xb9
	.4byte	0x266
	.uleb128 0x12
	.byte	0x4
	.byte	0xd
	.2byte	0x16b
	.4byte	0x42b
	.uleb128 0x13
	.4byte	.LASF68
	.byte	0xd
	.2byte	0x16d
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF69
	.byte	0xd
	.2byte	0x16e
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x13
	.4byte	.LASF70
	.byte	0xd
	.2byte	0x16f
	.4byte	0x251
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x14
	.4byte	.LASF71
	.byte	0xd
	.2byte	0x171
	.4byte	0x3f4
	.uleb128 0x12
	.byte	0xb
	.byte	0xd
	.2byte	0x193
	.4byte	0x48c
	.uleb128 0x13
	.4byte	.LASF72
	.byte	0xd
	.2byte	0x195
	.4byte	0x42b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF73
	.byte	0xd
	.2byte	0x196
	.4byte	0x42b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF74
	.byte	0xd
	.2byte	0x197
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF75
	.byte	0xd
	.2byte	0x198
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x9
	.uleb128 0x13
	.4byte	.LASF76
	.byte	0xd
	.2byte	0x199
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.byte	0
	.uleb128 0x14
	.4byte	.LASF77
	.byte	0xd
	.2byte	0x19b
	.4byte	0x437
	.uleb128 0x12
	.byte	0x4
	.byte	0xd
	.2byte	0x221
	.4byte	0x4cf
	.uleb128 0x13
	.4byte	.LASF78
	.byte	0xd
	.2byte	0x223
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF79
	.byte	0xd
	.2byte	0x225
	.4byte	0x25c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x13
	.4byte	.LASF80
	.byte	0xd
	.2byte	0x227
	.4byte	0x251
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x14
	.4byte	.LASF81
	.byte	0xd
	.2byte	0x229
	.4byte	0x498
	.uleb128 0xb
	.byte	0xc
	.byte	0xe
	.byte	0x25
	.4byte	0x50c
	.uleb128 0x15
	.ascii	"sn\000"
	.byte	0xe
	.byte	0x28
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF82
	.byte	0xe
	.byte	0x2b
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.ascii	"on\000"
	.byte	0xe
	.byte	0x2e
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF83
	.byte	0xe
	.byte	0x30
	.4byte	0x4db
	.uleb128 0x12
	.byte	0x4
	.byte	0xe
	.2byte	0x193
	.4byte	0x530
	.uleb128 0x13
	.4byte	.LASF84
	.byte	0xe
	.2byte	0x196
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x14
	.4byte	.LASF85
	.byte	0xe
	.2byte	0x198
	.4byte	0x517
	.uleb128 0x12
	.byte	0xc
	.byte	0xe
	.2byte	0x1b0
	.4byte	0x573
	.uleb128 0x13
	.4byte	.LASF86
	.byte	0xe
	.2byte	0x1b2
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF87
	.byte	0xe
	.2byte	0x1b7
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF88
	.byte	0xe
	.2byte	0x1bc
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x14
	.4byte	.LASF89
	.byte	0xe
	.2byte	0x1be
	.4byte	0x53c
	.uleb128 0x12
	.byte	0x4
	.byte	0xe
	.2byte	0x1c3
	.4byte	0x598
	.uleb128 0x13
	.4byte	.LASF90
	.byte	0xe
	.2byte	0x1ca
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x14
	.4byte	.LASF91
	.byte	0xe
	.2byte	0x1d0
	.4byte	0x57f
	.uleb128 0x16
	.4byte	.LASF182
	.byte	0x10
	.byte	0xe
	.2byte	0x1ff
	.4byte	0x5ee
	.uleb128 0x13
	.4byte	.LASF92
	.byte	0xe
	.2byte	0x202
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF93
	.byte	0xe
	.2byte	0x205
	.4byte	0x4cf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF94
	.byte	0xe
	.2byte	0x207
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF95
	.byte	0xe
	.2byte	0x20c
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x14
	.4byte	.LASF96
	.byte	0xe
	.2byte	0x211
	.4byte	0x5fa
	.uleb128 0x9
	.4byte	0x5a4
	.4byte	0x60a
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x7
	.byte	0
	.uleb128 0x12
	.byte	0xc
	.byte	0xe
	.2byte	0x3a4
	.4byte	0x66e
	.uleb128 0x13
	.4byte	.LASF97
	.byte	0xe
	.2byte	0x3a6
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF98
	.byte	0xe
	.2byte	0x3a8
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x13
	.4byte	.LASF99
	.byte	0xe
	.2byte	0x3aa
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF100
	.byte	0xe
	.2byte	0x3ac
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x13
	.4byte	.LASF101
	.byte	0xe
	.2byte	0x3ae
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF102
	.byte	0xe
	.2byte	0x3b0
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.byte	0
	.uleb128 0x14
	.4byte	.LASF103
	.byte	0xe
	.2byte	0x3b2
	.4byte	0x60a
	.uleb128 0x9
	.4byte	0x25
	.4byte	0x68a
	.uleb128 0xa
	.4byte	0x9d
	.byte	0xf
	.byte	0
	.uleb128 0x9
	.4byte	0x5e
	.4byte	0x69a
	.uleb128 0xa
	.4byte	0x9d
	.byte	0xb
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0xf
	.byte	0x1d
	.4byte	0x6bf
	.uleb128 0xc
	.4byte	.LASF104
	.byte	0xf
	.byte	0x20
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF105
	.byte	0xf
	.byte	0x25
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF106
	.byte	0xf
	.byte	0x27
	.4byte	0x69a
	.uleb128 0xb
	.byte	0x8
	.byte	0xf
	.byte	0x29
	.4byte	0x6ee
	.uleb128 0xc
	.4byte	.LASF107
	.byte	0xf
	.byte	0x2c
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.ascii	"on\000"
	.byte	0xf
	.byte	0x2f
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF108
	.byte	0xf
	.byte	0x31
	.4byte	0x6ca
	.uleb128 0xb
	.byte	0x3c
	.byte	0xf
	.byte	0x3c
	.4byte	0x747
	.uleb128 0x15
	.ascii	"sn\000"
	.byte	0xf
	.byte	0x40
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF93
	.byte	0xf
	.byte	0x45
	.4byte	0x4cf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF109
	.byte	0xf
	.byte	0x4a
	.4byte	0x3e9
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF110
	.byte	0xf
	.byte	0x4f
	.4byte	0x48c
	.byte	0x2
	.byte	0x23
	.uleb128 0x25
	.uleb128 0xc
	.4byte	.LASF111
	.byte	0xf
	.byte	0x56
	.4byte	0x66e
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.byte	0
	.uleb128 0x3
	.4byte	.LASF112
	.byte	0xf
	.byte	0x5a
	.4byte	0x6f9
	.uleb128 0x17
	.2byte	0x156c
	.byte	0xf
	.byte	0x82
	.4byte	0x972
	.uleb128 0xc
	.4byte	.LASF113
	.byte	0xf
	.byte	0x87
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF114
	.byte	0xf
	.byte	0x8e
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF115
	.byte	0xf
	.byte	0x96
	.4byte	0x530
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF116
	.byte	0xf
	.byte	0x9f
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF117
	.byte	0xf
	.byte	0xa6
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF118
	.byte	0xf
	.byte	0xab
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF119
	.byte	0xf
	.byte	0xad
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF120
	.byte	0xf
	.byte	0xaf
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF121
	.byte	0xf
	.byte	0xb4
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xc
	.4byte	.LASF122
	.byte	0xf
	.byte	0xbb
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xc
	.4byte	.LASF123
	.byte	0xf
	.byte	0xbc
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.4byte	.LASF124
	.byte	0xf
	.byte	0xbd
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xc
	.4byte	.LASF125
	.byte	0xf
	.byte	0xbe
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xc
	.4byte	.LASF126
	.byte	0xf
	.byte	0xc5
	.4byte	0x6bf
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xc
	.4byte	.LASF127
	.byte	0xf
	.byte	0xca
	.4byte	0x972
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xc
	.4byte	.LASF128
	.byte	0xf
	.byte	0xd0
	.4byte	0x68a
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0xc
	.4byte	.LASF129
	.byte	0xf
	.byte	0xda
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0xc
	.4byte	.LASF130
	.byte	0xf
	.byte	0xde
	.4byte	0x573
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0xc
	.4byte	.LASF131
	.byte	0xf
	.byte	0xe2
	.4byte	0x982
	.byte	0x3
	.byte	0x23
	.uleb128 0xdc
	.uleb128 0xc
	.4byte	.LASF132
	.byte	0xf
	.byte	0xe4
	.4byte	0x6ee
	.byte	0x3
	.byte	0x23
	.uleb128 0x139c
	.uleb128 0xc
	.4byte	.LASF133
	.byte	0xf
	.byte	0xea
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x13a4
	.uleb128 0xc
	.4byte	.LASF134
	.byte	0xf
	.byte	0xec
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x13a8
	.uleb128 0xc
	.4byte	.LASF135
	.byte	0xf
	.byte	0xee
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x13ac
	.uleb128 0xc
	.4byte	.LASF136
	.byte	0xf
	.byte	0xf0
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x13b0
	.uleb128 0xc
	.4byte	.LASF137
	.byte	0xf
	.byte	0xf2
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x13b4
	.uleb128 0xc
	.4byte	.LASF138
	.byte	0xf
	.byte	0xf7
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13b8
	.uleb128 0xc
	.4byte	.LASF139
	.byte	0xf
	.byte	0xfd
	.4byte	0x598
	.byte	0x3
	.byte	0x23
	.uleb128 0x13bc
	.uleb128 0x13
	.4byte	.LASF140
	.byte	0xf
	.2byte	0x102
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c0
	.uleb128 0x13
	.4byte	.LASF141
	.byte	0xf
	.2byte	0x104
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c4
	.uleb128 0x13
	.4byte	.LASF142
	.byte	0xf
	.2byte	0x106
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c8
	.uleb128 0x13
	.4byte	.LASF143
	.byte	0xf
	.2byte	0x10b
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x13cc
	.uleb128 0x13
	.4byte	.LASF144
	.byte	0xf
	.2byte	0x10d
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13d0
	.uleb128 0x13
	.4byte	.LASF145
	.byte	0xf
	.2byte	0x116
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0x13d4
	.uleb128 0x13
	.4byte	.LASF146
	.byte	0xf
	.2byte	0x118
	.4byte	0x50c
	.byte	0x3
	.byte	0x23
	.uleb128 0x13d8
	.uleb128 0x13
	.4byte	.LASF147
	.byte	0xf
	.2byte	0x11f
	.4byte	0x5ee
	.byte	0x3
	.byte	0x23
	.uleb128 0x13e4
	.uleb128 0x13
	.4byte	.LASF148
	.byte	0xf
	.2byte	0x12a
	.4byte	0x992
	.byte	0x3
	.byte	0x23
	.uleb128 0x1464
	.byte	0
	.uleb128 0x9
	.4byte	0x6bf
	.4byte	0x982
	.uleb128 0xa
	.4byte	0x9d
	.byte	0xb
	.byte	0
	.uleb128 0x9
	.4byte	0x747
	.4byte	0x992
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x4f
	.byte	0
	.uleb128 0x9
	.4byte	0x5e
	.4byte	0x9a2
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x41
	.byte	0
	.uleb128 0x14
	.4byte	.LASF149
	.byte	0xf
	.2byte	0x133
	.4byte	0x752
	.uleb128 0xb
	.byte	0xc
	.byte	0x1
	.byte	0x26
	.4byte	0x9e1
	.uleb128 0xc
	.4byte	.LASF150
	.byte	0x1
	.byte	0x28
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF151
	.byte	0x1
	.byte	0x2a
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF152
	.byte	0x1
	.byte	0x2c
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF153
	.byte	0x1
	.byte	0x2e
	.4byte	0x9ae
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF183
	.byte	0x1
	.byte	0x3b
	.byte	0x1
	.4byte	0x5e
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0xa5a
	.uleb128 0x19
	.4byte	.LASF154
	.byte	0x1
	.byte	0x3d
	.4byte	0xa5a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x19
	.4byte	.LASF155
	.byte	0x1
	.byte	0x3f
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF156
	.byte	0x1
	.byte	0x41
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x19
	.4byte	.LASF157
	.byte	0x1
	.byte	0x43
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1a
	.ascii	"i\000"
	.byte	0x1
	.byte	0x45
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.ascii	"j\000"
	.byte	0x1
	.byte	0x45
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x239
	.uleb128 0x1b
	.4byte	.LASF184
	.byte	0x1
	.byte	0x95
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0xa95
	.uleb128 0x1c
	.4byte	.LASF159
	.byte	0x1
	.byte	0x95
	.4byte	0x211
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.4byte	.LASF158
	.byte	0x1
	.byte	0x97
	.4byte	0x67a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF161
	.byte	0x1
	.byte	0xa1
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0xabd
	.uleb128 0x1c
	.4byte	.LASF160
	.byte	0x1
	.byte	0xa1
	.4byte	0xabd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.4byte	0x85
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF162
	.byte	0x1
	.byte	0xa9
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0xb06
	.uleb128 0x1c
	.4byte	.LASF163
	.byte	0x1
	.byte	0xa9
	.4byte	0xb06
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.4byte	.LASF154
	.byte	0x1
	.byte	0xab
	.4byte	0xa5a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF157
	.byte	0x1
	.byte	0xad
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.4byte	0x119
	.uleb128 0x1e
	.4byte	.LASF164
	.byte	0x10
	.2byte	0x2ec
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF165
	.byte	0x10
	.2byte	0x39d
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF166
	.byte	0x10
	.2byte	0x39e
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x25
	.4byte	0xb45
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x3
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF167
	.byte	0x10
	.2byte	0x3af
	.4byte	0xb35
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF168
	.byte	0x11
	.byte	0x30
	.4byte	0xb64
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xf
	.4byte	0xe4
	.uleb128 0x19
	.4byte	.LASF169
	.byte	0x11
	.byte	0x34
	.4byte	0xb7a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xf
	.4byte	0xe4
	.uleb128 0x19
	.4byte	.LASF170
	.byte	0x11
	.byte	0x36
	.4byte	0xb90
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xf
	.4byte	0xe4
	.uleb128 0x19
	.4byte	.LASF171
	.byte	0x11
	.byte	0x38
	.4byte	0xba6
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xf
	.4byte	0xe4
	.uleb128 0x1f
	.4byte	.LASF172
	.byte	0x12
	.byte	0x78
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF173
	.byte	0x12
	.byte	0xd5
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x22e
	.4byte	0xbd5
	.uleb128 0xa
	.4byte	0x9d
	.byte	0x31
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF174
	.byte	0x9
	.byte	0xac
	.4byte	0xbc5
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF175
	.byte	0x9
	.byte	0xae
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF176
	.byte	0xa
	.byte	0x64
	.4byte	0x173
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF177
	.byte	0xf
	.2byte	0x138
	.4byte	0x9a2
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x9e1
	.4byte	0xc1a
	.uleb128 0xa
	.4byte	0x9d
	.byte	0xaf
	.byte	0
	.uleb128 0x19
	.4byte	.LASF178
	.byte	0x1
	.byte	0x30
	.4byte	0xc0a
	.byte	0x5
	.byte	0x3
	.4byte	ORPHAN_STATIONS_list_of_orphans
	.uleb128 0x19
	.4byte	.LASF179
	.byte	0x1
	.byte	0x35
	.4byte	0x5e
	.byte	0x5
	.byte	0x3
	.4byte	g_ORPHAN_TWO_WIRE_STATIONS_num_stations
	.uleb128 0x1e
	.4byte	.LASF164
	.byte	0x10
	.2byte	0x2ec
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF165
	.byte	0x10
	.2byte	0x39d
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF166
	.byte	0x10
	.2byte	0x39e
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF167
	.byte	0x10
	.2byte	0x3af
	.4byte	0xb35
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF172
	.byte	0x12
	.byte	0x78
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF173
	.byte	0x12
	.byte	0xd5
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF174
	.byte	0x9
	.byte	0xac
	.4byte	0xbc5
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF175
	.byte	0x9
	.byte	0xae
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF176
	.byte	0xa
	.byte	0x64
	.4byte	0x173
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF177
	.byte	0xf
	.2byte	0x138
	.4byte	0x9a2
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF24:
	.ascii	"count\000"
.LASF111:
	.ascii	"comm_stats\000"
.LASF108:
	.ascii	"TWO_WIRE_CABLE_POWER_OPERATION_STRUCT\000"
.LASF118:
	.ascii	"et_gage_runaway_gage_in_effect_to_send_to_master\000"
.LASF30:
	.ascii	"_03_structure_to_draw\000"
.LASF146:
	.ascii	"two_wire_solenoid_location_on_off_command\000"
.LASF29:
	.ascii	"_02_menu\000"
.LASF104:
	.ascii	"measured_ma_current\000"
.LASF183:
	.ascii	"ORPHAN_TWO_WIRE_STATIONS_populate_list\000"
.LASF101:
	.ascii	"loop_data_bytes_sent\000"
.LASF54:
	.ascii	"rx_disc_rst_msgs\000"
.LASF65:
	.ascii	"rx_sol_ctl_msgs\000"
.LASF78:
	.ascii	"decoder_type__tpmicro_type\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF22:
	.ascii	"phead\000"
.LASF132:
	.ascii	"two_wire_cable_power_operation\000"
.LASF166:
	.ascii	"GuiVar_RptDecoderSN\000"
.LASF121:
	.ascii	"rain_bucket_pulse_count_to_send_to_the_master\000"
.LASF19:
	.ascii	"keycode\000"
.LASF62:
	.ascii	"rx_sol_cur_meas_req_msgs\000"
.LASF46:
	.ascii	"sol_2_ucos\000"
.LASF10:
	.ascii	"long long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF147:
	.ascii	"decoder_faults\000"
.LASF12:
	.ascii	"long unsigned int\000"
.LASF126:
	.ascii	"as_rcvd_from_tp_micro\000"
.LASF110:
	.ascii	"stat2_response\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF122:
	.ascii	"nlu_wind_mph\000"
.LASF56:
	.ascii	"rx_disc_conf_msgs\000"
.LASF116:
	.ascii	"whats_installed_has_arrived_from_the_tpmicro\000"
.LASF49:
	.ascii	"eep_crc_err_sol2_parms\000"
.LASF167:
	.ascii	"GuiVar_RptStation\000"
.LASF13:
	.ascii	"long int\000"
.LASF21:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF37:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF163:
	.ascii	"pkey_event\000"
.LASF6:
	.ascii	"short int\000"
.LASF175:
	.ascii	"screen_history_index\000"
.LASF59:
	.ascii	"rx_data_lpbk_msgs\000"
.LASF15:
	.ascii	"uint16_t\000"
.LASF16:
	.ascii	"portTickType\000"
.LASF27:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF88:
	.ascii	"station_or_light_number_0\000"
.LASF64:
	.ascii	"rx_stats_req_msgs\000"
.LASF171:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF172:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF99:
	.ascii	"unicast_response_crc_errs\000"
.LASF96:
	.ascii	"DECODER_FAULTS_ARRAY_TYPE\000"
.LASF148:
	.ascii	"filler\000"
.LASF28:
	.ascii	"_01_command\000"
.LASF113:
	.ascii	"send_wind_settings_structure_to_the_tpmicro\000"
.LASF92:
	.ascii	"fault_type_code\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF164:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF50:
	.ascii	"eep_crc_err_stats\000"
.LASF149:
	.ascii	"TPMICRO_DATA_STRUCT\000"
.LASF95:
	.ascii	"afflicted_output\000"
.LASF82:
	.ascii	"output\000"
.LASF135:
	.ascii	"two_wire_request_statistics_from_all_decoders\000"
.LASF136:
	.ascii	"two_wire_start_all_decoder_loopback_test\000"
.LASF32:
	.ascii	"key_process_func_ptr\000"
.LASF77:
	.ascii	"STAT2_REQ_RSP_s\000"
.LASF60:
	.ascii	"rx_put_parms_msgs\000"
.LASF107:
	.ascii	"send_command\000"
.LASF94:
	.ascii	"decoder_sn\000"
.LASF114:
	.ascii	"request_whats_installed_from_tp_micro\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF91:
	.ascii	"TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT\000"
.LASF180:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF75:
	.ascii	"sol2_status\000"
.LASF90:
	.ascii	"current_percentage_of_max\000"
.LASF34:
	.ascii	"_06_u32_argument1\000"
.LASF177:
	.ascii	"tpmicro_data\000"
.LASF68:
	.ascii	"seqnum\000"
.LASF76:
	.ascii	"sys_flags\000"
.LASF157:
	.ascii	"lbox_index_0\000"
.LASF51:
	.ascii	"rx_msgs\000"
.LASF85:
	.ascii	"ERROR_LOG_s\000"
.LASF79:
	.ascii	"decoder_subtype\000"
.LASF31:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF156:
	.ascii	"ldecoder_serial_number\000"
.LASF93:
	.ascii	"id_info\000"
.LASF17:
	.ascii	"xQueueHandle\000"
.LASF169:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF131:
	.ascii	"decoder_info\000"
.LASF36:
	.ascii	"_08_screen_to_draw\000"
.LASF153:
	.ascii	"ORPHAN_STATION_ITEM\000"
.LASF120:
	.ascii	"et_gage_clear_runaway_gage_being_processed\000"
.LASF73:
	.ascii	"sol2_cur_s\000"
.LASF69:
	.ascii	"dv_adc_cnts\000"
.LASF83:
	.ascii	"TWO_WIRE_DECODER_SOLENOID_OPERATION_STRUCT\000"
.LASF71:
	.ascii	"SOL_CUR_MEAS_s\000"
.LASF170:
	.ascii	"GuiFont_DecimalChar\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF142:
	.ascii	"two_wire_cable_cooled_off_xmission_state\000"
.LASF98:
	.ascii	"unicast_no_replies\000"
.LASF58:
	.ascii	"rx_dec_rst_msgs\000"
.LASF106:
	.ascii	"MEAS_MA_FOR_DISTRIBUTION\000"
.LASF125:
	.ascii	"nlu_fuse_blown\000"
.LASF70:
	.ascii	"duty_cycle_acc\000"
.LASF128:
	.ascii	"measured_ma_current_as_rcvd_from_master\000"
.LASF129:
	.ascii	"terminal_short_or_no_current_state\000"
.LASF168:
	.ascii	"GuiFont_LanguageActive\000"
.LASF57:
	.ascii	"rx_id_req_msgs\000"
.LASF174:
	.ascii	"ScreenHistory\000"
.LASF23:
	.ascii	"ptail\000"
.LASF47:
	.ascii	"eep_crc_err_com_parms\000"
.LASF184:
	.ascii	"ORPHAN_TWO_WIRE_load_guivars_for_scroll_line\000"
.LASF138:
	.ascii	"decoders_discovered_so_far\000"
.LASF39:
	.ascii	"float\000"
.LASF159:
	.ascii	"pline_index_0_i16\000"
.LASF130:
	.ascii	"terminal_short_or_no_current_report\000"
.LASF178:
	.ascii	"ORPHAN_STATIONS_list_of_orphans\000"
.LASF165:
	.ascii	"GuiVar_RptDecoderOutput\000"
.LASF89:
	.ascii	"TERMINAL_SHORT_OR_NO_CURRENT_STRUCT\000"
.LASF150:
	.ascii	"station_number_0\000"
.LASF151:
	.ascii	"decoder_serial_number\000"
.LASF18:
	.ascii	"xSemaphoreHandle\000"
.LASF44:
	.ascii	"bod_resets\000"
.LASF155:
	.ascii	"ldecoder_was_found\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF103:
	.ascii	"TWO_WIRE_COMM_STATS_PER_DECODER_STRUCT\000"
.LASF48:
	.ascii	"eep_crc_err_sol1_parms\000"
.LASF52:
	.ascii	"rx_long_msgs\000"
.LASF53:
	.ascii	"rx_crc_errs\000"
.LASF133:
	.ascii	"two_wire_perform_discovery\000"
.LASF154:
	.ascii	"lstation\000"
.LASF74:
	.ascii	"sol1_status\000"
.LASF97:
	.ascii	"unicast_msgs_sent\000"
.LASF119:
	.ascii	"et_gage_clear_runaway_gage\000"
.LASF26:
	.ascii	"InUse\000"
.LASF140:
	.ascii	"two_wire_cable_excessive_current_xmission_state\000"
.LASF66:
	.ascii	"tx_acks_sent\000"
.LASF87:
	.ascii	"terminal_type\000"
.LASF45:
	.ascii	"sol_1_ucos\000"
.LASF55:
	.ascii	"rx_enq_msgs\000"
.LASF161:
	.ascii	"FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report\000"
.LASF112:
	.ascii	"CS3000_DECODER_INFO_STRUCT\000"
.LASF63:
	.ascii	"rx_stat_req_msgs\000"
.LASF158:
	.ascii	"str_16\000"
.LASF105:
	.ascii	"current_needs_to_be_sent\000"
.LASF42:
	.ascii	"por_resets\000"
.LASF160:
	.ascii	"pcomplete_redraw\000"
.LASF84:
	.ascii	"errorBitField\000"
.LASF0:
	.ascii	"char\000"
.LASF41:
	.ascii	"temp_current\000"
.LASF134:
	.ascii	"two_wire_clear_statistics_at_all_decoders\000"
.LASF141:
	.ascii	"two_wire_cable_over_heated_xmission_state\000"
.LASF117:
	.ascii	"et_gage_pulse_count_to_send_to_the_master\000"
.LASF144:
	.ascii	"sn_to_set\000"
.LASF137:
	.ascii	"two_wire_stop_all_decoder_loopback_test\000"
.LASF124:
	.ascii	"nlu_freeze_switch_active\000"
.LASF80:
	.ascii	"fw_vers\000"
.LASF25:
	.ascii	"offset\000"
.LASF81:
	.ascii	"ID_REQ_RESP_s\000"
.LASF20:
	.ascii	"repeats\000"
.LASF67:
	.ascii	"DECODER_STATS_s\000"
.LASF179:
	.ascii	"g_ORPHAN_TWO_WIRE_STATIONS_num_stations\000"
.LASF115:
	.ascii	"rcvd_errors\000"
.LASF109:
	.ascii	"decoder_statistics\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF176:
	.ascii	"station_info_list_hdr\000"
.LASF40:
	.ascii	"temp_maximum\000"
.LASF123:
	.ascii	"nlu_rain_switch_active\000"
.LASF182:
	.ascii	"DECODER_FAULT_BASE_STRUCT\000"
.LASF152:
	.ascii	"decoder_output\000"
.LASF14:
	.ascii	"uint8_t\000"
.LASF35:
	.ascii	"_07_u32_argument2\000"
.LASF139:
	.ascii	"twccm\000"
.LASF181:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_orphan_two_wire_stations.c\000"
.LASF173:
	.ascii	"tpmicro_data_recursive_MUTEX\000"
.LASF38:
	.ascii	"STATION_STRUCT\000"
.LASF33:
	.ascii	"_04_func_ptr\000"
.LASF43:
	.ascii	"wdt_resets\000"
.LASF61:
	.ascii	"rx_get_parms_msgs\000"
.LASF145:
	.ascii	"send_2w_solenoid_location_on_off_command\000"
.LASF102:
	.ascii	"loop_data_bytes_recd\000"
.LASF162:
	.ascii	"ORPHAN_TWO_WIRE_STATIONS_process_report\000"
.LASF100:
	.ascii	"unicast_response_length_errs\000"
.LASF143:
	.ascii	"two_wire_set_decoder_sn\000"
.LASF72:
	.ascii	"sol1_cur_s\000"
.LASF127:
	.ascii	"as_rcvd_from_slaves\000"
.LASF86:
	.ascii	"result\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
