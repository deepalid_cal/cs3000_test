	.file	"mem_setup.c"
	.text
.Ltext0:
	.section	.text.setup_CS1_SRAM_R1LV0416DSB_timing,"ax",%progbits
	.align	2
	.global	setup_CS1_SRAM_R1LV0416DSB_timing
	.type	setup_CS1_SRAM_R1LV0416DSB_timing, %function
setup_CS1_SRAM_R1LV0416DSB_timing:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/mem_setup.c"
	.loc 1 48 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	.loc 1 52 0
	ldr	r3, .L2
	mov	r2, #129
	str	r2, [r3, #544]
	.loc 1 55 0
	ldr	r3, .L2
	mov	r2, #0
	str	r2, [r3, #548]
	.loc 1 58 0
	ldr	r3, .L2
	mov	r2, #0
	str	r2, [r3, #552]
	.loc 1 61 0
	ldr	r3, .L2
	mov	r2, #5
	str	r2, [r3, #556]
	.loc 1 63 0
	ldr	r3, .L2
	mov	r2, #5
	str	r2, [r3, #560]
	.loc 1 66 0
	ldr	r3, .L2
	mov	r2, #4
	str	r2, [r3, #564]
	.loc 1 69 0
	ldr	r3, .L2
	mov	r2, #2
	str	r2, [r3, #568]
	.loc 1 70 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L3:
	.align	2
.L2:
	.word	822607872
.LFE0:
	.size	setup_CS1_SRAM_R1LV0416DSB_timing, .-setup_CS1_SRAM_R1LV0416DSB_timing
	.section	.text.setup_SDRAM_timing,"ax",%progbits
	.align	2
	.global	setup_SDRAM_timing
	.type	setup_SDRAM_timing, %function
setup_SDRAM_timing:
.LFB1:
	.loc 1 74 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI2:
	add	fp, sp, #4
.LCFI3:
	sub	sp, sp, #4
.LCFI4:
	.loc 1 82 0
	ldr	r3, .L5
	mov	r2, #1
	str	r2, [r3, #20]
	.loc 1 85 0
	ldr	r3, .L5
	mov	r2, #0
	str	r2, [r3, #104]
	.loc 1 88 0
	ldr	r3, .L5+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 93 0
	ldr	r3, .L5+4
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 97 0
	ldr	r3, .L5+4
	ldr	r2, .L5+8
	str	r2, [r3, #36]
	.loc 1 100 0
	mov	r0, #33
	bl	clkpwr_get_clock_rate
	str	r0, [fp, #-8]
	.loc 1 104 0
	ldr	r0, [fp, #-8]
	mov	r1, #0
	bl	sdr_sdram_setup
	.loc 1 112 0
	ldr	r3, .L5+4
	mov	r2, #1
	str	r2, [r3, #1024]
	.loc 1 116 0
	ldr	r3, .L5+4
	mov	r2, #1
	str	r2, [r3, #1088]
	.loc 1 117 0
	ldr	r3, .L5+4
	mov	r2, #1
	str	r2, [r3, #1120]
	.loc 1 118 0
	ldr	r3, .L5+4
	mov	r2, #1
	str	r2, [r3, #1152]
	.loc 1 121 0
	ldr	r3, .L5+4
	mov	r2, #32
	str	r2, [r3, #1032]
	.loc 1 125 0
	ldr	r3, .L5+4
	mov	r2, #32
	str	r2, [r3, #1096]
	.loc 1 126 0
	ldr	r3, .L5+4
	mov	r2, #32
	str	r2, [r3, #1128]
	.loc 1 127 0
	ldr	r3, .L5+4
	mov	r2, #32
	str	r2, [r3, #1160]
	.loc 1 128 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	1073758208
	.word	822607872
	.word	2047
.LFE1:
	.size	setup_SDRAM_timing, .-setup_SDRAM_timing
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_emc.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x849
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF139
	.byte	0x1
	.4byte	.LASF140
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x3
	.byte	0x5e
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x5
	.byte	0x20
	.byte	0x2
	.byte	0x23
	.4byte	0xef
	.uleb128 0x6
	.4byte	.LASF9
	.byte	0x2
	.byte	0x25
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF10
	.byte	0x2
	.byte	0x26
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF11
	.byte	0x2
	.byte	0x27
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF12
	.byte	0x2
	.byte	0x28
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF13
	.byte	0x2
	.byte	0x29
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x2
	.byte	0x2a
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x2
	.byte	0x2b
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x2
	.byte	0x2c
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x7
	.4byte	0x48
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x2
	.byte	0x2d
	.4byte	0x76
	.uleb128 0x5
	.byte	0x20
	.byte	0x2
	.byte	0x30
	.4byte	0x140
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x2
	.byte	0x32
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF20
	.byte	0x2
	.byte	0x33
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF21
	.byte	0x2
	.byte	0x34
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x2
	.byte	0x35
	.4byte	0x150
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x150
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x4
	.byte	0
	.uleb128 0x7
	.4byte	0x140
	.uleb128 0x3
	.4byte	.LASF22
	.byte	0x2
	.byte	0x36
	.4byte	0xff
	.uleb128 0xa
	.2byte	0x4a0
	.byte	0x2
	.byte	0x39
	.4byte	0x327
	.uleb128 0x6
	.4byte	.LASF23
	.byte	0x2
	.byte	0x3b
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF24
	.byte	0x2
	.byte	0x3c
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x2
	.byte	0x3d
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x2
	.byte	0x3e
	.4byte	0x327
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x2
	.byte	0x3f
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x6
	.4byte	.LASF28
	.byte	0x2
	.byte	0x40
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x6
	.4byte	.LASF29
	.byte	0x2
	.byte	0x41
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x6
	.4byte	.LASF30
	.byte	0x2
	.byte	0x42
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x6
	.4byte	.LASF31
	.byte	0x2
	.byte	0x43
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x6
	.4byte	.LASF32
	.byte	0x2
	.byte	0x44
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x6
	.4byte	.LASF33
	.byte	0x2
	.byte	0x45
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x6
	.4byte	.LASF34
	.byte	0x2
	.byte	0x46
	.4byte	0x33c
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x6
	.4byte	.LASF35
	.byte	0x2
	.byte	0x47
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x6
	.4byte	.LASF36
	.byte	0x2
	.byte	0x48
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x6
	.4byte	.LASF37
	.byte	0x2
	.byte	0x49
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x6
	.4byte	.LASF38
	.byte	0x2
	.byte	0x4a
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x6
	.4byte	.LASF39
	.byte	0x2
	.byte	0x4b
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x6
	.4byte	.LASF40
	.byte	0x2
	.byte	0x4c
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x6
	.4byte	.LASF41
	.byte	0x2
	.byte	0x4d
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x6
	.4byte	.LASF42
	.byte	0x2
	.byte	0x4e
	.4byte	0x351
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x6
	.4byte	.LASF43
	.byte	0x2
	.byte	0x4f
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x6
	.4byte	.LASF44
	.byte	0x2
	.byte	0x50
	.4byte	0x366
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x6
	.4byte	.LASF45
	.byte	0x2
	.byte	0x51
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0x100
	.uleb128 0x6
	.4byte	.LASF46
	.byte	0x2
	.byte	0x52
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0x104
	.uleb128 0x6
	.4byte	.LASF47
	.byte	0x2
	.byte	0x53
	.4byte	0x37b
	.byte	0x3
	.byte	0x23
	.uleb128 0x108
	.uleb128 0x6
	.4byte	.LASF48
	.byte	0x2
	.byte	0x54
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0x120
	.uleb128 0x6
	.4byte	.LASF49
	.byte	0x2
	.byte	0x55
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0x124
	.uleb128 0x6
	.4byte	.LASF50
	.byte	0x2
	.byte	0x56
	.4byte	0x390
	.byte	0x3
	.byte	0x23
	.uleb128 0x128
	.uleb128 0x6
	.4byte	.LASF51
	.byte	0x2
	.byte	0x57
	.4byte	0x395
	.byte	0x3
	.byte	0x23
	.uleb128 0x200
	.uleb128 0x6
	.4byte	.LASF52
	.byte	0x2
	.byte	0x58
	.4byte	0x3b5
	.byte	0x3
	.byte	0x23
	.uleb128 0x280
	.uleb128 0x6
	.4byte	.LASF53
	.byte	0x2
	.byte	0x59
	.4byte	0x3ba
	.byte	0x3
	.byte	0x23
	.uleb128 0x400
	.byte	0
	.uleb128 0x7
	.4byte	0x140
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x33c
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.4byte	0x32c
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x351
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x7
	.byte	0
	.uleb128 0x7
	.4byte	0x341
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x366
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x1e
	.byte	0
	.uleb128 0x7
	.4byte	0x356
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x37b
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x5
	.byte	0
	.uleb128 0x7
	.4byte	0x36b
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x390
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x35
	.byte	0
	.uleb128 0x7
	.4byte	0x380
	.uleb128 0x8
	.4byte	0xf4
	.4byte	0x3a5
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x3b5
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x5f
	.byte	0
	.uleb128 0x7
	.4byte	0x3a5
	.uleb128 0x8
	.4byte	0x155
	.4byte	0x3ca
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF54
	.byte	0x2
	.byte	0x5a
	.4byte	0x160
	.uleb128 0xa
	.2byte	0x140
	.byte	0x4
	.byte	0x28
	.4byte	0x6de
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x4
	.byte	0x2a
	.4byte	0x6de
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF55
	.byte	0x4
	.byte	0x2b
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF56
	.byte	0x4
	.byte	0x2c
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF57
	.byte	0x4
	.byte	0x2d
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x6
	.4byte	.LASF58
	.byte	0x4
	.byte	0x2e
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x6
	.4byte	.LASF59
	.byte	0x4
	.byte	0x2f
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x6
	.4byte	.LASF60
	.byte	0x4
	.byte	0x30
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x6
	.4byte	.LASF61
	.byte	0x4
	.byte	0x31
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x6
	.4byte	.LASF62
	.byte	0x4
	.byte	0x32
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x6
	.4byte	.LASF63
	.byte	0x4
	.byte	0x33
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x6
	.4byte	.LASF64
	.byte	0x4
	.byte	0x34
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x6
	.4byte	.LASF65
	.byte	0x4
	.byte	0x35
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x6
	.4byte	.LASF66
	.byte	0x4
	.byte	0x36
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x6
	.4byte	.LASF67
	.byte	0x4
	.byte	0x37
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x6
	.4byte	.LASF68
	.byte	0x4
	.byte	0x38
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x6
	.4byte	.LASF69
	.byte	0x4
	.byte	0x39
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x6
	.4byte	.LASF70
	.byte	0x4
	.byte	0x3a
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x6
	.4byte	.LASF71
	.byte	0x4
	.byte	0x3b
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x6
	.4byte	.LASF72
	.byte	0x4
	.byte	0x3c
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x6
	.4byte	.LASF30
	.byte	0x4
	.byte	0x3d
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x6
	.4byte	.LASF73
	.byte	0x4
	.byte	0x3e
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x6
	.4byte	.LASF74
	.byte	0x4
	.byte	0x3f
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x6
	.4byte	.LASF75
	.byte	0x4
	.byte	0x40
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x6
	.4byte	.LASF76
	.byte	0x4
	.byte	0x41
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x6
	.4byte	.LASF77
	.byte	0x4
	.byte	0x42
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x6
	.4byte	.LASF78
	.byte	0x4
	.byte	0x43
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x6
	.4byte	.LASF79
	.byte	0x4
	.byte	0x44
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x6
	.4byte	.LASF80
	.byte	0x4
	.byte	0x45
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x6
	.4byte	.LASF81
	.byte	0x4
	.byte	0x46
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x6
	.4byte	.LASF42
	.byte	0x4
	.byte	0x47
	.4byte	0x6f3
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x6
	.4byte	.LASF82
	.byte	0x4
	.byte	0x48
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x6
	.4byte	.LASF44
	.byte	0x4
	.byte	0x49
	.4byte	0x708
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x6
	.4byte	.LASF83
	.byte	0x4
	.byte	0x4a
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x6
	.4byte	.LASF84
	.byte	0x4
	.byte	0x4b
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x6
	.4byte	.LASF85
	.byte	0x4
	.byte	0x4c
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x6
	.4byte	.LASF86
	.byte	0x4
	.byte	0x4d
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x6
	.4byte	.LASF87
	.byte	0x4
	.byte	0x4e
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0x6
	.4byte	.LASF88
	.byte	0x4
	.byte	0x4f
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0x6
	.4byte	.LASF89
	.byte	0x4
	.byte	0x50
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xbc
	.uleb128 0x6
	.4byte	.LASF90
	.byte	0x4
	.byte	0x51
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x6
	.4byte	.LASF91
	.byte	0x4
	.byte	0x52
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xc4
	.uleb128 0x6
	.4byte	.LASF92
	.byte	0x4
	.byte	0x53
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0x6
	.4byte	.LASF50
	.byte	0x4
	.byte	0x54
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x6
	.4byte	.LASF93
	.byte	0x4
	.byte	0x55
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x6
	.4byte	.LASF94
	.byte	0x4
	.byte	0x56
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0x6
	.4byte	.LASF95
	.byte	0x4
	.byte	0x57
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0x6
	.4byte	.LASF96
	.byte	0x4
	.byte	0x58
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xdc
	.uleb128 0x6
	.4byte	.LASF97
	.byte	0x4
	.byte	0x59
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xe0
	.uleb128 0x6
	.4byte	.LASF98
	.byte	0x4
	.byte	0x5a
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xe4
	.uleb128 0x6
	.4byte	.LASF99
	.byte	0x4
	.byte	0x5b
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xe8
	.uleb128 0x6
	.4byte	.LASF100
	.byte	0x4
	.byte	0x5c
	.4byte	0xef
	.byte	0x3
	.byte	0x23
	.uleb128 0xec
	.uleb128 0x6
	.4byte	.LASF52
	.byte	0x4
	.byte	0x5d
	.4byte	0x71d
	.byte	0x3
	.byte	0x23
	.uleb128 0xf0
	.uleb128 0x6
	.4byte	.LASF101
	.byte	0x4
	.byte	0x5e
	.4byte	0x722
	.byte	0x3
	.byte	0x23
	.uleb128 0x130
	.byte	0
	.uleb128 0x7
	.4byte	0x140
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x6f3
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.4byte	0x6e3
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x708
	.uleb128 0x9
	.4byte	0x6f
	.byte	0x3
	.byte	0
	.uleb128 0x7
	.4byte	0x6f8
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x71d
	.uleb128 0x9
	.4byte	0x6f
	.byte	0xf
	.byte	0
	.uleb128 0x7
	.4byte	0x70d
	.uleb128 0x7
	.4byte	0x6f8
	.uleb128 0x3
	.4byte	.LASF102
	.byte	0x4
	.byte	0x5f
	.4byte	0x3d5
	.uleb128 0xb
	.byte	0x4
	.byte	0x5
	.byte	0x24
	.4byte	0x813
	.uleb128 0xc
	.4byte	.LASF103
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF104
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF105
	.sleb128 1
	.uleb128 0xc
	.4byte	.LASF106
	.sleb128 2
	.uleb128 0xc
	.4byte	.LASF107
	.sleb128 3
	.uleb128 0xc
	.4byte	.LASF108
	.sleb128 4
	.uleb128 0xc
	.4byte	.LASF109
	.sleb128 5
	.uleb128 0xc
	.4byte	.LASF110
	.sleb128 6
	.uleb128 0xc
	.4byte	.LASF111
	.sleb128 7
	.uleb128 0xc
	.4byte	.LASF112
	.sleb128 8
	.uleb128 0xc
	.4byte	.LASF113
	.sleb128 9
	.uleb128 0xc
	.4byte	.LASF114
	.sleb128 10
	.uleb128 0xc
	.4byte	.LASF115
	.sleb128 11
	.uleb128 0xc
	.4byte	.LASF116
	.sleb128 12
	.uleb128 0xc
	.4byte	.LASF117
	.sleb128 13
	.uleb128 0xc
	.4byte	.LASF118
	.sleb128 14
	.uleb128 0xc
	.4byte	.LASF119
	.sleb128 15
	.uleb128 0xc
	.4byte	.LASF120
	.sleb128 16
	.uleb128 0xc
	.4byte	.LASF121
	.sleb128 17
	.uleb128 0xc
	.4byte	.LASF122
	.sleb128 18
	.uleb128 0xc
	.4byte	.LASF123
	.sleb128 19
	.uleb128 0xc
	.4byte	.LASF124
	.sleb128 20
	.uleb128 0xc
	.4byte	.LASF125
	.sleb128 21
	.uleb128 0xc
	.4byte	.LASF126
	.sleb128 22
	.uleb128 0xc
	.4byte	.LASF127
	.sleb128 23
	.uleb128 0xc
	.4byte	.LASF128
	.sleb128 24
	.uleb128 0xc
	.4byte	.LASF129
	.sleb128 25
	.uleb128 0xc
	.4byte	.LASF130
	.sleb128 26
	.uleb128 0xc
	.4byte	.LASF131
	.sleb128 27
	.uleb128 0xc
	.4byte	.LASF132
	.sleb128 28
	.uleb128 0xc
	.4byte	.LASF133
	.sleb128 29
	.uleb128 0xc
	.4byte	.LASF134
	.sleb128 30
	.uleb128 0xc
	.4byte	.LASF135
	.sleb128 31
	.uleb128 0xc
	.4byte	.LASF136
	.sleb128 32
	.uleb128 0xc
	.4byte	.LASF137
	.sleb128 33
	.uleb128 0xc
	.4byte	.LASF138
	.sleb128 34
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF141
	.byte	0x1
	.byte	0x2f
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF142
	.byte	0x1
	.byte	0x49
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0xf
	.4byte	.LASF143
	.byte	0x1
	.byte	0x4b
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF76:
	.ascii	"clkpwr_ddr_lap_nom\000"
.LASF15:
	.ascii	"emcstatic_turn_around_delay\000"
.LASF78:
	.ascii	"clkpwr_ddr_cal_delay\000"
.LASF55:
	.ascii	"clkpwr_bootmap\000"
.LASF85:
	.ascii	"clkpwr_i2c_clk_ctrl\000"
.LASF120:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF98:
	.ascii	"clkpwr_uart_clk_ctrl\000"
.LASF124:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF32:
	.ascii	"emcdynamictras\000"
.LASF132:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF13:
	.ascii	"emcstatic_page_RD_delay\000"
.LASF105:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF6:
	.ascii	"long long unsigned int\000"
.LASF41:
	.ascii	"emcdynamictcdlr\000"
.LASF82:
	.ascii	"clkpwr_macclk_ctrl\000"
.LASF17:
	.ascii	"UNS_32\000"
.LASF110:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF7:
	.ascii	"long long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF103:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF93:
	.ascii	"clkpwr_uart3_clk_ctrl\000"
.LASF94:
	.ascii	"clkpwr_uart4_clk_ctrl\000"
.LASF40:
	.ascii	"emcdynamictmrd\000"
.LASF96:
	.ascii	"clkpwr_uart6_clk_ctrl\000"
.LASF69:
	.ascii	"clkpwr_main_osc_ctrl\000"
.LASF136:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF57:
	.ascii	"clkpwr_usbclk_pdiv\000"
.LASF116:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF86:
	.ascii	"clkpwr_key_clk_ctrl\000"
.LASF65:
	.ascii	"clkpwr_pin_ap\000"
.LASF61:
	.ascii	"clkpwr_int_ap\000"
.LASF39:
	.ascii	"emcdynamictrrd\000"
.LASF19:
	.ascii	"emcahbcontrol\000"
.LASF90:
	.ascii	"clkpwr_timers_pwms_clk_ctrl_1\000"
.LASF16:
	.ascii	"reserved\000"
.LASF88:
	.ascii	"clkpwr_pwm_clk_ctrl\000"
.LASF73:
	.ascii	"clkpwr_adc_clk_ctrl_1\000"
.LASF108:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF79:
	.ascii	"clkpwr_ssp_blk_ctrl\000"
.LASF122:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF5:
	.ascii	"unsigned int\000"
.LASF113:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF131:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF77:
	.ascii	"clkpwr_ddr_lap_count\000"
.LASF54:
	.ascii	"EMC_REGS_T\000"
.LASF8:
	.ascii	"long unsigned int\000"
.LASF38:
	.ascii	"emcdynamictxsr\000"
.LASF35:
	.ascii	"emcdynamictwr\000"
.LASF102:
	.ascii	"CLKPWR_REGS_T\000"
.LASF21:
	.ascii	"emcahbtimeout\000"
.LASF95:
	.ascii	"clkpwr_uart5_clk_ctrl\000"
.LASF142:
	.ascii	"setup_SDRAM_timing\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF43:
	.ascii	"emcstaticextendedwait\000"
.LASF139:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF18:
	.ascii	"EMC_STATIC_CFG\000"
.LASF138:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF125:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF118:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF104:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF133:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF127:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF37:
	.ascii	"emcdynamictrfc\000"
.LASF114:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF72:
	.ascii	"clkpwr_hclkpll_ctrl\000"
.LASF63:
	.ascii	"clkpwr_pin_rs\000"
.LASF129:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF89:
	.ascii	"clkpwr_timer_clk_ctrl\000"
.LASF106:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF134:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF71:
	.ascii	"clkpwr_lcdclk_ctrl\000"
.LASF59:
	.ascii	"clkpwr_int_rs\000"
.LASF10:
	.ascii	"emcstatic_WE_delay\000"
.LASF20:
	.ascii	"emcahbstatus\000"
.LASF23:
	.ascii	"emccontrol\000"
.LASF28:
	.ascii	"emcdynamicrefresh\000"
.LASF81:
	.ascii	"clkpwr_ms_ctrl\000"
.LASF126:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF29:
	.ascii	"emcdynamicreadconfig\000"
.LASF83:
	.ascii	"clkpwr_test_clk_sel\000"
.LASF45:
	.ascii	"emcdynamicconfig0\000"
.LASF48:
	.ascii	"emcdynamicconfig1\000"
.LASF27:
	.ascii	"emcdynamiccontrol\000"
.LASF99:
	.ascii	"clkpwr_dmaclk_ctrl\000"
.LASF64:
	.ascii	"clkpwr_pin_sr\000"
.LASF91:
	.ascii	"clkpwr_spi_clk_ctrl\000"
.LASF101:
	.ascii	"clkpwr_uid\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF60:
	.ascii	"clkpwr_int_sr\000"
.LASF51:
	.ascii	"emcstatic_regs\000"
.LASF56:
	.ascii	"clkpwr_p01_er\000"
.LASF66:
	.ascii	"clkpwr_hclk_div\000"
.LASF87:
	.ascii	"clkpwr_adc_clk_ctrl\000"
.LASF4:
	.ascii	"short int\000"
.LASF36:
	.ascii	"emcdynamictrc\000"
.LASF53:
	.ascii	"emcahn_regs\000"
.LASF75:
	.ascii	"clkpwr_sdramclk_ctrl\000"
.LASF141:
	.ascii	"setup_CS1_SRAM_R1LV0416DSB_timing\000"
.LASF25:
	.ascii	"emcconfig\000"
.LASF109:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF123:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF31:
	.ascii	"emcdynamictrp\000"
.LASF74:
	.ascii	"clkpwr_usb_ctrl\000"
.LASF130:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF112:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF0:
	.ascii	"char\000"
.LASF67:
	.ascii	"clkpwr_pwr_ctrl\000"
.LASF62:
	.ascii	"clkpwr_pin_er\000"
.LASF24:
	.ascii	"emcstatus\000"
.LASF143:
	.ascii	"dramclk\000"
.LASF140:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/mem_setup.c\000"
.LASF111:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF84:
	.ascii	"clkpwr_sw_int\000"
.LASF58:
	.ascii	"clkpwr_int_er\000"
.LASF80:
	.ascii	"clkpwr_i2s_clk_ctrl\000"
.LASF119:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF12:
	.ascii	"emcstatic_RD_access_delay\000"
.LASF128:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF137:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF115:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF9:
	.ascii	"emcstatic_config\000"
.LASF70:
	.ascii	"clkpwr_sysclk_ctrl\000"
.LASF92:
	.ascii	"clkpwr_nand_clk_ctrl\000"
.LASF107:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF135:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF33:
	.ascii	"emcdynamictsrex\000"
.LASF46:
	.ascii	"emcdynamicrascas0\000"
.LASF49:
	.ascii	"emcdynamicrascas1\000"
.LASF117:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF14:
	.ascii	"emcstatic_WR_cycle_delay\000"
.LASF97:
	.ascii	"clkpwr_irda_clk_ctrl\000"
.LASF100:
	.ascii	"clkpwr_autoclock\000"
.LASF26:
	.ascii	"reserved1\000"
.LASF30:
	.ascii	"reserved2\000"
.LASF34:
	.ascii	"reserved3\000"
.LASF42:
	.ascii	"reserved4\000"
.LASF44:
	.ascii	"reserved5\000"
.LASF47:
	.ascii	"reserved6\000"
.LASF50:
	.ascii	"reserved7\000"
.LASF52:
	.ascii	"reserved8\000"
.LASF11:
	.ascii	"emcstatic_OE_delay\000"
.LASF68:
	.ascii	"clkpwr_pll397_ctrl\000"
.LASF121:
	.ascii	"CLKPWR_WDOG_CLK\000"
.LASF22:
	.ascii	"EMC_AHB_CTRL_T\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
