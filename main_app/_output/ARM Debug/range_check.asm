	.file	"range_check.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.text.RC_bool_with_filename,"ax",%progbits
	.align	2
	.global	RC_bool_with_filename
	.type	RC_bool_with_filename, %function
RC_bool_with_filename:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/range_check.c"
	.loc 1 56 0
	@ args = 8, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI0:
	add	fp, sp, #8
.LCFI1:
	sub	sp, sp, #28
.LCFI2:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 59 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 61 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L2
	.loc 1 61 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L2
	.loc 1 66 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #0]
	ldr	r0, [fp, #4]
	bl	RemovePathFromFileName
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r3, [fp, #8]
	str	r3, [sp, #4]
	ldr	r0, [fp, #-28]
	ldr	r1, [fp, #-24]
	mov	r2, r4
	ldr	r3, [fp, #-20]
	bl	Alert_range_check_failed_bool_with_filename
	.loc 1 77 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 81 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L2:
	.loc 1 84 0
	ldr	r3, [fp, #-12]
	.loc 1 85 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE0:
	.size	RC_bool_with_filename, .-RC_bool_with_filename
	.section	.text.RC_date_with_filename,"ax",%progbits
	.align	2
	.global	RC_date_with_filename
	.type	RC_date_with_filename, %function
RC_date_with_filename:
.LFB1:
	.loc 1 96 0
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI3:
	add	fp, sp, #8
.LCFI4:
	sub	sp, sp, #24
.LCFI5:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 99 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 101 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcc	.L4
	.loc 1 101 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bls	.L5
.L4:
	.loc 1 105 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 112 0
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #0]
	ldr	r0, [fp, #12]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r2, [fp, #16]
	str	r2, [sp, #0]
	ldr	r0, [fp, #8]
	ldr	r1, [fp, #4]
	mov	r2, r4
	bl	Alert_range_check_failed_date_with_filename
	.loc 1 121 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L5:
	.loc 1 124 0
	ldr	r3, [fp, #-12]
	.loc 1 125 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE1:
	.size	RC_date_with_filename, .-RC_date_with_filename
	.section	.text.RC_int32_with_filename,"ax",%progbits
	.align	2
	.global	RC_int32_with_filename
	.type	RC_int32_with_filename, %function
RC_int32_with_filename:
.LFB2:
	.loc 1 136 0
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI6:
	add	fp, sp, #8
.LCFI7:
	sub	sp, sp, #28
.LCFI8:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 139 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 141 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	blt	.L7
	.loc 1 141 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	ble	.L8
.L7:
	.loc 1 146 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #0]
	ldr	r0, [fp, #12]
	bl	RemovePathFromFileName
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r3, [fp, #16]
	str	r3, [sp, #4]
	ldr	r0, [fp, #8]
	ldr	r1, [fp, #4]
	mov	r2, r4
	ldr	r3, [fp, #-28]
	bl	Alert_range_check_failed_int32_with_filename
	.loc 1 157 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 161 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L8:
	.loc 1 164 0
	ldr	r3, [fp, #-12]
	.loc 1 165 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE2:
	.size	RC_int32_with_filename, .-RC_int32_with_filename
	.section	.text.RC_string_with_filename,"ax",%progbits
	.align	2
	.global	RC_string_with_filename
	.type	RC_string_with_filename, %function
RC_string_with_filename:
.LFB3:
	.loc 1 176 0
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #32
.LCFI11:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	str	r3, [fp, #-36]
	.loc 1 177 0
	mov	r3, #63
	strb	r3, [fp, #-17]
	.loc 1 185 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 187 0
	mov	r3, #0
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-8]
	b	.L10
.L13:
	.loc 1 189 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r0, r3
	bl	isprint
	mov	r3, r0
	cmp	r3, #0
	bne	.L11
	.loc 1 193 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [fp, #-17]
	strb	r2, [r3, #0]
	.loc 1 195 0
	mov	r3, #0
	str	r3, [fp, #-16]
.L11:
	.loc 1 187 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L10:
	.loc 1 187 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bcs	.L12
	.loc 1 187 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L13
.L12:
	.loc 1 199 0 is_stmt 1
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bne	.L14
	.loc 1 199 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L14
	.loc 1 204 0 is_stmt 1
	ldr	r3, [fp, #-8]
	mov	r2, #0
	strb	r2, [r3, #0]
.L14:
	.loc 1 207 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L15
	.loc 1 212 0
	ldr	r0, [fp, #-36]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, [fp, #-32]
	ldr	r1, [fp, #-24]
	mov	r2, r3
	ldr	r3, [fp, #4]
	bl	Alert_range_check_failed_string_with_filename
.L15:
	.loc 1 220 0
	ldr	r3, [fp, #-16]
	.loc 1 221 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE3:
	.size	RC_string_with_filename, .-RC_string_with_filename
	.section	.text.RC_time_with_filename,"ax",%progbits
	.align	2
	.global	RC_time_with_filename
	.type	RC_time_with_filename, %function
RC_time_with_filename:
.LFB4:
	.loc 1 232 0
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI12:
	add	fp, sp, #8
.LCFI13:
	sub	sp, sp, #24
.LCFI14:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 235 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 237 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcc	.L17
	.loc 1 237 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bls	.L18
.L17:
	.loc 1 241 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 248 0
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #0]
	ldr	r0, [fp, #12]
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r2, [fp, #16]
	str	r2, [sp, #0]
	ldr	r0, [fp, #8]
	ldr	r1, [fp, #4]
	mov	r2, r4
	bl	Alert_range_check_failed_time_with_filename
	.loc 1 257 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L18:
	.loc 1 260 0
	ldr	r3, [fp, #-12]
	.loc 1 261 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE4:
	.size	RC_time_with_filename, .-RC_time_with_filename
	.section	.text.RC_uint32_with_filename,"ax",%progbits
	.align	2
	.global	RC_uint32_with_filename
	.type	RC_uint32_with_filename, %function
RC_uint32_with_filename:
.LFB5:
	.loc 1 272 0
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI15:
	add	fp, sp, #8
.LCFI16:
	sub	sp, sp, #28
.LCFI17:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 275 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 277 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcc	.L20
	.loc 1 277 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bls	.L21
.L20:
	.loc 1 282 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #0]
	ldr	r0, [fp, #12]
	bl	RemovePathFromFileName
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r3, [fp, #16]
	str	r3, [sp, #4]
	ldr	r0, [fp, #8]
	ldr	r1, [fp, #4]
	mov	r2, r4
	ldr	r3, [fp, #-28]
	bl	Alert_range_check_failed_uint32_with_filename
	.loc 1 293 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 297 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L21:
	.loc 1 300 0
	ldr	r3, [fp, #-12]
	.loc 1 301 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE5:
	.size	RC_uint32_with_filename, .-RC_uint32_with_filename
	.section	.text.RC_uns8_with_filename,"ax",%progbits
	.align	2
	.global	RC_uns8_with_filename
	.type	RC_uns8_with_filename, %function
RC_uns8_with_filename:
.LFB6:
	.loc 1 312 0
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI18:
	add	fp, sp, #12
.LCFI19:
	sub	sp, sp, #28
.LCFI20:
	str	r0, [fp, #-20]
	strb	r1, [fp, #-24]
	strb	r2, [fp, #-28]
	strb	r3, [fp, #-32]
	.loc 1 315 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 317 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrb	r2, [fp, #-24]	@ zero_extendqisi2
	cmp	r2, r3
	bhi	.L23
	.loc 1 317 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrb	r2, [fp, #-28]	@ zero_extendqisi2
	cmp	r2, r3
	bcs	.L24
.L23:
	.loc 1 324 0 is_stmt 1
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r5, r3
	ldrb	r4, [fp, #-32]	@ zero_extendqisi2
	ldr	r0, [fp, #12]
	bl	RemovePathFromFileName
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r3, [fp, #16]
	str	r3, [sp, #4]
	ldr	r0, [fp, #8]
	ldr	r1, [fp, #4]
	mov	r2, r5
	mov	r3, r4
	bl	Alert_range_check_failed_uint32_with_filename
	.loc 1 335 0
	ldr	r3, [fp, #-20]
	ldrb	r2, [fp, #-32]
	strb	r2, [r3, #0]
	.loc 1 339 0
	mov	r3, #0
	str	r3, [fp, #-16]
.L24:
	.loc 1 342 0
	ldr	r3, [fp, #-16]
	.loc 1 343 0
	mov	r0, r3
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.LFE6:
	.size	RC_uns8_with_filename, .-RC_uns8_with_filename
	.section	.text.RC_float_with_filename,"ax",%progbits
	.align	2
	.global	RC_float_with_filename
	.type	RC_float_with_filename, %function
RC_float_with_filename:
.LFB7:
	.loc 1 354 0
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI21:
	add	fp, sp, #8
.LCFI22:
	sub	sp, sp, #28
.LCFI23:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]	@ float
	str	r2, [fp, #-24]	@ float
	str	r3, [fp, #-28]	@ float
	.loc 1 357 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 359 0
	ldr	r3, [fp, #-16]
	flds	s14, [r3, #0]
	flds	s15, [fp, #-20]
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L26
	.loc 1 359 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	flds	s14, [r3, #0]
	flds	s15, [fp, #-24]
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L27
.L26:
	.loc 1 364 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #0]	@ float
	ldr	r0, [fp, #12]
	bl	RemovePathFromFileName
	mov	r3, r0
	str	r3, [sp, #0]
	ldr	r3, [fp, #16]
	str	r3, [sp, #4]
	ldr	r0, [fp, #8]
	ldr	r1, [fp, #4]
	mov	r2, r4	@ float
	ldr	r3, [fp, #-28]	@ float
	bl	Alert_range_check_failed_float_with_filename
	.loc 1 375 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-28]	@ float
	str	r2, [r3, #0]	@ float
	.loc 1 379 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L27:
	.loc 1 382 0
	ldr	r3, [fp, #-12]
	.loc 1 383 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE7:
	.size	RC_float_with_filename, .-RC_float_with_filename
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6a0
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF41
	.byte	0x1
	.4byte	.LASF42
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x67
	.4byte	0x70
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x5
	.4byte	0x37
	.4byte	0xae
	.uleb128 0x6
	.4byte	0x90
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0x53
	.4byte	0xbe
	.uleb128 0x6
	.4byte	0x90
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x2c
	.uleb128 0x7
	.byte	0x4
	.4byte	0x25
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF14
	.uleb128 0x5
	.4byte	0x53
	.4byte	0xe1
	.uleb128 0x6
	.4byte	0x90
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF15
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.byte	0x34
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x167
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x1
	.byte	0x34
	.4byte	0x167
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x1
	.byte	0x34
	.4byte	0x172
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x1
	.byte	0x34
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x1
	.byte	0x34
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x1
	.byte	0x34
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x1
	.byte	0x34
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x39
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xb
	.4byte	0x16c
	.uleb128 0x7
	.byte	0x4
	.4byte	0x85
	.uleb128 0xb
	.4byte	0x85
	.uleb128 0xb
	.4byte	0x17c
	.uleb128 0x7
	.byte	0x4
	.4byte	0x182
	.uleb128 0xb
	.4byte	0x25
	.uleb128 0xb
	.4byte	0x53
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.byte	0x5c
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x227
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x1
	.byte	0x5c
	.4byte	0x227
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x1
	.byte	0x5c
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x1
	.byte	0x5c
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x1
	.byte	0x5c
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x1
	.byte	0x5c
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x1
	.byte	0x5c
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x1
	.byte	0x5c
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x1
	.byte	0x5c
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x61
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xb
	.4byte	0x22c
	.uleb128 0x7
	.byte	0x4
	.4byte	0x53
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.byte	0x84
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x2cd
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x1
	.byte	0x84
	.4byte	0x2cd
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x1
	.byte	0x84
	.4byte	0x2d8
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x1
	.byte	0x84
	.4byte	0x2d8
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x1
	.byte	0x84
	.4byte	0x2d8
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x1
	.byte	0x84
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x1
	.byte	0x84
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x1
	.byte	0x84
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x1
	.byte	0x84
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x89
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xb
	.4byte	0x2d2
	.uleb128 0x7
	.byte	0x4
	.4byte	0x65
	.uleb128 0xb
	.4byte	0x65
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.byte	0xac
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x376
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x1
	.byte	0xac
	.4byte	0x376
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x1
	.byte	0xac
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x1
	.byte	0xac
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x1
	.byte	0xac
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x1
	.byte	0xac
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x1
	.byte	0xb1
	.4byte	0x182
	.byte	0x2
	.byte	0x91
	.sleb128 -21
	.uleb128 0xa
	.ascii	"ptr\000"
	.byte	0x1
	.byte	0xb3
	.4byte	0xc4
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xa
	.ascii	"i\000"
	.byte	0x1
	.byte	0xb5
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xb7
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xb
	.4byte	0xc4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.byte	0xe4
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x416
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x1
	.byte	0xe4
	.4byte	0x227
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x1
	.byte	0xe4
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x1
	.byte	0xe4
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x1
	.byte	0xe4
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x1
	.byte	0xe4
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x1
	.byte	0xe4
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x1
	.byte	0xe4
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x1
	.byte	0xe4
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xe9
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x10c
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x4bb
	.uleb128 0xe
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x10c
	.4byte	0x227
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xe
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x10c
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xe
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x10c
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xe
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x10c
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xe
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x10c
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xe
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x10c
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0xe
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x10c
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0xe
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x10c
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0xf
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x111
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x134
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x560
	.uleb128 0xe
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x134
	.4byte	0x560
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xe
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x134
	.4byte	0x565
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xe
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x134
	.4byte	0x565
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xe
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x134
	.4byte	0x565
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xe
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x134
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xe
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x134
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0xe
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x134
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0xe
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x134
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0xf
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x139
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xb
	.4byte	0xbe
	.uleb128 0xb
	.4byte	0x2c
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x15e
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x60f
	.uleb128 0xe
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x15e
	.4byte	0x60f
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xe
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x15e
	.4byte	0x61a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xe
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x15e
	.4byte	0x61a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xe
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x15e
	.4byte	0x61a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xe
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x15e
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xe
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x15e
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0xe
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x15e
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0xe
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x15e
	.4byte	0x187
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0xf
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x163
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xb
	.4byte	0x614
	.uleb128 0x7
	.byte	0x4
	.4byte	0xca
	.uleb128 0xb
	.4byte	0xca
	.uleb128 0xc
	.4byte	.LASF35
	.byte	0x3
	.byte	0x30
	.4byte	0x630
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xb
	.4byte	0x9e
	.uleb128 0xc
	.4byte	.LASF36
	.byte	0x3
	.byte	0x34
	.4byte	0x646
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xb
	.4byte	0x9e
	.uleb128 0xc
	.4byte	.LASF37
	.byte	0x3
	.byte	0x36
	.4byte	0x65c
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xb
	.4byte	0x9e
	.uleb128 0xc
	.4byte	.LASF38
	.byte	0x3
	.byte	0x38
	.4byte	0x672
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xb
	.4byte	0x9e
	.uleb128 0xc
	.4byte	.LASF39
	.byte	0x4
	.byte	0x33
	.4byte	0x688
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xb
	.4byte	0xae
	.uleb128 0xc
	.4byte	.LASF40
	.byte	0x4
	.byte	0x3f
	.4byte	0x69e
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xb
	.4byte	0xd1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF35:
	.ascii	"GuiFont_LanguageActive\000"
.LASF31:
	.ascii	"RC_time_with_filename\000"
.LASF22:
	.ascii	"RC_bool_with_filename\000"
.LASF28:
	.ascii	"pString\000"
.LASF25:
	.ascii	"pMax\000"
.LASF16:
	.ascii	"pValue\000"
.LASF21:
	.ascii	"pLine\000"
.LASF41:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF19:
	.ascii	"pVariableName\000"
.LASF26:
	.ascii	"RC_int32_with_filename\000"
.LASF27:
	.ascii	"RC_string_with_filename\000"
.LASF24:
	.ascii	"pMin\000"
.LASF20:
	.ascii	"pFilename\000"
.LASF17:
	.ascii	"pDefault\000"
.LASF14:
	.ascii	"float\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF8:
	.ascii	"INT_32\000"
.LASF12:
	.ascii	"long unsigned int\000"
.LASF39:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF18:
	.ascii	"pGroupName\000"
.LASF36:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF15:
	.ascii	"double\000"
.LASF5:
	.ascii	"UNS_8\000"
.LASF42:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/range_check.c\000"
.LASF30:
	.ascii	"INVALID_CHAR_REPLACEMENT\000"
.LASF23:
	.ascii	"RC_date_with_filename\000"
.LASF0:
	.ascii	"char\000"
.LASF33:
	.ascii	"RC_uns8_with_filename\000"
.LASF40:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF32:
	.ascii	"RC_uint32_with_filename\000"
.LASF10:
	.ascii	"long long int\000"
.LASF34:
	.ascii	"RC_float_with_filename\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF29:
	.ascii	"pLength\000"
.LASF37:
	.ascii	"GuiFont_DecimalChar\000"
.LASF4:
	.ascii	"short int\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF13:
	.ascii	"long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF38:
	.ascii	"GuiFont_LanguageCharSets\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
