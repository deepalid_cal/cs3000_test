	.file	"d_copy_dialog.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_COPY_DIALOG_editing_group,"aw",%nobits
	.align	2
	.type	g_COPY_DIALOG_editing_group, %object
	.size	g_COPY_DIALOG_editing_group, 4
g_COPY_DIALOG_editing_group:
	.space	4
	.section	.bss.g_COPY_DIALOG_add_group_func_ptr,"aw",%nobits
	.align	2
	.type	g_COPY_DIALOG_add_group_func_ptr, %object
	.size	g_COPY_DIALOG_add_group_func_ptr, 4
g_COPY_DIALOG_add_group_func_ptr:
	.space	4
	.section	.bss.g_COPY_DIALOG_selected_group_index,"aw",%nobits
	.align	2
	.type	g_COPY_DIALOG_selected_group_index, %object
	.size	g_COPY_DIALOG_selected_group_index, 4
g_COPY_DIALOG_selected_group_index:
	.space	4
	.global	g_COPY_DIALOG_visible
	.section	.bss.g_COPY_DIALOG_visible,"aw",%nobits
	.align	2
	.type	g_COPY_DIALOG_visible, %object
	.size	g_COPY_DIALOG_visible, 4
g_COPY_DIALOG_visible:
	.space	4
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_copy_dialog.c\000"
	.section	.text.COPY_DIALOG_peform_copy_and_redraw_screen,"ax",%progbits
	.align	2
	.global	COPY_DIALOG_peform_copy_and_redraw_screen
	.type	COPY_DIALOG_peform_copy_and_redraw_screen, %function
COPY_DIALOG_peform_copy_and_redraw_screen:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_copy_dialog.c"
	.loc 1 47 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #24
.LCFI2:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 50 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L2
	.loc 1 52 0
	ldr	r3, .L3
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L3+4
	mov	r3, #52
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 54 0
	ldr	r3, [fp, #-20]
	blx	r3
	.loc 1 58 0
	ldr	r0, [fp, #-24]
	bl	STATION_GROUP_get_group_at_this_index
	str	r0, [fp, #-8]
	.loc 1 60 0
	ldr	r0, [fp, #-28]
	bl	STATION_GROUP_get_group_at_this_index
	str	r0, [fp, #-12]
	.loc 1 62 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	STATION_GROUP_copy_group
	.loc 1 66 0
	ldr	r0, [fp, #-28]
	bl	STATION_GROUP_copy_group_into_guivars
	.loc 1 68 0
	ldr	r3, .L3
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L2:
	.loc 1 71 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 73 0
	ldr	r3, .L3+8
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
	.loc 1 75 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 76 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L4:
	.align	2
.L3:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	GuiLib_ActiveCursorFieldNo
.LFE0:
	.size	COPY_DIALOG_peform_copy_and_redraw_screen, .-COPY_DIALOG_peform_copy_and_redraw_screen
	.section	.text.FDTO_COPY_DIALOG_draw_dialog,"ax",%progbits
	.align	2
	.type	FDTO_COPY_DIALOG_draw_dialog, %function
FDTO_COPY_DIALOG_draw_dialog:
.LFB1:
	.loc 1 80 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #12
.LCFI5:
	str	r0, [fp, #-12]
	.loc 1 83 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L6
	.loc 1 88 0
	ldr	r3, .L8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 90 0
	ldr	r3, .L8+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L8+8
	mov	r3, #90
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 92 0
	mov	r0, #0
	bl	STATION_GROUP_get_group_at_this_index
	mov	r3, r0
	mov	r0, r3
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r0, .L8+12
	mov	r1, r3
	mov	r2, #65
	bl	strlcpy
	.loc 1 94 0
	ldr	r3, .L8+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L7
.L6:
	.loc 1 98 0
	ldr	r3, .L8+16
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L7:
	.loc 1 101 0
	ldr	r3, .L8+20
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 103 0
	mov	r0, #596
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 104 0
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L8
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r1, .L8
	ldr	r1, [r1, #0]
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r1, r1, asl #16
	mov	r1, r1, asr #16
	str	r1, [sp, #0]
	mov	r0, #2
	ldr	r1, .L8+24
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	.loc 1 105 0
	bl	GuiLib_Refresh
	.loc 1 106 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	g_COPY_DIALOG_selected_group_index
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_GroupName
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_COPY_DIALOG_visible
	.word	nm_STATION_GROUP_load_group_name_into_guivar
.LFE1:
	.size	FDTO_COPY_DIALOG_draw_dialog, .-FDTO_COPY_DIALOG_draw_dialog
	.section	.text.COPY_DIALOG_draw_dialog,"ax",%progbits
	.align	2
	.global	COPY_DIALOG_draw_dialog
	.type	COPY_DIALOG_draw_dialog, %function
COPY_DIALOG_draw_dialog:
.LFB2:
	.loc 1 110 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #44
.LCFI8:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	.loc 1 113 0
	ldr	r3, .L11
	ldr	r2, [fp, #-44]
	str	r2, [r3, #0]
	.loc 1 114 0
	ldr	r3, .L11+4
	ldr	r2, [fp, #-48]
	str	r2, [r3, #0]
	.loc 1 119 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 120 0
	ldr	r3, .L11+8
	str	r3, [fp, #-20]
	.loc 1 121 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 122 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 123 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L12:
	.align	2
.L11:
	.word	g_COPY_DIALOG_editing_group
	.word	g_COPY_DIALOG_add_group_func_ptr
	.word	FDTO_COPY_DIALOG_draw_dialog
.LFE2:
	.size	COPY_DIALOG_draw_dialog, .-COPY_DIALOG_draw_dialog
	.section	.text.COPY_DIALOG_process_dialog,"ax",%progbits
	.align	2
	.global	COPY_DIALOG_process_dialog
	.type	COPY_DIALOG_process_dialog, %function
COPY_DIALOG_process_dialog:
.LFB3:
	.loc 1 127 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI9:
	add	fp, sp, #12
.LCFI10:
	sub	sp, sp, #44
.LCFI11:
	str	r0, [fp, #-56]
	str	r1, [fp, #-52]
	.loc 1 130 0
	ldr	r3, [fp, #-56]
	cmp	r3, #67
	beq	.L17
	cmp	r3, #67
	bhi	.L19
	cmp	r3, #2
	beq	.L16
	cmp	r3, #4
	beq	.L15
	cmp	r3, #0
	beq	.L15
	b	.L14
.L19:
	cmp	r3, #83
	beq	.L18
	cmp	r3, #84
	beq	.L15
	cmp	r3, #80
	beq	.L15
	b	.L14
.L18:
	.loc 1 133 0
	bl	KEY_process_ENG_SPA
	.loc 1 135 0
	mov	r3, #1
	str	r3, [fp, #-48]
	.loc 1 136 0
	ldr	r3, .L21
	str	r3, [fp, #-28]
	.loc 1 137 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 138 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 139 0
	b	.L13
.L16:
	.loc 1 142 0
	bl	good_key_beep
	.loc 1 144 0
	ldr	r3, .L21+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 146 0
	ldr	r3, .L21+8
	ldr	r5, [r3, #0]
	ldr	r3, .L21+12
	ldr	r3, [r3, #0]
	mov	r4, r3
	mov	r0, #2
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L21+16
	ldr	r3, [r3, #0]
	mov	r0, r5
	mov	r1, r4
	bl	COPY_DIALOG_peform_copy_and_redraw_screen
	.loc 1 147 0
	b	.L13
.L15:
	.loc 1 153 0
	ldr	r3, [fp, #-56]
	mov	r0, #2
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	.loc 1 154 0
	b	.L13
.L17:
	.loc 1 157 0
	bl	good_key_beep
	.loc 1 159 0
	ldr	r3, .L21+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 161 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 162 0
	b	.L13
.L14:
	.loc 1 165 0
	bl	bad_key_beep
.L13:
	.loc 1 167 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L22:
	.align	2
.L21:
	.word	FDTO_COPY_DIALOG_draw_dialog
	.word	g_COPY_DIALOG_visible
	.word	g_COPY_DIALOG_editing_group
	.word	g_COPY_DIALOG_add_group_func_ptr
	.word	g_GROUP_list_item_index
.LFE3:
	.size	COPY_DIALOG_process_dialog, .-COPY_DIALOG_process_dialog
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/station_groups.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_copy_dialog.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x44f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF55
	.byte	0x1
	.4byte	.LASF56
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x53
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x5
	.byte	0x4
	.4byte	0x92
	.uleb128 0x6
	.4byte	0x99
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x4
	.byte	0x57
	.4byte	0x99
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x5
	.byte	0x4c
	.4byte	0xad
	.uleb128 0x9
	.4byte	0x33
	.4byte	0xd3
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0xf8
	.uleb128 0xc
	.4byte	.LASF16
	.byte	0x6
	.byte	0x7e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF17
	.byte	0x6
	.byte	0x80
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x6
	.byte	0x82
	.4byte	0xd3
	.uleb128 0xb
	.byte	0x24
	.byte	0x7
	.byte	0x78
	.4byte	0x18a
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x7
	.byte	0x7b
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x7
	.byte	0x83
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x7
	.byte	0x86
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x7
	.byte	0x88
	.4byte	0x19b
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x7
	.byte	0x8d
	.4byte	0x1ad
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x7
	.byte	0x92
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x7
	.byte	0x96
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x7
	.byte	0x9a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x7
	.byte	0x9c
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	0x196
	.uleb128 0xe
	.4byte	0x196
	.byte	0
	.uleb128 0xf
	.4byte	0x48
	.uleb128 0x5
	.byte	0x4
	.4byte	0x18a
	.uleb128 0xd
	.byte	0x1
	.4byte	0x1ad
	.uleb128 0xe
	.4byte	0xf8
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1a1
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x7
	.byte	0x9e
	.4byte	0x103
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF29
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x8
	.2byte	0x1a2
	.4byte	0x1d1
	.uleb128 0x11
	.4byte	.LASF30
	.byte	0x1
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.byte	0x2e
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x245
	.uleb128 0x13
	.4byte	.LASF31
	.byte	0x1
	.byte	0x2e
	.4byte	0x245
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF32
	.byte	0x1
	.byte	0x2e
	.4byte	0x251
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.4byte	.LASF33
	.byte	0x1
	.byte	0x2e
	.4byte	0x257
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x13
	.4byte	.LASF34
	.byte	0x1
	.byte	0x2e
	.4byte	0x257
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x14
	.4byte	.LASF35
	.byte	0x1
	.byte	0x30
	.4byte	0x25c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF36
	.byte	0x1
	.byte	0x30
	.4byte	0x25c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x81
	.uleb128 0x15
	.byte	0x1
	.4byte	0x99
	.uleb128 0x5
	.byte	0x4
	.4byte	0x24b
	.uleb128 0xf
	.4byte	0x5a
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1c5
	.uleb128 0x16
	.4byte	.LASF57
	.byte	0x1
	.byte	0x4f
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x297
	.uleb128 0x13
	.4byte	.LASF37
	.byte	0x1
	.byte	0x4f
	.4byte	0x297
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x1
	.byte	0x51
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.4byte	0x81
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.byte	0x6d
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x2e0
	.uleb128 0x13
	.4byte	.LASF31
	.byte	0x1
	.byte	0x6d
	.4byte	0x245
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x13
	.4byte	.LASF32
	.byte	0x1
	.byte	0x6d
	.4byte	0x251
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x17
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x76
	.4byte	0x1b3
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.byte	0x7e
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x316
	.uleb128 0x13
	.4byte	.LASF42
	.byte	0x1
	.byte	0x7e
	.4byte	0x316
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x17
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x80
	.4byte	0x1b3
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.byte	0
	.uleb128 0xf
	.4byte	0xf8
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x32b
	.uleb128 0xa
	.4byte	0x25
	.byte	0x40
	.byte	0
	.uleb128 0x18
	.4byte	.LASF43
	.byte	0x9
	.2byte	0x1fc
	.4byte	0x31b
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF44
	.byte	0xa
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0xb
	.byte	0x30
	.4byte	0x358
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x14
	.4byte	.LASF46
	.byte	0xb
	.byte	0x34
	.4byte	0x36e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0xb
	.byte	0x36
	.4byte	0x384
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0xb
	.byte	0x38
	.4byte	0x39a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xf
	.4byte	0xc3
	.uleb128 0x19
	.4byte	.LASF49
	.byte	0xc
	.byte	0x14
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF50
	.byte	0xd
	.byte	0x78
	.4byte	0xb8
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF51
	.byte	0xe
	.byte	0x65
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF52
	.byte	0x1
	.byte	0x1d
	.4byte	0x245
	.byte	0x5
	.byte	0x3
	.4byte	g_COPY_DIALOG_editing_group
	.uleb128 0x1a
	.4byte	0x99
	.4byte	0x3e2
	.uleb128 0x7
	.byte	0
	.uleb128 0x14
	.4byte	.LASF53
	.byte	0x1
	.byte	0x1f
	.4byte	0x3f3
	.byte	0x5
	.byte	0x3
	.4byte	g_COPY_DIALOG_add_group_func_ptr
	.uleb128 0x5
	.byte	0x4
	.4byte	0x3d7
	.uleb128 0x14
	.4byte	.LASF54
	.byte	0x1
	.byte	0x21
	.4byte	0x5a
	.byte	0x5
	.byte	0x3
	.4byte	g_COPY_DIALOG_selected_group_index
	.uleb128 0x18
	.4byte	.LASF43
	.byte	0x9
	.2byte	0x1fc
	.4byte	0x31b
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF44
	.byte	0xa
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF49
	.byte	0x1
	.byte	0x23
	.4byte	0x81
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_COPY_DIALOG_visible
	.uleb128 0x19
	.4byte	.LASF50
	.byte	0xd
	.byte	0x78
	.4byte	0xb8
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF51
	.byte	0xe
	.byte	0x65
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF10:
	.ascii	"long long int\000"
.LASF43:
	.ascii	"GuiVar_GroupName\000"
.LASF1:
	.ascii	"char\000"
.LASF45:
	.ascii	"GuiFont_LanguageActive\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF24:
	.ascii	"_04_func_ptr\000"
.LASF13:
	.ascii	"portTickType\000"
.LASF20:
	.ascii	"_02_menu\000"
.LASF39:
	.ascii	"COPY_DIALOG_peform_copy_and_redraw_screen\000"
.LASF23:
	.ascii	"key_process_func_ptr\000"
.LASF37:
	.ascii	"pcomplete_redraw\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF15:
	.ascii	"xSemaphoreHandle\000"
.LASF22:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF27:
	.ascii	"_08_screen_to_draw\000"
.LASF36:
	.ascii	"lgroup_to_copy_to\000"
.LASF29:
	.ascii	"float\000"
.LASF14:
	.ascii	"xQueueHandle\000"
.LASF38:
	.ascii	"lcursor_to_select\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF49:
	.ascii	"g_COPY_DIALOG_visible\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF48:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF53:
	.ascii	"g_COPY_DIALOG_add_group_func_ptr\000"
.LASF41:
	.ascii	"COPY_DIALOG_process_dialog\000"
.LASF28:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF40:
	.ascii	"COPY_DIALOG_draw_dialog\000"
.LASF55:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF32:
	.ascii	"add_group_func_ptr\000"
.LASF51:
	.ascii	"g_GROUP_list_item_index\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF19:
	.ascii	"_01_command\000"
.LASF16:
	.ascii	"keycode\000"
.LASF52:
	.ascii	"g_COPY_DIALOG_editing_group\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF42:
	.ascii	"pkey_event\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF17:
	.ascii	"repeats\000"
.LASF56:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_copy_dialog.c\000"
.LASF34:
	.ascii	"pgroup_index_to_copy_to\000"
.LASF25:
	.ascii	"_06_u32_argument1\000"
.LASF18:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF30:
	.ascii	"STATION_GROUP_STRUCT\000"
.LASF31:
	.ascii	"pediting_group\000"
.LASF26:
	.ascii	"_07_u32_argument2\000"
.LASF46:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF33:
	.ascii	"pgroup_index_to_copy_from\000"
.LASF5:
	.ascii	"short int\000"
.LASF57:
	.ascii	"FDTO_COPY_DIALOG_draw_dialog\000"
.LASF54:
	.ascii	"g_COPY_DIALOG_selected_group_index\000"
.LASF21:
	.ascii	"_03_structure_to_draw\000"
.LASF50:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF12:
	.ascii	"long int\000"
.LASF35:
	.ascii	"lgroup_to_copy_from\000"
.LASF3:
	.ascii	"signed char\000"
.LASF44:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF47:
	.ascii	"GuiFont_DecimalChar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
