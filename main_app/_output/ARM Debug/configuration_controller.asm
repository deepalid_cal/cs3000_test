	.file	"configuration_controller.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	comm_device_names
	.section .rodata
	.align	2
.LC0:
	.ascii	"NONE INSTALLED\000"
	.align	2
.LC1:
	.ascii	"FireLine or StingRay\000"
	.align	2
.LC2:
	.ascii	"Freewave LRS455\000"
	.align	2
.LC3:
	.ascii	"Freewave FGR2\000"
	.align	2
.LC4:
	.ascii	"Lantronix UDS1100\000"
	.align	2
.LC5:
	.ascii	"Sierra Wireless LS300\000"
	.align	2
.LC6:
	.ascii	"Lantronix PremierWave XN\000"
	.align	2
.LC7:
	.ascii	"Lantronix PremierWave XC\000"
	.align	2
.LC8:
	.ascii	"Lantronix PremierWave XE\000"
	.align	2
.LC9:
	.ascii	"Lantronix WiBox\000"
	.align	2
.LC10:
	.ascii	"MultiTech LTE\000"
	.section	.rodata.comm_device_names,"a",%progbits
	.align	2
	.type	comm_device_names, %object
	.size	comm_device_names, 44
comm_device_names:
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.global	port_device_table
	.section	.rodata.port_device_table,"a",%progbits
	.align	2
	.type	port_device_table, %object
	.size	port_device_table, 616
port_device_table:
	.word	0
	.word	57600
	.word	0
	.word	1
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	9600
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	RAVEON_initialize_the_connection_process
	.word	RAVEON_connection_processing
	.word	RAVEON_is_connected
	.word	LR_RAVEON_initialize_device_exchange
	.word	LR_RAVEON_exchange_processing
	.word	1
	.word	57600
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	LR_FREEWAVE_power_control
	.word	LR_FREEWAVE_initialize_the_connection_process
	.word	LR_FREEWAVE_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	LR_FREEWAVE_initialize_device_exchange
	.word	LR_FREEWAVE_exchange_processing
	.word	1
	.word	57600
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	SR_FREEWAVE_power_control
	.word	SR_FREEWAVE_initialize_the_connection_process
	.word	SR_FREEWAVE_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	SR_FREEWAVE_initialize_device_exchange
	.word	SR_FREEWAVE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	UDS1100_initialize_the_connection_process
	.word	UDS1100_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	0
	.word	57600
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	0
	.word	0
	.word	__GENERIC_gr_card_is_connected
	.word	GR_AIRLINK_initialize_device_exchange
	.word	GR_AIRLINK_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	WIBOX_initialize_the_connection_process
	.word	WIBOX_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	PWXC_initialize_the_connection_process
	.word	PWXC_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	WIBOX_initialize_the_connection_process
	.word	WIBOX_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	WIBOX_initialize_the_connection_process
	.word	WIBOX_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	MTSMC_LAT_initialize_the_connection_process
	.word	MTSMC_LAT_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.section	.rodata.CONTROLLER_CONFIG_FILENAME,"a",%progbits
	.align	2
	.type	CONTROLLER_CONFIG_FILENAME, %object
	.size	CONTROLLER_CONFIG_FILENAME, 25
CONTROLLER_CONFIG_FILENAME:
	.ascii	"CONTROLLER_CONFIGURATION\000"
	.global	config_c
	.section	.bss.config_c,"aw",%nobits
	.align	2
	.type	config_c, %object
	.size	config_c, 148
config_c:
	.space	148
	.section .rodata
	.align	2
.LC11:
	.ascii	"64.73.242.99\000"
	.align	2
.LC12:
	.ascii	"16001\000"
	.section	.text.init_config_controller,"ax",%progbits
	.align	2
	.type	init_config_controller, %function
init_config_controller:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/configuration_controller.c"
	.loc 1 421 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	.loc 1 435 0
	ldr	r2, .L4
	ldrb	r3, [r2, #116]
	orr	r3, r3, #4
	strb	r3, [r2, #116]
	.loc 1 437 0
	ldr	r2, .L4
	ldrb	r3, [r2, #116]
	orr	r3, r3, #8
	strb	r3, [r2, #116]
	.loc 1 439 0
	ldr	r2, .L4
	ldrb	r3, [r2, #116]
	orr	r3, r3, #1
	strb	r3, [r2, #116]
	.loc 1 441 0
	ldr	r2, .L4
	ldrb	r3, [r2, #116]
	orr	r3, r3, #2
	strb	r3, [r2, #116]
	.loc 1 447 0
	ldr	r2, .L4
	ldrb	r3, [r2, #52]
	bic	r3, r3, #1
	strb	r3, [r2, #52]
	.loc 1 449 0
	ldr	r2, .L4
	ldrb	r3, [r2, #52]
	bic	r3, r3, #2
	strb	r3, [r2, #52]
	.loc 1 451 0
	ldr	r2, .L4
	ldrb	r3, [r2, #52]
	bic	r3, r3, #4
	strb	r3, [r2, #52]
	.loc 1 453 0
	ldr	r2, .L4
	ldrb	r3, [r2, #52]
	bic	r3, r3, #8
	strb	r3, [r2, #52]
	.loc 1 455 0
	ldr	r2, .L4
	ldrb	r3, [r2, #53]
	bic	r3, r3, #16
	strb	r3, [r2, #53]
	.loc 1 461 0
	ldr	r2, .L4
	ldrb	r3, [r2, #52]
	bic	r3, r3, #32
	orr	r3, r3, #16
	strb	r3, [r2, #52]
	.loc 1 463 0
	ldr	r2, .L4
	ldrb	r3, [r2, #52]
	bic	r3, r3, #64
	strb	r3, [r2, #52]
	.loc 1 469 0
	ldr	r2, .L4
	ldrb	r3, [r2, #52]
	orr	r3, r3, #128
	strb	r3, [r2, #52]
	.loc 1 473 0
	ldr	r2, .L4
	ldrb	r3, [r2, #53]
	bic	r3, r3, #2
	orr	r3, r3, #1
	strb	r3, [r2, #53]
	.loc 1 475 0
	ldr	r2, .L4
	ldrb	r3, [r2, #53]
	bic	r3, r3, #4
	strb	r3, [r2, #53]
	.loc 1 481 0
	ldr	r2, .L4
	ldrb	r3, [r2, #53]
	orr	r3, r3, #8
	strb	r3, [r2, #53]
	.loc 1 486 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L2
.L3:
	.loc 1 488 0 discriminator 2
	ldr	r3, .L4
	ldr	r2, [fp, #-8]
	add	r2, r2, #14
	mov	r1, #0
	str	r1, [r3, r2, asl #2]
	.loc 1 491 0 discriminator 2
	ldr	r2, .L4
	ldr	r3, [fp, #-8]
	add	r1, r3, #14
	ldrb	r3, [r2, r1, asl #2]
	orr	r3, r3, #32
	strb	r3, [r2, r1, asl #2]
	.loc 1 486 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L2:
	.loc 1 486 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #5
	bls	.L3
	.loc 1 498 0 is_stmt 1
	ldr	r3, .L4
	mov	r2, #0
	str	r2, [r3, #80]
	.loc 1 500 0
	ldr	r3, .L4
	mov	r2, #0
	str	r2, [r3, #84]
	.loc 1 511 0
	ldr	r0, .L4+4
	mov	r1, #16
	ldr	r2, .L4+8
	bl	snprintf
	.loc 1 513 0
	ldr	r0, .L4+12
	mov	r1, #8
	ldr	r2, .L4+16
	bl	snprintf
	.loc 1 521 0
	ldr	r3, .L4
	mov	r2, #0
	str	r2, [r3, #112]
	.loc 1 526 0
	ldr	r3, .L4
	ldr	r2, .L4+20
	str	r2, [r3, #48]
	.loc 1 530 0
	ldr	r3, .L4
	mov	r2, #0
	str	r2, [r3, #120]
	.loc 1 531 0
	ldr	r3, .L4
	mov	r2, #1
	str	r2, [r3, #124]
	.loc 1 532 0
	ldr	r3, .L4
	mov	r2, #25
	str	r2, [r3, #128]
	.loc 1 533 0
	ldr	r3, .L4
	mov	r2, #5
	str	r2, [r3, #132]
	.loc 1 538 0
	ldr	r3, .L4
	mov	r2, #120
	str	r2, [r3, #136]
	.loc 1 543 0
	ldr	r3, .L4
	mov	r2, #1024
	str	r2, [r3, #140]
	.loc 1 549 0
	ldr	r3, .L4
	mov	r2, #1
	str	r2, [r3, #144]
	.loc 1 550 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	config_c
	.word	config_c+88
	.word	.LC11
	.word	config_c+104
	.word	.LC12
	.word	50065
.LFE0:
	.size	init_config_controller, .-init_config_controller
	.section .rodata
	.align	2
.LC13:
	.ascii	"CONTLR CONFIG file unexpd update %u\000"
	.align	2
.LC14:
	.ascii	"CONTLR CONFIG file update : to revision %u from %u\000"
	.align	2
.LC15:
	.ascii	"CONTLR CONFIG updater error\000"
	.section	.text.controller_config_updater,"ax",%progbits
	.align	2
	.type	controller_config_updater, %function
controller_config_updater:
.LFB1:
	.loc 1 554 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 561 0
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	bne	.L7
	.loc 1 563 0
	ldr	r0, .L11
	ldr	r1, [fp, #-8]
	bl	Alert_Message_va
	b	.L8
.L7:
	.loc 1 567 0
	ldr	r0, .L11+4
	mov	r1, #2
	ldr	r2, [fp, #-8]
	bl	Alert_Message_va
	.loc 1 583 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L9
	.loc 1 589 0
	ldr	r3, .L11+8
	mov	r2, #1
	str	r2, [r3, #144]
	.loc 1 593 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L9:
	.loc 1 598 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L8
	.loc 1 604 0
	ldr	r2, .L11+8
	ldrb	r3, [r2, #52]
	bic	r3, r3, #32
	orr	r3, r3, #16
	strb	r3, [r2, #52]
	.loc 1 606 0
	ldr	r2, .L11+8
	ldrb	r3, [r2, #52]
	bic	r3, r3, #64
	strb	r3, [r2, #52]
	.loc 1 612 0
	ldr	r2, .L11+8
	ldrb	r3, [r2, #52]
	orr	r3, r3, #128
	strb	r3, [r2, #52]
	.loc 1 616 0
	ldr	r2, .L11+8
	ldrb	r3, [r2, #53]
	bic	r3, r3, #2
	orr	r3, r3, #1
	strb	r3, [r2, #53]
	.loc 1 618 0
	ldr	r2, .L11+8
	ldrb	r3, [r2, #53]
	bic	r3, r3, #4
	strb	r3, [r2, #53]
	.loc 1 624 0
	ldr	r2, .L11+8
	ldrb	r3, [r2, #53]
	orr	r3, r3, #8
	strb	r3, [r2, #53]
	.loc 1 626 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L8:
	.loc 1 644 0
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	beq	.L6
	.loc 1 646 0
	ldr	r0, .L11+12
	bl	Alert_Message
.L6:
	.loc 1 648 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L12:
	.align	2
.L11:
	.word	.LC13
	.word	.LC14
	.word	config_c
	.word	.LC15
.LFE1:
	.size	controller_config_updater, .-controller_config_updater
	.section	.text.init_file_configuration_controller,"ax",%progbits
	.align	2
	.global	init_file_configuration_controller
	.type	init_file_configuration_controller, %function
init_file_configuration_controller:
.LFB2:
	.loc 1 652 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #20
.LCFI8:
	.loc 1 659 0
	mov	r3, #148
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r3, .L14
	str	r3, [sp, #8]
	ldr	r3, .L14+4
	str	r3, [sp, #12]
	mov	r3, #0
	str	r3, [sp, #16]
	mov	r0, #0
	ldr	r1, .L14+8
	mov	r2, #2
	ldr	r3, .L14+12
	bl	FLASH_FILE_find_or_create_variable_file
	.loc 1 668 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L15:
	.align	2
.L14:
	.word	controller_config_updater
	.word	init_config_controller
	.word	CONTROLLER_CONFIG_FILENAME
	.word	config_c
.LFE2:
	.size	init_file_configuration_controller, .-init_file_configuration_controller
	.section	.text.save_file_configuration_controller,"ax",%progbits
	.align	2
	.global	save_file_configuration_controller
	.type	save_file_configuration_controller, %function
save_file_configuration_controller:
.LFB3:
	.loc 1 672 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #12
.LCFI11:
	.loc 1 673 0
	mov	r3, #148
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r0, #0
	ldr	r1, .L17
	mov	r2, #2
	ldr	r3, .L17+4
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	.loc 1 680 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L18:
	.align	2
.L17:
	.word	CONTROLLER_CONFIG_FILENAME
	.word	config_c
.LFE3:
	.size	save_file_configuration_controller, .-save_file_configuration_controller
	.section	.text.CONFIG_this_controller_originates_commserver_messages,"ax",%progbits
	.align	2
	.global	CONFIG_this_controller_originates_commserver_messages
	.type	CONFIG_this_controller_originates_commserver_messages, %function
CONFIG_this_controller_originates_commserver_messages:
.LFB4:
	.loc 1 685 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #4
.LCFI14:
	.loc 1 699 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 701 0
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	mov	r3, r0
	cmp	r3, #0
	beq	.L20
	.loc 1 703 0
	ldr	r3, .L21
	ldr	r2, [r3, #80]
	ldr	r1, .L21+4
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #3
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L20
	.loc 1 705 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L20:
	.loc 1 709 0
	ldr	r3, [fp, #-8]
	.loc 1 710 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L22:
	.align	2
.L21:
	.word	config_c
	.word	port_device_table
.LFE4:
	.size	CONFIG_this_controller_originates_commserver_messages, .-CONFIG_this_controller_originates_commserver_messages
	.section .rodata
	.align	2
.LC16:
	.ascii	"CI: cannot check for connection\000"
	.section	.text.CONFIG_is_connected,"ax",%progbits
	.align	2
	.global	CONFIG_is_connected
	.type	CONFIG_is_connected, %function
CONFIG_is_connected:
.LFB5:
	.loc 1 714 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #4
.LCFI17:
	.loc 1 717 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 721 0
	bl	CONFIG_this_controller_originates_commserver_messages
	mov	r3, r0
	cmp	r3, #0
	beq	.L24
	.loc 1 723 0
	ldr	r3, .L26
	ldr	r2, [r3, #80]
	ldr	r0, .L26+4
	mov	r1, #44
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L25
	.loc 1 725 0
	ldr	r3, .L26
	ldr	r2, [r3, #80]
	ldr	r0, .L26+4
	mov	r1, #44
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #3
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	mov	r0, #1
	blx	r3
	str	r0, [fp, #-8]
	b	.L24
.L25:
	.loc 1 729 0
	ldr	r0, .L26+8
	bl	Alert_Message
.L24:
	.loc 1 733 0
	ldr	r3, [fp, #-8]
	.loc 1 734 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L27:
	.align	2
.L26:
	.word	config_c
	.word	port_device_table
	.word	.LC16
.LFE5:
	.size	CONFIG_is_connected, .-CONFIG_is_connected
	.section	.text.CONFIG_this_controller_is_a_configured_hub,"ax",%progbits
	.align	2
	.global	CONFIG_this_controller_is_a_configured_hub
	.type	CONFIG_this_controller_is_a_configured_hub, %function
CONFIG_this_controller_is_a_configured_hub:
.LFB6:
	.loc 1 738 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #4
.LCFI20:
	.loc 1 747 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 752 0
	ldr	r3, .L31
	ldrb	r3, [r3, #52]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L29
	.loc 1 752 0 is_stmt 0 discriminator 1
	ldr	r3, .L31
	ldr	r3, [r3, #144]
	cmp	r3, #0
	beq	.L29
	.loc 1 754 0 is_stmt 1
	bl	CONFIG_this_controller_originates_commserver_messages
	mov	r3, r0
	cmp	r3, #0
	beq	.L29
	.loc 1 757 0
	ldr	r3, .L31
	ldr	r3, [r3, #84]
	cmp	r3, #2
	beq	.L30
	.loc 1 757 0 is_stmt 0 discriminator 1
	ldr	r3, .L31
	ldr	r3, [r3, #84]
	cmp	r3, #1
	beq	.L30
	ldr	r3, .L31
	ldr	r3, [r3, #84]
	cmp	r3, #3
	bne	.L29
.L30:
	.loc 1 759 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
.L29:
	.loc 1 764 0
	ldr	r3, [fp, #-8]
	.loc 1 765 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L32:
	.align	2
.L31:
	.word	config_c
.LFE6:
	.size	CONFIG_this_controller_is_a_configured_hub, .-CONFIG_this_controller_is_a_configured_hub
	.section	.text.CONFIG_this_controller_is_behind_a_hub,"ax",%progbits
	.align	2
	.global	CONFIG_this_controller_is_behind_a_hub
	.type	CONFIG_this_controller_is_behind_a_hub, %function
CONFIG_this_controller_is_behind_a_hub:
.LFB7:
	.loc 1 769 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #4
.LCFI23:
	.loc 1 772 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 777 0
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	mov	r3, r0
	cmp	r3, #0
	beq	.L34
	.loc 1 779 0
	ldr	r3, .L36
	ldr	r3, [r3, #80]
	cmp	r3, #2
	beq	.L35
	.loc 1 779 0 is_stmt 0 discriminator 1
	ldr	r3, .L36
	ldr	r3, [r3, #80]
	cmp	r3, #1
	beq	.L35
	ldr	r3, .L36
	ldr	r3, [r3, #80]
	cmp	r3, #3
	bne	.L34
.L35:
	.loc 1 781 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
.L34:
	.loc 1 785 0
	ldr	r3, [fp, #-8]
	.loc 1 786 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L37:
	.align	2
.L36:
	.word	config_c
.LFE7:
	.size	CONFIG_this_controller_is_behind_a_hub, .-CONFIG_this_controller_is_behind_a_hub
	.section	.text.CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets,"ax",%progbits
	.align	2
	.global	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets
	.type	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets, %function
CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets:
.LFB8:
	.loc 1 790 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI24:
	add	fp, sp, #0
.LCFI25:
	sub	sp, sp, #12
.LCFI26:
	str	r0, [fp, #-12]
	.loc 1 795 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 805 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	beq	.L39
	.loc 1 805 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #2
	bne	.L40
.L39:
	.loc 1 807 0 is_stmt 1
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L41
	.loc 1 809 0
	ldr	r3, .L43
	ldr	r3, [r3, #80]
	str	r3, [fp, #-8]
	b	.L42
.L41:
	.loc 1 813 0
	ldr	r3, .L43
	ldr	r3, [r3, #84]
	str	r3, [fp, #-8]
.L42:
	.loc 1 816 0
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bne	.L40
	.loc 1 818 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L40:
	.loc 1 822 0
	ldr	r3, [fp, #-4]
	.loc 1 823 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L44:
	.align	2
.L43:
	.word	config_c
.LFE8:
	.size	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets, .-CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets
	.section	.text.init_a_to_d,"ax",%progbits
	.align	2
	.global	init_a_to_d
	.type	init_a_to_d, %function
init_a_to_d:
.LFB9:
	.loc 1 827 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #8
.LCFI29:
	.loc 1 830 0
	ldr	r3, .L46
	str	r3, [fp, #-8]
	.loc 1 838 0
	mov	r0, #1
	mov	r1, #3
	bl	clkpwr_setup_adc_ts
	.loc 1 841 0
	mov	r0, #13
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 848 0
	mov	r0, #39
	bl	xDisable_ISR
	.loc 1 850 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #39
	mov	r1, #21
	mov	r2, #2
	mov	r3, #0
	bl	xSetISR_Vector
	.loc 1 858 0
	ldr	r3, [fp, #-8]
	mov	r2, #644
	str	r2, [r3, #4]
	.loc 1 861 0
	ldr	r3, [fp, #-8]
	mov	r2, #4
	str	r2, [r3, #8]
	.loc 1 864 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L47:
	.align	2
.L46:
	.word	1074036736
.LFE9:
	.size	init_a_to_d, .-init_a_to_d
	.section .rodata
	.align	2
.LC17:
	.ascii	"AD: poor channel choice.\000"
	.align	2
.LC18:
	.ascii	"AD: never finishes\000"
	.section	.text.AD_conversion_and_return_results,"ax",%progbits
	.align	2
	.type	AD_conversion_and_return_results, %function
AD_conversion_and_return_results:
.LFB10:
	.loc 1 869 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #16
.LCFI32:
	str	r0, [fp, #-20]
	.loc 1 882 0
	ldr	r3, .L55
	str	r3, [fp, #-12]
	.loc 1 886 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #72]
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	str	r3, [fp, #-16]
	.loc 1 890 0
	ldr	r3, .L55+4
	mov	r2, #128
	str	r2, [r3, #0]
	.loc 1 894 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L49
	.loc 1 896 0
	ldr	r3, [fp, #-12]
	mov	r2, #644
	str	r2, [r3, #4]
	b	.L50
.L49:
	.loc 1 899 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L51
	.loc 1 901 0
	ldr	r3, [fp, #-12]
	mov	r2, #660
	str	r2, [r3, #4]
	b	.L50
.L51:
	.loc 1 905 0
	ldr	r0, .L55+8
	bl	Alert_Message
.L50:
	.loc 1 911 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #8]
	orr	r2, r3, #2
	ldr	r3, [fp, #-12]
	str	r2, [r3, #8]
	.loc 1 916 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L54:
	.loc 1 923 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-8]
	cmp	r3, #200
	bls	.L52
	.loc 1 925 0
	ldr	r0, .L55+12
	bl	Alert_Message
	.loc 1 928 0
	b	.L53
.L52:
	.loc 1 931 0
	ldr	r3, .L55+4
	ldr	r3, [r3, #0]
	and	r3, r3, #128
	cmp	r3, #0
	beq	.L54
.L53:
	.loc 1 936 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #72]
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	.loc 1 939 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L56:
	.align	2
.L55:
	.word	1074036736
	.word	1073790980
	.word	.LC17
	.word	.LC18
.LFE10:
	.size	AD_conversion_and_return_results, .-AD_conversion_and_return_results
	.section .rodata
	.align	2
.LC19:
	.ascii	"Port %c ID error. (Interface card resistors?)\000"
	.align	2
.LC20:
	.ascii	"New Port %c Device: %s\000"
	.align	2
.LC21:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/conf"
	.ascii	"iguration/configuration_controller.c\000"
	.align	2
.LC22:
	.ascii	"Same Port %c Device: %s\000"
	.section	.text.CONFIG_device_discovery,"ax",%progbits
	.align	2
	.global	CONFIG_device_discovery
	.type	CONFIG_device_discovery, %function
CONFIG_device_discovery:
.LFB11:
	.loc 1 943 0
	@ args = 0, pretend = 0, frame = 100
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #104
.LCFI35:
	str	r0, [fp, #-104]
	.loc 1 958 0
	ldr	r3, .L70	@ float
	str	r3, [fp, #-96]	@ float
	.loc 1 960 0
	ldr	r3, .L70+4	@ float
	str	r3, [fp, #-100]	@ float
	.loc 1 964 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 968 0
	ldr	r3, [fp, #-104]
	cmp	r3, #1
	beq	.L58
	.loc 1 968 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-104]
	cmp	r3, #2
	bne	.L57
.L58:
	.loc 1 970 0 is_stmt 1
	ldr	r3, [fp, #-104]
	cmp	r3, #1
	bne	.L60
	.loc 1 972 0
	mov	r3, #65
	strb	r3, [fp, #-21]
	b	.L61
.L60:
	.loc 1 976 0
	mov	r3, #66
	strb	r3, [fp, #-21]
.L61:
	.loc 1 1006 0
	mov	r3, #0
	str	r3, [fp, #-16]
.L64:
	.loc 1 1010 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
	.loc 1 1012 0
	ldr	r3, [fp, #-104]
	cmp	r3, #1
	bne	.L62
	.loc 1 1014 0
	mov	r0, #0
	bl	AD_conversion_and_return_results
	str	r0, [fp, #-8]
	.loc 1 1016 0
	mov	r0, #0
	bl	AD_conversion_and_return_results
	str	r0, [fp, #-12]
	b	.L63
.L62:
	.loc 1 1020 0
	mov	r0, #1
	bl	AD_conversion_and_return_results
	str	r0, [fp, #-8]
	.loc 1 1022 0
	mov	r0, #1
	bl	AD_conversion_and_return_results
	str	r0, [fp, #-12]
.L63:
	.loc 1 1025 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bne	.L64
	.loc 1 1033 0
	ldr	r3, [fp, #-8]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-96]
	fdivs	s14, s14, s15
	flds	s15, [fp, #-100]
	fmuls	s15, s14, s15
	fmrs	r0, s15
	bl	__round_UNS16
	mov	r3, r0
	str	r3, [fp, #-28]
	.loc 1 1037 0
	ldr	r3, [fp, #-28]
	cmp	r3, #10
	bhi	.L65
	.loc 1 1039 0
	ldr	r3, [fp, #-104]
	cmp	r3, #1
	bne	.L66
	.loc 1 1041 0
	ldr	r3, .L70+8
	ldr	r2, [r3, #80]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	beq	.L67
	.loc 1 1043 0
	ldr	r3, .L70+8
	ldr	r2, [fp, #-28]
	str	r2, [r3, #80]
	.loc 1 1045 0
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L67
.L66:
	.loc 1 1050 0
	ldr	r3, .L70+8
	ldr	r2, [r3, #84]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	beq	.L67
	.loc 1 1052 0
	ldr	r3, .L70+8
	ldr	r2, [fp, #-28]
	str	r2, [r3, #84]
	.loc 1 1054 0
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L67
.L65:
	.loc 1 1061 0
	ldrb	r3, [fp, #-21]	@ zero_extendqisi2
	sub	r2, fp, #92
	mov	r0, r2
	mov	r1, #64
	ldr	r2, .L70+12
	bl	snprintf
	.loc 1 1063 0
	sub	r3, fp, #92
	mov	r0, r3
	bl	Alert_Message
	.loc 1 1069 0
	ldr	r3, [fp, #-104]
	cmp	r3, #1
	bne	.L68
	.loc 1 1071 0
	ldr	r3, .L70+8
	ldr	r3, [r3, #80]
	cmp	r3, #0
	beq	.L67
	.loc 1 1073 0
	ldr	r3, .L70+8
	mov	r2, #0
	str	r2, [r3, #80]
	.loc 1 1075 0
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L67
.L68:
	.loc 1 1080 0
	ldr	r3, .L70+8
	ldr	r3, [r3, #84]
	cmp	r3, #0
	beq	.L67
	.loc 1 1082 0
	ldr	r3, .L70+8
	mov	r2, #0
	str	r2, [r3, #84]
	.loc 1 1084 0
	mov	r3, #1
	str	r3, [fp, #-20]
.L67:
	.loc 1 1092 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L69
	.loc 1 1094 0
	ldrb	r3, [fp, #-21]	@ zero_extendqisi2
	ldr	r2, .L70+16
	ldr	r1, [fp, #-28]
	ldr	r1, [r2, r1, asl #2]
	sub	r2, fp, #92
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #64
	ldr	r2, .L70+20
	bl	snprintf
	.loc 1 1096 0
	sub	r3, fp, #92
	mov	r0, r3
	bl	Alert_Message
	.loc 1 1104 0
	mov	r0, #0
	mov	r1, #4
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	.loc 1 1110 0
	ldr	r3, .L70+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L70+28
	ldr	r3, .L70+32
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1114 0
	ldr	r3, .L70+36
	mov	r2, #1
	str	r2, [r3, #204]
	.loc 1 1116 0
	ldr	r3, .L70+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L57
.L69:
	.loc 1 1120 0
	ldrb	r3, [fp, #-21]	@ zero_extendqisi2
	ldr	r2, .L70+16
	ldr	r1, [fp, #-28]
	ldr	r1, [r2, r1, asl #2]
	sub	r2, fp, #92
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #64
	ldr	r2, .L70+40
	bl	snprintf
	.loc 1 1122 0
	sub	r3, fp, #92
	mov	r0, r3
	bl	Alert_Message
.L57:
	.loc 1 1126 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L71:
	.align	2
.L70:
	.word	1149239296
	.word	1107296256
	.word	config_c
	.word	.LC19
	.word	comm_device_names
	.word	.LC20
	.word	irri_comm_recursive_MUTEX
	.word	.LC21
	.word	1110
	.word	irri_comm
	.word	.LC22
.LFE11:
	.size	CONFIG_device_discovery, .-CONFIG_device_discovery
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/configuration_controller.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/foal_comm.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_irri.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/irri_comm.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_tsc.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_intc_driver.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xf8f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF224
	.byte	0x1
	.4byte	.LASF225
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x4c
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x9d
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x5
	.byte	0x4
	.4byte	0xa4
	.uleb128 0x6
	.byte	0x1
	.4byte	0xb0
	.uleb128 0x7
	.4byte	0xb0
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x4
	.byte	0x57
	.4byte	0xb0
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x5
	.byte	0x4c
	.4byte	0xbd
	.uleb128 0x9
	.4byte	0x33
	.4byte	0xe3
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.4byte	0x5a
	.uleb128 0x9
	.4byte	0x5a
	.4byte	0xf8
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xc
	.byte	0x4
	.byte	0x6
	.byte	0x2f
	.4byte	0x1ef
	.uleb128 0xd
	.4byte	.LASF17
	.byte	0x6
	.byte	0x35
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF18
	.byte	0x6
	.byte	0x3e
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF19
	.byte	0x6
	.byte	0x3f
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF20
	.byte	0x6
	.byte	0x46
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF21
	.byte	0x6
	.byte	0x4e
	.4byte	0x5a
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF22
	.byte	0x6
	.byte	0x4f
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF23
	.byte	0x6
	.byte	0x50
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF24
	.byte	0x6
	.byte	0x52
	.4byte	0x5a
	.byte	0x4
	.byte	0x2
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF25
	.byte	0x6
	.byte	0x53
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF26
	.byte	0x6
	.byte	0x54
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF27
	.byte	0x6
	.byte	0x58
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF28
	.byte	0x6
	.byte	0x59
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF29
	.byte	0x6
	.byte	0x5a
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF30
	.byte	0x6
	.byte	0x5b
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0x6
	.byte	0x2b
	.4byte	0x208
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x6
	.byte	0x2d
	.4byte	0x41
	.uleb128 0x10
	.4byte	0xf8
	.byte	0
	.uleb128 0xc
	.byte	0x4
	.byte	0x6
	.byte	0x29
	.4byte	0x219
	.uleb128 0x11
	.4byte	0x1ef
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF31
	.byte	0x6
	.byte	0x61
	.4byte	0x208
	.uleb128 0xc
	.byte	0x4
	.byte	0x6
	.byte	0x6c
	.4byte	0x271
	.uleb128 0xd
	.4byte	.LASF32
	.byte	0x6
	.byte	0x70
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF33
	.byte	0x6
	.byte	0x76
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF34
	.byte	0x6
	.byte	0x7a
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF35
	.byte	0x6
	.byte	0x7c
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0x6
	.byte	0x68
	.4byte	0x28a
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x6
	.byte	0x6a
	.4byte	0x41
	.uleb128 0x10
	.4byte	0x224
	.byte	0
	.uleb128 0xc
	.byte	0x4
	.byte	0x6
	.byte	0x66
	.4byte	0x29b
	.uleb128 0x11
	.4byte	0x271
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF37
	.byte	0x6
	.byte	0x82
	.4byte	0x28a
	.uleb128 0xc
	.byte	0x38
	.byte	0x6
	.byte	0xd2
	.4byte	0x379
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x6
	.byte	0xdc
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF39
	.byte	0x6
	.byte	0xe0
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x12
	.4byte	.LASF40
	.byte	0x6
	.byte	0xe9
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x12
	.4byte	.LASF41
	.byte	0x6
	.byte	0xed
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x12
	.4byte	.LASF42
	.byte	0x6
	.byte	0xef
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x12
	.4byte	.LASF43
	.byte	0x6
	.byte	0xf7
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x12
	.4byte	.LASF44
	.byte	0x6
	.byte	0xf9
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x12
	.4byte	.LASF45
	.byte	0x6
	.byte	0xfc
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x13
	.4byte	.LASF46
	.byte	0x6
	.2byte	0x102
	.4byte	0x38a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x13
	.4byte	.LASF47
	.byte	0x6
	.2byte	0x107
	.4byte	0x39c
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x13
	.4byte	.LASF48
	.byte	0x6
	.2byte	0x10a
	.4byte	0x39c
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x13
	.4byte	.LASF49
	.byte	0x6
	.2byte	0x10f
	.4byte	0x3b2
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x13
	.4byte	.LASF50
	.byte	0x6
	.2byte	0x115
	.4byte	0x3ba
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x13
	.4byte	.LASF51
	.byte	0x6
	.2byte	0x119
	.4byte	0x9e
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x6
	.byte	0x1
	.4byte	0x38a
	.uleb128 0x7
	.4byte	0x5a
	.uleb128 0x7
	.4byte	0x81
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x379
	.uleb128 0x6
	.byte	0x1
	.4byte	0x39c
	.uleb128 0x7
	.4byte	0x5a
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x390
	.uleb128 0x14
	.byte	0x1
	.4byte	0x81
	.4byte	0x3b2
	.uleb128 0x7
	.4byte	0x5a
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x3a2
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x5
	.byte	0x4
	.4byte	0x3b8
	.uleb128 0x16
	.4byte	.LASF52
	.byte	0x6
	.2byte	0x11b
	.4byte	0x2a6
	.uleb128 0x17
	.byte	0x4
	.byte	0x6
	.2byte	0x126
	.4byte	0x442
	.uleb128 0x18
	.4byte	.LASF53
	.byte	0x6
	.2byte	0x12a
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x18
	.4byte	.LASF54
	.byte	0x6
	.2byte	0x12b
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x18
	.4byte	.LASF55
	.byte	0x6
	.2byte	0x12c
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x18
	.4byte	.LASF56
	.byte	0x6
	.2byte	0x12d
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x18
	.4byte	.LASF57
	.byte	0x6
	.2byte	0x12e
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x18
	.4byte	.LASF58
	.byte	0x6
	.2byte	0x135
	.4byte	0x8c
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x19
	.byte	0x4
	.byte	0x6
	.2byte	0x122
	.4byte	0x45d
	.uleb128 0x1a
	.4byte	.LASF36
	.byte	0x6
	.2byte	0x124
	.4byte	0x5a
	.uleb128 0x10
	.4byte	0x3cc
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.byte	0x6
	.2byte	0x120
	.4byte	0x46f
	.uleb128 0x11
	.4byte	0x442
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x16
	.4byte	.LASF59
	.byte	0x6
	.2byte	0x13a
	.4byte	0x45d
	.uleb128 0x17
	.byte	0x94
	.byte	0x6
	.2byte	0x13e
	.4byte	0x589
	.uleb128 0x13
	.4byte	.LASF60
	.byte	0x6
	.2byte	0x14b
	.4byte	0x589
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF61
	.byte	0x6
	.2byte	0x150
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x13
	.4byte	.LASF62
	.byte	0x6
	.2byte	0x153
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x13
	.4byte	.LASF63
	.byte	0x6
	.2byte	0x158
	.4byte	0x599
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x13
	.4byte	.LASF64
	.byte	0x6
	.2byte	0x15e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x6
	.2byte	0x160
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x13
	.4byte	.LASF66
	.byte	0x6
	.2byte	0x16a
	.4byte	0x5a9
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x13
	.4byte	.LASF67
	.byte	0x6
	.2byte	0x170
	.4byte	0x5b9
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x13
	.4byte	.LASF68
	.byte	0x6
	.2byte	0x17a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x13
	.4byte	.LASF69
	.byte	0x6
	.2byte	0x17e
	.4byte	0x29b
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x13
	.4byte	.LASF70
	.byte	0x6
	.2byte	0x186
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x13
	.4byte	.LASF71
	.byte	0x6
	.2byte	0x191
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x13
	.4byte	.LASF72
	.byte	0x6
	.2byte	0x1b1
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x13
	.4byte	.LASF73
	.byte	0x6
	.2byte	0x1b3
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x13
	.4byte	.LASF74
	.byte	0x6
	.2byte	0x1b9
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x13
	.4byte	.LASF75
	.byte	0x6
	.2byte	0x1c1
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x13
	.4byte	.LASF76
	.byte	0x6
	.2byte	0x1d0
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x599
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0x9
	.4byte	0x46f
	.4byte	0x5a9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x5b9
	.uleb128 0xa
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x5c9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x7
	.byte	0
	.uleb128 0x16
	.4byte	.LASF77
	.byte	0x6
	.2byte	0x1d6
	.4byte	0x47b
	.uleb128 0x5
	.byte	0x4
	.4byte	0x5db
	.uleb128 0x1b
	.4byte	0x2c
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF78
	.uleb128 0x9
	.4byte	0x5a
	.4byte	0x5f7
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x17
	.byte	0x18
	.byte	0x7
	.2byte	0x210
	.4byte	0x65b
	.uleb128 0x13
	.4byte	.LASF79
	.byte	0x7
	.2byte	0x215
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF80
	.byte	0x7
	.2byte	0x217
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF81
	.byte	0x7
	.2byte	0x21e
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF82
	.byte	0x7
	.2byte	0x220
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF83
	.byte	0x7
	.2byte	0x224
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x13
	.4byte	.LASF84
	.byte	0x7
	.2byte	0x22d
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x16
	.4byte	.LASF85
	.byte	0x7
	.2byte	0x22f
	.4byte	0x5f7
	.uleb128 0x17
	.byte	0x10
	.byte	0x7
	.2byte	0x253
	.4byte	0x6ad
	.uleb128 0x13
	.4byte	.LASF86
	.byte	0x7
	.2byte	0x258
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF87
	.byte	0x7
	.2byte	0x25a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF88
	.byte	0x7
	.2byte	0x260
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF89
	.byte	0x7
	.2byte	0x263
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x16
	.4byte	.LASF90
	.byte	0x7
	.2byte	0x268
	.4byte	0x667
	.uleb128 0x17
	.byte	0x8
	.byte	0x7
	.2byte	0x26c
	.4byte	0x6e1
	.uleb128 0x13
	.4byte	.LASF86
	.byte	0x7
	.2byte	0x271
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF87
	.byte	0x7
	.2byte	0x273
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x16
	.4byte	.LASF91
	.byte	0x7
	.2byte	0x27c
	.4byte	0x6b9
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x6fd
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3f
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF92
	.uleb128 0x9
	.4byte	0x81
	.4byte	0x714
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0xc
	.byte	0x14
	.byte	0x8
	.byte	0x9c
	.4byte	0x763
	.uleb128 0x12
	.4byte	.LASF86
	.byte	0x8
	.byte	0xa2
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF93
	.byte	0x8
	.byte	0xa8
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x12
	.4byte	.LASF94
	.byte	0x8
	.byte	0xaa
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x12
	.4byte	.LASF95
	.byte	0x8
	.byte	0xac
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x12
	.4byte	.LASF96
	.byte	0x8
	.byte	0xae
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF97
	.byte	0x8
	.byte	0xb0
	.4byte	0x714
	.uleb128 0xc
	.byte	0x18
	.byte	0x8
	.byte	0xbe
	.4byte	0x7cb
	.uleb128 0x12
	.4byte	.LASF86
	.byte	0x8
	.byte	0xc0
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF98
	.byte	0x8
	.byte	0xc4
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x12
	.4byte	.LASF93
	.byte	0x8
	.byte	0xc8
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x12
	.4byte	.LASF94
	.byte	0x8
	.byte	0xca
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x12
	.4byte	.LASF99
	.byte	0x8
	.byte	0xcc
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x12
	.4byte	.LASF100
	.byte	0x8
	.byte	0xd0
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x3
	.4byte	.LASF101
	.byte	0x8
	.byte	0xd2
	.4byte	0x76e
	.uleb128 0xc
	.byte	0x14
	.byte	0x8
	.byte	0xd8
	.4byte	0x825
	.uleb128 0x12
	.4byte	.LASF86
	.byte	0x8
	.byte	0xda
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF102
	.byte	0x8
	.byte	0xde
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x12
	.4byte	.LASF103
	.byte	0x8
	.byte	0xe0
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x12
	.4byte	.LASF104
	.byte	0x8
	.byte	0xe4
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x12
	.4byte	.LASF105
	.byte	0x8
	.byte	0xe8
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF106
	.byte	0x8
	.byte	0xea
	.4byte	0x7d6
	.uleb128 0xc
	.byte	0xf0
	.byte	0x9
	.byte	0x21
	.4byte	0x913
	.uleb128 0x12
	.4byte	.LASF107
	.byte	0x9
	.byte	0x27
	.4byte	0x763
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF108
	.byte	0x9
	.byte	0x2e
	.4byte	0x7cb
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x12
	.4byte	.LASF109
	.byte	0x9
	.byte	0x32
	.4byte	0x6ad
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x12
	.4byte	.LASF110
	.byte	0x9
	.byte	0x3d
	.4byte	0x6e1
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x12
	.4byte	.LASF111
	.byte	0x9
	.byte	0x43
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x12
	.4byte	.LASF112
	.byte	0x9
	.byte	0x45
	.4byte	0x65b
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x12
	.4byte	.LASF113
	.byte	0x9
	.byte	0x50
	.4byte	0x704
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x12
	.4byte	.LASF114
	.byte	0x9
	.byte	0x58
	.4byte	0x704
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x12
	.4byte	.LASF115
	.byte	0x9
	.byte	0x60
	.4byte	0x913
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x12
	.4byte	.LASF116
	.byte	0x9
	.byte	0x67
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0x12
	.4byte	.LASF117
	.byte	0x9
	.byte	0x6c
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x12
	.4byte	.LASF118
	.byte	0x9
	.byte	0x72
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x12
	.4byte	.LASF119
	.byte	0x9
	.byte	0x76
	.4byte	0x825
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0x12
	.4byte	.LASF120
	.byte	0x9
	.byte	0x7c
	.4byte	0x81
	.byte	0x3
	.byte	0x23
	.uleb128 0xe8
	.uleb128 0x12
	.4byte	.LASF121
	.byte	0x9
	.byte	0x7e
	.4byte	0x5a
	.byte	0x3
	.byte	0x23
	.uleb128 0xec
	.byte	0
	.uleb128 0x9
	.4byte	0x41
	.4byte	0x923
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x3
	.4byte	.LASF122
	.byte	0x9
	.byte	0x80
	.4byte	0x830
	.uleb128 0xc
	.byte	0x4c
	.byte	0xa
	.byte	0x28
	.4byte	0xa41
	.uleb128 0x12
	.4byte	.LASF123
	.byte	0xa
	.byte	0x2a
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF124
	.byte	0xa
	.byte	0x2b
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x12
	.4byte	.LASF125
	.byte	0xa
	.byte	0x2c
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x12
	.4byte	.LASF126
	.byte	0xa
	.byte	0x2d
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x12
	.4byte	.LASF127
	.byte	0xa
	.byte	0x2e
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x12
	.4byte	.LASF128
	.byte	0xa
	.byte	0x2f
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x12
	.4byte	.LASF129
	.byte	0xa
	.byte	0x30
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x12
	.4byte	.LASF130
	.byte	0xa
	.byte	0x31
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x12
	.4byte	.LASF131
	.byte	0xa
	.byte	0x32
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x12
	.4byte	.LASF132
	.byte	0xa
	.byte	0x33
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x12
	.4byte	.LASF133
	.byte	0xa
	.byte	0x34
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x12
	.4byte	.LASF134
	.byte	0xa
	.byte	0x35
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x12
	.4byte	.LASF135
	.byte	0xa
	.byte	0x36
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x12
	.4byte	.LASF136
	.byte	0xa
	.byte	0x37
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x12
	.4byte	.LASF137
	.byte	0xa
	.byte	0x38
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x12
	.4byte	.LASF138
	.byte	0xa
	.byte	0x39
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x12
	.4byte	.LASF139
	.byte	0xa
	.byte	0x3a
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x12
	.4byte	.LASF140
	.byte	0xa
	.byte	0x3b
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x12
	.4byte	.LASF141
	.byte	0xa
	.byte	0x3c
	.4byte	0xe3
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.byte	0
	.uleb128 0x3
	.4byte	.LASF142
	.byte	0xa
	.byte	0x3d
	.4byte	0x92e
	.uleb128 0x1c
	.byte	0x4
	.byte	0xb
	.byte	0x24
	.4byte	0xb2d
	.uleb128 0x1d
	.4byte	.LASF143
	.sleb128 0
	.uleb128 0x1d
	.4byte	.LASF144
	.sleb128 0
	.uleb128 0x1d
	.4byte	.LASF145
	.sleb128 1
	.uleb128 0x1d
	.4byte	.LASF146
	.sleb128 2
	.uleb128 0x1d
	.4byte	.LASF147
	.sleb128 3
	.uleb128 0x1d
	.4byte	.LASF148
	.sleb128 4
	.uleb128 0x1d
	.4byte	.LASF149
	.sleb128 5
	.uleb128 0x1d
	.4byte	.LASF150
	.sleb128 6
	.uleb128 0x1d
	.4byte	.LASF151
	.sleb128 7
	.uleb128 0x1d
	.4byte	.LASF152
	.sleb128 8
	.uleb128 0x1d
	.4byte	.LASF153
	.sleb128 9
	.uleb128 0x1d
	.4byte	.LASF154
	.sleb128 10
	.uleb128 0x1d
	.4byte	.LASF155
	.sleb128 11
	.uleb128 0x1d
	.4byte	.LASF156
	.sleb128 12
	.uleb128 0x1d
	.4byte	.LASF157
	.sleb128 13
	.uleb128 0x1d
	.4byte	.LASF158
	.sleb128 14
	.uleb128 0x1d
	.4byte	.LASF159
	.sleb128 15
	.uleb128 0x1d
	.4byte	.LASF160
	.sleb128 16
	.uleb128 0x1d
	.4byte	.LASF161
	.sleb128 17
	.uleb128 0x1d
	.4byte	.LASF162
	.sleb128 18
	.uleb128 0x1d
	.4byte	.LASF163
	.sleb128 19
	.uleb128 0x1d
	.4byte	.LASF164
	.sleb128 20
	.uleb128 0x1d
	.4byte	.LASF165
	.sleb128 21
	.uleb128 0x1d
	.4byte	.LASF166
	.sleb128 22
	.uleb128 0x1d
	.4byte	.LASF167
	.sleb128 23
	.uleb128 0x1d
	.4byte	.LASF168
	.sleb128 24
	.uleb128 0x1d
	.4byte	.LASF169
	.sleb128 25
	.uleb128 0x1d
	.4byte	.LASF170
	.sleb128 26
	.uleb128 0x1d
	.4byte	.LASF171
	.sleb128 27
	.uleb128 0x1d
	.4byte	.LASF172
	.sleb128 28
	.uleb128 0x1d
	.4byte	.LASF173
	.sleb128 29
	.uleb128 0x1d
	.4byte	.LASF174
	.sleb128 30
	.uleb128 0x1d
	.4byte	.LASF175
	.sleb128 31
	.uleb128 0x1d
	.4byte	.LASF176
	.sleb128 32
	.uleb128 0x1d
	.4byte	.LASF177
	.sleb128 33
	.uleb128 0x1d
	.4byte	.LASF178
	.sleb128 34
	.byte	0
	.uleb128 0x1c
	.byte	0x4
	.byte	0xc
	.byte	0x1d
	.4byte	0xb5a
	.uleb128 0x1d
	.4byte	.LASF179
	.sleb128 0
	.uleb128 0x1d
	.4byte	.LASF180
	.sleb128 1
	.uleb128 0x1d
	.4byte	.LASF181
	.sleb128 2
	.uleb128 0x1d
	.4byte	.LASF182
	.sleb128 3
	.uleb128 0x1d
	.4byte	.LASF183
	.sleb128 4
	.uleb128 0x1d
	.4byte	.LASF184
	.sleb128 5
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF185
	.byte	0x1
	.2byte	0x1a4
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0xb81
	.uleb128 0x1f
	.ascii	"p\000"
	.byte	0x1
	.2byte	0x1ad
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF186
	.byte	0x1
	.2byte	0x229
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0xbaa
	.uleb128 0x20
	.4byte	.LASF194
	.byte	0x1
	.2byte	0x229
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x21
	.byte	0x1
	.4byte	.LASF187
	.byte	0x1
	.2byte	0x28b
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x21
	.byte	0x1
	.4byte	.LASF188
	.byte	0x1
	.2byte	0x29f
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF189
	.byte	0x1
	.2byte	0x2ac
	.byte	0x1
	.4byte	0x81
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0xc03
	.uleb128 0x1f
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x2b9
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF190
	.byte	0x1
	.2byte	0x2c9
	.byte	0x1
	.4byte	0x81
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0xc30
	.uleb128 0x1f
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x2cb
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF191
	.byte	0x1
	.2byte	0x2e1
	.byte	0x1
	.4byte	0x81
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0xc5d
	.uleb128 0x1f
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x2e9
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF192
	.byte	0x1
	.2byte	0x300
	.byte	0x1
	.4byte	0x81
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0xc8a
	.uleb128 0x1f
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x302
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x22
	.byte	0x1
	.4byte	.LASF193
	.byte	0x1
	.2byte	0x315
	.byte	0x1
	.4byte	0x81
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0xcd5
	.uleb128 0x20
	.4byte	.LASF195
	.byte	0x1
	.2byte	0x315
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1f
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x317
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x23
	.4byte	.LASF196
	.byte	0x1
	.2byte	0x319
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF201
	.byte	0x1
	.2byte	0x33a
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0xcff
	.uleb128 0x23
	.4byte	.LASF197
	.byte	0x1
	.2byte	0x33c
	.4byte	0xcff
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0xa41
	.uleb128 0x25
	.4byte	.LASF226
	.byte	0x1
	.2byte	0x364
	.byte	0x1
	.4byte	0x5a
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0xd5f
	.uleb128 0x20
	.4byte	.LASF198
	.byte	0x1
	.2byte	0x364
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x23
	.4byte	.LASF197
	.byte	0x1
	.2byte	0x36a
	.4byte	0xcff
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x23
	.4byte	.LASF199
	.byte	0x1
	.2byte	0x36c
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x23
	.4byte	.LASF200
	.byte	0x1
	.2byte	0x36e
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF202
	.byte	0x1
	.2byte	0x3ae
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0xe14
	.uleb128 0x20
	.4byte	.LASF195
	.byte	0x1
	.2byte	0x3ae
	.4byte	0x5a
	.byte	0x3
	.byte	0x91
	.sleb128 -108
	.uleb128 0x23
	.4byte	.LASF203
	.byte	0x1
	.2byte	0x3b6
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x23
	.4byte	.LASF204
	.byte	0x1
	.2byte	0x3b6
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x23
	.4byte	.LASF205
	.byte	0x1
	.2byte	0x3b6
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x23
	.4byte	.LASF206
	.byte	0x1
	.2byte	0x3b8
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x23
	.4byte	.LASF207
	.byte	0x1
	.2byte	0x3ba
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x23
	.4byte	.LASF208
	.byte	0x1
	.2byte	0x3bc
	.4byte	0x6ed
	.byte	0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x23
	.4byte	.LASF209
	.byte	0x1
	.2byte	0x3bc
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -25
	.uleb128 0x23
	.4byte	.LASF210
	.byte	0x1
	.2byte	0x3be
	.4byte	0xe14
	.byte	0x3
	.byte	0x91
	.sleb128 -100
	.uleb128 0x23
	.4byte	.LASF211
	.byte	0x1
	.2byte	0x3c0
	.4byte	0xe14
	.byte	0x3
	.byte	0x91
	.sleb128 -104
	.byte	0
	.uleb128 0xb
	.4byte	0xe19
	.uleb128 0x1b
	.4byte	0x5e0
	.uleb128 0x26
	.4byte	.LASF212
	.byte	0xd
	.byte	0x30
	.4byte	0xe2f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x1b
	.4byte	0xd3
	.uleb128 0x26
	.4byte	.LASF213
	.byte	0xd
	.byte	0x34
	.4byte	0xe45
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x1b
	.4byte	0xd3
	.uleb128 0x26
	.4byte	.LASF214
	.byte	0xd
	.byte	0x36
	.4byte	0xe5b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x1b
	.4byte	0xd3
	.uleb128 0x26
	.4byte	.LASF215
	.byte	0xd
	.byte	0x38
	.4byte	0xe71
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x1b
	.4byte	0xd3
	.uleb128 0x27
	.4byte	.LASF216
	.byte	0x6
	.2byte	0x1d9
	.4byte	0x5c9
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x5d5
	.4byte	0xe94
	.uleb128 0xa
	.4byte	0x25
	.byte	0xa
	.byte	0
	.uleb128 0x27
	.4byte	.LASF217
	.byte	0x6
	.2byte	0x1de
	.4byte	0xea2
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	0xe84
	.uleb128 0x9
	.4byte	0x3c0
	.4byte	0xeb7
	.uleb128 0xa
	.4byte	0x25
	.byte	0xa
	.byte	0
	.uleb128 0x27
	.4byte	.LASF218
	.byte	0x6
	.2byte	0x1e0
	.4byte	0xec5
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	0xea7
	.uleb128 0x28
	.4byte	.LASF219
	.byte	0xe
	.byte	0xaa
	.4byte	0xc8
	.byte	0x1
	.byte	0x1
	.uleb128 0x26
	.4byte	.LASF220
	.byte	0xf
	.byte	0x33
	.4byte	0xee8
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x1b
	.4byte	0xe8
	.uleb128 0x26
	.4byte	.LASF221
	.byte	0xf
	.byte	0x3f
	.4byte	0xefe
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x1b
	.4byte	0x5e7
	.uleb128 0x28
	.4byte	.LASF222
	.byte	0x9
	.byte	0x83
	.4byte	0x923
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0xf20
	.uleb128 0xa
	.4byte	0x25
	.byte	0x18
	.byte	0
	.uleb128 0x23
	.4byte	.LASF223
	.byte	0x1
	.2byte	0x196
	.4byte	0xf32
	.byte	0x5
	.byte	0x3
	.4byte	CONTROLLER_CONFIG_FILENAME
	.uleb128 0x1b
	.4byte	0xf10
	.uleb128 0x29
	.4byte	.LASF216
	.byte	0x1
	.2byte	0x1a0
	.4byte	0x5c9
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	config_c
	.uleb128 0x2a
	.4byte	.LASF217
	.byte	0x1
	.byte	0x47
	.4byte	0xf5c
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	comm_device_names
	.uleb128 0x1b
	.4byte	0xe84
	.uleb128 0x2a
	.4byte	.LASF218
	.byte	0x1
	.byte	0x49
	.4byte	0xf73
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	port_device_table
	.uleb128 0x1b
	.4byte	0xea7
	.uleb128 0x28
	.4byte	.LASF219
	.byte	0xe
	.byte	0xaa
	.4byte	0xc8
	.byte	0x1
	.byte	0x1
	.uleb128 0x28
	.4byte	.LASF222
	.byte	0x9
	.byte	0x83
	.4byte	0x923
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF60:
	.ascii	"nlu_controller_name\000"
.LASF199:
	.ascii	"count\000"
.LASF36:
	.ascii	"size_of_the_union\000"
.LASF113:
	.ascii	"two_wire_cable_excessive_current\000"
.LASF225:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/conf"
	.ascii	"iguration/configuration_controller.c\000"
.LASF65:
	.ascii	"port_B_device_index\000"
.LASF192:
	.ascii	"CONFIG_this_controller_is_behind_a_hub\000"
.LASF184:
	.ascii	"ISR_TRIGGER_DUAL_EDGE\000"
.LASF219:
	.ascii	"irri_comm_recursive_MUTEX\000"
.LASF194:
	.ascii	"pfrom_revision\000"
.LASF186:
	.ascii	"controller_config_updater\000"
.LASF3:
	.ascii	"signed char\000"
.LASF160:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF214:
	.ascii	"GuiFont_DecimalChar\000"
.LASF188:
	.ascii	"save_file_configuration_controller\000"
.LASF81:
	.ascii	"stop_for_the_highest_reason_in_all_systems\000"
.LASF85:
	.ascii	"STOP_KEY_RECORD_s\000"
.LASF77:
	.ascii	"CONTROLLER_CONFIGURATION_STRUCT\000"
.LASF164:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF48:
	.ascii	"__connection_processing\000"
.LASF120:
	.ascii	"walk_thru_need_to_send_gid_to_master\000"
.LASF44:
	.ascii	"dtr_level_to_connect\000"
.LASF209:
	.ascii	"port_char\000"
.LASF202:
	.ascii	"CONFIG_device_discovery\000"
.LASF145:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF39:
	.ascii	"baud_rate\000"
.LASF30:
	.ascii	"unused_15\000"
.LASF23:
	.ascii	"port_a_freewave_sr_set_for_repeater\000"
.LASF118:
	.ascii	"send_crc_list\000"
.LASF176:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF88:
	.ascii	"stop_datetime_d\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF140:
	.ascii	"tsc_aux_value\000"
.LASF150:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF10:
	.ascii	"long long int\000"
.LASF87:
	.ascii	"light_index_0_47\000"
.LASF121:
	.ascii	"walk_thru_gid_to_send\000"
.LASF143:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF89:
	.ascii	"stop_datetime_t\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF40:
	.ascii	"cts_when_OK_to_send\000"
.LASF211:
	.ascii	"THIRTY_TWO_POINT_O\000"
.LASF27:
	.ascii	"option_AQUAPONICS\000"
.LASF64:
	.ascii	"port_A_device_index\000"
.LASF222:
	.ascii	"irri_comm\000"
.LASF101:
	.ascii	"MANUAL_WATER_KICK_OFF_STRUCT\000"
.LASF13:
	.ascii	"long int\000"
.LASF169:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF206:
	.ascii	"conversion_index\000"
.LASF156:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF71:
	.ascii	"OM_Originator_Retries\000"
.LASF53:
	.ascii	"nlu_bit_0\000"
.LASF54:
	.ascii	"nlu_bit_1\000"
.LASF55:
	.ascii	"nlu_bit_2\000"
.LASF56:
	.ascii	"nlu_bit_3\000"
.LASF57:
	.ascii	"nlu_bit_4\000"
.LASF14:
	.ascii	"portTickType\000"
.LASF58:
	.ascii	"alert_about_crc_errors\000"
.LASF190:
	.ascii	"CONFIG_is_connected\000"
.LASF95:
	.ascii	"time_seconds\000"
.LASF215:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF17:
	.ascii	"option_FL\000"
.LASF67:
	.ascii	"comm_server_port\000"
.LASF102:
	.ascii	"mvor_action_to_take\000"
.LASF32:
	.ascii	"transport_om_show_unexpected_behavior_alerts\000"
.LASF91:
	.ascii	"LIGHTS_OFF_XFER_RECORD\000"
.LASF70:
	.ascii	"dummy\000"
.LASF139:
	.ascii	"reserved\000"
.LASF76:
	.ascii	"hub_enabled_user_setting\000"
.LASF216:
	.ascii	"config_c\000"
.LASF148:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF98:
	.ascii	"manual_how\000"
.LASF162:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF19:
	.ascii	"option_SSE_D\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF153:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF171:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF107:
	.ascii	"test_request\000"
.LASF130:
	.ascii	"tsc_ttr\000"
.LASF217:
	.ascii	"comm_device_names\000"
.LASF110:
	.ascii	"light_off_request\000"
.LASF49:
	.ascii	"__is_connected\000"
.LASF131:
	.ascii	"tsc_dxp\000"
.LASF34:
	.ascii	"use_new_k_and_offset_numbers\000"
.LASF200:
	.ascii	"results\000"
.LASF94:
	.ascii	"station_number\000"
.LASF63:
	.ascii	"port_settings\000"
.LASF218:
	.ascii	"port_device_table\000"
.LASF99:
	.ascii	"manual_seconds\000"
.LASF79:
	.ascii	"stop_for_this_reason\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF224:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF137:
	.ascii	"tsc_aux_min\000"
.LASF197:
	.ascii	"ladc\000"
.LASF29:
	.ascii	"unused_14\000"
.LASF179:
	.ascii	"ISR_TRIGGER_FIXED\000"
.LASF182:
	.ascii	"ISR_TRIGGER_NEGATIVE_EDGE\000"
.LASF135:
	.ascii	"tsc_max_y\000"
.LASF61:
	.ascii	"serial_number\000"
.LASF75:
	.ascii	"last_assigned_decoder_serial_number\000"
.LASF178:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF100:
	.ascii	"program_GID\000"
.LASF191:
	.ascii	"CONFIG_this_controller_is_a_configured_hub\000"
.LASF158:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF144:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF15:
	.ascii	"xQueueHandle\000"
.LASF173:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF210:
	.ascii	"ONE_THOUSAND_TWENTY_FOUR_POINT_O\000"
.LASF35:
	.ascii	"show_flow_table_interaction\000"
.LASF154:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF33:
	.ascii	"transport_om_show_status_timer_expired_alert\000"
.LASF226:
	.ascii	"AD_conversion_and_return_results\000"
.LASF146:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF174:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF41:
	.ascii	"cd_when_connected\000"
.LASF189:
	.ascii	"CONFIG_this_controller_originates_commserver_messag"
	.ascii	"es\000"
.LASF74:
	.ascii	"test_seconds\000"
.LASF68:
	.ascii	"nlu_prevent_automated_CI_transmissions\000"
.LASF116:
	.ascii	"clear_runaway_gage_pressed\000"
.LASF22:
	.ascii	"port_a_freewave_lr_set_for_repeater\000"
.LASF193:
	.ascii	"CONFIG_the_device_on_this_port_is_used_for_controll"
	.ascii	"er_to_controller_packets\000"
.LASF47:
	.ascii	"__initialize_the_connection_process\000"
.LASF212:
	.ascii	"GuiFont_LanguageActive\000"
.LASF134:
	.ascii	"tsc_min_y\000"
.LASF21:
	.ascii	"port_a_raveon_radio_type\000"
.LASF108:
	.ascii	"manual_water_request\000"
.LASF26:
	.ascii	"port_b_freewave_sr_set_for_repeater\000"
.LASF166:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF183:
	.ascii	"ISR_TRIGGER_POSITIVE_EDGE\000"
.LASF69:
	.ascii	"debug\000"
.LASF124:
	.ascii	"adc_sel\000"
.LASF109:
	.ascii	"light_on_request\000"
.LASF207:
	.ascii	"save_file\000"
.LASF136:
	.ascii	"tsc_aux_utr\000"
.LASF172:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF165:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF196:
	.ascii	"device_index\000"
.LASF16:
	.ascii	"xSemaphoreHandle\000"
.LASF185:
	.ascii	"init_config_controller\000"
.LASF62:
	.ascii	"purchased_options\000"
.LASF180:
	.ascii	"ISR_TRIGGER_LOW_LEVEL\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF205:
	.ascii	"tries\000"
.LASF51:
	.ascii	"__device_exchange_processing\000"
.LASF90:
	.ascii	"LIGHTS_ON_XFER_RECORD\000"
.LASF83:
	.ascii	"stop_for_all_reasons\000"
.LASF105:
	.ascii	"system_gid\000"
.LASF5:
	.ascii	"short int\000"
.LASF198:
	.ascii	"pchannel\000"
.LASF195:
	.ascii	"pport\000"
.LASF149:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF201:
	.ascii	"init_a_to_d\000"
.LASF187:
	.ascii	"init_file_configuration_controller\000"
.LASF163:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF46:
	.ascii	"__power_device_ptr\000"
.LASF106:
	.ascii	"MVOR_KICK_OFF_STRUCT\000"
.LASF43:
	.ascii	"rts_level_to_cause_device_to_send\000"
.LASF127:
	.ascii	"tsc_dtr\000"
.LASF181:
	.ascii	"ISR_TRIGGER_HIGH_LEVEL\000"
.LASF123:
	.ascii	"tsc_stat\000"
.LASF170:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF86:
	.ascii	"in_use\000"
.LASF115:
	.ascii	"system_gids_to_clear_mlbs_for\000"
.LASF213:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF128:
	.ascii	"tsc_rtr\000"
.LASF111:
	.ascii	"send_stop_key_record\000"
.LASF28:
	.ascii	"unused_13\000"
.LASF18:
	.ascii	"option_SSE\000"
.LASF78:
	.ascii	"float\000"
.LASF129:
	.ascii	"tsc_utr\000"
.LASF1:
	.ascii	"char\000"
.LASF142:
	.ascii	"LPC3250_ADCTSC_REGS_T\000"
.LASF42:
	.ascii	"ri_polarity\000"
.LASF114:
	.ascii	"two_wire_cable_overheated\000"
.LASF126:
	.ascii	"tsc_fifo\000"
.LASF141:
	.ascii	"adc_dat\000"
.LASF119:
	.ascii	"mvor_request\000"
.LASF151:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF82:
	.ascii	"stop_in_all_systems\000"
.LASF125:
	.ascii	"adc_con\000"
.LASF159:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF52:
	.ascii	"PORT_DEVICE_SETTINGS_STRUCT\000"
.LASF208:
	.ascii	"lstr_64\000"
.LASF25:
	.ascii	"port_b_freewave_lr_set_for_repeater\000"
.LASF24:
	.ascii	"port_b_raveon_radio_type\000"
.LASF96:
	.ascii	"set_expected\000"
.LASF138:
	.ascii	"tsc_aux_max\000"
.LASF31:
	.ascii	"PURCHASED_OPTIONS_STRUCT\000"
.LASF177:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF50:
	.ascii	"__initialize_device_exchange\000"
.LASF45:
	.ascii	"reset_active_level\000"
.LASF20:
	.ascii	"option_HUB\000"
.LASF221:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF155:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF133:
	.ascii	"tsc_max_x\000"
.LASF204:
	.ascii	"conversion_value_2\000"
.LASF6:
	.ascii	"UNS_16\000"
.LASF132:
	.ascii	"tsc_min_x\000"
.LASF112:
	.ascii	"stop_key_record\000"
.LASF147:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF72:
	.ascii	"OM_Seconds_for_Status_FOAL\000"
.LASF203:
	.ascii	"conversion_value_1\000"
.LASF122:
	.ascii	"IRRI_COMM\000"
.LASF157:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF73:
	.ascii	"OM_Minutes_To_Exist\000"
.LASF168:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF97:
	.ascii	"TEST_AND_MOBILE_KICK_OFF_STRUCT\000"
.LASF223:
	.ascii	"CONTROLLER_CONFIG_FILENAME\000"
.LASF37:
	.ascii	"DEBUG_BITS_STRUCT\000"
.LASF93:
	.ascii	"box_index_0\000"
.LASF38:
	.ascii	"on_port_A_enables_controller_to_make_CI_messages\000"
.LASF167:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF84:
	.ascii	"stations_removed_for_this_reason\000"
.LASF175:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF66:
	.ascii	"comm_server_ip_address\000"
.LASF12:
	.ascii	"BITFIELD_BOOL\000"
.LASF220:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF117:
	.ascii	"send_box_configuration_to_master\000"
.LASF59:
	.ascii	"CONFIGURATION_PORT_CONTROL_STRUCT\000"
.LASF103:
	.ascii	"mvor_seconds\000"
.LASF92:
	.ascii	"double\000"
.LASF152:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF161:
	.ascii	"CLKPWR_WDOG_CLK\000"
.LASF80:
	.ascii	"stop_in_this_system_gid\000"
.LASF104:
	.ascii	"initiated_by\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
