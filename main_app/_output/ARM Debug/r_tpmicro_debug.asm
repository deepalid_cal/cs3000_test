	.file	"r_tpmicro_debug.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section .rodata
	.align	2
.LC0:
	.ascii	"~~~~####~~~~####~~~~####~~~~####\000"
	.section	.text.FDTO_TPMICRO_draw_debug,"ax",%progbits
	.align	2
	.global	FDTO_TPMICRO_draw_debug
	.type	FDTO_TPMICRO_draw_debug, %function
FDTO_TPMICRO_draw_debug:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_tpmicro_debug.c"
	.loc 1 41 0
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #116
.LCFI2:
	str	r0, [fp, #-76]
	.loc 1 44 0
	ldr	r3, [fp, #-76]
	cmp	r3, #1
	bne	.L2
	.loc 1 46 0
	mov	r0, #104
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
.L2:
	.loc 1 49 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L3
.L4:
	.loc 1 51 0 discriminator 2
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #1
	add	r3, r2, r3
	mov	r3, r3, asl #2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r3, #27
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	.loc 1 52 0 discriminator 2
	ldr	r2, [fp, #-8]
	mov	r1, r2, asl #6
	.loc 1 51 0 discriminator 2
	ldr	r2, .L5
	add	r2, r1, r2
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r2, #1
	str	r2, [sp, #8]
	mov	r2, #0
	str	r2, [sp, #12]
	mov	r2, #0
	str	r2, [sp, #16]
	mov	r2, #240
	str	r2, [sp, #20]
	mov	r2, #0
	str	r2, [sp, #24]
	mov	r2, #0
	str	r2, [sp, #28]
	mov	r2, #0
	str	r2, [sp, #32]
	mov	r2, #0
	str	r2, [sp, #36]
	mov	r2, #15
	str	r2, [sp, #40]
	mov	r0, #62
	mov	r1, r3
	mov	r2, #1
	mvn	r3, #0
	bl	GuiLib_DrawStr
	.loc 1 49 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L3:
	.loc 1 49 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #13
	bls	.L4
	.loc 1 61 0 is_stmt 1
	ldr	r3, .L5+4
	str	r3, [sp, #0]
	mov	r3, #1
	str	r3, [sp, #4]
	mov	r3, #1
	str	r3, [sp, #8]
	mov	r3, #0
	str	r3, [sp, #12]
	mov	r3, #0
	str	r3, [sp, #16]
	mov	r3, #240
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
	mov	r3, #0
	str	r3, [sp, #28]
	mov	r3, #0
	str	r3, [sp, #32]
	mov	r3, #0
	str	r3, [sp, #36]
	mov	r3, #15
	str	r3, [sp, #40]
	mov	r0, #62
	mov	r1, #207
	mov	r2, #1
	mvn	r3, #0
	bl	GuiLib_DrawStr
	.loc 1 69 0
	sub	r3, fp, #72
	mov	r0, r3
	ldr	r1, .L5+8
	bl	format_binary_32
	.loc 1 72 0
	sub	r3, fp, #72
	.loc 1 71 0
	str	r3, [sp, #0]
	mov	r3, #1
	str	r3, [sp, #4]
	mov	r3, #1
	str	r3, [sp, #8]
	mov	r3, #0
	str	r3, [sp, #12]
	mov	r3, #0
	str	r3, [sp, #16]
	mov	r3, #240
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
	mov	r3, #0
	str	r3, [sp, #28]
	mov	r3, #0
	str	r3, [sp, #32]
	mov	r3, #0
	str	r3, [sp, #36]
	mov	r3, #15
	str	r3, [sp, #40]
	mov	r0, #62
	mov	r1, #219
	mov	r2, #1
	mvn	r3, #0
	bl	GuiLib_DrawStr
	.loc 1 79 0
	bl	GuiLib_Refresh
	.loc 1 80 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	TPMICRO_debug_text
	.word	.LC0
	.word	tpmicro_data+8
.LFE0:
	.size	FDTO_TPMICRO_draw_debug, .-FDTO_TPMICRO_draw_debug
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/stdint.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/../decoder/_common_decoder_files/shared_to_cs3000/data_types.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/../decoder/_common_decoder_files/shared_to_cs3000/eeprom.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/../decoder/_common_decoder_files/shared_to_cs3000/protocolmsgs.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_tpmicro_common.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/tp_board/tpmicro_data.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/tpmicro_comm.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x91d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF135
	.byte	0x1
	.4byte	.LASF136
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x62
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x74
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x74
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x11
	.4byte	0x37
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x12
	.4byte	0x50
	.uleb128 0x5
	.4byte	0x37
	.4byte	0xcf
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.ascii	"U16\000"
	.byte	0x4
	.byte	0xb
	.4byte	0xb4
	.uleb128 0x7
	.ascii	"U8\000"
	.byte	0x4
	.byte	0xc
	.4byte	0xa9
	.uleb128 0x8
	.byte	0x1d
	.byte	0x5
	.byte	0x9b
	.4byte	0x267
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x5
	.byte	0x9d
	.4byte	0xcf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x5
	.byte	0x9e
	.4byte	0xcf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x5
	.byte	0x9f
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x5
	.byte	0xa0
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x5
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x5
	.byte	0xa1
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x5
	.byte	0xa2
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x7
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x5
	.byte	0xa3
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x5
	.byte	0xa4
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x9
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x5
	.byte	0xa5
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x5
	.byte	0xa6
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0xb
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x5
	.byte	0xa7
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x5
	.byte	0xa8
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x5
	.byte	0xa9
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x5
	.byte	0xaa
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0xf
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x5
	.byte	0xab
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x5
	.byte	0xac
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.uleb128 0x9
	.4byte	.LASF33
	.byte	0x5
	.byte	0xad
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0x9
	.4byte	.LASF34
	.byte	0x5
	.byte	0xae
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.uleb128 0x9
	.4byte	.LASF35
	.byte	0x5
	.byte	0xaf
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x5
	.byte	0xb0
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x15
	.uleb128 0x9
	.4byte	.LASF37
	.byte	0x5
	.byte	0xb1
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x16
	.uleb128 0x9
	.4byte	.LASF38
	.byte	0x5
	.byte	0xb2
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x17
	.uleb128 0x9
	.4byte	.LASF39
	.byte	0x5
	.byte	0xb3
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x9
	.4byte	.LASF40
	.byte	0x5
	.byte	0xb4
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x19
	.uleb128 0x9
	.4byte	.LASF41
	.byte	0x5
	.byte	0xb5
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x1a
	.uleb128 0x9
	.4byte	.LASF42
	.byte	0x5
	.byte	0xb6
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x1b
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x5
	.byte	0xb7
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x3
	.4byte	.LASF44
	.byte	0x5
	.byte	0xb9
	.4byte	0xe4
	.uleb128 0xa
	.byte	0x4
	.byte	0x6
	.2byte	0x16b
	.4byte	0x2a9
	.uleb128 0xb
	.4byte	.LASF45
	.byte	0x6
	.2byte	0x16d
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF46
	.byte	0x6
	.2byte	0x16e
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0xb
	.4byte	.LASF47
	.byte	0x6
	.2byte	0x16f
	.4byte	0xcf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0xc
	.4byte	.LASF48
	.byte	0x6
	.2byte	0x171
	.4byte	0x272
	.uleb128 0xa
	.byte	0xb
	.byte	0x6
	.2byte	0x193
	.4byte	0x30a
	.uleb128 0xb
	.4byte	.LASF49
	.byte	0x6
	.2byte	0x195
	.4byte	0x2a9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF50
	.byte	0x6
	.2byte	0x196
	.4byte	0x2a9
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF51
	.byte	0x6
	.2byte	0x197
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF52
	.byte	0x6
	.2byte	0x198
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x9
	.uleb128 0xb
	.4byte	.LASF53
	.byte	0x6
	.2byte	0x199
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.byte	0
	.uleb128 0xc
	.4byte	.LASF54
	.byte	0x6
	.2byte	0x19b
	.4byte	0x2b5
	.uleb128 0xa
	.byte	0x4
	.byte	0x6
	.2byte	0x221
	.4byte	0x34d
	.uleb128 0xb
	.4byte	.LASF55
	.byte	0x6
	.2byte	0x223
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF56
	.byte	0x6
	.2byte	0x225
	.4byte	0xda
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0xb
	.4byte	.LASF57
	.byte	0x6
	.2byte	0x227
	.4byte	0xcf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0xc
	.4byte	.LASF58
	.byte	0x6
	.2byte	0x229
	.4byte	0x316
	.uleb128 0x8
	.byte	0xc
	.byte	0x7
	.byte	0x25
	.4byte	0x38a
	.uleb128 0xd
	.ascii	"sn\000"
	.byte	0x7
	.byte	0x28
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF59
	.byte	0x7
	.byte	0x2b
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.ascii	"on\000"
	.byte	0x7
	.byte	0x2e
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF60
	.byte	0x7
	.byte	0x30
	.4byte	0x359
	.uleb128 0xa
	.byte	0x4
	.byte	0x7
	.2byte	0x193
	.4byte	0x3ae
	.uleb128 0xb
	.4byte	.LASF61
	.byte	0x7
	.2byte	0x196
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xc
	.4byte	.LASF62
	.byte	0x7
	.2byte	0x198
	.4byte	0x395
	.uleb128 0xa
	.byte	0xc
	.byte	0x7
	.2byte	0x1b0
	.4byte	0x3f1
	.uleb128 0xb
	.4byte	.LASF63
	.byte	0x7
	.2byte	0x1b2
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF64
	.byte	0x7
	.2byte	0x1b7
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF65
	.byte	0x7
	.2byte	0x1bc
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xc
	.4byte	.LASF66
	.byte	0x7
	.2byte	0x1be
	.4byte	0x3ba
	.uleb128 0xa
	.byte	0x4
	.byte	0x7
	.2byte	0x1c3
	.4byte	0x416
	.uleb128 0xb
	.4byte	.LASF67
	.byte	0x7
	.2byte	0x1ca
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xc
	.4byte	.LASF68
	.byte	0x7
	.2byte	0x1d0
	.4byte	0x3fd
	.uleb128 0xe
	.4byte	.LASF137
	.byte	0x10
	.byte	0x7
	.2byte	0x1ff
	.4byte	0x46c
	.uleb128 0xb
	.4byte	.LASF69
	.byte	0x7
	.2byte	0x202
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF70
	.byte	0x7
	.2byte	0x205
	.4byte	0x34d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF71
	.byte	0x7
	.2byte	0x207
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF72
	.byte	0x7
	.2byte	0x20c
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0xc
	.4byte	.LASF73
	.byte	0x7
	.2byte	0x211
	.4byte	0x478
	.uleb128 0x5
	.4byte	0x422
	.4byte	0x488
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.byte	0xc
	.byte	0x7
	.2byte	0x3a4
	.4byte	0x4ec
	.uleb128 0xb
	.4byte	.LASF74
	.byte	0x7
	.2byte	0x3a6
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF75
	.byte	0x7
	.2byte	0x3a8
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xb
	.4byte	.LASF76
	.byte	0x7
	.2byte	0x3aa
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF77
	.byte	0x7
	.2byte	0x3ac
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xb
	.4byte	.LASF78
	.byte	0x7
	.2byte	0x3ae
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF79
	.byte	0x7
	.2byte	0x3b0
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.byte	0
	.uleb128 0xc
	.4byte	.LASF80
	.byte	0x7
	.2byte	0x3b2
	.4byte	0x488
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF81
	.uleb128 0x5
	.4byte	0x69
	.4byte	0x50f
	.uleb128 0x6
	.4byte	0x9b
	.byte	0xb
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.byte	0x8
	.byte	0x1d
	.4byte	0x534
	.uleb128 0x9
	.4byte	.LASF82
	.byte	0x8
	.byte	0x20
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF83
	.byte	0x8
	.byte	0x25
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF84
	.byte	0x8
	.byte	0x27
	.4byte	0x50f
	.uleb128 0x8
	.byte	0x8
	.byte	0x8
	.byte	0x29
	.4byte	0x563
	.uleb128 0x9
	.4byte	.LASF85
	.byte	0x8
	.byte	0x2c
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"on\000"
	.byte	0x8
	.byte	0x2f
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF86
	.byte	0x8
	.byte	0x31
	.4byte	0x53f
	.uleb128 0x8
	.byte	0x3c
	.byte	0x8
	.byte	0x3c
	.4byte	0x5bc
	.uleb128 0xd
	.ascii	"sn\000"
	.byte	0x8
	.byte	0x40
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF70
	.byte	0x8
	.byte	0x45
	.4byte	0x34d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF87
	.byte	0x8
	.byte	0x4a
	.4byte	0x267
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF88
	.byte	0x8
	.byte	0x4f
	.4byte	0x30a
	.byte	0x2
	.byte	0x23
	.uleb128 0x25
	.uleb128 0x9
	.4byte	.LASF89
	.byte	0x8
	.byte	0x56
	.4byte	0x4ec
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.byte	0
	.uleb128 0x3
	.4byte	.LASF90
	.byte	0x8
	.byte	0x5a
	.4byte	0x56e
	.uleb128 0xf
	.2byte	0x156c
	.byte	0x8
	.byte	0x82
	.4byte	0x7e7
	.uleb128 0x9
	.4byte	.LASF91
	.byte	0x8
	.byte	0x87
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF92
	.byte	0x8
	.byte	0x8e
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF93
	.byte	0x8
	.byte	0x96
	.4byte	0x3ae
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF94
	.byte	0x8
	.byte	0x9f
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x9
	.4byte	.LASF95
	.byte	0x8
	.byte	0xa6
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x9
	.4byte	.LASF96
	.byte	0x8
	.byte	0xab
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x9
	.4byte	.LASF97
	.byte	0x8
	.byte	0xad
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x9
	.4byte	.LASF98
	.byte	0x8
	.byte	0xaf
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x9
	.4byte	.LASF99
	.byte	0x8
	.byte	0xb4
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x9
	.4byte	.LASF100
	.byte	0x8
	.byte	0xbb
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x9
	.4byte	.LASF101
	.byte	0x8
	.byte	0xbc
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x9
	.4byte	.LASF102
	.byte	0x8
	.byte	0xbd
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x9
	.4byte	.LASF103
	.byte	0x8
	.byte	0xbe
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x9
	.4byte	.LASF104
	.byte	0x8
	.byte	0xc5
	.4byte	0x534
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x9
	.4byte	.LASF105
	.byte	0x8
	.byte	0xca
	.4byte	0x7e7
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x9
	.4byte	.LASF106
	.byte	0x8
	.byte	0xd0
	.4byte	0x4ff
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x9
	.4byte	.LASF107
	.byte	0x8
	.byte	0xda
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x9
	.4byte	.LASF108
	.byte	0x8
	.byte	0xde
	.4byte	0x3f1
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x9
	.4byte	.LASF109
	.byte	0x8
	.byte	0xe2
	.4byte	0x7f7
	.byte	0x3
	.byte	0x23
	.uleb128 0xdc
	.uleb128 0x9
	.4byte	.LASF110
	.byte	0x8
	.byte	0xe4
	.4byte	0x563
	.byte	0x3
	.byte	0x23
	.uleb128 0x139c
	.uleb128 0x9
	.4byte	.LASF111
	.byte	0x8
	.byte	0xea
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13a4
	.uleb128 0x9
	.4byte	.LASF112
	.byte	0x8
	.byte	0xec
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13a8
	.uleb128 0x9
	.4byte	.LASF113
	.byte	0x8
	.byte	0xee
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13ac
	.uleb128 0x9
	.4byte	.LASF114
	.byte	0x8
	.byte	0xf0
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13b0
	.uleb128 0x9
	.4byte	.LASF115
	.byte	0x8
	.byte	0xf2
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13b4
	.uleb128 0x9
	.4byte	.LASF116
	.byte	0x8
	.byte	0xf7
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x13b8
	.uleb128 0x9
	.4byte	.LASF117
	.byte	0x8
	.byte	0xfd
	.4byte	0x416
	.byte	0x3
	.byte	0x23
	.uleb128 0x13bc
	.uleb128 0xb
	.4byte	.LASF118
	.byte	0x8
	.2byte	0x102
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c0
	.uleb128 0xb
	.4byte	.LASF119
	.byte	0x8
	.2byte	0x104
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c4
	.uleb128 0xb
	.4byte	.LASF120
	.byte	0x8
	.2byte	0x106
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c8
	.uleb128 0xb
	.4byte	.LASF121
	.byte	0x8
	.2byte	0x10b
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13cc
	.uleb128 0xb
	.4byte	.LASF122
	.byte	0x8
	.2byte	0x10d
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x13d0
	.uleb128 0xb
	.4byte	.LASF123
	.byte	0x8
	.2byte	0x116
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13d4
	.uleb128 0xb
	.4byte	.LASF124
	.byte	0x8
	.2byte	0x118
	.4byte	0x38a
	.byte	0x3
	.byte	0x23
	.uleb128 0x13d8
	.uleb128 0xb
	.4byte	.LASF125
	.byte	0x8
	.2byte	0x11f
	.4byte	0x46c
	.byte	0x3
	.byte	0x23
	.uleb128 0x13e4
	.uleb128 0xb
	.4byte	.LASF126
	.byte	0x8
	.2byte	0x12a
	.4byte	0x807
	.byte	0x3
	.byte	0x23
	.uleb128 0x1464
	.byte	0
	.uleb128 0x5
	.4byte	0x534
	.4byte	0x7f7
	.uleb128 0x6
	.4byte	0x9b
	.byte	0xb
	.byte	0
	.uleb128 0x5
	.4byte	0x5bc
	.4byte	0x807
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x4f
	.byte	0
	.uleb128 0x5
	.4byte	0x69
	.4byte	0x817
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x41
	.byte	0
	.uleb128 0xc
	.4byte	.LASF127
	.byte	0x8
	.2byte	0x133
	.4byte	0x5c7
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF138
	.byte	0x1
	.byte	0x28
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x867
	.uleb128 0x11
	.4byte	.LASF139
	.byte	0x1
	.byte	0x28
	.4byte	0x867
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0x2a
	.4byte	0x69
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF128
	.byte	0x1
	.byte	0x43
	.4byte	0x86c
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.byte	0
	.uleb128 0x14
	.4byte	0x90
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0x87c
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x3f
	.byte	0
	.uleb128 0x13
	.4byte	.LASF129
	.byte	0x9
	.byte	0x30
	.4byte	0x88d
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x14
	.4byte	0xbf
	.uleb128 0x13
	.4byte	.LASF130
	.byte	0x9
	.byte	0x34
	.4byte	0x8a3
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x14
	.4byte	0xbf
	.uleb128 0x13
	.4byte	.LASF131
	.byte	0x9
	.byte	0x36
	.4byte	0x8b9
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x14
	.4byte	0xbf
	.uleb128 0x13
	.4byte	.LASF132
	.byte	0x9
	.byte	0x38
	.4byte	0x8cf
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x14
	.4byte	0xbf
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0x8ea
	.uleb128 0x6
	.4byte	0x9b
	.byte	0xd
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x3f
	.byte	0
	.uleb128 0x15
	.4byte	.LASF133
	.byte	0xa
	.byte	0xd4
	.4byte	0x8d4
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF134
	.byte	0x8
	.2byte	0x138
	.4byte	0x817
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF133
	.byte	0xa
	.byte	0xd4
	.4byte	0x8d4
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF134
	.byte	0x8
	.2byte	0x138
	.4byte	0x817
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF68:
	.ascii	"TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT\000"
.LASF89:
	.ascii	"comm_stats\000"
.LASF96:
	.ascii	"et_gage_runaway_gage_in_effect_to_send_to_master\000"
.LASF124:
	.ascii	"two_wire_solenoid_location_on_off_command\000"
.LASF82:
	.ascii	"measured_ma_current\000"
.LASF131:
	.ascii	"GuiFont_DecimalChar\000"
.LASF78:
	.ascii	"loop_data_bytes_sent\000"
.LASF31:
	.ascii	"rx_disc_rst_msgs\000"
.LASF42:
	.ascii	"rx_sol_ctl_msgs\000"
.LASF55:
	.ascii	"decoder_type__tpmicro_type\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF110:
	.ascii	"two_wire_cable_power_operation\000"
.LASF99:
	.ascii	"rain_bucket_pulse_count_to_send_to_the_master\000"
.LASF39:
	.ascii	"rx_sol_cur_meas_req_msgs\000"
.LASF23:
	.ascii	"sol_2_ucos\000"
.LASF11:
	.ascii	"long long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF17:
	.ascii	"temp_maximum\000"
.LASF125:
	.ascii	"decoder_faults\000"
.LASF13:
	.ascii	"long unsigned int\000"
.LASF104:
	.ascii	"as_rcvd_from_tp_micro\000"
.LASF88:
	.ascii	"stat2_response\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF100:
	.ascii	"nlu_wind_mph\000"
.LASF33:
	.ascii	"rx_disc_conf_msgs\000"
.LASF94:
	.ascii	"whats_installed_has_arrived_from_the_tpmicro\000"
.LASF26:
	.ascii	"eep_crc_err_sol2_parms\000"
.LASF14:
	.ascii	"long int\000"
.LASF7:
	.ascii	"short int\000"
.LASF20:
	.ascii	"wdt_resets\000"
.LASF36:
	.ascii	"rx_data_lpbk_msgs\000"
.LASF16:
	.ascii	"uint16_t\000"
.LASF47:
	.ascii	"duty_cycle_acc\000"
.LASF65:
	.ascii	"station_or_light_number_0\000"
.LASF41:
	.ascii	"rx_stats_req_msgs\000"
.LASF132:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF133:
	.ascii	"TPMICRO_debug_text\000"
.LASF76:
	.ascii	"unicast_response_crc_errs\000"
.LASF73:
	.ascii	"DECODER_FAULTS_ARRAY_TYPE\000"
.LASF126:
	.ascii	"filler\000"
.LASF136:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_tpmicro_debug.c\000"
.LASF24:
	.ascii	"eep_crc_err_com_parms\000"
.LASF91:
	.ascii	"send_wind_settings_structure_to_the_tpmicro\000"
.LASF128:
	.ascii	"ltext\000"
.LASF69:
	.ascii	"fault_type_code\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF102:
	.ascii	"nlu_freeze_switch_active\000"
.LASF27:
	.ascii	"eep_crc_err_stats\000"
.LASF127:
	.ascii	"TPMICRO_DATA_STRUCT\000"
.LASF72:
	.ascii	"afflicted_output\000"
.LASF59:
	.ascii	"output\000"
.LASF113:
	.ascii	"two_wire_request_statistics_from_all_decoders\000"
.LASF114:
	.ascii	"two_wire_start_all_decoder_loopback_test\000"
.LASF54:
	.ascii	"STAT2_REQ_RSP_s\000"
.LASF37:
	.ascii	"rx_put_parms_msgs\000"
.LASF85:
	.ascii	"send_command\000"
.LASF71:
	.ascii	"decoder_sn\000"
.LASF92:
	.ascii	"request_whats_installed_from_tp_micro\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF135:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF52:
	.ascii	"sol2_status\000"
.LASF67:
	.ascii	"current_percentage_of_max\000"
.LASF134:
	.ascii	"tpmicro_data\000"
.LASF45:
	.ascii	"seqnum\000"
.LASF53:
	.ascii	"sys_flags\000"
.LASF28:
	.ascii	"rx_msgs\000"
.LASF62:
	.ascii	"ERROR_LOG_s\000"
.LASF56:
	.ascii	"decoder_subtype\000"
.LASF70:
	.ascii	"id_info\000"
.LASF86:
	.ascii	"TWO_WIRE_CABLE_POWER_OPERATION_STRUCT\000"
.LASF130:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF109:
	.ascii	"decoder_info\000"
.LASF98:
	.ascii	"et_gage_clear_runaway_gage_being_processed\000"
.LASF50:
	.ascii	"sol2_cur_s\000"
.LASF46:
	.ascii	"dv_adc_cnts\000"
.LASF48:
	.ascii	"SOL_CUR_MEAS_s\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF120:
	.ascii	"two_wire_cable_cooled_off_xmission_state\000"
.LASF75:
	.ascii	"unicast_no_replies\000"
.LASF35:
	.ascii	"rx_dec_rst_msgs\000"
.LASF84:
	.ascii	"MEAS_MA_FOR_DISTRIBUTION\000"
.LASF103:
	.ascii	"nlu_fuse_blown\000"
.LASF106:
	.ascii	"measured_ma_current_as_rcvd_from_master\000"
.LASF107:
	.ascii	"terminal_short_or_no_current_state\000"
.LASF129:
	.ascii	"GuiFont_LanguageActive\000"
.LASF34:
	.ascii	"rx_id_req_msgs\000"
.LASF116:
	.ascii	"decoders_discovered_so_far\000"
.LASF81:
	.ascii	"float\000"
.LASF108:
	.ascii	"terminal_short_or_no_current_report\000"
.LASF66:
	.ascii	"TERMINAL_SHORT_OR_NO_CURRENT_STRUCT\000"
.LASF21:
	.ascii	"bod_resets\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF80:
	.ascii	"TWO_WIRE_COMM_STATS_PER_DECODER_STRUCT\000"
.LASF25:
	.ascii	"eep_crc_err_sol1_parms\000"
.LASF138:
	.ascii	"FDTO_TPMICRO_draw_debug\000"
.LASF29:
	.ascii	"rx_long_msgs\000"
.LASF30:
	.ascii	"rx_crc_errs\000"
.LASF111:
	.ascii	"two_wire_perform_discovery\000"
.LASF51:
	.ascii	"sol1_status\000"
.LASF74:
	.ascii	"unicast_msgs_sent\000"
.LASF97:
	.ascii	"et_gage_clear_runaway_gage\000"
.LASF118:
	.ascii	"two_wire_cable_excessive_current_xmission_state\000"
.LASF43:
	.ascii	"tx_acks_sent\000"
.LASF64:
	.ascii	"terminal_type\000"
.LASF22:
	.ascii	"sol_1_ucos\000"
.LASF32:
	.ascii	"rx_enq_msgs\000"
.LASF90:
	.ascii	"CS3000_DECODER_INFO_STRUCT\000"
.LASF40:
	.ascii	"rx_stat_req_msgs\000"
.LASF83:
	.ascii	"current_needs_to_be_sent\000"
.LASF19:
	.ascii	"por_resets\000"
.LASF139:
	.ascii	"pcomplete_redraw\000"
.LASF61:
	.ascii	"errorBitField\000"
.LASF0:
	.ascii	"char\000"
.LASF18:
	.ascii	"temp_current\000"
.LASF112:
	.ascii	"two_wire_clear_statistics_at_all_decoders\000"
.LASF119:
	.ascii	"two_wire_cable_over_heated_xmission_state\000"
.LASF95:
	.ascii	"et_gage_pulse_count_to_send_to_the_master\000"
.LASF122:
	.ascii	"sn_to_set\000"
.LASF115:
	.ascii	"two_wire_stop_all_decoder_loopback_test\000"
.LASF57:
	.ascii	"fw_vers\000"
.LASF58:
	.ascii	"ID_REQ_RESP_s\000"
.LASF44:
	.ascii	"DECODER_STATS_s\000"
.LASF93:
	.ascii	"rcvd_errors\000"
.LASF87:
	.ascii	"decoder_statistics\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF101:
	.ascii	"nlu_rain_switch_active\000"
.LASF137:
	.ascii	"DECODER_FAULT_BASE_STRUCT\000"
.LASF15:
	.ascii	"uint8_t\000"
.LASF117:
	.ascii	"twccm\000"
.LASF60:
	.ascii	"TWO_WIRE_DECODER_SOLENOID_OPERATION_STRUCT\000"
.LASF38:
	.ascii	"rx_get_parms_msgs\000"
.LASF123:
	.ascii	"send_2w_solenoid_location_on_off_command\000"
.LASF79:
	.ascii	"loop_data_bytes_recd\000"
.LASF77:
	.ascii	"unicast_response_length_errs\000"
.LASF121:
	.ascii	"two_wire_set_decoder_sn\000"
.LASF49:
	.ascii	"sol1_cur_s\000"
.LASF105:
	.ascii	"as_rcvd_from_slaves\000"
.LASF63:
	.ascii	"result\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
