	.file	"e_turn_off.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.text.FDTO_TURN_OFF_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_TURN_OFF_draw_screen
	.type	FDTO_TURN_OFF_draw_screen, %function
FDTO_TURN_OFF_draw_screen:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_turn_off.c"
	.loc 1 35 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-12]
	.loc 1 38 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L2
	.loc 1 40 0
	bl	NETWORK_CONFIG_get_controller_off
	mov	r2, r0
	ldr	r3, .L4
	str	r2, [r3, #0]
	.loc 1 42 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L3
.L2:
	.loc 1 46 0
	ldr	r3, .L4+4
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L3:
	.loc 1 49 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #61
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 50 0
	bl	GuiLib_Refresh
	.loc 1 51 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	GuiVar_TurnOffControllerIsOff
	.word	GuiLib_ActiveCursorFieldNo
.LFE0:
	.size	FDTO_TURN_OFF_draw_screen, .-FDTO_TURN_OFF_draw_screen
	.section	.text.TURN_OFF_process_screen,"ax",%progbits
	.align	2
	.global	TURN_OFF_process_screen
	.type	TURN_OFF_process_screen, %function
TURN_OFF_process_screen:
.LFB1:
	.loc 1 55 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI3:
	add	fp, sp, #12
.LCFI4:
	sub	sp, sp, #16
.LCFI5:
	str	r0, [fp, #-20]
	str	r1, [fp, #-16]
	.loc 1 56 0
	ldr	r3, [fp, #-20]
	cmp	r3, #2
	beq	.L8
	cmp	r3, #67
	beq	.L9
	b	.L7
.L8:
	.loc 1 59 0
	bl	good_key_beep
	.loc 1 61 0
	ldr	r3, .L16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, .L16
	str	r2, [r3, #0]
	.loc 1 63 0
	ldr	r3, .L16
	ldr	r5, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r4, r0
	mov	r0, #2
	bl	NETWORK_CONFIG_get_change_bits_ptr
	mov	r3, r0
	mov	r2, #1
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r5
	mov	r1, #1
	mov	r2, #2
	mov	r3, r4
	bl	NETWORK_CONFIG_set_scheduled_irrigation_off
	.loc 1 65 0
	ldr	r3, .L16+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 69 0
	ldr	r0, .L16+8
	bl	postBackground_Calculation_Event
	.loc 1 74 0
	b	.L10
.L11:
	.loc 1 76 0
	ldr	r3, .L16+12
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L16+12
	str	r2, [r3, #0]
.L10:
	.loc 1 74 0 discriminator 1
	ldr	r3, .L16+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L11
	.loc 1 79 0
	ldr	r3, .L16+12
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L16+16
	add	r3, r2, r3
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 80 0
	b	.L6
.L9:
	.loc 1 83 0
	ldr	r3, .L16+12
	ldr	r2, [r3, #0]
	ldr	r0, .L16+16
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L15
.L14:
	.loc 1 86 0
	ldr	r3, .L16+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 87 0
	b	.L7
.L15:
	.loc 1 90 0
	ldr	r3, .L16+4
	mov	r2, #8
	str	r2, [r3, #0]
.L7:
	.loc 1 97 0
	sub	r1, fp, #20
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L6:
	.loc 1 99 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L17:
	.align	2
.L16:
	.word	GuiVar_TurnOffControllerIsOff
	.word	GuiVar_MenuScreenToShow
	.word	4369
	.word	screen_history_index
	.word	ScreenHistory
.LFE1:
	.size	TURN_OFF_process_screen, .-TURN_OFF_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x2f0
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF40
	.byte	0x1
	.4byte	.LASF41
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8b
	.uleb128 0x6
	.4byte	0x92
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xb0
	.uleb128 0x9
	.4byte	0x92
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd5
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x82
	.4byte	0xb0
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x167
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x88
	.4byte	0x178
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x8d
	.4byte	0x18a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x92
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x173
	.uleb128 0xd
	.4byte	0x173
	.byte	0
	.uleb128 0xe
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x167
	.uleb128 0xc
	.byte	0x1
	.4byte	0x18a
	.uleb128 0xd
	.4byte	0xd5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x17e
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9e
	.4byte	0xe0
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.byte	0x22
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1d1
	.uleb128 0x10
	.4byte	.LASF28
	.byte	0x1
	.byte	0x22
	.4byte	0x1d1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF33
	.byte	0x1
	.byte	0x24
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x7a
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.byte	0x36
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x1fe
	.uleb128 0x10
	.4byte	.LASF29
	.byte	0x1
	.byte	0x36
	.4byte	0x1fe
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0xe
	.4byte	0xd5
	.uleb128 0x12
	.4byte	.LASF30
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF31
	.byte	0x5
	.2byte	0x478
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF32
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF34
	.byte	0x7
	.byte	0x30
	.4byte	0x23e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF35
	.byte	0x7
	.byte	0x34
	.4byte	0x254
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF36
	.byte	0x7
	.byte	0x36
	.4byte	0x26a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF37
	.byte	0x7
	.byte	0x38
	.4byte	0x280
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x8
	.4byte	0x190
	.4byte	0x295
	.uleb128 0x9
	.4byte	0x92
	.byte	0x31
	.byte	0
	.uleb128 0x13
	.4byte	.LASF38
	.byte	0x4
	.byte	0xac
	.4byte	0x285
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF39
	.byte	0x4
	.byte	0xae
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF30
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF31
	.byte	0x5
	.2byte	0x478
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF32
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF38
	.byte	0x4
	.byte	0xac
	.4byte	0x285
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF39
	.byte	0x4
	.byte	0xae
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF9:
	.ascii	"long long int\000"
.LASF34:
	.ascii	"GuiFont_LanguageActive\000"
.LASF21:
	.ascii	"_04_func_ptr\000"
.LASF41:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_turn_off.c\000"
.LASF17:
	.ascii	"_02_menu\000"
.LASF20:
	.ascii	"key_process_func_ptr\000"
.LASF28:
	.ascii	"pcomplete_redraw\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF38:
	.ascii	"ScreenHistory\000"
.LASF15:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF19:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF24:
	.ascii	"_08_screen_to_draw\000"
.LASF26:
	.ascii	"FDTO_TURN_OFF_draw_screen\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF27:
	.ascii	"TURN_OFF_process_screen\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF13:
	.ascii	"keycode\000"
.LASF16:
	.ascii	"_01_command\000"
.LASF25:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF40:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF35:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF29:
	.ascii	"pkey_event\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF14:
	.ascii	"repeats\000"
.LASF39:
	.ascii	"screen_history_index\000"
.LASF30:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF22:
	.ascii	"_06_u32_argument1\000"
.LASF31:
	.ascii	"GuiVar_TurnOffControllerIsOff\000"
.LASF0:
	.ascii	"char\000"
.LASF33:
	.ascii	"lcursor_to_select\000"
.LASF37:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF4:
	.ascii	"short int\000"
.LASF18:
	.ascii	"_03_structure_to_draw\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF23:
	.ascii	"_07_u32_argument2\000"
.LASF12:
	.ascii	"long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF32:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF36:
	.ascii	"GuiFont_DecimalChar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
