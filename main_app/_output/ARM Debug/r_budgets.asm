	.file	"r_budgets.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_budgets.c\000"
	.align	2
.LC1:
	.ascii	"\000"
	.align	2
.LC2:
	.ascii	"Alert only\012POC expected to be %d%% UNDER BUDGET "
	.ascii	"at end of period\000"
	.align	2
.LC3:
	.ascii	"Alert only\012POC expected to be %d%% OVER BUDGET a"
	.ascii	"t end of period\000"
	.align	2
.LC4:
	.ascii	"Alert only\012POC NOW %d%% OVER BUDGET. Expected to"
	.ascii	" be %d%% over budget at end of period\000"
	.align	2
.LC5:
	.ascii	"POC expected to be %d%% UNDER BUDGET at end of peri"
	.ascii	"od\000"
	.align	2
.LC6:
	.ascii	"POC expected to be AT BUDGET at end of period\000"
	.align	2
.LC7:
	.ascii	"POC expected to be %d%% OVER BUDGET at end of perio"
	.ascii	"d\000"
	.align	2
.LC8:
	.ascii	"POC NOW %d%% OVER BUDGET. Expected to be %d%% over "
	.ascii	"budget at end of period\000"
	.align	2
.LC9:
	.ascii	"unknown budget mode\000"
	.section	.text.BUDGET_REPORT_copy_group_into_guivars,"ax",%progbits
	.align	2
	.type	BUDGET_REPORT_copy_group_into_guivars, %function
BUDGET_REPORT_copy_group_into_guivars:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_budgets.c"
	.loc 1 48 0
	@ args = 0, pretend = 0, frame = 492
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #500
.LCFI2:
	str	r0, [fp, #-496]
	.loc 1 50 0
	ldr	r3, .L20	@ float
	str	r3, [fp, #-84]	@ float
	.loc 1 74 0
	sub	r3, fp, #440
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 78 0
	ldr	r3, .L20+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L20+8
	mov	r3, #78
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 80 0
	ldr	r0, [fp, #-496]
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-20]
	.loc 1 81 0
	ldr	r0, [fp, #-20]
	bl	POC_get_GID_irrigation_system
	str	r0, [fp, #-24]
	.loc 1 84 0
	bl	NETWORK_CONFIG_get_water_units
	mov	r2, r0
	ldr	r3, .L20+12
	str	r2, [r3, #0]
	.loc 1 86 0
	ldr	r3, .L20+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L2
	.loc 1 86 0 is_stmt 0 discriminator 1
	flds	s15, [fp, #-84]
	flds	s14, .L20+104
	fdivs	s15, s14, s15
	b	.L3
.L2:
	.loc 1 86 0 discriminator 2
	flds	s15, .L20+104
.L3:
	.loc 1 86 0 discriminator 3
	fsts	s15, [fp, #-28]
	.loc 1 90 0 is_stmt 1 discriminator 3
	ldr	r0, [fp, #-20]
	bl	nm_GROUP_get_group_ID
	mov	r2, r0
	ldr	r3, .L20+16
	str	r2, [r3, #0]
	.loc 1 92 0 discriminator 3
	ldr	r0, [fp, #-20]
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r0, .L20+20
	mov	r1, r3
	mov	r2, #65
	bl	strlcpy
	.loc 1 95 0 discriminator 3
	ldr	r0, [fp, #-496]
	bl	POC_get_gid_of_group_at_this_index
	str	r0, [fp, #-32]
	.loc 1 97 0 discriminator 3
	ldr	r0, [fp, #-32]
	bl	POC_get_box_index_0
	mov	r2, r0
	ldr	r3, .L20+24
	str	r2, [r3, #0]
	.loc 1 99 0 discriminator 3
	ldr	r0, [fp, #-20]
	mov	r1, #0
	bl	POC_get_budget
	str	r0, [fp, #-36]
	.loc 1 100 0 discriminator 3
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, [fp, #-28]
	fmuls	s15, s14, s15
	ldr	r3, .L20+28
	fsts	s15, [r3, #0]
	.loc 1 107 0 discriminator 3
	ldr	r3, .L20+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 112 0 discriminator 3
	ldr	r3, .L20+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L20+8
	mov	r3, #112
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 114 0 discriminator 3
	ldr	r0, [fp, #-24]
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-40]
	.loc 1 115 0 discriminator 3
	ldr	r0, [fp, #-40]
	bl	nm_SYSTEM_get_used
	str	r0, [fp, #-44]
	.loc 1 118 0 discriminator 3
	ldr	r0, [fp, #-40]
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r0, .L20+36
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 121 0 discriminator 3
	sub	r3, fp, #196
	ldr	r0, [fp, #-40]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 122 0 discriminator 3
	ldr	r0, [fp, #-40]
	mov	r1, #0
	bl	nm_SYSTEM_get_budget
	mov	r3, r0
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-28]
	fmuls	s15, s14, s15
	ldr	r3, .L20+40
	fsts	s15, [r3, #0]
	.loc 1 126 0 discriminator 3
	ldr	r3, [fp, #-88]
	cmp	r3, #0
	moveq	r3, #0
	movne	r3, #1
	str	r3, [fp, #-48]
	.loc 1 129 0 discriminator 3
	sub	r3, fp, #440
	mov	r0, r3
	mov	r1, #0
	bl	BUDGET_calculate_ratios
	.loc 1 131 0 discriminator 3
	sub	r1, fp, #196
	sub	r2, fp, #440
	ldr	r3, [fp, #-48]
	str	r3, [sp, #4]
	ldr	r3, .L20+68
	sub	r0, fp, #4
	ldrh	r3, [r0, r3]	@ movhi
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [fp, #-440]
	ldr	r0, [fp, #-24]
	bl	nm_BUDGET_predicted_volume
	str	r0, [fp, #-52]	@ float
	.loc 1 134 0 discriminator 3
	ldr	r3, .L20+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 138 0 discriminator 3
	ldr	r3, .L20+44
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L20+8
	mov	r3, #138
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 140 0 discriminator 3
	sub	r3, fp, #492
	ldr	r0, [fp, #-32]
	mov	r1, r3
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	.loc 1 141 0 discriminator 3
	ldr	r3, [fp, #-492]
	mov	r0, r3
	bl	nm_BUDGET_get_used
	fmdrr	d7, r0, r1
	fcvtsd	s15, d7
	fsts	s15, [fp, #-56]
	.loc 1 142 0 discriminator 3
	flds	s14, [fp, #-28]
	flds	s15, [fp, #-56]
	fmuls	s15, s14, s15
	ldr	r3, .L20+48
	fsts	s15, [r3, #0]
	.loc 1 143 0 discriminator 3
	ldr	r3, [fp, #-44]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, [fp, #-28]
	fmuls	s15, s14, s15
	ldr	r3, .L20+52
	fsts	s15, [r3, #0]
	.loc 1 146 0 discriminator 3
	sub	r3, fp, #488
	mov	r0, r3
	mov	r1, #0
	mov	r2, #48
	bl	memset
	.loc 1 147 0 discriminator 3
	sub	r3, fp, #488
	ldr	r0, [fp, #-24]
	mov	r1, r3
	bl	nm_BUDGET_calc_rpoc
	.loc 1 148 0 discriminator 3
	ldr	r3, .L20+44
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 151 0 discriminator 3
	ldr	r3, [fp, #-36]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-56]
	fsubs	s14, s14, s15
	ldr	r2, [fp, #-496]
	ldr	r3, .L20+100
	mov	r2, r2, asl #2
	sub	r0, fp, #4
	add	r2, r0, r2
	add	r3, r2, r3
	flds	s13, [r3, #0]
	flds	s15, [fp, #-52]
	fmuls	s15, s13, s15
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-60]
	.loc 1 154 0 discriminator 3
	flds	s14, .L20+104
	flds	s15, [fp, #-60]
	fsubs	s14, s14, s15
	flds	s15, .L20+108
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-8]
	.loc 1 155 0 discriminator 3
	flds	s14, [fp, #-8]
	flds	s15, .L20+108
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L4
	.loc 1 157 0
	ldr	r3, .L20+108	@ float
	str	r3, [fp, #-8]	@ float
.L4:
	.loc 1 161 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 162 0
	bl	STATION_GROUP_get_num_groups_in_use
	str	r0, [fp, #-64]
	.loc 1 163 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L5
.L8:
	.loc 1 165 0
	ldr	r0, [fp, #-16]
	bl	STATION_GROUP_get_group_at_this_index
	str	r0, [fp, #-68]
	.loc 1 166 0
	ldr	r0, [fp, #-68]
	bl	STATION_GROUP_get_GID_irrigation_system
	mov	r2, r0
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bne	.L6
.LBB2:
	.loc 1 169 0
	ldr	r0, [fp, #-68]
	bl	STATION_GROUP_get_budget_reduction_limit
	str	r0, [fp, #-72]
	.loc 1 170 0
	ldr	r3, [fp, #-72]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, [fp, #-8]
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L6
	.loc 1 170 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-72]
	cmp	r3, #99
	bhi	.L6
	.loc 1 172 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 173 0
	b	.L7
.L21:
	.align	2
.L20:
	.word	1144718131
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_BudgetUnitHCF
	.word	g_GROUP_ID
	.word	GuiVar_GroupName
	.word	GuiVar_POCBoxIndex
	.word	GuiVar_BudgetAllocation
	.word	list_system_recursive_MUTEX
	.word	GuiVar_BudgetMainlineName
	.word	GuiVar_BudgetAllocationSYS
	.word	poc_preserves_recursive_MUTEX
	.word	GuiVar_BudgetUsed
	.word	GuiVar_BudgetUsedSYS
	.word	GuiVar_BudgetEst
	.word	GuiVar_BudgetPeriod_0
	.word	GuiVar_BudgetPeriod_1
	.word	-432
	.word	GuiVar_BudgetDaysLeftInPeriod
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	-484
	.word	1065353216
	.word	1120403456
.L6:
.LBE2:
	.loc 1 163 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L5:
	.loc 1 163 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-64]
	cmp	r2, r3
	bcc	.L8
.L7:
	.loc 1 181 0 is_stmt 1
	ldr	r3, [fp, #-44]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-52]
	fadds	s14, s14, s15
	flds	s15, [fp, #-28]
	fmuls	s15, s14, s15
	ldr	r3, .L20+56
	fsts	s15, [r3, #0]
	.loc 1 186 0
	ldr	r3, [fp, #-184]
	sub	r2, fp, #420
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #224
	mov	r2, r3
	mov	r3, #125
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L20+60
	mov	r1, r3
	mov	r2, #16
	bl	strlcpy
	.loc 1 187 0
	ldr	r3, [fp, #-180]
	sub	r2, fp, #420
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #224
	mov	r2, r3
	mov	r3, #125
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L20+64
	mov	r1, r3
	mov	r2, #16
	bl	strlcpy
	.loc 1 190 0
	ldr	r2, [fp, #-180]
	ldr	r3, .L20+68
	sub	r0, fp, #4
	ldrh	r3, [r0, r3]
	rsb	r2, r3, r2
	ldr	r1, [fp, #-440]
	ldr	r3, [fp, #-192]
	cmp	r1, r3
	bhi	.L9
	.loc 1 190 0 is_stmt 0 discriminator 1
	mov	r3, #1
	b	.L10
.L9:
	.loc 1 190 0 discriminator 2
	mov	r3, #0
.L10:
	.loc 1 190 0 discriminator 3
	add	r2, r2, r3
	ldr	r3, .L20+72
	str	r2, [r3, #0]
	.loc 1 194 0 is_stmt 1 discriminator 3
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	bne	.L11
	.loc 1 196 0
	sub	r3, fp, #420
	mov	r0, r3
	mov	r1, #224
	ldr	r2, .L20+76
	bl	snprintf
	b	.L12
.L11:
	.loc 1 202 0
	ldr	r3, [fp, #-88]
	cmp	r3, #0
	bne	.L13
	.loc 1 204 0
	flds	s14, [fp, #-60]
	flds	s15, .L20+104
	fcmpes	s14, s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L14
	.loc 1 206 0
	ldr	r2, [fp, #-496]
	ldr	r3, .L20+100
	mov	r2, r2, asl #2
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-52]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-56]
	fadds	s14, s14, s15
	flds	s15, .L20+108
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	flds	s14, .L20+108
	fsubs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-76]
	.loc 1 207 0
	sub	r3, fp, #420
	mov	r0, r3
	mov	r1, #224
	ldr	r2, .L20+80
	ldr	r3, [fp, #-76]
	bl	snprintf
	b	.L12
.L14:
	.loc 1 209 0
	flds	s15, [fp, #-60]
	fcmpezs	s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L15
	.loc 1 212 0
	ldr	r2, [fp, #-496]
	ldr	r3, .L20+100
	mov	r2, r2, asl #2
	sub	r0, fp, #4
	add	r2, r0, r2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-52]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-56]
	fadds	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fsubs	s14, s14, s15
	flds	s15, .L20+108
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-76]
	.loc 1 213 0
	sub	r3, fp, #420
	mov	r0, r3
	mov	r1, #224
	ldr	r2, .L20+84
	ldr	r3, [fp, #-76]
	bl	snprintf
	b	.L12
.L15:
	.loc 1 218 0
	ldr	r2, [fp, #-496]
	ldr	r3, .L20+100
	mov	r2, r2, asl #2
	sub	r0, fp, #4
	add	r2, r0, r2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-52]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-56]
	fadds	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fsubs	s14, s14, s15
	flds	s15, .L20+108
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-76]
	.loc 1 219 0
	ldr	r3, [fp, #-36]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	flds	s14, [fp, #-56]
	fsubs	s14, s14, s15
	flds	s15, .L20+108
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-80]
	.loc 1 220 0
	sub	r3, fp, #420
	ldr	r2, [fp, #-76]
	str	r2, [sp, #0]
	mov	r0, r3
	mov	r1, #224
	ldr	r2, .L20+88
	ldr	r3, [fp, #-80]
	bl	snprintf
	b	.L12
.L13:
	.loc 1 223 0
	ldr	r3, [fp, #-88]
	cmp	r3, #1
	bne	.L16
	.loc 1 225 0
	flds	s14, [fp, #-60]
	flds	s15, .L20+104
	fcmpes	s14, s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L17
	.loc 1 227 0
	ldr	r2, [fp, #-496]
	ldr	r3, .L20+100
	mov	r2, r2, asl #2
	sub	r0, fp, #4
	add	r2, r0, r2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-52]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-56]
	fadds	s14, s14, s15
	flds	s15, .L20+108
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	flds	s14, .L20+108
	fsubs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-76]
	.loc 1 228 0
	sub	r3, fp, #420
	mov	r0, r3
	mov	r1, #224
	ldr	r2, .L20+92
	ldr	r3, [fp, #-76]
	bl	snprintf
	b	.L12
.L17:
	.loc 1 230 0
	flds	s15, [fp, #-60]
	fcmpezs	s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L18
	.loc 1 234 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L19
	.loc 1 236 0
	sub	r3, fp, #420
	mov	r0, r3
	mov	r1, #224
	ldr	r2, .L20+96
	bl	snprintf
	b	.L12
.L19:
	.loc 1 241 0
	ldr	r2, [fp, #-496]
	ldr	r3, .L20+100
	mov	r2, r2, asl #2
	sub	r0, fp, #4
	add	r2, r0, r2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-52]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-56]
	fadds	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fsubs	s14, s14, s15
	flds	s15, .L22
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-76]
	.loc 1 242 0
	sub	r3, fp, #420
	mov	r0, r3
	mov	r1, #224
	ldr	r2, .L22+4
	ldr	r3, [fp, #-76]
	bl	snprintf
	b	.L12
.L18:
	.loc 1 248 0
	ldr	r2, [fp, #-496]
	ldr	r3, .L22+8
	mov	r2, r2, asl #2
	sub	r0, fp, #4
	add	r2, r0, r2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-52]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-56]
	fadds	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fsubs	s14, s14, s15
	flds	s15, .L22
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-76]
	.loc 1 249 0
	ldr	r3, [fp, #-36]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	flds	s14, [fp, #-56]
	fsubs	s14, s14, s15
	flds	s15, .L22
	fmuls	s14, s14, s15
	ldr	r3, [fp, #-36]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-80]
	.loc 1 250 0
	sub	r3, fp, #420
	ldr	r2, [fp, #-76]
	str	r2, [sp, #0]
	mov	r0, r3
	mov	r1, #224
	ldr	r2, .L22+12
	ldr	r3, [fp, #-80]
	bl	snprintf
	b	.L12
.L16:
	.loc 1 255 0
	ldr	r0, .L22+16
	bl	Alert_Message
.L12:
	.loc 1 259 0
	sub	r3, fp, #420
	ldr	r0, .L22+20
	mov	r1, r3
	mov	r2, #225
	bl	strlcpy
	.loc 1 262 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L23:
	.align	2
.L22:
	.word	1120403456
	.word	.LC7
	.word	-484
	.word	.LC8
	.word	.LC9
	.word	GuiVar_BudgetReportText
.LFE0:
	.size	BUDGET_REPORT_copy_group_into_guivars, .-BUDGET_REPORT_copy_group_into_guivars
	.section	.text.FDTO_BUDGET_REPORT_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_BUDGET_REPORT_draw_report
	.type	FDTO_BUDGET_REPORT_draw_report, %function
FDTO_BUDGET_REPORT_draw_report:
.LFB1:
	.loc 1 269 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #20
.LCFI5:
	str	r0, [fp, #-20]
	.loc 1 272 0
	mov	r3, #0
	strh	r3, [fp, #-6]	@ movhi
	.loc 1 274 0
	ldr	r3, .L27
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L27+4
	ldr	r3, .L27+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 276 0
	mov	r0, #1
	bl	POC_populate_pointers_of_POCs_for_display
	str	r0, [fp, #-12]
	.loc 1 278 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L25
	.loc 1 280 0
	mvn	r3, #0
	strh	r3, [fp, #-6]	@ movhi
	.loc 1 281 0
	ldr	r3, .L27+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 282 0
	ldr	r3, .L27+16
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 283 0
	ldr	r3, .L27+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_ptr_to_physically_available_poc
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L27+20
	str	r2, [r3, #0]
	.loc 1 284 0
	ldr	r3, .L27+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	BUDGET_REPORT_copy_group_into_guivars
	b	.L26
.L25:
	.loc 1 288 0
	ldr	r3, .L27+24
	ldrh	r3, [r3, #0]	@ movhi
	strh	r3, [fp, #-6]	@ movhi
	.loc 1 289 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_GetTopLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L27+12
	str	r2, [r3, #0]
.L26:
	.loc 1 292 0
	ldrsh	r3, [fp, #-6]
	mov	r0, #75
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 293 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	.loc 1 295 0
	bl	POC_get_menu_index_for_displayed_poc
	str	r0, [fp, #-16]
	.loc 1 296 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r1, .L27+12
	ldr	r1, [r1, #0]
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r1, r1, asl #16
	mov	r1, r1, asr #16
	str	r1, [sp, #0]
	mov	r0, #0
	ldr	r1, .L27+28
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	.loc 1 298 0
	ldr	r3, .L27
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 300 0
	bl	GuiLib_Refresh
	.loc 1 301 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L28:
	.align	2
.L27:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	274
	.word	g_POC_top_line
	.word	g_POC_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	GuiLib_ActiveCursorFieldNo
	.word	POC_load_poc_name_into_guivar
.LFE1:
	.size	FDTO_BUDGET_REPORT_draw_report, .-FDTO_BUDGET_REPORT_draw_report
	.section	.text.BUDGET_REPORT_process_menu,"ax",%progbits
	.align	2
	.global	BUDGET_REPORT_process_menu
	.type	BUDGET_REPORT_process_menu, %function
BUDGET_REPORT_process_menu:
.LFB2:
	.loc 1 306 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 307 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	bhi	.L30
	mov	r2, #1
	mov	r2, r2, asl r3
	ldr	r3, .L35
	and	r3, r2, r3
	cmp	r3, #0
	bne	.L32
	ldr	r3, .L35+4
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L30
.L31:
	.loc 1 313 0
	mov	r0, #0
	mov	r1, #0
	bl	SCROLL_BOX_up_or_down
	.loc 1 314 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L35+8
	str	r2, [r3, #0]
	.loc 1 315 0
	ldr	r3, .L35+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_ptr_to_physically_available_poc
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L35+12
	str	r2, [r3, #0]
	.loc 1 316 0
	ldr	r3, .L35+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	BUDGET_REPORT_copy_group_into_guivars
	.loc 1 317 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 318 0
	b	.L29
.L32:
	.loc 1 323 0
	mov	r0, #0
	mov	r1, #4
	bl	SCROLL_BOX_up_or_down
	.loc 1 324 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L35+8
	str	r2, [r3, #0]
	.loc 1 325 0
	ldr	r3, .L35+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_ptr_to_physically_available_poc
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L35+12
	str	r2, [r3, #0]
	.loc 1 326 0
	ldr	r3, .L35+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	BUDGET_REPORT_copy_group_into_guivars
	.loc 1 327 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 328 0
	b	.L29
.L30:
	.loc 1 331 0
	ldr	r3, [fp, #-12]
	cmp	r3, #67
	bne	.L34
	.loc 1 333 0
	ldr	r3, .L35+16
	ldr	r2, [r3, #0]
	ldr	r0, .L35+20
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L35+24
	str	r2, [r3, #0]
.L34:
	.loc 1 336 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L29:
	.loc 1 338 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L36:
	.align	2
.L35:
	.word	65554
	.word	1048589
	.word	g_POC_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	BUDGET_REPORT_process_menu, .-BUDGET_REPORT_process_menu
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/station_groups.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/poc.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_poc.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x92e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF113
	.byte	0x1
	.4byte	.LASF114
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x3a
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x4c
	.4byte	0x65
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x55
	.4byte	0x77
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x5e
	.4byte	0x89
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x67
	.4byte	0x9b
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF14
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF15
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x2
	.byte	0x99
	.4byte	0x89
	.uleb128 0x5
	.byte	0x4
	.4byte	0xc1
	.uleb128 0x6
	.4byte	0xc8
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF17
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x4
	.byte	0x57
	.4byte	0xc8
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x5
	.byte	0x4c
	.4byte	0xdc
	.uleb128 0x9
	.4byte	0x4c
	.4byte	0x102
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0x127
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x6
	.byte	0x7e
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x6
	.byte	0x80
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF23
	.byte	0x6
	.byte	0x82
	.4byte	0x102
	.uleb128 0xb
	.byte	0x6
	.byte	0x7
	.byte	0x22
	.4byte	0x153
	.uleb128 0xd
	.ascii	"T\000"
	.byte	0x7
	.byte	0x24
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"D\000"
	.byte	0x7
	.byte	0x26
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x7
	.byte	0x28
	.4byte	0x132
	.uleb128 0xb
	.byte	0x14
	.byte	0x7
	.byte	0x31
	.4byte	0x1e5
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x7
	.byte	0x33
	.4byte	0x153
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x7
	.byte	0x35
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x7
	.byte	0x35
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x7
	.byte	0x35
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x7
	.byte	0x37
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x7
	.byte	0x37
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x7
	.byte	0x37
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF32
	.byte	0x7
	.byte	0x39
	.4byte	0x41
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x7
	.byte	0x3b
	.4byte	0x41
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.4byte	.LASF34
	.byte	0x7
	.byte	0x3d
	.4byte	0x15e
	.uleb128 0x9
	.4byte	0x7e
	.4byte	0x200
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x9
	.4byte	0x7e
	.4byte	0x210
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0xe
	.4byte	.LASF35
	.byte	0x8
	.2byte	0x1a2
	.4byte	0x21c
	.uleb128 0xf
	.4byte	.LASF35
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF36
	.byte	0x9
	.byte	0xd0
	.4byte	0x22d
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x1
	.uleb128 0x10
	.byte	0x70
	.byte	0x9
	.2byte	0x19b
	.4byte	0x288
	.uleb128 0x11
	.4byte	.LASF37
	.byte	0x9
	.2byte	0x19d
	.4byte	0xb0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x11
	.4byte	.LASF38
	.byte	0x9
	.2byte	0x19e
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x11
	.4byte	.LASF39
	.byte	0x9
	.2byte	0x19f
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x11
	.4byte	.LASF40
	.byte	0x9
	.2byte	0x1a0
	.4byte	0x288
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x11
	.4byte	.LASF41
	.byte	0x9
	.2byte	0x1a1
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.byte	0
	.uleb128 0x9
	.4byte	0x7e
	.4byte	0x298
	.uleb128 0xa
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0xe
	.4byte	.LASF42
	.byte	0x9
	.2byte	0x1a2
	.4byte	0x233
	.uleb128 0x3
	.4byte	.LASF43
	.byte	0xa
	.byte	0xda
	.4byte	0x2af
	.uleb128 0xf
	.4byte	.LASF43
	.byte	0x1
	.uleb128 0xb
	.byte	0x24
	.byte	0xb
	.byte	0x78
	.4byte	0x33c
	.uleb128 0xc
	.4byte	.LASF44
	.byte	0xb
	.byte	0x7b
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF45
	.byte	0xb
	.byte	0x83
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF46
	.byte	0xb
	.byte	0x86
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF47
	.byte	0xb
	.byte	0x88
	.4byte	0x34d
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF48
	.byte	0xb
	.byte	0x8d
	.4byte	0x35f
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF49
	.byte	0xb
	.byte	0x92
	.4byte	0xbb
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF50
	.byte	0xb
	.byte	0x96
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF51
	.byte	0xb
	.byte	0x9a
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF52
	.byte	0xb
	.byte	0x9c
	.4byte	0x7e
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	0x348
	.uleb128 0x13
	.4byte	0x348
	.byte	0
	.uleb128 0x14
	.4byte	0x6c
	.uleb128 0x5
	.byte	0x4
	.4byte	0x33c
	.uleb128 0x12
	.byte	0x1
	.4byte	0x35f
	.uleb128 0x13
	.4byte	0x127
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x353
	.uleb128 0x3
	.4byte	.LASF53
	.byte	0xb
	.byte	0x9e
	.4byte	0x2b5
	.uleb128 0x15
	.4byte	.LASF115
	.byte	0x1
	.byte	0x2f
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x508
	.uleb128 0x16
	.4byte	.LASF75
	.byte	0x1
	.byte	0x2f
	.4byte	0x508
	.byte	0x3
	.byte	0x91
	.sleb128 -500
	.uleb128 0x17
	.4byte	.LASF54
	.byte	0x1
	.byte	0x32
	.4byte	0x50d
	.byte	0x3
	.byte	0x91
	.sleb128 -88
	.uleb128 0x18
	.ascii	"sf\000"
	.byte	0x1
	.byte	0x33
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x17
	.4byte	.LASF55
	.byte	0x1
	.byte	0x35
	.4byte	0x517
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x17
	.4byte	.LASF56
	.byte	0x1
	.byte	0x36
	.4byte	0x51d
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x17
	.4byte	.LASF57
	.byte	0x1
	.byte	0x37
	.4byte	0x523
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x17
	.4byte	.LASF58
	.byte	0x1
	.byte	0x38
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x17
	.4byte	.LASF59
	.byte	0x1
	.byte	0x39
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x18
	.ascii	"bds\000"
	.byte	0x1
	.byte	0x3a
	.4byte	0x298
	.byte	0x3
	.byte	0x91
	.sleb128 -200
	.uleb128 0x18
	.ascii	"str\000"
	.byte	0x1
	.byte	0x3b
	.4byte	0x529
	.byte	0x3
	.byte	0x91
	.sleb128 -424
	.uleb128 0x17
	.4byte	.LASF60
	.byte	0x1
	.byte	0x3c
	.4byte	0x1e5
	.byte	0x3
	.byte	0x91
	.sleb128 -444
	.uleb128 0x17
	.4byte	.LASF61
	.byte	0x1
	.byte	0x3d
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x17
	.4byte	.LASF62
	.byte	0x1
	.byte	0x3e
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x17
	.4byte	.LASF63
	.byte	0x1
	.byte	0x3f
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x17
	.4byte	.LASF64
	.byte	0x1
	.byte	0x40
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x17
	.4byte	.LASF65
	.byte	0x1
	.byte	0x41
	.4byte	0x539
	.byte	0x3
	.byte	0x91
	.sleb128 -492
	.uleb128 0x17
	.4byte	.LASF66
	.byte	0x1
	.byte	0x42
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x17
	.4byte	.LASF67
	.byte	0x1
	.byte	0x43
	.4byte	0x7e
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x17
	.4byte	.LASF68
	.byte	0x1
	.byte	0x44
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF69
	.byte	0x1
	.byte	0x45
	.4byte	0xb0
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF70
	.byte	0x1
	.byte	0x46
	.4byte	0x7e
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x18
	.ascii	"i\000"
	.byte	0x1
	.byte	0x47
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF71
	.byte	0x1
	.byte	0x48
	.4byte	0x7e
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x17
	.4byte	.LASF72
	.byte	0x1
	.byte	0x7e
	.4byte	0xb0
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x17
	.4byte	.LASF73
	.byte	0x1
	.byte	0x8b
	.4byte	0x7e
	.byte	0x3
	.byte	0x91
	.sleb128 -496
	.uleb128 0x19
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x17
	.4byte	.LASF74
	.byte	0x1
	.byte	0xa9
	.4byte	0x7e
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.byte	0
	.byte	0
	.uleb128 0x14
	.4byte	0x7e
	.uleb128 0x1a
	.4byte	0x512
	.uleb128 0x14
	.4byte	0x2c
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2a4
	.uleb128 0x5
	.byte	0x4
	.4byte	0x222
	.uleb128 0x5
	.byte	0x4
	.4byte	0x210
	.uleb128 0x9
	.4byte	0x3a
	.4byte	0x539
	.uleb128 0xa
	.4byte	0x25
	.byte	0xdf
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x549
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x10c
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x5a0
	.uleb128 0x1c
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x10c
	.4byte	0x5a0
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1d
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x10e
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1d
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x10f
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1d
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x110
	.4byte	0x6c
	.byte	0x2
	.byte	0x91
	.sleb128 -10
	.byte	0
	.uleb128 0x14
	.4byte	0xb0
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x131
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x5cf
	.uleb128 0x1c
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x131
	.4byte	0x127
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF83
	.byte	0xc
	.2byte	0x112
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF84
	.byte	0xc
	.2byte	0x113
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF85
	.byte	0xc
	.2byte	0x115
	.4byte	0x89
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF86
	.byte	0xc
	.2byte	0x117
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x3a
	.4byte	0x617
	.uleb128 0xa
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF87
	.byte	0xc
	.2byte	0x120
	.4byte	0x607
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x3a
	.4byte	0x635
	.uleb128 0xa
	.4byte	0x25
	.byte	0x10
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF88
	.byte	0xc
	.2byte	0x122
	.4byte	0x625
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF89
	.byte	0xc
	.2byte	0x123
	.4byte	0x625
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x3a
	.4byte	0x661
	.uleb128 0xa
	.4byte	0x25
	.byte	0xe0
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF90
	.byte	0xc
	.2byte	0x132
	.4byte	0x651
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF91
	.byte	0xc
	.2byte	0x136
	.4byte	0x89
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF92
	.byte	0xc
	.2byte	0x137
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF93
	.byte	0xc
	.2byte	0x138
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x3a
	.4byte	0x6a9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x40
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF94
	.byte	0xc
	.2byte	0x1fc
	.4byte	0x699
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF95
	.byte	0xc
	.2byte	0x2ec
	.4byte	0x89
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF96
	.byte	0xc
	.2byte	0x341
	.4byte	0x89
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF97
	.byte	0xd
	.2byte	0x127
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF98
	.byte	0xe
	.byte	0x30
	.4byte	0x6f2
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x14
	.4byte	0xf2
	.uleb128 0x17
	.4byte	.LASF99
	.byte	0xe
	.byte	0x34
	.4byte	0x708
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x14
	.4byte	0xf2
	.uleb128 0x17
	.4byte	.LASF100
	.byte	0xe
	.byte	0x36
	.4byte	0x71e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x14
	.4byte	0xf2
	.uleb128 0x17
	.4byte	.LASF101
	.byte	0xe
	.byte	0x38
	.4byte	0x734
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x14
	.4byte	0xf2
	.uleb128 0x1f
	.4byte	.LASF102
	.byte	0xf
	.byte	0x63
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF103
	.byte	0xf
	.byte	0x65
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF104
	.byte	0x9
	.byte	0x33
	.4byte	0x764
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x14
	.4byte	0x1f0
	.uleb128 0x17
	.4byte	.LASF105
	.byte	0x9
	.byte	0x3f
	.4byte	0x77a
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x14
	.4byte	0x200
	.uleb128 0x1f
	.4byte	.LASF106
	.byte	0x10
	.byte	0xc3
	.4byte	0xe7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF107
	.byte	0x10
	.byte	0xc6
	.4byte	0xe7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF108
	.byte	0x10
	.byte	0xc9
	.4byte	0xe7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF109
	.byte	0x11
	.byte	0x1e
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF110
	.byte	0x11
	.byte	0x22
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x365
	.4byte	0x7d0
	.uleb128 0xa
	.4byte	0x25
	.byte	0x31
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF111
	.byte	0xb
	.byte	0xac
	.4byte	0x7c0
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF112
	.byte	0xb
	.byte	0xae
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF83
	.byte	0xc
	.2byte	0x112
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF84
	.byte	0xc
	.2byte	0x113
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF85
	.byte	0xc
	.2byte	0x115
	.4byte	0x89
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF86
	.byte	0xc
	.2byte	0x117
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF87
	.byte	0xc
	.2byte	0x120
	.4byte	0x607
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF88
	.byte	0xc
	.2byte	0x122
	.4byte	0x625
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF89
	.byte	0xc
	.2byte	0x123
	.4byte	0x625
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF90
	.byte	0xc
	.2byte	0x132
	.4byte	0x651
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF91
	.byte	0xc
	.2byte	0x136
	.4byte	0x89
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF92
	.byte	0xc
	.2byte	0x137
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF93
	.byte	0xc
	.2byte	0x138
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF94
	.byte	0xc
	.2byte	0x1fc
	.4byte	0x699
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF95
	.byte	0xc
	.2byte	0x2ec
	.4byte	0x89
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF96
	.byte	0xc
	.2byte	0x341
	.4byte	0x89
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF97
	.byte	0xd
	.2byte	0x127
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF102
	.byte	0xf
	.byte	0x63
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF103
	.byte	0xf
	.byte	0x65
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF106
	.byte	0x10
	.byte	0xc3
	.4byte	0xe7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF107
	.byte	0x10
	.byte	0xc6
	.4byte	0xe7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF108
	.byte	0x10
	.byte	0xc9
	.4byte	0xe7
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF109
	.byte	0x11
	.byte	0x1e
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF110
	.byte	0x11
	.byte	0x22
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF111
	.byte	0xb
	.byte	0xac
	.4byte	0x7c0
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF112
	.byte	0xb
	.byte	0xae
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF28:
	.ascii	"__year\000"
.LASF74:
	.ascii	"limit\000"
.LASF102:
	.ascii	"g_GROUP_ID\000"
.LASF25:
	.ascii	"date_time\000"
.LASF1:
	.ascii	"float\000"
.LASF67:
	.ascii	"percent\000"
.LASF24:
	.ascii	"DATE_TIME\000"
.LASF68:
	.ascii	"reduction\000"
.LASF6:
	.ascii	"UNS_8\000"
.LASF27:
	.ascii	"__month\000"
.LASF84:
	.ascii	"GuiVar_BudgetAllocationSYS\000"
.LASF10:
	.ascii	"short int\000"
.LASF64:
	.ascii	"usedpoc\000"
.LASF69:
	.ascii	"reduction_flag\000"
.LASF81:
	.ascii	"BUDGET_REPORT_process_menu\000"
.LASF44:
	.ascii	"_01_command\000"
.LASF56:
	.ascii	"lsystem\000"
.LASF41:
	.ascii	"mode\000"
.LASF108:
	.ascii	"list_poc_recursive_MUTEX\000"
.LASF18:
	.ascii	"portTickType\000"
.LASF95:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF54:
	.ascii	"HCF_CONVERSION\000"
.LASF85:
	.ascii	"GuiVar_BudgetDaysLeftInPeriod\000"
.LASF93:
	.ascii	"GuiVar_BudgetUsedSYS\000"
.LASF75:
	.ascii	"ppoc_index_0\000"
.LASF21:
	.ascii	"keycode\000"
.LASF45:
	.ascii	"_02_menu\000"
.LASF86:
	.ascii	"GuiVar_BudgetEst\000"
.LASF32:
	.ascii	"__dayofweek\000"
.LASF13:
	.ascii	"INT_32\000"
.LASF113:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF96:
	.ascii	"GuiVar_POCBoxIndex\000"
.LASF109:
	.ascii	"g_POC_top_line\000"
.LASF112:
	.ascii	"screen_history_index\000"
.LASF58:
	.ascii	"gid_sys\000"
.LASF15:
	.ascii	"long long int\000"
.LASF100:
	.ascii	"GuiFont_DecimalChar\000"
.LASF63:
	.ascii	"budget\000"
.LASF23:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF34:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF78:
	.ascii	"lactive_line\000"
.LASF94:
	.ascii	"GuiVar_GroupName\000"
.LASF19:
	.ascii	"xQueueHandle\000"
.LASF38:
	.ascii	"meter_read_time\000"
.LASF61:
	.ascii	"num_used\000"
.LASF77:
	.ascii	"lpocs_in_use\000"
.LASF7:
	.ascii	"UNS_16\000"
.LASF106:
	.ascii	"poc_preserves_recursive_MUTEX\000"
.LASF50:
	.ascii	"_06_u32_argument1\000"
.LASF71:
	.ascii	"over_budget\000"
.LASF59:
	.ascii	"gid_poc\000"
.LASF48:
	.ascii	"key_process_func_ptr\000"
.LASF52:
	.ascii	"_08_screen_to_draw\000"
.LASF76:
	.ascii	"pcomplete_redraw\000"
.LASF70:
	.ascii	"num_groups\000"
.LASF90:
	.ascii	"GuiVar_BudgetReportText\000"
.LASF4:
	.ascii	"unsigned char\000"
.LASF16:
	.ascii	"BOOL_32\000"
.LASF5:
	.ascii	"signed char\000"
.LASF14:
	.ascii	"long long unsigned int\000"
.LASF80:
	.ascii	"FDTO_BUDGET_REPORT_draw_report\000"
.LASF99:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF40:
	.ascii	"meter_read_date\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF107:
	.ascii	"list_system_recursive_MUTEX\000"
.LASF89:
	.ascii	"GuiVar_BudgetPeriod_1\000"
.LASF115:
	.ascii	"BUDGET_REPORT_copy_group_into_guivars\000"
.LASF20:
	.ascii	"xSemaphoreHandle\000"
.LASF8:
	.ascii	"short unsigned int\000"
.LASF97:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF110:
	.ascii	"g_POC_current_list_item_index\000"
.LASF3:
	.ascii	"char\000"
.LASF17:
	.ascii	"long int\000"
.LASF88:
	.ascii	"GuiVar_BudgetPeriod_0\000"
.LASF101:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF39:
	.ascii	"number_of_annual_periods\000"
.LASF92:
	.ascii	"GuiVar_BudgetUsed\000"
.LASF47:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF114:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_budgets.c\000"
.LASF51:
	.ascii	"_07_u32_argument2\000"
.LASF57:
	.ascii	"ptr_group\000"
.LASF72:
	.ascii	"use_limits\000"
.LASF33:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF66:
	.ascii	"ratio\000"
.LASF55:
	.ascii	"lpoc\000"
.LASF9:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF105:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF49:
	.ascii	"_04_func_ptr\000"
.LASF26:
	.ascii	"__day\000"
.LASF91:
	.ascii	"GuiVar_BudgetUnitHCF\000"
.LASF31:
	.ascii	"__seconds\000"
.LASF60:
	.ascii	"today\000"
.LASF46:
	.ascii	"_03_structure_to_draw\000"
.LASF36:
	.ascii	"IRRIGATION_SYSTEM_GROUP_STRUCT\000"
.LASF22:
	.ascii	"repeats\000"
.LASF98:
	.ascii	"GuiFont_LanguageActive\000"
.LASF79:
	.ascii	"lcursor_to_select\000"
.LASF104:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF62:
	.ascii	"predicted\000"
.LASF35:
	.ascii	"STATION_GROUP_STRUCT\000"
.LASF37:
	.ascii	"in_use\000"
.LASF73:
	.ascii	"index_poc_preserves\000"
.LASF83:
	.ascii	"GuiVar_BudgetAllocation\000"
.LASF43:
	.ascii	"POC_GROUP_STRUCT\000"
.LASF82:
	.ascii	"pkey_event\000"
.LASF2:
	.ascii	"double\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF53:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF103:
	.ascii	"g_GROUP_list_item_index\000"
.LASF87:
	.ascii	"GuiVar_BudgetMainlineName\000"
.LASF111:
	.ascii	"ScreenHistory\000"
.LASF42:
	.ascii	"BUDGET_DETAILS_STRUCT\000"
.LASF65:
	.ascii	"Rpoc\000"
.LASF30:
	.ascii	"__minutes\000"
.LASF29:
	.ascii	"__hours\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
