	.file	"k_process.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	stuck_key_data
	.section .rodata
	.align	2
.LC0:
	.ascii	"UP KEY\000"
	.align	2
.LC1:
	.ascii	"DOWN KEY\000"
	.align	2
.LC2:
	.ascii	"LEFT KEY\000"
	.align	2
.LC3:
	.ascii	"RIGHT KEY\000"
	.align	2
.LC4:
	.ascii	"SELECT KEY\000"
	.align	2
.LC5:
	.ascii	"NEXT KEY\000"
	.align	2
.LC6:
	.ascii	"PREV KEY\000"
	.align	2
.LC7:
	.ascii	"PLUS KEY\000"
	.align	2
.LC8:
	.ascii	"MINUS KEY\000"
	.align	2
.LC9:
	.ascii	"STOP KEY\000"
	.align	2
.LC10:
	.ascii	"HELP KEY\000"
	.align	2
.LC11:
	.ascii	"ENG_SPA KEY\000"
	.align	2
.LC12:
	.ascii	"BACK KEY\000"
	.section	.data.stuck_key_data,"aw",%progbits
	.align	2
	.type	stuck_key_data, %object
	.size	stuck_key_data, 156
stuck_key_data:
	.word	4
	.word	0
	.word	.LC0
	.word	0
	.word	0
	.word	.LC1
	.word	1
	.word	0
	.word	.LC2
	.word	3
	.word	0
	.word	.LC3
	.word	2
	.word	0
	.word	.LC4
	.word	20
	.word	0
	.word	.LC5
	.word	16
	.word	0
	.word	.LC6
	.word	84
	.word	0
	.word	.LC7
	.word	80
	.word	0
	.word	.LC8
	.word	48
	.word	0
	.word	.LC9
	.word	81
	.word	0
	.word	.LC10
	.word	83
	.word	0
	.word	.LC11
	.word	67
	.word	0
	.word	.LC12
	.section	.text.KEY_process_BACK,"ax",%progbits
	.align	2
	.type	KEY_process_BACK, %function
KEY_process_BACK:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.c"
	.loc 1 105 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	.loc 1 106 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L2
	.loc 1 108 0
	bl	good_key_beep
	.loc 1 110 0
	bl	SCROLL_BOX_close_all_scroll_boxes
	.loc 1 112 0
	bl	KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen
	.loc 1 116 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L5
	str	r2, [r3, #0]
	.loc 1 118 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L3
	.loc 1 120 0
	ldr	r3, .L5
	ldr	r2, [r3, #0]
	ldr	r0, .L5+4
	mov	r1, #32
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L5+8
	str	r2, [r3, #0]
.L3:
	.loc 1 126 0
	ldr	r3, .L5
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L5+4
	add	r3, r2, r3
	mov	r0, r3
	bl	Display_Post_Command
	b	.L1
.L2:
	.loc 1 130 0
	bl	bad_key_beep
.L1:
	.loc 1 132 0
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE0:
	.size	KEY_process_BACK, .-KEY_process_BACK
	.section	.text.KEY_process_BACK_from_editing_screen,"ax",%progbits
	.align	2
	.global	KEY_process_BACK_from_editing_screen
	.type	KEY_process_BACK_from_editing_screen, %function
KEY_process_BACK_from_editing_screen:
.LFB1:
	.loc 1 135 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI2:
	add	fp, sp, #4
.LCFI3:
	sub	sp, sp, #40
.LCFI4:
	str	r0, [fp, #-44]
	.loc 1 138 0
	bl	good_key_beep
	.loc 1 140 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 141 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-20]
	.loc 1 142 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 143 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	KEY_process_BACK_from_editing_screen, .-KEY_process_BACK_from_editing_screen
	.section	.text.KEY_process_ENG_SPA,"ax",%progbits
	.align	2
	.global	KEY_process_ENG_SPA
	.type	KEY_process_ENG_SPA, %function
KEY_process_ENG_SPA:
.LFB2:
	.loc 1 158 0
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #36
.LCFI7:
	.loc 1 163 0
	ldr	r3, .L13
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	.loc 1 163 0 is_stmt 0 discriminator 1
	bl	DIALOG_a_dialog_is_visible
	mov	r3, r0
	cmp	r3, #0
	bne	.L9
	ldr	r3, .L13+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	ldr	r3, .L13+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	.loc 1 164 0 is_stmt 1 discriminator 1
	ldr	r3, .L13+12
	ldr	r3, [r3, #0]
	.loc 1 163 0 discriminator 1
	cmp	r3, #0
	bne	.L9
	.loc 1 164 0
	ldr	r3, .L13+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	.loc 1 164 0 is_stmt 0 discriminator 1
	ldr	r3, .L13+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	ldr	r3, .L13+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	.loc 1 166 0 is_stmt 1
	bl	good_key_beep
	.loc 1 168 0
	ldr	r3, .L13+28
	ldrh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L10
	.loc 1 170 0
	mov	r0, #1
	bl	GuiLib_SetLanguage
	b	.L11
.L10:
	.loc 1 174 0
	mov	r0, #0
	bl	GuiLib_SetLanguage
.L11:
	.loc 1 181 0
	ldr	r3, .L13+28
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, r3
	bl	CONTRAST_AND_SPEAKER_VOL_set_screen_language
	.loc 1 183 0
	mov	r0, #15
	mov	r1, #10
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	.loc 1 189 0
	ldr	r3, .L13+32
	ldr	r2, [r3, #0]
	ldr	r1, .L13+36
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-40]
	.loc 1 190 0
	ldr	r3, .L13+32
	ldr	r2, [r3, #0]
	ldr	r0, .L13+36
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	str	r3, [fp, #-36]
	.loc 1 191 0
	ldr	r3, .L13+32
	ldr	r2, [r3, #0]
	ldr	r0, .L13+36
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	str	r3, [fp, #-32]
	.loc 1 192 0
	ldr	r3, .L13+32
	ldr	r2, [r3, #0]
	ldr	r0, .L13+36
	mov	r1, #20
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	str	r3, [fp, #-20]
	.loc 1 193 0
	ldr	r3, .L13+32
	ldr	r2, [r3, #0]
	ldr	r0, .L13+36
	mov	r1, #28
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 194 0
	ldr	r3, .L13+32
	ldr	r2, [r3, #0]
	ldr	r0, .L13+36
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	str	r3, [fp, #-24]
	.loc 1 195 0
	ldr	r3, .L13+32
	ldr	r2, [r3, #0]
	ldr	r0, .L13+36
	mov	r1, #12
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	str	r3, [fp, #-28]
	.loc 1 198 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 200 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	b	.L8
.L9:
	.loc 1 204 0
	bl	bad_key_beep
.L8:
	.loc 1 206 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L14:
	.align	2
.L13:
	.word	g_COPY_DIALOG_visible
	.word	g_COMM_OPTIONS_dialog_visible
	.word	GuiVar_DeviceExchangeSyncingRadios
	.word	g_LIVE_SCREENS_dialog_visible
	.word	g_TECH_SUPPORT_dialog_visible
	.word	g_TWO_WIRE_dialog_visible
	.word	g_TWO_WIRE_DEBUG_dialog_visible
	.word	GuiLib_LanguageIndex
	.word	screen_history_index
	.word	ScreenHistory
.LFE2:
	.size	KEY_process_ENG_SPA, .-KEY_process_ENG_SPA
	.section	.text.KEY_process_global_keys,"ax",%progbits
	.align	2
	.global	KEY_process_global_keys
	.type	KEY_process_global_keys, %function
KEY_process_global_keys:
.LFB3:
	.loc 1 223 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	sub	sp, sp, #8
.LCFI10:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 224 0
	ldr	r3, [fp, #-12]
	cmp	r3, #67
	beq	.L18
	cmp	r3, #83
	beq	.L19
	cmp	r3, #48
	beq	.L17
	b	.L22
.L18:
	.loc 1 227 0
	bl	KEY_process_BACK
	.loc 1 228 0
	b	.L15
.L19:
	.loc 1 231 0
	bl	KEY_process_ENG_SPA
	.loc 1 238 0
	b	.L15
.L17:
	.loc 1 243 0
	ldr	r3, .L23
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L21
	.loc 1 245 0
	bl	good_key_beep
	.loc 1 249 0
	mov	r0, #0
	bl	IRRI_COMM_process_stop_command
	.loc 1 259 0
	ldr	r3, .L23+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 261 0
	ldr	r3, .L23+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 262 0
	ldr	r3, .L23+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 264 0
	ldr	r0, .L23+16
	bl	DIALOG_draw_ok_dialog
	.loc 1 265 0
	b	.L15
.L21:
	.loc 1 269 0
	bl	bad_key_beep
	.loc 1 271 0
	b	.L15
.L22:
	.loc 1 275 0
	bl	bad_key_beep
.L15:
	.loc 1 277 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L24:
	.align	2
.L23:
	.word	irri_irri
	.word	GuiVar_StopKeyPending
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	GuiVar_ScrollBoxHorizScrollMarker
	.word	635
.LFE3:
	.size	KEY_process_global_keys, .-KEY_process_global_keys
	.section	.text.Key_Processing_Task,"ax",%progbits
	.align	2
	.global	Key_Processing_Task
	.type	Key_Processing_Task, %function
Key_Processing_Task:
.LFB4:
	.loc 1 305 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	sub	sp, sp, #24
.LCFI13:
	str	r0, [fp, #-28]
	.loc 1 316 0
	ldr	r2, [fp, #-28]
	ldr	r3, .L40
	rsb	r3, r3, r2
	mov	r3, r3, lsr #5
	str	r3, [fp, #-16]
	.loc 1 321 0
	ldr	r3, .L40+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 323 0
	ldr	r3, .L40+8
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 324 0
	ldr	r3, .L40+8
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 325 0
	ldr	r3, .L40+8
	ldr	r2, .L40+12
	str	r2, [r3, #20]
	.loc 1 326 0
	ldr	r3, .L40+8
	mov	r2, #1
	str	r2, [r3, #24]
	.loc 1 327 0
	ldr	r3, .L40+8
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 329 0
	ldr	r3, .L40+8
	ldr	r2, .L40+16
	str	r2, [r3, #16]
	.loc 1 331 0
	ldr	r3, .L40+8
	ldr	r2, [r3, #32]
	ldr	r3, .L40+20
	str	r2, [r3, #0]
	.loc 1 333 0
	ldr	r3, .L40+24
	str	r3, [fp, #-8]
.L39:
	.loc 1 339 0
	ldr	r3, .L40+28
	ldr	r2, [r3, #0]
	sub	r3, fp, #24
	mov	r0, r2
	mov	r1, r3
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	mov	r3, r0
	cmp	r3, #1
	bne	.L26
.LBB2:
	.loc 1 345 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L40+32
	cmp	r2, r3
	bls	.L27
	.loc 1 347 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L28
.L30:
	.loc 1 349 0
	ldr	r1, .L40+36
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bne	.L29
	.loc 1 351 0
	ldr	r1, [fp, #-20]
	ldr	ip, .L40+36
	ldr	r2, [fp, #-12]
	mov	r0, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	str	r1, [r3, #0]
	.loc 1 354 0
	b	.L27
.L29:
	.loc 1 347 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L28:
	.loc 1 347 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #12
	bls	.L30
.L27:
	.loc 1 364 0 is_stmt 1
	ldr	r3, .L40+40
	ldr	r3, [r3, #0]
	and	r3, r3, #16384
	cmp	r3, #0
	bne	.L31
	.loc 1 366 0
	ldr	r3, .L40+44
	mov	r2, #16384
	str	r2, [r3, #0]
	.loc 1 368 0
	bl	CONTRAST_AND_SPEAKER_VOL_get_speaker_volume
	mov	r3, r0
	ldr	r0, .L40+48
	mov	r1, r3
	bl	postContrast_or_Volume_Event
	b	.L32
.L31:
	.loc 1 372 0
	ldr	r3, .L40+4
	ldr	r2, [r3, #0]
	ldr	r0, .L40+8
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L32
	.loc 1 374 0
	ldr	r3, .L40+52
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L33
	.loc 1 376 0
	sub	r1, fp, #24
	ldmia	r1, {r0-r1}
	bl	DIALOG_process_ok_dialog
	b	.L32
.L33:
	.loc 1 378 0
	ldr	r3, .L40+56
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L34
	.loc 1 380 0
	sub	r1, fp, #24
	ldmia	r1, {r0-r1}
	bl	COPY_DIALOG_process_dialog
	b	.L32
.L34:
	.loc 1 387 0
	ldr	r3, .L40+4
	ldr	r2, [r3, #0]
	ldr	r0, .L40+8
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	sub	r1, fp, #24
	ldmia	r1, {r0-r1}
	blx	r3
.L32:
	.loc 1 401 0
	ldr	r3, .L40+60
	str	r3, [fp, #-8]
	b	.L35
.L26:
.LBE2:
	.loc 1 407 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L40+64
	cmp	r2, r3
	bls	.L35
	.loc 1 409 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #500
	str	r3, [fp, #-8]
	.loc 1 411 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L40+64
	cmp	r2, r3
	bhi	.L35
	.loc 1 413 0
	ldr	r3, .L40+68
	mov	r2, #16384
	str	r2, [r3, #0]
	.loc 1 415 0
	ldr	r0, .L40+48
	mov	r1, #0
	bl	postContrast_or_Volume_Event
	.loc 1 423 0
	mov	r0, #20480
	bl	RADIO_TEST_post_event
	.loc 1 430 0
	ldr	r3, .L40+72
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L35
	.loc 1 434 0
	bl	speaker_disable
	.loc 1 441 0
	b	.L36
.L37:
	.loc 1 448 0
	bl	DIALOG_close_all_dialogs
	.loc 1 450 0
	mov	r3, #67
	str	r3, [fp, #-24]
	.loc 1 451 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 452 0
	ldr	r3, .L40+4
	ldr	r2, [r3, #0]
	ldr	r0, .L40+8
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	sub	r1, fp, #24
	ldmia	r1, {r0-r1}
	blx	r3
.L36:
	.loc 1 441 0 discriminator 1
	ldr	r3, .L40+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L37
	.loc 1 460 0
	bl	DIALOG_close_all_dialogs
	.loc 1 464 0
	ldr	r3, .L40+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 468 0
	ldr	r3, .L40+76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L38
	.loc 1 470 0
	ldr	r3, .L40+76
	mov	r2, #0
	str	r2, [r3, #0]
.L38:
	.loc 1 477 0
	bl	speaker_enable
	.loc 1 481 0
	ldr	r3, .L40+4
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L40+8
	add	r3, r2, r3
	mov	r0, r3
	bl	Display_Post_Command
.L35:
	.loc 1 490 0
	bl	xTaskGetTickCount
	mov	r1, r0
	ldr	r3, .L40+80
	ldr	r2, [fp, #-16]
	str	r1, [r3, r2, asl #2]
	.loc 1 494 0
	b	.L39
.L41:
	.align	2
.L40:
	.word	Task_Table
	.word	screen_history_index
	.word	ScreenHistory
	.word	FDTO_MAIN_MENU_draw_menu
	.word	MAIN_MENU_process_menu
	.word	GuiVar_MenuScreenToShow
	.word	10000
	.word	Key_To_Process_Queue
	.word	1999
	.word	stuck_key_data
	.word	1073905676
	.word	1073905668
	.word	13107
	.word	g_DIALOG_ok_dialog_visible
	.word	g_COPY_DIALOG_visible
	.word	900000
	.word	499
	.word	1073905672
	.word	GuiVar_CodeDownloadState
	.word	GuiVar_StatusShowLiveScreens
	.word	task_last_execution_stamp
.LFE4:
	.size	Key_Processing_Task, .-Key_Processing_Task
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/projdefs.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/irri_irri.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/dialog.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_copy_dialog.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_comm_options.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_live_screens.h"
	.file 19 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_tech_support.h"
	.file 20 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_two_wire_assignment.h"
	.file 21 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_two_wire_debug.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x77a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF90
	.byte	0x1
	.4byte	.LASF91
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x53
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x5
	.byte	0x4
	.4byte	0x92
	.uleb128 0x6
	.4byte	0x99
	.uleb128 0x7
	.byte	0
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x3
	.byte	0x47
	.4byte	0xa4
	.uleb128 0x5
	.byte	0x4
	.4byte	0xaa
	.uleb128 0x8
	.byte	0x1
	.4byte	0xb6
	.uleb128 0x9
	.4byte	0xb6
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x4
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x5
	.byte	0x57
	.4byte	0xb6
	.uleb128 0xb
	.4byte	0x33
	.4byte	0xe5
	.uleb128 0xc
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xd
	.byte	0xc
	.byte	0x6
	.byte	0x67
	.4byte	0x118
	.uleb128 0xe
	.4byte	.LASF16
	.byte	0x6
	.byte	0x69
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF17
	.byte	0x6
	.byte	0x6b
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF18
	.byte	0x6
	.byte	0x6d
	.4byte	0x118
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x6
	.byte	0x6f
	.4byte	0xe5
	.uleb128 0xd
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0x14e
	.uleb128 0xe
	.4byte	.LASF20
	.byte	0x6
	.byte	0x7e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF21
	.byte	0x6
	.byte	0x80
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF22
	.byte	0x6
	.byte	0x82
	.4byte	0x129
	.uleb128 0xd
	.byte	0x14
	.byte	0x7
	.byte	0x18
	.4byte	0x1a8
	.uleb128 0xe
	.4byte	.LASF23
	.byte	0x7
	.byte	0x1a
	.4byte	0xb6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF24
	.byte	0x7
	.byte	0x1c
	.4byte	0xb6
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF25
	.byte	0x7
	.byte	0x1e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF26
	.byte	0x7
	.byte	0x20
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF27
	.byte	0x7
	.byte	0x23
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x7
	.byte	0x26
	.4byte	0x159
	.uleb128 0xb
	.4byte	0x5a
	.4byte	0x1c3
	.uleb128 0xc
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xd
	.byte	0x20
	.byte	0x8
	.byte	0x1e
	.4byte	0x23c
	.uleb128 0xe
	.4byte	.LASF29
	.byte	0x8
	.byte	0x20
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF30
	.byte	0x8
	.byte	0x22
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF31
	.byte	0x8
	.byte	0x24
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF32
	.byte	0x8
	.byte	0x26
	.4byte	0x99
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF33
	.byte	0x8
	.byte	0x28
	.4byte	0x23c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF34
	.byte	0x8
	.byte	0x2a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.4byte	.LASF35
	.byte	0x8
	.byte	0x2c
	.4byte	0xb6
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.4byte	.LASF36
	.byte	0x8
	.byte	0x2e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0xf
	.4byte	0x241
	.uleb128 0x5
	.byte	0x4
	.4byte	0x247
	.uleb128 0xf
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF37
	.byte	0x8
	.byte	0x30
	.4byte	0x1c3
	.uleb128 0xd
	.byte	0x24
	.byte	0x9
	.byte	0x78
	.4byte	0x2de
	.uleb128 0xe
	.4byte	.LASF38
	.byte	0x9
	.byte	0x7b
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF39
	.byte	0x9
	.byte	0x83
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF40
	.byte	0x9
	.byte	0x86
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF41
	.byte	0x9
	.byte	0x88
	.4byte	0x2ef
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF42
	.byte	0x9
	.byte	0x8d
	.4byte	0x301
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF43
	.byte	0x9
	.byte	0x92
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0x9
	.byte	0x96
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.4byte	.LASF45
	.byte	0x9
	.byte	0x9a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0x9
	.byte	0x9c
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0x8
	.byte	0x1
	.4byte	0x2ea
	.uleb128 0x9
	.4byte	0x2ea
	.byte	0
	.uleb128 0xf
	.4byte	0x48
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2de
	.uleb128 0x8
	.byte	0x1
	.4byte	0x301
	.uleb128 0x9
	.4byte	0x14e
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2f5
	.uleb128 0x3
	.4byte	.LASF47
	.byte	0x9
	.byte	0x9e
	.4byte	0x257
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF48
	.uleb128 0xb
	.4byte	0x5a
	.4byte	0x329
	.uleb128 0xc
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0xd
	.byte	0x14
	.byte	0xa
	.byte	0x88
	.4byte	0x340
	.uleb128 0xe
	.4byte	.LASF49
	.byte	0xa
	.byte	0x9e
	.4byte	0x1a8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF50
	.byte	0xa
	.byte	0xa0
	.4byte	0x329
	.uleb128 0xb
	.4byte	0x5a
	.4byte	0x35b
	.uleb128 0xc
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF51
	.uleb128 0x10
	.4byte	.LASF92
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF52
	.byte	0x1
	.byte	0x86
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x3ac
	.uleb128 0x12
	.4byte	.LASF55
	.byte	0x1
	.byte	0x86
	.4byte	0xb6
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x88
	.4byte	0x307
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF53
	.byte	0x1
	.byte	0x9d
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x3d4
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x9f
	.4byte	0x307
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF54
	.byte	0x1
	.byte	0xde
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x3fc
	.uleb128 0x12
	.4byte	.LASF56
	.byte	0x1
	.byte	0xde
	.4byte	0x14e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x130
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x46c
	.uleb128 0x15
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x130
	.4byte	0xb6
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x16
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x132
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x134
	.4byte	0x14e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x16
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x139
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x18
	.ascii	"iii\000"
	.byte	0x1
	.2byte	0x157
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.byte	0
	.uleb128 0x19
	.4byte	.LASF62
	.byte	0xb
	.2byte	0x165
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF63
	.byte	0xb
	.2byte	0x190
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF64
	.byte	0xb
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF65
	.byte	0xb
	.2byte	0x3c9
	.4byte	0x6c
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF66
	.byte	0xb
	.2byte	0x3ca
	.4byte	0x6c
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0xb
	.2byte	0x44b
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF68
	.byte	0xb
	.2byte	0x45a
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF69
	.byte	0xc
	.2byte	0x12e
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF70
	.byte	0xd
	.byte	0x30
	.4byte	0x4ed
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xf
	.4byte	0xd5
	.uleb128 0x1a
	.4byte	.LASF71
	.byte	0xd
	.byte	0x34
	.4byte	0x503
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xf
	.4byte	0xd5
	.uleb128 0x1a
	.4byte	.LASF72
	.byte	0xd
	.byte	0x36
	.4byte	0x519
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xf
	.4byte	0xd5
	.uleb128 0x1a
	.4byte	.LASF73
	.byte	0xd
	.byte	0x38
	.4byte	0x52f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xf
	.4byte	0xd5
	.uleb128 0xb
	.4byte	0x11e
	.4byte	0x544
	.uleb128 0xc
	.4byte	0x25
	.byte	0xc
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF74
	.byte	0x6
	.byte	0x71
	.4byte	0x534
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	0x24c
	.4byte	0x561
	.uleb128 0xc
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF75
	.byte	0x8
	.byte	0x38
	.4byte	0x56e
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	0x551
	.uleb128 0x1b
	.4byte	.LASF76
	.byte	0x8
	.byte	0x5a
	.4byte	0x34b
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF77
	.byte	0x8
	.2byte	0x139
	.4byte	0xca
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	0x307
	.4byte	0x59e
	.uleb128 0xc
	.4byte	0x25
	.byte	0x31
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF78
	.byte	0x9
	.byte	0xac
	.4byte	0x58e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF79
	.byte	0x9
	.byte	0xae
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF80
	.byte	0xa
	.byte	0xac
	.4byte	0x340
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF81
	.byte	0xe
	.byte	0x33
	.4byte	0x5d6
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xf
	.4byte	0x1b3
	.uleb128 0x1a
	.4byte	.LASF82
	.byte	0xe
	.byte	0x3f
	.4byte	0x5ec
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xf
	.4byte	0x319
	.uleb128 0x1b
	.4byte	.LASF83
	.byte	0xf
	.byte	0x33
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF84
	.byte	0x10
	.byte	0x14
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF85
	.byte	0x11
	.byte	0x24
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF86
	.byte	0x12
	.byte	0x21
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF87
	.byte	0x13
	.byte	0x17
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF88
	.byte	0x14
	.byte	0x17
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF89
	.byte	0x15
	.byte	0x17
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF62
	.byte	0xb
	.2byte	0x165
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF63
	.byte	0xb
	.2byte	0x190
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF64
	.byte	0xb
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF65
	.byte	0xb
	.2byte	0x3c9
	.4byte	0x6c
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF66
	.byte	0xb
	.2byte	0x3ca
	.4byte	0x6c
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0xb
	.2byte	0x44b
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF68
	.byte	0xb
	.2byte	0x45a
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF69
	.byte	0xc
	.2byte	0x12e
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF74
	.byte	0x1
	.byte	0x42
	.4byte	0x534
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	stuck_key_data
	.uleb128 0x1b
	.4byte	.LASF75
	.byte	0x8
	.byte	0x38
	.4byte	0x6db
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	0x551
	.uleb128 0x1b
	.4byte	.LASF76
	.byte	0x8
	.byte	0x5a
	.4byte	0x34b
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF77
	.byte	0x8
	.2byte	0x139
	.4byte	0xca
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF78
	.byte	0x9
	.byte	0xac
	.4byte	0x58e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF79
	.byte	0x9
	.byte	0xae
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF80
	.byte	0xa
	.byte	0xac
	.4byte	0x340
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF83
	.byte	0xf
	.byte	0x33
	.4byte	0x5a
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF84
	.byte	0x10
	.byte	0x14
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF85
	.byte	0x11
	.byte	0x24
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF86
	.byte	0x12
	.byte	0x21
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF87
	.byte	0x13
	.byte	0x17
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF88
	.byte	0x14
	.byte	0x17
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF89
	.byte	0x15
	.byte	0x17
	.4byte	0x81
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF90:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF91:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/key_"
	.ascii	"scanner/k_process.c\000"
.LASF39:
	.ascii	"_02_menu\000"
.LASF52:
	.ascii	"KEY_process_BACK_from_editing_screen\000"
.LASF85:
	.ascii	"g_COMM_OPTIONS_dialog_visible\000"
.LASF36:
	.ascii	"priority\000"
.LASF92:
	.ascii	"KEY_process_BACK\000"
.LASF47:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF55:
	.ascii	"preturn_to_menu_func_ptr\000"
.LASF14:
	.ascii	"portTickType\000"
.LASF49:
	.ascii	"list_of_irri_all_irrigation\000"
.LASF38:
	.ascii	"_01_command\000"
.LASF60:
	.ascii	"lktpqs\000"
.LASF16:
	.ascii	"key_code\000"
.LASF76:
	.ascii	"task_last_execution_stamp\000"
.LASF64:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF66:
	.ascii	"GuiVar_ScrollBoxHorizScrollPos\000"
.LASF5:
	.ascii	"short int\000"
.LASF20:
	.ascii	"keycode\000"
.LASF22:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF27:
	.ascii	"InUse\000"
.LASF31:
	.ascii	"execution_limit_ms\000"
.LASF78:
	.ascii	"ScreenHistory\000"
.LASF48:
	.ascii	"float\000"
.LASF77:
	.ascii	"Key_To_Process_Queue\000"
.LASF62:
	.ascii	"GuiVar_CodeDownloadState\000"
.LASF10:
	.ascii	"long long int\000"
.LASF12:
	.ascii	"pdTASK_CODE\000"
.LASF72:
	.ascii	"GuiFont_DecimalChar\000"
.LASF1:
	.ascii	"char\000"
.LASF13:
	.ascii	"long int\000"
.LASF75:
	.ascii	"Task_Table\000"
.LASF15:
	.ascii	"xQueueHandle\000"
.LASF26:
	.ascii	"offset\000"
.LASF65:
	.ascii	"GuiVar_ScrollBoxHorizScrollMarker\000"
.LASF87:
	.ascii	"g_TECH_SUPPORT_dialog_visible\000"
.LASF86:
	.ascii	"g_LIVE_SCREENS_dialog_visible\000"
.LASF61:
	.ascii	"task_index\000"
.LASF44:
	.ascii	"_06_u32_argument1\000"
.LASF30:
	.ascii	"include_in_wdt\000"
.LASF18:
	.ascii	"key_name_string\000"
.LASF54:
	.ascii	"KEY_process_global_keys\000"
.LASF19:
	.ascii	"STUCK_KEY_TRACKING_STRUCT\000"
.LASF46:
	.ascii	"_08_screen_to_draw\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF29:
	.ascii	"bCreateTask\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF3:
	.ascii	"signed char\000"
.LASF57:
	.ascii	"Key_Processing_Task\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF71:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF35:
	.ascii	"parameter\000"
.LASF56:
	.ascii	"pkey_event\000"
.LASF58:
	.ascii	"pvParameters\000"
.LASF24:
	.ascii	"ptail\000"
.LASF32:
	.ascii	"pTaskFunc\000"
.LASF79:
	.ascii	"screen_history_index\000"
.LASF28:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF69:
	.ascii	"GuiLib_LanguageIndex\000"
.LASF73:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF17:
	.ascii	"recorded_repeats\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF50:
	.ascii	"IRRI_IRRI\000"
.LASF41:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF84:
	.ascii	"g_COPY_DIALOG_visible\000"
.LASF45:
	.ascii	"_07_u32_argument2\000"
.LASF34:
	.ascii	"stack_depth\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF53:
	.ascii	"KEY_process_ENG_SPA\000"
.LASF74:
	.ascii	"stuck_key_data\000"
.LASF80:
	.ascii	"irri_irri\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF42:
	.ascii	"key_process_func_ptr\000"
.LASF82:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF43:
	.ascii	"_04_func_ptr\000"
.LASF40:
	.ascii	"_03_structure_to_draw\000"
.LASF25:
	.ascii	"count\000"
.LASF21:
	.ascii	"repeats\000"
.LASF70:
	.ascii	"GuiFont_LanguageActive\000"
.LASF81:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF89:
	.ascii	"g_TWO_WIRE_DEBUG_dialog_visible\000"
.LASF23:
	.ascii	"phead\000"
.LASF68:
	.ascii	"GuiVar_StopKeyPending\000"
.LASF33:
	.ascii	"TaskName\000"
.LASF51:
	.ascii	"double\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF37:
	.ascii	"TASK_ENTRY_STRUCT\000"
.LASF88:
	.ascii	"g_TWO_WIRE_dialog_visible\000"
.LASF83:
	.ascii	"g_DIALOG_ok_dialog_visible\000"
.LASF63:
	.ascii	"GuiVar_DeviceExchangeSyncingRadios\000"
.LASF67:
	.ascii	"GuiVar_StatusShowLiveScreens\000"
.LASF59:
	.ascii	"backlight_turn_off_ms\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
