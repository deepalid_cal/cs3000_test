	.file	"cs_mem_part.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	partitions
	.section	.rodata.partitions,"a",%progbits
	.align	2
	.type	partitions, %object
	.size	partitions, 88
partitions:
	.word	96
	.word	1024
	.word	208
	.word	1024
	.word	560
	.word	256
	.word	1152
	.word	32
	.word	2112
	.word	384
	.word	4352
	.word	32
	.word	8448
	.word	32
	.word	40688
	.word	6
	.word	116416
	.word	2
	.word	147456
	.word	2
	.word	1433628
	.word	1
	.global	OSMemTbl
	.section	.bss.OSMemTbl,"aw",%nobits
	.align	2
	.type	OSMemTbl, %object
	.size	OSMemTbl, 220
OSMemTbl:
	.space	220
	.section	.text.__os_mem_create,"ax",%progbits
	.align	2
	.type	__os_mem_create, %function
__os_mem_create:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cs_mem_part.c"
	.loc 1 191 0
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #32
.LCFI2:
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	str	r3, [fp, #-32]
	strh	r0, [fp, #-20]	@ movhi
	.loc 1 197 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L2
	.loc 1 198 0
	ldr	r3, [fp, #4]
	mov	r2, #73
	strb	r2, [r3, #0]
	.loc 1 199 0
	mov	r3, #0
	b	.L3
.L2:
	.loc 1 201 0
	ldr	r3, [fp, #-32]
	cmp	r3, #3
	bhi	.L4
	.loc 1 202 0
	ldr	r3, [fp, #4]
	mov	r2, #74
	strb	r2, [r3, #0]
	.loc 1 203 0
	mov	r3, #0
	b	.L3
.L4:
	.loc 1 207 0
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L5
	.loc 1 209 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-8]
	.loc 1 211 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L6
.L5:
	.loc 1 215 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-8]
	.loc 1 217 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-32]
	add	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 219 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L7
.L8:
	.loc 1 221 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 223 0 discriminator 2
	ldr	r3, [fp, #-4]
	str	r3, [fp, #-8]
	.loc 1 225 0 discriminator 2
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-32]
	add	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 219 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L7:
	.loc 1 219 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	sub	r2, r3, #1
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L8
	.loc 1 228 0 is_stmt 1
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #0]
.L6:
	.loc 1 233 0
	ldrh	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L9
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 235 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 236 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-24]
	str	r2, [r3, #4]
	.loc 1 237 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-28]
	str	r2, [r3, #16]
	.loc 1 238 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-28]
	str	r2, [r3, #12]
	.loc 1 239 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-32]
	str	r2, [r3, #8]
	.loc 1 242 0
	ldr	r3, [fp, #4]
	mov	r2, #0
	strb	r2, [r3, #0]
	.loc 1 244 0
	ldr	r3, [fp, #-16]
.L3:
	.loc 1 245 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L10:
	.align	2
.L9:
	.word	OSMemTbl
.LFE0:
	.size	__os_mem_create, .-__os_mem_create
	.section	.text.init_mem_partitioning,"ax",%progbits
	.align	2
	.global	init_mem_partitioning
	.type	init_mem_partitioning, %function
init_mem_partitioning:
.LFB1:
	.loc 1 248 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #20
.LCFI5:
	.loc 1 278 0
	mov	r3, #0
	strh	r3, [fp, #-6]	@ movhi
	b	.L12
.L15:
	.loc 1 280 0
	ldrh	r2, [fp, #-6]
	ldr	r1, .L16
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	ldrh	r1, [fp, #-6]
	ldr	r2, .L16
	ldr	r2, [r2, r1, asl #3]
	mul	r3, r2, r3
	mov	r0, r3
	bl	malloc
	str	r0, [fp, #-12]
	.loc 1 282 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L13
	.loc 1 287 0
	bl	freeze_and_restart_app
.L13:
	.loc 1 290 0
	ldrh	r1, [fp, #-6]
	ldrh	r2, [fp, #-6]
	ldr	r0, .L16
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r0, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldrh	r0, [fp, #-6]
	ldr	r3, .L16
	ldr	r3, [r3, r0, asl #3]
	sub	r0, fp, #17
	str	r0, [sp, #0]
	mov	r0, r1
	ldr	r1, [fp, #-12]
	bl	__os_mem_create
	str	r0, [fp, #-16]
	.loc 1 292 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L14
	.loc 1 297 0
	bl	freeze_and_restart_app
.L14:
	.loc 1 278 0
	ldrh	r3, [fp, #-6]	@ movhi
	add	r3, r3, #1
	strh	r3, [fp, #-6]	@ movhi
.L12:
	.loc 1 278 0 is_stmt 0 discriminator 1
	ldrh	r3, [fp, #-6]
	cmp	r3, #10
	bls	.L15
	.loc 1 323 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	partitions
.LFE1:
	.size	init_mem_partitioning, .-init_mem_partitioning
	.section	.text.OSMemGet,"ax",%progbits
	.align	2
	.global	OSMemGet
	.type	OSMemGet, %function
OSMemGet:
.LFB2:
	.loc 1 327 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #12
.LCFI8:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 333 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L19
	.loc 1 335 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-4]
	.loc 1 337 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #4]
	.loc 1 339 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #16]
	sub	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #16]
	.loc 1 341 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L20
.L19:
	.loc 1 345 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 347 0
	ldr	r3, [fp, #-12]
	mov	r2, #72
	str	r2, [r3, #0]
.L20:
	.loc 1 363 0
	ldr	r3, [fp, #-4]
	.loc 1 364 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE2:
	.size	OSMemGet, .-OSMemGet
	.section	.text.OSMemPut,"ax",%progbits
	.align	2
	.global	OSMemPut
	.type	OSMemPut, %function
OSMemPut:
.LFB3:
	.loc 1 380 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #16
.LCFI11:
	mov	r3, r0
	str	r1, [fp, #-16]
	strh	r3, [fp, #-12]	@ movhi
	.loc 1 411 0
	ldrh	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L24
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 414 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #16]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #12]
	cmp	r2, r3
	bcc	.L22
	.loc 1 429 0
	mov	r3, #75
	strb	r3, [fp, #-1]
	b	.L23
.L22:
	.loc 1 433 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 435 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #4]
	.loc 1 437 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #16]
	add	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #16]
	.loc 1 439 0
	mov	r3, #0
	strb	r3, [fp, #-1]
.L23:
	.loc 1 443 0
	ldrb	r3, [fp, #-1]	@ zero_extendqisi2
	.loc 1 444 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L25:
	.align	2
.L24:
	.word	OSMemTbl
.LFE3:
	.size	OSMemPut, .-OSMemPut
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cs_mem_part.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x3e1
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF39
	.byte	0x1
	.4byte	.LASF40
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x2
	.byte	0x3a
	.4byte	0x53
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x2
	.byte	0x5e
	.4byte	0x7a
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x5
	.byte	0x8
	.byte	0x3
	.byte	0x17
	.4byte	0xad
	.uleb128 0x6
	.4byte	.LASF12
	.byte	0x3
	.byte	0x19
	.4byte	0x25
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF13
	.byte	0x3
	.byte	0x1b
	.4byte	0x25
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x3
	.byte	0x1d
	.4byte	0x88
	.uleb128 0x5
	.byte	0x14
	.byte	0x3
	.byte	0x49
	.4byte	0x107
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x4a
	.4byte	0x107
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x3
	.byte	0x4b
	.4byte	0x107
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x3
	.byte	0x4c
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x3
	.byte	0x4d
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x3
	.byte	0x4e
	.4byte	0x6f
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.uleb128 0x4
	.4byte	.LASF20
	.byte	0x3
	.byte	0x4f
	.4byte	0xb8
	.uleb128 0x8
	.4byte	0x53
	.4byte	0x124
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0x48
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF21
	.uleb128 0x8
	.4byte	0x6f
	.4byte	0x141
	.uleb128 0x9
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x8
	.4byte	0x6f
	.4byte	0x151
	.uleb128 0x9
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0xb
	.4byte	.LASF41
	.byte	0x1
	.byte	0xbe
	.byte	0x1
	.4byte	0x1ea
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1ea
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x1
	.byte	0xbe
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x1
	.byte	0xbe
	.4byte	0x107
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x1
	.byte	0xbe
	.4byte	0x6f
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x1
	.byte	0xbe
	.4byte	0x6f
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xd
	.ascii	"err\000"
	.byte	0x1
	.byte	0xbe
	.4byte	0x124
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xe
	.4byte	.LASF26
	.byte	0x1
	.byte	0xc0
	.4byte	0x1ea
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xe
	.4byte	.LASF27
	.byte	0x1
	.byte	0xc1
	.4byte	0x124
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xe
	.4byte	.LASF28
	.byte	0x1
	.byte	0xc2
	.4byte	0x1f0
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xf
	.ascii	"i\000"
	.byte	0x1
	.byte	0xc3
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0x109
	.uleb128 0xa
	.byte	0x4
	.4byte	0x107
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF42
	.byte	0x1
	.byte	0xf8
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x247
	.uleb128 0xf
	.ascii	"i\000"
	.byte	0x1
	.byte	0xfa
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -10
	.uleb128 0xf
	.ascii	"err\000"
	.byte	0x1
	.byte	0xfc
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -21
	.uleb128 0xf
	.ascii	"ucp\000"
	.byte	0x1
	.byte	0xfe
	.4byte	0x247
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x100
	.4byte	0x1ea
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0x53
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x146
	.byte	0x1
	.4byte	0x107
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x298
	.uleb128 0x13
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x146
	.4byte	0x1ea
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.ascii	"err\000"
	.byte	0x1
	.2byte	0x146
	.4byte	0x298
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x15
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x14a
	.4byte	0x107
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0x6f
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x17b
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x2f8
	.uleb128 0x13
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x17b
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x17b
	.4byte	0x107
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x17d
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -5
	.uleb128 0x11
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x17f
	.4byte	0x1ea
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x8
	.4byte	0xad
	.4byte	0x308
	.uleb128 0x9
	.4byte	0x25
	.byte	0xa
	.byte	0
	.uleb128 0x16
	.4byte	.LASF31
	.byte	0x3
	.byte	0x20
	.4byte	0x315
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	0x2f8
	.uleb128 0x8
	.4byte	0x109
	.4byte	0x32a
	.uleb128 0x9
	.4byte	0x25
	.byte	0xa
	.byte	0
	.uleb128 0x16
	.4byte	.LASF32
	.byte	0x3
	.byte	0x51
	.4byte	0x31a
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF33
	.byte	0x4
	.byte	0x30
	.4byte	0x348
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x17
	.4byte	0x114
	.uleb128 0xe
	.4byte	.LASF34
	.byte	0x4
	.byte	0x34
	.4byte	0x35e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x17
	.4byte	0x114
	.uleb128 0xe
	.4byte	.LASF35
	.byte	0x4
	.byte	0x36
	.4byte	0x374
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x17
	.4byte	0x114
	.uleb128 0xe
	.4byte	.LASF36
	.byte	0x4
	.byte	0x38
	.4byte	0x38a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x17
	.4byte	0x114
	.uleb128 0xe
	.4byte	.LASF37
	.byte	0x5
	.byte	0x33
	.4byte	0x3a0
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x17
	.4byte	0x131
	.uleb128 0xe
	.4byte	.LASF38
	.byte	0x5
	.byte	0x3f
	.4byte	0x3b6
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x17
	.4byte	0x141
	.uleb128 0x18
	.4byte	.LASF31
	.byte	0x1
	.byte	0x2c
	.4byte	0x3cd
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	partitions
	.uleb128 0x17
	.4byte	0x2f8
	.uleb128 0x18
	.4byte	.LASF32
	.byte	0x1
	.byte	0xa2
	.4byte	0x31a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	OSMemTbl
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF33:
	.ascii	"GuiFont_LanguageActive\000"
.LASF27:
	.ascii	"pblk\000"
.LASF42:
	.ascii	"init_mem_partitioning\000"
.LASF39:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF15:
	.ascii	"OSMemAddr\000"
.LASF26:
	.ascii	"pmem\000"
.LASF10:
	.ascii	"unsigned int\000"
.LASF35:
	.ascii	"GuiFont_DecimalChar\000"
.LASF20:
	.ascii	"OS_MEM\000"
.LASF23:
	.ascii	"addr\000"
.LASF21:
	.ascii	"float\000"
.LASF18:
	.ascii	"OSMemNBlks\000"
.LASF4:
	.ascii	"unsigned char\000"
.LASF24:
	.ascii	"nblks\000"
.LASF25:
	.ascii	"blksize\000"
.LASF31:
	.ascii	"partitions\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF36:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF12:
	.ascii	"block_size\000"
.LASF22:
	.ascii	"pindex\000"
.LASF37:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF28:
	.ascii	"plink\000"
.LASF34:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF30:
	.ascii	"OSMemPut\000"
.LASF8:
	.ascii	"UNS_8\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF38:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF14:
	.ascii	"PARTITION_DEFINITION\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF2:
	.ascii	"long long int\000"
.LASF3:
	.ascii	"char\000"
.LASF17:
	.ascii	"OSMemBlkSize\000"
.LASF41:
	.ascii	"__os_mem_create\000"
.LASF32:
	.ascii	"OSMemTbl\000"
.LASF7:
	.ascii	"short int\000"
.LASF16:
	.ascii	"OSMemFreeList\000"
.LASF9:
	.ascii	"UNS_32\000"
.LASF1:
	.ascii	"long int\000"
.LASF40:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/cs_mem_part.c\000"
.LASF5:
	.ascii	"signed char\000"
.LASF29:
	.ascii	"OSMemGet\000"
.LASF19:
	.ascii	"OSMemNFree\000"
.LASF13:
	.ascii	"block_count\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
