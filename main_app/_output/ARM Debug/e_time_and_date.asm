	.file	"e_time_and_date.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_TIME_DATE_previous_cursor_pos,"aw",%nobits
	.align	2
	.type	g_TIME_DATE_previous_cursor_pos, %object
	.size	g_TIME_DATE_previous_cursor_pos, 4
g_TIME_DATE_previous_cursor_pos:
	.space	4
	.section	.bss.g_TIME_DATE_previous_time_cursor_pos,"aw",%nobits
	.align	2
	.type	g_TIME_DATE_previous_time_cursor_pos, %object
	.size	g_TIME_DATE_previous_time_cursor_pos, 4
g_TIME_DATE_previous_time_cursor_pos:
	.space	4
	.section	.text.TIME_DATE_increment_year,"ax",%progbits
	.align	2
	.type	TIME_DATE_increment_year, %function
TIME_DATE_increment_year:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_time_and_date.c"
	.loc 1 95 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 98 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L4
	cmp	r2, r3
	bls	.L2
	.loc 1 100 0
	ldr	r3, .L4+4
	str	r3, [fp, #-4]
	b	.L3
.L2:
	.loc 1 104 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L3:
	.loc 1 107 0
	ldr	r3, [fp, #-4]
	.loc 1 108 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L5:
	.align	2
.L4:
	.word	2077
	.word	1993
.LFE0:
	.size	TIME_DATE_increment_year, .-TIME_DATE_increment_year
	.section	.text.TIME_DATE_decrement_year,"ax",%progbits
	.align	2
	.type	TIME_DATE_decrement_year, %function
TIME_DATE_decrement_year:
.LFB1:
	.loc 1 130 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 133 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L9
	cmp	r2, r3
	bhi	.L7
	.loc 1 135 0
	ldr	r3, .L9+4
	str	r3, [fp, #-4]
	b	.L8
.L7:
	.loc 1 139 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-4]
.L8:
	.loc 1 142 0
	ldr	r3, [fp, #-4]
	.loc 1 143 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L10:
	.align	2
.L9:
	.word	1993
	.word	2078
.LFE1:
	.size	TIME_DATE_decrement_year, .-TIME_DATE_decrement_year
	.section	.text.FDTO_TIME_DATE_show_time_zone_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_TIME_DATE_show_time_zone_dropdown, %function
FDTO_TIME_DATE_show_time_zone_dropdown:
.LFB2:
	.loc 1 146 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	.loc 1 147 0
	ldr	r3, .L12
	ldr	r3, [r3, #0]
	ldr	r0, .L12+4
	ldr	r1, .L12+8
	mov	r2, #8
	bl	FDTO_COMBOBOX_show
	.loc 1 148 0
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	GuiVar_DateTimeTimeZone
	.word	750
	.word	FDTO_COMBOBOX_add_items
.LFE2:
	.size	FDTO_TIME_DATE_show_time_zone_dropdown, .-FDTO_TIME_DATE_show_time_zone_dropdown
	.section	.text.TIME_DATE_get_day_of_week,"ax",%progbits
	.align	2
	.type	TIME_DATE_get_day_of_week, %function
TIME_DATE_get_day_of_week:
.LFB3:
	.loc 1 171 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	sub	sp, sp, #20
.LCFI10:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	.loc 1 176 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-20]
	ldr	r2, [fp, #-24]
	bl	DMYToDate
	str	r0, [fp, #-8]
	.loc 1 178 0
	ldr	r0, [fp, #-8]
	bl	DayOfWeek
	str	r0, [fp, #-12]
	.loc 1 180 0
	ldr	r3, [fp, #-12]
	.loc 1 181 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE3:
	.size	TIME_DATE_get_day_of_week, .-TIME_DATE_get_day_of_week
	.section	.text.TIME_DATE_process_month,"ax",%progbits
	.align	2
	.type	TIME_DATE_process_month, %function
TIME_DATE_process_month:
.LFB4:
	.loc 1 198 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	sub	sp, sp, #4
.LCFI13:
	str	r0, [fp, #-8]
	.loc 1 199 0
	bl	good_key_beep
	.loc 1 201 0
	ldr	r3, [fp, #-8]
	cmp	r3, #84
	bne	.L16
	.loc 1 203 0
	ldr	r3, .L21
	ldr	r3, [r3, #0]
	cmp	r3, #11
	bhi	.L17
	.loc 1 205 0
	ldr	r3, .L21
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L21
	str	r2, [r3, #0]
	b	.L18
.L17:
	.loc 1 209 0
	ldr	r3, .L21+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	TIME_DATE_increment_year
	mov	r2, r0
	ldr	r3, .L21+4
	str	r2, [r3, #0]
	.loc 1 210 0
	ldr	r3, .L21
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L18
.L16:
	.loc 1 213 0
	ldr	r3, [fp, #-8]
	cmp	r3, #80
	bne	.L18
	.loc 1 215 0
	ldr	r3, .L21
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bls	.L19
	.loc 1 217 0
	ldr	r3, .L21
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L21
	str	r2, [r3, #0]
	b	.L18
.L19:
	.loc 1 221 0
	ldr	r3, .L21+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	TIME_DATE_decrement_year
	mov	r2, r0
	ldr	r3, .L21+4
	str	r2, [r3, #0]
	.loc 1 222 0
	ldr	r3, .L21
	mov	r2, #12
	str	r2, [r3, #0]
.L18:
	.loc 1 227 0
	ldr	r3, .L21
	ldr	r2, [r3, #0]
	ldr	r3, .L21+4
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	NumberOfDaysInMonth
	mov	r2, r0
	ldr	r3, .L21+8
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcs	.L20
	.loc 1 232 0
	ldr	r3, .L21
	ldr	r2, [r3, #0]
	ldr	r3, .L21+4
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	NumberOfDaysInMonth
	mov	r2, r0
	ldr	r3, .L21+8
	str	r2, [r3, #0]
.L20:
	.loc 1 236 0
	ldr	r3, .L21+8
	ldr	r1, [r3, #0]
	ldr	r3, .L21
	ldr	r2, [r3, #0]
	ldr	r3, .L21+4
	ldr	r3, [r3, #0]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	TIME_DATE_get_day_of_week
	mov	r2, r0
	ldr	r3, .L21+12
	str	r2, [r3, #0]
	.loc 1 239 0
	ldr	r3, .L21+16
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 240 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L22:
	.align	2
.L21:
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeYear
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeShowButton
.LFE4:
	.size	TIME_DATE_process_month, .-TIME_DATE_process_month
	.section	.text.TIME_DATE_process_day,"ax",%progbits
	.align	2
	.type	TIME_DATE_process_day, %function
TIME_DATE_process_day:
.LFB5:
	.loc 1 257 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI14:
	add	fp, sp, #4
.LCFI15:
	sub	sp, sp, #4
.LCFI16:
	str	r0, [fp, #-8]
	.loc 1 258 0
	bl	good_key_beep
	.loc 1 260 0
	ldr	r3, [fp, #-8]
	cmp	r3, #84
	bne	.L24
	.loc 1 262 0
	ldr	r3, .L32
	ldr	r2, [r3, #0]
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	NumberOfDaysInMonth
	mov	r2, r0
	ldr	r3, .L32+8
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L25
	.loc 1 264 0
	ldr	r3, .L32+8
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L32+8
	str	r2, [r3, #0]
	b	.L26
.L25:
	.loc 1 268 0
	ldr	r3, .L32
	ldr	r3, [r3, #0]
	cmp	r3, #11
	bhi	.L27
	.loc 1 270 0
	ldr	r3, .L32
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L32
	str	r2, [r3, #0]
	b	.L28
.L27:
	.loc 1 274 0
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	TIME_DATE_increment_year
	mov	r2, r0
	ldr	r3, .L32+4
	str	r2, [r3, #0]
	.loc 1 275 0
	ldr	r3, .L32
	mov	r2, #1
	str	r2, [r3, #0]
.L28:
	.loc 1 277 0
	ldr	r3, .L32+8
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L26
.L24:
	.loc 1 280 0
	ldr	r3, [fp, #-8]
	cmp	r3, #80
	bne	.L26
	.loc 1 282 0
	ldr	r3, .L32+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bls	.L29
	.loc 1 284 0
	ldr	r3, .L32+8
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L32+8
	str	r2, [r3, #0]
	b	.L26
.L29:
	.loc 1 288 0
	ldr	r3, .L32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bls	.L30
	.loc 1 290 0
	ldr	r3, .L32
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L32
	str	r2, [r3, #0]
	b	.L31
.L30:
	.loc 1 294 0
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	TIME_DATE_decrement_year
	mov	r2, r0
	ldr	r3, .L32+4
	str	r2, [r3, #0]
	.loc 1 295 0
	ldr	r3, .L32
	mov	r2, #12
	str	r2, [r3, #0]
.L31:
	.loc 1 297 0
	ldr	r3, .L32
	ldr	r2, [r3, #0]
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	NumberOfDaysInMonth
	mov	r2, r0
	ldr	r3, .L32+8
	str	r2, [r3, #0]
.L26:
	.loc 1 302 0
	ldr	r3, .L32+8
	ldr	r1, [r3, #0]
	ldr	r3, .L32
	ldr	r2, [r3, #0]
	ldr	r3, .L32+4
	ldr	r3, [r3, #0]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	TIME_DATE_get_day_of_week
	mov	r2, r0
	ldr	r3, .L32+12
	str	r2, [r3, #0]
	.loc 1 305 0
	ldr	r3, .L32+16
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 306 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L33:
	.align	2
.L32:
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeYear
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeShowButton
.LFE5:
	.size	TIME_DATE_process_day, .-TIME_DATE_process_day
	.section	.text.TIME_DATE_process_year,"ax",%progbits
	.align	2
	.type	TIME_DATE_process_year, %function
TIME_DATE_process_year:
.LFB6:
	.loc 1 323 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI17:
	add	fp, sp, #4
.LCFI18:
	sub	sp, sp, #4
.LCFI19:
	str	r0, [fp, #-8]
	.loc 1 324 0
	bl	good_key_beep
	.loc 1 326 0
	ldr	r3, [fp, #-8]
	cmp	r3, #84
	bne	.L35
	.loc 1 328 0
	ldr	r3, .L37
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	TIME_DATE_increment_year
	mov	r2, r0
	ldr	r3, .L37
	str	r2, [r3, #0]
	b	.L36
.L35:
	.loc 1 330 0
	ldr	r3, [fp, #-8]
	cmp	r3, #80
	bne	.L36
	.loc 1 332 0
	ldr	r3, .L37
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	TIME_DATE_decrement_year
	mov	r2, r0
	ldr	r3, .L37
	str	r2, [r3, #0]
.L36:
	.loc 1 336 0
	ldr	r3, .L37+4
	ldr	r1, [r3, #0]
	ldr	r3, .L37+8
	ldr	r2, [r3, #0]
	ldr	r3, .L37
	ldr	r3, [r3, #0]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	TIME_DATE_get_day_of_week
	mov	r2, r0
	ldr	r3, .L37+12
	str	r2, [r3, #0]
	.loc 1 339 0
	ldr	r3, .L37+16
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 340 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L38:
	.align	2
.L37:
	.word	GuiVar_DateTimeYear
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeShowButton
.LFE6:
	.size	TIME_DATE_process_year, .-TIME_DATE_process_year
	.section	.text.TIME_DATE_process_hour,"ax",%progbits
	.align	2
	.type	TIME_DATE_process_hour, %function
TIME_DATE_process_hour:
.LFB7:
	.loc 1 357 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI20:
	add	fp, sp, #4
.LCFI21:
	sub	sp, sp, #16
.LCFI22:
	str	r0, [fp, #-12]
	.loc 1 361 0
	ldr	r3, .L47
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L40
	.loc 1 363 0
	ldr	r3, .L47+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L41
	.loc 1 365 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L42
.L41:
	.loc 1 369 0
	mov	r3, #12
	str	r3, [fp, #-8]
	b	.L42
.L40:
	.loc 1 372 0
	ldr	r3, .L47+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L43
	.loc 1 374 0
	ldr	r3, .L47
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L42
.L43:
	.loc 1 378 0
	ldr	r3, .L47
	ldr	r3, [r3, #0]
	add	r3, r3, #12
	str	r3, [fp, #-8]
.L42:
	.loc 1 382 0
	sub	r3, fp, #8
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	ldr	r0, [fp, #-12]
	mov	r1, r3
	mov	r2, #0
	mov	r3, #23
	bl	process_uns32
	.loc 1 385 0
	ldr	r1, [fp, #-8]
	ldr	r3, .L47+8
	umull	r2, r3, r1, r3
	mov	r2, r3, lsr #3
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	rsb	r2, r3, r1
	ldr	r3, .L47
	str	r2, [r3, #0]
	.loc 1 387 0
	ldr	r3, .L47
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L44
	.loc 1 389 0
	ldr	r3, .L47
	mov	r2, #12
	str	r2, [r3, #0]
.L44:
	.loc 1 392 0
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bhi	.L45
	.loc 1 394 0
	ldr	r3, .L47+4
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L46
.L45:
	.loc 1 398 0
	ldr	r3, .L47+4
	mov	r2, #1
	str	r2, [r3, #0]
.L46:
	.loc 1 403 0
	ldr	r3, .L47+12
	mov	r2, #55
	str	r2, [r3, #0]
	.loc 1 406 0
	ldr	r3, .L47+16
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 407 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L48:
	.align	2
.L47:
	.word	GuiVar_DateTimeHour
	.word	GuiVar_DateTimeAMPM
	.word	-1431655765
	.word	GuiVar_DateTimeSec
	.word	GuiVar_DateTimeShowButton
.LFE7:
	.size	TIME_DATE_process_hour, .-TIME_DATE_process_hour
	.section	.text.TIME_DATE_process_minutes,"ax",%progbits
	.align	2
	.type	TIME_DATE_process_minutes, %function
TIME_DATE_process_minutes:
.LFB8:
	.loc 1 426 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI23:
	add	fp, sp, #4
.LCFI24:
	sub	sp, sp, #12
.LCFI25:
	str	r0, [fp, #-8]
	.loc 1 427 0
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r3, #1
	str	r3, [sp, #4]
	ldr	r0, [fp, #-8]
	ldr	r1, .L50
	mov	r2, #0
	mov	r3, #59
	bl	process_uns32
	.loc 1 431 0
	ldr	r3, .L50+4
	mov	r2, #55
	str	r2, [r3, #0]
	.loc 1 434 0
	ldr	r3, .L50+8
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 435 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L51:
	.align	2
.L50:
	.word	GuiVar_DateTimeMin
	.word	GuiVar_DateTimeSec
	.word	GuiVar_DateTimeShowButton
.LFE8:
	.size	TIME_DATE_process_minutes, .-TIME_DATE_process_minutes
	.section	.text.TIME_DATE_process_seconds,"ax",%progbits
	.align	2
	.type	TIME_DATE_process_seconds, %function
TIME_DATE_process_seconds:
.LFB9:
	.loc 1 452 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	sub	sp, sp, #12
.LCFI28:
	str	r0, [fp, #-8]
	.loc 1 453 0
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r3, #1
	str	r3, [sp, #4]
	ldr	r0, [fp, #-8]
	ldr	r1, .L53
	mov	r2, #0
	mov	r3, #59
	bl	process_uns32
	.loc 1 456 0
	ldr	r3, .L53+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 457 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L54:
	.align	2
.L53:
	.word	GuiVar_DateTimeSec
	.word	GuiVar_DateTimeShowButton
.LFE9:
	.size	TIME_DATE_process_seconds, .-TIME_DATE_process_seconds
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_time_and_date.c\000"
	.section	.text.TIME_DATE_store_changes,"ax",%progbits
	.align	2
	.type	TIME_DATE_store_changes, %function
TIME_DATE_store_changes:
.LFB10:
	.loc 1 486 0
	@ args = 0, pretend = 0, frame = 60
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI29:
	add	fp, sp, #4
.LCFI30:
	sub	sp, sp, #68
.LCFI31:
	str	r0, [fp, #-56]
	str	r1, [fp, #-60]
	str	r2, [fp, #-64]
	.loc 1 493 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 495 0
	ldr	r3, [fp, #-56]
	cmp	r3, #1
	bne	.L56
	.loc 1 497 0
	sub	r3, fp, #32
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 499 0
	sub	r3, fp, #52
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 501 0
	ldr	r3, .L62
	ldr	r3, [r3, #0]
	and	r3, r3, #255
	strb	r3, [fp, #-34]
	.loc 1 502 0
	ldr	r3, .L62+4
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-44]	@ movhi
	.loc 1 503 0
	ldr	r3, .L62+8
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-46]	@ movhi
	.loc 1 504 0
	ldr	r3, .L62+12
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-42]	@ movhi
	.loc 1 506 0
	ldr	r3, .L62+16
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L57
	.loc 1 508 0
	ldr	r3, .L62+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L58
	.loc 1 510 0
	mov	r3, #0
	strh	r3, [fp, #-40]	@ movhi
	b	.L59
.L58:
	.loc 1 514 0
	mov	r3, #12
	strh	r3, [fp, #-40]	@ movhi
	b	.L59
.L57:
	.loc 1 517 0
	ldr	r3, .L62+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L60
	.loc 1 519 0
	ldr	r3, .L62+16
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-40]	@ movhi
	b	.L59
.L60:
	.loc 1 523 0
	ldr	r3, .L62+16
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r3, #12
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-40]	@ movhi
.L59:
	.loc 1 526 0
	ldr	r3, .L62+24
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-38]	@ movhi
	.loc 1 527 0
	ldr	r3, .L62+28
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-36]	@ movhi
	.loc 1 529 0
	ldrh	r3, [fp, #-40]
	mov	r1, r3
	ldrh	r3, [fp, #-38]
	mov	r2, r3
	ldrh	r3, [fp, #-36]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	HMSToTime
	mov	r3, r0
	str	r3, [fp, #-52]
	.loc 1 530 0
	ldr	r3, .L62+8
	ldr	r1, [r3, #0]
	ldr	r3, .L62+4
	ldr	r2, [r3, #0]
	ldr	r3, .L62+12
	ldr	r3, [r3, #0]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	DMYToDate
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-48]	@ movhi
	.loc 1 533 0
	sub	r3, fp, #52
	ldmia	r3, {r0, r1}
	ldr	r2, [fp, #-60]
	bl	EPSON_set_date_time
	mov	r3, r0
	cmp	r3, #0
	beq	.L55
	.loc 1 538 0
	ldr	r3, .L62+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L62+36
	ldr	r3, .L62+40
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 540 0
	ldr	r3, .L62+44
	add	r3, r3, #460
	sub	r2, fp, #52
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 542 0
	ldr	r3, .L62+44
	ldr	r2, [fp, #-60]
	str	r2, [r3, #468]
	.loc 1 544 0
	ldr	r3, .L62+44
	mov	r2, #1
	str	r2, [r3, #456]
	.loc 1 546 0
	ldr	r3, .L62+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L55
.L56:
	.loc 1 551 0
	mov	r0, #2
	bl	NETWORK_CONFIG_get_change_bits_ptr
	str	r0, [fp, #-12]
	.loc 1 553 0
	ldr	r3, .L62+48
	ldr	r3, [r3, #0]
	mov	r2, #1
	str	r2, [sp, #0]
	ldr	r2, [fp, #-12]
	str	r2, [sp, #4]
	mov	r0, r3
	mov	r1, #1
	ldr	r2, [fp, #-60]
	ldr	r3, [fp, #-64]
	bl	NETWORK_CONFIG_set_time_zone
.L55:
	.loc 1 555 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L63:
	.align	2
.L62:
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeYear
	.word	GuiVar_DateTimeHour
	.word	GuiVar_DateTimeAMPM
	.word	GuiVar_DateTimeMin
	.word	GuiVar_DateTimeSec
	.word	comm_mngr_recursive_MUTEX
	.word	.LC0
	.word	538
	.word	comm_mngr
	.word	GuiVar_DateTimeTimeZone
.LFE10:
	.size	TIME_DATE_store_changes, .-TIME_DATE_store_changes
	.section	.text.FDTO_TIME_DATE_update_screen,"ax",%progbits
	.align	2
	.global	FDTO_TIME_DATE_update_screen
	.type	FDTO_TIME_DATE_update_screen, %function
FDTO_TIME_DATE_update_screen:
.LFB11:
	.loc 1 571 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI32:
	add	fp, sp, #4
.LCFI33:
	sub	sp, sp, #20
.LCFI34:
	.loc 1 575 0
	ldr	r3, .L69
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L64
	.loc 1 577 0
	sub	r3, fp, #24
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 579 0
	ldrb	r3, [fp, #-6]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L69+4
	str	r2, [r3, #0]
	.loc 1 580 0
	ldrh	r3, [fp, #-16]
	mov	r2, r3
	ldr	r3, .L69+8
	str	r2, [r3, #0]
	.loc 1 581 0
	ldrh	r3, [fp, #-18]
	mov	r2, r3
	ldr	r3, .L69+12
	str	r2, [r3, #0]
	.loc 1 582 0
	ldrh	r3, [fp, #-14]
	mov	r2, r3
	ldr	r3, .L69+16
	str	r2, [r3, #0]
	.loc 1 583 0
	ldrh	r2, [fp, #-12]
	ldr	r3, .L69+20
	umull	r1, r3, r2, r3
	mov	r1, r3, lsr #3
	mov	r3, r1
	mov	r3, r3, asl #1
	add	r3, r3, r1
	mov	r3, r3, asl #2
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3
	ldr	r3, .L69+24
	str	r2, [r3, #0]
	.loc 1 585 0
	ldr	r3, .L69+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L66
	.loc 1 587 0
	ldr	r3, .L69+24
	mov	r2, #12
	str	r2, [r3, #0]
.L66:
	.loc 1 590 0
	ldrh	r3, [fp, #-10]
	mov	r2, r3
	ldr	r3, .L69+28
	str	r2, [r3, #0]
	.loc 1 591 0
	ldrh	r3, [fp, #-8]
	mov	r2, r3
	ldr	r3, .L69+32
	str	r2, [r3, #0]
	.loc 1 593 0
	ldrh	r3, [fp, #-12]
	cmp	r3, #11
	bhi	.L67
	.loc 1 595 0
	ldr	r3, .L69+36
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L68
.L67:
	.loc 1 599 0
	ldr	r3, .L69+36
	mov	r2, #1
	str	r2, [r3, #0]
.L68:
	.loc 1 602 0
	bl	GuiLib_Refresh
.L64:
	.loc 1 604 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L70:
	.align	2
.L69:
	.word	GuiVar_DateTimeShowButton
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeYear
	.word	-1431655765
	.word	GuiVar_DateTimeHour
	.word	GuiVar_DateTimeMin
	.word	GuiVar_DateTimeSec
	.word	GuiVar_DateTimeAMPM
.LFE11:
	.size	FDTO_TIME_DATE_update_screen, .-FDTO_TIME_DATE_update_screen
	.section	.text.TIME_DATE_copy_settings_into_guivars,"ax",%progbits
	.align	2
	.type	TIME_DATE_copy_settings_into_guivars, %function
TIME_DATE_copy_settings_into_guivars:
.LFB12:
	.loc 1 621 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI35:
	add	fp, sp, #4
.LCFI36:
	sub	sp, sp, #20
.LCFI37:
	.loc 1 624 0
	ldr	r3, .L75
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 625 0
	ldr	r3, .L75+4
	mov	r2, #3
	str	r2, [r3, #0]
	.loc 1 627 0
	sub	r3, fp, #24
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 629 0
	ldrb	r3, [fp, #-6]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L75+8
	str	r2, [r3, #0]
	.loc 1 630 0
	ldrh	r3, [fp, #-16]
	mov	r2, r3
	ldr	r3, .L75+12
	str	r2, [r3, #0]
	.loc 1 631 0
	ldrh	r3, [fp, #-18]
	mov	r2, r3
	ldr	r3, .L75+16
	str	r2, [r3, #0]
	.loc 1 632 0
	ldrh	r3, [fp, #-14]
	mov	r2, r3
	ldr	r3, .L75+20
	str	r2, [r3, #0]
	.loc 1 633 0
	ldrh	r2, [fp, #-12]
	ldr	r3, .L75+24
	umull	r1, r3, r2, r3
	mov	r1, r3, lsr #3
	mov	r3, r1
	mov	r3, r3, asl #1
	add	r3, r3, r1
	mov	r3, r3, asl #2
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3
	ldr	r3, .L75+28
	str	r2, [r3, #0]
	.loc 1 635 0
	ldr	r3, .L75+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L72
	.loc 1 637 0
	ldr	r3, .L75+28
	mov	r2, #12
	str	r2, [r3, #0]
.L72:
	.loc 1 640 0
	ldrh	r3, [fp, #-10]
	mov	r2, r3
	ldr	r3, .L75+32
	str	r2, [r3, #0]
	.loc 1 641 0
	ldrh	r3, [fp, #-8]
	mov	r2, r3
	ldr	r3, .L75+36
	str	r2, [r3, #0]
	.loc 1 643 0
	ldrh	r3, [fp, #-12]
	cmp	r3, #11
	bhi	.L73
	.loc 1 645 0
	ldr	r3, .L75+40
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L74
.L73:
	.loc 1 649 0
	ldr	r3, .L75+40
	mov	r2, #1
	str	r2, [r3, #0]
.L74:
	.loc 1 652 0
	bl	NETWORK_CONFIG_get_time_zone
	mov	r2, r0
	ldr	r3, .L75+44
	str	r2, [r3, #0]
	.loc 1 654 0
	ldr	r3, .L75+48
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 655 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L76:
	.align	2
.L75:
	.word	g_TIME_DATE_previous_cursor_pos
	.word	g_TIME_DATE_previous_time_cursor_pos
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeYear
	.word	-1431655765
	.word	GuiVar_DateTimeHour
	.word	GuiVar_DateTimeMin
	.word	GuiVar_DateTimeSec
	.word	GuiVar_DateTimeAMPM
	.word	GuiVar_DateTimeTimeZone
	.word	GuiVar_DateTimeShowButton
.LFE12:
	.size	TIME_DATE_copy_settings_into_guivars, .-TIME_DATE_copy_settings_into_guivars
	.section	.text.FDTO_TIME_DATE_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_TIME_DATE_draw_screen
	.type	FDTO_TIME_DATE_draw_screen, %function
FDTO_TIME_DATE_draw_screen:
.LFB13:
	.loc 1 659 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI38:
	add	fp, sp, #4
.LCFI39:
	sub	sp, sp, #8
.LCFI40:
	str	r0, [fp, #-12]
	.loc 1 662 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L78
	.loc 1 664 0
	bl	TIME_DATE_copy_settings_into_guivars
	.loc 1 666 0
	mov	r3, #7
	str	r3, [fp, #-8]
	b	.L79
.L78:
	.loc 1 670 0
	ldr	r3, .L80
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L79:
	.loc 1 673 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #15
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 674 0
	bl	GuiLib_Refresh
	.loc 1 675 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L81:
	.align	2
.L80:
	.word	GuiLib_ActiveCursorFieldNo
.LFE13:
	.size	FDTO_TIME_DATE_draw_screen, .-FDTO_TIME_DATE_draw_screen
	.section	.text.TIME_DATE_process_screen,"ax",%progbits
	.align	2
	.global	TIME_DATE_process_screen
	.type	TIME_DATE_process_screen, %function
TIME_DATE_process_screen:
.LFB14:
	.loc 1 693 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI41:
	add	fp, sp, #4
.LCFI42:
	sub	sp, sp, #56
.LCFI43:
	str	r0, [fp, #-52]
	str	r1, [fp, #-48]
	.loc 1 698 0
	ldr	r3, .L139
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L139+4
	cmp	r2, r3
	bne	.L134
.L84:
	.loc 1 701 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	ldr	r1, .L139+8
	bl	COMBO_BOX_key_press
	.loc 1 702 0
	b	.L82
.L134:
	.loc 1 705 0
	ldr	r3, [fp, #-52]
	cmp	r3, #3
	beq	.L90
	cmp	r3, #3
	bhi	.L93
	cmp	r3, #1
	beq	.L88
	cmp	r3, #1
	bhi	.L89
	b	.L135
.L93:
	cmp	r3, #80
	beq	.L92
	cmp	r3, #84
	beq	.L92
	cmp	r3, #4
	beq	.L91
	b	.L136
.L89:
	.loc 1 708 0
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #6
	beq	.L95
	cmp	r3, #7
	beq	.L96
	b	.L137
.L95:
	.loc 1 711 0
	bl	good_key_beep
	.loc 1 713 0
	bl	FLOWSENSE_get_controller_index
	mov	r3, r0
	mov	r0, #1
	mov	r1, #2
	mov	r2, r3
	bl	TIME_DATE_store_changes
	.loc 1 716 0
	ldr	r3, .L139+16
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 718 0
	ldr	r3, .L139+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	bl	CURSOR_Select
	.loc 1 720 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 721 0
	b	.L97
.L96:
	.loc 1 724 0
	bl	good_key_beep
	.loc 1 725 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 726 0
	ldr	r3, .L139+24
	str	r3, [fp, #-24]
	.loc 1 727 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 728 0
	b	.L97
.L137:
	.loc 1 731 0
	bl	bad_key_beep
	.loc 1 734 0
	b	.L82
.L97:
	b	.L82
.L92:
	.loc 1 738 0
	ldr	r3, .L139+16
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 740 0
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L98
.L106:
	.word	.L99
	.word	.L100
	.word	.L101
	.word	.L102
	.word	.L103
	.word	.L104
	.word	.L98
	.word	.L105
.L99:
	.loc 1 743 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	TIME_DATE_process_month
	.loc 1 744 0
	b	.L107
.L100:
	.loc 1 747 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	TIME_DATE_process_day
	.loc 1 748 0
	b	.L107
.L101:
	.loc 1 751 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	TIME_DATE_process_year
	.loc 1 752 0
	b	.L107
.L102:
	.loc 1 755 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	TIME_DATE_process_hour
	.loc 1 756 0
	b	.L107
.L103:
	.loc 1 759 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	TIME_DATE_process_minutes
	.loc 1 760 0
	b	.L107
.L104:
	.loc 1 763 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	TIME_DATE_process_seconds
	.loc 1 764 0
	b	.L107
.L105:
	.loc 1 767 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L139+8
	mov	r2, #0
	mov	r3, #7
	bl	process_uns32
	.loc 1 768 0
	b	.L107
.L98:
	.loc 1 771 0
	bl	bad_key_beep
.L107:
	.loc 1 774 0
	ldr	r3, .L139+16
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	bne	.L108
	.loc 1 774 0 is_stmt 0 discriminator 1
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #7
	bne	.L109
.L108:
	.loc 1 776 0 is_stmt 1
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 782 0
	b	.L82
.L109:
	.loc 1 780 0
	bl	Refresh_Screen
	.loc 1 782 0
	b	.L82
.L91:
	.loc 1 785 0
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L111
.L116:
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L113
	.word	.L113
	.word	.L113
	.word	.L114
	.word	.L115
.L112:
	.loc 1 790 0
	bl	bad_key_beep
	.loc 1 791 0
	b	.L117
.L113:
	.loc 1 796 0
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L139+20
	str	r2, [r3, #0]
	.loc 1 798 0
	ldr	r3, .L139+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 799 0
	b	.L117
.L114:
	.loc 1 802 0
	ldr	r3, .L139+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 803 0
	b	.L117
.L115:
	.loc 1 806 0
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	mov	r3, r0
	cmp	r3, #0
	beq	.L118
	.loc 1 808 0
	ldr	r3, .L139+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L119
	.loc 1 810 0
	mov	r0, #6
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 823 0
	b	.L117
.L119:
	.loc 1 814 0
	ldr	r3, .L139+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 823 0
	b	.L117
.L118:
	.loc 1 821 0
	bl	bad_key_beep
	.loc 1 823 0
	b	.L117
.L111:
	.loc 1 826 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 827 0
	mov	r0, r0	@ nop
.L117:
	.loc 1 829 0
	b	.L82
.L135:
	.loc 1 832 0
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L121
.L124:
	.word	.L122
	.word	.L122
	.word	.L122
	.word	.L123
	.word	.L123
	.word	.L123
.L122:
	.loc 1 837 0
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L139+28
	str	r2, [r3, #0]
	.loc 1 839 0
	ldr	r3, .L139+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 840 0
	b	.L125
.L123:
	.loc 1 845 0
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L139+20
	str	r2, [r3, #0]
	.loc 1 847 0
	ldr	r3, .L139+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L126
	.loc 1 849 0
	mov	r0, #6
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 855 0
	b	.L125
.L126:
	.loc 1 853 0
	mov	r0, #7
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 855 0
	b	.L125
.L121:
	.loc 1 858 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 860 0
	b	.L82
.L125:
	b	.L82
.L88:
	.loc 1 863 0
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	mov	r3, r0
	cmp	r3, #0
	beq	.L128
	.loc 1 865 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 873 0
	b	.L82
.L128:
	.loc 1 871 0
	bl	bad_key_beep
	.loc 1 873 0
	b	.L82
.L90:
	.loc 1 876 0
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #5
	bne	.L138
.L131:
	.loc 1 879 0
	ldr	r3, .L139+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L139+20
	str	r2, [r3, #0]
	.loc 1 880 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 881 0
	mov	r0, r0	@ nop
	.loc 1 886 0
	b	.L82
.L138:
	.loc 1 884 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 886 0
	b	.L82
.L136:
	.loc 1 889 0
	ldr	r3, [fp, #-52]
	cmp	r3, #67
	bne	.L133
	.loc 1 891 0
	bl	FLOWSENSE_get_controller_index
	mov	r3, r0
	mov	r0, #0
	mov	r1, #2
	mov	r2, r3
	bl	TIME_DATE_store_changes
	.loc 1 893 0
	ldr	r3, .L139+32
	mov	r2, #11
	str	r2, [r3, #0]
.L133:
	.loc 1 896 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L82:
	.loc 1 899 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L140:
	.align	2
.L139:
	.word	GuiLib_CurStructureNdx
	.word	750
	.word	GuiVar_DateTimeTimeZone
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_DateTimeShowButton
	.word	g_TIME_DATE_previous_time_cursor_pos
	.word	FDTO_TIME_DATE_show_time_zone_dropdown
	.word	g_TIME_DATE_previous_cursor_pos
	.word	GuiVar_MenuScreenToShow
.LFE14:
	.size	TIME_DATE_process_screen, .-TIME_DATE_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI14-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI17-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI20-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI23-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI26-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI29-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI32-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI35-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI38-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI41-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI42-.LCFI41
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/comm_mngr.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xbbb
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF152
	.byte	0x1
	.4byte	.LASF153
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x62
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x74
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x74
	.uleb128 0x5
	.byte	0x4
	.4byte	0xa1
	.uleb128 0x6
	.4byte	0xa8
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x35
	.4byte	0xa8
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x4
	.byte	0x57
	.4byte	0xaf
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x5
	.byte	0x4c
	.4byte	0xc3
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x6
	.byte	0x65
	.4byte	0xaf
	.uleb128 0x9
	.4byte	0x37
	.4byte	0xf4
	.uleb128 0xa
	.4byte	0xa8
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x7
	.byte	0x7c
	.4byte	0x119
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x7
	.byte	0x7e
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x7
	.byte	0x80
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x7
	.byte	0x82
	.4byte	0xf4
	.uleb128 0xb
	.byte	0x6
	.byte	0x8
	.byte	0x22
	.4byte	0x145
	.uleb128 0xd
	.ascii	"T\000"
	.byte	0x8
	.byte	0x24
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"D\000"
	.byte	0x8
	.byte	0x26
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF22
	.byte	0x8
	.byte	0x28
	.4byte	0x124
	.uleb128 0xb
	.byte	0x14
	.byte	0x8
	.byte	0x31
	.4byte	0x1d7
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x8
	.byte	0x33
	.4byte	0x145
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x8
	.byte	0x35
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x8
	.byte	0x35
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x8
	.byte	0x35
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x8
	.byte	0x37
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x8
	.byte	0x37
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x8
	.byte	0x37
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x8
	.byte	0x39
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x8
	.byte	0x3b
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.4byte	.LASF32
	.byte	0x8
	.byte	0x3d
	.4byte	0x150
	.uleb128 0xb
	.byte	0x24
	.byte	0x9
	.byte	0x78
	.4byte	0x269
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x9
	.byte	0x7b
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF34
	.byte	0x9
	.byte	0x83
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF35
	.byte	0x9
	.byte	0x86
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF36
	.byte	0x9
	.byte	0x88
	.4byte	0x27a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF37
	.byte	0x9
	.byte	0x8d
	.4byte	0x28c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF38
	.byte	0x9
	.byte	0x92
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF39
	.byte	0x9
	.byte	0x96
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF40
	.byte	0x9
	.byte	0x9a
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF41
	.byte	0x9
	.byte	0x9c
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	0x275
	.uleb128 0xf
	.4byte	0x275
	.byte	0
	.uleb128 0x10
	.4byte	0x57
	.uleb128 0x5
	.byte	0x4
	.4byte	0x269
	.uleb128 0xe
	.byte	0x1
	.4byte	0x28c
	.uleb128 0xf
	.4byte	0x119
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x280
	.uleb128 0x3
	.4byte	.LASF42
	.byte	0x9
	.byte	0x9e
	.4byte	0x1e2
	.uleb128 0xb
	.byte	0x14
	.byte	0xa
	.byte	0x18
	.4byte	0x2ec
	.uleb128 0xc
	.4byte	.LASF43
	.byte	0xa
	.byte	0x1a
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF44
	.byte	0xa
	.byte	0x1c
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF45
	.byte	0xa
	.byte	0x1e
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF46
	.byte	0xa
	.byte	0x20
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF47
	.byte	0xa
	.byte	0x23
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF48
	.byte	0xa
	.byte	0x26
	.4byte	0x29d
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF49
	.uleb128 0x9
	.4byte	0x69
	.4byte	0x30e
	.uleb128 0xa
	.4byte	0xa8
	.byte	0xb
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0xb
	.byte	0xe7
	.4byte	0x333
	.uleb128 0xc
	.4byte	.LASF50
	.byte	0xb
	.byte	0xf6
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF51
	.byte	0xb
	.byte	0xfe
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0xb
	.2byte	0x100
	.4byte	0x30e
	.uleb128 0x12
	.byte	0xc
	.byte	0xb
	.2byte	0x105
	.4byte	0x366
	.uleb128 0x13
	.ascii	"dt\000"
	.byte	0xb
	.2byte	0x107
	.4byte	0x145
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF53
	.byte	0xb
	.2byte	0x108
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x11
	.4byte	.LASF54
	.byte	0xb
	.2byte	0x109
	.4byte	0x33f
	.uleb128 0x15
	.2byte	0x1e4
	.byte	0xb
	.2byte	0x10d
	.4byte	0x630
	.uleb128 0x14
	.4byte	.LASF55
	.byte	0xb
	.2byte	0x112
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.4byte	.LASF56
	.byte	0xb
	.2byte	0x116
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x14
	.4byte	.LASF57
	.byte	0xb
	.2byte	0x11f
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x14
	.4byte	.LASF58
	.byte	0xb
	.2byte	0x126
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x14
	.4byte	.LASF59
	.byte	0xb
	.2byte	0x12a
	.4byte	0xd9
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x14
	.4byte	.LASF60
	.byte	0xb
	.2byte	0x12e
	.4byte	0xd9
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x14
	.4byte	.LASF61
	.byte	0xb
	.2byte	0x133
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x14
	.4byte	.LASF62
	.byte	0xb
	.2byte	0x138
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x14
	.4byte	.LASF63
	.byte	0xb
	.2byte	0x13c
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x14
	.4byte	.LASF64
	.byte	0xb
	.2byte	0x143
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x14
	.4byte	.LASF65
	.byte	0xb
	.2byte	0x14c
	.4byte	0x630
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.4byte	.LASF66
	.byte	0xb
	.2byte	0x156
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x14
	.4byte	.LASF67
	.byte	0xb
	.2byte	0x158
	.4byte	0x2fe
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x14
	.4byte	.LASF68
	.byte	0xb
	.2byte	0x15a
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x14
	.4byte	.LASF69
	.byte	0xb
	.2byte	0x15c
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x14
	.4byte	.LASF70
	.byte	0xb
	.2byte	0x174
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x14
	.4byte	.LASF71
	.byte	0xb
	.2byte	0x176
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x14
	.4byte	.LASF72
	.byte	0xb
	.2byte	0x180
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x14
	.4byte	.LASF73
	.byte	0xb
	.2byte	0x182
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x14
	.4byte	.LASF74
	.byte	0xb
	.2byte	0x186
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x14
	.4byte	.LASF75
	.byte	0xb
	.2byte	0x195
	.4byte	0x2fe
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x14
	.4byte	.LASF76
	.byte	0xb
	.2byte	0x197
	.4byte	0x2fe
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0x14
	.4byte	.LASF77
	.byte	0xb
	.2byte	0x19b
	.4byte	0x630
	.byte	0x3
	.byte	0x23
	.uleb128 0x108
	.uleb128 0x14
	.4byte	.LASF78
	.byte	0xb
	.2byte	0x19d
	.4byte	0x630
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.uleb128 0x14
	.4byte	.LASF79
	.byte	0xb
	.2byte	0x1a2
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x168
	.uleb128 0x14
	.4byte	.LASF80
	.byte	0xb
	.2byte	0x1a9
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x16c
	.uleb128 0x14
	.4byte	.LASF81
	.byte	0xb
	.2byte	0x1ab
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x170
	.uleb128 0x14
	.4byte	.LASF82
	.byte	0xb
	.2byte	0x1ad
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x174
	.uleb128 0x14
	.4byte	.LASF83
	.byte	0xb
	.2byte	0x1af
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x178
	.uleb128 0x14
	.4byte	.LASF84
	.byte	0xb
	.2byte	0x1b5
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x17c
	.uleb128 0x14
	.4byte	.LASF85
	.byte	0xb
	.2byte	0x1b7
	.4byte	0xd9
	.byte	0x3
	.byte	0x23
	.uleb128 0x180
	.uleb128 0x14
	.4byte	.LASF86
	.byte	0xb
	.2byte	0x1be
	.4byte	0xd9
	.byte	0x3
	.byte	0x23
	.uleb128 0x184
	.uleb128 0x14
	.4byte	.LASF87
	.byte	0xb
	.2byte	0x1c0
	.4byte	0xd9
	.byte	0x3
	.byte	0x23
	.uleb128 0x188
	.uleb128 0x14
	.4byte	.LASF88
	.byte	0xb
	.2byte	0x1c4
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x18c
	.uleb128 0x14
	.4byte	.LASF89
	.byte	0xb
	.2byte	0x1c6
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x190
	.uleb128 0x14
	.4byte	.LASF90
	.byte	0xb
	.2byte	0x1cc
	.4byte	0x2ec
	.byte	0x3
	.byte	0x23
	.uleb128 0x194
	.uleb128 0x14
	.4byte	.LASF91
	.byte	0xb
	.2byte	0x1d0
	.4byte	0x2ec
	.byte	0x3
	.byte	0x23
	.uleb128 0x1a8
	.uleb128 0x14
	.4byte	.LASF92
	.byte	0xb
	.2byte	0x1d6
	.4byte	0x333
	.byte	0x3
	.byte	0x23
	.uleb128 0x1bc
	.uleb128 0x14
	.4byte	.LASF93
	.byte	0xb
	.2byte	0x1dc
	.4byte	0xd9
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c4
	.uleb128 0x14
	.4byte	.LASF94
	.byte	0xb
	.2byte	0x1e2
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c8
	.uleb128 0x14
	.4byte	.LASF95
	.byte	0xb
	.2byte	0x1e5
	.4byte	0x366
	.byte	0x3
	.byte	0x23
	.uleb128 0x1cc
	.uleb128 0x14
	.4byte	.LASF96
	.byte	0xb
	.2byte	0x1eb
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d8
	.uleb128 0x14
	.4byte	.LASF97
	.byte	0xb
	.2byte	0x1f2
	.4byte	0xd9
	.byte	0x3
	.byte	0x23
	.uleb128 0x1dc
	.uleb128 0x14
	.4byte	.LASF98
	.byte	0xb
	.2byte	0x1f4
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x1e0
	.byte	0
	.uleb128 0x9
	.4byte	0x90
	.4byte	0x640
	.uleb128 0xa
	.4byte	0xa8
	.byte	0xb
	.byte	0
	.uleb128 0x11
	.4byte	.LASF99
	.byte	0xb
	.2byte	0x1f6
	.4byte	0x372
	.uleb128 0x16
	.4byte	.LASF100
	.byte	0x1
	.byte	0x5e
	.byte	0x1
	.4byte	0x69
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x684
	.uleb128 0x17
	.4byte	.LASF102
	.byte	0x1
	.byte	0x5e
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x60
	.4byte	0x69
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x10
	.4byte	0x69
	.uleb128 0x16
	.4byte	.LASF101
	.byte	0x1
	.byte	0x81
	.byte	0x1
	.4byte	0x69
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x6c1
	.uleb128 0x17
	.4byte	.LASF102
	.byte	0x1
	.byte	0x81
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x83
	.4byte	0x69
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x19
	.4byte	.LASF154
	.byte	0x1
	.byte	0x91
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x16
	.4byte	.LASF103
	.byte	0x1
	.byte	0xaa
	.byte	0x1
	.4byte	0x69
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x737
	.uleb128 0x17
	.4byte	.LASF104
	.byte	0x1
	.byte	0xaa
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF105
	.byte	0x1
	.byte	0xaa
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x17
	.4byte	.LASF102
	.byte	0x1
	.byte	0xaa
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xac
	.4byte	0x69
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1a
	.4byte	.LASF106
	.byte	0x1
	.byte	0xae
	.4byte	0x69
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF108
	.byte	0x1
	.byte	0xc5
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x75e
	.uleb128 0x17
	.4byte	.LASF107
	.byte	0x1
	.byte	0xc5
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x100
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x787
	.uleb128 0x1d
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x100
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x142
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x7b0
	.uleb128 0x1d
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x142
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x164
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x7e8
	.uleb128 0x1d
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x164
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1e
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x166
	.4byte	0x69
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x1a9
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x811
	.uleb128 0x1d
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x1a9
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x1c3
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x83a
	.uleb128 0x1d
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x1c3
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x1e3
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x8be
	.uleb128 0x1d
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x1e3
	.4byte	0x8be
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x1d
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x1e4
	.4byte	0x684
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x1d
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x1e5
	.4byte	0x684
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x1e
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x1e7
	.4byte	0x8c3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1e
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x1e9
	.4byte	0x1d7
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1e
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x1e9
	.4byte	0x1d7
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x1e
	.4byte	.LASF122
	.byte	0x1
	.2byte	0x1eb
	.4byte	0x69
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.4byte	0x90
	.uleb128 0x5
	.byte	0x4
	.4byte	0x69
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF124
	.byte	0x1
	.2byte	0x23a
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x8f3
	.uleb128 0x20
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x23c
	.4byte	0x1d7
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF123
	.byte	0x1
	.2byte	0x26c
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x91c
	.uleb128 0x20
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x26e
	.4byte	0x1d7
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF125
	.byte	0x1
	.2byte	0x292
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x955
	.uleb128 0x1d
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x292
	.4byte	0x8be
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1e
	.4byte	.LASF127
	.byte	0x1
	.2byte	0x294
	.4byte	0x69
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF128
	.byte	0x1
	.2byte	0x2b4
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x99d
	.uleb128 0x1d
	.4byte	.LASF129
	.byte	0x1
	.2byte	0x2b4
	.4byte	0x99d
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x20
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x2b6
	.4byte	0x292
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1e
	.4byte	.LASF130
	.byte	0x1
	.2byte	0x2b8
	.4byte	0x90
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.4byte	0x119
	.uleb128 0x21
	.4byte	.LASF131
	.byte	0xc
	.2byte	0x183
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF132
	.byte	0xc
	.2byte	0x184
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF133
	.byte	0xc
	.2byte	0x185
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF134
	.byte	0xc
	.2byte	0x187
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF135
	.byte	0xc
	.2byte	0x188
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF136
	.byte	0xc
	.2byte	0x189
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF137
	.byte	0xc
	.2byte	0x18a
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF138
	.byte	0xc
	.2byte	0x18b
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF139
	.byte	0xc
	.2byte	0x18c
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF140
	.byte	0xc
	.2byte	0x18d
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF141
	.byte	0xc
	.2byte	0x2ec
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF142
	.byte	0xd
	.2byte	0x127
	.4byte	0x62
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF143
	.byte	0xd
	.2byte	0x132
	.4byte	0x62
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF144
	.byte	0xe
	.byte	0x30
	.4byte	0xa69
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x10
	.4byte	0xe4
	.uleb128 0x1a
	.4byte	.LASF145
	.byte	0xe
	.byte	0x34
	.4byte	0xa7f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x10
	.4byte	0xe4
	.uleb128 0x1a
	.4byte	.LASF146
	.byte	0xe
	.byte	0x36
	.4byte	0xa95
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x10
	.4byte	0xe4
	.uleb128 0x1a
	.4byte	.LASF147
	.byte	0xe
	.byte	0x38
	.4byte	0xaab
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x10
	.4byte	0xe4
	.uleb128 0x21
	.4byte	.LASF148
	.byte	0xb
	.2byte	0x20c
	.4byte	0x640
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF149
	.byte	0xf
	.byte	0x9f
	.4byte	0xce
	.byte	0x1
	.byte	0x1
	.uleb128 0x1a
	.4byte	.LASF150
	.byte	0x1
	.byte	0x3d
	.4byte	0x69
	.byte	0x5
	.byte	0x3
	.4byte	g_TIME_DATE_previous_cursor_pos
	.uleb128 0x1a
	.4byte	.LASF151
	.byte	0x1
	.byte	0x46
	.4byte	0x69
	.byte	0x5
	.byte	0x3
	.4byte	g_TIME_DATE_previous_time_cursor_pos
	.uleb128 0x21
	.4byte	.LASF131
	.byte	0xc
	.2byte	0x183
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF132
	.byte	0xc
	.2byte	0x184
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF133
	.byte	0xc
	.2byte	0x185
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF134
	.byte	0xc
	.2byte	0x187
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF135
	.byte	0xc
	.2byte	0x188
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF136
	.byte	0xc
	.2byte	0x189
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF137
	.byte	0xc
	.2byte	0x18a
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF138
	.byte	0xc
	.2byte	0x18b
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF139
	.byte	0xc
	.2byte	0x18c
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF140
	.byte	0xc
	.2byte	0x18d
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF141
	.byte	0xc
	.2byte	0x2ec
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF142
	.byte	0xd
	.2byte	0x127
	.4byte	0x62
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF143
	.byte	0xd
	.2byte	0x132
	.4byte	0x62
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF148
	.byte	0xb
	.2byte	0x20c
	.4byte	0x640
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF149
	.byte	0xf
	.byte	0x9f
	.4byte	0xce
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI18
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI21
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI24
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI33
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI36
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI39
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI42
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"UNS_16\000"
.LASF45:
	.ascii	"count\000"
.LASF75:
	.ascii	"failures_to_respond_to_the_token\000"
.LASF153:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_time_and_date.c\000"
.LASF35:
	.ascii	"_03_structure_to_draw\000"
.LASF51:
	.ascii	"send_changes_to_master\000"
.LASF25:
	.ascii	"__month\000"
.LASF34:
	.ascii	"_02_menu\000"
.LASF64:
	.ascii	"start_a_scan_captured_reason\000"
.LASF146:
	.ascii	"GuiFont_DecimalChar\000"
.LASF104:
	.ascii	"pday\000"
.LASF116:
	.ascii	"pchange_date_and_time_bool\000"
.LASF94:
	.ascii	"flag_update_date_time\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF151:
	.ascii	"g_TIME_DATE_previous_time_cursor_pos\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF43:
	.ascii	"phead\000"
.LASF111:
	.ascii	"TIME_DATE_process_hour\000"
.LASF109:
	.ascii	"TIME_DATE_process_day\000"
.LASF71:
	.ascii	"flag_myself_as_NEW_in_the_scan_RESP\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF19:
	.ascii	"keycode\000"
.LASF22:
	.ascii	"DATE_TIME\000"
.LASF11:
	.ascii	"long long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF70:
	.ascii	"request_a_scan_in_the_next_token_RESP\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF82:
	.ascii	"device_exchange_state\000"
.LASF120:
	.ascii	"lcurrent_dt\000"
.LASF88:
	.ascii	"token_rate_timer_has_timed_out\000"
.LASF47:
	.ascii	"InUse\000"
.LASF50:
	.ascii	"distribute_changes_to_slave\000"
.LASF56:
	.ascii	"state\000"
.LASF14:
	.ascii	"long int\000"
.LASF105:
	.ascii	"pmonth\000"
.LASF69:
	.ascii	"since_the_scan__slave__number_of_clean_token_respon"
	.ascii	"ses_made\000"
.LASF140:
	.ascii	"GuiVar_DateTimeYear\000"
.LASF86:
	.ascii	"timer_message_resp\000"
.LASF42:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF129:
	.ascii	"pkey_event\000"
.LASF7:
	.ascii	"short int\000"
.LASF28:
	.ascii	"__minutes\000"
.LASF79:
	.ascii	"broadcast_chain_members_array\000"
.LASF15:
	.ascii	"portTickType\000"
.LASF48:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF32:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF106:
	.ascii	"ldate\000"
.LASF147:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF149:
	.ascii	"comm_mngr_recursive_MUTEX\000"
.LASF97:
	.ascii	"flowsense_device_establish_connection_timer\000"
.LASF122:
	.ascii	"lnumber_of_changes\000"
.LASF67:
	.ascii	"since_the_scan__master__number_of_clean_token_respo"
	.ascii	"nses_rcvd\000"
.LASF130:
	.ascii	"lshow_set_button\000"
.LASF33:
	.ascii	"_01_command\000"
.LASF55:
	.ascii	"mode\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF141:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF99:
	.ascii	"COMM_MNGR_STRUCT\000"
.LASF13:
	.ascii	"long unsigned int\000"
.LASF90:
	.ascii	"packets_waiting_for_token\000"
.LASF37:
	.ascii	"key_process_func_ptr\000"
.LASF138:
	.ascii	"GuiVar_DateTimeShowButton\000"
.LASF115:
	.ascii	"TIME_DATE_store_changes\000"
.LASF85:
	.ascii	"timer_device_exchange\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF152:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF31:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF39:
	.ascii	"_06_u32_argument1\000"
.LASF57:
	.ascii	"chain_is_down\000"
.LASF110:
	.ascii	"TIME_DATE_process_year\000"
.LASF23:
	.ascii	"date_time\000"
.LASF76:
	.ascii	"failures_to_respond_to_the_token_today\000"
.LASF119:
	.ascii	"lchange_bitfield_to_set\000"
.LASF36:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF112:
	.ascii	"lhour\000"
.LASF107:
	.ascii	"pkeycode\000"
.LASF96:
	.ascii	"perform_two_wire_discovery\000"
.LASF16:
	.ascii	"xQueueHandle\000"
.LASF145:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF41:
	.ascii	"_08_screen_to_draw\000"
.LASF18:
	.ascii	"xTimerHandle\000"
.LASF61:
	.ascii	"scans_while_chain_is_down\000"
.LASF81:
	.ascii	"device_exchange_port\000"
.LASF87:
	.ascii	"timer_token_rate_timer\000"
.LASF133:
	.ascii	"GuiVar_DateTimeDayOfWeek\000"
.LASF83:
	.ascii	"device_exchange_device_index\000"
.LASF124:
	.ascii	"FDTO_TIME_DATE_update_screen\000"
.LASF101:
	.ascii	"TIME_DATE_decrement_year\000"
.LASF68:
	.ascii	"since_the_scan__slave__number_of_clean_tokens_rcvd\000"
.LASF58:
	.ascii	"flag_chain_down_at_the_next_opportunity\000"
.LASF30:
	.ascii	"__dayofweek\000"
.LASF21:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF95:
	.ascii	"token_date_time\000"
.LASF84:
	.ascii	"device_exchange_saved_comm_mngr_mode\000"
.LASF44:
	.ascii	"ptail\000"
.LASF72:
	.ascii	"i_am_a_slave_and_i_have_rebooted\000"
.LASF74:
	.ascii	"pending_device_exchange_request\000"
.LASF100:
	.ascii	"TIME_DATE_increment_year\000"
.LASF127:
	.ascii	"lcursor_to_select\000"
.LASF49:
	.ascii	"float\000"
.LASF144:
	.ascii	"GuiFont_LanguageActive\000"
.LASF78:
	.ascii	"tpmicro_firmware_up_to_date\000"
.LASF24:
	.ascii	"__day\000"
.LASF73:
	.ascii	"i_am_the_master_and_i_have_rebooted\000"
.LASF93:
	.ascii	"timer_commserver_msg_receipt_error\000"
.LASF142:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF143:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF108:
	.ascii	"TIME_DATE_process_month\000"
.LASF128:
	.ascii	"TIME_DATE_process_screen\000"
.LASF117:
	.ascii	"preason_for_change\000"
.LASF54:
	.ascii	"DATE_TIME_TOKEN_STRUCT\000"
.LASF27:
	.ascii	"__hours\000"
.LASF59:
	.ascii	"timer_rescan\000"
.LASF53:
	.ascii	"reason\000"
.LASF148:
	.ascii	"comm_mngr\000"
.LASF137:
	.ascii	"GuiVar_DateTimeSec\000"
.LASF60:
	.ascii	"timer_token_arrival\000"
.LASF102:
	.ascii	"pyear\000"
.LASF126:
	.ascii	"pcomplete_redraw\000"
.LASF0:
	.ascii	"char\000"
.LASF29:
	.ascii	"__seconds\000"
.LASF150:
	.ascii	"g_TIME_DATE_previous_cursor_pos\000"
.LASF135:
	.ascii	"GuiVar_DateTimeMin\000"
.LASF63:
	.ascii	"start_a_scan_at_the_next_opportunity\000"
.LASF98:
	.ascii	"flowsense_devices_are_connected\000"
.LASF77:
	.ascii	"main_app_firmware_up_to_date\000"
.LASF121:
	.ascii	"lnew_dt\000"
.LASF132:
	.ascii	"GuiVar_DateTimeDay\000"
.LASF65:
	.ascii	"is_a_NEW_member_in_the_chain\000"
.LASF46:
	.ascii	"offset\000"
.LASF20:
	.ascii	"repeats\000"
.LASF62:
	.ascii	"we_have_crossed_midnight_so_clear_the_error_counts\000"
.LASF123:
	.ascii	"TIME_DATE_copy_settings_into_guivars\000"
.LASF154:
	.ascii	"FDTO_TIME_DATE_show_time_zone_dropdown\000"
.LASF136:
	.ascii	"GuiVar_DateTimeMonth\000"
.LASF26:
	.ascii	"__year\000"
.LASF139:
	.ascii	"GuiVar_DateTimeTimeZone\000"
.LASF80:
	.ascii	"device_exchange_initial_event\000"
.LASF91:
	.ascii	"incoming_messages_or_packets\000"
.LASF131:
	.ascii	"GuiVar_DateTimeAMPM\000"
.LASF118:
	.ascii	"pbox_index_0\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF40:
	.ascii	"_07_u32_argument2\000"
.LASF134:
	.ascii	"GuiVar_DateTimeHour\000"
.LASF103:
	.ascii	"TIME_DATE_get_day_of_week\000"
.LASF114:
	.ascii	"TIME_DATE_process_seconds\000"
.LASF38:
	.ascii	"_04_func_ptr\000"
.LASF113:
	.ascii	"TIME_DATE_process_minutes\000"
.LASF17:
	.ascii	"xSemaphoreHandle\000"
.LASF66:
	.ascii	"since_the_scan__master__number_of_clean_tokens_made"
	.ascii	"\000"
.LASF125:
	.ascii	"FDTO_TIME_DATE_draw_screen\000"
.LASF89:
	.ascii	"token_in_transit\000"
.LASF92:
	.ascii	"changes\000"
.LASF52:
	.ascii	"CHANGE_TRACKING_FOR_COMMUNICATIONS_STRUCT\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
