	.file	"e_system_capacity.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.g_SYSTEM_CAPACITY_combo_box_guivar,"aw",%nobits
	.align	2
	.type	g_SYSTEM_CAPACITY_combo_box_guivar, %object
	.size	g_SYSTEM_CAPACITY_combo_box_guivar, 4
g_SYSTEM_CAPACITY_combo_box_guivar:
	.space	4
	.section	.text.SYSTEM_CAPACITY_process_capacity,"ax",%progbits
	.align	2
	.type	SYSTEM_CAPACITY_process_capacity, %function
SYSTEM_CAPACITY_process_capacity:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_system_capacity.c"
	.loc 1 51 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #28
.LCFI2:
	str	r0, [fp, #-16]
	str	r1, [fp, #-12]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 54 0
	ldr	r3, [fp, #-12]
	mov	r0, r3
	bl	SYSTEM_get_inc_by_for_MLB_and_capacity
	str	r0, [fp, #-8]
	.loc 1 56 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L2
	.loc 1 58 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, [fp, #-24]
	mov	r2, #1
	mov	r3, #4000
	bl	process_uns32
	b	.L1
.L2:
	.loc 1 62 0
	bl	bad_key_beep
.L1:
	.loc 1 64 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE0:
	.size	SYSTEM_CAPACITY_process_capacity, .-SYSTEM_CAPACITY_process_capacity
	.section	.text.FDTO_SYSTEM_CAPACITY_show_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_CAPACITY_show_dropdown, %function
FDTO_SYSTEM_CAPACITY_show_dropdown:
.LFB1:
	.loc 1 68 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 69 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r2, r3
	bl	FDTO_COMBO_BOX_show_no_yes_dropdown
	.loc 1 70 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	g_SYSTEM_CAPACITY_combo_box_guivar
.LFE1:
	.size	FDTO_SYSTEM_CAPACITY_show_dropdown, .-FDTO_SYSTEM_CAPACITY_show_dropdown
	.section	.text.FDTO_SYSTEM_CAPACITY_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_CAPACITY_draw_screen
	.type	FDTO_SYSTEM_CAPACITY_draw_screen, %function
FDTO_SYSTEM_CAPACITY_draw_screen:
.LFB2:
	.loc 1 74 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-12]
	.loc 1 77 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L8
	.loc 1 79 0
	bl	SYSTEM_CAPACITY_copy_group_into_guivars
	.loc 1 81 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L9
.L8:
	.loc 1 85 0
	ldr	r3, .L10
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L9:
	.loc 1 88 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #38
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 89 0
	bl	GuiLib_Refresh
	.loc 1 90 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L11:
	.align	2
.L10:
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_SYSTEM_CAPACITY_draw_screen, .-FDTO_SYSTEM_CAPACITY_draw_screen
	.section	.text.SYSTEM_CAPACITY_process_screen,"ax",%progbits
	.align	2
	.global	SYSTEM_CAPACITY_process_screen
	.type	SYSTEM_CAPACITY_process_screen, %function
SYSTEM_CAPACITY_process_screen:
.LFB3:
	.loc 1 94 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI9:
	add	fp, sp, #8
.LCFI10:
	sub	sp, sp, #52
.LCFI11:
	str	r0, [fp, #-52]
	str	r1, [fp, #-48]
	.loc 1 97 0
	ldr	r3, .L63
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #740
	bne	.L62
.L14:
	.loc 1 100 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L63+4
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	COMBO_BOX_key_press
	.loc 1 101 0
	b	.L12
.L62:
	.loc 1 104 0
	ldr	r3, [fp, #-52]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L16
.L24:
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L21
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L17
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L22
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L23
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L23
.L19:
	.loc 1 107 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L25
.L30:
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
.L26:
	.loc 1 110 0
	ldr	r3, .L63+4
	ldr	r2, .L63+12
	str	r2, [r3, #0]
	.loc 1 111 0
	b	.L25
.L27:
	.loc 1 114 0
	ldr	r3, .L63+4
	ldr	r2, .L63+16
	str	r2, [r3, #0]
	.loc 1 115 0
	b	.L25
.L28:
	.loc 1 118 0
	ldr	r3, .L63+4
	ldr	r2, .L63+20
	str	r2, [r3, #0]
	.loc 1 119 0
	b	.L25
.L29:
	.loc 1 122 0
	ldr	r3, .L63+4
	ldr	r2, .L63+24
	str	r2, [r3, #0]
	.loc 1 123 0
	mov	r0, r0	@ nop
.L25:
	.loc 1 126 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L31
	.loc 1 126 0 is_stmt 0 discriminator 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #3
	bgt	.L31
	.loc 1 128 0 is_stmt 1
	bl	good_key_beep
	.loc 1 130 0
	mov	r3, #3
	str	r3, [fp, #-44]
	.loc 1 131 0
	ldr	r3, .L63+28
	str	r3, [fp, #-24]
	.loc 1 132 0
	mov	r3, #158
	str	r3, [fp, #-20]
	.loc 1 133 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L32
	.loc 1 133 0 is_stmt 0 discriminator 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, #46
	b	.L33
.L32:
	.loc 1 133 0 discriminator 2
	mov	r3, #46
.L33:
	.loc 1 133 0 discriminator 3
	str	r3, [fp, #-16]
	.loc 1 134 0 is_stmt 1 discriminator 3
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 140 0 discriminator 3
	b	.L12
.L31:
	.loc 1 138 0
	bl	bad_key_beep
	.loc 1 140 0
	b	.L12
.L23:
	.loc 1 144 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #23
	ldrls	pc, [pc, r3, asl #2]
	b	.L35
.L48:
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L35
	.word	.L35
	.word	.L35
	.word	.L35
	.word	.L35
	.word	.L35
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L35
	.word	.L35
	.word	.L35
	.word	.L35
	.word	.L35
	.word	.L35
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
.L36:
	.loc 1 147 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L63+12
	mov	r2, #0
	mov	r3, #1
	bl	process_uns32
	.loc 1 148 0
	b	.L49
.L37:
	.loc 1 151 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L63+16
	mov	r2, #0
	mov	r3, #1
	bl	process_uns32
	.loc 1 152 0
	b	.L49
.L38:
	.loc 1 155 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L63+20
	mov	r2, #0
	mov	r3, #1
	bl	process_uns32
	.loc 1 156 0
	b	.L49
.L39:
	.loc 1 159 0
	ldr	r3, [fp, #-52]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L63+24
	mov	r2, #0
	mov	r3, #1
	bl	process_uns32
	.loc 1 160 0
	b	.L49
.L40:
	.loc 1 163 0
	ldr	r3, .L63+12
	ldr	r3, [r3, #0]
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	mov	r2, r3
	ldr	r3, .L63+32
	bl	SYSTEM_CAPACITY_process_capacity
	.loc 1 164 0
	b	.L49
.L41:
	.loc 1 167 0
	ldr	r3, .L63+16
	ldr	r3, [r3, #0]
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	mov	r2, r3
	ldr	r3, .L63+36
	bl	SYSTEM_CAPACITY_process_capacity
	.loc 1 168 0
	b	.L49
.L42:
	.loc 1 171 0
	ldr	r3, .L63+20
	ldr	r3, [r3, #0]
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	mov	r2, r3
	ldr	r3, .L63+40
	bl	SYSTEM_CAPACITY_process_capacity
	.loc 1 172 0
	b	.L49
.L43:
	.loc 1 175 0
	ldr	r3, .L63+24
	ldr	r3, [r3, #0]
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	mov	r2, r3
	ldr	r3, .L63+44
	bl	SYSTEM_CAPACITY_process_capacity
	.loc 1 176 0
	b	.L49
.L44:
	.loc 1 179 0
	ldr	r3, .L63+12
	ldr	r3, [r3, #0]
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	mov	r2, r3
	ldr	r3, .L63+48
	bl	SYSTEM_CAPACITY_process_capacity
	.loc 1 180 0
	b	.L49
.L45:
	.loc 1 183 0
	ldr	r3, .L63+16
	ldr	r3, [r3, #0]
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	mov	r2, r3
	ldr	r3, .L63+52
	bl	SYSTEM_CAPACITY_process_capacity
	.loc 1 184 0
	b	.L49
.L46:
	.loc 1 187 0
	ldr	r3, .L63+20
	ldr	r3, [r3, #0]
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	mov	r2, r3
	ldr	r3, .L63+56
	bl	SYSTEM_CAPACITY_process_capacity
	.loc 1 188 0
	b	.L49
.L47:
	.loc 1 191 0
	ldr	r3, .L63+24
	ldr	r3, [r3, #0]
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	mov	r2, r3
	ldr	r3, .L63+60
	bl	SYSTEM_CAPACITY_process_capacity
	.loc 1 192 0
	b	.L49
.L35:
	.loc 1 195 0
	bl	bad_key_beep
.L49:
	.loc 1 197 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 198 0
	b	.L12
.L21:
	.loc 1 202 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L63+64
	smull	r1, r3, r2, r3
	mov	r1, r3, asr #2
	mov	r3, r2, asr #31
	rsb	r1, r3, r1
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #1
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #0
	bne	.L50
	.loc 1 204 0
	bl	bad_key_beep
	.loc 1 210 0
	b	.L12
.L50:
	.loc 1 208 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 210 0
	b	.L12
.L17:
	.loc 1 214 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L63+64
	smull	r1, r3, r2, r3
	mov	r1, r3, asr #2
	mov	r3, r2, asr #31
	rsb	r1, r3, r1
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #1
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r4, r3, asr #16
	bl	SYSTEM_num_systems_in_use
	mov	r3, r0
	sub	r3, r3, #1
	cmp	r4, r3
	bne	.L52
	.loc 1 216 0
	bl	bad_key_beep
	.loc 1 222 0
	b	.L12
.L52:
	.loc 1 220 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 222 0
	b	.L12
.L18:
	.loc 1 225 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L54
	.loc 1 225 0 is_stmt 0 discriminator 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #3
	bgt	.L54
	.loc 1 227 0 is_stmt 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #19
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L55
.L54:
	.loc 1 229 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ble	.L56
	.loc 1 229 0 is_stmt 0 discriminator 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #13
	bgt	.L56
	.loc 1 231 0 is_stmt 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #10
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L55
.L56:
	.loc 1 233 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #19
	ble	.L57
	.loc 1 233 0 is_stmt 0 discriminator 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #23
	bgt	.L57
	.loc 1 235 0 is_stmt 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #10
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L55
.L57:
	.loc 1 239 0
	bl	bad_key_beep
	.loc 1 241 0
	b	.L12
.L55:
	b	.L12
.L20:
	.loc 1 244 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L58
	.loc 1 244 0 is_stmt 0 discriminator 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #3
	bgt	.L58
	.loc 1 246 0 is_stmt 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #10
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L59
.L58:
	.loc 1 248 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ble	.L60
	.loc 1 248 0 is_stmt 0 discriminator 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #13
	bgt	.L60
	.loc 1 250 0 is_stmt 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #10
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L59
.L60:
	.loc 1 252 0
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #19
	ble	.L61
	.loc 1 252 0 is_stmt 0 discriminator 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #23
	bgt	.L61
	.loc 1 254 0 is_stmt 1
	ldr	r3, .L63+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #19
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	b	.L59
.L61:
	.loc 1 258 0
	bl	bad_key_beep
	.loc 1 260 0
	b	.L12
.L59:
	b	.L12
.L22:
	.loc 1 263 0
	ldr	r3, .L63+68
	mov	r2, #3
	str	r2, [r3, #0]
	.loc 1 265 0
	bl	SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars
.L16:
	.loc 1 270 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L12:
	.loc 1 273 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L64:
	.align	2
.L63:
	.word	GuiLib_CurStructureNdx
	.word	g_SYSTEM_CAPACITY_combo_box_guivar
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	FDTO_SYSTEM_CAPACITY_show_dropdown
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSettingC_3
	.word	1717986919
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	SYSTEM_CAPACITY_process_screen, .-SYSTEM_CAPACITY_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x4ff
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF61
	.byte	0x1
	.4byte	.LASF62
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8b
	.uleb128 0x6
	.4byte	0x92
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xb0
	.uleb128 0x9
	.4byte	0x92
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd5
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x82
	.4byte	0xb0
	.uleb128 0x8
	.4byte	0x53
	.4byte	0xf0
	.uleb128 0x9
	.4byte	0x92
	.byte	0x2
	.byte	0
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x177
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x88
	.4byte	0x188
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x8d
	.4byte	0x19a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x92
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x183
	.uleb128 0xd
	.4byte	0x183
	.byte	0
	.uleb128 0xe
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x177
	.uleb128 0xc
	.byte	0x1
	.4byte	0x19a
	.uleb128 0xd
	.4byte	0xd5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x18e
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9e
	.4byte	0xf0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF26
	.uleb128 0x8
	.4byte	0x53
	.4byte	0x1c2
	.uleb128 0x9
	.4byte	0x92
	.byte	0x3
	.byte	0
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x1
	.byte	0x32
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x213
	.uleb128 0x10
	.4byte	.LASF27
	.byte	0x1
	.byte	0x32
	.4byte	0x213
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x10
	.4byte	.LASF28
	.byte	0x1
	.byte	0x32
	.4byte	0x218
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x10
	.4byte	.LASF29
	.byte	0x1
	.byte	0x32
	.4byte	0x21d
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x11
	.4byte	.LASF35
	.byte	0x1
	.byte	0x34
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0xd5
	.uleb128 0xe
	.4byte	0x7a
	.uleb128 0x5
	.byte	0x4
	.4byte	0x53
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x1
	.byte	0x43
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x258
	.uleb128 0x10
	.4byte	.LASF32
	.byte	0x1
	.byte	0x43
	.4byte	0x258
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LASF33
	.byte	0x1
	.byte	0x43
	.4byte	0x258
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xe
	.4byte	0x53
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.byte	0x49
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x293
	.uleb128 0x10
	.4byte	.LASF34
	.byte	0x1
	.byte	0x49
	.4byte	0x218
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF36
	.byte	0x1
	.byte	0x4b
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.byte	0x5d
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x2c9
	.uleb128 0x10
	.4byte	.LASF27
	.byte	0x1
	.byte	0x5d
	.4byte	0x213
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x5f
	.4byte	0x1a0
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x211
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x212
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x213
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x214
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x21b
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x21c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x21d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x21e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x225
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x226
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x227
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF50
	.byte	0x5
	.2byte	0x228
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF51
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF52
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF53
	.byte	0x6
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF54
	.byte	0x7
	.byte	0x30
	.4byte	0x3ac
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF55
	.byte	0x7
	.byte	0x34
	.4byte	0x3c2
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF56
	.byte	0x7
	.byte	0x36
	.4byte	0x3d8
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF57
	.byte	0x7
	.byte	0x38
	.4byte	0x3ee
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF58
	.byte	0x8
	.byte	0x33
	.4byte	0x404
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xe
	.4byte	0xe0
	.uleb128 0x11
	.4byte	.LASF59
	.byte	0x8
	.byte	0x3f
	.4byte	0x41a
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xe
	.4byte	0x1b2
	.uleb128 0x11
	.4byte	.LASF60
	.byte	0x1
	.byte	0x2c
	.4byte	0x21d
	.byte	0x5
	.byte	0x3
	.4byte	g_SYSTEM_CAPACITY_combo_box_guivar
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x211
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x212
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x213
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x214
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x21b
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x21c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x21d
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x21e
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x225
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x226
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x227
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF50
	.byte	0x5
	.2byte	0x228
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF51
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF52
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF53
	.byte	0x6
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF61:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF31:
	.ascii	"FDTO_SYSTEM_CAPACITY_show_dropdown\000"
.LASF62:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_system_capacity.c\000"
.LASF4:
	.ascii	"short int\000"
.LASF16:
	.ascii	"_01_command\000"
.LASF33:
	.ascii	"py_coord\000"
.LASF44:
	.ascii	"GuiVar_GroupSettingB_1\000"
.LASF51:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF13:
	.ascii	"keycode\000"
.LASF29:
	.ascii	"pcapacity_ptr\000"
.LASF17:
	.ascii	"_02_menu\000"
.LASF30:
	.ascii	"SYSTEM_CAPACITY_process_capacity\000"
.LASF60:
	.ascii	"g_SYSTEM_CAPACITY_combo_box_guivar\000"
.LASF26:
	.ascii	"float\000"
.LASF9:
	.ascii	"long long int\000"
.LASF56:
	.ascii	"GuiFont_DecimalChar\000"
.LASF38:
	.ascii	"SYSTEM_CAPACITY_process_screen\000"
.LASF12:
	.ascii	"long int\000"
.LASF47:
	.ascii	"GuiVar_GroupSettingC_0\000"
.LASF48:
	.ascii	"GuiVar_GroupSettingC_1\000"
.LASF49:
	.ascii	"GuiVar_GroupSettingC_2\000"
.LASF50:
	.ascii	"GuiVar_GroupSettingC_3\000"
.LASF22:
	.ascii	"_06_u32_argument1\000"
.LASF28:
	.ascii	"pcapacity_in_use\000"
.LASF20:
	.ascii	"key_process_func_ptr\000"
.LASF24:
	.ascii	"_08_screen_to_draw\000"
.LASF34:
	.ascii	"pcomplete_redraw\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF43:
	.ascii	"GuiVar_GroupSettingB_0\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF45:
	.ascii	"GuiVar_GroupSettingB_2\000"
.LASF46:
	.ascii	"GuiVar_GroupSettingB_3\000"
.LASF2:
	.ascii	"signed char\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF55:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF32:
	.ascii	"px_coord\000"
.LASF18:
	.ascii	"_03_structure_to_draw\000"
.LASF52:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF39:
	.ascii	"GuiVar_GroupSettingA_0\000"
.LASF40:
	.ascii	"GuiVar_GroupSettingA_1\000"
.LASF41:
	.ascii	"GuiVar_GroupSettingA_2\000"
.LASF42:
	.ascii	"GuiVar_GroupSettingA_3\000"
.LASF0:
	.ascii	"char\000"
.LASF37:
	.ascii	"FDTO_SYSTEM_CAPACITY_draw_screen\000"
.LASF57:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF19:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF23:
	.ascii	"_07_u32_argument2\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF59:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF21:
	.ascii	"_04_func_ptr\000"
.LASF15:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF53:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF14:
	.ascii	"repeats\000"
.LASF54:
	.ascii	"GuiFont_LanguageActive\000"
.LASF36:
	.ascii	"lcursor_to_select\000"
.LASF58:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF27:
	.ascii	"pkey_event\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF25:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF35:
	.ascii	"inc_by\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
