	.file	"d_two_wire_debug.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.global	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed
	.section	.bss.g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.type	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed, %object
	.size	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed, 4
g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed:
	.space	4
	.global	g_TWO_WIRE_DEBUG_dialog_visible
	.section	.bss.g_TWO_WIRE_DEBUG_dialog_visible,"aw",%nobits
	.align	2
	.type	g_TWO_WIRE_DEBUG_dialog_visible, %object
	.size	g_TWO_WIRE_DEBUG_dialog_visible, 4
g_TWO_WIRE_DEBUG_dialog_visible:
	.space	4
	.section	.bss.g_TWO_WIRE_DEBUG_last_cursor_position,"aw",%nobits
	.align	2
	.type	g_TWO_WIRE_DEBUG_last_cursor_position, %object
	.size	g_TWO_WIRE_DEBUG_last_cursor_position, 4
g_TWO_WIRE_DEBUG_last_cursor_position:
	.space	4
	.section	.text.FDTO_TWO_WIRE_DEBUG_draw_dialog,"ax",%progbits
	.align	2
	.type	FDTO_TWO_WIRE_DEBUG_draw_dialog, %function
FDTO_TWO_WIRE_DEBUG_draw_dialog:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_two_wire_debug.c"
	.loc 1 53 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-12]
	.loc 1 56 0
	ldr	r3, .L5
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L5+4
	str	r2, [r3, #0]
	.loc 1 58 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L2
	.loc 1 62 0
	ldr	r3, .L5+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L3
	.loc 1 64 0
	ldr	r3, .L5+8
	mov	r2, #50
	str	r2, [r3, #0]
.L3:
	.loc 1 67 0
	ldr	r3, .L5+8
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	b	.L4
.L2:
	.loc 1 71 0
	ldr	r3, .L5
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L4:
	.loc 1 74 0
	ldr	r3, .L5+12
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 76 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r0, .L5+16
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 77 0
	bl	GuiLib_Refresh
	.loc 1 78 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed
	.word	g_TWO_WIRE_DEBUG_last_cursor_position
	.word	g_TWO_WIRE_DEBUG_dialog_visible
	.word	643
.LFE0:
	.size	FDTO_TWO_WIRE_DEBUG_draw_dialog, .-FDTO_TWO_WIRE_DEBUG_draw_dialog
	.section	.text.TWO_WIRE_DEBUG_draw_dialog,"ax",%progbits
	.align	2
	.global	TWO_WIRE_DEBUG_draw_dialog
	.type	TWO_WIRE_DEBUG_draw_dialog, %function
TWO_WIRE_DEBUG_draw_dialog:
.LFB1:
	.loc 1 82 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #40
.LCFI5:
	str	r0, [fp, #-44]
	.loc 1 85 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 86 0
	ldr	r3, .L8
	str	r3, [fp, #-20]
	.loc 1 87 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 88 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 89 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	FDTO_TWO_WIRE_DEBUG_draw_dialog
.LFE1:
	.size	TWO_WIRE_DEBUG_draw_dialog, .-TWO_WIRE_DEBUG_draw_dialog
	.section	.text.TWO_WIRE_DEBUG_process_dialog,"ax",%progbits
	.align	2
	.global	TWO_WIRE_DEBUG_process_dialog
	.type	TWO_WIRE_DEBUG_process_dialog, %function
TWO_WIRE_DEBUG_process_dialog:
.LFB2:
	.loc 1 93 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #44
.LCFI8:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	.loc 1 96 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	beq	.L13
	cmp	r3, #2
	bhi	.L16
	cmp	r3, #0
	beq	.L12
	b	.L11
.L16:
	cmp	r3, #4
	beq	.L14
	cmp	r3, #67
	beq	.L15
	b	.L11
.L13:
	.loc 1 101 0
	ldr	r3, .L27
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L27+4
	str	r2, [r3, #0]
	.loc 1 103 0
	ldr	r3, .L27
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #50
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L17
.L24:
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L22
	.word	.L23
.L18:
	.loc 1 106 0
	ldr	r3, .L27+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 108 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 109 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 110 0
	mov	r3, #62
	str	r3, [fp, #-32]
	.loc 1 111 0
	ldr	r3, .L27+12
	str	r3, [fp, #-20]
	.loc 1 112 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 113 0
	ldr	r3, .L27+16
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 114 0
	ldr	r3, .L27+20
	str	r3, [fp, #-24]
	.loc 1 115 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 116 0
	b	.L25
.L19:
	.loc 1 119 0
	ldr	r3, .L27+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 121 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 122 0
	mov	r3, #11
	str	r3, [fp, #-36]
	.loc 1 123 0
	mov	r3, #76
	str	r3, [fp, #-32]
	.loc 1 124 0
	ldr	r3, .L27+24
	str	r3, [fp, #-20]
	.loc 1 125 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 126 0
	ldr	r3, .L27+16
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 127 0
	ldr	r3, .L27+28
	str	r3, [fp, #-24]
	.loc 1 128 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Change_Screen
	.loc 1 129 0
	b	.L25
.L20:
	.loc 1 132 0
	mov	r0, #1
	bl	TWO_WIRE_turn_cable_power_on_or_off_from_ui
	.loc 1 133 0
	b	.L25
.L21:
	.loc 1 136 0
	mov	r0, #0
	bl	TWO_WIRE_turn_cable_power_on_or_off_from_ui
	.loc 1 137 0
	b	.L25
.L22:
	.loc 1 140 0
	mov	r0, #1
	bl	TWO_WIRE_all_decoder_loopback_test_from_ui
	.loc 1 141 0
	b	.L25
.L23:
	.loc 1 144 0
	mov	r0, #0
	bl	TWO_WIRE_all_decoder_loopback_test_from_ui
	.loc 1 145 0
	b	.L25
.L17:
	.loc 1 148 0
	bl	bad_key_beep
	.loc 1 150 0
	b	.L10
.L25:
	b	.L10
.L14:
	.loc 1 153 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 154 0
	b	.L10
.L12:
	.loc 1 157 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 158 0
	b	.L10
.L15:
	.loc 1 161 0
	bl	good_key_beep
	.loc 1 165 0
	ldr	r3, .L27
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L27+4
	str	r2, [r3, #0]
	.loc 1 167 0
	ldr	r3, .L27+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 171 0
	ldr	r3, .L27+32
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L27
	strh	r2, [r3, #0]	@ movhi
	.loc 1 173 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 174 0
	b	.L10
.L11:
	.loc 1 177 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L10:
	.loc 1 179 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L28:
	.align	2
.L27:
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_TWO_WIRE_DEBUG_last_cursor_position
	.word	g_TWO_WIRE_DEBUG_dialog_visible
	.word	FDTO_TWO_WIRE_draw_assign_sn
	.word	GuiVar_MenuScreenToShow
	.word	TWO_WIRE_process_assign_sn
	.word	FDTO_TWO_WIRE_draw_decoder_stats_report
	.word	TWO_WIRE_process_decoder_stats_report
	.word	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed
.LFE2:
	.size	TWO_WIRE_DEBUG_process_dialog, .-TWO_WIRE_DEBUG_process_dialog
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_two_wire_debug.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x322
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF40
	.byte	0x1
	.4byte	.LASF41
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8b
	.uleb128 0x6
	.4byte	0x92
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xb0
	.uleb128 0x9
	.4byte	0x92
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd5
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x82
	.4byte	0xb0
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x167
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x88
	.4byte	0x178
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x8d
	.4byte	0x18a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x92
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x173
	.uleb128 0xd
	.4byte	0x173
	.byte	0
	.uleb128 0xe
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x167
	.uleb128 0xc
	.byte	0x1
	.4byte	0x18a
	.uleb128 0xd
	.4byte	0xd5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x17e
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9e
	.4byte	0xe0
	.uleb128 0xf
	.4byte	.LASF42
	.byte	0x1
	.byte	0x34
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1d0
	.uleb128 0x10
	.4byte	.LASF26
	.byte	0x1
	.byte	0x34
	.4byte	0x1d0
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF27
	.byte	0x1
	.byte	0x36
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x7a
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.byte	0x51
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x20b
	.uleb128 0x10
	.4byte	.LASF26
	.byte	0x1
	.byte	0x51
	.4byte	0x1d0
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x53
	.4byte	0x190
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.byte	0x5c
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x241
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x1
	.byte	0x5c
	.4byte	0x241
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x5e
	.4byte	0x190
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xe
	.4byte	0xd5
	.uleb128 0x14
	.4byte	.LASF31
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF32
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF33
	.byte	0x7
	.byte	0x30
	.4byte	0x273
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF34
	.byte	0x7
	.byte	0x34
	.4byte	0x289
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF35
	.byte	0x7
	.byte	0x36
	.4byte	0x29f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x11
	.4byte	.LASF36
	.byte	0x7
	.byte	0x38
	.4byte	0x2b5
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x15
	.4byte	.LASF37
	.byte	0x8
	.byte	0x15
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF38
	.byte	0x8
	.byte	0x17
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF39
	.byte	0x1
	.byte	0x2e
	.4byte	0x53
	.byte	0x5
	.byte	0x3
	.4byte	g_TWO_WIRE_DEBUG_last_cursor_position
	.uleb128 0x14
	.4byte	.LASF31
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF32
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF37
	.byte	0x1
	.byte	0x28
	.4byte	0x53
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed
	.uleb128 0x16
	.4byte	.LASF38
	.byte	0x1
	.byte	0x2a
	.4byte	0x7a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_TWO_WIRE_DEBUG_dialog_visible
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF28:
	.ascii	"TWO_WIRE_DEBUG_draw_dialog\000"
.LASF33:
	.ascii	"GuiFont_LanguageActive\000"
.LASF21:
	.ascii	"_04_func_ptr\000"
.LASF17:
	.ascii	"_02_menu\000"
.LASF20:
	.ascii	"key_process_func_ptr\000"
.LASF26:
	.ascii	"pcomplete_redraw\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF42:
	.ascii	"FDTO_TWO_WIRE_DEBUG_draw_dialog\000"
.LASF15:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF19:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF24:
	.ascii	"_08_screen_to_draw\000"
.LASF38:
	.ascii	"g_TWO_WIRE_DEBUG_dialog_visible\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF29:
	.ascii	"TWO_WIRE_DEBUG_process_dialog\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF13:
	.ascii	"keycode\000"
.LASF16:
	.ascii	"_01_command\000"
.LASF25:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF40:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF34:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF30:
	.ascii	"pkey_event\000"
.LASF35:
	.ascii	"GuiFont_DecimalChar\000"
.LASF14:
	.ascii	"repeats\000"
.LASF31:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF22:
	.ascii	"_06_u32_argument1\000"
.LASF9:
	.ascii	"long long int\000"
.LASF0:
	.ascii	"char\000"
.LASF27:
	.ascii	"lcursor_to_select\000"
.LASF36:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF4:
	.ascii	"short int\000"
.LASF18:
	.ascii	"_03_structure_to_draw\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF23:
	.ascii	"_07_u32_argument2\000"
.LASF37:
	.ascii	"g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displa"
	.ascii	"yed\000"
.LASF12:
	.ascii	"long int\000"
.LASF39:
	.ascii	"g_TWO_WIRE_DEBUG_last_cursor_position\000"
.LASF2:
	.ascii	"signed char\000"
.LASF41:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_two_wire_debug.c\000"
.LASF32:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
