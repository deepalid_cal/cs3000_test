	.file	"e_priorities.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_PRIORITY_combo_box_guivar,"aw",%nobits
	.align	2
	.type	g_PRIORITY_combo_box_guivar, %object
	.size	g_PRIORITY_combo_box_guivar, 4
g_PRIORITY_combo_box_guivar:
	.space	4
	.section	.text.FDTO_PRIORITY_populate_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_PRIORITY_populate_dropdown, %function
FDTO_PRIORITY_populate_dropdown:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_priorities.c"
	.loc 1 68 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	mov	r3, r0
	strh	r3, [fp, #-4]	@ movhi
	.loc 1 69 0
	ldrsh	r3, [fp, #-4]
	add	r3, r3, #1
	mov	r2, r3
	ldr	r3, .L2
	str	r2, [r3, #0]
	.loc 1 70 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L3:
	.align	2
.L2:
	.word	GuiVar_ComboBoxItemIndex
.LFE0:
	.size	FDTO_PRIORITY_populate_dropdown, .-FDTO_PRIORITY_populate_dropdown
	.section	.text.FDTO_PRIORITY_show_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_PRIORITY_show_dropdown, %function
FDTO_PRIORITY_show_dropdown:
.LFB1:
	.loc 1 88 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 89 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	ldr	r0, .L5+4
	ldr	r1, .L5+8
	mov	r2, #3
	bl	FDTO_COMBOBOX_show
	.loc 1 90 0
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	g_PRIORITY_combo_box_guivar
	.word	743
	.word	FDTO_PRIORITY_populate_dropdown
.LFE1:
	.size	FDTO_PRIORITY_show_dropdown, .-FDTO_PRIORITY_show_dropdown
	.section	.text.FDTO_PRIORITY_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_PRIORITY_draw_screen
	.type	FDTO_PRIORITY_draw_screen, %function
FDTO_PRIORITY_draw_screen:
.LFB2:
	.loc 1 94 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #8
.LCFI7:
	str	r0, [fp, #-12]
	.loc 1 97 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L8
	.loc 1 99 0
	bl	PRIORITY_copy_group_into_guivars
	.loc 1 101 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L9
.L8:
	.loc 1 105 0
	ldr	r3, .L10
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L9:
	.loc 1 108 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #49
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 109 0
	bl	GuiLib_Refresh
	.loc 1 110 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L11:
	.align	2
.L10:
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_PRIORITY_draw_screen, .-FDTO_PRIORITY_draw_screen
	.section	.text.PRIORITY_process_screen,"ax",%progbits
	.align	2
	.global	PRIORITY_process_screen
	.type	PRIORITY_process_screen, %function
PRIORITY_process_screen:
.LFB3:
	.loc 1 126 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	sub	sp, sp, #52
.LCFI10:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	.loc 1 129 0
	ldr	r3, .L53
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L53+4
	cmp	r2, r3
	bne	.L52
.L14:
	.loc 1 132 0
	ldr	r2, [fp, #-48]
	ldr	r3, .L53+8
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	COMBO_BOX_key_press
	.loc 1 135 0
	ldr	r3, .L53+8
	ldr	r3, [r3, #0]
	ldr	r2, .L53+8
	ldr	r2, [r2, #0]
	ldr	r2, [r2, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	.loc 1 136 0
	bl	Refresh_Screen
	.loc 1 137 0
	b	.L12
.L52:
	.loc 1 140 0
	ldr	r3, [fp, #-48]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L16
.L22:
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L17
	.word	.L18
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L18
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L17
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L20
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L21
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L21
.L19:
	.loc 1 143 0
	ldr	r3, .L53+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L23
.L34:
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
.L24:
	.loc 1 146 0
	ldr	r3, .L53+8
	ldr	r2, .L53+16
	str	r2, [r3, #0]
	.loc 1 147 0
	b	.L23
.L25:
	.loc 1 150 0
	ldr	r3, .L53+8
	ldr	r2, .L53+20
	str	r2, [r3, #0]
	.loc 1 151 0
	b	.L23
.L26:
	.loc 1 154 0
	ldr	r3, .L53+8
	ldr	r2, .L53+24
	str	r2, [r3, #0]
	.loc 1 155 0
	b	.L23
.L27:
	.loc 1 158 0
	ldr	r3, .L53+8
	ldr	r2, .L53+28
	str	r2, [r3, #0]
	.loc 1 159 0
	b	.L23
.L28:
	.loc 1 162 0
	ldr	r3, .L53+8
	ldr	r2, .L53+32
	str	r2, [r3, #0]
	.loc 1 163 0
	b	.L23
.L29:
	.loc 1 166 0
	ldr	r3, .L53+8
	ldr	r2, .L53+36
	str	r2, [r3, #0]
	.loc 1 167 0
	b	.L23
.L30:
	.loc 1 170 0
	ldr	r3, .L53+8
	ldr	r2, .L53+40
	str	r2, [r3, #0]
	.loc 1 171 0
	b	.L23
.L31:
	.loc 1 174 0
	ldr	r3, .L53+8
	ldr	r2, .L53+44
	str	r2, [r3, #0]
	.loc 1 175 0
	b	.L23
.L32:
	.loc 1 178 0
	ldr	r3, .L53+8
	ldr	r2, .L53+48
	str	r2, [r3, #0]
	.loc 1 179 0
	b	.L23
.L33:
	.loc 1 182 0
	ldr	r3, .L53+8
	ldr	r2, .L53+52
	str	r2, [r3, #0]
	.loc 1 183 0
	mov	r0, r0	@ nop
.L23:
	.loc 1 186 0
	ldr	r3, .L53+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L35
	.loc 1 188 0
	bl	good_key_beep
	.loc 1 190 0
	ldr	r3, .L53+12
	ldrh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L36
	.loc 1 192 0
	ldr	r3, .L53+56
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L37
.L36:
	.loc 1 196 0
	ldr	r3, .L53+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #1
	mov	r2, r3
	ldr	r3, .L53+56
	str	r2, [r3, #0]
.L37:
	.loc 1 199 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 200 0
	ldr	r3, .L53+60
	str	r3, [fp, #-20]
	.loc 1 201 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 207 0
	b	.L12
.L35:
	.loc 1 205 0
	bl	bad_key_beep
	.loc 1 207 0
	b	.L12
.L21:
	.loc 1 211 0
	ldr	r3, .L53+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L39
.L50:
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
.L40:
	.loc 1 214 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+16
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 215 0
	b	.L51
.L41:
	.loc 1 218 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+20
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 219 0
	b	.L51
.L42:
	.loc 1 222 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+24
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 223 0
	b	.L51
.L43:
	.loc 1 226 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+28
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 227 0
	b	.L51
.L44:
	.loc 1 230 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+32
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 231 0
	b	.L51
.L45:
	.loc 1 234 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+36
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 235 0
	b	.L51
.L46:
	.loc 1 238 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+40
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 239 0
	b	.L51
.L47:
	.loc 1 242 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+44
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 243 0
	b	.L51
.L48:
	.loc 1 246 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+48
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 247 0
	b	.L51
.L49:
	.loc 1 250 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L53+52
	mov	r2, #1
	mov	r3, #3
	bl	process_uns32
	.loc 1 251 0
	b	.L51
.L39:
	.loc 1 254 0
	bl	bad_key_beep
.L51:
	.loc 1 256 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 257 0
	b	.L12
.L18:
	.loc 1 262 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 263 0
	b	.L12
.L17:
	.loc 1 268 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 269 0
	b	.L12
.L20:
	.loc 1 272 0
	ldr	r3, .L53+64
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 273 0
	bl	PRIORITY_extract_and_store_changes_from_GuiVars
.L16:
	.loc 1 278 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L12:
	.loc 1 281 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L54:
	.align	2
.L53:
	.word	GuiLib_CurStructureNdx
	.word	743
	.word	g_PRIORITY_combo_box_guivar
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_ComboBox_Y1
	.word	FDTO_PRIORITY_show_dropdown
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	PRIORITY_process_screen, .-PRIORITY_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x463
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF53
	.byte	0x1
	.4byte	.LASF54
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x99
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x8b
	.uleb128 0x6
	.4byte	0x92
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xb0
	.uleb128 0x9
	.4byte	0x92
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd5
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x82
	.4byte	0xb0
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x167
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x88
	.4byte	0x178
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x8d
	.4byte	0x18a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x92
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x173
	.uleb128 0xd
	.4byte	0x173
	.byte	0
	.uleb128 0xe
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x167
	.uleb128 0xc
	.byte	0x1
	.4byte	0x18a
	.uleb128 0xd
	.4byte	0xd5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x17e
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9e
	.4byte	0xe0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF26
	.uleb128 0xf
	.4byte	.LASF55
	.byte	0x1
	.byte	0x43
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1c9
	.uleb128 0x10
	.4byte	.LASF27
	.byte	0x1
	.byte	0x43
	.4byte	0x173
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x11
	.4byte	.LASF56
	.byte	0x1
	.byte	0x57
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.byte	0x5d
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x213
	.uleb128 0x10
	.4byte	.LASF28
	.byte	0x1
	.byte	0x5d
	.4byte	0x213
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF32
	.byte	0x1
	.byte	0x5f
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x7a
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.byte	0x7d
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x24e
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0x7d
	.4byte	0x24e
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x14
	.ascii	"lde\000"
	.byte	0x1
	.byte	0x7f
	.4byte	0x190
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xe
	.4byte	0xd5
	.uleb128 0x15
	.4byte	.LASF33
	.byte	0x5
	.2byte	0x168
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF34
	.byte	0x5
	.2byte	0x169
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF35
	.byte	0x5
	.2byte	0x211
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x212
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x213
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x214
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x215
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x216
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x217
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x218
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x219
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x21a
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF46
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF47
	.byte	0x6
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF48
	.byte	0x7
	.byte	0x30
	.4byte	0x336
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x13
	.4byte	.LASF49
	.byte	0x7
	.byte	0x34
	.4byte	0x34c
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x13
	.4byte	.LASF50
	.byte	0x7
	.byte	0x36
	.4byte	0x362
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x13
	.4byte	.LASF51
	.byte	0x7
	.byte	0x38
	.4byte	0x378
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xa0
	.uleb128 0x13
	.4byte	.LASF52
	.byte	0x1
	.byte	0x2c
	.4byte	0x38e
	.byte	0x5
	.byte	0x3
	.4byte	g_PRIORITY_combo_box_guivar
	.uleb128 0x5
	.byte	0x4
	.4byte	0x53
	.uleb128 0x15
	.4byte	.LASF33
	.byte	0x5
	.2byte	0x168
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF34
	.byte	0x5
	.2byte	0x169
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF35
	.byte	0x5
	.2byte	0x211
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x212
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x213
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x214
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x215
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x216
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x217
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x218
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x219
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x21a
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF46
	.byte	0x6
	.2byte	0x127
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF47
	.byte	0x6
	.2byte	0x132
	.4byte	0x4c
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF9:
	.ascii	"long long int\000"
.LASF48:
	.ascii	"GuiFont_LanguageActive\000"
.LASF56:
	.ascii	"FDTO_PRIORITY_show_dropdown\000"
.LASF21:
	.ascii	"_04_func_ptr\000"
.LASF34:
	.ascii	"GuiVar_ComboBoxItemIndex\000"
.LASF17:
	.ascii	"_02_menu\000"
.LASF20:
	.ascii	"key_process_func_ptr\000"
.LASF28:
	.ascii	"pcomplete_redraw\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF38:
	.ascii	"GuiVar_GroupSettingA_3\000"
.LASF55:
	.ascii	"FDTO_PRIORITY_populate_dropdown\000"
.LASF19:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF41:
	.ascii	"GuiVar_GroupSettingA_6\000"
.LASF42:
	.ascii	"GuiVar_GroupSettingA_7\000"
.LASF24:
	.ascii	"_08_screen_to_draw\000"
.LASF33:
	.ascii	"GuiVar_ComboBox_Y1\000"
.LASF26:
	.ascii	"float\000"
.LASF32:
	.ascii	"lcursor_to_select\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF27:
	.ascii	"pindex_0\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF52:
	.ascii	"g_PRIORITY_combo_box_guivar\000"
.LASF54:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_priorities.c\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF51:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF16:
	.ascii	"_01_command\000"
.LASF25:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF47:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF53:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF49:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF4:
	.ascii	"short int\000"
.LASF13:
	.ascii	"keycode\000"
.LASF30:
	.ascii	"PRIORITY_process_screen\000"
.LASF35:
	.ascii	"GuiVar_GroupSettingA_0\000"
.LASF36:
	.ascii	"GuiVar_GroupSettingA_1\000"
.LASF37:
	.ascii	"GuiVar_GroupSettingA_2\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF39:
	.ascii	"GuiVar_GroupSettingA_4\000"
.LASF40:
	.ascii	"GuiVar_GroupSettingA_5\000"
.LASF31:
	.ascii	"pkey_event\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF43:
	.ascii	"GuiVar_GroupSettingA_8\000"
.LASF44:
	.ascii	"GuiVar_GroupSettingA_9\000"
.LASF10:
	.ascii	"BOOL_32\000"
.LASF22:
	.ascii	"_06_u32_argument1\000"
.LASF15:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF0:
	.ascii	"char\000"
.LASF29:
	.ascii	"FDTO_PRIORITY_draw_screen\000"
.LASF45:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF23:
	.ascii	"_07_u32_argument2\000"
.LASF18:
	.ascii	"_03_structure_to_draw\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF12:
	.ascii	"long int\000"
.LASF14:
	.ascii	"repeats\000"
.LASF2:
	.ascii	"signed char\000"
.LASF46:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF50:
	.ascii	"GuiFont_DecimalChar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
