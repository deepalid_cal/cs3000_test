	.file	"gpio_setup.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	display_model_is
	.section	.bss.display_model_is,"aw",%nobits
	.align	2
	.type	display_model_is, %object
	.size	display_model_is, 4
display_model_is:
	.space	4
	.global	board_hardware_id
	.section	.bss.board_hardware_id,"aw",%nobits
	.align	2
	.type	board_hardware_id, %object
	.size	board_hardware_id, 4
board_hardware_id:
	.space	4
	.section	.text.gpio_setup,"ax",%progbits
	.align	2
	.global	gpio_setup
	.type	gpio_setup, %function
gpio_setup:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/gpio_setup.c"
	.loc 1 72 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	.loc 1 84 0
	ldr	r3, .L2
	str	r3, [fp, #-4]
	.loc 1 90 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 94 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #0]
	orr	r2, r3, #32
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 100 0
	ldr	r3, .L2+4
	str	r3, [fp, #-8]
	.loc 1 106 0
	ldr	r3, [fp, #-8]
	mov	r2, #255
	str	r2, [r3, #292]
	.loc 1 109 0
	ldr	r3, [fp, #-8]
	mov	r2, #63
	str	r2, [r3, #80]
	.loc 1 112 0
	ldr	r3, [fp, #-8]
	mov	r2, #192
	str	r2, [r3, #84]
	.loc 1 115 0
	ldr	r3, [fp, #-8]
	mov	r2, #8
	str	r2, [r3, #68]
	.loc 1 118 0
	ldr	r3, [fp, #-8]
	mov	r2, #55
	str	r2, [r3, #72]
	.loc 1 124 0
	ldr	r3, .L2+8
	mov	r2, #8388608
	str	r2, [r3, #0]
	.loc 1 125 0
	ldr	r3, [fp, #-8]
	mov	r2, #8388608
	str	r2, [r3, #112]
	.loc 1 127 0
	ldr	r3, [fp, #-8]
	mvn	r2, #-16777216
	str	r2, [r3, #308]
	.loc 1 128 0
	ldr	r3, [fp, #-8]
	mov	r2, #8388608
	str	r2, [r3, #304]
	.loc 1 144 0
	ldr	r3, [fp, #-8]
	mov	r2, #3
	str	r2, [r3, #40]
	.loc 1 150 0
	ldr	r3, [fp, #-8]
	mov	r2, #60
	str	r2, [r3, #44]
	.loc 1 155 0
	ldr	r3, [fp, #-8]
	mov	r2, #1610612736
	str	r2, [r3, #4]
	.loc 1 157 0
	ldr	r3, [fp, #-8]
	mov	r2, #1610612736
	str	r2, [r3, #16]
	.loc 1 159 0
	ldr	r3, [fp, #-8]
	mov	r2, #100663296
	str	r2, [r3, #20]
	.loc 1 163 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L2+12
	str	r2, [r3, #8]
	.loc 1 207 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L2+16
	str	r2, [r3, #256]
	.loc 1 234 0
	ldr	r3, .L2+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 237 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L3:
	.align	2
.L2:
	.word	1074085888
	.word	1073905664
	.word	1073905768
	.word	14306162
	.word	5984
	.word	1073758336
.LFE0:
	.size	gpio_setup, .-gpio_setup
	.section .rodata
	.align	2
.LC0:
	.ascii	"Board Rev: %u\000"
	.section	.text.GPIO_SETUP_learn_and_set_board_hardware_id,"ax",%progbits
	.align	2
	.global	GPIO_SETUP_learn_and_set_board_hardware_id
	.type	GPIO_SETUP_learn_and_set_board_hardware_id, %function
GPIO_SETUP_learn_and_set_board_hardware_id:
.LFB1:
	.loc 1 241 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 251 0
	ldr	r3, .L7
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 253 0
	ldr	r3, .L7+4
	ldr	r3, [r3, #0]
	and	r3, r3, #64
	cmp	r3, #0
	beq	.L5
	.loc 1 255 0
	ldr	r3, .L7
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L7
	str	r2, [r3, #0]
.L5:
	.loc 1 258 0
	ldr	r3, .L7+4
	ldr	r3, [r3, #0]
	and	r3, r3, #128
	cmp	r3, #0
	beq	.L6
	.loc 1 260 0
	ldr	r3, .L7
	ldr	r3, [r3, #0]
	add	r2, r3, #2
	ldr	r3, .L7
	str	r2, [r3, #0]
.L6:
	.loc 1 263 0
	ldr	r3, .L7
	ldr	r3, [r3, #0]
	ldr	r0, .L7+8
	mov	r1, r3
	bl	Alert_Message_va
	.loc 1 264 0
	ldmfd	sp!, {fp, pc}
.L8:
	.align	2
.L7:
	.word	board_hardware_id
	.word	1073905728
	.word	.LC0
.LFE1:
	.size	GPIO_SETUP_learn_and_set_board_hardware_id, .-GPIO_SETUP_learn_and_set_board_hardware_id
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_gpio.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_uart.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/gpio_setup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x4ca
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF74
	.byte	0x1
	.4byte	.LASF75
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF56
	.byte	0x3
	.byte	0x5e
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0x8d
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.2byte	0x13c
	.byte	0x2
	.byte	0x38
	.4byte	0x32b
	.uleb128 0x8
	.4byte	.LASF10
	.byte	0x2
	.byte	0x3a
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF11
	.byte	0x2
	.byte	0x3b
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x8
	.4byte	.LASF12
	.byte	0x2
	.byte	0x3c
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x8
	.4byte	.LASF13
	.byte	0x2
	.byte	0x3d
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x8
	.4byte	.LASF14
	.byte	0x2
	.byte	0x3f
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x8
	.4byte	.LASF15
	.byte	0x2
	.byte	0x40
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x8
	.4byte	.LASF16
	.byte	0x2
	.byte	0x41
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x8
	.4byte	.LASF17
	.byte	0x2
	.byte	0x42
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x8
	.4byte	.LASF18
	.byte	0x2
	.byte	0x43
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x8
	.4byte	.LASF19
	.byte	0x2
	.byte	0x44
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x8
	.4byte	.LASF20
	.byte	0x2
	.byte	0x45
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x8
	.4byte	.LASF21
	.byte	0x2
	.byte	0x46
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x8
	.4byte	.LASF22
	.byte	0x2
	.byte	0x47
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x8
	.4byte	.LASF23
	.byte	0x2
	.byte	0x49
	.4byte	0x340
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x8
	.4byte	.LASF24
	.byte	0x2
	.byte	0x4b
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x8
	.4byte	.LASF25
	.byte	0x2
	.byte	0x4c
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x8
	.4byte	.LASF26
	.byte	0x2
	.byte	0x4d
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x8
	.4byte	.LASF27
	.byte	0x2
	.byte	0x4e
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x8
	.4byte	.LASF28
	.byte	0x2
	.byte	0x4f
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x8
	.4byte	.LASF29
	.byte	0x2
	.byte	0x50
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x8
	.4byte	.LASF30
	.byte	0x2
	.byte	0x51
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x8
	.4byte	.LASF31
	.byte	0x2
	.byte	0x53
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x8
	.4byte	.LASF32
	.byte	0x2
	.byte	0x55
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x8
	.4byte	.LASF33
	.byte	0x2
	.byte	0x56
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x8
	.4byte	.LASF34
	.byte	0x2
	.byte	0x57
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x8
	.4byte	.LASF35
	.byte	0x2
	.byte	0x58
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x8
	.4byte	.LASF36
	.byte	0x2
	.byte	0x59
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x8
	.4byte	.LASF37
	.byte	0x2
	.byte	0x5a
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x8
	.4byte	.LASF38
	.byte	0x2
	.byte	0x5b
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x8
	.4byte	.LASF39
	.byte	0x2
	.byte	0x5d
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x8
	.4byte	.LASF40
	.byte	0x2
	.byte	0x5f
	.4byte	0x355
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x8
	.4byte	.LASF41
	.byte	0x2
	.byte	0x61
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x100
	.uleb128 0x8
	.4byte	.LASF42
	.byte	0x2
	.byte	0x62
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x104
	.uleb128 0x8
	.4byte	.LASF43
	.byte	0x2
	.byte	0x63
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x108
	.uleb128 0x8
	.4byte	.LASF44
	.byte	0x2
	.byte	0x65
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x10c
	.uleb128 0x8
	.4byte	.LASF45
	.byte	0x2
	.byte	0x67
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x110
	.uleb128 0x8
	.4byte	.LASF46
	.byte	0x2
	.byte	0x68
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x114
	.uleb128 0x8
	.4byte	.LASF47
	.byte	0x2
	.byte	0x69
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x118
	.uleb128 0x8
	.4byte	.LASF48
	.byte	0x2
	.byte	0x6b
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x11c
	.uleb128 0x8
	.4byte	.LASF49
	.byte	0x2
	.byte	0x6d
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x120
	.uleb128 0x8
	.4byte	.LASF50
	.byte	0x2
	.byte	0x6e
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x124
	.uleb128 0x8
	.4byte	.LASF51
	.byte	0x2
	.byte	0x6f
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x128
	.uleb128 0x8
	.4byte	.LASF52
	.byte	0x2
	.byte	0x71
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x12c
	.uleb128 0x8
	.4byte	.LASF53
	.byte	0x2
	.byte	0x73
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x130
	.uleb128 0x8
	.4byte	.LASF54
	.byte	0x2
	.byte	0x74
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x134
	.uleb128 0x8
	.4byte	.LASF55
	.byte	0x2
	.byte	0x75
	.4byte	0x32b
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.byte	0
	.uleb128 0x9
	.4byte	0x48
	.uleb128 0x5
	.4byte	0x48
	.4byte	0x340
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x2
	.byte	0
	.uleb128 0x9
	.4byte	0x330
	.uleb128 0x5
	.4byte	0x48
	.4byte	0x355
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x1f
	.byte	0
	.uleb128 0x9
	.4byte	0x345
	.uleb128 0x3
	.4byte	.LASF57
	.byte	0x2
	.byte	0x77
	.4byte	0x8d
	.uleb128 0xa
	.byte	0xc
	.byte	0x4
	.byte	0x35
	.4byte	0x398
	.uleb128 0x8
	.4byte	.LASF58
	.byte	0x4
	.byte	0x37
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF59
	.byte	0x4
	.byte	0x38
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x8
	.4byte	.LASF60
	.byte	0x4
	.byte	0x39
	.4byte	0x32b
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF61
	.byte	0x4
	.byte	0x3a
	.4byte	0x365
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF62
	.uleb128 0x5
	.4byte	0x48
	.4byte	0x3ba
	.uleb128 0x6
	.4byte	0x6f
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF63
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF76
	.byte	0x1
	.byte	0x47
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x3f7
	.uleb128 0xc
	.4byte	.LASF64
	.byte	0x1
	.byte	0x52
	.4byte	0x3f7
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xc
	.4byte	.LASF65
	.byte	0x1
	.byte	0x62
	.4byte	0x3fd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xd
	.byte	0x4
	.4byte	0x398
	.uleb128 0xd
	.byte	0x4
	.4byte	0x35a
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF77
	.byte	0x1
	.byte	0xf0
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0xc
	.4byte	.LASF66
	.byte	0x5
	.byte	0x30
	.4byte	0x429
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xf
	.4byte	0x7d
	.uleb128 0xc
	.4byte	.LASF67
	.byte	0x5
	.byte	0x34
	.4byte	0x43f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xf
	.4byte	0x7d
	.uleb128 0xc
	.4byte	.LASF68
	.byte	0x5
	.byte	0x36
	.4byte	0x455
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xf
	.4byte	0x7d
	.uleb128 0xc
	.4byte	.LASF69
	.byte	0x5
	.byte	0x38
	.4byte	0x46b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xf
	.4byte	0x7d
	.uleb128 0x10
	.4byte	.LASF72
	.byte	0x7
	.byte	0x41
	.4byte	0x48
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF70
	.byte	0x6
	.byte	0x33
	.4byte	0x48e
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xf
	.4byte	0x330
	.uleb128 0xc
	.4byte	.LASF71
	.byte	0x6
	.byte	0x3f
	.4byte	0x4a4
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xf
	.4byte	0x3aa
	.uleb128 0x11
	.4byte	.LASF73
	.byte	0x1
	.byte	0x2b
	.4byte	0x48
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	display_model_is
	.uleb128 0x11
	.4byte	.LASF72
	.byte	0x1
	.byte	0x2f
	.4byte	0x48
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	board_hardware_id
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF59:
	.ascii	"clkmode\000"
.LASF74:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF30:
	.ascii	"p0_dir_state\000"
.LASF29:
	.ascii	"p0_dir_clr\000"
.LASF10:
	.ascii	"p3_inp_state\000"
.LASF37:
	.ascii	"p1_dir_clr\000"
.LASF4:
	.ascii	"short int\000"
.LASF71:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF15:
	.ascii	"p2_dir_clr\000"
.LASF17:
	.ascii	"p2_inp_state\000"
.LASF64:
	.ascii	"ucntrl_ptr\000"
.LASF57:
	.ascii	"GPIO_REGS_T\000"
.LASF61:
	.ascii	"UART_CNTL_REGS_T\000"
.LASF77:
	.ascii	"GPIO_SETUP_learn_and_set_board_hardware_id\000"
.LASF65:
	.ascii	"Lgpio\000"
.LASF73:
	.ascii	"display_model_is\000"
.LASF33:
	.ascii	"p1_outp_set\000"
.LASF12:
	.ascii	"p3_outp_clr\000"
.LASF27:
	.ascii	"p0_outp_state\000"
.LASF62:
	.ascii	"float\000"
.LASF7:
	.ascii	"long long int\000"
.LASF51:
	.ascii	"p0_mux_state\000"
.LASF28:
	.ascii	"p0_dir_set\000"
.LASF9:
	.ascii	"long int\000"
.LASF23:
	.ascii	"reserved1\000"
.LASF36:
	.ascii	"p1_dir_set\000"
.LASF39:
	.ascii	"reserved3\000"
.LASF40:
	.ascii	"reserved4\000"
.LASF19:
	.ascii	"p2_outp_clr\000"
.LASF48:
	.ascii	"reserved6\000"
.LASF52:
	.ascii	"reserved7\000"
.LASF14:
	.ascii	"p2_dir_set\000"
.LASF47:
	.ascii	"p3_mux_state\000"
.LASF50:
	.ascii	"p0_mux_clr\000"
.LASF68:
	.ascii	"GuiFont_DecimalChar\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF54:
	.ascii	"p1_mux_clr\000"
.LASF2:
	.ascii	"signed char\000"
.LASF6:
	.ascii	"long long unsigned int\000"
.LASF21:
	.ascii	"p2_mux_clr\000"
.LASF67:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF22:
	.ascii	"p2_mux_state\000"
.LASF5:
	.ascii	"unsigned int\000"
.LASF8:
	.ascii	"long unsigned int\000"
.LASF11:
	.ascii	"p3_outp_set\000"
.LASF75:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/gpio_setup.c\000"
.LASF26:
	.ascii	"p0_outp_clr\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF41:
	.ascii	"p_mux_set\000"
.LASF0:
	.ascii	"char\000"
.LASF55:
	.ascii	"p1_mux_state\000"
.LASF42:
	.ascii	"p_mux_clr\000"
.LASF44:
	.ascii	"reserved5\000"
.LASF69:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF46:
	.ascii	"p3_mux_clr\000"
.LASF66:
	.ascii	"GuiFont_LanguageActive\000"
.LASF18:
	.ascii	"p2_outp_set\000"
.LASF76:
	.ascii	"gpio_setup\000"
.LASF32:
	.ascii	"p1_inp_state\000"
.LASF49:
	.ascii	"p0_mux_set\000"
.LASF58:
	.ascii	"ctrl\000"
.LASF53:
	.ascii	"p1_mux_set\000"
.LASF20:
	.ascii	"p2_mux_set\000"
.LASF35:
	.ascii	"p1_outp_state\000"
.LASF70:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF16:
	.ascii	"p2_dir_state\000"
.LASF24:
	.ascii	"p0_inp_state\000"
.LASF45:
	.ascii	"p3_mux_set\000"
.LASF72:
	.ascii	"board_hardware_id\000"
.LASF25:
	.ascii	"p0_outp_set\000"
.LASF31:
	.ascii	"reserved2\000"
.LASF60:
	.ascii	"loop\000"
.LASF13:
	.ascii	"p3_outp_state\000"
.LASF63:
	.ascii	"double\000"
.LASF43:
	.ascii	"p_mux_state\000"
.LASF56:
	.ascii	"UNS_32\000"
.LASF38:
	.ascii	"p1_dir_state\000"
.LASF34:
	.ascii	"p1_outp_clr\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
