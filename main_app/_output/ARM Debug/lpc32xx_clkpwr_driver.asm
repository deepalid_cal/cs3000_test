	.file	"lpc32xx_clkpwr_driver.c"
	.text
.Ltext0:
	.global	clkpwr_base_clk
	.section	.data.clkpwr_base_clk,"aw",%progbits
	.align	2
	.type	clkpwr_base_clk, %object
	.size	clkpwr_base_clk, 136
clkpwr_base_clk:
	.word	4
	.word	4
	.word	4
	.word	4
	.word	4
	.word	4
	.word	3
	.word	4
	.word	4
	.word	4
	.word	4
	.word	4
	.word	1
	.word	1
	.word	1
	.word	1
	.word	5
	.word	5
	.word	5
	.word	5
	.word	5
	.word	5
	.word	5
	.word	5
	.word	4
	.word	4
	.word	4
	.word	4
	.word	5
	.word	5
	.word	5
	.word	5
	.word	4
	.word	8
	.section	.data.pll_postdivs,"aw",%progbits
	.align	2
	.type	pll_postdivs, %object
	.size	pll_postdivs, 16
pll_postdivs:
	.word	1
	.word	2
	.word	4
	.word	8
	.section	.data.hclkdivs,"aw",%progbits
	.align	2
	.type	hclkdivs, %object
	.size	hclkdivs, 16
hclkdivs:
	.word	1
	.word	2
	.word	4
	.word	4
	.section	.text.clkpwr_abs,"ax",%progbits
	.align	2
	.global	clkpwr_abs
	.type	clkpwr_abs, %function
clkpwr_abs:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.c"
	.loc 1 121 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 122 0
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	ble	.L2
	.loc 1 124 0
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-8]
	rsb	r3, r3, r2
	b	.L3
.L2:
	.loc 1 127 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-4]
	rsb	r3, r3, r2
.L3:
	.loc 1 128 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE0:
	.size	clkpwr_abs, .-clkpwr_abs
	.global	__udivdi3
	.section	.text.clkpwr_check_pll_setup,"ax",%progbits
	.align	2
	.global	clkpwr_check_pll_setup
	.type	clkpwr_check_pll_setup, %function
clkpwr_check_pll_setup:
.LFB1:
	.loc 1 152 0
	@ args = 0, pretend = 0, frame = 68
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI3:
	add	fp, sp, #8
.LCFI4:
	sub	sp, sp, #68
.LCFI5:
	str	r0, [fp, #-72]
	str	r1, [fp, #-76]
	.loc 1 153 0
	mov	r3, #0
	mov	r4, #0
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	mov	r3, #0
	mov	r4, #0
	str	r3, [fp, #-24]
	str	r4, [fp, #-20]
	mov	r3, #0
	mov	r4, #0
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
	.loc 1 163 0
	ldr	r2, [fp, #-72]
	mov	r3, r2
	mov	r4, #0
	str	r3, [fp, #-40]
	str	r4, [fp, #-36]
	.loc 1 164 0
	ldr	r3, [fp, #-76]
	ldr	r2, [r3, #24]
	mov	r3, r2
	mov	r4, #0
	str	r3, [fp, #-48]
	str	r4, [fp, #-44]
	.loc 1 165 0
	ldr	r3, [fp, #-76]
	ldr	r2, [r3, #20]
	mov	r3, r2
	mov	r4, r3, asr #31
	str	r3, [fp, #-56]
	str	r4, [fp, #-52]
	.loc 1 166 0
	ldr	r3, [fp, #-76]
	ldr	r2, [r3, #16]
	mov	r3, r2
	mov	r4, r3, asr #31
	str	r3, [fp, #-64]
	str	r4, [fp, #-60]
	.loc 1 169 0
	ldr	r3, [fp, #-76]
	ldr	r3, [r3, #4]
	mov	r2, r3, asl #2
	.loc 1 170 0
	ldr	r3, [fp, #-76]
	ldr	r3, [r3, #8]
	mov	r3, r3, asl #1
	.loc 1 169 0
	orr	r2, r2, r3
	.loc 1 171 0
	ldr	r3, [fp, #-76]
	ldr	r3, [r3, #12]
	.loc 1 169 0
	orr	r3, r2, r3
	str	r3, [fp, #-68]
	.loc 1 172 0
	ldr	r3, [fp, #-68]
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L5
.L11:
	.word	.L6
	.word	.L7
	.word	.L8
	.word	.L8
	.word	.L9
	.word	.L9
	.word	.L10
	.word	.L10
.L6:
	.loc 1 175 0
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-40]
	mul	r2, r3, r2
	ldr	r3, [fp, #-36]
	ldr	r1, [fp, #-48]
	mul	r3, r1, r3
	add	r3, r2, r3
	ldr	ip, [fp, #-48]
	ldr	r0, [fp, #-40]
	umull	r1, r2, ip, r0
	add	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-60]
	ldr	r0, [fp, #-56]
	mul	r0, r3, r0
	ldr	r3, [fp, #-52]
	ldr	ip, [fp, #-64]
	mul	r3, ip, r3
	add	r0, r0, r3
	ldr	lr, [fp, #-64]
	ldr	ip, [fp, #-56]
	umull	r3, r4, lr, ip
	add	r0, r0, r4
	mov	r4, r0
	adds	r3, r3, r3
	adc	r4, r4, r4
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
	.loc 1 176 0
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-40]
	mul	r2, r3, r2
	ldr	r3, [fp, #-36]
	ldr	r1, [fp, #-48]
	mul	r3, r1, r3
	add	r2, r2, r3
	ldr	r0, [fp, #-48]
	ldr	r1, [fp, #-40]
	umull	r3, r4, r0, r1
	add	r2, r2, r4
	mov	r4, r2
	mov	r0, r3
	mov	r1, r4
	sub	r3, fp, #56
	ldmia	r3, {r2-r3}
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	.loc 1 177 0
	sub	r1, fp, #40
	ldmia	r1, {r0-r1}
	sub	r3, fp, #56
	ldmia	r3, {r2-r3}
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-24]
	str	r4, [fp, #-20]
	.loc 1 178 0
	b	.L5
.L7:
	.loc 1 181 0
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-40]
	mul	r2, r3, r2
	ldr	r3, [fp, #-36]
	ldr	r1, [fp, #-48]
	mul	r3, r1, r3
	add	r2, r2, r3
	ldr	r0, [fp, #-48]
	ldr	r1, [fp, #-40]
	umull	r3, r4, r0, r1
	add	r2, r2, r4
	mov	r4, r2
	mov	r0, r3
	mov	r1, r4
	sub	r3, fp, #56
	ldmia	r3, {r2-r3}
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
	.loc 1 182 0
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-40]
	mul	r2, r3, r2
	ldr	r3, [fp, #-36]
	ldr	r1, [fp, #-48]
	mul	r3, r1, r3
	add	r3, r2, r3
	ldr	ip, [fp, #-48]
	ldr	r0, [fp, #-40]
	umull	r1, r2, ip, r0
	add	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-52]
	ldr	r0, [fp, #-64]
	mul	r0, r3, r0
	ldr	r3, [fp, #-60]
	ldr	ip, [fp, #-56]
	mul	r3, ip, r3
	add	r0, r0, r3
	ldr	lr, [fp, #-56]
	ldr	ip, [fp, #-64]
	umull	r3, r4, lr, ip
	add	r0, r0, r4
	mov	r4, r0
	adds	r3, r3, r3
	adc	r4, r4, r4
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	.loc 1 183 0
	sub	r1, fp, #40
	ldmia	r1, {r0-r1}
	sub	r3, fp, #56
	ldmia	r3, {r2-r3}
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-24]
	str	r4, [fp, #-20]
	.loc 1 184 0
	b	.L5
.L8:
	.loc 1 188 0
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-40]
	mul	r2, r3, r2
	ldr	r3, [fp, #-36]
	ldr	r1, [fp, #-48]
	mul	r3, r1, r3
	add	r2, r2, r3
	ldr	r0, [fp, #-48]
	ldr	r1, [fp, #-40]
	umull	r3, r4, r0, r1
	add	r2, r2, r4
	mov	r4, r2
	mov	r0, r3
	mov	r1, r4
	sub	r3, fp, #56
	ldmia	r3, {r2-r3}
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
	.loc 1 189 0
	sub	r4, fp, #32
	ldmia	r4, {r3-r4}
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	.loc 1 190 0
	sub	r1, fp, #40
	ldmia	r1, {r0-r1}
	sub	r3, fp, #56
	ldmia	r3, {r2-r3}
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-24]
	str	r4, [fp, #-20]
	.loc 1 191 0
	b	.L5
.L9:
	.loc 1 195 0
	sub	r4, fp, #64
	ldmia	r4, {r3-r4}
	adds	r3, r3, r3
	adc	r4, r4, r4
	sub	r1, fp, #40
	ldmia	r1, {r0-r1}
	mov	r2, r3
	mov	r3, r4
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
	.loc 1 196 0
	adr	r4, .L16
	ldmia	r4, {r3-r4}
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	.loc 1 197 0
	mov	r3, #999424
	add	r3, r3, #576
	mov	r4, #0
	str	r3, [fp, #-24]
	str	r4, [fp, #-20]
	.loc 1 198 0
	b	.L5
.L10:
	.loc 1 202 0
	sub	r4, fp, #40
	ldmia	r4, {r3-r4}
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
	.loc 1 203 0
	adr	r4, .L16
	ldmia	r4, {r3-r4}
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	.loc 1 204 0
	mov	r3, #999424
	add	r3, r3, #576
	mov	r4, #0
	str	r3, [fp, #-24]
	str	r4, [fp, #-20]
	.loc 1 205 0
	mov	r0, r0	@ nop
.L5:
	.loc 1 208 0
	sub	r4, fp, #16
	ldmia	r4, {r3-r4}
	adr	r2, .L16+8
	ldmia	r2, {r1-r2}
	cmp	r2, r4
	cmpeq	r1, r3
	bcs	.L12
	.loc 1 208 0 is_stmt 0 discriminator 1
	sub	r4, fp, #16
	ldmia	r4, {r3-r4}
	adr	r2, .L16+16
	ldmia	r2, {r1-r2}
	cmp	r2, r4
	cmpeq	r1, r3
	bcs	.L13
.L12:
	.loc 1 211 0 is_stmt 1
	mov	r3, #0
	mov	r4, #0
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
.L13:
	.loc 1 214 0
	sub	r4, fp, #24
	ldmia	r4, {r3-r4}
	adr	r2, .L16+24
	ldmia	r2, {r1-r2}
	cmp	r2, r4
	cmpeq	r1, r3
	bcs	.L14
	.loc 1 214 0 is_stmt 0 discriminator 1
	sub	r4, fp, #24
	ldmia	r4, {r3-r4}
	mov	r1, #27000832
	sub	r1, r1, #832
	mov	r2, #0
	cmp	r2, r4
	cmpeq	r1, r3
	bcs	.L15
.L14:
	.loc 1 217 0 is_stmt 1
	mov	r3, #0
	mov	r4, #0
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
.L15:
	.loc 1 220 0
	ldr	r3, [fp, #-32]
	.loc 1 221 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L17:
	.align	2
.L16:
	.word	156000000
	.word	0
	.word	155999999
	.word	0
	.word	320000000
	.word	0
	.word	999999
	.word	0
.LFE1:
	.size	clkpwr_check_pll_setup, .-clkpwr_check_pll_setup
	.section	.text.clkpwr_pll_rate_from_val,"ax",%progbits
	.align	2
	.global	clkpwr_pll_rate_from_val
	.type	clkpwr_pll_rate_from_val, %function
clkpwr_pll_rate_from_val:
.LFB2:
	.loc 1 245 0
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #36
.LCFI8:
	str	r0, [fp, #-36]
	str	r1, [fp, #-40]
	.loc 1 249 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 250 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 251 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 252 0
	ldr	r3, [fp, #-40]
	and	r3, r3, #32768
	cmp	r3, #0
	beq	.L19
	.loc 1 254 0
	mov	r3, #1
	str	r3, [fp, #-28]
.L19:
	.loc 1 256 0
	ldr	r3, [fp, #-40]
	and	r3, r3, #16384
	cmp	r3, #0
	beq	.L20
	.loc 1 258 0
	mov	r3, #1
	str	r3, [fp, #-24]
.L20:
	.loc 1 260 0
	ldr	r3, [fp, #-40]
	and	r3, r3, #8192
	cmp	r3, #0
	beq	.L21
	.loc 1 262 0
	mov	r3, #1
	str	r3, [fp, #-20]
.L21:
	.loc 1 264 0
	ldr	r3, [fp, #-40]
	mov	r3, r3, lsr #1
	and	r3, r3, #255
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 265 0
	ldr	r3, [fp, #-40]
	mov	r3, r3, lsr #9
	and	r3, r3, #3
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 266 0
	ldr	r3, [fp, #-40]
	mov	r3, r3, lsr #11
	and	r2, r3, #3
	ldr	r3, .L22
	ldr	r3, [r3, r2, asl #2]
	str	r3, [fp, #-16]
	.loc 1 268 0
	sub	r3, fp, #32
	ldr	r0, [fp, #-36]
	mov	r1, r3
	bl	clkpwr_check_pll_setup
	mov	r3, r0
	.loc 1 269 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L23:
	.align	2
.L22:
	.word	pll_postdivs
.LFE2:
	.size	clkpwr_pll_rate_from_val, .-clkpwr_pll_rate_from_val
	.section	.text.clkpwr_pll_rate,"ax",%progbits
	.align	2
	.global	clkpwr_pll_rate
	.type	clkpwr_pll_rate, %function
clkpwr_pll_rate:
.LFB3:
	.loc 1 293 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 294 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	bl	clkpwr_pll_rate_from_val
	mov	r3, r0
	.loc 1 295 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE3:
	.size	clkpwr_pll_rate, .-clkpwr_pll_rate
	.section	.text.clkpwr_mask_and_set,"ax",%progbits
	.align	2
	.global	clkpwr_mask_and_set
	.type	clkpwr_mask_and_set, %function
clkpwr_mask_and_set:
.LFB4:
	.loc 1 321 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI12:
	add	fp, sp, #0
.LCFI13:
	sub	sp, sp, #16
.LCFI14:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 324 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	mvn	r3, r3
	and	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 325 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L26
	.loc 1 327 0
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
.L26:
	.loc 1 329 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 330 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE4:
	.size	clkpwr_mask_and_set, .-clkpwr_mask_and_set
	.section	.text.clkpwr_get_event_field,"ax",%progbits
	.align	2
	.global	clkpwr_get_event_field
	.type	clkpwr_get_event_field, %function
clkpwr_get_event_field:
.LFB5:
	.loc 1 356 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI15:
	add	fp, sp, #0
.LCFI16:
	sub	sp, sp, #16
.LCFI17:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 357 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 359 0
	ldr	r3, [fp, #-8]
	cmp	r3, #31
	bhi	.L28
	.loc 1 361 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L31
	str	r2, [r3, #0]
	.loc 1 362 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L31+4
	str	r2, [r3, #4]
	.loc 1 363 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L31+8
	str	r2, [r3, #8]
	.loc 1 364 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L31+12
	str	r2, [r3, #12]
	.loc 1 365 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 366 0
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L29
.L28:
	.loc 1 368 0
	ldr	r3, [fp, #-8]
	cmp	r3, #63
	bhi	.L30
	.loc 1 370 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L31+16
	str	r2, [r3, #0]
	.loc 1 371 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L31+20
	str	r2, [r3, #4]
	.loc 1 372 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L31+24
	str	r2, [r3, #8]
	.loc 1 373 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L31+28
	str	r2, [r3, #12]
	.loc 1 374 0
	ldr	r3, [fp, #-8]
	rsb	r3, r3, #64
	mov	r2, r3
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 375 0
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L29
.L30:
	.loc 1 377 0
	ldr	r3, [fp, #-8]
	cmp	r3, #95
	bhi	.L29
	.loc 1 379 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L31+32
	str	r2, [r3, #0]
	.loc 1 380 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 381 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 382 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 383 0
	ldr	r3, [fp, #-8]
	rsb	r3, r3, #96
	mov	r2, r3
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 384 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L29:
	.loc 1 387 0
	ldr	r3, [fp, #-4]
	.loc 1 388 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L32:
	.align	2
.L31:
	.word	1073758240
	.word	1073758248
	.word	1073758244
	.word	1073758252
	.word	1073758256
	.word	1073758264
	.word	1073758260
	.word	1073758268
	.word	1073758232
.LFE5:
	.size	clkpwr_get_event_field, .-clkpwr_get_event_field
	.section	.text.clkpwr_get_base_clock,"ax",%progbits
	.align	2
	.global	clkpwr_get_base_clock
	.type	clkpwr_get_base_clock, %function
clkpwr_get_base_clock:
.LFB6:
	.loc 1 410 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI18:
	add	fp, sp, #0
.LCFI19:
	sub	sp, sp, #8
.LCFI20:
	str	r0, [fp, #-8]
	.loc 1 411 0
	mov	r3, #10
	str	r3, [fp, #-4]
	.loc 1 413 0
	ldr	r3, [fp, #-8]
	cmp	r3, #34
	bhi	.L34
	.loc 1 416 0
	ldr	r3, .L46
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	str	r3, [fp, #-4]
	.loc 1 419 0
	ldr	r3, [fp, #-8]
	cmp	r3, #14
	beq	.L37
	cmp	r3, #15
	beq	.L38
	cmp	r3, #13
	bne	.L42
.L36:
	.loc 1 423 0
	ldr	r3, .L46+4
	ldr	r3, [r3, #96]
	and	r3, r3, #256
	cmp	r3, #0
	beq	.L43
	.loc 1 426 0
	mov	r3, #5
	str	r3, [fp, #-4]
	.loc 1 428 0
	b	.L43
.L37:
	.loc 1 432 0
	ldr	r3, .L46+4
	ldr	r3, [r3, #184]
	and	r3, r3, #8
	cmp	r3, #0
	beq	.L44
	.loc 1 435 0
	mov	r3, #5
	str	r3, [fp, #-4]
	.loc 1 437 0
	b	.L44
.L38:
	.loc 1 441 0
	ldr	r3, .L46+4
	ldr	r3, [r3, #184]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L45
	.loc 1 444 0
	mov	r3, #5
	str	r3, [fp, #-4]
	.loc 1 446 0
	b	.L45
.L42:
	.loc 1 449 0
	b	.L34
.L43:
	.loc 1 428 0
	mov	r0, r0	@ nop
	b	.L34
.L44:
	.loc 1 437 0
	mov	r0, r0	@ nop
	b	.L34
.L45:
	.loc 1 446 0
	mov	r0, r0	@ nop
.L34:
	.loc 1 454 0
	ldr	r3, [fp, #-4]
	.loc 1 455 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L47:
	.align	2
.L46:
	.word	clkpwr_base_clk
	.word	1073758208
.LFE6:
	.size	clkpwr_get_base_clock, .-clkpwr_get_base_clock
	.section	.text.clkpwr_find_pll_cfg,"ax",%progbits
	.align	2
	.global	clkpwr_find_pll_cfg
	.type	clkpwr_find_pll_cfg, %function
clkpwr_find_pll_cfg:
.LFB7:
	.loc 1 490 0
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, fp, lr}
.LCFI21:
	add	fp, sp, #16
.LCFI22:
	sub	sp, sp, #56
.LCFI23:
	str	r0, [fp, #-60]
	str	r1, [fp, #-64]
	str	r2, [fp, #-68]
	str	r3, [fp, #-72]
	.loc 1 491 0
	mov	r3, #0
	str	r3, [fp, #-32]
	.loc 1 492 0
	mov	r3, #0
	str	r3, [fp, #-36]
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 1 496 0
	ldr	r3, [fp, #-64]
	mov	r5, r3
	mov	r6, #0
	ldr	r3, [fp, #-68]
	mov	r0, r3
	mov	r1, r0, asr #31
	mul	r2, r0, r6
	mul	r3, r5, r1
	add	r2, r2, r3
	umull	r3, r4, r5, r0
	add	r2, r2, r4
	mov	r4, r2
	mov	r5, #1000
	mov	r6, #0
	mov	r0, r3
	mov	r1, r4
	mov	r2, r5
	mov	r3, r6
	bl	__udivdi3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-48]
	str	r4, [fp, #-44]
	.loc 1 497 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-52]
	.loc 1 500 0
	ldr	r3, [fp, #-60]
	str	r3, [fp, #-56]
	.loc 1 503 0
	ldr	r2, [fp, #-60]
	ldr	r3, [fp, #-64]
	mov	r0, r2
	mov	r1, r3
	bl	clkpwr_abs
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-52]
	cmp	r2, r3
	bhi	.L49
	.loc 1 505 0
	mov	r3, #1
	str	r3, [fp, #-36]
	.loc 1 506 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 507 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #4]
	.loc 1 508 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #8]
	.loc 1 509 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #12]
	.loc 1 510 0
	ldr	r3, .L82
	ldr	r3, [r3, #0]
	mov	r2, r3
	ldr	r3, [fp, #-72]
	str	r2, [r3, #16]
	.loc 1 511 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #20]
	.loc 1 512 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #24]
	.loc 1 513 0
	ldr	r0, [fp, #-56]
	ldr	r1, [fp, #-72]
	bl	clkpwr_check_pll_setup
	str	r0, [fp, #-32]
	b	.L50
.L49:
	.loc 1 515 0
	ldr	r2, [fp, #-64]
	ldr	r3, [fp, #-56]
	cmp	r2, r3
	bhi	.L50
	.loc 1 517 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 518 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #4]
	.loc 1 519 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 520 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #12]
	.loc 1 521 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #20]
	.loc 1 522 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #24]
	.loc 1 523 0
	mov	r3, #0
	str	r3, [fp, #-28]
	b	.L51
.L53:
	.loc 1 525 0
	ldr	r3, .L82
	ldr	r2, [fp, #-28]
	ldr	r3, [r3, r2, asl #2]
	mov	r2, r3
	ldr	r3, [fp, #-72]
	str	r2, [r3, #16]
	.loc 1 526 0
	ldr	r0, [fp, #-56]
	ldr	r1, [fp, #-72]
	bl	clkpwr_check_pll_setup
	str	r0, [fp, #-32]
	.loc 1 527 0
	ldr	r2, [fp, #-64]
	ldr	r3, [fp, #-32]
	mov	r0, r2
	mov	r1, r3
	bl	clkpwr_abs
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-52]
	cmp	r2, r3
	bhi	.L52
	.loc 1 530 0
	mov	r3, #1
	str	r3, [fp, #-36]
.L52:
	.loc 1 523 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
.L51:
	.loc 1 523 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #3
	bhi	.L50
	.loc 1 523 0 discriminator 2
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L53
.L50:
	.loc 1 536 0 is_stmt 1
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	bne	.L54
	.loc 1 538 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 539 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 540 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #8]
	.loc 1 541 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 542 0
	ldr	r3, .L82
	ldr	r3, [r3, #0]
	mov	r2, r3
	ldr	r3, [fp, #-72]
	str	r2, [r3, #16]
	.loc 1 543 0
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L55
.L60:
	.loc 1 545 0
	mov	r3, #1
	str	r3, [fp, #-24]
	b	.L56
.L59:
	.loc 1 548 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-72]
	str	r2, [r3, #20]
	.loc 1 549 0
	ldr	r3, [fp, #-72]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #24]
	.loc 1 550 0
	ldr	r0, [fp, #-56]
	ldr	r1, [fp, #-72]
	bl	clkpwr_check_pll_setup
	str	r0, [fp, #-32]
	.loc 1 551 0
	ldr	r2, [fp, #-64]
	ldr	r3, [fp, #-32]
	mov	r0, r2
	mov	r1, r3
	bl	clkpwr_abs
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-52]
	cmp	r2, r3
	bhi	.L57
	.loc 1 553 0
	mov	r3, #1
	str	r3, [fp, #-36]
.L57:
	.loc 1 545 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
.L56:
	.loc 1 545 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #4
	bhi	.L58
	.loc 1 545 0 discriminator 2
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L59
.L58:
	.loc 1 543 0 is_stmt 1
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L55:
	.loc 1 543 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #256
	bhi	.L54
	.loc 1 543 0 discriminator 2
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L60
.L54:
	.loc 1 560 0 is_stmt 1
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	bne	.L61
	.loc 1 564 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 565 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 566 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 567 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #12]
	.loc 1 568 0
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L62
.L70:
	.loc 1 570 0
	mov	r3, #1
	str	r3, [fp, #-24]
	b	.L63
.L69:
	.loc 1 572 0
	mov	r3, #0
	str	r3, [fp, #-28]
	b	.L64
.L67:
	.loc 1 575 0
	ldr	r3, .L82
	ldr	r2, [fp, #-28]
	ldr	r3, [r3, r2, asl #2]
	mov	r2, r3
	ldr	r3, [fp, #-72]
	str	r2, [r3, #16]
	.loc 1 576 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-72]
	str	r2, [r3, #20]
	.loc 1 577 0
	ldr	r3, [fp, #-72]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #24]
	.loc 1 578 0
	ldr	r0, [fp, #-56]
	ldr	r1, [fp, #-72]
	bl	clkpwr_check_pll_setup
	str	r0, [fp, #-32]
	.loc 1 579 0
	ldr	r2, [fp, #-64]
	ldr	r3, [fp, #-32]
	mov	r0, r2
	mov	r1, r3
	bl	clkpwr_abs
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-52]
	cmp	r2, r3
	bhi	.L65
	.loc 1 581 0
	mov	r3, #1
	str	r3, [fp, #-36]
.L65:
	.loc 1 572 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
.L64:
	.loc 1 572 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #3
	bhi	.L66
	.loc 1 572 0 discriminator 2
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L67
.L66:
	.loc 1 570 0 is_stmt 1
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
.L63:
	.loc 1 570 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #4
	bhi	.L68
	.loc 1 570 0 discriminator 2
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L69
.L68:
	.loc 1 568 0 is_stmt 1
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L62:
	.loc 1 568 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #256
	bhi	.L61
	.loc 1 568 0 discriminator 2
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L70
.L61:
	.loc 1 588 0 is_stmt 1
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	bne	.L71
	.loc 1 591 0
	ldr	r3, [fp, #-72]
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 592 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 593 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 594 0
	ldr	r3, [fp, #-72]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 595 0
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L72
.L80:
	.loc 1 597 0
	mov	r3, #1
	str	r3, [fp, #-24]
	b	.L73
.L79:
	.loc 1 599 0
	mov	r3, #0
	str	r3, [fp, #-28]
	b	.L74
.L77:
	.loc 1 602 0
	ldr	r3, .L82
	ldr	r2, [fp, #-28]
	ldr	r3, [r3, r2, asl #2]
	mov	r2, r3
	ldr	r3, [fp, #-72]
	str	r2, [r3, #16]
	.loc 1 603 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-72]
	str	r2, [r3, #20]
	.loc 1 604 0
	ldr	r3, [fp, #-72]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #24]
	.loc 1 605 0
	ldr	r0, [fp, #-56]
	ldr	r1, [fp, #-72]
	bl	clkpwr_check_pll_setup
	str	r0, [fp, #-32]
	.loc 1 606 0
	ldr	r2, [fp, #-64]
	ldr	r3, [fp, #-32]
	mov	r0, r2
	mov	r1, r3
	bl	clkpwr_abs
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-52]
	cmp	r2, r3
	bhi	.L75
	.loc 1 608 0
	mov	r3, #1
	str	r3, [fp, #-36]
.L75:
	.loc 1 599 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
.L74:
	.loc 1 599 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #3
	bhi	.L76
	.loc 1 599 0 discriminator 2
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L77
.L76:
	.loc 1 597 0 is_stmt 1
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
.L73:
	.loc 1 597 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #4
	bhi	.L78
	.loc 1 597 0 discriminator 2
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L79
.L78:
	.loc 1 595 0 is_stmt 1
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L72:
	.loc 1 595 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #256
	bhi	.L71
	.loc 1 595 0 discriminator 2
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L80
.L71:
	.loc 1 615 0 is_stmt 1
	ldr	r3, [fp, #-36]
	cmp	r3, #1
	bne	.L81
	.loc 1 617 0
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-40]
.L81:
	.loc 1 620 0
	ldr	r3, [fp, #-40]
	.loc 1 621 0
	mov	r0, r3
	sub	sp, fp, #16
	ldmfd	sp!, {r4, r5, r6, fp, pc}
.L83:
	.align	2
.L82:
	.word	pll_postdivs
.LFE7:
	.size	clkpwr_find_pll_cfg, .-clkpwr_find_pll_cfg
	.section	.text.clkpwr_pll397_setup,"ax",%progbits
	.align	2
	.global	clkpwr_pll397_setup
	.type	clkpwr_pll397_setup, %function
clkpwr_pll397_setup:
.LFB8:
	.loc 1 647 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI24:
	add	fp, sp, #0
.LCFI25:
	sub	sp, sp, #16
.LCFI26:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 650 0
	ldr	r3, .L87
	ldr	r3, [r3, #72]
	bic	r3, r3, #960
	bic	r3, r3, #2
	str	r3, [fp, #-4]
	.loc 1 654 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L85
	.loc 1 656 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #512
	str	r3, [fp, #-4]
.L85:
	.loc 1 658 0
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 660 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L86
	.loc 1 662 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #2
	str	r3, [fp, #-4]
.L86:
	.loc 1 665 0
	ldr	r3, .L87
	ldr	r2, [fp, #-4]
	str	r2, [r3, #72]
	.loc 1 666 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L88:
	.align	2
.L87:
	.word	1073758208
.LFE8:
	.size	clkpwr_pll397_setup, .-clkpwr_pll397_setup
	.section	.text.clkpwr_hclkpll_setup,"ax",%progbits
	.align	2
	.global	clkpwr_hclkpll_setup
	.type	clkpwr_hclkpll_setup, %function
clkpwr_hclkpll_setup:
.LFB9:
	.loc 1 688 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #12
.LCFI29:
	str	r0, [fp, #-16]
	.loc 1 689 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 691 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L90
	.loc 1 693 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #65536
	str	r3, [fp, #-12]
.L90:
	.loc 1 695 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L91
	.loc 1 697 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #32768
	str	r3, [fp, #-12]
.L91:
	.loc 1 699 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L92
	.loc 1 701 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #16384
	str	r3, [fp, #-12]
.L92:
	.loc 1 703 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #12]
	cmp	r3, #0
	beq	.L93
	.loc 1 705 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #8192
	str	r3, [fp, #-12]
.L93:
	.loc 1 708 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #16]
	sub	r3, r3, #1
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L94
.L99:
	.word	.L95
	.word	.L96
	.word	.L94
	.word	.L97
	.word	.L94
	.word	.L94
	.word	.L94
	.word	.L98
.L95:
	.loc 1 711 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 712 0
	b	.L100
.L96:
	.loc 1 715 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 716 0
	b	.L100
.L97:
	.loc 1 719 0
	mov	r3, #2
	str	r3, [fp, #-8]
	.loc 1 720 0
	b	.L100
.L98:
	.loc 1 723 0
	mov	r3, #3
	str	r3, [fp, #-8]
	.loc 1 724 0
	b	.L100
.L94:
	.loc 1 727 0
	mov	r3, #0
	b	.L101
.L100:
	.loc 1 729 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #3
	mov	r3, r3, asl #11
	ldr	r2, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 730 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #20]
	sub	r3, r3, #1
	and	r3, r3, #3
	mov	r3, r3, asl #9
	ldr	r2, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 731 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #24]
	sub	r3, r3, #1
	and	r3, r3, #255
	mov	r3, r3, asl #1
	ldr	r2, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 733 0
	ldr	r3, .L102
	ldr	r2, [fp, #-12]
	str	r2, [r3, #88]
	.loc 1 735 0
	mov	r0, #2
	bl	clkpwr_get_base_clock_rate
	mov	r3, r0
	mov	r0, r3
	ldr	r1, [fp, #-16]
	bl	clkpwr_check_pll_setup
	mov	r3, r0
.L101:
	.loc 1 737 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L103:
	.align	2
.L102:
	.word	1073758208
.LFE9:
	.size	clkpwr_hclkpll_setup, .-clkpwr_hclkpll_setup
	.section	.text.clkpwr_usbclkpll_setup,"ax",%progbits
	.align	2
	.global	clkpwr_usbclkpll_setup
	.type	clkpwr_usbclkpll_setup, %function
clkpwr_usbclkpll_setup:
.LFB10:
	.loc 1 760 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #12
.LCFI32:
	str	r0, [fp, #-16]
	.loc 1 761 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 763 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L105
	.loc 1 765 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #65536
	str	r3, [fp, #-12]
.L105:
	.loc 1 767 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L106
	.loc 1 769 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #32768
	str	r3, [fp, #-12]
.L106:
	.loc 1 771 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L107
	.loc 1 773 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #16384
	str	r3, [fp, #-12]
.L107:
	.loc 1 775 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #12]
	cmp	r3, #0
	beq	.L108
	.loc 1 777 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #8192
	str	r3, [fp, #-12]
.L108:
	.loc 1 780 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #16]
	sub	r3, r3, #1
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L109
.L114:
	.word	.L110
	.word	.L111
	.word	.L109
	.word	.L112
	.word	.L109
	.word	.L109
	.word	.L109
	.word	.L113
.L110:
	.loc 1 783 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 784 0
	b	.L115
.L111:
	.loc 1 787 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 788 0
	b	.L115
.L112:
	.loc 1 791 0
	mov	r3, #2
	str	r3, [fp, #-8]
	.loc 1 792 0
	b	.L115
.L113:
	.loc 1 795 0
	mov	r3, #3
	str	r3, [fp, #-8]
	.loc 1 796 0
	b	.L115
.L109:
	.loc 1 799 0
	mov	r3, #0
	b	.L116
.L115:
	.loc 1 801 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #3
	mov	r3, r3, asl #11
	ldr	r2, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 802 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #20]
	sub	r3, r3, #1
	and	r3, r3, #3
	mov	r3, r3, asl #9
	ldr	r2, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 803 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #24]
	sub	r3, r3, #1
	and	r3, r3, #255
	mov	r3, r3, asl #1
	ldr	r2, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 805 0
	ldr	r3, .L117
	ldr	r2, [fp, #-12]
	str	r2, [r3, #100]
	.loc 1 808 0
	mov	r0, #2
	bl	clkpwr_get_base_clock_rate
	mov	r3, r0
	mov	r0, r3
	ldr	r1, [fp, #-16]
	bl	clkpwr_check_pll_setup
	mov	r3, r0
.L116:
	.loc 1 810 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L118:
	.align	2
.L117:
	.word	1073758208
.LFE10:
	.size	clkpwr_usbclkpll_setup, .-clkpwr_usbclkpll_setup
	.section	.text.clkpwr_hclkpll_direct_setup,"ax",%progbits
	.align	2
	.global	clkpwr_hclkpll_direct_setup
	.type	clkpwr_hclkpll_direct_setup, %function
clkpwr_hclkpll_direct_setup:
.LFB11:
	.loc 1 832 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI33:
	add	fp, sp, #0
.LCFI34:
	sub	sp, sp, #4
.LCFI35:
	str	r0, [fp, #-4]
	.loc 1 833 0
	ldr	r3, .L120
	ldr	r2, [fp, #-4]
	str	r2, [r3, #88]
	.loc 1 834 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L121:
	.align	2
.L120:
	.word	1073758208
.LFE11:
	.size	clkpwr_hclkpll_direct_setup, .-clkpwr_hclkpll_direct_setup
	.section	.text.clkpwr_pll_dis_en,"ax",%progbits
	.align	2
	.global	clkpwr_pll_dis_en
	.type	clkpwr_pll_dis_en, %function
clkpwr_pll_dis_en:
.LFB12:
	.loc 1 858 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI36:
	add	fp, sp, #0
.LCFI37:
	sub	sp, sp, #8
.LCFI38:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 859 0
	ldr	r3, [fp, #-4]
	cmp	r3, #1
	beq	.L125
	cmp	r3, #1
	bcc	.L124
	cmp	r3, #2
	beq	.L126
	b	.L134
.L124:
	.loc 1 862 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L127
	.loc 1 864 0
	ldr	r3, .L135
	ldr	r2, .L135
	ldr	r2, [r2, #72]
	orr	r2, r2, #2
	str	r2, [r3, #72]
	.loc 1 870 0
	b	.L122
.L127:
	.loc 1 868 0
	ldr	r3, .L135
	ldr	r2, .L135
	ldr	r2, [r2, #72]
	bic	r2, r2, #2
	str	r2, [r3, #72]
	.loc 1 870 0
	b	.L122
.L125:
	.loc 1 873 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L130
	.loc 1 875 0
	ldr	r3, .L135
	ldr	r2, .L135
	ldr	r2, [r2, #88]
	orr	r2, r2, #65536
	str	r2, [r3, #88]
	.loc 1 881 0
	b	.L122
.L130:
	.loc 1 879 0
	ldr	r3, .L135
	ldr	r2, .L135
	ldr	r2, [r2, #88]
	bic	r2, r2, #65536
	str	r2, [r3, #88]
	.loc 1 881 0
	b	.L122
.L126:
	.loc 1 884 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L132
	.loc 1 886 0
	ldr	r3, .L135
	ldr	r2, .L135
	ldr	r2, [r2, #100]
	orr	r2, r2, #65536
	str	r2, [r3, #100]
	.loc 1 892 0
	b	.L122
.L132:
	.loc 1 890 0
	ldr	r3, .L135
	ldr	r2, .L135
	ldr	r2, [r2, #100]
	bic	r2, r2, #65536
	str	r2, [r3, #100]
	.loc 1 892 0
	b	.L122
.L134:
	.loc 1 895 0
	mov	r0, r0	@ nop
.L122:
	.loc 1 897 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L136:
	.align	2
.L135:
	.word	1073758208
.LFE12:
	.size	clkpwr_pll_dis_en, .-clkpwr_pll_dis_en
	.section	.text.clkpwr_is_pll_locked,"ax",%progbits
	.align	2
	.global	clkpwr_is_pll_locked
	.type	clkpwr_is_pll_locked, %function
clkpwr_is_pll_locked:
.LFB13:
	.loc 1 922 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI39:
	add	fp, sp, #0
.LCFI40:
	sub	sp, sp, #8
.LCFI41:
	str	r0, [fp, #-8]
	.loc 1 923 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 925 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	beq	.L140
	cmp	r3, #1
	bcc	.L139
	cmp	r3, #2
	beq	.L141
	b	.L148
.L139:
	.loc 1 928 0
	ldr	r3, .L152
	ldr	r3, [r3, #72]
	.loc 1 929 0
	and	r3, r3, #1
	and	r3, r3, #255
	.loc 1 928 0
	cmp	r3, #0
	beq	.L149
	.loc 1 931 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 933 0
	b	.L149
.L140:
	.loc 1 936 0
	ldr	r3, .L152
	ldr	r3, [r3, #88]
	.loc 1 937 0
	and	r3, r3, #1
	and	r3, r3, #255
	.loc 1 936 0
	cmp	r3, #0
	beq	.L144
	.loc 1 939 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 947 0
	b	.L150
.L144:
	.loc 1 941 0
	ldr	r3, .L152
	ldr	r3, [r3, #88]
	and	r3, r3, #65536
	cmp	r3, #0
	bne	.L150
	.loc 1 945 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 947 0
	b	.L150
.L141:
	.loc 1 950 0
	ldr	r3, .L152
	ldr	r3, [r3, #100]
	.loc 1 951 0
	and	r3, r3, #1
	and	r3, r3, #255
	.loc 1 950 0
	cmp	r3, #0
	beq	.L146
	.loc 1 953 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 961 0
	b	.L151
.L146:
	.loc 1 955 0
	ldr	r3, .L152
	ldr	r3, [r3, #100]
	and	r3, r3, #65536
	cmp	r3, #0
	bne	.L151
	.loc 1 959 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 961 0
	b	.L151
.L148:
	.loc 1 964 0
	b	.L143
.L149:
	.loc 1 933 0
	mov	r0, r0	@ nop
	b	.L143
.L150:
	.loc 1 947 0
	mov	r0, r0	@ nop
	b	.L143
.L151:
	.loc 1 961 0
	mov	r0, r0	@ nop
.L143:
	.loc 1 967 0
	ldr	r3, [fp, #-4]
	.loc 1 968 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L153:
	.align	2
.L152:
	.word	1073758208
.LFE13:
	.size	clkpwr_is_pll_locked, .-clkpwr_is_pll_locked
	.section	.text.clkpwr_mainosc_setup,"ax",%progbits
	.align	2
	.global	clkpwr_mainosc_setup
	.type	clkpwr_mainosc_setup, %function
clkpwr_mainosc_setup:
.LFB14:
	.loc 1 992 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI42:
	add	fp, sp, #0
.LCFI43:
	sub	sp, sp, #12
.LCFI44:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 996 0
	ldr	r3, .L156
	ldr	r3, [r3, #76]
	bic	r3, r3, #508
	bic	r3, r3, #3
	str	r3, [fp, #-4]
	.loc 1 999 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #127
	mov	r2, r3, asl #2
	ldr	r3, [fp, #-4]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 1001 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L155
	.loc 1 1003 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #1
	str	r3, [fp, #-4]
.L155:
	.loc 1 1006 0
	ldr	r3, .L156
	ldr	r2, [fp, #-4]
	str	r2, [r3, #76]
	.loc 1 1007 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L157:
	.align	2
.L156:
	.word	1073758208
.LFE14:
	.size	clkpwr_mainosc_setup, .-clkpwr_mainosc_setup
	.section	.text.clkpwr_sysclk_setup,"ax",%progbits
	.align	2
	.global	clkpwr_sysclk_setup
	.type	clkpwr_sysclk_setup, %function
clkpwr_sysclk_setup:
.LFB15:
	.loc 1 1031 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI45:
	add	fp, sp, #0
.LCFI46:
	sub	sp, sp, #12
.LCFI47:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1034 0
	ldr	r3, .L163
	ldr	r3, [r3, #80]
	bic	r3, r3, #4080
	bic	r3, r3, #14
	str	r3, [fp, #-4]
	.loc 1 1037 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, [fp, #-4]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 1038 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L162
.L160:
	.loc 1 1041 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #2
	str	r3, [fp, #-4]
	.loc 1 1042 0
	b	.L161
.L162:
	.loc 1 1046 0
	mov	r0, r0	@ nop
.L161:
	.loc 1 1049 0
	ldr	r3, .L163
	ldr	r2, [fp, #-4]
	str	r2, [r3, #80]
	.loc 1 1050 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L164:
	.align	2
.L163:
	.word	1073758208
.LFE15:
	.size	clkpwr_sysclk_setup, .-clkpwr_sysclk_setup
	.section	.text.clkpwr_get_osc,"ax",%progbits
	.align	2
	.global	clkpwr_get_osc
	.type	clkpwr_get_osc, %function
clkpwr_get_osc:
.LFB16:
	.loc 1 1071 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI48:
	add	fp, sp, #0
.LCFI49:
	sub	sp, sp, #4
.LCFI50:
	.loc 1 1072 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 1073 0
	ldr	r3, .L167
	ldr	r3, [r3, #80]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L166
	.loc 1 1075 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L166:
	.loc 1 1078 0
	ldr	r3, [fp, #-4]
	.loc 1 1079 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L168:
	.align	2
.L167:
	.word	1073758208
.LFE16:
	.size	clkpwr_get_osc, .-clkpwr_get_osc
	.section	.text.clkpwr_set_hclk_divs,"ax",%progbits
	.align	2
	.global	clkpwr_set_hclk_divs
	.type	clkpwr_set_hclk_divs, %function
clkpwr_set_hclk_divs:
.LFB17:
	.loc 1 1105 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI51:
	add	fp, sp, #0
.LCFI52:
	sub	sp, sp, #16
.LCFI53:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 1106 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 1108 0
	ldr	r3, [fp, #-16]
	cmp	r3, #1
	beq	.L171
	cmp	r3, #2
	beq	.L172
	b	.L174
.L171:
	.loc 1 1111 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 1112 0
	b	.L173
.L172:
	.loc 1 1115 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 1116 0
	b	.L173
.L174:
	.loc 1 1120 0
	mov	r3, #2
	str	r3, [fp, #-4]
	.loc 1 1121 0
	mov	r0, r0	@ nop
.L173:
	.loc 1 1124 0
	ldr	r3, .L175
	.loc 1 1125 0
	ldr	r2, [fp, #-12]
	sub	r2, r2, #1
	and	r2, r2, #31
	mov	r2, r2, asl #2
	.loc 1 1124 0
	mov	r1, r2
	ldr	r2, [fp, #-8]
	orr	r1, r1, r2
	.loc 1 1126 0
	ldr	r2, [fp, #-4]
	and	r2, r2, #3
	.loc 1 1125 0
	orr	r2, r1, r2
	.loc 1 1124 0
	str	r2, [r3, #64]
	.loc 1 1127 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L176:
	.align	2
.L175:
	.word	1073758208
.LFE17:
	.size	clkpwr_set_hclk_divs, .-clkpwr_set_hclk_divs
	.global	__udivsi3
	.section	.text.clkpwr_get_base_clock_rate,"ax",%progbits
	.align	2
	.global	clkpwr_get_base_clock_rate
	.type	clkpwr_get_base_clock_rate, %function
clkpwr_get_base_clock_rate:
.LFB18:
	.loc 1 1151 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI54:
	add	fp, sp, #4
.LCFI55:
	sub	sp, sp, #48
.LCFI56:
	str	r0, [fp, #-52]
	.loc 1 1156 0
	ldr	r3, .L200
	ldr	r3, [r3, #80]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L178
	.loc 1 1159 0
	ldr	r3, .L200+4
	str	r3, [fp, #-8]
	b	.L179
.L178:
	.loc 1 1163 0
	ldr	r3, .L200+8
	str	r3, [fp, #-8]
.L179:
	.loc 1 1167 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1168 0
	ldr	r3, .L200
	ldr	r3, [r3, #104]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L180
	.loc 1 1171 0
	ldr	r3, .L200
	ldr	r3, [r3, #64]
	and	r3, r3, #128
	cmp	r3, #0
	beq	.L181
	.loc 1 1173 0
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L182
.L181:
	.loc 1 1175 0
	ldr	r3, .L200
	ldr	r3, [r3, #64]
	and	r3, r3, #256
	cmp	r3, #0
	beq	.L182
	.loc 1 1178 0
	mov	r3, #2
	str	r3, [fp, #-16]
	b	.L182
.L180:
	.loc 1 1184 0
	ldr	r3, .L200
	ldr	r3, [r3, #64]
	and	r3, r3, #3
	str	r3, [fp, #-44]
	.loc 1 1185 0
	ldr	r3, .L200+12
	ldr	r2, [fp, #-44]
	ldr	r3, [r3, r2, asl #2]
	sub	r3, r3, #1
	str	r3, [fp, #-16]
.L182:
	.loc 1 1189 0
	ldr	r3, .L200
	ldr	r3, [r3, #68]
	and	r3, r3, #4
	cmp	r3, #0
	beq	.L183
	.loc 1 1194 0
	ldr	r0, [fp, #-8]
	ldr	r1, .L200+16
	bl	clkpwr_pll_rate
	str	r0, [fp, #-48]
	.loc 1 1198 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-12]
	.loc 1 1201 0
	ldr	r3, .L200
	ldr	r3, [r3, #64]
	mov	r3, r3, lsr #2
	and	r3, r3, #31
	add	r3, r3, #1
	str	r3, [fp, #-44]
	.loc 1 1202 0
	ldr	r0, [fp, #-48]
	ldr	r1, [fp, #-44]
	bl	__udivsi3
	mov	r3, r0
	str	r3, [fp, #-20]
	.loc 1 1206 0
	ldr	r3, .L200
	ldr	r3, [r3, #64]
	and	r2, r3, #3
	ldr	r3, .L200+12
	ldr	r3, [r3, r2, asl #2]
	.loc 1 1205 0
	ldr	r0, [fp, #-48]
	mov	r1, r3
	bl	__udivsi3
	mov	r3, r0
	str	r3, [fp, #-24]
	.loc 1 1209 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-28]
	b	.L184
.L183:
	.loc 1 1216 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-12]
	.loc 1 1219 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-20]
	.loc 1 1222 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-24]
	.loc 1 1225 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-28]
.L184:
	.loc 1 1229 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	ldr	r0, [fp, #-12]
	mov	r1, r3
	bl	__udivsi3
	mov	r3, r0
	str	r3, [fp, #-12]
	.loc 1 1232 0
	ldr	r3, .L200
	ldr	r3, [r3, #68]
	and	r3, r3, #1024
	cmp	r3, #0
	beq	.L185
	.loc 1 1235 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-32]
	.loc 1 1236 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-36]
	b	.L186
.L185:
	.loc 1 1241 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-32]
	.loc 1 1242 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-36]
.L186:
	.loc 1 1246 0
	ldr	r3, [fp, #-52]
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L187
.L198:
	.word	.L188
	.word	.L189
	.word	.L190
	.word	.L191
	.word	.L192
	.word	.L193
	.word	.L194
	.word	.L195
	.word	.L196
	.word	.L197
.L188:
	.loc 1 1250 0
	ldr	r3, .L200+8
	str	r3, [fp, #-40]
	.loc 1 1251 0
	b	.L199
.L189:
	.loc 1 1255 0
	mov	r3, #32768
	str	r3, [fp, #-40]
	.loc 1 1256 0
	b	.L199
.L190:
	.loc 1 1260 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-40]
	.loc 1 1261 0
	b	.L199
.L191:
	.loc 1 1264 0
	ldr	r3, [fp, #-36]
	str	r3, [fp, #-40]
	.loc 1 1265 0
	b	.L199
.L192:
	.loc 1 1268 0
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-40]
	.loc 1 1269 0
	b	.L199
.L193:
	.loc 1 1272 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-40]
	.loc 1 1273 0
	b	.L199
.L194:
	.loc 1 1276 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 1 1277 0
	b	.L199
.L195:
	.loc 1 1280 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 1 1281 0
	b	.L199
.L196:
	.loc 1 1284 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-40]
	.loc 1 1285 0
	b	.L199
.L197:
	.loc 1 1288 0
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-40]
	.loc 1 1289 0
	b	.L199
.L187:
	.loc 1 1292 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 1 1293 0
	mov	r0, r0	@ nop
.L199:
	.loc 1 1296 0
	ldr	r3, [fp, #-40]
	.loc 1 1297 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L201:
	.align	2
.L200:
	.word	1073758208
	.word	13008896
	.word	13000000
	.word	hclkdivs
	.word	1073758296
.LFE18:
	.size	clkpwr_get_base_clock_rate, .-clkpwr_get_base_clock_rate
	.section	.text.clkpwr_force_arm_hclk_to_pclk,"ax",%progbits
	.align	2
	.global	clkpwr_force_arm_hclk_to_pclk
	.type	clkpwr_force_arm_hclk_to_pclk, %function
clkpwr_force_arm_hclk_to_pclk:
.LFB19:
	.loc 1 1319 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI57:
	add	fp, sp, #0
.LCFI58:
	sub	sp, sp, #8
.LCFI59:
	str	r0, [fp, #-8]
	.loc 1 1322 0
	ldr	r3, .L204
	ldr	r3, [r3, #68]
	bic	r3, r3, #1024
	str	r3, [fp, #-4]
	.loc 1 1323 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L203
	.loc 1 1325 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #1024
	str	r3, [fp, #-4]
.L203:
	.loc 1 1328 0
	ldr	r3, .L204
	ldr	r2, [fp, #-4]
	str	r2, [r3, #68]
	.loc 1 1329 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L205:
	.align	2
.L204:
	.word	1073758208
.LFE19:
	.size	clkpwr_force_arm_hclk_to_pclk, .-clkpwr_force_arm_hclk_to_pclk
	.section	.text.clkpwr_set_mode,"ax",%progbits
	.align	2
	.global	clkpwr_set_mode
	.type	clkpwr_set_mode, %function
clkpwr_set_mode:
.LFB20:
	.loc 1 1353 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI60:
	add	fp, sp, #0
.LCFI61:
	sub	sp, sp, #8
.LCFI62:
	str	r0, [fp, #-8]
	.loc 1 1356 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	beq	.L209
	cmp	r3, #1
	bcc	.L208
	cmp	r3, #2
	beq	.L210
	b	.L212
.L208:
	.loc 1 1359 0
	ldr	r3, .L213
	ldr	r3, [r3, #68]
	orr	r3, r3, #4
	str	r3, [fp, #-4]
	.loc 1 1360 0
	ldr	r3, .L213
	ldr	r2, [fp, #-4]
	str	r2, [r3, #68]
	.loc 1 1361 0
	b	.L206
.L209:
	.loc 1 1364 0
	ldr	r3, .L213
	ldr	r3, [r3, #68]
	bic	r3, r3, #4
	str	r3, [fp, #-4]
	.loc 1 1365 0
	ldr	r3, .L213
	ldr	r2, [fp, #-4]
	str	r2, [r3, #68]
	.loc 1 1366 0
	b	.L206
.L210:
	.loc 1 1369 0
	ldr	r3, .L213
	ldr	r3, [r3, #68]
	orr	r3, r3, #1
	str	r3, [fp, #-4]
	.loc 1 1370 0
	ldr	r3, .L213
	ldr	r2, [fp, #-4]
	str	r2, [r3, #68]
	.loc 1 1371 0
	b	.L206
.L212:
	.loc 1 1374 0
	mov	r0, r0	@ nop
.L206:
	.loc 1 1376 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L214:
	.align	2
.L213:
	.word	1073758208
.LFE20:
	.size	clkpwr_set_mode, .-clkpwr_set_mode
	.section	.text.clkpwr_clk_en_dis,"ax",%progbits
	.align	2
	.global	clkpwr_clk_en_dis
	.type	clkpwr_clk_en_dis, %function
clkpwr_clk_en_dis:
.LFB21:
	.loc 1 1400 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI63:
	add	fp, sp, #4
.LCFI64:
	sub	sp, sp, #8
.LCFI65:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1401 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	cmp	r3, #32
	ldrls	pc, [pc, r3, asl #2]
	b	.L253
.L250:
	.word	.L217
	.word	.L218
	.word	.L219
	.word	.L220
	.word	.L221
	.word	.L222
	.word	.L223
	.word	.L224
	.word	.L225
	.word	.L226
	.word	.L227
	.word	.L228
	.word	.L229
	.word	.L230
	.word	.L231
	.word	.L232
	.word	.L233
	.word	.L234
	.word	.L235
	.word	.L236
	.word	.L237
	.word	.L238
	.word	.L239
	.word	.L240
	.word	.L241
	.word	.L242
	.word	.L243
	.word	.L244
	.word	.L245
	.word	.L246
	.word	.L247
	.word	.L248
	.word	.L249
.L217:
	.loc 1 1410 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255
	mov	r1, #32
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1412 0
	b	.L215
.L218:
	.loc 1 1415 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+4
	mov	r1, #2
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1417 0
	b	.L215
.L219:
	.loc 1 1420 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+4
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1422 0
	b	.L215
.L220:
	.loc 1 1425 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+8
	mov	r1, #2
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1427 0
	b	.L215
.L221:
	.loc 1 1430 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+8
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1432 0
	b	.L215
.L222:
	.loc 1 1435 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+12
	mov	r1, #32
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1437 0
	ldr	r3, .L255+16
	ldr	r3, [r3, #128]
	and	r3, r3, #15
	cmp	r3, #0
	bne	.L254
	.loc 1 1441 0
	ldr	r3, .L255+16
	ldr	r2, .L255+16
	ldr	r2, [r2, #128]
	orr	r2, r2, #1
	str	r2, [r3, #128]
	.loc 1 1443 0
	b	.L254
.L223:
	.loc 1 1446 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+20
	mov	r1, #4
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1448 0
	b	.L215
.L224:
	.loc 1 1451 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+20
	mov	r1, #2
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1453 0
	b	.L215
.L225:
	.loc 1 1456 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+20
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1458 0
	b	.L215
.L226:
	.loc 1 1461 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+24
	mov	r1, #2
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1463 0
	b	.L215
.L227:
	.loc 1 1466 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+24
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1468 0
	b	.L215
.L228:
	.loc 1 1471 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+28
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1473 0
	b	.L215
.L229:
	.loc 1 1476 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+32
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1478 0
	b	.L215
.L230:
	.loc 1 1481 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+36
	mov	r1, #4
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1483 0
	b	.L215
.L231:
	.loc 1 1486 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+36
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1488 0
	b	.L215
.L232:
	.loc 1 1491 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+40
	mov	r1, #2
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1493 0
	b	.L215
.L233:
	.loc 1 1496 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+40
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1498 0
	b	.L215
.L234:
	.loc 1 1501 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+44
	mov	r1, #32
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1503 0
	b	.L215
.L235:
	.loc 1 1506 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+44
	mov	r1, #16
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1508 0
	b	.L215
.L236:
	.loc 1 1511 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+44
	mov	r1, #8
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1513 0
	b	.L215
.L237:
	.loc 1 1516 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+44
	mov	r1, #4
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1518 0
	b	.L215
.L238:
	.loc 1 1521 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+44
	mov	r1, #2
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1523 0
	b	.L215
.L239:
	.loc 1 1526 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+44
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1528 0
	b	.L215
.L240:
	.loc 1 1531 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+48
	mov	r1, #16
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1533 0
	b	.L215
.L241:
	.loc 1 1536 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+48
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1538 0
	b	.L215
.L242:
	.loc 1 1541 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+52
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1543 0
	b	.L215
.L243:
	.loc 1 1546 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+52
	mov	r1, #2
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1548 0
	b	.L215
.L244:
	.loc 1 1551 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+56
	mov	r1, #8
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1553 0
	b	.L215
.L245:
	.loc 1 1556 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+56
	mov	r1, #4
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1558 0
	b	.L215
.L246:
	.loc 1 1561 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+56
	mov	r1, #2
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1563 0
	b	.L215
.L247:
	.loc 1 1566 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+56
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1568 0
	b	.L215
.L248:
	.loc 1 1571 0
	ldr	r3, [fp, #-12]
	ldr	r0, .L255+60
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1573 0
	b	.L215
.L249:
	.loc 1 1576 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, .L255+64
	mov	r1, #1
	mov	r2, r3
	bl	clkpwr_mask_and_set
	.loc 1 1578 0
	b	.L215
.L253:
	.loc 1 1581 0
	mov	r0, r0	@ nop
	b	.L215
.L254:
	.loc 1 1443 0
	mov	r0, r0	@ nop
.L215:
	.loc 1 1583 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L256:
	.align	2
.L255:
	.word	1073758292
	.word	1073758328
	.word	1073758332
	.word	1073758336
	.word	1073758208
	.word	1073758352
	.word	1073758380
	.word	1073758384
	.word	1073758388
	.word	1073758392
	.word	1073758396
	.word	1073758400
	.word	1073758404
	.word	1073758408
	.word	1073758436
	.word	1073758440
	.word	1073758312
.LFE21:
	.size	clkpwr_clk_en_dis, .-clkpwr_clk_en_dis
	.section	.text.clkpwr_autoclk_en_dis,"ax",%progbits
	.align	2
	.global	clkpwr_autoclk_en_dis
	.type	clkpwr_autoclk_en_dis, %function
clkpwr_autoclk_en_dis:
.LFB22:
	.loc 1 1607 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI66:
	add	fp, sp, #0
.LCFI67:
	sub	sp, sp, #12
.LCFI68:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1610 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	beq	.L260
	cmp	r3, #1
	bcc	.L259
	cmp	r3, #2
	beq	.L261
	b	.L266
.L259:
	.loc 1 1613 0
	ldr	r3, .L267
	ldr	r3, [r3, #236]
	bic	r3, r3, #64
	str	r3, [fp, #-4]
	.loc 1 1614 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L262
	.loc 1 1616 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #64
	str	r3, [fp, #-4]
.L262:
	.loc 1 1618 0
	ldr	r3, .L267
	ldr	r2, [fp, #-4]
	str	r2, [r3, #236]
	.loc 1 1619 0
	b	.L257
.L260:
	.loc 1 1622 0
	ldr	r3, .L267
	ldr	r3, [r3, #236]
	bic	r3, r3, #2
	str	r3, [fp, #-4]
	.loc 1 1623 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L264
	.loc 1 1625 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #2
	str	r3, [fp, #-4]
.L264:
	.loc 1 1627 0
	ldr	r3, .L267
	ldr	r2, [fp, #-4]
	str	r2, [r3, #236]
	.loc 1 1628 0
	b	.L257
.L261:
	.loc 1 1631 0
	ldr	r3, .L267
	ldr	r3, [r3, #236]
	bic	r3, r3, #1
	str	r3, [fp, #-4]
	.loc 1 1632 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L265
	.loc 1 1634 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #1
	str	r3, [fp, #-4]
.L265:
	.loc 1 1636 0
	ldr	r3, .L267
	ldr	r2, [fp, #-4]
	str	r2, [r3, #236]
	.loc 1 1637 0
	b	.L257
.L266:
	.loc 1 1640 0
	mov	r0, r0	@ nop
.L257:
	.loc 1 1642 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L268:
	.align	2
.L267:
	.word	1073758208
.LFE22:
	.size	clkpwr_autoclk_en_dis, .-clkpwr_autoclk_en_dis
	.section	.text.clkpwr_get_clock_rate,"ax",%progbits
	.align	2
	.global	clkpwr_get_clock_rate
	.type	clkpwr_get_clock_rate, %function
clkpwr_get_clock_rate:
.LFB23:
	.loc 1 1666 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI69:
	add	fp, sp, #4
.LCFI70:
	sub	sp, sp, #20
.LCFI71:
	str	r0, [fp, #-24]
	.loc 1 1668 0
	mov	r3, #0
	str	r3, [fp, #-8]
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 1671 0
	ldr	r0, [fp, #-24]
	bl	clkpwr_get_base_clock
	str	r0, [fp, #-16]
	.loc 1 1672 0
	ldr	r3, [fp, #-16]
	cmp	r3, #10
	beq	.L270
	.loc 1 1676 0
	ldr	r0, [fp, #-16]
	bl	clkpwr_get_base_clock_rate
	str	r0, [fp, #-8]
	.loc 1 1677 0
	ldr	r3, [fp, #-24]
	sub	r3, r3, #1
	cmp	r3, #14
	ldrls	pc, [pc, r3, asl #2]
	b	.L286
.L277:
	.word	.L272
	.word	.L286
	.word	.L286
	.word	.L286
	.word	.L286
	.word	.L273
	.word	.L286
	.word	.L286
	.word	.L286
	.word	.L286
	.word	.L286
	.word	.L286
	.word	.L274
	.word	.L275
	.word	.L276
.L273:
	.loc 1 1680 0
	ldr	r3, .L288
	ldr	r3, [r3, #128]
	and	r3, r3, #15
	str	r3, [fp, #-20]
	.loc 1 1682 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L278
	.loc 1 1684 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1690 0
	b	.L270
.L278:
	.loc 1 1688 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-12]
	.loc 1 1690 0
	b	.L270
.L272:
	.loc 1 1693 0
	ldr	r3, .L288
	ldr	r3, [r3, #84]
	and	r3, r3, #31
	str	r3, [fp, #-20]
	.loc 1 1695 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 1696 0
	b	.L270
.L274:
	.loc 1 1701 0
	ldr	r3, .L288
	ldr	r3, [r3, #96]
	str	r3, [fp, #-20]
	.loc 1 1702 0
	ldr	r3, [fp, #-20]
	and	r3, r3, #256
	cmp	r3, #0
	beq	.L287
	.loc 1 1704 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 1705 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L281
	.loc 1 1707 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1714 0
	b	.L287
.L281:
	.loc 1 1711 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-12]
	.loc 1 1714 0
	b	.L287
.L275:
	.loc 1 1717 0
	ldr	r3, .L288
	ldr	r3, [r3, #184]
	and	r3, r3, #15
	str	r3, [fp, #-20]
	.loc 1 1719 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L282
	.loc 1 1721 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1727 0
	b	.L270
.L282:
	.loc 1 1725 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-12]
	.loc 1 1727 0
	b	.L270
.L276:
	.loc 1 1730 0
	ldr	r3, .L288
	ldr	r3, [r3, #184]
	and	r3, r3, #15
	str	r3, [fp, #-20]
	.loc 1 1732 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L284
	.loc 1 1734 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1740 0
	b	.L270
.L284:
	.loc 1 1738 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-12]
	.loc 1 1740 0
	b	.L270
.L286:
	.loc 1 1743 0
	mov	r0, r0	@ nop
	b	.L270
.L287:
	.loc 1 1714 0
	mov	r0, r0	@ nop
.L270:
	.loc 1 1747 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	__udivsi3
	mov	r3, r0
	.loc 1 1748 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L289:
	.align	2
.L288:
	.word	1073758208
.LFE23:
	.size	clkpwr_get_clock_rate, .-clkpwr_get_clock_rate
	.section	.text.clkpwr_wk_event_en_dis,"ax",%progbits
	.align	2
	.global	clkpwr_wk_event_en_dis
	.type	clkpwr_wk_event_en_dis, %function
clkpwr_wk_event_en_dis:
.LFB24:
	.loc 1 1772 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI72:
	add	fp, sp, #4
.LCFI73:
	sub	sp, sp, #20
.LCFI74:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 1 1774 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1775 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1777 0
	sub	r3, fp, #16
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-8]
	mov	r2, r3
	bl	clkpwr_get_event_field
	mov	r3, r0
	cmp	r3, #0
	beq	.L295
.L291:
	.loc 1 1783 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 1784 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L293
	.loc 1 1786 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	mov	r2, r2, asl r3
	ldr	r3, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
	b	.L294
.L293:
	.loc 1 1790 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	mov	r3, r2, asl r3
	mvn	r2, r3
	ldr	r3, [fp, #-12]
	and	r3, r2, r3
	str	r3, [fp, #-12]
.L294:
	.loc 1 1793 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	b	.L290
.L295:
	.loc 1 1780 0
	mov	r0, r0	@ nop
.L290:
	.loc 1 1794 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE24:
	.size	clkpwr_wk_event_en_dis, .-clkpwr_wk_event_en_dis
	.section	.text.clkpwr_is_raw_event_active,"ax",%progbits
	.align	2
	.global	clkpwr_is_raw_event_active
	.type	clkpwr_is_raw_event_active, %function
clkpwr_is_raw_event_active:
.LFB25:
	.loc 1 1816 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI75:
	add	fp, sp, #4
.LCFI76:
	sub	sp, sp, #16
.LCFI77:
	str	r0, [fp, #-20]
	.loc 1 1817 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1818 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1819 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 1821 0
	sub	r3, fp, #16
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-12]
	mov	r2, r3
	bl	clkpwr_get_event_field
	mov	r3, r0
	cmp	r3, #0
	beq	.L297
	.loc 1 1823 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	mov	r3, r2, lsr r3
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L297
	.loc 1 1825 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L297:
	.loc 1 1829 0
	ldr	r3, [fp, #-8]
	.loc 1 1830 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE25:
	.size	clkpwr_is_raw_event_active, .-clkpwr_is_raw_event_active
	.section	.text.clkpwr_is_msk_event_active,"ax",%progbits
	.align	2
	.global	clkpwr_is_msk_event_active
	.type	clkpwr_is_msk_event_active, %function
clkpwr_is_msk_event_active:
.LFB26:
	.loc 1 1852 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI78:
	add	fp, sp, #4
.LCFI79:
	sub	sp, sp, #16
.LCFI80:
	str	r0, [fp, #-20]
	.loc 1 1853 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1854 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1855 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 1857 0
	sub	r3, fp, #16
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-12]
	mov	r2, r3
	bl	clkpwr_get_event_field
	mov	r3, r0
	cmp	r3, #0
	bne	.L299
	.loc 1 1859 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	mov	r3, r2, lsr r3
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L299
	.loc 1 1861 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L299:
	.loc 1 1865 0
	ldr	r3, [fp, #-8]
	.loc 1 1866 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE26:
	.size	clkpwr_is_msk_event_active, .-clkpwr_is_msk_event_active
	.section	.text.clkpwr_clear_event,"ax",%progbits
	.align	2
	.global	clkpwr_clear_event
	.type	clkpwr_clear_event, %function
clkpwr_clear_event:
.LFB27:
	.loc 1 1888 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI81:
	add	fp, sp, #4
.LCFI82:
	sub	sp, sp, #12
.LCFI83:
	str	r0, [fp, #-16]
	.loc 1 1889 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 1890 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1892 0
	sub	r3, fp, #12
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-8]
	mov	r2, r3
	bl	clkpwr_get_event_field
	mov	r3, r0
	cmp	r3, #0
	beq	.L303
.L301:
	.loc 1 1899 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	ldr	r2, [fp, #-12]
	mov	r1, #1
	mov	r2, r1, asl r2
	str	r2, [r3, #0]
	b	.L300
.L303:
	.loc 1 1895 0
	mov	r0, r0	@ nop
.L300:
	.loc 1 1900 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE27:
	.size	clkpwr_clear_event, .-clkpwr_clear_event
	.section	.text.clkpwr_set_event_pol,"ax",%progbits
	.align	2
	.global	clkpwr_set_event_pol
	.type	clkpwr_set_event_pol, %function
clkpwr_set_event_pol:
.LFB28:
	.loc 1 1925 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI84:
	add	fp, sp, #4
.LCFI85:
	sub	sp, sp, #20
.LCFI86:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 1 1927 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1928 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1930 0
	sub	r3, fp, #16
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-8]
	mov	r2, r3
	bl	clkpwr_get_event_field
	mov	r3, r0
	cmp	r3, #0
	beq	.L308
.L305:
	.loc 1 1937 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	mov	r1, #1
	mov	r3, r1, asl r3
	mvn	r3, r3
	and	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 1938 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L307
	.loc 1 1940 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	mov	r2, r2, asl r3
	ldr	r3, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
.L307:
	.loc 1 1942 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #12]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	b	.L304
.L308:
	.loc 1 1933 0
	mov	r0, r0	@ nop
.L304:
	.loc 1 1943 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE28:
	.size	clkpwr_set_event_pol, .-clkpwr_set_event_pol
	.section	.text.clkpwr_get_uid,"ax",%progbits
	.align	2
	.global	clkpwr_get_uid
	.type	clkpwr_get_uid, %function
clkpwr_get_uid:
.LFB29:
	.loc 1 1965 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI87:
	add	fp, sp, #0
.LCFI88:
	sub	sp, sp, #4
.LCFI89:
	str	r0, [fp, #-4]
	.loc 1 1966 0
	ldr	r3, .L310
	ldr	r2, [r3, #304]
	ldr	r3, [fp, #-4]
	str	r2, [r3, #0]
	.loc 1 1967 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #4
	ldr	r2, .L310
	ldr	r2, [r2, #308]
	str	r2, [r3, #0]
	.loc 1 1968 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #8
	ldr	r2, .L310
	ldr	r2, [r2, #312]
	str	r2, [r3, #0]
	.loc 1 1969 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #12
	ldr	r2, .L310
	ldr	r2, [r2, #316]
	str	r2, [r3, #0]
	.loc 1 1970 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L311:
	.align	2
.L310:
	.word	1073758208
.LFE29:
	.size	clkpwr_get_uid, .-clkpwr_get_uid
	.section	.text.clkpwr_set_mux,"ax",%progbits
	.align	2
	.global	clkpwr_set_mux
	.type	clkpwr_set_mux, %function
clkpwr_set_mux:
.LFB30:
	.loc 1 1996 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI90:
	add	fp, sp, #0
.LCFI91:
	sub	sp, sp, #8
.LCFI92:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 1997 0
	ldr	r3, [fp, #-4]
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L334
.L320:
	.word	.L314
	.word	.L315
	.word	.L316
	.word	.L317
	.word	.L318
	.word	.L318
	.word	.L319
	.word	.L319
.L314:
	.loc 1 2000 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L321
	.loc 1 2002 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #68]
	bic	r2, r2, #2
	str	r2, [r3, #68]
	.loc 1 2008 0
	b	.L312
.L321:
	.loc 1 2006 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #68]
	orr	r2, r2, #2
	str	r2, [r3, #68]
	.loc 1 2008 0
	b	.L312
.L315:
	.loc 1 2011 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L324
	.loc 1 2013 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #68]
	bic	r2, r2, #8
	str	r2, [r3, #68]
	.loc 1 2019 0
	b	.L312
.L324:
	.loc 1 2017 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #68]
	orr	r2, r2, #8
	str	r2, [r3, #68]
	.loc 1 2019 0
	b	.L312
.L316:
	.loc 1 2022 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L326
	.loc 1 2024 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #164]
	bic	r2, r2, #16
	str	r2, [r3, #164]
	.loc 1 2032 0
	b	.L312
.L326:
	.loc 1 2029 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #164]
	orr	r2, r2, #16
	str	r2, [r3, #164]
	.loc 1 2032 0
	b	.L312
.L317:
	.loc 1 2035 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L328
	.loc 1 2037 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #164]
	bic	r2, r2, #1
	str	r2, [r3, #164]
	.loc 1 2045 0
	b	.L312
.L328:
	.loc 1 2042 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #164]
	orr	r2, r2, #1
	str	r2, [r3, #164]
	.loc 1 2045 0
	b	.L312
.L318:
	.loc 1 2049 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L330
	.loc 1 2051 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #196]
	bic	r2, r2, #32
	str	r2, [r3, #196]
	.loc 1 2057 0
	b	.L312
.L330:
	.loc 1 2055 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #196]
	orr	r2, r2, #32
	str	r2, [r3, #196]
	.loc 1 2057 0
	b	.L312
.L319:
	.loc 1 2061 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L332
	.loc 1 2063 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #196]
	bic	r2, r2, #2
	str	r2, [r3, #196]
	.loc 1 2069 0
	b	.L312
.L332:
	.loc 1 2067 0
	ldr	r3, .L335
	ldr	r2, .L335
	ldr	r2, [r2, #196]
	orr	r2, r2, #2
	str	r2, [r3, #196]
	.loc 1 2069 0
	b	.L312
.L334:
	.loc 1 2072 0
	mov	r0, r0	@ nop
.L312:
	.loc 1 2074 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L336:
	.align	2
.L335:
	.word	1073758208
.LFE30:
	.size	clkpwr_set_mux, .-clkpwr_set_mux
	.section	.text.clkpwr_set_mux_state,"ax",%progbits
	.align	2
	.global	clkpwr_set_mux_state
	.type	clkpwr_set_mux_state, %function
clkpwr_set_mux_state:
.LFB31:
	.loc 1 2099 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI93:
	add	fp, sp, #0
.LCFI94:
	sub	sp, sp, #12
.LCFI95:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 2102 0
	ldr	r3, [fp, #-8]
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L361
.L347:
	.word	.L339
	.word	.L340
	.word	.L341
	.word	.L342
	.word	.L343
	.word	.L344
	.word	.L345
	.word	.L346
.L339:
	.loc 1 2105 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L348
	.loc 1 2107 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #68]
	bic	r2, r2, #32
	str	r2, [r3, #68]
	.loc 1 2113 0
	b	.L337
.L348:
	.loc 1 2111 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #68]
	orr	r2, r2, #32
	str	r2, [r3, #68]
	.loc 1 2113 0
	b	.L337
.L340:
	.loc 1 2116 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L351
	.loc 1 2118 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #68]
	bic	r2, r2, #16
	str	r2, [r3, #68]
	.loc 1 2124 0
	b	.L337
.L351:
	.loc 1 2122 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #68]
	orr	r2, r2, #16
	str	r2, [r3, #68]
	.loc 1 2124 0
	b	.L337
.L341:
	.loc 1 2127 0
	ldr	r3, .L362
	ldr	r3, [r3, #164]
	bic	r2, r3, #96
	.loc 1 2128 0
	ldr	r3, [fp, #-12]
	orr	r3, r2, r3
	.loc 1 2127 0
	str	r3, [fp, #-4]
	.loc 1 2129 0
	ldr	r3, .L362
	ldr	r2, [fp, #-4]
	str	r2, [r3, #164]
	.loc 1 2130 0
	b	.L337
.L342:
	.loc 1 2133 0
	ldr	r3, .L362
	ldr	r3, [r3, #164]
	bic	r2, r3, #14
	.loc 1 2134 0
	ldr	r3, [fp, #-12]
	orr	r3, r2, r3
	.loc 1 2133 0
	str	r3, [fp, #-4]
	.loc 1 2135 0
	ldr	r3, .L362
	ldr	r2, [fp, #-4]
	str	r2, [r3, #164]
	.loc 1 2136 0
	b	.L337
.L343:
	.loc 1 2139 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L353
	.loc 1 2141 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #196]
	bic	r2, r2, #128
	str	r2, [r3, #196]
	.loc 1 2149 0
	b	.L337
.L353:
	.loc 1 2146 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #196]
	orr	r2, r2, #128
	str	r2, [r3, #196]
	.loc 1 2149 0
	b	.L337
.L344:
	.loc 1 2152 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L355
	.loc 1 2154 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #196]
	bic	r2, r2, #64
	str	r2, [r3, #196]
	.loc 1 2162 0
	b	.L337
.L355:
	.loc 1 2159 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #196]
	orr	r2, r2, #64
	str	r2, [r3, #196]
	.loc 1 2162 0
	b	.L337
.L345:
	.loc 1 2165 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L357
	.loc 1 2167 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #196]
	bic	r2, r2, #8
	str	r2, [r3, #196]
	.loc 1 2175 0
	b	.L337
.L357:
	.loc 1 2172 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #196]
	orr	r2, r2, #8
	str	r2, [r3, #196]
	.loc 1 2175 0
	b	.L337
.L346:
	.loc 1 2178 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L359
	.loc 1 2180 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #196]
	bic	r2, r2, #4
	str	r2, [r3, #196]
	.loc 1 2188 0
	b	.L337
.L359:
	.loc 1 2185 0
	ldr	r3, .L362
	ldr	r2, .L362
	ldr	r2, [r2, #196]
	orr	r2, r2, #4
	str	r2, [r3, #196]
	.loc 1 2188 0
	b	.L337
.L361:
	.loc 1 2191 0
	mov	r0, r0	@ nop
.L337:
	.loc 1 2193 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L363:
	.align	2
.L362:
	.word	1073758208
.LFE31:
	.size	clkpwr_set_mux_state, .-clkpwr_set_mux_state
	.section	.text.clkpwr_setup_mcard_ctrlr,"ax",%progbits
	.align	2
	.global	clkpwr_setup_mcard_ctrlr
	.type	clkpwr_setup_mcard_ctrlr, %function
clkpwr_setup_mcard_ctrlr:
.LFB32:
	.loc 1 2223 0
	@ args = 4, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI96:
	add	fp, sp, #0
.LCFI97:
	sub	sp, sp, #20
.LCFI98:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 2226 0
	ldr	r3, .L369
	ldr	r3, [r3, #128]
	and	r3, r3, #32
	str	r3, [fp, #-4]
	.loc 1 2227 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L365
	.loc 1 2229 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #512
	str	r3, [fp, #-4]
.L365:
	.loc 1 2231 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L366
	.loc 1 2233 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #256
	str	r3, [fp, #-4]
.L366:
	.loc 1 2235 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L367
	.loc 1 2237 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #128
	str	r3, [fp, #-4]
.L367:
	.loc 1 2239 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L368
	.loc 1 2241 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #64
	str	r3, [fp, #-4]
.L368:
	.loc 1 2243 0
	ldr	r3, [fp, #4]
	and	r3, r3, #15
	ldr	r2, [fp, #-4]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 2244 0
	ldr	r3, .L369
	ldr	r2, [fp, #-4]
	str	r2, [r3, #128]
	.loc 1 2245 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L370:
	.align	2
.L369:
	.word	1073758208
.LFE32:
	.size	clkpwr_setup_mcard_ctrlr, .-clkpwr_setup_mcard_ctrlr
	.section	.text.clkpwr_set_i2c_driver,"ax",%progbits
	.align	2
	.global	clkpwr_set_i2c_driver
	.type	clkpwr_set_i2c_driver, %function
clkpwr_set_i2c_driver:
.LFB33:
	.loc 1 2269 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI99:
	add	fp, sp, #0
.LCFI100:
	sub	sp, sp, #12
.LCFI101:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 2272 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L372
	.loc 1 2274 0
	ldr	r3, .L376
	ldr	r3, [r3, #172]
	bic	r3, r3, #4
	str	r3, [fp, #-4]
	.loc 1 2275 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L373
	.loc 1 2277 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #4
	str	r3, [fp, #-4]
.L373:
	.loc 1 2279 0
	ldr	r3, .L376
	ldr	r2, [fp, #-4]
	str	r2, [r3, #172]
	b	.L371
.L372:
	.loc 1 2281 0
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	bne	.L371
	.loc 1 2283 0
	ldr	r3, .L376
	ldr	r3, [r3, #172]
	bic	r3, r3, #8
	str	r3, [fp, #-4]
	.loc 1 2284 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L375
	.loc 1 2286 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #8
	str	r3, [fp, #-4]
.L375:
	.loc 1 2288 0
	ldr	r3, .L376
	ldr	r2, [fp, #-4]
	str	r2, [r3, #172]
.L371:
	.loc 1 2290 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L377:
	.align	2
.L376:
	.word	1073758208
.LFE33:
	.size	clkpwr_set_i2c_driver, .-clkpwr_set_i2c_driver
	.section	.text.clkpwr_setup_pwm,"ax",%progbits
	.align	2
	.global	clkpwr_setup_pwm
	.type	clkpwr_setup_pwm, %function
clkpwr_setup_pwm:
.LFB34:
	.loc 1 2317 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI102:
	add	fp, sp, #0
.LCFI103:
	sub	sp, sp, #16
.LCFI104:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 2320 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L379
	.loc 1 2322 0
	ldr	r3, .L383
	ldr	r3, [r3, #184]
	bic	r3, r3, #242
	str	r3, [fp, #-4]
	.loc 1 2324 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L380
	.loc 1 2326 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #2
	str	r3, [fp, #-4]
.L380:
	.loc 1 2328 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #4
	and	r2, r3, #255
	ldr	r3, [fp, #-4]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 2329 0
	ldr	r3, .L383
	ldr	r2, [fp, #-4]
	str	r2, [r3, #184]
	b	.L378
.L379:
	.loc 1 2331 0
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	bne	.L378
	.loc 1 2333 0
	ldr	r3, .L383
	ldr	r3, [r3, #184]
	bic	r3, r3, #3840
	bic	r3, r3, #8
	str	r3, [fp, #-4]
	.loc 1 2335 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L382
	.loc 1 2337 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #8
	str	r3, [fp, #-4]
.L382:
	.loc 1 2339 0
	ldr	r3, [fp, #-16]
	and	r3, r3, #15
	mov	r3, r3, asl #8
	mov	r2, r3
	ldr	r3, [fp, #-4]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 2340 0
	ldr	r3, .L383
	ldr	r2, [fp, #-4]
	str	r2, [r3, #184]
.L378:
	.loc 1 2342 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L384:
	.align	2
.L383:
	.word	1073758208
.LFE34:
	.size	clkpwr_setup_pwm, .-clkpwr_setup_pwm
	.section	.text.clkpwr_setup_nand_ctrlr,"ax",%progbits
	.align	2
	.global	clkpwr_setup_nand_ctrlr
	.type	clkpwr_setup_nand_ctrlr, %function
clkpwr_setup_nand_ctrlr:
.LFB35:
	.loc 1 2368 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI105:
	add	fp, sp, #0
.LCFI106:
	sub	sp, sp, #16
.LCFI107:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 2371 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L386
	.loc 1 2374 0
	mov	r3, #4
	str	r3, [fp, #-4]
	b	.L387
.L386:
	.loc 1 2379 0
	mov	r3, #32
	str	r3, [fp, #-4]
.L387:
	.loc 1 2383 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L388
	.loc 1 2385 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #16
	str	r3, [fp, #-4]
.L388:
	.loc 1 2387 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L389
	.loc 1 2389 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #8
	str	r3, [fp, #-4]
.L389:
	.loc 1 2392 0
	ldr	r3, .L390
	ldr	r2, [fp, #-4]
	str	r2, [r3, #200]
	.loc 1 2393 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L391:
	.align	2
.L390:
	.word	1073758208
.LFE35:
	.size	clkpwr_setup_nand_ctrlr, .-clkpwr_setup_nand_ctrlr
	.section	.text.clkpwr_setup_adc_ts,"ax",%progbits
	.align	2
	.global	clkpwr_setup_adc_ts
	.type	clkpwr_setup_adc_ts, %function
clkpwr_setup_adc_ts:
.LFB36:
	.loc 1 2417 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI108:
	add	fp, sp, #0
.LCFI109:
	sub	sp, sp, #12
.LCFI110:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 2420 0
	ldr	r3, [fp, #-12]
	and	r3, r3, #255
	str	r3, [fp, #-4]
	.loc 1 2421 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L393
	.loc 1 2423 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #256
	str	r3, [fp, #-4]
.L393:
	.loc 1 2426 0
	ldr	r3, .L394
	ldr	r2, [fp, #-4]
	str	r2, [r3, #96]
	.loc 1 2427 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L395:
	.align	2
.L394:
	.word	1073758208
.LFE36:
	.size	clkpwr_setup_adc_ts, .-clkpwr_setup_adc_ts
	.section	.text.clkpwr_setup_ssp,"ax",%progbits
	.align	2
	.global	clkpwr_setup_ssp
	.type	clkpwr_setup_ssp, %function
clkpwr_setup_ssp:
.LFB37:
	.loc 1 2455 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI111:
	add	fp, sp, #0
.LCFI112:
	sub	sp, sp, #20
.LCFI113:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 2458 0
	ldr	r3, .L401
	ldr	r3, [r3, #120]
	bic	r3, r3, #60
	str	r3, [fp, #-4]
	.loc 1 2461 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L397
	.loc 1 2463 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #32
	str	r3, [fp, #-4]
.L397:
	.loc 1 2465 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L398
	.loc 1 2467 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #16
	str	r3, [fp, #-4]
.L398:
	.loc 1 2469 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L399
	.loc 1 2471 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #8
	str	r3, [fp, #-4]
.L399:
	.loc 1 2473 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L400
	.loc 1 2475 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #4
	str	r3, [fp, #-4]
.L400:
	.loc 1 2478 0
	ldr	r3, .L401
	ldr	r2, [fp, #-4]
	str	r2, [r3, #120]
	.loc 1 2479 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L402:
	.align	2
.L401:
	.word	1073758208
.LFE37:
	.size	clkpwr_setup_ssp, .-clkpwr_setup_ssp
	.section	.text.clkpwr_setup_i2s,"ax",%progbits
	.align	2
	.global	clkpwr_setup_i2s
	.type	clkpwr_setup_i2s, %function
clkpwr_setup_i2s:
.LFB38:
	.loc 1 2509 0
	@ args = 4, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI114:
	add	fp, sp, #0
.LCFI115:
	sub	sp, sp, #20
.LCFI116:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 2512 0
	ldr	r3, .L409
	ldr	r3, [r3, #124]
	bic	r3, r3, #124
	str	r3, [fp, #-4]
	.loc 1 2515 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L404
	.loc 1 2517 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #64
	str	r3, [fp, #-4]
.L404:
	.loc 1 2519 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L405
	.loc 1 2521 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #32
	str	r3, [fp, #-4]
.L405:
	.loc 1 2523 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L406
	.loc 1 2525 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #16
	str	r3, [fp, #-4]
.L406:
	.loc 1 2527 0
	ldr	r3, [fp, #4]
	cmp	r3, #0
	beq	.L407
	.loc 1 2529 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #8
	str	r3, [fp, #-4]
.L407:
	.loc 1 2531 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L408
	.loc 1 2533 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #4
	str	r3, [fp, #-4]
.L408:
	.loc 1 2536 0
	ldr	r3, .L409
	ldr	r2, [fp, #-4]
	str	r2, [r3, #124]
	.loc 1 2537 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L410:
	.align	2
.L409:
	.word	1073758208
.LFE38:
	.size	clkpwr_setup_i2s, .-clkpwr_setup_i2s
	.section	.text.clkpwr_setup_lcd,"ax",%progbits
	.align	2
	.global	clkpwr_setup_lcd
	.type	clkpwr_setup_lcd, %function
clkpwr_setup_lcd:
.LFB39:
	.loc 1 2561 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI117:
	add	fp, sp, #0
.LCFI118:
	sub	sp, sp, #12
.LCFI119:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 2564 0
	ldr	r3, .L424
	ldr	r3, [r3, #84]
	bic	r3, r3, #476
	bic	r3, r3, #3
	str	r3, [fp, #-4]
	.loc 1 2566 0
	ldr	r3, [fp, #-8]
	cmp	r3, #192
	beq	.L416
	cmp	r3, #192
	bhi	.L421
	cmp	r3, #64
	beq	.L414
	cmp	r3, #128
	beq	.L415
	cmp	r3, #0
	beq	.L413
	b	.L412
.L421:
	cmp	r3, #320
	beq	.L418
	cmp	r3, #320
	bhi	.L422
	cmp	r3, #256
	beq	.L417
	b	.L412
.L422:
	cmp	r3, #384
	beq	.L419
	cmp	r3, #448
	beq	.L420
	b	.L412
.L413:
	.loc 1 2569 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 2570 0
	b	.L423
.L414:
	.loc 1 2573 0
	mov	r3, #64
	str	r3, [fp, #-4]
	.loc 1 2574 0
	b	.L423
.L415:
	.loc 1 2577 0
	mov	r3, #128
	str	r3, [fp, #-4]
	.loc 1 2578 0
	b	.L423
.L416:
	.loc 1 2581 0
	mov	r3, #192
	str	r3, [fp, #-4]
	.loc 1 2582 0
	b	.L423
.L417:
	.loc 1 2585 0
	mov	r3, #256
	str	r3, [fp, #-4]
	.loc 1 2586 0
	b	.L423
.L418:
	.loc 1 2589 0
	mov	r3, #320
	str	r3, [fp, #-4]
	.loc 1 2590 0
	b	.L423
.L419:
	.loc 1 2593 0
	mov	r3, #384
	str	r3, [fp, #-4]
	.loc 1 2594 0
	b	.L423
.L420:
	.loc 1 2597 0
	mov	r3, #448
	str	r3, [fp, #-4]
	.loc 1 2598 0
	b	.L423
.L412:
	.loc 1 2601 0
	mov	r0, r0	@ nop
.L423:
	.loc 1 2605 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	and	r2, r3, #31
	ldr	r3, [fp, #-4]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 2606 0
	ldr	r3, .L424
	ldr	r2, [fp, #-4]
	str	r2, [r3, #84]
	.loc 1 2607 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L425:
	.align	2
.L424:
	.word	1073758208
.LFE39:
	.size	clkpwr_setup_lcd, .-clkpwr_setup_lcd
	.section	.text.clkpwr_select_enet_iface,"ax",%progbits
	.align	2
	.global	clkpwr_select_enet_iface
	.type	clkpwr_select_enet_iface, %function
clkpwr_select_enet_iface:
.LFB40:
	.loc 1 2631 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI120:
	add	fp, sp, #0
.LCFI121:
	sub	sp, sp, #12
.LCFI122:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 2634 0
	ldr	r3, .L430
	ldr	r3, [r3, #144]
	bic	r3, r3, #24
	str	r3, [fp, #-4]
	.loc 1 2635 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L427
	.loc 1 2637 0
	ldr	r3, [fp, #-4]
	str	r3, [fp, #-4]
	b	.L428
.L427:
	.loc 1 2639 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L429
	.loc 1 2641 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #24
	str	r3, [fp, #-4]
	b	.L428
.L429:
	.loc 1 2645 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #8
	str	r3, [fp, #-4]
.L428:
	.loc 1 2648 0
	ldr	r3, .L430
	ldr	r2, [fp, #-4]
	str	r2, [r3, #144]
	.loc 1 2649 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L431:
	.align	2
.L430:
	.word	1073758208
.LFE40:
	.size	clkpwr_select_enet_iface, .-clkpwr_select_enet_iface
	.section	.text.clkpwr_halt_cpu,"ax",%progbits
	.align	2
	.global	clkpwr_halt_cpu
	.type	clkpwr_halt_cpu, %function
clkpwr_halt_cpu:
.LFB41:
	.loc 1 2675 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI123:
	add	fp, sp, #0
.LCFI124:
	.loc 1 2692 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE41:
	.size	clkpwr_halt_cpu, .-clkpwr_halt_cpu
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI45-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI48-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI51-.LFB17
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI52-.LCFI51
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI54-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI55-.LCFI54
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI57-.LFB19
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI58-.LCFI57
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI60-.LFB20
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI61-.LCFI60
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI63-.LFB21
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI64-.LCFI63
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI66-.LFB22
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI67-.LCFI66
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI69-.LFB23
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI70-.LCFI69
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI72-.LFB24
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI73-.LCFI72
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI75-.LFB25
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI76-.LCFI75
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI78-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI79-.LCFI78
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI81-.LFB27
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI82-.LCFI81
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI84-.LFB28
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI85-.LCFI84
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI87-.LFB29
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI88-.LCFI87
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI90-.LFB30
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI91-.LCFI90
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI93-.LFB31
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI94-.LCFI93
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI96-.LFB32
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI97-.LCFI96
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI99-.LFB33
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI100-.LCFI99
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI102-.LFB34
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI103-.LCFI102
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI105-.LFB35
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI106-.LCFI105
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI108-.LFB36
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI109-.LCFI108
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI111-.LFB37
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI112-.LCFI111
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI114-.LFB38
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI115-.LCFI114
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI117-.LFB39
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI118-.LCFI117
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI120-.LFB40
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI121-.LCFI120
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI123-.LFB41
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI124-.LCFI123
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE82:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1760
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF369
	.byte	0x1
	.4byte	.LASF370
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x61
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x67
	.4byte	0x73
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x70
	.4byte	0x85
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x5
	.2byte	0x140
	.byte	0x3
	.byte	0x28
	.4byte	0x39c
	.uleb128 0x6
	.4byte	.LASF13
	.byte	0x3
	.byte	0x2a
	.4byte	0x3ac
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x3
	.byte	0x2b
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x2c
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x3
	.byte	0x2d
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x3
	.byte	0x2e
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x3
	.byte	0x2f
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x3
	.byte	0x30
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x6
	.4byte	.LASF20
	.byte	0x3
	.byte	0x31
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x6
	.4byte	.LASF21
	.byte	0x3
	.byte	0x32
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x6
	.4byte	.LASF22
	.byte	0x3
	.byte	0x33
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x6
	.4byte	.LASF23
	.byte	0x3
	.byte	0x34
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x6
	.4byte	.LASF24
	.byte	0x3
	.byte	0x35
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x3
	.byte	0x36
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x3
	.byte	0x37
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x3
	.byte	0x38
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x6
	.4byte	.LASF28
	.byte	0x3
	.byte	0x39
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x6
	.4byte	.LASF29
	.byte	0x3
	.byte	0x3a
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x6
	.4byte	.LASF30
	.byte	0x3
	.byte	0x3b
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x6
	.4byte	.LASF31
	.byte	0x3
	.byte	0x3c
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x6
	.4byte	.LASF32
	.byte	0x3
	.byte	0x3d
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x6
	.4byte	.LASF33
	.byte	0x3
	.byte	0x3e
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x6
	.4byte	.LASF34
	.byte	0x3
	.byte	0x3f
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x6
	.4byte	.LASF35
	.byte	0x3
	.byte	0x40
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x6
	.4byte	.LASF36
	.byte	0x3
	.byte	0x41
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x6
	.4byte	.LASF37
	.byte	0x3
	.byte	0x42
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x6
	.4byte	.LASF38
	.byte	0x3
	.byte	0x43
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x6
	.4byte	.LASF39
	.byte	0x3
	.byte	0x44
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x6
	.4byte	.LASF40
	.byte	0x3
	.byte	0x45
	.4byte	0x3b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x6
	.4byte	.LASF41
	.byte	0x3
	.byte	0x46
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x6
	.4byte	.LASF42
	.byte	0x3
	.byte	0x47
	.4byte	0x3c6
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x6
	.4byte	.LASF43
	.byte	0x3
	.byte	0x48
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x6
	.4byte	.LASF44
	.byte	0x3
	.byte	0x49
	.4byte	0x3db
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x6
	.4byte	.LASF45
	.byte	0x3
	.byte	0x4a
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x6
	.4byte	.LASF46
	.byte	0x3
	.byte	0x4b
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x6
	.4byte	.LASF47
	.byte	0x3
	.byte	0x4c
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x6
	.4byte	.LASF48
	.byte	0x3
	.byte	0x4d
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x6
	.4byte	.LASF49
	.byte	0x3
	.byte	0x4e
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0x6
	.4byte	.LASF50
	.byte	0x3
	.byte	0x4f
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0x6
	.4byte	.LASF51
	.byte	0x3
	.byte	0x50
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xbc
	.uleb128 0x6
	.4byte	.LASF52
	.byte	0x3
	.byte	0x51
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x6
	.4byte	.LASF53
	.byte	0x3
	.byte	0x52
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xc4
	.uleb128 0x6
	.4byte	.LASF54
	.byte	0x3
	.byte	0x53
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0x6
	.4byte	.LASF55
	.byte	0x3
	.byte	0x54
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x6
	.4byte	.LASF56
	.byte	0x3
	.byte	0x55
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x6
	.4byte	.LASF57
	.byte	0x3
	.byte	0x56
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0x6
	.4byte	.LASF58
	.byte	0x3
	.byte	0x57
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0x6
	.4byte	.LASF59
	.byte	0x3
	.byte	0x58
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xdc
	.uleb128 0x6
	.4byte	.LASF60
	.byte	0x3
	.byte	0x59
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xe0
	.uleb128 0x6
	.4byte	.LASF61
	.byte	0x3
	.byte	0x5a
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xe4
	.uleb128 0x6
	.4byte	.LASF62
	.byte	0x3
	.byte	0x5b
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xe8
	.uleb128 0x6
	.4byte	.LASF63
	.byte	0x3
	.byte	0x5c
	.4byte	0x3b1
	.byte	0x3
	.byte	0x23
	.uleb128 0xec
	.uleb128 0x6
	.4byte	.LASF64
	.byte	0x3
	.byte	0x5d
	.4byte	0x3f0
	.byte	0x3
	.byte	0x23
	.uleb128 0xf0
	.uleb128 0x6
	.4byte	.LASF65
	.byte	0x3
	.byte	0x5e
	.4byte	0x3f5
	.byte	0x3
	.byte	0x23
	.uleb128 0x130
	.byte	0
	.uleb128 0x7
	.4byte	0x56
	.4byte	0x3ac
	.uleb128 0x8
	.4byte	0x25
	.byte	0x4
	.byte	0
	.uleb128 0x9
	.4byte	0x39c
	.uleb128 0x9
	.4byte	0x56
	.uleb128 0x7
	.4byte	0x56
	.4byte	0x3c6
	.uleb128 0x8
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x9
	.4byte	0x3b6
	.uleb128 0x7
	.4byte	0x56
	.4byte	0x3db
	.uleb128 0x8
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.4byte	0x3cb
	.uleb128 0x7
	.4byte	0x56
	.4byte	0x3f0
	.uleb128 0x8
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x9
	.4byte	0x3e0
	.uleb128 0x9
	.4byte	0x3cb
	.uleb128 0x3
	.4byte	.LASF66
	.byte	0x3
	.byte	0x5f
	.4byte	0x93
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0x24
	.4byte	0x4e6
	.uleb128 0xb
	.4byte	.LASF67
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF68
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF69
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF70
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF71
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF72
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF73
	.sleb128 5
	.uleb128 0xb
	.4byte	.LASF74
	.sleb128 6
	.uleb128 0xb
	.4byte	.LASF75
	.sleb128 7
	.uleb128 0xb
	.4byte	.LASF76
	.sleb128 8
	.uleb128 0xb
	.4byte	.LASF77
	.sleb128 9
	.uleb128 0xb
	.4byte	.LASF78
	.sleb128 10
	.uleb128 0xb
	.4byte	.LASF79
	.sleb128 11
	.uleb128 0xb
	.4byte	.LASF80
	.sleb128 12
	.uleb128 0xb
	.4byte	.LASF81
	.sleb128 13
	.uleb128 0xb
	.4byte	.LASF82
	.sleb128 14
	.uleb128 0xb
	.4byte	.LASF83
	.sleb128 15
	.uleb128 0xb
	.4byte	.LASF84
	.sleb128 16
	.uleb128 0xb
	.4byte	.LASF85
	.sleb128 17
	.uleb128 0xb
	.4byte	.LASF86
	.sleb128 18
	.uleb128 0xb
	.4byte	.LASF87
	.sleb128 19
	.uleb128 0xb
	.4byte	.LASF88
	.sleb128 20
	.uleb128 0xb
	.4byte	.LASF89
	.sleb128 21
	.uleb128 0xb
	.4byte	.LASF90
	.sleb128 22
	.uleb128 0xb
	.4byte	.LASF91
	.sleb128 23
	.uleb128 0xb
	.4byte	.LASF92
	.sleb128 24
	.uleb128 0xb
	.4byte	.LASF93
	.sleb128 25
	.uleb128 0xb
	.4byte	.LASF94
	.sleb128 26
	.uleb128 0xb
	.4byte	.LASF95
	.sleb128 27
	.uleb128 0xb
	.4byte	.LASF96
	.sleb128 28
	.uleb128 0xb
	.4byte	.LASF97
	.sleb128 29
	.uleb128 0xb
	.4byte	.LASF98
	.sleb128 30
	.uleb128 0xb
	.4byte	.LASF99
	.sleb128 31
	.uleb128 0xb
	.4byte	.LASF100
	.sleb128 32
	.uleb128 0xb
	.4byte	.LASF101
	.sleb128 33
	.uleb128 0xb
	.4byte	.LASF102
	.sleb128 34
	.byte	0
	.uleb128 0x3
	.4byte	.LASF103
	.byte	0x4
	.byte	0x49
	.4byte	0x405
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0x4d
	.4byte	0x53c
	.uleb128 0xb
	.4byte	.LASF104
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF105
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF106
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF107
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF108
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF109
	.sleb128 5
	.uleb128 0xb
	.4byte	.LASF110
	.sleb128 6
	.uleb128 0xb
	.4byte	.LASF111
	.sleb128 7
	.uleb128 0xb
	.4byte	.LASF112
	.sleb128 8
	.uleb128 0xb
	.4byte	.LASF113
	.sleb128 9
	.uleb128 0xb
	.4byte	.LASF114
	.sleb128 10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF115
	.byte	0x4
	.byte	0x63
	.4byte	0x4f1
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0x67
	.4byte	0x580
	.uleb128 0xb
	.4byte	.LASF116
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF117
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF118
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF119
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF120
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF121
	.sleb128 5
	.uleb128 0xb
	.4byte	.LASF122
	.sleb128 6
	.uleb128 0xb
	.4byte	.LASF123
	.sleb128 7
	.byte	0
	.uleb128 0x3
	.4byte	.LASF124
	.byte	0x4
	.byte	0x70
	.4byte	0x547
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0x74
	.4byte	0x5a0
	.uleb128 0xb
	.4byte	.LASF125
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF126
	.sleb128 1
	.byte	0
	.uleb128 0x3
	.4byte	.LASF127
	.byte	0x4
	.byte	0x77
	.4byte	0x58b
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0x7b
	.4byte	0x5c6
	.uleb128 0xb
	.4byte	.LASF128
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF129
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF130
	.sleb128 2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF131
	.byte	0x4
	.byte	0x7f
	.4byte	0x5ab
	.uleb128 0xc
	.byte	0x1c
	.byte	0x4
	.byte	0x82
	.4byte	0x63c
	.uleb128 0x6
	.4byte	.LASF132
	.byte	0x4
	.byte	0x85
	.4byte	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF133
	.byte	0x4
	.byte	0x88
	.4byte	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF134
	.byte	0x4
	.byte	0x8b
	.4byte	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF135
	.byte	0x4
	.byte	0x8d
	.4byte	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF136
	.byte	0x4
	.byte	0x8f
	.4byte	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF137
	.byte	0x4
	.byte	0x91
	.4byte	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF138
	.byte	0x4
	.byte	0x93
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x3
	.4byte	.LASF139
	.byte	0x4
	.byte	0x94
	.4byte	0x5d1
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0x98
	.4byte	0x662
	.uleb128 0xb
	.4byte	.LASF140
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF141
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF142
	.sleb128 2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF143
	.byte	0x4
	.byte	0x9c
	.4byte	0x647
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0xa3
	.4byte	0x86b
	.uleb128 0xb
	.4byte	.LASF144
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF145
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF146
	.sleb128 2
	.uleb128 0xb
	.4byte	.LASF147
	.sleb128 3
	.uleb128 0xb
	.4byte	.LASF148
	.sleb128 4
	.uleb128 0xb
	.4byte	.LASF149
	.sleb128 5
	.uleb128 0xb
	.4byte	.LASF150
	.sleb128 6
	.uleb128 0xb
	.4byte	.LASF151
	.sleb128 7
	.uleb128 0xb
	.4byte	.LASF152
	.sleb128 16
	.uleb128 0xb
	.4byte	.LASF153
	.sleb128 19
	.uleb128 0xb
	.4byte	.LASF154
	.sleb128 20
	.uleb128 0xb
	.4byte	.LASF155
	.sleb128 21
	.uleb128 0xb
	.4byte	.LASF156
	.sleb128 22
	.uleb128 0xb
	.4byte	.LASF157
	.sleb128 23
	.uleb128 0xb
	.4byte	.LASF158
	.sleb128 24
	.uleb128 0xb
	.4byte	.LASF159
	.sleb128 25
	.uleb128 0xb
	.4byte	.LASF160
	.sleb128 26
	.uleb128 0xb
	.4byte	.LASF161
	.sleb128 29
	.uleb128 0xb
	.4byte	.LASF162
	.sleb128 30
	.uleb128 0xb
	.4byte	.LASF163
	.sleb128 31
	.uleb128 0xb
	.4byte	.LASF164
	.sleb128 32
	.uleb128 0xb
	.4byte	.LASF165
	.sleb128 33
	.uleb128 0xb
	.4byte	.LASF166
	.sleb128 34
	.uleb128 0xb
	.4byte	.LASF167
	.sleb128 35
	.uleb128 0xb
	.4byte	.LASF168
	.sleb128 36
	.uleb128 0xb
	.4byte	.LASF169
	.sleb128 37
	.uleb128 0xb
	.4byte	.LASF170
	.sleb128 38
	.uleb128 0xb
	.4byte	.LASF171
	.sleb128 39
	.uleb128 0xb
	.4byte	.LASF172
	.sleb128 40
	.uleb128 0xb
	.4byte	.LASF173
	.sleb128 41
	.uleb128 0xb
	.4byte	.LASF174
	.sleb128 42
	.uleb128 0xb
	.4byte	.LASF175
	.sleb128 43
	.uleb128 0xb
	.4byte	.LASF176
	.sleb128 44
	.uleb128 0xb
	.4byte	.LASF177
	.sleb128 45
	.uleb128 0xb
	.4byte	.LASF178
	.sleb128 46
	.uleb128 0xb
	.4byte	.LASF179
	.sleb128 47
	.uleb128 0xb
	.4byte	.LASF180
	.sleb128 53
	.uleb128 0xb
	.4byte	.LASF181
	.sleb128 54
	.uleb128 0xb
	.4byte	.LASF182
	.sleb128 55
	.uleb128 0xb
	.4byte	.LASF183
	.sleb128 56
	.uleb128 0xb
	.4byte	.LASF184
	.sleb128 57
	.uleb128 0xb
	.4byte	.LASF185
	.sleb128 58
	.uleb128 0xb
	.4byte	.LASF186
	.sleb128 60
	.uleb128 0xb
	.4byte	.LASF187
	.sleb128 62
	.uleb128 0xb
	.4byte	.LASF188
	.sleb128 63
	.uleb128 0xb
	.4byte	.LASF189
	.sleb128 64
	.uleb128 0xb
	.4byte	.LASF190
	.sleb128 65
	.uleb128 0xb
	.4byte	.LASF191
	.sleb128 66
	.uleb128 0xb
	.4byte	.LASF192
	.sleb128 67
	.uleb128 0xb
	.4byte	.LASF193
	.sleb128 68
	.uleb128 0xb
	.4byte	.LASF194
	.sleb128 69
	.uleb128 0xb
	.4byte	.LASF195
	.sleb128 70
	.uleb128 0xb
	.4byte	.LASF196
	.sleb128 71
	.uleb128 0xb
	.4byte	.LASF197
	.sleb128 72
	.uleb128 0xb
	.4byte	.LASF198
	.sleb128 73
	.uleb128 0xb
	.4byte	.LASF199
	.sleb128 74
	.uleb128 0xb
	.4byte	.LASF200
	.sleb128 75
	.uleb128 0xb
	.4byte	.LASF201
	.sleb128 76
	.uleb128 0xb
	.4byte	.LASF202
	.sleb128 77
	.uleb128 0xb
	.4byte	.LASF203
	.sleb128 78
	.uleb128 0xb
	.4byte	.LASF204
	.sleb128 79
	.uleb128 0xb
	.4byte	.LASF205
	.sleb128 80
	.uleb128 0xb
	.4byte	.LASF206
	.sleb128 81
	.uleb128 0xb
	.4byte	.LASF207
	.sleb128 82
	.uleb128 0xb
	.4byte	.LASF208
	.sleb128 83
	.uleb128 0xb
	.4byte	.LASF209
	.sleb128 84
	.uleb128 0xb
	.4byte	.LASF210
	.sleb128 85
	.uleb128 0xb
	.4byte	.LASF211
	.sleb128 86
	.uleb128 0xb
	.4byte	.LASF212
	.sleb128 87
	.uleb128 0xb
	.4byte	.LASF213
	.sleb128 88
	.uleb128 0xb
	.4byte	.LASF214
	.sleb128 89
	.uleb128 0xb
	.4byte	.LASF215
	.sleb128 90
	.uleb128 0xb
	.4byte	.LASF216
	.sleb128 91
	.uleb128 0xb
	.4byte	.LASF217
	.sleb128 92
	.uleb128 0xb
	.4byte	.LASF218
	.sleb128 93
	.uleb128 0xb
	.4byte	.LASF219
	.sleb128 94
	.uleb128 0xb
	.4byte	.LASF220
	.sleb128 95
	.uleb128 0xb
	.4byte	.LASF221
	.sleb128 96
	.byte	0
	.uleb128 0x3
	.4byte	.LASF222
	.byte	0x4
	.byte	0xf7
	.4byte	0x66d
	.uleb128 0xa
	.byte	0x4
	.byte	0x4
	.byte	0xfb
	.4byte	0x891
	.uleb128 0xb
	.4byte	.LASF223
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF224
	.sleb128 1
	.uleb128 0xb
	.4byte	.LASF225
	.sleb128 2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF226
	.byte	0x4
	.byte	0xff
	.4byte	0x876
	.uleb128 0xd
	.byte	0x4
	.byte	0x4
	.2byte	0x19f
	.4byte	0x8dd
	.uleb128 0xb
	.4byte	.LASF227
	.sleb128 0
	.uleb128 0xb
	.4byte	.LASF228
	.sleb128 64
	.uleb128 0xb
	.4byte	.LASF229
	.sleb128 128
	.uleb128 0xb
	.4byte	.LASF230
	.sleb128 192
	.uleb128 0xb
	.4byte	.LASF231
	.sleb128 256
	.uleb128 0xb
	.4byte	.LASF232
	.sleb128 320
	.uleb128 0xb
	.4byte	.LASF233
	.sleb128 384
	.uleb128 0xb
	.4byte	.LASF234
	.sleb128 448
	.byte	0
	.uleb128 0xe
	.4byte	.LASF235
	.byte	0x4
	.2byte	0x1a8
	.4byte	0x89c
	.uleb128 0xc
	.byte	0x10
	.byte	0x1
	.byte	0x25
	.4byte	0x92a
	.uleb128 0x6
	.4byte	.LASF236
	.byte	0x1
	.byte	0x27
	.4byte	0x92a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF237
	.byte	0x1
	.byte	0x28
	.4byte	0x92a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF238
	.byte	0x1
	.byte	0x29
	.4byte	0x92a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF239
	.byte	0x1
	.byte	0x2a
	.4byte	0x92a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0xf
	.byte	0x4
	.4byte	0x3b1
	.uleb128 0x3
	.4byte	.LASF240
	.byte	0x1
	.byte	0x2b
	.4byte	0x8e9
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF241
	.byte	0x1
	.byte	0x78
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x973
	.uleb128 0x11
	.ascii	"v1\000"
	.byte	0x1
	.byte	0x78
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x11
	.ascii	"v2\000"
	.byte	0x1
	.byte	0x78
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF242
	.byte	0x1
	.byte	0x96
	.byte	0x1
	.4byte	0x56
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0xa1b
	.uleb128 0x12
	.4byte	.LASF243
	.byte	0x1
	.byte	0x96
	.4byte	0x56
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.uleb128 0x12
	.4byte	.LASF244
	.byte	0x1
	.byte	0x97
	.4byte	0xa1b
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x13
	.4byte	.LASF245
	.byte	0x1
	.byte	0x99
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x14
	.ascii	"p\000"
	.byte	0x1
	.byte	0x99
	.4byte	0x7a
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x14
	.ascii	"m\000"
	.byte	0x1
	.byte	0x99
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x14
	.ascii	"n\000"
	.byte	0x1
	.byte	0x99
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x13
	.4byte	.LASF246
	.byte	0x1
	.byte	0x99
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF247
	.byte	0x1
	.byte	0x99
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x13
	.4byte	.LASF248
	.byte	0x1
	.byte	0x99
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x13
	.4byte	.LASF249
	.byte	0x1
	.byte	0x9a
	.4byte	0x68
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.byte	0
	.uleb128 0xf
	.byte	0x4
	.4byte	0x63c
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF250
	.byte	0x1
	.byte	0xf3
	.byte	0x1
	.4byte	0x56
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0xa69
	.uleb128 0x12
	.4byte	.LASF251
	.byte	0x1
	.byte	0xf3
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x11
	.ascii	"val\000"
	.byte	0x1
	.byte	0xf4
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x13
	.4byte	.LASF252
	.byte	0x1
	.byte	0xf6
	.4byte	0x63c
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF253
	.byte	0x1
	.2byte	0x123
	.byte	0x1
	.4byte	0x56
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0xaa6
	.uleb128 0x16
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x123
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x124
	.4byte	0xaa6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xf
	.byte	0x4
	.4byte	0x56
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF273
	.byte	0x1
	.2byte	0x13e
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0xb03
	.uleb128 0x16
	.4byte	.LASF255
	.byte	0x1
	.2byte	0x13e
	.4byte	0x92a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x13f
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.ascii	"set\000"
	.byte	0x1
	.2byte	0x140
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x142
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x161
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0xb5e
	.uleb128 0x16
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x161
	.4byte	0x86b
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF255
	.byte	0x1
	.2byte	0x162
	.4byte	0xb5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x163
	.4byte	0xb64
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x165
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xf
	.byte	0x4
	.4byte	0x930
	.uleb128 0xf
	.byte	0x4
	.4byte	0x68
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF261
	.byte	0x1
	.2byte	0x199
	.byte	0x1
	.4byte	0x53c
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0xba7
	.uleb128 0x16
	.4byte	.LASF262
	.byte	0x1
	.2byte	0x199
	.4byte	0x4e6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1a
	.4byte	.LASF263
	.byte	0x1
	.2byte	0x19b
	.4byte	0x53c
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF264
	.byte	0x1
	.2byte	0x1e6
	.byte	0x1
	.4byte	0x56
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0xc86
	.uleb128 0x16
	.4byte	.LASF265
	.byte	0x1
	.2byte	0x1e6
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x16
	.4byte	.LASF266
	.byte	0x1
	.2byte	0x1e7
	.4byte	0x56
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x16
	.4byte	.LASF267
	.byte	0x1
	.2byte	0x1e8
	.4byte	0x68
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x16
	.4byte	.LASF244
	.byte	0x1
	.2byte	0x1e9
	.4byte	0xa1b
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.uleb128 0x1a
	.4byte	.LASF243
	.byte	0x1
	.2byte	0x1eb
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x1a
	.4byte	.LASF268
	.byte	0x1
	.2byte	0x1eb
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x19
	.ascii	"m\000"
	.byte	0x1
	.2byte	0x1eb
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.ascii	"n\000"
	.byte	0x1
	.2byte	0x1eb
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.ascii	"p\000"
	.byte	0x1
	.2byte	0x1eb
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1a
	.4byte	.LASF269
	.byte	0x1
	.2byte	0x1eb
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1a
	.4byte	.LASF270
	.byte	0x1
	.2byte	0x1ec
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1a
	.4byte	.LASF271
	.byte	0x1
	.2byte	0x1ec
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x1a
	.4byte	.LASF272
	.byte	0x1
	.2byte	0x1ed
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF274
	.byte	0x1
	.2byte	0x284
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0xcdd
	.uleb128 0x16
	.4byte	.LASF275
	.byte	0x1
	.2byte	0x284
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x285
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF277
	.byte	0x1
	.2byte	0x286
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x288
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF278
	.byte	0x1
	.2byte	0x2af
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0xd28
	.uleb128 0x16
	.4byte	.LASF279
	.byte	0x1
	.2byte	0x2af
	.4byte	0xa1b
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"tv\000"
	.byte	0x1
	.2byte	0x2b1
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x2b1
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF280
	.byte	0x1
	.2byte	0x2f7
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0xd73
	.uleb128 0x16
	.4byte	.LASF279
	.byte	0x1
	.2byte	0x2f7
	.4byte	0xa1b
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"tv\000"
	.byte	0x1
	.2byte	0x2f9
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x2f9
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF281
	.byte	0x1
	.2byte	0x33f
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0xd9d
	.uleb128 0x16
	.4byte	.LASF282
	.byte	0x1
	.2byte	0x33f
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF283
	.byte	0x1
	.2byte	0x358
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0xdd6
	.uleb128 0x18
	.ascii	"pll\000"
	.byte	0x1
	.2byte	0x358
	.4byte	0x5c6
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x16
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x359
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF285
	.byte	0x1
	.2byte	0x399
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0xe13
	.uleb128 0x18
	.ascii	"pll\000"
	.byte	0x1
	.2byte	0x399
	.4byte	0x5c6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1a
	.4byte	.LASF286
	.byte	0x1
	.2byte	0x39b
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF287
	.byte	0x1
	.2byte	0x3de
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0xe5b
	.uleb128 0x16
	.4byte	.LASF288
	.byte	0x1
	.2byte	0x3de
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF289
	.byte	0x1
	.2byte	0x3df
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x3e1
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF290
	.byte	0x1
	.2byte	0x405
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0xea3
	.uleb128 0x18
	.ascii	"osc\000"
	.byte	0x1
	.2byte	0x405
	.4byte	0x5a0
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF291
	.byte	0x1
	.2byte	0x406
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x408
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF292
	.byte	0x1
	.2byte	0x42e
	.byte	0x1
	.4byte	0x5a0
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0xed1
	.uleb128 0x1a
	.4byte	.LASF293
	.byte	0x1
	.2byte	0x430
	.4byte	0x5a0
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF294
	.byte	0x1
	.2byte	0x44e
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0xf28
	.uleb128 0x16
	.4byte	.LASF295
	.byte	0x1
	.2byte	0x44e
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF296
	.byte	0x1
	.2byte	0x44f
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF297
	.byte	0x1
	.2byte	0x450
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF298
	.byte	0x1
	.2byte	0x452
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF299
	.byte	0x1
	.2byte	0x47e
	.byte	0x1
	.4byte	0x56
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0xffb
	.uleb128 0x16
	.4byte	.LASF263
	.byte	0x1
	.2byte	0x47e
	.4byte	0x53c
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x1a
	.4byte	.LASF300
	.byte	0x1
	.2byte	0x480
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1a
	.4byte	.LASF301
	.byte	0x1
	.2byte	0x480
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1a
	.4byte	.LASF302
	.byte	0x1
	.2byte	0x480
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF303
	.byte	0x1
	.2byte	0x480
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x1a
	.4byte	.LASF304
	.byte	0x1
	.2byte	0x480
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x481
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1a
	.4byte	.LASF305
	.byte	0x1
	.2byte	0x481
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1a
	.4byte	.LASF306
	.byte	0x1
	.2byte	0x481
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1a
	.4byte	.LASF307
	.byte	0x1
	.2byte	0x481
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1a
	.4byte	.LASF308
	.byte	0x1
	.2byte	0x481
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1a
	.4byte	.LASF309
	.byte	0x1
	.2byte	0x481
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF310
	.byte	0x1
	.2byte	0x526
	.byte	0x1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0x1034
	.uleb128 0x16
	.4byte	.LASF311
	.byte	0x1
	.2byte	0x526
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x528
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF312
	.byte	0x1
	.2byte	0x548
	.byte	0x1
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.4byte	0x106d
	.uleb128 0x16
	.4byte	.LASF249
	.byte	0x1
	.2byte	0x548
	.4byte	0x891
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x54a
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF313
	.byte	0x1
	.2byte	0x576
	.byte	0x1
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0x10a6
	.uleb128 0x18
	.ascii	"clk\000"
	.byte	0x1
	.2byte	0x576
	.4byte	0x4e6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x577
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF314
	.byte	0x1
	.2byte	0x645
	.byte	0x1
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0x10ee
	.uleb128 0x18
	.ascii	"clk\000"
	.byte	0x1
	.2byte	0x645
	.4byte	0x662
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x646
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x648
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF315
	.byte	0x1
	.2byte	0x681
	.byte	0x1
	.4byte	0x56
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST23
	.4byte	0x1158
	.uleb128 0x16
	.4byte	.LASF262
	.byte	0x1
	.2byte	0x681
	.4byte	0x4e6
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1a
	.4byte	.LASF263
	.byte	0x1
	.2byte	0x683
	.4byte	0x53c
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x684
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1a
	.4byte	.LASF316
	.byte	0x1
	.2byte	0x684
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1a
	.4byte	.LASF317
	.byte	0x1
	.2byte	0x684
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF318
	.byte	0x1
	.2byte	0x6ea
	.byte	0x1
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST24
	.4byte	0x11be
	.uleb128 0x16
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x6ea
	.4byte	0x86b
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x6eb
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x6ed
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1a
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x6ee
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF319
	.byte	0x1
	.2byte	0x6ef
	.4byte	0xb5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF320
	.byte	0x1
	.2byte	0x717
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST25
	.4byte	0x1219
	.uleb128 0x16
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x717
	.4byte	0x86b
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1a
	.4byte	.LASF321
	.byte	0x1
	.2byte	0x719
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1a
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x71a
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF319
	.byte	0x1
	.2byte	0x71b
	.4byte	0xb5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF322
	.byte	0x1
	.2byte	0x73b
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST26
	.4byte	0x1274
	.uleb128 0x16
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x73b
	.4byte	0x86b
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1a
	.4byte	.LASF321
	.byte	0x1
	.2byte	0x73d
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1a
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x73e
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF319
	.byte	0x1
	.2byte	0x73f
	.4byte	0xb5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF323
	.byte	0x1
	.2byte	0x75f
	.byte	0x1
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST27
	.4byte	0x12bc
	.uleb128 0x16
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x75f
	.4byte	0x86b
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x761
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1a
	.4byte	.LASF319
	.byte	0x1
	.2byte	0x762
	.4byte	0xb5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF324
	.byte	0x1
	.2byte	0x783
	.byte	0x1
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST28
	.4byte	0x1322
	.uleb128 0x16
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x783
	.4byte	0x86b
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF325
	.byte	0x1
	.2byte	0x784
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x786
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1a
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x787
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1a
	.4byte	.LASF319
	.byte	0x1
	.2byte	0x788
	.4byte	0xb5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF326
	.byte	0x1
	.2byte	0x7ad
	.byte	0x1
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST29
	.4byte	0x134c
	.uleb128 0x18
	.ascii	"uid\000"
	.byte	0x1
	.2byte	0x7ad
	.4byte	0xaa6
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF327
	.byte	0x1
	.2byte	0x7ca
	.byte	0x1
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST30
	.4byte	0x1385
	.uleb128 0x16
	.4byte	.LASF328
	.byte	0x1
	.2byte	0x7ca
	.4byte	0x580
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x16
	.4byte	.LASF329
	.byte	0x1
	.2byte	0x7cb
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF330
	.byte	0x1
	.2byte	0x831
	.byte	0x1
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST31
	.4byte	0x13cd
	.uleb128 0x16
	.4byte	.LASF328
	.byte	0x1
	.2byte	0x831
	.4byte	0x580
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF331
	.byte	0x1
	.2byte	0x832
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x834
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF332
	.byte	0x1
	.2byte	0x8aa
	.byte	0x1
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST32
	.4byte	0x1442
	.uleb128 0x16
	.4byte	.LASF333
	.byte	0x1
	.2byte	0x8aa
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF334
	.byte	0x1
	.2byte	0x8ab
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF335
	.byte	0x1
	.2byte	0x8ac
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF336
	.byte	0x1
	.2byte	0x8ad
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF337
	.byte	0x1
	.2byte	0x8ae
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x8b0
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF338
	.byte	0x1
	.2byte	0x8db
	.byte	0x1
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST33
	.4byte	0x148a
	.uleb128 0x16
	.4byte	.LASF339
	.byte	0x1
	.2byte	0x8db
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF325
	.byte	0x1
	.2byte	0x8dc
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x8de
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF340
	.byte	0x1
	.2byte	0x90a
	.byte	0x1
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST34
	.4byte	0x14e1
	.uleb128 0x16
	.4byte	.LASF341
	.byte	0x1
	.2byte	0x90a
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF342
	.byte	0x1
	.2byte	0x90b
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF337
	.byte	0x1
	.2byte	0x90c
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x90e
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF343
	.byte	0x1
	.2byte	0x93d
	.byte	0x1
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST35
	.4byte	0x1538
	.uleb128 0x16
	.4byte	.LASF344
	.byte	0x1
	.2byte	0x93d
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF345
	.byte	0x1
	.2byte	0x93e
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF346
	.byte	0x1
	.2byte	0x93f
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x941
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF347
	.byte	0x1
	.2byte	0x96f
	.byte	0x1
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST36
	.4byte	0x1580
	.uleb128 0x16
	.4byte	.LASF348
	.byte	0x1
	.2byte	0x96f
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF349
	.byte	0x1
	.2byte	0x970
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x972
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF350
	.byte	0x1
	.2byte	0x993
	.byte	0x1
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST37
	.4byte	0x15e6
	.uleb128 0x16
	.4byte	.LASF351
	.byte	0x1
	.2byte	0x993
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF352
	.byte	0x1
	.2byte	0x994
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF353
	.byte	0x1
	.2byte	0x995
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF354
	.byte	0x1
	.2byte	0x996
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x998
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF355
	.byte	0x1
	.2byte	0x9c8
	.byte	0x1
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST38
	.4byte	0x165b
	.uleb128 0x16
	.4byte	.LASF356
	.byte	0x1
	.2byte	0x9c8
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF357
	.byte	0x1
	.2byte	0x9c9
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF358
	.byte	0x1
	.2byte	0x9ca
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF359
	.byte	0x1
	.2byte	0x9cb
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF360
	.byte	0x1
	.2byte	0x9cc
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x9ce
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF361
	.byte	0x1
	.2byte	0x9ff
	.byte	0x1
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST39
	.4byte	0x16a3
	.uleb128 0x16
	.4byte	.LASF362
	.byte	0x1
	.2byte	0x9ff
	.4byte	0x8dd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF349
	.byte	0x1
	.2byte	0xa00
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0xa02
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF363
	.byte	0x1
	.2byte	0xa45
	.byte	0x1
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST40
	.4byte	0x16eb
	.uleb128 0x16
	.4byte	.LASF284
	.byte	0x1
	.2byte	0xa45
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF364
	.byte	0x1
	.2byte	0xa46
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0xa48
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF365
	.byte	0x1
	.2byte	0xa72
	.byte	0x1
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST41
	.4byte	0x1712
	.uleb128 0x1b
	.4byte	.LASF260
	.byte	0x1
	.2byte	0xa74
	.4byte	0x56
	.byte	0
	.uleb128 0x7
	.4byte	0x53c
	.4byte	0x1722
	.uleb128 0x8
	.4byte	0x25
	.byte	0x21
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF368
	.byte	0x1
	.byte	0x2e
	.4byte	0x1712
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF366
	.byte	0x1
	.byte	0x55
	.4byte	0x3cb
	.byte	0x5
	.byte	0x3
	.4byte	pll_postdivs
	.uleb128 0x13
	.4byte	.LASF367
	.byte	0x1
	.byte	0x5b
	.4byte	0x3cb
	.byte	0x5
	.byte	0x3
	.4byte	hclkdivs
	.uleb128 0x1d
	.4byte	.LASF368
	.byte	0x1
	.byte	0x2e
	.4byte	0x1712
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	clkpwr_base_clk
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI43
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI46
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI49
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI52
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI55
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI58
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI61
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI64
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI67
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB23
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI69
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI70
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB24
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI73
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB25
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI75
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI76
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB26
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI78
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI79
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB27
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI81
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI82
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB28
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI85
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB29
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI87
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI88
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB30
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI90
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI91
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB31
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI93
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI94
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB32
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI96
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI97
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB33
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI99
	.4byte	.LCFI100
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI100
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB34
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI102
	.4byte	.LCFI103
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI103
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB35
	.4byte	.LCFI105
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI105
	.4byte	.LCFI106
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI106
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB36
	.4byte	.LCFI108
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI108
	.4byte	.LCFI109
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI109
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB37
	.4byte	.LCFI111
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI111
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI112
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB38
	.4byte	.LCFI114
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI114
	.4byte	.LCFI115
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI115
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB39
	.4byte	.LCFI117
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI117
	.4byte	.LCFI118
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI118
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB40
	.4byte	.LCFI120
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI120
	.4byte	.LCFI121
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI121
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB41
	.4byte	.LCFI123
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI123
	.4byte	.LCFI124
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI124
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x164
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF101:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF181:
	.ascii	"CLKPWR_EVENT_EXT_U2_RX\000"
.LASF272:
	.ascii	"lfreqtol\000"
.LASF334:
	.ascii	"dat2_3_pullup_on\000"
.LASF140:
	.ascii	"CLKPWR_ACLK_USB_DEV\000"
.LASF226:
	.ascii	"CLKPWR_MODE_T\000"
.LASF345:
	.ascii	"use_dma_req_on_rnb\000"
.LASF20:
	.ascii	"clkpwr_int_ap\000"
.LASF9:
	.ascii	"INT_32\000"
.LASF252:
	.ascii	"pllcfg\000"
.LASF339:
	.ascii	"i2c_num\000"
.LASF143:
	.ascii	"CLKPWR_AUTOCLK_T\000"
.LASF156:
	.ascii	"CLKPWR_EVENT_INT_USB\000"
.LASF355:
	.ascii	"clkpwr_setup_i2s\000"
.LASF266:
	.ascii	"target_freq\000"
.LASF224:
	.ascii	"CLKPWR_MD_DIRECTRUN\000"
.LASF366:
	.ascii	"pll_postdivs\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF227:
	.ascii	"CLKPWR_LCDMUX_TFT12\000"
.LASF229:
	.ascii	"CLKPWR_LCDMUX_TFT15\000"
.LASF115:
	.ascii	"CLKPWR_BASE_CLOCK_T\000"
.LASF305:
	.ascii	"hclk1_clk\000"
.LASF43:
	.ascii	"clkpwr_macclk_ctrl\000"
.LASF295:
	.ascii	"dram_clk_div\000"
.LASF110:
	.ascii	"CLKPWR_USB_HCLK_SYS\000"
.LASF60:
	.ascii	"clkpwr_irda_clk_ctrl\000"
.LASF130:
	.ascii	"CLKPWR_USB_PLL\000"
.LASF108:
	.ascii	"CLKPWR_HCLK\000"
.LASF323:
	.ascii	"clkpwr_clear_event\000"
.LASF370:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_clkpwr_driver.c\000"
.LASF275:
	.ascii	"bypass_enable\000"
.LASF56:
	.ascii	"clkpwr_uart3_clk_ctrl\000"
.LASF253:
	.ascii	"clkpwr_pll_rate\000"
.LASF67:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF179:
	.ascii	"CLKPWR_EVENT_EXT_SDIO\000"
.LASF80:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF237:
	.ascii	"start_sr\000"
.LASF132:
	.ascii	"analog_on\000"
.LASF316:
	.ascii	"baseclkrate\000"
.LASF264:
	.ascii	"clkpwr_find_pll_cfg\000"
.LASF273:
	.ascii	"clkpwr_mask_and_set\000"
.LASF235:
	.ascii	"LCD_PIN_MUX_T\000"
.LASF249:
	.ascii	"mode\000"
.LASF61:
	.ascii	"clkpwr_uart_clk_ctrl\000"
.LASF230:
	.ascii	"CLKPWR_LCDMUX_TFT24\000"
.LASF225:
	.ascii	"CLKPWR_MODE_STOP\000"
.LASF18:
	.ascii	"clkpwr_int_rs\000"
.LASF353:
	.ascii	"ssp0_rx_dma_en\000"
.LASF182:
	.ascii	"CLKPWR_EVENT_EXT_U3_HCTS\000"
.LASF52:
	.ascii	"clkpwr_timers_pwms_clk_ctrl_1\000"
.LASF243:
	.ascii	"ifreq\000"
.LASF347:
	.ascii	"clkpwr_setup_adc_ts\000"
.LASF292:
	.ascii	"clkpwr_get_osc\000"
.LASF167:
	.ascii	"CLKPWR_EVENT_EXT_SPI2_DATIN\000"
.LASF30:
	.ascii	"clkpwr_lcdclk_ctrl\000"
.LASF32:
	.ascii	"reserved2\000"
.LASF189:
	.ascii	"CLKPWR_EVENT_GPIO_P0_00\000"
.LASF190:
	.ascii	"CLKPWR_EVENT_GPIO_P0_01\000"
.LASF191:
	.ascii	"CLKPWR_EVENT_GPIO_P0_02\000"
.LASF121:
	.ascii	"CLKPWR_SPI2_CLK_PAD\000"
.LASF193:
	.ascii	"CLKPWR_EVENT_GPIO_P0_04\000"
.LASF38:
	.ascii	"clkpwr_ddr_cal_delay\000"
.LASF195:
	.ascii	"CLKPWR_EVENT_GPIO_P0_06\000"
.LASF196:
	.ascii	"CLKPWR_EVENT_GPIO_P0_07\000"
.LASF173:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O2\000"
.LASF284:
	.ascii	"enable\000"
.LASF100:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF257:
	.ascii	"clkpwr_get_event_field\000"
.LASF176:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O5\000"
.LASF91:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF349:
	.ascii	"clkdiv\000"
.LASF133:
	.ascii	"cco_bypass_b15\000"
.LASF307:
	.ascii	"hclk_clk\000"
.LASF312:
	.ascii	"clkpwr_set_mode\000"
.LASF236:
	.ascii	"start_er\000"
.LASF19:
	.ascii	"clkpwr_int_sr\000"
.LASF16:
	.ascii	"clkpwr_usbclk_pdiv\000"
.LASF276:
	.ascii	"bias\000"
.LASF320:
	.ascii	"clkpwr_is_raw_event_active\000"
.LASF153:
	.ascii	"CLKPWR_EVENT_INT_USBATXINT\000"
.LASF88:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF49:
	.ascii	"clkpwr_adc_clk_ctrl\000"
.LASF341:
	.ascii	"pwm_num\000"
.LASF269:
	.ascii	"fclkout\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF124:
	.ascii	"CLKPWR_MUX_STATE_T\000"
.LASF75:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF248:
	.ascii	"cfreq\000"
.LASF311:
	.ascii	"force\000"
.LASF322:
	.ascii	"clkpwr_is_msk_event_active\000"
.LASF285:
	.ascii	"clkpwr_is_pll_locked\000"
.LASF150:
	.ascii	"CLKPWR_EVENT_INT_GPIO_P0_P1\000"
.LASF325:
	.ascii	"high\000"
.LASF125:
	.ascii	"CLKPWR_MAIN_OSC\000"
.LASF298:
	.ascii	"hclkval\000"
.LASF134:
	.ascii	"direct_output_b14\000"
.LASF258:
	.ascii	"event_id\000"
.LASF29:
	.ascii	"clkpwr_sysclk_ctrl\000"
.LASF352:
	.ascii	"ssp1_tx_dma_en\000"
.LASF197:
	.ascii	"CLKPWR_EVENT_GPIO_P1_00\000"
.LASF198:
	.ascii	"CLKPWR_EVENT_GPIO_P1_01\000"
.LASF17:
	.ascii	"clkpwr_int_er\000"
.LASF117:
	.ascii	"CLKPWR_SYSCLKEN\000"
.LASF144:
	.ascii	"CLKPWR_EVENT_INT_GPIO_00\000"
.LASF145:
	.ascii	"CLKPWR_EVENT_INT_GPIO_01\000"
.LASF146:
	.ascii	"CLKPWR_EVENT_INT_GPIO_02\000"
.LASF14:
	.ascii	"clkpwr_bootmap\000"
.LASF148:
	.ascii	"CLKPWR_EVENT_INT_GPIO_04\000"
.LASF149:
	.ascii	"CLKPWR_EVENT_INT_GPIO_05\000"
.LASF294:
	.ascii	"clkpwr_set_hclk_divs\000"
.LASF289:
	.ascii	"osc_enable\000"
.LASF98:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF360:
	.ascii	"i2s0_tx_clk_from_rx\000"
.LASF118:
	.ascii	"CLKPWR_TEST_CLK1\000"
.LASF119:
	.ascii	"CLKPWR_TEST_CLK2\000"
.LASF242:
	.ascii	"clkpwr_check_pll_setup\000"
.LASF271:
	.ascii	"freqret\000"
.LASF57:
	.ascii	"clkpwr_uart4_clk_ctrl\000"
.LASF78:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF231:
	.ascii	"CLKPWR_LCDMUX_STN4M\000"
.LASF337:
	.ascii	"freq_div\000"
.LASF359:
	.ascii	"i2s0_rx_clk_from_tx\000"
.LASF362:
	.ascii	"lcdpins\000"
.LASF247:
	.ascii	"fref\000"
.LASF315:
	.ascii	"clkpwr_get_clock_rate\000"
.LASF81:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF186:
	.ascii	"CLKPWR_EVENT_EXT_U6_IRRX\000"
.LASF95:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF354:
	.ascii	"ssp0_tx_dma_en\000"
.LASF221:
	.ascii	"CLKPWR_EVENT_LAST\000"
.LASF343:
	.ascii	"clkpwr_setup_nand_ctrlr\000"
.LASF72:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF171:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O0\000"
.LASF172:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O1\000"
.LASF36:
	.ascii	"clkpwr_ddr_lap_nom\000"
.LASF174:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O3\000"
.LASF175:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O4\000"
.LASF127:
	.ascii	"CLKPWR_OSC_T\000"
.LASF170:
	.ascii	"CLKPWR_EVENT_EXT_SYSCLKEN\000"
.LASF168:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O7\000"
.LASF164:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O8\000"
.LASF165:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O9\000"
.LASF107:
	.ascii	"CLKPWR_ARM_CLK\000"
.LASF314:
	.ascii	"clkpwr_autoclk_en_dis\000"
.LASF158:
	.ascii	"CLKPWR_EVENT_INT_RTC\000"
.LASF120:
	.ascii	"CLKPWR_SPI2_DATIO\000"
.LASF138:
	.ascii	"pll_m\000"
.LASF137:
	.ascii	"pll_n\000"
.LASF139:
	.ascii	"CLKPWR_HCLK_PLL_SETUP_T\000"
.LASF136:
	.ascii	"pll_p\000"
.LASF203:
	.ascii	"CLKPWR_EVENT_GPIO_P1_06\000"
.LASF27:
	.ascii	"clkpwr_pll397_ctrl\000"
.LASF2:
	.ascii	"char\000"
.LASF223:
	.ascii	"CLKPWR_MD_RUN\000"
.LASF270:
	.ascii	"flag\000"
.LASF135:
	.ascii	"fdbk_div_ctrl_b13\000"
.LASF166:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_10\000"
.LASF299:
	.ascii	"clkpwr_get_base_clock_rate\000"
.LASF335:
	.ascii	"dat1_pullup_on\000"
.LASF282:
	.ascii	"pllsetupval\000"
.LASF180:
	.ascii	"CLKPWR_EVENT_EXT_U1_RX\000"
.LASF327:
	.ascii	"clkpwr_set_mux\000"
.LASF92:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF147:
	.ascii	"CLKPWR_EVENT_INT_GPIO_03\000"
.LASF129:
	.ascii	"CLKPWR_HCLK_PLL\000"
.LASF255:
	.ascii	"pReg\000"
.LASF151:
	.ascii	"CLKPWR_EVENT_INT_ENET_MAC\000"
.LASF356:
	.ascii	"i2s1_tx_clk_from_rx\000"
.LASF216:
	.ascii	"CLKPWR_EVENT_GPIO_P1_19\000"
.LASF37:
	.ascii	"clkpwr_ddr_lap_count\000"
.LASF340:
	.ascii	"clkpwr_setup_pwm\000"
.LASF263:
	.ascii	"baseclk\000"
.LASF84:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF368:
	.ascii	"clkpwr_base_clk\000"
.LASF260:
	.ascii	"status\000"
.LASF85:
	.ascii	"CLKPWR_WDOG_CLK\000"
.LASF286:
	.ascii	"locked\000"
.LASF40:
	.ascii	"clkpwr_i2s_clk_ctrl\000"
.LASF233:
	.ascii	"CLKPWR_LCDMUX_DSTN4M\000"
.LASF346:
	.ascii	"use_dma_req_on_int\000"
.LASF241:
	.ascii	"clkpwr_abs\000"
.LASF159:
	.ascii	"CLKPWR_EVENT_INT_MSTIMER\000"
.LASF246:
	.ascii	"fcco\000"
.LASF293:
	.ascii	"sosc\000"
.LASF319:
	.ascii	"pRegs\000"
.LASF363:
	.ascii	"clkpwr_select_enet_iface\000"
.LASF12:
	.ascii	"long long int\000"
.LASF332:
	.ascii	"clkpwr_setup_mcard_ctrlr\000"
.LASF65:
	.ascii	"clkpwr_uid\000"
.LASF41:
	.ascii	"clkpwr_ms_ctrl\000"
.LASF238:
	.ascii	"start_rsr\000"
.LASF96:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF160:
	.ascii	"CLKPWR_EVENT_INT_USBAHNEEDCLK\000"
.LASF321:
	.ascii	"event_sts\000"
.LASF15:
	.ascii	"clkpwr_p01_er\000"
.LASF304:
	.ascii	"periph_clk\000"
.LASF338:
	.ascii	"clkpwr_set_i2c_driver\000"
.LASF86:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF105:
	.ascii	"CLKPWR_RTC_CLK\000"
.LASF303:
	.ascii	"hclkpll_clk\000"
.LASF328:
	.ascii	"mux_signal\000"
.LASF68:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF281:
	.ascii	"clkpwr_hclkpll_direct_setup\000"
.LASF50:
	.ascii	"clkpwr_pwm_clk_ctrl\000"
.LASF154:
	.ascii	"CLKPWR_EVENT_INT_USBOTGTIMER\000"
.LASF336:
	.ascii	"dat0_pullup_on\000"
.LASF58:
	.ascii	"clkpwr_uart5_clk_ctrl\000"
.LASF192:
	.ascii	"CLKPWR_EVENT_GPIO_P0_03\000"
.LASF194:
	.ascii	"CLKPWR_EVENT_GPIO_P0_05\000"
.LASF131:
	.ascii	"CLKPWR_PLL_T\000"
.LASF24:
	.ascii	"clkpwr_pin_ap\000"
.LASF54:
	.ascii	"clkpwr_nand_clk_ctrl\000"
.LASF256:
	.ascii	"mask\000"
.LASF183:
	.ascii	"CLKPWR_EVENT_EXT_U3_RX\000"
.LASF69:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF25:
	.ascii	"clkpwr_hclk_div\000"
.LASF365:
	.ascii	"clkpwr_halt_cpu\000"
.LASF280:
	.ascii	"clkpwr_usbclkpll_setup\000"
.LASF82:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF232:
	.ascii	"CLKPWR_LCDMUX_STN8C\000"
.LASF169:
	.ascii	"CLKPWR_EVENT_EXT_SPI1_DATIN\000"
.LASF318:
	.ascii	"clkpwr_wk_event_en_dis\000"
.LASF163:
	.ascii	"CLKPWR_EVENT_INT_ADC\000"
.LASF297:
	.ascii	"hclk_div_val\000"
.LASF116:
	.ascii	"CLKPWR_HIGHCORE\000"
.LASF290:
	.ascii	"clkpwr_sysclk_setup\000"
.LASF70:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF94:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF39:
	.ascii	"clkpwr_ssp_blk_ctrl\000"
.LASF34:
	.ascii	"clkpwr_usb_ctrl\000"
.LASF239:
	.ascii	"start_apr\000"
.LASF278:
	.ascii	"clkpwr_hclkpll_setup\000"
.LASF357:
	.ascii	"i2s1_rx_clk_from_tx\000"
.LASF330:
	.ascii	"clkpwr_set_mux_state\000"
.LASF76:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF262:
	.ascii	"ipclk\000"
.LASF317:
	.ascii	"divrate\000"
.LASF244:
	.ascii	"pllsetup\000"
.LASF350:
	.ascii	"clkpwr_setup_ssp\000"
.LASF251:
	.ascii	"osc_rate\000"
.LASF152:
	.ascii	"CLKPWR_EVENT_INT_KEY\000"
.LASF102:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF113:
	.ascii	"CLKPWR_MSSD_CLK\000"
.LASF301:
	.ascii	"ddr_clock\000"
.LASF99:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF46:
	.ascii	"clkpwr_sw_int\000"
.LASF6:
	.ascii	"short int\000"
.LASF1:
	.ascii	"long int\000"
.LASF228:
	.ascii	"CLKPWR_LCDMUX_TFT16\000"
.LASF291:
	.ascii	"bpval\000"
.LASF261:
	.ascii	"clkpwr_get_base_clock\000"
.LASF22:
	.ascii	"clkpwr_pin_rs\000"
.LASF79:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF26:
	.ascii	"clkpwr_pwr_ctrl\000"
.LASF51:
	.ascii	"clkpwr_timer_clk_ctrl\000"
.LASF157:
	.ascii	"CLKPWR_EVENT_INT_USBNEEDCLK\000"
.LASF48:
	.ascii	"clkpwr_key_clk_ctrl\000"
.LASF103:
	.ascii	"CLKPWR_CLK_T\000"
.LASF302:
	.ascii	"ddr_hclk_div\000"
.LASF351:
	.ascii	"ssp1_rx_dma_en\000"
.LASF234:
	.ascii	"CLKPWR_LCDMUX_DSTN8C\000"
.LASF162:
	.ascii	"CLKPWR_EVENT_INT_PD\000"
.LASF288:
	.ascii	"capload_add\000"
.LASF326:
	.ascii	"clkpwr_get_uid\000"
.LASF47:
	.ascii	"clkpwr_i2c_clk_ctrl\000"
.LASF245:
	.ascii	"i64freq\000"
.LASF283:
	.ascii	"clkpwr_pll_dis_en\000"
.LASF73:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF309:
	.ascii	"clkrate\000"
.LASF111:
	.ascii	"CLKPWR_48M_CLK\000"
.LASF300:
	.ascii	"sys_clk\000"
.LASF161:
	.ascii	"CLKPWR_EVENT_INT_AUX\000"
.LASF13:
	.ascii	"reserved1\000"
.LASF369:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF199:
	.ascii	"CLKPWR_EVENT_GPIO_P1_02\000"
.LASF42:
	.ascii	"reserved4\000"
.LASF44:
	.ascii	"reserved5\000"
.LASF202:
	.ascii	"CLKPWR_EVENT_GPIO_P1_05\000"
.LASF55:
	.ascii	"reserved7\000"
.LASF64:
	.ascii	"reserved8\000"
.LASF205:
	.ascii	"CLKPWR_EVENT_GPIO_P1_08\000"
.LASF206:
	.ascii	"CLKPWR_EVENT_GPIO_P1_09\000"
.LASF23:
	.ascii	"clkpwr_pin_sr\000"
.LASF66:
	.ascii	"CLKPWR_REGS_T\000"
.LASF126:
	.ascii	"CLKPWR_PLL397_OSC\000"
.LASF267:
	.ascii	"tol_001\000"
.LASF63:
	.ascii	"clkpwr_autoclock\000"
.LASF35:
	.ascii	"clkpwr_sdramclk_ctrl\000"
.LASF268:
	.ascii	"freqtol\000"
.LASF93:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF342:
	.ascii	"clk_src\000"
.LASF329:
	.ascii	"mux1\000"
.LASF296:
	.ascii	"periph_clk_div\000"
.LASF344:
	.ascii	"use_slc\000"
.LASF287:
	.ascii	"clkpwr_mainosc_setup\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF28:
	.ascii	"clkpwr_main_osc_ctrl\000"
.LASF77:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF59:
	.ascii	"clkpwr_uart6_clk_ctrl\000"
.LASF31:
	.ascii	"clkpwr_hclkpll_ctrl\000"
.LASF306:
	.ascii	"arm1_clk\000"
.LASF277:
	.ascii	"pll_enable\000"
.LASF279:
	.ascii	"pHCLKPllSetup\000"
.LASF207:
	.ascii	"CLKPWR_EVENT_GPIO_P1_10\000"
.LASF208:
	.ascii	"CLKPWR_EVENT_GPIO_P1_11\000"
.LASF209:
	.ascii	"CLKPWR_EVENT_GPIO_P1_12\000"
.LASF210:
	.ascii	"CLKPWR_EVENT_GPIO_P1_13\000"
.LASF211:
	.ascii	"CLKPWR_EVENT_GPIO_P1_14\000"
.LASF212:
	.ascii	"CLKPWR_EVENT_GPIO_P1_15\000"
.LASF213:
	.ascii	"CLKPWR_EVENT_GPIO_P1_16\000"
.LASF214:
	.ascii	"CLKPWR_EVENT_GPIO_P1_17\000"
.LASF215:
	.ascii	"CLKPWR_EVENT_GPIO_P1_18\000"
.LASF128:
	.ascii	"CLKPWR_PLL397\000"
.LASF112:
	.ascii	"CLKPWR_DDR_CLK\000"
.LASF104:
	.ascii	"CLKPWR_MAINOSC_CLK\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF259:
	.ascii	"bitnum\000"
.LASF53:
	.ascii	"clkpwr_spi_clk_ctrl\000"
.LASF122:
	.ascii	"CLKPWR_SPI1_DATIO\000"
.LASF21:
	.ascii	"clkpwr_pin_er\000"
.LASF90:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF308:
	.ascii	"arm_clk\000"
.LASF200:
	.ascii	"CLKPWR_EVENT_GPIO_P1_03\000"
.LASF201:
	.ascii	"CLKPWR_EVENT_GPIO_P1_04\000"
.LASF333:
	.ascii	"enable_pullups\000"
.LASF184:
	.ascii	"CLKPWR_EVENT_EXT_GPI_11\000"
.LASF204:
	.ascii	"CLKPWR_EVENT_GPIO_P1_07\000"
.LASF106:
	.ascii	"CLKPWR_SYSCLK\000"
.LASF97:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF331:
	.ascii	"state\000"
.LASF142:
	.ascii	"CLKPWR_ACLK_IROM\000"
.LASF217:
	.ascii	"CLKPWR_EVENT_GPIO_P1_20\000"
.LASF218:
	.ascii	"CLKPWR_EVENT_GPIO_P1_21\000"
.LASF219:
	.ascii	"CLKPWR_EVENT_GPIO_P1_22\000"
.LASF220:
	.ascii	"CLKPWR_EVENT_GPIO_P1_23\000"
.LASF123:
	.ascii	"CLKPWR_SPI1_CLK_PAD\000"
.LASF188:
	.ascii	"CLKPWR_EVENT_EXT_U7_RX\000"
.LASF87:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF310:
	.ascii	"clkpwr_force_arm_hclk_to_pclk\000"
.LASF265:
	.ascii	"pllin_freq\000"
.LASF10:
	.ascii	"UNS_64\000"
.LASF240:
	.ascii	"CLKPWR_EVENTREG_GROUP_T\000"
.LASF313:
	.ascii	"clkpwr_clk_en_dis\000"
.LASF274:
	.ascii	"clkpwr_pll397_setup\000"
.LASF109:
	.ascii	"CLKPWR_PERIPH_CLK\000"
.LASF178:
	.ascii	"CLKPWR_EVENT_EXT_MSDIO_ST\000"
.LASF4:
	.ascii	"signed char\000"
.LASF358:
	.ascii	"i2s1_use_dma\000"
.LASF361:
	.ascii	"clkpwr_setup_lcd\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF187:
	.ascii	"CLKPWR_EVENT_EXT_U7_HXTC\000"
.LASF364:
	.ascii	"use_rmii\000"
.LASF250:
	.ascii	"clkpwr_pll_rate_from_val\000"
.LASF62:
	.ascii	"clkpwr_dmaclk_ctrl\000"
.LASF348:
	.ascii	"sel_periph_clk\000"
.LASF33:
	.ascii	"clkpwr_adc_clk_ctrl_1\000"
.LASF89:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF83:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF324:
	.ascii	"clkpwr_set_event_pol\000"
.LASF141:
	.ascii	"CLKPWR_ACLK_IRAM\000"
.LASF185:
	.ascii	"CLKPWR_EVENT_EXT_U5_RX\000"
.LASF45:
	.ascii	"clkpwr_test_clk_sel\000"
.LASF71:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF222:
	.ascii	"CLKPWR_EVENT_T\000"
.LASF114:
	.ascii	"CLKPWR_BASE_INVALID\000"
.LASF254:
	.ascii	"pPllreg\000"
.LASF74:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF177:
	.ascii	"CLKPWR_EVENT_EXT_GPIO_O6\000"
.LASF155:
	.ascii	"CLKPWR_EVENT_INT_USB_I2C\000"
.LASF367:
	.ascii	"hclkdivs\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
