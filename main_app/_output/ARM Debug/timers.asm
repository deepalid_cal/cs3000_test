	.file	"timers.c"
	.text
.Ltext0:
	.section	.bss.xActiveTimerList1,"aw",%nobits
	.align	2
	.type	xActiveTimerList1, %object
	.size	xActiveTimerList1, 20
xActiveTimerList1:
	.space	20
	.section	.bss.xActiveTimerList2,"aw",%nobits
	.align	2
	.type	xActiveTimerList2, %object
	.size	xActiveTimerList2, 20
xActiveTimerList2:
	.space	20
	.section	.bss.pxCurrentTimerList,"aw",%nobits
	.align	2
	.type	pxCurrentTimerList, %object
	.size	pxCurrentTimerList, 4
pxCurrentTimerList:
	.space	4
	.section	.bss.pxOverflowTimerList,"aw",%nobits
	.align	2
	.type	pxOverflowTimerList, %object
	.size	pxOverflowTimerList, 4
pxOverflowTimerList:
	.space	4
	.section	.bss.xTimerQueue,"aw",%nobits
	.align	2
	.type	xTimerQueue, %object
	.size	xTimerQueue, 4
xTimerQueue:
	.space	4
	.section	.bss.xTimerTaskHandle,"aw",%nobits
	.align	2
	.type	xTimerTaskHandle, %object
	.size	xTimerTaskHandle, 4
xTimerTaskHandle:
	.space	4
	.section .rodata
	.align	2
.LC0:
	.ascii	"Tmr Svc\000"
	.align	2
.LC1:
	.ascii	"xReturn\000"
	.align	2
.LC2:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/timers.c\000"
	.section	.text.xTimerCreateTimerTask,"ax",%progbits
	.align	2
	.global	xTimerCreateTimerTask
	.type	xTimerCreateTimerTask, %function
xTimerCreateTimerTask:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/timers.c"
	.loc 1 188 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #20
.LCFI2:
	.loc 1 189 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 195 0
	bl	prvCheckForValidListAndQueue
	.loc 1 197 0
	ldr	r3, .L4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L2
	.loc 1 203 0
	mov	r3, #13
	str	r3, [sp, #0]
	ldr	r3, .L4+4
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r3, #0
	str	r3, [sp, #12]
	ldr	r0, .L4+8
	ldr	r1, .L4+12
	mov	r2, #2048
	mov	r3, #0
	bl	xTaskGenericCreate
	str	r0, [fp, #-8]
.L2:
	.loc 1 213 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L3
	.loc 1 213 0 is_stmt 0 discriminator 1
	ldr	r0, .L4+16
	ldr	r1, .L4+20
	mov	r2, #213
	bl	__assert
.L3:
	.loc 1 214 0 is_stmt 1
	ldr	r3, [fp, #-8]
	.loc 1 215 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	xTimerQueue
	.word	xTimerTaskHandle
	.word	prvTimerTask
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE0:
	.size	xTimerCreateTimerTask, .-xTimerCreateTimerTask
	.section .rodata
	.align	2
.LC3:
	.ascii	"( xTimerPeriodInTicks > 0 )\000"
	.section	.text.xTimerCreate,"ax",%progbits
	.align	2
	.global	xTimerCreate
	.type	xTimerCreate, %function
xTimerCreate:
.LFB1:
	.loc 1 219 0
	@ args = 4, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #20
.LCFI5:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 223 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L7
	.loc 1 225 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 226 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L8
	.loc 1 226 0 is_stmt 0 discriminator 1
	ldr	r0, .L9
	ldr	r1, .L9+4
	mov	r2, #226
	bl	__assert
	b	.L8
.L7:
	.loc 1 230 0 is_stmt 1
	mov	r0, #40
	bl	pvPortMalloc
	str	r0, [fp, #-8]
	.loc 1 231 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L8
	.loc 1 235 0
	bl	prvCheckForValidListAndQueue
	.loc 1 238 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 239 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #24]
	.loc 1 240 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #28]
	.loc 1 241 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-24]
	str	r2, [r3, #32]
	.loc 1 242 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #4]
	str	r2, [r3, #36]
	.loc 1 243 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListInitialiseItem
.L8:
	.loc 1 253 0
	ldr	r3, [fp, #-8]
	.loc 1 254 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L10:
	.align	2
.L9:
	.word	.LC3
	.word	.LC2
.LFE1:
	.size	xTimerCreate, .-xTimerCreate
	.section .rodata
	.align	2
.LC4:
	.ascii	"--> NULL timer call <--\000"
	.section	.text.xTimerGenericCommand,"ax",%progbits
	.align	2
	.global	xTimerGenericCommand
	.type	xTimerGenericCommand, %function
xTimerGenericCommand:
.LFB2:
	.loc 1 258 0
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #32
.LCFI8:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	str	r3, [fp, #-36]
	.loc 1 259 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 277 0
	ldr	r3, .L17
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L12
	.loc 1 277 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L12
	.loc 1 281 0 is_stmt 1
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-20]
	.loc 1 282 0
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-16]
	.loc 1 283 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-12]
	.loc 1 285 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	bne	.L13
	.loc 1 287 0
	bl	xTaskGetSchedulerState
	mov	r3, r0
	cmp	r3, #1
	bne	.L14
	.loc 1 289 0
	ldr	r3, .L17
	ldr	r2, [r3, #0]
	sub	r3, fp, #20
	mov	r0, r2
	mov	r1, r3
	ldr	r2, [fp, #4]
	mov	r3, #0
	bl	xQueueGenericSend
	str	r0, [fp, #-8]
	.loc 1 285 0
	b	.L16
.L14:
	.loc 1 293 0
	ldr	r3, .L17
	ldr	r2, [r3, #0]
	sub	r3, fp, #20
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	str	r0, [fp, #-8]
	.loc 1 285 0
	b	.L16
.L13:
	.loc 1 298 0
	ldr	r3, .L17
	ldr	r2, [r3, #0]
	sub	r3, fp, #20
	mov	r0, r2
	mov	r1, r3
	ldr	r2, [fp, #-36]
	mov	r3, #0
	bl	xQueueGenericSendFromISR
	str	r0, [fp, #-8]
	.loc 1 285 0
	b	.L16
.L12:
.LBB2:
	.loc 1 308 0
	ldr	r0, .L17+4
	bl	Alert_Message
.L16:
.LBE2:
	.loc 1 311 0
	ldr	r3, [fp, #-8]
	.loc 1 312 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L18:
	.align	2
.L17:
	.word	xTimerQueue
	.word	.LC4
.LFE2:
	.size	xTimerGenericCommand, .-xTimerGenericCommand
	.section .rodata
	.align	2
.LC5:
	.ascii	"( xTimerTaskHandle != 0 )\000"
	.section	.text.xTimerGetTimerDaemonTaskHandle,"ax",%progbits
	.align	2
	.global	xTimerGetTimerDaemonTaskHandle
	.type	xTimerGetTimerDaemonTaskHandle, %function
xTimerGetTimerDaemonTaskHandle:
.LFB3:
	.loc 1 318 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	.loc 1 321 0
	ldr	r3, .L21
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L20
	.loc 1 321 0 is_stmt 0 discriminator 1
	ldr	r0, .L21+4
	ldr	r1, .L21+8
	ldr	r2, .L21+12
	bl	__assert
.L20:
	.loc 1 322 0 is_stmt 1
	ldr	r3, .L21
	ldr	r3, [r3, #0]
	.loc 1 323 0
	mov	r0, r3
	ldmfd	sp!, {fp, pc}
.L22:
	.align	2
.L21:
	.word	xTimerTaskHandle
	.word	.LC5
	.word	.LC2
	.word	321
.LFE3:
	.size	xTimerGetTimerDaemonTaskHandle, .-xTimerGetTimerDaemonTaskHandle
	.section .rodata
	.align	2
.LC6:
	.ascii	"xResult\000"
	.section	.text.prvProcessExpiredTimer,"ax",%progbits
	.align	2
	.type	prvProcessExpiredTimer, %function
prvProcessExpiredTimer:
.LFB4:
	.loc 1 329 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	sub	sp, sp, #20
.LCFI13:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 335 0
	ldr	r3, .L25
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #12]
	ldr	r3, [r3, #12]
	str	r3, [fp, #-8]
	.loc 1 336 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 341 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	cmp	r3, #1
	bne	.L24
	.loc 1 349 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #24]
	ldr	r3, [fp, #-16]
	add	r3, r2, r3
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-16]
	bl	prvInsertTimerInActiveList
	mov	r3, r0
	cmp	r3, #1
	bne	.L24
	.loc 1 353 0
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	mov	r1, #0
	ldr	r2, [fp, #-16]
	mov	r3, #0
	bl	xTimerGenericCommand
	str	r0, [fp, #-12]
	.loc 1 354 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L24
	.loc 1 354 0 is_stmt 0 discriminator 1
	ldr	r0, .L25+4
	ldr	r1, .L25+8
	ldr	r2, .L25+12
	bl	__assert
.L24:
	.loc 1 360 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #36]
	ldr	r0, [fp, #-8]
	blx	r3
	.loc 1 361 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L26:
	.align	2
.L25:
	.word	pxCurrentTimerList
	.word	.LC6
	.word	.LC2
	.word	354
.LFE4:
	.size	prvProcessExpiredTimer, .-prvProcessExpiredTimer
	.section	.text.prvTimerTask,"ax",%progbits
	.align	2
	.type	prvTimerTask, %function
prvTimerTask:
.LFB5:
	.loc 1 365 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI14:
	add	fp, sp, #4
.LCFI15:
	sub	sp, sp, #12
.LCFI16:
	str	r0, [fp, #-16]
.L28:
	.loc 1 376 0 discriminator 1
	sub	r3, fp, #12
	mov	r0, r3
	bl	prvGetNextExpireTime
	str	r0, [fp, #-8]
	.loc 1 380 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	bl	prvProcessTimerOrBlockTask
	.loc 1 383 0 discriminator 1
	bl	prvProcessReceivedCommands
	.loc 1 384 0 discriminator 1
	b	.L28
.LFE5:
	.size	prvTimerTask, .-prvTimerTask
	.section	.text.prvProcessTimerOrBlockTask,"ax",%progbits
	.align	2
	.type	prvProcessTimerOrBlockTask, %function
prvProcessTimerOrBlockTask:
.LFB6:
	.loc 1 389 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI17:
	add	fp, sp, #4
.LCFI18:
	sub	sp, sp, #16
.LCFI19:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 393 0
	bl	vTaskSuspendAll
	.loc 1 400 0
	sub	r3, fp, #12
	mov	r0, r3
	bl	prvSampleTimeNow
	str	r0, [fp, #-8]
	.loc 1 401 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L30
	.loc 1 404 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L31
	.loc 1 404 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bhi	.L31
	.loc 1 406 0 is_stmt 1
	bl	xTaskResumeAll
	.loc 1 407 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-8]
	bl	prvProcessExpiredTimer
	b	.L29
.L31:
	.loc 1 417 0
	ldr	r3, .L35
	ldr	r2, [r3, #0]
	ldr	r1, [fp, #-16]
	ldr	r3, [fp, #-8]
	rsb	r3, r3, r1
	mov	r0, r2
	mov	r1, r3
	bl	vQueueWaitForMessageRestricted
	.loc 1 419 0
	bl	xTaskResumeAll
	mov	r3, r0
	cmp	r3, #0
	bne	.L34
	.loc 1 425 0
@ 425 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/timers.c" 1
	SWI 0
@ 0 "" 2
	b	.L34
.L30:
	.loc 1 431 0
	bl	xTaskResumeAll
	b	.L29
.L34:
	.loc 1 425 0
	mov	r0, r0	@ nop
.L29:
	.loc 1 434 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L36:
	.align	2
.L35:
	.word	xTimerQueue
.LFE6:
	.size	prvProcessTimerOrBlockTask, .-prvProcessTimerOrBlockTask
	.section	.text.prvGetNextExpireTime,"ax",%progbits
	.align	2
	.type	prvGetNextExpireTime, %function
prvGetNextExpireTime:
.LFB7:
	.loc 1 438 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI20:
	add	fp, sp, #0
.LCFI21:
	sub	sp, sp, #8
.LCFI22:
	str	r0, [fp, #-8]
	.loc 1 448 0
	ldr	r3, .L40
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 449 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L38
	.loc 1 451 0
	ldr	r3, .L40
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #12]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-4]
	b	.L39
.L38:
	.loc 1 456 0
	mov	r3, #0
	str	r3, [fp, #-4]
.L39:
	.loc 1 459 0
	ldr	r3, [fp, #-4]
	.loc 1 460 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L41:
	.align	2
.L40:
	.word	pxCurrentTimerList
.LFE7:
	.size	prvGetNextExpireTime, .-prvGetNextExpireTime
	.section	.text.prvSampleTimeNow,"ax",%progbits
	.align	2
	.type	prvSampleTimeNow, %function
prvSampleTimeNow:
.LFB8:
	.loc 1 464 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI23:
	add	fp, sp, #4
.LCFI24:
	sub	sp, sp, #8
.LCFI25:
	str	r0, [fp, #-12]
	.loc 1 468 0
	bl	xTaskGetTickCount
	str	r0, [fp, #-8]
	.loc 1 470 0
	ldr	r3, .L45
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	bcs	.L43
	.loc 1 472 0
	ldr	r3, .L45
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	prvSwitchTimerLists
	.loc 1 473 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L44
.L43:
	.loc 1 477 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #0]
.L44:
	.loc 1 480 0
	ldr	r3, .L45
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 482 0
	ldr	r3, [fp, #-8]
	.loc 1 483 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L46:
	.align	2
.L45:
	.word	xLastTime.1193
.LFE8:
	.size	prvSampleTimeNow, .-prvSampleTimeNow
	.section	.text.prvInsertTimerInActiveList,"ax",%progbits
	.align	2
	.type	prvInsertTimerInActiveList, %function
prvInsertTimerInActiveList:
.LFB9:
	.loc 1 487 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	sub	sp, sp, #20
.LCFI28:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 488 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 490 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #4]
	.loc 1 491 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #16]
	.loc 1 493 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bhi	.L48
	.loc 1 497 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-24]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #24]
	cmp	r2, r3
	bcc	.L49
	.loc 1 501 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L50
.L49:
	.loc 1 505 0
	ldr	r3, .L52
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsert
	b	.L50
.L48:
	.loc 1 510 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bcs	.L51
	.loc 1 510 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bcc	.L51
	.loc 1 515 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L50
.L51:
	.loc 1 519 0
	ldr	r3, .L52+4
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsert
.L50:
	.loc 1 523 0
	ldr	r3, [fp, #-8]
	.loc 1 524 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L53:
	.align	2
.L52:
	.word	pxOverflowTimerList
	.word	pxCurrentTimerList
.LFE9:
	.size	prvInsertTimerInActiveList, .-prvInsertTimerInActiveList
	.section .rodata
	.align	2
.LC7:
	.ascii	"( pxTimer->xTimerPeriodInTicks > 0 )\000"
	.section	.text.prvProcessReceivedCommands,"ax",%progbits
	.align	2
	.type	prvProcessReceivedCommands, %function
prvProcessReceivedCommands:
.LFB10:
	.loc 1 528 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI29:
	add	fp, sp, #4
.LCFI30:
	sub	sp, sp, #32
.LCFI31:
	.loc 1 534 0
	b	.L55
.L65:
	.loc 1 536 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-8]
	.loc 1 538 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #20]
	cmp	r3, #0
	beq	.L56
	.loc 1 541 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
.L56:
	.loc 1 552 0
	sub	r3, fp, #32
	mov	r0, r3
	bl	prvSampleTimeNow
	str	r0, [fp, #-12]
	.loc 1 554 0
	ldr	r3, [fp, #-28]
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L66
.L62:
	.word	.L58
	.word	.L66
	.word	.L60
	.word	.L61
.L58:
	.loc 1 558 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	add	r2, r2, r3
	ldr	r3, [fp, #-24]
	ldr	r0, [fp, #-8]
	mov	r1, r2
	ldr	r2, [fp, #-12]
	bl	prvInsertTimerInActiveList
	mov	r3, r0
	cmp	r3, #1
	bne	.L67
	.loc 1 562 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #36]
	ldr	r0, [fp, #-8]
	blx	r3
	.loc 1 564 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #28]
	cmp	r3, #1
	bne	.L67
	.loc 1 566 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	add	r3, r2, r3
	mov	r2, #0
	str	r2, [sp, #0]
	ldr	r0, [fp, #-8]
	mov	r1, #0
	mov	r2, r3
	mov	r3, #0
	bl	xTimerGenericCommand
	str	r0, [fp, #-16]
	.loc 1 567 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L67
	.loc 1 567 0 is_stmt 0 discriminator 1
	ldr	r0, .L68
	ldr	r1, .L68+4
	ldr	r2, .L68+8
	bl	__assert
	.loc 1 571 0 is_stmt 1 discriminator 1
	b	.L67
.L60:
	.loc 1 579 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #24]
	.loc 1 580 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	cmp	r3, #0
	bne	.L64
	.loc 1 580 0 is_stmt 0 discriminator 1
	ldr	r0, .L68+12
	ldr	r1, .L68+4
	mov	r2, #580
	bl	__assert
.L64:
	.loc 1 581 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #24]
	ldr	r3, [fp, #-12]
	add	r3, r2, r3
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-12]
	bl	prvInsertTimerInActiveList
	.loc 1 582 0
	b	.L55
.L61:
	.loc 1 587 0
	ldr	r0, [fp, #-8]
	bl	vPortFree
	.loc 1 588 0
	b	.L55
.L66:
	.loc 1 592 0
	mov	r0, r0	@ nop
	b	.L55
.L67:
	.loc 1 571 0
	mov	r0, r0	@ nop
.L55:
	.loc 1 534 0 discriminator 1
	ldr	r3, .L68+16
	ldr	r2, [r3, #0]
	sub	r3, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	mov	r3, r0
	cmp	r3, #0
	bne	.L65
	.loc 1 595 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L69:
	.align	2
.L68:
	.word	.LC6
	.word	.LC2
	.word	567
	.word	.LC7
	.word	xTimerQueue
.LFE10:
	.size	prvProcessReceivedCommands, .-prvProcessReceivedCommands
	.section	.text.prvSwitchTimerLists,"ax",%progbits
	.align	2
	.type	prvSwitchTimerLists, %function
prvSwitchTimerLists:
.LFB11:
	.loc 1 599 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI32:
	add	fp, sp, #4
.LCFI33:
	sub	sp, sp, #28
.LCFI34:
	str	r0, [fp, #-28]
	.loc 1 612 0
	b	.L71
.L73:
	.loc 1 614 0
	ldr	r3, .L74
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #12]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 617 0
	ldr	r3, .L74
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #12]
	ldr	r3, [r3, #12]
	str	r3, [fp, #-12]
	.loc 1 618 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r3
	bl	vListRemove
	.loc 1 623 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #36]
	ldr	r0, [fp, #-12]
	blx	r3
	.loc 1 625 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #28]
	cmp	r3, #1
	bne	.L71
	.loc 1 633 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #24]
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 634 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bls	.L72
	.loc 1 636 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #4]
	.loc 1 637 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #16]
	.loc 1 638 0
	ldr	r3, .L74
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	vListInsert
	b	.L71
.L72:
	.loc 1 642 0
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r0, [fp, #-12]
	mov	r1, #0
	ldr	r2, [fp, #-8]
	mov	r3, #0
	bl	xTimerGenericCommand
	str	r0, [fp, #-20]
	.loc 1 643 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L71
	.loc 1 643 0 is_stmt 0 discriminator 1
	ldr	r0, .L74+4
	ldr	r1, .L74+8
	ldr	r2, .L74+12
	bl	__assert
.L71:
	.loc 1 612 0 is_stmt 1 discriminator 1
	ldr	r3, .L74
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L73
	.loc 1 649 0
	ldr	r3, .L74
	ldr	r3, [r3, #0]
	str	r3, [fp, #-24]
	.loc 1 650 0
	ldr	r3, .L74+16
	ldr	r2, [r3, #0]
	ldr	r3, .L74
	str	r2, [r3, #0]
	.loc 1 651 0
	ldr	r3, .L74+16
	ldr	r2, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 652 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L75:
	.align	2
.L74:
	.word	pxCurrentTimerList
	.word	.LC6
	.word	.LC2
	.word	643
	.word	pxOverflowTimerList
.LFE11:
	.size	prvSwitchTimerLists, .-prvSwitchTimerLists
	.section	.text.prvCheckForValidListAndQueue,"ax",%progbits
	.align	2
	.type	prvCheckForValidListAndQueue, %function
prvCheckForValidListAndQueue:
.LFB12:
	.loc 1 656 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI35:
	add	fp, sp, #4
.LCFI36:
	.loc 1 660 0
	bl	vPortEnterCritical
	.loc 1 662 0
	ldr	r3, .L78
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L77
	.loc 1 664 0
	ldr	r0, .L78+4
	bl	vListInitialise
	.loc 1 665 0
	ldr	r0, .L78+8
	bl	vListInitialise
	.loc 1 666 0
	ldr	r3, .L78+12
	ldr	r2, .L78+4
	str	r2, [r3, #0]
	.loc 1 667 0
	ldr	r3, .L78+16
	ldr	r2, .L78+8
	str	r2, [r3, #0]
	.loc 1 668 0
	mov	r0, #20
	mov	r1, #12
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L78
	str	r2, [r3, #0]
.L77:
	.loc 1 671 0
	bl	vPortExitCritical
	.loc 1 672 0
	ldmfd	sp!, {fp, pc}
.L79:
	.align	2
.L78:
	.word	xTimerQueue
	.word	xActiveTimerList1
	.word	xActiveTimerList2
	.word	pxCurrentTimerList
	.word	pxOverflowTimerList
.LFE12:
	.size	prvCheckForValidListAndQueue, .-prvCheckForValidListAndQueue
	.section	.text.xTimerIsTimerActive,"ax",%progbits
	.align	2
	.global	xTimerIsTimerActive
	.type	xTimerIsTimerActive, %function
xTimerIsTimerActive:
.LFB13:
	.loc 1 676 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI37:
	add	fp, sp, #4
.LCFI38:
	sub	sp, sp, #12
.LCFI39:
	str	r0, [fp, #-16]
	.loc 1 678 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-8]
	.loc 1 681 0
	bl	vPortEnterCritical
	.loc 1 686 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #20]
	cmp	r3, #0
	moveq	r3, #0
	movne	r3, #1
	str	r3, [fp, #-12]
	.loc 1 688 0
	bl	vPortExitCritical
	.loc 1 690 0
	ldr	r3, [fp, #-12]
	.loc 1 691 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE13:
	.size	xTimerIsTimerActive, .-xTimerIsTimerActive
	.section	.text.pvTimerGetTimerID,"ax",%progbits
	.align	2
	.global	pvTimerGetTimerID
	.type	pvTimerGetTimerID, %function
pvTimerGetTimerID:
.LFB14:
	.loc 1 695 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI40:
	add	fp, sp, #0
.LCFI41:
	sub	sp, sp, #8
.LCFI42:
	str	r0, [fp, #-8]
	.loc 1 696 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-4]
	.loc 1 698 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #32]
	.loc 1 699 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE14:
	.size	pvTimerGetTimerID, .-pvTimerGetTimerID
	.section	.bss.xLastTime.1193,"aw",%nobits
	.align	2
	.type	xLastTime.1193, %object
	.size	xLastTime.1193, 4
xLastTime.1193:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI14-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI17-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI20-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI23-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI26-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI29-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI32-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI35-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI37-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI40-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/list.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/task.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7a4
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF83
	.byte	0x1
	.4byte	.LASF84
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x4
	.byte	0x4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x5
	.4byte	.LASF13
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x14
	.byte	0x2
	.byte	0x69
	.4byte	0xc4
	.uleb128 0x7
	.4byte	.LASF8
	.byte	0x2
	.byte	0x6b
	.4byte	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF9
	.byte	0x2
	.byte	0x6c
	.4byte	0xc4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF10
	.byte	0x2
	.byte	0x6d
	.4byte	0xc4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x7
	.4byte	.LASF11
	.byte	0x2
	.byte	0x6e
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x7
	.4byte	.LASF12
	.byte	0x2
	.byte	0x6f
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0xca
	.uleb128 0x9
	.4byte	0x71
	.uleb128 0x5
	.4byte	.LASF14
	.byte	0x2
	.byte	0x71
	.4byte	0x71
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0xc
	.byte	0x2
	.byte	0x73
	.4byte	0x111
	.uleb128 0x7
	.4byte	.LASF8
	.byte	0x2
	.byte	0x75
	.4byte	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF9
	.byte	0x2
	.byte	0x76
	.4byte	0xc4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF10
	.byte	0x2
	.byte	0x77
	.4byte	0xc4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x5
	.4byte	.LASF17
	.byte	0x2
	.byte	0x79
	.4byte	0xda
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x14
	.byte	0x2
	.byte	0x7e
	.4byte	0x153
	.uleb128 0x7
	.4byte	.LASF19
	.byte	0x2
	.byte	0x80
	.4byte	0x153
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF20
	.byte	0x2
	.byte	0x81
	.4byte	0x158
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF21
	.byte	0x2
	.byte	0x82
	.4byte	0x163
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x9
	.4byte	0x25
	.uleb128 0x8
	.byte	0x4
	.4byte	0x15e
	.uleb128 0x9
	.4byte	0xcf
	.uleb128 0x9
	.4byte	0x111
	.uleb128 0x5
	.4byte	.LASF22
	.byte	0x2
	.byte	0x83
	.4byte	0x11c
	.uleb128 0x5
	.4byte	.LASF23
	.byte	0x4
	.byte	0x63
	.4byte	0x3a
	.uleb128 0x8
	.byte	0x4
	.4byte	0x184
	.uleb128 0xa
	.4byte	0x3c
	.uleb128 0x5
	.4byte	.LASF24
	.byte	0x5
	.byte	0x57
	.4byte	0x3a
	.uleb128 0x5
	.4byte	.LASF25
	.byte	0x6
	.byte	0x65
	.4byte	0x3a
	.uleb128 0x5
	.4byte	.LASF26
	.byte	0x6
	.byte	0x68
	.4byte	0x1aa
	.uleb128 0x8
	.byte	0x4
	.4byte	0x1b0
	.uleb128 0xb
	.byte	0x1
	.4byte	0x1bc
	.uleb128 0xc
	.4byte	0x194
	.byte	0
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x28
	.byte	0x1
	.byte	0x59
	.4byte	0x21d
	.uleb128 0x7
	.4byte	.LASF28
	.byte	0x1
	.byte	0x5b
	.4byte	0x17e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF29
	.byte	0x1
	.byte	0x5c
	.4byte	0xcf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF30
	.byte	0x1
	.byte	0x5d
	.4byte	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x7
	.4byte	.LASF31
	.byte	0x1
	.byte	0x5e
	.4byte	0x25
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x7
	.4byte	.LASF32
	.byte	0x1
	.byte	0x5f
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x7
	.4byte	.LASF33
	.byte	0x1
	.byte	0x60
	.4byte	0x19f
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x5
	.4byte	.LASF34
	.byte	0x1
	.byte	0x61
	.4byte	0x1bc
	.uleb128 0x6
	.4byte	.LASF35
	.byte	0xc
	.byte	0x1
	.byte	0x65
	.4byte	0x25f
	.uleb128 0x7
	.4byte	.LASF36
	.byte	0x1
	.byte	0x67
	.4byte	0x4a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.4byte	.LASF37
	.byte	0x1
	.byte	0x68
	.4byte	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x7
	.4byte	.LASF38
	.byte	0x1
	.byte	0x69
	.4byte	0x25f
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x21d
	.uleb128 0x5
	.4byte	.LASF39
	.byte	0x1
	.byte	0x6a
	.4byte	0x228
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.byte	0xbb
	.byte	0x1
	.4byte	0x4a
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x29c
	.uleb128 0xe
	.4byte	.LASF42
	.byte	0x1
	.byte	0xbd
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.byte	0xda
	.byte	0x1
	.4byte	0x194
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x30e
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x1
	.byte	0xda
	.4byte	0x17e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x1
	.byte	0xda
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x1
	.byte	0xda
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xf
	.4byte	.LASF32
	.byte	0x1
	.byte	0xda
	.4byte	0x3a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x1
	.byte	0xda
	.4byte	0x19f
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xe
	.4byte	.LASF43
	.byte	0x1
	.byte	0xdc
	.4byte	0x25f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x101
	.byte	0x1
	.4byte	0x4a
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x3b1
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x101
	.4byte	0x194
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x11
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x101
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x11
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x101
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x11
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x101
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x11
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x101
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x12
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x103
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x104
	.4byte	0x265
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x132
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	0x3b7
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x4a
	.uleb128 0x8
	.byte	0x4
	.4byte	0x3bd
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF51
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x13d
	.byte	0x1
	.4byte	0x173
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x16
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x148
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x434
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x148
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x148
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x14a
	.4byte	0x25f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x14b
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x16c
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x47b
	.uleb128 0x11
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x16c
	.4byte	0x3a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x16e
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x16f
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x184
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x4d1
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x184
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x184
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x186
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x187
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x17
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x1b5
	.byte	0x1
	.4byte	0x66
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x50d
	.uleb128 0x11
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x1b5
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x1b7
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x1cf
	.byte	0x1
	.4byte	0x66
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x55b
	.uleb128 0x11
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x1cf
	.4byte	0x3b1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x1d1
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x1d2
	.4byte	0x66
	.byte	0x5
	.byte	0x3
	.4byte	xLastTime.1193
	.byte	0
	.uleb128 0x17
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x1e6
	.byte	0x1
	.4byte	0x4a
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x5c4
	.uleb128 0x11
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x1e6
	.4byte	0x25f
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x1e6
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x1e6
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x11
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x1e6
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x1e8
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x20f
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x629
	.uleb128 0x12
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x211
	.4byte	0x265
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x212
	.4byte	0x25f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x213
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x213
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x214
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x256
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x69d
	.uleb128 0x11
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x256
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x258
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x258
	.4byte	0x66
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x259
	.4byte	0x69d
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x25a
	.4byte	0x25f
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x25b
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x168
	.uleb128 0x18
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x28f
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x2a3
	.byte	0x1
	.4byte	0x4a
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x704
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x2a3
	.4byte	0x194
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x2a5
	.4byte	0x4a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x2a6
	.4byte	0x25f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x2b6
	.byte	0x1
	.4byte	0x3a
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x741
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x2b6
	.4byte	0x194
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x2b8
	.4byte	0x25f
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xe
	.4byte	.LASF77
	.byte	0x1
	.byte	0x70
	.4byte	0x168
	.byte	0x5
	.byte	0x3
	.4byte	xActiveTimerList1
	.uleb128 0xe
	.4byte	.LASF78
	.byte	0x1
	.byte	0x71
	.4byte	0x168
	.byte	0x5
	.byte	0x3
	.4byte	xActiveTimerList2
	.uleb128 0xe
	.4byte	.LASF79
	.byte	0x1
	.byte	0x72
	.4byte	0x69d
	.byte	0x5
	.byte	0x3
	.4byte	pxCurrentTimerList
	.uleb128 0xe
	.4byte	.LASF80
	.byte	0x1
	.byte	0x73
	.4byte	0x69d
	.byte	0x5
	.byte	0x3
	.4byte	pxOverflowTimerList
	.uleb128 0xe
	.4byte	.LASF81
	.byte	0x1
	.byte	0x76
	.4byte	0x189
	.byte	0x5
	.byte	0x3
	.4byte	xTimerQueue
	.uleb128 0xe
	.4byte	.LASF82
	.byte	0x1
	.byte	0x7a
	.4byte	0x173
	.byte	0x5
	.byte	0x3
	.4byte	xTimerTaskHandle
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI18
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI21
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI24
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI33
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI36
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI38
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI41
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF83:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF54:
	.ascii	"xResult\000"
.LASF39:
	.ascii	"xTIMER_MESSAGE\000"
.LASF73:
	.ascii	"pxTemp\000"
.LASF46:
	.ascii	"xCommandID\000"
.LASF25:
	.ascii	"xTimerHandle\000"
.LASF3:
	.ascii	"short int\000"
.LASF13:
	.ascii	"portTickType\000"
.LASF52:
	.ascii	"xNextExpireTime\000"
.LASF28:
	.ascii	"pcTimerName\000"
.LASF10:
	.ascii	"pxPrevious\000"
.LASF67:
	.ascii	"xNextExpiryTime\000"
.LASF29:
	.ascii	"xTimerListItem\000"
.LASF60:
	.ascii	"xTimerListsWereSwitched\000"
.LASF76:
	.ascii	"pvTimerGetTimerID\000"
.LASF55:
	.ascii	"prvProcessExpiredTimer\000"
.LASF11:
	.ascii	"pvOwner\000"
.LASF34:
	.ascii	"xTIMER\000"
.LASF33:
	.ascii	"pxCallbackFunction\000"
.LASF41:
	.ascii	"xTimerCreate\000"
.LASF71:
	.ascii	"prvSwitchTimerLists\000"
.LASF43:
	.ascii	"pxNewTimer\000"
.LASF79:
	.ascii	"pxCurrentTimerList\000"
.LASF75:
	.ascii	"xTimerIsInActiveList\000"
.LASF50:
	.ascii	"xMessage\000"
.LASF63:
	.ascii	"prvSampleTimeNow\000"
.LASF70:
	.ascii	"prvProcessReceivedCommands\000"
.LASF37:
	.ascii	"xMessageValue\000"
.LASF6:
	.ascii	"long long int\000"
.LASF49:
	.ascii	"xBlockTime\000"
.LASF22:
	.ascii	"xList\000"
.LASF40:
	.ascii	"xTimerCreateTimerTask\000"
.LASF4:
	.ascii	"long int\000"
.LASF18:
	.ascii	"xLIST\000"
.LASF24:
	.ascii	"xQueueHandle\000"
.LASF8:
	.ascii	"xItemValue\000"
.LASF87:
	.ascii	"prvCheckForValidListAndQueue\000"
.LASF64:
	.ascii	"pxTimerListsWereSwitched\000"
.LASF85:
	.ascii	"Alert_Message\000"
.LASF15:
	.ascii	"xLIST_ITEM\000"
.LASF48:
	.ascii	"pxHigherPriorityTaskWoken\000"
.LASF59:
	.ascii	"prvProcessTimerOrBlockTask\000"
.LASF45:
	.ascii	"xTimer\000"
.LASF17:
	.ascii	"xMiniListItem\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF74:
	.ascii	"xTimerIsTimerActive\000"
.LASF57:
	.ascii	"pvParameters\000"
.LASF2:
	.ascii	"signed char\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF44:
	.ascii	"xTimerGenericCommand\000"
.LASF23:
	.ascii	"xTaskHandle\000"
.LASF82:
	.ascii	"xTimerTaskHandle\000"
.LASF12:
	.ascii	"pvContainer\000"
.LASF62:
	.ascii	"prvGetNextExpireTime\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF47:
	.ascii	"xOptionalValue\000"
.LASF51:
	.ascii	"char\000"
.LASF58:
	.ascii	"xListWasEmpty\000"
.LASF9:
	.ascii	"pxNext\000"
.LASF65:
	.ascii	"xLastTime\000"
.LASF56:
	.ascii	"prvTimerTask\000"
.LASF66:
	.ascii	"prvInsertTimerInActiveList\000"
.LASF81:
	.ascii	"xTimerQueue\000"
.LASF77:
	.ascii	"xActiveTimerList1\000"
.LASF78:
	.ascii	"xActiveTimerList2\000"
.LASF53:
	.ascii	"xTimeNow\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF61:
	.ascii	"pxListWasEmpty\000"
.LASF36:
	.ascii	"xMessageID\000"
.LASF19:
	.ascii	"uxNumberOfItems\000"
.LASF14:
	.ascii	"xListItem\000"
.LASF84:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/timers.c\000"
.LASF27:
	.ascii	"tmrTimerControl\000"
.LASF21:
	.ascii	"xListEnd\000"
.LASF80:
	.ascii	"pxOverflowTimerList\000"
.LASF42:
	.ascii	"xReturn\000"
.LASF31:
	.ascii	"uxAutoReload\000"
.LASF26:
	.ascii	"tmrTIMER_CALLBACK\000"
.LASF69:
	.ascii	"xProcessTimerNow\000"
.LASF35:
	.ascii	"tmrTimerQueueMessage\000"
.LASF86:
	.ascii	"xTimerGetTimerDaemonTaskHandle\000"
.LASF20:
	.ascii	"pxIndex\000"
.LASF38:
	.ascii	"pxTimer\000"
.LASF68:
	.ascii	"xCommandTime\000"
.LASF16:
	.ascii	"xMINI_LIST_ITEM\000"
.LASF30:
	.ascii	"xTimerPeriodInTicks\000"
.LASF32:
	.ascii	"pvTimerID\000"
.LASF72:
	.ascii	"xReloadTime\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
