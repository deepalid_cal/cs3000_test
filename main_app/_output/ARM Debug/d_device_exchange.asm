	.file	"d_device_exchange.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.text.DEVICE_EXCHANGE_draw_dialog,"ax",%progbits
	.align	2
	.global	DEVICE_EXCHANGE_draw_dialog
	.type	DEVICE_EXCHANGE_draw_dialog, %function
DEVICE_EXCHANGE_draw_dialog:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_device_exchange.c"
	.loc 1 32 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	.loc 1 34 0
	ldr	r3, .L2
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 36 0
	ldr	r0, .L2+4
	bl	DIALOG_draw_ok_dialog
	.loc 1 37 0
	ldmfd	sp!, {fp, pc}
.L3:
	.align	2
.L2:
	.word	GuiVar_DeviceExchangeProgressBar
	.word	598
.LFE0:
	.size	DEVICE_EXCHANGE_draw_dialog, .-DEVICE_EXCHANGE_draw_dialog
	.section	.text.FDTO_DEVICE_EXCHANGE_update_dialog,"ax",%progbits
	.align	2
	.global	FDTO_DEVICE_EXCHANGE_update_dialog
	.type	FDTO_DEVICE_EXCHANGE_update_dialog, %function
FDTO_DEVICE_EXCHANGE_update_dialog:
.LFB1:
	.loc 1 41 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI2:
	add	fp, sp, #4
.LCFI3:
	.loc 1 42 0
	ldr	r3, .L8
	ldr	r3, [r3, #0]
	add	r3, r3, #1
	and	r2, r3, #3
	ldr	r3, .L8
	str	r2, [r3, #0]
	.loc 1 45 0
	ldr	r3, .L8+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L5
	.loc 1 47 0
	ldr	r3, .L8+8
	ldr	r3, [r3, #0]
	cmp	r3, #199
	bhi	.L6
	.loc 1 49 0
	ldr	r3, .L8+8
	ldr	r3, [r3, #0]
	add	r2, r3, #20
	ldr	r3, .L8+8
	str	r2, [r3, #0]
	b	.L7
.L6:
	.loc 1 53 0
	ldr	r3, .L8+8
	mov	r2, #200
	str	r2, [r3, #0]
	b	.L7
.L5:
	.loc 1 58 0
	ldr	r3, .L8+8
	mov	r2, #200
	str	r2, [r3, #0]
.L7:
	.loc 1 61 0
	bl	FDTO_DIALOG_redraw_ok_dialog
	.loc 1 62 0
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	GuiVar_SpinnerPos
	.word	GuiVar_DeviceExchangeSyncingRadios
	.word	GuiVar_DeviceExchangeProgressBar
.LFE1:
	.size	FDTO_DEVICE_EXCHANGE_update_dialog, .-FDTO_DEVICE_EXCHANGE_update_dialog
	.section	.text.FDTO_DEVICE_EXCHANGE_close_dialog,"ax",%progbits
	.align	2
	.global	FDTO_DEVICE_EXCHANGE_close_dialog
	.type	FDTO_DEVICE_EXCHANGE_close_dialog, %function
FDTO_DEVICE_EXCHANGE_close_dialog:
.LFB2:
	.loc 1 66 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI4:
	add	fp, sp, #4
.LCFI5:
	sub	sp, sp, #16
.LCFI6:
	str	r0, [fp, #-20]
	.loc 1 71 0
	ldr	r3, .L25
	ldr	r3, [r3, #0]
	add	r3, r3, #36864
	str	r3, [fp, #-8]
	.loc 1 73 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #36864
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L11
.L15:
	.word	.L12
	.word	.L13
	.word	.L12
	.word	.L11
	.word	.L13
	.word	.L14
.L12:
	.loc 1 77 0
	ldr	r3, [fp, #-20]
	cmp	r3, #2
	beq	.L17
	cmp	r3, #67
	bne	.L23
.L17:
	.loc 1 84 0
	ldr	r3, .L25+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 87 0
	bl	DIALOG_close_ok_dialog
	.loc 1 90 0
	mov	r3, #67
	str	r3, [fp, #-16]
	.loc 1 91 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 92 0
	ldr	r3, .L25+8
	ldr	r2, [r3, #0]
	ldr	r0, .L25+12
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	blx	r3
	.loc 1 93 0
	mov	r0, r0	@ nop
	.loc 1 98 0
	b	.L10
.L23:
	.loc 1 96 0
	bl	bad_key_beep
	.loc 1 98 0
	b	.L10
.L14:
	.loc 1 101 0
	ldr	r3, [fp, #-20]
	cmp	r3, #2
	beq	.L21
	cmp	r3, #67
	bne	.L24
.L21:
	.loc 1 105 0
	bl	good_key_beep
	.loc 1 107 0
	ldr	r3, .L25+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 112 0
	bl	DIALOG_close_ok_dialog
	.loc 1 113 0
	mov	r0, r0	@ nop
	.loc 1 118 0
	b	.L10
.L24:
	.loc 1 116 0
	bl	bad_key_beep
	.loc 1 118 0
	b	.L10
.L13:
	.loc 1 123 0
	ldr	r3, .L25+4
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 127 0
	bl	DIALOG_close_ok_dialog
	.loc 1 128 0
	b	.L10
.L11:
	.loc 1 136 0
	bl	bad_key_beep
.L10:
	.loc 1 138 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L26:
	.align	2
.L25:
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	g_DIALOG_modal_result
	.word	screen_history_index
	.word	ScreenHistory
.LFE2:
	.size	FDTO_DEVICE_EXCHANGE_close_dialog, .-FDTO_DEVICE_EXCHANGE_close_dialog
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/dialog.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x326
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF40
	.byte	0x1
	.4byte	.LASF41
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x5
	.byte	0x4
	.4byte	0x80
	.uleb128 0x6
	.4byte	0x87
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xa5
	.uleb128 0x9
	.4byte	0x87
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xca
	.uleb128 0xb
	.4byte	.LASF12
	.byte	0x3
	.byte	0x7e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x80
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x82
	.4byte	0xa5
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x15c
	.uleb128 0xb
	.4byte	.LASF15
	.byte	0x4
	.byte	0x7b
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x83
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x86
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x88
	.4byte	0x16d
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x8d
	.4byte	0x17f
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x92
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x96
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x9a
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x9c
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x168
	.uleb128 0xd
	.4byte	0x168
	.byte	0
	.uleb128 0xe
	.4byte	0x41
	.uleb128 0x5
	.byte	0x4
	.4byte	0x15c
	.uleb128 0xc
	.byte	0x1
	.4byte	0x17f
	.uleb128 0xd
	.4byte	0xca
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x173
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9e
	.4byte	0xd5
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.byte	0x1f
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.byte	0x28
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF42
	.byte	0x1
	.byte	0x41
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x1fe
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x1
	.byte	0x41
	.4byte	0x1fe
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF27
	.byte	0x1
	.byte	0x43
	.4byte	0xca
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF28
	.byte	0x1
	.byte	0x45
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x53
	.uleb128 0x13
	.4byte	.LASF29
	.byte	0x5
	.2byte	0x16c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF30
	.byte	0x5
	.2byte	0x18f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF31
	.byte	0x5
	.2byte	0x190
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF32
	.byte	0x5
	.2byte	0x3df
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF33
	.byte	0x6
	.byte	0x30
	.4byte	0x24c
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0x95
	.uleb128 0x12
	.4byte	.LASF34
	.byte	0x6
	.byte	0x34
	.4byte	0x262
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0x95
	.uleb128 0x12
	.4byte	.LASF35
	.byte	0x6
	.byte	0x36
	.4byte	0x278
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0x95
	.uleb128 0x12
	.4byte	.LASF36
	.byte	0x6
	.byte	0x38
	.4byte	0x28e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0x95
	.uleb128 0x8
	.4byte	0x185
	.4byte	0x2a3
	.uleb128 0x9
	.4byte	0x87
	.byte	0x31
	.byte	0
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x4
	.byte	0xac
	.4byte	0x293
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x4
	.byte	0xae
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x7
	.byte	0x2f
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF29
	.byte	0x5
	.2byte	0x16c
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF30
	.byte	0x5
	.2byte	0x18f
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF31
	.byte	0x5
	.2byte	0x190
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF32
	.byte	0x5
	.2byte	0x3df
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x4
	.byte	0xac
	.4byte	0x293
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x4
	.byte	0xae
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x7
	.byte	0x2f
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI5
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF40:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF25:
	.ascii	"DEVICE_EXCHANGE_draw_dialog\000"
.LASF16:
	.ascii	"_02_menu\000"
.LASF42:
	.ascii	"FDTO_DEVICE_EXCHANGE_close_dialog\000"
.LASF4:
	.ascii	"short int\000"
.LASF30:
	.ascii	"GuiVar_DeviceExchangeProgressBar\000"
.LASF15:
	.ascii	"_01_command\000"
.LASF29:
	.ascii	"GuiVar_CommOptionDeviceExchangeResult\000"
.LASF12:
	.ascii	"keycode\000"
.LASF14:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF41:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_device_exchange.c\000"
.LASF32:
	.ascii	"GuiVar_SpinnerPos\000"
.LASF38:
	.ascii	"screen_history_index\000"
.LASF39:
	.ascii	"g_DIALOG_modal_result\000"
.LASF27:
	.ascii	"lktpqs\000"
.LASF9:
	.ascii	"long long int\000"
.LASF35:
	.ascii	"GuiFont_DecimalChar\000"
.LASF11:
	.ascii	"long int\000"
.LASF28:
	.ascii	"ldevice_exchange_result\000"
.LASF21:
	.ascii	"_06_u32_argument1\000"
.LASF19:
	.ascii	"key_process_func_ptr\000"
.LASF23:
	.ascii	"_08_screen_to_draw\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF2:
	.ascii	"signed char\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF34:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF37:
	.ascii	"ScreenHistory\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF0:
	.ascii	"char\000"
.LASF36:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF18:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF22:
	.ascii	"_07_u32_argument2\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF10:
	.ascii	"long unsigned int\000"
.LASF20:
	.ascii	"_04_func_ptr\000"
.LASF17:
	.ascii	"_03_structure_to_draw\000"
.LASF13:
	.ascii	"repeats\000"
.LASF33:
	.ascii	"GuiFont_LanguageActive\000"
.LASF43:
	.ascii	"pkeycode\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF24:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF31:
	.ascii	"GuiVar_DeviceExchangeSyncingRadios\000"
.LASF26:
	.ascii	"FDTO_DEVICE_EXCHANGE_update_dialog\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
