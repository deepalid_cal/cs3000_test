	.file	"port.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.text.pxPortInitialiseStack,"ax",%progbits
	.align	2
	.global	pxPortInitialiseStack
	.type	pxPortInitialiseStack, %function
pxPortInitialiseStack:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/port.c"
	.loc 1 102 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #16
.LCFI2:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 105 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-4]
	.loc 1 111 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 119 0
	ldr	r3, [fp, #-12]
	add	r2, r3, #4
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 120 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 122 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3
	str	r2, [r3, #0]
	.loc 1 123 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 124 0
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 125 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 126 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+4
	str	r2, [r3, #0]
	.loc 1 127 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 128 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+8
	str	r2, [r3, #0]
	.loc 1 129 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 130 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+12
	str	r2, [r3, #0]
	.loc 1 131 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 132 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+16
	str	r2, [r3, #0]
	.loc 1 133 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 134 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+20
	str	r2, [r3, #0]
	.loc 1 135 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 136 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+24
	str	r2, [r3, #0]
	.loc 1 137 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 138 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+28
	str	r2, [r3, #0]
	.loc 1 139 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 140 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+32
	str	r2, [r3, #0]
	.loc 1 141 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 142 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+36
	str	r2, [r3, #0]
	.loc 1 143 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 144 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+40
	str	r2, [r3, #0]
	.loc 1 145 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 146 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+44
	str	r2, [r3, #0]
	.loc 1 147 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 148 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L3+48
	str	r2, [r3, #0]
	.loc 1 149 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 153 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 154 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 157 0
	ldr	r3, [fp, #-8]
	mov	r2, #31
	str	r2, [r3, #0]
	.loc 1 159 0
	ldr	r3, [fp, #-12]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L2
	.loc 1 162 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	orr	r2, r3, #32
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
.L2:
	.loc 1 165 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 171 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 173 0
	ldr	r3, [fp, #-8]
	.loc 1 174 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L4:
	.align	2
.L3:
	.word	-1431655766
	.word	303174162
	.word	286331153
	.word	269488144
	.word	151587081
	.word	134744072
	.word	117901063
	.word	101058054
	.word	84215045
	.word	67372036
	.word	50529027
	.word	33686018
	.word	16843009
.LFE0:
	.size	pxPortInitialiseStack, .-pxPortInitialiseStack
	.section	.text.xPortStartScheduler,"ax",%progbits
	.align	2
	.global	xPortStartScheduler
	.type	xPortStartScheduler, %function
xPortStartScheduler:
.LFB1:
	.loc 1 178 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 183 0
	bl	prvSetupTimerInterrupt
	.loc 1 186 0
	bl	vPortISRStartFirstTask
	.loc 1 189 0
	mov	r3, #0
	.loc 1 190 0
	mov	r0, r3
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	xPortStartScheduler, .-xPortStartScheduler
	.section	.text.vPortEndScheduler,"ax",%progbits
	.align	2
	.global	vPortEndScheduler
	.type	vPortEndScheduler, %function
vPortEndScheduler:
.LFB2:
	.loc 1 194 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI5:
	add	fp, sp, #0
.LCFI6:
	.loc 1 197 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE2:
	.size	vPortEndScheduler, .-vPortEndScheduler
	.section	.text.prvSetupTimerInterrupt,"ax",%progbits
	.align	2
	.type	prvSetupTimerInterrupt, %function
prvSetupTimerInterrupt:
.LFB3:
	.loc 1 205 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI7:
	add	fp, sp, #4
.LCFI8:
	sub	sp, sp, #36
.LCFI9:
.LBB2:
	.loc 1 220 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #4
	mov	r1, #2
	mov	r2, #1
	ldr	r3, .L8
	bl	xSetISR_Vector
.LBE2:
	.loc 1 274 0
	ldr	r0, .L8+4
	mov	r1, #0
	bl	timer_open
	str	r0, [fp, #-8]
	.loc 1 278 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 279 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 280 0
	sub	r3, fp, #16
	ldr	r0, [fp, #-8]
	mov	r1, #2
	mov	r2, r3
	bl	timer_ioctl
	.loc 1 282 0
	mov	r3, #0
	str	r3, [fp, #-36]
	.loc 1 283 0
	mov	r3, #1
	str	r3, [fp, #-32]
	.loc 1 284 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 285 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 300 0
	ldr	r3, .L8+8
	str	r3, [fp, #-20]
	.loc 1 302 0
	sub	r3, fp, #36
	ldr	r0, [fp, #-8]
	mov	r1, #4
	mov	r2, r3
	bl	timer_ioctl
	.loc 1 306 0
	ldr	r0, [fp, #-8]
	mov	r1, #7
	mov	r2, #255
	bl	timer_ioctl
	.loc 1 312 0
	ldr	r0, [fp, #-8]
	mov	r1, #0
	mov	r2, #1
	bl	timer_ioctl
	.loc 1 316 0
	mov	r0, #4
	bl	xEnable_ISR
	.loc 1 317 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	vPreemptiveTick
	.word	1073938432
	.word	65000
.LFE3:
	.size	prvSetupTimerInterrupt, .-prvSetupTimerInterrupt
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/projdefs.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer_driver.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_intc_driver.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x3ed
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF57
	.byte	0x1
	.4byte	.LASF58
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x2
	.byte	0x47
	.4byte	0x45
	.uleb128 0x5
	.byte	0x4
	.4byte	0x4b
	.uleb128 0x6
	.byte	0x1
	.4byte	0x57
	.uleb128 0x7
	.4byte	0x57
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x5
	.byte	0x4
	.4byte	0x25
	.uleb128 0x9
	.4byte	0x6e
	.4byte	0x99
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.byte	0x6
	.byte	0x1d
	.4byte	0xc6
	.uleb128 0xc
	.4byte	.LASF8
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF9
	.sleb128 1
	.uleb128 0xc
	.4byte	.LASF10
	.sleb128 2
	.uleb128 0xc
	.4byte	.LASF11
	.sleb128 3
	.uleb128 0xc
	.4byte	.LASF12
	.sleb128 4
	.uleb128 0xc
	.4byte	.LASF13
	.sleb128 5
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF14
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x3
	.byte	0x5e
	.4byte	0xd8
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF17
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x3
	.byte	0x67
	.4byte	0x2c
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x3
	.byte	0x99
	.4byte	0xd8
	.uleb128 0xd
	.byte	0x74
	.byte	0x4
	.byte	0x28
	.4byte	0x1a0
	.uleb128 0xe
	.ascii	"ir\000"
	.byte	0x4
	.byte	0x2a
	.4byte	0x1a0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.ascii	"tcr\000"
	.byte	0x4
	.byte	0x2b
	.4byte	0x1a0
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.ascii	"tc\000"
	.byte	0x4
	.byte	0x2c
	.4byte	0x1a0
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.ascii	"pr\000"
	.byte	0x4
	.byte	0x2d
	.4byte	0x1a0
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.ascii	"pc\000"
	.byte	0x4
	.byte	0x2e
	.4byte	0x1a0
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.ascii	"mcr\000"
	.byte	0x4
	.byte	0x2f
	.4byte	0x1a0
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.ascii	"mr\000"
	.byte	0x4
	.byte	0x30
	.4byte	0x1b5
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.ascii	"ccr\000"
	.byte	0x4
	.byte	0x31
	.4byte	0x1a0
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xe
	.ascii	"cr\000"
	.byte	0x4
	.byte	0x32
	.4byte	0x1ba
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xe
	.ascii	"emr\000"
	.byte	0x4
	.byte	0x33
	.4byte	0x1a0
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xf
	.4byte	.LASF20
	.byte	0x4
	.byte	0x34
	.4byte	0x1cf
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xf
	.4byte	.LASF21
	.byte	0x4
	.byte	0x35
	.4byte	0x1a0
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.byte	0
	.uleb128 0x10
	.4byte	0xcd
	.uleb128 0x9
	.4byte	0xcd
	.4byte	0x1b5
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x10
	.4byte	0x1a5
	.uleb128 0x10
	.4byte	0x1a5
	.uleb128 0x9
	.4byte	0xcd
	.4byte	0x1cf
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x10
	.4byte	0x1bf
	.uleb128 0x4
	.4byte	.LASF22
	.byte	0x4
	.byte	0x36
	.4byte	0xf5
	.uleb128 0xd
	.byte	0x8
	.byte	0x5
	.byte	0x26
	.4byte	0x204
	.uleb128 0xf
	.4byte	.LASF23
	.byte	0x5
	.byte	0x2b
	.4byte	0xcd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x5
	.byte	0x2f
	.4byte	0xcd
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF25
	.byte	0x5
	.byte	0x30
	.4byte	0x1df
	.uleb128 0xd
	.byte	0x14
	.byte	0x5
	.byte	0x4b
	.4byte	0x25e
	.uleb128 0xf
	.4byte	.LASF26
	.byte	0x5
	.byte	0x4e
	.4byte	0xcd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF27
	.byte	0x5
	.byte	0x51
	.4byte	0xea
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x5
	.byte	0x55
	.4byte	0xea
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xf
	.4byte	.LASF29
	.byte	0x5
	.byte	0x58
	.4byte	0xea
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x5
	.byte	0x5b
	.4byte	0xcd
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x4
	.4byte	.LASF31
	.byte	0x5
	.byte	0x5c
	.4byte	0x20f
	.uleb128 0xb
	.byte	0x4
	.byte	0x5
	.byte	0x8e
	.4byte	0x2ae
	.uleb128 0xc
	.4byte	.LASF32
	.sleb128 0
	.uleb128 0xc
	.4byte	.LASF33
	.sleb128 1
	.uleb128 0xc
	.4byte	.LASF34
	.sleb128 2
	.uleb128 0xc
	.4byte	.LASF35
	.sleb128 3
	.uleb128 0xc
	.4byte	.LASF36
	.sleb128 4
	.uleb128 0xc
	.4byte	.LASF37
	.sleb128 5
	.uleb128 0xc
	.4byte	.LASF38
	.sleb128 6
	.uleb128 0xc
	.4byte	.LASF39
	.sleb128 7
	.uleb128 0xc
	.4byte	.LASF40
	.sleb128 8
	.uleb128 0xc
	.4byte	.LASF41
	.sleb128 9
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF45
	.byte	0x1
	.byte	0x65
	.byte	0x1
	.4byte	0x83
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x304
	.uleb128 0x12
	.4byte	.LASF42
	.byte	0x1
	.byte	0x65
	.4byte	0x83
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF43
	.byte	0x1
	.byte	0x65
	.4byte	0x3a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF44
	.byte	0x1
	.byte	0x65
	.4byte	0x57
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF47
	.byte	0x1
	.byte	0x67
	.4byte	0x83
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF46
	.byte	0x1
	.byte	0xb1
	.byte	0x1
	.4byte	0x67
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x32c
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF51
	.byte	0x1
	.byte	0xb3
	.byte	0x1
	.byte	0x1
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF59
	.byte	0x1
	.byte	0xc1
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x16
	.4byte	.LASF60
	.byte	0x1
	.byte	0xcc
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x398
	.uleb128 0x13
	.4byte	.LASF48
	.byte	0x1
	.byte	0xce
	.4byte	0xdf
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF49
	.byte	0x1
	.byte	0xcf
	.4byte	0x204
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF50
	.byte	0x1
	.byte	0xd0
	.4byte	0x25e
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x17
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF52
	.byte	0x1
	.byte	0xdb
	.byte	0x1
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x13
	.4byte	.LASF53
	.byte	0x7
	.byte	0x30
	.4byte	0x3a9
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x18
	.4byte	0x89
	.uleb128 0x13
	.4byte	.LASF54
	.byte	0x7
	.byte	0x34
	.4byte	0x3bf
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x18
	.4byte	0x89
	.uleb128 0x13
	.4byte	.LASF55
	.byte	0x7
	.byte	0x36
	.4byte	0x3d5
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x18
	.4byte	0x89
	.uleb128 0x13
	.4byte	.LASF56
	.byte	0x7
	.byte	0x38
	.4byte	0x3eb
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x18
	.4byte	0x89
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI8
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF55:
	.ascii	"GuiFont_DecimalChar\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF19:
	.ascii	"BOOL_32\000"
.LASF31:
	.ascii	"TMR_MATCH_SETUP_T\000"
.LASF35:
	.ascii	"TMR_SETUP_CLKIN\000"
.LASF16:
	.ascii	"UNS_32\000"
.LASF6:
	.ascii	"long long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF38:
	.ascii	"TMR_SETUP_MATCHOUT\000"
.LASF36:
	.ascii	"TMR_SETUP_MATCH\000"
.LASF4:
	.ascii	"long int\000"
.LASF10:
	.ascii	"ISR_TRIGGER_HIGH_LEVEL\000"
.LASF56:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF58:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/portable/GCC/ARM9_LPC32xx/port.c\000"
.LASF27:
	.ascii	"use_match_int\000"
.LASF17:
	.ascii	"unsigned int\000"
.LASF18:
	.ascii	"INT_32\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF42:
	.ascii	"pxTopOfStack\000"
.LASF44:
	.ascii	"pvParameters\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF57:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"ISR_TRIGGER_FIXED\000"
.LASF52:
	.ascii	"vPreemptiveTick\000"
.LASF39:
	.ascii	"TMR_CLEAR_INTS\000"
.LASF50:
	.ascii	"msetup\000"
.LASF26:
	.ascii	"timer_num\000"
.LASF23:
	.ascii	"ps_tick_val\000"
.LASF46:
	.ascii	"xPortStartScheduler\000"
.LASF34:
	.ascii	"TMR_SETUP_PSCALE\000"
.LASF12:
	.ascii	"ISR_TRIGGER_POSITIVE_EDGE\000"
.LASF22:
	.ascii	"TIMER_CNTR_REGS_T\000"
.LASF28:
	.ascii	"stop_on_match\000"
.LASF53:
	.ascii	"GuiFont_LanguageActive\000"
.LASF13:
	.ascii	"ISR_TRIGGER_DUAL_EDGE\000"
.LASF9:
	.ascii	"ISR_TRIGGER_LOW_LEVEL\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF60:
	.ascii	"prvSetupTimerInterrupt\000"
.LASF41:
	.ascii	"TMR_GET_STATUS\000"
.LASF3:
	.ascii	"short int\000"
.LASF20:
	.ascii	"rsvd2\000"
.LASF24:
	.ascii	"ps_us_val\000"
.LASF59:
	.ascii	"vPortEndScheduler\000"
.LASF48:
	.ascii	"ltimer\000"
.LASF33:
	.ascii	"TMR_RESET\000"
.LASF11:
	.ascii	"ISR_TRIGGER_NEGATIVE_EDGE\000"
.LASF29:
	.ascii	"reset_on_match\000"
.LASF54:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF14:
	.ascii	"char\000"
.LASF15:
	.ascii	"pdTASK_CODE\000"
.LASF47:
	.ascii	"pxOriginalTOS\000"
.LASF40:
	.ascii	"TMR_GET_COUNTS\000"
.LASF21:
	.ascii	"ctcr\000"
.LASF45:
	.ascii	"pxPortInitialiseStack\000"
.LASF43:
	.ascii	"pxCode\000"
.LASF32:
	.ascii	"TMR_ENABLE\000"
.LASF37:
	.ascii	"TMR_SETUP_CAPTURE\000"
.LASF30:
	.ascii	"match_tick_val\000"
.LASF49:
	.ascii	"pscale\000"
.LASF51:
	.ascii	"vPortISRStartFirstTask\000"
.LASF25:
	.ascii	"TMR_PSCALE_SETUP_T\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
