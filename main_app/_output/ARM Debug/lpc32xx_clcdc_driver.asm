	.file	"lpc32xx_clcdc_driver.c"
	.text
.Ltext0:
	.section	.bss.lcdcfg,"aw",%nobits
	.align	2
	.type	lcdcfg, %object
	.size	lcdcfg, 12
lcdcfg:
	.space	12
	.section	.rodata.lcd_vcmp_sel,"a",%progbits
	.align	2
	.type	lcd_vcmp_sel, %object
	.size	lcd_vcmp_sel, 16
lcd_vcmp_sel:
	.word	0
	.word	4096
	.word	8192
	.word	12288
	.global	__udivsi3
	.section	.text.lcd_update_clock,"ax",%progbits
	.align	2
	.type	lcd_update_clock, %function
lcd_update_clock:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clcdc_driver.c"
	.loc 1 83 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #24
.LCFI2:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	.loc 1 86 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-16]
	.loc 1 89 0
	mov	r0, #1
	bl	clkpwr_get_clock_rate
	str	r0, [fp, #-20]
	.loc 1 92 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 93 0
	b	.L2
.L4:
	.loc 1 95 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L2:
	.loc 1 93 0 discriminator 1
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-8]
	bl	__udivsi3
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bls	.L3
	.loc 1 93 0 is_stmt 0 discriminator 2
	ldr	r3, [fp, #-8]
	cmp	r3, #63
	bls	.L4
.L3:
	.loc 1 98 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #8]
	str	r3, [fp, #-12]
	.loc 1 99 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bhi	.L5
	.loc 1 102 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #67108864
	str	r3, [fp, #-12]
	b	.L6
.L5:
	.loc 1 107 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #2
	str	r3, [fp, #-8]
	.loc 1 108 0
	ldr	r3, [fp, #-12]
	bic	r3, r3, #-67108864
	bic	r3, r3, #29
	str	r3, [fp, #-12]
	.loc 1 110 0
	ldr	r3, [fp, #-8]
	and	r2, r3, #31
	.loc 1 111 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, lsr #5
	and	r3, r3, #31
	mov	r3, r3, asl #24
	orr	r3, r2, r3
	.loc 1 110 0
	ldr	r2, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
.L6:
	.loc 1 114 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #8]
	.loc 1 116 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE0:
	.size	lcd_update_clock, .-lcd_update_clock
	.section	.text.lcd_initialize,"ax",%progbits
	.align	2
	.type	lcd_initialize, %function
lcd_initialize:
.LFB1:
	.loc 1 141 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #20
.LCFI5:
	str	r0, [fp, #-24]
	.loc 1 143 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 144 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-16]
	.loc 1 145 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	str	r3, [fp, #-20]
	.loc 1 148 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #24]
	bic	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #24]
	.loc 1 151 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	mov	r3, r3, lsr #4
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	sub	r3, r3, #1
	mov	r3, r3, asl #2
	and	r2, r3, #255
	.loc 1 152 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	sub	r3, r3, #1
	mov	r3, r3, asl #8
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 151 0
	orr	r2, r2, r3
	.loc 1 153 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	sub	r3, r3, #1
	and	r3, r3, #255
	mov	r3, r3, asl #16
	.loc 1 152 0
	orr	r2, r2, r3
	.loc 1 154 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	sub	r3, r3, #1
	mov	r3, r3, asl #24
	.loc 1 151 0
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 155 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 158 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #28]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L8
	.loc 1 160 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #10]
	mov	r3, r3, lsr #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	str	r3, [fp, #-8]
	b	.L9
.L8:
	.loc 1 164 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #10]
	str	r3, [fp, #-8]
.L9:
	.loc 1 167 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	mov	r2, r3, asl #22
	mov	r2, r2, lsr #22
	.loc 1 168 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #8]	@ zero_extendqisi2
	sub	r3, r3, #1
	mov	r3, r3, asl #10
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 167 0
	orr	r2, r2, r3
	.loc 1 169 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #7]	@ zero_extendqisi2
	mov	r3, r3, asl #16
	.loc 1 168 0
	orr	r2, r2, r3
	.loc 1 170 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #6]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	.loc 1 167 0
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 171 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #4]
	.loc 1 174 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #16]	@ zero_extendqisi2
	sub	r3, r3, #1
	and	r3, r3, #31
	mov	r3, r3, asl #6
	str	r3, [fp, #-8]
	.loc 1 175 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #12]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L10
	.loc 1 177 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #16384
	str	r3, [fp, #-8]
.L10:
	.loc 1 179 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #13]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L11
	.loc 1 181 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #8192
	str	r3, [fp, #-8]
.L11:
	.loc 1 183 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #14]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L12
	.loc 1 185 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #4096
	str	r3, [fp, #-8]
.L12:
	.loc 1 187 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #15]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L13
	.loc 1 189 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #2048
	str	r3, [fp, #-8]
.L13:
	.loc 1 193 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #24]
	cmp	r3, #4
	beq	.L16
	cmp	r3, #5
	beq	.L17
	cmp	r3, #3
	bne	.L27
.L15:
	.loc 1 197 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	mov	r3, r3, lsr #2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	sub	r3, r3, #1
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	mov	r3, r3, asl #16
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 199 0
	b	.L18
.L16:
	.loc 1 203 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	mov	r3, r3, lsr #3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	sub	r3, r3, #1
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	mov	r3, r3, asl #16
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 205 0
	b	.L18
.L17:
	.loc 1 209 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r2, r3, #7
	cmp	r3, #0
	movlt	r3, r2
	mov	r3, r3, asr #3
	sub	r3, r3, #1
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	mov	r3, r3, asl #16
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 211 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #134217728
	str	r3, [fp, #-8]
	.loc 1 212 0
	b	.L18
.L27:
	.loc 1 218 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	sub	r3, r3, #1
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	mov	r3, r3, asl #16
	.loc 1 217 0
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 219 0
	mov	r0, r0	@ nop
.L18:
	.loc 1 222 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #8]
	.loc 1 225 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #20]
	ldr	r0, [fp, #-24]
	mov	r1, r3
	bl	lcd_update_clock
	.loc 1 228 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 231 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 235 0
	mov	r3, #268
	str	r3, [fp, #-8]
	.loc 1 237 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #24]
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L19
.L24:
	.word	.L20
	.word	.L19
	.word	.L19
	.word	.L21
	.word	.L22
	.word	.L28
.L20:
	.loc 1 240 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #32
	str	r3, [fp, #-8]
	.loc 1 241 0
	b	.L25
.L21:
	.loc 1 244 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #16
	str	r3, [fp, #-8]
	.loc 1 245 0
	b	.L25
.L22:
	.loc 1 248 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #80
	str	r3, [fp, #-8]
	.loc 1 249 0
	b	.L25
.L19:
	.loc 1 257 0
	mvn	r3, #0
	str	r3, [fp, #-12]
	.loc 1 258 0
	b	.L25
.L28:
	.loc 1 253 0
	mov	r0, r0	@ nop
.L25:
	.loc 1 262 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #28]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L26
	.loc 1 264 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #128
	str	r3, [fp, #-8]
.L26:
	.loc 1 266 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #24]
	.loc 1 269 0
	ldr	r3, [fp, #-12]
	.loc 1 270 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	lcd_initialize, .-lcd_initialize
	.section	.text.lcd_open,"ax",%progbits
	.align	2
	.global	lcd_open
	.type	lcd_open, %function
lcd_open:
.LFB2:
	.loc 1 304 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #12
.LCFI8:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 305 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 307 0
	ldr	r3, .L32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L30
	.loc 1 307 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r3, .L32+4
	cmp	r2, r3
	bne	.L30
	.loc 1 310 0 is_stmt 1
	ldr	r3, .L32
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 313 0
	ldr	r3, .L32
	ldr	r2, [fp, #-12]
	str	r2, [r3, #4]
	.loc 1 314 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L32
	str	r2, [r3, #8]
	.loc 1 317 0
	ldr	r3, .L32
	ldr	r3, [r3, #4]
	ldr	r2, .L32
	ldr	r2, [r2, #4]
	ldr	r2, [r2, #24]
	bic	r2, r2, #1
	str	r2, [r3, #24]
	.loc 1 322 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L31
	.loc 1 324 0
	ldr	r0, .L32
	bl	lcd_initialize
.L31:
	.loc 1 328 0
	ldr	r3, .L32
	str	r3, [fp, #-8]
.L30:
	.loc 1 331 0
	ldr	r3, [fp, #-8]
	.loc 1 332 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L33:
	.align	2
.L32:
	.word	lcdcfg
	.word	822345728
.LFE2:
	.size	lcd_open, .-lcd_open
	.section	.text.lcd_close,"ax",%progbits
	.align	2
	.global	lcd_close
	.type	lcd_close, %function
lcd_close:
.LFB3:
	.loc 1 356 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #12
.LCFI11:
	str	r0, [fp, #-12]
	.loc 1 357 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 358 0
	mvn	r3, #0
	str	r3, [fp, #-4]
	.loc 1 360 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L35
	.loc 1 363 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 364 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 367 0
	ldr	r3, .L36
	ldr	r3, [r3, #4]
	ldr	r2, .L36
	ldr	r2, [r2, #4]
	ldr	r2, [r2, #24]
	bic	r2, r2, #1
	str	r2, [r3, #24]
.L35:
	.loc 1 370 0
	ldr	r3, [fp, #-4]
	.loc 1 371 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L37:
	.align	2
.L36:
	.word	lcdcfg
.LFE3:
	.size	lcd_close, .-lcd_close
	.section	.text.lcd_ioctl,"ax",%progbits
	.align	2
	.global	lcd_ioctl
	.type	lcd_ioctl, %function
lcd_ioctl:
.LFB4:
	.loc 1 399 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #40
.LCFI14:
	str	r0, [fp, #-36]
	str	r1, [fp, #-40]
	str	r2, [fp, #-44]
	.loc 1 403 0
	ldr	r3, [fp, #-36]
	str	r3, [fp, #-24]
	.loc 1 404 0
	mvn	r3, #0
	str	r3, [fp, #-20]
	.loc 1 406 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L39
	.loc 1 408 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 409 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-28]
	.loc 1 411 0
	ldr	r3, [fp, #-40]
	cmp	r3, #21
	ldrls	pc, [pc, r3, asl #2]
	b	.L40
.L62:
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L40
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
	.word	.L50
	.word	.L51
	.word	.L52
	.word	.L53
	.word	.L54
	.word	.L55
	.word	.L56
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
.L41:
	.loc 1 416 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L63
	.loc 1 418 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-24]
	str	r2, [r3, #8]
	.loc 1 419 0
	ldr	r0, [fp, #-24]
	bl	lcd_initialize
	.loc 1 426 0
	b	.L39
.L63:
	.loc 1 424 0
	mvn	r3, #0
	str	r3, [fp, #-20]
	.loc 1 426 0
	b	.L39
.L42:
	.loc 1 430 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L65
	.loc 1 433 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	orr	r2, r3, #1
	ldr	r3, [fp, #-28]
	str	r2, [r3, #24]
	.loc 1 440 0
	b	.L39
.L65:
	.loc 1 438 0
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #24]
	mov	r3, r2
	mov	r3, r3, asl #31
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	mov	r2, r3
	ldr	r3, [fp, #-28]
	str	r2, [r3, #24]
	.loc 1 440 0
	b	.L39
.L43:
	.loc 1 444 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L67
	.loc 1 447 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	orr	r3, r3, #2048
	orr	r3, r3, #1
	ldr	r2, [fp, #-28]
	str	r3, [r2, #24]
	.loc 1 454 0
	b	.L39
.L67:
	.loc 1 452 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	bic	r3, r3, #2048
	bic	r3, r3, #1
	ldr	r2, [fp, #-28]
	str	r3, [r2, #24]
	.loc 1 454 0
	b	.L39
.L44:
	.loc 1 458 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-28]
	str	r2, [r3, #16]
	.loc 1 459 0
	b	.L39
.L45:
	.loc 1 463 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-28]
	str	r2, [r3, #20]
	.loc 1 464 0
	b	.L39
.L46:
	.loc 1 468 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #28]
	str	r3, [fp, #-12]
	.loc 1 469 0
	ldr	r3, [fp, #-44]
	and	r3, r3, #30
	ldr	r2, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 473 0
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #28]
	.loc 1 474 0
	b	.L39
.L47:
	.loc 1 478 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #28]
	str	r3, [fp, #-12]
	.loc 1 479 0
	ldr	r3, [fp, #-44]
	and	r3, r3, #30
	mvn	r3, r3
	ldr	r2, [fp, #-12]
	and	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 483 0
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #28]
	.loc 1 484 0
	b	.L39
.L48:
	.loc 1 489 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	blt	.L69
	.loc 1 489 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-44]
	cmp	r3, #3
	bgt	.L69
	.loc 1 493 0 is_stmt 1
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #28]
	str	r3, [fp, #-12]
	.loc 1 494 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #28]
	bic	r2, r3, #8
	ldr	r3, [fp, #-28]
	str	r2, [r3, #28]
	.loc 1 497 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	bic	r3, r3, #12288
	str	r3, [fp, #-32]
	.loc 1 500 0
	ldr	r3, .L114
	ldr	r2, [fp, #-44]
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, [fp, #-32]
	orr	r3, r2, r3
	str	r3, [fp, #-32]
	.loc 1 501 0
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-32]
	str	r2, [r3, #24]
	.loc 1 505 0
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #28]
	.loc 1 512 0
	b	.L39
.L69:
	.loc 1 510 0
	mvn	r3, #0
	str	r3, [fp, #-20]
	.loc 1 512 0
	b	.L39
.L49:
	.loc 1 516 0
	ldr	r3, [fp, #-28]
	mov	r2, #31
	str	r2, [r3, #40]
	.loc 1 517 0
	ldr	r3, [fp, #-28]
	mov	r2, #1
	str	r2, [r3, #3108]
	.loc 1 518 0
	b	.L39
.L50:
	.loc 1 523 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L71
	.loc 1 526 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	bic	r2, r3, #65536
	ldr	r3, [fp, #-28]
	str	r2, [r3, #24]
	.loc 1 533 0
	b	.L39
.L71:
	.loc 1 531 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	orr	r2, r3, #65536
	ldr	r3, [fp, #-28]
	str	r2, [r3, #24]
	.loc 1 533 0
	b	.L39
.L51:
	.loc 1 538 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L73
	.loc 1 541 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	bic	r2, r3, #256
	ldr	r3, [fp, #-28]
	str	r2, [r3, #24]
	.loc 1 548 0
	b	.L39
.L73:
	.loc 1 546 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	orr	r2, r3, #256
	ldr	r3, [fp, #-28]
	str	r2, [r3, #24]
	.loc 1 548 0
	b	.L39
.L52:
	.loc 1 552 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	bic	r3, r3, #14
	str	r3, [fp, #-12]
	.loc 1 553 0
	ldr	r3, [fp, #-44]
	sub	r3, r3, #1
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L75
.L80:
	.word	.L111
	.word	.L77
	.word	.L75
	.word	.L78
	.word	.L75
	.word	.L75
	.word	.L75
	.word	.L79
.L77:
	.loc 1 560 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #2
	str	r3, [fp, #-12]
	.loc 1 561 0
	b	.L81
.L78:
	.loc 1 564 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #4
	str	r3, [fp, #-12]
	.loc 1 565 0
	b	.L81
.L79:
	.loc 1 568 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #6
	str	r3, [fp, #-12]
	.loc 1 569 0
	b	.L81
.L75:
	.loc 1 573 0
	ldr	r3, [fp, #-12]
	orr	r3, r3, #12
	str	r3, [fp, #-12]
	.loc 1 574 0
	b	.L81
.L111:
	.loc 1 557 0
	mov	r0, r0	@ nop
.L81:
	.loc 1 576 0
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #24]
	.loc 1 577 0
	b	.L39
.L53:
	.loc 1 581 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	ble	.L112
	.loc 1 583 0
	ldr	r3, [fp, #-44]
	ldr	r0, [fp, #-24]
	mov	r1, r3
	bl	lcd_update_clock
	.loc 1 585 0
	b	.L112
.L54:
	.loc 1 589 0
	ldr	r3, [fp, #-44]
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L83
.L93:
	.word	.L84
	.word	.L85
	.word	.L86
	.word	.L87
	.word	.L83
	.word	.L88
	.word	.L113
	.word	.L90
	.word	.L91
	.word	.L92
.L84:
	.loc 1 593 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	.loc 1 594 0
	and	r3, r3, #1
	and	r3, r3, #255
	.loc 1 593 0
	cmp	r3, #0
	beq	.L94
	.loc 1 596 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 602 0
	b	.L96
.L94:
	.loc 1 600 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 602 0
	b	.L96
.L85:
	.loc 1 606 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	and	r3, r3, #2048
	cmp	r3, #0
	beq	.L97
	.loc 1 608 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 614 0
	b	.L96
.L97:
	.loc 1 612 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 614 0
	b	.L96
.L86:
	.loc 1 618 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #16]
	str	r3, [fp, #-20]
	.loc 1 619 0
	b	.L96
.L87:
	.loc 1 623 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #20]
	str	r3, [fp, #-20]
	.loc 1 624 0
	b	.L96
.L88:
	.loc 1 629 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #24]
	.loc 1 628 0
	str	r3, [fp, #-20]
	.loc 1 630 0
	b	.L96
.L90:
	.loc 1 641 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	ldrh	r3, [r3, #4]
	.loc 1 640 0
	str	r3, [fp, #-20]
	.loc 1 642 0
	b	.L96
.L91:
	.loc 1 648 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	ldrh	r3, [r3, #10]
	.loc 1 647 0
	str	r3, [fp, #-20]
	.loc 1 649 0
	b	.L96
.L92:
	.loc 1 653 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #24]
	and	r3, r3, #14
	cmp	r3, #12
	ldrls	pc, [pc, r3, asl #2]
	b	.L99
.L105:
	.word	.L100
	.word	.L99
	.word	.L101
	.word	.L99
	.word	.L102
	.word	.L99
	.word	.L103
	.word	.L99
	.word	.L99
	.word	.L99
	.word	.L99
	.word	.L99
	.word	.L104
.L100:
	.loc 1 657 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 658 0
	b	.L106
.L101:
	.loc 1 661 0
	mov	r3, #2
	str	r3, [fp, #-20]
	.loc 1 662 0
	b	.L106
.L102:
	.loc 1 665 0
	mov	r3, #4
	str	r3, [fp, #-20]
	.loc 1 666 0
	b	.L106
.L103:
	.loc 1 669 0
	mov	r3, #8
	str	r3, [fp, #-20]
	.loc 1 670 0
	b	.L106
.L104:
	.loc 1 673 0
	mov	r3, #16
	str	r3, [fp, #-20]
	.loc 1 674 0
	b	.L106
.L99:
	.loc 1 677 0
	mvn	r3, #6
	str	r3, [fp, #-20]
	.loc 1 679 0
	b	.L96
.L106:
	b	.L96
.L83:
	.loc 1 683 0
	mvn	r3, #6
	str	r3, [fp, #-20]
	.loc 1 685 0
	b	.L39
.L113:
	.loc 1 635 0
	mov	r0, r0	@ nop
.L96:
	.loc 1 685 0
	b	.L39
.L55:
	.loc 1 689 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-28]
	str	r2, [r3, #3072]
	.loc 1 690 0
	b	.L39
.L56:
	.loc 1 694 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-28]
	str	r2, [r3, #3088]
	.loc 1 695 0
	b	.L39
.L57:
	.loc 1 699 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-28]
	str	r2, [r3, #3080]
	.loc 1 700 0
	b	.L39
.L58:
	.loc 1 704 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-28]
	str	r2, [r3, #3084]
	.loc 1 705 0
	b	.L39
.L59:
	.loc 1 709 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-28]
	str	r2, [r3, #3104]
	.loc 1 710 0
	b	.L39
.L60:
	.loc 1 713 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-8]
	.loc 1 715 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L107
.L108:
	.loc 1 716 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	mov	r1, r3
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-16]
	add	r2, r2, #512
	str	r1, [r3, r2, asl #2]
	.loc 1 717 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 715 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L107:
	.loc 1 715 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #255
	bls	.L108
	.loc 1 719 0 is_stmt 1
	b	.L39
.L61:
	.loc 1 722 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-8]
	.loc 1 724 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L109
.L110:
	.loc 1 725 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	mov	r1, r3
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-16]
	add	r2, r2, #128
	str	r1, [r3, r2, asl #2]
	.loc 1 726 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 724 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L109:
	.loc 1 724 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #127
	bls	.L110
	.loc 1 728 0 is_stmt 1
	b	.L39
.L40:
	.loc 1 732 0
	mvn	r3, #6
	str	r3, [fp, #-20]
	b	.L39
.L112:
	.loc 1 585 0
	mov	r0, r0	@ nop
.L39:
	.loc 1 736 0
	ldr	r3, [fp, #-20]
	.loc 1 737 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L115:
	.align	2
.L114:
	.word	lcd_vcmp_sel
.LFE4:
	.size	lcd_ioctl, .-lcd_ioctl
	.section	.text.lcd_read,"ax",%progbits
	.align	2
	.global	lcd_read
	.type	lcd_read, %function
lcd_read:
.LFB5:
	.loc 1 763 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI15:
	add	fp, sp, #0
.LCFI16:
	sub	sp, sp, #12
.LCFI17:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	str	r2, [fp, #-12]
	.loc 1 765 0
	mov	r3, #0
	.loc 1 766 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE5:
	.size	lcd_read, .-lcd_read
	.section	.text.lcd_write,"ax",%progbits
	.align	2
	.global	lcd_write
	.type	lcd_write, %function
lcd_write:
.LFB6:
	.loc 1 792 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI18:
	add	fp, sp, #0
.LCFI19:
	sub	sp, sp, #12
.LCFI20:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	str	r2, [fp, #-12]
	.loc 1 793 0
	mov	r3, #0
	.loc 1 794 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE6:
	.size	lcd_write, .-lcd_write
	.section	.text.calsense_lcd_initialize,"ax",%progbits
	.align	2
	.type	calsense_lcd_initialize, %function
calsense_lcd_initialize:
.LFB7:
	.loc 1 799 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #20
.LCFI23:
	str	r0, [fp, #-24]
	.loc 1 801 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 802 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-16]
	.loc 1 803 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	str	r3, [fp, #-20]
	.loc 1 806 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #24]
	.loc 1 809 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	mov	r3, r3, lsr #4
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	sub	r3, r3, #1
	mov	r3, r3, asl #2
	and	r2, r3, #255
	.loc 1 810 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	sub	r3, r3, #1
	mov	r3, r3, asl #8
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 809 0
	orr	r2, r2, r3
	.loc 1 811 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	sub	r3, r3, #1
	and	r3, r3, #255
	mov	r3, r3, asl #16
	.loc 1 810 0
	orr	r2, r2, r3
	.loc 1 812 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	sub	r3, r3, #1
	mov	r3, r3, asl #24
	.loc 1 809 0
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 813 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 816 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #28]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L119
	.loc 1 818 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #10]
	mov	r3, r3, lsr #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	str	r3, [fp, #-8]
	b	.L120
.L119:
	.loc 1 822 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #10]
	str	r3, [fp, #-8]
.L120:
	.loc 1 825 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	mov	r2, r3, asl #22
	mov	r2, r2, lsr #22
	.loc 1 826 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #8]	@ zero_extendqisi2
	sub	r3, r3, #1
	mov	r3, r3, asl #10
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 825 0
	orr	r2, r2, r3
	.loc 1 827 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #7]	@ zero_extendqisi2
	mov	r3, r3, asl #16
	.loc 1 826 0
	orr	r2, r2, r3
	.loc 1 828 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #6]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	.loc 1 825 0
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 829 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #4]
	.loc 1 832 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #16]	@ zero_extendqisi2
	sub	r3, r3, #1
	and	r3, r3, #31
	mov	r3, r3, asl #6
	str	r3, [fp, #-8]
	.loc 1 833 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #12]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L121
	.loc 1 835 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #16384
	str	r3, [fp, #-8]
.L121:
	.loc 1 837 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #13]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L122
	.loc 1 839 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #8192
	str	r3, [fp, #-8]
.L122:
	.loc 1 841 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #14]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L123
	.loc 1 843 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #4096
	str	r3, [fp, #-8]
.L123:
	.loc 1 845 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #15]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L124
	.loc 1 847 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #2048
	str	r3, [fp, #-8]
.L124:
	.loc 1 851 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #24]
	cmp	r3, #4
	beq	.L127
	cmp	r3, #5
	beq	.L128
	cmp	r3, #3
	bne	.L130
.L126:
	.loc 1 855 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	mov	r3, r3, lsr #2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	sub	r3, r3, #1
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	mov	r3, r3, asl #16
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 856 0
	b	.L129
.L127:
	.loc 1 860 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	mov	r3, r3, lsr #3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	sub	r3, r3, #1
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	mov	r3, r3, asl #16
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 861 0
	b	.L129
.L128:
	.loc 1 865 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	mov	r2, r3
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	add	r2, r3, #7
	cmp	r3, #0
	movlt	r3, r2
	mov	r3, r3, asr #3
	sub	r3, r3, #1
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	mov	r3, r3, asl #16
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 866 0
	ldr	r3, [fp, #-8]
	orr	r3, r3, #134217728
	str	r3, [fp, #-8]
	.loc 1 867 0
	b	.L129
.L130:
	.loc 1 872 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #4]
	sub	r3, r3, #1
	mov	r3, r3, asl #22
	mov	r3, r3, lsr #22
	mov	r3, r3, asl #16
	ldr	r2, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 873 0
	mov	r0, r0	@ nop
.L129:
	.loc 1 875 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #8]
	.loc 1 878 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #20]
	ldr	r0, [fp, #-24]
	mov	r1, r3
	bl	lcd_update_clock
	.loc 1 881 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 884 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #28]
	.loc 1 887 0
	ldr	r3, [fp, #-16]
	mov	r2, #20
	str	r2, [r3, #24]
	.loc 1 889 0
	ldr	r3, [fp, #-12]
	.loc 1 890 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE7:
	.size	calsense_lcd_initialize, .-calsense_lcd_initialize
	.section	.text.calsense_lcd_open,"ax",%progbits
	.align	2
	.global	calsense_lcd_open
	.type	calsense_lcd_open, %function
calsense_lcd_open:
.LFB8:
	.loc 1 894 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #12
.LCFI26:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 895 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 897 0
	ldr	r3, .L134
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L132
	.loc 1 897 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r3, .L134+4
	cmp	r2, r3
	bne	.L132
	.loc 1 900 0 is_stmt 1
	ldr	r3, .L134
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 903 0
	ldr	r3, .L134
	ldr	r2, [fp, #-12]
	str	r2, [r3, #4]
	.loc 1 904 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L134
	str	r2, [r3, #8]
	.loc 1 907 0
	ldr	r3, .L134
	ldr	r3, [r3, #4]
	ldr	r2, .L134
	ldr	r2, [r2, #4]
	ldr	r2, [r2, #24]
	bic	r2, r2, #1
	str	r2, [r3, #24]
	.loc 1 912 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L133
	.loc 1 914 0
	ldr	r0, .L134
	bl	calsense_lcd_initialize
.L133:
	.loc 1 918 0
	ldr	r3, .L134
	str	r3, [fp, #-8]
.L132:
	.loc 1 921 0
	ldr	r3, [fp, #-8]
	.loc 1 922 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L135:
	.align	2
.L134:
	.word	lcdcfg
	.word	822345728
.LFE8:
	.size	calsense_lcd_open, .-calsense_lcd_open
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/lcd/lpc_lcd_params.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clcdc.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clcdc_driver.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa1a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF174
	.byte	0x1
	.4byte	.LASF175
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x67
	.4byte	0x7b
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x69
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0xfd
	.4byte	0x70
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x2f
	.4byte	0xd3
	.uleb128 0x6
	.ascii	"TFT\000"
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF14
	.sleb128 1
	.uleb128 0x7
	.4byte	.LASF15
	.sleb128 2
	.uleb128 0x7
	.4byte	.LASF16
	.sleb128 3
	.uleb128 0x7
	.4byte	.LASF17
	.sleb128 4
	.uleb128 0x7
	.4byte	.LASF18
	.sleb128 5
	.byte	0
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x3
	.byte	0x36
	.4byte	0xa6
	.uleb128 0x8
	.byte	0x28
	.byte	0x3
	.byte	0x39
	.4byte	0x237
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x3
	.byte	0x3b
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x3
	.byte	0x3d
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x3
	.byte	0x3f
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x3
	.byte	0x41
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x3
	.byte	0x43
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x3
	.byte	0x45
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x7
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x3
	.byte	0x47
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x3
	.byte	0x49
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x3
	.byte	0x4b
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x3
	.byte	0x4d
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x3
	.byte	0x4f
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x3
	.byte	0x50
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0xf
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x3
	.byte	0x51
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x9
	.4byte	.LASF33
	.byte	0x3
	.byte	0x53
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.uleb128 0x9
	.4byte	.LASF34
	.byte	0x3
	.byte	0x55
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x9
	.4byte	.LASF35
	.byte	0x3
	.byte	0x56
	.4byte	0xd3
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x3
	.byte	0x57
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x9
	.4byte	.LASF37
	.byte	0x3
	.byte	0x5c
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1d
	.uleb128 0x9
	.4byte	.LASF38
	.byte	0x3
	.byte	0x5e
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1e
	.uleb128 0x9
	.4byte	.LASF39
	.byte	0x3
	.byte	0x60
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1f
	.uleb128 0x9
	.4byte	.LASF40
	.byte	0x3
	.byte	0x62
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x9
	.4byte	.LASF41
	.byte	0x3
	.byte	0x64
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x21
	.uleb128 0x9
	.4byte	.LASF42
	.byte	0x3
	.byte	0x65
	.4byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x22
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x3
	.byte	0x68
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x3
	.4byte	.LASF44
	.byte	0x3
	.byte	0x69
	.4byte	0xde
	.uleb128 0xa
	.2byte	0xc30
	.byte	0x4
	.byte	0x23
	.4byte	0x3e2
	.uleb128 0x9
	.4byte	.LASF45
	.byte	0x4
	.byte	0x25
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF46
	.byte	0x4
	.byte	0x26
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF47
	.byte	0x4
	.byte	0x27
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x9
	.4byte	.LASF48
	.byte	0x4
	.byte	0x28
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x9
	.4byte	.LASF49
	.byte	0x4
	.byte	0x29
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x9
	.4byte	.LASF50
	.byte	0x4
	.byte	0x2b
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x9
	.4byte	.LASF51
	.byte	0x4
	.byte	0x2e
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x9
	.4byte	.LASF52
	.byte	0x4
	.byte	0x30
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x9
	.4byte	.LASF53
	.byte	0x4
	.byte	0x31
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x9
	.4byte	.LASF54
	.byte	0x4
	.byte	0x32
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x9
	.4byte	.LASF55
	.byte	0x4
	.byte	0x33
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x9
	.4byte	.LASF56
	.byte	0x4
	.byte	0x35
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x9
	.4byte	.LASF57
	.byte	0x4
	.byte	0x37
	.4byte	0x3e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x9
	.4byte	.LASF58
	.byte	0x4
	.byte	0x3a
	.4byte	0x3fe
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x9
	.4byte	.LASF59
	.byte	0x4
	.byte	0x3b
	.4byte	0x413
	.byte	0x3
	.byte	0x23
	.uleb128 0x200
	.uleb128 0x9
	.4byte	.LASF60
	.byte	0x4
	.byte	0x3c
	.4byte	0x428
	.byte	0x3
	.byte	0x23
	.uleb128 0x400
	.uleb128 0x9
	.4byte	.LASF61
	.byte	0x4
	.byte	0x3d
	.4byte	0x42d
	.byte	0x3
	.byte	0x23
	.uleb128 0x800
	.uleb128 0x9
	.4byte	.LASF62
	.byte	0x4
	.byte	0x3e
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc00
	.uleb128 0x9
	.4byte	.LASF63
	.byte	0x4
	.byte	0x40
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc04
	.uleb128 0x9
	.4byte	.LASF64
	.byte	0x4
	.byte	0x43
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc08
	.uleb128 0x9
	.4byte	.LASF65
	.byte	0x4
	.byte	0x45
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0c
	.uleb128 0x9
	.4byte	.LASF66
	.byte	0x4
	.byte	0x47
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc10
	.uleb128 0x9
	.4byte	.LASF67
	.byte	0x4
	.byte	0x48
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc14
	.uleb128 0x9
	.4byte	.LASF68
	.byte	0x4
	.byte	0x4a
	.4byte	0x442
	.byte	0x3
	.byte	0x23
	.uleb128 0xc18
	.uleb128 0x9
	.4byte	.LASF69
	.byte	0x4
	.byte	0x4c
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc20
	.uleb128 0x9
	.4byte	.LASF70
	.byte	0x4
	.byte	0x4e
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc24
	.uleb128 0x9
	.4byte	.LASF71
	.byte	0x4
	.byte	0x50
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc28
	.uleb128 0x9
	.4byte	.LASF72
	.byte	0x4
	.byte	0x52
	.4byte	0x3e2
	.byte	0x3
	.byte	0x23
	.uleb128 0xc2c
	.byte	0
	.uleb128 0xb
	.4byte	0x5e
	.uleb128 0xc
	.4byte	0x5e
	.4byte	0x3f7
	.uleb128 0xd
	.4byte	0x3f7
	.byte	0x72
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF73
	.uleb128 0xb
	.4byte	0x3e7
	.uleb128 0xc
	.4byte	0x5e
	.4byte	0x413
	.uleb128 0xd
	.4byte	0x3f7
	.byte	0x7f
	.byte	0
	.uleb128 0xb
	.4byte	0x403
	.uleb128 0xc
	.4byte	0x5e
	.4byte	0x428
	.uleb128 0xd
	.4byte	0x3f7
	.byte	0xff
	.byte	0
	.uleb128 0xb
	.4byte	0x418
	.uleb128 0xb
	.4byte	0x418
	.uleb128 0xc
	.4byte	0x5e
	.4byte	0x442
	.uleb128 0xd
	.4byte	0x3f7
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.4byte	0x432
	.uleb128 0x3
	.4byte	.LASF74
	.byte	0x4
	.byte	0x54
	.4byte	0x242
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.byte	0x28
	.4byte	0x4df
	.uleb128 0x7
	.4byte	.LASF75
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF76
	.sleb128 1
	.uleb128 0x7
	.4byte	.LASF77
	.sleb128 2
	.uleb128 0x7
	.4byte	.LASF78
	.sleb128 3
	.uleb128 0x7
	.4byte	.LASF79
	.sleb128 4
	.uleb128 0x7
	.4byte	.LASF80
	.sleb128 5
	.uleb128 0x7
	.4byte	.LASF81
	.sleb128 6
	.uleb128 0x7
	.4byte	.LASF82
	.sleb128 7
	.uleb128 0x7
	.4byte	.LASF83
	.sleb128 8
	.uleb128 0x7
	.4byte	.LASF84
	.sleb128 9
	.uleb128 0x7
	.4byte	.LASF85
	.sleb128 10
	.uleb128 0x7
	.4byte	.LASF86
	.sleb128 11
	.uleb128 0x7
	.4byte	.LASF87
	.sleb128 12
	.uleb128 0x7
	.4byte	.LASF88
	.sleb128 13
	.uleb128 0x7
	.4byte	.LASF89
	.sleb128 14
	.uleb128 0x7
	.4byte	.LASF90
	.sleb128 15
	.uleb128 0x7
	.4byte	.LASF91
	.sleb128 16
	.uleb128 0x7
	.4byte	.LASF92
	.sleb128 17
	.uleb128 0x7
	.4byte	.LASF93
	.sleb128 18
	.uleb128 0x7
	.4byte	.LASF94
	.sleb128 19
	.uleb128 0x7
	.4byte	.LASF95
	.sleb128 20
	.uleb128 0x7
	.4byte	.LASF96
	.sleb128 21
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.byte	0x6d
	.4byte	0x500
	.uleb128 0x7
	.4byte	.LASF97
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF98
	.sleb128 1
	.uleb128 0x7
	.4byte	.LASF99
	.sleb128 2
	.uleb128 0x7
	.4byte	.LASF100
	.sleb128 3
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.byte	0x77
	.4byte	0x545
	.uleb128 0x7
	.4byte	.LASF101
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF102
	.sleb128 1
	.uleb128 0x7
	.4byte	.LASF103
	.sleb128 2
	.uleb128 0x7
	.4byte	.LASF104
	.sleb128 3
	.uleb128 0x7
	.4byte	.LASF105
	.sleb128 4
	.uleb128 0x7
	.4byte	.LASF106
	.sleb128 5
	.uleb128 0x7
	.4byte	.LASF107
	.sleb128 6
	.uleb128 0x7
	.4byte	.LASF108
	.sleb128 7
	.uleb128 0x7
	.4byte	.LASF109
	.sleb128 8
	.uleb128 0x7
	.4byte	.LASF110
	.sleb128 9
	.byte	0
	.uleb128 0xc
	.4byte	0x5e
	.4byte	0x555
	.uleb128 0xd
	.4byte	0x3f7
	.byte	0x3
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.byte	0x6
	.byte	0x24
	.4byte	0x636
	.uleb128 0x7
	.4byte	.LASF111
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF112
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF113
	.sleb128 1
	.uleb128 0x7
	.4byte	.LASF114
	.sleb128 2
	.uleb128 0x7
	.4byte	.LASF115
	.sleb128 3
	.uleb128 0x7
	.4byte	.LASF116
	.sleb128 4
	.uleb128 0x7
	.4byte	.LASF117
	.sleb128 5
	.uleb128 0x7
	.4byte	.LASF118
	.sleb128 6
	.uleb128 0x7
	.4byte	.LASF119
	.sleb128 7
	.uleb128 0x7
	.4byte	.LASF120
	.sleb128 8
	.uleb128 0x7
	.4byte	.LASF121
	.sleb128 9
	.uleb128 0x7
	.4byte	.LASF122
	.sleb128 10
	.uleb128 0x7
	.4byte	.LASF123
	.sleb128 11
	.uleb128 0x7
	.4byte	.LASF124
	.sleb128 12
	.uleb128 0x7
	.4byte	.LASF125
	.sleb128 13
	.uleb128 0x7
	.4byte	.LASF126
	.sleb128 14
	.uleb128 0x7
	.4byte	.LASF127
	.sleb128 15
	.uleb128 0x7
	.4byte	.LASF128
	.sleb128 16
	.uleb128 0x7
	.4byte	.LASF129
	.sleb128 17
	.uleb128 0x7
	.4byte	.LASF130
	.sleb128 18
	.uleb128 0x7
	.4byte	.LASF131
	.sleb128 19
	.uleb128 0x7
	.4byte	.LASF132
	.sleb128 20
	.uleb128 0x7
	.4byte	.LASF133
	.sleb128 21
	.uleb128 0x7
	.4byte	.LASF134
	.sleb128 22
	.uleb128 0x7
	.4byte	.LASF135
	.sleb128 23
	.uleb128 0x7
	.4byte	.LASF136
	.sleb128 24
	.uleb128 0x7
	.4byte	.LASF137
	.sleb128 25
	.uleb128 0x7
	.4byte	.LASF138
	.sleb128 26
	.uleb128 0x7
	.4byte	.LASF139
	.sleb128 27
	.uleb128 0x7
	.4byte	.LASF140
	.sleb128 28
	.uleb128 0x7
	.4byte	.LASF141
	.sleb128 29
	.uleb128 0x7
	.4byte	.LASF142
	.sleb128 30
	.uleb128 0x7
	.4byte	.LASF143
	.sleb128 31
	.uleb128 0x7
	.4byte	.LASF144
	.sleb128 32
	.uleb128 0x7
	.4byte	.LASF145
	.sleb128 33
	.uleb128 0x7
	.4byte	.LASF146
	.sleb128 34
	.byte	0
	.uleb128 0x8
	.byte	0xc
	.byte	0x1
	.byte	0x1f
	.4byte	0x669
	.uleb128 0x9
	.4byte	.LASF147
	.byte	0x1
	.byte	0x21
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF148
	.byte	0x1
	.byte	0x22
	.4byte	0x669
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x9
	.4byte	.LASF149
	.byte	0x1
	.byte	0x23
	.4byte	0x66f
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.4byte	0x447
	.uleb128 0xe
	.byte	0x4
	.4byte	0x237
	.uleb128 0x3
	.4byte	.LASF150
	.byte	0x1
	.byte	0x24
	.4byte	0x636
	.uleb128 0xf
	.4byte	.LASF176
	.byte	0x1
	.byte	0x51
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x6ed
	.uleb128 0x10
	.4byte	.LASF151
	.byte	0x1
	.byte	0x51
	.4byte	0x6ed
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x10
	.4byte	.LASF152
	.byte	0x1
	.byte	0x52
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x11
	.4byte	.LASF153
	.byte	0x1
	.byte	0x54
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"tmp\000"
	.byte	0x1
	.byte	0x54
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.ascii	"clk\000"
	.byte	0x1
	.byte	0x55
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x11
	.4byte	.LASF154
	.byte	0x1
	.byte	0x56
	.4byte	0x669
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.4byte	0x675
	.uleb128 0x13
	.4byte	.LASF169
	.byte	0x1
	.byte	0x8c
	.byte	0x1
	.4byte	0x90
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x756
	.uleb128 0x10
	.4byte	.LASF151
	.byte	0x1
	.byte	0x8c
	.4byte	0x6ed
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.ascii	"tmp\000"
	.byte	0x1
	.byte	0x8e
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x11
	.4byte	.LASF155
	.byte	0x1
	.byte	0x8f
	.4byte	0x9b
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF154
	.byte	0x1
	.byte	0x90
	.4byte	0x669
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF156
	.byte	0x1
	.byte	0x91
	.4byte	0x66f
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF158
	.byte	0x1
	.2byte	0x12f
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x7a2
	.uleb128 0x15
	.4byte	.LASF157
	.byte	0x1
	.2byte	0x12f
	.4byte	0x7a2
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.ascii	"arg\000"
	.byte	0x1
	.2byte	0x12f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x131
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x4
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF159
	.byte	0x1
	.2byte	0x163
	.byte	0x1
	.4byte	0x9b
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x7f0
	.uleb128 0x15
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x163
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF151
	.byte	0x1
	.2byte	0x165
	.4byte	0x6ed
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x166
	.4byte	0x9b
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x18c
	.byte	0x1
	.4byte	0x9b
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x8a3
	.uleb128 0x15
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x18c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x16
	.ascii	"cmd\000"
	.byte	0x1
	.2byte	0x18d
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x16
	.ascii	"arg\000"
	.byte	0x1
	.2byte	0x18e
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x17
	.4byte	.LASF162
	.byte	0x1
	.2byte	0x190
	.4byte	0x669
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x19
	.ascii	"ptr\000"
	.byte	0x1
	.2byte	0x191
	.4byte	0x8a3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x192
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF163
	.byte	0x1
	.2byte	0x192
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x19
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x192
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF151
	.byte	0x1
	.2byte	0x193
	.4byte	0x6ed
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x17
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x194
	.4byte	0x9b
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.4byte	0x70
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF164
	.byte	0x1
	.2byte	0x2f8
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x8f5
	.uleb128 0x15
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x2f8
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x15
	.4byte	.LASF165
	.byte	0x1
	.2byte	0x2f9
	.4byte	0x7a2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x15
	.4byte	.LASF166
	.byte	0x1
	.2byte	0x2fa
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF167
	.byte	0x1
	.2byte	0x315
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x941
	.uleb128 0x15
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x315
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x15
	.4byte	.LASF165
	.byte	0x1
	.2byte	0x316
	.4byte	0x7a2
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x15
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x317
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF170
	.byte	0x1
	.2byte	0x31e
	.byte	0x1
	.4byte	0x90
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x9aa
	.uleb128 0x15
	.4byte	.LASF151
	.byte	0x1
	.2byte	0x31e
	.4byte	0x6ed
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x320
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x321
	.4byte	0x9b
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x322
	.4byte	0x669
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF156
	.byte	0x1
	.2byte	0x323
	.4byte	0x66f
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF171
	.byte	0x1
	.2byte	0x37d
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x9f6
	.uleb128 0x15
	.4byte	.LASF157
	.byte	0x1
	.2byte	0x37d
	.4byte	0x7a2
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.ascii	"arg\000"
	.byte	0x1
	.2byte	0x37d
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x37f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.4byte	.LASF172
	.byte	0x1
	.byte	0x27
	.4byte	0x675
	.byte	0x5
	.byte	0x3
	.4byte	lcdcfg
	.uleb128 0x11
	.4byte	.LASF173
	.byte	0x1
	.byte	0x2b
	.4byte	0xa18
	.byte	0x5
	.byte	0x3
	.4byte	lcd_vcmp_sel
	.uleb128 0x1b
	.4byte	0x545
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF145:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF58:
	.ascii	"rsrved1clcdc\000"
.LASF163:
	.ascii	"tmp1\000"
.LASF173:
	.ascii	"lcd_vcmp_sel\000"
.LASF169:
	.ascii	"lcd_initialize\000"
.LASF9:
	.ascii	"INT_32\000"
.LASF98:
	.ascii	"BACK_PORCH_START\000"
.LASF16:
	.ascii	"MONO_4BIT\000"
.LASF172:
	.ascii	"lcdcfg\000"
.LASF92:
	.ascii	"LCD_CRSR_PAL0\000"
.LASF93:
	.ascii	"LCD_CRSR_PAL1\000"
.LASF165:
	.ascii	"buffer\000"
.LASF31:
	.ascii	"invert_vsync\000"
.LASF81:
	.ascii	"LCD_ENABLE_INTS\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF52:
	.ascii	"lcdintrenable\000"
.LASF25:
	.ascii	"v_front_porch\000"
.LASF74:
	.ascii	"CLCDC_REGS_T\000"
.LASF171:
	.ascii	"calsense_lcd_open\000"
.LASF89:
	.ascii	"LCD_GET_STATUS\000"
.LASF111:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF59:
	.ascii	"lcdpalette\000"
.LASF124:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF28:
	.ascii	"invert_output_enable\000"
.LASF160:
	.ascii	"devid\000"
.LASF15:
	.ascii	"HRTFT\000"
.LASF97:
	.ascii	"VSYNC_START\000"
.LASF101:
	.ascii	"LCD_ENABLE_ST\000"
.LASF62:
	.ascii	"lcdcrsrcntl\000"
.LASF61:
	.ascii	"lcdcrsrimage\000"
.LASF68:
	.ascii	"rsrved3clcdc\000"
.LASF94:
	.ascii	"LCD_CRSR_INTMSK\000"
.LASF135:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF96:
	.ascii	"LCD_INIT_IMG_PALL\000"
.LASF112:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF24:
	.ascii	"v_back_porch\000"
.LASF21:
	.ascii	"h_front_porch\000"
.LASF154:
	.ascii	"lcdptr\000"
.LASF153:
	.ascii	"pixel_div\000"
.LASF49:
	.ascii	"lcdupbase\000"
.LASF170:
	.ascii	"calsense_lcd_initialize\000"
.LASF132:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF57:
	.ascii	"lcdlpcurr\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF63:
	.ascii	"lcdcrsrcnfg\000"
.LASF119:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF23:
	.ascii	"pixels_per_line\000"
.LASF99:
	.ascii	"VIDEO_START\000"
.LASF20:
	.ascii	"h_back_porch\000"
.LASF48:
	.ascii	"lcdle\000"
.LASF35:
	.ascii	"lcd_panel_type\000"
.LASF147:
	.ascii	"init\000"
.LASF27:
	.ascii	"lines_per_panel\000"
.LASF55:
	.ascii	"lcdintclr\000"
.LASF90:
	.ascii	"LCD_CRSR_EN\000"
.LASF72:
	.ascii	"lcdcrsrintstat\000"
.LASF70:
	.ascii	"lcdcrsrintclr\000"
.LASF44:
	.ascii	"LCD_PARAM_T\000"
.LASF84:
	.ascii	"LCD_CLEAR_INT\000"
.LASF142:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF125:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF122:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF17:
	.ascii	"MONO_8BIT\000"
.LASF22:
	.ascii	"h_sync_pulse_width\000"
.LASF109:
	.ascii	"LCD_YSIZE\000"
.LASF139:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF116:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF166:
	.ascii	"max_bytes\000"
.LASF40:
	.ascii	"hrtft_polarity_delay\000"
.LASF149:
	.ascii	"dptr\000"
.LASF0:
	.ascii	"char\000"
.LASF136:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF78:
	.ascii	"LCD_SET_UP_FB\000"
.LASF66:
	.ascii	"lcdcrsrxy\000"
.LASF128:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF155:
	.ascii	"status\000"
.LASF129:
	.ascii	"CLKPWR_WDOG_CLK\000"
.LASF162:
	.ascii	"lcdregs\000"
.LASF51:
	.ascii	"lcdctrl\000"
.LASF53:
	.ascii	"lcdinterrupt\000"
.LASF11:
	.ascii	"long long int\000"
.LASF158:
	.ascii	"lcd_open\000"
.LASF140:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF41:
	.ascii	"hrtft_lp_delay\000"
.LASF14:
	.ascii	"ADTFT\000"
.LASF42:
	.ascii	"hrtft_spl_delay\000"
.LASF130:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF110:
	.ascii	"LCD_COLOR_DEPTH\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF26:
	.ascii	"v_sync_pulse_width\000"
.LASF60:
	.ascii	"rsrved2clcdc\000"
.LASF103:
	.ascii	"LCD_UP_FB\000"
.LASF91:
	.ascii	"LCD_CRSR_XY\000"
.LASF76:
	.ascii	"LCD_ENABLE\000"
.LASF33:
	.ascii	"bits_per_pixel\000"
.LASF82:
	.ascii	"LCD_DISABLE_INTS\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF113:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF45:
	.ascii	"lcdtimh\000"
.LASF164:
	.ascii	"lcd_read\000"
.LASF126:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF46:
	.ascii	"lcdtimv\000"
.LASF77:
	.ascii	"LCD_PWENABLE\000"
.LASF79:
	.ascii	"LCD_SET_LW_FB\000"
.LASF19:
	.ascii	"LCD_PANEL_T\000"
.LASF114:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF138:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF176:
	.ascii	"lcd_update_clock\000"
.LASF39:
	.ascii	"hrtft_lp_to_ps_delay\000"
.LASF100:
	.ascii	"FRONT_PORCH_START\000"
.LASF54:
	.ascii	"lcdstatus\000"
.LASF151:
	.ascii	"lcdcfgptr\000"
.LASF120:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF85:
	.ascii	"LCD_DMA_ON_4MT\000"
.LASF156:
	.ascii	"cgdatptr\000"
.LASF67:
	.ascii	"lcdcrsrclip\000"
.LASF146:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF150:
	.ascii	"CLCDC_CFG_T\000"
.LASF143:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF56:
	.ascii	"lcdupcurr\000"
.LASF6:
	.ascii	"short int\000"
.LASF157:
	.ascii	"ipbase\000"
.LASF29:
	.ascii	"invert_panel_clock\000"
.LASF123:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF38:
	.ascii	"hrtft_sps_enable\000"
.LASF168:
	.ascii	"n_bytes\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF37:
	.ascii	"hrtft_cls_enable\000"
.LASF161:
	.ascii	"lcd_ioctl\000"
.LASF127:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF87:
	.ascii	"LCD_SET_BPP\000"
.LASF108:
	.ascii	"LCD_XSIZE\000"
.LASF104:
	.ascii	"LCD_LW_FB\000"
.LASF18:
	.ascii	"CSTN\000"
.LASF69:
	.ascii	"lcdcrsrintmsk\000"
.LASF43:
	.ascii	"hrtft_spl_to_cls_delay\000"
.LASF174:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF36:
	.ascii	"dual_panel\000"
.LASF105:
	.ascii	"LCD_OVR_FB\000"
.LASF117:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF159:
	.ascii	"lcd_close\000"
.LASF137:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF50:
	.ascii	"lcdlpbase\000"
.LASF73:
	.ascii	"long unsigned int\000"
.LASF121:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF144:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF83:
	.ascii	"LCD_PICK_INT\000"
.LASF32:
	.ascii	"ac_bias_frequency\000"
.LASF175:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_clcdc_driver.c\000"
.LASF75:
	.ascii	"LCD_CONFIG\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF134:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF148:
	.ascii	"regptr\000"
.LASF152:
	.ascii	"desired_clock\000"
.LASF141:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF47:
	.ascii	"lcdpol\000"
.LASF88:
	.ascii	"LCD_SET_CLOCK\000"
.LASF102:
	.ascii	"LCD_PWENABLE_ST\000"
.LASF131:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF71:
	.ascii	"lcdcrsrintraw\000"
.LASF86:
	.ascii	"LCD_SWAP_RGB\000"
.LASF2:
	.ascii	"signed char\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF106:
	.ascii	"LCD_PANEL_TYPE\000"
.LASF95:
	.ascii	"LCD_CRSR_INIT_IMG\000"
.LASF34:
	.ascii	"optimal_clock\000"
.LASF64:
	.ascii	"lcdcrsrpal0\000"
.LASF65:
	.ascii	"lcdcrsrpal1\000"
.LASF167:
	.ascii	"lcd_write\000"
.LASF133:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF30:
	.ascii	"invert_hsync\000"
.LASF13:
	.ascii	"STATUS\000"
.LASF115:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF107:
	.ascii	"LCD_CLOCK\000"
.LASF118:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF80:
	.ascii	"LCD_SET_OV_FB\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
