	.file	"e_budgets.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	g_BUDGET_SETUP_meter_read_date
	.section	.bss.g_BUDGET_SETUP_meter_read_date,"aw",%nobits
	.align	2
	.type	g_BUDGET_SETUP_meter_read_date, %object
	.size	g_BUDGET_SETUP_meter_read_date, 96
g_BUDGET_SETUP_meter_read_date:
	.space	96
	.global	g_BUDGET_SETUP_budget
	.section	.bss.g_BUDGET_SETUP_budget,"aw",%nobits
	.align	2
	.type	g_BUDGET_SETUP_budget, %object
	.size	g_BUDGET_SETUP_budget, 96
g_BUDGET_SETUP_budget:
	.space	96
	.global	g_BUDGET_SETUP_annual_budget
	.section	.bss.g_BUDGET_SETUP_annual_budget,"aw",%nobits
	.align	2
	.type	g_BUDGET_SETUP_annual_budget, %object
	.size	g_BUDGET_SETUP_annual_budget, 4
g_BUDGET_SETUP_annual_budget:
	.space	4
	.section	.bss.date_change_warning_flag,"aw",%nobits
	.align	2
	.type	date_change_warning_flag, %object
	.size	date_change_warning_flag, 4
date_change_warning_flag:
	.space	4
	.section	.text.BUDGETS_get_inc_by_for_yearly_budget,"ax",%progbits
	.align	2
	.type	BUDGETS_get_inc_by_for_yearly_budget, %function
BUDGETS_get_inc_by_for_yearly_budget:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_budgets.c"
	.loc 1 93 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 96 0
	ldr	r3, [fp, #-8]
	cmp	r3, #64
	bls	.L2
	.loc 1 98 0
	ldr	r3, .L5
	str	r3, [fp, #-4]
	b	.L3
.L2:
	.loc 1 100 0
	ldr	r3, [fp, #-8]
	cmp	r3, #32
	bls	.L4
	.loc 1 102 0
	mov	r3, #500
	str	r3, [fp, #-4]
	b	.L3
.L4:
	.loc 1 106 0
	mov	r3, #100
	str	r3, [fp, #-4]
.L3:
	.loc 1 109 0
	ldr	r3, [fp, #-4]
	.loc 1 110 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L6:
	.align	2
.L5:
	.word	5000
.LFE0:
	.size	BUDGETS_get_inc_by_for_yearly_budget, .-BUDGETS_get_inc_by_for_yearly_budget
	.section	.text.POC_get_inc_by_for_monthly_budget,"ax",%progbits
	.align	2
	.type	POC_get_inc_by_for_monthly_budget, %function
POC_get_inc_by_for_monthly_budget:
.LFB1:
	.loc 1 115 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #12
.LCFI5:
	str	r0, [fp, #-16]
	.loc 1 117 0
	mov	r3, #16
	str	r3, [fp, #-12]
	.loc 1 128 0
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r2, r3, r2
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcs	.L8
	.loc 1 130 0
	adr	r1, .L12
	ldmia	r1, {r0-r1}
	adr	r3, .L12+8
	ldmia	r3, {r2-r3}
	bl	pow
	fmdrr	d7, r0, r1
	ftouizd	s13, d7
	fmrs	r3, s13	@ int
	str	r3, [fp, #-8]
	b	.L9
.L8:
	.loc 1 132 0
	ldr	r3, [fp, #-12]
	mov	r2, r3, asl #1
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcs	.L10
	.loc 1 134 0
	adr	r1, .L12
	ldmia	r1, {r0-r1}
	adr	r3, .L12+16
	ldmia	r3, {r2-r3}
	bl	pow
	fmdrr	d7, r0, r1
	ftouizd	s13, d7
	fmrs	r3, s13	@ int
	str	r3, [fp, #-8]
	b	.L9
.L10:
	.loc 1 136 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bls	.L11
	.loc 1 139 0
	adr	r1, .L12
	ldmia	r1, {r0-r1}
	adr	r3, .L12+16
	ldmia	r3, {r2-r3}
	bl	pow
	fmdrr	d7, r0, r1
	ftouizd	s13, d7
	fmrs	r3, s13	@ int
	str	r3, [fp, #-8]
	b	.L9
.L11:
	.loc 1 145 0
	adr	r1, .L12
	ldmia	r1, {r0-r1}
	adr	r3, .L12+16
	ldmia	r3, {r2-r3}
	bl	pow
	fmdrr	d7, r0, r1
	ftouizd	s13, d7
	fmrs	r3, s13	@ int
	str	r3, [fp, #-8]
.L9:
	.loc 1 148 0
	ldr	r3, [fp, #-8]
	.loc 1 149 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	0
	.word	1076101120
	.word	0
	.word	1074266112
	.word	0
	.word	1073741824
.LFE1:
	.size	POC_get_inc_by_for_monthly_budget, .-POC_get_inc_by_for_monthly_budget
	.section	.text.BUDGETS_show_warnings,"ax",%progbits
	.align	2
	.type	BUDGETS_show_warnings, %function
BUDGETS_show_warnings:
.LFB2:
	.loc 1 161 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	.loc 1 163 0
	ldr	r3, .L18
	flds	s15, [r3, #0]
	fcmpzs	s15
	fmstat
	beq	.L15
	.loc 1 163 0 is_stmt 0 discriminator 2
	ldr	r3, .L18+4
	flds	s15, [r3, #0]
	fcmpzs	s15
	fmstat
	bne	.L16
.L15:
	.loc 1 163 0 discriminator 1
	mov	r3, #1
	b	.L17
.L16:
	mov	r3, #0
.L17:
	.loc 1 163 0 discriminator 3
	mov	r2, r3
	ldr	r3, .L18+8
	str	r2, [r3, #0]
	.loc 1 164 0 is_stmt 1 discriminator 3
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 165 0 discriminator 3
	ldmfd	sp!, {fp, pc}
.L19:
	.align	2
.L18:
	.word	GuiVar_BudgetForThisPeriod
	.word	GuiVar_BudgetAnnualValue
	.word	GuiVar_BudgetZeroWarning
.LFE2:
	.size	BUDGETS_show_warnings, .-BUDGETS_show_warnings
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budgets.c\000"
	.section	.text.BUDGETS_update_guivars,"ax",%progbits
	.align	2
	.type	BUDGETS_update_guivars, %function
BUDGETS_update_guivars:
.LFB3:
	.loc 1 172 0
	@ args = 0, pretend = 0, frame = 168
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	fstmfdd	sp!, {d8}
.LCFI9:
	add	fp, sp, #12
.LCFI10:
	sub	sp, sp, #176
.LCFI11:
	str	r0, [fp, #-180]
	.loc 1 174 0
	ldr	r3, .L28+4	@ float
	str	r3, [fp, #-24]	@ float
	.loc 1 190 0
	ldr	r3, .L28+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L28+12
	mov	r3, #190
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 192 0
	ldr	r3, .L28+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_group_at_this_index
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_GID_irrigation_system
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-20]
	.loc 1 197 0
	bl	NETWORK_CONFIG_get_water_units
	mov	r3, r0
	cmp	r3, #0
	bne	.L21
	.loc 1 199 0
	ldr	r3, .L28	@ float
	str	r3, [fp, #-16]	@ float
	.loc 1 201 0
	ldr	r3, .L28+20
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 202 0
	ldr	r3, .L28+24
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L22
.L21:
	.loc 1 206 0
	flds	s15, [fp, #-24]
	flds	s14, .L28
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-16]
	.loc 1 208 0
	ldr	r3, .L28+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 209 0
	ldr	r3, .L28+24
	mov	r2, #1
	str	r2, [r3, #0]
.L22:
	.loc 1 212 0
	ldr	r3, .L28+28
	ldr	r2, [fp, #-180]
	str	r2, [r3, #0]
	.loc 1 214 0
	ldr	r3, .L28+32
	ldr	r2, [fp, #-180]
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #176
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #16
	mov	r2, r3
	mov	r3, #125
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L28+36
	mov	r1, r3
	mov	r2, #16
	bl	strlcpy
	.loc 1 216 0
	ldr	r3, [fp, #-180]
	add	r2, r3, #1
	ldr	r3, .L28+32
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #176
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #16
	mov	r2, r3
	mov	r3, #125
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L28+40
	mov	r1, r3
	mov	r2, #16
	bl	strlcpy
	.loc 1 218 0
	ldr	r2, .L28+44
	ldr	r3, [fp, #-180]
	mov	r3, r3, asl #2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-16]
	fmuls	s15, s14, s15
	ldr	r3, .L28+48
	fsts	s15, [r3, #0]
	.loc 1 220 0
	ldr	r3, .L28+52
	flds	s14, [r3, #0]
	flds	s15, [fp, #-16]
	fmuls	s15, s14, s15
	ldr	r3, .L28+56
	fsts	s15, [r3, #0]
	.loc 1 224 0
	ldr	r3, .L28+60
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L23
	.loc 1 227 0
	ldr	r3, [fp, #-180]
	cmp	r3, #0
	bne	.L24
	.loc 1 229 0
	ldr	r3, .L28+64
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L28+12
	mov	r3, #229
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 231 0
	ldr	r3, .L28+68
	ldr	r2, [r3, #0]
	sub	r3, fp, #160
	mov	r0, r2
	mov	r1, r3
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	.loc 1 232 0
	flds	s15, [fp, #-16]
	fcvtds	d8, s15
	ldr	r3, [fp, #-160]
	mov	r0, r3
	bl	nm_BUDGET_get_used
	fmdrr	d7, r0, r1
	fmuld	d7, d8, d7
	fcvtsd	s15, d7
	ldr	r3, .L28+72
	fsts	s15, [r3, #0]
	.loc 1 234 0
	ldr	r3, .L28+64
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L25
.L24:
	.loc 1 239 0
	ldr	r3, .L28+72
	ldr	r2, .L28+76	@ float
	str	r2, [r3, #0]	@ float
.L25:
	.loc 1 244 0
	sub	r3, fp, #136
	ldr	r0, [fp, #-20]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 246 0
	ldr	r3, [fp, #-180]
	cmp	r3, #0
	bne	.L26
	.loc 1 249 0
	sub	r3, fp, #156
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 250 0
	ldr	r0, [fp, #-20]
	bl	nm_SYSTEM_get_used
	mov	r3, r0
	fmsr	s15, r3	@ int
	fuitos	s16, s15
	ldr	r0, [fp, #-20]
	bl	nm_GROUP_get_group_ID
	sub	r1, fp, #136
	sub	r2, fp, #156
	mov	r3, #0
	str	r3, [sp, #4]
	ldrh	r3, [fp, #-152]	@ movhi
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [fp, #-156]
	bl	nm_BUDGET_predicted_volume
	fmsr	s15, r0
	fadds	s14, s16, s15
	flds	s15, [fp, #-16]
	fmuls	s15, s14, s15
	ldr	r3, .L28+80
	fsts	s15, [r3, #0]
	b	.L27
.L26:
	.loc 1 255 0
	ldr	r3, .L28+32
	ldr	r2, [fp, #-180]
	ldr	r1, [r3, r2, asl #2]
	ldr	r2, [fp, #-132]
	sub	r3, fp, #156
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	DateAndTimeToDTCS
	.loc 1 256 0
	ldr	r0, [fp, #-20]
	bl	nm_GROUP_get_group_ID
	sub	r1, fp, #136
	sub	r2, fp, #156
	mov	r3, #0
	str	r3, [sp, #4]
	ldrh	r3, [fp, #-152]	@ movhi
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [fp, #-156]
	bl	nm_BUDGET_predicted_volume
	fmsr	s14, r0
	flds	s15, [fp, #-16]
	fmuls	s15, s14, s15
	ldr	r3, .L28+80
	fsts	s15, [r3, #0]
.L27:
	.loc 1 260 0
	ldr	r3, [fp, #-28]
	mov	r0, r3
	bl	BUDGETS_show_warnings
.L23:
	.loc 1 265 0
	ldr	r3, .L28+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 266 0
	sub	sp, fp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {fp, pc}
.L29:
	.align	2
.L28:
	.word	1065353216
	.word	1144718131
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	g_GROUP_list_item_index
	.word	GuiVar_BudgetUnitGallon
	.word	GuiVar_BudgetUnitHCF
	.word	GuiVar_BudgetPeriodIdx
	.word	g_BUDGET_SETUP_meter_read_date
	.word	GuiVar_BudgetPeriodStart
	.word	GuiVar_BudgetPeriodEnd
	.word	g_BUDGET_SETUP_budget
	.word	GuiVar_BudgetForThisPeriod
	.word	g_BUDGET_SETUP_annual_budget
	.word	GuiVar_BudgetAnnualValue
	.word	GuiVar_IsMaster
	.word	poc_preserves_recursive_MUTEX
	.word	g_GROUP_ID
	.word	GuiVar_BudgetUseThisPeriod
	.word	0
	.word	GuiVar_BudgetExpectedUseThisPeriod
.LFE3:
	.size	BUDGETS_update_guivars, .-BUDGETS_update_guivars
	.section	.text.BUDGET_SETUP_update_montly_budget,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_update_montly_budget, %function
BUDGET_SETUP_update_montly_budget:
.LFB4:
	.loc 1 285 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #40
.LCFI14:
	.loc 1 300 0
	ldr	r3, .L37	@ float
	str	r3, [fp, #-16]	@ float
	.loc 1 306 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L31
.L32:
	.loc 1 308 0 discriminator 2
	ldr	r0, [fp, #-8]
	bl	ET_DATA_et_number_for_the_month
	str	r0, [fp, #-20]	@ float
	.loc 1 310 0 discriminator 2
	flds	s14, [fp, #-16]
	flds	s15, [fp, #-20]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-16]
	.loc 1 306 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L31:
	.loc 1 306 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #12
	bls	.L32
	.loc 1 313 0 is_stmt 1
	ldr	r3, .L37+4
	flds	s14, [r3, #0]
	flds	s15, [fp, #-16]
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-24]
	.loc 1 316 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 318 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L33
.L36:
	.loc 1 320 0
	ldrh	r3, [fp, #-36]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 322 0
	ldrh	r3, [fp, #-36]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
	cmp	r3, #12
	bls	.L34
	.loc 1 324 0
	ldrh	r3, [fp, #-36]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
	sub	r3, r3, #12
	str	r3, [fp, #-12]
	b	.L35
.L34:
	.loc 1 328 0
	ldrh	r3, [fp, #-36]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-12]
.L35:
	.loc 1 331 0
	ldr	r0, [fp, #-12]
	bl	ET_DATA_et_number_for_the_month
	str	r0, [fp, #-20]	@ float
	.loc 1 333 0
	flds	s14, [fp, #-20]
	flds	s15, [fp, #-24]
	fmuls	s15, s14, s15
	ldr	r2, .L37+8
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #2
	add	r3, r2, r3
	fsts	s15, [r3, #0]
	.loc 1 318 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L33:
	.loc 1 318 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L36
	.loc 1 335 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L38:
	.align	2
.L37:
	.word	0
	.word	g_BUDGET_SETUP_annual_budget
	.word	g_BUDGET_SETUP_budget
.LFE4:
	.size	BUDGET_SETUP_update_montly_budget, .-BUDGET_SETUP_update_montly_budget
	.section	.text.BUDGET_SETUP_handle_annual_budget_change,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_handle_annual_budget_change, %function
BUDGET_SETUP_handle_annual_budget_change:
.LFB5:
	.loc 1 353 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #40
.LCFI17:
	str	r0, [fp, #-36]
	str	r1, [fp, #-32]
	.loc 1 355 0
	ldr	r3, .L44+4	@ float
	str	r3, [fp, #-24]	@ float
	.loc 1 368 0
	ldr	r3, .L44+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L40
	.loc 1 371 0
	ldr	r3, .L44+12	@ float
	str	r3, [fp, #-16]	@ float
	.loc 1 372 0
	ldr	r3, .L44+16	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 373 0
	flds	s15, [fp, #-24]
	flds	s14, .L44
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-12]
	.loc 1 374 0
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L41
.L40:
	.loc 1 378 0
	ldr	r3, [fp, #-32]
	mov	r0, r3
	bl	BUDGETS_get_inc_by_for_yearly_budget
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fsts	s15, [fp, #-16]
	.loc 1 379 0
	ldr	r3, .L44+16	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 380 0
	ldr	r3, .L44	@ float
	str	r3, [fp, #-12]	@ float
	.loc 1 381 0
	mov	r3, #0
	str	r3, [fp, #-20]
.L41:
	.loc 1 385 0
	ldr	r3, .L44+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L42
.LBB2:
	.loc 1 387 0
	ldr	r3, .L44+24
	ldr	r3, [r3, #0]	@ float
	mov	r0, r3	@ float
	ldr	r1, [fp, #-20]
	bl	__round_float
	fmsr	s15, r0
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-28]
	.loc 1 389 0
	ldr	r2, [fp, #-36]
	flds	s15, [fp, #-8]
	ftouizs	s15, s15
	fmrs	lr, s15	@ int
	flds	s15, [fp, #-12]
	ftouizs	s15, s15
	fmrs	ip, s15	@ int
	flds	s15, [fp, #-16]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	sub	r3, fp, #28
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, r3
	mov	r2, lr
	mov	r3, ip
	bl	process_uns32_r
	.loc 1 392 0
	ldr	r3, [fp, #-28]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	ldr	r3, .L44+24
	fsts	s15, [r3, #0]
	.loc 1 394 0
	ldr	r3, .L44+24
	ldr	r2, [r3, #0]	@ float
	ldr	r3, .L44+28
	str	r2, [r3, #0]	@ float
	b	.L43
.L42:
.LBE2:
	.loc 1 400 0
	ldr	r3, .L44+28
	ldr	r3, [r3, #0]	@ float
	mov	r0, r3	@ float
	ldr	r1, [fp, #-20]
	bl	__round_float
	mov	r2, r0	@ float
	ldr	r3, .L44+28
	str	r2, [r3, #0]	@ float
	.loc 1 402 0
	ldr	r3, [fp, #-36]
	ldr	r2, [fp, #-16]	@ float
	str	r2, [sp, #0]	@ float
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L44+28
	ldr	r2, [fp, #-8]	@ float
	ldr	r3, [fp, #-12]	@ float
	bl	process_fl
	.loc 1 404 0
	ldr	r3, .L44+28
	flds	s14, [r3, #0]
	flds	s15, [fp, #-24]
	fmuls	s15, s14, s15
	ldr	r3, .L44+24
	fsts	s15, [r3, #0]
.L43:
	.loc 1 407 0
	bl	BUDGET_SETUP_update_montly_budget
	.loc 1 411 0
	bl	BUDGETS_show_warnings
	.loc 1 412 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L45:
	.align	2
.L44:
	.word	1317997848
	.word	1144718131
	.word	GuiVar_BudgetUnitHCF
	.word	1036831949
	.word	0
	.word	GuiVar_BudgetUnitGallon
	.word	g_BUDGET_SETUP_annual_budget
	.word	GuiVar_BudgetAnnualValue
.LFE5:
	.size	BUDGET_SETUP_handle_annual_budget_change, .-BUDGET_SETUP_handle_annual_budget_change
	.section	.text.BUDGET_SETUP_handle_budget_change,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_handle_budget_change, %function
BUDGET_SETUP_handle_budget_change:
.LFB6:
	.loc 1 417 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #40
.LCFI20:
	str	r0, [fp, #-36]
	str	r1, [fp, #-32]
	.loc 1 419 0
	ldr	r3, .L51+4	@ float
	str	r3, [fp, #-24]	@ float
	.loc 1 445 0
	ldr	r3, .L51+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L47
	.loc 1 448 0
	ldr	r3, .L51+12	@ float
	str	r3, [fp, #-16]	@ float
	.loc 1 449 0
	ldr	r3, .L51+16	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 450 0
	flds	s15, [fp, #-24]
	flds	s14, .L51
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-12]
	.loc 1 451 0
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L48
.L47:
	.loc 1 455 0
	ldr	r3, [fp, #-32]
	mov	r0, r3
	bl	POC_get_inc_by_for_monthly_budget
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fsts	s15, [fp, #-16]
	.loc 1 456 0
	ldr	r3, .L51+16	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 457 0
	ldr	r3, .L51	@ float
	str	r3, [fp, #-12]	@ float
	.loc 1 458 0
	mov	r3, #0
	str	r3, [fp, #-20]
.L48:
	.loc 1 464 0
	ldr	r3, .L51+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L49
.LBB3:
	.loc 1 470 0
	ldr	r3, .L51+24
	ldr	r3, [r3, #0]
	ldr	r2, .L51+28
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r3, [r3, #0]	@ float
	mov	r0, r3	@ float
	ldr	r1, [fp, #-20]
	bl	__round_float
	fmsr	s15, r0
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-28]
	.loc 1 472 0
	ldr	r2, [fp, #-36]
	flds	s15, [fp, #-8]
	ftouizs	s15, s15
	fmrs	lr, s15	@ int
	flds	s15, [fp, #-12]
	ftouizs	s15, s15
	fmrs	ip, s15	@ int
	flds	s15, [fp, #-16]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	sub	r3, fp, #28
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, r3
	mov	r2, lr
	mov	r3, ip
	bl	process_uns32_r
	.loc 1 474 0
	ldr	r3, .L51+24
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-28]
	fmsr	s14, r2	@ int
	fuitos	s15, s14
	ldr	r2, .L51+28
	mov	r3, r3, asl #2
	add	r3, r2, r3
	fsts	s15, [r3, #0]
	.loc 1 476 0
	ldr	r3, .L51+24
	ldr	r3, [r3, #0]
	ldr	r2, .L51+28
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r2, [r3, #0]	@ float
	ldr	r3, .L51+32
	str	r2, [r3, #0]	@ float
	b	.L50
.L49:
.LBE3:
	.loc 1 482 0
	ldr	r3, .L51+32
	ldr	r3, [r3, #0]	@ float
	mov	r0, r3	@ float
	ldr	r1, [fp, #-20]
	bl	__round_float
	mov	r2, r0	@ float
	ldr	r3, .L51+32
	str	r2, [r3, #0]	@ float
	.loc 1 484 0
	ldr	r3, [fp, #-36]
	ldr	r2, [fp, #-16]	@ float
	str	r2, [sp, #0]	@ float
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L51+32
	ldr	r2, [fp, #-8]	@ float
	ldr	r3, [fp, #-12]	@ float
	bl	process_fl
	.loc 1 486 0
	ldr	r3, .L51+24
	ldr	r3, [r3, #0]
	ldr	r2, .L51+32
	flds	s14, [r2, #0]
	flds	s15, [fp, #-24]
	fmuls	s15, s14, s15
	ldr	r2, .L51+28
	mov	r3, r3, asl #2
	add	r3, r2, r3
	fsts	s15, [r3, #0]
.L50:
	.loc 1 491 0
	bl	BUDGETS_show_warnings
	.loc 1 492 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L52:
	.align	2
.L51:
	.word	1287568416
	.word	1144718131
	.word	GuiVar_BudgetUnitHCF
	.word	1036831949
	.word	0
	.word	GuiVar_BudgetUnitGallon
	.word	GuiVar_BudgetPeriodIdx
	.word	g_BUDGET_SETUP_budget
	.word	GuiVar_BudgetForThisPeriod
.LFE6:
	.size	BUDGET_SETUP_handle_budget_change, .-BUDGET_SETUP_handle_budget_change
	.section	.text.BUDGET_SETUP_handle_start_date_change,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_handle_start_date_change, %function
BUDGET_SETUP_handle_start_date_change:
.LFB7:
	.loc 1 500 0
	@ args = 0, pretend = 0, frame = 200
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #208
.LCFI23:
	str	r0, [fp, #-196]
	str	r1, [fp, #-200]
	str	r2, [fp, #-204]
	.loc 1 502 0
	ldr	r3, .L65+4	@ float
	str	r3, [fp, #-40]	@ float
	.loc 1 523 0
	ldr	r3, .L65+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L65+12
	ldr	r3, .L65+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 525 0
	ldr	r3, .L65+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L65+12
	ldr	r3, .L65+24
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 527 0
	ldr	r3, .L65+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-20]
	.loc 1 531 0
	ldr	r3, .L65+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L65+12
	ldr	r3, .L65+36
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 532 0
	ldr	r3, .L65+40
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L65+12
	mov	r3, #532
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 534 0
	ldr	r0, [fp, #-20]
	bl	nm_GROUP_get_group_ID
	mov	r2, r0
	sub	r3, fp, #172
	mov	r0, r2
	mov	r1, r3
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	str	r0, [fp, #-24]
	.loc 1 536 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #24]
	ldr	r3, [r3, #468]
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	and	r3, r3, #255
	str	r3, [fp, #-28]
	.loc 1 538 0
	ldr	r3, .L65+40
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 539 0
	ldr	r3, .L65+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 543 0
	ldr	r0, [fp, #-20]
	bl	POC_get_GID_irrigation_system
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-32]
	.loc 1 545 0
	ldr	r3, .L65+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 547 0
	sub	r3, fp, #168
	ldr	r0, [fp, #-32]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 549 0
	ldr	r3, .L65+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 557 0
	mov	r3, #68
	str	r3, [fp, #-36]
	.loc 1 562 0
	ldr	r3, [fp, #-204]
	cmp	r3, #0
	bne	.L54
	.loc 1 564 0
	ldr	r3, [fp, #-204]
	add	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-36]
	rsb	r3, r3, r2
	sub	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 565 0
	ldr	r3, [fp, #-204]
	add	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r3, [r3, r2, asl #2]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
	b	.L55
.L54:
	.loc 1 567 0
	ldr	r2, [fp, #-160]
	ldr	r3, [fp, #-204]
	cmp	r2, r3
	bls	.L56
	.loc 1 569 0
	ldr	r3, [fp, #-204]
	sub	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r3, [r3, r2, asl #2]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 570 0
	ldr	r3, [fp, #-204]
	add	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-36]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bls	.L57
	.loc 1 572 0
	ldr	r3, [fp, #-204]
	add	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-36]
	rsb	r3, r3, r2
	str	r3, [fp, #-8]
.L57:
	.loc 1 575 0
	ldr	r3, [fp, #-204]
	add	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r3, [r3, r2, asl #2]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 576 0
	ldr	r3, [fp, #-204]
	sub	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-36]
	add	r2, r2, r3
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bcs	.L55
	.loc 1 578 0
	ldr	r3, [fp, #-204]
	sub	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-36]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	b	.L55
.L56:
	.loc 1 583 0
	ldr	r3, [fp, #-204]
	sub	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r3, [r3, r2, asl #2]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 584 0
	ldr	r3, [fp, #-204]
	sub	r2, r3, #1
	ldr	r3, .L65+44
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-36]
	add	r3, r2, r3
	str	r3, [fp, #-12]
.L55:
	.loc 1 587 0
	ldr	r3, [fp, #-204]
	mov	r2, r3, asl #2
	ldr	r3, .L65+44
	add	r3, r2, r3
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	ldr	r0, [fp, #-196]
	mov	r1, r3
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	bl	process_uns32
	.loc 1 590 0
	ldr	r3, .L65+44
	ldr	r2, [fp, #-204]
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #56
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #16
	mov	r2, r3
	mov	r3, #125
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, [fp, #-200]
	mov	r1, r3
	mov	r2, #16
	bl	strlcpy
	.loc 1 593 0
	ldr	r3, .L65+48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L58
	.loc 1 593 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bls	.L58
	.loc 1 595 0 is_stmt 1
	ldr	r0, .L65+52
	bl	DIALOG_draw_ok_dialog
	.loc 1 596 0
	ldr	r3, .L65+48
	mov	r2, #1
	str	r2, [r3, #0]
.L58:
	.loc 1 600 0
	ldr	r3, .L65+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L65+12
	mov	r3, #600
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 601 0
	ldr	r3, .L65+40
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L65+12
	ldr	r3, .L65+56
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 604 0
	bl	NETWORK_CONFIG_get_water_units
	mov	r3, r0
	cmp	r3, #0
	bne	.L59
	.loc 1 606 0
	ldr	r3, .L65	@ float
	str	r3, [fp, #-16]	@ float
	b	.L60
.L59:
	.loc 1 610 0
	flds	s15, [fp, #-40]
	flds	s14, .L65
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-16]
.L60:
	.loc 1 613 0
	ldr	r3, .L65+40
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 614 0
	ldr	r3, .L65+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 621 0
	mov	r3, #0
	str	r3, [fp, #-172]
	b	.L61
.L62:
	.loc 1 623 0 discriminator 2
	ldr	r3, [fp, #-172]
	ldr	r1, [fp, #-172]
	ldr	r2, .L65+44
	ldr	r2, [r2, r1, asl #2]
	add	r1, r3, #3
	mvn	r3, #163
	mov	r1, r1, asl #2
	sub	r0, fp, #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 1 621 0 discriminator 2
	ldr	r3, [fp, #-172]
	add	r3, r3, #1
	str	r3, [fp, #-172]
.L61:
	.loc 1 621 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-172]
	cmp	r3, #23
	bls	.L62
	.loc 1 627 0 is_stmt 1
	ldr	r3, .L65+60
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L63
	.loc 1 630 0
	sub	r3, fp, #192
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 631 0
	ldr	r0, [fp, #-32]
	bl	nm_GROUP_get_group_ID
	sub	r1, fp, #168
	sub	r2, fp, #192
	mov	r3, #0
	str	r3, [sp, #4]
	ldrh	r3, [fp, #-188]	@ movhi
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [fp, #-192]
	bl	nm_BUDGET_predicted_volume
	fmsr	s14, r0
	flds	s15, [fp, #-16]
	fmuls	s14, s14, s15
	ldr	r3, .L65+64
	flds	s15, [r3, #0]
	fadds	s15, s14, s15
	ldr	r3, .L65+68
	fsts	s15, [r3, #0]
	b	.L64
.L63:
	.loc 1 636 0
	ldr	r3, .L65+44
	ldr	r2, [fp, #-204]
	ldr	r1, [r3, r2, asl #2]
	ldr	r2, [fp, #-164]
	sub	r3, fp, #192
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	DateAndTimeToDTCS
	.loc 1 637 0
	ldr	r0, [fp, #-32]
	bl	nm_GROUP_get_group_ID
	sub	r1, fp, #168
	sub	r2, fp, #192
	mov	r3, #0
	str	r3, [sp, #4]
	ldrh	r3, [fp, #-188]	@ movhi
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [fp, #-192]
	bl	nm_BUDGET_predicted_volume
	fmsr	s14, r0
	flds	s15, [fp, #-16]
	fmuls	s15, s14, s15
	ldr	r3, .L65+68
	fsts	s15, [r3, #0]
.L64:
	.loc 1 640 0
	bl	BUDGETS_show_warnings
	.loc 1 641 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L66:
	.align	2
.L65:
	.word	1065353216
	.word	1144718131
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	523
	.word	list_poc_recursive_MUTEX
	.word	525
	.word	g_GROUP_list_item_index
	.word	system_preserves_recursive_MUTEX
	.word	531
	.word	poc_preserves_recursive_MUTEX
	.word	g_BUDGET_SETUP_meter_read_date
	.word	date_change_warning_flag
	.word	586
	.word	601
	.word	GuiVar_BudgetPeriodIdx
	.word	GuiVar_BudgetUseThisPeriod
	.word	GuiVar_BudgetExpectedUseThisPeriod
.LFE7:
	.size	BUDGET_SETUP_handle_start_date_change, .-BUDGET_SETUP_handle_start_date_change
	.section	.text.BUDGET_SETUP_handle_period_change,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_handle_period_change, %function
BUDGET_SETUP_handle_period_change:
.LFB8:
	.loc 1 647 0
	@ args = 0, pretend = 0, frame = 124
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #132
.LCFI26:
	str	r0, [fp, #-128]
	.loc 1 654 0
	ldr	r3, .L68
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_group_at_this_index
	str	r0, [fp, #-8]
	.loc 1 656 0
	ldr	r0, [fp, #-8]
	bl	POC_get_GID_irrigation_system
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_get_group_with_this_GID
	str	r0, [fp, #-12]
	.loc 1 658 0
	sub	r3, fp, #124
	ldr	r0, [fp, #-12]
	mov	r1, r3
	bl	SYSTEM_get_budget_details
	.loc 1 661 0
	ldr	r3, [fp, #-116]
	sub	r3, r3, #1
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	ldr	r0, [fp, #-128]
	ldr	r1, .L68+4
	mov	r2, #0
	bl	process_uns32
	.loc 1 664 0
	ldr	r3, .L68+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	BUDGETS_update_guivars
	.loc 1 665 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L69:
	.align	2
.L68:
	.word	g_GROUP_list_item_index
	.word	GuiVar_BudgetPeriodIdx
.LFE8:
	.size	BUDGET_SETUP_handle_period_change, .-BUDGET_SETUP_handle_period_change
	.section	.text.BUDGET_SETUP_handle_units_change,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_handle_units_change, %function
BUDGET_SETUP_handle_units_change:
.LFB9:
	.loc 1 670 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI27:
	add	fp, sp, #12
.LCFI28:
	sub	sp, sp, #16
.LCFI29:
	str	r0, [fp, #-20]
	.loc 1 676 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 680 0
	ldr	r3, [fp, #-20]
	cmp	r3, #4
	bne	.L71
	.loc 1 684 0
	ldr	r3, .L76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L72
	.loc 1 686 0
	ldr	r0, .L76
	bl	process_bool
	.loc 1 689 0
	ldr	r3, .L76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, .L76+4
	str	r2, [r3, #0]
	.loc 1 691 0
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L73
.L72:
	.loc 1 695 0
	bl	bad_key_beep
	b	.L73
.L71:
	.loc 1 702 0
	ldr	r3, .L76+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L74
	.loc 1 704 0
	ldr	r0, .L76+4
	bl	process_bool
	.loc 1 707 0
	ldr	r3, .L76+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, .L76
	str	r2, [r3, #0]
	.loc 1 709 0
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L73
.L74:
	.loc 1 713 0
	bl	bad_key_beep
.L73:
	.loc 1 719 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L70
	.loc 1 722 0
	ldr	r3, .L76
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r5, #0
	movne	r5, #1
	bl	FLOWSENSE_get_controller_index
	mov	r4, r0
	mov	r0, #2
	bl	NETWORK_CONFIG_get_change_bits_ptr
	mov	r3, r0
	mov	r2, #1
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r5
	mov	r1, #1
	mov	r2, #2
	mov	r3, r4
	bl	NETWORK_CONFIG_set_water_units
	.loc 1 732 0
	ldr	r3, .L76+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	BUDGETS_update_guivars
	.loc 1 733 0
	mov	r0, #0
	bl	Redraw_Screen
.L70:
	.loc 1 735 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L77:
	.align	2
.L76:
	.word	GuiVar_BudgetUnitGallon
	.word	GuiVar_BudgetUnitHCF
	.word	GuiVar_BudgetPeriodIdx
.LFE9:
	.size	BUDGET_SETUP_handle_units_change, .-BUDGET_SETUP_handle_units_change
	.section	.text.BUDGETS_process_NEXT_and_PREV,"ax",%progbits
	.align	2
	.type	BUDGETS_process_NEXT_and_PREV, %function
BUDGETS_process_NEXT_and_PREV:
.LFB10:
	.loc 1 740 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #8
.LCFI32:
	str	r0, [fp, #-12]
	.loc 1 743 0
	bl	POC_get_menu_index_for_displayed_poc
	str	r0, [fp, #-8]
	.loc 1 745 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	beq	.L79
	.loc 1 745 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #84
	bne	.L80
.L79:
	.loc 1 749 0 is_stmt 1
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.loc 1 751 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_to_line
	.loc 1 755 0
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bhi	.L81
	.loc 1 757 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 760 0
	ldr	r3, .L88
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L82
	.loc 1 762 0
	mov	r0, #0
	mov	r1, #0
	bl	SCROLL_BOX_up_or_down
	b	.L82
.L81:
	.loc 1 767 0
	bl	bad_key_beep
.L82:
	.loc 1 772 0
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	b	.L83
.L80:
	.loc 1 778 0
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.loc 1 780 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_to_line
	.loc 1 783 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L84
	.loc 1 785 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 788 0
	ldr	r3, .L88
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L85
	.loc 1 790 0
	mov	r0, #0
	mov	r1, #4
	bl	SCROLL_BOX_up_or_down
	b	.L85
.L84:
	.loc 1 795 0
	bl	bad_key_beep
.L85:
	.loc 1 800 0
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
.L83:
	.loc 1 807 0
	ldr	r3, .L88
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L86
	.loc 1 809 0
	bl	BUDGETS_extract_and_store_changes_from_GuiVars
	.loc 1 811 0
	ldr	r3, .L88
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L88+4
	str	r2, [r3, #0]
	.loc 1 813 0
	ldr	r3, .L88+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L88+12
	ldr	r3, .L88+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 816 0
	ldr	r3, .L88+4
	ldr	r2, [r3, #0]
	ldr	r3, .L88+20
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	BUDGETS_copy_group_into_guivars
	.loc 1 818 0
	ldr	r3, .L88+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	BUDGETS_update_guivars
	.loc 1 820 0
	ldr	r3, .L88+24
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 822 0
	ldr	r3, .L88+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L87
.L86:
	.loc 1 826 0
	bl	bad_key_beep
.L87:
	.loc 1 834 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 835 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L89:
	.align	2
.L88:
	.word	POC_MENU_items
	.word	g_GROUP_list_item_index
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	813
	.word	GuiVar_BudgetPeriodIdx
	.word	date_change_warning_flag
.LFE10:
	.size	BUDGETS_process_NEXT_and_PREV, .-BUDGETS_process_NEXT_and_PREV
	.section	.text.FDTO_BUDGETS_after_keypad,"ax",%progbits
	.align	2
	.type	FDTO_BUDGETS_after_keypad, %function
FDTO_BUDGETS_after_keypad:
.LFB11:
	.loc 1 842 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #8
.LCFI35:
	str	r0, [fp, #-12]
	.loc 1 844 0
	ldr	r3, .L96	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 845 0
	ldr	r3, .L96+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L91
	.loc 1 847 0
	ldr	r3, .L96+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L92
	.loc 1 849 0
	ldr	r3, .L96+12
	ldr	r3, [r3, #0]
	ldr	r2, .L96+16
	ldr	r2, [r2, #0]	@ float
	ldr	r1, .L96+20
	mov	r3, r3, asl #2
	add	r3, r1, r3
	str	r2, [r3, #0]	@ float
	b	.L93
.L92:
	.loc 1 853 0
	ldr	r3, .L96+12
	ldr	r3, [r3, #0]
	ldr	r2, .L96+16
	flds	s14, [r2, #0]
	flds	s15, [fp, #-8]
	fmuls	s15, s14, s15
	ldr	r2, .L96+20
	mov	r3, r3, asl #2
	add	r3, r2, r3
	fsts	s15, [r3, #0]
	b	.L93
.L91:
	.loc 1 859 0
	ldr	r3, .L96+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L94
	.loc 1 861 0
	ldr	r3, .L96+24
	ldr	r2, [r3, #0]	@ float
	ldr	r3, .L96+28
	str	r2, [r3, #0]	@ float
	b	.L95
.L94:
	.loc 1 865 0
	ldr	r3, .L96+24
	flds	s14, [r3, #0]
	flds	s15, [fp, #-8]
	fmuls	s15, s14, s15
	ldr	r3, .L96+28
	fsts	s15, [r3, #0]
.L95:
	.loc 1 868 0
	bl	BUDGET_SETUP_update_montly_budget
.L93:
	.loc 1 871 0
	ldr	r0, [fp, #-12]
	bl	FDTO_BUDGETS_draw_menu
	.loc 1 872 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L97:
	.align	2
.L96:
	.word	1144718131
	.word	GuiVar_BudgetEntryOptionIdx
	.word	GuiVar_BudgetUnitGallon
	.word	GuiVar_BudgetPeriodIdx
	.word	GuiVar_BudgetForThisPeriod
	.word	g_BUDGET_SETUP_budget
	.word	GuiVar_BudgetAnnualValue
	.word	g_BUDGET_SETUP_annual_budget
.LFE11:
	.size	FDTO_BUDGETS_after_keypad, .-FDTO_BUDGETS_after_keypad
	.section	.text.FDTO_POC_show_budget_period_idx_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_period_idx_dropdown, %function
FDTO_POC_show_budget_period_idx_dropdown:
.LFB12:
	.loc 1 876 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	.loc 1 877 0
	ldr	r3, .L99
	ldr	r3, [r3, #0]
	ldr	r0, .L99+4
	ldr	r1, .L99+8
	mov	r2, #12
	bl	FDTO_COMBOBOX_show
	.loc 1 878 0
	ldmfd	sp!, {fp, pc}
.L100:
	.align	2
.L99:
	.word	GuiVar_BudgetPeriodIdx
	.word	723
	.word	FDTO_COMBOBOX_add_items
.LFE12:
	.size	FDTO_POC_show_budget_period_idx_dropdown, .-FDTO_POC_show_budget_period_idx_dropdown
	.section	.text.FDTO_POC_close_budget_period_idx_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_budget_period_idx_dropdown, %function
FDTO_POC_close_budget_period_idx_dropdown:
.LFB13:
	.loc 1 881 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI38:
	add	fp, sp, #4
.LCFI39:
	.loc 1 882 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L102
	str	r2, [r3, #0]
	.loc 1 883 0
	bl	FDTO_COMBOBOX_hide
	.loc 1 885 0
	ldr	r3, .L102
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	BUDGETS_update_guivars
	.loc 1 886 0
	ldmfd	sp!, {fp, pc}
.L103:
	.align	2
.L102:
	.word	GuiVar_BudgetPeriodIdx
.LFE13:
	.size	FDTO_POC_close_budget_period_idx_dropdown, .-FDTO_POC_close_budget_period_idx_dropdown
	.section	.text.BUDGETS_process_group,"ax",%progbits
	.align	2
	.type	BUDGETS_process_group, %function
BUDGETS_process_group:
.LFB14:
	.loc 1 906 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI40:
	add	fp, sp, #4
.LCFI41:
	sub	sp, sp, #48
.LCFI42:
	str	r0, [fp, #-52]
	str	r1, [fp, #-48]
	.loc 1 908 0
	ldr	r3, .L174+8	@ float
	str	r3, [fp, #-8]	@ float
	.loc 1 912 0
	ldr	r3, .L174+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #616
	beq	.L106
	ldr	r2, .L174+16
	cmp	r3, r2
	beq	.L107
	b	.L169
.L106:
	.loc 1 921 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	ldr	r2, .L174+20
	bl	NUMERIC_KEYPAD_process_key
	.loc 1 922 0
	b	.L104
.L107:
	.loc 1 925 0
	ldr	r3, [fp, #-52]
	cmp	r3, #2
	beq	.L109
	.loc 1 925 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-52]
	cmp	r3, #67
	bne	.L110
.L109:
	.loc 1 927 0 is_stmt 1
	bl	good_key_beep
	.loc 1 928 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 929 0
	ldr	r3, .L174+24
	str	r3, [fp, #-24]
	.loc 1 930 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 936 0
	b	.L104
.L110:
	.loc 1 934 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	mov	r1, #0
	bl	COMBO_BOX_key_press
	.loc 1 936 0
	b	.L104
.L169:
	.loc 1 939 0
	ldr	r3, [fp, #-52]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L112
.L121:
	.word	.L113
	.word	.L114
	.word	.L115
	.word	.L116
	.word	.L117
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L118
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L118
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L119
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L120
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L120
.L115:
	.loc 1 942 0
	ldr	r3, .L174+28
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L122
.L127:
	.word	.L123
	.word	.L122
	.word	.L122
	.word	.L124
	.word	.L125
	.word	.L126
.L123:
	.loc 1 947 0
	ldr	r3, .L174+32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L128
	.loc 1 949 0
	bl	good_key_beep
	.loc 1 950 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 951 0
	ldr	r3, .L174+36
	str	r3, [fp, #-24]
	.loc 1 952 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 972 0
	b	.L131
.L128:
	.loc 1 956 0
	bl	good_key_beep
	.loc 1 957 0
	ldr	r3, .L174+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L130
	.loc 1 959 0
	ldr	r0, .L174+44
	ldr	r1, .L174+48	@ float
	ldr	r2, .L174	@ float
	mov	r3, #0
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 972 0
	b	.L131
.L130:
	.loc 1 966 0
	flds	s15, [fp, #-8]
	flds	s14, .L174
	fdivs	s15, s14, s15
	ldr	r0, .L174+44
	ldr	r1, .L174+48	@ float
	fmrs	r2, s15
	mov	r3, #1
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 972 0
	b	.L131
.L124:
	.loc 1 974 0
	bl	good_key_beep
	.loc 1 975 0
	ldr	r3, .L174+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L132
	.loc 1 977 0
	ldr	r0, .L174+52
	ldr	r1, .L174+48	@ float
	ldr	r2, .L174+4	@ float
	mov	r3, #0
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 989 0
	b	.L131
.L132:
	.loc 1 984 0
	flds	s15, [fp, #-8]
	flds	s14, .L174+4
	fdivs	s15, s14, s15
	ldr	r0, .L174+52
	ldr	r1, .L174+48	@ float
	fmrs	r2, s15
	mov	r3, #1
	bl	NUMERIC_KEYPAD_draw_float_keypad
	.loc 1 989 0
	b	.L131
.L125:
	.loc 1 992 0
	mov	r0, #4
	bl	BUDGET_SETUP_handle_units_change
	.loc 1 993 0
	b	.L131
.L126:
	.loc 1 996 0
	mov	r0, #5
	bl	BUDGET_SETUP_handle_units_change
	.loc 1 997 0
	b	.L131
.L122:
	.loc 1 1000 0
	bl	bad_key_beep
.L131:
	.loc 1 1002 0
	bl	Refresh_Screen
	.loc 1 1003 0
	b	.L104
.L120:
	.loc 1 1007 0
	ldr	r3, .L174+28
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L134
.L141:
	.word	.L135
	.word	.L136
	.word	.L137
	.word	.L138
	.word	.L139
	.word	.L140
.L135:
	.loc 1 1012 0
	ldr	r3, .L174+32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L142
	.loc 1 1014 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	BUDGET_SETUP_handle_annual_budget_change
	.loc 1 1020 0
	b	.L144
.L142:
	.loc 1 1018 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	BUDGET_SETUP_handle_period_change
	.loc 1 1020 0
	b	.L144
.L136:
	.loc 1 1023 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L174+56
	ldr	r3, [r3, #0]
	mov	r0, r2
	ldr	r1, .L174+60
	mov	r2, r3
	bl	BUDGET_SETUP_handle_start_date_change
	.loc 1 1024 0
	b	.L144
.L137:
	.loc 1 1027 0
	ldr	r2, [fp, #-52]
	ldr	r3, .L174+56
	ldr	r3, [r3, #0]
	add	r3, r3, #1
	mov	r0, r2
	ldr	r1, .L174+64
	mov	r2, r3
	bl	BUDGET_SETUP_handle_start_date_change
	.loc 1 1028 0
	b	.L144
.L138:
	.loc 1 1031 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	BUDGET_SETUP_handle_budget_change
	.loc 1 1032 0
	b	.L144
.L139:
	.loc 1 1035 0
	mov	r0, #4
	bl	BUDGET_SETUP_handle_units_change
	.loc 1 1036 0
	b	.L144
.L140:
	.loc 1 1039 0
	mov	r0, #5
	bl	BUDGET_SETUP_handle_units_change
	.loc 1 1040 0
	b	.L144
.L134:
	.loc 1 1043 0
	bl	bad_key_beep
.L144:
	.loc 1 1045 0
	bl	Refresh_Screen
	.loc 1 1046 0
	b	.L104
.L118:
	.loc 1 1050 0
	ldr	r3, [fp, #-52]
	mov	r0, r3
	bl	BUDGETS_process_NEXT_and_PREV
	.loc 1 1051 0
	b	.L104
.L117:
	.loc 1 1054 0
	ldr	r3, .L174+28
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #4
	beq	.L146
	cmp	r3, #5
	beq	.L147
	b	.L170
.L146:
	.loc 1 1058 0
	ldr	r3, .L174+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L148
	.loc 1 1060 0
	bl	GuiLib_Cursor_Hide
	.loc 1 1061 0
	ldr	r3, .L174+28
	mov	r2, #1
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1062 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 1068 0
	b	.L150
.L148:
	.loc 1 1066 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 1068 0
	b	.L150
.L147:
	.loc 1 1072 0
	ldr	r3, .L174+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L151
	.loc 1 1074 0
	mov	r0, #3
	mov	r1, #1
	bl	FDTO_Cursor_Select
	.loc 1 1083 0
	b	.L150
.L151:
	.loc 1 1079 0
	bl	GuiLib_Cursor_Hide
	.loc 1 1080 0
	ldr	r3, .L174+28
	mov	r2, #1
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1081 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 1083 0
	b	.L150
.L170:
	.loc 1 1086 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 1088 0
	b	.L104
.L150:
	b	.L104
.L114:
	.loc 1 1091 0
	ldr	r3, .L174+28
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	beq	.L154
	cmp	r3, #4
	bne	.L171
.L155:
	.loc 1 1095 0
	ldr	r3, .L174+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L156
	.loc 1 1097 0
	bl	GuiLib_Cursor_Hide
	.loc 1 1098 0
	ldr	r3, .L174+28
	mov	r2, #1
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1099 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 1105 0
	b	.L158
.L156:
	.loc 1 1103 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 1105 0
	b	.L158
.L154:
	.loc 1 1110 0
	ldr	r0, .L174+68
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 1111 0
	b	.L158
.L171:
	.loc 1 1114 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 1116 0
	b	.L104
.L158:
	b	.L104
.L113:
	.loc 1 1119 0
	ldr	r3, .L174+28
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L172
.L160:
	.loc 1 1124 0
	ldr	r3, .L174+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L161
	.loc 1 1126 0
	ldr	r3, .L174+28
	mov	r2, #3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1127 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 1133 0
	b	.L163
.L161:
	.loc 1 1131 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 1133 0
	b	.L163
.L172:
	.loc 1 1136 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 1138 0
	b	.L104
.L163:
	b	.L104
.L116:
	.loc 1 1141 0
	ldr	r3, .L174+28
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L173
.L165:
	.loc 1 1146 0
	ldr	r3, .L174+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L166
	.loc 1 1148 0
	ldr	r3, .L174+28
	mov	r2, #3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1149 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 1155 0
	b	.L168
.L166:
	.loc 1 1153 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 1155 0
	b	.L168
.L173:
	.loc 1 1158 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 1160 0
	b	.L104
.L168:
	b	.L104
.L119:
	.loc 1 1163 0
	ldr	r0, .L174+68
	bl	KEY_process_BACK_from_editing_screen
	.loc 1 1164 0
	b	.L104
.L112:
	.loc 1 1167 0
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L104:
	.loc 1 1170 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L175:
	.align	2
.L174:
	.word	1317997848
	.word	1287568416
	.word	1144718131
	.word	GuiLib_CurStructureNdx
	.word	723
	.word	FDTO_BUDGETS_after_keypad
	.word	FDTO_POC_close_budget_period_idx_dropdown
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_BudgetEntryOptionIdx
	.word	FDTO_POC_show_budget_period_idx_dropdown
	.word	GuiVar_BudgetUnitGallon
	.word	GuiVar_BudgetAnnualValue
	.word	0
	.word	GuiVar_BudgetForThisPeriod
	.word	GuiVar_BudgetPeriodIdx
	.word	GuiVar_BudgetPeriodStart
	.word	GuiVar_BudgetPeriodEnd
	.word	FDTO_BUDGETS_return_to_menu
.LFE14:
	.size	BUDGETS_process_group, .-BUDGETS_process_group
	.section	.text.FDTO_BUDGETS_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_BUDGETS_return_to_menu, %function
FDTO_BUDGETS_return_to_menu:
.LFB15:
	.loc 1 1189 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI43:
	add	fp, sp, #4
.LCFI44:
	.loc 1 1190 0
	ldr	r3, .L177
	ldr	r0, .L177+4
	mov	r1, r3
	bl	FDTO_GROUP_return_to_menu
	.loc 1 1191 0
	ldmfd	sp!, {fp, pc}
.L178:
	.align	2
.L177:
	.word	BUDGETS_extract_and_store_changes_from_GuiVars
	.word	g_POC_editing_group
.LFE15:
	.size	FDTO_BUDGETS_return_to_menu, .-FDTO_BUDGETS_return_to_menu
	.section	.text.FDTO_BUDGETS_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_BUDGETS_draw_menu
	.type	FDTO_BUDGETS_draw_menu, %function
FDTO_BUDGETS_draw_menu:
.LFB16:
	.loc 1 1198 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI45:
	add	fp, sp, #4
.LCFI46:
	sub	sp, sp, #20
.LCFI47:
	str	r0, [fp, #-20]
	.loc 1 1201 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1203 0
	ldr	r3, .L184
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L184+4
	ldr	r3, .L184+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1205 0
	mov	r0, #1
	bl	POC_populate_pointers_of_POCs_for_display
	str	r0, [fp, #-12]
	.loc 1 1207 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L180
	.loc 1 1209 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1210 0
	ldr	r3, .L184+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1211 0
	ldr	r3, .L184+16
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1212 0
	ldr	r3, .L184+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1213 0
	ldr	r3, .L184+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_ptr_to_physically_available_poc
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L184+24
	str	r2, [r3, #0]
	.loc 1 1214 0
	ldr	r3, .L184+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	bl	BUDGETS_copy_group_into_guivars
	.loc 1 1215 0
	mov	r0, #0
	bl	BUDGETS_update_guivars
	.loc 1 1216 0
	ldr	r3, .L184+28
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L181
.L180:
	.loc 1 1220 0
	ldr	r3, .L184+32
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
	.loc 1 1221 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_GetTopLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L184+12
	str	r2, [r3, #0]
.L181:
	.loc 1 1224 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #9
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 1225 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	.loc 1 1226 0
	bl	POC_get_menu_index_for_displayed_poc
	str	r0, [fp, #-16]
	.loc 1 1228 0
	ldr	r3, .L184+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L182
	.loc 1 1232 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r2, .L184+12
	ldr	r2, [r2, #0]
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	mov	r2, r2, asl #16
	mov	r2, r2, asr #16
	str	r2, [sp, #0]
	mov	r0, #0
	ldr	r1, .L184+36
	mov	r2, r3
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	.loc 1 1233 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	mov	r1, r3
	bl	GuiLib_ScrollBox_SetIndicator
	.loc 1 1234 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
	b	.L183
.L182:
	.loc 1 1238 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r1, .L184+12
	ldr	r1, [r1, #0]
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r1, r1, asl #16
	mov	r1, r1, asr #16
	str	r1, [sp, #0]
	mov	r0, #0
	ldr	r1, .L184+36
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L183:
	.loc 1 1241 0
	ldr	r3, .L184
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1243 0
	bl	GuiLib_Refresh
	.loc 1 1244 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L185:
	.align	2
.L184:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	1203
	.word	g_POC_top_line
	.word	g_POC_editing_group
	.word	g_POC_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	date_change_warning_flag
	.word	GuiLib_ActiveCursorFieldNo
	.word	POC_load_poc_name_into_guivar
.LFE16:
	.size	FDTO_BUDGETS_draw_menu, .-FDTO_BUDGETS_draw_menu
	.section	.text.BUDGETS_process_menu,"ax",%progbits
	.align	2
	.global	BUDGETS_process_menu
	.type	BUDGETS_process_menu, %function
BUDGETS_process_menu:
.LFB17:
	.loc 1 1264 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI48:
	add	fp, sp, #4
.LCFI49:
	sub	sp, sp, #8
.LCFI50:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 1265 0
	ldr	r3, .L195
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L187
	.loc 1 1267 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	BUDGETS_process_group
	b	.L186
.L187:
	.loc 1 1271 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	bhi	.L189
	mov	r2, #1
	mov	r3, r2, asl r3
	and	r2, r3, #1114112
	cmp	r2, #0
	bne	.L192
	and	r2, r3, #17
	cmp	r2, #0
	bne	.L190
	and	r3, r3, #12
	cmp	r3, #0
	beq	.L189
.L191:
	.loc 1 1275 0
	bl	good_key_beep
	.loc 1 1277 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L195+4
	str	r2, [r3, #0]
	.loc 1 1278 0
	ldr	r3, .L195+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_ptr_to_physically_available_poc
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L195+8
	str	r2, [r3, #0]
	.loc 1 1279 0
	ldr	r3, .L195
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 1280 0
	ldr	r3, .L195+12
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1281 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 1282 0
	b	.L186
.L192:
	.loc 1 1288 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	bne	.L193
	.loc 1 1290 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L190
.L193:
	.loc 1 1294 0
	mov	r3, #4
	str	r3, [fp, #-12]
.L190:
	.loc 1 1302 0
	ldr	r3, [fp, #-12]
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	.loc 1 1304 0
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	mov	r2, r3
	ldr	r3, .L195+4
	str	r2, [r3, #0]
	.loc 1 1305 0
	ldr	r3, .L195+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	POC_get_ptr_to_physically_available_poc
	mov	r3, r0
	mov	r0, r3
	bl	POC_get_index_using_ptr_to_poc_struct
	mov	r2, r0
	ldr	r3, .L195+8
	str	r2, [r3, #0]
	.loc 1 1306 0
	ldr	r3, .L195+8
	ldr	r2, [r3, #0]
	ldr	r3, .L195+16
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	BUDGETS_copy_group_into_guivars
	.loc 1 1307 0
	ldr	r3, .L195+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	BUDGETS_update_guivars
	.loc 1 1308 0
	ldr	r3, .L195+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1314 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 1315 0
	b	.L186
.L189:
	.loc 1 1318 0
	ldr	r3, [fp, #-12]
	cmp	r3, #67
	bne	.L194
	.loc 1 1320 0
	ldr	r3, .L195+24
	ldr	r2, [r3, #0]
	ldr	r0, .L195+28
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L195+32
	str	r2, [r3, #0]
.L194:
	.loc 1 1323 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L186:
	.loc 1 1326 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L196:
	.align	2
.L195:
	.word	g_POC_editing_group
	.word	g_POC_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_BudgetPeriodIdx
	.word	date_change_warning_flag
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE17:
	.size	BUDGETS_process_menu, .-BUDGETS_process_menu
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x10
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI38-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI40-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI43-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI44-.LCFI43
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI45-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI48-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_defs.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/poc.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/flow_recorder.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_poc.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 19 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.file 20 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 21 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_budgets.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1da4
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF363
	.byte	0x1
	.4byte	.LASF364
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x55
	.4byte	0x69
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x5e
	.4byte	0x7b
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x67
	.4byte	0x8d
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x70
	.4byte	0x9f
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x2
	.byte	0x99
	.4byte	0x7b
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x2
	.byte	0x9d
	.4byte	0x7b
	.uleb128 0x5
	.byte	0x4
	.4byte	0xc9
	.uleb128 0x6
	.4byte	0xd0
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF17
	.uleb128 0x8
	.byte	0x6
	.byte	0x3
	.byte	0x22
	.4byte	0xf8
	.uleb128 0x9
	.ascii	"T\000"
	.byte	0x3
	.byte	0x24
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.ascii	"D\000"
	.byte	0x3
	.byte	0x26
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x3
	.byte	0x28
	.4byte	0xd7
	.uleb128 0x8
	.byte	0x14
	.byte	0x3
	.byte	0x31
	.4byte	0x18a
	.uleb128 0xa
	.4byte	.LASF19
	.byte	0x3
	.byte	0x33
	.4byte	0xf8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF20
	.byte	0x3
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x3
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x3
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x3
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x3
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0xa
	.4byte	.LASF25
	.byte	0x3
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF26
	.byte	0x3
	.byte	0x39
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x3
	.byte	0x3b
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x3
	.byte	0x3d
	.4byte	0x103
	.uleb128 0xb
	.byte	0x4
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x4
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF30
	.byte	0x5
	.byte	0x57
	.4byte	0x195
	.uleb128 0x3
	.4byte	.LASF31
	.byte	0x6
	.byte	0x4c
	.4byte	0x1a2
	.uleb128 0x3
	.4byte	.LASF32
	.byte	0x7
	.byte	0x65
	.4byte	0x195
	.uleb128 0xc
	.4byte	0x3e
	.4byte	0x1d3
	.uleb128 0xd
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xc
	.4byte	0x70
	.4byte	0x1e3
	.uleb128 0xd
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xc
	.4byte	0x2c
	.4byte	0x1f3
	.uleb128 0xd
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0xe
	.byte	0x8
	.byte	0x8
	.2byte	0x163
	.4byte	0x4a9
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x8
	.2byte	0x16b
	.4byte	0x70
	.byte	0x4
	.byte	0x4
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF34
	.byte	0x8
	.2byte	0x171
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF35
	.byte	0x8
	.2byte	0x17c
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x185
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF37
	.byte	0x8
	.2byte	0x19b
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF38
	.byte	0x8
	.2byte	0x19d
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF39
	.byte	0x8
	.2byte	0x19f
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF40
	.byte	0x8
	.2byte	0x1a1
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF41
	.byte	0x8
	.2byte	0x1a3
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF42
	.byte	0x8
	.2byte	0x1a5
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF43
	.byte	0x8
	.2byte	0x1a7
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF44
	.byte	0x8
	.2byte	0x1b1
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF45
	.byte	0x8
	.2byte	0x1b6
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF46
	.byte	0x8
	.2byte	0x1bb
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF47
	.byte	0x8
	.2byte	0x1c7
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF48
	.byte	0x8
	.2byte	0x1cd
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF49
	.byte	0x8
	.2byte	0x1d6
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF50
	.byte	0x8
	.2byte	0x1d8
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF51
	.byte	0x8
	.2byte	0x1e6
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0xa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF52
	.byte	0x8
	.2byte	0x1e7
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF53
	.byte	0x8
	.2byte	0x1e8
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF54
	.byte	0x8
	.2byte	0x1e9
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x1ea
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF56
	.byte	0x8
	.2byte	0x1eb
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF57
	.byte	0x8
	.2byte	0x1ec
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF58
	.byte	0x8
	.2byte	0x1f6
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF59
	.byte	0x8
	.2byte	0x1f7
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF60
	.byte	0x8
	.2byte	0x1f8
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF61
	.byte	0x8
	.2byte	0x1f9
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF62
	.byte	0x8
	.2byte	0x1fa
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF63
	.byte	0x8
	.2byte	0x1fb
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF64
	.byte	0x8
	.2byte	0x1fc
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF65
	.byte	0x8
	.2byte	0x206
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF66
	.byte	0x8
	.2byte	0x20d
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF67
	.byte	0x8
	.2byte	0x214
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF68
	.byte	0x8
	.2byte	0x216
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF69
	.byte	0x8
	.2byte	0x223
	.4byte	0x70
	.byte	0x4
	.byte	0x6
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xf
	.4byte	.LASF70
	.byte	0x8
	.2byte	0x227
	.4byte	0x70
	.byte	0x4
	.byte	0x4
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x10
	.byte	0x8
	.byte	0x8
	.2byte	0x15f
	.4byte	0x4c4
	.uleb128 0x11
	.4byte	.LASF236
	.byte	0x8
	.2byte	0x161
	.4byte	0x94
	.uleb128 0x12
	.4byte	0x1f3
	.byte	0
	.uleb128 0xe
	.byte	0x8
	.byte	0x8
	.2byte	0x15d
	.4byte	0x4d6
	.uleb128 0x13
	.4byte	0x4a9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x14
	.4byte	.LASF71
	.byte	0x8
	.2byte	0x230
	.4byte	0x4c4
	.uleb128 0x5
	.byte	0x4
	.4byte	0x33
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF72
	.byte	0x9
	.byte	0xda
	.4byte	0x4f9
	.uleb128 0x15
	.4byte	.LASF72
	.byte	0x1
	.uleb128 0x8
	.byte	0x8
	.byte	0xa
	.byte	0x7c
	.4byte	0x524
	.uleb128 0xa
	.4byte	.LASF73
	.byte	0xa
	.byte	0x7e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF74
	.byte	0xa
	.byte	0x80
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF75
	.byte	0xa
	.byte	0x82
	.4byte	0x4ff
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF76
	.uleb128 0xc
	.4byte	0x70
	.4byte	0x546
	.uleb128 0xd
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.byte	0x1c
	.byte	0xb
	.byte	0x8f
	.4byte	0x5b1
	.uleb128 0xa
	.4byte	.LASF77
	.byte	0xb
	.byte	0x94
	.4byte	0x4e2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF78
	.byte	0xb
	.byte	0x99
	.4byte	0x4e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF79
	.byte	0xb
	.byte	0x9e
	.4byte	0x4e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF80
	.byte	0xb
	.byte	0xa3
	.4byte	0x4e2
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF81
	.byte	0xb
	.byte	0xad
	.4byte	0x4e2
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF82
	.byte	0xb
	.byte	0xb8
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xa
	.4byte	.LASF83
	.byte	0xb
	.byte	0xbe
	.4byte	0x1b8
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x3
	.4byte	.LASF84
	.byte	0xb
	.byte	0xc2
	.4byte	0x546
	.uleb128 0x3
	.4byte	.LASF85
	.byte	0xc
	.byte	0xd0
	.4byte	0x5c7
	.uleb128 0x15
	.4byte	.LASF85
	.byte	0x1
	.uleb128 0xe
	.byte	0x70
	.byte	0xc
	.2byte	0x19b
	.4byte	0x622
	.uleb128 0x16
	.4byte	.LASF86
	.byte	0xc
	.2byte	0x19d
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF87
	.byte	0xc
	.2byte	0x19e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF88
	.byte	0xc
	.2byte	0x19f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x16
	.4byte	.LASF89
	.byte	0xc
	.2byte	0x1a0
	.4byte	0x622
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x16
	.4byte	.LASF90
	.byte	0xc
	.2byte	0x1a1
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.byte	0
	.uleb128 0xc
	.4byte	0x70
	.4byte	0x632
	.uleb128 0xd
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x14
	.4byte	.LASF91
	.byte	0xc
	.2byte	0x1a2
	.4byte	0x5cd
	.uleb128 0xe
	.byte	0x10
	.byte	0xd
	.2byte	0x366
	.4byte	0x6de
	.uleb128 0x16
	.4byte	.LASF92
	.byte	0xd
	.2byte	0x379
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF93
	.byte	0xd
	.2byte	0x37b
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x16
	.4byte	.LASF94
	.byte	0xd
	.2byte	0x37d
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x16
	.4byte	.LASF95
	.byte	0xd
	.2byte	0x381
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.uleb128 0x16
	.4byte	.LASF96
	.byte	0xd
	.2byte	0x387
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF97
	.byte	0xd
	.2byte	0x388
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x16
	.4byte	.LASF98
	.byte	0xd
	.2byte	0x38a
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x16
	.4byte	.LASF99
	.byte	0xd
	.2byte	0x38b
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x16
	.4byte	.LASF100
	.byte	0xd
	.2byte	0x38d
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x16
	.4byte	.LASF101
	.byte	0xd
	.2byte	0x38e
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.byte	0
	.uleb128 0x14
	.4byte	.LASF102
	.byte	0xd
	.2byte	0x390
	.4byte	0x63e
	.uleb128 0xe
	.byte	0x4c
	.byte	0xd
	.2byte	0x39b
	.4byte	0x802
	.uleb128 0x16
	.4byte	.LASF103
	.byte	0xd
	.2byte	0x39f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF104
	.byte	0xd
	.2byte	0x3a8
	.4byte	0xf8
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF105
	.byte	0xd
	.2byte	0x3aa
	.4byte	0xf8
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x16
	.4byte	.LASF106
	.byte	0xd
	.2byte	0x3b1
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x16
	.4byte	.LASF107
	.byte	0xd
	.2byte	0x3b7
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x16
	.4byte	.LASF108
	.byte	0xd
	.2byte	0x3b8
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x16
	.4byte	.LASF109
	.byte	0xd
	.2byte	0x3ba
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x16
	.4byte	.LASF110
	.byte	0xd
	.2byte	0x3bb
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x16
	.4byte	.LASF111
	.byte	0xd
	.2byte	0x3bd
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x16
	.4byte	.LASF112
	.byte	0xd
	.2byte	0x3be
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x16
	.4byte	.LASF113
	.byte	0xd
	.2byte	0x3c0
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x16
	.4byte	.LASF114
	.byte	0xd
	.2byte	0x3c1
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x16
	.4byte	.LASF115
	.byte	0xd
	.2byte	0x3c3
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x16
	.4byte	.LASF116
	.byte	0xd
	.2byte	0x3c4
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x16
	.4byte	.LASF117
	.byte	0xd
	.2byte	0x3c6
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x16
	.4byte	.LASF118
	.byte	0xd
	.2byte	0x3c7
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x16
	.4byte	.LASF119
	.byte	0xd
	.2byte	0x3c9
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x16
	.4byte	.LASF120
	.byte	0xd
	.2byte	0x3ca
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.byte	0
	.uleb128 0x14
	.4byte	.LASF121
	.byte	0xd
	.2byte	0x3d1
	.4byte	0x6ea
	.uleb128 0xe
	.byte	0x28
	.byte	0xd
	.2byte	0x3d4
	.4byte	0x8ae
	.uleb128 0x16
	.4byte	.LASF103
	.byte	0xd
	.2byte	0x3d6
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF86
	.byte	0xd
	.2byte	0x3d8
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF90
	.byte	0xd
	.2byte	0x3d9
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x16
	.4byte	.LASF122
	.byte	0xd
	.2byte	0x3db
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x16
	.4byte	.LASF123
	.byte	0xd
	.2byte	0x3dc
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x16
	.4byte	.LASF87
	.byte	0xd
	.2byte	0x3dd
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x16
	.4byte	.LASF124
	.byte	0xd
	.2byte	0x3e0
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x16
	.4byte	.LASF125
	.byte	0xd
	.2byte	0x3e3
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x16
	.4byte	.LASF126
	.byte	0xd
	.2byte	0x3f5
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x16
	.4byte	.LASF127
	.byte	0xd
	.2byte	0x3fa
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x14
	.4byte	.LASF128
	.byte	0xd
	.2byte	0x401
	.4byte	0x80e
	.uleb128 0xe
	.byte	0x30
	.byte	0xd
	.2byte	0x404
	.4byte	0x8f1
	.uleb128 0x17
	.ascii	"rip\000"
	.byte	0xd
	.2byte	0x406
	.4byte	0x8ae
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF129
	.byte	0xd
	.2byte	0x409
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x16
	.4byte	.LASF130
	.byte	0xd
	.2byte	0x40c
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.byte	0
	.uleb128 0x14
	.4byte	.LASF131
	.byte	0xd
	.2byte	0x40e
	.4byte	0x8ba
	.uleb128 0x18
	.2byte	0x3790
	.byte	0xd
	.2byte	0x418
	.4byte	0xd7a
	.uleb128 0x16
	.4byte	.LASF103
	.byte	0xd
	.2byte	0x420
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x17
	.ascii	"rip\000"
	.byte	0xd
	.2byte	0x425
	.4byte	0x802
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF132
	.byte	0xd
	.2byte	0x42f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x16
	.4byte	.LASF133
	.byte	0xd
	.2byte	0x442
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x16
	.4byte	.LASF134
	.byte	0xd
	.2byte	0x44e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x16
	.4byte	.LASF135
	.byte	0xd
	.2byte	0x458
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x16
	.4byte	.LASF136
	.byte	0xd
	.2byte	0x473
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x16
	.4byte	.LASF137
	.byte	0xd
	.2byte	0x47d
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x16
	.4byte	.LASF138
	.byte	0xd
	.2byte	0x499
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x16
	.4byte	.LASF139
	.byte	0xd
	.2byte	0x49d
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x16
	.4byte	.LASF140
	.byte	0xd
	.2byte	0x49f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x16
	.4byte	.LASF141
	.byte	0xd
	.2byte	0x4a9
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x16
	.4byte	.LASF142
	.byte	0xd
	.2byte	0x4ad
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x16
	.4byte	.LASF143
	.byte	0xd
	.2byte	0x4af
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x16
	.4byte	.LASF144
	.byte	0xd
	.2byte	0x4b3
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x16
	.4byte	.LASF145
	.byte	0xd
	.2byte	0x4b5
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x16
	.4byte	.LASF146
	.byte	0xd
	.2byte	0x4b7
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x16
	.4byte	.LASF147
	.byte	0xd
	.2byte	0x4bc
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x16
	.4byte	.LASF148
	.byte	0xd
	.2byte	0x4be
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x16
	.4byte	.LASF149
	.byte	0xd
	.2byte	0x4c1
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x16
	.4byte	.LASF150
	.byte	0xd
	.2byte	0x4c3
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x16
	.4byte	.LASF151
	.byte	0xd
	.2byte	0x4cc
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x16
	.4byte	.LASF152
	.byte	0xd
	.2byte	0x4cf
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x16
	.4byte	.LASF153
	.byte	0xd
	.2byte	0x4d1
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x16
	.4byte	.LASF154
	.byte	0xd
	.2byte	0x4d9
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x16
	.4byte	.LASF155
	.byte	0xd
	.2byte	0x4e3
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x16
	.4byte	.LASF156
	.byte	0xd
	.2byte	0x4e5
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x16
	.4byte	.LASF157
	.byte	0xd
	.2byte	0x4e9
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0x16
	.4byte	.LASF158
	.byte	0xd
	.2byte	0x4eb
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0x16
	.4byte	.LASF159
	.byte	0xd
	.2byte	0x4ed
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xbc
	.uleb128 0x16
	.4byte	.LASF160
	.byte	0xd
	.2byte	0x4f4
	.4byte	0x536
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x16
	.4byte	.LASF161
	.byte	0xd
	.2byte	0x4fe
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x16
	.4byte	.LASF162
	.byte	0xd
	.2byte	0x504
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0x16
	.4byte	.LASF163
	.byte	0xd
	.2byte	0x50c
	.4byte	0xd7a
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0x16
	.4byte	.LASF164
	.byte	0xd
	.2byte	0x512
	.4byte	0x52f
	.byte	0x3
	.byte	0x23
	.uleb128 0x128
	.uleb128 0x16
	.4byte	.LASF165
	.byte	0xd
	.2byte	0x515
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x12c
	.uleb128 0x16
	.4byte	.LASF166
	.byte	0xd
	.2byte	0x519
	.4byte	0x52f
	.byte	0x3
	.byte	0x23
	.uleb128 0x130
	.uleb128 0x16
	.4byte	.LASF167
	.byte	0xd
	.2byte	0x51e
	.4byte	0x52f
	.byte	0x3
	.byte	0x23
	.uleb128 0x134
	.uleb128 0x16
	.4byte	.LASF168
	.byte	0xd
	.2byte	0x524
	.4byte	0xd8a
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.uleb128 0x16
	.4byte	.LASF169
	.byte	0xd
	.2byte	0x52b
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1b0
	.uleb128 0x16
	.4byte	.LASF170
	.byte	0xd
	.2byte	0x536
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1b4
	.uleb128 0x16
	.4byte	.LASF171
	.byte	0xd
	.2byte	0x538
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1b8
	.uleb128 0x16
	.4byte	.LASF172
	.byte	0xd
	.2byte	0x53e
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1bc
	.uleb128 0x16
	.4byte	.LASF173
	.byte	0xd
	.2byte	0x54a
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c0
	.uleb128 0x16
	.4byte	.LASF174
	.byte	0xd
	.2byte	0x54c
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c4
	.uleb128 0x16
	.4byte	.LASF175
	.byte	0xd
	.2byte	0x555
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c8
	.uleb128 0x16
	.4byte	.LASF176
	.byte	0xd
	.2byte	0x55f
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x1cc
	.uleb128 0x17
	.ascii	"sbf\000"
	.byte	0xd
	.2byte	0x566
	.4byte	0x4d6
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d0
	.uleb128 0x16
	.4byte	.LASF177
	.byte	0xd
	.2byte	0x573
	.4byte	0x5b1
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d8
	.uleb128 0x16
	.4byte	.LASF178
	.byte	0xd
	.2byte	0x578
	.4byte	0x6de
	.byte	0x3
	.byte	0x23
	.uleb128 0x1f4
	.uleb128 0x16
	.4byte	.LASF179
	.byte	0xd
	.2byte	0x57b
	.4byte	0x6de
	.byte	0x3
	.byte	0x23
	.uleb128 0x204
	.uleb128 0x16
	.4byte	.LASF180
	.byte	0xd
	.2byte	0x57f
	.4byte	0xd9a
	.byte	0x3
	.byte	0x23
	.uleb128 0x214
	.uleb128 0x16
	.4byte	.LASF181
	.byte	0xd
	.2byte	0x581
	.4byte	0xdab
	.byte	0x3
	.byte	0x23
	.uleb128 0x253c
	.uleb128 0x16
	.4byte	.LASF182
	.byte	0xd
	.2byte	0x588
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36d0
	.uleb128 0x16
	.4byte	.LASF183
	.byte	0xd
	.2byte	0x58a
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36d4
	.uleb128 0x16
	.4byte	.LASF184
	.byte	0xd
	.2byte	0x58c
	.4byte	0xad
	.byte	0x3
	.byte	0x23
	.uleb128 0x36d8
	.uleb128 0x16
	.4byte	.LASF185
	.byte	0xd
	.2byte	0x58e
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36dc
	.uleb128 0x16
	.4byte	.LASF186
	.byte	0xd
	.2byte	0x590
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36e0
	.uleb128 0x16
	.4byte	.LASF187
	.byte	0xd
	.2byte	0x592
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x36e4
	.uleb128 0x16
	.4byte	.LASF188
	.byte	0xd
	.2byte	0x597
	.4byte	0x1d3
	.byte	0x3
	.byte	0x23
	.uleb128 0x36e8
	.uleb128 0x16
	.4byte	.LASF189
	.byte	0xd
	.2byte	0x599
	.4byte	0x536
	.byte	0x3
	.byte	0x23
	.uleb128 0x36f4
	.uleb128 0x16
	.4byte	.LASF190
	.byte	0xd
	.2byte	0x59b
	.4byte	0x536
	.byte	0x3
	.byte	0x23
	.uleb128 0x3704
	.uleb128 0x16
	.4byte	.LASF191
	.byte	0xd
	.2byte	0x5a0
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3714
	.uleb128 0x16
	.4byte	.LASF192
	.byte	0xd
	.2byte	0x5a2
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3718
	.uleb128 0x16
	.4byte	.LASF193
	.byte	0xd
	.2byte	0x5a4
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x371c
	.uleb128 0x16
	.4byte	.LASF194
	.byte	0xd
	.2byte	0x5aa
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3720
	.uleb128 0x16
	.4byte	.LASF195
	.byte	0xd
	.2byte	0x5b1
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3724
	.uleb128 0x16
	.4byte	.LASF196
	.byte	0xd
	.2byte	0x5b3
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3728
	.uleb128 0x16
	.4byte	.LASF197
	.byte	0xd
	.2byte	0x5b7
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x372c
	.uleb128 0x16
	.4byte	.LASF198
	.byte	0xd
	.2byte	0x5be
	.4byte	0x8f1
	.byte	0x3
	.byte	0x23
	.uleb128 0x3730
	.uleb128 0x16
	.4byte	.LASF199
	.byte	0xd
	.2byte	0x5c8
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x3760
	.uleb128 0x16
	.4byte	.LASF200
	.byte	0xd
	.2byte	0x5cf
	.4byte	0xdbc
	.byte	0x3
	.byte	0x23
	.uleb128 0x3764
	.byte	0
	.uleb128 0xc
	.4byte	0x52f
	.4byte	0xd8a
	.uleb128 0xd
	.4byte	0x25
	.byte	0x13
	.byte	0
	.uleb128 0xc
	.4byte	0x52f
	.4byte	0xd9a
	.uleb128 0xd
	.4byte	0x25
	.byte	0x1d
	.byte	0
	.uleb128 0xc
	.4byte	0x5e
	.4byte	0xdab
	.uleb128 0x19
	.4byte	0x25
	.2byte	0x1193
	.byte	0
	.uleb128 0xc
	.4byte	0x33
	.4byte	0xdbc
	.uleb128 0x19
	.4byte	0x25
	.2byte	0x1193
	.byte	0
	.uleb128 0xc
	.4byte	0x70
	.4byte	0xdcc
	.uleb128 0xd
	.4byte	0x25
	.byte	0xa
	.byte	0
	.uleb128 0x14
	.4byte	.LASF201
	.byte	0xd
	.2byte	0x5d6
	.4byte	0x8fd
	.uleb128 0xe
	.byte	0x3c
	.byte	0xd
	.2byte	0x60b
	.4byte	0xe96
	.uleb128 0x16
	.4byte	.LASF202
	.byte	0xd
	.2byte	0x60f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF203
	.byte	0xd
	.2byte	0x616
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF122
	.byte	0xd
	.2byte	0x618
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x16
	.4byte	.LASF204
	.byte	0xd
	.2byte	0x61d
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x16
	.4byte	.LASF205
	.byte	0xd
	.2byte	0x626
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x16
	.4byte	.LASF206
	.byte	0xd
	.2byte	0x62f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x16
	.4byte	.LASF207
	.byte	0xd
	.2byte	0x631
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x16
	.4byte	.LASF208
	.byte	0xd
	.2byte	0x63a
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x16
	.4byte	.LASF209
	.byte	0xd
	.2byte	0x63c
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x16
	.4byte	.LASF210
	.byte	0xd
	.2byte	0x645
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x16
	.4byte	.LASF211
	.byte	0xd
	.2byte	0x647
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x16
	.4byte	.LASF212
	.byte	0xd
	.2byte	0x650
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF213
	.uleb128 0x14
	.4byte	.LASF214
	.byte	0xd
	.2byte	0x652
	.4byte	0xdd8
	.uleb128 0xe
	.byte	0x60
	.byte	0xd
	.2byte	0x655
	.4byte	0xf85
	.uleb128 0x16
	.4byte	.LASF202
	.byte	0xd
	.2byte	0x657
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF198
	.byte	0xd
	.2byte	0x65b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF215
	.byte	0xd
	.2byte	0x65f
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x16
	.4byte	.LASF216
	.byte	0xd
	.2byte	0x660
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x16
	.4byte	.LASF217
	.byte	0xd
	.2byte	0x661
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x16
	.4byte	.LASF218
	.byte	0xd
	.2byte	0x662
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x16
	.4byte	.LASF219
	.byte	0xd
	.2byte	0x668
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x16
	.4byte	.LASF220
	.byte	0xd
	.2byte	0x669
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x16
	.4byte	.LASF221
	.byte	0xd
	.2byte	0x66a
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x16
	.4byte	.LASF222
	.byte	0xd
	.2byte	0x66b
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x16
	.4byte	.LASF223
	.byte	0xd
	.2byte	0x66c
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x16
	.4byte	.LASF224
	.byte	0xd
	.2byte	0x66d
	.4byte	0xe96
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x16
	.4byte	.LASF225
	.byte	0xd
	.2byte	0x671
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x16
	.4byte	.LASF226
	.byte	0xd
	.2byte	0x672
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.byte	0
	.uleb128 0x14
	.4byte	.LASF227
	.byte	0xd
	.2byte	0x674
	.4byte	0xea9
	.uleb128 0xe
	.byte	0x4
	.byte	0xd
	.2byte	0x687
	.4byte	0x102b
	.uleb128 0xf
	.4byte	.LASF228
	.byte	0xd
	.2byte	0x692
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF229
	.byte	0xd
	.2byte	0x696
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF230
	.byte	0xd
	.2byte	0x6a2
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF231
	.byte	0xd
	.2byte	0x6a9
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF232
	.byte	0xd
	.2byte	0x6af
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF233
	.byte	0xd
	.2byte	0x6b1
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF234
	.byte	0xd
	.2byte	0x6b3
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xf
	.4byte	.LASF235
	.byte	0xd
	.2byte	0x6b5
	.4byte	0xb8
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0xd
	.2byte	0x681
	.4byte	0x1046
	.uleb128 0x11
	.4byte	.LASF236
	.byte	0xd
	.2byte	0x685
	.4byte	0x70
	.uleb128 0x12
	.4byte	0xf91
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.byte	0xd
	.2byte	0x67f
	.4byte	0x1058
	.uleb128 0x13
	.4byte	0x102b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x14
	.4byte	.LASF237
	.byte	0xd
	.2byte	0x6be
	.4byte	0x1046
	.uleb128 0xe
	.byte	0x58
	.byte	0xd
	.2byte	0x6c1
	.4byte	0x11b8
	.uleb128 0x17
	.ascii	"pbf\000"
	.byte	0xd
	.2byte	0x6c3
	.4byte	0x1058
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF238
	.byte	0xd
	.2byte	0x6c8
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF239
	.byte	0xd
	.2byte	0x6cd
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x16
	.4byte	.LASF240
	.byte	0xd
	.2byte	0x6d4
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x16
	.4byte	.LASF241
	.byte	0xd
	.2byte	0x6d6
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x16
	.4byte	.LASF242
	.byte	0xd
	.2byte	0x6d8
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x16
	.4byte	.LASF243
	.byte	0xd
	.2byte	0x6da
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x16
	.4byte	.LASF244
	.byte	0xd
	.2byte	0x6dc
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x16
	.4byte	.LASF245
	.byte	0xd
	.2byte	0x6e3
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x16
	.4byte	.LASF246
	.byte	0xd
	.2byte	0x6e5
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x16
	.4byte	.LASF247
	.byte	0xd
	.2byte	0x6e7
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x16
	.4byte	.LASF248
	.byte	0xd
	.2byte	0x6e9
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x16
	.4byte	.LASF249
	.byte	0xd
	.2byte	0x6ef
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x16
	.4byte	.LASF167
	.byte	0xd
	.2byte	0x6fa
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x16
	.4byte	.LASF250
	.byte	0xd
	.2byte	0x705
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x16
	.4byte	.LASF251
	.byte	0xd
	.2byte	0x70c
	.4byte	0x52f
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x16
	.4byte	.LASF252
	.byte	0xd
	.2byte	0x718
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x16
	.4byte	.LASF253
	.byte	0xd
	.2byte	0x71a
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x16
	.4byte	.LASF254
	.byte	0xd
	.2byte	0x720
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x16
	.4byte	.LASF255
	.byte	0xd
	.2byte	0x722
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x16
	.4byte	.LASF256
	.byte	0xd
	.2byte	0x72a
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x16
	.4byte	.LASF257
	.byte	0xd
	.2byte	0x72c
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.byte	0
	.uleb128 0x14
	.4byte	.LASF258
	.byte	0xd
	.2byte	0x72e
	.4byte	0x1064
	.uleb128 0x18
	.2byte	0x1d8
	.byte	0xd
	.2byte	0x731
	.4byte	0x1295
	.uleb128 0x16
	.4byte	.LASF66
	.byte	0xd
	.2byte	0x736
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF202
	.byte	0xd
	.2byte	0x73f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x16
	.4byte	.LASF259
	.byte	0xd
	.2byte	0x749
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x16
	.4byte	.LASF260
	.byte	0xd
	.2byte	0x752
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x16
	.4byte	.LASF261
	.byte	0xd
	.2byte	0x756
	.4byte	0xad
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x16
	.4byte	.LASF262
	.byte	0xd
	.2byte	0x75b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x16
	.4byte	.LASF263
	.byte	0xd
	.2byte	0x762
	.4byte	0x1295
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x17
	.ascii	"rip\000"
	.byte	0xd
	.2byte	0x76b
	.4byte	0xe9d
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x17
	.ascii	"ws\000"
	.byte	0xd
	.2byte	0x772
	.4byte	0x129b
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x16
	.4byte	.LASF264
	.byte	0xd
	.2byte	0x77a
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x160
	.uleb128 0x16
	.4byte	.LASF265
	.byte	0xd
	.2byte	0x787
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x164
	.uleb128 0x16
	.4byte	.LASF198
	.byte	0xd
	.2byte	0x78e
	.4byte	0xf85
	.byte	0x3
	.byte	0x23
	.uleb128 0x168
	.uleb128 0x16
	.4byte	.LASF200
	.byte	0xd
	.2byte	0x797
	.4byte	0x536
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c8
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0xdcc
	.uleb128 0xc
	.4byte	0x11b8
	.4byte	0x12ab
	.uleb128 0xd
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x14
	.4byte	.LASF266
	.byte	0xd
	.2byte	0x79e
	.4byte	0x11c4
	.uleb128 0x8
	.byte	0x24
	.byte	0xe
	.byte	0x78
	.4byte	0x133e
	.uleb128 0xa
	.4byte	.LASF267
	.byte	0xe
	.byte	0x7b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF268
	.byte	0xe
	.byte	0x83
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF269
	.byte	0xe
	.byte	0x86
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF270
	.byte	0xe
	.byte	0x88
	.4byte	0x134f
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF271
	.byte	0xe
	.byte	0x8d
	.4byte	0x1361
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF272
	.byte	0xe
	.byte	0x92
	.4byte	0xc3
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xa
	.4byte	.LASF273
	.byte	0xe
	.byte	0x96
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xa
	.4byte	.LASF274
	.byte	0xe
	.byte	0x9a
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xa
	.4byte	.LASF275
	.byte	0xe
	.byte	0x9c
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	0x134a
	.uleb128 0x1b
	.4byte	0x134a
	.byte	0
	.uleb128 0x1c
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x4
	.4byte	0x133e
	.uleb128 0x1a
	.byte	0x1
	.4byte	0x1361
	.uleb128 0x1b
	.4byte	0x524
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x1355
	.uleb128 0x3
	.4byte	.LASF276
	.byte	0xe
	.byte	0x9e
	.4byte	0x12b7
	.uleb128 0x8
	.byte	0x4
	.byte	0xf
	.byte	0x16
	.4byte	0x1389
	.uleb128 0xa
	.4byte	.LASF277
	.byte	0xf
	.byte	0x18
	.4byte	0x195
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF278
	.byte	0xf
	.byte	0x1a
	.4byte	0x1372
	.uleb128 0x1d
	.4byte	.LASF279
	.byte	0x1
	.byte	0x5c
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x13cc
	.uleb128 0x1e
	.4byte	.LASF281
	.byte	0x1
	.byte	0x5c
	.4byte	0x13cc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1f
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x5e
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1c
	.4byte	0x70
	.uleb128 0x1d
	.4byte	.LASF280
	.byte	0x1
	.byte	0x72
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x1417
	.uleb128 0x1e
	.4byte	.LASF281
	.byte	0x1
	.byte	0x72
	.4byte	0x13cc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1f
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x74
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF282
	.byte	0x1
	.byte	0x75
	.4byte	0x13cc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x21
	.4byte	.LASF365
	.byte	0x1
	.byte	0xa0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x22
	.4byte	.LASF288
	.byte	0x1
	.byte	0xab
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x14b7
	.uleb128 0x1e
	.4byte	.LASF283
	.byte	0x1
	.byte	0xab
	.4byte	0x13cc
	.byte	0x3
	.byte	0x91
	.sleb128 -184
	.uleb128 0x20
	.4byte	.LASF284
	.byte	0x1
	.byte	0xae
	.4byte	0x14b7
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x20
	.4byte	.LASF285
	.byte	0x1
	.byte	0xb0
	.4byte	0x14c1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1f
	.ascii	"bds\000"
	.byte	0x1
	.byte	0xb2
	.4byte	0x632
	.byte	0x3
	.byte	0x91
	.sleb128 -140
	.uleb128 0x20
	.4byte	.LASF286
	.byte	0x1
	.byte	0xb4
	.4byte	0x18a
	.byte	0x3
	.byte	0x91
	.sleb128 -160
	.uleb128 0x1f
	.ascii	"sf\000"
	.byte	0x1
	.byte	0xb6
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x20
	.4byte	.LASF287
	.byte	0x1
	.byte	0xb8
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -164
	.uleb128 0x1f
	.ascii	"str\000"
	.byte	0x1
	.byte	0xba
	.4byte	0x1e3
	.byte	0x3
	.byte	0x91
	.sleb128 -180
	.byte	0
	.uleb128 0x23
	.4byte	0x14bc
	.uleb128 0x1c
	.4byte	0x52f
	.uleb128 0x5
	.byte	0x4
	.4byte	0x5bc
	.uleb128 0x24
	.4byte	.LASF289
	.byte	0x1
	.2byte	0x11c
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x1539
	.uleb128 0x25
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x11e
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x26
	.4byte	.LASF290
	.byte	0x1
	.2byte	0x120
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x26
	.4byte	.LASF291
	.byte	0x1
	.2byte	0x122
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x26
	.4byte	.LASF292
	.byte	0x1
	.2byte	0x124
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x26
	.4byte	.LASF293
	.byte	0x1
	.2byte	0x126
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x26
	.4byte	.LASF286
	.byte	0x1
	.2byte	0x128
	.4byte	0x18a
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x24
	.4byte	.LASF294
	.byte	0x1
	.2byte	0x160
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x15c6
	.uleb128 0x27
	.4byte	.LASF295
	.byte	0x1
	.2byte	0x160
	.4byte	0x15c6
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x26
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x163
	.4byte	0x14b7
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x26
	.4byte	.LASF296
	.byte	0x1
	.2byte	0x165
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x26
	.4byte	.LASF297
	.byte	0x1
	.2byte	0x167
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x26
	.4byte	.LASF298
	.byte	0x1
	.2byte	0x169
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x26
	.4byte	.LASF299
	.byte	0x1
	.2byte	0x16b
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x26
	.4byte	.LASF300
	.byte	0x1
	.2byte	0x183
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.byte	0
	.uleb128 0x1c
	.4byte	0x524
	.uleb128 0x24
	.4byte	.LASF301
	.byte	0x1
	.2byte	0x1a0
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x1658
	.uleb128 0x27
	.4byte	.LASF295
	.byte	0x1
	.2byte	0x1a0
	.4byte	0x15c6
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x26
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x1a3
	.4byte	0x14b7
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x26
	.4byte	.LASF296
	.byte	0x1
	.2byte	0x1a5
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x26
	.4byte	.LASF297
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x26
	.4byte	.LASF298
	.byte	0x1
	.2byte	0x1a9
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x26
	.4byte	.LASF299
	.byte	0x1
	.2byte	0x1ab
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0x26
	.4byte	.LASF300
	.byte	0x1
	.2byte	0x1d6
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.byte	0
	.uleb128 0x24
	.4byte	.LASF302
	.byte	0x1
	.2byte	0x1f3
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x1767
	.uleb128 0x27
	.4byte	.LASF303
	.byte	0x1
	.2byte	0x1f3
	.4byte	0x13cc
	.byte	0x3
	.byte	0x91
	.sleb128 -200
	.uleb128 0x27
	.4byte	.LASF304
	.byte	0x1
	.2byte	0x1f3
	.4byte	0x4e8
	.byte	0x3
	.byte	0x91
	.sleb128 -204
	.uleb128 0x29
	.ascii	"idx\000"
	.byte	0x1
	.2byte	0x1f3
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -208
	.uleb128 0x26
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x1f6
	.4byte	0x14b7
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x25
	.ascii	"str\000"
	.byte	0x1
	.2byte	0x1f8
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x26
	.4byte	.LASF305
	.byte	0x1
	.2byte	0x1fa
	.4byte	0x1767
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x26
	.4byte	.LASF285
	.byte	0x1
	.2byte	0x1fc
	.4byte	0x14c1
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x25
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x1fe
	.4byte	0x632
	.byte	0x3
	.byte	0x91
	.sleb128 -172
	.uleb128 0x26
	.4byte	.LASF306
	.byte	0x1
	.2byte	0x200
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x25
	.ascii	"dum\000"
	.byte	0x1
	.2byte	0x202
	.4byte	0x70
	.byte	0x3
	.byte	0x91
	.sleb128 -176
	.uleb128 0x25
	.ascii	"bpr\000"
	.byte	0x1
	.2byte	0x203
	.4byte	0x176d
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x25
	.ascii	"min\000"
	.byte	0x1
	.2byte	0x205
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x25
	.ascii	"max\000"
	.byte	0x1
	.2byte	0x206
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x26
	.4byte	.LASF307
	.byte	0x1
	.2byte	0x207
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x25
	.ascii	"sf\000"
	.byte	0x1
	.2byte	0x25b
	.4byte	0x52f
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x26
	.4byte	.LASF286
	.byte	0x1
	.2byte	0x272
	.4byte	0x18a
	.byte	0x3
	.byte	0x91
	.sleb128 -196
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x4ee
	.uleb128 0x5
	.byte	0x4
	.4byte	0x12ab
	.uleb128 0x24
	.4byte	.LASF308
	.byte	0x1
	.2byte	0x286
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x17cb
	.uleb128 0x27
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x286
	.4byte	0x13cc
	.byte	0x3
	.byte	0x91
	.sleb128 -132
	.uleb128 0x25
	.ascii	"bds\000"
	.byte	0x1
	.2byte	0x288
	.4byte	0x632
	.byte	0x3
	.byte	0x91
	.sleb128 -128
	.uleb128 0x26
	.4byte	.LASF305
	.byte	0x1
	.2byte	0x28a
	.4byte	0x1767
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x26
	.4byte	.LASF285
	.byte	0x1
	.2byte	0x28c
	.4byte	0x14c1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x24
	.4byte	.LASF309
	.byte	0x1
	.2byte	0x29d
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x1803
	.uleb128 0x27
	.4byte	.LASF310
	.byte	0x1
	.2byte	0x29d
	.4byte	0x13cc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x26
	.4byte	.LASF311
	.byte	0x1
	.2byte	0x2a0
	.4byte	0xad
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x24
	.4byte	.LASF312
	.byte	0x1
	.2byte	0x2e3
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x183b
	.uleb128 0x27
	.4byte	.LASF303
	.byte	0x1
	.2byte	0x2e3
	.4byte	0x13cc
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x26
	.4byte	.LASF313
	.byte	0x1
	.2byte	0x2e5
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x24
	.4byte	.LASF314
	.byte	0x1
	.2byte	0x349
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x1873
	.uleb128 0x27
	.4byte	.LASF315
	.byte	0x1
	.2byte	0x349
	.4byte	0x1873
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x26
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x34c
	.4byte	0x14b7
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.4byte	0xad
	.uleb128 0x2a
	.4byte	.LASF316
	.byte	0x1
	.2byte	0x36b
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.uleb128 0x2a
	.4byte	.LASF317
	.byte	0x1
	.2byte	0x370
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.uleb128 0x24
	.4byte	.LASF318
	.byte	0x1
	.2byte	0x389
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x18e9
	.uleb128 0x27
	.4byte	.LASF295
	.byte	0x1
	.2byte	0x389
	.4byte	0x15c6
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x26
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x38c
	.4byte	0x14b7
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x25
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x38e
	.4byte	0x1367
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x2a
	.4byte	.LASF319
	.byte	0x1
	.2byte	0x4a4
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.uleb128 0x2b
	.byte	0x1
	.4byte	.LASF323
	.byte	0x1
	.2byte	0x4ad
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x1955
	.uleb128 0x27
	.4byte	.LASF315
	.byte	0x1
	.2byte	0x4ad
	.4byte	0x1873
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x26
	.4byte	.LASF320
	.byte	0x1
	.2byte	0x4af
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x26
	.4byte	.LASF321
	.byte	0x1
	.2byte	0x4b0
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x26
	.4byte	.LASF322
	.byte	0x1
	.2byte	0x4b1
	.4byte	0x82
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x2b
	.byte	0x1
	.4byte	.LASF324
	.byte	0x1
	.2byte	0x4ef
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x197f
	.uleb128 0x27
	.4byte	.LASF295
	.byte	0x1
	.2byte	0x4ef
	.4byte	0x524
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x2c
	.4byte	.LASF325
	.byte	0x10
	.2byte	0x114
	.4byte	0x52f
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF326
	.byte	0x10
	.2byte	0x116
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF327
	.byte	0x10
	.2byte	0x119
	.4byte	0x52f
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF328
	.byte	0x10
	.2byte	0x11e
	.4byte	0x52f
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	0x2c
	.4byte	0x19c7
	.uleb128 0xd
	.4byte	0x25
	.byte	0x28
	.byte	0
	.uleb128 0x2c
	.4byte	.LASF329
	.byte	0x10
	.2byte	0x124
	.4byte	0x19b7
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF330
	.byte	0x10
	.2byte	0x125
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF331
	.byte	0x10
	.2byte	0x127
	.4byte	0x19b7
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF332
	.byte	0x10
	.2byte	0x135
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF333
	.byte	0x10
	.2byte	0x136
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF334
	.byte	0x10
	.2byte	0x139
	.4byte	0x52f
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF335
	.byte	0x10
	.2byte	0x13a
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF336
	.byte	0x10
	.2byte	0x268
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF337
	.byte	0x10
	.2byte	0x2ec
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF338
	.byte	0x11
	.2byte	0x127
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF339
	.byte	0x11
	.2byte	0x132
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF340
	.byte	0x12
	.byte	0x30
	.4byte	0x1a72
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x1c
	.4byte	0x1c3
	.uleb128 0x20
	.4byte	.LASF341
	.byte	0x12
	.byte	0x34
	.4byte	0x1a88
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x1c
	.4byte	0x1c3
	.uleb128 0x20
	.4byte	.LASF342
	.byte	0x12
	.byte	0x36
	.4byte	0x1a9e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x1c
	.4byte	0x1c3
	.uleb128 0x20
	.4byte	.LASF343
	.byte	0x12
	.byte	0x38
	.4byte	0x1ab4
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x1c
	.4byte	0x1c3
	.uleb128 0x2d
	.4byte	.LASF344
	.byte	0x13
	.byte	0x63
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF345
	.byte	0x13
	.byte	0x65
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF346
	.byte	0xc
	.byte	0x33
	.4byte	0x1ae4
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x1c
	.4byte	0x1d3
	.uleb128 0x20
	.4byte	.LASF347
	.byte	0xc
	.byte	0x3f
	.4byte	0x1afa
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x1c
	.4byte	0x536
	.uleb128 0x2d
	.4byte	.LASF348
	.byte	0x14
	.byte	0x78
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF349
	.byte	0x14
	.byte	0xc0
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF350
	.byte	0x14
	.byte	0xc3
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF351
	.byte	0x14
	.byte	0xc6
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF352
	.byte	0x14
	.byte	0xc9
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	0x1367
	.4byte	0x1b50
	.uleb128 0xd
	.4byte	0x25
	.byte	0x31
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF353
	.byte	0xe
	.byte	0xac
	.4byte	0x1b40
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF354
	.byte	0xe
	.byte	0xae
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF355
	.byte	0x15
	.byte	0x19
	.4byte	0x622
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	0x52f
	.4byte	0x1b87
	.uleb128 0xd
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF356
	.byte	0x15
	.byte	0x1d
	.4byte	0x1b77
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF357
	.byte	0x15
	.byte	0x20
	.4byte	0x52f
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	0x1389
	.4byte	0x1bb1
	.uleb128 0xd
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF358
	.byte	0xf
	.byte	0x1c
	.4byte	0x1ba1
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF359
	.byte	0xf
	.byte	0x1e
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF360
	.byte	0xf
	.byte	0x20
	.4byte	0xad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF361
	.byte	0xf
	.byte	0x22
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF362
	.byte	0x1
	.byte	0x59
	.4byte	0x70
	.byte	0x5
	.byte	0x3
	.4byte	date_change_warning_flag
	.uleb128 0x2c
	.4byte	.LASF325
	.byte	0x10
	.2byte	0x114
	.4byte	0x52f
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF326
	.byte	0x10
	.2byte	0x116
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF327
	.byte	0x10
	.2byte	0x119
	.4byte	0x52f
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF328
	.byte	0x10
	.2byte	0x11e
	.4byte	0x52f
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF329
	.byte	0x10
	.2byte	0x124
	.4byte	0x19b7
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF330
	.byte	0x10
	.2byte	0x125
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF331
	.byte	0x10
	.2byte	0x127
	.4byte	0x19b7
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF332
	.byte	0x10
	.2byte	0x135
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF333
	.byte	0x10
	.2byte	0x136
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF334
	.byte	0x10
	.2byte	0x139
	.4byte	0x52f
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF335
	.byte	0x10
	.2byte	0x13a
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF336
	.byte	0x10
	.2byte	0x268
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF337
	.byte	0x10
	.2byte	0x2ec
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF338
	.byte	0x11
	.2byte	0x127
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF339
	.byte	0x11
	.2byte	0x132
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF344
	.byte	0x13
	.byte	0x63
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF345
	.byte	0x13
	.byte	0x65
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF348
	.byte	0x14
	.byte	0x78
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF349
	.byte	0x14
	.byte	0xc0
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF350
	.byte	0x14
	.byte	0xc3
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF351
	.byte	0x14
	.byte	0xc6
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF352
	.byte	0x14
	.byte	0xc9
	.4byte	0x1ad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF353
	.byte	0xe
	.byte	0xac
	.4byte	0x1b40
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF354
	.byte	0xe
	.byte	0xae
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2e
	.4byte	.LASF355
	.byte	0x1
	.byte	0x4f
	.4byte	0x622
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_BUDGET_SETUP_meter_read_date
	.uleb128 0x2e
	.4byte	.LASF356
	.byte	0x1
	.byte	0x53
	.4byte	0x1b77
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_BUDGET_SETUP_budget
	.uleb128 0x2e
	.4byte	.LASF357
	.byte	0x1
	.byte	0x56
	.4byte	0x52f
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_BUDGET_SETUP_annual_budget
	.uleb128 0x2d
	.4byte	.LASF358
	.byte	0xf
	.byte	0x1c
	.4byte	0x1ba1
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF359
	.byte	0xf
	.byte	0x1e
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF360
	.byte	0xf
	.byte	0x20
	.4byte	0xad
	.byte	0x1
	.byte	0x1
	.uleb128 0x2d
	.4byte	.LASF361
	.byte	0xf
	.byte	0x22
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI39
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI41
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI44
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI46
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI49
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xa4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF242:
	.ascii	"fm_latest_5_second_pulse_count_irri\000"
.LASF47:
	.ascii	"there_are_pocs_without_flow_meters\000"
.LASF235:
	.ascii	"no_current_pump\000"
.LASF214:
	.ascii	"POC_REPORT_RECORD\000"
.LASF147:
	.ascii	"ufim_one_RRE_ON_to_set_expected_b\000"
.LASF220:
	.ascii	"used_manual_programmed_gallons\000"
.LASF69:
	.ascii	"number_of_flow_meters_in_this_sys\000"
.LASF360:
	.ascii	"g_POC_editing_group\000"
.LASF135:
	.ascii	"system_master_number_of_valves_ON\000"
.LASF141:
	.ascii	"ufim_list_contains_waiting_programmed_irrigation_b\000"
.LASF42:
	.ascii	"system_level_valves_are_ON_and_has_updated_the_dera"
	.ascii	"te_table\000"
.LASF245:
	.ascii	"fm_accumulated_pulses_foal\000"
.LASF294:
	.ascii	"BUDGET_SETUP_handle_annual_budget_change\000"
.LASF315:
	.ascii	"pcomplete_redraw\000"
.LASF101:
	.ascii	"mlb_limit_during_all_other_times_gpm\000"
.LASF71:
	.ascii	"SYSTEM_BIT_FIELD_STRUCT\000"
.LASF268:
	.ascii	"_02_menu\000"
.LASF211:
	.ascii	"gallons_during_irrigation\000"
.LASF125:
	.ascii	"reduction_gallons\000"
.LASF350:
	.ascii	"poc_preserves_recursive_MUTEX\000"
.LASF167:
	.ascii	"accumulated_gallons_for_accumulators_foal\000"
.LASF134:
	.ascii	"ufim_maximum_valves_in_system_we_can_have_ON_now\000"
.LASF271:
	.ascii	"key_process_func_ptr\000"
.LASF231:
	.ascii	"there_is_flow_meter_count_data_to_send_to_the_maste"
	.ascii	"r\000"
.LASF131:
	.ascii	"BY_SYSTEM_BUDGET_RECORD\000"
.LASF278:
	.ascii	"POC_MENU_SCROLL_BOX_ITEM\000"
.LASF190:
	.ascii	"flow_check_tolerance_minus_gpm\000"
.LASF236:
	.ascii	"overall_size\000"
.LASF62:
	.ascii	"ufim_one_or_more_in_list_for_walk_thru\000"
.LASF15:
	.ascii	"BOOL_32\000"
.LASF57:
	.ascii	"one_or_more_in_list_for_rre\000"
.LASF200:
	.ascii	"expansion\000"
.LASF174:
	.ascii	"transition_timer_all_pump_valves_are_OFF\000"
.LASF145:
	.ascii	"ufim_one_ON_to_set_expected_b\000"
.LASF307:
	.ascii	"MAXBUDGETDAYS\000"
.LASF321:
	.ascii	"lactive_line\000"
.LASF299:
	.ascii	"num_decimals\000"
.LASF30:
	.ascii	"xQueueHandle\000"
.LASF143:
	.ascii	"ufim_list_contains_waiting_non_pump_valves_b\000"
.LASF312:
	.ascii	"BUDGETS_process_NEXT_and_PREV\000"
.LASF114:
	.ascii	"manual_gallons_fl\000"
.LASF298:
	.ascii	"pdelta\000"
.LASF158:
	.ascii	"ufim_highest_pump_reason_in_list_available_to_turn_"
	.ascii	"ON\000"
.LASF126:
	.ascii	"ratio\000"
.LASF230:
	.ascii	"send_mv_pump_milli_amp_measurements_to_the_master\000"
.LASF28:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF314:
	.ascii	"FDTO_BUDGETS_after_keypad\000"
.LASF306:
	.ascii	"num_pocs\000"
.LASF142:
	.ascii	"ufim_list_contains_waiting_pump_valves_b\000"
.LASF329:
	.ascii	"GuiVar_BudgetPeriodEnd\000"
.LASF337:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF64:
	.ascii	"ufim_one_or_more_in_list_for_mobile\000"
.LASF287:
	.ascii	"index_poc_preserves\000"
.LASF295:
	.ascii	"pkey_event\000"
.LASF204:
	.ascii	"pad_bytes_avaiable_for_use\000"
.LASF88:
	.ascii	"number_of_annual_periods\000"
.LASF181:
	.ascii	"derate_cell_iterations\000"
.LASF17:
	.ascii	"long int\000"
.LASF253:
	.ascii	"pump_last_measured_current_from_the_tpmicro_ma\000"
.LASF317:
	.ascii	"FDTO_POC_close_budget_period_idx_dropdown\000"
.LASF108:
	.ascii	"rre_gallons_fl\000"
.LASF116:
	.ascii	"manual_program_gallons_fl\000"
.LASF246:
	.ascii	"fm_accumulated_ms_foal\000"
.LASF120:
	.ascii	"non_controller_gallons_fl\000"
.LASF33:
	.ascii	"unused_four_bits\000"
.LASF7:
	.ascii	"INT_16\000"
.LASF63:
	.ascii	"ufim_one_or_more_in_list_for_test\000"
.LASF152:
	.ascii	"ufim_based_on_reason_in_list_at_least_one_valve_is_"
	.ascii	"ON_that_wants_to_check_flow\000"
.LASF202:
	.ascii	"poc_gid\000"
.LASF252:
	.ascii	"mv_last_measured_current_from_the_tpmicro_ma\000"
.LASF99:
	.ascii	"mlb_limit_during_mvor_closed_gpm\000"
.LASF194:
	.ascii	"system_rcvd_most_recent_number_of_valves_ON\000"
.LASF296:
	.ascii	"pmin\000"
.LASF140:
	.ascii	"ufim_highest_priority_non_pump_waiting\000"
.LASF3:
	.ascii	"signed char\000"
.LASF333:
	.ascii	"GuiVar_BudgetUnitHCF\000"
.LASF170:
	.ascii	"last_off__station_number_0\000"
.LASF179:
	.ascii	"delivered_mlb_record\000"
.LASF148:
	.ascii	"ufim_list_contains_some_RRE_to_setex_that_are_not_O"
	.ascii	"N_b\000"
.LASF276:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF118:
	.ascii	"programmed_irrigation_gallons_fl\000"
.LASF27:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF283:
	.ascii	"budget_idx\000"
.LASF285:
	.ascii	"ptr_sys\000"
.LASF226:
	.ascii	"off_at_start_time\000"
.LASF282:
	.ascii	"scale\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF325:
	.ascii	"GuiVar_BudgetAnnualValue\000"
.LASF256:
	.ascii	"mv_current_as_delivered_from_the_master_ma\000"
.LASF20:
	.ascii	"__day\000"
.LASF146:
	.ascii	"ufim_list_contains_some_to_setex_that_are_not_ON_b\000"
.LASF192:
	.ascii	"flow_check_hi_limit\000"
.LASF11:
	.ascii	"INT_32\000"
.LASF239:
	.ascii	"master_valve_type\000"
.LASF85:
	.ascii	"IRRIGATION_SYSTEM_GROUP_STRUCT\000"
.LASF159:
	.ascii	"ufim_highest_non_pump_reason_in_list_available_to_t"
	.ascii	"urn_ON\000"
.LASF44:
	.ascii	"checked_or_updated_and_made_flow_recording_lines\000"
.LASF173:
	.ascii	"transition_timer_all_stations_are_OFF\000"
.LASF334:
	.ascii	"GuiVar_BudgetUseThisPeriod\000"
.LASF286:
	.ascii	"dtcs\000"
.LASF104:
	.ascii	"start_dt\000"
.LASF322:
	.ascii	"lcursor_to_select\000"
.LASF77:
	.ascii	"original_allocation\000"
.LASF70:
	.ascii	"number_of_pocs_in_this_system\000"
.LASF328:
	.ascii	"GuiVar_BudgetForThisPeriod\000"
.LASF234:
	.ascii	"no_current_mv\000"
.LASF1:
	.ascii	"char\000"
.LASF40:
	.ascii	"system_level_valves_are_ON_and_actively_checking\000"
.LASF258:
	.ascii	"POC_DECODER_OR_TERMINAL_WORKING_STRUCT\000"
.LASF305:
	.ascii	"ptr_poc\000"
.LASF361:
	.ascii	"g_POC_current_list_item_index\000"
.LASF49:
	.ascii	"MVOR_in_effect_opened\000"
.LASF87:
	.ascii	"meter_read_time\000"
.LASF58:
	.ascii	"ufim_one_or_more_in_list_for_programmed_irrigation\000"
.LASF130:
	.ascii	"last_rollover_day\000"
.LASF144:
	.ascii	"ufim_highest_reason_of_OFF_valve_to_set_expected\000"
.LASF117:
	.ascii	"programmed_irrigation_seconds\000"
.LASF233:
	.ascii	"shorted_pump\000"
.LASF136:
	.ascii	"ufim_what_are_we_turning_on_b\000"
.LASF19:
	.ascii	"date_time\000"
.LASF216:
	.ascii	"used_irrigation_gallons\000"
.LASF247:
	.ascii	"fm_latest_5_second_pulse_count_foal\000"
.LASF52:
	.ascii	"one_or_more_in_list_for_manual_program\000"
.LASF222:
	.ascii	"used_walkthru_gallons\000"
.LASF335:
	.ascii	"GuiVar_BudgetZeroWarning\000"
.LASF319:
	.ascii	"FDTO_BUDGETS_return_to_menu\000"
.LASF74:
	.ascii	"repeats\000"
.LASF86:
	.ascii	"in_use\000"
.LASF138:
	.ascii	"ufim_one_ON_from_the_problem_list_b\000"
.LASF280:
	.ascii	"POC_get_inc_by_for_monthly_budget\000"
.LASF80:
	.ascii	"first_to_send\000"
.LASF78:
	.ascii	"next_available\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF123:
	.ascii	"end_date\000"
.LASF272:
	.ascii	"_04_func_ptr\000"
.LASF225:
	.ascii	"on_at_start_time\000"
.LASF83:
	.ascii	"when_to_send_timer\000"
.LASF277:
	.ascii	"poc_ptr\000"
.LASF37:
	.ascii	"system_level_no_valves_ON_therefore_no_flow_checkin"
	.ascii	"g\000"
.LASF304:
	.ascii	"pgui_ptr\000"
.LASF93:
	.ascii	"there_was_a_MLB_during_mvor_closed\000"
.LASF265:
	.ascii	"msgs_to_tpmicro_with_no_valves_ON\000"
.LASF84:
	.ascii	"FLOW_RECORDING_CONTROL_STRUCT\000"
.LASF26:
	.ascii	"__dayofweek\000"
.LASF195:
	.ascii	"mvor_stop_date\000"
.LASF100:
	.ascii	"mlb_measured_during_all_other_times_gpm\000"
.LASF103:
	.ascii	"system_gid\000"
.LASF215:
	.ascii	"used_total_gallons\000"
.LASF66:
	.ascii	"accounted_for\000"
.LASF362:
	.ascii	"date_change_warning_flag\000"
.LASF193:
	.ascii	"flow_check_lo_limit\000"
.LASF365:
	.ascii	"BUDGETS_show_warnings\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF318:
	.ascii	"BUDGETS_process_group\000"
.LASF180:
	.ascii	"derate_table_10u\000"
.LASF59:
	.ascii	"ufim_one_or_more_in_list_for_manual_program\000"
.LASF9:
	.ascii	"UNS_32\000"
.LASF302:
	.ascii	"BUDGET_SETUP_handle_start_date_change\000"
.LASF61:
	.ascii	"ufim_one_or_more_in_list_for_manual\000"
.LASF113:
	.ascii	"manual_seconds\000"
.LASF16:
	.ascii	"BITFIELD_BOOL\000"
.LASF309:
	.ascii	"BUDGET_SETUP_handle_units_change\000"
.LASF288:
	.ascii	"BUDGETS_update_guivars\000"
.LASF32:
	.ascii	"xTimerHandle\000"
.LASF244:
	.ascii	"fm_seconds_since_last_pulse_irri\000"
.LASF301:
	.ascii	"BUDGET_SETUP_handle_budget_change\000"
.LASF228:
	.ascii	"master_valve_energized_irri\000"
.LASF185:
	.ascii	"flow_check_derate_table_gpm_slot_size\000"
.LASF109:
	.ascii	"test_seconds\000"
.LASF209:
	.ascii	"gallons_during_mvor\000"
.LASF259:
	.ascii	"box_index\000"
.LASF175:
	.ascii	"timer_MVJO_flow_checking_blockout_seconds_remaining"
	.ascii	"\000"
.LASF289:
	.ascii	"BUDGET_SETUP_update_montly_budget\000"
.LASF237:
	.ascii	"POC_BIT_FIELD_STRUCT\000"
.LASF166:
	.ascii	"system_rcvd_most_recent_token_5_second_average\000"
.LASF358:
	.ascii	"POC_MENU_items\000"
.LASF25:
	.ascii	"__seconds\000"
.LASF122:
	.ascii	"start_date\000"
.LASF347:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF51:
	.ascii	"one_or_more_in_list_for_programmed_irrigation\000"
.LASF171:
	.ascii	"last_off__reason_in_list\000"
.LASF65:
	.ascii	"due_to_edit_resync_to_the_system_list\000"
.LASF331:
	.ascii	"GuiVar_BudgetPeriodStart\000"
.LASF124:
	.ascii	"predicted_use_to_end_of_period\000"
.LASF105:
	.ascii	"no_longer_used_end_dt\000"
.LASF293:
	.ascii	"lbudget_per_inch_ET\000"
.LASF221:
	.ascii	"used_manual_gallons\000"
.LASF238:
	.ascii	"decoder_serial_number\000"
.LASF354:
	.ascii	"screen_history_index\000"
.LASF311:
	.ascii	"lunits_changed\000"
.LASF14:
	.ascii	"long long int\000"
.LASF345:
	.ascii	"g_GROUP_list_item_index\000"
.LASF336:
	.ascii	"GuiVar_IsMaster\000"
.LASF160:
	.ascii	"ufim_flow_check_group_count_of_ON\000"
.LASF279:
	.ascii	"BUDGETS_get_inc_by_for_yearly_budget\000"
.LASF292:
	.ascii	"lET_for_year\000"
.LASF132:
	.ascii	"highest_reason_in_list\000"
.LASF168:
	.ascii	"system_stability_averages_ring\000"
.LASF213:
	.ascii	"double\000"
.LASF45:
	.ascii	"flow_checking_enabled_and_allowed\000"
.LASF24:
	.ascii	"__minutes\000"
.LASF12:
	.ascii	"UNS_64\000"
.LASF267:
	.ascii	"_01_command\000"
.LASF169:
	.ascii	"stability_avgs_index_of_last_computed\000"
.LASF357:
	.ascii	"g_BUDGET_SETUP_annual_budget\000"
.LASF327:
	.ascii	"GuiVar_BudgetExpectedUseThisPeriod\000"
.LASF76:
	.ascii	"float\000"
.LASF73:
	.ascii	"keycode\000"
.LASF186:
	.ascii	"flow_check_derate_table_max_stations_ON\000"
.LASF198:
	.ascii	"budget\000"
.LASF41:
	.ascii	"system_level_valves_are_ON_and_waiting_to_update_de"
	.ascii	"rate_table\000"
.LASF92:
	.ascii	"there_was_a_MLB_during_irrigation\000"
.LASF10:
	.ascii	"unsigned int\000"
.LASF29:
	.ascii	"portTickType\000"
.LASF129:
	.ascii	"unused_0\000"
.LASF199:
	.ascii	"reason_in_running_list\000"
.LASF346:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF187:
	.ascii	"flow_check_derate_table_number_of_gpm_slots\000"
.LASF332:
	.ascii	"GuiVar_BudgetUnitGallon\000"
.LASF94:
	.ascii	"there_was_a_MLB_during_all_other_times\000"
.LASF178:
	.ascii	"latest_mlb_record\000"
.LASF155:
	.ascii	"ufim_stations_ON_with_the_pump_b\000"
.LASF153:
	.ascii	"ufim_the_valves_ON_meet_the_flow_checking_cycles_re"
	.ascii	"quirement\000"
.LASF323:
	.ascii	"FDTO_BUDGETS_draw_menu\000"
.LASF338:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF205:
	.ascii	"gallons_total\000"
.LASF107:
	.ascii	"rre_seconds\000"
.LASF240:
	.ascii	"fm_accumulated_pulses_irri\000"
.LASF270:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF324:
	.ascii	"BUDGETS_process_menu\000"
.LASF50:
	.ascii	"MVOR_in_effect_closed\000"
.LASF137:
	.ascii	"ufim_expected_flow_rate_for_those_ON\000"
.LASF161:
	.ascii	"flow_checking_block_out_remaining_seconds\000"
.LASF43:
	.ascii	"system_level_valves_are_ON_and_waiting_to_acquire_e"
	.ascii	"xpected\000"
.LASF250:
	.ascii	"latest_5_second_average_gpm_foal\000"
.LASF348:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF82:
	.ascii	"pending_first_to_send_in_use\000"
.LASF163:
	.ascii	"system_master_5_second_averages_ring\000"
.LASF201:
	.ascii	"BY_SYSTEM_RECORD\000"
.LASF35:
	.ascii	"mv_open_for_irrigation\000"
.LASF359:
	.ascii	"g_POC_top_line\000"
.LASF229:
	.ascii	"pump_energized_irri\000"
.LASF284:
	.ascii	"HCF_CONVERSION\000"
.LASF364:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budgets.c\000"
.LASF210:
	.ascii	"seconds_of_flow_during_mvor\000"
.LASF112:
	.ascii	"walk_thru_gallons_fl\000"
.LASF355:
	.ascii	"g_BUDGET_SETUP_meter_read_date\000"
.LASF188:
	.ascii	"flow_check_ranges_gpm\000"
.LASF251:
	.ascii	"delivered_5_second_average_gpm_irri\000"
.LASF91:
	.ascii	"BUDGET_DETAILS_STRUCT\000"
.LASF243:
	.ascii	"fm_delta_between_last_two_fm_pulses_4khz_irri\000"
.LASF4:
	.ascii	"UNS_8\000"
.LASF150:
	.ascii	"ufim_list_contains_some_non_pump_to_setex_that_are_"
	.ascii	"not_ON_b\000"
.LASF218:
	.ascii	"used_idle_gallons\000"
.LASF106:
	.ascii	"rainfall_raw_total_100u\000"
.LASF316:
	.ascii	"FDTO_POC_show_budget_period_idx_dropdown\000"
.LASF111:
	.ascii	"walk_thru_seconds\000"
.LASF13:
	.ascii	"long long unsigned int\000"
.LASF349:
	.ascii	"system_preserves_recursive_MUTEX\000"
.LASF207:
	.ascii	"gallons_during_idle\000"
.LASF206:
	.ascii	"seconds_of_flow_total\000"
.LASF281:
	.ascii	"prepeats\000"
.LASF273:
	.ascii	"_06_u32_argument1\000"
.LASF55:
	.ascii	"one_or_more_in_list_for_walk_thru\000"
.LASF182:
	.ascii	"flow_check_required_station_cycles\000"
.LASF72:
	.ascii	"POC_GROUP_STRUCT\000"
.LASF34:
	.ascii	"master_valve_has_at_least_one_normally_closed\000"
.LASF330:
	.ascii	"GuiVar_BudgetPeriodIdx\000"
.LASF275:
	.ascii	"_08_screen_to_draw\000"
.LASF98:
	.ascii	"mlb_measured_during_mvor_closed_gpm\000"
.LASF18:
	.ascii	"DATE_TIME\000"
.LASF53:
	.ascii	"no_longer_used_01\000"
.LASF60:
	.ascii	"no_longer_used_02\000"
.LASF351:
	.ascii	"list_system_recursive_MUTEX\000"
.LASF197:
	.ascii	"delivered_MVOR_remaining_seconds\000"
.LASF97:
	.ascii	"mlb_limit_during_irrigation_gpm\000"
.LASF219:
	.ascii	"used_programmed_gallons\000"
.LASF162:
	.ascii	"inhibit_next_turn_ON_remaining_seconds\000"
.LASF339:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF310:
	.ascii	"field\000"
.LASF249:
	.ascii	"fm_seconds_since_last_pulse_foal\000"
.LASF260:
	.ascii	"poc_type__file_type\000"
.LASF151:
	.ascii	"ufim_there_is_a_PUMP_mix_condition_b\000"
.LASF154:
	.ascii	"ufim_number_ON_during_test\000"
.LASF56:
	.ascii	"one_or_more_in_list_for_test\000"
.LASF128:
	.ascii	"SYSTEM_BUDGET_REPORT_RECORD\000"
.LASF254:
	.ascii	"mv_current_for_distribution_in_the_token_ma\000"
.LASF31:
	.ascii	"xSemaphoreHandle\000"
.LASF149:
	.ascii	"ufim_list_contains_some_pump_to_setex_that_are_not_"
	.ascii	"ON_b\000"
.LASF341:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF356:
	.ascii	"g_BUDGET_SETUP_budget\000"
.LASF121:
	.ascii	"SYSTEM_REPORT_RECORD\000"
.LASF290:
	.ascii	"month\000"
.LASF115:
	.ascii	"manual_program_seconds\000"
.LASF313:
	.ascii	"lmenu_index_for_displayed_poc\000"
.LASF352:
	.ascii	"list_poc_recursive_MUTEX\000"
.LASF172:
	.ascii	"MVOR_remaining_seconds\000"
.LASF189:
	.ascii	"flow_check_tolerance_plus_gpm\000"
.LASF68:
	.ascii	"delivered_MVOR_in_effect_closed\000"
.LASF96:
	.ascii	"mlb_measured_during_irrigation_gpm\000"
.LASF67:
	.ascii	"delivered_MVOR_in_effect_opened\000"
.LASF269:
	.ascii	"_03_structure_to_draw\000"
.LASF232:
	.ascii	"shorted_mv\000"
.LASF343:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF8:
	.ascii	"short int\000"
.LASF223:
	.ascii	"used_test_gallons\000"
.LASF344:
	.ascii	"g_GROUP_ID\000"
.LASF90:
	.ascii	"mode\000"
.LASF89:
	.ascii	"meter_read_date\000"
.LASF291:
	.ascii	"month_et\000"
.LASF262:
	.ascii	"usage\000"
.LASF255:
	.ascii	"pump_current_for_distribution_in_the_token_ma\000"
.LASF224:
	.ascii	"used_mobile_gallons\000"
.LASF340:
	.ascii	"GuiFont_LanguageActive\000"
.LASF320:
	.ascii	"lpocs_in_use\000"
.LASF297:
	.ascii	"pmax\000"
.LASF196:
	.ascii	"mvor_stop_time\000"
.LASF165:
	.ascii	"system_master_5_sec_avgs_next_index\000"
.LASF21:
	.ascii	"__month\000"
.LASF119:
	.ascii	"non_controller_seconds\000"
.LASF241:
	.ascii	"fm_accumulated_ms_irri\000"
.LASF183:
	.ascii	"flow_check_required_cell_iteration\000"
.LASF266:
	.ascii	"BY_POC_RECORD\000"
.LASF22:
	.ascii	"__year\000"
.LASF177:
	.ascii	"frcs\000"
.LASF75:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF164:
	.ascii	"system_master_most_recent_5_second_average\000"
.LASF227:
	.ascii	"BY_POC_BUDGET_RECORD\000"
.LASF212:
	.ascii	"seconds_of_flow_during_irrigation\000"
.LASF326:
	.ascii	"GuiVar_BudgetEntryOptionIdx\000"
.LASF133:
	.ascii	"ufim_valves_in_the_list_for_this_system\000"
.LASF36:
	.ascii	"pump_activate_for_irrigation\000"
.LASF81:
	.ascii	"pending_first_to_send\000"
.LASF79:
	.ascii	"first_to_display\000"
.LASF139:
	.ascii	"ufim_highest_priority_pump_waiting\000"
.LASF208:
	.ascii	"seconds_of_flow_during_idle\000"
.LASF274:
	.ascii	"_07_u32_argument2\000"
.LASF191:
	.ascii	"flow_check_derated_expected\000"
.LASF54:
	.ascii	"one_or_more_in_list_for_manual\000"
.LASF176:
	.ascii	"timer_MLB_just_stopped_irrigating_blockout_seconds_"
	.ascii	"remaining\000"
.LASF46:
	.ascii	"flow_checking_enabled_by_user_setting\000"
.LASF261:
	.ascii	"unused_01\000"
.LASF156:
	.ascii	"ufim_stations_ON_without_the_pump_b\000"
.LASF363:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF303:
	.ascii	"pkeycode\000"
.LASF127:
	.ascii	"closing_record_for_the_period\000"
.LASF110:
	.ascii	"test_gallons_fl\000"
.LASF48:
	.ascii	"stable_flow\000"
.LASF39:
	.ascii	"system_level_valves_are_ON_and_waiting_to_check_flo"
	.ascii	"w\000"
.LASF353:
	.ascii	"ScreenHistory\000"
.LASF203:
	.ascii	"start_time\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF257:
	.ascii	"pump_current_as_delivered_from_the_master_ma\000"
.LASF102:
	.ascii	"SYSTEM_MAINLINE_BREAK_RECORD\000"
.LASF157:
	.ascii	"ufim_highest_reason_in_list_available_to_turn_ON\000"
.LASF342:
	.ascii	"GuiFont_DecimalChar\000"
.LASF264:
	.ascii	"bypass_activate\000"
.LASF300:
	.ascii	"temp\000"
.LASF95:
	.ascii	"dummy_byte\000"
.LASF23:
	.ascii	"__hours\000"
.LASF263:
	.ascii	"this_pocs_system_preserves_ptr\000"
.LASF38:
	.ascii	"system_level_valves_are_ON_but_will_not_be_checking"
	.ascii	"_flow\000"
.LASF248:
	.ascii	"fm_delta_between_last_two_fm_pulses_4khz_foal\000"
.LASF217:
	.ascii	"used_mvor_gallons\000"
.LASF308:
	.ascii	"BUDGET_SETUP_handle_period_change\000"
.LASF184:
	.ascii	"flow_check_allow_table_to_lock\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
