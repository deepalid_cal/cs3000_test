	.file	"cal_math.c"
	.text
.Ltext0:
	.section	.text.__round_UNS16,"ax",%progbits
	.align	2
	.global	__round_UNS16
	.type	__round_UNS16, %function
__round_UNS16:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_math.c"
	.loc 1 39 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #24
.LCFI2:
	str	r0, [fp, #-28]	@ float
	.loc 1 48 0
	flds	s15, [fp, #-28]
	fcvtds	d7, s15
	sub	r3, fp, #24
	fmrrd	r0, r1, d7
	mov	r2, r3
	bl	modf
	str	r0, [fp, #-16]
	str	r1, [fp, #-12]
	.loc 1 50 0
	fldd	d6, [fp, #-16]
	fldd	d7, .L4
	fcmped	d6, d7
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L2
	.loc 1 52 0
	fldd	d6, [fp, #-24]
	fldd	d7, .L4+8
	faddd	d7, d6, d7
	ftouizd	s13, d7
	fmrs	r3, s13	@ int
	strh	r3, [fp, #-6]	@ movhi
	b	.L3
.L2:
	.loc 1 56 0
	fldd	d7, [fp, #-24]
	ftouizd	s13, d7
	fmrs	r3, s13	@ int
	strh	r3, [fp, #-6]	@ movhi
.L3:
	.loc 1 59 0
	ldrh	r3, [fp, #-6]
	.loc 1 60 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	0
	.word	1071644672
	.word	0
	.word	1072693248
.LFE0:
	.size	__round_UNS16, .-__round_UNS16
	.section	.text.roundf,"ax",%progbits
	.align	2
	.global	roundf
	.type	roundf, %function
roundf:
.LFB1:
	.loc 1 111 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #24
.LCFI5:
	str	r0, [fp, #-24]	@ float
.LBB2:
	.loc 1 119 0
	ldr	r3, [fp, #-24]	@ float
	str	r3, [fp, #-16]	@ float
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-4]
.LBE2:
	.loc 1 124 0
	ldr	r2, [fp, #-4]
	ldr	r3, .L13
	and	r3, r2, r3
	mov	r3, r3, lsr #23
	sub	r3, r3, #127
	str	r3, [fp, #-8]
	.loc 1 126 0
	ldr	r3, [fp, #-8]
	cmp	r3, #22
	bgt	.L7
	.loc 1 128 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bge	.L8
	.loc 1 130 0
	ldr	r3, [fp, #-4]
	and	r3, r3, #-2147483648
	str	r3, [fp, #-4]
	.loc 1 132 0
	ldr	r3, [fp, #-8]
	cmn	r3, #1
	bne	.L9
	.loc 1 135 0
	ldr	r3, [fp, #-4]
	orr	r3, r3, #1065353216
	str	r3, [fp, #-4]
	b	.L9
.L8:
.LBB3:
	.loc 1 140 0
	ldr	r2, .L13+4
	ldr	r3, [fp, #-8]
	mov	r3, r2, asr r3
	str	r3, [fp, #-12]
	.loc 1 142 0
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-12]
	and	r3, r2, r3
	cmp	r3, #0
	bne	.L10
	.loc 1 145 0
	flds	s15, [fp, #-24]
	b	.L11
.L10:
	.loc 1 148 0
	mov	r2, #4194304
	ldr	r3, [fp, #-8]
	mov	r3, r2, asr r3
	ldr	r2, [fp, #-4]
	add	r3, r2, r3
	str	r3, [fp, #-4]
	.loc 1 149 0
	ldr	r3, [fp, #-12]
	mvn	r3, r3
	ldr	r2, [fp, #-4]
	and	r3, r2, r3
	str	r3, [fp, #-4]
	b	.L9
.L7:
.LBE3:
	.loc 1 154 0
	ldr	r3, [fp, #-8]
	cmp	r3, #128
	bne	.L12
	.loc 1 157 0
	flds	s15, [fp, #-24]
	fadds	s15, s15, s15
	b	.L11
.L12:
	.loc 1 161 0
	flds	s15, [fp, #-24]
	b	.L11
.L9:
.LBB4:
	.loc 1 167 0
	ldr	r3, [fp, #-4]
	str	r3, [fp, #-20]
	ldr	r3, [fp, #-20]	@ float
	str	r3, [fp, #-24]	@ float
.LBE4:
	.loc 1 171 0
	flds	s15, [fp, #-24]
.L11:
	fmrs	r3, s15
	.loc 1 172 0
	mov	r0, r3	@ float
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L14:
	.align	2
.L13:
	.word	2139095040
	.word	8388607
.LFE1:
	.size	roundf, .-roundf
	.section	.text.__round_float,"ax",%progbits
	.align	2
	.global	__round_float
	.type	__round_float, %function
__round_float:
.LFB2:
	.loc 1 177 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	fstmfdd	sp!, {d8}
.LCFI7:
	add	fp, sp, #12
.LCFI8:
	sub	sp, sp, #12
.LCFI9:
	str	r0, [fp, #-20]	@ float
	str	r1, [fp, #-24]
	.loc 1 180 0
	flds	s15, [fp, #-20]
	fcvtds	d8, s15
	ldr	r3, [fp, #-24]
	fmsr	s13, r3	@ int
	fuitod	d7, s13
	adr	r1, .L16
	ldmia	r1, {r0-r1}
	fmrrd	r2, r3, d7
	bl	pow
	fmdrr	d7, r0, r1
	fmuld	d7, d8, d7
	fcvtsd	s15, d7
	fmrs	r0, s15
	bl	roundf
	fmsr	s15, r0
	fcvtds	d8, s15
	ldr	r3, [fp, #-24]
	fmsr	s13, r3	@ int
	fuitod	d7, s13
	adr	r1, .L16
	ldmia	r1, {r0-r1}
	fmrrd	r2, r3, d7
	bl	pow
	fmdrr	d7, r0, r1
	fdivd	d7, d8, d7
	fcvtsd	s15, d7
	fsts	s15, [fp, #-16]
	.loc 1 182 0
	ldr	r3, [fp, #-16]	@ float
	.loc 1 183 0
	mov	r0, r3	@ float
	sub	sp, fp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	0
	.word	1076101120
.LFE2:
	.size	__round_float, .-__round_float
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x10
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/stdint.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x202
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF28
	.byte	0x1
	.4byte	.LASF29
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x45
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x13
	.4byte	0x9a
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF14
	.uleb128 0x5
	.byte	0x4
	.byte	0x1
	.byte	0x48
	.4byte	0xc0
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x1
	.byte	0x4a
	.4byte	0x7a
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x1
	.byte	0x4c
	.4byte	0x8f
	.byte	0
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x1
	.byte	0x4e
	.4byte	0xa1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.byte	0x26
	.byte	0x1
	.4byte	0x3a
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x120
	.uleb128 0x8
	.ascii	"pfl\000"
	.byte	0x1
	.byte	0x26
	.4byte	0x120
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x1
	.byte	0x28
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x1
	.byte	0x2a
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x2c
	.4byte	0x3a
	.byte	0x2
	.byte	0x91
	.sleb128 -10
	.byte	0
	.uleb128 0xb
	.4byte	0x7a
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.byte	0x6e
	.byte	0x1
	.4byte	0x7a
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x1b9
	.uleb128 0x8
	.ascii	"x\000"
	.byte	0x1
	.byte	0x6e
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xa
	.ascii	"w\000"
	.byte	0x1
	.byte	0x70
	.4byte	0x8f
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x1
	.byte	0x73
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xc
	.4byte	.LBB2
	.4byte	.LBE2
	.4byte	0x184
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x1
	.byte	0x77
	.4byte	0xc0
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xc
	.4byte	.LBB3
	.4byte	.LBE3
	.4byte	0x1a0
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x1
	.byte	0x8c
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xd
	.4byte	.LBB4
	.4byte	.LBE4
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x1
	.byte	0xa7
	.4byte	0xc0
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.byte	0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.byte	0xb0
	.byte	0x1
	.4byte	0x7a
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x200
	.uleb128 0x8
	.ascii	"pfl\000"
	.byte	0x1
	.byte	0xb0
	.4byte	0x120
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xe
	.4byte	.LASF27
	.byte	0x1
	.byte	0xb0
	.4byte	0x200
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xa
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xb2
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xb
	.4byte	0x53
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI8
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF16:
	.ascii	"word\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF15:
	.ascii	"value\000"
.LASF25:
	.ascii	"sf_u\000"
.LASF22:
	.ascii	"exponent_less_127\000"
.LASF10:
	.ascii	"float\000"
.LASF21:
	.ascii	"roundf\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF20:
	.ascii	"__round_UNS16\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF26:
	.ascii	"__round_float\000"
.LASF14:
	.ascii	"long unsigned int\000"
.LASF18:
	.ascii	"lfrac\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF24:
	.ascii	"exponent_mask\000"
.LASF11:
	.ascii	"double\000"
.LASF29:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/cal_math.c\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF0:
	.ascii	"char\000"
.LASF17:
	.ascii	"ieee_float_shape_type\000"
.LASF9:
	.ascii	"long long int\000"
.LASF23:
	.ascii	"gf_u\000"
.LASF19:
	.ascii	"lint\000"
.LASF27:
	.ascii	"pprecision\000"
.LASF4:
	.ascii	"short int\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF28:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF13:
	.ascii	"uint32_t\000"
.LASF12:
	.ascii	"long int\000"
.LASF2:
	.ascii	"signed char\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
