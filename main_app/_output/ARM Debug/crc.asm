	.file	"crc.c"
	.text
.Ltext0:
	.section	.rodata.Crc32Table,"a",%progbits
	.align	2
	.type	Crc32Table, %object
	.size	Crc32Table, 1024
Crc32Table:
	.word	0
	.word	1996959894
	.word	-301047508
	.word	-1727442502
	.word	124634137
	.word	1886057615
	.word	-379345611
	.word	-1637575261
	.word	249268274
	.word	2044508324
	.word	-522852066
	.word	-1747789432
	.word	162941995
	.word	2125561021
	.word	-407360249
	.word	-1866523247
	.word	498536548
	.word	1789927666
	.word	-205950648
	.word	-2067906082
	.word	450548861
	.word	1843258603
	.word	-187386543
	.word	-2083289657
	.word	325883990
	.word	1684777152
	.word	-43845254
	.word	-1973040660
	.word	335633487
	.word	1661365465
	.word	-99664541
	.word	-1928851979
	.word	997073096
	.word	1281953886
	.word	-715111964
	.word	-1570279054
	.word	1006888145
	.word	1258607687
	.word	-770865667
	.word	-1526024853
	.word	901097722
	.word	1119000684
	.word	-608450090
	.word	-1396901568
	.word	853044451
	.word	1172266101
	.word	-589951537
	.word	-1412350631
	.word	651767980
	.word	1373503546
	.word	-925412992
	.word	-1076862698
	.word	565507253
	.word	1454621731
	.word	-809855591
	.word	-1195530993
	.word	671266974
	.word	1594198024
	.word	-972236366
	.word	-1324619484
	.word	795835527
	.word	1483230225
	.word	-1050600021
	.word	-1234817731
	.word	1994146192
	.word	31158534
	.word	-1731059524
	.word	-271249366
	.word	1907459465
	.word	112637215
	.word	-1614814043
	.word	-390540237
	.word	2013776290
	.word	251722036
	.word	-1777751922
	.word	-519137256
	.word	2137656763
	.word	141376813
	.word	-1855689577
	.word	-429695999
	.word	1802195444
	.word	476864866
	.word	-2056965928
	.word	-228458418
	.word	1812370925
	.word	453092731
	.word	-2113342271
	.word	-183516073
	.word	1706088902
	.word	314042704
	.word	-1950435094
	.word	-54949764
	.word	1658658271
	.word	366619977
	.word	-1932296973
	.word	-69972891
	.word	1303535960
	.word	984961486
	.word	-1547960204
	.word	-725929758
	.word	1256170817
	.word	1037604311
	.word	-1529756563
	.word	-740887301
	.word	1131014506
	.word	879679996
	.word	-1385723834
	.word	-631195440
	.word	1141124467
	.word	855842277
	.word	-1442165665
	.word	-586318647
	.word	1342533948
	.word	654459306
	.word	-1106571248
	.word	-921952122
	.word	1466479909
	.word	544179635
	.word	-1184443383
	.word	-832445281
	.word	1591671054
	.word	702138776
	.word	-1328506846
	.word	-942167884
	.word	1504918807
	.word	783551873
	.word	-1212326853
	.word	-1061524307
	.word	-306674912
	.word	-1698712650
	.word	62317068
	.word	1957810842
	.word	-355121351
	.word	-1647151185
	.word	81470997
	.word	1943803523
	.word	-480048366
	.word	-1805370492
	.word	225274430
	.word	2053790376
	.word	-468791541
	.word	-1828061283
	.word	167816743
	.word	2097651377
	.word	-267414716
	.word	-2029476910
	.word	503444072
	.word	1762050814
	.word	-144550051
	.word	-2140837941
	.word	426522225
	.word	1852507879
	.word	-19653770
	.word	-1982649376
	.word	282753626
	.word	1742555852
	.word	-105259153
	.word	-1900089351
	.word	397917763
	.word	1622183637
	.word	-690576408
	.word	-1580100738
	.word	953729732
	.word	1340076626
	.word	-776247311
	.word	-1497606297
	.word	1068828381
	.word	1219638859
	.word	-670225446
	.word	-1358292148
	.word	906185462
	.word	1090812512
	.word	-547295293
	.word	-1469587627
	.word	829329135
	.word	1181335161
	.word	-882789492
	.word	-1134132454
	.word	628085408
	.word	1382605366
	.word	-871598187
	.word	-1156888829
	.word	570562233
	.word	1426400815
	.word	-977650754
	.word	-1296233688
	.word	733239954
	.word	1555261956
	.word	-1026031705
	.word	-1244606671
	.word	752459403
	.word	1541320221
	.word	-1687895376
	.word	-328994266
	.word	1969922972
	.word	40735498
	.word	-1677130071
	.word	-351390145
	.word	1913087877
	.word	83908371
	.word	-1782625662
	.word	-491226604
	.word	2075208622
	.word	213261112
	.word	-1831694693
	.word	-438977011
	.word	2094854071
	.word	198958881
	.word	-2032938284
	.word	-237706686
	.word	1759359992
	.word	534414190
	.word	-2118248755
	.word	-155638181
	.word	1873836001
	.word	414664567
	.word	-2012718362
	.word	-15766928
	.word	1711684554
	.word	285281116
	.word	-1889165569
	.word	-127750551
	.word	1634467795
	.word	376229701
	.word	-1609899400
	.word	-686959890
	.word	1308918612
	.word	956543938
	.word	-1486412191
	.word	-799009033
	.word	1231636301
	.word	1047427035
	.word	-1362007478
	.word	-640263460
	.word	1088359270
	.word	936918000
	.word	-1447252397
	.word	-558129467
	.word	1202900863
	.word	817233897
	.word	-1111625188
	.word	-893730166
	.word	1404277552
	.word	615818150
	.word	-1160759803
	.word	-841546093
	.word	1423857449
	.word	601450431
	.word	-1285129682
	.word	-1000256840
	.word	1567103746
	.word	711928724
	.word	-1274298825
	.word	-1022587231
	.word	1510334235
	.word	755167117
	.section	.text.CRC_calculate_32bit_big_endian,"ax",%progbits
	.align	2
	.global	CRC_calculate_32bit_big_endian
	.type	CRC_calculate_32bit_big_endian, %function
CRC_calculate_32bit_big_endian:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/crc.c"
	.loc 1 71 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #28
.LCFI2:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	.loc 1 78 0
	mvn	r3, #0
	str	r3, [fp, #-16]
	.loc 1 82 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L2
.L3:
	.loc 1 86 0 discriminator 2
	ldr	r3, [fp, #-16]
	and	r2, r3, #255
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	eor	r3, r2, r3
	and	r3, r3, #255
	str	r3, [fp, #-8]
	.loc 1 88 0 discriminator 2
	ldr	r3, .L4
	ldr	r2, [fp, #-8]
	ldr	r2, [r3, r2, asl #2]
	ldr	r3, [fp, #-16]
	mov	r3, r3, lsr #8
	eor	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 90 0 discriminator 2
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
	.loc 1 82 0 discriminator 2
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L2:
	.loc 1 82 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-4]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bcc	.L3
	.loc 1 96 0 is_stmt 1
	ldr	r3, [fp, #-16]
	mvn	r3, r3
	str	r3, [fp, #-16]
	.loc 1 109 0
	sub	r3, fp, #20
	str	r3, [fp, #-12]
	.loc 1 111 0
	ldrb	r2, [fp, #-13]	@ zero_extendqisi2
	ldr	r3, [fp, #-12]
	strb	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 112 0
	ldrb	r2, [fp, #-14]	@ zero_extendqisi2
	ldr	r3, [fp, #-12]
	strb	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 113 0
	ldrb	r2, [fp, #-15]	@ zero_extendqisi2
	ldr	r3, [fp, #-12]
	strb	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 114 0
	ldrb	r2, [fp, #-16]	@ zero_extendqisi2
	ldr	r3, [fp, #-12]
	strb	r2, [r3, #0]
	.loc 1 118 0
	ldr	r3, [fp, #-20]
	.loc 1 119 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L5:
	.align	2
.L4:
	.word	Crc32Table
.LFE0:
	.size	CRC_calculate_32bit_big_endian, .-CRC_calculate_32bit_big_endian
	.section	.text.CRCIsOK,"ax",%progbits
	.align	2
	.global	CRCIsOK
	.type	CRCIsOK, %function
CRCIsOK:
.LFB1:
	.loc 1 176 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #24
.LCFI5:
	str	r0, [fp, #-28]
	str	r1, [fp, #-24]
	.loc 1 183 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 189 0
	ldr	r3, [fp, #-24]
	cmp	r3, #4
	bls	.L7
	.loc 1 194 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-12]
	.loc 1 195 0
	ldr	r3, [fp, #-24]
	sub	r3, r3, #4
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 199 0
	sub	r3, fp, #20
	mov	r0, r3
	ldr	r1, [fp, #-12]
	mov	r2, #4
	bl	memcpy
	.loc 1 203 0
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-24]
	sub	r3, r3, #4
	mov	r0, r2
	mov	r1, r3
	bl	CRC_calculate_32bit_big_endian
	str	r0, [fp, #-16]
	.loc 1 207 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	movne	r3, #0
	moveq	r3, #1
	str	r3, [fp, #-8]
.L7:
	.loc 1 228 0
	ldr	r3, [fp, #-8]
	.loc 1 229 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE1:
	.size	CRCIsOK, .-CRCIsOK
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/crc.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x208
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF27
	.byte	0x1
	.4byte	.LASF28
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x5
	.byte	0x8
	.byte	0x3
	.byte	0x14
	.4byte	0xb1
	.uleb128 0x6
	.4byte	.LASF12
	.byte	0x3
	.byte	0x17
	.4byte	0xb1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF13
	.byte	0x3
	.byte	0x1a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x33
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x1c
	.4byte	0x8c
	.uleb128 0x8
	.byte	0x4
	.byte	0x4
	.byte	0x19
	.4byte	0xdf
	.uleb128 0x9
	.ascii	"b\000"
	.byte	0x4
	.byte	0x1b
	.4byte	0xdf
	.uleb128 0x9
	.ascii	"crc\000"
	.byte	0x4
	.byte	0x1d
	.4byte	0x5a
	.byte	0
	.uleb128 0xa
	.4byte	0x33
	.4byte	0xef
	.uleb128 0xb
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x4
	.byte	0x1f
	.4byte	0xc2
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.byte	0x46
	.byte	0x1
	.4byte	0x5a
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x178
	.uleb128 0xd
	.4byte	.LASF16
	.byte	0x1
	.byte	0x46
	.4byte	0xb1
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xd
	.4byte	.LASF17
	.byte	0x1
	.byte	0x46
	.4byte	0x178
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xe
	.ascii	"i\000"
	.byte	0x1
	.byte	0x48
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xf
	.4byte	.LASF18
	.byte	0x1
	.byte	0x48
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xf
	.4byte	.LASF19
	.byte	0x1
	.byte	0x4a
	.4byte	0xef
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xf
	.4byte	.LASF20
	.byte	0x1
	.byte	0x69
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xf
	.4byte	.LASF21
	.byte	0x1
	.byte	0x6b
	.4byte	0xb1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x10
	.4byte	0x5a
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.byte	0xaf
	.byte	0x1
	.4byte	0x81
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x1e0
	.uleb128 0x11
	.ascii	"pdh\000"
	.byte	0x1
	.byte	0xaf
	.4byte	0x1e0
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xe
	.ascii	"ucp\000"
	.byte	0x1
	.byte	0xb1
	.4byte	0xb1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xf
	.4byte	.LASF24
	.byte	0x1
	.byte	0xb3
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xf
	.4byte	.LASF25
	.byte	0x1
	.byte	0xb3
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xe
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xb5
	.4byte	0x81
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.4byte	0xb7
	.uleb128 0xa
	.4byte	0x25
	.4byte	0x1f5
	.uleb128 0xb
	.4byte	0x25
	.byte	0xff
	.byte	0
	.uleb128 0xf
	.4byte	.LASF26
	.byte	0x1
	.byte	0x13
	.4byte	0x206
	.byte	0x5
	.byte	0x3
	.4byte	Crc32Table
	.uleb128 0x10
	.4byte	0x1e5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF17:
	.ascii	"pdlen\000"
.LASF16:
	.ascii	"pData_uc\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF21:
	.ascii	"lucp\000"
.LASF23:
	.ascii	"CRCIsOK\000"
.LASF13:
	.ascii	"dlen\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF14:
	.ascii	"DATA_HANDLE\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF22:
	.ascii	"CRC_calculate_32bit_big_endian\000"
.LASF25:
	.ascii	"calculated_crc\000"
.LASF18:
	.ascii	"table_index\000"
.LASF28:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/../commo"
	.ascii	"n_includes/crc.c\000"
.LASF6:
	.ascii	"UNS_8\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF20:
	.ascii	"final_crc\000"
.LASF1:
	.ascii	"char\000"
.LASF12:
	.ascii	"dptr\000"
.LASF19:
	.ascii	"lcrc\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF10:
	.ascii	"long long int\000"
.LASF5:
	.ascii	"short int\000"
.LASF24:
	.ascii	"crc_to_test\000"
.LASF27:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF26:
	.ascii	"Crc32Table\000"
.LASF15:
	.ascii	"CRC_TYPE\000"
.LASF3:
	.ascii	"signed char\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
