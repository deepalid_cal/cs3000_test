	.file	"foal_irri.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.text.rain_switch_timer_callback,"ax",%progbits
	.align	2
	.type	rain_switch_timer_callback, %function
rain_switch_timer_callback:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_irri.c"
	.loc 1 77 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 79 0
	ldr	r3, .L2
	mov	r2, #0
	str	r2, [r3, #100]
	.loc 1 81 0
	bl	Alert_rain_switch_inactive
	.loc 1 82 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L3:
	.align	2
.L2:
	.word	weather_preserves
.LFE0:
	.size	rain_switch_timer_callback, .-rain_switch_timer_callback
	.section	.text.freeze_switch_timer_callback,"ax",%progbits
	.align	2
	.type	freeze_switch_timer_callback, %function
freeze_switch_timer_callback:
.LFB1:
	.loc 1 86 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 89 0
	ldr	r3, .L5
	mov	r2, #0
	str	r2, [r3, #104]
	.loc 1 91 0
	bl	Alert_freeze_switch_inactive
	.loc 1 92 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	weather_preserves
.LFE1:
	.size	freeze_switch_timer_callback, .-freeze_switch_timer_callback
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_irri.c\000"
	.align	2
.LC1:
	.ascii	"Error removing from list : %s, %u\000"
	.section	.text.FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
	.type	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down, %function
FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down:
.LFB2:
	.loc 1 118 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #16
.LCFI8:
	str	r0, [fp, #-20]
	.loc 1 134 0
	ldr	r3, .L17
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L17+4
	mov	r3, #134
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 136 0
	ldr	r3, .L17+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L17+4
	mov	r3, #136
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 141 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L8
	.loc 1 146 0
	ldr	r0, .L17+12
	mov	r1, #0
	bl	nm_ListInit
	.loc 1 148 0
	ldr	r0, .L17+16
	mov	r1, #12
	bl	nm_ListInit
	.loc 1 151 0
	ldr	r0, .L17+20
	mov	r1, #24
	bl	nm_ListInit
	.loc 1 155 0
	ldr	r0, .L17+24
	mov	r1, #0
	mov	r2, #73728
	bl	memset
	.loc 1 159 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L9
.L10:
	.loc 1 161 0 discriminator 2
	ldr	r0, .L17+28
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r2, r3, r1
	ldrb	r3, [r2, #2]
	bic	r3, r3, #8
	strb	r3, [r2, #2]
	.loc 1 162 0 discriminator 2
	ldr	r0, .L17+28
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r2, r3, r1
	ldrb	r3, [r2, #2]
	bic	r3, r3, #16
	strb	r3, [r2, #2]
	.loc 1 163 0 discriminator 2
	ldr	r0, .L17+28
	ldr	r2, [fp, #-8]
	mov	r1, #460
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 165 0 discriminator 2
	ldr	r0, .L17+28
	ldr	r2, [fp, #-8]
	mov	r1, #484
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r2, r3, r1
	ldrb	r3, [r2, #0]
	bic	r3, r3, #32
	strb	r3, [r2, #0]
	.loc 1 166 0 discriminator 2
	ldr	r0, .L17+28
	ldr	r2, [fp, #-8]
	mov	r1, #484
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r2, r3, r1
	ldrb	r3, [r2, #0]
	bic	r3, r3, #64
	strb	r3, [r2, #0]
	.loc 1 167 0 discriminator 2
	ldr	r0, .L17+28
	ldr	r2, [fp, #-8]
	ldr	r1, .L17+32
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 159 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L9:
	.loc 1 159 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bls	.L10
	.loc 1 159 0
	b	.L11
.L8:
.LBB2:
	.loc 1 186 0 is_stmt 1
	ldr	r3, .L17+36
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 188 0
	ldr	r3, .L17+36
	mov	r2, #0
	str	r2, [r3, #52]
	.loc 1 190 0
	ldr	r3, .L17+36
	mov	r2, #0
	str	r2, [r3, #72]
	.loc 1 209 0
	ldr	r3, .L17+36
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 213 0
	ldr	r0, .L17+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 215 0
	b	.L12
.L14:
.LBB3:
	.loc 1 223 0
	ldr	r0, .L17+12
	ldr	r1, [fp, #-12]
	mov	r2, #103
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	str	r0, [fp, #-16]
	.loc 1 237 0
	ldr	r0, .L17+20
	ldr	r1, [fp, #-12]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L13
	.loc 1 239 0
	ldr	r0, .L17+20
	ldr	r1, [fp, #-12]
	ldr	r2, .L17+4
	mov	r3, #239
	bl	nm_ListRemove_debug
	mov	r3, r0
	cmp	r3, #0
	beq	.L13
	.loc 1 241 0
	ldr	r0, .L17+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L17+40
	mov	r1, r3
	mov	r2, #241
	bl	Alert_Message_va
.L13:
	.loc 1 249 0
	ldr	r2, [fp, #-12]
	ldrb	r3, [r2, #76]
	orr	r3, r3, #2
	strb	r3, [r2, #76]
	.loc 1 254 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-12]
.L12:
.LBE3:
	.loc 1 215 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L14
.L11:
.LBE2:
	.loc 1 260 0
	ldr	r0, .L17+44
	mov	r1, #0
	mov	r2, #48
	bl	memset
	.loc 1 264 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L15
.L16:
	.loc 1 275 0 discriminator 2
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	ldr	r3, .L17+48
	add	r3, r2, r3
	mov	r0, r3
	bl	nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds
	.loc 1 264 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L15:
	.loc 1 264 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bls	.L16
	.loc 1 280 0 is_stmt 1
	ldr	r3, .L17+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 282 0
	ldr	r3, .L17
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 290 0
	ldr	r3, .L17+36
	mov	r2, #0
	str	r2, [r3, #136]
	.loc 1 291 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L18:
	.align	2
.L17:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	system_preserves_recursive_MUTEX
	.word	foal_irri+16
	.word	foal_irri+36
	.word	foal_irri+56
	.word	foal_irri+140
	.word	system_preserves
	.word	14140
	.word	foal_irri
	.word	.LC1
	.word	foal_irri+80
	.word	system_preserves+16
.LFE2:
	.size	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down, .-FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
	.section	.text.FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation
	.type	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation, %function
FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation:
.LFB3:
	.loc 1 295 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	.loc 1 300 0
	mov	r0, #1
	bl	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
	.loc 1 302 0
	bl	IRRI_restart_all_of_irrigation
	.loc 1 312 0
	ldmfd	sp!, {fp, pc}
.LFE3:
	.size	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation, .-FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation
	.section	.rodata.FI_PRE_TEST_STRING,"a",%progbits
	.align	2
	.type	FI_PRE_TEST_STRING, %object
	.size	FI_PRE_TEST_STRING, 14
FI_PRE_TEST_STRING:
	.ascii	"foal_irri v04\000"
	.section .rodata
	.align	2
.LC2:
	.ascii	"Foal Irri\000"
	.align	2
.LC3:
	.ascii	"rain switch timer\000"
	.align	2
.LC4:
	.ascii	"Timer NOT CREATED : %s, %u\000"
	.align	2
.LC5:
	.ascii	"freeze switch timer\000"
	.section	.text.init_battery_backed_foal_irri,"ax",%progbits
	.align	2
	.global	init_battery_backed_foal_irri
	.type	init_battery_backed_foal_irri, %function
init_battery_backed_foal_irri:
.LFB4:
	.loc 1 338 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	sub	sp, sp, #8
.LCFI13:
	str	r0, [fp, #-8]
	.loc 1 339 0
	ldr	r3, .L26
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L26+4
	ldr	r3, .L26+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 344 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L21
	.loc 1 344 0 is_stmt 0 discriminator 1
	ldr	r0, .L26+12
	ldr	r1, .L26+16
	mov	r2, #14
	bl	strncmp
	mov	r3, r0
	cmp	r3, #0
	beq	.L22
.L21:
	.loc 1 346 0 is_stmt 1
	ldr	r0, .L26+20
	ldr	r1, [fp, #-8]
	bl	Alert_battery_backed_var_initialized
	.loc 1 351 0
	ldr	r0, .L26+12
	mov	r1, #0
	ldr	r2, .L26+24
	bl	memset
	.loc 1 355 0
	mov	r0, #1
	bl	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
	.loc 1 359 0
	ldr	r0, .L26+12
	ldr	r1, .L26+16
	mov	r2, #16
	bl	strlcpy
	b	.L23
.L22:
	.loc 1 371 0
	mov	r0, #0
	bl	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
.L23:
	.loc 1 386 0
	ldr	r3, .L26+28
	str	r3, [sp, #0]
	ldr	r0, .L26+32
	mov	r1, #4000
	mov	r2, #0
	mov	r3, #0
	bl	xTimerCreate
	mov	r2, r0
	ldr	r3, .L26+12
	str	r2, [r3, #128]
	.loc 1 388 0
	ldr	r3, .L26+12
	ldr	r3, [r3, #128]
	cmp	r3, #0
	bne	.L24
	.loc 1 390 0
	ldr	r0, .L26+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L26+36
	mov	r1, r3
	ldr	r2, .L26+40
	bl	Alert_Message_va
.L24:
	.loc 1 395 0
	ldr	r3, .L26+44
	str	r3, [sp, #0]
	ldr	r0, .L26+48
	mov	r1, #4000
	mov	r2, #0
	mov	r3, #0
	bl	xTimerCreate
	mov	r2, r0
	ldr	r3, .L26+12
	str	r2, [r3, #132]
	.loc 1 397 0
	ldr	r3, .L26+12
	ldr	r3, [r3, #132]
	cmp	r3, #0
	bne	.L25
	.loc 1 399 0
	ldr	r0, .L26+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L26+36
	mov	r1, r3
	ldr	r2, .L26+52
	bl	Alert_Message_va
.L25:
	.loc 1 407 0
	ldr	r0, .L26+56
	mov	r1, #0
	mov	r2, #128
	bl	memset
	.loc 1 411 0
	ldr	r3, .L26
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 412 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L27:
	.align	2
.L26:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	339
	.word	foal_irri
	.word	FI_PRE_TEST_STRING
	.word	.LC2
	.word	74048
	.word	rain_switch_timer_callback
	.word	.LC3
	.word	.LC4
	.word	390
	.word	freeze_switch_timer_callback
	.word	.LC5
	.word	399
	.word	foal_irri+73920
.LFE4:
	.size	init_battery_backed_foal_irri, .-init_battery_backed_foal_irri
	.section .rodata
	.align	2
.LC6:
	.ascii	"FOAL main list must have changed : %s, %u\000"
	.align	2
.LC7:
	.ascii	"FOAL_IRRI: token info error : %s, %u\000"
	.section	.text.FOAL_IRRI_load_sfml_for_distribution_to_slaves,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_load_sfml_for_distribution_to_slaves
	.type	FOAL_IRRI_load_sfml_for_distribution_to_slaves, %function
FOAL_IRRI_load_sfml_for_distribution_to_slaves:
.LFB5:
	.loc 1 437 0
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI14:
	add	fp, sp, #4
.LCFI15:
	sub	sp, sp, #52
.LCFI16:
	str	r0, [fp, #-52]
	str	r1, [fp, #-56]
	.loc 1 442 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 444 0
	ldr	r3, [fp, #-56]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 451 0
	ldr	r3, .L39
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L39+4
	ldr	r3, .L39+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 458 0
	mov	r3, #0
	strh	r3, [fp, #-22]	@ movhi
	.loc 1 460 0
	ldr	r0, .L39+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 462 0
	b	.L29
.L31:
	.loc 1 465 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #76]	@ zero_extendqisi2
	and	r3, r3, #2
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L30
	.loc 1 467 0
	ldrh	r3, [fp, #-22]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-22]	@ movhi
.L30:
	.loc 1 479 0
	ldr	r0, .L39+12
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L29:
	.loc 1 462 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L31
	.loc 1 482 0
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	beq	.L32
.LBB4:
	.loc 1 491 0
	ldrh	r3, [fp, #-22]
	mov	r2, r3
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, #2
	str	r3, [fp, #-16]
	.loc 1 493 0
	ldr	r0, [fp, #-16]
	ldr	r1, .L39+4
	ldr	r2, .L39+16
	bl	mem_malloc_debug
	str	r0, [fp, #-20]
	.loc 1 495 0
	ldr	r3, [fp, #-56]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #0]
	.loc 1 497 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-8]
	.loc 1 500 0
	sub	r3, fp, #22
	ldr	r0, [fp, #-20]
	mov	r1, r3
	mov	r2, #2
	bl	memcpy
	.loc 1 502 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #2
	str	r3, [fp, #-20]
	.loc 1 504 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #2
	str	r3, [fp, #-16]
	.loc 1 507 0
	ldr	r0, .L39+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 509 0
	b	.L33
.L37:
	.loc 1 511 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #76]	@ zero_extendqisi2
	and	r3, r3, #2
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L34
.LBB5:
	.loc 1 513 0
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	bne	.L35
	.loc 1 516 0
	ldr	r0, .L39+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L39+20
	mov	r1, r3
	mov	r2, #516
	bl	Alert_Message_va
	.loc 1 518 0
	b	.L36
.L35:
	.loc 1 521 0
	ldrh	r3, [fp, #-22]
	sub	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-22]	@ movhi
	.loc 1 525 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-48]
	.loc 1 526 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	str	r3, [fp, #-32]
	.loc 1 527 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	strb	r3, [fp, #-28]
	.loc 1 528 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	strb	r3, [fp, #-27]
	.loc 1 529 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #52]
	str	r3, [fp, #-44]
	.loc 1 530 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #56]
	str	r3, [fp, #-36]
	.loc 1 531 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #60]
	str	r3, [fp, #-40]
	.loc 1 533 0
	sub	r3, fp, #48
	ldr	r0, [fp, #-20]
	mov	r1, r3
	mov	r2, #24
	bl	memcpy
	.loc 1 535 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #24
	str	r3, [fp, #-20]
	.loc 1 539 0
	ldr	r2, [fp, #-12]
	ldrb	r3, [r2, #76]
	bic	r3, r3, #2
	strb	r3, [r2, #76]
	.loc 1 542 0
	ldr	r3, [fp, #-16]
	sub	r3, r3, #24
	str	r3, [fp, #-16]
.L34:
.LBE5:
	.loc 1 545 0
	ldr	r0, .L39+12
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L33:
	.loc 1 509 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L37
.L36:
	.loc 1 549 0
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	bne	.L38
	.loc 1 549 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L32
.L38:
	.loc 1 551 0 is_stmt 1
	ldr	r0, .L39+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L39+24
	mov	r1, r3
	ldr	r2, .L39+28
	bl	Alert_Message_va
.L32:
.LBE4:
	.loc 1 556 0
	ldr	r3, .L39
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 558 0
	ldr	r3, [fp, #-8]
	.loc 1 559 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L40:
	.align	2
.L39:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	451
	.word	foal_irri+16
	.word	493
	.word	.LC6
	.word	.LC7
	.word	551
.LFE5:
	.size	FOAL_IRRI_load_sfml_for_distribution_to_slaves, .-FOAL_IRRI_load_sfml_for_distribution_to_slaves
	.section .rodata
	.align	2
.LC8:
	.ascii	"Cycle=0 (out of range)\000"
	.global	__umodsi3
	.global	__udivsi3
	.section	.text.foal_irri_cycle_and_soak_weighting,"ax",%progbits
	.align	2
	.type	foal_irri_cycle_and_soak_weighting, %function
foal_irri_cycle_and_soak_weighting:
.LFB6:
	.loc 1 576 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI17:
	add	fp, sp, #4
.LCFI18:
	sub	sp, sp, #32
.LCFI19:
	str	r0, [fp, #-32]
	str	r1, [fp, #-36]
	.loc 1 585 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 587 0
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	beq	.L42
	.loc 1 589 0
	ldr	r3, [fp, #-36]
	str	r3, [fp, #-24]
	.loc 1 591 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #60]
	str	r3, [fp, #-8]
	.loc 1 595 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #72]
	str	r3, [fp, #-12]
	.loc 1 597 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #80]
	str	r3, [fp, #-16]
	b	.L43
.L42:
	.loc 1 601 0
	ldr	r3, [fp, #-36]
	str	r3, [fp, #-28]
	.loc 1 603 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #52]
	str	r3, [fp, #-8]
	.loc 1 605 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #56]
	str	r3, [fp, #-12]
	.loc 1 607 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #64]
	str	r3, [fp, #-16]
.L43:
	.loc 1 613 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L44
	.loc 1 615 0
	ldr	r0, .L49
	bl	Alert_Message
	.loc 1 617 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L45
.L44:
	.loc 1 621 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L46
	.loc 1 631 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L45
.L46:
	.loc 1 641 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bhi	.L47
	.loc 1 643 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L45
.L47:
	.loc 1 646 0
	ldr	r3, [fp, #-8]
	mov	r0, r3
	ldr	r1, [fp, #-12]
	bl	__umodsi3
	mov	r3, r0
	cmp	r3, #0
	bne	.L48
	.loc 1 649 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	__udivsi3
	mov	r3, r0
	sub	r3, r3, #1
	ldr	r2, [fp, #-16]
	mul	r3, r2, r3
	str	r3, [fp, #-20]
	b	.L45
.L48:
	.loc 1 654 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	__udivsi3
	mov	r3, r0
	ldr	r2, [fp, #-16]
	mul	r3, r2, r3
	str	r3, [fp, #-20]
.L45:
	.loc 1 659 0
	ldr	r3, [fp, #-20]
	.loc 1 660 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L50:
	.align	2
.L49:
	.word	.LC8
.LFE6:
	.size	foal_irri_cycle_and_soak_weighting, .-foal_irri_cycle_and_soak_weighting
	.section	.text._nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting,"ax",%progbits
	.align	2
	.type	_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting, %function
_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting:
.LFB7:
	.loc 1 664 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI20:
	add	fp, sp, #0
.LCFI21:
	sub	sp, sp, #8
.LCFI22:
	str	r0, [fp, #-8]
	.loc 1 667 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 671 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #0]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L52
	.loc 1 676 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	and	r3, r3, #16
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L53
	.loc 1 678 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #65536
	str	r3, [fp, #-4]
.L53:
	.loc 1 687 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #48
	and	r3, r3, #255
	cmp	r3, #48
	bne	.L54
	.loc 1 689 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #28672
	str	r3, [fp, #-4]
	b	.L52
.L54:
	.loc 1 692 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #48
	and	r3, r3, #255
	cmp	r3, #32
	bne	.L55
	.loc 1 694 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #24576
	str	r3, [fp, #-4]
	b	.L52
.L55:
	.loc 1 697 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #48
	and	r3, r3, #255
	cmp	r3, #16
	bne	.L52
	.loc 1 699 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #20480
	str	r3, [fp, #-4]
.L52:
	.loc 1 707 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L56
	.loc 1 709 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #256
	str	r3, [fp, #-4]
.L56:
	.loc 1 715 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L57
	.loc 1 717 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #16
	str	r3, [fp, #-4]
.L57:
	.loc 1 723 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L58
	.loc 1 730 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #3
	and	r3, r3, #255
	ldr	r2, [fp, #-4]
	add	r3, r2, r3
	str	r3, [fp, #-4]
.L58:
	.loc 1 735 0
	ldr	r3, [fp, #-4]
	.loc 1 736 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE7:
	.size	_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting, .-_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting
	.section	.text.nm_this_station_weighs_MORE_than_the_list_station,"ax",%progbits
	.align	2
	.type	nm_this_station_weighs_MORE_than_the_list_station, %function
nm_this_station_weighs_MORE_than_the_list_station:
.LFB8:
	.loc 1 772 0
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI23:
	add	fp, sp, #8
.LCFI24:
	sub	sp, sp, #56
.LCFI25:
	str	r0, [fp, #-56]
	str	r1, [fp, #-60]
	str	r2, [fp, #-64]
	.loc 1 834 0
	ldr	r3, [fp, #-60]
	str	r3, [fp, #-32]
	.loc 1 836 0
	ldr	r3, [fp, #-64]
	str	r3, [fp, #-36]
	.loc 1 838 0
	ldr	r3, [fp, #-60]
	str	r3, [fp, #-40]
	.loc 1 840 0
	ldr	r3, [fp, #-64]
	str	r3, [fp, #-44]
	.loc 1 846 0
	ldr	r3, [fp, #-56]
	cmp	r3, #0
	beq	.L60
	.loc 1 848 0
	ldr	r3, [fp, #-32]
	ldrh	r3, [r3, #104]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	strb	r3, [fp, #-9]
	.loc 1 850 0
	ldr	r3, [fp, #-36]
	ldrh	r3, [r3, #104]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	strb	r3, [fp, #-10]
	.loc 1 852 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 854 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 856 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #104
	str	r3, [fp, #-24]
	.loc 1 858 0
	ldr	r3, [fp, #-36]
	add	r3, r3, #104
	str	r3, [fp, #-28]
	b	.L61
.L60:
	.loc 1 862 0
	ldr	r3, [fp, #-40]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	strb	r3, [fp, #-9]
	.loc 1 864 0
	ldr	r3, [fp, #-44]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	strb	r3, [fp, #-10]
	.loc 1 866 0
	ldr	r3, [fp, #-40]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	mov	r3, r3, lsr #5
	and	r3, r3, #1
	and	r3, r3, #255
	str	r3, [fp, #-16]
	.loc 1 868 0
	ldr	r3, [fp, #-44]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	mov	r3, r3, lsr #5
	and	r3, r3, #1
	and	r3, r3, #255
	str	r3, [fp, #-20]
	.loc 1 870 0
	ldr	r3, [fp, #-40]
	add	r3, r3, #72
	str	r3, [fp, #-24]
	.loc 1 872 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #72
	str	r3, [fp, #-28]
.L61:
	.loc 1 877 0
	ldrb	r2, [fp, #-9]	@ zero_extendqisi2
	ldrb	r3, [fp, #-10]	@ zero_extendqisi2
	cmp	r2, r3
	bls	.L62
	.loc 1 879 0
	mov	r3, #1
	b	.L63
.L62:
	.loc 1 882 0
	ldrb	r2, [fp, #-9]	@ zero_extendqisi2
	ldrb	r3, [fp, #-10]	@ zero_extendqisi2
	cmp	r2, r3
	bcs	.L64
	.loc 1 886 0
	mov	r3, #0
	b	.L63
.L64:
	.loc 1 891 0
	ldr	r3, [fp, #-56]
	cmp	r3, #0
	bne	.L65
	.loc 1 897 0
	ldr	r3, [fp, #-40]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #320
	bne	.L65
	.loc 1 899 0
	mov	r3, #0
	b	.L63
.L65:
	.loc 1 911 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L66
	.loc 1 913 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L67
	.loc 1 917 0
	mov	r3, #0
	b	.L63
.L67:
	.loc 1 923 0
	mov	r3, #1
	b	.L63
.L66:
	.loc 1 930 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L68
	.loc 1 932 0
	mov	r3, #0
	b	.L63
.L68:
	.loc 1 939 0
	ldr	r0, [fp, #-24]
	bl	_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting
	str	r0, [fp, #-48]
	.loc 1 941 0
	ldr	r0, [fp, #-28]
	bl	_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting
	str	r0, [fp, #-52]
	.loc 1 943 0
	ldr	r2, [fp, #-48]
	ldr	r3, [fp, #-52]
	cmp	r2, r3
	bls	.L69
	.loc 1 945 0
	mov	r3, #1
	b	.L63
.L69:
	.loc 1 948 0
	ldr	r2, [fp, #-48]
	ldr	r3, [fp, #-52]
	cmp	r2, r3
	bcs	.L70
	.loc 1 950 0
	mov	r3, #0
	b	.L63
.L70:
	.loc 1 963 0
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #0]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L71
	.loc 1 965 0
	ldr	r0, [fp, #-56]
	ldr	r1, [fp, #-60]
	bl	foal_irri_cycle_and_soak_weighting
	mov	r4, r0
	ldr	r0, [fp, #-56]
	ldr	r1, [fp, #-64]
	bl	foal_irri_cycle_and_soak_weighting
	mov	r3, r0
	cmp	r4, r3
	bls	.L72
	.loc 1 967 0
	mov	r3, #1
	b	.L63
.L72:
	.loc 1 971 0
	mov	r3, #0
	b	.L63
.L71:
	.loc 1 978 0
	mov	r3, #0
.L63:
	.loc 1 981 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE8:
	.size	nm_this_station_weighs_MORE_than_the_list_station, .-nm_this_station_weighs_MORE_than_the_list_station
	.section	.text.nm_return_station_this_station_belongs_before_in_the_irrigation_list,"ax",%progbits
	.align	2
	.type	nm_return_station_this_station_belongs_before_in_the_irrigation_list, %function
nm_return_station_this_station_belongs_before_in_the_irrigation_list:
.LFB9:
	.loc 1 1003 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI26:
	add	fp, sp, #4
.LCFI27:
	sub	sp, sp, #16
.LCFI28:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	.loc 1 1014 0
	ldr	r0, [fp, #-16]
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 1016 0
	b	.L74
.L77:
	.loc 1 1025 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-20]
	ldr	r2, [fp, #-8]
	bl	nm_this_station_weighs_MORE_than_the_list_station
	mov	r3, r0
	cmp	r3, #0
	bne	.L78
.L75:
	.loc 1 1030 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L74:
	.loc 1 1016 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L77
	b	.L76
.L78:
	.loc 1 1027 0
	mov	r0, r0	@ nop
.L76:
	.loc 1 1035 0
	ldr	r3, [fp, #-8]
	.loc 1 1036 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE9:
	.size	nm_return_station_this_station_belongs_before_in_the_irrigation_list, .-nm_return_station_this_station_belongs_before_in_the_irrigation_list
	.section .rodata
	.align	2
.LC9:
	.ascii	"Couldn't remove from irri list to reinsert\000"
	.section	.text.FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	.type	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list, %function
FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list:
.LFB10:
	.loc 1 1058 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI29:
	add	fp, sp, #4
.LCFI30:
	sub	sp, sp, #12
.LCFI31:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 1064 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L80
	.loc 1 1066 0
	ldr	r0, .L84
	ldr	r1, [fp, #-16]
	ldr	r2, .L84+4
	ldr	r3, .L84+8
	bl	nm_ListRemove_debug
	.loc 1 1069 0
	mov	r0, #1
	ldr	r1, .L84
	ldr	r2, [fp, #-16]
	bl	nm_return_station_this_station_belongs_before_in_the_irrigation_list
	str	r0, [fp, #-8]
	.loc 1 1073 0
	ldr	r0, .L84
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-8]
	bl	nm_ListInsert
	b	.L79
.L80:
	.loc 1 1078 0
	ldr	r3, .L84+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L84+4
	ldr	r3, .L84+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1083 0
	ldr	r0, .L84+20
	ldr	r1, [fp, #-16]
	ldr	r2, .L84+4
	ldr	r3, .L84+24
	bl	nm_ListRemove_debug
	mov	r3, r0
	cmp	r3, #0
	beq	.L82
	.loc 1 1085 0
	ldr	r0, .L84+28
	bl	Alert_Message
	b	.L83
.L82:
	.loc 1 1090 0
	mov	r0, #0
	ldr	r1, .L84+20
	ldr	r2, [fp, #-16]
	bl	nm_return_station_this_station_belongs_before_in_the_irrigation_list
	str	r0, [fp, #-8]
	.loc 1 1094 0
	ldr	r0, .L84+20
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-8]
	bl	nm_ListInsert
.L83:
	.loc 1 1098 0
	ldr	r3, .L84+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L79:
	.loc 1 1100 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L85:
	.align	2
.L84:
	.word	ft_irrigating_stations_list_hdr
	.word	.LC0
	.word	1066
	.word	list_foal_irri_recursive_MUTEX
	.word	1078
	.word	foal_irri+16
	.word	1083
	.word	.LC9
.LFE10:
	.size	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list, .-FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	.global	last_time_a_valve_was_turned_off
	.section	.bss.last_time_a_valve_was_turned_off,"aw",%nobits
	.align	2
	.type	last_time_a_valve_was_turned_off, %object
	.size	last_time_a_valve_was_turned_off, 6
last_time_a_valve_was_turned_off:
	.space	6
	.section .rodata
	.align	2
.LC10:
	.ascii	"pilc_ptr\000"
	.align	2
.LC11:
	.ascii	"foal_irri.list_of_foal_all_irrigation\000"
	.align	2
.LC12:
	.ascii	"Box Index\000"
	.align	2
.LC13:
	.ascii	"FOAL_IRRI: expected count is 0. : %s, %u\000"
	.align	2
.LC14:
	.ascii	"FOAL: error removing from ON list : %s, %u\000"
	.align	2
.LC15:
	.ascii	"FOAL: should've been on the ON list. : %s, %u\000"
	.align	2
.LC16:
	.ascii	"FOAL_IRRI: number_of_valves_ON unexpectedly zero.\000"
	.align	2
.LC17:
	.ascii	"FOAL_IRRI: system expected flow rate out of range.\000"
	.align	2
.LC18:
	.ascii	"FOAL_IRRI: number_ON_for_test unexpectedly zero.\000"
	.align	2
.LC19:
	.ascii	"FOAL_IRRI: flow group count unexpectedly zero.\000"
	.align	2
.LC20:
	.ascii	"Slow closing delay ignored: pump running with no va"
	.ascii	"lves ON.\000"
	.align	2
.LC21:
	.ascii	"FOAL: unexpectedly on ON list. : %s, %u\000"
	.section	.text.nm_nm_FOAL_if_station_is_ON_turn_it_OFF,"ax",%progbits
	.align	2
	.global	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	.type	nm_nm_FOAL_if_station_is_ON_turn_it_OFF, %function
nm_nm_FOAL_if_station_is_ON_turn_it_OFF:
.LFB11:
	.loc 1 1163 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI32:
	add	fp, sp, #4
.LCFI33:
	sub	sp, sp, #40
.LCFI34:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	.loc 1 1170 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1177 0
	ldr	r3, .L123
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L123+4
	ldr	r3, .L123+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1179 0
	ldr	r3, .L123+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L123+4
	ldr	r3, .L123+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1184 0
	ldr	r0, .L123+20
	ldr	r1, [fp, #-24]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	bne	.L87
	.loc 1 1187 0
	ldr	r0, .L123+24
	ldr	r1, .L123+28
	ldr	r2, .L123+4
	ldr	r3, .L123+32
	bl	Alert_item_not_on_list_with_filename
	b	.L88
.L87:
	.loc 1 1198 0
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-24]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
	.loc 1 1204 0
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #74]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L89
	.loc 1 1206 0
	ldr	r3, .L123+36
	ldr	r3, [r3, #76]
	cmp	r3, #0
	bne	.L90
	.loc 1 1215 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldrb	r3, [r3, #465]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L91
.LBB6:
	.loc 1 1220 0
	mov	r3, #0
	strh	r3, [fp, #-16]	@ movhi
	.loc 1 1226 0
	ldr	r3, [fp, #-28]
	cmp	r3, #103
	bne	.L92
	.loc 1 1228 0
	ldrb	r3, [fp, #-15]
	orr	r3, r3, #16
	strb	r3, [fp, #-15]
.L92:
	.loc 1 1235 0
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L93
	.loc 1 1235 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	and	r3, r3, #2
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L93
	.loc 1 1239 0 is_stmt 1
	ldrb	r3, [fp, #-16]
	bic	r3, r3, #3
	orr	r3, r3, #4
	strb	r3, [fp, #-16]
	.loc 1 1246 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L94
	.loc 1 1248 0
	ldrb	r3, [fp, #-15]
	orr	r3, r3, #4
	strb	r3, [fp, #-15]
	.loc 1 1255 0
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	ldr	r0, .L123+40
	mov	r1, r2
	mov	r2, r3
	bl	Alert_flow_not_checked_with_reason_idx
	.loc 1 1246 0
	b	.L96
.L94:
	.loc 1 1259 0
	ldrb	r3, [fp, #-15]
	orr	r3, r3, #8
	strb	r3, [fp, #-15]
	.loc 1 1266 0
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r0, #256
	mov	r1, r2
	mov	r2, r3
	bl	Alert_flow_not_checked_with_reason_idx
	.loc 1 1246 0
	b	.L96
.L93:
	.loc 1 1276 0
	ldrb	r3, [fp, #-16]
	bic	r3, r3, #3
	orr	r3, r3, #4
	strb	r3, [fp, #-16]
	.loc 1 1278 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L97
	.loc 1 1280 0
	ldrb	r3, [fp, #-16]
	orr	r3, r3, #128
	strb	r3, [fp, #-16]
	b	.L96
.L97:
	.loc 1 1283 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	mov	r3, #468
	ldrh	r3, [r2, r3]
	and	r3, r3, #8064
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #0
	bne	.L98
	.loc 1 1285 0
	ldrb	r3, [fp, #-16]
	orr	r3, r3, #64
	strb	r3, [fp, #-16]
	b	.L96
.L98:
	.loc 1 1291 0
	ldrb	r3, [fp, #-15]
	orr	r3, r3, #32
	strb	r3, [fp, #-15]
.L96:
	.loc 1 1296 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	.loc 1 1301 0
	ldr	r2, [fp, #-24]
	ldr	r2, [r2, #40]
	flds	s15, [r2, #296]
	.loc 1 1296 0
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	ldrh	r2, [fp, #-16]	@ movhi
	strh	r2, [sp, #0]	@ movhi
	str	r1, [sp, #4]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #1
	bl	FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines
.L91:
.LBE6:
	.loc 1 1313 0
	ldr	r3, .L123+36
	mov	r2, #1
	str	r2, [r3, #76]
.L90:
	.loc 1 1323 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #64
	strb	r3, [r2, #465]
	.loc 1 1325 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #464]
	bic	r3, r3, #128
	strb	r3, [r2, #464]
	.loc 1 1326 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #1
	strb	r3, [r2, #465]
	.loc 1 1327 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #2
	strb	r3, [r2, #465]
	.loc 1 1328 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #4
	strb	r3, [r2, #465]
	.loc 1 1329 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #8
	strb	r3, [r2, #465]
	.loc 1 1330 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #16
	strb	r3, [r2, #465]
	.loc 1 1331 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #32
	strb	r3, [r2, #465]
	.loc 1 1338 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #91
	mov	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L123+44
	str	r2, [sp, #4]
	ldr	r2, .L123+4
	str	r2, [sp, #8]
	ldr	r2, .L123+48
	str	r2, [sp, #12]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #11
	mov	r3, #0
	bl	RC_uns8_with_filename
	mov	r3, r0
	cmp	r3, #0
	beq	.L99
	.loc 1 1340 0
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L123+36
	add	r2, r2, #20
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	bne	.L100
	.loc 1 1342 0
	ldr	r0, .L123+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L123+52
	mov	r1, r3
	ldr	r2, .L123+56
	bl	Alert_Message_va
	b	.L99
.L100:
	.loc 1 1346 0
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r1, r3
	ldr	r3, .L123+36
	add	r1, r1, #20
	ldr	r3, [r3, r1, asl #2]
	sub	r1, r3, #1
	ldr	r3, .L123+36
	add	r2, r2, #20
	str	r1, [r3, r2, asl #2]
.L99:
	.loc 1 1355 0
	ldr	r0, .L123+60
	ldr	r1, [fp, #-24]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L101
	.loc 1 1357 0
	ldr	r0, .L123+60
	ldr	r1, [fp, #-24]
	ldr	r2, .L123+4
	ldr	r3, .L123+64
	bl	nm_ListRemove_debug
	mov	r3, r0
	cmp	r3, #0
	beq	.L102
	.loc 1 1359 0
	ldr	r0, .L123+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L123+68
	mov	r1, r3
	ldr	r2, .L123+72
	bl	Alert_Message_va
	b	.L102
.L101:
	.loc 1 1365 0
	ldr	r0, .L123+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L123+76
	mov	r1, r3
	ldr	r2, .L123+80
	bl	Alert_Message_va
.L102:
	.loc 1 1372 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	bl	nm_FOAL_add_to_action_needed_list
	.loc 1 1378 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #92]
	cmp	r3, #0
	beq	.L103
	.loc 1 1380 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-24]
	ldr	r2, [r2, #40]
	ldr	r2, [r2, #92]
	sub	r2, r2, #1
	str	r2, [r3, #92]
	b	.L104
.L103:
	.loc 1 1385 0
	ldr	r0, .L123+84
	bl	Alert_Message
.L104:
	.loc 1 1390 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [r3, #100]
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #80]
	cmp	r2, r3
	bcc	.L105
	.loc 1 1392 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-24]
	ldr	r2, [r2, #40]
	ldr	r1, [r2, #100]
	ldr	r2, [fp, #-24]
	ldrh	r2, [r2, #80]
	rsb	r2, r2, r1
	str	r2, [r3, #100]
	b	.L106
.L105:
	.loc 1 1396 0
	ldr	r0, .L123+88
	bl	Alert_Message
	.loc 1 1399 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	mov	r2, #0
	str	r2, [r3, #100]
.L106:
	.loc 1 1404 0
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	bne	.L107
	.loc 1 1406 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #168]
	cmp	r3, #0
	beq	.L108
	.loc 1 1408 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-24]
	ldr	r2, [r2, #40]
	ldr	r2, [r2, #168]
	sub	r2, r2, #1
	str	r2, [r3, #168]
	b	.L107
.L108:
	.loc 1 1413 0
	ldr	r0, .L123+92
	bl	Alert_Message
.L107:
	.loc 1 1436 0
	ldr	r2, [fp, #-24]
	ldrb	r3, [r2, #73]
	bic	r3, r3, #4
	strb	r3, [r2, #73]
	.loc 1 1440 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-24]
	ldrb	r2, [r2, #74]	@ zero_extendqisi2
	mov	r2, r2, lsr #2
	and	r2, r2, #3
	and	r2, r2, #255
	add	r2, r2, #48
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L109
	.loc 1 1442 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-24]
	ldrb	r2, [r2, #74]	@ zero_extendqisi2
	mov	r2, r2, lsr #2
	and	r2, r2, #3
	and	r2, r2, #255
	ldr	r1, [fp, #-24]
	ldr	r1, [r1, #40]
	ldr	r0, [fp, #-24]
	ldrb	r0, [r0, #74]	@ zero_extendqisi2
	mov	r0, r0, lsr #2
	and	r0, r0, #3
	and	r0, r0, #255
	add	r0, r0, #48
	ldr	r1, [r1, r0, asl #2]
	sub	r1, r1, #1
	add	r2, r2, #48
	str	r1, [r3, r2, asl #2]
	b	.L110
.L109:
	.loc 1 1447 0
	ldr	r0, .L123+96
	bl	Alert_Message
.L110:
	.loc 1 1463 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [r3, #208]
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #82]
	cmp	r2, r3
	bcs	.L111
	.loc 1 1465 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-24]
	ldrh	r2, [r2, #82]
	str	r2, [r3, #208]
.L111:
	.loc 1 1475 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 1481 0
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	beq	.L112
	.loc 1 1481 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	bne	.L113
.L112:
	.loc 1 1483 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-12]
.L113:
	.loc 1 1493 0
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L114
	.loc 1 1497 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #92]
	cmp	r3, #0
	bne	.L114
	.loc 1 1499 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 1508 0
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #84]
	cmp	r3, #0
	beq	.L114
	.loc 1 1510 0
	ldr	r0, .L123+100
	bl	Alert_Message
.L114:
	.loc 1 1517 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L115
	.loc 1 1519 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [r3, #212]
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #84]
	cmp	r2, r3
	bcs	.L116
	.loc 1 1521 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-24]
	ldrh	r2, [r2, #84]
	str	r2, [r3, #212]
	b	.L116
.L115:
	.loc 1 1541 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	mov	r2, #0
	str	r2, [r3, #212]
.L116:
	.loc 1 1553 0
	ldr	r3, [fp, #-28]
	cmp	r3, #18
	beq	.L117
	.loc 1 1555 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #48]
	cmp	r3, #0
	ble	.L117
	.loc 1 1557 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #48]
	add	r2, r2, r3
	ldr	r3, [fp, #-24]
	str	r2, [r3, #52]
.L117:
	.loc 1 1563 0
	ldr	r2, [fp, #-24]
	ldrb	r3, [r2, #74]
	bic	r3, r3, #64
	strb	r3, [r2, #74]
	.loc 1 1569 0
	ldr	r3, [fp, #-24]
	mov	r2, #0
	str	r2, [r3, #48]
	.loc 1 1575 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-24]
	ldrb	r2, [r2, #90]	@ zero_extendqisi2
	str	r2, [r3, #436]
	.loc 1 1577 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-24]
	ldrh	r2, [r2, #72]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	and	r2, r2, #255
	str	r2, [r3, #440]
	.loc 1 1583 0
	ldr	r3, [fp, #-28]
	cmp	r3, #13
	bne	.L118
	.loc 1 1585 0
	ldr	r2, [fp, #-24]
	ldrb	r3, [r2, #75]
	orr	r3, r3, #4
	strb	r3, [r2, #75]
.L118:
	.loc 1 1590 0
	ldr	r3, [fp, #-28]
	cmp	r3, #5
	beq	.L119
	.loc 1 1590 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #103
	bne	.L120
.L119:
	.loc 1 1599 0 is_stmt 1
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #64]
	ldr	r3, [fp, #-24]
	str	r2, [r3, #60]
	b	.L121
.L120:
	.loc 1 1606 0
	ldr	r3, [fp, #-24]
	mov	r2, #0
	str	r2, [r3, #60]
.L121:
	.loc 1 1616 0
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L122
	.loc 1 1620 0
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	bl	Alert_mobile_station_off
	.loc 1 1626 0
	ldr	r0, .L123+104
	mov	r1, #0
	mov	r2, #512
	mov	r3, #0
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.L122:
	.loc 1 1636 0
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #320
	beq	.L88
	.loc 1 1638 0
	mov	r0, #0
	ldr	r1, [fp, #-24]
	bl	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	b	.L88
.L89:
	.loc 1 1645 0
	ldr	r0, .L123+60
	ldr	r1, [fp, #-24]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	beq	.L88
	.loc 1 1647 0
	ldr	r0, .L123+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L123+108
	mov	r1, r3
	ldr	r2, .L123+112
	bl	Alert_Message_va
	.loc 1 1649 0
	ldr	r0, .L123+60
	ldr	r1, [fp, #-24]
	ldr	r2, .L123+4
	ldr	r3, .L123+116
	bl	nm_ListRemove_debug
	mov	r3, r0
	cmp	r3, #0
	beq	.L88
	.loc 1 1651 0
	ldr	r0, .L123+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L123+120
	mov	r1, r3
	ldr	r2, .L123+124
	bl	Alert_Message_va
.L88:
	.loc 1 1660 0
	ldr	r3, .L123+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1662 0
	ldr	r3, .L123
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1666 0
	ldr	r3, [fp, #-8]
	.loc 1 1667 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L124:
	.align	2
.L123:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	1177
	.word	system_preserves_recursive_MUTEX
	.word	1179
	.word	foal_irri+16
	.word	.LC10
	.word	.LC11
	.word	1187
	.word	foal_irri
	.word	257
	.word	.LC12
	.word	1338
	.word	.LC13
	.word	1342
	.word	foal_irri+36
	.word	1357
	.word	.LC14
	.word	1359
	.word	.LC15
	.word	1365
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	.LC20
	.word	413
	.word	.LC21
	.word	1647
	.word	1649
	.word	.LC1
	.word	1651
.LFE11:
	.size	nm_nm_FOAL_if_station_is_ON_turn_it_OFF, .-nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	.section	.text.nm_shut_down_mvs_and_pmps_throughout_this_system,"ax",%progbits
	.align	2
	.global	nm_shut_down_mvs_and_pmps_throughout_this_system
	.type	nm_shut_down_mvs_and_pmps_throughout_this_system, %function
nm_shut_down_mvs_and_pmps_throughout_this_system:
.LFB12:
	.loc 1 1692 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI35:
	add	fp, sp, #0
.LCFI36:
	sub	sp, sp, #8
.LCFI37:
	str	r0, [fp, #-8]
	.loc 1 1700 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L126
.L128:
	.loc 1 1702 0
	ldr	r0, .L129
	ldr	r2, [fp, #-4]
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L127
	.loc 1 1707 0
	ldr	r0, .L129
	ldr	r2, [fp, #-4]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r2, r3, r1
	ldrb	r3, [r2, #0]
	bic	r3, r3, #32
	strb	r3, [r2, #0]
	.loc 1 1709 0
	ldr	r0, .L129
	ldr	r2, [fp, #-4]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r2, r3, r1
	ldrb	r3, [r2, #0]
	bic	r3, r3, #64
	strb	r3, [r2, #0]
	.loc 1 1713 0
	ldr	r0, .L129
	ldr	r2, [fp, #-4]
	mov	r1, #464
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1714 0
	ldr	r0, .L129
	ldr	r2, [fp, #-4]
	mov	r1, #468
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1728 0
	ldr	r0, .L129
	ldr	r2, [fp, #-4]
	mov	r1, #228
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
.L127:
	.loc 1 1700 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L126:
	.loc 1 1700 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #3
	bls	.L128
	.loc 1 1731 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L130:
	.align	2
.L129:
	.word	system_preserves
.LFE12:
	.size	nm_shut_down_mvs_and_pmps_throughout_this_system, .-nm_shut_down_mvs_and_pmps_throughout_this_system
	.section	.text.set_station_history_flags_if_in_the_list_for_programmed_irrigation,"ax",%progbits
	.align	2
	.type	set_station_history_flags_if_in_the_list_for_programmed_irrigation, %function
set_station_history_flags_if_in_the_list_for_programmed_irrigation:
.LFB13:
	.loc 1 1735 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI38:
	add	fp, sp, #4
.LCFI39:
	sub	sp, sp, #8
.LCFI40:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1742 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L131
	.loc 1 1745 0
	ldr	r3, .L152
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L152+4
	ldr	r3, .L152+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1749 0
	ldr	r3, [fp, #-12]
	cmp	r3, #104
	beq	.L133
	.loc 1 1749 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #108
	bne	.L134
.L133:
	.loc 1 1751 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #16
	strb	r3, [r2, #0]
	b	.L135
.L134:
	.loc 1 1754 0
	ldr	r3, [fp, #-12]
	cmp	r3, #107
	bne	.L136
	.loc 1 1756 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #8
	strb	r3, [r2, #3]
	b	.L135
.L136:
	.loc 1 1759 0
	ldr	r3, [fp, #-12]
	cmp	r3, #109
	bne	.L137
	.loc 1 1761 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #32
	strb	r3, [r2, #3]
	b	.L135
.L137:
	.loc 1 1764 0
	ldr	r3, [fp, #-12]
	cmp	r3, #114
	bne	.L138
	.loc 1 1767 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #8
	strb	r3, [r2, #3]
	b	.L135
.L138:
	.loc 1 1770 0
	ldr	r3, [fp, #-12]
	cmp	r3, #113
	bne	.L139
	.loc 1 1774 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #2
	strb	r3, [r2, #0]
	b	.L135
.L139:
	.loc 1 1777 0
	ldr	r3, [fp, #-12]
	cmp	r3, #101
	bne	.L140
	.loc 1 1779 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	orr	r3, r3, #128
	strb	r3, [r2, #1]
	b	.L135
.L140:
	.loc 1 1782 0
	ldr	r3, [fp, #-12]
	cmp	r3, #105
	bne	.L141
	.loc 1 1784 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #2
	strb	r3, [r2, #3]
	b	.L135
.L141:
	.loc 1 1787 0
	ldr	r3, [fp, #-12]
	cmp	r3, #106
	bne	.L142
	.loc 1 1789 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #2
	strb	r3, [r2, #3]
	b	.L135
.L142:
	.loc 1 1792 0
	ldr	r3, [fp, #-12]
	cmp	r3, #111
	bne	.L143
	.loc 1 1794 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #2
	strb	r3, [r2, #3]
	b	.L135
.L143:
	.loc 1 1797 0
	ldr	r3, [fp, #-12]
	cmp	r3, #110
	bne	.L144
	.loc 1 1799 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #8
	strb	r3, [r2, #3]
	b	.L135
.L144:
	.loc 1 1802 0
	ldr	r3, [fp, #-12]
	cmp	r3, #112
	bne	.L145
	.loc 1 1804 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #64
	strb	r3, [r2, #3]
	b	.L135
.L145:
	.loc 1 1807 0
	ldr	r3, [fp, #-12]
	cmp	r3, #100
	bne	.L146
	.loc 1 1809 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #8
	strb	r3, [r2, #0]
	b	.L135
.L146:
	.loc 1 1812 0
	ldr	r3, [fp, #-12]
	cmp	r3, #28
	bne	.L147
	.loc 1 1814 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #4
	strb	r3, [r2, #0]
	b	.L135
.L147:
	.loc 1 1817 0
	ldr	r3, [fp, #-12]
	cmp	r3, #30
	bne	.L148
	.loc 1 1819 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #16
	strb	r3, [r2, #2]
	b	.L135
.L148:
	.loc 1 1822 0
	ldr	r3, [fp, #-12]
	cmp	r3, #31
	bne	.L149
	.loc 1 1824 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #32
	strb	r3, [r2, #2]
	b	.L135
.L149:
	.loc 1 1827 0
	ldr	r3, [fp, #-12]
	cmp	r3, #29
	bne	.L150
	.loc 1 1829 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #8
	strb	r3, [r2, #2]
	b	.L135
.L150:
	.loc 1 1832 0
	ldr	r3, [fp, #-12]
	cmp	r3, #33
	bne	.L135
	.loc 1 1834 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #128
	strb	r3, [r2, #2]
.L135:
	.loc 1 1843 0
	ldr	r3, [fp, #-12]
	cmp	r3, #20
	beq	.L151
	.loc 1 1843 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #28
	beq	.L151
	.loc 1 1845 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L152+12
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	bic	r3, r3, #16
	strb	r3, [r2, #1]
.L151:
	.loc 1 1850 0
	ldr	r3, .L152
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L131:
	.loc 1 1852 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L153:
	.align	2
.L152:
	.word	station_preserves_recursive_MUTEX
	.word	.LC0
	.word	1745
	.word	station_preserves
.LFE13:
	.size	set_station_history_flags_if_in_the_list_for_programmed_irrigation, .-set_station_history_flags_if_in_the_list_for_programmed_irrigation
	.section	.text.nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists,"ax",%progbits
	.align	2
	.global	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	.type	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists, %function
nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists:
.LFB14:
	.loc 1 1881 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI41:
	add	fp, sp, #8
.LCFI42:
	sub	sp, sp, #36
.LCFI43:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	.loc 1 1889 0
	ldr	r3, .L160	@ float
	str	r3, [fp, #-20]	@ float
	.loc 1 1902 0
	ldr	r3, .L160+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L160+8
	ldr	r3, .L160+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1904 0
	ldr	r3, .L160+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L160+8
	mov	r3, #1904
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1910 0
	ldr	r0, [fp, #-28]
	ldr	r1, [fp, #-32]
	bl	set_station_history_flags_if_in_the_list_for_programmed_irrigation
	.loc 1 1915 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	ldr	r2, [fp, #-32]
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	str	r0, [fp, #-12]
	.loc 1 1922 0
	ldr	r0, [fp, #-28]
	ldr	r1, [fp, #-32]
	bl	nm_FOAL_add_to_action_needed_list
	.loc 1 1930 0
	ldr	r3, [fp, #-28]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L155
.LBB7:
	.loc 1 1934 0
	ldr	r3, .L160+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L160+8
	ldr	r3, .L160+24
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1945 0
	ldr	r3, .L160+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L160+8
	ldr	r3, .L160+32
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1947 0
	ldr	r3, [fp, #-28]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-28]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	bl	nm_STATION_get_pointer_to_station
	str	r0, [fp, #-16]
	.loc 1 1949 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L156
	.loc 1 1951 0
	ldr	r0, [fp, #-16]
	bl	WEATHER_get_station_uses_daily_et
	mov	r3, r0
	cmp	r3, #0
	beq	.L157
	.loc 1 1956 0
	ldr	r0, [fp, #-16]
	bl	nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag
	mov	r3, r0
	cmp	r3, #0
	beq	.L158
	.loc 1 1960 0
	bl	FLOWSENSE_get_controller_index
	mov	r4, r0
	ldr	r0, [fp, #-16]
	mov	r1, #11
	bl	STATION_get_change_bits_ptr
	mov	r3, r0
	str	r4, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	ldr	r1, .L160+36	@ float
	mov	r2, #0
	mov	r3, #11
	bl	nm_STATION_set_moisture_balance_percent
	.loc 1 1964 0
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #44]
	ldr	r1, .L160+40
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
	.loc 1 1967 0
	bl	FLOWSENSE_get_controller_index
	mov	r4, r0
	ldr	r0, [fp, #-16]
	mov	r1, #11
	bl	STATION_get_change_bits_ptr
	mov	r3, r0
	str	r4, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	mov	r1, #0
	mov	r2, #0
	mov	r3, #11
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
	b	.L157
.L158:
	.loc 1 2027 0
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #44]
	ldr	r1, .L160+40
	mov	r3, #52
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L157
	.loc 1 2029 0
	bl	FLOWSENSE_get_controller_index
	mov	r4, r0
	ldr	r0, [fp, #-16]
	mov	r1, #11
	bl	STATION_get_change_bits_ptr
	mov	r3, r0
	str	r4, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	ldr	r1, .L160+36	@ float
	mov	r2, #0
	mov	r3, #11
	bl	nm_STATION_set_moisture_balance_percent
.L157:
	.loc 1 2039 0
	ldr	r3, [fp, #-28]
	ldr	r4, [r3, #44]
	ldr	r0, [fp, #-16]
	bl	STATION_get_moisture_balance_percent
	fmsr	s14, r0
	flds	s15, [fp, #-20]
	fmuls	s14, s14, s15
	flds	s15, [fp, #-20]
	fmuls	s15, s14, s15
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L160+40
	mov	r3, #72
	mov	r1, r4, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 2044 0
	ldr	r3, [fp, #-28]
	ldr	r1, [r3, #44]
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #44]
	ldr	r0, .L160+40
	mov	r3, #48
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrh	r3, [r3, #2]
	mov	r2, r3
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #1
	mov	r2, r3
	ldr	r3, [fp, #-28]
	ldr	r0, [r3, #44]
	ldr	ip, .L160+40
	mov	r3, #136
	mov	r0, r0, asl #7
	add	r0, ip, r0
	add	r3, r0, r3
	ldrh	r3, [r3, #0]
	add	r3, r2, r3
	ldr	r2, .L160+44
	smull	r0, r2, r3, r2
	mov	r3, r3, asr #31
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L160+40
	mov	r3, #48
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #2]	@ movhi
	b	.L159
.L156:
	.loc 1 2048 0
	ldr	r0, .L160+8
	mov	r1, #2048
	bl	Alert_station_not_found_with_filename
	.loc 1 2052 0
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #44]
	ldr	r1, .L160+40
	mov	r3, #72
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
.L159:
	.loc 1 2055 0
	ldr	r3, .L160+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2059 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #44]
	mov	r0, r3
	bl	nm_STATION_HISTORY_close_and_start_a_new_record
	.loc 1 2061 0
	ldr	r3, .L160+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L155:
.LBE7:
	.loc 1 2069 0
	ldr	r0, .L160+48
	ldr	r1, [fp, #-28]
	ldr	r2, .L160+8
	ldr	r3, .L160+52
	bl	nm_ListRemove_debug
	.loc 1 2073 0
	ldr	r3, .L160+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2075 0
	ldr	r3, .L160+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2079 0
	ldr	r3, [fp, #-12]
	.loc 1 2080 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L161:
	.align	2
.L160:
	.word	1120403456
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	1902
	.word	system_preserves_recursive_MUTEX
	.word	station_preserves_recursive_MUTEX
	.word	1934
	.word	list_program_data_recursive_MUTEX
	.word	1945
	.word	1056964608
	.word	station_preserves
	.word	715827883
	.word	foal_irri+16
	.word	2069
.LFE14:
	.size	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists, .-nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	.section .rodata
	.align	2
.LC22:
	.ascii	"FOAL_IRRI: couldn't find failed station?\000"
	.section	.text.FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists, %function
FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists:
.LFB15:
	.loc 1 2102 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI44:
	add	fp, sp, #4
.LCFI45:
	sub	sp, sp, #24
.LCFI46:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	.loc 1 2116 0
	ldr	r3, .L172
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L172+4
	ldr	r3, .L172+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2118 0
	ldr	r3, .L172+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L172+4
	ldr	r3, .L172+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2125 0
	ldr	r3, .L172+20
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 2131 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 2138 0
	ldr	r0, .L172+24
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 2140 0
	b	.L163
.L165:
	.loc 1 2144 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L164
	.loc 1 2144 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bne	.L164
	.loc 1 2151 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	str	r3, [fp, #-8]
	.loc 1 2155 0
	ldr	r0, .L172+24
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-28]
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-12]
	b	.L163
.L164:
	.loc 1 2163 0
	ldr	r0, .L172+24
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L163:
	.loc 1 2140 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L165
	.loc 1 2169 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L166
	.loc 1 2173 0
	ldr	r3, [fp, #-28]
	cmp	r3, #109
	beq	.L167
	.loc 1 2173 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #112
	beq	.L167
	.loc 1 2175 0 is_stmt 1
	ldr	r0, .L172+28
	bl	Alert_Message
	b	.L167
.L166:
	.loc 1 2184 0
	ldr	r3, [fp, #-28]
	cmp	r3, #104
	bne	.L167
.LBB8:
	.loc 1 2198 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 2200 0
	ldr	r0, .L172+24
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 2202 0
	b	.L168
.L171:
	.loc 1 2204 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #40]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L169
	.loc 1 2206 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 2208 0
	b	.L170
.L169:
	.loc 1 2211 0
	ldr	r0, .L172+24
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L168:
	.loc 1 2202 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L171
.L170:
	.loc 1 2217 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L167
	.loc 1 2219 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	nm_shut_down_mvs_and_pmps_throughout_this_system
.L167:
.LBE8:
	.loc 1 2226 0
	ldr	r3, .L172+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2228 0
	ldr	r3, .L172
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2229 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L173:
	.align	2
.L172:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	2116
	.word	system_preserves_recursive_MUTEX
	.word	2118
	.word	foal_irri
	.word	foal_irri+16
	.word	.LC22
.LFE15:
	.size	FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists, .-FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists
	.section	.text.FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists, %function
FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists:
.LFB16:
	.loc 1 2233 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI47:
	add	fp, sp, #4
.LCFI48:
	sub	sp, sp, #16
.LCFI49:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 1 2240 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 2247 0
	ldr	r3, .L179
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L179+4
	ldr	r3, .L179+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2249 0
	ldr	r3, .L179+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L179+4
	ldr	r3, .L179+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2256 0
	ldr	r3, .L179+20
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 2263 0
	ldr	r0, .L179+24
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 2265 0
	b	.L175
.L178:
	.loc 1 2268 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L176
	.loc 1 2270 0
	ldr	r3, [fp, #-20]
	cmp	r3, #104
	bne	.L177
	.loc 1 2273 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	ldr	r0, [fp, #-16]
	mov	r1, r3
	bl	Alert_conventional_station_short_idx
.L177:
	.loc 1 2279 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 2285 0
	ldr	r0, .L179+24
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-20]
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-8]
	b	.L175
.L176:
	.loc 1 2289 0
	ldr	r0, .L179+24
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L175:
	.loc 1 2265 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L178
	.loc 1 2318 0
	ldr	r3, .L179+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2320 0
	ldr	r3, .L179
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2321 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L180:
	.align	2
.L179:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	2247
	.word	system_preserves_recursive_MUTEX
	.word	2249
	.word	foal_irri
	.word	foal_irri+36
.LFE16:
	.size	FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists, .-FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists
	.section .rodata
	.align	2
.LC23:
	.ascii	"FOAL_IRRI: couldn't find one in the list\000"
	.section	.text.FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists, %function
FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists:
.LFB17:
	.loc 1 2325 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI50:
	add	fp, sp, #4
.LCFI51:
	sub	sp, sp, #24
.LCFI52:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 2334 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 2341 0
	ldr	r3, .L186
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L186+4
	ldr	r3, .L186+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2343 0
	ldr	r3, .L186+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L186+4
	ldr	r3, .L186+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2350 0
	ldr	r3, .L186+20
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 2359 0
	ldr	r0, .L186+24
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 2361 0
	b	.L182
.L184:
	.loc 1 2363 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L183
	.loc 1 2363 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L183
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bne	.L183
	.loc 1 2366 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 2372 0
	ldr	r0, .L186+24
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-28]
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-12]
	b	.L182
.L183:
	.loc 1 2377 0
	ldr	r0, .L186+24
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L182:
	.loc 1 2361 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L184
	.loc 1 2383 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L185
	.loc 1 2385 0
	ldr	r0, .L186+28
	bl	Alert_Message
.L185:
	.loc 1 2390 0
	ldr	r3, .L186+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2392 0
	ldr	r3, .L186
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2393 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L187:
	.align	2
.L186:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	2341
	.word	system_preserves_recursive_MUTEX
	.word	2343
	.word	foal_irri
	.word	foal_irri+16
	.word	.LC23
.LFE17:
	.size	FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists, .-FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists
	.section	.text.FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists, %function
FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists:
.LFB18:
	.loc 1 2397 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI53:
	add	fp, sp, #4
.LCFI54:
	sub	sp, sp, #12
.LCFI55:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 2405 0
	ldr	r3, .L192
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L192+4
	ldr	r3, .L192+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2407 0
	ldr	r3, .L192+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L192+4
	ldr	r3, .L192+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2414 0
	ldr	r3, .L192+20
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 2420 0
	ldr	r0, .L192+24
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 2422 0
	b	.L189
.L191:
	.loc 1 2425 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bne	.L190
	.loc 1 2429 0
	ldr	r0, .L192+24
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-16]
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-8]
	b	.L189
.L190:
	.loc 1 2433 0
	ldr	r0, .L192+24
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L189:
	.loc 1 2422 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L191
	.loc 1 2439 0
	ldr	r3, .L192+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2441 0
	ldr	r3, .L192
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2442 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L193:
	.align	2
.L192:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	2405
	.word	system_preserves_recursive_MUTEX
	.word	2407
	.word	foal_irri
	.word	foal_irri+16
.LFE18:
	.size	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists, .-FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists
	.section	.text.FOAL_IRRI_remove_all_stations_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_all_stations_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_all_stations_from_the_irrigation_lists, %function
FOAL_IRRI_remove_all_stations_from_the_irrigation_lists:
.LFB19:
	.loc 1 2446 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI56:
	add	fp, sp, #4
.LCFI57:
	sub	sp, sp, #8
.LCFI58:
	str	r0, [fp, #-12]
	.loc 1 2449 0
	ldr	r3, .L198
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L198+4
	ldr	r3, .L198+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2451 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L195
.L197:
	.loc 1 2453 0
	ldr	r1, .L198+12
	ldr	r2, [fp, #-8]
	mov	r3, #16
	mov	r0, #92
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L196
	.loc 1 2457 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists
.L196:
	.loc 1 2451 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L195:
	.loc 1 2451 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L197
	.loc 1 2461 0 is_stmt 1
	ldr	r3, .L198
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2462 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L199:
	.align	2
.L198:
	.word	chain_members_recursive_MUTEX
	.word	.LC0
	.word	2449
	.word	chain
.LFE19:
	.size	FOAL_IRRI_remove_all_stations_from_the_irrigation_lists, .-FOAL_IRRI_remove_all_stations_from_the_irrigation_lists
	.section	.text.FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists, %function
FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists:
.LFB20:
	.loc 1 2484 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI59:
	add	fp, sp, #4
.LCFI60:
	sub	sp, sp, #28
.LCFI61:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	str	r3, [fp, #-32]
	.loc 1 2494 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 2499 0
	ldr	r3, .L210
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L210+4
	ldr	r3, .L210+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2501 0
	ldr	r3, .L210+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L210+4
	ldr	r3, .L210+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2509 0
	ldr	r3, .L210+20
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 2515 0
	ldr	r0, .L210+24
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 2517 0
	b	.L201
.L204:
	.loc 1 2519 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #40]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L202
	.loc 1 2519 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L203
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bne	.L202
.L203:
	.loc 1 2521 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 2523 0
	ldr	r0, .L210+24
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-32]
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-12]
	b	.L201
.L202:
	.loc 1 2527 0
	ldr	r0, .L210+24
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L201:
	.loc 1 2517 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L204
	.loc 1 2539 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 2541 0
	ldr	r0, .L210+24
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 2543 0
	b	.L205
.L208:
	.loc 1 2545 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #40]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L206
	.loc 1 2547 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 2549 0
	b	.L207
.L206:
	.loc 1 2552 0
	ldr	r0, .L210+24
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L205:
	.loc 1 2543 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L208
.L207:
	.loc 1 2559 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L209
	.loc 1 2561 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	nm_shut_down_mvs_and_pmps_throughout_this_system
.L209:
	.loc 1 2566 0
	ldr	r3, .L210+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2568 0
	ldr	r3, .L210
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2572 0
	ldr	r3, [fp, #-8]
	.loc 1 2573 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L211:
	.align	2
.L210:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	2499
	.word	system_preserves_recursive_MUTEX
	.word	2501
	.word	foal_irri
	.word	foal_irri+16
.LFE20:
	.size	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists, .-FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
	.section .rodata
	.align	2
.LC24:
	.ascii	"FOAL_IRRI: couldn't find no current station?\000"
	.section	.text.FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate
	.type	FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate, %function
FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate:
.LFB21:
	.loc 1 2577 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI62:
	add	fp, sp, #4
.LCFI63:
	sub	sp, sp, #12
.LCFI64:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 2581 0
	ldr	r3, .L220
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L220+4
	ldr	r3, .L220+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2588 0
	ldr	r0, .L220+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 2590 0
	b	.L213
.L217:
	.loc 1 2594 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bne	.L214
	.loc 1 2594 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L214
	.loc 1 2596 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L219
	.loc 1 2599 0
	ldr	r3, .L220+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L220+4
	ldr	r3, .L220+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2602 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L220+24
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #32
	strb	r3, [r2, #0]
	.loc 1 2604 0
	ldr	r3, .L220+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2609 0
	b	.L219
.L214:
	.loc 1 2613 0
	ldr	r0, .L220+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L213:
	.loc 1 2590 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L217
	b	.L216
.L219:
	.loc 1 2609 0
	mov	r0, r0	@ nop
.L216:
	.loc 1 2619 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L218
	.loc 1 2621 0
	ldr	r0, .L220+28
	bl	Alert_Message
.L218:
	.loc 1 2626 0
	ldr	r3, .L220
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2627 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L221:
	.align	2
.L220:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	2581
	.word	foal_irri+36
	.word	station_preserves_recursive_MUTEX
	.word	2599
	.word	station_preserves
	.word	.LC24
.LFE21:
	.size	FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate, .-FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate
	.section	.text.FOAL_extract_clear_mlb_gids,"ax",%progbits
	.align	2
	.global	FOAL_extract_clear_mlb_gids
	.type	FOAL_extract_clear_mlb_gids, %function
FOAL_extract_clear_mlb_gids:
.LFB22:
	.loc 1 2648 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI65:
	add	fp, sp, #4
.LCFI66:
	sub	sp, sp, #24
.LCFI67:
	str	r0, [fp, #-28]
	.loc 1 2651 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 2654 0
	ldr	r3, .L234
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L234+4
	ldr	r3, .L234+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2659 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #0]
	ldrb	r3, [r3, #0]
	strb	r3, [fp, #-13]
	.loc 1 2661 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 2663 0
	ldrb	r3, [fp, #-13]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L223
	.loc 1 2663 0 is_stmt 0 discriminator 1
	ldrb	r3, [fp, #-13]	@ zero_extendqisi2
	cmp	r3, #4
	bhi	.L223
.LBB9:
	.loc 1 2667 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L224
.L232:
.LBB10:
	.loc 1 2673 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #0]
	sub	r2, fp, #22
	mov	r0, r2
	mov	r1, r3
	mov	r2, #2
	bl	memcpy
	.loc 1 2675 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #0]
	add	r2, r3, #2
	ldr	r3, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 2677 0
	ldrh	r3, [fp, #-22]
	mov	r0, r3
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [fp, #-20]
	.loc 1 2679 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L225
	.loc 1 2681 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #500]	@ zero_extendqisi2
	cmp	r3, #1
	beq	.L226
	.loc 1 2682 0 discriminator 1
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #501]	@ zero_extendqisi2
	.loc 1 2681 0 discriminator 1
	cmp	r3, #1
	beq	.L226
	.loc 1 2683 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #502]	@ zero_extendqisi2
	.loc 1 2682 0
	cmp	r3, #1
	bne	.L227
.L226:
	.loc 1 2706 0
	ldr	r3, [fp, #-20]
	mov	r2, #150
	str	r2, [r3, #456]
.L227:
	.loc 1 2712 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #500]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L228
	.loc 1 2714 0
	ldrh	r3, [fp, #-22]
	mov	r0, r3
	bl	Alert_mainline_break_cleared
	.loc 1 2716 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	strb	r2, [r3, #500]
.L228:
	.loc 1 2719 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #501]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L229
	.loc 1 2721 0
	ldrh	r3, [fp, #-22]
	mov	r0, r3
	bl	Alert_mainline_break_cleared
	.loc 1 2723 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	strb	r2, [r3, #501]
.L229:
	.loc 1 2726 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #502]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L230
	.loc 1 2728 0
	ldrh	r3, [fp, #-22]
	mov	r0, r3
	bl	Alert_mainline_break_cleared
	.loc 1 2730 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	strb	r2, [r3, #502]
	b	.L230
.L225:
	.loc 1 2736 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 2738 0
	mov	r0, r0	@ nop
.LBE10:
.LBE9:
	.loc 1 2664 0
	b	.L233
.L230:
.LBB11:
	.loc 1 2667 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L224:
	.loc 1 2667 0 is_stmt 0 discriminator 1
	ldrb	r2, [fp, #-13]	@ zero_extendqisi2
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bgt	.L232
.LBE11:
	.loc 1 2664 0 is_stmt 1
	b	.L233
.L223:
	.loc 1 2747 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L233:
	.loc 1 2750 0
	ldr	r3, .L234
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2752 0
	ldr	r3, [fp, #-8]
	.loc 1 2753 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L235:
	.align	2
.L234:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	2654
.LFE22:
	.size	FOAL_extract_clear_mlb_gids, .-FOAL_extract_clear_mlb_gids
	.section	.text.FOAL_IRRI_load_mlb_info_into_outgoing_token,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_load_mlb_info_into_outgoing_token
	.type	FOAL_IRRI_load_mlb_info_into_outgoing_token, %function
FOAL_IRRI_load_mlb_info_into_outgoing_token:
.LFB23:
	.loc 1 2776 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI68:
	add	fp, sp, #4
.LCFI69:
	sub	sp, sp, #20
.LCFI70:
	str	r0, [fp, #-24]
	.loc 1 2779 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 2786 0
	ldr	r0, .L242
	ldr	r1, .L242+4
	ldr	r2, .L242+8
	bl	mem_malloc_debug
	mov	r2, r0
	ldr	r3, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 2790 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 2792 0
	mov	r3, #0
	strb	r3, [fp, #-13]
	.loc 1 2794 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 2797 0
	ldr	r3, .L242+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L242+4
	ldr	r3, .L242+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 2801 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L237
.L239:
	.loc 1 2804 0
	ldr	r0, .L242+20
	ldr	r2, [fp, #-20]
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L238
	.loc 1 2809 0
	ldr	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r2, r2, #516
	ldr	r3, .L242+20
	add	r3, r2, r3
	mov	r0, r3
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	mov	r3, r0
	cmp	r3, #0
	beq	.L238
	.loc 1 2813 0
	ldr	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r2, r2, #16
	ldr	r3, .L242+20
	add	r3, r2, r3
	ldr	r0, [fp, #-12]
	mov	r1, r3
	mov	r2, #4
	bl	memcpy
	.loc 1 2814 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	str	r3, [fp, #-12]
	.loc 1 2815 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 2817 0
	ldr	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r2, r2, #516
	ldr	r3, .L242+20
	add	r3, r2, r3
	ldr	r0, [fp, #-12]
	mov	r1, r3
	mov	r2, #16
	bl	memcpy
	.loc 1 2818 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #16
	str	r3, [fp, #-12]
	.loc 1 2819 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #16
	str	r3, [fp, #-8]
	.loc 1 2821 0
	ldrb	r3, [fp, #-13]
	add	r3, r3, #1
	strb	r3, [fp, #-13]
.L238:
	.loc 1 2801 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L237:
	.loc 1 2801 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #3
	ble	.L239
	.loc 1 2826 0 is_stmt 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L240
	.loc 1 2830 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, .L242+4
	ldr	r2, .L242+24
	bl	mem_free_debug
	.loc 1 2832 0
	ldr	r3, [fp, #-24]
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L241
.L240:
	.loc 1 2838 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 2842 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	ldrb	r2, [fp, #-13]
	strb	r2, [r3, #0]
.L241:
	.loc 1 2845 0
	ldr	r3, .L242+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 2847 0
	ldr	r3, [fp, #-8]
	.loc 1 2848 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L243:
	.align	2
.L242:
	.word	257
	.word	.LC0
	.word	2786
	.word	system_preserves_recursive_MUTEX
	.word	2797
	.word	system_preserves
	.word	2830
.LFE23:
	.size	FOAL_IRRI_load_mlb_info_into_outgoing_token, .-FOAL_IRRI_load_mlb_info_into_outgoing_token
	.section	.text.FOAL_IRRI_translate_alert_actions_to_flow_checking_group,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_translate_alert_actions_to_flow_checking_group
	.type	FOAL_IRRI_translate_alert_actions_to_flow_checking_group, %function
FOAL_IRRI_translate_alert_actions_to_flow_checking_group:
.LFB24:
	.loc 1 2859 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI71:
	add	fp, sp, #0
.LCFI72:
	sub	sp, sp, #16
.LCFI73:
	str	r0, [fp, #-16]
	.loc 1 2862 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 2868 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	bic	r3, r3, #63
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L245
	.loc 1 2870 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L246
.L245:
	.loc 1 2874 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L246:
	.loc 1 2880 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	and	r3, r3, #3
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L247
	.loc 1 2882 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L248
.L247:
	.loc 1 2886 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L248:
	.loc 1 2890 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L249
	.loc 1 2890 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L249
	.loc 1 2892 0 is_stmt 1
	mov	r3, #3
	str	r3, [fp, #-4]
	b	.L250
.L249:
	.loc 1 2895 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L251
	.loc 1 2895 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L251
	.loc 1 2897 0 is_stmt 1
	mov	r3, #2
	str	r3, [fp, #-4]
	b	.L250
.L251:
	.loc 1 2900 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L252
	.loc 1 2900 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L252
	.loc 1 2902 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-4]
	b	.L250
.L252:
	.loc 1 2905 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L250
	.loc 1 2905 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L250
	.loc 1 2907 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-4]
.L250:
	.loc 1 2910 0
	ldr	r3, [fp, #-4]
	.loc 1 2911 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE24:
	.size	FOAL_IRRI_translate_alert_actions_to_flow_checking_group, .-FOAL_IRRI_translate_alert_actions_to_flow_checking_group
	.section	.text.FOAL_IRRI_we_test_flow_when_ON_for_this_reason,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_we_test_flow_when_ON_for_this_reason
	.type	FOAL_IRRI_we_test_flow_when_ON_for_this_reason, %function
FOAL_IRRI_we_test_flow_when_ON_for_this_reason:
.LFB25:
	.loc 1 2915 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI74:
	add	fp, sp, #0
.LCFI75:
	sub	sp, sp, #8
.LCFI76:
	str	r0, [fp, #-8]
	.loc 1 2918 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 2927 0
	ldr	r3, [fp, #-8]
	cmp	r3, #7
	bne	.L254
	.loc 1 2934 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L255
.L254:
	.loc 1 2937 0
	ldr	r3, [fp, #-8]
	cmp	r3, #5
	bne	.L256
	.loc 1 2939 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L255
.L256:
	.loc 1 2942 0
	ldr	r3, [fp, #-8]
	cmp	r3, #4
	bne	.L255
	.loc 1 2944 0
	mov	r3, #0
	str	r3, [fp, #-4]
.L255:
	.loc 1 2947 0
	ldr	r3, [fp, #-4]
	.loc 1 2948 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE25:
	.size	FOAL_IRRI_we_test_flow_when_ON_for_this_reason, .-FOAL_IRRI_we_test_flow_when_ON_for_this_reason
	.section .rodata
	.align	2
.LC25:
	.ascii	"FOAL: dirty list item. : %s, %u\000"
	.align	2
.LC26:
	.ascii	"FOAL: Station Skipped (list full)\000"
	.section	.text.nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc,"ax",%progbits
	.align	2
	.type	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc, %function
nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc:
.LFB26:
	.loc 1 2971 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI77:
	add	fp, sp, #4
.LCFI78:
	sub	sp, sp, #8
.LCFI79:
	.loc 1 2976 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 2978 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L258
.L262:
	.loc 1 2980 0
	ldr	r0, .L264
	ldr	r2, [fp, #-12]
	mov	r1, #148
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #5
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L259
	.loc 1 2987 0
	ldr	r0, .L264
	ldr	r2, [fp, #-12]
	mov	r1, #172
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #5
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L259
	.loc 1 2991 0
	ldr	r0, .L264
	ldr	r2, [fp, #-12]
	mov	r1, #160
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #5
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L260
	.loc 1 2994 0
	ldr	r0, .L264+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L264+8
	mov	r1, r3
	ldr	r2, .L264+12
	bl	Alert_Message_va
.L260:
	.loc 1 2999 0
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #5
	mov	r2, r3
	ldr	r3, .L264+16
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 3003 0
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #5
	mov	r2, r3
	ldr	r3, .L264+16
	add	r3, r2, r3
	mov	r0, r3
	mov	r1, #0
	mov	r2, #96
	bl	memset
	.loc 1 3005 0
	b	.L261
.L259:
	.loc 1 2978 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L258:
	.loc 1 2978 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r3, .L264+20
	cmp	r2, r3
	bls	.L262
.L261:
	.loc 1 3010 0 is_stmt 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L263
	.loc 1 3012 0
	ldr	r0, .L264+24
	bl	Alert_Message
.L263:
	.loc 1 3015 0
	ldr	r3, [fp, #-8]
	.loc 1 3016 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L265:
	.align	2
.L264:
	.word	foal_irri
	.word	.LC0
	.word	.LC25
	.word	2994
	.word	foal_irri+140
	.word	767
	.word	.LC26
.LFE26:
	.size	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc, .-nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	.section .rodata
	.align	2
.LC27:
	.ascii	"FOAL: problem...can't add to list. : %s, %u\000"
	.section	.text._nm_nm_nm_add_to_the_irrigation_list,"ax",%progbits
	.align	2
	.type	_nm_nm_nm_add_to_the_irrigation_list, %function
_nm_nm_nm_add_to_the_irrigation_list:
.LFB27:
	.loc 1 3168 0
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI80:
	add	fp, sp, #4
.LCFI81:
	sub	sp, sp, #64
.LCFI82:
	str	r0, [fp, #-56]
	str	r1, [fp, #-60]
	str	r2, [fp, #-64]
	str	r3, [fp, #-68]
	.loc 1 3169 0
	ldr	r3, .L302+64	@ float
	str	r3, [fp, #-36]	@ float
	.loc 1 3177 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 3201 0
	ldr	r0, [fp, #-60]
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	mov	r3, r0
	mov	r0, r3
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	mov	r2, r0
	ldr	r3, [fp, #-56]
	str	r2, [r3, #40]
	.loc 1 3203 0
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #40]
	cmp	r3, #0
	beq	.L267
	.loc 1 3203 0 is_stmt 0 discriminator 1
	sub	r3, fp, #40
	ldr	r0, [fp, #-60]
	mov	r1, r3
	bl	STATION_PRESERVES_get_index_using_ptr_to_station_struct
	mov	r3, r0
	cmp	r3, #0
	beq	.L267
	.loc 1 3205 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 3209 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #64]
	cmp	r3, #0
	beq	.L268
	.loc 1 3214 0
	ldr	r2, [fp, #-40]
	ldr	r1, .L302+4
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrh	r3, [r3, #2]
	cmp	r3, #0
	beq	.L268
	.loc 1 3218 0
	ldr	r2, [fp, #-40]
	ldr	r1, .L302+4
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrh	r3, [r3, #2]
	mov	r2, r3
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #1
	mov	r2, r3
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #52]
	cmp	r2, r3
	bcc	.L269
	.loc 1 3220 0
	ldr	r1, [fp, #-40]
	ldr	r2, [fp, #-40]
	ldr	r0, .L302+4
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrh	r2, [r3, #2]
	ldr	r3, [fp, #-56]
	ldr	r0, [r3, #52]
	ldr	r3, .L302+8
	umull	ip, r3, r0, r3
	mov	r3, r3, lsr #2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L302+4
	mov	r3, #136
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #2]	@ movhi
	.loc 1 3222 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #68]
	.loc 1 3224 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L268
.L269:
	.loc 1 3228 0
	ldr	r3, [fp, #-56]
	ldr	r1, [r3, #52]
	ldr	r2, [fp, #-40]
	ldr	r0, .L302+4
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrh	r3, [r3, #2]
	mov	r2, r3
	mov	r3, r2
	mov	r2, r2, asl #2
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	add	r2, r1, r3
	ldr	r3, [fp, #-56]
	str	r2, [r3, #52]
	.loc 1 3230 0
	ldr	r2, [fp, #-40]
	ldr	r1, .L302+4
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	strh	r2, [r3, #2]	@ movhi
	.loc 1 3232 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #72]
.L268:
	.loc 1 3241 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #76]
	cmp	r3, #0
	beq	.L270
.LBB12:
	.loc 1 3246 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #76]
	orr	r3, r3, #4
	strb	r3, [r2, #76]
	.loc 1 3255 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 3257 0
	sub	r3, fp, #44
	mov	r0, #1
	mov	r1, r3
	bl	WEATHER_TABLES_get_rain_table_entry_for_index
	mov	r3, r0
	cmp	r3, #0
	beq	.L271
	.loc 1 3274 0
	ldrh	r3, [fp, #-42]
	cmp	r3, #1
	beq	.L272
	.loc 1 3275 0 discriminator 1
	ldrh	r3, [fp, #-42]
	.loc 1 3274 0 discriminator 1
	cmp	r3, #4
	beq	.L272
	.loc 1 3276 0
	ldrh	r3, [fp, #-42]
	.loc 1 3275 0
	cmp	r3, #5
	bne	.L271
	.loc 1 3276 0
	ldrh	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L271
.L272:
	.loc 1 3279 0
	mov	r3, #1
	str	r3, [fp, #-16]
.L271:
	.loc 1 3288 0
	ldr	r3, .L302+12
	ldr	r3, [r3, #60]
	cmp	r3, #0
	bne	.L273
	.loc 1 3288 0 is_stmt 0 discriminator 1
	ldr	r3, .L302+12
	ldr	r3, [r3, #56]
	cmp	r3, #0
	beq	.L274
.L273:
	.loc 1 3290 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-16]
.L274:
	.loc 1 3295 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L270
	.loc 1 3297 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #80]
	.loc 1 3299 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L270:
.LBE12:
	.loc 1 3305 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #40]
	cmp	r3, #0
	beq	.L275
	.loc 1 3308 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #76]
	orr	r3, r3, #64
	strb	r3, [r2, #76]
	.loc 1 3310 0
	ldr	r3, .L302+12
	ldr	r3, [r3, #100]
	cmp	r3, #0
	beq	.L275
	.loc 1 3312 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #44]
	.loc 1 3314 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L275:
	.loc 1 3320 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #48]
	cmp	r3, #0
	beq	.L276
	.loc 1 3323 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #76]
	orr	r3, r3, #128
	strb	r3, [r2, #76]
	.loc 1 3325 0
	ldr	r3, .L302+12
	ldr	r3, [r3, #104]
	cmp	r3, #0
	beq	.L276
	.loc 1 3327 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #52]
	.loc 1 3329 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L276:
	.loc 1 3335 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #56]
	cmp	r3, #0
	beq	.L277
	.loc 1 3338 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #77]
	orr	r3, r3, #1
	strb	r3, [r2, #77]
.L277:
	.loc 1 3354 0
	ldr	r3, [fp, #-56]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L302+16
	add	r2, r2, #36
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L278
	.loc 1 3356 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #84]
	.loc 1 3358 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L278:
	.loc 1 3365 0
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #40]
	add	r3, r3, #500
	mov	r0, r3
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	mov	r3, r0
	cmp	r3, #0
	beq	.L279
	.loc 1 3370 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #8]
	.loc 1 3372 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L279:
	.loc 1 3379 0
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #40]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #16
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L280
	.loc 1 3384 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #12]
	.loc 1 3386 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L280:
	.loc 1 3393 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #16]
	cmp	r3, #1
	bne	.L281
	.loc 1 3398 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #76]
	orr	r3, r3, #8
	strb	r3, [r2, #76]
	.loc 1 3402 0
	bl	NETWORK_CONFIG_get_controller_off
	mov	r3, r0
	cmp	r3, #1
	bne	.L281
	.loc 1 3404 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #20]
	.loc 1 3406 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L281:
	.loc 1 3414 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #24]
	cmp	r3, #1
	bne	.L282
	.loc 1 3419 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #76]
	orr	r3, r3, #16
	strb	r3, [r2, #76]
	.loc 1 3424 0
	ldr	r2, [fp, #-40]
	ldr	r1, .L302+4
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #1
	bne	.L282
	.loc 1 3426 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #28]
	.loc 1 3428 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L282:
	.loc 1 3436 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #32]
	cmp	r3, #1
	bne	.L283
	.loc 1 3441 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #76]
	orr	r3, r3, #32
	strb	r3, [r2, #76]
	.loc 1 3446 0
	ldr	r2, [fp, #-40]
	ldr	r1, .L302+4
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r3, r3, lsr #3
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #1
	bne	.L283
	.loc 1 3448 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #36]
	.loc 1 3450 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L283:
	.loc 1 3458 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #92]
	cmp	r3, #0
	beq	.L284
	.loc 1 3464 0
	ldr	r0, [fp, #-60]
	bl	nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag
	mov	r3, r0
	cmp	r3, #0
	bne	.L284
	.loc 1 3466 0
	ldr	r0, [fp, #-60]
	bl	STATION_get_moisture_balance_percent
	fmsr	s14, r0
	flds	s15, .L302
	fcmpes	s14, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L284
	.loc 1 3468 0
	ldr	r3, [fp, #-68]
	mov	r2, #1
	str	r2, [r3, #96]
	.loc 1 3470 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L284:
	.loc 1 3477 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L301
.LBB13:
	.loc 1 3480 0
	ldr	r3, .L302+48
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L302+52
	ldr	r3, .L302+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 3484 0
	ldr	r3, .L302+24
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 3495 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 3497 0
	ldr	r0, .L302+44
	bl	nm_ListGetFirst
	str	r0, [fp, #-20]
	.loc 1 3499 0
	b	.L286
.L290:
	.loc 1 3501 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 3506 0
	ldr	r3, [fp, #-20]
	ldrb	r2, [r3, #91]	@ zero_extendqisi2
	ldr	r3, [fp, #-56]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L287
	.loc 1 3506 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	ldrb	r2, [r3, #90]	@ zero_extendqisi2
	ldr	r3, [fp, #-56]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L287
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r2, r3, #255
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	cmp	r2, r3
	bne	.L287
	.loc 1 3508 0 is_stmt 1
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #128
	bne	.L288
	.loc 1 3513 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 3519 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #52]
	add	r2, r2, r3
	ldr	r3, [fp, #-20]
	str	r2, [r3, #52]
	.loc 1 3526 0
	ldr	r2, [fp, #-20]
	ldrb	r3, [r2, #76]
	orr	r3, r3, #2
	strb	r3, [r2, #76]
	.loc 1 3534 0
	ldr	r3, [fp, #-56]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 3536 0
	ldr	r3, [fp, #-56]
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 3538 0
	ldr	r3, [fp, #-56]
	mov	r2, #0
	str	r2, [r3, #20]
	.loc 1 3543 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L287
.L288:
	.loc 1 3548 0
	mov	r3, #1
	str	r3, [fp, #-28]
.L287:
	.loc 1 3552 0
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L289
	.loc 1 3555 0
	ldr	r3, .L302+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L302+52
	ldr	r3, .L302+32
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 3559 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-32]
	.loc 1 3565 0
	ldr	r0, .L302+44
	ldr	r1, [fp, #-20]
	mov	r2, #102
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-20]
	.loc 1 3572 0
	ldr	r0, .L302+36
	ldr	r1, [fp, #-32]
	ldr	r2, .L302+52
	ldr	r3, .L302+40
	bl	nm_ListRemove_debug
	.loc 1 3574 0
	ldr	r3, .L302+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L286
.L289:
	.loc 1 3578 0
	ldr	r0, .L302+44
	ldr	r1, [fp, #-20]
	bl	nm_ListGetNext
	str	r0, [fp, #-20]
.L286:
	.loc 1 3499 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L290
	.loc 1 3583 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L291
	.loc 1 3588 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-56]
	str	r2, [r3, #44]
	.loc 1 3592 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #74]
	bic	r3, r3, #64
	strb	r3, [r2, #74]
	.loc 1 3602 0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	beq	.L292
	.loc 1 3602 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #256
	bne	.L293
.L292:
	.loc 1 3604 0 is_stmt 1
	ldr	r0, [fp, #-60]
	bl	nm_STATION_get_soak_minutes
	mov	r2, r0
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, [fp, #-56]
	str	r2, [r3, #64]
	b	.L294
.L303:
	.align	2
.L302:
	.word	1056964608
	.word	station_preserves
	.word	-1431655765
	.word	weather_preserves
	.word	irri_comm
	.word	3480
	.word	foal_irri
	.word	system_preserves_recursive_MUTEX
	.word	3555
	.word	foal_irri+56
	.word	3572
	.word	foal_irri+16
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	.LC27
	.word	3750
	.word	1114636288
.L293:
	.loc 1 3608 0
	ldr	r3, [fp, #-56]
	mov	r2, #0
	str	r2, [r3, #64]
.L294:
	.loc 1 3612 0
	ldr	r3, [fp, #-56]
	mov	r2, #0
	str	r2, [r3, #60]
	.loc 1 3614 0
	ldr	r3, [fp, #-56]
	mov	r2, #0
	str	r2, [r3, #48]
	.loc 1 3620 0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L295
.LBB14:
	.loc 1 3623 0
	sub	r3, fp, #52
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 3624 0
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #52]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-36]
	fdivs	s15, s14, s15
	fmrs	r0, s15
	ldr	r1, [fp, #-60]
	sub	r3, fp, #52
	ldmia	r3, {r2, r3}
	bl	nm_BUDGET_calc_time_adjustment
	fmsr	s14, r0
	flds	s15, .L302+64
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	ldr	r3, [fp, #-56]
	str	r2, [r3, #52]
.L295:
.LBE14:
	.loc 1 3629 0
	ldr	r0, [fp, #-60]
	bl	PUMP_get_station_uses_pump
	mov	r3, r0
	and	r3, r3, #255
	and	r3, r3, #1
	and	r2, r3, #255
	ldr	r1, [fp, #-56]
	ldrb	r3, [r1, #73]
	and	r2, r2, #1
	bic	r3, r3, #8
	mov	r2, r2, asl #3
	orr	r3, r2, r3
	strb	r3, [r1, #73]
	.loc 1 3633 0
	ldr	r0, [fp, #-60]
	bl	STATION_get_GID_station_group
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-56]
	strh	r2, [r3, #86]	@ movhi
	.loc 1 3640 0
	ldr	r0, [fp, #-60]
	bl	ALERT_ACTIONS_get_station_high_flow_action
	mov	r3, r0
	and	r3, r3, #255
	and	r3, r3, #3
	and	r2, r3, #255
	ldr	r1, [fp, #-56]
	ldrb	r3, [r1, #73]
	and	r2, r2, #3
	bic	r3, r3, #192
	mov	r2, r2, asl #6
	orr	r3, r2, r3
	strb	r3, [r1, #73]
	.loc 1 3642 0
	ldr	r0, [fp, #-60]
	bl	ALERT_ACTIONS_get_station_low_flow_action
	mov	r3, r0
	and	r3, r3, #255
	and	r3, r3, #3
	and	r1, r3, #255
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #74]
	and	r1, r1, #3
	bic	r3, r3, #3
	orr	r3, r1, r3
	strb	r3, [r2, #74]
	.loc 1 3644 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	bic	r3, r3, #2
	strb	r3, [r2, #75]
	.loc 1 3653 0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r0, r3
	bl	FOAL_IRRI_we_test_flow_when_ON_for_this_reason
	mov	r3, r0
	cmp	r3, #0
	beq	.L296
	.loc 1 3655 0
	ldr	r3, [fp, #-56]
	add	r3, r3, #72
	mov	r0, r3
	bl	FOAL_IRRI_translate_alert_actions_to_flow_checking_group
	mov	r3, r0
	and	r3, r3, #255
	and	r3, r3, #3
	and	r2, r3, #255
	ldr	r1, [fp, #-56]
	ldrb	r3, [r1, #74]
	and	r2, r2, #3
	bic	r3, r3, #12
	mov	r2, r2, asl #2
	orr	r3, r2, r3
	strb	r3, [r1, #74]
	.loc 1 3657 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	orr	r3, r3, #1
	strb	r3, [r2, #75]
	b	.L297
.L296:
	.loc 1 3661 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #74]
	bic	r3, r3, #12
	strb	r3, [r2, #74]
	.loc 1 3663 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	bic	r3, r3, #1
	strb	r3, [r2, #75]
.L297:
	.loc 1 3668 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #73]
	bic	r3, r3, #32
	strb	r3, [r2, #73]
	.loc 1 3670 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	bic	r3, r3, #16
	strb	r3, [r2, #75]
	.loc 1 3672 0
	ldr	r3, [fp, #-56]
	ldrb	r3, [r3, #74]	@ zero_extendqisi2
	and	r3, r3, #12
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L298
	.loc 1 3673 0 discriminator 1
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #40]
	.loc 1 3672 0 discriminator 1
	ldrb	r3, [r3, #465]	@ zero_extendqisi2
	bic	r3, r3, #127
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L298
	.loc 1 3674 0
	ldr	r3, [fp, #-56]
	.loc 1 3673 0
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L298
	.loc 1 3676 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	orr	r3, r3, #8
	strb	r3, [r2, #75]
	b	.L299
.L298:
	.loc 1 3680 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	bic	r3, r3, #8
	strb	r3, [r2, #75]
.L299:
	.loc 1 3685 0
	ldr	r0, [fp, #-60]
	bl	nm_STATION_get_expected_flow_rate_gpm
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-56]
	strh	r2, [r3, #80]	@ movhi
	.loc 1 3689 0
	ldr	r0, [fp, #-60]
	bl	WEATHER_get_station_uses_wind
	mov	r3, r0
	and	r3, r3, #255
	and	r3, r3, #1
	and	r2, r3, #255
	ldr	r1, [fp, #-56]
	ldrb	r3, [r1, #74]
	and	r2, r2, #1
	bic	r3, r3, #16
	mov	r2, r2, asl #4
	orr	r3, r2, r3
	strb	r3, [r1, #74]
	.loc 1 3691 0
	ldr	r0, [fp, #-60]
	bl	WEATHER_get_station_uses_rain
	mov	r3, r0
	and	r3, r3, #255
	and	r3, r3, #1
	and	r2, r3, #255
	ldr	r1, [fp, #-56]
	ldrb	r3, [r1, #74]
	and	r2, r2, #1
	bic	r3, r3, #32
	mov	r2, r2, asl #5
	orr	r3, r2, r3
	strb	r3, [r1, #74]
	.loc 1 3696 0
	ldr	r0, [fp, #-64]
	bl	STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group
	mov	r2, r0
	ldr	r3, [fp, #-56]
	str	r2, [r3, #92]
	.loc 1 3700 0
	ldr	r0, [fp, #-60]
	bl	PRIORITY_get_station_priority_level
	mov	r3, r0
	and	r3, r3, #255
	and	r3, r3, #3
	and	r2, r3, #255
	ldr	r1, [fp, #-56]
	ldrb	r3, [r1, #72]
	and	r2, r2, #3
	bic	r3, r3, #48
	mov	r2, r2, asl #4
	orr	r3, r2, r3
	strb	r3, [r1, #72]
	.loc 1 3704 0
	ldr	r0, [fp, #-60]
	bl	LINE_FILL_TIME_get_station_line_fill_seconds
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-56]
	strh	r2, [r3, #82]	@ movhi
	.loc 1 3706 0
	ldr	r0, [fp, #-60]
	bl	DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-56]
	strh	r2, [r3, #84]	@ movhi
	.loc 1 3711 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	bic	r3, r3, #4
	strb	r3, [r2, #75]
	.loc 1 3713 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	bic	r3, r3, #32
	strb	r3, [r2, #75]
	.loc 1 3715 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	bic	r3, r3, #64
	strb	r3, [r2, #75]
	.loc 1 3717 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	bic	r3, r3, #128
	strb	r3, [r2, #75]
	.loc 1 3719 0
	ldr	r3, [fp, #-56]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L300
	.loc 1 3722 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #75]
	orr	r3, r3, #128
	strb	r3, [r2, #75]
.L300:
	.loc 1 3726 0
	ldr	r0, .L302+44
	ldr	r1, [fp, #-56]
	bl	nm_ListInsertTail
	mov	r3, r0
	cmp	r3, #0
	bne	.L291
	.loc 1 3729 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 3735 0
	ldr	r2, [fp, #-56]
	ldrb	r3, [r2, #76]
	orr	r3, r3, #2
	strb	r3, [r2, #76]
	.loc 1 3740 0
	mov	r0, #0
	ldr	r1, [fp, #-56]
	bl	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
.L291:
	.loc 1 3744 0
	ldr	r3, .L302+48
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.LBE13:
	.loc 1 3477 0
	b	.L301
.L267:
	.loc 1 3750 0
	ldr	r0, .L302+52
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L302+56
	mov	r1, r3
	ldr	r2, .L302+60
	bl	Alert_Message_va
.L301:
	.loc 1 3753 0
	ldr	r3, [fp, #-8]
	.loc 1 3754 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE27:
	.size	_nm_nm_nm_add_to_the_irrigation_list, .-_nm_nm_nm_add_to_the_irrigation_list
	.section	.text.FOAL_IRRI_return_stop_date_according_to_stop_time,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_return_stop_date_according_to_stop_time
	.type	FOAL_IRRI_return_stop_date_according_to_stop_time, %function
FOAL_IRRI_return_stop_date_according_to_stop_time:
.LFB28:
	.loc 1 3777 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI83:
	add	fp, sp, #0
.LCFI84:
	sub	sp, sp, #12
.LCFI85:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 3783 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L308
	cmp	r2, r3
	bne	.L305
	.loc 1 3787 0
	ldr	r3, .L308+4
	str	r3, [fp, #-4]
	b	.L306
.L305:
	.loc 1 3791 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bls	.L307
	.loc 1 3794 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r3, #1
	str	r3, [fp, #-4]
	b	.L306
.L307:
	.loc 1 3799 0
	ldr	r3, [fp, #-8]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	str	r3, [fp, #-4]
.L306:
	.loc 1 3804 0
	ldr	r3, [fp, #-4]
	.loc 1 3805 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L309:
	.align	2
.L308:
	.word	86400
	.word	65535
.LFE28:
	.size	FOAL_IRRI_return_stop_date_according_to_stop_time, .-FOAL_IRRI_return_stop_date_according_to_stop_time
	.section	.text.nm_at_starttime_decrement_NOW_days_count,"ax",%progbits
	.align	2
	.type	nm_at_starttime_decrement_NOW_days_count, %function
nm_at_starttime_decrement_NOW_days_count:
.LFB29:
	.loc 1 3826 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI86:
	add	fp, sp, #4
.LCFI87:
	sub	sp, sp, #12
.LCFI88:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 3827 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L316
	cmp	r2, r3
	bls	.L311
	.loc 1 3829 0
	ldr	r0, .L316+4
	ldr	r1, .L316+8
	bl	Alert_index_out_of_range_with_filename
	b	.L310
.L311:
	.loc 1 3833 0
	ldr	r0, [fp, #-8]
	bl	STATION_get_no_water_days
	mov	r3, r0
	cmp	r3, #0
	beq	.L313
	.loc 1 3838 0
	ldr	r0, [fp, #-8]
	bl	STATION_decrement_no_water_days_and_return_value_after_decrement
	mov	r3, r0
	cmp	r3, #0
	bne	.L310
	.loc 1 3848 0
	ldr	r3, [fp, #-16]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	mov	r2, r3
	ldr	r3, .L316+12
	cmp	r2, r3
	bhi	.L314
	.loc 1 3850 0
	ldr	r3, [fp, #-16]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L316+16
	ldr	r1, [fp, #-12]
	mov	r3, #132
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	b	.L315
.L314:
	.loc 1 3854 0
	ldr	r3, [fp, #-16]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	mov	r3, r3, asl #8
	orr	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L316+16
	ldr	r1, [fp, #-12]
	mov	r3, #132
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
.L315:
	.loc 1 3859 0
	ldr	r1, .L316+16
	ldr	r2, [fp, #-12]
	mov	r3, #124
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, .L316+12
	str	r2, [r3, #0]
	b	.L310
.L313:
	.loc 1 3883 0
	ldr	r1, .L316+16
	ldr	r2, [fp, #-12]
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	bic	r3, r3, #4
	strb	r3, [r2, #1]
.L310:
	.loc 1 3886 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L317:
	.align	2
.L316:
	.word	2111
	.word	.LC0
	.word	3829
	.word	18000
	.word	station_preserves
.LFE29:
	.size	nm_at_starttime_decrement_NOW_days_count, .-nm_at_starttime_decrement_NOW_days_count
	.section	.text.nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_and_check_for_isolated_stations,"ax",%progbits
	.align	2
	.type	nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_and_check_for_isolated_stations, %function
nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_and_check_for_isolated_stations:
.LFB30:
	.loc 1 3909 0
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI89:
	add	fp, sp, #4
.LCFI90:
	sub	sp, sp, #36
.LCFI91:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	str	r2, [fp, #-36]
	str	r3, [fp, #-40]
	.loc 1 3921 0
	ldr	r3, [fp, #-40]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 3925 0
	ldr	r2, [fp, #-32]
	ldr	r3, .L326
	cmp	r2, r3
	bls	.L319
	.loc 1 3927 0
	ldr	r0, .L326+4
	ldr	r1, .L326+8
	bl	Alert_index_out_of_range_with_filename
	b	.L318
.L319:
	.loc 1 3931 0
	ldr	r0, [fp, #-28]
	bl	STATION_get_no_water_days
	mov	r3, r0
	cmp	r3, #0
	beq	.L321
	.loc 1 3936 0
	ldr	r1, .L326+12
	ldr	r2, [fp, #-32]
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	orr	r3, r3, #4
	strb	r3, [r2, #1]
	b	.L322
.L321:
	.loc 1 3952 0
	ldr	r1, .L326+12
	ldr	r2, [fp, #-32]
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L322
	.loc 1 3954 0
	ldr	r1, .L326+12
	ldr	r2, [fp, #-32]
	mov	r3, #132
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrh	r3, [r3, #0]
	strh	r3, [fp, #-20]	@ movhi
	.loc 1 3955 0
	ldr	r1, .L326+12
	ldr	r2, [fp, #-32]
	mov	r3, #124
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-24]
	.loc 1 3957 0
	sub	r3, fp, #24
	ldr	r0, [fp, #-36]
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #0
	beq	.L322
	.loc 1 3959 0
	ldr	r1, .L326+12
	ldr	r2, [fp, #-32]
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	bic	r3, r3, #4
	strb	r3, [r2, #1]
.L322:
	.loc 1 3971 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 3973 0
	ldr	r0, [fp, #-28]
	bl	STATION_get_GID_station_group
	str	r0, [fp, #-12]
	.loc 1 3975 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L323
	.loc 1 3977 0
	ldr	r0, [fp, #-12]
	bl	STATION_GROUP_get_group_with_this_GID
	mov	r2, r0
	ldr	r3, [fp, #-40]
	str	r2, [r3, #0]
	.loc 1 3979 0
	ldr	r3, [fp, #-40]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L324
	.loc 1 3983 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L325
.L324:
	.loc 1 3987 0
	ldr	r3, [fp, #-40]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SCHEDULE_get_start_time
	str	r0, [fp, #-16]
	.loc 1 3989 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L326+16
	cmp	r2, r3
	bne	.L325
	.loc 1 3993 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L325
.L323:
	.loc 1 4001 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L325:
	.loc 1 4004 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L318
	.loc 1 4008 0
	ldr	r3, [fp, #-36]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r1, r1, asl #8
	orr	r2, r1, r2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	orr	r2, r1, r2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, asl #24
	orr	r3, r3, r2
	cmp	r3, #0
	bne	.L318
	.loc 1 4010 0
	ldr	r0, [fp, #-28]
	ldr	r1, [fp, #-32]
	ldr	r2, [fp, #-36]
	bl	nm_at_starttime_decrement_NOW_days_count
.L318:
	.loc 1 4014 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L327:
	.align	2
.L326:
	.word	2111
	.word	.LC0
	.word	3927
	.word	station_preserves
	.word	86400
.LFE30:
	.size	nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_and_check_for_isolated_stations, .-nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_and_check_for_isolated_stations
	.global	__largest_start_time_delta
	.section	.bss.__largest_start_time_delta,"aw",%nobits
	.align	2
	.type	__largest_start_time_delta, %object
	.size	__largest_start_time_delta, 4
__largest_start_time_delta:
	.space	4
	.section .rodata
	.align	2
.LC28:
	.ascii	"Foal Irri SX: error removing\000"
	.align	2
.LC29:
	.ascii	"Programmed Irrigation: NO irrigation due to specifi"
	.ascii	"ed reason(s)\000"
	.align	2
.LC30:
	.ascii	"Manual Program: no irrigation due to specified reas"
	.ascii	"on(s)\000"
	.section	.text.TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start,"ax",%progbits
	.align	2
	.global	TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start
	.type	TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start, %function
TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start:
.LFB31:
	.loc 1 4043 0
	@ args = 0, pretend = 0, frame = 440
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI92:
	fstmfdd	sp!, {d8}
.LCFI93:
	add	fp, sp, #20
.LCFI94:
	sub	sp, sp, #456
.LCFI95:
	str	r0, [fp, #-460]
	.loc 1 4063 0
	ldr	r3, .L433+8	@ float
	str	r3, [fp, #-432]	@ float
	.loc 1 4134 0
	mov	r3, #0
	str	r3, [fp, #-96]
	.loc 1 4138 0
	ldr	r3, .L433+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L433+16
	ldr	r3, .L433+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 4141 0
	ldr	r3, .L433+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L433+16
	ldr	r3, .L433+28
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 4151 0
	sub	r3, fp, #328
	mov	r0, r3
	mov	r1, #0
	mov	r2, #100
	bl	memset
	.loc 1 4153 0
	sub	r3, fp, #428
	mov	r0, r3
	mov	r1, #0
	mov	r2, #100
	bl	memset
	.loc 1 4157 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 4159 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 4161 0
	mov	r3, #0
	str	r3, [fp, #-32]
	.loc 1 4163 0
	mov	r3, #0
	str	r3, [fp, #-36]
	.loc 1 4165 0
	mov	r3, #0
	str	r3, [fp, #-40]
	.loc 1 4167 0
	mov	r3, #0
	str	r3, [fp, #-60]
	.loc 1 4169 0
	mov	r3, #0
	str	r3, [fp, #-64]
	.loc 1 4181 0
	ldr	r3, .L433+32
	ldr	r3, [r3, #0]
	str	r3, [fp, #-100]
	.loc 1 4187 0
	bl	STATION_GROUP_clear_budget_start_time_flags
	.loc 1 4191 0
	ldr	r0, .L433+36
	bl	nm_ListGetFirst
	str	r0, [fp, #-56]
	.loc 1 4193 0
	b	.L329
.L394:
	.loc 1 4197 0
	sub	r3, fp, #452
	ldr	r0, [fp, #-56]
	mov	r1, r3
	bl	STATION_PRESERVES_get_index_using_ptr_to_station_struct
	mov	r3, r0
	cmp	r3, #0
	beq	.L330
.LBB15:
	.loc 1 4208 0
	ldr	r0, [fp, #-56]
	bl	nm_STATION_get_box_index_0
	str	r0, [fp, #-104]
	.loc 1 4210 0
	ldr	r0, [fp, #-56]
	bl	nm_STATION_get_station_number_0
	str	r0, [fp, #-108]
	.loc 1 4216 0
	mov	r3, #0
	str	r3, [fp, #-76]
	.loc 1 4218 0
	mov	r3, #0
	str	r3, [fp, #-72]
	.loc 1 4225 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L433+72
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r3, r3, lsr #5
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L331
	.loc 1 4229 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L433+72
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	orr	r3, r3, #32
	strb	r3, [r2, #1]
	.loc 1 4232 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-104]
	and	r2, r3, #255
	ldr	r0, .L433+72
	mov	r3, #120
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strb	r2, [r3, #3]
	.loc 1 4234 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-108]
	and	r2, r3, #255
	ldr	r0, .L433+72
	mov	r3, #120
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strb	r2, [r3, #2]
	.loc 1 4236 0
	ldr	r4, [fp, #-452]
	ldr	r0, [fp, #-56]
	bl	STATION_get_GID_station_group
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L433+72
	mov	r3, #108
	mov	r1, r4, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
.L331:
	.loc 1 4244 0
	ldr	r1, [fp, #-452]
	ldr	r2, [fp, #-460]
	sub	r3, fp, #448
	ldr	r0, [fp, #-56]
	bl	nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_and_check_for_isolated_stations
	.loc 1 4255 0
	ldr	r3, [fp, #-448]
	cmp	r3, #0
	beq	.L332
	.loc 1 4257 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	bl	SCHEDULE_get_start_time
	str	r0, [fp, #-112]
	.loc 1 4265 0
	ldr	r3, [fp, #-460]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-112]
	cmp	r2, r3
	bne	.L332
	.loc 1 4265 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-460]
	ldrb	r3, [r3, #19]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L332
	.loc 1 4267 0 is_stmt 1
	ldr	r0, [fp, #-56]
	bl	STATION_station_is_available_for_use
	mov	r3, r0
	cmp	r3, #0
	beq	.L333
	.loc 1 4272 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_daily_et
	mov	r3, r0
	cmp	r3, #0
	beq	.L334
	.loc 1 4274 0
	ldr	r0, [fp, #-56]
	bl	STATION_get_moisture_balance
	str	r0, [fp, #-92]	@ float
	.loc 1 4276 0
	ldr	r0, [fp, #-56]
	bl	STATION_GROUP_get_soil_storage_capacity_inches_100u
	mov	r3, r0
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, .L433
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-116]
	.loc 1 4282 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_rain
	mov	r3, r0
	cmp	r3, #0
	beq	.L335
	.loc 1 4284 0
	sub	r3, fp, #456
	mov	r0, #1
	mov	r1, r3
	bl	WEATHER_TABLES_get_rain_table_entry_for_index
	.loc 1 4288 0
	ldr	r3, .L433+40
	sub	r0, fp, #20
	ldrh	r3, [r0, r3]
	cmp	r3, #100
	bls	.L335
	.loc 1 4290 0
	flds	s14, [fp, #-116]
	flds	s15, .L433+4
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-92]
.L335:
	.loc 1 4294 0
	ldr	r3, [fp, #-460]
	ldrh	r3, [r3, #8]
	ldr	r0, [fp, #-56]
	mov	r1, r3
	bl	STATION_GROUP_get_station_crop_coefficient_100u
	mov	r3, r0
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, .L433
	fdivs	s16, s14, s15
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #0
	mov	r1, #0
	ldr	r2, [fp, #-460]
	mov	r3, #1
	bl	WATERSENSE_get_et_value_after_substituting_or_limiting
	mov	r3, r0	@ float
	fmrs	r0, s16
	mov	r1, r3	@ float
	ldr	r2, [fp, #-92]	@ float
	bl	WATERSENSE_reduce_moisture_balance_by_ETc
	str	r0, [fp, #-88]	@ float
	.loc 1 4296 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_rain
	mov	r3, r0
	cmp	r3, #0
	beq	.L336
	.loc 1 4304 0
	ldr	r3, .L433+44
	sub	r0, fp, #20
	ldrh	r3, [r0, r3]
	cmp	r3, #5
	bne	.L337
	.loc 1 4306 0
	ldr	r0, [fp, #-56]
	bl	STATION_GROUP_get_station_usable_rain_percentage_100u
	str	r0, [fp, #-84]
	b	.L338
.L337:
	.loc 1 4310 0
	mov	r3, #100
	str	r3, [fp, #-84]
.L338:
	.loc 1 4313 0
	ldr	r3, .L433+40
	sub	r1, fp, #20
	ldrh	r3, [r1, r3]
	fmsr	s13, r3	@ int
	fsitos	s14, s13
	flds	s15, .L433
	fdivs	s16, s14, s15
	ldr	r0, [fp, #-56]
	bl	STATION_GROUP_get_soil_storage_capacity_inches_100u
	mov	r3, r0
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, .L433
	fdivs	s13, s14, s15
	ldr	r3, [fp, #-84]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, .L433
	fdivs	s15, s14, s15
	fmrs	r0, s16
	fmrs	r1, s13
	fmrs	r2, s15
	ldr	r3, [fp, #-88]	@ float
	bl	WATERSENSE_increase_moisture_balance_by_rain
	str	r0, [fp, #-88]	@ float
.L336:
	.loc 1 4316 0
	flds	s14, [fp, #-88]
	flds	s15, [fp, #-116]
	fdivs	s16, s14, s15
	ldr	r0, [fp, #-56]
	mov	r1, #6
	bl	STATION_get_change_bits_ptr
	mov	r3, r0
	ldr	r2, [fp, #-104]
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-56]
	fmrs	r1, s16
	mov	r2, #0
	mov	r3, #6
	bl	nm_STATION_set_moisture_balance_percent
	.loc 1 4323 0
	ldr	r1, [fp, #-452]
	flds	s14, [fp, #-88]
	flds	s15, [fp, #-116]
	fdivs	s14, s14, s15
	flds	s15, .L433
	fmuls	s14, s14, s15
	flds	s15, .L433
	fmuls	s15, s14, s15
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L433+72
	mov	r3, #72
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 4329 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L433+72
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	strh	r2, [r3, #2]	@ movhi
	b	.L333
.L334:
	.loc 1 4336 0
	ldr	r3, [fp, #-452]
	ldr	r0, [fp, #-56]
	mov	r1, r3
	bl	nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1
.L333:
	.loc 1 4344 0
	ldr	r2, [fp, #-452]
	ldr	r3, [fp, #-460]
	ldr	r0, [fp, #-56]
	mov	r1, r2
	mov	r2, r3
	bl	nm_at_starttime_decrement_NOW_days_count
	.loc 1 4354 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	ldr	r1, [fp, #-460]
	mov	r2, #1
	bl	SCHEDULE_does_it_irrigate_on_this_date
	mov	r3, r0
	cmp	r3, #1
	bne	.L332
	.loc 1 4356 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 4374 0
	ldr	r3, .L433+48
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L433+16
	ldr	r3, .L433+52
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 4378 0
	ldr	r3, .L433+56
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 4380 0
	ldr	r0, .L433+68
	bl	nm_ListGetFirst
	str	r0, [fp, #-68]
	.loc 1 4382 0
	b	.L339
.L342:
	.loc 1 4386 0
	ldr	r3, [fp, #-68]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-104]
	cmp	r2, r3
	bne	.L340
	.loc 1 4386 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-68]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-108]
	cmp	r2, r3
	bne	.L340
	ldr	r3, [fp, #-68]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L340
	.loc 1 4388 0 is_stmt 1
	ldr	r0, [fp, #-104]
	ldr	r1, [fp, #-108]
	bl	Alert_programmed_irrigation_still_running_idx
	.loc 1 4409 0
	ldr	r3, [fp, #-68]
	str	r3, [fp, #-72]
	.loc 1 4412 0
	ldr	r3, .L433+60
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L433+16
	ldr	r3, .L433+64
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 4417 0
	ldr	r0, .L433+68
	ldr	r1, [fp, #-68]
	mov	r2, #102
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-68]
	.loc 1 4419 0
	ldr	r3, .L433+60
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 4422 0
	b	.L341
.L434:
	.align	2
.L433:
	.word	1120403456
	.word	1056964608
	.word	1114636288
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	4138
	.word	station_preserves_recursive_MUTEX
	.word	4141
	.word	my_tick_count
	.word	station_info_list_hdr
	.word	-436
	.word	-434
	.word	list_foal_irri_recursive_MUTEX
	.word	4374
	.word	foal_irri
	.word	system_preserves_recursive_MUTEX
	.word	4412
	.word	foal_irri+16
	.word	station_preserves
	.word	0
	.word	1076101120
	.word	0
	.word	1078853632
.L340:
	.loc 1 4426 0
	ldr	r0, .L433+68
	ldr	r1, [fp, #-68]
	bl	nm_ListGetNext
	str	r0, [fp, #-68]
.L339:
	.loc 1 4382 0 discriminator 1
	ldr	r3, [fp, #-68]
	cmp	r3, #0
	bne	.L342
.L341:
	.loc 1 4434 0
	ldr	r0, [fp, #-56]
	bl	STATION_station_is_available_for_use
	mov	r3, r0
	cmp	r3, #0
	beq	.L343
	.loc 1 4436 0
	sub	r3, fp, #228
	mov	r0, r3
	mov	r1, #0
	mov	r2, #100
	bl	memset
	.loc 1 4449 0
	ldr	r3, [fp, #-452]
	mov	r3, r3, asl #7
	add	r2, r3, #16
	ldr	r3, .L433+72
	add	r3, r2, r3
	mov	r0, r3
	bl	nm_init_station_history_record
	.loc 1 4454 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L433+72
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #16
	strb	r3, [r2, #3]
	.loc 1 4458 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-460]
	ldr	r2, [r3, #0]
	ldr	r0, .L433+72
	mov	r3, #16
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 1 4459 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-460]
	ldrh	r2, [r3, #4]
	ldr	r0, .L433+72
	mov	r3, #44
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 4465 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-460]
	ldr	r2, [r3, #0]
	ldr	r0, .L433+72
	mov	r3, #20
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 1 4466 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-460]
	ldrh	r2, [r3, #4]
	ldr	r0, .L433+72
	mov	r3, #44
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #2]	@ movhi
	.loc 1 4467 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-460]
	ldr	r2, [r3, #0]
	ldr	r0, .L433+72
	mov	r3, #24
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 1 4468 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-460]
	ldrh	r2, [r3, #4]
	ldr	r0, .L433+72
	mov	r3, #48
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 4472 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-104]
	and	r2, r3, #255
	ldr	r0, .L433+72
	mov	r3, #68
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strb	r2, [r3, #2]
	.loc 1 4474 0
	ldr	r1, [fp, #-452]
	ldr	r3, [fp, #-108]
	and	r2, r3, #255
	ldr	r0, .L433+72
	mov	r3, #68
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strb	r2, [r3, #0]
	.loc 1 4476 0
	ldr	r4, [fp, #-452]
	ldr	r0, [fp, #-56]
	bl	STATION_get_GID_station_group
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L433+72
	mov	r3, #40
	mov	r1, r4, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #2]	@ movhi
	.loc 1 4478 0
	ldr	r4, [fp, #-452]
	ldr	r0, [fp, #-56]
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L433+72
	mov	r3, #40
	mov	r1, r4, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 4484 0
	ldr	r4, [fp, #-448]
	ldr	r3, [fp, #-448]
	mov	r0, r3
	mov	r1, #6
	bl	STATION_GROUP_get_change_bits_ptr
	mov	r2, r0
	ldr	r3, [fp, #-460]
	mov	r1, #6
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r1, #1
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	mov	r0, r4
	ldmia	r3, {r1, r2}
	mov	r3, #0
	bl	nm_SCHEDULE_set_last_ran
	.loc 1 4492 0
	ldr	r3, [fp, #-60]
	cmp	r3, #0
	bne	.L344
	.loc 1 4492 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_daily_et
	mov	r3, r0
	cmp	r3, #0
	beq	.L344
	.loc 1 4494 0 is_stmt 1
	ldr	r3, [fp, #-56]
	str	r3, [fp, #-60]
	.loc 1 4496 0
	ldr	r3, [fp, #-448]
	str	r3, [fp, #-64]
.L344:
	.loc 1 4503 0
	ldr	r4, [fp, #-452]
	ldr	r5, [fp, #-448]
	.loc 1 4508 0
	ldr	r0, [fp, #-56]
	bl	nm_STATION_get_distribution_uniformity_100u
	mov	r3, r0
	.loc 1 4503 0
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	ldr	r3, [fp, #-460]
	str	r3, [sp, #0]
	fsts	s15, [sp, #4]
	mov	r0, #0
	ldr	r1, [fp, #-56]
	mov	r2, r5
	mov	r3, #0
	bl	nm_WATERSENSE_calculate_run_minutes_for_this_station
	fmsr	s14, r0
	flds	s15, [fp, #-432]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	ldr	r1, .L433+72
	mov	r3, #52
	mov	r2, r4, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	str	r0, [r3, #0]
	.loc 1 4514 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_daily_et
	mov	r3, r0
	cmp	r3, #0
	beq	.L345
	.loc 1 4518 0
	ldr	r1, [fp, #-452]
	ldr	r2, [fp, #-452]
	ldr	r0, .L433+72
	mov	r3, #52
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r0, [fp, #-452]
	ldr	ip, .L433+72
	mov	r3, #136
	mov	r0, r0, asl #7
	add	r0, ip, r0
	add	r3, r0, r3
	ldrh	r3, [r3, #0]
	add	r2, r2, r3
	ldr	r0, .L433+72
	mov	r3, #52
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 1 4520 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L433+72
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
	.loc 1 4524 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L433+72
	mov	r3, #52
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L345
	.loc 1 4531 0
	ldr	r4, [fp, #-452]
	.loc 1 4532 0
	ldr	r3, [fp, #-452]
	.loc 1 4531 0
	mov	r3, r3, asl #7
	add	r2, r3, #52
	ldr	r3, .L433+72
	add	r5, r2, r3
	.loc 1 4533 0
	ldr	r0, [fp, #-56]
	bl	nm_STATION_get_cycle_minutes_10u
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitod	d6, s14
	fldd	d7, .L433+76
	fdivd	d6, d6, d7
	fldd	d7, .L433+84
	fmuld	d7, d6, d7
	.loc 1 4531 0
	ftouizd	s13, d7
	fmrs	ip, s13	@ int
	.loc 1 4534 0
	ldr	r3, [fp, #-452]
	.loc 1 4531 0
	mov	r3, r3, asl #7
	add	r2, r3, #16
	ldr	r3, .L433+72
	add	r3, r2, r3
	mov	r0, #0
	mov	r1, r5
	mov	r2, ip
	bl	WATERSENSE_make_min_cycle_adjustment
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L433+72
	mov	r3, #136
	mov	r1, r4, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 4536 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L433+72
	mov	r3, #52
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L345
	.loc 1 4538 0
	mov	r3, #1
	str	r3, [fp, #-140]
.L345:
	.loc 1 4550 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L433+72
	mov	r3, #52
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	.loc 1 4547 0
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, .L435
	fdivs	s16, s14, s15
	ldr	r0, [fp, #-56]
	bl	STATION_get_ET_factor_100u
	mov	r4, r0
	.loc 1 4552 0
	ldr	r3, [fp, #-460]
	ldrh	r3, [r3, #4]
	.loc 1 4547 0
	ldr	r0, [fp, #-56]
	mov	r1, r3
	bl	PERCENT_ADJUST_get_station_percentage_100u
	mov	r3, r0
	str	r4, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, #0
	ldr	r1, [fp, #-56]
	mov	r2, #0
	fmrs	r3, s16
	bl	nm_WATERSENSE_calculate_adjusted_run_time
	fmsr	s14, r0
	.loc 1 4552 0
	flds	s15, .L435
	fmuls	s15, s14, s15
	.loc 1 4547 0
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [fp, #-436]
	.loc 1 4560 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_daily_et
	mov	r3, r0
	cmp	r3, #0
	beq	.L346
	.loc 1 4562 0
	ldr	r3, [fp, #-436]
	cmp	r3, #0
	beq	.L346
	.loc 1 4567 0
	ldr	r0, [fp, #-56]
	bl	nm_STATION_get_cycle_minutes_10u
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitod	d6, s14
	fldd	d7, .L435+32
	fdivd	d6, d6, d7
	fldd	d7, .L435+40
	fmuld	d7, d6, d7
	.loc 1 4565 0
	ftouizd	s13, d7
	fmrs	ip, s13	@ int
	.loc 1 4568 0
	ldr	r3, [fp, #-452]
	.loc 1 4565 0
	mov	r3, r3, asl #7
	add	r2, r3, #16
	ldr	r3, .L435+28
	add	r3, r2, r3
	sub	r2, fp, #436
	mov	r0, #0
	mov	r1, r2
	mov	r2, ip
	bl	WATERSENSE_make_min_cycle_adjustment
	.loc 1 4570 0
	ldr	r3, [fp, #-436]
	cmp	r3, #0
	bne	.L346
	.loc 1 4572 0
	mov	r3, #1
	str	r3, [fp, #-140]
.L346:
	.loc 1 4585 0
	ldr	r1, [fp, #-452]
	ldr	r2, [fp, #-436]
	ldr	r3, .L435+4
	umull	r0, r3, r2, r3
	mov	r3, r3, lsr #2
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L435+28
	mov	r3, #48
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #2]	@ movhi
	.loc 1 4590 0
	ldr	r1, [fp, #-452]
	ldr	r2, [fp, #-452]
	ldr	r0, .L435+28
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrh	r2, [r3, #2]
	ldr	r0, .L435+28
	mov	r3, #56
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 4597 0
	ldr	r1, [fp, #-452]
	ldr	r2, [fp, #-452]
	ldr	r0, .L435+28
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrh	r2, [r3, #2]
	ldr	r0, .L435+28
	mov	r3, #60
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 4601 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	bl	SCHEDULE_get_stop_time
	mov	r3, r0
	str	r3, [fp, #-444]
	.loc 1 4605 0
	ldr	r3, [fp, #-460]
	ldrh	r2, [r3, #4]
	ldr	r3, .L435+8
	sub	r1, fp, #20
	strh	r2, [r1, r3]	@ movhi
	.loc 1 4608 0
	ldr	r2, [fp, #-444]
	ldr	r3, [fp, #-460]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L347
	.loc 1 4611 0
	mov	r3, #0
	str	r3, [fp, #-436]
	.loc 1 4613 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #4
	strb	r3, [r2, #0]
	.loc 1 4615 0
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L348
.L436:
	.align	2
.L435:
	.word	1114636288
	.word	-1431655765
	.word	-420
	.word	foal_irri+56
	.word	.LC0
	.word	4786
	.word	.LC28
	.word	station_preserves
	.word	0
	.word	1076101120
	.word	0
	.word	1078853632
.L347:
	.loc 1 4620 0
	ldr	r2, [fp, #-460]
	ldr	r3, [fp, #-444]
	mov	r0, r2
	mov	r1, r3
	bl	FOAL_IRRI_return_stop_date_according_to_stop_time
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L435+8
	sub	r0, fp, #20
	strh	r2, [r0, r3]	@ movhi
.L348:
	.loc 1 4630 0
	ldr	r3, [fp, #-448]
	mov	r0, #0
	mov	r1, r3
	mov	r2, #0
	ldr	r3, [fp, #-460]
	bl	STATION_GROUPS_skip_irrigation_due_to_a_mow_day
	mov	r3, r0
	cmp	r3, #0
	beq	.L349
	.loc 1 4632 0
	mov	r3, #1
	str	r3, [fp, #-32]
	.loc 1 4634 0
	mov	r3, #0
	str	r3, [fp, #-436]
	.loc 1 4636 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #4
	strb	r3, [r2, #3]
.L349:
	.loc 1 4641 0
	ldr	r3, [fp, #-436]
	cmp	r3, #0
	beq	.L350
	.loc 1 4645 0
	mov	r3, #0
	str	r3, [fp, #-68]
	.loc 1 4648 0
	ldr	r3, [fp, #-328]
	cmp	r3, #0
	bne	.L351
	.loc 1 4651 0
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	str	r0, [fp, #-68]
	.loc 1 4653 0
	ldr	r3, [fp, #-68]
	cmp	r3, #0
	bne	.L351
	.loc 1 4657 0
	mov	r3, #1
	str	r3, [fp, #-328]
.L351:
	.loc 1 4663 0
	ldr	r3, [fp, #-68]
	cmp	r3, #0
	beq	.L350
	.loc 1 4665 0
	ldr	r3, [fp, #-104]
	and	r2, r3, #255
	ldr	r3, [fp, #-68]
	strb	r2, [r3, #91]
	.loc 1 4667 0
	ldr	r3, [fp, #-108]
	and	r2, r3, #255
	ldr	r3, [fp, #-68]
	strb	r2, [r3, #90]
	.loc 1 4669 0
	ldr	r2, [fp, #-68]
	ldrh	r3, [r2, #72]	@ movhi
	bic	r3, r3, #896
	orr	r3, r3, #64
	strh	r3, [r2, #72]	@ movhi
	.loc 1 4671 0
	ldr	r0, [fp, #-56]
	bl	ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow
	mov	r3, r0
	and	r3, r3, #255
	and	r3, r3, #1
	and	r2, r3, #255
	ldr	r1, [fp, #-68]
	ldrb	r3, [r1, #73]
	and	r2, r2, #1
	bic	r3, r3, #4
	mov	r2, r2, asl #2
	orr	r3, r2, r3
	strb	r3, [r1, #73]
	.loc 1 4673 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #1
	and	r2, r3, #255
	ldr	r1, [fp, #-68]
	ldrb	r3, [r1, #73]
	and	r2, r2, #1
	bic	r3, r3, #16
	mov	r2, r2, asl #4
	orr	r3, r2, r3
	strb	r3, [r1, #73]
	.loc 1 4675 0
	ldr	r2, [fp, #-436]
	ldr	r3, [fp, #-68]
	str	r2, [r3, #52]
	.loc 1 4677 0
	ldr	r0, [fp, #-56]
	bl	nm_STATION_get_cycle_minutes_10u
	mov	r3, r0
	fmsr	s14, r3	@ int
	fuitod	d6, s14
	fldd	d7, .L435+32
	fdivd	d6, d6, d7
	fldd	d7, .L435+40
	fmuld	d7, d6, d7
	ftouizd	s13, d7
	fmrs	r2, s13	@ int
	ldr	r3, [fp, #-68]
	str	r2, [r3, #56]
	.loc 1 4679 0
	ldr	r3, .L435+8
	sub	r0, fp, #20
	ldrh	r2, [r0, r3]
	ldr	r3, [fp, #-68]
	strh	r2, [r3, #88]	@ movhi
	.loc 1 4681 0
	ldr	r2, [fp, #-444]
	ldr	r3, [fp, #-68]
	str	r2, [r3, #68]
	.loc 1 4685 0
	mov	r3, #1
	str	r3, [fp, #-212]
	.loc 1 4687 0
	mov	r3, #1
	str	r3, [fp, #-204]
	.loc 1 4689 0
	mov	r3, #1
	str	r3, [fp, #-196]
	.loc 1 4695 0
	mov	r3, #1
	str	r3, [fp, #-188]
	.loc 1 4697 0
	mov	r3, #1
	str	r3, [fp, #-180]
	.loc 1 4701 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_wind
	mov	r3, r0
	str	r3, [fp, #-172]
	.loc 1 4707 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_rain
	mov	r3, r0
	cmp	r3, #0
	bne	.L352
	.loc 1 4709 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	strh	r2, [r3, #2]	@ movhi
	.loc 1 4711 0
	mov	r3, #0
	str	r3, [fp, #-164]
	.loc 1 4713 0
	mov	r3, #0
	str	r3, [fp, #-152]
	b	.L353
.L352:
	.loc 1 4717 0
	mov	r3, #1
	str	r3, [fp, #-164]
	.loc 1 4719 0
	mov	r3, #1
	str	r3, [fp, #-152]
.L353:
	.loc 1 4726 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_daily_et
	mov	r3, r0
	str	r3, [fp, #-136]
	.loc 1 4731 0
	ldr	r3, [fp, #-96]
	cmp	r3, #0
	bne	.L354
	.loc 1 4733 0
	ldr	r0, [fp, #-460]
	mov	r1, #1
	bl	BUDGET_calculate_ratios
	.loc 1 4735 0
	mov	r3, #1
	str	r3, [fp, #-96]
.L354:
	.loc 1 4739 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	bl	nm_STATION_GROUP_get_budget_start_time_flag
	mov	r3, r0
	cmp	r3, #0
	bne	.L355
.LBB16:
	.loc 1 4742 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	bl	STATION_GROUP_get_GID_irrigation_system
	str	r0, [fp, #-120]
	.loc 1 4744 0
	ldr	r3, [fp, #-448]
	ldr	r0, [fp, #-120]
	mov	r1, r3
	bl	BUDGET_handle_alerts_at_start_time
	.loc 1 4746 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	mov	r1, #1
	bl	nm_STATION_GROUP_set_budget_start_time_flag
.L355:
.LBE16:
	.loc 1 4751 0
	ldr	r2, [fp, #-448]
	sub	r3, fp, #228
	ldr	r0, [fp, #-68]
	ldr	r1, [fp, #-56]
	bl	_nm_nm_nm_add_to_the_irrigation_list
	mov	r3, r0
	cmp	r3, #1
	bne	.L356
	.loc 1 4753 0
	mov	r3, #1
	str	r3, [fp, #-76]
	.loc 1 4759 0
	ldr	r3, [fp, #-68]
	ldr	r3, [r3, #92]
	mov	r0, r3
	bl	MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor
	.loc 1 4765 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	orr	r3, r3, #16
	strb	r3, [r2, #1]
	.loc 1 4768 0
	mov	r3, #1
	str	r3, [fp, #-324]
	.loc 1 4771 0
	ldr	r3, [fp, #-156]
	cmp	r3, #1
	bne	.L357
	.loc 1 4773 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #4
	strb	r3, [r2, #2]
	.loc 1 4776 0
	mov	r3, #1
	str	r3, [fp, #-256]
.L357:
	.loc 1 4784 0
	ldr	r3, [fp, #-72]
	cmp	r3, #0
	beq	.L350
	.loc 1 4786 0
	ldr	r0, .L435+12
	ldr	r1, [fp, #-72]
	ldr	r2, .L435+16
	ldr	r3, .L435+20
	bl	nm_ListRemove_debug
	mov	r3, r0
	cmp	r3, #0
	beq	.L350
	.loc 1 4788 0
	ldr	r0, .L435+24
	bl	Alert_Message
	b	.L350
.L356:
	.loc 1 4795 0
	ldr	r3, [fp, #-144]
	cmp	r3, #1
	bne	.L358
	.loc 1 4797 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #8
	strb	r3, [r2, #3]
	.loc 1 4800 0
	mov	r3, #1
	str	r3, [fp, #-244]
.L358:
	.loc 1 4806 0
	ldr	r3, [fp, #-220]
	cmp	r3, #1
	bne	.L359
	.loc 1 4808 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	orr	r3, r3, #128
	strb	r3, [r2, #1]
	.loc 1 4811 0
	mov	r3, #1
	str	r3, [fp, #-320]
.L359:
	.loc 1 4817 0
	ldr	r3, [fp, #-216]
	cmp	r3, #1
	bne	.L360
	.loc 1 4819 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #1
	strb	r3, [r2, #2]
	.loc 1 4821 0
	mov	r3, #1
	str	r3, [fp, #-316]
.L360:
	.loc 1 4827 0
	ldr	r3, [fp, #-208]
	cmp	r3, #1
	bne	.L361
	.loc 1 4829 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #0]
	orr	r3, r3, #2
	strb	r3, [r2, #0]
	.loc 1 4831 0
	mov	r3, #1
	str	r3, [fp, #-308]
.L361:
	.loc 1 4837 0
	ldr	r3, [fp, #-200]
	cmp	r3, #1
	bne	.L362
	.loc 1 4839 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	orr	r3, r3, #32
	strb	r3, [r2, #1]
	.loc 1 4841 0
	mov	r3, #1
	str	r3, [fp, #-300]
.L362:
	.loc 1 4847 0
	ldr	r3, [fp, #-192]
	cmp	r3, #1
	bne	.L363
	.loc 1 4849 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	orr	r3, r3, #64
	strb	r3, [r2, #1]
	.loc 1 4851 0
	mov	r3, #1
	str	r3, [fp, #-292]
.L363:
	.loc 1 4857 0
	ldr	r3, [fp, #-184]
	cmp	r3, #1
	bne	.L364
	.loc 1 4859 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #16
	strb	r3, [r2, #2]
	.loc 1 4861 0
	mov	r3, #1
	str	r3, [fp, #-284]
.L364:
	.loc 1 4867 0
	ldr	r3, [fp, #-176]
	cmp	r3, #1
	bne	.L365
	.loc 1 4869 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #32
	strb	r3, [r2, #2]
	.loc 1 4871 0
	mov	r3, #1
	str	r3, [fp, #-276]
.L365:
	.loc 1 4877 0
	ldr	r3, [fp, #-168]
	cmp	r3, #1
	bne	.L366
	.loc 1 4879 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #64
	strb	r3, [r2, #2]
	.loc 1 4881 0
	mov	r3, #1
	str	r3, [fp, #-268]
.L366:
	.loc 1 4887 0
	ldr	r3, [fp, #-160]
	cmp	r3, #1
	bne	.L367
	.loc 1 4889 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #2
	strb	r3, [r2, #2]
	.loc 1 4891 0
	mov	r3, #1
	str	r3, [fp, #-260]
.L367:
	.loc 1 4895 0
	ldr	r3, [fp, #-148]
	cmp	r3, #1
	bne	.L368
	.loc 1 4897 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #8
	strb	r3, [r2, #2]
	.loc 1 4899 0
	mov	r3, #1
	str	r3, [fp, #-248]
.L368:
	.loc 1 4905 0
	ldr	r3, [fp, #-132]
	cmp	r3, #1
	bne	.L350
	.loc 1 4907 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #3]
	orr	r3, r3, #128
	strb	r3, [r2, #3]
	.loc 1 4909 0
	mov	r3, #1
	str	r3, [fp, #-232]
.L350:
	.loc 1 4926 0
	ldr	r3, [fp, #-140]
	cmp	r3, #1
	bne	.L369
	.loc 1 4928 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L435+28
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	orr	r3, r3, #2
	strb	r3, [r2, #1]
	.loc 1 4930 0
	mov	r3, #1
	str	r3, [fp, #-240]
.L369:
	.loc 1 4938 0
	ldr	r1, [fp, #-452]
	ldr	r2, [fp, #-452]
	ldr	r0, .L435+28
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrh	r2, [r3, #2]
	ldr	r0, .L435+28
	mov	r3, #56
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #2]	@ movhi
	.loc 1 4947 0
	ldr	r3, [fp, #-76]
	cmp	r3, #0
	bne	.L370
	.loc 1 4953 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_daily_et
	mov	r3, r0
	cmp	r3, #0
	beq	.L371
	.loc 1 4955 0
	ldr	r4, [fp, #-452]
	ldr	r0, [fp, #-56]
	bl	STATION_get_moisture_balance
	fmsr	s14, r0
	flds	s15, .L437
	fmuls	s14, s14, s15
	flds	s15, .L437
	fmuls	s15, s14, s15
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r0, .L437+4
	mov	r3, #72
	mov	r1, r4, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
.L371:
	.loc 1 4959 0
	ldr	r3, [fp, #-452]
	mov	r0, r3
	bl	nm_STATION_HISTORY_close_and_start_a_new_record
	b	.L370
.L343:
	.loc 1 4966 0
	mov	r3, #1
	str	r3, [fp, #-36]
	.loc 1 4971 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L437+4
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
	.loc 1 4973 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L437+4
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	strh	r2, [r3, #2]	@ movhi
.L370:
	.loc 1 4978 0
	ldr	r3, .L437+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L332:
	.loc 1 4998 0
	ldr	r0, [fp, #-56]
	bl	STATION_station_is_available_for_use
	mov	r3, r0
	cmp	r3, #0
	beq	.L330
	.loc 1 5001 0
	ldr	r0, [fp, #-56]
	bl	STATION_get_GID_manual_program_A
	str	r0, [fp, #-52]
	.loc 1 5006 0
	mov	r3, #0
	str	r3, [fp, #-44]
	b	.L372
.L393:
	.loc 1 5012 0
	mov	r3, #0
	str	r3, [fp, #-48]
	.loc 1 5013 0
	ldr	r3, [fp, #-52]
	cmp	r3, #0
	beq	.L373
	.loc 1 5016 0
	ldr	r0, [fp, #-52]
	ldr	r1, [fp, #-460]
	bl	MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID
	str	r0, [fp, #-48]
.L373:
	.loc 1 5021 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L374
	.loc 1 5024 0
	ldr	r0, [fp, #-52]
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	MANUAL_PROGRAMS_get_run_time
	str	r0, [fp, #-124]
	.loc 1 5028 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 5034 0
	ldr	r3, .L437+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L437+8
	ldr	r3, .L437+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5039 0
	mov	r3, #0
	str	r3, [fp, #-68]
	.loc 1 5042 0
	ldr	r3, [fp, #-124]
	cmp	r3, #0
	beq	.L375
	.loc 1 5042 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-428]
	cmp	r3, #0
	bne	.L375
	.loc 1 5045 0 is_stmt 1
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	str	r0, [fp, #-68]
	.loc 1 5047 0
	ldr	r3, [fp, #-68]
	cmp	r3, #0
	bne	.L375
	.loc 1 5051 0
	mov	r3, #1
	str	r3, [fp, #-428]
.L375:
	.loc 1 5057 0
	ldr	r3, [fp, #-68]
	cmp	r3, #0
	beq	.L376
	.loc 1 5061 0
	ldr	r3, [fp, #-104]
	and	r2, r3, #255
	ldr	r3, [fp, #-68]
	strb	r2, [r3, #91]
	.loc 1 5063 0
	ldr	r3, [fp, #-108]
	and	r2, r3, #255
	ldr	r3, [fp, #-68]
	strb	r2, [r3, #90]
	.loc 1 5065 0
	ldr	r2, [fp, #-68]
	ldrh	r3, [r2, #72]	@ movhi
	bic	r3, r3, #832
	orr	r3, r3, #128
	strh	r3, [r2, #72]	@ movhi
	.loc 1 5067 0
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-124]
	mul	r2, r3, r2
	ldr	r3, [fp, #-68]
	str	r2, [r3, #52]
	.loc 1 5069 0
	ldr	r3, [fp, #-68]
	ldr	r2, [fp, #-124]
	str	r2, [r3, #56]
	.loc 1 5079 0
	sub	r3, fp, #228
	mov	r0, r3
	mov	r1, #0
	mov	r2, #100
	bl	memset
	.loc 1 5081 0
	mov	r3, #1
	str	r3, [fp, #-212]
	.loc 1 5083 0
	mov	r3, #1
	str	r3, [fp, #-204]
	.loc 1 5085 0
	mov	r3, #1
	str	r3, [fp, #-196]
	.loc 1 5087 0
	mov	r3, #1
	str	r3, [fp, #-188]
	.loc 1 5089 0
	mov	r3, #1
	str	r3, [fp, #-180]
	.loc 1 5093 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_wind
	mov	r3, r0
	str	r3, [fp, #-172]
	.loc 1 5099 0
	ldr	r0, [fp, #-56]
	bl	WEATHER_get_station_uses_rain
	mov	r3, r0
	cmp	r3, #0
	bne	.L377
	.loc 1 5101 0
	ldr	r2, [fp, #-452]
	ldr	r1, .L437+4
	mov	r3, #136
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #0
	strh	r2, [r3, #2]	@ movhi
	.loc 1 5103 0
	mov	r3, #0
	str	r3, [fp, #-164]
	.loc 1 5105 0
	mov	r3, #0
	str	r3, [fp, #-152]
	b	.L378
.L377:
	.loc 1 5109 0
	mov	r3, #1
	str	r3, [fp, #-164]
	.loc 1 5111 0
	mov	r3, #1
	str	r3, [fp, #-152]
.L378:
	.loc 1 5117 0
	ldr	r3, [fp, #-96]
	cmp	r3, #0
	bne	.L379
	.loc 1 5119 0
	ldr	r0, [fp, #-460]
	mov	r1, #1
	bl	BUDGET_calculate_ratios
	.loc 1 5120 0
	mov	r3, #1
	str	r3, [fp, #-96]
.L379:
	.loc 1 5124 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	bl	nm_STATION_GROUP_get_budget_start_time_flag
	mov	r3, r0
	cmp	r3, #0
	bne	.L380
.LBB17:
	.loc 1 5127 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	bl	STATION_GROUP_get_GID_irrigation_system
	str	r0, [fp, #-128]
	.loc 1 5128 0
	ldr	r3, [fp, #-448]
	ldr	r0, [fp, #-128]
	mov	r1, r3
	bl	BUDGET_handle_alerts_at_start_time
	.loc 1 5129 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	mov	r1, #1
	bl	nm_STATION_GROUP_set_budget_start_time_flag
.L380:
.LBE17:
	.loc 1 5134 0
	sub	r3, fp, #228
	ldr	r0, [fp, #-68]
	ldr	r1, [fp, #-56]
	mov	r2, #0
	bl	_nm_nm_nm_add_to_the_irrigation_list
	mov	r3, r0
	cmp	r3, #1
	bne	.L381
	.loc 1 5137 0
	mov	r3, #1
	str	r3, [fp, #-424]
	.loc 1 5141 0
	ldr	r3, [fp, #-156]
	cmp	r3, #1
	bne	.L376
	.loc 1 5144 0
	mov	r3, #1
	str	r3, [fp, #-356]
	b	.L376
.L438:
	.align	2
.L437:
	.word	1120403456
	.word	station_preserves
	.word	.LC0
	.word	5034
	.word	list_foal_irri_recursive_MUTEX
	.word	station_info_list_hdr
	.word	.LC29
	.word	.LC30
	.word	4369
	.word	my_tick_count
	.word	station_preserves_recursive_MUTEX
	.word	list_program_data_recursive_MUTEX
.L381:
	.loc 1 5152 0
	ldr	r3, [fp, #-144]
	cmp	r3, #1
	bne	.L382
	.loc 1 5155 0
	mov	r3, #1
	str	r3, [fp, #-344]
.L382:
	.loc 1 5160 0
	ldr	r3, [fp, #-220]
	cmp	r3, #1
	bne	.L383
	.loc 1 5163 0
	mov	r3, #1
	str	r3, [fp, #-420]
.L383:
	.loc 1 5168 0
	ldr	r3, [fp, #-216]
	cmp	r3, #1
	bne	.L384
	.loc 1 5170 0
	mov	r3, #1
	str	r3, [fp, #-416]
.L384:
	.loc 1 5175 0
	ldr	r3, [fp, #-208]
	cmp	r3, #1
	bne	.L385
	.loc 1 5177 0
	mov	r3, #1
	str	r3, [fp, #-408]
.L385:
	.loc 1 5182 0
	ldr	r3, [fp, #-200]
	cmp	r3, #1
	bne	.L386
	.loc 1 5184 0
	mov	r3, #1
	str	r3, [fp, #-400]
.L386:
	.loc 1 5190 0
	ldr	r3, [fp, #-192]
	cmp	r3, #1
	bne	.L387
	.loc 1 5192 0
	mov	r3, #1
	str	r3, [fp, #-392]
.L387:
	.loc 1 5198 0
	ldr	r3, [fp, #-184]
	cmp	r3, #1
	bne	.L388
	.loc 1 5200 0
	mov	r3, #1
	str	r3, [fp, #-384]
.L388:
	.loc 1 5206 0
	ldr	r3, [fp, #-176]
	cmp	r3, #1
	bne	.L389
	.loc 1 5208 0
	mov	r3, #1
	str	r3, [fp, #-376]
.L389:
	.loc 1 5214 0
	ldr	r3, [fp, #-168]
	cmp	r3, #1
	bne	.L390
	.loc 1 5216 0
	mov	r3, #1
	str	r3, [fp, #-368]
.L390:
	.loc 1 5222 0
	ldr	r3, [fp, #-160]
	cmp	r3, #1
	bne	.L391
	.loc 1 5224 0
	mov	r3, #1
	str	r3, [fp, #-360]
.L391:
	.loc 1 5228 0
	ldr	r3, [fp, #-148]
	cmp	r3, #1
	bne	.L392
	.loc 1 5230 0
	mov	r3, #1
	str	r3, [fp, #-348]
.L392:
	.loc 1 5237 0
	mov	r3, #0
	str	r3, [fp, #-340]
.L376:
	.loc 1 5242 0
	ldr	r3, .L437+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L374:
	.loc 1 5249 0
	ldr	r0, [fp, #-56]
	bl	STATION_get_GID_manual_program_B
	str	r0, [fp, #-52]
	.loc 1 5006 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #1
	str	r3, [fp, #-44]
.L372:
	.loc 1 5006 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bls	.L393
.L330:
.LBE15:
	.loc 1 5263 0 is_stmt 1
	ldr	r0, .L437+20
	ldr	r1, [fp, #-56]
	bl	nm_ListGetNext
	str	r0, [fp, #-56]
.L329:
	.loc 1 4193 0 discriminator 1
	ldr	r3, [fp, #-56]
	cmp	r3, #0
	bne	.L394
	.loc 1 5275 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L395
	.loc 1 5284 0
	mov	r0, #1
	bl	Alert_scheduled_irrigation_started
	.loc 1 5289 0
	ldr	r3, [fp, #-60]
	cmp	r3, #0
	beq	.L395
	.loc 1 5289 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-64]
	cmp	r3, #0
	beq	.L395
	.loc 1 5291 0 is_stmt 1
	ldr	r0, [fp, #-64]
	ldr	r1, [fp, #-60]
	ldr	r2, [fp, #-460]
	bl	IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran
.L395:
	.loc 1 5295 0
	ldr	r3, [fp, #-328]
	cmp	r3, #0
	beq	.L396
	.loc 1 5297 0
	mov	r0, #1
	mov	r1, #12
	bl	Alert_some_or_all_skipping_their_start
.L396:
	.loc 1 5300 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L397
	.loc 1 5302 0
	mov	r0, #1
	mov	r1, #15
	bl	Alert_some_or_all_skipping_their_start
.L397:
	.loc 1 5305 0
	ldr	r3, [fp, #-232]
	cmp	r3, #0
	beq	.L398
	.loc 1 5307 0
	mov	r0, #1
	mov	r1, #16
	bl	Alert_some_or_all_skipping_their_start
.L398:
	.loc 1 5310 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	beq	.L399
	.loc 1 5312 0
	mov	r0, #1
	mov	r1, #11
	bl	Alert_some_or_all_skipping_their_start
.L399:
	.loc 1 5315 0
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	beq	.L400
	.loc 1 5317 0
	mov	r0, #1
	mov	r1, #13
	bl	Alert_some_or_all_skipping_their_start
.L400:
	.loc 1 5320 0
	ldr	r3, [fp, #-308]
	cmp	r3, #0
	beq	.L401
	.loc 1 5322 0
	mov	r0, #1
	mov	r1, #0
	bl	Alert_some_or_all_skipping_their_start
.L401:
	.loc 1 5325 0
	ldr	r3, [fp, #-320]
	cmp	r3, #0
	beq	.L402
	.loc 1 5327 0
	mov	r0, #1
	mov	r1, #1
	bl	Alert_some_or_all_skipping_their_start
.L402:
	.loc 1 5330 0
	ldr	r3, [fp, #-244]
	cmp	r3, #0
	beq	.L403
	.loc 1 5332 0
	mov	r0, #1
	mov	r1, #14
	bl	Alert_some_or_all_skipping_their_start
.L403:
	.loc 1 5335 0
	ldr	r3, [fp, #-300]
	cmp	r3, #0
	beq	.L404
	.loc 1 5337 0
	mov	r0, #1
	mov	r1, #2
	bl	Alert_some_or_all_skipping_their_start
.L404:
	.loc 1 5340 0
	ldr	r3, [fp, #-292]
	cmp	r3, #0
	beq	.L405
	.loc 1 5342 0
	mov	r0, #1
	mov	r1, #3
	bl	Alert_some_or_all_skipping_their_start
.L405:
	.loc 1 5345 0
	ldr	r3, [fp, #-316]
	cmp	r3, #0
	beq	.L406
	.loc 1 5347 0
	mov	r0, #1
	mov	r1, #4
	bl	Alert_some_or_all_skipping_their_start
.L406:
	.loc 1 5350 0
	ldr	r3, [fp, #-248]
	cmp	r3, #0
	beq	.L407
	.loc 1 5352 0
	mov	r0, #1
	mov	r1, #5
	bl	Alert_some_or_all_skipping_their_start
.L407:
	.loc 1 5355 0
	ldr	r3, [fp, #-260]
	cmp	r3, #0
	beq	.L408
	.loc 1 5357 0
	mov	r0, #1
	mov	r1, #6
	bl	Alert_some_or_all_skipping_their_start
.L408:
	.loc 1 5360 0
	ldr	r3, [fp, #-284]
	cmp	r3, #0
	beq	.L409
	.loc 1 5362 0
	mov	r0, #1
	mov	r1, #7
	bl	Alert_some_or_all_skipping_their_start
.L409:
	.loc 1 5365 0
	ldr	r3, [fp, #-276]
	cmp	r3, #0
	beq	.L410
	.loc 1 5367 0
	mov	r0, #1
	mov	r1, #8
	bl	Alert_some_or_all_skipping_their_start
.L410:
	.loc 1 5370 0
	ldr	r3, [fp, #-268]
	cmp	r3, #0
	beq	.L411
	.loc 1 5372 0
	mov	r0, #1
	mov	r1, #9
	bl	Alert_some_or_all_skipping_their_start
.L411:
	.loc 1 5375 0
	ldr	r3, [fp, #-240]
	cmp	r3, #0
	beq	.L412
	.loc 1 5377 0
	mov	r0, #1
	mov	r1, #10
	bl	Alert_some_or_all_skipping_their_start
.L412:
	.loc 1 5380 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L413
	.loc 1 5380 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-324]
	cmp	r3, #0
	bne	.L413
	.loc 1 5382 0 is_stmt 1
	ldr	r0, .L437+24
	bl	Alert_Message
.L413:
	.loc 1 5392 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L414
	.loc 1 5396 0
	mov	r0, #2
	bl	Alert_scheduled_irrigation_started
.L414:
	.loc 1 5399 0
	ldr	r3, [fp, #-428]
	cmp	r3, #0
	beq	.L415
	.loc 1 5401 0
	mov	r0, #2
	mov	r1, #12
	bl	Alert_some_or_all_skipping_their_start
.L415:
	.loc 1 5404 0
	ldr	r3, [fp, #-408]
	cmp	r3, #0
	beq	.L416
	.loc 1 5406 0
	mov	r0, #2
	mov	r1, #0
	bl	Alert_some_or_all_skipping_their_start
.L416:
	.loc 1 5409 0
	ldr	r3, [fp, #-420]
	cmp	r3, #0
	beq	.L417
	.loc 1 5411 0
	mov	r0, #2
	mov	r1, #1
	bl	Alert_some_or_all_skipping_their_start
.L417:
	.loc 1 5414 0
	ldr	r3, [fp, #-344]
	cmp	r3, #0
	beq	.L418
	.loc 1 5416 0
	mov	r0, #2
	mov	r1, #14
	bl	Alert_some_or_all_skipping_their_start
.L418:
	.loc 1 5419 0
	ldr	r3, [fp, #-400]
	cmp	r3, #0
	beq	.L419
	.loc 1 5421 0
	mov	r0, #2
	mov	r1, #2
	bl	Alert_some_or_all_skipping_their_start
.L419:
	.loc 1 5424 0
	ldr	r3, [fp, #-392]
	cmp	r3, #0
	beq	.L420
	.loc 1 5426 0
	mov	r0, #2
	mov	r1, #3
	bl	Alert_some_or_all_skipping_their_start
.L420:
	.loc 1 5429 0
	ldr	r3, [fp, #-416]
	cmp	r3, #0
	beq	.L421
	.loc 1 5431 0
	mov	r0, #2
	mov	r1, #4
	bl	Alert_some_or_all_skipping_their_start
.L421:
	.loc 1 5434 0
	ldr	r3, [fp, #-348]
	cmp	r3, #0
	beq	.L422
	.loc 1 5436 0
	mov	r0, #2
	mov	r1, #5
	bl	Alert_some_or_all_skipping_their_start
.L422:
	.loc 1 5439 0
	ldr	r3, [fp, #-360]
	cmp	r3, #0
	beq	.L423
	.loc 1 5441 0
	mov	r0, #2
	mov	r1, #6
	bl	Alert_some_or_all_skipping_their_start
.L423:
	.loc 1 5444 0
	ldr	r3, [fp, #-384]
	cmp	r3, #0
	beq	.L424
	.loc 1 5446 0
	mov	r0, #2
	mov	r1, #7
	bl	Alert_some_or_all_skipping_their_start
.L424:
	.loc 1 5449 0
	ldr	r3, [fp, #-376]
	cmp	r3, #0
	beq	.L425
	.loc 1 5451 0
	mov	r0, #2
	mov	r1, #8
	bl	Alert_some_or_all_skipping_their_start
.L425:
	.loc 1 5454 0
	ldr	r3, [fp, #-368]
	cmp	r3, #0
	beq	.L426
	.loc 1 5456 0
	mov	r0, #2
	mov	r1, #9
	bl	Alert_some_or_all_skipping_their_start
.L426:
	.loc 1 5459 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L427
	.loc 1 5459 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-424]
	cmp	r3, #0
	bne	.L427
	.loc 1 5461 0 is_stmt 1
	ldr	r0, .L437+28
	bl	Alert_Message
.L427:
	.loc 1 5468 0
	mov	r3, #0
	str	r3, [fp, #-80]
	b	.L428
.L430:
	.loc 1 5470 0
	ldr	r0, [fp, #-80]
	bl	STATION_GROUP_get_group_at_this_index
	mov	r3, r0
	str	r3, [fp, #-448]
	.loc 1 5472 0
	ldr	r3, [fp, #-448]
	mov	r0, r3
	bl	SCHEDULE_get_start_time
	mov	r2, r0
	ldr	r3, [fp, #-460]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L429
	.loc 1 5472 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-460]
	ldrb	r3, [r3, #19]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L429
	.loc 1 5476 0 is_stmt 1
	ldr	r3, [fp, #-448]
	mov	r0, r3
	ldr	r1, [fp, #-460]
	mov	r2, #2
	bl	SCHEDULE_does_it_irrigate_on_this_date
.L429:
	.loc 1 5468 0
	ldr	r3, [fp, #-80]
	add	r3, r3, #1
	str	r3, [fp, #-80]
.L428:
	.loc 1 5468 0 is_stmt 0 discriminator 1
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r2, r0
	ldr	r3, [fp, #-80]
	cmp	r2, r3
	bhi	.L430
	.loc 1 5484 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L431
	.loc 1 5484 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L432
.L431:
	.loc 1 5490 0 is_stmt 1
	ldr	r0, .L437+32
	bl	postBackground_Calculation_Event
	.loc 1 5494 0
	ldr	r3, .L437+36
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-100]
	rsb	r3, r3, r2
	str	r3, [fp, #-100]
.L432:
	.loc 1 5511 0
	ldr	r3, .L437+40
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5513 0
	ldr	r3, .L437+44
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5514 0
	sub	sp, fp, #20
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, fp, pc}
.LFE31:
	.size	TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start, .-TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start
	.section .rodata
	.align	2
.LC31:
	.ascii	"FOAL_IRRI: sta not found : %s, %u\000"
	.align	2
.LC32:
	.ascii	"FOAL_IRRI: ilc not available : %s, %u\000"
	.section	.text.FOAL_IRRI_initiate_a_station_test,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_initiate_a_station_test
	.type	FOAL_IRRI_initiate_a_station_test, %function
FOAL_IRRI_initiate_a_station_test:
.LFB32:
	.loc 1 5537 0
	@ args = 0, pretend = 0, frame = 116
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI96:
	add	fp, sp, #4
.LCFI97:
	sub	sp, sp, #116
.LCFI98:
	str	r0, [fp, #-116]
	str	r1, [fp, #-120]
	.loc 1 5546 0
	ldr	r3, .L443
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L443+4
	ldr	r3, .L443+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5548 0
	ldr	r3, .L443+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L443+4
	ldr	r3, .L443+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5550 0
	ldr	r3, .L443+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L443+4
	ldr	r3, .L443+24
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5556 0
	ldr	r3, [fp, #-116]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-116]
	ldr	r3, [r3, #8]
	mov	r0, r2
	mov	r1, r3
	bl	nm_STATION_get_pointer_to_station
	str	r0, [fp, #-8]
	.loc 1 5558 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L440
	.loc 1 5560 0
	ldr	r0, .L443+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L443+28
	mov	r1, r3
	ldr	r2, .L443+32
	bl	Alert_Message_va
	b	.L441
.L440:
	.loc 1 5565 0
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	str	r0, [fp, #-12]
	.loc 1 5567 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L442
	.loc 1 5569 0
	ldr	r0, .L443+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L443+36
	mov	r1, r3
	ldr	r2, .L443+40
	bl	Alert_Message_va
	b	.L441
.L442:
	.loc 1 5573 0
	ldr	r3, [fp, #-116]
	ldr	r3, [r3, #4]
	and	r2, r3, #255
	ldr	r3, [fp, #-12]
	strb	r2, [r3, #91]
	.loc 1 5575 0
	ldr	r3, [fp, #-116]
	ldr	r3, [r3, #8]
	and	r2, r3, #255
	ldr	r3, [fp, #-12]
	strb	r2, [r3, #90]
	.loc 1 5577 0
	ldr	r3, [fp, #-120]
	and	r3, r3, #255
	and	r3, r3, #15
	and	r2, r3, #255
	ldr	r1, [fp, #-12]
	ldrh	r3, [r1, #72]	@ movhi
	and	r2, r2, #15
	bic	r3, r3, #960
	mov	r2, r2, asl #6
	orr	r3, r2, r3
	strh	r3, [r1, #72]	@ movhi
	.loc 1 5579 0
	ldr	r3, [fp, #-116]
	ldr	r3, [r3, #16]
	and	r3, r3, #255
	and	r3, r3, #1
	and	r2, r3, #255
	ldr	r1, [fp, #-12]
	ldrb	r3, [r1, #73]
	and	r2, r2, #1
	bic	r3, r3, #4
	mov	r2, r2, asl #2
	orr	r3, r2, r3
	strb	r3, [r1, #73]
	.loc 1 5581 0
	ldr	r3, [fp, #-116]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #52]
	.loc 1 5584 0
	ldr	r3, [fp, #-116]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #56]
	.loc 1 5591 0
	sub	r3, fp, #112
	mov	r0, r3
	mov	r1, #0
	mov	r2, #100
	bl	memset
	.loc 1 5598 0
	sub	r3, fp, #112
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-8]
	mov	r2, #0
	bl	_nm_nm_nm_add_to_the_irrigation_list
	mov	r3, r0
	cmp	r3, #1
	bne	.L441
	.loc 1 5600 0
	ldr	r3, [fp, #-120]
	cmp	r3, #6
	bne	.L441
	.loc 1 5603 0
	ldr	r3, [fp, #-116]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-116]
	ldr	r3, [r3, #8]
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	bl	Alert_test_station_started_idx
.L441:
	.loc 1 5621 0
	ldr	r3, .L443+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5623 0
	ldr	r3, .L443+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5625 0
	ldr	r3, .L443
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5626 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L444:
	.align	2
.L443:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	5546
	.word	station_preserves_recursive_MUTEX
	.word	5548
	.word	list_foal_irri_recursive_MUTEX
	.word	5550
	.word	.LC31
	.word	5560
	.word	.LC32
	.word	5569
.LFE32:
	.size	FOAL_IRRI_initiate_a_station_test, .-FOAL_IRRI_initiate_a_station_test
	.section .rodata
	.align	2
.LC33:
	.ascii	"WALK THRU: station not found : %s, %u\000"
	.align	2
.LC34:
	.ascii	"WALK THRU: ilc not available : %s, %u\000"
	.section	.text.FOAL_IRRI_start_a_walk_thru,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_start_a_walk_thru
	.type	FOAL_IRRI_start_a_walk_thru, %function
FOAL_IRRI_start_a_walk_thru:
.LFB33:
	.loc 1 5630 0
	@ args = 0, pretend = 0, frame = 124
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI99:
	add	fp, sp, #4
.LCFI100:
	sub	sp, sp, #124
.LCFI101:
	str	r0, [fp, #-128]
	.loc 1 5647 0
	sub	r3, fp, #24
	mov	r0, #568
	mov	r1, r3
	ldr	r2, .L452
	ldr	r3, .L452+4
	bl	mem_oabia
	mov	r3, r0
	cmp	r3, #0
	beq	.L445
	.loc 1 5651 0
	ldr	r3, [fp, #-24]
	ldr	r0, [fp, #-128]
	mov	r1, r3
	bl	WALK_THRU_load_kick_off_struct
	mov	r3, r0
	cmp	r3, #0
	beq	.L445
	.loc 1 5658 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 5663 0
	ldr	r3, .L452+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L452
	ldr	r3, .L452+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5665 0
	ldr	r3, .L452+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L452
	ldr	r3, .L452+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5667 0
	ldr	r3, .L452+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L452
	ldr	r3, .L452+28
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5671 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L447
.L451:
	.loc 1 5673 0
	ldr	r3, [fp, #-24]
	ldr	r2, [fp, #-8]
	add	r2, r2, #1
	ldr	r3, [r3, r2, asl #2]
	cmn	r3, #1
	beq	.L448
	.loc 1 5675 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	ldr	r1, [fp, #-8]
	add	r1, r1, #1
	ldr	r3, [r3, r1, asl #2]
	mov	r0, r2
	mov	r1, r3
	bl	nm_STATION_get_pointer_to_station
	str	r0, [fp, #-16]
	.loc 1 5677 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L449
	.loc 1 5679 0
	ldr	r0, .L452
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L452+32
	mov	r1, r3
	ldr	r2, .L452+36
	bl	Alert_Message_va
	b	.L448
.L449:
	.loc 1 5684 0
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	str	r0, [fp, #-20]
	.loc 1 5686 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L450
	.loc 1 5688 0
	ldr	r0, .L452
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L452+40
	mov	r1, r3
	ldr	r2, .L452+44
	bl	Alert_Message_va
	b	.L448
.L450:
	.loc 1 5692 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	and	r2, r3, #255
	ldr	r3, [fp, #-20]
	strb	r2, [r3, #91]
	.loc 1 5694 0
	ldr	r3, [fp, #-24]
	ldr	r2, [fp, #-8]
	add	r2, r2, #1
	ldr	r3, [r3, r2, asl #2]
	and	r2, r3, #255
	ldr	r3, [fp, #-20]
	strb	r2, [r3, #90]
	.loc 1 5696 0
	ldr	r2, [fp, #-20]
	ldrh	r3, [r2, #72]	@ movhi
	bic	r3, r3, #640
	orr	r3, r3, #320
	strh	r3, [r2, #72]	@ movhi
	.loc 1 5698 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #516]
	ldr	r3, [fp, #-20]
	str	r2, [r3, #52]
	.loc 1 5701 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #516]
	ldr	r3, [fp, #-20]
	str	r2, [r3, #56]
	.loc 1 5708 0
	sub	r3, fp, #124
	mov	r0, r3
	mov	r1, #0
	mov	r2, #100
	bl	memset
	.loc 1 5715 0
	sub	r3, fp, #124
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-16]
	mov	r2, #0
	bl	_nm_nm_nm_add_to_the_irrigation_list
	mov	r3, r0
	cmp	r3, #0
	beq	.L448
	.loc 1 5717 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L448:
	.loc 1 5671 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L447:
	.loc 1 5671 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #127
	bls	.L451
	.loc 1 5732 0 is_stmt 1
	ldr	r3, .L452+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5734 0
	ldr	r3, .L452+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5736 0
	ldr	r3, .L452+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5742 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L445
	.loc 1 5744 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #520
	mov	r0, r3
	bl	Alert_walk_thru_started
.L445:
	.loc 1 5751 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L453:
	.align	2
.L452:
	.word	.LC0
	.word	5647
	.word	list_program_data_recursive_MUTEX
	.word	5663
	.word	station_preserves_recursive_MUTEX
	.word	5665
	.word	list_foal_irri_recursive_MUTEX
	.word	5667
	.word	.LC33
	.word	5679
	.word	.LC34
	.word	5688
.LFE33:
	.size	FOAL_IRRI_start_a_walk_thru, .-FOAL_IRRI_start_a_walk_thru
	.section	.text.FOAL_IRRI_initiate_manual_watering,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_initiate_manual_watering
	.type	FOAL_IRRI_initiate_manual_watering, %function
FOAL_IRRI_initiate_manual_watering:
.LFB34:
	.loc 1 5755 0
	@ args = 0, pretend = 0, frame = 152
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI102:
	fstmfdd	sp!, {d8}
.LCFI103:
	add	fp, sp, #16
.LCFI104:
	sub	sp, sp, #160
.LCFI105:
	str	r0, [fp, #-168]
	.loc 1 5756 0
	ldr	r3, .L476	@ float
	str	r3, [fp, #-44]	@ float
	.loc 1 5770 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 5776 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 5782 0
	sub	r3, fp, #164
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 5786 0
	ldr	r3, .L476+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L476+8
	ldr	r3, .L476+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5788 0
	ldr	r3, .L476+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L476+8
	ldr	r3, .L476+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5790 0
	ldr	r3, .L476+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L476+8
	ldr	r3, .L476+28
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5796 0
	ldr	r0, .L476+32
	bl	nm_ListGetFirst
	str	r0, [fp, #-32]
	.loc 1 5798 0
	b	.L455
.L468:
	.loc 1 5800 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 5802 0
	ldr	r0, [fp, #-32]
	bl	STATION_get_GID_station_group
	str	r0, [fp, #-36]
	.loc 1 5804 0
	ldr	r3, [fp, #-168]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	bne	.L456
	.loc 1 5807 0
	ldr	r0, [fp, #-32]
	bl	nm_STATION_get_box_index_0
	mov	r2, r0
	ldr	r3, [fp, #-168]
	ldr	r3, [r3, #8]
	cmp	r2, r3
	bne	.L457
	.loc 1 5807 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-32]
	bl	nm_STATION_get_station_number_0
	mov	r2, r0
	ldr	r3, [fp, #-168]
	ldr	r3, [r3, #12]
	cmp	r2, r3
	bne	.L457
	.loc 1 5809 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L457
.L456:
	.loc 1 5812 0
	ldr	r3, [fp, #-168]
	ldr	r3, [r3, #4]
	cmp	r3, #1
	bne	.L458
	.loc 1 5814 0
	ldr	r3, [fp, #-168]
	ldr	r2, [r3, #20]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bne	.L457
	.loc 1 5816 0
	mov	r3, #1
	str	r3, [fp, #-20]
	b	.L457
.L458:
	.loc 1 5821 0
	mov	r3, #1
	str	r3, [fp, #-20]
.L457:
	.loc 1 5827 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L459
	.loc 1 5829 0
	ldr	r0, [fp, #-32]
	bl	STATION_station_is_available_for_use
	str	r0, [fp, #-20]
.L459:
	.loc 1 5832 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L460
	.loc 1 5835 0
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	str	r0, [fp, #-40]
	.loc 1 5837 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	bne	.L461
	.loc 1 5839 0
	ldr	r0, .L476+8
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L476+36
	mov	r1, r3
	ldr	r2, .L476+40
	bl	Alert_Message_va
	b	.L462
.L461:
	.loc 1 5844 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L463
	.loc 1 5846 0
	ldr	r0, [fp, #-36]
	bl	STATION_GROUP_get_group_with_this_GID
	str	r0, [fp, #-24]
	b	.L464
.L463:
	.loc 1 5850 0
	mov	r3, #0
	str	r3, [fp, #-24]
.L464:
	.loc 1 5855 0
	ldr	r0, [fp, #-32]
	bl	nm_STATION_get_box_index_0
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, [fp, #-40]
	strb	r2, [r3, #91]
	.loc 1 5857 0
	ldr	r0, [fp, #-32]
	bl	nm_STATION_get_station_number_0
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, [fp, #-40]
	strb	r2, [r3, #90]
	.loc 1 5859 0
	ldr	r2, [fp, #-40]
	ldrh	r3, [r2, #72]	@ movhi
	bic	r3, r3, #704
	orr	r3, r3, #256
	strh	r3, [r2, #72]	@ movhi
	.loc 1 5861 0
	ldr	r3, [fp, #-168]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	bne	.L465
	.loc 1 5863 0
	ldr	r3, [fp, #-168]
	ldr	r2, [r3, #16]
	ldr	r3, [fp, #-40]
	str	r2, [r3, #52]
	b	.L466
.L465:
	.loc 1 5872 0
	ldr	r0, [fp, #-32]
	bl	nm_STATION_get_distribution_uniformity_100u
	mov	r3, r0
	.loc 1 5867 0
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	sub	r3, fp, #164
	str	r3, [sp, #0]
	fsts	s15, [sp, #4]
	mov	r0, #0
	ldr	r1, [fp, #-32]
	ldr	r2, [fp, #-24]
	mov	r3, #0
	bl	nm_WATERSENSE_calculate_run_minutes_for_this_station
	fmsr	s14, r0
	flds	s15, [fp, #-44]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	ldr	r3, [fp, #-40]
	str	r2, [r3, #52]
	.loc 1 5877 0
	ldr	r3, [fp, #-40]
	ldr	r3, [r3, #52]
	.loc 1 5874 0
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-44]
	fdivs	s16, s14, s15
	ldr	r0, [fp, #-32]
	bl	STATION_get_ET_factor_100u
	mov	r4, r0
	.loc 1 5879 0
	ldrh	r3, [fp, #-160]
	.loc 1 5874 0
	ldr	r0, [fp, #-32]
	mov	r1, r3
	bl	PERCENT_ADJUST_get_station_percentage_100u
	mov	r3, r0
	str	r4, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, #0
	ldr	r1, [fp, #-32]
	mov	r2, #0
	fmrs	r3, s16
	bl	nm_WATERSENSE_calculate_adjusted_run_time
	fmsr	s14, r0
	flds	s15, [fp, #-44]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	ldr	r3, [fp, #-40]
	str	r2, [r3, #52]
.L466:
	.loc 1 5882 0
	ldr	r0, [fp, #-32]
	bl	nm_STATION_get_cycle_minutes_10u
	mov	r2, r0
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #1
	mov	r2, r3
	ldr	r3, [fp, #-40]
	str	r2, [r3, #56]
	.loc 1 5889 0
	sub	r3, fp, #144
	mov	r0, r3
	mov	r1, #0
	mov	r2, #100
	bl	memset
	.loc 1 5897 0
	sub	r3, fp, #144
	ldr	r0, [fp, #-40]
	ldr	r1, [fp, #-32]
	mov	r2, #0
	bl	_nm_nm_nm_add_to_the_irrigation_list
	mov	r3, r0
	cmp	r3, #1
	bne	.L462
	.loc 1 5899 0
	mov	r3, #1
	str	r3, [fp, #-28]
.L462:
	.loc 1 5906 0
	ldr	r3, [fp, #-168]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L474
.L460:
	.loc 1 5914 0
	ldr	r0, .L476+32
	ldr	r1, [fp, #-32]
	bl	nm_ListGetNext
	str	r0, [fp, #-32]
.L455:
	.loc 1 5798 0 discriminator 1
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	bne	.L468
	b	.L467
.L474:
	.loc 1 5908 0
	mov	r0, r0	@ nop
.L467:
	.loc 1 5919 0
	ldr	r3, .L476+24
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5921 0
	ldr	r3, .L476+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5923 0
	ldr	r3, .L476+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5929 0
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L454
	.loc 1 5931 0
	ldr	r3, [fp, #-168]
	ldr	r3, [r3, #4]
	cmp	r3, #1
	beq	.L471
	cmp	r3, #1
	bcc	.L470
	cmp	r3, #2
	beq	.L472
	b	.L454
.L470:
	.loc 1 5934 0
	ldr	r3, [fp, #-168]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-168]
	ldr	r3, [r3, #12]
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	bl	Alert_manual_water_station_started_idx
	.loc 1 5935 0
	b	.L454
.L471:
	.loc 1 5938 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L475
	.loc 1 5940 0
	ldr	r0, [fp, #-24]
	bl	nm_GROUP_get_name
	mov	r3, r0
	mov	r0, r3
	mov	r1, #0
	bl	Alert_manual_water_program_started
	.loc 1 5942 0
	b	.L475
.L472:
	.loc 1 5945 0
	mov	r0, #0
	bl	Alert_manual_water_all_started
	.loc 1 5946 0
	mov	r0, r0	@ nop
	b	.L454
.L475:
	.loc 1 5942 0
	mov	r0, r0	@ nop
.L454:
	.loc 1 5949 0
	sub	sp, fp, #16
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, fp, pc}
.L477:
	.align	2
.L476:
	.word	1114636288
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	5786
	.word	station_preserves_recursive_MUTEX
	.word	5788
	.word	list_foal_irri_recursive_MUTEX
	.word	5790
	.word	station_info_list_hdr
	.word	.LC32
	.word	5839
.LFE34:
	.size	FOAL_IRRI_initiate_manual_watering, .-FOAL_IRRI_initiate_manual_watering
	.section .rodata
	.align	2
.LC35:
	.ascii	"MVOR: System GID should not be 0 (%d)\000"
	.section	.text.FOAL_IRRI_initiate_or_cancel_a_master_valve_override,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
	.type	FOAL_IRRI_initiate_or_cancel_a_master_valve_override, %function
FOAL_IRRI_initiate_or_cancel_a_master_valve_override:
.LFB35:
	.loc 1 5953 0
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI106:
	add	fp, sp, #4
.LCFI107:
	sub	sp, sp, #88
.LCFI108:
	str	r0, [fp, #-80]
	str	r1, [fp, #-84]
	str	r2, [fp, #-88]
	strb	r3, [fp, #-92]
	.loc 1 5962 0
	ldr	r3, .L493
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L493+4
	ldr	r3, .L493+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5966 0
	sub	r3, fp, #28
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 5968 0
	ldr	r3, [fp, #-80]
	cmp	r3, #0
	beq	.L479
	.loc 1 5970 0
	ldr	r0, [fp, #-80]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [fp, #-8]
	.loc 1 5974 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L480
	.loc 1 5978 0
	ldr	r3, .L493+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L493+4
	ldr	r3, .L493+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 5980 0
	ldr	r0, [fp, #-80]
	bl	SYSTEM_get_group_with_this_GID
	mov	r3, r0
	mov	r0, r3
	bl	nm_GROUP_get_name
	mov	r3, r0
	sub	r2, fp, #76
	mov	r0, r2
	mov	r1, r3
	mov	r2, #48
	bl	strlcpy
	.loc 1 5982 0
	ldr	r3, .L493+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 5988 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #516
	mov	r0, r3
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	mov	r3, r0
	cmp	r3, #0
	beq	.L481
	.loc 1 5988 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-84]
	cmp	r3, #2
	beq	.L481
	.loc 1 5990 0 is_stmt 1
	sub	r3, fp, #76
	mov	r0, r3
	mov	r1, #1
	bl	Alert_MVOR_skipped
	b	.L492
.L481:
	.loc 1 5993 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #16
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L483
	.loc 1 5993 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-84]
	cmp	r3, #0
	bne	.L483
	.loc 1 5999 0 is_stmt 1
	sub	r3, fp, #76
	mov	r0, r3
	mov	r1, #4
	bl	Alert_MVOR_skipped
	b	.L492
.L483:
	.loc 1 6011 0
	ldr	r3, [fp, #-84]
	cmp	r3, #0
	bne	.L484
	.loc 1 6011 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L484
	.loc 1 6014 0 is_stmt 1
	ldr	r3, [fp, #-8]
	mov	r2, #150
	str	r2, [r3, #456]
.L484:
	.loc 1 6017 0
	ldr	r3, [fp, #-84]
	cmp	r3, #1
	bne	.L485
	.loc 1 6017 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #16
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L485
	.loc 1 6020 0 is_stmt 1
	ldr	r3, [fp, #-8]
	mov	r2, #150
	str	r2, [r3, #456]
.L485:
	.loc 1 6023 0
	ldr	r3, [fp, #-84]
	cmp	r3, #2
	bne	.L486
	.loc 1 6023 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L487
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #16
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L486
.L487:
	.loc 1 6026 0 is_stmt 1
	ldr	r3, [fp, #-8]
	mov	r2, #150
	str	r2, [r3, #456]
.L486:
	.loc 1 6031 0
	ldr	r3, [fp, #-84]
	cmp	r3, #1
	bhi	.L488
	.loc 1 6036 0
	ldrb	r3, [fp, #-92]	@ zero_extendqisi2
	sub	r2, fp, #76
	mov	r0, r2
	ldr	r1, [fp, #-84]
	ldr	r2, [fp, #-88]
	bl	Alert_MVOR_started
	.loc 1 6038 0
	ldr	r3, [fp, #-84]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	and	r2, r3, #255
	ldr	r1, [fp, #-8]
	ldrb	r3, [r1, #466]
	and	r2, r2, #1
	bic	r3, r3, #8
	mov	r2, r2, asl #3
	orr	r3, r2, r3
	strb	r3, [r1, #466]
	.loc 1 6039 0
	ldr	r3, [fp, #-84]
	cmp	r3, #1
	movne	r3, #0
	moveq	r3, #1
	and	r2, r3, #255
	ldr	r1, [fp, #-8]
	ldrb	r3, [r1, #466]
	and	r2, r2, #1
	bic	r3, r3, #16
	mov	r2, r2, asl #4
	orr	r3, r2, r3
	strb	r3, [r1, #466]
	.loc 1 6041 0
	sub	r3, fp, #28
	mov	r0, r3
	ldr	r1, [fp, #-88]
	bl	TDUTILS_add_seconds_to_passed_DT_ptr
	.loc 1 6043 0
	ldrh	r3, [fp, #-24]
	mov	r1, r3
	ldr	r2, [fp, #-8]
	ldr	r3, .L493+20
	str	r1, [r2, r3]
	.loc 1 6044 0
	ldr	r1, [fp, #-28]
	ldr	r2, [fp, #-8]
	ldr	r3, .L493+24
	str	r1, [r2, r3]
	.loc 1 6046 0
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-88]
	str	r2, [r3, #444]
	.loc 1 6048 0
	mov	r0, r0	@ nop
	b	.L492
.L488:
	.loc 1 6055 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L490
	.loc 1 6055 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	and	r3, r3, #16
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L491
.L490:
	.loc 1 6057 0 is_stmt 1
	ldrb	r3, [fp, #-92]	@ zero_extendqisi2
	sub	r2, fp, #76
	mov	r0, r2
	mov	r1, #2
	ldr	r2, [fp, #-88]
	bl	Alert_MVOR_started
.L491:
	.loc 1 6060 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #466]
	bic	r3, r3, #8
	strb	r3, [r2, #466]
	.loc 1 6061 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #466]
	bic	r3, r3, #16
	strb	r3, [r2, #466]
	.loc 1 6063 0
	mov	r0, #1
	mov	r1, #1
	ldr	r2, .L493+28
	bl	DMYToDate
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r1, r3
	ldr	r2, [fp, #-8]
	ldr	r3, .L493+20
	str	r1, [r2, r3]
	.loc 1 6064 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L493+24
	mov	r1, #0
	str	r1, [r2, r3]
	.loc 1 6068 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #444]
	.loc 1 6070 0
	b	.L492
.L480:
	.loc 1 6077 0
	ldr	r0, .L493+4
	ldr	r1, .L493+32
	bl	Alert_func_call_with_null_ptr_with_filename
	b	.L492
.L479:
	.loc 1 6082 0
	ldrb	r3, [fp, #-92]	@ zero_extendqisi2
	ldr	r0, .L493+36
	mov	r1, r3
	bl	Alert_Message_va
.L492:
	.loc 1 6087 0
	ldr	r3, .L493
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 6088 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L494:
	.align	2
.L493:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	5962
	.word	list_system_recursive_MUTEX
	.word	5978
	.word	14116
	.word	14120
	.word	2011
	.word	6077
	.word	.LC35
.LFE35:
	.size	FOAL_IRRI_initiate_or_cancel_a_master_valve_override, .-FOAL_IRRI_initiate_or_cancel_a_master_valve_override
	.section .rodata
	.align	2
.LC36:
	.ascii	"Controller: %u, sta %u : not added to ON list\000"
	.section	.text._nm_nm_foal_irri_complete_the_action_needed_turn_ON,"ax",%progbits
	.align	2
	.type	_nm_nm_foal_irri_complete_the_action_needed_turn_ON, %function
_nm_nm_foal_irri_complete_the_action_needed_turn_ON:
.LFB36:
	.loc 1 6110 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI109:
	add	fp, sp, #4
.LCFI110:
	sub	sp, sp, #28
.LCFI111:
	str	r0, [fp, #-16]
	.loc 1 6111 0
	ldr	r0, .L502
	ldr	r1, [fp, #-16]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	bne	.L496
	.loc 1 6114 0
	ldr	r0, .L502+4
	ldr	r1, .L502+8
	ldr	r2, .L502+12
	ldr	r3, .L502+16
	bl	Alert_item_not_on_list_with_filename
	b	.L495
.L496:
	.loc 1 6122 0
	ldr	r2, [fp, #-16]
	ldrb	r3, [r2, #74]
	orr	r3, r3, #64
	strb	r3, [r2, #74]
	.loc 1 6130 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #91
	mov	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L502+20
	str	r2, [sp, #4]
	ldr	r2, .L502+12
	str	r2, [sp, #8]
	ldr	r2, .L502+24
	str	r2, [sp, #12]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #11
	mov	r3, #0
	bl	RC_uns8_with_filename
	.loc 1 6133 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r1, r3
	ldr	r3, .L502+28
	add	r1, r1, #20
	ldr	r3, [r3, r1, asl #2]
	add	r1, r3, #1
	ldr	r3, .L502+28
	add	r2, r2, #20
	str	r1, [r3, r2, asl #2]
	.loc 1 6139 0
	ldr	r0, .L502+32
	ldr	r1, [fp, #-16]
	bl	nm_ListInsertTail
	mov	r3, r0
	cmp	r3, #0
	beq	.L498
	.loc 1 6141 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	add	r2, r3, #65
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	add	r3, r3, #1
	ldr	r0, .L502+36
	mov	r1, r2
	mov	r2, r3
	bl	Alert_Message_va
.L498:
	.loc 1 6148 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #40]
	ldr	r2, [r3, #208]
	ldr	r3, [fp, #-16]
	ldrh	r3, [r3, #82]
	cmp	r2, r3
	bcs	.L499
	.loc 1 6150 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-16]
	ldrh	r2, [r2, #82]
	str	r2, [r3, #208]
.L499:
	.loc 1 6163 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #64
	strb	r3, [r2, #465]
	.loc 1 6165 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #464]
	bic	r3, r3, #128
	strb	r3, [r2, #464]
	.loc 1 6166 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #1
	strb	r3, [r2, #465]
	.loc 1 6167 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #2
	strb	r3, [r2, #465]
	.loc 1 6168 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #4
	strb	r3, [r2, #465]
	.loc 1 6169 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #8
	strb	r3, [r2, #465]
	.loc 1 6170 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #16
	strb	r3, [r2, #465]
	.loc 1 6171 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #465]
	bic	r3, r3, #32
	strb	r3, [r2, #465]
	.loc 1 6175 0
	ldr	r3, [fp, #-16]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L495
	.loc 1 6178 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #44]
	ldr	r1, .L502+40
	mov	r3, #68
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L500
.LBB18:
	.loc 1 6182 0
	sub	r3, fp, #12
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 6184 0
	ldr	r3, [fp, #-16]
	ldr	r1, [r3, #44]
	ldrh	r2, [fp, #-8]
	ldr	r0, .L502+40
	mov	r3, #44
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strh	r2, [r3, #2]	@ movhi
	.loc 1 6186 0
	ldr	r3, [fp, #-16]
	ldr	r1, [r3, #44]
	ldr	r2, [fp, #-12]
	ldr	r0, .L502+40
	mov	r3, #20
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
.L500:
.LBE18:
	.loc 1 6194 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #32
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L501
	.loc 1 6196 0
	ldr	r3, [fp, #-16]
	ldr	r1, [r3, #44]
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #44]
	ldr	r0, .L502+40
	mov	r3, #68
	mov	r2, r2, asl #7
	add	r2, r0, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	add	r3, r3, #1
	and	r2, r3, #255
	ldr	r0, .L502+40
	mov	r3, #68
	mov	r1, r1, asl #7
	add	r1, r0, r1
	add	r3, r1, r3
	strb	r2, [r3, #1]
.L501:
	.loc 1 6202 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L495
	.loc 1 6204 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #44]
	ldr	r1, .L502+40
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	bic	r3, r3, #16
	strb	r3, [r2, #1]
.L495:
	.loc 1 6218 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L503:
	.align	2
.L502:
	.word	foal_irri+16
	.word	.LC10
	.word	.LC11
	.word	.LC0
	.word	6114
	.word	.LC12
	.word	6130
	.word	foal_irri
	.word	foal_irri+36
	.word	.LC36
	.word	station_preserves
.LFE36:
	.size	_nm_nm_foal_irri_complete_the_action_needed_turn_ON, .-_nm_nm_foal_irri_complete_the_action_needed_turn_ON
	.section .rodata
	.align	2
.LC37:
	.ascii	"FOAL: action_needed token error. : %s, %u\000"
	.section	.text.FOAL_IRRI_buildup_action_needed_records_for_token,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_buildup_action_needed_records_for_token
	.type	FOAL_IRRI_buildup_action_needed_records_for_token, %function
FOAL_IRRI_buildup_action_needed_records_for_token:
.LFB37:
	.loc 1 6259 0
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI112:
	add	fp, sp, #4
.LCFI113:
	sub	sp, sp, #36
.LCFI114:
	str	r0, [fp, #-40]
	.loc 1 6263 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 6265 0
	ldr	r3, [fp, #-40]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 6268 0
	ldr	r3, .L512
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L512+4
	ldr	r3, .L512+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 6276 0
	ldr	r3, .L512+12
	ldr	r3, [r3, #64]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-22]	@ movhi
	.loc 1 6278 0
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	beq	.L505
.LBB19:
	.loc 1 6287 0
	ldrh	r3, [fp, #-22]
	mov	r2, r3
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, #2
	str	r3, [fp, #-12]
	.loc 1 6289 0
	ldr	r0, [fp, #-12]
	ldr	r1, .L512+4
	ldr	r2, .L512+16
	bl	mem_malloc_debug
	str	r0, [fp, #-16]
	.loc 1 6291 0
	ldr	r3, [fp, #-40]
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 6293 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 6296 0
	sub	r3, fp, #22
	ldr	r0, [fp, #-16]
	mov	r1, r3
	mov	r2, #2
	bl	memcpy
	.loc 1 6297 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #2
	str	r3, [fp, #-16]
	.loc 1 6298 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #2
	str	r3, [fp, #-12]
	.loc 1 6303 0
	ldr	r0, .L512+20
	ldr	r1, .L512+4
	ldr	r2, .L512+24
	bl	nm_ListRemoveHead_debug
	str	r0, [fp, #-20]
	.loc 1 6305 0
	b	.L506
.L510:
.LBB20:
	.loc 1 6310 0
	sub	r3, fp, #36
	mov	r0, r3
	mov	r1, #0
	mov	r2, #12
	bl	memset
	.loc 1 6312 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #36]
	and	r3, r3, #255
	strb	r3, [fp, #-33]
	.loc 1 6314 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	strb	r3, [fp, #-36]
	.loc 1 6316 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	strb	r3, [fp, #-35]
	.loc 1 6318 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	strb	r3, [fp, #-34]
	.loc 1 6323 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #52]
	str	r3, [fp, #-28]
	.loc 1 6325 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #36]
	cmp	r3, #1
	beq	.L507
	.loc 1 6326 0 discriminator 1
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #36]
	.loc 1 6325 0 discriminator 1
	cmp	r3, #2
	bne	.L508
.L507:
	.loc 1 6331 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #48]
	str	r3, [fp, #-32]
	b	.L509
.L508:
	.loc 1 6337 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #60]
	str	r3, [fp, #-32]
.L509:
	.loc 1 6343 0
	sub	r3, fp, #36
	ldr	r0, [fp, #-16]
	mov	r1, r3
	mov	r2, #12
	bl	memcpy
	.loc 1 6344 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #12
	str	r3, [fp, #-16]
	.loc 1 6345 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #12
	str	r3, [fp, #-12]
	.loc 1 6346 0
	ldrh	r3, [fp, #-22]
	sub	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-22]	@ movhi
	.loc 1 6351 0
	ldr	r0, .L512+20
	ldr	r1, .L512+4
	ldr	r2, .L512+28
	bl	nm_ListRemoveHead_debug
	str	r0, [fp, #-20]
.L506:
.LBE20:
	.loc 1 6305 0 discriminator 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L510
	.loc 1 6356 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L511
	.loc 1 6356 0 is_stmt 0 discriminator 1
	ldrh	r3, [fp, #-22]
	cmp	r3, #0
	beq	.L505
.L511:
	.loc 1 6358 0 is_stmt 1
	ldr	r0, .L512+4
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L512+32
	mov	r1, r3
	ldr	r2, .L512+36
	bl	Alert_Message_va
.L505:
.LBE19:
	.loc 1 6363 0
	ldr	r3, .L512
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 6365 0
	ldr	r3, [fp, #-8]
	.loc 1 6366 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L513:
	.align	2
.L512:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	6268
	.word	foal_irri
	.word	6289
	.word	foal_irri+56
	.word	6303
	.word	6351
	.word	.LC37
	.word	6358
.LFE37:
	.size	FOAL_IRRI_buildup_action_needed_records_for_token, .-FOAL_IRRI_buildup_action_needed_records_for_token
	.section	.text.set_remaining_seconds_on,"ax",%progbits
	.align	2
	.type	set_remaining_seconds_on, %function
set_remaining_seconds_on:
.LFB38:
	.loc 1 6383 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI115:
	add	fp, sp, #0
.LCFI116:
	sub	sp, sp, #4
.LCFI117:
	str	r0, [fp, #-4]
	.loc 1 6386 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #48]
	.loc 1 6392 0
	ldr	r3, [fp, #-4]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L515
	.loc 1 6394 0
	ldr	r3, [fp, #-4]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L516
	.loc 1 6396 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #52]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6398 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #52]
	b	.L514
.L516:
	.loc 1 6401 0
	ldr	r3, [fp, #-4]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	bne	.L518
	.loc 1 6403 0
	ldr	r3, [fp, #-4]
	mov	r2, #360
	str	r2, [r3, #48]
	.loc 1 6407 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #52]
	b	.L514
.L518:
	.loc 1 6421 0
	ldr	r3, [fp, #-4]
	mov	r2, #360
	str	r2, [r3, #48]
	b	.L514
.L515:
	.loc 1 6428 0
	ldr	r3, [fp, #-4]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	sub	r3, r3, #1
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L514
.L525:
	.word	.L519
	.word	.L520
	.word	.L514
	.word	.L521
	.word	.L522
	.word	.L523
	.word	.L524
.L524:
	.loc 1 6433 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #52]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6435 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #52]
	.loc 1 6437 0
	b	.L514
.L523:
	.loc 1 6443 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #52]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6445 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #52]
	.loc 1 6447 0
	b	.L514
.L522:
	.loc 1 6453 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #52]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6455 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #52]
	.loc 1 6457 0
	b	.L514
.L521:
	.loc 1 6463 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #56]
	cmp	r2, r3
	bls	.L526
	.loc 1 6465 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #56]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6467 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #56]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-4]
	str	r2, [r3, #52]
	.loc 1 6475 0
	b	.L514
.L526:
	.loc 1 6471 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #52]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6473 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #52]
	.loc 1 6475 0
	b	.L514
.L520:
	.loc 1 6483 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #56]
	cmp	r2, r3
	bls	.L528
	.loc 1 6485 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #56]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6487 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #56]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-4]
	str	r2, [r3, #52]
	.loc 1 6495 0
	b	.L514
.L528:
	.loc 1 6491 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #52]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6493 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #52]
	.loc 1 6495 0
	b	.L514
.L519:
	.loc 1 6499 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #56]
	cmp	r2, r3
	bls	.L530
	.loc 1 6501 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #56]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6503 0
	ldr	r3, [fp, #-4]
	ldr	r2, [r3, #52]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #56]
	rsb	r2, r3, r2
	ldr	r3, [fp, #-4]
	str	r2, [r3, #52]
	.loc 1 6511 0
	b	.L532
.L530:
	.loc 1 6507 0
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #52]
	mov	r2, r3
	ldr	r3, [fp, #-4]
	str	r2, [r3, #48]
	.loc 1 6509 0
	ldr	r3, [fp, #-4]
	mov	r2, #0
	str	r2, [r3, #52]
.L532:
	.loc 1 6511 0
	mov	r0, r0	@ nop
.L514:
	.loc 1 6517 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE38:
	.size	set_remaining_seconds_on, .-set_remaining_seconds_on
	.section .rodata
	.align	2
.LC38:
	.ascii	"Pump Capacity Override: Station %s expected is high"
	.ascii	"er.\000"
	.align	2
.LC39:
	.ascii	"Non-Pump Capacity Override: Station %s expected is "
	.ascii	"higher.\000"
	.section	.text.will_exceed_system_capacity,"ax",%progbits
	.align	2
	.type	will_exceed_system_capacity, %function
will_exceed_system_capacity:
.LFB39:
	.loc 1 6549 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI118:
	add	fp, sp, #4
.LCFI119:
	sub	sp, sp, #24
.LCFI120:
	str	r0, [fp, #-28]
	.loc 1 6559 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 6561 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SYSTEM_get_capacity_in_use_bool
	mov	r3, r0
	cmp	r3, #1
	bne	.L534
.LBB21:
	.loc 1 6570 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #172]
	cmp	r3, #1
	beq	.L535
	.loc 1 6570 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-28]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L536
.L535:
	.loc 1 6572 0 is_stmt 1
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SYSTEM_get_capacity_with_pump_gpm
	str	r0, [fp, #-16]
	.loc 1 6574 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L537
.L536:
	.loc 1 6578 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	SYSTEM_get_capacity_without_pump_gpm
	str	r0, [fp, #-16]
	.loc 1 6580 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L537:
	.loc 1 6583 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #92]
	cmp	r3, #0
	bne	.L538
	.loc 1 6587 0
	ldr	r3, [fp, #-28]
	ldrh	r3, [r3, #80]
	mov	r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bls	.L538
	.loc 1 6589 0
	ldr	r3, [fp, #-28]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r1, r3
	ldr	r3, [fp, #-28]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r2, r3
	sub	r3, fp, #24
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #8
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	.loc 1 6592 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L539
	.loc 1 6594 0
	sub	r3, fp, #24
	ldr	r0, .L541
	mov	r1, r3
	bl	Alert_Message_va
	b	.L540
.L539:
	.loc 1 6598 0
	sub	r3, fp, #24
	ldr	r0, .L541+4
	mov	r1, r3
	bl	Alert_Message_va
.L540:
	.loc 1 6603 0
	ldr	r3, [fp, #-28]
	ldrh	r3, [r3, #80]
	str	r3, [fp, #-16]
.L538:
	.loc 1 6607 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #40]
	ldr	r2, [r3, #100]
	ldr	r3, [fp, #-28]
	ldrh	r3, [r3, #80]
	add	r2, r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bls	.L534
	.loc 1 6609 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L534:
.LBE21:
	.loc 1 6613 0
	ldr	r3, [fp, #-8]
	.loc 1 6614 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L542:
	.align	2
.L541:
	.word	.LC38
	.word	.LC39
.LFE39:
	.size	will_exceed_system_capacity, .-will_exceed_system_capacity
	.section	.text.FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	.type	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list, %function
FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list:
.LFB40:
	.loc 1 6618 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI121:
	add	fp, sp, #0
.LCFI122:
	sub	sp, sp, #12
.LCFI123:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 6624 0
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	beq	.L544
	.loc 1 6624 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L545
.L544:
	.loc 1 6626 0 is_stmt 1
	ldr	r3, [fp, #-12]
	cmp	r3, #2
	movls	r3, #0
	movhi	r3, #1
	str	r3, [fp, #-4]
	b	.L546
.L545:
	.loc 1 6630 0
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	movls	r3, #0
	movhi	r3, #1
	str	r3, [fp, #-4]
.L546:
	.loc 1 6633 0
	ldr	r3, [fp, #-4]
	.loc 1 6634 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE40:
	.size	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list, .-FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	.section	.text.FOAL_IRRI_there_are_other_flow_check_groups_already_ON,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_there_are_other_flow_check_groups_already_ON
	.type	FOAL_IRRI_there_are_other_flow_check_groups_already_ON, %function
FOAL_IRRI_there_are_other_flow_check_groups_already_ON:
.LFB41:
	.loc 1 6638 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI124:
	add	fp, sp, #0
.LCFI125:
	sub	sp, sp, #16
.LCFI126:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 6641 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 6645 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L548
.L551:
	.loc 1 6647 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #3
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	beq	.L549
	.loc 1 6649 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L549
	.loc 1 6651 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 6654 0
	b	.L550
.L549:
	.loc 1 6658 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #4
	str	r3, [fp, #-16]
	.loc 1 6645 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L548:
	.loc 1 6645 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bls	.L551
.L550:
	.loc 1 6661 0 is_stmt 1
	ldr	r3, [fp, #-4]
	.loc 1 6662 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE41:
	.size	FOAL_IRRI_there_are_other_flow_check_groups_already_ON, .-FOAL_IRRI_there_are_other_flow_check_groups_already_ON
	.section	.text.FOAL_IRRI_there_is_more_than_one_flow_group_ON,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_there_is_more_than_one_flow_group_ON
	.type	FOAL_IRRI_there_is_more_than_one_flow_group_ON, %function
FOAL_IRRI_there_is_more_than_one_flow_group_ON:
.LFB42:
	.loc 1 6666 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI127:
	add	fp, sp, #0
.LCFI128:
	sub	sp, sp, #16
.LCFI129:
	str	r0, [fp, #-16]
	.loc 1 6669 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 6673 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 6675 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L553
.L555:
	.loc 1 6677 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L554
	.loc 1 6679 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L554:
	.loc 1 6682 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #4
	str	r3, [fp, #-16]
	.loc 1 6675 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L553:
	.loc 1 6675 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bls	.L555
	.loc 1 6685 0 is_stmt 1
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bls	.L556
	.loc 1 6687 0
	mov	r3, #1
	str	r3, [fp, #-4]
.L556:
	.loc 1 6690 0
	ldr	r3, [fp, #-4]
	.loc 1 6691 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE42:
	.size	FOAL_IRRI_there_is_more_than_one_flow_group_ON, .-FOAL_IRRI_there_is_more_than_one_flow_group_ON
	.section .rodata
	.align	2
.LC40:
	.ascii	"Already on action needed list: for %u, changing to "
	.ascii	"%u, count=%u\000"
	.section	.text.nm_FOAL_add_to_action_needed_list,"ax",%progbits
	.align	2
	.global	nm_FOAL_add_to_action_needed_list
	.type	nm_FOAL_add_to_action_needed_list, %function
nm_FOAL_add_to_action_needed_list:
.LFB43:
	.loc 1 6715 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI130:
	add	fp, sp, #4
.LCFI131:
	sub	sp, sp, #12
.LCFI132:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 6718 0
	ldr	r0, .L560
	ldr	r1, [fp, #-12]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	bne	.L558
	.loc 1 6721 0
	ldr	r0, .L560
	ldr	r1, [fp, #-12]
	bl	nm_ListInsertTail
	b	.L559
.L558:
	.loc 1 6742 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #36]
	cmp	r3, #5
	beq	.L559
	.loc 1 6745 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #36]
	mov	r2, r3
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	beq	.L559
	.loc 1 6747 0
	ldr	r3, .L560+4
	ldr	r3, [r3, #64]
	str	r3, [fp, #-8]
	.loc 1 6749 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #36]
	ldr	r0, .L560+8
	mov	r1, r3
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-8]
	bl	Alert_Message_va
.L559:
	.loc 1 6758 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-12]
	strh	r2, [r3, #36]	@ movhi
	.loc 1 6759 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L561:
	.align	2
.L560:
	.word	foal_irri+56
	.word	foal_irri
	.word	.LC40
.LFE43:
	.size	nm_FOAL_add_to_action_needed_list, .-nm_FOAL_add_to_action_needed_list
	.section	.text._if_supposed_to_but_did_not_ever_check_flow_make_an_alert,"ax",%progbits
	.align	2
	.type	_if_supposed_to_but_did_not_ever_check_flow_make_an_alert, %function
_if_supposed_to_but_did_not_ever_check_flow_make_an_alert:
.LFB44:
	.loc 1 6763 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI133:
	add	fp, sp, #4
.LCFI134:
	sub	sp, sp, #4
.LCFI135:
	str	r0, [fp, #-8]
	.loc 1 6765 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L562
	.loc 1 6767 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L562
	.loc 1 6769 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	and	r3, r3, #16
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L562
	.loc 1 6771 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L564
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #1]
	orr	r3, r3, #16
	strb	r3, [r2, #1]
	.loc 1 6773 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	bl	Alert_flow_not_checked_with_no_reasons_idx
.L562:
	.loc 1 6777 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L565:
	.align	2
.L564:
	.word	station_preserves
.LFE44:
	.size	_if_supposed_to_but_did_not_ever_check_flow_make_an_alert, .-_if_supposed_to_but_did_not_ever_check_flow_make_an_alert
	.section	.text.foal_irri_maintenance_may_run,"ax",%progbits
	.align	2
	.type	foal_irri_maintenance_may_run, %function
foal_irri_maintenance_may_run:
.LFB45:
	.loc 1 6781 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI136:
	add	fp, sp, #0
.LCFI137:
	sub	sp, sp, #4
.LCFI138:
	.loc 1 6786 0
	mov	r3, #1
	str	r3, [fp, #-4]
	.loc 1 6792 0
	ldr	r3, .L568
	ldr	r3, [r3, #64]
	cmp	r3, #0
	beq	.L567
	.loc 1 6794 0
	mov	r3, #0
	str	r3, [fp, #-4]
.L567:
	.loc 1 6799 0
	ldr	r3, [fp, #-4]
	.loc 1 6800 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L569:
	.align	2
.L568:
	.word	foal_irri
.LFE45:
	.size	foal_irri_maintenance_may_run, .-foal_irri_maintenance_may_run
	.section	.text.decrement_system_preserves_timers,"ax",%progbits
	.align	2
	.type	decrement_system_preserves_timers, %function
decrement_system_preserves_timers:
.LFB46:
	.loc 1 6823 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI139:
	add	fp, sp, #4
.LCFI140:
	sub	sp, sp, #8
.LCFI141:
	.loc 1 6828 0
	ldr	r3, .L578
	str	r3, [fp, #-12]
	.loc 1 6830 0
	ldr	r3, .L578+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L578+8
	ldr	r3, .L578+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 6832 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L571
.L577:
	.loc 1 6835 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L572
	.loc 1 6837 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #456]
	cmp	r3, #0
	beq	.L573
	.loc 1 6839 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #456]
	sub	r2, r3, #1
	ldr	r3, [fp, #-12]
	str	r2, [r3, #456]
.L573:
	.loc 1 6842 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #460]
	cmp	r3, #0
	beq	.L574
	.loc 1 6844 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #460]
	sub	r2, r3, #1
	ldr	r3, [fp, #-12]
	str	r2, [r3, #460]
.L574:
	.loc 1 6847 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #208]
	cmp	r3, #0
	beq	.L575
	.loc 1 6849 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #208]
	sub	r2, r3, #1
	ldr	r3, [fp, #-12]
	str	r2, [r3, #208]
.L575:
	.loc 1 6852 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #212]
	cmp	r3, #0
	beq	.L576
	.loc 1 6854 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #212]
	sub	r2, r3, #1
	ldr	r3, [fp, #-12]
	str	r2, [r3, #212]
.L576:
	.loc 1 6860 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #444]
	cmp	r3, #0
	beq	.L572
	.loc 1 6862 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #444]
	sub	r2, r3, #1
	ldr	r3, [fp, #-12]
	str	r2, [r3, #444]
.L572:
	.loc 1 6869 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #14208
	add	r3, r3, #16
	str	r3, [fp, #-12]
	.loc 1 6832 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L571:
	.loc 1 6832 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bls	.L577
	.loc 1 6874 0 is_stmt 1
	ldr	r3, .L578+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 6875 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L579:
	.align	2
.L578:
	.word	system_preserves+16
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	6830
.LFE46:
	.size	decrement_system_preserves_timers, .-decrement_system_preserves_timers
	.section .rodata
	.align	2
.LC41:
	.ascii	"FOAL_IRRI: unexp condition : %s, %u\000"
	.align	2
.LC42:
	.ascii	"FOAL_IRRI: unexp pump setting! : %s, %u\000"
	.section	.text._nm_anti_chatter_maintenance_sub_function,"ax",%progbits
	.align	2
	.type	_nm_anti_chatter_maintenance_sub_function, %function
_nm_anti_chatter_maintenance_sub_function:
.LFB47:
	.loc 1 6904 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI142:
	add	fp, sp, #4
.LCFI143:
	sub	sp, sp, #4
.LCFI144:
	str	r0, [fp, #-8]
	.loc 1 6913 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L580
	.loc 1 6915 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #92]
	cmp	r3, #0
	beq	.L582
	.loc 1 6921 0
	ldr	r3, [fp, #-8]
	mov	r2, #3
	str	r2, [r3, #448]
	.loc 1 6930 0
	ldr	r3, [fp, #-8]
	mov	r2, #150
	str	r2, [r3, #460]
	.loc 1 6934 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	and	r3, r3, #32
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L583
	.loc 1 6936 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	and	r3, r3, #16
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L584
	.loc 1 6941 0
	ldr	r3, [fp, #-8]
	mov	r2, #150
	str	r2, [r3, #456]
.L584:
	.loc 1 6944 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #464]
	orr	r3, r3, #32
	strb	r3, [r2, #464]
.L583:
	.loc 1 6949 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #172]
	cmp	r3, #1
	bne	.L585
	.loc 1 6952 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #464]
	orr	r3, r3, #64
	strb	r3, [r2, #464]
	.loc 1 6955 0
	ldr	r3, [fp, #-8]
	mov	r2, #3
	str	r2, [r3, #452]
	b	.L586
.L585:
	.loc 1 6959 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #452]
	cmp	r3, #0
	beq	.L587
	.loc 1 6961 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #452]
	sub	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #452]
.L587:
	.loc 1 6964 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #452]
	cmp	r3, #0
	bne	.L586
	.loc 1 6967 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #464]
	bic	r3, r3, #64
	strb	r3, [r2, #464]
	b	.L586
.L582:
	.loc 1 6980 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #212]
	cmp	r3, #0
	bne	.L588
	.loc 1 6982 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #448]
	cmp	r3, #0
	beq	.L589
	.loc 1 6986 0
	ldr	r3, [fp, #-8]
	mov	r2, #150
	str	r2, [r3, #460]
	.loc 1 6988 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #448]
	sub	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #448]
	.loc 1 6990 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #448]
	cmp	r3, #0
	bne	.L589
	.loc 1 6993 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #464]
	bic	r3, r3, #32
	strb	r3, [r2, #464]
.L589:
	.loc 1 6997 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #452]
	cmp	r3, #0
	beq	.L590
	.loc 1 6999 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #452]
	sub	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #452]
	.loc 1 7001 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #452]
	cmp	r3, #0
	bne	.L590
	.loc 1 7004 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #464]
	bic	r3, r3, #64
	strb	r3, [r2, #464]
	b	.L590
.L588:
	.loc 1 7012 0
	ldr	r3, [fp, #-8]
	mov	r2, #150
	str	r2, [r3, #460]
.L590:
	.loc 1 7016 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #448]
	cmp	r3, #0
	bne	.L591
	.loc 1 7016 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	and	r3, r3, #32
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L591
	.loc 1 7018 0 is_stmt 1
	ldr	r0, .L592
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L592+4
	mov	r1, r3
	ldr	r2, .L592+8
	bl	Alert_Message_va
	.loc 1 7021 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #464]
	bic	r3, r3, #32
	strb	r3, [r2, #464]
.L591:
	.loc 1 7025 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #452]
	cmp	r3, #0
	bne	.L586
	.loc 1 7025 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L586
	.loc 1 7027 0 is_stmt 1
	ldr	r0, .L592
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L592+4
	mov	r1, r3
	ldr	r2, .L592+12
	bl	Alert_Message_va
	.loc 1 7030 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #464]
	bic	r3, r3, #64
	strb	r3, [r2, #464]
.L586:
	.loc 1 7037 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L580
	.loc 1 7039 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	and	r3, r3, #32
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L580
	.loc 1 7041 0
	ldr	r0, .L592
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L592+16
	mov	r1, r3
	ldr	r2, .L592+20
	bl	Alert_Message_va
	.loc 1 7043 0
	ldr	r2, [fp, #-8]
	ldrb	r3, [r2, #464]
	bic	r3, r3, #64
	strb	r3, [r2, #464]
.L580:
	.loc 1 7048 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L593:
	.align	2
.L592:
	.word	.LC0
	.word	.LC41
	.word	7018
	.word	7027
	.word	.LC42
	.word	7041
.LFE47:
	.size	_nm_anti_chatter_maintenance_sub_function, .-_nm_anti_chatter_maintenance_sub_function
	.section	.text._load_and_maintain_system_anti_chatter_timers,"ax",%progbits
	.align	2
	.type	_load_and_maintain_system_anti_chatter_timers, %function
_load_and_maintain_system_anti_chatter_timers:
.LFB48:
	.loc 1 7051 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI145:
	add	fp, sp, #4
.LCFI146:
	sub	sp, sp, #8
.LCFI147:
	.loc 1 7056 0
	ldr	r3, .L597
	str	r3, [fp, #-12]
	.loc 1 7058 0
	ldr	r3, .L597+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L597+8
	ldr	r3, .L597+12
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 7060 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L595
.L596:
	.loc 1 7062 0 discriminator 2
	ldr	r0, [fp, #-12]
	bl	_nm_anti_chatter_maintenance_sub_function
	.loc 1 7066 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #14208
	add	r3, r3, #16
	str	r3, [fp, #-12]
	.loc 1 7060 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L595:
	.loc 1 7060 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bls	.L596
	.loc 1 7071 0 is_stmt 1
	ldr	r3, .L597+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 7072 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L598:
	.align	2
.L597:
	.word	system_preserves+16
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	7058
.LFE48:
	.size	_load_and_maintain_system_anti_chatter_timers, .-_load_and_maintain_system_anti_chatter_timers
	.global	one_ON_for_walk_thru
	.section	.bss.one_ON_for_walk_thru,"aw",%nobits
	.align	2
	.type	one_ON_for_walk_thru, %object
	.size	one_ON_for_walk_thru, 4
one_ON_for_walk_thru:
	.space	4
	.section .rodata
	.align	2
.LC43:
	.ascii	"ilc\000"
	.align	2
.LC44:
	.ascii	"foal_irri.list_of_foal_stations_ON\000"
	.align	2
.LC45:
	.ascii	"Is OFF but on the ON list! : %s, %u\000"
	.section	.text.irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations,"ax",%progbits
	.align	2
	.global	irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations
	.type	irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations, %function
irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations:
.LFB49:
	.loc 1 7083 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI148:
	add	fp, sp, #4
.LCFI149:
	sub	sp, sp, #60
.LCFI150:
	str	r0, [fp, #-48]
	.loc 1 7109 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 7110 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 7111 0
	mov	r3, #0
	str	r3, [fp, #-28]
	.loc 1 7112 0
	mov	r3, #0
	str	r3, [fp, #-32]
	.loc 1 7116 0
	ldr	r0, .L646
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 7121 0
	b	.L600
.L645:
.LBB22:
	.loc 1 7134 0
	mov	r3, #1
	str	r3, [fp, #-36]
	.loc 1 7140 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #91
	mov	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L646+4
	str	r2, [sp, #4]
	ldr	r2, .L646+8
	str	r2, [sp, #8]
	ldr	r2, .L646+12
	str	r2, [sp, #12]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #11
	mov	r3, #0
	bl	RC_uns8_with_filename
	.loc 1 7145 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L601
	.loc 1 7147 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #88]
	strh	r3, [fp, #-40]	@ movhi
	.loc 1 7148 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #68]
	str	r3, [fp, #-44]
	.loc 1 7150 0
	sub	r3, fp, #44
	ldr	r0, [fp, #-48]
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L601
	.loc 1 7154 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L602
	.loc 1 7156 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 7158 0
	bl	Alert_hit_stop_time
.L602:
	.loc 1 7165 0
	ldr	r0, .L646
	ldr	r1, [fp, #-8]
	mov	r2, #28
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-8]
	.loc 1 7171 0
	mov	r3, #0
	str	r3, [fp, #-36]
.L601:
	.loc 1 7177 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #76]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L603
	.loc 1 7182 0
	ldr	r3, .L646+16
	ldr	r3, [r3, #60]
	cmp	r3, #0
	bne	.L604
	.loc 1 7182 0 is_stmt 0 discriminator 1
	ldr	r3, .L646+16
	ldr	r3, [r3, #56]
	cmp	r3, #0
	beq	.L603
.L604:
	.loc 1 7184 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L605
	.loc 1 7186 0
	mov	r3, #1
	str	r3, [fp, #-24]
	.loc 1 7188 0
	bl	Alert_rain_affecting_irrigation
.L605:
	.loc 1 7195 0
	ldr	r0, .L646
	ldr	r1, [fp, #-8]
	mov	r2, #29
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-8]
	.loc 1 7197 0
	mov	r3, #0
	str	r3, [fp, #-36]
.L603:
	.loc 1 7203 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #76]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L606
	.loc 1 7205 0
	ldr	r3, .L646+16
	ldr	r3, [r3, #100]
	cmp	r3, #1
	bne	.L606
	.loc 1 7207 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L607
	.loc 1 7209 0
	mov	r3, #1
	str	r3, [fp, #-28]
	.loc 1 7211 0
	bl	Alert_rain_switch_affecting_irrigation
.L607:
	.loc 1 7218 0
	ldr	r0, .L646
	ldr	r1, [fp, #-8]
	mov	r2, #30
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-8]
	.loc 1 7220 0
	mov	r3, #0
	str	r3, [fp, #-36]
.L606:
	.loc 1 7226 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #76]	@ zero_extendqisi2
	bic	r3, r3, #127
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L608
	.loc 1 7228 0
	ldr	r3, .L646+16
	ldr	r3, [r3, #104]
	cmp	r3, #1
	bne	.L608
	.loc 1 7230 0
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	bne	.L609
	.loc 1 7232 0
	mov	r3, #1
	str	r3, [fp, #-32]
	.loc 1 7234 0
	bl	Alert_freeze_switch_affecting_irrigation
.L609:
	.loc 1 7241 0
	ldr	r0, .L646
	ldr	r1, [fp, #-8]
	mov	r2, #31
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-8]
	.loc 1 7243 0
	mov	r3, #0
	str	r3, [fp, #-36]
.L608:
	.loc 1 7254 0
	ldr	r3, [fp, #-36]
	cmp	r3, #1
	bne	.L610
	.loc 1 7266 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L611
	.loc 1 7268 0
	ldr	r0, [fp, #-8]
	bl	MOISTURE_SENSORS_update__transient__control_variables_for_this_station
.L611:
	.loc 1 7275 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #40]
	ldr	r2, [r2, #84]
	add	r2, r2, #1
	str	r2, [r3, #84]
	.loc 1 7279 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #80]
	cmp	r2, r3
	bls	.L612
	.loc 1 7281 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #72]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	and	r2, r2, #255
	str	r2, [r3, #80]
.L612:
	.loc 1 7287 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L613
	.loc 1 7289 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #467]
	orr	r3, r3, #16
	strb	r3, [r2, #467]
	b	.L614
.L613:
	.loc 1 7292 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #128
	bne	.L615
	.loc 1 7294 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #467]
	orr	r3, r3, #32
	strb	r3, [r2, #467]
	b	.L614
.L615:
	.loc 1 7297 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #256
	bne	.L616
	.loc 1 7299 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #467]
	orr	r3, r3, #128
	strb	r3, [r2, #467]
	b	.L614
.L616:
	.loc 1 7302 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #320
	bne	.L617
	.loc 1 7304 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #468]
	orr	r3, r3, #1
	strb	r3, [r2, #468]
	b	.L614
.L617:
	.loc 1 7307 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	bne	.L618
	.loc 1 7309 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #468]
	orr	r3, r3, #2
	strb	r3, [r2, #468]
	b	.L614
.L618:
	.loc 1 7312 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L614
	.loc 1 7314 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #40]
	ldrb	r3, [r2, #468]
	orr	r3, r3, #4
	strb	r3, [r2, #468]
.L614:
	.loc 1 7328 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	bne	.L619
	.loc 1 7333 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #180]
	cmp	r2, r3
	bls	.L620
	.loc 1 7336 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #72]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	and	r2, r2, #255
	str	r2, [r3, #180]
.L620:
	.loc 1 7340 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L621
	.loc 1 7340 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #184]
	cmp	r2, r3
	bls	.L621
	.loc 1 7342 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #72]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	and	r2, r2, #255
	str	r2, [r3, #184]
.L621:
	.loc 1 7345 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L619
	.loc 1 7345 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #188]
	cmp	r2, r3
	bls	.L619
	.loc 1 7347 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #72]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	and	r2, r2, #255
	str	r2, [r3, #188]
.L619:
	.loc 1 7353 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #74]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L622
	.loc 1 7363 0
	ldr	r0, .L646+20
	ldr	r1, [fp, #-8]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #0
	bne	.L623
	.loc 1 7366 0
	ldr	r0, .L646+24
	ldr	r1, .L646+28
	ldr	r2, .L646+8
	ldr	r3, .L646+32
	bl	Alert_item_not_on_list_with_filename
.L623:
	.loc 1 7370 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #48]
	sub	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #48]
	.loc 1 7375 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #48]
	cmp	r3, #0
	bgt	.L624
	.loc 1 7380 0
	ldr	r0, .L646
	ldr	r1, [fp, #-8]
	mov	r2, #5
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	str	r0, [fp, #-8]
	.loc 1 7382 0
	mov	r3, #0
	str	r3, [fp, #-36]
	b	.L610
.L624:
	.loc 1 7388 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L625
	.loc 1 7390 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #160]
.L625:
	.loc 1 7393 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #44]
	ldr	r1, .L646+36
	mov	r3, #140
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #0]
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r1, [r3, #40]
	ldr	r3, .L646+40
	ldr	r3, [r1, r3]
	cmp	r2, r3
	bcs	.L626
	.loc 1 7395 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #0
	str	r2, [r3, #164]
.L626:
	.loc 1 7402 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	beq	.L627
	.loc 1 7402 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	beq	.L627
	.loc 1 7406 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #40]
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #86]
	mov	r0, r2
	mov	r1, r3
	bl	SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit
	.loc 1 7409 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #86]
	mov	r0, r3
	bl	ON_AT_A_TIME_bump_this_groups_ON_count
.L627:
	.loc 1 7414 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #320
	bne	.L628
	.loc 1 7419 0
	ldr	r3, .L646+44
	mov	r2, #1
	str	r2, [r3, #0]
.L628:
	.loc 1 7428 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #32
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L629
	.loc 1 7431 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #104]
.L629:
	.loc 1 7435 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L630
	.loc 1 7437 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #172]
	b	.L631
.L630:
	.loc 1 7441 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #176]
.L631:
	.loc 1 7445 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L610
	.loc 1 7448 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L632
	.loc 1 7450 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #140]
	b	.L610
.L632:
	.loc 1 7455 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #132]
	.loc 1 7466 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L610
	.loc 1 7468 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #86]
	mov	r0, r3
	bl	ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID
	b	.L610
.L622:
	.loc 1 7484 0
	ldr	r0, .L646+20
	ldr	r1, [fp, #-8]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #1
	bne	.L633
	.loc 1 7486 0
	ldr	r0, .L646+8
	bl	RemovePathFromFileName
	mov	r3, r0
	ldr	r0, .L646+48
	mov	r1, r3
	ldr	r2, .L646+52
	bl	Alert_Message_va
.L633:
	.loc 1 7490 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	beq	.L634
	.loc 1 7492 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	sub	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #60]
.L634:
	.loc 1 7500 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 7502 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #52]
	cmp	r3, #0
	bne	.L635
	.loc 1 7504 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 7506 0
	mov	r3, #20
	str	r3, [fp, #-16]
	b	.L636
.L635:
	.loc 1 7511 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L636
	.loc 1 7513 0
	ldr	r0, [fp, #-8]
	bl	MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list
	mov	r3, r0
	cmp	r3, #0
	beq	.L636
	.loc 1 7515 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 7517 0
	mov	r3, #33
	str	r3, [fp, #-16]
.L636:
	.loc 1 7523 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L637
	.loc 1 7529 0
	ldr	r0, [fp, #-8]
	bl	_if_supposed_to_but_did_not_ever_check_flow_make_an_alert
	.loc 1 7534 0
	ldr	r0, .L646
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-16]
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-8]
	.loc 1 7536 0
	mov	r3, #0
	str	r3, [fp, #-36]
	b	.L610
.L637:
	.loc 1 7545 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L638
	.loc 1 7548 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L639
	.loc 1 7550 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #144]
	b	.L640
.L639:
	.loc 1 7554 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #136]
	.loc 1 7556 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L641
	.loc 1 7558 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #148]
	b	.L640
.L641:
	.loc 1 7562 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #152]
.L640:
	.loc 1 7568 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #128]
	cmp	r2, r3
	bls	.L638
	.loc 1 7570 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldrh	r2, [r2, #72]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	and	r2, r2, #255
	str	r2, [r3, #128]
.L638:
	.loc 1 7575 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L642
	.loc 1 7579 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	bne	.L642
	.loc 1 7579 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #52]
	cmp	r3, #0
	beq	.L642
	.loc 1 7583 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #72]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #3
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #108]
	cmp	r2, r3
	bls	.L643
	.loc 1 7583 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L643
	.loc 1 7585 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldrb	r2, [r2, #72]	@ zero_extendqisi2
	mov	r2, r2, lsr #4
	and	r2, r2, #3
	and	r2, r2, #255
	str	r2, [r3, #108]
.L643:
	.loc 1 7588 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #72]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #3
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #112]
	cmp	r2, r3
	bls	.L642
	.loc 1 7588 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L642
	.loc 1 7590 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldrb	r2, [r2, #72]	@ zero_extendqisi2
	mov	r2, r2, lsr #4
	and	r2, r2, #3
	and	r2, r2, #255
	str	r2, [r3, #112]
.L642:
	.loc 1 7624 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	bne	.L610
	.loc 1 7624 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #52]
	cmp	r3, #0
	beq	.L610
	.loc 1 7626 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L644
	.loc 1 7628 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #120]
	b	.L610
.L644:
	.loc 1 7632 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #124]
.L610:
	.loc 1 7646 0
	ldr	r3, [fp, #-36]
	cmp	r3, #1
	bne	.L600
	.loc 1 7648 0
	ldr	r0, .L646
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L600:
.LBE22:
	.loc 1 7121 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L645
	.loc 1 7652 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L647:
	.align	2
.L646:
	.word	foal_irri+16
	.word	.LC12
	.word	.LC0
	.word	7140
	.word	weather_preserves
	.word	foal_irri+36
	.word	.LC43
	.word	.LC44
	.word	7366
	.word	station_preserves
	.word	14032
	.word	one_ON_for_walk_thru
	.word	.LC45
	.word	7486
.LFE49:
	.size	irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations, .-irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations
	.section .rodata
	.align	2
.LC46:
	.ascii	"FOAL_IRRI: expected flow rate should be zero.\000"
	.align	2
.LC47:
	.ascii	"FOAL_IRRI: number ON during TEST should be zero.\000"
	.align	2
.LC48:
	.ascii	"FOAL_IRRI: mix of flow groups ON.\000"
	.section	.text.irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars,"ax",%progbits
	.align	2
	.global	irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars
	.type	irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars, %function
irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars:
.LFB50:
	.loc 1 7656 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI151:
	add	fp, sp, #4
.LCFI152:
	sub	sp, sp, #4
.LCFI153:
	.loc 1 7663 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L649
.L679:
	.loc 1 7665 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L650
	.loc 1 7670 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #108
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L651
	.loc 1 7672 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #116
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L652
	.loc 1 7674 0
	ldr	r0, .L680
	bl	Alert_Message
	.loc 1 7676 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #116
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
.L652:
	.loc 1 7679 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #184
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L651
	.loc 1 7681 0
	ldr	r0, .L680+4
	bl	Alert_Message
	.loc 1 7683 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #184
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
.L651:
	.loc 1 7691 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #152
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L653
	.loc 1 7693 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #128
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 7695 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #124
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
.L653:
	.loc 1 7704 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #100
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L654
	.loc 1 7706 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L655
.L654:
	.loc 1 7713 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #124
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r0, #128
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	cmp	r1, r3
	bls	.L656
	.loc 1 7714 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #196
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	.loc 1 7713 0 discriminator 1
	cmp	r3, #1
	bne	.L656
	.loc 1 7716 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L655
.L656:
	.loc 1 7718 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #128
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r0, #124
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	cmp	r1, r3
	bls	.L657
	.loc 1 7719 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #196
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	.loc 1 7718 0 discriminator 1
	cmp	r3, #1
	bne	.L657
	.loc 1 7721 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L655
.L657:
	.loc 1 7725 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #188
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L658
	.loc 1 7727 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L655
.L658:
	.loc 1 7730 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #192
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L659
	.loc 1 7732 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L655
.L659:
	.loc 1 7739 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #196
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bls	.L660
	.loc 1 7741 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #200
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r0, #204
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	cmp	r1, r3
	bls	.L661
	.loc 1 7743 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L655
.L661:
	.loc 1 7745 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #200
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r0, #204
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	cmp	r1, r3
	bcs	.L655
	.loc 1 7747 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L655
.L660:
	.loc 1 7766 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #152
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L662
	.loc 1 7768 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L663
	.loc 1 7769 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #164
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	.loc 1 7768 0 discriminator 1
	cmp	r3, #1
	bne	.L663
	.loc 1 7771 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L655
.L663:
	.loc 1 7773 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L665
	.loc 1 7774 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #168
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	.loc 1 7773 0 discriminator 1
	cmp	r3, #1
	bne	.L665
	.loc 1 7776 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L655
.L665:
	.loc 1 7780 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L666
	.loc 1 7782 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 7786 0
	b	.L655
.L666:
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L655
.L662:
	.loc 1 7794 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L667
	.loc 1 7795 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #136
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	.loc 1 7794 0 discriminator 1
	cmp	r3, #1
	bne	.L667
	.loc 1 7797 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L655
.L667:
	.loc 1 7799 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L668
	.loc 1 7800 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #140
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	.loc 1 7799 0 discriminator 1
	cmp	r3, #1
	bne	.L668
	.loc 1 7802 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L655
.L668:
	.loc 1 7806 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L669
	.loc 1 7808 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L655
.L669:
	.loc 1 7812 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #112
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
.L655:
	.loc 1 7832 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #176
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L670
	.loc 1 7832 0 is_stmt 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #184
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bhi	.L670
	.loc 1 7834 0 is_stmt 1
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r2, r2, #208
	ldr	r3, .L680+12
	add	r3, r2, r3
	mov	r0, r3
	bl	FOAL_IRRI_there_is_more_than_one_flow_group_ON
	mov	r3, r0
	cmp	r3, #0
	beq	.L670
	.loc 1 7836 0
	ldr	r0, .L680+8
	bl	Alert_Message
.L670:
	.loc 1 7849 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #192
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L671
	.loc 1 7849 0 is_stmt 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #188
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L671
	.loc 1 7851 0 is_stmt 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #172
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L672
.L681:
	.align	2
.L680:
	.word	.LC46
	.word	.LC47
	.word	.LC48
	.word	system_preserves
.L671:
	.loc 1 7855 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #172
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
.L672:
	.loc 1 7860 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r3, r3, lsr #5
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L673
	.loc 1 7860 0 is_stmt 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L673
	.loc 1 7862 0 is_stmt 1
	mov	r0, #1
	bl	Alert_irrigation_ended
.L673:
	.loc 1 7865 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r3, r3, lsr #6
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L674
	.loc 1 7865 0 is_stmt 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #5
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L674
	.loc 1 7867 0 is_stmt 1
	mov	r0, #2
	bl	Alert_irrigation_ended
.L674:
	.loc 1 7870 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L675
	.loc 1 7870 0 is_stmt 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #7
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L675
	.loc 1 7872 0 is_stmt 1
	mov	r0, #4
	bl	Alert_irrigation_ended
.L675:
	.loc 1 7875 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #1
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L676
	.loc 1 7875 0 is_stmt 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #484
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #0]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L676
	.loc 1 7877 0 is_stmt 1
	mov	r0, #5
	bl	Alert_irrigation_ended
.L676:
	.loc 1 7880 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L677
	.loc 1 7880 0 is_stmt 0 discriminator 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #484
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r3, r3, lsr #1
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L677
	.loc 1 7882 0 is_stmt 1
	mov	r0, #6
	bl	Alert_irrigation_ended
.L677:
	.loc 1 7885 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #3
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L678
	.loc 1 7885 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
.L678:
	.loc 1 7895 0 is_stmt 1
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #1
	and	r0, r3, #255
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r1, r3, r1
	ldrb	r3, [r1, #2]
	and	r2, r0, #1
	bic	r3, r3, #32
	mov	r2, r2, asl #5
	orr	r3, r2, r3
	strb	r3, [r1, #2]
	.loc 1 7896 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #5
	and	r3, r3, #1
	and	r0, r3, #255
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r1, r3, r1
	ldrb	r3, [r1, #2]
	and	r2, r0, #1
	bic	r3, r3, #64
	mov	r2, r2, asl #6
	orr	r3, r2, r3
	strb	r3, [r1, #2]
	.loc 1 7897 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #7
	and	r1, r3, #255
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r0, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r2, r3, r0
	ldrb	r3, [r2, #3]
	and	r1, r1, #1
	bic	r3, r3, #1
	orr	r3, r1, r3
	strb	r3, [r2, #3]
	.loc 1 7898 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #484
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #0]
	and	r3, r3, #1
	and	r0, r3, #255
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r1, r3, r1
	ldrb	r3, [r1, #3]
	and	r2, r0, #1
	bic	r3, r3, #2
	mov	r2, r2, asl #1
	orr	r3, r2, r3
	strb	r3, [r1, #3]
	.loc 1 7899 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #484
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r3, r3, lsr #1
	and	r3, r3, #1
	and	r0, r3, #255
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r1, r3, r1
	ldrb	r3, [r1, #3]
	and	r2, r0, #1
	bic	r3, r3, #4
	mov	r2, r2, asl #2
	orr	r3, r2, r3
	strb	r3, [r1, #3]
	.loc 1 7900 0
	ldr	r0, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #484
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #1
	and	r0, r3, #255
	ldr	ip, .L680+12
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, ip, r2
	add	r1, r3, r1
	ldrb	r3, [r1, #3]
	and	r2, r0, #1
	bic	r3, r3, #8
	mov	r2, r2, asl #3
	orr	r3, r2, r3
	strb	r3, [r1, #3]
.L650:
	.loc 1 7663 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L649:
	.loc 1 7663 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	bls	.L679
	.loc 1 7907 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE50:
	.size	irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars, .-irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars
	.section	.text.irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON,"ax",%progbits
	.align	2
	.global	irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON
	.type	irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON, %function
irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON:
.LFB51:
	.loc 1 7911 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI154:
	add	fp, sp, #4
.LCFI155:
	sub	sp, sp, #16
.LCFI156:
	.loc 1 7925 0
	ldr	r0, .L689
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 1 7927 0
	b	.L683
.L688:
.LBB23:
	.loc 1 7931 0
	mov	r3, #1
	str	r3, [fp, #-12]
	.loc 1 7936 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #74]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L684
.LBB24:
	.loc 1 7942 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 7950 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #180]
	mov	r0, r2
	mov	r1, r3
	bl	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	mov	r3, r0
	cmp	r3, #1
	bne	.L685
	.loc 1 7964 0
	mov	r3, #12
	str	r3, [fp, #-20]
	.loc 1 7966 0
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L686
.L685:
	.loc 1 7969 0
	ldr	r3, [fp, #-8]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L687
	.loc 1 7981 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #144]
	cmp	r3, #1
	bne	.L686
	.loc 1 7983 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L686
	.loc 1 7989 0
	mov	r3, #12
	str	r3, [fp, #-20]
	.loc 1 7991 0
	mov	r3, #1
	str	r3, [fp, #-16]
	b	.L686
.L687:
	.loc 1 7996 0
	ldr	r3, [fp, #-8]
	ldrb	r3, [r3, #77]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L686
	.loc 1 7996 0 is_stmt 0 discriminator 1
	ldr	r3, .L689+4
	ldr	r3, [r3, #136]
	cmp	r3, #0
	beq	.L686
	.loc 1 8001 0 is_stmt 1
	mov	r3, #14
	str	r3, [fp, #-20]
	.loc 1 8003 0
	mov	r3, #1
	str	r3, [fp, #-16]
.L686:
	.loc 1 8008 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L684
	.loc 1 8011 0
	ldr	r0, .L689
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-20]
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	str	r0, [fp, #-8]
	.loc 1 8013 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L684:
.LBE24:
	.loc 1 8021 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L683
	.loc 1 8023 0
	ldr	r0, .L689
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L683:
.LBE23:
	.loc 1 7927 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L688
	.loc 1 8027 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L690:
	.align	2
.L689:
	.word	foal_irri+16
	.word	foal_irri
.LFE51:
	.size	irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON, .-irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON
	.section .rodata
	.align	2
.LC49:
	.ascii	"Station %s Skipped: (0 time)\000"
	.section	.text.irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON,"ax",%progbits
	.align	2
	.global	irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON
	.type	irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON, %function
irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON:
.LFB52:
	.loc 1 8031 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI157:
	add	fp, sp, #8
.LCFI158:
	sub	sp, sp, #32
.LCFI159:
	.loc 1 8047 0
	bl	MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables
	.loc 1 8053 0
	bl	MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold
	.loc 1 8076 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 8080 0
	ldr	r0, .L774
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	b	.L747
.L773:
	.loc 1 8681 0
	mov	r0, r0	@ nop
.L747:
.LBB25:
	.loc 1 8093 0
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L692
	.loc 1 8095 0
	ldr	r0, .L774
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L692:
	.loc 1 8098 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 8101 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L749
.L693:
	.loc 1 8106 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #74]	@ zero_extendqisi2
	and	r3, r3, #64
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L750
.L695:
	.loc 1 8109 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #60]
	cmp	r3, #0
	bne	.L751
.L697:
	.loc 1 8117 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	beq	.L698
	.loc 1 8117 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	beq	.L698
	.loc 1 8121 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #212]
	cmp	r3, #0
	bne	.L752
.L698:
	.loc 1 8126 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L699
	.loc 1 8128 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L753
.L699:
	.loc 1 8152 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #180]
	mov	r0, r2
	mov	r1, r3
	bl	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	mov	r3, r0
	cmp	r3, #1
	bne	.L700
	.loc 1 8162 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	beq	.L701
	.loc 1 8162 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	bne	.L754
.L701:
	.loc 1 8164 0 is_stmt 1
	ldr	r0, .L774
	ldr	r1, [fp, #-12]
	mov	r2, #21
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-12]
	.loc 1 8166 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 8171 0
	b	.L754
.L700:
	.loc 1 8175 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #32
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L703
	.loc 1 8175 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #104]
	cmp	r3, #1
	beq	.L755
.L703:
	.loc 1 8184 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L704
	.loc 1 8184 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	beq	.L704
	.loc 1 8186 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r2, r3, #72
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	add	r3, r3, #192
	mov	r0, r2
	mov	r1, r3
	bl	FOAL_IRRI_there_are_other_flow_check_groups_already_ON
	mov	r3, r0
	cmp	r3, #0
	bne	.L756
.L704:
	.loc 1 8198 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	beq	.L705
	.loc 1 8198 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	beq	.L705
	.loc 1 8200 0 is_stmt 1
	ldr	r0, [fp, #-12]
	bl	will_exceed_system_capacity
	mov	r3, r0
	cmp	r3, #1
	beq	.L757
.L705:
	.loc 1 8209 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	beq	.L706
	.loc 1 8209 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	beq	.L706
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #320
	beq	.L706
	.loc 1 8213 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #156]
	cmp	r3, #1
	beq	.L758
.L707:
	.loc 1 8216 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L708
	.loc 1 8222 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #96]
	cmp	r3, #0
	beq	.L759
.L709:
	.loc 1 8228 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #92]
	cmp	r3, #0
	beq	.L706
	.loc 1 8228 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #172]
	cmp	r3, #0
	bne	.L706
	.loc 1 8228 0
	b	.L696
.L708:
	.loc 1 8232 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #96]
	cmp	r3, #1
	beq	.L760
.L710:
	.loc 1 8236 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #172]
	cmp	r3, #1
	beq	.L761
.L706:
	.loc 1 8243 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L774+4
	add	r2, r2, #20
	ldr	r4, [r3, r2, asl #2]
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r0, r3
	bl	NETWORK_CONFIG_get_electrical_limit
	mov	r3, r0
	cmp	r4, r3
	bcc	.L711
	.loc 1 8247 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	beq	.L712
	.loc 1 8247 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	bne	.L762
.L712:
	.loc 1 8249 0 is_stmt 1
	ldr	r0, .L774
	ldr	r1, [fp, #-12]
	mov	r2, #22
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-12]
	.loc 1 8251 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 8255 0
	b	.L762
.L711:
	.loc 1 8260 0
	ldr	r3, .L774+4
	ldr	r3, [r3, #136]
	cmp	r3, #0
	beq	.L714
	.loc 1 8264 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #74]	@ zero_extendqisi2
	and	r3, r3, #16
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L714
	.loc 1 8268 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L763
	.loc 1 8270 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r1, .L774+8
	mov	r3, #36
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r2, r2, r3
	ldrb	r3, [r2, #2]
	orr	r3, r3, #64
	strb	r3, [r2, #2]
	.loc 1 8273 0
	b	.L763
.L714:
	.loc 1 8287 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	beq	.L716
	.loc 1 8287 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	beq	.L716
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #320
	beq	.L716
	.loc 1 8292 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #40]
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #86]
	mov	r0, r2
	mov	r1, r3
	bl	SYSTEM_PRESERVES_is_this_valve_allowed_ON
	mov	r3, r0
	cmp	r3, #0
	beq	.L764
.L717:
	.loc 1 8301 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #86]
	cmp	r3, #0
	beq	.L765
	.loc 1 8304 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #86]
	mov	r0, r3
	bl	ON_AT_A_TIME_can_another_valve_come_ON_within_this_group
	mov	r3, r0
	cmp	r3, #0
	bne	.L765
	.loc 1 8306 0
	b	.L696
.L716:
	.loc 1 8311 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #320
	bne	.L719
	.loc 1 8338 0
	ldr	r3, .L774+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L766
	b	.L719
.L765:
	.loc 1 8301 0
	mov	r0, r0	@ nop
.L719:
	.loc 1 8355 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #132]
	cmp	r3, #1
	beq	.L720
	.loc 1 8355 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #140]
	cmp	r3, #1
	bne	.L721
.L720:
	.loc 1 8361 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	bne	.L767
	.loc 1 8363 0
	ldr	r0, .L774
	ldr	r1, [fp, #-12]
	mov	r2, #23
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	str	r0, [fp, #-12]
	.loc 1 8365 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 8369 0
	b	.L767
.L721:
	.loc 1 8376 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #136]
	cmp	r3, #1
	beq	.L723
	.loc 1 8376 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #144]
	cmp	r3, #1
	bne	.L724
.L723:
	.loc 1 8378 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #180]
	mov	r0, r2
	mov	r1, r3
	bl	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	mov	r3, r0
	cmp	r3, #1
	beq	.L768
.L725:
	.loc 1 8391 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r2, [r3, #128]
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	and	r3, r3, #255
	cmp	r2, r3
	bne	.L726
	.loc 1 8393 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L769
.L726:
	.loc 1 8411 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #92]
	cmp	r3, #0
	bne	.L770
.L724:
	.loc 1 8426 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 8431 0
	ldr	r0, .L774+16
	bl	nm_ListGetFirst
	str	r0, [fp, #-16]
	.loc 1 8433 0
	b	.L727
.L729:
	.loc 1 8435 0
	ldr	r3, [fp, #-16]
	ldrb	r2, [r3, #91]	@ zero_extendqisi2
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L728
	.loc 1 8435 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldrb	r2, [r3, #90]	@ zero_extendqisi2
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L728
	.loc 1 8437 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-24]
.L728:
	.loc 1 8440 0
	ldr	r0, .L774+16
	ldr	r1, [fp, #-16]
	bl	nm_ListGetNext
	str	r0, [fp, #-16]
.L727:
	.loc 1 8433 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L729
	.loc 1 8443 0
	ldr	r3, [fp, #-24]
	cmp	r3, #1
	beq	.L771
.L730:
	.loc 1 8448 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L731
	.loc 1 8450 0
	ldr	r0, [fp, #-12]
	bl	MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles
	mov	r3, r0
	cmp	r3, #0
	bne	.L772
.L731:
	.loc 1 8459 0
	ldr	r0, [fp, #-12]
	bl	set_remaining_seconds_on
	.loc 1 8461 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #48]
	cmp	r3, #0
	ble	.L732
.LBB26:
	.loc 1 8476 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #92]
	cmp	r3, #0
	beq	.L733
	.loc 1 8478 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r3, [r3, #172]
	cmp	r3, #0
	bne	.L734
	.loc 1 8480 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L733
	.loc 1 8484 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #156]
	b	.L733
.L734:
	.loc 1 8489 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L733
	.loc 1 8493 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #156]
.L733:
	.loc 1 8506 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #64
	bne	.L735
	.loc 1 8509 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #44]
	ldr	r1, .L774+8
	mov	r3, #68
	mov	r2, r2, asl #7
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L736
	.loc 1 8511 0
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L737
.L736:
	.loc 1 8515 0
	mov	r3, #2
	str	r3, [fp, #-28]
	b	.L737
.L735:
	.loc 1 8519 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #256
	bne	.L738
	.loc 1 8524 0
	mov	r3, #2
	str	r3, [fp, #-28]
	b	.L737
.L738:
	.loc 1 8528 0
	mov	r3, #1
	str	r3, [fp, #-28]
.L737:
	.loc 1 8540 0
	ldr	r0, [fp, #-12]
	bl	_nm_nm_foal_irri_complete_the_action_needed_turn_ON
	.loc 1 8547 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-28]
	bl	nm_FOAL_add_to_action_needed_list
	.loc 1 8552 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #40]
	ldr	r1, [r2, #100]
	ldr	r2, [fp, #-12]
	ldrh	r2, [r2, #80]
	add	r2, r1, r2
	str	r2, [r3, #100]
	.loc 1 8555 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-12]
	ldrb	r2, [r2, #74]	@ zero_extendqisi2
	mov	r2, r2, lsr #2
	and	r2, r2, #3
	and	r2, r2, #255
	ldr	r1, [fp, #-12]
	ldr	r1, [r1, #40]
	ldr	r0, [fp, #-12]
	ldrb	r0, [r0, #74]	@ zero_extendqisi2
	mov	r0, r0, lsr #2
	and	r0, r0, #3
	and	r0, r0, #255
	add	r0, r0, #48
	ldr	r1, [r1, r0, asl #2]
	add	r1, r1, #1
	add	r2, r2, #48
	str	r1, [r3, r2, asl #2]
	.loc 1 8559 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #40]
	ldr	r2, [r2, #92]
	add	r2, r2, #1
	str	r2, [r3, #92]
	.loc 1 8565 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	beq	.L739
	.loc 1 8565 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	beq	.L739
	.loc 1 8569 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #40]
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #86]
	mov	r0, r2
	mov	r1, r3
	bl	SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit
	.loc 1 8573 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #86]
	mov	r0, r3
	bl	ON_AT_A_TIME_bump_this_groups_ON_count
.L739:
	.loc 1 8578 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #320
	bne	.L740
	.loc 1 8582 0
	ldr	r3, .L774+12
	mov	r2, #1
	str	r2, [r3, #0]
.L740:
	.loc 1 8587 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #384
	bne	.L741
	.loc 1 8589 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-12]
	ldr	r2, [r2, #40]
	ldr	r2, [r2, #168]
	add	r2, r2, #1
	str	r2, [r3, #168]
.L741:
	.loc 1 8595 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #32
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L742
	.loc 1 8597 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #104]
.L742:
	.loc 1 8603 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #8
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L743
	.loc 1 8605 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #172]
	b	.L744
.L743:
	.loc 1 8609 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #176]
.L744:
	.loc 1 8615 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #73]	@ zero_extendqisi2
	and	r3, r3, #4
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L745
	.loc 1 8617 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L746
	.loc 1 8620 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #140]
	b	.L745
.L746:
	.loc 1 8626 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #132]
.L745:
	.loc 1 8634 0
	ldr	r3, [fp, #-12]
	ldrh	r3, [r3, #72]
	and	r3, r3, #960
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #448
	bne	.L773
	.loc 1 8637 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r0, r2
	mov	r1, r3
	bl	Alert_mobile_station_on
	.loc 1 8639 0
	ldr	r0, .L774+20
	mov	r1, #0
	mov	r2, #512
	mov	r3, #0
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.LBE26:
.LBE25:
	.loc 1 8681 0
	b	.L773
.L732:
.LBB27:
	.loc 1 8661 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #91]	@ zero_extendqisi2
	mov	r1, r3
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #90]	@ zero_extendqisi2
	mov	r2, r3
	sub	r3, fp, #40
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #8
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	.loc 1 8663 0
	sub	r3, fp, #40
	ldr	r0, .L774+24
	mov	r1, r3
	bl	Alert_Message_va
	.loc 1 8667 0
	ldr	r0, [fp, #-12]
	mov	r1, #24
	bl	nm_FOAL_add_to_action_needed_list
	.loc 1 8669 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-32]
	.loc 1 8671 0
	ldr	r0, .L774
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
	.loc 1 8673 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 8675 0
	ldr	r0, .L774
	ldr	r1, [fp, #-32]
	ldr	r2, .L774+28
	ldr	r3, .L774+32
	bl	nm_ListRemove_debug
	.loc 1 8677 0
	b	.L696
.L750:
	.loc 1 8106 0
	mov	r0, r0	@ nop
	b	.L696
.L751:
	.loc 1 8109 0
	mov	r0, r0	@ nop
	b	.L696
.L752:
	.loc 1 8121 0
	mov	r0, r0	@ nop
	b	.L696
.L753:
	.loc 1 8128 0
	mov	r0, r0	@ nop
	b	.L696
.L754:
	.loc 1 8171 0
	mov	r0, r0	@ nop
	b	.L696
.L755:
	.loc 1 8177 0
	mov	r0, r0	@ nop
	b	.L696
.L756:
	.loc 1 8189 0
	mov	r0, r0	@ nop
	b	.L696
.L757:
	.loc 1 8200 0
	mov	r0, r0	@ nop
	b	.L696
.L758:
	.loc 1 8213 0
	mov	r0, r0	@ nop
	b	.L696
.L759:
	.loc 1 8222 0
	mov	r0, r0	@ nop
	b	.L696
.L760:
	.loc 1 8232 0
	mov	r0, r0	@ nop
	b	.L696
.L761:
	.loc 1 8236 0
	mov	r0, r0	@ nop
	b	.L696
.L762:
	.loc 1 8255 0
	mov	r0, r0	@ nop
	b	.L696
.L763:
	.loc 1 8273 0
	mov	r0, r0	@ nop
	b	.L696
.L764:
	.loc 1 8294 0
	mov	r0, r0	@ nop
	b	.L696
.L766:
	.loc 1 8340 0
	mov	r0, r0	@ nop
	b	.L696
.L767:
	.loc 1 8369 0
	mov	r0, r0	@ nop
	b	.L696
.L768:
	.loc 1 8384 0
	mov	r0, r0	@ nop
	b	.L696
.L769:
	.loc 1 8396 0
	mov	r0, r0	@ nop
	b	.L696
.L770:
	.loc 1 8414 0
	mov	r0, r0	@ nop
	b	.L696
.L771:
	.loc 1 8443 0
	mov	r0, r0	@ nop
	b	.L696
.L772:
	.loc 1 8453 0
	mov	r0, r0	@ nop
.L696:
.LBE27:
	.loc 1 8681 0
	b	.L773
.L749:
.LBB28:
	.loc 1 8101 0
	mov	r0, r0	@ nop
.L748:
.LBE28:
	.loc 1 8682 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L775:
	.align	2
.L774:
	.word	foal_irri+16
	.word	foal_irri
	.word	station_preserves
	.word	one_ON_for_walk_thru
	.word	foal_irri+36
	.word	413
	.word	.LC49
	.word	.LC0
	.word	8675
.LFE52:
	.size	irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON, .-irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON
	.section	.text.FOAL_IRRI_maintain_irrigation_list,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_maintain_irrigation_list
	.type	FOAL_IRRI_maintain_irrigation_list, %function
FOAL_IRRI_maintain_irrigation_list:
.LFB53:
	.loc 1 8699 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI160:
	add	fp, sp, #4
.LCFI161:
	sub	sp, sp, #16
.LCFI162:
	str	r0, [fp, #-20]
	.loc 1 8710 0
	bl	decrement_system_preserves_timers
	.loc 1 8715 0
	ldr	r3, .L783
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L783+4
	ldr	r3, .L783+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 8720 0
	ldr	r3, .L783+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L783+4
	ldr	r3, .L783+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 8735 0
	ldr	r3, .L783+20
	mov	r2, #0
	str	r2, [r3, #76]
	.loc 1 8750 0
	bl	foal_irri_maintenance_may_run
	mov	r3, r0
	cmp	r3, #0
	beq	.L777
	.loc 1 8754 0
	bl	SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems
	.loc 1 8756 0
	bl	ON_AT_A_TIME_init_all_groups_ON_count
	.loc 1 8760 0
	bl	MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors
	.loc 1 8764 0
	ldr	r3, .L783+24
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 8795 0
	ldr	r0, [fp, #-20]
	bl	irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations
	.loc 1 8800 0
	bl	foal_irri_maintenance_may_run
	mov	r3, r0
	cmp	r3, #0
	beq	.L778
	.loc 1 8802 0
	bl	irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars
	.loc 1 8804 0
	bl	irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON
.L778:
	.loc 1 8815 0
	bl	foal_irri_maintenance_may_run
	mov	r3, r0
	cmp	r3, #0
	beq	.L777
	.loc 1 8817 0
	bl	irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON
	.loc 1 8824 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L779
.L782:
	.loc 1 8826 0
	ldr	r0, .L783+28
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r3, r3, lsr #3
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	bne	.L780
	.loc 1 8826 0 is_stmt 0 discriminator 1
	ldr	r0, .L783+28
	ldr	r2, [fp, #-8]
	mov	r1, #480
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L781
.L780:
	.loc 1 8828 0 is_stmt 1
	ldr	r0, .L783+28
	ldr	r2, [fp, #-8]
	ldr	r1, .L783+32
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-12]	@ movhi
	.loc 1 8829 0
	ldr	r0, .L783+28
	ldr	r2, [fp, #-8]
	ldr	r1, .L783+36
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	str	r3, [fp, #-16]
	.loc 1 8831 0
	sub	r3, fp, #16
	ldr	r0, [fp, #-20]
	mov	r1, r3
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #0
	beq	.L781
	.loc 1 8833 0
	ldr	r0, .L783+28
	ldr	r2, [fp, #-8]
	mov	r1, #16
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r2, r3, asl #7
	rsb	r2, r3, r2
	mov	r3, r2, asl #4
	mov	r2, r3
	add	r3, r0, r2
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #2
	mov	r2, #0
	mov	r3, #4
	bl	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
.L781:
	.loc 1 8824 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L779:
	.loc 1 8824 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #3
	ble	.L782
	.loc 1 8852 0 is_stmt 1
	bl	_load_and_maintain_system_anti_chatter_timers
.L777:
	.loc 1 8860 0
	ldr	r3, .L783+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 8862 0
	ldr	r3, .L783
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 8863 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L784:
	.align	2
.L783:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	8715
	.word	system_preserves_recursive_MUTEX
	.word	8720
	.word	foal_irri
	.word	one_ON_for_walk_thru
	.word	system_preserves
	.word	14132
	.word	14136
.LFE53:
	.size	FOAL_IRRI_maintain_irrigation_list, .-FOAL_IRRI_maintain_irrigation_list
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI14-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI17-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI20-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI23-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI26-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI29-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI32-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI35-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI38-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI41-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI42-.LCFI41
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI44-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI45-.LCFI44
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI47-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI48-.LCFI47
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI50-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI51-.LCFI50
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI53-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI54-.LCFI53
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI56-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI57-.LCFI56
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI59-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI60-.LCFI59
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI62-.LFB21
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI63-.LCFI62
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI65-.LFB22
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI66-.LCFI65
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI68-.LFB23
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI69-.LCFI68
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI71-.LFB24
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI72-.LCFI71
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI74-.LFB25
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI75-.LCFI74
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI77-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI78-.LCFI77
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI80-.LFB27
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI81-.LCFI80
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI83-.LFB28
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI84-.LCFI83
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI86-.LFB29
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI87-.LCFI86
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI89-.LFB30
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI90-.LCFI89
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI92-.LFB31
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI93-.LCFI92
	.byte	0xe
	.uleb128 0x18
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI94-.LCFI93
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI96-.LFB32
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI97-.LCFI96
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI99-.LFB33
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI100-.LCFI99
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI102-.LFB34
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI103-.LCFI102
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI104-.LCFI103
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI106-.LFB35
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI107-.LCFI106
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI109-.LFB36
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI110-.LCFI109
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI112-.LFB37
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI113-.LCFI112
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI115-.LFB38
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI116-.LCFI115
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI118-.LFB39
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI119-.LCFI118
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI121-.LFB40
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI122-.LCFI121
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI124-.LFB41
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI125-.LCFI124
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI127-.LFB42
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI128-.LCFI127
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI130-.LFB43
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI131-.LCFI130
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI133-.LFB44
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI134-.LCFI133
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.byte	0x4
	.4byte	.LCFI136-.LFB45
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI137-.LCFI136
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI139-.LFB46
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI140-.LCFI139
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI142-.LFB47
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI143-.LCFI142
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE94:
.LSFDE96:
	.4byte	.LEFDE96-.LASFDE96
.LASFDE96:
	.4byte	.Lframe0
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.byte	0x4
	.4byte	.LCFI145-.LFB48
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI146-.LCFI145
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE96:
.LSFDE98:
	.4byte	.LEFDE98-.LASFDE98
.LASFDE98:
	.4byte	.Lframe0
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.byte	0x4
	.4byte	.LCFI148-.LFB49
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI149-.LCFI148
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE98:
.LSFDE100:
	.4byte	.LEFDE100-.LASFDE100
.LASFDE100:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.byte	0x4
	.4byte	.LCFI151-.LFB50
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI152-.LCFI151
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE100:
.LSFDE102:
	.4byte	.LEFDE102-.LASFDE102
.LASFDE102:
	.4byte	.Lframe0
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.byte	0x4
	.4byte	.LCFI154-.LFB51
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI155-.LCFI154
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE102:
.LSFDE104:
	.4byte	.LEFDE104-.LASFDE104
.LASFDE104:
	.4byte	.Lframe0
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.byte	0x4
	.4byte	.LCFI157-.LFB52
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI158-.LCFI157
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE104:
.LSFDE106:
	.4byte	.LEFDE106-.LASFDE106
.LASFDE106:
	.4byte	.Lframe0
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.byte	0x4
	.4byte	.LCFI160-.LFB53
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI161-.LCFI160
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE106:
	.text
.Letext0:
	.file 2 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/string.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_defs.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_comm_server_common.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/configuration_controller.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_tpmicro_common.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/stations.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ftimes/ftimes_vars.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/station_groups.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/foal_comm.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/flow_recorder.h"
	.file 19 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/station_history_data.h"
	.file 20 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/station_report_data.h"
	.file 21 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 22 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/walk_thru.h"
	.file 23 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_irri.h"
	.file 24 "C:/CS3000/cs3_branches/chain_sync/main_app/src/alerts/alerts.h"
	.file 25 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/irri_comm.h"
	.file 26 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 27 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/FreeRTOSConfig.h"
	.file 28 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 29 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x47f9
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF836
	.byte	0x1
	.4byte	.LASF837
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0x38
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x2
	.byte	0x16
	.4byte	0x30
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x5
	.byte	0x4
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x3
	.byte	0x35
	.4byte	0x30
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x4
	.byte	0x57
	.4byte	0x45
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x5
	.byte	0x4c
	.4byte	0x7c
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x6
	.byte	0x65
	.4byte	0x45
	.uleb128 0x6
	.4byte	0x5c
	.4byte	0xad
	.uleb128 0x7
	.4byte	0x30
	.byte	0x1
	.byte	0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF13
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x7
	.byte	0x3a
	.4byte	0x5c
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x7
	.byte	0x4c
	.4byte	0x3e
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x7
	.byte	0x55
	.4byte	0x4e
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x7
	.byte	0x5e
	.4byte	0xe0
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF18
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x7
	.byte	0x67
	.4byte	0x37
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x7
	.byte	0x70
	.4byte	0x6a
	.uleb128 0x2
	.4byte	.LASF21
	.byte	0x7
	.byte	0x99
	.4byte	0xe0
	.uleb128 0x2
	.4byte	.LASF22
	.byte	0x7
	.byte	0x9d
	.4byte	0xe0
	.uleb128 0x8
	.byte	0x8
	.byte	0x8
	.byte	0xba
	.4byte	0x33e
	.uleb128 0x9
	.4byte	.LASF23
	.byte	0x8
	.byte	0xbc
	.4byte	0xd5
	.byte	0x4
	.byte	0x4
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x8
	.byte	0xc6
	.4byte	0xd5
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x8
	.byte	0xc9
	.4byte	0xd5
	.byte	0x4
	.byte	0x4
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x8
	.byte	0xcd
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x8
	.byte	0xcf
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x8
	.byte	0xd1
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x8
	.byte	0xd7
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x8
	.byte	0xdd
	.4byte	0xd5
	.byte	0x4
	.byte	0x2
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x8
	.byte	0xdf
	.4byte	0xd5
	.byte	0x4
	.byte	0x2
	.byte	0xe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x8
	.byte	0xf5
	.4byte	0xd5
	.byte	0x4
	.byte	0x2
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF33
	.byte	0x8
	.byte	0xfb
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF34
	.byte	0x8
	.byte	0xff
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x8
	.2byte	0x102
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x106
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF37
	.byte	0x8
	.2byte	0x10c
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x8
	.2byte	0x111
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x8
	.2byte	0x117
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x8
	.2byte	0x11e
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF41
	.byte	0x8
	.2byte	0x120
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF42
	.byte	0x8
	.2byte	0x128
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x8
	.2byte	0x12a
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF44
	.byte	0x8
	.2byte	0x12c
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0x8
	.2byte	0x130
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF46
	.byte	0x8
	.2byte	0x136
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF47
	.byte	0x8
	.2byte	0x13d
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF48
	.byte	0x8
	.2byte	0x13f
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF49
	.byte	0x8
	.2byte	0x141
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0x8
	.2byte	0x143
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0x8
	.2byte	0x145
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF52
	.byte	0x8
	.2byte	0x147
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF53
	.byte	0x8
	.2byte	0x149
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x8
	.byte	0xb6
	.4byte	0x357
	.uleb128 0xc
	.4byte	.LASF91
	.byte	0x8
	.byte	0xb8
	.4byte	0xf2
	.uleb128 0xd
	.4byte	0x113
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.byte	0x8
	.byte	0xb4
	.4byte	0x368
	.uleb128 0xe
	.4byte	0x33e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF54
	.byte	0x8
	.2byte	0x156
	.4byte	0x357
	.uleb128 0x10
	.byte	0x8
	.byte	0x8
	.2byte	0x163
	.4byte	0x62a
	.uleb128 0xa
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x16b
	.4byte	0xd5
	.byte	0x4
	.byte	0x4
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF56
	.byte	0x8
	.2byte	0x171
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0x8
	.2byte	0x17c
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF58
	.byte	0x8
	.2byte	0x185
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF59
	.byte	0x8
	.2byte	0x19b
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF60
	.byte	0x8
	.2byte	0x19d
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF61
	.byte	0x8
	.2byte	0x19f
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF62
	.byte	0x8
	.2byte	0x1a1
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF63
	.byte	0x8
	.2byte	0x1a3
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF64
	.byte	0x8
	.2byte	0x1a5
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF65
	.byte	0x8
	.2byte	0x1a7
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF66
	.byte	0x8
	.2byte	0x1b1
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF67
	.byte	0x8
	.2byte	0x1b6
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF68
	.byte	0x8
	.2byte	0x1bb
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF69
	.byte	0x8
	.2byte	0x1c7
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF70
	.byte	0x8
	.2byte	0x1cd
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF71
	.byte	0x8
	.2byte	0x1d6
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF72
	.byte	0x8
	.2byte	0x1d8
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF73
	.byte	0x8
	.2byte	0x1e6
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF74
	.byte	0x8
	.2byte	0x1e7
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x8
	.2byte	0x1e8
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF75
	.byte	0x8
	.2byte	0x1e9
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF76
	.byte	0x8
	.2byte	0x1ea
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF77
	.byte	0x8
	.2byte	0x1eb
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF78
	.byte	0x8
	.2byte	0x1ec
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF79
	.byte	0x8
	.2byte	0x1f6
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF80
	.byte	0x8
	.2byte	0x1f7
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x1f8
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF81
	.byte	0x8
	.2byte	0x1f9
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF82
	.byte	0x8
	.2byte	0x1fa
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF83
	.byte	0x8
	.2byte	0x1fb
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF84
	.byte	0x8
	.2byte	0x1fc
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF85
	.byte	0x8
	.2byte	0x206
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF86
	.byte	0x8
	.2byte	0x20d
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF87
	.byte	0x8
	.2byte	0x214
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF88
	.byte	0x8
	.2byte	0x216
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF89
	.byte	0x8
	.2byte	0x223
	.4byte	0xd5
	.byte	0x4
	.byte	0x6
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF90
	.byte	0x8
	.2byte	0x227
	.4byte	0xd5
	.byte	0x4
	.byte	0x4
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x11
	.byte	0x8
	.byte	0x8
	.2byte	0x15f
	.4byte	0x645
	.uleb128 0x12
	.4byte	.LASF92
	.byte	0x8
	.2byte	0x161
	.4byte	0xf2
	.uleb128 0xd
	.4byte	0x374
	.byte	0
	.uleb128 0x10
	.byte	0x8
	.byte	0x8
	.2byte	0x15d
	.4byte	0x657
	.uleb128 0xe
	.4byte	0x62a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF93
	.byte	0x8
	.2byte	0x230
	.4byte	0x645
	.uleb128 0x8
	.byte	0x4
	.byte	0x9
	.byte	0x4c
	.4byte	0x688
	.uleb128 0x13
	.4byte	.LASF94
	.byte	0x9
	.byte	0x55
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF95
	.byte	0x9
	.byte	0x57
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x2
	.4byte	.LASF96
	.byte	0x9
	.byte	0x59
	.4byte	0x663
	.uleb128 0x8
	.byte	0x4
	.byte	0x9
	.byte	0x65
	.4byte	0x6b8
	.uleb128 0x13
	.4byte	.LASF97
	.byte	0x9
	.byte	0x67
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF95
	.byte	0x9
	.byte	0x69
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x2
	.4byte	.LASF98
	.byte	0x9
	.byte	0x6b
	.4byte	0x693
	.uleb128 0x6
	.4byte	0xad
	.4byte	0x6d3
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2f
	.byte	0
	.uleb128 0x8
	.byte	0x6
	.byte	0xa
	.byte	0x22
	.4byte	0x6f4
	.uleb128 0x14
	.ascii	"T\000"
	.byte	0xa
	.byte	0x24
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.ascii	"D\000"
	.byte	0xa
	.byte	0x26
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x2
	.4byte	.LASF99
	.byte	0xa
	.byte	0x28
	.4byte	0x6d3
	.uleb128 0x8
	.byte	0x14
	.byte	0xa
	.byte	0x31
	.4byte	0x786
	.uleb128 0x13
	.4byte	.LASF100
	.byte	0xa
	.byte	0x33
	.4byte	0x6f4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF101
	.byte	0xa
	.byte	0x35
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x13
	.4byte	.LASF102
	.byte	0xa
	.byte	0x35
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF103
	.byte	0xa
	.byte	0x35
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x13
	.4byte	.LASF104
	.byte	0xa
	.byte	0x37
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF105
	.byte	0xa
	.byte	0x37
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x13
	.4byte	.LASF106
	.byte	0xa
	.byte	0x37
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x13
	.4byte	.LASF107
	.byte	0xa
	.byte	0x39
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0x13
	.4byte	.LASF108
	.byte	0xa
	.byte	0x3b
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x2
	.4byte	.LASF109
	.byte	0xa
	.byte	0x3d
	.4byte	0x6ff
	.uleb128 0x6
	.4byte	0xd5
	.4byte	0x7a1
	.uleb128 0x7
	.4byte	0x30
	.byte	0x2
	.byte	0
	.uleb128 0x6
	.4byte	0xd5
	.4byte	0x7b1
	.uleb128 0x7
	.4byte	0x30
	.byte	0x1f
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.byte	0xb
	.byte	0x2f
	.4byte	0x8a8
	.uleb128 0x9
	.4byte	.LASF110
	.byte	0xb
	.byte	0x35
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF111
	.byte	0xb
	.byte	0x3e
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF112
	.byte	0xb
	.byte	0x3f
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF113
	.byte	0xb
	.byte	0x46
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF114
	.byte	0xb
	.byte	0x4e
	.4byte	0xd5
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF115
	.byte	0xb
	.byte	0x4f
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF116
	.byte	0xb
	.byte	0x50
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF117
	.byte	0xb
	.byte	0x52
	.4byte	0xd5
	.byte	0x4
	.byte	0x2
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF118
	.byte	0xb
	.byte	0x53
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF119
	.byte	0xb
	.byte	0x54
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF120
	.byte	0xb
	.byte	0x58
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF121
	.byte	0xb
	.byte	0x59
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF122
	.byte	0xb
	.byte	0x5a
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF123
	.byte	0xb
	.byte	0x5b
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.byte	0xb
	.byte	0x2b
	.4byte	0x8c1
	.uleb128 0xc
	.4byte	.LASF124
	.byte	0xb
	.byte	0x2d
	.4byte	0xbf
	.uleb128 0xd
	.4byte	0x7b1
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.byte	0xb
	.byte	0x29
	.4byte	0x8d2
	.uleb128 0xe
	.4byte	0x8a8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x2
	.4byte	.LASF125
	.byte	0xb
	.byte	0x61
	.4byte	0x8c1
	.uleb128 0x6
	.4byte	0xad
	.4byte	0x8ed
	.uleb128 0x7
	.4byte	0x30
	.byte	0xf
	.byte	0
	.uleb128 0x6
	.4byte	0xad
	.4byte	0x8fd
	.uleb128 0x7
	.4byte	0x30
	.byte	0x7
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0xc
	.2byte	0x1c3
	.4byte	0x916
	.uleb128 0x15
	.4byte	.LASF126
	.byte	0xc
	.2byte	0x1ca
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF127
	.byte	0xc
	.2byte	0x1d0
	.4byte	0x8fd
	.uleb128 0x10
	.byte	0x4
	.byte	0xc
	.2byte	0x235
	.4byte	0x950
	.uleb128 0xa
	.4byte	.LASF128
	.byte	0xc
	.2byte	0x237
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF129
	.byte	0xc
	.2byte	0x239
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x11
	.byte	0x4
	.byte	0xc
	.2byte	0x231
	.4byte	0x96b
	.uleb128 0x12
	.4byte	.LASF130
	.byte	0xc
	.2byte	0x233
	.4byte	0xd5
	.uleb128 0xd
	.4byte	0x922
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0xc
	.2byte	0x22f
	.4byte	0x97d
	.uleb128 0xe
	.4byte	0x950
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF131
	.byte	0xc
	.2byte	0x23e
	.4byte	0x96b
	.uleb128 0x10
	.byte	0x38
	.byte	0xc
	.2byte	0x241
	.4byte	0xa1a
	.uleb128 0x15
	.4byte	.LASF132
	.byte	0xc
	.2byte	0x245
	.4byte	0xa1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.ascii	"poc\000"
	.byte	0xc
	.2byte	0x247
	.4byte	0x97d
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x15
	.4byte	.LASF133
	.byte	0xc
	.2byte	0x249
	.4byte	0x97d
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x15
	.4byte	.LASF134
	.byte	0xc
	.2byte	0x24f
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x15
	.4byte	.LASF135
	.byte	0xc
	.2byte	0x250
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x15
	.4byte	.LASF136
	.byte	0xc
	.2byte	0x252
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF137
	.byte	0xc
	.2byte	0x253
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x15
	.4byte	.LASF138
	.byte	0xc
	.2byte	0x254
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x15
	.4byte	.LASF139
	.byte	0xc
	.2byte	0x256
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x6
	.4byte	0x97d
	.4byte	0xa2a
	.uleb128 0x7
	.4byte	0x30
	.byte	0x5
	.byte	0
	.uleb128 0xf
	.4byte	.LASF140
	.byte	0xc
	.2byte	0x258
	.4byte	0x989
	.uleb128 0x8
	.byte	0x14
	.byte	0xd
	.byte	0x18
	.4byte	0xa85
	.uleb128 0x13
	.4byte	.LASF141
	.byte	0xd
	.byte	0x1a
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF142
	.byte	0xd
	.byte	0x1c
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF143
	.byte	0xd
	.byte	0x1e
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF144
	.byte	0xd
	.byte	0x20
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF145
	.byte	0xd
	.byte	0x23
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x2
	.4byte	.LASF146
	.byte	0xd
	.byte	0x26
	.4byte	0xa36
	.uleb128 0x2
	.4byte	.LASF147
	.byte	0xd
	.byte	0x26
	.4byte	0xa9b
	.uleb128 0x17
	.byte	0x4
	.4byte	0xa36
	.uleb128 0x8
	.byte	0xc
	.byte	0xd
	.byte	0x2a
	.4byte	0xad4
	.uleb128 0x13
	.4byte	.LASF148
	.byte	0xd
	.byte	0x2c
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF149
	.byte	0xd
	.byte	0x2e
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF150
	.byte	0xd
	.byte	0x30
	.4byte	0xad4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0xa85
	.uleb128 0x2
	.4byte	.LASF151
	.byte	0xd
	.byte	0x32
	.4byte	0xaa1
	.uleb128 0x17
	.byte	0x4
	.4byte	0xb4
	.uleb128 0x2
	.4byte	.LASF152
	.byte	0xe
	.byte	0x69
	.4byte	0xaf6
	.uleb128 0x18
	.4byte	.LASF152
	.byte	0x1
	.uleb128 0x3
	.byte	0x4
	.byte	0x4
	.4byte	.LASF153
	.uleb128 0x19
	.2byte	0x100
	.byte	0xf
	.byte	0x4c
	.4byte	0xd77
	.uleb128 0x13
	.4byte	.LASF154
	.byte	0xf
	.byte	0x4e
	.4byte	0xada
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF155
	.byte	0xf
	.byte	0x50
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF156
	.byte	0xf
	.byte	0x53
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x13
	.4byte	.LASF157
	.byte	0xf
	.byte	0x55
	.4byte	0xd77
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x13
	.4byte	.LASF158
	.byte	0xf
	.byte	0x5a
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x13
	.4byte	.LASF159
	.byte	0xf
	.byte	0x5f
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x13
	.4byte	.LASF160
	.byte	0xf
	.byte	0x62
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x13
	.4byte	.LASF161
	.byte	0xf
	.byte	0x65
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x13
	.4byte	.LASF162
	.byte	0xf
	.byte	0x6a
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x13
	.4byte	.LASF163
	.byte	0xf
	.byte	0x6c
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x13
	.4byte	.LASF164
	.byte	0xf
	.byte	0x6e
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x13
	.4byte	.LASF165
	.byte	0xf
	.byte	0x70
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x13
	.4byte	.LASF166
	.byte	0xf
	.byte	0x77
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x13
	.4byte	.LASF167
	.byte	0xf
	.byte	0x79
	.4byte	0xd87
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x13
	.4byte	.LASF168
	.byte	0xf
	.byte	0x80
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x13
	.4byte	.LASF169
	.byte	0xf
	.byte	0x82
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x13
	.4byte	.LASF170
	.byte	0xf
	.byte	0x84
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x13
	.4byte	.LASF171
	.byte	0xf
	.byte	0x8f
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x13
	.4byte	.LASF172
	.byte	0xf
	.byte	0x94
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x13
	.4byte	.LASF173
	.byte	0xf
	.byte	0x98
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x13
	.4byte	.LASF174
	.byte	0xf
	.byte	0xa8
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x13
	.4byte	.LASF175
	.byte	0xf
	.byte	0xac
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x13
	.4byte	.LASF176
	.byte	0xf
	.byte	0xb0
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x13
	.4byte	.LASF177
	.byte	0xf
	.byte	0xb3
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x13
	.4byte	.LASF178
	.byte	0xf
	.byte	0xb7
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x13
	.4byte	.LASF179
	.byte	0xf
	.byte	0xb9
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x13
	.4byte	.LASF180
	.byte	0xf
	.byte	0xc1
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0x13
	.4byte	.LASF181
	.byte	0xf
	.byte	0xc6
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0x13
	.4byte	.LASF182
	.byte	0xf
	.byte	0xc8
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xbc
	.uleb128 0x13
	.4byte	.LASF183
	.byte	0xf
	.byte	0xd3
	.4byte	0x6f4
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x13
	.4byte	.LASF184
	.byte	0xf
	.byte	0xd7
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0x13
	.4byte	.LASF185
	.byte	0xf
	.byte	0xda
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x13
	.4byte	.LASF186
	.byte	0xf
	.byte	0xe0
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x13
	.4byte	.LASF187
	.byte	0xf
	.byte	0xe4
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0x13
	.4byte	.LASF188
	.byte	0xf
	.byte	0xeb
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0x13
	.4byte	.LASF189
	.byte	0xf
	.byte	0xed
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xdc
	.uleb128 0x13
	.4byte	.LASF190
	.byte	0xf
	.byte	0xf3
	.4byte	0x6f4
	.byte	0x3
	.byte	0x23
	.uleb128 0xe0
	.uleb128 0x13
	.4byte	.LASF191
	.byte	0xf
	.byte	0xf6
	.4byte	0x6f4
	.byte	0x3
	.byte	0x23
	.uleb128 0xe6
	.uleb128 0x13
	.4byte	.LASF192
	.byte	0xf
	.byte	0xf8
	.4byte	0x6f4
	.byte	0x3
	.byte	0x23
	.uleb128 0xec
	.uleb128 0x13
	.4byte	.LASF193
	.byte	0xf
	.byte	0xfc
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xf4
	.uleb128 0x15
	.4byte	.LASF194
	.byte	0xf
	.2byte	0x100
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xf8
	.uleb128 0x15
	.4byte	.LASF195
	.byte	0xf
	.2byte	0x104
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xfc
	.byte	0
	.uleb128 0x6
	.4byte	0xd5
	.4byte	0xd87
	.uleb128 0x7
	.4byte	0x30
	.byte	0xb
	.byte	0
	.uleb128 0x6
	.4byte	0xfd
	.4byte	0xd97
	.uleb128 0x7
	.4byte	0x30
	.byte	0x6
	.byte	0
	.uleb128 0xf
	.4byte	.LASF196
	.byte	0xf
	.2byte	0x106
	.4byte	0xb03
	.uleb128 0x10
	.byte	0x50
	.byte	0xf
	.2byte	0x10f
	.4byte	0xe16
	.uleb128 0x15
	.4byte	.LASF197
	.byte	0xf
	.2byte	0x111
	.4byte	0xada
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF155
	.byte	0xf
	.2byte	0x113
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x15
	.4byte	.LASF198
	.byte	0xf
	.2byte	0x116
	.4byte	0xe16
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF199
	.byte	0xf
	.2byte	0x118
	.4byte	0xd87
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF200
	.byte	0xf
	.2byte	0x11a
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x15
	.4byte	.LASF201
	.byte	0xf
	.2byte	0x11c
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x15
	.4byte	.LASF202
	.byte	0xf
	.2byte	0x11e
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.byte	0
	.uleb128 0x6
	.4byte	0xd5
	.4byte	0xe26
	.uleb128 0x7
	.4byte	0x30
	.byte	0x5
	.byte	0
	.uleb128 0xf
	.4byte	.LASF203
	.byte	0xf
	.2byte	0x120
	.4byte	0xda3
	.uleb128 0x10
	.byte	0x90
	.byte	0xf
	.2byte	0x129
	.4byte	0x1000
	.uleb128 0x15
	.4byte	.LASF204
	.byte	0xf
	.2byte	0x131
	.4byte	0xada
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF205
	.byte	0xf
	.2byte	0x139
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x15
	.4byte	.LASF206
	.byte	0xf
	.2byte	0x13e
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF207
	.byte	0xf
	.2byte	0x140
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x15
	.4byte	.LASF208
	.byte	0xf
	.2byte	0x142
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x15
	.4byte	.LASF209
	.byte	0xf
	.2byte	0x149
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x15
	.4byte	.LASF210
	.byte	0xf
	.2byte	0x151
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x15
	.4byte	.LASF211
	.byte	0xf
	.2byte	0x158
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x15
	.4byte	.LASF212
	.byte	0xf
	.2byte	0x173
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF213
	.byte	0xf
	.2byte	0x17d
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x15
	.4byte	.LASF214
	.byte	0xf
	.2byte	0x199
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x15
	.4byte	.LASF215
	.byte	0xf
	.2byte	0x19d
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x15
	.4byte	.LASF216
	.byte	0xf
	.2byte	0x19f
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x15
	.4byte	.LASF217
	.byte	0xf
	.2byte	0x1a9
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x15
	.4byte	.LASF218
	.byte	0xf
	.2byte	0x1ad
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x15
	.4byte	.LASF219
	.byte	0xf
	.2byte	0x1af
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x15
	.4byte	.LASF220
	.byte	0xf
	.2byte	0x1b7
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x15
	.4byte	.LASF221
	.byte	0xf
	.2byte	0x1c1
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x15
	.4byte	.LASF222
	.byte	0xf
	.2byte	0x1c3
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x15
	.4byte	.LASF223
	.byte	0xf
	.2byte	0x1cc
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x15
	.4byte	.LASF224
	.byte	0xf
	.2byte	0x1d1
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x15
	.4byte	.LASF225
	.byte	0xf
	.2byte	0x1d9
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x15
	.4byte	.LASF226
	.byte	0xf
	.2byte	0x1e3
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x15
	.4byte	.LASF227
	.byte	0xf
	.2byte	0x1e5
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x15
	.4byte	.LASF228
	.byte	0xf
	.2byte	0x1e9
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x15
	.4byte	.LASF229
	.byte	0xf
	.2byte	0x1eb
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x15
	.4byte	.LASF230
	.byte	0xf
	.2byte	0x1ed
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x15
	.4byte	.LASF231
	.byte	0xf
	.2byte	0x1f4
	.4byte	0x1000
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x15
	.4byte	.LASF232
	.byte	0xf
	.2byte	0x1fc
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x16
	.ascii	"sbf\000"
	.byte	0xf
	.2byte	0x203
	.4byte	0x657
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.byte	0
	.uleb128 0x6
	.4byte	0xd5
	.4byte	0x1010
	.uleb128 0x7
	.4byte	0x30
	.byte	0x3
	.byte	0
	.uleb128 0xf
	.4byte	.LASF233
	.byte	0xf
	.2byte	0x205
	.4byte	0xe32
	.uleb128 0x10
	.byte	0x4
	.byte	0xf
	.2byte	0x213
	.4byte	0x106e
	.uleb128 0xa
	.4byte	.LASF234
	.byte	0xf
	.2byte	0x215
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF235
	.byte	0xf
	.2byte	0x21d
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF236
	.byte	0xf
	.2byte	0x227
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF237
	.byte	0xf
	.2byte	0x233
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x11
	.byte	0x4
	.byte	0xf
	.2byte	0x20f
	.4byte	0x1089
	.uleb128 0x12
	.4byte	.LASF91
	.byte	0xf
	.2byte	0x211
	.4byte	0xd5
	.uleb128 0xd
	.4byte	0x101c
	.byte	0
	.uleb128 0x10
	.byte	0x4
	.byte	0xf
	.2byte	0x20d
	.4byte	0x109b
	.uleb128 0xe
	.4byte	0x106e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF238
	.byte	0xf
	.2byte	0x23b
	.4byte	0x1089
	.uleb128 0x10
	.byte	0x70
	.byte	0xf
	.2byte	0x23e
	.4byte	0x120a
	.uleb128 0x15
	.4byte	.LASF239
	.byte	0xf
	.2byte	0x249
	.4byte	0x109b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF240
	.byte	0xf
	.2byte	0x24b
	.4byte	0xada
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF241
	.byte	0xf
	.2byte	0x24d
	.4byte	0xada
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF242
	.byte	0xf
	.2byte	0x251
	.4byte	0x120a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x15
	.4byte	.LASF243
	.byte	0xf
	.2byte	0x253
	.4byte	0x1210
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x15
	.4byte	.LASF244
	.byte	0xf
	.2byte	0x258
	.4byte	0x1216
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x15
	.4byte	.LASF245
	.byte	0xf
	.2byte	0x25c
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x15
	.4byte	.LASF246
	.byte	0xf
	.2byte	0x25e
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x15
	.4byte	.LASF247
	.byte	0xf
	.2byte	0x266
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x15
	.4byte	.LASF248
	.byte	0xf
	.2byte	0x26a
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x15
	.4byte	.LASF249
	.byte	0xf
	.2byte	0x26e
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x15
	.4byte	.LASF250
	.byte	0xf
	.2byte	0x272
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x15
	.4byte	.LASF251
	.byte	0xf
	.2byte	0x277
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x15
	.4byte	.LASF252
	.byte	0xf
	.2byte	0x27c
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x15
	.4byte	.LASF253
	.byte	0xf
	.2byte	0x281
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x15
	.4byte	.LASF254
	.byte	0xf
	.2byte	0x285
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x15
	.4byte	.LASF255
	.byte	0xf
	.2byte	0x288
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x15
	.4byte	.LASF256
	.byte	0xf
	.2byte	0x28c
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x15
	.4byte	.LASF257
	.byte	0xf
	.2byte	0x28e
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x15
	.4byte	.LASF258
	.byte	0xf
	.2byte	0x298
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x15
	.4byte	.LASF259
	.byte	0xf
	.2byte	0x29a
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x15
	.4byte	.LASF260
	.byte	0xf
	.2byte	0x29f
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x66
	.uleb128 0x16
	.ascii	"bbf\000"
	.byte	0xf
	.2byte	0x2a3
	.4byte	0x368
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0x1010
	.uleb128 0x17
	.byte	0x4
	.4byte	0xd97
	.uleb128 0x6
	.4byte	0x1226
	.4byte	0x1226
	.uleb128 0x7
	.4byte	0x30
	.byte	0x1
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0xe26
	.uleb128 0xf
	.4byte	.LASF261
	.byte	0xf
	.2byte	0x2a5
	.4byte	0x10a7
	.uleb128 0xf
	.4byte	.LASF262
	.byte	0x10
	.2byte	0x1a2
	.4byte	0x1244
	.uleb128 0x18
	.4byte	.LASF262
	.byte	0x1
	.uleb128 0x8
	.byte	0x48
	.byte	0x11
	.byte	0x3b
	.4byte	0x1298
	.uleb128 0x13
	.4byte	.LASF263
	.byte	0x11
	.byte	0x44
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF264
	.byte	0x11
	.byte	0x46
	.4byte	0x8d2
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x14
	.ascii	"wi\000"
	.byte	0x11
	.byte	0x48
	.4byte	0xa2a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF265
	.byte	0x11
	.byte	0x4c
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x13
	.4byte	.LASF266
	.byte	0x11
	.byte	0x4e
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.byte	0
	.uleb128 0x2
	.4byte	.LASF267
	.byte	0x11
	.byte	0x54
	.4byte	0x124a
	.uleb128 0x10
	.byte	0x18
	.byte	0x11
	.2byte	0x116
	.4byte	0x1316
	.uleb128 0x15
	.4byte	.LASF205
	.byte	0x11
	.2byte	0x11c
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF268
	.byte	0x11
	.2byte	0x121
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF269
	.byte	0x11
	.2byte	0x126
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x15
	.4byte	.LASF270
	.byte	0x11
	.2byte	0x129
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x15
	.4byte	.LASF246
	.byte	0x11
	.2byte	0x12b
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF271
	.byte	0x11
	.2byte	0x12d
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x15
	.4byte	.LASF272
	.byte	0x11
	.2byte	0x12f
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x15
	.byte	0
	.uleb128 0xf
	.4byte	.LASF273
	.byte	0x11
	.2byte	0x131
	.4byte	0x12a3
	.uleb128 0x10
	.byte	0xc
	.byte	0x11
	.2byte	0x1f8
	.4byte	0x1386
	.uleb128 0x15
	.4byte	.LASF246
	.byte	0x11
	.2byte	0x1fb
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF271
	.byte	0x11
	.2byte	0x1fd
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x15
	.4byte	.LASF274
	.byte	0x11
	.2byte	0x1ff
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x15
	.4byte	.LASF275
	.byte	0x11
	.2byte	0x201
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.uleb128 0x15
	.4byte	.LASF276
	.byte	0x11
	.2byte	0x206
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF277
	.byte	0x11
	.2byte	0x208
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xf
	.4byte	.LASF278
	.byte	0x11
	.2byte	0x20a
	.4byte	0x1322
	.uleb128 0x10
	.byte	0x18
	.byte	0x11
	.2byte	0x210
	.4byte	0x13f6
	.uleb128 0x15
	.4byte	.LASF279
	.byte	0x11
	.2byte	0x215
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF280
	.byte	0x11
	.2byte	0x217
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF281
	.byte	0x11
	.2byte	0x21e
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x15
	.4byte	.LASF282
	.byte	0x11
	.2byte	0x220
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x15
	.4byte	.LASF283
	.byte	0x11
	.2byte	0x224
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF284
	.byte	0x11
	.2byte	0x22d
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0xf
	.4byte	.LASF285
	.byte	0x11
	.2byte	0x22f
	.4byte	0x1392
	.uleb128 0x10
	.byte	0x10
	.byte	0x11
	.2byte	0x253
	.4byte	0x1448
	.uleb128 0x15
	.4byte	.LASF286
	.byte	0x11
	.2byte	0x258
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF287
	.byte	0x11
	.2byte	0x25a
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF259
	.byte	0x11
	.2byte	0x260
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x15
	.4byte	.LASF258
	.byte	0x11
	.2byte	0x263
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0xf
	.4byte	.LASF288
	.byte	0x11
	.2byte	0x268
	.4byte	0x1402
	.uleb128 0x10
	.byte	0x8
	.byte	0x11
	.2byte	0x26c
	.4byte	0x147c
	.uleb128 0x15
	.4byte	.LASF286
	.byte	0x11
	.2byte	0x271
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF287
	.byte	0x11
	.2byte	0x273
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xf
	.4byte	.LASF289
	.byte	0x11
	.2byte	0x27c
	.4byte	0x1454
	.uleb128 0x8
	.byte	0x2
	.byte	0x12
	.byte	0x35
	.4byte	0x157f
	.uleb128 0x9
	.4byte	.LASF290
	.byte	0x12
	.byte	0x3e
	.4byte	0xd5
	.byte	0x4
	.byte	0x3
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF291
	.byte	0x12
	.byte	0x42
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF292
	.byte	0x12
	.byte	0x43
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF293
	.byte	0x12
	.byte	0x44
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF294
	.byte	0x12
	.byte	0x45
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF295
	.byte	0x12
	.byte	0x46
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF296
	.byte	0x12
	.byte	0x48
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF297
	.byte	0x12
	.byte	0x49
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF298
	.byte	0x12
	.byte	0x4a
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF299
	.byte	0x12
	.byte	0x4b
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF300
	.byte	0x12
	.byte	0x4c
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF301
	.byte	0x12
	.byte	0x4d
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF302
	.byte	0x12
	.byte	0x4f
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF303
	.byte	0x12
	.byte	0x50
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xb
	.byte	0x2
	.byte	0x12
	.byte	0x2f
	.4byte	0x1598
	.uleb128 0xc
	.4byte	.LASF92
	.byte	0x12
	.byte	0x33
	.4byte	0xbf
	.uleb128 0xd
	.4byte	0x1488
	.byte	0
	.uleb128 0x2
	.4byte	.LASF304
	.byte	0x12
	.byte	0x58
	.4byte	0x157f
	.uleb128 0x8
	.byte	0x1c
	.byte	0x12
	.byte	0x8f
	.4byte	0x160e
	.uleb128 0x13
	.4byte	.LASF305
	.byte	0x12
	.byte	0x94
	.4byte	0xae5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF306
	.byte	0x12
	.byte	0x99
	.4byte	0xae5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF307
	.byte	0x12
	.byte	0x9e
	.4byte	0xae5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF308
	.byte	0x12
	.byte	0xa3
	.4byte	0xae5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF309
	.byte	0x12
	.byte	0xad
	.4byte	0xae5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x13
	.4byte	.LASF310
	.byte	0x12
	.byte	0xb8
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x13
	.4byte	.LASF311
	.byte	0x12
	.byte	0xbe
	.4byte	0x92
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x2
	.4byte	.LASF312
	.byte	0x12
	.byte	0xc2
	.4byte	0x15a3
	.uleb128 0x8
	.byte	0x4
	.byte	0x13
	.byte	0x24
	.4byte	0x1842
	.uleb128 0x9
	.4byte	.LASF313
	.byte	0x13
	.byte	0x31
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF314
	.byte	0x13
	.byte	0x35
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF315
	.byte	0x13
	.byte	0x37
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF316
	.byte	0x13
	.byte	0x39
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF317
	.byte	0x13
	.byte	0x3b
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF318
	.byte	0x13
	.byte	0x3c
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF319
	.byte	0x13
	.byte	0x3d
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF320
	.byte	0x13
	.byte	0x3e
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF321
	.byte	0x13
	.byte	0x40
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF322
	.byte	0x13
	.byte	0x44
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF323
	.byte	0x13
	.byte	0x46
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF324
	.byte	0x13
	.byte	0x47
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF325
	.byte	0x13
	.byte	0x4d
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF326
	.byte	0x13
	.byte	0x4f
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF327
	.byte	0x13
	.byte	0x50
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF328
	.byte	0x13
	.byte	0x52
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF329
	.byte	0x13
	.byte	0x53
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF330
	.byte	0x13
	.byte	0x55
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xe
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF331
	.byte	0x13
	.byte	0x56
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF332
	.byte	0x13
	.byte	0x5b
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF333
	.byte	0x13
	.byte	0x5d
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF334
	.byte	0x13
	.byte	0x5e
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0xa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF335
	.byte	0x13
	.byte	0x5f
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF336
	.byte	0x13
	.byte	0x61
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF337
	.byte	0x13
	.byte	0x62
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF338
	.byte	0x13
	.byte	0x68
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF169
	.byte	0x13
	.byte	0x6a
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF339
	.byte	0x13
	.byte	0x70
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF340
	.byte	0x13
	.byte	0x78
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF341
	.byte	0x13
	.byte	0x7c
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF342
	.byte	0x13
	.byte	0x7e
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.4byte	.LASF343
	.byte	0x13
	.byte	0x82
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.byte	0x13
	.byte	0x20
	.4byte	0x185b
	.uleb128 0xc
	.4byte	.LASF92
	.byte	0x13
	.byte	0x22
	.4byte	0xd5
	.uleb128 0xd
	.4byte	0x1619
	.byte	0
	.uleb128 0x2
	.4byte	.LASF344
	.byte	0x13
	.byte	0x8d
	.4byte	0x1842
	.uleb128 0x8
	.byte	0x3c
	.byte	0x13
	.byte	0xa5
	.4byte	0x19d8
	.uleb128 0x13
	.4byte	.LASF345
	.byte	0x13
	.byte	0xb0
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF346
	.byte	0x13
	.byte	0xb5
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF347
	.byte	0x13
	.byte	0xb8
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF348
	.byte	0x13
	.byte	0xbd
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF349
	.byte	0x13
	.byte	0xc3
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x13
	.4byte	.LASF350
	.byte	0x13
	.byte	0xd0
	.4byte	0x185b
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x13
	.4byte	.LASF180
	.byte	0x13
	.byte	0xdb
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x13
	.4byte	.LASF351
	.byte	0x13
	.byte	0xdd
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x1a
	.uleb128 0x13
	.4byte	.LASF352
	.byte	0x13
	.byte	0xe4
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x13
	.4byte	.LASF353
	.byte	0x13
	.byte	0xe8
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x1e
	.uleb128 0x13
	.4byte	.LASF354
	.byte	0x13
	.byte	0xea
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x13
	.4byte	.LASF355
	.byte	0x13
	.byte	0xf0
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x22
	.uleb128 0x13
	.4byte	.LASF356
	.byte	0x13
	.byte	0xf9
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x13
	.4byte	.LASF357
	.byte	0x13
	.byte	0xff
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF358
	.byte	0x13
	.2byte	0x101
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2a
	.uleb128 0x15
	.4byte	.LASF359
	.byte	0x13
	.2byte	0x109
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x15
	.4byte	.LASF360
	.byte	0x13
	.2byte	0x10f
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2e
	.uleb128 0x15
	.4byte	.LASF361
	.byte	0x13
	.2byte	0x111
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x15
	.4byte	.LASF362
	.byte	0x13
	.2byte	0x113
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x32
	.uleb128 0x15
	.4byte	.LASF363
	.byte	0x13
	.2byte	0x118
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x15
	.4byte	.LASF364
	.byte	0x13
	.2byte	0x11a
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x35
	.uleb128 0x15
	.4byte	.LASF246
	.byte	0x13
	.2byte	0x11d
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x36
	.uleb128 0x15
	.4byte	.LASF365
	.byte	0x13
	.2byte	0x121
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x37
	.uleb128 0x15
	.4byte	.LASF366
	.byte	0x13
	.2byte	0x12c
	.4byte	0xca
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x15
	.4byte	.LASF367
	.byte	0x13
	.2byte	0x12e
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x3a
	.byte	0
	.uleb128 0xf
	.4byte	.LASF368
	.byte	0x13
	.2byte	0x13a
	.4byte	0x1866
	.uleb128 0x8
	.byte	0x30
	.byte	0x14
	.byte	0x22
	.4byte	0x1adb
	.uleb128 0x13
	.4byte	.LASF345
	.byte	0x14
	.byte	0x24
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF369
	.byte	0x14
	.byte	0x2a
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF370
	.byte	0x14
	.byte	0x2c
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF371
	.byte	0x14
	.byte	0x2e
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF372
	.byte	0x14
	.byte	0x30
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x13
	.4byte	.LASF373
	.byte	0x14
	.byte	0x32
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x13
	.4byte	.LASF374
	.byte	0x14
	.byte	0x34
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x13
	.4byte	.LASF375
	.byte	0x14
	.byte	0x39
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x13
	.4byte	.LASF376
	.byte	0x14
	.byte	0x44
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x13
	.4byte	.LASF352
	.byte	0x14
	.byte	0x48
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x22
	.uleb128 0x13
	.4byte	.LASF377
	.byte	0x14
	.byte	0x4c
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x13
	.4byte	.LASF378
	.byte	0x14
	.byte	0x4e
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x26
	.uleb128 0x13
	.4byte	.LASF379
	.byte	0x14
	.byte	0x50
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x13
	.4byte	.LASF380
	.byte	0x14
	.byte	0x52
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2a
	.uleb128 0x13
	.4byte	.LASF381
	.byte	0x14
	.byte	0x54
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x13
	.4byte	.LASF363
	.byte	0x14
	.byte	0x59
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x2e
	.uleb128 0x13
	.4byte	.LASF246
	.byte	0x14
	.byte	0x5c
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x2f
	.byte	0
	.uleb128 0x2
	.4byte	.LASF382
	.byte	0x14
	.byte	0x66
	.4byte	0x19e4
	.uleb128 0x10
	.byte	0x1c
	.byte	0x15
	.2byte	0x10c
	.4byte	0x1b59
	.uleb128 0x16
	.ascii	"rip\000"
	.byte	0x15
	.2byte	0x112
	.4byte	0x6b8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF383
	.byte	0x15
	.2byte	0x11b
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF384
	.byte	0x15
	.2byte	0x122
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x15
	.4byte	.LASF385
	.byte	0x15
	.2byte	0x127
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x15
	.4byte	.LASF386
	.byte	0x15
	.2byte	0x138
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF387
	.byte	0x15
	.2byte	0x144
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x15
	.4byte	.LASF388
	.byte	0x15
	.2byte	0x14b
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0xf
	.4byte	.LASF389
	.byte	0x15
	.2byte	0x14d
	.4byte	0x1ae6
	.uleb128 0x10
	.byte	0xec
	.byte	0x15
	.2byte	0x150
	.4byte	0x1d39
	.uleb128 0x15
	.4byte	.LASF390
	.byte	0x15
	.2byte	0x157
	.4byte	0x8dd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF391
	.byte	0x15
	.2byte	0x162
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF392
	.byte	0x15
	.2byte	0x164
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x15
	.4byte	.LASF393
	.byte	0x15
	.2byte	0x166
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x15
	.4byte	.LASF394
	.byte	0x15
	.2byte	0x168
	.4byte	0x6f4
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x15
	.4byte	.LASF395
	.byte	0x15
	.2byte	0x16e
	.4byte	0x688
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x15
	.4byte	.LASF396
	.byte	0x15
	.2byte	0x174
	.4byte	0x1b59
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF397
	.byte	0x15
	.2byte	0x17b
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x15
	.4byte	.LASF398
	.byte	0x15
	.2byte	0x17d
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x15
	.4byte	.LASF399
	.byte	0x15
	.2byte	0x185
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x15
	.4byte	.LASF400
	.byte	0x15
	.2byte	0x18d
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x15
	.4byte	.LASF401
	.byte	0x15
	.2byte	0x191
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x15
	.4byte	.LASF402
	.byte	0x15
	.2byte	0x195
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x15
	.4byte	.LASF403
	.byte	0x15
	.2byte	0x199
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x15
	.4byte	.LASF404
	.byte	0x15
	.2byte	0x19e
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x15
	.4byte	.LASF405
	.byte	0x15
	.2byte	0x1a2
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x15
	.4byte	.LASF406
	.byte	0x15
	.2byte	0x1a6
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x15
	.4byte	.LASF407
	.byte	0x15
	.2byte	0x1b4
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x15
	.4byte	.LASF408
	.byte	0x15
	.2byte	0x1ba
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x15
	.4byte	.LASF409
	.byte	0x15
	.2byte	0x1c2
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x15
	.4byte	.LASF410
	.byte	0x15
	.2byte	0x1c4
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x15
	.4byte	.LASF411
	.byte	0x15
	.2byte	0x1c6
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x15
	.4byte	.LASF412
	.byte	0x15
	.2byte	0x1d5
	.4byte	0xb4
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x15
	.4byte	.LASF413
	.byte	0x15
	.2byte	0x1d7
	.4byte	0xb4
	.byte	0x3
	.byte	0x23
	.uleb128 0x81
	.uleb128 0x15
	.4byte	.LASF414
	.byte	0x15
	.2byte	0x1dd
	.4byte	0xb4
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0x15
	.4byte	.LASF415
	.byte	0x15
	.2byte	0x1e7
	.4byte	0xb4
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0x15
	.4byte	.LASF416
	.byte	0x15
	.2byte	0x1f0
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x15
	.4byte	.LASF417
	.byte	0x15
	.2byte	0x1f7
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x15
	.4byte	.LASF418
	.byte	0x15
	.2byte	0x1f9
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x15
	.4byte	.LASF419
	.byte	0x15
	.2byte	0x1fd
	.4byte	0x1d39
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0x6
	.4byte	0xd5
	.4byte	0x1d49
	.uleb128 0x7
	.4byte	0x30
	.byte	0x16
	.byte	0
	.uleb128 0xf
	.4byte	.LASF420
	.byte	0x15
	.2byte	0x204
	.4byte	0x1b65
	.uleb128 0x10
	.byte	0x2
	.byte	0x15
	.2byte	0x249
	.4byte	0x1e01
	.uleb128 0xa
	.4byte	.LASF421
	.byte	0x15
	.2byte	0x25d
	.4byte	0xd5
	.byte	0x4
	.byte	0x4
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF422
	.byte	0x15
	.2byte	0x264
	.4byte	0xd5
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF423
	.byte	0x15
	.2byte	0x26d
	.4byte	0xd5
	.byte	0x4
	.byte	0x4
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF424
	.byte	0x15
	.2byte	0x271
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF425
	.byte	0x15
	.2byte	0x273
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF426
	.byte	0x15
	.2byte	0x277
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF427
	.byte	0x15
	.2byte	0x281
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF428
	.byte	0x15
	.2byte	0x289
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF429
	.byte	0x15
	.2byte	0x290
	.4byte	0x108
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x11
	.byte	0x2
	.byte	0x15
	.2byte	0x243
	.4byte	0x1e1c
	.uleb128 0x12
	.4byte	.LASF92
	.byte	0x15
	.2byte	0x247
	.4byte	0xbf
	.uleb128 0xd
	.4byte	0x1d55
	.byte	0
	.uleb128 0xf
	.4byte	.LASF430
	.byte	0x15
	.2byte	0x296
	.4byte	0x1e01
	.uleb128 0x10
	.byte	0x80
	.byte	0x15
	.2byte	0x2aa
	.4byte	0x1ec8
	.uleb128 0x15
	.4byte	.LASF431
	.byte	0x15
	.2byte	0x2b5
	.4byte	0x19d8
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF432
	.byte	0x15
	.2byte	0x2b9
	.4byte	0x1adb
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x15
	.4byte	.LASF433
	.byte	0x15
	.2byte	0x2bf
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x15
	.4byte	.LASF434
	.byte	0x15
	.2byte	0x2c3
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x15
	.4byte	.LASF435
	.byte	0x15
	.2byte	0x2c9
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x15
	.4byte	.LASF436
	.byte	0x15
	.2byte	0x2cd
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x76
	.uleb128 0x15
	.4byte	.LASF437
	.byte	0x15
	.2byte	0x2d4
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x15
	.4byte	.LASF438
	.byte	0x15
	.2byte	0x2d8
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x7a
	.uleb128 0x15
	.4byte	.LASF439
	.byte	0x15
	.2byte	0x2dd
	.4byte	0x1e1c
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x15
	.4byte	.LASF440
	.byte	0x15
	.2byte	0x2e5
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x7e
	.byte	0
	.uleb128 0xf
	.4byte	.LASF441
	.byte	0x15
	.2byte	0x2ff
	.4byte	0x1e28
	.uleb128 0x1a
	.4byte	0x42010
	.byte	0x15
	.2byte	0x309
	.4byte	0x1eff
	.uleb128 0x15
	.4byte	.LASF390
	.byte	0x15
	.2byte	0x310
	.4byte	0x8dd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.ascii	"sps\000"
	.byte	0x15
	.2byte	0x314
	.4byte	0x1eff
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x6
	.4byte	0x1ec8
	.4byte	0x1f10
	.uleb128 0x1b
	.4byte	0x30
	.2byte	0x83f
	.byte	0
	.uleb128 0xf
	.4byte	.LASF442
	.byte	0x15
	.2byte	0x31b
	.4byte	0x1ed4
	.uleb128 0x10
	.byte	0x10
	.byte	0x15
	.2byte	0x366
	.4byte	0x1fbc
	.uleb128 0x15
	.4byte	.LASF443
	.byte	0x15
	.2byte	0x379
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF444
	.byte	0x15
	.2byte	0x37b
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x15
	.4byte	.LASF445
	.byte	0x15
	.2byte	0x37d
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x15
	.4byte	.LASF446
	.byte	0x15
	.2byte	0x381
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.uleb128 0x15
	.4byte	.LASF447
	.byte	0x15
	.2byte	0x387
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF448
	.byte	0x15
	.2byte	0x388
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x15
	.4byte	.LASF449
	.byte	0x15
	.2byte	0x38a
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x15
	.4byte	.LASF450
	.byte	0x15
	.2byte	0x38b
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x15
	.4byte	.LASF451
	.byte	0x15
	.2byte	0x38d
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x15
	.4byte	.LASF452
	.byte	0x15
	.2byte	0x38e
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.byte	0
	.uleb128 0xf
	.4byte	.LASF453
	.byte	0x15
	.2byte	0x390
	.4byte	0x1f1c
	.uleb128 0x10
	.byte	0x4c
	.byte	0x15
	.2byte	0x39b
	.4byte	0x20e0
	.uleb128 0x15
	.4byte	.LASF205
	.byte	0x15
	.2byte	0x39f
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF454
	.byte	0x15
	.2byte	0x3a8
	.4byte	0x6f4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF455
	.byte	0x15
	.2byte	0x3aa
	.4byte	0x6f4
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x15
	.4byte	.LASF456
	.byte	0x15
	.2byte	0x3b1
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF457
	.byte	0x15
	.2byte	0x3b7
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x15
	.4byte	.LASF458
	.byte	0x15
	.2byte	0x3b8
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x15
	.4byte	.LASF459
	.byte	0x15
	.2byte	0x3ba
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x15
	.4byte	.LASF373
	.byte	0x15
	.2byte	0x3bb
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x15
	.4byte	.LASF460
	.byte	0x15
	.2byte	0x3bd
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x15
	.4byte	.LASF372
	.byte	0x15
	.2byte	0x3be
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF461
	.byte	0x15
	.2byte	0x3c0
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x15
	.4byte	.LASF371
	.byte	0x15
	.2byte	0x3c1
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x15
	.4byte	.LASF462
	.byte	0x15
	.2byte	0x3c3
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x15
	.4byte	.LASF370
	.byte	0x15
	.2byte	0x3c4
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x15
	.4byte	.LASF463
	.byte	0x15
	.2byte	0x3c6
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x15
	.4byte	.LASF464
	.byte	0x15
	.2byte	0x3c7
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x15
	.4byte	.LASF465
	.byte	0x15
	.2byte	0x3c9
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x15
	.4byte	.LASF466
	.byte	0x15
	.2byte	0x3ca
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.byte	0
	.uleb128 0xf
	.4byte	.LASF467
	.byte	0x15
	.2byte	0x3d1
	.4byte	0x1fc8
	.uleb128 0x10
	.byte	0x28
	.byte	0x15
	.2byte	0x3d4
	.4byte	0x218c
	.uleb128 0x15
	.4byte	.LASF205
	.byte	0x15
	.2byte	0x3d6
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF286
	.byte	0x15
	.2byte	0x3d8
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF468
	.byte	0x15
	.2byte	0x3d9
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x15
	.4byte	.LASF200
	.byte	0x15
	.2byte	0x3db
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x15
	.4byte	.LASF469
	.byte	0x15
	.2byte	0x3dc
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF470
	.byte	0x15
	.2byte	0x3dd
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x15
	.4byte	.LASF471
	.byte	0x15
	.2byte	0x3e0
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x15
	.4byte	.LASF472
	.byte	0x15
	.2byte	0x3e3
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x15
	.4byte	.LASF473
	.byte	0x15
	.2byte	0x3f5
	.4byte	0xafc
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x15
	.4byte	.LASF474
	.byte	0x15
	.2byte	0x3fa
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0xf
	.4byte	.LASF475
	.byte	0x15
	.2byte	0x401
	.4byte	0x20ec
	.uleb128 0x10
	.byte	0x30
	.byte	0x15
	.2byte	0x404
	.4byte	0x21cf
	.uleb128 0x16
	.ascii	"rip\000"
	.byte	0x15
	.2byte	0x406
	.4byte	0x218c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF476
	.byte	0x15
	.2byte	0x409
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF477
	.byte	0x15
	.2byte	0x40c
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.byte	0
	.uleb128 0xf
	.4byte	.LASF478
	.byte	0x15
	.2byte	0x40e
	.4byte	0x2198
	.uleb128 0x1c
	.2byte	0x3790
	.byte	0x15
	.2byte	0x418
	.4byte	0x2658
	.uleb128 0x15
	.4byte	.LASF205
	.byte	0x15
	.2byte	0x420
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.ascii	"rip\000"
	.byte	0x15
	.2byte	0x425
	.4byte	0x20e0
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF479
	.byte	0x15
	.2byte	0x42f
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x15
	.4byte	.LASF209
	.byte	0x15
	.2byte	0x442
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x15
	.4byte	.LASF480
	.byte	0x15
	.2byte	0x44e
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x15
	.4byte	.LASF211
	.byte	0x15
	.2byte	0x458
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x15
	.4byte	.LASF212
	.byte	0x15
	.2byte	0x473
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x15
	.4byte	.LASF213
	.byte	0x15
	.2byte	0x47d
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x15
	.4byte	.LASF214
	.byte	0x15
	.2byte	0x499
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x15
	.4byte	.LASF215
	.byte	0x15
	.2byte	0x49d
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x15
	.4byte	.LASF216
	.byte	0x15
	.2byte	0x49f
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x15
	.4byte	.LASF217
	.byte	0x15
	.2byte	0x4a9
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x15
	.4byte	.LASF218
	.byte	0x15
	.2byte	0x4ad
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x15
	.4byte	.LASF219
	.byte	0x15
	.2byte	0x4af
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x15
	.4byte	.LASF481
	.byte	0x15
	.2byte	0x4b3
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x15
	.4byte	.LASF482
	.byte	0x15
	.2byte	0x4b5
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x15
	.4byte	.LASF220
	.byte	0x15
	.2byte	0x4b7
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x15
	.4byte	.LASF483
	.byte	0x15
	.2byte	0x4bc
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x15
	.4byte	.LASF484
	.byte	0x15
	.2byte	0x4be
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x15
	.4byte	.LASF221
	.byte	0x15
	.2byte	0x4c1
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x15
	.4byte	.LASF222
	.byte	0x15
	.2byte	0x4c3
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x15
	.4byte	.LASF223
	.byte	0x15
	.2byte	0x4cc
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x15
	.4byte	.LASF224
	.byte	0x15
	.2byte	0x4cf
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x15
	.4byte	.LASF485
	.byte	0x15
	.2byte	0x4d1
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x15
	.4byte	.LASF225
	.byte	0x15
	.2byte	0x4d9
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x15
	.4byte	.LASF226
	.byte	0x15
	.2byte	0x4e3
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x15
	.4byte	.LASF227
	.byte	0x15
	.2byte	0x4e5
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x15
	.4byte	.LASF228
	.byte	0x15
	.2byte	0x4e9
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0x15
	.4byte	.LASF229
	.byte	0x15
	.2byte	0x4eb
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0x15
	.4byte	.LASF230
	.byte	0x15
	.2byte	0x4ed
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xbc
	.uleb128 0x15
	.4byte	.LASF231
	.byte	0x15
	.2byte	0x4f4
	.4byte	0x1000
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x15
	.4byte	.LASF486
	.byte	0x15
	.2byte	0x4fe
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x15
	.4byte	.LASF232
	.byte	0x15
	.2byte	0x504
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0x15
	.4byte	.LASF487
	.byte	0x15
	.2byte	0x50c
	.4byte	0x2658
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0x15
	.4byte	.LASF488
	.byte	0x15
	.2byte	0x512
	.4byte	0xafc
	.byte	0x3
	.byte	0x23
	.uleb128 0x128
	.uleb128 0x15
	.4byte	.LASF489
	.byte	0x15
	.2byte	0x515
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x12c
	.uleb128 0x15
	.4byte	.LASF490
	.byte	0x15
	.2byte	0x519
	.4byte	0xafc
	.byte	0x3
	.byte	0x23
	.uleb128 0x130
	.uleb128 0x15
	.4byte	.LASF491
	.byte	0x15
	.2byte	0x51e
	.4byte	0xafc
	.byte	0x3
	.byte	0x23
	.uleb128 0x134
	.uleb128 0x15
	.4byte	.LASF492
	.byte	0x15
	.2byte	0x524
	.4byte	0x2668
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.uleb128 0x15
	.4byte	.LASF493
	.byte	0x15
	.2byte	0x52b
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1b0
	.uleb128 0x15
	.4byte	.LASF494
	.byte	0x15
	.2byte	0x536
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1b4
	.uleb128 0x15
	.4byte	.LASF495
	.byte	0x15
	.2byte	0x538
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1b8
	.uleb128 0x15
	.4byte	.LASF496
	.byte	0x15
	.2byte	0x53e
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1bc
	.uleb128 0x15
	.4byte	.LASF497
	.byte	0x15
	.2byte	0x54a
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c0
	.uleb128 0x15
	.4byte	.LASF498
	.byte	0x15
	.2byte	0x54c
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c4
	.uleb128 0x15
	.4byte	.LASF499
	.byte	0x15
	.2byte	0x555
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c8
	.uleb128 0x15
	.4byte	.LASF500
	.byte	0x15
	.2byte	0x55f
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1cc
	.uleb128 0x16
	.ascii	"sbf\000"
	.byte	0x15
	.2byte	0x566
	.4byte	0x657
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d0
	.uleb128 0x15
	.4byte	.LASF501
	.byte	0x15
	.2byte	0x573
	.4byte	0x160e
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d8
	.uleb128 0x15
	.4byte	.LASF502
	.byte	0x15
	.2byte	0x578
	.4byte	0x1fbc
	.byte	0x3
	.byte	0x23
	.uleb128 0x1f4
	.uleb128 0x15
	.4byte	.LASF503
	.byte	0x15
	.2byte	0x57b
	.4byte	0x1fbc
	.byte	0x3
	.byte	0x23
	.uleb128 0x204
	.uleb128 0x15
	.4byte	.LASF504
	.byte	0x15
	.2byte	0x57f
	.4byte	0x2678
	.byte	0x3
	.byte	0x23
	.uleb128 0x214
	.uleb128 0x15
	.4byte	.LASF505
	.byte	0x15
	.2byte	0x581
	.4byte	0x2689
	.byte	0x3
	.byte	0x23
	.uleb128 0x253c
	.uleb128 0x15
	.4byte	.LASF506
	.byte	0x15
	.2byte	0x588
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x36d0
	.uleb128 0x15
	.4byte	.LASF507
	.byte	0x15
	.2byte	0x58a
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x36d4
	.uleb128 0x15
	.4byte	.LASF508
	.byte	0x15
	.2byte	0x58c
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x36d8
	.uleb128 0x15
	.4byte	.LASF509
	.byte	0x15
	.2byte	0x58e
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x36dc
	.uleb128 0x15
	.4byte	.LASF510
	.byte	0x15
	.2byte	0x590
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x36e0
	.uleb128 0x15
	.4byte	.LASF511
	.byte	0x15
	.2byte	0x592
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x36e4
	.uleb128 0x15
	.4byte	.LASF512
	.byte	0x15
	.2byte	0x597
	.4byte	0x791
	.byte	0x3
	.byte	0x23
	.uleb128 0x36e8
	.uleb128 0x15
	.4byte	.LASF513
	.byte	0x15
	.2byte	0x599
	.4byte	0x1000
	.byte	0x3
	.byte	0x23
	.uleb128 0x36f4
	.uleb128 0x15
	.4byte	.LASF514
	.byte	0x15
	.2byte	0x59b
	.4byte	0x1000
	.byte	0x3
	.byte	0x23
	.uleb128 0x3704
	.uleb128 0x15
	.4byte	.LASF515
	.byte	0x15
	.2byte	0x5a0
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x3714
	.uleb128 0x15
	.4byte	.LASF516
	.byte	0x15
	.2byte	0x5a2
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x3718
	.uleb128 0x15
	.4byte	.LASF517
	.byte	0x15
	.2byte	0x5a4
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x371c
	.uleb128 0x15
	.4byte	.LASF518
	.byte	0x15
	.2byte	0x5aa
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x3720
	.uleb128 0x15
	.4byte	.LASF519
	.byte	0x15
	.2byte	0x5b1
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x3724
	.uleb128 0x15
	.4byte	.LASF520
	.byte	0x15
	.2byte	0x5b3
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x3728
	.uleb128 0x15
	.4byte	.LASF521
	.byte	0x15
	.2byte	0x5b7
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x372c
	.uleb128 0x15
	.4byte	.LASF522
	.byte	0x15
	.2byte	0x5be
	.4byte	0x21cf
	.byte	0x3
	.byte	0x23
	.uleb128 0x3730
	.uleb128 0x15
	.4byte	.LASF523
	.byte	0x15
	.2byte	0x5c8
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x3760
	.uleb128 0x15
	.4byte	.LASF419
	.byte	0x15
	.2byte	0x5cf
	.4byte	0x269a
	.byte	0x3
	.byte	0x23
	.uleb128 0x3764
	.byte	0
	.uleb128 0x6
	.4byte	0xafc
	.4byte	0x2668
	.uleb128 0x7
	.4byte	0x30
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.4byte	0xafc
	.4byte	0x2678
	.uleb128 0x7
	.4byte	0x30
	.byte	0x1d
	.byte	0
	.uleb128 0x6
	.4byte	0xca
	.4byte	0x2689
	.uleb128 0x1b
	.4byte	0x30
	.2byte	0x1193
	.byte	0
	.uleb128 0x6
	.4byte	0xb4
	.4byte	0x269a
	.uleb128 0x1b
	.4byte	0x30
	.2byte	0x1193
	.byte	0
	.uleb128 0x6
	.4byte	0xd5
	.4byte	0x26aa
	.uleb128 0x7
	.4byte	0x30
	.byte	0xa
	.byte	0
	.uleb128 0xf
	.4byte	.LASF524
	.byte	0x15
	.2byte	0x5d6
	.4byte	0x21db
	.uleb128 0x1c
	.2byte	0xde50
	.byte	0x15
	.2byte	0x5d8
	.4byte	0x26df
	.uleb128 0x15
	.4byte	.LASF390
	.byte	0x15
	.2byte	0x5df
	.4byte	0x8dd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF525
	.byte	0x15
	.2byte	0x5e4
	.4byte	0x26df
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x6
	.4byte	0x26aa
	.4byte	0x26ef
	.uleb128 0x7
	.4byte	0x30
	.byte	0x3
	.byte	0
	.uleb128 0xf
	.4byte	.LASF526
	.byte	0x15
	.2byte	0x5eb
	.4byte	0x26b6
	.uleb128 0x3
	.byte	0x8
	.byte	0x4
	.4byte	.LASF527
	.uleb128 0x17
	.byte	0x4
	.4byte	0x26aa
	.uleb128 0x10
	.byte	0x5c
	.byte	0x15
	.2byte	0x7c7
	.4byte	0x273f
	.uleb128 0x15
	.4byte	.LASF528
	.byte	0x15
	.2byte	0x7cf
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF529
	.byte	0x15
	.2byte	0x7d6
	.4byte	0x1298
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF419
	.byte	0x15
	.2byte	0x7df
	.4byte	0x1000
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.byte	0
	.uleb128 0xf
	.4byte	.LASF530
	.byte	0x15
	.2byte	0x7e6
	.4byte	0x2708
	.uleb128 0x1c
	.2byte	0x460
	.byte	0x15
	.2byte	0x7f0
	.4byte	0x2774
	.uleb128 0x15
	.4byte	.LASF390
	.byte	0x15
	.2byte	0x7f7
	.4byte	0x8dd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF531
	.byte	0x15
	.2byte	0x7fd
	.4byte	0x2774
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x6
	.4byte	0x273f
	.4byte	0x2784
	.uleb128 0x7
	.4byte	0x30
	.byte	0xb
	.byte	0
	.uleb128 0xf
	.4byte	.LASF532
	.byte	0x15
	.2byte	0x804
	.4byte	0x274b
	.uleb128 0x10
	.byte	0x60
	.byte	0x15
	.2byte	0x812
	.4byte	0x28d5
	.uleb128 0x15
	.4byte	.LASF533
	.byte	0x15
	.2byte	0x814
	.4byte	0xada
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF534
	.byte	0x15
	.2byte	0x816
	.4byte	0xada
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x15
	.4byte	.LASF535
	.byte	0x15
	.2byte	0x820
	.4byte	0xada
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x15
	.4byte	.LASF536
	.byte	0x15
	.2byte	0x823
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x16
	.ascii	"bsr\000"
	.byte	0x15
	.2byte	0x82d
	.4byte	0x2702
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF537
	.byte	0x15
	.2byte	0x839
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x15
	.4byte	.LASF250
	.byte	0x15
	.2byte	0x841
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x15
	.4byte	.LASF538
	.byte	0x15
	.2byte	0x847
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x15
	.4byte	.LASF539
	.byte	0x15
	.2byte	0x849
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x15
	.4byte	.LASF255
	.byte	0x15
	.2byte	0x84b
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x15
	.4byte	.LASF540
	.byte	0x15
	.2byte	0x84e
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x15
	.4byte	.LASF258
	.byte	0x15
	.2byte	0x854
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x16
	.ascii	"bbf\000"
	.byte	0x15
	.2byte	0x85a
	.4byte	0x368
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x15
	.4byte	.LASF260
	.byte	0x15
	.2byte	0x85c
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x15
	.4byte	.LASF541
	.byte	0x15
	.2byte	0x85f
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x52
	.uleb128 0x15
	.4byte	.LASF542
	.byte	0x15
	.2byte	0x863
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x15
	.4byte	.LASF543
	.byte	0x15
	.2byte	0x86b
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x56
	.uleb128 0x15
	.4byte	.LASF259
	.byte	0x15
	.2byte	0x872
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x15
	.4byte	.LASF271
	.byte	0x15
	.2byte	0x875
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x5a
	.uleb128 0x15
	.4byte	.LASF246
	.byte	0x15
	.2byte	0x87d
	.4byte	0xb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x5b
	.uleb128 0x15
	.4byte	.LASF544
	.byte	0x15
	.2byte	0x886
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.byte	0
	.uleb128 0xf
	.4byte	.LASF545
	.byte	0x15
	.2byte	0x88d
	.4byte	0x2790
	.uleb128 0x1a
	.4byte	0x12140
	.byte	0x15
	.2byte	0x892
	.4byte	0x29bb
	.uleb128 0x15
	.4byte	.LASF390
	.byte	0x15
	.2byte	0x899
	.4byte	0x8dd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF546
	.byte	0x15
	.2byte	0x8a0
	.4byte	0xa85
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF547
	.byte	0x15
	.2byte	0x8a6
	.4byte	0xa85
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x15
	.4byte	.LASF548
	.byte	0x15
	.2byte	0x8b0
	.4byte	0xa85
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x15
	.4byte	.LASF549
	.byte	0x15
	.2byte	0x8be
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x15
	.4byte	.LASF550
	.byte	0x15
	.2byte	0x8c8
	.4byte	0xd77
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x15
	.4byte	.LASF551
	.byte	0x15
	.2byte	0x8cc
	.4byte	0x92
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x15
	.4byte	.LASF552
	.byte	0x15
	.2byte	0x8ce
	.4byte	0x92
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x15
	.4byte	.LASF553
	.byte	0x15
	.2byte	0x8d4
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x15
	.4byte	.LASF554
	.byte	0x15
	.2byte	0x8de
	.4byte	0x29bb
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x15
	.4byte	.LASF555
	.byte	0x15
	.2byte	0x8e2
	.4byte	0xfd
	.byte	0x4
	.byte	0x23
	.uleb128 0x1208c
	.uleb128 0x15
	.4byte	.LASF556
	.byte	0x15
	.2byte	0x8e4
	.4byte	0x29cc
	.byte	0x4
	.byte	0x23
	.uleb128 0x12090
	.uleb128 0x15
	.4byte	.LASF419
	.byte	0x15
	.2byte	0x8ed
	.4byte	0x7a1
	.byte	0x4
	.byte	0x23
	.uleb128 0x120c0
	.byte	0
	.uleb128 0x6
	.4byte	0x28d5
	.4byte	0x29cc
	.uleb128 0x1b
	.4byte	0x30
	.2byte	0x2ff
	.byte	0
	.uleb128 0x6
	.4byte	0x916
	.4byte	0x29dc
	.uleb128 0x7
	.4byte	0x30
	.byte	0xb
	.byte	0
	.uleb128 0xf
	.4byte	.LASF557
	.byte	0x15
	.2byte	0x8f4
	.4byte	0x28e1
	.uleb128 0x6
	.4byte	0xfd
	.4byte	0x29f8
	.uleb128 0x7
	.4byte	0x30
	.byte	0xb
	.byte	0
	.uleb128 0x19
	.2byte	0x238
	.byte	0x16
	.byte	0x4a
	.4byte	0x2a3c
	.uleb128 0x13
	.4byte	.LASF246
	.byte	0x16
	.byte	0x4c
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF132
	.byte	0x16
	.byte	0x4e
	.4byte	0x2a3c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF202
	.byte	0x16
	.byte	0x50
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0x204
	.uleb128 0x13
	.4byte	.LASF558
	.byte	0x16
	.byte	0x54
	.4byte	0x6c3
	.byte	0x3
	.byte	0x23
	.uleb128 0x208
	.byte	0
	.uleb128 0x6
	.4byte	0xe7
	.4byte	0x2a4c
	.uleb128 0x7
	.4byte	0x30
	.byte	0x7f
	.byte	0
	.uleb128 0x2
	.4byte	.LASF559
	.byte	0x16
	.byte	0x56
	.4byte	0x29f8
	.uleb128 0x8
	.byte	0x14
	.byte	0x17
	.byte	0x9c
	.4byte	0x2aa6
	.uleb128 0x13
	.4byte	.LASF286
	.byte	0x17
	.byte	0xa2
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF246
	.byte	0x17
	.byte	0xa8
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF363
	.byte	0x17
	.byte	0xaa
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF560
	.byte	0x17
	.byte	0xac
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF561
	.byte	0x17
	.byte	0xae
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x2
	.4byte	.LASF562
	.byte	0x17
	.byte	0xb0
	.4byte	0x2a57
	.uleb128 0x8
	.byte	0x18
	.byte	0x17
	.byte	0xbe
	.4byte	0x2b0e
	.uleb128 0x13
	.4byte	.LASF286
	.byte	0x17
	.byte	0xc0
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF563
	.byte	0x17
	.byte	0xc4
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF246
	.byte	0x17
	.byte	0xc8
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF363
	.byte	0x17
	.byte	0xca
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF461
	.byte	0x17
	.byte	0xcc
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x13
	.4byte	.LASF564
	.byte	0x17
	.byte	0xd0
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x2
	.4byte	.LASF565
	.byte	0x17
	.byte	0xd2
	.4byte	0x2ab1
	.uleb128 0x8
	.byte	0x14
	.byte	0x17
	.byte	0xd8
	.4byte	0x2b68
	.uleb128 0x13
	.4byte	.LASF286
	.byte	0x17
	.byte	0xda
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF566
	.byte	0x17
	.byte	0xde
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x13
	.4byte	.LASF567
	.byte	0x17
	.byte	0xe0
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.4byte	.LASF568
	.byte	0x17
	.byte	0xe4
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x13
	.4byte	.LASF205
	.byte	0x17
	.byte	0xe8
	.4byte	0xd5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x2
	.4byte	.LASF569
	.byte	0x17
	.byte	0xea
	.4byte	0x2b19
	.uleb128 0x2
	.4byte	.LASF570
	.byte	0x18
	.byte	0x7a
	.4byte	0xb4
	.uleb128 0x8
	.byte	0xf0
	.byte	0x19
	.byte	0x21
	.4byte	0x2c61
	.uleb128 0x13
	.4byte	.LASF571
	.byte	0x19
	.byte	0x27
	.4byte	0x2aa6
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.4byte	.LASF572
	.byte	0x19
	.byte	0x2e
	.4byte	0x2b0e
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x13
	.4byte	.LASF573
	.byte	0x19
	.byte	0x32
	.4byte	0x1448
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x13
	.4byte	.LASF574
	.byte	0x19
	.byte	0x3d
	.4byte	0x147c
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x13
	.4byte	.LASF575
	.byte	0x19
	.byte	0x43
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x13
	.4byte	.LASF576
	.byte	0x19
	.byte	0x45
	.4byte	0x13f6
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x13
	.4byte	.LASF577
	.byte	0x19
	.byte	0x50
	.4byte	0x29e8
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x13
	.4byte	.LASF578
	.byte	0x19
	.byte	0x58
	.4byte	0x29e8
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x13
	.4byte	.LASF579
	.byte	0x19
	.byte	0x60
	.4byte	0x2c61
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x13
	.4byte	.LASF580
	.byte	0x19
	.byte	0x67
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0x13
	.4byte	.LASF581
	.byte	0x19
	.byte	0x6c
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x13
	.4byte	.LASF582
	.byte	0x19
	.byte	0x72
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x13
	.4byte	.LASF583
	.byte	0x19
	.byte	0x76
	.4byte	0x2b68
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0x13
	.4byte	.LASF584
	.byte	0x19
	.byte	0x7c
	.4byte	0xfd
	.byte	0x3
	.byte	0x23
	.uleb128 0xe8
	.uleb128 0x13
	.4byte	.LASF585
	.byte	0x19
	.byte	0x7e
	.4byte	0xd5
	.byte	0x3
	.byte	0x23
	.uleb128 0xec
	.byte	0
	.uleb128 0x6
	.4byte	0xbf
	.4byte	0x2c71
	.uleb128 0x7
	.4byte	0x30
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.4byte	.LASF586
	.byte	0x19
	.byte	0x80
	.4byte	0x2b7e
	.uleb128 0x10
	.byte	0x64
	.byte	0x1
	.2byte	0xbcc
	.4byte	0x2dfd
	.uleb128 0x15
	.4byte	.LASF587
	.byte	0x1
	.2byte	0xbd1
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x15
	.4byte	.LASF588
	.byte	0x1
	.2byte	0xbd7
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x15
	.4byte	.LASF589
	.byte	0x1
	.2byte	0xbde
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x15
	.4byte	.LASF590
	.byte	0x1
	.2byte	0xbe6
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x15
	.4byte	.LASF48
	.byte	0x1
	.2byte	0xbee
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x15
	.4byte	.LASF591
	.byte	0x1
	.2byte	0xbef
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x15
	.4byte	.LASF49
	.byte	0x1
	.2byte	0xbf6
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x15
	.4byte	.LASF592
	.byte	0x1
	.2byte	0xbf7
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x15
	.4byte	.LASF50
	.byte	0x1
	.2byte	0xbfe
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x15
	.4byte	.LASF593
	.byte	0x1
	.2byte	0xbff
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x15
	.4byte	.LASF51
	.byte	0x1
	.2byte	0xc06
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x15
	.4byte	.LASF594
	.byte	0x1
	.2byte	0xc07
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x15
	.4byte	.LASF52
	.byte	0x1
	.2byte	0xc0a
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x15
	.4byte	.LASF595
	.byte	0x1
	.2byte	0xc0b
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x15
	.4byte	.LASF53
	.byte	0x1
	.2byte	0xc0e
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x15
	.4byte	.LASF596
	.byte	0x1
	.2byte	0xc0f
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x15
	.4byte	.LASF597
	.byte	0x1
	.2byte	0xc14
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x15
	.4byte	.LASF598
	.byte	0x1
	.2byte	0xc15
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x15
	.4byte	.LASF599
	.byte	0x1
	.2byte	0xc16
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x15
	.4byte	.LASF47
	.byte	0x1
	.2byte	0xc1d
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x15
	.4byte	.LASF600
	.byte	0x1
	.2byte	0xc1e
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x15
	.4byte	.LASF601
	.byte	0x1
	.2byte	0xc25
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x15
	.4byte	.LASF602
	.byte	0x1
	.2byte	0xc29
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x15
	.4byte	.LASF603
	.byte	0x1
	.2byte	0xc2f
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x15
	.4byte	.LASF604
	.byte	0x1
	.2byte	0xc30
	.4byte	0xfd
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.byte	0
	.uleb128 0xf
	.4byte	.LASF605
	.byte	0x1
	.2byte	0xc34
	.4byte	0x2c7c
	.uleb128 0x1d
	.4byte	.LASF606
	.byte	0x1
	.byte	0x4c
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x2e30
	.uleb128 0x1e
	.4byte	.LASF608
	.byte	0x1
	.byte	0x4c
	.4byte	0x92
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1d
	.4byte	.LASF607
	.byte	0x1
	.byte	0x55
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x2e57
	.uleb128 0x1e
	.4byte	.LASF608
	.byte	0x1
	.byte	0x55
	.4byte	0x92
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF612
	.byte	0x1
	.byte	0x75
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x2ebb
	.uleb128 0x1e
	.4byte	.LASF609
	.byte	0x1
	.byte	0x75
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x20
	.ascii	"i\000"
	.byte	0x1
	.byte	0x81
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x22
	.4byte	.LASF610
	.byte	0x1
	.byte	0xd3
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0x22
	.4byte	.LASF611
	.byte	0x1
	.byte	0xda
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0x28d5
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF838
	.byte	0x1
	.2byte	0x126
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF613
	.byte	0x1
	.2byte	0x151
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x2f01
	.uleb128 0x25
	.4byte	.LASF614
	.byte	0x1
	.2byte	0x151
	.4byte	0x2f01
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x26
	.4byte	0xfd
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF649
	.byte	0x1
	.2byte	0x1b4
	.byte	0x1
	.4byte	0xd5
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x2fb0
	.uleb128 0x25
	.4byte	.LASF615
	.byte	0x1
	.2byte	0x1b4
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x25
	.4byte	.LASF616
	.byte	0x1
	.2byte	0x1b4
	.4byte	0x2fb0
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1b6
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF617
	.byte	0x1
	.2byte	0x1c6
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -26
	.uleb128 0x29
	.4byte	.LASF618
	.byte	0x1
	.2byte	0x1c8
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LBB4
	.4byte	.LBE4
	.uleb128 0x29
	.4byte	.LASF619
	.byte	0x1
	.2byte	0x1e5
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.ascii	"ucp\000"
	.byte	0x1
	.2byte	0x1e7
	.4byte	0xae5
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x21
	.4byte	.LBB5
	.4byte	.LBE5
	.uleb128 0x28
	.ascii	"sds\000"
	.byte	0x1
	.2byte	0x20b
	.4byte	0x1316
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0xae5
	.uleb128 0x2a
	.4byte	.LASF624
	.byte	0x1
	.2byte	0x23f
	.byte	0x1
	.4byte	0xd5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x3045
	.uleb128 0x25
	.4byte	.LASF620
	.byte	0x1
	.2byte	0x23f
	.4byte	0x2f01
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x25
	.4byte	.LASF621
	.byte	0x1
	.2byte	0x23f
	.4byte	0x3045
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x29
	.4byte	.LASF622
	.byte	0x1
	.2byte	0x241
	.4byte	0x304a
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x29
	.4byte	.LASF623
	.byte	0x1
	.2byte	0x243
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.ascii	"t\000"
	.byte	0x1
	.2byte	0x245
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.ascii	"c\000"
	.byte	0x1
	.2byte	0x245
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.ascii	"s\000"
	.byte	0x1
	.2byte	0x245
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x245
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x26
	.4byte	0x45
	.uleb128 0x17
	.byte	0x4
	.4byte	0x122c
	.uleb128 0x2a
	.4byte	.LASF625
	.byte	0x1
	.2byte	0x297
	.byte	0x1
	.4byte	0xd5
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x308b
	.uleb128 0x25
	.4byte	.LASF626
	.byte	0x1
	.2byte	0x297
	.4byte	0x308b
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x299
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x26
	.4byte	0x3090
	.uleb128 0x17
	.byte	0x4
	.4byte	0x3096
	.uleb128 0x26
	.4byte	0x368
	.uleb128 0x2a
	.4byte	.LASF627
	.byte	0x1
	.2byte	0x303
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x319b
	.uleb128 0x25
	.4byte	.LASF620
	.byte	0x1
	.2byte	0x303
	.4byte	0x2f01
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x25
	.4byte	.LASF628
	.byte	0x1
	.2byte	0x303
	.4byte	0x3045
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x25
	.4byte	.LASF629
	.byte	0x1
	.2byte	0x303
	.4byte	0x3045
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x29
	.4byte	.LASF630
	.byte	0x1
	.2byte	0x305
	.4byte	0xb4
	.byte	0x2
	.byte	0x91
	.sleb128 -13
	.uleb128 0x29
	.4byte	.LASF631
	.byte	0x1
	.2byte	0x305
	.4byte	0xb4
	.byte	0x2
	.byte	0x91
	.sleb128 -14
	.uleb128 0x29
	.4byte	.LASF632
	.byte	0x1
	.2byte	0x307
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x29
	.4byte	.LASF633
	.byte	0x1
	.2byte	0x307
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x29
	.4byte	.LASF634
	.byte	0x1
	.2byte	0x309
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x29
	.4byte	.LASF635
	.byte	0x1
	.2byte	0x309
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF636
	.byte	0x1
	.2byte	0x30b
	.4byte	0x304a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x29
	.4byte	.LASF637
	.byte	0x1
	.2byte	0x30b
	.4byte	0x304a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x29
	.4byte	.LASF638
	.byte	0x1
	.2byte	0x30d
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x29
	.4byte	.LASF639
	.byte	0x1
	.2byte	0x30d
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x29
	.4byte	.LASF640
	.byte	0x1
	.2byte	0x30f
	.4byte	0x319b
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x29
	.4byte	.LASF641
	.byte	0x1
	.2byte	0x30f
	.4byte	0x319b
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0x368
	.uleb128 0x2a
	.4byte	.LASF642
	.byte	0x1
	.2byte	0x3ea
	.byte	0x1
	.4byte	0x45
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x31fb
	.uleb128 0x25
	.4byte	.LASF620
	.byte	0x1
	.2byte	0x3ea
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x25
	.4byte	.LASF643
	.byte	0x1
	.2byte	0x3ea
	.4byte	0xad4
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x25
	.4byte	.LASF644
	.byte	0x1
	.2byte	0x3ea
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF645
	.byte	0x1
	.2byte	0x3f4
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF646
	.byte	0x1
	.2byte	0x421
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x3243
	.uleb128 0x25
	.4byte	.LASF620
	.byte	0x1
	.2byte	0x421
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x25
	.4byte	.LASF647
	.byte	0x1
	.2byte	0x421
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x29
	.4byte	.LASF648
	.byte	0x1
	.2byte	0x426
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF650
	.byte	0x1
	.2byte	0x48a
	.byte	0x1
	.4byte	0x2ebb
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x32c5
	.uleb128 0x25
	.4byte	.LASF651
	.byte	0x1
	.2byte	0x48a
	.4byte	0xa90
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x25
	.4byte	.LASF652
	.byte	0x1
	.2byte	0x48a
	.4byte	0x32c5
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x48a
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x48c
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF654
	.byte	0x1
	.2byte	0x48e
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LBB6
	.4byte	.LBE6
	.uleb128 0x29
	.4byte	.LASF655
	.byte	0x1
	.2byte	0x4c1
	.4byte	0x1598
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x26
	.4byte	0x2ebb
	.uleb128 0x26
	.4byte	0xd5
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF656
	.byte	0x1
	.2byte	0x69b
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x3308
	.uleb128 0x25
	.4byte	.LASF657
	.byte	0x1
	.2byte	0x69b
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.ascii	"sss\000"
	.byte	0x1
	.2byte	0x6a2
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF658
	.byte	0x1
	.2byte	0x6c6
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x3340
	.uleb128 0x25
	.4byte	.LASF659
	.byte	0x1
	.2byte	0x6c6
	.4byte	0x3340
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x6c6
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x26
	.4byte	0x3345
	.uleb128 0x17
	.byte	0x4
	.4byte	0x334b
	.uleb128 0x26
	.4byte	0x28d5
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF660
	.byte	0x1
	.2byte	0x758
	.byte	0x1
	.4byte	0x2ebb
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x33d2
	.uleb128 0x25
	.4byte	.LASF651
	.byte	0x1
	.2byte	0x758
	.4byte	0xa90
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x25
	.4byte	.LASF652
	.byte	0x1
	.2byte	0x758
	.4byte	0x32c5
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x758
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x29
	.4byte	.LASF661
	.byte	0x1
	.2byte	0x761
	.4byte	0x33d2
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x767
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LBB7
	.4byte	.LBE7
	.uleb128 0x29
	.4byte	.LASF662
	.byte	0x1
	.2byte	0x791
	.4byte	0x33dc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x2c
	.4byte	0x33d7
	.uleb128 0x26
	.4byte	0xafc
	.uleb128 0x17
	.byte	0x4
	.4byte	0xaeb
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF663
	.byte	0x1
	.2byte	0x835
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x3461
	.uleb128 0x25
	.4byte	.LASF664
	.byte	0x1
	.2byte	0x835
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x25
	.4byte	.LASF665
	.byte	0x1
	.2byte	0x835
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x835
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x29
	.4byte	.LASF666
	.byte	0x1
	.2byte	0x851
	.4byte	0x2702
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF610
	.byte	0x1
	.2byte	0x857
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LBB8
	.4byte	.LBE8
	.uleb128 0x29
	.4byte	.LASF667
	.byte	0x1
	.2byte	0x894
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF668
	.byte	0x1
	.2byte	0x8b8
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x34b8
	.uleb128 0x25
	.4byte	.LASF669
	.byte	0x1
	.2byte	0x8b8
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x8b8
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF670
	.byte	0x1
	.2byte	0x8be
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x29
	.4byte	.LASF610
	.byte	0x1
	.2byte	0x8d4
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF671
	.byte	0x1
	.2byte	0x914
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x352d
	.uleb128 0x25
	.4byte	.LASF664
	.byte	0x1
	.2byte	0x914
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x25
	.4byte	.LASF665
	.byte	0x1
	.2byte	0x914
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x25
	.4byte	.LASF672
	.byte	0x1
	.2byte	0x914
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x914
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x29
	.4byte	.LASF670
	.byte	0x1
	.2byte	0x91c
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF610
	.byte	0x1
	.2byte	0x932
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF673
	.byte	0x1
	.2byte	0x95c
	.byte	0x1
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0x3575
	.uleb128 0x25
	.4byte	.LASF669
	.byte	0x1
	.2byte	0x95c
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x95c
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x29
	.4byte	.LASF610
	.byte	0x1
	.2byte	0x95e
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF674
	.byte	0x1
	.2byte	0x98d
	.byte	0x1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0x35ae
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x98d
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.ascii	"iii\000"
	.byte	0x1
	.2byte	0x98f
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF675
	.byte	0x1
	.2byte	0x9b3
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.4byte	0x3635
	.uleb128 0x25
	.4byte	.LASF676
	.byte	0x1
	.2byte	0x9b3
	.4byte	0x3635
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x25
	.4byte	.LASF677
	.byte	0x1
	.2byte	0x9b3
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x25
	.4byte	.LASF678
	.byte	0x1
	.2byte	0x9b3
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x9b3
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x9b8
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF610
	.byte	0x1
	.2byte	0x9ba
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x29
	.4byte	.LASF667
	.byte	0x1
	.2byte	0x9bc
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x26
	.4byte	0x363a
	.uleb128 0x17
	.byte	0x4
	.4byte	0x3640
	.uleb128 0x26
	.4byte	0x26aa
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF679
	.byte	0x1
	.2byte	0xa10
	.byte	0x1
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0x368d
	.uleb128 0x25
	.4byte	.LASF669
	.byte	0x1
	.2byte	0xa10
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x25
	.4byte	.LASF680
	.byte	0x1
	.2byte	0xa10
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x29
	.4byte	.LASF610
	.byte	0x1
	.2byte	0xa19
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF681
	.byte	0x1
	.2byte	0xa57
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0x3713
	.uleb128 0x25
	.4byte	.LASF682
	.byte	0x1
	.2byte	0xa57
	.4byte	0x2fb0
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0xa59
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF683
	.byte	0x1
	.2byte	0xa60
	.4byte	0xb4
	.byte	0x2
	.byte	0x91
	.sleb128 -17
	.uleb128 0x2d
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x1
	.2byte	0xa69
	.4byte	0xe7
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LBB10
	.4byte	.LBE10
	.uleb128 0x28
	.ascii	"bsr\000"
	.byte	0x1
	.2byte	0xa6d
	.4byte	0x2702
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF684
	.byte	0x1
	.2byte	0xa6f
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF685
	.byte	0x1
	.2byte	0xad7
	.byte	0x1
	.4byte	0xd5
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST23
	.4byte	0x377a
	.uleb128 0x25
	.4byte	.LASF686
	.byte	0x1
	.2byte	0xad7
	.4byte	0x2fb0
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0xad9
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.ascii	"ucp\000"
	.byte	0x1
	.2byte	0xadd
	.4byte	0xae5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x29
	.4byte	.LASF683
	.byte	0x1
	.2byte	0xadd
	.4byte	0xb4
	.byte	0x2
	.byte	0x91
	.sleb128 -17
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x1
	.2byte	0xaef
	.4byte	0xe7
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF687
	.byte	0x1
	.2byte	0xb2a
	.byte	0x1
	.4byte	0xd5
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST24
	.4byte	0x37d4
	.uleb128 0x25
	.4byte	.LASF626
	.byte	0x1
	.2byte	0xb2a
	.4byte	0x319b
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0xb2c
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x29
	.4byte	.LASF688
	.byte	0x1
	.2byte	0xb31
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF689
	.byte	0x1
	.2byte	0xb31
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF690
	.byte	0x1
	.2byte	0xb62
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST25
	.4byte	0x3810
	.uleb128 0x25
	.4byte	.LASF672
	.byte	0x1
	.2byte	0xb62
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0xb64
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x2a
	.4byte	.LASF691
	.byte	0x1
	.2byte	0xb9a
	.byte	0x1
	.4byte	0x2ebb
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST26
	.4byte	0x3849
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0xb9c
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x1
	.2byte	0xb9e
	.4byte	0xe7
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x2a
	.4byte	.LASF692
	.byte	0x1
	.2byte	0xc5c
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST27
	.4byte	0x396a
	.uleb128 0x25
	.4byte	.LASF652
	.byte	0x1
	.2byte	0xc5c
	.4byte	0x32c5
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x25
	.4byte	.LASF693
	.byte	0x1
	.2byte	0xc5d
	.4byte	0x396a
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x25
	.4byte	.LASF694
	.byte	0x1
	.2byte	0xc5e
	.4byte	0x396f
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x25
	.4byte	.LASF695
	.byte	0x1
	.2byte	0xc5f
	.4byte	0x397a
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x29
	.4byte	.LASF696
	.byte	0x1
	.2byte	0xc61
	.4byte	0x33d2
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0xc63
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF697
	.byte	0x1
	.2byte	0xc65
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x29
	.4byte	.LASF698
	.byte	0x1
	.2byte	0xc67
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x2e
	.4byte	.LBB12
	.4byte	.LBE12
	.4byte	0x390b
	.uleb128 0x29
	.4byte	.LASF699
	.byte	0x1
	.2byte	0xcb2
	.4byte	0x6b8
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x29
	.4byte	.LASF700
	.byte	0x1
	.2byte	0xcb5
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x21
	.4byte	.LBB13
	.4byte	.LBE13
	.uleb128 0x29
	.4byte	.LASF618
	.byte	0x1
	.2byte	0xda3
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF701
	.byte	0x1
	.2byte	0xda3
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x29
	.4byte	.LASF702
	.byte	0x1
	.2byte	0xda5
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x29
	.4byte	.LASF703
	.byte	0x1
	.2byte	0xda5
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x21
	.4byte	.LBB14
	.4byte	.LBE14
	.uleb128 0x28
	.ascii	"dt\000"
	.byte	0x1
	.2byte	0xe26
	.4byte	0x6f4
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x26
	.4byte	0x33dc
	.uleb128 0x26
	.4byte	0x3974
	.uleb128 0x17
	.byte	0x4
	.4byte	0x1238
	.uleb128 0x26
	.4byte	0x397f
	.uleb128 0x17
	.byte	0x4
	.4byte	0x2dfd
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF704
	.byte	0x1
	.2byte	0xec0
	.byte	0x1
	.4byte	0xd5
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST28
	.4byte	0x39d0
	.uleb128 0x25
	.4byte	.LASF705
	.byte	0x1
	.2byte	0xec0
	.4byte	0x39d0
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x25
	.4byte	.LASF706
	.byte	0x1
	.2byte	0xec0
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0xec2
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x26
	.4byte	0x39d5
	.uleb128 0x17
	.byte	0x4
	.4byte	0x39db
	.uleb128 0x26
	.4byte	0x6f4
	.uleb128 0x2b
	.4byte	.LASF707
	.byte	0x1
	.2byte	0xef1
	.byte	0x1
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST29
	.4byte	0x3a27
	.uleb128 0x25
	.4byte	.LASF708
	.byte	0x1
	.2byte	0xef1
	.4byte	0x396a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x25
	.4byte	.LASF709
	.byte	0x1
	.2byte	0xef1
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x25
	.4byte	.LASF710
	.byte	0x1
	.2byte	0xef1
	.4byte	0x39d0
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF711
	.byte	0x1
	.2byte	0xf41
	.byte	0x1
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST30
	.4byte	0x3ab9
	.uleb128 0x25
	.4byte	.LASF712
	.byte	0x1
	.2byte	0xf41
	.4byte	0x396a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x25
	.4byte	.LASF709
	.byte	0x1
	.2byte	0xf42
	.4byte	0x32ca
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x25
	.4byte	.LASF710
	.byte	0x1
	.2byte	0xf43
	.4byte	0x39d0
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x25
	.4byte	.LASF713
	.byte	0x1
	.2byte	0xf44
	.4byte	0x3ab9
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x28
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0xf46
	.4byte	0x6f4
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x29
	.4byte	.LASF714
	.byte	0x1
	.2byte	0xf48
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x29
	.4byte	.LASF715
	.byte	0x1
	.2byte	0xf4a
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF716
	.byte	0x1
	.2byte	0xf4c
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0x3974
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF717
	.byte	0x1
	.2byte	0xfca
	.byte	0x1
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST31
	.4byte	0x3d3e
	.uleb128 0x25
	.4byte	.LASF718
	.byte	0x1
	.2byte	0xfca
	.4byte	0x3d3e
	.byte	0x3
	.byte	0x91
	.sleb128 -464
	.uleb128 0x29
	.4byte	.LASF719
	.byte	0x1
	.2byte	0xfd7
	.4byte	0x2dfd
	.byte	0x3
	.byte	0x91
	.sleb128 -232
	.uleb128 0x29
	.4byte	.LASF720
	.byte	0x1
	.2byte	0xfd9
	.4byte	0x2dfd
	.byte	0x3
	.byte	0x91
	.sleb128 -332
	.uleb128 0x29
	.4byte	.LASF721
	.byte	0x1
	.2byte	0xfdb
	.4byte	0x2dfd
	.byte	0x3
	.byte	0x91
	.sleb128 -432
	.uleb128 0x29
	.4byte	.LASF696
	.byte	0x1
	.2byte	0xfdf
	.4byte	0x33d2
	.byte	0x3
	.byte	0x91
	.sleb128 -436
	.uleb128 0x29
	.4byte	.LASF722
	.byte	0x1
	.2byte	0xfe3
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x29
	.4byte	.LASF723
	.byte	0x1
	.2byte	0xfe5
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x29
	.4byte	.LASF724
	.byte	0x1
	.2byte	0xfe7
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x29
	.4byte	.LASF725
	.byte	0x1
	.2byte	0xfe9
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x29
	.4byte	.LASF726
	.byte	0x1
	.2byte	0xfeb
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x28
	.ascii	"mp\000"
	.byte	0x1
	.2byte	0xfed
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x29
	.4byte	.LASF727
	.byte	0x1
	.2byte	0xfef
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x29
	.4byte	.LASF728
	.byte	0x1
	.2byte	0xfef
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x29
	.4byte	.LASF729
	.byte	0x1
	.2byte	0xfef
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -128
	.uleb128 0x29
	.4byte	.LASF538
	.byte	0x1
	.2byte	0xff3
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -440
	.uleb128 0x29
	.4byte	.LASF730
	.byte	0x1
	.2byte	0xff5
	.4byte	0x6f4
	.byte	0x3
	.byte	0x91
	.sleb128 -448
	.uleb128 0x29
	.4byte	.LASF731
	.byte	0x1
	.2byte	0xff9
	.4byte	0x33dc
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x29
	.4byte	.LASF732
	.byte	0x1
	.2byte	0xffb
	.4byte	0x3974
	.byte	0x3
	.byte	0x91
	.sleb128 -452
	.uleb128 0x29
	.4byte	.LASF733
	.byte	0x1
	.2byte	0x1002
	.4byte	0x3d4e
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x29
	.4byte	.LASF734
	.byte	0x1
	.2byte	0x1004
	.4byte	0x3d59
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x29
	.4byte	.LASF716
	.byte	0x1
	.2byte	0x1008
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -116
	.uleb128 0x29
	.4byte	.LASF697
	.byte	0x1
	.2byte	0x100a
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -456
	.uleb128 0x29
	.4byte	.LASF618
	.byte	0x1
	.2byte	0x100c
	.4byte	0x2ebb
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x29
	.4byte	.LASF735
	.byte	0x1
	.2byte	0x100c
	.4byte	0x2ebb
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.uleb128 0x29
	.4byte	.LASF736
	.byte	0x1
	.2byte	0x100e
	.4byte	0xfd
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x1012
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x29
	.4byte	.LASF737
	.byte	0x1
	.2byte	0x1017
	.4byte	0x6b8
	.byte	0x3
	.byte	0x91
	.sleb128 -460
	.uleb128 0x29
	.4byte	.LASF738
	.byte	0x1
	.2byte	0x1019
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -88
	.uleb128 0x29
	.4byte	.LASF739
	.byte	0x1
	.2byte	0x101b
	.4byte	0xafc
	.byte	0x3
	.byte	0x91
	.sleb128 -120
	.uleb128 0x28
	.ascii	"MB\000"
	.byte	0x1
	.2byte	0x101d
	.4byte	0xafc
	.byte	0x3
	.byte	0x91
	.sleb128 -92
	.uleb128 0x28
	.ascii	"MBo\000"
	.byte	0x1
	.2byte	0x101f
	.4byte	0xafc
	.byte	0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x29
	.4byte	.LASF740
	.byte	0x1
	.2byte	0x1022
	.4byte	0xfd
	.byte	0x3
	.byte	0x91
	.sleb128 -100
	.uleb128 0x29
	.4byte	.LASF741
	.byte	0x1
	.2byte	0x1053
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -104
	.uleb128 0x21
	.4byte	.LBB15
	.4byte	.LBE15
	.uleb128 0x29
	.4byte	.LASF742
	.byte	0x1
	.2byte	0x106e
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -112
	.uleb128 0x29
	.4byte	.LASF743
	.byte	0x1
	.2byte	0x106e
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -108
	.uleb128 0x2e
	.4byte	.LBB16
	.4byte	.LBE16
	.4byte	0x3d22
	.uleb128 0x29
	.4byte	.LASF744
	.byte	0x1
	.2byte	0x1286
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -124
	.byte	0
	.uleb128 0x21
	.4byte	.LBB17
	.4byte	.LBE17
	.uleb128 0x29
	.4byte	.LASF744
	.byte	0x1
	.2byte	0x1407
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -132
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x26
	.4byte	0x3d43
	.uleb128 0x17
	.byte	0x4
	.4byte	0x3d49
	.uleb128 0x26
	.4byte	0x786
	.uleb128 0x17
	.byte	0x4
	.4byte	0x3d54
	.uleb128 0x26
	.4byte	0xaeb
	.uleb128 0x17
	.byte	0x4
	.4byte	0x3d5f
	.uleb128 0x26
	.4byte	0x1238
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF745
	.byte	0x1
	.2byte	0x15a0
	.byte	0x1
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST32
	.4byte	0x3dcd
	.uleb128 0x25
	.4byte	.LASF746
	.byte	0x1
	.2byte	0x15a0
	.4byte	0x3dcd
	.byte	0x3
	.byte	0x91
	.sleb128 -120
	.uleb128 0x25
	.4byte	.LASF672
	.byte	0x1
	.2byte	0x15a0
	.4byte	0x32ca
	.byte	0x3
	.byte	0x91
	.sleb128 -124
	.uleb128 0x29
	.4byte	.LASF731
	.byte	0x1
	.2byte	0x15a2
	.4byte	0x33dc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF618
	.byte	0x1
	.2byte	0x15a4
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x29
	.4byte	.LASF747
	.byte	0x1
	.2byte	0x15a6
	.4byte	0x2dfd
	.byte	0x3
	.byte	0x91
	.sleb128 -116
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0x2aa6
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF748
	.byte	0x1
	.2byte	0x15fd
	.byte	0x1
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST33
	.4byte	0x3e59
	.uleb128 0x25
	.4byte	.LASF749
	.byte	0x1
	.2byte	0x15fd
	.4byte	0xd5
	.byte	0x3
	.byte	0x91
	.sleb128 -132
	.uleb128 0x29
	.4byte	.LASF750
	.byte	0x1
	.2byte	0x1601
	.4byte	0x3e59
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x29
	.4byte	.LASF731
	.byte	0x1
	.2byte	0x1603
	.4byte	0x33dc
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.ascii	"iii\000"
	.byte	0x1
	.2byte	0x1605
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF618
	.byte	0x1
	.2byte	0x1607
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF747
	.byte	0x1
	.2byte	0x1609
	.4byte	0x2dfd
	.byte	0x3
	.byte	0x91
	.sleb128 -128
	.uleb128 0x29
	.4byte	.LASF751
	.byte	0x1
	.2byte	0x160b
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0x2a4c
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF752
	.byte	0x1
	.2byte	0x167a
	.byte	0x1
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST34
	.4byte	0x3f13
	.uleb128 0x25
	.4byte	.LASF753
	.byte	0x1
	.2byte	0x167a
	.4byte	0x3f13
	.byte	0x3
	.byte	0x91
	.sleb128 -172
	.uleb128 0x29
	.4byte	.LASF696
	.byte	0x1
	.2byte	0x167c
	.4byte	0x33d2
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x29
	.4byte	.LASF754
	.byte	0x1
	.2byte	0x167e
	.4byte	0x2dfd
	.byte	0x3
	.byte	0x91
	.sleb128 -148
	.uleb128 0x29
	.4byte	.LASF618
	.byte	0x1
	.2byte	0x1680
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x29
	.4byte	.LASF755
	.byte	0x1
	.2byte	0x1682
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF756
	.byte	0x1
	.2byte	0x1686
	.4byte	0x3974
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x29
	.4byte	.LASF351
	.byte	0x1
	.2byte	0x1688
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x29
	.4byte	.LASF757
	.byte	0x1
	.2byte	0x168e
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x1694
	.4byte	0x786
	.byte	0x3
	.byte	0x91
	.sleb128 -168
	.uleb128 0x29
	.4byte	.LASF731
	.byte	0x1
	.2byte	0x16a2
	.4byte	0x33dc
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0x2b0e
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF758
	.byte	0x1
	.2byte	0x1740
	.byte	0x1
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST35
	.4byte	0x3fa2
	.uleb128 0x25
	.4byte	.LASF759
	.byte	0x1
	.2byte	0x1740
	.4byte	0x32ca
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x25
	.4byte	.LASF760
	.byte	0x1
	.2byte	0x1740
	.4byte	0x32ca
	.byte	0x3
	.byte	0x91
	.sleb128 -88
	.uleb128 0x25
	.4byte	.LASF567
	.byte	0x1
	.2byte	0x1740
	.4byte	0x32ca
	.byte	0x3
	.byte	0x91
	.sleb128 -92
	.uleb128 0x25
	.4byte	.LASF761
	.byte	0x1
	.2byte	0x1740
	.4byte	0x3fa2
	.byte	0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x29
	.4byte	.LASF762
	.byte	0x1
	.2byte	0x1742
	.4byte	0x786
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x29
	.4byte	.LASF763
	.byte	0x1
	.2byte	0x1744
	.4byte	0x2702
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF764
	.byte	0x1
	.2byte	0x1746
	.4byte	0x6c3
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.byte	0
	.uleb128 0x26
	.4byte	0x2b73
	.uleb128 0x2b
	.4byte	.LASF765
	.byte	0x1
	.2byte	0x17dd
	.byte	0x1
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST36
	.4byte	0x3fe9
	.uleb128 0x25
	.4byte	.LASF652
	.byte	0x1
	.2byte	0x17dd
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LBB18
	.4byte	.LBE18
	.uleb128 0x28
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x1824
	.4byte	0x6f4
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF766
	.byte	0x1
	.2byte	0x1872
	.byte	0x1
	.4byte	0xd5
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST37
	.4byte	0x4084
	.uleb128 0x25
	.4byte	.LASF616
	.byte	0x1
	.2byte	0x1872
	.4byte	0x2fb0
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1874
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF767
	.byte	0x1
	.2byte	0x1880
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -26
	.uleb128 0x21
	.4byte	.LBB19
	.4byte	.LBE19
	.uleb128 0x29
	.4byte	.LASF619
	.byte	0x1
	.2byte	0x1889
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.ascii	"ucp\000"
	.byte	0x1
	.2byte	0x188b
	.4byte	0xae5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x29
	.4byte	.LASF610
	.byte	0x1
	.2byte	0x189d
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x21
	.4byte	.LBB20
	.4byte	.LBE20
	.uleb128 0x29
	.4byte	.LASF768
	.byte	0x1
	.2byte	0x18a3
	.4byte	0x1386
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF769
	.byte	0x1
	.2byte	0x18ee
	.byte	0x1
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST38
	.4byte	0x40ad
	.uleb128 0x2f
	.ascii	"ilc\000"
	.byte	0x1
	.2byte	0x18ee
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x2a
	.4byte	.LASF770
	.byte	0x1
	.2byte	0x1994
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST39
	.4byte	0x411f
	.uleb128 0x25
	.4byte	.LASF659
	.byte	0x1
	.2byte	0x1994
	.4byte	0x3345
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x199b
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF771
	.byte	0x1
	.2byte	0x199b
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x29
	.4byte	.LASF772
	.byte	0x1
	.2byte	0x199d
	.4byte	0x8ed
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x21
	.4byte	.LBB21
	.4byte	.LBE21
	.uleb128 0x29
	.4byte	.LASF773
	.byte	0x1
	.2byte	0x19a8
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF774
	.byte	0x1
	.2byte	0x19d9
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST40
	.4byte	0x416a
	.uleb128 0x25
	.4byte	.LASF775
	.byte	0x1
	.2byte	0x19d9
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x25
	.4byte	.LASF776
	.byte	0x1
	.2byte	0x19d9
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x19db
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF777
	.byte	0x1
	.2byte	0x19ed
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST41
	.4byte	0x41c2
	.uleb128 0x25
	.4byte	.LASF626
	.byte	0x1
	.2byte	0x19ed
	.4byte	0x319b
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x25
	.4byte	.LASF778
	.byte	0x1
	.2byte	0x19ed
	.4byte	0x41c2
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x19ef
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x19f3
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.4byte	0xd5
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF779
	.byte	0x1
	.2byte	0x1a09
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST42
	.4byte	0x4220
	.uleb128 0x25
	.4byte	.LASF778
	.byte	0x1
	.2byte	0x1a09
	.4byte	0x41c2
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1a0b
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x1a0f
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF780
	.byte	0x1
	.2byte	0x1a0f
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF781
	.byte	0x1
	.2byte	0x1a3a
	.byte	0x1
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST43
	.4byte	0x4268
	.uleb128 0x25
	.4byte	.LASF659
	.byte	0x1
	.2byte	0x1a3a
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x25
	.4byte	.LASF653
	.byte	0x1
	.2byte	0x1a3a
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x29
	.4byte	.LASF782
	.byte	0x1
	.2byte	0x1a3c
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF783
	.byte	0x1
	.2byte	0x1a6a
	.byte	0x1
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST44
	.4byte	0x4291
	.uleb128 0x25
	.4byte	.LASF659
	.byte	0x1
	.2byte	0x1a6a
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x2a
	.4byte	.LASF784
	.byte	0x1
	.2byte	0x1a7c
	.byte	0x1
	.4byte	0xfd
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LLST45
	.4byte	0x42bd
	.uleb128 0x28
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1a7e
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF785
	.byte	0x1
	.2byte	0x1aa6
	.byte	0x1
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST46
	.4byte	0x42f3
	.uleb128 0x28
	.ascii	"s\000"
	.byte	0x1
	.2byte	0x1aa8
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF786
	.byte	0x1
	.2byte	0x1aaa
	.4byte	0x2702
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF787
	.byte	0x1
	.2byte	0x1af7
	.byte	0x1
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST47
	.4byte	0x431c
	.uleb128 0x25
	.4byte	.LASF786
	.byte	0x1
	.2byte	0x1af7
	.4byte	0x2702
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF788
	.byte	0x1
	.2byte	0x1b8a
	.byte	0x1
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LLST48
	.4byte	0x4352
	.uleb128 0x28
	.ascii	"s\000"
	.byte	0x1
	.2byte	0x1b8c
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF789
	.byte	0x1
	.2byte	0x1b8e
	.4byte	0x2702
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF790
	.byte	0x1
	.2byte	0x1baa
	.byte	0x1
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LLST49
	.4byte	0x440d
	.uleb128 0x25
	.4byte	.LASF791
	.byte	0x1
	.2byte	0x1baa
	.4byte	0x39d0
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x28
	.ascii	"ilc\000"
	.byte	0x1
	.2byte	0x1bb1
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x29
	.4byte	.LASF792
	.byte	0x1
	.2byte	0x1bb3
	.4byte	0x6f4
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x29
	.4byte	.LASF793
	.byte	0x1
	.2byte	0x1bb5
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x29
	.4byte	.LASF794
	.byte	0x1
	.2byte	0x1bb7
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x29
	.4byte	.LASF795
	.byte	0x1
	.2byte	0x1bbe
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF796
	.byte	0x1
	.2byte	0x1bbf
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x29
	.4byte	.LASF797
	.byte	0x1
	.2byte	0x1bc0
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x29
	.4byte	.LASF798
	.byte	0x1
	.2byte	0x1bc1
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x21
	.4byte	.LBB22
	.4byte	.LBE22
	.uleb128 0x29
	.4byte	.LASF799
	.byte	0x1
	.2byte	0x1bdc
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF800
	.byte	0x1
	.2byte	0x1de7
	.byte	0x1
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LLST50
	.4byte	0x4435
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x1de9
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF801
	.byte	0x1
	.2byte	0x1ee6
	.byte	0x1
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LLST51
	.4byte	0x44a0
	.uleb128 0x28
	.ascii	"ilc\000"
	.byte	0x1
	.2byte	0x1ee8
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.4byte	.LBB23
	.4byte	.LBE23
	.uleb128 0x29
	.4byte	.LASF802
	.byte	0x1
	.2byte	0x1ef9
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LBB24
	.4byte	.LBE24
	.uleb128 0x29
	.4byte	.LASF803
	.byte	0x1
	.2byte	0x1f02
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x29
	.4byte	.LASF804
	.byte	0x1
	.2byte	0x1f04
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF805
	.byte	0x1
	.2byte	0x1f5e
	.byte	0x1
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LLST52
	.4byte	0x4534
	.uleb128 0x28
	.ascii	"ilc\000"
	.byte	0x1
	.2byte	0x1f60
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x29
	.4byte	.LASF806
	.byte	0x1
	.2byte	0x1f62
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x29
	.4byte	.LASF802
	.byte	0x1
	.2byte	0x1f64
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF772
	.byte	0x1
	.2byte	0x1f66
	.4byte	0x8ed
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x2d
	.4byte	.Ldebug_ranges0+0x18
	.uleb128 0x29
	.4byte	.LASF807
	.byte	0x1
	.2byte	0x1f97
	.4byte	0x2ebb
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x29
	.4byte	.LASF808
	.byte	0x1
	.2byte	0x20e8
	.4byte	0xfd
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x21
	.4byte	.LBB26
	.4byte	.LBE26
	.uleb128 0x29
	.4byte	.LASF809
	.byte	0x1
	.2byte	0x2138
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x24
	.byte	0x1
	.4byte	.LASF810
	.byte	0x1
	.2byte	0x21fa
	.byte	0x1
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LLST53
	.4byte	0x457a
	.uleb128 0x25
	.4byte	.LASF791
	.byte	0x1
	.2byte	0x21fa
	.4byte	0x39d0
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x29
	.4byte	.LASF792
	.byte	0x1
	.2byte	0x21fc
	.4byte	0x6f4
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x21fe
	.4byte	0xe7
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x30
	.4byte	.LASF815
	.byte	0x1b
	.byte	0x7c
	.4byte	0x30
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF811
	.byte	0x1a
	.byte	0x30
	.4byte	0x4598
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x26
	.4byte	0x9d
	.uleb128 0x22
	.4byte	.LASF812
	.byte	0x1a
	.byte	0x34
	.4byte	0x45ae
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x26
	.4byte	0x9d
	.uleb128 0x22
	.4byte	.LASF813
	.byte	0x1a
	.byte	0x36
	.4byte	0x45c4
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x26
	.4byte	0x9d
	.uleb128 0x22
	.4byte	.LASF814
	.byte	0x1a
	.byte	0x38
	.4byte	0x45da
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x26
	.4byte	0x9d
	.uleb128 0x30
	.4byte	.LASF816
	.byte	0xe
	.byte	0x64
	.4byte	0xa85
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF817
	.byte	0xf
	.byte	0x42
	.4byte	0xa85
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF818
	.byte	0x1c
	.byte	0x33
	.4byte	0x460a
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x26
	.4byte	0x791
	.uleb128 0x22
	.4byte	.LASF819
	.byte	0x1c
	.byte	0x3f
	.4byte	0x4620
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x26
	.4byte	0x1000
	.uleb128 0x31
	.4byte	.LASF820
	.byte	0x15
	.2byte	0x206
	.4byte	0x1d49
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF821
	.byte	0x15
	.2byte	0x31e
	.4byte	0x1f10
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF822
	.byte	0x15
	.2byte	0x5ee
	.4byte	0x26ef
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF823
	.byte	0x15
	.2byte	0x80b
	.4byte	0x2784
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF824
	.byte	0x15
	.2byte	0x8ff
	.4byte	0x29dc
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF825
	.byte	0x1d
	.byte	0x78
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF826
	.byte	0x1d
	.byte	0x98
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF827
	.byte	0x1d
	.byte	0xa5
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF828
	.byte	0x1d
	.byte	0xbd
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF829
	.byte	0x1d
	.byte	0xc0
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF830
	.byte	0x1d
	.byte	0xc6
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF831
	.byte	0x19
	.byte	0x83
	.4byte	0x2c71
	.byte	0x1
	.byte	0x1
	.uleb128 0x6
	.4byte	0xad
	.4byte	0x46d6
	.uleb128 0x7
	.4byte	0x30
	.byte	0xd
	.byte	0
	.uleb128 0x29
	.4byte	.LASF832
	.byte	0x1
	.2byte	0x143
	.4byte	0x46e8
	.byte	0x5
	.byte	0x3
	.4byte	FI_PRE_TEST_STRING
	.uleb128 0x26
	.4byte	0x46c6
	.uleb128 0x31
	.4byte	.LASF833
	.byte	0x1
	.2byte	0x1ba7
	.4byte	0xfd
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF815
	.byte	0x1b
	.byte	0x7c
	.4byte	0x30
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF816
	.byte	0xe
	.byte	0x64
	.4byte	0xa85
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF817
	.byte	0xf
	.byte	0x42
	.4byte	0xa85
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF820
	.byte	0x15
	.2byte	0x206
	.4byte	0x1d49
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF821
	.byte	0x15
	.2byte	0x31e
	.4byte	0x1f10
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF822
	.byte	0x15
	.2byte	0x5ee
	.4byte	0x26ef
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF823
	.byte	0x15
	.2byte	0x80b
	.4byte	0x2784
	.byte	0x1
	.byte	0x1
	.uleb128 0x31
	.4byte	.LASF824
	.byte	0x15
	.2byte	0x8ff
	.4byte	0x29dc
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF825
	.byte	0x1d
	.byte	0x78
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF826
	.byte	0x1d
	.byte	0x98
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF827
	.byte	0x1d
	.byte	0xa5
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF828
	.byte	0x1d
	.byte	0xbd
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF829
	.byte	0x1d
	.byte	0xc0
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF830
	.byte	0x1d
	.byte	0xc6
	.4byte	0x87
	.byte	0x1
	.byte	0x1
	.uleb128 0x30
	.4byte	.LASF831
	.byte	0x19
	.byte	0x83
	.4byte	0x2c71
	.byte	0x1
	.byte	0x1
	.uleb128 0x32
	.4byte	.LASF834
	.byte	0x1
	.2byte	0x467
	.4byte	0x6f4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	last_time_a_valve_was_turned_off
	.uleb128 0x32
	.4byte	.LASF835
	.byte	0x1
	.2byte	0xfb6
	.4byte	0xd5
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	__largest_start_time_delta
	.uleb128 0x32
	.4byte	.LASF833
	.byte	0x1
	.2byte	0x1ba7
	.4byte	0xfd
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	one_ON_for_walk_thru
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x6
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI18
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI21
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI24
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI27
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI33
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI36
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI39
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI42
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI45
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI48
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI51
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI54
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI57
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI60
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI62
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI63
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI65
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI66
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB23
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI68
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI69
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB24
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI71
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI72
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB25
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI74
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI75
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB26
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI77
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI78
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB27
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI80
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI81
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB28
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI83
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI84
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB29
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI86
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI87
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB30
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI89
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI90
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB31
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI92
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI93
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI94
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB32
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI96
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI97
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB33
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI99
	.4byte	.LCFI100
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI100
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB34
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI102
	.4byte	.LCFI103
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI103
	.4byte	.LCFI104
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI104
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB35
	.4byte	.LCFI106
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI106
	.4byte	.LCFI107
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI107
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB36
	.4byte	.LCFI109
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI109
	.4byte	.LCFI110
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI110
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB37
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI112
	.4byte	.LCFI113
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI113
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB38
	.4byte	.LCFI115
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI115
	.4byte	.LCFI116
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI116
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB39
	.4byte	.LCFI118
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI118
	.4byte	.LCFI119
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI119
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB40
	.4byte	.LCFI121
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI121
	.4byte	.LCFI122
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI122
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB41
	.4byte	.LCFI124
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI124
	.4byte	.LCFI125
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI125
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB42
	.4byte	.LCFI127
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI127
	.4byte	.LCFI128
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI128
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB43
	.4byte	.LCFI130
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI130
	.4byte	.LCFI131
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI131
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB44
	.4byte	.LCFI133
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI133
	.4byte	.LCFI134
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI134
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB45
	.4byte	.LCFI136
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI136
	.4byte	.LCFI137
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI137
	.4byte	.LFE45
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB46
	.4byte	.LCFI139
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI139
	.4byte	.LCFI140
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI140
	.4byte	.LFE46
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB47
	.4byte	.LCFI142
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI142
	.4byte	.LCFI143
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI143
	.4byte	.LFE47
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST48:
	.4byte	.LFB48
	.4byte	.LCFI145
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI145
	.4byte	.LCFI146
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI146
	.4byte	.LFE48
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST49:
	.4byte	.LFB49
	.4byte	.LCFI148
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI148
	.4byte	.LCFI149
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI149
	.4byte	.LFE49
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST50:
	.4byte	.LFB50
	.4byte	.LCFI151
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI151
	.4byte	.LCFI152
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI152
	.4byte	.LFE50
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST51:
	.4byte	.LFB51
	.4byte	.LCFI154
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI154
	.4byte	.LCFI155
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI155
	.4byte	.LFE51
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST52:
	.4byte	.LFB52
	.4byte	.LCFI157
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI157
	.4byte	.LCFI158
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI158
	.4byte	.LFE52
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST53:
	.4byte	.LFB53
	.4byte	.LCFI160
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI160
	.4byte	.LCFI161
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI161
	.4byte	.LFE53
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LBB9
	.4byte	.LBE9
	.4byte	.LBB11
	.4byte	.LBE11
	.4byte	0
	.4byte	0
	.4byte	.LBB25
	.4byte	.LBE25
	.4byte	.LBB27
	.4byte	.LBE27
	.4byte	.LBB28
	.4byte	.LBE28
	.4byte	0
	.4byte	0
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF489:
	.ascii	"system_master_5_sec_avgs_next_index\000"
.LASF707:
	.ascii	"nm_at_starttime_decrement_NOW_days_count\000"
.LASF185:
	.ascii	"results_exceeded_stop_time_by_seconds_holding\000"
.LASF418:
	.ascii	"hub_needs_to_distribute_main_binary\000"
.LASF274:
	.ascii	"w_irrigation_reason_in_list_u8\000"
.LASF619:
	.ascii	"llength\000"
.LASF449:
	.ascii	"mlb_measured_during_mvor_closed_gpm\000"
.LASF706:
	.ascii	"pstop_time\000"
.LASF176:
	.ascii	"line_fill_time_sec\000"
.LASF754:
	.ascii	"manual_d_and_r\000"
.LASF76:
	.ascii	"one_or_more_in_list_for_walk_thru\000"
.LASF142:
	.ascii	"ptail\000"
.LASF757:
	.ascii	"lat_least_one_station_added\000"
.LASF623:
	.ascii	"pilc_station_ptr\000"
.LASF584:
	.ascii	"walk_thru_need_to_send_gid_to_master\000"
.LASF631:
	.ascii	"list_in_list_for\000"
.LASF755:
	.ascii	"lwater_this_station\000"
.LASF579:
	.ascii	"system_gids_to_clear_mlbs_for\000"
.LASF447:
	.ascii	"mlb_measured_during_irrigation_gpm\000"
.LASF763:
	.ascii	"lpreserve\000"
.LASF256:
	.ascii	"et_factor_100u\000"
.LASF342:
	.ascii	"two_wire_poc_decoder_inoperative\000"
.LASF510:
	.ascii	"flow_check_derate_table_max_stations_ON\000"
.LASF151:
	.ascii	"MIST_DLINK_TYPE\000"
.LASF641:
	.ascii	"list_bbf_ptr\000"
.LASF690:
	.ascii	"FOAL_IRRI_we_test_flow_when_ON_for_this_reason\000"
.LASF93:
	.ascii	"SYSTEM_BIT_FIELD_STRUCT\000"
.LASF741:
	.ascii	"__this_time_delta\000"
.LASF692:
	.ascii	"_nm_nm_nm_add_to_the_irrigation_list\000"
.LASF726:
	.ascii	"mp_alert_there_was_a_start\000"
.LASF622:
	.ascii	"pft_station_ptr\000"
.LASF608:
	.ascii	"pxTimer\000"
.LASF578:
	.ascii	"two_wire_cable_overheated\000"
.LASF215:
	.ascii	"ufim_highest_priority_pump_waiting\000"
.LASF756:
	.ascii	"lschedule\000"
.LASF430:
	.ascii	"STATION_PRESERVES_BIT_FIELD\000"
.LASF766:
	.ascii	"FOAL_IRRI_buildup_action_needed_records_for_token\000"
.LASF287:
	.ascii	"light_index_0_47\000"
.LASF133:
	.ascii	"lights\000"
.LASF600:
	.ascii	"results_skipped_due_to_RAIN_TABLE\000"
.LASF327:
	.ascii	"no_water_by_calendar_prevented\000"
.LASF370:
	.ascii	"manual_program_gallons_fl\000"
.LASF809:
	.ascii	"action_needed_reason\000"
.LASF749:
	.ascii	"pwalk_thru_gid\000"
.LASF484:
	.ascii	"ufim_list_contains_some_RRE_to_setex_that_are_not_O"
	.ascii	"N_b\000"
.LASF71:
	.ascii	"MVOR_in_effect_opened\000"
.LASF773:
	.ascii	"capacity\000"
.LASF767:
	.ascii	"action_records_to_xfer\000"
.LASF771:
	.ascii	"with_pump\000"
.LASF635:
	.ascii	"list_involved_in_a_flow_problem\000"
.LASF713:
	.ascii	"pstation_group_ptr_ptr\000"
.LASF588:
	.ascii	"results_at_least_one_valve_irrigated\000"
.LASF284:
	.ascii	"stations_removed_for_this_reason\000"
.LASF813:
	.ascii	"GuiFont_DecimalChar\000"
.LASF167:
	.ascii	"water_days_bool\000"
.LASF281:
	.ascii	"stop_for_the_highest_reason_in_all_systems\000"
.LASF455:
	.ascii	"no_longer_used_end_dt\000"
.LASF48:
	.ascii	"directions_honor_controller_set_to_OFF\000"
.LASF758:
	.ascii	"FOAL_IRRI_initiate_or_cancel_a_master_valve_overrid"
	.ascii	"e\000"
.LASF70:
	.ascii	"stable_flow\000"
.LASF258:
	.ascii	"stop_datetime_t\000"
.LASF84:
	.ascii	"ufim_one_or_more_in_list_for_mobile\000"
.LASF406:
	.ascii	"freeze_switch_active\000"
.LASF303:
	.ascii	"FRF_NO_UPDATE_REASON_thirty_percent\000"
.LASF496:
	.ascii	"MVOR_remaining_seconds\000"
.LASF683:
	.ascii	"record_count\000"
.LASF643:
	.ascii	"pptr_irrigation_list_hdr\000"
.LASF191:
	.ascii	"results_latest_finish_date_and_time\000"
.LASF68:
	.ascii	"flow_checking_enabled_by_user_setting\000"
.LASF543:
	.ascii	"GID_on_at_a_time\000"
.LASF807:
	.ascii	"ilc_to_remove\000"
.LASF367:
	.ascii	"expansion_u16\000"
.LASF585:
	.ascii	"walk_thru_gid_to_send\000"
.LASF323:
	.ascii	"flow_low\000"
.LASF837:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_irri.c\000"
.LASF21:
	.ascii	"BOOL_32\000"
.LASF797:
	.ascii	"alert_already_made_for_rain_switch\000"
.LASF647:
	.ascii	"pstation_list_element\000"
.LASF366:
	.ascii	"pi_moisture_balance_percentage_after_schedule_compl"
	.ascii	"etes_100u\000"
.LASF476:
	.ascii	"unused_0\000"
.LASF743:
	.ascii	"lbox_index_0\000"
.LASF768:
	.ascii	"lanxr\000"
.LASF433:
	.ascii	"skip_irrigation_till_due_to_manual_NOW_time\000"
.LASF383:
	.ascii	"hourly_total_inches_100u\000"
.LASF32:
	.ascii	"flow_check_group\000"
.LASF748:
	.ascii	"FOAL_IRRI_start_a_walk_thru\000"
.LASF611:
	.ascii	"next_ilc\000"
.LASF275:
	.ascii	"sxru_xfer_reason_u8\000"
.LASF648:
	.ascii	"list_element\000"
.LASF252:
	.ascii	"cycle_seconds_used_during_irrigation\000"
.LASF392:
	.ascii	"dls_eligible_to_fall_back\000"
.LASF361:
	.ascii	"pi_flow_check_share_of_hi_limit_gpm\000"
.LASF309:
	.ascii	"pending_first_to_send\000"
.LASF356:
	.ascii	"pi_watersense_requested_seconds\000"
.LASF697:
	.ascii	"lstation_preserves_index\000"
.LASF559:
	.ascii	"WALK_THRU_KICK_OFF_STRUCT\000"
.LASF534:
	.ascii	"list_support_foal_stations_ON\000"
.LASF318:
	.ascii	"current_none\000"
.LASF308:
	.ascii	"first_to_send\000"
.LASF175:
	.ascii	"pump_in_use\000"
.LASF434:
	.ascii	"skip_irrigation_till_due_to_calendar_NOW_time\000"
.LASF16:
	.ascii	"INT_16\000"
.LASF222:
	.ascii	"ufim_list_contains_some_non_pump_to_setex_that_are_"
	.ascii	"not_ON_b\000"
.LASF700:
	.ascii	"rain_causes_irrigation_to_be_skipped\000"
.LASF368:
	.ascii	"STATION_HISTORY_RECORD\000"
.LASF207:
	.ascii	"capacity_with_pump_gpm\000"
.LASF54:
	.ascii	"BIG_BIT_FIELD_FOR_ILC_STRUCT\000"
.LASF241:
	.ascii	"list_of_stations_ON\000"
.LASF538:
	.ascii	"requested_irrigation_seconds_ul\000"
.LASF624:
	.ascii	"foal_irri_cycle_and_soak_weighting\000"
.LASF77:
	.ascii	"one_or_more_in_list_for_test\000"
.LASF586:
	.ascii	"IRRI_COMM\000"
.LASF289:
	.ascii	"LIGHTS_OFF_XFER_RECORD\000"
.LASF89:
	.ascii	"number_of_flow_meters_in_this_sys\000"
.LASF248:
	.ascii	"water_sense_deferred_seconds\000"
.LASF759:
	.ascii	"psystem_gid\000"
.LASF413:
	.ascii	"factory_reset_registration_to_tell_commserver_to_cl"
	.ascii	"ear_pdata\000"
.LASF55:
	.ascii	"unused_four_bits\000"
.LASF782:
	.ascii	"how_many_on_list\000"
.LASF262:
	.ascii	"STATION_GROUP_STRUCT\000"
.LASF131:
	.ascii	"I2C_CARD_DETAILS_STRUCT\000"
.LASF220:
	.ascii	"ufim_list_contains_some_to_setex_that_are_not_ON_b\000"
.LASF260:
	.ascii	"expected_flow_rate_gpm_u16\000"
.LASF62:
	.ascii	"system_level_valves_are_ON_and_actively_checking\000"
.LASF620:
	.ascii	"pfor_ftimes\000"
.LASF576:
	.ascii	"stop_key_record\000"
.LASF719:
	.ascii	"station_d_and_r\000"
.LASF835:
	.ascii	"__largest_start_time_delta\000"
.LASF136:
	.ascii	"dash_m_card_present\000"
.LASF186:
	.ascii	"for_this_SECOND_add_to_exceeded_stop_time_by_second"
	.ascii	"s_holding\000"
.LASF118:
	.ascii	"port_b_freewave_lr_set_for_repeater\000"
.LASF82:
	.ascii	"ufim_one_or_more_in_list_for_walk_thru\000"
.LASF19:
	.ascii	"INT_32\000"
.LASF761:
	.ascii	"pinitiated_by\000"
.LASF419:
	.ascii	"expansion\000"
.LASF638:
	.ascii	"station_to_add\000"
.LASF533:
	.ascii	"list_support_foal_all_irrigation\000"
.LASF107:
	.ascii	"__dayofweek\000"
.LASF566:
	.ascii	"mvor_action_to_take\000"
.LASF225:
	.ascii	"ufim_number_ON_during_test\000"
.LASF237:
	.ascii	"done_with_runtime_cycle\000"
.LASF689:
	.ascii	"ASO_AO_LO\000"
.LASF537:
	.ascii	"station_preserves_index\000"
.LASF22:
	.ascii	"BITFIELD_BOOL\000"
.LASF448:
	.ascii	"mlb_limit_during_irrigation_gpm\000"
.LASF165:
	.ascii	"stop_time\000"
.LASF336:
	.ascii	"mois_cause_cycle_skip\000"
.LASF267:
	.ascii	"BOX_CONFIGURATION_STRUCT\000"
.LASF674:
	.ascii	"FOAL_IRRI_remove_all_stations_from_the_irrigation_l"
	.ascii	"ists\000"
.LASF818:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF613:
	.ascii	"init_battery_backed_foal_irri\000"
.LASF458:
	.ascii	"rre_gallons_fl\000"
.LASF409:
	.ascii	"commserver_monthly_rcvd_bytes\000"
.LASF680:
	.ascii	"pstation_number\000"
.LASF626:
	.ascii	"pbbf_ptr\000"
.LASF91:
	.ascii	"whole_thing\000"
.LASF549:
	.ascii	"flow_recording_group_made_during_this_turn_OFF_loop"
	.ascii	"\000"
.LASF390:
	.ascii	"verify_string_pre\000"
.LASF372:
	.ascii	"walk_thru_gallons_fl\000"
.LASF331:
	.ascii	"rain_as_negative_time_reduced_irrigation\000"
.LASF243:
	.ascii	"station_group_ptr\000"
.LASF208:
	.ascii	"capacity_without_pump_gpm\000"
.LASF604:
	.ascii	"results_skipped_due_to_MOISTURE_BALANCE\000"
.LASF333:
	.ascii	"switch_rain_prevented_or_curtailed\000"
.LASF466:
	.ascii	"non_controller_gallons_fl\000"
.LASF226:
	.ascii	"ufim_stations_ON_with_the_pump_b\000"
.LASF725:
	.ascii	"pi_alert_some_skipped_due_to_not_being_in_use\000"
.LASF814:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF456:
	.ascii	"rainfall_raw_total_100u\000"
.LASF659:
	.ascii	"pilc\000"
.LASF468:
	.ascii	"mode\000"
.LASF40:
	.ascii	"at_some_point_should_check_flow\000"
.LASF564:
	.ascii	"program_GID\000"
.LASF216:
	.ascii	"ufim_highest_priority_non_pump_waiting\000"
.LASF607:
	.ascii	"freeze_switch_timer_callback\000"
.LASF777:
	.ascii	"FOAL_IRRI_there_are_other_flow_check_groups_already"
	.ascii	"_ON\000"
.LASF15:
	.ascii	"UNS_16\000"
.LASF148:
	.ascii	"pPrev\000"
.LASF535:
	.ascii	"list_support_foal_action_needed\000"
.LASF334:
	.ascii	"switch_freeze_prevented_or_curtailed\000"
.LASF49:
	.ascii	"directions_honor_MANUAL_NOW\000"
.LASF550:
	.ascii	"stations_ON_by_controller\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF369:
	.ascii	"programmed_irrigation_gallons_irrigated_fl\000"
.LASF469:
	.ascii	"end_date\000"
.LASF688:
	.ascii	"ASO_AO_HI\000"
.LASF556:
	.ascii	"twccm\000"
.LASF774:
	.ascii	"FOAL_IRRI_for_turn_ON_rules__there_are_higher_reaso"
	.ascii	"ns_in_the_list\000"
.LASF139:
	.ascii	"two_wire_terminal_present\000"
.LASF153:
	.ascii	"float\000"
.LASF516:
	.ascii	"flow_check_hi_limit\000"
.LASF820:
	.ascii	"weather_preserves\000"
.LASF802:
	.ascii	"need_to_get_the_next_ilc\000"
.LASF625:
	.ascii	"_nm_pumpuse_priority_setexpected_flowchecking_irrig"
	.ascii	"atedlasttime_weighting\000"
.LASF838:
	.ascii	"FOAL_IRRI_completely_wipe_both_foal_side_and_irri_s"
	.ascii	"ide_irrigation\000"
.LASF34:
	.ascii	"responds_to_rain\000"
.LASF377:
	.ascii	"mobile_seconds_us\000"
.LASF187:
	.ascii	"results_manual_programs_and_programmed_irrigation_c"
	.ascii	"lashed_holding\000"
.LASF491:
	.ascii	"accumulated_gallons_for_accumulators_foal\000"
.LASF268:
	.ascii	"requested_irrigation_seconds_u32\000"
.LASF259:
	.ascii	"stop_datetime_d\000"
.LASF17:
	.ascii	"UNS_32\000"
.LASF194:
	.ascii	"results_exceeded_stop_time_by_seconds\000"
.LASF164:
	.ascii	"start_time\000"
.LASF239:
	.ascii	"ftimes_support\000"
.LASF340:
	.ascii	"rip_valid_to_show\000"
.LASF630:
	.ascii	"add_in_list_for\000"
.LASF168:
	.ascii	"irrigate_on_29th_or_31st_bool\000"
.LASF507:
	.ascii	"flow_check_required_cell_iteration\000"
.LASF332:
	.ascii	"rain_table_R_M_or_Poll_prevented_or_curtailed\000"
.LASF386:
	.ascii	"rain_shutdown_rcvd_from_commserver_uns32\000"
.LASF663:
	.ascii	"FOAL_IRRI_remove_all_instances_of_this_station_from"
	.ascii	"_the_irrigation_lists\000"
.LASF349:
	.ascii	"pi_gallons_irrigated_fl\000"
.LASF408:
	.ascii	"pending_changes_to_send_to_comm_server\000"
.LASF764:
	.ascii	"lgroup_name\000"
.LASF371:
	.ascii	"manual_gallons_fl\000"
.LASF211:
	.ascii	"system_master_number_of_valves_ON\000"
.LASF232:
	.ascii	"inhibit_next_turn_ON_remaining_seconds\000"
.LASF823:
	.ascii	"chain\000"
.LASF277:
	.ascii	"updated_requested_irrigation_seconds_ul\000"
.LASF617:
	.ascii	"lnumber_of_stations\000"
.LASF610:
	.ascii	"lilc\000"
.LASF273:
	.ascii	"STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT\000"
.LASF509:
	.ascii	"flow_check_derate_table_gpm_slot_size\000"
.LASF193:
	.ascii	"results_hit_the_stop_time\000"
.LASF826:
	.ascii	"list_foal_irri_recursive_MUTEX\000"
.LASF518:
	.ascii	"system_rcvd_most_recent_number_of_valves_ON\000"
.LASF832:
	.ascii	"FI_PRE_TEST_STRING\000"
.LASF46:
	.ascii	"xfer_to_irri_machines\000"
.LASF453:
	.ascii	"SYSTEM_MAINLINE_BREAK_RECORD\000"
.LASF160:
	.ascii	"percent_adjust_start_date\000"
.LASF159:
	.ascii	"percent_adjust_100u\000"
.LASF362:
	.ascii	"pi_flow_check_share_of_lo_limit_gpm\000"
.LASF810:
	.ascii	"FOAL_IRRI_maintain_irrigation_list\000"
.LASF457:
	.ascii	"rre_seconds\000"
.LASF675:
	.ascii	"FOAL_IRRI_remove_stations_in_this_system_from_the_i"
	.ascii	"rrigation_lists\000"
.LASF301:
	.ascii	"FRF_NO_CHECK_REASON_not_supposed_to_check\000"
.LASF555:
	.ascii	"need_to_distribute_twci\000"
.LASF487:
	.ascii	"system_master_5_second_averages_ring\000"
.LASF230:
	.ascii	"ufim_highest_non_pump_reason_in_list_available_to_t"
	.ascii	"urn_ON\000"
.LASF503:
	.ascii	"delivered_mlb_record\000"
.LASF20:
	.ascii	"UNS_64\000"
.LASF388:
	.ascii	"needs_to_be_broadcast\000"
.LASF355:
	.ascii	"pi_total_requested_minutes_us_10u\000"
.LASF155:
	.ascii	"group_identity_number\000"
.LASF415:
	.ascii	"ununsed_uns8_1\000"
.LASF558:
	.ascii	"group_name\000"
.LASF416:
	.ascii	"commserver_monthly_mobile_status_updates_bytes\000"
.LASF149:
	.ascii	"pNext\000"
.LASF197:
	.ascii	"list_support_manual_program\000"
.LASF654:
	.ascii	"inhibit_next_turn_on\000"
.LASF776:
	.ascii	"phighest_reason\000"
.LASF9:
	.ascii	"portTickType\000"
.LASF594:
	.ascii	"results_skipped_due_to_RAIN_SWITCH\000"
.LASF100:
	.ascii	"date_time\000"
.LASF658:
	.ascii	"set_station_history_flags_if_in_the_list_for_progra"
	.ascii	"mmed_irrigation\000"
.LASF189:
	.ascii	"results_elapsed_irrigation_time\000"
.LASF143:
	.ascii	"count\000"
.LASF628:
	.ascii	"pstation_to_add\000"
.LASF595:
	.ascii	"results_skipped_due_to_FREEZE_SWITCH\000"
.LASF73:
	.ascii	"one_or_more_in_list_for_programmed_irrigation\000"
.LASF403:
	.ascii	"remaining_gage_pulses\000"
.LASF562:
	.ascii	"TEST_AND_MOBILE_KICK_OFF_STRUCT\000"
.LASF822:
	.ascii	"system_preserves\000"
.LASF180:
	.ascii	"GID_irrigation_system\000"
.LASF65:
	.ascii	"system_level_valves_are_ON_and_waiting_to_acquire_e"
	.ascii	"xpected\000"
.LASF567:
	.ascii	"mvor_seconds\000"
.LASF124:
	.ascii	"size_of_the_union\000"
.LASF717:
	.ascii	"TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_s"
	.ascii	"tart\000"
.LASF747:
	.ascii	"test_d_and_r\000"
.LASF109:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF317:
	.ascii	"current_short\000"
.LASF228:
	.ascii	"ufim_highest_reason_in_list_available_to_turn_ON\000"
.LASF8:
	.ascii	"size_t\000"
.LASF441:
	.ascii	"STATION_PRESERVES_RECORD\000"
.LASF593:
	.ascii	"results_skipped_due_to_CALENDAR_NOW\000"
.LASF482:
	.ascii	"ufim_one_ON_to_set_expected_b\000"
.LASF156:
	.ascii	"precip_rate_in_100000u\000"
.LASF43:
	.ascii	"rre_on_sxr_to_turn_OFF\000"
.LASF474:
	.ascii	"closing_record_for_the_period\000"
.LASF74:
	.ascii	"one_or_more_in_list_for_manual_program\000"
.LASF170:
	.ascii	"et_in_use\000"
.LASF561:
	.ascii	"set_expected\000"
.LASF330:
	.ascii	"rain_as_negative_time_prevented_irrigation\000"
.LASF687:
	.ascii	"FOAL_IRRI_translate_alert_actions_to_flow_checking_"
	.ascii	"group\000"
.LASF571:
	.ascii	"test_request\000"
.LASF831:
	.ascii	"irri_comm\000"
.LASF10:
	.ascii	"xQueueHandle\000"
.LASF402:
	.ascii	"yesterdays_ET_was_zero\000"
.LASF668:
	.ascii	"FOAL_IRRI_remove_all_stations_ON_at_this_box_from_t"
	.ascii	"he_irrigation_lists\000"
.LASF794:
	.ascii	"removal_reason\000"
.LASF812:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF734:
	.ascii	"lss_ptr_to_station_group_using_et\000"
.LASF679:
	.ascii	"FOAL_IRRI_set_no_current_flag_in_station_history_li"
	.ascii	"ne_if_appropriate\000"
.LASF127:
	.ascii	"TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT\000"
.LASF227:
	.ascii	"ufim_stations_ON_without_the_pump_b\000"
.LASF224:
	.ascii	"ufim_based_on_reason_in_list_at_least_one_valve_is_"
	.ascii	"ON_that_wants_to_check_flow\000"
.LASF177:
	.ascii	"delay_between_valve_time_sec\000"
.LASF670:
	.ascii	"found_one\000"
.LASF279:
	.ascii	"stop_for_this_reason\000"
.LASF353:
	.ascii	"pi_first_cycle_start_date\000"
.LASF120:
	.ascii	"option_AQUAPONICS\000"
.LASF290:
	.ascii	"flow_check_status\000"
.LASF827:
	.ascii	"chain_members_recursive_MUTEX\000"
.LASF126:
	.ascii	"current_percentage_of_max\000"
.LASF341:
	.ascii	"two_wire_station_decoder_inoperative\000"
.LASF173:
	.ascii	"on_at_a_time__allowed_ON_in_mainline__user_setting\000"
.LASF798:
	.ascii	"alert_already_made_for_freeze_switch\000"
.LASF523:
	.ascii	"reason_in_running_list\000"
.LASF221:
	.ascii	"ufim_list_contains_some_pump_to_setex_that_are_not_"
	.ascii	"ON_b\000"
.LASF393:
	.ascii	"dls_after_fall_back_ignore_sxs\000"
.LASF614:
	.ascii	"pforce_the_initialization\000"
.LASF481:
	.ascii	"ufim_highest_reason_of_OFF_valve_to_set_expected\000"
.LASF470:
	.ascii	"meter_read_time\000"
.LASF686:
	.ascii	"pmlb_info_data_ptr\000"
.LASF233:
	.ascii	"FT_SYSTEM_GROUP_STRUCT\000"
.LASF374:
	.ascii	"mobile_gallons_fl\000"
.LASF568:
	.ascii	"initiated_by\000"
.LASF519:
	.ascii	"mvor_stop_date\000"
.LASF574:
	.ascii	"light_off_request\000"
.LASF140:
	.ascii	"WHATS_INSTALLED_STRUCT\000"
.LASF801:
	.ascii	"irrigation_maintenance_THIRD__full_station_list_pas"
	.ascii	"s_to_turn_off_stations_that_should_not_be_ON_based_"
	.ascii	"upon_those_trying_to_come_ON\000"
.LASF544:
	.ascii	"moisture_sensor_decoder_serial_number\000"
.LASF551:
	.ascii	"timer_rain_switch\000"
.LASF681:
	.ascii	"FOAL_extract_clear_mlb_gids\000"
.LASF511:
	.ascii	"flow_check_derate_table_number_of_gpm_slots\000"
.LASF299:
	.ascii	"FRF_NO_CHECK_REASON_cycle_time_too_short\000"
.LASF92:
	.ascii	"overall_size\000"
.LASF304:
	.ascii	"FLOW_RECORDER_FLAG_BIT_FIELD\000"
.LASF399:
	.ascii	"et_table_update_all_historical_values\000"
.LASF462:
	.ascii	"manual_program_seconds\000"
.LASF11:
	.ascii	"xSemaphoreHandle\000"
.LASF128:
	.ascii	"card_present\000"
.LASF157:
	.ascii	"crop_coefficient_100u\000"
.LASF79:
	.ascii	"ufim_one_or_more_in_list_for_programmed_irrigation\000"
.LASF784:
	.ascii	"foal_irri_maintenance_may_run\000"
.LASF497:
	.ascii	"transition_timer_all_stations_are_OFF\000"
.LASF212:
	.ascii	"ufim_what_are_we_turning_on_b\000"
.LASF760:
	.ascii	"pmvor_action_to_take\000"
.LASF125:
	.ascii	"PURCHASED_OPTIONS_STRUCT\000"
.LASF169:
	.ascii	"mow_day\000"
.LASF315:
	.ascii	"hit_stop_time\000"
.LASF591:
	.ascii	"results_skipped_as_the_controller_is_set_to_OFF\000"
.LASF412:
	.ascii	"factory_reset_do_not_allow_pdata_to_be_sent\000"
.LASF321:
	.ascii	"watersense_min_cycle_eliminated_a_cycle\000"
.LASF47:
	.ascii	"directions_honor_RAIN_TABLE\000"
.LASF517:
	.ascii	"flow_check_lo_limit\000"
.LASF31:
	.ascii	"flow_check_lo_action\000"
.LASF63:
	.ascii	"system_level_valves_are_ON_and_waiting_to_update_de"
	.ascii	"rate_table\000"
.LASF307:
	.ascii	"first_to_display\000"
.LASF669:
	.ascii	"pcontroller_index\000"
.LASF677:
	.ascii	"pfor_this_reason\000"
.LASF387:
	.ascii	"inhibit_irrigation_have_crossed_the_minimum\000"
.LASF103:
	.ascii	"__year\000"
.LASF178:
	.ascii	"high_flow_action\000"
.LASF657:
	.ascii	"pfor_which_system_gid\000"
.LASF384:
	.ascii	"roll_to_roll_raw_pulse_count\000"
.LASF830:
	.ascii	"list_system_recursive_MUTEX\000"
.LASF443:
	.ascii	"there_was_a_MLB_during_irrigation\000"
.LASF130:
	.ascii	"sizer\000"
.LASF163:
	.ascii	"schedule_type\000"
.LASF560:
	.ascii	"time_seconds\000"
.LASF263:
	.ascii	"serial_number\000"
.LASF261:
	.ascii	"FT_STATION_STRUCT\000"
.LASF27:
	.ascii	"w_uses_the_pump\000"
.LASF438:
	.ascii	"rain_minutes_10u\000"
.LASF205:
	.ascii	"system_gid\000"
.LASF87:
	.ascii	"delivered_MVOR_in_effect_opened\000"
.LASF667:
	.ascii	"no_more_for_system\000"
.LASF775:
	.ascii	"preason\000"
.LASF265:
	.ascii	"port_A_device_index\000"
.LASF81:
	.ascii	"ufim_one_or_more_in_list_for_manual\000"
.LASF300:
	.ascii	"FRF_NO_CHECK_REASON_reboot_or_chain_went_down\000"
.LASF709:
	.ascii	"pstation_preserves_index\000"
.LASF693:
	.ascii	"pss_ptr\000"
.LASF597:
	.ascii	"directions_consume_RAIN_TIME\000"
.LASF540:
	.ascii	"soak_seconds_ul\000"
.LASF428:
	.ascii	"station_history_rip_needs_to_be_saved\000"
.LASF701:
	.ascii	"ilc_ptr_copy\000"
.LASF310:
	.ascii	"pending_first_to_send_in_use\000"
.LASF800:
	.ascii	"irrigation_maintenance_SECOND__pass_through_all_sys"
	.ascii	"tems_to_develop_more_ufim_vars\000"
.LASF799:
	.ascii	"ilc_still_in_the_list_and_need_to_get_the_next_ilc\000"
.LASF548:
	.ascii	"list_of_foal_stations_with_action_needed\000"
.LASF716:
	.ascii	"station_start_time\000"
.LASF629:
	.ascii	"pstation_in_list\000"
.LASF60:
	.ascii	"system_level_valves_are_ON_but_will_not_be_checking"
	.ascii	"_flow\000"
.LASF805:
	.ascii	"irrigation_maintenance_FOURTH__full_station_list_pa"
	.ascii	"ss_to_hunt_for_stations_to_turn_ON\000"
.LASF803:
	.ascii	"pause_it\000"
.LASF114:
	.ascii	"port_a_raveon_radio_type\000"
.LASF357:
	.ascii	"pi_rain_at_start_time_before_working_down__minutes_"
	.ascii	"10u\000"
.LASF141:
	.ascii	"phead\000"
.LASF98:
	.ascii	"RAIN_TABLE_ENTRY\000"
.LASF182:
	.ascii	"after_removal_opportunity_one_or_more_in_the_list_f"
	.ascii	"or_programmed_irrigation\000"
.LASF703:
	.ascii	"delete_this_ilc\000"
.LASF306:
	.ascii	"next_available\000"
.LASF528:
	.ascii	"saw_during_the_scan\000"
.LASF210:
	.ascii	"ufim_on_at_a_time__presently_allowed_ON_in_mainline"
	.ascii	"__learned\000"
.LASF324:
	.ascii	"flow_high\000"
.LASF735:
	.ascii	"ilc_on_action_list\000"
.LASF672:
	.ascii	"preason_in_list\000"
.LASF531:
	.ascii	"members\000"
.LASF391:
	.ascii	"dls_saved_date\000"
.LASF295:
	.ascii	"FRF_NO_CHECK_REASON_not_enabled_by_user_setting\000"
.LASF715:
	.ascii	"is_an_isolated_station\000"
.LASF298:
	.ascii	"FRF_NO_CHECK_REASON_unstable_flow\000"
.LASF288:
	.ascii	"LIGHTS_ON_XFER_RECORD\000"
.LASF242:
	.ascii	"system_ptr\000"
.LASF117:
	.ascii	"port_b_raveon_radio_type\000"
.LASF684:
	.ascii	"lgid\000"
.LASF257:
	.ascii	"distribution_uniformity_100u\000"
.LASF192:
	.ascii	"results_latest_finish_date_and_time_holding\000"
.LASF811:
	.ascii	"GuiFont_LanguageActive\000"
.LASF621:
	.ascii	"p_ptr\000"
.LASF244:
	.ascii	"manual_program_ptrs\000"
.LASF33:
	.ascii	"responds_to_wind\000"
.LASF351:
	.ascii	"GID_irrigation_schedule\000"
.LASF57:
	.ascii	"mv_open_for_irrigation\000"
.LASF730:
	.ascii	"lstop_datetime\000"
.LASF500:
	.ascii	"timer_MLB_just_stopped_irrigating_blockout_seconds_"
	.ascii	"remaining\000"
.LASF714:
	.ascii	"station_group_GID\000"
.LASF702:
	.ascii	"add_to_the_list\000"
.LASF599:
	.ascii	"results_reduced_due_to_RAIN_TIME\000"
.LASF360:
	.ascii	"pi_flow_check_share_of_actual_gpm\000"
.LASF39:
	.ascii	"rre_station_is_paused\000"
.LASF525:
	.ascii	"system\000"
.LASF113:
	.ascii	"option_HUB\000"
.LASF454:
	.ascii	"start_dt\000"
.LASF781:
	.ascii	"nm_FOAL_add_to_action_needed_list\000"
.LASF28:
	.ascii	"w_did_not_irrigate_last_time\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF246:
	.ascii	"box_index_0\000"
.LASF673:
	.ascii	"FOAL_IRRI_remove_all_stations_at_this_box_from_the_"
	.ascii	"irrigation_lists\000"
.LASF432:
	.ascii	"station_report_data_rip\000"
.LASF378:
	.ascii	"test_seconds_us\000"
.LASF606:
	.ascii	"rain_switch_timer_callback\000"
.LASF320:
	.ascii	"current_high\000"
.LASF297:
	.ascii	"FRF_NO_CHECK_REASON_combo_not_allowed_to_check\000"
.LASF99:
	.ascii	"DATE_TIME\000"
.LASF203:
	.ascii	"FT_MANUAL_PROGRAM_STRUCT\000"
.LASF67:
	.ascii	"flow_checking_enabled_and_allowed\000"
.LASF95:
	.ascii	"status\000"
.LASF276:
	.ascii	"remaining_ON_or_soak_seconds\000"
.LASF129:
	.ascii	"tb_present\000"
.LASF85:
	.ascii	"due_to_edit_resync_to_the_system_list\000"
.LASF602:
	.ascii	"results_skipped_due_to_WATERSENSE_MIN_CYCLE\000"
.LASF358:
	.ascii	"pi_rain_at_start_time_after_working_down__minutes_1"
	.ascii	"0u\000"
.LASF404:
	.ascii	"clear_runaway_gage\000"
.LASF280:
	.ascii	"stop_in_this_system_gid\000"
.LASF405:
	.ascii	"rain_switch_active\000"
.LASF816:
	.ascii	"station_info_list_hdr\000"
.LASF293:
	.ascii	"FRF_NO_CHECK_REASON_cell_iterations_too_low\000"
.LASF24:
	.ascii	"station_priority\000"
.LASF553:
	.ascii	"wind_paused\000"
.LASF582:
	.ascii	"send_crc_list\000"
.LASF111:
	.ascii	"option_SSE\000"
.LASF132:
	.ascii	"stations\000"
.LASF475:
	.ascii	"SYSTEM_BUDGET_REPORT_RECORD\000"
.LASF605:
	.ascii	"ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT\000"
.LASF363:
	.ascii	"station_number\000"
.LASF61:
	.ascii	"system_level_valves_are_ON_and_waiting_to_check_flo"
	.ascii	"w\000"
.LASF364:
	.ascii	"pi_number_of_repeats\000"
.LASF736:
	.ascii	"new_station_made_it_onto_the_foal_irrigation_list\000"
.LASF471:
	.ascii	"predicted_use_to_end_of_period\000"
.LASF504:
	.ascii	"derate_table_10u\000"
.LASF634:
	.ascii	"add_involved_in_a_flow_problem\000"
.LASF521:
	.ascii	"delivered_MVOR_remaining_seconds\000"
.LASF728:
	.ascii	"manual_program_GID\000"
.LASF653:
	.ascii	"paction_reason\000"
.LASF791:
	.ascii	"pdate_time\000"
.LASF119:
	.ascii	"port_b_freewave_sr_set_for_repeater\000"
.LASF721:
	.ascii	"mp_overall_d_and_r\000"
.LASF720:
	.ascii	"pi_overall_d_and_r\000"
.LASF656:
	.ascii	"nm_shut_down_mvs_and_pmps_throughout_this_system\000"
.LASF401:
	.ascii	"run_away_gage\000"
.LASF53:
	.ascii	"directions_honor_WIND_PAUSE\000"
.LASF753:
	.ascii	"mwkos\000"
.LASF461:
	.ascii	"manual_seconds\000"
.LASF421:
	.ascii	"flow_check_station_cycles_count\000"
.LASF96:
	.ascii	"ET_TABLE_ENTRY\000"
.LASF765:
	.ascii	"_nm_nm_foal_irri_complete_the_action_needed_turn_ON"
	.ascii	"\000"
.LASF354:
	.ascii	"pi_last_cycle_end_date\000"
.LASF666:
	.ascii	"captured_bsr\000"
.LASF183:
	.ascii	"results_start_date_and_time_holding\000"
.LASF286:
	.ascii	"in_use\000"
.LASF376:
	.ascii	"GID_station_group\000"
.LASF352:
	.ascii	"record_start_date\000"
.LASF644:
	.ascii	"plist_item\000"
.LASF732:
	.ascii	"lsgs_ptr\000"
.LASF234:
	.ascii	"slot_loaded\000"
.LASF335:
	.ascii	"wind_conditions_prevented_or_curtailed\000"
.LASF442:
	.ascii	"STATION_PRESERVES_STRUCT\000"
.LASF4:
	.ascii	"long int\000"
.LASF271:
	.ascii	"station_number_0_u8\000"
.LASF429:
	.ascii	"distribute_last_measured_current_ma\000"
.LASF294:
	.ascii	"FRF_NO_CHECK_REASON_no_flow_meter\000"
.LASF444:
	.ascii	"there_was_a_MLB_during_mvor_closed\000"
.LASF198:
	.ascii	"start_times\000"
.LASF738:
	.ascii	"usable_rain_100u\000"
.LASF112:
	.ascii	"option_SSE_D\000"
.LASF190:
	.ascii	"results_start_date_and_time_associated_with_the_lat"
	.ascii	"est_finish_date_and_time\000"
.LASF526:
	.ascii	"SYSTEM_BB_STRUCT\000"
.LASF572:
	.ascii	"manual_water_request\000"
.LASF439:
	.ascii	"spbf\000"
.LASF147:
	.ascii	"MIST_LIST_HDR_TYPE_PTR\000"
.LASF512:
	.ascii	"flow_check_ranges_gpm\000"
.LASF41:
	.ascii	"at_some_point_flow_was_checked\000"
.LASF699:
	.ascii	"rain_entry\000"
.LASF440:
	.ascii	"last_measured_current_ma\000"
.LASF344:
	.ascii	"STATION_HISTORY_BITFIELD\000"
.LASF236:
	.ascii	"did_not_irrigate_last_time_holding_copy\000"
.LASF609:
	.ascii	"pcompletely_wipe\000"
.LASF417:
	.ascii	"hub_needs_to_distribute_tpmicro_binary\000"
.LASF637:
	.ascii	"ft_station_in_list\000"
.LASF796:
	.ascii	"alert_already_made_for_rain\000"
.LASF379:
	.ascii	"walk_thru_seconds_us\000"
.LASF420:
	.ascii	"WEATHER_PRESERVES_STRUCT\000"
.LASF616:
	.ascii	"pdata_ptr\000"
.LASF655:
	.ascii	"flow_recorder_flag\000"
.LASF633:
	.ascii	"list_weighting\000"
.LASF530:
	.ascii	"CHAIN_MEMBERS_SHARED_STRUCT\000"
.LASF238:
	.ascii	"FTIMES_STATION_BIT_FIELD_STRUCT\000"
.LASF451:
	.ascii	"mlb_measured_during_all_other_times_gpm\000"
.LASF785:
	.ascii	"decrement_system_preserves_timers\000"
.LASF385:
	.ascii	"midnight_to_midnight_raw_pulse_count\000"
.LASF395:
	.ascii	"et_rip\000"
.LASF25:
	.ascii	"w_reason_in_list\000"
.LASF834:
	.ascii	"last_time_a_valve_was_turned_off\000"
.LASF545:
	.ascii	"IRRIGATION_LIST_COMPONENT\000"
.LASF596:
	.ascii	"results_skipped_due_to_WIND_PAUSE\000"
.LASF146:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF783:
	.ascii	"_if_supposed_to_but_did_not_ever_check_flow_make_an"
	.ascii	"_alert\000"
.LASF134:
	.ascii	"weather_card_present\000"
.LASF86:
	.ascii	"accounted_for\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF649:
	.ascii	"FOAL_IRRI_load_sfml_for_distribution_to_slaves\000"
.LASF423:
	.ascii	"i_status\000"
.LASF676:
	.ascii	"pbsr\000"
.LASF722:
	.ascii	"pi_alert_there_was_a_start\000"
.LASF250:
	.ascii	"remaining_seconds_ON\000"
.LASF50:
	.ascii	"directions_honor_CALENDAR_NOW\000"
.LASF411:
	.ascii	"commserver_monthly_code_receipt_bytes\000"
.LASF152:
	.ascii	"STATION_STRUCT\000"
.LASF305:
	.ascii	"original_allocation\000"
.LASF632:
	.ascii	"add_weighting\000"
.LASF490:
	.ascii	"system_rcvd_most_recent_token_5_second_average\000"
.LASF598:
	.ascii	"results_skipped_due_to_RAIN_TIME\000"
.LASF106:
	.ascii	"__seconds\000"
.LASF790:
	.ascii	"irrigation_maintenance_FIRST__full_list_pass_to_dev"
	.ascii	"elop_ufim_vars_and_remove_completed_stations\000"
.LASF138:
	.ascii	"dash_m_card_type\000"
.LASF314:
	.ascii	"controller_turned_off\000"
.LASF711:
	.ascii	"nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_"
	.ascii	"and_check_for_isolated_stations\000"
.LASF80:
	.ascii	"ufim_one_or_more_in_list_for_manual_program\000"
.LASF514:
	.ascii	"flow_check_tolerance_minus_gpm\000"
.LASF13:
	.ascii	"char\000"
.LASF373:
	.ascii	"test_gallons_fl\000"
.LASF464:
	.ascii	"programmed_irrigation_gallons_fl\000"
.LASF627:
	.ascii	"nm_this_station_weighs_MORE_than_the_list_station\000"
.LASF196:
	.ascii	"FT_STATION_GROUP_STRUCT\000"
.LASF312:
	.ascii	"FLOW_RECORDING_CONTROL_STRUCT\000"
.LASF172:
	.ascii	"on_at_a_time__allowed_ON_in_station_group__user_set"
	.ascii	"ting\000"
.LASF499:
	.ascii	"timer_MVJO_flow_checking_blockout_seconds_remaining"
	.ascii	"\000"
.LASF642:
	.ascii	"nm_return_station_this_station_belongs_before_in_th"
	.ascii	"e_irrigation_list\000"
.LASF431:
	.ascii	"station_history_rip\000"
.LASF446:
	.ascii	"dummy_byte\000"
.LASF694:
	.ascii	"psgs_ptr\000"
.LASF825:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF710:
	.ascii	"pcurrent_dt\000"
.LASF557:
	.ascii	"FOAL_IRRI_BB_STRUCT\000"
.LASF14:
	.ascii	"UNS_8\000"
.LASF245:
	.ascii	"station_number_0\000"
.LASF296:
	.ascii	"FRF_NO_CHECK_REASON_acquiring_expected\000"
.LASF539:
	.ascii	"cycle_seconds_ul\000"
.LASF204:
	.ascii	"list_support_systems\000"
.LASF69:
	.ascii	"there_are_pocs_without_flow_meters\000"
.LASF105:
	.ascii	"__minutes\000"
.LASF202:
	.ascii	"run_time_seconds\000"
.LASF380:
	.ascii	"manual_seconds_us\000"
.LASF483:
	.ascii	"ufim_one_RRE_ON_to_set_expected_b\000"
.LASF254:
	.ascii	"soak_seconds_used_during_irrigation\000"
.LASF493:
	.ascii	"stability_avgs_index_of_last_computed\000"
.LASF282:
	.ascii	"stop_in_all_systems\000"
.LASF569:
	.ascii	"MVOR_KICK_OFF_STRUCT\000"
.LASF650:
	.ascii	"nm_nm_FOAL_if_station_is_ON_turn_it_OFF\000"
.LASF407:
	.ascii	"write_tpmicro_file_in_new_file_format\000"
.LASF646:
	.ascii	"FOAL_IRRI_remove_and_reinsert_into_foal_irri_irriga"
	.ascii	"tion_list\000"
.LASF283:
	.ascii	"stop_for_all_reasons\000"
.LASF101:
	.ascii	"__day\000"
.LASF733:
	.ascii	"lss_ptr_to_station_using_et\000"
.LASF752:
	.ascii	"FOAL_IRRI_initiate_manual_watering\000"
.LASF51:
	.ascii	"directions_honor_RAIN_SWITCH\000"
.LASF435:
	.ascii	"skip_irrigation_till_due_to_manual_NOW_date\000"
.LASF319:
	.ascii	"current_low\000"
.LASF541:
	.ascii	"line_fill_seconds\000"
.LASF26:
	.ascii	"w_to_set_expected\000"
.LASF219:
	.ascii	"ufim_list_contains_waiting_non_pump_valves_b\000"
.LASF478:
	.ascii	"BY_SYSTEM_BUDGET_RECORD\000"
.LASF414:
	.ascii	"perform_a_discovery_following_a_FACTORY_reset\000"
.LASF575:
	.ascii	"send_stop_key_record\000"
.LASF821:
	.ascii	"station_preserves\000"
.LASF705:
	.ascii	"pcurrent_datetime\000"
.LASF532:
	.ascii	"CHAIN_MEMBERS_STRUCT\000"
.LASF83:
	.ascii	"ufim_one_or_more_in_list_for_test\000"
.LASF437:
	.ascii	"left_over_irrigation_seconds\000"
.LASF640:
	.ascii	"add_bbf_ptr\000"
.LASF589:
	.ascii	"results_skipped_as_there_is_a_MLB\000"
.LASF473:
	.ascii	"ratio\000"
.LASF266:
	.ascii	"port_B_device_index\000"
.LASF445:
	.ascii	"there_was_a_MLB_during_all_other_times\000"
.LASF382:
	.ascii	"STATION_REPORT_DATA_RECORD\000"
.LASF792:
	.ascii	"stop_dt\000"
.LASF664:
	.ascii	"pbox_index_0\000"
.LASF195:
	.ascii	"results_manual_programs_and_programmed_irrigation_c"
	.ascii	"lashed\000"
.LASF472:
	.ascii	"reduction_gallons\000"
.LASF42:
	.ascii	"rre_on_sxr_to_pause\000"
.LASF436:
	.ascii	"skip_irrigation_till_due_to_calendar_NOW_date\000"
.LASF346:
	.ascii	"pi_first_cycle_start_time\000"
.LASF793:
	.ascii	"may_remove\000"
.LASF513:
	.ascii	"flow_check_tolerance_plus_gpm\000"
.LASF833:
	.ascii	"one_ON_for_walk_thru\000"
.LASF527:
	.ascii	"double\000"
.LASF665:
	.ascii	"pstation_number_0\000"
.LASF316:
	.ascii	"stop_key_pressed\000"
.LASF746:
	.ascii	"tkos\000"
.LASF229:
	.ascii	"ufim_highest_pump_reason_in_list_available_to_turn_"
	.ascii	"ON\000"
.LASF422:
	.ascii	"flow_status\000"
.LASF218:
	.ascii	"ufim_list_contains_waiting_pump_valves_b\000"
.LASF678:
	.ascii	"pfor_all_reasons\000"
.LASF520:
	.ascii	"mvor_stop_time\000"
.LASF278:
	.ascii	"ACTION_NEEDED_XFER_RECORD\000"
.LASF328:
	.ascii	"mlb_prevented_or_curtailed\000"
.LASF184:
	.ascii	"results_hit_the_stop_time_holding\000"
.LASF52:
	.ascii	"directions_honor_FREEZE_SWITCH\000"
.LASF661:
	.ascii	"ONE_HUNDRED_POINT_ZERO\000"
.LASF158:
	.ascii	"priority_level\000"
.LASF375:
	.ascii	"programmed_irrigation_seconds_irrigated_ul\000"
.LASF723:
	.ascii	"pi_alert_some_skipped_due_to_stop_time\000"
.LASF235:
	.ascii	"is_a_duplicate_in_the_array\000"
.LASF583:
	.ascii	"mvor_request\000"
.LASF231:
	.ascii	"ufim_flow_check_group_count_of_ON\000"
.LASF762:
	.ascii	"ldtcs\000"
.LASF397:
	.ascii	"sync_the_et_rain_tables\000"
.LASF828:
	.ascii	"station_preserves_recursive_MUTEX\000"
.LASF37:
	.ascii	"flow_check_when_possible_based_on_reason_in_list\000"
.LASF795:
	.ascii	"alert_already_made_for_stop_time\000"
.LASF181:
	.ascii	"at_start_of_second_one_or_more_in_the_list_for_prog"
	.ascii	"rammed_irrigation\000"
.LASF214:
	.ascii	"ufim_one_ON_from_the_problem_list_b\000"
.LASF329:
	.ascii	"mvor_closed_prevented_or_curtailed\000"
.LASF253:
	.ascii	"soak_seconds_original_from_station_info\000"
.LASF396:
	.ascii	"rain\000"
.LASF685:
	.ascii	"FOAL_IRRI_load_mlb_info_into_outgoing_token\000"
.LASF731:
	.ascii	"lss_ptr\000"
.LASF682:
	.ascii	"pucp_ptr\000"
.LASF691:
	.ascii	"nm_FOAL_return_pointer_to_next_available_and_fully_"
	.ascii	"cleaned_ilc\000"
.LASF56:
	.ascii	"master_valve_has_at_least_one_normally_closed\000"
.LASF603:
	.ascii	"directions_honor_MOISTURE_BALANCE\000"
.LASF339:
	.ascii	"two_wire_cable_problem\000"
.LASF135:
	.ascii	"weather_terminal_present\000"
.LASF744:
	.ascii	"gid_sys\000"
.LASF565:
	.ascii	"MANUAL_WATER_KICK_OFF_STRUCT\000"
.LASF581:
	.ascii	"send_box_configuration_to_master\000"
.LASF326:
	.ascii	"no_water_by_manual_prevented\000"
.LASF255:
	.ascii	"soak_seconds_remaining_ul\000"
.LASF739:
	.ascii	"RZWWS\000"
.LASF601:
	.ascii	"results_skipped_due_to_2_WIRE_CABLE_OVERHEATED\000"
.LASF201:
	.ascii	"stop_date\000"
.LASF394:
	.ascii	"dls_after_fall_when_to_clear_ignore_sxs\000"
.LASF425:
	.ascii	"skip_irrigation_due_to_calendar_NOW\000"
.LASF240:
	.ascii	"list_of_irrigating_stations\000"
.LASF272:
	.ascii	"reason_in_list_u8\000"
.LASF718:
	.ascii	"pdtcs_ptr\000"
.LASF348:
	.ascii	"pi_seconds_irrigated_ul\000"
.LASF737:
	.ascii	"rain_data\000"
.LASF780:
	.ascii	"groups_on\000"
.LASF615:
	.ascii	"pcontact_index\000"
.LASF209:
	.ascii	"ufim_valves_in_the_list_for_this_system\000"
.LASF502:
	.ascii	"latest_mlb_record\000"
.LASF618:
	.ascii	"lilc_ptr\000"
.LASF292:
	.ascii	"FRF_NO_CHECK_REASON_station_cycles_too_low\000"
.LASF787:
	.ascii	"_nm_anti_chatter_maintenance_sub_function\000"
.LASF64:
	.ascii	"system_level_valves_are_ON_and_has_updated_the_dera"
	.ascii	"te_table\000"
.LASF704:
	.ascii	"FOAL_IRRI_return_stop_date_according_to_stop_time\000"
.LASF35:
	.ascii	"station_is_ON\000"
.LASF612:
	.ascii	"FOAL_IRRI_restart_irrigation_on_reboot_and_when_cha"
	.ascii	"in_goes_down\000"
.LASF695:
	.ascii	"pd_and_r_ptr\000"
.LASF660:
	.ascii	"nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_"
	.ascii	"all_irrigation_lists\000"
.LASF639:
	.ascii	"station_in_list\000"
.LASF815:
	.ascii	"my_tick_count\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF144:
	.ascii	"offset\000"
.LASF116:
	.ascii	"port_a_freewave_sr_set_for_repeater\000"
.LASF495:
	.ascii	"last_off__reason_in_list\000"
.LASF424:
	.ascii	"skip_irrigation_due_to_manual_NOW\000"
.LASF778:
	.ascii	"parray_ptr\000"
.LASF30:
	.ascii	"flow_check_hi_action\000"
.LASF542:
	.ascii	"slow_closing_valve_seconds\000"
.LASF94:
	.ascii	"et_inches_u16_10000u\000"
.LASF179:
	.ascii	"low_flow_action\000"
.LASF338:
	.ascii	"poc_short_cancelled_irrigation\000"
.LASF522:
	.ascii	"budget\000"
.LASF494:
	.ascii	"last_off__station_number_0\000"
.LASF217:
	.ascii	"ufim_list_contains_waiting_programmed_irrigation_b\000"
.LASF501:
	.ascii	"frcs\000"
.LASF696:
	.ascii	"SIXTY_POINT_ZERO\000"
.LASF772:
	.ascii	"str_8\000"
.LASF121:
	.ascii	"unused_13\000"
.LASF122:
	.ascii	"unused_14\000"
.LASF58:
	.ascii	"pump_activate_for_irrigation\000"
.LASF115:
	.ascii	"port_a_freewave_lr_set_for_repeater\000"
.LASF592:
	.ascii	"results_skipped_due_to_MANUAL_NOW\000"
.LASF515:
	.ascii	"flow_check_derated_expected\000"
.LASF291:
	.ascii	"FRF_NO_CHECK_REASON_indicies_out_of_range\000"
.LASF302:
	.ascii	"FRF_NO_UPDATE_REASON_zero_flow_rate\000"
.LASF698:
	.ascii	"irrigation_denied\000"
.LASF389:
	.ascii	"RAIN_STATE\000"
.LASF479:
	.ascii	"highest_reason_in_list\000"
.LASF480:
	.ascii	"ufim_maximum_valves_in_system_we_can_have_ON_now\000"
.LASF824:
	.ascii	"foal_irri\000"
.LASF465:
	.ascii	"non_controller_seconds\000"
.LASF199:
	.ascii	"days\000"
.LASF97:
	.ascii	"rain_inches_u16_100u\000"
.LASF547:
	.ascii	"list_of_foal_stations_ON\000"
.LASF171:
	.ascii	"use_et_averaging_bool\000"
.LASF467:
	.ascii	"SYSTEM_REPORT_RECORD\000"
.LASF450:
	.ascii	"mlb_limit_during_mvor_closed_gpm\000"
.LASF645:
	.ascii	"llist_item\000"
.LASF110:
	.ascii	"option_FL\000"
.LASF836:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF365:
	.ascii	"pi_flag2\000"
.LASF485:
	.ascii	"ufim_the_valves_ON_meet_the_flow_checking_cycles_re"
	.ascii	"quirement\000"
.LASF651:
	.ascii	"plist_hdr\000"
.LASF712:
	.ascii	"pstation_ptr\000"
.LASF23:
	.ascii	"no_longer_used_01\000"
.LASF36:
	.ascii	"no_longer_used_02\000"
.LASF45:
	.ascii	"no_longer_used_03\000"
.LASF66:
	.ascii	"checked_or_updated_and_made_flow_recording_lines\000"
.LASF200:
	.ascii	"start_date\000"
.LASF359:
	.ascii	"pi_last_measured_current_ma\000"
.LASF662:
	.ascii	"lstation\000"
.LASF350:
	.ascii	"pi_flag\000"
.LASF808:
	.ascii	"lthis_station_is_already_on_b\000"
.LASF786:
	.ascii	"pbsr_ptr\000"
.LASF264:
	.ascii	"purchased_options\000"
.LASF6:
	.ascii	"long long int\000"
.LASF311:
	.ascii	"when_to_send_timer\000"
.LASF671:
	.ascii	"FOAL_IRRI_remove_this_station_for_this_reason_from_"
	.ascii	"the_irrigation_lists\000"
.LASF102:
	.ascii	"__month\000"
.LASF742:
	.ascii	"lstation_number_0\000"
.LASF174:
	.ascii	"on_at_a_time__presently_ON_in_station_group_count\000"
.LASF427:
	.ascii	"station_report_data_record_is_in_use\000"
.LASF552:
	.ascii	"timer_freeze_switch\000"
.LASF44:
	.ascii	"rre_in_process_to_turn_ON\000"
.LASF145:
	.ascii	"InUse\000"
.LASF577:
	.ascii	"two_wire_cable_excessive_current\000"
.LASF166:
	.ascii	"a_scheduled_irrigation_date_in_the_past\000"
.LASF529:
	.ascii	"box_configuration\000"
.LASF213:
	.ascii	"ufim_expected_flow_rate_for_those_ON\000"
.LASF381:
	.ascii	"manual_program_seconds_us\000"
.LASF460:
	.ascii	"walk_thru_seconds\000"
.LASF463:
	.ascii	"programmed_irrigation_seconds\000"
.LASF322:
	.ascii	"watersense_min_cycle_zeroed_the_irrigation_time\000"
.LASF398:
	.ascii	"nlu_old_rain_table_sync_variable\000"
.LASF188:
	.ascii	"results_elapsed_irrigation_time_holding\000"
.LASF563:
	.ascii	"manual_how\000"
.LASF285:
	.ascii	"STOP_KEY_RECORD_s\000"
.LASF150:
	.ascii	"pListHdr\000"
.LASF652:
	.ascii	"pilc_ptr\000"
.LASF251:
	.ascii	"cycle_seconds_original_from_station_info\000"
.LASF580:
	.ascii	"clear_runaway_gage_pressed\000"
.LASF269:
	.ascii	"soak_seconds_remaining\000"
.LASF90:
	.ascii	"number_of_pocs_in_this_system\000"
.LASF636:
	.ascii	"ft_station_to_add\000"
.LASF789:
	.ascii	"bsr_ptr\000"
.LASF162:
	.ascii	"schedule_enabled_bool\000"
.LASF249:
	.ascii	"requested_irrigation_seconds_balance_ul\000"
.LASF546:
	.ascii	"list_of_foal_all_irrigation\000"
.LASF459:
	.ascii	"test_seconds\000"
.LASF426:
	.ascii	"did_not_irrigate_last_time\000"
.LASF819:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF508:
	.ascii	"flow_check_allow_table_to_lock\000"
.LASF488:
	.ascii	"system_master_most_recent_5_second_average\000"
.LASF740:
	.ascii	"Vp_flag\000"
.LASF727:
	.ascii	"number_of_mp_starts\000"
.LASF38:
	.ascii	"flow_check_to_be_excluded_from_future_checking\000"
.LASF88:
	.ascii	"delivered_MVOR_in_effect_closed\000"
.LASF123:
	.ascii	"unused_15\000"
.LASF161:
	.ascii	"percent_adjust_end_date\000"
.LASF247:
	.ascii	"user_programmed_seconds\000"
.LASF788:
	.ascii	"_load_and_maintain_system_anti_chatter_timers\000"
.LASF506:
	.ascii	"flow_check_required_station_cycles\000"
.LASF769:
	.ascii	"set_remaining_seconds_on\000"
.LASF29:
	.ascii	"w_involved_in_a_flow_problem\000"
.LASF343:
	.ascii	"moisture_balance_prevented_irrigation\000"
.LASF505:
	.ascii	"derate_cell_iterations\000"
.LASF486:
	.ascii	"flow_checking_block_out_remaining_seconds\000"
.LASF745:
	.ascii	"FOAL_IRRI_initiate_a_station_test\000"
.LASF536:
	.ascii	"action_reason\000"
.LASF108:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF804:
	.ascii	"pause_reason\000"
.LASF137:
	.ascii	"dash_m_terminal_present\000"
.LASF223:
	.ascii	"ufim_there_is_a_PUMP_mix_condition_b\000"
.LASF587:
	.ascii	"results_irrigation_list_full\000"
.LASF452:
	.ascii	"mlb_limit_during_all_other_times_gpm\000"
.LASF270:
	.ascii	"cycle_seconds\000"
.LASF325:
	.ascii	"flow_never_checked\000"
.LASF59:
	.ascii	"system_level_no_valves_ON_therefore_no_flow_checkin"
	.ascii	"g\000"
.LASF751:
	.ascii	"number_added\000"
.LASF477:
	.ascii	"last_rollover_day\000"
.LASF570:
	.ascii	"INITIATED_VIA_ENUM\000"
.LASF78:
	.ascii	"one_or_more_in_list_for_rre\000"
.LASF75:
	.ascii	"one_or_more_in_list_for_manual\000"
.LASF18:
	.ascii	"unsigned int\000"
.LASF206:
	.ascii	"capacity_in_use_bool\000"
.LASF12:
	.ascii	"xTimerHandle\000"
.LASF72:
	.ascii	"MVOR_in_effect_closed\000"
.LASF806:
	.ascii	"tmp_ilc\000"
.LASF573:
	.ascii	"light_on_request\000"
.LASF347:
	.ascii	"pi_last_cycle_end_time\000"
.LASF524:
	.ascii	"BY_SYSTEM_RECORD\000"
.LASF337:
	.ascii	"mois_max_water_day\000"
.LASF817:
	.ascii	"ft_irrigating_stations_list_hdr\000"
.LASF3:
	.ascii	"short int\000"
.LASF345:
	.ascii	"record_start_time\000"
.LASF779:
	.ascii	"FOAL_IRRI_there_is_more_than_one_flow_group_ON\000"
.LASF400:
	.ascii	"dont_use_et_gage_today\000"
.LASF154:
	.ascii	"list_support_station_groups\000"
.LASF829:
	.ascii	"system_preserves_recursive_MUTEX\000"
.LASF750:
	.ascii	"lwalk_thru\000"
.LASF104:
	.ascii	"__hours\000"
.LASF410:
	.ascii	"commserver_monthly_xmit_bytes\000"
.LASF498:
	.ascii	"transition_timer_all_pump_valves_are_OFF\000"
.LASF708:
	.ascii	"pstation\000"
.LASF724:
	.ascii	"pi_alert_some_skipped_due_to_mow_day\000"
.LASF554:
	.ascii	"ilcs\000"
.LASF770:
	.ascii	"will_exceed_system_capacity\000"
.LASF492:
	.ascii	"system_stability_averages_ring\000"
.LASF313:
	.ascii	"pi_flow_data_has_been_stamped\000"
.LASF729:
	.ascii	"mp_seconds\000"
.LASF590:
	.ascii	"results_skipped_as_there_is_a_MVOR_CLOSED\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
