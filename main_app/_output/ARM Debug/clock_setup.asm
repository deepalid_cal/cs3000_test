	.file	"clock_setup.c"
	.text
.Ltext0:
	.section	.text.clock_setup,"ax",%progbits
	.align	2
	.global	clock_setup
	.type	clock_setup, %function
clock_setup:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/clock_setup.c"
	.loc 1 51 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #40
.LCFI2:
	str	r0, [fp, #-36]
	str	r1, [fp, #-40]
	str	r2, [fp, #-44]
	.loc 1 54 0
	mov	r0, #0
	mov	r1, #1
	mov	r2, #2
	bl	clkpwr_set_hclk_divs
	.loc 1 56 0
	mov	r0, #1
	bl	clkpwr_set_mode
	.loc 1 58 0
	mov	r0, #1
	mov	r1, #0
	bl	clkpwr_pll_dis_en
	.loc 1 60 0
	ldr	r0, .L6
	mov	r1, #2
	bl	timer_wait_ms
	.loc 1 63 0
	bl	clkpwr_get_osc
	mov	r3, r0
	cmp	r3, #1
	bne	.L2
	.loc 1 68 0
	mov	r0, #0
	mov	r1, #1
	bl	clkpwr_mainosc_setup
	.loc 1 71 0
	ldr	r0, .L6
	mov	r1, #100
	bl	timer_wait_ms
	.loc 1 74 0
	mov	r0, #0
	mov	r1, #80
	bl	clkpwr_sysclk_setup
	.loc 1 76 0
	mov	r0, #0
	mov	r1, #0
	mov	r2, #0
	bl	clkpwr_pll397_setup
	b	.L3
.L2:
	.loc 1 81 0
	mov	r0, #0
	mov	r1, #80
	bl	clkpwr_sysclk_setup
.L3:
	.loc 1 86 0
	sub	r3, fp, #32
	ldr	r0, .L6+4
	ldr	r1, [fp, #-36]
	mov	r2, #5
	bl	clkpwr_find_pll_cfg
	str	r0, [fp, #-36]
	.loc 1 88 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L1
	.loc 1 92 0
	sub	r3, fp, #32
	mov	r0, r3
	bl	clkpwr_hclkpll_setup
	.loc 1 95 0
	mov	r0, r0	@ nop
.L5:
	.loc 1 95 0 is_stmt 0 discriminator 1
	mov	r0, #1
	bl	clkpwr_is_pll_locked
	mov	r3, r0
	cmp	r3, #0
	beq	.L5
	.loc 1 99 0 is_stmt 1
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-40]
	mov	r0, #0
	mov	r1, r2
	mov	r2, r3
	bl	clkpwr_set_hclk_divs
	.loc 1 102 0
	mov	r0, #0
	bl	clkpwr_force_arm_hclk_to_pclk
	.loc 1 104 0
	mov	r0, #0
	bl	clkpwr_set_mode
.L1:
	.loc 1 106 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L7:
	.align	2
.L6:
	.word	1074020352
	.word	13000000
.LFE0:
	.size	clock_setup, .-clock_setup
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x277
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF33
	.byte	0x1
	.4byte	.LASF34
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x5e
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x67
	.4byte	0x65
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x5
	.4byte	0x48
	.uleb128 0x6
	.4byte	0x48
	.4byte	0x96
	.uleb128 0x7
	.4byte	0x7a
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.byte	0x3
	.byte	0x74
	.4byte	0xab
	.uleb128 0x9
	.4byte	.LASF11
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF12
	.sleb128 1
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.byte	0x3
	.byte	0x7b
	.4byte	0xc6
	.uleb128 0x9
	.4byte	.LASF13
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF14
	.sleb128 1
	.uleb128 0x9
	.4byte	.LASF15
	.sleb128 2
	.byte	0
	.uleb128 0xa
	.byte	0x1c
	.byte	0x3
	.byte	0x82
	.4byte	0x131
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x3
	.byte	0x85
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x3
	.byte	0x88
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x3
	.byte	0x8b
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x3
	.byte	0x8d
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x3
	.byte	0x8f
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x3
	.byte	0x91
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x3
	.byte	0x93
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x3
	.4byte	.LASF23
	.byte	0x3
	.byte	0x94
	.4byte	0xc6
	.uleb128 0x8
	.byte	0x4
	.byte	0x3
	.byte	0xfb
	.4byte	0x157
	.uleb128 0x9
	.4byte	.LASF24
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF25
	.sleb128 1
	.uleb128 0x9
	.4byte	.LASF26
	.sleb128 2
	.byte	0
	.uleb128 0xa
	.byte	0x74
	.byte	0x4
	.byte	0x28
	.4byte	0x202
	.uleb128 0xc
	.ascii	"ir\000"
	.byte	0x4
	.byte	0x2a
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.ascii	"tcr\000"
	.byte	0x4
	.byte	0x2b
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.ascii	"tc\000"
	.byte	0x4
	.byte	0x2c
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.ascii	"pr\000"
	.byte	0x4
	.byte	0x2d
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.ascii	"pc\000"
	.byte	0x4
	.byte	0x2e
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.ascii	"mcr\000"
	.byte	0x4
	.byte	0x2f
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.ascii	"mr\000"
	.byte	0x4
	.byte	0x30
	.4byte	0x202
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.ascii	"ccr\000"
	.byte	0x4
	.byte	0x31
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xc
	.ascii	"cr\000"
	.byte	0x4
	.byte	0x32
	.4byte	0x207
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xc
	.ascii	"emr\000"
	.byte	0x4
	.byte	0x33
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x4
	.byte	0x34
	.4byte	0x21c
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x4
	.byte	0x35
	.4byte	0x81
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.byte	0
	.uleb128 0x5
	.4byte	0x86
	.uleb128 0x5
	.4byte	0x86
	.uleb128 0x6
	.4byte	0x48
	.4byte	0x21c
	.uleb128 0x7
	.4byte	0x7a
	.byte	0xb
	.byte	0
	.uleb128 0x5
	.4byte	0x20c
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x4
	.byte	0x36
	.4byte	0x157
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.byte	0x32
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0xe
	.4byte	.LASF30
	.byte	0x1
	.byte	0x32
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0xe
	.4byte	.LASF31
	.byte	0x1
	.byte	0x32
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0xe
	.4byte	.LASF32
	.byte	0x1
	.byte	0x32
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x1
	.byte	0x34
	.4byte	0x131
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF19:
	.ascii	"fdbk_div_ctrl_b13\000"
.LASF24:
	.ascii	"CLKPWR_MD_RUN\000"
.LASF27:
	.ascii	"rsvd2\000"
.LASF17:
	.ascii	"cco_bypass_b15\000"
.LASF15:
	.ascii	"CLKPWR_USB_PLL\000"
.LASF33:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"unsigned int\000"
.LASF11:
	.ascii	"CLKPWR_MAIN_OSC\000"
.LASF13:
	.ascii	"CLKPWR_PLL397\000"
.LASF32:
	.ascii	"pdiv\000"
.LASF25:
	.ascii	"CLKPWR_MD_DIRECTRUN\000"
.LASF29:
	.ascii	"TIMER_CNTR_REGS_T\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF7:
	.ascii	"INT_32\000"
.LASF10:
	.ascii	"long unsigned int\000"
.LASF23:
	.ascii	"CLKPWR_HCLK_PLL_SETUP_T\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF18:
	.ascii	"direct_output_b14\000"
.LASF16:
	.ascii	"analog_on\000"
.LASF34:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/clock_setup.c\000"
.LASF26:
	.ascii	"CLKPWR_MODE_STOP\000"
.LASF22:
	.ascii	"pll_m\000"
.LASF21:
	.ascii	"pll_n\000"
.LASF20:
	.ascii	"pll_p\000"
.LASF8:
	.ascii	"long long unsigned int\000"
.LASF12:
	.ascii	"CLKPWR_PLL397_OSC\000"
.LASF9:
	.ascii	"long long int\000"
.LASF14:
	.ascii	"CLKPWR_HCLK_PLL\000"
.LASF0:
	.ascii	"char\000"
.LASF36:
	.ascii	"pllcfg\000"
.LASF35:
	.ascii	"clock_setup\000"
.LASF4:
	.ascii	"short int\000"
.LASF28:
	.ascii	"ctcr\000"
.LASF6:
	.ascii	"UNS_32\000"
.LASF31:
	.ascii	"hdiv\000"
.LASF2:
	.ascii	"signed char\000"
.LASF30:
	.ascii	"clkrate\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
