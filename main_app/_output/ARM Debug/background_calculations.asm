	.file	"background_calculations.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.STATUS_next_scheduled_calc_queued,"aw",%nobits
	.align	2
	.type	STATUS_next_scheduled_calc_queued, %object
	.size	STATUS_next_scheduled_calc_queued, 4
STATUS_next_scheduled_calc_queued:
	.space	4
	.section	.bss.__largest_next_scheduled_delta,"aw",%nobits
	.align	2
	.type	__largest_next_scheduled_delta, %object
	.size	__largest_next_scheduled_delta, 4
__largest_next_scheduled_delta:
	.space	4
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/b"
	.ascii	"ackground_calculations.c\000"
	.align	2
.LC1:
	.ascii	"%s %s\000"
	.align	2
.LC2:
	.ascii	" on %s, %s %d, %d\000"
	.align	2
.LC3:
	.ascii	"\000"
	.section	.text.STATUS_load_next_scheduled_into_guivars,"ax",%progbits
	.align	2
	.type	STATUS_load_next_scheduled_into_guivars, %function
STATUS_load_next_scheduled_into_guivars:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/background_calculations.c"
	.loc 1 53 0
	@ args = 0, pretend = 0, frame = 196
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI0:
	add	fp, sp, #8
.LCFI1:
	sub	sp, sp, #208
.LCFI2:
	.loc 1 92 0
	ldr	r3, .L53
	ldr	r3, [r3, #0]
	str	r3, [fp, #-32]
	.loc 1 95 0
	sub	r3, fp, #76
	mov	r0, r3
	bl	EPSON_obtain_latest_complete_time_and_date
	.loc 1 98 0
	ldrh	r3, [fp, #-72]
	str	r3, [fp, #-36]
	.loc 1 101 0
	ldr	r3, [fp, #-76]
	ldr	r2, .L53+4
	umull	r1, r2, r3, r2
	mov	r2, r2, lsr #6
	mov	r1, #600
	mul	r2, r1, r2
	rsb	r3, r2, r3
	str	r3, [fp, #-40]
	.loc 1 103 0
	ldr	r3, [fp, #-40]
	cmp	r3, #0
	beq	.L2
	.loc 1 105 0
	ldr	r2, [fp, #-76]
	ldr	r3, [fp, #-40]
	rsb	r3, r3, r2
	add	r3, r3, #600
	str	r3, [fp, #-76]
.L2:
	.loc 1 109 0
	ldr	r2, [fp, #-76]
	ldr	r3, .L53+8
	cmp	r2, r3
	bls	.L3
	.loc 1 111 0
	mov	r3, #0
	str	r3, [fp, #-76]
	.loc 1 112 0
	ldrh	r3, [fp, #-72]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-72]	@ movhi
.L3:
	.loc 1 115 0
	ldr	r3, .L53+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 116 0
	ldr	r3, .L53+16
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 118 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 120 0
	bl	NETWORK_CONFIG_get_controller_off
	mov	r3, r0
	cmp	r3, #0
	bne	.L4
	.loc 1 124 0
	mov	r3, #0
	str	r3, [fp, #-24]
	b	.L5
.L39:
	.loc 1 126 0
	ldrh	r3, [fp, #-72]
	mov	r0, r3
	sub	r1, fp, #80
	sub	r2, fp, #84
	sub	r3, fp, #88
	sub	ip, fp, #92
	str	ip, [sp, #0]
	bl	DateToDMY
	.loc 1 132 0
	ldr	r3, [fp, #-84]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-68]	@ movhi
	.loc 1 133 0
	ldr	r3, [fp, #-80]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-70]	@ movhi
	.loc 1 134 0
	ldr	r3, [fp, #-88]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-66]	@ movhi
	.loc 1 135 0
	ldr	r3, [fp, #-92]
	and	r3, r3, #255
	strb	r3, [fp, #-58]
	.loc 1 137 0
	b	.L6
.L37:
	.loc 1 145 0
	mov	r3, #0
	str	r3, [fp, #-28]
	b	.L7
.L23:
	.loc 1 147 0
	ldr	r0, [fp, #-28]
	bl	MANUAL_PROGRAMS_get_group_at_this_index
	str	r0, [fp, #-44]
	.loc 1 149 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L8
	.loc 1 153 0
	ldrh	r3, [fp, #-72]
	mov	r4, r3
	ldr	r0, [fp, #-44]
	bl	MANUAL_PROGRAMS_get_start_date
	mov	r3, r0
	cmp	r4, r3
	bcc	.L47
	.loc 1 153 0 is_stmt 0 discriminator 1
	ldrh	r3, [fp, #-72]
	mov	r4, r3
	ldr	r0, [fp, #-44]
	bl	MANUAL_PROGRAMS_get_end_date
	mov	r3, r0
	cmp	r4, r3
	bhi	.L47
	ldr	r3, [fp, #-92]
	ldr	r0, [fp, #-44]
	mov	r1, r3
	bl	MANUAL_PROGRAMS_today_is_a_water_day
	mov	r3, r0
	cmp	r3, #1
	bne	.L47
	.loc 1 155 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L10
.L19:
	.loc 1 157 0
	ldr	r0, [fp, #-44]
	ldr	r1, [fp, #-16]
	bl	MANUAL_PROGRAMS_get_start_time
	mov	r2, r0
	ldr	r3, [fp, #-76]
	cmp	r2, r3
	bne	.L11
	.loc 1 164 0
	ldr	r3, .L53+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L53+24
	mov	r3, #164
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 166 0
	ldr	r0, .L53+28
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 168 0
	ldr	r3, .L53+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 170 0
	b	.L12
.L17:
	.loc 1 172 0
	ldr	r0, [fp, #-44]
	bl	nm_GROUP_get_group_ID
	mov	r3, r0
	ldr	r0, [fp, #-12]
	mov	r1, r3
	bl	STATION_does_this_station_belong_to_this_manual_program
	mov	r3, r0
	cmp	r3, #0
	beq	.L13
	.loc 1 172 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-12]
	bl	STATION_station_is_available_for_use
	mov	r3, r0
	cmp	r3, #0
	beq	.L13
	.loc 1 174 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-48]
	.loc 1 176 0
	ldr	r0, [fp, #-44]
	bl	MANUAL_PROGRAMS_get_run_time
	str	r0, [fp, #-48]
	.loc 1 178 0
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	beq	.L13
	.loc 1 180 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 182 0
	ldr	r0, [fp, #-12]
	bl	STATION_get_no_water_days
	str	r0, [fp, #-52]
	.loc 1 184 0
	ldr	r2, [fp, #-52]
	ldr	r3, [fp, #-24]
	rsb	r3, r3, r2
	cmp	r3, #0
	blt	.L14
	.loc 1 184 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-52]
	ldr	r3, [fp, #-24]
	cmp	r2, r3
	bne	.L13
	ldr	r3, [fp, #-76]
	cmp	r3, #14400
	bls	.L13
.L14:
	.loc 1 186 0 is_stmt 1
	ldr	r3, .L53+12
	mov	r2, #2
	str	r2, [r3, #0]
	.loc 1 188 0
	ldr	r0, [fp, #-44]
	bl	nm_GROUP_get_group_ID
	mov	r2, r0
	ldr	r3, .L53+16
	str	r2, [r3, #0]
	.loc 1 190 0
	ldr	r0, [fp, #-44]
	bl	nm_GROUP_get_name
	mov	r4, r0
	ldr	r0, .L53+32
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r2, r0
	sub	r3, fp, #140
	str	r2, [sp, #0]
	mov	r0, r3
	mov	r1, #48
	ldr	r2, .L53+36
	mov	r3, r4
	bl	snprintf
	.loc 1 192 0
	ldr	r3, [fp, #-76]
	sub	r2, fp, #204
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, #64
	mov	r2, r3
	mov	r3, #0
	bl	TDUTILS_time_to_time_string_with_ampm
	.loc 1 194 0
	ldrh	r3, [fp, #-72]
	mov	r2, r3
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bne	.L15
	.loc 1 196 0
	ldr	r0, .L53+40
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	sub	r2, fp, #204
	mov	r0, r2
	mov	r1, r3
	mov	r2, #64
	bl	strlcat
	.loc 1 203 0
	b	.L11
.L15:
	.loc 1 200 0
	ldr	r3, [fp, #-92]
	mov	r0, r3
	bl	GetDayLongStr
	mov	r4, r0
	ldr	r3, [fp, #-84]
	sub	r3, r3, #1
	mov	r0, r3
	bl	GetMonthShortStr
	ldr	r1, [fp, #-80]
	ldr	r2, [fp, #-88]
	sub	r3, fp, #204
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	str	r2, [sp, #8]
	mov	r0, r3
	mov	r1, #64
	ldr	r2, .L53+44
	mov	r3, r4
	bl	sp_strlcat
	.loc 1 203 0
	b	.L11
.L13:
	.loc 1 208 0
	ldr	r3, .L53+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L53+24
	mov	r3, #208
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 210 0
	ldr	r0, .L53+28
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
	.loc 1 212 0
	ldr	r3, .L53+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 217 0
	ldr	r3, .L53+48
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L48
.L12:
	.loc 1 170 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L17
	b	.L11
.L48:
	.loc 1 219 0
	mov	r0, r0	@ nop
.L11:
	.loc 1 224 0
	ldr	r3, .L53+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L49
.L18:
	.loc 1 155 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L10:
	.loc 1 155 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #5
	bls	.L19
	.loc 1 155 0
	b	.L47
.L8:
	.loc 1 233 0 is_stmt 1
	ldr	r0, .L53+24
	mov	r1, #233
	bl	Alert_group_not_found_with_filename
	b	.L20
.L47:
	.loc 1 155 0
	mov	r0, r0	@ nop
	b	.L20
.L49:
	.loc 1 226 0
	mov	r0, r0	@ nop
.L20:
	.loc 1 236 0
	ldr	r3, .L53+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L50
.L21:
	.loc 1 145 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
.L7:
	.loc 1 145 0 is_stmt 0 discriminator 1
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	mov	r2, r0
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bhi	.L23
	b	.L22
.L50:
	.loc 1 238 0 is_stmt 1
	mov	r0, r0	@ nop
.L22:
	.loc 1 243 0
	ldr	r3, .L53+48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L24
	.loc 1 243 0 is_stmt 0 discriminator 1
	ldr	r3, .L53+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L24
	.loc 1 245 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-28]
	b	.L25
.L35:
	.loc 1 247 0
	ldr	r0, [fp, #-28]
	bl	STATION_GROUP_get_group_at_this_index
	str	r0, [fp, #-56]
	.loc 1 249 0
	ldr	r3, [fp, #-56]
	cmp	r3, #0
	beq	.L26
	.loc 1 253 0
	ldr	r4, [fp, #-76]
	ldr	r0, [fp, #-56]
	bl	SCHEDULE_get_start_time
	mov	r3, r0
	cmp	r4, r3
	bne	.L27
	.loc 1 253 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-56]
	bl	SCHEDULE_get_start_time
	mov	r4, r0
	ldr	r0, [fp, #-56]
	bl	SCHEDULE_get_stop_time
	mov	r3, r0
	cmp	r4, r3
	beq	.L27
	.loc 1 255 0 is_stmt 1
	sub	r3, fp, #76
	ldr	r0, [fp, #-56]
	mov	r1, r3
	mov	r2, #0
	bl	SCHEDULE_does_it_irrigate_on_this_date
	mov	r3, r0
	cmp	r3, #0
	beq	.L27
	.loc 1 257 0
	ldr	r3, .L53+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L53+24
	ldr	r3, .L53+52
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 259 0
	ldr	r0, .L53+28
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 1 261 0
	ldr	r3, .L53+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 263 0
	b	.L28
.L32:
	.loc 1 265 0
	ldr	r0, [fp, #-12]
	bl	STATION_station_is_available_for_use
	mov	r3, r0
	cmp	r3, #1
	bne	.L29
	.loc 1 265 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-12]
	bl	STATION_get_GID_station_group
	mov	r4, r0
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_group_ID
	mov	r3, r0
	cmp	r4, r3
	bne	.L29
	.loc 1 267 0 is_stmt 1
	ldr	r0, [fp, #-12]
	bl	STATION_get_no_water_days
	str	r0, [fp, #-52]
	.loc 1 269 0
	ldr	r0, [fp, #-12]
	bl	WEATHER_get_station_uses_daily_et
	mov	r3, r0
	cmp	r3, #1
	bne	.L30
	.loc 1 271 0
	ldr	r0, [fp, #-12]
	bl	STATION_get_ET_factor_100u
	mov	r3, r0
	cmp	r3, #0
	beq	.L29
	.loc 1 273 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 275 0
	ldr	r2, [fp, #-52]
	ldr	r3, [fp, #-24]
	rsb	r3, r3, r2
	cmp	r3, #0
	bgt	.L29
	.loc 1 277 0
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_group_ID
	mov	r2, r0
	ldr	r3, .L53+16
	str	r2, [r3, #0]
	.loc 1 279 0
	ldr	r3, .L53+12
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 281 0
	b	.L31
.L30:
	.loc 1 285 0
	ldr	r0, [fp, #-12]
	bl	STATION_get_total_run_minutes_10u
	mov	r3, r0
	cmp	r3, #0
	beq	.L29
	.loc 1 287 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 1 289 0
	ldr	r2, [fp, #-52]
	ldr	r3, [fp, #-24]
	rsb	r3, r3, r2
	cmp	r3, #0
	bgt	.L29
	.loc 1 291 0
	ldr	r3, .L53+12
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 293 0
	b	.L31
.L29:
	.loc 1 298 0
	ldr	r3, .L53+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L53+24
	ldr	r3, .L53+56
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 300 0
	ldr	r0, .L53+28
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
	.loc 1 302 0
	ldr	r3, .L53+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 307 0
	ldr	r3, .L53+48
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L51
.L28:
	.loc 1 263 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L32
	b	.L31
.L51:
	.loc 1 309 0
	mov	r0, r0	@ nop
.L31:
	.loc 1 313 0
	ldr	r3, .L53+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L27
	.loc 1 315 0
	ldr	r0, [fp, #-56]
	bl	nm_GROUP_get_name
	mov	r4, r0
	ldr	r0, .L53+32
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r2, r0
	sub	r3, fp, #140
	str	r2, [sp, #0]
	mov	r0, r3
	mov	r1, #48
	ldr	r2, .L53+36
	mov	r3, r4
	bl	snprintf
	.loc 1 317 0
	ldr	r3, [fp, #-76]
	sub	r2, fp, #204
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, #64
	mov	r2, r3
	mov	r3, #0
	bl	TDUTILS_time_to_time_string_with_ampm
	.loc 1 319 0
	ldrh	r3, [fp, #-72]
	mov	r2, r3
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bne	.L33
	.loc 1 321 0
	ldr	r0, .L53+40
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	sub	r2, fp, #204
	mov	r0, r2
	mov	r1, r3
	mov	r2, #64
	bl	strlcat
	.loc 1 328 0
	b	.L24
.L33:
	.loc 1 325 0
	ldr	r3, [fp, #-92]
	mov	r0, r3
	bl	GetDayLongStr
	mov	r4, r0
	ldr	r3, [fp, #-84]
	sub	r3, r3, #1
	mov	r0, r3
	bl	GetMonthShortStr
	ldr	r1, [fp, #-80]
	ldr	r2, [fp, #-88]
	sub	r3, fp, #204
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	str	r2, [sp, #8]
	mov	r0, r3
	mov	r1, #64
	ldr	r2, .L53+44
	mov	r3, r4
	bl	sp_strlcat
	.loc 1 328 0
	b	.L24
.L26:
	.loc 1 335 0
	ldr	r0, .L53+24
	ldr	r1, .L53+60
	bl	Alert_group_not_found_with_filename
.L27:
	.loc 1 245 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
.L25:
	.loc 1 245 0 is_stmt 0 discriminator 1
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r2, r0
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bhi	.L35
.L24:
	.loc 1 342 0 is_stmt 1
	ldr	r3, [fp, #-76]
	add	r3, r3, #600
	str	r3, [fp, #-76]
.L6:
	.loc 1 137 0 discriminator 1
	ldr	r2, [fp, #-76]
	ldr	r3, .L53+8
	cmp	r2, r3
	bhi	.L36
	.loc 1 137 0 is_stmt 0 discriminator 2
	ldr	r3, .L53+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L37
.L36:
	.loc 1 347 0 is_stmt 1
	ldr	r3, .L53+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L52
.L38:
	.loc 1 354 0
	mov	r3, #0
	str	r3, [fp, #-76]
	.loc 1 357 0
	ldrh	r3, [fp, #-72]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-72]	@ movhi
	.loc 1 124 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
.L5:
	.loc 1 124 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	cmp	r3, #37
	bls	.L39
	b	.L4
.L52:
	.loc 1 349 0 is_stmt 1
	mov	r0, r0	@ nop
.L4:
	.loc 1 365 0
	ldr	r3, .L53+12
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L40
	.loc 1 366 0 discriminator 1
	ldr	r3, .L53+12
	ldr	r3, [r3, #0]
	.loc 1 365 0 discriminator 1
	cmp	r3, #1
	bne	.L41
.L40:
	.loc 1 368 0
	sub	r3, fp, #140
	ldr	r0, .L53+64
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 369 0
	sub	r3, fp, #204
	ldr	r0, .L53+68
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	b	.L42
.L41:
	.loc 1 371 0
	bl	NETWORK_CONFIG_get_controller_off
	mov	r3, r0
	cmp	r3, #0
	beq	.L43
	.loc 1 373 0
	ldr	r0, .L53+72
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	ldr	r0, .L53+64
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 374 0
	ldr	r0, .L53+76
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	ldr	r0, .L53+68
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	b	.L42
.L43:
	.loc 1 376 0
	bl	STATION_get_num_stations_assigned_to_groups
	mov	r3, r0
	cmp	r3, #0
	bne	.L44
	.loc 1 378 0
	ldr	r3, .L53+12
	mov	r2, #3
	str	r2, [r3, #0]
	.loc 1 380 0
	ldr	r0, .L53+76
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	ldr	r0, .L53+64
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 381 0
	ldr	r0, .L53+80
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	ldr	r0, .L53+68
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	b	.L42
.L44:
	.loc 1 383 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L45
	.loc 1 385 0
	ldr	r0, .L53+84
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r3, r0
	ldr	r0, .L53+64
	mov	r1, r3
	mov	r2, #49
	bl	strlcpy
	.loc 1 386 0
	ldr	r0, .L53+68
	ldr	r1, .L53+88
	mov	r2, #49
	bl	strlcpy
	b	.L42
.L45:
	.loc 1 390 0
	ldr	r0, .L53+64
	ldr	r1, .L53+88
	mov	r2, #49
	bl	strlcpy
	.loc 1 391 0
	ldr	r0, .L53+68
	ldr	r1, .L53+88
	mov	r2, #49
	bl	strlcpy
.L42:
	.loc 1 394 0
	ldr	r3, .L53
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-32]
	rsb	r3, r3, r2
	str	r3, [fp, #-32]
	.loc 1 396 0
	ldr	r3, .L53+92
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-32]
	cmp	r2, r3
	bls	.L1
	.loc 1 398 0
	ldr	r3, .L53+92
	ldr	r2, [fp, #-32]
	str	r2, [r3, #0]
.L1:
	.loc 1 400 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L54:
	.align	2
.L53:
	.word	my_tick_count
	.word	458129845
	.word	86399
	.word	GuiVar_StatusTypeOfSchedule
	.word	GuiVar_StatusNextScheduledIrriGID
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	station_info_list_hdr
	.word	1112
	.word	.LC1
	.word	1089
	.word	.LC2
	.word	STATUS_next_scheduled_calc_queued
	.word	257
	.word	298
	.word	335
	.word	GuiVar_StatusNextScheduledIrriType
	.word	GuiVar_StatusNextScheduledIrriDate
	.word	902
	.word	1085
	.word	885
	.word	1084
	.word	.LC3
	.word	__largest_next_scheduled_delta
.LFE0:
	.size	STATUS_load_next_scheduled_into_guivars, .-STATUS_load_next_scheduled_into_guivars
	.section	.text.background_calculation_task,"ax",%progbits
	.align	2
	.global	background_calculation_task
	.type	background_calculation_task, %function
background_calculation_task:
.LFB1:
	.loc 1 404 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #12
.LCFI5:
	str	r0, [fp, #-16]
	.loc 1 413 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L60
	rsb	r3, r3, r2
	mov	r3, r3, lsr #5
	str	r3, [fp, #-8]
	.loc 1 417 0
	ldr	r3, .L60+4
	mov	r2, #0
	str	r2, [r3, #0]
.L59:
	.loc 1 422 0
	ldr	r3, .L60+8
	ldr	r2, [r3, #0]
	sub	r3, fp, #12
	mov	r0, r2
	mov	r1, r3
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	mov	r3, r0
	cmp	r3, #1
	bne	.L56
	.loc 1 437 0
	mov	r0, #240
	bl	vTaskDelay
	.loc 1 439 0
	ldr	r3, [fp, #-12]
	ldr	r2, .L60+12
	cmp	r3, r2
	beq	.L57
	ldr	r2, .L60+16
	cmp	r3, r2
	.loc 1 448 0
	b	.L56
.L57:
	.loc 1 442 0
	ldr	r3, .L60+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 444 0
	bl	STATUS_load_next_scheduled_into_guivars
	.loc 1 445 0
	mov	r0, r0	@ nop
.L56:
	.loc 1 455 0
	bl	xTaskGetTickCount
	mov	r1, r0
	ldr	r3, .L60+20
	ldr	r2, [fp, #-8]
	str	r1, [r3, r2, asl #2]
	.loc 1 459 0
	b	.L59
.L61:
	.align	2
.L60:
	.word	Task_Table
	.word	STATUS_next_scheduled_calc_queued
	.word	Background_Calculation_Queue
	.word	4369
	.word	8738
	.word	task_last_execution_stamp
.LFE1:
	.size	background_calculation_task, .-background_calculation_task
	.section .rodata
	.align	2
.LC4:
	.ascii	"Background Calculation QUEUE OVERFLOW!\000"
	.section	.text.postBackground_Calculation_Event,"ax",%progbits
	.align	2
	.global	postBackground_Calculation_Event
	.type	postBackground_Calculation_Event, %function
postBackground_Calculation_Event:
.LFB2:
	.loc 1 465 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #4
.LCFI8:
	str	r0, [fp, #-8]
	.loc 1 468 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L69
	cmp	r3, r2
	beq	.L64
	ldr	r2, .L69+4
	cmp	r3, r2
	.loc 1 490 0
	b	.L62
.L64:
	.loc 1 475 0
	ldr	r3, .L69+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L68
	.loc 1 478 0
	ldr	r3, .L69+12
	ldr	r2, [r3, #0]
	sub	r3, fp, #8
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	mov	r3, r0
	cmp	r3, #0
	bne	.L67
	.loc 1 480 0
	ldr	r0, .L69+16
	bl	Alert_Message
	.loc 1 487 0
	b	.L68
.L67:
	.loc 1 484 0
	ldr	r3, .L69+8
	mov	r2, #1
	str	r2, [r3, #0]
.L68:
	.loc 1 487 0
	mov	r0, r0	@ nop
.L62:
	.loc 1 492 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L70:
	.align	2
.L69:
	.word	4369
	.word	8738
	.word	STATUS_next_scheduled_calc_queued
	.word	Background_Calculation_Queue
	.word	.LC4
.LFE2:
	.size	postBackground_Calculation_Event, .-postBackground_Calculation_Event
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/projdefs.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/stations.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/manual_programs.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/station_groups.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/FreeRTOSConfig.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6c1
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF91
	.byte	0x1
	.4byte	.LASF92
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x67
	.4byte	0x82
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x70
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x47
	.4byte	0xad
	.uleb128 0x5
	.byte	0x4
	.4byte	0xb3
	.uleb128 0x6
	.byte	0x1
	.4byte	0xbf
	.uleb128 0x7
	.4byte	0xbf
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF15
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x4
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x5
	.byte	0x57
	.4byte	0xbf
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x6
	.byte	0x4c
	.4byte	0xd3
	.uleb128 0x9
	.4byte	0x3e
	.4byte	0xf9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x14
	.byte	0x7
	.byte	0x18
	.4byte	0x148
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x7
	.byte	0x1a
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x7
	.byte	0x1c
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x7
	.byte	0x1e
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x7
	.byte	0x20
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x7
	.byte	0x23
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x7
	.byte	0x26
	.4byte	0xf9
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x163
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xb
	.byte	0x6
	.byte	0x8
	.byte	0x22
	.4byte	0x184
	.uleb128 0xd
	.ascii	"T\000"
	.byte	0x8
	.byte	0x24
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"D\000"
	.byte	0x8
	.byte	0x26
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x8
	.byte	0x28
	.4byte	0x163
	.uleb128 0xb
	.byte	0x14
	.byte	0x8
	.byte	0x31
	.4byte	0x216
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x8
	.byte	0x33
	.4byte	0x184
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x8
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x8
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x8
	.byte	0x35
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x8
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x8
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0xc
	.4byte	.LASF32
	.byte	0x8
	.byte	0x37
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x8
	.byte	0x39
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0xc
	.4byte	.LASF34
	.byte	0x8
	.byte	0x3b
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.4byte	.LASF35
	.byte	0x8
	.byte	0x3d
	.4byte	0x18f
	.uleb128 0xb
	.byte	0x20
	.byte	0x9
	.byte	0x1e
	.4byte	0x29a
	.uleb128 0xc
	.4byte	.LASF36
	.byte	0x9
	.byte	0x20
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF37
	.byte	0x9
	.byte	0x22
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF38
	.byte	0x9
	.byte	0x24
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF39
	.byte	0x9
	.byte	0x26
	.4byte	0xa2
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF40
	.byte	0x9
	.byte	0x28
	.4byte	0x29a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF41
	.byte	0x9
	.byte	0x2a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF42
	.byte	0x9
	.byte	0x2c
	.4byte	0xbf
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF43
	.byte	0x9
	.byte	0x2e
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0xe
	.4byte	0x29f
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2a5
	.uleb128 0xe
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF44
	.byte	0x9
	.byte	0x30
	.4byte	0x221
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x2c5
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0x3
	.4byte	.LASF45
	.byte	0xa
	.byte	0x69
	.4byte	0x2d0
	.uleb128 0xf
	.4byte	.LASF45
	.byte	0x1
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF46
	.uleb128 0x3
	.4byte	.LASF47
	.byte	0xb
	.byte	0x31
	.4byte	0x2e8
	.uleb128 0xf
	.4byte	.LASF47
	.byte	0x1
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x2fe
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x10
	.4byte	.LASF48
	.byte	0xc
	.2byte	0x1a2
	.4byte	0x30a
	.uleb128 0xf
	.4byte	.LASF48
	.byte	0x1
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x320
	.uleb128 0xa
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x330
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3f
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF49
	.uleb128 0x11
	.4byte	.LASF93
	.byte	0x1
	.byte	0x34
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x45d
	.uleb128 0x12
	.4byte	.LASF50
	.byte	0x1
	.byte	0x36
	.4byte	0x45d
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x12
	.4byte	.LASF51
	.byte	0x1
	.byte	0x38
	.4byte	0x463
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x12
	.4byte	.LASF52
	.byte	0x1
	.byte	0x3a
	.4byte	0x469
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF53
	.byte	0x1
	.byte	0x3c
	.4byte	0x216
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x1
	.byte	0x3e
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x12
	.4byte	.LASF55
	.byte	0x1
	.byte	0x40
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF56
	.byte	0x1
	.byte	0x42
	.4byte	0x65
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.byte	0x42
	.4byte	0x65
	.byte	0x3
	.byte	0x91
	.sleb128 -88
	.uleb128 0x12
	.4byte	.LASF58
	.byte	0x1
	.byte	0x42
	.4byte	0x65
	.byte	0x3
	.byte	0x91
	.sleb128 -92
	.uleb128 0x12
	.4byte	.LASF59
	.byte	0x1
	.byte	0x42
	.4byte	0x65
	.byte	0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x12
	.4byte	.LASF60
	.byte	0x1
	.byte	0x42
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x12
	.4byte	.LASF61
	.byte	0x1
	.byte	0x44
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x12
	.4byte	.LASF62
	.byte	0x1
	.byte	0x46
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x12
	.4byte	.LASF63
	.byte	0x1
	.byte	0x48
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.ascii	"d\000"
	.byte	0x1
	.byte	0x4a
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x13
	.ascii	"i\000"
	.byte	0x1
	.byte	0x4a
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.4byte	.LASF64
	.byte	0x1
	.byte	0x4c
	.4byte	0x2b5
	.byte	0x3
	.byte	0x91
	.sleb128 -144
	.uleb128 0x12
	.4byte	.LASF65
	.byte	0x1
	.byte	0x4e
	.4byte	0x320
	.byte	0x3
	.byte	0x91
	.sleb128 -208
	.uleb128 0x12
	.4byte	.LASF66
	.byte	0x1
	.byte	0x5a
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2dd
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2fe
	.uleb128 0x5
	.byte	0x4
	.4byte	0x2c5
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x193
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x4b7
	.uleb128 0x15
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x193
	.4byte	0xbf
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x195
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x19a
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x1d0
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x4e1
	.uleb128 0x15
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x1d0
	.4byte	0x4e1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x65
	.uleb128 0x17
	.4byte	.LASF73
	.byte	0xd
	.byte	0x7c
	.4byte	0x25
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x503
	.uleb128 0xa
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x18
	.4byte	.LASF74
	.byte	0xe
	.2byte	0x441
	.4byte	0x4f3
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF75
	.byte	0xe
	.2byte	0x442
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF76
	.byte	0xe
	.2byte	0x443
	.4byte	0x4f3
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF77
	.byte	0xe
	.2byte	0x455
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF78
	.byte	0xf
	.byte	0x30
	.4byte	0x54c
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xe9
	.uleb128 0x12
	.4byte	.LASF79
	.byte	0xf
	.byte	0x34
	.4byte	0x562
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xe9
	.uleb128 0x12
	.4byte	.LASF80
	.byte	0xf
	.byte	0x36
	.4byte	0x578
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xe9
	.uleb128 0x12
	.4byte	.LASF81
	.byte	0xf
	.byte	0x38
	.4byte	0x58e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xe9
	.uleb128 0x9
	.4byte	0x2aa
	.4byte	0x5a3
	.uleb128 0xa
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x17
	.4byte	.LASF82
	.byte	0x9
	.byte	0x38
	.4byte	0x5b0
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0x593
	.uleb128 0x17
	.4byte	.LASF83
	.byte	0x9
	.byte	0x5a
	.4byte	0x310
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF84
	.byte	0x9
	.byte	0x78
	.4byte	0xde
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF85
	.byte	0x9
	.2byte	0x159
	.4byte	0xd3
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF86
	.byte	0xa
	.byte	0x64
	.4byte	0x148
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF87
	.byte	0x10
	.byte	0x33
	.4byte	0x5fb
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xe
	.4byte	0x153
	.uleb128 0x12
	.4byte	.LASF88
	.byte	0x10
	.byte	0x3f
	.4byte	0x611
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xe
	.4byte	0x2ee
	.uleb128 0x12
	.4byte	.LASF89
	.byte	0x1
	.byte	0x2a
	.4byte	0x97
	.byte	0x5
	.byte	0x3
	.4byte	STATUS_next_scheduled_calc_queued
	.uleb128 0x12
	.4byte	.LASF90
	.byte	0x1
	.byte	0x2e
	.4byte	0x65
	.byte	0x5
	.byte	0x3
	.4byte	__largest_next_scheduled_delta
	.uleb128 0x17
	.4byte	.LASF73
	.byte	0xd
	.byte	0x7c
	.4byte	0x25
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF74
	.byte	0xe
	.2byte	0x441
	.4byte	0x4f3
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF75
	.byte	0xe
	.2byte	0x442
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF76
	.byte	0xe
	.2byte	0x443
	.4byte	0x4f3
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF77
	.byte	0xe
	.2byte	0x455
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF82
	.byte	0x9
	.byte	0x38
	.4byte	0x68a
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0x593
	.uleb128 0x17
	.4byte	.LASF83
	.byte	0x9
	.byte	0x5a
	.4byte	0x310
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF84
	.byte	0x9
	.byte	0x78
	.4byte	0xde
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF85
	.byte	0x9
	.2byte	0x159
	.4byte	0xd3
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF86
	.byte	0xa
	.byte	0x64
	.4byte	0x148
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF29:
	.ascii	"__year\000"
.LASF91:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF26:
	.ascii	"date_time\000"
.LASF58:
	.ascii	"lyear\000"
.LASF4:
	.ascii	"UNS_8\000"
.LASF28:
	.ascii	"__month\000"
.LASF51:
	.ascii	"lschedule\000"
.LASF43:
	.ascii	"priority\000"
.LASF3:
	.ascii	"signed char\000"
.LASF45:
	.ascii	"STATION_STRUCT\000"
.LASF7:
	.ascii	"short int\000"
.LASF16:
	.ascii	"portTickType\000"
.LASF75:
	.ascii	"GuiVar_StatusNextScheduledIrriGID\000"
.LASF54:
	.ascii	"residual\000"
.LASF74:
	.ascii	"GuiVar_StatusNextScheduledIrriDate\000"
.LASF90:
	.ascii	"__largest_next_scheduled_delta\000"
.LASF83:
	.ascii	"task_last_execution_stamp\000"
.LASF65:
	.ascii	"lirrigation_date_str_64\000"
.LASF59:
	.ascii	"ldow\000"
.LASF89:
	.ascii	"STATUS_next_scheduled_calc_queued\000"
.LASF23:
	.ascii	"InUse\000"
.LASF33:
	.ascii	"__dayofweek\000"
.LASF63:
	.ascii	"lthere_is_at_least_one_start_time_and_water_day\000"
.LASF44:
	.ascii	"TASK_ENTRY_STRUCT\000"
.LASF10:
	.ascii	"INT_32\000"
.LASF47:
	.ascii	"MANUAL_PROGRAMS_GROUP_STRUCT\000"
.LASF53:
	.ascii	"ldtcs\000"
.LASF46:
	.ascii	"float\000"
.LASF57:
	.ascii	"lmonth_1_12\000"
.LASF12:
	.ascii	"long long int\000"
.LASF14:
	.ascii	"pdTASK_CODE\000"
.LASF80:
	.ascii	"GuiFont_DecimalChar\000"
.LASF24:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF35:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF15:
	.ascii	"long int\000"
.LASF82:
	.ascii	"Task_Table\000"
.LASF17:
	.ascii	"xQueueHandle\000"
.LASF70:
	.ascii	"postBackground_Calculation_Event\000"
.LASF52:
	.ascii	"lstation\000"
.LASF85:
	.ascii	"Background_Calculation_Queue\000"
.LASF61:
	.ascii	"lseconds\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF77:
	.ascii	"GuiVar_StatusTypeOfSchedule\000"
.LASF68:
	.ascii	"task_index\000"
.LASF76:
	.ascii	"GuiVar_StatusNextScheduledIrriType\000"
.LASF25:
	.ascii	"DATE_TIME\000"
.LASF55:
	.ascii	"lstart_time_idx\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF84:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF60:
	.ascii	"ltodays_date\000"
.LASF50:
	.ascii	"lman_prog\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF79:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF38:
	.ascii	"execution_limit_ms\000"
.LASF42:
	.ascii	"parameter\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF71:
	.ascii	"pvParameters\000"
.LASF20:
	.ascii	"ptail\000"
.LASF22:
	.ascii	"offset\000"
.LASF64:
	.ascii	"lgroup_name_str_48\000"
.LASF66:
	.ascii	"__this_time_delta\000"
.LASF1:
	.ascii	"char\000"
.LASF81:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF18:
	.ascii	"xSemaphoreHandle\000"
.LASF31:
	.ascii	"__minutes\000"
.LASF67:
	.ascii	"lcmd\000"
.LASF41:
	.ascii	"stack_depth\000"
.LASF34:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF72:
	.ascii	"pcmd\000"
.LASF73:
	.ascii	"my_tick_count\000"
.LASF39:
	.ascii	"pTaskFunc\000"
.LASF92:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/b"
	.ascii	"ackground_calculations.c\000"
.LASF62:
	.ascii	"lno_water_days\000"
.LASF88:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF27:
	.ascii	"__day\000"
.LASF32:
	.ascii	"__seconds\000"
.LASF37:
	.ascii	"include_in_wdt\000"
.LASF21:
	.ascii	"count\000"
.LASF36:
	.ascii	"bCreateTask\000"
.LASF78:
	.ascii	"GuiFont_LanguageActive\000"
.LASF87:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF93:
	.ascii	"STATUS_load_next_scheduled_into_guivars\000"
.LASF86:
	.ascii	"station_info_list_hdr\000"
.LASF48:
	.ascii	"STATION_GROUP_STRUCT\000"
.LASF19:
	.ascii	"phead\000"
.LASF40:
	.ascii	"TaskName\000"
.LASF49:
	.ascii	"double\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF69:
	.ascii	"background_calculation_task\000"
.LASF56:
	.ascii	"lday_of_month\000"
.LASF30:
	.ascii	"__hours\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
