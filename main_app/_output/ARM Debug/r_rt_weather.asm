	.file	"r_rt_weather.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_weather.c\000"
	.section	.text.FDTO_REAL_TIME_WEATHER_update_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_WEATHER_update_report
	.type	FDTO_REAL_TIME_WEATHER_update_report, %function
FDTO_REAL_TIME_WEATHER_update_report:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_rt_weather.c"
	.loc 1 40 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	.loc 1 43 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 47 0
	bl	WEATHER_get_et_gage_box_index
	mov	r3, r0
	mov	r0, r3
	ldr	r1, .L5+16
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	.loc 1 49 0
	bl	WEATHER_get_rain_bucket_box_index
	mov	r3, r0
	mov	r0, r3
	ldr	r1, .L5+20
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	.loc 1 51 0
	bl	WEATHER_get_wind_gage_box_index
	mov	r3, r0
	mov	r0, r3
	ldr	r1, .L5+24
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	.loc 1 55 0
	ldr	r3, .L5+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L5+32
	mov	r3, #55
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 57 0
	ldr	r3, .L5+36
	ldrh	r3, [r3, #36]
	fmsr	s15, r3	@ int
	fsitod	d6, s15
	fldd	d7, .L5
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L5+40
	fsts	s15, [r3, #0]
	.loc 1 59 0
	ldr	r3, .L5+36
	ldr	r2, [r3, #84]
	ldr	r3, .L5+44
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L2
	.loc 1 61 0
	ldr	r3, .L5+36
	ldr	r2, [r3, #84]
	ldr	r3, .L5+44
	str	r2, [r3, #0]
	.loc 1 65 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L2:
	.loc 1 70 0
	ldr	r3, .L5+36
	ldr	r3, [r3, #52]
	fmsr	s15, r3	@ int
	fuitod	d6, s15
	fldd	d7, .L5+8
	fdivd	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r3, .L5+48
	fsts	s15, [r3, #0]
	.loc 1 75 0
	ldr	r3, .L5+36
	ldr	r2, [r3, #100]
	ldr	r3, .L5+52
	str	r2, [r3, #0]
	.loc 1 77 0
	ldr	r3, .L5+36
	ldr	r2, [r3, #104]
	ldr	r3, .L5+56
	str	r2, [r3, #0]
	.loc 1 79 0
	ldr	r3, .L5+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 83 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L3
	.loc 1 85 0
	mov	r0, #1
	bl	FDTO_Redraw_Screen
	b	.L1
.L3:
	.loc 1 89 0
	bl	GuiLib_Refresh
.L1:
	.loc 1 91 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	0
	.word	1086556160
	.word	0
	.word	1079574528
	.word	GuiVar_StatusETGageControllerName
	.word	GuiVar_StatusRainBucketControllerName
	.word	GuiVar_StatusWindGageControllerName
	.word	weather_preserves_recursive_MUTEX
	.word	.LC0
	.word	weather_preserves
	.word	GuiVar_StatusETGageReading
	.word	GuiVar_ETGageRunawayGage
	.word	GuiVar_StatusRainBucketReading
	.word	GuiVar_StatusRainSwitchState
	.word	GuiVar_StatusFreezeSwitchState
.LFE0:
	.size	FDTO_REAL_TIME_WEATHER_update_report, .-FDTO_REAL_TIME_WEATHER_update_report
	.section	.text.FDTO_REAL_TIME_WEATHER_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_WEATHER_draw_report
	.type	FDTO_REAL_TIME_WEATHER_draw_report, %function
FDTO_REAL_TIME_WEATHER_draw_report:
.LFB1:
	.loc 1 95 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-12]
	.loc 1 101 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	beq	.L8
	.loc 1 101 0 is_stmt 0 discriminator 1
	ldr	r3, .L12
	ldr	r2, [r3, #84]
	ldr	r3, .L12+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L9
.L8:
	.loc 1 103 0 is_stmt 1
	ldr	r3, .L12
	ldr	r3, [r3, #84]
	cmp	r3, #1
	bne	.L10
	.loc 1 105 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L11
.L10:
	.loc 1 109 0
	mvn	r3, #0
	str	r3, [fp, #-8]
.L11:
	.loc 1 112 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #98
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
.L9:
	.loc 1 115 0
	bl	FDTO_REAL_TIME_WEATHER_update_report
	.loc 1 116 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	weather_preserves
	.word	GuiVar_ETGageRunawayGage
.LFE1:
	.size	FDTO_REAL_TIME_WEATHER_draw_report, .-FDTO_REAL_TIME_WEATHER_draw_report
	.section	.text.REAL_TIME_WEATHER_process_report,"ax",%progbits
	.align	2
	.global	REAL_TIME_WEATHER_process_report
	.type	REAL_TIME_WEATHER_process_report, %function
REAL_TIME_WEATHER_process_report:
.LFB2:
	.loc 1 120 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 121 0
	ldr	r3, [fp, #-12]
	cmp	r3, #2
	beq	.L16
	cmp	r3, #67
	beq	.L17
	b	.L25
.L16:
	.loc 1 124 0
	ldr	r3, .L28
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L26
.L19:
	.loc 1 127 0
	ldr	r3, .L28+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L20
	.loc 1 129 0
	mov	r0, #0
	mov	r1, #0
	bl	CURSOR_Select
	.loc 1 131 0
	bl	ET_GAGE_clear_runaway_gage
	.loc 1 137 0
	b	.L22
.L20:
	.loc 1 135 0
	bl	bad_key_beep
	.loc 1 137 0
	b	.L22
.L26:
	.loc 1 140 0
	bl	bad_key_beep
	.loc 1 142 0
	b	.L14
.L22:
	b	.L14
.L17:
	.loc 1 145 0
	ldr	r3, .L28+8
	ldr	r2, [r3, #0]
	ldr	r0, .L28+12
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L28+16
	str	r2, [r3, #0]
	.loc 1 147 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 149 0
	ldr	r3, .L28+16
	ldr	r3, [r3, #0]
	cmp	r3, #10
	bne	.L27
	.loc 1 153 0
	mov	r0, #1
	bl	LIVE_SCREENS_draw_dialog
	.loc 1 155 0
	b	.L27
.L25:
	.loc 1 158 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	b	.L14
.L27:
	.loc 1 155 0
	mov	r0, r0	@ nop
.L14:
	.loc 1 160 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L29:
	.align	2
.L28:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ETGageRunawayGage
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	REAL_TIME_WEATHER_process_report, .-REAL_TIME_WEATHER_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_comm_server_common.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7c1
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF105
	.byte	0x1
	.4byte	.LASF106
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x62
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x74
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x67
	.4byte	0x86
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x74
	.uleb128 0x5
	.byte	0x4
	.4byte	0xac
	.uleb128 0x6
	.4byte	0xb3
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF14
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF15
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x35
	.4byte	0xb3
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x4
	.byte	0x57
	.4byte	0xba
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x5
	.byte	0x4c
	.4byte	0xce
	.uleb128 0x9
	.4byte	0x37
	.4byte	0xf4
	.uleb128 0xa
	.4byte	0xb3
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0x119
	.uleb128 0xc
	.4byte	.LASF19
	.byte	0x6
	.byte	0x7e
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x6
	.byte	0x80
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x6
	.byte	0x82
	.4byte	0xf4
	.uleb128 0xb
	.byte	0x4
	.byte	0x7
	.byte	0x4c
	.4byte	0x149
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x7
	.byte	0x55
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x7
	.byte	0x57
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x7
	.byte	0x59
	.4byte	0x124
	.uleb128 0xb
	.byte	0x4
	.byte	0x7
	.byte	0x65
	.4byte	0x179
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x7
	.byte	0x67
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x7
	.byte	0x69
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF26
	.byte	0x7
	.byte	0x6b
	.4byte	0x154
	.uleb128 0x9
	.4byte	0x69
	.4byte	0x194
	.uleb128 0xa
	.4byte	0xb3
	.byte	0x2
	.byte	0
	.uleb128 0xb
	.byte	0x6
	.byte	0x8
	.byte	0x22
	.4byte	0x1b5
	.uleb128 0xd
	.ascii	"T\000"
	.byte	0x8
	.byte	0x24
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"D\000"
	.byte	0x8
	.byte	0x26
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF27
	.byte	0x8
	.byte	0x28
	.4byte	0x194
	.uleb128 0x9
	.4byte	0x25
	.4byte	0x1d0
	.uleb128 0xa
	.4byte	0xb3
	.byte	0xf
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF28
	.uleb128 0x9
	.4byte	0x69
	.4byte	0x1e7
	.uleb128 0xa
	.4byte	0xb3
	.byte	0x3
	.byte	0
	.uleb128 0xe
	.byte	0x1c
	.byte	0x9
	.2byte	0x10c
	.4byte	0x25a
	.uleb128 0xf
	.ascii	"rip\000"
	.byte	0x9
	.2byte	0x112
	.4byte	0x179
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF29
	.byte	0x9
	.2byte	0x11b
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x9
	.2byte	0x122
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x9
	.2byte	0x127
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x10
	.4byte	.LASF32
	.byte	0x9
	.2byte	0x138
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x10
	.4byte	.LASF33
	.byte	0x9
	.2byte	0x144
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x10
	.4byte	.LASF34
	.byte	0x9
	.2byte	0x14b
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x11
	.4byte	.LASF35
	.byte	0x9
	.2byte	0x14d
	.4byte	0x1e7
	.uleb128 0xe
	.byte	0xec
	.byte	0x9
	.2byte	0x150
	.4byte	0x43a
	.uleb128 0x10
	.4byte	.LASF36
	.byte	0x9
	.2byte	0x157
	.4byte	0x1c0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF37
	.byte	0x9
	.2byte	0x162
	.4byte	0x45
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x10
	.4byte	.LASF38
	.byte	0x9
	.2byte	0x164
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x10
	.4byte	.LASF39
	.byte	0x9
	.2byte	0x166
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x10
	.4byte	.LASF40
	.byte	0x9
	.2byte	0x168
	.4byte	0x1b5
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x10
	.4byte	.LASF41
	.byte	0x9
	.2byte	0x16e
	.4byte	0x149
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x10
	.4byte	.LASF42
	.byte	0x9
	.2byte	0x174
	.4byte	0x25a
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x10
	.4byte	.LASF43
	.byte	0x9
	.2byte	0x17b
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x10
	.4byte	.LASF44
	.byte	0x9
	.2byte	0x17d
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x10
	.4byte	.LASF45
	.byte	0x9
	.2byte	0x185
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x10
	.4byte	.LASF46
	.byte	0x9
	.2byte	0x18d
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x10
	.4byte	.LASF47
	.byte	0x9
	.2byte	0x191
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x10
	.4byte	.LASF48
	.byte	0x9
	.2byte	0x195
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x10
	.4byte	.LASF49
	.byte	0x9
	.2byte	0x199
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x10
	.4byte	.LASF50
	.byte	0x9
	.2byte	0x19e
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x10
	.4byte	.LASF51
	.byte	0x9
	.2byte	0x1a2
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x10
	.4byte	.LASF52
	.byte	0x9
	.2byte	0x1a6
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x10
	.4byte	.LASF53
	.byte	0x9
	.2byte	0x1b4
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x10
	.4byte	.LASF54
	.byte	0x9
	.2byte	0x1ba
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x10
	.4byte	.LASF55
	.byte	0x9
	.2byte	0x1c2
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x10
	.4byte	.LASF56
	.byte	0x9
	.2byte	0x1c4
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x10
	.4byte	.LASF57
	.byte	0x9
	.2byte	0x1c6
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x10
	.4byte	.LASF58
	.byte	0x9
	.2byte	0x1d5
	.4byte	0x2c
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x10
	.4byte	.LASF59
	.byte	0x9
	.2byte	0x1d7
	.4byte	0x2c
	.byte	0x3
	.byte	0x23
	.uleb128 0x81
	.uleb128 0x10
	.4byte	.LASF60
	.byte	0x9
	.2byte	0x1dd
	.4byte	0x2c
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0x10
	.4byte	.LASF61
	.byte	0x9
	.2byte	0x1e7
	.4byte	0x2c
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0x10
	.4byte	.LASF62
	.byte	0x9
	.2byte	0x1f0
	.4byte	0x69
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x10
	.4byte	.LASF63
	.byte	0x9
	.2byte	0x1f7
	.4byte	0x9b
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x10
	.4byte	.LASF64
	.byte	0x9
	.2byte	0x1f9
	.4byte	0x9b
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x10
	.4byte	.LASF65
	.byte	0x9
	.2byte	0x1fd
	.4byte	0x43a
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0x9
	.4byte	0x69
	.4byte	0x44a
	.uleb128 0xa
	.4byte	0xb3
	.byte	0x16
	.byte	0
	.uleb128 0x11
	.4byte	.LASF66
	.byte	0x9
	.2byte	0x204
	.4byte	0x266
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF67
	.uleb128 0xb
	.byte	0x24
	.byte	0xa
	.byte	0x78
	.4byte	0x4e4
	.uleb128 0xc
	.4byte	.LASF68
	.byte	0xa
	.byte	0x7b
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF69
	.byte	0xa
	.byte	0x83
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF70
	.byte	0xa
	.byte	0x86
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF71
	.byte	0xa
	.byte	0x88
	.4byte	0x4f5
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF72
	.byte	0xa
	.byte	0x8d
	.4byte	0x507
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF73
	.byte	0xa
	.byte	0x92
	.4byte	0xa6
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF74
	.byte	0xa
	.byte	0x96
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF75
	.byte	0xa
	.byte	0x9a
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF76
	.byte	0xa
	.byte	0x9c
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0x12
	.byte	0x1
	.4byte	0x4f0
	.uleb128 0x13
	.4byte	0x4f0
	.byte	0
	.uleb128 0x14
	.4byte	0x57
	.uleb128 0x5
	.byte	0x4
	.4byte	0x4e4
	.uleb128 0x12
	.byte	0x1
	.4byte	0x507
	.uleb128 0x13
	.4byte	0x119
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x4fb
	.uleb128 0x3
	.4byte	.LASF77
	.byte	0xa
	.byte	0x9e
	.4byte	0x45d
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF78
	.byte	0x1
	.byte	0x27
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x540
	.uleb128 0x16
	.4byte	.LASF80
	.byte	0x1
	.byte	0x29
	.4byte	0x9b
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF79
	.byte	0x1
	.byte	0x5e
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x576
	.uleb128 0x17
	.4byte	.LASF83
	.byte	0x1
	.byte	0x5e
	.4byte	0x9b
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF81
	.byte	0x1
	.byte	0x60
	.4byte	0x7b
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF82
	.byte	0x1
	.byte	0x77
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x59e
	.uleb128 0x17
	.4byte	.LASF84
	.byte	0x1
	.byte	0x77
	.4byte	0x119
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x18
	.4byte	.LASF85
	.byte	0xb
	.2byte	0x1a7
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF86
	.byte	0xb
	.2byte	0x2ec
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x25
	.4byte	0x5ca
	.uleb128 0xa
	.4byte	0xb3
	.byte	0x30
	.byte	0
	.uleb128 0x18
	.4byte	.LASF87
	.byte	0xb
	.2byte	0x438
	.4byte	0x5ba
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF88
	.byte	0xb
	.2byte	0x439
	.4byte	0x1d0
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF89
	.byte	0xb
	.2byte	0x43c
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF90
	.byte	0xb
	.2byte	0x447
	.4byte	0x5ba
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF91
	.byte	0xb
	.2byte	0x448
	.4byte	0x1d0
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF92
	.byte	0xb
	.2byte	0x44a
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF93
	.byte	0xb
	.2byte	0x457
	.4byte	0x5ba
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF94
	.byte	0xc
	.2byte	0x127
	.4byte	0x62
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF95
	.byte	0xd
	.byte	0x30
	.4byte	0x64b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x14
	.4byte	0xe4
	.uleb128 0x16
	.4byte	.LASF96
	.byte	0xd
	.byte	0x34
	.4byte	0x661
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x14
	.4byte	0xe4
	.uleb128 0x16
	.4byte	.LASF97
	.byte	0xd
	.byte	0x36
	.4byte	0x677
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x14
	.4byte	0xe4
	.uleb128 0x16
	.4byte	.LASF98
	.byte	0xd
	.byte	0x38
	.4byte	0x68d
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x14
	.4byte	0xe4
	.uleb128 0x19
	.4byte	.LASF99
	.byte	0xe
	.byte	0xba
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF100
	.byte	0xf
	.byte	0x33
	.4byte	0x6b0
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x14
	.4byte	0x184
	.uleb128 0x16
	.4byte	.LASF101
	.byte	0xf
	.byte	0x3f
	.4byte	0x6c6
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x14
	.4byte	0x1d7
	.uleb128 0x18
	.4byte	.LASF102
	.byte	0x9
	.2byte	0x206
	.4byte	0x44a
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x50d
	.4byte	0x6e9
	.uleb128 0xa
	.4byte	0xb3
	.byte	0x31
	.byte	0
	.uleb128 0x19
	.4byte	.LASF103
	.byte	0xa
	.byte	0xac
	.4byte	0x6d9
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF104
	.byte	0xa
	.byte	0xae
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF85
	.byte	0xb
	.2byte	0x1a7
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF86
	.byte	0xb
	.2byte	0x2ec
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF87
	.byte	0xb
	.2byte	0x438
	.4byte	0x5ba
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF88
	.byte	0xb
	.2byte	0x439
	.4byte	0x1d0
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF89
	.byte	0xb
	.2byte	0x43c
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF90
	.byte	0xb
	.2byte	0x447
	.4byte	0x5ba
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF91
	.byte	0xb
	.2byte	0x448
	.4byte	0x1d0
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF92
	.byte	0xb
	.2byte	0x44a
	.4byte	0x74
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF93
	.byte	0xb
	.2byte	0x457
	.4byte	0x5ba
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF94
	.byte	0xc
	.2byte	0x127
	.4byte	0x62
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF99
	.byte	0xe
	.byte	0xba
	.4byte	0xd9
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF102
	.byte	0x9
	.2byte	0x206
	.4byte	0x44a
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF103
	.byte	0xa
	.byte	0xac
	.4byte	0x6d9
	.byte	0x1
	.byte	0x1
	.uleb128 0x19
	.4byte	.LASF104
	.byte	0xa
	.byte	0xae
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF105:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF28:
	.ascii	"float\000"
.LASF69:
	.ascii	"_02_menu\000"
.LASF27:
	.ascii	"DATE_TIME\000"
.LASF93:
	.ascii	"GuiVar_StatusWindGageControllerName\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF34:
	.ascii	"needs_to_be_broadcast\000"
.LASF54:
	.ascii	"pending_changes_to_send_to_comm_server\000"
.LASF81:
	.ascii	"lcursor_to_show\000"
.LASF85:
	.ascii	"GuiVar_ETGageRunawayGage\000"
.LASF7:
	.ascii	"short int\000"
.LASF16:
	.ascii	"portTickType\000"
.LASF33:
	.ascii	"inhibit_irrigation_have_crossed_the_minimum\000"
.LASF30:
	.ascii	"roll_to_roll_raw_pulse_count\000"
.LASF41:
	.ascii	"et_rip\000"
.LASF68:
	.ascii	"_01_command\000"
.LASF61:
	.ascii	"ununsed_uns8_1\000"
.LASF82:
	.ascii	"REAL_TIME_WEATHER_process_report\000"
.LASF43:
	.ascii	"sync_the_et_rain_tables\000"
.LASF32:
	.ascii	"rain_shutdown_rcvd_from_commserver_uns32\000"
.LASF53:
	.ascii	"write_tpmicro_file_in_new_file_format\000"
.LASF86:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF87:
	.ascii	"GuiVar_StatusETGageControllerName\000"
.LASF19:
	.ascii	"keycode\000"
.LASF64:
	.ascii	"hub_needs_to_distribute_main_binary\000"
.LASF88:
	.ascii	"GuiVar_StatusETGageReading\000"
.LASF21:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF44:
	.ascii	"nlu_old_rain_table_sync_variable\000"
.LASF48:
	.ascii	"yesterdays_ET_was_zero\000"
.LASF78:
	.ascii	"FDTO_REAL_TIME_WEATHER_update_report\000"
.LASF10:
	.ascii	"INT_32\000"
.LASF106:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_weather.c\000"
.LASF50:
	.ascii	"clear_runaway_gage\000"
.LASF37:
	.ascii	"dls_saved_date\000"
.LASF25:
	.ascii	"rain_inches_u16_100u\000"
.LASF12:
	.ascii	"long long int\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF97:
	.ascii	"GuiFont_DecimalChar\000"
.LASF15:
	.ascii	"long int\000"
.LASF38:
	.ascii	"dls_eligible_to_fall_back\000"
.LASF17:
	.ascii	"xQueueHandle\000"
.LASF90:
	.ascii	"GuiVar_StatusRainBucketControllerName\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF57:
	.ascii	"commserver_monthly_code_receipt_bytes\000"
.LASF52:
	.ascii	"freeze_switch_active\000"
.LASF74:
	.ascii	"_06_u32_argument1\000"
.LASF55:
	.ascii	"commserver_monthly_rcvd_bytes\000"
.LASF58:
	.ascii	"factory_reset_do_not_allow_pdata_to_be_sent\000"
.LASF47:
	.ascii	"run_away_gage\000"
.LASF72:
	.ascii	"key_process_func_ptr\000"
.LASF76:
	.ascii	"_08_screen_to_draw\000"
.LASF79:
	.ascii	"FDTO_REAL_TIME_WEATHER_draw_report\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF89:
	.ascii	"GuiVar_StatusFreezeSwitchState\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF2:
	.ascii	"signed char\000"
.LASF95:
	.ascii	"GuiFont_LanguageActive\000"
.LASF26:
	.ascii	"RAIN_TABLE_ENTRY\000"
.LASF96:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF63:
	.ascii	"hub_needs_to_distribute_tpmicro_binary\000"
.LASF84:
	.ascii	"pkey_event\000"
.LASF39:
	.ascii	"dls_after_fall_back_ignore_sxs\000"
.LASF102:
	.ascii	"weather_preserves\000"
.LASF23:
	.ascii	"status\000"
.LASF46:
	.ascii	"dont_use_et_gage_today\000"
.LASF22:
	.ascii	"et_inches_u16_10000u\000"
.LASF94:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF40:
	.ascii	"dls_after_fall_when_to_clear_ignore_sxs\000"
.LASF62:
	.ascii	"commserver_monthly_mobile_status_updates_bytes\000"
.LASF104:
	.ascii	"screen_history_index\000"
.LASF0:
	.ascii	"char\000"
.LASF31:
	.ascii	"midnight_to_midnight_raw_pulse_count\000"
.LASF92:
	.ascii	"GuiVar_StatusRainSwitchState\000"
.LASF98:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF71:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF18:
	.ascii	"xSemaphoreHandle\000"
.LASF75:
	.ascii	"_07_u32_argument2\000"
.LASF49:
	.ascii	"remaining_gage_pulses\000"
.LASF35:
	.ascii	"RAIN_STATE\000"
.LASF45:
	.ascii	"et_table_update_all_historical_values\000"
.LASF103:
	.ascii	"ScreenHistory\000"
.LASF36:
	.ascii	"verify_string_pre\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF14:
	.ascii	"long unsigned int\000"
.LASF83:
	.ascii	"pcomplete_redraw\000"
.LASF101:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF73:
	.ascii	"_04_func_ptr\000"
.LASF70:
	.ascii	"_03_structure_to_draw\000"
.LASF80:
	.ascii	"lforce_complete_redraw\000"
.LASF20:
	.ascii	"repeats\000"
.LASF60:
	.ascii	"perform_a_discovery_following_a_FACTORY_reset\000"
.LASF100:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF99:
	.ascii	"weather_preserves_recursive_MUTEX\000"
.LASF91:
	.ascii	"GuiVar_StatusRainBucketReading\000"
.LASF65:
	.ascii	"expansion\000"
.LASF66:
	.ascii	"WEATHER_PRESERVES_STRUCT\000"
.LASF67:
	.ascii	"double\000"
.LASF51:
	.ascii	"rain_switch_active\000"
.LASF56:
	.ascii	"commserver_monthly_xmit_bytes\000"
.LASF24:
	.ascii	"ET_TABLE_ENTRY\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF77:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF59:
	.ascii	"factory_reset_registration_to_tell_commserver_to_cl"
	.ascii	"ear_pdata\000"
.LASF42:
	.ascii	"rain\000"
.LASF29:
	.ascii	"hourly_total_inches_100u\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
