	.file	"lpc3xxx_spwm_driver.c"
	.text
.Ltext0:
	.section	.bss.spwmdat,"aw",%nobits
	.align	2
	.type	spwmdat, %object
	.size	spwmdat, 24
spwmdat:
	.space	24
	.section	.text.spwm_percent_to_ctrl,"ax",%progbits
	.align	2
	.global	spwm_percent_to_ctrl
	.type	spwm_percent_to_ctrl, %function
spwm_percent_to_ctrl:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc3xxx_spwm_driver.c"
	.loc 1 67 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 68 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L2
	.loc 1 70 0
	ldr	r3, [fp, #-4]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	b	.L3
.L2:
	.loc 1 74 0
	ldr	r3, [fp, #-8]
	cmp	r3, #99
	bls	.L4
	.loc 1 77 0
	ldr	r3, [fp, #-4]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	.loc 1 76 0
	orr	r3, r3, #1073741824
	b	.L3
.L4:
	.loc 1 82 0
	ldr	r2, [fp, #-4]
	ldr	r3, .L5
	and	r3, r2, r3
	.loc 1 84 0
	ldr	r2, [fp, #-8]
	rsb	r2, r2, #100
	mov	r1, r2, asl #8
	ldr	r2, [fp, #-8]
	rsb	r1, r2, r1
	ldr	r2, .L5+4
	umull	r0, r2, r1, r2
	mov	r2, r2, lsr #5
	and	r2, r2, #255
	.loc 1 82 0
	orr	r3, r3, r2
.L3:
	.loc 1 87 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L6:
	.align	2
.L5:
	.word	-1073676544
	.word	1374389535
.LFE0:
	.size	spwm_percent_to_ctrl, .-spwm_percent_to_ctrl
	.section	.text.spwm_open,"ax",%progbits
	.align	2
	.global	spwm_open
	.type	spwm_open, %function
spwm_open:
.LFB1:
	.loc 1 118 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #16
.LCFI5:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 119 0
	ldr	r3, .L15
	str	r3, [fp, #-4]
	.loc 1 120 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 122 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L15+4
	cmp	r2, r3
	bne	.L8
	.loc 1 122 0 is_stmt 0 discriminator 1
	mov	r3, #0
	str	r3, [fp, #-4]
.L8:
	.loc 1 123 0 is_stmt 1
	ldr	r2, [fp, #-12]
	ldr	r3, .L15+8
	cmp	r2, r3
	bne	.L9
	.loc 1 123 0 is_stmt 0 discriminator 1
	mov	r3, #1
	str	r3, [fp, #-4]
.L9:
	.loc 1 124 0 is_stmt 1
	ldr	r2, [fp, #-4]
	ldr	r3, .L15
	cmp	r2, r3
	beq	.L10
	.loc 1 127 0
	ldr	r1, .L15+12
	ldr	r2, [fp, #-4]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L10
	.loc 1 131 0
	ldr	r1, .L15+12
	ldr	r2, [fp, #-4]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 132 0
	ldr	r0, .L15+12
	ldr	r2, [fp, #-4]
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 135 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	bne	.L11
	.loc 1 135 0 is_stmt 0 discriminator 1
	ldr	r0, .L15+12
	ldr	r2, [fp, #-4]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, .L15+4
	str	r2, [r3, #0]
.L11:
	.loc 1 136 0 is_stmt 1
	ldr	r3, [fp, #-4]
	cmp	r3, #1
	bne	.L12
	.loc 1 136 0 is_stmt 0 discriminator 1
	ldr	r0, .L15+12
	ldr	r2, [fp, #-4]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, .L15+8
	str	r2, [r3, #0]
.L12:
	.loc 1 139 0 is_stmt 1
	ldr	r0, .L15+12
	ldr	r2, [fp, #-4]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L15+12
	ldr	r2, [fp, #-4]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	bic	r3, r3, #-2147483648
	str	r3, [r1, #0]
	.loc 1 142 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L13
	.loc 1 143 0
	ldr	r0, .L15+12
	ldr	r2, [fp, #-4]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L15+12
	ldr	r2, [fp, #-4]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	bic	r3, r3, #1073741824
	str	r3, [r1, #0]
	b	.L14
.L13:
	.loc 1 145 0
	ldr	r0, .L15+12
	ldr	r2, [fp, #-4]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L15+12
	ldr	r2, [fp, #-4]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	orr	r3, r3, #1073741824
	str	r3, [r1, #0]
.L14:
	.loc 1 147 0
	ldr	r2, [fp, #-4]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L15+12
	add	r3, r2, r3
	str	r3, [fp, #-8]
.L10:
	.loc 1 151 0
	ldr	r3, [fp, #-8]
	.loc 1 152 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L16:
	.align	2
.L15:
	.word	9999
	.word	1074118656
	.word	1074118660
	.word	spwmdat
.LFE1:
	.size	spwm_open, .-spwm_open
	.section	.text.spwm_close,"ax",%progbits
	.align	2
	.global	spwm_close
	.type	spwm_close, %function
spwm_close:
.LFB2:
	.loc 1 176 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #12
.LCFI8:
	str	r0, [fp, #-12]
	.loc 1 177 0
	ldr	r3, .L21
	str	r3, [fp, #-4]
	.loc 1 178 0
	mvn	r3, #0
	str	r3, [fp, #-8]
	.loc 1 180 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L21+4
	cmp	r2, r3
	bne	.L18
	.loc 1 180 0 is_stmt 0 discriminator 1
	mov	r3, #0
	str	r3, [fp, #-4]
.L18:
	.loc 1 181 0 is_stmt 1
	ldr	r2, [fp, #-12]
	ldr	r3, .L21+8
	cmp	r2, r3
	bne	.L19
	.loc 1 181 0 is_stmt 0 discriminator 1
	mov	r3, #1
	str	r3, [fp, #-4]
.L19:
	.loc 1 183 0 is_stmt 1
	ldr	r1, .L21+4
	ldr	r2, [fp, #-4]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L20
	.loc 1 186 0
	ldr	r0, .L21+4
	ldr	r2, [fp, #-4]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L21+4
	ldr	r2, [fp, #-4]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	bic	r3, r3, #-2147483648
	str	r3, [r1, #0]
	.loc 1 189 0
	ldr	r1, .L21+4
	ldr	r2, [fp, #-4]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 190 0
	ldr	r0, .L21+4
	ldr	r2, [fp, #-4]
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 193 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L20:
	.loc 1 197 0
	ldr	r3, [fp, #-8]
	.loc 1 198 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L22:
	.align	2
.L21:
	.word	9999
	.word	spwmdat
	.word	spwmdat+12
.LFE2:
	.size	spwm_close, .-spwm_close
	.section	.text.spwm_ioctl,"ax",%progbits
	.align	2
	.global	spwm_ioctl
	.type	spwm_ioctl, %function
spwm_ioctl:
.LFB3:
	.loc 1 225 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI9:
	add	fp, sp, #8
.LCFI10:
	sub	sp, sp, #28
.LCFI11:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	str	r2, [fp, #-36]
	.loc 1 227 0
	ldr	r3, .L41
	str	r3, [fp, #-12]
	.loc 1 228 0
	ldr	r3, [fp, #-36]
	str	r3, [fp, #-20]
	.loc 1 229 0
	mvn	r3, #0
	str	r3, [fp, #-16]
	.loc 1 231 0
	ldr	r2, [fp, #-28]
	ldr	r3, .L41+4
	cmp	r2, r3
	bne	.L24
	.loc 1 231 0 is_stmt 0 discriminator 1
	mov	r3, #0
	str	r3, [fp, #-12]
.L24:
	.loc 1 232 0 is_stmt 1
	ldr	r2, [fp, #-28]
	ldr	r3, .L41+8
	cmp	r2, r3
	bne	.L25
	.loc 1 232 0 is_stmt 0 discriminator 1
	mov	r3, #1
	str	r3, [fp, #-12]
.L25:
	.loc 1 234 0 is_stmt 1
	ldr	r2, [fp, #-12]
	ldr	r3, .L41
	cmp	r2, r3
	beq	.L26
	.loc 1 236 0
	ldr	r1, .L41+4
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r1, r3
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L26
	.loc 1 238 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 240 0
	ldr	r3, [fp, #-32]
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L27
.L34:
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
.L28:
	.loc 1 243 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L41+4
	ldr	r2, [fp, #-12]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	orr	r3, r3, #-2147483648
	str	r3, [r1, #0]
	.loc 1 244 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 245 0
	b	.L26
.L29:
	.loc 1 248 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 249 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	bne	.L35
	.loc 1 250 0
	mov	r3, #0
	str	r3, [fp, #-24]
	b	.L36
.L35:
	.loc 1 252 0
	mov	r3, #100
	str	r3, [fp, #-24]
.L36:
	.loc 1 253 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r4, [r3, #0]
	.loc 1 255 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	.loc 1 254 0
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	mov	r0, r2
	mov	r1, r3
	bl	spwm_percent_to_ctrl
	mov	r3, r0
	.loc 1 253 0
	str	r3, [r4, #0]
	.loc 1 256 0
	b	.L26
.L30:
	.loc 1 259 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	.loc 1 260 0
	ldr	ip, .L41+4
	ldr	r2, [fp, #-12]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	bic	r2, r3, #65280
	.loc 1 261 0
	ldr	r3, [fp, #-36]
	mov	r3, r3, asl #8
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	orr	r3, r2, r3
	.loc 1 259 0
	str	r3, [r1, #0]
	.loc 1 262 0
	b	.L26
.L31:
	.loc 1 265 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L39
	.loc 1 267 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	.loc 1 268 0
	ldr	ip, .L41+4
	ldr	r2, [fp, #-12]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	bic	r2, r3, #255
	.loc 1 269 0
	ldr	r3, [fp, #-36]
	and	r3, r3, #255
	orr	r3, r2, r3
	.loc 1 267 0
	str	r3, [r1, #0]
	.loc 1 270 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L39
	.loc 1 272 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L41+4
	ldr	r2, [fp, #-12]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	orr	r3, r3, #-2147483648
	str	r3, [r1, #0]
	.loc 1 275 0
	b	.L39
.L32:
	.loc 1 278 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r4, [r3, #0]
	.loc 1 280 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	.loc 1 279 0
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-36]
	mov	r0, r2
	mov	r1, r3
	bl	spwm_percent_to_ctrl
	mov	r3, r0
	.loc 1 278 0
	str	r3, [r4, #0]
	.loc 1 281 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	ble	.L40
	.loc 1 281 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	cmp	r3, #99
	bgt	.L40
	.loc 1 282 0 is_stmt 1 discriminator 1
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	.loc 1 281 0 discriminator 1
	cmp	r3, #1
	bne	.L40
	.loc 1 284 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L41+4
	ldr	r2, [fp, #-12]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	orr	r3, r3, #-2147483648
	str	r3, [r1, #0]
	.loc 1 286 0
	b	.L40
.L33:
	.loc 1 289 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #8
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	orr	r3, r3, #-2147483648
	str	r3, [fp, #-24]
	.loc 1 290 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	mov	r0, r2
	mov	r1, r3
	bl	spwm_percent_to_ctrl
	mov	r3, r0
	str	r3, [fp, #-24]
	.loc 1 291 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 292 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r1, [r3, #0]
	ldr	ip, .L41+4
	ldr	r2, [fp, #-12]
	mov	r0, #8
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, ip, r3
	add	r3, r3, r0
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #0]
	orr	r3, r3, #-2147483648
	str	r3, [r1, #0]
	.loc 1 293 0
	ldr	r0, .L41+4
	ldr	r2, [fp, #-12]
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #1
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 294 0
	b	.L26
.L27:
	.loc 1 298 0
	mvn	r3, #6
	str	r3, [fp, #-16]
	b	.L26
.L39:
	.loc 1 275 0
	mov	r0, r0	@ nop
	b	.L26
.L40:
	.loc 1 286 0
	mov	r0, r0	@ nop
.L26:
	.loc 1 303 0
	ldr	r3, [fp, #-16]
	.loc 1 304 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L42:
	.align	2
.L41:
	.word	9999
	.word	spwmdat
	.word	spwmdat+12
.LFE3:
	.size	spwm_ioctl, .-spwm_ioctl
	.section	.text.spwm_read,"ax",%progbits
	.align	2
	.global	spwm_read
	.type	spwm_read, %function
spwm_read:
.LFB4:
	.loc 1 330 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI12:
	add	fp, sp, #0
.LCFI13:
	sub	sp, sp, #12
.LCFI14:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	str	r2, [fp, #-12]
	.loc 1 331 0
	mov	r3, #0
	.loc 1 332 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE4:
	.size	spwm_read, .-spwm_read
	.section	.text.spwm_write,"ax",%progbits
	.align	2
	.global	spwm_write
	.type	spwm_write, %function
spwm_write:
.LFB5:
	.loc 1 358 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI15:
	add	fp, sp, #0
.LCFI16:
	sub	sp, sp, #12
.LCFI17:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	str	r2, [fp, #-12]
	.loc 1 359 0
	mov	r3, #0
	.loc 1 360 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE5:
	.size	spwm_write, .-spwm_write
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc3xxx_spwm.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc3xxx_spwm_driver.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x37c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF47
	.byte	0x1
	.4byte	.LASF48
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x61
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x67
	.4byte	0x73
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x61
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0xfd
	.4byte	0x68
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x28
	.4byte	0xb5
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x2a
	.4byte	0xb5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x7
	.4byte	0x56
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x2b
	.4byte	0x9e
	.uleb128 0x5
	.byte	0x8
	.byte	0x4
	.byte	0x21
	.4byte	0xea
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x4
	.byte	0x23
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x4
	.byte	0x24
	.4byte	0x56
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x4
	.byte	0x25
	.4byte	0xc5
	.uleb128 0x8
	.byte	0x4
	.byte	0x4
	.byte	0x2e
	.4byte	0x122
	.uleb128 0x9
	.4byte	.LASF19
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF20
	.sleb128 1
	.uleb128 0x9
	.4byte	.LASF21
	.sleb128 2
	.uleb128 0x9
	.4byte	.LASF22
	.sleb128 3
	.uleb128 0x9
	.4byte	.LASF23
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF24
	.sleb128 5
	.byte	0
	.uleb128 0x5
	.byte	0xc
	.byte	0x1
	.byte	0x20
	.4byte	0x155
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x1
	.byte	0x22
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x1
	.byte	0x23
	.4byte	0x88
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x1
	.byte	0x24
	.4byte	0x155
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0xba
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x1
	.byte	0x25
	.4byte	0x122
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.byte	0x42
	.byte	0x1
	.4byte	0x56
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1a0
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x1
	.byte	0x42
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x1
	.byte	0x42
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.byte	0x75
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x1f6
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x1
	.byte	0x75
	.4byte	0x1f6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xd
	.ascii	"arg\000"
	.byte	0x1
	.byte	0x75
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xe
	.4byte	.LASF34
	.byte	0x1
	.byte	0x77
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xe
	.4byte	.LASF35
	.byte	0x1
	.byte	0x78
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.byte	0x4
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.byte	0xaf
	.byte	0x1
	.4byte	0x93
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x240
	.uleb128 0xc
	.4byte	.LASF37
	.byte	0x1
	.byte	0xaf
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xe
	.4byte	.LASF34
	.byte	0x1
	.byte	0xb1
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xe
	.4byte	.LASF38
	.byte	0x1
	.byte	0xb2
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.byte	0xde
	.byte	0x1
	.4byte	0x93
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x2c0
	.uleb128 0xc
	.4byte	.LASF37
	.byte	0x1
	.byte	0xde
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xd
	.ascii	"cmd\000"
	.byte	0x1
	.byte	0xdf
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xd
	.ascii	"arg\000"
	.byte	0x1
	.byte	0xe0
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x10
	.ascii	"tmp\000"
	.byte	0x1
	.byte	0xe2
	.4byte	0xb5
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xe
	.4byte	.LASF34
	.byte	0x1
	.byte	0xe3
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xe
	.4byte	.LASF40
	.byte	0x1
	.byte	0xe4
	.4byte	0x2c0
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xe
	.4byte	.LASF38
	.byte	0x1
	.byte	0xe5
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.4byte	0xea
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x147
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x312
	.uleb128 0x12
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x147
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x12
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x148
	.4byte	0x1f6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x149
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x163
	.byte	0x1
	.4byte	0x68
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x35e
	.uleb128 0x12
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x163
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x12
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x164
	.4byte	0x1f6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x165
	.4byte	0x68
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x13
	.4byte	0x15b
	.4byte	0x36e
	.uleb128 0x14
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0x1
	.byte	0x28
	.4byte	0x35e
	.byte	0x5
	.byte	0x3
	.4byte	spwmdat
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF8:
	.ascii	"UNS_32\000"
.LASF36:
	.ascii	"spwm_close\000"
.LASF22:
	.ascii	"SPWM_DUTY_REG\000"
.LASF16:
	.ascii	"reload\000"
.LASF43:
	.ascii	"max_bytes\000"
.LASF25:
	.ascii	"init\000"
.LASF38:
	.ascii	"status\000"
.LASF48:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc3xxx_spwm_driver.c\000"
.LASF23:
	.ascii	"SPWM_DUTY_PER\000"
.LASF17:
	.ascii	"duty_per\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF13:
	.ascii	"STATUS\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF9:
	.ascii	"INT_32\000"
.LASF20:
	.ascii	"SPWM_OFF\000"
.LASF21:
	.ascii	"SPWM_RELOAD_REG\000"
.LASF18:
	.ascii	"SPWM_SETUP_T\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF46:
	.ascii	"spwmdat\000"
.LASF26:
	.ascii	"onoff\000"
.LASF41:
	.ascii	"spwm_read\000"
.LASF14:
	.ascii	"SPWM_REGS_T\000"
.LASF33:
	.ascii	"ipbase\000"
.LASF29:
	.ascii	"ctrlreg\000"
.LASF27:
	.ascii	"regptr\000"
.LASF7:
	.ascii	"unsigned int\000"
.LASF39:
	.ascii	"spwm_ioctl\000"
.LASF2:
	.ascii	"char\000"
.LASF44:
	.ascii	"spwm_write\000"
.LASF19:
	.ascii	"SPWM_ON\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF30:
	.ascii	"percent\000"
.LASF11:
	.ascii	"long long int\000"
.LASF15:
	.ascii	"pwm_ctrl\000"
.LASF6:
	.ascii	"short int\000"
.LASF37:
	.ascii	"devid\000"
.LASF42:
	.ascii	"buffer\000"
.LASF32:
	.ascii	"spwm_open\000"
.LASF47:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF24:
	.ascii	"SPWM_SETUP\000"
.LASF1:
	.ascii	"long int\000"
.LASF35:
	.ascii	"tptr\000"
.LASF40:
	.ascii	"pspwmct\000"
.LASF4:
	.ascii	"signed char\000"
.LASF34:
	.ascii	"pwm_id\000"
.LASF31:
	.ascii	"spwm_percent_to_ctrl\000"
.LASF28:
	.ascii	"SPWM_CFG_T\000"
.LASF45:
	.ascii	"n_bytes\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
