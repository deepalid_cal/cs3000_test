	.file	"queue.c"
	.text
.Ltext0:
	.global	xQueueRegistry
	.section	.bss.xQueueRegistry,"aw",%nobits
	.align	2
	.type	xQueueRegistry, %object
	.size	xQueueRegistry, 80
xQueueRegistry:
	.space	80
	.section .rodata
	.align	2
.LC0:
	.ascii	"pxQueue\000"
	.align	2
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/queue.c\000"
	.section	.text.xQueueGenericReset,"ax",%progbits
	.align	2
	.global	xQueueGenericReset
	.type	xQueueGenericReset, %function
xQueueGenericReset:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c"
	.loc 1 280 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 281 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L2
	.loc 1 281 0 is_stmt 0 discriminator 1
	ldr	r0, .L5
	ldr	r1, .L5+4
	ldr	r2, .L5+8
	bl	__assert
.L2:
	.loc 1 283 0 is_stmt 1
	bl	vPortEnterCritical
	.loc 1 285 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	ldr	r1, [fp, #-8]
	ldr	r1, [r1, #64]
	mul	r3, r1, r3
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #4]
	.loc 1 286 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #56]
	.loc 1 287 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #8]
	.loc 1 288 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	sub	r3, r3, #1
	ldr	r1, [fp, #-8]
	ldr	r1, [r1, #64]
	mul	r3, r1, r3
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #12]
	.loc 1 289 0
	ldr	r3, [fp, #-8]
	mvn	r2, #0
	str	r2, [r3, #68]
	.loc 1 290 0
	ldr	r3, [fp, #-8]
	mvn	r2, #0
	str	r2, [r3, #72]
	.loc 1 292 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L3
	.loc 1 299 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L4
	.loc 1 301 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #16
	mov	r0, r3
	bl	xTaskRemoveFromEventList
	mov	r3, r0
	cmp	r3, #1
	bne	.L4
	.loc 1 303 0
@ 303 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
	b	.L4
.L3:
	.loc 1 310 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #16
	mov	r0, r3
	bl	vListInitialise
	.loc 1 311 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #36
	mov	r0, r3
	bl	vListInitialise
.L4:
	.loc 1 314 0
	bl	vPortExitCritical
	.loc 1 318 0
	mov	r3, #1
	.loc 1 319 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	.LC0
	.word	.LC1
	.word	281
.LFE0:
	.size	xQueueGenericReset, .-xQueueGenericReset
	.section .rodata
	.align	2
.LC2:
	.ascii	"xReturn\000"
	.section	.text.xQueueGenericCreate,"ax",%progbits
	.align	2
	.global	xQueueGenericCreate
	.type	xQueueGenericCreate, %function
xQueueGenericCreate:
.LFB1:
	.loc 1 323 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #24
.LCFI5:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	mov	r3, r2
	strb	r3, [fp, #-28]
	.loc 1 326 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 333 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L8
	.loc 1 335 0
	mov	r0, #76
	bl	pvPortMalloc
	str	r0, [fp, #-12]
	.loc 1 336 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L8
	.loc 1 340 0
	ldr	r3, [fp, #-20]
	ldr	r2, [fp, #-24]
	mul	r3, r2, r3
	add	r3, r3, #1
	str	r3, [fp, #-16]
	.loc 1 342 0
	ldr	r0, [fp, #-16]
	bl	pvPortMalloc
	mov	r2, r0
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 343 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L9
	.loc 1 347 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #60]
	.loc 1 348 0
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-24]
	str	r2, [r3, #64]
	.loc 1 349 0
	ldr	r0, [fp, #-12]
	mov	r1, #1
	bl	xQueueGenericReset
	.loc 1 357 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	b	.L8
.L9:
	.loc 1 362 0
	ldr	r0, [fp, #-12]
	bl	vPortFree
.L8:
	.loc 1 367 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L10
	.loc 1 367 0 is_stmt 0 discriminator 1
	ldr	r0, .L11
	ldr	r1, .L11+4
	ldr	r2, .L11+8
	bl	__assert
.L10:
	.loc 1 369 0 is_stmt 1
	ldr	r3, [fp, #-8]
	.loc 1 370 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L12:
	.align	2
.L11:
	.word	.LC2
	.word	.LC1
	.word	367
.LFE1:
	.size	xQueueGenericCreate, .-xQueueGenericCreate
	.section .rodata
	.align	2
.LC3:
	.ascii	"pxNewQueue\000"
	.section	.text.xQueueCreateMutex,"ax",%progbits
	.align	2
	.global	xQueueCreateMutex
	.type	xQueueCreateMutex, %function
xQueueCreateMutex:
.LFB2:
	.loc 1 376 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	mov	r3, r0
	strb	r3, [fp, #-12]
	.loc 1 384 0
	mov	r0, #76
	bl	pvPortMalloc
	str	r0, [fp, #-8]
	.loc 1 385 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L14
	.loc 1 388 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 389 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 393 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #8]
	.loc 1 394 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #12]
	.loc 1 399 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #56]
	.loc 1 400 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	str	r2, [r3, #60]
	.loc 1 401 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #64]
	.loc 1 402 0
	ldr	r3, [fp, #-8]
	mvn	r2, #0
	str	r2, [r3, #68]
	.loc 1 403 0
	ldr	r3, [fp, #-8]
	mvn	r2, #0
	str	r2, [r3, #72]
	.loc 1 412 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #16
	mov	r0, r3
	bl	vListInitialise
	.loc 1 413 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #36
	mov	r0, r3
	bl	vListInitialise
	.loc 1 418 0
	ldr	r0, [fp, #-8]
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
.L14:
	.loc 1 425 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L15
	.loc 1 425 0 is_stmt 0 discriminator 1
	ldr	r0, .L16
	ldr	r1, .L16+4
	ldr	r2, .L16+8
	bl	__assert
.L15:
	.loc 1 426 0 is_stmt 1
	ldr	r3, [fp, #-8]
	.loc 1 427 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	.LC3
	.word	.LC1
	.word	425
.LFE2:
	.size	xQueueCreateMutex, .-xQueueCreateMutex
	.section	.text.xQueueGetMutexHolder,"ax",%progbits
	.align	2
	.global	xQueueGetMutexHolder
	.type	xQueueGetMutexHolder, %function
xQueueGetMutexHolder:
.LFB3:
	.loc 1 435 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-12]
	.loc 1 443 0
	bl	vPortEnterCritical
	.loc 1 445 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L19
	.loc 1 447 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-8]
	b	.L20
.L19:
	.loc 1 451 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L20:
	.loc 1 454 0
	bl	vPortExitCritical
	.loc 1 456 0
	ldr	r3, [fp, #-8]
	.loc 1 457 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE3:
	.size	xQueueGetMutexHolder, .-xQueueGetMutexHolder
	.section .rodata
	.align	2
.LC4:
	.ascii	"pxMutex\000"
	.section	.text.xQueueGiveMutexRecursive,"ax",%progbits
	.align	2
	.global	xQueueGiveMutexRecursive
	.type	xQueueGiveMutexRecursive, %function
xQueueGiveMutexRecursive:
.LFB4:
	.loc 1 465 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI12:
	add	fp, sp, #8
.LCFI13:
	sub	sp, sp, #8
.LCFI14:
	str	r0, [fp, #-16]
	.loc 1 468 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L22
	.loc 1 468 0 is_stmt 0 discriminator 1
	ldr	r0, .L26
	ldr	r1, .L26+4
	mov	r2, #468
	bl	__assert
.L22:
	.loc 1 476 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #4]
	bl	xTaskGetCurrentTaskHandle
	mov	r3, r0
	cmp	r4, r3
	bne	.L23
	.loc 1 485 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #12]
	sub	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #12]
	.loc 1 488 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #12]
	cmp	r3, #0
	bne	.L24
	.loc 1 492 0
	ldr	r0, [fp, #-16]
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
.L24:
	.loc 1 495 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L25
.L23:
	.loc 1 500 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L25:
	.loc 1 505 0
	ldr	r3, [fp, #-12]
	.loc 1 506 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L27:
	.align	2
.L26:
	.word	.LC4
	.word	.LC1
.LFE4:
	.size	xQueueGiveMutexRecursive, .-xQueueGiveMutexRecursive
	.section .rodata
	.align	2
.LC5:
	.ascii	"MUTEX TAKE FAILED: %s, %u, held by %s\000"
	.section	.text.my_take_recursive_failed,"ax",%progbits
	.align	2
	.global	my_take_recursive_failed
	.type	my_take_recursive_failed, %function
my_take_recursive_failed:
.LFB5:
	.loc 1 561 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #16
.LCFI17:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	mov	r3, r2
	strh	r3, [fp, #-20]	@ movhi
	.loc 1 565 0
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	.loc 1 567 0
	ldrh	r2, [fp, #-20]
	ldr	r3, [fp, #-8]
	add	r3, r3, #52
	ldr	r0, .L29
	ldr	r1, [fp, #-16]
	bl	Alert_Message_va
	.loc 1 568 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L30:
	.align	2
.L29:
	.word	.LC5
.LFE5:
	.size	my_take_recursive_failed, .-my_take_recursive_failed
	.section	.text.xQueueTakeMutexRecursive_debug,"ax",%progbits
	.align	2
	.global	xQueueTakeMutexRecursive_debug
	.type	xQueueTakeMutexRecursive_debug, %function
xQueueTakeMutexRecursive_debug:
.LFB6:
	.loc 1 574 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI18:
	add	fp, sp, #8
.LCFI19:
	sub	sp, sp, #20
.LCFI20:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	strh	r3, [fp, #-28]	@ movhi
	.loc 1 577 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L32
	.loc 1 577 0 is_stmt 0 discriminator 1
	ldr	r0, .L36
	ldr	r1, .L36+4
	ldr	r2, .L36+8
	bl	__assert
.L32:
	.loc 1 584 0 is_stmt 1
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #4]
	bl	xTaskGetCurrentTaskHandle
	mov	r3, r0
	cmp	r4, r3
	bne	.L33
	.loc 1 586 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #12]
	add	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #12]
	.loc 1 587 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L34
.L33:
	.loc 1 591 0
	ldr	r0, [fp, #-16]
	mov	r1, #0
	ldr	r2, [fp, #-20]
	mov	r3, #0
	bl	xQueueGenericReceive
	str	r0, [fp, #-12]
	.loc 1 595 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L35
	.loc 1 597 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #12]
	add	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #12]
	b	.L34
.L35:
	.loc 1 603 0
	ldr	r3, [fp, #-16]
	ldr	r4, [r3, #4]
	ldr	r0, [fp, #-24]
	bl	RemovePathFromFileName
	mov	r2, r0
	ldrh	r3, [fp, #-28]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	bl	my_take_recursive_failed
.L34:
	.loc 1 607 0
	ldr	r3, [fp, #-12]
	.loc 1 608 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L37:
	.align	2
.L36:
	.word	.LC4
	.word	.LC1
	.word	577
.LFE6:
	.size	xQueueTakeMutexRecursive_debug, .-xQueueTakeMutexRecursive_debug
	.section .rodata
	.align	2
.LC6:
	.ascii	"!( ( pvItemToQueue == 0 ) && ( pxQueue->uxItemSize "
	.ascii	"!= ( unsigned long ) 0U ) )\000"
	.section	.text.xQueueGenericSend,"ax",%progbits
	.align	2
	.global	xQueueGenericSend
	.type	xQueueGenericSend, %function
xQueueGenericSend:
.LFB7:
	.loc 1 640 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #28
.LCFI23:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	str	r3, [fp, #-32]
	.loc 1 641 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 644 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L39
	.loc 1 644 0 is_stmt 0 discriminator 1
	ldr	r0, .L52
	ldr	r1, .L52+4
	mov	r2, #644
	bl	__assert
.L39:
	.loc 1 645 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L40
	.loc 1 645 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #64]
	cmp	r3, #0
	beq	.L40
	.loc 1 645 0 discriminator 2
	ldr	r0, .L52+8
	ldr	r1, .L52+4
	ldr	r2, .L52+12
	bl	__assert
	b	.L40
.L51:
	.loc 1 754 0 is_stmt 1
	mov	r0, r0	@ nop
.L40:
	.loc 1 652 0
	bl	vPortEnterCritical
	.loc 1 656 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #56]
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #60]
	cmp	r2, r3
	bcs	.L41
	.loc 1 659 0
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-24]
	ldr	r2, [fp, #-32]
	bl	prvCopyDataToQueue
	.loc 1 663 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L42
	.loc 1 665 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #36
	mov	r0, r3
	bl	xTaskRemoveFromEventList
	mov	r3, r0
	cmp	r3, #1
	bne	.L42
	.loc 1 671 0
@ 671 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
.L42:
	.loc 1 675 0
	bl	vPortExitCritical
	.loc 1 679 0
	mov	r3, #1
	b	.L43
.L41:
	.loc 1 683 0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L44
	.loc 1 687 0
	bl	vPortExitCritical
	.loc 1 692 0
	mov	r3, #0
	b	.L43
.L44:
	.loc 1 694 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L45
	.loc 1 698 0
	sub	r3, fp, #16
	mov	r0, r3
	bl	vTaskSetTimeOutState
	.loc 1 699 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L45:
	.loc 1 703 0
	bl	vPortExitCritical
	.loc 1 708 0
	bl	vTaskSuspendAll
	.loc 1 709 0
	bl	vPortEnterCritical
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #68]
	cmn	r3, #1
	bne	.L46
	.loc 1 709 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-20]
	mov	r2, #0
	str	r2, [r3, #68]
.L46:
	.loc 1 709 0 discriminator 2
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #72]
	cmn	r3, #1
	bne	.L47
	.loc 1 709 0 discriminator 3
	ldr	r3, [fp, #-20]
	mov	r2, #0
	str	r2, [r3, #72]
.L47:
	.loc 1 709 0 discriminator 4
	bl	vPortExitCritical
	.loc 1 712 0 is_stmt 1 discriminator 4
	sub	r2, fp, #16
	sub	r3, fp, #28
	mov	r0, r2
	mov	r1, r3
	bl	xTaskCheckForTimeOut
	mov	r3, r0
	cmp	r3, #0
	bne	.L48
	.loc 1 714 0
	ldr	r0, [fp, #-20]
	bl	prvIsQueueFull
	mov	r3, r0
	cmp	r3, #0
	beq	.L49
	.loc 1 717 0
	ldr	r3, [fp, #-20]
	add	r2, r3, #16
	ldr	r3, [fp, #-28]
	mov	r0, r2
	mov	r1, r3
	bl	vTaskPlaceOnEventList
	.loc 1 724 0
	ldr	r0, [fp, #-20]
	bl	prvUnlockQueue
	.loc 1 731 0
	bl	xTaskResumeAll
	mov	r3, r0
	cmp	r3, #0
	bne	.L51
	.loc 1 733 0
@ 733 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
	.loc 1 754 0
	b	.L51
.L49:
	.loc 1 739 0
	ldr	r0, [fp, #-20]
	bl	prvUnlockQueue
	.loc 1 740 0
	bl	xTaskResumeAll
	.loc 1 754 0
	b	.L51
.L48:
	.loc 1 746 0
	ldr	r0, [fp, #-20]
	bl	prvUnlockQueue
	.loc 1 747 0
	bl	xTaskResumeAll
	.loc 1 752 0
	mov	r3, #0
.L43:
	.loc 1 755 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L53:
	.align	2
.L52:
	.word	.LC0
	.word	.LC1
	.word	.LC6
	.word	645
.LFE7:
	.size	xQueueGenericSend, .-xQueueGenericSend
	.section	.text.xQueueGenericSendFromISR,"ax",%progbits
	.align	2
	.global	xQueueGenericSendFromISR
	.type	xQueueGenericSendFromISR, %function
xQueueGenericSendFromISR:
.LFB8:
	.loc 1 965 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #24
.LCFI26:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 969 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L55
	.loc 1 969 0 is_stmt 0 discriminator 1
	ldr	r0, .L61
	ldr	r1, .L61+4
	ldr	r2, .L61+8
	bl	__assert
.L55:
	.loc 1 970 0 is_stmt 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L56
	.loc 1 970 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #64]
	cmp	r3, #0
	beq	.L56
	.loc 1 970 0 discriminator 2
	ldr	r0, .L61+12
	ldr	r1, .L61+4
	ldr	r2, .L61+16
	bl	__assert
.L56:
	.loc 1 977 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 979 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #56]
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #60]
	cmp	r2, r3
	bcs	.L57
	.loc 1 983 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-20]
	ldr	r2, [fp, #-28]
	bl	prvCopyDataToQueue
	.loc 1 987 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #72]
	cmn	r3, #1
	bne	.L58
	.loc 1 989 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L59
	.loc 1 991 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #36
	mov	r0, r3
	bl	xTaskRemoveFromEventList
	mov	r3, r0
	cmp	r3, #0
	beq	.L59
	.loc 1 995 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L59
	.loc 1 997 0
	ldr	r3, [fp, #-24]
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L59
.L58:
	.loc 1 1006 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #72]
	add	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #72]
.L59:
	.loc 1 1009 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L60
.L57:
	.loc 1 1014 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L60:
	.loc 1 1019 0
	ldr	r3, [fp, #-8]
	.loc 1 1020 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L62:
	.align	2
.L61:
	.word	.LC0
	.word	.LC1
	.word	969
	.word	.LC6
	.word	970
.LFE8:
	.size	xQueueGenericSendFromISR, .-xQueueGenericSendFromISR
	.section .rodata
	.align	2
.LC7:
	.ascii	"!( ( pvBuffer == 0 ) && ( pxQueue->uxItemSize != ( "
	.ascii	"unsigned long ) 0U ) )\000"
	.section	.text.xQueueGenericReceive,"ax",%progbits
	.align	2
	.global	xQueueGenericReceive
	.type	xQueueGenericReceive, %function
xQueueGenericReceive:
.LFB9:
	.loc 1 1024 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #32
.LCFI29:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	str	r3, [fp, #-36]
	.loc 1 1025 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 1029 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L64
	.loc 1 1029 0 is_stmt 0 discriminator 1
	ldr	r0, .L80
	ldr	r1, .L80+4
	ldr	r2, .L80+8
	bl	__assert
.L64:
	.loc 1 1030 0 is_stmt 1
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bne	.L65
	.loc 1 1030 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #64]
	cmp	r3, #0
	beq	.L65
	.loc 1 1030 0 discriminator 2
	ldr	r0, .L80+12
	ldr	r1, .L80+4
	ldr	r2, .L80+16
	bl	__assert
	b	.L65
.L79:
	.loc 1 1168 0 is_stmt 1
	mov	r0, r0	@ nop
.L65:
	.loc 1 1038 0
	bl	vPortEnterCritical
	.loc 1 1042 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #56]
	cmp	r3, #0
	beq	.L66
	.loc 1 1045 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #12]
	str	r3, [fp, #-12]
	.loc 1 1047 0
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	bl	prvCopyDataFromQueue
	.loc 1 1049 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	bne	.L67
	.loc 1 1054 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #56]
	sub	r2, r3, #1
	ldr	r3, [fp, #-24]
	str	r2, [r3, #56]
	.loc 1 1058 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L68
	.loc 1 1062 0
	bl	xTaskGetCurrentTaskHandle
	mov	r2, r0
	ldr	r3, [fp, #-24]
	str	r2, [r3, #4]
.L68:
	.loc 1 1067 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L69
	.loc 1 1069 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #16
	mov	r0, r3
	bl	xTaskRemoveFromEventList
	mov	r3, r0
	cmp	r3, #1
	bne	.L69
	.loc 1 1071 0
@ 1071 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
	b	.L69
.L67:
	.loc 1 1081 0
	ldr	r3, [fp, #-24]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #12]
	.loc 1 1085 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L69
	.loc 1 1089 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #36
	mov	r0, r3
	bl	xTaskRemoveFromEventList
	mov	r3, r0
	cmp	r3, #0
	beq	.L69
	.loc 1 1092 0
@ 1092 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
.L69:
	.loc 1 1097 0
	bl	vPortExitCritical
	.loc 1 1098 0
	mov	r3, #1
	b	.L70
.L66:
	.loc 1 1102 0
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	bne	.L71
	.loc 1 1106 0
	bl	vPortExitCritical
	.loc 1 1108 0
	mov	r3, #0
	b	.L70
.L71:
	.loc 1 1110 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L72
	.loc 1 1114 0
	sub	r3, fp, #20
	mov	r0, r3
	bl	vTaskSetTimeOutState
	.loc 1 1115 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L72:
	.loc 1 1119 0
	bl	vPortExitCritical
	.loc 1 1124 0
	bl	vTaskSuspendAll
	.loc 1 1125 0
	bl	vPortEnterCritical
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #68]
	cmn	r3, #1
	bne	.L73
	.loc 1 1125 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-24]
	mov	r2, #0
	str	r2, [r3, #68]
.L73:
	.loc 1 1125 0 discriminator 2
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #72]
	cmn	r3, #1
	bne	.L74
	.loc 1 1125 0 discriminator 3
	ldr	r3, [fp, #-24]
	mov	r2, #0
	str	r2, [r3, #72]
.L74:
	.loc 1 1125 0 discriminator 4
	bl	vPortExitCritical
	.loc 1 1128 0 is_stmt 1 discriminator 4
	sub	r2, fp, #20
	sub	r3, fp, #32
	mov	r0, r2
	mov	r1, r3
	bl	xTaskCheckForTimeOut
	mov	r3, r0
	cmp	r3, #0
	bne	.L75
	.loc 1 1130 0
	ldr	r0, [fp, #-24]
	bl	prvIsQueueEmpty
	mov	r3, r0
	cmp	r3, #0
	beq	.L76
	.loc 1 1136 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L77
	.loc 1 1138 0
	bl	vPortEnterCritical
	.loc 1 1140 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	mov	r0, r3
	bl	vTaskPriorityInherit
	.loc 1 1142 0
	bl	vPortExitCritical
.L77:
	.loc 1 1147 0
	ldr	r3, [fp, #-24]
	add	r2, r3, #36
	ldr	r3, [fp, #-32]
	mov	r0, r2
	mov	r1, r3
	bl	vTaskPlaceOnEventList
	.loc 1 1148 0
	ldr	r0, [fp, #-24]
	bl	prvUnlockQueue
	.loc 1 1149 0
	bl	xTaskResumeAll
	mov	r3, r0
	cmp	r3, #0
	bne	.L79
	.loc 1 1151 0
@ 1151 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
	.loc 1 1168 0
	b	.L79
.L76:
	.loc 1 1157 0
	ldr	r0, [fp, #-24]
	bl	prvUnlockQueue
	.loc 1 1158 0
	bl	xTaskResumeAll
	.loc 1 1168 0
	b	.L79
.L75:
	.loc 1 1163 0
	ldr	r0, [fp, #-24]
	bl	prvUnlockQueue
	.loc 1 1164 0
	bl	xTaskResumeAll
	.loc 1 1166 0
	mov	r3, #0
.L70:
	.loc 1 1169 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L81:
	.align	2
.L80:
	.word	.LC0
	.word	.LC1
	.word	1029
	.word	.LC7
	.word	1030
.LFE9:
	.size	xQueueGenericReceive, .-xQueueGenericReceive
	.section	.text.xQueueReceiveFromISR,"ax",%progbits
	.align	2
	.global	xQueueReceiveFromISR
	.type	xQueueReceiveFromISR, %function
xQueueReceiveFromISR:
.LFB10:
	.loc 1 1173 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #20
.LCFI32:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	.loc 1 1177 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L83
	.loc 1 1177 0 is_stmt 0 discriminator 1
	ldr	r0, .L89
	ldr	r1, .L89+4
	ldr	r2, .L89+8
	bl	__assert
.L83:
	.loc 1 1178 0 is_stmt 1
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L84
	.loc 1 1178 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #64]
	cmp	r3, #0
	beq	.L84
	.loc 1 1178 0 discriminator 2
	ldr	r0, .L89+12
	ldr	r1, .L89+4
	ldr	r2, .L89+16
	bl	__assert
.L84:
	.loc 1 1180 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 1183 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #56]
	cmp	r3, #0
	beq	.L85
	.loc 1 1187 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-20]
	bl	prvCopyDataFromQueue
	.loc 1 1188 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #56]
	sub	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #56]
	.loc 1 1193 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #68]
	cmn	r3, #1
	bne	.L86
	.loc 1 1195 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L87
	.loc 1 1197 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #16
	mov	r0, r3
	bl	xTaskRemoveFromEventList
	mov	r3, r0
	cmp	r3, #0
	beq	.L87
	.loc 1 1201 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L87
	.loc 1 1203 0
	ldr	r3, [fp, #-24]
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L87
.L86:
	.loc 1 1212 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #68]
	add	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #68]
.L87:
	.loc 1 1215 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L88
.L85:
	.loc 1 1219 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L88:
	.loc 1 1225 0
	ldr	r3, [fp, #-8]
	.loc 1 1226 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L90:
	.align	2
.L89:
	.word	.LC0
	.word	.LC1
	.word	1177
	.word	.LC7
	.word	1178
.LFE10:
	.size	xQueueReceiveFromISR, .-xQueueReceiveFromISR
	.section	.text.uxQueueMessagesWaiting,"ax",%progbits
	.align	2
	.global	uxQueueMessagesWaiting
	.type	uxQueueMessagesWaiting, %function
uxQueueMessagesWaiting:
.LFB11:
	.loc 1 1230 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #8
.LCFI35:
	str	r0, [fp, #-12]
	.loc 1 1233 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L92
	.loc 1 1233 0 is_stmt 0 discriminator 1
	ldr	r0, .L93
	ldr	r1, .L93+4
	ldr	r2, .L93+8
	bl	__assert
.L92:
	.loc 1 1235 0 is_stmt 1
	bl	vPortEnterCritical
	.loc 1 1236 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #56]
	str	r3, [fp, #-8]
	.loc 1 1237 0
	bl	vPortExitCritical
	.loc 1 1239 0
	ldr	r3, [fp, #-8]
	.loc 1 1240 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L94:
	.align	2
.L93:
	.word	.LC0
	.word	.LC1
	.word	1233
.LFE11:
	.size	uxQueueMessagesWaiting, .-uxQueueMessagesWaiting
	.section	.text.uxQueueMessagesWaitingFromISR,"ax",%progbits
	.align	2
	.global	uxQueueMessagesWaitingFromISR
	.type	uxQueueMessagesWaitingFromISR, %function
uxQueueMessagesWaitingFromISR:
.LFB12:
	.loc 1 1244 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #8
.LCFI38:
	str	r0, [fp, #-12]
	.loc 1 1247 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L96
	.loc 1 1247 0 is_stmt 0 discriminator 1
	ldr	r0, .L97
	ldr	r1, .L97+4
	ldr	r2, .L97+8
	bl	__assert
.L96:
	.loc 1 1249 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #56]
	str	r3, [fp, #-8]
	.loc 1 1251 0
	ldr	r3, [fp, #-8]
	.loc 1 1252 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L98:
	.align	2
.L97:
	.word	.LC0
	.word	.LC1
	.word	1247
.LFE12:
	.size	uxQueueMessagesWaitingFromISR, .-uxQueueMessagesWaitingFromISR
	.section	.text.vQueueDelete,"ax",%progbits
	.align	2
	.global	vQueueDelete
	.type	vQueueDelete, %function
vQueueDelete:
.LFB13:
	.loc 1 1256 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI39:
	add	fp, sp, #4
.LCFI40:
	sub	sp, sp, #4
.LCFI41:
	str	r0, [fp, #-8]
	.loc 1 1257 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L100
	.loc 1 1257 0 is_stmt 0 discriminator 1
	ldr	r0, .L101
	ldr	r1, .L101+4
	ldr	r2, .L101+8
	bl	__assert
.L100:
	.loc 1 1260 0 is_stmt 1
	ldr	r0, [fp, #-8]
	bl	vQueueUnregisterQueue
	.loc 1 1261 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	vPortFree
	.loc 1 1262 0
	ldr	r0, [fp, #-8]
	bl	vPortFree
	.loc 1 1263 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L102:
	.align	2
.L101:
	.word	.LC0
	.word	.LC1
	.word	1257
.LFE13:
	.size	vQueueDelete, .-vQueueDelete
	.section	.text.prvCopyDataToQueue,"ax",%progbits
	.align	2
	.type	prvCopyDataToQueue, %function
prvCopyDataToQueue:
.LFB14:
	.loc 1 1297 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI42:
	add	fp, sp, #4
.LCFI43:
	sub	sp, sp, #12
.LCFI44:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 1298 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	cmp	r3, #0
	bne	.L104
	.loc 1 1302 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L105
	.loc 1 1305 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	mov	r0, r3
	bl	vTaskPriorityDisinherit
	.loc 1 1306 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #4]
	b	.L105
.L104:
	.loc 1 1311 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	bne	.L106
	.loc 1 1313 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	mov	r0, r2
	ldr	r1, [fp, #-12]
	mov	r2, r3
	bl	memcpy
	.loc 1 1314 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #8]
	.loc 1 1315 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	cmp	r2, r3
	bcc	.L105
	.loc 1 1317 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #8]
	b	.L105
.L106:
	.loc 1 1322 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	mov	r0, r2
	ldr	r1, [fp, #-12]
	mov	r2, r3
	bl	memcpy
	.loc 1 1323 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	rsb	r3, r3, #0
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #12]
	.loc 1 1324 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bcs	.L105
	.loc 1 1326 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	rsb	r3, r3, #0
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #12]
.L105:
	.loc 1 1330 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #56]
	add	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #56]
	.loc 1 1331 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE14:
	.size	prvCopyDataToQueue, .-prvCopyDataToQueue
	.section	.text.prvCopyDataFromQueue,"ax",%progbits
	.align	2
	.type	prvCopyDataFromQueue, %function
prvCopyDataFromQueue:
.LFB15:
	.loc 1 1335 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI45:
	add	fp, sp, #4
.LCFI46:
	sub	sp, sp, #8
.LCFI47:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1336 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L107
	.loc 1 1338 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	add	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #12]
	.loc 1 1339 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	cmp	r2, r3
	bcc	.L109
	.loc 1 1341 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #12]
.L109:
	.loc 1 1343 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #64]
	ldr	r0, [fp, #-12]
	mov	r1, r2
	mov	r2, r3
	bl	memcpy
.L107:
	.loc 1 1345 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE15:
	.size	prvCopyDataFromQueue, .-prvCopyDataFromQueue
	.section	.text.prvUnlockQueue,"ax",%progbits
	.align	2
	.type	prvUnlockQueue, %function
prvUnlockQueue:
.LFB16:
	.loc 1 1349 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI48:
	add	fp, sp, #4
.LCFI49:
	sub	sp, sp, #4
.LCFI50:
	str	r0, [fp, #-8]
	.loc 1 1356 0
	bl	vPortEnterCritical
	.loc 1 1359 0
	b	.L111
.L115:
	.loc 1 1363 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L121
	.loc 1 1367 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #36
	mov	r0, r3
	bl	xTaskRemoveFromEventList
	mov	r3, r0
	cmp	r3, #0
	beq	.L113
	.loc 1 1371 0
	bl	vTaskMissedYield
.L113:
	.loc 1 1374 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #72]
	sub	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #72]
.L111:
	.loc 1 1359 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #72]
	cmp	r3, #0
	bgt	.L115
	b	.L114
.L121:
	.loc 1 1378 0
	mov	r0, r0	@ nop
.L114:
	.loc 1 1382 0
	ldr	r3, [fp, #-8]
	mvn	r2, #0
	str	r2, [r3, #72]
	.loc 1 1384 0
	bl	vPortExitCritical
	.loc 1 1387 0
	bl	vPortEnterCritical
	.loc 1 1389 0
	b	.L116
.L120:
	.loc 1 1391 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L122
	.loc 1 1393 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #16
	mov	r0, r3
	bl	xTaskRemoveFromEventList
	mov	r3, r0
	cmp	r3, #0
	beq	.L118
	.loc 1 1395 0
	bl	vTaskMissedYield
.L118:
	.loc 1 1398 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #68]
	sub	r2, r3, #1
	ldr	r3, [fp, #-8]
	str	r2, [r3, #68]
.L116:
	.loc 1 1389 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #68]
	cmp	r3, #0
	bgt	.L120
	b	.L119
.L122:
	.loc 1 1402 0
	mov	r0, r0	@ nop
.L119:
	.loc 1 1406 0
	ldr	r3, [fp, #-8]
	mvn	r2, #0
	str	r2, [r3, #68]
	.loc 1 1408 0
	bl	vPortExitCritical
	.loc 1 1409 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE16:
	.size	prvUnlockQueue, .-prvUnlockQueue
	.section	.text.prvIsQueueEmpty,"ax",%progbits
	.align	2
	.type	prvIsQueueEmpty, %function
prvIsQueueEmpty:
.LFB17:
	.loc 1 1413 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI51:
	add	fp, sp, #4
.LCFI52:
	sub	sp, sp, #8
.LCFI53:
	str	r0, [fp, #-12]
	.loc 1 1416 0
	bl	vPortEnterCritical
	.loc 1 1417 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #56]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	str	r3, [fp, #-8]
	.loc 1 1418 0
	bl	vPortExitCritical
	.loc 1 1420 0
	ldr	r3, [fp, #-8]
	.loc 1 1421 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE17:
	.size	prvIsQueueEmpty, .-prvIsQueueEmpty
	.section	.text.xQueueIsQueueEmptyFromISR,"ax",%progbits
	.align	2
	.global	xQueueIsQueueEmptyFromISR
	.type	xQueueIsQueueEmptyFromISR, %function
xQueueIsQueueEmptyFromISR:
.LFB18:
	.loc 1 1425 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI54:
	add	fp, sp, #4
.LCFI55:
	sub	sp, sp, #8
.LCFI56:
	str	r0, [fp, #-12]
	.loc 1 1428 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L125
	.loc 1 1428 0 is_stmt 0 discriminator 1
	ldr	r0, .L126
	ldr	r1, .L126+4
	ldr	r2, .L126+8
	bl	__assert
.L125:
	.loc 1 1429 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #56]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	str	r3, [fp, #-8]
	.loc 1 1431 0
	ldr	r3, [fp, #-8]
	.loc 1 1432 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L127:
	.align	2
.L126:
	.word	.LC0
	.word	.LC1
	.word	1428
.LFE18:
	.size	xQueueIsQueueEmptyFromISR, .-xQueueIsQueueEmptyFromISR
	.section	.text.prvIsQueueFull,"ax",%progbits
	.align	2
	.type	prvIsQueueFull, %function
prvIsQueueFull:
.LFB19:
	.loc 1 1436 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI57:
	add	fp, sp, #4
.LCFI58:
	sub	sp, sp, #8
.LCFI59:
	str	r0, [fp, #-12]
	.loc 1 1439 0
	bl	vPortEnterCritical
	.loc 1 1440 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #56]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #60]
	cmp	r2, r3
	movne	r3, #0
	moveq	r3, #1
	str	r3, [fp, #-8]
	.loc 1 1441 0
	bl	vPortExitCritical
	.loc 1 1443 0
	ldr	r3, [fp, #-8]
	.loc 1 1444 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE19:
	.size	prvIsQueueFull, .-prvIsQueueFull
	.section	.text.xQueueIsQueueFullFromISR,"ax",%progbits
	.align	2
	.global	xQueueIsQueueFullFromISR
	.type	xQueueIsQueueFullFromISR, %function
xQueueIsQueueFullFromISR:
.LFB20:
	.loc 1 1448 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI60:
	add	fp, sp, #4
.LCFI61:
	sub	sp, sp, #8
.LCFI62:
	str	r0, [fp, #-12]
	.loc 1 1451 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L130
	.loc 1 1451 0 is_stmt 0 discriminator 1
	ldr	r0, .L131
	ldr	r1, .L131+4
	ldr	r2, .L131+8
	bl	__assert
.L130:
	.loc 1 1452 0 is_stmt 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #56]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #60]
	cmp	r2, r3
	movne	r3, #0
	moveq	r3, #1
	str	r3, [fp, #-8]
	.loc 1 1454 0
	ldr	r3, [fp, #-8]
	.loc 1 1455 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L132:
	.align	2
.L131:
	.word	.LC0
	.word	.LC1
	.word	1451
.LFE20:
	.size	xQueueIsQueueFullFromISR, .-xQueueIsQueueFullFromISR
	.section	.text.vQueueAddToRegistry,"ax",%progbits
	.align	2
	.global	vQueueAddToRegistry
	.type	vQueueAddToRegistry, %function
vQueueAddToRegistry:
.LFB21:
	.loc 1 1673 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI63:
	add	fp, sp, #0
.LCFI64:
	sub	sp, sp, #12
.LCFI65:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1678 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L134
.L137:
	.loc 1 1680 0
	ldr	r3, .L138
	ldr	r2, [fp, #-4]
	ldr	r3, [r3, r2, asl #3]
	cmp	r3, #0
	bne	.L135
	.loc 1 1683 0
	ldr	r3, .L138
	ldr	r2, [fp, #-4]
	ldr	r1, [fp, #-12]
	str	r1, [r3, r2, asl #3]
	.loc 1 1684 0
	ldr	r1, .L138
	ldr	r2, [fp, #-4]
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 1685 0
	b	.L133
.L135:
	.loc 1 1678 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L134:
	.loc 1 1678 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #9
	bls	.L137
.L133:
	.loc 1 1688 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L139:
	.align	2
.L138:
	.word	xQueueRegistry
.LFE21:
	.size	vQueueAddToRegistry, .-vQueueAddToRegistry
	.section	.text.vQueueUnregisterQueue,"ax",%progbits
	.align	2
	.type	vQueueUnregisterQueue, %function
vQueueUnregisterQueue:
.LFB22:
	.loc 1 1696 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI66:
	add	fp, sp, #0
.LCFI67:
	sub	sp, sp, #8
.LCFI68:
	str	r0, [fp, #-8]
	.loc 1 1701 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L141
.L144:
	.loc 1 1703 0
	ldr	r1, .L145
	ldr	r2, [fp, #-4]
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bne	.L142
	.loc 1 1706 0
	ldr	r3, .L145
	ldr	r2, [fp, #-4]
	mov	r1, #0
	str	r1, [r3, r2, asl #3]
	.loc 1 1707 0
	b	.L140
.L142:
	.loc 1 1701 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L141:
	.loc 1 1701 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #9
	bls	.L144
.L140:
	.loc 1 1711 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L146:
	.align	2
.L145:
	.word	xQueueRegistry
.LFE22:
	.size	vQueueUnregisterQueue, .-vQueueUnregisterQueue
	.section	.text.vQueueWaitForMessageRestricted,"ax",%progbits
	.align	2
	.global	vQueueWaitForMessageRestricted
	.type	vQueueWaitForMessageRestricted, %function
vQueueWaitForMessageRestricted:
.LFB23:
	.loc 1 1719 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI69:
	add	fp, sp, #4
.LCFI70:
	sub	sp, sp, #8
.LCFI71:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 1734 0
	bl	vPortEnterCritical
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #68]
	cmn	r3, #1
	bne	.L148
	.loc 1 1734 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #68]
.L148:
	.loc 1 1734 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #72]
	cmn	r3, #1
	bne	.L149
	.loc 1 1734 0 discriminator 3
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #72]
.L149:
	.loc 1 1734 0 discriminator 4
	bl	vPortExitCritical
	.loc 1 1735 0 is_stmt 1 discriminator 4
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #56]
	cmp	r3, #0
	bne	.L150
	.loc 1 1738 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #36
	mov	r0, r3
	ldr	r1, [fp, #-12]
	bl	vTaskPlaceOnEventListRestricted
.L150:
	.loc 1 1740 0
	ldr	r0, [fp, #-8]
	bl	prvUnlockQueue
	.loc 1 1741 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE23:
	.size	vQueueWaitForMessageRestricted, .-vQueueWaitForMessageRestricted
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI45-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI48-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI51-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI52-.LCFI51
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI54-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI55-.LCFI54
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI57-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI58-.LCFI57
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI60-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI61-.LCFI60
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI63-.LFB21
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI64-.LCFI63
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI66-.LFB22
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI67-.LCFI66
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI69-.LFB23
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI70-.LCFI69
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE46:
	.text
.Letext0:
	.file 2 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/stdlib.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/FreeRTOS.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/list.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/task.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xafa
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF109
	.byte	0x1
	.4byte	.LASF110
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x2
	.byte	0x16
	.4byte	0x30
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x5
	.byte	0x4
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.4byte	.LASF4
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF6
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x3
	.byte	0x35
	.4byte	0x30
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x4
	.byte	0x58
	.4byte	0x87
	.uleb128 0x6
	.byte	0x4
	.4byte	0x8d
	.uleb128 0x7
	.byte	0x1
	.4byte	0x3e
	.4byte	0x9d
	.uleb128 0x8
	.4byte	0x53
	.byte	0
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x14
	.byte	0x5
	.byte	0x69
	.4byte	0xf0
	.uleb128 0xa
	.4byte	.LASF11
	.byte	0x5
	.byte	0x6b
	.4byte	0x71
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF12
	.byte	0x5
	.byte	0x6c
	.4byte	0xf0
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF13
	.byte	0x5
	.byte	0x6d
	.4byte	0xf0
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF14
	.byte	0x5
	.byte	0x6e
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF15
	.byte	0x5
	.byte	0x6f
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0xf6
	.uleb128 0xb
	.4byte	0x9d
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x5
	.byte	0x71
	.4byte	0x9d
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0xc
	.byte	0x5
	.byte	0x73
	.4byte	0x13d
	.uleb128 0xa
	.4byte	.LASF11
	.byte	0x5
	.byte	0x75
	.4byte	0x71
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF12
	.byte	0x5
	.byte	0x76
	.4byte	0xf0
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF13
	.byte	0x5
	.byte	0x77
	.4byte	0xf0
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x5
	.byte	0x79
	.4byte	0x106
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x14
	.byte	0x5
	.byte	0x7e
	.4byte	0x17f
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x5
	.byte	0x80
	.4byte	0x17f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x5
	.byte	0x81
	.4byte	0x184
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x5
	.byte	0x82
	.4byte	0x18f
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xb
	.4byte	0x30
	.uleb128 0x6
	.byte	0x4
	.4byte	0x18a
	.uleb128 0xb
	.4byte	0xfb
	.uleb128 0xb
	.4byte	0x13d
	.uleb128 0x2
	.4byte	.LASF24
	.byte	0x5
	.byte	0x83
	.4byte	0x148
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x8
	.byte	0x6
	.byte	0x68
	.4byte	0x1c8
	.uleb128 0xa
	.4byte	.LASF26
	.byte	0x6
	.byte	0x6a
	.4byte	0x3e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x6
	.byte	0x6b
	.4byte	0x71
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x2
	.4byte	.LASF28
	.byte	0x6
	.byte	0x6c
	.4byte	0x19f
	.uleb128 0x6
	.byte	0x4
	.4byte	0x30
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x4c
	.byte	0x1
	.byte	0x79
	.4byte	0x280
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x1
	.byte	0x7b
	.4byte	0x280
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF31
	.byte	0x1
	.byte	0x7c
	.4byte	0x280
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xa
	.4byte	.LASF32
	.byte	0x1
	.byte	0x7e
	.4byte	0x280
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x1
	.byte	0x7f
	.4byte	0x280
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xa
	.4byte	.LASF34
	.byte	0x1
	.byte	0x81
	.4byte	0x194
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x1
	.byte	0x82
	.4byte	0x194
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x1
	.byte	0x84
	.4byte	0x17f
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0xa
	.4byte	.LASF37
	.byte	0x1
	.byte	0x85
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x1
	.byte	0x86
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x1
	.byte	0x88
	.4byte	0x286
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x1
	.byte	0x89
	.4byte	0x286
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x55
	.uleb128 0xb
	.4byte	0x3e
	.uleb128 0x2
	.4byte	.LASF41
	.byte	0x1
	.byte	0x90
	.4byte	0x1d9
	.uleb128 0x2
	.4byte	.LASF42
	.byte	0x1
	.byte	0x98
	.4byte	0x2a1
	.uleb128 0x6
	.byte	0x4
	.4byte	0x28b
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x8
	.byte	0x1
	.byte	0xca
	.4byte	0x2d0
	.uleb128 0xa
	.4byte	.LASF44
	.byte	0x1
	.byte	0xcc
	.4byte	0x280
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0x1
	.byte	0xcd
	.4byte	0x296
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x2
	.4byte	.LASF46
	.byte	0x1
	.byte	0xce
	.4byte	0x2a7
	.uleb128 0xc
	.byte	0x60
	.byte	0x1
	.2byte	0x206
	.4byte	0x36c
	.uleb128 0xd
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x208
	.4byte	0x36c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x20e
	.4byte	0xfb
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x20f
	.4byte	0xfb
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xd
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x210
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0xd
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x211
	.4byte	0x1d3
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xd
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x212
	.4byte	0x372
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0xd
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x222
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xd
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x226
	.4byte	0x7c
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xd
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x22a
	.4byte	0x30
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x17f
	.uleb128 0xe
	.4byte	0x55
	.4byte	0x382
	.uleb128 0xf
	.4byte	0x30
	.byte	0x1f
	.byte	0
	.uleb128 0x10
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x22c
	.4byte	0x2db
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x117
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x3cb
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x117
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x117
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x142
	.byte	0x1
	.4byte	0x296
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x444
	.uleb128 0x12
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x142
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x142
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x142
	.4byte	0x63
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x13
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x144
	.4byte	0x2a1
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x145
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x146
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x177
	.byte	0x1
	.4byte	0x296
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x481
	.uleb128 0x12
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x177
	.4byte	0x63
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x179
	.4byte	0x2a1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x1b2
	.byte	0x1
	.4byte	0x53
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x4be
	.uleb128 0x12
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x1b2
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x1b4
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x1d0
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x4fb
	.uleb128 0x12
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x1d0
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x1d2
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x230
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x552
	.uleb128 0x12
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x230
	.4byte	0x53
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x15
	.ascii	"fil\000"
	.byte	0x1
	.2byte	0x230
	.4byte	0x552
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.ascii	"lin\000"
	.byte	0x1
	.2byte	0x230
	.4byte	0x4c
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x232
	.4byte	0x55f
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x558
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF74
	.uleb128 0x6
	.byte	0x4
	.4byte	0x565
	.uleb128 0xb
	.4byte	0x382
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x23c
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x5d4
	.uleb128 0x12
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x23c
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x23c
	.4byte	0x71
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x15
	.ascii	"fil\000"
	.byte	0x1
	.2byte	0x23c
	.4byte	0x552
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x15
	.ascii	"lin\000"
	.byte	0x1
	.2byte	0x23c
	.4byte	0x4c
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x23f
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x27f
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x64d
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x27f
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x27f
	.4byte	0x64d
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x27f
	.4byte	0x71
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x27f
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x13
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x281
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x282
	.4byte	0x1c8
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x16
	.4byte	0x652
	.uleb128 0x6
	.byte	0x4
	.4byte	0x658
	.uleb128 0x17
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x3c4
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x6d2
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x3c4
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x3c4
	.4byte	0x64d
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x3c4
	.4byte	0x6d2
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x3c4
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x3c6
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x3c7
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.4byte	0x3e
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x3ff
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x760
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x3ff
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x3ff
	.4byte	0x760
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x12
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x3ff
	.4byte	0x71
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x12
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x3ff
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x13
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x401
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x402
	.4byte	0x1c8
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x13
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x403
	.4byte	0x280
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.4byte	0x53
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x494
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x7cf
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x494
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x494
	.4byte	0x760
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x494
	.4byte	0x6d2
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x496
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x13
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x497
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x4cd
	.byte	0x1
	.4byte	0x30
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x80c
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x4cd
	.4byte	0x80c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x4cf
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.4byte	0x296
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x4db
	.byte	0x1
	.4byte	0x30
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x84e
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x4db
	.4byte	0x80c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x4dd
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x4e7
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x878
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x4e7
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x510
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x8bf
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x510
	.4byte	0x2a1
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x510
	.4byte	0x652
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x12
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x510
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x18
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x536
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x8f7
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x536
	.4byte	0x8f7
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x536
	.4byte	0x652
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.4byte	0x2a1
	.uleb128 0x18
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x544
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x925
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x544
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x584
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x961
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x584
	.4byte	0x80c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x586
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x590
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0x99e
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x590
	.4byte	0x80c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x592
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x59b
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0x9da
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x59b
	.4byte	0x80c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x59d
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x5a7
	.byte	0x1
	.4byte	0x3e
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.4byte	0xa17
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x5a7
	.4byte	0x80c
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x5a9
	.4byte	0x3e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x688
	.byte	0x1
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0xa5e
	.uleb128 0x12
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x688
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x688
	.4byte	0x280
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1a
	.ascii	"ux\000"
	.byte	0x1
	.2byte	0x68a
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x18
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x69f
	.byte	0x1
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0xa95
	.uleb128 0x12
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x69f
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1a
	.ascii	"ux\000"
	.byte	0x1
	.2byte	0x6a1
	.4byte	0x30
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x6b6
	.byte	0x1
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST23
	.4byte	0xace
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x6b6
	.4byte	0x296
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x6b6
	.4byte	0x71
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xe
	.4byte	0x2d0
	.4byte	0xade
	.uleb128 0xf
	.4byte	0x30
	.byte	0x9
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF108
	.byte	0x1
	.byte	0xd3
	.4byte	0xace
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF108
	.byte	0x1
	.byte	0xd3
	.4byte	0xace
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	xQueueRegistry
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI43
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI46
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI49
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI52
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI55
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI58
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI61
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI64
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI67
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB23
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI69
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI70
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xd4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF109:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF51:
	.ascii	"pxStack\000"
.LASF88:
	.ascii	"xJustPeeking\000"
.LASF59:
	.ascii	"xQueueGenericReset\000"
.LASF13:
	.ascii	"pxPrevious\000"
.LASF25:
	.ascii	"xTIME_OUT\000"
.LASF55:
	.ascii	"ulRunTimeCounter\000"
.LASF5:
	.ascii	"short int\000"
.LASF8:
	.ascii	"size_t\000"
.LASF10:
	.ascii	"pdTASK_HOOK_CODE\000"
.LASF44:
	.ascii	"pcQueueName\000"
.LASF97:
	.ascii	"prvCopyDataToQueue\000"
.LASF43:
	.ascii	"QUEUE_REGISTRY_ITEM\000"
.LASF72:
	.ascii	"pptr\000"
.LASF91:
	.ascii	"uxQueueMessagesWaiting\000"
.LASF48:
	.ascii	"xGenericListItem\000"
.LASF14:
	.ascii	"pvOwner\000"
.LASF47:
	.ascii	"pxTopOfStack\000"
.LASF73:
	.ascii	"tcb_ptr\000"
.LASF62:
	.ascii	"ucQueueType\000"
.LASF107:
	.ascii	"vQueueWaitForMessageRestricted\000"
.LASF90:
	.ascii	"xQueueReceiveFromISR\000"
.LASF89:
	.ascii	"pcOriginalReadPosition\000"
.LASF102:
	.ascii	"prvIsQueueFull\000"
.LASF31:
	.ascii	"pcTail\000"
.LASF71:
	.ascii	"pxMutex\000"
.LASF77:
	.ascii	"xQueueGenericSend\000"
.LASF110:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/queue.c\000"
.LASF68:
	.ascii	"xSemaphore\000"
.LASF79:
	.ascii	"xTicksToWait\000"
.LASF2:
	.ascii	"long long int\000"
.LASF40:
	.ascii	"xTxLock\000"
.LASF76:
	.ascii	"xBlockTime\000"
.LASF24:
	.ascii	"xList\000"
.LASF66:
	.ascii	"xQueueCreateMutex\000"
.LASF74:
	.ascii	"char\000"
.LASF37:
	.ascii	"uxLength\000"
.LASF1:
	.ascii	"long int\000"
.LASF20:
	.ascii	"xLIST\000"
.LASF100:
	.ascii	"xQueueIsQueueEmptyFromISR\000"
.LASF42:
	.ascii	"xQueueHandle\000"
.LASF28:
	.ascii	"xTimeOutType\000"
.LASF11:
	.ascii	"xItemValue\000"
.LASF93:
	.ascii	"uxQueueMessagesWaitingFromISR\000"
.LASF41:
	.ascii	"xQUEUE\000"
.LASF38:
	.ascii	"uxItemSize\000"
.LASF63:
	.ascii	"pxNewQueue\000"
.LASF69:
	.ascii	"pxReturn\000"
.LASF17:
	.ascii	"xLIST_ITEM\000"
.LASF67:
	.ascii	"xQueueGetMutexHolder\000"
.LASF105:
	.ascii	"xQueue\000"
.LASF60:
	.ascii	"xQueueGenericCreate\000"
.LASF19:
	.ascii	"xMiniListItem\000"
.LASF108:
	.ascii	"xQueueRegistry\000"
.LASF6:
	.ascii	"unsigned char\000"
.LASF27:
	.ascii	"xTimeOnEntering\000"
.LASF56:
	.ascii	"my_tcb\000"
.LASF26:
	.ascii	"xOverflowCount\000"
.LASF4:
	.ascii	"signed char\000"
.LASF98:
	.ascii	"prvCopyDataFromQueue\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF30:
	.ascii	"pcHead\000"
.LASF75:
	.ascii	"xQueueTakeMutexRecursive_debug\000"
.LASF92:
	.ascii	"uxReturn\000"
.LASF78:
	.ascii	"pvItemToQueue\000"
.LASF15:
	.ascii	"pvContainer\000"
.LASF35:
	.ascii	"xTasksWaitingToReceive\000"
.LASF64:
	.ascii	"xQueueSizeInBytes\000"
.LASF87:
	.ascii	"pvBuffer\000"
.LASF29:
	.ascii	"QueueDefinition\000"
.LASF99:
	.ascii	"prvUnlockQueue\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF61:
	.ascii	"uxQueueLength\000"
.LASF57:
	.ascii	"pxQueue\000"
.LASF83:
	.ascii	"xQueueGenericSendFromISR\000"
.LASF9:
	.ascii	"portTickType\000"
.LASF32:
	.ascii	"pcWriteTo\000"
.LASF12:
	.ascii	"pxNext\000"
.LASF103:
	.ascii	"xQueueIsQueueFullFromISR\000"
.LASF53:
	.ascii	"uxBasePriority\000"
.LASF33:
	.ascii	"pcReadFrom\000"
.LASF94:
	.ascii	"my_take_recursive_failed\000"
.LASF86:
	.ascii	"xQueueGenericReceive\000"
.LASF96:
	.ascii	"xPosition\000"
.LASF81:
	.ascii	"xEntryTimeSet\000"
.LASF95:
	.ascii	"vQueueDelete\000"
.LASF46:
	.ascii	"xQueueRegistryItem\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF80:
	.ascii	"xCopyPosition\000"
.LASF21:
	.ascii	"uxNumberOfItems\000"
.LASF16:
	.ascii	"xListItem\000"
.LASF45:
	.ascii	"xHandle\000"
.LASF101:
	.ascii	"prvIsQueueEmpty\000"
.LASF36:
	.ascii	"uxMessagesWaiting\000"
.LASF82:
	.ascii	"xTimeOut\000"
.LASF106:
	.ascii	"vQueueUnregisterQueue\000"
.LASF23:
	.ascii	"xListEnd\000"
.LASF49:
	.ascii	"xEventListItem\000"
.LASF39:
	.ascii	"xRxLock\000"
.LASF85:
	.ascii	"uxSavedInterruptStatus\000"
.LASF65:
	.ascii	"xReturn\000"
.LASF58:
	.ascii	"xNewQueue\000"
.LASF84:
	.ascii	"pxHigherPriorityTaskWoken\000"
.LASF104:
	.ascii	"vQueueAddToRegistry\000"
.LASF34:
	.ascii	"xTasksWaitingToSend\000"
.LASF50:
	.ascii	"uxPriority\000"
.LASF22:
	.ascii	"pxIndex\000"
.LASF54:
	.ascii	"pxTaskTag\000"
.LASF18:
	.ascii	"xMINI_LIST_ITEM\000"
.LASF70:
	.ascii	"xQueueGiveMutexRecursive\000"
.LASF52:
	.ascii	"pcTaskName\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
