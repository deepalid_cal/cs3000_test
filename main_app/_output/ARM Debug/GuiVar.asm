	.file	"GuiVar.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.global	GuiVar_AboutAntennaHoles
	.section	.bss.GuiVar_AboutAntennaHoles,"aw",%nobits
	.align	2
	.type	GuiVar_AboutAntennaHoles, %object
	.size	GuiVar_AboutAntennaHoles, 4
GuiVar_AboutAntennaHoles:
	.space	4
	.global	GuiVar_AboutAquaponicsOption
	.section	.bss.GuiVar_AboutAquaponicsOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutAquaponicsOption, %object
	.size	GuiVar_AboutAquaponicsOption, 4
GuiVar_AboutAquaponicsOption:
	.space	4
	.global	GuiVar_AboutDebug
	.section	.bss.GuiVar_AboutDebug,"aw",%nobits
	.align	2
	.type	GuiVar_AboutDebug, %object
	.size	GuiVar_AboutDebug, 4
GuiVar_AboutDebug:
	.space	4
	.global	GuiVar_AboutFLOption
	.section	.bss.GuiVar_AboutFLOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutFLOption, %object
	.size	GuiVar_AboutFLOption, 4
GuiVar_AboutFLOption:
	.space	4
	.global	GuiVar_AboutHubOption
	.section	.bss.GuiVar_AboutHubOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutHubOption, %object
	.size	GuiVar_AboutHubOption, 4
GuiVar_AboutHubOption:
	.space	4
	.global	GuiVar_AboutLOption
	.section	.bss.GuiVar_AboutLOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutLOption, %object
	.size	GuiVar_AboutLOption, 4
GuiVar_AboutLOption:
	.space	4
	.global	GuiVar_AboutModelNumber
	.section	.bss.GuiVar_AboutModelNumber,"aw",%nobits
	.align	2
	.type	GuiVar_AboutModelNumber, %object
	.size	GuiVar_AboutModelNumber, 49
GuiVar_AboutModelNumber:
	.space	49
	.global	GuiVar_AboutMOption
	.section	.bss.GuiVar_AboutMOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutMOption, %object
	.size	GuiVar_AboutMOption, 4
GuiVar_AboutMOption:
	.space	4
	.global	GuiVar_AboutMSSEOption
	.section	.bss.GuiVar_AboutMSSEOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutMSSEOption, %object
	.size	GuiVar_AboutMSSEOption, 4
GuiVar_AboutMSSEOption:
	.space	4
	.global	GuiVar_AboutNetworkID
	.section	.bss.GuiVar_AboutNetworkID,"aw",%nobits
	.align	2
	.type	GuiVar_AboutNetworkID, %object
	.size	GuiVar_AboutNetworkID, 4
GuiVar_AboutNetworkID:
	.space	4
	.global	GuiVar_AboutSerialNumber
	.section	.bss.GuiVar_AboutSerialNumber,"aw",%nobits
	.align	2
	.type	GuiVar_AboutSerialNumber, %object
	.size	GuiVar_AboutSerialNumber, 4
GuiVar_AboutSerialNumber:
	.space	4
	.global	GuiVar_AboutSSEDOption
	.section	.bss.GuiVar_AboutSSEDOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutSSEDOption, %object
	.size	GuiVar_AboutSSEDOption, 4
GuiVar_AboutSSEDOption:
	.space	4
	.global	GuiVar_AboutSSEOption
	.section	.bss.GuiVar_AboutSSEOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutSSEOption, %object
	.size	GuiVar_AboutSSEOption, 4
GuiVar_AboutSSEOption:
	.space	4
	.global	GuiVar_AboutTP2WireTerminal
	.section	.bss.GuiVar_AboutTP2WireTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTP2WireTerminal, %object
	.size	GuiVar_AboutTP2WireTerminal, 4
GuiVar_AboutTP2WireTerminal:
	.space	4
	.global	GuiVar_AboutTPDashLCard
	.section	.bss.GuiVar_AboutTPDashLCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashLCard, %object
	.size	GuiVar_AboutTPDashLCard, 4
GuiVar_AboutTPDashLCard:
	.space	4
	.global	GuiVar_AboutTPDashLTerminal
	.section	.bss.GuiVar_AboutTPDashLTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashLTerminal, %object
	.size	GuiVar_AboutTPDashLTerminal, 4
GuiVar_AboutTPDashLTerminal:
	.space	4
	.global	GuiVar_AboutTPDashMCard
	.section	.bss.GuiVar_AboutTPDashMCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashMCard, %object
	.size	GuiVar_AboutTPDashMCard, 4
GuiVar_AboutTPDashMCard:
	.space	4
	.global	GuiVar_AboutTPDashMTerminal
	.section	.bss.GuiVar_AboutTPDashMTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashMTerminal, %object
	.size	GuiVar_AboutTPDashMTerminal, 4
GuiVar_AboutTPDashMTerminal:
	.space	4
	.global	GuiVar_AboutTPDashWCard
	.section	.bss.GuiVar_AboutTPDashWCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashWCard, %object
	.size	GuiVar_AboutTPDashWCard, 4
GuiVar_AboutTPDashWCard:
	.space	4
	.global	GuiVar_AboutTPDashWTerminal
	.section	.bss.GuiVar_AboutTPDashWTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashWTerminal, %object
	.size	GuiVar_AboutTPDashWTerminal, 4
GuiVar_AboutTPDashWTerminal:
	.space	4
	.global	GuiVar_AboutTPFuseCard
	.section	.bss.GuiVar_AboutTPFuseCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPFuseCard, %object
	.size	GuiVar_AboutTPFuseCard, 4
GuiVar_AboutTPFuseCard:
	.space	4
	.global	GuiVar_AboutTPMicroCard
	.section	.bss.GuiVar_AboutTPMicroCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPMicroCard, %object
	.size	GuiVar_AboutTPMicroCard, 4
GuiVar_AboutTPMicroCard:
	.space	4
	.global	GuiVar_AboutTPPOCCard
	.section	.bss.GuiVar_AboutTPPOCCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPPOCCard, %object
	.size	GuiVar_AboutTPPOCCard, 4
GuiVar_AboutTPPOCCard:
	.space	4
	.global	GuiVar_AboutTPPOCTerminal
	.section	.bss.GuiVar_AboutTPPOCTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPPOCTerminal, %object
	.size	GuiVar_AboutTPPOCTerminal, 4
GuiVar_AboutTPPOCTerminal:
	.space	4
	.global	GuiVar_AboutTPSta01_08Card
	.section	.bss.GuiVar_AboutTPSta01_08Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta01_08Card, %object
	.size	GuiVar_AboutTPSta01_08Card, 4
GuiVar_AboutTPSta01_08Card:
	.space	4
	.global	GuiVar_AboutTPSta01_08Terminal
	.section	.bss.GuiVar_AboutTPSta01_08Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta01_08Terminal, %object
	.size	GuiVar_AboutTPSta01_08Terminal, 4
GuiVar_AboutTPSta01_08Terminal:
	.space	4
	.global	GuiVar_AboutTPSta09_16Card
	.section	.bss.GuiVar_AboutTPSta09_16Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta09_16Card, %object
	.size	GuiVar_AboutTPSta09_16Card, 4
GuiVar_AboutTPSta09_16Card:
	.space	4
	.global	GuiVar_AboutTPSta09_16Terminal
	.section	.bss.GuiVar_AboutTPSta09_16Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta09_16Terminal, %object
	.size	GuiVar_AboutTPSta09_16Terminal, 4
GuiVar_AboutTPSta09_16Terminal:
	.space	4
	.global	GuiVar_AboutTPSta17_24Card
	.section	.bss.GuiVar_AboutTPSta17_24Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta17_24Card, %object
	.size	GuiVar_AboutTPSta17_24Card, 4
GuiVar_AboutTPSta17_24Card:
	.space	4
	.global	GuiVar_AboutTPSta17_24Terminal
	.section	.bss.GuiVar_AboutTPSta17_24Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta17_24Terminal, %object
	.size	GuiVar_AboutTPSta17_24Terminal, 4
GuiVar_AboutTPSta17_24Terminal:
	.space	4
	.global	GuiVar_AboutTPSta25_32Card
	.section	.bss.GuiVar_AboutTPSta25_32Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta25_32Card, %object
	.size	GuiVar_AboutTPSta25_32Card, 4
GuiVar_AboutTPSta25_32Card:
	.space	4
	.global	GuiVar_AboutTPSta25_32Terminal
	.section	.bss.GuiVar_AboutTPSta25_32Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta25_32Terminal, %object
	.size	GuiVar_AboutTPSta25_32Terminal, 4
GuiVar_AboutTPSta25_32Terminal:
	.space	4
	.global	GuiVar_AboutTPSta33_40Card
	.section	.bss.GuiVar_AboutTPSta33_40Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta33_40Card, %object
	.size	GuiVar_AboutTPSta33_40Card, 4
GuiVar_AboutTPSta33_40Card:
	.space	4
	.global	GuiVar_AboutTPSta33_40Terminal
	.section	.bss.GuiVar_AboutTPSta33_40Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta33_40Terminal, %object
	.size	GuiVar_AboutTPSta33_40Terminal, 4
GuiVar_AboutTPSta33_40Terminal:
	.space	4
	.global	GuiVar_AboutTPSta41_48Card
	.section	.bss.GuiVar_AboutTPSta41_48Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta41_48Card, %object
	.size	GuiVar_AboutTPSta41_48Card, 4
GuiVar_AboutTPSta41_48Card:
	.space	4
	.global	GuiVar_AboutTPSta41_48Terminal
	.section	.bss.GuiVar_AboutTPSta41_48Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta41_48Terminal, %object
	.size	GuiVar_AboutTPSta41_48Terminal, 4
GuiVar_AboutTPSta41_48Terminal:
	.space	4
	.global	GuiVar_AboutVersion
	.section	.bss.GuiVar_AboutVersion,"aw",%nobits
	.align	2
	.type	GuiVar_AboutVersion, %object
	.size	GuiVar_AboutVersion, 49
GuiVar_AboutVersion:
	.space	49
	.global	GuiVar_AboutWMOption
	.section	.bss.GuiVar_AboutWMOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutWMOption, %object
	.size	GuiVar_AboutWMOption, 4
GuiVar_AboutWMOption:
	.space	4
	.global	GuiVar_AboutWOption
	.section	.bss.GuiVar_AboutWOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutWOption, %object
	.size	GuiVar_AboutWOption, 4
GuiVar_AboutWOption:
	.space	4
	.global	GuiVar_AccessControlEnabled
	.section	.bss.GuiVar_AccessControlEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_AccessControlEnabled, %object
	.size	GuiVar_AccessControlEnabled, 4
GuiVar_AccessControlEnabled:
	.space	4
	.global	GuiVar_AccessControlLoggedIn
	.section	.bss.GuiVar_AccessControlLoggedIn,"aw",%nobits
	.align	2
	.type	GuiVar_AccessControlLoggedIn, %object
	.size	GuiVar_AccessControlLoggedIn, 4
GuiVar_AccessControlLoggedIn:
	.space	4
	.global	GuiVar_AccessControlPwd
	.section	.bss.GuiVar_AccessControlPwd,"aw",%nobits
	.align	2
	.type	GuiVar_AccessControlPwd, %object
	.size	GuiVar_AccessControlPwd, 49
GuiVar_AccessControlPwd:
	.space	49
	.global	GuiVar_AccessControlUser
	.section	.bss.GuiVar_AccessControlUser,"aw",%nobits
	.align	2
	.type	GuiVar_AccessControlUser, %object
	.size	GuiVar_AccessControlUser, 49
GuiVar_AccessControlUser:
	.space	49
	.global	GuiVar_AccessControlUserRole
	.section	.bss.GuiVar_AccessControlUserRole,"aw",%nobits
	.align	2
	.type	GuiVar_AccessControlUserRole, %object
	.size	GuiVar_AccessControlUserRole, 49
GuiVar_AccessControlUserRole:
	.space	49
	.global	GuiVar_ActivationCode
	.section	.bss.GuiVar_ActivationCode,"aw",%nobits
	.align	2
	.type	GuiVar_ActivationCode, %object
	.size	GuiVar_ActivationCode, 21
GuiVar_ActivationCode:
	.space	21
	.global	GuiVar_AlertDate
	.section	.bss.GuiVar_AlertDate,"aw",%nobits
	.align	2
	.type	GuiVar_AlertDate, %object
	.size	GuiVar_AlertDate, 6
GuiVar_AlertDate:
	.space	6
	.global	GuiVar_AlertString
	.section	.bss.GuiVar_AlertString,"aw",%nobits
	.align	2
	.type	GuiVar_AlertString, %object
	.size	GuiVar_AlertString, 257
GuiVar_AlertString:
	.space	257
	.global	GuiVar_AlertTime
	.section	.bss.GuiVar_AlertTime,"aw",%nobits
	.align	2
	.type	GuiVar_AlertTime, %object
	.size	GuiVar_AlertTime, 10
GuiVar_AlertTime:
	.space	10
	.global	GuiVar_Backlight
	.section	.bss.GuiVar_Backlight,"aw",%nobits
	.align	2
	.type	GuiVar_Backlight, %object
	.size	GuiVar_Backlight, 4
GuiVar_Backlight:
	.space	4
	.global	GuiVar_BacklightSliderPos
	.section	.bss.GuiVar_BacklightSliderPos,"aw",%nobits
	.align	2
	.type	GuiVar_BacklightSliderPos, %object
	.size	GuiVar_BacklightSliderPos, 4
GuiVar_BacklightSliderPos:
	.space	4
	.global	GuiVar_BudgetAllocation
	.section	.bss.GuiVar_BudgetAllocation,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetAllocation, %object
	.size	GuiVar_BudgetAllocation, 4
GuiVar_BudgetAllocation:
	.space	4
	.global	GuiVar_BudgetAllocationSYS
	.section	.bss.GuiVar_BudgetAllocationSYS,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetAllocationSYS, %object
	.size	GuiVar_BudgetAllocationSYS, 4
GuiVar_BudgetAllocationSYS:
	.space	4
	.global	GuiVar_BudgetAnnualValue
	.section	.bss.GuiVar_BudgetAnnualValue,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetAnnualValue, %object
	.size	GuiVar_BudgetAnnualValue, 4
GuiVar_BudgetAnnualValue:
	.space	4
	.global	GuiVar_BudgetDaysLeftInPeriod
	.section	.bss.GuiVar_BudgetDaysLeftInPeriod,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetDaysLeftInPeriod, %object
	.size	GuiVar_BudgetDaysLeftInPeriod, 4
GuiVar_BudgetDaysLeftInPeriod:
	.space	4
	.global	GuiVar_BudgetEntryOptionIdx
	.section	.bss.GuiVar_BudgetEntryOptionIdx,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetEntryOptionIdx, %object
	.size	GuiVar_BudgetEntryOptionIdx, 4
GuiVar_BudgetEntryOptionIdx:
	.space	4
	.global	GuiVar_BudgetEst
	.section	.bss.GuiVar_BudgetEst,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetEst, %object
	.size	GuiVar_BudgetEst, 4
GuiVar_BudgetEst:
	.space	4
	.global	GuiVar_BudgetETPercentageUsed
	.section	.bss.GuiVar_BudgetETPercentageUsed,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetETPercentageUsed, %object
	.size	GuiVar_BudgetETPercentageUsed, 4
GuiVar_BudgetETPercentageUsed:
	.space	4
	.global	GuiVar_BudgetExpectedUseThisPeriod
	.section	.bss.GuiVar_BudgetExpectedUseThisPeriod,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetExpectedUseThisPeriod, %object
	.size	GuiVar_BudgetExpectedUseThisPeriod, 4
GuiVar_BudgetExpectedUseThisPeriod:
	.space	4
	.global	GuiVar_BudgetFlowTypeIndex
	.section	.bss.GuiVar_BudgetFlowTypeIndex,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetFlowTypeIndex, %object
	.size	GuiVar_BudgetFlowTypeIndex, 4
GuiVar_BudgetFlowTypeIndex:
	.space	4
	.global	GuiVar_BudgetFlowTypeIrrigation
	.section	.bss.GuiVar_BudgetFlowTypeIrrigation,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetFlowTypeIrrigation, %object
	.size	GuiVar_BudgetFlowTypeIrrigation, 4
GuiVar_BudgetFlowTypeIrrigation:
	.space	4
	.global	GuiVar_BudgetFlowTypeMVOR
	.section	.bss.GuiVar_BudgetFlowTypeMVOR,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetFlowTypeMVOR, %object
	.size	GuiVar_BudgetFlowTypeMVOR, 4
GuiVar_BudgetFlowTypeMVOR:
	.space	4
	.global	GuiVar_BudgetFlowTypeNonController
	.section	.bss.GuiVar_BudgetFlowTypeNonController,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetFlowTypeNonController, %object
	.size	GuiVar_BudgetFlowTypeNonController, 4
GuiVar_BudgetFlowTypeNonController:
	.space	4
	.global	GuiVar_BudgetForThisPeriod
	.section	.bss.GuiVar_BudgetForThisPeriod,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetForThisPeriod, %object
	.size	GuiVar_BudgetForThisPeriod, 4
GuiVar_BudgetForThisPeriod:
	.space	4
	.global	GuiVar_BudgetInUse
	.section	.bss.GuiVar_BudgetInUse,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetInUse, %object
	.size	GuiVar_BudgetInUse, 4
GuiVar_BudgetInUse:
	.space	4
	.global	GuiVar_BudgetMainlineName
	.section	.bss.GuiVar_BudgetMainlineName,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetMainlineName, %object
	.size	GuiVar_BudgetMainlineName, 49
GuiVar_BudgetMainlineName:
	.space	49
	.global	GuiVar_BudgetModeIdx
	.section	.bss.GuiVar_BudgetModeIdx,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetModeIdx, %object
	.size	GuiVar_BudgetModeIdx, 4
GuiVar_BudgetModeIdx:
	.space	4
	.global	GuiVar_BudgetPeriod_0
	.section	.bss.GuiVar_BudgetPeriod_0,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetPeriod_0, %object
	.size	GuiVar_BudgetPeriod_0, 17
GuiVar_BudgetPeriod_0:
	.space	17
	.global	GuiVar_BudgetPeriod_1
	.section	.bss.GuiVar_BudgetPeriod_1,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetPeriod_1, %object
	.size	GuiVar_BudgetPeriod_1, 17
GuiVar_BudgetPeriod_1:
	.space	17
	.global	GuiVar_BudgetPeriodEnd
	.section	.bss.GuiVar_BudgetPeriodEnd,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetPeriodEnd, %object
	.size	GuiVar_BudgetPeriodEnd, 41
GuiVar_BudgetPeriodEnd:
	.space	41
	.global	GuiVar_BudgetPeriodIdx
	.section	.bss.GuiVar_BudgetPeriodIdx,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetPeriodIdx, %object
	.size	GuiVar_BudgetPeriodIdx, 4
GuiVar_BudgetPeriodIdx:
	.space	4
	.global	GuiVar_BudgetPeriodsPerYearIdx
	.section	.bss.GuiVar_BudgetPeriodsPerYearIdx,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetPeriodsPerYearIdx, %object
	.size	GuiVar_BudgetPeriodsPerYearIdx, 4
GuiVar_BudgetPeriodsPerYearIdx:
	.space	4
	.global	GuiVar_BudgetPeriodStart
	.section	.bss.GuiVar_BudgetPeriodStart,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetPeriodStart, %object
	.size	GuiVar_BudgetPeriodStart, 41
GuiVar_BudgetPeriodStart:
	.space	41
	.global	GuiVar_BudgetReductionLimit_0
	.section	.bss.GuiVar_BudgetReductionLimit_0,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_0, %object
	.size	GuiVar_BudgetReductionLimit_0, 4
GuiVar_BudgetReductionLimit_0:
	.space	4
	.global	GuiVar_BudgetReductionLimit_1
	.section	.bss.GuiVar_BudgetReductionLimit_1,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_1, %object
	.size	GuiVar_BudgetReductionLimit_1, 4
GuiVar_BudgetReductionLimit_1:
	.space	4
	.global	GuiVar_BudgetReductionLimit_2
	.section	.bss.GuiVar_BudgetReductionLimit_2,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_2, %object
	.size	GuiVar_BudgetReductionLimit_2, 4
GuiVar_BudgetReductionLimit_2:
	.space	4
	.global	GuiVar_BudgetReductionLimit_3
	.section	.bss.GuiVar_BudgetReductionLimit_3,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_3, %object
	.size	GuiVar_BudgetReductionLimit_3, 4
GuiVar_BudgetReductionLimit_3:
	.space	4
	.global	GuiVar_BudgetReductionLimit_4
	.section	.bss.GuiVar_BudgetReductionLimit_4,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_4, %object
	.size	GuiVar_BudgetReductionLimit_4, 4
GuiVar_BudgetReductionLimit_4:
	.space	4
	.global	GuiVar_BudgetReductionLimit_5
	.section	.bss.GuiVar_BudgetReductionLimit_5,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_5, %object
	.size	GuiVar_BudgetReductionLimit_5, 4
GuiVar_BudgetReductionLimit_5:
	.space	4
	.global	GuiVar_BudgetReductionLimit_6
	.section	.bss.GuiVar_BudgetReductionLimit_6,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_6, %object
	.size	GuiVar_BudgetReductionLimit_6, 4
GuiVar_BudgetReductionLimit_6:
	.space	4
	.global	GuiVar_BudgetReductionLimit_7
	.section	.bss.GuiVar_BudgetReductionLimit_7,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_7, %object
	.size	GuiVar_BudgetReductionLimit_7, 4
GuiVar_BudgetReductionLimit_7:
	.space	4
	.global	GuiVar_BudgetReductionLimit_8
	.section	.bss.GuiVar_BudgetReductionLimit_8,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_8, %object
	.size	GuiVar_BudgetReductionLimit_8, 4
GuiVar_BudgetReductionLimit_8:
	.space	4
	.global	GuiVar_BudgetReductionLimit_9
	.section	.bss.GuiVar_BudgetReductionLimit_9,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_9, %object
	.size	GuiVar_BudgetReductionLimit_9, 4
GuiVar_BudgetReductionLimit_9:
	.space	4
	.global	GuiVar_BudgetReportText
	.section	.bss.GuiVar_BudgetReportText,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReportText, %object
	.size	GuiVar_BudgetReportText, 225
GuiVar_BudgetReportText:
	.space	225
	.global	GuiVar_BudgetsInEffect
	.section	.bss.GuiVar_BudgetsInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetsInEffect, %object
	.size	GuiVar_BudgetsInEffect, 4
GuiVar_BudgetsInEffect:
	.space	4
	.global	GuiVar_BudgetSysHasPOC
	.section	.bss.GuiVar_BudgetSysHasPOC,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetSysHasPOC, %object
	.size	GuiVar_BudgetSysHasPOC, 4
GuiVar_BudgetSysHasPOC:
	.space	4
	.global	GuiVar_BudgetUnitGallon
	.section	.bss.GuiVar_BudgetUnitGallon,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUnitGallon, %object
	.size	GuiVar_BudgetUnitGallon, 4
GuiVar_BudgetUnitGallon:
	.space	4
	.global	GuiVar_BudgetUnitHCF
	.section	.bss.GuiVar_BudgetUnitHCF,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUnitHCF, %object
	.size	GuiVar_BudgetUnitHCF, 4
GuiVar_BudgetUnitHCF:
	.space	4
	.global	GuiVar_BudgetUsed
	.section	.bss.GuiVar_BudgetUsed,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUsed, %object
	.size	GuiVar_BudgetUsed, 4
GuiVar_BudgetUsed:
	.space	4
	.global	GuiVar_BudgetUsedSYS
	.section	.bss.GuiVar_BudgetUsedSYS,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUsedSYS, %object
	.size	GuiVar_BudgetUsedSYS, 4
GuiVar_BudgetUsedSYS:
	.space	4
	.global	GuiVar_BudgetUseThisPeriod
	.section	.bss.GuiVar_BudgetUseThisPeriod,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUseThisPeriod, %object
	.size	GuiVar_BudgetUseThisPeriod, 4
GuiVar_BudgetUseThisPeriod:
	.space	4
	.global	GuiVar_BudgetZeroWarning
	.section	.bss.GuiVar_BudgetZeroWarning,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetZeroWarning, %object
	.size	GuiVar_BudgetZeroWarning, 4
GuiVar_BudgetZeroWarning:
	.space	4
	.global	GuiVar_ChainStatsCommMngrMode
	.section	.bss.GuiVar_ChainStatsCommMngrMode,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsCommMngrMode, %object
	.size	GuiVar_ChainStatsCommMngrMode, 4
GuiVar_ChainStatsCommMngrMode:
	.space	4
	.global	GuiVar_ChainStatsCommMngrState
	.section	.bss.GuiVar_ChainStatsCommMngrState,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsCommMngrState, %object
	.size	GuiVar_ChainStatsCommMngrState, 4
GuiVar_ChainStatsCommMngrState:
	.space	4
	.global	GuiVar_ChainStatsControllersInChain
	.section	.bss.GuiVar_ChainStatsControllersInChain,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsControllersInChain, %object
	.size	GuiVar_ChainStatsControllersInChain, 49
GuiVar_ChainStatsControllersInChain:
	.space	49
	.global	GuiVar_ChainStatsErrorsA
	.section	.bss.GuiVar_ChainStatsErrorsA,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsA, %object
	.size	GuiVar_ChainStatsErrorsA, 4
GuiVar_ChainStatsErrorsA:
	.space	4
	.global	GuiVar_ChainStatsErrorsB
	.section	.bss.GuiVar_ChainStatsErrorsB,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsB, %object
	.size	GuiVar_ChainStatsErrorsB, 4
GuiVar_ChainStatsErrorsB:
	.space	4
	.global	GuiVar_ChainStatsErrorsC
	.section	.bss.GuiVar_ChainStatsErrorsC,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsC, %object
	.size	GuiVar_ChainStatsErrorsC, 4
GuiVar_ChainStatsErrorsC:
	.space	4
	.global	GuiVar_ChainStatsErrorsD
	.section	.bss.GuiVar_ChainStatsErrorsD,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsD, %object
	.size	GuiVar_ChainStatsErrorsD, 4
GuiVar_ChainStatsErrorsD:
	.space	4
	.global	GuiVar_ChainStatsErrorsE
	.section	.bss.GuiVar_ChainStatsErrorsE,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsE, %object
	.size	GuiVar_ChainStatsErrorsE, 4
GuiVar_ChainStatsErrorsE:
	.space	4
	.global	GuiVar_ChainStatsErrorsF
	.section	.bss.GuiVar_ChainStatsErrorsF,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsF, %object
	.size	GuiVar_ChainStatsErrorsF, 4
GuiVar_ChainStatsErrorsF:
	.space	4
	.global	GuiVar_ChainStatsErrorsG
	.section	.bss.GuiVar_ChainStatsErrorsG,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsG, %object
	.size	GuiVar_ChainStatsErrorsG, 4
GuiVar_ChainStatsErrorsG:
	.space	4
	.global	GuiVar_ChainStatsErrorsH
	.section	.bss.GuiVar_ChainStatsErrorsH,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsH, %object
	.size	GuiVar_ChainStatsErrorsH, 4
GuiVar_ChainStatsErrorsH:
	.space	4
	.global	GuiVar_ChainStatsErrorsI
	.section	.bss.GuiVar_ChainStatsErrorsI,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsI, %object
	.size	GuiVar_ChainStatsErrorsI, 4
GuiVar_ChainStatsErrorsI:
	.space	4
	.global	GuiVar_ChainStatsErrorsJ
	.section	.bss.GuiVar_ChainStatsErrorsJ,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsJ, %object
	.size	GuiVar_ChainStatsErrorsJ, 4
GuiVar_ChainStatsErrorsJ:
	.space	4
	.global	GuiVar_ChainStatsErrorsK
	.section	.bss.GuiVar_ChainStatsErrorsK,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsK, %object
	.size	GuiVar_ChainStatsErrorsK, 4
GuiVar_ChainStatsErrorsK:
	.space	4
	.global	GuiVar_ChainStatsErrorsL
	.section	.bss.GuiVar_ChainStatsErrorsL,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsL, %object
	.size	GuiVar_ChainStatsErrorsL, 4
GuiVar_ChainStatsErrorsL:
	.space	4
	.global	GuiVar_ChainStatsFOALChainRRcvd
	.section	.bss.GuiVar_ChainStatsFOALChainRRcvd,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsFOALChainRRcvd, %object
	.size	GuiVar_ChainStatsFOALChainRRcvd, 4
GuiVar_ChainStatsFOALChainRRcvd:
	.space	4
	.global	GuiVar_ChainStatsFOALChainTGen
	.section	.bss.GuiVar_ChainStatsFOALChainTGen,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsFOALChainTGen, %object
	.size	GuiVar_ChainStatsFOALChainTGen, 4
GuiVar_ChainStatsFOALChainTGen:
	.space	4
	.global	GuiVar_ChainStatsFOALIrriRRcvd
	.section	.bss.GuiVar_ChainStatsFOALIrriRRcvd,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsFOALIrriRRcvd, %object
	.size	GuiVar_ChainStatsFOALIrriRRcvd, 4
GuiVar_ChainStatsFOALIrriRRcvd:
	.space	4
	.global	GuiVar_ChainStatsFOALIrriTGen
	.section	.bss.GuiVar_ChainStatsFOALIrriTGen,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsFOALIrriTGen, %object
	.size	GuiVar_ChainStatsFOALIrriTGen, 4
GuiVar_ChainStatsFOALIrriTGen:
	.space	4
	.global	GuiVar_ChainStatsInForced
	.section	.bss.GuiVar_ChainStatsInForced,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInForced, %object
	.size	GuiVar_ChainStatsInForced, 4
GuiVar_ChainStatsInForced:
	.space	4
	.global	GuiVar_ChainStatsInUseA
	.section	.bss.GuiVar_ChainStatsInUseA,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseA, %object
	.size	GuiVar_ChainStatsInUseA, 4
GuiVar_ChainStatsInUseA:
	.space	4
	.global	GuiVar_ChainStatsInUseB
	.section	.bss.GuiVar_ChainStatsInUseB,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseB, %object
	.size	GuiVar_ChainStatsInUseB, 4
GuiVar_ChainStatsInUseB:
	.space	4
	.global	GuiVar_ChainStatsInUseC
	.section	.bss.GuiVar_ChainStatsInUseC,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseC, %object
	.size	GuiVar_ChainStatsInUseC, 4
GuiVar_ChainStatsInUseC:
	.space	4
	.global	GuiVar_ChainStatsInUseD
	.section	.bss.GuiVar_ChainStatsInUseD,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseD, %object
	.size	GuiVar_ChainStatsInUseD, 4
GuiVar_ChainStatsInUseD:
	.space	4
	.global	GuiVar_ChainStatsInUseE
	.section	.bss.GuiVar_ChainStatsInUseE,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseE, %object
	.size	GuiVar_ChainStatsInUseE, 4
GuiVar_ChainStatsInUseE:
	.space	4
	.global	GuiVar_ChainStatsInUseF
	.section	.bss.GuiVar_ChainStatsInUseF,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseF, %object
	.size	GuiVar_ChainStatsInUseF, 4
GuiVar_ChainStatsInUseF:
	.space	4
	.global	GuiVar_ChainStatsInUseG
	.section	.bss.GuiVar_ChainStatsInUseG,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseG, %object
	.size	GuiVar_ChainStatsInUseG, 4
GuiVar_ChainStatsInUseG:
	.space	4
	.global	GuiVar_ChainStatsInUseH
	.section	.bss.GuiVar_ChainStatsInUseH,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseH, %object
	.size	GuiVar_ChainStatsInUseH, 4
GuiVar_ChainStatsInUseH:
	.space	4
	.global	GuiVar_ChainStatsInUseI
	.section	.bss.GuiVar_ChainStatsInUseI,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseI, %object
	.size	GuiVar_ChainStatsInUseI, 4
GuiVar_ChainStatsInUseI:
	.space	4
	.global	GuiVar_ChainStatsInUseJ
	.section	.bss.GuiVar_ChainStatsInUseJ,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseJ, %object
	.size	GuiVar_ChainStatsInUseJ, 4
GuiVar_ChainStatsInUseJ:
	.space	4
	.global	GuiVar_ChainStatsInUseK
	.section	.bss.GuiVar_ChainStatsInUseK,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseK, %object
	.size	GuiVar_ChainStatsInUseK, 4
GuiVar_ChainStatsInUseK:
	.space	4
	.global	GuiVar_ChainStatsInUseL
	.section	.bss.GuiVar_ChainStatsInUseL,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseL, %object
	.size	GuiVar_ChainStatsInUseL, 4
GuiVar_ChainStatsInUseL:
	.space	4
	.global	GuiVar_ChainStatsIRRIChainRGen
	.section	.bss.GuiVar_ChainStatsIRRIChainRGen,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsIRRIChainRGen, %object
	.size	GuiVar_ChainStatsIRRIChainRGen, 4
GuiVar_ChainStatsIRRIChainRGen:
	.space	4
	.global	GuiVar_ChainStatsIRRIChainTRcvd
	.section	.bss.GuiVar_ChainStatsIRRIChainTRcvd,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsIRRIChainTRcvd, %object
	.size	GuiVar_ChainStatsIRRIChainTRcvd, 4
GuiVar_ChainStatsIRRIChainTRcvd:
	.space	4
	.global	GuiVar_ChainStatsIRRIIrriRGen
	.section	.bss.GuiVar_ChainStatsIRRIIrriRGen,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsIRRIIrriRGen, %object
	.size	GuiVar_ChainStatsIRRIIrriRGen, 4
GuiVar_ChainStatsIRRIIrriRGen:
	.space	4
	.global	GuiVar_ChainStatsIRRIIrriTRcvd
	.section	.bss.GuiVar_ChainStatsIRRIIrriTRcvd,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsIRRIIrriTRcvd, %object
	.size	GuiVar_ChainStatsIRRIIrriTRcvd, 4
GuiVar_ChainStatsIRRIIrriTRcvd:
	.space	4
	.global	GuiVar_ChainStatsNextContact
	.section	.bss.GuiVar_ChainStatsNextContact,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsNextContact, %object
	.size	GuiVar_ChainStatsNextContact, 4
GuiVar_ChainStatsNextContact:
	.space	4
	.global	GuiVar_ChainStatsNextContactSerNum
	.section	.bss.GuiVar_ChainStatsNextContactSerNum,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsNextContactSerNum, %object
	.size	GuiVar_ChainStatsNextContactSerNum, 4
GuiVar_ChainStatsNextContactSerNum:
	.space	4
	.global	GuiVar_ChainStatsPresentlyMakingTokens
	.section	.bss.GuiVar_ChainStatsPresentlyMakingTokens,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsPresentlyMakingTokens, %object
	.size	GuiVar_ChainStatsPresentlyMakingTokens, 4
GuiVar_ChainStatsPresentlyMakingTokens:
	.space	4
	.global	GuiVar_ChainStatsScans
	.section	.bss.GuiVar_ChainStatsScans,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsScans, %object
	.size	GuiVar_ChainStatsScans, 4
GuiVar_ChainStatsScans:
	.space	4
	.global	GuiVar_CodeDownloadPercentComplete
	.section	.bss.GuiVar_CodeDownloadPercentComplete,"aw",%nobits
	.align	2
	.type	GuiVar_CodeDownloadPercentComplete, %object
	.size	GuiVar_CodeDownloadPercentComplete, 4
GuiVar_CodeDownloadPercentComplete:
	.space	4
	.global	GuiVar_CodeDownloadProgressBar
	.section	.bss.GuiVar_CodeDownloadProgressBar,"aw",%nobits
	.align	2
	.type	GuiVar_CodeDownloadProgressBar, %object
	.size	GuiVar_CodeDownloadProgressBar, 4
GuiVar_CodeDownloadProgressBar:
	.space	4
	.global	GuiVar_CodeDownloadState
	.section	.bss.GuiVar_CodeDownloadState,"aw",%nobits
	.align	2
	.type	GuiVar_CodeDownloadState, %object
	.size	GuiVar_CodeDownloadState, 4
GuiVar_CodeDownloadState:
	.space	4
	.global	GuiVar_CodeDownloadUpdatingTPMicro
	.section	.bss.GuiVar_CodeDownloadUpdatingTPMicro,"aw",%nobits
	.align	2
	.type	GuiVar_CodeDownloadUpdatingTPMicro, %object
	.size	GuiVar_CodeDownloadUpdatingTPMicro, 4
GuiVar_CodeDownloadUpdatingTPMicro:
	.space	4
	.global	GuiVar_ComboBox_X1
	.section	.bss.GuiVar_ComboBox_X1,"aw",%nobits
	.align	2
	.type	GuiVar_ComboBox_X1, %object
	.size	GuiVar_ComboBox_X1, 4
GuiVar_ComboBox_X1:
	.space	4
	.global	GuiVar_ComboBox_Y1
	.section	.bss.GuiVar_ComboBox_Y1,"aw",%nobits
	.align	2
	.type	GuiVar_ComboBox_Y1, %object
	.size	GuiVar_ComboBox_Y1, 4
GuiVar_ComboBox_Y1:
	.space	4
	.global	GuiVar_ComboBoxItemIndex
	.section	.bss.GuiVar_ComboBoxItemIndex,"aw",%nobits
	.align	2
	.type	GuiVar_ComboBoxItemIndex, %object
	.size	GuiVar_ComboBoxItemIndex, 4
GuiVar_ComboBoxItemIndex:
	.space	4
	.global	GuiVar_ComboBoxItemString
	.section	.bss.GuiVar_ComboBoxItemString,"aw",%nobits
	.align	2
	.type	GuiVar_ComboBoxItemString, %object
	.size	GuiVar_ComboBoxItemString, 49
GuiVar_ComboBoxItemString:
	.space	49
	.global	GuiVar_CommOptionCloudCommTestAvailable
	.section	.bss.GuiVar_CommOptionCloudCommTestAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionCloudCommTestAvailable, %object
	.size	GuiVar_CommOptionCloudCommTestAvailable, 4
GuiVar_CommOptionCloudCommTestAvailable:
	.space	4
	.global	GuiVar_CommOptionDeviceExchangeResult
	.section	.bss.GuiVar_CommOptionDeviceExchangeResult,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionDeviceExchangeResult, %object
	.size	GuiVar_CommOptionDeviceExchangeResult, 4
GuiVar_CommOptionDeviceExchangeResult:
	.space	4
	.global	GuiVar_CommOptionInfoText
	.section	.bss.GuiVar_CommOptionInfoText,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionInfoText, %object
	.size	GuiVar_CommOptionInfoText, 49
GuiVar_CommOptionInfoText:
	.space	49
	.global	GuiVar_CommOptionIsConnected
	.section	.bss.GuiVar_CommOptionIsConnected,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionIsConnected, %object
	.size	GuiVar_CommOptionIsConnected, 4
GuiVar_CommOptionIsConnected:
	.space	4
	.global	GuiVar_CommOptionPortAIndex
	.section	.bss.GuiVar_CommOptionPortAIndex,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionPortAIndex, %object
	.size	GuiVar_CommOptionPortAIndex, 4
GuiVar_CommOptionPortAIndex:
	.space	4
	.global	GuiVar_CommOptionPortAInstalled
	.section	.bss.GuiVar_CommOptionPortAInstalled,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionPortAInstalled, %object
	.size	GuiVar_CommOptionPortAInstalled, 4
GuiVar_CommOptionPortAInstalled:
	.space	4
	.global	GuiVar_CommOptionPortBIndex
	.section	.bss.GuiVar_CommOptionPortBIndex,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionPortBIndex, %object
	.size	GuiVar_CommOptionPortBIndex, 4
GuiVar_CommOptionPortBIndex:
	.space	4
	.global	GuiVar_CommOptionPortBInstalled
	.section	.bss.GuiVar_CommOptionPortBInstalled,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionPortBInstalled, %object
	.size	GuiVar_CommOptionPortBInstalled, 4
GuiVar_CommOptionPortBInstalled:
	.space	4
	.global	GuiVar_CommOptionRadioTestAvailable
	.section	.bss.GuiVar_CommOptionRadioTestAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionRadioTestAvailable, %object
	.size	GuiVar_CommOptionRadioTestAvailable, 4
GuiVar_CommOptionRadioTestAvailable:
	.space	4
	.global	GuiVar_CommTestIPOctect1
	.section	.bss.GuiVar_CommTestIPOctect1,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPOctect1, %object
	.size	GuiVar_CommTestIPOctect1, 4
GuiVar_CommTestIPOctect1:
	.space	4
	.global	GuiVar_CommTestIPOctect2
	.section	.bss.GuiVar_CommTestIPOctect2,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPOctect2, %object
	.size	GuiVar_CommTestIPOctect2, 4
GuiVar_CommTestIPOctect2:
	.space	4
	.global	GuiVar_CommTestIPOctect3
	.section	.bss.GuiVar_CommTestIPOctect3,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPOctect3, %object
	.size	GuiVar_CommTestIPOctect3, 4
GuiVar_CommTestIPOctect3:
	.space	4
	.global	GuiVar_CommTestIPOctect4
	.section	.bss.GuiVar_CommTestIPOctect4,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPOctect4, %object
	.size	GuiVar_CommTestIPOctect4, 4
GuiVar_CommTestIPOctect4:
	.space	4
	.global	GuiVar_CommTestIPPort
	.section	.bss.GuiVar_CommTestIPPort,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPPort, %object
	.size	GuiVar_CommTestIPPort, 4
GuiVar_CommTestIPPort:
	.space	4
	.global	GuiVar_CommTestShowDebug
	.section	.bss.GuiVar_CommTestShowDebug,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestShowDebug, %object
	.size	GuiVar_CommTestShowDebug, 4
GuiVar_CommTestShowDebug:
	.space	4
	.global	GuiVar_CommTestStatus
	.section	.bss.GuiVar_CommTestStatus,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestStatus, %object
	.size	GuiVar_CommTestStatus, 49
GuiVar_CommTestStatus:
	.space	49
	.global	GuiVar_CommTestSuppressConnections
	.section	.bss.GuiVar_CommTestSuppressConnections,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestSuppressConnections, %object
	.size	GuiVar_CommTestSuppressConnections, 4
GuiVar_CommTestSuppressConnections:
	.space	4
	.global	GuiVar_ContactStatusAlive
	.section	.bss.GuiVar_ContactStatusAlive,"aw",%nobits
	.align	2
	.type	GuiVar_ContactStatusAlive, %object
	.size	GuiVar_ContactStatusAlive, 4
GuiVar_ContactStatusAlive:
	.space	4
	.global	GuiVar_ContactStatusFailures
	.section	.bss.GuiVar_ContactStatusFailures,"aw",%nobits
	.align	2
	.type	GuiVar_ContactStatusFailures, %object
	.size	GuiVar_ContactStatusFailures, 4
GuiVar_ContactStatusFailures:
	.space	4
	.global	GuiVar_ContactStatusNameAbbrv
	.section	.bss.GuiVar_ContactStatusNameAbbrv,"aw",%nobits
	.align	2
	.type	GuiVar_ContactStatusNameAbbrv, %object
	.size	GuiVar_ContactStatusNameAbbrv, 49
GuiVar_ContactStatusNameAbbrv:
	.space	49
	.global	GuiVar_ContactStatusPort
	.section	.bss.GuiVar_ContactStatusPort,"aw",%nobits
	.align	2
	.type	GuiVar_ContactStatusPort, %object
	.size	GuiVar_ContactStatusPort, 4
GuiVar_ContactStatusPort:
	.space	4
	.global	GuiVar_ContactStatusSerialNumber
	.section	.bss.GuiVar_ContactStatusSerialNumber,"aw",%nobits
	.align	2
	.type	GuiVar_ContactStatusSerialNumber, %object
	.size	GuiVar_ContactStatusSerialNumber, 4
GuiVar_ContactStatusSerialNumber:
	.space	4
	.global	GuiVar_Contrast
	.section	.bss.GuiVar_Contrast,"aw",%nobits
	.align	2
	.type	GuiVar_Contrast, %object
	.size	GuiVar_Contrast, 4
GuiVar_Contrast:
	.space	4
	.global	GuiVar_ContrastSliderPos
	.section	.bss.GuiVar_ContrastSliderPos,"aw",%nobits
	.align	2
	.type	GuiVar_ContrastSliderPos, %object
	.size	GuiVar_ContrastSliderPos, 4
GuiVar_ContrastSliderPos:
	.space	4
	.global	GuiVar_DateTimeAMPM
	.section	.bss.GuiVar_DateTimeAMPM,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeAMPM, %object
	.size	GuiVar_DateTimeAMPM, 4
GuiVar_DateTimeAMPM:
	.space	4
	.global	GuiVar_DateTimeDay
	.section	.bss.GuiVar_DateTimeDay,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeDay, %object
	.size	GuiVar_DateTimeDay, 4
GuiVar_DateTimeDay:
	.space	4
	.global	GuiVar_DateTimeDayOfWeek
	.section	.bss.GuiVar_DateTimeDayOfWeek,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeDayOfWeek, %object
	.size	GuiVar_DateTimeDayOfWeek, 4
GuiVar_DateTimeDayOfWeek:
	.space	4
	.global	GuiVar_DateTimeFullStr
	.section	.bss.GuiVar_DateTimeFullStr,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeFullStr, %object
	.size	GuiVar_DateTimeFullStr, 65
GuiVar_DateTimeFullStr:
	.space	65
	.global	GuiVar_DateTimeHour
	.section	.bss.GuiVar_DateTimeHour,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeHour, %object
	.size	GuiVar_DateTimeHour, 4
GuiVar_DateTimeHour:
	.space	4
	.global	GuiVar_DateTimeMin
	.section	.bss.GuiVar_DateTimeMin,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeMin, %object
	.size	GuiVar_DateTimeMin, 4
GuiVar_DateTimeMin:
	.space	4
	.global	GuiVar_DateTimeMonth
	.section	.bss.GuiVar_DateTimeMonth,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeMonth, %object
	.size	GuiVar_DateTimeMonth, 4
GuiVar_DateTimeMonth:
	.space	4
	.global	GuiVar_DateTimeSec
	.section	.bss.GuiVar_DateTimeSec,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeSec, %object
	.size	GuiVar_DateTimeSec, 4
GuiVar_DateTimeSec:
	.space	4
	.global	GuiVar_DateTimeShowButton
	.section	.bss.GuiVar_DateTimeShowButton,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeShowButton, %object
	.size	GuiVar_DateTimeShowButton, 4
GuiVar_DateTimeShowButton:
	.space	4
	.global	GuiVar_DateTimeTimeZone
	.section	.bss.GuiVar_DateTimeTimeZone,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeTimeZone, %object
	.size	GuiVar_DateTimeTimeZone, 4
GuiVar_DateTimeTimeZone:
	.space	4
	.global	GuiVar_DateTimeYear
	.section	.bss.GuiVar_DateTimeYear,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeYear, %object
	.size	GuiVar_DateTimeYear, 4
GuiVar_DateTimeYear:
	.space	4
	.global	GuiVar_DelayBetweenValvesInUse
	.section	.bss.GuiVar_DelayBetweenValvesInUse,"aw",%nobits
	.align	2
	.type	GuiVar_DelayBetweenValvesInUse, %object
	.size	GuiVar_DelayBetweenValvesInUse, 4
GuiVar_DelayBetweenValvesInUse:
	.space	4
	.global	GuiVar_DeviceExchangeProgressBar
	.section	.bss.GuiVar_DeviceExchangeProgressBar,"aw",%nobits
	.align	2
	.type	GuiVar_DeviceExchangeProgressBar, %object
	.size	GuiVar_DeviceExchangeProgressBar, 4
GuiVar_DeviceExchangeProgressBar:
	.space	4
	.global	GuiVar_DeviceExchangeSyncingRadios
	.section	.bss.GuiVar_DeviceExchangeSyncingRadios,"aw",%nobits
	.align	2
	.type	GuiVar_DeviceExchangeSyncingRadios, %object
	.size	GuiVar_DeviceExchangeSyncingRadios, 4
GuiVar_DeviceExchangeSyncingRadios:
	.space	4
	.global	GuiVar_DisplayType
	.section	.bss.GuiVar_DisplayType,"aw",%nobits
	.align	2
	.type	GuiVar_DisplayType, %object
	.size	GuiVar_DisplayType, 4
GuiVar_DisplayType:
	.space	4
	.global	GuiVar_ENDHCPName
	.section	.bss.GuiVar_ENDHCPName,"aw",%nobits
	.align	2
	.type	GuiVar_ENDHCPName, %object
	.size	GuiVar_ENDHCPName, 17
GuiVar_ENDHCPName:
	.space	17
	.global	GuiVar_ENDHCPNameNotSet
	.section	.bss.GuiVar_ENDHCPNameNotSet,"aw",%nobits
	.align	2
	.type	GuiVar_ENDHCPNameNotSet, %object
	.size	GuiVar_ENDHCPNameNotSet, 4
GuiVar_ENDHCPNameNotSet:
	.space	4
	.global	GuiVar_ENFirmwareVer
	.section	.bss.GuiVar_ENFirmwareVer,"aw",%nobits
	.align	2
	.type	GuiVar_ENFirmwareVer, %object
	.size	GuiVar_ENFirmwareVer, 49
GuiVar_ENFirmwareVer:
	.space	49
	.global	GuiVar_ENGateway_1
	.section	.bss.GuiVar_ENGateway_1,"aw",%nobits
	.align	2
	.type	GuiVar_ENGateway_1, %object
	.size	GuiVar_ENGateway_1, 4
GuiVar_ENGateway_1:
	.space	4
	.global	GuiVar_ENGateway_2
	.section	.bss.GuiVar_ENGateway_2,"aw",%nobits
	.align	2
	.type	GuiVar_ENGateway_2, %object
	.size	GuiVar_ENGateway_2, 4
GuiVar_ENGateway_2:
	.space	4
	.global	GuiVar_ENGateway_3
	.section	.bss.GuiVar_ENGateway_3,"aw",%nobits
	.align	2
	.type	GuiVar_ENGateway_3, %object
	.size	GuiVar_ENGateway_3, 4
GuiVar_ENGateway_3:
	.space	4
	.global	GuiVar_ENGateway_4
	.section	.bss.GuiVar_ENGateway_4,"aw",%nobits
	.align	2
	.type	GuiVar_ENGateway_4, %object
	.size	GuiVar_ENGateway_4, 4
GuiVar_ENGateway_4:
	.space	4
	.global	GuiVar_ENIPAddress_1
	.section	.bss.GuiVar_ENIPAddress_1,"aw",%nobits
	.align	2
	.type	GuiVar_ENIPAddress_1, %object
	.size	GuiVar_ENIPAddress_1, 4
GuiVar_ENIPAddress_1:
	.space	4
	.global	GuiVar_ENIPAddress_2
	.section	.bss.GuiVar_ENIPAddress_2,"aw",%nobits
	.align	2
	.type	GuiVar_ENIPAddress_2, %object
	.size	GuiVar_ENIPAddress_2, 4
GuiVar_ENIPAddress_2:
	.space	4
	.global	GuiVar_ENIPAddress_3
	.section	.bss.GuiVar_ENIPAddress_3,"aw",%nobits
	.align	2
	.type	GuiVar_ENIPAddress_3, %object
	.size	GuiVar_ENIPAddress_3, 4
GuiVar_ENIPAddress_3:
	.space	4
	.global	GuiVar_ENIPAddress_4
	.section	.bss.GuiVar_ENIPAddress_4,"aw",%nobits
	.align	2
	.type	GuiVar_ENIPAddress_4, %object
	.size	GuiVar_ENIPAddress_4, 4
GuiVar_ENIPAddress_4:
	.space	4
	.global	GuiVar_ENMACAddress
	.section	.bss.GuiVar_ENMACAddress,"aw",%nobits
	.align	2
	.type	GuiVar_ENMACAddress, %object
	.size	GuiVar_ENMACAddress, 49
GuiVar_ENMACAddress:
	.space	49
	.global	GuiVar_ENModel
	.section	.bss.GuiVar_ENModel,"aw",%nobits
	.align	2
	.type	GuiVar_ENModel, %object
	.size	GuiVar_ENModel, 49
GuiVar_ENModel:
	.space	49
	.global	GuiVar_ENNetmask
	.section	.bss.GuiVar_ENNetmask,"aw",%nobits
	.align	2
	.type	GuiVar_ENNetmask, %object
	.size	GuiVar_ENNetmask, 4
GuiVar_ENNetmask:
	.space	4
	.global	GuiVar_ENObtainIPAutomatically
	.section	.bss.GuiVar_ENObtainIPAutomatically,"aw",%nobits
	.align	2
	.type	GuiVar_ENObtainIPAutomatically, %object
	.size	GuiVar_ENObtainIPAutomatically, 4
GuiVar_ENObtainIPAutomatically:
	.space	4
	.global	GuiVar_ENSubnetMask
	.section	.bss.GuiVar_ENSubnetMask,"aw",%nobits
	.align	2
	.type	GuiVar_ENSubnetMask, %object
	.size	GuiVar_ENSubnetMask, 49
GuiVar_ENSubnetMask:
	.space	49
	.global	GuiVar_ETGageInstalledAt
	.section	.bss.GuiVar_ETGageInstalledAt,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageInstalledAt, %object
	.size	GuiVar_ETGageInstalledAt, 3
GuiVar_ETGageInstalledAt:
	.space	3
	.global	GuiVar_ETGageInUse
	.section	.bss.GuiVar_ETGageInUse,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageInUse, %object
	.size	GuiVar_ETGageInUse, 4
GuiVar_ETGageInUse:
	.space	4
	.global	GuiVar_ETGageLogPulses
	.section	.bss.GuiVar_ETGageLogPulses,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageLogPulses, %object
	.size	GuiVar_ETGageLogPulses, 4
GuiVar_ETGageLogPulses:
	.space	4
	.global	GuiVar_ETGageMaxPercent
	.section	.bss.GuiVar_ETGageMaxPercent,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageMaxPercent, %object
	.size	GuiVar_ETGageMaxPercent, 4
GuiVar_ETGageMaxPercent:
	.space	4
	.global	GuiVar_ETGagePercentFull
	.section	.bss.GuiVar_ETGagePercentFull,"aw",%nobits
	.align	2
	.type	GuiVar_ETGagePercentFull, %object
	.size	GuiVar_ETGagePercentFull, 4
GuiVar_ETGagePercentFull:
	.space	4
	.global	GuiVar_ETGageRunawayGage
	.section	.bss.GuiVar_ETGageRunawayGage,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageRunawayGage, %object
	.size	GuiVar_ETGageRunawayGage, 4
GuiVar_ETGageRunawayGage:
	.space	4
	.global	GuiVar_ETGageSkipTonight
	.section	.bss.GuiVar_ETGageSkipTonight,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageSkipTonight, %object
	.size	GuiVar_ETGageSkipTonight, 4
GuiVar_ETGageSkipTonight:
	.space	4
	.global	GuiVar_ETRainTableCurrentET
	.section	.bss.GuiVar_ETRainTableCurrentET,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableCurrentET, %object
	.size	GuiVar_ETRainTableCurrentET, 4
GuiVar_ETRainTableCurrentET:
	.space	4
	.global	GuiVar_ETRainTableDate
	.section	.bss.GuiVar_ETRainTableDate,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableDate, %object
	.size	GuiVar_ETRainTableDate, 49
GuiVar_ETRainTableDate:
	.space	49
	.global	GuiVar_ETRainTableET
	.section	.bss.GuiVar_ETRainTableET,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableET, %object
	.size	GuiVar_ETRainTableET, 4
GuiVar_ETRainTableET:
	.space	4
	.global	GuiVar_ETRainTableETStatus
	.section	.bss.GuiVar_ETRainTableETStatus,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableETStatus, %object
	.size	GuiVar_ETRainTableETStatus, 4
GuiVar_ETRainTableETStatus:
	.space	4
	.global	GuiVar_ETRainTableRain
	.section	.bss.GuiVar_ETRainTableRain,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableRain, %object
	.size	GuiVar_ETRainTableRain, 4
GuiVar_ETRainTableRain:
	.space	4
	.global	GuiVar_ETRainTableRainStatus
	.section	.bss.GuiVar_ETRainTableRainStatus,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableRainStatus, %object
	.size	GuiVar_ETRainTableRainStatus, 4
GuiVar_ETRainTableRainStatus:
	.space	4
	.global	GuiVar_ETRainTableReport
	.section	.bss.GuiVar_ETRainTableReport,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableReport, %object
	.size	GuiVar_ETRainTableReport, 4
GuiVar_ETRainTableReport:
	.space	4
	.global	GuiVar_ETRainTableShutdown
	.section	.bss.GuiVar_ETRainTableShutdown,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableShutdown, %object
	.size	GuiVar_ETRainTableShutdown, 4
GuiVar_ETRainTableShutdown:
	.space	4
	.global	GuiVar_ETRainTableTable
	.section	.bss.GuiVar_ETRainTableTable,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableTable, %object
	.size	GuiVar_ETRainTableTable, 4
GuiVar_ETRainTableTable:
	.space	4
	.global	GuiVar_FinishTimes
	.section	.bss.GuiVar_FinishTimes,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimes, %object
	.size	GuiVar_FinishTimes, 65
GuiVar_FinishTimes:
	.space	65
	.global	GuiVar_FinishTimesCalcPercentComplete
	.section	.bss.GuiVar_FinishTimesCalcPercentComplete,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesCalcPercentComplete, %object
	.size	GuiVar_FinishTimesCalcPercentComplete, 4
GuiVar_FinishTimesCalcPercentComplete:
	.space	4
	.global	GuiVar_FinishTimesCalcProgressBar
	.section	.bss.GuiVar_FinishTimesCalcProgressBar,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesCalcProgressBar, %object
	.size	GuiVar_FinishTimesCalcProgressBar, 4
GuiVar_FinishTimesCalcProgressBar:
	.space	4
	.global	GuiVar_FinishTimesCalcTime
	.section	.bss.GuiVar_FinishTimesCalcTime,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesCalcTime, %object
	.size	GuiVar_FinishTimesCalcTime, 4
GuiVar_FinishTimesCalcTime:
	.space	4
	.global	GuiVar_FinishTimesNotes
	.section	.bss.GuiVar_FinishTimesNotes,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesNotes, %object
	.size	GuiVar_FinishTimesNotes, 101
GuiVar_FinishTimesNotes:
	.space	101
	.global	GuiVar_FinishTimesScrollBoxH
	.section	.bss.GuiVar_FinishTimesScrollBoxH,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesScrollBoxH, %object
	.size	GuiVar_FinishTimesScrollBoxH, 4
GuiVar_FinishTimesScrollBoxH:
	.space	4
	.global	GuiVar_FinishTimesStartTime
	.section	.bss.GuiVar_FinishTimesStartTime,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesStartTime, %object
	.size	GuiVar_FinishTimesStartTime, 65
GuiVar_FinishTimesStartTime:
	.space	65
	.global	GuiVar_FinishTimesStationGroup
	.section	.bss.GuiVar_FinishTimesStationGroup,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesStationGroup, %object
	.size	GuiVar_FinishTimesStationGroup, 21
GuiVar_FinishTimesStationGroup:
	.space	21
	.global	GuiVar_FinishTimesSystemGroup
	.section	.bss.GuiVar_FinishTimesSystemGroup,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesSystemGroup, %object
	.size	GuiVar_FinishTimesSystemGroup, 21
GuiVar_FinishTimesSystemGroup:
	.space	21
	.global	GuiVar_FLControllerIndex_0
	.section	.bss.GuiVar_FLControllerIndex_0,"aw",%nobits
	.align	2
	.type	GuiVar_FLControllerIndex_0, %object
	.size	GuiVar_FLControllerIndex_0, 4
GuiVar_FLControllerIndex_0:
	.space	4
	.global	GuiVar_FLNumControllersInChain
	.section	.bss.GuiVar_FLNumControllersInChain,"aw",%nobits
	.align	2
	.type	GuiVar_FLNumControllersInChain, %object
	.size	GuiVar_FLNumControllersInChain, 4
GuiVar_FLNumControllersInChain:
	.space	4
	.global	GuiVar_FlowCheckingAllowed
	.section	.bss.GuiVar_FlowCheckingAllowed,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingAllowed, %object
	.size	GuiVar_FlowCheckingAllowed, 4
GuiVar_FlowCheckingAllowed:
	.space	4
	.global	GuiVar_FlowCheckingAllowedToLock
	.section	.bss.GuiVar_FlowCheckingAllowedToLock,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingAllowedToLock, %object
	.size	GuiVar_FlowCheckingAllowedToLock, 4
GuiVar_FlowCheckingAllowedToLock:
	.space	4
	.global	GuiVar_FlowCheckingChecked
	.section	.bss.GuiVar_FlowCheckingChecked,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingChecked, %object
	.size	GuiVar_FlowCheckingChecked, 4
GuiVar_FlowCheckingChecked:
	.space	4
	.global	GuiVar_FlowCheckingCycles
	.section	.bss.GuiVar_FlowCheckingCycles,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingCycles, %object
	.size	GuiVar_FlowCheckingCycles, 4
GuiVar_FlowCheckingCycles:
	.space	4
	.global	GuiVar_FlowCheckingExp
	.section	.bss.GuiVar_FlowCheckingExp,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingExp, %object
	.size	GuiVar_FlowCheckingExp, 4
GuiVar_FlowCheckingExp:
	.space	4
	.global	GuiVar_FlowCheckingExpOn
	.section	.bss.GuiVar_FlowCheckingExpOn,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingExpOn, %object
	.size	GuiVar_FlowCheckingExpOn, 4
GuiVar_FlowCheckingExpOn:
	.space	4
	.global	GuiVar_FlowCheckingFlag0
	.section	.bss.GuiVar_FlowCheckingFlag0,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag0, %object
	.size	GuiVar_FlowCheckingFlag0, 4
GuiVar_FlowCheckingFlag0:
	.space	4
	.global	GuiVar_FlowCheckingFlag1
	.section	.bss.GuiVar_FlowCheckingFlag1,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag1, %object
	.size	GuiVar_FlowCheckingFlag1, 4
GuiVar_FlowCheckingFlag1:
	.space	4
	.global	GuiVar_FlowCheckingFlag2
	.section	.bss.GuiVar_FlowCheckingFlag2,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag2, %object
	.size	GuiVar_FlowCheckingFlag2, 4
GuiVar_FlowCheckingFlag2:
	.space	4
	.global	GuiVar_FlowCheckingFlag3
	.section	.bss.GuiVar_FlowCheckingFlag3,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag3, %object
	.size	GuiVar_FlowCheckingFlag3, 4
GuiVar_FlowCheckingFlag3:
	.space	4
	.global	GuiVar_FlowCheckingFlag4
	.section	.bss.GuiVar_FlowCheckingFlag4,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag4, %object
	.size	GuiVar_FlowCheckingFlag4, 4
GuiVar_FlowCheckingFlag4:
	.space	4
	.global	GuiVar_FlowCheckingFlag5
	.section	.bss.GuiVar_FlowCheckingFlag5,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag5, %object
	.size	GuiVar_FlowCheckingFlag5, 4
GuiVar_FlowCheckingFlag5:
	.space	4
	.global	GuiVar_FlowCheckingFlag6
	.section	.bss.GuiVar_FlowCheckingFlag6,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag6, %object
	.size	GuiVar_FlowCheckingFlag6, 4
GuiVar_FlowCheckingFlag6:
	.space	4
	.global	GuiVar_FlowCheckingFMs
	.section	.bss.GuiVar_FlowCheckingFMs,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFMs, %object
	.size	GuiVar_FlowCheckingFMs, 4
GuiVar_FlowCheckingFMs:
	.space	4
	.global	GuiVar_FlowCheckingGroupCnt0
	.section	.bss.GuiVar_FlowCheckingGroupCnt0,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingGroupCnt0, %object
	.size	GuiVar_FlowCheckingGroupCnt0, 4
GuiVar_FlowCheckingGroupCnt0:
	.space	4
	.global	GuiVar_FlowCheckingGroupCnt1
	.section	.bss.GuiVar_FlowCheckingGroupCnt1,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingGroupCnt1, %object
	.size	GuiVar_FlowCheckingGroupCnt1, 4
GuiVar_FlowCheckingGroupCnt1:
	.space	4
	.global	GuiVar_FlowCheckingGroupCnt2
	.section	.bss.GuiVar_FlowCheckingGroupCnt2,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingGroupCnt2, %object
	.size	GuiVar_FlowCheckingGroupCnt2, 4
GuiVar_FlowCheckingGroupCnt2:
	.space	4
	.global	GuiVar_FlowCheckingGroupCnt3
	.section	.bss.GuiVar_FlowCheckingGroupCnt3,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingGroupCnt3, %object
	.size	GuiVar_FlowCheckingGroupCnt3, 4
GuiVar_FlowCheckingGroupCnt3:
	.space	4
	.global	GuiVar_FlowCheckingHasNCMV
	.section	.bss.GuiVar_FlowCheckingHasNCMV,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingHasNCMV, %object
	.size	GuiVar_FlowCheckingHasNCMV, 4
GuiVar_FlowCheckingHasNCMV:
	.space	4
	.global	GuiVar_FlowCheckingHi
	.section	.bss.GuiVar_FlowCheckingHi,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingHi, %object
	.size	GuiVar_FlowCheckingHi, 4
GuiVar_FlowCheckingHi:
	.space	4
	.global	GuiVar_FlowCheckingINTime
	.section	.bss.GuiVar_FlowCheckingINTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingINTime, %object
	.size	GuiVar_FlowCheckingINTime, 4
GuiVar_FlowCheckingINTime:
	.space	4
	.global	GuiVar_FlowCheckingIterations
	.section	.bss.GuiVar_FlowCheckingIterations,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingIterations, %object
	.size	GuiVar_FlowCheckingIterations, 4
GuiVar_FlowCheckingIterations:
	.space	4
	.global	GuiVar_FlowCheckingLFTime
	.section	.bss.GuiVar_FlowCheckingLFTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingLFTime, %object
	.size	GuiVar_FlowCheckingLFTime, 4
GuiVar_FlowCheckingLFTime:
	.space	4
	.global	GuiVar_FlowCheckingLo
	.section	.bss.GuiVar_FlowCheckingLo,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingLo, %object
	.size	GuiVar_FlowCheckingLo, 4
GuiVar_FlowCheckingLo:
	.space	4
	.global	GuiVar_FlowCheckingMaxOn
	.section	.bss.GuiVar_FlowCheckingMaxOn,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingMaxOn, %object
	.size	GuiVar_FlowCheckingMaxOn, 4
GuiVar_FlowCheckingMaxOn:
	.space	4
	.global	GuiVar_FlowCheckingMVJOTime
	.section	.bss.GuiVar_FlowCheckingMVJOTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingMVJOTime, %object
	.size	GuiVar_FlowCheckingMVJOTime, 4
GuiVar_FlowCheckingMVJOTime:
	.space	4
	.global	GuiVar_FlowCheckingMVState
	.section	.bss.GuiVar_FlowCheckingMVState,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingMVState, %object
	.size	GuiVar_FlowCheckingMVState, 4
GuiVar_FlowCheckingMVState:
	.space	4
	.global	GuiVar_FlowCheckingMVTime
	.section	.bss.GuiVar_FlowCheckingMVTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingMVTime, %object
	.size	GuiVar_FlowCheckingMVTime, 4
GuiVar_FlowCheckingMVTime:
	.space	4
	.global	GuiVar_FlowCheckingNumOn
	.section	.bss.GuiVar_FlowCheckingNumOn,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingNumOn, %object
	.size	GuiVar_FlowCheckingNumOn, 4
GuiVar_FlowCheckingNumOn:
	.space	4
	.global	GuiVar_FlowCheckingOnForProbList
	.section	.bss.GuiVar_FlowCheckingOnForProbList,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingOnForProbList, %object
	.size	GuiVar_FlowCheckingOnForProbList, 4
GuiVar_FlowCheckingOnForProbList:
	.space	4
	.global	GuiVar_FlowCheckingPumpMix
	.section	.bss.GuiVar_FlowCheckingPumpMix,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingPumpMix, %object
	.size	GuiVar_FlowCheckingPumpMix, 4
GuiVar_FlowCheckingPumpMix:
	.space	4
	.global	GuiVar_FlowCheckingPumpState
	.section	.bss.GuiVar_FlowCheckingPumpState,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingPumpState, %object
	.size	GuiVar_FlowCheckingPumpState, 4
GuiVar_FlowCheckingPumpState:
	.space	4
	.global	GuiVar_FlowCheckingPumpTime
	.section	.bss.GuiVar_FlowCheckingPumpTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingPumpTime, %object
	.size	GuiVar_FlowCheckingPumpTime, 4
GuiVar_FlowCheckingPumpTime:
	.space	4
	.global	GuiVar_FlowCheckingSlots
	.section	.bss.GuiVar_FlowCheckingSlots,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingSlots, %object
	.size	GuiVar_FlowCheckingSlots, 4
GuiVar_FlowCheckingSlots:
	.space	4
	.global	GuiVar_FlowCheckingSlotSize
	.section	.bss.GuiVar_FlowCheckingSlotSize,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingSlotSize, %object
	.size	GuiVar_FlowCheckingSlotSize, 4
GuiVar_FlowCheckingSlotSize:
	.space	4
	.global	GuiVar_FlowCheckingStable
	.section	.bss.GuiVar_FlowCheckingStable,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingStable, %object
	.size	GuiVar_FlowCheckingStable, 4
GuiVar_FlowCheckingStable:
	.space	4
	.global	GuiVar_FlowCheckingStopIrriTime
	.section	.bss.GuiVar_FlowCheckingStopIrriTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingStopIrriTime, %object
	.size	GuiVar_FlowCheckingStopIrriTime, 4
GuiVar_FlowCheckingStopIrriTime:
	.space	4
	.global	GuiVar_FlowRecActual
	.section	.bss.GuiVar_FlowRecActual,"aw",%nobits
	.align	2
	.type	GuiVar_FlowRecActual, %object
	.size	GuiVar_FlowRecActual, 4
GuiVar_FlowRecActual:
	.space	4
	.global	GuiVar_FlowRecExpected
	.section	.bss.GuiVar_FlowRecExpected,"aw",%nobits
	.align	2
	.type	GuiVar_FlowRecExpected, %object
	.size	GuiVar_FlowRecExpected, 4
GuiVar_FlowRecExpected:
	.space	4
	.global	GuiVar_FlowRecFlag
	.section	.bss.GuiVar_FlowRecFlag,"aw",%nobits
	.align	2
	.type	GuiVar_FlowRecFlag, %object
	.size	GuiVar_FlowRecFlag, 17
GuiVar_FlowRecFlag:
	.space	17
	.global	GuiVar_FlowRecTimeStamp
	.section	.bss.GuiVar_FlowRecTimeStamp,"aw",%nobits
	.align	2
	.type	GuiVar_FlowRecTimeStamp, %object
	.size	GuiVar_FlowRecTimeStamp, 17
GuiVar_FlowRecTimeStamp:
	.space	17
	.global	GuiVar_FlowTable10Sta
	.section	.bss.GuiVar_FlowTable10Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable10Sta, %object
	.size	GuiVar_FlowTable10Sta, 4
GuiVar_FlowTable10Sta:
	.space	4
	.global	GuiVar_FlowTable2Sta
	.section	.bss.GuiVar_FlowTable2Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable2Sta, %object
	.size	GuiVar_FlowTable2Sta, 4
GuiVar_FlowTable2Sta:
	.space	4
	.global	GuiVar_FlowTable3Sta
	.section	.bss.GuiVar_FlowTable3Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable3Sta, %object
	.size	GuiVar_FlowTable3Sta, 4
GuiVar_FlowTable3Sta:
	.space	4
	.global	GuiVar_FlowTable4Sta
	.section	.bss.GuiVar_FlowTable4Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable4Sta, %object
	.size	GuiVar_FlowTable4Sta, 4
GuiVar_FlowTable4Sta:
	.space	4
	.global	GuiVar_FlowTable5Sta
	.section	.bss.GuiVar_FlowTable5Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable5Sta, %object
	.size	GuiVar_FlowTable5Sta, 4
GuiVar_FlowTable5Sta:
	.space	4
	.global	GuiVar_FlowTable6Sta
	.section	.bss.GuiVar_FlowTable6Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable6Sta, %object
	.size	GuiVar_FlowTable6Sta, 4
GuiVar_FlowTable6Sta:
	.space	4
	.global	GuiVar_FlowTable7Sta
	.section	.bss.GuiVar_FlowTable7Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable7Sta, %object
	.size	GuiVar_FlowTable7Sta, 4
GuiVar_FlowTable7Sta:
	.space	4
	.global	GuiVar_FlowTable8Sta
	.section	.bss.GuiVar_FlowTable8Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable8Sta, %object
	.size	GuiVar_FlowTable8Sta, 4
GuiVar_FlowTable8Sta:
	.space	4
	.global	GuiVar_FlowTable9Sta
	.section	.bss.GuiVar_FlowTable9Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable9Sta, %object
	.size	GuiVar_FlowTable9Sta, 4
GuiVar_FlowTable9Sta:
	.space	4
	.global	GuiVar_FlowTableGPM
	.section	.bss.GuiVar_FlowTableGPM,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTableGPM, %object
	.size	GuiVar_FlowTableGPM, 4
GuiVar_FlowTableGPM:
	.space	4
	.global	GuiVar_FLPartOfChain
	.section	.bss.GuiVar_FLPartOfChain,"aw",%nobits
	.align	2
	.type	GuiVar_FLPartOfChain, %object
	.size	GuiVar_FLPartOfChain, 4
GuiVar_FLPartOfChain:
	.space	4
	.global	GuiVar_FreezeSwitchControllerName
	.section	.bss.GuiVar_FreezeSwitchControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_FreezeSwitchControllerName, %object
	.size	GuiVar_FreezeSwitchControllerName, 49
GuiVar_FreezeSwitchControllerName:
	.space	49
	.global	GuiVar_FreezeSwitchInUse
	.section	.bss.GuiVar_FreezeSwitchInUse,"aw",%nobits
	.align	2
	.type	GuiVar_FreezeSwitchInUse, %object
	.size	GuiVar_FreezeSwitchInUse, 4
GuiVar_FreezeSwitchInUse:
	.space	4
	.global	GuiVar_FreezeSwitchSelected
	.section	.bss.GuiVar_FreezeSwitchSelected,"aw",%nobits
	.align	2
	.type	GuiVar_FreezeSwitchSelected, %object
	.size	GuiVar_FreezeSwitchSelected, 4
GuiVar_FreezeSwitchSelected:
	.space	4
	.global	GuiVar_GRCarrier
	.section	.bss.GuiVar_GRCarrier,"aw",%nobits
	.align	2
	.type	GuiVar_GRCarrier, %object
	.size	GuiVar_GRCarrier, 49
GuiVar_GRCarrier:
	.space	49
	.global	GuiVar_GRECIO
	.section	.bss.GuiVar_GRECIO,"aw",%nobits
	.align	2
	.type	GuiVar_GRECIO, %object
	.size	GuiVar_GRECIO, 49
GuiVar_GRECIO:
	.space	49
	.global	GuiVar_GRFirmwareVersion
	.section	.bss.GuiVar_GRFirmwareVersion,"aw",%nobits
	.align	2
	.type	GuiVar_GRFirmwareVersion, %object
	.size	GuiVar_GRFirmwareVersion, 49
GuiVar_GRFirmwareVersion:
	.space	49
	.global	GuiVar_GRGPRSStatus
	.section	.bss.GuiVar_GRGPRSStatus,"aw",%nobits
	.align	2
	.type	GuiVar_GRGPRSStatus, %object
	.size	GuiVar_GRGPRSStatus, 49
GuiVar_GRGPRSStatus:
	.space	49
	.global	GuiVar_GRIPAddress
	.section	.bss.GuiVar_GRIPAddress,"aw",%nobits
	.align	2
	.type	GuiVar_GRIPAddress, %object
	.size	GuiVar_GRIPAddress, 49
GuiVar_GRIPAddress:
	.space	49
	.global	GuiVar_GRMake
	.section	.bss.GuiVar_GRMake,"aw",%nobits
	.align	2
	.type	GuiVar_GRMake, %object
	.size	GuiVar_GRMake, 49
GuiVar_GRMake:
	.space	49
	.global	GuiVar_GRModel
	.section	.bss.GuiVar_GRModel,"aw",%nobits
	.align	2
	.type	GuiVar_GRModel, %object
	.size	GuiVar_GRModel, 49
GuiVar_GRModel:
	.space	49
	.global	GuiVar_GRNetworkState
	.section	.bss.GuiVar_GRNetworkState,"aw",%nobits
	.align	2
	.type	GuiVar_GRNetworkState, %object
	.size	GuiVar_GRNetworkState, 49
GuiVar_GRNetworkState:
	.space	49
	.global	GuiVar_GroupName
	.section	.bss.GuiVar_GroupName,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName, %object
	.size	GuiVar_GroupName, 65
GuiVar_GroupName:
	.space	65
	.global	GuiVar_GroupName_0
	.section	.bss.GuiVar_GroupName_0,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_0, %object
	.size	GuiVar_GroupName_0, 49
GuiVar_GroupName_0:
	.space	49
	.global	GuiVar_GroupName_1
	.section	.bss.GuiVar_GroupName_1,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_1, %object
	.size	GuiVar_GroupName_1, 49
GuiVar_GroupName_1:
	.space	49
	.global	GuiVar_GroupName_2
	.section	.bss.GuiVar_GroupName_2,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_2, %object
	.size	GuiVar_GroupName_2, 49
GuiVar_GroupName_2:
	.space	49
	.global	GuiVar_GroupName_3
	.section	.bss.GuiVar_GroupName_3,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_3, %object
	.size	GuiVar_GroupName_3, 49
GuiVar_GroupName_3:
	.space	49
	.global	GuiVar_GroupName_4
	.section	.bss.GuiVar_GroupName_4,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_4, %object
	.size	GuiVar_GroupName_4, 49
GuiVar_GroupName_4:
	.space	49
	.global	GuiVar_GroupName_5
	.section	.bss.GuiVar_GroupName_5,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_5, %object
	.size	GuiVar_GroupName_5, 49
GuiVar_GroupName_5:
	.space	49
	.global	GuiVar_GroupName_6
	.section	.bss.GuiVar_GroupName_6,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_6, %object
	.size	GuiVar_GroupName_6, 49
GuiVar_GroupName_6:
	.space	49
	.global	GuiVar_GroupName_7
	.section	.bss.GuiVar_GroupName_7,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_7, %object
	.size	GuiVar_GroupName_7, 49
GuiVar_GroupName_7:
	.space	49
	.global	GuiVar_GroupName_8
	.section	.bss.GuiVar_GroupName_8,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_8, %object
	.size	GuiVar_GroupName_8, 49
GuiVar_GroupName_8:
	.space	49
	.global	GuiVar_GroupName_9
	.section	.bss.GuiVar_GroupName_9,"aw",%nobits
	.align	2
	.type	GuiVar_GroupName_9, %object
	.size	GuiVar_GroupName_9, 49
GuiVar_GroupName_9:
	.space	49
	.global	GuiVar_GroupSetting_Visible_0
	.section	.bss.GuiVar_GroupSetting_Visible_0,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_0, %object
	.size	GuiVar_GroupSetting_Visible_0, 4
GuiVar_GroupSetting_Visible_0:
	.space	4
	.global	GuiVar_GroupSetting_Visible_1
	.section	.bss.GuiVar_GroupSetting_Visible_1,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_1, %object
	.size	GuiVar_GroupSetting_Visible_1, 4
GuiVar_GroupSetting_Visible_1:
	.space	4
	.global	GuiVar_GroupSetting_Visible_2
	.section	.bss.GuiVar_GroupSetting_Visible_2,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_2, %object
	.size	GuiVar_GroupSetting_Visible_2, 4
GuiVar_GroupSetting_Visible_2:
	.space	4
	.global	GuiVar_GroupSetting_Visible_3
	.section	.bss.GuiVar_GroupSetting_Visible_3,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_3, %object
	.size	GuiVar_GroupSetting_Visible_3, 4
GuiVar_GroupSetting_Visible_3:
	.space	4
	.global	GuiVar_GroupSetting_Visible_4
	.section	.bss.GuiVar_GroupSetting_Visible_4,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_4, %object
	.size	GuiVar_GroupSetting_Visible_4, 4
GuiVar_GroupSetting_Visible_4:
	.space	4
	.global	GuiVar_GroupSetting_Visible_5
	.section	.bss.GuiVar_GroupSetting_Visible_5,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_5, %object
	.size	GuiVar_GroupSetting_Visible_5, 4
GuiVar_GroupSetting_Visible_5:
	.space	4
	.global	GuiVar_GroupSetting_Visible_6
	.section	.bss.GuiVar_GroupSetting_Visible_6,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_6, %object
	.size	GuiVar_GroupSetting_Visible_6, 4
GuiVar_GroupSetting_Visible_6:
	.space	4
	.global	GuiVar_GroupSetting_Visible_7
	.section	.bss.GuiVar_GroupSetting_Visible_7,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_7, %object
	.size	GuiVar_GroupSetting_Visible_7, 4
GuiVar_GroupSetting_Visible_7:
	.space	4
	.global	GuiVar_GroupSetting_Visible_8
	.section	.bss.GuiVar_GroupSetting_Visible_8,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_8, %object
	.size	GuiVar_GroupSetting_Visible_8, 4
GuiVar_GroupSetting_Visible_8:
	.space	4
	.global	GuiVar_GroupSetting_Visible_9
	.section	.bss.GuiVar_GroupSetting_Visible_9,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_9, %object
	.size	GuiVar_GroupSetting_Visible_9, 4
GuiVar_GroupSetting_Visible_9:
	.space	4
	.global	GuiVar_GroupSettingA_0
	.section	.bss.GuiVar_GroupSettingA_0,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_0, %object
	.size	GuiVar_GroupSettingA_0, 4
GuiVar_GroupSettingA_0:
	.space	4
	.global	GuiVar_GroupSettingA_1
	.section	.bss.GuiVar_GroupSettingA_1,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_1, %object
	.size	GuiVar_GroupSettingA_1, 4
GuiVar_GroupSettingA_1:
	.space	4
	.global	GuiVar_GroupSettingA_2
	.section	.bss.GuiVar_GroupSettingA_2,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_2, %object
	.size	GuiVar_GroupSettingA_2, 4
GuiVar_GroupSettingA_2:
	.space	4
	.global	GuiVar_GroupSettingA_3
	.section	.bss.GuiVar_GroupSettingA_3,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_3, %object
	.size	GuiVar_GroupSettingA_3, 4
GuiVar_GroupSettingA_3:
	.space	4
	.global	GuiVar_GroupSettingA_4
	.section	.bss.GuiVar_GroupSettingA_4,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_4, %object
	.size	GuiVar_GroupSettingA_4, 4
GuiVar_GroupSettingA_4:
	.space	4
	.global	GuiVar_GroupSettingA_5
	.section	.bss.GuiVar_GroupSettingA_5,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_5, %object
	.size	GuiVar_GroupSettingA_5, 4
GuiVar_GroupSettingA_5:
	.space	4
	.global	GuiVar_GroupSettingA_6
	.section	.bss.GuiVar_GroupSettingA_6,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_6, %object
	.size	GuiVar_GroupSettingA_6, 4
GuiVar_GroupSettingA_6:
	.space	4
	.global	GuiVar_GroupSettingA_7
	.section	.bss.GuiVar_GroupSettingA_7,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_7, %object
	.size	GuiVar_GroupSettingA_7, 4
GuiVar_GroupSettingA_7:
	.space	4
	.global	GuiVar_GroupSettingA_8
	.section	.bss.GuiVar_GroupSettingA_8,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_8, %object
	.size	GuiVar_GroupSettingA_8, 4
GuiVar_GroupSettingA_8:
	.space	4
	.global	GuiVar_GroupSettingA_9
	.section	.bss.GuiVar_GroupSettingA_9,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_9, %object
	.size	GuiVar_GroupSettingA_9, 4
GuiVar_GroupSettingA_9:
	.space	4
	.global	GuiVar_GroupSettingB_0
	.section	.bss.GuiVar_GroupSettingB_0,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_0, %object
	.size	GuiVar_GroupSettingB_0, 4
GuiVar_GroupSettingB_0:
	.space	4
	.global	GuiVar_GroupSettingB_1
	.section	.bss.GuiVar_GroupSettingB_1,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_1, %object
	.size	GuiVar_GroupSettingB_1, 4
GuiVar_GroupSettingB_1:
	.space	4
	.global	GuiVar_GroupSettingB_2
	.section	.bss.GuiVar_GroupSettingB_2,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_2, %object
	.size	GuiVar_GroupSettingB_2, 4
GuiVar_GroupSettingB_2:
	.space	4
	.global	GuiVar_GroupSettingB_3
	.section	.bss.GuiVar_GroupSettingB_3,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_3, %object
	.size	GuiVar_GroupSettingB_3, 4
GuiVar_GroupSettingB_3:
	.space	4
	.global	GuiVar_GroupSettingB_4
	.section	.bss.GuiVar_GroupSettingB_4,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_4, %object
	.size	GuiVar_GroupSettingB_4, 4
GuiVar_GroupSettingB_4:
	.space	4
	.global	GuiVar_GroupSettingB_5
	.section	.bss.GuiVar_GroupSettingB_5,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_5, %object
	.size	GuiVar_GroupSettingB_5, 4
GuiVar_GroupSettingB_5:
	.space	4
	.global	GuiVar_GroupSettingB_6
	.section	.bss.GuiVar_GroupSettingB_6,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_6, %object
	.size	GuiVar_GroupSettingB_6, 4
GuiVar_GroupSettingB_6:
	.space	4
	.global	GuiVar_GroupSettingB_7
	.section	.bss.GuiVar_GroupSettingB_7,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_7, %object
	.size	GuiVar_GroupSettingB_7, 4
GuiVar_GroupSettingB_7:
	.space	4
	.global	GuiVar_GroupSettingB_8
	.section	.bss.GuiVar_GroupSettingB_8,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_8, %object
	.size	GuiVar_GroupSettingB_8, 4
GuiVar_GroupSettingB_8:
	.space	4
	.global	GuiVar_GroupSettingB_9
	.section	.bss.GuiVar_GroupSettingB_9,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_9, %object
	.size	GuiVar_GroupSettingB_9, 4
GuiVar_GroupSettingB_9:
	.space	4
	.global	GuiVar_GroupSettingC_0
	.section	.bss.GuiVar_GroupSettingC_0,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_0, %object
	.size	GuiVar_GroupSettingC_0, 4
GuiVar_GroupSettingC_0:
	.space	4
	.global	GuiVar_GroupSettingC_1
	.section	.bss.GuiVar_GroupSettingC_1,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_1, %object
	.size	GuiVar_GroupSettingC_1, 4
GuiVar_GroupSettingC_1:
	.space	4
	.global	GuiVar_GroupSettingC_2
	.section	.bss.GuiVar_GroupSettingC_2,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_2, %object
	.size	GuiVar_GroupSettingC_2, 4
GuiVar_GroupSettingC_2:
	.space	4
	.global	GuiVar_GroupSettingC_3
	.section	.bss.GuiVar_GroupSettingC_3,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_3, %object
	.size	GuiVar_GroupSettingC_3, 4
GuiVar_GroupSettingC_3:
	.space	4
	.global	GuiVar_GroupSettingC_4
	.section	.bss.GuiVar_GroupSettingC_4,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_4, %object
	.size	GuiVar_GroupSettingC_4, 4
GuiVar_GroupSettingC_4:
	.space	4
	.global	GuiVar_GroupSettingC_5
	.section	.bss.GuiVar_GroupSettingC_5,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_5, %object
	.size	GuiVar_GroupSettingC_5, 4
GuiVar_GroupSettingC_5:
	.space	4
	.global	GuiVar_GroupSettingC_6
	.section	.bss.GuiVar_GroupSettingC_6,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_6, %object
	.size	GuiVar_GroupSettingC_6, 4
GuiVar_GroupSettingC_6:
	.space	4
	.global	GuiVar_GroupSettingC_7
	.section	.bss.GuiVar_GroupSettingC_7,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_7, %object
	.size	GuiVar_GroupSettingC_7, 4
GuiVar_GroupSettingC_7:
	.space	4
	.global	GuiVar_GroupSettingC_8
	.section	.bss.GuiVar_GroupSettingC_8,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_8, %object
	.size	GuiVar_GroupSettingC_8, 4
GuiVar_GroupSettingC_8:
	.space	4
	.global	GuiVar_GroupSettingC_9
	.section	.bss.GuiVar_GroupSettingC_9,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_9, %object
	.size	GuiVar_GroupSettingC_9, 4
GuiVar_GroupSettingC_9:
	.space	4
	.global	GuiVar_GRPhoneNumber
	.section	.bss.GuiVar_GRPhoneNumber,"aw",%nobits
	.align	2
	.type	GuiVar_GRPhoneNumber, %object
	.size	GuiVar_GRPhoneNumber, 49
GuiVar_GRPhoneNumber:
	.space	49
	.global	GuiVar_GRRadioSerialNum
	.section	.bss.GuiVar_GRRadioSerialNum,"aw",%nobits
	.align	2
	.type	GuiVar_GRRadioSerialNum, %object
	.size	GuiVar_GRRadioSerialNum, 49
GuiVar_GRRadioSerialNum:
	.space	49
	.global	GuiVar_GRRSSI
	.section	.bss.GuiVar_GRRSSI,"aw",%nobits
	.align	2
	.type	GuiVar_GRRSSI, %object
	.size	GuiVar_GRRSSI, 49
GuiVar_GRRSSI:
	.space	49
	.global	GuiVar_GRService
	.section	.bss.GuiVar_GRService,"aw",%nobits
	.align	2
	.type	GuiVar_GRService, %object
	.size	GuiVar_GRService, 49
GuiVar_GRService:
	.space	49
	.global	GuiVar_GRSIMID
	.section	.bss.GuiVar_GRSIMID,"aw",%nobits
	.align	2
	.type	GuiVar_GRSIMID, %object
	.size	GuiVar_GRSIMID, 49
GuiVar_GRSIMID:
	.space	49
	.global	GuiVar_GRSIMState
	.section	.bss.GuiVar_GRSIMState,"aw",%nobits
	.align	2
	.type	GuiVar_GRSIMState, %object
	.size	GuiVar_GRSIMState, 49
GuiVar_GRSIMState:
	.space	49
	.global	GuiVar_HistoricalET1
	.section	.bss.GuiVar_HistoricalET1,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET1, %object
	.size	GuiVar_HistoricalET1, 4
GuiVar_HistoricalET1:
	.space	4
	.global	GuiVar_HistoricalET10
	.section	.bss.GuiVar_HistoricalET10,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET10, %object
	.size	GuiVar_HistoricalET10, 4
GuiVar_HistoricalET10:
	.space	4
	.global	GuiVar_HistoricalET11
	.section	.bss.GuiVar_HistoricalET11,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET11, %object
	.size	GuiVar_HistoricalET11, 4
GuiVar_HistoricalET11:
	.space	4
	.global	GuiVar_HistoricalET12
	.section	.bss.GuiVar_HistoricalET12,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET12, %object
	.size	GuiVar_HistoricalET12, 4
GuiVar_HistoricalET12:
	.space	4
	.global	GuiVar_HistoricalET2
	.section	.bss.GuiVar_HistoricalET2,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET2, %object
	.size	GuiVar_HistoricalET2, 4
GuiVar_HistoricalET2:
	.space	4
	.global	GuiVar_HistoricalET3
	.section	.bss.GuiVar_HistoricalET3,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET3, %object
	.size	GuiVar_HistoricalET3, 4
GuiVar_HistoricalET3:
	.space	4
	.global	GuiVar_HistoricalET4
	.section	.bss.GuiVar_HistoricalET4,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET4, %object
	.size	GuiVar_HistoricalET4, 4
GuiVar_HistoricalET4:
	.space	4
	.global	GuiVar_HistoricalET5
	.section	.bss.GuiVar_HistoricalET5,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET5, %object
	.size	GuiVar_HistoricalET5, 4
GuiVar_HistoricalET5:
	.space	4
	.global	GuiVar_HistoricalET6
	.section	.bss.GuiVar_HistoricalET6,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET6, %object
	.size	GuiVar_HistoricalET6, 4
GuiVar_HistoricalET6:
	.space	4
	.global	GuiVar_HistoricalET7
	.section	.bss.GuiVar_HistoricalET7,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET7, %object
	.size	GuiVar_HistoricalET7, 4
GuiVar_HistoricalET7:
	.space	4
	.global	GuiVar_HistoricalET8
	.section	.bss.GuiVar_HistoricalET8,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET8, %object
	.size	GuiVar_HistoricalET8, 4
GuiVar_HistoricalET8:
	.space	4
	.global	GuiVar_HistoricalET9
	.section	.bss.GuiVar_HistoricalET9,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET9, %object
	.size	GuiVar_HistoricalET9, 4
GuiVar_HistoricalET9:
	.space	4
	.global	GuiVar_HistoricalETCity
	.section	.bss.GuiVar_HistoricalETCity,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETCity, %object
	.size	GuiVar_HistoricalETCity, 25
GuiVar_HistoricalETCity:
	.space	25
	.global	GuiVar_HistoricalETCounty
	.section	.bss.GuiVar_HistoricalETCounty,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETCounty, %object
	.size	GuiVar_HistoricalETCounty, 25
GuiVar_HistoricalETCounty:
	.space	25
	.global	GuiVar_HistoricalETState
	.section	.bss.GuiVar_HistoricalETState,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETState, %object
	.size	GuiVar_HistoricalETState, 25
GuiVar_HistoricalETState:
	.space	25
	.global	GuiVar_HistoricalETUser1
	.section	.bss.GuiVar_HistoricalETUser1,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser1, %object
	.size	GuiVar_HistoricalETUser1, 4
GuiVar_HistoricalETUser1:
	.space	4
	.global	GuiVar_HistoricalETUser10
	.section	.bss.GuiVar_HistoricalETUser10,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser10, %object
	.size	GuiVar_HistoricalETUser10, 4
GuiVar_HistoricalETUser10:
	.space	4
	.global	GuiVar_HistoricalETUser11
	.section	.bss.GuiVar_HistoricalETUser11,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser11, %object
	.size	GuiVar_HistoricalETUser11, 4
GuiVar_HistoricalETUser11:
	.space	4
	.global	GuiVar_HistoricalETUser12
	.section	.bss.GuiVar_HistoricalETUser12,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser12, %object
	.size	GuiVar_HistoricalETUser12, 4
GuiVar_HistoricalETUser12:
	.space	4
	.global	GuiVar_HistoricalETUser2
	.section	.bss.GuiVar_HistoricalETUser2,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser2, %object
	.size	GuiVar_HistoricalETUser2, 4
GuiVar_HistoricalETUser2:
	.space	4
	.global	GuiVar_HistoricalETUser3
	.section	.bss.GuiVar_HistoricalETUser3,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser3, %object
	.size	GuiVar_HistoricalETUser3, 4
GuiVar_HistoricalETUser3:
	.space	4
	.global	GuiVar_HistoricalETUser4
	.section	.bss.GuiVar_HistoricalETUser4,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser4, %object
	.size	GuiVar_HistoricalETUser4, 4
GuiVar_HistoricalETUser4:
	.space	4
	.global	GuiVar_HistoricalETUser5
	.section	.bss.GuiVar_HistoricalETUser5,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser5, %object
	.size	GuiVar_HistoricalETUser5, 4
GuiVar_HistoricalETUser5:
	.space	4
	.global	GuiVar_HistoricalETUser6
	.section	.bss.GuiVar_HistoricalETUser6,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser6, %object
	.size	GuiVar_HistoricalETUser6, 4
GuiVar_HistoricalETUser6:
	.space	4
	.global	GuiVar_HistoricalETUser7
	.section	.bss.GuiVar_HistoricalETUser7,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser7, %object
	.size	GuiVar_HistoricalETUser7, 4
GuiVar_HistoricalETUser7:
	.space	4
	.global	GuiVar_HistoricalETUser8
	.section	.bss.GuiVar_HistoricalETUser8,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser8, %object
	.size	GuiVar_HistoricalETUser8, 4
GuiVar_HistoricalETUser8:
	.space	4
	.global	GuiVar_HistoricalETUser9
	.section	.bss.GuiVar_HistoricalETUser9,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser9, %object
	.size	GuiVar_HistoricalETUser9, 4
GuiVar_HistoricalETUser9:
	.space	4
	.global	GuiVar_HistoricalETUserCity
	.section	.bss.GuiVar_HistoricalETUserCity,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUserCity, %object
	.size	GuiVar_HistoricalETUserCity, 25
GuiVar_HistoricalETUserCity:
	.space	25
	.global	GuiVar_HistoricalETUserCounty
	.section	.bss.GuiVar_HistoricalETUserCounty,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUserCounty, %object
	.size	GuiVar_HistoricalETUserCounty, 25
GuiVar_HistoricalETUserCounty:
	.space	25
	.global	GuiVar_HistoricalETUserState
	.section	.bss.GuiVar_HistoricalETUserState,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUserState, %object
	.size	GuiVar_HistoricalETUserState, 25
GuiVar_HistoricalETUserState:
	.space	25
	.global	GuiVar_HistoricalETUseYourOwn
	.section	.bss.GuiVar_HistoricalETUseYourOwn,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUseYourOwn, %object
	.size	GuiVar_HistoricalETUseYourOwn, 4
GuiVar_HistoricalETUseYourOwn:
	.space	4
	.global	GuiVar_HubList
	.section	.bss.GuiVar_HubList,"aw",%nobits
	.align	2
	.type	GuiVar_HubList, %object
	.size	GuiVar_HubList, 513
GuiVar_HubList:
	.space	513
	.global	GuiVar_IrriCommChainConfigActive
	.section	.bss.GuiVar_IrriCommChainConfigActive,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigActive, %object
	.size	GuiVar_IrriCommChainConfigActive, 4
GuiVar_IrriCommChainConfigActive:
	.space	4
	.global	GuiVar_IrriCommChainConfigCurrentFromFOAL
	.section	.bss.GuiVar_IrriCommChainConfigCurrentFromFOAL,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentFromFOAL, %object
	.size	GuiVar_IrriCommChainConfigCurrentFromFOAL, 4
GuiVar_IrriCommChainConfigCurrentFromFOAL:
	.space	4
	.global	GuiVar_IrriCommChainConfigCurrentFromIrri
	.section	.bss.GuiVar_IrriCommChainConfigCurrentFromIrri,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentFromIrri, %object
	.size	GuiVar_IrriCommChainConfigCurrentFromIrri, 4
GuiVar_IrriCommChainConfigCurrentFromIrri:
	.space	4
	.global	GuiVar_IrriCommChainConfigCurrentFromTP
	.section	.bss.GuiVar_IrriCommChainConfigCurrentFromTP,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentFromTP, %object
	.size	GuiVar_IrriCommChainConfigCurrentFromTP, 4
GuiVar_IrriCommChainConfigCurrentFromTP:
	.space	4
	.global	GuiVar_IrriCommChainConfigCurrentSendFromIrri
	.section	.bss.GuiVar_IrriCommChainConfigCurrentSendFromIrri,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentSendFromIrri, %object
	.size	GuiVar_IrriCommChainConfigCurrentSendFromIrri, 4
GuiVar_IrriCommChainConfigCurrentSendFromIrri:
	.space	4
	.global	GuiVar_IrriCommChainConfigCurrentSendFromTP
	.section	.bss.GuiVar_IrriCommChainConfigCurrentSendFromTP,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentSendFromTP, %object
	.size	GuiVar_IrriCommChainConfigCurrentSendFromTP, 4
GuiVar_IrriCommChainConfigCurrentSendFromTP:
	.space	4
	.global	GuiVar_IrriCommChainConfigNameAbbrv
	.section	.bss.GuiVar_IrriCommChainConfigNameAbbrv,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigNameAbbrv, %object
	.size	GuiVar_IrriCommChainConfigNameAbbrv, 49
GuiVar_IrriCommChainConfigNameAbbrv:
	.space	49
	.global	GuiVar_IrriCommChainConfigSerialNum
	.section	.bss.GuiVar_IrriCommChainConfigSerialNum,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigSerialNum, %object
	.size	GuiVar_IrriCommChainConfigSerialNum, 4
GuiVar_IrriCommChainConfigSerialNum:
	.space	4
	.global	GuiVar_IrriDetailsController
	.section	.bss.GuiVar_IrriDetailsController,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsController, %object
	.size	GuiVar_IrriDetailsController, 4
GuiVar_IrriDetailsController:
	.space	4
	.global	GuiVar_IrriDetailsCurrentDraw
	.section	.bss.GuiVar_IrriDetailsCurrentDraw,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsCurrentDraw, %object
	.size	GuiVar_IrriDetailsCurrentDraw, 4
GuiVar_IrriDetailsCurrentDraw:
	.space	4
	.global	GuiVar_IrriDetailsProgCycle
	.section	.bss.GuiVar_IrriDetailsProgCycle,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsProgCycle, %object
	.size	GuiVar_IrriDetailsProgCycle, 4
GuiVar_IrriDetailsProgCycle:
	.space	4
	.global	GuiVar_IrriDetailsProgLeft
	.section	.bss.GuiVar_IrriDetailsProgLeft,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsProgLeft, %object
	.size	GuiVar_IrriDetailsProgLeft, 4
GuiVar_IrriDetailsProgLeft:
	.space	4
	.global	GuiVar_IrriDetailsReason
	.section	.bss.GuiVar_IrriDetailsReason,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsReason, %object
	.size	GuiVar_IrriDetailsReason, 4
GuiVar_IrriDetailsReason:
	.space	4
	.global	GuiVar_IrriDetailsRunCycle
	.section	.bss.GuiVar_IrriDetailsRunCycle,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsRunCycle, %object
	.size	GuiVar_IrriDetailsRunCycle, 4
GuiVar_IrriDetailsRunCycle:
	.space	4
	.global	GuiVar_IrriDetailsRunSoak
	.section	.bss.GuiVar_IrriDetailsRunSoak,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsRunSoak, %object
	.size	GuiVar_IrriDetailsRunSoak, 4
GuiVar_IrriDetailsRunSoak:
	.space	4
	.global	GuiVar_IrriDetailsShowFOAL
	.section	.bss.GuiVar_IrriDetailsShowFOAL,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsShowFOAL, %object
	.size	GuiVar_IrriDetailsShowFOAL, 4
GuiVar_IrriDetailsShowFOAL:
	.space	4
	.global	GuiVar_IrriDetailsStation
	.section	.bss.GuiVar_IrriDetailsStation,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsStation, %object
	.size	GuiVar_IrriDetailsStation, 4
GuiVar_IrriDetailsStation:
	.space	4
	.global	GuiVar_IrriDetailsStatus
	.section	.bss.GuiVar_IrriDetailsStatus,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsStatus, %object
	.size	GuiVar_IrriDetailsStatus, 4
GuiVar_IrriDetailsStatus:
	.space	4
	.global	GuiVar_IsAHub
	.section	.bss.GuiVar_IsAHub,"aw",%nobits
	.align	2
	.type	GuiVar_IsAHub, %object
	.size	GuiVar_IsAHub, 4
GuiVar_IsAHub:
	.space	4
	.global	GuiVar_IsMaster
	.section	.bss.GuiVar_IsMaster,"aw",%nobits
	.align	2
	.type	GuiVar_IsMaster, %object
	.size	GuiVar_IsMaster, 4
GuiVar_IsMaster:
	.space	4
	.global	GuiVar_itmDecoder_Debug
	.section	.bss.GuiVar_itmDecoder_Debug,"aw",%nobits
	.align	2
	.type	GuiVar_itmDecoder_Debug, %object
	.size	GuiVar_itmDecoder_Debug, 49
GuiVar_itmDecoder_Debug:
	.space	49
	.global	GuiVar_itmGroupName
	.section	.bss.GuiVar_itmGroupName,"aw",%nobits
	.align	2
	.type	GuiVar_itmGroupName, %object
	.size	GuiVar_itmGroupName, 49
GuiVar_itmGroupName:
	.space	49
	.global	GuiVar_itmMenuItem
	.section	.bss.GuiVar_itmMenuItem,"aw",%nobits
	.align	2
	.type	GuiVar_itmMenuItem, %object
	.size	GuiVar_itmMenuItem, 65
GuiVar_itmMenuItem:
	.space	65
	.global	GuiVar_itmMenuItemDesc
	.section	.bss.GuiVar_itmMenuItemDesc,"aw",%nobits
	.align	2
	.type	GuiVar_itmMenuItemDesc, %object
	.size	GuiVar_itmMenuItemDesc, 65
GuiVar_itmMenuItemDesc:
	.space	65
	.global	GuiVar_itmPOC
	.section	.bss.GuiVar_itmPOC,"aw",%nobits
	.align	2
	.type	GuiVar_itmPOC, %object
	.size	GuiVar_itmPOC, 49
GuiVar_itmPOC:
	.space	49
	.global	GuiVar_itmStationName
	.section	.bss.GuiVar_itmStationName,"aw",%nobits
	.align	2
	.type	GuiVar_itmStationName, %object
	.size	GuiVar_itmStationName, 49
GuiVar_itmStationName:
	.space	49
	.global	GuiVar_itmStationNumber
	.section	.bss.GuiVar_itmStationNumber,"aw",%nobits
	.align	2
	.type	GuiVar_itmStationNumber, %object
	.size	GuiVar_itmStationNumber, 4
GuiVar_itmStationNumber:
	.space	4
	.global	GuiVar_itmSubNode
	.section	.bss.GuiVar_itmSubNode,"aw",%nobits
	.align	2
	.type	GuiVar_itmSubNode, %object
	.size	GuiVar_itmSubNode, 4
GuiVar_itmSubNode:
	.space	4
	.global	GuiVar_KeyboardShiftEnabled
	.section	.bss.GuiVar_KeyboardShiftEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_KeyboardShiftEnabled, %object
	.size	GuiVar_KeyboardShiftEnabled, 4
GuiVar_KeyboardShiftEnabled:
	.space	4
	.global	GuiVar_KeyboardStartX
	.section	.bss.GuiVar_KeyboardStartX,"aw",%nobits
	.align	2
	.type	GuiVar_KeyboardStartX, %object
	.size	GuiVar_KeyboardStartX, 4
GuiVar_KeyboardStartX:
	.space	4
	.global	GuiVar_KeyboardStartY
	.section	.bss.GuiVar_KeyboardStartY,"aw",%nobits
	.align	2
	.type	GuiVar_KeyboardStartY, %object
	.size	GuiVar_KeyboardStartY, 4
GuiVar_KeyboardStartY:
	.space	4
	.global	GuiVar_KeyboardType
	.section	.bss.GuiVar_KeyboardType,"aw",%nobits
	.align	2
	.type	GuiVar_KeyboardType, %object
	.size	GuiVar_KeyboardType, 4
GuiVar_KeyboardType:
	.space	4
	.global	GuiVar_LightCalendarWeek1Fri
	.section	.bss.GuiVar_LightCalendarWeek1Fri,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek1Fri, %object
	.size	GuiVar_LightCalendarWeek1Fri, 3
GuiVar_LightCalendarWeek1Fri:
	.space	3
	.global	GuiVar_LightCalendarWeek1Mon
	.section	.bss.GuiVar_LightCalendarWeek1Mon,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek1Mon, %object
	.size	GuiVar_LightCalendarWeek1Mon, 3
GuiVar_LightCalendarWeek1Mon:
	.space	3
	.global	GuiVar_LightCalendarWeek1Sat
	.section	.bss.GuiVar_LightCalendarWeek1Sat,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek1Sat, %object
	.size	GuiVar_LightCalendarWeek1Sat, 3
GuiVar_LightCalendarWeek1Sat:
	.space	3
	.global	GuiVar_LightCalendarWeek1Sun
	.section	.bss.GuiVar_LightCalendarWeek1Sun,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek1Sun, %object
	.size	GuiVar_LightCalendarWeek1Sun, 3
GuiVar_LightCalendarWeek1Sun:
	.space	3
	.global	GuiVar_LightCalendarWeek1Thu
	.section	.bss.GuiVar_LightCalendarWeek1Thu,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek1Thu, %object
	.size	GuiVar_LightCalendarWeek1Thu, 3
GuiVar_LightCalendarWeek1Thu:
	.space	3
	.global	GuiVar_LightCalendarWeek1Tue
	.section	.bss.GuiVar_LightCalendarWeek1Tue,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek1Tue, %object
	.size	GuiVar_LightCalendarWeek1Tue, 3
GuiVar_LightCalendarWeek1Tue:
	.space	3
	.global	GuiVar_LightCalendarWeek1Wed
	.section	.bss.GuiVar_LightCalendarWeek1Wed,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek1Wed, %object
	.size	GuiVar_LightCalendarWeek1Wed, 3
GuiVar_LightCalendarWeek1Wed:
	.space	3
	.global	GuiVar_LightCalendarWeek2Fri
	.section	.bss.GuiVar_LightCalendarWeek2Fri,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek2Fri, %object
	.size	GuiVar_LightCalendarWeek2Fri, 3
GuiVar_LightCalendarWeek2Fri:
	.space	3
	.global	GuiVar_LightCalendarWeek2Mon
	.section	.bss.GuiVar_LightCalendarWeek2Mon,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek2Mon, %object
	.size	GuiVar_LightCalendarWeek2Mon, 3
GuiVar_LightCalendarWeek2Mon:
	.space	3
	.global	GuiVar_LightCalendarWeek2Sat
	.section	.bss.GuiVar_LightCalendarWeek2Sat,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek2Sat, %object
	.size	GuiVar_LightCalendarWeek2Sat, 3
GuiVar_LightCalendarWeek2Sat:
	.space	3
	.global	GuiVar_LightCalendarWeek2Sun
	.section	.bss.GuiVar_LightCalendarWeek2Sun,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek2Sun, %object
	.size	GuiVar_LightCalendarWeek2Sun, 3
GuiVar_LightCalendarWeek2Sun:
	.space	3
	.global	GuiVar_LightCalendarWeek2Thu
	.section	.bss.GuiVar_LightCalendarWeek2Thu,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek2Thu, %object
	.size	GuiVar_LightCalendarWeek2Thu, 3
GuiVar_LightCalendarWeek2Thu:
	.space	3
	.global	GuiVar_LightCalendarWeek2Tue
	.section	.bss.GuiVar_LightCalendarWeek2Tue,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek2Tue, %object
	.size	GuiVar_LightCalendarWeek2Tue, 3
GuiVar_LightCalendarWeek2Tue:
	.space	3
	.global	GuiVar_LightCalendarWeek2Wed
	.section	.bss.GuiVar_LightCalendarWeek2Wed,"aw",%nobits
	.align	2
	.type	GuiVar_LightCalendarWeek2Wed, %object
	.size	GuiVar_LightCalendarWeek2Wed, 3
GuiVar_LightCalendarWeek2Wed:
	.space	3
	.global	GuiVar_LightEditableStop
	.section	.bss.GuiVar_LightEditableStop,"aw",%nobits
	.align	2
	.type	GuiVar_LightEditableStop, %object
	.size	GuiVar_LightEditableStop, 33
GuiVar_LightEditableStop:
	.space	33
	.global	GuiVar_LightIrriListStop
	.section	.bss.GuiVar_LightIrriListStop,"aw",%nobits
	.align	2
	.type	GuiVar_LightIrriListStop, %object
	.size	GuiVar_LightIrriListStop, 33
GuiVar_LightIrriListStop:
	.space	33
	.global	GuiVar_LightIsEnergized
	.section	.bss.GuiVar_LightIsEnergized,"aw",%nobits
	.align	2
	.type	GuiVar_LightIsEnergized, %object
	.size	GuiVar_LightIsEnergized, 4
GuiVar_LightIsEnergized:
	.space	4
	.global	GuiVar_LightIsPhysicallyAvailable
	.section	.bss.GuiVar_LightIsPhysicallyAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_LightIsPhysicallyAvailable, %object
	.size	GuiVar_LightIsPhysicallyAvailable, 4
GuiVar_LightIsPhysicallyAvailable:
	.space	4
	.global	GuiVar_LightRemainingMinutes
	.section	.bss.GuiVar_LightRemainingMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightRemainingMinutes, %object
	.size	GuiVar_LightRemainingMinutes, 4
GuiVar_LightRemainingMinutes:
	.space	4
	.global	GuiVar_LightsAreEnergized
	.section	.bss.GuiVar_LightsAreEnergized,"aw",%nobits
	.align	2
	.type	GuiVar_LightsAreEnergized, %object
	.size	GuiVar_LightsAreEnergized, 4
GuiVar_LightsAreEnergized:
	.space	4
	.global	GuiVar_LightsBoxIndex_0
	.section	.bss.GuiVar_LightsBoxIndex_0,"aw",%nobits
	.align	2
	.type	GuiVar_LightsBoxIndex_0, %object
	.size	GuiVar_LightsBoxIndex_0, 4
GuiVar_LightsBoxIndex_0:
	.space	4
	.global	GuiVar_LightsCalendarHighlightXOffset
	.section	.bss.GuiVar_LightsCalendarHighlightXOffset,"aw",%nobits
	.align	2
	.type	GuiVar_LightsCalendarHighlightXOffset, %object
	.size	GuiVar_LightsCalendarHighlightXOffset, 4
GuiVar_LightsCalendarHighlightXOffset:
	.space	4
	.global	GuiVar_LightsCalendarHighlightYOffset
	.section	.bss.GuiVar_LightsCalendarHighlightYOffset,"aw",%nobits
	.align	2
	.type	GuiVar_LightsCalendarHighlightYOffset, %object
	.size	GuiVar_LightsCalendarHighlightYOffset, 4
GuiVar_LightsCalendarHighlightYOffset:
	.space	4
	.global	GuiVar_LightScheduledStart
	.section	.bss.GuiVar_LightScheduledStart,"aw",%nobits
	.align	2
	.type	GuiVar_LightScheduledStart, %object
	.size	GuiVar_LightScheduledStart, 33
GuiVar_LightScheduledStart:
	.space	33
	.global	GuiVar_LightScheduledStop
	.section	.bss.GuiVar_LightScheduledStop,"aw",%nobits
	.align	2
	.type	GuiVar_LightScheduledStop, %object
	.size	GuiVar_LightScheduledStop, 33
GuiVar_LightScheduledStop:
	.space	33
	.global	GuiVar_LightScheduleState
	.section	.bss.GuiVar_LightScheduleState,"aw",%nobits
	.align	2
	.type	GuiVar_LightScheduleState, %object
	.size	GuiVar_LightScheduleState, 4
GuiVar_LightScheduleState:
	.space	4
	.global	GuiVar_LightsDate
	.section	.bss.GuiVar_LightsDate,"aw",%nobits
	.align	2
	.type	GuiVar_LightsDate, %object
	.size	GuiVar_LightsDate, 49
GuiVar_LightsDate:
	.space	49
	.global	GuiVar_LightsOutput
	.section	.bss.GuiVar_LightsOutput,"aw",%nobits
	.align	2
	.type	GuiVar_LightsOutput, %object
	.size	GuiVar_LightsOutput, 4
GuiVar_LightsOutput:
	.space	4
	.global	GuiVar_LightsOutputIndex_0
	.section	.bss.GuiVar_LightsOutputIndex_0,"aw",%nobits
	.align	2
	.type	GuiVar_LightsOutputIndex_0, %object
	.size	GuiVar_LightsOutputIndex_0, 4
GuiVar_LightsOutputIndex_0:
	.space	4
	.global	GuiVar_LightsOutputIsDeenergized
	.section	.bss.GuiVar_LightsOutputIsDeenergized,"aw",%nobits
	.align	2
	.type	GuiVar_LightsOutputIsDeenergized, %object
	.size	GuiVar_LightsOutputIsDeenergized, 4
GuiVar_LightsOutputIsDeenergized:
	.space	4
	.global	GuiVar_LightsStartTime1InMinutes
	.section	.bss.GuiVar_LightsStartTime1InMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStartTime1InMinutes, %object
	.size	GuiVar_LightsStartTime1InMinutes, 4
GuiVar_LightsStartTime1InMinutes:
	.space	4
	.global	GuiVar_LightsStartTime2InMinutes
	.section	.bss.GuiVar_LightsStartTime2InMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStartTime2InMinutes, %object
	.size	GuiVar_LightsStartTime2InMinutes, 4
GuiVar_LightsStartTime2InMinutes:
	.space	4
	.global	GuiVar_LightsStartTimeEnabled1
	.section	.bss.GuiVar_LightsStartTimeEnabled1,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStartTimeEnabled1, %object
	.size	GuiVar_LightsStartTimeEnabled1, 4
GuiVar_LightsStartTimeEnabled1:
	.space	4
	.global	GuiVar_LightsStartTimeEnabled2
	.section	.bss.GuiVar_LightsStartTimeEnabled2,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStartTimeEnabled2, %object
	.size	GuiVar_LightsStartTimeEnabled2, 4
GuiVar_LightsStartTimeEnabled2:
	.space	4
	.global	GuiVar_LightsStopTime1InMinutes
	.section	.bss.GuiVar_LightsStopTime1InMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTime1InMinutes, %object
	.size	GuiVar_LightsStopTime1InMinutes, 4
GuiVar_LightsStopTime1InMinutes:
	.space	4
	.global	GuiVar_LightsStopTime2InMinutes
	.section	.bss.GuiVar_LightsStopTime2InMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTime2InMinutes, %object
	.size	GuiVar_LightsStopTime2InMinutes, 4
GuiVar_LightsStopTime2InMinutes:
	.space	4
	.global	GuiVar_LightsStopTimeEnabled1
	.section	.bss.GuiVar_LightsStopTimeEnabled1,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTimeEnabled1, %object
	.size	GuiVar_LightsStopTimeEnabled1, 4
GuiVar_LightsStopTimeEnabled1:
	.space	4
	.global	GuiVar_LightsStopTimeEnabled2
	.section	.bss.GuiVar_LightsStopTimeEnabled2,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTimeEnabled2, %object
	.size	GuiVar_LightsStopTimeEnabled2, 4
GuiVar_LightsStopTimeEnabled2:
	.space	4
	.global	GuiVar_LightsStopTimeHasBeenEdited
	.section	.bss.GuiVar_LightsStopTimeHasBeenEdited,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTimeHasBeenEdited, %object
	.size	GuiVar_LightsStopTimeHasBeenEdited, 4
GuiVar_LightsStopTimeHasBeenEdited:
	.space	4
	.global	GuiVar_LightStartReason
	.section	.bss.GuiVar_LightStartReason,"aw",%nobits
	.align	2
	.type	GuiVar_LightStartReason, %object
	.size	GuiVar_LightStartReason, 4
GuiVar_LightStartReason:
	.space	4
	.global	GuiVar_LiveScreensBypassFlowAvgFlow
	.section	.bss.GuiVar_LiveScreensBypassFlowAvgFlow,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowAvgFlow, %object
	.size	GuiVar_LiveScreensBypassFlowAvgFlow, 4
GuiVar_LiveScreensBypassFlowAvgFlow:
	.space	4
	.global	GuiVar_LiveScreensBypassFlowFlow
	.section	.bss.GuiVar_LiveScreensBypassFlowFlow,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowFlow, %object
	.size	GuiVar_LiveScreensBypassFlowFlow, 4
GuiVar_LiveScreensBypassFlowFlow:
	.space	4
	.global	GuiVar_LiveScreensBypassFlowLevel
	.section	.bss.GuiVar_LiveScreensBypassFlowLevel,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowLevel, %object
	.size	GuiVar_LiveScreensBypassFlowLevel, 4
GuiVar_LiveScreensBypassFlowLevel:
	.space	4
	.global	GuiVar_LiveScreensBypassFlowMaxFlow
	.section	.bss.GuiVar_LiveScreensBypassFlowMaxFlow,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowMaxFlow, %object
	.size	GuiVar_LiveScreensBypassFlowMaxFlow, 4
GuiVar_LiveScreensBypassFlowMaxFlow:
	.space	4
	.global	GuiVar_LiveScreensBypassFlowMVOpen
	.section	.bss.GuiVar_LiveScreensBypassFlowMVOpen,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowMVOpen, %object
	.size	GuiVar_LiveScreensBypassFlowMVOpen, 4
GuiVar_LiveScreensBypassFlowMVOpen:
	.space	4
	.global	GuiVar_LiveScreensBypassFlowShowMaxFlow
	.section	.bss.GuiVar_LiveScreensBypassFlowShowMaxFlow,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowShowMaxFlow, %object
	.size	GuiVar_LiveScreensBypassFlowShowMaxFlow, 4
GuiVar_LiveScreensBypassFlowShowMaxFlow:
	.space	4
	.global	GuiVar_LiveScreensCommCentralConnected
	.section	.bss.GuiVar_LiveScreensCommCentralConnected,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommCentralConnected, %object
	.size	GuiVar_LiveScreensCommCentralConnected, 4
GuiVar_LiveScreensCommCentralConnected:
	.space	4
	.global	GuiVar_LiveScreensCommCentralEnabled
	.section	.bss.GuiVar_LiveScreensCommCentralEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommCentralEnabled, %object
	.size	GuiVar_LiveScreensCommCentralEnabled, 4
GuiVar_LiveScreensCommCentralEnabled:
	.space	4
	.global	GuiVar_LiveScreensCommFLCommMngrMode
	.section	.bss.GuiVar_LiveScreensCommFLCommMngrMode,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommFLCommMngrMode, %object
	.size	GuiVar_LiveScreensCommFLCommMngrMode, 4
GuiVar_LiveScreensCommFLCommMngrMode:
	.space	4
	.global	GuiVar_LiveScreensCommFLEnabled
	.section	.bss.GuiVar_LiveScreensCommFLEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommFLEnabled, %object
	.size	GuiVar_LiveScreensCommFLEnabled, 4
GuiVar_LiveScreensCommFLEnabled:
	.space	4
	.global	GuiVar_LiveScreensCommFLInForced
	.section	.bss.GuiVar_LiveScreensCommFLInForced,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommFLInForced, %object
	.size	GuiVar_LiveScreensCommFLInForced, 4
GuiVar_LiveScreensCommFLInForced:
	.space	4
	.global	GuiVar_LiveScreensCommTPMicroConnected
	.section	.bss.GuiVar_LiveScreensCommTPMicroConnected,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommTPMicroConnected, %object
	.size	GuiVar_LiveScreensCommTPMicroConnected, 4
GuiVar_LiveScreensCommTPMicroConnected:
	.space	4
	.global	GuiVar_LiveScreensElectricalControllerName
	.section	.bss.GuiVar_LiveScreensElectricalControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensElectricalControllerName, %object
	.size	GuiVar_LiveScreensElectricalControllerName, 49
GuiVar_LiveScreensElectricalControllerName:
	.space	49
	.global	GuiVar_LiveScreensPOCFlowMVState
	.section	.bss.GuiVar_LiveScreensPOCFlowMVState,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensPOCFlowMVState, %object
	.size	GuiVar_LiveScreensPOCFlowMVState, 4
GuiVar_LiveScreensPOCFlowMVState:
	.space	4
	.global	GuiVar_LiveScreensPOCFlowName
	.section	.bss.GuiVar_LiveScreensPOCFlowName,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensPOCFlowName, %object
	.size	GuiVar_LiveScreensPOCFlowName, 49
GuiVar_LiveScreensPOCFlowName:
	.space	49
	.global	GuiVar_LiveScreensSystemMLBExists
	.section	.bss.GuiVar_LiveScreensSystemMLBExists,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensSystemMLBExists, %object
	.size	GuiVar_LiveScreensSystemMLBExists, 4
GuiVar_LiveScreensSystemMLBExists:
	.space	4
	.global	GuiVar_LiveScreensSystemMVORExists
	.section	.bss.GuiVar_LiveScreensSystemMVORExists,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensSystemMVORExists, %object
	.size	GuiVar_LiveScreensSystemMVORExists, 4
GuiVar_LiveScreensSystemMVORExists:
	.space	4
	.global	GuiVar_LRBeaconRate
	.section	.bss.GuiVar_LRBeaconRate,"aw",%nobits
	.align	2
	.type	GuiVar_LRBeaconRate, %object
	.size	GuiVar_LRBeaconRate, 4
GuiVar_LRBeaconRate:
	.space	4
	.global	GuiVar_LRFirmwareVer
	.section	.bss.GuiVar_LRFirmwareVer,"aw",%nobits
	.align	2
	.type	GuiVar_LRFirmwareVer, %object
	.size	GuiVar_LRFirmwareVer, 49
GuiVar_LRFirmwareVer:
	.space	49
	.global	GuiVar_LRFrequency_Decimal
	.section	.bss.GuiVar_LRFrequency_Decimal,"aw",%nobits
	.align	2
	.type	GuiVar_LRFrequency_Decimal, %object
	.size	GuiVar_LRFrequency_Decimal, 4
GuiVar_LRFrequency_Decimal:
	.space	4
	.global	GuiVar_LRFrequency_WholeNum
	.section	.bss.GuiVar_LRFrequency_WholeNum,"aw",%nobits
	.align	2
	.type	GuiVar_LRFrequency_WholeNum, %object
	.size	GuiVar_LRFrequency_WholeNum, 4
GuiVar_LRFrequency_WholeNum:
	.space	4
	.global	GuiVar_LRGroup
	.section	.bss.GuiVar_LRGroup,"aw",%nobits
	.align	2
	.type	GuiVar_LRGroup, %object
	.size	GuiVar_LRGroup, 4
GuiVar_LRGroup:
	.space	4
	.global	GuiVar_LRHubFeatureNotAvailable
	.section	.bss.GuiVar_LRHubFeatureNotAvailable,"aw",%nobits
	.type	GuiVar_LRHubFeatureNotAvailable, %object
	.size	GuiVar_LRHubFeatureNotAvailable, 1
GuiVar_LRHubFeatureNotAvailable:
	.space	1
	.global	GuiVar_LRMode
	.section	.bss.GuiVar_LRMode,"aw",%nobits
	.align	2
	.type	GuiVar_LRMode, %object
	.size	GuiVar_LRMode, 4
GuiVar_LRMode:
	.space	4
	.global	GuiVar_LRPort
	.section	.bss.GuiVar_LRPort,"aw",%nobits
	.align	2
	.type	GuiVar_LRPort, %object
	.size	GuiVar_LRPort, 4
GuiVar_LRPort:
	.space	4
	.global	GuiVar_LRRadioType
	.section	.bss.GuiVar_LRRadioType,"aw",%nobits
	.align	2
	.type	GuiVar_LRRadioType, %object
	.size	GuiVar_LRRadioType, 4
GuiVar_LRRadioType:
	.space	4
	.global	GuiVar_LRRepeaterInUse
	.section	.bss.GuiVar_LRRepeaterInUse,"aw",%nobits
	.align	2
	.type	GuiVar_LRRepeaterInUse, %object
	.size	GuiVar_LRRepeaterInUse, 4
GuiVar_LRRepeaterInUse:
	.space	4
	.global	GuiVar_LRSerialNumber
	.section	.bss.GuiVar_LRSerialNumber,"aw",%nobits
	.align	2
	.type	GuiVar_LRSerialNumber, %object
	.size	GuiVar_LRSerialNumber, 9
GuiVar_LRSerialNumber:
	.space	9
	.global	GuiVar_LRSlaveLinksToRepeater
	.section	.bss.GuiVar_LRSlaveLinksToRepeater,"aw",%nobits
	.align	2
	.type	GuiVar_LRSlaveLinksToRepeater, %object
	.size	GuiVar_LRSlaveLinksToRepeater, 4
GuiVar_LRSlaveLinksToRepeater:
	.space	4
	.global	GuiVar_LRTemperature
	.section	.bss.GuiVar_LRTemperature,"aw",%nobits
	.align	2
	.type	GuiVar_LRTemperature, %object
	.size	GuiVar_LRTemperature, 4
GuiVar_LRTemperature:
	.space	4
	.global	GuiVar_LRTransmitPower
	.section	.bss.GuiVar_LRTransmitPower,"aw",%nobits
	.align	2
	.type	GuiVar_LRTransmitPower, %object
	.size	GuiVar_LRTransmitPower, 4
GuiVar_LRTransmitPower:
	.space	4
	.global	GuiVar_MainMenu2WireOptionsExist
	.section	.bss.GuiVar_MainMenu2WireOptionsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenu2WireOptionsExist, %object
	.size	GuiVar_MainMenu2WireOptionsExist, 4
GuiVar_MainMenu2WireOptionsExist:
	.space	4
	.global	GuiVar_MainMenuAquaponicsMode
	.section	.bss.GuiVar_MainMenuAquaponicsMode,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuAquaponicsMode, %object
	.size	GuiVar_MainMenuAquaponicsMode, 4
GuiVar_MainMenuAquaponicsMode:
	.space	4
	.global	GuiVar_MainMenuBudgetsInUse
	.section	.bss.GuiVar_MainMenuBudgetsInUse,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuBudgetsInUse, %object
	.size	GuiVar_MainMenuBudgetsInUse, 4
GuiVar_MainMenuBudgetsInUse:
	.space	4
	.global	GuiVar_MainMenuBypassExists
	.section	.bss.GuiVar_MainMenuBypassExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuBypassExists, %object
	.size	GuiVar_MainMenuBypassExists, 4
GuiVar_MainMenuBypassExists:
	.space	4
	.global	GuiVar_MainMenuCommOptionExists
	.section	.bss.GuiVar_MainMenuCommOptionExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuCommOptionExists, %object
	.size	GuiVar_MainMenuCommOptionExists, 4
GuiVar_MainMenuCommOptionExists:
	.space	4
	.global	GuiVar_MainMenuFLOptionExists
	.section	.bss.GuiVar_MainMenuFLOptionExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuFLOptionExists, %object
	.size	GuiVar_MainMenuFLOptionExists, 4
GuiVar_MainMenuFLOptionExists:
	.space	4
	.global	GuiVar_MainMenuFlowCheckingInUse
	.section	.bss.GuiVar_MainMenuFlowCheckingInUse,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuFlowCheckingInUse, %object
	.size	GuiVar_MainMenuFlowCheckingInUse, 4
GuiVar_MainMenuFlowCheckingInUse:
	.space	4
	.global	GuiVar_MainMenuFlowMetersExist
	.section	.bss.GuiVar_MainMenuFlowMetersExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuFlowMetersExist, %object
	.size	GuiVar_MainMenuFlowMetersExist, 4
GuiVar_MainMenuFlowMetersExist:
	.space	4
	.global	GuiVar_MainMenuHubOptionExists
	.section	.bss.GuiVar_MainMenuHubOptionExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuHubOptionExists, %object
	.size	GuiVar_MainMenuHubOptionExists, 4
GuiVar_MainMenuHubOptionExists:
	.space	4
	.global	GuiVar_MainMenuLightsOptionsExist
	.section	.bss.GuiVar_MainMenuLightsOptionsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuLightsOptionsExist, %object
	.size	GuiVar_MainMenuLightsOptionsExist, 4
GuiVar_MainMenuLightsOptionsExist:
	.space	4
	.global	GuiVar_MainMenuMainLinesAvailable
	.section	.bss.GuiVar_MainMenuMainLinesAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuMainLinesAvailable, %object
	.size	GuiVar_MainMenuMainLinesAvailable, 4
GuiVar_MainMenuMainLinesAvailable:
	.space	4
	.global	GuiVar_MainMenuManualAvailable
	.section	.bss.GuiVar_MainMenuManualAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuManualAvailable, %object
	.size	GuiVar_MainMenuManualAvailable, 4
GuiVar_MainMenuManualAvailable:
	.space	4
	.global	GuiVar_MainMenuMoisSensorExists
	.section	.bss.GuiVar_MainMenuMoisSensorExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuMoisSensorExists, %object
	.size	GuiVar_MainMenuMoisSensorExists, 4
GuiVar_MainMenuMoisSensorExists:
	.space	4
	.global	GuiVar_MainMenuPOCsExist
	.section	.bss.GuiVar_MainMenuPOCsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuPOCsExist, %object
	.size	GuiVar_MainMenuPOCsExist, 4
GuiVar_MainMenuPOCsExist:
	.space	4
	.global	GuiVar_MainMenuStationsExist
	.section	.bss.GuiVar_MainMenuStationsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuStationsExist, %object
	.size	GuiVar_MainMenuStationsExist, 4
GuiVar_MainMenuStationsExist:
	.space	4
	.global	GuiVar_MainMenuWeatherDeviceExists
	.section	.bss.GuiVar_MainMenuWeatherDeviceExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuWeatherDeviceExists, %object
	.size	GuiVar_MainMenuWeatherDeviceExists, 4
GuiVar_MainMenuWeatherDeviceExists:
	.space	4
	.global	GuiVar_MainMenuWeatherOptionsExist
	.section	.bss.GuiVar_MainMenuWeatherOptionsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuWeatherOptionsExist, %object
	.size	GuiVar_MainMenuWeatherOptionsExist, 4
GuiVar_MainMenuWeatherOptionsExist:
	.space	4
	.global	GuiVar_ManualPEndDateStr
	.section	.bss.GuiVar_ManualPEndDateStr,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPEndDateStr, %object
	.size	GuiVar_ManualPEndDateStr, 49
GuiVar_ManualPEndDateStr:
	.space	49
	.global	GuiVar_ManualPInUse
	.section	.bss.GuiVar_ManualPInUse,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPInUse, %object
	.size	GuiVar_ManualPInUse, 4
GuiVar_ManualPInUse:
	.space	4
	.global	GuiVar_ManualPRunTime
	.section	.bss.GuiVar_ManualPRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPRunTime, %object
	.size	GuiVar_ManualPRunTime, 4
GuiVar_ManualPRunTime:
	.space	4
	.global	GuiVar_ManualPRunTimeSingle
	.section	.bss.GuiVar_ManualPRunTimeSingle,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPRunTimeSingle, %object
	.size	GuiVar_ManualPRunTimeSingle, 4
GuiVar_ManualPRunTimeSingle:
	.space	4
	.global	GuiVar_ManualPStartDateStr
	.section	.bss.GuiVar_ManualPStartDateStr,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartDateStr, %object
	.size	GuiVar_ManualPStartDateStr, 49
GuiVar_ManualPStartDateStr:
	.space	49
	.global	GuiVar_ManualPStartTime1
	.section	.bss.GuiVar_ManualPStartTime1,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime1, %object
	.size	GuiVar_ManualPStartTime1, 4
GuiVar_ManualPStartTime1:
	.space	4
	.global	GuiVar_ManualPStartTime1Enabled
	.section	.bss.GuiVar_ManualPStartTime1Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime1Enabled, %object
	.size	GuiVar_ManualPStartTime1Enabled, 4
GuiVar_ManualPStartTime1Enabled:
	.space	4
	.global	GuiVar_ManualPStartTime2
	.section	.bss.GuiVar_ManualPStartTime2,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime2, %object
	.size	GuiVar_ManualPStartTime2, 4
GuiVar_ManualPStartTime2:
	.space	4
	.global	GuiVar_ManualPStartTime2Enabled
	.section	.bss.GuiVar_ManualPStartTime2Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime2Enabled, %object
	.size	GuiVar_ManualPStartTime2Enabled, 4
GuiVar_ManualPStartTime2Enabled:
	.space	4
	.global	GuiVar_ManualPStartTime3
	.section	.bss.GuiVar_ManualPStartTime3,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime3, %object
	.size	GuiVar_ManualPStartTime3, 4
GuiVar_ManualPStartTime3:
	.space	4
	.global	GuiVar_ManualPStartTime3Enabled
	.section	.bss.GuiVar_ManualPStartTime3Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime3Enabled, %object
	.size	GuiVar_ManualPStartTime3Enabled, 4
GuiVar_ManualPStartTime3Enabled:
	.space	4
	.global	GuiVar_ManualPStartTime4
	.section	.bss.GuiVar_ManualPStartTime4,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime4, %object
	.size	GuiVar_ManualPStartTime4, 4
GuiVar_ManualPStartTime4:
	.space	4
	.global	GuiVar_ManualPStartTime4Enabled
	.section	.bss.GuiVar_ManualPStartTime4Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime4Enabled, %object
	.size	GuiVar_ManualPStartTime4Enabled, 4
GuiVar_ManualPStartTime4Enabled:
	.space	4
	.global	GuiVar_ManualPStartTime5
	.section	.bss.GuiVar_ManualPStartTime5,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime5, %object
	.size	GuiVar_ManualPStartTime5, 4
GuiVar_ManualPStartTime5:
	.space	4
	.global	GuiVar_ManualPStartTime5Enabled
	.section	.bss.GuiVar_ManualPStartTime5Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime5Enabled, %object
	.size	GuiVar_ManualPStartTime5Enabled, 4
GuiVar_ManualPStartTime5Enabled:
	.space	4
	.global	GuiVar_ManualPStartTime6
	.section	.bss.GuiVar_ManualPStartTime6,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime6, %object
	.size	GuiVar_ManualPStartTime6, 4
GuiVar_ManualPStartTime6:
	.space	4
	.global	GuiVar_ManualPStartTime6Enabled
	.section	.bss.GuiVar_ManualPStartTime6Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime6Enabled, %object
	.size	GuiVar_ManualPStartTime6Enabled, 4
GuiVar_ManualPStartTime6Enabled:
	.space	4
	.global	GuiVar_ManualPStationRunTime
	.section	.bss.GuiVar_ManualPStationRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStationRunTime, %object
	.size	GuiVar_ManualPStationRunTime, 4
GuiVar_ManualPStationRunTime:
	.space	4
	.global	GuiVar_ManualPStationRunTimeControllerName
	.section	.bss.GuiVar_ManualPStationRunTimeControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStationRunTimeControllerName, %object
	.size	GuiVar_ManualPStationRunTimeControllerName, 49
GuiVar_ManualPStationRunTimeControllerName:
	.space	49
	.global	GuiVar_ManualPStationRunTimeStationNumber
	.section	.bss.GuiVar_ManualPStationRunTimeStationNumber,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStationRunTimeStationNumber, %object
	.size	GuiVar_ManualPStationRunTimeStationNumber, 4
GuiVar_ManualPStationRunTimeStationNumber:
	.space	4
	.global	GuiVar_ManualPWaterDay_Fri
	.section	.bss.GuiVar_ManualPWaterDay_Fri,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Fri, %object
	.size	GuiVar_ManualPWaterDay_Fri, 4
GuiVar_ManualPWaterDay_Fri:
	.space	4
	.global	GuiVar_ManualPWaterDay_Mon
	.section	.bss.GuiVar_ManualPWaterDay_Mon,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Mon, %object
	.size	GuiVar_ManualPWaterDay_Mon, 4
GuiVar_ManualPWaterDay_Mon:
	.space	4
	.global	GuiVar_ManualPWaterDay_Sat
	.section	.bss.GuiVar_ManualPWaterDay_Sat,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Sat, %object
	.size	GuiVar_ManualPWaterDay_Sat, 4
GuiVar_ManualPWaterDay_Sat:
	.space	4
	.global	GuiVar_ManualPWaterDay_Sun
	.section	.bss.GuiVar_ManualPWaterDay_Sun,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Sun, %object
	.size	GuiVar_ManualPWaterDay_Sun, 4
GuiVar_ManualPWaterDay_Sun:
	.space	4
	.global	GuiVar_ManualPWaterDay_Thu
	.section	.bss.GuiVar_ManualPWaterDay_Thu,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Thu, %object
	.size	GuiVar_ManualPWaterDay_Thu, 4
GuiVar_ManualPWaterDay_Thu:
	.space	4
	.global	GuiVar_ManualPWaterDay_Tue
	.section	.bss.GuiVar_ManualPWaterDay_Tue,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Tue, %object
	.size	GuiVar_ManualPWaterDay_Tue, 4
GuiVar_ManualPWaterDay_Tue:
	.space	4
	.global	GuiVar_ManualPWaterDay_Wed
	.section	.bss.GuiVar_ManualPWaterDay_Wed,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Wed, %object
	.size	GuiVar_ManualPWaterDay_Wed, 4
GuiVar_ManualPWaterDay_Wed:
	.space	4
	.global	GuiVar_ManualPWillNotRunReason
	.section	.bss.GuiVar_ManualPWillNotRunReason,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWillNotRunReason, %object
	.size	GuiVar_ManualPWillNotRunReason, 4
GuiVar_ManualPWillNotRunReason:
	.space	4
	.global	GuiVar_ManualPWillRun
	.section	.bss.GuiVar_ManualPWillRun,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWillRun, %object
	.size	GuiVar_ManualPWillRun, 4
GuiVar_ManualPWillRun:
	.space	4
	.global	GuiVar_ManualWaterProg
	.section	.bss.GuiVar_ManualWaterProg,"aw",%nobits
	.align	2
	.type	GuiVar_ManualWaterProg, %object
	.size	GuiVar_ManualWaterProg, 49
GuiVar_ManualWaterProg:
	.space	49
	.global	GuiVar_ManualWaterRunTime
	.section	.bss.GuiVar_ManualWaterRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_ManualWaterRunTime, %object
	.size	GuiVar_ManualWaterRunTime, 4
GuiVar_ManualWaterRunTime:
	.space	4
	.global	GuiVar_MenuScreenToShow
	.section	.bss.GuiVar_MenuScreenToShow,"aw",%nobits
	.align	2
	.type	GuiVar_MenuScreenToShow, %object
	.size	GuiVar_MenuScreenToShow, 4
GuiVar_MenuScreenToShow:
	.space	4
	.global	GuiVar_MoisAdditionalSoakSeconds
	.section	.bss.GuiVar_MoisAdditionalSoakSeconds,"aw",%nobits
	.align	2
	.type	GuiVar_MoisAdditionalSoakSeconds, %object
	.size	GuiVar_MoisAdditionalSoakSeconds, 4
GuiVar_MoisAdditionalSoakSeconds:
	.space	4
	.global	GuiVar_MoisBoxIndex
	.section	.bss.GuiVar_MoisBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_MoisBoxIndex, %object
	.size	GuiVar_MoisBoxIndex, 4
GuiVar_MoisBoxIndex:
	.space	4
	.global	GuiVar_MoisControlMode
	.section	.bss.GuiVar_MoisControlMode,"aw",%nobits
	.align	2
	.type	GuiVar_MoisControlMode, %object
	.size	GuiVar_MoisControlMode, 4
GuiVar_MoisControlMode:
	.space	4
	.global	GuiVar_MoisDecoderSN
	.section	.bss.GuiVar_MoisDecoderSN,"aw",%nobits
	.align	2
	.type	GuiVar_MoisDecoderSN, %object
	.size	GuiVar_MoisDecoderSN, 4
GuiVar_MoisDecoderSN:
	.space	4
	.global	GuiVar_MoisEC_Current
	.section	.bss.GuiVar_MoisEC_Current,"aw",%nobits
	.align	2
	.type	GuiVar_MoisEC_Current, %object
	.size	GuiVar_MoisEC_Current, 4
GuiVar_MoisEC_Current:
	.space	4
	.global	GuiVar_MoisECAction_High
	.section	.bss.GuiVar_MoisECAction_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisECAction_High, %object
	.size	GuiVar_MoisECAction_High, 4
GuiVar_MoisECAction_High:
	.space	4
	.global	GuiVar_MoisECAction_Low
	.section	.bss.GuiVar_MoisECAction_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisECAction_Low, %object
	.size	GuiVar_MoisECAction_Low, 4
GuiVar_MoisECAction_Low:
	.space	4
	.global	GuiVar_MoisECPoint_High
	.section	.bss.GuiVar_MoisECPoint_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisECPoint_High, %object
	.size	GuiVar_MoisECPoint_High, 4
GuiVar_MoisECPoint_High:
	.space	4
	.global	GuiVar_MoisECPoint_Low
	.section	.bss.GuiVar_MoisECPoint_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisECPoint_Low, %object
	.size	GuiVar_MoisECPoint_Low, 4
GuiVar_MoisECPoint_Low:
	.space	4
	.global	GuiVar_MoisInUse
	.section	.bss.GuiVar_MoisInUse,"aw",%nobits
	.align	2
	.type	GuiVar_MoisInUse, %object
	.size	GuiVar_MoisInUse, 4
GuiVar_MoisInUse:
	.space	4
	.global	GuiVar_MoisPhysicallyAvailable
	.section	.bss.GuiVar_MoisPhysicallyAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_MoisPhysicallyAvailable, %object
	.size	GuiVar_MoisPhysicallyAvailable, 4
GuiVar_MoisPhysicallyAvailable:
	.space	4
	.global	GuiVar_MoisReading_Current
	.section	.bss.GuiVar_MoisReading_Current,"aw",%nobits
	.align	2
	.type	GuiVar_MoisReading_Current, %object
	.size	GuiVar_MoisReading_Current, 4
GuiVar_MoisReading_Current:
	.space	4
	.global	GuiVar_MoisSensorIncludesEC
	.section	.bss.GuiVar_MoisSensorIncludesEC,"aw",%nobits
	.align	2
	.type	GuiVar_MoisSensorIncludesEC, %object
	.size	GuiVar_MoisSensorIncludesEC, 4
GuiVar_MoisSensorIncludesEC:
	.space	4
	.global	GuiVar_MoisSensorName
	.section	.bss.GuiVar_MoisSensorName,"aw",%nobits
	.align	2
	.type	GuiVar_MoisSensorName, %object
	.size	GuiVar_MoisSensorName, 49
GuiVar_MoisSensorName:
	.space	49
	.global	GuiVar_MoisSetPoint_High
	.section	.bss.GuiVar_MoisSetPoint_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisSetPoint_High, %object
	.size	GuiVar_MoisSetPoint_High, 4
GuiVar_MoisSetPoint_High:
	.space	4
	.global	GuiVar_MoisSetPoint_Low
	.section	.bss.GuiVar_MoisSetPoint_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisSetPoint_Low, %object
	.size	GuiVar_MoisSetPoint_Low, 4
GuiVar_MoisSetPoint_Low:
	.space	4
	.global	GuiVar_MoisTemp_Current
	.section	.bss.GuiVar_MoisTemp_Current,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTemp_Current, %object
	.size	GuiVar_MoisTemp_Current, 4
GuiVar_MoisTemp_Current:
	.space	4
	.global	GuiVar_MoisTempAction_High
	.section	.bss.GuiVar_MoisTempAction_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTempAction_High, %object
	.size	GuiVar_MoisTempAction_High, 4
GuiVar_MoisTempAction_High:
	.space	4
	.global	GuiVar_MoisTempAction_Low
	.section	.bss.GuiVar_MoisTempAction_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTempAction_Low, %object
	.size	GuiVar_MoisTempAction_Low, 4
GuiVar_MoisTempAction_Low:
	.space	4
	.global	GuiVar_MoisTempPoint_High
	.section	.bss.GuiVar_MoisTempPoint_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTempPoint_High, %object
	.size	GuiVar_MoisTempPoint_High, 4
GuiVar_MoisTempPoint_High:
	.space	4
	.global	GuiVar_MoisTempPoint_Low
	.section	.bss.GuiVar_MoisTempPoint_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTempPoint_Low, %object
	.size	GuiVar_MoisTempPoint_Low, 4
GuiVar_MoisTempPoint_Low:
	.space	4
	.global	GuiVar_MoistureTabToDisplay
	.section	.bss.GuiVar_MoistureTabToDisplay,"aw",%nobits
	.align	2
	.type	GuiVar_MoistureTabToDisplay, %object
	.size	GuiVar_MoistureTabToDisplay, 4
GuiVar_MoistureTabToDisplay:
	.space	4
	.global	GuiVar_MVORClose0
	.section	.bss.GuiVar_MVORClose0,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose0, %object
	.size	GuiVar_MVORClose0, 4
GuiVar_MVORClose0:
	.space	4
	.global	GuiVar_MVORClose0Enabled
	.section	.bss.GuiVar_MVORClose0Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose0Enabled, %object
	.size	GuiVar_MVORClose0Enabled, 4
GuiVar_MVORClose0Enabled:
	.space	4
	.global	GuiVar_MVORClose1
	.section	.bss.GuiVar_MVORClose1,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose1, %object
	.size	GuiVar_MVORClose1, 4
GuiVar_MVORClose1:
	.space	4
	.global	GuiVar_MVORClose1Enabled
	.section	.bss.GuiVar_MVORClose1Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose1Enabled, %object
	.size	GuiVar_MVORClose1Enabled, 4
GuiVar_MVORClose1Enabled:
	.space	4
	.global	GuiVar_MVORClose2
	.section	.bss.GuiVar_MVORClose2,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose2, %object
	.size	GuiVar_MVORClose2, 4
GuiVar_MVORClose2:
	.space	4
	.global	GuiVar_MVORClose2Enabled
	.section	.bss.GuiVar_MVORClose2Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose2Enabled, %object
	.size	GuiVar_MVORClose2Enabled, 4
GuiVar_MVORClose2Enabled:
	.space	4
	.global	GuiVar_MVORClose3
	.section	.bss.GuiVar_MVORClose3,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose3, %object
	.size	GuiVar_MVORClose3, 4
GuiVar_MVORClose3:
	.space	4
	.global	GuiVar_MVORClose3Enabled
	.section	.bss.GuiVar_MVORClose3Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose3Enabled, %object
	.size	GuiVar_MVORClose3Enabled, 4
GuiVar_MVORClose3Enabled:
	.space	4
	.global	GuiVar_MVORClose4
	.section	.bss.GuiVar_MVORClose4,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose4, %object
	.size	GuiVar_MVORClose4, 4
GuiVar_MVORClose4:
	.space	4
	.global	GuiVar_MVORClose4Enabled
	.section	.bss.GuiVar_MVORClose4Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose4Enabled, %object
	.size	GuiVar_MVORClose4Enabled, 4
GuiVar_MVORClose4Enabled:
	.space	4
	.global	GuiVar_MVORClose5
	.section	.bss.GuiVar_MVORClose5,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose5, %object
	.size	GuiVar_MVORClose5, 4
GuiVar_MVORClose5:
	.space	4
	.global	GuiVar_MVORClose5Enabled
	.section	.bss.GuiVar_MVORClose5Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose5Enabled, %object
	.size	GuiVar_MVORClose5Enabled, 4
GuiVar_MVORClose5Enabled:
	.space	4
	.global	GuiVar_MVORClose6
	.section	.bss.GuiVar_MVORClose6,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose6, %object
	.size	GuiVar_MVORClose6, 4
GuiVar_MVORClose6:
	.space	4
	.global	GuiVar_MVORClose6Enabled
	.section	.bss.GuiVar_MVORClose6Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose6Enabled, %object
	.size	GuiVar_MVORClose6Enabled, 4
GuiVar_MVORClose6Enabled:
	.space	4
	.global	GuiVar_MVORInEffect
	.section	.bss.GuiVar_MVORInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_MVORInEffect, %object
	.size	GuiVar_MVORInEffect, 4
GuiVar_MVORInEffect:
	.space	4
	.global	GuiVar_MVOROpen0
	.section	.bss.GuiVar_MVOROpen0,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen0, %object
	.size	GuiVar_MVOROpen0, 4
GuiVar_MVOROpen0:
	.space	4
	.global	GuiVar_MVOROpen0Enabled
	.section	.bss.GuiVar_MVOROpen0Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen0Enabled, %object
	.size	GuiVar_MVOROpen0Enabled, 4
GuiVar_MVOROpen0Enabled:
	.space	4
	.global	GuiVar_MVOROpen1
	.section	.bss.GuiVar_MVOROpen1,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen1, %object
	.size	GuiVar_MVOROpen1, 4
GuiVar_MVOROpen1:
	.space	4
	.global	GuiVar_MVOROpen1Enabled
	.section	.bss.GuiVar_MVOROpen1Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen1Enabled, %object
	.size	GuiVar_MVOROpen1Enabled, 4
GuiVar_MVOROpen1Enabled:
	.space	4
	.global	GuiVar_MVOROpen2
	.section	.bss.GuiVar_MVOROpen2,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen2, %object
	.size	GuiVar_MVOROpen2, 4
GuiVar_MVOROpen2:
	.space	4
	.global	GuiVar_MVOROpen2Enabled
	.section	.bss.GuiVar_MVOROpen2Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen2Enabled, %object
	.size	GuiVar_MVOROpen2Enabled, 4
GuiVar_MVOROpen2Enabled:
	.space	4
	.global	GuiVar_MVOROpen3
	.section	.bss.GuiVar_MVOROpen3,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen3, %object
	.size	GuiVar_MVOROpen3, 4
GuiVar_MVOROpen3:
	.space	4
	.global	GuiVar_MVOROpen3Enabled
	.section	.bss.GuiVar_MVOROpen3Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen3Enabled, %object
	.size	GuiVar_MVOROpen3Enabled, 4
GuiVar_MVOROpen3Enabled:
	.space	4
	.global	GuiVar_MVOROpen4
	.section	.bss.GuiVar_MVOROpen4,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen4, %object
	.size	GuiVar_MVOROpen4, 4
GuiVar_MVOROpen4:
	.space	4
	.global	GuiVar_MVOROpen4Enabled
	.section	.bss.GuiVar_MVOROpen4Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen4Enabled, %object
	.size	GuiVar_MVOROpen4Enabled, 4
GuiVar_MVOROpen4Enabled:
	.space	4
	.global	GuiVar_MVOROpen5
	.section	.bss.GuiVar_MVOROpen5,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen5, %object
	.size	GuiVar_MVOROpen5, 4
GuiVar_MVOROpen5:
	.space	4
	.global	GuiVar_MVOROpen5Enabled
	.section	.bss.GuiVar_MVOROpen5Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen5Enabled, %object
	.size	GuiVar_MVOROpen5Enabled, 4
GuiVar_MVOROpen5Enabled:
	.space	4
	.global	GuiVar_MVOROpen6
	.section	.bss.GuiVar_MVOROpen6,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen6, %object
	.size	GuiVar_MVOROpen6, 4
GuiVar_MVOROpen6:
	.space	4
	.global	GuiVar_MVOROpen6Enabled
	.section	.bss.GuiVar_MVOROpen6Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen6Enabled, %object
	.size	GuiVar_MVOROpen6Enabled, 4
GuiVar_MVOROpen6Enabled:
	.space	4
	.global	GuiVar_MVORRunTime
	.section	.bss.GuiVar_MVORRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_MVORRunTime, %object
	.size	GuiVar_MVORRunTime, 4
GuiVar_MVORRunTime:
	.space	4
	.global	GuiVar_MVORState
	.section	.bss.GuiVar_MVORState,"aw",%nobits
	.align	2
	.type	GuiVar_MVORState, %object
	.size	GuiVar_MVORState, 4
GuiVar_MVORState:
	.space	4
	.global	GuiVar_MVORTimeRemaining
	.section	.bss.GuiVar_MVORTimeRemaining,"aw",%nobits
	.align	2
	.type	GuiVar_MVORTimeRemaining, %object
	.size	GuiVar_MVORTimeRemaining, 4
GuiVar_MVORTimeRemaining:
	.space	4
	.global	GuiVar_NetworkAvailable
	.section	.bss.GuiVar_NetworkAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_NetworkAvailable, %object
	.size	GuiVar_NetworkAvailable, 4
GuiVar_NetworkAvailable:
	.space	4
	.global	GuiVar_NoWaterDaysDays
	.section	.bss.GuiVar_NoWaterDaysDays,"aw",%nobits
	.align	2
	.type	GuiVar_NoWaterDaysDays, %object
	.size	GuiVar_NoWaterDaysDays, 4
GuiVar_NoWaterDaysDays:
	.space	4
	.global	GuiVar_NoWaterDaysProg
	.section	.bss.GuiVar_NoWaterDaysProg,"aw",%nobits
	.align	2
	.type	GuiVar_NoWaterDaysProg, %object
	.size	GuiVar_NoWaterDaysProg, 49
GuiVar_NoWaterDaysProg:
	.space	49
	.global	GuiVar_NumericKeypadShowDecimalPoint
	.section	.bss.GuiVar_NumericKeypadShowDecimalPoint,"aw",%nobits
	.align	2
	.type	GuiVar_NumericKeypadShowDecimalPoint, %object
	.size	GuiVar_NumericKeypadShowDecimalPoint, 4
GuiVar_NumericKeypadShowDecimalPoint:
	.space	4
	.global	GuiVar_NumericKeypadShowPlusMinus
	.section	.bss.GuiVar_NumericKeypadShowPlusMinus,"aw",%nobits
	.align	2
	.type	GuiVar_NumericKeypadShowPlusMinus, %object
	.size	GuiVar_NumericKeypadShowPlusMinus, 4
GuiVar_NumericKeypadShowPlusMinus:
	.space	4
	.global	GuiVar_NumericKeypadValue
	.section	.bss.GuiVar_NumericKeypadValue,"aw",%nobits
	.align	2
	.type	GuiVar_NumericKeypadValue, %object
	.size	GuiVar_NumericKeypadValue, 11
GuiVar_NumericKeypadValue:
	.space	11
	.global	GuiVar_OnAtATimeInfoDisplay
	.section	.bss.GuiVar_OnAtATimeInfoDisplay,"aw",%nobits
	.type	GuiVar_OnAtATimeInfoDisplay, %object
	.size	GuiVar_OnAtATimeInfoDisplay, 1
GuiVar_OnAtATimeInfoDisplay:
	.space	1
	.global	GuiVar_PercentAdjustEndDate_0
	.section	.bss.GuiVar_PercentAdjustEndDate_0,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_0, %object
	.size	GuiVar_PercentAdjustEndDate_0, 49
GuiVar_PercentAdjustEndDate_0:
	.space	49
	.global	GuiVar_PercentAdjustEndDate_1
	.section	.bss.GuiVar_PercentAdjustEndDate_1,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_1, %object
	.size	GuiVar_PercentAdjustEndDate_1, 49
GuiVar_PercentAdjustEndDate_1:
	.space	49
	.global	GuiVar_PercentAdjustEndDate_2
	.section	.bss.GuiVar_PercentAdjustEndDate_2,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_2, %object
	.size	GuiVar_PercentAdjustEndDate_2, 49
GuiVar_PercentAdjustEndDate_2:
	.space	49
	.global	GuiVar_PercentAdjustEndDate_3
	.section	.bss.GuiVar_PercentAdjustEndDate_3,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_3, %object
	.size	GuiVar_PercentAdjustEndDate_3, 49
GuiVar_PercentAdjustEndDate_3:
	.space	49
	.global	GuiVar_PercentAdjustEndDate_4
	.section	.bss.GuiVar_PercentAdjustEndDate_4,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_4, %object
	.size	GuiVar_PercentAdjustEndDate_4, 49
GuiVar_PercentAdjustEndDate_4:
	.space	49
	.global	GuiVar_PercentAdjustEndDate_5
	.section	.bss.GuiVar_PercentAdjustEndDate_5,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_5, %object
	.size	GuiVar_PercentAdjustEndDate_5, 49
GuiVar_PercentAdjustEndDate_5:
	.space	49
	.global	GuiVar_PercentAdjustEndDate_6
	.section	.bss.GuiVar_PercentAdjustEndDate_6,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_6, %object
	.size	GuiVar_PercentAdjustEndDate_6, 49
GuiVar_PercentAdjustEndDate_6:
	.space	49
	.global	GuiVar_PercentAdjustEndDate_7
	.section	.bss.GuiVar_PercentAdjustEndDate_7,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_7, %object
	.size	GuiVar_PercentAdjustEndDate_7, 49
GuiVar_PercentAdjustEndDate_7:
	.space	49
	.global	GuiVar_PercentAdjustEndDate_8
	.section	.bss.GuiVar_PercentAdjustEndDate_8,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_8, %object
	.size	GuiVar_PercentAdjustEndDate_8, 49
GuiVar_PercentAdjustEndDate_8:
	.space	49
	.global	GuiVar_PercentAdjustEndDate_9
	.section	.bss.GuiVar_PercentAdjustEndDate_9,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustEndDate_9, %object
	.size	GuiVar_PercentAdjustEndDate_9, 49
GuiVar_PercentAdjustEndDate_9:
	.space	49
	.global	GuiVar_PercentAdjustNumberOfDays
	.section	.bss.GuiVar_PercentAdjustNumberOfDays,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustNumberOfDays, %object
	.size	GuiVar_PercentAdjustNumberOfDays, 4
GuiVar_PercentAdjustNumberOfDays:
	.space	4
	.global	GuiVar_PercentAdjustPercent
	.section	.bss.GuiVar_PercentAdjustPercent,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent, %object
	.size	GuiVar_PercentAdjustPercent, 4
GuiVar_PercentAdjustPercent:
	.space	4
	.global	GuiVar_PercentAdjustPercent_0
	.section	.bss.GuiVar_PercentAdjustPercent_0,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_0, %object
	.size	GuiVar_PercentAdjustPercent_0, 4
GuiVar_PercentAdjustPercent_0:
	.space	4
	.global	GuiVar_PercentAdjustPercent_1
	.section	.bss.GuiVar_PercentAdjustPercent_1,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_1, %object
	.size	GuiVar_PercentAdjustPercent_1, 4
GuiVar_PercentAdjustPercent_1:
	.space	4
	.global	GuiVar_PercentAdjustPercent_2
	.section	.bss.GuiVar_PercentAdjustPercent_2,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_2, %object
	.size	GuiVar_PercentAdjustPercent_2, 4
GuiVar_PercentAdjustPercent_2:
	.space	4
	.global	GuiVar_PercentAdjustPercent_3
	.section	.bss.GuiVar_PercentAdjustPercent_3,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_3, %object
	.size	GuiVar_PercentAdjustPercent_3, 4
GuiVar_PercentAdjustPercent_3:
	.space	4
	.global	GuiVar_PercentAdjustPercent_4
	.section	.bss.GuiVar_PercentAdjustPercent_4,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_4, %object
	.size	GuiVar_PercentAdjustPercent_4, 4
GuiVar_PercentAdjustPercent_4:
	.space	4
	.global	GuiVar_PercentAdjustPercent_5
	.section	.bss.GuiVar_PercentAdjustPercent_5,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_5, %object
	.size	GuiVar_PercentAdjustPercent_5, 4
GuiVar_PercentAdjustPercent_5:
	.space	4
	.global	GuiVar_PercentAdjustPercent_6
	.section	.bss.GuiVar_PercentAdjustPercent_6,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_6, %object
	.size	GuiVar_PercentAdjustPercent_6, 4
GuiVar_PercentAdjustPercent_6:
	.space	4
	.global	GuiVar_PercentAdjustPercent_7
	.section	.bss.GuiVar_PercentAdjustPercent_7,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_7, %object
	.size	GuiVar_PercentAdjustPercent_7, 4
GuiVar_PercentAdjustPercent_7:
	.space	4
	.global	GuiVar_PercentAdjustPercent_8
	.section	.bss.GuiVar_PercentAdjustPercent_8,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_8, %object
	.size	GuiVar_PercentAdjustPercent_8, 4
GuiVar_PercentAdjustPercent_8:
	.space	4
	.global	GuiVar_PercentAdjustPercent_9
	.section	.bss.GuiVar_PercentAdjustPercent_9,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_9, %object
	.size	GuiVar_PercentAdjustPercent_9, 4
GuiVar_PercentAdjustPercent_9:
	.space	4
	.global	GuiVar_PercentAdjustPositive
	.section	.bss.GuiVar_PercentAdjustPositive,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPositive, %object
	.size	GuiVar_PercentAdjustPositive, 4
GuiVar_PercentAdjustPositive:
	.space	4
	.global	GuiVar_POCBoxIndex
	.section	.bss.GuiVar_POCBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_POCBoxIndex, %object
	.size	GuiVar_POCBoxIndex, 4
GuiVar_POCBoxIndex:
	.space	4
	.global	GuiVar_POCBudget1
	.section	.bss.GuiVar_POCBudget1,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget1, %object
	.size	GuiVar_POCBudget1, 4
GuiVar_POCBudget1:
	.space	4
	.global	GuiVar_POCBudget10
	.section	.bss.GuiVar_POCBudget10,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget10, %object
	.size	GuiVar_POCBudget10, 4
GuiVar_POCBudget10:
	.space	4
	.global	GuiVar_POCBudget11
	.section	.bss.GuiVar_POCBudget11,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget11, %object
	.size	GuiVar_POCBudget11, 4
GuiVar_POCBudget11:
	.space	4
	.global	GuiVar_POCBudget12
	.section	.bss.GuiVar_POCBudget12,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget12, %object
	.size	GuiVar_POCBudget12, 4
GuiVar_POCBudget12:
	.space	4
	.global	GuiVar_POCBudget2
	.section	.bss.GuiVar_POCBudget2,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget2, %object
	.size	GuiVar_POCBudget2, 4
GuiVar_POCBudget2:
	.space	4
	.global	GuiVar_POCBudget3
	.section	.bss.GuiVar_POCBudget3,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget3, %object
	.size	GuiVar_POCBudget3, 4
GuiVar_POCBudget3:
	.space	4
	.global	GuiVar_POCBudget4
	.section	.bss.GuiVar_POCBudget4,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget4, %object
	.size	GuiVar_POCBudget4, 4
GuiVar_POCBudget4:
	.space	4
	.global	GuiVar_POCBudget5
	.section	.bss.GuiVar_POCBudget5,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget5, %object
	.size	GuiVar_POCBudget5, 4
GuiVar_POCBudget5:
	.space	4
	.global	GuiVar_POCBudget6
	.section	.bss.GuiVar_POCBudget6,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget6, %object
	.size	GuiVar_POCBudget6, 4
GuiVar_POCBudget6:
	.space	4
	.global	GuiVar_POCBudget7
	.section	.bss.GuiVar_POCBudget7,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget7, %object
	.size	GuiVar_POCBudget7, 4
GuiVar_POCBudget7:
	.space	4
	.global	GuiVar_POCBudget8
	.section	.bss.GuiVar_POCBudget8,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget8, %object
	.size	GuiVar_POCBudget8, 4
GuiVar_POCBudget8:
	.space	4
	.global	GuiVar_POCBudget9
	.section	.bss.GuiVar_POCBudget9,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget9, %object
	.size	GuiVar_POCBudget9, 4
GuiVar_POCBudget9:
	.space	4
	.global	GuiVar_POCBudgetOption
	.section	.bss.GuiVar_POCBudgetOption,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudgetOption, %object
	.size	GuiVar_POCBudgetOption, 4
GuiVar_POCBudgetOption:
	.space	4
	.global	GuiVar_POCBudgetPercentOfET
	.section	.bss.GuiVar_POCBudgetPercentOfET,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudgetPercentOfET, %object
	.size	GuiVar_POCBudgetPercentOfET, 4
GuiVar_POCBudgetPercentOfET:
	.space	4
	.global	GuiVar_POCBudgetYearly
	.section	.bss.GuiVar_POCBudgetYearly,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudgetYearly, %object
	.size	GuiVar_POCBudgetYearly, 4
GuiVar_POCBudgetYearly:
	.space	4
	.global	GuiVar_POCBypassStages
	.section	.bss.GuiVar_POCBypassStages,"aw",%nobits
	.align	2
	.type	GuiVar_POCBypassStages, %object
	.size	GuiVar_POCBypassStages, 4
GuiVar_POCBypassStages:
	.space	4
	.global	GuiVar_POCDecoderSN1
	.section	.bss.GuiVar_POCDecoderSN1,"aw",%nobits
	.align	2
	.type	GuiVar_POCDecoderSN1, %object
	.size	GuiVar_POCDecoderSN1, 4
GuiVar_POCDecoderSN1:
	.space	4
	.global	GuiVar_POCDecoderSN2
	.section	.bss.GuiVar_POCDecoderSN2,"aw",%nobits
	.align	2
	.type	GuiVar_POCDecoderSN2, %object
	.size	GuiVar_POCDecoderSN2, 4
GuiVar_POCDecoderSN2:
	.space	4
	.global	GuiVar_POCDecoderSN3
	.section	.bss.GuiVar_POCDecoderSN3,"aw",%nobits
	.align	2
	.type	GuiVar_POCDecoderSN3, %object
	.size	GuiVar_POCDecoderSN3, 4
GuiVar_POCDecoderSN3:
	.space	4
	.global	GuiVar_POCFlowMeterK1
	.section	.bss.GuiVar_POCFlowMeterK1,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterK1, %object
	.size	GuiVar_POCFlowMeterK1, 4
GuiVar_POCFlowMeterK1:
	.space	4
	.global	GuiVar_POCFlowMeterK2
	.section	.bss.GuiVar_POCFlowMeterK2,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterK2, %object
	.size	GuiVar_POCFlowMeterK2, 4
GuiVar_POCFlowMeterK2:
	.space	4
	.global	GuiVar_POCFlowMeterK3
	.section	.bss.GuiVar_POCFlowMeterK3,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterK3, %object
	.size	GuiVar_POCFlowMeterK3, 4
GuiVar_POCFlowMeterK3:
	.space	4
	.global	GuiVar_POCFlowMeterOffset1
	.section	.bss.GuiVar_POCFlowMeterOffset1,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterOffset1, %object
	.size	GuiVar_POCFlowMeterOffset1, 4
GuiVar_POCFlowMeterOffset1:
	.space	4
	.global	GuiVar_POCFlowMeterOffset2
	.section	.bss.GuiVar_POCFlowMeterOffset2,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterOffset2, %object
	.size	GuiVar_POCFlowMeterOffset2, 4
GuiVar_POCFlowMeterOffset2:
	.space	4
	.global	GuiVar_POCFlowMeterOffset3
	.section	.bss.GuiVar_POCFlowMeterOffset3,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterOffset3, %object
	.size	GuiVar_POCFlowMeterOffset3, 4
GuiVar_POCFlowMeterOffset3:
	.space	4
	.global	GuiVar_POCFlowMeterType1
	.section	.bss.GuiVar_POCFlowMeterType1,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterType1, %object
	.size	GuiVar_POCFlowMeterType1, 4
GuiVar_POCFlowMeterType1:
	.space	4
	.global	GuiVar_POCFlowMeterType2
	.section	.bss.GuiVar_POCFlowMeterType2,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterType2, %object
	.size	GuiVar_POCFlowMeterType2, 4
GuiVar_POCFlowMeterType2:
	.space	4
	.global	GuiVar_POCFlowMeterType3
	.section	.bss.GuiVar_POCFlowMeterType3,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterType3, %object
	.size	GuiVar_POCFlowMeterType3, 4
GuiVar_POCFlowMeterType3:
	.space	4
	.global	GuiVar_POCFMType1IsBermad
	.section	.bss.GuiVar_POCFMType1IsBermad,"aw",%nobits
	.align	2
	.type	GuiVar_POCFMType1IsBermad, %object
	.size	GuiVar_POCFMType1IsBermad, 4
GuiVar_POCFMType1IsBermad:
	.space	4
	.global	GuiVar_POCFMType2IsBermad
	.section	.bss.GuiVar_POCFMType2IsBermad,"aw",%nobits
	.align	2
	.type	GuiVar_POCFMType2IsBermad, %object
	.size	GuiVar_POCFMType2IsBermad, 4
GuiVar_POCFMType2IsBermad:
	.space	4
	.global	GuiVar_POCFMType3IsBermad
	.section	.bss.GuiVar_POCFMType3IsBermad,"aw",%nobits
	.align	2
	.type	GuiVar_POCFMType3IsBermad, %object
	.size	GuiVar_POCFMType3IsBermad, 4
GuiVar_POCFMType3IsBermad:
	.space	4
	.global	GuiVar_POCGroupID
	.section	.bss.GuiVar_POCGroupID,"aw",%nobits
	.align	2
	.type	GuiVar_POCGroupID, %object
	.size	GuiVar_POCGroupID, 4
GuiVar_POCGroupID:
	.space	4
	.global	GuiVar_POCMVType1
	.section	.bss.GuiVar_POCMVType1,"aw",%nobits
	.align	2
	.type	GuiVar_POCMVType1, %object
	.size	GuiVar_POCMVType1, 4
GuiVar_POCMVType1:
	.space	4
	.global	GuiVar_POCPhysicallyAvailable
	.section	.bss.GuiVar_POCPhysicallyAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_POCPhysicallyAvailable, %object
	.size	GuiVar_POCPhysicallyAvailable, 4
GuiVar_POCPhysicallyAvailable:
	.space	4
	.global	GuiVar_POCPreservesAccumGal1
	.section	.bss.GuiVar_POCPreservesAccumGal1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumGal1, %object
	.size	GuiVar_POCPreservesAccumGal1, 4
GuiVar_POCPreservesAccumGal1:
	.space	4
	.global	GuiVar_POCPreservesAccumGal2
	.section	.bss.GuiVar_POCPreservesAccumGal2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumGal2, %object
	.size	GuiVar_POCPreservesAccumGal2, 4
GuiVar_POCPreservesAccumGal2:
	.space	4
	.global	GuiVar_POCPreservesAccumGal3
	.section	.bss.GuiVar_POCPreservesAccumGal3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumGal3, %object
	.size	GuiVar_POCPreservesAccumGal3, 4
GuiVar_POCPreservesAccumGal3:
	.space	4
	.global	GuiVar_POCPreservesAccumMS1
	.section	.bss.GuiVar_POCPreservesAccumMS1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumMS1, %object
	.size	GuiVar_POCPreservesAccumMS1, 4
GuiVar_POCPreservesAccumMS1:
	.space	4
	.global	GuiVar_POCPreservesAccumMS2
	.section	.bss.GuiVar_POCPreservesAccumMS2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumMS2, %object
	.size	GuiVar_POCPreservesAccumMS2, 4
GuiVar_POCPreservesAccumMS2:
	.space	4
	.global	GuiVar_POCPreservesAccumMS3
	.section	.bss.GuiVar_POCPreservesAccumMS3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumMS3, %object
	.size	GuiVar_POCPreservesAccumMS3, 4
GuiVar_POCPreservesAccumMS3:
	.space	4
	.global	GuiVar_POCPreservesAccumPulses1
	.section	.bss.GuiVar_POCPreservesAccumPulses1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumPulses1, %object
	.size	GuiVar_POCPreservesAccumPulses1, 4
GuiVar_POCPreservesAccumPulses1:
	.space	4
	.global	GuiVar_POCPreservesAccumPulses2
	.section	.bss.GuiVar_POCPreservesAccumPulses2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumPulses2, %object
	.size	GuiVar_POCPreservesAccumPulses2, 4
GuiVar_POCPreservesAccumPulses2:
	.space	4
	.global	GuiVar_POCPreservesAccumPulses3
	.section	.bss.GuiVar_POCPreservesAccumPulses3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumPulses3, %object
	.size	GuiVar_POCPreservesAccumPulses3, 4
GuiVar_POCPreservesAccumPulses3:
	.space	4
	.global	GuiVar_POCPreservesBoxIndex
	.section	.bss.GuiVar_POCPreservesBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesBoxIndex, %object
	.size	GuiVar_POCPreservesBoxIndex, 4
GuiVar_POCPreservesBoxIndex:
	.space	4
	.global	GuiVar_POCPreservesDecoderSN1
	.section	.bss.GuiVar_POCPreservesDecoderSN1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDecoderSN1, %object
	.size	GuiVar_POCPreservesDecoderSN1, 4
GuiVar_POCPreservesDecoderSN1:
	.space	4
	.global	GuiVar_POCPreservesDecoderSN2
	.section	.bss.GuiVar_POCPreservesDecoderSN2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDecoderSN2, %object
	.size	GuiVar_POCPreservesDecoderSN2, 4
GuiVar_POCPreservesDecoderSN2:
	.space	4
	.global	GuiVar_POCPreservesDecoderSN3
	.section	.bss.GuiVar_POCPreservesDecoderSN3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDecoderSN3, %object
	.size	GuiVar_POCPreservesDecoderSN3, 4
GuiVar_POCPreservesDecoderSN3:
	.space	4
	.global	GuiVar_POCPreservesDelivered5SecAvg1
	.section	.bss.GuiVar_POCPreservesDelivered5SecAvg1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDelivered5SecAvg1, %object
	.size	GuiVar_POCPreservesDelivered5SecAvg1, 4
GuiVar_POCPreservesDelivered5SecAvg1:
	.space	4
	.global	GuiVar_POCPreservesDelivered5SecAvg2
	.section	.bss.GuiVar_POCPreservesDelivered5SecAvg2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDelivered5SecAvg2, %object
	.size	GuiVar_POCPreservesDelivered5SecAvg2, 4
GuiVar_POCPreservesDelivered5SecAvg2:
	.space	4
	.global	GuiVar_POCPreservesDelivered5SecAvg3
	.section	.bss.GuiVar_POCPreservesDelivered5SecAvg3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDelivered5SecAvg3, %object
	.size	GuiVar_POCPreservesDelivered5SecAvg3, 4
GuiVar_POCPreservesDelivered5SecAvg3:
	.space	4
	.global	GuiVar_POCPreservesDeliveredMVCurrent1
	.section	.bss.GuiVar_POCPreservesDeliveredMVCurrent1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredMVCurrent1, %object
	.size	GuiVar_POCPreservesDeliveredMVCurrent1, 4
GuiVar_POCPreservesDeliveredMVCurrent1:
	.space	4
	.global	GuiVar_POCPreservesDeliveredMVCurrent2
	.section	.bss.GuiVar_POCPreservesDeliveredMVCurrent2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredMVCurrent2, %object
	.size	GuiVar_POCPreservesDeliveredMVCurrent2, 4
GuiVar_POCPreservesDeliveredMVCurrent2:
	.space	4
	.global	GuiVar_POCPreservesDeliveredMVCurrent3
	.section	.bss.GuiVar_POCPreservesDeliveredMVCurrent3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredMVCurrent3, %object
	.size	GuiVar_POCPreservesDeliveredMVCurrent3, 4
GuiVar_POCPreservesDeliveredMVCurrent3:
	.space	4
	.global	GuiVar_POCPreservesDeliveredPumpCurrent1
	.section	.bss.GuiVar_POCPreservesDeliveredPumpCurrent1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredPumpCurrent1, %object
	.size	GuiVar_POCPreservesDeliveredPumpCurrent1, 4
GuiVar_POCPreservesDeliveredPumpCurrent1:
	.space	4
	.global	GuiVar_POCPreservesDeliveredPumpCurrent2
	.section	.bss.GuiVar_POCPreservesDeliveredPumpCurrent2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredPumpCurrent2, %object
	.size	GuiVar_POCPreservesDeliveredPumpCurrent2, 4
GuiVar_POCPreservesDeliveredPumpCurrent2:
	.space	4
	.global	GuiVar_POCPreservesDeliveredPumpCurrent3
	.section	.bss.GuiVar_POCPreservesDeliveredPumpCurrent3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredPumpCurrent3, %object
	.size	GuiVar_POCPreservesDeliveredPumpCurrent3, 4
GuiVar_POCPreservesDeliveredPumpCurrent3:
	.space	4
	.global	GuiVar_POCPreservesFileType
	.section	.bss.GuiVar_POCPreservesFileType,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesFileType, %object
	.size	GuiVar_POCPreservesFileType, 4
GuiVar_POCPreservesFileType:
	.space	4
	.global	GuiVar_POCPreservesGroupID
	.section	.bss.GuiVar_POCPreservesGroupID,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesGroupID, %object
	.size	GuiVar_POCPreservesGroupID, 4
GuiVar_POCPreservesGroupID:
	.space	4
	.global	GuiVar_POCPreservesIndex
	.section	.bss.GuiVar_POCPreservesIndex,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesIndex, %object
	.size	GuiVar_POCPreservesIndex, 4
GuiVar_POCPreservesIndex:
	.space	4
	.global	GuiVar_POCReedSwitchType1
	.section	.bss.GuiVar_POCReedSwitchType1,"aw",%nobits
	.align	2
	.type	GuiVar_POCReedSwitchType1, %object
	.size	GuiVar_POCReedSwitchType1, 4
GuiVar_POCReedSwitchType1:
	.space	4
	.global	GuiVar_POCReedSwitchType2
	.section	.bss.GuiVar_POCReedSwitchType2,"aw",%nobits
	.align	2
	.type	GuiVar_POCReedSwitchType2, %object
	.size	GuiVar_POCReedSwitchType2, 4
GuiVar_POCReedSwitchType2:
	.space	4
	.global	GuiVar_POCReedSwitchType3
	.section	.bss.GuiVar_POCReedSwitchType3,"aw",%nobits
	.align	2
	.type	GuiVar_POCReedSwitchType3, %object
	.size	GuiVar_POCReedSwitchType3, 4
GuiVar_POCReedSwitchType3:
	.space	4
	.global	GuiVar_POCType
	.section	.bss.GuiVar_POCType,"aw",%nobits
	.align	2
	.type	GuiVar_POCType, %object
	.size	GuiVar_POCType, 4
GuiVar_POCType:
	.space	4
	.global	GuiVar_POCUsage
	.section	.bss.GuiVar_POCUsage,"aw",%nobits
	.align	2
	.type	GuiVar_POCUsage, %object
	.size	GuiVar_POCUsage, 4
GuiVar_POCUsage:
	.space	4
	.global	GuiVar_RadioTestBadCRC
	.section	.bss.GuiVar_RadioTestBadCRC,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestBadCRC, %object
	.size	GuiVar_RadioTestBadCRC, 4
GuiVar_RadioTestBadCRC:
	.space	4
	.global	GuiVar_RadioTestCommWithET2000e
	.section	.bss.GuiVar_RadioTestCommWithET2000e,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestCommWithET2000e, %object
	.size	GuiVar_RadioTestCommWithET2000e, 4
GuiVar_RadioTestCommWithET2000e:
	.space	4
	.global	GuiVar_RadioTestCS3000SN
	.section	.bss.GuiVar_RadioTestCS3000SN,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestCS3000SN, %object
	.size	GuiVar_RadioTestCS3000SN, 4
GuiVar_RadioTestCS3000SN:
	.space	4
	.global	GuiVar_RadioTestDeviceIndex
	.section	.bss.GuiVar_RadioTestDeviceIndex,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestDeviceIndex, %object
	.size	GuiVar_RadioTestDeviceIndex, 4
GuiVar_RadioTestDeviceIndex:
	.space	4
	.global	GuiVar_RadioTestET2000eAddr0
	.section	.bss.GuiVar_RadioTestET2000eAddr0,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestET2000eAddr0, %object
	.size	GuiVar_RadioTestET2000eAddr0, 2
GuiVar_RadioTestET2000eAddr0:
	.space	2
	.global	GuiVar_RadioTestET2000eAddr1
	.section	.bss.GuiVar_RadioTestET2000eAddr1,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestET2000eAddr1, %object
	.size	GuiVar_RadioTestET2000eAddr1, 2
GuiVar_RadioTestET2000eAddr1:
	.space	2
	.global	GuiVar_RadioTestET2000eAddr2
	.section	.bss.GuiVar_RadioTestET2000eAddr2,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestET2000eAddr2, %object
	.size	GuiVar_RadioTestET2000eAddr2, 2
GuiVar_RadioTestET2000eAddr2:
	.space	2
	.global	GuiVar_RadioTestInProgress
	.section	.bss.GuiVar_RadioTestInProgress,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestInProgress, %object
	.size	GuiVar_RadioTestInProgress, 4
GuiVar_RadioTestInProgress:
	.space	4
	.global	GuiVar_RadioTestNoResp
	.section	.bss.GuiVar_RadioTestNoResp,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestNoResp, %object
	.size	GuiVar_RadioTestNoResp, 4
GuiVar_RadioTestNoResp:
	.space	4
	.global	GuiVar_RadioTestPacketsSent
	.section	.bss.GuiVar_RadioTestPacketsSent,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestPacketsSent, %object
	.size	GuiVar_RadioTestPacketsSent, 4
GuiVar_RadioTestPacketsSent:
	.space	4
	.global	GuiVar_RadioTestPort
	.section	.bss.GuiVar_RadioTestPort,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestPort, %object
	.size	GuiVar_RadioTestPort, 4
GuiVar_RadioTestPort:
	.space	4
	.global	GuiVar_RadioTestRoundTripAvgMS
	.section	.bss.GuiVar_RadioTestRoundTripAvgMS,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestRoundTripAvgMS, %object
	.size	GuiVar_RadioTestRoundTripAvgMS, 4
GuiVar_RadioTestRoundTripAvgMS:
	.space	4
	.global	GuiVar_RadioTestRoundTripMaxMS
	.section	.bss.GuiVar_RadioTestRoundTripMaxMS,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestRoundTripMaxMS, %object
	.size	GuiVar_RadioTestRoundTripMaxMS, 4
GuiVar_RadioTestRoundTripMaxMS:
	.space	4
	.global	GuiVar_RadioTestRoundTripMinMS
	.section	.bss.GuiVar_RadioTestRoundTripMinMS,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestRoundTripMinMS, %object
	.size	GuiVar_RadioTestRoundTripMinMS, 4
GuiVar_RadioTestRoundTripMinMS:
	.space	4
	.global	GuiVar_RadioTestStartTime
	.section	.bss.GuiVar_RadioTestStartTime,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestStartTime, %object
	.size	GuiVar_RadioTestStartTime, 4
GuiVar_RadioTestStartTime:
	.space	4
	.global	GuiVar_RadioTestSuccessfulPercent
	.section	.bss.GuiVar_RadioTestSuccessfulPercent,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestSuccessfulPercent, %object
	.size	GuiVar_RadioTestSuccessfulPercent, 4
GuiVar_RadioTestSuccessfulPercent:
	.space	4
	.global	GuiVar_RainBucketInstalledAt
	.section	.bss.GuiVar_RainBucketInstalledAt,"aw",%nobits
	.align	2
	.type	GuiVar_RainBucketInstalledAt, %object
	.size	GuiVar_RainBucketInstalledAt, 3
GuiVar_RainBucketInstalledAt:
	.space	3
	.global	GuiVar_RainBucketInUse
	.section	.bss.GuiVar_RainBucketInUse,"aw",%nobits
	.align	2
	.type	GuiVar_RainBucketInUse, %object
	.size	GuiVar_RainBucketInUse, 4
GuiVar_RainBucketInUse:
	.space	4
	.global	GuiVar_RainBucketMaximum24Hour
	.section	.bss.GuiVar_RainBucketMaximum24Hour,"aw",%nobits
	.align	2
	.type	GuiVar_RainBucketMaximum24Hour, %object
	.size	GuiVar_RainBucketMaximum24Hour, 4
GuiVar_RainBucketMaximum24Hour:
	.space	4
	.global	GuiVar_RainBucketMaximumHourly
	.section	.bss.GuiVar_RainBucketMaximumHourly,"aw",%nobits
	.align	2
	.type	GuiVar_RainBucketMaximumHourly, %object
	.size	GuiVar_RainBucketMaximumHourly, 4
GuiVar_RainBucketMaximumHourly:
	.space	4
	.global	GuiVar_RainBucketMinimum
	.section	.bss.GuiVar_RainBucketMinimum,"aw",%nobits
	.align	2
	.type	GuiVar_RainBucketMinimum, %object
	.size	GuiVar_RainBucketMinimum, 4
GuiVar_RainBucketMinimum:
	.space	4
	.global	GuiVar_RainSwitchControllerName
	.section	.bss.GuiVar_RainSwitchControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_RainSwitchControllerName, %object
	.size	GuiVar_RainSwitchControllerName, 49
GuiVar_RainSwitchControllerName:
	.space	49
	.global	GuiVar_RainSwitchInUse
	.section	.bss.GuiVar_RainSwitchInUse,"aw",%nobits
	.align	2
	.type	GuiVar_RainSwitchInUse, %object
	.size	GuiVar_RainSwitchInUse, 4
GuiVar_RainSwitchInUse:
	.space	4
	.global	GuiVar_RainSwitchSelected
	.section	.bss.GuiVar_RainSwitchSelected,"aw",%nobits
	.align	2
	.type	GuiVar_RainSwitchSelected, %object
	.size	GuiVar_RainSwitchSelected, 4
GuiVar_RainSwitchSelected:
	.space	4
	.global	GuiVar_RptController
	.section	.bss.GuiVar_RptController,"aw",%nobits
	.align	2
	.type	GuiVar_RptController, %object
	.size	GuiVar_RptController, 4
GuiVar_RptController:
	.space	4
	.global	GuiVar_RptCycles
	.section	.bss.GuiVar_RptCycles,"aw",%nobits
	.align	2
	.type	GuiVar_RptCycles, %object
	.size	GuiVar_RptCycles, 4
GuiVar_RptCycles:
	.space	4
	.global	GuiVar_RptDate
	.section	.bss.GuiVar_RptDate,"aw",%nobits
	.align	2
	.type	GuiVar_RptDate, %object
	.size	GuiVar_RptDate, 6
GuiVar_RptDate:
	.space	6
	.global	GuiVar_RptDecoderOutput
	.section	.bss.GuiVar_RptDecoderOutput,"aw",%nobits
	.align	2
	.type	GuiVar_RptDecoderOutput, %object
	.size	GuiVar_RptDecoderOutput, 4
GuiVar_RptDecoderOutput:
	.space	4
	.global	GuiVar_RptDecoderSN
	.section	.bss.GuiVar_RptDecoderSN,"aw",%nobits
	.align	2
	.type	GuiVar_RptDecoderSN, %object
	.size	GuiVar_RptDecoderSN, 4
GuiVar_RptDecoderSN:
	.space	4
	.global	GuiVar_RptFlags
	.section	.bss.GuiVar_RptFlags,"aw",%nobits
	.align	2
	.type	GuiVar_RptFlags, %object
	.size	GuiVar_RptFlags, 65
GuiVar_RptFlags:
	.space	65
	.global	GuiVar_RptFlow
	.section	.bss.GuiVar_RptFlow,"aw",%nobits
	.align	2
	.type	GuiVar_RptFlow, %object
	.size	GuiVar_RptFlow, 4
GuiVar_RptFlow:
	.space	4
	.global	GuiVar_RptIrrigGal
	.section	.bss.GuiVar_RptIrrigGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptIrrigGal, %object
	.size	GuiVar_RptIrrigGal, 8
GuiVar_RptIrrigGal:
	.space	8
	.global	GuiVar_RptIrrigMin
	.section	.bss.GuiVar_RptIrrigMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptIrrigMin, %object
	.size	GuiVar_RptIrrigMin, 4
GuiVar_RptIrrigMin:
	.space	4
	.global	GuiVar_RptLastMeasuredCurrent
	.section	.bss.GuiVar_RptLastMeasuredCurrent,"aw",%nobits
	.align	2
	.type	GuiVar_RptLastMeasuredCurrent, %object
	.size	GuiVar_RptLastMeasuredCurrent, 4
GuiVar_RptLastMeasuredCurrent:
	.space	4
	.global	GuiVar_RptManualGal
	.section	.bss.GuiVar_RptManualGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptManualGal, %object
	.size	GuiVar_RptManualGal, 8
GuiVar_RptManualGal:
	.space	8
	.global	GuiVar_RptManualMin
	.section	.bss.GuiVar_RptManualMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptManualMin, %object
	.size	GuiVar_RptManualMin, 4
GuiVar_RptManualMin:
	.space	4
	.global	GuiVar_RptManualPGal
	.section	.bss.GuiVar_RptManualPGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptManualPGal, %object
	.size	GuiVar_RptManualPGal, 8
GuiVar_RptManualPGal:
	.space	8
	.global	GuiVar_RptManualPMin
	.section	.bss.GuiVar_RptManualPMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptManualPMin, %object
	.size	GuiVar_RptManualPMin, 4
GuiVar_RptManualPMin:
	.space	4
	.global	GuiVar_RptNonCGal
	.section	.bss.GuiVar_RptNonCGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptNonCGal, %object
	.size	GuiVar_RptNonCGal, 8
GuiVar_RptNonCGal:
	.space	8
	.global	GuiVar_RptNonCMin
	.section	.bss.GuiVar_RptNonCMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptNonCMin, %object
	.size	GuiVar_RptNonCMin, 4
GuiVar_RptNonCMin:
	.space	4
	.global	GuiVar_RptRainMin
	.section	.bss.GuiVar_RptRainMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptRainMin, %object
	.size	GuiVar_RptRainMin, 4
GuiVar_RptRainMin:
	.space	4
	.global	GuiVar_RptRReGal
	.section	.bss.GuiVar_RptRReGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptRReGal, %object
	.size	GuiVar_RptRReGal, 8
GuiVar_RptRReGal:
	.space	8
	.global	GuiVar_RptRReMin
	.section	.bss.GuiVar_RptRReMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptRReMin, %object
	.size	GuiVar_RptRReMin, 4
GuiVar_RptRReMin:
	.space	4
	.global	GuiVar_RptScheduledMin
	.section	.bss.GuiVar_RptScheduledMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptScheduledMin, %object
	.size	GuiVar_RptScheduledMin, 4
GuiVar_RptScheduledMin:
	.space	4
	.global	GuiVar_RptStartTime
	.section	.bss.GuiVar_RptStartTime,"aw",%nobits
	.align	2
	.type	GuiVar_RptStartTime, %object
	.size	GuiVar_RptStartTime, 8
GuiVar_RptStartTime:
	.space	8
	.global	GuiVar_RptStation
	.section	.bss.GuiVar_RptStation,"aw",%nobits
	.align	2
	.type	GuiVar_RptStation, %object
	.size	GuiVar_RptStation, 4
GuiVar_RptStation:
	.space	4
	.global	GuiVar_RptTestGal
	.section	.bss.GuiVar_RptTestGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptTestGal, %object
	.size	GuiVar_RptTestGal, 8
GuiVar_RptTestGal:
	.space	8
	.global	GuiVar_RptTestMin
	.section	.bss.GuiVar_RptTestMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptTestMin, %object
	.size	GuiVar_RptTestMin, 4
GuiVar_RptTestMin:
	.space	4
	.global	GuiVar_RptWalkThruGal
	.section	.bss.GuiVar_RptWalkThruGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptWalkThruGal, %object
	.size	GuiVar_RptWalkThruGal, 8
GuiVar_RptWalkThruGal:
	.space	8
	.global	GuiVar_RptWalkThruMin
	.section	.bss.GuiVar_RptWalkThruMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptWalkThruMin, %object
	.size	GuiVar_RptWalkThruMin, 4
GuiVar_RptWalkThruMin:
	.space	4
	.global	GuiVar_ScheduleBeginDate
	.section	.bss.GuiVar_ScheduleBeginDate,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleBeginDate, %object
	.size	GuiVar_ScheduleBeginDate, 4
GuiVar_ScheduleBeginDate:
	.space	4
	.global	GuiVar_ScheduleBeginDateStr
	.section	.bss.GuiVar_ScheduleBeginDateStr,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleBeginDateStr, %object
	.size	GuiVar_ScheduleBeginDateStr, 49
GuiVar_ScheduleBeginDateStr:
	.space	49
	.global	GuiVar_ScheduleIrrigateOn29thOr31st
	.section	.bss.GuiVar_ScheduleIrrigateOn29thOr31st,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleIrrigateOn29thOr31st, %object
	.size	GuiVar_ScheduleIrrigateOn29thOr31st, 4
GuiVar_ScheduleIrrigateOn29thOr31st:
	.space	4
	.global	GuiVar_ScheduleMowDay
	.section	.bss.GuiVar_ScheduleMowDay,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleMowDay, %object
	.size	GuiVar_ScheduleMowDay, 4
GuiVar_ScheduleMowDay:
	.space	4
	.global	GuiVar_ScheduleNoStationsInGroup
	.section	.bss.GuiVar_ScheduleNoStationsInGroup,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleNoStationsInGroup, %object
	.size	GuiVar_ScheduleNoStationsInGroup, 4
GuiVar_ScheduleNoStationsInGroup:
	.space	4
	.global	GuiVar_ScheduleStartTime
	.section	.bss.GuiVar_ScheduleStartTime,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStartTime, %object
	.size	GuiVar_ScheduleStartTime, 4
GuiVar_ScheduleStartTime:
	.space	4
	.global	GuiVar_ScheduleStartTimeEnabled
	.section	.bss.GuiVar_ScheduleStartTimeEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStartTimeEnabled, %object
	.size	GuiVar_ScheduleStartTimeEnabled, 4
GuiVar_ScheduleStartTimeEnabled:
	.space	4
	.global	GuiVar_ScheduleStartTimeEqualsStopTime
	.section	.bss.GuiVar_ScheduleStartTimeEqualsStopTime,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStartTimeEqualsStopTime, %object
	.size	GuiVar_ScheduleStartTimeEqualsStopTime, 4
GuiVar_ScheduleStartTimeEqualsStopTime:
	.space	4
	.global	GuiVar_ScheduleStopTime
	.section	.bss.GuiVar_ScheduleStopTime,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStopTime, %object
	.size	GuiVar_ScheduleStopTime, 4
GuiVar_ScheduleStopTime:
	.space	4
	.global	GuiVar_ScheduleStopTimeEnabled
	.section	.bss.GuiVar_ScheduleStopTimeEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStopTimeEnabled, %object
	.size	GuiVar_ScheduleStopTimeEnabled, 4
GuiVar_ScheduleStopTimeEnabled:
	.space	4
	.global	GuiVar_ScheduleType
	.section	.bss.GuiVar_ScheduleType,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleType, %object
	.size	GuiVar_ScheduleType, 4
GuiVar_ScheduleType:
	.space	4
	.global	GuiVar_ScheduleTypeScrollItem
	.section	.bss.GuiVar_ScheduleTypeScrollItem,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleTypeScrollItem, %object
	.size	GuiVar_ScheduleTypeScrollItem, 4
GuiVar_ScheduleTypeScrollItem:
	.space	4
	.global	GuiVar_ScheduleUseETAveraging
	.section	.bss.GuiVar_ScheduleUseETAveraging,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleUseETAveraging, %object
	.size	GuiVar_ScheduleUseETAveraging, 4
GuiVar_ScheduleUseETAveraging:
	.space	4
	.global	GuiVar_ScheduleUsesET
	.section	.bss.GuiVar_ScheduleUsesET,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleUsesET, %object
	.size	GuiVar_ScheduleUsesET, 4
GuiVar_ScheduleUsesET:
	.space	4
	.global	GuiVar_ScheduleWaterDay_Fri
	.section	.bss.GuiVar_ScheduleWaterDay_Fri,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Fri, %object
	.size	GuiVar_ScheduleWaterDay_Fri, 4
GuiVar_ScheduleWaterDay_Fri:
	.space	4
	.global	GuiVar_ScheduleWaterDay_Mon
	.section	.bss.GuiVar_ScheduleWaterDay_Mon,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Mon, %object
	.size	GuiVar_ScheduleWaterDay_Mon, 4
GuiVar_ScheduleWaterDay_Mon:
	.space	4
	.global	GuiVar_ScheduleWaterDay_Sat
	.section	.bss.GuiVar_ScheduleWaterDay_Sat,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Sat, %object
	.size	GuiVar_ScheduleWaterDay_Sat, 4
GuiVar_ScheduleWaterDay_Sat:
	.space	4
	.global	GuiVar_ScheduleWaterDay_Sun
	.section	.bss.GuiVar_ScheduleWaterDay_Sun,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Sun, %object
	.size	GuiVar_ScheduleWaterDay_Sun, 4
GuiVar_ScheduleWaterDay_Sun:
	.space	4
	.global	GuiVar_ScheduleWaterDay_Thu
	.section	.bss.GuiVar_ScheduleWaterDay_Thu,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Thu, %object
	.size	GuiVar_ScheduleWaterDay_Thu, 4
GuiVar_ScheduleWaterDay_Thu:
	.space	4
	.global	GuiVar_ScheduleWaterDay_Tue
	.section	.bss.GuiVar_ScheduleWaterDay_Tue,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Tue, %object
	.size	GuiVar_ScheduleWaterDay_Tue, 4
GuiVar_ScheduleWaterDay_Tue:
	.space	4
	.global	GuiVar_ScheduleWaterDay_Wed
	.section	.bss.GuiVar_ScheduleWaterDay_Wed,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Wed, %object
	.size	GuiVar_ScheduleWaterDay_Wed, 4
GuiVar_ScheduleWaterDay_Wed:
	.space	4
	.global	GuiVar_ScrollBoxHorizScrollMarker
	.section	.bss.GuiVar_ScrollBoxHorizScrollMarker,"aw",%nobits
	.align	2
	.type	GuiVar_ScrollBoxHorizScrollMarker, %object
	.size	GuiVar_ScrollBoxHorizScrollMarker, 4
GuiVar_ScrollBoxHorizScrollMarker:
	.space	4
	.global	GuiVar_ScrollBoxHorizScrollPos
	.section	.bss.GuiVar_ScrollBoxHorizScrollPos,"aw",%nobits
	.align	2
	.type	GuiVar_ScrollBoxHorizScrollPos, %object
	.size	GuiVar_ScrollBoxHorizScrollPos, 4
GuiVar_ScrollBoxHorizScrollPos:
	.space	4
	.global	GuiVar_SerialPortCDA
	.section	.bss.GuiVar_SerialPortCDA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortCDA, %object
	.size	GuiVar_SerialPortCDA, 4
GuiVar_SerialPortCDA:
	.space	4
	.global	GuiVar_SerialPortCDB
	.section	.bss.GuiVar_SerialPortCDB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortCDB, %object
	.size	GuiVar_SerialPortCDB, 4
GuiVar_SerialPortCDB:
	.space	4
	.global	GuiVar_SerialPortCTSA
	.section	.bss.GuiVar_SerialPortCTSA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortCTSA, %object
	.size	GuiVar_SerialPortCTSA, 4
GuiVar_SerialPortCTSA:
	.space	4
	.global	GuiVar_SerialPortCTSB
	.section	.bss.GuiVar_SerialPortCTSB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortCTSB, %object
	.size	GuiVar_SerialPortCTSB, 4
GuiVar_SerialPortCTSB:
	.space	4
	.global	GuiVar_SerialPortOptionA
	.section	.bss.GuiVar_SerialPortOptionA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortOptionA, %object
	.size	GuiVar_SerialPortOptionA, 4
GuiVar_SerialPortOptionA:
	.space	4
	.global	GuiVar_SerialPortOptionB
	.section	.bss.GuiVar_SerialPortOptionB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortOptionB, %object
	.size	GuiVar_SerialPortOptionB, 4
GuiVar_SerialPortOptionB:
	.space	4
	.global	GuiVar_SerialPortRcvdA
	.section	.bss.GuiVar_SerialPortRcvdA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortRcvdA, %object
	.size	GuiVar_SerialPortRcvdA, 4
GuiVar_SerialPortRcvdA:
	.space	4
	.global	GuiVar_SerialPortRcvdB
	.section	.bss.GuiVar_SerialPortRcvdB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortRcvdB, %object
	.size	GuiVar_SerialPortRcvdB, 4
GuiVar_SerialPortRcvdB:
	.space	4
	.global	GuiVar_SerialPortRcvdTP
	.section	.bss.GuiVar_SerialPortRcvdTP,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortRcvdTP, %object
	.size	GuiVar_SerialPortRcvdTP, 4
GuiVar_SerialPortRcvdTP:
	.space	4
	.global	GuiVar_SerialPortStateA
	.section	.bss.GuiVar_SerialPortStateA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortStateA, %object
	.size	GuiVar_SerialPortStateA, 4
GuiVar_SerialPortStateA:
	.space	4
	.global	GuiVar_SerialPortStateB
	.section	.bss.GuiVar_SerialPortStateB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortStateB, %object
	.size	GuiVar_SerialPortStateB, 4
GuiVar_SerialPortStateB:
	.space	4
	.global	GuiVar_SerialPortStateTP
	.section	.bss.GuiVar_SerialPortStateTP,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortStateTP, %object
	.size	GuiVar_SerialPortStateTP, 4
GuiVar_SerialPortStateTP:
	.space	4
	.global	GuiVar_SerialPortXmitA
	.section	.bss.GuiVar_SerialPortXmitA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitA, %object
	.size	GuiVar_SerialPortXmitA, 4
GuiVar_SerialPortXmitA:
	.space	4
	.global	GuiVar_SerialPortXmitB
	.section	.bss.GuiVar_SerialPortXmitB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitB, %object
	.size	GuiVar_SerialPortXmitB, 4
GuiVar_SerialPortXmitB:
	.space	4
	.global	GuiVar_SerialPortXmitLengthA
	.section	.bss.GuiVar_SerialPortXmitLengthA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitLengthA, %object
	.size	GuiVar_SerialPortXmitLengthA, 4
GuiVar_SerialPortXmitLengthA:
	.space	4
	.global	GuiVar_SerialPortXmitLengthB
	.section	.bss.GuiVar_SerialPortXmitLengthB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitLengthB, %object
	.size	GuiVar_SerialPortXmitLengthB, 4
GuiVar_SerialPortXmitLengthB:
	.space	4
	.global	GuiVar_SerialPortXmittingA
	.section	.bss.GuiVar_SerialPortXmittingA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmittingA, %object
	.size	GuiVar_SerialPortXmittingA, 4
GuiVar_SerialPortXmittingA:
	.space	4
	.global	GuiVar_SerialPortXmittingB
	.section	.bss.GuiVar_SerialPortXmittingB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmittingB, %object
	.size	GuiVar_SerialPortXmittingB, 4
GuiVar_SerialPortXmittingB:
	.space	4
	.global	GuiVar_SerialPortXmitTP
	.section	.bss.GuiVar_SerialPortXmitTP,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitTP, %object
	.size	GuiVar_SerialPortXmitTP, 4
GuiVar_SerialPortXmitTP:
	.space	4
	.global	GuiVar_ShowDivider
	.section	.bss.GuiVar_ShowDivider,"aw",%nobits
	.align	2
	.type	GuiVar_ShowDivider, %object
	.size	GuiVar_ShowDivider, 4
GuiVar_ShowDivider:
	.space	4
	.global	GuiVar_SpinnerPos
	.section	.bss.GuiVar_SpinnerPos,"aw",%nobits
	.align	2
	.type	GuiVar_SpinnerPos, %object
	.size	GuiVar_SpinnerPos, 4
GuiVar_SpinnerPos:
	.space	4
	.global	GuiVar_SRFirmwareVer
	.section	.bss.GuiVar_SRFirmwareVer,"aw",%nobits
	.align	2
	.type	GuiVar_SRFirmwareVer, %object
	.size	GuiVar_SRFirmwareVer, 49
GuiVar_SRFirmwareVer:
	.space	49
	.global	GuiVar_SRGroup
	.section	.bss.GuiVar_SRGroup,"aw",%nobits
	.align	2
	.type	GuiVar_SRGroup, %object
	.size	GuiVar_SRGroup, 4
GuiVar_SRGroup:
	.space	4
	.global	GuiVar_SRMode
	.section	.bss.GuiVar_SRMode,"aw",%nobits
	.align	2
	.type	GuiVar_SRMode, %object
	.size	GuiVar_SRMode, 4
GuiVar_SRMode:
	.space	4
	.global	GuiVar_SRPort
	.section	.bss.GuiVar_SRPort,"aw",%nobits
	.align	2
	.type	GuiVar_SRPort, %object
	.size	GuiVar_SRPort, 4
GuiVar_SRPort:
	.space	4
	.global	GuiVar_SRRepeater
	.section	.bss.GuiVar_SRRepeater,"aw",%nobits
	.align	2
	.type	GuiVar_SRRepeater, %object
	.size	GuiVar_SRRepeater, 4
GuiVar_SRRepeater:
	.space	4
	.global	GuiVar_SRRepeaterLinkedToItself
	.section	.bss.GuiVar_SRRepeaterLinkedToItself,"aw",%nobits
	.align	2
	.type	GuiVar_SRRepeaterLinkedToItself, %object
	.size	GuiVar_SRRepeaterLinkedToItself, 4
GuiVar_SRRepeaterLinkedToItself:
	.space	4
	.global	GuiVar_SRSerialNumber
	.section	.bss.GuiVar_SRSerialNumber,"aw",%nobits
	.align	2
	.type	GuiVar_SRSerialNumber, %object
	.size	GuiVar_SRSerialNumber, 9
GuiVar_SRSerialNumber:
	.space	9
	.global	GuiVar_SRSubnetRcvMaster
	.section	.bss.GuiVar_SRSubnetRcvMaster,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetRcvMaster, %object
	.size	GuiVar_SRSubnetRcvMaster, 4
GuiVar_SRSubnetRcvMaster:
	.space	4
	.global	GuiVar_SRSubnetRcvRepeater
	.section	.bss.GuiVar_SRSubnetRcvRepeater,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetRcvRepeater, %object
	.size	GuiVar_SRSubnetRcvRepeater, 4
GuiVar_SRSubnetRcvRepeater:
	.space	4
	.global	GuiVar_SRSubnetRcvSlave
	.section	.bss.GuiVar_SRSubnetRcvSlave,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetRcvSlave, %object
	.size	GuiVar_SRSubnetRcvSlave, 4
GuiVar_SRSubnetRcvSlave:
	.space	4
	.global	GuiVar_SRSubnetXmtMaster
	.section	.bss.GuiVar_SRSubnetXmtMaster,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetXmtMaster, %object
	.size	GuiVar_SRSubnetXmtMaster, 4
GuiVar_SRSubnetXmtMaster:
	.space	4
	.global	GuiVar_SRSubnetXmtRepeater
	.section	.bss.GuiVar_SRSubnetXmtRepeater,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetXmtRepeater, %object
	.size	GuiVar_SRSubnetXmtRepeater, 4
GuiVar_SRSubnetXmtRepeater:
	.space	4
	.global	GuiVar_SRSubnetXmtSlave
	.section	.bss.GuiVar_SRSubnetXmtSlave,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetXmtSlave, %object
	.size	GuiVar_SRSubnetXmtSlave, 4
GuiVar_SRSubnetXmtSlave:
	.space	4
	.global	GuiVar_SRTransmitPower
	.section	.bss.GuiVar_SRTransmitPower,"aw",%nobits
	.align	2
	.type	GuiVar_SRTransmitPower, %object
	.size	GuiVar_SRTransmitPower, 4
GuiVar_SRTransmitPower:
	.space	4
	.global	GuiVar_StationCopyGroupName
	.section	.bss.GuiVar_StationCopyGroupName,"aw",%nobits
	.align	2
	.type	GuiVar_StationCopyGroupName, %object
	.size	GuiVar_StationCopyGroupName, 49
GuiVar_StationCopyGroupName:
	.space	49
	.global	GuiVar_StationCopyTextOffset_Left
	.section	.bss.GuiVar_StationCopyTextOffset_Left,"aw",%nobits
	.align	2
	.type	GuiVar_StationCopyTextOffset_Left, %object
	.size	GuiVar_StationCopyTextOffset_Left, 4
GuiVar_StationCopyTextOffset_Left:
	.space	4
	.global	GuiVar_StationCopyTextOffset_Right
	.section	.bss.GuiVar_StationCopyTextOffset_Right,"aw",%nobits
	.align	2
	.type	GuiVar_StationCopyTextOffset_Right, %object
	.size	GuiVar_StationCopyTextOffset_Right, 4
GuiVar_StationCopyTextOffset_Right:
	.space	4
	.global	GuiVar_StationDecoderOutput
	.section	.bss.GuiVar_StationDecoderOutput,"aw",%nobits
	.align	2
	.type	GuiVar_StationDecoderOutput, %object
	.size	GuiVar_StationDecoderOutput, 4
GuiVar_StationDecoderOutput:
	.space	4
	.global	GuiVar_StationDecoderSerialNumber
	.section	.bss.GuiVar_StationDecoderSerialNumber,"aw",%nobits
	.align	2
	.type	GuiVar_StationDecoderSerialNumber, %object
	.size	GuiVar_StationDecoderSerialNumber, 4
GuiVar_StationDecoderSerialNumber:
	.space	4
	.global	GuiVar_StationDescription
	.section	.bss.GuiVar_StationDescription,"aw",%nobits
	.align	2
	.type	GuiVar_StationDescription, %object
	.size	GuiVar_StationDescription, 49
GuiVar_StationDescription:
	.space	49
	.global	GuiVar_StationGroupAllowableDepletion
	.section	.bss.GuiVar_StationGroupAllowableDepletion,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupAllowableDepletion, %object
	.size	GuiVar_StationGroupAllowableDepletion, 4
GuiVar_StationGroupAllowableDepletion:
	.space	4
	.global	GuiVar_StationGroupAllowableSurfaceAccum
	.section	.bss.GuiVar_StationGroupAllowableSurfaceAccum,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupAllowableSurfaceAccum, %object
	.size	GuiVar_StationGroupAllowableSurfaceAccum, 4
GuiVar_StationGroupAllowableSurfaceAccum:
	.space	4
	.global	GuiVar_StationGroupAvailableWater
	.section	.bss.GuiVar_StationGroupAvailableWater,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupAvailableWater, %object
	.size	GuiVar_StationGroupAvailableWater, 4
GuiVar_StationGroupAvailableWater:
	.space	4
	.global	GuiVar_StationGroupDensityFactor
	.section	.bss.GuiVar_StationGroupDensityFactor,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupDensityFactor, %object
	.size	GuiVar_StationGroupDensityFactor, 4
GuiVar_StationGroupDensityFactor:
	.space	4
	.global	GuiVar_StationGroupExposure
	.section	.bss.GuiVar_StationGroupExposure,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupExposure, %object
	.size	GuiVar_StationGroupExposure, 4
GuiVar_StationGroupExposure:
	.space	4
	.global	GuiVar_StationGroupHeadType
	.section	.bss.GuiVar_StationGroupHeadType,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupHeadType, %object
	.size	GuiVar_StationGroupHeadType, 4
GuiVar_StationGroupHeadType:
	.space	4
	.global	GuiVar_StationGroupInUse
	.section	.bss.GuiVar_StationGroupInUse,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupInUse, %object
	.size	GuiVar_StationGroupInUse, 4
GuiVar_StationGroupInUse:
	.space	4
	.global	GuiVar_StationGroupKc1
	.section	.bss.GuiVar_StationGroupKc1,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc1, %object
	.size	GuiVar_StationGroupKc1, 4
GuiVar_StationGroupKc1:
	.space	4
	.global	GuiVar_StationGroupKc10
	.section	.bss.GuiVar_StationGroupKc10,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc10, %object
	.size	GuiVar_StationGroupKc10, 4
GuiVar_StationGroupKc10:
	.space	4
	.global	GuiVar_StationGroupKc11
	.section	.bss.GuiVar_StationGroupKc11,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc11, %object
	.size	GuiVar_StationGroupKc11, 4
GuiVar_StationGroupKc11:
	.space	4
	.global	GuiVar_StationGroupKc12
	.section	.bss.GuiVar_StationGroupKc12,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc12, %object
	.size	GuiVar_StationGroupKc12, 4
GuiVar_StationGroupKc12:
	.space	4
	.global	GuiVar_StationGroupKc2
	.section	.bss.GuiVar_StationGroupKc2,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc2, %object
	.size	GuiVar_StationGroupKc2, 4
GuiVar_StationGroupKc2:
	.space	4
	.global	GuiVar_StationGroupKc3
	.section	.bss.GuiVar_StationGroupKc3,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc3, %object
	.size	GuiVar_StationGroupKc3, 4
GuiVar_StationGroupKc3:
	.space	4
	.global	GuiVar_StationGroupKc4
	.section	.bss.GuiVar_StationGroupKc4,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc4, %object
	.size	GuiVar_StationGroupKc4, 4
GuiVar_StationGroupKc4:
	.space	4
	.global	GuiVar_StationGroupKc5
	.section	.bss.GuiVar_StationGroupKc5,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc5, %object
	.size	GuiVar_StationGroupKc5, 4
GuiVar_StationGroupKc5:
	.space	4
	.global	GuiVar_StationGroupKc6
	.section	.bss.GuiVar_StationGroupKc6,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc6, %object
	.size	GuiVar_StationGroupKc6, 4
GuiVar_StationGroupKc6:
	.space	4
	.global	GuiVar_StationGroupKc7
	.section	.bss.GuiVar_StationGroupKc7,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc7, %object
	.size	GuiVar_StationGroupKc7, 4
GuiVar_StationGroupKc7:
	.space	4
	.global	GuiVar_StationGroupKc8
	.section	.bss.GuiVar_StationGroupKc8,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc8, %object
	.size	GuiVar_StationGroupKc8, 4
GuiVar_StationGroupKc8:
	.space	4
	.global	GuiVar_StationGroupKc9
	.section	.bss.GuiVar_StationGroupKc9,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc9, %object
	.size	GuiVar_StationGroupKc9, 4
GuiVar_StationGroupKc9:
	.space	4
	.global	GuiVar_StationGroupKcsAreCustom
	.section	.bss.GuiVar_StationGroupKcsAreCustom,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKcsAreCustom, %object
	.size	GuiVar_StationGroupKcsAreCustom, 4
GuiVar_StationGroupKcsAreCustom:
	.space	4
	.global	GuiVar_StationGroupMicroclimateFactor
	.section	.bss.GuiVar_StationGroupMicroclimateFactor,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupMicroclimateFactor, %object
	.size	GuiVar_StationGroupMicroclimateFactor, 4
GuiVar_StationGroupMicroclimateFactor:
	.space	4
	.global	GuiVar_StationGroupMoistureSensorDecoderSN
	.section	.bss.GuiVar_StationGroupMoistureSensorDecoderSN,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupMoistureSensorDecoderSN, %object
	.size	GuiVar_StationGroupMoistureSensorDecoderSN, 4
GuiVar_StationGroupMoistureSensorDecoderSN:
	.space	4
	.global	GuiVar_StationGroupPlantType
	.section	.bss.GuiVar_StationGroupPlantType,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupPlantType, %object
	.size	GuiVar_StationGroupPlantType, 4
GuiVar_StationGroupPlantType:
	.space	4
	.global	GuiVar_StationGroupPrecipRate
	.section	.bss.GuiVar_StationGroupPrecipRate,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupPrecipRate, %object
	.size	GuiVar_StationGroupPrecipRate, 4
GuiVar_StationGroupPrecipRate:
	.space	4
	.global	GuiVar_StationGroupRootZoneDepth
	.section	.bss.GuiVar_StationGroupRootZoneDepth,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupRootZoneDepth, %object
	.size	GuiVar_StationGroupRootZoneDepth, 4
GuiVar_StationGroupRootZoneDepth:
	.space	4
	.global	GuiVar_StationGroupSlopePercentage
	.section	.bss.GuiVar_StationGroupSlopePercentage,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSlopePercentage, %object
	.size	GuiVar_StationGroupSlopePercentage, 4
GuiVar_StationGroupSlopePercentage:
	.space	4
	.global	GuiVar_StationGroupSoilIntakeRate
	.section	.bss.GuiVar_StationGroupSoilIntakeRate,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSoilIntakeRate, %object
	.size	GuiVar_StationGroupSoilIntakeRate, 4
GuiVar_StationGroupSoilIntakeRate:
	.space	4
	.global	GuiVar_StationGroupSoilStorageCapacity
	.section	.bss.GuiVar_StationGroupSoilStorageCapacity,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSoilStorageCapacity, %object
	.size	GuiVar_StationGroupSoilStorageCapacity, 4
GuiVar_StationGroupSoilStorageCapacity:
	.space	4
	.global	GuiVar_StationGroupSoilType
	.section	.bss.GuiVar_StationGroupSoilType,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSoilType, %object
	.size	GuiVar_StationGroupSoilType, 4
GuiVar_StationGroupSoilType:
	.space	4
	.global	GuiVar_StationGroupSpeciesFactor
	.section	.bss.GuiVar_StationGroupSpeciesFactor,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSpeciesFactor, %object
	.size	GuiVar_StationGroupSpeciesFactor, 4
GuiVar_StationGroupSpeciesFactor:
	.space	4
	.global	GuiVar_StationGroupSystemGID
	.section	.bss.GuiVar_StationGroupSystemGID,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSystemGID, %object
	.size	GuiVar_StationGroupSystemGID, 4
GuiVar_StationGroupSystemGID:
	.space	4
	.global	GuiVar_StationGroupUsableRain
	.section	.bss.GuiVar_StationGroupUsableRain,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupUsableRain, %object
	.size	GuiVar_StationGroupUsableRain, 4
GuiVar_StationGroupUsableRain:
	.space	4
	.global	GuiVar_StationInfoBoxIndex
	.section	.bss.GuiVar_StationInfoBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoBoxIndex, %object
	.size	GuiVar_StationInfoBoxIndex, 4
GuiVar_StationInfoBoxIndex:
	.space	4
	.global	GuiVar_StationInfoControllerName
	.section	.bss.GuiVar_StationInfoControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoControllerName, %object
	.size	GuiVar_StationInfoControllerName, 49
GuiVar_StationInfoControllerName:
	.space	49
	.global	GuiVar_StationInfoCycleTime
	.section	.bss.GuiVar_StationInfoCycleTime,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoCycleTime, %object
	.size	GuiVar_StationInfoCycleTime, 4
GuiVar_StationInfoCycleTime:
	.space	4
	.global	GuiVar_StationInfoCycleTooShort
	.section	.bss.GuiVar_StationInfoCycleTooShort,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoCycleTooShort, %object
	.size	GuiVar_StationInfoCycleTooShort, 4
GuiVar_StationInfoCycleTooShort:
	.space	4
	.global	GuiVar_StationInfoDU
	.section	.bss.GuiVar_StationInfoDU,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoDU, %object
	.size	GuiVar_StationInfoDU, 4
GuiVar_StationInfoDU:
	.space	4
	.global	GuiVar_StationInfoEstMin
	.section	.bss.GuiVar_StationInfoEstMin,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoEstMin, %object
	.size	GuiVar_StationInfoEstMin, 4
GuiVar_StationInfoEstMin:
	.space	4
	.global	GuiVar_StationInfoETFactor
	.section	.bss.GuiVar_StationInfoETFactor,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoETFactor, %object
	.size	GuiVar_StationInfoETFactor, 4
GuiVar_StationInfoETFactor:
	.space	4
	.global	GuiVar_StationInfoExpectedFlow
	.section	.bss.GuiVar_StationInfoExpectedFlow,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoExpectedFlow, %object
	.size	GuiVar_StationInfoExpectedFlow, 4
GuiVar_StationInfoExpectedFlow:
	.space	4
	.global	GuiVar_StationInfoFlowStatus
	.section	.bss.GuiVar_StationInfoFlowStatus,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoFlowStatus, %object
	.size	GuiVar_StationInfoFlowStatus, 4
GuiVar_StationInfoFlowStatus:
	.space	4
	.global	GuiVar_StationInfoGroupHasStartTime
	.section	.bss.GuiVar_StationInfoGroupHasStartTime,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoGroupHasStartTime, %object
	.size	GuiVar_StationInfoGroupHasStartTime, 4
GuiVar_StationInfoGroupHasStartTime:
	.space	4
	.global	GuiVar_StationInfoIStatus
	.section	.bss.GuiVar_StationInfoIStatus,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoIStatus, %object
	.size	GuiVar_StationInfoIStatus, 4
GuiVar_StationInfoIStatus:
	.space	4
	.global	GuiVar_StationInfoMoistureBalance
	.section	.bss.GuiVar_StationInfoMoistureBalance,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoMoistureBalance, %object
	.size	GuiVar_StationInfoMoistureBalance, 4
GuiVar_StationInfoMoistureBalance:
	.space	4
	.global	GuiVar_StationInfoMoistureBalancePercent
	.section	.bss.GuiVar_StationInfoMoistureBalancePercent,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoMoistureBalancePercent, %object
	.size	GuiVar_StationInfoMoistureBalancePercent, 4
GuiVar_StationInfoMoistureBalancePercent:
	.space	4
	.global	GuiVar_StationInfoNoWaterDays
	.section	.bss.GuiVar_StationInfoNoWaterDays,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoNoWaterDays, %object
	.size	GuiVar_StationInfoNoWaterDays, 4
GuiVar_StationInfoNoWaterDays:
	.space	4
	.global	GuiVar_StationInfoNumber
	.section	.bss.GuiVar_StationInfoNumber,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoNumber, %object
	.size	GuiVar_StationInfoNumber, 4
GuiVar_StationInfoNumber:
	.space	4
	.global	GuiVar_StationInfoNumber_str
	.section	.bss.GuiVar_StationInfoNumber_str,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoNumber_str, %object
	.size	GuiVar_StationInfoNumber_str, 4
GuiVar_StationInfoNumber_str:
	.space	4
	.global	GuiVar_StationInfoShowCopyStation
	.section	.bss.GuiVar_StationInfoShowCopyStation,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoShowCopyStation, %object
	.size	GuiVar_StationInfoShowCopyStation, 4
GuiVar_StationInfoShowCopyStation:
	.space	4
	.global	GuiVar_StationInfoShowEstMin
	.section	.bss.GuiVar_StationInfoShowEstMin,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoShowEstMin, %object
	.size	GuiVar_StationInfoShowEstMin, 4
GuiVar_StationInfoShowEstMin:
	.space	4
	.global	GuiVar_StationInfoShowPercentAdjust
	.section	.bss.GuiVar_StationInfoShowPercentAdjust,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoShowPercentAdjust, %object
	.size	GuiVar_StationInfoShowPercentAdjust, 4
GuiVar_StationInfoShowPercentAdjust:
	.space	4
	.global	GuiVar_StationInfoShowPercentOfET
	.section	.bss.GuiVar_StationInfoShowPercentOfET,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoShowPercentOfET, %object
	.size	GuiVar_StationInfoShowPercentOfET, 4
GuiVar_StationInfoShowPercentOfET:
	.space	4
	.global	GuiVar_StationInfoSoakInTime
	.section	.bss.GuiVar_StationInfoSoakInTime,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoSoakInTime, %object
	.size	GuiVar_StationInfoSoakInTime, 4
GuiVar_StationInfoSoakInTime:
	.space	4
	.global	GuiVar_StationInfoSquareFootage
	.section	.bss.GuiVar_StationInfoSquareFootage,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoSquareFootage, %object
	.size	GuiVar_StationInfoSquareFootage, 4
GuiVar_StationInfoSquareFootage:
	.space	4
	.global	GuiVar_StationInfoStationGroup
	.section	.bss.GuiVar_StationInfoStationGroup,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoStationGroup, %object
	.size	GuiVar_StationInfoStationGroup, 49
GuiVar_StationInfoStationGroup:
	.space	49
	.global	GuiVar_StationInfoTotalMinutes
	.section	.bss.GuiVar_StationInfoTotalMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoTotalMinutes, %object
	.size	GuiVar_StationInfoTotalMinutes, 4
GuiVar_StationInfoTotalMinutes:
	.space	4
	.global	GuiVar_StationSelectionGridControllerName
	.section	.bss.GuiVar_StationSelectionGridControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridControllerName, %object
	.size	GuiVar_StationSelectionGridControllerName, 49
GuiVar_StationSelectionGridControllerName:
	.space	49
	.global	GuiVar_StationSelectionGridGroupName
	.section	.bss.GuiVar_StationSelectionGridGroupName,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridGroupName, %object
	.size	GuiVar_StationSelectionGridGroupName, 49
GuiVar_StationSelectionGridGroupName:
	.space	49
	.global	GuiVar_StationSelectionGridMarkerPos
	.section	.bss.GuiVar_StationSelectionGridMarkerPos,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridMarkerPos, %object
	.size	GuiVar_StationSelectionGridMarkerPos, 4
GuiVar_StationSelectionGridMarkerPos:
	.space	4
	.global	GuiVar_StationSelectionGridMarkerSize
	.section	.bss.GuiVar_StationSelectionGridMarkerSize,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridMarkerSize, %object
	.size	GuiVar_StationSelectionGridMarkerSize, 4
GuiVar_StationSelectionGridMarkerSize:
	.space	4
	.global	GuiVar_StationSelectionGridScreenTitle
	.section	.bss.GuiVar_StationSelectionGridScreenTitle,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridScreenTitle, %object
	.size	GuiVar_StationSelectionGridScreenTitle, 129
GuiVar_StationSelectionGridScreenTitle:
	.space	129
	.global	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.section	.bss.GuiVar_StationSelectionGridShowOnlyStationsInThisGroup,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup, %object
	.size	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup, 4
GuiVar_StationSelectionGridShowOnlyStationsInThisGroup:
	.space	4
	.global	GuiVar_StationSelectionGridStationCount
	.section	.bss.GuiVar_StationSelectionGridStationCount,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridStationCount, %object
	.size	GuiVar_StationSelectionGridStationCount, 4
GuiVar_StationSelectionGridStationCount:
	.space	4
	.global	GuiVar_StatusChainDown
	.section	.bss.GuiVar_StatusChainDown,"aw",%nobits
	.align	2
	.type	GuiVar_StatusChainDown, %object
	.size	GuiVar_StatusChainDown, 4
GuiVar_StatusChainDown:
	.space	4
	.global	GuiVar_StatusCurrentDraw
	.section	.bss.GuiVar_StatusCurrentDraw,"aw",%nobits
	.align	2
	.type	GuiVar_StatusCurrentDraw, %object
	.size	GuiVar_StatusCurrentDraw, 4
GuiVar_StatusCurrentDraw:
	.space	4
	.global	GuiVar_StatusCycleLeft
	.section	.bss.GuiVar_StatusCycleLeft,"aw",%nobits
	.align	2
	.type	GuiVar_StatusCycleLeft, %object
	.size	GuiVar_StatusCycleLeft, 65
GuiVar_StatusCycleLeft:
	.space	65
	.global	GuiVar_StatusElectricalErrors
	.section	.bss.GuiVar_StatusElectricalErrors,"aw",%nobits
	.align	2
	.type	GuiVar_StatusElectricalErrors, %object
	.size	GuiVar_StatusElectricalErrors, 4
GuiVar_StatusElectricalErrors:
	.space	4
	.global	GuiVar_StatusETGageConnected
	.section	.bss.GuiVar_StatusETGageConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusETGageConnected, %object
	.size	GuiVar_StatusETGageConnected, 4
GuiVar_StatusETGageConnected:
	.space	4
	.global	GuiVar_StatusETGageControllerName
	.section	.bss.GuiVar_StatusETGageControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_StatusETGageControllerName, %object
	.size	GuiVar_StatusETGageControllerName, 49
GuiVar_StatusETGageControllerName:
	.space	49
	.global	GuiVar_StatusETGageReading
	.section	.bss.GuiVar_StatusETGageReading,"aw",%nobits
	.align	2
	.type	GuiVar_StatusETGageReading, %object
	.size	GuiVar_StatusETGageReading, 4
GuiVar_StatusETGageReading:
	.space	4
	.global	GuiVar_StatusFlowErrors
	.section	.bss.GuiVar_StatusFlowErrors,"aw",%nobits
	.align	2
	.type	GuiVar_StatusFlowErrors, %object
	.size	GuiVar_StatusFlowErrors, 4
GuiVar_StatusFlowErrors:
	.space	4
	.global	GuiVar_StatusFreezeSwitchConnected
	.section	.bss.GuiVar_StatusFreezeSwitchConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusFreezeSwitchConnected, %object
	.size	GuiVar_StatusFreezeSwitchConnected, 4
GuiVar_StatusFreezeSwitchConnected:
	.space	4
	.global	GuiVar_StatusFreezeSwitchState
	.section	.bss.GuiVar_StatusFreezeSwitchState,"aw",%nobits
	.align	2
	.type	GuiVar_StatusFreezeSwitchState, %object
	.size	GuiVar_StatusFreezeSwitchState, 4
GuiVar_StatusFreezeSwitchState:
	.space	4
	.global	GuiVar_StatusFuseBlown
	.section	.bss.GuiVar_StatusFuseBlown,"aw",%nobits
	.align	2
	.type	GuiVar_StatusFuseBlown, %object
	.size	GuiVar_StatusFuseBlown, 4
GuiVar_StatusFuseBlown:
	.space	4
	.global	GuiVar_StatusIrrigationActivityState
	.section	.bss.GuiVar_StatusIrrigationActivityState,"aw",%nobits
	.align	2
	.type	GuiVar_StatusIrrigationActivityState, %object
	.size	GuiVar_StatusIrrigationActivityState, 4
GuiVar_StatusIrrigationActivityState:
	.space	4
	.global	GuiVar_StatusMLBInEffect
	.section	.bss.GuiVar_StatusMLBInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_StatusMLBInEffect, %object
	.size	GuiVar_StatusMLBInEffect, 4
GuiVar_StatusMLBInEffect:
	.space	4
	.global	GuiVar_StatusMVORInEffect
	.section	.bss.GuiVar_StatusMVORInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_StatusMVORInEffect, %object
	.size	GuiVar_StatusMVORInEffect, 4
GuiVar_StatusMVORInEffect:
	.space	4
	.global	GuiVar_StatusNextScheduledIrriDate
	.section	.bss.GuiVar_StatusNextScheduledIrriDate,"aw",%nobits
	.align	2
	.type	GuiVar_StatusNextScheduledIrriDate, %object
	.size	GuiVar_StatusNextScheduledIrriDate, 49
GuiVar_StatusNextScheduledIrriDate:
	.space	49
	.global	GuiVar_StatusNextScheduledIrriGID
	.section	.bss.GuiVar_StatusNextScheduledIrriGID,"aw",%nobits
	.align	2
	.type	GuiVar_StatusNextScheduledIrriGID, %object
	.size	GuiVar_StatusNextScheduledIrriGID, 4
GuiVar_StatusNextScheduledIrriGID:
	.space	4
	.global	GuiVar_StatusNextScheduledIrriType
	.section	.bss.GuiVar_StatusNextScheduledIrriType,"aw",%nobits
	.align	2
	.type	GuiVar_StatusNextScheduledIrriType, %object
	.size	GuiVar_StatusNextScheduledIrriType, 49
GuiVar_StatusNextScheduledIrriType:
	.space	49
	.global	GuiVar_StatusNOWDaysInEffect
	.section	.bss.GuiVar_StatusNOWDaysInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_StatusNOWDaysInEffect, %object
	.size	GuiVar_StatusNOWDaysInEffect, 4
GuiVar_StatusNOWDaysInEffect:
	.space	4
	.global	GuiVar_StatusOverviewStationStr
	.section	.bss.GuiVar_StatusOverviewStationStr,"aw",%nobits
	.align	2
	.type	GuiVar_StatusOverviewStationStr, %object
	.size	GuiVar_StatusOverviewStationStr, 65
GuiVar_StatusOverviewStationStr:
	.space	65
	.global	GuiVar_StatusRainBucketConnected
	.section	.bss.GuiVar_StatusRainBucketConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusRainBucketConnected, %object
	.size	GuiVar_StatusRainBucketConnected, 4
GuiVar_StatusRainBucketConnected:
	.space	4
	.global	GuiVar_StatusRainBucketControllerName
	.section	.bss.GuiVar_StatusRainBucketControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_StatusRainBucketControllerName, %object
	.size	GuiVar_StatusRainBucketControllerName, 49
GuiVar_StatusRainBucketControllerName:
	.space	49
	.global	GuiVar_StatusRainBucketReading
	.section	.bss.GuiVar_StatusRainBucketReading,"aw",%nobits
	.align	2
	.type	GuiVar_StatusRainBucketReading, %object
	.size	GuiVar_StatusRainBucketReading, 4
GuiVar_StatusRainBucketReading:
	.space	4
	.global	GuiVar_StatusRainSwitchConnected
	.section	.bss.GuiVar_StatusRainSwitchConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusRainSwitchConnected, %object
	.size	GuiVar_StatusRainSwitchConnected, 4
GuiVar_StatusRainSwitchConnected:
	.space	4
	.global	GuiVar_StatusRainSwitchState
	.section	.bss.GuiVar_StatusRainSwitchState,"aw",%nobits
	.align	2
	.type	GuiVar_StatusRainSwitchState, %object
	.size	GuiVar_StatusRainSwitchState, 4
GuiVar_StatusRainSwitchState:
	.space	4
	.global	GuiVar_StatusShowLiveScreens
	.section	.bss.GuiVar_StatusShowLiveScreens,"aw",%nobits
	.align	2
	.type	GuiVar_StatusShowLiveScreens, %object
	.size	GuiVar_StatusShowLiveScreens, 4
GuiVar_StatusShowLiveScreens:
	.space	4
	.global	GuiVar_StatusShowSystemFlow
	.section	.bss.GuiVar_StatusShowSystemFlow,"aw",%nobits
	.align	2
	.type	GuiVar_StatusShowSystemFlow, %object
	.size	GuiVar_StatusShowSystemFlow, 4
GuiVar_StatusShowSystemFlow:
	.space	4
	.global	GuiVar_StatusSystemFlowActual
	.section	.bss.GuiVar_StatusSystemFlowActual,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowActual, %object
	.size	GuiVar_StatusSystemFlowActual, 4
GuiVar_StatusSystemFlowActual:
	.space	4
	.global	GuiVar_StatusSystemFlowExpected
	.section	.bss.GuiVar_StatusSystemFlowExpected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowExpected, %object
	.size	GuiVar_StatusSystemFlowExpected, 4
GuiVar_StatusSystemFlowExpected:
	.space	4
	.global	GuiVar_StatusSystemFlowMLBAllowed
	.section	.bss.GuiVar_StatusSystemFlowMLBAllowed,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowMLBAllowed, %object
	.size	GuiVar_StatusSystemFlowMLBAllowed, 4
GuiVar_StatusSystemFlowMLBAllowed:
	.space	4
	.global	GuiVar_StatusSystemFlowMLBMeasured
	.section	.bss.GuiVar_StatusSystemFlowMLBMeasured,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowMLBMeasured, %object
	.size	GuiVar_StatusSystemFlowMLBMeasured, 4
GuiVar_StatusSystemFlowMLBMeasured:
	.space	4
	.global	GuiVar_StatusSystemFlowShowMLBDetected
	.section	.bss.GuiVar_StatusSystemFlowShowMLBDetected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowShowMLBDetected, %object
	.size	GuiVar_StatusSystemFlowShowMLBDetected, 4
GuiVar_StatusSystemFlowShowMLBDetected:
	.space	4
	.global	GuiVar_StatusSystemFlowStatus
	.section	.bss.GuiVar_StatusSystemFlowStatus,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowStatus, %object
	.size	GuiVar_StatusSystemFlowStatus, 4
GuiVar_StatusSystemFlowStatus:
	.space	4
	.global	GuiVar_StatusSystemFlowSystem
	.section	.bss.GuiVar_StatusSystemFlowSystem,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowSystem, %object
	.size	GuiVar_StatusSystemFlowSystem, 49
GuiVar_StatusSystemFlowSystem:
	.space	49
	.global	GuiVar_StatusSystemFlowValvesOn
	.section	.bss.GuiVar_StatusSystemFlowValvesOn,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowValvesOn, %object
	.size	GuiVar_StatusSystemFlowValvesOn, 4
GuiVar_StatusSystemFlowValvesOn:
	.space	4
	.global	GuiVar_StatusTypeOfSchedule
	.section	.bss.GuiVar_StatusTypeOfSchedule,"aw",%nobits
	.align	2
	.type	GuiVar_StatusTypeOfSchedule, %object
	.size	GuiVar_StatusTypeOfSchedule, 4
GuiVar_StatusTypeOfSchedule:
	.space	4
	.global	GuiVar_StatusWindGageConnected
	.section	.bss.GuiVar_StatusWindGageConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusWindGageConnected, %object
	.size	GuiVar_StatusWindGageConnected, 4
GuiVar_StatusWindGageConnected:
	.space	4
	.global	GuiVar_StatusWindGageControllerName
	.section	.bss.GuiVar_StatusWindGageControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_StatusWindGageControllerName, %object
	.size	GuiVar_StatusWindGageControllerName, 49
GuiVar_StatusWindGageControllerName:
	.space	49
	.global	GuiVar_StatusWindGageReading
	.section	.bss.GuiVar_StatusWindGageReading,"aw",%nobits
	.align	2
	.type	GuiVar_StatusWindGageReading, %object
	.size	GuiVar_StatusWindGageReading, 4
GuiVar_StatusWindGageReading:
	.space	4
	.global	GuiVar_StatusWindPaused
	.section	.bss.GuiVar_StatusWindPaused,"aw",%nobits
	.align	2
	.type	GuiVar_StatusWindPaused, %object
	.size	GuiVar_StatusWindPaused, 4
GuiVar_StatusWindPaused:
	.space	4
	.global	GuiVar_StopKeyPending
	.section	.bss.GuiVar_StopKeyPending,"aw",%nobits
	.align	2
	.type	GuiVar_StopKeyPending, %object
	.size	GuiVar_StopKeyPending, 4
GuiVar_StopKeyPending:
	.space	4
	.global	GuiVar_StopKeyReasonInList
	.section	.bss.GuiVar_StopKeyReasonInList,"aw",%nobits
	.align	2
	.type	GuiVar_StopKeyReasonInList, %object
	.size	GuiVar_StopKeyReasonInList, 4
GuiVar_StopKeyReasonInList:
	.space	4
	.global	GuiVar_SystemFlowCheckingInUse
	.section	.bss.GuiVar_SystemFlowCheckingInUse,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingInUse, %object
	.size	GuiVar_SystemFlowCheckingInUse, 4
GuiVar_SystemFlowCheckingInUse:
	.space	4
	.global	GuiVar_SystemFlowCheckingRange1
	.section	.bss.GuiVar_SystemFlowCheckingRange1,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingRange1, %object
	.size	GuiVar_SystemFlowCheckingRange1, 4
GuiVar_SystemFlowCheckingRange1:
	.space	4
	.global	GuiVar_SystemFlowCheckingRange2
	.section	.bss.GuiVar_SystemFlowCheckingRange2,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingRange2, %object
	.size	GuiVar_SystemFlowCheckingRange2, 4
GuiVar_SystemFlowCheckingRange2:
	.space	4
	.global	GuiVar_SystemFlowCheckingRange3
	.section	.bss.GuiVar_SystemFlowCheckingRange3,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingRange3, %object
	.size	GuiVar_SystemFlowCheckingRange3, 4
GuiVar_SystemFlowCheckingRange3:
	.space	4
	.global	GuiVar_SystemFlowCheckingToleranceMinus1
	.section	.bss.GuiVar_SystemFlowCheckingToleranceMinus1,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingToleranceMinus1, %object
	.size	GuiVar_SystemFlowCheckingToleranceMinus1, 4
GuiVar_SystemFlowCheckingToleranceMinus1:
	.space	4
	.global	GuiVar_SystemFlowCheckingToleranceMinus2
	.section	.bss.GuiVar_SystemFlowCheckingToleranceMinus2,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingToleranceMinus2, %object
	.size	GuiVar_SystemFlowCheckingToleranceMinus2, 4
GuiVar_SystemFlowCheckingToleranceMinus2:
	.space	4
	.global	GuiVar_SystemFlowCheckingToleranceMinus3
	.section	.bss.GuiVar_SystemFlowCheckingToleranceMinus3,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingToleranceMinus3, %object
	.size	GuiVar_SystemFlowCheckingToleranceMinus3, 4
GuiVar_SystemFlowCheckingToleranceMinus3:
	.space	4
	.global	GuiVar_SystemFlowCheckingToleranceMinus4
	.section	.bss.GuiVar_SystemFlowCheckingToleranceMinus4,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingToleranceMinus4, %object
	.size	GuiVar_SystemFlowCheckingToleranceMinus4, 4
GuiVar_SystemFlowCheckingToleranceMinus4:
	.space	4
	.global	GuiVar_SystemFlowCheckingTolerancePlus1
	.section	.bss.GuiVar_SystemFlowCheckingTolerancePlus1,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingTolerancePlus1, %object
	.size	GuiVar_SystemFlowCheckingTolerancePlus1, 4
GuiVar_SystemFlowCheckingTolerancePlus1:
	.space	4
	.global	GuiVar_SystemFlowCheckingTolerancePlus2
	.section	.bss.GuiVar_SystemFlowCheckingTolerancePlus2,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingTolerancePlus2, %object
	.size	GuiVar_SystemFlowCheckingTolerancePlus2, 4
GuiVar_SystemFlowCheckingTolerancePlus2:
	.space	4
	.global	GuiVar_SystemFlowCheckingTolerancePlus3
	.section	.bss.GuiVar_SystemFlowCheckingTolerancePlus3,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingTolerancePlus3, %object
	.size	GuiVar_SystemFlowCheckingTolerancePlus3, 4
GuiVar_SystemFlowCheckingTolerancePlus3:
	.space	4
	.global	GuiVar_SystemFlowCheckingTolerancePlus4
	.section	.bss.GuiVar_SystemFlowCheckingTolerancePlus4,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingTolerancePlus4, %object
	.size	GuiVar_SystemFlowCheckingTolerancePlus4, 4
GuiVar_SystemFlowCheckingTolerancePlus4:
	.space	4
	.global	GuiVar_SystemInUse
	.section	.bss.GuiVar_SystemInUse,"aw",%nobits
	.align	2
	.type	GuiVar_SystemInUse, %object
	.size	GuiVar_SystemInUse, 4
GuiVar_SystemInUse:
	.space	4
	.global	GuiVar_SystemName
	.section	.bss.GuiVar_SystemName,"aw",%nobits
	.align	2
	.type	GuiVar_SystemName, %object
	.size	GuiVar_SystemName, 49
GuiVar_SystemName:
	.space	49
	.global	GuiVar_SystemPOCSelected
	.section	.bss.GuiVar_SystemPOCSelected,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPOCSelected, %object
	.size	GuiVar_SystemPOCSelected, 4
GuiVar_SystemPOCSelected:
	.space	4
	.global	GuiVar_SystemPreserves5SecMostRecent
	.section	.bss.GuiVar_SystemPreserves5SecMostRecent,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPreserves5SecMostRecent, %object
	.size	GuiVar_SystemPreserves5SecMostRecent, 4
GuiVar_SystemPreserves5SecMostRecent:
	.space	4
	.global	GuiVar_SystemPreserves5SecNext
	.section	.bss.GuiVar_SystemPreserves5SecNext,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPreserves5SecNext, %object
	.size	GuiVar_SystemPreserves5SecNext, 4
GuiVar_SystemPreserves5SecNext:
	.space	4
	.global	GuiVar_SystemPreservesStabilityMostRecent5Sec
	.section	.bss.GuiVar_SystemPreservesStabilityMostRecent5Sec,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPreservesStabilityMostRecent5Sec, %object
	.size	GuiVar_SystemPreservesStabilityMostRecent5Sec, 4
GuiVar_SystemPreservesStabilityMostRecent5Sec:
	.space	4
	.global	GuiVar_SystemPreservesStabilityNext
	.section	.bss.GuiVar_SystemPreservesStabilityNext,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPreservesStabilityNext, %object
	.size	GuiVar_SystemPreservesStabilityNext, 4
GuiVar_SystemPreservesStabilityNext:
	.space	4
	.global	GuiVar_TaskList
	.section	.bss.GuiVar_TaskList,"aw",%nobits
	.align	2
	.type	GuiVar_TaskList, %object
	.size	GuiVar_TaskList, 2001
GuiVar_TaskList:
	.space	2001
	.global	GuiVar_TestRunTime
	.section	.bss.GuiVar_TestRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_TestRunTime, %object
	.size	GuiVar_TestRunTime, 4
GuiVar_TestRunTime:
	.space	4
	.global	GuiVar_TestStationAutoMode
	.section	.bss.GuiVar_TestStationAutoMode,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationAutoMode, %object
	.size	GuiVar_TestStationAutoMode, 4
GuiVar_TestStationAutoMode:
	.space	4
	.global	GuiVar_TestStationAutoOffTime
	.section	.bss.GuiVar_TestStationAutoOffTime,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationAutoOffTime, %object
	.size	GuiVar_TestStationAutoOffTime, 4
GuiVar_TestStationAutoOffTime:
	.space	4
	.global	GuiVar_TestStationAutoOnTime
	.section	.bss.GuiVar_TestStationAutoOnTime,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationAutoOnTime, %object
	.size	GuiVar_TestStationAutoOnTime, 4
GuiVar_TestStationAutoOnTime:
	.space	4
	.global	GuiVar_TestStationOnOff
	.section	.bss.GuiVar_TestStationOnOff,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationOnOff, %object
	.size	GuiVar_TestStationOnOff, 4
GuiVar_TestStationOnOff:
	.space	4
	.global	GuiVar_TestStationStatus
	.section	.bss.GuiVar_TestStationStatus,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationStatus, %object
	.size	GuiVar_TestStationStatus, 4
GuiVar_TestStationStatus:
	.space	4
	.global	GuiVar_TestStationTimeDisplay
	.section	.bss.GuiVar_TestStationTimeDisplay,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationTimeDisplay, %object
	.size	GuiVar_TestStationTimeDisplay, 4
GuiVar_TestStationTimeDisplay:
	.space	4
	.global	GuiVar_TestStationTimeRemaining
	.section	.bss.GuiVar_TestStationTimeRemaining,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationTimeRemaining, %object
	.size	GuiVar_TestStationTimeRemaining, 4
GuiVar_TestStationTimeRemaining:
	.space	4
	.global	GuiVar_TurnOffControllerIsOff
	.section	.bss.GuiVar_TurnOffControllerIsOff,"aw",%nobits
	.align	2
	.type	GuiVar_TurnOffControllerIsOff, %object
	.size	GuiVar_TurnOffControllerIsOff, 4
GuiVar_TurnOffControllerIsOff:
	.space	4
	.global	GuiVar_TwoWireDecoderFWVersion
	.section	.bss.GuiVar_TwoWireDecoderFWVersion,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDecoderFWVersion, %object
	.size	GuiVar_TwoWireDecoderFWVersion, 4
GuiVar_TwoWireDecoderFWVersion:
	.space	4
	.global	GuiVar_TwoWireDecoderSN
	.section	.bss.GuiVar_TwoWireDecoderSN,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDecoderSN, %object
	.size	GuiVar_TwoWireDecoderSN, 4
GuiVar_TwoWireDecoderSN:
	.space	4
	.global	GuiVar_TwoWireDecoderSNToAssign
	.section	.bss.GuiVar_TwoWireDecoderSNToAssign,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDecoderSNToAssign, %object
	.size	GuiVar_TwoWireDecoderSNToAssign, 4
GuiVar_TwoWireDecoderSNToAssign:
	.space	4
	.global	GuiVar_TwoWireDecoderType
	.section	.bss.GuiVar_TwoWireDecoderType,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDecoderType, %object
	.size	GuiVar_TwoWireDecoderType, 4
GuiVar_TwoWireDecoderType:
	.space	4
	.global	GuiVar_TwoWireDescA
	.section	.bss.GuiVar_TwoWireDescA,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDescA, %object
	.size	GuiVar_TwoWireDescA, 49
GuiVar_TwoWireDescA:
	.space	49
	.global	GuiVar_TwoWireDescB
	.section	.bss.GuiVar_TwoWireDescB,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDescB, %object
	.size	GuiVar_TwoWireDescB, 49
GuiVar_TwoWireDescB:
	.space	49
	.global	GuiVar_TwoWireDiscoveredMoisDecoders
	.section	.bss.GuiVar_TwoWireDiscoveredMoisDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveredMoisDecoders, %object
	.size	GuiVar_TwoWireDiscoveredMoisDecoders, 4
GuiVar_TwoWireDiscoveredMoisDecoders:
	.space	4
	.global	GuiVar_TwoWireDiscoveredPOCDecoders
	.section	.bss.GuiVar_TwoWireDiscoveredPOCDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveredPOCDecoders, %object
	.size	GuiVar_TwoWireDiscoveredPOCDecoders, 4
GuiVar_TwoWireDiscoveredPOCDecoders:
	.space	4
	.global	GuiVar_TwoWireDiscoveredStaDecoders
	.section	.bss.GuiVar_TwoWireDiscoveredStaDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveredStaDecoders, %object
	.size	GuiVar_TwoWireDiscoveredStaDecoders, 4
GuiVar_TwoWireDiscoveredStaDecoders:
	.space	4
	.global	GuiVar_TwoWireDiscoveryProgressBar
	.section	.bss.GuiVar_TwoWireDiscoveryProgressBar,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveryProgressBar, %object
	.size	GuiVar_TwoWireDiscoveryProgressBar, 4
GuiVar_TwoWireDiscoveryProgressBar:
	.space	4
	.global	GuiVar_TwoWireDiscoveryState
	.section	.bss.GuiVar_TwoWireDiscoveryState,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveryState, %object
	.size	GuiVar_TwoWireDiscoveryState, 4
GuiVar_TwoWireDiscoveryState:
	.space	4
	.global	GuiVar_TwoWireNumDiscoveredMoisDecoders
	.section	.bss.GuiVar_TwoWireNumDiscoveredMoisDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireNumDiscoveredMoisDecoders, %object
	.size	GuiVar_TwoWireNumDiscoveredMoisDecoders, 4
GuiVar_TwoWireNumDiscoveredMoisDecoders:
	.space	4
	.global	GuiVar_TwoWireNumDiscoveredPOCDecoders
	.section	.bss.GuiVar_TwoWireNumDiscoveredPOCDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireNumDiscoveredPOCDecoders, %object
	.size	GuiVar_TwoWireNumDiscoveredPOCDecoders, 4
GuiVar_TwoWireNumDiscoveredPOCDecoders:
	.space	4
	.global	GuiVar_TwoWireNumDiscoveredStaDecoders
	.section	.bss.GuiVar_TwoWireNumDiscoveredStaDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireNumDiscoveredStaDecoders, %object
	.size	GuiVar_TwoWireNumDiscoveredStaDecoders, 4
GuiVar_TwoWireNumDiscoveredStaDecoders:
	.space	4
	.global	GuiVar_TwoWireOrphanedPOCsExist
	.section	.bss.GuiVar_TwoWireOrphanedPOCsExist,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireOrphanedPOCsExist, %object
	.size	GuiVar_TwoWireOrphanedPOCsExist, 4
GuiVar_TwoWireOrphanedPOCsExist:
	.space	4
	.global	GuiVar_TwoWireOrphanedStationsExist
	.section	.bss.GuiVar_TwoWireOrphanedStationsExist,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireOrphanedStationsExist, %object
	.size	GuiVar_TwoWireOrphanedStationsExist, 4
GuiVar_TwoWireOrphanedStationsExist:
	.space	4
	.global	GuiVar_TwoWireOutputOnA
	.section	.bss.GuiVar_TwoWireOutputOnA,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireOutputOnA, %object
	.size	GuiVar_TwoWireOutputOnA, 4
GuiVar_TwoWireOutputOnA:
	.space	4
	.global	GuiVar_TwoWireOutputOnB
	.section	.bss.GuiVar_TwoWireOutputOnB,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireOutputOnB, %object
	.size	GuiVar_TwoWireOutputOnB, 4
GuiVar_TwoWireOutputOnB:
	.space	4
	.global	GuiVar_TwoWirePOCDecoderIndex
	.section	.bss.GuiVar_TwoWirePOCDecoderIndex,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWirePOCDecoderIndex, %object
	.size	GuiVar_TwoWirePOCDecoderIndex, 4
GuiVar_TwoWirePOCDecoderIndex:
	.space	4
	.global	GuiVar_TwoWirePOCInUse
	.section	.bss.GuiVar_TwoWirePOCInUse,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWirePOCInUse, %object
	.size	GuiVar_TwoWirePOCInUse, 4
GuiVar_TwoWirePOCInUse:
	.space	4
	.global	GuiVar_TwoWirePOCUsedForBypass
	.section	.bss.GuiVar_TwoWirePOCUsedForBypass,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWirePOCUsedForBypass, %object
	.size	GuiVar_TwoWirePOCUsedForBypass, 4
GuiVar_TwoWirePOCUsedForBypass:
	.space	4
	.global	GuiVar_TwoWireStaA
	.section	.bss.GuiVar_TwoWireStaA,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaA, %object
	.size	GuiVar_TwoWireStaA, 4
GuiVar_TwoWireStaA:
	.space	4
	.global	GuiVar_TwoWireStaAIsSet
	.section	.bss.GuiVar_TwoWireStaAIsSet,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaAIsSet, %object
	.size	GuiVar_TwoWireStaAIsSet, 4
GuiVar_TwoWireStaAIsSet:
	.space	4
	.global	GuiVar_TwoWireStaB
	.section	.bss.GuiVar_TwoWireStaB,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaB, %object
	.size	GuiVar_TwoWireStaB, 4
GuiVar_TwoWireStaB:
	.space	4
	.global	GuiVar_TwoWireStaBIsSet
	.section	.bss.GuiVar_TwoWireStaBIsSet,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaBIsSet, %object
	.size	GuiVar_TwoWireStaBIsSet, 4
GuiVar_TwoWireStaBIsSet:
	.space	4
	.global	GuiVar_TwoWireStaDecoderIndex
	.section	.bss.GuiVar_TwoWireStaDecoderIndex,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaDecoderIndex, %object
	.size	GuiVar_TwoWireStaDecoderIndex, 4
GuiVar_TwoWireStaDecoderIndex:
	.space	4
	.global	GuiVar_TwoWireStaStrA
	.section	.bss.GuiVar_TwoWireStaStrA,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaStrA, %object
	.size	GuiVar_TwoWireStaStrA, 5
GuiVar_TwoWireStaStrA:
	.space	5
	.global	GuiVar_TwoWireStaStrB
	.section	.bss.GuiVar_TwoWireStaStrB,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaStrB, %object
	.size	GuiVar_TwoWireStaStrB, 5
GuiVar_TwoWireStaStrB:
	.space	5
	.global	GuiVar_TwoWireStatsBODs
	.section	.bss.GuiVar_TwoWireStatsBODs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsBODs, %object
	.size	GuiVar_TwoWireStatsBODs, 4
GuiVar_TwoWireStatsBODs:
	.space	4
	.global	GuiVar_TwoWireStatsEEPCRCComParams
	.section	.bss.GuiVar_TwoWireStatsEEPCRCComParams,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsEEPCRCComParams, %object
	.size	GuiVar_TwoWireStatsEEPCRCComParams, 4
GuiVar_TwoWireStatsEEPCRCComParams:
	.space	4
	.global	GuiVar_TwoWireStatsEEPCRCErrStats
	.section	.bss.GuiVar_TwoWireStatsEEPCRCErrStats,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsEEPCRCErrStats, %object
	.size	GuiVar_TwoWireStatsEEPCRCErrStats, 4
GuiVar_TwoWireStatsEEPCRCErrStats:
	.space	4
	.global	GuiVar_TwoWireStatsEEPCRCS1Params
	.section	.bss.GuiVar_TwoWireStatsEEPCRCS1Params,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsEEPCRCS1Params, %object
	.size	GuiVar_TwoWireStatsEEPCRCS1Params, 4
GuiVar_TwoWireStatsEEPCRCS1Params:
	.space	4
	.global	GuiVar_TwoWireStatsEEPCRCS2Params
	.section	.bss.GuiVar_TwoWireStatsEEPCRCS2Params,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsEEPCRCS2Params, %object
	.size	GuiVar_TwoWireStatsEEPCRCS2Params, 4
GuiVar_TwoWireStatsEEPCRCS2Params:
	.space	4
	.global	GuiVar_TwoWireStatsPORs
	.section	.bss.GuiVar_TwoWireStatsPORs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsPORs, %object
	.size	GuiVar_TwoWireStatsPORs, 4
GuiVar_TwoWireStatsPORs:
	.space	4
	.global	GuiVar_TwoWireStatsRxCRCErrs
	.section	.bss.GuiVar_TwoWireStatsRxCRCErrs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxCRCErrs, %object
	.size	GuiVar_TwoWireStatsRxCRCErrs, 4
GuiVar_TwoWireStatsRxCRCErrs:
	.space	4
	.global	GuiVar_TwoWireStatsRxDataLpbkMsgs
	.section	.bss.GuiVar_TwoWireStatsRxDataLpbkMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxDataLpbkMsgs, %object
	.size	GuiVar_TwoWireStatsRxDataLpbkMsgs, 4
GuiVar_TwoWireStatsRxDataLpbkMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxDecRstMsgs
	.section	.bss.GuiVar_TwoWireStatsRxDecRstMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxDecRstMsgs, %object
	.size	GuiVar_TwoWireStatsRxDecRstMsgs, 4
GuiVar_TwoWireStatsRxDecRstMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxDiscConfMsgs
	.section	.bss.GuiVar_TwoWireStatsRxDiscConfMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxDiscConfMsgs, %object
	.size	GuiVar_TwoWireStatsRxDiscConfMsgs, 4
GuiVar_TwoWireStatsRxDiscConfMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxDiscRstMsgs
	.section	.bss.GuiVar_TwoWireStatsRxDiscRstMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxDiscRstMsgs, %object
	.size	GuiVar_TwoWireStatsRxDiscRstMsgs, 4
GuiVar_TwoWireStatsRxDiscRstMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxEnqMsgs
	.section	.bss.GuiVar_TwoWireStatsRxEnqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxEnqMsgs, %object
	.size	GuiVar_TwoWireStatsRxEnqMsgs, 4
GuiVar_TwoWireStatsRxEnqMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxGetParamsMsgs
	.section	.bss.GuiVar_TwoWireStatsRxGetParamsMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxGetParamsMsgs, %object
	.size	GuiVar_TwoWireStatsRxGetParamsMsgs, 4
GuiVar_TwoWireStatsRxGetParamsMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxIDReqMsgs
	.section	.bss.GuiVar_TwoWireStatsRxIDReqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxIDReqMsgs, %object
	.size	GuiVar_TwoWireStatsRxIDReqMsgs, 4
GuiVar_TwoWireStatsRxIDReqMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxLongMsgs
	.section	.bss.GuiVar_TwoWireStatsRxLongMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxLongMsgs, %object
	.size	GuiVar_TwoWireStatsRxLongMsgs, 4
GuiVar_TwoWireStatsRxLongMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxMsgs
	.section	.bss.GuiVar_TwoWireStatsRxMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxMsgs, %object
	.size	GuiVar_TwoWireStatsRxMsgs, 4
GuiVar_TwoWireStatsRxMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxPutParamsMsgs
	.section	.bss.GuiVar_TwoWireStatsRxPutParamsMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxPutParamsMsgs, %object
	.size	GuiVar_TwoWireStatsRxPutParamsMsgs, 4
GuiVar_TwoWireStatsRxPutParamsMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxSolCtrlMsgs
	.section	.bss.GuiVar_TwoWireStatsRxSolCtrlMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxSolCtrlMsgs, %object
	.size	GuiVar_TwoWireStatsRxSolCtrlMsgs, 4
GuiVar_TwoWireStatsRxSolCtrlMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs
	.section	.bss.GuiVar_TwoWireStatsRxSolCurMeasReqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs, %object
	.size	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs, 4
GuiVar_TwoWireStatsRxSolCurMeasReqMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxStatReqMsgs
	.section	.bss.GuiVar_TwoWireStatsRxStatReqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxStatReqMsgs, %object
	.size	GuiVar_TwoWireStatsRxStatReqMsgs, 4
GuiVar_TwoWireStatsRxStatReqMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsRxStatsReqMsgs
	.section	.bss.GuiVar_TwoWireStatsRxStatsReqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxStatsReqMsgs, %object
	.size	GuiVar_TwoWireStatsRxStatsReqMsgs, 4
GuiVar_TwoWireStatsRxStatsReqMsgs:
	.space	4
	.global	GuiVar_TwoWireStatsS1UCos
	.section	.bss.GuiVar_TwoWireStatsS1UCos,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsS1UCos, %object
	.size	GuiVar_TwoWireStatsS1UCos, 4
GuiVar_TwoWireStatsS1UCos:
	.space	4
	.global	GuiVar_TwoWireStatsS2UCos
	.section	.bss.GuiVar_TwoWireStatsS2UCos,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsS2UCos, %object
	.size	GuiVar_TwoWireStatsS2UCos, 4
GuiVar_TwoWireStatsS2UCos:
	.space	4
	.global	GuiVar_TwoWireStatsTempCur
	.section	.bss.GuiVar_TwoWireStatsTempCur,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsTempCur, %object
	.size	GuiVar_TwoWireStatsTempCur, 4
GuiVar_TwoWireStatsTempCur:
	.space	4
	.global	GuiVar_TwoWireStatsTempMax
	.section	.bss.GuiVar_TwoWireStatsTempMax,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsTempMax, %object
	.size	GuiVar_TwoWireStatsTempMax, 4
GuiVar_TwoWireStatsTempMax:
	.space	4
	.global	GuiVar_TwoWireStatsTxAcksSent
	.section	.bss.GuiVar_TwoWireStatsTxAcksSent,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsTxAcksSent, %object
	.size	GuiVar_TwoWireStatsTxAcksSent, 4
GuiVar_TwoWireStatsTxAcksSent:
	.space	4
	.global	GuiVar_TwoWireStatsWDTs
	.section	.bss.GuiVar_TwoWireStatsWDTs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsWDTs, %object
	.size	GuiVar_TwoWireStatsWDTs, 4
GuiVar_TwoWireStatsWDTs:
	.space	4
	.global	GuiVar_UnusedGroupsRemoved
	.section	.bss.GuiVar_UnusedGroupsRemoved,"aw",%nobits
	.align	2
	.type	GuiVar_UnusedGroupsRemoved, %object
	.size	GuiVar_UnusedGroupsRemoved, 4
GuiVar_UnusedGroupsRemoved:
	.space	4
	.global	GuiVar_UnusedGroupsRemovedGreaterThan1
	.section	.bss.GuiVar_UnusedGroupsRemovedGreaterThan1,"aw",%nobits
	.align	2
	.type	GuiVar_UnusedGroupsRemovedGreaterThan1, %object
	.size	GuiVar_UnusedGroupsRemovedGreaterThan1, 4
GuiVar_UnusedGroupsRemovedGreaterThan1:
	.space	4
	.global	GuiVar_Volume
	.section	.bss.GuiVar_Volume,"aw",%nobits
	.align	2
	.type	GuiVar_Volume, %object
	.size	GuiVar_Volume, 4
GuiVar_Volume:
	.space	4
	.global	GuiVar_VolumeSliderPos
	.section	.bss.GuiVar_VolumeSliderPos,"aw",%nobits
	.align	2
	.type	GuiVar_VolumeSliderPos, %object
	.size	GuiVar_VolumeSliderPos, 4
GuiVar_VolumeSliderPos:
	.space	4
	.global	GuiVar_WalkThruBoxIndex
	.section	.bss.GuiVar_WalkThruBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruBoxIndex, %object
	.size	GuiVar_WalkThruBoxIndex, 4
GuiVar_WalkThruBoxIndex:
	.space	4
	.global	GuiVar_WalkThruControllerName
	.section	.bss.GuiVar_WalkThruControllerName,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruControllerName, %object
	.size	GuiVar_WalkThruControllerName, 49
GuiVar_WalkThruControllerName:
	.space	49
	.global	GuiVar_WalkThruCountingDown
	.section	.bss.GuiVar_WalkThruCountingDown,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruCountingDown, %object
	.size	GuiVar_WalkThruCountingDown, 4
GuiVar_WalkThruCountingDown:
	.space	4
	.global	GuiVar_WalkThruDelay
	.section	.bss.GuiVar_WalkThruDelay,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruDelay, %object
	.size	GuiVar_WalkThruDelay, 4
GuiVar_WalkThruDelay:
	.space	4
	.global	GuiVar_WalkThruOrder
	.section	.bss.GuiVar_WalkThruOrder,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruOrder, %object
	.size	GuiVar_WalkThruOrder, 4
GuiVar_WalkThruOrder:
	.space	4
	.global	GuiVar_WalkThruRunTime
	.section	.bss.GuiVar_WalkThruRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruRunTime, %object
	.size	GuiVar_WalkThruRunTime, 4
GuiVar_WalkThruRunTime:
	.space	4
	.global	GuiVar_WalkThruShowStartInSeconds
	.section	.bss.GuiVar_WalkThruShowStartInSeconds,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruShowStartInSeconds, %object
	.size	GuiVar_WalkThruShowStartInSeconds, 4
GuiVar_WalkThruShowStartInSeconds:
	.space	4
	.global	GuiVar_WalkThruStation_Str
	.section	.bss.GuiVar_WalkThruStation_Str,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruStation_Str, %object
	.size	GuiVar_WalkThruStation_Str, 4
GuiVar_WalkThruStation_Str:
	.space	4
	.global	GuiVar_WalkThruStationDesc
	.section	.bss.GuiVar_WalkThruStationDesc,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruStationDesc, %object
	.size	GuiVar_WalkThruStationDesc, 49
GuiVar_WalkThruStationDesc:
	.space	49
	.global	GuiVar_WENChangeCredentials
	.section	.bss.GuiVar_WENChangeCredentials,"aw",%nobits
	.align	2
	.type	GuiVar_WENChangeCredentials, %object
	.size	GuiVar_WENChangeCredentials, 4
GuiVar_WENChangeCredentials:
	.space	4
	.global	GuiVar_WENChangeKey
	.section	.bss.GuiVar_WENChangeKey,"aw",%nobits
	.align	2
	.type	GuiVar_WENChangeKey, %object
	.size	GuiVar_WENChangeKey, 4
GuiVar_WENChangeKey:
	.space	4
	.global	GuiVar_WENChangePassphrase
	.section	.bss.GuiVar_WENChangePassphrase,"aw",%nobits
	.align	2
	.type	GuiVar_WENChangePassphrase, %object
	.size	GuiVar_WENChangePassphrase, 4
GuiVar_WENChangePassphrase:
	.space	4
	.global	GuiVar_WENChangePassword
	.section	.bss.GuiVar_WENChangePassword,"aw",%nobits
	.align	2
	.type	GuiVar_WENChangePassword, %object
	.size	GuiVar_WENChangePassword, 4
GuiVar_WENChangePassword:
	.space	4
	.global	GuiVar_WENKey
	.section	.bss.GuiVar_WENKey,"aw",%nobits
	.align	2
	.type	GuiVar_WENKey, %object
	.size	GuiVar_WENKey, 65
GuiVar_WENKey:
	.space	65
	.global	GuiVar_WENMACAddress
	.section	.bss.GuiVar_WENMACAddress,"aw",%nobits
	.align	2
	.type	GuiVar_WENMACAddress, %object
	.size	GuiVar_WENMACAddress, 49
GuiVar_WENMACAddress:
	.space	49
	.global	GuiVar_WENPassphrase
	.section	.bss.GuiVar_WENPassphrase,"aw",%nobits
	.align	2
	.type	GuiVar_WENPassphrase, %object
	.size	GuiVar_WENPassphrase, 65
GuiVar_WENPassphrase:
	.space	65
	.global	GuiVar_WENRadioMode
	.section	.bss.GuiVar_WENRadioMode,"aw",%nobits
	.align	2
	.type	GuiVar_WENRadioMode, %object
	.size	GuiVar_WENRadioMode, 4
GuiVar_WENRadioMode:
	.space	4
	.global	GuiVar_WENRadioType
	.section	.bss.GuiVar_WENRadioType,"aw",%nobits
	.align	2
	.type	GuiVar_WENRadioType, %object
	.size	GuiVar_WENRadioType, 4
GuiVar_WENRadioType:
	.space	4
	.global	GuiVar_WENRSSI
	.section	.bss.GuiVar_WENRSSI,"aw",%nobits
	.align	2
	.type	GuiVar_WENRSSI, %object
	.size	GuiVar_WENRSSI, 4
GuiVar_WENRSSI:
	.space	4
	.global	GuiVar_WENSecurity
	.section	.bss.GuiVar_WENSecurity,"aw",%nobits
	.align	2
	.type	GuiVar_WENSecurity, %object
	.size	GuiVar_WENSecurity, 4
GuiVar_WENSecurity:
	.space	4
	.global	GuiVar_WENSSID
	.section	.bss.GuiVar_WENSSID,"aw",%nobits
	.align	2
	.type	GuiVar_WENSSID, %object
	.size	GuiVar_WENSSID, 33
GuiVar_WENSSID:
	.space	33
	.global	GuiVar_WENStatus
	.section	.bss.GuiVar_WENStatus,"aw",%nobits
	.align	2
	.type	GuiVar_WENStatus, %object
	.size	GuiVar_WENStatus, 49
GuiVar_WENStatus:
	.space	49
	.global	GuiVar_WENWEPAuthentication
	.section	.bss.GuiVar_WENWEPAuthentication,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPAuthentication, %object
	.size	GuiVar_WENWEPAuthentication, 4
GuiVar_WENWEPAuthentication:
	.space	4
	.global	GuiVar_WENWEPKeySize
	.section	.bss.GuiVar_WENWEPKeySize,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPKeySize, %object
	.size	GuiVar_WENWEPKeySize, 4
GuiVar_WENWEPKeySize:
	.space	4
	.global	GuiVar_WENWEPKeySizeChanged
	.section	.bss.GuiVar_WENWEPKeySizeChanged,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPKeySizeChanged, %object
	.size	GuiVar_WENWEPKeySizeChanged, 4
GuiVar_WENWEPKeySizeChanged:
	.space	4
	.global	GuiVar_WENWEPKeyType
	.section	.bss.GuiVar_WENWEPKeyType,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPKeyType, %object
	.size	GuiVar_WENWEPKeyType, 4
GuiVar_WENWEPKeyType:
	.space	4
	.global	GuiVar_WENWEPShowTXKeyIndex
	.section	.bss.GuiVar_WENWEPShowTXKeyIndex,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPShowTXKeyIndex, %object
	.size	GuiVar_WENWEPShowTXKeyIndex, 4
GuiVar_WENWEPShowTXKeyIndex:
	.space	4
	.global	GuiVar_WENWEPTXKey
	.section	.bss.GuiVar_WENWEPTXKey,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPTXKey, %object
	.size	GuiVar_WENWEPTXKey, 4
GuiVar_WENWEPTXKey:
	.space	4
	.global	GuiVar_WENWPA2Encryption
	.section	.bss.GuiVar_WENWPA2Encryption,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPA2Encryption, %object
	.size	GuiVar_WENWPA2Encryption, 4
GuiVar_WENWPA2Encryption:
	.space	4
	.global	GuiVar_WENWPAAuthentication
	.section	.bss.GuiVar_WENWPAAuthentication,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAAuthentication, %object
	.size	GuiVar_WENWPAAuthentication, 4
GuiVar_WENWPAAuthentication:
	.space	4
	.global	GuiVar_WENWPAEAPTLSCredentials
	.section	.bss.GuiVar_WENWPAEAPTLSCredentials,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEAPTLSCredentials, %object
	.size	GuiVar_WENWPAEAPTLSCredentials, 65
GuiVar_WENWPAEAPTLSCredentials:
	.space	65
	.global	GuiVar_WENWPAEAPTLSValidateCert
	.section	.bss.GuiVar_WENWPAEAPTLSValidateCert,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEAPTLSValidateCert, %object
	.size	GuiVar_WENWPAEAPTLSValidateCert, 4
GuiVar_WENWPAEAPTLSValidateCert:
	.space	4
	.global	GuiVar_WENWPAEAPTTLSOption
	.section	.bss.GuiVar_WENWPAEAPTTLSOption,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEAPTTLSOption, %object
	.size	GuiVar_WENWPAEAPTTLSOption, 4
GuiVar_WENWPAEAPTTLSOption:
	.space	4
	.global	GuiVar_WENWPAEncryption
	.section	.bss.GuiVar_WENWPAEncryption,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEncryption, %object
	.size	GuiVar_WENWPAEncryption, 4
GuiVar_WENWPAEncryption:
	.space	4
	.global	GuiVar_WENWPAEncryptionCCMP
	.section	.bss.GuiVar_WENWPAEncryptionCCMP,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEncryptionCCMP, %object
	.size	GuiVar_WENWPAEncryptionCCMP, 4
GuiVar_WENWPAEncryptionCCMP:
	.space	4
	.global	GuiVar_WENWPAEncryptionTKIP
	.section	.bss.GuiVar_WENWPAEncryptionTKIP,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEncryptionTKIP, %object
	.size	GuiVar_WENWPAEncryptionTKIP, 4
GuiVar_WENWPAEncryptionTKIP:
	.space	4
	.global	GuiVar_WENWPAEncryptionWEP
	.section	.bss.GuiVar_WENWPAEncryptionWEP,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEncryptionWEP, %object
	.size	GuiVar_WENWPAEncryptionWEP, 4
GuiVar_WENWPAEncryptionWEP:
	.space	4
	.global	GuiVar_WENWPAIEEE8021X
	.section	.bss.GuiVar_WENWPAIEEE8021X,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAIEEE8021X, %object
	.size	GuiVar_WENWPAIEEE8021X, 4
GuiVar_WENWPAIEEE8021X:
	.space	4
	.global	GuiVar_WENWPAKeyType
	.section	.bss.GuiVar_WENWPAKeyType,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAKeyType, %object
	.size	GuiVar_WENWPAKeyType, 4
GuiVar_WENWPAKeyType:
	.space	4
	.global	GuiVar_WENWPAPassword
	.section	.bss.GuiVar_WENWPAPassword,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAPassword, %object
	.size	GuiVar_WENWPAPassword, 64
GuiVar_WENWPAPassword:
	.space	64
	.global	GuiVar_WENWPAPEAPOption
	.section	.bss.GuiVar_WENWPAPEAPOption,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAPEAPOption, %object
	.size	GuiVar_WENWPAPEAPOption, 4
GuiVar_WENWPAPEAPOption:
	.space	4
	.global	GuiVar_WENWPAUsername
	.section	.bss.GuiVar_WENWPAUsername,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAUsername, %object
	.size	GuiVar_WENWPAUsername, 64
GuiVar_WENWPAUsername:
	.space	64
	.global	GuiVar_WindGageInstalledAt
	.section	.bss.GuiVar_WindGageInstalledAt,"aw",%nobits
	.align	2
	.type	GuiVar_WindGageInstalledAt, %object
	.size	GuiVar_WindGageInstalledAt, 3
GuiVar_WindGageInstalledAt:
	.space	3
	.global	GuiVar_WindGageInUse
	.section	.bss.GuiVar_WindGageInUse,"aw",%nobits
	.align	2
	.type	GuiVar_WindGageInUse, %object
	.size	GuiVar_WindGageInUse, 4
GuiVar_WindGageInUse:
	.space	4
	.global	GuiVar_WindGagePauseSpeed
	.section	.bss.GuiVar_WindGagePauseSpeed,"aw",%nobits
	.align	2
	.type	GuiVar_WindGagePauseSpeed, %object
	.size	GuiVar_WindGagePauseSpeed, 4
GuiVar_WindGagePauseSpeed:
	.space	4
	.global	GuiVar_WindGageResumeSpeed
	.section	.bss.GuiVar_WindGageResumeSpeed,"aw",%nobits
	.align	2
	.type	GuiVar_WindGageResumeSpeed, %object
	.size	GuiVar_WindGageResumeSpeed, 4
GuiVar_WindGageResumeSpeed:
	.space	4
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x4d7c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF1038
	.byte	0x1
	.4byte	.LASF1039
	.4byte	.Ltext0
	.4byte	.Letext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	0x3f
	.4byte	0x38
	.uleb128 0x4
	.4byte	0x38
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0x30
	.4byte	0x5e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x6
	.4byte	0x28
	.uleb128 0x5
	.4byte	.LASF5
	.byte	0x1
	.byte	0x34
	.4byte	0x74
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x6
	.4byte	0x28
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.byte	0x36
	.4byte	0x8a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x6
	.4byte	0x28
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.byte	0x38
	.4byte	0xa0
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x6
	.4byte	0x28
	.uleb128 0x7
	.4byte	.LASF9
	.byte	0x2
	.byte	0x1e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutAntennaHoles
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x7
	.4byte	.LASF10
	.byte	0x2
	.byte	0x1f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutAquaponicsOption
	.uleb128 0x7
	.4byte	.LASF11
	.byte	0x2
	.byte	0x20
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutDebug
	.uleb128 0x7
	.4byte	.LASF12
	.byte	0x2
	.byte	0x21
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutFLOption
	.uleb128 0x7
	.4byte	.LASF13
	.byte	0x2
	.byte	0x22
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutHubOption
	.uleb128 0x7
	.4byte	.LASF14
	.byte	0x2
	.byte	0x23
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutLOption
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x128
	.uleb128 0x4
	.4byte	0x38
	.byte	0x30
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF15
	.uleb128 0x7
	.4byte	.LASF16
	.byte	0x2
	.byte	0x24
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutModelNumber
	.uleb128 0x7
	.4byte	.LASF17
	.byte	0x2
	.byte	0x25
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutMOption
	.uleb128 0x7
	.4byte	.LASF18
	.byte	0x2
	.byte	0x26
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutMSSEOption
	.uleb128 0x7
	.4byte	.LASF19
	.byte	0x2
	.byte	0x27
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutNetworkID
	.uleb128 0x7
	.4byte	.LASF20
	.byte	0x2
	.byte	0x28
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutSerialNumber
	.uleb128 0x7
	.4byte	.LASF21
	.byte	0x2
	.byte	0x29
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutSSEDOption
	.uleb128 0x7
	.4byte	.LASF22
	.byte	0x2
	.byte	0x2a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutSSEOption
	.uleb128 0x7
	.4byte	.LASF23
	.byte	0x2
	.byte	0x2b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTP2WireTerminal
	.uleb128 0x7
	.4byte	.LASF24
	.byte	0x2
	.byte	0x2c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPDashLCard
	.uleb128 0x7
	.4byte	.LASF25
	.byte	0x2
	.byte	0x2d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPDashLTerminal
	.uleb128 0x7
	.4byte	.LASF26
	.byte	0x2
	.byte	0x2e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPDashMCard
	.uleb128 0x7
	.4byte	.LASF27
	.byte	0x2
	.byte	0x2f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPDashMTerminal
	.uleb128 0x7
	.4byte	.LASF28
	.byte	0x2
	.byte	0x30
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPDashWCard
	.uleb128 0x7
	.4byte	.LASF29
	.byte	0x2
	.byte	0x31
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPDashWTerminal
	.uleb128 0x7
	.4byte	.LASF30
	.byte	0x2
	.byte	0x32
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPFuseCard
	.uleb128 0x7
	.4byte	.LASF31
	.byte	0x2
	.byte	0x33
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPMicroCard
	.uleb128 0x7
	.4byte	.LASF32
	.byte	0x2
	.byte	0x34
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPPOCCard
	.uleb128 0x7
	.4byte	.LASF33
	.byte	0x2
	.byte	0x35
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPPOCTerminal
	.uleb128 0x7
	.4byte	.LASF34
	.byte	0x2
	.byte	0x36
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta01_08Card
	.uleb128 0x7
	.4byte	.LASF35
	.byte	0x2
	.byte	0x37
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta01_08Terminal
	.uleb128 0x7
	.4byte	.LASF36
	.byte	0x2
	.byte	0x38
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta09_16Card
	.uleb128 0x7
	.4byte	.LASF37
	.byte	0x2
	.byte	0x39
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta09_16Terminal
	.uleb128 0x7
	.4byte	.LASF38
	.byte	0x2
	.byte	0x3a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta17_24Card
	.uleb128 0x7
	.4byte	.LASF39
	.byte	0x2
	.byte	0x3b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta17_24Terminal
	.uleb128 0x7
	.4byte	.LASF40
	.byte	0x2
	.byte	0x3c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta25_32Card
	.uleb128 0x7
	.4byte	.LASF41
	.byte	0x2
	.byte	0x3d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta25_32Terminal
	.uleb128 0x7
	.4byte	.LASF42
	.byte	0x2
	.byte	0x3e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta33_40Card
	.uleb128 0x7
	.4byte	.LASF43
	.byte	0x2
	.byte	0x3f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta33_40Terminal
	.uleb128 0x7
	.4byte	.LASF44
	.byte	0x2
	.byte	0x40
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta41_48Card
	.uleb128 0x7
	.4byte	.LASF45
	.byte	0x2
	.byte	0x41
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutTPSta41_48Terminal
	.uleb128 0x7
	.4byte	.LASF46
	.byte	0x2
	.byte	0x42
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutVersion
	.uleb128 0x7
	.4byte	.LASF47
	.byte	0x2
	.byte	0x43
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutWMOption
	.uleb128 0x7
	.4byte	.LASF48
	.byte	0x2
	.byte	0x44
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AboutWOption
	.uleb128 0x7
	.4byte	.LASF49
	.byte	0x2
	.byte	0x45
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AccessControlEnabled
	.uleb128 0x7
	.4byte	.LASF50
	.byte	0x2
	.byte	0x46
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AccessControlLoggedIn
	.uleb128 0x7
	.4byte	.LASF51
	.byte	0x2
	.byte	0x47
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AccessControlPwd
	.uleb128 0x7
	.4byte	.LASF52
	.byte	0x2
	.byte	0x48
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AccessControlUser
	.uleb128 0x7
	.4byte	.LASF53
	.byte	0x2
	.byte	0x49
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AccessControlUserRole
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x3eb
	.uleb128 0x4
	.4byte	0x38
	.byte	0x14
	.byte	0
	.uleb128 0x7
	.4byte	.LASF54
	.byte	0x2
	.byte	0x4a
	.4byte	0x3db
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ActivationCode
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x40d
	.uleb128 0x4
	.4byte	0x38
	.byte	0x5
	.byte	0
	.uleb128 0x7
	.4byte	.LASF55
	.byte	0x2
	.byte	0x4b
	.4byte	0x3fd
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AlertDate
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x430
	.uleb128 0x8
	.4byte	0x38
	.2byte	0x100
	.byte	0
	.uleb128 0x7
	.4byte	.LASF56
	.byte	0x2
	.byte	0x4c
	.4byte	0x41f
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AlertString
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x452
	.uleb128 0x4
	.4byte	0x38
	.byte	0x9
	.byte	0
	.uleb128 0x7
	.4byte	.LASF57
	.byte	0x2
	.byte	0x4d
	.4byte	0x442
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_AlertTime
	.uleb128 0x7
	.4byte	.LASF58
	.byte	0x2
	.byte	0x4e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_Backlight
	.uleb128 0x7
	.4byte	.LASF59
	.byte	0x2
	.byte	0x4f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BacklightSliderPos
	.uleb128 0x7
	.4byte	.LASF60
	.byte	0x2
	.byte	0x50
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetAllocation
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF61
	.uleb128 0x7
	.4byte	.LASF62
	.byte	0x2
	.byte	0x51
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetAllocationSYS
	.uleb128 0x7
	.4byte	.LASF63
	.byte	0x2
	.byte	0x52
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetAnnualValue
	.uleb128 0x7
	.4byte	.LASF64
	.byte	0x2
	.byte	0x53
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetDaysLeftInPeriod
	.uleb128 0x7
	.4byte	.LASF65
	.byte	0x2
	.byte	0x54
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetEntryOptionIdx
	.uleb128 0x7
	.4byte	.LASF66
	.byte	0x2
	.byte	0x55
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetEst
	.uleb128 0x7
	.4byte	.LASF67
	.byte	0x2
	.byte	0x56
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetETPercentageUsed
	.uleb128 0x7
	.4byte	.LASF68
	.byte	0x2
	.byte	0x57
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetExpectedUseThisPeriod
	.uleb128 0x7
	.4byte	.LASF69
	.byte	0x2
	.byte	0x58
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetFlowTypeIndex
	.uleb128 0x7
	.4byte	.LASF70
	.byte	0x2
	.byte	0x59
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetFlowTypeIrrigation
	.uleb128 0x7
	.4byte	.LASF71
	.byte	0x2
	.byte	0x5a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetFlowTypeMVOR
	.uleb128 0x7
	.4byte	.LASF72
	.byte	0x2
	.byte	0x5b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetFlowTypeNonController
	.uleb128 0x7
	.4byte	.LASF73
	.byte	0x2
	.byte	0x5c
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetForThisPeriod
	.uleb128 0x7
	.4byte	.LASF74
	.byte	0x2
	.byte	0x5d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetInUse
	.uleb128 0x7
	.4byte	.LASF75
	.byte	0x2
	.byte	0x5e
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetMainlineName
	.uleb128 0x7
	.4byte	.LASF76
	.byte	0x2
	.byte	0x5f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetModeIdx
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x5bf
	.uleb128 0x4
	.4byte	0x38
	.byte	0x10
	.byte	0
	.uleb128 0x7
	.4byte	.LASF77
	.byte	0x2
	.byte	0x60
	.4byte	0x5af
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetPeriod_0
	.uleb128 0x7
	.4byte	.LASF78
	.byte	0x2
	.byte	0x61
	.4byte	0x5af
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetPeriod_1
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x5f3
	.uleb128 0x4
	.4byte	0x38
	.byte	0x28
	.byte	0
	.uleb128 0x7
	.4byte	.LASF79
	.byte	0x2
	.byte	0x62
	.4byte	0x5e3
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetPeriodEnd
	.uleb128 0x7
	.4byte	.LASF80
	.byte	0x2
	.byte	0x63
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetPeriodIdx
	.uleb128 0x7
	.4byte	.LASF81
	.byte	0x2
	.byte	0x64
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetPeriodsPerYearIdx
	.uleb128 0x7
	.4byte	.LASF82
	.byte	0x2
	.byte	0x65
	.4byte	0x5e3
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetPeriodStart
	.uleb128 0x7
	.4byte	.LASF83
	.byte	0x2
	.byte	0x66
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_0
	.uleb128 0x9
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x7
	.4byte	.LASF84
	.byte	0x2
	.byte	0x67
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_1
	.uleb128 0x7
	.4byte	.LASF85
	.byte	0x2
	.byte	0x68
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_2
	.uleb128 0x7
	.4byte	.LASF86
	.byte	0x2
	.byte	0x69
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_3
	.uleb128 0x7
	.4byte	.LASF87
	.byte	0x2
	.byte	0x6a
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_4
	.uleb128 0x7
	.4byte	.LASF88
	.byte	0x2
	.byte	0x6b
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_5
	.uleb128 0x7
	.4byte	.LASF89
	.byte	0x2
	.byte	0x6c
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_6
	.uleb128 0x7
	.4byte	.LASF90
	.byte	0x2
	.byte	0x6d
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_7
	.uleb128 0x7
	.4byte	.LASF91
	.byte	0x2
	.byte	0x6e
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_8
	.uleb128 0x7
	.4byte	.LASF92
	.byte	0x2
	.byte	0x6f
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReductionLimit_9
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x706
	.uleb128 0x4
	.4byte	0x38
	.byte	0xe0
	.byte	0
	.uleb128 0x7
	.4byte	.LASF93
	.byte	0x2
	.byte	0x70
	.4byte	0x6f6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetReportText
	.uleb128 0x7
	.4byte	.LASF94
	.byte	0x2
	.byte	0x71
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetsInEffect
	.uleb128 0x7
	.4byte	.LASF95
	.byte	0x2
	.byte	0x72
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetSysHasPOC
	.uleb128 0x7
	.4byte	.LASF96
	.byte	0x2
	.byte	0x73
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetUnitGallon
	.uleb128 0x7
	.4byte	.LASF97
	.byte	0x2
	.byte	0x74
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetUnitHCF
	.uleb128 0x7
	.4byte	.LASF98
	.byte	0x2
	.byte	0x75
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetUsed
	.uleb128 0x7
	.4byte	.LASF99
	.byte	0x2
	.byte	0x76
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetUsedSYS
	.uleb128 0x7
	.4byte	.LASF100
	.byte	0x2
	.byte	0x77
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetUseThisPeriod
	.uleb128 0x7
	.4byte	.LASF101
	.byte	0x2
	.byte	0x78
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_BudgetZeroWarning
	.uleb128 0x7
	.4byte	.LASF102
	.byte	0x2
	.byte	0x79
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsCommMngrMode
	.uleb128 0x7
	.4byte	.LASF103
	.byte	0x2
	.byte	0x7a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsCommMngrState
	.uleb128 0x7
	.4byte	.LASF104
	.byte	0x2
	.byte	0x7b
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsControllersInChain
	.uleb128 0x7
	.4byte	.LASF105
	.byte	0x2
	.byte	0x7c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsA
	.uleb128 0x7
	.4byte	.LASF106
	.byte	0x2
	.byte	0x7d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsB
	.uleb128 0x7
	.4byte	.LASF107
	.byte	0x2
	.byte	0x7e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsC
	.uleb128 0x7
	.4byte	.LASF108
	.byte	0x2
	.byte	0x7f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsD
	.uleb128 0x7
	.4byte	.LASF109
	.byte	0x2
	.byte	0x80
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsE
	.uleb128 0x7
	.4byte	.LASF110
	.byte	0x2
	.byte	0x81
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsF
	.uleb128 0x7
	.4byte	.LASF111
	.byte	0x2
	.byte	0x82
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsG
	.uleb128 0x7
	.4byte	.LASF112
	.byte	0x2
	.byte	0x83
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsH
	.uleb128 0x7
	.4byte	.LASF113
	.byte	0x2
	.byte	0x84
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsI
	.uleb128 0x7
	.4byte	.LASF114
	.byte	0x2
	.byte	0x85
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsJ
	.uleb128 0x7
	.4byte	.LASF115
	.byte	0x2
	.byte	0x86
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsK
	.uleb128 0x7
	.4byte	.LASF116
	.byte	0x2
	.byte	0x87
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsErrorsL
	.uleb128 0x7
	.4byte	.LASF117
	.byte	0x2
	.byte	0x88
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsFOALChainRRcvd
	.uleb128 0x7
	.4byte	.LASF118
	.byte	0x2
	.byte	0x89
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsFOALChainTGen
	.uleb128 0x7
	.4byte	.LASF119
	.byte	0x2
	.byte	0x8a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsFOALIrriRRcvd
	.uleb128 0x7
	.4byte	.LASF120
	.byte	0x2
	.byte	0x8b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsFOALIrriTGen
	.uleb128 0x7
	.4byte	.LASF121
	.byte	0x2
	.byte	0x8c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInForced
	.uleb128 0x7
	.4byte	.LASF122
	.byte	0x2
	.byte	0x8d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseA
	.uleb128 0x7
	.4byte	.LASF123
	.byte	0x2
	.byte	0x8e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseB
	.uleb128 0x7
	.4byte	.LASF124
	.byte	0x2
	.byte	0x8f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseC
	.uleb128 0x7
	.4byte	.LASF125
	.byte	0x2
	.byte	0x90
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseD
	.uleb128 0x7
	.4byte	.LASF126
	.byte	0x2
	.byte	0x91
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseE
	.uleb128 0x7
	.4byte	.LASF127
	.byte	0x2
	.byte	0x92
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseF
	.uleb128 0x7
	.4byte	.LASF128
	.byte	0x2
	.byte	0x93
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseG
	.uleb128 0x7
	.4byte	.LASF129
	.byte	0x2
	.byte	0x94
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseH
	.uleb128 0x7
	.4byte	.LASF130
	.byte	0x2
	.byte	0x95
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseI
	.uleb128 0x7
	.4byte	.LASF131
	.byte	0x2
	.byte	0x96
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseJ
	.uleb128 0x7
	.4byte	.LASF132
	.byte	0x2
	.byte	0x97
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseK
	.uleb128 0x7
	.4byte	.LASF133
	.byte	0x2
	.byte	0x98
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsInUseL
	.uleb128 0x7
	.4byte	.LASF134
	.byte	0x2
	.byte	0x99
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsIRRIChainRGen
	.uleb128 0x7
	.4byte	.LASF135
	.byte	0x2
	.byte	0x9a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsIRRIChainTRcvd
	.uleb128 0x7
	.4byte	.LASF136
	.byte	0x2
	.byte	0x9b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsIRRIIrriRGen
	.uleb128 0x7
	.4byte	.LASF137
	.byte	0x2
	.byte	0x9c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsIRRIIrriTRcvd
	.uleb128 0x7
	.4byte	.LASF138
	.byte	0x2
	.byte	0x9d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsNextContact
	.uleb128 0x7
	.4byte	.LASF139
	.byte	0x2
	.byte	0x9e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsNextContactSerNum
	.uleb128 0x7
	.4byte	.LASF140
	.byte	0x2
	.byte	0x9f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsPresentlyMakingTokens
	.uleb128 0x7
	.4byte	.LASF141
	.byte	0x2
	.byte	0xa0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ChainStatsScans
	.uleb128 0x7
	.4byte	.LASF142
	.byte	0x2
	.byte	0xa1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CodeDownloadPercentComplete
	.uleb128 0x7
	.4byte	.LASF143
	.byte	0x2
	.byte	0xa2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CodeDownloadProgressBar
	.uleb128 0x7
	.4byte	.LASF144
	.byte	0x2
	.byte	0xa3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CodeDownloadState
	.uleb128 0x7
	.4byte	.LASF145
	.byte	0x2
	.byte	0xa4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CodeDownloadUpdatingTPMicro
	.uleb128 0x7
	.4byte	.LASF146
	.byte	0x2
	.byte	0xa5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ComboBox_X1
	.uleb128 0x7
	.4byte	.LASF147
	.byte	0x2
	.byte	0xa6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ComboBox_Y1
	.uleb128 0x7
	.4byte	.LASF148
	.byte	0x2
	.byte	0xa7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ComboBoxItemIndex
	.uleb128 0x7
	.4byte	.LASF149
	.byte	0x2
	.byte	0xa8
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ComboBoxItemString
	.uleb128 0x7
	.4byte	.LASF150
	.byte	0x2
	.byte	0xa9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommOptionCloudCommTestAvailable
	.uleb128 0x7
	.4byte	.LASF151
	.byte	0x2
	.byte	0xaa
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommOptionDeviceExchangeResult
	.uleb128 0x7
	.4byte	.LASF152
	.byte	0x2
	.byte	0xab
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommOptionInfoText
	.uleb128 0x7
	.4byte	.LASF153
	.byte	0x2
	.byte	0xac
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommOptionIsConnected
	.uleb128 0x7
	.4byte	.LASF154
	.byte	0x2
	.byte	0xad
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommOptionPortAIndex
	.uleb128 0x7
	.4byte	.LASF155
	.byte	0x2
	.byte	0xae
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommOptionPortAInstalled
	.uleb128 0x7
	.4byte	.LASF156
	.byte	0x2
	.byte	0xaf
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommOptionPortBIndex
	.uleb128 0x7
	.4byte	.LASF157
	.byte	0x2
	.byte	0xb0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommOptionPortBInstalled
	.uleb128 0x7
	.4byte	.LASF158
	.byte	0x2
	.byte	0xb1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommOptionRadioTestAvailable
	.uleb128 0x7
	.4byte	.LASF159
	.byte	0x2
	.byte	0xb2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommTestIPOctect1
	.uleb128 0x7
	.4byte	.LASF160
	.byte	0x2
	.byte	0xb3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommTestIPOctect2
	.uleb128 0x7
	.4byte	.LASF161
	.byte	0x2
	.byte	0xb4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommTestIPOctect3
	.uleb128 0x7
	.4byte	.LASF162
	.byte	0x2
	.byte	0xb5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommTestIPOctect4
	.uleb128 0x7
	.4byte	.LASF163
	.byte	0x2
	.byte	0xb6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommTestIPPort
	.uleb128 0x7
	.4byte	.LASF164
	.byte	0x2
	.byte	0xb7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommTestShowDebug
	.uleb128 0x7
	.4byte	.LASF165
	.byte	0x2
	.byte	0xb8
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommTestStatus
	.uleb128 0x7
	.4byte	.LASF166
	.byte	0x2
	.byte	0xb9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_CommTestSuppressConnections
	.uleb128 0x7
	.4byte	.LASF167
	.byte	0x2
	.byte	0xba
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ContactStatusAlive
	.uleb128 0x7
	.4byte	.LASF168
	.byte	0x2
	.byte	0xbb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ContactStatusFailures
	.uleb128 0x7
	.4byte	.LASF169
	.byte	0x2
	.byte	0xbc
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ContactStatusNameAbbrv
	.uleb128 0x7
	.4byte	.LASF170
	.byte	0x2
	.byte	0xbd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ContactStatusPort
	.uleb128 0x7
	.4byte	.LASF171
	.byte	0x2
	.byte	0xbe
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ContactStatusSerialNumber
	.uleb128 0x7
	.4byte	.LASF172
	.byte	0x2
	.byte	0xbf
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_Contrast
	.uleb128 0x7
	.4byte	.LASF173
	.byte	0x2
	.byte	0xc0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ContrastSliderPos
	.uleb128 0x7
	.4byte	.LASF174
	.byte	0x2
	.byte	0xc1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeAMPM
	.uleb128 0x7
	.4byte	.LASF175
	.byte	0x2
	.byte	0xc2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeDay
	.uleb128 0x7
	.4byte	.LASF176
	.byte	0x2
	.byte	0xc3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeDayOfWeek
	.uleb128 0x3
	.4byte	0x128
	.4byte	0xcfe
	.uleb128 0x4
	.4byte	0x38
	.byte	0x40
	.byte	0
	.uleb128 0x7
	.4byte	.LASF177
	.byte	0x2
	.byte	0xc4
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeFullStr
	.uleb128 0x7
	.4byte	.LASF178
	.byte	0x2
	.byte	0xc5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeHour
	.uleb128 0x7
	.4byte	.LASF179
	.byte	0x2
	.byte	0xc6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeMin
	.uleb128 0x7
	.4byte	.LASF180
	.byte	0x2
	.byte	0xc7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeMonth
	.uleb128 0x7
	.4byte	.LASF181
	.byte	0x2
	.byte	0xc8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeSec
	.uleb128 0x7
	.4byte	.LASF182
	.byte	0x2
	.byte	0xc9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeShowButton
	.uleb128 0x7
	.4byte	.LASF183
	.byte	0x2
	.byte	0xca
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeTimeZone
	.uleb128 0x7
	.4byte	.LASF184
	.byte	0x2
	.byte	0xcb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DateTimeYear
	.uleb128 0x7
	.4byte	.LASF185
	.byte	0x2
	.byte	0xcc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DelayBetweenValvesInUse
	.uleb128 0x7
	.4byte	.LASF186
	.byte	0x2
	.byte	0xcd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DeviceExchangeProgressBar
	.uleb128 0x7
	.4byte	.LASF187
	.byte	0x2
	.byte	0xce
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DeviceExchangeSyncingRadios
	.uleb128 0x7
	.4byte	.LASF188
	.byte	0x2
	.byte	0xcf
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_DisplayType
	.uleb128 0x7
	.4byte	.LASF189
	.byte	0x2
	.byte	0xd0
	.4byte	0x5af
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENDHCPName
	.uleb128 0x7
	.4byte	.LASF190
	.byte	0x2
	.byte	0xd1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENDHCPNameNotSet
	.uleb128 0x7
	.4byte	.LASF191
	.byte	0x2
	.byte	0xd2
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENFirmwareVer
	.uleb128 0x7
	.4byte	.LASF192
	.byte	0x2
	.byte	0xd3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENGateway_1
	.uleb128 0x7
	.4byte	.LASF193
	.byte	0x2
	.byte	0xd4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENGateway_2
	.uleb128 0x7
	.4byte	.LASF194
	.byte	0x2
	.byte	0xd5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENGateway_3
	.uleb128 0x7
	.4byte	.LASF195
	.byte	0x2
	.byte	0xd6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENGateway_4
	.uleb128 0x7
	.4byte	.LASF196
	.byte	0x2
	.byte	0xd7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENIPAddress_1
	.uleb128 0x7
	.4byte	.LASF197
	.byte	0x2
	.byte	0xd8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENIPAddress_2
	.uleb128 0x7
	.4byte	.LASF198
	.byte	0x2
	.byte	0xd9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENIPAddress_3
	.uleb128 0x7
	.4byte	.LASF199
	.byte	0x2
	.byte	0xda
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENIPAddress_4
	.uleb128 0x7
	.4byte	.LASF200
	.byte	0x2
	.byte	0xdb
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENMACAddress
	.uleb128 0x7
	.4byte	.LASF201
	.byte	0x2
	.byte	0xdc
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENModel
	.uleb128 0x7
	.4byte	.LASF202
	.byte	0x2
	.byte	0xdd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENNetmask
	.uleb128 0x7
	.4byte	.LASF203
	.byte	0x2
	.byte	0xde
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENObtainIPAutomatically
	.uleb128 0x7
	.4byte	.LASF204
	.byte	0x2
	.byte	0xdf
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ENSubnetMask
	.uleb128 0x3
	.4byte	0x128
	.4byte	0xf06
	.uleb128 0x4
	.4byte	0x38
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.4byte	.LASF205
	.byte	0x2
	.byte	0xe0
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETGageInstalledAt
	.uleb128 0x7
	.4byte	.LASF206
	.byte	0x2
	.byte	0xe1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETGageInUse
	.uleb128 0x7
	.4byte	.LASF207
	.byte	0x2
	.byte	0xe2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETGageLogPulses
	.uleb128 0x7
	.4byte	.LASF208
	.byte	0x2
	.byte	0xe3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETGageMaxPercent
	.uleb128 0x7
	.4byte	.LASF209
	.byte	0x2
	.byte	0xe4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETGagePercentFull
	.uleb128 0x7
	.4byte	.LASF210
	.byte	0x2
	.byte	0xe5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETGageRunawayGage
	.uleb128 0x7
	.4byte	.LASF211
	.byte	0x2
	.byte	0xe6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETGageSkipTonight
	.uleb128 0x7
	.4byte	.LASF212
	.byte	0x2
	.byte	0xe7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETRainTableCurrentET
	.uleb128 0x7
	.4byte	.LASF213
	.byte	0x2
	.byte	0xe8
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETRainTableDate
	.uleb128 0x7
	.4byte	.LASF214
	.byte	0x2
	.byte	0xe9
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETRainTableET
	.uleb128 0x7
	.4byte	.LASF215
	.byte	0x2
	.byte	0xea
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETRainTableETStatus
	.uleb128 0x7
	.4byte	.LASF216
	.byte	0x2
	.byte	0xeb
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETRainTableRain
	.uleb128 0x7
	.4byte	.LASF217
	.byte	0x2
	.byte	0xec
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETRainTableRainStatus
	.uleb128 0x7
	.4byte	.LASF218
	.byte	0x2
	.byte	0xed
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETRainTableReport
	.uleb128 0x7
	.4byte	.LASF219
	.byte	0x2
	.byte	0xee
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETRainTableShutdown
	.uleb128 0x7
	.4byte	.LASF220
	.byte	0x2
	.byte	0xef
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ETRainTableTable
	.uleb128 0x7
	.4byte	.LASF221
	.byte	0x2
	.byte	0xf0
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FinishTimes
	.uleb128 0x7
	.4byte	.LASF222
	.byte	0x2
	.byte	0xf1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FinishTimesCalcPercentComplete
	.uleb128 0x7
	.4byte	.LASF223
	.byte	0x2
	.byte	0xf2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FinishTimesCalcProgressBar
	.uleb128 0x7
	.4byte	.LASF224
	.byte	0x2
	.byte	0xf3
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FinishTimesCalcTime
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x107e
	.uleb128 0x4
	.4byte	0x38
	.byte	0x64
	.byte	0
	.uleb128 0x7
	.4byte	.LASF225
	.byte	0x2
	.byte	0xf4
	.4byte	0x106e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FinishTimesNotes
	.uleb128 0x7
	.4byte	.LASF226
	.byte	0x2
	.byte	0xf5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FinishTimesScrollBoxH
	.uleb128 0x7
	.4byte	.LASF227
	.byte	0x2
	.byte	0xf6
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FinishTimesStartTime
	.uleb128 0x7
	.4byte	.LASF228
	.byte	0x2
	.byte	0xf7
	.4byte	0x3db
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FinishTimesStationGroup
	.uleb128 0x7
	.4byte	.LASF229
	.byte	0x2
	.byte	0xf8
	.4byte	0x3db
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FinishTimesSystemGroup
	.uleb128 0x7
	.4byte	.LASF230
	.byte	0x2
	.byte	0xf9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FLControllerIndex_0
	.uleb128 0x7
	.4byte	.LASF231
	.byte	0x2
	.byte	0xfa
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FLNumControllersInChain
	.uleb128 0x7
	.4byte	.LASF232
	.byte	0x2
	.byte	0xfb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingAllowed
	.uleb128 0x7
	.4byte	.LASF233
	.byte	0x2
	.byte	0xfc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingAllowedToLock
	.uleb128 0x7
	.4byte	.LASF234
	.byte	0x2
	.byte	0xfd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingChecked
	.uleb128 0x7
	.4byte	.LASF235
	.byte	0x2
	.byte	0xfe
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingCycles
	.uleb128 0x7
	.4byte	.LASF236
	.byte	0x2
	.byte	0xff
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingExp
	.uleb128 0xa
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x100
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingExpOn
	.uleb128 0xa
	.4byte	.LASF238
	.byte	0x2
	.2byte	0x101
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingFlag0
	.uleb128 0xa
	.4byte	.LASF239
	.byte	0x2
	.2byte	0x102
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingFlag1
	.uleb128 0xa
	.4byte	.LASF240
	.byte	0x2
	.2byte	0x103
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingFlag2
	.uleb128 0xa
	.4byte	.LASF241
	.byte	0x2
	.2byte	0x104
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingFlag3
	.uleb128 0xa
	.4byte	.LASF242
	.byte	0x2
	.2byte	0x105
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingFlag4
	.uleb128 0xa
	.4byte	.LASF243
	.byte	0x2
	.2byte	0x106
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingFlag5
	.uleb128 0xa
	.4byte	.LASF244
	.byte	0x2
	.2byte	0x107
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingFlag6
	.uleb128 0xa
	.4byte	.LASF245
	.byte	0x2
	.2byte	0x108
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingFMs
	.uleb128 0xa
	.4byte	.LASF246
	.byte	0x2
	.2byte	0x109
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingGroupCnt0
	.uleb128 0xa
	.4byte	.LASF247
	.byte	0x2
	.2byte	0x10a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingGroupCnt1
	.uleb128 0xa
	.4byte	.LASF248
	.byte	0x2
	.2byte	0x10b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingGroupCnt2
	.uleb128 0xa
	.4byte	.LASF249
	.byte	0x2
	.2byte	0x10c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingGroupCnt3
	.uleb128 0xa
	.4byte	.LASF250
	.byte	0x2
	.2byte	0x10d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingHasNCMV
	.uleb128 0xa
	.4byte	.LASF251
	.byte	0x2
	.2byte	0x10e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingHi
	.uleb128 0xa
	.4byte	.LASF252
	.byte	0x2
	.2byte	0x10f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingINTime
	.uleb128 0xa
	.4byte	.LASF253
	.byte	0x2
	.2byte	0x110
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingIterations
	.uleb128 0xa
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x111
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingLFTime
	.uleb128 0xa
	.4byte	.LASF255
	.byte	0x2
	.2byte	0x112
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingLo
	.uleb128 0xa
	.4byte	.LASF256
	.byte	0x2
	.2byte	0x113
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingMaxOn
	.uleb128 0xa
	.4byte	.LASF257
	.byte	0x2
	.2byte	0x114
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingMVJOTime
	.uleb128 0xa
	.4byte	.LASF258
	.byte	0x2
	.2byte	0x115
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingMVState
	.uleb128 0xa
	.4byte	.LASF259
	.byte	0x2
	.2byte	0x116
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingMVTime
	.uleb128 0xa
	.4byte	.LASF260
	.byte	0x2
	.2byte	0x117
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingNumOn
	.uleb128 0xa
	.4byte	.LASF261
	.byte	0x2
	.2byte	0x118
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingOnForProbList
	.uleb128 0xa
	.4byte	.LASF262
	.byte	0x2
	.2byte	0x119
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingPumpMix
	.uleb128 0xa
	.4byte	.LASF263
	.byte	0x2
	.2byte	0x11a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingPumpState
	.uleb128 0xa
	.4byte	.LASF264
	.byte	0x2
	.2byte	0x11b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingPumpTime
	.uleb128 0xa
	.4byte	.LASF265
	.byte	0x2
	.2byte	0x11c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingSlots
	.uleb128 0xa
	.4byte	.LASF266
	.byte	0x2
	.2byte	0x11d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingSlotSize
	.uleb128 0xa
	.4byte	.LASF267
	.byte	0x2
	.2byte	0x11e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingStable
	.uleb128 0xa
	.4byte	.LASF268
	.byte	0x2
	.2byte	0x11f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowCheckingStopIrriTime
	.uleb128 0xa
	.4byte	.LASF269
	.byte	0x2
	.2byte	0x120
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowRecActual
	.uleb128 0xa
	.4byte	.LASF270
	.byte	0x2
	.2byte	0x121
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowRecExpected
	.uleb128 0xa
	.4byte	.LASF271
	.byte	0x2
	.2byte	0x122
	.4byte	0x5af
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowRecFlag
	.uleb128 0xa
	.4byte	.LASF272
	.byte	0x2
	.2byte	0x123
	.4byte	0x5af
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowRecTimeStamp
	.uleb128 0xa
	.4byte	.LASF273
	.byte	0x2
	.2byte	0x124
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTable10Sta
	.uleb128 0xa
	.4byte	.LASF274
	.byte	0x2
	.2byte	0x125
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTable2Sta
	.uleb128 0xa
	.4byte	.LASF275
	.byte	0x2
	.2byte	0x126
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTable3Sta
	.uleb128 0xa
	.4byte	.LASF276
	.byte	0x2
	.2byte	0x127
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTable4Sta
	.uleb128 0xa
	.4byte	.LASF277
	.byte	0x2
	.2byte	0x128
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTable5Sta
	.uleb128 0xa
	.4byte	.LASF278
	.byte	0x2
	.2byte	0x129
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTable6Sta
	.uleb128 0xa
	.4byte	.LASF279
	.byte	0x2
	.2byte	0x12a
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTable7Sta
	.uleb128 0xa
	.4byte	.LASF280
	.byte	0x2
	.2byte	0x12b
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTable8Sta
	.uleb128 0xa
	.4byte	.LASF281
	.byte	0x2
	.2byte	0x12c
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTable9Sta
	.uleb128 0xa
	.4byte	.LASF282
	.byte	0x2
	.2byte	0x12d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FlowTableGPM
	.uleb128 0xa
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x12e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FLPartOfChain
	.uleb128 0xa
	.4byte	.LASF284
	.byte	0x2
	.2byte	0x12f
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FreezeSwitchControllerName
	.uleb128 0xa
	.4byte	.LASF285
	.byte	0x2
	.2byte	0x130
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FreezeSwitchInUse
	.uleb128 0xa
	.4byte	.LASF286
	.byte	0x2
	.2byte	0x131
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_FreezeSwitchSelected
	.uleb128 0xa
	.4byte	.LASF287
	.byte	0x2
	.2byte	0x132
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRCarrier
	.uleb128 0xa
	.4byte	.LASF288
	.byte	0x2
	.2byte	0x133
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRECIO
	.uleb128 0xa
	.4byte	.LASF289
	.byte	0x2
	.2byte	0x134
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRFirmwareVersion
	.uleb128 0xa
	.4byte	.LASF290
	.byte	0x2
	.2byte	0x135
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRGPRSStatus
	.uleb128 0xa
	.4byte	.LASF291
	.byte	0x2
	.2byte	0x136
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRIPAddress
	.uleb128 0xa
	.4byte	.LASF292
	.byte	0x2
	.2byte	0x137
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRMake
	.uleb128 0xa
	.4byte	.LASF293
	.byte	0x2
	.2byte	0x138
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRModel
	.uleb128 0xa
	.4byte	.LASF294
	.byte	0x2
	.2byte	0x139
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRNetworkState
	.uleb128 0xa
	.4byte	.LASF295
	.byte	0x2
	.2byte	0x13a
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName
	.uleb128 0xa
	.4byte	.LASF296
	.byte	0x2
	.2byte	0x13b
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_0
	.uleb128 0xa
	.4byte	.LASF297
	.byte	0x2
	.2byte	0x13c
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_1
	.uleb128 0xa
	.4byte	.LASF298
	.byte	0x2
	.2byte	0x13d
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_2
	.uleb128 0xa
	.4byte	.LASF299
	.byte	0x2
	.2byte	0x13e
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_3
	.uleb128 0xa
	.4byte	.LASF300
	.byte	0x2
	.2byte	0x13f
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_4
	.uleb128 0xa
	.4byte	.LASF301
	.byte	0x2
	.2byte	0x140
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_5
	.uleb128 0xa
	.4byte	.LASF302
	.byte	0x2
	.2byte	0x141
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_6
	.uleb128 0xa
	.4byte	.LASF303
	.byte	0x2
	.2byte	0x142
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_7
	.uleb128 0xa
	.4byte	.LASF304
	.byte	0x2
	.2byte	0x143
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_8
	.uleb128 0xa
	.4byte	.LASF305
	.byte	0x2
	.2byte	0x144
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupName_9
	.uleb128 0xa
	.4byte	.LASF306
	.byte	0x2
	.2byte	0x145
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_0
	.uleb128 0xa
	.4byte	.LASF307
	.byte	0x2
	.2byte	0x146
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_1
	.uleb128 0xa
	.4byte	.LASF308
	.byte	0x2
	.2byte	0x147
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_2
	.uleb128 0xa
	.4byte	.LASF309
	.byte	0x2
	.2byte	0x148
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_3
	.uleb128 0xa
	.4byte	.LASF310
	.byte	0x2
	.2byte	0x149
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_4
	.uleb128 0xa
	.4byte	.LASF311
	.byte	0x2
	.2byte	0x14a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_5
	.uleb128 0xa
	.4byte	.LASF312
	.byte	0x2
	.2byte	0x14b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_6
	.uleb128 0xa
	.4byte	.LASF313
	.byte	0x2
	.2byte	0x14c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_7
	.uleb128 0xa
	.4byte	.LASF314
	.byte	0x2
	.2byte	0x14d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_8
	.uleb128 0xa
	.4byte	.LASF315
	.byte	0x2
	.2byte	0x14e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSetting_Visible_9
	.uleb128 0xa
	.4byte	.LASF316
	.byte	0x2
	.2byte	0x14f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_0
	.uleb128 0xa
	.4byte	.LASF317
	.byte	0x2
	.2byte	0x150
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_1
	.uleb128 0xa
	.4byte	.LASF318
	.byte	0x2
	.2byte	0x151
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_2
	.uleb128 0xa
	.4byte	.LASF319
	.byte	0x2
	.2byte	0x152
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_3
	.uleb128 0xa
	.4byte	.LASF320
	.byte	0x2
	.2byte	0x153
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_4
	.uleb128 0xa
	.4byte	.LASF321
	.byte	0x2
	.2byte	0x154
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_5
	.uleb128 0xa
	.4byte	.LASF322
	.byte	0x2
	.2byte	0x155
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_6
	.uleb128 0xa
	.4byte	.LASF323
	.byte	0x2
	.2byte	0x156
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_7
	.uleb128 0xa
	.4byte	.LASF324
	.byte	0x2
	.2byte	0x157
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_8
	.uleb128 0xa
	.4byte	.LASF325
	.byte	0x2
	.2byte	0x158
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingA_9
	.uleb128 0xa
	.4byte	.LASF326
	.byte	0x2
	.2byte	0x159
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_0
	.uleb128 0xa
	.4byte	.LASF327
	.byte	0x2
	.2byte	0x15a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_1
	.uleb128 0xa
	.4byte	.LASF328
	.byte	0x2
	.2byte	0x15b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_2
	.uleb128 0xa
	.4byte	.LASF329
	.byte	0x2
	.2byte	0x15c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_3
	.uleb128 0xa
	.4byte	.LASF330
	.byte	0x2
	.2byte	0x15d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_4
	.uleb128 0xa
	.4byte	.LASF331
	.byte	0x2
	.2byte	0x15e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_5
	.uleb128 0xa
	.4byte	.LASF332
	.byte	0x2
	.2byte	0x15f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_6
	.uleb128 0xa
	.4byte	.LASF333
	.byte	0x2
	.2byte	0x160
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_7
	.uleb128 0xa
	.4byte	.LASF334
	.byte	0x2
	.2byte	0x161
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_8
	.uleb128 0xa
	.4byte	.LASF335
	.byte	0x2
	.2byte	0x162
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingB_9
	.uleb128 0xa
	.4byte	.LASF336
	.byte	0x2
	.2byte	0x163
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_0
	.uleb128 0xa
	.4byte	.LASF337
	.byte	0x2
	.2byte	0x164
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_1
	.uleb128 0xa
	.4byte	.LASF338
	.byte	0x2
	.2byte	0x165
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_2
	.uleb128 0xa
	.4byte	.LASF339
	.byte	0x2
	.2byte	0x166
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_3
	.uleb128 0xa
	.4byte	.LASF340
	.byte	0x2
	.2byte	0x167
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_4
	.uleb128 0xa
	.4byte	.LASF341
	.byte	0x2
	.2byte	0x168
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_5
	.uleb128 0xa
	.4byte	.LASF342
	.byte	0x2
	.2byte	0x169
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_6
	.uleb128 0xa
	.4byte	.LASF343
	.byte	0x2
	.2byte	0x16a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_7
	.uleb128 0xa
	.4byte	.LASF344
	.byte	0x2
	.2byte	0x16b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_8
	.uleb128 0xa
	.4byte	.LASF345
	.byte	0x2
	.2byte	0x16c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GroupSettingC_9
	.uleb128 0xa
	.4byte	.LASF346
	.byte	0x2
	.2byte	0x16d
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRPhoneNumber
	.uleb128 0xa
	.4byte	.LASF347
	.byte	0x2
	.2byte	0x16e
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRRadioSerialNum
	.uleb128 0xa
	.4byte	.LASF348
	.byte	0x2
	.2byte	0x16f
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRRSSI
	.uleb128 0xa
	.4byte	.LASF349
	.byte	0x2
	.2byte	0x170
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRService
	.uleb128 0xa
	.4byte	.LASF350
	.byte	0x2
	.2byte	0x171
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRSIMID
	.uleb128 0xa
	.4byte	.LASF351
	.byte	0x2
	.2byte	0x172
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_GRSIMState
	.uleb128 0xa
	.4byte	.LASF352
	.byte	0x2
	.2byte	0x173
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET1
	.uleb128 0xa
	.4byte	.LASF353
	.byte	0x2
	.2byte	0x174
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET10
	.uleb128 0xa
	.4byte	.LASF354
	.byte	0x2
	.2byte	0x175
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET11
	.uleb128 0xa
	.4byte	.LASF355
	.byte	0x2
	.2byte	0x176
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET12
	.uleb128 0xa
	.4byte	.LASF356
	.byte	0x2
	.2byte	0x177
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET2
	.uleb128 0xa
	.4byte	.LASF357
	.byte	0x2
	.2byte	0x178
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET3
	.uleb128 0xa
	.4byte	.LASF358
	.byte	0x2
	.2byte	0x179
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET4
	.uleb128 0xa
	.4byte	.LASF359
	.byte	0x2
	.2byte	0x17a
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET5
	.uleb128 0xa
	.4byte	.LASF360
	.byte	0x2
	.2byte	0x17b
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET6
	.uleb128 0xa
	.4byte	.LASF361
	.byte	0x2
	.2byte	0x17c
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET7
	.uleb128 0xa
	.4byte	.LASF362
	.byte	0x2
	.2byte	0x17d
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET8
	.uleb128 0xa
	.4byte	.LASF363
	.byte	0x2
	.2byte	0x17e
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalET9
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x1ad3
	.uleb128 0x4
	.4byte	0x38
	.byte	0x18
	.byte	0
	.uleb128 0xa
	.4byte	.LASF364
	.byte	0x2
	.2byte	0x17f
	.4byte	0x1ac3
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETCity
	.uleb128 0xa
	.4byte	.LASF365
	.byte	0x2
	.2byte	0x180
	.4byte	0x1ac3
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETCounty
	.uleb128 0xa
	.4byte	.LASF366
	.byte	0x2
	.2byte	0x181
	.4byte	0x1ac3
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETState
	.uleb128 0xa
	.4byte	.LASF367
	.byte	0x2
	.2byte	0x182
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser1
	.uleb128 0xa
	.4byte	.LASF368
	.byte	0x2
	.2byte	0x183
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser10
	.uleb128 0xa
	.4byte	.LASF369
	.byte	0x2
	.2byte	0x184
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser11
	.uleb128 0xa
	.4byte	.LASF370
	.byte	0x2
	.2byte	0x185
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser12
	.uleb128 0xa
	.4byte	.LASF371
	.byte	0x2
	.2byte	0x186
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser2
	.uleb128 0xa
	.4byte	.LASF372
	.byte	0x2
	.2byte	0x187
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser3
	.uleb128 0xa
	.4byte	.LASF373
	.byte	0x2
	.2byte	0x188
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser4
	.uleb128 0xa
	.4byte	.LASF374
	.byte	0x2
	.2byte	0x189
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser5
	.uleb128 0xa
	.4byte	.LASF375
	.byte	0x2
	.2byte	0x18a
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser6
	.uleb128 0xa
	.4byte	.LASF376
	.byte	0x2
	.2byte	0x18b
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser7
	.uleb128 0xa
	.4byte	.LASF377
	.byte	0x2
	.2byte	0x18c
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser8
	.uleb128 0xa
	.4byte	.LASF378
	.byte	0x2
	.2byte	0x18d
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUser9
	.uleb128 0xa
	.4byte	.LASF379
	.byte	0x2
	.2byte	0x18e
	.4byte	0x1ac3
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUserCity
	.uleb128 0xa
	.4byte	.LASF380
	.byte	0x2
	.2byte	0x18f
	.4byte	0x1ac3
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUserCounty
	.uleb128 0xa
	.4byte	.LASF381
	.byte	0x2
	.2byte	0x190
	.4byte	0x1ac3
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUserState
	.uleb128 0xa
	.4byte	.LASF382
	.byte	0x2
	.2byte	0x191
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HistoricalETUseYourOwn
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x1c4d
	.uleb128 0x8
	.4byte	0x38
	.2byte	0x200
	.byte	0
	.uleb128 0xa
	.4byte	.LASF383
	.byte	0x2
	.2byte	0x192
	.4byte	0x1c3c
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_HubList
	.uleb128 0xa
	.4byte	.LASF384
	.byte	0x2
	.2byte	0x193
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriCommChainConfigActive
	.uleb128 0xa
	.4byte	.LASF385
	.byte	0x2
	.2byte	0x194
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriCommChainConfigCurrentFromFOAL
	.uleb128 0xa
	.4byte	.LASF386
	.byte	0x2
	.2byte	0x195
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriCommChainConfigCurrentFromIrri
	.uleb128 0xa
	.4byte	.LASF387
	.byte	0x2
	.2byte	0x196
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriCommChainConfigCurrentFromTP
	.uleb128 0xa
	.4byte	.LASF388
	.byte	0x2
	.2byte	0x197
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriCommChainConfigCurrentSendFromIrri
	.uleb128 0xa
	.4byte	.LASF389
	.byte	0x2
	.2byte	0x198
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriCommChainConfigCurrentSendFromTP
	.uleb128 0xa
	.4byte	.LASF390
	.byte	0x2
	.2byte	0x199
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriCommChainConfigNameAbbrv
	.uleb128 0xa
	.4byte	.LASF391
	.byte	0x2
	.2byte	0x19a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriCommChainConfigSerialNum
	.uleb128 0xa
	.4byte	.LASF392
	.byte	0x2
	.2byte	0x19b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsController
	.uleb128 0xa
	.4byte	.LASF393
	.byte	0x2
	.2byte	0x19c
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsCurrentDraw
	.uleb128 0xa
	.4byte	.LASF394
	.byte	0x2
	.2byte	0x19d
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsProgCycle
	.uleb128 0xa
	.4byte	.LASF395
	.byte	0x2
	.2byte	0x19e
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsProgLeft
	.uleb128 0xa
	.4byte	.LASF396
	.byte	0x2
	.2byte	0x19f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsReason
	.uleb128 0xa
	.4byte	.LASF397
	.byte	0x2
	.2byte	0x1a0
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsRunCycle
	.uleb128 0xa
	.4byte	.LASF398
	.byte	0x2
	.2byte	0x1a1
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsRunSoak
	.uleb128 0xa
	.4byte	.LASF399
	.byte	0x2
	.2byte	0x1a2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsShowFOAL
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x1da0
	.uleb128 0x4
	.4byte	0x38
	.byte	0x3
	.byte	0
	.uleb128 0xa
	.4byte	.LASF400
	.byte	0x2
	.2byte	0x1a3
	.4byte	0x1d90
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsStation
	.uleb128 0xa
	.4byte	.LASF401
	.byte	0x2
	.2byte	0x1a4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IrriDetailsStatus
	.uleb128 0xa
	.4byte	.LASF402
	.byte	0x2
	.2byte	0x1a5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IsAHub
	.uleb128 0xa
	.4byte	.LASF403
	.byte	0x2
	.2byte	0x1a6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_IsMaster
	.uleb128 0xa
	.4byte	.LASF404
	.byte	0x2
	.2byte	0x1a7
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_itmDecoder_Debug
	.uleb128 0xa
	.4byte	.LASF405
	.byte	0x2
	.2byte	0x1a8
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_itmGroupName
	.uleb128 0xa
	.4byte	.LASF406
	.byte	0x2
	.2byte	0x1a9
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_itmMenuItem
	.uleb128 0xa
	.4byte	.LASF407
	.byte	0x2
	.2byte	0x1aa
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_itmMenuItemDesc
	.uleb128 0xa
	.4byte	.LASF408
	.byte	0x2
	.2byte	0x1ab
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_itmPOC
	.uleb128 0xa
	.4byte	.LASF409
	.byte	0x2
	.2byte	0x1ac
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_itmStationName
	.uleb128 0xa
	.4byte	.LASF410
	.byte	0x2
	.2byte	0x1ad
	.4byte	0x1d90
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_itmStationNumber
	.uleb128 0xa
	.4byte	.LASF411
	.byte	0x2
	.2byte	0x1ae
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_itmSubNode
	.uleb128 0xa
	.4byte	.LASF412
	.byte	0x2
	.2byte	0x1af
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_KeyboardShiftEnabled
	.uleb128 0xa
	.4byte	.LASF413
	.byte	0x2
	.2byte	0x1b0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_KeyboardStartX
	.uleb128 0xa
	.4byte	.LASF414
	.byte	0x2
	.2byte	0x1b1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_KeyboardStartY
	.uleb128 0xa
	.4byte	.LASF415
	.byte	0x2
	.2byte	0x1b2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_KeyboardType
	.uleb128 0xa
	.4byte	.LASF416
	.byte	0x2
	.2byte	0x1b3
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek1Fri
	.uleb128 0xa
	.4byte	.LASF417
	.byte	0x2
	.2byte	0x1b4
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek1Mon
	.uleb128 0xa
	.4byte	.LASF418
	.byte	0x2
	.2byte	0x1b5
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek1Sat
	.uleb128 0xa
	.4byte	.LASF419
	.byte	0x2
	.2byte	0x1b6
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek1Sun
	.uleb128 0xa
	.4byte	.LASF420
	.byte	0x2
	.2byte	0x1b7
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek1Thu
	.uleb128 0xa
	.4byte	.LASF421
	.byte	0x2
	.2byte	0x1b8
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek1Tue
	.uleb128 0xa
	.4byte	.LASF422
	.byte	0x2
	.2byte	0x1b9
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek1Wed
	.uleb128 0xa
	.4byte	.LASF423
	.byte	0x2
	.2byte	0x1ba
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek2Fri
	.uleb128 0xa
	.4byte	.LASF424
	.byte	0x2
	.2byte	0x1bb
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek2Mon
	.uleb128 0xa
	.4byte	.LASF425
	.byte	0x2
	.2byte	0x1bc
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek2Sat
	.uleb128 0xa
	.4byte	.LASF426
	.byte	0x2
	.2byte	0x1bd
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek2Sun
	.uleb128 0xa
	.4byte	.LASF427
	.byte	0x2
	.2byte	0x1be
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek2Thu
	.uleb128 0xa
	.4byte	.LASF428
	.byte	0x2
	.2byte	0x1bf
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek2Tue
	.uleb128 0xa
	.4byte	.LASF429
	.byte	0x2
	.2byte	0x1c0
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightCalendarWeek2Wed
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x1fea
	.uleb128 0x4
	.4byte	0x38
	.byte	0x20
	.byte	0
	.uleb128 0xa
	.4byte	.LASF430
	.byte	0x2
	.2byte	0x1c1
	.4byte	0x1fda
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightEditableStop
	.uleb128 0xa
	.4byte	.LASF431
	.byte	0x2
	.2byte	0x1c2
	.4byte	0x1fda
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightIrriListStop
	.uleb128 0xa
	.4byte	.LASF432
	.byte	0x2
	.2byte	0x1c3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightIsEnergized
	.uleb128 0xa
	.4byte	.LASF433
	.byte	0x2
	.2byte	0x1c4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightIsPhysicallyAvailable
	.uleb128 0xa
	.4byte	.LASF434
	.byte	0x2
	.2byte	0x1c5
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightRemainingMinutes
	.uleb128 0xa
	.4byte	.LASF435
	.byte	0x2
	.2byte	0x1c6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsAreEnergized
	.uleb128 0xa
	.4byte	.LASF436
	.byte	0x2
	.2byte	0x1c7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsBoxIndex_0
	.uleb128 0xa
	.4byte	.LASF437
	.byte	0x2
	.2byte	0x1c8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsCalendarHighlightXOffset
	.uleb128 0xa
	.4byte	.LASF438
	.byte	0x2
	.2byte	0x1c9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsCalendarHighlightYOffset
	.uleb128 0xa
	.4byte	.LASF439
	.byte	0x2
	.2byte	0x1ca
	.4byte	0x1fda
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightScheduledStart
	.uleb128 0xa
	.4byte	.LASF440
	.byte	0x2
	.2byte	0x1cb
	.4byte	0x1fda
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightScheduledStop
	.uleb128 0xa
	.4byte	.LASF441
	.byte	0x2
	.2byte	0x1cc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightScheduleState
	.uleb128 0xa
	.4byte	.LASF442
	.byte	0x2
	.2byte	0x1cd
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsDate
	.uleb128 0xa
	.4byte	.LASF443
	.byte	0x2
	.2byte	0x1ce
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsOutput
	.uleb128 0xa
	.4byte	.LASF444
	.byte	0x2
	.2byte	0x1cf
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsOutputIndex_0
	.uleb128 0xa
	.4byte	.LASF445
	.byte	0x2
	.2byte	0x1d0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsOutputIsDeenergized
	.uleb128 0xa
	.4byte	.LASF446
	.byte	0x2
	.2byte	0x1d1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsStartTime1InMinutes
	.uleb128 0xa
	.4byte	.LASF447
	.byte	0x2
	.2byte	0x1d2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsStartTime2InMinutes
	.uleb128 0xa
	.4byte	.LASF448
	.byte	0x2
	.2byte	0x1d3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsStartTimeEnabled1
	.uleb128 0xa
	.4byte	.LASF449
	.byte	0x2
	.2byte	0x1d4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsStartTimeEnabled2
	.uleb128 0xa
	.4byte	.LASF450
	.byte	0x2
	.2byte	0x1d5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsStopTime1InMinutes
	.uleb128 0xa
	.4byte	.LASF451
	.byte	0x2
	.2byte	0x1d6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsStopTime2InMinutes
	.uleb128 0xa
	.4byte	.LASF452
	.byte	0x2
	.2byte	0x1d7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsStopTimeEnabled1
	.uleb128 0xa
	.4byte	.LASF453
	.byte	0x2
	.2byte	0x1d8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsStopTimeEnabled2
	.uleb128 0xa
	.4byte	.LASF454
	.byte	0x2
	.2byte	0x1d9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightsStopTimeHasBeenEdited
	.uleb128 0xa
	.4byte	.LASF455
	.byte	0x2
	.2byte	0x1da
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LightStartReason
	.uleb128 0xa
	.4byte	.LASF456
	.byte	0x2
	.2byte	0x1db
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensBypassFlowAvgFlow
	.uleb128 0xa
	.4byte	.LASF457
	.byte	0x2
	.2byte	0x1dc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensBypassFlowFlow
	.uleb128 0xa
	.4byte	.LASF458
	.byte	0x2
	.2byte	0x1dd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensBypassFlowLevel
	.uleb128 0xa
	.4byte	.LASF459
	.byte	0x2
	.2byte	0x1de
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensBypassFlowMaxFlow
	.uleb128 0xa
	.4byte	.LASF460
	.byte	0x2
	.2byte	0x1df
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensBypassFlowMVOpen
	.uleb128 0xa
	.4byte	.LASF461
	.byte	0x2
	.2byte	0x1e0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensBypassFlowShowMaxFlow
	.uleb128 0xa
	.4byte	.LASF462
	.byte	0x2
	.2byte	0x1e1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensCommCentralConnected
	.uleb128 0xa
	.4byte	.LASF463
	.byte	0x2
	.2byte	0x1e2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensCommCentralEnabled
	.uleb128 0xa
	.4byte	.LASF464
	.byte	0x2
	.2byte	0x1e3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensCommFLCommMngrMode
	.uleb128 0xa
	.4byte	.LASF465
	.byte	0x2
	.2byte	0x1e4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensCommFLEnabled
	.uleb128 0xa
	.4byte	.LASF466
	.byte	0x2
	.2byte	0x1e5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensCommFLInForced
	.uleb128 0xa
	.4byte	.LASF467
	.byte	0x2
	.2byte	0x1e6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensCommTPMicroConnected
	.uleb128 0xa
	.4byte	.LASF468
	.byte	0x2
	.2byte	0x1e7
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensElectricalControllerName
	.uleb128 0xa
	.4byte	.LASF469
	.byte	0x2
	.2byte	0x1e8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensPOCFlowMVState
	.uleb128 0xa
	.4byte	.LASF470
	.byte	0x2
	.2byte	0x1e9
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensPOCFlowName
	.uleb128 0xa
	.4byte	.LASF471
	.byte	0x2
	.2byte	0x1ea
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensSystemMLBExists
	.uleb128 0xa
	.4byte	.LASF472
	.byte	0x2
	.2byte	0x1eb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LiveScreensSystemMVORExists
	.uleb128 0xa
	.4byte	.LASF473
	.byte	0x2
	.2byte	0x1ec
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRBeaconRate
	.uleb128 0xa
	.4byte	.LASF474
	.byte	0x2
	.2byte	0x1ed
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRFirmwareVer
	.uleb128 0xa
	.4byte	.LASF475
	.byte	0x2
	.2byte	0x1ee
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRFrequency_Decimal
	.uleb128 0xa
	.4byte	.LASF476
	.byte	0x2
	.2byte	0x1ef
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRFrequency_WholeNum
	.uleb128 0xa
	.4byte	.LASF477
	.byte	0x2
	.2byte	0x1f0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRGroup
	.uleb128 0xa
	.4byte	.LASF478
	.byte	0x2
	.2byte	0x1f1
	.4byte	0x3f
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRHubFeatureNotAvailable
	.uleb128 0xa
	.4byte	.LASF479
	.byte	0x2
	.2byte	0x1f2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRMode
	.uleb128 0xa
	.4byte	.LASF480
	.byte	0x2
	.2byte	0x1f3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRPort
	.uleb128 0xa
	.4byte	.LASF481
	.byte	0x2
	.2byte	0x1f4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRRadioType
	.uleb128 0xa
	.4byte	.LASF482
	.byte	0x2
	.2byte	0x1f5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRRepeaterInUse
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x23e9
	.uleb128 0x4
	.4byte	0x38
	.byte	0x8
	.byte	0
	.uleb128 0xa
	.4byte	.LASF483
	.byte	0x2
	.2byte	0x1f6
	.4byte	0x23d9
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRSerialNumber
	.uleb128 0xa
	.4byte	.LASF484
	.byte	0x2
	.2byte	0x1f7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRSlaveLinksToRepeater
	.uleb128 0xa
	.4byte	.LASF485
	.byte	0x2
	.2byte	0x1f8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRTemperature
	.uleb128 0xa
	.4byte	.LASF486
	.byte	0x2
	.2byte	0x1f9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_LRTransmitPower
	.uleb128 0xa
	.4byte	.LASF487
	.byte	0x2
	.2byte	0x1fa
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenu2WireOptionsExist
	.uleb128 0xa
	.4byte	.LASF488
	.byte	0x2
	.2byte	0x1fb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuAquaponicsMode
	.uleb128 0xa
	.4byte	.LASF489
	.byte	0x2
	.2byte	0x1fc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuBudgetsInUse
	.uleb128 0xa
	.4byte	.LASF490
	.byte	0x2
	.2byte	0x1fd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuBypassExists
	.uleb128 0xa
	.4byte	.LASF491
	.byte	0x2
	.2byte	0x1fe
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuCommOptionExists
	.uleb128 0xa
	.4byte	.LASF492
	.byte	0x2
	.2byte	0x1ff
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuFLOptionExists
	.uleb128 0xa
	.4byte	.LASF493
	.byte	0x2
	.2byte	0x200
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuFlowCheckingInUse
	.uleb128 0xa
	.4byte	.LASF494
	.byte	0x2
	.2byte	0x201
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuFlowMetersExist
	.uleb128 0xa
	.4byte	.LASF495
	.byte	0x2
	.2byte	0x202
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuHubOptionExists
	.uleb128 0xa
	.4byte	.LASF496
	.byte	0x2
	.2byte	0x203
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuLightsOptionsExist
	.uleb128 0xa
	.4byte	.LASF497
	.byte	0x2
	.2byte	0x204
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuMainLinesAvailable
	.uleb128 0xa
	.4byte	.LASF498
	.byte	0x2
	.2byte	0x205
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuManualAvailable
	.uleb128 0xa
	.4byte	.LASF499
	.byte	0x2
	.2byte	0x206
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuMoisSensorExists
	.uleb128 0xa
	.4byte	.LASF500
	.byte	0x2
	.2byte	0x207
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuPOCsExist
	.uleb128 0xa
	.4byte	.LASF501
	.byte	0x2
	.2byte	0x208
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuStationsExist
	.uleb128 0xa
	.4byte	.LASF502
	.byte	0x2
	.2byte	0x209
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuWeatherDeviceExists
	.uleb128 0xa
	.4byte	.LASF503
	.byte	0x2
	.2byte	0x20a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MainMenuWeatherOptionsExist
	.uleb128 0xa
	.4byte	.LASF504
	.byte	0x2
	.2byte	0x20b
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPEndDateStr
	.uleb128 0xa
	.4byte	.LASF505
	.byte	0x2
	.2byte	0x20c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPInUse
	.uleb128 0xa
	.4byte	.LASF506
	.byte	0x2
	.2byte	0x20d
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPRunTime
	.uleb128 0xa
	.4byte	.LASF507
	.byte	0x2
	.2byte	0x20e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPRunTimeSingle
	.uleb128 0xa
	.4byte	.LASF508
	.byte	0x2
	.2byte	0x20f
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartDateStr
	.uleb128 0xa
	.4byte	.LASF509
	.byte	0x2
	.2byte	0x210
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime1
	.uleb128 0xa
	.4byte	.LASF510
	.byte	0x2
	.2byte	0x211
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime1Enabled
	.uleb128 0xa
	.4byte	.LASF511
	.byte	0x2
	.2byte	0x212
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime2
	.uleb128 0xa
	.4byte	.LASF512
	.byte	0x2
	.2byte	0x213
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime2Enabled
	.uleb128 0xa
	.4byte	.LASF513
	.byte	0x2
	.2byte	0x214
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime3
	.uleb128 0xa
	.4byte	.LASF514
	.byte	0x2
	.2byte	0x215
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime3Enabled
	.uleb128 0xa
	.4byte	.LASF515
	.byte	0x2
	.2byte	0x216
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime4
	.uleb128 0xa
	.4byte	.LASF516
	.byte	0x2
	.2byte	0x217
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime4Enabled
	.uleb128 0xa
	.4byte	.LASF517
	.byte	0x2
	.2byte	0x218
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime5
	.uleb128 0xa
	.4byte	.LASF518
	.byte	0x2
	.2byte	0x219
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime5Enabled
	.uleb128 0xa
	.4byte	.LASF519
	.byte	0x2
	.2byte	0x21a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime6
	.uleb128 0xa
	.4byte	.LASF520
	.byte	0x2
	.2byte	0x21b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStartTime6Enabled
	.uleb128 0xa
	.4byte	.LASF521
	.byte	0x2
	.2byte	0x21c
	.4byte	0x1d90
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStationRunTime
	.uleb128 0xa
	.4byte	.LASF522
	.byte	0x2
	.2byte	0x21d
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStationRunTimeControllerName
	.uleb128 0xa
	.4byte	.LASF523
	.byte	0x2
	.2byte	0x21e
	.4byte	0x1d90
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPStationRunTimeStationNumber
	.uleb128 0xa
	.4byte	.LASF524
	.byte	0x2
	.2byte	0x21f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPWaterDay_Fri
	.uleb128 0xa
	.4byte	.LASF525
	.byte	0x2
	.2byte	0x220
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPWaterDay_Mon
	.uleb128 0xa
	.4byte	.LASF526
	.byte	0x2
	.2byte	0x221
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPWaterDay_Sat
	.uleb128 0xa
	.4byte	.LASF527
	.byte	0x2
	.2byte	0x222
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPWaterDay_Sun
	.uleb128 0xa
	.4byte	.LASF528
	.byte	0x2
	.2byte	0x223
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPWaterDay_Thu
	.uleb128 0xa
	.4byte	.LASF529
	.byte	0x2
	.2byte	0x224
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPWaterDay_Tue
	.uleb128 0xa
	.4byte	.LASF530
	.byte	0x2
	.2byte	0x225
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPWaterDay_Wed
	.uleb128 0xa
	.4byte	.LASF531
	.byte	0x2
	.2byte	0x226
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPWillNotRunReason
	.uleb128 0xa
	.4byte	.LASF532
	.byte	0x2
	.2byte	0x227
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualPWillRun
	.uleb128 0xa
	.4byte	.LASF533
	.byte	0x2
	.2byte	0x228
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualWaterProg
	.uleb128 0xa
	.4byte	.LASF534
	.byte	0x2
	.2byte	0x229
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ManualWaterRunTime
	.uleb128 0xa
	.4byte	.LASF535
	.byte	0x2
	.2byte	0x22a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MenuScreenToShow
	.uleb128 0xa
	.4byte	.LASF536
	.byte	0x2
	.2byte	0x22b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisAdditionalSoakSeconds
	.uleb128 0xa
	.4byte	.LASF537
	.byte	0x2
	.2byte	0x22c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisBoxIndex
	.uleb128 0xa
	.4byte	.LASF538
	.byte	0x2
	.2byte	0x22d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisControlMode
	.uleb128 0xa
	.4byte	.LASF539
	.byte	0x2
	.2byte	0x22e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisDecoderSN
	.uleb128 0xa
	.4byte	.LASF540
	.byte	0x2
	.2byte	0x22f
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisEC_Current
	.uleb128 0xa
	.4byte	.LASF541
	.byte	0x2
	.2byte	0x230
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisECAction_High
	.uleb128 0xa
	.4byte	.LASF542
	.byte	0x2
	.2byte	0x231
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisECAction_Low
	.uleb128 0xa
	.4byte	.LASF543
	.byte	0x2
	.2byte	0x232
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisECPoint_High
	.uleb128 0xa
	.4byte	.LASF544
	.byte	0x2
	.2byte	0x233
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisECPoint_Low
	.uleb128 0xa
	.4byte	.LASF545
	.byte	0x2
	.2byte	0x234
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisInUse
	.uleb128 0xa
	.4byte	.LASF546
	.byte	0x2
	.2byte	0x235
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisPhysicallyAvailable
	.uleb128 0xa
	.4byte	.LASF547
	.byte	0x2
	.2byte	0x236
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisReading_Current
	.uleb128 0xa
	.4byte	.LASF548
	.byte	0x2
	.2byte	0x237
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisSensorIncludesEC
	.uleb128 0xa
	.4byte	.LASF549
	.byte	0x2
	.2byte	0x238
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisSensorName
	.uleb128 0xa
	.4byte	.LASF550
	.byte	0x2
	.2byte	0x239
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisSetPoint_High
	.uleb128 0xa
	.4byte	.LASF551
	.byte	0x2
	.2byte	0x23a
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisSetPoint_Low
	.uleb128 0xa
	.4byte	.LASF552
	.byte	0x2
	.2byte	0x23b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisTemp_Current
	.uleb128 0xa
	.4byte	.LASF553
	.byte	0x2
	.2byte	0x23c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisTempAction_High
	.uleb128 0xa
	.4byte	.LASF554
	.byte	0x2
	.2byte	0x23d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisTempAction_Low
	.uleb128 0xa
	.4byte	.LASF555
	.byte	0x2
	.2byte	0x23e
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisTempPoint_High
	.uleb128 0xa
	.4byte	.LASF556
	.byte	0x2
	.2byte	0x23f
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoisTempPoint_Low
	.uleb128 0xa
	.4byte	.LASF557
	.byte	0x2
	.2byte	0x240
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MoistureTabToDisplay
	.uleb128 0xa
	.4byte	.LASF558
	.byte	0x2
	.2byte	0x241
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose0
	.uleb128 0xa
	.4byte	.LASF559
	.byte	0x2
	.2byte	0x242
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose0Enabled
	.uleb128 0xa
	.4byte	.LASF560
	.byte	0x2
	.2byte	0x243
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose1
	.uleb128 0xa
	.4byte	.LASF561
	.byte	0x2
	.2byte	0x244
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose1Enabled
	.uleb128 0xa
	.4byte	.LASF562
	.byte	0x2
	.2byte	0x245
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose2
	.uleb128 0xa
	.4byte	.LASF563
	.byte	0x2
	.2byte	0x246
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose2Enabled
	.uleb128 0xa
	.4byte	.LASF564
	.byte	0x2
	.2byte	0x247
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose3
	.uleb128 0xa
	.4byte	.LASF565
	.byte	0x2
	.2byte	0x248
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose3Enabled
	.uleb128 0xa
	.4byte	.LASF566
	.byte	0x2
	.2byte	0x249
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose4
	.uleb128 0xa
	.4byte	.LASF567
	.byte	0x2
	.2byte	0x24a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose4Enabled
	.uleb128 0xa
	.4byte	.LASF568
	.byte	0x2
	.2byte	0x24b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose5
	.uleb128 0xa
	.4byte	.LASF569
	.byte	0x2
	.2byte	0x24c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose5Enabled
	.uleb128 0xa
	.4byte	.LASF570
	.byte	0x2
	.2byte	0x24d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose6
	.uleb128 0xa
	.4byte	.LASF571
	.byte	0x2
	.2byte	0x24e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORClose6Enabled
	.uleb128 0xa
	.4byte	.LASF572
	.byte	0x2
	.2byte	0x24f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORInEffect
	.uleb128 0xa
	.4byte	.LASF573
	.byte	0x2
	.2byte	0x250
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen0
	.uleb128 0xa
	.4byte	.LASF574
	.byte	0x2
	.2byte	0x251
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen0Enabled
	.uleb128 0xa
	.4byte	.LASF575
	.byte	0x2
	.2byte	0x252
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen1
	.uleb128 0xa
	.4byte	.LASF576
	.byte	0x2
	.2byte	0x253
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen1Enabled
	.uleb128 0xa
	.4byte	.LASF577
	.byte	0x2
	.2byte	0x254
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen2
	.uleb128 0xa
	.4byte	.LASF578
	.byte	0x2
	.2byte	0x255
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen2Enabled
	.uleb128 0xa
	.4byte	.LASF579
	.byte	0x2
	.2byte	0x256
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen3
	.uleb128 0xa
	.4byte	.LASF580
	.byte	0x2
	.2byte	0x257
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen3Enabled
	.uleb128 0xa
	.4byte	.LASF581
	.byte	0x2
	.2byte	0x258
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen4
	.uleb128 0xa
	.4byte	.LASF582
	.byte	0x2
	.2byte	0x259
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen4Enabled
	.uleb128 0xa
	.4byte	.LASF583
	.byte	0x2
	.2byte	0x25a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen5
	.uleb128 0xa
	.4byte	.LASF584
	.byte	0x2
	.2byte	0x25b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen5Enabled
	.uleb128 0xa
	.4byte	.LASF585
	.byte	0x2
	.2byte	0x25c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen6
	.uleb128 0xa
	.4byte	.LASF586
	.byte	0x2
	.2byte	0x25d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVOROpen6Enabled
	.uleb128 0xa
	.4byte	.LASF587
	.byte	0x2
	.2byte	0x25e
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORRunTime
	.uleb128 0xa
	.4byte	.LASF588
	.byte	0x2
	.2byte	0x25f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORState
	.uleb128 0xa
	.4byte	.LASF589
	.byte	0x2
	.2byte	0x260
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_MVORTimeRemaining
	.uleb128 0xa
	.4byte	.LASF590
	.byte	0x2
	.2byte	0x261
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_NetworkAvailable
	.uleb128 0xa
	.4byte	.LASF591
	.byte	0x2
	.2byte	0x262
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_NoWaterDaysDays
	.uleb128 0xa
	.4byte	.LASF592
	.byte	0x2
	.2byte	0x263
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_NoWaterDaysProg
	.uleb128 0xa
	.4byte	.LASF593
	.byte	0x2
	.2byte	0x264
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_NumericKeypadShowDecimalPoint
	.uleb128 0xa
	.4byte	.LASF594
	.byte	0x2
	.2byte	0x265
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_NumericKeypadShowPlusMinus
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x2c49
	.uleb128 0x4
	.4byte	0x38
	.byte	0xa
	.byte	0
	.uleb128 0xa
	.4byte	.LASF595
	.byte	0x2
	.2byte	0x266
	.4byte	0x2c39
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_NumericKeypadValue
	.uleb128 0xa
	.4byte	.LASF596
	.byte	0x2
	.2byte	0x267
	.4byte	0x3f
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_OnAtATimeInfoDisplay
	.uleb128 0xa
	.4byte	.LASF597
	.byte	0x2
	.2byte	0x268
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_0
	.uleb128 0xa
	.4byte	.LASF598
	.byte	0x2
	.2byte	0x269
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_1
	.uleb128 0xa
	.4byte	.LASF599
	.byte	0x2
	.2byte	0x26a
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_2
	.uleb128 0xa
	.4byte	.LASF600
	.byte	0x2
	.2byte	0x26b
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_3
	.uleb128 0xa
	.4byte	.LASF601
	.byte	0x2
	.2byte	0x26c
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_4
	.uleb128 0xa
	.4byte	.LASF602
	.byte	0x2
	.2byte	0x26d
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_5
	.uleb128 0xa
	.4byte	.LASF603
	.byte	0x2
	.2byte	0x26e
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_6
	.uleb128 0xa
	.4byte	.LASF604
	.byte	0x2
	.2byte	0x26f
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_7
	.uleb128 0xa
	.4byte	.LASF605
	.byte	0x2
	.2byte	0x270
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_8
	.uleb128 0xa
	.4byte	.LASF606
	.byte	0x2
	.2byte	0x271
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustEndDate_9
	.uleb128 0xa
	.4byte	.LASF607
	.byte	0x2
	.2byte	0x272
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustNumberOfDays
	.uleb128 0xa
	.4byte	.LASF608
	.byte	0x2
	.2byte	0x273
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent
	.uleb128 0xa
	.4byte	.LASF609
	.byte	0x2
	.2byte	0x274
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_0
	.uleb128 0xa
	.4byte	.LASF610
	.byte	0x2
	.2byte	0x275
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_1
	.uleb128 0xa
	.4byte	.LASF611
	.byte	0x2
	.2byte	0x276
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_2
	.uleb128 0xa
	.4byte	.LASF612
	.byte	0x2
	.2byte	0x277
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_3
	.uleb128 0xa
	.4byte	.LASF613
	.byte	0x2
	.2byte	0x278
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_4
	.uleb128 0xa
	.4byte	.LASF614
	.byte	0x2
	.2byte	0x279
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_5
	.uleb128 0xa
	.4byte	.LASF615
	.byte	0x2
	.2byte	0x27a
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_6
	.uleb128 0xa
	.4byte	.LASF616
	.byte	0x2
	.2byte	0x27b
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_7
	.uleb128 0xa
	.4byte	.LASF617
	.byte	0x2
	.2byte	0x27c
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_8
	.uleb128 0xa
	.4byte	.LASF618
	.byte	0x2
	.2byte	0x27d
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPercent_9
	.uleb128 0xa
	.4byte	.LASF619
	.byte	0x2
	.2byte	0x27e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_PercentAdjustPositive
	.uleb128 0xa
	.4byte	.LASF620
	.byte	0x2
	.2byte	0x27f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBoxIndex
	.uleb128 0xa
	.4byte	.LASF621
	.byte	0x2
	.2byte	0x280
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget1
	.uleb128 0xa
	.4byte	.LASF622
	.byte	0x2
	.2byte	0x281
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget10
	.uleb128 0xa
	.4byte	.LASF623
	.byte	0x2
	.2byte	0x282
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget11
	.uleb128 0xa
	.4byte	.LASF624
	.byte	0x2
	.2byte	0x283
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget12
	.uleb128 0xa
	.4byte	.LASF625
	.byte	0x2
	.2byte	0x284
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget2
	.uleb128 0xa
	.4byte	.LASF626
	.byte	0x2
	.2byte	0x285
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget3
	.uleb128 0xa
	.4byte	.LASF627
	.byte	0x2
	.2byte	0x286
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget4
	.uleb128 0xa
	.4byte	.LASF628
	.byte	0x2
	.2byte	0x287
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget5
	.uleb128 0xa
	.4byte	.LASF629
	.byte	0x2
	.2byte	0x288
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget6
	.uleb128 0xa
	.4byte	.LASF630
	.byte	0x2
	.2byte	0x289
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget7
	.uleb128 0xa
	.4byte	.LASF631
	.byte	0x2
	.2byte	0x28a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget8
	.uleb128 0xa
	.4byte	.LASF632
	.byte	0x2
	.2byte	0x28b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudget9
	.uleb128 0xa
	.4byte	.LASF633
	.byte	0x2
	.2byte	0x28c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudgetOption
	.uleb128 0xa
	.4byte	.LASF634
	.byte	0x2
	.2byte	0x28d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudgetPercentOfET
	.uleb128 0xa
	.4byte	.LASF635
	.byte	0x2
	.2byte	0x28e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBudgetYearly
	.uleb128 0xa
	.4byte	.LASF636
	.byte	0x2
	.2byte	0x28f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCBypassStages
	.uleb128 0xa
	.4byte	.LASF637
	.byte	0x2
	.2byte	0x290
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCDecoderSN1
	.uleb128 0xa
	.4byte	.LASF638
	.byte	0x2
	.2byte	0x291
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCDecoderSN2
	.uleb128 0xa
	.4byte	.LASF639
	.byte	0x2
	.2byte	0x292
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCDecoderSN3
	.uleb128 0xa
	.4byte	.LASF640
	.byte	0x2
	.2byte	0x293
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFlowMeterK1
	.uleb128 0xa
	.4byte	.LASF641
	.byte	0x2
	.2byte	0x294
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFlowMeterK2
	.uleb128 0xa
	.4byte	.LASF642
	.byte	0x2
	.2byte	0x295
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFlowMeterK3
	.uleb128 0xa
	.4byte	.LASF643
	.byte	0x2
	.2byte	0x296
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFlowMeterOffset1
	.uleb128 0xa
	.4byte	.LASF644
	.byte	0x2
	.2byte	0x297
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFlowMeterOffset2
	.uleb128 0xa
	.4byte	.LASF645
	.byte	0x2
	.2byte	0x298
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFlowMeterOffset3
	.uleb128 0xa
	.4byte	.LASF646
	.byte	0x2
	.2byte	0x299
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFlowMeterType1
	.uleb128 0xa
	.4byte	.LASF647
	.byte	0x2
	.2byte	0x29a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFlowMeterType2
	.uleb128 0xa
	.4byte	.LASF648
	.byte	0x2
	.2byte	0x29b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFlowMeterType3
	.uleb128 0xa
	.4byte	.LASF649
	.byte	0x2
	.2byte	0x29c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFMType1IsBermad
	.uleb128 0xa
	.4byte	.LASF650
	.byte	0x2
	.2byte	0x29d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFMType2IsBermad
	.uleb128 0xa
	.4byte	.LASF651
	.byte	0x2
	.2byte	0x29e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCFMType3IsBermad
	.uleb128 0xa
	.4byte	.LASF652
	.byte	0x2
	.2byte	0x29f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCGroupID
	.uleb128 0xa
	.4byte	.LASF653
	.byte	0x2
	.2byte	0x2a0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCMVType1
	.uleb128 0xa
	.4byte	.LASF654
	.byte	0x2
	.2byte	0x2a1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPhysicallyAvailable
	.uleb128 0xa
	.4byte	.LASF655
	.byte	0x2
	.2byte	0x2a2
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesAccumGal1
	.uleb128 0xa
	.4byte	.LASF656
	.byte	0x2
	.2byte	0x2a3
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesAccumGal2
	.uleb128 0xa
	.4byte	.LASF657
	.byte	0x2
	.2byte	0x2a4
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesAccumGal3
	.uleb128 0xa
	.4byte	.LASF658
	.byte	0x2
	.2byte	0x2a5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesAccumMS1
	.uleb128 0xa
	.4byte	.LASF659
	.byte	0x2
	.2byte	0x2a6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesAccumMS2
	.uleb128 0xa
	.4byte	.LASF660
	.byte	0x2
	.2byte	0x2a7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesAccumMS3
	.uleb128 0xa
	.4byte	.LASF661
	.byte	0x2
	.2byte	0x2a8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesAccumPulses1
	.uleb128 0xa
	.4byte	.LASF662
	.byte	0x2
	.2byte	0x2a9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesAccumPulses2
	.uleb128 0xa
	.4byte	.LASF663
	.byte	0x2
	.2byte	0x2aa
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesAccumPulses3
	.uleb128 0xa
	.4byte	.LASF664
	.byte	0x2
	.2byte	0x2ab
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesBoxIndex
	.uleb128 0xa
	.4byte	.LASF665
	.byte	0x2
	.2byte	0x2ac
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDecoderSN1
	.uleb128 0xa
	.4byte	.LASF666
	.byte	0x2
	.2byte	0x2ad
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDecoderSN2
	.uleb128 0xa
	.4byte	.LASF667
	.byte	0x2
	.2byte	0x2ae
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDecoderSN3
	.uleb128 0xa
	.4byte	.LASF668
	.byte	0x2
	.2byte	0x2af
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDelivered5SecAvg1
	.uleb128 0xa
	.4byte	.LASF669
	.byte	0x2
	.2byte	0x2b0
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDelivered5SecAvg2
	.uleb128 0xa
	.4byte	.LASF670
	.byte	0x2
	.2byte	0x2b1
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDelivered5SecAvg3
	.uleb128 0xa
	.4byte	.LASF671
	.byte	0x2
	.2byte	0x2b2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDeliveredMVCurrent1
	.uleb128 0xa
	.4byte	.LASF672
	.byte	0x2
	.2byte	0x2b3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDeliveredMVCurrent2
	.uleb128 0xa
	.4byte	.LASF673
	.byte	0x2
	.2byte	0x2b4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDeliveredMVCurrent3
	.uleb128 0xa
	.4byte	.LASF674
	.byte	0x2
	.2byte	0x2b5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDeliveredPumpCurrent1
	.uleb128 0xa
	.4byte	.LASF675
	.byte	0x2
	.2byte	0x2b6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDeliveredPumpCurrent2
	.uleb128 0xa
	.4byte	.LASF676
	.byte	0x2
	.2byte	0x2b7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesDeliveredPumpCurrent3
	.uleb128 0xa
	.4byte	.LASF677
	.byte	0x2
	.2byte	0x2b8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesFileType
	.uleb128 0xa
	.4byte	.LASF678
	.byte	0x2
	.2byte	0x2b9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesGroupID
	.uleb128 0xa
	.4byte	.LASF679
	.byte	0x2
	.2byte	0x2ba
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCPreservesIndex
	.uleb128 0xa
	.4byte	.LASF680
	.byte	0x2
	.2byte	0x2bb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCReedSwitchType1
	.uleb128 0xa
	.4byte	.LASF681
	.byte	0x2
	.2byte	0x2bc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCReedSwitchType2
	.uleb128 0xa
	.4byte	.LASF682
	.byte	0x2
	.2byte	0x2bd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCReedSwitchType3
	.uleb128 0xa
	.4byte	.LASF683
	.byte	0x2
	.2byte	0x2be
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCType
	.uleb128 0xa
	.4byte	.LASF684
	.byte	0x2
	.2byte	0x2bf
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_POCUsage
	.uleb128 0xa
	.4byte	.LASF685
	.byte	0x2
	.2byte	0x2c0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestBadCRC
	.uleb128 0xa
	.4byte	.LASF686
	.byte	0x2
	.2byte	0x2c1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestCommWithET2000e
	.uleb128 0xa
	.4byte	.LASF687
	.byte	0x2
	.2byte	0x2c2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestCS3000SN
	.uleb128 0xa
	.4byte	.LASF688
	.byte	0x2
	.2byte	0x2c3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestDeviceIndex
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x3353
	.uleb128 0x4
	.4byte	0x38
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.4byte	.LASF689
	.byte	0x2
	.2byte	0x2c4
	.4byte	0x3343
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestET2000eAddr0
	.uleb128 0xa
	.4byte	.LASF690
	.byte	0x2
	.2byte	0x2c5
	.4byte	0x3343
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestET2000eAddr1
	.uleb128 0xa
	.4byte	.LASF691
	.byte	0x2
	.2byte	0x2c6
	.4byte	0x3343
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestET2000eAddr2
	.uleb128 0xa
	.4byte	.LASF692
	.byte	0x2
	.2byte	0x2c7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestInProgress
	.uleb128 0xa
	.4byte	.LASF693
	.byte	0x2
	.2byte	0x2c8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestNoResp
	.uleb128 0xa
	.4byte	.LASF694
	.byte	0x2
	.2byte	0x2c9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestPacketsSent
	.uleb128 0xa
	.4byte	.LASF695
	.byte	0x2
	.2byte	0x2ca
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestPort
	.uleb128 0xa
	.4byte	.LASF696
	.byte	0x2
	.2byte	0x2cb
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestRoundTripAvgMS
	.uleb128 0xa
	.4byte	.LASF697
	.byte	0x2
	.2byte	0x2cc
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestRoundTripMaxMS
	.uleb128 0xa
	.4byte	.LASF698
	.byte	0x2
	.2byte	0x2cd
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestRoundTripMinMS
	.uleb128 0xa
	.4byte	.LASF699
	.byte	0x2
	.2byte	0x2ce
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestStartTime
	.uleb128 0xa
	.4byte	.LASF700
	.byte	0x2
	.2byte	0x2cf
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RadioTestSuccessfulPercent
	.uleb128 0xa
	.4byte	.LASF701
	.byte	0x2
	.2byte	0x2d0
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RainBucketInstalledAt
	.uleb128 0xa
	.4byte	.LASF702
	.byte	0x2
	.2byte	0x2d1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RainBucketInUse
	.uleb128 0xa
	.4byte	.LASF703
	.byte	0x2
	.2byte	0x2d2
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RainBucketMaximum24Hour
	.uleb128 0xa
	.4byte	.LASF704
	.byte	0x2
	.2byte	0x2d3
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RainBucketMaximumHourly
	.uleb128 0xa
	.4byte	.LASF705
	.byte	0x2
	.2byte	0x2d4
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RainBucketMinimum
	.uleb128 0xa
	.4byte	.LASF706
	.byte	0x2
	.2byte	0x2d5
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RainSwitchControllerName
	.uleb128 0xa
	.4byte	.LASF707
	.byte	0x2
	.2byte	0x2d6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RainSwitchInUse
	.uleb128 0xa
	.4byte	.LASF708
	.byte	0x2
	.2byte	0x2d7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RainSwitchSelected
	.uleb128 0xa
	.4byte	.LASF709
	.byte	0x2
	.2byte	0x2d8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptController
	.uleb128 0xa
	.4byte	.LASF710
	.byte	0x2
	.2byte	0x2d9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptCycles
	.uleb128 0xa
	.4byte	.LASF711
	.byte	0x2
	.2byte	0x2da
	.4byte	0x3fd
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptDate
	.uleb128 0xa
	.4byte	.LASF712
	.byte	0x2
	.2byte	0x2db
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptDecoderOutput
	.uleb128 0xa
	.4byte	.LASF713
	.byte	0x2
	.2byte	0x2dc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptDecoderSN
	.uleb128 0xa
	.4byte	.LASF714
	.byte	0x2
	.2byte	0x2dd
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptFlags
	.uleb128 0xa
	.4byte	.LASF715
	.byte	0x2
	.2byte	0x2de
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptFlow
	.uleb128 0xa
	.4byte	.LASF716
	.byte	0x2
	.2byte	0x2df
	.4byte	0x3567
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptIrrigGal
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF717
	.uleb128 0xa
	.4byte	.LASF718
	.byte	0x2
	.2byte	0x2e0
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptIrrigMin
	.uleb128 0xa
	.4byte	.LASF719
	.byte	0x2
	.2byte	0x2e1
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptLastMeasuredCurrent
	.uleb128 0xa
	.4byte	.LASF720
	.byte	0x2
	.2byte	0x2e2
	.4byte	0x3567
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptManualGal
	.uleb128 0xa
	.4byte	.LASF721
	.byte	0x2
	.2byte	0x2e3
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptManualMin
	.uleb128 0xa
	.4byte	.LASF722
	.byte	0x2
	.2byte	0x2e4
	.4byte	0x3567
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptManualPGal
	.uleb128 0xa
	.4byte	.LASF723
	.byte	0x2
	.2byte	0x2e5
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptManualPMin
	.uleb128 0xa
	.4byte	.LASF724
	.byte	0x2
	.2byte	0x2e6
	.4byte	0x3567
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptNonCGal
	.uleb128 0xa
	.4byte	.LASF725
	.byte	0x2
	.2byte	0x2e7
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptNonCMin
	.uleb128 0xa
	.4byte	.LASF726
	.byte	0x2
	.2byte	0x2e8
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptRainMin
	.uleb128 0xa
	.4byte	.LASF727
	.byte	0x2
	.2byte	0x2e9
	.4byte	0x3567
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptRReGal
	.uleb128 0xa
	.4byte	.LASF728
	.byte	0x2
	.2byte	0x2ea
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptRReMin
	.uleb128 0xa
	.4byte	.LASF729
	.byte	0x2
	.2byte	0x2eb
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptScheduledMin
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x3662
	.uleb128 0x4
	.4byte	0x38
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.4byte	.LASF730
	.byte	0x2
	.2byte	0x2ec
	.4byte	0x3652
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptStartTime
	.uleb128 0xa
	.4byte	.LASF731
	.byte	0x2
	.2byte	0x2ed
	.4byte	0x1d90
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptStation
	.uleb128 0xa
	.4byte	.LASF732
	.byte	0x2
	.2byte	0x2ee
	.4byte	0x3567
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptTestGal
	.uleb128 0xa
	.4byte	.LASF733
	.byte	0x2
	.2byte	0x2ef
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptTestMin
	.uleb128 0xa
	.4byte	.LASF734
	.byte	0x2
	.2byte	0x2f0
	.4byte	0x3567
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptWalkThruGal
	.uleb128 0xa
	.4byte	.LASF735
	.byte	0x2
	.2byte	0x2f1
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_RptWalkThruMin
	.uleb128 0xa
	.4byte	.LASF736
	.byte	0x2
	.2byte	0x2f2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleBeginDate
	.uleb128 0xa
	.4byte	.LASF737
	.byte	0x2
	.2byte	0x2f3
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleBeginDateStr
	.uleb128 0xa
	.4byte	.LASF738
	.byte	0x2
	.2byte	0x2f4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleIrrigateOn29thOr31st
	.uleb128 0xa
	.4byte	.LASF739
	.byte	0x2
	.2byte	0x2f5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleMowDay
	.uleb128 0xa
	.4byte	.LASF740
	.byte	0x2
	.2byte	0x2f6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleNoStationsInGroup
	.uleb128 0xa
	.4byte	.LASF741
	.byte	0x2
	.2byte	0x2f7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleStartTime
	.uleb128 0xa
	.4byte	.LASF742
	.byte	0x2
	.2byte	0x2f8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleStartTimeEnabled
	.uleb128 0xa
	.4byte	.LASF743
	.byte	0x2
	.2byte	0x2f9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleStartTimeEqualsStopTime
	.uleb128 0xa
	.4byte	.LASF744
	.byte	0x2
	.2byte	0x2fa
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleStopTime
	.uleb128 0xa
	.4byte	.LASF745
	.byte	0x2
	.2byte	0x2fb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleStopTimeEnabled
	.uleb128 0xa
	.4byte	.LASF746
	.byte	0x2
	.2byte	0x2fc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleType
	.uleb128 0xa
	.4byte	.LASF747
	.byte	0x2
	.2byte	0x2fd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleTypeScrollItem
	.uleb128 0xa
	.4byte	.LASF748
	.byte	0x2
	.2byte	0x2fe
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleUseETAveraging
	.uleb128 0xa
	.4byte	.LASF749
	.byte	0x2
	.2byte	0x2ff
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleUsesET
	.uleb128 0xa
	.4byte	.LASF750
	.byte	0x2
	.2byte	0x300
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleWaterDay_Fri
	.uleb128 0xa
	.4byte	.LASF751
	.byte	0x2
	.2byte	0x301
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleWaterDay_Mon
	.uleb128 0xa
	.4byte	.LASF752
	.byte	0x2
	.2byte	0x302
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleWaterDay_Sat
	.uleb128 0xa
	.4byte	.LASF753
	.byte	0x2
	.2byte	0x303
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleWaterDay_Sun
	.uleb128 0xa
	.4byte	.LASF754
	.byte	0x2
	.2byte	0x304
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleWaterDay_Thu
	.uleb128 0xa
	.4byte	.LASF755
	.byte	0x2
	.2byte	0x305
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleWaterDay_Tue
	.uleb128 0xa
	.4byte	.LASF756
	.byte	0x2
	.2byte	0x306
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScheduleWaterDay_Wed
	.uleb128 0xa
	.4byte	.LASF757
	.byte	0x2
	.2byte	0x307
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScrollBoxHorizScrollMarker
	.uleb128 0xa
	.4byte	.LASF758
	.byte	0x2
	.2byte	0x308
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ScrollBoxHorizScrollPos
	.uleb128 0xa
	.4byte	.LASF759
	.byte	0x2
	.2byte	0x309
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortCDA
	.uleb128 0xa
	.4byte	.LASF760
	.byte	0x2
	.2byte	0x30a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortCDB
	.uleb128 0xa
	.4byte	.LASF761
	.byte	0x2
	.2byte	0x30b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortCTSA
	.uleb128 0xa
	.4byte	.LASF762
	.byte	0x2
	.2byte	0x30c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortCTSB
	.uleb128 0xa
	.4byte	.LASF763
	.byte	0x2
	.2byte	0x30d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortOptionA
	.uleb128 0xa
	.4byte	.LASF764
	.byte	0x2
	.2byte	0x30e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortOptionB
	.uleb128 0xa
	.4byte	.LASF765
	.byte	0x2
	.2byte	0x30f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortRcvdA
	.uleb128 0xa
	.4byte	.LASF766
	.byte	0x2
	.2byte	0x310
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortRcvdB
	.uleb128 0xa
	.4byte	.LASF767
	.byte	0x2
	.2byte	0x311
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortRcvdTP
	.uleb128 0xa
	.4byte	.LASF768
	.byte	0x2
	.2byte	0x312
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortStateA
	.uleb128 0xa
	.4byte	.LASF769
	.byte	0x2
	.2byte	0x313
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortStateB
	.uleb128 0xa
	.4byte	.LASF770
	.byte	0x2
	.2byte	0x314
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortStateTP
	.uleb128 0xa
	.4byte	.LASF771
	.byte	0x2
	.2byte	0x315
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortXmitA
	.uleb128 0xa
	.4byte	.LASF772
	.byte	0x2
	.2byte	0x316
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortXmitB
	.uleb128 0xa
	.4byte	.LASF773
	.byte	0x2
	.2byte	0x317
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortXmitLengthA
	.uleb128 0xa
	.4byte	.LASF774
	.byte	0x2
	.2byte	0x318
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortXmitLengthB
	.uleb128 0xa
	.4byte	.LASF775
	.byte	0x2
	.2byte	0x319
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortXmittingA
	.uleb128 0xa
	.4byte	.LASF776
	.byte	0x2
	.2byte	0x31a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortXmittingB
	.uleb128 0xa
	.4byte	.LASF777
	.byte	0x2
	.2byte	0x31b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SerialPortXmitTP
	.uleb128 0xa
	.4byte	.LASF778
	.byte	0x2
	.2byte	0x31c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_ShowDivider
	.uleb128 0xa
	.4byte	.LASF779
	.byte	0x2
	.2byte	0x31d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SpinnerPos
	.uleb128 0xa
	.4byte	.LASF780
	.byte	0x2
	.2byte	0x31e
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRFirmwareVer
	.uleb128 0xa
	.4byte	.LASF781
	.byte	0x2
	.2byte	0x31f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRGroup
	.uleb128 0xa
	.4byte	.LASF782
	.byte	0x2
	.2byte	0x320
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRMode
	.uleb128 0xa
	.4byte	.LASF783
	.byte	0x2
	.2byte	0x321
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRPort
	.uleb128 0xa
	.4byte	.LASF784
	.byte	0x2
	.2byte	0x322
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRRepeater
	.uleb128 0xa
	.4byte	.LASF785
	.byte	0x2
	.2byte	0x323
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRRepeaterLinkedToItself
	.uleb128 0xa
	.4byte	.LASF786
	.byte	0x2
	.2byte	0x324
	.4byte	0x23d9
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRSerialNumber
	.uleb128 0xa
	.4byte	.LASF787
	.byte	0x2
	.2byte	0x325
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRSubnetRcvMaster
	.uleb128 0xa
	.4byte	.LASF788
	.byte	0x2
	.2byte	0x326
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRSubnetRcvRepeater
	.uleb128 0xa
	.4byte	.LASF789
	.byte	0x2
	.2byte	0x327
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRSubnetRcvSlave
	.uleb128 0xa
	.4byte	.LASF790
	.byte	0x2
	.2byte	0x328
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRSubnetXmtMaster
	.uleb128 0xa
	.4byte	.LASF791
	.byte	0x2
	.2byte	0x329
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRSubnetXmtRepeater
	.uleb128 0xa
	.4byte	.LASF792
	.byte	0x2
	.2byte	0x32a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRSubnetXmtSlave
	.uleb128 0xa
	.4byte	.LASF793
	.byte	0x2
	.2byte	0x32b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SRTransmitPower
	.uleb128 0xa
	.4byte	.LASF794
	.byte	0x2
	.2byte	0x32c
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationCopyGroupName
	.uleb128 0xa
	.4byte	.LASF795
	.byte	0x2
	.2byte	0x32d
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationCopyTextOffset_Left
	.uleb128 0xa
	.4byte	.LASF796
	.byte	0x2
	.2byte	0x32e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationCopyTextOffset_Right
	.uleb128 0xa
	.4byte	.LASF797
	.byte	0x2
	.2byte	0x32f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationDecoderOutput
	.uleb128 0xa
	.4byte	.LASF798
	.byte	0x2
	.2byte	0x330
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationDecoderSerialNumber
	.uleb128 0xa
	.4byte	.LASF799
	.byte	0x2
	.2byte	0x331
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationDescription
	.uleb128 0xa
	.4byte	.LASF800
	.byte	0x2
	.2byte	0x332
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupAllowableDepletion
	.uleb128 0xa
	.4byte	.LASF801
	.byte	0x2
	.2byte	0x333
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupAllowableSurfaceAccum
	.uleb128 0xa
	.4byte	.LASF802
	.byte	0x2
	.2byte	0x334
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupAvailableWater
	.uleb128 0xa
	.4byte	.LASF803
	.byte	0x2
	.2byte	0x335
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupDensityFactor
	.uleb128 0xa
	.4byte	.LASF804
	.byte	0x2
	.2byte	0x336
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupExposure
	.uleb128 0xa
	.4byte	.LASF805
	.byte	0x2
	.2byte	0x337
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupHeadType
	.uleb128 0xa
	.4byte	.LASF806
	.byte	0x2
	.2byte	0x338
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupInUse
	.uleb128 0xa
	.4byte	.LASF807
	.byte	0x2
	.2byte	0x339
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc1
	.uleb128 0xa
	.4byte	.LASF808
	.byte	0x2
	.2byte	0x33a
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc10
	.uleb128 0xa
	.4byte	.LASF809
	.byte	0x2
	.2byte	0x33b
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc11
	.uleb128 0xa
	.4byte	.LASF810
	.byte	0x2
	.2byte	0x33c
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc12
	.uleb128 0xa
	.4byte	.LASF811
	.byte	0x2
	.2byte	0x33d
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc2
	.uleb128 0xa
	.4byte	.LASF812
	.byte	0x2
	.2byte	0x33e
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc3
	.uleb128 0xa
	.4byte	.LASF813
	.byte	0x2
	.2byte	0x33f
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc4
	.uleb128 0xa
	.4byte	.LASF814
	.byte	0x2
	.2byte	0x340
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc5
	.uleb128 0xa
	.4byte	.LASF815
	.byte	0x2
	.2byte	0x341
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc6
	.uleb128 0xa
	.4byte	.LASF816
	.byte	0x2
	.2byte	0x342
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc7
	.uleb128 0xa
	.4byte	.LASF817
	.byte	0x2
	.2byte	0x343
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc8
	.uleb128 0xa
	.4byte	.LASF818
	.byte	0x2
	.2byte	0x344
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKc9
	.uleb128 0xa
	.4byte	.LASF819
	.byte	0x2
	.2byte	0x345
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupKcsAreCustom
	.uleb128 0xa
	.4byte	.LASF820
	.byte	0x2
	.2byte	0x346
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupMicroclimateFactor
	.uleb128 0xa
	.4byte	.LASF821
	.byte	0x2
	.2byte	0x347
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupMoistureSensorDecoderSN
	.uleb128 0xa
	.4byte	.LASF822
	.byte	0x2
	.2byte	0x348
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupPlantType
	.uleb128 0xa
	.4byte	.LASF823
	.byte	0x2
	.2byte	0x349
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupPrecipRate
	.uleb128 0xa
	.4byte	.LASF824
	.byte	0x2
	.2byte	0x34a
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupRootZoneDepth
	.uleb128 0xa
	.4byte	.LASF825
	.byte	0x2
	.2byte	0x34b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupSlopePercentage
	.uleb128 0xa
	.4byte	.LASF826
	.byte	0x2
	.2byte	0x34c
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupSoilIntakeRate
	.uleb128 0xa
	.4byte	.LASF827
	.byte	0x2
	.2byte	0x34d
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupSoilStorageCapacity
	.uleb128 0xa
	.4byte	.LASF828
	.byte	0x2
	.2byte	0x34e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupSoilType
	.uleb128 0xa
	.4byte	.LASF829
	.byte	0x2
	.2byte	0x34f
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupSpeciesFactor
	.uleb128 0xa
	.4byte	.LASF830
	.byte	0x2
	.2byte	0x350
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupSystemGID
	.uleb128 0xa
	.4byte	.LASF831
	.byte	0x2
	.2byte	0x351
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationGroupUsableRain
	.uleb128 0xa
	.4byte	.LASF832
	.byte	0x2
	.2byte	0x352
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoBoxIndex
	.uleb128 0xa
	.4byte	.LASF833
	.byte	0x2
	.2byte	0x353
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoControllerName
	.uleb128 0xa
	.4byte	.LASF834
	.byte	0x2
	.2byte	0x354
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoCycleTime
	.uleb128 0xa
	.4byte	.LASF835
	.byte	0x2
	.2byte	0x355
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoCycleTooShort
	.uleb128 0xa
	.4byte	.LASF836
	.byte	0x2
	.2byte	0x356
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoDU
	.uleb128 0xa
	.4byte	.LASF837
	.byte	0x2
	.2byte	0x357
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoEstMin
	.uleb128 0xa
	.4byte	.LASF838
	.byte	0x2
	.2byte	0x358
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoETFactor
	.uleb128 0xa
	.4byte	.LASF839
	.byte	0x2
	.2byte	0x359
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoExpectedFlow
	.uleb128 0xa
	.4byte	.LASF840
	.byte	0x2
	.2byte	0x35a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoFlowStatus
	.uleb128 0xa
	.4byte	.LASF841
	.byte	0x2
	.2byte	0x35b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoGroupHasStartTime
	.uleb128 0xa
	.4byte	.LASF842
	.byte	0x2
	.2byte	0x35c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoIStatus
	.uleb128 0xa
	.4byte	.LASF843
	.byte	0x2
	.2byte	0x35d
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoMoistureBalance
	.uleb128 0xa
	.4byte	.LASF844
	.byte	0x2
	.2byte	0x35e
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoMoistureBalancePercent
	.uleb128 0xa
	.4byte	.LASF845
	.byte	0x2
	.2byte	0x35f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoNoWaterDays
	.uleb128 0xa
	.4byte	.LASF846
	.byte	0x2
	.2byte	0x360
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoNumber
	.uleb128 0xa
	.4byte	.LASF847
	.byte	0x2
	.2byte	0x361
	.4byte	0x1d90
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoNumber_str
	.uleb128 0xa
	.4byte	.LASF848
	.byte	0x2
	.2byte	0x362
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoShowCopyStation
	.uleb128 0xa
	.4byte	.LASF849
	.byte	0x2
	.2byte	0x363
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoShowEstMin
	.uleb128 0xa
	.4byte	.LASF850
	.byte	0x2
	.2byte	0x364
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoShowPercentAdjust
	.uleb128 0xa
	.4byte	.LASF851
	.byte	0x2
	.2byte	0x365
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoShowPercentOfET
	.uleb128 0xa
	.4byte	.LASF852
	.byte	0x2
	.2byte	0x366
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoSoakInTime
	.uleb128 0xa
	.4byte	.LASF853
	.byte	0x2
	.2byte	0x367
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoSquareFootage
	.uleb128 0xa
	.4byte	.LASF854
	.byte	0x2
	.2byte	0x368
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoStationGroup
	.uleb128 0xa
	.4byte	.LASF855
	.byte	0x2
	.2byte	0x369
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationInfoTotalMinutes
	.uleb128 0xa
	.4byte	.LASF856
	.byte	0x2
	.2byte	0x36a
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationSelectionGridControllerName
	.uleb128 0xa
	.4byte	.LASF857
	.byte	0x2
	.2byte	0x36b
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationSelectionGridGroupName
	.uleb128 0xa
	.4byte	.LASF858
	.byte	0x2
	.2byte	0x36c
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationSelectionGridMarkerPos
	.uleb128 0xa
	.4byte	.LASF859
	.byte	0x2
	.2byte	0x36d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationSelectionGridMarkerSize
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x4018
	.uleb128 0x4
	.4byte	0x38
	.byte	0x80
	.byte	0
	.uleb128 0xa
	.4byte	.LASF860
	.byte	0x2
	.2byte	0x36e
	.4byte	0x4008
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationSelectionGridScreenTitle
	.uleb128 0xa
	.4byte	.LASF861
	.byte	0x2
	.2byte	0x36f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.uleb128 0xa
	.4byte	.LASF862
	.byte	0x2
	.2byte	0x370
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StationSelectionGridStationCount
	.uleb128 0xa
	.4byte	.LASF863
	.byte	0x2
	.2byte	0x371
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusChainDown
	.uleb128 0xa
	.4byte	.LASF864
	.byte	0x2
	.2byte	0x372
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusCurrentDraw
	.uleb128 0xa
	.4byte	.LASF865
	.byte	0x2
	.2byte	0x373
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusCycleLeft
	.uleb128 0xa
	.4byte	.LASF866
	.byte	0x2
	.2byte	0x374
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusElectricalErrors
	.uleb128 0xa
	.4byte	.LASF867
	.byte	0x2
	.2byte	0x375
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusETGageConnected
	.uleb128 0xa
	.4byte	.LASF868
	.byte	0x2
	.2byte	0x376
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusETGageControllerName
	.uleb128 0xa
	.4byte	.LASF869
	.byte	0x2
	.2byte	0x377
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusETGageReading
	.uleb128 0xa
	.4byte	.LASF870
	.byte	0x2
	.2byte	0x378
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusFlowErrors
	.uleb128 0xa
	.4byte	.LASF871
	.byte	0x2
	.2byte	0x379
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusFreezeSwitchConnected
	.uleb128 0xa
	.4byte	.LASF872
	.byte	0x2
	.2byte	0x37a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusFreezeSwitchState
	.uleb128 0xa
	.4byte	.LASF873
	.byte	0x2
	.2byte	0x37b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusFuseBlown
	.uleb128 0xa
	.4byte	.LASF874
	.byte	0x2
	.2byte	0x37c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusIrrigationActivityState
	.uleb128 0xa
	.4byte	.LASF875
	.byte	0x2
	.2byte	0x37d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusMLBInEffect
	.uleb128 0xa
	.4byte	.LASF876
	.byte	0x2
	.2byte	0x37e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusMVORInEffect
	.uleb128 0xa
	.4byte	.LASF877
	.byte	0x2
	.2byte	0x37f
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusNextScheduledIrriDate
	.uleb128 0xa
	.4byte	.LASF878
	.byte	0x2
	.2byte	0x380
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusNextScheduledIrriGID
	.uleb128 0xa
	.4byte	.LASF879
	.byte	0x2
	.2byte	0x381
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusNextScheduledIrriType
	.uleb128 0xa
	.4byte	.LASF880
	.byte	0x2
	.2byte	0x382
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusNOWDaysInEffect
	.uleb128 0xa
	.4byte	.LASF881
	.byte	0x2
	.2byte	0x383
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusOverviewStationStr
	.uleb128 0xa
	.4byte	.LASF882
	.byte	0x2
	.2byte	0x384
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusRainBucketConnected
	.uleb128 0xa
	.4byte	.LASF883
	.byte	0x2
	.2byte	0x385
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusRainBucketControllerName
	.uleb128 0xa
	.4byte	.LASF884
	.byte	0x2
	.2byte	0x386
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusRainBucketReading
	.uleb128 0xa
	.4byte	.LASF885
	.byte	0x2
	.2byte	0x387
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusRainSwitchConnected
	.uleb128 0xa
	.4byte	.LASF886
	.byte	0x2
	.2byte	0x388
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusRainSwitchState
	.uleb128 0xa
	.4byte	.LASF887
	.byte	0x2
	.2byte	0x389
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusShowLiveScreens
	.uleb128 0xa
	.4byte	.LASF888
	.byte	0x2
	.2byte	0x38a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusShowSystemFlow
	.uleb128 0xa
	.4byte	.LASF889
	.byte	0x2
	.2byte	0x38b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusSystemFlowActual
	.uleb128 0xa
	.4byte	.LASF890
	.byte	0x2
	.2byte	0x38c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusSystemFlowExpected
	.uleb128 0xa
	.4byte	.LASF891
	.byte	0x2
	.2byte	0x38d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusSystemFlowMLBAllowed
	.uleb128 0xa
	.4byte	.LASF892
	.byte	0x2
	.2byte	0x38e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusSystemFlowMLBMeasured
	.uleb128 0xa
	.4byte	.LASF893
	.byte	0x2
	.2byte	0x38f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusSystemFlowShowMLBDetected
	.uleb128 0xa
	.4byte	.LASF894
	.byte	0x2
	.2byte	0x390
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusSystemFlowStatus
	.uleb128 0xa
	.4byte	.LASF895
	.byte	0x2
	.2byte	0x391
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusSystemFlowSystem
	.uleb128 0xa
	.4byte	.LASF896
	.byte	0x2
	.2byte	0x392
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusSystemFlowValvesOn
	.uleb128 0xa
	.4byte	.LASF897
	.byte	0x2
	.2byte	0x393
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusTypeOfSchedule
	.uleb128 0xa
	.4byte	.LASF898
	.byte	0x2
	.2byte	0x394
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusWindGageConnected
	.uleb128 0xa
	.4byte	.LASF899
	.byte	0x2
	.2byte	0x395
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusWindGageControllerName
	.uleb128 0xa
	.4byte	.LASF900
	.byte	0x2
	.2byte	0x396
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusWindGageReading
	.uleb128 0xa
	.4byte	.LASF901
	.byte	0x2
	.2byte	0x397
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StatusWindPaused
	.uleb128 0xa
	.4byte	.LASF902
	.byte	0x2
	.2byte	0x398
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StopKeyPending
	.uleb128 0xa
	.4byte	.LASF903
	.byte	0x2
	.2byte	0x399
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_StopKeyReasonInList
	.uleb128 0xa
	.4byte	.LASF904
	.byte	0x2
	.2byte	0x39a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingInUse
	.uleb128 0xa
	.4byte	.LASF905
	.byte	0x2
	.2byte	0x39b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingRange1
	.uleb128 0xa
	.4byte	.LASF906
	.byte	0x2
	.2byte	0x39c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingRange2
	.uleb128 0xa
	.4byte	.LASF907
	.byte	0x2
	.2byte	0x39d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingRange3
	.uleb128 0xa
	.4byte	.LASF908
	.byte	0x2
	.2byte	0x39e
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingToleranceMinus1
	.uleb128 0xa
	.4byte	.LASF909
	.byte	0x2
	.2byte	0x39f
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingToleranceMinus2
	.uleb128 0xa
	.4byte	.LASF910
	.byte	0x2
	.2byte	0x3a0
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingToleranceMinus3
	.uleb128 0xa
	.4byte	.LASF911
	.byte	0x2
	.2byte	0x3a1
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingToleranceMinus4
	.uleb128 0xa
	.4byte	.LASF912
	.byte	0x2
	.2byte	0x3a2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingTolerancePlus1
	.uleb128 0xa
	.4byte	.LASF913
	.byte	0x2
	.2byte	0x3a3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingTolerancePlus2
	.uleb128 0xa
	.4byte	.LASF914
	.byte	0x2
	.2byte	0x3a4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingTolerancePlus3
	.uleb128 0xa
	.4byte	.LASF915
	.byte	0x2
	.2byte	0x3a5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemFlowCheckingTolerancePlus4
	.uleb128 0xa
	.4byte	.LASF916
	.byte	0x2
	.2byte	0x3a6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemInUse
	.uleb128 0xa
	.4byte	.LASF917
	.byte	0x2
	.2byte	0x3a7
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemName
	.uleb128 0xa
	.4byte	.LASF918
	.byte	0x2
	.2byte	0x3a8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemPOCSelected
	.uleb128 0xa
	.4byte	.LASF919
	.byte	0x2
	.2byte	0x3a9
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemPreserves5SecMostRecent
	.uleb128 0xa
	.4byte	.LASF920
	.byte	0x2
	.2byte	0x3aa
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemPreserves5SecNext
	.uleb128 0xa
	.4byte	.LASF921
	.byte	0x2
	.2byte	0x3ab
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemPreservesStabilityMostRecent5Sec
	.uleb128 0xa
	.4byte	.LASF922
	.byte	0x2
	.2byte	0x3ac
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_SystemPreservesStabilityNext
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x44d6
	.uleb128 0x8
	.4byte	0x38
	.2byte	0x7d0
	.byte	0
	.uleb128 0xa
	.4byte	.LASF923
	.byte	0x2
	.2byte	0x3ad
	.4byte	0x44c5
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TaskList
	.uleb128 0xa
	.4byte	.LASF924
	.byte	0x2
	.2byte	0x3ae
	.4byte	0x49a
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TestRunTime
	.uleb128 0xa
	.4byte	.LASF925
	.byte	0x2
	.2byte	0x3af
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TestStationAutoMode
	.uleb128 0xa
	.4byte	.LASF926
	.byte	0x2
	.2byte	0x3b0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TestStationAutoOffTime
	.uleb128 0xa
	.4byte	.LASF927
	.byte	0x2
	.2byte	0x3b1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TestStationAutoOnTime
	.uleb128 0xa
	.4byte	.LASF928
	.byte	0x2
	.2byte	0x3b2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TestStationOnOff
	.uleb128 0xa
	.4byte	.LASF929
	.byte	0x2
	.2byte	0x3b3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TestStationStatus
	.uleb128 0xa
	.4byte	.LASF930
	.byte	0x2
	.2byte	0x3b4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TestStationTimeDisplay
	.uleb128 0xa
	.4byte	.LASF931
	.byte	0x2
	.2byte	0x3b5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TestStationTimeRemaining
	.uleb128 0xa
	.4byte	.LASF932
	.byte	0x2
	.2byte	0x3b6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TurnOffControllerIsOff
	.uleb128 0xa
	.4byte	.LASF933
	.byte	0x2
	.2byte	0x3b7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDecoderFWVersion
	.uleb128 0xa
	.4byte	.LASF934
	.byte	0x2
	.2byte	0x3b8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDecoderSN
	.uleb128 0xa
	.4byte	.LASF935
	.byte	0x2
	.2byte	0x3b9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDecoderSNToAssign
	.uleb128 0xa
	.4byte	.LASF936
	.byte	0x2
	.2byte	0x3ba
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDecoderType
	.uleb128 0xa
	.4byte	.LASF937
	.byte	0x2
	.2byte	0x3bb
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDescA
	.uleb128 0xa
	.4byte	.LASF938
	.byte	0x2
	.2byte	0x3bc
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDescB
	.uleb128 0xa
	.4byte	.LASF939
	.byte	0x2
	.2byte	0x3bd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDiscoveredMoisDecoders
	.uleb128 0xa
	.4byte	.LASF940
	.byte	0x2
	.2byte	0x3be
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDiscoveredPOCDecoders
	.uleb128 0xa
	.4byte	.LASF941
	.byte	0x2
	.2byte	0x3bf
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDiscoveredStaDecoders
	.uleb128 0xa
	.4byte	.LASF942
	.byte	0x2
	.2byte	0x3c0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDiscoveryProgressBar
	.uleb128 0xa
	.4byte	.LASF943
	.byte	0x2
	.2byte	0x3c1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireDiscoveryState
	.uleb128 0xa
	.4byte	.LASF944
	.byte	0x2
	.2byte	0x3c2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireNumDiscoveredMoisDecoders
	.uleb128 0xa
	.4byte	.LASF945
	.byte	0x2
	.2byte	0x3c3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireNumDiscoveredPOCDecoders
	.uleb128 0xa
	.4byte	.LASF946
	.byte	0x2
	.2byte	0x3c4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireNumDiscoveredStaDecoders
	.uleb128 0xa
	.4byte	.LASF947
	.byte	0x2
	.2byte	0x3c5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireOrphanedPOCsExist
	.uleb128 0xa
	.4byte	.LASF948
	.byte	0x2
	.2byte	0x3c6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireOrphanedStationsExist
	.uleb128 0xa
	.4byte	.LASF949
	.byte	0x2
	.2byte	0x3c7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireOutputOnA
	.uleb128 0xa
	.4byte	.LASF950
	.byte	0x2
	.2byte	0x3c8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireOutputOnB
	.uleb128 0xa
	.4byte	.LASF951
	.byte	0x2
	.2byte	0x3c9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWirePOCDecoderIndex
	.uleb128 0xa
	.4byte	.LASF952
	.byte	0x2
	.2byte	0x3ca
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWirePOCInUse
	.uleb128 0xa
	.4byte	.LASF953
	.byte	0x2
	.2byte	0x3cb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWirePOCUsedForBypass
	.uleb128 0xa
	.4byte	.LASF954
	.byte	0x2
	.2byte	0x3cc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStaA
	.uleb128 0xa
	.4byte	.LASF955
	.byte	0x2
	.2byte	0x3cd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStaAIsSet
	.uleb128 0xa
	.4byte	.LASF956
	.byte	0x2
	.2byte	0x3ce
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStaB
	.uleb128 0xa
	.4byte	.LASF957
	.byte	0x2
	.2byte	0x3cf
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStaBIsSet
	.uleb128 0xa
	.4byte	.LASF958
	.byte	0x2
	.2byte	0x3d0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStaDecoderIndex
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x4792
	.uleb128 0x4
	.4byte	0x38
	.byte	0x4
	.byte	0
	.uleb128 0xa
	.4byte	.LASF959
	.byte	0x2
	.2byte	0x3d1
	.4byte	0x4782
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStaStrA
	.uleb128 0xa
	.4byte	.LASF960
	.byte	0x2
	.2byte	0x3d2
	.4byte	0x4782
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStaStrB
	.uleb128 0xa
	.4byte	.LASF961
	.byte	0x2
	.2byte	0x3d3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsBODs
	.uleb128 0xa
	.4byte	.LASF962
	.byte	0x2
	.2byte	0x3d4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsEEPCRCComParams
	.uleb128 0xa
	.4byte	.LASF963
	.byte	0x2
	.2byte	0x3d5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsEEPCRCErrStats
	.uleb128 0xa
	.4byte	.LASF964
	.byte	0x2
	.2byte	0x3d6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsEEPCRCS1Params
	.uleb128 0xa
	.4byte	.LASF965
	.byte	0x2
	.2byte	0x3d7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsEEPCRCS2Params
	.uleb128 0xa
	.4byte	.LASF966
	.byte	0x2
	.2byte	0x3d8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsPORs
	.uleb128 0xa
	.4byte	.LASF967
	.byte	0x2
	.2byte	0x3d9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxCRCErrs
	.uleb128 0xa
	.4byte	.LASF968
	.byte	0x2
	.2byte	0x3da
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxDataLpbkMsgs
	.uleb128 0xa
	.4byte	.LASF969
	.byte	0x2
	.2byte	0x3db
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxDecRstMsgs
	.uleb128 0xa
	.4byte	.LASF970
	.byte	0x2
	.2byte	0x3dc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxDiscConfMsgs
	.uleb128 0xa
	.4byte	.LASF971
	.byte	0x2
	.2byte	0x3dd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxDiscRstMsgs
	.uleb128 0xa
	.4byte	.LASF972
	.byte	0x2
	.2byte	0x3de
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxEnqMsgs
	.uleb128 0xa
	.4byte	.LASF973
	.byte	0x2
	.2byte	0x3df
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxGetParamsMsgs
	.uleb128 0xa
	.4byte	.LASF974
	.byte	0x2
	.2byte	0x3e0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxIDReqMsgs
	.uleb128 0xa
	.4byte	.LASF975
	.byte	0x2
	.2byte	0x3e1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxLongMsgs
	.uleb128 0xa
	.4byte	.LASF976
	.byte	0x2
	.2byte	0x3e2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxMsgs
	.uleb128 0xa
	.4byte	.LASF977
	.byte	0x2
	.2byte	0x3e3
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxPutParamsMsgs
	.uleb128 0xa
	.4byte	.LASF978
	.byte	0x2
	.2byte	0x3e4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxSolCtrlMsgs
	.uleb128 0xa
	.4byte	.LASF979
	.byte	0x2
	.2byte	0x3e5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs
	.uleb128 0xa
	.4byte	.LASF980
	.byte	0x2
	.2byte	0x3e6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxStatReqMsgs
	.uleb128 0xa
	.4byte	.LASF981
	.byte	0x2
	.2byte	0x3e7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsRxStatsReqMsgs
	.uleb128 0xa
	.4byte	.LASF982
	.byte	0x2
	.2byte	0x3e8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsS1UCos
	.uleb128 0xa
	.4byte	.LASF983
	.byte	0x2
	.2byte	0x3e9
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsS2UCos
	.uleb128 0xa
	.4byte	.LASF984
	.byte	0x2
	.2byte	0x3ea
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsTempCur
	.uleb128 0xa
	.4byte	.LASF985
	.byte	0x2
	.2byte	0x3eb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsTempMax
	.uleb128 0xa
	.4byte	.LASF986
	.byte	0x2
	.2byte	0x3ec
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsTxAcksSent
	.uleb128 0xa
	.4byte	.LASF987
	.byte	0x2
	.2byte	0x3ed
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_TwoWireStatsWDTs
	.uleb128 0xa
	.4byte	.LASF988
	.byte	0x2
	.2byte	0x3ee
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_UnusedGroupsRemoved
	.uleb128 0xa
	.4byte	.LASF989
	.byte	0x2
	.2byte	0x3ef
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_UnusedGroupsRemovedGreaterThan1
	.uleb128 0xa
	.4byte	.LASF990
	.byte	0x2
	.2byte	0x3f0
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_Volume
	.uleb128 0xa
	.4byte	.LASF991
	.byte	0x2
	.2byte	0x3f1
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_VolumeSliderPos
	.uleb128 0xa
	.4byte	.LASF992
	.byte	0x2
	.2byte	0x3f2
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WalkThruBoxIndex
	.uleb128 0xa
	.4byte	.LASF993
	.byte	0x2
	.2byte	0x3f3
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WalkThruControllerName
	.uleb128 0xa
	.4byte	.LASF994
	.byte	0x2
	.2byte	0x3f4
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WalkThruCountingDown
	.uleb128 0xa
	.4byte	.LASF995
	.byte	0x2
	.2byte	0x3f5
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WalkThruDelay
	.uleb128 0xa
	.4byte	.LASF996
	.byte	0x2
	.2byte	0x3f6
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WalkThruOrder
	.uleb128 0xa
	.4byte	.LASF997
	.byte	0x2
	.2byte	0x3f7
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WalkThruRunTime
	.uleb128 0xa
	.4byte	.LASF998
	.byte	0x2
	.2byte	0x3f8
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WalkThruShowStartInSeconds
	.uleb128 0xa
	.4byte	.LASF999
	.byte	0x2
	.2byte	0x3f9
	.4byte	0x1d90
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WalkThruStation_Str
	.uleb128 0xa
	.4byte	.LASF1000
	.byte	0x2
	.2byte	0x3fa
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WalkThruStationDesc
	.uleb128 0xa
	.4byte	.LASF1001
	.byte	0x2
	.2byte	0x3fb
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENChangeCredentials
	.uleb128 0xa
	.4byte	.LASF1002
	.byte	0x2
	.2byte	0x3fc
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENChangeKey
	.uleb128 0xa
	.4byte	.LASF1003
	.byte	0x2
	.2byte	0x3fd
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENChangePassphrase
	.uleb128 0xa
	.4byte	.LASF1004
	.byte	0x2
	.2byte	0x3fe
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENChangePassword
	.uleb128 0xa
	.4byte	.LASF1005
	.byte	0x2
	.2byte	0x3ff
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENKey
	.uleb128 0xa
	.4byte	.LASF1006
	.byte	0x2
	.2byte	0x400
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENMACAddress
	.uleb128 0xa
	.4byte	.LASF1007
	.byte	0x2
	.2byte	0x401
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENPassphrase
	.uleb128 0xa
	.4byte	.LASF1008
	.byte	0x2
	.2byte	0x402
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENRadioMode
	.uleb128 0xa
	.4byte	.LASF1009
	.byte	0x2
	.2byte	0x403
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENRadioType
	.uleb128 0xa
	.4byte	.LASF1010
	.byte	0x2
	.2byte	0x404
	.4byte	0x64d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENRSSI
	.uleb128 0xa
	.4byte	.LASF1011
	.byte	0x2
	.2byte	0x405
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENSecurity
	.uleb128 0xa
	.4byte	.LASF1012
	.byte	0x2
	.2byte	0x406
	.4byte	0x1fda
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENSSID
	.uleb128 0xa
	.4byte	.LASF1013
	.byte	0x2
	.2byte	0x407
	.4byte	0x118
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENStatus
	.uleb128 0xa
	.4byte	.LASF1014
	.byte	0x2
	.2byte	0x408
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWEPAuthentication
	.uleb128 0xa
	.4byte	.LASF1015
	.byte	0x2
	.2byte	0x409
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWEPKeySize
	.uleb128 0xa
	.4byte	.LASF1016
	.byte	0x2
	.2byte	0x40a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWEPKeySizeChanged
	.uleb128 0xa
	.4byte	.LASF1017
	.byte	0x2
	.2byte	0x40b
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWEPKeyType
	.uleb128 0xa
	.4byte	.LASF1018
	.byte	0x2
	.2byte	0x40c
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWEPShowTXKeyIndex
	.uleb128 0xa
	.4byte	.LASF1019
	.byte	0x2
	.2byte	0x40d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWEPTXKey
	.uleb128 0xa
	.4byte	.LASF1020
	.byte	0x2
	.2byte	0x40e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPA2Encryption
	.uleb128 0xa
	.4byte	.LASF1021
	.byte	0x2
	.2byte	0x40f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAAuthentication
	.uleb128 0xa
	.4byte	.LASF1022
	.byte	0x2
	.2byte	0x410
	.4byte	0xcee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAEAPTLSCredentials
	.uleb128 0xa
	.4byte	.LASF1023
	.byte	0x2
	.2byte	0x411
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAEAPTLSValidateCert
	.uleb128 0xa
	.4byte	.LASF1024
	.byte	0x2
	.2byte	0x412
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAEAPTTLSOption
	.uleb128 0xa
	.4byte	.LASF1025
	.byte	0x2
	.2byte	0x413
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAEncryption
	.uleb128 0xa
	.4byte	.LASF1026
	.byte	0x2
	.2byte	0x414
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAEncryptionCCMP
	.uleb128 0xa
	.4byte	.LASF1027
	.byte	0x2
	.2byte	0x415
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAEncryptionTKIP
	.uleb128 0xa
	.4byte	.LASF1028
	.byte	0x2
	.2byte	0x416
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAEncryptionWEP
	.uleb128 0xa
	.4byte	.LASF1029
	.byte	0x2
	.2byte	0x417
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAIEEE8021X
	.uleb128 0xa
	.4byte	.LASF1030
	.byte	0x2
	.2byte	0x418
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAKeyType
	.uleb128 0x3
	.4byte	0x128
	.4byte	0x4cfa
	.uleb128 0x4
	.4byte	0x38
	.byte	0x3f
	.byte	0
	.uleb128 0xa
	.4byte	.LASF1031
	.byte	0x2
	.2byte	0x419
	.4byte	0x4cea
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAPassword
	.uleb128 0xa
	.4byte	.LASF1032
	.byte	0x2
	.2byte	0x41a
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAPEAPOption
	.uleb128 0xa
	.4byte	.LASF1033
	.byte	0x2
	.2byte	0x41b
	.4byte	0x4cea
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WENWPAUsername
	.uleb128 0xa
	.4byte	.LASF1034
	.byte	0x2
	.2byte	0x41c
	.4byte	0xef6
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WindGageInstalledAt
	.uleb128 0xa
	.4byte	.LASF1035
	.byte	0x2
	.2byte	0x41d
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WindGageInUse
	.uleb128 0xa
	.4byte	.LASF1036
	.byte	0x2
	.2byte	0x41e
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WindGagePauseSpeed
	.uleb128 0xa
	.4byte	.LASF1037
	.byte	0x2
	.2byte	0x41f
	.4byte	0xb7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GuiVar_WindGageResumeSpeed
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF166:
	.ascii	"GuiVar_CommTestSuppressConnections\000"
.LASF680:
	.ascii	"GuiVar_POCReedSwitchType1\000"
.LASF681:
	.ascii	"GuiVar_POCReedSwitchType2\000"
.LASF148:
	.ascii	"GuiVar_ComboBoxItemIndex\000"
.LASF231:
	.ascii	"GuiVar_FLNumControllersInChain\000"
.LASF407:
	.ascii	"GuiVar_itmMenuItemDesc\000"
.LASF940:
	.ascii	"GuiVar_TwoWireDiscoveredPOCDecoders\000"
.LASF739:
	.ascii	"GuiVar_ScheduleMowDay\000"
.LASF866:
	.ascii	"GuiVar_StatusElectricalErrors\000"
.LASF897:
	.ascii	"GuiVar_StatusTypeOfSchedule\000"
.LASF658:
	.ascii	"GuiVar_POCPreservesAccumMS1\000"
.LASF590:
	.ascii	"GuiVar_NetworkAvailable\000"
.LASF660:
	.ascii	"GuiVar_POCPreservesAccumMS3\000"
.LASF557:
	.ascii	"GuiVar_MoistureTabToDisplay\000"
.LASF986:
	.ascii	"GuiVar_TwoWireStatsTxAcksSent\000"
.LASF426:
	.ascii	"GuiVar_LightCalendarWeek2Sun\000"
.LASF29:
	.ascii	"GuiVar_AboutTPDashWTerminal\000"
.LASF479:
	.ascii	"GuiVar_LRMode\000"
.LASF1003:
	.ascii	"GuiVar_WENChangePassphrase\000"
.LASF865:
	.ascii	"GuiVar_StatusCycleLeft\000"
.LASF745:
	.ascii	"GuiVar_ScheduleStopTimeEnabled\000"
.LASF507:
	.ascii	"GuiVar_ManualPRunTimeSingle\000"
.LASF859:
	.ascii	"GuiVar_StationSelectionGridMarkerSize\000"
.LASF1012:
	.ascii	"GuiVar_WENSSID\000"
.LASF468:
	.ascii	"GuiVar_LiveScreensElectricalControllerName\000"
.LASF677:
	.ascii	"GuiVar_POCPreservesFileType\000"
.LASF538:
	.ascii	"GuiVar_MoisControlMode\000"
.LASF34:
	.ascii	"GuiVar_AboutTPSta01_08Card\000"
.LASF412:
	.ascii	"GuiVar_KeyboardShiftEnabled\000"
.LASF664:
	.ascii	"GuiVar_POCPreservesBoxIndex\000"
.LASF947:
	.ascii	"GuiVar_TwoWireOrphanedPOCsExist\000"
.LASF165:
	.ascii	"GuiVar_CommTestStatus\000"
.LASF196:
	.ascii	"GuiVar_ENIPAddress_1\000"
.LASF96:
	.ascii	"GuiVar_BudgetUnitGallon\000"
.LASF198:
	.ascii	"GuiVar_ENIPAddress_3\000"
.LASF199:
	.ascii	"GuiVar_ENIPAddress_4\000"
.LASF693:
	.ascii	"GuiVar_RadioTestNoResp\000"
.LASF158:
	.ascii	"GuiVar_CommOptionRadioTestAvailable\000"
.LASF433:
	.ascii	"GuiVar_LightIsPhysicallyAvailable\000"
.LASF436:
	.ascii	"GuiVar_LightsBoxIndex_0\000"
.LASF737:
	.ascii	"GuiVar_ScheduleBeginDateStr\000"
.LASF275:
	.ascii	"GuiVar_FlowTable3Sta\000"
.LASF188:
	.ascii	"GuiVar_DisplayType\000"
.LASF16:
	.ascii	"GuiVar_AboutModelNumber\000"
.LASF963:
	.ascii	"GuiVar_TwoWireStatsEEPCRCErrStats\000"
.LASF735:
	.ascii	"GuiVar_RptWalkThruMin\000"
.LASF66:
	.ascii	"GuiVar_BudgetEst\000"
.LASF805:
	.ascii	"GuiVar_StationGroupHeadType\000"
.LASF177:
	.ascii	"GuiVar_DateTimeFullStr\000"
.LASF846:
	.ascii	"GuiVar_StationInfoNumber\000"
.LASF98:
	.ascii	"GuiVar_BudgetUsed\000"
.LASF233:
	.ascii	"GuiVar_FlowCheckingAllowedToLock\000"
.LASF798:
	.ascii	"GuiVar_StationDecoderSerialNumber\000"
.LASF442:
	.ascii	"GuiVar_LightsDate\000"
.LASF277:
	.ascii	"GuiVar_FlowTable5Sta\000"
.LASF186:
	.ascii	"GuiVar_DeviceExchangeProgressBar\000"
.LASF729:
	.ascii	"GuiVar_RptScheduledMin\000"
.LASF211:
	.ascii	"GuiVar_ETGageSkipTonight\000"
.LASF784:
	.ascii	"GuiVar_SRRepeater\000"
.LASF592:
	.ascii	"GuiVar_NoWaterDaysProg\000"
.LASF571:
	.ascii	"GuiVar_MVORClose6Enabled\000"
.LASF962:
	.ascii	"GuiVar_TwoWireStatsEEPCRCComParams\000"
.LASF279:
	.ascii	"GuiVar_FlowTable7Sta\000"
.LASF253:
	.ascii	"GuiVar_FlowCheckingIterations\000"
.LASF531:
	.ascii	"GuiVar_ManualPWillNotRunReason\000"
.LASF512:
	.ascii	"GuiVar_ManualPStartTime2Enabled\000"
.LASF751:
	.ascii	"GuiVar_ScheduleWaterDay_Mon\000"
.LASF928:
	.ascii	"GuiVar_TestStationOnOff\000"
.LASF1005:
	.ascii	"GuiVar_WENKey\000"
.LASF351:
	.ascii	"GuiVar_GRSIMState\000"
.LASF281:
	.ascii	"GuiVar_FlowTable9Sta\000"
.LASF806:
	.ascii	"GuiVar_StationGroupInUse\000"
.LASF168:
	.ascii	"GuiVar_ContactStatusFailures\000"
.LASF697:
	.ascii	"GuiVar_RadioTestRoundTripMaxMS\000"
.LASF202:
	.ascii	"GuiVar_ENNetmask\000"
.LASF790:
	.ascii	"GuiVar_SRSubnetXmtMaster\000"
.LASF23:
	.ascii	"GuiVar_AboutTP2WireTerminal\000"
.LASF368:
	.ascii	"GuiVar_HistoricalETUser10\000"
.LASF369:
	.ascii	"GuiVar_HistoricalETUser11\000"
.LASF370:
	.ascii	"GuiVar_HistoricalETUser12\000"
.LASF636:
	.ascii	"GuiVar_POCBypassStages\000"
.LASF28:
	.ascii	"GuiVar_AboutTPDashWCard\000"
.LASF718:
	.ascii	"GuiVar_RptIrrigMin\000"
.LASF978:
	.ascii	"GuiVar_TwoWireStatsRxSolCtrlMsgs\000"
.LASF821:
	.ascii	"GuiVar_StationGroupMoistureSensorDecoderSN\000"
.LASF709:
	.ascii	"GuiVar_RptController\000"
.LASF146:
	.ascii	"GuiVar_ComboBox_X1\000"
.LASF268:
	.ascii	"GuiVar_FlowCheckingStopIrriTime\000"
.LASF671:
	.ascii	"GuiVar_POCPreservesDeliveredMVCurrent1\000"
.LASF672:
	.ascii	"GuiVar_POCPreservesDeliveredMVCurrent2\000"
.LASF673:
	.ascii	"GuiVar_POCPreservesDeliveredMVCurrent3\000"
.LASF942:
	.ascii	"GuiVar_TwoWireDiscoveryProgressBar\000"
.LASF118:
	.ascii	"GuiVar_ChainStatsFOALChainTGen\000"
.LASF505:
	.ascii	"GuiVar_ManualPInUse\000"
.LASF961:
	.ascii	"GuiVar_TwoWireStatsBODs\000"
.LASF679:
	.ascii	"GuiVar_POCPreservesIndex\000"
.LASF121:
	.ascii	"GuiVar_ChainStatsInForced\000"
.LASF350:
	.ascii	"GuiVar_GRSIMID\000"
.LASF417:
	.ascii	"GuiVar_LightCalendarWeek1Mon\000"
.LASF561:
	.ascii	"GuiVar_MVORClose1Enabled\000"
.LASF765:
	.ascii	"GuiVar_SerialPortRcvdA\000"
.LASF33:
	.ascii	"GuiVar_AboutTPPOCTerminal\000"
.LASF857:
	.ascii	"GuiVar_StationSelectionGridGroupName\000"
.LASF864:
	.ascii	"GuiVar_StatusCurrentDraw\000"
.LASF909:
	.ascii	"GuiVar_SystemFlowCheckingToleranceMinus2\000"
.LASF738:
	.ascii	"GuiVar_ScheduleIrrigateOn29thOr31st\000"
.LASF782:
	.ascii	"GuiVar_SRMode\000"
.LASF725:
	.ascii	"GuiVar_RptNonCMin\000"
.LASF103:
	.ascii	"GuiVar_ChainStatsCommMngrState\000"
.LASF213:
	.ascii	"GuiVar_ETRainTableDate\000"
.LASF785:
	.ascii	"GuiVar_SRRepeaterLinkedToItself\000"
.LASF625:
	.ascii	"GuiVar_POCBudget2\000"
.LASF754:
	.ascii	"GuiVar_ScheduleWaterDay_Thu\000"
.LASF626:
	.ascii	"GuiVar_POCBudget3\000"
.LASF627:
	.ascii	"GuiVar_POCBudget4\000"
.LASF756:
	.ascii	"GuiVar_ScheduleWaterDay_Wed\000"
.LASF696:
	.ascii	"GuiVar_RadioTestRoundTripAvgMS\000"
.LASF1019:
	.ascii	"GuiVar_WENWEPTXKey\000"
.LASF218:
	.ascii	"GuiVar_ETRainTableReport\000"
.LASF230:
	.ascii	"GuiVar_FLControllerIndex_0\000"
.LASF352:
	.ascii	"GuiVar_HistoricalET1\000"
.LASF356:
	.ascii	"GuiVar_HistoricalET2\000"
.LASF357:
	.ascii	"GuiVar_HistoricalET3\000"
.LASF358:
	.ascii	"GuiVar_HistoricalET4\000"
.LASF359:
	.ascii	"GuiVar_HistoricalET5\000"
.LASF212:
	.ascii	"GuiVar_ETRainTableCurrentET\000"
.LASF361:
	.ascii	"GuiVar_HistoricalET7\000"
.LASF362:
	.ascii	"GuiVar_HistoricalET8\000"
.LASF363:
	.ascii	"GuiVar_HistoricalET9\000"
.LASF884:
	.ascii	"GuiVar_StatusRainBucketReading\000"
.LASF57:
	.ascii	"GuiVar_AlertTime\000"
.LASF388:
	.ascii	"GuiVar_IrriCommChainConfigCurrentSendFromIrri\000"
.LASF45:
	.ascii	"GuiVar_AboutTPSta41_48Terminal\000"
.LASF1018:
	.ascii	"GuiVar_WENWEPShowTXKeyIndex\000"
.LASF266:
	.ascii	"GuiVar_FlowCheckingSlotSize\000"
.LASF12:
	.ascii	"GuiVar_AboutFLOption\000"
.LASF930:
	.ascii	"GuiVar_TestStationTimeDisplay\000"
.LASF222:
	.ascii	"GuiVar_FinishTimesCalcPercentComplete\000"
.LASF766:
	.ascii	"GuiVar_SerialPortRcvdB\000"
.LASF873:
	.ascii	"GuiVar_StatusFuseBlown\000"
.LASF10:
	.ascii	"GuiVar_AboutAquaponicsOption\000"
.LASF902:
	.ascii	"GuiVar_StopKeyPending\000"
.LASF491:
	.ascii	"GuiVar_MainMenuCommOptionExists\000"
.LASF896:
	.ascii	"GuiVar_StatusSystemFlowValvesOn\000"
.LASF392:
	.ascii	"GuiVar_IrriDetailsController\000"
.LASF1026:
	.ascii	"GuiVar_WENWPAEncryptionCCMP\000"
.LASF966:
	.ascii	"GuiVar_TwoWireStatsPORs\000"
.LASF862:
	.ascii	"GuiVar_StationSelectionGridStationCount\000"
.LASF336:
	.ascii	"GuiVar_GroupSettingC_0\000"
.LASF237:
	.ascii	"GuiVar_FlowCheckingExpOn\000"
.LASF338:
	.ascii	"GuiVar_GroupSettingC_2\000"
.LASF339:
	.ascii	"GuiVar_GroupSettingC_3\000"
.LASF340:
	.ascii	"GuiVar_GroupSettingC_4\000"
.LASF141:
	.ascii	"GuiVar_ChainStatsScans\000"
.LASF342:
	.ascii	"GuiVar_GroupSettingC_6\000"
.LASF343:
	.ascii	"GuiVar_GroupSettingC_7\000"
.LASF344:
	.ascii	"GuiVar_GroupSettingC_8\000"
.LASF345:
	.ascii	"GuiVar_GroupSettingC_9\000"
.LASF916:
	.ascii	"GuiVar_SystemInUse\000"
.LASF867:
	.ascii	"GuiVar_StatusETGageConnected\000"
.LASF416:
	.ascii	"GuiVar_LightCalendarWeek1Fri\000"
.LASF812:
	.ascii	"GuiVar_StationGroupKc3\000"
.LASF701:
	.ascii	"GuiVar_RainBucketInstalledAt\000"
.LASF394:
	.ascii	"GuiVar_IrriDetailsProgCycle\000"
.LASF836:
	.ascii	"GuiVar_StationInfoDU\000"
.LASF815:
	.ascii	"GuiVar_StationGroupKc6\000"
.LASF898:
	.ascii	"GuiVar_StatusWindGageConnected\000"
.LASF841:
	.ascii	"GuiVar_StationInfoGroupHasStartTime\000"
.LASF83:
	.ascii	"GuiVar_BudgetReductionLimit_0\000"
.LASF84:
	.ascii	"GuiVar_BudgetReductionLimit_1\000"
.LASF85:
	.ascii	"GuiVar_BudgetReductionLimit_2\000"
.LASF86:
	.ascii	"GuiVar_BudgetReductionLimit_3\000"
.LASF87:
	.ascii	"GuiVar_BudgetReductionLimit_4\000"
.LASF88:
	.ascii	"GuiVar_BudgetReductionLimit_5\000"
.LASF89:
	.ascii	"GuiVar_BudgetReductionLimit_6\000"
.LASF90:
	.ascii	"GuiVar_BudgetReductionLimit_7\000"
.LASF91:
	.ascii	"GuiVar_BudgetReductionLimit_8\000"
.LASF92:
	.ascii	"GuiVar_BudgetReductionLimit_9\000"
.LASF723:
	.ascii	"GuiVar_RptManualPMin\000"
.LASF1015:
	.ascii	"GuiVar_WENWEPKeySize\000"
.LASF977:
	.ascii	"GuiVar_TwoWireStatsRxPutParamsMsgs\000"
.LASF46:
	.ascii	"GuiVar_AboutVersion\000"
.LASF980:
	.ascii	"GuiVar_TwoWireStatsRxStatReqMsgs\000"
.LASF988:
	.ascii	"GuiVar_UnusedGroupsRemoved\000"
.LASF714:
	.ascii	"GuiVar_RptFlags\000"
.LASF521:
	.ascii	"GuiVar_ManualPStationRunTime\000"
.LASF1027:
	.ascii	"GuiVar_WENWPAEncryptionTKIP\000"
.LASF102:
	.ascii	"GuiVar_ChainStatsCommMngrMode\000"
.LASF575:
	.ascii	"GuiVar_MVOROpen1\000"
.LASF740:
	.ascii	"GuiVar_ScheduleNoStationsInGroup\000"
.LASF463:
	.ascii	"GuiVar_LiveScreensCommCentralEnabled\000"
.LASF662:
	.ascii	"GuiVar_POCPreservesAccumPulses2\000"
.LASF72:
	.ascii	"GuiVar_BudgetFlowTypeNonController\000"
.LASF583:
	.ascii	"GuiVar_MVOROpen5\000"
.LASF364:
	.ascii	"GuiVar_HistoricalETCity\000"
.LASF710:
	.ascii	"GuiVar_RptCycles\000"
.LASF365:
	.ascii	"GuiVar_HistoricalETCounty\000"
.LASF759:
	.ascii	"GuiVar_SerialPortCDA\000"
.LASF563:
	.ascii	"GuiVar_MVORClose2Enabled\000"
.LASF602:
	.ascii	"GuiVar_PercentAdjustEndDate_5\000"
.LASF793:
	.ascii	"GuiVar_SRTransmitPower\000"
.LASF809:
	.ascii	"GuiVar_StationGroupKc11\000"
.LASF810:
	.ascii	"GuiVar_StationGroupKc12\000"
.LASF160:
	.ascii	"GuiVar_CommTestIPOctect2\000"
.LASF208:
	.ascii	"GuiVar_ETGageMaxPercent\000"
.LASF161:
	.ascii	"GuiVar_CommTestIPOctect3\000"
.LASF97:
	.ascii	"GuiVar_BudgetUnitHCF\000"
.LASF589:
	.ascii	"GuiVar_MVORTimeRemaining\000"
.LASF787:
	.ascii	"GuiVar_SRSubnetRcvMaster\000"
.LASF861:
	.ascii	"GuiVar_StationSelectionGridShowOnlyStationsInThisGr"
	.ascii	"oup\000"
.LASF885:
	.ascii	"GuiVar_StatusRainSwitchConnected\000"
.LASF227:
	.ascii	"GuiVar_FinishTimesStartTime\000"
.LASF661:
	.ascii	"GuiVar_POCPreservesAccumPulses1\000"
.LASF868:
	.ascii	"GuiVar_StatusETGageControllerName\000"
.LASF210:
	.ascii	"GuiVar_ETGageRunawayGage\000"
.LASF428:
	.ascii	"GuiVar_LightCalendarWeek2Tue\000"
.LASF999:
	.ascii	"GuiVar_WalkThruStation_Str\000"
.LASF851:
	.ascii	"GuiVar_StationInfoShowPercentOfET\000"
.LASF832:
	.ascii	"GuiVar_StationInfoBoxIndex\000"
.LASF736:
	.ascii	"GuiVar_ScheduleBeginDate\000"
.LASF50:
	.ascii	"GuiVar_AccessControlLoggedIn\000"
.LASF189:
	.ascii	"GuiVar_ENDHCPName\000"
.LASF704:
	.ascii	"GuiVar_RainBucketMaximumHourly\000"
.LASF267:
	.ascii	"GuiVar_FlowCheckingStable\000"
.LASF596:
	.ascii	"GuiVar_OnAtATimeInfoDisplay\000"
.LASF1017:
	.ascii	"GuiVar_WENWEPKeyType\000"
.LASF170:
	.ascii	"GuiVar_ContactStatusPort\000"
.LASF724:
	.ascii	"GuiVar_RptNonCGal\000"
.LASF869:
	.ascii	"GuiVar_StatusETGageReading\000"
.LASF594:
	.ascii	"GuiVar_NumericKeypadShowPlusMinus\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF539:
	.ascii	"GuiVar_MoisDecoderSN\000"
.LASF79:
	.ascii	"GuiVar_BudgetPeriodEnd\000"
.LASF544:
	.ascii	"GuiVar_MoisECPoint_Low\000"
.LASF614:
	.ascii	"GuiVar_PercentAdjustPercent_5\000"
.LASF840:
	.ascii	"GuiVar_StationInfoFlowStatus\000"
.LASF744:
	.ascii	"GuiVar_ScheduleStopTime\000"
.LASF264:
	.ascii	"GuiVar_FlowCheckingPumpTime\000"
.LASF948:
	.ascii	"GuiVar_TwoWireOrphanedStationsExist\000"
.LASF21:
	.ascii	"GuiVar_AboutSSEDOption\000"
.LASF282:
	.ascii	"GuiVar_FlowTableGPM\000"
.LASF391:
	.ascii	"GuiVar_IrriCommChainConfigSerialNum\000"
.LASF888:
	.ascii	"GuiVar_StatusShowSystemFlow\000"
.LASF555:
	.ascii	"GuiVar_MoisTempPoint_High\000"
.LASF748:
	.ascii	"GuiVar_ScheduleUseETAveraging\000"
.LASF929:
	.ascii	"GuiVar_TestStationStatus\000"
.LASF755:
	.ascii	"GuiVar_ScheduleWaterDay_Tue\000"
.LASF663:
	.ascii	"GuiVar_POCPreservesAccumPulses3\000"
.LASF669:
	.ascii	"GuiVar_POCPreservesDelivered5SecAvg2\000"
.LASF587:
	.ascii	"GuiVar_MVORRunTime\000"
.LASF440:
	.ascii	"GuiVar_LightScheduledStop\000"
.LASF295:
	.ascii	"GuiVar_GroupName\000"
.LASF197:
	.ascii	"GuiVar_ENIPAddress_2\000"
.LASF700:
	.ascii	"GuiVar_RadioTestSuccessfulPercent\000"
.LASF455:
	.ascii	"GuiVar_LightStartReason\000"
.LASF482:
	.ascii	"GuiVar_LRRepeaterInUse\000"
.LASF217:
	.ascii	"GuiVar_ETRainTableRainStatus\000"
.LASF742:
	.ascii	"GuiVar_ScheduleStartTimeEnabled\000"
.LASF825:
	.ascii	"GuiVar_StationGroupSlopePercentage\000"
.LASF203:
	.ascii	"GuiVar_ENObtainIPAutomatically\000"
.LASF574:
	.ascii	"GuiVar_MVOROpen0Enabled\000"
.LASF1006:
	.ascii	"GuiVar_WENMACAddress\000"
.LASF1004:
	.ascii	"GuiVar_WENChangePassword\000"
.LASF882:
	.ascii	"GuiVar_StatusRainBucketConnected\000"
.LASF931:
	.ascii	"GuiVar_TestStationTimeRemaining\000"
.LASF137:
	.ascii	"GuiVar_ChainStatsIRRIIrriTRcvd\000"
.LASF593:
	.ascii	"GuiVar_NumericKeypadShowDecimalPoint\000"
.LASF730:
	.ascii	"GuiVar_RptStartTime\000"
.LASF73:
	.ascii	"GuiVar_BudgetForThisPeriod\000"
.LASF381:
	.ascii	"GuiVar_HistoricalETUserState\000"
.LASF528:
	.ascii	"GuiVar_ManualPWaterDay_Thu\000"
.LASF15:
	.ascii	"char\000"
.LASF297:
	.ascii	"GuiVar_GroupName_1\000"
.LASF298:
	.ascii	"GuiVar_GroupName_2\000"
.LASF254:
	.ascii	"GuiVar_FlowCheckingLFTime\000"
.LASF300:
	.ascii	"GuiVar_GroupName_4\000"
.LASF301:
	.ascii	"GuiVar_GroupName_5\000"
.LASF302:
	.ascii	"GuiVar_GroupName_6\000"
.LASF285:
	.ascii	"GuiVar_FreezeSwitchInUse\000"
.LASF304:
	.ascii	"GuiVar_GroupName_8\000"
.LASF305:
	.ascii	"GuiVar_GroupName_9\000"
.LASF799:
	.ascii	"GuiVar_StationDescription\000"
.LASF968:
	.ascii	"GuiVar_TwoWireStatsRxDataLpbkMsgs\000"
.LASF1010:
	.ascii	"GuiVar_WENRSSI\000"
.LASF245:
	.ascii	"GuiVar_FlowCheckingFMs\000"
.LASF206:
	.ascii	"GuiVar_ETGageInUse\000"
.LASF142:
	.ascii	"GuiVar_CodeDownloadPercentComplete\000"
.LASF22:
	.ascii	"GuiVar_AboutSSEOption\000"
.LASF1002:
	.ascii	"GuiVar_WENChangeKey\000"
.LASF474:
	.ascii	"GuiVar_LRFirmwareVer\000"
.LASF420:
	.ascii	"GuiVar_LightCalendarWeek1Thu\000"
.LASF525:
	.ascii	"GuiVar_ManualPWaterDay_Mon\000"
.LASF429:
	.ascii	"GuiVar_LightCalendarWeek2Wed\000"
.LASF406:
	.ascii	"GuiVar_itmMenuItem\000"
.LASF872:
	.ascii	"GuiVar_StatusFreezeSwitchState\000"
.LASF53:
	.ascii	"GuiVar_AccessControlUserRole\000"
.LASF423:
	.ascii	"GuiVar_LightCalendarWeek2Fri\000"
.LASF960:
	.ascii	"GuiVar_TwoWireStaStrB\000"
.LASF819:
	.ascii	"GuiVar_StationGroupKcsAreCustom\000"
.LASF883:
	.ascii	"GuiVar_StatusRainBucketControllerName\000"
.LASF518:
	.ascii	"GuiVar_ManualPStartTime5Enabled\000"
.LASF995:
	.ascii	"GuiVar_WalkThruDelay\000"
.LASF39:
	.ascii	"GuiVar_AboutTPSta17_24Terminal\000"
.LASF511:
	.ascii	"GuiVar_ManualPStartTime2\000"
.LASF534:
	.ascii	"GuiVar_ManualWaterRunTime\000"
.LASF580:
	.ascii	"GuiVar_MVOROpen3Enabled\000"
.LASF517:
	.ascii	"GuiVar_ManualPStartTime5\000"
.LASF489:
	.ascii	"GuiVar_MainMenuBudgetsInUse\000"
.LASF843:
	.ascii	"GuiVar_StationInfoMoistureBalance\000"
.LASF226:
	.ascii	"GuiVar_FinishTimesScrollBoxH\000"
.LASF633:
	.ascii	"GuiVar_POCBudgetOption\000"
.LASF508:
	.ascii	"GuiVar_ManualPStartDateStr\000"
.LASF638:
	.ascii	"GuiVar_POCDecoderSN2\000"
.LASF826:
	.ascii	"GuiVar_StationGroupSoilIntakeRate\000"
.LASF174:
	.ascii	"GuiVar_DateTimeAMPM\000"
.LASF42:
	.ascii	"GuiVar_AboutTPSta33_40Card\000"
.LASF645:
	.ascii	"GuiVar_POCFlowMeterOffset3\000"
.LASF721:
	.ascii	"GuiVar_RptManualMin\000"
.LASF991:
	.ascii	"GuiVar_VolumeSliderPos\000"
.LASF265:
	.ascii	"GuiVar_FlowCheckingSlots\000"
.LASF163:
	.ascii	"GuiVar_CommTestIPPort\000"
.LASF152:
	.ascii	"GuiVar_CommOptionInfoText\000"
.LASF371:
	.ascii	"GuiVar_HistoricalETUser2\000"
.LASF372:
	.ascii	"GuiVar_HistoricalETUser3\000"
.LASF373:
	.ascii	"GuiVar_HistoricalETUser4\000"
.LASF374:
	.ascii	"GuiVar_HistoricalETUser5\000"
.LASF375:
	.ascii	"GuiVar_HistoricalETUser6\000"
.LASF376:
	.ascii	"GuiVar_HistoricalETUser7\000"
.LASF377:
	.ascii	"GuiVar_HistoricalETUser8\000"
.LASF378:
	.ascii	"GuiVar_HistoricalETUser9\000"
.LASF397:
	.ascii	"GuiVar_IrriDetailsRunCycle\000"
.LASF458:
	.ascii	"GuiVar_LiveScreensBypassFlowLevel\000"
.LASF1000:
	.ascii	"GuiVar_WalkThruStationDesc\000"
.LASF64:
	.ascii	"GuiVar_BudgetDaysLeftInPeriod\000"
.LASF101:
	.ascii	"GuiVar_BudgetZeroWarning\000"
.LASF903:
	.ascii	"GuiVar_StopKeyReasonInList\000"
.LASF984:
	.ascii	"GuiVar_TwoWireStatsTempCur\000"
.LASF399:
	.ascii	"GuiVar_IrriDetailsShowFOAL\000"
.LASF405:
	.ascii	"GuiVar_itmGroupName\000"
.LASF856:
	.ascii	"GuiVar_StationSelectionGridControllerName\000"
.LASF910:
	.ascii	"GuiVar_SystemFlowCheckingToleranceMinus3\000"
.LASF705:
	.ascii	"GuiVar_RainBucketMinimum\000"
.LASF1016:
	.ascii	"GuiVar_WENWEPKeySizeChanged\000"
.LASF860:
	.ascii	"GuiVar_StationSelectionGridScreenTitle\000"
.LASF728:
	.ascii	"GuiVar_RptRReMin\000"
.LASF938:
	.ascii	"GuiVar_TwoWireDescB\000"
.LASF1037:
	.ascii	"GuiVar_WindGageResumeSpeed\000"
.LASF547:
	.ascii	"GuiVar_MoisReading_Current\000"
.LASF807:
	.ascii	"GuiVar_StationGroupKc1\000"
.LASF811:
	.ascii	"GuiVar_StationGroupKc2\000"
.LASF209:
	.ascii	"GuiVar_ETGagePercentFull\000"
.LASF813:
	.ascii	"GuiVar_StationGroupKc4\000"
.LASF814:
	.ascii	"GuiVar_StationGroupKc5\000"
.LASF434:
	.ascii	"GuiVar_LightRemainingMinutes\000"
.LASF816:
	.ascii	"GuiVar_StationGroupKc7\000"
.LASF817:
	.ascii	"GuiVar_StationGroupKc8\000"
.LASF818:
	.ascii	"GuiVar_StationGroupKc9\000"
.LASF912:
	.ascii	"GuiVar_SystemFlowCheckingTolerancePlus1\000"
.LASF607:
	.ascii	"GuiVar_PercentAdjustNumberOfDays\000"
.LASF499:
	.ascii	"GuiVar_MainMenuMoisSensorExists\000"
.LASF915:
	.ascii	"GuiVar_SystemFlowCheckingTolerancePlus4\000"
.LASF776:
	.ascii	"GuiVar_SerialPortXmittingB\000"
.LASF588:
	.ascii	"GuiVar_MVORState\000"
.LASF572:
	.ascii	"GuiVar_MVORInEffect\000"
.LASF380:
	.ascii	"GuiVar_HistoricalETUserCounty\000"
.LASF393:
	.ascii	"GuiVar_IrriDetailsCurrentDraw\000"
.LASF560:
	.ascii	"GuiVar_MVORClose1\000"
.LASF562:
	.ascii	"GuiVar_MVORClose2\000"
.LASF564:
	.ascii	"GuiVar_MVORClose3\000"
.LASF566:
	.ascii	"GuiVar_MVORClose4\000"
.LASF568:
	.ascii	"GuiVar_MVORClose5\000"
.LASF570:
	.ascii	"GuiVar_MVORClose6\000"
.LASF998:
	.ascii	"GuiVar_WalkThruShowStartInSeconds\000"
.LASF425:
	.ascii	"GuiVar_LightCalendarWeek2Sat\000"
.LASF788:
	.ascii	"GuiVar_SRSubnetRcvRepeater\000"
.LASF456:
	.ascii	"GuiVar_LiveScreensBypassFlowAvgFlow\000"
.LASF1022:
	.ascii	"GuiVar_WENWPAEAPTLSCredentials\000"
.LASF1031:
	.ascii	"GuiVar_WENWPAPassword\000"
.LASF1036:
	.ascii	"GuiVar_WindGagePauseSpeed\000"
.LASF927:
	.ascii	"GuiVar_TestStationAutoOnTime\000"
.LASF402:
	.ascii	"GuiVar_IsAHub\000"
.LASF37:
	.ascii	"GuiVar_AboutTPSta09_16Terminal\000"
.LASF36:
	.ascii	"GuiVar_AboutTPSta09_16Card\000"
.LASF610:
	.ascii	"GuiVar_PercentAdjustPercent_1\000"
.LASF313:
	.ascii	"GuiVar_GroupSetting_Visible_7\000"
.LASF612:
	.ascii	"GuiVar_PercentAdjustPercent_3\000"
.LASF613:
	.ascii	"GuiVar_PercentAdjustPercent_4\000"
.LASF981:
	.ascii	"GuiVar_TwoWireStatsRxStatsReqMsgs\000"
.LASF532:
	.ascii	"GuiVar_ManualPWillRun\000"
.LASF946:
	.ascii	"GuiVar_TwoWireNumDiscoveredStaDecoders\000"
.LASF615:
	.ascii	"GuiVar_PercentAdjustPercent_6\000"
.LASF290:
	.ascii	"GuiVar_GRGPRSStatus\000"
.LASF616:
	.ascii	"GuiVar_PercentAdjustPercent_7\000"
.LASF900:
	.ascii	"GuiVar_StatusWindGageReading\000"
.LASF617:
	.ascii	"GuiVar_PercentAdjustPercent_8\000"
.LASF432:
	.ascii	"GuiVar_LightIsEnergized\000"
.LASF618:
	.ascii	"GuiVar_PercentAdjustPercent_9\000"
.LASF727:
	.ascii	"GuiVar_RptRReGal\000"
.LASF926:
	.ascii	"GuiVar_TestStationAutoOffTime\000"
.LASF475:
	.ascii	"GuiVar_LRFrequency_Decimal\000"
.LASF533:
	.ascii	"GuiVar_ManualWaterProg\000"
.LASF708:
	.ascii	"GuiVar_RainSwitchSelected\000"
.LASF891:
	.ascii	"GuiVar_StatusSystemFlowMLBAllowed\000"
.LASF150:
	.ascii	"GuiVar_CommOptionCloudCommTestAvailable\000"
.LASF655:
	.ascii	"GuiVar_POCPreservesAccumGal1\000"
.LASF656:
	.ascii	"GuiVar_POCPreservesAccumGal2\000"
.LASF657:
	.ascii	"GuiVar_POCPreservesAccumGal3\000"
.LASF586:
	.ascii	"GuiVar_MVOROpen6Enabled\000"
.LASF949:
	.ascii	"GuiVar_TwoWireOutputOnA\000"
.LASF692:
	.ascii	"GuiVar_RadioTestInProgress\000"
.LASF535:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF870:
	.ascii	"GuiVar_StatusFlowErrors\000"
.LASF81:
	.ascii	"GuiVar_BudgetPeriodsPerYearIdx\000"
.LASF259:
	.ascii	"GuiVar_FlowCheckingMVTime\000"
.LASF749:
	.ascii	"GuiVar_ScheduleUsesET\000"
.LASF451:
	.ascii	"GuiVar_LightsStopTime2InMinutes\000"
.LASF1008:
	.ascii	"GuiVar_WENRadioMode\000"
.LASF296:
	.ascii	"GuiVar_GroupName_0\000"
.LASF829:
	.ascii	"GuiVar_StationGroupSpeciesFactor\000"
.LASF261:
	.ascii	"GuiVar_FlowCheckingOnForProbList\000"
.LASF901:
	.ascii	"GuiVar_StatusWindPaused\000"
.LASF299:
	.ascii	"GuiVar_GroupName_3\000"
.LASF600:
	.ascii	"GuiVar_PercentAdjustEndDate_3\000"
.LASF1:
	.ascii	"long unsigned int\000"
.LASF219:
	.ascii	"GuiVar_ETRainTableShutdown\000"
.LASF303:
	.ascii	"GuiVar_GroupName_7\000"
.LASF390:
	.ascii	"GuiVar_IrriCommChainConfigNameAbbrv\000"
.LASF649:
	.ascii	"GuiVar_POCFMType1IsBermad\000"
.LASF850:
	.ascii	"GuiVar_StationInfoShowPercentAdjust\000"
.LASF1030:
	.ascii	"GuiVar_WENWPAKeyType\000"
.LASF976:
	.ascii	"GuiVar_TwoWireStatsRxMsgs\000"
.LASF366:
	.ascii	"GuiVar_HistoricalETState\000"
.LASF395:
	.ascii	"GuiVar_IrriDetailsProgLeft\000"
.LASF863:
	.ascii	"GuiVar_StatusChainDown\000"
.LASF506:
	.ascii	"GuiVar_ManualPRunTime\000"
.LASF935:
	.ascii	"GuiVar_TwoWireDecoderSNToAssign\000"
.LASF569:
	.ascii	"GuiVar_MVORClose5Enabled\000"
.LASF920:
	.ascii	"GuiVar_SystemPreserves5SecNext\000"
.LASF510:
	.ascii	"GuiVar_ManualPStartTime1Enabled\000"
.LASF176:
	.ascii	"GuiVar_DateTimeDayOfWeek\000"
.LASF94:
	.ascii	"GuiVar_BudgetsInEffect\000"
.LASF791:
	.ascii	"GuiVar_SRSubnetXmtRepeater\000"
.LASF159:
	.ascii	"GuiVar_CommTestIPOctect1\000"
.LASF40:
	.ascii	"GuiVar_AboutTPSta25_32Card\000"
.LASF157:
	.ascii	"GuiVar_CommOptionPortBInstalled\000"
.LASF162:
	.ascii	"GuiVar_CommTestIPOctect4\000"
.LASF411:
	.ascii	"GuiVar_itmSubNode\000"
.LASF591:
	.ascii	"GuiVar_NoWaterDaysDays\000"
.LASF772:
	.ascii	"GuiVar_SerialPortXmitB\000"
.LASF546:
	.ascii	"GuiVar_MoisPhysicallyAvailable\000"
.LASF789:
	.ascii	"GuiVar_SRSubnetRcvSlave\000"
.LASF151:
	.ascii	"GuiVar_CommOptionDeviceExchangeResult\000"
.LASF685:
	.ascii	"GuiVar_RadioTestBadCRC\000"
.LASF971:
	.ascii	"GuiVar_TwoWireStatsRxDiscRstMsgs\000"
.LASF459:
	.ascii	"GuiVar_LiveScreensBypassFlowMaxFlow\000"
.LASF879:
	.ascii	"GuiVar_StatusNextScheduledIrriType\000"
.LASF954:
	.ascii	"GuiVar_TwoWireStaA\000"
.LASF777:
	.ascii	"GuiVar_SerialPortXmitTP\000"
.LASF503:
	.ascii	"GuiVar_MainMenuWeatherOptionsExist\000"
.LASF5:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF549:
	.ascii	"GuiVar_MoisSensorName\000"
.LASF758:
	.ascii	"GuiVar_ScrollBoxHorizScrollPos\000"
.LASF854:
	.ascii	"GuiVar_StationInfoStationGroup\000"
.LASF20:
	.ascii	"GuiVar_AboutSerialNumber\000"
.LASF135:
	.ascii	"GuiVar_ChainStatsIRRIChainTRcvd\000"
.LASF684:
	.ascii	"GuiVar_POCUsage\000"
.LASF82:
	.ascii	"GuiVar_BudgetPeriodStart\000"
.LASF905:
	.ascii	"GuiVar_SystemFlowCheckingRange1\000"
.LASF753:
	.ascii	"GuiVar_ScheduleWaterDay_Sun\000"
.LASF907:
	.ascii	"GuiVar_SystemFlowCheckingRange3\000"
.LASF804:
	.ascii	"GuiVar_StationGroupExposure\000"
.LASF800:
	.ascii	"GuiVar_StationGroupAllowableDepletion\000"
.LASF478:
	.ascii	"GuiVar_LRHubFeatureNotAvailable\000"
.LASF145:
	.ascii	"GuiVar_CodeDownloadUpdatingTPMicro\000"
.LASF384:
	.ascii	"GuiVar_IrriCommChainConfigActive\000"
.LASF548:
	.ascii	"GuiVar_MoisSensorIncludesEC\000"
.LASF224:
	.ascii	"GuiVar_FinishTimesCalcTime\000"
.LASF620:
	.ascii	"GuiVar_POCBoxIndex\000"
.LASF385:
	.ascii	"GuiVar_IrriCommChainConfigCurrentFromFOAL\000"
.LASF803:
	.ascii	"GuiVar_StationGroupDensityFactor\000"
.LASF200:
	.ascii	"GuiVar_ENMACAddress\000"
.LASF288:
	.ascii	"GuiVar_GRECIO\000"
.LASF382:
	.ascii	"GuiVar_HistoricalETUseYourOwn\000"
.LASF527:
	.ascii	"GuiVar_ManualPWaterDay_Sun\000"
.LASF236:
	.ascii	"GuiVar_FlowCheckingExp\000"
.LASF204:
	.ascii	"GuiVar_ENSubnetMask\000"
.LASF623:
	.ascii	"GuiVar_POCBudget11\000"
.LASF71:
	.ascii	"GuiVar_BudgetFlowTypeMVOR\000"
.LASF229:
	.ascii	"GuiVar_FinishTimesSystemGroup\000"
.LASF918:
	.ascii	"GuiVar_SystemPOCSelected\000"
.LASF403:
	.ascii	"GuiVar_IsMaster\000"
.LASF796:
	.ascii	"GuiVar_StationCopyTextOffset_Right\000"
.LASF221:
	.ascii	"GuiVar_FinishTimes\000"
.LASF190:
	.ascii	"GuiVar_ENDHCPNameNotSet\000"
.LASF711:
	.ascii	"GuiVar_RptDate\000"
.LASF773:
	.ascii	"GuiVar_SerialPortXmitLengthA\000"
.LASF774:
	.ascii	"GuiVar_SerialPortXmitLengthB\000"
.LASF973:
	.ascii	"GuiVar_TwoWireStatsRxGetParamsMsgs\000"
.LASF353:
	.ascii	"GuiVar_HistoricalET10\000"
.LASF354:
	.ascii	"GuiVar_HistoricalET11\000"
.LASF355:
	.ascii	"GuiVar_HistoricalET12\000"
.LASF983:
	.ascii	"GuiVar_TwoWireStatsS2UCos\000"
.LASF717:
	.ascii	"double\000"
.LASF144:
	.ascii	"GuiVar_CodeDownloadState\000"
.LASF648:
	.ascii	"GuiVar_POCFlowMeterType3\000"
.LASF258:
	.ascii	"GuiVar_FlowCheckingMVState\000"
.LASF698:
	.ascii	"GuiVar_RadioTestRoundTripMinMS\000"
.LASF250:
	.ascii	"GuiVar_FlowCheckingHasNCMV\000"
.LASF156:
	.ascii	"GuiVar_CommOptionPortBIndex\000"
.LASF100:
	.ascii	"GuiVar_BudgetUseThisPeriod\000"
.LASF913:
	.ascii	"GuiVar_SystemFlowCheckingTolerancePlus2\000"
.LASF481:
	.ascii	"GuiVar_LRRadioType\000"
.LASF914:
	.ascii	"GuiVar_SystemFlowCheckingTolerancePlus3\000"
.LASF51:
	.ascii	"GuiVar_AccessControlPwd\000"
.LASF415:
	.ascii	"GuiVar_KeyboardType\000"
.LASF464:
	.ascii	"GuiVar_LiveScreensCommFLCommMngrMode\000"
.LASF578:
	.ascii	"GuiVar_MVOROpen2Enabled\000"
.LASF792:
	.ascii	"GuiVar_SRSubnetXmtSlave\000"
.LASF974:
	.ascii	"GuiVar_TwoWireStatsRxIDReqMsgs\000"
.LASF430:
	.ascii	"GuiVar_LightEditableStop\000"
.LASF56:
	.ascii	"GuiVar_AlertString\000"
.LASF573:
	.ascii	"GuiVar_MVOROpen0\000"
.LASF252:
	.ascii	"GuiVar_FlowCheckingINTime\000"
.LASF577:
	.ascii	"GuiVar_MVOROpen2\000"
.LASF579:
	.ascii	"GuiVar_MVOROpen3\000"
.LASF581:
	.ascii	"GuiVar_MVOROpen4\000"
.LASF274:
	.ascii	"GuiVar_FlowTable2Sta\000"
.LASF585:
	.ascii	"GuiVar_MVOROpen6\000"
.LASF678:
	.ascii	"GuiVar_POCPreservesGroupID\000"
.LASF61:
	.ascii	"float\000"
.LASF1021:
	.ascii	"GuiVar_WENWPAAuthentication\000"
.LASF712:
	.ascii	"GuiVar_RptDecoderOutput\000"
.LASF643:
	.ascii	"GuiVar_POCFlowMeterOffset1\000"
.LASF644:
	.ascii	"GuiVar_POCFlowMeterOffset2\000"
.LASF387:
	.ascii	"GuiVar_IrriCommChainConfigCurrentFromTP\000"
.LASF653:
	.ascii	"GuiVar_POCMVType1\000"
.LASF69:
	.ascii	"GuiVar_BudgetFlowTypeIndex\000"
.LASF48:
	.ascii	"GuiVar_AboutWOption\000"
.LASF149:
	.ascii	"GuiVar_ComboBoxItemString\000"
.LASF276:
	.ascii	"GuiVar_FlowTable4Sta\000"
.LASF234:
	.ascii	"GuiVar_FlowCheckingChecked\000"
.LASF1023:
	.ascii	"GuiVar_WENWPAEAPTLSValidateCert\000"
.LASF827:
	.ascii	"GuiVar_StationGroupSoilStorageCapacity\000"
.LASF703:
	.ascii	"GuiVar_RainBucketMaximum24Hour\000"
.LASF41:
	.ascii	"GuiVar_AboutTPSta25_32Terminal\000"
.LASF967:
	.ascii	"GuiVar_TwoWireStatsRxCRCErrs\000"
.LASF878:
	.ascii	"GuiVar_StatusNextScheduledIrriGID\000"
.LASF262:
	.ascii	"GuiVar_FlowCheckingPumpMix\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF278:
	.ascii	"GuiVar_FlowTable6Sta\000"
.LASF886:
	.ascii	"GuiVar_StatusRainSwitchState\000"
.LASF972:
	.ascii	"GuiVar_TwoWireStatsRxEnqMsgs\000"
.LASF38:
	.ascii	"GuiVar_AboutTPSta17_24Card\000"
.LASF187:
	.ascii	"GuiVar_DeviceExchangeSyncingRadios\000"
.LASF959:
	.ascii	"GuiVar_TwoWireStaStrA\000"
.LASF477:
	.ascii	"GuiVar_LRGroup\000"
.LASF280:
	.ascii	"GuiVar_FlowTable8Sta\000"
.LASF939:
	.ascii	"GuiVar_TwoWireDiscoveredMoisDecoders\000"
.LASF964:
	.ascii	"GuiVar_TwoWireStatsEEPCRCS1Params\000"
.LASF917:
	.ascii	"GuiVar_SystemName\000"
.LASF734:
	.ascii	"GuiVar_RptWalkThruGal\000"
.LASF551:
	.ascii	"GuiVar_MoisSetPoint_Low\000"
.LASF922:
	.ascii	"GuiVar_SystemPreservesStabilityNext\000"
.LASF337:
	.ascii	"GuiVar_GroupSettingC_1\000"
.LASF715:
	.ascii	"GuiVar_RptFlow\000"
.LASF687:
	.ascii	"GuiVar_RadioTestCS3000SN\000"
.LASF341:
	.ascii	"GuiVar_GroupSettingC_5\000"
.LASF835:
	.ascii	"GuiVar_StationInfoCycleTooShort\000"
.LASF349:
	.ascii	"GuiVar_GRService\000"
.LASF68:
	.ascii	"GuiVar_BudgetExpectedUseThisPeriod\000"
.LASF25:
	.ascii	"GuiVar_AboutTPDashLTerminal\000"
.LASF154:
	.ascii	"GuiVar_CommOptionPortAIndex\000"
.LASF955:
	.ascii	"GuiVar_TwoWireStaAIsSet\000"
.LASF450:
	.ascii	"GuiVar_LightsStopTime1InMinutes\000"
.LASF830:
	.ascii	"GuiVar_StationGroupSystemGID\000"
.LASF719:
	.ascii	"GuiVar_RptLastMeasuredCurrent\000"
.LASF77:
	.ascii	"GuiVar_BudgetPeriod_0\000"
.LASF78:
	.ascii	"GuiVar_BudgetPeriod_1\000"
.LASF844:
	.ascii	"GuiVar_StationInfoMoistureBalancePercent\000"
.LASF989:
	.ascii	"GuiVar_UnusedGroupsRemovedGreaterThan1\000"
.LASF30:
	.ascii	"GuiVar_AboutTPFuseCard\000"
.LASF876:
	.ascii	"GuiVar_StatusMVORInEffect\000"
.LASF923:
	.ascii	"GuiVar_TaskList\000"
.LASF348:
	.ascii	"GuiVar_GRRSSI\000"
.LASF443:
	.ascii	"GuiVar_LightsOutput\000"
.LASF346:
	.ascii	"GuiVar_GRPhoneNumber\000"
.LASF74:
	.ascii	"GuiVar_BudgetInUse\000"
.LASF720:
	.ascii	"GuiVar_RptManualGal\000"
.LASF140:
	.ascii	"GuiVar_ChainStatsPresentlyMakingTokens\000"
.LASF770:
	.ascii	"GuiVar_SerialPortStateTP\000"
.LASF398:
	.ascii	"GuiVar_IrriDetailsRunSoak\000"
.LASF424:
	.ascii	"GuiVar_LightCalendarWeek2Mon\000"
.LASF437:
	.ascii	"GuiVar_LightsCalendarHighlightXOffset\000"
.LASF540:
	.ascii	"GuiVar_MoisEC_Current\000"
.LASF452:
	.ascii	"GuiVar_LightsStopTimeEnabled1\000"
.LASF453:
	.ascii	"GuiVar_LightsStopTimeEnabled2\000"
.LASF924:
	.ascii	"GuiVar_TestRunTime\000"
.LASF93:
	.ascii	"GuiVar_BudgetReportText\000"
.LASF497:
	.ascii	"GuiVar_MainMenuMainLinesAvailable\000"
.LASF155:
	.ascii	"GuiVar_CommOptionPortAInstalled\000"
.LASF965:
	.ascii	"GuiVar_TwoWireStatsEEPCRCS2Params\000"
.LASF54:
	.ascii	"GuiVar_ActivationCode\000"
.LASF880:
	.ascii	"GuiVar_StatusNOWDaysInEffect\000"
.LASF597:
	.ascii	"GuiVar_PercentAdjustEndDate_0\000"
.LASF419:
	.ascii	"GuiVar_LightCalendarWeek1Sun\000"
.LASF598:
	.ascii	"GuiVar_PercentAdjustEndDate_1\000"
.LASF447:
	.ascii	"GuiVar_LightsStartTime2InMinutes\000"
.LASF306:
	.ascii	"GuiVar_GroupSetting_Visible_0\000"
.LASF307:
	.ascii	"GuiVar_GroupSetting_Visible_1\000"
.LASF308:
	.ascii	"GuiVar_GroupSetting_Visible_2\000"
.LASF309:
	.ascii	"GuiVar_GroupSetting_Visible_3\000"
.LASF310:
	.ascii	"GuiVar_GroupSetting_Visible_4\000"
.LASF311:
	.ascii	"GuiVar_GroupSetting_Visible_5\000"
.LASF312:
	.ascii	"GuiVar_GroupSetting_Visible_6\000"
.LASF147:
	.ascii	"GuiVar_ComboBox_Y1\000"
.LASF314:
	.ascii	"GuiVar_GroupSetting_Visible_8\000"
.LASF315:
	.ascii	"GuiVar_GroupSetting_Visible_9\000"
.LASF26:
	.ascii	"GuiVar_AboutTPDashMCard\000"
.LASF706:
	.ascii	"GuiVar_RainSwitchControllerName\000"
.LASF260:
	.ascii	"GuiVar_FlowCheckingNumOn\000"
.LASF501:
	.ascii	"GuiVar_MainMenuStationsExist\000"
.LASF509:
	.ascii	"GuiVar_ManualPStartTime1\000"
.LASF49:
	.ascii	"GuiVar_AccessControlEnabled\000"
.LASF513:
	.ascii	"GuiVar_ManualPStartTime3\000"
.LASF515:
	.ascii	"GuiVar_ManualPStartTime4\000"
.LASF153:
	.ascii	"GuiVar_CommOptionIsConnected\000"
.LASF519:
	.ascii	"GuiVar_ManualPStartTime6\000"
.LASF950:
	.ascii	"GuiVar_TwoWireOutputOnB\000"
.LASF360:
	.ascii	"GuiVar_HistoricalET6\000"
.LASF651:
	.ascii	"GuiVar_POCFMType3IsBermad\000"
.LASF654:
	.ascii	"GuiVar_POCPhysicallyAvailable\000"
.LASF1024:
	.ascii	"GuiVar_WENWPAEAPTTLSOption\000"
.LASF855:
	.ascii	"GuiVar_StationInfoTotalMinutes\000"
.LASF987:
	.ascii	"GuiVar_TwoWireStatsWDTs\000"
.LASF838:
	.ascii	"GuiVar_StationInfoETFactor\000"
.LASF47:
	.ascii	"GuiVar_AboutWMOption\000"
.LASF143:
	.ascii	"GuiVar_CodeDownloadProgressBar\000"
.LASF537:
	.ascii	"GuiVar_MoisBoxIndex\000"
.LASF1028:
	.ascii	"GuiVar_WENWPAEncryptionWEP\000"
.LASF624:
	.ascii	"GuiVar_POCBudget12\000"
.LASF445:
	.ascii	"GuiVar_LightsOutputIsDeenergized\000"
.LASF225:
	.ascii	"GuiVar_FinishTimesNotes\000"
.LASF246:
	.ascii	"GuiVar_FlowCheckingGroupCnt0\000"
.LASF247:
	.ascii	"GuiVar_FlowCheckingGroupCnt1\000"
.LASF248:
	.ascii	"GuiVar_FlowCheckingGroupCnt2\000"
.LASF249:
	.ascii	"GuiVar_FlowCheckingGroupCnt3\000"
.LASF67:
	.ascii	"GuiVar_BudgetETPercentageUsed\000"
.LASF272:
	.ascii	"GuiVar_FlowRecTimeStamp\000"
.LASF992:
	.ascii	"GuiVar_WalkThruBoxIndex\000"
.LASF490:
	.ascii	"GuiVar_MainMenuBypassExists\000"
.LASF438:
	.ascii	"GuiVar_LightsCalendarHighlightYOffset\000"
.LASF516:
	.ascii	"GuiVar_ManualPStartTime4Enabled\000"
.LASF686:
	.ascii	"GuiVar_RadioTestCommWithET2000e\000"
.LASF634:
	.ascii	"GuiVar_POCBudgetPercentOfET\000"
.LASF55:
	.ascii	"GuiVar_AlertDate\000"
.LASF877:
	.ascii	"GuiVar_StatusNextScheduledIrriDate\000"
.LASF982:
	.ascii	"GuiVar_TwoWireStatsS1UCos\000"
.LASF238:
	.ascii	"GuiVar_FlowCheckingFlag0\000"
.LASF239:
	.ascii	"GuiVar_FlowCheckingFlag1\000"
.LASF240:
	.ascii	"GuiVar_FlowCheckingFlag2\000"
.LASF241:
	.ascii	"GuiVar_FlowCheckingFlag3\000"
.LASF242:
	.ascii	"GuiVar_FlowCheckingFlag4\000"
.LASF243:
	.ascii	"GuiVar_FlowCheckingFlag5\000"
.LASF244:
	.ascii	"GuiVar_FlowCheckingFlag6\000"
.LASF286:
	.ascii	"GuiVar_FreezeSwitchSelected\000"
.LASF665:
	.ascii	"GuiVar_POCPreservesDecoderSN1\000"
.LASF666:
	.ascii	"GuiVar_POCPreservesDecoderSN2\000"
.LASF14:
	.ascii	"GuiVar_AboutLOption\000"
.LASF695:
	.ascii	"GuiVar_RadioTestPort\000"
.LASF1009:
	.ascii	"GuiVar_WENRadioType\000"
.LASF386:
	.ascii	"GuiVar_IrriCommChainConfigCurrentFromIrri\000"
.LASF105:
	.ascii	"GuiVar_ChainStatsErrorsA\000"
.LASF106:
	.ascii	"GuiVar_ChainStatsErrorsB\000"
.LASF107:
	.ascii	"GuiVar_ChainStatsErrorsC\000"
.LASF108:
	.ascii	"GuiVar_ChainStatsErrorsD\000"
.LASF109:
	.ascii	"GuiVar_ChainStatsErrorsE\000"
.LASF110:
	.ascii	"GuiVar_ChainStatsErrorsF\000"
.LASF111:
	.ascii	"GuiVar_ChainStatsErrorsG\000"
.LASF112:
	.ascii	"GuiVar_ChainStatsErrorsH\000"
.LASF113:
	.ascii	"GuiVar_ChainStatsErrorsI\000"
.LASF114:
	.ascii	"GuiVar_ChainStatsErrorsJ\000"
.LASF115:
	.ascii	"GuiVar_ChainStatsErrorsK\000"
.LASF116:
	.ascii	"GuiVar_ChainStatsErrorsL\000"
.LASF435:
	.ascii	"GuiVar_LightsAreEnergized\000"
.LASF839:
	.ascii	"GuiVar_StationInfoExpectedFlow\000"
.LASF291:
	.ascii	"GuiVar_GRIPAddress\000"
.LASF95:
	.ascii	"GuiVar_BudgetSysHasPOC\000"
.LASF488:
	.ascii	"GuiVar_MainMenuAquaponicsMode\000"
.LASF164:
	.ascii	"GuiVar_CommTestShowDebug\000"
.LASF985:
	.ascii	"GuiVar_TwoWireStatsTempMax\000"
.LASF542:
	.ascii	"GuiVar_MoisECAction_Low\000"
.LASF722:
	.ascii	"GuiVar_RptManualPGal\000"
.LASF180:
	.ascii	"GuiVar_DateTimeMonth\000"
.LASF731:
	.ascii	"GuiVar_RptStation\000"
.LASF1025:
	.ascii	"GuiVar_WENWPAEncryption\000"
.LASF895:
	.ascii	"GuiVar_StatusSystemFlowSystem\000"
.LASF543:
	.ascii	"GuiVar_MoisECPoint_High\000"
.LASF713:
	.ascii	"GuiVar_RptDecoderSN\000"
.LASF621:
	.ascii	"GuiVar_POCBudget1\000"
.LASF44:
	.ascii	"GuiVar_AboutTPSta41_48Card\000"
.LASF476:
	.ascii	"GuiVar_LRFrequency_WholeNum\000"
.LASF292:
	.ascii	"GuiVar_GRMake\000"
.LASF628:
	.ascii	"GuiVar_POCBudget5\000"
.LASF629:
	.ascii	"GuiVar_POCBudget6\000"
.LASF630:
	.ascii	"GuiVar_POCBudget7\000"
.LASF631:
	.ascii	"GuiVar_POCBudget8\000"
.LASF514:
	.ascii	"GuiVar_ManualPStartTime3Enabled\000"
.LASF761:
	.ascii	"GuiVar_SerialPortCTSA\000"
.LASF762:
	.ascii	"GuiVar_SerialPortCTSB\000"
.LASF263:
	.ascii	"GuiVar_FlowCheckingPumpState\000"
.LASF19:
	.ascii	"GuiVar_AboutNetworkID\000"
.LASF471:
	.ascii	"GuiVar_LiveScreensSystemMLBExists\000"
.LASF421:
	.ascii	"GuiVar_LightCalendarWeek1Tue\000"
.LASF205:
	.ascii	"GuiVar_ETGageInstalledAt\000"
.LASF473:
	.ascii	"GuiVar_LRBeaconRate\000"
.LASF699:
	.ascii	"GuiVar_RadioTestStartTime\000"
.LASF175:
	.ascii	"GuiVar_DateTimeDay\000"
.LASF743:
	.ascii	"GuiVar_ScheduleStartTimeEqualsStopTime\000"
.LASF80:
	.ascii	"GuiVar_BudgetPeriodIdx\000"
.LASF771:
	.ascii	"GuiVar_SerialPortXmitA\000"
.LASF122:
	.ascii	"GuiVar_ChainStatsInUseA\000"
.LASF123:
	.ascii	"GuiVar_ChainStatsInUseB\000"
.LASF124:
	.ascii	"GuiVar_ChainStatsInUseC\000"
.LASF125:
	.ascii	"GuiVar_ChainStatsInUseD\000"
.LASF126:
	.ascii	"GuiVar_ChainStatsInUseE\000"
.LASF127:
	.ascii	"GuiVar_ChainStatsInUseF\000"
.LASF128:
	.ascii	"GuiVar_ChainStatsInUseG\000"
.LASF129:
	.ascii	"GuiVar_ChainStatsInUseH\000"
.LASF130:
	.ascii	"GuiVar_ChainStatsInUseI\000"
.LASF131:
	.ascii	"GuiVar_ChainStatsInUseJ\000"
.LASF132:
	.ascii	"GuiVar_ChainStatsInUseK\000"
.LASF133:
	.ascii	"GuiVar_ChainStatsInUseL\000"
.LASF17:
	.ascii	"GuiVar_AboutMOption\000"
.LASF408:
	.ascii	"GuiVar_itmPOC\000"
.LASF833:
	.ascii	"GuiVar_StationInfoControllerName\000"
.LASF523:
	.ascii	"GuiVar_ManualPStationRunTimeStationNumber\000"
.LASF469:
	.ascii	"GuiVar_LiveScreensPOCFlowMVState\000"
.LASF444:
	.ascii	"GuiVar_LightsOutputIndex_0\000"
.LASF956:
	.ascii	"GuiVar_TwoWireStaB\000"
.LASF63:
	.ascii	"GuiVar_BudgetAnnualValue\000"
.LASF192:
	.ascii	"GuiVar_ENGateway_1\000"
.LASF193:
	.ascii	"GuiVar_ENGateway_2\000"
.LASF194:
	.ascii	"GuiVar_ENGateway_3\000"
.LASF195:
	.ascii	"GuiVar_ENGateway_4\000"
.LASF945:
	.ascii	"GuiVar_TwoWireNumDiscoveredPOCDecoders\000"
.LASF707:
	.ascii	"GuiVar_RainSwitchInUse\000"
.LASF271:
	.ascii	"GuiVar_FlowRecFlag\000"
.LASF797:
	.ascii	"GuiVar_StationDecoderOutput\000"
.LASF454:
	.ascii	"GuiVar_LightsStopTimeHasBeenEdited\000"
.LASF347:
	.ascii	"GuiVar_GRRadioSerialNum\000"
.LASF172:
	.ascii	"GuiVar_Contrast\000"
.LASF448:
	.ascii	"GuiVar_LightsStartTimeEnabled1\000"
.LASF449:
	.ascii	"GuiVar_LightsStartTimeEnabled2\000"
.LASF316:
	.ascii	"GuiVar_GroupSettingA_0\000"
.LASF317:
	.ascii	"GuiVar_GroupSettingA_1\000"
.LASF318:
	.ascii	"GuiVar_GroupSettingA_2\000"
.LASF319:
	.ascii	"GuiVar_GroupSettingA_3\000"
.LASF320:
	.ascii	"GuiVar_GroupSettingA_4\000"
.LASF321:
	.ascii	"GuiVar_GroupSettingA_5\000"
.LASF322:
	.ascii	"GuiVar_GroupSettingA_6\000"
.LASF323:
	.ascii	"GuiVar_GroupSettingA_7\000"
.LASF171:
	.ascii	"GuiVar_ContactStatusSerialNumber\000"
.LASF325:
	.ascii	"GuiVar_GroupSettingA_9\000"
.LASF764:
	.ascii	"GuiVar_SerialPortOptionB\000"
.LASF750:
	.ascii	"GuiVar_ScheduleWaterDay_Fri\000"
.LASF228:
	.ascii	"GuiVar_FinishTimesStationGroup\000"
.LASF674:
	.ascii	"GuiVar_POCPreservesDeliveredPumpCurrent1\000"
.LASF99:
	.ascii	"GuiVar_BudgetUsedSYS\000"
.LASF675:
	.ascii	"GuiVar_POCPreservesDeliveredPumpCurrent2\000"
.LASF500:
	.ascii	"GuiVar_MainMenuPOCsExist\000"
.LASF676:
	.ascii	"GuiVar_POCPreservesDeliveredPumpCurrent3\000"
.LASF483:
	.ascii	"GuiVar_LRSerialNumber\000"
.LASF283:
	.ascii	"GuiVar_FLPartOfChain\000"
.LASF823:
	.ascii	"GuiVar_StationGroupPrecipRate\000"
.LASF584:
	.ascii	"GuiVar_MVOROpen5Enabled\000"
.LASF794:
	.ascii	"GuiVar_StationCopyGroupName\000"
.LASF530:
	.ascii	"GuiVar_ManualPWaterDay_Wed\000"
.LASF682:
	.ascii	"GuiVar_POCReedSwitchType3\000"
.LASF524:
	.ascii	"GuiVar_ManualPWaterDay_Fri\000"
.LASF619:
	.ascii	"GuiVar_PercentAdjustPositive\000"
.LASF639:
	.ascii	"GuiVar_POCDecoderSN3\000"
.LASF608:
	.ascii	"GuiVar_PercentAdjustPercent\000"
.LASF70:
	.ascii	"GuiVar_BudgetFlowTypeIrrigation\000"
.LASF60:
	.ascii	"GuiVar_BudgetAllocation\000"
.LASF496:
	.ascii	"GuiVar_MainMenuLightsOptionsExist\000"
.LASF659:
	.ascii	"GuiVar_POCPreservesAccumMS2\000"
.LASF457:
	.ascii	"GuiVar_LiveScreensBypassFlowFlow\000"
.LASF31:
	.ascii	"GuiVar_AboutTPMicroCard\000"
.LASF117:
	.ascii	"GuiVar_ChainStatsFOALChainRRcvd\000"
.LASF235:
	.ascii	"GuiVar_FlowCheckingCycles\000"
.LASF842:
	.ascii	"GuiVar_StationInfoIStatus\000"
.LASF993:
	.ascii	"GuiVar_WalkThruControllerName\000"
.LASF757:
	.ascii	"GuiVar_ScrollBoxHorizScrollMarker\000"
.LASF410:
	.ascii	"GuiVar_itmStationNumber\000"
.LASF1039:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/"
	.ascii	"GuiVar.c\000"
.LASF951:
	.ascii	"GuiVar_TwoWirePOCDecoderIndex\000"
.LASF179:
	.ascii	"GuiVar_DateTimeMin\000"
.LASF495:
	.ascii	"GuiVar_MainMenuHubOptionExists\000"
.LASF899:
	.ascii	"GuiVar_StatusWindGageControllerName\000"
.LASF640:
	.ascii	"GuiVar_POCFlowMeterK1\000"
.LASF641:
	.ascii	"GuiVar_POCFlowMeterK2\000"
.LASF642:
	.ascii	"GuiVar_POCFlowMeterK3\000"
.LASF422:
	.ascii	"GuiVar_LightCalendarWeek1Wed\000"
.LASF567:
	.ascii	"GuiVar_MVORClose4Enabled\000"
.LASF504:
	.ascii	"GuiVar_ManualPEndDateStr\000"
.LASF24:
	.ascii	"GuiVar_AboutTPDashLCard\000"
.LASF251:
	.ascii	"GuiVar_FlowCheckingHi\000"
.LASF716:
	.ascii	"GuiVar_RptIrrigGal\000"
.LASF324:
	.ascii	"GuiVar_GroupSettingA_8\000"
.LASF215:
	.ascii	"GuiVar_ETRainTableETStatus\000"
.LASF427:
	.ascii	"GuiVar_LightCalendarWeek2Thu\000"
.LASF921:
	.ascii	"GuiVar_SystemPreservesStabilityMostRecent5Sec\000"
.LASF996:
	.ascii	"GuiVar_WalkThruOrder\000"
.LASF545:
	.ascii	"GuiVar_MoisInUse\000"
.LASF611:
	.ascii	"GuiVar_PercentAdjustPercent_2\000"
.LASF413:
	.ascii	"GuiVar_KeyboardStartX\000"
.LASF414:
	.ascii	"GuiVar_KeyboardStartY\000"
.LASF808:
	.ascii	"GuiVar_StationGroupKc10\000"
.LASF139:
	.ascii	"GuiVar_ChainStatsNextContactSerNum\000"
.LASF52:
	.ascii	"GuiVar_AccessControlUser\000"
.LASF466:
	.ascii	"GuiVar_LiveScreensCommFLInForced\000"
.LASF120:
	.ascii	"GuiVar_ChainStatsFOALIrriTGen\000"
.LASF635:
	.ascii	"GuiVar_POCBudgetYearly\000"
.LASF59:
	.ascii	"GuiVar_BacklightSliderPos\000"
.LASF27:
	.ascii	"GuiVar_AboutTPDashMTerminal\000"
.LASF293:
	.ascii	"GuiVar_GRModel\000"
.LASF484:
	.ascii	"GuiVar_LRSlaveLinksToRepeater\000"
.LASF43:
	.ascii	"GuiVar_AboutTPSta33_40Terminal\000"
.LASF446:
	.ascii	"GuiVar_LightsStartTime1InMinutes\000"
.LASF881:
	.ascii	"GuiVar_StatusOverviewStationStr\000"
.LASF622:
	.ascii	"GuiVar_POCBudget10\000"
.LASF257:
	.ascii	"GuiVar_FlowCheckingMVJOTime\000"
.LASF460:
	.ascii	"GuiVar_LiveScreensBypassFlowMVOpen\000"
.LASF953:
	.ascii	"GuiVar_TwoWirePOCUsedForBypass\000"
.LASF874:
	.ascii	"GuiVar_StatusIrrigationActivityState\000"
.LASF7:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF786:
	.ascii	"GuiVar_SRSerialNumber\000"
.LASF0:
	.ascii	"short int\000"
.LASF990:
	.ascii	"GuiVar_Volume\000"
.LASF852:
	.ascii	"GuiVar_StationInfoSoakInTime\000"
.LASF1035:
	.ascii	"GuiVar_WindGageInUse\000"
.LASF752:
	.ascii	"GuiVar_ScheduleWaterDay_Sat\000"
.LASF273:
	.ascii	"GuiVar_FlowTable10Sta\000"
.LASF220:
	.ascii	"GuiVar_ETRainTableTable\000"
.LASF934:
	.ascii	"GuiVar_TwoWireDecoderSN\000"
.LASF944:
	.ascii	"GuiVar_TwoWireNumDiscoveredMoisDecoders\000"
.LASF1007:
	.ascii	"GuiVar_WENPassphrase\000"
.LASF1013:
	.ascii	"GuiVar_WENStatus\000"
.LASF383:
	.ascii	"GuiVar_HubList\000"
.LASF75:
	.ascii	"GuiVar_BudgetMainlineName\000"
.LASF893:
	.ascii	"GuiVar_StatusSystemFlowShowMLBDetected\000"
.LASF994:
	.ascii	"GuiVar_WalkThruCountingDown\000"
.LASF216:
	.ascii	"GuiVar_ETRainTableRain\000"
.LASF289:
	.ascii	"GuiVar_GRFirmwareVersion\000"
.LASF182:
	.ascii	"GuiVar_DateTimeShowButton\000"
.LASF409:
	.ascii	"GuiVar_itmStationName\000"
.LASF668:
	.ascii	"GuiVar_POCPreservesDelivered5SecAvg1\000"
.LASF526:
	.ascii	"GuiVar_ManualPWaterDay_Sat\000"
.LASF670:
	.ascii	"GuiVar_POCPreservesDelivered5SecAvg3\000"
.LASF937:
	.ascii	"GuiVar_TwoWireDescA\000"
.LASF294:
	.ascii	"GuiVar_GRNetworkState\000"
.LASF4:
	.ascii	"GuiFont_LanguageActive\000"
.LASF461:
	.ascii	"GuiVar_LiveScreensBypassFlowShowMaxFlow\000"
.LASF778:
	.ascii	"GuiVar_ShowDivider\000"
.LASF522:
	.ascii	"GuiVar_ManualPStationRunTimeControllerName\000"
.LASF969:
	.ascii	"GuiVar_TwoWireStatsRxDecRstMsgs\000"
.LASF763:
	.ascii	"GuiVar_SerialPortOptionA\000"
.LASF104:
	.ascii	"GuiVar_ChainStatsControllersInChain\000"
.LASF552:
	.ascii	"GuiVar_MoisTemp_Current\000"
.LASF941:
	.ascii	"GuiVar_TwoWireDiscoveredStaDecoders\000"
.LASF32:
	.ascii	"GuiVar_AboutTPPOCCard\000"
.LASF134:
	.ascii	"GuiVar_ChainStatsIRRIChainRGen\000"
.LASF828:
	.ascii	"GuiVar_StationGroupSoilType\000"
.LASF223:
	.ascii	"GuiVar_FinishTimesCalcProgressBar\000"
.LASF11:
	.ascii	"GuiVar_AboutDebug\000"
.LASF599:
	.ascii	"GuiVar_PercentAdjustEndDate_2\000"
.LASF367:
	.ascii	"GuiVar_HistoricalETUser1\000"
.LASF601:
	.ascii	"GuiVar_PercentAdjustEndDate_4\000"
.LASF184:
	.ascii	"GuiVar_DateTimeYear\000"
.LASF603:
	.ascii	"GuiVar_PercentAdjustEndDate_6\000"
.LASF604:
	.ascii	"GuiVar_PercentAdjustEndDate_7\000"
.LASF605:
	.ascii	"GuiVar_PercentAdjustEndDate_8\000"
.LASF606:
	.ascii	"GuiVar_PercentAdjustEndDate_9\000"
.LASF733:
	.ascii	"GuiVar_RptTestMin\000"
.LASF779:
	.ascii	"GuiVar_SpinnerPos\000"
.LASF637:
	.ascii	"GuiVar_POCDecoderSN1\000"
.LASF529:
	.ascii	"GuiVar_ManualPWaterDay_Tue\000"
.LASF181:
	.ascii	"GuiVar_DateTimeSec\000"
.LASF925:
	.ascii	"GuiVar_TestStationAutoMode\000"
.LASF418:
	.ascii	"GuiVar_LightCalendarWeek1Sat\000"
.LASF837:
	.ascii	"GuiVar_StationInfoEstMin\000"
.LASF35:
	.ascii	"GuiVar_AboutTPSta01_08Terminal\000"
.LASF439:
	.ascii	"GuiVar_LightScheduledStart\000"
.LASF892:
	.ascii	"GuiVar_StatusSystemFlowMLBMeasured\000"
.LASF780:
	.ascii	"GuiVar_SRFirmwareVer\000"
.LASF431:
	.ascii	"GuiVar_LightIrriListStop\000"
.LASF556:
	.ascii	"GuiVar_MoisTempPoint_Low\000"
.LASF9:
	.ascii	"GuiVar_AboutAntennaHoles\000"
.LASF822:
	.ascii	"GuiVar_StationGroupPlantType\000"
.LASF919:
	.ascii	"GuiVar_SystemPreserves5SecMostRecent\000"
.LASF13:
	.ascii	"GuiVar_AboutHubOption\000"
.LASF492:
	.ascii	"GuiVar_MainMenuFLOptionExists\000"
.LASF689:
	.ascii	"GuiVar_RadioTestET2000eAddr0\000"
.LASF465:
	.ascii	"GuiVar_LiveScreensCommFLEnabled\000"
.LASF690:
	.ascii	"GuiVar_RadioTestET2000eAddr1\000"
.LASF858:
	.ascii	"GuiVar_StationSelectionGridMarkerPos\000"
.LASF691:
	.ascii	"GuiVar_RadioTestET2000eAddr2\000"
.LASF65:
	.ascii	"GuiVar_BudgetEntryOptionIdx\000"
.LASF553:
	.ascii	"GuiVar_MoisTempAction_High\000"
.LASF470:
	.ascii	"GuiVar_LiveScreensPOCFlowName\000"
.LASF848:
	.ascii	"GuiVar_StationInfoShowCopyStation\000"
.LASF441:
	.ascii	"GuiVar_LightScheduleState\000"
.LASF889:
	.ascii	"GuiVar_StatusSystemFlowActual\000"
.LASF958:
	.ascii	"GuiVar_TwoWireStaDecoderIndex\000"
.LASF119:
	.ascii	"GuiVar_ChainStatsFOALIrriRRcvd\000"
.LASF550:
	.ascii	"GuiVar_MoisSetPoint_High\000"
.LASF379:
	.ascii	"GuiVar_HistoricalETUserCity\000"
.LASF694:
	.ascii	"GuiVar_RadioTestPacketsSent\000"
.LASF908:
	.ascii	"GuiVar_SystemFlowCheckingToleranceMinus1\000"
.LASF173:
	.ascii	"GuiVar_ContrastSliderPos\000"
.LASF485:
	.ascii	"GuiVar_LRTemperature\000"
.LASF269:
	.ascii	"GuiVar_FlowRecActual\000"
.LASF183:
	.ascii	"GuiVar_DateTimeTimeZone\000"
.LASF911:
	.ascii	"GuiVar_SystemFlowCheckingToleranceMinus4\000"
.LASF726:
	.ascii	"GuiVar_RptRainMin\000"
.LASF576:
	.ascii	"GuiVar_MVOROpen1Enabled\000"
.LASF487:
	.ascii	"GuiVar_MainMenu2WireOptionsExist\000"
.LASF595:
	.ascii	"GuiVar_NumericKeypadValue\000"
.LASF732:
	.ascii	"GuiVar_RptTestGal\000"
.LASF767:
	.ascii	"GuiVar_SerialPortRcvdTP\000"
.LASF768:
	.ascii	"GuiVar_SerialPortStateA\000"
.LASF769:
	.ascii	"GuiVar_SerialPortStateB\000"
.LASF847:
	.ascii	"GuiVar_StationInfoNumber_str\000"
.LASF486:
	.ascii	"GuiVar_LRTransmitPower\000"
.LASF256:
	.ascii	"GuiVar_FlowCheckingMaxOn\000"
.LASF952:
	.ascii	"GuiVar_TwoWirePOCInUse\000"
.LASF890:
	.ascii	"GuiVar_StatusSystemFlowExpected\000"
.LASF1038:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF894:
	.ascii	"GuiVar_StatusSystemFlowStatus\000"
.LASF683:
	.ascii	"GuiVar_POCType\000"
.LASF502:
	.ascii	"GuiVar_MainMenuWeatherDeviceExists\000"
.LASF824:
	.ascii	"GuiVar_StationGroupRootZoneDepth\000"
.LASF741:
	.ascii	"GuiVar_ScheduleStartTime\000"
.LASF646:
	.ascii	"GuiVar_POCFlowMeterType1\000"
.LASF647:
	.ascii	"GuiVar_POCFlowMeterType2\000"
.LASF541:
	.ascii	"GuiVar_MoisECAction_High\000"
.LASF781:
	.ascii	"GuiVar_SRGroup\000"
.LASF201:
	.ascii	"GuiVar_ENModel\000"
.LASF18:
	.ascii	"GuiVar_AboutMSSEOption\000"
.LASF1011:
	.ascii	"GuiVar_WENSecurity\000"
.LASF559:
	.ascii	"GuiVar_MVORClose0Enabled\000"
.LASF702:
	.ascii	"GuiVar_RainBucketInUse\000"
.LASF1029:
	.ascii	"GuiVar_WENWPAIEEE8021X\000"
.LASF169:
	.ascii	"GuiVar_ContactStatusNameAbbrv\000"
.LASF255:
	.ascii	"GuiVar_FlowCheckingLo\000"
.LASF845:
	.ascii	"GuiVar_StationInfoNoWaterDays\000"
.LASF76:
	.ascii	"GuiVar_BudgetModeIdx\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF167:
	.ascii	"GuiVar_ContactStatusAlive\000"
.LASF801:
	.ascii	"GuiVar_StationGroupAllowableSurfaceAccum\000"
.LASF650:
	.ascii	"GuiVar_POCFMType2IsBermad\000"
.LASF746:
	.ascii	"GuiVar_ScheduleType\000"
.LASF520:
	.ascii	"GuiVar_ManualPStartTime6Enabled\000"
.LASF849:
	.ascii	"GuiVar_StationInfoShowEstMin\000"
.LASF178:
	.ascii	"GuiVar_DateTimeHour\000"
.LASF389:
	.ascii	"GuiVar_IrriCommChainConfigCurrentSendFromTP\000"
.LASF853:
	.ascii	"GuiVar_StationInfoSquareFootage\000"
.LASF58:
	.ascii	"GuiVar_Backlight\000"
.LASF493:
	.ascii	"GuiVar_MainMenuFlowCheckingInUse\000"
.LASF904:
	.ascii	"GuiVar_SystemFlowCheckingInUse\000"
.LASF979:
	.ascii	"GuiVar_TwoWireStatsRxSolCurMeasReqMsgs\000"
.LASF957:
	.ascii	"GuiVar_TwoWireStaBIsSet\000"
.LASF494:
	.ascii	"GuiVar_MainMenuFlowMetersExist\000"
.LASF558:
	.ascii	"GuiVar_MVORClose0\000"
.LASF667:
	.ascii	"GuiVar_POCPreservesDecoderSN3\000"
.LASF970:
	.ascii	"GuiVar_TwoWireStatsRxDiscConfMsgs\000"
.LASF975:
	.ascii	"GuiVar_TwoWireStatsRxLongMsgs\000"
.LASF232:
	.ascii	"GuiVar_FlowCheckingAllowed\000"
.LASF609:
	.ascii	"GuiVar_PercentAdjustPercent_0\000"
.LASF326:
	.ascii	"GuiVar_GroupSettingB_0\000"
.LASF327:
	.ascii	"GuiVar_GroupSettingB_1\000"
.LASF328:
	.ascii	"GuiVar_GroupSettingB_2\000"
.LASF329:
	.ascii	"GuiVar_GroupSettingB_3\000"
.LASF330:
	.ascii	"GuiVar_GroupSettingB_4\000"
.LASF331:
	.ascii	"GuiVar_GroupSettingB_5\000"
.LASF332:
	.ascii	"GuiVar_GroupSettingB_6\000"
.LASF333:
	.ascii	"GuiVar_GroupSettingB_7\000"
.LASF334:
	.ascii	"GuiVar_GroupSettingB_8\000"
.LASF335:
	.ascii	"GuiVar_GroupSettingB_9\000"
.LASF396:
	.ascii	"GuiVar_IrriDetailsReason\000"
.LASF6:
	.ascii	"GuiFont_DecimalChar\000"
.LASF472:
	.ascii	"GuiVar_LiveScreensSystemMVORExists\000"
.LASF802:
	.ascii	"GuiVar_StationGroupAvailableWater\000"
.LASF565:
	.ascii	"GuiVar_MVORClose3Enabled\000"
.LASF1001:
	.ascii	"GuiVar_WENChangeCredentials\000"
.LASF795:
	.ascii	"GuiVar_StationCopyTextOffset_Left\000"
.LASF498:
	.ascii	"GuiVar_MainMenuManualAvailable\000"
.LASF207:
	.ascii	"GuiVar_ETGageLogPulses\000"
.LASF783:
	.ascii	"GuiVar_SRPort\000"
.LASF1020:
	.ascii	"GuiVar_WENWPA2Encryption\000"
.LASF632:
	.ascii	"GuiVar_POCBudget9\000"
.LASF480:
	.ascii	"GuiVar_LRPort\000"
.LASF185:
	.ascii	"GuiVar_DelayBetweenValvesInUse\000"
.LASF887:
	.ascii	"GuiVar_StatusShowLiveScreens\000"
.LASF688:
	.ascii	"GuiVar_RadioTestDeviceIndex\000"
.LASF287:
	.ascii	"GuiVar_GRCarrier\000"
.LASF62:
	.ascii	"GuiVar_BudgetAllocationSYS\000"
.LASF401:
	.ascii	"GuiVar_IrriDetailsStatus\000"
.LASF834:
	.ascii	"GuiVar_StationInfoCycleTime\000"
.LASF1014:
	.ascii	"GuiVar_WENWEPAuthentication\000"
.LASF404:
	.ascii	"GuiVar_itmDecoder_Debug\000"
.LASF943:
	.ascii	"GuiVar_TwoWireDiscoveryState\000"
.LASF875:
	.ascii	"GuiVar_StatusMLBInEffect\000"
.LASF582:
	.ascii	"GuiVar_MVOROpen4Enabled\000"
.LASF214:
	.ascii	"GuiVar_ETRainTableET\000"
.LASF933:
	.ascii	"GuiVar_TwoWireDecoderFWVersion\000"
.LASF284:
	.ascii	"GuiVar_FreezeSwitchControllerName\000"
.LASF136:
	.ascii	"GuiVar_ChainStatsIRRIIrriRGen\000"
.LASF760:
	.ascii	"GuiVar_SerialPortCDB\000"
.LASF936:
	.ascii	"GuiVar_TwoWireDecoderType\000"
.LASF1034:
	.ascii	"GuiVar_WindGageInstalledAt\000"
.LASF400:
	.ascii	"GuiVar_IrriDetailsStation\000"
.LASF1032:
	.ascii	"GuiVar_WENWPAPEAPOption\000"
.LASF462:
	.ascii	"GuiVar_LiveScreensCommCentralConnected\000"
.LASF820:
	.ascii	"GuiVar_StationGroupMicroclimateFactor\000"
.LASF652:
	.ascii	"GuiVar_POCGroupID\000"
.LASF1033:
	.ascii	"GuiVar_WENWPAUsername\000"
.LASF831:
	.ascii	"GuiVar_StationGroupUsableRain\000"
.LASF270:
	.ascii	"GuiVar_FlowRecExpected\000"
.LASF191:
	.ascii	"GuiVar_ENFirmwareVer\000"
.LASF997:
	.ascii	"GuiVar_WalkThruRunTime\000"
.LASF932:
	.ascii	"GuiVar_TurnOffControllerIsOff\000"
.LASF906:
	.ascii	"GuiVar_SystemFlowCheckingRange2\000"
.LASF747:
	.ascii	"GuiVar_ScheduleTypeScrollItem\000"
.LASF775:
	.ascii	"GuiVar_SerialPortXmittingA\000"
.LASF138:
	.ascii	"GuiVar_ChainStatsNextContact\000"
.LASF871:
	.ascii	"GuiVar_StatusFreezeSwitchConnected\000"
.LASF536:
	.ascii	"GuiVar_MoisAdditionalSoakSeconds\000"
.LASF467:
	.ascii	"GuiVar_LiveScreensCommTPMicroConnected\000"
.LASF554:
	.ascii	"GuiVar_MoisTempAction_Low\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
