	.file	"r_alerts.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	g_ALERTS_line_count
	.section	.bss.g_ALERTS_line_count,"aw",%nobits
	.align	2
	.type	g_ALERTS_line_count, %object
	.size	g_ALERTS_line_count, 4
g_ALERTS_line_count:
	.space	4
	.global	g_ALERTS_pile_to_show
	.section	.bss.g_ALERTS_pile_to_show,"aw",%nobits
	.align	2
	.type	g_ALERTS_pile_to_show, %object
	.size	g_ALERTS_pile_to_show, 4
g_ALERTS_pile_to_show:
	.space	4
	.global	g_ALERTS_filter_to_show
	.section	.bss.g_ALERTS_filter_to_show,"aw",%nobits
	.align	2
	.type	g_ALERTS_filter_to_show, %object
	.size	g_ALERTS_filter_to_show, 4
g_ALERTS_filter_to_show:
	.space	4
	.global	alerts_display_struct_user
	.section	.bss.alerts_display_struct_user,"aw",%nobits
	.align	2
	.type	alerts_display_struct_user, %object
	.size	alerts_display_struct_user, 3656
alerts_display_struct_user:
	.space	3656
	.global	alerts_display_struct_changes
	.section	.bss.alerts_display_struct_changes,"aw",%nobits
	.align	2
	.type	alerts_display_struct_changes, %object
	.size	alerts_display_struct_changes, 3656
alerts_display_struct_changes:
	.space	3656
	.global	alerts_display_struct_tp_micro
	.section	.bss.alerts_display_struct_tp_micro,"aw",%nobits
	.align	2
	.type	alerts_display_struct_tp_micro, %object
	.size	alerts_display_struct_tp_micro, 3656
alerts_display_struct_tp_micro:
	.space	3656
	.global	alerts_display_struct_engineering
	.section	.bss.alerts_display_struct_engineering,"aw",%nobits
	.align	2
	.type	alerts_display_struct_engineering, %object
	.size	alerts_display_struct_engineering, 3656
alerts_display_struct_engineering:
	.space	3656
	.section .rodata
	.align	2
.LC0:
	.ascii	"%d/%02d  \000"
	.section	.text.nm_ALERTS_parse_alert_and_return_length,"ax",%progbits
	.align	2
	.global	nm_ALERTS_parse_alert_and_return_length
	.type	nm_ALERTS_parse_alert_and_return_length, %function
nm_ALERTS_parse_alert_and_return_length:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_alerts.c"
	.loc 1 230 0
	@ args = 4, pretend = 0, frame = 64
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #72
.LCFI2:
	str	r0, [fp, #-56]
	str	r1, [fp, #-60]
	str	r2, [fp, #-64]
	str	r3, [fp, #-68]
	.loc 1 251 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 256 0
	sub	r3, fp, #14
	ldr	r0, [fp, #-56]
	mov	r1, r3
	add	r3, fp, #4
	mov	r2, r3
	mov	r3, #2
	bl	ALERTS_pull_object_off_pile
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 258 0
	ldrh	r3, [fp, #-14]
	str	r3, [fp, #-12]
	.loc 1 261 0
	sub	r3, fp, #20
	ldr	r0, [fp, #-56]
	mov	r1, r3
	add	r3, fp, #4
	mov	r2, r3
	mov	r3, #6
	bl	ALERTS_pull_object_off_pile
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 265 0
	ldr	r3, [fp, #-20]
	bic	r3, r3, #-16777216
	bic	r3, r3, #12582912
	str	r3, [fp, #-20]
	.loc 1 269 0
	ldr	r3, [fp, #-60]
	cmp	r3, #200
	bne	.L2
	.loc 1 271 0
	ldr	r3, .L4
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L3
.LBB2:
	.loc 1 278 0
	ldrh	r3, [fp, #-16]
	mov	r0, r3
	sub	r1, fp, #40
	sub	r2, fp, #44
	sub	r3, fp, #48
	sub	ip, fp, #52
	str	ip, [sp, #0]
	bl	DateToDMY
	.loc 1 286 0
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-40]
	str	r2, [sp, #0]
	ldr	r0, .L4+4
	mov	r1, #6
	ldr	r2, .L4+8
	bl	snprintf
	.loc 1 290 0
	ldr	r3, [fp, #-20]
	sub	r2, fp, #36
	mov	r1, #1
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, #16
	mov	r2, r3
	mov	r3, #0
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r3, r0
	ldr	r0, .L4+12
	mov	r1, r3
	mov	r2, #10
	bl	strlcpy
	b	.L2
.L3:
.LBE2:
	.loc 1 294 0
	ldrh	r3, [fp, #-16]
	sub	r2, fp, #36
	mov	r1, #250
	str	r1, [sp, #0]
	mov	r0, r2
	mov	r1, #16
	mov	r2, r3
	mov	r3, #150
	bl	GetDateStr
	mov	r3, r0
	ldr	r0, .L4+4
	mov	r1, r3
	mov	r2, #6
	bl	strlcpy
	.loc 1 295 0
	ldr	r3, [fp, #-20]
	sub	r2, fp, #36
	mov	r1, #0
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	mov	r0, r2
	mov	r1, #16
	mov	r2, r3
	mov	r3, #0
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r3, r0
	ldr	r0, .L4+12
	mov	r1, r3
	mov	r2, #10
	bl	strlcpy
.L2:
	.loc 1 300 0
	ldr	r3, [fp, #4]
	ldr	r2, [fp, #-68]
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-56]
	ldr	r2, [fp, #-60]
	ldr	r3, [fp, #-64]
	bl	nm_ALERT_PARSING_parse_alert_and_return_length
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 302 0
	ldr	r3, [fp, #-8]
	.loc 1 303 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	g_ALERTS_pile_to_show
	.word	GuiVar_AlertDate
	.word	.LC0
	.word	GuiVar_AlertTime
.LFE0:
	.size	nm_ALERTS_parse_alert_and_return_length, .-nm_ALERTS_parse_alert_and_return_length
	.section	.text.nm_ALERTS_restart_display_start_indicies,"ax",%progbits
	.align	2
	.type	nm_ALERTS_restart_display_start_indicies, %function
nm_ALERTS_restart_display_start_indicies:
.LFB1:
	.loc 1 334 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #12
.LCFI5:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 337 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #3644]
	.loc 1 339 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L7
.L9:
	.loc 1 343 0
	ldr	r3, .L10
	ldr	r2, [r3, #0]
	ldr	r1, [fp, #-16]
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #1
	add	r3, r1, r3
	ldrh	r3, [r3, #0]
	ldr	r0, [fp, #-12]
	mov	r1, r2
	mov	r2, r3
	bl	ALERT_this_alert_is_to_be_displayed
	mov	r3, r0
	cmp	r3, #1
	bne	.L8
	.loc 1 345 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #3644]
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-8]
	mov	r2, r2, asl #1
	add	r2, r1, r2
	ldrh	r2, [r2, #0]
	ldr	r1, [fp, #-16]
	add	r3, r3, #908
	add	r3, r3, #2
	mov	r3, r3, asl #1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 347 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #3644]
	add	r2, r3, #1
	ldr	r3, [fp, #-16]
	str	r2, [r3, #3644]
.L8:
	.loc 1 339 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L7:
	.loc 1 339 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #28]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bhi	.L9
	.loc 1 350 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L11:
	.align	2
.L10:
	.word	g_ALERTS_filter_to_show
.LFE1:
	.size	nm_ALERTS_restart_display_start_indicies, .-nm_ALERTS_restart_display_start_indicies
	.section	.text.nm_ALERTS_refresh_all_start_indicies,"ax",%progbits
	.align	2
	.global	nm_ALERTS_refresh_all_start_indicies
	.type	nm_ALERTS_refresh_all_start_indicies, %function
nm_ALERTS_refresh_all_start_indicies:
.LFB2:
	.loc 1 367 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #20
.LCFI8:
	str	r0, [fp, #-20]
	.loc 1 374 0
	ldr	r0, [fp, #-20]
	bl	ALERTS_get_display_structure_for_pile
	str	r0, [fp, #-12]
	.loc 1 376 0
	ldr	r3, [fp, #-12]
	mov	r0, r3
	mov	r1, #0
	ldr	r2, .L16
	bl	memset
	.loc 1 379 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	cmp	r3, #0
	beq	.L12
	.loc 1 381 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #20]
	str	r3, [fp, #-16]
	.loc 1 383 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #28]
	str	r3, [fp, #-8]
	b	.L14
.L15:
	.loc 1 385 0 discriminator 2
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	ldr	r2, [fp, #-16]
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	ldr	r1, [fp, #-12]
	mov	r3, r3, asl #1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 387 0 discriminator 2
	ldr	r3, [fp, #-16]
	str	r3, [sp, #0]
	ldr	r0, [fp, #-20]
	mov	r1, #100
	mov	r2, #0
	mov	r3, #0
	bl	nm_ALERTS_parse_alert_and_return_length
	mov	r3, r0
	sub	r2, fp, #16
	ldr	r0, [fp, #-20]
	mov	r1, r2
	mov	r2, r3
	bl	ALERTS_inc_index
	.loc 1 383 0 discriminator 2
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
.L14:
	.loc 1 383 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L15
.L12:
	.loc 1 390 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	1820
.LFE2:
	.size	nm_ALERTS_refresh_all_start_indicies, .-nm_ALERTS_refresh_all_start_indicies
	.section .rodata
	.align	2
.LC1:
	.ascii	"0\000"
	.align	2
.LC2:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_alerts.c\000"
	.section	.text.nm_ALERTS_roll_and_add_new_display_start_index,"ax",%progbits
	.align	2
	.global	nm_ALERTS_roll_and_add_new_display_start_index
	.type	nm_ALERTS_roll_and_add_new_display_start_index, %function
nm_ALERTS_roll_and_add_new_display_start_index:
.LFB3:
	.loc 1 422 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #20
.LCFI11:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	.loc 1 431 0
	ldr	r3, [fp, #-20]
	ldr	r2, [r3, #3644]
	ldr	r3, [fp, #-16]
	ldr	r1, [r3, #16]
	ldr	r0, .L24
	mov	r3, #4
	mov	r1, r1, asl #3
	add	r1, r0, r1
	add	r3, r1, r3
	ldr	r1, [r3, #0]
	ldr	r3, .L24+4
	umull	r0, r3, r1, r3
	mov	r3, r3, lsr #1
	sub	r3, r3, #1
	cmp	r2, r3
	bls	.L19
	.loc 1 446 0
	ldr	r3, .L24+8
	mov	r2, #0
	str	r2, [r3, #32]
	.loc 1 450 0
	ldr	r3, [fp, #-20]
	mov	r2, #5
	str	r2, [r3, #3644]
	.loc 1 452 0
	ldr	r0, .L24+12
	ldr	r1, .L24+16
	mov	r2, #452
	bl	__assert
	b	.L20
.L19:
	.loc 1 454 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #3644]
	cmp	r3, #0
	beq	.L20
	.loc 1 460 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #3644]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 461 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #3644]
	str	r3, [fp, #-12]
.L22:
	.loc 1 465 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-8]
	add	r3, r3, #908
	add	r3, r3, #2
	mov	r3, r3, asl #1
	add	r3, r2, r3
	ldrh	r2, [r3, #0]
	ldr	r1, [fp, #-20]
	ldr	r3, [fp, #-12]
	add	r3, r3, #908
	add	r3, r3, #2
	mov	r3, r3, asl #1
	add	r3, r1, r3
	strh	r2, [r3, #0]	@ movhi
	.loc 1 468 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L23
.L21:
	.loc 1 474 0
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 475 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 477 0
	b	.L22
.L23:
	.loc 1 470 0
	mov	r0, r0	@ nop
.L20:
	.loc 1 481 0
	ldr	r3, [fp, #-24]
	mov	r3, r3, asl #16
	mov	r1, r3, lsr #16
	ldr	r2, [fp, #-20]
	ldr	r3, .L24+20
	strh	r1, [r2, r3]	@ movhi
	.loc 1 486 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #3644]
	add	r2, r3, #1
	ldr	r3, [fp, #-20]
	str	r2, [r3, #3644]
	.loc 1 487 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L25:
	.align	2
.L24:
	.word	alert_piles
	.word	954437177
	.word	alerts_struct_engineering
	.word	.LC1
	.word	.LC2
	.word	1820
.LFE3:
	.size	nm_ALERTS_roll_and_add_new_display_start_index, .-nm_ALERTS_roll_and_add_new_display_start_index
	.section	.text.nm_ALERTS_change_filter,"ax",%progbits
	.align	2
	.global	nm_ALERTS_change_filter
	.type	nm_ALERTS_change_filter, %function
nm_ALERTS_change_filter:
.LFB4:
	.loc 1 572 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #8
.LCFI14:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 573 0
	ldr	r3, .L28
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 575 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L26
	.loc 1 577 0
	ldr	r0, .L28+4
	ldr	r1, .L28+8
	bl	nm_ALERTS_restart_display_start_indicies
	.loc 1 580 0
	ldr	r3, .L28+8
	mov	r2, #1
	str	r2, [r3, #3652]
.L26:
	.loc 1 582 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L29:
	.align	2
.L28:
	.word	g_ALERTS_filter_to_show
	.word	alerts_struct_user
	.word	alerts_display_struct_user
.LFE4:
	.size	nm_ALERTS_change_filter, .-nm_ALERTS_change_filter
	.section	.text.ALERT_this_alert_is_to_be_displayed,"ax",%progbits
	.align	2
	.global	ALERT_this_alert_is_to_be_displayed
	.type	ALERT_this_alert_is_to_be_displayed, %function
ALERT_this_alert_is_to_be_displayed:
.LFB5:
	.loc 1 609 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #28
.LCFI17:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	.loc 1 627 0
	ldr	r3, .L40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L31
	.loc 1 629 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 633 0
	sub	r2, fp, #14
	sub	r3, fp, #32
	ldr	r0, [fp, #-24]
	mov	r1, r2
	mov	r2, r3
	mov	r3, #2
	bl	ALERTS_pull_object_off_pile
	.loc 1 634 0
	ldrh	r3, [fp, #-14]
	str	r3, [fp, #-12]
	.loc 1 636 0
	sub	r2, fp, #20
	sub	r3, fp, #32
	ldr	r0, [fp, #-24]
	mov	r1, r2
	mov	r2, r3
	mov	r3, #6
	bl	ALERTS_pull_object_off_pile
	.loc 1 638 0
	ldr	r3, [fp, #-28]
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L39
.L37:
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
.L33:
	.loc 1 641 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 642 0
	b	.L38
.L34:
	.loc 1 645 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 646 0
	b	.L38
.L35:
	.loc 1 649 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 650 0
	mov	r0, r0	@ nop
	b	.L38
.L36:
	.loc 1 653 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 654 0
	b	.L39
.L31:
	.loc 1 659 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L38
.L39:
	.loc 1 654 0
	mov	r0, r0	@ nop
.L38:
	.loc 1 662 0
	ldr	r3, [fp, #-8]
	.loc 1 663 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L41:
	.align	2
.L40:
	.word	g_ALERTS_pile_to_show
.LFE5:
	.size	ALERT_this_alert_is_to_be_displayed, .-ALERT_this_alert_is_to_be_displayed
	.section .rodata
	.align	2
.LC3:
	.ascii	"Invalid alert pile to show: %d\000"
	.section	.text.ALERTS_get_currently_displayed_pile,"ax",%progbits
	.align	2
	.type	ALERTS_get_currently_displayed_pile, %function
ALERTS_get_currently_displayed_pile:
.LFB6:
	.loc 1 674 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #4
.LCFI20:
	.loc 1 677 0
	ldr	r3, .L48
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L43
	.loc 1 679 0
	ldr	r3, .L48+4
	str	r3, [fp, #-8]
	b	.L44
.L43:
	.loc 1 681 0
	ldr	r3, .L48
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L45
	.loc 1 683 0
	ldr	r3, .L48+8
	str	r3, [fp, #-8]
	b	.L44
.L45:
	.loc 1 685 0
	ldr	r3, .L48
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L46
	.loc 1 687 0
	ldr	r3, .L48+12
	str	r3, [fp, #-8]
	b	.L44
.L46:
	.loc 1 689 0
	ldr	r3, .L48
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bne	.L47
	.loc 1 691 0
	ldr	r3, .L48+16
	str	r3, [fp, #-8]
	b	.L44
.L47:
	.loc 1 695 0
	ldr	r3, .L48+4
	str	r3, [fp, #-8]
	.loc 1 697 0
	ldr	r3, .L48
	ldr	r3, [r3, #0]
	ldr	r0, .L48+20
	mov	r1, r3
	bl	Alert_Message_va
.L44:
	.loc 1 700 0
	ldr	r3, [fp, #-8]
	.loc 1 701 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L49:
	.align	2
.L48:
	.word	g_ALERTS_pile_to_show
	.word	alerts_struct_user
	.word	alerts_struct_changes
	.word	alerts_struct_tp_micro
	.word	alerts_struct_engineering
	.word	.LC3
.LFE6:
	.size	ALERTS_get_currently_displayed_pile, .-ALERTS_get_currently_displayed_pile
	.section .rodata
	.align	2
.LC4:
	.ascii	"Invalid pile\000"
	.section	.text.ALERTS_get_display_structure_for_pile,"ax",%progbits
	.align	2
	.global	ALERTS_get_display_structure_for_pile
	.type	ALERTS_get_display_structure_for_pile, %function
ALERTS_get_display_structure_for_pile:
.LFB7:
	.loc 1 705 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #8
.LCFI23:
	str	r0, [fp, #-12]
	.loc 1 708 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L56
	cmp	r2, r3
	bne	.L51
	.loc 1 710 0
	ldr	r3, .L56+4
	str	r3, [fp, #-8]
	b	.L52
.L51:
	.loc 1 712 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L56+8
	cmp	r2, r3
	bne	.L53
	.loc 1 714 0
	ldr	r3, .L56+12
	str	r3, [fp, #-8]
	b	.L52
.L53:
	.loc 1 716 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L56+16
	cmp	r2, r3
	bne	.L54
	.loc 1 718 0
	ldr	r3, .L56+20
	str	r3, [fp, #-8]
	b	.L52
.L54:
	.loc 1 720 0
	ldr	r2, [fp, #-12]
	ldr	r3, .L56+24
	cmp	r2, r3
	bne	.L55
	.loc 1 722 0
	ldr	r3, .L56+28
	str	r3, [fp, #-8]
	b	.L52
.L55:
	.loc 1 726 0
	ldr	r3, .L56+4
	str	r3, [fp, #-8]
	.loc 1 728 0
	ldr	r0, .L56+32
	bl	Alert_Message
.L52:
	.loc 1 731 0
	ldr	r3, [fp, #-8]
	.loc 1 732 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L57:
	.align	2
.L56:
	.word	alerts_struct_user
	.word	alerts_display_struct_user
	.word	alerts_struct_changes
	.word	alerts_display_struct_changes
	.word	alerts_struct_tp_micro
	.word	alerts_display_struct_tp_micro
	.word	alerts_struct_engineering
	.word	alerts_display_struct_engineering
	.word	.LC4
.LFE7:
	.size	ALERTS_get_display_structure_for_pile, .-ALERTS_get_display_structure_for_pile
	.section	.text.nm_ALERTS_draw_scroll_line,"ax",%progbits
	.align	2
	.type	nm_ALERTS_draw_scroll_line, %function
nm_ALERTS_draw_scroll_line:
.LFB8:
	.loc 1 754 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #16
.LCFI26:
	mov	r3, r0
	strh	r3, [fp, #-16]	@ movhi
	.loc 1 759 0
	bl	ALERTS_get_currently_displayed_pile
	str	r0, [fp, #-8]
	.loc 1 761 0
	ldr	r0, [fp, #-8]
	bl	ALERTS_get_display_structure_for_pile
	str	r0, [fp, #-12]
	.loc 1 763 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #3640]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r2, r3
	ldrsh	r3, [fp, #-16]
	add	r3, r2, r3
	ldr	r2, [fp, #-12]
	add	r3, r3, #908
	add	r3, r3, #2
	mov	r3, r3, asl #1
	add	r3, r2, r3
	ldrh	r3, [r3, #0]
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	mov	r1, #200
	ldr	r2, .L59
	mov	r3, #256
	bl	nm_ALERTS_parse_alert_and_return_length
	.loc 1 764 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L60:
	.align	2
.L59:
	.word	GuiVar_AlertString
.LFE8:
	.size	nm_ALERTS_draw_scroll_line, .-nm_ALERTS_draw_scroll_line
	.section	.text.FDTO_ALERTS_redraw_retaining_topline,"ax",%progbits
	.align	2
	.type	FDTO_ALERTS_redraw_retaining_topline, %function
FDTO_ALERTS_redraw_retaining_topline:
.LFB9:
	.loc 1 771 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #28
.LCFI29:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 775 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	bne	.L62
	.loc 1 777 0
	mov	r0, #0
	bl	Redraw_Screen
	b	.L61
.L62:
	.loc 1 781 0
	ldr	r3, [fp, #-24]
	cmp	r3, #1
	bne	.L64
	.loc 1 783 0
	ldr	r3, .L71
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L65
	.loc 1 785 0
	ldr	r3, [fp, #-16]
	and	r3, r3, #255
	mov	r0, r3
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	str	r3, [fp, #-8]
	.loc 1 787 0
	ldr	r3, [fp, #-16]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_GetTopLine
	mov	r3, r0
	str	r3, [fp, #-12]
	.loc 1 792 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bge	.L66
	.loc 1 794 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L66:
	.loc 1 800 0
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L67
	.loc 1 804 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	ble	.L67
	.loc 1 806 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L67:
	.loc 1 812 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bls	.L68
	.loc 1 814 0
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-8]
.L68:
	.loc 1 820 0
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L69
	.loc 1 820 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	add	r2, r3, #17
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bge	.L69
	.loc 1 822 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	b	.L70
.L69:
	.loc 1 827 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	ble	.L70
	.loc 1 827 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	add	r3, r2, r3
	mov	r2, r3
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L70
	.loc 1 829 0 is_stmt 1
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
.L70:
	.loc 1 832 0
	ldr	r3, [fp, #-16]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Close
	.loc 1 836 0
	ldr	r3, [fp, #-16]
	and	r3, r3, #255
	mov	r1, r3
	ldr	r3, [fp, #-20]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r0, [fp, #-12]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	str	r0, [sp, #0]
	mov	r0, r1
	ldr	r1, .L71+4
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	b	.L65
.L64:
	.loc 1 841 0
	ldr	r3, [fp, #-16]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Redraw
.L65:
	.loc 1 845 0
	bl	GuiLib_Refresh
.L61:
	.loc 1 847 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L72:
	.align	2
.L71:
	.word	g_DIALOG_ok_dialog_visible
	.word	nm_ALERTS_draw_scroll_line
.LFE9:
	.size	FDTO_ALERTS_redraw_retaining_topline, .-FDTO_ALERTS_redraw_retaining_topline
	.section	.text.FDTO_ALERTS_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_ALERTS_redraw_scrollbox
	.type	FDTO_ALERTS_redraw_scrollbox, %function
FDTO_ALERTS_redraw_scrollbox:
.LFB10:
	.loc 1 866 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #8
.LCFI32:
	.loc 1 871 0
	bl	ALERTS_get_currently_displayed_pile
	str	r0, [fp, #-8]
	.loc 1 873 0
	ldr	r0, [fp, #-8]
	bl	ALERTS_get_display_structure_for_pile
	str	r0, [fp, #-12]
	.loc 1 875 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L77+4
	ldr	r3, .L77+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 877 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #3652]
	cmp	r3, #1
	bne	.L74
	.loc 1 879 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	nm_ALERTS_restart_display_start_indicies
	.loc 1 881 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #3652]
	.loc 1 885 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #3640]
	.loc 1 887 0
	ldr	r3, [fp, #-12]
	mov	r2, #1
	str	r2, [r3, #3648]
.L74:
	.loc 1 890 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #3648]
	cmp	r3, #1
	bne	.L75
	.loc 1 893 0
	ldr	r3, .L77+12
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 895 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #3644]
	cmp	r3, #18
	bhi	.L76
	.loc 1 897 0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	.loc 1 898 0
	mov	r0, #1
	bl	FDTO_Redraw_Screen
	b	.L75
.L76:
	.loc 1 902 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #3644]
	ldr	r3, [fp, #-12]
	ldr	r1, [r3, #3644]
	ldr	r3, .L77+16
	ldr	r3, [r3, #0]
	cmp	r1, r3
	moveq	r3, #0
	movne	r3, #1
	mov	r0, #0
	mov	r1, r2
	mov	r2, r3
	mov	r3, #1
	bl	FDTO_ALERTS_redraw_retaining_topline
.L75:
	.loc 1 906 0
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 908 0
	bl	GuiLib_Refresh
	.loc 1 910 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #3644]
	ldr	r3, .L77+16
	str	r2, [r3, #0]
	.loc 1 912 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #3648]
	.loc 1 913 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L78:
	.align	2
.L77:
	.word	alerts_pile_recursive_MUTEX
	.word	.LC2
	.word	875
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	g_ALERTS_line_count
.LFE10:
	.size	FDTO_ALERTS_redraw_scrollbox, .-FDTO_ALERTS_redraw_scrollbox
	.section	.text.FDTO_ALERTS_draw_alerts,"ax",%progbits
	.align	2
	.global	FDTO_ALERTS_draw_alerts
	.type	FDTO_ALERTS_draw_alerts, %function
FDTO_ALERTS_draw_alerts:
.LFB11:
	.loc 1 931 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #16
.LCFI35:
	str	r0, [fp, #-20]
	.loc 1 944 0
	mov	r0, #74
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 948 0
	bl	ALERTS_get_currently_displayed_pile
	str	r0, [fp, #-12]
	.loc 1 950 0
	ldr	r0, [fp, #-12]
	bl	ALERTS_get_display_structure_for_pile
	str	r0, [fp, #-16]
	.loc 1 954 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L86
	cmp	r2, r3
	bne	.L80
	.loc 1 956 0
	mov	r3, #892
	str	r3, [fp, #-8]
	b	.L81
.L80:
	.loc 1 958 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L86+4
	cmp	r2, r3
	bne	.L82
	.loc 1 960 0
	ldr	r3, .L86+8
	str	r3, [fp, #-8]
	b	.L81
.L82:
	.loc 1 962 0
	ldr	r2, [fp, #-16]
	ldr	r3, .L86+12
	cmp	r2, r3
	bne	.L83
	.loc 1 964 0
	ldr	r3, .L86+16
	str	r3, [fp, #-8]
	b	.L81
.L83:
	.loc 1 968 0
	ldr	r3, .L86+20
	str	r3, [fp, #-8]
.L81:
	.loc 1 972 0
	ldr	r3, .L86+24
	ldrh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L84
	.loc 1 974 0
	ldr	r3, [fp, #-8]
	mov	r0, #108
	mov	r1, r3
	bl	FDTO_show_screen_name_in_title_bar
	b	.L85
.L84:
	.loc 1 978 0
	ldr	r3, [fp, #-8]
	mov	r0, #128
	mov	r1, r3
	bl	FDTO_show_screen_name_in_title_bar
.L85:
	.loc 1 984 0
	ldr	r3, .L86+28
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 986 0
	ldr	r3, .L86+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L86+36
	ldr	r3, .L86+40
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 995 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	str	r2, [r3, #3652]
	.loc 1 997 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	bl	nm_ALERTS_restart_display_start_indicies
	.loc 1 999 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #3652]
	.loc 1 1003 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #3640]
	.loc 1 1005 0
	ldr	r3, [fp, #-16]
	mov	r2, #1
	str	r2, [r3, #3648]
	.loc 1 1007 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #3644]
	ldr	r3, .L86+44
	str	r2, [r3, #0]
	.loc 1 1011 0
	ldr	r3, .L86+44
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-20]
	mov	r1, r3
	ldr	r2, .L86+48
	bl	FDTO_REPORTS_draw_report
	.loc 1 1015 0
	ldr	r3, .L86+32
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1017 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #3648]
	.loc 1 1018 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L87:
	.align	2
.L86:
	.word	alerts_display_struct_changes
	.word	alerts_display_struct_tp_micro
	.word	1075
	.word	alerts_display_struct_engineering
	.word	918
	.word	878
	.word	GuiLib_LanguageIndex
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	alerts_pile_recursive_MUTEX
	.word	.LC2
	.word	986
	.word	g_ALERTS_line_count
	.word	nm_ALERTS_draw_scroll_line
.LFE11:
	.size	FDTO_ALERTS_draw_alerts, .-FDTO_ALERTS_draw_alerts
	.section	.text.ALERTS_process_report,"ax",%progbits
	.align	2
	.global	ALERTS_process_report
	.type	ALERTS_process_report, %function
ALERTS_process_report:
.LFB12:
	.loc 1 1034 0
	@ args = 0, pretend = 0, frame = 284
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI36:
	add	fp, sp, #8
.LCFI37:
	sub	sp, sp, #288
.LCFI38:
	str	r0, [fp, #-292]
	str	r1, [fp, #-288]
	.loc 1 1050 0
	mov	r3, #79
	str	r3, [fp, #-16]
	.loc 1 1052 0
	bl	ALERTS_get_currently_displayed_pile
	str	r0, [fp, #-20]
	.loc 1 1054 0
	ldr	r0, [fp, #-20]
	bl	ALERTS_get_display_structure_for_pile
	str	r0, [fp, #-24]
	.loc 1 1056 0
	ldr	r3, [fp, #-292]
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L89
.L92:
	.word	.L90
	.word	.L91
	.word	.L89
	.word	.L91
	.word	.L90
.L91:
	.loc 1 1060 0
	ldr	r3, .L105
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L105+4
	ldr	r3, .L105+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 1062 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #3640]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r4, r3
	mov	r0, #0
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	add	r3, r4, r3
	ldr	r2, [fp, #-24]
	add	r3, r3, #908
	add	r3, r3, #2
	mov	r3, r3, asl #1
	add	r3, r2, r3
	ldrh	r3, [r3, #0]
	mov	r2, r3
	sub	r3, fp, #284
	str	r2, [sp, #0]
	ldr	r0, [fp, #-20]
	mov	r1, #200
	mov	r2, r3
	mov	r3, #256
	bl	nm_ALERTS_parse_alert_and_return_length
	.loc 1 1064 0
	ldr	r3, .L105+12
	ldr	r3, [r3, #4]
	sub	r2, fp, #284
	mov	r0, r2
	mov	r1, r3
	mov	r2, #2
	bl	GuiLib_GetTextWidth
	mov	r3, r0
	mov	r2, r3
	ldr	r3, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-28]
	.loc 1 1066 0
	ldr	r3, .L105+16
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-28]
	cmp	r2, r3
	bls	.L93
	.loc 1 1068 0
	ldr	r3, [fp, #-292]
	cmp	r3, #1
	bne	.L94
	.loc 1 1068 0 is_stmt 0 discriminator 1
	ldr	r3, .L105+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	blt	.L95
.L94:
	.loc 1 1069 0 is_stmt 1 discriminator 2
	ldr	r3, [fp, #-292]
	.loc 1 1068 0 discriminator 2
	cmp	r3, #3
	bne	.L96
	.loc 1 1069 0
	ldr	r3, .L105+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	abs
	mov	r2, r0
	ldr	r3, .L105+16
	ldr	r3, [r3, #0]
	ldr	r1, [fp, #-28]
	rsb	r3, r3, r1
	cmp	r2, r3
	bge	.L96
.L95:
	.loc 1 1071 0
	bl	good_key_beep
	.loc 1 1073 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L97
.L101:
	.loc 1 1075 0
	ldr	r3, [fp, #-292]
	cmp	r3, #1
	bne	.L98
	.loc 1 1077 0
	ldr	r3, .L105+20
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L105+20
	str	r2, [r3, #0]
	b	.L99
.L98:
	.loc 1 1081 0
	ldr	r3, .L105+20
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L105+20
	str	r2, [r3, #0]
.L99:
	.loc 1 1073 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L97:
	.loc 1 1073 0 is_stmt 0 discriminator 1
	ldr	r3, .L105+20
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #0
	bge	.L100
	.loc 1 1073 0 discriminator 2
	ldr	r3, [fp, #-12]
	cmp	r3, #3
	bls	.L101
.L100:
	.loc 1 1085 0 is_stmt 1
	mov	r0, #0
	bl	SCROLL_BOX_redraw
	b	.L103
.L96:
	.loc 1 1090 0
	bl	bad_key_beep
	b	.L103
.L93:
	.loc 1 1096 0
	bl	bad_key_beep
.L103:
	.loc 1 1099 0
	ldr	r3, .L105
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 1100 0
	b	.L88
.L90:
	.loc 1 1106 0
	ldr	r3, .L105+20
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 1108 0
	sub	r1, fp, #292
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #0
	bl	REPORTS_process_report
	.loc 1 1109 0
	b	.L88
.L89:
	.loc 1 1121 0
	sub	r1, fp, #292
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #0
	bl	REPORTS_process_report
.L88:
	.loc 1 1123 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L106:
	.align	2
.L105:
	.word	alerts_pile_recursive_MUTEX
	.word	.LC2
	.word	1060
	.word	GuiFont_FontList
	.word	MAX_ALERT_PIXELS.8365
	.word	GuiVar_ScrollBoxHorizScrollPos
.LFE12:
	.size	ALERTS_process_report, .-ALERTS_process_report
	.section	.rodata.MAX_ALERT_PIXELS.8365,"a",%progbits
	.align	2
	.type	MAX_ALERT_PIXELS.8365, %object
	.size	MAX_ALERT_PIXELS.8365, 4
MAX_ALERT_PIXELS.8365:
	.word	307
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/alerts/alerts.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_alerts.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/dialog.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xc55
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF138
	.byte	0x1
	.4byte	.LASF139
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x2
	.byte	0x3a
	.4byte	0x53
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF5
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x2
	.byte	0x4c
	.4byte	0x6c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x2
	.byte	0x55
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x2
	.byte	0x5e
	.4byte	0x90
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x2
	.byte	0x67
	.4byte	0x2c
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF14
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x2
	.byte	0x99
	.4byte	0x90
	.uleb128 0x5
	.byte	0x6
	.byte	0x3
	.byte	0x22
	.4byte	0xd5
	.uleb128 0x6
	.ascii	"T\000"
	.byte	0x3
	.byte	0x24
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.ascii	"D\000"
	.byte	0x3
	.byte	0x26
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x3
	.byte	0x28
	.4byte	0xb4
	.uleb128 0x7
	.byte	0x4
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x4
	.byte	0x35
	.4byte	0x25
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x5
	.byte	0x57
	.4byte	0xe0
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x6
	.byte	0x4c
	.4byte	0xed
	.uleb128 0x5
	.byte	0x18
	.byte	0x7
	.byte	0xb6
	.4byte	0x1fa
	.uleb128 0x8
	.4byte	.LASF20
	.byte	0x7
	.byte	0xba
	.4byte	0x1fa
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF21
	.byte	0x7
	.byte	0xbb
	.4byte	0x1fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x8
	.4byte	.LASF22
	.byte	0x7
	.byte	0xbc
	.4byte	0x20a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x8
	.4byte	.LASF23
	.byte	0x7
	.byte	0xbd
	.4byte	0x6c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x8
	.4byte	.LASF24
	.byte	0x7
	.byte	0xc2
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x8
	.4byte	.LASF25
	.byte	0x7
	.byte	0xc2
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0xb
	.uleb128 0x8
	.4byte	.LASF26
	.byte	0x7
	.byte	0xc6
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x8
	.4byte	.LASF27
	.byte	0x7
	.byte	0xc6
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0x8
	.4byte	.LASF28
	.byte	0x7
	.byte	0xc6
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x8
	.4byte	.LASF29
	.byte	0x7
	.byte	0xc7
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0xf
	.uleb128 0x8
	.4byte	.LASF30
	.byte	0x7
	.byte	0xc7
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x8
	.4byte	.LASF31
	.byte	0x7
	.byte	0xc8
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.uleb128 0x8
	.4byte	.LASF32
	.byte	0x7
	.byte	0xc8
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0x8
	.4byte	.LASF33
	.byte	0x7
	.byte	0xc9
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.uleb128 0x8
	.4byte	.LASF34
	.byte	0x7
	.byte	0xca
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x8
	.4byte	.LASF35
	.byte	0x7
	.byte	0xcb
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x15
	.uleb128 0x8
	.4byte	.LASF36
	.byte	0x7
	.byte	0xcc
	.4byte	0x53
	.byte	0x2
	.byte	0x23
	.uleb128 0x16
	.byte	0
	.uleb128 0x9
	.4byte	0x53
	.4byte	0x20a
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x9
	.4byte	0x6c
	.4byte	0x21a
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x4
	.4byte	.LASF37
	.byte	0x7
	.byte	0xce
	.4byte	0x103
	.uleb128 0x4
	.4byte	.LASF38
	.byte	0x7
	.byte	0xd8
	.4byte	0x230
	.uleb128 0xb
	.byte	0x4
	.4byte	0x21a
	.uleb128 0x9
	.4byte	0x85
	.4byte	0x246
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x9
	.4byte	0x41
	.4byte	0x256
	.uleb128 0xa
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.4byte	0x41
	.uleb128 0x5
	.byte	0x8
	.byte	0x8
	.byte	0x7c
	.4byte	0x281
	.uleb128 0x8
	.4byte	.LASF39
	.byte	0x8
	.byte	0x7e
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF40
	.byte	0x8
	.byte	0x80
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF41
	.byte	0x8
	.byte	0x82
	.4byte	0x25c
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF42
	.uleb128 0x9
	.4byte	0x85
	.4byte	0x2a3
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x5
	.byte	0x34
	.byte	0x9
	.byte	0x58
	.4byte	0x338
	.uleb128 0x8
	.4byte	.LASF43
	.byte	0x9
	.byte	0x68
	.4byte	0x246
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF44
	.byte	0x9
	.byte	0x6d
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x8
	.4byte	.LASF45
	.byte	0x9
	.byte	0x74
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x8
	.4byte	.LASF46
	.byte	0x9
	.byte	0x77
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x8
	.4byte	.LASF47
	.byte	0x9
	.byte	0x7b
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x8
	.4byte	.LASF48
	.byte	0x9
	.byte	0x81
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x8
	.4byte	.LASF49
	.byte	0x9
	.byte	0x8a
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x8
	.4byte	.LASF50
	.byte	0x9
	.byte	0x8f
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x8
	.4byte	.LASF51
	.byte	0x9
	.byte	0x9b
	.4byte	0xa9
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x8
	.4byte	.LASF52
	.byte	0x9
	.byte	0xa2
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.byte	0
	.uleb128 0x4
	.4byte	.LASF53
	.byte	0x9
	.byte	0xaa
	.4byte	0x2a3
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF54
	.uleb128 0x5
	.byte	0x8
	.byte	0xa
	.byte	0x31
	.4byte	0x36f
	.uleb128 0x8
	.4byte	.LASF55
	.byte	0xa
	.byte	0x34
	.4byte	0xe0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF56
	.byte	0xa
	.byte	0x38
	.4byte	0x85
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF57
	.byte	0xa
	.byte	0x3a
	.4byte	0x34a
	.uleb128 0xc
	.2byte	0xe48
	.byte	0xb
	.byte	0x4f
	.4byte	0x3dd
	.uleb128 0x8
	.4byte	.LASF58
	.byte	0xb
	.byte	0x56
	.4byte	0x3dd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF59
	.byte	0xb
	.byte	0x60
	.4byte	0x3dd
	.byte	0x3
	.byte	0x23
	.uleb128 0x71c
	.uleb128 0x8
	.4byte	.LASF60
	.byte	0xb
	.byte	0x63
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0xe38
	.uleb128 0x8
	.4byte	.LASF61
	.byte	0xb
	.byte	0x66
	.4byte	0x85
	.byte	0x3
	.byte	0x23
	.uleb128 0xe3c
	.uleb128 0x8
	.4byte	.LASF62
	.byte	0xb
	.byte	0x69
	.4byte	0xa9
	.byte	0x3
	.byte	0x23
	.uleb128 0xe40
	.uleb128 0x8
	.4byte	.LASF63
	.byte	0xb
	.byte	0x6f
	.4byte	0xa9
	.byte	0x3
	.byte	0x23
	.uleb128 0xe44
	.byte	0
	.uleb128 0x9
	.4byte	0x61
	.4byte	0x3ee
	.uleb128 0xd
	.4byte	0x25
	.2byte	0x38d
	.byte	0
	.uleb128 0x4
	.4byte	.LASF64
	.byte	0xb
	.byte	0x71
	.4byte	0x37a
	.uleb128 0xe
	.4byte	0x73
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF88
	.byte	0x1
	.byte	0xe5
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x4ef
	.uleb128 0x10
	.4byte	.LASF65
	.byte	0x1
	.byte	0xe5
	.4byte	0x4ef
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x10
	.4byte	.LASF66
	.byte	0x1
	.byte	0xe5
	.4byte	0x4fa
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x10
	.4byte	.LASF67
	.byte	0x1
	.byte	0xe5
	.4byte	0x256
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x10
	.4byte	.LASF68
	.byte	0x1
	.byte	0xe5
	.4byte	0x4fa
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x10
	.4byte	.LASF69
	.byte	0x1
	.byte	0xe5
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x11
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xe7
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF70
	.byte	0x1
	.byte	0xee
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -18
	.uleb128 0x12
	.4byte	.LASF71
	.byte	0x1
	.byte	0xf0
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.ascii	"ldt\000"
	.byte	0x1
	.byte	0xf4
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF72
	.byte	0x1
	.byte	0xf7
	.4byte	0x246
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x13
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x14
	.4byte	.LASF73
	.byte	0x1
	.2byte	0x114
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x14
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x114
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x14
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x114
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x14
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x114
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.byte	0
	.byte	0
	.uleb128 0xe
	.4byte	0x4f4
	.uleb128 0xb
	.byte	0x4
	.4byte	0x338
	.uleb128 0xe
	.4byte	0x85
	.uleb128 0x15
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x14d
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x544
	.uleb128 0x16
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x14d
	.4byte	0x4ef
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x16
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x14d
	.4byte	0x544
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x14f
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x549
	.uleb128 0xb
	.byte	0x4
	.4byte	0x3ee
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x16e
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x5a4
	.uleb128 0x16
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x16e
	.4byte	0x4ef
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x170
	.4byte	0x549
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x172
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x17
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x174
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x1a5
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x60a
	.uleb128 0x16
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x1a5
	.4byte	0x4ef
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x1a5
	.4byte	0x544
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x1a5
	.4byte	0x4fa
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x14
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x23b
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x643
	.uleb128 0x16
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x23b
	.4byte	0x4fa
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x23b
	.4byte	0x643
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xe
	.4byte	0xa9
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x260
	.byte	0x1
	.4byte	0xa9
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x6cf
	.uleb128 0x16
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x260
	.4byte	0x4ef
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x16
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x260
	.4byte	0x4fa
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x16
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x260
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x14
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x267
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -18
	.uleb128 0x14
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x269
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x26f
	.4byte	0xd5
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x17
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x271
	.4byte	0xa9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF140
	.byte	0x1
	.2byte	0x2a1
	.byte	0x1
	.4byte	0x4f4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x6fc
	.uleb128 0x14
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x2a3
	.4byte	0x4f4
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.byte	0x1
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x2c0
	.byte	0x1
	.4byte	0x549
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x739
	.uleb128 0x16
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x2c0
	.4byte	0x739
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x2c2
	.4byte	0x549
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x73e
	.uleb128 0xb
	.byte	0x4
	.4byte	0x744
	.uleb128 0xe
	.4byte	0x338
	.uleb128 0x15
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x2f1
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x790
	.uleb128 0x16
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x2f1
	.4byte	0x3f9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x2f3
	.4byte	0x4f4
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x2f5
	.4byte	0x549
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x15
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x2ff
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x804
	.uleb128 0x16
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x2ff
	.4byte	0x4fa
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x300
	.4byte	0x4fa
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x301
	.4byte	0x643
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x16
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x302
	.4byte	0x643
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x14
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x304
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x305
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x361
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x83d
	.uleb128 0x14
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x363
	.4byte	0x4f4
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x365
	.4byte	0x549
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x3a2
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x894
	.uleb128 0x16
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x3a2
	.4byte	0x643
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x3a8
	.4byte	0x4f4
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x3aa
	.4byte	0x549
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x3ac
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x409
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0x92a
	.uleb128 0x16
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x409
	.4byte	0x281
	.byte	0x3
	.byte	0x91
	.sleb128 -296
	.uleb128 0x14
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x40b
	.4byte	0x4fa
	.byte	0x5
	.byte	0x3
	.4byte	MAX_ALERT_PIXELS.8365
	.uleb128 0x14
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x40d
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x14
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x40f
	.4byte	0x4f4
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x411
	.4byte	0x549
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x14
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x413
	.4byte	0x92a
	.byte	0x3
	.byte	0x91
	.sleb128 -288
	.uleb128 0x14
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x415
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x17
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x417
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x9
	.4byte	0x41
	.4byte	0x93a
	.uleb128 0xa
	.4byte	0x25
	.byte	0xff
	.byte	0
	.uleb128 0x9
	.4byte	0x41
	.4byte	0x94a
	.uleb128 0xa
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF112
	.byte	0xc
	.2byte	0x10d
	.4byte	0x93a
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x41
	.4byte	0x969
	.uleb128 0xd
	.4byte	0x25
	.2byte	0x100
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF113
	.byte	0xc
	.2byte	0x10e
	.4byte	0x958
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x41
	.4byte	0x987
	.uleb128 0xa
	.4byte	0x25
	.byte	0x9
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF114
	.byte	0xc
	.2byte	0x10f
	.4byte	0x977
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF115
	.byte	0xc
	.2byte	0x3ca
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF116
	.byte	0x7
	.2byte	0x12e
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x225
	.4byte	0x9c1
	.uleb128 0xa
	.4byte	0x25
	.byte	0x6
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF117
	.byte	0xd
	.byte	0x2e
	.4byte	0x9ce
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0x9b1
	.uleb128 0x12
	.4byte	.LASF118
	.byte	0xd
	.byte	0x30
	.4byte	0x9e4
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0x1fa
	.uleb128 0x12
	.4byte	.LASF119
	.byte	0xd
	.byte	0x34
	.4byte	0x9fa
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0x1fa
	.uleb128 0x12
	.4byte	.LASF120
	.byte	0xd
	.byte	0x36
	.4byte	0xa10
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0x1fa
	.uleb128 0x12
	.4byte	.LASF121
	.byte	0xd
	.byte	0x38
	.4byte	0xa26
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0x1fa
	.uleb128 0x12
	.4byte	.LASF122
	.byte	0xe
	.byte	0x33
	.4byte	0xa3c
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0xe
	.4byte	0x236
	.uleb128 0x12
	.4byte	.LASF123
	.byte	0xe
	.byte	0x3f
	.4byte	0xa52
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0xe
	.4byte	0x293
	.uleb128 0x1c
	.4byte	.LASF124
	.byte	0x9
	.byte	0xad
	.4byte	0x338
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF125
	.byte	0x9
	.byte	0xae
	.4byte	0x338
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF126
	.byte	0x9
	.byte	0xaf
	.4byte	0x338
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF127
	.byte	0x9
	.byte	0xb0
	.4byte	0x338
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x36f
	.4byte	0xa9b
	.uleb128 0xa
	.4byte	0x25
	.byte	0x4
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF128
	.byte	0xa
	.byte	0x3d
	.4byte	0xaa8
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0xa8b
	.uleb128 0x1c
	.4byte	.LASF129
	.byte	0xb
	.byte	0x76
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF130
	.byte	0xb
	.byte	0x78
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF131
	.byte	0xb
	.byte	0x7a
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF132
	.byte	0xb
	.byte	0x7f
	.4byte	0x3ee
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF133
	.byte	0xb
	.byte	0x81
	.4byte	0x3ee
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF134
	.byte	0xb
	.byte	0x83
	.4byte	0x3ee
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF135
	.byte	0xb
	.byte	0x85
	.4byte	0x3ee
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF136
	.byte	0xf
	.byte	0x6b
	.4byte	0xf8
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF137
	.byte	0x10
	.byte	0x33
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF112
	.byte	0xc
	.2byte	0x10d
	.4byte	0x93a
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF113
	.byte	0xc
	.2byte	0x10e
	.4byte	0x958
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF114
	.byte	0xc
	.2byte	0x10f
	.4byte	0x977
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF115
	.byte	0xc
	.2byte	0x3ca
	.4byte	0x2c
	.byte	0x1
	.byte	0x1
	.uleb128 0x1b
	.4byte	.LASF116
	.byte	0x7
	.2byte	0x12e
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF117
	.byte	0xd
	.byte	0x2e
	.4byte	0xb75
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0x9b1
	.uleb128 0x1c
	.4byte	.LASF124
	.byte	0x9
	.byte	0xad
	.4byte	0x338
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF125
	.byte	0x9
	.byte	0xae
	.4byte	0x338
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF126
	.byte	0x9
	.byte	0xaf
	.4byte	0x338
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF127
	.byte	0x9
	.byte	0xb0
	.4byte	0x338
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF128
	.byte	0xa
	.byte	0x3d
	.4byte	0xbbb
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0xa8b
	.uleb128 0x1d
	.4byte	.LASF129
	.byte	0x1
	.byte	0x41
	.4byte	0x85
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_ALERTS_line_count
	.uleb128 0x1d
	.4byte	.LASF130
	.byte	0x1
	.byte	0x43
	.4byte	0x85
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_ALERTS_pile_to_show
	.uleb128 0x1d
	.4byte	.LASF131
	.byte	0x1
	.byte	0x45
	.4byte	0x85
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_ALERTS_filter_to_show
	.uleb128 0x1d
	.4byte	.LASF132
	.byte	0x1
	.byte	0x4c
	.4byte	0x3ee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	alerts_display_struct_user
	.uleb128 0x1d
	.4byte	.LASF133
	.byte	0x1
	.byte	0x4e
	.4byte	0x3ee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	alerts_display_struct_changes
	.uleb128 0x1d
	.4byte	.LASF134
	.byte	0x1
	.byte	0x50
	.4byte	0x3ee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	alerts_display_struct_tp_micro
	.uleb128 0x1d
	.4byte	.LASF135
	.byte	0x1
	.byte	0x52
	.4byte	0x3ee
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	alerts_display_struct_engineering
	.uleb128 0x1c
	.4byte	.LASF136
	.byte	0xf
	.byte	0x6b
	.4byte	0xf8
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF137
	.byte	0x10
	.byte	0x33
	.4byte	0x85
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x7c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF138:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF47:
	.ascii	"count\000"
.LASF126:
	.ascii	"alerts_struct_tp_micro\000"
.LASF87:
	.ascii	"ptriggered_by_key_press\000"
.LASF66:
	.ascii	"pparse_why\000"
.LASF62:
	.ascii	"reparse\000"
.LASF57:
	.ascii	"ALERT_DEFS\000"
.LASF67:
	.ascii	"pdest_ptr\000"
.LASF120:
	.ascii	"GuiFont_DecimalChar\000"
.LASF89:
	.ascii	"ALERT_this_alert_is_to_be_displayed\000"
.LASF104:
	.ascii	"pcomplete_redraw\000"
.LASF92:
	.ascii	"nm_ALERTS_restart_display_start_indicies\000"
.LASF124:
	.ascii	"alerts_struct_user\000"
.LASF15:
	.ascii	"BOOL_32\000"
.LASF133:
	.ascii	"alerts_display_struct_changes\000"
.LASF55:
	.ascii	"addr\000"
.LASF139:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_alerts.c\000"
.LASF116:
	.ascii	"GuiLib_LanguageIndex\000"
.LASF46:
	.ascii	"next\000"
.LASF11:
	.ascii	"UNS_32\000"
.LASF39:
	.ascii	"keycode\000"
.LASF99:
	.ascii	"pretain_active_line\000"
.LASF16:
	.ascii	"DATE_TIME\000"
.LASF2:
	.ascii	"long long int\000"
.LASF5:
	.ascii	"signed char\000"
.LASF140:
	.ascii	"ALERTS_get_currently_displayed_pile\000"
.LASF101:
	.ascii	"ltop_line\000"
.LASF21:
	.ascii	"LastChar\000"
.LASF14:
	.ascii	"long long unsigned int\000"
.LASF9:
	.ascii	"INT_16\000"
.LASF60:
	.ascii	"display_index_of_first_line\000"
.LASF135:
	.ascii	"alerts_display_struct_engineering\000"
.LASF88:
	.ascii	"nm_ALERTS_parse_alert_and_return_length\000"
.LASF83:
	.ascii	"lfrom_index\000"
.LASF61:
	.ascii	"display_total\000"
.LASF1:
	.ascii	"long int\000"
.LASF44:
	.ascii	"alerts_pile_index\000"
.LASF53:
	.ascii	"ALERTS_PILE_STRUCT\000"
.LASF107:
	.ascii	"pkey_event\000"
.LASF50:
	.ascii	"pending_first_to_send\000"
.LASF98:
	.ascii	"predraw_entire_scrollbox\000"
.LASF17:
	.ascii	"portTickType\000"
.LASF136:
	.ascii	"alerts_pile_recursive_MUTEX\000"
.LASF109:
	.ascii	"Y_POS_OF_ALERT_STRING\000"
.LASF64:
	.ascii	"ALERTS_DISPLAY_STRUCT\000"
.LASF121:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF36:
	.ascii	"LineSize\000"
.LASF78:
	.ascii	"ldisplay_struct\000"
.LASF25:
	.ascii	"YSize\000"
.LASF93:
	.ascii	"nm_ALERTS_draw_scroll_line\000"
.LASF115:
	.ascii	"GuiVar_ScrollBoxHorizScrollPos\000"
.LASF108:
	.ascii	"MAX_ALERT_PIXELS\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF86:
	.ascii	"pfilter\000"
.LASF103:
	.ascii	"FDTO_ALERTS_draw_alerts\000"
.LASF76:
	.ascii	"ldow\000"
.LASF70:
	.ascii	"aid_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF48:
	.ascii	"ready_to_use_bool\000"
.LASF8:
	.ascii	"short unsigned int\000"
.LASF26:
	.ascii	"TopLine\000"
.LASF84:
	.ascii	"lto_index\000"
.LASF74:
	.ascii	"lmonth\000"
.LASF90:
	.ascii	"lpile\000"
.LASF132:
	.ascii	"alerts_display_struct_user\000"
.LASF18:
	.ascii	"xQueueHandle\000"
.LASF119:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF91:
	.ascii	"ALERTS_get_display_structure_for_pile\000"
.LASF85:
	.ascii	"nm_ALERTS_change_filter\000"
.LASF111:
	.ascii	"ltext_width\000"
.LASF33:
	.ascii	"BoxWidth\000"
.LASF56:
	.ascii	"pile_size\000"
.LASF117:
	.ascii	"GuiFont_FontList\000"
.LASF73:
	.ascii	"lday\000"
.LASF6:
	.ascii	"UNS_8\000"
.LASF134:
	.ascii	"alerts_display_struct_tp_micro\000"
.LASF105:
	.ascii	"ltext_to_draw\000"
.LASF41:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF79:
	.ascii	"a_index\000"
.LASF59:
	.ascii	"display_start_indicies\000"
.LASF113:
	.ascii	"GuiVar_AlertString\000"
.LASF42:
	.ascii	"float\000"
.LASF94:
	.ascii	"pline_index_0_i16\000"
.LASF118:
	.ascii	"GuiFont_LanguageActive\000"
.LASF71:
	.ascii	"aid_32\000"
.LASF128:
	.ascii	"alert_piles\000"
.LASF19:
	.ascii	"xSemaphoreHandle\000"
.LASF52:
	.ascii	"ci_duration_ms\000"
.LASF4:
	.ascii	"unsigned char\000"
.LASF63:
	.ascii	"regenerate_displaystartindicies\000"
.LASF69:
	.ascii	"pindex\000"
.LASF28:
	.ascii	"BaseLine\000"
.LASF10:
	.ascii	"short int\000"
.LASF13:
	.ascii	"INT_32\000"
.LASF110:
	.ascii	"str_256\000"
.LASF34:
	.ascii	"PsNumWidth\000"
.LASF68:
	.ascii	"pallowable_size\000"
.LASF49:
	.ascii	"first_to_send\000"
.LASF97:
	.ascii	"pnumber_of_lines\000"
.LASF127:
	.ascii	"alerts_struct_engineering\000"
.LASF54:
	.ascii	"double\000"
.LASF75:
	.ascii	"lyear\000"
.LASF45:
	.ascii	"first\000"
.LASF3:
	.ascii	"char\000"
.LASF100:
	.ascii	"lactive_line\000"
.LASF82:
	.ascii	"pstart_index\000"
.LASF22:
	.ascii	"FirstCharNdx\000"
.LASF129:
	.ascii	"g_ALERTS_line_count\000"
.LASF51:
	.ascii	"pending_first_to_send_in_use\000"
.LASF40:
	.ascii	"repeats\000"
.LASF123:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF23:
	.ascii	"IllegalCharNdx\000"
.LASF77:
	.ascii	"pdisplay_struct\000"
.LASF114:
	.ascii	"GuiVar_AlertTime\000"
.LASF137:
	.ascii	"g_DIALOG_ok_dialog_visible\000"
.LASF7:
	.ascii	"UNS_16\000"
.LASF96:
	.ascii	"pscrollbox_index\000"
.LASF38:
	.ascii	"GuiLib_FontRecPtr\000"
.LASF58:
	.ascii	"all_start_indicies\000"
.LASF24:
	.ascii	"XSize\000"
.LASF31:
	.ascii	"Underline1\000"
.LASF32:
	.ascii	"Underline2\000"
.LASF95:
	.ascii	"FDTO_ALERTS_redraw_retaining_topline\000"
.LASF27:
	.ascii	"MidLine\000"
.LASF102:
	.ascii	"FDTO_ALERTS_redraw_scrollbox\000"
.LASF125:
	.ascii	"alerts_struct_changes\000"
.LASF20:
	.ascii	"FirstChar\000"
.LASF35:
	.ascii	"PsSpace\000"
.LASF65:
	.ascii	"ppile\000"
.LASF106:
	.ascii	"ALERTS_process_report\000"
.LASF43:
	.ascii	"verify_string_pre\000"
.LASF131:
	.ascii	"g_ALERTS_filter_to_show\000"
.LASF122:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF80:
	.ascii	"nm_ALERTS_refresh_all_start_indicies\000"
.LASF29:
	.ascii	"Cursor1\000"
.LASF30:
	.ascii	"Cursor2\000"
.LASF130:
	.ascii	"g_ALERTS_pile_to_show\000"
.LASF72:
	.ascii	"dt_buf\000"
.LASF112:
	.ascii	"GuiVar_AlertDate\000"
.LASF37:
	.ascii	"GuiLib_FontRec\000"
.LASF81:
	.ascii	"nm_ALERTS_roll_and_add_new_display_start_index\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
