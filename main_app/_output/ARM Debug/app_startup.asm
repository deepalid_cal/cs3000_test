	.file	"app_startup.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	debugio_MUTEX
	.section	.bss.debugio_MUTEX,"aw",%nobits
	.align	2
	.type	debugio_MUTEX, %object
	.size	debugio_MUTEX, 4
debugio_MUTEX:
	.space	4
	.global	startup_rtc_task_sync_semaphore
	.section	.bss.startup_rtc_task_sync_semaphore,"aw",%nobits
	.align	2
	.type	startup_rtc_task_sync_semaphore, %object
	.size	startup_rtc_task_sync_semaphore, 4
startup_rtc_task_sync_semaphore:
	.space	4
	.global	alerts_pile_recursive_MUTEX
	.section	.bss.alerts_pile_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	alerts_pile_recursive_MUTEX, %object
	.size	alerts_pile_recursive_MUTEX, 4
alerts_pile_recursive_MUTEX:
	.space	4
	.global	Flash_0_MUTEX
	.section	.bss.Flash_0_MUTEX,"aw",%nobits
	.align	2
	.type	Flash_0_MUTEX, %object
	.size	Flash_0_MUTEX, 4
Flash_0_MUTEX:
	.space	4
	.global	Flash_1_MUTEX
	.section	.bss.Flash_1_MUTEX,"aw",%nobits
	.align	2
	.type	Flash_1_MUTEX, %object
	.size	Flash_1_MUTEX, 4
Flash_1_MUTEX:
	.space	4
	.global	rcvd_data_binary_semaphore
	.section	.bss.rcvd_data_binary_semaphore,"aw",%nobits
	.align	2
	.type	rcvd_data_binary_semaphore, %object
	.size	rcvd_data_binary_semaphore, 20
rcvd_data_binary_semaphore:
	.space	20
	.global	list_program_data_recursive_MUTEX
	.section	.bss.list_program_data_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	list_program_data_recursive_MUTEX, %object
	.size	list_program_data_recursive_MUTEX, 4
list_program_data_recursive_MUTEX:
	.space	4
	.global	list_tpl_in_messages_MUTEX
	.section	.bss.list_tpl_in_messages_MUTEX,"aw",%nobits
	.align	2
	.type	list_tpl_in_messages_MUTEX, %object
	.size	list_tpl_in_messages_MUTEX, 4
list_tpl_in_messages_MUTEX:
	.space	4
	.global	list_tpl_out_messages_MUTEX
	.section	.bss.list_tpl_out_messages_MUTEX,"aw",%nobits
	.align	2
	.type	list_tpl_out_messages_MUTEX, %object
	.size	list_tpl_out_messages_MUTEX, 4
list_tpl_out_messages_MUTEX:
	.space	4
	.global	list_incoming_messages_recursive_MUTEX
	.section	.bss.list_incoming_messages_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	list_incoming_messages_recursive_MUTEX, %object
	.size	list_incoming_messages_recursive_MUTEX, 4
list_incoming_messages_recursive_MUTEX:
	.space	4
	.global	list_packets_waiting_for_token_MUTEX
	.section	.bss.list_packets_waiting_for_token_MUTEX,"aw",%nobits
	.align	2
	.type	list_packets_waiting_for_token_MUTEX, %object
	.size	list_packets_waiting_for_token_MUTEX, 4
list_packets_waiting_for_token_MUTEX:
	.space	4
	.global	xmit_list_MUTEX
	.section	.bss.xmit_list_MUTEX,"aw",%nobits
	.align	2
	.type	xmit_list_MUTEX, %object
	.size	xmit_list_MUTEX, 20
xmit_list_MUTEX:
	.space	20
	.global	ci_and_comm_mngr_activity_recursive_MUTEX
	.section	.bss.ci_and_comm_mngr_activity_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	ci_and_comm_mngr_activity_recursive_MUTEX, %object
	.size	ci_and_comm_mngr_activity_recursive_MUTEX, 4
ci_and_comm_mngr_activity_recursive_MUTEX:
	.space	4
	.global	irri_irri_recursive_MUTEX
	.section	.bss.irri_irri_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	irri_irri_recursive_MUTEX, %object
	.size	irri_irri_recursive_MUTEX, 4
irri_irri_recursive_MUTEX:
	.space	4
	.global	list_foal_irri_recursive_MUTEX
	.section	.bss.list_foal_irri_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	list_foal_irri_recursive_MUTEX, %object
	.size	list_foal_irri_recursive_MUTEX, 4
list_foal_irri_recursive_MUTEX:
	.space	4
	.global	comm_mngr_recursive_MUTEX
	.section	.bss.comm_mngr_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	comm_mngr_recursive_MUTEX, %object
	.size	comm_mngr_recursive_MUTEX, 4
comm_mngr_recursive_MUTEX:
	.space	4
	.global	chain_members_recursive_MUTEX
	.section	.bss.chain_members_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	chain_members_recursive_MUTEX, %object
	.size	chain_members_recursive_MUTEX, 4
chain_members_recursive_MUTEX:
	.space	4
	.global	irri_comm_recursive_MUTEX
	.section	.bss.irri_comm_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	irri_comm_recursive_MUTEX, %object
	.size	irri_comm_recursive_MUTEX, 4
irri_comm_recursive_MUTEX:
	.space	4
	.global	system_report_completed_records_recursive_MUTEX
	.section	.bss.system_report_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	system_report_completed_records_recursive_MUTEX, %object
	.size	system_report_completed_records_recursive_MUTEX, 4
system_report_completed_records_recursive_MUTEX:
	.space	4
	.global	poc_report_completed_records_recursive_MUTEX
	.section	.bss.poc_report_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	poc_report_completed_records_recursive_MUTEX, %object
	.size	poc_report_completed_records_recursive_MUTEX, 4
poc_report_completed_records_recursive_MUTEX:
	.space	4
	.global	station_report_data_completed_records_recursive_MUTEX
	.section	.bss.station_report_data_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	station_report_data_completed_records_recursive_MUTEX, %object
	.size	station_report_data_completed_records_recursive_MUTEX, 4
station_report_data_completed_records_recursive_MUTEX:
	.space	4
	.global	station_history_completed_records_recursive_MUTEX
	.section	.bss.station_history_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	station_history_completed_records_recursive_MUTEX, %object
	.size	station_history_completed_records_recursive_MUTEX, 4
station_history_completed_records_recursive_MUTEX:
	.space	4
	.global	weather_preserves_recursive_MUTEX
	.section	.bss.weather_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	weather_preserves_recursive_MUTEX, %object
	.size	weather_preserves_recursive_MUTEX, 4
weather_preserves_recursive_MUTEX:
	.space	4
	.global	station_preserves_recursive_MUTEX
	.section	.bss.station_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	station_preserves_recursive_MUTEX, %object
	.size	station_preserves_recursive_MUTEX, 4
station_preserves_recursive_MUTEX:
	.space	4
	.global	system_preserves_recursive_MUTEX
	.section	.bss.system_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	system_preserves_recursive_MUTEX, %object
	.size	system_preserves_recursive_MUTEX, 4
system_preserves_recursive_MUTEX:
	.space	4
	.global	poc_preserves_recursive_MUTEX
	.section	.bss.poc_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	poc_preserves_recursive_MUTEX, %object
	.size	poc_preserves_recursive_MUTEX, 4
poc_preserves_recursive_MUTEX:
	.space	4
	.global	list_system_recursive_MUTEX
	.section	.bss.list_system_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	list_system_recursive_MUTEX, %object
	.size	list_system_recursive_MUTEX, 4
list_system_recursive_MUTEX:
	.space	4
	.global	list_poc_recursive_MUTEX
	.section	.bss.list_poc_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	list_poc_recursive_MUTEX, %object
	.size	list_poc_recursive_MUTEX, 4
list_poc_recursive_MUTEX:
	.space	4
	.global	weather_control_recursive_MUTEX
	.section	.bss.weather_control_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	weather_control_recursive_MUTEX, %object
	.size	weather_control_recursive_MUTEX, 4
weather_control_recursive_MUTEX:
	.space	4
	.global	weather_tables_recursive_MUTEX
	.section	.bss.weather_tables_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	weather_tables_recursive_MUTEX, %object
	.size	weather_tables_recursive_MUTEX, 4
weather_tables_recursive_MUTEX:
	.space	4
	.global	speaker_hardware_MUTEX
	.section	.bss.speaker_hardware_MUTEX,"aw",%nobits
	.align	2
	.type	speaker_hardware_MUTEX, %object
	.size	speaker_hardware_MUTEX, 4
speaker_hardware_MUTEX:
	.space	4
	.global	tpmicro_data_recursive_MUTEX
	.section	.bss.tpmicro_data_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	tpmicro_data_recursive_MUTEX, %object
	.size	tpmicro_data_recursive_MUTEX, 4
tpmicro_data_recursive_MUTEX:
	.space	4
	.global	pending_changes_recursive_MUTEX
	.section	.bss.pending_changes_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	pending_changes_recursive_MUTEX, %object
	.size	pending_changes_recursive_MUTEX, 4
pending_changes_recursive_MUTEX:
	.space	4
	.global	list_lights_recursive_MUTEX
	.section	.bss.list_lights_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	list_lights_recursive_MUTEX, %object
	.size	list_lights_recursive_MUTEX, 4
list_lights_recursive_MUTEX:
	.space	4
	.global	list_foal_lights_recursive_MUTEX
	.section	.bss.list_foal_lights_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	list_foal_lights_recursive_MUTEX, %object
	.size	list_foal_lights_recursive_MUTEX, 4
list_foal_lights_recursive_MUTEX:
	.space	4
	.global	irri_lights_recursive_MUTEX
	.section	.bss.irri_lights_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	irri_lights_recursive_MUTEX, %object
	.size	irri_lights_recursive_MUTEX, 4
irri_lights_recursive_MUTEX:
	.space	4
	.global	lights_preserves_recursive_MUTEX
	.section	.bss.lights_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	lights_preserves_recursive_MUTEX, %object
	.size	lights_preserves_recursive_MUTEX, 4
lights_preserves_recursive_MUTEX:
	.space	4
	.global	lights_report_completed_records_recursive_MUTEX
	.section	.bss.lights_report_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	lights_report_completed_records_recursive_MUTEX, %object
	.size	lights_report_completed_records_recursive_MUTEX, 4
lights_report_completed_records_recursive_MUTEX:
	.space	4
	.global	moisture_sensor_items_recursive_MUTEX
	.section	.bss.moisture_sensor_items_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	moisture_sensor_items_recursive_MUTEX, %object
	.size	moisture_sensor_items_recursive_MUTEX, 4
moisture_sensor_items_recursive_MUTEX:
	.space	4
	.global	moisture_sensor_recorder_recursive_MUTEX
	.section	.bss.moisture_sensor_recorder_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	moisture_sensor_recorder_recursive_MUTEX, %object
	.size	moisture_sensor_recorder_recursive_MUTEX, 4
moisture_sensor_recorder_recursive_MUTEX:
	.space	4
	.global	walk_thru_recursive_MUTEX
	.section	.bss.walk_thru_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	walk_thru_recursive_MUTEX, %object
	.size	walk_thru_recursive_MUTEX, 4
walk_thru_recursive_MUTEX:
	.space	4
	.global	budget_report_completed_records_recursive_MUTEX
	.section	.bss.budget_report_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	budget_report_completed_records_recursive_MUTEX, %object
	.size	budget_report_completed_records_recursive_MUTEX, 4
budget_report_completed_records_recursive_MUTEX:
	.space	4
	.global	ci_message_list_MUTEX
	.section	.bss.ci_message_list_MUTEX,"aw",%nobits
	.align	2
	.type	ci_message_list_MUTEX, %object
	.size	ci_message_list_MUTEX, 4
ci_message_list_MUTEX:
	.space	4
	.global	code_distribution_control_structure_recursive_MUTEX
	.section	.bss.code_distribution_control_structure_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	code_distribution_control_structure_recursive_MUTEX, %object
	.size	code_distribution_control_structure_recursive_MUTEX, 4
code_distribution_control_structure_recursive_MUTEX:
	.space	4
	.global	chain_sync_control_structure_recursive_MUTEX
	.section	.bss.chain_sync_control_structure_recursive_MUTEX,"aw",%nobits
	.align	2
	.type	chain_sync_control_structure_recursive_MUTEX, %object
	.size	chain_sync_control_structure_recursive_MUTEX, 4
chain_sync_control_structure_recursive_MUTEX:
	.space	4
	.global	router_hub_list_MUTEX
	.section	.bss.router_hub_list_MUTEX,"aw",%nobits
	.align	2
	.type	router_hub_list_MUTEX, %object
	.size	router_hub_list_MUTEX, 4
router_hub_list_MUTEX:
	.space	4
	.global	router_hub_list_queue
	.section	.bss.router_hub_list_queue,"aw",%nobits
	.align	2
	.type	router_hub_list_queue, %object
	.size	router_hub_list_queue, 4
router_hub_list_queue:
	.space	4
	.global	Contrast_and_Volume_Queue
	.section	.bss.Contrast_and_Volume_Queue,"aw",%nobits
	.align	2
	.type	Contrast_and_Volume_Queue, %object
	.size	Contrast_and_Volume_Queue, 4
Contrast_and_Volume_Queue:
	.space	4
	.global	Key_Scanner_Queue
	.section	.bss.Key_Scanner_Queue,"aw",%nobits
	.align	2
	.type	Key_Scanner_Queue, %object
	.size	Key_Scanner_Queue, 4
Key_Scanner_Queue:
	.space	4
	.global	Key_To_Process_Queue
	.section	.bss.Key_To_Process_Queue,"aw",%nobits
	.align	2
	.type	Key_To_Process_Queue, %object
	.size	Key_To_Process_Queue, 4
Key_To_Process_Queue:
	.space	4
	.global	FLASH_STORAGE_task_queue_0
	.section	.bss.FLASH_STORAGE_task_queue_0,"aw",%nobits
	.align	2
	.type	FLASH_STORAGE_task_queue_0, %object
	.size	FLASH_STORAGE_task_queue_0, 4
FLASH_STORAGE_task_queue_0:
	.space	4
	.global	FLASH_STORAGE_task_queue_1
	.section	.bss.FLASH_STORAGE_task_queue_1,"aw",%nobits
	.align	2
	.type	FLASH_STORAGE_task_queue_1, %object
	.size	FLASH_STORAGE_task_queue_1, 4
FLASH_STORAGE_task_queue_1:
	.space	4
	.global	Display_Command_Queue
	.section	.bss.Display_Command_Queue,"aw",%nobits
	.align	2
	.type	Display_Command_Queue, %object
	.size	Display_Command_Queue, 4
Display_Command_Queue:
	.space	4
	.global	TPL_IN_event_queue
	.section	.bss.TPL_IN_event_queue,"aw",%nobits
	.align	2
	.type	TPL_IN_event_queue, %object
	.size	TPL_IN_event_queue, 4
TPL_IN_event_queue:
	.space	4
	.global	TPL_OUT_event_queue
	.section	.bss.TPL_OUT_event_queue,"aw",%nobits
	.align	2
	.type	TPL_OUT_event_queue, %object
	.size	TPL_OUT_event_queue, 4
TPL_OUT_event_queue:
	.space	4
	.global	COMM_MNGR_task_queue
	.section	.bss.COMM_MNGR_task_queue,"aw",%nobits
	.align	2
	.type	COMM_MNGR_task_queue, %object
	.size	COMM_MNGR_task_queue, 4
COMM_MNGR_task_queue:
	.space	4
	.global	CODE_DISTRIBUTION_task_queue
	.section	.bss.CODE_DISTRIBUTION_task_queue,"aw",%nobits
	.align	2
	.type	CODE_DISTRIBUTION_task_queue, %object
	.size	CODE_DISTRIBUTION_task_queue, 4
CODE_DISTRIBUTION_task_queue:
	.space	4
	.global	CONTROLLER_INITIATED_task_queue
	.section	.bss.CONTROLLER_INITIATED_task_queue,"aw",%nobits
	.align	2
	.type	CONTROLLER_INITIATED_task_queue, %object
	.size	CONTROLLER_INITIATED_task_queue, 4
CONTROLLER_INITIATED_task_queue:
	.space	4
	.global	BYPASS_event_queue
	.section	.bss.BYPASS_event_queue,"aw",%nobits
	.align	2
	.type	BYPASS_event_queue, %object
	.size	BYPASS_event_queue, 4
BYPASS_event_queue:
	.space	4
	.global	RADIO_TEST_task_queue
	.section	.bss.RADIO_TEST_task_queue,"aw",%nobits
	.align	2
	.type	RADIO_TEST_task_queue, %object
	.size	RADIO_TEST_task_queue, 4
RADIO_TEST_task_queue:
	.space	4
	.global	Background_Calculation_Queue
	.section	.bss.Background_Calculation_Queue,"aw",%nobits
	.align	2
	.type	Background_Calculation_Queue, %object
	.size	Background_Calculation_Queue, 4
Background_Calculation_Queue:
	.space	4
	.global	FTIMES_task_queue
	.section	.bss.FTIMES_task_queue,"aw",%nobits
	.align	2
	.type	FTIMES_task_queue, %object
	.size	FTIMES_task_queue, 4
FTIMES_task_queue:
	.space	4
	.global	startup_task_FLOP_registers
	.section	.iram_vars,"aw",%progbits
	.align	2
	.type	startup_task_FLOP_registers, %object
	.size	startup_task_FLOP_registers, 132
startup_task_FLOP_registers:
	.space	132
	.align	2
	.type	epson_RTC_task_FLOP_registers, %object
	.size	epson_RTC_task_FLOP_registers, 132
epson_RTC_task_FLOP_registers:
	.space	132
	.align	2
	.type	powerfail_task_FLOP_registers, %object
	.size	powerfail_task_FLOP_registers, 132
powerfail_task_FLOP_registers:
	.space	132
	.align	2
	.type	wdt_task_FLOP_registers, %object
	.size	wdt_task_FLOP_registers, 132
wdt_task_FLOP_registers:
	.space	132
	.global	timer_task_FLOP_registers
	.align	2
	.type	timer_task_FLOP_registers, %object
	.size	timer_task_FLOP_registers, 132
timer_task_FLOP_registers:
	.space	132
	.align	2
	.type	task_table_FLOP_registers, %object
	.size	task_table_FLOP_registers, 3168
task_table_FLOP_registers:
	.space	3168
	.global	startup_task_handle
	.section	.bss.startup_task_handle,"aw",%nobits
	.align	2
	.type	startup_task_handle, %object
	.size	startup_task_handle, 4
startup_task_handle:
	.space	4
	.global	epson_rtc_task_handle
	.section	.bss.epson_rtc_task_handle,"aw",%nobits
	.align	2
	.type	epson_rtc_task_handle, %object
	.size	epson_rtc_task_handle, 4
epson_rtc_task_handle:
	.space	4
	.global	powerfail_task_handle
	.section	.bss.powerfail_task_handle,"aw",%nobits
	.align	2
	.type	powerfail_task_handle, %object
	.size	powerfail_task_handle, 4
powerfail_task_handle:
	.space	4
	.global	wdt_task_handle
	.section	.bss.wdt_task_handle,"aw",%nobits
	.align	2
	.type	wdt_task_handle, %object
	.size	wdt_task_handle, 4
wdt_task_handle:
	.space	4
	.global	timer_task_handle
	.section	.bss.timer_task_handle,"aw",%nobits
	.align	2
	.type	timer_task_handle, %object
	.size	timer_task_handle, 4
timer_task_handle:
	.space	4
	.global	flash_storage_0_task_handle
	.section	.bss.flash_storage_0_task_handle,"aw",%nobits
	.align	2
	.type	flash_storage_0_task_handle, %object
	.size	flash_storage_0_task_handle, 4
flash_storage_0_task_handle:
	.space	4
	.global	flash_storage_1_task_handle
	.section	.bss.flash_storage_1_task_handle,"aw",%nobits
	.align	2
	.type	flash_storage_1_task_handle, %object
	.size	flash_storage_1_task_handle, 4
flash_storage_1_task_handle:
	.space	4
	.section	.bss.comm_mngr_task_handle,"aw",%nobits
	.align	2
	.type	comm_mngr_task_handle, %object
	.size	comm_mngr_task_handle, 4
comm_mngr_task_handle:
	.space	4
	.section	.bss.port_tp_ring_buffer_analysis_task_handle,"aw",%nobits
	.align	2
	.type	port_tp_ring_buffer_analysis_task_handle, %object
	.size	port_tp_ring_buffer_analysis_task_handle, 4
port_tp_ring_buffer_analysis_task_handle:
	.space	4
	.section	.bss.port_tp_serial_driver_task_handle,"aw",%nobits
	.align	2
	.type	port_tp_serial_driver_task_handle, %object
	.size	port_tp_serial_driver_task_handle, 4
port_tp_serial_driver_task_handle:
	.space	4
	.global	created_task_handles
	.section	.bss.created_task_handles,"aw",%nobits
	.align	2
	.type	created_task_handles, %object
	.size	created_task_handles, 96
created_task_handles:
	.space	96
	.global	rtc_task_last_x_stamp
	.section	.bss.rtc_task_last_x_stamp,"aw",%nobits
	.align	2
	.type	rtc_task_last_x_stamp, %object
	.size	rtc_task_last_x_stamp, 4
rtc_task_last_x_stamp:
	.space	4
	.global	pwrfail_task_last_x_stamp
	.section	.iram_vars
	.align	2
	.type	pwrfail_task_last_x_stamp, %object
	.size	pwrfail_task_last_x_stamp, 4
pwrfail_task_last_x_stamp:
	.space	4
	.global	task_last_execution_stamp
	.align	2
	.type	task_last_execution_stamp, %object
	.size	task_last_execution_stamp, 96
task_last_execution_stamp:
	.space	96
	.global	Task_Table
	.section .rodata
	.align	2
.LC0:
	.ascii	"Dsply_Proc\000"
	.align	2
.LC1:
	.ascii	"Key_Proc\000"
	.align	2
.LC2:
	.ascii	"Key_Scan\000"
	.align	2
.LC3:
	.ascii	"TD_Check\000"
	.align	2
.LC4:
	.ascii	"Bypass\000"
	.align	2
.LC5:
	.ascii	"Comm_Mngr\000"
	.align	2
.LC6:
	.ascii	"Code Distrib\000"
	.align	2
.LC7:
	.ascii	"Ring_TP\000"
	.align	2
.LC8:
	.ascii	"Ring_A\000"
	.align	2
.LC9:
	.ascii	"Ring_B\000"
	.align	2
.LC10:
	.ascii	"Ring_RRE\000"
	.align	2
.LC11:
	.ascii	"CI task\000"
	.align	2
.LC12:
	.ascii	"LINK task\000"
	.align	2
.LC13:
	.ascii	"TPL_IN\000"
	.align	2
.LC14:
	.ascii	"TPL_OUT\000"
	.align	2
.LC15:
	.ascii	"SerDrvr_TP\000"
	.align	2
.LC16:
	.ascii	"SerDrvr_A\000"
	.align	2
.LC17:
	.ascii	"SerDrvr_B\000"
	.align	2
.LC18:
	.ascii	"SerDrvr_RRE\000"
	.align	2
.LC19:
	.ascii	"Contrast\000"
	.align	2
.LC20:
	.ascii	"Flash_0\000"
	.align	2
.LC21:
	.ascii	"Flash_1\000"
	.align	2
.LC22:
	.ascii	"BCalc\000"
	.align	2
.LC23:
	.ascii	"FTimes\000"
	.section	.rodata.Task_Table,"a",%progbits
	.align	2
	.type	Task_Table, %object
	.size	Task_Table, 768
Task_Table:
	.word	1
	.word	1
	.word	1000
	.word	Display_Processing_Task
	.word	.LC0
	.word	2048
	.word	0
	.word	8
	.word	1
	.word	1
	.word	1000
	.word	Key_Processing_Task
	.word	.LC1
	.word	1024
	.word	0
	.word	7
	.word	1
	.word	1
	.word	1000
	.word	key_scanner_task
	.word	.LC2
	.word	1024
	.word	0
	.word	6
	.word	1
	.word	1
	.word	1000
	.word	TD_CHECK_task
	.word	.LC3
	.word	1024
	.word	0
	.word	6
	.word	1
	.word	1
	.word	1000
	.word	BYPASS_task
	.word	.LC4
	.word	1024
	.word	0
	.word	6
	.word	1
	.word	1
	.word	5000
	.word	COMM_MNGR_task
	.word	.LC5
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	5000
	.word	CODE_DISTRIBUTION_task
	.word	.LC6
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	1000
	.word	ring_buffer_analysis_task
	.word	.LC7
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	10500
	.word	ring_buffer_analysis_task
	.word	.LC8
	.word	1024
	.word	1
	.word	5
	.word	1
	.word	1
	.word	1000
	.word	ring_buffer_analysis_task
	.word	.LC9
	.word	1024
	.word	2
	.word	5
	.word	1
	.word	1
	.word	1000
	.word	ring_buffer_analysis_task
	.word	.LC10
	.word	1024
	.word	3
	.word	5
	.word	1
	.word	1
	.word	2000
	.word	CONTROLLER_INITIATED_task
	.word	.LC11
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	2000
	.word	RADIO_TEST_task
	.word	.LC12
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	1000
	.word	TPL_IN_task
	.word	.LC13
	.word	1024
	.word	0
	.word	4
	.word	1
	.word	1
	.word	1000
	.word	TPL_OUT_task
	.word	.LC14
	.word	1024
	.word	0
	.word	4
	.word	1
	.word	1
	.word	1000
	.word	serial_driver_task
	.word	.LC15
	.word	1024
	.word	0
	.word	3
	.word	1
	.word	1
	.word	1000
	.word	serial_driver_task
	.word	.LC16
	.word	1024
	.word	1
	.word	3
	.word	1
	.word	1
	.word	1000
	.word	serial_driver_task
	.word	.LC17
	.word	1024
	.word	2
	.word	3
	.word	1
	.word	1
	.word	1000
	.word	serial_driver_task
	.word	.LC18
	.word	1024
	.word	3
	.word	3
	.word	1
	.word	1
	.word	1000
	.word	contrast_and_speaker_volume_control_task
	.word	.LC19
	.word	1024
	.word	0
	.word	2
	.word	1
	.word	1
	.word	30000
	.word	FLASH_STORAGE_flash_storage_task
	.word	.LC20
	.word	1024
	.word	0
	.word	2
	.word	1
	.word	1
	.word	20000
	.word	FLASH_STORAGE_flash_storage_task
	.word	.LC21
	.word	1024
	.word	1
	.word	2
	.word	1
	.word	1
	.word	20000
	.word	background_calculation_task
	.word	.LC22
	.word	1024
	.word	0
	.word	1
	.word	1
	.word	1
	.word	1000
	.word	FTIMES_TASK_task
	.word	.LC23
	.word	1024
	.word	0
	.word	1
	.section .rodata
	.align	2
.LC24:
	.ascii	"NULL Task Handle!\000"
	.section	.text.elevate_priority_of_ISP_related_code_update_tasks,"ax",%progbits
	.align	2
	.global	elevate_priority_of_ISP_related_code_update_tasks
	.type	elevate_priority_of_ISP_related_code_update_tasks, %function
elevate_priority_of_ISP_related_code_update_tasks:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.c"
	.loc 1 847 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	.loc 1 854 0
	ldr	r3, .L4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L2
	.loc 1 854 0 is_stmt 0 discriminator 1
	ldr	r3, .L4+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L2
	ldr	r3, .L4+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L2
	.loc 1 856 0 is_stmt 1
	ldr	r3, .L4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #12
	bl	vTaskPrioritySet
	.loc 1 858 0
	ldr	r3, .L4+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #12
	bl	vTaskPrioritySet
	.loc 1 860 0
	ldr	r3, .L4+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #12
	bl	vTaskPrioritySet
	b	.L1
.L2:
	.loc 1 864 0
	ldr	r0, .L4+12
	bl	Alert_Message
.L1:
	.loc 1 866 0
	ldmfd	sp!, {fp, pc}
.L5:
	.align	2
.L4:
	.word	comm_mngr_task_handle
	.word	port_tp_ring_buffer_analysis_task_handle
	.word	port_tp_serial_driver_task_handle
	.word	.LC24
.LFE0:
	.size	elevate_priority_of_ISP_related_code_update_tasks, .-elevate_priority_of_ISP_related_code_update_tasks
	.section	.text.restore_priority_of_ISP_related_code_update_tasks,"ax",%progbits
	.align	2
	.global	restore_priority_of_ISP_related_code_update_tasks
	.type	restore_priority_of_ISP_related_code_update_tasks, %function
restore_priority_of_ISP_related_code_update_tasks:
.LFB1:
	.loc 1 869 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI2:
	add	fp, sp, #4
.LCFI3:
	.loc 1 872 0
	ldr	r3, .L9
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L7
	.loc 1 872 0 is_stmt 0 discriminator 1
	ldr	r3, .L9+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L7
	ldr	r3, .L9+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L7
	.loc 1 874 0 is_stmt 1
	ldr	r3, .L9
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #5
	bl	vTaskPrioritySet
	.loc 1 876 0
	ldr	r3, .L9+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #5
	bl	vTaskPrioritySet
	.loc 1 878 0
	ldr	r3, .L9+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #3
	bl	vTaskPrioritySet
	b	.L6
.L7:
	.loc 1 882 0
	ldr	r0, .L9+12
	bl	Alert_Message
.L6:
	.loc 1 884 0
	ldmfd	sp!, {fp, pc}
.L10:
	.align	2
.L9:
	.word	comm_mngr_task_handle
	.word	port_tp_ring_buffer_analysis_task_handle
	.word	port_tp_serial_driver_task_handle
	.word	.LC24
.LFE1:
	.size	restore_priority_of_ISP_related_code_update_tasks, .-restore_priority_of_ISP_related_code_update_tasks
	.section .rodata
	.align	2
.LC25:
	.ascii	"( debugio_MUTEX = xSemaphoreCreateMutex() )\000"
	.align	2
.LC26:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/app_"
	.ascii	"startup.c\000"
	.section	.text.__create_system_semaphores,"ax",%progbits
	.align	2
	.type	__create_system_semaphores, %function
__create_system_semaphores:
.LFB2:
	.loc 1 888 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI4:
	add	fp, sp, #4
.LCFI5:
	sub	sp, sp, #4
.LCFI6:
	.loc 1 893 0
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16
	str	r2, [r3, #0]
	ldr	r3, .L16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L12
	.loc 1 893 0 is_stmt 0 discriminator 1
	ldr	r0, .L16+4
	ldr	r1, .L16+8
	ldr	r2, .L16+12
	bl	__assert
.L12:
	.loc 1 899 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L13
.L15:
	.loc 1 919 0
	mov	r0, #1
	mov	r1, #0
	mov	r2, #3
	bl	xQueueGenericCreate
	mov	r1, r0
	ldr	r3, .L16+16
	ldr	r2, [fp, #-8]
	str	r1, [r3, r2, asl #2]
	ldr	r3, .L16+16
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L14
	.loc 1 919 0 is_stmt 0 discriminator 1
	ldr	r3, .L16+16
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
.L14:
	.loc 1 921 0 is_stmt 1
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r1, r0
	ldr	r3, .L16+20
	ldr	r2, [fp, #-8]
	str	r1, [r3, r2, asl #2]
	.loc 1 899 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L13:
	.loc 1 899 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #4
	ble	.L15
	.loc 1 928 0 is_stmt 1
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+24
	str	r2, [r3, #0]
	.loc 1 931 0
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+28
	str	r2, [r3, #0]
	.loc 1 936 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+32
	str	r2, [r3, #0]
	.loc 1 942 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+36
	str	r2, [r3, #0]
	.loc 1 947 0
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+40
	str	r2, [r3, #0]
	.loc 1 950 0
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+44
	str	r2, [r3, #0]
	.loc 1 955 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+48
	str	r2, [r3, #0]
	.loc 1 958 0
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+52
	str	r2, [r3, #0]
	.loc 1 963 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+56
	str	r2, [r3, #0]
	.loc 1 966 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+60
	str	r2, [r3, #0]
	.loc 1 971 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+64
	str	r2, [r3, #0]
	.loc 1 974 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+68
	str	r2, [r3, #0]
	.loc 1 977 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+72
	str	r2, [r3, #0]
	.loc 1 982 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+76
	str	r2, [r3, #0]
	.loc 1 986 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+80
	str	r2, [r3, #0]
	.loc 1 989 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+84
	str	r2, [r3, #0]
	.loc 1 992 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+88
	str	r2, [r3, #0]
	.loc 1 995 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+92
	str	r2, [r3, #0]
	.loc 1 1000 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+96
	str	r2, [r3, #0]
	.loc 1 1003 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+100
	str	r2, [r3, #0]
	.loc 1 1006 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+104
	str	r2, [r3, #0]
	.loc 1 1009 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+108
	str	r2, [r3, #0]
	.loc 1 1014 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+112
	str	r2, [r3, #0]
	.loc 1 1017 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+116
	str	r2, [r3, #0]
	.loc 1 1022 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+120
	str	r2, [r3, #0]
	.loc 1 1025 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+124
	str	r2, [r3, #0]
	.loc 1 1030 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+128
	str	r2, [r3, #0]
	.loc 1 1033 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+132
	str	r2, [r3, #0]
	.loc 1 1036 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+136
	str	r2, [r3, #0]
	.loc 1 1039 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+140
	str	r2, [r3, #0]
	.loc 1 1042 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+144
	str	r2, [r3, #0]
	.loc 1 1048 0
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+148
	str	r2, [r3, #0]
	.loc 1 1052 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+152
	str	r2, [r3, #0]
	.loc 1 1057 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+156
	str	r2, [r3, #0]
	.loc 1 1061 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+160
	str	r2, [r3, #0]
	.loc 1 1063 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+164
	str	r2, [r3, #0]
	.loc 1 1068 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+168
	str	r2, [r3, #0]
	.loc 1 1073 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+172
	str	r2, [r3, #0]
	.loc 1 1077 0
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+176
	str	r2, [r3, #0]
	.loc 1 1081 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+180
	str	r2, [r3, #0]
	.loc 1 1085 0
	mov	r0, #4
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+184
	str	r2, [r3, #0]
	.loc 1 1089 0
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L16+188
	str	r2, [r3, #0]
	.loc 1 1090 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	debugio_MUTEX
	.word	.LC25
	.word	.LC26
	.word	893
	.word	rcvd_data_binary_semaphore
	.word	xmit_list_MUTEX
	.word	Flash_0_MUTEX
	.word	Flash_1_MUTEX
	.word	alerts_pile_recursive_MUTEX
	.word	list_program_data_recursive_MUTEX
	.word	list_tpl_in_messages_MUTEX
	.word	list_tpl_out_messages_MUTEX
	.word	list_incoming_messages_recursive_MUTEX
	.word	list_packets_waiting_for_token_MUTEX
	.word	irri_irri_recursive_MUTEX
	.word	list_foal_irri_recursive_MUTEX
	.word	comm_mngr_recursive_MUTEX
	.word	chain_members_recursive_MUTEX
	.word	irri_comm_recursive_MUTEX
	.word	ci_and_comm_mngr_activity_recursive_MUTEX
	.word	system_report_completed_records_recursive_MUTEX
	.word	poc_report_completed_records_recursive_MUTEX
	.word	station_report_data_completed_records_recursive_MUTEX
	.word	station_history_completed_records_recursive_MUTEX
	.word	weather_preserves_recursive_MUTEX
	.word	station_preserves_recursive_MUTEX
	.word	system_preserves_recursive_MUTEX
	.word	poc_preserves_recursive_MUTEX
	.word	list_system_recursive_MUTEX
	.word	list_poc_recursive_MUTEX
	.word	weather_control_recursive_MUTEX
	.word	weather_tables_recursive_MUTEX
	.word	list_lights_recursive_MUTEX
	.word	list_foal_lights_recursive_MUTEX
	.word	irri_lights_recursive_MUTEX
	.word	lights_report_completed_records_recursive_MUTEX
	.word	lights_preserves_recursive_MUTEX
	.word	speaker_hardware_MUTEX
	.word	tpmicro_data_recursive_MUTEX
	.word	pending_changes_recursive_MUTEX
	.word	moisture_sensor_items_recursive_MUTEX
	.word	moisture_sensor_recorder_recursive_MUTEX
	.word	walk_thru_recursive_MUTEX
	.word	budget_report_completed_records_recursive_MUTEX
	.word	ci_message_list_MUTEX
	.word	code_distribution_control_structure_recursive_MUTEX
	.word	chain_sync_control_structure_recursive_MUTEX
	.word	router_hub_list_MUTEX
.LFE2:
	.size	__create_system_semaphores, .-__create_system_semaphores
	.section	.text.__create_system_queues,"ax",%progbits
	.align	2
	.type	__create_system_queues, %function
__create_system_queues:
.LFB3:
	.loc 1 1094 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI7:
	add	fp, sp, #4
.LCFI8:
	sub	sp, sp, #4
.LCFI9:
	.loc 1 1097 0
	mov	r0, #10
	mov	r1, #8
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21
	str	r2, [r3, #0]
	.loc 1 1104 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L19
.L20:
	.loc 1 1113 0 discriminator 2
	mov	r0, #170
	mov	r1, #4
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r1, .L21+4
	ldr	r3, [fp, #-8]
	ldr	r0, .L21+8
	mul	r3, r0, r3
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 1 1104 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L19:
	.loc 1 1104 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #4
	bls	.L20
	.loc 1 1119 0 is_stmt 1
	mov	r0, #10
	mov	r1, #40
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+12
	str	r2, [r3, #0]
	.loc 1 1121 0
	mov	r0, #25
	mov	r1, #8
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+16
	str	r2, [r3, #0]
	.loc 1 1126 0
	mov	r0, #50
	mov	r1, #28
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+20
	str	r2, [r3, #0]
	.loc 1 1128 0
	mov	r0, #50
	mov	r1, #28
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+24
	str	r2, [r3, #0]
	.loc 1 1132 0
	mov	r0, #25
	mov	r1, #36
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+28
	str	r2, [r3, #0]
	.loc 1 1136 0
	mov	r0, #30
	mov	r1, #20
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+32
	str	r2, [r3, #0]
	.loc 1 1138 0
	mov	r0, #30
	mov	r1, #36
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+36
	str	r2, [r3, #0]
	.loc 1 1141 0
	mov	r0, #30
	mov	r1, #40
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+40
	str	r2, [r3, #0]
	.loc 1 1143 0
	mov	r0, #30
	mov	r1, #24
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+44
	str	r2, [r3, #0]
	.loc 1 1145 0
	mov	r0, #15
	mov	r1, #20
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+48
	str	r2, [r3, #0]
	.loc 1 1148 0
	mov	r0, #30
	mov	r1, #20
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+52
	str	r2, [r3, #0]
	.loc 1 1151 0
	mov	r0, #10
	mov	r1, #32
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+56
	str	r2, [r3, #0]
	.loc 1 1155 0
	mov	r0, #2
	mov	r1, #4
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+60
	str	r2, [r3, #0]
	.loc 1 1161 0
	mov	r0, #15
	mov	r1, #4
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+64
	str	r2, [r3, #0]
	.loc 1 1179 0
	mov	r0, #512
	mov	r1, #4
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L21+68
	str	r2, [r3, #0]
	.loc 1 1180 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L22:
	.align	2
.L21:
	.word	Contrast_and_Volume_Queue
	.word	SerDrvrVars_s
	.word	4280
	.word	Key_Scanner_Queue
	.word	Key_To_Process_Queue
	.word	FLASH_STORAGE_task_queue_0
	.word	FLASH_STORAGE_task_queue_1
	.word	Display_Command_Queue
	.word	TPL_IN_event_queue
	.word	TPL_OUT_event_queue
	.word	COMM_MNGR_task_queue
	.word	CODE_DISTRIBUTION_task_queue
	.word	CONTROLLER_INITIATED_task_queue
	.word	BYPASS_event_queue
	.word	RADIO_TEST_task_queue
	.word	Background_Calculation_Queue
	.word	FTIMES_task_queue
	.word	router_hub_list_queue
.LFE3:
	.size	__create_system_queues, .-__create_system_queues
	.section .rodata
	.align	2
.LC27:
	.ascii	"WDT\000"
	.align	2
.LC28:
	.ascii	"RTC\000"
	.align	2
.LC29:
	.ascii	"PWRFAIL\000"
	.align	2
.LC30:
	.ascii	"ci alerts tmr\000"
	.align	2
.LC31:
	.ascii	"%s\000"
	.section	.text.system_startup_task,"ax",%progbits
	.align	2
	.global	system_startup_task
	.type	system_startup_task, %function
system_startup_task:
.LFB4:
	.loc 1 1199 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI10:
	add	fp, sp, #4
.LCFI11:
	sub	sp, sp, #36
.LCFI12:
	str	r0, [fp, #-24]
	.loc 1 1222 0
	mov	r3, #15
	str	r3, [sp, #0]
	ldr	r3, .L79
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r3, #0
	str	r3, [sp, #12]
	ldr	r0, .L79+4
	ldr	r1, .L79+8
	mov	r2, #2048
	mov	r3, #0
	bl	xTaskGenericCreate
	.loc 1 1226 0
	ldr	r0, .L79+12
	bl	vPortSaveVFPRegisters
	ldr	r3, .L79
	ldr	r2, [r3, #0]
	ldr	r3, .L79+12
	mov	r0, r2
	mov	r1, r3
	bl	vTaskSetApplicationTaskTag
	.loc 1 1234 0
	ldr	r3, .L79
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	vTaskPrioritySet
	.loc 1 1240 0
	mov	r0, #1
	mov	r1, #0
	mov	r2, #3
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L79+16
	str	r2, [r3, #0]
	ldr	r3, .L79+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L24
	.loc 1 1240 0 is_stmt 0 discriminator 1
	ldr	r3, .L79+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
.L24:
	.loc 1 1243 0 is_stmt 1
	ldr	r3, .L79+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 1250 0
	mov	r3, #14
	str	r3, [sp, #0]
	ldr	r3, .L79+20
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r3, #0
	str	r3, [sp, #12]
	ldr	r0, .L79+24
	ldr	r1, .L79+28
	mov	r2, #2048
	mov	r3, #0
	bl	xTaskGenericCreate
	.loc 1 1252 0
	ldr	r0, .L79+32
	bl	vPortSaveVFPRegisters
	ldr	r3, .L79+20
	ldr	r2, [r3, #0]
	ldr	r3, .L79+32
	mov	r0, r2
	mov	r1, r3
	bl	vTaskSetApplicationTaskTag
	.loc 1 1261 0
	ldr	r3, .L79+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 1271 0
	bl	xTimerGetTimerDaemonTaskHandle
	mov	r2, r0
	ldr	r3, .L79+36
	str	r2, [r3, #0]
	.loc 1 1275 0
	ldr	r0, .L79+40
	bl	vPortSaveVFPRegisters
	ldr	r3, .L79+36
	ldr	r2, [r3, #0]
	ldr	r3, .L79+40
	mov	r0, r2
	mov	r1, r3
	bl	vTaskSetApplicationTaskTag
	.loc 1 1283 0
	mov	r3, #15
	str	r3, [sp, #0]
	ldr	r3, .L79+44
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r3, #0
	str	r3, [sp, #12]
	ldr	r0, .L79+48
	ldr	r1, .L79+52
	mov	r2, #2048
	mov	r3, #0
	bl	xTaskGenericCreate
	.loc 1 1292 0
	ldr	r0, .L79+56
	bl	vPortSaveVFPRegisters
	ldr	r3, .L79+44
	ldr	r2, [r3, #0]
	ldr	r3, .L79+56
	mov	r0, r2
	mov	r1, r3
	bl	vTaskSetApplicationTaskTag
	.loc 1 1303 0
	bl	__create_system_semaphores
	.loc 1 1305 0
	bl	__create_system_queues
	.loc 1 1314 0
	ldr	r3, .L79+60
	str	r3, [sp, #0]
	ldr	r0, .L79+64
	ldr	r1, .L79+68
	mov	r2, #0
	mov	r3, #0
	bl	xTimerCreate
	mov	r2, r0
	ldr	r3, .L79+72
	str	r2, [r3, #64]
	.loc 1 1320 0
	mov	r0, #0
	bl	ALERTS_test_all_piles
	.loc 1 1336 0
	mov	r0, #0
	bl	init_restart_info
	str	r0, [fp, #-16]
	.loc 1 1367 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L25
	.loc 1 1367 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L25:
	.loc 1 1374 0 is_stmt 1
	bl	GPIO_SETUP_learn_and_set_board_hardware_id
	.loc 1 1385 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L26
	.loc 1 1385 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L26:
	.loc 1 1386 0 is_stmt 1
	bl	FLASH_STORAGE_create_delayed_file_save_timers
	.loc 1 1400 0
	ldr	r3, .L79+80
	mov	r2, #32
	str	r2, [r3, #0]
	.loc 1 1402 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L27
	.loc 1 1402 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L27:
	.loc 1 1405 0 is_stmt 1
	mov	r0, #0
	bl	init_FLASH_DRIVE_SSP_channel
	.loc 1 1407 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L28
	.loc 1 1407 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L28:
	.loc 1 1408 0 is_stmt 1
	mov	r0, #1
	bl	init_FLASH_DRIVE_SSP_channel
	.loc 1 1410 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L29
	.loc 1 1410 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L29:
	.loc 1 1411 0 is_stmt 1
	mov	r0, #0
	mov	r1, #0
	bl	init_FLASH_DRIVE_file_system
	.loc 1 1413 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L30
	.loc 1 1413 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L30:
	.loc 1 1414 0 is_stmt 1
	mov	r0, #1
	mov	r1, #0
	bl	init_FLASH_DRIVE_file_system
	.loc 1 1423 0
	bl	init_chain_sync_vars
	.loc 1 1430 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L31
	.loc 1 1430 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L31:
	.loc 1 1431 0 is_stmt 1
	bl	init_file_configuration_controller
	.loc 1 1439 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L32
	.loc 1 1439 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L32:
	.loc 1 1440 0 is_stmt 1
	bl	init_file_flowsense
	.loc 1 1448 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L33
	.loc 1 1448 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L33:
	.loc 1 1449 0 is_stmt 1
	bl	init_file_configuration_network
	.loc 1 1455 0
	bl	FLOWSENSE_get_controller_index
	mov	r3, r0
	mov	r0, r3
	bl	Alert_program_restart_idx
	.loc 1 1459 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L34
	.loc 1 1459 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L34:
	.loc 1 1460 0 is_stmt 1
	bl	init_file_contrast_and_speaker_vol
	.loc 1 1472 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L35
	.loc 1 1472 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L35:
	.loc 1 1473 0 is_stmt 1
	bl	init_lcd_2
	.loc 1 1476 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L36
	.loc 1 1476 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L36:
	.loc 1 1477 0 is_stmt 1
	bl	init_speaker
	.loc 1 1483 0
	bl	init_a_to_d
	.loc 1 1495 0
	mov	r0, #1
	bl	CONFIG_device_discovery
	.loc 1 1497 0
	mov	r0, #2
	bl	CONFIG_device_discovery
	.loc 1 1504 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L37
	.loc 1 1504 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L37:
	.loc 1 1505 0 is_stmt 1
	bl	init_irri_irri
	.loc 1 1508 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L38
	.loc 1 1508 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L38:
	.loc 1 1509 0 is_stmt 1
	bl	init_irri_lights
	.loc 1 1571 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L39
	.loc 1 1571 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L39:
	.loc 1 1572 0 is_stmt 1
	bl	init_file_system_report_records
	.loc 1 1574 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L40
	.loc 1 1574 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L40:
	.loc 1 1575 0 is_stmt 1
	bl	init_file_poc_report_records
	.loc 1 1577 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L41
	.loc 1 1577 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L41:
	.loc 1 1578 0 is_stmt 1
	bl	init_file_station_report_data
	.loc 1 1580 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L42
	.loc 1 1580 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L42:
	.loc 1 1581 0 is_stmt 1
	bl	init_file_station_history
	.loc 1 1583 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L43
	.loc 1 1583 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L43:
	.loc 1 1584 0 is_stmt 1
	bl	init_file_lights_report_data
	.loc 1 1587 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L44
	.loc 1 1587 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L44:
	.loc 1 1588 0 is_stmt 1
	bl	init_file_budget_report_records
	.loc 1 1601 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L45
	.loc 1 1601 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L45:
	.loc 1 1602 0 is_stmt 1
	bl	init_file_tpmicro_data
	.loc 1 1606 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L46
	.loc 1 1606 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L46:
	.loc 1 1607 0 is_stmt 1
	bl	init_file_weather_control
	.loc 1 1610 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L47
	.loc 1 1610 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L47:
	.loc 1 1611 0 is_stmt 1
	bl	init_file_weather_tables
	.loc 1 1614 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L48
	.loc 1 1614 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L48:
	.loc 1 1615 0 is_stmt 1
	bl	init_file_manual_programs
	.loc 1 1618 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L49
	.loc 1 1618 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L49:
	.loc 1 1619 0 is_stmt 1
	bl	init_file_irrigation_system
	.loc 1 1624 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L50
	.loc 1 1624 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L50:
	.loc 1 1625 0 is_stmt 1
	bl	init_file_POC
	.loc 1 1630 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L51
	.loc 1 1630 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L51:
	.loc 1 1631 0 is_stmt 1
	bl	init_file_station_group
	.loc 1 1641 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L52
	.loc 1 1641 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L52:
	.loc 1 1642 0 is_stmt 1
	bl	init_file_station_info
	.loc 1 1646 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L53
	.loc 1 1646 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L53:
	.loc 1 1647 0 is_stmt 1
	bl	init_file_LIGHTS
	.loc 1 1652 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L54
	.loc 1 1652 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L54:
	.loc 1 1653 0 is_stmt 1
	bl	init_file_moisture_sensor
	.loc 1 1657 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L55
	.loc 1 1657 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L55:
	.loc 1 1658 0 is_stmt 1
	bl	init_file_walk_thru
	.loc 1 1663 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L56
	.loc 1 1663 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L56:
	.loc 1 1664 0 is_stmt 1
	bl	init_tpmicro_data_on_boot
	.loc 1 1670 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L57
	.loc 1 1670 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L57:
	.loc 1 1671 0 is_stmt 1
	bl	init_radio_test_gui_vars_and_state_machine
	.loc 1 1699 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L58
	.loc 1 1699 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L58:
	.loc 1 1700 0 is_stmt 1
	mov	r0, #0
	bl	init_battery_backed_weather_preserves
	.loc 1 1704 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L59
	.loc 1 1704 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L59:
	.loc 1 1710 0 is_stmt 1
	mov	r0, #0
	bl	init_battery_backed_station_preserves
	.loc 1 1719 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L60
	.loc 1 1719 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L60:
	.loc 1 1720 0 is_stmt 1
	mov	r0, #0
	bl	init_battery_backed_system_preserves
	.loc 1 1722 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L61
	.loc 1 1722 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L61:
	.loc 1 1723 0 is_stmt 1
	bl	SYSTEM_PRESERVES_synchronize_preserves_to_file
	.loc 1 1730 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L62
	.loc 1 1730 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L62:
	.loc 1 1731 0 is_stmt 1
	mov	r0, #0
	bl	init_battery_backed_poc_preserves
	.loc 1 1733 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L63
	.loc 1 1733 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L63:
	.loc 1 1734 0 is_stmt 1
	bl	POC_PRESERVES_synchronize_preserves_to_file
	.loc 1 1741 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L64
	.loc 1 1741 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L64:
	.loc 1 1742 0 is_stmt 1
	mov	r0, #0
	bl	init_battery_backed_lights_preserves
	.loc 1 1749 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L65
	.loc 1 1749 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L65:
	.loc 1 1750 0 is_stmt 1
	mov	r0, #0
	bl	init_battery_backed_foal_irri
	.loc 1 1756 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L66
	.loc 1 1756 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L66:
	.loc 1 1757 0 is_stmt 1
	mov	r0, #0
	bl	init_battery_backed_foal_lights
	.loc 1 1759 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L67
	.loc 1 1759 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L67:
	.loc 1 1760 0 is_stmt 1
	mov	r0, #0
	bl	init_battery_backed_general_use
	.loc 1 1764 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L68
	.loc 1 1764 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L68:
	.loc 1 1765 0 is_stmt 1
	mov	r0, #0
	bl	init_battery_backed_chain_members
	.loc 1 1778 0
	ldr	r0, .L79+84
	mov	r1, #0
	mov	r2, #48
	bl	memset
	.loc 1 1780 0
	ldr	r0, .L79+84
	mov	r1, #48
	ldr	r2, .L79+88
	ldr	r3, .L79+92
	bl	snprintf
	.loc 1 1784 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L69
	.loc 1 1784 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L69:
	.loc 1 1793 0 is_stmt 1
	ldr	r0, .L79+96
	mov	r1, #0
	mov	r2, #3168
	bl	memset
	.loc 1 1797 0
	ldr	r0, .L79+100
	mov	r1, #0
	mov	r2, #96
	bl	memset
	.loc 1 1807 0
	ldr	r3, .L79+104
	str	r3, [fp, #-8]
	.loc 1 1809 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L70
.L77:
	.loc 1 1811 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L71
.LBB2:
	.loc 1 1813 0
	ldr	r3, .L79+76
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L72
	.loc 1 1813 0 is_stmt 0 discriminator 1
	mov	r0, #0
	bl	vTaskSuspend
.L72:
	.loc 1 1816 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r1, [r3, #12]
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #16]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #20]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	ldr	r0, [fp, #-8]
	ldr	r0, [r0, #28]
	str	r0, [sp, #0]
	sub	r0, fp, #20
	str	r0, [sp, #4]
	mov	r0, #0
	str	r0, [sp, #8]
	mov	r0, #0
	str	r0, [sp, #12]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	ldr	r3, [fp, #-8]
	bl	xTaskGenericCreate
	.loc 1 1829 0
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #5
	add	r3, r3, r2
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r3, .L79+96
	add	r3, r2, r3
	mov	r0, r3
	bl	vPortSaveVFPRegisters
	ldr	r1, [fp, #-20]
	ldr	r0, .L79+96
	ldr	r2, [fp, #-12]
	mov	r3, r2
	mov	r3, r3, asl #5
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	mov	r0, r1
	mov	r1, r3
	bl	vTaskSetApplicationTaskTag
	.loc 1 1832 0
	ldr	r1, [fp, #-20]
	ldr	r3, .L79+100
	ldr	r2, [fp, #-12]
	str	r1, [r3, r2, asl #2]
	.loc 1 1837 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, .L79+108
	cmp	r2, r3
	bne	.L73
	.loc 1 1837 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	cmp	r3, #0
	bne	.L73
	.loc 1 1839 0 is_stmt 1
	ldr	r2, [fp, #-20]
	ldr	r3, .L79+112
	str	r2, [r3, #0]
.L73:
	.loc 1 1842 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, .L79+108
	cmp	r2, r3
	bne	.L74
	.loc 1 1842 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	cmp	r3, #1
	bne	.L74
	.loc 1 1844 0 is_stmt 1
	ldr	r2, [fp, #-20]
	ldr	r3, .L79+116
	str	r2, [r3, #0]
.L74:
	.loc 1 1851 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, .L79+120
	cmp	r2, r3
	bne	.L75
	.loc 1 1853 0
	ldr	r2, [fp, #-20]
	ldr	r3, .L79+124
	str	r2, [r3, #0]
.L75:
	.loc 1 1856 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, .L79+128
	cmp	r2, r3
	bne	.L76
	.loc 1 1856 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	cmp	r3, #0
	bne	.L76
	.loc 1 1858 0 is_stmt 1
	ldr	r2, [fp, #-20]
	ldr	r3, .L79+132
	str	r2, [r3, #0]
.L76:
	.loc 1 1861 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #12]
	ldr	r3, .L79+136
	cmp	r2, r3
	bne	.L71
	.loc 1 1861 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	cmp	r3, #0
	bne	.L71
	.loc 1 1863 0 is_stmt 1
	ldr	r2, [fp, #-20]
	ldr	r3, .L79+140
	str	r2, [r3, #0]
.L71:
.LBE2:
	.loc 1 1809 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-8]
	add	r3, r3, #32
	str	r3, [fp, #-8]
.L70:
	.loc 1 1809 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #23
	bls	.L77
	.loc 1 1874 0 is_stmt 1
	mov	r0, #20
	bl	CONTROLLER_INITIATED_post_event
	.loc 1 1879 0
	mov	r0, #0
	bl	vTaskDelete
.L78:
	.loc 1 1883 0 discriminator 1
	b	.L78
.L80:
	.align	2
.L79:
	.word	wdt_task_handle
	.word	wdt_monitoring_task
	.word	.LC27
	.word	wdt_task_FLOP_registers
	.word	startup_rtc_task_sync_semaphore
	.word	epson_rtc_task_handle
	.word	EPSON_rtc_control_task
	.word	.LC28
	.word	epson_RTC_task_FLOP_registers
	.word	timer_task_handle
	.word	timer_task_FLOP_registers
	.word	powerfail_task_handle
	.word	system_shutdown_task
	.word	.LC29
	.word	powerfail_task_FLOP_registers
	.word	ci_alerts_timer_callback
	.word	.LC30
	.word	180000
	.word	cics
	.word	restart_info
	.word	display_model_is
	.word	restart_info+16
	.word	.LC31
	.word	-2147483584
	.word	task_table_FLOP_registers
	.word	created_task_handles
	.word	Task_Table
	.word	FLASH_STORAGE_flash_storage_task
	.word	flash_storage_0_task_handle
	.word	flash_storage_1_task_handle
	.word	COMM_MNGR_task
	.word	comm_mngr_task_handle
	.word	ring_buffer_analysis_task
	.word	port_tp_ring_buffer_analysis_task_handle
	.word	serial_driver_task
	.word	port_tp_serial_driver_task_handle
.LFE4:
	.size	system_startup_task, .-system_startup_task
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI10-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/projdefs.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/task.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/serial.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/serport_drvr.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/flow_recorder.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/controller_initiated.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/gpio_setup.h"
	.file 19 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1760
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF272
	.byte	0x1
	.4byte	.LASF273
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x2
	.byte	0x47
	.4byte	0x45
	.uleb128 0x5
	.byte	0x4
	.4byte	0x4b
	.uleb128 0x6
	.byte	0x1
	.4byte	0x57
	.uleb128 0x7
	.4byte	0x57
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x4
	.byte	0x63
	.4byte	0x57
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x5
	.byte	0x57
	.4byte	0x57
	.uleb128 0x4
	.4byte	.LASF12
	.byte	0x6
	.byte	0x4c
	.4byte	0x99
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x7
	.byte	0x65
	.4byte	0x57
	.uleb128 0x9
	.4byte	0x6e
	.4byte	0xca
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF14
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x8
	.byte	0x3a
	.4byte	0x6e
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x8
	.byte	0x4c
	.4byte	0x33
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x8
	.byte	0x5e
	.4byte	0xf2
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF18
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x8
	.byte	0x99
	.4byte	0xf2
	.uleb128 0xb
	.4byte	0xe7
	.uleb128 0x9
	.4byte	0xe7
	.4byte	0x119
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xc
	.byte	0x8
	.byte	0x9
	.byte	0x14
	.4byte	0x13e
	.uleb128 0xd
	.4byte	.LASF20
	.byte	0x9
	.byte	0x17
	.4byte	0x13e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF21
	.byte	0x9
	.byte	0x1a
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0xd1
	.uleb128 0x4
	.4byte	.LASF22
	.byte	0x9
	.byte	0x1c
	.4byte	0x119
	.uleb128 0xc
	.byte	0x6
	.byte	0xa
	.byte	0x22
	.4byte	0x170
	.uleb128 0xe
	.ascii	"T\000"
	.byte	0xa
	.byte	0x24
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.ascii	"D\000"
	.byte	0xa
	.byte	0x26
	.4byte	0xdc
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF23
	.byte	0xa
	.byte	0x28
	.4byte	0x14f
	.uleb128 0xc
	.byte	0x8
	.byte	0xb
	.byte	0x2e
	.4byte	0x1e6
	.uleb128 0xd
	.4byte	.LASF24
	.byte	0xb
	.byte	0x33
	.4byte	0xd1
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF25
	.byte	0xb
	.byte	0x35
	.4byte	0xd1
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0xd
	.4byte	.LASF26
	.byte	0xb
	.byte	0x38
	.4byte	0xd1
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xd
	.4byte	.LASF27
	.byte	0xb
	.byte	0x3c
	.4byte	0xd1
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.uleb128 0xd
	.4byte	.LASF28
	.byte	0xb
	.byte	0x40
	.4byte	0xd1
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF29
	.byte	0xb
	.byte	0x42
	.4byte	0xd1
	.byte	0x2
	.byte	0x23
	.uleb128 0x5
	.uleb128 0xd
	.4byte	.LASF30
	.byte	0xb
	.byte	0x44
	.4byte	0xd1
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.byte	0
	.uleb128 0x4
	.4byte	.LASF31
	.byte	0xb
	.byte	0x46
	.4byte	0x17b
	.uleb128 0xc
	.byte	0x10
	.byte	0xb
	.byte	0x54
	.4byte	0x25c
	.uleb128 0xd
	.4byte	.LASF32
	.byte	0xb
	.byte	0x57
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF33
	.byte	0xb
	.byte	0x5a
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xd
	.4byte	.LASF34
	.byte	0xb
	.byte	0x5b
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF35
	.byte	0xb
	.byte	0x5c
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xd
	.4byte	.LASF36
	.byte	0xb
	.byte	0x5d
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF37
	.byte	0xb
	.byte	0x5f
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0xd
	.4byte	.LASF38
	.byte	0xb
	.byte	0x61
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x4
	.4byte	.LASF39
	.byte	0xb
	.byte	0x63
	.4byte	0x1f1
	.uleb128 0xc
	.byte	0x18
	.byte	0xb
	.byte	0x66
	.4byte	0x2c4
	.uleb128 0xd
	.4byte	.LASF40
	.byte	0xb
	.byte	0x69
	.4byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF41
	.byte	0xb
	.byte	0x6b
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF42
	.byte	0xb
	.byte	0x6c
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF43
	.byte	0xb
	.byte	0x6d
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xd
	.4byte	.LASF44
	.byte	0xb
	.byte	0x6e
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF45
	.byte	0xb
	.byte	0x6f
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x4
	.4byte	.LASF46
	.byte	0xb
	.byte	0x71
	.4byte	0x267
	.uleb128 0xc
	.byte	0x14
	.byte	0xb
	.byte	0x74
	.4byte	0x31e
	.uleb128 0xd
	.4byte	.LASF47
	.byte	0xb
	.byte	0x7b
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF48
	.byte	0xb
	.byte	0x7d
	.4byte	0x31e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF49
	.byte	0xb
	.byte	0x81
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF50
	.byte	0xb
	.byte	0x86
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xd
	.4byte	.LASF51
	.byte	0xb
	.byte	0x8d
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0xca
	.uleb128 0x4
	.4byte	.LASF52
	.byte	0xb
	.byte	0x8f
	.4byte	0x2cf
	.uleb128 0xc
	.byte	0xc
	.byte	0xb
	.byte	0x91
	.4byte	0x362
	.uleb128 0xd
	.4byte	.LASF47
	.byte	0xb
	.byte	0x97
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF48
	.byte	0xb
	.byte	0x9a
	.4byte	0x31e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF49
	.byte	0xb
	.byte	0x9e
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x4
	.4byte	.LASF53
	.byte	0xb
	.byte	0xa0
	.4byte	0x32f
	.uleb128 0xf
	.2byte	0x1074
	.byte	0xb
	.byte	0xa6
	.4byte	0x462
	.uleb128 0xd
	.4byte	.LASF54
	.byte	0xb
	.byte	0xa8
	.4byte	0x462
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF55
	.byte	0xb
	.byte	0xac
	.4byte	0x104
	.byte	0x3
	.byte	0x23
	.uleb128 0x1000
	.uleb128 0xd
	.4byte	.LASF56
	.byte	0xb
	.byte	0xb0
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x1004
	.uleb128 0xd
	.4byte	.LASF57
	.byte	0xb
	.byte	0xb2
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x1008
	.uleb128 0xd
	.4byte	.LASF58
	.byte	0xb
	.byte	0xb8
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x100c
	.uleb128 0xd
	.4byte	.LASF59
	.byte	0xb
	.byte	0xbb
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x1010
	.uleb128 0xd
	.4byte	.LASF60
	.byte	0xb
	.byte	0xbe
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x1014
	.uleb128 0xd
	.4byte	.LASF61
	.byte	0xb
	.byte	0xc2
	.4byte	0xe7
	.byte	0x3
	.byte	0x23
	.uleb128 0x1018
	.uleb128 0xe
	.ascii	"ph\000"
	.byte	0xb
	.byte	0xc7
	.4byte	0x25c
	.byte	0x3
	.byte	0x23
	.uleb128 0x101c
	.uleb128 0xe
	.ascii	"dh\000"
	.byte	0xb
	.byte	0xca
	.4byte	0x2c4
	.byte	0x3
	.byte	0x23
	.uleb128 0x102c
	.uleb128 0xe
	.ascii	"sh\000"
	.byte	0xb
	.byte	0xcd
	.4byte	0x324
	.byte	0x3
	.byte	0x23
	.uleb128 0x1044
	.uleb128 0xe
	.ascii	"th\000"
	.byte	0xb
	.byte	0xd1
	.4byte	0x362
	.byte	0x3
	.byte	0x23
	.uleb128 0x1058
	.uleb128 0xd
	.4byte	.LASF62
	.byte	0xb
	.byte	0xd5
	.4byte	0x473
	.byte	0x3
	.byte	0x23
	.uleb128 0x1064
	.uleb128 0xd
	.4byte	.LASF63
	.byte	0xb
	.byte	0xd7
	.4byte	0x473
	.byte	0x3
	.byte	0x23
	.uleb128 0x1068
	.uleb128 0xd
	.4byte	.LASF64
	.byte	0xb
	.byte	0xd9
	.4byte	0x473
	.byte	0x3
	.byte	0x23
	.uleb128 0x106c
	.uleb128 0xd
	.4byte	.LASF65
	.byte	0xb
	.byte	0xdb
	.4byte	0x473
	.byte	0x3
	.byte	0x23
	.uleb128 0x1070
	.byte	0
	.uleb128 0x9
	.4byte	0xd1
	.4byte	0x473
	.uleb128 0x10
	.4byte	0x25
	.2byte	0xfff
	.byte	0
	.uleb128 0xb
	.4byte	0xf9
	.uleb128 0x4
	.4byte	.LASF66
	.byte	0xb
	.byte	0xdd
	.4byte	0x36d
	.uleb128 0xc
	.byte	0x24
	.byte	0xb
	.byte	0xe1
	.4byte	0x50b
	.uleb128 0xd
	.4byte	.LASF67
	.byte	0xb
	.byte	0xe3
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF68
	.byte	0xb
	.byte	0xe5
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF69
	.byte	0xb
	.byte	0xe7
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF70
	.byte	0xb
	.byte	0xe9
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xd
	.4byte	.LASF71
	.byte	0xb
	.byte	0xeb
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF72
	.byte	0xb
	.byte	0xfa
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xd
	.4byte	.LASF73
	.byte	0xb
	.byte	0xfc
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xd
	.4byte	.LASF74
	.byte	0xb
	.byte	0xfe
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x11
	.4byte	.LASF75
	.byte	0xb
	.2byte	0x100
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0x12
	.4byte	.LASF76
	.byte	0xb
	.2byte	0x102
	.4byte	0x483
	.uleb128 0xc
	.byte	0x20
	.byte	0xc
	.byte	0x1e
	.4byte	0x590
	.uleb128 0xd
	.4byte	.LASF77
	.byte	0xc
	.byte	0x20
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF78
	.byte	0xc
	.byte	0x22
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF79
	.byte	0xc
	.byte	0x24
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF80
	.byte	0xc
	.byte	0x26
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xd
	.4byte	.LASF81
	.byte	0xc
	.byte	0x28
	.4byte	0x590
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF82
	.byte	0xc
	.byte	0x2a
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xd
	.4byte	.LASF83
	.byte	0xc
	.byte	0x2c
	.4byte	0x57
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xd
	.4byte	.LASF84
	.byte	0xc
	.byte	0x2e
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x13
	.4byte	0x595
	.uleb128 0x5
	.byte	0x4
	.4byte	0x59b
	.uleb128 0x13
	.4byte	0xca
	.uleb128 0x4
	.4byte	.LASF85
	.byte	0xc
	.byte	0x30
	.4byte	0x517
	.uleb128 0x9
	.4byte	0xca
	.4byte	0x5bb
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0x9
	.4byte	0xca
	.4byte	0x5cb
	.uleb128 0xa
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0xf
	.2byte	0x10b8
	.byte	0xd
	.byte	0x48
	.4byte	0x655
	.uleb128 0xd
	.4byte	.LASF86
	.byte	0xd
	.byte	0x4a
	.4byte	0x99
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF87
	.byte	0xd
	.byte	0x4c
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF88
	.byte	0xd
	.byte	0x53
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF89
	.byte	0xd
	.byte	0x55
	.4byte	0x104
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xd
	.4byte	.LASF90
	.byte	0xd
	.byte	0x57
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF91
	.byte	0xd
	.byte	0x59
	.4byte	0x655
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xd
	.4byte	.LASF92
	.byte	0xd
	.byte	0x5b
	.4byte	0x478
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xd
	.4byte	.LASF93
	.byte	0xd
	.byte	0x5d
	.4byte	0x50b
	.byte	0x3
	.byte	0x23
	.uleb128 0x1090
	.uleb128 0xd
	.4byte	.LASF94
	.byte	0xd
	.byte	0x61
	.4byte	0xaf
	.byte	0x3
	.byte	0x23
	.uleb128 0x10b4
	.byte	0
	.uleb128 0xb
	.4byte	0x1e6
	.uleb128 0x4
	.4byte	.LASF95
	.byte	0xd
	.byte	0x63
	.4byte	0x5cb
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF96
	.uleb128 0x9
	.4byte	0xe7
	.4byte	0x67c
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0xc
	.byte	0x1c
	.byte	0xe
	.byte	0x8f
	.4byte	0x6e7
	.uleb128 0xd
	.4byte	.LASF97
	.byte	0xe
	.byte	0x94
	.4byte	0x13e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF98
	.byte	0xe
	.byte	0x99
	.4byte	0x13e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.4byte	.LASF99
	.byte	0xe
	.byte	0x9e
	.4byte	0x13e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xd
	.4byte	.LASF100
	.byte	0xe
	.byte	0xa3
	.4byte	0x13e
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xd
	.4byte	.LASF101
	.byte	0xe
	.byte	0xad
	.4byte	0x13e
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF102
	.byte	0xe
	.byte	0xb8
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xd
	.4byte	.LASF103
	.byte	0xe
	.byte	0xbe
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x4
	.4byte	.LASF104
	.byte	0xe
	.byte	0xc2
	.4byte	0x67c
	.uleb128 0x9
	.4byte	0xe7
	.4byte	0x702
	.uleb128 0xa
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0xf
	.2byte	0x12c
	.byte	0xf
	.byte	0xb5
	.4byte	0x7f1
	.uleb128 0xd
	.4byte	.LASF105
	.byte	0xf
	.byte	0xbc
	.4byte	0x5bb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.4byte	.LASF106
	.byte	0xf
	.byte	0xc1
	.4byte	0x5ab
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xd
	.4byte	.LASF107
	.byte	0xf
	.byte	0xcd
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0xd
	.4byte	.LASF108
	.byte	0xf
	.byte	0xd5
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xd
	.4byte	.LASF109
	.byte	0xf
	.byte	0xd7
	.4byte	0x170
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xd
	.4byte	.LASF110
	.byte	0xf
	.byte	0xdb
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xd
	.4byte	.LASF111
	.byte	0xf
	.byte	0xe1
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xd
	.4byte	.LASF112
	.byte	0xf
	.byte	0xe3
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xd
	.4byte	.LASF113
	.byte	0xf
	.byte	0xea
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0xd
	.4byte	.LASF114
	.byte	0xf
	.byte	0xec
	.4byte	0x170
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0xd
	.4byte	.LASF115
	.byte	0xf
	.byte	0xee
	.4byte	0x5ab
	.byte	0x2
	.byte	0x23
	.uleb128 0x66
	.uleb128 0xd
	.4byte	.LASF116
	.byte	0xf
	.byte	0xf0
	.4byte	0x5ab
	.byte	0x3
	.byte	0x23
	.uleb128 0x96
	.uleb128 0xd
	.4byte	.LASF117
	.byte	0xf
	.byte	0xf5
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0xd
	.4byte	.LASF118
	.byte	0xf
	.byte	0xf7
	.4byte	0x170
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0xd
	.4byte	.LASF119
	.byte	0xf
	.byte	0xfa
	.4byte	0x7f1
	.byte	0x3
	.byte	0x23
	.uleb128 0xd2
	.uleb128 0xd
	.4byte	.LASF120
	.byte	0xf
	.byte	0xfe
	.4byte	0x801
	.byte	0x3
	.byte	0x23
	.uleb128 0xea
	.byte	0
	.uleb128 0x9
	.4byte	0xca
	.4byte	0x801
	.uleb128 0xa
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x9
	.4byte	0xca
	.4byte	0x811
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3f
	.byte	0
	.uleb128 0x12
	.4byte	.LASF121
	.byte	0xf
	.2byte	0x105
	.4byte	0x702
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF122
	.uleb128 0x14
	.byte	0x18
	.byte	0x10
	.2byte	0x14b
	.4byte	0x879
	.uleb128 0x11
	.4byte	.LASF123
	.byte	0x10
	.2byte	0x150
	.4byte	0x144
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x11
	.4byte	.LASF124
	.byte	0x10
	.2byte	0x157
	.4byte	0x879
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x11
	.4byte	.LASF125
	.byte	0x10
	.2byte	0x159
	.4byte	0x87f
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x11
	.4byte	.LASF126
	.byte	0x10
	.2byte	0x15b
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x11
	.4byte	.LASF127
	.byte	0x10
	.2byte	0x15d
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0xf9
	.uleb128 0x5
	.byte	0x4
	.4byte	0x6e7
	.uleb128 0x12
	.4byte	.LASF128
	.byte	0x10
	.2byte	0x15f
	.4byte	0x824
	.uleb128 0x14
	.byte	0xbc
	.byte	0x10
	.2byte	0x163
	.4byte	0xb20
	.uleb128 0x11
	.4byte	.LASF129
	.byte	0x10
	.2byte	0x165
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x11
	.4byte	.LASF130
	.byte	0x10
	.2byte	0x167
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x11
	.4byte	.LASF131
	.byte	0x10
	.2byte	0x16c
	.4byte	0x885
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x11
	.4byte	.LASF132
	.byte	0x10
	.2byte	0x173
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x11
	.4byte	.LASF133
	.byte	0x10
	.2byte	0x179
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x11
	.4byte	.LASF134
	.byte	0x10
	.2byte	0x17e
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x11
	.4byte	.LASF135
	.byte	0x10
	.2byte	0x184
	.4byte	0xe7
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x11
	.4byte	.LASF136
	.byte	0x10
	.2byte	0x18a
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x11
	.4byte	.LASF137
	.byte	0x10
	.2byte	0x18c
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x11
	.4byte	.LASF138
	.byte	0x10
	.2byte	0x191
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x11
	.4byte	.LASF139
	.byte	0x10
	.2byte	0x197
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x11
	.4byte	.LASF140
	.byte	0x10
	.2byte	0x1a0
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x11
	.4byte	.LASF141
	.byte	0x10
	.2byte	0x1a8
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x11
	.4byte	.LASF142
	.byte	0x10
	.2byte	0x1b2
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x11
	.4byte	.LASF143
	.byte	0x10
	.2byte	0x1b8
	.4byte	0x87f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x11
	.4byte	.LASF144
	.byte	0x10
	.2byte	0x1c2
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x11
	.4byte	.LASF145
	.byte	0x10
	.2byte	0x1c8
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x11
	.4byte	.LASF146
	.byte	0x10
	.2byte	0x1cc
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x11
	.4byte	.LASF147
	.byte	0x10
	.2byte	0x1d0
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x11
	.4byte	.LASF148
	.byte	0x10
	.2byte	0x1d4
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x11
	.4byte	.LASF149
	.byte	0x10
	.2byte	0x1d8
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x11
	.4byte	.LASF150
	.byte	0x10
	.2byte	0x1dc
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x11
	.4byte	.LASF151
	.byte	0x10
	.2byte	0x1e0
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x11
	.4byte	.LASF152
	.byte	0x10
	.2byte	0x1e6
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x11
	.4byte	.LASF153
	.byte	0x10
	.2byte	0x1e8
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x11
	.4byte	.LASF154
	.byte	0x10
	.2byte	0x1ef
	.4byte	0xf9
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x11
	.4byte	.LASF155
	.byte	0x10
	.2byte	0x1f1
	.4byte	0xaf
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x11
	.4byte	.LASF156
	.byte	0x10
	.2byte	0x1f9
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x11
	.4byte	.LASF157
	.byte	0x10
	.2byte	0x1fb
	.4byte	0xaf
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x11
	.4byte	.LASF158
	.byte	0x10
	.2byte	0x1fd
	.4byte	0xe7
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x11
	.4byte	.LASF159
	.byte	0x10
	.2byte	0x203
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x11
	.4byte	.LASF160
	.byte	0x10
	.2byte	0x20d
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x11
	.4byte	.LASF161
	.byte	0x10
	.2byte	0x20f
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x11
	.4byte	.LASF162
	.byte	0x10
	.2byte	0x215
	.4byte	0xaf
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x11
	.4byte	.LASF163
	.byte	0x10
	.2byte	0x21c
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x11
	.4byte	.LASF164
	.byte	0x10
	.2byte	0x21e
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x11
	.4byte	.LASF165
	.byte	0x10
	.2byte	0x222
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x11
	.4byte	.LASF166
	.byte	0x10
	.2byte	0x226
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x11
	.4byte	.LASF167
	.byte	0x10
	.2byte	0x228
	.4byte	0xf9
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x11
	.4byte	.LASF168
	.byte	0x10
	.2byte	0x237
	.4byte	0x99
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x11
	.4byte	.LASF169
	.byte	0x10
	.2byte	0x23f
	.4byte	0xaf
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0x11
	.4byte	.LASF170
	.byte	0x10
	.2byte	0x249
	.4byte	0xaf
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.byte	0
	.uleb128 0x12
	.4byte	.LASF171
	.byte	0x10
	.2byte	0x24b
	.4byte	0x891
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF172
	.byte	0x1
	.2byte	0x34e
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x15
	.byte	0x1
	.4byte	.LASF173
	.byte	0x1
	.2byte	0x364
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x16
	.4byte	.LASF174
	.byte	0x1
	.2byte	0x377
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0xb7f
	.uleb128 0x17
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x379
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.4byte	.LASF175
	.byte	0x1
	.2byte	0x445
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0xba6
	.uleb128 0x17
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x44e
	.4byte	0xe7
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF274
	.byte	0x1
	.2byte	0x4ae
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0xc3a
	.uleb128 0x19
	.4byte	.LASF275
	.byte	0x1
	.2byte	0x4ae
	.4byte	0x57
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x4ca
	.byte	0x1
	.byte	0x1
	.4byte	0xbe4
	.uleb128 0x7
	.4byte	0x57
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF176
	.byte	0x1
	.2byte	0x536
	.4byte	0xf9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1b
	.4byte	.LASF177
	.byte	0x1
	.2byte	0x709
	.4byte	0xc3a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.ascii	"n\000"
	.byte	0x1
	.2byte	0x70b
	.4byte	0xe7
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.4byte	.LASF178
	.byte	0x1
	.2byte	0x70d
	.4byte	0x8e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1c
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x1d
	.byte	0x1
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x725
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	0x57
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x5a0
	.uleb128 0x1e
	.4byte	.LASF179
	.byte	0x11
	.byte	0x30
	.4byte	0xc51
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x13
	.4byte	0xba
	.uleb128 0x1e
	.4byte	.LASF180
	.byte	0x11
	.byte	0x34
	.4byte	0xc67
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x13
	.4byte	0xba
	.uleb128 0x1e
	.4byte	.LASF181
	.byte	0x11
	.byte	0x36
	.4byte	0xc7d
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x13
	.4byte	0xba
	.uleb128 0x1e
	.4byte	.LASF182
	.byte	0x11
	.byte	0x38
	.4byte	0xc93
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x13
	.4byte	0xba
	.uleb128 0x1f
	.4byte	.LASF183
	.byte	0x12
	.byte	0x3d
	.4byte	0xe7
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x5a0
	.4byte	0xcb5
	.uleb128 0xa
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF184
	.byte	0xc
	.byte	0x38
	.4byte	0xcc2
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	0xca5
	.uleb128 0x1f
	.4byte	.LASF185
	.byte	0xc
	.byte	0x48
	.4byte	0x8e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF186
	.byte	0xc
	.byte	0x48
	.4byte	0x8e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF187
	.byte	0xc
	.byte	0x48
	.4byte	0x8e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF188
	.byte	0xc
	.byte	0x4b
	.4byte	0x8e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF189
	.byte	0xc
	.byte	0x50
	.4byte	0x8e
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF190
	.byte	0xc
	.byte	0x52
	.4byte	0x8e
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x8e
	.4byte	0xd25
	.uleb128 0xa
	.4byte	0x25
	.byte	0x17
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF191
	.byte	0xc
	.byte	0x55
	.4byte	0xd15
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF192
	.byte	0xc
	.byte	0x64
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF193
	.byte	0xc
	.byte	0x6b
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF194
	.byte	0xc
	.byte	0x6f
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF195
	.byte	0xc
	.byte	0x71
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0xa4
	.4byte	0xd76
	.uleb128 0xa
	.4byte	0x25
	.byte	0x4
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF196
	.byte	0xc
	.byte	0x75
	.4byte	0xd66
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF197
	.byte	0xc
	.byte	0x78
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF198
	.byte	0xc
	.byte	0x7b
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF199
	.byte	0xc
	.byte	0x7d
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF200
	.byte	0xc
	.byte	0x80
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF201
	.byte	0xc
	.byte	0x83
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF202
	.byte	0xc
	.byte	0x86
	.4byte	0xd66
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF203
	.byte	0xc
	.byte	0x89
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF204
	.byte	0xc
	.byte	0x8f
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF205
	.byte	0xc
	.byte	0x98
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF206
	.byte	0xc
	.byte	0x9f
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF207
	.byte	0xc
	.byte	0xa5
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF208
	.byte	0xc
	.byte	0xaa
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF209
	.byte	0xc
	.byte	0xae
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF210
	.byte	0xc
	.byte	0xb1
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF211
	.byte	0xc
	.byte	0xb4
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF212
	.byte	0xc
	.byte	0xb7
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF213
	.byte	0xc
	.byte	0xba
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF214
	.byte	0xc
	.byte	0xbd
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF215
	.byte	0xc
	.byte	0xc0
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF216
	.byte	0xc
	.byte	0xc3
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF217
	.byte	0xc
	.byte	0xc6
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF218
	.byte	0xc
	.byte	0xc9
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF219
	.byte	0xc
	.byte	0xcc
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF220
	.byte	0xc
	.byte	0xcf
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF221
	.byte	0xc
	.byte	0xd2
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF222
	.byte	0xc
	.byte	0xd5
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF223
	.byte	0xc
	.byte	0xd8
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF224
	.byte	0xc
	.byte	0xfe
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF225
	.byte	0xc
	.2byte	0x101
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF226
	.byte	0xc
	.2byte	0x104
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF227
	.byte	0xc
	.2byte	0x107
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF228
	.byte	0xc
	.2byte	0x10b
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF229
	.byte	0xc
	.2byte	0x111
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF230
	.byte	0xc
	.2byte	0x114
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF231
	.byte	0xc
	.2byte	0x117
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF232
	.byte	0xc
	.2byte	0x11b
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF233
	.byte	0xc
	.2byte	0x11d
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF234
	.byte	0xc
	.2byte	0x11f
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF235
	.byte	0xc
	.2byte	0x122
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF236
	.byte	0xc
	.2byte	0x128
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF237
	.byte	0xc
	.2byte	0x12a
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF238
	.byte	0xc
	.2byte	0x132
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF239
	.byte	0xc
	.2byte	0x137
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF240
	.byte	0xc
	.2byte	0x139
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF241
	.byte	0xc
	.2byte	0x13c
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF242
	.byte	0xc
	.2byte	0x13e
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF243
	.byte	0xc
	.2byte	0x141
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF244
	.byte	0xc
	.2byte	0x147
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF245
	.byte	0xc
	.2byte	0x149
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF246
	.byte	0xc
	.2byte	0x14c
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF247
	.byte	0xc
	.2byte	0x14e
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF248
	.byte	0xc
	.2byte	0x150
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF249
	.byte	0xc
	.2byte	0x153
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF250
	.byte	0xc
	.2byte	0x155
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF251
	.byte	0xc
	.2byte	0x159
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF252
	.byte	0xc
	.2byte	0x15b
	.4byte	0x99
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x65a
	.4byte	0x1087
	.uleb128 0xa
	.4byte	0x25
	.byte	0x4
	.byte	0
	.uleb128 0x1f
	.4byte	.LASF253
	.byte	0xd
	.byte	0x68
	.4byte	0x1077
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	.LASF254
	.byte	0x13
	.byte	0x33
	.4byte	0x10a5
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x13
	.4byte	0x109
	.uleb128 0x1e
	.4byte	.LASF255
	.byte	0x13
	.byte	0x3f
	.4byte	0x10bb
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x13
	.4byte	0x66c
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0xf
	.2byte	0x108
	.4byte	0x811
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x10
	.2byte	0x24f
	.4byte	0xb20
	.byte	0x1
	.byte	0x1
	.uleb128 0x1f
	.4byte	.LASF258
	.byte	0x1
	.byte	0x89
	.4byte	0xa4
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0xe7
	.4byte	0x10f9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x20
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x1b4
	.4byte	0x10e9
	.byte	0x5
	.byte	0x3
	.4byte	epson_RTC_task_FLOP_registers
	.uleb128 0x1b
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x1b6
	.4byte	0x10e9
	.byte	0x5
	.byte	0x3
	.4byte	powerfail_task_FLOP_registers
	.uleb128 0x1b
	.4byte	.LASF261
	.byte	0x1
	.2byte	0x1b8
	.4byte	0x10e9
	.byte	0x5
	.byte	0x3
	.4byte	wdt_task_FLOP_registers
	.uleb128 0x20
	.4byte	.LASF262
	.byte	0x1
	.2byte	0x1bc
	.4byte	0x10e9
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0xe7
	.4byte	0x1153
	.uleb128 0xa
	.4byte	0x25
	.byte	0x17
	.uleb128 0xa
	.4byte	0x25
	.byte	0x20
	.byte	0
	.uleb128 0x1b
	.4byte	.LASF263
	.byte	0x1
	.2byte	0x1be
	.4byte	0x113d
	.byte	0x5
	.byte	0x3
	.4byte	task_table_FLOP_registers
	.uleb128 0x1b
	.4byte	.LASF264
	.byte	0x1
	.2byte	0x1df
	.4byte	0x8e
	.byte	0x5
	.byte	0x3
	.4byte	comm_mngr_task_handle
	.uleb128 0x1b
	.4byte	.LASF265
	.byte	0x1
	.2byte	0x1e1
	.4byte	0x8e
	.byte	0x5
	.byte	0x3
	.4byte	port_tp_ring_buffer_analysis_task_handle
	.uleb128 0x1b
	.4byte	.LASF266
	.byte	0x1
	.2byte	0x1e3
	.4byte	0x8e
	.byte	0x5
	.byte	0x3
	.4byte	port_tp_serial_driver_task_handle
	.uleb128 0x1f
	.4byte	.LASF183
	.byte	0x12
	.byte	0x3d
	.4byte	0xe7
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF184
	.byte	0x1
	.2byte	0x207
	.4byte	0x11bb
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Task_Table
	.uleb128 0x13
	.4byte	0xca5
	.uleb128 0x21
	.4byte	.LASF267
	.byte	0x1
	.2byte	0x1b2
	.4byte	0x10e9
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	startup_task_FLOP_registers
	.uleb128 0x21
	.4byte	.LASF268
	.byte	0x1
	.2byte	0x1c8
	.4byte	0x8e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	startup_task_handle
	.uleb128 0x21
	.4byte	.LASF185
	.byte	0x1
	.2byte	0x1c8
	.4byte	0x8e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	epson_rtc_task_handle
	.uleb128 0x21
	.4byte	.LASF186
	.byte	0x1
	.2byte	0x1c8
	.4byte	0x8e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	powerfail_task_handle
	.uleb128 0x21
	.4byte	.LASF187
	.byte	0x1
	.2byte	0x1c8
	.4byte	0x8e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	wdt_task_handle
	.uleb128 0x21
	.4byte	.LASF188
	.byte	0x1
	.2byte	0x1cb
	.4byte	0x8e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	timer_task_handle
	.uleb128 0x21
	.4byte	.LASF189
	.byte	0x1
	.2byte	0x1d2
	.4byte	0x8e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	flash_storage_0_task_handle
	.uleb128 0x21
	.4byte	.LASF190
	.byte	0x1
	.2byte	0x1d4
	.4byte	0x8e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	flash_storage_1_task_handle
	.uleb128 0x21
	.4byte	.LASF191
	.byte	0x1
	.2byte	0x1e8
	.4byte	0xd15
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	created_task_handles
	.uleb128 0x21
	.4byte	.LASF269
	.byte	0x1
	.2byte	0x1eb
	.4byte	0xe7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	rtc_task_last_x_stamp
	.uleb128 0x21
	.4byte	.LASF270
	.byte	0x1
	.2byte	0x1eb
	.4byte	0xe7
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	pwrfail_task_last_x_stamp
	.uleb128 0x21
	.4byte	.LASF271
	.byte	0x1
	.2byte	0x1ef
	.4byte	0x6f2
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	task_last_execution_stamp
	.uleb128 0x22
	.4byte	.LASF192
	.byte	0x1
	.byte	0x8e
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	startup_rtc_task_sync_semaphore
	.uleb128 0x22
	.4byte	.LASF193
	.byte	0x1
	.byte	0x91
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	alerts_pile_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF194
	.byte	0x1
	.byte	0x95
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Flash_0_MUTEX
	.uleb128 0x22
	.4byte	.LASF195
	.byte	0x1
	.byte	0x97
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Flash_1_MUTEX
	.uleb128 0x22
	.4byte	.LASF196
	.byte	0x1
	.byte	0x9b
	.4byte	0xd66
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	rcvd_data_binary_semaphore
	.uleb128 0x22
	.4byte	.LASF197
	.byte	0x1
	.byte	0xb4
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_program_data_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF198
	.byte	0x1
	.byte	0xb8
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_tpl_in_messages_MUTEX
	.uleb128 0x22
	.4byte	.LASF199
	.byte	0x1
	.byte	0xba
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_tpl_out_messages_MUTEX
	.uleb128 0x22
	.4byte	.LASF200
	.byte	0x1
	.byte	0xbe
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_incoming_messages_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF201
	.byte	0x1
	.byte	0xc1
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_packets_waiting_for_token_MUTEX
	.uleb128 0x22
	.4byte	.LASF202
	.byte	0x1
	.byte	0xc5
	.4byte	0xd66
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	xmit_list_MUTEX
	.uleb128 0x22
	.4byte	.LASF203
	.byte	0x1
	.byte	0xcf
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	ci_and_comm_mngr_activity_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF204
	.byte	0x1
	.byte	0xd5
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	irri_irri_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF205
	.byte	0x1
	.byte	0xdd
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_foal_irri_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF206
	.byte	0x1
	.byte	0xe2
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	comm_mngr_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF207
	.byte	0x1
	.byte	0xe8
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	chain_members_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF208
	.byte	0x1
	.byte	0xed
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	irri_comm_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF209
	.byte	0x1
	.byte	0xf2
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	system_report_completed_records_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF210
	.byte	0x1
	.byte	0xf5
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	poc_report_completed_records_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF211
	.byte	0x1
	.byte	0xf8
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	station_report_data_completed_records_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF212
	.byte	0x1
	.byte	0xfb
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	station_history_completed_records_recursive_MUTEX
	.uleb128 0x22
	.4byte	.LASF213
	.byte	0x1
	.byte	0xfe
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	weather_preserves_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF214
	.byte	0x1
	.2byte	0x101
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	station_preserves_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF215
	.byte	0x1
	.2byte	0x104
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	system_preserves_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF216
	.byte	0x1
	.2byte	0x107
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	poc_preserves_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF217
	.byte	0x1
	.2byte	0x10a
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_system_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF218
	.byte	0x1
	.2byte	0x10d
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_poc_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF219
	.byte	0x1
	.2byte	0x110
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	weather_control_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF220
	.byte	0x1
	.2byte	0x113
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	weather_tables_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF221
	.byte	0x1
	.2byte	0x11c
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	speaker_hardware_MUTEX
	.uleb128 0x21
	.4byte	.LASF222
	.byte	0x1
	.2byte	0x11f
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	tpmicro_data_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF223
	.byte	0x1
	.2byte	0x127
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	pending_changes_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF224
	.byte	0x1
	.2byte	0x12b
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_lights_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF225
	.byte	0x1
	.2byte	0x12e
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	list_foal_lights_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF226
	.byte	0x1
	.2byte	0x131
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	irri_lights_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF227
	.byte	0x1
	.2byte	0x134
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	lights_preserves_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF228
	.byte	0x1
	.2byte	0x138
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	lights_report_completed_records_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF229
	.byte	0x1
	.2byte	0x13e
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	moisture_sensor_items_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF230
	.byte	0x1
	.2byte	0x141
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	moisture_sensor_recorder_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF231
	.byte	0x1
	.2byte	0x144
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	walk_thru_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF232
	.byte	0x1
	.2byte	0x148
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	budget_report_completed_records_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF233
	.byte	0x1
	.2byte	0x14d
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	ci_message_list_MUTEX
	.uleb128 0x21
	.4byte	.LASF234
	.byte	0x1
	.2byte	0x14f
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	code_distribution_control_structure_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF235
	.byte	0x1
	.2byte	0x152
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	chain_sync_control_structure_recursive_MUTEX
	.uleb128 0x21
	.4byte	.LASF236
	.byte	0x1
	.2byte	0x15c
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	router_hub_list_MUTEX
	.uleb128 0x21
	.4byte	.LASF237
	.byte	0x1
	.2byte	0x168
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	router_hub_list_queue
	.uleb128 0x21
	.4byte	.LASF238
	.byte	0x1
	.2byte	0x170
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Contrast_and_Volume_Queue
	.uleb128 0x21
	.4byte	.LASF239
	.byte	0x1
	.2byte	0x173
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Key_Scanner_Queue
	.uleb128 0x21
	.4byte	.LASF240
	.byte	0x1
	.2byte	0x175
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Key_To_Process_Queue
	.uleb128 0x21
	.4byte	.LASF241
	.byte	0x1
	.2byte	0x178
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	FLASH_STORAGE_task_queue_0
	.uleb128 0x21
	.4byte	.LASF242
	.byte	0x1
	.2byte	0x17a
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	FLASH_STORAGE_task_queue_1
	.uleb128 0x21
	.4byte	.LASF243
	.byte	0x1
	.2byte	0x17e
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Display_Command_Queue
	.uleb128 0x21
	.4byte	.LASF244
	.byte	0x1
	.2byte	0x182
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	TPL_IN_event_queue
	.uleb128 0x21
	.4byte	.LASF245
	.byte	0x1
	.2byte	0x185
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	TPL_OUT_event_queue
	.uleb128 0x21
	.4byte	.LASF246
	.byte	0x1
	.2byte	0x188
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	COMM_MNGR_task_queue
	.uleb128 0x21
	.4byte	.LASF247
	.byte	0x1
	.2byte	0x18a
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	CODE_DISTRIBUTION_task_queue
	.uleb128 0x21
	.4byte	.LASF248
	.byte	0x1
	.2byte	0x18c
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	CONTROLLER_INITIATED_task_queue
	.uleb128 0x21
	.4byte	.LASF249
	.byte	0x1
	.2byte	0x18f
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	BYPASS_event_queue
	.uleb128 0x21
	.4byte	.LASF250
	.byte	0x1
	.2byte	0x191
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	RADIO_TEST_task_queue
	.uleb128 0x21
	.4byte	.LASF251
	.byte	0x1
	.2byte	0x196
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Background_Calculation_Queue
	.uleb128 0x21
	.4byte	.LASF252
	.byte	0x1
	.2byte	0x199
	.4byte	0x99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	FTIMES_task_queue
	.uleb128 0x1f
	.4byte	.LASF253
	.byte	0xd
	.byte	0x68
	.4byte	0x1077
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0xf
	.2byte	0x108
	.4byte	0x811
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x10
	.2byte	0x24f
	.4byte	0xb20
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF258
	.byte	0x1
	.byte	0x89
	.4byte	0xa4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	debugio_MUTEX
	.uleb128 0x21
	.4byte	.LASF262
	.byte	0x1
	.2byte	0x1bc
	.4byte	0x10e9
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	timer_task_FLOP_registers
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI5
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI8
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI11
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF203:
	.ascii	"ci_and_comm_mngr_activity_recursive_MUTEX\000"
.LASF97:
	.ascii	"original_allocation\000"
.LASF153:
	.ascii	"send_weather_data_timer\000"
.LASF32:
	.ascii	"packet_index\000"
.LASF202:
	.ascii	"xmit_list_MUTEX\000"
.LASF67:
	.ascii	"errors_overrun\000"
.LASF28:
	.ascii	"o_rts\000"
.LASF104:
	.ascii	"FLOW_RECORDING_CONTROL_STRUCT\000"
.LASF135:
	.ascii	"connection_process_failures\000"
.LASF116:
	.ascii	"exception_description_string\000"
.LASF66:
	.ascii	"UART_RING_BUFFER_s\000"
.LASF65:
	.ascii	"th_tail_caught_index\000"
.LASF88:
	.ascii	"cts_polling_timer\000"
.LASF140:
	.ascii	"alerts_timer\000"
.LASF177:
	.ascii	"pTaskEntry\000"
.LASF187:
	.ascii	"wdt_task_handle\000"
.LASF234:
	.ascii	"code_distribution_control_structure_recursive_MUTEX"
	.ascii	"\000"
.LASF123:
	.ascii	"message\000"
.LASF165:
	.ascii	"waiting_for_et_rain_tables_response\000"
.LASF172:
	.ascii	"elevate_priority_of_ISP_related_code_update_tasks\000"
.LASF236:
	.ascii	"router_hub_list_MUTEX\000"
.LASF81:
	.ascii	"TaskName\000"
.LASF55:
	.ascii	"next\000"
.LASF91:
	.ascii	"modem_control_line_status\000"
.LASF113:
	.ascii	"exception_noted\000"
.LASF137:
	.ascii	"last_message_concluded_with_a_new_inbound_message\000"
.LASF173:
	.ascii	"restore_priority_of_ISP_related_code_update_tasks\000"
.LASF178:
	.ascii	"just_created_task\000"
.LASF133:
	.ascii	"waiting_to_start_the_connection_process_timer\000"
.LASF119:
	.ascii	"assert_current_task\000"
.LASF245:
	.ascii	"TPL_OUT_event_queue\000"
.LASF275:
	.ascii	"pvParameters\000"
.LASF9:
	.ascii	"portTickType\000"
.LASF126:
	.ascii	"init_packet_message_id\000"
.LASF174:
	.ascii	"__create_system_semaphores\000"
.LASF85:
	.ascii	"TASK_ENTRY_STRUCT\000"
.LASF129:
	.ascii	"mode\000"
.LASF72:
	.ascii	"rcvd_bytes\000"
.LASF134:
	.ascii	"process_timer\000"
.LASF45:
	.ascii	"transfer_from_this_port_to_USB\000"
.LASF182:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF219:
	.ascii	"weather_control_recursive_MUTEX\000"
.LASF194:
	.ascii	"Flash_0_MUTEX\000"
.LASF175:
	.ascii	"__create_system_queues\000"
.LASF24:
	.ascii	"i_cts\000"
.LASF151:
	.ascii	"waiting_for_lights_report_data_response\000"
.LASF212:
	.ascii	"station_history_completed_records_recursive_MUTEX\000"
.LASF232:
	.ascii	"budget_report_completed_records_recursive_MUTEX\000"
.LASF80:
	.ascii	"pTaskFunc\000"
.LASF22:
	.ascii	"DATA_HANDLE\000"
.LASF90:
	.ascii	"SerportTaskState\000"
.LASF99:
	.ascii	"first_to_display\000"
.LASF256:
	.ascii	"restart_info\000"
.LASF270:
	.ascii	"pwrfail_task_last_x_stamp\000"
.LASF121:
	.ascii	"RESTART_INFORMATION_STRUCT\000"
.LASF48:
	.ascii	"str_to_find\000"
.LASF243:
	.ascii	"Display_Command_Queue\000"
.LASF96:
	.ascii	"float\000"
.LASF230:
	.ascii	"moisture_sensor_recorder_recursive_MUTEX\000"
.LASF105:
	.ascii	"verify_string_pre\000"
.LASF57:
	.ascii	"hunt_for_data\000"
.LASF23:
	.ascii	"DATE_TIME\000"
.LASF193:
	.ascii	"alerts_pile_recursive_MUTEX\000"
.LASF261:
	.ascii	"wdt_task_FLOP_registers\000"
.LASF7:
	.ascii	"long long unsigned int\000"
.LASF239:
	.ascii	"Key_Scanner_Queue\000"
.LASF167:
	.ascii	"waiting_for_hub_is_busy_msg_response\000"
.LASF37:
	.ascii	"datastart\000"
.LASF112:
	.ascii	"we_were_reading_spi_flash_data\000"
.LASF217:
	.ascii	"list_system_recursive_MUTEX\000"
.LASF263:
	.ascii	"task_table_FLOP_registers\000"
.LASF92:
	.ascii	"UartRingBuffer_s\000"
.LASF258:
	.ascii	"debugio_MUTEX\000"
.LASF205:
	.ascii	"list_foal_irri_recursive_MUTEX\000"
.LASF237:
	.ascii	"router_hub_list_queue\000"
.LASF250:
	.ascii	"RADIO_TEST_task_queue\000"
.LASF192:
	.ascii	"startup_rtc_task_sync_semaphore\000"
.LASF238:
	.ascii	"Contrast_and_Volume_Queue\000"
.LASF115:
	.ascii	"exception_current_task\000"
.LASF13:
	.ascii	"xTimerHandle\000"
.LASF227:
	.ascii	"lights_preserves_recursive_MUTEX\000"
.LASF253:
	.ascii	"SerDrvrVars_s\000"
.LASF240:
	.ascii	"Key_To_Process_Queue\000"
.LASF74:
	.ascii	"code_receipt_bytes\000"
.LASF268:
	.ascii	"startup_task_handle\000"
.LASF10:
	.ascii	"xTaskHandle\000"
.LASF222:
	.ascii	"tpmicro_data_recursive_MUTEX\000"
.LASF215:
	.ascii	"system_preserves_recursive_MUTEX\000"
.LASF122:
	.ascii	"double\000"
.LASF265:
	.ascii	"port_tp_ring_buffer_analysis_task_handle\000"
.LASF114:
	.ascii	"exception_td\000"
.LASF266:
	.ascii	"port_tp_serial_driver_task_handle\000"
.LASF42:
	.ascii	"transfer_from_this_port_to_A\000"
.LASF43:
	.ascii	"transfer_from_this_port_to_B\000"
.LASF154:
	.ascii	"waiting_for_rain_indication_response\000"
.LASF100:
	.ascii	"first_to_send\000"
.LASF190:
	.ascii	"flash_storage_1_task_handle\000"
.LASF204:
	.ascii	"irri_irri_recursive_MUTEX\000"
.LASF131:
	.ascii	"now_xmitting\000"
.LASF160:
	.ascii	"a_pdata_message_is_on_the_list\000"
.LASF185:
	.ascii	"epson_rtc_task_handle\000"
.LASF120:
	.ascii	"assert_description_string\000"
.LASF198:
	.ascii	"list_tpl_in_messages_MUTEX\000"
.LASF221:
	.ascii	"speaker_hardware_MUTEX\000"
.LASF201:
	.ascii	"list_packets_waiting_for_token_MUTEX\000"
.LASF199:
	.ascii	"list_tpl_out_messages_MUTEX\000"
.LASF20:
	.ascii	"dptr\000"
.LASF26:
	.ascii	"i_ri\000"
.LASF14:
	.ascii	"char\000"
.LASF68:
	.ascii	"errors_parity\000"
.LASF148:
	.ascii	"waiting_for_poc_report_data_response\000"
.LASF27:
	.ascii	"i_cd\000"
.LASF136:
	.ascii	"last_message_concluded_with_a_response_timeout\000"
.LASF231:
	.ascii	"walk_thru_recursive_MUTEX\000"
.LASF63:
	.ascii	"dh_tail_caught_index\000"
.LASF269:
	.ascii	"rtc_task_last_x_stamp\000"
.LASF149:
	.ascii	"waiting_for_system_report_data_response\000"
.LASF252:
	.ascii	"FTIMES_task_queue\000"
.LASF38:
	.ascii	"packetlength\000"
.LASF188:
	.ascii	"timer_task_handle\000"
.LASF143:
	.ascii	"current_msg_frcs_ptr\000"
.LASF6:
	.ascii	"long long int\000"
.LASF106:
	.ascii	"main_app_code_revision_string\000"
.LASF229:
	.ascii	"moisture_sensor_items_recursive_MUTEX\000"
.LASF139:
	.ascii	"a_registration_message_is_on_the_list\000"
.LASF255:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF29:
	.ascii	"o_dtr\000"
.LASF274:
	.ascii	"system_startup_task\000"
.LASF31:
	.ascii	"UART_CTL_LINE_STATE_s\000"
.LASF118:
	.ascii	"assert_td\000"
.LASF146:
	.ascii	"waiting_for_station_history_response\000"
.LASF25:
	.ascii	"not_used_i_dsr\000"
.LASF60:
	.ascii	"hunt_for_specified_termination\000"
.LASF254:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF19:
	.ascii	"BOOL_32\000"
.LASF79:
	.ascii	"execution_limit_ms\000"
.LASF224:
	.ascii	"list_lights_recursive_MUTEX\000"
.LASF50:
	.ascii	"depth_into_the_buffer_to_look\000"
.LASF248:
	.ascii	"CONTROLLER_INITIATED_task_queue\000"
.LASF150:
	.ascii	"waiting_for_budget_report_data_response\000"
.LASF117:
	.ascii	"assert_noted\000"
.LASF197:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF87:
	.ascii	"cts_main_timer\000"
.LASF15:
	.ascii	"UNS_8\000"
.LASF158:
	.ascii	"mobile_seconds_since_last_command\000"
.LASF98:
	.ascii	"next_available\000"
.LASF170:
	.ascii	"hub_packet_activity_timer\000"
.LASF159:
	.ascii	"waiting_for_firmware_version_check_response\000"
.LASF109:
	.ascii	"shutdown_td\000"
.LASF128:
	.ascii	"CONTROLLER_INITIATED_MESSAGE_TRANSMITTING\000"
.LASF41:
	.ascii	"transfer_from_this_port_to_TP\000"
.LASF16:
	.ascii	"UNS_16\000"
.LASF247:
	.ascii	"CODE_DISTRIBUTION_task_queue\000"
.LASF161:
	.ascii	"waiting_for_pdata_response\000"
.LASF147:
	.ascii	"waiting_for_station_report_data_response\000"
.LASF164:
	.ascii	"waiting_for_asked_commserver_if_there_is_pdata_for_"
	.ascii	"us_response\000"
.LASF249:
	.ascii	"BYPASS_event_queue\000"
.LASF76:
	.ascii	"UART_STATS_STRUCT\000"
.LASF271:
	.ascii	"task_last_execution_stamp\000"
.LASF145:
	.ascii	"waiting_for_check_for_updates_response\000"
.LASF101:
	.ascii	"pending_first_to_send\000"
.LASF264:
	.ascii	"comm_mngr_task_handle\000"
.LASF138:
	.ascii	"waiting_for_registration_response\000"
.LASF62:
	.ascii	"ph_tail_caught_index\000"
.LASF61:
	.ascii	"task_to_signal_when_string_found\000"
.LASF111:
	.ascii	"we_were_writing_spi_flash_data\000"
.LASF196:
	.ascii	"rcvd_data_binary_semaphore\000"
.LASF1:
	.ascii	"short unsigned int\000"
.LASF235:
	.ascii	"chain_sync_control_structure_recursive_MUTEX\000"
.LASF228:
	.ascii	"lights_report_completed_records_recursive_MUTEX\000"
.LASF168:
	.ascii	"msgs_to_send_queue\000"
.LASF225:
	.ascii	"list_foal_lights_recursive_MUTEX\000"
.LASF155:
	.ascii	"send_rain_indication_timer\000"
.LASF214:
	.ascii	"station_preserves_recursive_MUTEX\000"
.LASF208:
	.ascii	"irri_comm_recursive_MUTEX\000"
.LASF267:
	.ascii	"startup_task_FLOP_registers\000"
.LASF191:
	.ascii	"created_task_handles\000"
.LASF89:
	.ascii	"cts_main_timer_state\000"
.LASF156:
	.ascii	"waiting_for_mobile_status_response\000"
.LASF77:
	.ascii	"bCreateTask\000"
.LASF56:
	.ascii	"hunt_for_packets\000"
.LASF51:
	.ascii	"find_initial_CRLF\000"
.LASF233:
	.ascii	"ci_message_list_MUTEX\000"
.LASF3:
	.ascii	"short int\000"
.LASF83:
	.ascii	"parameter\000"
.LASF69:
	.ascii	"errors_frame\000"
.LASF64:
	.ascii	"sh_tail_caught_index\000"
.LASF4:
	.ascii	"long int\000"
.LASF33:
	.ascii	"ringhead1\000"
.LASF34:
	.ascii	"ringhead2\000"
.LASF35:
	.ascii	"ringhead3\000"
.LASF36:
	.ascii	"ringhead4\000"
.LASF17:
	.ascii	"UNS_32\000"
.LASF251:
	.ascii	"Background_Calculation_Queue\000"
.LASF110:
	.ascii	"shutdown_reason\000"
.LASF84:
	.ascii	"priority\000"
.LASF12:
	.ascii	"xSemaphoreHandle\000"
.LASF75:
	.ascii	"mobile_status_updates_bytes\000"
.LASF195:
	.ascii	"Flash_1_MUTEX\000"
.LASF226:
	.ascii	"irri_lights_recursive_MUTEX\000"
.LASF259:
	.ascii	"epson_RTC_task_FLOP_registers\000"
.LASF176:
	.ascii	"code_updated\000"
.LASF220:
	.ascii	"weather_tables_recursive_MUTEX\000"
.LASF73:
	.ascii	"xmit_bytes\000"
.LASF184:
	.ascii	"Task_Table\000"
.LASF102:
	.ascii	"pending_first_to_send_in_use\000"
.LASF213:
	.ascii	"weather_preserves_recursive_MUTEX\000"
.LASF272:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF30:
	.ascii	"o_reset\000"
.LASF71:
	.ascii	"errors_fifo\000"
.LASF18:
	.ascii	"unsigned int\000"
.LASF47:
	.ascii	"string_index\000"
.LASF183:
	.ascii	"display_model_is\000"
.LASF163:
	.ascii	"waiting_for_firmware_version_check_before_asking_fo"
	.ascii	"r_pdata_response\000"
.LASF273:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/app_"
	.ascii	"startup.c\000"
.LASF157:
	.ascii	"send_mobile_status_timer\000"
.LASF218:
	.ascii	"list_poc_recursive_MUTEX\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF59:
	.ascii	"hunt_for_crlf_delimited_string\000"
.LASF180:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF162:
	.ascii	"pdata_timer\000"
.LASF95:
	.ascii	"SERPORT_DRVR_TASK_VARS_s\000"
.LASF46:
	.ascii	"DATA_HUNT_S\000"
.LASF132:
	.ascii	"response_timer\000"
.LASF58:
	.ascii	"hunt_for_specified_string\000"
.LASF40:
	.ascii	"data_index\000"
.LASF244:
	.ascii	"TPL_IN_event_queue\000"
.LASF209:
	.ascii	"system_report_completed_records_recursive_MUTEX\000"
.LASF11:
	.ascii	"xQueueHandle\000"
.LASF108:
	.ascii	"SHUTDOWN_impending\000"
.LASF5:
	.ascii	"unsigned char\000"
.LASF21:
	.ascii	"dlen\000"
.LASF78:
	.ascii	"include_in_wdt\000"
.LASF152:
	.ascii	"waiting_for_weather_data_receipt_response\000"
.LASF246:
	.ascii	"COMM_MNGR_task_queue\000"
.LASF52:
	.ascii	"STRING_HUNT_S\000"
.LASF216:
	.ascii	"poc_preserves_recursive_MUTEX\000"
.LASF125:
	.ascii	"frcs_ptr\000"
.LASF107:
	.ascii	"controller_index\000"
.LASF39:
	.ascii	"PACKET_HUNT_S\000"
.LASF200:
	.ascii	"list_incoming_messages_recursive_MUTEX\000"
.LASF130:
	.ascii	"state\000"
.LASF86:
	.ascii	"SerportDrvrEventQHandle\000"
.LASF103:
	.ascii	"when_to_send_timer\000"
.LASF70:
	.ascii	"errors_break\000"
.LASF49:
	.ascii	"chars_to_match\000"
.LASF93:
	.ascii	"stats\000"
.LASF166:
	.ascii	"waiting_for_the_no_more_messages_msg_response\000"
.LASF127:
	.ascii	"data_packet_message_id\000"
.LASF94:
	.ascii	"flow_control_timer\000"
.LASF44:
	.ascii	"transfer_from_this_port_to_RRE\000"
.LASF241:
	.ascii	"FLASH_STORAGE_task_queue_0\000"
.LASF242:
	.ascii	"FLASH_STORAGE_task_queue_1\000"
.LASF260:
	.ascii	"powerfail_task_FLOP_registers\000"
.LASF54:
	.ascii	"ring\000"
.LASF124:
	.ascii	"activity_flag_ptr\000"
.LASF2:
	.ascii	"signed char\000"
.LASF211:
	.ascii	"station_report_data_completed_records_recursive_MUT"
	.ascii	"EX\000"
.LASF53:
	.ascii	"TERMINATION_HUNT_S\000"
.LASF206:
	.ascii	"comm_mngr_recursive_MUTEX\000"
.LASF179:
	.ascii	"GuiFont_LanguageActive\000"
.LASF82:
	.ascii	"stack_depth\000"
.LASF210:
	.ascii	"poc_report_completed_records_recursive_MUTEX\000"
.LASF8:
	.ascii	"pdTASK_CODE\000"
.LASF144:
	.ascii	"waiting_for_moisture_sensor_recording_response\000"
.LASF142:
	.ascii	"waiting_for_flow_recording_response\000"
.LASF257:
	.ascii	"cics\000"
.LASF207:
	.ascii	"chain_members_recursive_MUTEX\000"
.LASF189:
	.ascii	"flash_storage_0_task_handle\000"
.LASF171:
	.ascii	"CONTROLLER_INITIATED_CONTROL_STRUCT\000"
.LASF141:
	.ascii	"waiting_for_alerts_response\000"
.LASF223:
	.ascii	"pending_changes_recursive_MUTEX\000"
.LASF262:
	.ascii	"timer_task_FLOP_registers\000"
.LASF169:
	.ascii	"queued_msgs_polling_timer\000"
.LASF186:
	.ascii	"powerfail_task_handle\000"
.LASF181:
	.ascii	"GuiFont_DecimalChar\000"
.LASF276:
	.ascii	"vPortSaveVFPRegisters\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
