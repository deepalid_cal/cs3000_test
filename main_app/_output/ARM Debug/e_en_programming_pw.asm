	.file	"e_en_programming_pw.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_PW_XE_PROGRAMMING_port,"aw",%nobits
	.align	2
	.type	g_PW_XE_PROGRAMMING_port, %object
	.size	g_PW_XE_PROGRAMMING_port, 4
g_PW_XE_PROGRAMMING_port:
	.space	4
	.section	.bss.g_PW_XE_PROGRAMMING_previous_cursor_pos,"aw",%nobits
	.align	2
	.type	g_PW_XE_PROGRAMMING_previous_cursor_pos, %object
	.size	g_PW_XE_PROGRAMMING_previous_cursor_pos, 4
g_PW_XE_PROGRAMMING_previous_cursor_pos:
	.space	4
	.section	.bss.g_PW_XE_PROGRAMMING_PW_querying_device,"aw",%nobits
	.align	2
	.type	g_PW_XE_PROGRAMMING_PW_querying_device, %object
	.size	g_PW_XE_PROGRAMMING_PW_querying_device, 4
g_PW_XE_PROGRAMMING_PW_querying_device:
	.space	4
	.section	.bss.g_PW_XE_PROGRAMMING_read_after_write_pending,"aw",%nobits
	.align	2
	.type	g_PW_XE_PROGRAMMING_read_after_write_pending, %object
	.size	g_PW_XE_PROGRAMMING_read_after_write_pending, 4
g_PW_XE_PROGRAMMING_read_after_write_pending:
	.space	4
	.section	.bss.g_PW_XE_PROGRAMMING_editing_wifi_settings,"aw",%nobits
	.align	2
	.type	g_PW_XE_PROGRAMMING_editing_wifi_settings, %object
	.size	g_PW_XE_PROGRAMMING_editing_wifi_settings, 4
g_PW_XE_PROGRAMMING_editing_wifi_settings:
	.space	4
	.section	.text.FDTO_PW_XE_PROGRAMMING_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_PW_XE_PROGRAMMING_process_device_exchange_key, %function
FDTO_PW_XE_PROGRAMMING_process_device_exchange_key:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_en_programming_pw.c"
	.loc 1 69 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 70 0
	bl	e_SHARED_get_info_text_from_programming_struct
	.loc 1 72 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #36864
	cmp	r3, #6
	bhi	.L1
	mov	r2, #1
	mov	r3, r2, asl r3
	and	r2, r3, #73
	cmp	r2, #0
	bne	.L3
	and	r2, r3, #36
	cmp	r2, #0
	bne	.L5
	and	r3, r3, #18
	cmp	r3, #0
	beq	.L1
.L4:
	.loc 1 76 0
	ldr	r3, .L8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L6
	.loc 1 82 0
	ldr	r0, .L8+4
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L8+8
	str	r2, [r3, #0]
	b	.L7
.L6:
	.loc 1 89 0
	bl	PW_XE_PROGRAMMING_copy_programming_into_GuiVars
	.loc 1 92 0
	ldr	r0, .L8+12
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L8+8
	str	r2, [r3, #0]
.L7:
	.loc 1 96 0
	bl	DIALOG_close_ok_dialog
	.loc 1 98 0
	ldr	r3, .L8+16
	ldr	r3, [r3, #0]
	mov	r0, #0
	mov	r1, r3
	bl	FDTO_PW_XE_PROGRAMMING_draw_screen
	.loc 1 99 0
	b	.L1
.L5:
	.loc 1 106 0
	bl	PW_XE_PROGRAMMING_copy_programming_into_GuiVars
.L3:
	.loc 1 114 0
	ldr	r0, [fp, #-8]
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L8+8
	str	r2, [r3, #0]
	.loc 1 117 0
	bl	DEVICE_EXCHANGE_draw_dialog
	.loc 1 118 0
	mov	r0, r0	@ nop
.L1:
	.loc 1 120 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L9:
	.align	2
.L8:
	.word	g_PW_XE_PROGRAMMING_read_after_write_pending
	.word	36867
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36865
	.word	g_PW_XE_PROGRAMMING_port
.LFE0:
	.size	FDTO_PW_XE_PROGRAMMING_process_device_exchange_key, .-FDTO_PW_XE_PROGRAMMING_process_device_exchange_key
	.section	.text.FDTO_PW_XE_PROGRAMMING_redraw_screen,"ax",%progbits
	.align	2
	.type	FDTO_PW_XE_PROGRAMMING_redraw_screen, %function
FDTO_PW_XE_PROGRAMMING_redraw_screen:
.LFB1:
	.loc 1 124 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 132 0
	ldr	r3, .L11
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	bl	FDTO_PW_XE_PROGRAMMING_draw_screen
	.loc 1 133 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L12:
	.align	2
.L11:
	.word	g_PW_XE_PROGRAMMING_port
.LFE1:
	.size	FDTO_PW_XE_PROGRAMMING_redraw_screen, .-FDTO_PW_XE_PROGRAMMING_redraw_screen
	.section	.text.FDTO_PW_XE_PROGRAMMING_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_PW_XE_PROGRAMMING_draw_screen
	.type	FDTO_PW_XE_PROGRAMMING_draw_screen, %function
FDTO_PW_XE_PROGRAMMING_draw_screen:
.LFB2:
	.loc 1 137 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #12
.LCFI8:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 140 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L14
	.loc 1 142 0
	ldr	r3, .L17
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 144 0
	ldr	r3, .L17+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 146 0
	ldr	r3, .L17+8
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 150 0
	ldr	r3, .L17+12
	ldr	r2, .L17+16
	str	r2, [r3, #0]
	.loc 1 157 0
	ldr	r0, .L17+20
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L17+12
	str	r2, [r3, #0]
	.loc 1 161 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L15
.L14:
	.loc 1 165 0
	ldr	r3, .L17+24
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L15:
	.loc 1 168 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #17
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 169 0
	bl	GuiLib_Refresh
	.loc 1 173 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L13
	.loc 1 177 0
	bl	DEVICE_EXCHANGE_draw_dialog
	.loc 1 181 0
	ldr	r3, .L17
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #82
	ldr	r2, .L17+28
	bl	e_SHARED_start_device_communication
.L13:
	.loc 1 183 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L18:
	.align	2
.L17:
	.word	g_PW_XE_PROGRAMMING_port
	.word	g_PW_XE_PROGRAMMING_read_after_write_pending
	.word	g_PW_XE_PROGRAMMING_editing_wifi_settings
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36865
	.word	36867
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_PW_XE_PROGRAMMING_PW_querying_device
.LFE2:
	.size	FDTO_PW_XE_PROGRAMMING_draw_screen, .-FDTO_PW_XE_PROGRAMMING_draw_screen
	.section	.text.PW_XE_PROGRAMMING_process_screen,"ax",%progbits
	.align	2
	.global	PW_XE_PROGRAMMING_process_screen
	.type	PW_XE_PROGRAMMING_process_screen, %function
PW_XE_PROGRAMMING_process_screen:
.LFB3:
	.loc 1 187 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #52
.LCFI11:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	.loc 1 190 0
	ldr	r3, .L100
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ldr	r2, .L100+4
	cmp	r3, r2
	beq	.L22
	cmp	r3, #740
	beq	.L23
	ldr	r2, .L100+8
	cmp	r3, r2
	bne	.L98
.L21:
	.loc 1 193 0
	ldr	r3, [fp, #-48]
	cmp	r3, #67
	beq	.L24
	.loc 1 193 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	bne	.L25
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #49
	bne	.L25
.L24:
	.loc 1 197 0 is_stmt 1
	ldr	r0, .L100+16
	ldr	r1, .L100+20
	mov	r2, #17
	bl	strlcpy
.L25:
	.loc 1 200 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	ldr	r2, .L100+24
	bl	KEYBOARD_process_key
	.loc 1 201 0
	b	.L19
.L22:
	.loc 1 204 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L100+28
	bl	COMBO_BOX_key_press
	.loc 1 205 0
	b	.L19
.L23:
	.loc 1 208 0
	ldr	r3, [fp, #-48]
	mov	r0, r3
	ldr	r1, .L100+32
	bl	COMBO_BOX_key_press
	.loc 1 209 0
	b	.L19
.L98:
	.loc 1 212 0
	ldr	r3, [fp, #-48]
	cmp	r3, #4
	beq	.L32
	cmp	r3, #4
	bhi	.L36
	cmp	r3, #1
	beq	.L29
	cmp	r3, #1
	bcc	.L28
	cmp	r3, #2
	beq	.L30
	cmp	r3, #3
	beq	.L31
	b	.L27
.L36:
	cmp	r3, #84
	beq	.L34
	cmp	r3, #84
	bhi	.L37
	cmp	r3, #67
	beq	.L33
	cmp	r3, #80
	beq	.L34
	b	.L27
.L37:
	sub	r3, r3, #36864
	cmp	r3, #6
	bhi	.L27
	.loc 1 224 0
	ldr	r2, [fp, #-48]
	ldr	r3, .L100+36
	cmp	r2, r3
	beq	.L38
	.loc 1 224 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-48]
	ldr	r3, .L100+40
	cmp	r2, r3
	beq	.L38
	.loc 1 226 0 is_stmt 1
	ldr	r3, .L100+44
	mov	r2, #0
	str	r2, [r3, #0]
.L38:
	.loc 1 231 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 232 0
	ldr	r3, .L100+48
	str	r3, [fp, #-20]
	.loc 1 233 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-16]
	.loc 1 234 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 236 0
	ldr	r2, [fp, #-48]
	ldr	r3, .L100+52
	cmp	r2, r3
	bne	.L39
	.loc 1 237 0 discriminator 1
	ldr	r3, .L100+56
	ldr	r3, [r3, #0]
	.loc 1 236 0 discriminator 1
	cmp	r3, #1
	bne	.L39
	.loc 1 241 0
	ldr	r3, .L100+56
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 242 0
	ldr	r3, .L100+60
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #82
	ldr	r2, .L100+44
	bl	e_SHARED_start_device_communication
	.loc 1 250 0
	b	.L99
.L39:
	.loc 1 244 0
	ldr	r2, [fp, #-48]
	ldr	r3, .L100+64
	cmp	r2, r3
	bne	.L99
	.loc 1 248 0
	ldr	r3, .L100+56
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 250 0
	b	.L99
.L30:
	.loc 1 255 0
	ldr	r0, .L100+36
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L100+68
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L41
	.loc 1 256 0 discriminator 1
	ldr	r0, .L100+40
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L100+68
	ldr	r3, [r3, #0]
	.loc 1 255 0 discriminator 1
	cmp	r2, r3
	beq	.L41
	.loc 1 258 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #12
	ldrls	pc, [pc, r3, asl #2]
	b	.L42
.L48:
	.word	.L43
	.word	.L44
	.word	.L42
	.word	.L42
	.word	.L42
	.word	.L42
	.word	.L45
	.word	.L42
	.word	.L42
	.word	.L42
	.word	.L42
	.word	.L46
	.word	.L47
.L43:
	.loc 1 261 0
	bl	good_key_beep
	.loc 1 262 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 263 0
	ldr	r3, .L100+72
	str	r3, [fp, #-20]
	.loc 1 264 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 265 0
	b	.L49
.L44:
	.loc 1 268 0
	bl	good_key_beep
	.loc 1 273 0
	ldr	r0, .L100+20
	ldr	r1, .L100+16
	mov	r2, #17
	bl	strlcpy
	.loc 1 276 0
	ldr	r0, .L100+16
	mov	r1, #0
	mov	r2, #17
	bl	memset
	.loc 1 278 0
	mov	r0, #102
	mov	r1, #68
	mov	r2, #17
	mov	r3, #0
	bl	KEYBOARD_draw_keyboard
	.loc 1 279 0
	b	.L49
.L45:
	.loc 1 282 0
	bl	good_key_beep
	.loc 1 283 0
	mov	r3, #1
	str	r3, [fp, #-40]
	.loc 1 284 0
	ldr	r3, .L100+76
	str	r3, [fp, #-20]
	.loc 1 285 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 286 0
	b	.L49
.L47:
	.loc 1 290 0
	ldr	r3, .L100+44
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L50
	.loc 1 292 0
	bl	good_key_beep
	.loc 1 296 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L100+80
	str	r2, [r3, #0]
	.loc 1 299 0
	bl	PW_XE_PROGRAMMING_extract_and_store_GuiVars
	.loc 1 302 0
	ldr	r3, .L100+60
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #87
	ldr	r2, .L100+44
	bl	e_SHARED_start_device_communication
	.loc 1 306 0
	ldr	r3, .L100+56
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 312 0
	b	.L49
.L50:
	.loc 1 310 0
	bl	bad_key_beep
	.loc 1 312 0
	b	.L49
.L46:
	.loc 1 316 0
	ldr	r3, .L100+44
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L52
	.loc 1 318 0
	bl	good_key_beep
	.loc 1 322 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L100+80
	str	r2, [r3, #0]
	.loc 1 324 0
	ldr	r3, .L100+60
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #82
	ldr	r2, .L100+44
	bl	e_SHARED_start_device_communication
	.loc 1 330 0
	b	.L49
.L52:
	.loc 1 328 0
	bl	bad_key_beep
	.loc 1 330 0
	b	.L49
.L42:
	.loc 1 333 0
	bl	bad_key_beep
	.loc 1 335 0
	b	.L54
.L49:
	b	.L54
.L41:
	.loc 1 338 0
	bl	bad_key_beep
	.loc 1 340 0
	b	.L19
.L54:
	b	.L19
.L34:
	.loc 1 344 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L55
.L66:
	.word	.L56
	.word	.L55
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
.L56:
	.loc 1 347 0
	ldr	r0, .L100+32
	bl	process_bool
	.loc 1 351 0
	mov	r0, #0
	bl	Redraw_Screen
	.loc 1 352 0
	b	.L67
.L57:
	.loc 1 355 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L100+84
	mov	r2, #0
	mov	r3, #255
	bl	process_uns32
	.loc 1 356 0
	b	.L67
.L58:
	.loc 1 359 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L100+88
	mov	r2, #0
	mov	r3, #255
	bl	process_uns32
	.loc 1 360 0
	b	.L67
.L59:
	.loc 1 363 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L100+92
	mov	r2, #0
	mov	r3, #255
	bl	process_uns32
	.loc 1 364 0
	b	.L67
.L60:
	.loc 1 367 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L100+96
	mov	r2, #0
	mov	r3, #255
	bl	process_uns32
	.loc 1 368 0
	b	.L67
.L61:
	.loc 1 371 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L100+28
	mov	r2, #0
	mov	r3, #24
	bl	process_uns32
	.loc 1 372 0
	b	.L67
.L62:
	.loc 1 375 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L100+100
	mov	r2, #0
	mov	r3, #255
	bl	process_uns32
	.loc 1 376 0
	b	.L67
.L63:
	.loc 1 379 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L100+104
	mov	r2, #0
	mov	r3, #255
	bl	process_uns32
	.loc 1 380 0
	b	.L67
.L64:
	.loc 1 383 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L100+108
	mov	r2, #0
	mov	r3, #255
	bl	process_uns32
	.loc 1 384 0
	b	.L67
.L65:
	.loc 1 387 0
	ldr	r3, [fp, #-48]
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L100+112
	mov	r2, #0
	mov	r3, #255
	bl	process_uns32
	.loc 1 388 0
	b	.L67
.L55:
	.loc 1 391 0
	bl	bad_key_beep
.L67:
	.loc 1 394 0
	bl	Refresh_Screen
	.loc 1 395 0
	b	.L19
.L32:
	.loc 1 398 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #2
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L68
.L74:
	.word	.L69
	.word	.L69
	.word	.L69
	.word	.L69
	.word	.L70
	.word	.L71
	.word	.L71
	.word	.L71
	.word	.L71
	.word	.L72
	.word	.L73
.L69:
	.loc 1 404 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L100+80
	str	r2, [r3, #0]
	.loc 1 406 0
	mov	r0, #0
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 407 0
	b	.L75
.L70:
	.loc 1 412 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bls	.L76
	.loc 1 412 0 is_stmt 0 discriminator 1
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #5
	bls	.L77
.L76:
	.loc 1 414 0 is_stmt 1
	ldr	r3, .L100+80
	mov	r2, #2
	str	r2, [r3, #0]
.L77:
	.loc 1 418 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 419 0
	b	.L75
.L71:
	.loc 1 425 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L100+80
	str	r2, [r3, #0]
	.loc 1 427 0
	mov	r0, #6
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 428 0
	b	.L75
.L73:
	.loc 1 433 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bls	.L78
	.loc 1 433 0 is_stmt 0 discriminator 1
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #10
	bls	.L79
.L78:
	.loc 1 435 0 is_stmt 1
	ldr	r3, .L100+80
	mov	r2, #7
	str	r2, [r3, #0]
.L79:
	.loc 1 438 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 439 0
	b	.L75
.L72:
	.loc 1 442 0
	ldr	r3, .L100+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L80
	.loc 1 444 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 458 0
	b	.L75
.L80:
	.loc 1 450 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bls	.L82
	.loc 1 450 0 is_stmt 0 discriminator 1
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #10
	bls	.L83
.L82:
	.loc 1 452 0 is_stmt 1
	ldr	r3, .L100+80
	mov	r2, #7
	str	r2, [r3, #0]
.L83:
	.loc 1 456 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 458 0
	b	.L75
.L68:
	.loc 1 461 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 463 0
	b	.L19
.L75:
	b	.L19
.L29:
	.loc 1 466 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 467 0
	b	.L19
.L28:
	.loc 1 470 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L84
.L90:
	.word	.L85
	.word	.L84
	.word	.L86
	.word	.L86
	.word	.L86
	.word	.L86
	.word	.L87
	.word	.L88
	.word	.L88
	.word	.L88
	.word	.L88
	.word	.L89
.L85:
	.loc 1 473 0
	ldr	r3, .L100+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L91
	.loc 1 475 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 489 0
	b	.L95
.L91:
	.loc 1 481 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bls	.L93
	.loc 1 481 0 is_stmt 0 discriminator 1
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #5
	bls	.L94
.L93:
	.loc 1 483 0 is_stmt 1
	ldr	r3, .L100+80
	mov	r2, #2
	str	r2, [r3, #0]
.L94:
	.loc 1 487 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 489 0
	b	.L95
.L86:
	.loc 1 495 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L100+80
	str	r2, [r3, #0]
	.loc 1 497 0
	mov	r0, #6
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 498 0
	b	.L95
.L87:
	.loc 1 503 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bls	.L96
	.loc 1 503 0 is_stmt 0 discriminator 1
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	cmp	r3, #10
	bls	.L97
.L96:
	.loc 1 505 0 is_stmt 1
	ldr	r3, .L100+80
	mov	r2, #7
	str	r2, [r3, #0]
.L97:
	.loc 1 509 0
	ldr	r3, .L100+80
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 510 0
	b	.L95
.L88:
	.loc 1 516 0
	ldr	r3, .L100+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L100+80
	str	r2, [r3, #0]
	.loc 1 518 0
	mov	r0, #11
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 519 0
	b	.L95
.L89:
	.loc 1 522 0
	bl	bad_key_beep
	.loc 1 523 0
	b	.L95
.L84:
	.loc 1 526 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 527 0
	mov	r0, r0	@ nop
.L95:
	.loc 1 529 0
	b	.L19
.L31:
	.loc 1 532 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 533 0
	b	.L19
.L33:
	.loc 1 536 0
	ldr	r3, .L100+116
	mov	r2, #11
	str	r2, [r3, #0]
	.loc 1 541 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 545 0
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	.loc 1 546 0
	b	.L19
.L27:
	.loc 1 549 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	b	.L19
.L99:
	.loc 1 250 0
	mov	r0, r0	@ nop
.L19:
	.loc 1 552 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L101:
	.align	2
.L100:
	.word	GuiLib_CurStructureNdx
	.word	726
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ENDHCPName
	.word	GuiVar_GroupName
	.word	FDTO_PW_XE_PROGRAMMING_redraw_screen
	.word	GuiVar_ENNetmask
	.word	GuiVar_ENObtainIPAutomatically
	.word	36867
	.word	36870
	.word	g_PW_XE_PROGRAMMING_PW_querying_device
	.word	FDTO_PW_XE_PROGRAMMING_process_device_exchange_key
	.word	36868
	.word	g_PW_XE_PROGRAMMING_read_after_write_pending
	.word	g_PW_XE_PROGRAMMING_port
	.word	36869
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	FDTO_e_SHARED_show_obtain_ip_automatically_dropdown
	.word	FDTO_e_SHARED_show_subnet_dropdown
	.word	g_PW_XE_PROGRAMMING_previous_cursor_pos
	.word	GuiVar_ENIPAddress_1
	.word	GuiVar_ENIPAddress_2
	.word	GuiVar_ENIPAddress_3
	.word	GuiVar_ENIPAddress_4
	.word	GuiVar_ENGateway_1
	.word	GuiVar_ENGateway_2
	.word	GuiVar_ENGateway_3
	.word	GuiVar_ENGateway_4
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	PW_XE_PROGRAMMING_process_screen, .-PW_XE_PROGRAMMING_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x503
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF61
	.byte	0x1
	.4byte	.LASF62
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x53
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x5
	.byte	0x4
	.4byte	0x92
	.uleb128 0x6
	.4byte	0x99
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x8
	.4byte	0x33
	.4byte	0xb0
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd5
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x7e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x80
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x82
	.4byte	0xb0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF16
	.uleb128 0xc
	.4byte	0x5a
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0xfc
	.uleb128 0x9
	.4byte	0x25
	.byte	0x40
	.byte	0
	.uleb128 0xa
	.byte	0x24
	.byte	0x4
	.byte	0x78
	.4byte	0x183
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x7b
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x4
	.byte	0x83
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x4
	.byte	0x86
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x4
	.byte	0x88
	.4byte	0x194
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.byte	0x8d
	.4byte	0x1a6
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x92
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.byte	0x96
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x4
	.byte	0x9a
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x4
	.byte	0x9c
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xd
	.byte	0x1
	.4byte	0x18f
	.uleb128 0xe
	.4byte	0x18f
	.byte	0
	.uleb128 0xc
	.4byte	0x48
	.uleb128 0x5
	.byte	0x4
	.4byte	0x183
	.uleb128 0xd
	.byte	0x1
	.4byte	0x1a6
	.uleb128 0xe
	.4byte	0xd5
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x19a
	.uleb128 0x3
	.4byte	.LASF26
	.byte	0x4
	.byte	0x9e
	.4byte	0xfc
	.uleb128 0xf
	.4byte	.LASF27
	.byte	0x1
	.byte	0x44
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1de
	.uleb128 0x10
	.4byte	.LASF29
	.byte	0x1
	.byte	0x44
	.4byte	0xe7
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x1
	.byte	0x7b
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x205
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x1
	.byte	0x7b
	.4byte	0x205
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xc
	.4byte	0x81
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.byte	0x88
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x24e
	.uleb128 0x10
	.4byte	.LASF30
	.byte	0x1
	.byte	0x88
	.4byte	0x205
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0x88
	.4byte	0xe7
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF35
	.byte	0x1
	.byte	0x8a
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.byte	0xba
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x284
	.uleb128 0x10
	.4byte	.LASF34
	.byte	0x1
	.byte	0xba
	.4byte	0x284
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x13
	.ascii	"lde\000"
	.byte	0x1
	.byte	0xbc
	.4byte	0x1ac
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xc
	.4byte	0xd5
	.uleb128 0x14
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x16c
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x2a7
	.uleb128 0x9
	.4byte	0x25
	.byte	0x10
	.byte	0
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x192
	.4byte	0x297
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x195
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x196
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x197
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x198
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x199
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x19a
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x19b
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x19c
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x19f
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x1a0
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x1fc
	.4byte	0xec
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF50
	.byte	0x6
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF51
	.byte	0x6
	.2byte	0x132
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF52
	.byte	0x7
	.byte	0x30
	.4byte	0x38a
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xc
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF53
	.byte	0x7
	.byte	0x34
	.4byte	0x3a0
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xc
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF54
	.byte	0x7
	.byte	0x36
	.4byte	0x3b6
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xc
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF55
	.byte	0x7
	.byte	0x38
	.4byte	0x3cc
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xc
	.4byte	0xa0
	.uleb128 0x12
	.4byte	.LASF56
	.byte	0x1
	.byte	0x36
	.4byte	0x5a
	.byte	0x5
	.byte	0x3
	.4byte	g_PW_XE_PROGRAMMING_port
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.byte	0x38
	.4byte	0x5a
	.byte	0x5
	.byte	0x3
	.4byte	g_PW_XE_PROGRAMMING_previous_cursor_pos
	.uleb128 0x12
	.4byte	.LASF58
	.byte	0x1
	.byte	0x3a
	.4byte	0x81
	.byte	0x5
	.byte	0x3
	.4byte	g_PW_XE_PROGRAMMING_PW_querying_device
	.uleb128 0x12
	.4byte	.LASF59
	.byte	0x1
	.byte	0x3c
	.4byte	0x81
	.byte	0x5
	.byte	0x3
	.4byte	g_PW_XE_PROGRAMMING_read_after_write_pending
	.uleb128 0x12
	.4byte	.LASF60
	.byte	0x1
	.byte	0x3e
	.4byte	0x81
	.byte	0x5
	.byte	0x3
	.4byte	g_PW_XE_PROGRAMMING_editing_wifi_settings
	.uleb128 0x14
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x16c
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF37
	.byte	0x5
	.2byte	0x192
	.4byte	0x297
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF38
	.byte	0x5
	.2byte	0x195
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF39
	.byte	0x5
	.2byte	0x196
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF40
	.byte	0x5
	.2byte	0x197
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF41
	.byte	0x5
	.2byte	0x198
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x199
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF43
	.byte	0x5
	.2byte	0x19a
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF44
	.byte	0x5
	.2byte	0x19b
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF45
	.byte	0x5
	.2byte	0x19c
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF46
	.byte	0x5
	.2byte	0x19f
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF47
	.byte	0x5
	.2byte	0x1a0
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF48
	.byte	0x5
	.2byte	0x1fc
	.4byte	0xec
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF49
	.byte	0x5
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF50
	.byte	0x6
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF51
	.byte	0x6
	.2byte	0x132
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF61:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF56:
	.ascii	"g_PW_XE_PROGRAMMING_port\000"
.LASF38:
	.ascii	"GuiVar_ENGateway_1\000"
.LASF39:
	.ascii	"GuiVar_ENGateway_2\000"
.LASF40:
	.ascii	"GuiVar_ENGateway_3\000"
.LASF41:
	.ascii	"GuiVar_ENGateway_4\000"
.LASF45:
	.ascii	"GuiVar_ENIPAddress_4\000"
.LASF5:
	.ascii	"short int\000"
.LASF17:
	.ascii	"_01_command\000"
.LASF47:
	.ascii	"GuiVar_ENObtainIPAutomatically\000"
.LASF37:
	.ascii	"GuiVar_ENDHCPName\000"
.LASF49:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF36:
	.ascii	"GuiVar_CommOptionDeviceExchangeResult\000"
.LASF60:
	.ascii	"g_PW_XE_PROGRAMMING_editing_wifi_settings\000"
.LASF13:
	.ascii	"keycode\000"
.LASF42:
	.ascii	"GuiVar_ENIPAddress_1\000"
.LASF43:
	.ascii	"GuiVar_ENIPAddress_2\000"
.LASF44:
	.ascii	"GuiVar_ENIPAddress_3\000"
.LASF18:
	.ascii	"_02_menu\000"
.LASF32:
	.ascii	"FDTO_PW_XE_PROGRAMMING_draw_screen\000"
.LASF59:
	.ascii	"g_PW_XE_PROGRAMMING_read_after_write_pending\000"
.LASF16:
	.ascii	"float\000"
.LASF10:
	.ascii	"long long int\000"
.LASF54:
	.ascii	"GuiFont_DecimalChar\000"
.LASF15:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF12:
	.ascii	"long int\000"
.LASF48:
	.ascii	"GuiVar_GroupName\000"
.LASF27:
	.ascii	"FDTO_PW_XE_PROGRAMMING_process_device_exchange_key\000"
.LASF31:
	.ascii	"pport\000"
.LASF57:
	.ascii	"g_PW_XE_PROGRAMMING_previous_cursor_pos\000"
.LASF23:
	.ascii	"_06_u32_argument1\000"
.LASF21:
	.ascii	"key_process_func_ptr\000"
.LASF25:
	.ascii	"_08_screen_to_draw\000"
.LASF30:
	.ascii	"pcomplete_redraw\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF3:
	.ascii	"signed char\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF53:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF19:
	.ascii	"_03_structure_to_draw\000"
.LASF50:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF1:
	.ascii	"char\000"
.LASF28:
	.ascii	"FDTO_PW_XE_PROGRAMMING_redraw_screen\000"
.LASF55:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF20:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF24:
	.ascii	"_07_u32_argument2\000"
.LASF46:
	.ascii	"GuiVar_ENNetmask\000"
.LASF58:
	.ascii	"g_PW_XE_PROGRAMMING_PW_querying_device\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF62:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_en_programming_pw.c\000"
.LASF22:
	.ascii	"_04_func_ptr\000"
.LASF33:
	.ascii	"PW_XE_PROGRAMMING_process_screen\000"
.LASF51:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF14:
	.ascii	"repeats\000"
.LASF52:
	.ascii	"GuiFont_LanguageActive\000"
.LASF35:
	.ascii	"lcursor_to_select\000"
.LASF29:
	.ascii	"pkeycode\000"
.LASF34:
	.ascii	"pkey_event\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF26:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
