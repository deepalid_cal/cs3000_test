	.file	"lights.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section .rodata
	.align	2
.LC0:
	.ascii	"Name\000"
	.align	2
.LC1:
	.ascii	"BoxIndex\000"
	.align	2
.LC2:
	.ascii	"OutputIndex\000"
	.align	2
.LC3:
	.ascii	"CardConnected\000"
	.align	2
.LC4:
	.ascii	"Day00_\000"
	.align	2
.LC5:
	.ascii	"Day01_\000"
	.align	2
.LC6:
	.ascii	"Day02_\000"
	.align	2
.LC7:
	.ascii	"Day03_\000"
	.align	2
.LC8:
	.ascii	"Day04_\000"
	.align	2
.LC9:
	.ascii	"Day05_\000"
	.align	2
.LC10:
	.ascii	"Day06_\000"
	.align	2
.LC11:
	.ascii	"Day07_\000"
	.align	2
.LC12:
	.ascii	"Day08_\000"
	.align	2
.LC13:
	.ascii	"Day09_\000"
	.align	2
.LC14:
	.ascii	"Day10_\000"
	.align	2
.LC15:
	.ascii	"Day11_\000"
	.align	2
.LC16:
	.ascii	"Day12_\000"
	.align	2
.LC17:
	.ascii	"Day13_\000"
	.section	.rodata.LIGHTS_database_field_names,"a",%progbits
	.align	2
	.type	LIGHTS_database_field_names, %object
	.size	LIGHTS_database_field_names, 72
LIGHTS_database_field_names:
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.section	.text.nm_LIGHTS_set_name,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_name
	.type	nm_LIGHTS_set_name, %function
nm_LIGHTS_set_name:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_lights.c"
	.loc 1 428 0
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #36
.LCFI2:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 429 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #316
	ldr	r2, [fp, #4]
	str	r2, [sp, #0]
	ldr	r2, [fp, #8]
	str	r2, [sp, #4]
	ldr	r2, [fp, #12]
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	mov	r3, #0
	str	r3, [sp, #16]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	bl	SHARED_set_name_32_bit_change_bits
	.loc 1 445 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE0:
	.size	nm_LIGHTS_set_name, .-nm_LIGHTS_set_name
	.section	.text.nm_LIGHTS_set_box_index,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_box_index
	.type	nm_LIGHTS_set_box_index, %function
nm_LIGHTS_set_box_index:
.LFB1:
	.loc 1 492 0
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #60
.LCFI5:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 493 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #72
	ldr	r2, [fp, #-8]
	add	r1, r2, #316
	.loc 1 507 0
	ldr	r2, .L3
	ldr	r2, [r2, #4]
	.loc 1 493 0
	mov	r0, #11
	str	r0, [sp, #0]
	mov	r0, #0
	str	r0, [sp, #4]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #8]
	ldr	r0, .L3+4
	str	r0, [sp, #12]
	ldr	r0, [fp, #-20]
	str	r0, [sp, #16]
	ldr	r0, [fp, #4]
	str	r0, [sp, #20]
	ldr	r0, [fp, #8]
	str	r0, [sp, #24]
	ldr	r0, [fp, #12]
	str	r0, [sp, #28]
	str	r1, [sp, #32]
	mov	r1, #1
	str	r1, [sp, #36]
	str	r2, [sp, #40]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, [fp, #-12]
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	.loc 1 522 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L4:
	.align	2
.L3:
	.word	LIGHTS_database_field_names
	.word	49296
.LFE1:
	.size	nm_LIGHTS_set_box_index, .-nm_LIGHTS_set_box_index
	.section	.text.nm_LIGHTS_set_output_index,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_output_index
	.type	nm_LIGHTS_set_output_index, %function
nm_LIGHTS_set_output_index:
.LFB2:
	.loc 1 571 0
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #60
.LCFI8:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 572 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #76
	ldr	r2, [fp, #-8]
	add	r1, r2, #316
	.loc 1 586 0
	ldr	r2, .L6
	ldr	r2, [r2, #8]
	.loc 1 572 0
	mov	r0, #3
	str	r0, [sp, #0]
	mov	r0, #0
	str	r0, [sp, #4]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #8]
	ldr	r0, .L6+4
	str	r0, [sp, #12]
	ldr	r0, [fp, #-20]
	str	r0, [sp, #16]
	ldr	r0, [fp, #4]
	str	r0, [sp, #20]
	ldr	r0, [fp, #8]
	str	r0, [sp, #24]
	ldr	r0, [fp, #12]
	str	r0, [sp, #28]
	str	r1, [sp, #32]
	mov	r1, #2
	str	r1, [sp, #36]
	str	r2, [sp, #40]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, [fp, #-12]
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	.loc 1 601 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L7:
	.align	2
.L6:
	.word	LIGHTS_database_field_names
	.word	49297
.LFE2:
	.size	nm_LIGHTS_set_output_index, .-nm_LIGHTS_set_output_index
	.section	.text.nm_LIGHTS_set_physically_available,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_physically_available
	.type	nm_LIGHTS_set_physically_available, %function
nm_LIGHTS_set_physically_available:
.LFB3:
	.loc 1 650 0
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #52
.LCFI11:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 651 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #80
	ldr	r2, [fp, #-8]
	add	r1, r2, #316
	.loc 1 663 0
	ldr	r2, .L9
	ldr	r2, [r2, #12]
	.loc 1 651 0
	ldr	r0, [fp, #-16]
	str	r0, [sp, #0]
	ldr	r0, .L9+4
	str	r0, [sp, #4]
	ldr	r0, [fp, #-20]
	str	r0, [sp, #8]
	ldr	r0, [fp, #4]
	str	r0, [sp, #12]
	ldr	r0, [fp, #8]
	str	r0, [sp, #16]
	ldr	r0, [fp, #12]
	str	r0, [sp, #20]
	str	r1, [sp, #24]
	mov	r1, #3
	str	r1, [sp, #28]
	str	r2, [sp, #32]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, [fp, #-12]
	mov	r3, #0
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	.loc 1 689 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #72]
	.loc 1 690 0
	ldr	r3, [fp, #-8]
	.loc 1 689 0
	ldr	r3, [r3, #76]
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	ldr	r3, [fp, #-12]
	bl	LIGHTS_PRESERVES_set_bit_field_bit
	.loc 1 695 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L10:
	.align	2
.L9:
	.word	LIGHTS_database_field_names
	.word	49298
.LFE3:
	.size	nm_LIGHTS_set_physically_available, .-nm_LIGHTS_set_physically_available
	.section .rodata
	.align	2
.LC18:
	.ascii	"%sStartTime1\000"
	.align	2
.LC19:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_lights.c\000"
	.section	.text.nm_LIGHTS_set_start_time_1,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_start_time_1
	.type	nm_LIGHTS_set_start_time_1, %function
nm_LIGHTS_set_start_time_1:
.LFB4:
	.loc 1 747 0
	@ args = 16, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #80
.LCFI14:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	str	r2, [fp, #-36]
	str	r3, [fp, #-40]
	.loc 1 750 0
	ldr	r3, [fp, #-32]
	cmp	r3, #13
	bhi	.L12
	.loc 1 752 0
	ldr	r3, [fp, #-32]
	add	r2, r3, #4
	ldr	r3, .L14
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #24
	mov	r0, r2
	mov	r1, #17
	ldr	r2, .L14+4
	bl	snprintf
	.loc 1 754 0
	ldr	r3, [fp, #-32]
	mov	r3, r3, asl #4
	add	r3, r3, #84
	ldr	r2, [fp, #-28]
	add	r2, r2, r3
	ldr	r3, [fp, #-32]
	add	r3, r3, #49152
	add	r3, r3, #147
	ldr	r1, [fp, #-28]
	add	r0, r1, #316
	ldr	r1, [fp, #-32]
	add	r1, r1, #4
	ldr	ip, .L14+8
	str	ip, [sp, #0]
	ldr	ip, .L14+8
	str	ip, [sp, #4]
	ldr	ip, [fp, #-40]
	str	ip, [sp, #8]
	str	r3, [sp, #12]
	ldr	r3, [fp, #4]
	str	r3, [sp, #16]
	ldr	r3, [fp, #8]
	str	r3, [sp, #20]
	ldr	r3, [fp, #12]
	str	r3, [sp, #24]
	ldr	r3, [fp, #16]
	str	r3, [sp, #28]
	str	r0, [sp, #32]
	str	r1, [sp, #36]
	.loc 1 768 0
	sub	r3, fp, #24
	.loc 1 754 0
	str	r3, [sp, #40]
	ldr	r0, [fp, #-28]
	mov	r1, r2
	ldr	r2, [fp, #-36]
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L11
.L12:
	.loc 1 780 0
	ldr	r0, .L14+12
	mov	r1, #780
	bl	Alert_index_out_of_range_with_filename
.L11:
	.loc 1 785 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L15:
	.align	2
.L14:
	.word	LIGHTS_database_field_names
	.word	.LC18
	.word	86400
	.word	.LC19
.LFE4:
	.size	nm_LIGHTS_set_start_time_1, .-nm_LIGHTS_set_start_time_1
	.section .rodata
	.align	2
.LC20:
	.ascii	"%sStartTime2\000"
	.section	.text.nm_LIGHTS_set_start_time_2,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_start_time_2
	.type	nm_LIGHTS_set_start_time_2, %function
nm_LIGHTS_set_start_time_2:
.LFB5:
	.loc 1 837 0
	@ args = 16, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #80
.LCFI17:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	str	r2, [fp, #-36]
	str	r3, [fp, #-40]
	.loc 1 840 0
	ldr	r3, [fp, #-32]
	cmp	r3, #13
	bhi	.L17
	.loc 1 842 0
	ldr	r3, [fp, #-32]
	add	r2, r3, #4
	ldr	r3, .L19
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #24
	mov	r0, r2
	mov	r1, #17
	ldr	r2, .L19+4
	bl	snprintf
	.loc 1 844 0
	ldr	r3, [fp, #-32]
	mov	r3, r3, asl #4
	add	r3, r3, #88
	ldr	r2, [fp, #-28]
	add	r2, r2, r3
	ldr	r3, [fp, #-32]
	add	r3, r3, #49152
	add	r3, r3, #161
	ldr	r1, [fp, #-28]
	add	r0, r1, #316
	ldr	r1, [fp, #-32]
	add	r1, r1, #4
	ldr	ip, .L19+8
	str	ip, [sp, #0]
	ldr	ip, .L19+8
	str	ip, [sp, #4]
	ldr	ip, [fp, #-40]
	str	ip, [sp, #8]
	str	r3, [sp, #12]
	ldr	r3, [fp, #4]
	str	r3, [sp, #16]
	ldr	r3, [fp, #8]
	str	r3, [sp, #20]
	ldr	r3, [fp, #12]
	str	r3, [sp, #24]
	ldr	r3, [fp, #16]
	str	r3, [sp, #28]
	str	r0, [sp, #32]
	str	r1, [sp, #36]
	.loc 1 858 0
	sub	r3, fp, #24
	.loc 1 844 0
	str	r3, [sp, #40]
	ldr	r0, [fp, #-28]
	mov	r1, r2
	ldr	r2, [fp, #-36]
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L16
.L17:
	.loc 1 870 0
	ldr	r0, .L19+12
	ldr	r1, .L19+16
	bl	Alert_index_out_of_range_with_filename
.L16:
	.loc 1 875 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L20:
	.align	2
.L19:
	.word	LIGHTS_database_field_names
	.word	.LC20
	.word	86400
	.word	.LC19
	.word	870
.LFE5:
	.size	nm_LIGHTS_set_start_time_2, .-nm_LIGHTS_set_start_time_2
	.section .rodata
	.align	2
.LC21:
	.ascii	"%sStopTime1\000"
	.section	.text.nm_LIGHTS_set_stop_time_1,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_stop_time_1
	.type	nm_LIGHTS_set_stop_time_1, %function
nm_LIGHTS_set_stop_time_1:
.LFB6:
	.loc 1 927 0
	@ args = 16, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #80
.LCFI20:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	str	r2, [fp, #-36]
	str	r3, [fp, #-40]
	.loc 1 930 0
	ldr	r3, [fp, #-32]
	cmp	r3, #13
	bhi	.L22
	.loc 1 932 0
	ldr	r3, [fp, #-32]
	add	r2, r3, #4
	ldr	r3, .L24
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #24
	mov	r0, r2
	mov	r1, #17
	ldr	r2, .L24+4
	bl	snprintf
	.loc 1 934 0
	ldr	r3, [fp, #-32]
	mov	r3, r3, asl #4
	add	r3, r3, #92
	ldr	r2, [fp, #-28]
	add	r2, r2, r3
	ldr	r3, [fp, #-32]
	add	r3, r3, #49152
	add	r3, r3, #175
	ldr	r1, [fp, #-28]
	add	r0, r1, #316
	ldr	r1, [fp, #-32]
	add	r1, r1, #4
	ldr	ip, .L24+8
	str	ip, [sp, #0]
	ldr	ip, .L24+8
	str	ip, [sp, #4]
	ldr	ip, [fp, #-40]
	str	ip, [sp, #8]
	str	r3, [sp, #12]
	ldr	r3, [fp, #4]
	str	r3, [sp, #16]
	ldr	r3, [fp, #8]
	str	r3, [sp, #20]
	ldr	r3, [fp, #12]
	str	r3, [sp, #24]
	ldr	r3, [fp, #16]
	str	r3, [sp, #28]
	str	r0, [sp, #32]
	str	r1, [sp, #36]
	.loc 1 948 0
	sub	r3, fp, #24
	.loc 1 934 0
	str	r3, [sp, #40]
	ldr	r0, [fp, #-28]
	mov	r1, r2
	ldr	r2, [fp, #-36]
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L21
.L22:
	.loc 1 960 0
	ldr	r0, .L24+12
	mov	r1, #960
	bl	Alert_index_out_of_range_with_filename
.L21:
	.loc 1 965 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L25:
	.align	2
.L24:
	.word	LIGHTS_database_field_names
	.word	.LC21
	.word	86400
	.word	.LC19
.LFE6:
	.size	nm_LIGHTS_set_stop_time_1, .-nm_LIGHTS_set_stop_time_1
	.section .rodata
	.align	2
.LC22:
	.ascii	"%sStopTime2\000"
	.section	.text.nm_LIGHTS_set_stop_time_2,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_stop_time_2
	.type	nm_LIGHTS_set_stop_time_2, %function
nm_LIGHTS_set_stop_time_2:
.LFB7:
	.loc 1 1017 0
	@ args = 16, pretend = 0, frame = 36
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #80
.LCFI23:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	str	r2, [fp, #-36]
	str	r3, [fp, #-40]
	.loc 1 1020 0
	ldr	r3, [fp, #-32]
	cmp	r3, #13
	bhi	.L27
	.loc 1 1022 0
	ldr	r3, [fp, #-32]
	add	r2, r3, #4
	ldr	r3, .L29
	ldr	r3, [r3, r2, asl #2]
	sub	r2, fp, #24
	mov	r0, r2
	mov	r1, #17
	ldr	r2, .L29+4
	bl	snprintf
	.loc 1 1024 0
	ldr	r3, [fp, #-32]
	add	r3, r3, #6
	mov	r3, r3, asl #4
	ldr	r2, [fp, #-28]
	add	r2, r2, r3
	ldr	r3, [fp, #-32]
	add	r3, r3, #49152
	add	r3, r3, #189
	ldr	r1, [fp, #-28]
	add	r0, r1, #316
	ldr	r1, [fp, #-32]
	add	r1, r1, #4
	ldr	ip, .L29+8
	str	ip, [sp, #0]
	ldr	ip, .L29+8
	str	ip, [sp, #4]
	ldr	ip, [fp, #-40]
	str	ip, [sp, #8]
	str	r3, [sp, #12]
	ldr	r3, [fp, #4]
	str	r3, [sp, #16]
	ldr	r3, [fp, #8]
	str	r3, [sp, #20]
	ldr	r3, [fp, #12]
	str	r3, [sp, #24]
	ldr	r3, [fp, #16]
	str	r3, [sp, #28]
	str	r0, [sp, #32]
	str	r1, [sp, #36]
	.loc 1 1038 0
	sub	r3, fp, #24
	.loc 1 1024 0
	str	r3, [sp, #40]
	ldr	r0, [fp, #-28]
	mov	r1, r2
	ldr	r2, [fp, #-36]
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L26
.L27:
	.loc 1 1050 0
	ldr	r0, .L29+12
	ldr	r1, .L29+16
	bl	Alert_index_out_of_range_with_filename
.L26:
	.loc 1 1055 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L30:
	.align	2
.L29:
	.word	LIGHTS_database_field_names
	.word	.LC22
	.word	86400
	.word	.LC19
	.word	1050
.LFE7:
	.size	nm_LIGHTS_set_stop_time_2, .-nm_LIGHTS_set_stop_time_2
	.section	.text.nm_LIGHTS_store_changes,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_store_changes, %function
nm_LIGHTS_store_changes:
.LFB8:
	.loc 1 1111 0
	@ args = 12, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI24:
	add	fp, sp, #8
.LCFI25:
	sub	sp, sp, #44
.LCFI26:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	str	r3, [fp, #-36]
	.loc 1 1116 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 1120 0
	sub	r3, fp, #20
	ldr	r0, .L47
	ldr	r1, [fp, #-24]
	mov	r2, r3
	bl	nm_GROUP_find_this_group_in_list
	.loc 1 1122 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L32
	.loc 1 1134 0
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L33
	.loc 1 1134 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #8]
	cmp	r3, #15
	beq	.L33
	ldr	r3, [fp, #8]
	cmp	r3, #16
	beq	.L33
	.loc 1 1136 0 is_stmt 1
	ldr	r0, [fp, #-24]
	bl	nm_GROUP_get_name
	mov	r3, r0
	mov	r2, #48
	str	r2, [sp, #0]
	ldr	r2, [fp, #-32]
	str	r2, [sp, #4]
	ldr	r2, [fp, #-36]
	str	r2, [sp, #8]
	mov	r0, #49152
	mov	r1, r3
	mov	r2, #0
	mov	r3, #0
	bl	Alert_ChangeLine_Group
.L33:
	.loc 1 1144 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1148 0
	ldr	r3, [fp, #-20]
	mov	r0, r3
	ldr	r1, [fp, #8]
	bl	LIGHTS_get_change_bits_ptr
	str	r0, [fp, #-16]
	.loc 1 1172 0
	ldr	r3, [fp, #-32]
	cmp	r3, #2
	beq	.L34
	.loc 1 1172 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #12]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L35
.L34:
	.loc 1 1174 0 is_stmt 1
	ldr	r4, [fp, #-20]
	ldr	r0, [fp, #-24]
	bl	nm_GROUP_get_name
	mov	r2, r0
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r1, [fp, #-36]
	str	r1, [sp, #0]
	ldr	r1, [fp, #4]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #8]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	ldr	r3, [fp, #-32]
	bl	nm_LIGHTS_set_name
.L35:
	.loc 1 1181 0
	ldr	r3, [fp, #-32]
	cmp	r3, #2
	beq	.L36
	.loc 1 1181 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #12]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L37
.L36:
	.loc 1 1183 0 is_stmt 1
	ldr	r1, [fp, #-20]
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #72]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-36]
	str	r0, [sp, #0]
	ldr	r0, [fp, #4]
	str	r0, [sp, #4]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #8]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	ldr	r3, [fp, #-32]
	bl	nm_LIGHTS_set_box_index
.L37:
	.loc 1 1190 0
	ldr	r3, [fp, #-32]
	cmp	r3, #2
	beq	.L38
	.loc 1 1190 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #12]
	and	r3, r3, #4
	cmp	r3, #0
	beq	.L39
.L38:
	.loc 1 1192 0 is_stmt 1
	ldr	r1, [fp, #-20]
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-36]
	str	r0, [sp, #0]
	ldr	r0, [fp, #4]
	str	r0, [sp, #4]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #8]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	ldr	r3, [fp, #-32]
	bl	nm_LIGHTS_set_output_index
.L39:
	.loc 1 1199 0
	ldr	r3, [fp, #-32]
	cmp	r3, #2
	beq	.L40
	.loc 1 1199 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #12]
	and	r3, r3, #8
	cmp	r3, #0
	beq	.L41
.L40:
	.loc 1 1201 0 is_stmt 1
	ldr	r1, [fp, #-20]
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #80]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-36]
	str	r0, [sp, #0]
	ldr	r0, [fp, #4]
	str	r0, [sp, #4]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #8]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	ldr	r3, [fp, #-32]
	bl	nm_LIGHTS_set_physically_available
.L41:
	.loc 1 1210 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L42
.L45:
	.loc 1 1212 0
	ldr	r3, [fp, #-32]
	cmp	r3, #2
	beq	.L43
	.loc 1 1212 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r2, #1
	mov	r2, r2, asl r3
	ldr	r3, [fp, #12]
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L44
.L43:
	.loc 1 1214 0 is_stmt 1
	ldr	r1, [fp, #-20]
	.loc 1 1216 0
	ldr	r0, [fp, #-24]
	.loc 1 1214 0
	ldr	r2, [fp, #-12]
	mov	r3, #84
	mov	r2, r2, asl #4
	add	r2, r0, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-32]
	str	r0, [sp, #0]
	ldr	r0, [fp, #-36]
	str	r0, [sp, #4]
	ldr	r0, [fp, #4]
	str	r0, [sp, #8]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #12]
	mov	r0, r1
	ldr	r1, [fp, #-12]
	bl	nm_LIGHTS_set_start_time_1
	.loc 1 1226 0
	ldr	r1, [fp, #-20]
	.loc 1 1228 0
	ldr	r0, [fp, #-24]
	.loc 1 1226 0
	ldr	r2, [fp, #-12]
	mov	r3, #88
	mov	r2, r2, asl #4
	add	r2, r0, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-32]
	str	r0, [sp, #0]
	ldr	r0, [fp, #-36]
	str	r0, [sp, #4]
	ldr	r0, [fp, #4]
	str	r0, [sp, #8]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #12]
	mov	r0, r1
	ldr	r1, [fp, #-12]
	bl	nm_LIGHTS_set_start_time_2
	.loc 1 1238 0
	ldr	r1, [fp, #-20]
	.loc 1 1240 0
	ldr	r0, [fp, #-24]
	.loc 1 1238 0
	ldr	r2, [fp, #-12]
	mov	r3, #92
	mov	r2, r2, asl #4
	add	r2, r0, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-32]
	str	r0, [sp, #0]
	ldr	r0, [fp, #-36]
	str	r0, [sp, #4]
	ldr	r0, [fp, #4]
	str	r0, [sp, #8]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #12]
	mov	r0, r1
	ldr	r1, [fp, #-12]
	bl	nm_LIGHTS_set_stop_time_1
	.loc 1 1250 0
	ldr	r1, [fp, #-20]
	.loc 1 1252 0
	ldr	r3, [fp, #-24]
	.loc 1 1250 0
	ldr	r2, [fp, #-12]
	add	r2, r2, #6
	ldr	r2, [r3, r2, asl #4]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [fp, #-32]
	str	r0, [sp, #0]
	ldr	r0, [fp, #-36]
	str	r0, [sp, #4]
	ldr	r0, [fp, #4]
	str	r0, [sp, #8]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #12]
	mov	r0, r1
	ldr	r1, [fp, #-12]
	bl	nm_LIGHTS_set_stop_time_2
.L44:
	.loc 1 1210 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L42:
	.loc 1 1210 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #13
	bls	.L45
	.loc 1 1210 0
	b	.L31
.L32:
	.loc 1 1268 0 is_stmt 1
	ldr	r0, .L47+4
	ldr	r1, .L47+8
	bl	Alert_group_not_found_with_filename
.L31:
	.loc 1 1271 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L48:
	.align	2
.L47:
	.word	light_list_hdr
	.word	.LC19
	.word	1268
.LFE8:
	.size	nm_LIGHTS_store_changes, .-nm_LIGHTS_store_changes
	.section	.text.nm_LIGHTS_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_extract_and_store_changes_from_comm
	.type	nm_LIGHTS_extract_and_store_changes_from_comm, %function
nm_LIGHTS_extract_and_store_changes_from_comm:
.LFB9:
	.loc 1 1310 0
	@ args = 0, pretend = 0, frame = 60
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #72
.LCFI29:
	str	r0, [fp, #-52]
	str	r1, [fp, #-56]
	str	r2, [fp, #-60]
	str	r3, [fp, #-64]
	.loc 1 1345 0
	mov	r3, #0
	str	r3, [fp, #-24]
	.loc 1 1347 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 1352 0
	sub	r3, fp, #36
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #4
	bl	memcpy
	.loc 1 1353 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #4
	str	r3, [fp, #-52]
	.loc 1 1354 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #4
	str	r3, [fp, #-24]
	.loc 1 1375 0
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L50
.L61:
	.loc 1 1383 0
	sub	r3, fp, #40
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #4
	bl	memcpy
	.loc 1 1384 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #4
	str	r3, [fp, #-52]
	.loc 1 1385 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #4
	str	r3, [fp, #-24]
	.loc 1 1387 0
	sub	r3, fp, #44
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #4
	bl	memcpy
	.loc 1 1388 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #4
	str	r3, [fp, #-52]
	.loc 1 1389 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #4
	str	r3, [fp, #-24]
	.loc 1 1391 0
	sub	r3, fp, #48
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #4
	bl	memcpy
	.loc 1 1392 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #4
	str	r3, [fp, #-52]
	.loc 1 1393 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #4
	str	r3, [fp, #-24]
	.loc 1 1395 0
	sub	r3, fp, #32
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #4
	bl	memcpy
	.loc 1 1396 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #4
	str	r3, [fp, #-52]
	.loc 1 1397 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #4
	str	r3, [fp, #-24]
	.loc 1 1404 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-44]
	mov	r0, r2
	mov	r1, r3
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	str	r0, [fp, #-8]
	.loc 1 1408 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L51
	.loc 1 1410 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-44]
	mov	r0, r2
	mov	r1, r3
	bl	nm_LIGHTS_create_new_group
	str	r0, [fp, #-8]
	.loc 1 1412 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 1419 0
	ldr	r3, [fp, #-64]
	cmp	r3, #16
	bne	.L51
	.loc 1 1427 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #308
	ldr	r3, .L65
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
.L51:
	.loc 1 1447 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L52
	.loc 1 1453 0
	mov	r0, #324
	ldr	r1, .L65+4
	ldr	r2, .L65+8
	bl	mem_malloc_debug
	str	r0, [fp, #-28]
	.loc 1 1458 0
	ldr	r0, [fp, #-28]
	mov	r1, #0
	mov	r2, #324
	bl	memset
	.loc 1 1465 0
	ldr	r0, [fp, #-28]
	ldr	r1, [fp, #-8]
	mov	r2, #324
	bl	memcpy
	.loc 1 1494 0
	ldr	r3, [fp, #-32]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L53
	.loc 1 1496 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #20
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #48
	bl	memcpy
	.loc 1 1497 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #48
	str	r3, [fp, #-52]
	.loc 1 1498 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #48
	str	r3, [fp, #-24]
.L53:
	.loc 1 1503 0
	ldr	r3, [fp, #-32]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L54
	.loc 1 1505 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #72
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #4
	bl	memcpy
	.loc 1 1506 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #4
	str	r3, [fp, #-52]
	.loc 1 1507 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #4
	str	r3, [fp, #-24]
.L54:
	.loc 1 1512 0
	ldr	r3, [fp, #-32]
	and	r3, r3, #4
	cmp	r3, #0
	beq	.L55
	.loc 1 1514 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #76
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #4
	bl	memcpy
	.loc 1 1515 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #4
	str	r3, [fp, #-52]
	.loc 1 1516 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #4
	str	r3, [fp, #-24]
.L55:
	.loc 1 1519 0
	ldr	r3, [fp, #-32]
	and	r3, r3, #8
	cmp	r3, #0
	beq	.L56
	.loc 1 1521 0
	ldr	r3, [fp, #-28]
	add	r3, r3, #80
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #4
	bl	memcpy
	.loc 1 1522 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #4
	str	r3, [fp, #-52]
	.loc 1 1523 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #4
	str	r3, [fp, #-24]
.L56:
	.loc 1 1526 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L57
.L59:
	.loc 1 1529 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #4
	mov	r2, #1
	mov	r2, r2, asl r3
	ldr	r3, [fp, #-32]
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L58
	.loc 1 1531 0
	ldr	r3, [fp, #-28]
	add	r2, r3, #84
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #4
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, [fp, #-52]
	mov	r2, #16
	bl	memcpy
	.loc 1 1532 0
	ldr	r3, [fp, #-52]
	add	r3, r3, #16
	str	r3, [fp, #-52]
	.loc 1 1533 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #16
	str	r3, [fp, #-24]
.L58:
	.loc 1 1526 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L57:
	.loc 1 1526 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #13
	bls	.L59
	.loc 1 1555 0 is_stmt 1
	ldr	r3, [fp, #-32]
	ldr	r2, [fp, #-60]
	str	r2, [sp, #0]
	ldr	r2, [fp, #-64]
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-28]
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-56]
	mov	r3, #0
	bl	nm_LIGHTS_store_changes
	.loc 1 1562 0
	ldr	r0, [fp, #-28]
	ldr	r1, .L65+4
	ldr	r2, .L65+12
	bl	mem_free_debug
	b	.L60
.L52:
	.loc 1 1581 0
	ldr	r0, .L65+4
	ldr	r1, .L65+16
	bl	Alert_group_not_found_with_filename
.L60:
	.loc 1 1375 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L50:
	.loc 1 1375 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-36]
	ldr	r2, [fp, #-20]
	cmp	r2, r3
	bcc	.L61
	.loc 1 1590 0 is_stmt 1
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L62
	.loc 1 1603 0
	ldr	r3, [fp, #-64]
	cmp	r3, #1
	beq	.L63
	.loc 1 1603 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-64]
	cmp	r3, #15
	beq	.L63
	.loc 1 1604 0 is_stmt 1
	ldr	r3, [fp, #-64]
	cmp	r3, #16
	bne	.L64
	.loc 1 1605 0
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	mov	r3, r0
	cmp	r3, #0
	bne	.L64
.L63:
	.loc 1 1610 0
	mov	r0, #16
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L64
.L62:
	.loc 1 1615 0
	ldr	r0, .L65+4
	ldr	r1, .L65+20
	bl	Alert_bit_set_with_no_data_with_filename
.L64:
	.loc 1 1620 0
	ldr	r3, [fp, #-24]
	.loc 1 1621 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L66:
	.align	2
.L65:
	.word	list_lights_recursive_MUTEX
	.word	.LC19
	.word	1453
	.word	1562
	.word	1581
	.word	1615
.LFE9:
	.size	nm_LIGHTS_extract_and_store_changes_from_comm, .-nm_LIGHTS_extract_and_store_changes_from_comm
	.section	.rodata.LIGHTS_FILENAME,"a",%progbits
	.align	2
	.type	LIGHTS_FILENAME, %object
	.size	LIGHTS_FILENAME, 7
LIGHTS_FILENAME:
	.ascii	"LIGHTS\000"
	.global	light_list_hdr
	.section	.bss.light_list_hdr,"aw",%nobits
	.align	2
	.type	light_list_hdr, %object
	.size	light_list_hdr, 20
light_list_hdr:
	.space	20
	.global	LIGHTS_DEFAULT_NAME
	.section	.rodata.LIGHTS_DEFAULT_NAME,"a",%progbits
	.align	2
	.type	LIGHTS_DEFAULT_NAME, %object
	.size	LIGHTS_DEFAULT_NAME, 26
LIGHTS_DEFAULT_NAME:
	.ascii	"(description not yet set)\000"
	.global	g_LIGHTS_initial_date_time
	.section	.bss.g_LIGHTS_initial_date_time,"aw",%nobits
	.align	2
	.type	g_LIGHTS_initial_date_time, %object
	.size	g_LIGHTS_initial_date_time, 6
g_LIGHTS_initial_date_time:
	.space	6
	.global	light_list_item_sizes
	.section	.rodata.light_list_item_sizes,"a",%progbits
	.align	2
	.type	light_list_item_sizes, %object
	.size	light_list_item_sizes, 4
light_list_item_sizes:
	.word	324
	.section	.text.nm_light_structure_updater,"ax",%progbits
	.align	2
	.type	nm_light_structure_updater, %function
nm_light_structure_updater:
.LFB10:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/lights.c"
	.loc 2 105 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI30:
	add	fp, sp, #0
.LCFI31:
	sub	sp, sp, #4
.LCFI32:
	str	r0, [fp, #-4]
	.loc 2 108 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE10:
	.size	nm_light_structure_updater, .-nm_light_structure_updater
	.section	.text.init_file_LIGHTS,"ax",%progbits
	.align	2
	.global	init_file_LIGHTS
	.type	init_file_LIGHTS, %function
init_file_LIGHTS:
.LFB11:
	.loc 2 112 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #24
.LCFI35:
	.loc 2 113 0
	ldr	r3, .L69
	ldr	r3, [r3, #0]
	ldr	r2, .L69+4
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	ldr	r3, .L69+8
	str	r3, [sp, #8]
	ldr	r3, .L69+12
	str	r3, [sp, #12]
	ldr	r3, .L69+16
	str	r3, [sp, #16]
	mov	r3, #16
	str	r3, [sp, #20]
	mov	r0, #1
	ldr	r1, .L69+20
	mov	r2, #0
	ldr	r3, .L69+24
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	.loc 2 123 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L70:
	.align	2
.L69:
	.word	list_lights_recursive_MUTEX
	.word	light_list_item_sizes
	.word	nm_light_structure_updater
	.word	nm_LIGHTS_set_default_values
	.word	LIGHTS_DEFAULT_NAME
	.word	LIGHTS_FILENAME
	.word	light_list_hdr
.LFE11:
	.size	init_file_LIGHTS, .-init_file_LIGHTS
	.section	.text.save_file_LIGHTS,"ax",%progbits
	.align	2
	.global	save_file_LIGHTS
	.type	save_file_LIGHTS, %function
save_file_LIGHTS:
.LFB12:
	.loc 2 127 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #12
.LCFI38:
	.loc 2 131 0
	ldr	r3, .L72
	ldr	r3, [r3, #0]
	mov	r2, #324
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	mov	r3, #16
	str	r3, [sp, #8]
	mov	r0, #1
	ldr	r1, .L72+4
	mov	r2, #0
	ldr	r3, .L72+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	.loc 2 138 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L73:
	.align	2
.L72:
	.word	list_lights_recursive_MUTEX
	.word	LIGHTS_FILENAME
	.word	light_list_hdr
.LFE12:
	.size	save_file_LIGHTS, .-save_file_LIGHTS
	.section .rodata
	.align	2
.LC23:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/lights.c\000"
	.section	.text.nm_LIGHTS_set_default_values,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_set_default_values, %function
nm_LIGHTS_set_default_values:
.LFB13:
	.loc 2 163 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI39:
	add	fp, sp, #4
.LCFI40:
	sub	sp, sp, #36
.LCFI41:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 2 172 0
	bl	FLOWSENSE_get_controller_index
	str	r0, [fp, #-12]
	.loc 2 176 0
	ldr	r3, [fp, #-20]
	cmp	r3, #0
	beq	.L75
	.loc 2 178 0
	ldr	r3, .L79
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L79+4
	mov	r3, #178
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 183 0
	ldr	r3, [fp, #-20]
	add	r2, r3, #308
	ldr	r3, .L79
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
	.loc 2 184 0
	ldr	r3, [fp, #-20]
	add	r2, r3, #312
	ldr	r3, .L79
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
	.loc 2 185 0
	ldr	r3, [fp, #-20]
	add	r2, r3, #320
	ldr	r3, .L79
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
	.loc 2 189 0
	ldr	r3, [fp, #-20]
	add	r2, r3, #316
	ldr	r3, .L79
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_set_all_32_bit_change_bits
	.loc 2 196 0
	ldr	r3, [fp, #-20]
	add	r3, r3, #308
	str	r3, [fp, #-16]
	.loc 2 200 0
	ldr	r3, [fp, #-12]
	str	r3, [sp, #0]
	ldr	r3, [fp, #-24]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-16]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-20]
	ldr	r1, .L79+8
	mov	r2, #0
	mov	r3, #11
	bl	nm_LIGHTS_set_name
	.loc 2 206 0
	ldr	r3, [fp, #-12]
	str	r3, [sp, #0]
	ldr	r3, [fp, #-24]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-16]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-12]
	mov	r2, #0
	mov	r3, #11
	bl	nm_LIGHTS_set_box_index
	.loc 2 208 0
	ldr	r3, [fp, #-12]
	str	r3, [sp, #0]
	ldr	r3, [fp, #-24]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-16]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-20]
	mov	r1, #0
	mov	r2, #0
	mov	r3, #11
	bl	nm_LIGHTS_set_output_index
	.loc 2 212 0
	ldr	r3, [fp, #-12]
	str	r3, [sp, #0]
	ldr	r3, [fp, #-24]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-16]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-20]
	mov	r1, #0
	mov	r2, #0
	mov	r3, #11
	bl	nm_LIGHTS_set_physically_available
	.loc 2 214 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L76
.L77:
	.loc 2 216 0 discriminator 2
	mov	r3, #11
	str	r3, [sp, #0]
	ldr	r3, [fp, #-12]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-24]
	str	r3, [sp, #8]
	ldr	r3, [fp, #-16]
	str	r3, [sp, #12]
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-8]
	ldr	r2, .L79+12
	mov	r3, #0
	bl	nm_LIGHTS_set_start_time_1
	.loc 2 218 0 discriminator 2
	mov	r3, #11
	str	r3, [sp, #0]
	ldr	r3, [fp, #-12]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-24]
	str	r3, [sp, #8]
	ldr	r3, [fp, #-16]
	str	r3, [sp, #12]
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-8]
	ldr	r2, .L79+12
	mov	r3, #0
	bl	nm_LIGHTS_set_start_time_2
	.loc 2 220 0 discriminator 2
	mov	r3, #11
	str	r3, [sp, #0]
	ldr	r3, [fp, #-12]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-24]
	str	r3, [sp, #8]
	ldr	r3, [fp, #-16]
	str	r3, [sp, #12]
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-8]
	ldr	r2, .L79+12
	mov	r3, #0
	bl	nm_LIGHTS_set_stop_time_1
	.loc 2 222 0 discriminator 2
	mov	r3, #11
	str	r3, [sp, #0]
	ldr	r3, [fp, #-12]
	str	r3, [sp, #4]
	ldr	r3, [fp, #-24]
	str	r3, [sp, #8]
	ldr	r3, [fp, #-16]
	str	r3, [sp, #12]
	ldr	r0, [fp, #-20]
	ldr	r1, [fp, #-8]
	ldr	r2, .L79+12
	mov	r3, #0
	bl	nm_LIGHTS_set_stop_time_2
	.loc 2 214 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L76:
	.loc 2 214 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #13
	bls	.L77
	.loc 2 225 0 is_stmt 1
	ldr	r3, .L79
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	b	.L74
.L75:
	.loc 2 229 0
	ldr	r0, .L79+4
	mov	r1, #229
	bl	Alert_func_call_with_null_ptr_with_filename
.L74:
	.loc 2 231 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L80:
	.align	2
.L79:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	LIGHTS_DEFAULT_NAME
	.word	86400
.LFE13:
	.size	nm_LIGHTS_set_default_values, .-nm_LIGHTS_set_default_values
	.section	.text.nm_LIGHTS_create_new_group,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_create_new_group
	.type	nm_LIGHTS_create_new_group, %function
nm_LIGHTS_create_new_group:
.LFB14:
	.loc 2 253 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI42:
	add	fp, sp, #4
.LCFI43:
	sub	sp, sp, #24
.LCFI44:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	.loc 2 256 0
	ldr	r0, .L89
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 258 0
	b	.L82
.L86:
	.loc 2 262 0
	ldr	r0, [fp, #-8]
	bl	LIGHTS_get_box_index
	mov	r2, r0
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bhi	.L87
.L83:
	.loc 2 266 0
	ldr	r0, [fp, #-8]
	bl	LIGHTS_get_box_index
	mov	r2, r0
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L85
	.loc 2 269 0
	ldr	r0, [fp, #-8]
	bl	LIGHTS_get_output_index
	mov	r2, r0
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bhi	.L88
.L85:
	.loc 2 276 0
	ldr	r0, .L89
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L82:
	.loc 2 258 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L86
	b	.L84
.L87:
	.loc 2 264 0
	mov	r0, r0	@ nop
	b	.L84
.L88:
	.loc 2 272 0
	mov	r0, r0	@ nop
.L84:
	.loc 2 281 0
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, [fp, #-8]
	str	r3, [sp, #4]
	ldr	r0, .L89
	ldr	r1, .L89+4
	ldr	r2, .L89+8
	mov	r3, #324
	bl	nm_GROUP_create_new_group
	str	r0, [fp, #-12]
	.loc 2 283 0
	ldr	r3, [fp, #-12]
	.loc 2 284 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L90:
	.align	2
.L89:
	.word	light_list_hdr
	.word	LIGHTS_DEFAULT_NAME
	.word	nm_LIGHTS_set_default_values
.LFE14:
	.size	nm_LIGHTS_create_new_group, .-nm_LIGHTS_create_new_group
	.section	.text.LIGHTS_build_data_to_send,"ax",%progbits
	.align	2
	.global	LIGHTS_build_data_to_send
	.type	LIGHTS_build_data_to_send, %function
LIGHTS_build_data_to_send:
.LFB15:
	.loc 2 319 0
	@ args = 4, pretend = 0, frame = 56
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI45:
	add	fp, sp, #4
.LCFI46:
	sub	sp, sp, #84
.LCFI47:
	str	r0, [fp, #-48]
	str	r1, [fp, #-52]
	str	r2, [fp, #-56]
	str	r3, [fp, #-60]
	.loc 2 347 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 2 349 0
	mov	r3, #0
	str	r3, [fp, #-44]
	.loc 2 355 0
	ldr	r3, .L106
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L106+4
	ldr	r3, .L106+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 359 0
	ldr	r0, .L106+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 364 0
	b	.L92
.L95:
	.loc 2 366 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-60]
	bl	LIGHTS_get_change_bits_ptr
	mov	r3, r0
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L93
	.loc 2 368 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #1
	str	r3, [fp, #-44]
	.loc 2 372 0
	b	.L94
.L93:
	.loc 2 375 0
	ldr	r0, .L106+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L92:
	.loc 2 364 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L95
.L94:
	.loc 2 380 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L96
	.loc 2 385 0
	mov	r3, #0
	str	r3, [fp, #-44]
	.loc 2 392 0
	sub	r3, fp, #36
	ldr	r2, [fp, #4]
	str	r2, [sp, #0]
	ldr	r0, [fp, #-48]
	mov	r1, r3
	ldr	r2, [fp, #-52]
	ldr	r3, [fp, #-56]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	str	r0, [fp, #-20]
	.loc 2 398 0
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L96
	.loc 2 400 0
	ldr	r0, .L106+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 402 0
	b	.L97
.L104:
	.loc 2 404 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-60]
	bl	LIGHTS_get_change_bits_ptr
	str	r0, [fp, #-24]
	.loc 2 407 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L98
	.loc 2 411 0
	mov	r3, #16
	str	r3, [fp, #-28]
	.loc 2 415 0
	ldr	r3, [fp, #-56]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L99
	.loc 2 415 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-52]
	ldr	r3, [fp, #-20]
	add	r2, r2, r3
	ldr	r3, [fp, #-28]
	add	r2, r2, r3
	ldr	r3, [fp, #4]
	cmp	r2, r3
	bcs	.L99
	.loc 2 417 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r3, r3, #72
	ldr	r0, [fp, #-48]
	mov	r1, r3
	mov	r2, #4
	bl	PDATA_copy_var_into_pucp
	.loc 2 419 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #76
	ldr	r0, [fp, #-48]
	mov	r1, r3
	mov	r2, #4
	bl	PDATA_copy_var_into_pucp
	.loc 2 421 0
	sub	r3, fp, #40
	ldr	r0, [fp, #-48]
	mov	r1, #4
	mov	r2, #4
	bl	PDATA_copy_bitfield_info_into_pucp
	.loc 2 434 0
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-12]
	.loc 2 439 0
	mov	r3, #0
	str	r3, [fp, #-32]
	.loc 2 441 0
	ldr	r3, [fp, #-8]
	add	r0, r3, #320
	.loc 2 447 0
	ldr	r3, [fp, #-8]
	add	r1, r3, #20
	.loc 2 449 0
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-20]
	add	r2, r2, r3
	.loc 2 441 0
	ldr	r3, [fp, #-52]
	add	r2, r2, r3
	sub	r3, fp, #32
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	mov	r1, #48
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	ldr	r2, [fp, #-56]
	str	r2, [sp, #16]
	ldr	r2, [fp, #4]
	str	r2, [sp, #20]
	ldr	r2, [fp, #-60]
	str	r2, [sp, #24]
	ldr	r0, [fp, #-48]
	mov	r1, r3
	mov	r2, #0
	ldr	r3, [fp, #-24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	mov	r3, r0
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 2 454 0
	ldr	r3, [fp, #-8]
	add	r0, r3, #320
	.loc 2 460 0
	ldr	r3, [fp, #-8]
	add	r1, r3, #72
	.loc 2 462 0
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-20]
	add	r2, r2, r3
	.loc 2 454 0
	ldr	r3, [fp, #-52]
	add	r2, r2, r3
	sub	r3, fp, #32
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	mov	r1, #4
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	ldr	r2, [fp, #-56]
	str	r2, [sp, #16]
	ldr	r2, [fp, #4]
	str	r2, [sp, #20]
	ldr	r2, [fp, #-60]
	str	r2, [sp, #24]
	ldr	r0, [fp, #-48]
	mov	r1, r3
	mov	r2, #1
	ldr	r3, [fp, #-24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	mov	r3, r0
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 2 467 0
	ldr	r3, [fp, #-8]
	add	r0, r3, #320
	.loc 2 473 0
	ldr	r3, [fp, #-8]
	add	r1, r3, #76
	.loc 2 475 0
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-20]
	add	r2, r2, r3
	.loc 2 467 0
	ldr	r3, [fp, #-52]
	add	r2, r2, r3
	sub	r3, fp, #32
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	mov	r1, #4
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	ldr	r2, [fp, #-56]
	str	r2, [sp, #16]
	ldr	r2, [fp, #4]
	str	r2, [sp, #20]
	ldr	r2, [fp, #-60]
	str	r2, [sp, #24]
	ldr	r0, [fp, #-48]
	mov	r1, r3
	mov	r2, #2
	ldr	r3, [fp, #-24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	mov	r3, r0
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 2 480 0
	ldr	r3, [fp, #-8]
	add	r0, r3, #320
	.loc 2 486 0
	ldr	r3, [fp, #-8]
	add	r1, r3, #80
	.loc 2 488 0
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-20]
	add	r2, r2, r3
	.loc 2 480 0
	ldr	r3, [fp, #-52]
	add	r2, r2, r3
	sub	r3, fp, #32
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	mov	r1, #4
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	ldr	r2, [fp, #-56]
	str	r2, [sp, #16]
	ldr	r2, [fp, #4]
	str	r2, [sp, #20]
	ldr	r2, [fp, #-60]
	str	r2, [sp, #24]
	ldr	r0, [fp, #-48]
	mov	r1, r3
	mov	r2, #3
	ldr	r3, [fp, #-24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	mov	r3, r0
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 2 493 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L100
.L99:
	.loc 2 427 0
	ldr	r3, [fp, #-56]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 2 431 0
	b	.L101
.L102:
	.loc 2 495 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #4
	ldr	r2, [fp, #-8]
	add	ip, r2, #320
	.loc 2 501 0 discriminator 2
	ldr	r2, [fp, #-8]
	add	r1, r2, #84
	ldr	r2, [fp, #-16]
	mov	r2, r2, asl #4
	.loc 2 495 0 discriminator 2
	add	r0, r1, r2
	.loc 2 503 0 discriminator 2
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-20]
	add	r1, r1, r2
	.loc 2 495 0 discriminator 2
	ldr	r2, [fp, #-52]
	add	r1, r1, r2
	sub	r2, fp, #32
	str	ip, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, #16
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	ldr	r1, [fp, #-56]
	str	r1, [sp, #16]
	ldr	r1, [fp, #4]
	str	r1, [sp, #20]
	ldr	r1, [fp, #-60]
	str	r1, [sp, #24]
	ldr	r0, [fp, #-48]
	mov	r1, r2
	mov	r2, r3
	ldr	r3, [fp, #-24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	mov	r3, r0
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 2 493 0 discriminator 2
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L100:
	.loc 2 493 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	cmp	r3, #13
	bls	.L102
	.loc 2 514 0 is_stmt 1
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-28]
	cmp	r2, r3
	bls	.L103
	.loc 2 518 0
	ldr	r3, [fp, #-44]
	add	r3, r3, #1
	str	r3, [fp, #-44]
	.loc 2 520 0
	ldr	r2, [fp, #-40]
	sub	r3, fp, #32
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	memcpy
	.loc 2 522 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-20]
	b	.L98
.L103:
	.loc 2 527 0
	ldr	r3, [fp, #-48]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-28]
	rsb	r3, r3, #0
	add	r2, r2, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #0]
.L98:
	.loc 2 532 0
	ldr	r0, .L106+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L97:
	.loc 2 402 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L104
.L101:
	.loc 2 539 0
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L105
	.loc 2 541 0
	ldr	r2, [fp, #-36]
	sub	r3, fp, #44
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	memcpy
	b	.L96
.L105:
	.loc 2 548 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #0]
	sub	r2, r3, #4
	ldr	r3, [fp, #-48]
	str	r2, [r3, #0]
	.loc 2 550 0
	mov	r3, #0
	str	r3, [fp, #-20]
.L96:
	.loc 2 563 0
	ldr	r3, .L106
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 567 0
	ldr	r3, [fp, #-20]
	.loc 2 568 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L107:
	.align	2
.L106:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	355
	.word	light_list_hdr
.LFE15:
	.size	LIGHTS_build_data_to_send, .-LIGHTS_build_data_to_send
	.section	.text.LIGHTS_copy_guivars_to_daily_schedule,"ax",%progbits
	.align	2
	.global	LIGHTS_copy_guivars_to_daily_schedule
	.type	LIGHTS_copy_guivars_to_daily_schedule, %function
LIGHTS_copy_guivars_to_daily_schedule:
.LFB16:
	.loc 2 584 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI48:
	add	fp, sp, #0
.LCFI49:
	sub	sp, sp, #4
.LCFI50:
	str	r0, [fp, #-4]
	.loc 2 587 0
	ldr	r3, .L109
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	mov	r1, r3
	ldr	r3, .L109+4
	ldr	r2, [fp, #-4]
	str	r1, [r3, r2, asl #4]
	.loc 2 588 0
	ldr	r3, .L109+8
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r0, .L109+4
	ldr	r1, [fp, #-4]
	mov	r3, #4
	mov	r1, r1, asl #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 2 589 0
	ldr	r3, .L109+12
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r0, .L109+4
	ldr	r1, [fp, #-4]
	mov	r3, #8
	mov	r1, r1, asl #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 2 590 0
	ldr	r3, .L109+16
	ldr	r2, [r3, #0]
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	mov	r2, r3
	ldr	r0, .L109+4
	ldr	r1, [fp, #-4]
	mov	r3, #12
	mov	r1, r1, asl #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 2 591 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L110:
	.align	2
.L109:
	.word	GuiVar_LightsStartTime1InMinutes
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.word	GuiVar_LightsStartTime2InMinutes
	.word	GuiVar_LightsStopTime1InMinutes
	.word	GuiVar_LightsStopTime2InMinutes
.LFE16:
	.size	LIGHTS_copy_guivars_to_daily_schedule, .-LIGHTS_copy_guivars_to_daily_schedule
	.section	.text.LIGHTS_copy_daily_schedule_to_guivars,"ax",%progbits
	.align	2
	.global	LIGHTS_copy_daily_schedule_to_guivars
	.type	LIGHTS_copy_daily_schedule_to_guivars, %function
LIGHTS_copy_daily_schedule_to_guivars:
.LFB17:
	.loc 2 607 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI51:
	add	fp, sp, #0
.LCFI52:
	sub	sp, sp, #4
.LCFI53:
	str	r0, [fp, #-4]
	.loc 2 610 0
	ldr	r3, .L112
	ldr	r2, [fp, #-4]
	ldr	r2, [r3, r2, asl #4]
	ldr	r3, .L112+4
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L112+8
	str	r2, [r3, #0]
	.loc 2 611 0
	ldr	r1, .L112
	ldr	r2, [fp, #-4]
	mov	r3, #4
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L112+4
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L112+12
	str	r2, [r3, #0]
	.loc 2 612 0
	ldr	r1, .L112
	ldr	r2, [fp, #-4]
	mov	r3, #8
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L112+4
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L112+16
	str	r2, [r3, #0]
	.loc 2 613 0
	ldr	r1, .L112
	ldr	r2, [fp, #-4]
	mov	r3, #12
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, .L112+4
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #5
	ldr	r3, .L112+20
	str	r2, [r3, #0]
	.loc 2 616 0
	ldr	r3, .L112+8
	ldr	r3, [r3, #0]
	cmp	r3, #1440
	moveq	r2, #0
	movne	r2, #1
	ldr	r3, .L112+24
	str	r2, [r3, #0]
	.loc 2 617 0
	ldr	r3, .L112+12
	ldr	r3, [r3, #0]
	cmp	r3, #1440
	moveq	r2, #0
	movne	r2, #1
	ldr	r3, .L112+28
	str	r2, [r3, #0]
	.loc 2 618 0
	ldr	r3, .L112+16
	ldr	r3, [r3, #0]
	cmp	r3, #1440
	moveq	r2, #0
	movne	r2, #1
	ldr	r3, .L112+32
	str	r2, [r3, #0]
	.loc 2 619 0
	ldr	r3, .L112+20
	ldr	r3, [r3, #0]
	cmp	r3, #1440
	moveq	r2, #0
	movne	r2, #1
	ldr	r3, .L112+36
	str	r2, [r3, #0]
	.loc 2 620 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L113:
	.align	2
.L112:
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.word	-2004318071
	.word	GuiVar_LightsStartTime1InMinutes
	.word	GuiVar_LightsStartTime2InMinutes
	.word	GuiVar_LightsStopTime1InMinutes
	.word	GuiVar_LightsStopTime2InMinutes
	.word	GuiVar_LightsStartTimeEnabled1
	.word	GuiVar_LightsStartTimeEnabled2
	.word	GuiVar_LightsStopTimeEnabled1
	.word	GuiVar_LightsStopTimeEnabled2
.LFE17:
	.size	LIGHTS_copy_daily_schedule_to_guivars, .-LIGHTS_copy_daily_schedule_to_guivars
	.section .rodata
	.align	2
.LC24:
	.ascii	"Light %c%d: %s\000"
	.section	.text.LIGHTS_populate_group_name,"ax",%progbits
	.align	2
	.global	LIGHTS_populate_group_name
	.type	LIGHTS_populate_group_name, %function
LIGHTS_populate_group_name:
.LFB18:
	.loc 2 636 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI54:
	add	fp, sp, #12
.LCFI55:
	sub	sp, sp, #20
.LCFI56:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 2 639 0
	ldr	r3, .L117
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L117+4
	ldr	r3, .L117+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 641 0
	ldr	r0, [fp, #-20]
	bl	LIGHTS_get_light_at_this_index
	str	r0, [fp, #-16]
	.loc 2 643 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L115
	.loc 2 646 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #72]
	add	r4, r3, #65
	ldr	r3, .L117+12
	ldr	r5, [r3, #0]
	ldr	r0, [fp, #-16]
	bl	nm_GROUP_get_name
	mov	r3, r0
	str	r5, [sp, #0]
	str	r3, [sp, #4]
	ldr	r0, .L117+16
	mov	r1, #65
	ldr	r2, .L117+20
	mov	r3, r4
	bl	snprintf
	b	.L116
.L115:
	.loc 2 651 0
	ldr	r0, [fp, #-16]
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r0, .L117+16
	mov	r1, r3
	mov	r2, #65
	bl	strlcpy
.L116:
	.loc 2 654 0
	ldr	r3, .L117
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 656 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L118:
	.align	2
.L117:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	639
	.word	GuiVar_LightsOutput
	.word	GuiVar_GroupName
	.word	.LC24
.LFE18:
	.size	LIGHTS_populate_group_name, .-LIGHTS_populate_group_name
	.section	.text.LIGHTS_copy_light_struct_into_guivars,"ax",%progbits
	.align	2
	.global	LIGHTS_copy_light_struct_into_guivars
	.type	LIGHTS_copy_light_struct_into_guivars, %function
LIGHTS_copy_light_struct_into_guivars:
.LFB19:
	.loc 2 670 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI57:
	add	fp, sp, #4
.LCFI58:
	sub	sp, sp, #8
.LCFI59:
	str	r0, [fp, #-12]
	.loc 2 673 0
	ldr	r3, .L122
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L122+4
	ldr	r3, .L122+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 675 0
	ldr	r0, [fp, #-12]
	bl	LIGHTS_get_light_at_this_index
	str	r0, [fp, #-8]
	.loc 2 677 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L120
	.loc 2 679 0
	ldr	r0, [fp, #-8]
	bl	nm_GROUP_get_group_ID
	mov	r2, r0
	ldr	r3, .L122+12
	str	r2, [r3, #0]
	.loc 2 681 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #72]
	ldr	r3, .L122+16
	str	r2, [r3, #0]
	.loc 2 683 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #80]
	ldr	r3, .L122+20
	str	r2, [r3, #0]
	.loc 2 685 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #76]
	ldr	r3, .L122+24
	str	r2, [r3, #0]
	.loc 2 687 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #76]
	add	r2, r3, #1
	ldr	r3, .L122+28
	str	r2, [r3, #0]
	.loc 2 690 0
	ldr	r0, [fp, #-12]
	bl	IRRI_LIGHTS_light_is_energized
	mov	r2, r0
	ldr	r3, .L122+32
	str	r2, [r3, #0]
	.loc 2 692 0
	ldr	r3, .L122+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, .L122+36
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	b	.L121
.L120:
	.loc 2 696 0
	ldr	r0, .L122+4
	mov	r1, #696
	bl	Alert_group_not_found_with_filename
.L121:
	.loc 2 699 0
	ldr	r3, .L122
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 700 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L123:
	.align	2
.L122:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	673
	.word	g_GROUP_ID
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightIsPhysicallyAvailable
	.word	GuiVar_LightsOutputIndex_0
	.word	GuiVar_LightsOutput
	.word	GuiVar_LightIsEnergized
	.word	GuiVar_StationInfoControllerName
.LFE19:
	.size	LIGHTS_copy_light_struct_into_guivars, .-LIGHTS_copy_light_struct_into_guivars
	.section	.text.LIGHTS_copy_file_schedule_into_global_daily_schedule,"ax",%progbits
	.align	2
	.global	LIGHTS_copy_file_schedule_into_global_daily_schedule
	.type	LIGHTS_copy_file_schedule_into_global_daily_schedule, %function
LIGHTS_copy_file_schedule_into_global_daily_schedule:
.LFB20:
	.loc 2 715 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI60:
	add	fp, sp, #4
.LCFI61:
	sub	sp, sp, #12
.LCFI62:
	str	r0, [fp, #-16]
	.loc 2 720 0
	ldr	r3, .L127
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L127+4
	mov	r3, #720
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 722 0
	ldr	r0, [fp, #-16]
	bl	LIGHTS_get_light_at_this_index
	str	r0, [fp, #-12]
	.loc 2 728 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L125
.L126:
	.loc 2 733 0 discriminator 2
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-8]
	mov	r3, #84
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r1, [r3, #0]
	ldr	r3, .L127+8
	ldr	r2, [fp, #-8]
	str	r1, [r3, r2, asl #4]
	.loc 2 734 0 discriminator 2
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-8]
	mov	r3, #88
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r0, .L127+8
	ldr	r1, [fp, #-8]
	mov	r3, #4
	mov	r1, r1, asl #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 2 735 0 discriminator 2
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-8]
	mov	r3, #92
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r0, .L127+8
	ldr	r1, [fp, #-8]
	mov	r3, #8
	mov	r1, r1, asl #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 2 736 0 discriminator 2
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-8]
	add	r2, r2, #6
	ldr	r2, [r3, r2, asl #4]
	ldr	r0, .L127+8
	ldr	r1, [fp, #-8]
	mov	r3, #12
	mov	r1, r1, asl #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 2 728 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L125:
	.loc 2 728 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #13
	bls	.L126
	.loc 2 739 0 is_stmt 1
	ldr	r3, .L127
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 740 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L128:
	.align	2
.L127:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
.LFE20:
	.size	LIGHTS_copy_file_schedule_into_global_daily_schedule, .-LIGHTS_copy_file_schedule_into_global_daily_schedule
	.section	.text.LIGHTS_extract_and_store_group_name_from_GuiVars,"ax",%progbits
	.align	2
	.global	LIGHTS_extract_and_store_group_name_from_GuiVars
	.type	LIGHTS_extract_and_store_group_name_from_GuiVars, %function
LIGHTS_extract_and_store_group_name_from_GuiVars:
.LFB21:
	.loc 2 760 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI63:
	add	fp, sp, #8
.LCFI64:
	sub	sp, sp, #24
.LCFI65:
	.loc 2 763 0
	ldr	r3, .L131
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L131+4
	ldr	r3, .L131+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 765 0
	ldr	r3, .L131+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_light_at_this_index
	str	r0, [fp, #-12]
	.loc 2 767 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L130
	.loc 2 769 0
	bl	FLOWSENSE_get_controller_index
	mov	r4, r0
	ldr	r0, [fp, #-12]
	mov	r1, #2
	bl	LIGHTS_get_change_bits_ptr
	mov	r2, r0
	ldr	r3, [fp, #-12]
	add	r3, r3, #316
	str	r4, [sp, #0]
	mov	r1, #1
	str	r1, [sp, #4]
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	mov	r3, #0
	str	r3, [sp, #16]
	ldr	r0, [fp, #-12]
	ldr	r1, .L131+16
	mov	r2, #1
	mov	r3, #2
	bl	SHARED_set_name_32_bit_change_bits
.L130:
	.loc 2 772 0
	ldr	r3, .L131
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 774 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L132:
	.align	2
.L131:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	763
	.word	g_GROUP_list_item_index
	.word	GuiVar_GroupName
.LFE21:
	.size	LIGHTS_extract_and_store_group_name_from_GuiVars, .-LIGHTS_extract_and_store_group_name_from_GuiVars
	.section	.text.LIGHTS_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	LIGHTS_extract_and_store_changes_from_GuiVars
	.type	LIGHTS_extract_and_store_changes_from_GuiVars, %function
LIGHTS_extract_and_store_changes_from_GuiVars:
.LFB22:
	.loc 2 796 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI66:
	add	fp, sp, #8
.LCFI67:
	sub	sp, sp, #20
.LCFI68:
	.loc 2 799 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 2 801 0
	ldr	r3, .L136
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L136+4
	ldr	r3, .L136+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 805 0
	mov	r0, #324
	ldr	r1, .L136+4
	ldr	r2, .L136+12
	bl	mem_malloc_debug
	str	r0, [fp, #-16]
	.loc 2 809 0
	ldr	r3, .L136+16
	ldr	r2, [r3, #0]
	ldr	r3, .L136+20
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	mov	r3, r0
	ldr	r0, [fp, #-16]
	mov	r1, r3
	mov	r2, #324
	bl	memcpy
	.loc 2 813 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #20
	mov	r0, r3
	ldr	r1, .L136+24
	mov	r2, #48
	bl	strlcpy
	.loc 2 818 0
	ldr	r3, .L136+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	LIGHTS_get_day_index
	mov	r3, r0
	mov	r0, r3
	bl	LIGHTS_copy_guivars_to_daily_schedule
	.loc 2 823 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L134
.L135:
	.loc 2 825 0 discriminator 2
	ldr	r3, .L136+32
	ldr	r2, [fp, #-12]
	ldr	r2, [r3, r2, asl #4]
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-12]
	mov	r3, #84
	mov	r1, r1, asl #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 2 826 0 discriminator 2
	ldr	r1, .L136+32
	ldr	r2, [fp, #-12]
	mov	r3, #4
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-12]
	mov	r3, #88
	mov	r1, r1, asl #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 2 827 0 discriminator 2
	ldr	r1, .L136+32
	ldr	r2, [fp, #-12]
	mov	r3, #8
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-12]
	mov	r3, #92
	mov	r1, r1, asl #4
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	.loc 2 828 0 discriminator 2
	ldr	r1, .L136+32
	ldr	r2, [fp, #-12]
	mov	r3, #12
	mov	r2, r2, asl #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r1, [r3, #0]
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-12]
	add	r2, r2, #6
	str	r1, [r3, r2, asl #4]
	.loc 2 823 0 discriminator 2
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L134:
	.loc 2 823 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #13
	bls	.L135
	.loc 2 833 0 is_stmt 1
	ldr	r3, .L136+36
	ldr	r4, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r3, r0
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r2, #2
	str	r2, [sp, #4]
	mov	r2, #0
	str	r2, [sp, #8]
	ldr	r0, [fp, #-16]
	mov	r1, r4
	mov	r2, #2
	bl	nm_LIGHTS_store_changes
	.loc 2 837 0
	ldr	r0, [fp, #-16]
	ldr	r1, .L136+4
	ldr	r2, .L136+40
	bl	mem_free_debug
	.loc 2 841 0
	ldr	r3, .L136
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 845 0
	ldr	r3, .L136+36
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 2 847 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L137:
	.align	2
.L136:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	801
	.word	805
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	GuiVar_GroupName
	.word	g_LIGHTS_numeric_date
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.word	g_GROUP_creating_new
	.word	837
.LFE22:
	.size	LIGHTS_extract_and_store_changes_from_GuiVars, .-LIGHTS_extract_and_store_changes_from_GuiVars
	.section	.text.LIGHTS_load_group_name_into_guivar,"ax",%progbits
	.align	2
	.global	LIGHTS_load_group_name_into_guivar
	.type	LIGHTS_load_group_name_into_guivar, %function
LIGHTS_load_group_name_into_guivar:
.LFB23:
	.loc 2 867 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI69:
	add	fp, sp, #4
.LCFI70:
	sub	sp, sp, #8
.LCFI71:
	mov	r3, r0
	strh	r3, [fp, #-12]	@ movhi
	.loc 2 870 0
	ldr	r3, .L141
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L141+4
	ldr	r3, .L141+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 872 0
	ldrsh	r3, [fp, #-12]
	mov	r0, r3
	bl	LIGHTS_get_light_at_this_index
	str	r0, [fp, #-8]
	.loc 2 874 0
	ldr	r0, .L141+12
	ldr	r1, [fp, #-8]
	bl	nm_OnList
	mov	r3, r0
	cmp	r3, #1
	bne	.L139
	.loc 2 876 0
	ldr	r0, [fp, #-8]
	bl	nm_GROUP_get_name
	mov	r3, r0
	ldr	r0, .L141+16
	mov	r1, r3
	mov	r2, #48
	bl	strlcpy
	b	.L140
.L139:
	.loc 2 880 0
	ldr	r0, .L141+4
	mov	r1, #880
	bl	Alert_group_not_found_with_filename
.L140:
	.loc 2 883 0
	ldr	r3, .L141
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 885 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L142:
	.align	2
.L141:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	870
	.word	light_list_hdr
	.word	GuiVar_itmGroupName
.LFE23:
	.size	LIGHTS_load_group_name_into_guivar, .-LIGHTS_load_group_name_into_guivar
	.section	.text.LIGHTS_get_index_using_ptr_to_light_struct,"ax",%progbits
	.align	2
	.global	LIGHTS_get_index_using_ptr_to_light_struct
	.type	LIGHTS_get_index_using_ptr_to_light_struct, %function
LIGHTS_get_index_using_ptr_to_light_struct:
.LFB24:
	.loc 2 889 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI72:
	add	fp, sp, #4
.LCFI73:
	sub	sp, sp, #16
.LCFI74:
	str	r0, [fp, #-20]
	.loc 2 896 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 2 898 0
	ldr	r3, .L148
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L148+4
	ldr	r3, .L148+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 900 0
	ldr	r0, .L148+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 902 0
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L144
.L147:
	.loc 2 904 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L145
	.loc 2 906 0
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-12]
	.loc 2 908 0
	b	.L146
.L145:
	.loc 2 911 0
	ldr	r0, .L148+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
	.loc 2 902 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L144:
	.loc 2 902 0 is_stmt 0 discriminator 1
	ldr	r3, .L148+12
	ldr	r2, [r3, #8]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bhi	.L147
.L146:
	.loc 2 914 0 is_stmt 1
	ldr	r3, .L148
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 916 0
	ldr	r3, [fp, #-12]
	.loc 2 917 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L149:
	.align	2
.L148:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	898
	.word	light_list_hdr
.LFE24:
	.size	LIGHTS_get_index_using_ptr_to_light_struct, .-LIGHTS_get_index_using_ptr_to_light_struct
	.section	.text.nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	.type	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index, %function
nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index:
.LFB25:
	.loc 2 943 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI75:
	add	fp, sp, #4
.LCFI76:
	sub	sp, sp, #12
.LCFI77:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 2 946 0
	ldr	r3, .L156
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L156+4
	ldr	r3, .L156+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 948 0
	ldr	r0, .L156+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 950 0
	b	.L151
.L154:
	.loc 2 952 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #72]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bne	.L152
	.loc 2 952 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	beq	.L155
.L152:
	.loc 2 957 0 is_stmt 1
	ldr	r0, .L156+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L151:
	.loc 2 950 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L154
	b	.L153
.L155:
	.loc 2 954 0
	mov	r0, r0	@ nop
.L153:
	.loc 2 960 0
	ldr	r3, .L156
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 962 0
	ldr	r3, [fp, #-8]
	.loc 2 964 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L157:
	.align	2
.L156:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	946
	.word	light_list_hdr
.LFE25:
	.size	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index, .-nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	.section	.text.LIGHTS_get_light_at_this_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_light_at_this_index
	.type	LIGHTS_get_light_at_this_index, %function
LIGHTS_get_light_at_this_index:
.LFB26:
	.loc 2 990 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI78:
	add	fp, sp, #4
.LCFI79:
	sub	sp, sp, #8
.LCFI80:
	str	r0, [fp, #-12]
	.loc 2 993 0
	ldr	r3, .L159
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L159+4
	ldr	r3, .L159+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 995 0
	ldr	r0, .L159+12
	ldr	r1, [fp, #-12]
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	str	r0, [fp, #-8]
	.loc 2 997 0
	ldr	r3, .L159
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 999 0
	ldr	r3, [fp, #-8]
	.loc 2 1000 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L160:
	.align	2
.L159:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	993
	.word	light_list_hdr
.LFE26:
	.size	LIGHTS_get_light_at_this_index, .-LIGHTS_get_light_at_this_index
	.section	.text.LIGHTS_get_list_count,"ax",%progbits
	.align	2
	.global	LIGHTS_get_list_count
	.type	LIGHTS_get_list_count, %function
LIGHTS_get_list_count:
.LFB27:
	.loc 2 1025 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI81:
	add	fp, sp, #4
.LCFI82:
	sub	sp, sp, #4
.LCFI83:
	.loc 2 1028 0
	ldr	r3, .L162
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L162+4
	ldr	r3, .L162+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1030 0
	ldr	r3, .L162+12
	ldr	r3, [r3, #8]
	str	r3, [fp, #-8]
	.loc 2 1032 0
	ldr	r3, .L162
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1034 0
	ldr	r3, [fp, #-8]
	.loc 2 1035 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L163:
	.align	2
.L162:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1028
	.word	light_list_hdr
.LFE27:
	.size	LIGHTS_get_list_count, .-LIGHTS_get_list_count
	.section .rodata
	.align	2
.LC25:
	.ascii	"Box Index\000"
	.align	2
.LC26:
	.ascii	"Output Index\000"
	.section	.text.LIGHTS_get_lights_array_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_lights_array_index
	.type	LIGHTS_get_lights_array_index, %function
LIGHTS_get_lights_array_index:
.LFB28:
	.loc 2 1055 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI84:
	add	fp, sp, #4
.LCFI85:
	sub	sp, sp, #24
.LCFI86:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 2 1057 0
	sub	r3, fp, #8
	mov	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L165
	str	r2, [sp, #4]
	ldr	r2, .L165+4
	str	r2, [sp, #8]
	ldr	r2, .L165+8
	str	r2, [sp, #12]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #11
	mov	r3, #0
	bl	RC_uint32_with_filename
	.loc 2 1058 0
	sub	r3, fp, #12
	mov	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L165+12
	str	r2, [sp, #4]
	ldr	r2, .L165+4
	str	r2, [sp, #8]
	ldr	r2, .L165+16
	str	r2, [sp, #12]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #3
	mov	r3, #0
	bl	RC_uint32_with_filename
	.loc 2 1061 0
	ldr	r3, [fp, #-8]
	mov	r2, r3, asl #2
	ldr	r3, [fp, #-12]
	add	r3, r2, r3
	.loc 2 1063 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L166:
	.align	2
.L165:
	.word	.LC25
	.word	.LC23
	.word	1057
	.word	.LC26
	.word	1058
.LFE28:
	.size	LIGHTS_get_lights_array_index, .-LIGHTS_get_lights_array_index
	.section	.text.LIGHTS_get_box_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_box_index
	.type	LIGHTS_get_box_index, %function
LIGHTS_get_box_index:
.LFB29:
	.loc 2 1079 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI87:
	add	fp, sp, #8
.LCFI88:
	sub	sp, sp, #24
.LCFI89:
	str	r0, [fp, #-16]
	.loc 2 1082 0
	ldr	r3, .L168
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L168+4
	ldr	r3, .L168+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1084 0
	ldr	r3, [fp, #-16]
	add	r4, r3, #72
	bl	FLOWSENSE_get_controller_letter
	mov	r1, r0
	ldr	r3, [fp, #-16]
	add	r2, r3, #308
	.loc 2 1091 0
	ldr	r3, .L168+12
	ldr	r3, [r3, #4]
	.loc 2 1084 0
	str	r1, [sp, #0]
	ldr	r1, .L168+16
	str	r1, [sp, #4]
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	ldr	r0, [fp, #-16]
	mov	r1, r4
	mov	r2, #0
	mov	r3, #11
	bl	SHARED_get_uint32_32_bit_change_bits_group
	str	r0, [fp, #-12]
	.loc 2 1093 0
	ldr	r3, .L168
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1095 0
	ldr	r3, [fp, #-12]
	.loc 2 1096 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L169:
	.align	2
.L168:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1082
	.word	LIGHTS_database_field_names
	.word	nm_LIGHTS_set_box_index
.LFE29:
	.size	LIGHTS_get_box_index, .-LIGHTS_get_box_index
	.section	.text.LIGHTS_get_output_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_output_index
	.type	LIGHTS_get_output_index, %function
LIGHTS_get_output_index:
.LFB30:
	.loc 2 1112 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI90:
	add	fp, sp, #8
.LCFI91:
	sub	sp, sp, #24
.LCFI92:
	str	r0, [fp, #-16]
	.loc 2 1115 0
	ldr	r3, .L171
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L171+4
	ldr	r3, .L171+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1117 0
	ldr	r3, [fp, #-16]
	add	r4, r3, #76
	bl	FLOWSENSE_get_controller_letter
	mov	r1, r0
	ldr	r3, [fp, #-16]
	add	r2, r3, #308
	.loc 2 1124 0
	ldr	r3, .L171+12
	ldr	r3, [r3, #8]
	.loc 2 1117 0
	str	r1, [sp, #0]
	ldr	r1, .L171+16
	str	r1, [sp, #4]
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	ldr	r0, [fp, #-16]
	mov	r1, r4
	mov	r2, #0
	mov	r3, #3
	bl	SHARED_get_uint32_32_bit_change_bits_group
	str	r0, [fp, #-12]
	.loc 2 1126 0
	ldr	r3, .L171
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1128 0
	ldr	r3, [fp, #-12]
	.loc 2 1129 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L172:
	.align	2
.L171:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1115
	.word	LIGHTS_database_field_names
	.word	nm_LIGHTS_set_box_index
.LFE30:
	.size	LIGHTS_get_output_index, .-LIGHTS_get_output_index
	.section	.text.LIGHTS_get_physically_available,"ax",%progbits
	.align	2
	.global	LIGHTS_get_physically_available
	.type	LIGHTS_get_physically_available, %function
LIGHTS_get_physically_available:
.LFB31:
	.loc 2 1153 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI93:
	add	fp, sp, #4
.LCFI94:
	sub	sp, sp, #16
.LCFI95:
	str	r0, [fp, #-12]
	.loc 2 1156 0
	ldr	r3, .L174
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L174+4
	ldr	r3, .L174+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1158 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #80
	ldr	r2, [fp, #-12]
	add	r1, r2, #308
	.loc 2 1163 0
	ldr	r2, .L174+12
	ldr	r2, [r2, #12]
	.loc 2 1158 0
	str	r1, [sp, #0]
	str	r2, [sp, #4]
	ldr	r0, [fp, #-12]
	mov	r1, r3
	mov	r2, #0
	ldr	r3, .L174+16
	bl	SHARED_get_bool_32_bit_change_bits_group
	str	r0, [fp, #-8]
	.loc 2 1165 0
	ldr	r3, .L174
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1167 0
	ldr	r3, [fp, #-8]
	.loc 2 1168 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L175:
	.align	2
.L174:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1156
	.word	LIGHTS_database_field_names
	.word	nm_LIGHTS_set_physically_available
.LFE31:
	.size	LIGHTS_get_physically_available, .-LIGHTS_get_physically_available
	.section	.text.LIGHTS_get_start_time_1,"ax",%progbits
	.align	2
	.global	LIGHTS_get_start_time_1
	.type	LIGHTS_get_start_time_1, %function
LIGHTS_get_start_time_1:
.LFB32:
	.loc 2 1186 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI96:
	add	fp, sp, #4
.LCFI97:
	sub	sp, sp, #36
.LCFI98:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 2 1193 0
	ldr	r3, .L177
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L177+4
	ldr	r3, .L177+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1195 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #4
	add	r3, r3, #84
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	ldr	r2, [fp, #-12]
	add	r1, r2, #308
	.loc 2 1204 0
	ldr	r2, [fp, #-16]
	add	r0, r2, #4
	ldr	r2, .L177+12
	ldr	r2, [r2, r0, asl #2]
	.loc 2 1195 0
	mov	r0, #0
	str	r0, [sp, #0]
	ldr	r0, .L177+16
	str	r0, [sp, #4]
	ldr	r0, .L177+16
	str	r0, [sp, #8]
	ldr	r0, .L177+20
	str	r0, [sp, #12]
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	mov	r2, r3
	mov	r3, #14
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	str	r0, [fp, #-8]
	.loc 2 1206 0
	ldr	r3, .L177
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1208 0
	ldr	r3, [fp, #-8]
	.loc 2 1209 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L178:
	.align	2
.L177:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1193
	.word	LIGHTS_database_field_names
	.word	86400
	.word	nm_LIGHTS_set_start_time_1
.LFE32:
	.size	LIGHTS_get_start_time_1, .-LIGHTS_get_start_time_1
	.section	.text.LIGHTS_get_start_time_2,"ax",%progbits
	.align	2
	.global	LIGHTS_get_start_time_2
	.type	LIGHTS_get_start_time_2, %function
LIGHTS_get_start_time_2:
.LFB33:
	.loc 2 1227 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI99:
	add	fp, sp, #4
.LCFI100:
	sub	sp, sp, #36
.LCFI101:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 2 1230 0
	ldr	r3, .L180
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L180+4
	ldr	r3, .L180+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1232 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #4
	add	r3, r3, #88
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	ldr	r2, [fp, #-12]
	add	r1, r2, #308
	.loc 2 1241 0
	ldr	r2, [fp, #-16]
	add	r0, r2, #4
	ldr	r2, .L180+12
	ldr	r2, [r2, r0, asl #2]
	.loc 2 1232 0
	mov	r0, #0
	str	r0, [sp, #0]
	ldr	r0, .L180+16
	str	r0, [sp, #4]
	ldr	r0, .L180+16
	str	r0, [sp, #8]
	ldr	r0, .L180+20
	str	r0, [sp, #12]
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	mov	r2, r3
	mov	r3, #14
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	str	r0, [fp, #-8]
	.loc 2 1243 0
	ldr	r3, .L180
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1245 0
	ldr	r3, [fp, #-8]
	.loc 2 1246 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L181:
	.align	2
.L180:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1230
	.word	LIGHTS_database_field_names
	.word	86400
	.word	nm_LIGHTS_set_start_time_2
.LFE33:
	.size	LIGHTS_get_start_time_2, .-LIGHTS_get_start_time_2
	.section	.text.LIGHTS_get_stop_time_1,"ax",%progbits
	.align	2
	.global	LIGHTS_get_stop_time_1
	.type	LIGHTS_get_stop_time_1, %function
LIGHTS_get_stop_time_1:
.LFB34:
	.loc 2 1264 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI102:
	add	fp, sp, #4
.LCFI103:
	sub	sp, sp, #36
.LCFI104:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 2 1267 0
	ldr	r3, .L183
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L183+4
	ldr	r3, .L183+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1269 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #4
	add	r3, r3, #92
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	ldr	r2, [fp, #-12]
	add	r1, r2, #308
	.loc 2 1278 0
	ldr	r2, [fp, #-16]
	add	r0, r2, #4
	ldr	r2, .L183+12
	ldr	r2, [r2, r0, asl #2]
	.loc 2 1269 0
	mov	r0, #0
	str	r0, [sp, #0]
	ldr	r0, .L183+16
	str	r0, [sp, #4]
	ldr	r0, .L183+16
	str	r0, [sp, #8]
	ldr	r0, .L183+20
	str	r0, [sp, #12]
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	mov	r2, r3
	mov	r3, #14
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	str	r0, [fp, #-8]
	.loc 2 1280 0
	ldr	r3, .L183
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1282 0
	ldr	r3, [fp, #-8]
	.loc 2 1283 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L184:
	.align	2
.L183:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1267
	.word	LIGHTS_database_field_names
	.word	86400
	.word	nm_LIGHTS_set_stop_time_1
.LFE34:
	.size	LIGHTS_get_stop_time_1, .-LIGHTS_get_stop_time_1
	.section	.text.LIGHTS_get_stop_time_2,"ax",%progbits
	.align	2
	.global	LIGHTS_get_stop_time_2
	.type	LIGHTS_get_stop_time_2, %function
LIGHTS_get_stop_time_2:
.LFB35:
	.loc 2 1301 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI105:
	add	fp, sp, #4
.LCFI106:
	sub	sp, sp, #36
.LCFI107:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 2 1304 0
	ldr	r3, .L186
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L186+4
	ldr	r3, .L186+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1306 0
	ldr	r3, [fp, #-16]
	add	r3, r3, #6
	mov	r3, r3, asl #4
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	ldr	r2, [fp, #-12]
	add	r1, r2, #308
	.loc 2 1315 0
	ldr	r2, [fp, #-16]
	add	r0, r2, #4
	ldr	r2, .L186+12
	ldr	r2, [r2, r0, asl #2]
	.loc 2 1306 0
	mov	r0, #0
	str	r0, [sp, #0]
	ldr	r0, .L186+16
	str	r0, [sp, #4]
	ldr	r0, .L186+16
	str	r0, [sp, #8]
	ldr	r0, .L186+20
	str	r0, [sp, #12]
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	mov	r2, r3
	mov	r3, #14
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	str	r0, [fp, #-8]
	.loc 2 1317 0
	ldr	r3, .L186
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1319 0
	ldr	r3, [fp, #-8]
	.loc 2 1320 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L187:
	.align	2
.L186:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1304
	.word	LIGHTS_database_field_names
	.word	86400
	.word	nm_LIGHTS_set_stop_time_2
.LFE35:
	.size	LIGHTS_get_stop_time_2, .-LIGHTS_get_stop_time_2
	.section	.text.LIGHTS_get_day_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_day_index
	.type	LIGHTS_get_day_index, %function
LIGHTS_get_day_index:
.LFB36:
	.loc 2 1340 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI108:
	add	fp, sp, #0
.LCFI109:
	sub	sp, sp, #4
.LCFI110:
	str	r0, [fp, #-4]
	.loc 2 1342 0
	ldr	r3, [fp, #-4]
	sub	r1, r3, #40704
	sub	r1, r1, #203
	mov	r2, r1, lsr #1
	ldr	r3, .L189
	umull	r0, r3, r2, r3
	mov	r2, r3, lsr #2
	mov	r3, r2
	mov	r3, r3, asl #3
	rsb	r3, r2, r3
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	mov	r3, r2
	.loc 2 1344 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L190:
	.align	2
.L189:
	.word	-1840700269
.LFE36:
	.size	LIGHTS_get_day_index, .-LIGHTS_get_day_index
	.section	.text.LIGHTS_has_a_stop_time,"ax",%progbits
	.align	2
	.global	LIGHTS_has_a_stop_time
	.type	LIGHTS_has_a_stop_time, %function
LIGHTS_has_a_stop_time:
.LFB37:
	.loc 2 1348 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI111:
	add	fp, sp, #4
.LCFI112:
	sub	sp, sp, #12
.LCFI113:
	str	r0, [fp, #-16]
	.loc 2 1355 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 2 1359 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L192
.L196:
	.loc 2 1361 0
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-12]
	bl	LIGHTS_get_stop_time_1
	mov	r2, r0
	ldr	r3, .L197
	cmp	r2, r3
	bne	.L193
	.loc 2 1361 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-16]
	ldr	r1, [fp, #-12]
	bl	LIGHTS_get_stop_time_2
	mov	r2, r0
	ldr	r3, .L197
	cmp	r2, r3
	beq	.L194
.L193:
	.loc 2 1363 0 is_stmt 1
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 2 1366 0
	b	.L195
.L194:
	.loc 2 1359 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L192:
	.loc 2 1359 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #13
	bls	.L196
.L195:
	.loc 2 1372 0 is_stmt 1
	ldr	r3, [fp, #-8]
	.loc 2 1373 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L198:
	.align	2
.L197:
	.word	86400
.LFE37:
	.size	LIGHTS_has_a_stop_time, .-LIGHTS_has_a_stop_time
	.section	.text.LIGHTS_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	LIGHTS_get_change_bits_ptr
	.type	LIGHTS_get_change_bits_ptr, %function
LIGHTS_get_change_bits_ptr:
.LFB38:
	.loc 2 1377 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI114:
	add	fp, sp, #4
.LCFI115:
	sub	sp, sp, #8
.LCFI116:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 2 1378 0
	ldr	r3, [fp, #-8]
	add	r1, r3, #308
	ldr	r3, [fp, #-8]
	add	r2, r3, #312
	ldr	r3, [fp, #-8]
	add	r3, r3, #316
	ldr	r0, [fp, #-12]
	bl	SHARED_get_32_bit_change_bits_ptr
	mov	r3, r0
	.loc 2 1379 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE38:
	.size	LIGHTS_get_change_bits_ptr, .-LIGHTS_get_change_bits_ptr
	.section .rodata
	.align	2
.LC27:
	.ascii	"No lights available\000"
	.section	.text.nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars
	.type	nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars, %function
nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars:
.LFB39:
	.loc 2 1405 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI117:
	add	fp, sp, #4
.LCFI118:
	sub	sp, sp, #4
.LCFI119:
	.loc 2 1408 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 2 1412 0
	ldr	r3, .L206
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L206+4
	ldr	r3, .L206+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1414 0
	ldr	r3, .L206+12
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L201
	.loc 2 1419 0
	ldr	r3, .L206+16
	ldr	r2, [r3, #0]
	ldr	r3, .L206+20
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	str	r0, [fp, #-8]
	.loc 2 1424 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L202
	.loc 2 1424 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-8]
	bl	LIGHTS_get_physically_available
	mov	r3, r0
	cmp	r3, #0
	bne	.L203
.L202:
	.loc 2 1426 0 is_stmt 1
	ldr	r0, .L206+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 1428 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L203
	.loc 2 1428 0 is_stmt 0 discriminator 1
	ldr	r0, [fp, #-8]
	bl	LIGHTS_get_physically_available
	mov	r3, r0
	cmp	r3, #0
	bne	.L203
.L204:
	.loc 2 1432 0 is_stmt 1 discriminator 1
	ldr	r0, .L206+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
	.loc 2 1434 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L203
	ldr	r0, [fp, #-8]
	bl	LIGHTS_get_physically_available
	mov	r3, r0
	cmp	r3, #0
	beq	.L204
.L203:
	.loc 2 1438 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L205
	.loc 2 1441 0
	ldr	r0, [fp, #-8]
	bl	LIGHTS_get_box_index
	mov	r2, r0
	ldr	r3, .L206+16
	str	r2, [r3, #0]
	.loc 2 1442 0
	ldr	r0, [fp, #-8]
	bl	LIGHTS_get_output_index
	mov	r2, r0
	ldr	r3, .L206+20
	str	r2, [r3, #0]
	b	.L201
.L205:
	.loc 2 1448 0
	ldr	r0, .L206+24
	bl	Alert_Message
.L201:
	.loc 2 1452 0
	ldr	r3, .L206
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1454 0
	ldr	r3, [fp, #-8]
	.loc 2 1456 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L207:
	.align	2
.L206:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1412
	.word	light_list_hdr
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	.LC27
.LFE39:
	.size	nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars, .-nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars
	.section	.text.nm_LIGHTS_get_next_available_light,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_get_next_available_light
	.type	nm_LIGHTS_get_next_available_light, %function
nm_LIGHTS_get_next_available_light:
.LFB40:
	.loc 2 1484 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI120:
	add	fp, sp, #4
.LCFI121:
	sub	sp, sp, #12
.LCFI122:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 2 1487 0
	ldr	r3, .L216
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L216+4
	ldr	r3, .L216+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1492 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	str	r0, [fp, #-8]
	.loc 2 1494 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L209
.L213:
	.loc 2 1502 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	mov	r0, r3
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
	.loc 2 1504 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L210
	.loc 2 1507 0
	ldr	r0, .L216+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
.L210:
	.loc 2 1510 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #72]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L211
	.loc 2 1510 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L215
.L211:
	.loc 2 1517 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #80]
	cmp	r3, #0
	beq	.L213
	b	.L212
.L215:
	.loc 2 1514 0
	mov	r0, r0	@ nop
.L212:
	.loc 2 1520 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #72]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 2 1522 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	b	.L214
.L209:
	.loc 2 1526 0
	ldr	r0, .L216+4
	ldr	r1, .L216+16
	bl	Alert_group_not_found_with_filename
.L214:
	.loc 2 1529 0
	ldr	r3, .L216
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1531 0
	ldr	r3, [fp, #-8]
	.loc 2 1533 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L217:
	.align	2
.L216:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1487
	.word	light_list_hdr
	.word	1526
.LFE40:
	.size	nm_LIGHTS_get_next_available_light, .-nm_LIGHTS_get_next_available_light
	.section	.text.nm_LIGHTS_get_prev_available_light,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_get_prev_available_light
	.type	nm_LIGHTS_get_prev_available_light, %function
nm_LIGHTS_get_prev_available_light:
.LFB41:
	.loc 2 1561 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI123:
	add	fp, sp, #4
.LCFI124:
	sub	sp, sp, #12
.LCFI125:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 2 1564 0
	ldr	r3, .L226
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L226+4
	ldr	r3, .L226+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1569 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	str	r0, [fp, #-8]
	.loc 2 1571 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L219
.L222:
	.loc 2 1579 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #8]
	mov	r0, r3
	ldr	r1, [fp, #-8]
	bl	nm_ListGetPrev
	str	r0, [fp, #-8]
	.loc 2 1581 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L220
	.loc 2 1584 0
	ldr	r0, .L226+12
	bl	nm_ListGetLast
	str	r0, [fp, #-8]
.L220:
	.loc 2 1587 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #72]
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L221
	.loc 2 1587 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L225
.L221:
	.loc 2 1594 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #80]
	cmp	r3, #0
	beq	.L222
	b	.L219
.L225:
	.loc 2 1591 0
	mov	r0, r0	@ nop
.L219:
	.loc 2 1598 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L223
	.loc 2 1600 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #72]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 2 1602 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	b	.L224
.L223:
	.loc 2 1606 0
	ldr	r0, .L226+4
	ldr	r1, .L226+16
	bl	Alert_group_not_found_with_filename
.L224:
	.loc 2 1609 0
	ldr	r3, .L226
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1611 0
	ldr	r3, [fp, #-8]
	.loc 2 1613 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L227:
	.align	2
.L226:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1564
	.word	light_list_hdr
	.word	1606
.LFE41:
	.size	nm_LIGHTS_get_prev_available_light, .-nm_LIGHTS_get_prev_available_light
	.section	.text.LIGHTS_clean_house_processing,"ax",%progbits
	.align	2
	.global	LIGHTS_clean_house_processing
	.type	LIGHTS_clean_house_processing, %function
LIGHTS_clean_house_processing:
.LFB42:
	.loc 2 1617 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI126:
	add	fp, sp, #4
.LCFI127:
	sub	sp, sp, #16
.LCFI128:
	.loc 2 1630 0
	ldr	r3, .L231
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L231+4
	ldr	r3, .L231+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1632 0
	ldr	r0, .L231+12
	ldr	r1, .L231+4
	mov	r2, #1632
	bl	nm_ListRemoveHead_debug
	str	r0, [fp, #-8]
	.loc 2 1634 0
	b	.L229
.L230:
	.loc 2 1636 0
	ldr	r0, [fp, #-8]
	ldr	r1, .L231+4
	ldr	r2, .L231+16
	bl	mem_free_debug
	.loc 2 1638 0
	ldr	r0, .L231+12
	ldr	r1, .L231+4
	ldr	r2, .L231+20
	bl	nm_ListRemoveHead_debug
	str	r0, [fp, #-8]
.L229:
	.loc 2 1634 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L230
	.loc 2 1641 0
	ldr	r3, .L231
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1648 0
	ldr	r3, .L231
	ldr	r3, [r3, #0]
	mov	r2, #324
	str	r2, [sp, #0]
	str	r3, [sp, #4]
	mov	r3, #16
	str	r3, [sp, #8]
	mov	r0, #1
	ldr	r1, .L231+24
	mov	r2, #0
	ldr	r3, .L231+12
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	.loc 2 1650 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L232:
	.align	2
.L231:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1630
	.word	light_list_hdr
	.word	1636
	.word	1638
	.word	LIGHTS_FILENAME
.LFE42:
	.size	LIGHTS_clean_house_processing, .-LIGHTS_clean_house_processing
	.section	.text.LIGHTS_set_not_physically_available_based_upon_communication_scan_results,"ax",%progbits
	.align	2
	.global	LIGHTS_set_not_physically_available_based_upon_communication_scan_results
	.type	LIGHTS_set_not_physically_available_based_upon_communication_scan_results, %function
LIGHTS_set_not_physically_available_based_upon_communication_scan_results:
.LFB43:
	.loc 2 1654 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI129:
	add	fp, sp, #4
.LCFI130:
	sub	sp, sp, #24
.LCFI131:
	.loc 2 1669 0
	ldr	r3, .L239
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L239+4
	ldr	r3, .L239+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1675 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L234
.L238:
	.loc 2 1677 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L235
.L237:
	.loc 2 1681 0
	ldr	r1, .L239+12
	ldr	r2, [fp, #-8]
	mov	r3, #16
	mov	r0, #92
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L236
	.loc 2 1683 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	str	r0, [fp, #-16]
	.loc 2 1687 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L236
	.loc 2 1689 0
	ldr	r0, [fp, #-16]
	mov	r1, #10
	bl	LIGHTS_get_change_bits_ptr
	mov	r3, r0
	ldr	r2, [fp, #-8]
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, [fp, #-16]
	mov	r1, #0
	mov	r2, #1
	mov	r3, #10
	bl	nm_LIGHTS_set_physically_available
.L236:
	.loc 2 1677 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L235:
	.loc 2 1677 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #3
	bls	.L237
	.loc 2 1675 0 is_stmt 1
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L234:
	.loc 2 1675 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L238
	.loc 2 1696 0 is_stmt 1
	ldr	r3, .L239
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1698 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L240:
	.align	2
.L239:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1669
	.word	chain
.LFE43:
	.size	LIGHTS_set_not_physically_available_based_upon_communication_scan_results, .-LIGHTS_set_not_physically_available_based_upon_communication_scan_results
	.section	.text.LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token
	.type	LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token, %function
LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token:
.LFB44:
	.loc 2 1702 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI132:
	add	fp, sp, #4
.LCFI133:
	sub	sp, sp, #4
.LCFI134:
	.loc 2 1707 0
	ldr	r3, .L244
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L244+4
	ldr	r3, .L244+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1709 0
	ldr	r0, .L244+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 1711 0
	b	.L242
.L243:
	.loc 2 1716 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #312
	ldr	r3, .L244
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_set_all_32_bit_change_bits
	.loc 2 1718 0
	ldr	r0, .L244+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L242:
	.loc 2 1711 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L243
	.loc 2 1721 0
	ldr	r3, .L244
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1729 0
	ldr	r3, .L244+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L244+4
	ldr	r3, .L244+20
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1731 0
	ldr	r3, .L244+24
	mov	r2, #1
	str	r2, [r3, #444]
	.loc 2 1733 0
	ldr	r3, .L244+16
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1735 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L245:
	.align	2
.L244:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1707
	.word	light_list_hdr
	.word	comm_mngr_recursive_MUTEX
	.word	1729
	.word	comm_mngr
.LFE44:
	.size	LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token, .-LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token
	.section	.text.LIGHTS_on_all_lights_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	LIGHTS_on_all_lights_set_or_clear_commserver_change_bits
	.type	LIGHTS_on_all_lights_set_or_clear_commserver_change_bits, %function
LIGHTS_on_all_lights_set_or_clear_commserver_change_bits:
.LFB45:
	.loc 2 1739 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI135:
	add	fp, sp, #4
.LCFI136:
	sub	sp, sp, #8
.LCFI137:
	str	r0, [fp, #-12]
	.loc 2 1742 0
	ldr	r3, .L251
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L251+4
	ldr	r3, .L251+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1744 0
	ldr	r0, .L251+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 1746 0
	b	.L247
.L250:
	.loc 2 1748 0
	ldr	r3, [fp, #-12]
	cmp	r3, #51
	bne	.L248
	.loc 2 1750 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #316
	ldr	r3, .L251
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L249
.L248:
	.loc 2 1754 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #316
	ldr	r3, .L251
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_set_all_32_bit_change_bits
.L249:
	.loc 2 1757 0
	ldr	r0, .L251+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L247:
	.loc 2 1746 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L250
	.loc 2 1760 0
	ldr	r3, .L251
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1762 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L252:
	.align	2
.L251:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1742
	.word	light_list_hdr
.LFE45:
	.size	LIGHTS_on_all_lights_set_or_clear_commserver_change_bits, .-LIGHTS_on_all_lights_set_or_clear_commserver_change_bits
	.section	.text.nm_LIGHTS_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_update_pending_change_bits
	.type	nm_LIGHTS_update_pending_change_bits, %function
nm_LIGHTS_update_pending_change_bits:
.LFB46:
	.loc 2 1773 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI138:
	add	fp, sp, #4
.LCFI139:
	sub	sp, sp, #16
.LCFI140:
	str	r0, [fp, #-16]
	.loc 2 1778 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 2 1780 0
	ldr	r3, .L260
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L260+4
	ldr	r3, .L260+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1782 0
	ldr	r0, .L260+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-8]
	.loc 2 1784 0
	b	.L254
.L258:
	.loc 2 1788 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #320]
	cmp	r3, #0
	beq	.L255
	.loc 2 1793 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L256
	.loc 2 1795 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #316]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #320]
	orr	r2, r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #316]
	.loc 2 1799 0
	ldr	r3, .L260+16
	mov	r2, #1
	str	r2, [r3, #112]
	.loc 2 1807 0
	ldr	r3, .L260+20
	ldr	r3, [r3, #152]
	mvn	r2, #0
	str	r2, [sp, #0]
	mov	r0, r3
	mov	r1, #2
	ldr	r2, .L260+24
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L257
.L256:
	.loc 2 1811 0
	ldr	r3, [fp, #-8]
	add	r2, r3, #320
	ldr	r3, .L260
	ldr	r3, [r3, #0]
	mov	r0, r2
	mov	r1, r3
	bl	SHARED_clear_all_32_bit_change_bits
.L257:
	.loc 2 1815 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L255:
	.loc 2 1818 0
	ldr	r0, .L260+12
	ldr	r1, [fp, #-8]
	bl	nm_ListGetNext
	str	r0, [fp, #-8]
.L254:
	.loc 2 1784 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	bne	.L258
	.loc 2 1821 0
	ldr	r3, .L260
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1823 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L253
	.loc 2 1829 0
	mov	r0, #16
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L253:
	.loc 2 1832 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L261:
	.align	2
.L260:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1780
	.word	light_list_hdr
	.word	weather_preserves
	.word	cics
	.word	60000
.LFE46:
	.size	nm_LIGHTS_update_pending_change_bits, .-nm_LIGHTS_update_pending_change_bits
	.section .rodata
	.align	2
.LC28:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.text.LIGHTS_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	LIGHTS_calculate_chain_sync_crc
	.type	LIGHTS_calculate_chain_sync_crc, %function
LIGHTS_calculate_chain_sync_crc:
.LFB47:
	.loc 2 1836 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI141:
	add	fp, sp, #8
.LCFI142:
	sub	sp, sp, #24
.LCFI143:
	str	r0, [fp, #-32]
	.loc 2 1854 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 2 1858 0
	ldr	r3, .L267
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L267+4
	ldr	r3, .L267+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 2 1870 0
	ldr	r3, .L267+12
	ldr	r3, [r3, #8]
	mov	r2, #324
	mul	r2, r3, r2
	sub	r3, fp, #24
	mov	r0, r2
	mov	r1, r3
	ldr	r2, .L267+4
	ldr	r3, .L267+16
	bl	mem_oabia
	mov	r3, r0
	cmp	r3, #0
	beq	.L263
	.loc 2 1874 0
	mov	r3, #1
	str	r3, [fp, #-20]
	.loc 2 1878 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 2 1880 0
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 2 1886 0
	ldr	r0, .L267+12
	bl	nm_ListGetFirst
	str	r0, [fp, #-12]
	.loc 2 1888 0
	b	.L264
.L265:
	.loc 2 1900 0
	ldr	r3, [fp, #-12]
	add	r4, r3, #20
	ldr	r3, [fp, #-12]
	add	r3, r3, #20
	mov	r0, r3
	bl	strlen
	mov	r3, r0
	sub	r2, fp, #28
	mov	r0, r2
	mov	r1, r4
	mov	r2, r3
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r3, r0
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 2 1902 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #72
	sub	r2, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r3, r0
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 2 1904 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #76
	sub	r2, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r3, r0
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 2 1906 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #80
	sub	r2, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #4
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r3, r0
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 2 1908 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #84
	sub	r2, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #224
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r3, r0
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 2 1916 0
	ldr	r0, .L267+12
	ldr	r1, [fp, #-12]
	bl	nm_ListGetNext
	str	r0, [fp, #-12]
.L264:
	.loc 2 1888 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L265
	.loc 2 1922 0
	ldr	r3, [fp, #-24]
	mov	r0, r3
	ldr	r1, [fp, #-16]
	bl	CRC_calculate_32bit_big_endian
	mov	r1, r0
	ldr	r3, .L267+20
	ldr	r2, [fp, #-32]
	add	r2, r2, #43
	str	r1, [r3, r2, asl #2]
	.loc 2 1927 0
	ldr	r3, [fp, #-24]
	mov	r0, r3
	ldr	r1, .L267+4
	ldr	r2, .L267+24
	bl	mem_free_debug
	b	.L266
.L263:
	.loc 2 1935 0
	ldr	r3, .L267+28
	ldr	r2, [fp, #-32]
	ldr	r3, [r3, r2, asl #4]
	ldr	r0, .L267+32
	mov	r1, r3
	bl	Alert_Message_va
.L266:
	.loc 2 1940 0
	ldr	r3, .L267
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 2 1944 0
	ldr	r3, [fp, #-20]
	.loc 2 1945 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L268:
	.align	2
.L267:
	.word	list_lights_recursive_MUTEX
	.word	.LC23
	.word	1858
	.word	light_list_hdr
	.word	1870
	.word	cscs
	.word	1927
	.word	chain_sync_file_pertinants
	.word	.LC28
.LFE47:
	.size	LIGHTS_calculate_chain_sync_crc, .-LIGHTS_calculate_chain_sync_crc
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI45-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI48-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI51-.LFB17
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI52-.LCFI51
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI54-.LFB18
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI55-.LCFI54
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI57-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI58-.LCFI57
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI60-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI61-.LCFI60
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI63-.LFB21
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI64-.LCFI63
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI66-.LFB22
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI67-.LCFI66
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI69-.LFB23
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI70-.LCFI69
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI72-.LFB24
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI73-.LCFI72
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI75-.LFB25
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI76-.LCFI75
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI78-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI79-.LCFI78
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI81-.LFB27
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI82-.LCFI81
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI84-.LFB28
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI85-.LCFI84
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI87-.LFB29
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI88-.LCFI87
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI90-.LFB30
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI91-.LCFI90
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI93-.LFB31
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI94-.LCFI93
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI96-.LFB32
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI97-.LCFI96
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI99-.LFB33
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI100-.LCFI99
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI102-.LFB34
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI103-.LCFI102
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI105-.LFB35
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI106-.LCFI105
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI108-.LFB36
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI109-.LCFI108
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI111-.LFB37
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI112-.LCFI111
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI114-.LFB38
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI115-.LCFI114
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI117-.LFB39
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI118-.LCFI117
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI120-.LFB40
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI121-.LCFI120
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI123-.LFB41
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI124-.LCFI123
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI126-.LFB42
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI127-.LCFI126
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI129-.LFB43
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI130-.LCFI129
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI132-.LFB44
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI133-.LCFI132
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.byte	0x4
	.4byte	.LCFI135-.LFB45
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI136-.LCFI135
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI138-.LFB46
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI139-.LCFI138
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI141-.LFB47
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI142-.LCFI141
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE94:
	.text
.Letext0:
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_comm_server_common.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/timers.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_tpmicro_common.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/lights.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/configuration_controller.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/foal_comm.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/comm_mngr.h"
	.file 18 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/flow_recorder.h"
	.file 19 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 20 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/controller_initiated.h"
	.file 21 "C:/CS3000/cs3_branches/chain_sync/main_app/src/flash_storage/chain_sync_vars.h"
	.file 22 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 23 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 24 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_lights.h"
	.file 25 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 26 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x268f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF407
	.byte	0x1
	.4byte	.LASF408
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x3
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x3
	.byte	0x4c
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x3
	.byte	0x55
	.4byte	0x69
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x3
	.byte	0x5e
	.4byte	0x7b
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x99
	.4byte	0x7b
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x9d
	.4byte	0x7b
	.uleb128 0x5
	.byte	0x14
	.byte	0x4
	.byte	0x18
	.4byte	0xfc
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x4
	.byte	0x1a
	.4byte	0xfc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x4
	.byte	0x1c
	.4byte	0xfc
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x4
	.byte	0x1e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x4
	.byte	0x20
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x4
	.byte	0x23
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x4
	.byte	0x26
	.4byte	0xad
	.uleb128 0x5
	.byte	0xc
	.byte	0x4
	.byte	0x2a
	.4byte	0x13c
	.uleb128 0x6
	.4byte	.LASF21
	.byte	0x4
	.byte	0x2c
	.4byte	0xfc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF22
	.byte	0x4
	.byte	0x2e
	.4byte	0xfc
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF23
	.byte	0x4
	.byte	0x30
	.4byte	0x13c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0xfe
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x4
	.byte	0x32
	.4byte	0x109
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.byte	0x4c
	.4byte	0x172
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x5
	.byte	0x55
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x5
	.byte	0x57
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF27
	.byte	0x5
	.byte	0x59
	.4byte	0x14d
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.byte	0x65
	.4byte	0x1a2
	.uleb128 0x6
	.4byte	.LASF28
	.byte	0x5
	.byte	0x67
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x5
	.byte	0x69
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x5
	.byte	0x6b
	.4byte	0x17d
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF30
	.uleb128 0x3
	.4byte	.LASF31
	.byte	0x6
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF32
	.byte	0x7
	.byte	0x57
	.4byte	0xfc
	.uleb128 0x3
	.4byte	.LASF33
	.byte	0x8
	.byte	0x4c
	.4byte	0x1bf
	.uleb128 0x3
	.4byte	.LASF34
	.byte	0x9
	.byte	0x65
	.4byte	0xfc
	.uleb128 0x9
	.4byte	0x3e
	.4byte	0x1f0
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x2c
	.uleb128 0x5
	.byte	0x48
	.byte	0xa
	.byte	0x3a
	.4byte	0x245
	.uleb128 0x6
	.4byte	.LASF35
	.byte	0xa
	.byte	0x3e
	.4byte	0x142
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF36
	.byte	0xa
	.byte	0x46
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF37
	.byte	0xa
	.byte	0x4d
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF38
	.byte	0xa
	.byte	0x50
	.4byte	0x245
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF39
	.byte	0xa
	.byte	0x5a
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x255
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2f
	.byte	0
	.uleb128 0x3
	.4byte	.LASF40
	.byte	0xa
	.byte	0x5c
	.4byte	0x1f6
	.uleb128 0xb
	.byte	0x4
	.byte	0xb
	.2byte	0x235
	.4byte	0x28e
	.uleb128 0xc
	.4byte	.LASF41
	.byte	0xb
	.2byte	0x237
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF42
	.byte	0xb
	.2byte	0x239
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xd
	.byte	0x4
	.byte	0xb
	.2byte	0x231
	.4byte	0x2a9
	.uleb128 0xe
	.4byte	.LASF87
	.byte	0xb
	.2byte	0x233
	.4byte	0x70
	.uleb128 0xf
	.4byte	0x260
	.byte	0
	.uleb128 0xb
	.byte	0x4
	.byte	0xb
	.2byte	0x22f
	.4byte	0x2bb
	.uleb128 0x10
	.4byte	0x28e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0xb
	.2byte	0x23e
	.4byte	0x2a9
	.uleb128 0xb
	.byte	0x38
	.byte	0xb
	.2byte	0x241
	.4byte	0x358
	.uleb128 0x12
	.4byte	.LASF44
	.byte	0xb
	.2byte	0x245
	.4byte	0x358
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x13
	.ascii	"poc\000"
	.byte	0xb
	.2byte	0x247
	.4byte	0x2bb
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x12
	.4byte	.LASF45
	.byte	0xb
	.2byte	0x249
	.4byte	0x2bb
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x12
	.4byte	.LASF46
	.byte	0xb
	.2byte	0x24f
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x12
	.4byte	.LASF47
	.byte	0xb
	.2byte	0x250
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x12
	.4byte	.LASF48
	.byte	0xb
	.2byte	0x252
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x12
	.4byte	.LASF49
	.byte	0xb
	.2byte	0x253
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x12
	.4byte	.LASF50
	.byte	0xb
	.2byte	0x254
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x12
	.4byte	.LASF51
	.byte	0xb
	.2byte	0x256
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x9
	.4byte	0x2bb
	.4byte	0x368
	.uleb128 0xa
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0xb
	.2byte	0x258
	.4byte	0x2c7
	.uleb128 0x5
	.byte	0x8
	.byte	0xc
	.byte	0x14
	.4byte	0x399
	.uleb128 0x6
	.4byte	.LASF53
	.byte	0xc
	.byte	0x17
	.4byte	0x399
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF54
	.byte	0xc
	.byte	0x1a
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x33
	.uleb128 0x3
	.4byte	.LASF55
	.byte	0xc
	.byte	0x1c
	.4byte	0x374
	.uleb128 0x5
	.byte	0x6
	.byte	0xd
	.byte	0x22
	.4byte	0x3cb
	.uleb128 0x14
	.ascii	"T\000"
	.byte	0xd
	.byte	0x24
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.ascii	"D\000"
	.byte	0xd
	.byte	0x26
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF56
	.byte	0xd
	.byte	0x28
	.4byte	0x3aa
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF57
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x3ed
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x3fd
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x40d
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x3
	.4byte	.LASF58
	.byte	0xe
	.byte	0x5d
	.4byte	0x418
	.uleb128 0x15
	.4byte	.LASF58
	.2byte	0x144
	.byte	0x1
	.byte	0xa5
	.4byte	0x4a8
	.uleb128 0x6
	.4byte	.LASF59
	.byte	0x1
	.byte	0xa7
	.4byte	0x255
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF60
	.byte	0x1
	.byte	0xb9
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x6
	.4byte	.LASF61
	.byte	0x1
	.byte	0xbd
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x6
	.4byte	.LASF62
	.byte	0x1
	.byte	0xc8
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x6
	.4byte	.LASF63
	.byte	0x1
	.byte	0xd2
	.4byte	0xd43
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x6
	.4byte	.LASF64
	.byte	0x1
	.byte	0xd8
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x134
	.uleb128 0x6
	.4byte	.LASF65
	.byte	0x1
	.byte	0xd9
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.uleb128 0x6
	.4byte	.LASF66
	.byte	0x1
	.byte	0xda
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c
	.uleb128 0x6
	.4byte	.LASF67
	.byte	0x1
	.byte	0xdb
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x140
	.byte	0
	.uleb128 0x5
	.byte	0x10
	.byte	0xe
	.byte	0x6b
	.4byte	0x4e9
	.uleb128 0x6
	.4byte	.LASF68
	.byte	0xe
	.byte	0x70
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF69
	.byte	0xe
	.byte	0x72
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF70
	.byte	0xe
	.byte	0x74
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF71
	.byte	0xe
	.byte	0x76
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x3
	.4byte	.LASF72
	.byte	0xe
	.byte	0x78
	.4byte	0x4a8
	.uleb128 0x5
	.byte	0x4
	.byte	0xf
	.byte	0x2f
	.4byte	0x5eb
	.uleb128 0x16
	.4byte	.LASF73
	.byte	0xf
	.byte	0x35
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF74
	.byte	0xf
	.byte	0x3e
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF75
	.byte	0xf
	.byte	0x3f
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF76
	.byte	0xf
	.byte	0x46
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF77
	.byte	0xf
	.byte	0x4e
	.4byte	0x70
	.byte	0x4
	.byte	0x2
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF78
	.byte	0xf
	.byte	0x4f
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF79
	.byte	0xf
	.byte	0x50
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF80
	.byte	0xf
	.byte	0x52
	.4byte	0x70
	.byte	0x4
	.byte	0x2
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF81
	.byte	0xf
	.byte	0x53
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF82
	.byte	0xf
	.byte	0x54
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF83
	.byte	0xf
	.byte	0x58
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF84
	.byte	0xf
	.byte	0x59
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF85
	.byte	0xf
	.byte	0x5a
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x16
	.4byte	.LASF86
	.byte	0xf
	.byte	0x5b
	.4byte	0xa2
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x17
	.byte	0x4
	.byte	0xf
	.byte	0x2b
	.4byte	0x604
	.uleb128 0x18
	.4byte	.LASF88
	.byte	0xf
	.byte	0x2d
	.4byte	0x4c
	.uleb128 0xf
	.4byte	0x4f4
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.byte	0xf
	.byte	0x29
	.4byte	0x615
	.uleb128 0x10
	.4byte	0x5eb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x3
	.4byte	.LASF89
	.byte	0xf
	.byte	0x61
	.4byte	0x604
	.uleb128 0x19
	.byte	0x1
	.uleb128 0x8
	.byte	0x4
	.4byte	0x620
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x638
	.uleb128 0xa
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x5
	.byte	0x48
	.byte	0x10
	.byte	0x3b
	.4byte	0x686
	.uleb128 0x6
	.4byte	.LASF90
	.byte	0x10
	.byte	0x44
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF91
	.byte	0x10
	.byte	0x46
	.4byte	0x615
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x14
	.ascii	"wi\000"
	.byte	0x10
	.byte	0x48
	.4byte	0x368
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF92
	.byte	0x10
	.byte	0x4c
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x6
	.4byte	.LASF93
	.byte	0x10
	.byte	0x4e
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.byte	0
	.uleb128 0x3
	.4byte	.LASF94
	.byte	0x10
	.byte	0x54
	.4byte	0x638
	.uleb128 0x5
	.byte	0x8
	.byte	0x11
	.byte	0xe7
	.4byte	0x6b6
	.uleb128 0x6
	.4byte	.LASF95
	.byte	0x11
	.byte	0xf6
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF96
	.byte	0x11
	.byte	0xfe
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x11
	.4byte	.LASF97
	.byte	0x11
	.2byte	0x100
	.4byte	0x691
	.uleb128 0xb
	.byte	0xc
	.byte	0x11
	.2byte	0x105
	.4byte	0x6e9
	.uleb128 0x13
	.ascii	"dt\000"
	.byte	0x11
	.2byte	0x107
	.4byte	0x3cb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF98
	.byte	0x11
	.2byte	0x108
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x11
	.4byte	.LASF99
	.byte	0x11
	.2byte	0x109
	.4byte	0x6c2
	.uleb128 0x1a
	.2byte	0x1e4
	.byte	0x11
	.2byte	0x10d
	.4byte	0x9b3
	.uleb128 0x12
	.4byte	.LASF100
	.byte	0x11
	.2byte	0x112
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF101
	.byte	0x11
	.2byte	0x116
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x12
	.4byte	.LASF102
	.byte	0x11
	.2byte	0x11f
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x12
	.4byte	.LASF103
	.byte	0x11
	.2byte	0x126
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x12
	.4byte	.LASF104
	.byte	0x11
	.2byte	0x12a
	.4byte	0x1d5
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x12
	.4byte	.LASF105
	.byte	0x11
	.2byte	0x12e
	.4byte	0x1d5
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x12
	.4byte	.LASF106
	.byte	0x11
	.2byte	0x133
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x12
	.4byte	.LASF107
	.byte	0x11
	.2byte	0x138
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x12
	.4byte	.LASF108
	.byte	0x11
	.2byte	0x13c
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x12
	.4byte	.LASF109
	.byte	0x11
	.2byte	0x143
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x12
	.4byte	.LASF110
	.byte	0x11
	.2byte	0x14c
	.4byte	0x9b3
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x12
	.4byte	.LASF111
	.byte	0x11
	.2byte	0x156
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x12
	.4byte	.LASF112
	.byte	0x11
	.2byte	0x158
	.4byte	0x3ed
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x12
	.4byte	.LASF113
	.byte	0x11
	.2byte	0x15a
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x12
	.4byte	.LASF114
	.byte	0x11
	.2byte	0x15c
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x12
	.4byte	.LASF115
	.byte	0x11
	.2byte	0x174
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x12
	.4byte	.LASF116
	.byte	0x11
	.2byte	0x176
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x12
	.4byte	.LASF117
	.byte	0x11
	.2byte	0x180
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x12
	.4byte	.LASF118
	.byte	0x11
	.2byte	0x182
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x12
	.4byte	.LASF119
	.byte	0x11
	.2byte	0x186
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x12
	.4byte	.LASF120
	.byte	0x11
	.2byte	0x195
	.4byte	0x3ed
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x12
	.4byte	.LASF121
	.byte	0x11
	.2byte	0x197
	.4byte	0x3ed
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0x12
	.4byte	.LASF122
	.byte	0x11
	.2byte	0x19b
	.4byte	0x9b3
	.byte	0x3
	.byte	0x23
	.uleb128 0x108
	.uleb128 0x12
	.4byte	.LASF123
	.byte	0x11
	.2byte	0x19d
	.4byte	0x9b3
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.uleb128 0x12
	.4byte	.LASF124
	.byte	0x11
	.2byte	0x1a2
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x168
	.uleb128 0x12
	.4byte	.LASF125
	.byte	0x11
	.2byte	0x1a9
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x16c
	.uleb128 0x12
	.4byte	.LASF126
	.byte	0x11
	.2byte	0x1ab
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x170
	.uleb128 0x12
	.4byte	.LASF127
	.byte	0x11
	.2byte	0x1ad
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x174
	.uleb128 0x12
	.4byte	.LASF128
	.byte	0x11
	.2byte	0x1af
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x178
	.uleb128 0x12
	.4byte	.LASF129
	.byte	0x11
	.2byte	0x1b5
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x17c
	.uleb128 0x12
	.4byte	.LASF130
	.byte	0x11
	.2byte	0x1b7
	.4byte	0x1d5
	.byte	0x3
	.byte	0x23
	.uleb128 0x180
	.uleb128 0x12
	.4byte	.LASF131
	.byte	0x11
	.2byte	0x1be
	.4byte	0x1d5
	.byte	0x3
	.byte	0x23
	.uleb128 0x184
	.uleb128 0x12
	.4byte	.LASF132
	.byte	0x11
	.2byte	0x1c0
	.4byte	0x1d5
	.byte	0x3
	.byte	0x23
	.uleb128 0x188
	.uleb128 0x12
	.4byte	.LASF133
	.byte	0x11
	.2byte	0x1c4
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x18c
	.uleb128 0x12
	.4byte	.LASF134
	.byte	0x11
	.2byte	0x1c6
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x190
	.uleb128 0x12
	.4byte	.LASF135
	.byte	0x11
	.2byte	0x1cc
	.4byte	0xfe
	.byte	0x3
	.byte	0x23
	.uleb128 0x194
	.uleb128 0x12
	.4byte	.LASF136
	.byte	0x11
	.2byte	0x1d0
	.4byte	0xfe
	.byte	0x3
	.byte	0x23
	.uleb128 0x1a8
	.uleb128 0x12
	.4byte	.LASF137
	.byte	0x11
	.2byte	0x1d6
	.4byte	0x6b6
	.byte	0x3
	.byte	0x23
	.uleb128 0x1bc
	.uleb128 0x12
	.4byte	.LASF138
	.byte	0x11
	.2byte	0x1dc
	.4byte	0x1d5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c4
	.uleb128 0x12
	.4byte	.LASF139
	.byte	0x11
	.2byte	0x1e2
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x1c8
	.uleb128 0x12
	.4byte	.LASF140
	.byte	0x11
	.2byte	0x1e5
	.4byte	0x6e9
	.byte	0x3
	.byte	0x23
	.uleb128 0x1cc
	.uleb128 0x12
	.4byte	.LASF141
	.byte	0x11
	.2byte	0x1eb
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x1d8
	.uleb128 0x12
	.4byte	.LASF142
	.byte	0x11
	.2byte	0x1f2
	.4byte	0x1d5
	.byte	0x3
	.byte	0x23
	.uleb128 0x1dc
	.uleb128 0x12
	.4byte	.LASF143
	.byte	0x11
	.2byte	0x1f4
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x1e0
	.byte	0
	.uleb128 0x9
	.4byte	0x97
	.4byte	0x9c3
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x11
	.4byte	.LASF144
	.byte	0x11
	.2byte	0x1f6
	.4byte	0x6f5
	.uleb128 0x5
	.byte	0x1c
	.byte	0x12
	.byte	0x8f
	.4byte	0xa3a
	.uleb128 0x6
	.4byte	.LASF145
	.byte	0x12
	.byte	0x94
	.4byte	0x399
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF146
	.byte	0x12
	.byte	0x99
	.4byte	0x399
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF147
	.byte	0x12
	.byte	0x9e
	.4byte	0x399
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF148
	.byte	0x12
	.byte	0xa3
	.4byte	0x399
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF149
	.byte	0x12
	.byte	0xad
	.4byte	0x399
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF150
	.byte	0x12
	.byte	0xb8
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF151
	.byte	0x12
	.byte	0xbe
	.4byte	0x1d5
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x3
	.4byte	.LASF152
	.byte	0x12
	.byte	0xc2
	.4byte	0x9cf
	.uleb128 0xb
	.byte	0x1c
	.byte	0x13
	.2byte	0x10c
	.4byte	0xab8
	.uleb128 0x13
	.ascii	"rip\000"
	.byte	0x13
	.2byte	0x112
	.4byte	0x1a2
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF153
	.byte	0x13
	.2byte	0x11b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x12
	.4byte	.LASF154
	.byte	0x13
	.2byte	0x122
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x12
	.4byte	.LASF155
	.byte	0x13
	.2byte	0x127
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x12
	.4byte	.LASF156
	.byte	0x13
	.2byte	0x138
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x12
	.4byte	.LASF157
	.byte	0x13
	.2byte	0x144
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x12
	.4byte	.LASF158
	.byte	0x13
	.2byte	0x14b
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x11
	.4byte	.LASF159
	.byte	0x13
	.2byte	0x14d
	.4byte	0xa45
	.uleb128 0xb
	.byte	0xec
	.byte	0x13
	.2byte	0x150
	.4byte	0xc98
	.uleb128 0x12
	.4byte	.LASF160
	.byte	0x13
	.2byte	0x157
	.4byte	0x628
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF161
	.byte	0x13
	.2byte	0x162
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x12
	.4byte	.LASF162
	.byte	0x13
	.2byte	0x164
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x12
	.4byte	.LASF163
	.byte	0x13
	.2byte	0x166
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x12
	.4byte	.LASF164
	.byte	0x13
	.2byte	0x168
	.4byte	0x3cb
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x12
	.4byte	.LASF165
	.byte	0x13
	.2byte	0x16e
	.4byte	0x172
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x12
	.4byte	.LASF166
	.byte	0x13
	.2byte	0x174
	.4byte	0xab8
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x12
	.4byte	.LASF167
	.byte	0x13
	.2byte	0x17b
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x12
	.4byte	.LASF168
	.byte	0x13
	.2byte	0x17d
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x12
	.4byte	.LASF169
	.byte	0x13
	.2byte	0x185
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x12
	.4byte	.LASF170
	.byte	0x13
	.2byte	0x18d
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x12
	.4byte	.LASF171
	.byte	0x13
	.2byte	0x191
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x12
	.4byte	.LASF172
	.byte	0x13
	.2byte	0x195
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x12
	.4byte	.LASF173
	.byte	0x13
	.2byte	0x199
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x12
	.4byte	.LASF174
	.byte	0x13
	.2byte	0x19e
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x12
	.4byte	.LASF175
	.byte	0x13
	.2byte	0x1a2
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x12
	.4byte	.LASF176
	.byte	0x13
	.2byte	0x1a6
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x12
	.4byte	.LASF177
	.byte	0x13
	.2byte	0x1b4
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x12
	.4byte	.LASF178
	.byte	0x13
	.2byte	0x1ba
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x12
	.4byte	.LASF179
	.byte	0x13
	.2byte	0x1c2
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x12
	.4byte	.LASF180
	.byte	0x13
	.2byte	0x1c4
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x12
	.4byte	.LASF181
	.byte	0x13
	.2byte	0x1c6
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x12
	.4byte	.LASF182
	.byte	0x13
	.2byte	0x1d5
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x12
	.4byte	.LASF183
	.byte	0x13
	.2byte	0x1d7
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x81
	.uleb128 0x12
	.4byte	.LASF184
	.byte	0x13
	.2byte	0x1dd
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0x12
	.4byte	.LASF185
	.byte	0x13
	.2byte	0x1e7
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0x12
	.4byte	.LASF186
	.byte	0x13
	.2byte	0x1f0
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x12
	.4byte	.LASF187
	.byte	0x13
	.2byte	0x1f7
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x12
	.4byte	.LASF188
	.byte	0x13
	.2byte	0x1f9
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x12
	.4byte	.LASF189
	.byte	0x13
	.2byte	0x1fd
	.4byte	0xc98
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0x9
	.4byte	0x70
	.4byte	0xca8
	.uleb128 0xa
	.4byte	0x25
	.byte	0x16
	.byte	0
	.uleb128 0x11
	.4byte	.LASF190
	.byte	0x13
	.2byte	0x204
	.4byte	0xac4
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF191
	.uleb128 0xb
	.byte	0x5c
	.byte	0x13
	.2byte	0x7c7
	.4byte	0xcf2
	.uleb128 0x12
	.4byte	.LASF192
	.byte	0x13
	.2byte	0x7cf
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF193
	.byte	0x13
	.2byte	0x7d6
	.4byte	0x686
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x12
	.4byte	.LASF189
	.byte	0x13
	.2byte	0x7df
	.4byte	0x3fd
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.byte	0
	.uleb128 0x11
	.4byte	.LASF194
	.byte	0x13
	.2byte	0x7e6
	.4byte	0xcbb
	.uleb128 0x1a
	.2byte	0x460
	.byte	0x13
	.2byte	0x7f0
	.4byte	0xd27
	.uleb128 0x12
	.4byte	.LASF160
	.byte	0x13
	.2byte	0x7f7
	.4byte	0x628
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF195
	.byte	0x13
	.2byte	0x7fd
	.4byte	0xd27
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x9
	.4byte	0xcf2
	.4byte	0xd37
	.uleb128 0xa
	.4byte	0x25
	.byte	0xb
	.byte	0
	.uleb128 0x11
	.4byte	.LASF196
	.byte	0x13
	.2byte	0x804
	.4byte	0xcfe
	.uleb128 0x9
	.4byte	0x4e9
	.4byte	0xd53
	.uleb128 0xa
	.4byte	0x25
	.byte	0xd
	.byte	0
	.uleb128 0xb
	.byte	0x18
	.byte	0x14
	.2byte	0x14b
	.4byte	0xda8
	.uleb128 0x12
	.4byte	.LASF197
	.byte	0x14
	.2byte	0x150
	.4byte	0x39f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF198
	.byte	0x14
	.2byte	0x157
	.4byte	0xda8
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x12
	.4byte	.LASF199
	.byte	0x14
	.2byte	0x159
	.4byte	0xdae
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x12
	.4byte	.LASF200
	.byte	0x14
	.2byte	0x15b
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x12
	.4byte	.LASF201
	.byte	0x14
	.2byte	0x15d
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x97
	.uleb128 0x8
	.byte	0x4
	.4byte	0xa3a
	.uleb128 0x11
	.4byte	.LASF202
	.byte	0x14
	.2byte	0x15f
	.4byte	0xd53
	.uleb128 0xb
	.byte	0xbc
	.byte	0x14
	.2byte	0x163
	.4byte	0x104f
	.uleb128 0x12
	.4byte	.LASF100
	.byte	0x14
	.2byte	0x165
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x12
	.4byte	.LASF101
	.byte	0x14
	.2byte	0x167
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x12
	.4byte	.LASF203
	.byte	0x14
	.2byte	0x16c
	.4byte	0xdb4
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x12
	.4byte	.LASF204
	.byte	0x14
	.2byte	0x173
	.4byte	0x1d5
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x12
	.4byte	.LASF205
	.byte	0x14
	.2byte	0x179
	.4byte	0x1d5
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x12
	.4byte	.LASF206
	.byte	0x14
	.2byte	0x17e
	.4byte	0x1d5
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x12
	.4byte	.LASF207
	.byte	0x14
	.2byte	0x184
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x12
	.4byte	.LASF208
	.byte	0x14
	.2byte	0x18a
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x12
	.4byte	.LASF209
	.byte	0x14
	.2byte	0x18c
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x12
	.4byte	.LASF210
	.byte	0x14
	.2byte	0x191
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x12
	.4byte	.LASF211
	.byte	0x14
	.2byte	0x197
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x12
	.4byte	.LASF212
	.byte	0x14
	.2byte	0x1a0
	.4byte	0x1d5
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x12
	.4byte	.LASF213
	.byte	0x14
	.2byte	0x1a8
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x12
	.4byte	.LASF214
	.byte	0x14
	.2byte	0x1b2
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x12
	.4byte	.LASF215
	.byte	0x14
	.2byte	0x1b8
	.4byte	0xdae
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x12
	.4byte	.LASF216
	.byte	0x14
	.2byte	0x1c2
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x12
	.4byte	.LASF217
	.byte	0x14
	.2byte	0x1c8
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x12
	.4byte	.LASF218
	.byte	0x14
	.2byte	0x1cc
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x12
	.4byte	.LASF219
	.byte	0x14
	.2byte	0x1d0
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x12
	.4byte	.LASF220
	.byte	0x14
	.2byte	0x1d4
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x12
	.4byte	.LASF221
	.byte	0x14
	.2byte	0x1d8
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x12
	.4byte	.LASF222
	.byte	0x14
	.2byte	0x1dc
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x12
	.4byte	.LASF223
	.byte	0x14
	.2byte	0x1e0
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x12
	.4byte	.LASF224
	.byte	0x14
	.2byte	0x1e6
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x12
	.4byte	.LASF225
	.byte	0x14
	.2byte	0x1e8
	.4byte	0x1d5
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x12
	.4byte	.LASF226
	.byte	0x14
	.2byte	0x1ef
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x12
	.4byte	.LASF227
	.byte	0x14
	.2byte	0x1f1
	.4byte	0x1d5
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x12
	.4byte	.LASF228
	.byte	0x14
	.2byte	0x1f9
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x12
	.4byte	.LASF229
	.byte	0x14
	.2byte	0x1fb
	.4byte	0x1d5
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x12
	.4byte	.LASF230
	.byte	0x14
	.2byte	0x1fd
	.4byte	0x70
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x12
	.4byte	.LASF231
	.byte	0x14
	.2byte	0x203
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x12
	.4byte	.LASF232
	.byte	0x14
	.2byte	0x20d
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x12
	.4byte	.LASF233
	.byte	0x14
	.2byte	0x20f
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x12
	.4byte	.LASF234
	.byte	0x14
	.2byte	0x215
	.4byte	0x1d5
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x12
	.4byte	.LASF235
	.byte	0x14
	.2byte	0x21c
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x12
	.4byte	.LASF236
	.byte	0x14
	.2byte	0x21e
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x12
	.4byte	.LASF237
	.byte	0x14
	.2byte	0x222
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x12
	.4byte	.LASF238
	.byte	0x14
	.2byte	0x226
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x12
	.4byte	.LASF239
	.byte	0x14
	.2byte	0x228
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x12
	.4byte	.LASF240
	.byte	0x14
	.2byte	0x237
	.4byte	0x1bf
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x12
	.4byte	.LASF241
	.byte	0x14
	.2byte	0x23f
	.4byte	0x1d5
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0x12
	.4byte	.LASF242
	.byte	0x14
	.2byte	0x249
	.4byte	0x1d5
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.byte	0
	.uleb128 0x11
	.4byte	.LASF243
	.byte	0x14
	.2byte	0x24b
	.4byte	0xdc0
	.uleb128 0x1b
	.2byte	0x100
	.byte	0x15
	.byte	0x2f
	.4byte	0x109e
	.uleb128 0x6
	.4byte	.LASF244
	.byte	0x15
	.byte	0x34
	.4byte	0x70
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF245
	.byte	0x15
	.byte	0x3b
	.4byte	0x109e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF246
	.byte	0x15
	.byte	0x46
	.4byte	0x10ae
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x6
	.4byte	.LASF247
	.byte	0x15
	.byte	0x50
	.4byte	0x109e
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.byte	0
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x10ae
	.uleb128 0xa
	.4byte	0x25
	.byte	0x14
	.byte	0
	.uleb128 0x9
	.4byte	0x97
	.4byte	0x10be
	.uleb128 0xa
	.4byte	0x25
	.byte	0x14
	.byte	0
	.uleb128 0x3
	.4byte	.LASF248
	.byte	0x15
	.byte	0x52
	.4byte	0x105b
	.uleb128 0x5
	.byte	0x10
	.byte	0x15
	.byte	0x5a
	.4byte	0x110a
	.uleb128 0x6
	.4byte	.LASF249
	.byte	0x15
	.byte	0x61
	.4byte	0x1f0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF250
	.byte	0x15
	.byte	0x63
	.4byte	0x111f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF251
	.byte	0x15
	.byte	0x65
	.4byte	0x112a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF252
	.byte	0x15
	.byte	0x6a
	.4byte	0x112a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x1c
	.byte	0x1
	.4byte	0x97
	.4byte	0x111a
	.uleb128 0x1d
	.4byte	0x111a
	.byte	0
	.uleb128 0x1e
	.4byte	0x70
	.uleb128 0x1e
	.4byte	0x1124
	.uleb128 0x8
	.byte	0x4
	.4byte	0x110a
	.uleb128 0x1e
	.4byte	0x622
	.uleb128 0x3
	.4byte	.LASF253
	.byte	0x15
	.byte	0x6c
	.4byte	0x10c9
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF261
	.byte	0x1
	.2byte	0x19f
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x11be
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x19f
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF255
	.byte	0x1
	.2byte	0x1a0
	.4byte	0x1f0
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x1a1
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x1a2
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x1a3
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x1a4
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x20
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x1a5
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.byte	0
	.uleb128 0x1e
	.4byte	0x11c3
	.uleb128 0x8
	.byte	0x4
	.4byte	0x40d
	.uleb128 0x1e
	.4byte	0x97
	.uleb128 0x8
	.byte	0x4
	.4byte	0x70
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF262
	.byte	0x1
	.2byte	0x1df
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x1258
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x1df
	.4byte	0x1258
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF263
	.byte	0x1
	.2byte	0x1e0
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x1e1
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x1e2
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x1e3
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x1e4
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x20
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x1e5
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.byte	0
	.uleb128 0x1e
	.4byte	0xfc
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF264
	.byte	0x1
	.2byte	0x22e
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x12e1
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x22e
	.4byte	0x1258
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF265
	.byte	0x1
	.2byte	0x22f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x230
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x231
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x232
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x233
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x20
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x234
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF266
	.byte	0x1
	.2byte	0x27d
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x1365
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x27d
	.4byte	0x1258
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF267
	.byte	0x1
	.2byte	0x27e
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x27f
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x280
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x281
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x282
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x20
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x283
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF268
	.byte	0x1
	.2byte	0x2dd
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x1407
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x2dd
	.4byte	0x1258
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x20
	.4byte	.LASF269
	.byte	0x1
	.2byte	0x2de
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x20
	.4byte	.LASF270
	.byte	0x1
	.2byte	0x2df
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x2e0
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x2e1
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x2e2
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x2e3
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x20
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x2e4
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x21
	.4byte	.LASF273
	.byte	0x1
	.2byte	0x2ec
	.4byte	0x1407
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x1417
	.uleb128 0xa
	.4byte	0x25
	.byte	0x10
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF271
	.byte	0x1
	.2byte	0x337
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x14b9
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x337
	.4byte	0x1258
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x20
	.4byte	.LASF269
	.byte	0x1
	.2byte	0x338
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x20
	.4byte	.LASF272
	.byte	0x1
	.2byte	0x339
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x33a
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x33b
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x33c
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x33d
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x20
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x33e
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x21
	.4byte	.LASF273
	.byte	0x1
	.2byte	0x346
	.4byte	0x1407
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF274
	.byte	0x1
	.2byte	0x391
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x155b
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x391
	.4byte	0x1258
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x20
	.4byte	.LASF269
	.byte	0x1
	.2byte	0x392
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x20
	.4byte	.LASF275
	.byte	0x1
	.2byte	0x393
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x394
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x395
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x396
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x397
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x20
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x398
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x21
	.4byte	.LASF273
	.byte	0x1
	.2byte	0x3a0
	.4byte	0x1407
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF276
	.byte	0x1
	.2byte	0x3eb
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x15fd
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x1
	.2byte	0x3eb
	.4byte	0x1258
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x20
	.4byte	.LASF269
	.byte	0x1
	.2byte	0x3ec
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x20
	.4byte	.LASF277
	.byte	0x1
	.2byte	0x3ed
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x20
	.4byte	.LASF256
	.byte	0x1
	.2byte	0x3ee
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x3ef
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x3f0
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x3f1
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x20
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x3f2
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x21
	.4byte	.LASF273
	.byte	0x1
	.2byte	0x3fa
	.4byte	0x1407
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x22
	.4byte	.LASF294
	.byte	0x1
	.2byte	0x44a
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x16ad
	.uleb128 0x20
	.4byte	.LASF278
	.byte	0x1
	.2byte	0x44a
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x20
	.4byte	.LASF279
	.byte	0x1
	.2byte	0x44b
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x44c
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x1
	.2byte	0x44d
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x44e
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x20
	.4byte	.LASF280
	.byte	0x1
	.2byte	0x44f
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x20
	.4byte	.LASF281
	.byte	0x1
	.2byte	0x450
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x21
	.4byte	.LASF282
	.byte	0x1
	.2byte	0x458
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x1
	.2byte	0x45a
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x21
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x45c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF300
	.byte	0x1
	.2byte	0x510
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x17ab
	.uleb128 0x20
	.4byte	.LASF285
	.byte	0x1
	.2byte	0x510
	.4byte	0x17ab
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x20
	.4byte	.LASF257
	.byte	0x1
	.2byte	0x511
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x20
	.4byte	.LASF259
	.byte	0x1
	.2byte	0x512
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x20
	.4byte	.LASF280
	.byte	0x1
	.2byte	0x513
	.4byte	0x111a
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x21
	.4byte	.LASF286
	.byte	0x1
	.2byte	0x51f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x21
	.4byte	.LASF287
	.byte	0x1
	.2byte	0x521
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x21
	.4byte	.LASF288
	.byte	0x1
	.2byte	0x525
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.4byte	.LASF289
	.byte	0x1
	.2byte	0x529
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x21
	.4byte	.LASF290
	.byte	0x1
	.2byte	0x52b
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x21
	.4byte	.LASF291
	.byte	0x1
	.2byte	0x52d
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x21
	.4byte	.LASF284
	.byte	0x1
	.2byte	0x52f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LASF292
	.byte	0x1
	.2byte	0x531
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x21
	.4byte	.LASF293
	.byte	0x1
	.2byte	0x533
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x535
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x537
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x17b1
	.uleb128 0x1e
	.4byte	0x3e
	.uleb128 0x25
	.4byte	.LASF295
	.byte	0x2
	.byte	0x68
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x17dd
	.uleb128 0x26
	.4byte	.LASF296
	.byte	0x2
	.byte	0x68
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF297
	.byte	0x2
	.byte	0x6f
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.uleb128 0x27
	.byte	0x1
	.4byte	.LASF298
	.byte	0x2
	.byte	0x7e
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.uleb128 0x25
	.4byte	.LASF299
	.byte	0x2
	.byte	0xa2
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0x1866
	.uleb128 0x26
	.4byte	.LASF254
	.byte	0x2
	.byte	0xa2
	.4byte	0x1258
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x26
	.4byte	.LASF259
	.byte	0x2
	.byte	0xa2
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x28
	.4byte	.LASF282
	.byte	0x2
	.byte	0xa4
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x28
	.4byte	.LASF60
	.byte	0x2
	.byte	0xa6
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF284
	.byte	0x2
	.byte	0xa8
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x29
	.byte	0x1
	.4byte	.LASF301
	.byte	0x2
	.byte	0xfc
	.byte	0x1
	.4byte	0xfc
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0x18bc
	.uleb128 0x26
	.4byte	.LASF258
	.byte	0x2
	.byte	0xfc
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x26
	.4byte	.LASF302
	.byte	0x2
	.byte	0xfc
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.4byte	.LASF283
	.byte	0x2
	.byte	0xfe
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x28
	.4byte	.LASF303
	.byte	0x2
	.byte	0xfe
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF304
	.byte	0x2
	.2byte	0x13a
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0x19bb
	.uleb128 0x20
	.4byte	.LASF285
	.byte	0x2
	.2byte	0x13a
	.4byte	0x19bb
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x20
	.4byte	.LASF305
	.byte	0x2
	.2byte	0x13b
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x20
	.4byte	.LASF306
	.byte	0x2
	.2byte	0x13c
	.4byte	0xda8
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x20
	.4byte	.LASF307
	.byte	0x2
	.2byte	0x13d
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x20
	.4byte	.LASF308
	.byte	0x2
	.2byte	0x13e
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x21
	.4byte	.LASF309
	.byte	0x2
	.2byte	0x140
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x21
	.4byte	.LASF310
	.byte	0x2
	.2byte	0x142
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x144
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.4byte	.LASF311
	.byte	0x2
	.2byte	0x146
	.4byte	0x399
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x21
	.4byte	.LASF312
	.byte	0x2
	.2byte	0x148
	.4byte	0x399
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x21
	.4byte	.LASF289
	.byte	0x2
	.2byte	0x14b
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x21
	.4byte	.LASF313
	.byte	0x2
	.2byte	0x14f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LASF314
	.byte	0x2
	.2byte	0x153
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x21
	.4byte	.LASF284
	.byte	0x2
	.2byte	0x155
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x157
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x399
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF315
	.byte	0x2
	.2byte	0x247
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0x19eb
	.uleb128 0x20
	.4byte	.LASF316
	.byte	0x2
	.2byte	0x247
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF317
	.byte	0x2
	.2byte	0x25e
	.byte	0x1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0x1a15
	.uleb128 0x20
	.4byte	.LASF316
	.byte	0x2
	.2byte	0x25e
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF318
	.byte	0x2
	.2byte	0x27b
	.byte	0x1
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0x1a5d
	.uleb128 0x20
	.4byte	.LASF319
	.byte	0x2
	.2byte	0x27b
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x20
	.4byte	.LASF320
	.byte	0x2
	.2byte	0x27b
	.4byte	0x11c9
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x27d
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF321
	.byte	0x2
	.2byte	0x29d
	.byte	0x1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.4byte	0x1a96
	.uleb128 0x20
	.4byte	.LASF319
	.byte	0x2
	.2byte	0x29d
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x29f
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF322
	.byte	0x2
	.2byte	0x2ca
	.byte	0x1
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.4byte	0x1ade
	.uleb128 0x20
	.4byte	.LASF319
	.byte	0x2
	.2byte	0x2ca
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x2cc
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LASF284
	.byte	0x2
	.2byte	0x2ce
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF323
	.byte	0x2
	.2byte	0x2f7
	.byte	0x1
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.4byte	0x1b08
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x2f9
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF324
	.byte	0x2
	.2byte	0x31b
	.byte	0x1
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST22
	.4byte	0x1b41
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x31d
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LASF284
	.byte	0x2
	.2byte	0x31f
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF325
	.byte	0x2
	.2byte	0x362
	.byte	0x1
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST23
	.4byte	0x1b7a
	.uleb128 0x20
	.4byte	.LASF326
	.byte	0x2
	.2byte	0x362
	.4byte	0x1b7a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x364
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1e
	.4byte	0x5e
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF327
	.byte	0x2
	.2byte	0x378
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST24
	.4byte	0x1bd7
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x378
	.4byte	0x1bd7
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x37a
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x37c
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x24
	.ascii	"i\000"
	.byte	0x2
	.2byte	0x37e
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x1e
	.4byte	0x1bdc
	.uleb128 0x8
	.byte	0x4
	.4byte	0x1be2
	.uleb128 0x1e
	.4byte	0x40d
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF328
	.byte	0x2
	.2byte	0x3ae
	.byte	0x1
	.4byte	0xfc
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST25
	.4byte	0x1c33
	.uleb128 0x20
	.4byte	.LASF258
	.byte	0x2
	.2byte	0x3ae
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF329
	.byte	0x2
	.2byte	0x3ae
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x3b0
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF330
	.byte	0x2
	.2byte	0x3dd
	.byte	0x1
	.4byte	0xfc
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST26
	.4byte	0x1c70
	.uleb128 0x20
	.4byte	.LASF331
	.byte	0x2
	.2byte	0x3dd
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x3df
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF332
	.byte	0x2
	.2byte	0x400
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST27
	.4byte	0x1c9d
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x402
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF333
	.byte	0x2
	.2byte	0x41e
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST28
	.4byte	0x1cda
	.uleb128 0x20
	.4byte	.LASF263
	.byte	0x2
	.2byte	0x41e
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF334
	.byte	0x2
	.2byte	0x41e
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF335
	.byte	0x2
	.2byte	0x436
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST29
	.4byte	0x1d16
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x436
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x438
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF336
	.byte	0x2
	.2byte	0x457
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST30
	.4byte	0x1d52
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x457
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x459
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF337
	.byte	0x2
	.2byte	0x480
	.byte	0x1
	.4byte	0x97
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST31
	.4byte	0x1d8e
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x480
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x482
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF338
	.byte	0x2
	.2byte	0x4a1
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST32
	.4byte	0x1dd9
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x4a1
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF269
	.byte	0x2
	.2byte	0x4a1
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x4a5
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF339
	.byte	0x2
	.2byte	0x4ca
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST33
	.4byte	0x1e24
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x4ca
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF269
	.byte	0x2
	.2byte	0x4ca
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x4cc
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF340
	.byte	0x2
	.2byte	0x4ef
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST34
	.4byte	0x1e6f
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x4ef
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF269
	.byte	0x2
	.2byte	0x4ef
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x4f1
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF341
	.byte	0x2
	.2byte	0x514
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST35
	.4byte	0x1eba
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x514
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF269
	.byte	0x2
	.2byte	0x514
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x516
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF342
	.byte	0x2
	.2byte	0x53b
	.byte	0x1
	.4byte	0x70
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST36
	.4byte	0x1ee8
	.uleb128 0x20
	.4byte	.LASF343
	.byte	0x2
	.2byte	0x53b
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF344
	.byte	0x2
	.2byte	0x543
	.byte	0x1
	.4byte	0x97
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST37
	.4byte	0x1f33
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x543
	.4byte	0x11be
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x545
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.4byte	.LASF345
	.byte	0x2
	.2byte	0x547
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF346
	.byte	0x2
	.2byte	0x560
	.byte	0x1
	.4byte	0x11ce
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST38
	.4byte	0x1f70
	.uleb128 0x20
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x560
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x20
	.4byte	.LASF347
	.byte	0x2
	.2byte	0x560
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF348
	.byte	0x2
	.2byte	0x57c
	.byte	0x1
	.4byte	0x11c3
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST39
	.4byte	0x1f9e
	.uleb128 0x21
	.4byte	.LASF349
	.byte	0x2
	.2byte	0x57e
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF350
	.byte	0x2
	.2byte	0x5cb
	.byte	0x1
	.4byte	0x11c3
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST40
	.4byte	0x1fea
	.uleb128 0x20
	.4byte	.LASF351
	.byte	0x2
	.2byte	0x5cb
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF329
	.byte	0x2
	.2byte	0x5cb
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LASF349
	.byte	0x2
	.2byte	0x5cd
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF352
	.byte	0x2
	.2byte	0x618
	.byte	0x1
	.4byte	0x11c3
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST41
	.4byte	0x2036
	.uleb128 0x20
	.4byte	.LASF351
	.byte	0x2
	.2byte	0x618
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x20
	.4byte	.LASF329
	.byte	0x2
	.2byte	0x618
	.4byte	0x11ce
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LASF349
	.byte	0x2
	.2byte	0x61a
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF353
	.byte	0x2
	.2byte	0x650
	.byte	0x1
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST42
	.4byte	0x2060
	.uleb128 0x21
	.4byte	.LASF354
	.byte	0x2
	.2byte	0x65a
	.4byte	0xfc
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF355
	.byte	0x2
	.2byte	0x675
	.byte	0x1
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST43
	.4byte	0x20a8
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x67e
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LASF60
	.byte	0x2
	.2byte	0x680
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.4byte	.LASF61
	.byte	0x2
	.2byte	0x681
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF356
	.byte	0x2
	.2byte	0x6a5
	.byte	0x1
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST44
	.4byte	0x20d2
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x6a7
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF357
	.byte	0x2
	.2byte	0x6ca
	.byte	0x1
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LLST45
	.4byte	0x210b
	.uleb128 0x20
	.4byte	.LASF358
	.byte	0x2
	.2byte	0x6ca
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x6cc
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF359
	.byte	0x2
	.2byte	0x6ec
	.byte	0x1
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST46
	.4byte	0x2153
	.uleb128 0x20
	.4byte	.LASF360
	.byte	0x2
	.2byte	0x6ec
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x21
	.4byte	.LASF283
	.byte	0x2
	.2byte	0x6ee
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x21
	.4byte	.LASF361
	.byte	0x2
	.2byte	0x6f0
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.4byte	.LASF362
	.byte	0x2
	.2byte	0x72b
	.byte	0x1
	.4byte	0x97
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST47
	.4byte	0x21cb
	.uleb128 0x20
	.4byte	.LASF363
	.byte	0x2
	.2byte	0x72b
	.4byte	0x111a
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x21
	.4byte	.LASF349
	.byte	0x2
	.2byte	0x732
	.4byte	0x11c3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x21
	.4byte	.LASF364
	.byte	0x2
	.2byte	0x734
	.4byte	0x399
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x21
	.4byte	.LASF365
	.byte	0x2
	.2byte	0x736
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x24
	.ascii	"ucp\000"
	.byte	0x2
	.2byte	0x738
	.4byte	0x399
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x24
	.ascii	"rv\000"
	.byte	0x2
	.2byte	0x73a
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x21db
	.uleb128 0xa
	.4byte	0x25
	.byte	0x40
	.byte	0
	.uleb128 0x2a
	.4byte	.LASF366
	.byte	0x16
	.2byte	0x1fc
	.4byte	0x21cb
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x21f9
	.uleb128 0xa
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x2a
	.4byte	.LASF367
	.byte	0x16
	.2byte	0x26a
	.4byte	0x21e9
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF368
	.byte	0x16
	.2byte	0x285
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF369
	.byte	0x16
	.2byte	0x286
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF370
	.byte	0x16
	.2byte	0x289
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF371
	.byte	0x16
	.2byte	0x290
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF372
	.byte	0x16
	.2byte	0x291
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF373
	.byte	0x16
	.2byte	0x293
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF374
	.byte	0x16
	.2byte	0x294
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF375
	.byte	0x16
	.2byte	0x295
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF376
	.byte	0x16
	.2byte	0x296
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF377
	.byte	0x16
	.2byte	0x297
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF378
	.byte	0x16
	.2byte	0x298
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF379
	.byte	0x16
	.2byte	0x299
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF380
	.byte	0x16
	.2byte	0x29a
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF381
	.byte	0x16
	.2byte	0x415
	.4byte	0x21e9
	.byte	0x1
	.byte	0x1
	.uleb128 0x28
	.4byte	.LASF382
	.byte	0x17
	.byte	0x30
	.4byte	0x22dc
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x1e
	.4byte	0x1e0
	.uleb128 0x28
	.4byte	.LASF383
	.byte	0x17
	.byte	0x34
	.4byte	0x22f2
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x1e
	.4byte	0x1e0
	.uleb128 0x28
	.4byte	.LASF384
	.byte	0x17
	.byte	0x36
	.4byte	0x2308
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x1e
	.4byte	0x1e0
	.uleb128 0x28
	.4byte	.LASF385
	.byte	0x17
	.byte	0x38
	.4byte	0x231e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x1e
	.4byte	0x1e0
	.uleb128 0x2b
	.4byte	.LASF386
	.byte	0xa
	.byte	0x61
	.4byte	0x97
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF387
	.byte	0xa
	.byte	0x63
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF388
	.byte	0xa
	.byte	0x65
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF389
	.byte	0xe
	.byte	0x62
	.4byte	0xfe
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x2367
	.uleb128 0xa
	.4byte	0x25
	.byte	0x19
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF390
	.byte	0xe
	.byte	0x64
	.4byte	0x2374
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	0x2357
	.uleb128 0x2b
	.4byte	.LASF391
	.byte	0x18
	.byte	0x16
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF392
	.byte	0x18
	.byte	0x1a
	.4byte	0xd43
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF393
	.byte	0x11
	.2byte	0x20c
	.4byte	0x9c3
	.byte	0x1
	.byte	0x1
	.uleb128 0x28
	.4byte	.LASF394
	.byte	0x19
	.byte	0x33
	.4byte	0x23b2
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x1e
	.4byte	0x3dd
	.uleb128 0x28
	.4byte	.LASF395
	.byte	0x19
	.byte	0x3f
	.4byte	0x23c8
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x1e
	.4byte	0x3fd
	.uleb128 0x2b
	.4byte	.LASF396
	.byte	0x1a
	.byte	0x9f
	.4byte	0x1ca
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF397
	.byte	0x1a
	.byte	0xfe
	.4byte	0x1ca
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF398
	.byte	0x13
	.2byte	0x206
	.4byte	0xca8
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF399
	.byte	0x13
	.2byte	0x80b
	.4byte	0xd37
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x1f0
	.4byte	0x2413
	.uleb128 0xa
	.4byte	0x25
	.byte	0x11
	.byte	0
	.uleb128 0x28
	.4byte	.LASF400
	.byte	0x1
	.byte	0x7d
	.4byte	0x2424
	.byte	0x5
	.byte	0x3
	.4byte	LIGHTS_database_field_names
	.uleb128 0x1e
	.4byte	0x2403
	.uleb128 0x2a
	.4byte	.LASF401
	.byte	0x14
	.2byte	0x24f
	.4byte	0x104f
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF402
	.byte	0x15
	.byte	0x55
	.4byte	0x10be
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x112f
	.4byte	0x2454
	.uleb128 0xa
	.4byte	0x25
	.byte	0x14
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF403
	.byte	0x15
	.byte	0x6f
	.4byte	0x2461
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	0x2444
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x2476
	.uleb128 0xa
	.4byte	0x25
	.byte	0x6
	.byte	0
	.uleb128 0x28
	.4byte	.LASF404
	.byte	0x2
	.byte	0x42
	.4byte	0x2487
	.byte	0x5
	.byte	0x3
	.4byte	LIGHTS_FILENAME
	.uleb128 0x1e
	.4byte	0x2466
	.uleb128 0x9
	.4byte	0x70
	.4byte	0x249c
	.uleb128 0xa
	.4byte	0x25
	.byte	0
	.byte	0
	.uleb128 0x2b
	.4byte	.LASF405
	.byte	0x2
	.byte	0x5a
	.4byte	0x24a9
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	0x248c
	.uleb128 0x2a
	.4byte	.LASF366
	.byte	0x16
	.2byte	0x1fc
	.4byte	0x21cb
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF367
	.byte	0x16
	.2byte	0x26a
	.4byte	0x21e9
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF368
	.byte	0x16
	.2byte	0x285
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF369
	.byte	0x16
	.2byte	0x286
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF370
	.byte	0x16
	.2byte	0x289
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF371
	.byte	0x16
	.2byte	0x290
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF372
	.byte	0x16
	.2byte	0x291
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF373
	.byte	0x16
	.2byte	0x293
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF374
	.byte	0x16
	.2byte	0x294
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF375
	.byte	0x16
	.2byte	0x295
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF376
	.byte	0x16
	.2byte	0x296
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF377
	.byte	0x16
	.2byte	0x297
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF378
	.byte	0x16
	.2byte	0x298
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF379
	.byte	0x16
	.2byte	0x299
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF380
	.byte	0x16
	.2byte	0x29a
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF381
	.byte	0x16
	.2byte	0x415
	.4byte	0x21e9
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF386
	.byte	0xa
	.byte	0x61
	.4byte	0x97
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF387
	.byte	0xa
	.byte	0x63
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF388
	.byte	0xa
	.byte	0x65
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2c
	.4byte	.LASF389
	.byte	0x2
	.byte	0x44
	.4byte	0xfe
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	light_list_hdr
	.uleb128 0x2c
	.4byte	.LASF390
	.byte	0x2
	.byte	0x46
	.4byte	0x25d9
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	LIGHTS_DEFAULT_NAME
	.uleb128 0x1e
	.4byte	0x2357
	.uleb128 0x2c
	.4byte	.LASF406
	.byte	0x2
	.byte	0x48
	.4byte	0x3cb
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	g_LIGHTS_initial_date_time
	.uleb128 0x2b
	.4byte	.LASF391
	.byte	0x18
	.byte	0x16
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF392
	.byte	0x18
	.byte	0x1a
	.4byte	0xd43
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF393
	.byte	0x11
	.2byte	0x20c
	.4byte	0x9c3
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF396
	.byte	0x1a
	.byte	0x9f
	.4byte	0x1ca
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF397
	.byte	0x1a
	.byte	0xfe
	.4byte	0x1ca
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF398
	.byte	0x13
	.2byte	0x206
	.4byte	0xca8
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF399
	.byte	0x13
	.2byte	0x80b
	.4byte	0xd37
	.byte	0x1
	.byte	0x1
	.uleb128 0x2a
	.4byte	.LASF401
	.byte	0x14
	.2byte	0x24f
	.4byte	0x104f
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF402
	.byte	0x15
	.byte	0x55
	.4byte	0x10be
	.byte	0x1
	.byte	0x1
	.uleb128 0x2b
	.4byte	.LASF403
	.byte	0x15
	.byte	0x6f
	.4byte	0x2676
	.byte	0x1
	.byte	0x1
	.uleb128 0x1e
	.4byte	0x2444
	.uleb128 0x2c
	.4byte	.LASF405
	.byte	0x2
	.byte	0x5a
	.4byte	0x268d
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	light_list_item_sizes
	.uleb128 0x1e
	.4byte	0x248c
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI43
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI46
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI49
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI52
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI55
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI58
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI61
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI64
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB22
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI67
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB23
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI69
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI70
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB24
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI73
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB25
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI75
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI76
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB26
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI78
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI79
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB27
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI81
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI82
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB28
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI85
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB29
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI87
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI88
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB30
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI90
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI91
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB31
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI93
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI94
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB32
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI96
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI97
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB33
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI99
	.4byte	.LCFI100
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI100
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB34
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI102
	.4byte	.LCFI103
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI103
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB35
	.4byte	.LCFI105
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI105
	.4byte	.LCFI106
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI106
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB36
	.4byte	.LCFI108
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI108
	.4byte	.LCFI109
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI109
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB37
	.4byte	.LCFI111
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI111
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI112
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB38
	.4byte	.LCFI114
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI114
	.4byte	.LCFI115
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI115
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB39
	.4byte	.LCFI117
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI117
	.4byte	.LCFI118
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI118
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB40
	.4byte	.LCFI120
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI120
	.4byte	.LCFI121
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI121
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB41
	.4byte	.LCFI123
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI123
	.4byte	.LCFI124
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI124
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB42
	.4byte	.LCFI126
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI126
	.4byte	.LCFI127
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI127
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB43
	.4byte	.LCFI129
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI129
	.4byte	.LCFI130
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI130
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB44
	.4byte	.LCFI132
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI132
	.4byte	.LCFI133
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI133
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB45
	.4byte	.LCFI135
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI135
	.4byte	.LCFI136
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI136
	.4byte	.LFE45
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB46
	.4byte	.LCFI138
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI138
	.4byte	.LCFI139
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI139
	.4byte	.LFE46
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB47
	.4byte	.LCFI141
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI141
	.4byte	.LCFI142
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI142
	.4byte	.LFE47
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x194
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF43:
	.ascii	"I2C_CARD_DETAILS_STRUCT\000"
.LASF225:
	.ascii	"send_weather_data_timer\000"
.LASF358:
	.ascii	"pset_or_clear\000"
.LASF78:
	.ascii	"port_a_freewave_lr_set_for_repeater\000"
.LASF121:
	.ascii	"failures_to_respond_to_the_token_today\000"
.LASF290:
	.ascii	"lbox_index\000"
.LASF345:
	.ascii	"lday\000"
.LASF287:
	.ascii	"ltemporary_light\000"
.LASF316:
	.ascii	"pday_index\000"
.LASF168:
	.ascii	"nlu_old_rain_table_sync_variable\000"
.LASF207:
	.ascii	"connection_process_failures\000"
.LASF184:
	.ascii	"perform_a_discovery_following_a_FACTORY_reset\000"
.LASF21:
	.ascii	"pPrev\000"
.LASF254:
	.ascii	"plight\000"
.LASF212:
	.ascii	"alerts_timer\000"
.LASF192:
	.ascii	"saw_during_the_scan\000"
.LASF197:
	.ascii	"message\000"
.LASF322:
	.ascii	"LIGHTS_copy_file_schedule_into_global_daily_schedul"
	.ascii	"e\000"
.LASF40:
	.ascii	"GROUP_BASE_DEFINITION_STRUCT\000"
.LASF237:
	.ascii	"waiting_for_et_rain_tables_response\000"
.LASF126:
	.ascii	"device_exchange_port\000"
.LASF141:
	.ascii	"perform_two_wire_discovery\000"
.LASF196:
	.ascii	"CHAIN_MEMBERS_STRUCT\000"
.LASF90:
	.ascii	"serial_number\000"
.LASF371:
	.ascii	"GuiVar_LightsOutput\000"
.LASF332:
	.ascii	"LIGHTS_get_list_count\000"
.LASF252:
	.ascii	"__clean_house_processing\000"
.LASF10:
	.ascii	"unsigned int\000"
.LASF155:
	.ascii	"midnight_to_midnight_raw_pulse_count\000"
.LASF143:
	.ascii	"flowsense_devices_are_connected\000"
.LASF336:
	.ascii	"LIGHTS_get_output_index\000"
.LASF145:
	.ascii	"original_allocation\000"
.LASF115:
	.ascii	"request_a_scan_in_the_next_token_RESP\000"
.LASF35:
	.ascii	"list_support\000"
.LASF298:
	.ascii	"save_file_LIGHTS\000"
.LASF391:
	.ascii	"g_LIGHTS_numeric_date\000"
.LASF91:
	.ascii	"purchased_options\000"
.LASF312:
	.ascii	"llocation_of_bitfield\000"
.LASF255:
	.ascii	"pname\000"
.LASF118:
	.ascii	"i_am_the_master_and_i_have_rebooted\000"
.LASF66:
	.ascii	"changes_to_upload_to_comm_server\000"
.LASF205:
	.ascii	"waiting_to_start_the_connection_process_timer\000"
.LASF405:
	.ascii	"light_list_item_sizes\000"
.LASF377:
	.ascii	"GuiVar_LightsStopTime1InMinutes\000"
.LASF132:
	.ascii	"timer_token_rate_timer\000"
.LASF300:
	.ascii	"nm_LIGHTS_extract_and_store_changes_from_comm\000"
.LASF31:
	.ascii	"portTickType\000"
.LASF200:
	.ascii	"init_packet_message_id\000"
.LASF397:
	.ascii	"list_lights_recursive_MUTEX\000"
.LASF8:
	.ascii	"short int\000"
.LASF130:
	.ascii	"timer_device_exchange\000"
.LASF27:
	.ascii	"ET_TABLE_ENTRY\000"
.LASF271:
	.ascii	"nm_LIGHTS_set_start_time_2\000"
.LASF179:
	.ascii	"commserver_monthly_rcvd_bytes\000"
.LASF100:
	.ascii	"mode\000"
.LASF152:
	.ascii	"FLOW_RECORDING_CONTROL_STRUCT\000"
.LASF206:
	.ascii	"process_timer\000"
.LASF385:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF75:
	.ascii	"option_SSE_D\000"
.LASF258:
	.ascii	"pbox_index_0\000"
.LASF295:
	.ascii	"nm_light_structure_updater\000"
.LASF223:
	.ascii	"waiting_for_lights_report_data_response\000"
.LASF103:
	.ascii	"flag_chain_down_at_the_next_opportunity\000"
.LASF362:
	.ascii	"LIGHTS_calculate_chain_sync_crc\000"
.LASF404:
	.ascii	"LIGHTS_FILENAME\000"
.LASF55:
	.ascii	"DATA_HANDLE\000"
.LASF154:
	.ascii	"roll_to_roll_raw_pulse_count\000"
.LASF408:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/lights.c\000"
.LASF147:
	.ascii	"first_to_display\000"
.LASF72:
	.ascii	"LIGHTS_DAY_STRUCT\000"
.LASF342:
	.ascii	"LIGHTS_get_day_index\000"
.LASF172:
	.ascii	"yesterdays_ET_was_zero\000"
.LASF289:
	.ascii	"lnum_changed_lights\000"
.LASF285:
	.ascii	"pucp\000"
.LASF350:
	.ascii	"nm_LIGHTS_get_next_available_light\000"
.LASF39:
	.ascii	"deleted\000"
.LASF165:
	.ascii	"et_rip\000"
.LASF171:
	.ascii	"run_away_gage\000"
.LASF195:
	.ascii	"members\000"
.LASF390:
	.ascii	"LIGHTS_DEFAULT_NAME\000"
.LASF76:
	.ascii	"option_HUB\000"
.LASF57:
	.ascii	"float\000"
.LASF46:
	.ascii	"weather_card_present\000"
.LASF114:
	.ascii	"since_the_scan__slave__number_of_clean_token_respon"
	.ascii	"ses_made\000"
.LASF160:
	.ascii	"verify_string_pre\000"
.LASF56:
	.ascii	"DATE_TIME\000"
.LASF59:
	.ascii	"base\000"
.LASF17:
	.ascii	"count\000"
.LASF36:
	.ascii	"number_of_groups_ever_created\000"
.LASF239:
	.ascii	"waiting_for_hub_is_busy_msg_response\000"
.LASF83:
	.ascii	"option_AQUAPONICS\000"
.LASF164:
	.ascii	"dls_after_fall_when_to_clear_ignore_sxs\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF364:
	.ascii	"checksum_start\000"
.LASF148:
	.ascii	"first_to_send\000"
.LASF333:
	.ascii	"LIGHTS_get_lights_array_index\000"
.LASF98:
	.ascii	"reason\000"
.LASF284:
	.ascii	"day_index\000"
.LASF137:
	.ascii	"changes\000"
.LASF365:
	.ascii	"checksum_length\000"
.LASF183:
	.ascii	"factory_reset_registration_to_tell_commserver_to_cl"
	.ascii	"ear_pdata\000"
.LASF153:
	.ascii	"hourly_total_inches_100u\000"
.LASF181:
	.ascii	"commserver_monthly_code_receipt_bytes\000"
.LASF69:
	.ascii	"start_time_2_in_seconds\000"
.LASF325:
	.ascii	"LIGHTS_load_group_name_into_guivar\000"
.LASF310:
	.ascii	"lbitfield_of_changes_to_send\000"
.LASF249:
	.ascii	"file_name_string\000"
.LASF257:
	.ascii	"preason_for_change\000"
.LASF80:
	.ascii	"port_b_raveon_radio_type\000"
.LASF311:
	.ascii	"llocation_of_num_changed_lights\000"
.LASF134:
	.ascii	"token_in_transit\000"
.LASF344:
	.ascii	"LIGHTS_has_a_stop_time\000"
.LASF125:
	.ascii	"device_exchange_initial_event\000"
.LASF67:
	.ascii	"changes_uploaded_to_comm_server_awaiting_ACK\000"
.LASF93:
	.ascii	"port_B_device_index\000"
.LASF70:
	.ascii	"stop_time_1_in_seconds\000"
.LASF369:
	.ascii	"GuiVar_LightIsPhysicallyAvailable\000"
.LASF34:
	.ascii	"xTimerHandle\000"
.LASF29:
	.ascii	"RAIN_TABLE_ENTRY\000"
.LASF16:
	.ascii	"ptail\000"
.LASF335:
	.ascii	"LIGHTS_get_box_index\000"
.LASF361:
	.ascii	"lfile_save_necessary\000"
.LASF178:
	.ascii	"pending_changes_to_send_to_comm_server\000"
.LASF203:
	.ascii	"now_xmitting\000"
.LASF306:
	.ascii	"pmem_used_so_far_is_less_than_allocated_memory\000"
.LASF158:
	.ascii	"needs_to_be_broadcast\000"
.LASF387:
	.ascii	"g_GROUP_ID\000"
.LASF18:
	.ascii	"offset\000"
.LASF209:
	.ascii	"last_message_concluded_with_a_new_inbound_message\000"
.LASF28:
	.ascii	"rain_inches_u16_100u\000"
.LASF19:
	.ascii	"InUse\000"
.LASF92:
	.ascii	"port_A_device_index\000"
.LASF348:
	.ascii	"nm_LIGHTS_get_first_available_light_and_init_lights"
	.ascii	"_output_GuiVars\000"
.LASF175:
	.ascii	"rain_switch_active\000"
.LASF276:
	.ascii	"nm_LIGHTS_set_stop_time_2\000"
.LASF120:
	.ascii	"failures_to_respond_to_the_token\000"
.LASF248:
	.ascii	"CHAIN_SYNC_CONTROL_STRUCTURE\000"
.LASF270:
	.ascii	"plight_start_time_1\000"
.LASF272:
	.ascii	"plight_start_time_2\000"
.LASF324:
	.ascii	"LIGHTS_extract_and_store_changes_from_GuiVars\000"
.LASF246:
	.ascii	"crc_is_valid\000"
.LASF117:
	.ascii	"i_am_a_slave_and_i_have_rebooted\000"
.LASF370:
	.ascii	"GuiVar_LightsBoxIndex_0\000"
.LASF378:
	.ascii	"GuiVar_LightsStopTime2InMinutes\000"
.LASF161:
	.ascii	"dls_saved_date\000"
.LASF116:
	.ascii	"flag_myself_as_NEW_in_the_scan_RESP\000"
.LASF356:
	.ascii	"LIGHTS_set_bits_on_all_lights_to_cause_distribution"
	.ascii	"_in_the_next_token\000"
.LASF64:
	.ascii	"changes_to_send_to_master\000"
.LASF360:
	.ascii	"pcomm_error_occurred\000"
.LASF232:
	.ascii	"a_pdata_message_is_on_the_list\000"
.LASF388:
	.ascii	"g_GROUP_list_item_index\000"
.LASF282:
	.ascii	"lchange_bitfield_to_set\000"
.LASF340:
	.ascii	"LIGHTS_get_stop_time_1\000"
.LASF341:
	.ascii	"LIGHTS_get_stop_time_2\000"
.LASF42:
	.ascii	"tb_present\000"
.LASF53:
	.ascii	"dptr\000"
.LASF314:
	.ascii	"lmem_overhead_per_light\000"
.LASF1:
	.ascii	"char\000"
.LASF176:
	.ascii	"freeze_switch_active\000"
.LASF302:
	.ascii	"output_index\000"
.LASF220:
	.ascii	"waiting_for_poc_report_data_response\000"
.LASF208:
	.ascii	"last_message_concluded_with_a_response_timeout\000"
.LASF313:
	.ascii	"lmem_used_by_this_light\000"
.LASF94:
	.ascii	"BOX_CONFIGURATION_STRUCT\000"
.LASF221:
	.ascii	"waiting_for_system_report_data_response\000"
.LASF262:
	.ascii	"nm_LIGHTS_set_box_index\000"
.LASF169:
	.ascii	"et_table_update_all_historical_values\000"
.LASF49:
	.ascii	"dash_m_terminal_present\000"
.LASF15:
	.ascii	"phead\000"
.LASF215:
	.ascii	"current_msg_frcs_ptr\000"
.LASF12:
	.ascii	"long long int\000"
.LASF273:
	.ascii	"lfield_name\000"
.LASF211:
	.ascii	"a_registration_message_is_on_the_list\000"
.LASF261:
	.ascii	"nm_LIGHTS_set_name\000"
.LASF268:
	.ascii	"nm_LIGHTS_set_start_time_1\000"
.LASF74:
	.ascii	"option_SSE\000"
.LASF395:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF400:
	.ascii	"LIGHTS_database_field_names\000"
.LASF26:
	.ascii	"status\000"
.LASF259:
	.ascii	"pset_change_bits\000"
.LASF188:
	.ascii	"hub_needs_to_distribute_main_binary\000"
.LASF267:
	.ascii	"pavailable\000"
.LASF182:
	.ascii	"factory_reset_do_not_allow_pdata_to_be_sent\000"
.LASF87:
	.ascii	"sizer\000"
.LASF124:
	.ascii	"broadcast_chain_members_array\000"
.LASF308:
	.ascii	"pallocated_memory\000"
.LASF107:
	.ascii	"we_have_crossed_midnight_so_clear_the_error_counts\000"
.LASF304:
	.ascii	"LIGHTS_build_data_to_send\000"
.LASF394:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF194:
	.ascii	"CHAIN_MEMBERS_SHARED_STRUCT\000"
.LASF329:
	.ascii	"poutput_index_0\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF24:
	.ascii	"MIST_DLINK_TYPE\000"
.LASF376:
	.ascii	"GuiVar_LightsStartTimeEnabled2\000"
.LASF112:
	.ascii	"since_the_scan__master__number_of_clean_token_respo"
	.ascii	"nses_rcvd\000"
.LASF37:
	.ascii	"group_identity_number\000"
.LASF38:
	.ascii	"description\000"
.LASF77:
	.ascii	"port_a_raveon_radio_type\000"
.LASF337:
	.ascii	"LIGHTS_get_physically_available\000"
.LASF185:
	.ascii	"ununsed_uns8_1\000"
.LASF299:
	.ascii	"nm_LIGHTS_set_default_values\000"
.LASF286:
	.ascii	"lbitfield_of_changes\000"
.LASF140:
	.ascii	"token_date_time\000"
.LASF162:
	.ascii	"dls_eligible_to_fall_back\000"
.LASF359:
	.ascii	"nm_LIGHTS_update_pending_change_bits\000"
.LASF222:
	.ascii	"waiting_for_budget_report_data_response\000"
.LASF135:
	.ascii	"packets_waiting_for_token\000"
.LASF354:
	.ascii	"tptr\000"
.LASF52:
	.ascii	"WHATS_INSTALLED_STRUCT\000"
.LASF177:
	.ascii	"write_tpmicro_file_in_new_file_format\000"
.LASF4:
	.ascii	"UNS_8\000"
.LASF407:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF139:
	.ascii	"flag_update_date_time\000"
.LASF351:
	.ascii	"pbox_index_ptr\000"
.LASF97:
	.ascii	"CHANGE_TRACKING_FOR_COMMUNICATIONS_STRUCT\000"
.LASF230:
	.ascii	"mobile_seconds_since_last_command\000"
.LASF331:
	.ascii	"pindex_0\000"
.LASF146:
	.ascii	"next_available\000"
.LASF242:
	.ascii	"hub_packet_activity_timer\000"
.LASF231:
	.ascii	"waiting_for_firmware_version_check_response\000"
.LASF89:
	.ascii	"PURCHASED_OPTIONS_STRUCT\000"
.LASF170:
	.ascii	"dont_use_et_gage_today\000"
.LASF202:
	.ascii	"CONTROLLER_INITIATED_MESSAGE_TRANSMITTING\000"
.LASF247:
	.ascii	"the_crc\000"
.LASF353:
	.ascii	"LIGHTS_clean_house_processing\000"
.LASF375:
	.ascii	"GuiVar_LightsStartTimeEnabled1\000"
.LASF173:
	.ascii	"remaining_gage_pulses\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF288:
	.ascii	"lmatching_light\000"
.LASF357:
	.ascii	"LIGHTS_on_all_lights_set_or_clear_commserver_change"
	.ascii	"_bits\000"
.LASF73:
	.ascii	"option_FL\000"
.LASF44:
	.ascii	"stations\000"
.LASF319:
	.ascii	"plight_index_0\000"
.LASF275:
	.ascii	"plight_stop_time_1\000"
.LASF277:
	.ascii	"plight_stop_time_2\000"
.LASF219:
	.ascii	"waiting_for_station_report_data_response\000"
.LASF60:
	.ascii	"box_index_0\000"
.LASF119:
	.ascii	"pending_device_exchange_request\000"
.LASF236:
	.ascii	"waiting_for_asked_commserver_if_there_is_pdata_for_"
	.ascii	"us_response\000"
.LASF269:
	.ascii	"pindex\000"
.LASF309:
	.ascii	"lbitfield_of_changes_to_use_to_determine_what_to_se"
	.ascii	"nd\000"
.LASF283:
	.ascii	"llight\000"
.LASF25:
	.ascii	"et_inches_u16_10000u\000"
.LASF217:
	.ascii	"waiting_for_check_for_updates_response\000"
.LASF174:
	.ascii	"clear_runaway_gage\000"
.LASF296:
	.ascii	"pfrom_revision\000"
.LASF149:
	.ascii	"pending_first_to_send\000"
.LASF330:
	.ascii	"LIGHTS_get_light_at_this_index\000"
.LASF142:
	.ascii	"flowsense_device_establish_connection_timer\000"
.LASF210:
	.ascii	"waiting_for_registration_response\000"
.LASF303:
	.ascii	"lnext_light\000"
.LASF63:
	.ascii	"daily_schedule\000"
.LASF347:
	.ascii	"pchange_reason\000"
.LASF127:
	.ascii	"device_exchange_state\000"
.LASF180:
	.ascii	"commserver_monthly_xmit_bytes\000"
.LASF266:
	.ascii	"nm_LIGHTS_set_physically_available\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF99:
	.ascii	"DATE_TIME_TOKEN_STRUCT\000"
.LASF50:
	.ascii	"dash_m_card_type\000"
.LASF79:
	.ascii	"port_a_freewave_sr_set_for_repeater\000"
.LASF366:
	.ascii	"GuiVar_GroupName\000"
.LASF349:
	.ascii	"llight_ptr\000"
.LASF71:
	.ascii	"stop_time_2_in_seconds\000"
.LASF240:
	.ascii	"msgs_to_send_queue\000"
.LASF61:
	.ascii	"output_index_0\000"
.LASF227:
	.ascii	"send_rain_indication_timer\000"
.LASF131:
	.ascii	"timer_message_resp\000"
.LASF102:
	.ascii	"chain_is_down\000"
.LASF167:
	.ascii	"sync_the_et_rain_tables\000"
.LASF381:
	.ascii	"GuiVar_StationInfoControllerName\000"
.LASF305:
	.ascii	"pmem_used_so_far\000"
.LASF251:
	.ascii	"__set_bits_on_all_groups_to_cause_distribution_in_t"
	.ascii	"he_next_token\000"
.LASF279:
	.ascii	"plight_created\000"
.LASF110:
	.ascii	"is_a_NEW_member_in_the_chain\000"
.LASF228:
	.ascii	"waiting_for_mobile_status_response\000"
.LASF45:
	.ascii	"lights\000"
.LASF301:
	.ascii	"nm_LIGHTS_create_new_group\000"
.LASF33:
	.ascii	"xSemaphoreHandle\000"
.LASF30:
	.ascii	"long int\000"
.LASF144:
	.ascii	"COMM_MNGR_STRUCT\000"
.LASF374:
	.ascii	"GuiVar_LightsStartTime2InMinutes\000"
.LASF233:
	.ascii	"waiting_for_pdata_response\000"
.LASF9:
	.ascii	"UNS_32\000"
.LASF226:
	.ascii	"waiting_for_rain_indication_response\000"
.LASF166:
	.ascii	"rain\000"
.LASF367:
	.ascii	"GuiVar_itmGroupName\000"
.LASF47:
	.ascii	"weather_terminal_present\000"
.LASF315:
	.ascii	"LIGHTS_copy_guivars_to_daily_schedule\000"
.LASF187:
	.ascii	"hub_needs_to_distribute_tpmicro_binary\000"
.LASF82:
	.ascii	"port_b_freewave_sr_set_for_repeater\000"
.LASF111:
	.ascii	"since_the_scan__master__number_of_clean_tokens_made"
	.ascii	"\000"
.LASF95:
	.ascii	"distribute_changes_to_slave\000"
.LASF355:
	.ascii	"LIGHTS_set_not_physically_available_based_upon_comm"
	.ascii	"unication_scan_results\000"
.LASF373:
	.ascii	"GuiVar_LightsStartTime1InMinutes\000"
.LASF96:
	.ascii	"send_changes_to_master\000"
.LASF265:
	.ascii	"plight_output_index\000"
.LASF58:
	.ascii	"LIGHT_STRUCT\000"
.LASF323:
	.ascii	"LIGHTS_extract_and_store_group_name_from_GuiVars\000"
.LASF136:
	.ascii	"incoming_messages_or_packets\000"
.LASF403:
	.ascii	"chain_sync_file_pertinants\000"
.LASF108:
	.ascii	"start_a_scan_at_the_next_opportunity\000"
.LASF68:
	.ascii	"start_time_1_in_seconds\000"
.LASF150:
	.ascii	"pending_first_to_send_in_use\000"
.LASF138:
	.ascii	"timer_commserver_msg_receipt_error\000"
.LASF20:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF105:
	.ascii	"timer_token_arrival\000"
.LASF393:
	.ascii	"comm_mngr\000"
.LASF327:
	.ascii	"LIGHTS_get_index_using_ptr_to_light_struct\000"
.LASF256:
	.ascii	"pgenerate_change_line_bool\000"
.LASF392:
	.ascii	"g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY\000"
.LASF133:
	.ascii	"token_rate_timer_has_timed_out\000"
.LASF23:
	.ascii	"pListHdr\000"
.LASF321:
	.ascii	"LIGHTS_copy_light_struct_into_guivars\000"
.LASF235:
	.ascii	"waiting_for_firmware_version_check_before_asking_fo"
	.ascii	"r_pdata_response\000"
.LASF81:
	.ascii	"port_b_freewave_lr_set_for_repeater\000"
.LASF104:
	.ascii	"timer_rescan\000"
.LASF129:
	.ascii	"device_exchange_saved_comm_mngr_mode\000"
.LASF229:
	.ascii	"send_mobile_status_timer\000"
.LASF372:
	.ascii	"GuiVar_LightsOutputIndex_0\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF274:
	.ascii	"nm_LIGHTS_set_stop_time_1\000"
.LASF326:
	.ascii	"pindex_0_i16\000"
.LASF383:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF156:
	.ascii	"rain_shutdown_rcvd_from_commserver_uns32\000"
.LASF234:
	.ascii	"pdata_timer\000"
.LASF368:
	.ascii	"GuiVar_LightIsEnergized\000"
.LASF204:
	.ascii	"response_timer\000"
.LASF338:
	.ascii	"LIGHTS_get_start_time_1\000"
.LASF339:
	.ascii	"LIGHTS_get_start_time_2\000"
.LASF189:
	.ascii	"expansion\000"
.LASF264:
	.ascii	"nm_LIGHTS_set_output_index\000"
.LASF32:
	.ascii	"xQueueHandle\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF54:
	.ascii	"dlen\000"
.LASF317:
	.ascii	"LIGHTS_copy_daily_schedule_to_guivars\000"
.LASF51:
	.ascii	"two_wire_terminal_present\000"
.LASF224:
	.ascii	"waiting_for_weather_data_receipt_response\000"
.LASF84:
	.ascii	"unused_13\000"
.LASF85:
	.ascii	"unused_14\000"
.LASF86:
	.ascii	"unused_15\000"
.LASF159:
	.ascii	"RAIN_STATE\000"
.LASF363:
	.ascii	"pff_name\000"
.LASF244:
	.ascii	"ff_next_file_crc_to_send_0\000"
.LASF199:
	.ascii	"frcs_ptr\000"
.LASF106:
	.ascii	"scans_while_chain_is_down\000"
.LASF190:
	.ascii	"WEATHER_PRESERVES_STRUCT\000"
.LASF352:
	.ascii	"nm_LIGHTS_get_prev_available_light\000"
.LASF294:
	.ascii	"nm_LIGHTS_store_changes\000"
.LASF379:
	.ascii	"GuiVar_LightsStopTimeEnabled1\000"
.LASF380:
	.ascii	"GuiVar_LightsStopTimeEnabled2\000"
.LASF101:
	.ascii	"state\000"
.LASF14:
	.ascii	"BITFIELD_BOOL\000"
.LASF245:
	.ascii	"clean_tokens_since_change_detected_or_file_save\000"
.LASF151:
	.ascii	"when_to_send_timer\000"
.LASF65:
	.ascii	"changes_to_distribute_to_slaves\000"
.LASF318:
	.ascii	"LIGHTS_populate_group_name\000"
.LASF307:
	.ascii	"preason_data_is_being_built\000"
.LASF334:
	.ascii	"poutput_index\000"
.LASF122:
	.ascii	"main_app_firmware_up_to_date\000"
.LASF238:
	.ascii	"waiting_for_the_no_more_messages_msg_response\000"
.LASF201:
	.ascii	"data_packet_message_id\000"
.LASF48:
	.ascii	"dash_m_card_present\000"
.LASF389:
	.ascii	"light_list_hdr\000"
.LASF193:
	.ascii	"box_configuration\000"
.LASF297:
	.ascii	"init_file_LIGHTS\000"
.LASF402:
	.ascii	"cscs\000"
.LASF7:
	.ascii	"INT_16\000"
.LASF198:
	.ascii	"activity_flag_ptr\000"
.LASF3:
	.ascii	"signed char\000"
.LASF62:
	.ascii	"light_physically_available\000"
.LASF109:
	.ascii	"start_a_scan_captured_reason\000"
.LASF396:
	.ascii	"comm_mngr_recursive_MUTEX\000"
.LASF399:
	.ascii	"chain\000"
.LASF382:
	.ascii	"GuiFont_LanguageActive\000"
.LASF250:
	.ascii	"__crc_calculation_function_ptr\000"
.LASF218:
	.ascii	"waiting_for_station_history_response\000"
.LASF263:
	.ascii	"pbox_index\000"
.LASF260:
	.ascii	"pchange_bitfield_to_set\000"
.LASF163:
	.ascii	"dls_after_fall_back_ignore_sxs\000"
.LASF278:
	.ascii	"ptemporary_group\000"
.LASF216:
	.ascii	"waiting_for_moisture_sensor_recording_response\000"
.LASF214:
	.ascii	"waiting_for_flow_recording_response\000"
.LASF401:
	.ascii	"cics\000"
.LASF157:
	.ascii	"inhibit_irrigation_have_crossed_the_minimum\000"
.LASF191:
	.ascii	"double\000"
.LASF41:
	.ascii	"card_present\000"
.LASF253:
	.ascii	"CHAIN_SYNC_FILE_PERTINANTS\000"
.LASF320:
	.ascii	"long_format\000"
.LASF113:
	.ascii	"since_the_scan__slave__number_of_clean_tokens_rcvd\000"
.LASF281:
	.ascii	"pbitfield_of_changes\000"
.LASF292:
	.ascii	"lsize_of_bitfield\000"
.LASF343:
	.ascii	"numeric_date\000"
.LASF398:
	.ascii	"weather_preserves\000"
.LASF328:
	.ascii	"nm_LIGHTS_get_pointer_to_light_struct_with_this_box"
	.ascii	"_and_output_index\000"
.LASF346:
	.ascii	"LIGHTS_get_change_bits_ptr\000"
.LASF280:
	.ascii	"pchanges_received_from\000"
.LASF88:
	.ascii	"size_of_the_union\000"
.LASF213:
	.ascii	"waiting_for_alerts_response\000"
.LASF386:
	.ascii	"g_GROUP_creating_new\000"
.LASF22:
	.ascii	"pNext\000"
.LASF406:
	.ascii	"g_LIGHTS_initial_date_time\000"
.LASF186:
	.ascii	"commserver_monthly_mobile_status_updates_bytes\000"
.LASF291:
	.ascii	"loutput_index\000"
.LASF128:
	.ascii	"device_exchange_device_index\000"
.LASF241:
	.ascii	"queued_msgs_polling_timer\000"
.LASF243:
	.ascii	"CONTROLLER_INITIATED_CONTROL_STRUCT\000"
.LASF123:
	.ascii	"tpmicro_firmware_up_to_date\000"
.LASF293:
	.ascii	"llight_created\000"
.LASF384:
	.ascii	"GuiFont_DecimalChar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
