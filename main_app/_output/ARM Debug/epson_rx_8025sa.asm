	.file	"epson_rx_8025sa.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	Epson_RTC_Queue
	.section	.bss.Epson_RTC_Queue,"aw",%nobits
	.align	2
	.type	Epson_RTC_Queue, %object
	.size	Epson_RTC_Queue, 4
Epson_RTC_Queue:
	.space	4
	.global	TD_CHECK_queue
	.section	.bss.TD_CHECK_queue,"aw",%nobits
	.align	2
	.type	TD_CHECK_queue, %object
	.size	TD_CHECK_queue, 4
TD_CHECK_queue:
	.space	4
	.section	.bss.i2c1cs,"aw",%nobits
	.align	2
	.type	i2c1cs, %object
	.size	i2c1cs, 56
i2c1cs:
	.space	56
	.section	.bss.local_dt,"aw",%nobits
	.align	2
	.type	local_dt, %object
	.size	local_dt, 20
local_dt:
	.space	20
	.section	.bss.Time_and_date_MUTEX,"aw",%nobits
	.align	2
	.type	Time_and_date_MUTEX, %object
	.size	Time_and_date_MUTEX, 4
Time_and_date_MUTEX:
	.space	4
	.section	.text.hextobcd,"ax",%progbits
	.align	2
	.global	hextobcd
	.type	hextobcd, %function
hextobcd:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/rtc/epson_rx_8025sa.c"
	.loc 1 163 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #12
.LCFI2:
	str	r0, [fp, #-12]
	.loc 1 165 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 167 0
	mov	r3, #0
	strb	r3, [fp, #-1]
	b	.L2
.L5:
	.loc 1 169 0
	ldrb	r3, [fp, #-1]	@ zero_extendqisi2
	mov	r3, r3, lsr #1
	and	r3, r3, #255
	mov	r2, r3
	sub	r1, fp, #8
	add	r0, r1, r2
	sub	r2, fp, #8
	add	r3, r2, r3
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r3, [fp, #-1]	@ zero_extendqisi2
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L3
	.loc 1 169 0 is_stmt 0 discriminator 1
	ldr	r1, [fp, #-12]
	ldr	r3, .L6
	umull	r2, r3, r1, r3
	mov	r2, r3, lsr #3
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	and	r3, r2, #255
	mov	r3, r3, asl #4
	and	r3, r3, #255
	b	.L4
.L3:
	.loc 1 169 0 discriminator 2
	ldr	r1, [fp, #-12]
	ldr	r3, .L6
	umull	r2, r3, r1, r3
	mov	r2, r3, lsr #3
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	and	r3, r2, #255
	and	r3, r3, #15
	and	r3, r3, #255
.L4:
	.loc 1 169 0 discriminator 3
	orr	r3, ip, r3
	and	r3, r3, #255
	strb	r3, [r0, #0]
	.loc 1 170 0 is_stmt 1 discriminator 3
	ldr	r2, [fp, #-12]
	ldr	r3, .L6
	umull	r1, r3, r2, r3
	mov	r3, r3, lsr #3
	str	r3, [fp, #-12]
	.loc 1 167 0 discriminator 3
	ldrb	r3, [fp, #-1]
	add	r3, r3, #1
	strb	r3, [fp, #-1]
.L2:
	.loc 1 167 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L5
	.loc 1 173 0 is_stmt 1
	ldr	r3, [fp, #-8]
	.loc 1 174 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L7:
	.align	2
.L6:
	.word	-858993459
.LFE0:
	.size	hextobcd, .-hextobcd
	.section	.text.epson_periodic_isr,"ax",%progbits
	.align	2
	.global	epson_periodic_isr
	.type	epson_periodic_isr, %function
epson_periodic_isr:
.LFB1:
	.loc 1 178 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #28
.LCFI5:
	.loc 1 183 0
	mov	r3, #0
	str	r3, [fp, #-32]
	.loc 1 186 0
	mov	r3, #68
	str	r3, [fp, #-28]
	.loc 1 188 0
	ldr	r3, .L10
	ldr	r1, [r3, #0]
	sub	r2, fp, #28
	sub	r3, fp, #32
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #1
	bl	xQueueGenericSendFromISR
	.loc 1 192 0
	ldr	r3, [fp, #-32]
	cmp	r3, #1
	bne	.L8
.LBB2:
	.loc 1 194 0
	bl	vTaskSwitchContext
.L8:
.LBE2:
	.loc 1 196 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L11:
	.align	2
.L10:
	.word	Epson_RTC_Queue
.LFE1:
	.size	epson_periodic_isr, .-epson_periodic_isr
	.section	.text.epson_i2c1_write_isr,"ax",%progbits
	.align	2
	.global	epson_i2c1_write_isr
	.type	epson_i2c1_write_isr, %function
epson_i2c1_write_isr:
.LFB2:
	.loc 1 200 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #32
.LCFI8:
	.loc 1 201 0
	ldr	r3, .L20
	str	r3, [fp, #-8]
	.loc 1 207 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-28]
	.loc 1 209 0
	ldr	r3, [fp, #-28]
	and	r3, r3, #6
	cmp	r3, #0
	beq	.L13
	.loc 1 212 0
	mov	r3, #17
	str	r3, [fp, #-32]
	.loc 1 215 0
	ldr	r3, .L20+4
	ldr	r1, [r3, #0]
	sub	r2, fp, #32
	sub	r3, fp, #36
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #0
	bl	xQueueGenericSendFromISR
	.loc 1 218 0
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 1 220 0
	ldr	r3, .L20+8
	mov	r2, #0
	str	r2, [r3, #48]
	b	.L12
.L13:
	.loc 1 223 0
	ldr	r3, [fp, #-28]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L19
	.loc 1 226 0
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 1 228 0
	ldr	r3, .L20+8
	mov	r2, #0
	str	r2, [r3, #48]
	.loc 1 231 0
	ldr	r3, .L20+8
	ldr	r3, [r3, #52]
	str	r3, [fp, #-32]
	.loc 1 235 0
	ldr	r3, .L20+4
	ldr	r1, [r3, #0]
	sub	r2, fp, #32
	sub	r3, fp, #36
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #1
	bl	xQueueGenericSendFromISR
	b	.L12
.L18:
	.loc 1 242 0
	ldr	r3, .L20+8
	ldr	r3, [r3, #32]
	sub	r2, r3, #1
	ldr	r3, .L20+8
	str	r2, [r3, #32]
	.loc 1 244 0
	ldr	r3, .L20+8
	ldr	r3, [r3, #32]
	cmp	r3, #0
	bne	.L17
	.loc 1 246 0
	ldr	r3, [fp, #-8]
	mov	r2, #7
	str	r2, [r3, #8]
	.loc 1 247 0
	ldr	r3, .L20+8
	ldrb	r3, [r3, #0]
	and	r3, r3, #255
	orr	r2, r3, #512
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	b	.L16
.L17:
	.loc 1 251 0
	ldr	r3, .L20+8
	ldr	r3, [r3, #32]
	ldr	r2, .L20+8
	ldrb	r3, [r2, r3]
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	b	.L16
.L19:
	.loc 1 240 0
	mov	r0, r0	@ nop
.L16:
	.loc 1 240 0 is_stmt 0 discriminator 1
	ldr	r3, .L20+8
	ldr	r3, [r3, #32]
	cmp	r3, #0
	beq	.L12
	.loc 1 240 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	and	r3, r3, #1024
	cmp	r3, #0
	beq	.L18
.L12:
	.loc 1 255 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L21:
	.align	2
.L20:
	.word	1074397184
	.word	Epson_RTC_Queue
	.word	i2c1cs
.LFE2:
	.size	epson_i2c1_write_isr, .-epson_i2c1_write_isr
	.section	.text.epson_i2c1_read_isr,"ax",%progbits
	.align	2
	.global	epson_i2c1_read_isr
	.type	epson_i2c1_read_isr, %function
epson_i2c1_read_isr:
.LFB3:
	.loc 1 259 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #32
.LCFI11:
	.loc 1 260 0
	ldr	r3, .L37
	str	r3, [fp, #-8]
	.loc 1 266 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	str	r3, [fp, #-28]
	.loc 1 268 0
	ldr	r3, [fp, #-28]
	and	r3, r3, #6
	cmp	r3, #0
	beq	.L23
	.loc 1 271 0
	mov	r3, #17
	str	r3, [fp, #-32]
	.loc 1 274 0
	ldr	r3, .L37+4
	ldr	r1, [r3, #0]
	sub	r2, fp, #32
	sub	r3, fp, #36
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #0
	bl	xQueueGenericSendFromISR
	.loc 1 277 0
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 1 279 0
	ldr	r3, .L37+8
	mov	r2, #0
	str	r2, [r3, #48]
	b	.L22
.L23:
	.loc 1 282 0
	ldr	r3, [fp, #-28]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L35
	.loc 1 294 0
	b	.L26
.L28:
	.loc 1 296 0
	ldr	r3, .L37+8
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #0]
	and	r1, r2, #255
	ldr	r0, .L37+8
	mov	r2, #16
	add	r0, r0, r3
	add	r2, r0, r2
	strb	r1, [r2, #0]
	add	r2, r3, #1
	ldr	r3, .L37+8
	str	r2, [r3, #40]
	.loc 1 298 0
	ldr	r3, .L37+8
	ldr	r3, [r3, #36]
	sub	r2, r3, #1
	ldr	r3, .L37+8
	str	r2, [r3, #36]
.L26:
	.loc 1 294 0 discriminator 1
	ldr	r3, .L37+8
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L27
	.loc 1 294 0 is_stmt 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	and	r3, r3, #512
	cmp	r3, #0
	beq	.L28
.L27:
	.loc 1 302 0 is_stmt 1
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 1 304 0
	ldr	r3, .L37+8
	mov	r2, #0
	str	r2, [r3, #48]
	.loc 1 307 0
	ldr	r3, .L37+8
	ldr	r3, [r3, #52]
	str	r3, [fp, #-32]
	.loc 1 309 0
	sub	r3, fp, #32
	add	r3, r3, #8
	mov	r0, r3
	ldr	r1, .L37+12
	mov	r2, #16
	bl	memcpy
	.loc 1 313 0
	ldr	r3, .L37+4
	ldr	r1, [r3, #0]
	sub	r2, fp, #32
	sub	r3, fp, #36
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #1
	bl	xQueueGenericSendFromISR
	b	.L22
.L31:
	.loc 1 320 0
	ldr	r3, .L37+8
	ldr	r3, [r3, #40]
	ldr	r2, [fp, #-8]
	ldr	r2, [r2, #0]
	and	r1, r2, #255
	ldr	r0, .L37+8
	mov	r2, #16
	add	r0, r0, r3
	add	r2, r0, r2
	strb	r1, [r2, #0]
	add	r2, r3, #1
	ldr	r3, .L37+8
	str	r2, [r3, #40]
	.loc 1 322 0
	ldr	r3, .L37+8
	ldr	r3, [r3, #36]
	sub	r2, r3, #1
	ldr	r3, .L37+8
	str	r2, [r3, #36]
	b	.L29
.L35:
	.loc 1 318 0
	mov	r0, r0	@ nop
.L29:
	.loc 1 318 0 is_stmt 0 discriminator 1
	ldr	r3, .L37+8
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L36
	.loc 1 318 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	and	r3, r3, #512
	cmp	r3, #0
	beq	.L31
	.loc 1 326 0 is_stmt 1
	b	.L36
.L34:
	.loc 1 328 0
	ldr	r3, .L37+8
	ldr	r3, [r3, #44]
	sub	r2, r3, #1
	ldr	r3, .L37+8
	str	r2, [r3, #44]
	.loc 1 330 0
	ldr	r3, .L37+8
	ldr	r3, [r3, #44]
	cmp	r3, #0
	bne	.L33
	.loc 1 332 0
	ldr	r3, [fp, #-8]
	mov	r2, #512
	str	r2, [r3, #0]
	b	.L32
.L33:
	.loc 1 336 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L32
.L36:
	.loc 1 326 0
	mov	r0, r0	@ nop
.L32:
	.loc 1 326 0 is_stmt 0 discriminator 1
	ldr	r3, .L37+8
	ldr	r3, [r3, #44]
	cmp	r3, #0
	beq	.L22
	.loc 1 326 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	and	r3, r3, #1024
	cmp	r3, #0
	beq	.L34
.L22:
	.loc 1 341 0 is_stmt 1
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L38:
	.align	2
.L37:
	.word	1074397184
	.word	Epson_RTC_Queue
	.word	i2c1cs
	.word	i2c1cs+16
.LFE3:
	.size	epson_i2c1_read_isr, .-epson_i2c1_read_isr
	.section	.text._epson_read_REG_F,"ax",%progbits
	.align	2
	.global	_epson_read_REG_F
	.type	_epson_read_REG_F, %function
_epson_read_REG_F:
.LFB4:
	.loc 1 345 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #8
.LCFI14:
	.loc 1 346 0
	ldr	r3, .L40
	str	r3, [fp, #-8]
	.loc 1 352 0
	mov	r0, #51
	bl	xDisable_ISR
	.loc 1 354 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #51
	mov	r1, #14
	mov	r2, #1
	ldr	r3, .L40+4
	bl	xSetISR_Vector
	.loc 1 356 0
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 1 358 0
	ldr	r3, .L40+8
	mov	r2, #1
	str	r2, [r3, #36]
	.loc 1 360 0
	ldr	r3, .L40+8
	mov	r2, #0
	str	r2, [r3, #40]
	.loc 1 362 0
	ldr	r3, .L40+8
	mov	r2, #0
	str	r2, [r3, #44]
	.loc 1 364 0
	ldr	r3, .L40+8
	mov	r2, #1
	str	r2, [r3, #48]
	.loc 1 366 0
	ldr	r3, .L40+8
	mov	r2, #34
	str	r2, [r3, #52]
	.loc 1 368 0
	ldr	r3, [fp, #-8]
	mov	r2, #111
	str	r2, [r3, #8]
	.loc 1 370 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L40+12
	str	r2, [r3, #0]
	.loc 1 372 0
	ldr	r3, [fp, #-8]
	mov	r2, #512
	str	r2, [r3, #0]
	.loc 1 374 0
	mov	r0, #51
	bl	xEnable_ISR
	.loc 1 375 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L41:
	.align	2
.L40:
	.word	1074397184
	.word	epson_i2c1_read_isr
	.word	i2c1cs
	.word	357
.LFE4:
	.size	_epson_read_REG_F, .-_epson_read_REG_F
	.section	.text._epson_read_the_time_and_date,"ax",%progbits
	.align	2
	.global	_epson_read_the_time_and_date
	.type	_epson_read_the_time_and_date, %function
_epson_read_the_time_and_date:
.LFB5:
	.loc 1 379 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #8
.LCFI17:
	.loc 1 380 0
	ldr	r3, .L43
	str	r3, [fp, #-8]
	.loc 1 387 0
	mov	r0, #51
	bl	xDisable_ISR
	.loc 1 389 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #51
	mov	r1, #14
	mov	r2, #1
	ldr	r3, .L43+4
	bl	xSetISR_Vector
	.loc 1 391 0
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 1 393 0
	ldr	r3, .L43+8
	mov	r2, #8
	str	r2, [r3, #36]
	.loc 1 395 0
	ldr	r3, .L43+8
	mov	r2, #0
	str	r2, [r3, #40]
	.loc 1 397 0
	ldr	r3, .L43+8
	mov	r2, #6
	str	r2, [r3, #44]
	.loc 1 400 0
	ldr	r3, .L43+8
	mov	r2, #1
	str	r2, [r3, #48]
	.loc 1 402 0
	ldr	r3, .L43+8
	mov	r2, #85
	str	r2, [r3, #52]
	.loc 1 404 0
	ldr	r3, [fp, #-8]
	mov	r2, #111
	str	r2, [r3, #8]
	.loc 1 406 0
	ldr	r3, [fp, #-8]
	ldr	r2, .L43+12
	str	r2, [r3, #0]
	.loc 1 408 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 410 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 412 0
	mov	r0, #51
	bl	xEnable_ISR
	.loc 1 413 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L44:
	.align	2
.L43:
	.word	1074397184
	.word	epson_i2c1_read_isr
	.word	i2c1cs
	.word	357
.LFE5:
	.size	_epson_read_the_time_and_date, .-_epson_read_the_time_and_date
	.section	.text._epson_set_the_time_and_date,"ax",%progbits
	.align	2
	.global	_epson_set_the_time_and_date
	.type	_epson_set_the_time_and_date, %function
_epson_set_the_time_and_date:
.LFB6:
	.loc 1 417 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #12
.LCFI20:
	str	r0, [fp, #-12]
	.loc 1 424 0
	ldr	r3, .L46
	str	r3, [fp, #-8]
	.loc 1 430 0
	mov	r0, #51
	bl	xDisable_ISR
	.loc 1 432 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #51
	mov	r1, #14
	mov	r2, #1
	ldr	r3, .L46+4
	bl	xSetISR_Vector
	.loc 1 435 0
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 1 438 0
	ldr	r3, .L46+8
	mov	r2, #119
	str	r2, [r3, #52]
	.loc 1 441 0
	ldr	r3, .L46+8
	mov	r2, #0
	strb	r2, [r3, #7]
	.loc 1 443 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #8]	@ zero_extendqisi2
	mov	r0, r3
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L46+8
	strb	r2, [r3, #6]
	.loc 1 444 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #9]	@ zero_extendqisi2
	mov	r0, r3
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L46+8
	strb	r2, [r3, #5]
	.loc 1 445 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #10]	@ zero_extendqisi2
	mov	r0, r3
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L46+8
	strb	r2, [r3, #4]
	.loc 1 446 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #11]	@ zero_extendqisi2
	mov	r0, r3
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L46+8
	strb	r2, [r3, #3]
	.loc 1 447 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #12]	@ zero_extendqisi2
	mov	r0, r3
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L46+8
	strb	r2, [r3, #2]
	.loc 1 448 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #13]	@ zero_extendqisi2
	mov	r0, r3
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L46+8
	strb	r2, [r3, #1]
	.loc 1 449 0
	ldr	r3, [fp, #-12]
	ldrb	r3, [r3, #14]	@ zero_extendqisi2
	mov	r0, r3
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L46+8
	strb	r2, [r3, #0]
	.loc 1 451 0
	ldr	r3, .L46+8
	mov	r2, #8
	str	r2, [r3, #32]
	.loc 1 453 0
	ldr	r3, .L46+8
	mov	r2, #1
	str	r2, [r3, #48]
	.loc 1 455 0
	ldr	r3, [fp, #-8]
	mov	r2, #143
	str	r2, [r3, #8]
	.loc 1 457 0
	ldr	r3, [fp, #-8]
	mov	r2, #356
	str	r2, [r3, #0]
	.loc 1 460 0
	mov	r0, #51
	bl	xEnable_ISR
	.loc 1 461 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L47:
	.align	2
.L46:
	.word	1074397184
	.word	epson_i2c1_write_isr
	.word	i2c1cs
.LFE6:
	.size	_epson_set_the_time_and_date, .-_epson_set_the_time_and_date
	.section	.text._epson_rtc_cold_start,"ax",%progbits
	.align	2
	.global	_epson_rtc_cold_start
	.type	_epson_rtc_cold_start, %function
_epson_rtc_cold_start:
.LFB7:
	.loc 1 465 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #8
.LCFI23:
	.loc 1 472 0
	ldr	r3, .L49
	str	r3, [fp, #-8]
	.loc 1 478 0
	mov	r0, #51
	bl	xDisable_ISR
	.loc 1 480 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #51
	mov	r1, #14
	mov	r2, #1
	ldr	r3, .L49+4
	bl	xSetISR_Vector
	.loc 1 483 0
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 1 486 0
	ldr	r3, .L49+8
	mov	r2, #51
	str	r2, [r3, #52]
	.loc 1 489 0
	ldr	r3, .L49+8
	mvn	r2, #31
	strb	r2, [r3, #9]
	.loc 1 490 0
	ldr	r3, .L49+8
	mov	r2, #51
	strb	r2, [r3, #8]
	.loc 1 491 0
	ldr	r3, .L49+8
	mov	r2, #40
	strb	r2, [r3, #7]
	.loc 1 493 0
	mov	r0, #0
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L49+8
	strb	r2, [r3, #6]
	.loc 1 494 0
	mov	r0, #0
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L49+8
	strb	r2, [r3, #5]
	.loc 1 495 0
	mov	r0, #12
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L49+8
	strb	r2, [r3, #4]
	.loc 1 496 0
	mov	r0, #0
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L49+8
	strb	r2, [r3, #3]
	.loc 1 497 0
	mov	r0, #1
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L49+8
	strb	r2, [r3, #2]
	.loc 1 498 0
	mov	r0, #1
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L49+8
	strb	r2, [r3, #1]
	.loc 1 499 0
	mov	r0, #12
	bl	hextobcd
	mov	r3, r0
	and	r2, r3, #255
	ldr	r3, .L49+8
	strb	r2, [r3, #0]
	.loc 1 501 0
	ldr	r3, .L49+8
	mov	r2, #10
	str	r2, [r3, #32]
	.loc 1 503 0
	ldr	r3, .L49+8
	mov	r2, #1
	str	r2, [r3, #48]
	.loc 1 505 0
	ldr	r3, [fp, #-8]
	mov	r2, #143
	str	r2, [r3, #8]
	.loc 1 507 0
	ldr	r3, [fp, #-8]
	mov	r2, #356
	str	r2, [r3, #0]
	.loc 1 510 0
	mov	r0, #51
	bl	xEnable_ISR
	.loc 1 511 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L50:
	.align	2
.L49:
	.word	1074397184
	.word	epson_i2c1_write_isr
	.word	i2c1cs
.LFE7:
	.size	_epson_rtc_cold_start, .-_epson_rtc_cold_start
	.global	__udivsi3
	.section	.text._init_epson_i2c_channel,"ax",%progbits
	.align	2
	.global	_init_epson_i2c_channel
	.type	_init_epson_i2c_channel, %function
_init_epson_i2c_channel:
.LFB8:
	.loc 1 515 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #20
.LCFI26:
	.loc 1 520 0
	ldr	r3, .L52
	str	r3, [fp, #-8]
	.loc 1 524 0
	mov	r0, #11
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	.loc 1 527 0
	mov	r3, #50
	str	r3, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 528 0
	mov	r0, #4
	bl	clkpwr_get_base_clock_rate
	mov	r3, r0
	str	r3, [fp, #-12]
	.loc 1 529 0
	ldr	r2, [fp, #-12]
	ldr	r1, [fp, #-16]
	ldr	r3, [fp, #-20]
	add	r3, r1, r3
	mov	r0, r2
	mov	r1, r3
	bl	__udivsi3
	mov	r3, r0
	ldr	r2, [fp, #-20]
	mul	r3, r2, r3
	mov	r2, r3, lsr #6
	ldr	r3, .L52+4
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #6
	ldr	r3, [fp, #-8]
	str	r2, [r3, #16]
	.loc 1 530 0
	ldr	r2, [fp, #-12]
	ldr	r1, [fp, #-16]
	ldr	r3, [fp, #-20]
	add	r3, r1, r3
	mov	r0, r2
	mov	r1, r3
	bl	__udivsi3
	mov	r3, r0
	ldr	r2, [fp, #-16]
	mul	r3, r2, r3
	mov	r2, r3, lsr #6
	ldr	r3, .L52+4
	umull	r1, r3, r2, r3
	mov	r2, r3, lsr #6
	ldr	r3, [fp, #-8]
	str	r2, [r3, #12]
	.loc 1 532 0
	mov	r0, #1
	mov	r1, #1
	bl	clkpwr_set_i2c_driver
	.loc 1 534 0
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 1 536 0
	ldr	r3, .L52+8
	mov	r2, #0
	str	r2, [r3, #48]
	.loc 1 539 0
	mov	r0, #51
	bl	xDisable_ISR
	.loc 1 543 0
	mov	r0, #90
	bl	xDisable_ISR
	.loc 1 548 0
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #90
	mov	r1, #15
	mov	r2, #3
	ldr	r3, .L52+12
	bl	xSetISR_Vector
	.loc 1 552 0
	bl	_epson_read_REG_F
	.loc 1 553 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L53:
	.align	2
.L52:
	.word	1074397184
	.word	87960931
	.word	i2c1cs
	.word	epson_periodic_isr
.LFE8:
	.size	_init_epson_i2c_channel, .-_init_epson_i2c_channel
	.section .rodata
	.align	2
.LC0:
	.ascii	"Epson RTC I2C: isr error detected\000"
	.align	2
.LC1:
	.ascii	"Epson RTC: cold start completed\000"
	.align	2
.LC2:
	.ascii	"RTC 1 hz error\000"
	.align	2
.LC3:
	.ascii	"TD_CHECK QUEUE OVERFLOW!\000"
	.align	2
.LC4:
	.ascii	"Unexpected EPSON RTC I2C delay!\000"
	.align	2
.LC5:
	.ascii	"EPSON RTC queue handle not valid!\000"
	.section	.text.EPSON_rtc_control_task,"ax",%progbits
	.align	2
	.global	EPSON_rtc_control_task
	.type	EPSON_rtc_control_task, %function
EPSON_rtc_control_task:
.LFB9:
	.loc 1 557 0
	@ args = 0, pretend = 0, frame = 92
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #92
.LCFI29:
	str	r0, [fp, #-96]
	.loc 1 574 0
	mov	r0, #10
	mov	r1, #24
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L79
	str	r2, [r3, #0]
	.loc 1 581 0
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r2, r0
	ldr	r3, .L79+4
	str	r2, [r3, #0]
	.loc 1 588 0
	mov	r0, #30
	mov	r1, #24
	mov	r2, #0
	bl	xQueueGenericCreate
	mov	r2, r0
	ldr	r3, .L79+8
	str	r2, [r3, #0]
	.loc 1 594 0
	bl	_init_epson_i2c_channel
	.loc 1 598 0
	mov	r3, #0
	str	r3, [fp, #-8]
.L77:
	.loc 1 604 0
	ldr	r3, .L79
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L55
	.loc 1 607 0
	ldr	r3, .L79
	ldr	r2, [r3, #0]
	sub	r3, fp, #48
	mov	r0, r2
	mov	r1, r3
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	mov	r3, r0
	cmp	r3, #1
	bne	.L56
.LBB3:
	.loc 1 609 0
	ldr	r3, [fp, #-48]
	cmp	r3, #51
	beq	.L60
	cmp	r3, #51
	bhi	.L64
	cmp	r3, #17
	beq	.L58
	cmp	r3, #34
	beq	.L59
	b	.L57
.L64:
	cmp	r3, #85
	beq	.L62
	cmp	r3, #102
	beq	.L63
	cmp	r3, #68
	beq	.L61
	b	.L57
.L58:
	.loc 1 612 0
	ldr	r0, .L79+12
	bl	Alert_Message
	.loc 1 613 0
	b	.L57
.L59:
	.loc 1 618 0
	ldrb	r3, [fp, #-40]
	strb	r3, [fp, #-13]
	.loc 1 620 0
	ldrb	r3, [fp, #-13]	@ zero_extendqisi2
	and	r3, r3, #112
	cmp	r3, #32
	beq	.L65
	.loc 1 624 0
	bl	_epson_rtc_cold_start
	.loc 1 631 0
	b	.L57
.L65:
	.loc 1 629 0
	mov	r0, #90
	bl	xEnable_ISR
	.loc 1 631 0
	b	.L57
.L60:
	.loc 1 634 0
	ldr	r0, .L79+16
	bl	Alert_Message
	.loc 1 636 0
	mov	r0, #90
	bl	xEnable_ISR
	.loc 1 637 0
	b	.L57
.L63:
	.loc 1 640 0
	sub	r3, fp, #48
	mov	r0, r3
	bl	_epson_set_the_time_and_date
	.loc 1 641 0
	b	.L57
.L61:
	.loc 1 644 0
	bl	_epson_read_the_time_and_date
	.loc 1 645 0
	b	.L57
.L62:
	.loc 1 651 0
	ldrb	r3, [fp, #-40]	@ zero_extendqisi2
	and	r3, r3, #112
	cmp	r3, #32
	beq	.L67
	.loc 1 653 0
	ldr	r0, .L79+20
	bl	Alert_Message
	.loc 1 835 0
	b	.L78
.L67:
	.loc 1 659 0
	ldr	r3, .L79+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 661 0
	ldrb	r3, [fp, #-39]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #255
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #2
	add	r3, r2, r3
	mov	r3, r3, asl #1
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldrb	r3, [fp, #-39]	@ zero_extendqisi2
	and	r3, r3, #15
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L79+24
	strh	r2, [r3, #16]	@ movhi
	.loc 1 662 0
	ldrb	r3, [fp, #-38]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #255
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #2
	add	r3, r2, r3
	mov	r3, r3, asl #1
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldrb	r3, [fp, #-38]	@ zero_extendqisi2
	and	r3, r3, #15
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L79+24
	strh	r2, [r3, #14]	@ movhi
	.loc 1 663 0
	ldrb	r3, [fp, #-37]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #255
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #2
	add	r3, r2, r3
	mov	r3, r3, asl #1
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldrb	r3, [fp, #-37]	@ zero_extendqisi2
	and	r3, r3, #15
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L79+24
	strh	r2, [r3, #12]	@ movhi
	.loc 1 665 0
	ldrb	r2, [fp, #-36]	@ zero_extendqisi2
	ldr	r3, .L79+24
	strb	r2, [r3, #18]
	.loc 1 666 0
	ldrb	r3, [fp, #-35]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #255
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #2
	add	r3, r2, r3
	mov	r3, r3, asl #1
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldrb	r3, [fp, #-35]	@ zero_extendqisi2
	and	r3, r3, #15
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L79+24
	strh	r2, [r3, #6]	@ movhi
	.loc 1 667 0
	ldrb	r3, [fp, #-34]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #255
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #2
	add	r3, r2, r3
	mov	r3, r3, asl #1
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldrb	r3, [fp, #-34]	@ zero_extendqisi2
	and	r3, r3, #15
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L79+24
	strh	r2, [r3, #8]	@ movhi
	.loc 1 668 0
	ldrb	r3, [fp, #-33]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #255
	mov	r2, r3	@ movhi
	mov	r2, r2, asl #2
	add	r3, r2, r3
	mov	r3, r3, asl #1
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldrb	r3, [fp, #-33]	@ zero_extendqisi2
	and	r3, r3, #15
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r2, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r3, r3, #2000
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L79+24
	strh	r2, [r3, #10]	@ movhi
	.loc 1 670 0
	ldr	r3, .L79+24
	ldrh	r3, [r3, #12]
	mov	r1, r3
	ldr	r3, .L79+24
	ldrh	r3, [r3, #14]
	mov	r2, r3
	ldr	r3, .L79+24
	ldrh	r3, [r3, #16]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	HMSToTime
	mov	r2, r0
	ldr	r3, .L79+24
	str	r2, [r3, #0]
	.loc 1 671 0
	ldr	r3, .L79+24
	ldrh	r3, [r3, #6]
	mov	r1, r3
	ldr	r3, .L79+24
	ldrh	r3, [r3, #8]
	mov	r2, r3
	ldr	r3, .L79+24
	ldrh	r3, [r3, #10]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	DMYToDate
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, .L79+24
	strh	r2, [r3, #4]	@ movhi
	.loc 1 674 0
	ldr	r3, .L79+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	.loc 1 682 0
	ldr	r3, .L79+28
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	.loc 1 702 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 706 0
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	bls	.L78
	.loc 1 708 0
	ldr	r3, .L79+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 718 0
	ldr	r3, .L79+24
	ldrh	r2, [r3, #4]
	ldr	r3, .L79+32
	ldrh	r3, [r3, #16]
	cmp	r2, r3
	beq	.L69
	.loc 1 720 0
	ldr	r3, .L79+32
	mov	r2, #1
	str	r2, [r3, #20]
	.loc 1 722 0
	ldr	r3, .L79+24
	ldrh	r2, [r3, #4]
	ldr	r3, .L79+32
	strh	r2, [r3, #16]	@ movhi
.L69:
	.loc 1 731 0
	ldr	r3, .L79+32
	ldr	r3, [r3, #24]
	cmp	r3, #1
	bne	.L70
	.loc 1 733 0
	ldr	r0, .L79+24
	ldr	r1, .L79+36
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	mov	r3, r0
	cmp	r3, #1
	bne	.L70
	.loc 1 735 0
	ldr	r3, .L79+32
	mov	r2, #0
	str	r2, [r3, #24]
.L70:
	.loc 1 740 0
	bl	NETWORK_CONFIG_get_is_dls_used_for_the_selected_time_zone
	mov	r3, r0
	cmp	r3, #1
	bne	.L71
	.loc 1 742 0
	ldr	r3, .L79+24
	ldr	r2, [r3, #0]
	ldr	r3, .L79+40
	cmp	r2, r3
	bne	.L72
	.loc 1 744 0
	ldr	r3, .L79+24
	ldrb	r3, [r3, #18]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L72
	.loc 1 746 0
	mov	r0, #1
	bl	NETWORK_CONFIG_get_dls_ptr
	str	r0, [fp, #-20]
	.loc 1 748 0
	mov	r0, #0
	bl	NETWORK_CONFIG_get_dls_ptr
	str	r0, [fp, #-24]
	.loc 1 751 0
	ldr	r3, .L79+24
	ldrh	r3, [r3, #8]
	mov	r2, r3
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L73
	.loc 1 753 0
	ldr	r3, .L79+24
	ldrh	r3, [r3, #6]
	mov	r2, r3
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #4]
	cmp	r2, r3
	bls	.L72
	.loc 1 753 0 is_stmt 0 discriminator 1
	ldr	r3, .L79+24
	ldrh	r3, [r3, #6]
	mov	r2, r3
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #8]
	cmp	r2, r3
	bcs	.L72
	.loc 1 755 0 is_stmt 1
	ldr	r3, .L79+24
	sub	ip, fp, #92
	mov	lr, r3
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	str	r3, [ip, #0]
	.loc 1 757 0
	ldr	r3, [fp, #-92]
	add	r3, r3, #3600
	str	r3, [fp, #-92]
	.loc 1 759 0
	ldrh	r3, [fp, #-80]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-80]	@ movhi
	.loc 1 761 0
	sub	r3, fp, #92
	mov	r0, r3
	bl	set_the_time_and_date
	b	.L72
.L73:
	.loc 1 765 0
	ldr	r3, .L79+24
	ldrh	r3, [r3, #8]
	mov	r2, r3
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L72
	.loc 1 767 0
	ldr	r3, .L79+24
	ldrh	r3, [r3, #6]
	mov	r2, r3
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #4]
	cmp	r2, r3
	bls	.L72
	.loc 1 767 0 is_stmt 0 discriminator 1
	ldr	r3, .L79+24
	ldrh	r3, [r3, #6]
	mov	r2, r3
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #8]
	cmp	r2, r3
	bcs	.L72
	.loc 1 772 0 is_stmt 1
	ldr	r3, .L79+32
	ldr	r3, [r3, #20]
	cmp	r3, #0
	beq	.L72
	.loc 1 775 0
	ldr	r3, .L79+32
	ldr	r2, .L79+24
	add	r3, r3, #28
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 777 0
	ldr	r3, .L79+24
	sub	ip, fp, #92
	mov	lr, r3
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	str	r3, [ip, #0]
	.loc 1 779 0
	ldr	r3, [fp, #-92]
	sub	r3, r3, #3600
	str	r3, [fp, #-92]
	.loc 1 781 0
	ldrh	r3, [fp, #-80]
	sub	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-80]	@ movhi
	.loc 1 783 0
	sub	r3, fp, #92
	mov	r0, r3
	bl	set_the_time_and_date
	.loc 1 785 0
	ldr	r3, .L79+32
	mov	r2, #1
	str	r2, [r3, #24]
	.loc 1 787 0
	ldr	r3, .L79+32
	mov	r2, #0
	str	r2, [r3, #20]
	b	.L72
.L71:
	.loc 1 802 0
	ldr	r3, .L79+32
	mov	r2, #0
	str	r2, [r3, #24]
.L72:
	.loc 1 808 0
	ldr	r3, .L79+32
	ldr	r3, [r3, #24]
	and	r2, r3, #255
	ldr	r3, .L79+24
	strb	r2, [r3, #19]
	.loc 1 813 0
	ldr	r3, .L79+44
	str	r3, [fp, #-72]
	.loc 1 816 0
	ldr	r3, .L79+24
	sub	ip, fp, #68
	mov	lr, r3
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	str	r3, [ip, #0]
	.loc 1 821 0
	ldr	r3, .L79+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	.loc 1 823 0
	ldr	r3, .L79+8
	ldr	r2, [r3, #0]
	sub	r3, fp, #72
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	mov	r3, r0
	cmp	r3, #1
	beq	.L78
	.loc 1 828 0
	ldr	r0, .L79+48
	bl	Alert_Message
.L78:
	.loc 1 835 0
	mov	r0, r0	@ nop
.L57:
	.loc 1 853 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 855 0
	b	.L74
.L75:
	.loc 1 865 0
	mov	r0, #1
	bl	vTaskDelay
	.loc 1 867 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-12]
	cmp	r3, #10
	bne	.L74
	.loc 1 871 0
	ldr	r0, .L79+52
	bl	Alert_Message
	.loc 1 872 0
	b	.L56
.L74:
	.loc 1 855 0 discriminator 1
	ldr	r3, .L79+56
	ldr	r3, [r3, #48]
	cmp	r3, #1
	beq	.L75
.L56:
.LBE3:
	.loc 1 881 0
	bl	xTaskGetTickCount
	mov	r2, r0
	ldr	r3, .L79+60
	str	r2, [r3, #0]
	.loc 1 891 0
	b	.L77
.L55:
	.loc 1 888 0
	ldr	r0, .L79+64
	bl	Alert_Message
	.loc 1 891 0
	b	.L77
.L80:
	.align	2
.L79:
	.word	Epson_RTC_Queue
	.word	Time_and_date_MUTEX
	.word	TD_CHECK_queue
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	local_dt
	.word	startup_rtc_task_sync_semaphore
	.word	weather_preserves
	.word	weather_preserves+28
	.word	7200
	.word	4369
	.word	.LC3
	.word	.LC4
	.word	i2c1cs
	.word	rtc_task_last_x_stamp
	.word	.LC5
.LFE9:
	.size	EPSON_rtc_control_task, .-EPSON_rtc_control_task
	.section .rodata
	.align	2
.LC6:
	.ascii	"EPSON RTC QUEUE OVERFLOW!\000"
	.section	.text.set_the_time_and_date,"ax",%progbits
	.align	2
	.type	set_the_time_and_date, %function
set_the_time_and_date:
.LFB10:
	.loc 1 896 0
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #28
.LCFI32:
	str	r0, [fp, #-32]
	.loc 1 902 0
	mov	r3, #102
	str	r3, [fp, #-28]
	.loc 1 904 0
	ldr	r3, [fp, #-32]
	ldrh	r3, [r3, #16]
	and	r3, r3, #255
	strb	r3, [fp, #-20]
	.loc 1 905 0
	ldr	r3, [fp, #-32]
	ldrh	r3, [r3, #14]
	and	r3, r3, #255
	strb	r3, [fp, #-19]
	.loc 1 906 0
	ldr	r3, [fp, #-32]
	ldrh	r3, [r3, #12]
	and	r3, r3, #255
	strb	r3, [fp, #-18]
	.loc 1 907 0
	ldr	r3, [fp, #-32]
	ldrb	r3, [r3, #18]	@ zero_extendqisi2
	strb	r3, [fp, #-17]
	.loc 1 908 0
	ldr	r3, [fp, #-32]
	ldrh	r3, [r3, #6]
	and	r3, r3, #255
	strb	r3, [fp, #-16]
	.loc 1 909 0
	ldr	r3, [fp, #-32]
	ldrh	r3, [r3, #8]
	and	r3, r3, #255
	strb	r3, [fp, #-15]
	.loc 1 910 0
	ldr	r3, [fp, #-32]
	ldrh	r3, [r3, #10]
	and	r3, r3, #255
	add	r3, r3, #48
	and	r3, r3, #255
	strb	r3, [fp, #-14]
	.loc 1 913 0
	ldr	r3, .L83
	ldr	r2, [r3, #0]
	sub	r3, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	mov	r3, r0
	cmp	r3, #1
	beq	.L82
	.loc 1 915 0
	ldr	r0, .L83+4
	bl	Alert_Message
.L82:
	.loc 1 936 0
	ldr	r0, .L83+8
	bl	postBackground_Calculation_Event
	.loc 1 937 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L84:
	.align	2
.L83:
	.word	Epson_RTC_Queue
	.word	.LC6
	.word	4369
.LFE10:
	.size	set_the_time_and_date, .-set_the_time_and_date
	.section	.text.EPSON_obtain_latest_complete_time_and_date,"ax",%progbits
	.align	2
	.global	EPSON_obtain_latest_complete_time_and_date
	.type	EPSON_obtain_latest_complete_time_and_date, %function
EPSON_obtain_latest_complete_time_and_date:
.LFB11:
	.loc 1 941 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #4
.LCFI35:
	str	r0, [fp, #-8]
	.loc 1 946 0
	ldr	r3, .L88
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L86
	.loc 1 950 0
	ldr	r3, .L88
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 952 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L88+4
	mov	ip, r2
	mov	lr, r3
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	str	r3, [ip, #0]
	.loc 1 954 0
	ldr	r3, .L88
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	b	.L85
.L86:
	.loc 1 958 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	strh	r2, [r3, #6]	@ movhi
	.loc 1 959 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	strb	r2, [r3, #18]
	.loc 1 960 0
	ldr	r3, [fp, #-8]
	mov	r2, #12
	strh	r2, [r3, #12]	@ movhi
	.loc 1 961 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	strh	r2, [r3, #14]	@ movhi
	.loc 1 962 0
	ldr	r3, [fp, #-8]
	mov	r2, #1
	strh	r2, [r3, #8]	@ movhi
	.loc 1 963 0
	ldr	r3, [fp, #-8]
	mov	r2, #0
	strh	r2, [r3, #16]	@ movhi
	.loc 1 964 0
	ldr	r3, [fp, #-8]
	mov	r2, #12
	strh	r2, [r3, #10]	@ movhi
	.loc 1 965 0
	mov	r0, #1
	mov	r1, #1
	mov	r2, #12
	bl	DMYToDate
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-8]
	strh	r2, [r3, #4]	@ movhi
	.loc 1 966 0
	mov	r0, #12
	mov	r1, #0
	mov	r2, #0
	bl	HMSToTime
	mov	r2, r0
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
.L85:
	.loc 1 968 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L89:
	.align	2
.L88:
	.word	Time_and_date_MUTEX
	.word	local_dt
.LFE11:
	.size	EPSON_obtain_latest_complete_time_and_date, .-EPSON_obtain_latest_complete_time_and_date
	.section	.text.EPSON_obtain_latest_time_and_date,"ax",%progbits
	.align	2
	.global	EPSON_obtain_latest_time_and_date
	.type	EPSON_obtain_latest_time_and_date, %function
EPSON_obtain_latest_time_and_date:
.LFB12:
	.loc 1 972 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #4
.LCFI38:
	str	r0, [fp, #-8]
	.loc 1 977 0
	ldr	r3, .L93
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L91
	.loc 1 981 0
	ldr	r3, .L93
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mvn	r2, #0
	mov	r3, #0
	bl	xQueueGenericReceive
	.loc 1 983 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L93+4
	mov	r1, r2
	mov	r2, r3
	mov	r3, #6
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	memcpy
	.loc 1 985 0
	ldr	r3, .L93
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	bl	xQueueGenericSend
	b	.L90
.L91:
	.loc 1 989 0
	mov	r0, #1
	mov	r1, #1
	mov	r2, #12
	bl	DMYToDate
	mov	r3, r0
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-8]
	and	r1, r2, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #4]
	mov	r2, r2, lsr #8
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	mov	r1, #0
	orr	r2, r1, r2
	strb	r2, [r3, #5]
	.loc 1 990 0
	mov	r0, #12
	mov	r1, #0
	mov	r2, #0
	bl	HMSToTime
	mov	r2, r0
	ldr	r3, [fp, #-8]
	and	r1, r2, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #0]
	mov	r1, r2, lsr #8
	and	r1, r1, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #1]
	mov	r1, r2, lsr #16
	and	r1, r1, #255
	mov	r0, #0
	orr	r1, r0, r1
	strb	r1, [r3, #2]
	mov	r2, r2, lsr #24
	mov	r1, #0
	orr	r2, r1, r2
	strb	r2, [r3, #3]
.L90:
	.loc 1 992 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L94:
	.align	2
.L93:
	.word	Time_and_date_MUTEX
	.word	local_dt
.LFE12:
	.size	EPSON_obtain_latest_time_and_date, .-EPSON_obtain_latest_time_and_date
	.section	.text.EXCEPTION_USE_ONLY_obtain_latest_time_and_date,"ax",%progbits
	.align	2
	.global	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	.type	EXCEPTION_USE_ONLY_obtain_latest_time_and_date, %function
EXCEPTION_USE_ONLY_obtain_latest_time_and_date:
.LFB13:
	.loc 1 996 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI39:
	add	fp, sp, #4
.LCFI40:
	sub	sp, sp, #4
.LCFI41:
	str	r0, [fp, #-8]
	.loc 1 1001 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L96
	mov	r1, r2
	mov	r2, r3
	mov	r3, #6
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	memcpy
	.loc 1 1002 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L97:
	.align	2
.L96:
	.word	local_dt
.LFE13:
	.size	EXCEPTION_USE_ONLY_obtain_latest_time_and_date, .-EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	.section	.text.EPSON_set_date_time,"ax",%progbits
	.align	2
	.global	EPSON_set_date_time
	.type	EPSON_set_date_time, %function
EPSON_set_date_time:
.LFB14:
	.loc 1 1009 0
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI42:
	add	fp, sp, #8
.LCFI43:
	sub	sp, sp, #84
.LCFI44:
	sub	r3, fp, #76
	stmia	r3, {r0, r1}
	str	r2, [fp, #-80]
	.loc 1 1022 0
	sub	r3, fp, #20
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 1029 0
	ldr	r3, [fp, #-80]
	cmp	r3, #1
	bne	.L99
	.loc 1 1036 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 1038 0
	ldrh	r2, [fp, #-16]
	ldrh	r3, [fp, #-72]
	cmp	r2, r3
	beq	.L100
	.loc 1 1040 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L100:
	.loc 1 1043 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-76]
	rsb	r3, r3, r2
	mov	r0, r3
	bl	abs
	mov	r3, r0
	cmp	r3, #30
	ble	.L101
	.loc 1 1045 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L101
.L99:
	.loc 1 1051 0
	mov	r3, #1
	str	r3, [fp, #-12]
.L101:
	.loc 1 1056 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L102
	.loc 1 1058 0
	sub	r3, fp, #40
	sub	r2, fp, #76
	ldmia	r2, {r0, r1}
	str	r0, [r3, #0]
	add	r3, r3, #4
	strh	r1, [r3, #0]	@ movhi
	.loc 1 1060 0
	ldrh	r3, [fp, #-72]
	mov	r0, r3
	sub	r1, fp, #44
	sub	r2, fp, #48
	sub	r3, fp, #52
	sub	ip, fp, #56
	str	ip, [sp, #0]
	bl	DateToDMY
	.loc 1 1062 0
	ldr	r0, [fp, #-76]
	sub	r1, fp, #60
	sub	r2, fp, #64
	sub	r3, fp, #68
	bl	TimeToHMS
	.loc 1 1064 0
	ldr	r3, [fp, #-44]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-34]	@ movhi
	.loc 1 1065 0
	ldr	r3, [fp, #-48]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-32]	@ movhi
	.loc 1 1066 0
	ldr	r3, [fp, #-52]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-30]	@ movhi
	.loc 1 1067 0
	ldr	r3, [fp, #-56]
	and	r3, r3, #255
	strb	r3, [fp, #-22]
	.loc 1 1068 0
	ldr	r3, [fp, #-60]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-28]	@ movhi
	.loc 1 1069 0
	ldr	r3, [fp, #-64]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-26]	@ movhi
	.loc 1 1070 0
	ldr	r3, [fp, #-68]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [fp, #-24]	@ movhi
	.loc 1 1072 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	set_the_time_and_date
	.loc 1 1074 0
	bl	FLOWSENSE_get_controller_index
	mov	r4, r0
	bl	FLOWSENSE_get_controller_index
	mov	r1, r0
	sub	r2, fp, #20
	sub	r3, fp, #76
	mov	r0, #6
	str	r0, [sp, #0]
	ldr	r0, [fp, #-80]
	str	r0, [sp, #4]
	str	r1, [sp, #8]
	mov	r0, #57344
	mov	r1, r4
	bl	Alert_ChangeLine_Controller
.L102:
	.loc 1 1079 0
	ldr	r3, [fp, #-12]
	.loc 1 1080 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.LFE14:
	.size	EPSON_set_date_time, .-EPSON_set_date_time
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_comm_server_common.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/rtc/epson_rx_8025sa.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_intc_driver.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr_driver.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_i2c.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_i2c_driver.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/configuration_network.h"
	.file 15 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 16 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 17 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xdec
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF213
	.byte	0x1
	.4byte	.LASF214
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x2
	.byte	0x3a
	.4byte	0x53
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF5
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x2
	.byte	0x4c
	.4byte	0x6c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x2
	.byte	0x5e
	.4byte	0x85
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x85
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x4c
	.4byte	0xc3
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x3
	.byte	0x55
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x57
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x3
	.byte	0x59
	.4byte	0x9e
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x65
	.4byte	0xf3
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x3
	.byte	0x67
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x69
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x3
	.byte	0x6b
	.4byte	0xce
	.uleb128 0x5
	.byte	0x6
	.byte	0x4
	.byte	0x22
	.4byte	0x11f
	.uleb128 0x7
	.ascii	"T\000"
	.byte	0x4
	.byte	0x24
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.ascii	"D\000"
	.byte	0x4
	.byte	0x26
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x4
	.byte	0x28
	.4byte	0xfe
	.uleb128 0x5
	.byte	0x14
	.byte	0x4
	.byte	0x31
	.4byte	0x1b1
	.uleb128 0x6
	.4byte	.LASF20
	.byte	0x4
	.byte	0x33
	.4byte	0x11f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF21
	.byte	0x4
	.byte	0x35
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x6
	.4byte	.LASF22
	.byte	0x4
	.byte	0x35
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF23
	.byte	0x4
	.byte	0x35
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x6
	.4byte	.LASF24
	.byte	0x4
	.byte	0x37
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x4
	.byte	0x37
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x4
	.byte	0x37
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x4
	.byte	0x39
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0x6
	.4byte	.LASF28
	.byte	0x4
	.byte	0x3b
	.4byte	0x48
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.byte	0
	.uleb128 0x4
	.4byte	.LASF29
	.byte	0x4
	.byte	0x3d
	.4byte	0x12a
	.uleb128 0x5
	.byte	0x18
	.byte	0x5
	.byte	0x19
	.4byte	0x1e1
	.uleb128 0x6
	.4byte	.LASF30
	.byte	0x5
	.byte	0x1c
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF31
	.byte	0x5
	.byte	0x1e
	.4byte	0x1b1
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x4
	.4byte	.LASF32
	.byte	0x5
	.byte	0x20
	.4byte	0x1bc
	.uleb128 0x8
	.byte	0x4
	.byte	0x6
	.byte	0x1d
	.4byte	0x219
	.uleb128 0x9
	.4byte	.LASF33
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF34
	.sleb128 1
	.uleb128 0x9
	.4byte	.LASF35
	.sleb128 2
	.uleb128 0x9
	.4byte	.LASF36
	.sleb128 3
	.uleb128 0x9
	.4byte	.LASF37
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF38
	.sleb128 5
	.byte	0
	.uleb128 0xa
	.4byte	0x7a
	.uleb128 0xb
	.4byte	0x7a
	.4byte	0x22e
	.uleb128 0xc
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xb
	.4byte	0x7a
	.4byte	0x23e
	.uleb128 0xc
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.byte	0x7
	.byte	0x24
	.4byte	0x31f
	.uleb128 0x9
	.4byte	.LASF39
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF40
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF41
	.sleb128 1
	.uleb128 0x9
	.4byte	.LASF42
	.sleb128 2
	.uleb128 0x9
	.4byte	.LASF43
	.sleb128 3
	.uleb128 0x9
	.4byte	.LASF44
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF45
	.sleb128 5
	.uleb128 0x9
	.4byte	.LASF46
	.sleb128 6
	.uleb128 0x9
	.4byte	.LASF47
	.sleb128 7
	.uleb128 0x9
	.4byte	.LASF48
	.sleb128 8
	.uleb128 0x9
	.4byte	.LASF49
	.sleb128 9
	.uleb128 0x9
	.4byte	.LASF50
	.sleb128 10
	.uleb128 0x9
	.4byte	.LASF51
	.sleb128 11
	.uleb128 0x9
	.4byte	.LASF52
	.sleb128 12
	.uleb128 0x9
	.4byte	.LASF53
	.sleb128 13
	.uleb128 0x9
	.4byte	.LASF54
	.sleb128 14
	.uleb128 0x9
	.4byte	.LASF55
	.sleb128 15
	.uleb128 0x9
	.4byte	.LASF56
	.sleb128 16
	.uleb128 0x9
	.4byte	.LASF57
	.sleb128 17
	.uleb128 0x9
	.4byte	.LASF58
	.sleb128 18
	.uleb128 0x9
	.4byte	.LASF59
	.sleb128 19
	.uleb128 0x9
	.4byte	.LASF60
	.sleb128 20
	.uleb128 0x9
	.4byte	.LASF61
	.sleb128 21
	.uleb128 0x9
	.4byte	.LASF62
	.sleb128 22
	.uleb128 0x9
	.4byte	.LASF63
	.sleb128 23
	.uleb128 0x9
	.4byte	.LASF64
	.sleb128 24
	.uleb128 0x9
	.4byte	.LASF65
	.sleb128 25
	.uleb128 0x9
	.4byte	.LASF66
	.sleb128 26
	.uleb128 0x9
	.4byte	.LASF67
	.sleb128 27
	.uleb128 0x9
	.4byte	.LASF68
	.sleb128 28
	.uleb128 0x9
	.4byte	.LASF69
	.sleb128 29
	.uleb128 0x9
	.4byte	.LASF70
	.sleb128 30
	.uleb128 0x9
	.4byte	.LASF71
	.sleb128 31
	.uleb128 0x9
	.4byte	.LASF72
	.sleb128 32
	.uleb128 0x9
	.4byte	.LASF73
	.sleb128 33
	.uleb128 0x9
	.4byte	.LASF74
	.sleb128 34
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.byte	0x7
	.byte	0x4d
	.4byte	0x36a
	.uleb128 0x9
	.4byte	.LASF75
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF76
	.sleb128 1
	.uleb128 0x9
	.4byte	.LASF77
	.sleb128 2
	.uleb128 0x9
	.4byte	.LASF78
	.sleb128 3
	.uleb128 0x9
	.4byte	.LASF79
	.sleb128 4
	.uleb128 0x9
	.4byte	.LASF80
	.sleb128 5
	.uleb128 0x9
	.4byte	.LASF81
	.sleb128 6
	.uleb128 0x9
	.4byte	.LASF82
	.sleb128 7
	.uleb128 0x9
	.4byte	.LASF83
	.sleb128 8
	.uleb128 0x9
	.4byte	.LASF84
	.sleb128 9
	.uleb128 0x9
	.4byte	.LASF85
	.sleb128 10
	.byte	0
	.uleb128 0x5
	.byte	0x30
	.byte	0x8
	.byte	0x28
	.4byte	0x41b
	.uleb128 0x6
	.4byte	.LASF86
	.byte	0x8
	.byte	0x2a
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF87
	.byte	0x8
	.byte	0x2b
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF88
	.byte	0x8
	.byte	0x2c
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF89
	.byte	0x8
	.byte	0x2d
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF90
	.byte	0x8
	.byte	0x2e
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF91
	.byte	0x8
	.byte	0x2f
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF92
	.byte	0x8
	.byte	0x30
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF93
	.byte	0x8
	.byte	0x31
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x6
	.4byte	.LASF94
	.byte	0x8
	.byte	0x32
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x6
	.4byte	.LASF95
	.byte	0x8
	.byte	0x33
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x6
	.4byte	.LASF96
	.byte	0x8
	.byte	0x34
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x6
	.4byte	.LASF97
	.byte	0x8
	.byte	0x35
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.byte	0
	.uleb128 0x4
	.4byte	.LASF98
	.byte	0x8
	.byte	0x36
	.4byte	0x36a
	.uleb128 0x8
	.byte	0x4
	.byte	0x9
	.byte	0x79
	.4byte	0x43b
	.uleb128 0x9
	.4byte	.LASF99
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF100
	.sleb128 1
	.byte	0
	.uleb128 0xd
	.byte	0x4
	.uleb128 0x4
	.4byte	.LASF101
	.byte	0xa
	.byte	0x35
	.4byte	0x25
	.uleb128 0x4
	.4byte	.LASF102
	.byte	0xb
	.byte	0x57
	.4byte	0x43b
	.uleb128 0x4
	.4byte	.LASF103
	.byte	0xc
	.byte	0x4c
	.4byte	0x448
	.uleb128 0xb
	.4byte	0x53
	.4byte	0x46e
	.uleb128 0xc
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.4byte	0x41
	.4byte	0x47e
	.uleb128 0xc
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF104
	.uleb128 0xe
	.byte	0x1c
	.byte	0xd
	.2byte	0x10c
	.4byte	0x4f8
	.uleb128 0xf
	.ascii	"rip\000"
	.byte	0xd
	.2byte	0x112
	.4byte	0xf3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF105
	.byte	0xd
	.2byte	0x11b
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x10
	.4byte	.LASF106
	.byte	0xd
	.2byte	0x122
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x10
	.4byte	.LASF107
	.byte	0xd
	.2byte	0x127
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x10
	.4byte	.LASF108
	.byte	0xd
	.2byte	0x138
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x10
	.4byte	.LASF109
	.byte	0xd
	.2byte	0x144
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x10
	.4byte	.LASF110
	.byte	0xd
	.2byte	0x14b
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0x11
	.4byte	.LASF111
	.byte	0xd
	.2byte	0x14d
	.4byte	0x485
	.uleb128 0xe
	.byte	0xec
	.byte	0xd
	.2byte	0x150
	.4byte	0x6d8
	.uleb128 0x10
	.4byte	.LASF112
	.byte	0xd
	.2byte	0x157
	.4byte	0x46e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x10
	.4byte	.LASF113
	.byte	0xd
	.2byte	0x162
	.4byte	0x61
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x10
	.4byte	.LASF114
	.byte	0xd
	.2byte	0x164
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x10
	.4byte	.LASF115
	.byte	0xd
	.2byte	0x166
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x10
	.4byte	.LASF116
	.byte	0xd
	.2byte	0x168
	.4byte	0x11f
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x10
	.4byte	.LASF117
	.byte	0xd
	.2byte	0x16e
	.4byte	0xc3
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x10
	.4byte	.LASF118
	.byte	0xd
	.2byte	0x174
	.4byte	0x4f8
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x10
	.4byte	.LASF119
	.byte	0xd
	.2byte	0x17b
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x10
	.4byte	.LASF120
	.byte	0xd
	.2byte	0x17d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x10
	.4byte	.LASF121
	.byte	0xd
	.2byte	0x185
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x10
	.4byte	.LASF122
	.byte	0xd
	.2byte	0x18d
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x10
	.4byte	.LASF123
	.byte	0xd
	.2byte	0x191
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x10
	.4byte	.LASF124
	.byte	0xd
	.2byte	0x195
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x10
	.4byte	.LASF125
	.byte	0xd
	.2byte	0x199
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x10
	.4byte	.LASF126
	.byte	0xd
	.2byte	0x19e
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x10
	.4byte	.LASF127
	.byte	0xd
	.2byte	0x1a2
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x10
	.4byte	.LASF128
	.byte	0xd
	.2byte	0x1a6
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x10
	.4byte	.LASF129
	.byte	0xd
	.2byte	0x1b4
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x10
	.4byte	.LASF130
	.byte	0xd
	.2byte	0x1ba
	.4byte	0x93
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x10
	.4byte	.LASF131
	.byte	0xd
	.2byte	0x1c2
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x10
	.4byte	.LASF132
	.byte	0xd
	.2byte	0x1c4
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x10
	.4byte	.LASF133
	.byte	0xd
	.2byte	0x1c6
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x10
	.4byte	.LASF134
	.byte	0xd
	.2byte	0x1d5
	.4byte	0x48
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x10
	.4byte	.LASF135
	.byte	0xd
	.2byte	0x1d7
	.4byte	0x48
	.byte	0x3
	.byte	0x23
	.uleb128 0x81
	.uleb128 0x10
	.4byte	.LASF136
	.byte	0xd
	.2byte	0x1dd
	.4byte	0x48
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0x10
	.4byte	.LASF137
	.byte	0xd
	.2byte	0x1e7
	.4byte	0x48
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0x10
	.4byte	.LASF138
	.byte	0xd
	.2byte	0x1f0
	.4byte	0x7a
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x10
	.4byte	.LASF139
	.byte	0xd
	.2byte	0x1f7
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x10
	.4byte	.LASF140
	.byte	0xd
	.2byte	0x1f9
	.4byte	0x93
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0x10
	.4byte	.LASF141
	.byte	0xd
	.2byte	0x1fd
	.4byte	0x6d8
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0xb
	.4byte	0x7a
	.4byte	0x6e8
	.uleb128 0xc
	.4byte	0x25
	.byte	0x16
	.byte	0
	.uleb128 0x11
	.4byte	.LASF142
	.byte	0xd
	.2byte	0x204
	.4byte	0x504
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF143
	.uleb128 0x5
	.byte	0xc
	.byte	0xe
	.byte	0x43
	.4byte	0x72e
	.uleb128 0x6
	.4byte	.LASF144
	.byte	0xe
	.byte	0x45
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF145
	.byte	0xe
	.byte	0x47
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF146
	.byte	0xe
	.byte	0x49
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x4
	.4byte	.LASF147
	.byte	0xe
	.byte	0x4b
	.4byte	0x6fb
	.uleb128 0x5
	.byte	0x18
	.byte	0x1
	.byte	0x40
	.4byte	0x76c
	.uleb128 0x7
	.ascii	"cmd\000"
	.byte	0x1
	.byte	0x42
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x1
	.byte	0x44
	.4byte	0x7a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF148
	.byte	0x1
	.byte	0x46
	.4byte	0x76c
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xb
	.4byte	0x48
	.4byte	0x77c
	.uleb128 0xc
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x4
	.4byte	.LASF149
	.byte	0x1
	.byte	0x48
	.4byte	0x739
	.uleb128 0x5
	.byte	0x38
	.byte	0x1
	.byte	0x6b
	.4byte	0x800
	.uleb128 0x6
	.4byte	.LASF150
	.byte	0x1
	.byte	0x6d
	.4byte	0x800
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF151
	.byte	0x1
	.byte	0x6f
	.4byte	0x805
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF152
	.byte	0x1
	.byte	0x71
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x6
	.4byte	.LASF153
	.byte	0x1
	.byte	0x73
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x6
	.4byte	.LASF154
	.byte	0x1
	.byte	0x74
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x6
	.4byte	.LASF155
	.byte	0x1
	.byte	0x75
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x6
	.4byte	.LASF156
	.byte	0x1
	.byte	0x77
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x6
	.4byte	.LASF157
	.byte	0x1
	.byte	0x79
	.4byte	0x219
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0xa
	.4byte	0x76c
	.uleb128 0xa
	.4byte	0x76c
	.uleb128 0x4
	.4byte	.LASF158
	.byte	0x1
	.byte	0x7b
	.4byte	0x787
	.uleb128 0x12
	.byte	0x1
	.4byte	.LASF187
	.byte	0x1
	.byte	0xa2
	.byte	0x1
	.4byte	0x7a
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x85b
	.uleb128 0x13
	.4byte	.LASF169
	.byte	0x1
	.byte	0xa2
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x14
	.4byte	.LASF159
	.byte	0x1
	.byte	0xa5
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x15
	.ascii	"i\000"
	.byte	0x1
	.byte	0xa5
	.4byte	0x41
	.byte	0x2
	.byte	0x91
	.sleb128 -5
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF162
	.byte	0x1
	.byte	0xb1
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x8a5
	.uleb128 0x14
	.4byte	.LASF160
	.byte	0x1
	.byte	0xb3
	.4byte	0x77c
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x14
	.4byte	.LASF161
	.byte	0x1
	.byte	0xb5
	.4byte	0x33
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x17
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF215
	.byte	0x1
	.byte	0xc2
	.byte	0x1
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF163
	.byte	0x1
	.byte	0xc7
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x8e9
	.uleb128 0x14
	.4byte	.LASF164
	.byte	0x1
	.byte	0xc9
	.4byte	0x8e9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x14
	.4byte	.LASF160
	.byte	0x1
	.byte	0xcb
	.4byte	0x77c
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x14
	.4byte	.LASF161
	.byte	0x1
	.byte	0xcd
	.4byte	0x33
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.uleb128 0x19
	.byte	0x4
	.4byte	0x41b
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF165
	.byte	0x1
	.2byte	0x102
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x937
	.uleb128 0x1b
	.4byte	.LASF164
	.byte	0x1
	.2byte	0x104
	.4byte	0x8e9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1b
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x106
	.4byte	0x77c
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1b
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x108
	.4byte	0x33
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF166
	.byte	0x1
	.2byte	0x158
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x961
	.uleb128 0x1b
	.4byte	.LASF164
	.byte	0x1
	.2byte	0x15a
	.4byte	0x8e9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF167
	.byte	0x1
	.2byte	0x17a
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x98b
	.uleb128 0x1b
	.4byte	.LASF164
	.byte	0x1
	.2byte	0x17c
	.4byte	0x8e9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF168
	.byte	0x1
	.2byte	0x1a0
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x9c4
	.uleb128 0x1c
	.4byte	.LASF170
	.byte	0x1
	.2byte	0x1a0
	.4byte	0x9c4
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.4byte	.LASF164
	.byte	0x1
	.2byte	0x1a8
	.4byte	0x8e9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.byte	0x4
	.4byte	0x77c
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF171
	.byte	0x1
	.2byte	0x1d0
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x9f4
	.uleb128 0x1b
	.4byte	.LASF164
	.byte	0x1
	.2byte	0x1d8
	.4byte	0x8e9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF172
	.byte	0x1
	.2byte	0x202
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0xa4b
	.uleb128 0x1b
	.4byte	.LASF164
	.byte	0x1
	.2byte	0x208
	.4byte	0x8e9
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1d
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x20a
	.4byte	0x219
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.4byte	.LASF173
	.byte	0x1
	.2byte	0x20a
	.4byte	0x219
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1d
	.ascii	"low\000"
	.byte	0x1
	.2byte	0x20a
	.4byte	0x219
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF174
	.byte	0x1
	.2byte	0x22c
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0xafa
	.uleb128 0x1c
	.4byte	.LASF175
	.byte	0x1
	.2byte	0x22c
	.4byte	0x43b
	.byte	0x3
	.byte	0x91
	.sleb128 -100
	.uleb128 0x1b
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x22e
	.4byte	0x77c
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x1b
	.4byte	.LASF176
	.byte	0x1
	.2byte	0x230
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1b
	.4byte	.LASF177
	.byte	0x1
	.2byte	0x232
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -17
	.uleb128 0x1b
	.4byte	.LASF178
	.byte	0x1
	.2byte	0x234
	.4byte	0x1e1
	.byte	0x3
	.byte	0x91
	.sleb128 -76
	.uleb128 0x1b
	.4byte	.LASF179
	.byte	0x1
	.2byte	0x236
	.4byte	0x1b1
	.byte	0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x1b
	.4byte	.LASF180
	.byte	0x1
	.2byte	0x238
	.4byte	0xafa
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1b
	.4byte	.LASF181
	.byte	0x1
	.2byte	0x238
	.4byte	0xafa
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x17
	.4byte	.LBB3
	.4byte	.LBE3
	.uleb128 0x1b
	.4byte	.LASF182
	.byte	0x1
	.2byte	0x353
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.byte	0
	.uleb128 0x19
	.byte	0x4
	.4byte	0x72e
	.uleb128 0x1e
	.4byte	.LASF216
	.byte	0x1
	.2byte	0x37f
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0xb38
	.uleb128 0x1f
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x37f
	.4byte	0xb38
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1b
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x384
	.4byte	0x77c
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.uleb128 0x19
	.byte	0x4
	.4byte	0x1b1
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF183
	.byte	0x1
	.2byte	0x3ac
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0xb68
	.uleb128 0x1c
	.4byte	.LASF184
	.byte	0x1
	.2byte	0x3ac
	.4byte	0xb38
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF185
	.byte	0x1
	.2byte	0x3cb
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0xb92
	.uleb128 0x1f
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x3cb
	.4byte	0xb92
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x19
	.byte	0x4
	.4byte	0x11f
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF186
	.byte	0x1
	.2byte	0x3e3
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0xbc2
	.uleb128 0x1f
	.ascii	"ldt\000"
	.byte	0x1
	.2byte	0x3e3
	.4byte	0xb92
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.4byte	.LASF188
	.byte	0x1
	.2byte	0x3f0
	.byte	0x1
	.4byte	0x93
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0xc99
	.uleb128 0x1f
	.ascii	"pdt\000"
	.byte	0x1
	.2byte	0x3f0
	.4byte	0xc99
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x1c
	.4byte	.LASF189
	.byte	0x1
	.2byte	0x3f0
	.4byte	0xc9e
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x1b
	.4byte	.LASF190
	.byte	0x1
	.2byte	0x3f2
	.4byte	0x11f
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1b
	.4byte	.LASF191
	.byte	0x1
	.2byte	0x3f4
	.4byte	0x93
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1b
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x3f6
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x1b
	.4byte	.LASF192
	.byte	0x1
	.2byte	0x3f8
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1b
	.4byte	.LASF193
	.byte	0x1
	.2byte	0x3f8
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x1b
	.4byte	.LASF194
	.byte	0x1
	.2byte	0x3f8
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x1b
	.4byte	.LASF195
	.byte	0x1
	.2byte	0x3f8
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x1b
	.4byte	.LASF196
	.byte	0x1
	.2byte	0x3f8
	.4byte	0x7a
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x1b
	.4byte	.LASF197
	.byte	0x1
	.2byte	0x3f8
	.4byte	0x7a
	.byte	0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x1b
	.4byte	.LASF198
	.byte	0x1
	.2byte	0x3f8
	.4byte	0x7a
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.byte	0
	.uleb128 0x21
	.4byte	0x11f
	.uleb128 0x21
	.4byte	0x7a
	.uleb128 0x14
	.4byte	.LASF199
	.byte	0xf
	.byte	0x30
	.4byte	0xcb4
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x21
	.4byte	0x45e
	.uleb128 0x14
	.4byte	.LASF200
	.byte	0xf
	.byte	0x34
	.4byte	0xcca
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x21
	.4byte	0x45e
	.uleb128 0x14
	.4byte	.LASF201
	.byte	0xf
	.byte	0x36
	.4byte	0xce0
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x21
	.4byte	0x45e
	.uleb128 0x14
	.4byte	.LASF202
	.byte	0xf
	.byte	0x38
	.4byte	0xcf6
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x21
	.4byte	0x45e
	.uleb128 0x22
	.4byte	.LASF203
	.byte	0x10
	.byte	0x58
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF204
	.byte	0x10
	.byte	0x64
	.4byte	0x453
	.byte	0x1
	.byte	0x1
	.uleb128 0x23
	.4byte	.LASF205
	.byte	0x10
	.2byte	0x134
	.4byte	0x448
	.byte	0x1
	.byte	0x1
	.uleb128 0x23
	.4byte	.LASF206
	.byte	0x10
	.2byte	0x144
	.4byte	0x448
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF207
	.byte	0x11
	.byte	0x33
	.4byte	0xd42
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x21
	.4byte	0x21e
	.uleb128 0x14
	.4byte	.LASF208
	.byte	0x11
	.byte	0x3f
	.4byte	0xd58
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x21
	.4byte	0x22e
	.uleb128 0x23
	.4byte	.LASF209
	.byte	0xd
	.2byte	0x206
	.4byte	0x6e8
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	.LASF210
	.byte	0x1
	.byte	0x7e
	.4byte	0xd7c
	.byte	0x5
	.byte	0x3
	.4byte	i2c1cs
	.uleb128 0xa
	.4byte	0x80a
	.uleb128 0x14
	.4byte	.LASF211
	.byte	0x1
	.byte	0x9a
	.4byte	0x1b1
	.byte	0x5
	.byte	0x3
	.4byte	local_dt
	.uleb128 0x14
	.4byte	.LASF212
	.byte	0x1
	.byte	0x9d
	.4byte	0x453
	.byte	0x5
	.byte	0x3
	.4byte	Time_and_date_MUTEX
	.uleb128 0x22
	.4byte	.LASF203
	.byte	0x10
	.byte	0x58
	.4byte	0x7a
	.byte	0x1
	.byte	0x1
	.uleb128 0x22
	.4byte	.LASF204
	.byte	0x10
	.byte	0x64
	.4byte	0x453
	.byte	0x1
	.byte	0x1
	.uleb128 0x24
	.4byte	.LASF205
	.byte	0x1
	.byte	0x4a
	.4byte	0x448
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	Epson_RTC_Queue
	.uleb128 0x24
	.4byte	.LASF206
	.byte	0x1
	.byte	0x53
	.4byte	0x448
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	TD_CHECK_queue
	.uleb128 0x23
	.4byte	.LASF209
	.byte	0xd
	.2byte	0x206
	.4byte	0x6e8
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI43
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF172:
	.ascii	"_init_epson_i2c_channel\000"
.LASF185:
	.ascii	"EPSON_obtain_latest_time_and_date\000"
.LASF190:
	.ascii	"controller_dt\000"
.LASF137:
	.ascii	"ununsed_uns8_1\000"
.LASF61:
	.ascii	"CLKPWR_TIMER0_CLK\000"
.LASF98:
	.ascii	"I2C_REGS_T\000"
.LASF110:
	.ascii	"needs_to_be_broadcast\000"
.LASF180:
	.ascii	"ldls_spring_ahead\000"
.LASF22:
	.ascii	"__month\000"
.LASF56:
	.ascii	"CLKPWR_HSTIMER_CLK\000"
.LASF201:
	.ascii	"GuiFont_DecimalChar\000"
.LASF139:
	.ascii	"hub_needs_to_distribute_tpmicro_binary\000"
.LASF124:
	.ascii	"yesterdays_ET_was_zero\000"
.LASF88:
	.ascii	"i2c_ctrl\000"
.LASF68:
	.ascii	"CLKPWR_UART6_CLK\000"
.LASF1:
	.ascii	"long int\000"
.LASF96:
	.ascii	"i2c_stx\000"
.LASF87:
	.ascii	"i2c_stat\000"
.LASF41:
	.ascii	"CLKPWR_LCD_CLK\000"
.LASF12:
	.ascii	"long long unsigned int\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF148:
	.ascii	"data\000"
.LASF183:
	.ascii	"EPSON_obtain_latest_complete_time_and_date\000"
.LASF154:
	.ascii	"bytes_in_the_rcvd_fifo\000"
.LASF92:
	.ascii	"i2c_rxfl\000"
.LASF164:
	.ascii	"reg_ptr\000"
.LASF77:
	.ascii	"CLKPWR_SYSCLK\000"
.LASF113:
	.ascii	"dls_saved_date\000"
.LASF109:
	.ascii	"inhibit_irrigation_have_crossed_the_minimum\000"
.LASF10:
	.ascii	"UNS_32\000"
.LASF97:
	.ascii	"i2c_stxfl\000"
.LASF46:
	.ascii	"CLKPWR_MSCARD_CLK\000"
.LASF2:
	.ascii	"long long int\000"
.LASF5:
	.ascii	"signed char\000"
.LASF107:
	.ascii	"midnight_to_midnight_raw_pulse_count\000"
.LASF39:
	.ascii	"CLKPWR_FIRST_CLK\000"
.LASF196:
	.ascii	"lhour\000"
.LASF198:
	.ascii	"lsec\000"
.LASF60:
	.ascii	"CLKPWR_TIMER1_CLK\000"
.LASF210:
	.ascii	"i2c1cs\000"
.LASF119:
	.ascii	"sync_the_et_rain_tables\000"
.LASF216:
	.ascii	"set_the_time_and_date\000"
.LASF141:
	.ascii	"expansion\000"
.LASF69:
	.ascii	"CLKPWR_UART5_CLK\000"
.LASF26:
	.ascii	"__seconds\000"
.LASF189:
	.ascii	"preason\000"
.LASF134:
	.ascii	"factory_reset_do_not_allow_pdata_to_be_sent\000"
.LASF136:
	.ascii	"perform_a_discovery_following_a_FACTORY_reset\000"
.LASF116:
	.ascii	"dls_after_fall_when_to_clear_ignore_sxs\000"
.LASF65:
	.ascii	"CLKPWR_SPI1_CLK\000"
.LASF72:
	.ascii	"CLKPWR_DMA_CLK\000"
.LASF168:
	.ascii	"_epson_set_the_time_and_date\000"
.LASF91:
	.ascii	"i2c_adr\000"
.LASF215:
	.ascii	"vTaskSwitchContext\000"
.LASF153:
	.ascii	"remaining_to_rcv\000"
.LASF78:
	.ascii	"CLKPWR_ARM_CLK\000"
.LASF35:
	.ascii	"ISR_TRIGGER_HIGH_LEVEL\000"
.LASF147:
	.ascii	"DAYLIGHT_SAVING_TIME_STRUCT\000"
.LASF101:
	.ascii	"portTickType\000"
.LASF205:
	.ascii	"Epson_RTC_Queue\000"
.LASF93:
	.ascii	"i2c_txfl\000"
.LASF202:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF89:
	.ascii	"i2c_clk_hi\000"
.LASF108:
	.ascii	"rain_shutdown_rcvd_from_commserver_uns32\000"
.LASF100:
	.ascii	"I2C_PINS_HIGH_DRIVE\000"
.LASF166:
	.ascii	"_epson_read_REG_F\000"
.LASF44:
	.ascii	"CLKPWR_I2S1_CLK\000"
.LASF149:
	.ascii	"EPSON_RTC_QUEUE_STRUCT\000"
.LASF19:
	.ascii	"DATE_TIME\000"
.LASF152:
	.ascii	"remaining_to_xmit\000"
.LASF144:
	.ascii	"month\000"
.LASF11:
	.ascii	"unsigned int\000"
.LASF49:
	.ascii	"CLKPWR_MAC_HRC_CLK\000"
.LASF67:
	.ascii	"CLKPWR_NAND_MLC_CLK\000"
.LASF195:
	.ascii	"ldow\000"
.LASF187:
	.ascii	"hextobcd\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF115:
	.ascii	"dls_after_fall_back_ignore_sxs\000"
.LASF126:
	.ascii	"clear_runaway_gage\000"
.LASF175:
	.ascii	"pvParameters\000"
.LASF121:
	.ascii	"et_table_update_all_historical_values\000"
.LASF159:
	.ascii	"bcdresult\000"
.LASF95:
	.ascii	"i2c_txb\000"
.LASF8:
	.ascii	"short unsigned int\000"
.LASF213:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF28:
	.ascii	"dls_after_fall_back_ignore_start_times\000"
.LASF133:
	.ascii	"commserver_monthly_code_receipt_bytes\000"
.LASF33:
	.ascii	"ISR_TRIGGER_FIXED\000"
.LASF206:
	.ascii	"TD_CHECK_queue\000"
.LASF193:
	.ascii	"lmonth\000"
.LASF145:
	.ascii	"min_day\000"
.LASF20:
	.ascii	"date_time\000"
.LASF181:
	.ascii	"ldls_fall_back\000"
.LASF184:
	.ascii	"lcdt\000"
.LASF85:
	.ascii	"CLKPWR_BASE_INVALID\000"
.LASF74:
	.ascii	"CLKPWR_LAST_CLK\000"
.LASF76:
	.ascii	"CLKPWR_RTC_CLK\000"
.LASF157:
	.ascii	"qcmd\000"
.LASF140:
	.ascii	"hub_needs_to_distribute_main_binary\000"
.LASF54:
	.ascii	"CLKPWR_PWM2_CLK\000"
.LASF40:
	.ascii	"CLKPWR_USB_HCLK\000"
.LASF102:
	.ascii	"xQueueHandle\000"
.LASF204:
	.ascii	"startup_rtc_task_sync_semaphore\000"
.LASF200:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF43:
	.ascii	"CLKPWR_SSP0_CLK\000"
.LASF132:
	.ascii	"commserver_monthly_xmit_bytes\000"
.LASF86:
	.ascii	"i2c_txrx\000"
.LASF50:
	.ascii	"CLKPWR_I2C2_CLK\000"
.LASF203:
	.ascii	"rtc_task_last_x_stamp\000"
.LASF128:
	.ascii	"freeze_switch_active\000"
.LASF192:
	.ascii	"lday\000"
.LASF42:
	.ascii	"CLKPWR_SSP1_CLK\000"
.LASF70:
	.ascii	"CLKPWR_UART4_CLK\000"
.LASF80:
	.ascii	"CLKPWR_PERIPH_CLK\000"
.LASF130:
	.ascii	"pending_changes_to_send_to_comm_server\000"
.LASF186:
	.ascii	"EXCEPTION_USE_ONLY_obtain_latest_time_and_date\000"
.LASF27:
	.ascii	"__dayofweek\000"
.LASF169:
	.ascii	"input_val\000"
.LASF120:
	.ascii	"nlu_old_rain_table_sync_variable\000"
.LASF81:
	.ascii	"CLKPWR_USB_HCLK_SYS\000"
.LASF82:
	.ascii	"CLKPWR_48M_CLK\000"
.LASF32:
	.ascii	"TD_CHECK_QUEUE_STRUCT\000"
.LASF62:
	.ascii	"CLKPWR_TIMER5_CLK\000"
.LASF37:
	.ascii	"ISR_TRIGGER_POSITIVE_EDGE\000"
.LASF158:
	.ascii	"EPSON_I2C_CONTROL_STRUCT\000"
.LASF45:
	.ascii	"CLKPWR_I2S0_CLK\000"
.LASF104:
	.ascii	"float\000"
.LASF199:
	.ascii	"GuiFont_LanguageActive\000"
.LASF179:
	.ascii	"ldtcs\000"
.LASF135:
	.ascii	"factory_reset_registration_to_tell_commserver_to_cl"
	.ascii	"ear_pdata\000"
.LASF138:
	.ascii	"commserver_monthly_mobile_status_updates_bytes\000"
.LASF21:
	.ascii	"__day\000"
.LASF38:
	.ascii	"ISR_TRIGGER_DUAL_EDGE\000"
.LASF131:
	.ascii	"commserver_monthly_rcvd_bytes\000"
.LASF106:
	.ascii	"roll_to_roll_raw_pulse_count\000"
.LASF178:
	.ascii	"ltdcqs\000"
.LASF34:
	.ascii	"ISR_TRIGGER_LOW_LEVEL\000"
.LASF4:
	.ascii	"unsigned char\000"
.LASF163:
	.ascii	"epson_i2c1_write_isr\000"
.LASF9:
	.ascii	"short int\000"
.LASF151:
	.ascii	"our_rx_fifo\000"
.LASF18:
	.ascii	"RAIN_TABLE_ENTRY\000"
.LASF24:
	.ascii	"__hours\000"
.LASF52:
	.ascii	"CLKPWR_KEYSCAN_CLK\000"
.LASF59:
	.ascii	"CLKPWR_TIMER2_CLK\000"
.LASF191:
	.ascii	"set_d_and_t\000"
.LASF160:
	.ascii	"lerqs\000"
.LASF214:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/rtc/"
	.ascii	"epson_rx_8025sa.c\000"
.LASF182:
	.ascii	"waited\000"
.LASF58:
	.ascii	"CLKPWR_TIMER3_CLK\000"
.LASF174:
	.ascii	"EPSON_rtc_control_task\000"
.LASF118:
	.ascii	"rain\000"
.LASF36:
	.ascii	"ISR_TRIGGER_NEGATIVE_EDGE\000"
.LASF66:
	.ascii	"CLKPWR_NAND_SLC_CLK\000"
.LASF194:
	.ascii	"lyear\000"
.LASF55:
	.ascii	"CLKPWR_PWM1_CLK\000"
.LASF94:
	.ascii	"i2c_rxb\000"
.LASF31:
	.ascii	"dtcs\000"
.LASF3:
	.ascii	"char\000"
.LASF209:
	.ascii	"weather_preserves\000"
.LASF211:
	.ascii	"local_dt\000"
.LASF129:
	.ascii	"write_tpmicro_file_in_new_file_format\000"
.LASF25:
	.ascii	"__minutes\000"
.LASF123:
	.ascii	"run_away_gage\000"
.LASF150:
	.ascii	"our_tx_fifo\000"
.LASF162:
	.ascii	"epson_periodic_isr\000"
.LASF161:
	.ascii	"higher_priority_task_woken\000"
.LASF122:
	.ascii	"dont_use_et_gage_today\000"
.LASF165:
	.ascii	"epson_i2c1_read_isr\000"
.LASF30:
	.ascii	"command\000"
.LASF75:
	.ascii	"CLKPWR_MAINOSC_CLK\000"
.LASF127:
	.ascii	"rain_switch_active\000"
.LASF79:
	.ascii	"CLKPWR_HCLK\000"
.LASF155:
	.ascii	"isr_bytes_to_be_feed_to_cause_rcv\000"
.LASF173:
	.ascii	"high\000"
.LASF14:
	.ascii	"et_inches_u16_10000u\000"
.LASF105:
	.ascii	"hourly_total_inches_100u\000"
.LASF90:
	.ascii	"i2c_clk_lo\000"
.LASF47:
	.ascii	"CLKPWR_MAC_DMA_CLK\000"
.LASF64:
	.ascii	"CLKPWR_SPI2_CLK\000"
.LASF73:
	.ascii	"CLKPWR_SDRAMDDR_CLK\000"
.LASF188:
	.ascii	"EPSON_set_date_time\000"
.LASF208:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF51:
	.ascii	"CLKPWR_I2C1_CLK\000"
.LASF23:
	.ascii	"__year\000"
.LASF177:
	.ascii	"powerup_status\000"
.LASF7:
	.ascii	"UNS_16\000"
.LASF15:
	.ascii	"status\000"
.LASF125:
	.ascii	"remaining_gage_pulses\000"
.LASF71:
	.ascii	"CLKPWR_UART3_CLK\000"
.LASF6:
	.ascii	"UNS_8\000"
.LASF111:
	.ascii	"RAIN_STATE\000"
.LASF83:
	.ascii	"CLKPWR_DDR_CLK\000"
.LASF146:
	.ascii	"max_day\000"
.LASF117:
	.ascii	"et_rip\000"
.LASF156:
	.ascii	"busy\000"
.LASF197:
	.ascii	"lmin\000"
.LASF53:
	.ascii	"CLKPWR_ADC_CLK\000"
.LASF212:
	.ascii	"Time_and_date_MUTEX\000"
.LASF114:
	.ascii	"dls_eligible_to_fall_back\000"
.LASF103:
	.ascii	"xSemaphoreHandle\000"
.LASF167:
	.ascii	"_epson_read_the_time_and_date\000"
.LASF17:
	.ascii	"rain_inches_u16_100u\000"
.LASF171:
	.ascii	"_epson_rtc_cold_start\000"
.LASF16:
	.ascii	"ET_TABLE_ENTRY\000"
.LASF63:
	.ascii	"CLKPWR_TIMER4_CLK\000"
.LASF84:
	.ascii	"CLKPWR_MSSD_CLK\000"
.LASF142:
	.ascii	"WEATHER_PRESERVES_STRUCT\000"
.LASF176:
	.ascii	"new_seconds\000"
.LASF112:
	.ascii	"verify_string_pre\000"
.LASF207:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF170:
	.ascii	"ptr_rtcq\000"
.LASF143:
	.ascii	"double\000"
.LASF48:
	.ascii	"CLKPWR_MAC_MMIO_CLK\000"
.LASF57:
	.ascii	"CLKPWR_WDOG_CLK\000"
.LASF29:
	.ascii	"DATE_TIME_COMPLETE_STRUCT\000"
.LASF99:
	.ascii	"I2C_PINS_LOW_DRIVE\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
