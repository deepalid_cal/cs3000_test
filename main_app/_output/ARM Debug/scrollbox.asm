	.file	"scrollbox.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_SCROLL_BOX_prevent_redraw,"aw",%nobits
	.align	2
	.type	g_SCROLL_BOX_prevent_redraw, %object
	.size	g_SCROLL_BOX_prevent_redraw, 4
g_SCROLL_BOX_prevent_redraw:
	.space	4
	.section	.text.FDTO_SCROLL_BOX_down,"ax",%progbits
	.align	2
	.type	FDTO_SCROLL_BOX_down, %function
FDTO_SCROLL_BOX_down:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/scrollbox.c"
	.loc 1 77 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 79 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Down
	mov	r3, r0
	cmp	r3, #1
	bne	.L2
	.loc 1 81 0
	bl	good_key_beep
	.loc 1 83 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L3
	.loc 1 87 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Redraw
	.loc 1 88 0
	bl	GuiLib_Refresh
.L3:
	.loc 1 91 0
	mov	r0, #1
	bl	vTaskDelay
	b	.L1
.L2:
	.loc 1 95 0
	bl	bad_key_beep
.L1:
	.loc 1 97 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	g_SCROLL_BOX_prevent_redraw
.LFE0:
	.size	FDTO_SCROLL_BOX_down, .-FDTO_SCROLL_BOX_down
	.section	.text.FDTO_SCROLL_BOX_up,"ax",%progbits
	.align	2
	.type	FDTO_SCROLL_BOX_up, %function
FDTO_SCROLL_BOX_up:
.LFB1:
	.loc 1 121 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 123 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Up
	mov	r3, r0
	cmp	r3, #1
	bne	.L8
	.loc 1 125 0
	bl	good_key_beep
	.loc 1 127 0
	ldr	r3, .L11
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	.loc 1 131 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Redraw
	.loc 1 132 0
	bl	GuiLib_Refresh
.L9:
	.loc 1 135 0
	mov	r0, #1
	bl	vTaskDelay
	b	.L7
.L8:
	.loc 1 139 0
	bl	bad_key_beep
.L7:
	.loc 1 141 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L12:
	.align	2
.L11:
	.word	g_SCROLL_BOX_prevent_redraw
.LFE1:
	.size	FDTO_SCROLL_BOX_up, .-FDTO_SCROLL_BOX_up
	.section	.text.FDTO_SCROLL_BOX_to_line,"ax",%progbits
	.align	2
	.global	FDTO_SCROLL_BOX_to_line
	.type	FDTO_SCROLL_BOX_to_line, %function
FDTO_SCROLL_BOX_to_line:
.LFB2:
	.loc 1 164 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 165 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r2, r3
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r2
	mov	r1, r3
	bl	GuiLib_ScrollBox_To_Line
	mov	r3, r0
	cmp	r3, #1
	bne	.L13
	.loc 1 167 0
	ldr	r3, .L16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L15
	.loc 1 171 0
	ldr	r3, .L16+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 175 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Redraw
	.loc 1 176 0
	bl	GuiLib_Refresh
.L15:
	.loc 1 179 0
	mov	r0, #1
	bl	vTaskDelay
.L13:
	.loc 1 185 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L17:
	.align	2
.L16:
	.word	g_SCROLL_BOX_prevent_redraw
	.word	GuiVar_ScrollBoxHorizScrollPos
.LFE2:
	.size	FDTO_SCROLL_BOX_to_line, .-FDTO_SCROLL_BOX_to_line
	.section	.text.FDTO_SCROLL_BOX_redraw,"ax",%progbits
	.align	2
	.type	FDTO_SCROLL_BOX_redraw, %function
FDTO_SCROLL_BOX_redraw:
.LFB3:
	.loc 1 205 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #4
.LCFI11:
	str	r0, [fp, #-8]
	.loc 1 206 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Redraw
	.loc 1 207 0
	bl	GuiLib_Refresh
	.loc 1 208 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE3:
	.size	FDTO_SCROLL_BOX_redraw, .-FDTO_SCROLL_BOX_redraw
	.section	.text.FDTO_SCROLL_BOX_redraw_retaining_topline,"ax",%progbits
	.align	2
	.global	FDTO_SCROLL_BOX_redraw_retaining_topline
	.type	FDTO_SCROLL_BOX_redraw_retaining_topline, %function
FDTO_SCROLL_BOX_redraw_retaining_topline:
.LFB4:
	.loc 1 251 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #12
.LCFI14:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 252 0
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L20
	.loc 1 254 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L21
	.loc 1 254 0 is_stmt 0 discriminator 1
	bl	DIALOG_a_dialog_is_visible
	mov	r3, r0
	cmp	r3, #0
	bne	.L21
	.loc 1 256 0 is_stmt 1
	mov	r0, #0
	bl	FDTO_Redraw_Screen
	b	.L19
.L21:
	.loc 1 260 0
	bl	GuiLib_Refresh
	b	.L19
.L20:
	.loc 1 265 0
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L24
	.loc 1 267 0
	bl	DIALOG_a_dialog_is_visible
	mov	r3, r0
	cmp	r3, #0
	bne	.L19
	.loc 1 269 0
	mov	r0, #0
	bl	FDTO_Redraw_Screen
	b	.L19
.L24:
	.loc 1 274 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Redraw
	.loc 1 277 0
	bl	GuiLib_Refresh
.L19:
	.loc 1 280 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE4:
	.size	FDTO_SCROLL_BOX_redraw_retaining_topline, .-FDTO_SCROLL_BOX_redraw_retaining_topline
	.section	.text.FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw,"ax",%progbits
	.align	2
	.type	FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw, %function
FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw:
.LFB5:
	.loc 1 284 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI15:
	add	fp, sp, #0
.LCFI16:
	sub	sp, sp, #4
.LCFI17:
	str	r0, [fp, #-4]
	.loc 1 285 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	movne	r2, #0
	moveq	r2, #1
	ldr	r3, .L26
	str	r2, [r3, #0]
	.loc 1 286 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L27:
	.align	2
.L26:
	.word	g_SCROLL_BOX_prevent_redraw
.LFE5:
	.size	FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw, .-FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.section	.text.SCROLL_BOX_redraw,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_redraw
	.type	SCROLL_BOX_redraw, %function
SCROLL_BOX_redraw:
.LFB6:
	.loc 1 309 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #40
.LCFI20:
	str	r0, [fp, #-44]
	.loc 1 312 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 313 0
	ldr	r3, .L29
	str	r3, [fp, #-20]
	.loc 1 314 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 315 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 316 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L30:
	.align	2
.L29:
	.word	FDTO_SCROLL_BOX_redraw
.LFE6:
	.size	SCROLL_BOX_redraw, .-SCROLL_BOX_redraw
	.section	.text.SCROLL_BOX_redraw_line,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_redraw_line
	.type	SCROLL_BOX_redraw_line, %function
SCROLL_BOX_redraw_line:
.LFB7:
	.loc 1 320 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #44
.LCFI23:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	.loc 1 323 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 324 0
	ldr	r3, .L32
	str	r3, [fp, #-20]
	.loc 1 325 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 326 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-12]
	.loc 1 327 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 328 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L33:
	.align	2
.L32:
	.word	GuiLib_ScrollBox_RedrawLine
.LFE7:
	.size	SCROLL_BOX_redraw_line, .-SCROLL_BOX_redraw_line
	.section	.text.SCROLL_BOX_up_or_down,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_up_or_down
	.type	SCROLL_BOX_up_or_down, %function
SCROLL_BOX_up_or_down:
.LFB8:
	.loc 1 354 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI24:
	add	fp, sp, #4
.LCFI25:
	sub	sp, sp, #44
.LCFI26:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	.loc 1 357 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 359 0
	ldr	r3, [fp, #-48]
	cmp	r3, #4
	beq	.L35
	.loc 1 359 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-48]
	cmp	r3, #84
	bne	.L36
.L35:
	.loc 1 361 0 is_stmt 1
	ldr	r3, .L38
	str	r3, [fp, #-20]
	b	.L37
.L36:
	.loc 1 365 0
	ldr	r3, .L38+4
	str	r3, [fp, #-20]
.L37:
	.loc 1 368 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 370 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 371 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L39:
	.align	2
.L38:
	.word	FDTO_SCROLL_BOX_up
	.word	FDTO_SCROLL_BOX_down
.LFE8:
	.size	SCROLL_BOX_up_or_down, .-SCROLL_BOX_up_or_down
	.section	.text.SCROLL_BOX_to_line,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_to_line
	.type	SCROLL_BOX_to_line, %function
SCROLL_BOX_to_line:
.LFB9:
	.loc 1 375 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #44
.LCFI29:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	.loc 1 378 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 379 0
	ldr	r3, .L41
	str	r3, [fp, #-20]
	.loc 1 380 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 381 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-12]
	.loc 1 382 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 383 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L42:
	.align	2
.L41:
	.word	FDTO_SCROLL_BOX_to_line
.LFE9:
	.size	SCROLL_BOX_to_line, .-SCROLL_BOX_to_line
	.section	.text.SCROLL_BOX_close_all_scroll_boxes,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_close_all_scroll_boxes
	.type	SCROLL_BOX_close_all_scroll_boxes, %function
SCROLL_BOX_close_all_scroll_boxes:
.LFB10:
	.loc 1 387 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #4
.LCFI32:
	.loc 1 391 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L44
.L46:
	.loc 1 393 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r0, r3
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r3, r0
	cmp	r3, #0
	blt	.L45
	.loc 1 395 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mov	r0, r3
	bl	GuiLib_ScrollBox_Close
.L45:
	.loc 1 391 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L44:
	.loc 1 391 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	bls	.L46
	.loc 1 400 0 is_stmt 1
	ldr	r3, .L47
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 401 0
	ldr	r3, .L47+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 402 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L48:
	.align	2
.L47:
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	GuiVar_ScrollBoxHorizScrollMarker
.LFE10:
	.size	SCROLL_BOX_close_all_scroll_boxes, .-SCROLL_BOX_close_all_scroll_boxes
	.section	.text.SCROLL_BOX_toggle_scroll_box_automatic_redraw,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.type	SCROLL_BOX_toggle_scroll_box_automatic_redraw, %function
SCROLL_BOX_toggle_scroll_box_automatic_redraw:
.LFB11:
	.loc 1 441 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI33:
	add	fp, sp, #4
.LCFI34:
	sub	sp, sp, #40
.LCFI35:
	str	r0, [fp, #-44]
	.loc 1 444 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 445 0
	ldr	r3, .L50
	str	r3, [fp, #-20]
	.loc 1 446 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-16]
	.loc 1 447 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 448 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L51:
	.align	2
.L50:
	.word	FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw
.LFE11:
	.size	SCROLL_BOX_toggle_scroll_box_automatic_redraw, .-SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x4ee
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF55
	.byte	0x1
	.4byte	.LASF56
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x62
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x74
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x74
	.uleb128 0x5
	.byte	0x4
	.4byte	0xa1
	.uleb128 0x6
	.4byte	0xa8
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x35
	.4byte	0xa8
	.uleb128 0x8
	.4byte	0x37
	.4byte	0xd1
	.uleb128 0x9
	.4byte	0xa8
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x8
	.byte	0x4
	.byte	0x7c
	.4byte	0xf6
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x7e
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x80
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x4
	.byte	0x82
	.4byte	0xd1
	.uleb128 0xa
	.byte	0x24
	.byte	0x5
	.byte	0x78
	.4byte	0x188
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x5
	.byte	0x7b
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x5
	.byte	0x83
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x5
	.byte	0x86
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x5
	.byte	0x88
	.4byte	0x199
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x5
	.byte	0x8d
	.4byte	0x1ab
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x5
	.byte	0x92
	.4byte	0x9b
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x5
	.byte	0x96
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x5
	.byte	0x9a
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x5
	.byte	0x9c
	.4byte	0x69
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	0x194
	.uleb128 0xd
	.4byte	0x194
	.byte	0
	.uleb128 0xe
	.4byte	0x57
	.uleb128 0x5
	.byte	0x4
	.4byte	0x188
	.uleb128 0xc
	.byte	0x1
	.4byte	0x1ab
	.uleb128 0xd
	.4byte	0xf6
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x19f
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x5
	.byte	0x9e
	.4byte	0x101
	.uleb128 0xf
	.4byte	.LASF29
	.byte	0x1
	.byte	0x4c
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x1e3
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0x4c
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	0x69
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x1
	.byte	0x78
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x20f
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0x78
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.byte	0xa3
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x245
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0xa3
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LASF32
	.byte	0x1
	.byte	0xa3
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x1
	.byte	0xcc
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x26c
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0xcc
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.byte	0xf8
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x2b0
	.uleb128 0x10
	.4byte	.LASF31
	.byte	0x1
	.byte	0xf8
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LASF36
	.byte	0x1
	.byte	0xf9
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x10
	.4byte	.LASF37
	.byte	0x1
	.byte	0xfa
	.4byte	0x2b0
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xe
	.4byte	0x90
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x11b
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x2de
	.uleb128 0x13
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x11b
	.4byte	0x2b0
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x134
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x317
	.uleb128 0x13
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x134
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x15
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x136
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x13f
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x35f
	.uleb128 0x13
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x13f
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x13
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x13f
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x15
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x141
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x161
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x3a7
	.uleb128 0x13
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x161
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x13
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x161
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x15
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x163
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x176
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x3ef
	.uleb128 0x13
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x176
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x13
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x176
	.4byte	0x1e3
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x15
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x178
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x182
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0x417
	.uleb128 0x15
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x184
	.4byte	0x69
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x1b8
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0x450
	.uleb128 0x13
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x1b8
	.4byte	0x2b0
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x15
	.ascii	"lde\000"
	.byte	0x1
	.2byte	0x1ba
	.4byte	0x1b1
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x16
	.4byte	.LASF48
	.byte	0x6
	.2byte	0x3c9
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF49
	.byte	0x6
	.2byte	0x3ca
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF50
	.byte	0x7
	.byte	0x30
	.4byte	0x47d
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xe
	.4byte	0xc1
	.uleb128 0x17
	.4byte	.LASF51
	.byte	0x7
	.byte	0x34
	.4byte	0x493
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xe
	.4byte	0xc1
	.uleb128 0x17
	.4byte	.LASF52
	.byte	0x7
	.byte	0x36
	.4byte	0x4a9
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xe
	.4byte	0xc1
	.uleb128 0x17
	.4byte	.LASF53
	.byte	0x7
	.byte	0x38
	.4byte	0x4bf
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xe
	.4byte	0xc1
	.uleb128 0x17
	.4byte	.LASF54
	.byte	0x1
	.byte	0x32
	.4byte	0x90
	.byte	0x5
	.byte	0x3
	.4byte	g_SCROLL_BOX_prevent_redraw
	.uleb128 0x16
	.4byte	.LASF48
	.byte	0x6
	.2byte	0x3c9
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF49
	.byte	0x6
	.2byte	0x3ca
	.4byte	0x7b
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF50:
	.ascii	"GuiFont_LanguageActive\000"
.LASF49:
	.ascii	"GuiVar_ScrollBoxHorizScrollPos\000"
.LASF56:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/s"
	.ascii	"crollbox.c\000"
.LASF42:
	.ascii	"pline_index\000"
.LASF24:
	.ascii	"_04_func_ptr\000"
.LASF45:
	.ascii	"SCROLL_BOX_to_line\000"
.LASF15:
	.ascii	"portTickType\000"
.LASF20:
	.ascii	"_02_menu\000"
.LASF23:
	.ascii	"key_process_func_ptr\000"
.LASF47:
	.ascii	"SCROLL_BOX_toggle_scroll_box_automatic_redraw\000"
.LASF16:
	.ascii	"keycode\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF43:
	.ascii	"SCROLL_BOX_up_or_down\000"
.LASF22:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF18:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF48:
	.ascii	"GuiVar_ScrollBoxHorizScrollMarker\000"
.LASF29:
	.ascii	"FDTO_SCROLL_BOX_down\000"
.LASF27:
	.ascii	"_08_screen_to_draw\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF40:
	.ascii	"SCROLL_BOX_redraw\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF44:
	.ascii	"pkeycode\000"
.LASF30:
	.ascii	"FDTO_SCROLL_BOX_up\000"
.LASF13:
	.ascii	"long unsigned int\000"
.LASF53:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF19:
	.ascii	"_01_command\000"
.LASF28:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF55:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF51:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF34:
	.ascii	"FDTO_SCROLL_BOX_to_line\000"
.LASF31:
	.ascii	"pscrollbox_index\000"
.LASF54:
	.ascii	"g_SCROLL_BOX_prevent_redraw\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF36:
	.ascii	"pnumber_of_lines\000"
.LASF52:
	.ascii	"GuiFont_DecimalChar\000"
.LASF17:
	.ascii	"repeats\000"
.LASF41:
	.ascii	"SCROLL_BOX_redraw_line\000"
.LASF32:
	.ascii	"pnew_scroll_line\000"
.LASF39:
	.ascii	"pallow_redraw\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF25:
	.ascii	"_06_u32_argument1\000"
.LASF11:
	.ascii	"long long int\000"
.LASF0:
	.ascii	"char\000"
.LASF26:
	.ascii	"_07_u32_argument2\000"
.LASF33:
	.ascii	"FDTO_SCROLL_BOX_redraw\000"
.LASF37:
	.ascii	"predraw_entire_scrollbox\000"
.LASF38:
	.ascii	"FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw\000"
.LASF7:
	.ascii	"short int\000"
.LASF21:
	.ascii	"_03_structure_to_draw\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF14:
	.ascii	"long int\000"
.LASF35:
	.ascii	"FDTO_SCROLL_BOX_redraw_retaining_topline\000"
.LASF46:
	.ascii	"SCROLL_BOX_close_all_scroll_boxes\000"
.LASF2:
	.ascii	"signed char\000"
.LASF4:
	.ascii	"UNS_16\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
