	.file	"e_budget_reduction_limits.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.text.REDUCTION_LIMIT_process_percentage,"ax",%progbits
	.align	2
	.type	REDUCTION_LIMIT_process_percentage, %function
REDUCTION_LIMIT_process_percentage:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_budget_reduction_limits.c"
	.loc 1 56 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #28
.LCFI2:
	str	r0, [fp, #-20]
	str	r1, [fp, #-16]
	str	r2, [fp, #-24]
	.loc 1 61 0
	ldr	r3, [fp, #-16]
	cmp	r3, #72
	bls	.L2
	.loc 1 63 0
	mov	r3, #4
	str	r3, [fp, #-8]
	b	.L3
.L2:
	.loc 1 65 0
	ldr	r3, [fp, #-16]
	cmp	r3, #48
	bls	.L4
	.loc 1 67 0
	mov	r3, #2
	str	r3, [fp, #-8]
	b	.L3
.L4:
	.loc 1 71 0
	mov	r3, #1
	str	r3, [fp, #-8]
.L3:
	.loc 1 76 0
	ldr	r3, [fp, #-20]
	cmp	r3, #80
	bne	.L5
	.loc 1 76 0 is_stmt 0 discriminator 1
	mov	r3, #84
	b	.L6
.L5:
	.loc 1 76 0 discriminator 2
	mov	r3, #80
.L6:
	.loc 1 76 0 discriminator 3
	str	r3, [fp, #-12]
	.loc 1 78 0 is_stmt 1 discriminator 3
	ldr	r3, [fp, #-8]
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-24]
	mvn	r2, #99
	mov	r3, #0
	bl	process_int32
	.loc 1 83 0 discriminator 3
	bl	Refresh_Screen
	.loc 1 84 0 discriminator 3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE0:
	.size	REDUCTION_LIMIT_process_percentage, .-REDUCTION_LIMIT_process_percentage
	.section	.text.FDTO_BUDGET_REDUCTION_LIMITS_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_BUDGET_REDUCTION_LIMITS_draw_screen
	.type	FDTO_BUDGET_REDUCTION_LIMITS_draw_screen, %function
FDTO_BUDGET_REDUCTION_LIMITS_draw_screen:
.LFB1:
	.loc 1 88 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-12]
	.loc 1 91 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L8
	.loc 1 93 0
	bl	BUDGET_REDUCTION_LIMITS_copy_group_into_guivars
	.loc 1 95 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L9
.L8:
	.loc 1 99 0
	ldr	r3, .L10
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L9:
	.loc 1 102 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #11
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 103 0
	bl	GuiLib_Refresh
	.loc 1 104 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L11:
	.align	2
.L10:
	.word	GuiLib_ActiveCursorFieldNo
.LFE1:
	.size	FDTO_BUDGET_REDUCTION_LIMITS_draw_screen, .-FDTO_BUDGET_REDUCTION_LIMITS_draw_screen
	.section	.text.BUDGET_REDUCTION_LIMIT_process_screen,"ax",%progbits
	.align	2
	.global	BUDGET_REDUCTION_LIMIT_process_screen
	.type	BUDGET_REDUCTION_LIMIT_process_screen, %function
BUDGET_REDUCTION_LIMIT_process_screen:
.LFB2:
	.loc 1 108 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI6:
	add	fp, sp, #8
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-16]
	str	r1, [fp, #-12]
	.loc 1 110 0
	ldr	r3, [fp, #-16]
	cmp	r3, #84
	ldrls	pc, [pc, r3, asl #2]
	b	.L13
.L20:
	.word	.L14
	.word	.L15
	.word	.L13
	.word	.L16
	.word	.L17
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L17
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L14
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L18
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L19
	.word	.L13
	.word	.L13
	.word	.L13
	.word	.L19
.L19:
	.loc 1 114 0
	ldr	r3, .L43
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L21
.L32:
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
.L22:
	.loc 1 117 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+4
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 118 0
	b	.L33
.L23:
	.loc 1 121 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+8
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 122 0
	b	.L33
.L24:
	.loc 1 125 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+12
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 126 0
	b	.L33
.L25:
	.loc 1 129 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+16
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 130 0
	b	.L33
.L26:
	.loc 1 133 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+20
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 134 0
	b	.L33
.L27:
	.loc 1 137 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+24
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 138 0
	b	.L33
.L28:
	.loc 1 141 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+28
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 142 0
	b	.L33
.L29:
	.loc 1 145 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+32
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 146 0
	b	.L33
.L30:
	.loc 1 149 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+36
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 150 0
	b	.L33
.L31:
	.loc 1 153 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	ldr	r2, .L43+40
	bl	REDUCTION_LIMIT_process_percentage
	.loc 1 154 0
	b	.L33
.L21:
	.loc 1 157 0
	bl	bad_key_beep
	.loc 1 159 0
	b	.L12
.L33:
	b	.L12
.L17:
	.loc 1 163 0
	ldr	r3, .L43
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L43+44
	smull	r1, r3, r2, r3
	mov	r1, r3, asr #2
	mov	r3, r2, asr #31
	rsb	r1, r3, r1
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #1
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #0
	bne	.L35
	.loc 1 165 0
	bl	bad_key_beep
	.loc 1 171 0
	b	.L12
.L35:
	.loc 1 169 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 171 0
	b	.L12
.L14:
	.loc 1 175 0
	ldr	r3, .L43
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	ldr	r3, .L43+44
	smull	r1, r3, r2, r3
	mov	r1, r3, asr #2
	mov	r3, r2, asr #31
	rsb	r1, r3, r1
	mov	r3, r1
	mov	r3, r3, asl #2
	add	r3, r3, r1
	mov	r3, r3, asl #1
	rsb	r3, r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r4, r3, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r3, r0
	sub	r3, r3, #1
	cmp	r4, r3
	bne	.L37
	.loc 1 177 0
	bl	bad_key_beep
	.loc 1 183 0
	b	.L12
.L37:
	.loc 1 181 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 183 0
	b	.L12
.L15:
	.loc 1 186 0
	ldr	r3, .L43
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L39
	.loc 1 186 0 is_stmt 0 discriminator 1
	ldr	r3, .L43
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	bgt	.L39
	.loc 1 188 0 is_stmt 1
	ldr	r3, .L43
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #9
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 194 0
	b	.L12
.L39:
	.loc 1 192 0
	bl	bad_key_beep
	.loc 1 194 0
	b	.L12
.L16:
	.loc 1 197 0
	ldr	r3, .L43
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	blt	.L41
	.loc 1 197 0 is_stmt 0 discriminator 1
	ldr	r3, .L43
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #9
	bgt	.L41
	.loc 1 199 0 is_stmt 1
	ldr	r3, .L43
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, #10
	mov	r0, r3
	mov	r1, #1
	bl	CURSOR_Select
	.loc 1 205 0
	b	.L12
.L41:
	.loc 1 203 0
	bl	bad_key_beep
	.loc 1 205 0
	b	.L12
.L18:
	.loc 1 208 0
	ldr	r3, .L43+48
	mov	r2, #5
	str	r2, [r3, #0]
	.loc 1 210 0
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars
.L13:
	.loc 1 215 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L12:
	.loc 1 217 0
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L44:
	.align	2
.L43:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_BudgetReductionLimit_0
	.word	GuiVar_BudgetReductionLimit_1
	.word	GuiVar_BudgetReductionLimit_2
	.word	GuiVar_BudgetReductionLimit_3
	.word	GuiVar_BudgetReductionLimit_4
	.word	GuiVar_BudgetReductionLimit_5
	.word	GuiVar_BudgetReductionLimit_6
	.word	GuiVar_BudgetReductionLimit_7
	.word	GuiVar_BudgetReductionLimit_8
	.word	GuiVar_BudgetReductionLimit_9
	.word	1717986919
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	BUDGET_REDUCTION_LIMIT_process_screen, .-BUDGET_REDUCTION_LIMIT_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x349
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF41
	.byte	0x1
	.4byte	.LASF42
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x53
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x67
	.4byte	0x77
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x65
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x5
	.4byte	0x33
	.4byte	0xae
	.uleb128 0x6
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.byte	0x3
	.byte	0x7c
	.4byte	0xd3
	.uleb128 0x8
	.4byte	.LASF14
	.byte	0x3
	.byte	0x7e
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF15
	.byte	0x3
	.byte	0x80
	.4byte	0x5a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x82
	.4byte	0xae
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF17
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x1
	.byte	0x37
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x136
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x1
	.byte	0x37
	.4byte	0x136
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xa
	.4byte	.LASF19
	.byte	0x1
	.byte	0x37
	.4byte	0x13b
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x1
	.byte	0x39
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x1
	.byte	0x3b
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0xc
	.4byte	0xd3
	.uleb128 0xd
	.byte	0x4
	.4byte	0x6c
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.byte	0x57
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x177
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x1
	.byte	0x57
	.4byte	0x177
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x1
	.byte	0x59
	.4byte	0x5a
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xc
	.4byte	0x8c
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.byte	0x6b
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x1a4
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x1
	.byte	0x6b
	.4byte	0x136
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0xf
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x128
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x129
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x12a
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x12b
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x12c
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x12d
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x12e
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x12f
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x130
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x131
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x6
	.byte	0x30
	.4byte	0x25d
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xc
	.4byte	0x9e
	.uleb128 0xb
	.4byte	.LASF38
	.byte	0x6
	.byte	0x34
	.4byte	0x273
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xc
	.4byte	0x9e
	.uleb128 0xb
	.4byte	.LASF39
	.byte	0x6
	.byte	0x36
	.4byte	0x289
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xc
	.4byte	0x9e
	.uleb128 0xb
	.4byte	.LASF40
	.byte	0x6
	.byte	0x38
	.4byte	0x29f
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xc
	.4byte	0x9e
	.uleb128 0xf
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x128
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x129
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x12a
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x12b
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x12c
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x12d
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x12e
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x12f
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x130
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x131
	.4byte	0x77
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2ec
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x5
	.2byte	0x127
	.4byte	0x53
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF11:
	.ascii	"long long int\000"
.LASF37:
	.ascii	"GuiFont_LanguageActive\000"
.LASF23:
	.ascii	"FDTO_BUDGET_REDUCTION_LIMITS_draw_screen\000"
.LASF21:
	.ascii	"pcomplete_redraw\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF32:
	.ascii	"GuiVar_BudgetReductionLimit_7\000"
.LASF35:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF42:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budget_reduction_limits.c\000"
.LASF17:
	.ascii	"float\000"
.LASF22:
	.ascii	"lcursor_to_select\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF9:
	.ascii	"INT_32\000"
.LASF19:
	.ascii	"ppercentage\000"
.LASF40:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF43:
	.ascii	"REDUCTION_LIMIT_process_percentage\000"
.LASF41:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF38:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF18:
	.ascii	"pkey_event\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF15:
	.ascii	"repeats\000"
.LASF25:
	.ascii	"GuiVar_BudgetReductionLimit_0\000"
.LASF26:
	.ascii	"GuiVar_BudgetReductionLimit_1\000"
.LASF27:
	.ascii	"GuiVar_BudgetReductionLimit_2\000"
.LASF28:
	.ascii	"GuiVar_BudgetReductionLimit_3\000"
.LASF29:
	.ascii	"GuiVar_BudgetReductionLimit_4\000"
.LASF30:
	.ascii	"GuiVar_BudgetReductionLimit_5\000"
.LASF31:
	.ascii	"GuiVar_BudgetReductionLimit_6\000"
.LASF16:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF33:
	.ascii	"GuiVar_BudgetReductionLimit_8\000"
.LASF34:
	.ascii	"GuiVar_BudgetReductionLimit_9\000"
.LASF1:
	.ascii	"char\000"
.LASF20:
	.ascii	"inc_by\000"
.LASF5:
	.ascii	"short int\000"
.LASF14:
	.ascii	"keycode\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF24:
	.ascii	"BUDGET_REDUCTION_LIMIT_process_screen\000"
.LASF13:
	.ascii	"long int\000"
.LASF3:
	.ascii	"signed char\000"
.LASF36:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF39:
	.ascii	"GuiFont_DecimalChar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
