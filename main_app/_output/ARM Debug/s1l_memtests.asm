	.file	"s1l_memtests.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.data.w1s_msg,"aw",%progbits
	.align	2
	.type	w1s_msg, %object
	.size	w1s_msg, 26
w1s_msg:
	.ascii	"Starting walking 1 test\012\015\000"
	.section	.data.w0s_msg,"aw",%progbits
	.align	2
	.type	w0s_msg, %object
	.size	w0s_msg, 26
w0s_msg:
	.ascii	"Starting walking 0 test\012\015\000"
	.section	.data.invtst_msg,"aw",%progbits
	.align	2
	.type	invtst_msg, %object
	.size	invtst_msg, 23
invtst_msg:
	.ascii	"Inverse address test\012\015\000"
	.section	.data.pattst_msg,"aw",%progbits
	.align	2
	.type	pattst_msg, %object
	.size	pattst_msg, 15
pattst_msg:
	.ascii	"Pattern test\012\015\000"
	.section	.data.invwidth_msg,"aw",%progbits
	.align	2
	.type	invwidth_msg, %object
	.size	invwidth_msg, 42
invwidth_msg:
	.ascii	"Invalid width, must be 1, 2, or 4 bytes\012\015\000"
	.section	.data.addr_msg,"aw",%progbits
	.align	2
	.type	addr_msg, %object
	.size	addr_msg, 15
addr_msg:
	.ascii	"Address      :\000"
	.section	.data.width_msg,"aw",%progbits
	.align	2
	.type	width_msg, %object
	.size	width_msg, 15
width_msg:
	.ascii	"Width (bytes):\000"
	.section	.data.bytes_msg,"aw",%progbits
	.align	2
	.type	bytes_msg, %object
	.size	bytes_msg, 15
bytes_msg:
	.ascii	"Bytes        :\000"
	.section	.data.passed_msg,"aw",%progbits
	.align	2
	.type	passed_msg, %object
	.size	passed_msg, 14
passed_msg:
	.ascii	"Test passed\012\015\000"
	.section	.data.failed1_msg,"aw",%progbits
	.align	2
	.type	failed1_msg, %object
	.size	failed1_msg, 24
failed1_msg:
	.ascii	"Test failed at address \000"
	.section	.data.failed2_msg,"aw",%progbits
	.align	2
	.type	failed2_msg, %object
	.size	failed2_msg, 19
failed2_msg:
	.ascii	"Expected pattern: \000"
	.section	.data.failed3_msg,"aw",%progbits
	.align	2
	.type	failed3_msg, %object
	.size	failed3_msg, 19
failed3_msg:
	.ascii	"Actual pattern  : \000"
	.section	.data.start_msg,"aw",%progbits
	.align	2
	.type	start_msg, %object
	.size	start_msg, 18
start_msg:
	.ascii	"Starting test....\000"
	.section	.data.verify_msg,"aw",%progbits
	.align	2
	.type	verify_msg, %object
	.size	verify_msg, 19
verify_msg:
	.ascii	"Verifying data....\000"
	.section	.data.patts_55aa,"aw",%progbits
	.align	2
	.type	patts_55aa, %object
	.size	patts_55aa, 16
patts_55aa:
	.word	-1431655766
	.word	1431655765
	.word	-1515870811
	.word	1515870810
	.section	.data.patts_00ff,"aw",%progbits
	.align	2
	.type	patts_00ff, %object
	.size	patts_00ff, 8
patts_00ff:
	.word	0
	.word	-1
	.section	.text.memtest_disp_addr,"ax",%progbits
	.align	2
	.global	memtest_disp_addr
	.type	memtest_disp_addr, %function
memtest_disp_addr:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/memtest/s1l_memtests.c"
	.loc 1 83 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #40
.LCFI2:
	str	r0, [fp, #-32]
	str	r1, [fp, #-36]
	str	r2, [fp, #-40]
	str	r3, [fp, #-44]
	.loc 1 86 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 88 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L2
	.loc 1 88 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L2
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	cmp	r3, #4
	bne	.L3
.L2:
	.loc 1 91 0 is_stmt 1
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	rsb	r3, r3, #0
	and	r2, r2, r3
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L4
	.loc 1 93 0
	ldr	r3, [fp, #-36]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	rsb	r3, r3, #0
	and	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 94 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 95 0
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #0]
	ldr	r1, [fp, #-12]
	rsb	r3, r3, r1
	add	r2, r2, r3
	ldr	r3, [fp, #-40]
	str	r2, [r3, #0]
	.loc 1 96 0
	ldr	r3, [fp, #-40]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	rsb	r3, r3, #0
	and	r2, r2, r3
	ldr	r3, [fp, #-40]
	str	r2, [r3, #0]
	.loc 1 97 0
	ldr	r3, [fp, #-36]
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
.L4:
	.loc 1 100 0
	ldr	r3, [fp, #-32]
	cmp	r3, #0
	beq	.L5
	.loc 1 102 0
	ldr	r0, [fp, #-32]
	bl	term_dat_out
	.loc 1 104 0
	ldr	r0, .L7
	bl	term_dat_out
	.loc 1 105 0
	ldr	r3, [fp, #-36]
	ldr	r3, [r3, #0]
	sub	r2, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #8
	bl	str_makehex
	.loc 1 106 0
	sub	r3, fp, #28
	mov	r0, r3
	bl	term_dat_out_crlf
	.loc 1 108 0
	ldr	r0, .L7+4
	bl	term_dat_out
	.loc 1 109 0
	ldr	r3, [fp, #-40]
	ldr	r3, [r3, #0]
	sub	r2, fp, #28
	mov	r0, r2
	mov	r1, r3
	bl	str_makedec
	.loc 1 110 0
	sub	r3, fp, #28
	mov	r0, r3
	bl	term_dat_out_crlf
	.loc 1 112 0
	ldr	r0, .L7+8
	bl	term_dat_out
	.loc 1 113 0
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #0]
	sub	r2, fp, #28
	mov	r0, r2
	mov	r1, r3
	bl	str_makedec
	.loc 1 114 0
	sub	r3, fp, #28
	mov	r0, r3
	bl	term_dat_out_crlf
.L5:
	.loc 1 117 0
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L6
.L3:
	.loc 1 121 0
	ldr	r0, .L7+12
	bl	term_dat_out
.L6:
	.loc 1 124 0
	ldr	r3, [fp, #-8]
	.loc 1 125 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L8:
	.align	2
.L7:
	.word	addr_msg
	.word	bytes_msg
	.word	width_msg
	.word	invwidth_msg
.LFE0:
	.size	memtest_disp_addr, .-memtest_disp_addr
	.section	.text.memtest_pf,"ax",%progbits
	.align	2
	.global	memtest_pf
	.type	memtest_pf, %function
memtest_pf:
.LFB1:
	.loc 1 155 0
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #32
.LCFI5:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	str	r3, [fp, #-36]
	.loc 1 158 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	bne	.L10
	.loc 1 160 0
	ldr	r0, .L12
	bl	term_dat_out
	.loc 1 161 0
	ldr	r3, [fp, #-28]
	mov	r3, r3, asl #1
	sub	r2, fp, #20
	mov	r0, r2
	ldr	r1, [fp, #-32]
	mov	r2, r3
	bl	str_makehex
	.loc 1 162 0
	sub	r3, fp, #20
	mov	r0, r3
	bl	term_dat_out_crlf
	.loc 1 164 0
	ldr	r0, .L12+4
	bl	term_dat_out
	.loc 1 165 0
	ldr	r3, [fp, #-28]
	mov	r3, r3, asl #1
	sub	r2, fp, #20
	mov	r0, r2
	ldr	r1, [fp, #4]
	mov	r2, r3
	bl	str_makehex
	.loc 1 166 0
	sub	r3, fp, #20
	mov	r0, r3
	bl	term_dat_out_crlf
	.loc 1 168 0
	ldr	r0, .L12+8
	bl	term_dat_out
	.loc 1 169 0
	ldr	r3, [fp, #-28]
	mov	r3, r3, asl #1
	sub	r2, fp, #20
	mov	r0, r2
	ldr	r1, [fp, #-36]
	mov	r2, r3
	bl	str_makehex
	.loc 1 170 0
	sub	r3, fp, #20
	mov	r0, r3
	bl	term_dat_out_crlf
	b	.L9
.L10:
	.loc 1 174 0
	ldr	r0, .L12+12
	bl	term_dat_out
.L9:
	.loc 1 176 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	failed1_msg
	.word	failed2_msg
	.word	failed3_msg
	.word	passed_msg
.LFE1:
	.size	memtest_pf, .-memtest_pf
	.section	.text.memtest_w10,"ax",%progbits
	.align	2
	.global	memtest_w10
	.type	memtest_w10, %function
memtest_w10:
.LFB2:
	.loc 1 204 0
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #56
.LCFI8:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	str	r2, [fp, #-52]
	str	r3, [fp, #-56]
	.loc 1 211 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L15
	.loc 1 213 0
	sub	r1, fp, #48
	sub	r2, fp, #52
	sub	r3, fp, #56
	ldr	r0, .L49
	bl	memtest_disp_addr
	mov	r3, r0
	cmp	r3, #0
	bne	.L16
	.loc 1 215 0
	b	.L14
.L15:
	.loc 1 220 0
	sub	r1, fp, #48
	sub	r2, fp, #52
	sub	r3, fp, #56
	ldr	r0, .L49+4
	bl	memtest_disp_addr
	mov	r3, r0
	cmp	r3, #0
	beq	.L44
.L16:
	.loc 1 227 0
	ldr	r2, [fp, #-48]
	ldr	r3, [fp, #-52]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 228 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 231 0
	ldr	r0, .L49+8
	bl	term_dat_out
	.loc 1 232 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-8]
	b	.L18
.L30:
	.loc 1 234 0
	ldr	r3, [fp, #-56]
	cmp	r3, #2
	beq	.L21
	cmp	r3, #4
	beq	.L22
	cmp	r3, #1
	bne	.L19
.L20:
	.loc 1 237 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-20]
	.loc 1 238 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L23
	.loc 1 240 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	and	r2, r3, #255
	ldr	r3, [fp, #-20]
	strb	r2, [r3, #0]
	.loc 1 246 0
	b	.L19
.L23:
	.loc 1 244 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	and	r3, r3, #255
	mvn	r3, r3
	and	r2, r3, #255
	ldr	r3, [fp, #-20]
	strb	r2, [r3, #0]
	.loc 1 246 0
	b	.L19
.L21:
	.loc 1 249 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-24]
	.loc 1 250 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L25
	.loc 1 252 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-24]
	strh	r2, [r3, #0]	@ movhi
	.loc 1 258 0
	b	.L19
.L25:
	.loc 1 256 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mvn	r3, r3
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-24]
	strh	r2, [r3, #0]	@ movhi
	.loc 1 258 0
	b	.L19
.L22:
	.loc 1 261 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-28]
	.loc 1 262 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L27
	.loc 1 264 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r2, r2, asl r3
	ldr	r3, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 270 0
	b	.L45
.L27:
	.loc 1 268 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	mvn	r2, r3
	ldr	r3, [fp, #-28]
	str	r2, [r3, #0]
.L45:
	.loc 1 270 0
	mov	r0, r0	@ nop
.L19:
	.loc 1 273 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 274 0
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-56]
	mov	r3, r3, asl #3
	cmp	r2, r3
	bcc	.L29
	.loc 1 276 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L29:
	.loc 1 232 0
	ldr	r3, [fp, #-56]
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
.L18:
	.loc 1 232 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcc	.L30
	.loc 1 281 0 is_stmt 1
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 284 0
	ldr	r0, .L49+12
	bl	term_dat_out
	.loc 1 285 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-8]
	b	.L31
.L43:
	.loc 1 287 0
	ldr	r3, [fp, #-56]
	cmp	r3, #2
	beq	.L34
	cmp	r3, #4
	beq	.L35
	cmp	r3, #1
	bne	.L32
.L33:
	.loc 1 290 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-20]
	.loc 1 291 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L36
	.loc 1 293 0
	ldr	r3, [fp, #-20]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	mov	r1, #1
	ldr	r3, [fp, #-12]
	mov	r3, r1, asl r3
	and	r3, r3, #255
	cmp	r2, r3
	beq	.L46
	.loc 1 295 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r0, #1
	ldr	r1, [fp, #-12]
	mov	r1, r0, asl r1
	str	r1, [sp, #0]
	mov	r0, #0
	mov	r1, #1
	bl	memtest_pf
	.loc 1 297 0
	b	.L14
.L36:
	.loc 1 302 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	and	r3, r3, #255
	mvn	r3, r3
	strb	r3, [fp, #-29]
	.loc 1 303 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrb	r2, [fp, #-29]	@ zero_extendqisi2
	cmp	r2, r3
	beq	.L46
	.loc 1 305 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [fp, #-29]	@ zero_extendqisi2
	str	r1, [sp, #0]
	mov	r0, #0
	mov	r1, #1
	bl	memtest_pf
	.loc 1 307 0
	b	.L14
.L34:
	.loc 1 313 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-24]
	.loc 1 314 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L38
	.loc 1 316 0
	ldr	r3, [fp, #-24]
	ldrh	r2, [r3, #0]
	mov	r1, #1
	ldr	r3, [fp, #-12]
	mov	r3, r1, asl r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r2, r3
	beq	.L47
	.loc 1 318 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #0]
	mov	r0, #1
	ldr	r1, [fp, #-12]
	mov	r1, r0, asl r1
	str	r1, [sp, #0]
	mov	r0, #0
	mov	r1, #2
	bl	memtest_pf
	.loc 1 320 0
	b	.L14
.L38:
	.loc 1 325 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mvn	r3, r3
	strh	r3, [fp, #-32]	@ movhi
	.loc 1 326 0
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #0]
	ldrh	r2, [fp, #-32]
	cmp	r2, r3
	beq	.L47
	.loc 1 328 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #0]
	ldrh	r1, [fp, #-32]
	str	r1, [sp, #0]
	mov	r0, #0
	mov	r1, #2
	bl	memtest_pf
	.loc 1 330 0
	b	.L14
.L35:
	.loc 1 336 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-28]
	.loc 1 337 0
	ldr	r3, [fp, #-44]
	cmp	r3, #1
	bne	.L40
	.loc 1 339 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	str	r3, [fp, #-36]
	.loc 1 340 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-40]
	.loc 1 341 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	beq	.L48
	.loc 1 343 0
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-36]
	str	r2, [sp, #0]
	mov	r0, #0
	mov	r1, #4
	mov	r2, r3
	ldr	r3, [fp, #-40]
	bl	memtest_pf
	.loc 1 345 0
	b	.L14
.L40:
	.loc 1 350 0
	mov	r2, #1
	ldr	r3, [fp, #-12]
	mov	r3, r2, asl r3
	mvn	r3, r3
	str	r3, [fp, #-36]
	.loc 1 351 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-40]
	.loc 1 352 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	beq	.L48
	.loc 1 354 0
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-36]
	str	r2, [sp, #0]
	mov	r0, #0
	mov	r1, #4
	mov	r2, r3
	ldr	r3, [fp, #-40]
	bl	memtest_pf
	.loc 1 355 0
	b	.L14
.L46:
	.loc 1 310 0
	mov	r0, r0	@ nop
	b	.L32
.L47:
	.loc 1 333 0
	mov	r0, r0	@ nop
	b	.L32
.L48:
	.loc 1 358 0
	mov	r0, r0	@ nop
.L32:
	.loc 1 361 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
	.loc 1 362 0
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-56]
	mov	r3, r3, asl #3
	cmp	r2, r3
	bcc	.L42
	.loc 1 364 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L42:
	.loc 1 285 0
	ldr	r3, [fp, #-56]
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
.L31:
	.loc 1 285 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcc	.L43
	.loc 1 369 0 is_stmt 1
	ldr	r3, [fp, #-56]
	mov	r2, #0
	str	r2, [sp, #0]
	mov	r0, #1
	mov	r1, r3
	mov	r2, #0
	mov	r3, #0
	bl	memtest_pf
	b	.L14
.L44:
	.loc 1 222 0
	mov	r0, r0	@ nop
.L14:
	.loc 1 370 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L50:
	.align	2
.L49:
	.word	w1s_msg
	.word	w0s_msg
	.word	start_msg
	.word	verify_msg
.LFE2:
	.size	memtest_w10, .-memtest_w10
	.section	.text.memtest_ia,"ax",%progbits
	.align	2
	.global	memtest_ia
	.type	memtest_ia, %function
memtest_ia:
.LFB3:
	.loc 1 396 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #48
.LCFI11:
	str	r0, [fp, #-40]
	str	r1, [fp, #-44]
	str	r2, [fp, #-48]
	.loc 1 402 0
	sub	r1, fp, #40
	sub	r2, fp, #44
	sub	r3, fp, #48
	ldr	r0, .L73
	bl	memtest_disp_addr
	mov	r3, r0
	cmp	r3, #0
	beq	.L69
.L52:
	.loc 1 408 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-44]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 411 0
	ldr	r0, .L73+4
	bl	term_dat_out
	.loc 1 412 0
	ldr	r3, [fp, #-40]
	str	r3, [fp, #-8]
	b	.L54
.L59:
	.loc 1 414 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	beq	.L57
	cmp	r3, #4
	beq	.L58
	cmp	r3, #1
	bne	.L55
.L56:
	.loc 1 417 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-16]
	.loc 1 418 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mvn	r3, r3
	and	r2, r3, #255
	ldr	r3, [fp, #-16]
	strb	r2, [r3, #0]
	.loc 1 419 0
	b	.L55
.L57:
	.loc 1 422 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-20]
	.loc 1 423 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mvn	r3, r3
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-20]
	strh	r2, [r3, #0]	@ movhi
	.loc 1 424 0
	b	.L55
.L58:
	.loc 1 427 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-24]
	.loc 1 428 0
	ldr	r3, [fp, #-8]
	mvn	r2, r3
	ldr	r3, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 429 0
	mov	r0, r0	@ nop
.L55:
	.loc 1 412 0
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
.L54:
	.loc 1 412 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bcc	.L59
	.loc 1 434 0 is_stmt 1
	ldr	r0, .L73+8
	bl	term_dat_out
	.loc 1 435 0
	ldr	r3, [fp, #-40]
	str	r3, [fp, #-8]
	b	.L60
.L68:
	.loc 1 437 0
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	beq	.L63
	cmp	r3, #4
	beq	.L64
	cmp	r3, #1
	bne	.L61
.L62:
	.loc 1 440 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-16]
	.loc 1 441 0
	ldr	r3, [fp, #-8]
	and	r3, r3, #255
	mvn	r3, r3
	strb	r3, [fp, #-25]
	.loc 1 442 0
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrb	r2, [fp, #-25]	@ zero_extendqisi2
	cmp	r2, r3
	beq	.L70
	.loc 1 444 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-16]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [fp, #-25]	@ zero_extendqisi2
	str	r1, [sp, #0]
	mov	r0, #0
	mov	r1, #1
	bl	memtest_pf
	.loc 1 447 0
	b	.L70
.L63:
	.loc 1 450 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-20]
	.loc 1 451 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mvn	r3, r3
	strh	r3, [fp, #-28]	@ movhi
	.loc 1 452 0
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #0]
	ldrh	r2, [fp, #-28]
	cmp	r2, r3
	beq	.L71
	.loc 1 454 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-20]
	ldrh	r3, [r3, #0]
	ldrh	r1, [fp, #-28]
	str	r1, [sp, #0]
	mov	r0, #0
	mov	r1, #2
	bl	memtest_pf
	.loc 1 457 0
	b	.L71
.L64:
	.loc 1 460 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-24]
	.loc 1 461 0
	ldr	r3, [fp, #-8]
	mvn	r3, r3
	str	r3, [fp, #-32]
	.loc 1 462 0
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-36]
	.loc 1 463 0
	ldr	r2, [fp, #-36]
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	beq	.L72
	.loc 1 465 0
	ldr	r3, [fp, #-24]
	ldr	r2, [fp, #-32]
	str	r2, [sp, #0]
	mov	r0, #0
	mov	r1, #4
	mov	r2, r3
	ldr	r3, [fp, #-36]
	bl	memtest_pf
	.loc 1 468 0
	b	.L72
.L70:
	.loc 1 447 0
	mov	r0, r0	@ nop
	b	.L61
.L71:
	.loc 1 457 0
	mov	r0, r0	@ nop
	b	.L61
.L72:
	.loc 1 468 0
	mov	r0, r0	@ nop
.L61:
	.loc 1 435 0
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
.L60:
	.loc 1 435 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	cmp	r2, r3
	bcc	.L68
	.loc 1 473 0 is_stmt 1
	ldr	r3, [fp, #-48]
	mov	r2, #0
	str	r2, [sp, #0]
	mov	r0, #1
	mov	r1, r3
	mov	r2, #0
	mov	r3, #0
	bl	memtest_pf
	b	.L51
.L69:
	.loc 1 404 0
	mov	r0, r0	@ nop
.L51:
	.loc 1 474 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L74:
	.align	2
.L73:
	.word	invtst_msg
	.word	start_msg
	.word	verify_msg
.LFE3:
	.size	memtest_ia, .-memtest_ia
	.section	.text.memtest_pt,"ax",%progbits
	.align	2
	.global	memtest_pt
	.type	memtest_pt, %function
memtest_pt:
.LFB4:
	.loc 1 504 0
	@ args = 4, pretend = 0, frame = 52
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #56
.LCFI14:
	str	r0, [fp, #-44]
	str	r1, [fp, #-48]
	str	r2, [fp, #-52]
	str	r3, [fp, #-56]
	.loc 1 511 0
	sub	r1, fp, #44
	sub	r2, fp, #48
	sub	r3, fp, #52
	ldr	r0, .L99
	bl	memtest_disp_addr
	mov	r3, r0
	cmp	r3, #0
	beq	.L95
.L76:
	.loc 1 517 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-48]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 518 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 520 0
	b	.L78
.L94:
	.loc 1 523 0
	ldr	r0, .L99+4
	bl	term_dat_out
	.loc 1 524 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-8]
	b	.L79
.L84:
	.loc 1 526 0
	ldr	r3, [fp, #-52]
	cmp	r3, #2
	beq	.L82
	cmp	r3, #4
	beq	.L83
	cmp	r3, #1
	bne	.L80
.L81:
	.loc 1 529 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-20]
	.loc 1 530 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-56]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	and	r2, r3, #255
	ldr	r3, [fp, #-20]
	strb	r2, [r3, #0]
	.loc 1 531 0
	b	.L80
.L82:
	.loc 1 534 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-24]
	.loc 1 535 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-56]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, lsr #16
	ldr	r3, [fp, #-24]
	strh	r2, [r3, #0]	@ movhi
	.loc 1 536 0
	b	.L80
.L83:
	.loc 1 539 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-28]
	.loc 1 540 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-56]
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 541 0
	mov	r0, r0	@ nop
.L80:
	.loc 1 524 0
	ldr	r3, [fp, #-52]
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
.L79:
	.loc 1 524 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcc	.L84
	.loc 1 546 0 is_stmt 1
	ldr	r0, .L99+8
	bl	term_dat_out
	.loc 1 547 0
	ldr	r3, [fp, #-44]
	str	r3, [fp, #-8]
	b	.L85
.L93:
	.loc 1 549 0
	ldr	r3, [fp, #-52]
	cmp	r3, #2
	beq	.L88
	cmp	r3, #4
	beq	.L89
	cmp	r3, #1
	bne	.L86
.L87:
	.loc 1 552 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-20]
	.loc 1 553 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-56]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	strb	r3, [fp, #-29]
	.loc 1 554 0
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrb	r2, [fp, #-29]	@ zero_extendqisi2
	cmp	r2, r3
	beq	.L96
	.loc 1 556 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-20]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [fp, #-29]	@ zero_extendqisi2
	str	r1, [sp, #0]
	mov	r0, #0
	mov	r1, #1
	bl	memtest_pf
	.loc 1 557 0
	b	.L75
.L88:
	.loc 1 562 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-24]
	.loc 1 563 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-56]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	strh	r3, [fp, #-32]	@ movhi
	.loc 1 564 0
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #0]
	ldrh	r2, [fp, #-32]
	cmp	r2, r3
	beq	.L97
	.loc 1 566 0
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-24]
	ldrh	r3, [r3, #0]
	ldrh	r1, [fp, #-32]
	str	r1, [sp, #0]
	mov	r0, #0
	mov	r1, #2
	bl	memtest_pf
	.loc 1 567 0
	b	.L75
.L89:
	.loc 1 572 0
	ldr	r3, [fp, #-8]
	str	r3, [fp, #-28]
	.loc 1 573 0
	ldr	r3, [fp, #-12]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-56]
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	str	r3, [fp, #-36]
	.loc 1 574 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #0]
	str	r3, [fp, #-40]
	.loc 1 575 0
	ldr	r2, [fp, #-40]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	beq	.L98
	.loc 1 577 0
	ldr	r3, [fp, #-28]
	ldr	r2, [fp, #-36]
	str	r2, [sp, #0]
	mov	r0, #0
	mov	r1, #4
	mov	r2, r3
	ldr	r3, [fp, #-40]
	bl	memtest_pf
	.loc 1 578 0
	b	.L75
.L96:
	.loc 1 559 0
	mov	r0, r0	@ nop
	b	.L86
.L97:
	.loc 1 569 0
	mov	r0, r0	@ nop
	b	.L86
.L98:
	.loc 1 580 0
	mov	r0, r0	@ nop
.L86:
	.loc 1 547 0
	ldr	r3, [fp, #-52]
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
.L85:
	.loc 1 547 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bcc	.L93
	.loc 1 584 0 is_stmt 1
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L78:
	.loc 1 520 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #4]
	cmp	r2, r3
	blt	.L94
	.loc 1 588 0
	ldr	r3, [fp, #-52]
	mov	r2, #0
	str	r2, [sp, #0]
	mov	r0, #1
	mov	r1, r3
	mov	r2, #0
	mov	r3, #0
	bl	memtest_pf
	b	.L75
.L95:
	.loc 1 513 0
	mov	r0, r0	@ nop
.L75:
	.loc 1 589 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L100:
	.align	2
.L99:
	.word	pattst_msg
	.word	start_msg
	.word	verify_msg
.LFE4:
	.size	memtest_pt, .-memtest_pt
	.section .rodata
	.align	2
.LC0:
	.ascii	"*************************\015\012\000"
	.align	2
.LC1:
	.ascii	"\015\012\000"
	.section	.text.memory_test,"ax",%progbits
	.align	2
	.global	memory_test
	.type	memory_test, %function
memory_test:
.LFB5:
	.loc 1 619 0
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #20
.LCFI17:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 620 0
	b	.L102
.L112:
	.loc 1 623 0
	ldr	r3, [fp, #-20]
	cmn	r3, #-2147483647
	bne	.L103
	.loc 1 626 0
	ldr	r0, .L114
	bl	term_dat_out
	.loc 1 628 0
	mov	r0, #1
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-16]
	bl	memtest_w10
	.loc 1 629 0
	mov	r0, #250
	bl	vTaskDelay
	.loc 1 631 0
	ldr	r0, .L114+4
	bl	term_dat_out
	.loc 1 632 0
	mov	r0, #0
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-16]
	bl	memtest_w10
	.loc 1 633 0
	mov	r0, #250
	bl	vTaskDelay
	.loc 1 635 0
	ldr	r0, .L114+4
	bl	term_dat_out
	.loc 1 636 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	bl	memtest_ia
	.loc 1 637 0
	mov	r0, #250
	bl	vTaskDelay
	.loc 1 639 0
	ldr	r0, .L114+4
	bl	term_dat_out
	.loc 1 640 0
	mov	r3, #4
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	ldr	r3, .L114+8
	bl	memtest_pt
	.loc 1 641 0
	mov	r0, #250
	bl	vTaskDelay
	.loc 1 643 0
	ldr	r0, .L114+4
	bl	term_dat_out
	.loc 1 644 0
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	ldr	r3, .L114+12
	bl	memtest_pt
	.loc 1 645 0
	mov	r0, #250
	bl	vTaskDelay
	.loc 1 647 0
	ldr	r0, .L114
	bl	term_dat_out
	b	.L104
.L103:
	.loc 1 651 0
	ldr	r3, [fp, #-20]
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L113
.L111:
	.word	.L106
	.word	.L107
	.word	.L108
	.word	.L109
	.word	.L110
.L106:
	.loc 1 654 0
	mov	r0, #1
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-16]
	bl	memtest_w10
	.loc 1 655 0
	b	.L104
.L107:
	.loc 1 658 0
	mov	r0, #0
	ldr	r1, [fp, #-8]
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-16]
	bl	memtest_w10
	.loc 1 659 0
	b	.L104
.L108:
	.loc 1 662 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	bl	memtest_ia
	.loc 1 663 0
	b	.L104
.L109:
	.loc 1 666 0
	mov	r3, #4
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	ldr	r3, .L114+8
	bl	memtest_pt
	.loc 1 667 0
	b	.L104
.L110:
	.loc 1 670 0
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	ldr	r3, .L114+12
	bl	memtest_pt
	.loc 1 671 0
	b	.L104
.L113:
	.loc 1 674 0
	mov	r0, r0	@ nop
.L104:
	.loc 1 678 0
	ldr	r3, [fp, #4]
	sub	r3, r3, #1
	str	r3, [fp, #4]
.L102:
	.loc 1 620 0 discriminator 1
	ldr	r3, [fp, #4]
	cmp	r3, #0
	bne	.L112
	.loc 1 680 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L115:
	.align	2
.L114:
	.word	.LC0
	.word	.LC1
	.word	patts_55aa
	.word	patts_00ff
.LFE5:
	.size	memory_test, .-memory_test
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/src/memtest/s1l_memtests.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x716
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF67
	.byte	0x1
	.4byte	.LASF68
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x3a
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x2
	.byte	0x99
	.4byte	0x69
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x20
	.4byte	0xcd
	.uleb128 0x6
	.4byte	.LASF12
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF13
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF14
	.sleb128 1
	.uleb128 0x6
	.4byte	.LASF15
	.sleb128 2
	.uleb128 0x6
	.4byte	.LASF16
	.sleb128 3
	.uleb128 0x6
	.4byte	.LASF17
	.sleb128 4
	.uleb128 0x6
	.4byte	.LASF18
	.sleb128 5
	.uleb128 0x6
	.4byte	.LASF19
	.sleb128 2147483647
	.byte	0
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x3
	.byte	0x29
	.4byte	0x90
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF21
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF22
	.uleb128 0x7
	.4byte	0x37
	.4byte	0xf6
	.uleb128 0x8
	.4byte	0xd8
	.byte	0x1
	.byte	0
	.uleb128 0x9
	.byte	0x4
	.4byte	0x2c
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF69
	.byte	0x1
	.byte	0x4f
	.byte	0x1
	.4byte	0x85
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x17c
	.uleb128 0xb
	.ascii	"hdr\000"
	.byte	0x1
	.byte	0x4f
	.4byte	0xf6
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x1
	.byte	0x50
	.4byte	0x17c
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x1
	.byte	0x51
	.4byte	0x17c
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x1
	.byte	0x52
	.4byte	0x17c
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0xd
	.ascii	"str\000"
	.byte	0x1
	.byte	0x54
	.4byte	0x182
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xe
	.4byte	.LASF26
	.byte	0x1
	.byte	0x55
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0xe
	.4byte	.LASF27
	.byte	0x1
	.byte	0x56
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x9
	.byte	0x4
	.4byte	0x5e
	.uleb128 0x7
	.4byte	0x2c
	.4byte	0x192
	.uleb128 0x8
	.4byte	0xd8
	.byte	0xf
	.byte	0
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.byte	0x96
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x200
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x1
	.byte	0x96
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x1
	.byte	0x97
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x1
	.byte	0x98
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x1
	.byte	0x99
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0xc
	.4byte	.LASF31
	.byte	0x1
	.byte	0x9a
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0xd
	.ascii	"str\000"
	.byte	0x1
	.byte	0x9c
	.4byte	0x182
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0xf
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.byte	0xc8
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x2dc
	.uleb128 0xb
	.ascii	"w1\000"
	.byte	0x1
	.byte	0xc8
	.4byte	0x85
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x1
	.byte	0xc9
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x1
	.byte	0xca
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x1
	.byte	0xcb
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0xe
	.4byte	.LASF34
	.byte	0x1
	.byte	0xcd
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -33
	.uleb128 0xd
	.ascii	"p8\000"
	.byte	0x1
	.byte	0xcd
	.4byte	0xf6
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xe
	.4byte	.LASF35
	.byte	0x1
	.byte	0xce
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xd
	.ascii	"p16\000"
	.byte	0x1
	.byte	0xce
	.4byte	0x2dc
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xe
	.4byte	.LASF26
	.byte	0x1
	.byte	0xcf
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xe
	.4byte	.LASF36
	.byte	0x1
	.byte	0xcf
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0xe
	.4byte	.LASF37
	.byte	0x1
	.byte	0xcf
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0xe
	.4byte	.LASF38
	.byte	0x1
	.byte	0xcf
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xd
	.ascii	"p32\000"
	.byte	0x1
	.byte	0xcf
	.4byte	0x17c
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xe
	.4byte	.LASF39
	.byte	0x1
	.byte	0xd0
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x9
	.byte	0x4
	.4byte	0x45
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x189
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x3b0
	.uleb128 0x11
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x189
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x11
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x18a
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x11
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x18b
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x12
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x18d
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -29
	.uleb128 0x13
	.ascii	"p8\000"
	.byte	0x1
	.2byte	0x18d
	.4byte	0xf6
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x18e
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x13
	.ascii	"p16\000"
	.byte	0x1
	.2byte	0x18e
	.4byte	0x2dc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x18f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x18f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x12
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x18f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x18f
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x13
	.ascii	"p32\000"
	.byte	0x1
	.2byte	0x18f
	.4byte	0x17c
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x1f3
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x4ab
	.uleb128 0x11
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x1f3
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x11
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x1f4
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x11
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x1f5
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x11
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x1f6
	.4byte	0x17c
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x1f7
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x12
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x1f9
	.4byte	0x2c
	.byte	0x2
	.byte	0x91
	.sleb128 -33
	.uleb128 0x13
	.ascii	"p8\000"
	.byte	0x1
	.2byte	0x1f9
	.4byte	0xf6
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x1fa
	.4byte	0x45
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x13
	.ascii	"p16\000"
	.byte	0x1
	.2byte	0x1fa
	.4byte	0x2dc
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x12
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x1fb
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x1fb
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x12
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x1fb
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x12
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x1fb
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.ascii	"p32\000"
	.byte	0x1
	.2byte	0x1fb
	.4byte	0x17c
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x13
	.ascii	"idx\000"
	.byte	0x1
	.2byte	0x1fc
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x266
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x511
	.uleb128 0x11
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x266
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x11
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x267
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x268
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x269
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x11
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x26a
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.byte	0
	.uleb128 0xe
	.4byte	.LASF47
	.byte	0x4
	.byte	0x30
	.4byte	0x522
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x14
	.4byte	0xe6
	.uleb128 0xe
	.4byte	.LASF48
	.byte	0x4
	.byte	0x34
	.4byte	0x538
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x14
	.4byte	0xe6
	.uleb128 0xe
	.4byte	.LASF49
	.byte	0x4
	.byte	0x36
	.4byte	0x54e
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x14
	.4byte	0xe6
	.uleb128 0xe
	.4byte	.LASF50
	.byte	0x4
	.byte	0x38
	.4byte	0x564
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x14
	.4byte	0xe6
	.uleb128 0x7
	.4byte	0x2c
	.4byte	0x579
	.uleb128 0x8
	.4byte	0xd8
	.byte	0x19
	.byte	0
	.uleb128 0xe
	.4byte	.LASF51
	.byte	0x1
	.byte	0x1d
	.4byte	0x569
	.byte	0x5
	.byte	0x3
	.4byte	w1s_msg
	.uleb128 0xe
	.4byte	.LASF52
	.byte	0x1
	.byte	0x1e
	.4byte	0x569
	.byte	0x5
	.byte	0x3
	.4byte	w0s_msg
	.uleb128 0x7
	.4byte	0x2c
	.4byte	0x5ab
	.uleb128 0x8
	.4byte	0xd8
	.byte	0x16
	.byte	0
	.uleb128 0xe
	.4byte	.LASF53
	.byte	0x1
	.byte	0x1f
	.4byte	0x59b
	.byte	0x5
	.byte	0x3
	.4byte	invtst_msg
	.uleb128 0x7
	.4byte	0x2c
	.4byte	0x5cc
	.uleb128 0x8
	.4byte	0xd8
	.byte	0xe
	.byte	0
	.uleb128 0xe
	.4byte	.LASF54
	.byte	0x1
	.byte	0x20
	.4byte	0x5bc
	.byte	0x5
	.byte	0x3
	.4byte	pattst_msg
	.uleb128 0x7
	.4byte	0x2c
	.4byte	0x5ed
	.uleb128 0x8
	.4byte	0xd8
	.byte	0x29
	.byte	0
	.uleb128 0xe
	.4byte	.LASF55
	.byte	0x1
	.byte	0x21
	.4byte	0x5dd
	.byte	0x5
	.byte	0x3
	.4byte	invwidth_msg
	.uleb128 0xe
	.4byte	.LASF56
	.byte	0x1
	.byte	0x23
	.4byte	0x5bc
	.byte	0x5
	.byte	0x3
	.4byte	addr_msg
	.uleb128 0xe
	.4byte	.LASF57
	.byte	0x1
	.byte	0x24
	.4byte	0x5bc
	.byte	0x5
	.byte	0x3
	.4byte	width_msg
	.uleb128 0xe
	.4byte	.LASF58
	.byte	0x1
	.byte	0x25
	.4byte	0x5bc
	.byte	0x5
	.byte	0x3
	.4byte	bytes_msg
	.uleb128 0x7
	.4byte	0x2c
	.4byte	0x641
	.uleb128 0x8
	.4byte	0xd8
	.byte	0xd
	.byte	0
	.uleb128 0xe
	.4byte	.LASF59
	.byte	0x1
	.byte	0x26
	.4byte	0x631
	.byte	0x5
	.byte	0x3
	.4byte	passed_msg
	.uleb128 0x7
	.4byte	0x2c
	.4byte	0x662
	.uleb128 0x8
	.4byte	0xd8
	.byte	0x17
	.byte	0
	.uleb128 0xe
	.4byte	.LASF60
	.byte	0x1
	.byte	0x27
	.4byte	0x652
	.byte	0x5
	.byte	0x3
	.4byte	failed1_msg
	.uleb128 0x7
	.4byte	0x2c
	.4byte	0x683
	.uleb128 0x8
	.4byte	0xd8
	.byte	0x12
	.byte	0
	.uleb128 0xe
	.4byte	.LASF61
	.byte	0x1
	.byte	0x28
	.4byte	0x673
	.byte	0x5
	.byte	0x3
	.4byte	failed2_msg
	.uleb128 0xe
	.4byte	.LASF62
	.byte	0x1
	.byte	0x29
	.4byte	0x673
	.byte	0x5
	.byte	0x3
	.4byte	failed3_msg
	.uleb128 0x7
	.4byte	0x2c
	.4byte	0x6b5
	.uleb128 0x8
	.4byte	0xd8
	.byte	0x11
	.byte	0
	.uleb128 0xe
	.4byte	.LASF63
	.byte	0x1
	.byte	0x2a
	.4byte	0x6a5
	.byte	0x5
	.byte	0x3
	.4byte	start_msg
	.uleb128 0xe
	.4byte	.LASF64
	.byte	0x1
	.byte	0x2b
	.4byte	0x673
	.byte	0x5
	.byte	0x3
	.4byte	verify_msg
	.uleb128 0x7
	.4byte	0x5e
	.4byte	0x6e7
	.uleb128 0x8
	.4byte	0xd8
	.byte	0x3
	.byte	0
	.uleb128 0xe
	.4byte	.LASF65
	.byte	0x1
	.byte	0x35
	.4byte	0x6d7
	.byte	0x5
	.byte	0x3
	.4byte	patts_55aa
	.uleb128 0x7
	.4byte	0x5e
	.4byte	0x708
	.uleb128 0x8
	.4byte	0xd8
	.byte	0x1
	.byte	0
	.uleb128 0xe
	.4byte	.LASF66
	.byte	0x1
	.byte	0x37
	.4byte	0x6f8
	.byte	0x5
	.byte	0x3
	.4byte	patts_00ff
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF44:
	.ascii	"memory_test\000"
.LASF52:
	.ascii	"w0s_msg\000"
.LASF15:
	.ascii	"MTST_INVADDR\000"
.LASF38:
	.ascii	"lastaddr\000"
.LASF20:
	.ascii	"MEMTEST_TEST_T\000"
.LASF23:
	.ascii	"hexaddr\000"
.LASF3:
	.ascii	"UNS_8\000"
.LASF36:
	.ascii	"tmp32a\000"
.LASF37:
	.ascii	"tmp32b\000"
.LASF46:
	.ascii	"iters\000"
.LASF43:
	.ascii	"numpatts\000"
.LASF10:
	.ascii	"long long int\000"
.LASF56:
	.ascii	"addr_msg\000"
.LASF18:
	.ascii	"MTST_LAST\000"
.LASF6:
	.ascii	"short int\000"
.LASF12:
	.ascii	"MTST_FIRST\000"
.LASF30:
	.ascii	"actual\000"
.LASF16:
	.ascii	"MTST_PATTERN\000"
.LASF27:
	.ascii	"goodw\000"
.LASF62:
	.ascii	"failed3_msg\000"
.LASF33:
	.ascii	"memtest_w10\000"
.LASF25:
	.ascii	"width\000"
.LASF26:
	.ascii	"tmp32\000"
.LASF49:
	.ascii	"GuiFont_DecimalChar\000"
.LASF45:
	.ascii	"tstnum\000"
.LASF53:
	.ascii	"invtst_msg\000"
.LASF22:
	.ascii	"long int\000"
.LASF57:
	.ascii	"width_msg\000"
.LASF29:
	.ascii	"addr\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF58:
	.ascii	"bytes_msg\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF11:
	.ascii	"BOOL_32\000"
.LASF54:
	.ascii	"pattst_msg\000"
.LASF2:
	.ascii	"signed char\000"
.LASF9:
	.ascii	"long long unsigned int\000"
.LASF48:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF32:
	.ascii	"memtest_pf\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF66:
	.ascii	"patts_00ff\000"
.LASF60:
	.ascii	"failed1_msg\000"
.LASF69:
	.ascii	"memtest_disp_addr\000"
.LASF41:
	.ascii	"memtest_pt\000"
.LASF59:
	.ascii	"passed_msg\000"
.LASF68:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/memt"
	.ascii	"est/s1l_memtests.c\000"
.LASF28:
	.ascii	"pass\000"
.LASF0:
	.ascii	"char\000"
.LASF40:
	.ascii	"memtest_ia\000"
.LASF50:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF51:
	.ascii	"w1s_msg\000"
.LASF14:
	.ascii	"MTST_WALKING0\000"
.LASF13:
	.ascii	"MTST_WALKING1\000"
.LASF31:
	.ascii	"expected\000"
.LASF34:
	.ascii	"tmp8\000"
.LASF64:
	.ascii	"verify_msg\000"
.LASF21:
	.ascii	"long unsigned int\000"
.LASF63:
	.ascii	"start_msg\000"
.LASF24:
	.ascii	"bytes\000"
.LASF67:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF19:
	.ascii	"MTST_ALL\000"
.LASF17:
	.ascii	"MTST_SET_CLR\000"
.LASF35:
	.ascii	"tmp16\000"
.LASF47:
	.ascii	"GuiFont_LanguageActive\000"
.LASF39:
	.ascii	"windex\000"
.LASF61:
	.ascii	"failed2_msg\000"
.LASF65:
	.ascii	"patts_55aa\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF55:
	.ascii	"invwidth_msg\000"
.LASF42:
	.ascii	"patterns\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
