	.file	"sdram_common.c"
	.text
.Ltext0:
	.global	bus32
	.section	.rodata.bus32,"a",%progbits
	.align	2
	.type	bus32, %object
	.size	bus32, 4
bus32:
	.word	1
	.global	sdram_map
	.section	.rodata.sdram_map,"a",%progbits
	.align	2
	.type	sdram_map, %object
	.size	sdram_map, 52
sdram_map:
	.byte	2
	.byte	13
	.byte	10
	.byte	-111
	.byte	2
	.byte	13
	.byte	11
	.byte	-112
	.byte	2
	.byte	13
	.byte	8
	.byte	-114
	.byte	2
	.byte	13
	.byte	9
	.byte	-115
	.byte	2
	.byte	12
	.byte	8
	.byte	-118
	.byte	2
	.byte	12
	.byte	9
	.byte	-119
	.byte	2
	.byte	12
	.byte	10
	.byte	-120
	.byte	2
	.byte	11
	.byte	8
	.byte	-122
	.byte	1
	.byte	11
	.byte	8
	.byte	-127
	.byte	1
	.byte	11
	.byte	9
	.byte	-128
	.space	12
	.global	modeshift
	.section	.bss.modeshift,"aw",%nobits
	.align	2
	.type	modeshift, %object
	.size	modeshift, 4
modeshift:
	.space	4
	.global	bankshift
	.section	.bss.bankshift,"aw",%nobits
	.align	2
	.type	bankshift, %object
	.size	bankshift, 4
bankshift:
	.space	4
	.section	.rodata.dqs2calsen,"a",%progbits
	.align	2
	.type	dqs2calsen, %object
	.size	dqs2calsen, 32
dqs2calsen:
	.byte	7
	.byte	5
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.global	dqsstart
	.section	.bss.dqsstart,"aw",%nobits
	.align	2
	.type	dqsstart, %object
	.size	dqsstart, 4
dqsstart:
	.space	4
	.global	dqsend
	.section	.bss.dqsend,"aw",%nobits
	.align	2
	.type	dqsend, %object
	.size	dqsend, 4
dqsend:
	.space	4
	.section	.text.walking0bitsetup,"ax",%progbits
	.align	2
	.type	walking0bitsetup, %function
walking0bitsetup:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/sdram_common.c"
	.loc 1 158 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-8]
	.loc 1 160 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L2
.L3:
	.loc 1 162 0 discriminator 2
	mov	r2, #1
	ldr	r3, [fp, #-4]
	mov	r3, r2, asl r3
	mvn	r3, r3
	mov	r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 163 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 160 0 discriminator 2
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L2:
	.loc 1 160 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L3
	.loc 1 165 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE0:
	.size	walking0bitsetup, .-walking0bitsetup
	.section	.text.walking0bitcheck,"ax",%progbits
	.align	2
	.type	walking0bitcheck, %function
walking0bitcheck:
.LFB1:
	.loc 1 167 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 169 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L5
.L8:
	.loc 1 171 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	mov	r1, #1
	ldr	r3, [fp, #-4]
	mov	r3, r1, asl r3
	mvn	r3, r3
	cmp	r2, r3
	beq	.L6
	.loc 1 173 0
	mov	r3, #0
	b	.L7
.L6:
	.loc 1 176 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 169 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L5:
	.loc 1 169 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L8
	.loc 1 179 0 is_stmt 1
	mov	r3, #1
.L7:
	.loc 1 180 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE1:
	.size	walking0bitcheck, .-walking0bitcheck
	.section	.text.walking1bitsetup,"ax",%progbits
	.align	2
	.type	walking1bitsetup, %function
walking1bitsetup:
.LFB2:
	.loc 1 182 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-8]
	.loc 1 184 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L10
.L11:
	.loc 1 186 0 discriminator 2
	mov	r2, #1
	ldr	r3, [fp, #-4]
	mov	r3, r2, asl r3
	mov	r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 187 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 184 0 discriminator 2
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L10:
	.loc 1 184 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L11
	.loc 1 189 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE2:
	.size	walking1bitsetup, .-walking1bitsetup
	.section	.text.walking1bitcheck,"ax",%progbits
	.align	2
	.type	walking1bitcheck, %function
walking1bitcheck:
.LFB3:
	.loc 1 191 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-8]
	.loc 1 193 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L13
.L16:
	.loc 1 195 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	mov	r1, #1
	ldr	r3, [fp, #-4]
	mov	r3, r1, asl r3
	cmp	r2, r3
	beq	.L14
	.loc 1 197 0
	mov	r3, #0
	b	.L15
.L14:
	.loc 1 200 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 193 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L13:
	.loc 1 193 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L16
	.loc 1 203 0 is_stmt 1
	mov	r3, #1
.L15:
	.loc 1 204 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE3:
	.size	walking1bitcheck, .-walking1bitcheck
	.section	.text.invaddrsetup,"ax",%progbits
	.align	2
	.type	invaddrsetup, %function
invaddrsetup:
.LFB4:
	.loc 1 206 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI12:
	add	fp, sp, #0
.LCFI13:
	sub	sp, sp, #8
.LCFI14:
	str	r0, [fp, #-8]
	.loc 1 208 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L18
.L19:
	.loc 1 210 0 discriminator 2
	ldr	r3, [fp, #-8]
	mvn	r2, r3
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 211 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 208 0 discriminator 2
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L18:
	.loc 1 208 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L19
	.loc 1 213 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE4:
	.size	invaddrsetup, .-invaddrsetup
	.section	.text.invaddrcheck,"ax",%progbits
	.align	2
	.type	invaddrcheck, %function
invaddrcheck:
.LFB5:
	.loc 1 215 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI15:
	add	fp, sp, #0
.LCFI16:
	sub	sp, sp, #8
.LCFI17:
	str	r0, [fp, #-8]
	.loc 1 217 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L21
.L24:
	.loc 1 219 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	mvn	r3, r3
	cmp	r2, r3
	beq	.L22
	.loc 1 221 0
	mov	r3, #0
	b	.L23
.L22:
	.loc 1 224 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 217 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L21:
	.loc 1 217 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L24
	.loc 1 227 0 is_stmt 1
	mov	r3, #1
.L23:
	.loc 1 228 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE5:
	.size	invaddrcheck, .-invaddrcheck
	.section	.text.noninvaddrsetup,"ax",%progbits
	.align	2
	.type	noninvaddrsetup, %function
noninvaddrsetup:
.LFB6:
	.loc 1 230 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI18:
	add	fp, sp, #0
.LCFI19:
	sub	sp, sp, #8
.LCFI20:
	str	r0, [fp, #-8]
	.loc 1 232 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L26
.L27:
	.loc 1 234 0 discriminator 2
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 235 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 232 0 discriminator 2
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L26:
	.loc 1 232 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L27
	.loc 1 237 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE6:
	.size	noninvaddrsetup, .-noninvaddrsetup
	.section	.text.noninvaddrcheck,"ax",%progbits
	.align	2
	.type	noninvaddrcheck, %function
noninvaddrcheck:
.LFB7:
	.loc 1 239 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI21:
	add	fp, sp, #0
.LCFI22:
	sub	sp, sp, #8
.LCFI23:
	str	r0, [fp, #-8]
	.loc 1 241 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L29
.L32:
	.loc 1 243 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	beq	.L30
	.loc 1 245 0
	mov	r3, #0
	b	.L31
.L30:
	.loc 1 248 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 241 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L29:
	.loc 1 241 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L32
	.loc 1 251 0 is_stmt 1
	mov	r3, #1
.L31:
	.loc 1 252 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE7:
	.size	noninvaddrcheck, .-noninvaddrcheck
	.section	.text.aa55setup,"ax",%progbits
	.align	2
	.type	aa55setup, %function
aa55setup:
.LFB8:
	.loc 1 254 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI24:
	add	fp, sp, #0
.LCFI25:
	sub	sp, sp, #8
.LCFI26:
	str	r0, [fp, #-8]
	.loc 1 256 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L34
.L35:
	.loc 1 258 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r2, .L36
	str	r2, [r3, #0]
	.loc 1 259 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 256 0 discriminator 2
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L34:
	.loc 1 256 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L35
	.loc 1 261 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L37:
	.align	2
.L36:
	.word	1437226410
.LFE8:
	.size	aa55setup, .-aa55setup
	.section	.text.aa55check,"ax",%progbits
	.align	2
	.type	aa55check, %function
aa55check:
.LFB9:
	.loc 1 263 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI27:
	add	fp, sp, #0
.LCFI28:
	sub	sp, sp, #8
.LCFI29:
	str	r0, [fp, #-8]
	.loc 1 265 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L39
.L42:
	.loc 1 267 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, .L43
	cmp	r2, r3
	beq	.L40
	.loc 1 269 0
	mov	r3, #0
	b	.L41
.L40:
	.loc 1 272 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 265 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L39:
	.loc 1 265 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L42
	.loc 1 275 0 is_stmt 1
	mov	r3, #1
.L41:
	.loc 1 276 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L44:
	.align	2
.L43:
	.word	1437226410
.LFE9:
	.size	aa55check, .-aa55check
	.section	.text._55aasetup,"ax",%progbits
	.align	2
	.type	_55aasetup, %function
_55aasetup:
.LFB10:
	.loc 1 278 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI30:
	add	fp, sp, #0
.LCFI31:
	sub	sp, sp, #8
.LCFI32:
	str	r0, [fp, #-8]
	.loc 1 280 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L46
.L47:
	.loc 1 282 0 discriminator 2
	ldr	r3, [fp, #-8]
	ldr	r2, .L48
	str	r2, [r3, #0]
	.loc 1 283 0 discriminator 2
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 280 0 discriminator 2
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L46:
	.loc 1 280 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L47
	.loc 1 285 0 is_stmt 1
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L49:
	.align	2
.L48:
	.word	1437226410
.LFE10:
	.size	_55aasetup, .-_55aasetup
	.section	.text._55aacheck,"ax",%progbits
	.align	2
	.type	_55aacheck, %function
_55aacheck:
.LFB11:
	.loc 1 287 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI33:
	add	fp, sp, #0
.LCFI34:
	sub	sp, sp, #8
.LCFI35:
	str	r0, [fp, #-8]
	.loc 1 289 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L51
.L54:
	.loc 1 291 0
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, .L55
	cmp	r2, r3
	beq	.L52
	.loc 1 293 0
	mov	r3, #0
	b	.L53
.L52:
	.loc 1 296 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	str	r3, [fp, #-8]
	.loc 1 289 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L51:
	.loc 1 289 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #31
	ble	.L54
	.loc 1 299 0 is_stmt 1
	mov	r3, #1
.L53:
	.loc 1 300 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L56:
	.align	2
.L55:
	.word	1437226410
.LFE11:
	.size	_55aacheck, .-_55aacheck
	.section	.data.testvecs,"aw",%progbits
	.align	2
	.type	testvecs, %object
	.size	testvecs, 56
testvecs:
	.word	walking0bitsetup
	.word	walking0bitcheck
	.word	walking1bitsetup
	.word	walking1bitcheck
	.word	invaddrsetup
	.word	invaddrcheck
	.word	noninvaddrsetup
	.word	noninvaddrcheck
	.word	aa55setup
	.word	aa55check
	.word	_55aasetup
	.word	_55aacheck
	.word	0
	.word	0
	.section	.text.dqsin_ddr_mod,"ax",%progbits
	.align	2
	.type	dqsin_ddr_mod, %function
dqsin_ddr_mod:
.LFB12:
	.loc 1 331 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI36:
	add	fp, sp, #0
.LCFI37:
	sub	sp, sp, #8
.LCFI38:
	str	r0, [fp, #-8]
	.loc 1 335 0
	ldr	r3, .L58
	ldr	r3, [r3, #104]
	bic	r3, r3, #7232
	bic	r3, r3, #60
	str	r3, [fp, #-4]
	.loc 1 337 0
	ldr	r3, .L58
	ldr	r2, [fp, #-8]
	and	r2, r2, #31
	mov	r1, r2, asl #2
	ldr	r2, [fp, #-4]
	orr	r1, r1, r2
	.loc 1 338 0
	ldr	r0, .L58+4
	ldr	r2, [fp, #-8]
	add	r2, r0, r2
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	and	r2, r2, #7
	mov	r2, r2, asl #10
	.loc 1 337 0
	orr	r2, r1, r2
	str	r2, [r3, #104]
	.loc 1 339 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L59:
	.align	2
.L58:
	.word	1073758208
	.word	dqs2calsen
.LFE12:
	.size	dqsin_ddr_mod, .-dqsin_ddr_mod
	.section	.text.ddr_memtst,"ax",%progbits
	.align	2
	.type	ddr_memtst, %function
ddr_memtst:
.LFB13:
	.loc 1 367 0
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI39:
	add	fp, sp, #4
.LCFI40:
	sub	sp, sp, #24
.LCFI41:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	str	r2, [fp, #-28]
	.loc 1 369 0
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-12]
	.loc 1 373 0
	ldr	r2, [fp, #-20]
	mov	r3, r2
	mov	r3, r3, asl #12
	add	r3, r3, r2
	mov	r3, r3, asl #4
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
	.loc 1 374 0
	ldr	r3, [fp, #-28]
	mov	r3, r3, lsr #2
	str	r3, [fp, #-16]
	.loc 1 375 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, lsr #8
	str	r3, [fp, #-16]
	.loc 1 382 0
	b	.L61
.L66:
	.loc 1 385 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 386 0
	b	.L62
.L65:
	.loc 1 388 0
	ldr	r3, .L67
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #3]
	ldr	r0, [fp, #-12]
	blx	r3
	.loc 1 389 0
	ldr	r1, .L67
	ldr	r2, [fp, #-8]
	mov	r3, #4
	mov	r2, r2, asl #3
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-12]
	blx	r3
	mov	r3, r0
	cmp	r3, #0
	bne	.L63
	.loc 1 392 0
	mov	r3, #0
	b	.L64
.L63:
	.loc 1 395 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L62:
	.loc 1 386 0 discriminator 1
	ldr	r3, .L67
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #3]
	cmp	r3, #0
	bne	.L65
	.loc 1 398 0
	ldr	r3, [fp, #-16]
	mov	r3, r3, asl #2
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-12]
.L61:
	.loc 1 382 0 discriminator 1
	ldr	r2, [fp, #-12]
	ldr	r1, [fp, #-24]
	ldr	r3, [fp, #-28]
	add	r3, r1, r3
	sub	r3, r3, #128
	cmp	r2, r3
	bcc	.L66
	.loc 1 402 0
	mov	r3, #1
.L64:
	.loc 1 403 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L68:
	.align	2
.L67:
	.word	testvecs
.LFE13:
	.size	ddr_memtst, .-ddr_memtst
	.section	.text.ddr_find_dqsin_delay,"ax",%progbits
	.align	2
	.global	ddr_find_dqsin_delay
	.type	ddr_find_dqsin_delay, %function
ddr_find_dqsin_delay:
.LFB14:
	.loc 1 434 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI42:
	add	fp, sp, #4
.LCFI43:
	sub	sp, sp, #20
.LCFI44:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 1 436 0
	mov	r3, #0
	str	r3, [fp, #-12]
	mov	r3, #0
	str	r3, [fp, #-16]
	.loc 1 441 0
	mov	r3, #1
	str	r3, [fp, #-8]
	.loc 1 442 0
	ldr	r3, .L77
	mov	r2, #255
	str	r2, [r3, #0]
	ldr	r3, .L77
	ldr	r2, [r3, #0]
	ldr	r3, .L77+4
	str	r2, [r3, #0]
	.loc 1 451 0
	b	.L70
.L74:
	.loc 1 455 0
	ldr	r0, [fp, #-8]
	bl	dqsin_ddr_mod
	.loc 1 459 0
	ldr	r0, [fp, #-8]
	ldr	r1, [fp, #-20]
	ldr	r2, [fp, #-24]
	bl	ddr_memtst
	mov	r3, r0
	cmp	r3, #1
	bne	.L71
	.loc 1 462 0
	ldr	r3, .L77+4
	ldr	r3, [r3, #0]
	cmp	r3, #255
	bne	.L72
	.loc 1 464 0
	ldr	r2, [fp, #-8]
	ldr	r3, .L77+4
	str	r2, [r3, #0]
.L72:
	.loc 1 467 0
	mov	r3, #1
	str	r3, [fp, #-12]
	b	.L73
.L71:
	.loc 1 472 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L73
	.loc 1 474 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	mov	r2, r3
	ldr	r3, .L77
	str	r2, [r3, #0]
	.loc 1 475 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 476 0
	mov	r3, #0
	str	r3, [fp, #-12]
.L73:
	.loc 1 481 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L70:
	.loc 1 451 0 discriminator 1
	ldr	r3, [fp, #-8]
	cmp	r3, #30
	bls	.L74
	.loc 1 486 0
	ldr	r3, [fp, #-16]
	cmp	r3, #1
	bne	.L75
	.loc 1 488 0
	ldr	r3, .L77+4
	ldr	r2, [r3, #0]
	ldr	r3, .L77
	ldr	r3, [r3, #0]
	add	r3, r2, r3
	mov	r2, r3, lsr #31
	add	r3, r2, r3
	mov	r3, r3, asr #1
	str	r3, [fp, #-8]
	b	.L76
.L75:
	.loc 1 494 0
	mov	r3, #15
	str	r3, [fp, #-8]
.L76:
	.loc 1 498 0
	ldr	r0, [fp, #-8]
	bl	dqsin_ddr_mod
	.loc 1 500 0
	ldr	r3, [fp, #-16]
	.loc 1 501 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L78:
	.align	2
.L77:
	.word	dqsend
	.word	dqsstart
.LFE14:
	.size	ddr_find_dqsin_delay, .-ddr_find_dqsin_delay
	.section	.text.ddr_clock_resync,"ax",%progbits
	.align	2
	.global	ddr_clock_resync
	.type	ddr_clock_resync, %function
ddr_clock_resync:
.LFB15:
	.loc 1 524 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI45:
	add	fp, sp, #0
.LCFI46:
	sub	sp, sp, #4
.LCFI47:
	str	r0, [fp, #-4]
	.loc 1 530 0
	ldr	r3, .L80
	ldr	r2, .L80
	ldr	r2, [r2, #104]
	orr	r2, r2, #524288
	str	r2, [r3, #104]
	.loc 1 531 0
	ldr	r3, .L80
	ldr	r2, .L80
	ldr	r2, [r2, #104]
	bic	r2, r2, #524288
	str	r2, [r3, #104]
	.loc 1 537 0
	ldr	r3, .L80+4
	ldr	r2, [fp, #-4]
	mov	r2, r2, asl #7
	orr	r2, r2, #6
	str	r2, [r3, #256]
	.loc 1 542 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L81:
	.align	2
.L80:
	.word	1073758208
	.word	822607872
.LFE15:
	.size	ddr_clock_resync, .-ddr_clock_resync
	.section	.text.sdram_adjust_timing,"ax",%progbits
	.align	2
	.global	sdram_adjust_timing
	.type	sdram_adjust_timing, %function
sdram_adjust_timing:
.LFB16:
	.loc 1 572 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI48:
	add	fp, sp, #0
.LCFI49:
	sub	sp, sp, #4
.LCFI50:
	str	r0, [fp, #-4]
	.loc 1 579 0
	ldr	r1, .L83
	ldr	r2, [fp, #-4]
	ldr	r3, .L83+4
	umull	r0, r3, r2, r3
	rsb	r2, r3, r2
	mov	r2, r2, lsr #1
	add	r3, r3, r2
	mov	r3, r3, lsr #25
	and	r3, r3, #15
	str	r3, [r1, #48]
	.loc 1 589 0
	ldr	r3, .L83
	ldr	r1, [fp, #-4]
	ldr	r2, .L83+8
	umull	r0, r2, r1, r2
	mov	r2, r2, lsr #24
	and	r2, r2, #15
	str	r2, [r3, #52]
	.loc 1 600 0
	ldr	r3, .L83
	ldr	r1, [fp, #-4]
	ldr	r2, .L83+12
	umull	r0, r2, r1, r2
	mov	r2, r2, lsr #22
	and	r2, r2, #127
	str	r2, [r3, #56]
	.loc 1 610 0
	ldr	r3, .L83
	ldr	r1, [fp, #-4]
	ldr	r2, .L83+16
	umull	r0, r2, r1, r2
	mov	r2, r2, lsr #22
	and	r2, r2, #15
	str	r2, [r3, #68]
	.loc 1 619 0
	ldr	r3, .L83
	mov	r2, #7
	str	r2, [r3, #72]
	.loc 1 626 0
	ldr	r3, .L83
	ldr	r1, [fp, #-4]
	ldr	r2, .L83+20
	umull	r0, r2, r1, r2
	mov	r2, r2, lsr #21
	and	r2, r2, #31
	str	r2, [r3, #76]
	.loc 1 636 0
	ldr	r3, .L83
	ldr	r1, [fp, #-4]
	ldr	r2, .L83+12
	umull	r0, r2, r1, r2
	mov	r2, r2, lsr #22
	and	r2, r2, #255
	str	r2, [r3, #80]
	.loc 1 646 0
	ldr	r3, .L83
	ldr	r1, [fp, #-4]
	ldr	r2, .L83+16
	umull	r0, r2, r1, r2
	mov	r2, r2, lsr #22
	and	r2, r2, #15
	str	r2, [r3, #84]
	.loc 1 656 0
	ldr	r3, .L83
	ldr	r2, [fp, #-4]
	mov	r2, r2, lsr #1
	and	r2, r2, #15
	str	r2, [r3, #88]
	.loc 1 671 0
	ldr	r3, .L83
	mov	r2, #1
	str	r2, [r3, #92]
	.loc 1 683 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L84:
	.align	2
.L83:
	.word	822607872
	.word	893179527
	.word	-1268548243
	.word	1206964711
	.word	216172783
	.word	540431977
.LFE16:
	.size	sdram_adjust_timing, .-sdram_adjust_timing
	.section	.text.sdram_find_config,"ax",%progbits
	.align	2
	.global	sdram_find_config
	.type	sdram_find_config, %function
sdram_find_config:
.LFB17:
	.loc 1 707 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI51:
	add	fp, sp, #0
.LCFI52:
	sub	sp, sp, #8
.LCFI53:
	.loc 1 709 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 711 0
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L86
.L89:
	.loc 1 713 0
	ldr	r1, .L90
	ldr	r2, [fp, #-4]
	mov	r3, #2
	mov	r2, r2, asl #2
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #8
	bne	.L87
	.loc 1 714 0 discriminator 1
	ldr	r1, .L90
	ldr	r2, [fp, #-4]
	mov	r3, #1
	mov	r2, r2, asl #2
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	.loc 1 713 0 discriminator 1
	cmp	r3, #11
	bne	.L87
	.loc 1 715 0
	ldr	r3, .L90
	ldr	r2, [fp, #-4]
	ldrb	r3, [r3, r2, asl #2]	@ zero_extendqisi2
	.loc 1 714 0
	cmp	r3, #2
	bne	.L87
	.loc 1 717 0
	ldr	r1, .L90
	ldr	r2, [fp, #-4]
	mov	r3, #3
	mov	r2, r2, asl #2
	add	r2, r1, r2
	add	r3, r2, r3
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	str	r3, [fp, #-8]
.L87:
	.loc 1 711 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L86:
	.loc 1 711 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-4]
	cmp	r3, #12
	bgt	.L88
	.loc 1 711 0 discriminator 2
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L89
.L88:
	.loc 1 727 0 is_stmt 1
	ldr	r3, .L90+4
	ldr	r3, [r3, #0]
	add	r2, r3, #11
	ldr	r3, .L90+8
	str	r2, [r3, #0]
	.loc 1 728 0
	ldr	r3, .L90+4
	ldr	r3, [r3, #0]
	add	r2, r3, #9
	ldr	r3, .L90+12
	str	r2, [r3, #0]
	.loc 1 735 0
	ldr	r3, [fp, #-8]
	.loc 1 736 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L91:
	.align	2
.L90:
	.word	sdram_map
	.word	bus32
	.word	modeshift
	.word	bankshift
.LFE17:
	.size	sdram_find_config, .-sdram_find_config
	.section	.text.sdram_get_bankmask,"ax",%progbits
	.align	2
	.global	sdram_get_bankmask
	.type	sdram_get_bankmask, %function
sdram_get_bankmask:
.LFB18:
	.loc 1 764 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI54:
	add	fp, sp, #0
.LCFI55:
	sub	sp, sp, #12
.LCFI56:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	.loc 1 765 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 767 0
	ldr	r3, .L95
	ldr	r3, [r3, #0]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L93
	.loc 1 770 0
	ldr	r3, [fp, #-12]
	mov	r2, r3, asl #1
	ldr	r3, [fp, #-8]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
	b	.L94
.L93:
	.loc 1 776 0
	ldr	r3, [fp, #-8]
	mov	r2, r3, asl #1
	ldr	r3, [fp, #-12]
	orr	r3, r2, r3
	str	r3, [fp, #-4]
.L94:
	.loc 1 779 0
	ldr	r3, .L95
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-4]
	mov	r3, r2, asl r3
	.loc 1 780 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L96:
	.align	2
.L95:
	.word	bankshift
.LFE18:
	.size	sdram_get_bankmask, .-sdram_get_bankmask
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI39-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI42-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI45-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI48-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI51-.LFB17
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI52-.LCFI51
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI54-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI55-.LCFI54
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE36:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_emc.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clkpwr.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xd90
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF151
	.byte	0x1
	.4byte	.LASF152
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x3a
	.4byte	0x45
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x6c
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x5
	.byte	0x20
	.byte	0x3
	.byte	0x23
	.4byte	0x101
	.uleb128 0x6
	.4byte	.LASF12
	.byte	0x3
	.byte	0x25
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF13
	.byte	0x3
	.byte	0x26
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x3
	.byte	0x27
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x28
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x3
	.byte	0x29
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x3
	.byte	0x2a
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x3
	.byte	0x2b
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x3
	.byte	0x2c
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x7
	.4byte	0x61
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x3
	.byte	0x2d
	.4byte	0x88
	.uleb128 0x5
	.byte	0x20
	.byte	0x3
	.byte	0x30
	.4byte	0x152
	.uleb128 0x6
	.4byte	.LASF21
	.byte	0x3
	.byte	0x32
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF22
	.byte	0x3
	.byte	0x33
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF23
	.byte	0x3
	.byte	0x34
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x3
	.byte	0x35
	.4byte	0x162
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x162
	.uleb128 0x9
	.4byte	0x25
	.byte	0x4
	.byte	0
	.uleb128 0x7
	.4byte	0x152
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x3
	.byte	0x36
	.4byte	0x111
	.uleb128 0xa
	.2byte	0x4a0
	.byte	0x3
	.byte	0x39
	.4byte	0x339
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x3
	.byte	0x3b
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x3
	.byte	0x3c
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x3
	.byte	0x3d
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF28
	.byte	0x3
	.byte	0x3e
	.4byte	0x339
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF29
	.byte	0x3
	.byte	0x3f
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x6
	.4byte	.LASF30
	.byte	0x3
	.byte	0x40
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x6
	.4byte	.LASF31
	.byte	0x3
	.byte	0x41
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x6
	.4byte	.LASF32
	.byte	0x3
	.byte	0x42
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x6
	.4byte	.LASF33
	.byte	0x3
	.byte	0x43
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x6
	.4byte	.LASF34
	.byte	0x3
	.byte	0x44
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x6
	.4byte	.LASF35
	.byte	0x3
	.byte	0x45
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x6
	.4byte	.LASF36
	.byte	0x3
	.byte	0x46
	.4byte	0x34e
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x6
	.4byte	.LASF37
	.byte	0x3
	.byte	0x47
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x6
	.4byte	.LASF38
	.byte	0x3
	.byte	0x48
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x6
	.4byte	.LASF39
	.byte	0x3
	.byte	0x49
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x6
	.4byte	.LASF40
	.byte	0x3
	.byte	0x4a
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x6
	.4byte	.LASF41
	.byte	0x3
	.byte	0x4b
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x6
	.4byte	.LASF42
	.byte	0x3
	.byte	0x4c
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x6
	.4byte	.LASF43
	.byte	0x3
	.byte	0x4d
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x6
	.4byte	.LASF44
	.byte	0x3
	.byte	0x4e
	.4byte	0x363
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x6
	.4byte	.LASF45
	.byte	0x3
	.byte	0x4f
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x6
	.4byte	.LASF46
	.byte	0x3
	.byte	0x50
	.4byte	0x378
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x6
	.4byte	.LASF47
	.byte	0x3
	.byte	0x51
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0x100
	.uleb128 0x6
	.4byte	.LASF48
	.byte	0x3
	.byte	0x52
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0x104
	.uleb128 0x6
	.4byte	.LASF49
	.byte	0x3
	.byte	0x53
	.4byte	0x38d
	.byte	0x3
	.byte	0x23
	.uleb128 0x108
	.uleb128 0x6
	.4byte	.LASF50
	.byte	0x3
	.byte	0x54
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0x120
	.uleb128 0x6
	.4byte	.LASF51
	.byte	0x3
	.byte	0x55
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0x124
	.uleb128 0x6
	.4byte	.LASF52
	.byte	0x3
	.byte	0x56
	.4byte	0x3a2
	.byte	0x3
	.byte	0x23
	.uleb128 0x128
	.uleb128 0x6
	.4byte	.LASF53
	.byte	0x3
	.byte	0x57
	.4byte	0x3a7
	.byte	0x3
	.byte	0x23
	.uleb128 0x200
	.uleb128 0x6
	.4byte	.LASF54
	.byte	0x3
	.byte	0x58
	.4byte	0x3c7
	.byte	0x3
	.byte	0x23
	.uleb128 0x280
	.uleb128 0x6
	.4byte	.LASF55
	.byte	0x3
	.byte	0x59
	.4byte	0x3cc
	.byte	0x3
	.byte	0x23
	.uleb128 0x400
	.byte	0
	.uleb128 0x7
	.4byte	0x152
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x34e
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.4byte	0x33e
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x363
	.uleb128 0x9
	.4byte	0x25
	.byte	0x7
	.byte	0
	.uleb128 0x7
	.4byte	0x353
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x378
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1e
	.byte	0
	.uleb128 0x7
	.4byte	0x368
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x38d
	.uleb128 0x9
	.4byte	0x25
	.byte	0x5
	.byte	0
	.uleb128 0x7
	.4byte	0x37d
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x3a2
	.uleb128 0x9
	.4byte	0x25
	.byte	0x35
	.byte	0
	.uleb128 0x7
	.4byte	0x392
	.uleb128 0x8
	.4byte	0x106
	.4byte	0x3b7
	.uleb128 0x9
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x3c7
	.uleb128 0x9
	.4byte	0x25
	.byte	0x5f
	.byte	0
	.uleb128 0x7
	.4byte	0x3b7
	.uleb128 0x8
	.4byte	0x167
	.4byte	0x3dc
	.uleb128 0x9
	.4byte	0x25
	.byte	0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF56
	.byte	0x3
	.byte	0x5a
	.4byte	0x172
	.uleb128 0xa
	.2byte	0x140
	.byte	0x4
	.byte	0x28
	.4byte	0x6f0
	.uleb128 0x6
	.4byte	.LASF28
	.byte	0x4
	.byte	0x2a
	.4byte	0x6f0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF57
	.byte	0x4
	.byte	0x2b
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF58
	.byte	0x4
	.byte	0x2c
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF59
	.byte	0x4
	.byte	0x2d
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x6
	.4byte	.LASF60
	.byte	0x4
	.byte	0x2e
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x6
	.4byte	.LASF61
	.byte	0x4
	.byte	0x2f
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x6
	.4byte	.LASF62
	.byte	0x4
	.byte	0x30
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x6
	.4byte	.LASF63
	.byte	0x4
	.byte	0x31
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x6
	.4byte	.LASF64
	.byte	0x4
	.byte	0x32
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x6
	.4byte	.LASF65
	.byte	0x4
	.byte	0x33
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x6
	.4byte	.LASF66
	.byte	0x4
	.byte	0x34
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x6
	.4byte	.LASF67
	.byte	0x4
	.byte	0x35
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x6
	.4byte	.LASF68
	.byte	0x4
	.byte	0x36
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x6
	.4byte	.LASF69
	.byte	0x4
	.byte	0x37
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x6
	.4byte	.LASF70
	.byte	0x4
	.byte	0x38
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x6
	.4byte	.LASF71
	.byte	0x4
	.byte	0x39
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x6
	.4byte	.LASF72
	.byte	0x4
	.byte	0x3a
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x6
	.4byte	.LASF73
	.byte	0x4
	.byte	0x3b
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x6
	.4byte	.LASF74
	.byte	0x4
	.byte	0x3c
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x6
	.4byte	.LASF32
	.byte	0x4
	.byte	0x3d
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x6
	.4byte	.LASF75
	.byte	0x4
	.byte	0x3e
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x6
	.4byte	.LASF76
	.byte	0x4
	.byte	0x3f
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x6
	.4byte	.LASF77
	.byte	0x4
	.byte	0x40
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x6
	.4byte	.LASF78
	.byte	0x4
	.byte	0x41
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x6
	.4byte	.LASF79
	.byte	0x4
	.byte	0x42
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x6
	.4byte	.LASF80
	.byte	0x4
	.byte	0x43
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x6
	.4byte	.LASF81
	.byte	0x4
	.byte	0x44
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x6
	.4byte	.LASF82
	.byte	0x4
	.byte	0x45
	.4byte	0x101
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x6
	.4byte	.LASF83
	.byte	0x4
	.byte	0x46
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x6
	.4byte	.LASF44
	.byte	0x4
	.byte	0x47
	.4byte	0x705
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0x6
	.4byte	.LASF84
	.byte	0x4
	.byte	0x48
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x6
	.4byte	.LASF46
	.byte	0x4
	.byte	0x49
	.4byte	0x71a
	.byte	0x3
	.byte	0x23
	.uleb128 0x94
	.uleb128 0x6
	.4byte	.LASF85
	.byte	0x4
	.byte	0x4a
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xa4
	.uleb128 0x6
	.4byte	.LASF86
	.byte	0x4
	.byte	0x4b
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x6
	.4byte	.LASF87
	.byte	0x4
	.byte	0x4c
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xac
	.uleb128 0x6
	.4byte	.LASF88
	.byte	0x4
	.byte	0x4d
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x6
	.4byte	.LASF89
	.byte	0x4
	.byte	0x4e
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xb4
	.uleb128 0x6
	.4byte	.LASF90
	.byte	0x4
	.byte	0x4f
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0x6
	.4byte	.LASF91
	.byte	0x4
	.byte	0x50
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xbc
	.uleb128 0x6
	.4byte	.LASF92
	.byte	0x4
	.byte	0x51
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x6
	.4byte	.LASF93
	.byte	0x4
	.byte	0x52
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xc4
	.uleb128 0x6
	.4byte	.LASF94
	.byte	0x4
	.byte	0x53
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xc8
	.uleb128 0x6
	.4byte	.LASF52
	.byte	0x4
	.byte	0x54
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x6
	.4byte	.LASF95
	.byte	0x4
	.byte	0x55
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x6
	.4byte	.LASF96
	.byte	0x4
	.byte	0x56
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xd4
	.uleb128 0x6
	.4byte	.LASF97
	.byte	0x4
	.byte	0x57
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xd8
	.uleb128 0x6
	.4byte	.LASF98
	.byte	0x4
	.byte	0x58
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xdc
	.uleb128 0x6
	.4byte	.LASF99
	.byte	0x4
	.byte	0x59
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xe0
	.uleb128 0x6
	.4byte	.LASF100
	.byte	0x4
	.byte	0x5a
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xe4
	.uleb128 0x6
	.4byte	.LASF101
	.byte	0x4
	.byte	0x5b
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xe8
	.uleb128 0x6
	.4byte	.LASF102
	.byte	0x4
	.byte	0x5c
	.4byte	0x101
	.byte	0x3
	.byte	0x23
	.uleb128 0xec
	.uleb128 0x6
	.4byte	.LASF54
	.byte	0x4
	.byte	0x5d
	.4byte	0x72f
	.byte	0x3
	.byte	0x23
	.uleb128 0xf0
	.uleb128 0x6
	.4byte	.LASF103
	.byte	0x4
	.byte	0x5e
	.4byte	0x734
	.byte	0x3
	.byte	0x23
	.uleb128 0x130
	.byte	0
	.uleb128 0x7
	.4byte	0x152
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x705
	.uleb128 0x9
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.4byte	0x6f5
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x71a
	.uleb128 0x9
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x7
	.4byte	0x70a
	.uleb128 0x8
	.4byte	0x61
	.4byte	0x72f
	.uleb128 0x9
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0x7
	.4byte	0x71f
	.uleb128 0x7
	.4byte	0x70a
	.uleb128 0x3
	.4byte	.LASF104
	.byte	0x4
	.byte	0x5f
	.4byte	0x3e7
	.uleb128 0xb
	.4byte	.LASF111
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.4byte	0x789
	.uleb128 0x6
	.4byte	.LASF105
	.byte	0x1
	.byte	0x20
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF106
	.byte	0x1
	.byte	0x21
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x6
	.4byte	.LASF107
	.byte	0x1
	.byte	0x22
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x6
	.4byte	.LASF108
	.byte	0x1
	.byte	0x23
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.byte	0
	.uleb128 0x3
	.4byte	.LASF109
	.byte	0x1
	.byte	0x6f
	.4byte	0x794
	.uleb128 0xc
	.byte	0x4
	.4byte	0x79a
	.uleb128 0xd
	.byte	0x1
	.4byte	0x7a6
	.uleb128 0xe
	.4byte	0x7a6
	.byte	0
	.uleb128 0xc
	.byte	0x4
	.4byte	0x61
	.uleb128 0x3
	.4byte	.LASF110
	.byte	0x1
	.byte	0x70
	.4byte	0x7b7
	.uleb128 0xc
	.byte	0x4
	.4byte	0x7bd
	.uleb128 0xf
	.byte	0x1
	.4byte	0x73
	.4byte	0x7cd
	.uleb128 0xe
	.4byte	0x7a6
	.byte	0
	.uleb128 0xb
	.4byte	.LASF112
	.byte	0x8
	.byte	0x1
	.byte	0x71
	.4byte	0x7f6
	.uleb128 0x6
	.4byte	.LASF113
	.byte	0x1
	.byte	0x73
	.4byte	0x789
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF114
	.byte	0x1
	.byte	0x74
	.4byte	0x7ac
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x10
	.4byte	.LASF116
	.byte	0x1
	.byte	0x9d
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x829
	.uleb128 0x11
	.4byte	.LASF115
	.byte	0x1
	.byte	0x9d
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0x9f
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x13
	.4byte	.LASF118
	.byte	0x1
	.byte	0xa6
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x860
	.uleb128 0x11
	.4byte	.LASF115
	.byte	0x1
	.byte	0xa6
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0xa8
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x10
	.4byte	.LASF117
	.byte	0x1
	.byte	0xb5
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x893
	.uleb128 0x11
	.4byte	.LASF115
	.byte	0x1
	.byte	0xb5
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0xb7
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x13
	.4byte	.LASF119
	.byte	0x1
	.byte	0xbe
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x8ca
	.uleb128 0x11
	.4byte	.LASF115
	.byte	0x1
	.byte	0xbe
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0xc0
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x10
	.4byte	.LASF120
	.byte	0x1
	.byte	0xcd
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x8fd
	.uleb128 0x11
	.4byte	.LASF115
	.byte	0x1
	.byte	0xcd
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0xcf
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x13
	.4byte	.LASF121
	.byte	0x1
	.byte	0xd6
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x934
	.uleb128 0x11
	.4byte	.LASF115
	.byte	0x1
	.byte	0xd6
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0xd8
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x10
	.4byte	.LASF122
	.byte	0x1
	.byte	0xe5
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x967
	.uleb128 0x11
	.4byte	.LASF115
	.byte	0x1
	.byte	0xe5
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0xe7
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x13
	.4byte	.LASF123
	.byte	0x1
	.byte	0xee
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x99e
	.uleb128 0x11
	.4byte	.LASF115
	.byte	0x1
	.byte	0xee
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0xf0
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x10
	.4byte	.LASF124
	.byte	0x1
	.byte	0xfd
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x9d1
	.uleb128 0x11
	.4byte	.LASF115
	.byte	0x1
	.byte	0xfd
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"i\000"
	.byte	0x1
	.byte	0xff
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x14
	.4byte	.LASF125
	.byte	0x1
	.2byte	0x106
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0xa0b
	.uleb128 0x15
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x106
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x108
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x115
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0xa41
	.uleb128 0x15
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x115
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x117
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x14
	.4byte	.LASF127
	.byte	0x1
	.2byte	0x11e
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0xa7b
	.uleb128 0x15
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x11e
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x120
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x17
	.4byte	.LASF128
	.byte	0x1
	.2byte	0x14a
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0xab3
	.uleb128 0x18
	.ascii	"ddl\000"
	.byte	0x1
	.2byte	0x14a
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.ascii	"tmp\000"
	.byte	0x1
	.2byte	0x14c
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x14
	.4byte	.LASF129
	.byte	0x1
	.2byte	0x16e
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.4byte	0xb2b
	.uleb128 0x15
	.4byte	.LASF130
	.byte	0x1
	.2byte	0x16e
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x15
	.4byte	.LASF131
	.byte	0x1
	.2byte	0x16e
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x15
	.4byte	.LASF132
	.byte	0x1
	.2byte	0x16e
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x19
	.4byte	.LASF133
	.byte	0x1
	.2byte	0x170
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.ascii	"inc\000"
	.byte	0x1
	.2byte	0x171
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x171
	.4byte	0x7a6
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF139
	.byte	0x1
	.2byte	0x1b1
	.byte	0x1
	.4byte	0x73
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.4byte	0xb95
	.uleb128 0x15
	.4byte	.LASF131
	.byte	0x1
	.2byte	0x1b1
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x15
	.4byte	.LASF132
	.byte	0x1
	.2byte	0x1b1
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.4byte	.LASF134
	.byte	0x1
	.2byte	0x1b3
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.4byte	.LASF135
	.byte	0x1
	.2byte	0x1b4
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF136
	.byte	0x1
	.2byte	0x1b4
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF137
	.byte	0x1
	.2byte	0x20b
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.4byte	0xbbf
	.uleb128 0x18
	.ascii	"cfg\000"
	.byte	0x1
	.2byte	0x20b
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1b
	.byte	0x1
	.4byte	.LASF138
	.byte	0x1
	.2byte	0x23b
	.byte	0x1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.4byte	0xbe9
	.uleb128 0x18
	.ascii	"clk\000"
	.byte	0x1
	.2byte	0x23b
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF140
	.byte	0x1
	.2byte	0x2c2
	.byte	0x1
	.4byte	0x61
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.4byte	0xc26
	.uleb128 0x16
	.ascii	"idx\000"
	.byte	0x1
	.2byte	0x2c4
	.4byte	0x73
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x16
	.ascii	"cfg\000"
	.byte	0x1
	.2byte	0x2c5
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.byte	0x1
	.4byte	.LASF141
	.byte	0x1
	.2byte	0x2fb
	.byte	0x1
	.4byte	0x61
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.4byte	0xc72
	.uleb128 0x18
	.ascii	"ba1\000"
	.byte	0x1
	.2byte	0x2fb
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.ascii	"ba0\000"
	.byte	0x1
	.2byte	0x2fb
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF142
	.byte	0x1
	.2byte	0x2fd
	.4byte	0x61
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF143
	.byte	0x5
	.byte	0x5d
	.4byte	0x73
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF144
	.byte	0x5
	.byte	0x5e
	.4byte	0x73
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF145
	.byte	0x5
	.byte	0x5f
	.4byte	0xc99
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	0x73
	.uleb128 0x1c
	.4byte	.LASF146
	.byte	0x5
	.byte	0x63
	.4byte	0x73
	.byte	0x1
	.byte	0x1
	.uleb128 0x1c
	.4byte	.LASF147
	.byte	0x5
	.byte	0x63
	.4byte	0x73
	.byte	0x1
	.byte	0x1
	.uleb128 0x8
	.4byte	0x744
	.4byte	0xcc8
	.uleb128 0x9
	.4byte	0x25
	.byte	0xc
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF148
	.byte	0x1
	.byte	0x32
	.4byte	0xcd5
	.byte	0x1
	.byte	0x1
	.uleb128 0x1d
	.4byte	0xcb8
	.uleb128 0x8
	.4byte	0x3a
	.4byte	0xcea
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1f
	.byte	0
	.uleb128 0x1e
	.4byte	.LASF149
	.byte	0x1
	.byte	0x78
	.4byte	0xcfb
	.byte	0x5
	.byte	0x3
	.4byte	dqs2calsen
	.uleb128 0x1d
	.4byte	0xcda
	.uleb128 0x8
	.4byte	0x7cd
	.4byte	0xd10
	.uleb128 0x9
	.4byte	0x25
	.byte	0x6
	.byte	0
	.uleb128 0x19
	.4byte	.LASF150
	.byte	0x1
	.2byte	0x12d
	.4byte	0xd00
	.byte	0x5
	.byte	0x3
	.4byte	testvecs
	.uleb128 0x1f
	.4byte	.LASF143
	.byte	0x1
	.byte	0x6b
	.4byte	0x73
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	modeshift
	.uleb128 0x1f
	.4byte	.LASF144
	.byte	0x1
	.byte	0x6c
	.4byte	0x73
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	bankshift
	.uleb128 0x1f
	.4byte	.LASF145
	.byte	0x1
	.byte	0x30
	.4byte	0xc99
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	bus32
	.uleb128 0x1f
	.4byte	.LASF146
	.byte	0x1
	.byte	0x7d
	.4byte	0x73
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	dqsstart
	.uleb128 0x1f
	.4byte	.LASF147
	.byte	0x1
	.byte	0x7d
	.4byte	0x73
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	dqsend
	.uleb128 0x1f
	.4byte	.LASF148
	.byte	0x1
	.byte	0x32
	.4byte	0xd8e
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	sdram_map
	.uleb128 0x1d
	.4byte	0xcb8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI40
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI43
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI46
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI49
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI52
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI55
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xac
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF120:
	.ascii	"invaddrsetup\000"
.LASF78:
	.ascii	"clkpwr_ddr_lap_nom\000"
.LASF18:
	.ascii	"emcstatic_turn_around_delay\000"
.LASF80:
	.ascii	"clkpwr_ddr_cal_delay\000"
.LASF57:
	.ascii	"clkpwr_bootmap\000"
.LASF111:
	.ascii	"SDRAM_CFG_MAP\000"
.LASF106:
	.ascii	"rows\000"
.LASF87:
	.ascii	"clkpwr_i2c_clk_ctrl\000"
.LASF100:
	.ascii	"clkpwr_uart_clk_ctrl\000"
.LASF34:
	.ascii	"emcdynamictras\000"
.LASF16:
	.ascii	"emcstatic_page_RD_delay\000"
.LASF128:
	.ascii	"dqsin_ddr_mod\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF123:
	.ascii	"noninvaddrcheck\000"
.LASF43:
	.ascii	"emcdynamictcdlr\000"
.LASF84:
	.ascii	"clkpwr_macclk_ctrl\000"
.LASF133:
	.ascii	"testnum\000"
.LASF117:
	.ascii	"walking1bitsetup\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF144:
	.ascii	"bankshift\000"
.LASF11:
	.ascii	"long long int\000"
.LASF4:
	.ascii	"signed char\000"
.LASF122:
	.ascii	"noninvaddrsetup\000"
.LASF113:
	.ascii	"testsetup\000"
.LASF95:
	.ascii	"clkpwr_uart3_clk_ctrl\000"
.LASF96:
	.ascii	"clkpwr_uart4_clk_ctrl\000"
.LASF145:
	.ascii	"bus32\000"
.LASF42:
	.ascii	"emcdynamictmrd\000"
.LASF98:
	.ascii	"clkpwr_uart6_clk_ctrl\000"
.LASF1:
	.ascii	"long int\000"
.LASF125:
	.ascii	"aa55check\000"
.LASF71:
	.ascii	"clkpwr_main_osc_ctrl\000"
.LASF59:
	.ascii	"clkpwr_usbclk_pdiv\000"
.LASF152:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/sdram_common.c\000"
.LASF88:
	.ascii	"clkpwr_key_clk_ctrl\000"
.LASF67:
	.ascii	"clkpwr_pin_ap\000"
.LASF124:
	.ascii	"aa55setup\000"
.LASF63:
	.ascii	"clkpwr_int_ap\000"
.LASF41:
	.ascii	"emcdynamictrrd\000"
.LASF21:
	.ascii	"emcahbcontrol\000"
.LASF92:
	.ascii	"clkpwr_timers_pwms_clk_ctrl_1\000"
.LASF19:
	.ascii	"reserved\000"
.LASF90:
	.ascii	"clkpwr_pwm_clk_ctrl\000"
.LASF75:
	.ascii	"clkpwr_adc_clk_ctrl_1\000"
.LASF81:
	.ascii	"clkpwr_ssp_blk_ctrl\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF149:
	.ascii	"dqs2calsen\000"
.LASF135:
	.ascii	"ppass\000"
.LASF130:
	.ascii	"seed\000"
.LASF79:
	.ascii	"clkpwr_ddr_lap_count\000"
.LASF56:
	.ascii	"EMC_REGS_T\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF40:
	.ascii	"emcdynamictxsr\000"
.LASF37:
	.ascii	"emcdynamictwr\000"
.LASF104:
	.ascii	"CLKPWR_REGS_T\000"
.LASF23:
	.ascii	"emcahbtimeout\000"
.LASF97:
	.ascii	"clkpwr_uart5_clk_ctrl\000"
.LASF107:
	.ascii	"cols\000"
.LASF108:
	.ascii	"cfgval\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF45:
	.ascii	"emcstaticextendedwait\000"
.LASF151:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF140:
	.ascii	"sdram_find_config\000"
.LASF139:
	.ascii	"ddr_find_dqsin_delay\000"
.LASF112:
	.ascii	"_memtests\000"
.LASF20:
	.ascii	"EMC_STATIC_CFG\000"
.LASF147:
	.ascii	"dqsend\000"
.LASF39:
	.ascii	"emcdynamictrfc\000"
.LASF110:
	.ascii	"pfrii\000"
.LASF119:
	.ascii	"walking1bitcheck\000"
.LASF115:
	.ascii	"base\000"
.LASF74:
	.ascii	"clkpwr_hclkpll_ctrl\000"
.LASF65:
	.ascii	"clkpwr_pin_rs\000"
.LASF91:
	.ascii	"clkpwr_timer_clk_ctrl\000"
.LASF73:
	.ascii	"clkpwr_lcdclk_ctrl\000"
.LASF61:
	.ascii	"clkpwr_int_rs\000"
.LASF13:
	.ascii	"emcstatic_WE_delay\000"
.LASF22:
	.ascii	"emcahbstatus\000"
.LASF25:
	.ascii	"emccontrol\000"
.LASF30:
	.ascii	"emcdynamicrefresh\000"
.LASF83:
	.ascii	"clkpwr_ms_ctrl\000"
.LASF31:
	.ascii	"emcdynamicreadconfig\000"
.LASF85:
	.ascii	"clkpwr_test_clk_sel\000"
.LASF143:
	.ascii	"modeshift\000"
.LASF47:
	.ascii	"emcdynamicconfig0\000"
.LASF50:
	.ascii	"emcdynamicconfig1\000"
.LASF29:
	.ascii	"emcdynamiccontrol\000"
.LASF101:
	.ascii	"clkpwr_dmaclk_ctrl\000"
.LASF66:
	.ascii	"clkpwr_pin_sr\000"
.LASF121:
	.ascii	"invaddrcheck\000"
.LASF93:
	.ascii	"clkpwr_spi_clk_ctrl\000"
.LASF103:
	.ascii	"clkpwr_uid\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF62:
	.ascii	"clkpwr_int_sr\000"
.LASF53:
	.ascii	"emcstatic_regs\000"
.LASF58:
	.ascii	"clkpwr_p01_er\000"
.LASF68:
	.ascii	"clkpwr_hclk_div\000"
.LASF89:
	.ascii	"clkpwr_adc_clk_ctrl\000"
.LASF6:
	.ascii	"short int\000"
.LASF38:
	.ascii	"emcdynamictrc\000"
.LASF55:
	.ascii	"emcahn_regs\000"
.LASF77:
	.ascii	"clkpwr_sdramclk_ctrl\000"
.LASF27:
	.ascii	"emcconfig\000"
.LASF131:
	.ascii	"start\000"
.LASF33:
	.ascii	"emcdynamictrp\000"
.LASF142:
	.ascii	"bankoffs\000"
.LASF76:
	.ascii	"clkpwr_usb_ctrl\000"
.LASF109:
	.ascii	"pfrvi\000"
.LASF114:
	.ascii	"testcheck\000"
.LASF2:
	.ascii	"char\000"
.LASF69:
	.ascii	"clkpwr_pwr_ctrl\000"
.LASF116:
	.ascii	"walking0bitsetup\000"
.LASF64:
	.ascii	"clkpwr_pin_er\000"
.LASF26:
	.ascii	"emcstatus\000"
.LASF141:
	.ascii	"sdram_get_bankmask\000"
.LASF86:
	.ascii	"clkpwr_sw_int\000"
.LASF60:
	.ascii	"clkpwr_int_er\000"
.LASF82:
	.ascii	"clkpwr_i2s_clk_ctrl\000"
.LASF127:
	.ascii	"_55aacheck\000"
.LASF148:
	.ascii	"sdram_map\000"
.LASF15:
	.ascii	"emcstatic_RD_access_delay\000"
.LASF150:
	.ascii	"testvecs\000"
.LASF146:
	.ascii	"dqsstart\000"
.LASF12:
	.ascii	"emcstatic_config\000"
.LASF72:
	.ascii	"clkpwr_sysclk_ctrl\000"
.LASF94:
	.ascii	"clkpwr_nand_clk_ctrl\000"
.LASF126:
	.ascii	"_55aasetup\000"
.LASF134:
	.ascii	"dqsindly\000"
.LASF7:
	.ascii	"UNS_8\000"
.LASF35:
	.ascii	"emcdynamictsrex\000"
.LASF138:
	.ascii	"sdram_adjust_timing\000"
.LASF48:
	.ascii	"emcdynamicrascas0\000"
.LASF51:
	.ascii	"emcdynamicrascas1\000"
.LASF17:
	.ascii	"emcstatic_WR_cycle_delay\000"
.LASF118:
	.ascii	"walking0bitcheck\000"
.LASF99:
	.ascii	"clkpwr_irda_clk_ctrl\000"
.LASF102:
	.ascii	"clkpwr_autoclock\000"
.LASF132:
	.ascii	"size\000"
.LASF137:
	.ascii	"ddr_clock_resync\000"
.LASF28:
	.ascii	"reserved1\000"
.LASF32:
	.ascii	"reserved2\000"
.LASF36:
	.ascii	"reserved3\000"
.LASF44:
	.ascii	"reserved4\000"
.LASF46:
	.ascii	"reserved5\000"
.LASF49:
	.ascii	"reserved6\000"
.LASF52:
	.ascii	"reserved7\000"
.LASF54:
	.ascii	"reserved8\000"
.LASF136:
	.ascii	"pass\000"
.LASF129:
	.ascii	"ddr_memtst\000"
.LASF14:
	.ascii	"emcstatic_OE_delay\000"
.LASF70:
	.ascii	"clkpwr_pll397_ctrl\000"
.LASF105:
	.ascii	"bank_bits\000"
.LASF24:
	.ascii	"EMC_AHB_CTRL_T\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
