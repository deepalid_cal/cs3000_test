	.file	"pdata_changes.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/pdata_changes.c\000"
	.section	.text.PDATA_extract_settings,"ax",%progbits
	.align	2
	.type	PDATA_extract_settings, %function
PDATA_extract_settings:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/pdata_changes.c"
	.loc 1 94 0
	@ args = 16, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, fp, lr}
.LCFI0:
	add	fp, sp, #16
.LCFI1:
	sub	sp, sp, #24
.LCFI2:
	str	r0, [fp, #-28]
	str	r1, [fp, #-32]
	str	r2, [fp, #-36]
	str	r3, [fp, #-40]
	.loc 1 99 0
	mov	r3, #0
	str	r3, [fp, #-20]
	.loc 1 101 0
	ldr	r3, [fp, #-36]
	ldmia	r3, {r2-r3}
	ldr	r1, [fp, #-40]
	sub	ip, r1, #32
	mov	r6, r3, lsr ip
	rsb	r0, r1, #32
	mov	r0, r3, asl r0
	mov	r4, r2, lsr r1
	orr	r4, r0, r4
	cmp	ip, #0
	movge	r4, r6
	mov	r5, r3, lsr r1
	mov	r3, r4
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L2
	.loc 1 104 0
	ldr	r0, [fp, #8]
	mov	r1, #400
	ldr	r2, .L6
	mov	r3, #104
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 106 0
	ldr	r3, [fp, #-28]
	ldr	r3, [r3, #0]
	ldr	ip, [fp, #4]
	mov	r0, r3
	ldr	r1, [fp, #-32]
	ldr	r2, [fp, #12]
	ldr	r3, [fp, #16]
	blx	ip
	str	r0, [fp, #-24]
	.loc 1 108 0
	ldr	r0, [fp, #8]
	bl	xQueueGiveMutexRecursive
	.loc 1 113 0
	ldr	r3, [fp, #-24]
	cmp	r3, #0
	beq	.L3
	.loc 1 126 0
	ldr	r3, [fp, #16]
	cmp	r3, #1
	beq	.L4
	.loc 1 126 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #16]
	cmp	r3, #15
	beq	.L4
	.loc 1 127 0 is_stmt 1
	ldr	r3, [fp, #16]
	cmp	r3, #16
	bne	.L5
	.loc 1 128 0
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	mov	r3, r0
	cmp	r3, #0
	bne	.L5
.L4:
	.loc 1 130 0
	ldr	r0, [fp, #-40]
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L5:
	.loc 1 142 0
	ldr	r0, [fp, #-40]
	bl	CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response
.L3:
	.loc 1 155 0
	ldr	r3, [fp, #-28]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	add	r2, r2, r3
	ldr	r3, [fp, #-28]
	str	r2, [r3, #0]
	.loc 1 156 0
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-24]
	add	r3, r2, r3
	str	r3, [fp, #-20]
	.loc 1 158 0
	ldr	r3, [fp, #-36]
	ldmia	r3, {r1-r2}
	ldr	r3, [fp, #-40]
	mov	r0, #1
	mov	r3, r0, asl r3
	mvn	r0, r3
	mov	r3, r0
	mov	r4, #0
	and	r3, r3, r1
	and	r4, r4, r2
	ldr	r2, [fp, #-36]
	stmia	r2, {r3-r4}
.L2:
	.loc 1 161 0
	ldr	r3, [fp, #-20]
	.loc 1 162 0
	mov	r0, r3
	sub	sp, fp, #16
	ldmfd	sp!, {r4, r5, r6, fp, pc}
.L7:
	.align	2
.L6:
	.word	.LC0
.LFE0:
	.size	PDATA_extract_settings, .-PDATA_extract_settings
	.section	.text.PDATA_extract_and_store_changes,"ax",%progbits
	.align	2
	.global	PDATA_extract_and_store_changes
	.type	PDATA_extract_and_store_changes, %function
PDATA_extract_and_store_changes:
.LFB1:
	.loc 1 201 0
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #48
.LCFI5:
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	str	r3, [fp, #-36]
	.loc 1 208 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 214 0
	ldr	r3, [fp, #-24]
	sub	r2, fp, #17
	mov	r0, r2
	mov	r1, r3
	mov	r2, #1
	bl	memcpy
	.loc 1 215 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
	.loc 1 216 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
	.loc 1 221 0
	ldr	r3, [fp, #-24]
	sub	r2, fp, #16
	mov	r0, r2
	mov	r1, r3
	mov	r2, #8
	bl	memcpy
	.loc 1 222 0
	ldr	r3, [fp, #-24]
	add	r3, r3, #8
	str	r3, [fp, #-24]
	.loc 1 223 0
	ldr	r3, [fp, #-8]
	add	r3, r3, #8
	str	r3, [fp, #-8]
	.loc 1 230 0
	ldr	r3, .L10
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+4
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #1
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 231 0
	ldr	r3, .L10+8
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+12
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #6
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 232 0
	ldr	r3, .L10+16
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+20
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #10
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 233 0
	ldr	r3, .L10
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+24
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #13
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 234 0
	ldr	r3, .L10
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+28
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #12
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 235 0
	ldr	r3, .L10
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+32
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #9
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 236 0
	ldr	r3, .L10+36
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+40
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #11
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 237 0
	ldr	r3, .L10+44
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+48
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #16
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 238 0
	ldr	r3, .L10+52
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+56
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #18
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 239 0
	ldr	r3, .L10+60
	ldr	r1, [r3, #0]
	sub	r2, fp, #24
	sub	r3, fp, #16
	ldr	r0, .L10+64
	str	r0, [sp, #0]
	str	r1, [sp, #4]
	ldr	r1, [fp, #-32]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-36]
	str	r1, [sp, #12]
	mov	r0, r2
	ldr	r1, [fp, #-28]
	mov	r2, r3
	mov	r3, #20
	bl	PDATA_extract_settings
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 248 0
	ldr	r0, .L10+68
	bl	postBackground_Calculation_Event
	.loc 1 254 0
	bl	FTIMES_TASK_restart_calculation
	.loc 1 269 0
	ldr	r3, [fp, #-36]
	cmp	r3, #1
	bne	.L9
	.loc 1 269 0 is_stmt 0 discriminator 1
	ldr	r3, .L10+72
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #36
	beq	.L9
	.loc 1 273 0 is_stmt 1
	bl	DIALOG_close_all_dialogs
	.loc 1 275 0
	mov	r0, #1
	bl	Redraw_Screen
.L9:
	.loc 1 299 0
	ldr	r3, [fp, #-8]
	.loc 1 300 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L11:
	.align	2
.L10:
	.word	list_program_data_recursive_MUTEX
	.word	NETWORK_CONFIG_extract_and_store_changes_from_comm
	.word	weather_control_recursive_MUTEX
	.word	nm_WEATHER_extract_and_store_changes_from_comm
	.word	list_system_recursive_MUTEX
	.word	nm_SYSTEM_extract_and_store_changes_from_comm
	.word	nm_STATION_GROUP_extract_and_store_changes_from_comm
	.word	nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm
	.word	nm_STATION_extract_and_store_changes_from_comm
	.word	list_poc_recursive_MUTEX
	.word	nm_POC_extract_and_store_changes_from_comm
	.word	list_lights_recursive_MUTEX
	.word	nm_LIGHTS_extract_and_store_changes_from_comm
	.word	moisture_sensor_items_recursive_MUTEX
	.word	nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm
	.word	walk_thru_recursive_MUTEX
	.word	nm_WALK_THRU_extract_and_store_changes_from_comm
	.word	4369
	.word	GuiLib_CurStructureNdx
.LFE1:
	.size	PDATA_extract_and_store_changes, .-PDATA_extract_and_store_changes
	.section	.text.PDATA_allocate_memory_for_and_initialize_memory_for_sending_data,"ax",%progbits
	.align	2
	.global	PDATA_allocate_memory_for_and_initialize_memory_for_sending_data
	.type	PDATA_allocate_memory_for_and_initialize_memory_for_sending_data, %function
PDATA_allocate_memory_for_and_initialize_memory_for_sending_data:
.LFB2:
	.loc 1 378 0
	@ args = 8, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI6:
	add	fp, sp, #8
.LCFI7:
	sub	sp, sp, #20
.LCFI8:
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	str	r2, [fp, #-24]
	str	r3, [fp, #-28]
	.loc 1 381 0
	mov	r3, #0
	str	r3, [fp, #-12]
	.loc 1 385 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 396 0
	ldr	r0, [fp, #8]
	ldr	r1, [fp, #-16]
	ldr	r2, .L15
	mov	r3, #396
	bl	mem_oabia
	mov	r3, r0
	cmp	r3, #0
	bne	.L13
	.loc 1 399 0
	ldr	r3, [fp, #-16]
	mov	r2, #0
	str	r2, [r3, #0]
.L13:
	.loc 1 411 0
	ldr	r2, [fp, #-20]
	mov	r3, #0
	mov	r4, #0
	stmia	r2, {r3-r4}
	.loc 1 413 0
	ldr	r3, [fp, #-24]
	mov	r2, #8
	strb	r2, [r3, #0]
	.loc 1 417 0
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L14
	.loc 1 420 0
	ldr	r3, [fp, #-24]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	.loc 1 419 0
	ldr	r0, [fp, #-16]
	mov	r1, r3
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #4]
	bl	PDATA_copy_bitfield_info_into_pucp
	str	r0, [fp, #-12]
.L14:
	.loc 1 427 0
	ldr	r3, [fp, #-12]
	.loc 1 428 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L16:
	.align	2
.L15:
	.word	.LC0
.LFE2:
	.size	PDATA_allocate_memory_for_and_initialize_memory_for_sending_data, .-PDATA_allocate_memory_for_and_initialize_memory_for_sending_data
	.section	.text.PDATA_allocate_space_for_num_changed_groups_in_pucp,"ax",%progbits
	.align	2
	.global	PDATA_allocate_space_for_num_changed_groups_in_pucp
	.type	PDATA_allocate_space_for_num_changed_groups_in_pucp, %function
PDATA_allocate_space_for_num_changed_groups_in_pucp:
.LFB3:
	.loc 1 436 0
	@ args = 4, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #20
.LCFI11:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	str	r3, [fp, #-20]
	.loc 1 439 0
	mov	r3, #0
	str	r3, [fp, #-4]
	.loc 1 441 0
	ldr	r3, [fp, #-20]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L18
	.loc 1 441 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-16]
	add	r2, r3, #4
	ldr	r3, [fp, #4]
	cmp	r2, r3
	bcs	.L18
	.loc 1 443 0 is_stmt 1
	ldr	r3, [fp, #-8]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 444 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	add	r2, r3, #4
	ldr	r3, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 445 0
	ldr	r3, [fp, #-4]
	add	r3, r3, #4
	str	r3, [fp, #-4]
	b	.L19
.L18:
	.loc 1 449 0
	ldr	r3, [fp, #-12]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 451 0
	ldr	r3, [fp, #-20]
	mov	r2, #0
	str	r2, [r3, #0]
.L19:
	.loc 1 454 0
	ldr	r3, [fp, #-4]
	.loc 1 455 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.LFE3:
	.size	PDATA_allocate_space_for_num_changed_groups_in_pucp, .-PDATA_allocate_space_for_num_changed_groups_in_pucp
	.section	.text.PDATA_copy_var_into_pucp,"ax",%progbits
	.align	2
	.global	PDATA_copy_var_into_pucp
	.type	PDATA_copy_var_into_pucp, %function
PDATA_copy_var_into_pucp:
.LFB4:
	.loc 1 461 0
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #16
.LCFI14:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	.loc 1 464 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 466 0
	ldr	r3, [fp, #-12]
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, [fp, #-16]
	ldr	r2, [fp, #-20]
	bl	memcpy
	.loc 1 467 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	add	r2, r2, r3
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 468 0
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-20]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 470 0
	ldr	r3, [fp, #-8]
	.loc 1 471 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE4:
	.size	PDATA_copy_var_into_pucp, .-PDATA_copy_var_into_pucp
	.section	.text.PDATA_copy_bitfield_info_into_pucp,"ax",%progbits
	.align	2
	.global	PDATA_copy_bitfield_info_into_pucp
	.type	PDATA_copy_bitfield_info_into_pucp, %function
PDATA_copy_bitfield_info_into_pucp:
.LFB5:
	.loc 1 478 0
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI15:
	add	fp, sp, #4
.LCFI16:
	sub	sp, sp, #20
.LCFI17:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 481 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 483 0
	sub	r3, fp, #16
	ldr	r0, [fp, #-12]
	mov	r1, r3
	ldr	r2, [fp, #-20]
	bl	PDATA_copy_var_into_pucp
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 487 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 488 0
	ldr	r3, [fp, #-12]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	add	r2, r2, r3
	ldr	r3, [fp, #-12]
	str	r2, [r3, #0]
	.loc 1 489 0
	ldr	r3, [fp, #-16]
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 491 0
	ldr	r3, [fp, #-8]
	.loc 1 492 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE5:
	.size	PDATA_copy_bitfield_info_into_pucp, .-PDATA_copy_bitfield_info_into_pucp
	.section	.text.PDATA_copy_var_into_pucp_and_set_32_bit_change_bits,"ax",%progbits
	.align	2
	.global	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	.type	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits, %function
PDATA_copy_var_into_pucp_and_set_32_bit_change_bits:
.LFB6:
	.loc 1 506 0
	@ args = 28, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI18:
	add	fp, sp, #4
.LCFI19:
	sub	sp, sp, #20
.LCFI20:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 509 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 514 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	mov	r1, #1
	mov	r3, r1, asl r3
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L23
	.loc 1 517 0
	ldr	r2, [fp, #16]
	ldr	r3, [fp, #12]
	add	r2, r2, r3
	ldr	r3, [fp, #24]
	cmp	r2, r3
	bcs	.L24
	.loc 1 519 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #8]
	ldr	r2, [fp, #12]
	bl	PDATA_copy_var_into_pucp
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 521 0
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	mov	r1, #1
	mov	r3, r1, asl r3
	orr	r2, r2, r3
	ldr	r3, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 532 0
	ldr	r3, [fp, #-24]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	mov	r1, #1
	mov	r3, r1, asl r3
	mvn	r3, r3
	and	r2, r2, r3
	ldr	r3, [fp, #-24]
	str	r2, [r3, #0]
	.loc 1 536 0
	ldr	r3, [fp, #28]
	cmp	r3, #19
	bne	.L23
	.loc 1 538 0
	ldr	r3, [fp, #4]
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-20]
	mov	r1, #1
	mov	r3, r1, asl r3
	orr	r2, r2, r3
	ldr	r3, [fp, #4]
	str	r2, [r3, #0]
	b	.L23
.L24:
	.loc 1 545 0
	ldr	r3, [fp, #20]
	mov	r2, #0
	str	r2, [r3, #0]
.L23:
	.loc 1 549 0
	ldr	r3, [fp, #-8]
	.loc 1 550 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE6:
	.size	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits, .-PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	.section	.text.PDATA_copy_var_into_pucp_and_set_64_bit_change_bits,"ax",%progbits
	.align	2
	.global	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	.type	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits, %function
PDATA_copy_var_into_pucp_and_set_64_bit_change_bits:
.LFB7:
	.loc 1 564 0
	@ args = 28, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI21:
	add	fp, sp, #32
.LCFI22:
	sub	sp, sp, #28
.LCFI23:
	str	r0, [fp, #-40]
	str	r1, [fp, #-44]
	str	r2, [fp, #-48]
	str	r3, [fp, #-52]
	.loc 1 567 0
	mov	r3, #0
	str	r3, [fp, #-36]
	.loc 1 572 0
	ldr	r3, [fp, #-52]
	ldmia	r3, {r1-r2}
	ldr	r0, [fp, #-48]
	sub	ip, r0, #32
	mov	sl, r2, lsr ip
	rsb	r3, r0, #32
	mov	r3, r2, asl r3
	mov	r4, r1, lsr r0
	orr	r4, r3, r4
	cmp	ip, #0
	movge	r4, sl
	mov	r5, r2, lsr r0
	mov	r3, r4
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L26
	.loc 1 575 0
	ldr	r2, [fp, #16]
	ldr	r3, [fp, #12]
	add	r2, r2, r3
	ldr	r3, [fp, #24]
	cmp	r2, r3
	bcs	.L27
	.loc 1 577 0
	ldr	r0, [fp, #-40]
	ldr	r1, [fp, #8]
	ldr	r2, [fp, #12]
	bl	PDATA_copy_var_into_pucp
	mov	r3, r0
	ldr	r2, [fp, #-36]
	add	r3, r2, r3
	str	r3, [fp, #-36]
	.loc 1 581 0
	ldr	r3, [fp, #-44]
	ldmia	r3, {r4-r5}
	ldr	r1, [fp, #-48]
	sub	r0, r1, #32
	mov	r3, #1
	mov	ip, r3, asl r0
	rsb	r2, r1, #32
	mov	r3, #1
	mov	r2, r3, lsr r2
	mov	r3, #0
	mov	r3, r3, asl r1
	str	r3, [fp, #-56]
	ldr	r3, [fp, #-56]
	orr	r3, r2, r3
	str	r3, [fp, #-56]
	cmp	r0, #0
	ldr	r8, [fp, #-56]
	movge	r8, ip
	str	r8, [fp, #-56]
	mov	r3, #1
	mov	r3, r3, asl r1
	str	r3, [fp, #-60]
	sub	r9, fp, #60
	ldmia	r9, {r8-r9}
	orr	r1, r4, r8
	orr	r2, r5, r9
	mov	r3, r1
	mov	r4, r2
	ldr	r2, [fp, #-44]
	stmia	r2, {r3-r4}
	.loc 1 592 0
	ldr	r3, [fp, #-52]
	ldmia	r3, {r0-r1}
	ldr	r2, [fp, #-48]
	sub	r4, r2, #32
	mov	r3, #1
	mov	r5, r3, asl r4
	rsb	r3, r2, #32
	mov	ip, #1
	mov	r3, ip, lsr r3
	mov	ip, #0
	mov	r9, ip, asl r2
	orr	r9, r3, r9
	cmp	r4, #0
	movge	r9, r5
	mov	r3, #1
	mov	r8, r3, asl r2
	mvn	r3, r8
	mvn	r4, r9
	and	r3, r3, r0
	and	r4, r4, r1
	ldr	r2, [fp, #-52]
	stmia	r2, {r3-r4}
	.loc 1 596 0
	ldr	r3, [fp, #28]
	cmp	r3, #19
	bne	.L26
	.loc 1 598 0
	ldr	r3, [fp, #4]
	ldmia	r3, {r1-r2}
	ldr	r3, [fp, #-48]
	sub	r0, r3, #32
	mov	ip, #1
	mov	ip, ip, asl r0
	rsb	r4, r3, #32
	mov	r5, #1
	mov	r4, r5, lsr r4
	mov	r5, #0
	mov	r7, r5, asl r3
	orr	r7, r4, r7
	cmp	r0, #0
	movge	r7, ip
	mov	r0, #1
	mov	r6, r0, asl r3
	orr	r3, r1, r6
	orr	r4, r2, r7
	ldr	r2, [fp, #4]
	stmia	r2, {r3-r4}
	b	.L26
.L27:
	.loc 1 605 0
	ldr	r3, [fp, #20]
	mov	r2, #0
	str	r2, [r3, #0]
.L26:
	.loc 1 609 0
	ldr	r3, [fp, #-36]
	.loc 1 610 0
	mov	r0, r3
	sub	sp, fp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.LFE7:
	.size	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits, .-PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	.section	.text.PDATA_set_bit_that_file_has_changed,"ax",%progbits
	.align	2
	.type	PDATA_set_bit_that_file_has_changed, %function
PDATA_set_bit_that_file_has_changed:
.LFB8:
	.loc 1 615 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, fp}
.LCFI24:
	add	fp, sp, #16
.LCFI25:
	sub	sp, sp, #8
.LCFI26:
	str	r0, [fp, #-20]
	str	r1, [fp, #-24]
	.loc 1 616 0
	ldr	r2, [fp, #-24]
	ldmia	r2, {r0-r1}
	ldr	r2, [fp, #-20]
	sub	ip, r2, #32
	mov	r5, #1
	mov	r5, r5, asl ip
	rsb	r6, r2, #32
	mov	r7, #1
	mov	r6, r7, lsr r6
	mov	r7, #0
	mov	r4, r7, asl r2
	orr	r4, r6, r4
	cmp	ip, #0
	movge	r4, r5
	mov	ip, #1
	mov	r3, ip, asl r2
	orr	r3, r3, r0
	orr	r4, r4, r1
	ldr	r2, [fp, #-24]
	stmia	r2, {r3-r4}
	.loc 1 617 0
	sub	sp, fp, #16
	ldmfd	sp!, {r4, r5, r6, r7, fp}
	bx	lr
.LFE8:
	.size	PDATA_set_bit_that_file_has_changed, .-PDATA_set_bit_that_file_has_changed
	.section	.text.PDATA_copy_file_bitfield_into_pucp,"ax",%progbits
	.align	2
	.type	PDATA_copy_file_bitfield_into_pucp, %function
PDATA_copy_file_bitfield_into_pucp:
.LFB9:
	.loc 1 624 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI27:
	add	fp, sp, #4
.LCFI28:
	sub	sp, sp, #12
.LCFI29:
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	.loc 1 625 0
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, [fp, #-12]
	ldr	r2, [fp, #-16]
	bl	memcpy
	.loc 1 626 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE9:
	.size	PDATA_copy_file_bitfield_into_pucp, .-PDATA_copy_file_bitfield_into_pucp
	.section	.text.PDATA_build_setting_to_send,"ax",%progbits
	.align	2
	.type	PDATA_build_setting_to_send, %function
PDATA_build_setting_to_send:
.LFB10:
	.loc 1 649 0
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI30:
	add	fp, sp, #4
.LCFI31:
	sub	sp, sp, #24
.LCFI32:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	str	r2, [fp, #-20]
	str	r3, [fp, #-24]
	.loc 1 652 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 654 0
	ldr	r3, [fp, #8]
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L31
	.loc 1 656 0
	ldr	r3, [fp, #16]
	str	r3, [sp, #0]
	ldr	ip, [fp, #-20]
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #4]
	ldr	r2, [fp, #8]
	ldr	r3, [fp, #12]
	blx	ip
	mov	r3, r0
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
	.loc 1 658 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L31
	.loc 1 660 0
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-16]
	bl	PDATA_set_bit_that_file_has_changed
	.loc 1 672 0
	ldr	r0, [fp, #-12]
	bl	CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response
.L31:
	.loc 1 676 0
	ldr	r3, [fp, #-8]
	.loc 1 677 0
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.LFE10:
	.size	PDATA_build_setting_to_send, .-PDATA_build_setting_to_send
	.section .rodata
	.align	2
.LC1:
	.ascii	"No memory to send program data\000"
	.align	2
.LC2:
	.ascii	"Pending changes flag unexpdly set\000"
	.section	.text.PDATA_build_data_to_send,"ax",%progbits
	.align	2
	.global	PDATA_build_data_to_send
	.type	PDATA_build_data_to_send, %function
PDATA_build_data_to_send:
.LFB11:
	.loc 1 695 0
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI33:
	add	fp, sp, #8
.LCFI34:
	sub	sp, sp, #64
.LCFI35:
	str	r0, [fp, #-48]
	str	r1, [fp, #-52]
	str	r2, [fp, #-56]
	.loc 1 724 0
	ldr	r3, [fp, #-48]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 726 0
	ldr	r3, [fp, #-48]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 730 0
	ldr	r3, [fp, #-52]
	cmp	r3, #19
	beq	.L33
	.loc 1 730 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-52]
	cmp	r3, #20
	bne	.L34
.L33:
	.loc 1 735 0 is_stmt 1
	ldr	r3, .L42
	str	r3, [fp, #-16]
	b	.L35
.L34:
	.loc 1 739 0
	ldr	r3, [fp, #-56]
	cmp	r3, #0
	beq	.L36
	.loc 1 741 0
	mov	r3, #24576
	str	r3, [fp, #-16]
	b	.L35
.L36:
	.loc 1 745 0
	mov	r3, #2048
	str	r3, [fp, #-16]
.L35:
	.loc 1 751 0
	sub	r1, fp, #36
	sub	r2, fp, #28
	sub	r3, fp, #29
	sub	r0, fp, #40
	str	r0, [sp, #0]
	ldr	r0, [fp, #-16]
	str	r0, [sp, #4]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, #1
	bl	PDATA_allocate_memory_for_and_initialize_memory_for_sending_data
	str	r0, [fp, #-20]
	.loc 1 758 0
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	bne	.L37
	.loc 1 760 0
	ldr	r0, .L42+4
	bl	Alert_Message
	.loc 1 762 0
	mov	r3, #256
	str	r3, [fp, #-12]
	b	.L38
.L37:
	.loc 1 770 0
	ldr	r3, .L42+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L42+12
	ldr	r3, .L42+16
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 776 0
	ldr	r2, [fp, #-36]
	ldr	r3, [fp, #-20]
	rsb	r3, r3, #0
	add	r2, r2, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #0]
	.loc 1 778 0
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-20]
	str	r2, [r3, #4]
	.loc 1 782 0
	mov	r3, #1
	str	r3, [fp, #-44]
	.loc 1 789 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #1
	mov	r1, r2
	ldr	r2, .L42+20
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 791 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #6
	mov	r1, r2
	ldr	r2, .L42+24
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 793 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #10
	mov	r1, r2
	ldr	r2, .L42+28
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 795 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #13
	mov	r1, r2
	ldr	r2, .L42+32
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 797 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #12
	mov	r1, r2
	ldr	r2, .L42+36
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 799 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #9
	mov	r1, r2
	ldr	r2, .L42+40
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 801 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #11
	mov	r1, r2
	ldr	r2, .L42+44
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 803 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #16
	mov	r1, r2
	ldr	r2, .L42+48
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 805 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #18
	mov	r1, r2
	ldr	r2, .L42+52
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 807 0
	ldr	r3, [fp, #-48]
	ldr	r4, [r3, #4]
	ldr	r3, [fp, #-48]
	ldr	r1, [r3, #4]
	sub	r2, fp, #28
	sub	r3, fp, #36
	str	r1, [sp, #0]
	sub	r1, fp, #44
	str	r1, [sp, #4]
	ldr	r1, [fp, #-52]
	str	r1, [sp, #8]
	ldr	r1, [fp, #-16]
	str	r1, [sp, #12]
	mov	r0, #20
	mov	r1, r2
	ldr	r2, .L42+56
	bl	PDATA_build_setting_to_send
	mov	r3, r0
	add	r2, r4, r3
	ldr	r3, [fp, #-48]
	str	r2, [r3, #4]
	.loc 1 811 0
	sub	r2, fp, #40
	sub	r3, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #8
	bl	PDATA_copy_file_bitfield_into_pucp
	.loc 1 818 0
	ldr	r3, [fp, #-48]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L39
	.loc 1 820 0
	ldr	r3, [fp, #-52]
	cmp	r3, #19
	bne	.L40
	.loc 1 824 0
	ldr	r3, .L42+60
	ldr	r3, [r3, #112]
	cmp	r3, #0
	beq	.L40
	.loc 1 826 0
	ldr	r0, .L42+64
	bl	Alert_Message
	.loc 1 829 0
	ldr	r3, .L42+60
	mov	r2, #0
	str	r2, [r3, #112]
.L40:
	.loc 1 834 0
	ldr	r3, [fp, #-48]
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, .L42+12
	ldr	r2, .L42+68
	bl	mem_free_debug
	.loc 1 839 0
	ldr	r3, [fp, #-48]
	mov	r2, #0
	str	r2, [r3, #4]
	.loc 1 841 0
	ldr	r3, [fp, #-48]
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 846 0
	mov	r3, #512
	str	r3, [fp, #-12]
	b	.L41
.L39:
	.loc 1 850 0
	mov	r3, #768
	str	r3, [fp, #-12]
	.loc 1 856 0
	ldr	r3, [fp, #-52]
	cmp	r3, #19
	bne	.L41
	.loc 1 858 0
	ldr	r3, .L42+60
	mov	r2, #0
	str	r2, [r3, #112]
.L41:
	.loc 1 862 0
	ldr	r3, .L42+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L38:
	.loc 1 867 0
	ldr	r3, [fp, #-12]
	.loc 1 868 0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L43:
	.align	2
.L42:
	.word	1433600
	.word	.LC1
	.word	pending_changes_recursive_MUTEX
	.word	.LC0
	.word	770
	.word	NETWORK_CONFIG_build_data_to_send
	.word	WEATHER_build_data_to_send
	.word	SYSTEM_build_data_to_send
	.word	STATION_GROUP_build_data_to_send
	.word	MANUAL_PROGRAMS_build_data_to_send
	.word	STATION_build_data_to_send
	.word	POC_build_data_to_send
	.word	LIGHTS_build_data_to_send
	.word	MOISTURE_SENSOR_build_data_to_send
	.word	WALK_THRU_build_data_to_send
	.word	weather_preserves
	.word	.LC2
	.word	834
.LFE11:
	.size	PDATA_build_data_to_send, .-PDATA_build_data_to_send
	.section	.text.PDATA_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	PDATA_update_pending_change_bits
	.type	PDATA_update_pending_change_bits, %function
PDATA_update_pending_change_bits:
.LFB12:
	.loc 1 878 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI36:
	add	fp, sp, #4
.LCFI37:
	sub	sp, sp, #4
.LCFI38:
	str	r0, [fp, #-8]
	.loc 1 879 0
	ldr	r3, .L45
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L45+4
	ldr	r3, .L45+8
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 881 0
	ldr	r0, [fp, #-8]
	bl	nm_NETWORK_CONFIG_update_pending_change_bits
	.loc 1 883 0
	ldr	r0, [fp, #-8]
	bl	nm_WEATHER_update_pending_change_bits
	.loc 1 885 0
	ldr	r0, [fp, #-8]
	bl	nm_SYSTEM_update_pending_change_bits
	.loc 1 887 0
	ldr	r0, [fp, #-8]
	bl	nm_STATION_GROUP_update_pending_change_bits
	.loc 1 889 0
	ldr	r0, [fp, #-8]
	bl	nm_MANUAL_PROGRAMS_update_pending_change_bits
	.loc 1 891 0
	ldr	r0, [fp, #-8]
	bl	nm_STATION_update_pending_change_bits
	.loc 1 893 0
	ldr	r0, [fp, #-8]
	bl	nm_POC_update_pending_change_bits
	.loc 1 895 0
	ldr	r0, [fp, #-8]
	bl	nm_LIGHTS_update_pending_change_bits
	.loc 1 897 0
	ldr	r0, [fp, #-8]
	bl	nm_MOISTURE_SENSOR_update_pending_change_bits
	.loc 1 899 0
	ldr	r0, [fp, #-8]
	bl	nm_WALK_THRU_update_pending_change_bits
	.loc 1 901 0
	ldr	r3, .L45
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 903 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L46:
	.align	2
.L45:
	.word	pending_changes_recursive_MUTEX
	.word	.LC0
	.word	879
.LFE12:
	.size	PDATA_update_pending_change_bits, .-PDATA_update_pending_change_bits
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI24-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8b
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI27-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI30-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE24:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_comm_server_common.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_vars.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xd04
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF138
	.byte	0x1
	.4byte	.LASF139
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x70
	.4byte	0x89
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x2
	.byte	0x99
	.4byte	0x70
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x4c
	.4byte	0xc7
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x3
	.byte	0x55
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x57
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x59
	.4byte	0xa2
	.uleb128 0x5
	.byte	0x4
	.byte	0x3
	.byte	0x65
	.4byte	0xf7
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x3
	.byte	0x67
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x3
	.byte	0x69
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x3
	.byte	0x6b
	.4byte	0xd2
	.uleb128 0x5
	.byte	0x8
	.byte	0x4
	.byte	0x14
	.4byte	0x127
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x4
	.byte	0x17
	.4byte	0x127
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF20
	.byte	0x4
	.byte	0x1a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x33
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x4
	.byte	0x1c
	.4byte	0x102
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF22
	.uleb128 0x3
	.4byte	.LASF23
	.byte	0x5
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x6
	.byte	0x57
	.4byte	0x138
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x7
	.byte	0x4c
	.4byte	0x14c
	.uleb128 0x9
	.4byte	0x3e
	.4byte	0x172
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x182
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.byte	0x6
	.byte	0x8
	.byte	0x22
	.4byte	0x1a3
	.uleb128 0xb
	.ascii	"T\000"
	.byte	0x8
	.byte	0x24
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.ascii	"D\000"
	.byte	0x8
	.byte	0x26
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF26
	.byte	0x8
	.byte	0x28
	.4byte	0x182
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF27
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x1c5
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x1d5
	.uleb128 0xa
	.4byte	0x25
	.byte	0xf
	.byte	0
	.uleb128 0xc
	.byte	0x1c
	.byte	0x9
	.2byte	0x10c
	.4byte	0x248
	.uleb128 0xd
	.ascii	"rip\000"
	.byte	0x9
	.2byte	0x112
	.4byte	0xf7
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF28
	.byte	0x9
	.2byte	0x11b
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xe
	.4byte	.LASF29
	.byte	0x9
	.2byte	0x122
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xe
	.4byte	.LASF30
	.byte	0x9
	.2byte	0x127
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xe
	.4byte	.LASF31
	.byte	0x9
	.2byte	0x138
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF32
	.byte	0x9
	.2byte	0x144
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.4byte	.LASF33
	.byte	0x9
	.2byte	0x14b
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.byte	0
	.uleb128 0xf
	.4byte	.LASF34
	.byte	0x9
	.2byte	0x14d
	.4byte	0x1d5
	.uleb128 0xc
	.byte	0xec
	.byte	0x9
	.2byte	0x150
	.4byte	0x428
	.uleb128 0xe
	.4byte	.LASF35
	.byte	0x9
	.2byte	0x157
	.4byte	0x1c5
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xe
	.4byte	.LASF36
	.byte	0x9
	.2byte	0x162
	.4byte	0x4c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xe
	.4byte	.LASF37
	.byte	0x9
	.2byte	0x164
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xe
	.4byte	.LASF38
	.byte	0x9
	.2byte	0x166
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xe
	.4byte	.LASF39
	.byte	0x9
	.2byte	0x168
	.4byte	0x1a3
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xe
	.4byte	.LASF40
	.byte	0x9
	.2byte	0x16e
	.4byte	0xc7
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0xe
	.4byte	.LASF41
	.byte	0x9
	.2byte	0x174
	.4byte	0x248
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0xe
	.4byte	.LASF42
	.byte	0x9
	.2byte	0x17b
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0xe
	.4byte	.LASF43
	.byte	0x9
	.2byte	0x17d
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0x9
	.2byte	0x185
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0xe
	.4byte	.LASF45
	.byte	0x9
	.2byte	0x18d
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0x9
	.2byte	0x191
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0xe
	.4byte	.LASF47
	.byte	0x9
	.2byte	0x195
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0xe
	.4byte	.LASF48
	.byte	0x9
	.2byte	0x199
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0xe
	.4byte	.LASF49
	.byte	0x9
	.2byte	0x19e
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0xe
	.4byte	.LASF50
	.byte	0x9
	.2byte	0x1a2
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0xe
	.4byte	.LASF51
	.byte	0x9
	.2byte	0x1a6
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0xe
	.4byte	.LASF52
	.byte	0x9
	.2byte	0x1b4
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0xe
	.4byte	.LASF53
	.byte	0x9
	.2byte	0x1ba
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0xe
	.4byte	.LASF54
	.byte	0x9
	.2byte	0x1c2
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0xe
	.4byte	.LASF55
	.byte	0x9
	.2byte	0x1c4
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0xe
	.4byte	.LASF56
	.byte	0x9
	.2byte	0x1c6
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0xe
	.4byte	.LASF57
	.byte	0x9
	.2byte	0x1d5
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0xe
	.4byte	.LASF58
	.byte	0x9
	.2byte	0x1d7
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x81
	.uleb128 0xe
	.4byte	.LASF59
	.byte	0x9
	.2byte	0x1dd
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0xe
	.4byte	.LASF60
	.byte	0x9
	.2byte	0x1e7
	.4byte	0x33
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0xe
	.4byte	.LASF61
	.byte	0x9
	.2byte	0x1f0
	.4byte	0x65
	.byte	0x3
	.byte	0x23
	.uleb128 0x84
	.uleb128 0xe
	.4byte	.LASF62
	.byte	0x9
	.2byte	0x1f7
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0xe
	.4byte	.LASF63
	.byte	0x9
	.2byte	0x1f9
	.4byte	0x97
	.byte	0x3
	.byte	0x23
	.uleb128 0x8c
	.uleb128 0xe
	.4byte	.LASF64
	.byte	0x9
	.2byte	0x1fd
	.4byte	0x428
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.byte	0
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x438
	.uleb128 0xa
	.4byte	0x25
	.byte	0x16
	.byte	0
	.uleb128 0xf
	.4byte	.LASF65
	.byte	0x9
	.2byte	0x204
	.4byte	0x254
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF66
	.uleb128 0x10
	.4byte	.LASF107
	.byte	0x1
	.byte	0x46
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x4f3
	.uleb128 0x11
	.4byte	.LASF67
	.byte	0x1
	.byte	0x46
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x11
	.4byte	.LASF68
	.byte	0x1
	.byte	0x47
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x11
	.4byte	.LASF69
	.byte	0x1
	.byte	0x48
	.4byte	0x4fe
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x11
	.4byte	.LASF70
	.byte	0x1
	.byte	0x49
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x11
	.4byte	.LASF71
	.byte	0x1
	.byte	0x4a
	.4byte	0x533
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x11
	.4byte	.LASF72
	.byte	0x1
	.byte	0x4b
	.4byte	0x138
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x11
	.4byte	.LASF73
	.byte	0x1
	.byte	0x4c
	.4byte	0x52e
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x11
	.4byte	.LASF74
	.byte	0x1
	.byte	0x4d
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x12
	.4byte	.LASF75
	.byte	0x1
	.byte	0x5f
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x13
	.ascii	"rv\000"
	.byte	0x1
	.byte	0x61
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x127
	.uleb128 0x14
	.4byte	0x65
	.uleb128 0x7
	.byte	0x4
	.4byte	0x7e
	.uleb128 0x15
	.byte	0x1
	.4byte	0x65
	.4byte	0x523
	.uleb128 0x16
	.4byte	0x523
	.uleb128 0x16
	.4byte	0x4f9
	.uleb128 0x16
	.4byte	0x52e
	.uleb128 0x16
	.4byte	0x4f9
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x529
	.uleb128 0x14
	.4byte	0x33
	.uleb128 0x14
	.4byte	0x97
	.uleb128 0x7
	.byte	0x4
	.4byte	0x504
	.uleb128 0x17
	.byte	0x1
	.4byte	.LASF78
	.byte	0x1
	.byte	0xbb
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x5b8
	.uleb128 0x11
	.4byte	.LASF67
	.byte	0x1
	.byte	0xbb
	.4byte	0x127
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x11
	.4byte	.LASF68
	.byte	0x1
	.byte	0xbc
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x11
	.4byte	.LASF73
	.byte	0x1
	.byte	0xbd
	.4byte	0x52e
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x11
	.4byte	.LASF74
	.byte	0x1
	.byte	0xbe
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x12
	.4byte	.LASF76
	.byte	0x1
	.byte	0xca
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF77
	.byte	0x1
	.byte	0xcc
	.4byte	0x33
	.byte	0x2
	.byte	0x91
	.sleb128 -21
	.uleb128 0x13
	.ascii	"rv\000"
	.byte	0x1
	.byte	0xce
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x174
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x63f
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x174
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x175
	.4byte	0x4fe
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x176
	.4byte	0x127
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x177
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x19
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x178
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x19
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x179
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x1a
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x17b
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x1af
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x6b7
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x1af
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x1b0
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x1b1
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x1b2
	.4byte	0x6b7
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x1b3
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x1a
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1b5
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x97
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x1ca
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x717
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x1ca
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x1cb
	.4byte	0x717
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x1cc
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1a
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1ce
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.4byte	0x71c
	.uleb128 0x7
	.byte	0x4
	.4byte	0x722
	.uleb128 0x1b
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x1da
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x78c
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x1da
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x1db
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.4byte	.LASF95
	.byte	0x1
	.2byte	0x1dc
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x1dd
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1a
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1df
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x1ef
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x85e
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x1ef
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x1f0
	.4byte	0x85e
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x1f1
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x1f2
	.4byte	0x85e
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x1f3
	.4byte	0x85e
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x19
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x1f4
	.4byte	0x138
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x19
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x1f5
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x19
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x1f6
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x19
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x1f7
	.4byte	0x6b7
	.byte	0x2
	.byte	0x91
	.sleb128 16
	.uleb128 0x19
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x1f8
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 20
	.uleb128 0x19
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x1f9
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 24
	.uleb128 0x1a
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x1fb
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x65
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x229
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.4byte	0x936
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x229
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x19
	.4byte	.LASF97
	.byte	0x1
	.2byte	0x22a
	.4byte	0x4fe
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x19
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x22b
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x19
	.4byte	.LASF99
	.byte	0x1
	.2byte	0x22c
	.4byte	0x4fe
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x19
	.4byte	.LASF100
	.byte	0x1
	.2byte	0x22d
	.4byte	0x4fe
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x19
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x22e
	.4byte	0x138
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x19
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x22f
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x19
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x230
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x19
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x231
	.4byte	0x6b7
	.byte	0x2
	.byte	0x91
	.sleb128 16
	.uleb128 0x19
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x232
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 20
	.uleb128 0x19
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x233
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 24
	.uleb128 0x1a
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x235
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x265
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.4byte	0x96e
	.uleb128 0x19
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x265
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x266
	.4byte	0x4fe
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x26c
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.4byte	0x9b5
	.uleb128 0x19
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x26c
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x26d
	.4byte	0x4fe
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x26e
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x1d
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x27d
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.4byte	0xa59
	.uleb128 0x19
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x27d
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x27e
	.4byte	0x4fe
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x27f
	.4byte	0xa7d
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x284
	.4byte	0x4f3
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x285
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x19
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x286
	.4byte	0x6b7
	.byte	0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x19
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x287
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x19
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x288
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.uleb128 0x1a
	.ascii	"rv\000"
	.byte	0x1
	.2byte	0x28a
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.4byte	0x65
	.4byte	0xa7d
	.uleb128 0x16
	.4byte	0x4f3
	.uleb128 0x16
	.4byte	0x4f9
	.uleb128 0x16
	.4byte	0x6b7
	.uleb128 0x16
	.4byte	0x4f9
	.uleb128 0x16
	.4byte	0x4f9
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0xa59
	.uleb128 0x18
	.byte	0x1
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x2b6
	.byte	0x1
	.4byte	0x65
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.4byte	0xb47
	.uleb128 0x19
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x2b6
	.4byte	0xb47
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x19
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x2b6
	.4byte	0x4f9
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x19
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x2b6
	.4byte	0x52e
	.byte	0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x1e
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x2c0
	.4byte	0x7e
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x1e
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x2c2
	.4byte	0x33
	.byte	0x2
	.byte	0x91
	.sleb128 -33
	.uleb128 0x1a
	.ascii	"ucp\000"
	.byte	0x1
	.2byte	0x2c4
	.4byte	0x127
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x1e
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x2c6
	.4byte	0x127
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x1e
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x2c8
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1e
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x2ca
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1e
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x2cc
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1e
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x2ce
	.4byte	0x97
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x7
	.byte	0x4
	.4byte	0x12d
	.uleb128 0x1f
	.byte	0x1
	.4byte	.LASF140
	.byte	0x1
	.2byte	0x36d
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.4byte	0xb77
	.uleb128 0x19
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x36d
	.4byte	0x52e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x20
	.4byte	.LASF126
	.byte	0xb
	.2byte	0x132
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF122
	.byte	0xa
	.byte	0x30
	.4byte	0xb96
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x14
	.4byte	0x162
	.uleb128 0x12
	.4byte	.LASF123
	.byte	0xa
	.byte	0x34
	.4byte	0xbac
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x14
	.4byte	0x162
	.uleb128 0x12
	.4byte	.LASF124
	.byte	0xa
	.byte	0x36
	.4byte	0xbc2
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x14
	.4byte	0x162
	.uleb128 0x12
	.4byte	.LASF125
	.byte	0xa
	.byte	0x38
	.4byte	0xbd8
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x14
	.4byte	0x162
	.uleb128 0x21
	.4byte	.LASF127
	.byte	0xc
	.byte	0x78
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF128
	.byte	0xc
	.byte	0xc6
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF129
	.byte	0xc
	.byte	0xc9
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF130
	.byte	0xc
	.byte	0xcc
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF131
	.byte	0xc
	.byte	0xd8
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF132
	.byte	0xc
	.byte	0xfe
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF133
	.byte	0xc
	.2byte	0x111
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF134
	.byte	0xc
	.2byte	0x117
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF135
	.byte	0xd
	.byte	0x33
	.4byte	0xc58
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x14
	.4byte	0x172
	.uleb128 0x12
	.4byte	.LASF136
	.byte	0xd
	.byte	0x3f
	.4byte	0xc6e
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x14
	.4byte	0x1b5
	.uleb128 0x20
	.4byte	.LASF137
	.byte	0x9
	.2byte	0x206
	.4byte	0x438
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF126
	.byte	0xb
	.2byte	0x132
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF127
	.byte	0xc
	.byte	0x78
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF128
	.byte	0xc
	.byte	0xc6
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF129
	.byte	0xc
	.byte	0xc9
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF130
	.byte	0xc
	.byte	0xcc
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF131
	.byte	0xc
	.byte	0xd8
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x21
	.4byte	.LASF132
	.byte	0xc
	.byte	0xfe
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF133
	.byte	0xc
	.2byte	0x111
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF134
	.byte	0xc
	.2byte	0x117
	.4byte	0x157
	.byte	0x1
	.byte	0x1
	.uleb128 0x20
	.4byte	.LASF137
	.byte	0x9
	.2byte	0x206
	.4byte	0x438
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI28
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI34
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x7c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF102:
	.ascii	"PDATA_copy_var_into_pucp_and_set_64_bit_change_bits"
	.ascii	"\000"
.LASF73:
	.ascii	"pset_change_bits\000"
.LASF55:
	.ascii	"commserver_monthly_xmit_bytes\000"
.LASF60:
	.ascii	"ununsed_uns8_1\000"
.LASF124:
	.ascii	"GuiFont_DecimalChar\000"
.LASF62:
	.ascii	"hub_needs_to_distribute_tpmicro_binary\000"
.LASF47:
	.ascii	"yesterdays_ET_was_zero\000"
.LASF74:
	.ascii	"pchanges_received_from\000"
.LASF83:
	.ascii	"plocation_of_bitfield\000"
.LASF104:
	.ascii	"pdata_bitfield_of_files_that_have_changed\000"
.LASF13:
	.ascii	"BOOL_32\000"
.LASF72:
	.ascii	"plist_recursive_mutex\000"
.LASF36:
	.ascii	"dls_saved_date\000"
.LASF32:
	.ascii	"inhibit_irrigation_have_crossed_the_minimum\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF26:
	.ascii	"DATE_TIME\000"
.LASF12:
	.ascii	"long long int\000"
.LASF3:
	.ascii	"signed char\000"
.LASF30:
	.ascii	"midnight_to_midnight_raw_pulse_count\000"
.LASF80:
	.ascii	"pbitfield_of_files_that_have_changed\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF64:
	.ascii	"expansion\000"
.LASF17:
	.ascii	"rain_inches_u16_100u\000"
.LASF22:
	.ascii	"long int\000"
.LASF59:
	.ascii	"perform_a_discovery_following_a_FACTORY_reset\000"
.LASF39:
	.ascii	"dls_after_fall_when_to_clear_ignore_sxs\000"
.LASF66:
	.ascii	"double\000"
.LASF67:
	.ascii	"pucp\000"
.LASF99:
	.ascii	"pchange_bits_for_controller_ptr\000"
.LASF33:
	.ascii	"needs_to_be_broadcast\000"
.LASF97:
	.ascii	"pbitfield_of_changes_to_send\000"
.LASF139:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/pdata_changes.c\000"
.LASF127:
	.ascii	"list_program_data_recursive_MUTEX\000"
.LASF31:
	.ascii	"rain_shutdown_rcvd_from_commserver_uns32\000"
.LASF103:
	.ascii	"pfile_that_has_changed\000"
.LASF57:
	.ascii	"factory_reset_do_not_allow_pdata_to_be_sent\000"
.LASF110:
	.ascii	"pbuild_data_func_ptr\000"
.LASF105:
	.ascii	"PDATA_set_bit_that_file_has_changed\000"
.LASF69:
	.ascii	"pbitfield_of_rcvd_changes\000"
.LASF106:
	.ascii	"PDATA_copy_file_bitfield_into_pucp\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF38:
	.ascii	"dls_after_fall_back_ignore_sxs\000"
.LASF134:
	.ascii	"walk_thru_recursive_MUTEX\000"
.LASF115:
	.ascii	"pdata_bitfield_size\000"
.LASF21:
	.ascii	"DATA_HANDLE\000"
.LASF44:
	.ascii	"et_table_update_all_historical_values\000"
.LASF34:
	.ascii	"RAIN_STATE\000"
.LASF71:
	.ascii	"pextract_and_store_changes_func_ptr\000"
.LASF6:
	.ascii	"short unsigned int\000"
.LASF82:
	.ascii	"psizeof_bitfield_size\000"
.LASF138:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF56:
	.ascii	"commserver_monthly_code_receipt_bytes\000"
.LASF113:
	.ascii	"this_is_to_ourself\000"
.LASF92:
	.ascii	"psize_of_var\000"
.LASF109:
	.ascii	"pfile_index\000"
.LASF95:
	.ascii	"psize_of_size_of_bitfield\000"
.LASF63:
	.ascii	"hub_needs_to_distribute_main_binary\000"
.LASF24:
	.ascii	"xQueueHandle\000"
.LASF130:
	.ascii	"weather_control_recursive_MUTEX\000"
.LASF108:
	.ascii	"PDATA_build_setting_to_send\000"
.LASF76:
	.ascii	"lbitfield_of_changes\000"
.LASF70:
	.ascii	"pbit\000"
.LASF51:
	.ascii	"freeze_switch_active\000"
.LASF101:
	.ascii	"preason_data_is_being_built\000"
.LASF53:
	.ascii	"pending_changes_to_send_to_comm_server\000"
.LASF125:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF10:
	.ascii	"UNS_64\000"
.LASF79:
	.ascii	"PDATA_allocate_memory_for_and_initialize_memory_for"
	.ascii	"_sending_data\000"
.LASF43:
	.ascii	"nlu_old_rain_table_sync_variable\000"
.LASF117:
	.ascii	"lmessage_overhead\000"
.LASF128:
	.ascii	"list_system_recursive_MUTEX\000"
.LASF98:
	.ascii	"pbit_to_set\000"
.LASF86:
	.ascii	"llocation_of_num_changed_groups\000"
.LASF77:
	.ascii	"lsize_of_bitfield\000"
.LASF119:
	.ascii	"lmemory_to_allocate\000"
.LASF27:
	.ascii	"float\000"
.LASF84:
	.ascii	"pmemory_to_allocate\000"
.LASF122:
	.ascii	"GuiFont_LanguageActive\000"
.LASF20:
	.ascii	"dlen\000"
.LASF58:
	.ascii	"factory_reset_registration_to_tell_commserver_to_cl"
	.ascii	"ear_pdata\000"
.LASF61:
	.ascii	"commserver_monthly_mobile_status_updates_bytes\000"
.LASF112:
	.ascii	"pmsg_handle\000"
.LASF54:
	.ascii	"commserver_monthly_rcvd_bytes\000"
.LASF29:
	.ascii	"roll_to_roll_raw_pulse_count\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF126:
	.ascii	"GuiLib_CurStructureNdx\000"
.LASF90:
	.ascii	"PDATA_copy_var_into_pucp\000"
.LASF121:
	.ascii	"pcomm_error_occurred\000"
.LASF7:
	.ascii	"short int\000"
.LASF68:
	.ascii	"preason_for_change\000"
.LASF42:
	.ascii	"sync_the_et_rain_tables\000"
.LASF49:
	.ascii	"clear_runaway_gage\000"
.LASF131:
	.ascii	"pending_changes_recursive_MUTEX\000"
.LASF23:
	.ascii	"portTickType\000"
.LASF133:
	.ascii	"moisture_sensor_items_recursive_MUTEX\000"
.LASF19:
	.ascii	"dptr\000"
.LASF116:
	.ascii	"llocation_of_bitfield\000"
.LASF41:
	.ascii	"rain\000"
.LASF123:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF78:
	.ascii	"PDATA_extract_and_store_changes\000"
.LASF81:
	.ascii	"pbitfield_size\000"
.LASF93:
	.ascii	"PDATA_copy_bitfield_info_into_pucp\000"
.LASF1:
	.ascii	"char\000"
.LASF137:
	.ascii	"weather_preserves\000"
.LASF52:
	.ascii	"write_tpmicro_file_in_new_file_format\000"
.LASF107:
	.ascii	"PDATA_extract_settings\000"
.LASF46:
	.ascii	"run_away_gage\000"
.LASF45:
	.ascii	"dont_use_et_gage_today\000"
.LASF132:
	.ascii	"list_lights_recursive_MUTEX\000"
.LASF120:
	.ascii	"still_space_available\000"
.LASF50:
	.ascii	"rain_switch_active\000"
.LASF85:
	.ascii	"PDATA_allocate_space_for_num_changed_groups_in_pucp"
	.ascii	"\000"
.LASF91:
	.ascii	"pvar\000"
.LASF14:
	.ascii	"et_inches_u16_10000u\000"
.LASF28:
	.ascii	"hourly_total_inches_100u\000"
.LASF140:
	.ascii	"PDATA_update_pending_change_bits\000"
.LASF75:
	.ascii	"lsize_of_changes\000"
.LASF96:
	.ascii	"PDATA_copy_var_into_pucp_and_set_32_bit_change_bits"
	.ascii	"\000"
.LASF18:
	.ascii	"RAIN_TABLE_ENTRY\000"
.LASF129:
	.ascii	"list_poc_recursive_MUTEX\000"
.LASF136:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF94:
	.ascii	"psize_of_bitfield\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF15:
	.ascii	"status\000"
.LASF48:
	.ascii	"remaining_gage_pulses\000"
.LASF4:
	.ascii	"UNS_8\000"
.LASF40:
	.ascii	"et_rip\000"
.LASF111:
	.ascii	"PDATA_build_data_to_send\000"
.LASF37:
	.ascii	"dls_eligible_to_fall_back\000"
.LASF25:
	.ascii	"xSemaphoreHandle\000"
.LASF16:
	.ascii	"ET_TABLE_ENTRY\000"
.LASF89:
	.ascii	"pallocated_memory\000"
.LASF65:
	.ascii	"WEATHER_PRESERVES_STRUCT\000"
.LASF35:
	.ascii	"verify_string_pre\000"
.LASF118:
	.ascii	"results\000"
.LASF135:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF88:
	.ascii	"pmem_used_so_far_is_less_than_allocated_memory\000"
.LASF114:
	.ascii	"lbitfield_of_files_that_have_changed\000"
.LASF87:
	.ascii	"pmem_used_so_far\000"
.LASF100:
	.ascii	"ppending_change_bits_to_send_to_comm_server\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
