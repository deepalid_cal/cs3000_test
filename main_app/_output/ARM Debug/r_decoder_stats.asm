	.file	"r_decoder_stats.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_TWO_WIRE_list_item_index,"aw",%nobits
	.align	2
	.type	g_TWO_WIRE_list_item_index, %object
	.size	g_TWO_WIRE_list_item_index, 4
g_TWO_WIRE_list_item_index:
	.space	4
	.section	.text.TWO_WIRE_copy_decoder_stats_into_guivars,"ax",%progbits
	.align	2
	.type	TWO_WIRE_copy_decoder_stats_into_guivars, %function
TWO_WIRE_copy_decoder_stats_into_guivars:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_decoder_stats.c"
	.loc 1 54 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	str	r0, [fp, #-4]
	.loc 1 55 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #220
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L2+4
	str	r2, [r3, #0]
	.loc 1 57 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #232
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+8
	str	r2, [r3, #0]
	.loc 1 58 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #236
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+12
	str	r2, [r3, #0]
	.loc 1 59 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #240
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+16
	str	r2, [r3, #0]
	.loc 1 60 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #236
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+20
	str	r2, [r3, #0]
	.loc 1 61 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #236
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+24
	str	r2, [r3, #0]
	.loc 1 62 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #240
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+28
	str	r2, [r3, #0]
	.loc 1 63 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #248
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+32
	str	r2, [r3, #0]
	.loc 1 64 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #248
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+36
	str	r2, [r3, #0]
	.loc 1 65 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #244
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+40
	str	r2, [r3, #0]
	.loc 1 66 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #244
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+44
	str	r2, [r3, #0]
	.loc 1 67 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #244
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+48
	str	r2, [r3, #0]
	.loc 1 68 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #248
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+52
	str	r2, [r3, #0]
	.loc 1 69 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #244
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+56
	str	r2, [r3, #0]
	.loc 1 70 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #240
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+60
	str	r2, [r3, #0]
	.loc 1 71 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #240
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+64
	str	r2, [r3, #0]
	.loc 1 72 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #248
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+68
	str	r2, [r3, #0]
	.loc 1 73 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #252
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+72
	str	r2, [r3, #0]
	.loc 1 74 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #252
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+76
	str	r2, [r3, #0]
	.loc 1 75 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #252
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+80
	str	r2, [r3, #0]
	.loc 1 76 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #252
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+84
	str	r2, [r3, #0]
	.loc 1 77 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #232
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+88
	str	r2, [r3, #0]
	.loc 1 78 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #236
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+92
	str	r2, [r3, #0]
	.loc 1 79 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #256
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+96
	str	r2, [r3, #0]
	.loc 1 80 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #232
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+100
	str	r2, [r3, #0]
	.loc 1 82 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #232
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L2+104
	str	r2, [r3, #0]
	.loc 1 83 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #228
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrh	r3, [r3, #0]
	mov	r2, r3
	ldr	r3, .L2+108
	str	r2, [r3, #0]
	.loc 1 84 0
	ldr	r0, .L2
	ldr	r2, [fp, #-4]
	mov	r1, #228
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldrh	r3, [r3, #2]
	mov	r2, r3
	ldr	r3, .L2+112
	str	r2, [r3, #0]
	.loc 1 85 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L3:
	.align	2
.L2:
	.word	tpmicro_data
	.word	GuiVar_TwoWireDecoderSN
	.word	GuiVar_TwoWireStatsBODs
	.word	GuiVar_TwoWireStatsEEPCRCComParams
	.word	GuiVar_TwoWireStatsEEPCRCErrStats
	.word	GuiVar_TwoWireStatsEEPCRCS1Params
	.word	GuiVar_TwoWireStatsEEPCRCS2Params
	.word	GuiVar_TwoWireStatsRxCRCErrs
	.word	GuiVar_TwoWireStatsRxDataLpbkMsgs
	.word	GuiVar_TwoWireStatsRxDecRstMsgs
	.word	GuiVar_TwoWireStatsRxDiscConfMsgs
	.word	GuiVar_TwoWireStatsRxDiscRstMsgs
	.word	GuiVar_TwoWireStatsRxEnqMsgs
	.word	GuiVar_TwoWireStatsRxGetParamsMsgs
	.word	GuiVar_TwoWireStatsRxIDReqMsgs
	.word	GuiVar_TwoWireStatsRxLongMsgs
	.word	GuiVar_TwoWireStatsRxMsgs
	.word	GuiVar_TwoWireStatsRxPutParamsMsgs
	.word	GuiVar_TwoWireStatsRxSolCtrlMsgs
	.word	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs
	.word	GuiVar_TwoWireStatsRxStatReqMsgs
	.word	GuiVar_TwoWireStatsRxStatsReqMsgs
	.word	GuiVar_TwoWireStatsS1UCos
	.word	GuiVar_TwoWireStatsS2UCos
	.word	GuiVar_TwoWireStatsTxAcksSent
	.word	GuiVar_TwoWireStatsWDTs
	.word	GuiVar_TwoWireStatsPORs
	.word	GuiVar_TwoWireStatsTempMax
	.word	GuiVar_TwoWireStatsTempCur
.LFE0:
	.size	TWO_WIRE_copy_decoder_stats_into_guivars, .-TWO_WIRE_copy_decoder_stats_into_guivars
	.section	.text.FDTO_TWO_WIRE_update_decoder_stats,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_update_decoder_stats
	.type	FDTO_TWO_WIRE_update_decoder_stats, %function
FDTO_TWO_WIRE_update_decoder_stats:
.LFB1:
	.loc 1 102 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 103 0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	TWO_WIRE_copy_decoder_stats_into_guivars
	.loc 1 105 0
	bl	GuiLib_Refresh
	.loc 1 106 0
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	g_TWO_WIRE_list_item_index
.LFE1:
	.size	FDTO_TWO_WIRE_update_decoder_stats, .-FDTO_TWO_WIRE_update_decoder_stats
	.section	.text.FDTO_TWO_WIRE_draw_decoder_stats_report,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_draw_decoder_stats_report
	.type	FDTO_TWO_WIRE_draw_decoder_stats_report, %function
FDTO_TWO_WIRE_draw_decoder_stats_report:
.LFB2:
	.loc 1 125 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #12
.LCFI7:
	str	r0, [fp, #-16]
	.loc 1 130 0
	ldr	r3, [fp, #-16]
	cmp	r3, #1
	bne	.L8
	.loc 1 132 0
	ldr	r3, .L15
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 134 0
	mov	r3, #0
	str	r3, [fp, #-8]
	.loc 1 136 0
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	TWO_WIRE_copy_decoder_stats_into_guivars
	.loc 1 138 0
	ldr	r3, .L15+4
	mov	r2, #0
	str	r2, [r3, #0]
	.loc 1 141 0
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L9
.L12:
	.loc 1 143 0
	ldr	r0, .L15+8
	ldr	r2, [fp, #-12]
	mov	r1, #220
	mov	r3, r2
	mov	r3, r3, asl #4
	rsb	r3, r2, r3
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L14
	.loc 1 145 0
	ldr	r3, .L15+4
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L15+4
	str	r2, [r3, #0]
	.loc 1 141 0
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L9:
	.loc 1 141 0 is_stmt 0 discriminator 1
	ldr	r3, [fp, #-12]
	cmp	r3, #79
	bls	.L12
	.loc 1 141 0
	b	.L13
.L8:
	.loc 1 155 0 is_stmt 1
	ldr	r3, .L15+12
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
	b	.L13
.L14:
	.loc 1 149 0
	mov	r0, r0	@ nop
.L13:
	.loc 1 158 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #76
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 159 0
	bl	GuiLib_Refresh
	.loc 1 160 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L16:
	.align	2
.L15:
	.word	g_TWO_WIRE_list_item_index
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
	.word	tpmicro_data
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_TWO_WIRE_draw_decoder_stats_report, .-FDTO_TWO_WIRE_draw_decoder_stats_report
	.section	.text.TWO_WIRE_process_decoder_stats_report,"ax",%progbits
	.align	2
	.global	TWO_WIRE_process_decoder_stats_report
	.type	TWO_WIRE_process_decoder_stats_report, %function
TWO_WIRE_process_decoder_stats_report:
.LFB3:
	.loc 1 179 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	sub	sp, sp, #12
.LCFI10:
	str	r0, [fp, #-16]
	str	r1, [fp, #-12]
	.loc 1 182 0
	ldr	r3, [fp, #-16]
	cmp	r3, #3
	beq	.L21
	cmp	r3, #3
	bhi	.L24
	cmp	r3, #1
	beq	.L19
	cmp	r3, #2
	beq	.L20
	b	.L18
.L24:
	cmp	r3, #20
	beq	.L22
	cmp	r3, #67
	beq	.L23
	cmp	r3, #16
	beq	.L22
	b	.L18
.L20:
	.loc 1 185 0
	ldr	r3, .L35
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	beq	.L26
	cmp	r3, #1
	beq	.L27
	b	.L34
.L26:
	.loc 1 188 0
	bl	TWO_WIRE_request_statistics_from_all_decoders_from_ui
	.loc 1 189 0
	b	.L28
.L27:
	.loc 1 192 0
	bl	TWO_WIRE_clear_statistics_at_all_decoders_from_ui
	.loc 1 193 0
	b	.L28
.L34:
	.loc 1 196 0
	bl	bad_key_beep
	.loc 1 198 0
	b	.L17
.L28:
	b	.L17
.L22:
	.loc 1 202 0
	ldr	r3, .L35+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-8]
	.loc 1 204 0
	ldr	r3, [fp, #-16]
	cmp	r3, #20
	bne	.L30
	.loc 1 206 0
	ldr	r3, .L35+8
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L35+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L31
	.loc 1 208 0
	ldr	r3, .L35+4
	ldr	r3, [r3, #0]
	add	r2, r3, #1
	ldr	r3, .L35+4
	str	r2, [r3, #0]
	b	.L31
.L30:
	.loc 1 213 0
	ldr	r3, .L35+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L31
	.loc 1 215 0
	ldr	r3, .L35+4
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	ldr	r3, .L35+4
	str	r2, [r3, #0]
.L31:
	.loc 1 219 0
	ldr	r3, .L35+4
	ldr	r3, [r3, #0]
	mov	r2, r3
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	beq	.L32
	.loc 1 221 0
	bl	good_key_beep
	.loc 1 223 0
	ldr	r3, .L35+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	TWO_WIRE_copy_decoder_stats_into_guivars
	.loc 1 225 0
	bl	Refresh_Screen
	.loc 1 231 0
	b	.L17
.L32:
	.loc 1 229 0
	bl	bad_key_beep
	.loc 1 231 0
	b	.L17
.L19:
	.loc 1 234 0
	mov	r0, #1
	bl	CURSOR_Up
	.loc 1 235 0
	b	.L17
.L21:
	.loc 1 238 0
	mov	r0, #1
	bl	CURSOR_Down
	.loc 1 239 0
	b	.L17
.L23:
	.loc 1 242 0
	ldr	r3, .L35+12
	mov	r2, #11
	str	r2, [r3, #0]
	.loc 1 244 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 248 0
	mov	r0, #1
	bl	TWO_WIRE_DEBUG_draw_dialog
	.loc 1 249 0
	b	.L17
.L18:
	.loc 1 252 0
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L17:
	.loc 1 254 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L36:
	.align	2
.L35:
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_TWO_WIRE_list_item_index
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	TWO_WIRE_process_decoder_stats_report, .-TWO_WIRE_process_decoder_stats_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/Program Files (x86)/Rowley Associates Limited/CrossWorks for ARM 2.2/include/stdint.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/../decoder/_common_decoder_files/shared_to_cs3000/data_types.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/../decoder/_common_decoder_files/shared_to_cs3000/eeprom.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../decoder/_common_decoder_files/shared_to_cs3000/protocolmsgs.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cs3000_tpmicro_common.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/tp_board/tpmicro_data.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xcfc
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF175
	.byte	0x1
	.4byte	.LASF176
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4c
	.4byte	0x45
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x55
	.4byte	0x57
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5e
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x67
	.4byte	0x7b
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF13
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF14
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x11
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x12
	.4byte	0x45
	.uleb128 0x5
	.4byte	0x2c
	.4byte	0xcf
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.byte	0x4
	.byte	0x7c
	.4byte	0xf4
	.uleb128 0x8
	.4byte	.LASF17
	.byte	0x4
	.byte	0x7e
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF18
	.byte	0x4
	.byte	0x80
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x4
	.byte	0x82
	.4byte	0xcf
	.uleb128 0x9
	.ascii	"U16\000"
	.byte	0x5
	.byte	0xb
	.4byte	0xb4
	.uleb128 0x9
	.ascii	"U8\000"
	.byte	0x5
	.byte	0xc
	.4byte	0xa9
	.uleb128 0x7
	.byte	0x1d
	.byte	0x6
	.byte	0x9b
	.4byte	0x297
	.uleb128 0x8
	.4byte	.LASF20
	.byte	0x6
	.byte	0x9d
	.4byte	0xff
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF21
	.byte	0x6
	.byte	0x9e
	.4byte	0xff
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x8
	.4byte	.LASF22
	.byte	0x6
	.byte	0x9f
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x8
	.4byte	.LASF23
	.byte	0x6
	.byte	0xa0
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x5
	.uleb128 0x8
	.4byte	.LASF24
	.byte	0x6
	.byte	0xa1
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x8
	.4byte	.LASF25
	.byte	0x6
	.byte	0xa2
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x7
	.uleb128 0x8
	.4byte	.LASF26
	.byte	0x6
	.byte	0xa3
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x8
	.4byte	.LASF27
	.byte	0x6
	.byte	0xa4
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x9
	.uleb128 0x8
	.4byte	.LASF28
	.byte	0x6
	.byte	0xa5
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.uleb128 0x8
	.4byte	.LASF29
	.byte	0x6
	.byte	0xa6
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0xb
	.uleb128 0x8
	.4byte	.LASF30
	.byte	0x6
	.byte	0xa7
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x8
	.4byte	.LASF31
	.byte	0x6
	.byte	0xa8
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0xd
	.uleb128 0x8
	.4byte	.LASF32
	.byte	0x6
	.byte	0xa9
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0xe
	.uleb128 0x8
	.4byte	.LASF33
	.byte	0x6
	.byte	0xaa
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0xf
	.uleb128 0x8
	.4byte	.LASF34
	.byte	0x6
	.byte	0xab
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x8
	.4byte	.LASF35
	.byte	0x6
	.byte	0xac
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x11
	.uleb128 0x8
	.4byte	.LASF36
	.byte	0x6
	.byte	0xad
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x12
	.uleb128 0x8
	.4byte	.LASF37
	.byte	0x6
	.byte	0xae
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x13
	.uleb128 0x8
	.4byte	.LASF38
	.byte	0x6
	.byte	0xaf
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x8
	.4byte	.LASF39
	.byte	0x6
	.byte	0xb0
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x15
	.uleb128 0x8
	.4byte	.LASF40
	.byte	0x6
	.byte	0xb1
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x16
	.uleb128 0x8
	.4byte	.LASF41
	.byte	0x6
	.byte	0xb2
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x17
	.uleb128 0x8
	.4byte	.LASF42
	.byte	0x6
	.byte	0xb3
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x8
	.4byte	.LASF43
	.byte	0x6
	.byte	0xb4
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x19
	.uleb128 0x8
	.4byte	.LASF44
	.byte	0x6
	.byte	0xb5
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1a
	.uleb128 0x8
	.4byte	.LASF45
	.byte	0x6
	.byte	0xb6
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1b
	.uleb128 0x8
	.4byte	.LASF46
	.byte	0x6
	.byte	0xb7
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.byte	0
	.uleb128 0x3
	.4byte	.LASF47
	.byte	0x6
	.byte	0xb9
	.4byte	0x114
	.uleb128 0xa
	.byte	0x4
	.byte	0x7
	.2byte	0x16b
	.4byte	0x2d9
	.uleb128 0xb
	.4byte	.LASF48
	.byte	0x7
	.2byte	0x16d
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF49
	.byte	0x7
	.2byte	0x16e
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0xb
	.4byte	.LASF50
	.byte	0x7
	.2byte	0x16f
	.4byte	0xff
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0xc
	.4byte	.LASF51
	.byte	0x7
	.2byte	0x171
	.4byte	0x2a2
	.uleb128 0xa
	.byte	0xb
	.byte	0x7
	.2byte	0x193
	.4byte	0x33a
	.uleb128 0xb
	.4byte	.LASF52
	.byte	0x7
	.2byte	0x195
	.4byte	0x2d9
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF53
	.byte	0x7
	.2byte	0x196
	.4byte	0x2d9
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF54
	.byte	0x7
	.2byte	0x197
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF55
	.byte	0x7
	.2byte	0x198
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x9
	.uleb128 0xb
	.4byte	.LASF56
	.byte	0x7
	.2byte	0x199
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.byte	0
	.uleb128 0xc
	.4byte	.LASF57
	.byte	0x7
	.2byte	0x19b
	.4byte	0x2e5
	.uleb128 0xa
	.byte	0x4
	.byte	0x7
	.2byte	0x221
	.4byte	0x37d
	.uleb128 0xb
	.4byte	.LASF58
	.byte	0x7
	.2byte	0x223
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF59
	.byte	0x7
	.2byte	0x225
	.4byte	0x10a
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0xb
	.4byte	.LASF60
	.byte	0x7
	.2byte	0x227
	.4byte	0xff
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0xc
	.4byte	.LASF61
	.byte	0x7
	.2byte	0x229
	.4byte	0x346
	.uleb128 0x7
	.byte	0xc
	.byte	0x8
	.byte	0x25
	.4byte	0x3ba
	.uleb128 0xd
	.ascii	"sn\000"
	.byte	0x8
	.byte	0x28
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF62
	.byte	0x8
	.byte	0x2b
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xd
	.ascii	"on\000"
	.byte	0x8
	.byte	0x2e
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.4byte	.LASF63
	.byte	0x8
	.byte	0x30
	.4byte	0x389
	.uleb128 0xa
	.byte	0x4
	.byte	0x8
	.2byte	0x193
	.4byte	0x3de
	.uleb128 0xb
	.4byte	.LASF64
	.byte	0x8
	.2byte	0x196
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xc
	.4byte	.LASF65
	.byte	0x8
	.2byte	0x198
	.4byte	0x3c5
	.uleb128 0xa
	.byte	0xc
	.byte	0x8
	.2byte	0x1b0
	.4byte	0x421
	.uleb128 0xb
	.4byte	.LASF66
	.byte	0x8
	.2byte	0x1b2
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF67
	.byte	0x8
	.2byte	0x1b7
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF68
	.byte	0x8
	.2byte	0x1bc
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0xc
	.4byte	.LASF69
	.byte	0x8
	.2byte	0x1be
	.4byte	0x3ea
	.uleb128 0xa
	.byte	0x4
	.byte	0x8
	.2byte	0x1c3
	.4byte	0x446
	.uleb128 0xb
	.4byte	.LASF70
	.byte	0x8
	.2byte	0x1ca
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0xc
	.4byte	.LASF71
	.byte	0x8
	.2byte	0x1d0
	.4byte	0x42d
	.uleb128 0xe
	.4byte	.LASF177
	.byte	0x10
	.byte	0x8
	.2byte	0x1ff
	.4byte	0x49c
	.uleb128 0xb
	.4byte	.LASF72
	.byte	0x8
	.2byte	0x202
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF73
	.byte	0x8
	.2byte	0x205
	.4byte	0x37d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF74
	.byte	0x8
	.2byte	0x207
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF75
	.byte	0x8
	.2byte	0x20c
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.byte	0
	.uleb128 0xc
	.4byte	.LASF76
	.byte	0x8
	.2byte	0x211
	.4byte	0x4a8
	.uleb128 0x5
	.4byte	0x452
	.4byte	0x4b8
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.byte	0xc
	.byte	0x8
	.2byte	0x3a4
	.4byte	0x51c
	.uleb128 0xb
	.4byte	.LASF77
	.byte	0x8
	.2byte	0x3a6
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF78
	.byte	0x8
	.2byte	0x3a8
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xb
	.4byte	.LASF79
	.byte	0x8
	.2byte	0x3aa
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF80
	.byte	0x8
	.2byte	0x3ac
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0xb
	.4byte	.LASF81
	.byte	0x8
	.2byte	0x3ae
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF82
	.byte	0x8
	.2byte	0x3b0
	.4byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0xa
	.byte	0
	.uleb128 0xc
	.4byte	.LASF83
	.byte	0x8
	.2byte	0x3b2
	.4byte	0x4b8
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF84
	.uleb128 0x5
	.4byte	0x5e
	.4byte	0x53f
	.uleb128 0x6
	.4byte	0x9b
	.byte	0xb
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.byte	0x9
	.byte	0x1d
	.4byte	0x564
	.uleb128 0x8
	.4byte	.LASF85
	.byte	0x9
	.byte	0x20
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF86
	.byte	0x9
	.byte	0x25
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF87
	.byte	0x9
	.byte	0x27
	.4byte	0x53f
	.uleb128 0x7
	.byte	0x8
	.byte	0x9
	.byte	0x29
	.4byte	0x593
	.uleb128 0x8
	.4byte	.LASF88
	.byte	0x9
	.byte	0x2c
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"on\000"
	.byte	0x9
	.byte	0x2f
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF89
	.byte	0x9
	.byte	0x31
	.4byte	0x56f
	.uleb128 0x7
	.byte	0x3c
	.byte	0x9
	.byte	0x3c
	.4byte	0x5ec
	.uleb128 0xd
	.ascii	"sn\000"
	.byte	0x9
	.byte	0x40
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF73
	.byte	0x9
	.byte	0x45
	.4byte	0x37d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x8
	.4byte	.LASF90
	.byte	0x9
	.byte	0x4a
	.4byte	0x297
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x8
	.4byte	.LASF91
	.byte	0x9
	.byte	0x4f
	.4byte	0x33a
	.byte	0x2
	.byte	0x23
	.uleb128 0x25
	.uleb128 0x8
	.4byte	.LASF92
	.byte	0x9
	.byte	0x56
	.4byte	0x51c
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.byte	0
	.uleb128 0x3
	.4byte	.LASF93
	.byte	0x9
	.byte	0x5a
	.4byte	0x59e
	.uleb128 0xf
	.2byte	0x156c
	.byte	0x9
	.byte	0x82
	.4byte	0x817
	.uleb128 0x8
	.4byte	.LASF94
	.byte	0x9
	.byte	0x87
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.4byte	.LASF95
	.byte	0x9
	.byte	0x8e
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x8
	.4byte	.LASF96
	.byte	0x9
	.byte	0x96
	.4byte	0x3de
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x8
	.4byte	.LASF97
	.byte	0x9
	.byte	0x9f
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x8
	.4byte	.LASF98
	.byte	0x9
	.byte	0xa6
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x8
	.4byte	.LASF99
	.byte	0x9
	.byte	0xab
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x8
	.4byte	.LASF100
	.byte	0x9
	.byte	0xad
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x8
	.4byte	.LASF101
	.byte	0x9
	.byte	0xaf
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x8
	.4byte	.LASF102
	.byte	0x9
	.byte	0xb4
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x8
	.4byte	.LASF103
	.byte	0x9
	.byte	0xbb
	.4byte	0x5e
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x8
	.4byte	.LASF104
	.byte	0x9
	.byte	0xbc
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x8
	.4byte	.LASF105
	.byte	0x9
	.byte	0xbd
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x8
	.4byte	.LASF106
	.byte	0x9
	.byte	0xbe
	.4byte	0x90
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x8
	.4byte	.LASF107
	.byte	0x9
	.byte	0xc5
	.4byte	0x564
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x8
	.4byte	.LASF108
	.byte	0x9
	.byte	0xca
	.4byte	0x817
	.byte	0x2
	.byte	0x23
	.uleb128 0x3c
	.uleb128 0x8
	.4byte	.LASF109
	.byte	0x9
	.byte	0xd0
	.4byte	0x52f
	.byte	0x3
	.byte	0x23
	.uleb128 0x9c
	.uleb128 0x8
	.4byte	.LASF110
	.byte	0x9
	.byte	0xda
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0xcc
	.uleb128 0x8
	.4byte	.LASF111
	.byte	0x9
	.byte	0xde
	.4byte	0x421
	.byte	0x3
	.byte	0x23
	.uleb128 0xd0
	.uleb128 0x8
	.4byte	.LASF112
	.byte	0x9
	.byte	0xe2
	.4byte	0x827
	.byte	0x3
	.byte	0x23
	.uleb128 0xdc
	.uleb128 0x8
	.4byte	.LASF113
	.byte	0x9
	.byte	0xe4
	.4byte	0x593
	.byte	0x3
	.byte	0x23
	.uleb128 0x139c
	.uleb128 0x8
	.4byte	.LASF114
	.byte	0x9
	.byte	0xea
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13a4
	.uleb128 0x8
	.4byte	.LASF115
	.byte	0x9
	.byte	0xec
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13a8
	.uleb128 0x8
	.4byte	.LASF116
	.byte	0x9
	.byte	0xee
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13ac
	.uleb128 0x8
	.4byte	.LASF117
	.byte	0x9
	.byte	0xf0
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13b0
	.uleb128 0x8
	.4byte	.LASF118
	.byte	0x9
	.byte	0xf2
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13b4
	.uleb128 0x8
	.4byte	.LASF119
	.byte	0x9
	.byte	0xf7
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13b8
	.uleb128 0x8
	.4byte	.LASF120
	.byte	0x9
	.byte	0xfd
	.4byte	0x446
	.byte	0x3
	.byte	0x23
	.uleb128 0x13bc
	.uleb128 0xb
	.4byte	.LASF121
	.byte	0x9
	.2byte	0x102
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c0
	.uleb128 0xb
	.4byte	.LASF122
	.byte	0x9
	.2byte	0x104
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c4
	.uleb128 0xb
	.4byte	.LASF123
	.byte	0x9
	.2byte	0x106
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13c8
	.uleb128 0xb
	.4byte	.LASF124
	.byte	0x9
	.2byte	0x10b
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13cc
	.uleb128 0xb
	.4byte	.LASF125
	.byte	0x9
	.2byte	0x10d
	.4byte	0x5e
	.byte	0x3
	.byte	0x23
	.uleb128 0x13d0
	.uleb128 0xb
	.4byte	.LASF126
	.byte	0x9
	.2byte	0x116
	.4byte	0x90
	.byte	0x3
	.byte	0x23
	.uleb128 0x13d4
	.uleb128 0xb
	.4byte	.LASF127
	.byte	0x9
	.2byte	0x118
	.4byte	0x3ba
	.byte	0x3
	.byte	0x23
	.uleb128 0x13d8
	.uleb128 0xb
	.4byte	.LASF128
	.byte	0x9
	.2byte	0x11f
	.4byte	0x49c
	.byte	0x3
	.byte	0x23
	.uleb128 0x13e4
	.uleb128 0xb
	.4byte	.LASF129
	.byte	0x9
	.2byte	0x12a
	.4byte	0x837
	.byte	0x3
	.byte	0x23
	.uleb128 0x1464
	.byte	0
	.uleb128 0x5
	.4byte	0x564
	.4byte	0x827
	.uleb128 0x6
	.4byte	0x9b
	.byte	0xb
	.byte	0
	.uleb128 0x5
	.4byte	0x5ec
	.4byte	0x837
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x4f
	.byte	0
	.uleb128 0x5
	.4byte	0x5e
	.4byte	0x847
	.uleb128 0x6
	.4byte	0x9b
	.byte	0x41
	.byte	0
	.uleb128 0xc
	.4byte	.LASF130
	.byte	0x9
	.2byte	0x133
	.4byte	0x5f7
	.uleb128 0x10
	.4byte	.LASF178
	.byte	0x1
	.byte	0x35
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x87a
	.uleb128 0x11
	.4byte	.LASF131
	.byte	0x1
	.byte	0x35
	.4byte	0x87a
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0x12
	.4byte	0x5e
	.uleb128 0x13
	.byte	0x1
	.4byte	.LASF179
	.byte	0x1
	.byte	0x65
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF134
	.byte	0x1
	.byte	0x7c
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x8d6
	.uleb128 0x11
	.4byte	.LASF132
	.byte	0x1
	.byte	0x7c
	.4byte	0x8d6
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.4byte	.LASF133
	.byte	0x1
	.byte	0x7e
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x16
	.ascii	"i\000"
	.byte	0x1
	.byte	0x80
	.4byte	0x5e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x12
	.4byte	0x90
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF135
	.byte	0x1
	.byte	0xb2
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x911
	.uleb128 0x11
	.4byte	.LASF136
	.byte	0x1
	.byte	0xb2
	.4byte	0x911
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x15
	.4byte	.LASF137
	.byte	0x1
	.byte	0xb4
	.4byte	0x70
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x12
	.4byte	0xf4
	.uleb128 0x17
	.4byte	.LASF138
	.byte	0xa
	.2byte	0x2ec
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF139
	.byte	0xa
	.2byte	0x47a
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF140
	.byte	0xa
	.2byte	0x486
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF141
	.byte	0xa
	.2byte	0x495
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF142
	.byte	0xa
	.2byte	0x496
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF143
	.byte	0xa
	.2byte	0x497
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF144
	.byte	0xa
	.2byte	0x498
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF145
	.byte	0xa
	.2byte	0x499
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF146
	.byte	0xa
	.2byte	0x49a
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF147
	.byte	0xa
	.2byte	0x49b
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF148
	.byte	0xa
	.2byte	0x49c
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF149
	.byte	0xa
	.2byte	0x49d
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF150
	.byte	0xa
	.2byte	0x49e
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF151
	.byte	0xa
	.2byte	0x49f
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF152
	.byte	0xa
	.2byte	0x4a0
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF153
	.byte	0xa
	.2byte	0x4a1
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF154
	.byte	0xa
	.2byte	0x4a2
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF155
	.byte	0xa
	.2byte	0x4a3
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF156
	.byte	0xa
	.2byte	0x4a4
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF157
	.byte	0xa
	.2byte	0x4a5
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF158
	.byte	0xa
	.2byte	0x4a6
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF159
	.byte	0xa
	.2byte	0x4a7
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF160
	.byte	0xa
	.2byte	0x4a8
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF161
	.byte	0xa
	.2byte	0x4a9
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF162
	.byte	0xa
	.2byte	0x4aa
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF163
	.byte	0xa
	.2byte	0x4ab
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF164
	.byte	0xa
	.2byte	0x4ac
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF165
	.byte	0xa
	.2byte	0x4ad
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF166
	.byte	0xa
	.2byte	0x4ae
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF167
	.byte	0xa
	.2byte	0x4af
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF168
	.byte	0xb
	.2byte	0x127
	.4byte	0x57
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF169
	.byte	0xc
	.byte	0x30
	.4byte	0xad9
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x12
	.4byte	0xbf
	.uleb128 0x15
	.4byte	.LASF170
	.byte	0xc
	.byte	0x34
	.4byte	0xaef
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x12
	.4byte	0xbf
	.uleb128 0x15
	.4byte	.LASF171
	.byte	0xc
	.byte	0x36
	.4byte	0xb05
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x12
	.4byte	0xbf
	.uleb128 0x15
	.4byte	.LASF172
	.byte	0xc
	.byte	0x38
	.4byte	0xb1b
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x12
	.4byte	0xbf
	.uleb128 0x17
	.4byte	.LASF173
	.byte	0x9
	.2byte	0x138
	.4byte	0x847
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	.LASF174
	.byte	0x1
	.byte	0x20
	.4byte	0x5e
	.byte	0x5
	.byte	0x3
	.4byte	g_TWO_WIRE_list_item_index
	.uleb128 0x17
	.4byte	.LASF138
	.byte	0xa
	.2byte	0x2ec
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF139
	.byte	0xa
	.2byte	0x47a
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF140
	.byte	0xa
	.2byte	0x486
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF141
	.byte	0xa
	.2byte	0x495
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF142
	.byte	0xa
	.2byte	0x496
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF143
	.byte	0xa
	.2byte	0x497
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF144
	.byte	0xa
	.2byte	0x498
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF145
	.byte	0xa
	.2byte	0x499
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF146
	.byte	0xa
	.2byte	0x49a
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF147
	.byte	0xa
	.2byte	0x49b
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF148
	.byte	0xa
	.2byte	0x49c
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF149
	.byte	0xa
	.2byte	0x49d
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF150
	.byte	0xa
	.2byte	0x49e
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF151
	.byte	0xa
	.2byte	0x49f
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF152
	.byte	0xa
	.2byte	0x4a0
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF153
	.byte	0xa
	.2byte	0x4a1
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF154
	.byte	0xa
	.2byte	0x4a2
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF155
	.byte	0xa
	.2byte	0x4a3
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF156
	.byte	0xa
	.2byte	0x4a4
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF157
	.byte	0xa
	.2byte	0x4a5
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF158
	.byte	0xa
	.2byte	0x4a6
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF159
	.byte	0xa
	.2byte	0x4a7
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF160
	.byte	0xa
	.2byte	0x4a8
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF161
	.byte	0xa
	.2byte	0x4a9
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF162
	.byte	0xa
	.2byte	0x4aa
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF163
	.byte	0xa
	.2byte	0x4ab
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF164
	.byte	0xa
	.2byte	0x4ac
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF165
	.byte	0xa
	.2byte	0x4ad
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF166
	.byte	0xa
	.2byte	0x4ae
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF167
	.byte	0xa
	.2byte	0x4af
	.4byte	0x69
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF168
	.byte	0xb
	.2byte	0x127
	.4byte	0x57
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF173
	.byte	0x9
	.2byte	0x138
	.4byte	0x847
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF71:
	.ascii	"TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT\000"
.LASF92:
	.ascii	"comm_stats\000"
.LASF99:
	.ascii	"et_gage_runaway_gage_in_effect_to_send_to_master\000"
.LASF127:
	.ascii	"two_wire_solenoid_location_on_off_command\000"
.LASF85:
	.ascii	"measured_ma_current\000"
.LASF171:
	.ascii	"GuiFont_DecimalChar\000"
.LASF148:
	.ascii	"GuiVar_TwoWireStatsRxDataLpbkMsgs\000"
.LASF81:
	.ascii	"loop_data_bytes_sent\000"
.LASF152:
	.ascii	"GuiVar_TwoWireStatsRxEnqMsgs\000"
.LASF162:
	.ascii	"GuiVar_TwoWireStatsS1UCos\000"
.LASF45:
	.ascii	"rx_sol_ctl_msgs\000"
.LASF58:
	.ascii	"decoder_type__tpmicro_type\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF165:
	.ascii	"GuiVar_TwoWireStatsTempMax\000"
.LASF134:
	.ascii	"FDTO_TWO_WIRE_draw_decoder_stats_report\000"
.LASF137:
	.ascii	"lprev_decoder_index\000"
.LASF113:
	.ascii	"two_wire_cable_power_operation\000"
.LASF149:
	.ascii	"GuiVar_TwoWireStatsRxDecRstMsgs\000"
.LASF102:
	.ascii	"rain_bucket_pulse_count_to_send_to_the_master\000"
.LASF17:
	.ascii	"keycode\000"
.LASF42:
	.ascii	"rx_sol_cur_meas_req_msgs\000"
.LASF161:
	.ascii	"GuiVar_TwoWireStatsRxStatsReqMsgs\000"
.LASF26:
	.ascii	"sol_2_ucos\000"
.LASF11:
	.ascii	"long long int\000"
.LASF2:
	.ascii	"signed char\000"
.LASF154:
	.ascii	"GuiVar_TwoWireStatsRxIDReqMsgs\000"
.LASF128:
	.ascii	"decoder_faults\000"
.LASF13:
	.ascii	"long unsigned int\000"
.LASF107:
	.ascii	"as_rcvd_from_tp_micro\000"
.LASF91:
	.ascii	"stat2_response\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF5:
	.ascii	"INT_16\000"
.LASF103:
	.ascii	"nlu_wind_mph\000"
.LASF36:
	.ascii	"rx_disc_conf_msgs\000"
.LASF97:
	.ascii	"whats_installed_has_arrived_from_the_tpmicro\000"
.LASF29:
	.ascii	"eep_crc_err_sol2_parms\000"
.LASF14:
	.ascii	"long int\000"
.LASF151:
	.ascii	"GuiVar_TwoWireStatsRxDiscRstMsgs\000"
.LASF19:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF142:
	.ascii	"GuiVar_TwoWireStatsEEPCRCComParams\000"
.LASF136:
	.ascii	"pkey_event\000"
.LASF6:
	.ascii	"short int\000"
.LASF23:
	.ascii	"wdt_resets\000"
.LASF39:
	.ascii	"rx_data_lpbk_msgs\000"
.LASF16:
	.ascii	"uint16_t\000"
.LASF50:
	.ascii	"duty_cycle_acc\000"
.LASF68:
	.ascii	"station_or_light_number_0\000"
.LASF44:
	.ascii	"rx_stats_req_msgs\000"
.LASF145:
	.ascii	"GuiVar_TwoWireStatsEEPCRCS2Params\000"
.LASF179:
	.ascii	"FDTO_TWO_WIRE_update_decoder_stats\000"
.LASF172:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF155:
	.ascii	"GuiVar_TwoWireStatsRxLongMsgs\000"
.LASF79:
	.ascii	"unicast_response_crc_errs\000"
.LASF76:
	.ascii	"DECODER_FAULTS_ARRAY_TYPE\000"
.LASF129:
	.ascii	"filler\000"
.LASF27:
	.ascii	"eep_crc_err_com_parms\000"
.LASF94:
	.ascii	"send_wind_settings_structure_to_the_tpmicro\000"
.LASF34:
	.ascii	"rx_disc_rst_msgs\000"
.LASF72:
	.ascii	"fault_type_code\000"
.LASF8:
	.ascii	"unsigned int\000"
.LASF138:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF30:
	.ascii	"eep_crc_err_stats\000"
.LASF130:
	.ascii	"TPMICRO_DATA_STRUCT\000"
.LASF167:
	.ascii	"GuiVar_TwoWireStatsWDTs\000"
.LASF75:
	.ascii	"afflicted_output\000"
.LASF9:
	.ascii	"INT_32\000"
.LASF62:
	.ascii	"output\000"
.LASF116:
	.ascii	"two_wire_request_statistics_from_all_decoders\000"
.LASF117:
	.ascii	"two_wire_start_all_decoder_loopback_test\000"
.LASF57:
	.ascii	"STAT2_REQ_RSP_s\000"
.LASF40:
	.ascii	"rx_put_parms_msgs\000"
.LASF88:
	.ascii	"send_command\000"
.LASF74:
	.ascii	"decoder_sn\000"
.LASF163:
	.ascii	"GuiVar_TwoWireStatsS2UCos\000"
.LASF95:
	.ascii	"request_whats_installed_from_tp_micro\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF175:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF55:
	.ascii	"sol2_status\000"
.LASF143:
	.ascii	"GuiVar_TwoWireStatsEEPCRCErrStats\000"
.LASF178:
	.ascii	"TWO_WIRE_copy_decoder_stats_into_guivars\000"
.LASF173:
	.ascii	"tpmicro_data\000"
.LASF111:
	.ascii	"terminal_short_or_no_current_report\000"
.LASF48:
	.ascii	"seqnum\000"
.LASF56:
	.ascii	"sys_flags\000"
.LASF156:
	.ascii	"GuiVar_TwoWireStatsRxMsgs\000"
.LASF31:
	.ascii	"rx_msgs\000"
.LASF65:
	.ascii	"ERROR_LOG_s\000"
.LASF59:
	.ascii	"decoder_subtype\000"
.LASF141:
	.ascii	"GuiVar_TwoWireStatsBODs\000"
.LASF89:
	.ascii	"TWO_WIRE_CABLE_POWER_OPERATION_STRUCT\000"
.LASF170:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF112:
	.ascii	"decoder_info\000"
.LASF101:
	.ascii	"et_gage_clear_runaway_gage_being_processed\000"
.LASF166:
	.ascii	"GuiVar_TwoWireStatsTxAcksSent\000"
.LASF53:
	.ascii	"sol2_cur_s\000"
.LASF49:
	.ascii	"dv_adc_cnts\000"
.LASF63:
	.ascii	"TWO_WIRE_DECODER_SOLENOID_OPERATION_STRUCT\000"
.LASF51:
	.ascii	"SOL_CUR_MEAS_s\000"
.LASF169:
	.ascii	"GuiFont_LanguageActive\000"
.LASF147:
	.ascii	"GuiVar_TwoWireStatsRxCRCErrs\000"
.LASF7:
	.ascii	"UNS_32\000"
.LASF123:
	.ascii	"two_wire_cable_cooled_off_xmission_state\000"
.LASF78:
	.ascii	"unicast_no_replies\000"
.LASF38:
	.ascii	"rx_dec_rst_msgs\000"
.LASF87:
	.ascii	"MEAS_MA_FOR_DISTRIBUTION\000"
.LASF106:
	.ascii	"nlu_fuse_blown\000"
.LASF164:
	.ascii	"GuiVar_TwoWireStatsTempCur\000"
.LASF109:
	.ascii	"measured_ma_current_as_rcvd_from_master\000"
.LASF110:
	.ascii	"terminal_short_or_no_current_state\000"
.LASF158:
	.ascii	"GuiVar_TwoWireStatsRxSolCtrlMsgs\000"
.LASF37:
	.ascii	"rx_id_req_msgs\000"
.LASF135:
	.ascii	"TWO_WIRE_process_decoder_stats_report\000"
.LASF105:
	.ascii	"nlu_freeze_switch_active\000"
.LASF140:
	.ascii	"GuiVar_TwoWireNumDiscoveredStaDecoders\000"
.LASF119:
	.ascii	"decoders_discovered_so_far\000"
.LASF84:
	.ascii	"float\000"
.LASF157:
	.ascii	"GuiVar_TwoWireStatsRxPutParamsMsgs\000"
.LASF69:
	.ascii	"TERMINAL_SHORT_OR_NO_CURRENT_STRUCT\000"
.LASF24:
	.ascii	"bod_resets\000"
.LASF174:
	.ascii	"g_TWO_WIRE_list_item_index\000"
.LASF133:
	.ascii	"lcursor_to_show\000"
.LASF25:
	.ascii	"sol_1_ucos\000"
.LASF139:
	.ascii	"GuiVar_TwoWireDecoderSN\000"
.LASF83:
	.ascii	"TWO_WIRE_COMM_STATS_PER_DECODER_STRUCT\000"
.LASF28:
	.ascii	"eep_crc_err_sol1_parms\000"
.LASF32:
	.ascii	"rx_long_msgs\000"
.LASF33:
	.ascii	"rx_crc_errs\000"
.LASF114:
	.ascii	"two_wire_perform_discovery\000"
.LASF54:
	.ascii	"sol1_status\000"
.LASF77:
	.ascii	"unicast_msgs_sent\000"
.LASF100:
	.ascii	"et_gage_clear_runaway_gage\000"
.LASF121:
	.ascii	"two_wire_cable_excessive_current_xmission_state\000"
.LASF46:
	.ascii	"tx_acks_sent\000"
.LASF73:
	.ascii	"id_info\000"
.LASF67:
	.ascii	"terminal_type\000"
.LASF159:
	.ascii	"GuiVar_TwoWireStatsRxSolCurMeasReqMsgs\000"
.LASF35:
	.ascii	"rx_enq_msgs\000"
.LASF93:
	.ascii	"CS3000_DECODER_INFO_STRUCT\000"
.LASF43:
	.ascii	"rx_stat_req_msgs\000"
.LASF144:
	.ascii	"GuiVar_TwoWireStatsEEPCRCS1Params\000"
.LASF86:
	.ascii	"current_needs_to_be_sent\000"
.LASF22:
	.ascii	"por_resets\000"
.LASF132:
	.ascii	"pcomplete_redraw\000"
.LASF64:
	.ascii	"errorBitField\000"
.LASF0:
	.ascii	"char\000"
.LASF21:
	.ascii	"temp_current\000"
.LASF115:
	.ascii	"two_wire_clear_statistics_at_all_decoders\000"
.LASF122:
	.ascii	"two_wire_cable_over_heated_xmission_state\000"
.LASF98:
	.ascii	"et_gage_pulse_count_to_send_to_the_master\000"
.LASF125:
	.ascii	"sn_to_set\000"
.LASF131:
	.ascii	"pdecoder_index_0\000"
.LASF118:
	.ascii	"two_wire_stop_all_decoder_loopback_test\000"
.LASF60:
	.ascii	"fw_vers\000"
.LASF61:
	.ascii	"ID_REQ_RESP_s\000"
.LASF18:
	.ascii	"repeats\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF47:
	.ascii	"DECODER_STATS_s\000"
.LASF96:
	.ascii	"rcvd_errors\000"
.LASF146:
	.ascii	"GuiVar_TwoWireStatsPORs\000"
.LASF90:
	.ascii	"decoder_statistics\000"
.LASF160:
	.ascii	"GuiVar_TwoWireStatsRxStatReqMsgs\000"
.LASF168:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF4:
	.ascii	"UNS_16\000"
.LASF20:
	.ascii	"temp_maximum\000"
.LASF104:
	.ascii	"nlu_rain_switch_active\000"
.LASF177:
	.ascii	"DECODER_FAULT_BASE_STRUCT\000"
.LASF15:
	.ascii	"uint8_t\000"
.LASF120:
	.ascii	"twccm\000"
.LASF70:
	.ascii	"current_percentage_of_max\000"
.LASF41:
	.ascii	"rx_get_parms_msgs\000"
.LASF126:
	.ascii	"send_2w_solenoid_location_on_off_command\000"
.LASF82:
	.ascii	"loop_data_bytes_recd\000"
.LASF153:
	.ascii	"GuiVar_TwoWireStatsRxGetParamsMsgs\000"
.LASF80:
	.ascii	"unicast_response_length_errs\000"
.LASF124:
	.ascii	"two_wire_set_decoder_sn\000"
.LASF52:
	.ascii	"sol1_cur_s\000"
.LASF108:
	.ascii	"as_rcvd_from_slaves\000"
.LASF66:
	.ascii	"result\000"
.LASF150:
	.ascii	"GuiVar_TwoWireStatsRxDiscConfMsgs\000"
.LASF176:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_decoder_stats.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
