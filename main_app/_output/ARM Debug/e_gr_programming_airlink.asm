	.file	"e_gr_programming_airlink.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.bss.g_GR_PROGRAMMING_AIRLINK_port,"aw",%nobits
	.align	2
	.type	g_GR_PROGRAMMING_AIRLINK_port, %object
	.size	g_GR_PROGRAMMING_AIRLINK_port, 4
g_GR_PROGRAMMING_AIRLINK_port:
	.space	4
	.section	.bss.GR_PROGRAMMING_AIRLINK_querying_device,"aw",%nobits
	.align	2
	.type	GR_PROGRAMMING_AIRLINK_querying_device, %object
	.size	GR_PROGRAMMING_AIRLINK_querying_device, 4
GR_PROGRAMMING_AIRLINK_querying_device:
	.space	4
	.section	.text.start_gr_device_inquiry,"ax",%progbits
	.align	2
	.type	start_gr_device_inquiry, %function
start_gr_device_inquiry:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_gr_programming_airlink.c"
	.loc 1 49 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #44
.LCFI2:
	str	r0, [fp, #-48]
	.loc 1 50 0
	ldr	r3, .L2
	mov	r2, #1
	str	r2, [r3, #0]
	.loc 1 57 0
	mov	r3, #4352
	str	r3, [fp, #-44]
	.loc 1 59 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-12]
	.loc 1 61 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	COMM_MNGR_post_event_with_details
	.loc 1 62 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L3:
	.align	2
.L2:
	.word	GR_PROGRAMMING_AIRLINK_querying_device
.LFE0:
	.size	start_gr_device_inquiry, .-start_gr_device_inquiry
	.section .rodata
	.align	2
.LC0:
	.ascii	"Sierra Wireless AirLink\000"
	.align	2
.LC1:
	.ascii	"-----\000"
	.align	2
.LC2:
	.ascii	"-----------\000"
	.align	2
.LC3:
	.ascii	"---.---.---.---\000"
	.align	2
.LC4:
	.ascii	"--------------------\000"
	.align	2
.LC5:
	.ascii	"-------------\000"
	.align	2
.LC6:
	.ascii	"----\000"
	.align	2
.LC7:
	.ascii	"---------------\000"
	.align	2
.LC8:
	.ascii	"---------\000"
	.section	.text.GR_PROGRAMMING_AIRLINK_initialize_guivars,"ax",%progbits
	.align	2
	.type	GR_PROGRAMMING_AIRLINK_initialize_guivars, %function
GR_PROGRAMMING_AIRLINK_initialize_guivars:
.LFB1:
	.loc 1 66 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	.loc 1 68 0
	ldr	r3, .L5
	ldr	r2, .L5+4
	str	r2, [r3, #0]
	.loc 1 73 0
	ldr	r0, .L5+8
	ldr	r1, .L5+12
	mov	r2, #49
	bl	strlcpy
	.loc 1 76 0
	ldr	r0, .L5+16
	ldr	r1, .L5+20
	mov	r2, #49
	bl	strlcpy
	.loc 1 82 0
	ldr	r0, .L5+24
	ldr	r1, .L5+28
	mov	r2, #49
	bl	strlcpy
	.loc 1 85 0
	ldr	r0, .L5+32
	ldr	r1, .L5+36
	mov	r2, #49
	bl	strlcpy
	.loc 1 88 0
	ldr	r0, .L5+40
	ldr	r1, .L5+44
	mov	r2, #49
	bl	strlcpy
	.loc 1 94 0
	ldr	r0, .L5+48
	ldr	r1, .L5+52
	mov	r2, #49
	bl	strlcpy
	.loc 1 97 0
	ldr	r0, .L5+56
	ldr	r1, .L5+60
	mov	r2, #49
	bl	strlcpy
	.loc 1 100 0
	ldr	r0, .L5+64
	ldr	r1, .L5+60
	mov	r2, #49
	bl	strlcpy
	.loc 1 103 0
	ldr	r0, .L5+68
	ldr	r1, .L5+60
	mov	r2, #49
	bl	strlcpy
	.loc 1 106 0
	ldr	r0, .L5+72
	ldr	r1, .L5+60
	mov	r2, #49
	bl	strlcpy
	.loc 1 112 0
	ldr	r0, .L5+76
	ldr	r1, .L5+80
	mov	r2, #49
	bl	strlcpy
	.loc 1 115 0
	ldr	r0, .L5+84
	ldr	r1, .L5+88
	mov	r2, #49
	bl	strlcpy
	.loc 1 116 0
	ldmfd	sp!, {fp, pc}
.L6:
	.align	2
.L5:
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36865
	.word	GuiVar_GRMake
	.word	.LC0
	.word	GuiVar_GRModel
	.word	.LC1
	.word	GuiVar_GRPhoneNumber
	.word	.LC2
	.word	GuiVar_GRIPAddress
	.word	.LC3
	.word	GuiVar_GRSIMID
	.word	.LC4
	.word	GuiVar_GRNetworkState
	.word	.LC5
	.word	GuiVar_GRCarrier
	.word	.LC6
	.word	GuiVar_GRService
	.word	GuiVar_GRRSSI
	.word	GuiVar_GRECIO
	.word	GuiVar_GRRadioSerialNum
	.word	.LC7
	.word	GuiVar_GRFirmwareVersion
	.word	.LC8
.LFE1:
	.size	GR_PROGRAMMING_AIRLINK_initialize_guivars, .-GR_PROGRAMMING_AIRLINK_initialize_guivars
	.section	.text.FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key, %function
FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key:
.LFB2:
	.loc 1 120 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI5:
	add	fp, sp, #4
.LCFI6:
	sub	sp, sp, #4
.LCFI7:
	str	r0, [fp, #-8]
	.loc 1 121 0
	ldr	r3, [fp, #-8]
	sub	r3, r3, #36864
	cmp	r3, #6
	bhi	.L7
	mov	r2, #1
	mov	r3, r2, asl r3
	and	r2, r3, #109
	cmp	r2, #0
	bne	.L9
	and	r3, r3, #18
	cmp	r3, #0
	beq	.L7
.L10:
	.loc 1 128 0
	ldr	r0, .L11
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L11+4
	str	r2, [r3, #0]
	.loc 1 131 0
	bl	DEVICE_EXCHANGE_draw_dialog
	.loc 1 132 0
	b	.L7
.L9:
	.loc 1 140 0
	ldr	r0, [fp, #-8]
	bl	e_SHARED_index_keycode_for_gui
	mov	r2, r0
	ldr	r3, .L11+4
	str	r2, [r3, #0]
	.loc 1 143 0
	bl	DIALOG_close_ok_dialog
	.loc 1 145 0
	ldr	r3, .L11+8
	ldr	r3, [r3, #0]
	mov	r0, #0
	mov	r1, r3
	bl	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen
	.loc 1 146 0
	mov	r0, r0	@ nop
.L7:
	.loc 1 148 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L12:
	.align	2
.L11:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	g_GR_PROGRAMMING_AIRLINK_port
.LFE2:
	.size	FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key, .-FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key
	.section	.text.FDTO_GR_PROGRAMMING_AIRLINK_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen
	.type	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen, %function
FDTO_GR_PROGRAMMING_AIRLINK_draw_screen:
.LFB3:
	.loc 1 152 0
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI8:
	add	fp, sp, #4
.LCFI9:
	sub	sp, sp, #12
.LCFI10:
	str	r0, [fp, #-12]
	str	r1, [fp, #-16]
	.loc 1 155 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L14
	.loc 1 157 0
	bl	GR_PROGRAMMING_AIRLINK_initialize_guivars
	.loc 1 159 0
	ldr	r3, .L17
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	.loc 1 161 0
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L15
.L14:
	.loc 1 165 0
	ldr	r3, .L17+4
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [fp, #-8]
.L15:
	.loc 1 168 0
	ldr	r3, [fp, #-8]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r0, #23
	mov	r1, r3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 169 0
	bl	GuiLib_Refresh
	.loc 1 171 0
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L13
	.loc 1 175 0
	bl	DEVICE_EXCHANGE_draw_dialog
	.loc 1 180 0
	ldr	r0, [fp, #-16]
	bl	start_gr_device_inquiry
.L13:
	.loc 1 182 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L18:
	.align	2
.L17:
	.word	g_GR_PROGRAMMING_AIRLINK_port
	.word	GuiLib_ActiveCursorFieldNo
.LFE3:
	.size	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen, .-FDTO_GR_PROGRAMMING_AIRLINK_draw_screen
	.section	.text.GR_PROGRAMMING_AIRLINK_process_screen,"ax",%progbits
	.align	2
	.global	GR_PROGRAMMING_AIRLINK_process_screen
	.type	GR_PROGRAMMING_AIRLINK_process_screen, %function
GR_PROGRAMMING_AIRLINK_process_screen:
.LFB4:
	.loc 1 186 0
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI11:
	add	fp, sp, #4
.LCFI12:
	sub	sp, sp, #44
.LCFI13:
	str	r0, [fp, #-48]
	str	r1, [fp, #-44]
	.loc 1 189 0
	ldr	r3, [fp, #-48]
	cmp	r3, #67
	beq	.L22
	cmp	r3, #67
	bhi	.L24
	cmp	r3, #2
	beq	.L21
	b	.L20
.L24:
	sub	r3, r3, #36864
	cmp	r3, #6
	bhi	.L20
	.loc 1 199 0
	ldr	r2, [fp, #-48]
	ldr	r3, .L33
	cmp	r2, r3
	beq	.L25
	.loc 1 199 0 is_stmt 0 discriminator 1
	ldr	r2, [fp, #-48]
	ldr	r3, .L33+4
	cmp	r2, r3
	beq	.L25
	.loc 1 201 0 is_stmt 1
	ldr	r3, .L33+8
	mov	r2, #0
	str	r2, [r3, #0]
.L25:
	.loc 1 206 0
	mov	r3, #2
	str	r3, [fp, #-40]
	.loc 1 207 0
	ldr	r3, .L33+12
	str	r3, [fp, #-20]
	.loc 1 208 0
	ldr	r3, [fp, #-48]
	str	r3, [fp, #-16]
	.loc 1 209 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 210 0
	b	.L19
.L21:
	.loc 1 213 0
	ldr	r3, .L33+16
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bne	.L32
.L28:
	.loc 1 216 0
	ldr	r3, .L33+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L29
	.loc 1 218 0
	bl	good_key_beep
	.loc 1 219 0
	mov	r3, #3
	str	r3, [fp, #-40]
	.loc 1 220 0
	ldr	r3, .L33+20
	str	r3, [fp, #-20]
	.loc 1 221 0
	mov	r3, #1
	str	r3, [fp, #-16]
	.loc 1 222 0
	ldr	r3, .L33+24
	ldr	r3, [r3, #0]
	str	r3, [fp, #-12]
	.loc 1 223 0
	sub	r3, fp, #40
	mov	r0, r3
	bl	Display_Post_Command
	.loc 1 229 0
	b	.L31
.L29:
	.loc 1 227 0
	bl	bad_key_beep
	.loc 1 229 0
	b	.L31
.L32:
	.loc 1 232 0
	bl	bad_key_beep
	.loc 1 234 0
	b	.L19
.L31:
	b	.L19
.L22:
	.loc 1 237 0
	ldr	r3, .L33+28
	mov	r2, #11
	str	r2, [r3, #0]
	.loc 1 239 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 243 0
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	.loc 1 244 0
	b	.L19
.L20:
	.loc 1 247 0
	sub	r1, fp, #48
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
.L19:
	.loc 1 249 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L34:
	.align	2
.L33:
	.word	36867
	.word	36870
	.word	GR_PROGRAMMING_AIRLINK_querying_device
	.word	FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen
	.word	g_GR_PROGRAMMING_AIRLINK_port
	.word	GuiVar_MenuScreenToShow
.LFE4:
	.size	GR_PROGRAMMING_AIRLINK_process_screen, .-GR_PROGRAMMING_AIRLINK_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI11-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/packet_definitions.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/general_picked_support.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/comm_mngr.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5c9
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF72
	.byte	0x1
	.4byte	.LASF73
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x3a
	.4byte	0x3e
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x70
	.uleb128 0x5
	.byte	0x4
	.4byte	0x9d
	.uleb128 0x6
	.4byte	0xa4
	.uleb128 0x7
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x8
	.4byte	0x3e
	.4byte	0xbb
	.uleb128 0x9
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xa
	.byte	0x6
	.byte	0x3
	.byte	0x3c
	.4byte	0xdf
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x3e
	.4byte	0xdf
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.ascii	"to\000"
	.byte	0x3
	.byte	0x40
	.4byte	0xdf
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.byte	0
	.uleb128 0x8
	.4byte	0x33
	.4byte	0xef
	.uleb128 0x9
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x42
	.4byte	0xbb
	.uleb128 0xa
	.byte	0x8
	.byte	0x4
	.byte	0x14
	.4byte	0x11f
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x4
	.byte	0x17
	.4byte	0x11f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x4
	.byte	0x1a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x33
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x4
	.byte	0x1c
	.4byte	0xfa
	.uleb128 0xa
	.byte	0x8
	.byte	0x5
	.byte	0x7c
	.4byte	0x155
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x5
	.byte	0x7e
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x5
	.byte	0x80
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x5
	.byte	0x82
	.4byte	0x130
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF22
	.uleb128 0xa
	.byte	0x28
	.byte	0x6
	.byte	0x74
	.4byte	0x1df
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x6
	.byte	0x77
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x6
	.byte	0x7a
	.4byte	0xef
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x6
	.byte	0x7d
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.ascii	"dh\000"
	.byte	0x6
	.byte	0x81
	.4byte	0x125
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x6
	.byte	0x85
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x6
	.byte	0x87
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x6
	.byte	0x8a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xb
	.4byte	.LASF29
	.byte	0x6
	.byte	0x8c
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x3
	.4byte	.LASF30
	.byte	0x6
	.byte	0x8e
	.4byte	0x167
	.uleb128 0xd
	.4byte	0x65
	.uleb128 0xa
	.byte	0x24
	.byte	0x7
	.byte	0x78
	.4byte	0x276
	.uleb128 0xb
	.4byte	.LASF31
	.byte	0x7
	.byte	0x7b
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x7
	.byte	0x83
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.4byte	.LASF33
	.byte	0x7
	.byte	0x86
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.4byte	.LASF34
	.byte	0x7
	.byte	0x88
	.4byte	0x287
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xb
	.4byte	.LASF35
	.byte	0x7
	.byte	0x8d
	.4byte	0x299
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.4byte	.LASF36
	.byte	0x7
	.byte	0x92
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xb
	.4byte	.LASF37
	.byte	0x7
	.byte	0x96
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.4byte	.LASF38
	.byte	0x7
	.byte	0x9a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xb
	.4byte	.LASF39
	.byte	0x7
	.byte	0x9c
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	0x282
	.uleb128 0xf
	.4byte	0x282
	.byte	0
	.uleb128 0xd
	.4byte	0x53
	.uleb128 0x5
	.byte	0x4
	.4byte	0x276
	.uleb128 0xe
	.byte	0x1
	.4byte	0x299
	.uleb128 0xf
	.4byte	0x155
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x28d
	.uleb128 0x3
	.4byte	.LASF40
	.byte	0x7
	.byte	0x9e
	.4byte	0x1ef
	.uleb128 0x10
	.4byte	.LASF41
	.byte	0x1
	.byte	0x30
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x2df
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x1
	.byte	0x30
	.4byte	0x1ea
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x12
	.4byte	.LASF46
	.byte	0x1
	.byte	0x37
	.4byte	0x1df
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x13
	.4byte	.LASF74
	.byte	0x1
	.byte	0x41
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x10
	.4byte	.LASF42
	.byte	0x1
	.byte	0x77
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x31a
	.uleb128 0x11
	.4byte	.LASF44
	.byte	0x1
	.byte	0x77
	.4byte	0x1ea
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF48
	.byte	0x1
	.byte	0x97
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x35e
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x1
	.byte	0x97
	.4byte	0x35e
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x1
	.byte	0x97
	.4byte	0x1ea
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x12
	.4byte	.LASF47
	.byte	0x1
	.byte	0x99
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xd
	.4byte	0x8c
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF49
	.byte	0x1
	.byte	0xb9
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x399
	.uleb128 0x11
	.4byte	.LASF50
	.byte	0x1
	.byte	0xb9
	.4byte	0x399
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x15
	.ascii	"lde\000"
	.byte	0x1
	.byte	0xbb
	.4byte	0x29f
	.byte	0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0xd
	.4byte	0x155
	.uleb128 0x16
	.4byte	.LASF51
	.byte	0x8
	.2byte	0x16c
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x8
	.4byte	0x2c
	.4byte	0x3bc
	.uleb128 0x9
	.4byte	0x25
	.byte	0x30
	.byte	0
	.uleb128 0x16
	.4byte	.LASF52
	.byte	0x8
	.2byte	0x1f4
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF53
	.byte	0x8
	.2byte	0x1f5
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF54
	.byte	0x8
	.2byte	0x1f6
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x1f8
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF56
	.byte	0x8
	.2byte	0x1f9
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF57
	.byte	0x8
	.2byte	0x1fa
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF58
	.byte	0x8
	.2byte	0x1fb
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF59
	.byte	0x8
	.2byte	0x22f
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF60
	.byte	0x8
	.2byte	0x230
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF61
	.byte	0x8
	.2byte	0x231
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF62
	.byte	0x8
	.2byte	0x232
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF63
	.byte	0x8
	.2byte	0x233
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF64
	.byte	0x8
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF65
	.byte	0x9
	.2byte	0x127
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.uleb128 0x12
	.4byte	.LASF66
	.byte	0xa
	.byte	0x30
	.4byte	0x491
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0xd
	.4byte	0xab
	.uleb128 0x12
	.4byte	.LASF67
	.byte	0xa
	.byte	0x34
	.4byte	0x4a7
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0xd
	.4byte	0xab
	.uleb128 0x12
	.4byte	.LASF68
	.byte	0xa
	.byte	0x36
	.4byte	0x4bd
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0xd
	.4byte	0xab
	.uleb128 0x12
	.4byte	.LASF69
	.byte	0xa
	.byte	0x38
	.4byte	0x4d3
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0xd
	.4byte	0xab
	.uleb128 0x12
	.4byte	.LASF70
	.byte	0x1
	.byte	0x28
	.4byte	0x65
	.byte	0x5
	.byte	0x3
	.4byte	g_GR_PROGRAMMING_AIRLINK_port
	.uleb128 0x12
	.4byte	.LASF71
	.byte	0x1
	.byte	0x2a
	.4byte	0x8c
	.byte	0x5
	.byte	0x3
	.4byte	GR_PROGRAMMING_AIRLINK_querying_device
	.uleb128 0x16
	.4byte	.LASF51
	.byte	0x8
	.2byte	0x16c
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF52
	.byte	0x8
	.2byte	0x1f4
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF53
	.byte	0x8
	.2byte	0x1f5
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF54
	.byte	0x8
	.2byte	0x1f6
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x1f8
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF56
	.byte	0x8
	.2byte	0x1f9
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF57
	.byte	0x8
	.2byte	0x1fa
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF58
	.byte	0x8
	.2byte	0x1fb
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF59
	.byte	0x8
	.2byte	0x22f
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF60
	.byte	0x8
	.2byte	0x230
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF61
	.byte	0x8
	.2byte	0x231
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF62
	.byte	0x8
	.2byte	0x232
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF63
	.byte	0x8
	.2byte	0x233
	.4byte	0x3ac
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF64
	.byte	0x8
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x16
	.4byte	.LASF65
	.byte	0x9
	.2byte	0x127
	.4byte	0x5e
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF72:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF62:
	.ascii	"GuiVar_GRService\000"
.LASF16:
	.ascii	"dptr\000"
.LASF61:
	.ascii	"GuiVar_GRRSSI\000"
.LASF15:
	.ascii	"ADDR_TYPE\000"
.LASF7:
	.ascii	"short int\000"
.LASF31:
	.ascii	"_01_command\000"
.LASF23:
	.ascii	"event\000"
.LASF64:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF51:
	.ascii	"GuiVar_CommOptionDeviceExchangeResult\000"
.LASF44:
	.ascii	"pkeycode\000"
.LASF19:
	.ascii	"keycode\000"
.LASF32:
	.ascii	"_02_menu\000"
.LASF49:
	.ascii	"GR_PROGRAMMING_AIRLINK_process_screen\000"
.LASF70:
	.ascii	"g_GR_PROGRAMMING_AIRLINK_port\000"
.LASF17:
	.ascii	"dlen\000"
.LASF26:
	.ascii	"code_date\000"
.LASF41:
	.ascii	"start_gr_device_inquiry\000"
.LASF58:
	.ascii	"GuiVar_GRNetworkState\000"
.LASF22:
	.ascii	"float\000"
.LASF11:
	.ascii	"long long int\000"
.LASF46:
	.ascii	"cmqs\000"
.LASF5:
	.ascii	"UNS_8\000"
.LASF60:
	.ascii	"GuiVar_GRRadioSerialNum\000"
.LASF24:
	.ascii	"who_the_message_was_to\000"
.LASF13:
	.ascii	"long int\000"
.LASF43:
	.ascii	"pport\000"
.LASF25:
	.ascii	"message_class\000"
.LASF37:
	.ascii	"_06_u32_argument1\000"
.LASF18:
	.ascii	"DATA_HANDLE\000"
.LASF35:
	.ascii	"key_process_func_ptr\000"
.LASF39:
	.ascii	"_08_screen_to_draw\000"
.LASF45:
	.ascii	"pcomplete_redraw\000"
.LASF68:
	.ascii	"GuiFont_DecimalChar\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF57:
	.ascii	"GuiVar_GRModel\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF3:
	.ascii	"signed char\000"
.LASF36:
	.ascii	"_04_func_ptr\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF67:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF71:
	.ascii	"GR_PROGRAMMING_AIRLINK_querying_device\000"
.LASF59:
	.ascii	"GuiVar_GRPhoneNumber\000"
.LASF33:
	.ascii	"_03_structure_to_draw\000"
.LASF27:
	.ascii	"code_time\000"
.LASF65:
	.ascii	"GuiLib_ActiveCursorFieldNo\000"
.LASF1:
	.ascii	"char\000"
.LASF52:
	.ascii	"GuiVar_GRCarrier\000"
.LASF56:
	.ascii	"GuiVar_GRMake\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF34:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF42:
	.ascii	"FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange"
	.ascii	"_key\000"
.LASF38:
	.ascii	"_07_u32_argument2\000"
.LASF30:
	.ascii	"COMM_MNGR_TASK_QUEUE_STRUCT\000"
.LASF28:
	.ascii	"port\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF73:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_gr_programming_airlink.c\000"
.LASF63:
	.ascii	"GuiVar_GRSIMID\000"
.LASF21:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF54:
	.ascii	"GuiVar_GRFirmwareVersion\000"
.LASF29:
	.ascii	"reason_for_scan\000"
.LASF20:
	.ascii	"repeats\000"
.LASF66:
	.ascii	"GuiFont_LanguageActive\000"
.LASF47:
	.ascii	"lcursor_to_select\000"
.LASF55:
	.ascii	"GuiVar_GRIPAddress\000"
.LASF48:
	.ascii	"FDTO_GR_PROGRAMMING_AIRLINK_draw_screen\000"
.LASF53:
	.ascii	"GuiVar_GRECIO\000"
.LASF50:
	.ascii	"pkey_event\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF40:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF69:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF14:
	.ascii	"from\000"
.LASF74:
	.ascii	"GR_PROGRAMMING_AIRLINK_initialize_guivars\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
