	.file	"r_rt_lights.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.bss.g_REAL_TIME_LIGHTS_PreviousEnergizedCount,"aw",%nobits
	.align	2
	.type	g_REAL_TIME_LIGHTS_PreviousEnergizedCount, %object
	.size	g_REAL_TIME_LIGHTS_PreviousEnergizedCount, 4
g_REAL_TIME_LIGHTS_PreviousEnergizedCount:
	.space	4
	.section .rodata
	.align	2
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_lights.c\000"
	.align	2
.LC1:
	.ascii	"Light %c%d: %s\000"
	.section	.text.REAL_TIME_LIGHTS_draw_scroll_line,"ax",%progbits
	.align	2
	.type	REAL_TIME_LIGHTS_draw_scroll_line, %function
REAL_TIME_LIGHTS_draw_scroll_line:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_rt_lights.c"
	.loc 1 42 0
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, fp, lr}
.LCFI0:
	add	fp, sp, #12
.LCFI1:
	sub	sp, sp, #48
.LCFI2:
	mov	r3, r0
	strh	r3, [fp, #-52]	@ movhi
	.loc 1 67 0
	ldr	r3, .L6	@ float
	str	r3, [fp, #-48]	@ float
	.loc 1 69 0
	ldr	r3, .L6+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L6+8
	mov	r3, #69
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 71 0
	ldr	r3, .L6+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L6+8
	mov	r3, #71
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 73 0
	ldrsh	r3, [fp, #-52]
	mov	r0, r3
	bl	IRRI_LIGHTS_get_ptr_to_energized_light
	str	r0, [fp, #-20]
	.loc 1 75 0
	ldrsh	r3, [fp, #-52]
	mov	r0, r3
	bl	IRRI_LIGHTS_get_box_index
	str	r0, [fp, #-24]
	.loc 1 76 0
	ldrsh	r3, [fp, #-52]
	mov	r0, r3
	bl	IRRI_LIGHTS_get_output_index
	str	r0, [fp, #-28]
	.loc 1 77 0
	ldrsh	r3, [fp, #-52]
	mov	r0, r3
	bl	IRRI_LIGHTS_get_stop_date
	str	r0, [fp, #-32]
	.loc 1 78 0
	ldrsh	r3, [fp, #-52]
	mov	r0, r3
	bl	IRRI_LIGHTS_get_stop_time
	str	r0, [fp, #-36]
	.loc 1 80 0
	ldr	r3, [fp, #-24]
	add	r4, r3, #65
	ldr	r3, [fp, #-28]
	add	r5, r3, #1
	ldr	r0, [fp, #-24]
	ldr	r1, [fp, #-28]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	mov	r3, r0
	mov	r0, r3
	bl	nm_GROUP_get_name
	mov	r3, r0
	str	r5, [sp, #0]
	str	r3, [sp, #4]
	ldr	r0, .L6+16
	mov	r1, #65
	ldr	r2, .L6+20
	mov	r3, r4
	bl	snprintf
	.loc 1 84 0
	ldrsh	r3, [fp, #-52]
	mov	r0, r3
	bl	IRRI_LIGHTS_get_reason_on_list
	mov	r2, r0
	ldr	r3, .L6+24
	str	r2, [r3, #0]
	.loc 1 90 0
	sub	r3, fp, #44
	mov	r0, r3
	bl	EPSON_obtain_latest_time_and_date
	.loc 1 93 0
	ldrh	r3, [fp, #-40]
	mov	r2, r3
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bne	.L2
	.loc 1 97 0
	ldr	r2, [fp, #-44]
	ldr	r3, [fp, #-36]
	cmp	r2, r3
	bhi	.L3
	.loc 1 99 0
	ldr	r3, [fp, #-44]
	ldr	r2, [fp, #-36]
	rsb	r3, r3, r2
	str	r3, [fp, #-16]
	b	.L4
.L3:
	.loc 1 105 0
	mov	r3, #0
	str	r3, [fp, #-16]
.L4:
	.loc 1 108 0
	ldr	r3, [fp, #-16]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-48]
	fdivs	s15, s14, s15
	ldr	r3, .L6+28
	fsts	s15, [r3, #0]
	b	.L5
.L2:
	.loc 1 115 0
	ldr	r3, [fp, #-44]
	rsb	r3, r3, #86016
	add	r3, r3, #384
	str	r3, [fp, #-16]
	.loc 1 116 0
	ldrh	r3, [fp, #-40]
	ldr	r2, [fp, #-32]
	rsb	r3, r3, r2
	ldr	r2, .L6+32
	mul	r2, r3, r2
	ldr	r3, [fp, #-16]
	add	r3, r2, r3
	sub	r3, r3, #86016
	sub	r3, r3, #384
	str	r3, [fp, #-16]
	.loc 1 117 0
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-36]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	.loc 1 118 0
	ldr	r3, [fp, #-16]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-48]
	fdivs	s15, s14, s15
	ldr	r3, .L6+28
	fsts	s15, [r3, #0]
.L5:
	.loc 1 121 0
	ldr	r3, .L6+12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 123 0
	ldr	r3, .L6+4
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 125 0
	sub	sp, fp, #12
	ldmfd	sp!, {r4, r5, fp, pc}
.L7:
	.align	2
.L6:
	.word	1114636288
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	irri_lights_recursive_MUTEX
	.word	GuiVar_GroupName
	.word	.LC1
	.word	GuiVar_LightStartReason
	.word	GuiVar_LightRemainingMinutes
	.word	86400
.LFE0:
	.size	REAL_TIME_LIGHTS_draw_scroll_line, .-REAL_TIME_LIGHTS_draw_scroll_line
	.section	.text.FDTO_REAL_TIME_LIGHTS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_LIGHTS_draw_report
	.type	FDTO_REAL_TIME_LIGHTS_draw_report, %function
FDTO_REAL_TIME_LIGHTS_draw_report:
.LFB1:
	.loc 1 129 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	str	r0, [fp, #-8]
	.loc 1 133 0
	mov	r0, #95
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
	.loc 1 136 0
	ldr	r3, .L9
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L9+4
	mov	r3, #136
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 138 0
	bl	IRRI_LIGHTS_populate_pointers_of_energized_lights
	mov	r2, r0
	ldr	r3, .L9+8
	str	r2, [r3, #0]
	.loc 1 145 0
	ldr	r3, .L9+8
	ldr	r3, [r3, #0]
	ldr	r0, [fp, #-8]
	mov	r1, r3
	ldr	r2, .L9+12
	bl	FDTO_REPORTS_draw_report
	.loc 1 147 0
	ldr	r3, .L9
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 149 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L10:
	.align	2
.L9:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	g_REAL_TIME_LIGHTS_PreviousEnergizedCount
	.word	REAL_TIME_LIGHTS_draw_scroll_line
.LFE1:
	.size	FDTO_REAL_TIME_LIGHTS_draw_report, .-FDTO_REAL_TIME_LIGHTS_draw_report
	.section	.text.FDTO_REAL_TIME_LIGHTS_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_LIGHTS_redraw_scrollbox
	.type	FDTO_REAL_TIME_LIGHTS_redraw_scrollbox, %function
FDTO_REAL_TIME_LIGHTS_redraw_scrollbox:
.LFB2:
	.loc 1 153 0
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #4
.LCFI8:
	.loc 1 157 0
	ldr	r3, .L12
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L12+4
	mov	r3, #157
	bl	xQueueTakeMutexRecursive_debug
	.loc 1 160 0
	bl	IRRI_LIGHTS_populate_pointers_of_energized_lights
	str	r0, [fp, #-8]
	.loc 1 169 0
	ldr	r3, .L12+8
	ldr	r3, [r3, #0]
	ldr	r2, [fp, #-8]
	cmp	r2, r3
	moveq	r3, #0
	movne	r3, #1
	mov	r0, #0
	ldr	r1, [fp, #-8]
	mov	r2, r3
	bl	FDTO_SCROLL_BOX_redraw_retaining_topline
	.loc 1 171 0
	ldr	r3, .L12+8
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	.loc 1 173 0
	ldr	r3, .L12
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	.loc 1 175 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L13:
	.align	2
.L12:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	g_REAL_TIME_LIGHTS_PreviousEnergizedCount
.LFE2:
	.size	FDTO_REAL_TIME_LIGHTS_redraw_scrollbox, .-FDTO_REAL_TIME_LIGHTS_redraw_scrollbox
	.section	.text.REAL_TIME_LIGHTS_process_report,"ax",%progbits
	.align	2
	.global	REAL_TIME_LIGHTS_process_report
	.type	REAL_TIME_LIGHTS_process_report, %function
REAL_TIME_LIGHTS_process_report:
.LFB3:
	.loc 1 179 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-12]
	str	r1, [fp, #-8]
	.loc 1 180 0
	ldr	r3, [fp, #-12]
	cmp	r3, #67
	bne	.L19
.L16:
	.loc 1 183 0
	ldr	r3, .L21
	ldr	r2, [r3, #0]
	ldr	r0, .L21+4
	mov	r1, #4
	mov	r3, r2
	mov	r3, r3, asl #3
	add	r3, r3, r2
	mov	r3, r3, asl #2
	add	r3, r0, r3
	add	r3, r3, r1
	ldr	r2, [r3, #0]
	ldr	r3, .L21+8
	str	r2, [r3, #0]
	.loc 1 185 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	bl	KEY_process_global_keys
	.loc 1 187 0
	ldr	r3, .L21+8
	ldr	r3, [r3, #0]
	cmp	r3, #10
	bne	.L20
	.loc 1 191 0
	mov	r0, #1
	bl	LIVE_SCREENS_draw_dialog
	.loc 1 193 0
	b	.L20
.L19:
	.loc 1 196 0
	sub	r1, fp, #12
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #0
	bl	REPORTS_process_report
	b	.L14
.L20:
	.loc 1 193 0
	mov	r0, r0	@ nop
.L14:
	.loc 1 199 0
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L22:
	.align	2
.L21:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	REAL_TIME_LIGHTS_process_report, .-REAL_TIME_LIGHTS_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portmacro.h"
	.file 4 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/queue.h"
	.file 5 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/include/semphr.h"
	.file 6 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.h"
	.file 7 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.h"
	.file 8 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_list.h"
	.file 9 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/irri_lights.h"
	.file 10 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.h"
	.file 11 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiVar.h"
	.file 12 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/GuiFont.h"
	.file 13 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.h"
	.file 14 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5fa
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF79
	.byte	0x1
	.4byte	.LASF80
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x4c
	.4byte	0x4c
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x55
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x5e
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x2
	.byte	0x99
	.4byte	0x70
	.uleb128 0x5
	.byte	0x4
	.4byte	0x9d
	.uleb128 0x6
	.4byte	0xa4
	.uleb128 0x7
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF13
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x35
	.4byte	0x25
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x4
	.byte	0x57
	.4byte	0xa4
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x5
	.byte	0x4c
	.4byte	0xb8
	.uleb128 0x9
	.4byte	0x33
	.4byte	0xde
	.uleb128 0xa
	.4byte	0x25
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.byte	0x8
	.byte	0x6
	.byte	0x7c
	.4byte	0x103
	.uleb128 0xc
	.4byte	.LASF17
	.byte	0x6
	.byte	0x7e
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF18
	.byte	0x6
	.byte	0x80
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x6
	.byte	0x82
	.4byte	0xde
	.uleb128 0xb
	.byte	0x6
	.byte	0x7
	.byte	0x22
	.4byte	0x12f
	.uleb128 0xd
	.ascii	"T\000"
	.byte	0x7
	.byte	0x24
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xd
	.ascii	"D\000"
	.byte	0x7
	.byte	0x26
	.4byte	0x41
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x7
	.byte	0x28
	.4byte	0x10e
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x14a
	.uleb128 0xa
	.4byte	0x25
	.byte	0x2
	.byte	0
	.uleb128 0xb
	.byte	0x14
	.byte	0x8
	.byte	0x18
	.4byte	0x199
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x8
	.byte	0x1a
	.4byte	0xa4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x8
	.byte	0x1c
	.4byte	0xa4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x8
	.byte	0x1e
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x8
	.byte	0x20
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x8
	.byte	0x23
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x3
	.4byte	.LASF26
	.byte	0x8
	.byte	0x26
	.4byte	0x14a
	.uleb128 0xb
	.byte	0xc
	.byte	0x8
	.byte	0x2a
	.4byte	0x1d7
	.uleb128 0xc
	.4byte	.LASF27
	.byte	0x8
	.byte	0x2c
	.4byte	0xa4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x8
	.byte	0x2e
	.4byte	0xa4
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF29
	.byte	0x8
	.byte	0x30
	.4byte	0x1d7
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x199
	.uleb128 0x3
	.4byte	.LASF30
	.byte	0x8
	.byte	0x32
	.4byte	0x1a4
	.uleb128 0x2
	.byte	0x4
	.byte	0x4
	.4byte	.LASF31
	.uleb128 0x9
	.4byte	0x65
	.4byte	0x1ff
	.uleb128 0xa
	.4byte	0x25
	.byte	0x3
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x4
	.4byte	.LASF32
	.uleb128 0xb
	.byte	0x28
	.byte	0x9
	.byte	0x48
	.4byte	0x27f
	.uleb128 0xc
	.4byte	.LASF33
	.byte	0x9
	.byte	0x4d
	.4byte	0x1dd
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF34
	.byte	0x9
	.byte	0x57
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF35
	.byte	0x9
	.byte	0x5b
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF36
	.byte	0x9
	.byte	0x5d
	.4byte	0x8c
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF37
	.byte	0x9
	.byte	0x61
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF38
	.byte	0x9
	.byte	0x63
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF39
	.byte	0x9
	.byte	0x6a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0xc
	.4byte	.LASF40
	.byte	0x9
	.byte	0x6c
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.byte	0
	.uleb128 0x3
	.4byte	.LASF41
	.byte	0x9
	.byte	0x70
	.4byte	0x206
	.uleb128 0xb
	.byte	0x24
	.byte	0xa
	.byte	0x78
	.4byte	0x311
	.uleb128 0xc
	.4byte	.LASF42
	.byte	0xa
	.byte	0x7b
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.4byte	.LASF43
	.byte	0xa
	.byte	0x83
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xc
	.4byte	.LASF44
	.byte	0xa
	.byte	0x86
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xc
	.4byte	.LASF45
	.byte	0xa
	.byte	0x88
	.4byte	0x322
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0xc
	.4byte	.LASF46
	.byte	0xa
	.byte	0x8d
	.4byte	0x334
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xc
	.4byte	.LASF47
	.byte	0xa
	.byte	0x92
	.4byte	0x97
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0xc
	.4byte	.LASF48
	.byte	0xa
	.byte	0x96
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xc
	.4byte	.LASF49
	.byte	0xa
	.byte	0x9a
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0xc
	.4byte	.LASF50
	.byte	0xa
	.byte	0x9c
	.4byte	0x65
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	0x31d
	.uleb128 0xf
	.4byte	0x31d
	.byte	0
	.uleb128 0x10
	.4byte	0x53
	.uleb128 0x5
	.byte	0x4
	.4byte	0x311
	.uleb128 0xe
	.byte	0x1
	.4byte	0x334
	.uleb128 0xf
	.4byte	0x103
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x328
	.uleb128 0x3
	.4byte	.LASF51
	.byte	0xa
	.byte	0x9e
	.4byte	0x28a
	.uleb128 0x11
	.4byte	.LASF81
	.byte	0x1
	.byte	0x28
	.byte	0x1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x3dc
	.uleb128 0x12
	.4byte	.LASF57
	.byte	0x1
	.byte	0x28
	.4byte	0x31d
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x13
	.4byte	.LASF52
	.byte	0x1
	.byte	0x2b
	.4byte	0x3dc
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x14
	.ascii	"ldt\000"
	.byte	0x1
	.byte	0x2d
	.4byte	0x12f
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x13
	.4byte	.LASF53
	.byte	0x1
	.byte	0x2f
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x13
	.4byte	.LASF54
	.byte	0x1
	.byte	0x31
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x13
	.4byte	.LASF38
	.byte	0x1
	.byte	0x33
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x13
	.4byte	.LASF37
	.byte	0x1
	.byte	0x35
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x13
	.4byte	.LASF55
	.byte	0x1
	.byte	0x37
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x13
	.4byte	.LASF56
	.byte	0x1
	.byte	0x43
	.4byte	0x3e2
	.byte	0x2
	.byte	0x91
	.sleb128 -52
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.4byte	0x27f
	.uleb128 0x15
	.4byte	0x3e7
	.uleb128 0x10
	.4byte	0x1e8
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF59
	.byte	0x1
	.byte	0x80
	.byte	0x1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x414
	.uleb128 0x12
	.4byte	.LASF58
	.byte	0x1
	.byte	0x80
	.4byte	0x414
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x10
	.4byte	0x8c
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF60
	.byte	0x1
	.byte	0x98
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x441
	.uleb128 0x13
	.4byte	.LASF61
	.byte	0x1
	.byte	0x9a
	.4byte	0x65
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x16
	.byte	0x1
	.4byte	.LASF62
	.byte	0x1
	.byte	0xb2
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x469
	.uleb128 0x12
	.4byte	.LASF63
	.byte	0x1
	.byte	0xb2
	.4byte	0x469
	.byte	0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x10
	.4byte	0x103
	.uleb128 0x9
	.4byte	0x2c
	.4byte	0x47e
	.uleb128 0xa
	.4byte	0x25
	.byte	0x40
	.byte	0
	.uleb128 0x17
	.4byte	.LASF64
	.byte	0xb
	.2byte	0x1fc
	.4byte	0x46e
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF65
	.byte	0xb
	.2byte	0x287
	.4byte	0x1e8
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF66
	.byte	0xb
	.2byte	0x29c
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF67
	.byte	0xb
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF68
	.byte	0xc
	.byte	0x30
	.4byte	0x4c7
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageActive
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF69
	.byte	0xc
	.byte	0x34
	.4byte	0x4dd
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageTextDir
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF70
	.byte	0xc
	.byte	0x36
	.4byte	0x4f3
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_DecimalChar
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF71
	.byte	0xc
	.byte	0x38
	.4byte	0x509
	.byte	0x5
	.byte	0x3
	.4byte	GuiFont_LanguageCharSets
	.uleb128 0x10
	.4byte	0xce
	.uleb128 0x13
	.4byte	.LASF72
	.byte	0xd
	.byte	0x33
	.4byte	0x51f
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS
	.uleb128 0x10
	.4byte	0x13a
	.uleb128 0x13
	.4byte	.LASF73
	.byte	0xd
	.byte	0x3f
	.4byte	0x535
	.byte	0x5
	.byte	0x3
	.4byte	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS
	.uleb128 0x10
	.4byte	0x1ef
	.uleb128 0x18
	.4byte	.LASF74
	.byte	0xe
	.byte	0xfe
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF75
	.byte	0xe
	.2byte	0x104
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x9
	.4byte	0x33a
	.4byte	0x565
	.uleb128 0xa
	.4byte	0x25
	.byte	0x31
	.byte	0
	.uleb128 0x18
	.4byte	.LASF76
	.byte	0xa
	.byte	0xac
	.4byte	0x555
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF77
	.byte	0xa
	.byte	0xae
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.4byte	.LASF78
	.byte	0x1
	.byte	0x1f
	.4byte	0x65
	.byte	0x5
	.byte	0x3
	.4byte	g_REAL_TIME_LIGHTS_PreviousEnergizedCount
	.uleb128 0x17
	.4byte	.LASF64
	.byte	0xb
	.2byte	0x1fc
	.4byte	0x46e
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF65
	.byte	0xb
	.2byte	0x287
	.4byte	0x1e8
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF66
	.byte	0xb
	.2byte	0x29c
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF67
	.byte	0xb
	.2byte	0x2ec
	.4byte	0x70
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF74
	.byte	0xe
	.byte	0xfe
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x17
	.4byte	.LASF75
	.byte	0xe
	.2byte	0x104
	.4byte	0xc3
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF76
	.byte	0xa
	.byte	0xac
	.4byte	0x555
	.byte	0x1
	.byte	0x1
	.uleb128 0x18
	.4byte	.LASF77
	.byte	0xa
	.byte	0xae
	.4byte	0x65
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF62:
	.ascii	"REAL_TIME_LIGHTS_process_report\000"
.LASF79:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF39:
	.ascii	"box_index_0\000"
.LASF20:
	.ascii	"DATE_TIME\000"
.LASF66:
	.ascii	"GuiVar_LightStartReason\000"
.LASF22:
	.ascii	"ptail\000"
.LASF7:
	.ascii	"short int\000"
.LASF14:
	.ascii	"portTickType\000"
.LASF35:
	.ascii	"reason_on_list\000"
.LASF42:
	.ascii	"_01_command\000"
.LASF59:
	.ascii	"FDTO_REAL_TIME_LIGHTS_draw_report\000"
.LASF41:
	.ascii	"IRRI_LIGHTS_LIST_COMPONENT\000"
.LASF55:
	.ascii	"remaining_seconds\000"
.LASF67:
	.ascii	"GuiVar_MenuScreenToShow\000"
.LASF17:
	.ascii	"keycode\000"
.LASF43:
	.ascii	"_02_menu\000"
.LASF25:
	.ascii	"InUse\000"
.LASF56:
	.ascii	"SIXTY_SECONDS_PER_MINUTE\000"
.LASF27:
	.ascii	"pPrev\000"
.LASF77:
	.ascii	"screen_history_index\000"
.LASF31:
	.ascii	"float\000"
.LASF11:
	.ascii	"long long int\000"
.LASF70:
	.ascii	"GuiFont_DecimalChar\000"
.LASF34:
	.ascii	"action_needed\000"
.LASF81:
	.ascii	"REAL_TIME_LIGHTS_draw_scroll_line\000"
.LASF26:
	.ascii	"MIST_LIST_HDR_TYPE\000"
.LASF74:
	.ascii	"list_lights_recursive_MUTEX\000"
.LASF13:
	.ascii	"long int\000"
.LASF64:
	.ascii	"GuiVar_GroupName\000"
.LASF15:
	.ascii	"xQueueHandle\000"
.LASF24:
	.ascii	"offset\000"
.LASF5:
	.ascii	"UNS_16\000"
.LASF36:
	.ascii	"light_is_energized\000"
.LASF48:
	.ascii	"_06_u32_argument1\000"
.LASF53:
	.ascii	"box_index\000"
.LASF46:
	.ascii	"key_process_func_ptr\000"
.LASF50:
	.ascii	"_08_screen_to_draw\000"
.LASF58:
	.ascii	"pcomplete_redraw\000"
.LASF75:
	.ascii	"irri_lights_recursive_MUTEX\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF12:
	.ascii	"BOOL_32\000"
.LASF3:
	.ascii	"signed char\000"
.LASF47:
	.ascii	"_04_func_ptr\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF69:
	.ascii	"GuiFont_LanguageTextDir\000"
.LASF61:
	.ascii	"energized_count\000"
.LASF9:
	.ascii	"unsigned int\000"
.LASF54:
	.ascii	"output_index\000"
.LASF28:
	.ascii	"pNext\000"
.LASF38:
	.ascii	"stop_datetime_d\000"
.LASF1:
	.ascii	"char\000"
.LASF78:
	.ascii	"g_REAL_TIME_LIGHTS_PreviousEnergizedCount\000"
.LASF71:
	.ascii	"GuiFont_LanguageCharSets\000"
.LASF4:
	.ascii	"short unsigned int\000"
.LASF45:
	.ascii	"populate_scroll_box_func_ptr\000"
.LASF16:
	.ascii	"xSemaphoreHandle\000"
.LASF49:
	.ascii	"_07_u32_argument2\000"
.LASF37:
	.ascii	"stop_datetime_t\000"
.LASF40:
	.ascii	"output_index_0\000"
.LASF33:
	.ascii	"list_linkage_for_irri_lights_ON_list\000"
.LASF6:
	.ascii	"INT_16\000"
.LASF0:
	.ascii	"long unsigned int\000"
.LASF73:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS\000"
.LASF57:
	.ascii	"pline_index_i16\000"
.LASF19:
	.ascii	"KEY_TO_PROCESS_QUEUE_STRUCT\000"
.LASF44:
	.ascii	"_03_structure_to_draw\000"
.LASF23:
	.ascii	"count\000"
.LASF18:
	.ascii	"repeats\000"
.LASF68:
	.ascii	"GuiFont_LanguageActive\000"
.LASF72:
	.ascii	"IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS\000"
.LASF80:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_lights.c\000"
.LASF52:
	.ascii	"lillc\000"
.LASF21:
	.ascii	"phead\000"
.LASF29:
	.ascii	"pListHdr\000"
.LASF63:
	.ascii	"pkey_event\000"
.LASF32:
	.ascii	"double\000"
.LASF8:
	.ascii	"UNS_32\000"
.LASF51:
	.ascii	"DISPLAY_EVENT_STRUCT\000"
.LASF65:
	.ascii	"GuiVar_LightRemainingMinutes\000"
.LASF60:
	.ascii	"FDTO_REAL_TIME_LIGHTS_redraw_scrollbox\000"
.LASF76:
	.ascii	"ScreenHistory\000"
.LASF30:
	.ascii	"MIST_DLINK_TYPE\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
