	.file	"lpc32xx_gpio_driver.c"
	.text
.Ltext0:
	.section	.text.gpio_set_gpo_state,"ax",%progbits
	.align	2
	.global	gpio_set_gpo_state
	.type	gpio_set_gpo_state, %function
gpio_set_gpo_state:
.LFB0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_gpio_driver.c"
	.loc 1 55 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI0:
	add	fp, sp, #0
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 56 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	beq	.L2
	.loc 1 58 0
	ldr	r3, .L4
	ldr	r2, [fp, #-4]
	str	r2, [r3, #4]
.L2:
	.loc 1 61 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L3
	.loc 1 63 0
	ldr	r3, .L4
	ldr	r2, [fp, #-8]
	str	r2, [r3, #8]
.L3:
	.loc 1 66 0
	ldr	r3, .L4
	ldr	r3, [r3, #12]
	.loc 1 67 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L5:
	.align	2
.L4:
	.word	1073905664
.LFE0:
	.size	gpio_set_gpo_state, .-gpio_set_gpo_state
	.section	.text.gpio_set_dir,"ax",%progbits
	.align	2
	.global	gpio_set_dir
	.type	gpio_set_dir, %function
gpio_set_dir:
.LFB1:
	.loc 1 93 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI3:
	add	fp, sp, #0
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 94 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	beq	.L7
	.loc 1 96 0
	ldr	r3, .L9
	ldr	r2, [fp, #-4]
	str	r2, [r3, #16]
.L7:
	.loc 1 99 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L8
	.loc 1 101 0
	ldr	r3, .L9
	ldr	r2, [fp, #-8]
	str	r2, [r3, #20]
.L8:
	.loc 1 104 0
	ldr	r3, .L9
	ldr	r3, [r3, #24]
	.loc 1 105 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L10:
	.align	2
.L9:
	.word	1073905664
.LFE1:
	.size	gpio_set_dir, .-gpio_set_dir
	.section	.text.gpio_set_sdr_state,"ax",%progbits
	.align	2
	.global	gpio_set_sdr_state
	.type	gpio_set_sdr_state, %function
gpio_set_sdr_state:
.LFB2:
	.loc 1 131 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI6:
	add	fp, sp, #0
.LCFI7:
	sub	sp, sp, #8
.LCFI8:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 132 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	beq	.L12
	.loc 1 134 0
	ldr	r3, .L14
	ldr	r2, [fp, #-4]
	str	r2, [r3, #32]
.L12:
	.loc 1 137 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L13
	.loc 1 139 0
	ldr	r3, .L14
	ldr	r2, [fp, #-8]
	str	r2, [r3, #36]
.L13:
	.loc 1 142 0
	ldr	r3, .L14
	ldr	r3, [r3, #28]
	.loc 1 143 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L15:
	.align	2
.L14:
	.word	1073905664
.LFE2:
	.size	gpio_set_sdr_state, .-gpio_set_sdr_state
	.section	.text.gpio_set_p0_state,"ax",%progbits
	.align	2
	.global	gpio_set_p0_state
	.type	gpio_set_p0_state, %function
gpio_set_p0_state:
.LFB3:
	.loc 1 169 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI9:
	add	fp, sp, #0
.LCFI10:
	sub	sp, sp, #8
.LCFI11:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 170 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	beq	.L17
	.loc 1 172 0
	ldr	r3, .L19
	ldr	r2, [fp, #-4]
	str	r2, [r3, #68]
.L17:
	.loc 1 175 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L18
	.loc 1 177 0
	ldr	r3, .L19
	ldr	r2, [fp, #-8]
	str	r2, [r3, #72]
.L18:
	.loc 1 180 0
	ldr	r3, .L19
	ldr	r3, [r3, #76]
	.loc 1 181 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L20:
	.align	2
.L19:
	.word	1073905664
.LFE3:
	.size	gpio_set_p0_state, .-gpio_set_p0_state
	.section	.text.gpio_set_p0_dir,"ax",%progbits
	.align	2
	.global	gpio_set_p0_dir
	.type	gpio_set_p0_dir, %function
gpio_set_p0_dir:
.LFB4:
	.loc 1 205 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI12:
	add	fp, sp, #0
.LCFI13:
	sub	sp, sp, #8
.LCFI14:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 206 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	beq	.L22
	.loc 1 208 0
	ldr	r3, .L24
	ldr	r2, [fp, #-4]
	str	r2, [r3, #80]
.L22:
	.loc 1 211 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L23
	.loc 1 213 0
	ldr	r3, .L24
	ldr	r2, [fp, #-8]
	str	r2, [r3, #84]
.L23:
	.loc 1 216 0
	ldr	r3, .L24
	ldr	r3, [r3, #88]
	.loc 1 217 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L25:
	.align	2
.L24:
	.word	1073905664
.LFE4:
	.size	gpio_set_p0_dir, .-gpio_set_p0_dir
	.section	.text.gpio_set_p1_state,"ax",%progbits
	.align	2
	.global	gpio_set_p1_state
	.type	gpio_set_p1_state, %function
gpio_set_p1_state:
.LFB5:
	.loc 1 243 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI15:
	add	fp, sp, #0
.LCFI16:
	sub	sp, sp, #8
.LCFI17:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 244 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	beq	.L27
	.loc 1 246 0
	ldr	r3, .L29
	ldr	r2, [fp, #-4]
	str	r2, [r3, #100]
.L27:
	.loc 1 249 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L28
	.loc 1 251 0
	ldr	r3, .L29
	ldr	r2, [fp, #-8]
	str	r2, [r3, #104]
.L28:
	.loc 1 254 0
	ldr	r3, .L29
	ldr	r3, [r3, #108]
	.loc 1 255 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L30:
	.align	2
.L29:
	.word	1073905664
.LFE5:
	.size	gpio_set_p1_state, .-gpio_set_p1_state
	.section	.text.gpio_set_p1_dir,"ax",%progbits
	.align	2
	.global	gpio_set_p1_dir
	.type	gpio_set_p1_dir, %function
gpio_set_p1_dir:
.LFB6:
	.loc 1 279 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI18:
	add	fp, sp, #0
.LCFI19:
	sub	sp, sp, #8
.LCFI20:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 280 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	beq	.L32
	.loc 1 282 0
	ldr	r3, .L34
	ldr	r2, [fp, #-4]
	str	r2, [r3, #112]
.L32:
	.loc 1 285 0
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L33
	.loc 1 287 0
	ldr	r3, .L34
	ldr	r2, [fp, #-8]
	str	r2, [r3, #116]
.L33:
	.loc 1 290 0
	ldr	r3, .L34
	ldr	r3, [r3, #120]
	.loc 1 291 0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L35:
	.align	2
.L34:
	.word	1073905664
.LFE6:
	.size	gpio_set_p1_dir, .-gpio_set_p1_dir
	.section	.text.i2s_pin_mux,"ax",%progbits
	.align	2
	.global	i2s_pin_mux
	.type	i2s_pin_mux, %function
i2s_pin_mux:
.LFB7:
	.loc 1 312 0
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI21:
	add	fp, sp, #0
.LCFI22:
	sub	sp, sp, #8
.LCFI23:
	str	r0, [fp, #-4]
	str	r1, [fp, #-8]
	.loc 1 315 0
	ldr	r3, [fp, #-4]
	cmp	r3, #0
	bne	.L37
	.loc 1 317 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L38
	.loc 1 319 0
	ldr	r3, .L41
	mov	r2, #252
	str	r2, [r3, #288]
	b	.L37
.L38:
	.loc 1 325 0
	ldr	r3, .L41
	mov	r2, #252
	str	r2, [r3, #292]
.L37:
	.loc 1 332 0
	ldr	r3, [fp, #-4]
	cmp	r3, #1
	bne	.L36
	.loc 1 334 0
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L40
	.loc 1 336 0
	ldr	r3, .L41
	mov	r2, #28
	str	r2, [r3, #260]
	.loc 1 338 0
	ldr	r3, .L41
	mov	r2, #3
	str	r2, [r3, #288]
	b	.L36
.L40:
	.loc 1 344 0
	ldr	r3, .L41
	mov	r2, #28
	str	r2, [r3, #256]
	.loc 1 346 0
	ldr	r3, .L41
	mov	r2, #3
	str	r2, [r3, #292]
.L36:
	.loc 1 349 0
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L42:
	.align	2
.L41:
	.word	1073905664
.LFE7:
	.size	i2s_pin_mux, .-i2s_pin_mux
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_gpio.h"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/lpc_types.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x519
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF70
	.byte	0x1
	.4byte	.LASF71
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF55
	.byte	0x3
	.byte	0x5e
	.4byte	0x53
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x5
	.2byte	0x13c
	.byte	0x2
	.byte	0x38
	.4byte	0x30d
	.uleb128 0x6
	.4byte	.LASF8
	.byte	0x2
	.byte	0x3a
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.4byte	.LASF9
	.byte	0x2
	.byte	0x3b
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x6
	.4byte	.LASF10
	.byte	0x2
	.byte	0x3c
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x6
	.4byte	.LASF11
	.byte	0x2
	.byte	0x3d
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0xc
	.uleb128 0x6
	.4byte	.LASF12
	.byte	0x2
	.byte	0x3f
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x6
	.4byte	.LASF13
	.byte	0x2
	.byte	0x40
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x14
	.uleb128 0x6
	.4byte	.LASF14
	.byte	0x2
	.byte	0x41
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x6
	.4byte	.LASF15
	.byte	0x2
	.byte	0x42
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x1c
	.uleb128 0x6
	.4byte	.LASF16
	.byte	0x2
	.byte	0x43
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x2
	.byte	0x44
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x24
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x2
	.byte	0x45
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x2
	.byte	0x46
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x2c
	.uleb128 0x6
	.4byte	.LASF20
	.byte	0x2
	.byte	0x47
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x6
	.4byte	.LASF21
	.byte	0x2
	.byte	0x49
	.4byte	0x329
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x6
	.4byte	.LASF22
	.byte	0x2
	.byte	0x4b
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x6
	.4byte	.LASF23
	.byte	0x2
	.byte	0x4c
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x44
	.uleb128 0x6
	.4byte	.LASF24
	.byte	0x2
	.byte	0x4d
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x2
	.byte	0x4e
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x4c
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x2
	.byte	0x4f
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x2
	.byte	0x50
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x54
	.uleb128 0x6
	.4byte	.LASF28
	.byte	0x2
	.byte	0x51
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x6
	.4byte	.LASF29
	.byte	0x2
	.byte	0x53
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x5c
	.uleb128 0x6
	.4byte	.LASF30
	.byte	0x2
	.byte	0x55
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x6
	.4byte	.LASF31
	.byte	0x2
	.byte	0x56
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x64
	.uleb128 0x6
	.4byte	.LASF32
	.byte	0x2
	.byte	0x57
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x6
	.4byte	.LASF33
	.byte	0x2
	.byte	0x58
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x6c
	.uleb128 0x6
	.4byte	.LASF34
	.byte	0x2
	.byte	0x59
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x6
	.4byte	.LASF35
	.byte	0x2
	.byte	0x5a
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x6
	.4byte	.LASF36
	.byte	0x2
	.byte	0x5b
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x6
	.4byte	.LASF37
	.byte	0x2
	.byte	0x5d
	.4byte	0x30d
	.byte	0x2
	.byte	0x23
	.uleb128 0x7c
	.uleb128 0x6
	.4byte	.LASF38
	.byte	0x2
	.byte	0x5f
	.4byte	0x33e
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x6
	.4byte	.LASF39
	.byte	0x2
	.byte	0x61
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x100
	.uleb128 0x6
	.4byte	.LASF40
	.byte	0x2
	.byte	0x62
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x104
	.uleb128 0x6
	.4byte	.LASF41
	.byte	0x2
	.byte	0x63
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x108
	.uleb128 0x6
	.4byte	.LASF42
	.byte	0x2
	.byte	0x65
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x10c
	.uleb128 0x6
	.4byte	.LASF43
	.byte	0x2
	.byte	0x67
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x110
	.uleb128 0x6
	.4byte	.LASF44
	.byte	0x2
	.byte	0x68
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x114
	.uleb128 0x6
	.4byte	.LASF45
	.byte	0x2
	.byte	0x69
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x118
	.uleb128 0x6
	.4byte	.LASF46
	.byte	0x2
	.byte	0x6b
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x11c
	.uleb128 0x6
	.4byte	.LASF47
	.byte	0x2
	.byte	0x6d
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x120
	.uleb128 0x6
	.4byte	.LASF48
	.byte	0x2
	.byte	0x6e
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x124
	.uleb128 0x6
	.4byte	.LASF49
	.byte	0x2
	.byte	0x6f
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x128
	.uleb128 0x6
	.4byte	.LASF50
	.byte	0x2
	.byte	0x71
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x12c
	.uleb128 0x6
	.4byte	.LASF51
	.byte	0x2
	.byte	0x73
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x130
	.uleb128 0x6
	.4byte	.LASF52
	.byte	0x2
	.byte	0x74
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x134
	.uleb128 0x6
	.4byte	.LASF53
	.byte	0x2
	.byte	0x75
	.4byte	0x30d
	.byte	0x3
	.byte	0x23
	.uleb128 0x138
	.byte	0
	.uleb128 0x7
	.4byte	0x48
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x322
	.uleb128 0x9
	.4byte	0x322
	.byte	0x2
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF54
	.uleb128 0x7
	.4byte	0x312
	.uleb128 0x8
	.4byte	0x48
	.4byte	0x33e
	.uleb128 0x9
	.4byte	0x322
	.byte	0x1f
	.byte	0
	.uleb128 0x7
	.4byte	0x32e
	.uleb128 0x3
	.4byte	.LASF56
	.byte	0x2
	.byte	0x77
	.4byte	0x6f
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF59
	.byte	0x1
	.byte	0x35
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.4byte	0x388
	.uleb128 0xb
	.4byte	.LASF57
	.byte	0x1
	.byte	0x35
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xb
	.4byte	.LASF58
	.byte	0x1
	.byte	0x36
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF60
	.byte	0x1
	.byte	0x5b
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.4byte	0x3c2
	.uleb128 0xb
	.4byte	.LASF61
	.byte	0x1
	.byte	0x5b
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xb
	.4byte	.LASF62
	.byte	0x1
	.byte	0x5c
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF63
	.byte	0x1
	.byte	0x81
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.4byte	0x3fc
	.uleb128 0xb
	.4byte	.LASF57
	.byte	0x1
	.byte	0x81
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xb
	.4byte	.LASF58
	.byte	0x1
	.byte	0x82
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF64
	.byte	0x1
	.byte	0xa7
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.4byte	0x436
	.uleb128 0xb
	.4byte	.LASF57
	.byte	0x1
	.byte	0xa7
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xb
	.4byte	.LASF58
	.byte	0x1
	.byte	0xa8
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF65
	.byte	0x1
	.byte	0xcb
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.4byte	0x470
	.uleb128 0xb
	.4byte	.LASF61
	.byte	0x1
	.byte	0xcb
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xb
	.4byte	.LASF62
	.byte	0x1
	.byte	0xcc
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF66
	.byte	0x1
	.byte	0xf1
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.4byte	0x4aa
	.uleb128 0xb
	.4byte	.LASF57
	.byte	0x1
	.byte	0xf1
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xb
	.4byte	.LASF58
	.byte	0x1
	.byte	0xf2
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x115
	.byte	0x1
	.4byte	0x48
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.4byte	0x4e7
	.uleb128 0xd
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x115
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xd
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x116
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x137
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0xd
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x137
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0xd
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x137
	.4byte	0x48
	.byte	0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF72:
	.ascii	"i2s_pin_mux\000"
.LASF51:
	.ascii	"p1_mux_set\000"
.LASF11:
	.ascii	"p3_outp_state\000"
.LASF55:
	.ascii	"UNS_32\000"
.LASF66:
	.ascii	"gpio_set_p1_state\000"
.LASF9:
	.ascii	"p3_outp_set\000"
.LASF64:
	.ascii	"gpio_set_p0_state\000"
.LASF16:
	.ascii	"p2_outp_set\000"
.LASF52:
	.ascii	"p1_mux_clr\000"
.LASF20:
	.ascii	"p2_mux_state\000"
.LASF33:
	.ascii	"p1_outp_state\000"
.LASF36:
	.ascii	"p1_dir_state\000"
.LASF15:
	.ascii	"p2_inp_state\000"
.LASF10:
	.ascii	"p3_outp_clr\000"
.LASF47:
	.ascii	"p0_mux_set\000"
.LASF67:
	.ascii	"gpio_set_p1_dir\000"
.LASF17:
	.ascii	"p2_outp_clr\000"
.LASF31:
	.ascii	"p1_outp_set\000"
.LASF60:
	.ascii	"gpio_set_dir\000"
.LASF6:
	.ascii	"long long unsigned int\000"
.LASF39:
	.ascii	"p_mux_set\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF57:
	.ascii	"set_pins\000"
.LASF23:
	.ascii	"p0_outp_set\000"
.LASF30:
	.ascii	"p1_inp_state\000"
.LASF44:
	.ascii	"p3_mux_clr\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF32:
	.ascii	"p1_outp_clr\000"
.LASF59:
	.ascii	"gpio_set_gpo_state\000"
.LASF18:
	.ascii	"p2_mux_set\000"
.LASF40:
	.ascii	"p_mux_clr\000"
.LASF65:
	.ascii	"gpio_set_p0_dir\000"
.LASF56:
	.ascii	"GPIO_REGS_T\000"
.LASF14:
	.ascii	"p2_dir_state\000"
.LASF12:
	.ascii	"p2_dir_set\000"
.LASF62:
	.ascii	"in_pins\000"
.LASF68:
	.ascii	"i2s_num\000"
.LASF19:
	.ascii	"p2_mux_clr\000"
.LASF54:
	.ascii	"long unsigned int\000"
.LASF5:
	.ascii	"unsigned int\000"
.LASF41:
	.ascii	"p_mux_state\000"
.LASF13:
	.ascii	"p2_dir_clr\000"
.LASF0:
	.ascii	"char\000"
.LASF24:
	.ascii	"p0_outp_clr\000"
.LASF46:
	.ascii	"reserved6\000"
.LASF63:
	.ascii	"gpio_set_sdr_state\000"
.LASF25:
	.ascii	"p0_outp_state\000"
.LASF50:
	.ascii	"reserved7\000"
.LASF35:
	.ascii	"p1_dir_clr\000"
.LASF53:
	.ascii	"p1_mux_state\000"
.LASF58:
	.ascii	"clr_pins\000"
.LASF26:
	.ascii	"p0_dir_set\000"
.LASF71:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_gpio_driver.c\000"
.LASF7:
	.ascii	"long long int\000"
.LASF48:
	.ascii	"p0_mux_clr\000"
.LASF43:
	.ascii	"p3_mux_set\000"
.LASF4:
	.ascii	"short int\000"
.LASF27:
	.ascii	"p0_dir_clr\000"
.LASF61:
	.ascii	"out_pins\000"
.LASF70:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF22:
	.ascii	"p0_inp_state\000"
.LASF69:
	.ascii	"en_dis\000"
.LASF45:
	.ascii	"p3_mux_state\000"
.LASF49:
	.ascii	"p0_mux_state\000"
.LASF2:
	.ascii	"signed char\000"
.LASF8:
	.ascii	"p3_inp_state\000"
.LASF21:
	.ascii	"reserved1\000"
.LASF29:
	.ascii	"reserved2\000"
.LASF37:
	.ascii	"reserved3\000"
.LASF38:
	.ascii	"reserved4\000"
.LASF42:
	.ascii	"reserved5\000"
.LASF28:
	.ascii	"p0_dir_state\000"
.LASF34:
	.ascii	"p1_dir_set\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
