@echo off

rem Create the directory to store the binary in. If it the directory already
rem exists, nothing happens.
mkdir "C:\CS3000\fw_upgrade\bin\mainboard\"

rem Copy the binary from the Release folder to the newly created directory, over-
rem writing the file if it already exists.
rem NOTE: The %~dp0 indicates the folder in which this batch file resides.
copy %~dp0"\ARM Release\cs3000.bin" "C:\CS3000\fw_upgrade\bin\mainboard\cs3000.bin" /y

rem Start the CS3000 Firmware Upgrade Utility. Note the use of the Start command.
rem This executes the application in a separate window, immediately returning to
rem the batch file, effectively returning control back to Rowley CrossStudio when
rem this batch file is executed as part of the compilation process.
rem NOTE: The command will not execute properly if the quotes ("") between start
rem and the application are removed.
start "" "C:\CS3000\fw_upgrade\bin\FW_Upgrade.exe"
