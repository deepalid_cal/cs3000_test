/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*

SCHEMATIC REV D CHANGES WITH PROGRAMATIC IMPLICATIONS

1. Moved WDT_KICK (a GPIO output) from p2.13 to P0.22

2. Moved nTRIAC_BUS_RESET (a GPIO output) from P2.12 to P0.21

3. Moved RAIN_CLICK_micro (a GPIO input) from P0.6 to P1.0.

4. Moved FREEZE_CLICK_micro (a GPIO input) from P0.7 to P1.1





*/ 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/23/2015 rmd : Some interesting hardware facts:

// The TPMicro is using an LPC1763FBD100 (100 pin quad flat pak).
 
// We have 256K of Flash for program image. And 64K of SRAM.
 
// The SRAM is split into two 32K blocks. The AHB SRAM Banks 0 & 1 are 16K each and are
// arranged as a contiguous 32K block. This is the program variables SRAM space. And then
// there is a 32K block of SRAM that we devote entirely to the heap which is used for our
// partitioned memory pool.

// There are 3 uarts. Two of which are in use. One for the serial link to the main board.
// And one to drive the RS485 -M channel.










/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */



// 1/17/2013 rmd : ---- IMPORTANT NOTE ---- 'HARDWARE WRITE CACHING PROBLEM'

	// 11/30/2012 rmd : Due to internal race condition between an internal register write (they
	// are cached?) and execution time till we exit the ISR, if we leave the isr too quickly the
	// VIC can pick up another interrupt. We must clear the interrupt register in the peripheral
	// very first thing in the interrupt. To make sure the bits are cleared and have propagated
	// through to the VIC by the time we exit the ISR. Else we'll get another interrupt! Two
	// back to back interrupts that is. The second one being in error.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
1/16/2013 rmd : ---- IMPORTANT NOTE ---- 

SYSTICK - To save the use of a timer, I decided to piggy back on the FreeRTOS timer tick 
interrupt to run our trigger our ADC conversions. The timer runs and interrupts at a 4kHz 
rate. Every forth interrupt we steer into the FreeRTOS core. So the RTOS tick rate is 1kHz. 
*/ 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"

#include	"GpioPinChg.h"

#include	"hardwaremanager.h"

#include	"eeprom_map.h"

#include	"irrigation_manager.h"

#include	"tpmicro_comm_mngr.h"

#include	"iap.h"

#include	"wdt_and_powerfail.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void vSerialTestTask(void *pvParameters)
{
	/*
	UNS_32 currentlight = 0;
	UNS_32 targetlights;
	
	UNS_8 *ucp;
	
	// ----------	
	
	DATA_HANDLE dh_for_serial_test;
	dh_for_serial_test.dlen = 100; //we are going to store the serial number here for safe keeping.
	dh_for_serial_test.dptr = (UNS_8 *) GetMemBlk(  dh_for_serial_test.dlen,  portNO_DELAY );
	
	ucp = dh_for_serial_test.dptr;
	
	for( int i=0; i<100; i++ )
	{
		*ucp++ = i;
	}
	UNS_8 stations[5];
	
	UNS_8 light1;
	UNS_8 light2;
	
	srand(100);
	
	AddDataBlockToXmitQueue( UPORT_M1, dh_for_serial_test, TRUE, TRUE, TRUE );
	
	// ----------	
	
	// 6/25/2012 raf :  We dont start irrigation until our ADC task is ready, so we need to signal it here.
	if( xSemaphoreTake( g_xTaskInitMutex, portMAX_DELAY ) == pdTRUE )
	{
		// 8/15/2012 raf :  Set the init bit
		g_task_init_status.tasks.bits.testtask = 1;
	
		xSemaphoreGive(g_xTaskInitMutex);
	}
	*/
	
	// ----------	
	
	while( (true) )
	{
		// ----------
		
		vTaskDelay( MS_to_TICKS(5000) );
		
		// ----------	
	
		/*
		INT_COMMS_STRUCT	lmsg;

		// 11/15/2012 rmd : Queue request to send task info to the CS3000 screen.
		lmsg.command = TP_SEND_DEBUG_MSG_TO_MAIN;
	
		if( xQueueSendToBack( g_xTpMicroMsgQueue, &lmsg, portNO_DELAY ) != pdTRUE )
		{
			DEBUG_STOP( DS_TEST );
		}
		*/
			
		// ----------	
	
		continue;
	
		// ----------	

		/*
		stations[0] = (int)(((float)rand()/RAND_MAX) * 47) + 1;
	
		stations[1] = (int)(((float)rand()/RAND_MAX) * 47) + 1;
		stations[2] = (int)(((float)rand()/RAND_MAX) * 47) + 1;
		stations[3] = (int)(((float)rand()/RAND_MAX) * 47) + 1;
		stations[4] = (int)(((float)rand()/RAND_MAX) * 47) + 1;

		for(int i=0; i<5; i++)
		{
			if((stations[i] == 9) || (stations[i] == 20))
			{
				//stations[i]++;
			}
		}

		light1 = (int)(((float)rand()/RAND_MAX) * 4);
		light2 = (int)(((float)rand()/RAND_MAX) * 4);
		if(light1 == 4) light1 = 0;
		if(light2 == 4) light2 = 0;
		targetlights = 0;

		UNS_16 master_valves_to_turn_on = (int)(((float)rand()/RAND_MAX) * 2);
		if(master_valves_to_turn_on == 2) master_valves_to_turn_on = 1;

		UNS_16 pumps_to_turn_on = (int)(((float)rand()/RAND_MAX) * 2);
		if(pumps_to_turn_on == 2) pumps_to_turn_on = 1;
	
		//set_which_irrigation_peripherals_should_be_on(5, (UNS_8 *)&stations[0], targetlights, master_valves_to_turn_on, pumps_to_turn_on);
		//set_which_irrigation_peripherals_should_be_on(2, (UNS_8 *)&stations[0], targetlights, 0, 0);


		//set_which_irrigation_peripherals_should_be_on(0, (UNS_8 *)&stations[0], targetlights, 0, 0);
		//vTaskDelay(MS_to_TICKS(2000));
	
	
		set_which_irrigation_peripherals_should_be_on(2, (UNS_8 *)&stations[0], targetlights, master_valves_to_turn_on, pumps_to_turn_on);
	
		vTaskDelay(MS_to_TICKS(3000));
	
		set_which_irrigation_peripherals_should_be_on(3, (UNS_8 *)&stations[0], targetlights, master_valves_to_turn_on, pumps_to_turn_on);
	
		vTaskDelay(MS_to_TICKS(3000));
	
		set_which_irrigation_peripherals_should_be_on(4, (UNS_8 *)&stations[0], targetlights, master_valves_to_turn_on, pumps_to_turn_on);
	
		vTaskDelay(MS_to_TICKS(3000));
	
		// set_which_irrigation_peripherals_should_be_on(0, (UNS_8 *)&stations[0], targetlights, master_valves_to_turn_on, pumps_to_turn_on);
	
		//vTaskDelay(MS_to_TICKS(3000));
	
		continue;
	
	
	
		targetlights = 0x01020305;
		//turn on/off the stations the master wants to see on. stations_to_turn_on is a pointer into the message, dont free it yet!
		set_which_irrigation_peripherals_should_be_on(4, (UNS_8 *)&targetlights, 0xf, master_valves_to_turn_on, pumps_to_turn_on);
		vTaskDelay(MS_to_TICKS(1000));
	
	
		master_valves_to_turn_on = 0x00;
		pumps_to_turn_on = 0x00;
		targetlights = 0x06070809;
		//turn on/off the stations the master wants to see on. stations_to_turn_on is a pointer into the message, dont free it yet!
		set_which_irrigation_peripherals_should_be_on(4, (UNS_8 *)&targetlights, 0, master_valves_to_turn_on, pumps_to_turn_on);
	
	
		vTaskDelay(MS_to_TICKS(8000));
	
		continue;
	
	
	
		targetlights = 32;
		//targetlights = ((currentlight)<<24) + ((currentlight+1)<<16) + ((currentlight+2)<<8) + (currentlight+3);
		currentlight+=4;
		if(currentlight > 44) currentlight = 0;
		//set_which_stations_should_be_on(0x01, (UNS_8 *)&targetlights);
	
	
	
	
	
		vTaskDelay(MS_to_TICKS(5000));
	
		continue;
	
	
		// i know this can look scary when running through it, but its actually ok, freertos queues
		// take copies of the data being queued, and in that data is a pointer that we are making
		// here over and over again, which gets freed later.. so dont worry about it so much!
		for( int i=0; i< 6; i++ )
		{
		    serialNumber = i+1;
			 intMsg_s.command = TP_HANDLE_FLOW_UPDATES;
	
			fbup_s.poc_type = POC_TYPE_2_WIRE_BYPASS_DECODER;
			fbup_s.serialNumber = serialNumber;
			fbup_s.flowbufferdata[0] = count;
			if( count == 0xFF )
			{
				count = 0;
			}
			else
			{
				count++;
			}
			fbup_s.flowbufferdata[1] = count;
			if( count == 0xFF )
			{
				count = 0;
			}
			else
			{
				count++;
			}
			fbup_s.flowbufferdata[2] = count;
			if( count == 0xFB )
			{
				count = 0;
			}
			else
			{
				count++;
			}
	
	
			intMsg_s.dh.dlen = sizeof(FLOW_BUFFER_UPDATE_PACKET_s);
			intMsg_s.dh.dptr = (UNS_8 *) GetMemBlk(  intMsg_s.dh.dlen,  portMAX_DELAY );
	
			if( intMsg_s.dh.dptr == NULL )
			{
				flag_error(BIT_TPME_we_couldnt_get_a_suitable_heap_block);
			}
			else
			{
	
				memcpy(intMsg_s.dh.dptr, &fbup_s, sizeof(FLOW_BUFFER_UPDATE_PACKET_s));
	
				lstatus = xQueueSendToBack( xTpMicroMsgQueue, &intMsg_s, portMAX_DELAY );
	
				if( pdTRUE != lstatus )
				{
					DEBUG_STOP( RX_COMM + 1 );
				}
			}
	
		}

		if( targetlights == 0x02030100 )
		{
			targetlights = 0x04060705;
			set_which_stations_should_be_on(0x04, (UNS_8 *)&targetlights);
		}
		else if( targetlights == 0x04060705 )
		{
			targetlights = 0x08090a0b;
			set_which_stations_should_be_on(0x04, (UNS_8 *)&targetlights);
		}
		else if( targetlights == 0x08090a0b )
		{
			targetlights = 0x0c0d0e0f;
		 set_which_stations_should_be_on(0x04, (UNS_8 *)&targetlights);
		}
		else
		{
			targetlights = 0x02030100;
			set_which_stations_should_be_on(0x04, (UNS_8 *)&targetlights);
		}

		//targetlights = 0x08090a0b;
		// set_which_stations_should_be_on(0x04, (UNS_8 *)&targetlights);
	
	
	
		//  targetlights = 0x0f;
		//   set_which_stations_should_be_on(0x01, &targetlights);
		serialNumber = 0xBBCC;
		intMsg_s.command = TP_2_WIRE_POC_FLOW_BUFFER_UPDATE;
	
		intMsg_s.dh.dlen = sizeof(serialNumber) + sizeof(count); //we are going to store the serial number here for safe keeping.
		intMsg_s.dh.dptr = (UNS_8 *) GetMemBlk(  intMsg_s.dh.dlen,  portMAX_DELAY );
	
		memcpy(intMsg_s.dh.dptr, &serialNumber, sizeof(serialNumber));
		memcpy(intMsg_s.dh.dptr + sizeof(serialNumber) , &count, sizeof(count));
	
		lstatus = xQueueSendToBack( xTpMicroMsgQueue, &intMsg_s, portMAX_DELAY );
	
	
	
		serialNumber = 0xDDEE;
		intMsg_s.command = TP_2_WIRE_POC_FLOW_BUFFER_UPDATE;
	
		intMsg_s.dh.dlen = sizeof(serialNumber) + sizeof(count); //we are going to store the serial number here for safe keeping.
		intMsg_s.dh.dptr = (UNS_8 *) GetMemBlk(  intMsg_s.dh.dlen,  portMAX_DELAY );
	
	
		memcpy(intMsg_s.dh.dptr, &serialNumber, sizeof(serialNumber));
		memcpy(intMsg_s.dh.dptr + sizeof(serialNumber) , &count, sizeof(count));
	
		lstatus = xQueueSendToBack( xTpMicroMsgQueue, &intMsg_s, portMAX_DELAY );
	
	
	
		serialNumber = 0xFF11;
		intMsg_s.command = TP_2_WIRE_BYPASS_FLOW_BUFFER_UPDATE;
	
		intMsg_s.dh.dlen = sizeof(serialNumber) + 3*sizeof(count); //we are going to store the serial number here for safe keeping.
		intMsg_s.dh.dptr = (UNS_8 *) GetMemBlk(  intMsg_s.dh.dlen,  portMAX_DELAY );
	
		memcpy(intMsg_s.dh.dptr, &serialNumber, sizeof(serialNumber));
		memcpy(intMsg_s.dh.dptr + sizeof(serialNumber) , &count, sizeof(count));
		memcpy(intMsg_s.dh.dptr + sizeof(serialNumber) + sizeof(count) , &count, sizeof(count));
		memcpy(intMsg_s.dh.dptr + sizeof(serialNumber) + 2*sizeof(count), &count, sizeof(count));
	
		BOOL_32 lstatus = xQueueSendToBack( xTpMicroMsgQueue, &intMsg_s, portMAX_DELAY );
	
	
		for( int i=0; i<6; i++ )
		{
			if( uxQueueMessagesWaiting( xEEPromMsgQueue ) == 0 )
			{
	
				eeMsg.EE_address = ee_location;//EE_DATA_LOCATION_DUMMYTEST;
				eeMsg.action = EE_WRITE;
				eeMsg.dh.dlen = EE_DATA_SIZE_DUMMY_TESTS;
				eeMsg.dh.dptr = ( UNS_8 * ) GetMemBlk( eeMsg.dh.dlen,  portMAX_DELAY );
				if( eeMsg.dh.dptr == NULL )
				{
					flag_error( BIT_TPME_we_couldnt_get_a_suitable_heap_block );
					//TODO: do some error handling here
				}
				else
				{
					ucp = eeMsg.dh.dptr;
					for( int i=0; i<EE_DATA_SIZE_DUMMY_TESTS; i++ )
					{
						*ucp++ = i+data_offset;
					}
					BOOL_32 lstatus = xQueueSendToBack( xEEPromMsgQueue, &eeMsg, portMAX_DELAY );
					if( pdTRUE != lstatus )
					{
						DEBUG_STOP( RX_COMM + 1 );
					}
				}
	
				eeMsg.EE_address = ee_location;//EE_DATA_LOCATION_DUMMYTEST;
				eeMsg.action = EE_READ;
				eeMsg.dh.dlen = EE_DATA_SIZE_DUMMY_TESTS;
				BOOL_32 lstatus = xQueueSendToBack( xEEPromMsgQueue, &eeMsg, portMAX_DELAY );
				if( pdTRUE != lstatus )
				{
					DEBUG_STOP( RX_COMM + 1 );
				}
				vTaskDelay( MS_to_TICKS( 500 ) );
	
	
				ee_location++;
				if( ee_location >= SEEP_CAPACITY_BYTES - EE_DATA_SIZE_DUMMY_TESTS )
					ee_location = 0;
	
				data_offset+=2;
				if( data_offset >= 100 )
				{
					data_offset = 0;
				}
			}

			//wait for messages to get processed
			vTaskDelay( MS_to_TICKS( 2 ) );
		}

		*/

	}  // of while forever
	
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 12/4/2012 rmd : From observation this is the most main stack space seen used. Not much
// more to note about it.
#define MAIN_STACK_MAX_OBSERVED_SIZE	376

// ----------

// 9/11/2012 : These variables are created by the linker script. __stack_start__ is a
// variable at location bottom of the stack. The trickier one is __STACKSIZE__ which is
// defined in the compiler options, but it isnt stored in an address with data, it seems to
// have the data AS the address.  So to get the actual stack size, you have to use the
// address of __STACKSIZE__.
extern UNS_8	__stack_start__;

extern UNS_32	__STACKSIZE__;

// ----------

void check_main_stack_size()
{
	static UNS_32	greatest_stack_size = 0;

	UNS_32			current_stack_size;

	UNS_8			*ucp;
	
	CHAR			lstr[ 64 ];

	// 12/4/2012 rmd : Using the linker generated defines we know where the stack is. On the
	// C-M3 the stack grows DOWN. The __stack_start__ variable here represents the lowest stack
	// location in memory. So on the M3 this is the bottom of the stack. That is the unused
	// portion of the stack.
	ucp = (UNS_8 *)&__stack_start__;

	current_stack_size = (UNS_32)&__STACKSIZE__;

	// 12/4/2012 rmd : Start at the bottom and work up till we see there has been stack
	// activity. For time efficiency sakes jump 16 bytes at a time. An approximate to the
	// largest used stack is good enough.
	while( *ucp == 0xCC )
	{
		ucp += 16;

		current_stack_size -= 16;
	}

	if( current_stack_size > greatest_stack_size )
	{
		// 2/12/2013 rmd : Have seen this grow to just over 300 bytes.
		greatest_stack_size = current_stack_size;

		snprintf( lstr, sizeof(lstr), "Stack Use: %u bytes", greatest_stack_size );
		
		alert_message_to_tpmicro_pile( lstr );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
int main( void )
{
	/*
	// 5/17/2013 rmd : Alignment experiments

	volatile UNS_32	lsize __attribute__((__unused__));

	volatile UNS_8	*lptr __attribute__((__unused__));
	
	// 4/19/2013 rmd : To use the debugger and see the sizeof structures of interest.

	//lsize = sizeof( TPMICRO_DECODER_LIST_STRUCT );

	lsize = sizeof( TPMICRO_DECODER_INFO_STRUCT );
	
	lptr = (UNS_8*)&decoder_info;

	lptr = (UNS_8*)&decoder_info[ 0 ].ms_since_last_command_sent;
	lptr = (UNS_8*)&decoder_info[ 1 ].ms_since_last_command_sent;
	lptr = (UNS_8*)&decoder_info[ 2 ].ms_since_last_command_sent;
	lptr = (UNS_8*)&decoder_info[ 3 ].ms_since_last_command_sent;
	
	decoder_info[ 0 ].ms_since_last_command_sent = 0x7834;

	decoder_info[ 2 ].ms_since_last_command_sent = 0xAA34;
	*/

	// ----------

	/*
	volatile UNS_32	size_test __attribute__((__unused__));
	
	size_test = sizeof(whats_installed);
	
	*/

	// ----------

	// 12/9/2014 rmd : Counter roll-over experiments. Conclusion is a single roll-over is not a
	// concern to differencing math. Always produes the correct delta as long as all variables
	// are UNS_32.
	/*
	volatile UNS_32	new_value, last_value, delta __attribute((unused));
	
	last_value = 0xFFFFFFFE;

	new_value = 0x00000001;
	
	delta = new_value - last_value;
	*/
	
	// ----------
	
	// 2/12/2014 rmd : As a scope marker make LED2 pin an output so we can drive it. This gets
	// done again shortly but 
	//
	// 2/12/2014 rmd : If you want to blip an LED here run this function first to set up the
	// GPIO to do so. ISP still enters correctly even if this pin configuration is made.
	//
	//PINCFG_configure_all_tpmicro_pins_using_table();
	//TPM_LED2_ON;  // so fast hard to see on a scope!
	//TPM_LED2_OFF;

	// 2/12/2014 rmd : From the time the intentional WD timeout occurred till we are back here
	// restarting the program is a total of about 3 ms. Yes that's right 3 ms. We make this test
	// FIRST as we've learned the closer to the reset state we are the more likely our chance of
	// sucess entering ISP mode from our user code. This is because if we go to far into our
	// peripheral and core configuration something - did not figure out what - makes it so the
	// ISP will not run correctly.
	IAP_test_to_invoke_isp();
	
	// ----------
	
	//volatile UNS_32	lpconp_reg __attribute__((__unused__));
	//lpconp_reg = ((LPC_SC_TypeDef*)LPC_SC)->PCONP;

	// 11/14/2012 rmd : Power down ALL peripherals. Individually as needed peripheral blocks
	// must be re-powered.
	((LPC_SC_TypeDef*)LPC_SC)->PCONP = 0;
	
	// ----------

	// 11/14/2012 rmd : No particular reason but ensure the TRACE output is disabled. This line
	// was in the code base I started with. Not sure why. The reset value is to have the trace
	// functionality disabled. But it's little code so I'll leave it.
	((LPC_PINCON_TypeDef*)LPC_PINCON)->PINSEL10 = 0;
	
	// ----------

	reset_source_reg = ((LPC_SC_TypeDef*)LPC_SC)->RSID;

	// 7/28/2015 rmd : Clear all the bits so the next startup reflects the true reason and not
	// holding some residual reason. Say if power didn't fully cycle or in the case of a WDT.
	((LPC_SC_TypeDef*)LPC_SC)->RSID = 0x0F;
	
	// ----------
	
	// 1/9/2013 rmd : The CMSIS SystemCoreClock variable defaults to 100MHz. If the
	// pre-processor define FULL_SPEED is included in the Crossworks project the core speed will
	// be indeed 100MHz. If that define is not included the core speed will be 72MHz. We have
	// decided to back away from the rated maximum speed. Therefore not including the FULL_SPEED
	// define and set the core speed to 72MHz. And this function call updates the
	// SystemCoreClock variable to that. It is important to have an accurate SystemCoreClock
	// variable as some of the CMSIS functions that setup peripheral hardware use this
	// value.
	SystemCoreClockUpdate();
	
	// 4/24/2013 rmd : Because we have a basic NOP loop for a delay in the two_wire_io file if
	// the CPU clock changes let's help identify that. We do not change the clock rate during
	// execution so making this check here should suffice. UPDATE: we no longer have that NOP
	// loop but let's leave this check of the system clock rate.
	if( SystemCoreClock != 72000000 )
	{
		DEBUG_STOP();
	}
	
	// ----------
	
	// 2/6/2013 rmd : Because we are mirror the total stored data had better be less than half.
	// Plus some margin.
	if( EE_TOTAL_STORED_SIZE > ((EE_CHIP_TOTAL_MEMORY_SIZE/2) - 0x100) )
	{
		DEBUG_STOP();
	}

	// ----------

	// 3/10/2016 mjb : TODO Temporarily added the IFNDEF below to prevent the init of watchdog while debugging. This is to prevent the watchdog from firing while trying to 
	//debug especially while in a breakpoint.
#ifndef DEBUG
	init_watchdog_hardware();
#endif 
	
	enable_brown_out_interrupt();
	
	// ----------

	// 11/14/2012 rmd : Get 'em all done.
	PINCFG_configure_all_tpmicro_pins_using_table();
 	
	// ----------
	
	xTaskCreate( system_startup_task, (signed char*)"START", 150, NULL, PRIORITY_STARTUP_TASK_2, NULL );

	// ----------

	// Start the scheduler.
	vTaskStartScheduler();

	// ----------
	
	//  Will only get here if there was insufficient memory to create the idle
	//  task.  The idle task is created within vTaskStartScheduler().
	while( 1 );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	/* This function will get called if a task overflows its stack. */

	( void ) pxTask;
	( void ) pcTaskName;

	for( ;; );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 11/7/2013 rmd : If it isn't the debug version we are not collecting the extremely
// disruptive run time stats.
#ifdef SEND_RUNTIME_STATS

	void vConfigureTimerForRunTimeStats( void )
	{
		// 11/16/2012 rmd : Enable power to peripheral.
		CLKPWR_ConfigPPWR( CLKPWR_PCONP_PCTIM0, ENABLE );
		
		// 11/16/2012 rmd : Set clk divider to 4. So it is clocked at CCLK/4 (72mhz/4).
		CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_TIMER0, CLKPWR_PCLKSEL_CCLK_DIV_4 );
		
		// 11/16/2012 rmd : Timer is disabled from counting.
		((LPC_TIM_TypeDef*)LPC_TIM0)->TIMER_CONTROL_REG = 0;
	
		// 11/16/2012 rmd : Clear all the interrupts. Just for throughness.
		((LPC_TIM_TypeDef*)LPC_TIM0)->INTERRUPT_REG = 0x0000003F;
		
		// 11/16/2012 rmd : Set such that TC is incremented when the prescale counter matches the
		// prescale register. And the prescale counter counts on each PCLK rising edge.
		((LPC_TIM_TypeDef*)LPC_TIM0)->COUNT_CONTROL_REG = 0x00000000;
		
		((LPC_TIM_TypeDef*)LPC_TIM0)->TC = 0;
		((LPC_TIM_TypeDef*)LPC_TIM0)->PRESCALE_COUNTER = 0;
	
		// 11/16/2012 rmd : If we are running at 72MHz. Which we are. The CLK to this peripheral has
		// been set for a divide by 4 or 18MHz. If we want the counter to roll over in about 8 hours
		// we need to slow this down about 90 times. This should give us a count time of about 5
		// usec. when running at 72MHz.
		((LPC_TIM_TypeDef*)LPC_TIM0)->PRESCALE_MATCH_VALUE = 90;
	
	
		((LPC_TIM_TypeDef*)LPC_TIM0)->MATCH_CONTROL_REG = 0;
	
		((LPC_TIM_TypeDef*)LPC_TIM0)->MR0 = 0;
		((LPC_TIM_TypeDef*)LPC_TIM0)->MR1 = 0;
		((LPC_TIM_TypeDef*)LPC_TIM0)->MR2 = 0;
		((LPC_TIM_TypeDef*)LPC_TIM0)->MR3 = 0;
		
	
		((LPC_TIM_TypeDef*)LPC_TIM0)->CAPTURE_CONTROL_REG = 0;
	
		((LPC_TIM_TypeDef*)LPC_TIM0)->EXTERNAL_MATCH_REG = 0;
	
	
		// 11/16/2012 rmd : Start the timer!
		((LPC_TIM_TypeDef*)LPC_TIM0)->TIMER_CONTROL_REG = TIM_ENABLE;
	}

#endif

// ----------

void HardFault_Handler( void )
{
	__asm volatile
	(
		" tst lr, #4										\n"
		" ite eq											\n"
		" mrseq r0, msp										\n"
		" mrsne r0, psp										\n"
		" ldr r1, [r0, #24]									\n"
		" ldr r2, handler2_address_const						\n"
		" bx r2												\n"
		" handler2_address_const: .word pop_registers_from_fault_stack	\n"
	);
}

// ----------

void pop_registers_from_fault_stack( unsigned int * hardfault_args )
{
	unsigned int stacked_r0;
	unsigned int stacked_r1;
	unsigned int stacked_r2;
	unsigned int stacked_r3;
	unsigned int stacked_r12;
	unsigned int stacked_lr;
	unsigned int stacked_pc;
	unsigned int stacked_psr;

	stacked_r0 = ((unsigned long) hardfault_args[0]);
	stacked_r1 = ((unsigned long) hardfault_args[1]);
	stacked_r2 = ((unsigned long) hardfault_args[2]);
	stacked_r3 = ((unsigned long) hardfault_args[3]);

	stacked_r12 = ((unsigned long) hardfault_args[4]);
	stacked_lr = ((unsigned long) hardfault_args[5]);
	stacked_pc = ((unsigned long) hardfault_args[6]);
	stacked_psr = ((unsigned long) hardfault_args[7]);
	(void)stacked_r0;
    (void)stacked_r1;
    (void)stacked_r2;
    (void)stacked_r3;
    (void)stacked_r12;
    (void)stacked_lr;
    (void)stacked_pc;
    (void)stacked_psr;

	/* Inspect stacked_pc to locate the offending instruction. */
	for( ;; );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

