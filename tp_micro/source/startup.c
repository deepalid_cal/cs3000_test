/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"startup.h"


#include	"irrigation_manager.h"


#include	"ringbuffer.h"


#include	"SpiDrvr.h"

#include	"I2cDrvr.h"

#include	"two_wire_task.h"

#include	"GpioPinChg.h"

#include	"eeprom_map.h"


#if UART_TX_DMA != 0
	#include "x_uart.h"
#else
	#include "uart.h"
#endif

#include	"RxCommTask.h"

#include	"adc.h"

#include	"rit.h"

#include	"25lc640a.h"

#include	"tpmicro_comm_mngr.h"

#include	"two_wire_task.h"

#include	"lpc1763.h"

#include	"wdt_and_powerfail.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/10/2013 rmd : If more than this much time elapses since receiving a msg from main all
// the stations, mv's, and pump outputs will be de-energized. Including 2 Wire based
// solenoids.
#define	TP_MICRO_MAX_SECONDS_WITHOUT_A_MESSAGE_FROM_MAIN	(10)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/28/2015 rmd : Captured very early on during the startup sequence. Just seemed like the
// right thing to do even though probably could be read later where it is used. Is used in
// the startup task to send the restart reason alerts.
volatile UNS_32		reset_source_reg;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/12/2013 rmd : Used to block the ADChold off on creation of the final tasks until we
// have received a message from main.
xSemaphoreHandle    packet_rcvd_from_main_semaphore;

// ----------

////////////////////////////////
// System Semaphore Handles
////////////////////////////////
xSemaphoreHandle g_xEEPromMutex;

xSemaphoreHandle g_xIrrigationMutex;

xSemaphoreHandle g_xErrorLogMutex;

xSemaphoreHandle g_xHardwareProfileMutex;

xSemaphoreHandle g_xAdcSema;

xSemaphoreHandle g_xI2cSema;

// ----------

// 1/31/2013 rmd : Because we share data between the two_wire task and the internal_comms
// task we use these MUTEXES to protect.
xSemaphoreHandle    decoder_list_MUTEX;

xSemaphoreHandle    decoder_control_MUTEX;

xSemaphoreHandle    decoder_statistics_MUTEX;

// 5/21/2014 rmd : Used to protect both the terminal and any decoder flow count structure.
// When receiving the terminal poc counts as part of the 1 hertz processing. And when
// receiving flow count when delivered from a decoder. And when either is placed into the
// message to the main board.
xSemaphoreHandle	flow_count_MUTEX;


// 1/28/2016 rmd : To protect the moisture reading string while it is being written to by
// the two-wire receive task and read from by the comm_mngr task.
xSemaphoreHandle	moisture_reading_MUTEX;

// ----------

// 1/29/2013 rmd : there is one more small queue buried within the two_wire_io_control (not
// the two_wire_task_queue). Which since only handles a single message could be converted to
// a semaphore.
xQueueHandle    comm_mngr_task_queue;

xQueueHandle    g_xEEPromMsgQueue;

xQueueHandle    irrigation_task_queue;

xQueueHandle    two_wire_task_queue;

xTimerHandle    irrigation_task_timer;

xTimerHandle    last_msg_from_main_timer;

xTimerHandle    two_wire_activity_timer;

//////////////////////////////////////////////////////////////////////////////
//
// SYSTEM TASK TABLE
//
// Notes:
//
//   1. Setting the first column value to 1 or 0 enables or
//      disables creation of that task.
//
//   2. For some tasks, the Task Parameter is the UART port used for
//      output or managed by the task. See some specific cases below.
//
//////////////////////////////////////////////////////////////////////////////
const TASK_TABLE_STRUCT task_table[ NUMBER_OF_TABLE_TASKS ] =
{
	// 7/31/2015 rmd : DO NOT CHANGE THE ORDER OF THE TASKS WITHIN THIS TABLE. SEE THE CREATE
	// FUNCTION WHICH ROLLS THROUGH THIS TABLE. WE PEND BEFORE MAKING THE 5TH TASK.
	

	//	CREATE	TASK FUNCTION		TASK NAME	STACK DEPTH			TASK PARAM.		TASK PRIORITY						PTR TO TASK HANDLE
	//											(32-BIT WORDS)						(configMAX_PRIORITIES-1) is the		(can be set to NULL)
	//																				highest possible priority

	{   1,      vRingBfrScanTask,   "RScn0",		125,                (void*)UPORT_MAIN,      PRIORITY_RING_BUFFER_SCAN_TASK_4,		&task_last_execution_stamp[ 0 ] },

	{   1,      vRingBfrScanTask,   "RScn2",		125,                (void*)UPORT_M1,        PRIORITY_RING_BUFFER_SCAN_TASK_4,		&task_last_execution_stamp[ 1 ] },

	// ----------
	
	{   1,      vSerialTask,        "UART0",		125,                (void*)UPORT_MAIN,      PRIORITY_SERIAL_DRIVER_TASK_3,			&task_last_execution_stamp[ 2 ] },

	{   1,      vSerialTask,        "UART2",		125,                (void*)UPORT_M1,        PRIORITY_SERIAL_DRIVER_TASK_3,			&task_last_execution_stamp[ 3 ] },

	// ----------

	// 2/12/2013 rmd : With the exceptions of the TIMER task the ADC task is the highest
	// priority task in the system. So that the A-D conversions are handled immediately ahead of
	// any other task activity. Have seen this task use 636 bytes of stack space! Hence the 1000
	// byte stack.
	{   1,		adc_task,			"ADC",			250,				NULL,					PRIORITY_ADC_TASK_8,					&task_last_execution_stamp[ 4 ] },

	// ----------
	
	{   1,		two_wire_task,		"2Wire",		150,				NULL,					PRIORITY_2WIRE_TASK_7,					&task_last_execution_stamp[ 5 ] },

	// ----------

	// 3/8/2013 rmd : The irrigation task will execute till pending on it's queue. Part of the
	// startup code is to scan for the what's installed structure. And set a flag to transmit
	// that whats installed data with the first message to the main board. Have seen this task
	// use 436 bytes of stack space. Hence the 800 byte stack (200*4).
	{   1,		irrigation_task,	"irri",			200,				NULL,					PRIORITY_IRRI_TASK_6,					&task_last_execution_stamp[ 6 ] },

	// ----------

	// 3/26/2013 rmd : Have seen this task use 456 bytes of stack space. Hence the 800 byte
	// stack space.
	{   1,		comm_mngr_task,		"commmngr",		200,				NULL,					PRIORITY_COMM_MNGR_TASK_5,				&task_last_execution_stamp[ 7 ] },

	// ----------
	
	// 10/16/2012 rmd : Create the WDT task. A word on the WDT task priority. We want it to be a
	// low priority task. That way if it doesn't get time to run due to starvation the WDT
	// counter will expire and we'll reset. So we'll be able to detect starvation that way.
	{   1,		wdt_monitoring_task,	"WDT",		125,				NULL,					PRIORITY_WDT_TASK_1,					&task_last_execution_stamp[ 8 ] }

};


UNS_32	task_last_execution_stamp[ NUMBER_OF_TABLE_TASKS ];


UNS_32	startup_task_last_execution_stamp;

/* ---------------------------------------------------------- */

/////////////////////////////////////
//  The System Semaphore table
/////////////////////////////////////
static const SEMA_ENTRY_s Semaphore_Table[] =
{
	//	CREATE	SEMA TYPE			pxSemaphoreHandle
	{   1,      SEMA_BINARY_TYPE,   xSEMPTR &uart_control[ UPORT_MAIN ].xPacketSentSema },		// UART0 (Main)
	{   1,      SEMA_BINARY_TYPE,   xSEMPTR &uart_control[ UPORT_MAIN ].xRxDataAvailSema },		// UART0 (Main)

	{   1,      SEMA_BINARY_TYPE,   xSEMPTR &uart_control[ UPORT_M1 ].xPacketSentSema },		// UART2 (M1)
	{   1,      SEMA_BINARY_TYPE,   xSEMPTR &uart_control[ UPORT_M1 ].xRxDataAvailSema },		// UART2 (M1)

	{   1,      SEMA_BINARY_TYPE,   xSEMPTR &g_xI2cSema },										// I2C bus driver

	{   1,      SEMA_BINARY_TYPE,   xSEMPTR &g_xAdcSema },										// ADC access

	{   1,      SEMA_MUTEX_TYPE,    xSEMPTR &g_xEEPromMutex },									//

	{   1,      SEMA_MUTEX_TYPE,    xSEMPTR &g_xIrrigationMutex },								// Irrigation

	{   1,      SEMA_MUTEX_TYPE,    xSEMPTR &g_xErrorLogMutex },								// Errors

	{   1,      SEMA_MUTEX_TYPE,    xSEMPTR &g_xHardwareProfileMutex },							// Hardware profile

	{   1,      SEMA_MUTEX_TYPE,    xSEMPTR &decoder_list_MUTEX },

	{   1,      SEMA_MUTEX_TYPE,    xSEMPTR &decoder_control_MUTEX },

	{   1,      SEMA_MUTEX_TYPE,	xSEMPTR &decoder_statistics_MUTEX },

	{   1,      SEMA_MUTEX_TYPE,	xSEMPTR &flow_count_MUTEX },

	{   1,      SEMA_MUTEX_TYPE,	xSEMPTR &moisture_reading_MUTEX },

};

/* ---------------------------------------------------------- */

////////////////////////////////////////////////////////////////////////////////
//  System Queue Table
//
//	NOTE: The only FreeRTOS queues not created here are those created by the
//        InitMemBlkQueues() function in src/utils/MemBlkMgr.c
////////////////////////////////////////////////////////////////////////////////
static const QUEUE_ENTRY_s Queue_Table[] =
{
	//	CREATE	#ITEMS								ITEM_SIZE   							pxQueueHandle
	{   1,      COMM_MNGR_QUEUE_LENGTH,             sizeof(COMM_MNGR_QUEUE_STRUCT),         &comm_mngr_task_queue },

	{   1,      MAX_EEPROM_MESSAGES,                sizeof(EEPROM_MESSAGE_STRUCT),          &g_xEEPromMsgQueue},

	{   1,      MAX_IRRIGATION_MESSAGES,            sizeof(IRRIGATION_TASK_QUEUE_STRUCT),   &irrigation_task_queue},

	{   1,      UPORT_MAIN_MAX_QUEUED_XMIT_BLOCKS,  sizeof(XMIT_QUEUE_ITEM_STRUCT),         (void*)&(uart_control[ UPORT_MAIN ].xTxDataHandleQueue)},	// UART0 - Main

	{   1,      UPORT_M1_MAX_QUEUED_XMIT_BLOCKS,    sizeof(XMIT_QUEUE_ITEM_STRUCT),         (void*)&(uart_control[ UPORT_M1 ].xTxDataHandleQueue)},		// UART2 - M1

	{   1,      TWO_WIRE_TASK_QUEUE_SIZE,           sizeof(TWO_WIRE_TASK_QUEUE_STRUCT),     &two_wire_task_queue}
};

/* ---------------------------------------------------------- */

////////////////////////////////////////////////////////////////////////////////
//  System Timer Table
////////////////////////////////////////////////////////////////////////////////
static const TIMER_ENTRY_s Timer_Table[] =
{
	//  CREATE	Name			Timer Period 															AutoReload?	TimerID		CallBackFunction					Handle
	//							(Ticks)


	// 9/20/2013 rmd : This is the irrigation timer. It controls the stagger rate when valves
	// are being turn ON. And the hunt rate while hunting for a short. I've made this 500 ms to
	// give us more 60hertz cycles incorporated into the current measurement average. UPDATED
	// NOTES - DO NOT CHANGE this without taking into account the length of time needed to
	// develop the current measurement. If you shorten this you must assure that a FULL period
	// of readings while the output is ON is incorporated into the current measurement assigned
	// to the output.
	{   1,      "",     MS_to_TICKS( 600 ),                                                     FALSE,      NULL,       irri_timer_handler,                         &irrigation_task_timer },

	{   1,      "",     MS_to_TICKS( TP_MICRO_MAX_SECONDS_WITHOUT_A_MESSAGE_FROM_MAIN * 1000 ), FALSE,      NULL,       TIMER_no_msg_from_main_handler,             &last_msg_from_main_timer},

	{   1,      "",     MS_to_TICKS( TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS ),                      FALSE,      NULL,       TWO_WIRE_activity_timer_handler,            &two_wire_activity_timer},

	{   1,      "",     MS_to_TICKS( SPECIAL_ACTIVATION_OUTPUT_ON_DURATION ),                   FALSE,      NULL,       TWO_WIRE_special_activation_timer_handler,  &two_wire_bus_info.special_activation_timer},

	{   1,      "",     MS_to_TICKS( TWO_WIRE_CABLE_CHARGE_TIME_MS ),                           FALSE,      NULL,       TWO_WIRE_cable_charge_timer_handler,        &two_wire_bus_info.cable_charge_timer}

};

//-----------------------------------------------------------------------------
//  Function to create all of the system semaphores
//
//  Returns pdTRUE if error else pdFALSE if all created OK
//-----------------------------------------------------------------------------
BOOL_32 CreateSystemSemaphores()
{
	SEMA_ENTRY_s *pSemaEntry;

	UNS_32 EntryCount;

	BOOL_32 bError;

	xSemaphoreHandle xSemHndl;

	pSemaEntry = (SEMA_ENTRY_s *) &Semaphore_Table[0];
	EntryCount = sizeof( Semaphore_Table ) / sizeof( SEMA_ENTRY_s );

	bError = pdFALSE;

	while( ( bError == pdFALSE ) && EntryCount-- )
	{
		xSemHndl = NULL;

		if( pSemaEntry->bCreateSema )
		{
			/////////////////////////////////////
			//  This entry is to be processed
			/////////////////////////////////////
			switch( pSemaEntry->sema_type_e )
			{
				case SEMA_BINARY_TYPE:
					vSemaphoreCreateBinary( xSemHndl );
					break;

				case SEMA_MUTEX_TYPE:
					xSemHndl = xSemaphoreCreateMutex();
					break;

					// Unsupported types generate an error return
				case SEMA_COUNTING_TYPE:
				default:
					break;
			}

			// Check if the semaphore was created OK
			if( xSemHndl )
			{
				// Yes, the semaphore was created OK, so store the semaphore handle
				*(pSemaEntry->pxSemaHandle) = xSemHndl;

				if( pSemaEntry->sema_type_e == SEMA_BINARY_TYPE )
				{
					// If this is a binary semaphore, take the semaphore since it's initially set
					xSemaphoreTake( xSemHndl, portMAX_DELAY );
				}

				// Point to the next entry in the table
				pSemaEntry++;
			}
			else
			{
				bError = pdTRUE;
			}
		}
	}

	return(bError);
}


//-----------------------------------------------------------------------------
//  Function to create all system queues
//
//  Returns pdFALSE upon success, else pdTRUE if ERROR
//-----------------------------------------------------------------------------
BOOL_32 CreateSystemQueues()
{
	QUEUE_ENTRY_s   *pQueueEntry;
	UNS_32             EntryCount;
	xQueueHandle    xQHndl;
	BOOL_32         bError;

	EntryCount = sizeof( Queue_Table ) / sizeof( Queue_Table[0] );
	bError = pdFALSE;

	pQueueEntry = (QUEUE_ENTRY_s *) &Queue_Table[0];

	while( (bError == pdFALSE ) && EntryCount-- )
	{
		if( pQueueEntry->bCreateQueue )
		{
			xQHndl = xQueueCreate( pQueueEntry->uxMaxQueueItems, pQueueEntry->uxItemSize );

			// Check to see if the queue was created
			if( xQHndl )
			{
				// Yes, the queue was created OK, so store the queue handle
				*(pQueueEntry->pxQueueHandle) = xQHndl;

				//  Advance pointer into queue table
				pQueueEntry++;
			}
			else
			{
				bError = pdTRUE;
			}
		}
	}

	return bError;
}

/* ---------------------------------------------------------- */
void create_tasks( void )
{
	UNS_32				n;
	
	xTaskHandle			just_created_task;  // ptr to handle of new task 

	for( n=0; n<NUMBER_OF_TABLE_TASKS; n++ )
	{
		// 7/31/2015 rmd : The original behavior was to pend on this semaphore showing a packet
		// arrived BEFORE creating the ADC, TWO_WIRE, IRRIGATION, and COMM_MNGR tasks. So we mimic
		// that behavior here to avoid unwanted behaviors. Notice the WDT task is last. That is
		// fine. The watchdog timer is actually running right now. With a 10 second timeout. So
		// we're covered if the whole process hangs. There will be a restart.
		if( n == 4 )
		{
			// 3/7/2013 rmd : So at this point the following task exist: this startup task, the idle
			// task, the timer task, the 2 serial ring buffer tasks , and the 2 serial driver tasks.
		
			// 2/12/2013 rmd : Now wait here till we have seen a packet from the main board. Take a
			// system power up scenario, where both the main board and the TP Micro get powered up at
			// the same time as normal. The TPMicro, this board, boots up very quickly and could be
			// sending messages to main before main is ready. And actually if we do not wait here to
			// hear from the main board first, the first messages we send to the main board are missed.
			// Seeing a packet from the main board is our indication it is safe to come fully to life,
			// send messages to the main board, and complete the intialization by building out the rest
			// of the tasks.
		
			// 3/19/2013 rmd : Not only does this pend on this semaphore cause us to visually mark where
			// we are in the startup code, it importantly waits for a packet (single packet full
			// message) from the main board before we create the comm_mngr task. By processing this
			// message when the commmngr task is first created we capture the serial number of the main
			// board. And are then able to send messages back to him. Such as the restart reason alerts.
			TPM_LED2_ON;
		
			xSemaphoreTake( packet_rcvd_from_main_semaphore, portMAX_DELAY );
		
			TPM_LED2_OFF;
		}
		
		// ----------
		
		if( task_table[ n ].bCreateTask )
		{
			// Create the task for this entry
			xTaskCreate( task_table[ n ].pTaskFunc,

						 (signed char *)task_table[ n ].task_name,

						 (UNS_16)task_table[ n ].stack_depth,

						 // 7/31/2015 rmd : The parameter is the task_table item address. That way the task can get
						 // at both the info parameter and the execution time stamp pointer.
						 (void*)&task_table[ n ],

						 task_table[ n ].priority,

						 &just_created_task
					   );
		}
	}
}

/* ---------------------------------------------------------- */
BOOL_32 CreateSystemTimers(void)
{
	TIMER_ENTRY_s *pTimerEntry;
	UNS_32             EntryCount;
	xTimerHandle    xTHndl;
	BOOL_32 error;

	EntryCount = sizeof( Timer_Table ) / sizeof( Timer_Table[0] );
	error = pdFALSE;

	pTimerEntry = (TIMER_ENTRY_s *) &Timer_Table[0];

	while( (error == pdFALSE ) && EntryCount-- )
	{
		if( pTimerEntry->bCreateTimer )
		{
			xTHndl = xTimerCreate(  (signed char *)pTimerEntry->pcTimerName,
									pTimerEntry->xTimerPeriodInTicks,
									pTimerEntry->uxAutoReload,
									pTimerEntry->pvTimerID,
									pTimerEntry->pxCallbackFunction
								 );

			// Check to see if the queue was created
			if( xTHndl )
			{
				// Yes, the queue was created OK, so store the queue handle
				*(pTimerEntry->pxTimerHandle) = xTHndl;

				//  Advance pointer into queue table
				pTimerEntry++;
			}
			else
			{
				error = pdTRUE;
			}
		}
	}

	return error;

}

/* ---------------------------------------------------------- */
void init_ee_and_retrieve_all_parameters( void )
{
	// ----------

	// 11/14/2012 rmd : Enable power to the SSP0 peripheral block, set functionality, and
	// enable.
	serial_eeprom_init();

	// ----------

	// Check if our eeprom data has been initialized
	seep_rd( EE_LOCATION_TP_MICRO_EE_STATUS, (void*)&tp_micro_eeprom_status, EE_SIZE_TP_MICRO_EE_STATUS );

	if( (tp_micro_eeprom_status.eeprom_init_key == EE_INIT_KEY) && 
		(tp_micro_eeprom_status.version == EE_VERSION) &&
		(tp_micro_eeprom_status.total_stored_size == EE_TOTAL_STORED_SIZE) )
	{
		// ----------

		seep_rd( EE_LOCATION_WHATS_INSTALLED, (void*)&whats_installed, EE_SIZE_WHATS_INSTALLED );

		// ----------

		seep_rd( EE_LOCATION_OUTPUT_CURRENT, (void*)&output_current, EE_SIZE_OUTPUT_CURRENT );

		// ----------

		seep_rd( EE_LOCATION_WEATHER_INFO, (void*)&weather_info, EE_SIZE_WEATHER_INFO );

		// ----------

		seep_rd( EE_LOCATION_DECODER_LIST, (void*)&decoder_list, EE_SIZE_DECODER_LIST );

		// ----------
	}
	else
	{
		init_eeprom_to_default_values_and_mirror();
	}

	// ----------

	// 2/7/2013 rmd : Parts of what are stored MUST be initialized. Saved only because of the
	// convienence of their presence within the structure.
	init_weather_info();

	// ----------

	UNS_32  ddd;

	for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
	{
		// 4/15/2013 rmd : Almost for sure this flag is set in the decoder_info copy that is stored
		// in the EE. Because that is when the structure is saved ... at completion of the discovery
		// process. And as we send the discovered decoders to the main board we do not re-save the
		// decoder list. Hmm I think we should re-save it when we discover there are none to send to
		// the main board. But then we would do it again and again so to manage would require
		// another flag. This is just as easy!
		B_UNSET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_SEND_DISCOVERED_TO_MAIN_BOARD );

		// 5/1/2013 rmd : What about the sending decoder inoperative setting? The send decoder
		// inoperative flag behaves very much the same way as the send discovered flag. So we also
		// clear it here on startup. The point of sending the flag to the main board is to remove
		// the decoders station(s) from the irrigation list. And if we are starting up the
		// irrigation list would be empty? And sending the flag to a station in the list will cause
		// a change of state for us that we will attempt to process except that the decoder
		// inoperative flag will still be set. So we'll undo the commands to execute. Which is what
		// we want. When the initial determination of the inoperative decoder was made we powered
		// down the 2-wire bus to ensure the decoder indeed has it's outputs OFF.
		B_UNSET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_SEND_DECODER_INOPERATIVE_TO_MAIN_BOARD );
	}
}

/* ---------------------------------------------------------- */
void system_startup_task( void *pvParameters )
{
	// 7/23/2015 rmd : Create the WDT task. So we get the watchdog counter up and running. To
	// cover any actual coding errors, exceptions, etc. Also remember the watchdog needs to be
	// running to support the powerfail task. When the voltage threshold pwr fail interrupt
	// fires we count on the WDT to restart us in a brown out condition.
	//
	// 7/23/2015 rmd : A word on the WDT task priority. We want it to eventually be a low
	// priority task. That way if it doesn't get time to run due to starvation the WDT counter
	// will expire and we'll reset. So we'll be able to detect starvation that way. HOWEVER we
	// want the WDT task to run now till it hits it's delay. This way we know we've got the WDT
	// hardware intialized. So we create it with a high priority and then lower it after all the
	// other tasks have been created.
	//xTaskCreate( wdt_monitoring_task, (signed char *) "WDT", (0x2000 / 4), NULL, (configMAX_PRIORITIES-1), &wdt_task_handle );

	// 10/16/2012 rmd : At this point we KNOW the wdt task has run till blocking on its 500ms
	// delay (cause we created the task at the highest possible priority). And there is a 10
	// second watchdog timer over our heads. So now set the WDT task to it's final desired
	// priority level 1. Which is is the same as the flash file task priority level. And during
	// large file writes to flash the RTOS should time slice and allow the wdt task to run and
	// service the wdt hardware.
	//vTaskPrioritySet( wdt_task_handle, PRIORITY_LEVEL_WDT_TASK_1 );

	// ----------

	if( InitMemBlkQueues() == pdTRUE )
	{
		DEBUG_STOP();
	}

	// ----------

	if( CreateSystemSemaphores() == pdTRUE )
	{
		DEBUG_STOP();
	}

	// ----------

	if( CreateSystemQueues() == pdTRUE )
	{
		DEBUG_STOP();
	}

	// ----------

	if( CreateSystemTimers() == pdTRUE )
	{
		DEBUG_STOP();
	}

	// ----------

	// 2/7/2013 rmd : 

	// Initialize the I2C driver
	initI2cDrvr();

	// ----------
	// The Uarts cant be initialized until we know what the baudrate should be, which will be
	// contained in eeprom.
	init_ee_and_retrieve_all_parameters();

	// ----------

	// 2/12/2013 rmd : Create this semaphore before we create the serial tasks. It is the ring
	// buffer task for the main port that will give the semaphore. When a message from main
	// arrives.
	vSemaphoreCreateBinary( packet_rcvd_from_main_semaphore );

	// Take the semaphore once as it is created signaled!
	xSemaphoreTake( packet_rcvd_from_main_semaphore, 0 );

	// ----------

	// 2/6/2013 rmd : The startup task is the lowest priority task with the exception of the WDT
	// task. So as each additional task is created it runs till it blocks.
	create_tasks();
	
	// ----------

	// 3/8/2013 rmd : Okay the main board has sent something to us. AND WE PROCESSED it when the
	// comm_mngr_task started up (see create_tasks function). And therefore have learned a 'to'
	// serial number. So we can now send him serial traffic (ie alerts).
	
	// ----------
	
	// 7/30/2015 rmd : Analyze why we restarted. Powerfailure, brown-out, watchdog, etc. This is
	// the function that sends the FIRST alert lines to the main board.
	init_restart_info();
	
	// ----------

	// 1/15/2013 rmd : Now that ALL the tasks have been created and are ready for action, bring
	// up some more of the hardware that feeds the tasks.
	init_gpio_pin_change_interrupts();

	// 1/15/2013 rmd : And the Repetitive Interrupt Timer. Which ticks along at 200ms posting to
	// various tasks as time passes. Specifically each 400ms to the irrigation task, once per
	// second to read accumulated pin change counts. And more. See RIT isr for more details.
	init_RIT_For_Counters();

	// ----------

	// 2/7/2013 rmd : New job for the startup task! Handle the eeprom activity. And the triac
	// WDT transitioning. And the OS watchdog monitoring. Remember our OS task priority is just
	// above IDLE at priority 1.

	EEPROM_MESSAGE_STRUCT   eeqm;

	static UNS_32   runtime_stats_counter, led_blink_counter;

	runtime_stats_counter = 0;

	led_blink_counter = 0;

	while( (true) )
	{
		startup_task_last_execution_stamp = xTaskGetTickCount();
		
		// ----------

		// 2/12/2013 rmd : Wait only 500ms to support the WDT kick needed to keep the triacs ON.
		// Also works in the task watchdog scheme.
		if( xQueueReceive( g_xEEPromMsgQueue, (void *) &eeqm, MS_to_TICKS(500) ) )
		{
			switch( eeqm.cmd )
			{
				case EE_ACTION_WRITE:
					if( xSemaphoreTake( g_xEEPromMutex, portMAX_DELAY ) == pdTRUE )
					{
						seep_wr( eeqm.EE_address, eeqm.RAM_address, eeqm.length );

						// 2/6/2013 rmd : We always update the mirror copy.
						mirror_eeprom( eeqm.EE_address, eeqm.length );

						xSemaphoreGive( g_xEEPromMutex );
					}

					break;

				case EE_ACTION_CHECK_MIRROR:
					if( xSemaphoreTake( g_xEEPromMutex, portMAX_DELAY ) == pdTRUE )
					{
						if(check_eeprom_mirror() == FALSE)
						{
							flag_error( BIT_TPME_eeprom_mirror_didnt_match );

							pre_defined_alert_to_main( TPALERT_our_eeprom_mirro_didnt_match_our_critical_section);

							init_eeprom_to_default_values_and_mirror();
						}
						xSemaphoreGive( g_xEEPromMutex );
					}
					break;

			}

		}

		// ----------

		#ifdef DEBUG

			// 1/27/2016 rmd : If there was a application level watchdog timeout make an alert line.
			// During DEBUG mode the watchdog hard reset is suppressed.
			if( alert_main_of_an_watchdog_timeout )
			{
				alert_message_to_tpmicro_pile( "TPMICRO WATCHDOG TIMEOUT" );
				
				alert_main_of_an_watchdog_timeout = (false);
			}
			
		#endif
		
		// ----------

		// 2/12/2013 rmd : Perform the WDT kick outside of the queue processing if statement. Not
		// even in a else part of it. Because if we were to get a queue message just before the
		// 500ms mark we would process the EEPROM action then go wait another 500ms. Potentially for
		// a total of 1000ms which is beyond the TPS3823 limit.
		//
		// 11/7/2012 rmd : Once per 500ms we kick the triac output watchdog circuit. The WDI input
		// to the TPS3823 needs to transition at least once every 900ms. Though it needs only a
		// transition we are going to make a pulse. Which makes the logic a bit simpler. We delay
		// between the transitions to make sure we have a good clean fully rising edge. And you
		// can't just delay 1 tick as that may result in a 0 to 1 tick delay.
		TPM_TRIAC_WDT_KICK_HIGH;

		vTaskDelay( 2 );

		TPM_TRIAC_WDT_KICK_LOW;

		// ----------

		// 2/12/2013 rmd : Blink the LED at an approx 1 hertz rate.
		if( led_blink_counter < 1 )
		{
			led_blink_counter += 1;
		}
		else
		{
			TPM_LED0_ON;

			vTaskDelay( MS_to_TICKS( 25 ) );  // 10ms just barely see it

			TPM_LED0_OFF;

			led_blink_counter = 0;
		}

		// ----------

		check_main_stack_size();

		// ----------

		// 1/21/2013 rmd : Send runtime stats once every 5 seconds - approximately.
		if( runtime_stats_counter < 9 )
		{
			runtime_stats_counter += 1;
		}
		else
		{
			runtime_stats_counter = 0;

			// ----------

			// 11/7/2013 rmd : Don't show for the release version. Too disruptive.
#ifdef SEND_RUNTIME_STATS

			// 11/7/2013 rmd : Because the run-time stats consume quite a few memory blocks (one for
			// each display line) and a two-wire activity such as clearing all the decoder stats for 70
			// decoders also produces its own line, we supress the task stats as a matter of caution.
			if( !two_wire_bus_info.task_is_busy )
			{
				vTaskGetRunTimeStats( NULL );

			}

#endif
		}

	}  // of task while( (true) )
	
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

