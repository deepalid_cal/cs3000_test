/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"weather_manager.h"

#include	"gpiopinchg.h"

#include	"hardwaremanager.h"

#include	"tpmicro_comm_mngr.h"

#include	"eeprom_map.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


WEATHER_INFO_STRUCT		weather_info;


UNS_32		et_gage_pulse_deltas[ 10 ];

UNS_32		et_gage_pulse_index;

BOOL_32		et_gage_have_received_a_pulse;


UNS_32		wind_pulses_per_minute[ MAX_WIND_SAMPLE_PERIOD_MINUTES ];

UNS_32		wind_pulses_per_minute_index;

// 10/13/2014 rmd : Used to know we have been running for at least the length of time we are
// going to average the wind over. So that following a power failure we don't "resume"
// irrigation in error because the initial one-minute wind speed array contains all zeros!
BOOL_32		wind_index_has_wrapped;

UNS_32		wind_timer_seconds;

UNS_32		wind_speed_mph;

UNS_32		wind_speed_mph;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void __init_etgage_pulse_history( void )
{
	memset( &et_gage_pulse_deltas, 0x00, sizeof( et_gage_pulse_deltas ) );
	
	et_gage_pulse_index = 0;
	
	et_gage_have_received_a_pulse = (false);
}

/* ---------------------------------------------------------- */
extern void init_weather_info( void )
{
	// 2/7/2013 rmd : Called at each startup after the structure is read out from the eeprom.
	
	weather_info.rain_bucket_counts = 0;
	
	weather_info.etgage_counts = 0;

	weather_info.rain_clik_count_to_opposing_state = 0;
	
	weather_info.freeze_clik_count_to_opposing_state = 0;
	
	// 2/7/2013 rmd : Always reset on startup. Note this does not clear the run away gage
	// setting. That is only cleared via a message from the main board.
	__init_etgage_pulse_history();
	
	// ----------

	memset( &wind_pulses_per_minute, 0x00, sizeof( wind_pulses_per_minute ) );
	
	wind_pulses_per_minute_index = 0;
	
	wind_index_has_wrapped = (false);
	
	wind_timer_seconds = 0;
	
	wind_speed_mph = 0;
}

/* ---------------------------------------------------------- */
extern void clear_run_away_gage_state( void )
{
	weather_info.run_away_gage = (false);

	__init_etgage_pulse_history();
	
	// ----------
	
	// 2/7/2013 rmd : Save the weather info structure.
	EEPROM_MESSAGE_STRUCT	eeqm;

	eeqm.cmd = EE_ACTION_WRITE;

	eeqm.EE_address = EE_LOCATION_WEATHER_INFO;

	eeqm.RAM_address = (UNS_8*)&weather_info;

	eeqm.length = EE_SIZE_WEATHER_INFO;
	
	if( xQueueSendToBack( g_xEEPromMsgQueue, &eeqm, portNO_DELAY ) != pdTRUE )
	{
		flag_error( BIT_TPME_queue_full );
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called at a 1 hertz rate as part of the polling of the rain clik input.
    Only called if both the weather terminal and weather card are installed.
	
	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITIES

	@EXECUTED Within the context of the Internal Messenger Task.

	@RETURN Returns (true) if there is a state change.
*/ 
static BOOL_32 evaluate_rain_clik_input( void )
{
	BOOL_32		rv;
	
	rv = (false);
	
	if( weather_info.rain_clik_shows_rain == (false) )
	{
		// 1/10/2013 rmd : So we think it has not rained.
		if( TPM_RAIN_CLIK_SHOWS_RAIN )
		{
			if( weather_info.rain_clik_count_to_opposing_state < RAIN_CLIK_DEBOUNCE_COUNT )
			{
				weather_info.rain_clik_count_to_opposing_state += 1;
			}
			else
			{
				weather_info.rain_clik_shows_rain = (true);
				
				weather_info.rain_clik_count_to_opposing_state = 0;
				
				// 2/7/2013 rmd : State change.
				rv = (true);

				pre_defined_alert_to_main( TPALERT_it_has_started_to_rain );
			}
		}
		else
		{
			weather_info.rain_clik_count_to_opposing_state = 0;
		}
	}
	else
	{
		// 1/10/2013 rmd : So we think it has rained.
		if( TPM_RAIN_CLIK_DRY )
		{
			if( weather_info.rain_clik_count_to_opposing_state < RAIN_CLIK_DEBOUNCE_COUNT )
			{
				weather_info.rain_clik_count_to_opposing_state += 1;
			}
			else
			{
				weather_info.rain_clik_shows_rain = (false);
				
				weather_info.rain_clik_count_to_opposing_state = 0;

				// 2/7/2013 rmd : State change.
				rv = (true);

				pre_defined_alert_to_main( TPALERT_it_is_no_longer_raining );
			}
		}
		else
		{
			weather_info.rain_clik_count_to_opposing_state = 0;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called at a 1 hertz rate as part of the polling of the freeze clik input.
    Only called if both the weather terminal and weather card are installed.
	
	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITIES

	@EXECUTED Within the context of the Internal Messenger Task.

	@RETURN Returns (true) if there is a state change.
*/ 
static BOOL_32 evaluate_freeze_clik_input( void )
{
	BOOL_32		rv;
	
	rv = (false);
	
	if( weather_info.freeze_clik_shows_it_is_freezing == (false) )
	{
		// 1/10/2013 rmd : So we think it isn't freezing.
		if( TPM_FREEZE_CLIK_SHOWS_FREEZE )
		{
			if( weather_info.freeze_clik_count_to_opposing_state < FREEZE_CLIK_DEBOUNCE_COUNT )
			{
				weather_info.freeze_clik_count_to_opposing_state += 1;
			}
			else
			{
				weather_info.freeze_clik_shows_it_is_freezing = (true);
				
				weather_info.freeze_clik_count_to_opposing_state = 0;

				// 2/7/2013 rmd : State change.
				rv = (true);

				pre_defined_alert_to_main( TPALERT_freeze_conditions_are_present );
			}
		}
		else
		{
			weather_info.freeze_clik_count_to_opposing_state = 0;
		}
	}
	else
	{
		// 1/10/2013 rmd : So we think it is freezing.
		if( TPM_FREEZE_CLIK_WARM )
		{
			if( weather_info.freeze_clik_count_to_opposing_state < FREEZE_CLIK_DEBOUNCE_COUNT )
			{
				weather_info.freeze_clik_count_to_opposing_state += 1;
			}
			else
			{
				weather_info.freeze_clik_shows_it_is_freezing = (false);
				
				weather_info.freeze_clik_count_to_opposing_state = 0;

				// 2/7/2013 rmd : State change.
				rv = (true);

				pre_defined_alert_to_main( TPALERT_freeze_conditions_are_over );
			}
		}
		else
		{
			weather_info.freeze_clik_count_to_opposing_state = 0;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The 1Hz repetitive interrupt timer posts a queue command to the Internal
    Messenger Task. The result of that queue command is to call this function.
	
	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITIES

	@EXECUTED Within the context of the Internal Messenger Task.

	@RETURN (none)
*/ 
extern void WEATHER_one_hertz_update( PORT_ZERO_COUNTS_STRUCT* const paccumulators_ptr )
{
	// ----------

	// 1/10/2013 rmd : If both the weather card and terminal are not installed the managed
	// devices go to their passive state and remain in that state. Therefore it is important
	// that before we execute the code here and set our states we had a firm grip on the
	// installed cards and terminals. ADDITIONALLY for the case of wind, we must make sure we
	// have read the previously attained wind state from the EE rom. As that is to be preserved
	// through power failures.

	// ----------

	UNS_32	interval_in_minutes;

	UNS_32	pulseTotalDuringInterval;

	UNS_32	windSpeedForInterval;

	BOOL_32	weather_state_change;  // should we save to ee
	
	UNS_32	iii, summing_index;
	
	// ----------

	weather_state_change = (false);	
	
	// ----------

	// 1/10/2013 rmd : Only if the weather card and terminal are present do we evaluate.
	// Otherwise device states are set to their passive condition.
	if( whats_installed.weather_card_present && whats_installed.weather_terminal_present )
	{
		// ----------
		
		// 1/10/2013 rmd : Once per second evaluate and update the state of the freeze and rain clik
		// devices.
		if( evaluate_rain_clik_input() )
		{
			weather_state_change = (true);  // trigger eeprom save
		}

		
		if( evaluate_freeze_clik_input() )
		{
			weather_state_change = (true);  // trigger eeprom save
		}
		
		// ----------

		// 5/22/2014 rmd : Grab the latest one second rain bucket counts. No need to zero this count
		//as that is done in the rit interrupt! 
		weather_info.rain_bucket_counts += (paccumulators_ptr->RAIN_BUCKET_CNTS);
		
		// ----------

		// 10/13/2014 rmd : To prevent roll-over concerns reset on the hour. The subsequent minute
		// boundary detection continues to work even though we reset the counter at the hour mark.
		wind_timer_seconds += 1;

		if( wind_timer_seconds >= 3600 )
		{
			wind_timer_seconds = 0;
		}
		
		// ----------

		// 7/25/2012 raf :  We need to calculate wind speed.  We have pulses/second to work with and
		// we also know that the gage produces 200 pulses/min per mph.  1mph = 200p/m  therefore X
		// mph = (pulses measured in 1 second)*60s/m / (200p/m)   We need to calculate 1s wind speed
		// for display and also wind speeds for our irrigation pause and resume parameters. The max
		// those paramaters can be is set in MAX_WIND_SAMPLE_PERIOD_MINUTES.
		//
		// 10/13/2014 rmd : The math might be a bit confusing but it is correct. Divide the pulse
		// count by 200/60 to get the MPH. Or multiply the HZ by 60/200. Which is to say multiple
		// the hz by (3/10). Desireable to use integer math so multiply first by the three and then
		// divide by the ten.
		wind_speed_mph = (paccumulators_ptr->WIND_GAGE_CNTS * 3) / 10; //(60/200)

		// 7/25/2012 raf :  Add the pulses to the running total for this weather minute
		wind_pulses_per_minute[ wind_pulses_per_minute_index ] += paccumulators_ptr->WIND_GAGE_CNTS;

		// ----------

		// 7/25/2012 raf :  If we have passed a minute boundary, check pause and resume and increment our index
		if( (wind_timer_seconds % 60) == 0 )
		{
			if( weather_info.wind_paused )
			{
				// 7/25/2012 raf :  we are paused right now, so load up how long we need to have low wind to resume
				interval_in_minutes = weather_info.wss.wind_resume_minutes;
			}
			else
			{
				// 7/25/2012 raf :  we are irrigating right now, so load up how long we need to have high wind to pause
				interval_in_minutes = weather_info.wss.wind_pause_minutes;
			}

			// ----------

			// 10/13/2014 rmd : Only compute and make pause and resume decisions if we've been gathering
			// live data for at least as long as the interval we are interested in averaging over.
			if( (wind_pulses_per_minute_index >= interval_in_minutes) || wind_index_has_wrapped )
			{
				pulseTotalDuringInterval = 0;
	
				summing_index = wind_pulses_per_minute_index;
	
				for( iii=0; iii<interval_in_minutes; iii++ )
				{
					// 10/13/2014 rmd : Add in the totals for the minute periods so we can build the average
					// over the desired interval.
					pulseTotalDuringInterval += wind_pulses_per_minute[ summing_index ];
					
					if( summing_index > 0 )
					{
						summing_index -= 1;	
					}
					else
					{
						// 10/13/2014 rmd : Set to the last entry in the array.
						summing_index = (MAX_WIND_SAMPLE_PERIOD_MINUTES - 1);
					}
				}
				
				windSpeedForInterval = pulseTotalDuringInterval/(200*interval_in_minutes);
	
				// ----------
				
				if( weather_info.wind_paused )
				{
					if( windSpeedForInterval < weather_info.wss.wind_resume_mph )
					{
						weather_info.wind_paused = (false);
	
						weather_state_change = (true);  // trigger eeprom save
					}
				}
				else
				{
					if( windSpeedForInterval > weather_info.wss.wind_pause_mph )
					{
						weather_info.wind_paused = (true);
	
						weather_state_change = (true);  // trigger eeprom save
					}
				}
			}

			// ----------
			
			// 10/13/2014 rmd : Now move the minute index accumulator slot to the next one and check for
			// wrap around.
			wind_pulses_per_minute_index++;

			if( wind_pulses_per_minute_index >= MAX_WIND_SAMPLE_PERIOD_MINUTES )
			{
				wind_pulses_per_minute_index = 0;
				
				// 10/13/2014 rmd : Used to tell how long we've been running.
				wind_index_has_wrapped = (true);
			}
			
			// 10/15/2014 rmd : And ZERO out the new slot we are accumulating into to start the new
			// minute. Must do this else slots will accumulate forever!
			wind_pulses_per_minute[ wind_pulses_per_minute_index ] = 0;
		}

		// ----------
		
		// 2/20/2013 rmd : The interrupt driven gage pulse count is looked at as if it is either 0
		// or not 0. Meaning only interrupt generated pulse per second is counted. No matter how
		// many are racked up during the one second interval. Furthermore we only allow one pulse
		// per 30 seconds via the processing here.
		if( paccumulators_ptr->ET_GAGE_CNTS )
		{
			// 2/7/2013 rmd : There are only TWO times this state exists (true). First after power up.
			// And second after a run away gage state has been cleared. In both cases the full pulse
			// delta array has been zeroed. And the pulse index has been restarted. This 'first' pulse
			// mechanism allows us to get a pulse immediately after boot without waiting the 30 seconds.
			// Also helps us to start over the detection for a run away gage. Waiting the 30 seconds is
			// probably acceptable but raf had this implemented so I left it.
			if( !et_gage_have_received_a_pulse )
			{
				weather_info.etgage_counts += 1;

				et_gage_have_received_a_pulse = (true);

				et_gage_pulse_index += 1;
			}
			else
			if( et_gage_pulse_deltas[ et_gage_pulse_index ] >= 30 )
			{
				// 8/15/2012 raf :  we store our total time in between 10 pulses, so we know if its runaway or not
				UNS_32 totalDeltaFor10Pulses = 0;

				for( int i=0; i<10; i++ )
				{
					//first check individual result, if its over 20 mins, there cant be 20 min 10 pulse error
					if( et_gage_pulse_deltas[ i ] < 1200 )
					{
						// 7/19/2012 raf :  if we have a 0 here, it means we havent gotten enought data yet, so let it keep filling up
						if( et_gage_pulse_deltas[ i ] == 0 )
						{
							break;
						}

						totalDeltaFor10Pulses += et_gage_pulse_deltas[ i ];

						if(totalDeltaFor10Pulses > 1200)
						{
							//No error, possible, break
							break;
						}
					}
					else
					{
						// 7/19/2012 raf :  runaway gauge not possible, break
						break;
					}

					// 7/19/2012 raf : our last stop, check if its less than 1200 (20 mins), if so, flag runaway
					// gauge!
					if( i == 9 )
					{
						if( totalDeltaFor10Pulses <= 1200 )
						{
							if( !weather_info.run_away_gage )
							{
								weather_info.run_away_gage = (true);
								
								weather_state_change = (true);  // trigger eeprom save
								
								pre_defined_alert_to_main( TPALERT_we_have_runaway_et_gage_conditions );
							}
						}
					}
				}
				
				weather_info.etgage_counts += 1;

				//Now increment our index
				if( et_gage_pulse_index == 9 )
				{
					et_gage_pulse_index = 0;
				}
				else
				{
					et_gage_pulse_index++;
				}

				et_gage_pulse_deltas[ et_gage_pulse_index ] = 0;
			}
			else
			{
				// 7/19/2012 raf :  Increase the delta counter
				et_gage_pulse_deltas[ et_gage_pulse_index ]++;
			}
		}
		else
		{
			et_gage_pulse_deltas[ et_gage_pulse_index ]++;
		}

	}

	// ----------

	if( weather_state_change )
	{
		// 2/7/2013 rmd : Save the weather info structure.
		EEPROM_MESSAGE_STRUCT	eeqm;
	
		eeqm.cmd = EE_ACTION_WRITE;
	
		eeqm.EE_address = EE_LOCATION_WEATHER_INFO;
	
		eeqm.RAM_address = (UNS_8*)&weather_info;

		eeqm.length = EE_SIZE_WEATHER_INFO;
		
		if( xQueueSendToBack( g_xEEPromMsgQueue, &eeqm, portNO_DELAY ) != pdTRUE )
		{
			flag_error( BIT_TPME_queue_full );
		}
	}

	// ----------
	
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

