/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_WEATHERMANAGER_H
#define _INC_WEATHERMANAGER_H

/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"

#include	"gpiopinchg.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define MAX_WIND_SAMPLE_PERIOD_MINUTES		(30)


// 1/10/2013 rmd : These debounce counts are used to require stability in the rain and
// freeze clik inputs before switching states.

#define	RAIN_CLIK_DEBOUNCE_COUNT		(3)

#define	FREEZE_CLIK_DEBOUNCE_COUNT		(3)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// ----------

	// 2/7/2013 rmd : Should be zeroed on startup. Saved to the EE but only by virtue of being
	// within this structure.
	UNS_32		rain_bucket_counts;

	// ----------

	// 2/7/2013 rmd : Should be zeroed on startup. Saved to the EE but only by virtue of being
	// within this structure.
	UNS_32		etgage_counts;

	// 2/6/2013 rmd : Required to be saved to eeprom. Retained through a power failure. Latches
	// true when more than 10 pulse come in within a 20 minute period. Once latched must be
	// cleared via a message from the main board.
	BOOL_32		run_away_gage;

	// ----------

	// 2/7/2013 rmd : Should be zeroed on startup. Saved to the EE but only by virtue of being
	// within this structure. When in one state or the other, that is raining or not raining, we
	// like to see 3 consecutive reads in the opposite state before switching states. Remember
	// this input is polled at a 1 hertz rate.
	UNS_32		rain_clik_count_to_opposing_state;

	// 2/6/2013 rmd : Required to be saved to eeprom. Retained through a power failure. The
	// typical rain clik device gets wet during the rain and shows that is it raining by
	// floating the input to our weather card terminal rain clik input. Additionally for
	// sometime afterwards, while the device dries out, it shows as if there is rain. This
	// period of drying may be for hours or even in some cases days.
	BOOL_32		rain_clik_shows_rain;

	// ----------

	// 2/7/2013 rmd : Should be zeroed on startup. Saved to the EE but only by virtue of being
	// within this structure. When in one state or the other, that is freezing or not freezing,
	// we like to see 3 consecutive reads in the opposite state before switching states.
	// Remember this input is polled at a 1 hertz rate.
	UNS_32		freeze_clik_count_to_opposing_state;

	// 2/6/2013 rmd : Required to be saved to eeprom. Retained through a power failure. See
	// description for rain click above. This input is similar except that when temps climb
	// above freezing the response is generally within hours.
	BOOL_32		freeze_clik_shows_it_is_freezing;

	// ----------

	// 2/7/2013 rmd : From the main board and saved to the eeprom.
	WIND_SETTINGS_STRUCT	wss;

	BOOL_32					need_wind_settings_from_main_board;
	
	// ----------

	// 2/6/2013 rmd : The latest one second count as delivered via the interrupts. Should be set
	// to 0 when read out from the eeprom.
	//
	// 10/13/2014 rmd : No reason to include wind speed in the EE. And in the presence of an
	// input pulse train, I'm trying to discover why it flip-flops between 0 and mph and back to
	// 0 several times during a tpmicro restart. BUT did not want to ERASE this variable from
	// this structure. Else the decoder list would be lost upon an update. And that would be big
	// trouble for existing customers. (A discovery upon a code update may be a way to manage
	// this.)
	UNS_32					nlu_wind_speed_mph;

	// 2/6/2013 rmd : Required to be saved to eeprom. Retained through a power failure. When
	// (true) there should be no irrigation. And coming out of a power failure the wind must be
	// under the resume speed for the specified time before this is set (false).
	BOOL_32					wind_paused;
	
	// ----------

} WEATHER_INFO_STRUCT;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern WEATHER_INFO_STRUCT	weather_info;

// 10/13/2014 rmd : The real time wind speed. Transmitted to the main board when non-zero.
extern UNS_32	wind_speed_mph;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_weather_info( void );


extern void clear_run_away_gage_state( void );


extern void WEATHER_one_hertz_update( PORT_ZERO_COUNTS_STRUCT* const paccumulators_ptr );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*WEATHERMANAGER_H*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

