/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#include	"tpmicro_comm_mngr.h"

#include	"gpiopinchg.h"

#include	"irrigation_manager.h"

#include	"hardwaremanager.h"

#include	"comm_utils.h"

#include	"weather_manager.h"

#include	"x_uart.h"

#include	"iap.h"

#include	"adc.h"

#include	"two_wire_task.h"

#include	"two_wire_fet_control.h"

#include	"crc.h"

#include	"two_wire_discovery_process.h"

#include	"df_storage_mngr.h"

#include	"rxcommtask.h"

#include	"eeprom_map.h"

#include	"wdt_and_powerfail.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*

* File Description / Theory of operation

GENERAL - This file contains the INTERNAL TASK, which handles a lot of the general functionality of the TP micro that isnt critically timed, it is
basically a task for the other tasks to utilize so the other tasks can work on more core functionality that may be time critical

MESSAGE PROCESSING - In general, this TASK handles all message processing, it takes messages from the main board and alerts the other tasks if they
have duties to preform due to that message (IE processing irrigation messages from main and forwarding the results to the IRRIGATION TASK
queue).  It is also responsible for constructing messages that will be sent to main and feeding the message to the SERIAL TASK queue for later sending.

WEATHER RESULTS - This task is queued from the RIT. It takes the latest generated weather results and stuffs them into a global that other tasks can
safely access. It also calculates whether we have runaway ETGAGE conditions.  It does this by storing the last 10 deltas between pulses and adding
them up and comparing them to set limites (1 per 30 seconds, 10 per 20 mins). After a pulse is acknowldedged, we disregard any additional pulses in a
15 second window.

FLOW UPDATES - Flow readings are constantly fed into the internal message queue from the RIT and they are stored in the appropriate flow buffers,
every second a message is sent from our RIT interrupt which advances the buffers index. This ensures that our flow buffers array indexes are seperated
by 1 second of time.

*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/8/2014 rmd : On startup we wait for a message FROM the main board before we attempt to
// send any of our own messages TO the main board. When the first message arrives from the
// main board (actually every message that arrives) we extract the FROM address. And use
// that as our TO address when making our own outbound messages. The address is stored in
// this variable.
UNS_32	main_board_serial_number;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void TIMER_no_msg_from_main_handler( xTimerHandle xTimer )
{
	// This is NOT an interrupt. But rather executed within the context of the timer task.
	
	// ----------

	// 10/3/2013 rmd : First turn off all the CONVENTIONAL and DECODER based STATIONS.
	
	TWO_WIRE_TASK_QUEUE_STRUCT	twtqs;

	// ----------

	twtqs.command = TWO_WIRE_TASK_QUEUE_COMMAND_receive_stations_ON_from_main;
	
	// 4/17/2013 rmd : This is our indication that NO STATIONS are to be ON!
	twtqs.ucp = NULL;
	
	xQueueSendToBack( two_wire_task_queue, &twtqs, portMAX_DELAY );
	
	// ----------

	twtqs.command = TWO_WIRE_TASK_QUEUE_COMMAND_receive_pocs_ON_from_main;
	
	// 4/17/2013 rmd : This is our indication that NO POCS are to be ON!
	twtqs.ucp = NULL;
	
	xQueueSendToBack( two_wire_task_queue, &twtqs, portMAX_DELAY );
	
	// ----------

	pre_defined_alert_to_main( TPALERT_no_msgs_from_main );

	// ----------
}

/* ---------------------------------------------------------- */

// 3/4/2014 rmd : Starts out false. The main board always asks for it after booting in its
// first message to the tpmicro.
static BOOL_32	send_executing_date_and_time;

/* ---------------------------------------------------------- */
static void construct_and_route_outgoing_message_to_main( void )
{
	// 1/31/2013 rmd : Executes at a 1 hertz rate within the context of the comm_mngr task.
	// Biggest message we can build is 1024 bytes. Limited I suppose by the allocation block we
	// obtain.
	
	// ----------

	XMIT_QUEUE_ITEM_STRUCT	om;
	
	UNS_8					*ucp, *crc_start;

	UNS_32					ddd, remaining_bytes;

	UNS_32					predicted_size, test_size;
	
	UNS_32					temp_uns32;

	DATA_HANDLE				data_handle;
	
	// 11/4/2014 rmd : Some variables to implement the logic used to determine if there are any
	// decoder faults to send.
	BOOL_32		short_to_send;
	UNS_32		short_index;
	UNS_32		short_output;

	BOOL_32		low_voltage_to_send;
	UNS_32		low_voltage_index;
	UNS_32		low_voltage_output;

	BOOL_32		in_op_to_send;
	UNS_32		in_op_index;
	
	DECODER_FAULT_BASE_TYPE		decoder_fault;
	
	UNS_8		sw_state;
	
	// ----------

	// 3/14/2013 rmd : Its going out the main port and therefore is not part of the flow control
	// scheme.
	om.give_main_goahead = (false);
	
	// 3/22/2013 rmd : The absolute maximum packet size is the limit. Check for overflow as we
	// build and fill the memory block. About 2K of user data.
	om.dh.dptr = GetMemBlk( TPMICRO_MAX_AMBLE_TO_AMBLE_PACKET_SIZE );

	remaining_bytes = TPMICRO_MAX_AMBLE_TO_AMBLE_PACKET_SIZE;
	
	// 3/13/2013 rmd : If memory not available flag set internal to the memory manager. And the
	// message won't be sent.
	if( om.dh.dptr != NULL )
	{
		ucp = om.dh.dptr;

		om.dh.dlen = 0;

		// ----------

		add_PRE_AMBLE( &om.dh, &ucp );

		// ----------

		// 3/12/2013 rmd : And the CRC starts with the header.
		crc_start = ucp;

		// ----------
		
		add_CLASS_FROM_TP_MICRO_header( &om.dh, &ucp );

		// ----------
		
		// 4/3/2012 rmd : The fc structure location within the message needs to be filled out. And
		// the bits set as we build the message. To be EXTRA safe you should not simply declare a
		// ptr to a FOAL_COMMANDS structure at the current value of ucp. If ucp turned out to not be
		// ALIGNED on a word boundary and you tried to write to fc->pieces I'm pretty sure we would
		// suffer an ADDRESS EXCEPTION.
		FOAL_COMMANDS   fc;

		UNS_8	*where_to_place_fc_ptr;

		where_to_place_fc_ptr = ucp;

		// 2/1/2013 rmd : Filling with 0's has the desired effect of unsetting the bits in pieces.
		memset( &fc, 0x00, sizeof(FOAL_COMMANDS) );

		fc.mid = MID_TPMICRO_TO_MAIN_PRIMARY_MESSAGE;

		ucp += sizeof( FOAL_COMMANDS );

		om.dh.dlen += sizeof( FOAL_COMMANDS );

		// ----------

		remaining_bytes -= ( sizeof(AMBLE_TYPE)	+ sizeof(TPL_DATA_HEADER_TYPE) + sizeof(FOAL_COMMANDS) );
		
		// 3/22/2013 rmd : And subtract out what we know must come at the end so we stop adding to
		// the message with room to spare for the crc and the post amble.
		remaining_bytes -= ( sizeof(CRC_TYPE) + sizeof(AMBLE_TYPE) );
		
		// ----------

		if( send_executing_date_and_time )
		{
			B_SET( fc.pieces, BIT_TTMB_PRIMARY_executing_code_date_and_time );

			UNS_32	tmp_32;
			
			tmp_32 = convert_code_image_date_to_version_number( 0, TPMICRO_APP_FILENAME );
			memcpy( ucp, &tmp_32, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
			
			tmp_32 = convert_code_image_time_to_edit_count( 0, TPMICRO_APP_FILENAME );
			memcpy( ucp, &tmp_32, sizeof(UNS_32) );
			ucp += sizeof(UNS_32);
			
			// ----------
			
			om.dh.dlen += (2*sizeof(UNS_32));

			remaining_bytes -= (2*sizeof(UNS_32));

			// ----------
			
			send_executing_date_and_time = (false);
		}
		else
		{
			// 10/14/2014 rmd : BEGINNING of regular message content. Starts with the whats installed
			// data. Followed by the content that has "implicit value when absent". See below for more
			// information.
			if( send_whats_installed_to_the_main_board )
			{
				// 10/14/2014 rmd : No need to check remaining_bytes. It has to fit. It is first in the
				// message! The size of whats_installed is at 56 bytes now. I just checked.
				xSemaphoreTake( g_xHardwareProfileMutex, portMAX_DELAY );
	
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_here_is_whats_installed );
				
				memcpy( ucp, &whats_installed, sizeof(whats_installed) );
	
				ucp += sizeof(whats_installed);
	
				om.dh.dlen += sizeof(whats_installed);
	
				// ----------
				
				send_whats_installed_to_the_main_board = (false);
	
				// ----------
				
				remaining_bytes -= sizeof( whats_installed );
	
				xSemaphoreGive( g_xHardwareProfileMutex );
			}
	
			// ----------
			
			if( weather_info.need_wind_settings_from_main_board )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_wind_requesting_settings );
			}
	
			// ----------
	
			if( two_wire_bus_info.cable_short_xmission_state == TWO_WIRE_CABLE_STATUS_XMISSION_STATE_signal_main )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				//
				// 10/31/2013 rmd : Xmit the fact that the event occurred to the main board. The goal being
				// to get a flag to the master so we can remove all the two wire stations.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_cable_excessive_current_marker );
	
				two_wire_bus_info.cable_short_xmission_state = TWO_WIRE_CABLE_STATUS_XMISSION_STATE_waiting_for_ack;
			}
	
			// ----------
	
			if( two_wire_bus_info.cable_over_heated_xmission_state == TWO_WIRE_CABLE_STATUS_XMISSION_STATE_signal_main )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				//
				// 10/31/2013 rmd : Xmit the fact that the event occurred to the main board. The goal being
				// to get a flag to the master so we can remove all the two wire stations.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_cable_overheated_marker );
	
				two_wire_bus_info.cable_over_heated_xmission_state = TWO_WIRE_CABLE_STATUS_XMISSION_STATE_waiting_for_ack;
			}
	
			// ----------
	
			if( two_wire_bus_info.cable_cooled_off_xmission_state == TWO_WIRE_CABLE_STATUS_XMISSION_STATE_signal_main )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				//
				// 11/13/2013 ajv : Transmit the fact that the terminal cooled off to the mainboard. The
				// goal being to get a flag to the master so it can disemminate it to the IRRI machines to
				// allow irrigation to resume.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_cable_cooled_off_marker );
	
				// 11/13/2013 ajv : Since we don't explicitly respond with an ACK for this message, set it
				// back to normal.
				two_wire_bus_info.cable_cooled_off_xmission_state = TWO_WIRE_CABLE_STATUS_XMISSION_STATE_normal;
			}
	
			// ----------

			// 10/15/2014 rmd : Be SURE to include wind speed if there is wind. Remember the absence of
			// this data IMPLIES the wind speed is ZERO. But do preform a size test when attempting to
			// include. We surely don't want to overrun our memory block.
			if( wind_speed_mph > 0 )
			{
				if( remaining_bytes >= sizeof( UNS_32 ) )
				{
					B_SET( fc.pieces, BIT_TTMB_PRIMARY_we_have_wind_speed );
		
					memcpy( ucp, &wind_speed_mph, sizeof( wind_speed_mph ) );
		
					ucp += sizeof( wind_speed_mph );
		
					om.dh.dlen += sizeof( wind_speed_mph );
	
					remaining_bytes -= sizeof( wind_speed_mph );
				}
				else
				{
					pre_defined_alert_to_main( TPALERT_constructed_message_length_too_large );
				}
			}
	
			// ----------
			
			if( weather_info.rain_clik_shows_rain )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_it_is_currently_raining );
			}
	
			// ----------
	
			if( weather_info.freeze_clik_shows_it_is_freezing )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_it_is_currently_freezing );
			}
	
			// ----------
	
			if( weather_info.etgage_counts > 0 )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_we_had_an_et_gage_pulse );
	
				// 2/20/2013 rmd : No reason to not set the remaining pulse count to 0 after this message.
				// These message a created approximately once per second. And all the previous logic on the
				// gage pulses attempt to limit the pulses to one per second per 30 seconds. In other words
				// one pulse per 30 seconds.
				weather_info.etgage_counts = 0;
			}
	
			// ----------
	
			if( weather_info.rain_bucket_counts > 0 )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_we_had_a_rain_bucket_pulse );
	
				// 2/12/2013 rmd : By setting the count here to 0, regardless of how many pulses have been
				// racked up we limit the number to 1 per second. This is our debounce. You see, if you are
				// maually tapping, because of the hardware it is almost impossible to get just one pulse -
				// you get many. This could be fixed in hardware, or software, or both. For now it is only
				// managed right here in software. And this works just fine. At .01 inch/sec x 60 sec/min *
				// 60 min/hr we get 36 inches per hour. A physical impossiblity.
				weather_info.rain_bucket_counts = 0;
			}
			
			// ----------
			// ----------
	
			// 5/8/2014 rmd : This is the point in the message after which content will be ignored if
			// the main board network is not available. Which can mean a fresh main board boot, or a
			// scan is taking place, and more reasons. See the main board COMM_MNGR_network_available
			// function for more info.
			
			// ----------
	
			// 5/22/2014 rmd : Because we need to utilize the decoder_list array to find the poc
			// decoders if the discovery process is ongoing do not attempt to send any poc flow
			// info.
			if( !two_wire_bus_info.discovery_in_process )
			{
				// 1/28/2016 rmd : FIRST the flow counts for each POC decoder on this cable.

				// 8/4/2015 rmd : Because of the way flow readings from the tpmicro can be 'summed' upon
				// receipt at the irri machine I felt the safest and most logical method to assure all flow
				// data was driven to zero during cable ANOMOLY events was to continue sending fake flow
				// readings for the POC decoders with the content driven to represent a zero flow rate.
				// There is probably another way to attach this up in the main board. But approaching it
				// from this end seemed like a good way to go at it.
				//
				// 8/4/2015 rmd : I do believe that it is ONLY the latest 5 second average that needs to get
				// clobbered and that only takes one of these transmissions of fake readings. The
				// accumulated pulses get 'consumed' when added to the accumulators so they are not a
				// problem. However this approach of send every message while these faults exist is okay to
				// do and should work fine.
				with_cable_error_or_inop_poc_decoder_fake_zero_flow();

				// ----------

				add_latest_poc_flow_counts_to_the_message_to_main( &data_handle );
		
				// ----------
				
				if( remaining_bytes >= data_handle.dlen )
				{
					if( data_handle.dptr != NULL )
					{
						B_SET( fc.pieces, BIT_TTMB_PRIMARY_here_are_the_poc_flow_meter_counts );
		
						memcpy( ucp, data_handle.dptr, data_handle.dlen );
		
						ucp += data_handle.dlen;
		
						om.dh.dlen += data_handle.dlen;
		
						remaining_bytes -= data_handle.dlen;
			
						// 5/22/2014 rmd : We're done with it so free it.
						FreeMemBlk( data_handle.dptr );
					}
				}
				else
				{
					if( data_handle.dptr != NULL )
					{
						// 3/22/2013 rmd : Hey it has been allocated. Even though not using must free the block.
						FreeMemBlk( data_handle.dptr );
					}
					
					alert_message_to_tpmicro_pile( "flow data wouldn't fit" );
		
					pre_defined_alert_to_main( TPALERT_constructed_message_length_too_large );
				}

				// ----------

				// 1/28/2016 rmd : SECOND the moisture reading for a single moisture decoder. The way I
				// figure it we can afford to send one moisture reading at a time to keep the message
				// building and parsing simple. Remember the moisture is read from each moisture decoder
				// once every 10 minutes by the TPMicro. And this message is built once every second. So to
				// have a reading or two waiting a few seconds to send to the main board is acceptable.
				if( there_is_a_moisture_reading_to_send( &data_handle ) )
				{
					if( remaining_bytes >= data_handle.dlen )
					{
						B_SET( fc.pieces, BIT_TTMB_PRIMARY_here_is_a_moisture_reading );
		
						memcpy( ucp, data_handle.dptr, data_handle.dlen );
		
						ucp += data_handle.dlen;
		
						om.dh.dlen += data_handle.dlen;
		
						remaining_bytes -= data_handle.dlen;
			
						// 5/22/2014 rmd : We're done with it so free it.
						FreeMemBlk( data_handle.dptr );
					}
					else
					{
						// 3/22/2013 rmd : Hey it has been allocated so free it.
						FreeMemBlk( data_handle.dptr );
						
						alert_message_to_tpmicro_pile( "moisture wouldn't fit" );
			
						pre_defined_alert_to_main( TPALERT_constructed_message_length_too_large );
					}
					
				}

			}  // of if NOT doing a discovery
			
			// ----------
	
			if( send_measured_currents_for_those_ON_to_main )
			{
				DATA_HANDLE current_data = get_ma_readings_for_the_triacs_ON();
	
				if( remaining_bytes >= current_data.dlen )
				{
					if( current_data.dptr != NULL )
					{
						B_SET( fc.pieces, BIT_TTMB_PRIMARY_measured_currents_for_the_stations_ON );
		
						memcpy( ucp, current_data.dptr, current_data.dlen );
		
						ucp += current_data.dlen;
						
						om.dh.dlen += current_data.dlen;
		
						// ----------
		
						remaining_bytes -= current_data.dlen;					
		
						// ----------
		
						send_measured_currents_for_those_ON_to_main = (false);
	
						// ----------
		
						FreeMemBlk( current_data.dptr );
					}
				}
				else
				{
					if( current_data.dptr != NULL )
					{
						// 3/22/2013 rmd : Hey it has been allocated. Even though not using must free the block.
						FreeMemBlk( current_data.dptr );
					}
					
					pre_defined_alert_to_main( TPALERT_constructed_message_length_too_large );
				}
			}
	
			// ----------
	
			if( scs.state == SHORT_STATE_send_short_or_no_current_to_main )
			{
				if( remaining_bytes >= sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT) )
				{
					B_SET( fc.pieces, BIT_TTMB_PRIMARY_terminal_short_report );
	
					// ----------
	
					memcpy( ucp, &scs.short_or_no_current_report, sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT) );
	
					ucp += sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT);
					
					om.dh.dlen += sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT);
	
					// ----------
	
					remaining_bytes -= sizeof(TERMINAL_SHORT_OR_NO_CURRENT_STRUCT);					
	
					// ----------
	
					// 10/28/2013 rmd : Even a NO_CURRENT must wait for an ACK. Suppose we didn't wait and then
					// the next station to turn ON had a short. Does it all work out? Or is the NO_CURRENT
					// record up in the main board still in use as it is distributed. Or more likely as it is
					// waiting for a token to arrive.
					scs.state = SHORT_STATE_short_or_no_current_waiting_for_ack;
					
					// 10/17/2016 rmd : And initialize the safety timer put in place to unlatch the waiting for
					// ACK state. In case it never comes from the main board. We take this precaution because
					// this state is very powerful - blocking out all further irrigation (till a pwoer failure).
					scs.waiting_for_ACK_count = 0;
				}
				else
				{
					pre_defined_alert_to_main( TPALERT_constructed_message_length_too_large );
				}
			}
	
			// ----------
	
			if( fuse_is_blown )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_fuse_is_blown );
			}
	
			// ----------
	
			if( remaining_bytes >= sizeof( UNS_32 ) )
			{
				// 3/12/2013 rmd : We always send the most recently calculated current number.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_here_is_the_1s_current_draw);
		
				// ----------
	
				// 9/20/2013 rmd : Be careful to do this atomically. First read the ma variable into a local
				// variable here in preparation for the memcpy. The memcpy is NOT atomic of course so must
				// be done this way! (technically, as it turns out, since the adc readings are created
				// within this comm_mngr task this is not necessary - but so hard to find the bug if we
				// moved that processing out of this task).
				temp_uns32 = adc_readings.field_current_short_avg_ma;
	
				memcpy( ucp, &temp_uns32, sizeof(UNS_32) );
		
				ucp += sizeof(UNS_32);
		
				om.dh.dlen += sizeof(UNS_32);
	
				remaining_bytes -= sizeof(UNS_32);
			}
			else
			{
				pre_defined_alert_to_main( TPALERT_constructed_message_length_too_large );
			}
	
			// ----------
	
			if( weather_info.wind_paused )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_wind_is_in_paused_state );
			}
	
			// ----------
	
			// 5/22/2014 rmd : Do not try to take the list MUTEX here. During the discovery process that
			// MUTEX is held. And thats the point of this content. To send a progress update. And done
			// so utilizing an atomic read.
			if( two_wire_bus_info.discovery_in_process )
			{
				predicted_size = sizeof(UNS_32);
				
				if( remaining_bytes >= predicted_size )
				{
					B_SET( fc.pieces, BIT_TTMB_PRIMARY_decoders_discovered_so_far );
					
					// 10/16/2013 rmd : Move the discovered count in an ATOMIC manner into a temporary variable.
					// So that we can use memcpy to place the count into the message. Remember the HIGH priority
					// 2W task is busy moving the discovery process along. But allow other task such as us to
					// remain active. As such, the count may change mid-stream the memcpy. So protect against
					// that.
					ddd = two_wire_bus_info.discovered_count;
	
					memcpy( ucp, &ddd, sizeof(UNS_32) );
					ucp += sizeof(UNS_32);
					
					om.dh.dlen += predicted_size;
	
					remaining_bytes -= predicted_size;
				}
				else
				{
					pre_defined_alert_to_main( TPALERT_constructed_message_length_too_large );
				}
	
			}  // of if the discovery process is ongoing
	
			// ----------
	
			// 5/22/2014 rmd : If we are being asked to send the discovery results do so.
			if( two_wire_bus_info.send_main_the_discovery_process_results )
			{
				// 5/22/2014 rmd : As an added level of list protection take this MUTEX. I suppose a second
				// discovery could be attempted while we were building this message? If the MUTEX is NOT
				// available it is okay to skip this content right now. We'll get it on the next message
				// build attempt.
				if( xSemaphoreTake( decoder_list_MUTEX, 0 ) == pdTRUE )
				{
					ddd = number_of_newly_discovered_decoder_serial_nums_to_send_to_the_mb();
					
					predicted_size = ( sizeof(BOOL_32) + sizeof(UNS_32) + ( ddd * (sizeof(UNS_32) + sizeof(ID_REQ_RESP_s)) ) );
					
					test_size = 0;
					
					if( remaining_bytes >= predicted_size )
					{
						B_SET( fc.pieces, BIT_TTMB_PRIMARY_sending_discovered_decoders );
						
						// 2/1/2013 rmd : The indication is this a complete list or added on list.
						memcpy( ucp, &two_wire_bus_info.full_discovery, sizeof(BOOL_32) );
						ucp += sizeof(BOOL_32);
						test_size += sizeof(BOOL_32);
						
						// 2/1/2013 rmd : Remember we hold the MUTEX so the quantity to send will not change while
						// we prepare the content here.
						memcpy( ucp, &ddd, sizeof(UNS_32) );
						ucp += sizeof(UNS_32);
						test_size += sizeof(UNS_32);
						
						for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
						{
							if( B_IS_SET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_SEND_DISCOVERED_TO_MAIN_BOARD ) )
							{
								memcpy( ucp, &decoder_list[ ddd ].sn, sizeof(UNS_32) );
						
								ucp += sizeof(UNS_32);
								test_size += sizeof(UNS_32);
								
								memcpy( ucp, &decoder_list[ ddd ].id_info, sizeof(ID_REQ_RESP_s) );
						
								ucp += sizeof(ID_REQ_RESP_s);
								test_size += sizeof(ID_REQ_RESP_s);
								
								B_UNSET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_SEND_DISCOVERED_TO_MAIN_BOARD );
							}
						}
						
						if( test_size != predicted_size )
						{
							// 3/22/2013 rmd : This mean the MUTEX protection failed? ddd changed?
							DEBUG_STOP();	
						}
		
						om.dh.dlen += predicted_size;
		
						remaining_bytes -= predicted_size;
					}
					else
					{
						pre_defined_alert_to_main( TPALERT_constructed_message_length_too_large );
					}
		
					// ----------
					
					// 10/16/2013 rmd : Clear the flag. We've sent the results.
					two_wire_bus_info.send_main_the_discovery_process_results = (false);
					
					// ----------
					
					xSemaphoreGive( decoder_list_MUTEX );
				}
				else
				{
					alert_message_to_tpmicro_pile( "discovery results skipped" );
				}
			}
	
			// ----------
	
			if( xSemaphoreTake( decoder_statistics_MUTEX, 0 ) == pdTRUE )
			{
				// 4/16/2013 rmd : See explanation for the decoder_list_MUTEX above for a description of the
				// strategy employed here to protect the data. NOTE the 0 wait time. Being very careful not
				// to block on this MUTEX but rather skip over this section if not available!
				
				ddd = number_of_decoder_statistics_to_send_to_the_mb();
				
				if( ddd > 0 )
				{
					// 3/22/2013 rmd : Because we can have 100 decoders and the statistics size is 24 bytes we
					// can fill a message. Right here check we can send even 1.
					if( remaining_bytes >= ( sizeof(UNS_32) + sizeof(UNS_32) + sizeof(DECODER_STATS_s) ) )
					{
						// 3/22/2013 rmd : Okay there is at least room for ONE.
						B_SET( fc.pieces, BIT_TTMB_PRIMARY_sending_decoder_statistics );
						
						// 2/1/2013 rmd : Skip over where the count goes. Because this can be so big we don't know
						// yet how many we'll fit in.
						UNS_8	*ptr_to_count;
						UNS_32	count;
		
						count = 0;
						
						ptr_to_count = ucp;
						ucp += sizeof( UNS_32 );
						om.dh.dlen += sizeof(UNS_32);
						remaining_bytes -= sizeof(UNS_32);
						
						for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
						{
							if( B_IS_SET( decoder_info[ ddd ].send_to_main, SEND_TO_MAIN_BOARD_DECODER_STATISTICS ) )
							{
								if( remaining_bytes >= ( sizeof(UNS_32) + sizeof(DECODER_STATS_s) ) )
								{
									// 2/1/2013 rmd : Okay one more will fit.
									count += 1;
		
									memcpy( ucp, &decoder_list[ ddd ].sn, sizeof(UNS_32) );
									ucp += sizeof(UNS_32);
		
									memcpy( ucp, &decoder_info[ ddd ].decoder_statistics, sizeof(DECODER_STATS_s) );
									ucp += sizeof(DECODER_STATS_s);
	
									om.dh.dlen += ( sizeof(UNS_32) + sizeof(DECODER_STATS_s) );
	
									remaining_bytes -= ( sizeof(UNS_32) + sizeof(DECODER_STATS_s) );
									
									B_UNSET( decoder_info[ ddd ].send_to_main, SEND_TO_MAIN_BOARD_DECODER_STATISTICS );
								}
								else
								{
									break;  // no more will fit in this message - next message will pick up
								}
							}
						}
					
						// 2/1/2013 rmd : Put in the count.
						memcpy( ptr_to_count, &count, sizeof(UNS_32) );
					}
				}
	
				// ----------
	
				xSemaphoreGive( decoder_statistics_MUTEX );
			}
	
			// ----------
	
			// 3/22/2013 rmd : From this point on it doesn't make any sense to alert if something won't
			// fit in the message. Cause the decoder statistics may have left no space available. And
			// that should be okay. We'll send whatever can't fit in the next message.
	
			// ----------
	
			if( weather_info.run_away_gage )
			{
				// 10/14/2014 rmd : NO DATA ACCOMPANIES. ONLY BIT SET.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_run_away_gage_state_latched );
			}
			
			// ----------
	
			if( remaining_bytes >= sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT) )
			{
				// 3/12/2013 rmd : We always send the most recent 2W cable percentage of current. Whether
				// we have a two-wire card or not.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_2W_cable_current_measurement );
		
				// ----------
	
				TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT	twccms;
				
				twccms.current_percentage_of_max = two_wire_bus_info.cable_percent_of_maximum_current;
				
				memcpy( ucp, &twccms, sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT) );
				ucp += sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT);
		
				om.dh.dlen += sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT);
	
				remaining_bytes -= sizeof(TWO_WIRE_CABLE_CURRENT_MEASUREMENT_STRUCT);
			}
	
			// ----------
			
			short_to_send = (false);
			short_index = 0;  // compiler warns about possible uninitialized variable use
			short_output = 0;  // compiler warns about possible uninitialized variable use

			low_voltage_to_send = (false);
			low_voltage_index = 0;
			low_voltage_output = 0;

			in_op_to_send = (false);
			in_op_index = 0;
			
			for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
			{
				if( remaining_bytes >= sizeof(DECODER_FAULT_BASE_TYPE) )
				{
					if( !short_to_send )
					{
						if( B_IS_SET( decoder_info[ ddd ].output_A.output_status, OUTPUT_STATUS_BIT_SEND_SHORT_TO_MAIN ) )
						{
							short_to_send = (true);
							short_index = ddd;
							short_output = DECODER_OUTPUT_A_BLACK;
							
							remaining_bytes -= sizeof(DECODER_FAULT_BASE_TYPE);
							
							// ----------
		
							// 10/24/2014 rmd : And clear the flag to send this information to the main baord.
							B_UNSET( (decoder_info[ ddd ].output_A).output_status, OUTPUT_STATUS_BIT_SEND_SHORT_TO_MAIN );
						}
						else
						if( B_IS_SET( (decoder_info[ ddd ].output_B).output_status, OUTPUT_STATUS_BIT_SEND_SHORT_TO_MAIN ) )
						{
							short_to_send = (true);
							short_index = ddd;
							short_output = DECODER_OUTPUT_B_ORANGE;
							
							remaining_bytes -= sizeof(DECODER_FAULT_BASE_TYPE);
							
							// ----------
		
							// 10/24/2014 rmd : And clear the flag to send this information to the main baord.
							B_UNSET( (decoder_info[ ddd ].output_B).output_status, OUTPUT_STATUS_BIT_SEND_SHORT_TO_MAIN );
						}
					}
				}
				else
				{
					// 11/4/2014 rmd : Can't fit anymore so git.
					break;
				}

				// ----------
				
				// 11/4/2014 rmd : Now look for the low voltage condition.
				if( remaining_bytes >= sizeof(DECODER_FAULT_BASE_TYPE) )
				{
					if( !low_voltage_to_send )
					{
						if( B_IS_SET( decoder_info[ ddd ].output_A.output_status, OUTPUT_STATUS_BIT_SEND_VOLTAGE_TOO_LOW_TO_MAIN ) )
						{
							low_voltage_to_send = (true);
							low_voltage_index = ddd;
							low_voltage_output = DECODER_OUTPUT_A_BLACK;
							
							remaining_bytes -= sizeof(DECODER_FAULT_BASE_TYPE);
							
							// ----------
							
							// 10/24/2014 rmd : And clear the flag to send this information to the main baord.
							B_UNSET( decoder_info[ ddd ].output_A.output_status, OUTPUT_STATUS_BIT_SEND_VOLTAGE_TOO_LOW_TO_MAIN );
						}
						else
						if( B_IS_SET( (decoder_info[ ddd ].output_B).output_status, OUTPUT_STATUS_BIT_SEND_VOLTAGE_TOO_LOW_TO_MAIN ) )
						{
							low_voltage_to_send = (true);
							low_voltage_index = ddd;
							low_voltage_output = DECODER_OUTPUT_B_ORANGE;
							
							remaining_bytes -= sizeof(DECODER_FAULT_BASE_TYPE);
							
							// ----------
	
							// 10/24/2014 rmd : And clear the flag to send this information to the main baord.
							B_UNSET( decoder_info[ ddd ].output_B.output_status, OUTPUT_STATUS_BIT_SEND_VOLTAGE_TOO_LOW_TO_MAIN );
						}
					}
				}
				else
				{
					// 11/4/2014 rmd : Can't fit anymore so git.
					break;
				}

				// ----------
				
				// 11/4/2014 rmd : Now look for the io_op condition.
				if( remaining_bytes >= sizeof(DECODER_FAULT_BASE_TYPE) )
				{
					if( !in_op_to_send )
					{
						if( B_IS_SET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_SEND_DECODER_INOPERATIVE_TO_MAIN_BOARD ) )
						{
							in_op_to_send = (true);
							in_op_index = ddd;
							
							remaining_bytes -= sizeof(DECODER_FAULT_BASE_TYPE);
							
							// ----------

							// 10/24/2014 rmd : Clear the flag to send this information to the main baord.
							B_UNSET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_SEND_DECODER_INOPERATIVE_TO_MAIN_BOARD );
						}
					}
				}
				else
				{
					// 11/4/2014 rmd : Can't fit anymore so git.
					break;
				}
			}  // of check each decoder
			
			// ----------
			
			if( short_to_send )
			{
				// 11/4/2014 rmd : Remember only send one short record at a time.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_decoder_output_short );

				// ----------
				
				decoder_fault.fault_type_code = DECODER_FAULT_CODE_solenoid_output_short;

				decoder_fault.id_info = decoder_list[ short_index ].id_info;

				decoder_fault.decoder_sn = decoder_list[ short_index ].sn;

				decoder_fault.afflicted_output = short_output;

				// ----------
				
				memcpy( ucp, &decoder_fault, sizeof(DECODER_FAULT_BASE_TYPE) );

				ucp += sizeof(DECODER_FAULT_BASE_TYPE);

				om.dh.dlen += sizeof(DECODER_FAULT_BASE_TYPE);

				// 11/4/2014 rmd : DO NOT decrement the remaining bytes. That was already done in the loop
				// hunting for the faults to send.
			}

			// ----------

			if( low_voltage_to_send )
			{
				// 11/4/2014 rmd : Remember only send one record at a time.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_decoder_output_voltage_too_low );

				// ----------
				
				decoder_fault.fault_type_code = DECODER_FAULT_CODE_low_voltage_detected;

				decoder_fault.id_info = decoder_list[ low_voltage_index ].id_info;

				decoder_fault.decoder_sn = decoder_list[ low_voltage_index ].sn;

				decoder_fault.afflicted_output = low_voltage_output;

				// ----------
				
				memcpy( ucp, &decoder_fault, sizeof(DECODER_FAULT_BASE_TYPE) );

				ucp += sizeof(DECODER_FAULT_BASE_TYPE);

				om.dh.dlen += sizeof(DECODER_FAULT_BASE_TYPE);

				// 11/4/2014 rmd : DO NOT decrement the remaining bytes. That was already done in the loop
				// hunting for the faults to send.
			}

			// ----------
			
			if( in_op_to_send )
			{
				// 11/4/2014 rmd : Remember only send one record at a time.
				B_SET( fc.pieces, BIT_TTMB_PRIMARY_two_wire_decoder_inoperative );
				
				// ----------
				
				decoder_fault.fault_type_code = DECODER_FAULT_CODE_inoperative_decoder;

				decoder_fault.id_info = decoder_list[ in_op_index ].id_info;

				decoder_fault.decoder_sn = decoder_list[ in_op_index ].sn;

				// ----------
				
				memcpy( ucp, &decoder_fault, sizeof(DECODER_FAULT_BASE_TYPE) );

				ucp += sizeof(DECODER_FAULT_BASE_TYPE);

				om.dh.dlen += sizeof(DECODER_FAULT_BASE_TYPE);

				// 11/4/2014 rmd : DO NOT decrement the remaining bytes. That was already done in the loop
				// hunting for the faults to send.
			}
			
			// ----------

			// 3/8/2016 mjb : First we need to check our flag to make sure we can include the SW1 status
			//in the message to main. We need to wait until we know SW1 has been read before we can 
			//include in the message. Additionally, Only if the weather card and terminal are present do 
			//we evaluate. This is because if weather and terminal are present, we want a rain switch to 
			//be used in weather and only use a rain switch on SW1 if they do NOT have weather. 
			if ( terminal_poc.sw_read_status_ready && (!whats_installed.weather_card_present || !whats_installed.weather_terminal_present)) 
			{
				// 3/8/2016 mjb : Set the bit.
				B_SET(fc.pieces, BIT_TTMB_PRIMARY_status_sw1_status_in_message );
				
				// 3/8/2016 mjb : If our flag is set, let's go ahead and create a byte to use
				// at all times in our message to main. The idea is either we will not have the byte in the 
				// message at all, which means that our flag was set to false OR we will have the byte in 
				// the message which means the flag is true in which case, the byte will either be set or 
				// not set and should follow the actual status of SW1.
				if( terminal_poc.sw_open )
				{
					// 3/8/2016 mjb : If we determine sw_open from the I2C bus/SW1, let's set our sw_state bit to 1. This will then be
					// used in the message to main. Setting sw_state seems redundant since we already have 
					// sw_open because it is redundant. However, we are doing this because sw_open was already 
					// declared as a bool_32 and for this we just wanted/needed an uns_8
					sw_state = 0x01;
				}
				else
				{
					sw_state = 0x00;
				}

				memcpy( ucp, &sw_state, sizeof(UNS_8) );
		
				ucp += sizeof(UNS_8);
		
				om.dh.dlen += sizeof(UNS_8);
	
				remaining_bytes -= sizeof(UNS_8);
			}
	
			// ---------- 

			if( do_we_have_any_errors_to_report_to_main() )
			{
				if( remaining_bytes >= sizeof(ERROR_LOG_s) )
				{
					B_SET( fc.pieces, BIT_TTMB_PRIMARY_we_have_errors_to_report );
	
					// ----------
	
					// 8/10/2012 raf : We operate on the ERROR LOG non atomically from multiple tasks, so
					// anywhere we use it we have to take and release a mutex!
					xSemaphoreTake( g_xErrorLogMutex, portMAX_DELAY );
				
					memcpy( ucp, (void*)&g_error_log, sizeof(ERROR_LOG_s) );
	
					// 11/6/2012 rmd : As part of sending to the main board we clear the error field.
					g_error_log.errorBitField = 0;
				
					xSemaphoreGive( g_xErrorLogMutex );
	
					// ----------
	
					ucp += sizeof( ERROR_LOG_s );
	
					om.dh.dlen += sizeof( ERROR_LOG_s );
	
					remaining_bytes -= sizeof( ERROR_LOG_s );
				}
			}
	
		}  // of building regular message content

		// ----------

		// 4/3/2012 rmd : Done adding content. NOW put the fc structure into place.
		memcpy( where_to_place_fc_ptr, &fc, sizeof(FOAL_COMMANDS) );

		// ----------
	
		add_CRC( &om.dh, &ucp, crc_start, om.dh.dlen - sizeof(AMBLE_TYPE) );

		// ----------
	
		add_POST_AMBLE( &om.dh, &ucp );
		
		// ----------

		// 3/12/2013 rmd : Send the message to the main board.
		inc_memory_use_count_and_add_packet_to_xmit_queue( UPORT_MAIN, om );
		
		// 3/12/2013 rmd : And free the block. Because when the block is added to the xmit queue its
		// reference count is incremented. Which is in effect like checking out a new block. So we
		// are responsible for freeing this allocation we made.
		FreeMemBlk( om.dh.dptr );

		// ----------

	}  // of if the message memory block was allocated
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
  ----------------------------------------------------------
  FORMAT OF STATIONS ON PIECE
  ----------------------------------------------------------
	| # of Stations requested to be on (x)												//UNS_8
	|
	| |---------------------- (we now read x number of stations in the proceeding bytes in this example its 4)
	| | station id																		//UNS_8
	| | ...
	| | ...
	| | ...
	| | ...
	| | station id																		//UNS_8
	| | ...
	| | ...
	| | ...
	| | ...
	| | station id																		//UNS_8
	| | ...
	| | ...
	| | ...
	| | ...
	| | station id																		//UNS_8
	| | ...
	| | ...
	| | ...
	| | ...
	| |----------------------
	|
	-------------------------
*/

/*
  ----------------------------------------------------------
  FORMAT OF POC STATE SETTINGS PIECE
  ----------------------------------------------------------
	| # of POCs 											//UNS_8
	|
	| |----------------------
	| |type of POC  										//UNS_8
	| |
	| | |--------------------
	| |	| (potential) decoder serial number 				//UNS_32
	| | |
	| | | |------------------
	| | | | content byte:   								//UNS_8
	| | | | 	bit 0 => MV1 Engergized
	| | | | 	bit 1 => Pump1 Engergized
	| | | | 	bit 2 => MV2 Engergized
	| | | | 	bit 3 => Pump2 Engergized
	| | | | 	bit 4 => MV3 Engergized
	| | | | 	bit 5 => Pump3 Engergized
	| | | | 	bit 6 => Unused
	| | | | 	bit 7 => Unused
	| | | |------------------
	| | |
	| | |--------------------
	| |
	| |----------------------
	|
	-------------------------
*/
/*
  ----------------------------------------------------------
  FORMAT OF LIGHTS ON PIECE
  ----------------------------------------------------------
	|------------------
	| lights on byte:   								//UNS_8
	| 	bit 0 => Light 1
	| 	bit 1 => Light 2
	| 	bit 2 => Light 3
	| 	bit 3 => Light 4
	| 	bit 4 => Unused
	| 	bit 5 => Unused
	| 	bit 6 => Unused
	| 	bit 7 => Unused
	|------------------
*/

/* ---------------------------------------------------------- */
static void __post_to_two_wire_task( TWO_WIRE_TASK_QUEUE_STRUCT *twtq_ptr )
{
	// 4/17/2013 rmd : During the discovery process we cannot keep posting to the two_wire_task
	// queue. The discovery process is long-winded and the queue will overflow. During the
	// process other tasks will continue to function as we have OS delays during the process.
	if( !two_wire_bus_info.task_is_busy )
	{
		if( xQueueSendToBack( two_wire_task_queue, twtq_ptr, portNO_DELAY ) != pdTRUE )
		{
			flag_error( BIT_TPME_queue_full );
		}
	}	
}

/* ---------------------------------------------------------- */
static BOOL_32 pull_apart_primary_message_from_main( COMM_MNGR_QUEUE_STRUCT *pmessage_from_main_ptr )
{
	FOAL_COMMANDS	fc;

	BOOL_32			valid_message;

	UNS_8			*ucp, *tmp_ucp;

	UNS_8			number_of_stations_to_turn_ON;

	UNS_8			number_of_pocs_to_turn_ON;

	UNS_8			number_of_lights_to_turn_ON;

	TWO_WIRE_TASK_QUEUE_STRUCT	twtq;
	
	// ----------

	valid_message = (true);
	
	// ----------

	ucp = pmessage_from_main_ptr->dh.dptr;	//we use this to move through the message

	// 3/11/2013 rmd : Remember the delivered packet is the WHOLE packet including pre and post
	// ambles, crc, and packet header.
	ucp += sizeof(AMBLE_TYPE) + sizeof(TPL_DATA_HEADER_TYPE);

	// ----------

	memcpy( &fc, ucp, sizeof(FOAL_COMMANDS) );

	ucp += sizeof(FOAL_COMMANDS);

	// ----------

	if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_request_executing_code_date_and_time ) )
	{
		// 10/14/2014 rmd : NOTE when the main board sets this bit to send the executing time and
		// date it will be the ONLY bit set in the message. That is what the main board convention
		// is. Hence we use the else on the set bit as emphasis.
		//
		// 3/4/2014 rmd : No data with this one. The next message we send should include the
		// executing code date and time.
		send_executing_date_and_time = (true);
	}
	else
	if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_isp_entry_requested ) )
	{
		// 2/11/2014 rmd : Okay we are going to proceed towards entering the ISP. THERE IS NO RETURN
		// from the following function call as a WDT timeout is forced in order to cause a reset.
		// Which is how we get into the isp mode. I think it may not matter but for logical purposes
		// if this bit is set it is the only content of the message processed.
		IAP_set_stage_for_isp_entry_and_cause_a_restart();
	}
	else
	{
		// 10/14/2014 rmd : And now the regular message content. NOTE: one can re-arrange the order
		// of this content as part of a code update. BECAUSE of the way this content is delayed
		// until after the code date and time requests and potential code update phase have been
		// passed through and completed.
		
		// ----------
		
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_request_whats_installed ) )
		{
			// 3/22/2013 rmd : We've agreed that when the CS3000 main board reports to us that is has
			// restarted we will send the latest whats_installed. So set that flag here to cause the
			// next message to contain that information.
			send_whats_installed_to_the_main_board = (true);
		}
	
		// ----------
		
		if( scs.state == SHORT_STATE_short_or_no_current_waiting_for_ack )
		{
			scs.waiting_for_ACK_count += 1;
		}
		else
		{
			// 10/17/2016 rmd : Clear for housekeeping.
			scs.waiting_for_ACK_count = 0;
		}
		
		if( (valid_message && B_IS_SET( fc.pieces, BIT_TTPM_short_ack_terminal )) || (scs.waiting_for_ACK_count > 100) )
		{
			if( scs.state == SHORT_STATE_short_or_no_current_waiting_for_ack )
			{
				// 9/18/2013 rmd : When in this state the irrigation task, the other manipulator of this
				// state, is effectively shut down waiting for this state change. So directly change the
				// state here. As an alternate to directly changing the state here we could post a queue
				// message. But I don't feel that's necessary here. Move to the next state which is to wait
				// for the next message from main.
				//
				// 9/28/2015 rmd : But set the message variables to force refresh of those to be ON before
				// processing.
				scs.station_q_message_arrived = (false);

				scs.poc_mv_q_message_arrived = (false);

				scs.lights_q_message_arrived = (false);
								
				// 9/28/2015 rmd : Set to regular irrigation state but output processing will not commence
				// till the previously set (false) variables are all true.
				scs.state = SHORT_STATE_regular_irrigation;
				
				// ----------
				
				if( scs.waiting_for_ACK_count > 100 )
				{
					alert_message_to_tpmicro_pile( "Main did not ACK a short/no current" );
				}

				if( scs.short_or_no_current_report.result == SHORT_OR_NO_CURRENT_RESULT_no_current )
				{
					alert_message_to_tpmicro_pile( "No Current ack'd by main" );
				}
				else
				{
					alert_message_to_tpmicro_pile( "Short ack'd by main" );
				}
			}
			else
			{
				// 3/7/2013 rmd : We shouldnt have gotten this bit. Perhaps flag about such? NOTE - as a
				// precaution to prevent the lock up effect of a short in the tp micro (that is nothing will
				// continue to irrigate until acknowledged by the main board), the main board ALWAYS sends
				// an acknowledge with its first message to the tp micro. So suppress this alert in
				// time.
				//alert_message_to_tpmicro_pile( "Unexpected Short ACK from main" );
			}
		}
	
		// ----------
		
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_clear_two_wire_cable_excessive_current ) )
		{
			// 10/29/2013 rmd : The point of this ACK is to clear the 2W cable excessive current
			// condition. And the xmission state. The excessive current state prevents any cable
			// powering attempts and therefore any decoder communications.
			if( adc_readings.two_wire_cable_excessive_current )
			{
				alert_message_to_tpmicro_pile( "2W Cable Short ACK" );

				// ----------
				
				// 11/6/2013 rmd : Signal the two_wire task to clear the excessive current flag. And any
				// pending decoder commands. 
				twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_ack_cable_excessive_current;
				
				// 11/6/2013 rmd : In THIS case post directly to the queue. DO NOT use the function that
				// checks if the task is busy or not. We MUST post this message otherwise the cable fault
				// will NEVER be cleared!
				if( xQueueSendToBack( two_wire_task_queue, &twtq, portNO_DELAY ) != pdTRUE )
				{
					flag_error( BIT_TPME_queue_full );
				}
		
				// ----------
		
				// 11/6/2013 rmd : NOTE - It is very important not to clear the excessive current flag here.
				// But to let the TWO_WIRE_TASK do it. That way once it is set we know the two_wire task
				// reached idle before it can possibly be cleared. This is interesting - say we were in some
				// long winded process (ie the discovery process) and the short was tripped. But before we
				// 'backed out' of that process the short was cleared by the comm_mngr receiving the ACK.
				// Guess what. The code may bump into a cable mode change statement and the cable will
				// surprisingly repower! This prevents that by guaranteeing the process has ended before the
				// excessive current can be cleared.
				//
				// 11/6/2013 rmd : REGARDING THE OVERHEATED STATE - the same argument above holds. However
				// the overheated state is self-clearing. We count on the physics involved. It couldn't
				// possibly cool down before any process completed.
				//
				// So the implications is that it is safe to test both the excessive current and the
				// overheated states to 'back out' of any two wire communication exchange.
			}
			else
			{
				//alert_message_to_tpmicro_pile( "2W Cable Short unexpd ack" );
			}
				
			// ----------
	
			if( two_wire_bus_info.cable_short_xmission_state != TWO_WIRE_CABLE_STATUS_XMISSION_STATE_waiting_for_ack )
			{
				//alert_message_to_tpmicro_pile( "2W Cable Short unexp xmission state" );
			}
				
			two_wire_bus_info.cable_short_xmission_state = TWO_WIRE_CABLE_STATUS_XMISSION_STATE_normal;
		}
	
		// ----------
	
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_ack_two_wire_cable_overheated ) )
		{
			// 10/29/2013 rmd : The point of this ACK is to make sure there are no decoder commands
			// clear the 2W cable excessive current condition. And the xmission state. The excessive
			// current state prevents any cable powering attempts and therefore any decoder
			// communications.
			if( adc_readings.two_wire_terminal_over_heated )
			{
				alert_message_to_tpmicro_pile( "2W Cable Overheated ACK rcvd" );

				// ----------
				
				// 10/30/2013 rmd : Ensure there are no residual pending decoder commands. When the over
				// heated condition occurs the commands that were already set to execute will persist. No
				// further command processing will take place. So before we clear any condition start with a
				// clean slate. Remember the overheated condition clears itself. But it cannot celar itself
				// even if colled down until we set the xmission state back to normal!
				twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_ack_cable_overheated;
				
				// 11/6/2013 rmd : In THIS case post directly to the queue. DO NOT use the function that
				// checks if the task is busy or not. We MUST post this message otherwise the residual
				// commands may not be cleared (that is very unlikely but mimic the posting for the
				// excessive current for consistancy sakes).
				if( xQueueSendToBack( two_wire_task_queue, &twtq, portNO_DELAY ) != pdTRUE )
				{
					flag_error( BIT_TPME_queue_full );
				}
			}
			else
			{
				//alert_message_to_tpmicro_pile( "2W Cable Overheated unexpd ACK" );
			}
				
			// ----------
	
			if( two_wire_bus_info.cable_over_heated_xmission_state != TWO_WIRE_CABLE_STATUS_XMISSION_STATE_waiting_for_ack )
			{
				//alert_message_to_tpmicro_pile( "2W Cable Overheated unexp xmission state" );
			}
				
			// 10/29/2013 rmd : DO NOT clear the adc overheated state! That happens all by itself when
			// the parts cool down. But do clear the xmission state to allow the overheated state to be
			// cleared when the parts cool down. Clearing is blocked until the xmission state returns to
			// normal.
			two_wire_bus_info.cable_over_heated_xmission_state = TWO_WIRE_CABLE_STATUS_XMISSION_STATE_normal;
		}
	
		// ----------
		
		// 5/16/2014 rmd : STATIONS TO TURN ON
	
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_here_are_the_stations_to_turn_on ) )
		{
			number_of_stations_to_turn_ON = *ucp;
	
			ucp += sizeof( UNS_8 );
		
			// 5/16/2013 rmd : This message content includes both the conventional and decoder based
			// stations.
			if( number_of_stations_to_turn_ON > MAX_NUMBER_OF_TERMINAL_AND_DECODER_BASED_OUTPUTS_ON_AT_A_TIME )
			{
				pre_defined_alert_to_main( TPALERT_main_requested_too_many_stations_on );
	
				valid_message = (false);
			}
			else
			{
				// 9/30/2014 rmd : IF the two-wire task is busy do not get the memory block. BECAUSE the
				// function that posts to the task also checks if the task is busy, and if so, does not post
				// to the task. That would then leave these ORPHAN memory blocks. That never get released.
				// But still however need to move the ucp pointer ahead within the message.
				if( two_wire_bus_info.task_is_busy )
				{
					ucp += ( number_of_stations_to_turn_ON * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
				}
				else
				{
					// 4/17/2013 rmd : Obtain a 145 byte block. And take a copy of this portion of the message
					// content. We are passing it to the two-wire task where it can fully parse the contents.
					// This approach avoids some nasty MUTEX implications when protecting the decoder_info
					// structure from dual tasks writes. The crux of the problem comes about because the
					// two-wire task has some relatively 'slow' operations it performs and therefore is holding
					// the MUTEX for a lengthy period of time.
					twtq.ucp = GetMemBlk( sizeof(UNS_8) + (MAX_NUMBER_OF_TERMINAL_AND_DECODER_BASED_OUTPUTS_ON_AT_A_TIME * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT)) );
				
					if( twtq.ucp != NULL )
					{
						// 5/16/2014 rmd : Work with a tmp ucp to preserve the twtq.ucp value.
						tmp_ucp = twtq.ucp;
		
						twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_receive_stations_ON_from_main;
						
						*(tmp_ucp) = number_of_stations_to_turn_ON;
						
						tmp_ucp += sizeof(UNS_8);
						
						// 4/17/2013 rmd : And snatch all the records.
						memcpy( tmp_ucp, ucp, (number_of_stations_to_turn_ON * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT)) );
		
						// 5/14/2014 rmd : Keep the message pointer up. But not the tmp_ucp pointer as that is not
						// used again.
						ucp += ( number_of_stations_to_turn_ON * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
		
						__post_to_two_wire_task( &twtq );
					}
					else
					{
						alert_message_to_tpmicro_pile( "no memory for stations ON!" );
						
						// 4/17/2013 rmd : Well even though we couldn't get memory, the message may still be valid.
						// So bump ucp along.
						ucp += ( number_of_stations_to_turn_ON * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
					}
					
				}  // of if 2w task is not busy
				
			}  // of a valid number to turn ON
			
		}  // of stations to turn ON bit
		else
		{
			// 4/17/2013 rmd : The bit is not set meaning NO stations are ON according to the message
			// from main. But we still need to convey this to our tasks. Do that by setting the ucp ptr
			// to NULL.
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_receive_stations_ON_from_main;
			
			twtq.ucp = NULL;
			
			// 9/30/2014 rmd : This function call checks if the 2w task is busy and if so does not post
			// to the task.
			__post_to_two_wire_task( &twtq );
		}
		
		// ----------
		
		// 5/16/2014 rmd : POCS
	
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_here_are_the_pocs_to_turn_on ) )
		{
			number_of_pocs_to_turn_ON = *ucp;
	
			ucp += sizeof( UNS_8 );
		
			// 5/16/2013 rmd : This message content includes both the conventional and decoder based
			// POCs.
			if( number_of_pocs_to_turn_ON > MAX_NUMBER_OF_POCS_ON_AT_A_TIME )
			{
				alert_message_to_tpmicro_pile( "Too Many POCS trying to turn ON!" );
	
				valid_message = (false);
			}
			else
			{
				// 9/30/2014 rmd : IF the two-wire task is busy do not get the memory block. BECAUSE the
				// function that posts to the task also checks if the task is busy and if so does not post
				// to the task. That would then leave these ORPHAN memory blocks. That never get released.
				// But still however need to move the ucp pointer ahead within the message.
				if( two_wire_bus_info.task_is_busy )
				{
					ucp += ( number_of_pocs_to_turn_ON * sizeof(POC_ON_FOR_TPMICRO_STRUCT) );
				}
				else
				{
					// 5/15/2014 rmd : Obtain a block of memory that can hold the largest poc message content.
					// And take a copy of the message content and pass it to the two-wire task. This approach
					// avoids some nasty MUTEX implications when protecting the decoder_info structure from dual
					// tasks writes. The crux of the problem comes about because the two-wire task has some
					// relatively 'slow' operations it performs and therefore is holding the MUTEX for a lengthy
					// period of time.
					//
					// 5/15/2014 rmd : The memory is one byte for the number of pocs, one byte for the poc type,
					// one byte for the pump/MV signaling, and potentially an UNS_32 for the decoder serial
					// number. This is only about 40 bytes.
					twtq.ucp = GetMemBlk( sizeof(UNS_8) + (MAX_NUMBER_OF_POCS_ON_AT_A_TIME * sizeof(POC_ON_FOR_TPMICRO_STRUCT)) );
				
					if( twtq.ucp != NULL )
					{
						// 5/16/2014 rmd : Work with a tmp ucp to preserve the twtq.ucp value.
						tmp_ucp = twtq.ucp;
		
						twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_receive_pocs_ON_from_main;
						
						*(tmp_ucp) = number_of_pocs_to_turn_ON;
						
						tmp_ucp += sizeof(UNS_8);
						
						// 4/17/2013 rmd : And snatch all the records.
						memcpy( tmp_ucp, ucp, (number_of_pocs_to_turn_ON * sizeof(POC_ON_FOR_TPMICRO_STRUCT)) );
		
						// 5/14/2014 rmd : Keep the message pointer up. But not the tmp_ucp pointer as that is not
						// used again.
						ucp += ( number_of_pocs_to_turn_ON * sizeof(POC_ON_FOR_TPMICRO_STRUCT) );
		
						__post_to_two_wire_task( &twtq );
					}
					else
					{
						alert_message_to_tpmicro_pile( "no memory for pocs ON!" );
						
						// 4/17/2013 rmd : Well even though we couldn't get memory, the message may still be valid.
						// So bump ucp along.
						ucp += ( number_of_pocs_to_turn_ON * sizeof(POC_ON_FOR_TPMICRO_STRUCT) );
					}
					
				}  // of if 2w task is not busy
				
			}  // of a valid number to turn ON
			
		}  // of stations to turn ON bit
		else
		{
			// 4/17/2013 rmd : The bit is not set meaning NO pocs are ON according to the message from
			// main. But we still need to convey this to our tasks. Do that by setting the ucp ptr to
			// NULL.
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_receive_pocs_ON_from_main;
			
			twtq.ucp = NULL;
			
			// 9/30/2014 rmd : This function call checks if the 2w task is busy and if so does not post
			// to the task.
			__post_to_two_wire_task( &twtq );
		}
		
		// ----------
		
		// 9/23/2015 rmd : The LIGHTS to turn ON.
		
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_here_are_the_lights_to_turn_on ) )
		{
			// 9/23/2015 rmd : We are going to code this as if there are going to be LIGHTS DECODERS
			// someday. Right now there are not. But we'll sketch it in as if there are. What I mean by
			// that is we pass the lights information delivered here to the two_wire task. Which then in
			// turn sends it on to the irrigation task.
			
			// ----------
			
			number_of_lights_to_turn_ON = *ucp;
	
			ucp += sizeof( UNS_8 );
		
			// ----------

			// 9/24/2015 rmd : With decoders one may need to rethink this limit test.
			if( number_of_lights_to_turn_ON > MAX_NUMBER_OF_TERMINAL_LIGHTS )
			{
				alert_message_to_tpmicro_pile( "Main LIGHTS ON count too high" );
	
				valid_message = (false);
			}
			else
			{
				// 9/30/2014 rmd : IF the two-wire task is busy do not get the memory block. BECAUSE the
				// function that posts to the task also checks if the task is busy, and if so, does not post
				// to the task. That would then leave these ORPHAN memory blocks. That never get released.
				// But still however need to move the ucp pointer ahead within the message.
				if( two_wire_bus_info.task_is_busy )
				{
					ucp += ( number_of_lights_to_turn_ON * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
				}
				else
				{
					// 9/24/2015 rmd : Obtain a memory block and take a copy of this portion of the message
					// content. We are passing it to the two-wire task where it can fully parse the contents.
					// See the same behavior for stations for a more detailed explanation.
					twtq.ucp = GetMemBlk( sizeof(UNS_8) + (MAX_NUMBER_OF_TERMINAL_LIGHTS * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT)) );
				
					if( twtq.ucp != NULL )
					{
						// 5/16/2014 rmd : Work with a tmp ucp to preserve the twtq.ucp value.
						tmp_ucp = twtq.ucp;
		
						twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_receive_lights_ON_from_main;
						
						*(tmp_ucp) = number_of_lights_to_turn_ON;
						
						tmp_ucp += sizeof(UNS_8);
						
						// 4/17/2013 rmd : And snatch all the records.
						memcpy( tmp_ucp, ucp, (number_of_lights_to_turn_ON * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT)) );
		
						// 5/14/2014 rmd : Keep the message pointer up. But not the tmp_ucp pointer as that is not
						// used again.
						ucp += ( number_of_lights_to_turn_ON * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
		
						__post_to_two_wire_task( &twtq );
					}
					else
					{
						alert_message_to_tpmicro_pile( "no memory for lights ON!" );
						
						// 4/17/2013 rmd : Well even though we couldn't get memory, the message may still be valid.
						// So bump ucp along.
						ucp += ( number_of_lights_to_turn_ON * sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
					}
					
				}  // of if 2w task is not busy
				
			}  // of a valid number to turn ON
			
		}  // of stations to turn ON bit
		else
		{
			// 4/17/2013 rmd : The bit is not set meaning NO stations are ON according to the message
			// from main. But we still need to convey this to our tasks. Do that by setting the ucp ptr
			// to NULL.
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_receive_lights_ON_from_main;
			
			twtq.ucp = NULL;
			
			// 9/30/2014 rmd : This function call checks if the 2w task is busy and if so does not post
			// to the task.
			__post_to_two_wire_task( &twtq );
		}
		
		// ----------
		
		if( valid_message && B_IS_SET(fc.pieces, BIT_TTPM_here_are_the_wind_settings))
		{
			memcpy( &weather_info.wss, ucp, sizeof(WIND_SETTINGS_STRUCT) );
	
			ucp += sizeof(WIND_SETTINGS_STRUCT);
			
			// 3/25/2013 rmd : Since we've received it, clear this flag.
			weather_info.need_wind_settings_from_main_board = (false);
			
			// ----------
	
			// 2/7/2013 rmd : Save the weather info structure.
			EEPROM_MESSAGE_STRUCT	eeqm;
		
			eeqm.cmd = EE_ACTION_WRITE;
		
			eeqm.EE_address = EE_LOCATION_WEATHER_INFO;
		
			eeqm.RAM_address = (UNS_8*)&weather_info;
	
			eeqm.length = EE_SIZE_WEATHER_INFO;
			
			if( xQueueSendToBack( g_xEEPromMsgQueue, &eeqm, portNO_DELAY ) != pdTRUE )
			{
				flag_error( BIT_TPME_queue_full );
			}
		}
	
		// ----------
	
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_clear_run_away_gage_state ) )
		{
			// 2/7/2013 rmd : Because only the internal comms task touches the weather_info struct this
			// is safe to call without the use of a MUTEX. However this subject needs more study and
			// documentation within the code.
			clear_run_away_gage_state();
		}
		
		// ----------
	
		// bus - a BOOL_32 accompanies
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_two_wire_control_bus_power ) )
		{
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_control_bus_power;
	
			memcpy( &twtq.bus_on_or_off, ucp, sizeof(BOOL_32) );
	
			ucp += sizeof(BOOL_32);
			
			// 9/30/2014 rmd : This function checks if the 2w task is busy and if so does not post to
			// the task.
			__post_to_two_wire_task( &twtq );
		}
	
		// ----------
	
		// discovery - no data accompanies this one
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_two_wire_perform_discovery_process ) )
		{
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_perform_discovery_process;
	
			// 9/30/2014 rmd : This function checks if the 2w task is busy and if so does not post to
			// the task.
			__post_to_two_wire_task( &twtq );
		}
	
		// ----------
	
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_two_wire_clear_statistics_at_all_decoders ) )
		{
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_clear_statistics_at_all_decoders;
	
			// 9/30/2014 rmd : This function checks if the 2w task is busy and if so does not post to
			// the task.
			__post_to_two_wire_task( &twtq );
		}
		
		// ----------
	
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_two_wire_request_statistics_from_all_decoders ) )
		{
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_request_statistics_from_all_decoders;
	
			// 9/30/2014 rmd : This function checks if the 2w task is busy and if so does not post to
			// the task.
			__post_to_two_wire_task( &twtq );
		}
		
		// ----------
	
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_two_wire_start_all_decoder_loopback_test ) )
		{
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_start_all_decoder_loopback_test;
	
			// 9/30/2014 rmd : This function checks if the 2w task is busy and if so does not post to
			// the task.
			__post_to_two_wire_task( &twtq );
		}
		
		// ----------
	
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_two_wire_stop_all_decoder_loopback_test ) )
		{
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_stop_all_decoder_loopback_test;
	
			// 9/30/2014 rmd : This function checks if the 2w task is busy and if so does not post to
			// the task.
			__post_to_two_wire_task( &twtq );
		}
		
		// ----------
	
		// decoder operation - a 12 byte structure accompanies
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_two_wire_decoder_solenoid_on_off ) )
		{
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_decoder_solenoid_operation;
	
			memcpy( &twtq.decoder_operation, ucp, sizeof(twtq.decoder_operation) );
	
			ucp += sizeof(twtq.decoder_operation);
			
			// 9/30/2014 rmd : This function checks if the 2w task is busy and if so does not post to
			// the task.
			__post_to_two_wire_task( &twtq );
		}
		
		// ----------
	
		// set the serial number - a 4 byte serial number accompanies
		if( valid_message && B_IS_SET( fc.pieces, BIT_TTPM_two_wire_set_serial_number ) )
		{
			twtq.command = TWO_WIRE_TASK_QUEUE_COMMAND_set_serial_number;
	
			memcpy( &twtq.decoder_operation.sn, ucp, sizeof(UNS_32) );
	
			ucp += sizeof(UNS_32);
			
			// 9/30/2014 rmd : This function checks if the 2w task is busy and if so does not post to
			// the task.
			__post_to_two_wire_task( &twtq );
		}
		
	}  // end of normal message parsing
	
	// ----------

	return( valid_message );
}

/* ---------------------------------------------------------- */
void process_incoming_message_from_main( COMM_MNGR_QUEUE_STRUCT *pinternal_message_ptr)
{
	FOAL_COMMANDS			fc;

	TPL_DATA_HEADER_TYPE	*header;

	BOOL_32					valid_message;

	UNS_32					lser_no;

	// ----------
	
	valid_message = (true);

	// ----------
	
	// 3/11/2013 rmd : Pick out the serial number for the main board that sent this message. We
	// do this for two reasons. First to have reference when building an outgoing message that
	// we know which main board is connected to us. And secondly just to have it? (don't really
	// have a second reason yet). REMEMBER - The data delevered via the queue is the WHOLE
	// packet including pre and post ambles.

	header = (TPL_DATA_HEADER_TYPE*)(pinternal_message_ptr->dh.dptr + sizeof(AMBLE_TYPE));
	
	// 5/8/2014 rmd : Important to use a local variable to first extract and then determine if
	// we need to set the main board serial number used in outbound messaging. Because multiple
	// tasks use the main board serial number variable.
	lser_no = 0;

	memcpy( &lser_no, header->H.FromTo.from, 3 );
	
	if( main_board_serial_number != lser_no  )
	{
		// 5/9/2014 rmd : Capture for use in all messages we generate for the mainboard.
		main_board_serial_number = lser_no;
	}
	
	// ----------
	
	// 3/11/2013 rmd : REMEMBER - The data delivered via the queue is the WHOLE packet including
	// pre and post ambles.
	memcpy( &fc, (pinternal_message_ptr->dh.dptr + sizeof(AMBLE_TYPE) + sizeof(TPL_DATA_HEADER_TYPE)), sizeof(FOAL_COMMANDS) );

	switch( fc.mid )
	{
		case MID_MAIN_TO_TPMICRO:
			valid_message = pull_apart_primary_message_from_main( pinternal_message_ptr );
			break;

		default:
			valid_message = (false);
			break;
	}

	// ----------

	// 3/7/2013 rmd : Hey we got one from the main board so restart the timer.
	if( valid_message )
	{
		xTimerReset( last_msg_from_main_timer, portMAX_DELAY );
	}
	else
	{
		flag_error( BIT_TPME_the_message_from_main_was_invalid );

		pre_defined_alert_to_main( TPALERT_the_message_from_main_was_invalid );
	}

	// ----------

	// 3/11/2013 rmd : And make the call to possibly return the block back to the memory pool. I
	// say possibly because of the reference count. See memory pool functions to learn more.
	FreeMemBlk( pinternal_message_ptr->dh.dptr );

	// ----------
}

/* ---------------------------------------------------------- */
extern void construct_and_route_software_handshake_message_to_main_board( void )
{
	// ----------

	XMIT_QUEUE_ITEM_STRUCT	om;
	
	UNS_8			*ucp, *crc_start;
	
	FOAL_COMMANDS	fc;

	// ----------

	// 3/11/2013 rmd : We need to build the entire packet. Ambles, header, content, and crc.
	om.dh.dptr = GetMemBlk( (2*sizeof(AMBLE_TYPE)) + sizeof(TPL_DATA_HEADER_TYPE) + sizeof(FOAL_COMMANDS) + sizeof(CRC_TYPE) );

	// 3/13/2013 rmd : If memory not available flag set internal to the memory manager. And the
	// message won't be sent.
	if( om.dh.dptr != NULL )
	{
		// ----------

		om.dh.dlen = 0;

		ucp = om.dh.dptr;

		// ----------

		add_PRE_AMBLE( &om.dh, &ucp );

		// ----------

		// 3/12/2013 rmd : And the CRC starts with the header.
		crc_start = ucp;

		// ----------
		
		add_CLASS_FROM_TP_MICRO_header( &om.dh, &ucp );

		// ----------
		
		// 3/13/2013 rmd : Build and place fc into message.
		memset( &fc, 0x00, sizeof(FOAL_COMMANDS) );

		fc.mid = MID_TPMICRO_TO_MAIN_SW_HANDSHAKE;

		memcpy( ucp, &fc, sizeof(FOAL_COMMANDS) );

		ucp += sizeof( FOAL_COMMANDS );

		om.dh.dlen += sizeof( FOAL_COMMANDS );

		// ----------

		add_CRC( &om.dh, &ucp, crc_start, om.dh.dlen - sizeof(AMBLE_TYPE) );

		// ----------
	
		add_POST_AMBLE( &om.dh, &ucp );
		
		// ----------

		// 3/14/2013 rmd : The flow control sceme is between the MAIN board and port M1 (and between
		// the MAIN board and port M2 if M2 is implemented). Because this is a packet (message)
		// being built for transmission out the main port it therefore is not part of the flow
		// control scheme.
		om.give_main_goahead = (false);
	
		// ----------

		// 3/12/2013 rmd : Send the message to the main board.
		inc_memory_use_count_and_add_packet_to_xmit_queue( UPORT_MAIN, om );
		
		// 3/12/2013 rmd : And free the block. Because when the block is added to the xmit queue its
		// reference count is incremented. Which is in effect like checking out a new block. So we
		// are responsible for freeing this allocation we made.
		FreeMemBlk( om.dh.dptr );

		// ----------

	}  // of if the message memory block was allocated

}

/* ---------------------------------------------------------- */
void comm_mngr_task( void *pvParameters )
{
	UNS_32	*task_watchdog_time_stamp;
	
	COMM_MNGR_QUEUE_STRUCT		im;

	PORT_ZERO_COUNTS_STRUCT		pzcs;
	
	// ----------

	task_watchdog_time_stamp = ((TASK_TABLE_STRUCT*)pvParameters)->last_execution_time_stamp;
	
	// ----------

	// 5/22/2014 rmd : Explicitly initialize the terminal POC control structure for clarity.
	memset( &terminal_poc, 0x00, sizeof(TERMINAL_POC_FLOW_AND_SWITCH_STRUCT) );

	// 3/8/2016 mjb : Set SW1 read status flag to false. This will be used to block including the SW1
	//status in the message to main until it has been read.
	terminal_poc.sw_read_status_ready = (false); 
	
	// ----------

	while( (true) )
	{
		// 7/31/2015 rmd : The task is moving. Acquire latest time stamp for task watchdog scheme.
		*task_watchdog_time_stamp = xTaskGetTickCount();
		
		// ----------
		
		if( xQueueReceive( comm_mngr_task_queue, &im, MS_to_TICKS( 1000 ) ) )
		{
			switch( im.command )
			{

				case COMM_MNGR_CMD_develop_adc_reading_averages:
	
					develop_average_adc_readings_for_each_channel( (ADC_RAW_SAMPLES_STRUCT*)im.dh.dptr );
	
					break;
	
				// ----------
				
				// 1/10/2013 rmd : Via the one hertz repetitive interrupt timer this command is posted to
				// the queue.
				case COMM_MNGR_CMD_PERFORM_ONE_HERTZ_PROCESSING:

					// 2/12/2013 rmd : We wait until 1 second after all tasks are up and running before allowing
					// the A-D conversions to take off. Remember the A-D conversion control is piggy backed into
					// the OS 4KHz timer tick.
					commence_A_D_conversions = (true);

					// ----------
			
					// 1/15/2013 rmd : Now take a copy of the latest counts. This includes the RAIN_BUCKET,
					// TERMINAL_POC_FLOW_PULSE count, WIND_GAGE counts, and the ET_GAGE count. We must protect
					// against the RIT interrupt though. As these counts are updated within that interrupt.
					portENTER_CRITICAL();
					
					pzcs = accumulators_at_the_task_level;
					
					portEXIT_CRITICAL();
			
					// ----------

					// 6/6/2014 rmd : If the poc terminal hardware is not installed skip posting the poc flow
					// data and flagging to xfer to main.
					if( whats_installed.poc.card_present && whats_installed.poc.tb_present )
					{
						// 6/9/2014 rmd : Prevent the whole comm_mngr task from freezing if the two wire task has
						// this MUTEX.
						if( xSemaphoreTake( flow_count_MUTEX, MS_to_TICKS( 25 ) ) )
						{
							// 5/22/2014 rmd : Extract the terminal POC flow meter data.
							terminal_poc.accumulated_ms += 1000;
		
							terminal_poc.accumulated_pulses += pzcs.fm_pulse_count;
							
							terminal_poc.fm_seconds_since_last_pulse = pzcs.fm_seconds_since_last_pulse;
							
							terminal_poc.fm_delta_between_last_two_fm_pulses_4khz = pzcs.fm_delta_between_last_two_fm_pulses_4khz;
							
							terminal_poc.pulses_5_second_count[ terminal_poc.pulses_5_second_index ] = pzcs.fm_pulse_count;
							
							// 5/22/2014 rmd : And perform the index maintenance.
							terminal_poc.pulses_5_second_index += 1;
							
							if( terminal_poc.pulses_5_second_index == 5 )
							{
								terminal_poc.pulses_5_second_index = 0;	
							}
		
							// ----------
							
							// 5/22/2014 rmd : And flag indeed there is data to send to the main board.
							terminal_poc.send_to_main = (true);
							
							xSemaphoreGive( flow_count_MUTEX );
						}
						else
						{
							// 5/22/2014 rmd : So we know if this is happenening.
							alert_message_to_tpmicro_pile( "comm_mngr: flow mutex unavail!" );
						}
						
					}
					else
					{
						// 6/6/2014 rmd : Make the point. If the card or terminal is not installed do not send
						// terminal poc flow data to the master.
						terminal_poc.send_to_main = (false);
					}
					
					// ----------

					WEATHER_one_hertz_update( &pzcs );
				
					// ----------

					FUSE_one_hertz_blown_fuse_test();
					
					// ----------

					construct_and_route_outgoing_message_to_main();
				
					// ----------

					break;

				// ----------
				
				case COMM_MNGR_CMD_PROCESS_MESSAGE_FROM_MAIN:
					process_incoming_message_from_main( &im );
					break;

				// ----------
				
				case COMM_MNGR_CMD_FLAG_THIS_ERROR:
					flag_error( im.data );
					break;

				// ----------
				
				default:
					break;

			}  // of switch

		}  // if queue event rcvd

	}  // of task while true

}
