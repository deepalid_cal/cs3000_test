/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef RXCOMMTASK_H_
#define RXCOMMTASK_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"

#include	"x_uart.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//	Statistical Counters TODO: get rid of these
typedef struct
{
	unsigned short	pkts_recd_good_crc;
	unsigned short	pkts_recd_bad_crc;
	unsigned short	pkts_sent;

} Packet_Stat_s;

typedef struct
{
	Packet_Stat_s	Main;

	Packet_Stat_s	M1;

} PORT_PKT_STATS_s;


extern PORT_PKT_STATS_s		g_Port_Pkt_Stats_s;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void inc_memory_use_count_and_add_packet_to_xmit_queue( UNS_32 pxmit_port, XMIT_QUEUE_ITEM_STRUCT ppacket_to_queue );


extern void vRingBfrScanTask( void *pvParameters );


extern void add_PRE_AMBLE( DATA_HANDLE *pom, UNS_8 **pucp_ptr );

extern void add_CLASS_FROM_TP_MICRO_header( DATA_HANDLE *pom, UNS_8 **pucp_ptr );

extern void add_CRC( DATA_HANDLE *pom, UNS_8 **pucp_ptr, UNS_8 *pcrc_start, UNS_32 pcrc_length );

extern void add_POST_AMBLE( DATA_HANDLE *pom, UNS_8 **pucp_ptr );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

