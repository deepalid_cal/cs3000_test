/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#include	"tpmicro_alerts.h"

#include	"x_uart.h"

#include	"rxcommtask.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

const char * const g_alert_messages[] =
{
	"The token response grew too large",							 	// 0

	"",
	"",

	"Main station ON count too high",
	"",

	"CRC failed: UPORT_MAIN",
	"",
	"The message from main was too large",

	"Stack is in danger of overflow! Bytes Used: ",

	"Pool Memory not available",
	"Size bumped up to handle memory request",

	"Queue FULL!",  	 												// 11

	"Main RX Tail Caught index",										// 12
	"M1 RX Tail Caught index",  										// 13
	"",

	"M1 Packet too big",
	"",

	"",
	"",

	"",
	"", 																// 20
	"",

	"The field fuse is blown",

	"The message from main was invalid",

	"",																	// 24

	"Triac 1-8 Card installed",
	"Triac 1-8 Card removed",

	"Triac 9-16 Card installed",
	"Triac 9-16 Card removed",

	"Triac 17-24 Card installed",
	"Triac 17-24 Card removed",

	"Triac 25-32 Card installed",
	"Triac 25-32 Card removed",

	"Triac 33-40 Card installed",
	"Triac 33-40 Card removed",

	"Triac 41-48 Card installed",
	"Triac 41-48 Card removed",

	"Lights Card installed",
	"Lights Card removed",

	"Dash M Card installed",
	"Dash M Card removed",												// 40

	"POC Card installed",
	"POC Card removed",

	"Weather Card installed",
	"Weather Card removed",

	"",
	"",

	"Triac 1-8 Terminal installed",
	"Triac 1-8 Terminal removed",

	"Triac 9-16 Terminal installed",
	"Triac 9-16 Terminal removed",                                      // 50

	"Triac 17-24 Terminal installed",
	"Triac 17-24 Terminal removed",

	"Triac 25-32 Terminal installed",
	"Triac 25-32 Terminal removed",

	"Triac 33-40 Terminal installed",
	"Triac 33-40 Terminal removed",

	"Triac 41-48 Terminal installed",
	"Triac 41-48 Terminal removed",

	"Lights Terminal installed",
	"Lights Terminal removed",											// 60

	"Dash M Terminal installed",
	"Dash M Terminal removed",

	"POC Terminal installed",
	"POC Terminal removed",

	"Weather Terminal installed",
	"Weather Terminal removed",

	"2 Wire Terminal installed",
	"2 Wire Terminal removed",

	"CRC failed: UPORT_M1",
	"", 		   														// 70

	"Blown field fuse has been replaced",

	"Short on station: ",

	"It has started to rain",
	"It is no longer raining",

	"Temperature below freezing",
	"Temperature back above freezing",

	"Total rainfall today in inches is:",

	"Short ACK'd by main board",

	"Short on light: ",
	"Short on pump: ",  	  											// 80
	"Short on master valve: ",

	"",

	"Runaway et-gage",

	"High wind : irrigation paused",
	"Not so windy anymore : irigation resumed",

	"NO messages from main",

	"Functional ADC Error",

	"EEPROM mirror didnt match",

	"Dash M card type changed"  										// 89

};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void alert_message_to_tpmicro_pile( char *pstr )
{
	// 4/8/2013 rmd : Builds an outgoing message for the main board containing the error string.
	// Complete packet is built. This message hold only a single TP Micro alert message that
	// will show in the TP Micro alert pile at the main board. This function can be called from
	// any other function. This function secures its own block of memory to build the packet up
	// in, and then hands the packet to the serial dirver for transmission.

	// ----------

	XMIT_QUEUE_ITEM_STRUCT	om;
	
	UNS_8					*ucp, *crc_start;
	
	UNS_32					total_msg_length, alert_str_len;

	// ----------

	alert_str_len = strlen( pstr );

	if( alert_str_len < ALERTS_MAX_STORAGE_OF_TEXT_CHARS )
	{
		// ----------
	
		// 9/4/2013 rmd : Figure the complete message size and obtain the memory needed.
		total_msg_length = ( sizeof(AMBLE_TYPE) + sizeof(TPL_DATA_HEADER_TYPE) + sizeof(FOAL_COMMANDS) + sizeof(UNS_8) + alert_str_len + sizeof(CRC_TYPE) + sizeof(AMBLE_TYPE) );
		
		om.dh.dptr = GetMemBlk( total_msg_length );
		
		// 9/4/2013 rmd : Start off dh.dlen at 0 because the support function increment this as we
		// go along adding the message content.
		om.dh.dlen = 0;
		
		// ----------
	
		// 3/13/2013 rmd : If memory is not available a flag is set internal to the memory manager.
		// And the message won't be sent.
		if( om.dh.dptr != NULL )
		{
			// ----------
	
			ucp = om.dh.dptr;
	
			// ----------
	
			// 9/4/2013 rmd : Place pre-amble and bump ucp pointer and dh message length.
			add_PRE_AMBLE( &om.dh, &ucp );
	
			// ----------
	
			// 3/12/2013 rmd : And the CRC starts with the header.
			crc_start = ucp;
	
			// ----------
			
			// 9/4/2013 rmd : Place message header and bump ucp pointer and dh message length.
			add_CLASS_FROM_TP_MICRO_header( &om.dh, &ucp );
	
			// ----------
			
			// 4/3/2012 rmd : The fc structure location within the message needs to be filled out. And
			// the bits set as we build the message. To be EXTRA safe you should not simply declare a
			// ptr to a FOAL_COMMANDS structure at the current value of ucp. If ucp turned out to not be
			// ALIGNED on a word boundary and you tried to write to fc->pieces I'm pretty sure we would
			// suffer an ADDRESS EXCEPTION.
			FOAL_COMMANDS   fc;
	
			UNS_8	*where_to_place_fc_ptr;
	
			where_to_place_fc_ptr = ucp;
	
			// 2/1/2013 rmd : Filling with 0's has the desired effect of unsetting the bits in pieces.
			memset( &fc, 0x00, sizeof(FOAL_COMMANDS) );
	
			fc.mid = MID_TPMICRO_TO_MAIN_SECONDARY_MESSAGE;
	
			ucp += sizeof( FOAL_COMMANDS );
	
			om.dh.dlen += sizeof( FOAL_COMMANDS );
	
			// ----------
	
			B_SET( fc.pieces, BIT_TTMB_SECONDARY_alert_message_string );
	
			// ----------
	
			// 9/4/2013 rmd : Put the string length byte into the message.
			*ucp = alert_str_len;
			
			ucp += sizeof(UNS_8);
				
			om.dh.dlen += sizeof(UNS_8);
				
			// ----------
			
			// 9/4/2013 rmd : Put the string itself into the message.
			memcpy( ucp, pstr, alert_str_len );
	
			ucp += alert_str_len;
			
			om.dh.dlen += alert_str_len;
	
			// ----------
			
			// 4/3/2012 rmd : Done adding content. NOW put the fc structure into place.
			memcpy( where_to_place_fc_ptr, &fc, sizeof(FOAL_COMMANDS) );
	
			// ----------
		
			add_CRC( &om.dh, &ucp, crc_start, om.dh.dlen - sizeof(AMBLE_TYPE) );
	
			// ----------
		
			add_POST_AMBLE( &om.dh, &ucp );
			
			// ----------
	
			// 3/14/2013 rmd : The flow control sceme is between the MAIN board and port M1 (and between
			// the MAIN board and port M2 if M2 is implemented). Because this is a packet (message)
			// being built for transmission out the main port it therefore is not part of the flow
			// control scheme.
			om.give_main_goahead = (false);
		
			// 3/12/2013 rmd : Send the message to the main board.
			inc_memory_use_count_and_add_packet_to_xmit_queue( UPORT_MAIN, om );
			
			// ----------

			// 3/12/2013 rmd : And free the block. Because when the block is added to the xmit queue its
			// reference count is incremented. Which is in effect like checking out a new block. So we
			// are responsible for freeing this allocation we made. No matter if we send to the xmit
			// queue or not, we are responsible to free this block.
			FreeMemBlk( om.dh.dptr );
	
		}  // of if the message memory block was allocated
		
	}
	else
	{
		// 9/4/2013 rmd : String length exceeds our maximum allowable. Make a recursive call, to
		// send a short alert about such. And this works. I have tested it.
		alert_message_to_tpmicro_pile( "Alert Message: string too long!" );
	}
	
}

/* ---------------------------------------------------------- */
// 4/29/2013 rmd : Specific for two_wire decoder errors. Uses a global buffer to keep the
// buffer off the task stack. Vurnable to corruption as no MUTEX used! However these
// functions are only called from the two_wire task function.
char	decoder_str_48[ 48 ];

extern void decoder_alert_message_to_tpmicro_pile( UNS_32 pser_no, char *pstr )
{
	snprintf( decoder_str_48, sizeof(decoder_str_48), "%06d %s", pser_no, pstr );
	
	alert_message_to_tpmicro_pile( decoder_str_48 );
}

extern void decoder_output_alert_message_to_tpmicro_pile( UNS_32 pser_no, UNS_32 poutput, char *pstr )
{
	snprintf( decoder_str_48, sizeof(decoder_str_48), "%06d %c - %s", pser_no, poutput+'A', pstr );
	
	alert_message_to_tpmicro_pile( decoder_str_48 );
}

/* ---------------------------------------------------------- */
extern void pre_defined_alert_to_main( UNS_32 pcannedResponseIndex )
{
	alert_message_to_tpmicro_pile( (char*)g_alert_messages[ pcannedResponseIndex ] );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Sends a premade string with an embedded 32bit number at the end to our
    alert screen on the main board.  This function constructs a message for the internal
    comms to handle and route to the main board.

	@PARAMETERS

		pcannedResponseIndex: an index into the alert messages defined at the top of internalComms.c

		ptailEnd: a 32 bit number that is appended to the end of the string

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITIES(none)

	@EXECUTED Anywhere, except ISRs!!!

	@RETURN (none)

	@ORIGINAL 8/15/2012

	@REVISIONS
*/
extern void alert_electrical_short_to_main( int pcannedResponseIndex, UNS_32 ptailEnd )
{
	char	*str_ptr;

	// 4/8/2013 rmd : I think it better to pull from the memory pool than to impose upon the
	// stack.
	str_ptr = GetMemBlk( ALERTS_MAX_STORAGE_OF_TEXT_CHARS );
	
	// 3/13/2013 rmd : If memory not available flag set internal to the memory manager. And this
	// data won't be sent.
	if( str_ptr != NULL )
	{
		snprintf( str_ptr, ALERTS_MAX_STORAGE_OF_TEXT_CHARS, "%s %d", g_alert_messages[ pcannedResponseIndex ], ptailEnd );
		
		alert_message_to_tpmicro_pile( str_ptr );
		
		FreeMemBlk( str_ptr );
	}
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Generates an alert line for xmission to the main board.
	
	@CALLER_MUTEX_REQUIREMENTS (none)
	
	@MEMORY_RESPONSIBILITES (none)
	
    @EXECUTED Executed within the context of the RING BUFFER SCAN tasks if a crc fails when
    checking the crc of rcvd packets from any port (M1, M2, MAIN).

	@RETURN (none)
	
	@ORIGINAL 2013.03.26 rmd
	
	@REVISIONS (none)
*/
extern void show_crc_error( UNS_32 calculated_crc, UNS_32 crc_to_test )
{
	char	str_40[ 40 ];
	
	snprintf( str_40, 40, "calc: %08x, msg: %08x", calculated_crc, crc_to_test );
	
	alert_message_to_tpmicro_pile( str_40 );
}
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

