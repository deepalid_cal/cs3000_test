/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_TPMICRO_ALERTS_H
#define _INC_TPMICRO_ALERTS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	TPALERT_constructed_message_length_too_large					(0)

#define	no_longer_used_1												(1)
#define	no_longer_used_2												(2)

#define	TPALERT_main_requested_too_many_stations_on						(3)
#define	no_longer_used_4												(4)

#define	TPALERT_CRC_Failed_Port_Main									(5)
#define	no_longer_used_6												(6)
#define	TPALERT_message_fom_main_was_too_large							(7)

#define	TPALERT_main_stack_is_in_danger_of_overflow						(8)

#define	TPALERT_we_couldnt_get_a_suitable_heap_block					(9)
#define	TPALERT_had_to_pull_from_the_next_size_heap_block				(10)

#define	TPALERT_a_queue_was_full_when_it_was_added_to					(11)

#define	TPALERT_main_rx_tail_caught_index								(12)
#define	TPALERT_M1_rx_tail_caught_index									(13)
#define	no_longer_used_14												(14)

#define	TPALERT_message_fom_M1_was_too_large							(15)
#define	no_longer_used_16												(16)

#define	no_longer_used_17												(17)
#define	no_longer_used_18												(18)

#define	no_longer_used_19												(19)
#define	no_longer_used_20												(20)

#define	no_longer_used_21												(21)

#define	TPALERT_field_power_fuse_blown									(22)

#define	TPALERT_the_message_from_main_was_invalid						(23)

#define	no_longer_used_24												(24)

#define	TPALERT_Triac_1_8_card_installed								(25)
#define	TPALERT_Triac_1_8_card_removed									(26)

#define	TPALERT_Triac_9_16_card_installed								(27)
#define	TPALERT_Triac_9_16_card_removed									(28)

#define	TPALERT_Triac_17_24_card_installed								(29)
#define	TPALERT_Triac_17_24_card_removed								(30)

#define	TPALERT_Triac_25_32_card_installed								(31)
#define	TPALERT_Triac_25_32_card_removed								(32)

#define	TPALERT_Triac_33_40_card_installed								(33)
#define	TPALERT_Triac_33_40_card_removed								(34)

#define	TPALERT_Triac_41_48_card_installed								(35)
#define	TPALERT_Triac_41_48_card_removed								(36)

#define	TPALERT_Lights_card_installed									(37)
#define	TPALERT_Lights_card_removed										(38)

#define	TPALERT_Dash_M_card_installed									(39)
#define	TPALERT_Dash_M_card_removed										(40)

#define	TPALERT_POC_card_installed										(41)
#define	TPALERT_POC_card_removed										(42)

#define	TPALERT_Weather_card_installed									(43)
#define	TPALERT_Weather_card_removed									(44)

#define	no_longer_used_45												(45)
#define	no_longer_used_46												(46)

#define	TPALERT_Triac_1_8_tb_installed									(47)
#define	TPALERT_Triac_1_8_tb_removed									(48)

#define	TPALERT_Triac_9_16_tb_installed									(49)
#define	TPALERT_Triac_9_16_tb_removed									(50)

#define	TPALERT_Triac_17_24_tb_installed								(51)
#define	TPALERT_Triac_17_24_tb_removed									(52)

#define	TPALERT_Triac_25_32_tb_installed								(53)
#define	TPALERT_Triac_25_32_tb_removed									(54)

#define	TPALERT_Triac_33_40_tb_installed								(55)
#define	TPALERT_Triac_33_40_tb_removed									(56)

#define	TPALERT_Triac_41_48_tb_installed								(57)
#define	TPALERT_Triac_41_48_tb_removed									(58)

#define	TPALERT_Lights_tb_installed										(59)
#define	TPALERT_Lights_tb_removed										(60)

#define	TPALERT_Dash_M_tb_installed										(61)
#define	TPALERT_Dash_M_tb_removed										(62)

#define	TPALERT_POC_tb_installed										(63)
#define	TPALERT_POC_tb_removed											(64)

#define	TPALERT_Weather_tb_installed									(65)
#define	TPALERT_Weather_tb_removed										(66)

#define	TPALERT_2Wire_tb_installed										(67)
#define	TPALERT_2Wire_tb_removed										(68)

#define	TPALERT_CRC_Failed_Port_M1										(69)
#define	no_longer_used_70												(70)

#define	TPALERT_field_power_fuse_okay									(71)

#define	TPALERT_short_on_this_station									(72)

#define TPALERT_it_has_started_to_rain									(73)
#define TPALERT_it_is_no_longer_raining									(74)

#define TPALERT_freeze_conditions_are_present							(75)
#define TPALERT_freeze_conditions_are_over								(76)

#define TPALERT_our_total_rainfall_for_today_in_inches_is				(77)

#define TPALERT_main_acknowledged_short									(78)

#define	TPALERT_a_short_happened_on_this_light							(79)
#define	TPALERT_a_short_happened_on_this_pump							(80)
#define	TPALERT_a_short_happened_on_this_master_valve					(81)

#define no_longer_used_82												(82)

#define TPALERT_we_have_runaway_et_gage_conditions						(83)

#define	TPALERT_we_paused_irrigation_due_to_windy_conditions			(84)
#define TPALERT_we_resumed_irrigation_that_was_paused_by_wind			(85)

#define TPALERT_no_msgs_from_main										(86)

#define TPALERT_functional_adc_error									(87)

#define TPALERT_our_eeprom_mirro_didnt_match_our_critical_section		(88)

#define	TPALERT_Dash_M_card_type_changed								(89)

// ----------

extern const char * const g_alert_messages[];

// ----------

extern void alert_message_to_tpmicro_pile( char *pstr );


extern void decoder_alert_message_to_tpmicro_pile( UNS_32 pser_no, char *pstr );

extern void decoder_output_alert_message_to_tpmicro_pile( UNS_32 pser_no, UNS_32 poutput, char *pstr );


extern void pre_defined_alert_to_main( UNS_32 pcannedResponseIndex );

extern void alert_electrical_short_to_main( int pcannedResponseIndex, UNS_32 ptailEnd );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

