/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"rxcommtask.h"

#include	"crc.h"

#include	"ringbuffer.h"

#include	"GpioPinChg.h"

#include	"IAP.h"


#if UART_TX_DMA != 0
	#include "x_uart.h"
#else
	#include "uart.h"
#endif

#include	"tpmicro_comm_mngr.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/8/2014 rmd : There is NO tpmicro serial number requirement at the main board. The
// packet is accepted based upon the message class and the to serial number (the main board
// serial number) not the from serial number. So there is NO tpmicro serial number. However
// we stuff with this fixed value for recognition purposes if needed during development.
#define	TPMICRO_SERIAL_NUMBER		(0x00FCFBFA)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//	Statistical Counters for packet communications
PORT_PKT_STATS_s	g_Port_Pkt_Stats_s;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 3/11/2013 rmd : This function expects a full ready to go packet in memory from our
// partition pool. Instead of obtaining a new packet from the pool so that we would take a
// copy of this packet we bump the allocation count (also called the reference count). This
// plays into the scheme where this packet is being sent out of multiple ports at the same
// time. Using the reference count to manage when this packet should be freed, when the last
// serial driver is done the memory will finally be freed for real.
extern void inc_memory_use_count_and_add_packet_to_xmit_queue( UNS_32 pxmit_port, XMIT_QUEUE_ITEM_STRUCT ppacket_to_queue )
{
	volatile UART_CTL_s		*pUartCtl_s;
	
	// ----------

	// 3/14/2013 rmd : If the caller is asking for a go ahead signal for a packet going out the
	// main port the caller has messed up. Only packets out ports M1 or M2 participate in the
	// flow control scheme.
	if( (pxmit_port == UPORT_MAIN) && ppacket_to_queue.give_main_goahead )
	{
		alert_message_to_tpmicro_pile( "Unexp Flow Control Request" );
	}
	
	// ----------

	//  Get pointer to this port's UART control structure
	pUartCtl_s = &(uart_control[ pxmit_port ]);

	// ----------

	// 3/11/2013 rmd : Bump the memory use count as we prepare to place a reference to the block
	// on the xmit queue.
	IncrementMemBlkRefCount( ppacket_to_queue.dh.dptr );
	
	// ----------

	// 11/15/2012 rmd : After transmission a call to free the memory block will be made.
	if( xQueueSendToBack( pUartCtl_s->xTxDataHandleQueue, &ppacket_to_queue, portNO_DELAY ) )
	{
		// ----------

		// 11/15/2012 rmd : Update statistics.
		switch( pxmit_port )
		{
			case UPORT_MAIN:
				g_Port_Pkt_Stats_s.Main.pkts_sent++;
				break;
			
			case UPORT_M1:
				g_Port_Pkt_Stats_s.M1.pkts_sent++;
				break;
		}

		// ----------
	}
	else
	{
		// 3/11/2013 rmd : If we didn't achieve a success queue we must free the block here. We
		// already incremented the use count. And if not queued will be missing a free mem call.
		FreeMemBlk( ppacket_to_queue.dh.dptr );

		flag_error( BIT_TPME_queue_full );

		// 3/14/2013 rmd : Well we've failed to queue  a packet. Suppose the packet was a packet
		// that required flow control. The serial driver in the main board would be waiting for a go
		// ahead event. So we had better send one. If the driver isn't waiting for the go ahead this
		// won't hurt.
		if( ppacket_to_queue.give_main_goahead )
		{
			construct_and_route_software_handshake_message_to_main_board();
		}
	}

}

/* ---------------------------------------------------------- */
static void route_incoming_packet( UNS_32 pfrom_port, DATA_HANDLE pfull_packet_with_ambles )
{
	COMM_MNGR_QUEUE_STRUCT	intMsg_s;

	TPL_DATA_HEADER_TYPE	*header;
	
	DATA_HANDLE				crc_check_handle;
	
	XMIT_QUEUE_ITEM_STRUCT	qi;

	ROUTING_CLASS_DETAILS_STRUCT	routing;
	
	// ----------

	// 3/11/2013 rmd : Check the CRC verify packet content. Build a data handle for the CRC test
	// function. The data ptr should start at the start of the header. And the length should not
	// include the pre and post ambles.
	crc_check_handle.dptr = pfull_packet_with_ambles.dptr + sizeof(AMBLE_TYPE);

	crc_check_handle.dlen = pfull_packet_with_ambles.dlen - (2*sizeof(AMBLE_TYPE));

	if( !CRCIsOK( crc_check_handle ) )
	{
		// 3/12/2013 rmd : CRC failed. Flag the error.
		switch( pfrom_port )
		{
			case UPORT_MAIN:
				pre_defined_alert_to_main( TPALERT_CRC_Failed_Port_Main );
				break;
			case UPORT_M1:
				pre_defined_alert_to_main( TPALERT_CRC_Failed_Port_M1 );
				break;
		}
		
		// 3/14/2013 rmd : Well we got a packet. So there was some attempt to send us something.
		// Suppose the packet was a packet that required flow control. The serial driver in the main
		// board would be waiting for a go ahead event. So we had better send one. If the driver
		// isn't waiting for the go ahead this won't hurt.
		if( pfrom_port == UPORT_MAIN )
		{
			// 3/13/2013 rmd : Okay so the memory was actually freed. Tell the main board about this.
			construct_and_route_software_handshake_message_to_main_board();
		}
		
	}
	else
	{
		// 3/14/2013 rmd : Prepare the queue item.
		qi.give_main_goahead = (false);

		qi.dh = pfull_packet_with_ambles; 

		// ----------

		header = (TPL_DATA_HEADER_TYPE*)(pfull_packet_with_ambles.dptr + sizeof(AMBLE_TYPE));

		// ----------

		get_this_packets_message_class( header, &routing );
		
		if( (routing.routing_class_base == ROUTING_CLASS_IS_3000_BASED) && (routing.rclass == MSG_CLASS_CS3000_TO_TP_MICRO) )
		{
			// 3/11/2013 rmd : We only accept packets TO_TP_MICRO from the MAIN port.
			if( pfrom_port == UPORT_MAIN )
			{
				// 3/11/2013 rmd : Bump the memory use count as we prepare to place a reference to the block
				// on the incoming message queue. When the task is done processing it will make a call to
				// free.
				IncrementMemBlkRefCount( pfull_packet_with_ambles.dptr );
				
				// 1/29/2013 rmd : We only rely upon the msg class and the port it is from to decide if we
				// are going to accept this as a message for our tpmicro. We do not look at the from or to
				// addressses.
				intMsg_s.command = COMM_MNGR_CMD_PROCESS_MESSAGE_FROM_MAIN;

				intMsg_s.dh = pfull_packet_with_ambles;

				xQueueSendToBack( comm_mngr_task_queue, &intMsg_s, portMAX_DELAY );

				// ----------

				// 3/8/2013 rmd : Signal the startup task that a packet from main to us has been received.
				// Since at the startup task level, we have not created the COMM_MNGR_TASK task and are
				// pending on this semaphore, we must GIVE this semaphore at the packet level. Not at the
				// message processing level.
				xSemaphoreGive( packet_rcvd_from_main_semaphore );
			}
		}
		else
		{
			if( pfrom_port == UPORT_MAIN )
			{
				// 3/14/2013 rmd : Because it came in the MAIN PORT this packet qualifies for go ahead when
				// xmission is done.
				qi.give_main_goahead = (true);
				
				// 4/2/2014 rmd : If it came in MAIN and it isn't the TPMICRO class we ALWAYS push it out
				// M1. Disregard routing rules.
				inc_memory_use_count_and_add_packet_to_xmit_queue( UPORT_M1, qi );
			}
			else
			if( pfrom_port == UPORT_M1 )
			{
				// 4/2/2014 rmd : If it came in M1 we ALWAYS give to MAIN. It certainly isn't for the
				// tpmicro.
				inc_memory_use_count_and_add_packet_to_xmit_queue( UPORT_MAIN, qi );
			}
			
			// 3/16/2018 rmd : ELSE the packet is dropped.
		}
		
	}  // of if CRC passed

	// ----------
	
	// 3/11/2013 rmd : Make a call to free the memory for the packet. Any other process that
	// need it to remain allocated (xmit driver or message parsing) has incremented the use
	// count. So this free will only decrement the count and not actually return the block to
	// the pool for re-use.
	FreeMemBlk( pfull_packet_with_ambles.dptr );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void RCVD_DATA_enable_packethunting( UNS_32 pport )
{
	volatile UART_CTL_s			*pUartCtl_s;

	volatile UART_RINGBUFFER_s	*ptr_ring_buffer_s;		// for convenience

	//  Get pointer to this port's UART control structure
	pUartCtl_s = &uart_control[ pport ];

	ptr_ring_buffer_s = (UART_RINGBUFFER_s *) &pUartCtl_s->ringbuffer_s;

	taskENTER_CRITICAL();

	// Zero out all the PACKET HUNT structure variables
	memset( (void *) &ptr_ring_buffer_s->ph, 0, sizeof(ptr_ring_buffer_s->ph));

	// so no matter what mode we are entering we 0 the tail...we rarely change modes and it is acceptable that
	// other communications that may be going on at the time of a mode change won't survive
	ptr_ring_buffer_s->ringtail = 0;

	ptr_ring_buffer_s->hunt_for_packets = TRUE;

	taskEXIT_CRITICAL();
}

/* ---------------------------------------------------------- */
void RCVD_DATA_disable_packethunting( UNS_32 pport )
{
	volatile UART_CTL_s *pUartCtl_s;

	//  Get pointer to this port's UART control structure
	pUartCtl_s = &uart_control[ pport ];

	pUartCtl_s->ringbuffer_s.hunt_for_packets = FALSE;
}

/* ---------------------------------------------------------- */
void check_ring_buffer_for_CalsenseTPL_packet( UNS_32 pport )
{
	volatile UART_CTL_s *pUartCtl_s;

	volatile UART_RINGBUFFER_s  *ptr_ring_buffer_s;		// for convenience

	unsigned short  itc1,itc2,itc3,itc4;

	unsigned char   *tptr;

	unsigned short  plen,psri;

	DATA_HANDLE		found_packet;


	//  Get pointer to this port's UART control structure
	pUartCtl_s = &uart_control[ pport ];

	ptr_ring_buffer_s = (UART_RINGBUFFER_s *) &pUartCtl_s->ringbuffer_s;

	// loop around looking for either the pre or post amble until we strike the tail
	do
	{
		itc1 = ptr_ring_buffer_s->ph.packet_index;
		if( itc1 == ptr_ring_buffer_s->ringtail ) return;

		itc2 = itc1+1; if( itc2 == MX_RINGBUFFER_SIZE )	itc2=0;
		if( itc2 == ptr_ring_buffer_s->ringtail ) return;

		itc3 = itc2+1; if( itc3 == MX_RINGBUFFER_SIZE )	itc3=0;
		if( itc3 == ptr_ring_buffer_s->ringtail ) return;

		itc4 = itc3+1; if( itc4 == MX_RINGBUFFER_SIZE )	itc4=0;
		if( itc4 == ptr_ring_buffer_s->ringtail ) return;

		if( ptr_ring_buffer_s->ringbuffer[ itc1 ] == preamble._1 )
		{
			if( ptr_ring_buffer_s->ringbuffer[ itc2 ] == preamble._2 )
			{
				if( ptr_ring_buffer_s->ringbuffer[ itc3 ] == preamble._3 )
				{
					if( ptr_ring_buffer_s->ringbuffer[ itc4 ] == preamble._4 )
					{
						ptr_ring_buffer_s->ph.ringhead1 = itc1;	   /* head points to the preamble */
						ptr_ring_buffer_s->ph.ringhead2 = itc2;
						ptr_ring_buffer_s->ph.ringhead3 = itc3;
						ptr_ring_buffer_s->ph.ringhead4 = itc4;

						// 3/11/2013 rmd : The length will include the pre and post amble lengths. So start it with
						// the pre amble length included.
						ptr_ring_buffer_s->ph.packetlength = sizeof(AMBLE_TYPE);

						// 3/11/2013 rmd : Because it is the job of the TP Micro to re-route a good portion of the
						// packets we see, pick out whole intact packets from this packet hunter. Including pre and
						// post ambles. That way they are easy to re-route without further memory allocation work
						// needed to add the ambles.
						ptr_ring_buffer_s->ph.datastart = itc1;		// Point to the start of PREAMBLE.
					}
				}
			}
		}

		if( ptr_ring_buffer_s->ringbuffer[ itc1 ] == postamble._1 )
		{
			if( ptr_ring_buffer_s->ringbuffer[ itc2 ] == postamble._2 )
			{
				if( ptr_ring_buffer_s->ringbuffer[ itc3 ] == postamble._3 )
				{
					if( ptr_ring_buffer_s->ringbuffer[ itc4 ] == postamble._4 )
					{
						// check the preamble is still there. if not move on
						if( (ptr_ring_buffer_s->ringbuffer[ ptr_ring_buffer_s->ph.ringhead1 ] == preamble._1) &&
							(ptr_ring_buffer_s->ringbuffer[ ptr_ring_buffer_s->ph.ringhead2 ] == preamble._2) &&
							(ptr_ring_buffer_s->ringbuffer[ ptr_ring_buffer_s->ph.ringhead3 ] == preamble._3) &&
							(ptr_ring_buffer_s->ringbuffer[ ptr_ring_buffer_s->ph.ringhead4 ] == preamble._4) )
						{
							// PRE-AMBLE STILL GOOD.

							// 3/22/2013 rmd : Check against our defined maximum packet size. Remember this size is
							// tightly coupled with our memory pool capabilities.
							if( ptr_ring_buffer_s->ph.packetlength > TPMICRO_MAX_AMBLE_TO_AMBLE_PACKET_SIZE )
							{
								switch( pport )
								{
									case UPORT_MAIN:
										{
											pre_defined_alert_to_main( TPALERT_message_fom_main_was_too_large );
											break;
										}
									case UPORT_M1:
										{
											pre_defined_alert_to_main( TPALERT_message_fom_M1_was_too_large );
											break;
										}
								}
							}
							else
							{
								// 3/11/2013 rmd : Get a block of memory to put the packet, pre and post ambles included
								// into memory.
								found_packet.dlen = ptr_ring_buffer_s->ph.packetlength;

								// 3/13/2013 rmd : Obtain a block from the memory pool. If unsuccessful the packet will
								// effectively be dropped and, internal to the memory pool management, an error flag will be
								// set.
								found_packet.dptr = GetMemBlk( found_packet.dlen );

								if( found_packet.dptr != NULL )
								{
									tptr = found_packet.dptr;

									psri = ptr_ring_buffer_s->ph.datastart;

									plen = 0;
									do
									{
										*tptr++ = ptr_ring_buffer_s->ringbuffer[ psri++ ];

										// Handle ring buffer pointer wraparound..
										if( psri == MX_RINGBUFFER_SIZE )
										{
											psri=0;
										}

										plen++;

									} while( plen < ptr_ring_buffer_s->ph.packetlength );	// Copy thru last byte of data

									route_incoming_packet( pport, found_packet );
								}
								
							}  /* of packet size makes sense */

						}  /* of if the preamble is still there */

						// reset these to guarantee new packet start must be found
						ptr_ring_buffer_s->ph.ringhead1 = itc1;
						ptr_ring_buffer_s->ph.ringhead2 = itc1;
						ptr_ring_buffer_s->ph.ringhead3 = itc1;
						ptr_ring_buffer_s->ph.ringhead4 = itc1;

					}  /* post amble check 4 */

				}  /* post amble check 3 */

			}  /* post amble check 2 */

		}  /* post amble check 1 */

		// each loop move index along
		if( ++ptr_ring_buffer_s->ph.packet_index == MX_RINGBUFFER_SIZE )
		{
			ptr_ring_buffer_s->ph.packet_index = 0;
		}

		ptr_ring_buffer_s->ph.packetlength++;  // each loop makes the packet 1 bigger

	} while( TRUE );  // of do while loop
}

/* ---------------------------------------------------------- */
void vRingBfrScanTask( void *pvParameters )
{
	UNS_32	*task_watchdog_time_stamp;
	
	volatile UART_CTL_s *pUartCtl;

	UNS_32	port;

	volatile UART_RINGBUFFER_s  *ptr_ring_buffer;		// for convenience - easier code to read

	// ----------

	task_watchdog_time_stamp = ((TASK_TABLE_STRUCT*)pvParameters)->last_execution_time_stamp;
	
	// 7/31/2015 rmd : Obtain the port number this task instance is for.
	port = (UNS_32)(((TASK_TABLE_STRUCT*)pvParameters)->task_info);

	// ----------
	
	//  Get pointer to this port's UART control structure
	pUartCtl = &uart_control[ port ];

	// create a ptr to the ring buffer controlling struct
	ptr_ring_buffer = (UART_RINGBUFFER_s *)&pUartCtl->ringbuffer_s;

	// ----------
	
	RCVD_DATA_enable_packethunting( port );

	// ----------
	
	while( (true) )
	{
		// 7/31/2015 rmd : Task is moving. Grab latest time stamp for task watchdog scheme.
		*task_watchdog_time_stamp = xTaskGetTickCount();

		// ----------
		
		if( xSemaphoreTake( pUartCtl->xRxDataAvailSema, MS_to_TICKS( 1000 ) ) )
		{
			if( ptr_ring_buffer->ph_tail_caught_index == TRUE )
			{
				ptr_ring_buffer->ph_tail_caught_index = FALSE;
	
				switch( (int)pvParameters )
				{
					case UPORT_MAIN:
						{
							flag_error( BIT_TPME_main_rx_tail_caught_index );
	
							pre_defined_alert_to_main( TPALERT_main_rx_tail_caught_index );
	
							break;
						}
					case UPORT_M1:
						{
							flag_error( BIT_TPME_M1_rx_tail_caught_index );
	
							pre_defined_alert_to_main( TPALERT_M1_rx_tail_caught_index );
	
							break;
						}
				}
				
				// 12/2/2013 rmd : We used to include a DEBUG_STOP line here. But gee ... if we suffered
				// this error and made the above alert line and then hit the DEBUG stop line we'd never see
				// the alert line. Additionally the error is not critical meaning leads to a missed packet
				// but won't lead to a crash.
			}
	
			// ----------
			
			if( ptr_ring_buffer->hunt_for_packets )
			{
				check_ring_buffer_for_CalsenseTPL_packet( port );
			}
			
		}  // of if semaohore rcvd

	}  // of task while forever

}

/* ---------------------------------------------------------- */
extern void add_PRE_AMBLE( DATA_HANDLE *pom, UNS_8 **pucp_ptr )
{
	// 9/4/2013 rmd : This function used while building an outgoing message to the main board.
	// Copy in the pre-amble, incrementing the user pointer as we go along. And finally updating
	// the message length.

	**pucp_ptr = preamble._1;
	*pucp_ptr += 1;
	
	**pucp_ptr = preamble._2;
	*pucp_ptr += 1;
	
	**pucp_ptr = preamble._3;
	*pucp_ptr += 1;
	
	**pucp_ptr = preamble._4;
	*pucp_ptr += 1;
	
	pom->dlen += sizeof( AMBLE_TYPE );
}

/* ---------------------------------------------------------- */
extern void add_CLASS_FROM_TP_MICRO_header( DATA_HANDLE *pom, UNS_8 **pucp_ptr )
{
	TPL_DATA_HEADER_TYPE	header;
	
	memset( &header, 0x00, sizeof(TPL_DATA_HEADER_TYPE) );
	
	set_this_packets_CS3000_message_class( &header, MSG_CLASS_CS3000_FROM_TP_MICRO );
	
	// 4/3/2012 rmd : We set the FROM address with our serial number. I think this is not even
	// used at the main board. All the main board does is to look at the msg class.
	header.H.FromTo.from[ 2 ] = (TPMICRO_SERIAL_NUMBER)&0xFF;
	header.H.FromTo.from[ 1 ] = (TPMICRO_SERIAL_NUMBER>>8)&0xFF;
	header.H.FromTo.from[ 0 ] = (TPMICRO_SERIAL_NUMBER>>16)&0xFF;
	
	// 2/25/2013 rmd : Sending TO the address we last recv'd from. In BIG endian format (our
	// packet standard).
	header.H.FromTo.to[ 2 ] = ((main_board_serial_number      ) & 0xFF);
	header.H.FromTo.to[ 1 ] = ((main_board_serial_number >> 8 ) & 0xFF);
	header.H.FromTo.to[ 0 ] = ((main_board_serial_number >> 16) & 0xFF);
	
	header.H.ThisBlock = 1;
	header.H.TotalBlocks = 1;
	
	// 3/12/2013 rmd : And put the header into the packet.
	memcpy( *pucp_ptr, &header, sizeof(TPL_DATA_HEADER_TYPE) );
	
	*pucp_ptr +=  sizeof( TPL_DATA_HEADER_TYPE );
	
	pom->dlen += sizeof( TPL_DATA_HEADER_TYPE );
}

/* ---------------------------------------------------------- */
extern void add_CRC( DATA_HANDLE *pom, UNS_8 **pucp_ptr, UNS_8 *pcrc_start, UNS_32 pcrc_length )
{
	CRC_TYPE	lcrc;
	
	// 3/12/2013 rmd : Figure and place the crc into the message.
	lcrc.crc = CRC_calculate_32bit_big_endian( pcrc_start, pcrc_length );
	
	// 11/7/2012 rmd : Move crc into message. Retaining big endian byte order that it was
	// caluclated with. Remember all crcs are big endian format throughout all of Calsense.
	**pucp_ptr = lcrc.b[ 0 ];
	*pucp_ptr += 1;
	
	**pucp_ptr = lcrc.b[ 1 ];
	*pucp_ptr += 1;
	
	**pucp_ptr = lcrc.b[ 2 ];
	*pucp_ptr += 1;
	
	**pucp_ptr = lcrc.b[ 3 ];
	*pucp_ptr += 1;
	
	pom->dlen += sizeof(CRC_TYPE);
}

/* ---------------------------------------------------------- */
extern void add_POST_AMBLE( DATA_HANDLE *pom, UNS_8 **pucp_ptr )
{
	// 3/12/2013 rmd : Put in the pre amble.
	**pucp_ptr = postamble._1;
	*pucp_ptr += 1;
	
	**pucp_ptr = postamble._2;
	*pucp_ptr += 1;
	
	**pucp_ptr = postamble._3;
	*pucp_ptr += 1;
	
	**pucp_ptr = postamble._4;
	*pucp_ptr += 1;
	
	pom->dlen += sizeof( AMBLE_TYPE );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

