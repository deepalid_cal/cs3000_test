/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_TPMICRO_COMM_MNGR_H
#define _INC_TPMICRO_COMM_MNGR_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define DEBUG_SHOW_CPU_STATS					(1)


#define MAX_LINES_FOR_MAIN_DISLAY				(9)

#define MAX_CHARS_PER_LINE_FOR_MAIN_DISPLAY 	(38)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Number of queue entries for Internal Comms messages.
#define COMM_MNGR_QUEUE_LENGTH											(15)


#define COMM_MNGR_CMD_PERFORM_ONE_HERTZ_PROCESSING						(1)

#define COMM_MNGR_CMD_PROCESS_MESSAGE_FROM_MAIN							(2)

#define COMM_MNGR_CMD_FLAG_THIS_ERROR									(3)

#define COMM_MNGR_CMD_develop_adc_reading_averages						(4)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_32			command;

	UNS_32			data;

	DATA_HANDLE		dh;

} COMM_MNGR_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32	main_board_serial_number;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void TIMER_no_msg_from_main_handler( xTimerHandle xTimer );


extern void construct_and_route_software_handshake_message_to_main_board( void );


extern void comm_mngr_task( void *pvParamaters );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

