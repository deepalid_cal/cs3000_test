/* ---------------------------------------------------------- */

#include "utils.h"




/* ---------------------------------------------------------- */
UNS_8 getByteFromLongLong( UNS_64 *pLongLong, UNS_8 byte )
{
	UNS_64	temp;
	
	UNS_8	val8;
	
	temp = *pLongLong;

	temp >>= (8 * byte);   // shift bits of interest into d7..d0

	val8 = (UNS_8) (temp & (UNS_64)0x00000000000000ff);          // keep only 8 bits
	
	return( val8 );
}

//-----------------------------------------------------------------------------
//    UNS_8 putByteIntoLongLong( volatile unsigned long long *pLongLong, UNS_8 byte, UNS_8 data)
//
//    This function updates the specified byte number in a long long 
//    variable with the 8-bit data value provided.
//-----------------------------------------------------------------------------
void putByteIntoLongLong( UNS_64 *pLongLong, UNS_8 byte, UNS_8 data )
{
	UNS_64	temp;
	
	// ----------

	// First, clear byte of interest in target long long
	temp = (UNS_64)0xff << (8 * byte);

	*pLongLong &= ~temp;
	
	// ----------

	// Now move new data to target byte position
	temp = (UNS_64)data << (8 * byte);
	
	// Finally, OR in the new data in place
	*pLongLong |= temp;

	// ----------
}

/* ---------------------------------------------------------- */
extern char *RemovePathFromFileName( char *str1 )
{
	// this function assumes that str1 points to a pathname+filename
	// if the last char is a \ it will return a pointer to the terminating null
	// this function assumes there is a terminating null
	//
	// moves through str1 finding the \'s and then pretending the string
	// start just after each occurance
		  
	char *ptr_to_char;
	
	// if there are no \'s will return ptr to original string
	char *just_beyond_last_occurance = str1;

	// in the gcc environment the file names have forward slashes instead of
	// back slashes
	ptr_to_char = strrchr( str1, '/' );  // strrchr finds the last occurance

	if( ptr_to_char != NULL )
	{
		ptr_to_char++;  // hop over it

		just_beyond_last_occurance = ptr_to_char;
	}

	return( just_beyond_last_occurance );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

