/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"comm_utils.h"

#include	"hardwaremanager.h"

#include	"irrigation_manager.h"

#include	"tpmicro_comm_mngr.h"

#include	"tpmicro_comm_mngr.h"

#include	"two_wire_task.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void with_cable_error_or_inop_poc_decoder_fake_zero_flow( void )
{
	// 5/22/2014 rmd : This function is called within the context of the comm_mngr task. Called
	// when building the outgoing message to the main board and we see we are sending a cable
	// excessive current fault message to main. We want to make sure that we send to main a ZERO
	// flow rate for all associated POC decoders. This is very important to set the LAST flow
	// reading from the POC decoders sent to the main board to zero. Its the last cause of the
	// cable fault. I beleive the safest approach is to do it here originating with the TPMicro.
	// Thereby overwriting any potential 'last real' reading we might have pending.
	
	// 1/28/2016 rmd : UPDATED to also zero the flow rate in the case a decoder goes
	// inoperative. This cleans up a bug where the latest flow rate persists up in the main
	// board. I don't believe it tallys any flow but the display shows flow and the report
	// SECONDS of flow accumulators count up (because there is a system flow rate). This
	// function will zero all out!
	
	// ----------
	
	UNS_32	ddd;
	
	// ----------
	
	// 8/4/2015 rmd : We've already checked that we were not doing a discovery prior to this
	// function call. So the list MUTEX should be available. Remember during the ENTIRE
	// discovery process the list MUTEX is held.
	if( xSemaphoreTake( decoder_list_MUTEX, 25 ) )
	{
		// 6/9/2014 rmd : Prevent the whole comm_mngr task from freezing if the two wire task has
		// this MUTEX.
		if( xSemaphoreTake( flow_count_MUTEX, MS_to_TICKS( 25 ) ) )
		{
			for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
			{
				// 5/22/2014 rmd : If no serial number we're at the end of the active decoder list.
				if( !decoder_list[ ddd ].sn )
				{
					break;
				}
	
				// ----------
				
				if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
				{
					if( adc_readings.two_wire_cable_excessive_current || adc_readings.two_wire_terminal_over_heated || B_IS_SET(decoder_list[ ddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE) )
					{
						B_SET( decoder_info[ ddd ].send_to_main, SEND_TO_MAIN_BOARD_DECODER_FLOW_COUNT );
	
						// 8/4/2015 rmd : We want to make it look like we got 0 pulses over a period of time in
						// order to fake up a 0 flow rate coming from a decoder. Normally we get one flow reading
						// each 10 seconds from the decoder.
						decoder_info[ ddd ].latest_flow.accumulated_pulses = 0;
	
						decoder_info[ ddd ].latest_flow.accumulated_ms = 10000;
						
						decoder_info[ ddd ].latest_flow.latest_5_second_count = 0;
	
						// 8/3/2015 rmd : TODO there should be BERMAD variables to zero here! That don't yet exist.
					}
				}
			}
			
			// ----------
			
			xSemaphoreGive( flow_count_MUTEX );

			xSemaphoreGive( decoder_list_MUTEX );
		}
		else
		{
			// 1/14/2015 rmd : Let us know when this happens.
			alert_message_to_tpmicro_pile( "cbl err: flow mutex" );
			
			xSemaphoreGive( decoder_list_MUTEX );
		}
	}
	else
	{
		alert_message_to_tpmicro_pile( "cbl err: list mutex" );
	}
}

/* ---------------------------------------------------------- */
extern void add_latest_poc_flow_counts_to_the_message_to_main( DATA_HANDLE *pdh_ptr )
{

	// 5/22/2014 rmd : This function is called within the context of the comm_mngr task. Called
	// when building the outgoing message to the main board.

	// ----------
	
	UNS_32	ddd;
	
	UNS_8	records_to_send;

	UNS_8	*ucp;
	
	FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT		fcx;
	
	// ----------

	pdh_ptr->dlen = 0;

	pdh_ptr->dptr = NULL;
	
	records_to_send = 0;
	
	// ----------
	
	// 5/22/2014 rmd : We've already checked that we were not doing a discovery prior to this
	// function call. So the list MUTEX should be available. Remember during the ENTIRE
	// discovery process the list MUTEX is held.
	if( xSemaphoreTake( decoder_list_MUTEX, 25 ) )
	{
		// 6/9/2014 rmd : Prevent the whole comm_mngr task from freezing if the two wire task has
		// this MUTEX.
		if( xSemaphoreTake( flow_count_MUTEX, MS_to_TICKS( 25 ) ) )
		{
			if( terminal_poc.send_to_main )
			{
				records_to_send += 1;		
			}
			
			for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
			{
				// 5/22/2014 rmd : If no serial number we're at the end of the active decoder list.
				if( !decoder_list[ ddd ].sn )
				{
					break;
				}
	
				// ----------
				
				if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
				{
					if( B_IS_SET( decoder_info[ ddd ].send_to_main, SEND_TO_MAIN_BOARD_DECODER_FLOW_COUNT ) )
					{
						records_to_send += 1;
					}
				}
			}
			
			if( records_to_send )
			{
				// 5/22/2014 rmd : The format is a char indicating how many records followed by the records.
				pdh_ptr->dlen = (sizeof(UNS_8) + (records_to_send * sizeof(FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT)));
	
				pdh_ptr->dptr = GetMemBlk( pdh_ptr->dlen );
					
				if( !pdh_ptr->dptr )
				{
					// 5/22/2014 rmd : We didn't get the block. Zero the length and return.
					pdh_ptr->dlen = 0;
					
					alert_message_to_tpmicro_pile( "flow count memory not avail!" );
				}
				else
				{
					ucp = pdh_ptr->dptr;
	
					// ----------
					
					*ucp = records_to_send;
					
					ucp += 1;
					
					// ----------
					
					if( terminal_poc.send_to_main )
					{
						fcx.serial_number = 0;
	
						fcx.fm_accumulated_pulses = terminal_poc.accumulated_pulses;
	
						fcx.fm_accumulated_ms = terminal_poc.accumulated_ms;
	
						fcx.fm_latest_5_second_count =	terminal_poc.pulses_5_second_count[ 0 ] + 
														terminal_poc.pulses_5_second_count[ 1 ] +
														terminal_poc.pulses_5_second_count[ 2 ] +
														terminal_poc.pulses_5_second_count[ 3 ] +
														terminal_poc.pulses_5_second_count[ 4 ];
						
						fcx.fm_delta_between_last_two_fm_pulses_4khz = terminal_poc.fm_delta_between_last_two_fm_pulses_4khz;

						fcx.fm_seconds_since_last_pulse = terminal_poc.fm_seconds_since_last_pulse;

						// ----------
						
						memcpy( ucp, &fcx, sizeof(FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT) );
						
						ucp += sizeof(FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT);
						
						// ----------
	
						// 5/22/2014 rmd : And clear the count and flags. Remember we hold the MUTEX.
						terminal_poc.accumulated_pulses = 0;
	
						terminal_poc.accumulated_ms = 0;
						
						terminal_poc.send_to_main = (false);
					}
					
					for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
					{
						// 5/22/2014 rmd : If no serial number we're at the end of the active decoder list.
						if( !decoder_list[ ddd ].sn )
						{
							break;
						}
	
						// ----------
						
						if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
						{
							if( B_IS_SET( decoder_info[ ddd ].send_to_main, SEND_TO_MAIN_BOARD_DECODER_FLOW_COUNT ) )
							{
								fcx.serial_number = decoder_list[ ddd ].sn;
	
								fcx.fm_accumulated_pulses = decoder_info[ ddd ].latest_flow.accumulated_pulses;
	
								fcx.fm_accumulated_ms = decoder_info[ ddd ].latest_flow.accumulated_ms;
	
								fcx.fm_latest_5_second_count = decoder_info[ ddd ].latest_flow.latest_5_second_count;

								// 1/16/2015 rmd : We have not yet integrated the Bermad methods into the POC decoder. So
								// for now the variables are 0.
								fcx.fm_delta_between_last_two_fm_pulses_4khz = 0;

								fcx.fm_seconds_since_last_pulse = 0;
								
								// ----------
								
								memcpy( ucp, &fcx, sizeof(FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT) );
								
								ucp += sizeof(FLOW_COUNT_XFER_FROM_THE_TPMICRO_STRUCT);
								
								// ----------
			
								// 5/22/2014 rmd : And clear the count and flags. Remember we hold the MUTEX.
								decoder_info[ ddd ].latest_flow.accumulated_pulses = 0;
	
								decoder_info[ ddd ].latest_flow.accumulated_ms = 0;
								
								B_UNSET( decoder_info[ ddd ].send_to_main, SEND_TO_MAIN_BOARD_DECODER_FLOW_COUNT );
	
							}  // of marked to send
	
						}  // of if poc decoder
						
					}  // of all the decoders
					
				}  // of we got the memory
				
			}  // of if there are records to send
			
			// ----------
			
			xSemaphoreGive( flow_count_MUTEX );

			xSemaphoreGive( decoder_list_MUTEX );
		}
		else
		{
			// 1/14/2015 rmd : Let us know when this happens.
			alert_message_to_tpmicro_pile( "flow mutex unavailable" );
			
			xSemaphoreGive( decoder_list_MUTEX );
		}
	}
	else
	{
		alert_message_to_tpmicro_pile( "flow: list mutex unavailable" );
	}
}

/* ---------------------------------------------------------- */
extern BOOL_32 there_is_a_moisture_reading_to_send( DATA_HANDLE *pdh_ptr )
{
	// 1/28/2016 rmd : Executes within the context of the comm_mngr task. Returns false if
	// cannot obtain the memory or there is not a moisture reading to send. Also MUST make sure
	// no memory is allocated if returns false. Returns true with a valid pointer and length if
	// there is a mopisture reading to send. We send only one reading at a time. If more than
	// one pending the next one will go in a second. And that is an okay behavior.
	
	// ----------
	
	BOOL_32	rv;
	
	UNS_32	ddd;
	
	UNS_8	*ucp;
	
	// ----------

	rv = (false);
	
	pdh_ptr->dlen = 0;

	pdh_ptr->dptr = NULL;
	
	// ----------
	
	// 5/22/2014 rmd : We've already checked that we were not doing a discovery prior to this
	// function call. So the list MUTEX should be available. Remember during the ENTIRE
	// discovery process the list MUTEX is held.
	if( xSemaphoreTake( decoder_list_MUTEX, 25 ) )
	{
		// 6/9/2014 rmd : Prevent the whole comm_mngr task from freezing if the two wire task has
		// this MUTEX. Use a 25ms attempt to obtain the mutex.
		if( xSemaphoreTake( moisture_reading_MUTEX, MS_to_TICKS( 25 ) ) )
		{
			for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
			{
				// 5/22/2014 rmd : If no serial number we're at the end of the active decoder list.
				if( !decoder_list[ ddd ].sn )
				{
					break;
				}
	
				// ----------
				
				if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE )
				{
					if( B_IS_SET( decoder_info[ ddd ].send_to_main, SEND_TO_MAIN_BOARD_DECODER_MOISTURE_READING ) )
					{
						// 1/28/2016 rmd : We send the decoder serial number and the response structure. FOR ONE
						// DECODER.
						pdh_ptr->dlen = sizeof(UNS_32) + sizeof(MOISTURE_SENSOR_DECODER_RESPONSE);

						pdh_ptr->dptr = GetMemBlk( pdh_ptr->dlen );
							
						if( !pdh_ptr->dptr )
						{
							// 5/22/2014 rmd : We didn't get the block. Alert and return.
							alert_message_to_tpmicro_pile( "moisture memory not avail!" );
						}
						else
						{
							ucp = pdh_ptr->dptr;
							
							memcpy( ucp, &decoder_list[ ddd ].sn, sizeof(UNS_32) );
							
							ucp += sizeof(UNS_32);
							
							memcpy( ucp, &decoder_info[ ddd ].latest_moisture, sizeof(MOISTURE_SENSOR_DECODER_RESPONSE) );
							
							B_UNSET( decoder_info[ ddd ].send_to_main, SEND_TO_MAIN_BOARD_DECODER_MOISTURE_READING );
							
							rv = (true);
						}
						
						// 1/28/2016 rmd : All done. We are only sending ONE.
						break;

					}  // of if the bit is set

				}  // of if its a moisture decoder
				
			}  // of all decoders in the list
			
			xSemaphoreGive( moisture_reading_MUTEX );
		}
		else
		{
			// 1/14/2015 rmd : Let us know when this happens.
			alert_message_to_tpmicro_pile( "moisture mutex unavailable" );
		}

		xSemaphoreGive( decoder_list_MUTEX );
	}
	else
	{
		alert_message_to_tpmicro_pile( "mois: list mutex unavailable" );
	}
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern DATA_HANDLE get_ma_readings_for_the_triacs_ON( void )
{
// 9/16/2013 rmd : Always allocates memory for the maximum message size. The caller must be
// aware of this and ALWAYS plan a mem_free.

/*
  ----------------------------------------------------------
  Current Data Packet Format
  ----------------------------------------------------------
	| # of Device Types to report						//UNS_8
	|
	| |----------------------
	| | type of Device  								//UNS_8
	| |
	| | |------------------
	| | | # of readings for device  					//UNS_8
	| | |
	| | | |------------------
	| | | | index of device (optional)					//UNS_8
	| | | |
	| | | | (reading)   								//UNS_32
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | index of device (optional)					//UNS_8
	| | | |
	| | | | (reading)   								//UNS_32
	| | | | ...
	| | | | ...
	| | | | ...
	| | | | index of device (optional)					//UNS_8
	| | | |
	| | | | (reading)   								//UNS_32
	| | | |------------------
	| | |
	| | |--------------------
    | |
	| |----------------------
	|
	-------------------------
*/

	// 5/23/2012 rmd : General Inits, we need these to move through the message block we are
	// constructing, and to keep track of how many POCs we are reporting on.

	DATA_HANDLE		rv;

	UNS_8			*next_data_location_ptr;

	UNS_8			*number_of_device_types_to_report_on_ptr;

	UNS_64			station_states_local;
	
	UNS_8			light_states_local;
	
	BOOL_32			mv_state_local;
	
	BOOL_32			pump_state_local;
	
	UNS_32			current_draw_local;

	// ----------

	// 8/14/2012 raf : Accessing our station states is not an atomic operation. Botht he
	// irrigation manager and this  Because we are accessing it in a different task (internal
	// comms task) we need to get a mutex to guard against it changing.
	xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );

	station_states_local = terminal_stations_that_are_ON;

	light_states_local = terminal_lights_that_are_ON;

	mv_state_local = terminal_mv_is_ON;

	pump_state_local = terminal_pump_is_ON;
	
	xSemaphoreGive( g_xIrrigationMutex );

	// ----------

	// 5/23/2012 raf : Get a memory block as big as our largest possible size of data. We need
	// to be sure that our calculation will never be above our max available memory partition
	// size. Otherwise we will never be able to send data!

	//# Devices byte (1) + # Device Types (4) + # Readings for device (4) + (reading (4) + index (1)) * max number of devices on (34 currently: 6 stations, 12 MVs, 12 Pumps, and 4 stations)
	const UNS_16 maxDataSize = 1 + 4 + 4 + ( ( MAX_NUMBER_OF_LIGHTS_ON_IN_A_BOX + MAX_NUMBER_OF_TERMINAL_BASED_OUTPUTS_ON_AT_A_TIME + (2 * MAX_NUMBER_OF_DECODER_BASED_OUTPUTS_ON_AT_A_TIME) ) * 5);

	rv.dptr = GetMemBlk( maxDataSize );

	// 3/13/2013 rmd : If we can't obtain the memory we won't be sending this data. Internal to
	// the memory mananger an error flag is set.
	if( rv.dptr != NULL )
	{
		// 5/23/2012 raf :  We got a block, so now we need to fill it with our current data.  The first byte in the message chunk is how many Actions we are
		// reporting on.  Since we wont know until we process them all, we need to skip over this data location for now.
		number_of_device_types_to_report_on_ptr = rv.dptr;

		(*number_of_device_types_to_report_on_ptr) = 0;

		next_data_location_ptr = (rv.dptr + 1); //skip over the # of Actions variable for now

		rv.dlen = 1;

		UNS_8 *number_of_actions_for_this_device_type_ptr;

		// ----------
		
		if( station_states_local != 0 )
		{
			*next_data_location_ptr = OUTPUT_TYPE_TERMINAL_STATION;
			next_data_location_ptr++;
			rv.dlen++;

			(*number_of_device_types_to_report_on_ptr)++;


			// 7/18/2012 raf :  We need to make space for the number which we dont know yet.
			number_of_actions_for_this_device_type_ptr = next_data_location_ptr;
			(*number_of_actions_for_this_device_type_ptr) = 0;
			next_data_location_ptr++;
			rv.dlen++;


			int stationID;

			for( stationID=0; stationID<MAX_CONVENTIONAL_STATIONS_PER_BOX; stationID++ )
			{
				// 7/18/2012 raf :  Check if the bit is set, if so the station is on and we need to report the current and increase our station count
				if(((station_states_local >> stationID) &0x01) == 0x01)
				{

					//increment the number of stations reporting
					(*number_of_actions_for_this_device_type_ptr)++;

					(*next_data_location_ptr) = stationID;
					next_data_location_ptr++;
					rv.dlen++;


					current_draw_local = output_current.stations[stationID];
					memcpy( next_data_location_ptr,&current_draw_local, sizeof(current_draw_local));
					next_data_location_ptr += sizeof(current_draw_local);
					rv.dlen += sizeof(current_draw_local);
				}
			}
		}
		
		// ----------
		
		if( light_states_local != 0 )
		{
			*next_data_location_ptr = OUTPUT_TYPE_TERMINAL_LIGHT;
			next_data_location_ptr++;
			rv.dlen++;

			(*number_of_device_types_to_report_on_ptr)++;

			// 7/18/2012 raf :  We need to make space for the number which we dont know yet.
			number_of_actions_for_this_device_type_ptr = next_data_location_ptr;
			(*number_of_actions_for_this_device_type_ptr) = 0;
			next_data_location_ptr++;
			rv.dlen++;


			int lightID;
			for(lightID=0; lightID<MAX_NUMBER_OF_TERMINAL_LIGHTS; lightID++)
			{
				// 7/18/2012 raf :  Check if the bit is set, if so the station is on and we need to report the current and increase our station count
				if(((light_states_local >> lightID) &0x01) == 0x01)
				{
					(*number_of_actions_for_this_device_type_ptr)++;

					(*next_data_location_ptr) = lightID;
					next_data_location_ptr++;
					rv.dlen++;


					// 7/19/2012 raf :  copy current data to message struct
					current_draw_local = output_current.lights[lightID];
					memcpy( next_data_location_ptr,&current_draw_local, sizeof(current_draw_local));
					next_data_location_ptr += sizeof(current_draw_local);
					rv.dlen += sizeof(current_draw_local);
				}
			}
		}
		
		// ----------
		
		if( mv_state_local )
		{
			*next_data_location_ptr = OUTPUT_TYPE_TERMINAL_MASTER_VALVE;
			next_data_location_ptr++;
			rv.dlen++;

			(*number_of_device_types_to_report_on_ptr)++;

			// 7/18/2012 raf :  We need to make space for the number which we dont know yet.
			number_of_actions_for_this_device_type_ptr = next_data_location_ptr;
			(*number_of_actions_for_this_device_type_ptr) = 0;
			next_data_location_ptr++;
			rv.dlen++;


			//increment the number of stations reporting
			(*number_of_actions_for_this_device_type_ptr)++;


			current_draw_local = output_current.terminal_mv;
			memcpy( next_data_location_ptr,&current_draw_local, sizeof(current_draw_local));
			next_data_location_ptr += sizeof(current_draw_local);
			rv.dlen += sizeof(current_draw_local);

		}
		
		// ----------
		
		if( pump_state_local )
		{
			*next_data_location_ptr = OUTPUT_TYPE_TERMINAL_PUMP;
			next_data_location_ptr++;
			rv.dlen++;

			(*number_of_device_types_to_report_on_ptr)++;

			// 7/18/2012 raf :  We need to make space for the number which we dont know yet.
			number_of_actions_for_this_device_type_ptr = next_data_location_ptr;
			(*number_of_actions_for_this_device_type_ptr) = 0;
			next_data_location_ptr++;
			rv.dlen++;

			//increment the number of stations reporting
			(*number_of_actions_for_this_device_type_ptr)++;

			current_draw_local = output_current.terminal_pump;
			memcpy( next_data_location_ptr,&current_draw_local, sizeof(current_draw_local));
			next_data_location_ptr += sizeof(current_draw_local);
			rv.dlen += sizeof(current_draw_local);
		}

	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

