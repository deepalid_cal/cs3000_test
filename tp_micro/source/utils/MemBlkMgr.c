//-----------------------------------------------------------------------------
//    $Id: MemBlkMgr.c,v 1.7 2011/12/09 00:47:16 dsquires Exp $
//    $Log: MemBlkMgr.c,v $
//    Revision 1.7  2011/12/09 00:47:16  dsquires
//    Revised return data types.
//
//    Revision 1.6  2011/11/19 21:24:34  dsquires
//    Modified block sizes and counts.
//
//    Revision 1.5  2011/11/17 22:18:26  dsquires
//    Added global flag to indicate when the memory manager was in control (initialized).
//
//    Revision 1.4  2011/11/16 20:25:24  dsquires
//    Work in progress,
//
//    Revision 1.3  2011/10/31 21:41:19  dsquires
//    Rewrote queue index into block when it was freed. Should not be needed.
//
//    Revision 1.2  2011/10/28 23:09:57  dsquires
//    First cut. May have problems.
//
//    Revision 1.1  2011/10/28 18:07:47  dsquires
//    New files to implement a more robust memory block management scheme.
//
//-----------------------------------------------------------------------------

#include "MemBlkMgr.h"


/* ---------------------------------------------------------- */

// 9/3/2013 rmd : Global variable to indicate how much memory from the heap the memory
// manager has allocated. This variable here is simply the sum of the block declarations in
// the memory manager. Which presently totals 15476 bytes. Or a little less than half of the
// total heap usage of 28394. The 28394 is tracked via a FreeRTOS variable called
// PortMallocMemTakenFromHeap. The difference between the two numbers is due to tasks,
// queues, semaphores, etc..
UNS_16	g_MemMgrMemTakenFromHeap = 0;

/* ---------------------------------------------------------- */

//    Array of Memory Block Queue Definitions
//
//    IMPORTANT:  These definitions should be ordered from smallest block
//                sizes to the largest block sizes. Otherwise the
//                GetMemBlk() function will fail.

MEM_BLK_Q_DEF_s mem_blocks[ MEM_BLK_SIZES ] =
{
	{  0, MEM_BLK_0_USABLE_SIZE, MEM_BLK_0_BLK_CNT, 0 },
	{  0, MEM_BLK_1_USABLE_SIZE, MEM_BLK_1_BLK_CNT, 0 },
	{  0, MEM_BLK_2_USABLE_SIZE, MEM_BLK_2_BLK_CNT, 0 },
	{  0, MEM_BLK_3_USABLE_SIZE, MEM_BLK_3_BLK_CNT, 0 },
};

/* ---------------------------------------------------------- */
//-----------------------------------------------------------------------------
//
//    BOOL_32 InitMemBlkQueues(void)
//
//    returns:    pdFALSE if all queues were created
//                pdTRUE if an error occurred
//
//    This function allocates memory from the heap to create blocks for each
//    of the Memory Block Queues.
//-----------------------------------------------------------------------------
extern BOOL_32 InitMemBlkQueues(void)
{
	UNS_32 QCnt;
	MEM_BLK_Q_DEF_s *pMemQDef;
	MEM_BLK_HDR_s *pHdr;
	UNS_32 Blks;
	UNS_32 TotalBlockSize;
	BOOL_32 bError = pdFALSE;

	// Initialize pointer to array of memory block queue definitions
	pMemQDef = &mem_blocks[ 0 ];

	for( QCnt = 0; (bError == pdFALSE ) && ( QCnt < MEM_BLK_SIZES ) ; QCnt++, pMemQDef++ )
	{
		//-----------------------------------------------------------------------------
		//    First, create the FreeRTOS queue for the pointers to each block.
		//-----------------------------------------------------------------------------
		pMemQDef->xFreeBlockQHandle =  xQueueCreate( pMemQDef->BlockCount, sizeof(void *) );

		// if unable to create the queue, exit with error status
		if( pMemQDef->xFreeBlockQHandle == NULL )
		{
			DEBUG_STOP();

			bError = pdTRUE;

			break;
		}

		// Record the number of blocks initially placed on the free queue
		pMemQDef->BlocksOnQueue = pMemQDef->BlockCount;

		pMemQDef->minBlocksLeft = pMemQDef->BlockCount;

		for( Blks = 0; Blks < pMemQDef->BlockCount; Blks++ )
		{
			// Calculate total block size required
			TotalBlockSize = pMemQDef->BlockSize + sizeof( MEM_BLK_HDR_s );

			// Allocate a block from the heap of the usable size plus the block header size
			pHdr = ( MEM_BLK_HDR_s * ) pvPortMalloc( TotalBlockSize );

			// Make sure we got the memory block
			if( pHdr )
			{
				// Add to sum of memory taken from the heap
				g_MemMgrMemTakenFromHeap += TotalBlockSize;

				// Record the MEM BLOCK Queue index for this block
				pHdr->MemBlkQIndex = QCnt;

				// Zero the reference count
				pHdr->ReferenceCnt = 0;

				// Post the pointer to the block on the free block queue
				if( xQueueSendToBack( pMemQDef->xFreeBlockQHandle, &pHdr, 0 ) != pdTRUE )
				{
					DEBUG_STOP();

					// Force an error exit - this should never fail!
					bError = pdTRUE;

					break;
				}
			}
			else
			{
				DEBUG_STOP();

				// Force an error exit
				bError = pdTRUE;

				break;
			}
		}
	}

	return bError;
}

//-----------------------------------------------------------------------------
//    void SetMemBlkRefCount( void *pBlock, UNS_8 ReferenceCnt )
//
//    This function sets the message block's reference count to the specified
//    value.
//-----------------------------------------------------------------------------
/*
void SetMemBlkRefCount( void *pBlock, UNS_8 ReferenceCnt )
{
	MEM_BLK_HDR_s *pHdr;

	// Zero is not a valid reference count
	configASSERT( ReferenceCnt );

	// Point to the message block header
	pHdr = ((MEM_BLK_HDR_s *) pBlock) - 1;

	pHdr->ReferenceCnt = ReferenceCnt;
}
*/

//-----------------------------------------------------------------------------
//    void SetMemBlkRefCount( void *pBlock, UNS_8 ReferenceCnt )
//
//    This function sets the message block's reference count to the specified
//    value.
//-----------------------------------------------------------------------------
extern void IncrementMemBlkRefCount( void *pBlock )
{
	MEM_BLK_HDR_s *pHdr;

	// Point to the message block header
	pHdr = ((MEM_BLK_HDR_s*)pBlock) - 1;

	pHdr->ReferenceCnt++;
}

//-----------------------------------------------------------------------------
//    void *GetMemBlk( UNS_16 usable_size )
//
//    This function returns a pointer to a memory block of AT LEAST the
//    requested size.
//
//    If no suitable memory block is available, a NULL pointer is returned.
//-----------------------------------------------------------------------------
extern void *get_mem_blk_debug( UNS_32 usable_size, char *fil, UNS_32 lin )
//void *GetMemBlk( UNS_32 usable_size, UNS_32 xTicksToWait )
{
	UNS_16 QCnt;

	MEM_BLK_Q_DEF_s *pMemQDef;

	MEM_BLK_HDR_s *pHdr;

	long status = pdFALSE;

	// Zero length not allowed
	configASSERT( usable_size );

	pHdr = NULL;

	// Initialize pointer to array of memory block queue definitions
	pMemQDef = &mem_blocks[ 0 ];

	// Find a memory block definition to accommodate the requested size
	for( QCnt = 0; QCnt < MEM_BLK_SIZES ; QCnt++, pMemQDef++ )
	{
		if( usable_size <= pMemQDef->BlockSize )
		{
			// 3/13/2013 rmd : If this partition has an available block we'll try to get one. If not
			// we'll flag and check the next largest partition.
			if( pMemQDef->BlocksOnQueue > 0 )
			{
				status = pdTRUE;

				break;
			}
			else
			{
				// 3/8/2013 rmd : Cannot send an alert message to main board cause that takes getting a
				// block from the pool. And we are already in trouble. Just set the bit in the error bit
				// field (which may also not be able to be sent to the main board).
				flag_error( BIT_TPME_memory_partition_empty );
			}
		}
	}

	// Continue if we found a suitable block definition
	if( pdTRUE == status )
	{
		// Try to receive a block pointer from the associated queue. No wait time!
		status = xQueueReceive( pMemQDef->xFreeBlockQHandle, &pHdr, 0 );

		if( pdTRUE == status )
		{
			// We got a pointer to a suitable block, so lets initialize the ref count
			pHdr->ReferenceCnt = 1;

			// Write queue index back too
			pHdr->MemBlkQIndex = QCnt;

			// Advance the pointer to point past the block header structure
			pHdr++;

			// Update # of blocks on free queue
			portENTER_CRITICAL();

			pMemQDef->BlocksOnQueue--;

			portEXIT_CRITICAL();

			// 3/13/2013 rmd : Track the least number of blocks free in this partition.
			if( pMemQDef->BlocksOnQueue < pMemQDef->minBlocksLeft )
			{
				pMemQDef->minBlocksLeft = pMemQDef->BlocksOnQueue;
			}

		}
		else
		{
			// 3/13/2013 rmd : This should NEVER happen. We've already determined there is a free block
			// in the pool that meets the size requirement.
			DEBUG_STOP();
		}
		
	}

	if( pHdr == NULL )
	{
		flag_error( BIT_TPME_memory_pool_empty );

		DEBUG_STOP();
	}

	// Return a pointer to the application's view of the block start
	return( (void*)pHdr );
}
//-----------------------------------------------------------------------------
//    void *GetMemBlkFromISR( UNS_16 usable_size,  portTickType xTicksToWait )
//
//    This function returns a pointer to a memory block of AT LEAST the
//    requested size, waiting the specified number of ticks.
//
//    If no suitable memory block is available, a NULL pointer is returned.
//-----------------------------------------------------------------------------
/*
void *GetMemBlkFromISR( UNS_32 usable_size, signed portBASE_TYPE *pxHigherPriorityTaskWoken )
{
	UNS_16 QCnt;

	MEM_BLK_Q_DEF_s *pMemQDef;

	MEM_BLK_HDR_s *pHdr;

	long status = pdFALSE;

	// Zero length not allowed
	configASSERT(usable_size);

	pHdr = (MEM_BLK_HDR_s *) NULL;

	// Initialize pointer to array of memory block queue definitions
	pMemQDef = &g_MemBlkQDef_s[ 0 ];

	// Find a memory block definition to accommodate the requested size
	for ( QCnt = 0; QCnt < MEM_BLK_SIZES ; QCnt++, pMemQDef++ )
	{
		if( usable_size <= pMemQDef->BlockSize )
		{
			//4/24/2012 raf :  added this to enable moving up in size if we run out of a certain size block.
			if(pMemQDef->BlocksOnQueue > 0)
			{
				status = pdTRUE;
				break;
			}
			else
			{
				//If we are at the last position, we dont want to send a generic alert, because that checks out a block.
				if(QCnt == MEM_BLK_SIZES - 1)
				{

				}
				else
				{
					flag_error_from_isr( BIT_TPME_had_to_pull_from_the_next_size_heap_block, pxHigherPriorityTaskWoken );

					send_generic_alert_to_queue_for_packaging_from_isr( TPALERT_had_to_pull_from_the_next_size_heap_block, pxHigherPriorityTaskWoken );
				}

			}
		}
	}

	// Continue if we found a suitable block definition
	if( pdTRUE == status )
	{
		// Try to receive a block pointer from the associated queue
		status = xQueueReceiveFromISR( pMemQDef->xFreeBlockQHandle, &pHdr, pxHigherPriorityTaskWoken );

		if( pdTRUE == status )
		{
			// We got a pointer to a suitable block, so lets initialize the ref count
			pHdr->ReferenceCnt = 1;

			// Write queue index back too
			pHdr->MemBlkQIndex = QCnt;

			// Advance the pointer to point past the block header structure
			pHdr++;

			// Update # of blocks on free queue
			portENTER_CRITICAL();

			pMemQDef->BlocksOnQueue--;

			portEXIT_CRITICAL();

			if(pMemQDef->BlocksOnQueue < pMemQDef->minBlocksLeft)
			{
				pMemQDef->minBlocksLeft = pMemQDef->BlocksOnQueue;
			}
		}
		else
		{
			// This is probably superfluous
			pHdr = NULL;
		}
	}

	if( pHdr == NULL )
	{
		flag_error_from_isr( BIT_TPME_we_couldnt_get_a_suitable_heap_block, pxHigherPriorityTaskWoken );

		send_generic_alert_to_queue_for_packaging_from_isr( TPALERT_we_couldnt_get_a_suitable_heap_block, pxHigherPriorityTaskWoken );
	}

	// Return a pointer to the application's view of the block start
	return ( (void  *) pHdr );
}
*/

//-----------------------------------------------------------------------------
//    void FreeMemBlk( void *pBlock )
//
//    This function returns the specified block to its free block
//    queue if its reference count is one.
//
//    Otherwise the block is still being used and will be freed when
//    the last task using the block frees it.
//-----------------------------------------------------------------------------
extern BOOL_32 FreeMemBlk( void *pBlock )
{
	BOOL_32			rv;

	MEM_BLK_HDR_s	*pMemBlkHdr;

	UNS_8			index;
	
	// ----------

	// Catch NULL pointers
	configASSERT( pBlock );
	
	// ----------

	rv = (false);
	
	// ----------

	// Point to the memory block header
	pMemBlkHdr = ((MEM_BLK_HDR_s*)pBlock) - 1;

	// Get the index for this block
	index = pMemBlkHdr->MemBlkQIndex;

	// Do sanity check for valid block
	if( index >= MEM_BLK_SIZES )
	{
		// This is not a valid index!
		DEBUG_STOP();
	}
	
	// ----------

	// Decrement the block's reference count
	portENTER_CRITICAL();

	pMemBlkHdr->ReferenceCnt--;

	// Free the block only if the reference count is now zero
	if( pMemBlkHdr->ReferenceCnt == 0 )
	{
		// Send the address of the block to the free block queue
		if( pdTRUE != xQueueSendToBack( mem_blocks[ index ].xFreeBlockQHandle, &pMemBlkHdr, 0 ) )
		{
			DEBUG_STOP();
		}
		else
		{
			// Update # of blocks on free queue
			mem_blocks[ index ].BlocksOnQueue++;
			
			rv = (true);
		}
	}

	portEXIT_CRITICAL();
	
	// ----------

	return( rv );
}


//-----------------------------------------------------------------------------
//    void FreeMemBlkFromISR( void *pBlock )
//
//    This function returns the specified block to its free block
//    queue if its reference count is one.
//
//    Otherwise the block is still being used and will be freed when
//    the last task using the block frees it.
//-----------------------------------------------------------------------------
/*
void FreeMemBlkFromISR( void *pBlock, signed portBASE_TYPE *pxHigherPriorityTaskWoken)
{
   MEM_BLK_HDR_s *pMemBlkHdr;
   UNS_8 index;
   UNS_8 refcnt;

   // Catch NULL pointers
   configASSERT( pBlock );

   // Point to the memory block header
   pMemBlkHdr = ((MEM_BLK_HDR_s *) pBlock) - 1;

   // Get the index for this block
   index = pMemBlkHdr->MemBlkQIndex;

   // Do sanity check for valid block
   if( index >= MEM_BLK_SIZES )
   {
	  // This is not a valid index!
	  DEBUG_STOP( DS_MEM_BLK_MGR_C );
   }

   // Decrement the block's reference count
   portENTER_CRITICAL();

   pMemBlkHdr->ReferenceCnt--;

   refcnt = pMemBlkHdr->ReferenceCnt;

   // Free the block only if the reference count is now zero
   if( 0 == refcnt )
   {
	  // Send the address of the block to the free block queue
	  if( pdTRUE != xQueueSendToBackFromISR( g_MemBlkQDef_s[ index ].xFreeBlockQHandle, &pMemBlkHdr, pxHigherPriorityTaskWoken ) )
	  {
		 DEBUG_STOP( DS_MEM_BLK_MGR_D );
	  }
	  else
	  {
		 // Update # of blocks on free queue
		 g_MemBlkQDef_s[ index ].BlocksOnQueue++;
	  }
   }

   portEXIT_CRITICAL();
}
*/


