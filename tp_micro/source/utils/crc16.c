/////////////////////////////////////////////////////////
//
//    File: crc16.c 
//
//    Description: CRC16 calculation for Two Wire messages
//
//    $Id: crc16.c,v 1.1 2011/11/29 20:55:29 dsquires Exp $
//    $Log:
/////////////////////////////////////////////////////////
#include "LPC17xx.h"

#include "tpmicro_common.h"
#include "crc16.h"

//////////////////////////////////////////
//  Polynomial is: x^16 + x^15 + x^2 + 1
//////////////////////////////////////////
#define CRC16_POLY_MASK         0xa001

UNS_16 _update_crc16(UNS_16 _crc, UNS_8 _data)
{
   int i;

   _crc ^= _data;
   for( i = 0; i < 8; i++ )
   {
      _crc = (_crc & 1) ? ((_crc >> 1) ^ CRC16_POLY_MASK) : (_crc >> 1);
   }
   return _crc;
}



