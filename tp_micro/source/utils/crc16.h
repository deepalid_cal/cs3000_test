/////////////////////////////////////////////////////////
//
//    File: crc16.h
//
//    Description: header file for CRC16 calculation 
//
//    $Id: crc16.h,v 1.1 2011/11/29 20:55:29 dsquires Exp $
//    $Log:
/////////////////////////////////////////////////////////
#ifndef CRC16_H_
#define CRC16_H_

///////////////////////////////////////////////////////////////
//  The crc16 accumulator must be initialized with this value
///////////////////////////////////////////////////////////////
#define CRC16_INIT_VAL          0xffff

// Function Prototype
extern UNS_16 _update_crc16(UNS_16 _crc, UNS_8 data);

#endif /* CRC16_H_ */
