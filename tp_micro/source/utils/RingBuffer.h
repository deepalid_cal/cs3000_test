/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_RING_BUFFER_H_
#define INC_RING_BUFFER_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"general_picked_support.h"

#include	"crc.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 3/22/2013 rmd : This #define sets the size of the two uart ring buffers in use. Notice
// how the maximum block size is tied to the memory pool. We've done that to reveal the
// relationship that does exist between the two. We add 256 bytes to allow the packet hunter
// time, in the face of back to back packets, to find and account for one packet while the
// next one has started to arrive (256 bytes is about 25ms at 115200 baud).
#define MX_RINGBUFFER_SIZE		(TPMICRO_MAX_AMBLE_TO_AMBLE_PACKET_SIZE + 256)

// ----------

typedef struct
{
	// ring_index is where we start examining for 500 series packets from
	unsigned short	packet_index;

	// 500 series packet hunting support
	unsigned short	ringhead1;
	unsigned short	ringhead2;
	unsigned short	ringhead3;
	unsigned short	ringhead4;

	unsigned short	datastart;

	unsigned short	packetlength;

	// AS WE ADD TO THIS STRUCTURE THE VARS NEED TO BE ADDED TO THE PACKET HUNT MODE ENABLE FUNCTION

} PACKET_HUNT_S;


typedef struct
{
	DATA_HANDLE		DataHandle_s;       	// Chunck of heap where our ringbuffer data is stored

	UNS_8			*last_read_location;     	//the last memory location read

	UNS_8			*last_write_location;		//the last memory location written to

	UNS_32			missed_data_count;         	//this will hold a count of how many bytes have been lost due to insufficient read rate

	UNS_32			data_too_large_count;		//this will hold a count of how many bytes have been too large (>0xFF)

} RINGBUFFER_CONTROL_STRUCT;


// UART_RINGBUFFER_s - UART Ring Buffer data structure
//                      One instance for each UART port
typedef struct
{
	unsigned char			ringbuffer[ MX_RINGBUFFER_SIZE ];	// the actual ring buffer

	// ring_tail is where the next incoming byte from the UART is placed into the ring buffer
	volatile unsigned short	ringtail;

	// Flag set to enable packet hunting
	BOOL_32					hunt_for_packets;

    // variables used during the transport PACKET hunt
	PACKET_HUNT_S			ph;

	volatile BOOL_32		ph_tail_caught_index;		// set TRUE when this happens - (set within an interrupt)

} UART_RINGBUFFER_s;

//Return values from initRungBufferChar

#define INIT_SUCCESS	 			0
#define INIT_INCORRECT_BUFFER		1


// Increment a ring buffer index. The parmater 'index' can be either (variable_name) OR
// (*variable_name). Either approach works.
#define INC_RING_BUFFER_INDEX( index, length )		\
{													\
	(index) += 1;				   					\
													\
	if ( (index) >= (length) )     					\
	{   											\
		(index) = 0;			   					\
	}   											\
}

// Decrement a ring buffer index. The parmater 'index' can be either (variable_name) OR
// (*variable_name). Either approach works.
#define DEC_RING_BUFFER_INDEX( index, length )		\
{													\
	if( (index) == 0 )								\
	{												\
		(index) = (length);		   					\
	}												\
													\
	(index) -= 1;				   					\
}


// Increment a ring buffer pointer. The parmater 'pointer' can be either (variable_name) OR
// (*variable_name). Either approach works.    //not finished yet, dont use!
#define INC_RING_BUFFER_POINTER( buffer_start_location, current_index_location, buffer_length )		\
{													\
	(index) += 1;				   					\
													\
	if ( (index) >= (length) )     					\
	{   											\
		(index) = 0;			   					\
	}   											\
}

// Function declarations
extern void RingBufferPut_ISR( volatile UART_RINGBUFFER_s *pRingBuffer_s, UNS_8 data );
extern void initRingBuffer( volatile UART_RINGBUFFER_s *pRingBuffer_s );
extern UNS_32 initRingBufferChar( RINGBUFFER_CONTROL_STRUCT *pRingBufferChar_s, UNS_32 pring_buffer_size );
extern BOOL_32 bIsRingBufferCharDataAvail( RINGBUFFER_CONTROL_STRUCT *pRingBufferChar_s);
extern void RingBufferCharPut(  RINGBUFFER_CONTROL_STRUCT *pRingBufferChar_s, UNS_32 data );
extern UNS_8 RingBufferCharGet( RINGBUFFER_CONTROL_STRUCT *pRingBufferChar_s );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*RINGBUFFER_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


