/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//-----------------------------------------------------------------------------
//  UART Ring buffer functions (public)
//-----------------------------------------------------------------------------
void initRingBuffer( volatile UART_RINGBUFFER_s *pRingBuffer_s )
{
    //  Clear tail and head pointers of ring buffer
    pRingBuffer_s->ph.packet_index = 0;
    pRingBuffer_s->ph.ringhead1 = 0;
    pRingBuffer_s->ph.ringhead2 = 0;
    pRingBuffer_s->ph.ringhead3 = 0;
    pRingBuffer_s->ph.ringhead4 = 0;
}

//-----------------------------------------------------------------------------
//  void RingBufferPut( UART_RINGBUFFER_s *pRingBuffer_s, UNS_8 data )
//
//  Store a byte in a ring buffer
//-----------------------------------------------------------------------------
void RingBufferPut_ISR( volatile UART_RINGBUFFER_s *pRingBuffer_s, UNS_8 data )
{
	// 12/2/2013 rmd : Note - called from the UART ISR.
	
	// ----------
	
	//  Store the data byte
	pRingBuffer_s->ringbuffer[ pRingBuffer_s->ringtail ] = data;
	
	// Update the tail pointer (index)
	pRingBuffer_s->ringtail = ( (pRingBuffer_s->ringtail + 1) % MX_RINGBUFFER_SIZE );
	
	// ----------

	//  Check for overflow, set flag if it occurs
	if( pRingBuffer_s->ringtail == pRingBuffer_s->ph.packet_index )
	{
	  pRingBuffer_s->ph_tail_caught_index = TRUE;
	}
	
	// ----------
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION    inits the ring buffer, to call this, space already needs to have been
    made for the ringbuffer, this is just setting values to 0 and setting the pointers to
    the beginning of the structure.


	@PARAMETERS

    	*pRingBufferChar_s: a data handle containg a pointer to where we store our flow buffer results.  This can be dynamically allocated or declared
    	 in ram (static allocation?).  The data handle also contains a length that must be equal to or greater than the pring_buffer_size.

		pring_buffer_size: the size of our ring buffer

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITIES(none)

	@EXECUTED  At program startup

    @RETURN  A status code indicating whether the buffer was init successfully or not.

	@ORIGINAL 8/20/2012 raf

	@REVISIONS
*/

extern UNS_32 initRingBufferChar( RINGBUFFER_CONTROL_STRUCT *pRingBufferChar_s, UNS_32 pring_buffer_size )
{
	UNS_32 result = INIT_SUCCESS;
	if( (pRingBufferChar_s->DataHandle_s.dlen < pring_buffer_size) || (pRingBufferChar_s->DataHandle_s.dptr == NULL) )
	{
		result = INIT_INCORRECT_BUFFER;
	}
	else
	{
		pRingBufferChar_s->data_too_large_count = 0;
		pRingBufferChar_s->missed_data_count = 0;
		pRingBufferChar_s->last_read_location = pRingBufferChar_s->DataHandle_s.dptr;
		pRingBufferChar_s->last_write_location = pRingBufferChar_s->DataHandle_s.dptr;

	}
	return result;
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION    Puts the specified charachter into the specified ringbuffer,  flags if
    the data is too large (ex 0x100 into an 8 bitter)  and caps it to the appropriate size.
    It advances the last read pointer after making its read.  If the last write pointer then
    bumps into the last read, we make a dummy call to RingBufferCharGet to advance the read
    pointer and drop the data.  Upon doing this, we increment the missed data count.

	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITIES

	@EXECUTED

	@RETURN (none)

	@ORIGINAL 4/4/2012 raf

	@REVISIONS
*/


void RingBufferCharPut( RINGBUFFER_CONTROL_STRUCT *pRingBufferChar_s, UNS_32 data )
{
	//the ring buffer will accept data up to 32 bits, but this function is used when we want to
	//cap it at 8 bits, so we need to test for that and increase the data_too_large_count
	//variable.
	if( data > 0xFF )
	{
		// 4/13/2012 rmd : Bobby and I talked about what to do with excessive readings. We've
		// decided to cap them and to NOT throw them away. We'll include them into the flow. And set
		// the error flag that gets sent to the main board.
		data = 0xFF;

		if( pRingBufferChar_s->data_too_large_count < 0xFFFFFFFF )
		{
			pRingBufferChar_s->data_too_large_count++;
		}
	}

	//if the last place we wrote was the last place in the buffer memory map, we need to loop
	//backaround to the front before writing this new value //tried to change the last part of this to have a const length, but since this ring buffer is
	//generic as possible, its not possible to know how big it could be.
	if( pRingBufferChar_s->last_write_location == pRingBufferChar_s->DataHandle_s.dptr + (pRingBufferChar_s->DataHandle_s.dlen-1))
	{
		*(pRingBufferChar_s->DataHandle_s.dptr) = data;

		pRingBufferChar_s->last_write_location = pRingBufferChar_s->DataHandle_s.dptr;
	}
	else
	{
		*(pRingBufferChar_s->last_write_location+1) = data;

		pRingBufferChar_s->last_write_location++;
	}

	//lastly, we need to make sure we arent going to be overflowing our buffer by making this
	//call, if we were to overflow it, we just advance the last read location by calling the
	//data and not doing anything with it.
	if(pRingBufferChar_s->last_write_location == pRingBufferChar_s->last_read_location)
	{
		RingBufferCharGet(pRingBufferChar_s);    //black hole, used to advance the ring buffer read mark only

		pRingBufferChar_s->missed_data_count++;

		if( pRingBufferChar_s->missed_data_count == 0 )    //i think this is a clever way to prevent overflow....
		{
			 pRingBufferChar_s->missed_data_count--;
		}
	}

}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Checks if there is data available. it does this by comparing the LW and LR
    pointers and making sure they are different. The only time they are the same is when
    we've read all the way up to the LW.

	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITIES

	@EXECUTED

	@RETURN (none)

	@ORIGINAL 4/4/2012 raf

	@REVISIONS
*/

BOOL_32 bIsRingBufferCharDataAvail( RINGBUFFER_CONTROL_STRUCT *pRingBufferChar_s )
{
	return (pRingBufferChar_s->last_read_location == pRingBufferChar_s->last_write_location) ? FALSE : TRUE;
	/*BOOL_32 dataAvailable = TRUE;
	if( pRingBufferChar_s->last_read_location == pRingBufferChar_s->last_write_location )
	{
		 dataAvailable = FALSE;
	}
	return dataAvailable;      */
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION   Gets the latest character from the ringbuffer. Before calling this, we have to check to be sure data is available!!

	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITIES

	@EXECUTED

	@RETURN (none)

	@ORIGINAL 4/4/2012 raf

	@REVISIONS
*/

UNS_8 RingBufferCharGet( RINGBUFFER_CONTROL_STRUCT *pRingBufferChar_s )
{
	UNS_8 rv = 0;
	//if( bIsRingBufferCharDataAvail( pRingBufferChar_s ) == FALSE )
	{
		//dont know what to do if this happens, this should be checked before this call is made, or
		//else maybe we need to return a struct with an error field
	}
	//3/29/2012 raf :   check to see if the last location we read was the last spot on the ring buffer, if it
	//was loop back around to the beginning of the ring buffer, otherwise, continue on like
	//normal
	if( pRingBufferChar_s->last_read_location == pRingBufferChar_s->DataHandle_s.dptr + ( pRingBufferChar_s->DataHandle_s.dlen-1 ) )
	{
		rv = *pRingBufferChar_s->DataHandle_s.dptr;
		pRingBufferChar_s->last_read_location = pRingBufferChar_s->DataHandle_s.dptr;
	}
	else
	{
		rv = *( pRingBufferChar_s->last_read_location+1 );
		pRingBufferChar_s->last_read_location++;
	}

	return rv;
}

