/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_EEPROMMAP_H
#define _INC_EEPROMMAP_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"

#include	"rxcommtask.h"

#include	"weather_manager.h"

#include	"tpmicro_comm_mngr.h"

#include	"two_wire_task.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/6/2013 rmd : Well since we are mirroring everything the stored data had better be less
// than half of this.
#define EE_CHIP_TOTAL_MEMORY_SIZE	(8192)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define EE_INIT_KEY									(0x778899AB)

// 2/7/2013 rmd : When an item is added or removed to the EE should roll the version number.
// Just add 1. So that we get a variable initialization and a clean mirror. The mirror
// should fail and cause this but I have as of yet to test that. I didn't write that mirror
// stuff. And something else - the total stored size test should fail also when an item is
// added or removed.
#define EE_VERSION									(1006)

#define TP_MICRO_check_eeprom_mirror_ms				(60000)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define EE_SIZE_TP_MICRO_EE_STATUS					( sizeof( tp_micro_eeprom_status ) )

#define EE_SIZE_WHATS_INSTALLED						( sizeof( whats_installed ) )

#define EE_SIZE_OUTPUT_CURRENT						( sizeof( output_current ) )

#define EE_SIZE_WEATHER_INFO						( sizeof( weather_info ) )

#define EE_SIZE_DECODER_LIST						( sizeof( decoder_list ) )



#define EE_LOCATION_TP_MICRO_EE_STATUS				( 0 )

#define EE_LOCATION_WHATS_INSTALLED					( EE_LOCATION_TP_MICRO_EE_STATUS + EE_SIZE_TP_MICRO_EE_STATUS )

#define EE_LOCATION_OUTPUT_CURRENT					( EE_LOCATION_WHATS_INSTALLED + EE_SIZE_WHATS_INSTALLED )

#define EE_LOCATION_WEATHER_INFO					( EE_LOCATION_OUTPUT_CURRENT + EE_SIZE_OUTPUT_CURRENT )

#define EE_LOCATION_DECODER_LIST					( EE_LOCATION_WEATHER_INFO + EE_SIZE_WEATHER_INFO )

// 2/7/2013 rmd : Because we start at 0 the addresses and total stored size are the same.
#define EE_TOTAL_STORED_SIZE						( EE_LOCATION_DECODER_LIST + EE_SIZE_DECODER_LIST )


// 2/6/2013 rmd : Don't know why. I'm just keeping a gap before the mirror starts.
#define EE_DATA_LOCATION_MIRROR						( EE_TOTAL_STORED_SIZE + 16 )


#define EE_ACTION_READ									( 0 )
#define EE_ACTION_WRITE									( 1 )
#define EE_ACTION_CHECK_MIRROR							( 2 )


/* ---------------------------------------------------------- */
/**
	@DESCRIPTION this struct is passed in to the eeprom queue to write data to the eeprom.

	@FIELDS

		@ee_address:	The address in eeprom to write to, choose from defines in eeprom_map.h

		@action:		This will always be write, maybe impelement a read feature later?   TODO:

    	@dh:			Contains a pointer and datalength of what we are writing to eeprom.  The datalength are already defined in eeprom_map.h for
    				our normal writes.

	@ORIGINAL 9/18/2012 raf

	@REVISIONS
*/
typedef struct
{
	UNS_32			cmd;

	// 2/7/2013 rmd : This is the to/from location within the ee.
	UNS_32			EE_address;

	// 2/7/2013 rmd : This is the to/from RAM address for the variable.
	UNS_8			*RAM_address;

	// 2/7/2013 rmd : This is the to/from location within the ee.
	UNS_32			length;

} EEPROM_MESSAGE_STRUCT;

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION this struct is saved in eeprom and contains the status of our eeprom.  @FIELDS

		eeprom_init_key:	On bootup, we look to see if the init key is set right to see if we need to zero our eeprom and load it with default
							values.  The value it needs to be is defined as EE_IS_INIT_KEY.

    	init_status:		These dont have much of a use yet, but they can be used reinitialize the structs on bootup?  Maybe we will unset them
    						before doing writes, and then set them after.  That way if there is a powerfail during a write, we will reinit on next
    						bootup.  The eeprom chip already protects against this with the write enable pin, but extra security is always nice too.

	@ORIGINAL 9/18/2012 raf

	@REVISIONS
*/
typedef struct
{
	UNS_32	eeprom_init_key;

	UNS_32	version;
	
	UNS_32	total_stored_size;
	
	union
	{
		UNS_32 raw_byte;

		struct
		{
			unsigned int eeprom_init				: 1;
			unsigned int hardware_table_init		: 1;
			unsigned int current_table_init 		: 1;
			unsigned int routing_table_init			: 1;
			unsigned int settings_from_main_init	: 1;
		};

	};

} TP_MICRO_EEPROM_STATUS_s;

extern TP_MICRO_EEPROM_STATUS_s tp_micro_eeprom_status;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void mirror_eeprom( UNS_16 pstart_address, UNS_16 psize_to_mirror );

extern BOOL_32 check_eeprom_mirror();

extern void init_eeprom_to_default_values_and_mirror();


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

