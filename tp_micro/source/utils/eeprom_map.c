/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"eeprom_map.h"

#include	"25lc640a.h"

#include	"tpmicro_comm_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

TP_MICRO_EEPROM_STATUS_s	tp_micro_eeprom_status;

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Takes a copy of eeprom and stores it in a separate eeprom location as a
    mirror.  This function takes 168ms to run to completion with 375 bytes of eeprom being
    used.

	@PARAMETERS

		pstart_address:	The location of the first byte to mirror

		psize_to_mirror:	The size of the data we want to mirror

	@CALLER_MUTEX_REQUIREMENTS ALWAYS take the eeprom mutex before calling this function

	@MEMORY_RESPONSIBILITIES(none)

	@EXECUTED At program startup, and whenever else we want to verify the eeprom is still operating

	@RETURN (none)

	@ORIGINAL 9/18/2012 raf

	@REVISIONS
*/
extern void mirror_eeprom( UNS_16 pstart_address, UNS_16 psize_to_mirror )
{
	UNS_16 write_address;

	UNS_16 read_address;

	UNS_32 bytes_left_to_mirror;

	UNS_8 data_buffer[32];

	bytes_left_to_mirror = psize_to_mirror;

	read_address = pstart_address;

	write_address = EE_DATA_LOCATION_MIRROR + pstart_address;

	while(bytes_left_to_mirror > 0)
	{
		UNS_16 bytes_to_write;
		if(bytes_left_to_mirror > SEEP_PAGE_SIZE_IN_BYTES)
		{
			bytes_to_write = SEEP_PAGE_SIZE_IN_BYTES;
		}
		else
		{
			bytes_to_write = bytes_left_to_mirror;
		}

		seep_rd(read_address, (void *) data_buffer, bytes_to_write);

		seep_wr(write_address, (void *) data_buffer, bytes_to_write);

		read_address += bytes_to_write;
		write_address += bytes_to_write;
		bytes_left_to_mirror -= bytes_to_write;
	}
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION verifys the eeprom mirror is still the same as the eeprom data

	@PARAMETERS (none)

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITIES(none)

	@EXECUTED

	@RETURN True if everything is ok, false if theres a mismatch

	@ORIGINAL 9/18/2012 raf

	@REVISIONS
*/
extern BOOL_32 check_eeprom_mirror()
{
	BOOL_32 rv;

	UNS_16 read_address_critical;

	UNS_16 read_address_mirror;

	UNS_32 bytes_left_to_check;

	UNS_8 data_buffer_critical[32];

	UNS_8 data_buffer_mirror[32];

	// 9/18/2012 raf :  this will get set to false if our mirror doesnt match our critical section
	rv = true;

	bytes_left_to_check = EE_TOTAL_STORED_SIZE;

	read_address_critical = 0;

	read_address_mirror = EE_DATA_LOCATION_MIRROR;

	// 9/18/2012 raf :  loop over the entire critical and associated mirror section and read one page at a time
	while(bytes_left_to_check > 0)
	{
		UNS_16 bytes_to_check;

		if( bytes_left_to_check > SEEP_PAGE_SIZE_IN_BYTES )
		{
			bytes_to_check = SEEP_PAGE_SIZE_IN_BYTES;
		}
		else
		{
			bytes_to_check = bytes_left_to_check;
		}

		seep_rd( read_address_critical, (void *) data_buffer_critical, bytes_to_check );

		seep_rd( read_address_mirror, (void *) data_buffer_mirror, bytes_to_check );

		if( memcmp( data_buffer_critical, data_buffer_mirror, bytes_to_check ) != 0)
		{
			rv = false;

			break;
		}


		read_address_critical += bytes_to_check;

		read_address_mirror += bytes_to_check;

		bytes_left_to_check -= bytes_to_check;
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
	@DESCRIPTION Sets the eeprom to 0's and then loads default values into some paramaters

	@PARAMETERS (none)

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITIES(none)

	@EXECUTED

	@RETURN (none)

	@ORIGINAL 9/20/2012 raf

	@REVISIONS
*/
extern void init_eeprom_to_default_values_and_mirror()
{
	/*
	// 7/24/2012 raf :  wipe the eeprom memory a page at a time
	UNS_8 dummy_ee_data[32];
	for(int i=0; i<32; i++)
	{
		dummy_ee_data[i] = 0;
	}
	for(int i=0; i<EE_DATA_TOTAL_SIZE/32; i++)
	{
		seep_wr(32 * i,&dummy_ee_data[0], 32);
	}
	*/
	
	// ----------

	memset( &tp_micro_eeprom_status, 0x00, sizeof( tp_micro_eeprom_status ) );
	
	tp_micro_eeprom_status.eeprom_init = (true);

	tp_micro_eeprom_status.eeprom_init_key = EE_INIT_KEY;

	tp_micro_eeprom_status.version = EE_VERSION;

	tp_micro_eeprom_status.total_stored_size = EE_TOTAL_STORED_SIZE;
	
	seep_wr( EE_LOCATION_TP_MICRO_EE_STATUS, (UNS_8*)&tp_micro_eeprom_status, EE_SIZE_TP_MICRO_EE_STATUS);
	
	// ----------

	memset( &whats_installed, 0x00, sizeof( whats_installed ) );
	
	seep_wr( EE_LOCATION_WHATS_INSTALLED, (UNS_8*)&whats_installed, EE_SIZE_WHATS_INSTALLED );

	// ----------

	memset( &output_current, 0x00, sizeof( output_current ) );
	
	seep_wr( EE_LOCATION_OUTPUT_CURRENT, (UNS_8*)&output_current, EE_SIZE_OUTPUT_CURRENT );

	// ----------

	// 3/25/2013 rmd : Clear the weather info structure. And request the main board to send a
	// copy of the wind speed pause and resume settings to us.
	memset( &weather_info, 0x00, sizeof( weather_info ) );
	
	weather_info.need_wind_settings_from_main_board = (true);

	seep_wr( EE_LOCATION_WEATHER_INFO, (UNS_8*)&weather_info, EE_SIZE_WEATHER_INFO );

	// ----------

	// 4/15/2013 rmd : Zero out the decoder info structure. Which means there are NO decoders.
	memset( &decoder_list, 0x00, sizeof(decoder_list) );
	
	seep_wr( EE_LOCATION_DECODER_LIST, (UNS_8*)&decoder_list, EE_SIZE_DECODER_LIST );

	// ----------

	// 2/6/2013 rmd : Mirror it.
	mirror_eeprom( 0, EE_TOTAL_STORED_SIZE );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

