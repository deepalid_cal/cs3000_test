/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"IAP.h"

#include	"LPC17xx.h"

#include	"lpc17xx_clkpwr.h"

#include	"FreeRTOS.h"

#include	"task.h"

#include	"intrinsics.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void __iap_invoke_isp()
{
	volatile UNS_32	IAP_commands[5], IAP_results[5];

	// ----------

	// 2/11/2014 rmd : Clear the content in the RTC that got us here.
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG0 = 0x00;
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG1 = 0x00;
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG2 = 0x00;
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG3 = 0x00;
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG4 = 0x00;

	// ----------

	vTaskEndScheduler();

	// ----------

	// 2/10/2014 rmd : Using the built-in disable ALL priority configurable interrupts. This
	// disables all interrupts except for the NMI, Reset, and Hard Fault interrupts.
	__disable_interrupt();
	
	// ----------

	// 2/10/2014 rmd : TWO-WIRE cable power down using brute force. After we killed all
	// interrupts we need to directly force this.
	TPM_2W_NEG_OFF;
	
	TPM_2W_POS_OFF;

	// ----------

	// 2/11/2014 rmd : UART 0 is used by the ISP. Reset to default state.
	((LPC_UART0_TypeDef*)LPC_UART0)->LCR = UART_LCR_DLAB_EN;

	((LPC_UART0_TypeDef*)LPC_UART0)->DLL = 0x01;
	((LPC_UART0_TypeDef*)LPC_UART0)->DLM = 0x00;

	((LPC_UART0_TypeDef*)LPC_UART0)->LCR = 0x00;

	((LPC_UART0_TypeDef*)LPC_UART0)->IER = 0x00;

	((LPC_UART0_TypeDef*)LPC_UART0)->FCR = 0x00;

	((LPC_UART0_TypeDef*)LPC_UART0)->ACR = 0x00;

	((LPC_UART0_TypeDef*)LPC_UART0)->ICR = 0x00;

	((LPC_UART0_TypeDef*)LPC_UART0)->FDR = 0x10;

	((LPC_UART0_TypeDef*)LPC_UART0)->TER = 0x80;
	
	// ----------

	// 2/11/2014 rmd : Timer 0 is used by the ISP. Note - statements enveloped by disabled timer
	// state.

	// 2/11/2014 rmd : Timer 0 may not have been powered at all. Especially for a RELEASE build.
	// Because we only use it to track the RTOS task stats and once each 5 seconds send to the
	// main card. So MUST make sure the peripheral is powered.
	CLKPWR_ConfigPPWR( CLKPWR_PCONP_PCTIM0, ENABLE );
	CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_TIMER0, CLKPWR_PCLKSEL_CCLK_DIV_4 );

	((LPC_TIM_TypeDef*)LPC_TIM0)->TIMER_CONTROL_REG = 0;

	// 2/11/2014 rmd : Writing ones to the bits clears any pending interrupts.
	((LPC_TIM_TypeDef*)LPC_TIM0)->INTERRUPT_REG = 0x0000003F;

	((LPC_TIM_TypeDef*)LPC_TIM0)->TC = 0;

	((LPC_TIM_TypeDef*)LPC_TIM0)->PRESCALE_MATCH_VALUE = 0;

	((LPC_TIM_TypeDef*)LPC_TIM0)->PRESCALE_COUNTER = 0;

	((LPC_TIM_TypeDef*)LPC_TIM0)->MATCH_CONTROL_REG = 0;

	((LPC_TIM_TypeDef*)LPC_TIM0)->MR0 = 0;
	((LPC_TIM_TypeDef*)LPC_TIM0)->MR1 = 0;
	((LPC_TIM_TypeDef*)LPC_TIM0)->MR2 = 0;
	((LPC_TIM_TypeDef*)LPC_TIM0)->MR3 = 0;

	((LPC_TIM_TypeDef*)LPC_TIM0)->CAPTURE_CONTROL_REG = 0;

	((LPC_TIM_TypeDef*)LPC_TIM0)->EXTERNAL_MATCH_REG = 0;

	((LPC_TIM_TypeDef*)LPC_TIM0)->COUNT_CONTROL_REG = 0;

	((LPC_TIM_TypeDef*)LPC_TIM0)->TIMER_CONTROL_REG = 0;

	// ----------

	// 2/11/2014 rmd : MUST return peripheral powering to reset value. I obtained this value by
	// reading the register right first thing in main. And the value does the trick. Without
	// this line the ISP will hard fault.
	((LPC_SC_TypeDef*)LPC_SC)->PCONP = 0x042887DE;

	// 2/11/2014 rmd : Set all peripheral dividers back to CCLK/4
	((LPC_SC_TypeDef*)LPC_SC)->PCLKSEL0 = 0x00;
	((LPC_SC_TypeDef*)LPC_SC)->PCLKSEL1 = 0x00;
	
	// ----------

	// 2/11/2014 rmd : Disconnect and disable PLL0 in stages. Waiting for the status before
	// continuing. DISCONNECT first. And then DISABLE.
	((LPC_SC_TypeDef*)LPC_SC)->PLL0CON = 0x1;  // disconnected but enabled
	((LPC_SC_TypeDef*)LPC_SC)->PLL0FEED = 0xAA;
	((LPC_SC_TypeDef*)LPC_SC)->PLL0FEED = 0x55;
	while (((LPC_SC_TypeDef*)LPC_SC)->PLL0STAT&(1<<25));

	((LPC_SC_TypeDef*)LPC_SC)->PLL0CON = 0x0;  // now disabled
	((LPC_SC_TypeDef*)LPC_SC)->PLL0FEED = 0xAA;
	((LPC_SC_TypeDef*)LPC_SC)->PLL0FEED = 0x55;
	while (((LPC_SC_TypeDef*)LPC_SC)->PLL0STAT&(1<<24));
	
	// ----------

	// This is the default flash read/write setting for IRC
	((LPC_SC_TypeDef*)LPC_SC)->FLASHCFG &= 0x0fff;
	((LPC_SC_TypeDef*)LPC_SC)->FLASHCFG |= 0x5000;
	
	// ----------

	//  Select the IRC as clk
	((LPC_SC_TypeDef*)LPC_SC)->CLKSRCSEL = 0x00;

	// not using XTAL anymore
	((LPC_SC_TypeDef*)LPC_SC)->SCS = 0x00;
	
	// ----------

	// 2/11/2014 rmd : Load the command to invoke ISP into the command array
	IAP_commands[ 0 ] = IAP_CMD_REINVOKE_ISP;

	// 2/11/2014 rmd : Enter ISP via IAP
	IAP_EXECUTE_CMD( IAP_commands, IAP_results );

	// ----------
}

/*
	// 1/30/2014 rmd : And what happens if we kill the power to UART_0. How will the IAP/ISP
	// communicate using it?
	CLKPWR_ConfigPPWR (CLKPWR_PCONP_PCUART0, DISABLE);
	CLKPWR_ConfigPPWR (CLKPWR_PCONP_PCUART1, DISABLE);
	CLKPWR_ConfigPPWR (CLKPWR_PCONP_PCUART2, DISABLE);
	CLKPWR_ConfigPPWR (CLKPWR_PCONP_PCUART3, DISABLE);

	// ----------

	SSP_Cmd( LPC_SSP0, DISABLE );

	// ----------

	I2C_DeInit( LPC_I2C0 );
*/


/*

disable IRQs (so an interrupt won't disrupt the sequential loads to the PLL0FEED registers, see user manual section 4.5.8)
set PLL0CON = 0x01 to take the system off PLL (with mbed default divider of /3, core speed is now 4MHz - see this thread for details about the clock configuration). Wait for the flag to read back the update.
set PLL0CON = 0x00 to power down the PLL unit. Wait for the flag to read back the update.
set FLASHCFG = 0x5??? (the bottom 12 bits must be preserved, hence the read). This tells the flash interface to wait 6 cycles between operations, the "safe" setting for all CPU clocks.
set CCLKCFG = 0x00, sets the clock input postscaler to divide by 1. Core is now running at the input clock of 12MHz for the moment, since we haven't moved to int RC yet... but we will on the next instruction.
set CLKSRCSEL = 0x00, chip is now using the internal RC oscillator (default 4MHz)
set SCS = 0x00, tells it that the external clock signal should not be used
sets up a function pointer to the pre-defined location for the IAP code. This is because, as Sam mentioned above, you can't get directly into ISP: rather, you have to call the IAP function with an argument of 57 (dec) which will then put you in ISP.

*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 7/28/2015 rmd : This needs to be 5 times 4 bytes long. To match how we write it into 5
// 32-bit registers.
const char RTC_DROP_INTO_ISP_STRING[] =	"DROP INTO ISP       ";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void IAP_test_to_invoke_isp( void )
{
	UNS_32	*tptr;

	tptr = (UNS_32*)&RTC_DROP_INTO_ISP_STRING;

	if( ((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG0 == *tptr++ )
	{
		if( ((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG1 == *tptr++ )
		{
			if( ((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG2 == *tptr++ )
			{
				if( ((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG3 == *tptr++ )
				{
					if( ((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG4 == *tptr++ )
					{
						__iap_invoke_isp();
					}
				}
			}
		}
	}
}	
	
/* ---------------------------------------------------------- */
extern void IAP_set_stage_for_isp_entry_and_cause_a_restart( void )
{
	UNS_32	*tptr;

	// ----------

	TPM_LED1_ON;  // scope marker

	// ----------
	
	// 2/18/2014 rmd : Write our magic string into the LPC176x RTC. When program starts, before
	// a minimal amount of hardware configuration has been done, we test for this string. If it
	// is present we drop into ISP mode.
	tptr = (UNS_32*)&RTC_DROP_INTO_ISP_STRING;
	
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG0 = *tptr++;
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG1 = *tptr++;
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG2 = *tptr++;
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG3 = *tptr++;
	((LPC_RTC_TypeDef*)LPC_RTC_BASE)->GPREG4 = *tptr++;

	// ----------

	// 2/11/2014 rmd : To focus on what we are doing. And to ensure the spin loop at the end
	// here is all that is going on while we wait for the wdt timeout to throw a reset.
	__disable_interrupt();
	
	// 2/11/2014 rmd : The Watchdog CANNOT be stopped once it has been started. So we must
	// assume it is presently running.

	// 2/11/2014 rmd : Load the minimum timeout value.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDTC = 0xFF;

	// 2/11/2014 rmd : Choose PCLK as the clock source.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDCLKSEL = 0x01;
	
	// 2/11/2014 rmd : Enable it.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDMOD = 0x03;

	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0xAA;
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0x55;

	// ----------

	TPM_LED1_OFF;  // scope marker

	// ----------

	// 2/12/2014 rmd : Wait for WD timeout. We ONLY sit here for maybe 50usec!
	for(;;);
}	

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
