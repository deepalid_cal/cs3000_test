//-----------------------------------------------------------------------------
//    $Id: MemBlkMgr.h,v 1.7 2011/12/09 00:47:16 dsquires Exp $
//    $Log: MemBlkMgr.h,v $
//    Revision 1.7  2011/12/09 00:47:16  dsquires
//    Revised return data types.
//
//    Revision 1.6  2011/11/19 21:24:34  dsquires
//    Modified block sizes and counts.
//
//    Revision 1.5  2011/11/17 22:17:43  dsquires
//    Revised block sizes and counts.
//
//    Revision 1.4  2011/11/16 20:25:24  dsquires
//    Work in progress,
//
//    Revision 1.3  2011/11/02 23:33:15  dsquires
//    Changed memory block sizes.
//
//    Revision 1.2  2011/10/28 23:09:57  dsquires
//    First cut. May have problems.
//
//    Revision 1.1  2011/10/28 18:07:47  dsquires
//    New files to implement a more robust memory block management scheme.
//
//-----------------------------------------------------------------------------
/* ---------------------------------------------------------- */

#ifndef INC_MEM_BLK_MGR_H_
#define INC_MEM_BLK_MGR_H_

#include "tpmicro_common.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//-----------------------------------------------------------------------------
//    These #defines configure the number of queues, block sizes and block
//   counts.
//-----------------------------------------------------------------------------


#define MEM_BLK_SIZES			(4)

// ----------

#define MEM_BLK_0_USABLE_SIZE	(50)
#define MEM_BLK_0_BLK_CNT		(12)

// ----------

#define MEM_BLK_1_USABLE_SIZE	(160)

// 11/15/2012 rmd : Set to 16 to support the debug blocks queued on the serial port.
#define MEM_BLK_1_BLK_CNT		(16)

// ----------

// 4/17/2013 rmd : Within the comm_mngr when one or more stations are ON takes one of these
// blocks. Also the ADC routins use to xfer the readings array.
#define MEM_BLK_2_USABLE_SIZE	(560)
#define MEM_BLK_2_BLK_CNT		(6)

// ----------

// 3/22/2013 rmd : Set the size to handle the maximum packet size. This includes ambles,
// header, crc, and payload. So the MAX payload is actually 20 bytes less than this value.
#define MEM_BLK_3_USABLE_SIZE	(TPMICRO_MAX_AMBLE_TO_AMBLE_PACKET_SIZE)

// 9/4/2013 rmd : These blocks are used when removing incoming packets from the ring
// buffers. Coming from both the MAIN BOARD and M1 ports (remember M2 has been rendered
// obsolete per David Bymas SSE Motherboard design). And when building outgoing messages
// from the TPMicro to the MAIN Board. That activity should be no more than one at a time.
// It used to be each alert message to the TPMicro consumed one of these blocks. And if
// several alerts were made boom boom boom we would run out of memory. That issue has been
// resolved by only taking the block size needed for the alert message. Also used by the ADC
// readings.
#define MEM_BLK_3_BLK_CNT		(6)

// ----------

//-----------------------------------------------------------------------------
//    Structure of header that precedes the usable part of a memory block
//-----------------------------------------------------------------------------
typedef struct
{
	UNS_8	MemBlkQIndex;		// Index of MEM_BLK_Q_DEF_s associated with this block

	UNS_8	ReferenceCnt;		// Number of tasks using this block concurrently

} MEM_BLK_HDR_s;

//-----------------------------------------------------------------------------
//    Structure that defines what is on a Free Block Queue
//-----------------------------------------------------------------------------
typedef struct
{
	xQueueHandle	xFreeBlockQHandle;		// FreeRTOS Queue Handle

	UNS_32			BlockSize;				// Usable Block size

	UNS_32			BlockCount;				// # Blocks created on this queue

	UNS_32			BlocksOnQueue;			// # Blocks presently on queue

	UNS_32			minBlocksLeft;			// the most we have seen taken

} MEM_BLK_Q_DEF_s;

// Global variables
extern UNS_16 g_MemMgrMemTakenFromHeap;

extern MEM_BLK_Q_DEF_s mem_blocks[ MEM_BLK_SIZES ];

// Global function declarations
extern BOOL_32 InitMemBlkQueues(void);

// ----------

extern void *get_mem_blk_debug( UNS_32 usable_size, char *fil, UNS_32 lin );

#define GetMemBlk( s )   get_mem_blk_debug( (s), CS_FILE_NAME( __FILE__ ), __LINE__ )

// ----------

extern BOOL_32 FreeMemBlk( void *pBlock );

extern void IncrementMemBlkRefCount( void *pBlock );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*MEMBLKMGR_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
