/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef STARTUP_H_
#define STARTUP_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Structure for a task creation table entry
typedef struct
{
	BOOL_32				bCreateTask;	// TRUE if this entry is to be processed

	pdTASK_CODE			pTaskFunc;		// The task function

	const char * const	task_name;		// The task name, maximum 16 chars. incl. NULL

	UNS_32				stack_depth;	// The depth of the stack in 32-bit words

	void				*task_info;		// Some detail the task may need. Like a serial port number.

	UNS_32				priority;		// Task priority

//  xTaskHandle			*pTaskHandle;	// ptr to handle of task  NOT NEEDED IN THIS APP

	UNS_32				*last_execution_time_stamp;
	
} TASK_TABLE_STRUCT;

#define NUMBER_OF_TABLE_TASKS	(9)

extern const TASK_TABLE_STRUCT task_table[ NUMBER_OF_TABLE_TASKS ];

extern UNS_32	task_last_execution_stamp[ NUMBER_OF_TABLE_TASKS ];

extern UNS_32	startup_task_last_execution_stamp;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//  Semaphore types for CreateSemaphores() function
typedef enum
{
	SEMA_BINARY_TYPE = 0,
	SEMA_COUNTING_TYPE = 1,
	SEMA_MUTEX_TYPE = 2

} SEMA_TYPE_e;

// Structure for a semaphore table entry
typedef struct
{
	BOOL_32             bCreateSema;		// TRUE if this entry is to be processed

	SEMA_TYPE_e         sema_type_e;		// Type of semaphore to be created

	xSemaphoreHandle    *pxSemaHandle;		// Pointer to semaphore handle

} SEMA_ENTRY_s;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//  Structure for a queue table entry
typedef struct
{
	BOOL_32			bCreateQueue;		// TRUE if this entry is to be processed

	UNS_32			uxMaxQueueItems;	// Max # items queue will hold

	UNS_32			uxItemSize;			// Size of each queue item

	xQueueHandle	*pxQueueHandle;		// Pointer to queue handle

} QUEUE_ENTRY_s;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	BOOL_32					bCreateTimer;				// TRUE if this entry is to be processed
	const char				*pcTimerName;				//name of the timer
	portTickType			xTimerPeriodInTicks;		//how often we want the timer to fire
	unsigned portBASE_TYPE	uxAutoReload;
	void					*pvTimerID;
	tmrTIMER_CALLBACK		pxCallbackFunction;			//the function the is called when the timer expires
	xTimerHandle			*pxTimerHandle;
} TIMER_ENTRY_s;

// For the semaphore creation table entries
#define xSEMPTR   (xSemaphoreHandle *)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define MAX_EEPROM_MESSAGES				5

#define MAX_IRRIGATION_MESSAGES			15

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern volatile UNS_32		reset_source_reg;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern xSemaphoreHandle	packet_rcvd_from_main_semaphore;


////////////////////////////////
// System Semaphore Handles
////////////////////////////////
extern xSemaphoreHandle g_xIrrigationMutex;

extern xSemaphoreHandle g_xErrorLogMutex;

extern xSemaphoreHandle g_xHardwareProfileMutex;

extern xSemaphoreHandle g_xAdcSema;

extern xSemaphoreHandle g_xEEPromMutex;

extern xSemaphoreHandle g_xI2cSema;


extern xSemaphoreHandle	decoder_list_MUTEX;

extern xSemaphoreHandle	decoder_control_MUTEX;

extern xSemaphoreHandle	decoder_statistics_MUTEX;

extern xSemaphoreHandle 	flow_count_MUTEX;

extern xSemaphoreHandle 	moisture_reading_MUTEX;

////////////////////////////////
// System Timer Handles
////////////////////////////////
extern xTimerHandle irrigation_task_timer;

extern xTimerHandle	last_msg_from_main_timer;

extern xTimerHandle	two_wire_activity_timer;


////////////////////////////////
// System Queue Handles
////////////////////////////////
extern xQueueHandle g_xFreeDataHandleQueue;

extern xQueueHandle comm_mngr_task_queue;

extern xQueueHandle g_xEEPromMsgQueue;

extern xQueueHandle	irrigation_task_queue;

extern xQueueHandle	two_wire_task_queue;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void check_main_stack_size();


extern void system_startup_task( void *pvParameters );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

