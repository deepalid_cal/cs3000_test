//-----------------------------------------------------------------------------
//  test.c - a test task for the serial driver
//-----------------------------------------------------------------------------
// LPC includes
#include "LPC17xx.h"
#include "lpc17xx_clkpwr.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_i2c.h"

// For RIT
#include "lpc17xx_rit.h"
#include "lpc17xx_libcfg.h"
#include "lpc17xx_gpio.h"

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

// Other includes
#include <string.h>

// TP Micro includes
#include "tp_micro_data_types.h"
#include "startup.h"
#include "MemBlkMgr.h"
#include "test.h"
#include "RingBuffer.h"

#if UART_TX_DMA != 0
#include "x_uart.h"
#else
#include "uart.h"
#endif

#define TIME_INTERVAL 	1000

// Heartbeat LED Bit for Port 0 on FDI board
#define FDI_HBEAT_LED   (1<<7)

#define FIRST_CTL_IDX   3
#define SECOND_CTL_IDX  3

// Short string
const S8 *TestStrings[8] =
{
   // 2 byte
   "*.",

   // 7 bytes
   "123456.",

   // 8 bytes
   "1234567.",

   // 15 bytes
   "12345678901234.",

   // 16 bytes
   "This is so cool.",

   // 45 bytes
   "The quick brown fox jumped over the lazy dog.",

   // 112 bytes
   "I only wish I had lots of money when I find out that Wall Street is ripping us off left and right without shame.",

   // 72 bytes
   "THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG, but that's not unexpected.",
};

xSemaphoreHandle xRIT_Semaphore = NULL;

/************************** PRIVATE FUNCTION *************************/
void RIT_IRQHandler(void);

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
/*********************************************************************//**
 * @brief		RIT interrupt handler sub-routine
 * @param[in]	None
 * @return 		None
 **********************************************************************/
void RIT_IRQHandler(void)
{
   static UNS_32 counter;

   signed portBASE_TYPE	lHigherPriorityTaskWoken = pdFALSE;

   RIT_GetIntStatus(LPC_RIT); //call this to clear interrupt flag

   // Signal the task every 4 interrupts (seconds)
   counter++;
   if( 0 == (counter %4 ))
   {
      xSemaphoreGiveFromISR( xRIT_Semaphore, &lHigherPriorityTaskWoken );
   }

   portEND_SWITCHING_ISR( lHigherPriorityTaskWoken );
}

void vTest_TaskRIT( void *pvParameters )
{
   /* The parameters are not used. */
   ( void ) pvParameters;
    // Configure Heartbeat LED (driven by P0.7)

   // Create a binary semaphore for the timer
   vSemaphoreCreateBinary( xRIT_Semaphore );

   // Take it once
   xSemaphoreTake(xRIT_Semaphore, portMAX_DELAY);

   RIT_Init(LPC_RIT);
   /* Configure time_interval for RIT
	 * In this case: time_interval = 1000 ms = 1s
	 * So, RIT will generate interrupt each 1s
	 */
   RIT_TimerConfig(LPC_RIT,TIME_INTERVAL);

   NVIC_EnableIRQ(RIT_IRQn);

   // The main task loop
   while( 1 )
   {
      // Wait for semaphore
      if( xSemaphoreTake(xRIT_Semaphore, portMAX_DELAY) == pdTRUE )
      {
         // Turn on the Heartbeat LED
         //LPC_GPIO0->FIOSET = FDI_HBEAT_LED;
   
         vTaskDelay( MS_to_TICKS( 100 ) );
   
         // Turn off the heartbeat LED
         //LPC_GPIO0->FIOCLR = FDI_HBEAT_LED;
      }
   }
}

// MAKE SURE THIS IS BIG ENOUGH!!
#define RB_MSG_MAX               128
#define MSG_TERMINATION_CHAR     '.'

//-----------------------------------------------------------------------------
//    Private function to poll ring buffer for new data
//-----------------------------------------------------------------------------
static UNS_8 get_next_rb_char( UART_RINGBUFFER_s *pRingBuffer)
{
   UNS_8 c;
   while( !bIsRingBufferDataAvail( pRingBuffer ) )
   {
      vTaskDelay( MS_to_TICKS( 1 ) );
      // taskYIELD();
   }

   // Get the next byte from the ring buffer
   c = RingBufferGet( pRingBuffer );

   return c;
}

// The Task parameter is the index for the UART Control Structure
void vRingBufferRxTask( void *pvParameters )
{
   UNS_8 *pBuffer;
   UNS_8 *pMsg;
   UART_RINGBUFFER_s *pRingBuffer_s;
   UNS_16 read_cnt;
   BOOL_32 bEOM;     // End of message flag
   DATA_HANDLE_s  DataHandle;
   void *pBlock;
   long lstatus;
   UNS_32 rxUART;
   UNS_32 txUART;
   UNS_32 *pParms;
   
   // Get UART ports to use for Rx and Tx
   pParms = (UNS_32 *) pvParameters;

   rxUART = *pParms++;
   txUART = *pParms;

   // Point to our UART's ring buffer structure
   pRingBuffer_s = &g_UartCtl_s[ rxUART ].ringbuffer_s;

   // allocate some memory for our message buffer
   pBuffer = pvPortMalloc( RB_MSG_MAX );

   // Make sure we got it
   if( !pBuffer ) DEBUG_STOP( TEST + 20 );

   // Clear/initialize the ring buffer to empty
   initRingBuffer(pRingBuffer_s);

   while( TRUE )
   {
      // Point to start of our buffer
      pMsg = pBuffer;
      read_cnt = 0;
      bEOM = FALSE;

      // Read from ring buffer until we process a period ('.') or are about to
      // overflow our message buffer
      while( (bEOM == FALSE) && (read_cnt < RB_MSG_MAX ) )
      {
         *pMsg = get_next_rb_char( pRingBuffer_s );
         read_cnt++;

         if( *pMsg == MSG_TERMINATION_CHAR )
         {
            // We received a complete message. Send it to our UART's transmitter

            // Allocate a free block for our message, wait if necessary
            pBlock = GetMemBlk( read_cnt, portMAX_DELAY );
            // Make sure we got a block
            if( pBlock != NULL) )
            {
               // Copy the message into the block
               memcpy( pBlock, pBuffer, read_cnt );

               // Set up a DATA HANDLE for the message
               DataHandle.dptr = pBlock;
               DataHandle.dlen = read_cnt;

               // Post the initialized DATA_HANDLE_s on the DATA HANDLE queue for our UART's transmits

			   this queue does not hold DATA_HANDLE type any longer!!!!
             

               lstatus = xQueueSendToBack( g_UartCtl_s[ txUART ].xTxDataHandleQueue, &DataHandle, portMAX_DELAY ) ;

               // Make sure it worked
               if( lstatus == pdFALSE )
               {
                  DEBUG_STOP( TEST + 21 );
               }

               // Force an exit of the inner loop
                  bEOM = TRUE;
            }
            else
            {
               // GetMemBlk() failed!!
               DEBUG_STOP( TEST + 22 );
            }
         }
         else  // We didn't see the message termination character yet
         {
               pMsg++;  // Bump the message pointer
         }
      }  // end inner while()

   }  // end outer while()
}

// Buffer for vTaskList output
portCHAR xbuf[500];

void vTest_Task( void *pvParameters )
{
   DATA_HANDLE_s DataHandle_s;
   long lstatus;
   void *pBlock;
   UNS_16 block_size;
   const S8 *pMsgString;
   UNS_32 loop_counter;
   UNS_32 str_idx;

   /* The parameters are not used. */
   ( void ) pvParameters;

   vTaskDelay( 1000 );
   
   loop_counter = 0;
   str_idx = 0;

   while( 1 )
   {
      if( 0 == (loop_counter % 100) )
      {
         // Use vTaskList output as message
         vTaskList( xbuf);
         pMsgString = (const S8 *)&xbuf;
      }
      else
      {
         // Rotate among the test strings
         str_idx = loop_counter % 8;
         pMsgString = TestStrings[ str_idx ];
      }

      pMsgString = TestStrings[ 4 ];   // Choose "this is so cool."


      // Get size of test string selected (w/o terminating null)
      block_size = strlen( pMsgString );
    
      // Get a suitable free memory block in which to store the test string data
      pBlock = GetMemBlk( block_size, portMAX_DELAY );
      //  Check the result status
      if( pBlock )
      {
         // We got a data block of (at least) the requested size
         DataHandle_s.dptr = (UNS_8 *) pBlock;

         // Set the memory block reference count to 2
         SetMemBlkRefCount( pBlock, 2 );

         // We will now copy the selected string (sans terminating null) into the buffer
         memcpy( (void *) pBlock, (const void *) pMsgString, block_size );

         // Set the exact data length in the DATA HANDLE structure
         DataHandle_s.dlen = block_size;



			   this queue does not hold DATA_HANDLE type any longer!!!!

         // Post the initialized DATA_HANDLE_s on the DATA HANDLE queue for UART0 transmits
         lstatus = xQueueSendToBack( g_UartCtl_s[ FIRST_CTL_IDX ].xTxDataHandleQueue, &DataHandle_s, portMAX_DELAY ) ;

         //  Check the result status
         if( pdTRUE == lstatus )
         {
            // The DATA HANDLE was successfully placed on the UART's transmit queue
            if( loop_counter == 1 )
            {
               // Only now enable the driver for UART being tested
               g_UartCtl_s[ FIRST_CTL_IDX ].bEnaCmd = TRUE;
             }
         }
         else
         {
            // Error with xQueueSendToBack()
            DEBUG_STOP( TEST + 0 );
         }




			   this queue does not hold DATA_HANDLE type any longer!!!!

         lstatus = xQueueSendToBack( g_UartCtl_s[ SECOND_CTL_IDX ].xTxDataHandleQueue, &DataHandle_s, portMAX_DELAY ) ;

         //  Check the result status
         if( pdTRUE == lstatus )
         {
            // The DATA HANDLE was successfully placed on the UART's transmit queue
            if( loop_counter == 1 )
            {
               // Only now enable the driver for UART being tested
               g_UartCtl_s[ SECOND_CTL_IDX ].bEnaCmd = TRUE;
             }
             vTaskDelay( MS_to_TICKS( 20 ) ); 
         }
         else
         {
            // Error with xQueueSendToBack()
            DEBUG_STOP( TEST + 1 );
         }
      }
      else
      {
         // No more free Blocks???
         DEBUG_STOP( TEST + 2 ); // Should not get here
      }

      loop_counter++;
   }
}

void vSeedXmit( void *pvParameters )
{
   DATA_HANDLE_s DataHandle_s;
   long lstatus;
   void *pBlock;
   UNS_16 block_size;
   const S8 *pMsgString;
   UNS_32 UartIdx;

   // The pointer points to the UART channel to send to
   UartIdx = *(( UNS_32 * ) pvParameters);

   vTaskDelay( MS_to_TICKS( 1000 ) );

   // Only now enable the driver for UARTs being tested
   g_UartCtl_s[ 3 ].bEnaCmd = TRUE;

   // Also enable UART0
   g_UartCtl_s[ 0 ].bEnaCmd = TRUE;

   // Also enable UART1
   g_UartCtl_s[ 1 ].bEnaCmd = TRUE;

   // Also enable UART2
   g_UartCtl_s[ 2 ].bEnaCmd = TRUE;

   // Pick a test string for the seed message
   pMsgString = TestStrings[ 4 ];

   // Get size of test string selected (w/o terminating null)
   block_size = strlen( pMsgString );
    
   // Get a suitable free memory block in which to store the test string data
   pBlock = GetMemBlk( block_size, portMAX_DELAY );
   //  Check the result status
   if( pBlock )
   {
      // We got a data block of (at least) the requested size
      DataHandle_s.dptr = (UNS_8 *) pBlock;

      // We will now copy the selected string (sans terminating null) into the buffer
      memcpy( (void *) pBlock, (const void *) pMsgString, block_size );

      // Set the exact data length in the DATA HANDLE structure
      DataHandle_s.dlen = block_size;




			   this queue does not hold DATA_HANDLE type any longer!!!!

      // Post the initialized DATA_HANDLE_s on the DATA HANDLE queue for UART0 transmits
      lstatus = xQueueSendToBack( g_UartCtl_s[ UartIdx ].xTxDataHandleQueue, &DataHandle_s, portMAX_DELAY ) ;

      //  Check the result status
      if( pdTRUE == lstatus )
      {
         // The DATA HANDLE was successfully placed on the UART's transmit queue
         while(1)
         {
            vTaskDelay( MS_to_TICKS( 1000 ) );
         }
      }
      else
      {
         // Error with xQueueSendToBack()
         DEBUG_STOP( TEST + 0 );
      }
   }
   else
   {
      // Failed to get a block
      DEBUG_STOP( TEST + 1 );
   }
}

void vEnableUarts( void *pvParameters )
{
   /* The parameters are not used. */
   ( void ) pvParameters;

   vTaskDelay( MS_to_TICKS( 1000 ) );

   // Only now enable the driver for UARTs being tested

   // Also enable UART0
   g_UartCtl_s[ 0 ].bEnaCmd = TRUE;

   // Also enable UART1
   g_UartCtl_s[ 1 ].bEnaCmd = TRUE;

   // Also enable UART2
   //   UartCtl_s[ 2 ].bEnaCmd = TRUE;

   // Also enable UART3
   //   UartCtl_s[ 3 ].bEnaCmd = TRUE;

   // Now just hang around delaying
   while( TRUE )
   {
      vTaskDelay( MS_to_TICKS( 1000 ) );
   }
}