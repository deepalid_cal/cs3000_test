/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"hardwaremanager.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

TERMINAL_POC_FLOW_AND_SWITCH_STRUCT		terminal_poc;

// ----------

// 2/8/2013 rmd : Flag to send whats installed to the main board. Set when a change is
// detected.
BOOL_32						send_whats_installed_to_the_main_board;

WHATS_INSTALLED_STRUCT		whats_installed;


OUTPUT_CURRENT_STRUCT		output_current;

// ----------

// 5/8/2014 rmd : Indicates the state of the fuse. On reboot is (false) from C startup code.
// While true tells the main board in each and every message to the main board with a bit
// set in pieces.
BOOL_32	fuse_is_blown;

/* ---------------------------------------------------------- */
extern BOOL_32 card_and_terminal_are_present_for_this_station( UNS_32 pstation_num_0 )
{
	BOOL_32 rv;

	rv = (false);

	if( pstation_num_0 < MAX_CONVENTIONAL_STATIONS_PER_BOX )
	{
		rv = (whats_installed.stations[ (pstation_num_0/STATIONS_PER_CARD) ].card_present && whats_installed.stations[ (pstation_num_0/STATIONS_PER_CARD) ].tb_present);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

