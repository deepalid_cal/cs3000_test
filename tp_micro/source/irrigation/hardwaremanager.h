/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_HARDWARE_MANAGER_H_
#define INC_HARDWARE_MANAGER_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"

#include	"ringbuffer.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/8/2012 raf :  By definition, there are 4 light controls on a mainboard.
#define MAX_NUMBER_OF_TERMINAL_LIGHTS		( 4 )


/* ---------------------------------------------------------- */
// PERIPHERAL CARD DEFINES
/* ---------------------------------------------------------- */


// 8/20/2012 Starting i2C address for stations, all station cards increment afterwards.
#define I2C_ADDRESS_STATION_CARD_0			(0x21)

// ----------

// 8/20/2012 I2C address for the lights card.
#define I2C_ADDRESS_LIGHTS_CARD				(0x27)

// 8/20/2012 I2C address for the POC card.
#define I2C_ADDRESS_POC_CARD				(0x20)


//    Module Present GPIO input bit numbers for LPC176X port 1
#define MP_SIGNALS_GPIO_PORT			( 1 )     // The GPIO port number to read MP signals
#define MP_DASH_W_CARD_BIT				( 19 )     // Dash W card present bit
#define MP_DASH_W_TB_BIT				( 20 )     // Dash W terminal board present bit
#define MP_DASH_M_CARD_BIT				( 21 )     // Dash M card present bit
#define MP_DASH_M_TB_BIT				( 22 )     // Dash M terminal board present bit
#define MP_DASH_M_TYPE_BIT				( 23 )     // Dash M type bit
#define MP_2_WIRE_TB_BIT				( 24 )     // 2-wire terminal block present bit

//    Masks for different data bits
#define TRIAC_READ2_TBP_MASK			( 0x80 )     // Mask for nTB_PRESENT status
#define TRIAC_TB_PRESENT_BIT			(7)			// for use with the BIT_SET macros

#define POC_WMASK						( 0xf0 )     // P7..P4 : Inputs, P3..P0 : Outputs
#define POC_TRIAC_BIT_MASK				( 0x03 )     // P1, P0 are triac I/O bits
#define POC_TB_PRESENT_BIT				(7)			// for use with the BIT_SET macros
#define POC_READ1_BERMAD_MASK			( 0x40 )     // P6 is BERMAD status
#define POC_READ1_SWC_MASK				( 0x10 )     // P4 is SW_CLOSED status
#define POC_READ1_MV_MASK				( 0x01 )     // P4 is SW_CLOSED status
#define POC_READ1_PUMP_MASK				( 0x02 )     // P4 is SW_CLOSED status

// ----------

// 9/16/2013 rmd : The measured current when each output turns ON. As part of the staggerred
// turn ONs.
typedef struct
{
	UNS_32	stations[ MAX_CONVENTIONAL_STATIONS_PER_BOX ];
	
	UNS_32	lights[ MAX_NUMBER_OF_TERMINAL_LIGHTS ];

	UNS_32	terminal_mv;

	UNS_32	terminal_pump;

} OUTPUT_CURRENT_STRUCT;

// ----------

typedef struct
{
	// 5/22/2014 rmd : When (true) indeed there is fresh data to send to the main board. Which
	// by design does NOT have to go each and every second. Though that is what it presently
	// does.
	BOOL_32		send_to_main;


	UNS_32		accumulated_ms;			// Time (ms.) since last request

	UNS_32		accumulated_pulses;		// Flow sensor counts since last request


	UNS_32		pulses_5_second_count[ 5 ];		// Flow sensor counts over last 5 second window

	UNS_32		pulses_5_second_index;


	// 1/14/2015 rmd : Number of 4kHz ticks between the last two pulses. This is active for all
	// flow meter types. The TPMicro does not know which flow meter has been choosen by the user
	// nor what is installed. We always do the same thing.
	UNS_32		fm_delta_between_last_two_fm_pulses_4khz;
	
	// 1/15/2015 rmd : As we get a pulse from the once per second accumulated structure we clear
	// this to 0. When we don't see a pulse we just let this keep counting.
	UNS_32		fm_seconds_since_last_pulse;
	

	// 5/22/2014 rmd : This is updated once every 400ms as read during the terminal poc update
	// i2c function. Should of course not change from one reading to the next. However this
	// information is presently not used.
	//
	// 3/7/2016 rmd : And it is not yet transmitted to the main board that I can tell.
	BOOL_32		bermad_configuration;
	
	// 5/22/2014 rmd : Switch state as read during the terminal poc update i2c function.
	BOOL_32		sw_open;

    // 3/8/2016 mjb : This flag will be used to prevent including the SW1 status in the message to main
	// until we know for sure that it has been read. This flag will be set to false during 
	// startup and then set to true once we know SW1 has been read which is done in 
	// irrigation_manager line 847, just after we do a read of the SW1 data. 
    BOOL_32     sw_read_status_ready;
    
	
} TERMINAL_POC_FLOW_AND_SWITCH_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern TERMINAL_POC_FLOW_AND_SWITCH_STRUCT		terminal_poc;

// ----------

extern BOOL_32									send_whats_installed_to_the_main_board;

extern WHATS_INSTALLED_STRUCT					whats_installed;

// ----------

extern OUTPUT_CURRENT_STRUCT					output_current;

// ----------

extern BOOL_32	fuse_is_blown;
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */



extern BOOL_32 card_and_terminal_are_present_for_this_station( UNS_32 pstation_num_0 );



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*HARDWAREMANAGER_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

