/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_IRRIGATION_MANAGER_H_
#define INC_IRRIGATION_MANAGER_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"

#include	"adc.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/8/2013 rmd : Posted to the irrigation task queue once every 10 seconds to look for
// hardware changes.
#define IRRI_TASK_CMD_look_for_whats_installed				(1)

#define IRRI_TASK_CMD_short_detected						(2)

// 1/10/2013 rmd : Posted to the irrigation task queue once each 400ms via the irrigation
// timer.
#define IRRI_TASK_CMD_400ms_tick							(3)

#define IRRI_TASK_CMD_terminal_based_stations_from_main		(5)

#define IRRI_TASK_CMD_terminal_based_mv_and_pmp_from_main	(6)

#define IRRI_TASK_CMD_terminal_based_lights_from_main		(7)

// ----------

typedef struct
{
	UNS_32		command;

	// ----------
	
	// 4/10/2013 rmd : The following are used to transfer to the irrigation task the particular
	// TERMINAL outputs requested to be ON.
	
	UNS_64		terminal_stations_to_be_turned_ON;

	BOOL_32		terminal_mv_to_be_turned_ON;

	BOOL_32		terminal_pump_to_be_turned_ON;
	
	UNS_8		terminal_lights_to_be_turned_ON;

	// ----------

} IRRIGATION_TASK_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/10/2013 rmd : Once every 10 seconds we re-evaluate the hardware present. If there is a
// change we tell the main board.
#define HOW_OFTEN_TO_UPDATE_WHATS_INSTALLED_MS	(10*1000)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32		send_measured_currents_for_those_ON_to_main;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/10/2013 rmd : These are a reflection of the triacs presently ON.
extern UNS_64		terminal_stations_that_are_ON;

extern UNS_8		terminal_lights_that_are_ON;

extern BOOL_32		terminal_mv_is_ON;

extern BOOL_32		terminal_pump_is_ON;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/13/2013 rmd : The irrigation task not ready state of 0 is important. That is the state
// on startup. And is needed to supress the generation of the
// IRRI_TASK_CMD_update_output_states queue message from the RIT until we are ready to
// process them. If allowed to rack up from the start, if the main board has irrigation to
// turn ON, we get sync problems with the 100ms current number. We do not properly wait the
// full 400ms turn ON stagger for the first valve. The state starts out initialized to 0 by
// the C start up code.
#define		SHORT_STATE_not_ready								(0)

#define		SHORT_STATE_regular_irrigation						(1)

#define		SHORT_STATE_hunting									(2)

#define		SHORT_STATE_send_short_or_no_current_to_main		(3)

#define		SHORT_STATE_short_or_no_current_waiting_for_ack		(4)

// ----------

// 9/17/2013 rmd : Allow 4 shorts from an undetermined source before preventing any further
// irrigation.
#define		SHORT_ERROR_LIMIT				(4)

// ----------

typedef struct
{

	UNS_32		state;
	
	// ----------

	// 10/17/2016 rmd : It is possible that the ACK to a short or no current report never comes
	// to us. The state waiting_for_ack is extremely powerful. All future station turns ON's are
	// ignored until the state is returned back to normal irrigation. The state waiting for the
	// ACK is supposed to be transitory. One way it gets stuck is during a MLB. We don't clear
	// the battery backed MLB record and it is transferred to the TP Micro on power up when the
	// chain is down! To get the MV energized as needed. (I'm afraid to clear the
	// system_preserves copy of the delivered MLB on power up. It does self generate with each
	// token delivery. But clearing it may not fix the problem because the MLB info is delivered
	// in the very FIRST token. SO ANYWAY ... to make a long story short we are going to watch
	// for this latched up state and if it persists for too long we will clear it. It is after
	// all supposed to be a transitory state.
	//
	// 10/17/2016 rmd : SCENARIO IS - MLB energizes terminal-based MV, NO CURRENT occurs,
	// message to main board contain no current record, but netowrk not yet ready so message
	// content dropped. And no ACK ever comes back to the TP Micro.
	UNS_32		waiting_for_ACK_count;
	
	// ----------
	
	// 9/28/2015 rmd : After the short hunt and main board message exchange we want to wait each
	// of the to_be_turned_ON control variables to be re-loaded via a rcvd message from main and
	// the subsequent queue messages from the two-wire task. This is all about knowing when to
	// clear the error_short_with_nothing_ON_count and error_short_hunt_exhausted_count
	// variables.
	BOOL_32		station_q_message_arrived;

	BOOL_32		poc_mv_q_message_arrived;

	BOOL_32		lights_q_message_arrived;
	
	// ----------

	// 9/23/2013 rmd : A short occurred but no outputs were actually ON. At least according to
	// their indicators.
	UNS_32		error_short_with_nothing_ON_count;

	// 9/23/2013 rmd : A short occurred and after proceeding through the hunt process we came up
	// empty - meaning the output was not found. We keep a count of this occurance for a group
	// of valves ON cause it can lead to a pretty nasty behaviour. 
	UNS_32		error_short_hunt_exhausted_count;

	// ----------

	// 9/16/2013 rmd : The remaining to be activated during the hunt.

	UNS_64		stations_to_test;
	
	UNS_8		lights_to_test;
	
	BOOL_32		mv_test;
	
	BOOL_32		pump_test;
	
	// ----------

	// 9/16/2013 rmd : Station NUMBER or light NUMBER of the output being tested. NOTE - for
	// station and lights is a 1 BASED number (not 0 based) to facilitate knowing when a station
	// is being tested.
	UNS_32		station_being_tested_1;
	
	UNS_32		light_being_tested_1;
	
	BOOL_32		mv_being_tested;
	
	BOOL_32		pump_being_tested;

	// ----------

	// 9/16/2013 rmd : Holding variable for the results of the hunt for a shorted station.
	TERMINAL_SHORT_OR_NO_CURRENT_STRUCT		short_or_no_current_report;
	
	// ----------

} SHORT_CONTROL_STRUCT;


extern SHORT_CONTROL_STRUCT	scs;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void irri_timer_handler( xTimerHandle xTimer );



extern void irrigation_task( void *pvParameters );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

