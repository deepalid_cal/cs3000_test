/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"irrigation_manager.h"

#include	"i2cdrvr.h"

#include	"utils.h"

#include	"25lc640a.h"

#include	"tpmicro_comm_mngr.h"

#include	"eeprom_map.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32	g_ic2_success_count = 0;

UNS_32	g_ic2_error_count = 0;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/2/2013 rmd : When set (true) this variable has the effect of freezing the output state
// until the next outgoing message to main. ACTUALLY VERY POWERFUL.
BOOL_32		send_measured_currents_for_those_ON_to_main;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/10/2013 rmd : These are the desired triac states as rcvd from the main board. So, per
// the main board the stations that should be ON.
UNS_64		terminal_stations_to_be_turned_ON;

UNS_8		terminal_lights_to_be_turned_ON;

BOOL_32		terminal_mv_to_be_turned_ON;

BOOL_32		terminal_pump_to_be_turned_ON;


UNS_32		pre_turn_ON_100ma_measurement;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The new triac states to be set the next update cycle
UNS_64		terminal_stations_next_command;

UNS_8		terminal_lights_next_command;

// 4/10/2013 rmd : (true) means we are turning this one ON next.
BOOL_32		terminal_mv_next_command;

BOOL_32		terminal_pump_next_command;


// 4/10/2013 rmd : The current states of the triacs.
UNS_64		terminal_stations_that_are_ON;

UNS_8		terminal_lights_that_are_ON;

BOOL_32		terminal_mv_is_ON;

BOOL_32		terminal_pump_is_ON;


// 4/11/2013 rmd : This group used when figuring the measured current delta. The bit # of
// the last triac turned on (or -1 if not turned on).
INT_32	terminal_just_turned_ON_station_number;

INT_32	terminal_just_turned_ON_light_number;

BOOL_32	terminal_just_turned_ON_mv;

BOOL_32	terminal_just_turned_ON_pump;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/16/2013 rmd : Structure that controls the hunt for a short.
SHORT_CONTROL_STRUCT	scs;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void ping_i2c_station_triac_card( UNS_32 pcard, BOOL_32 *save_whats_installed )
{
	// 2/8/2013 rmd : Build up a message to ONLY read the state of the outputs and compare to
	// the last written state. If we get a response and it matches, if not already marked as
	// present we mark the card as present. In the case of the POC card we additionally check to
	// make sure it is in the list of pocs.

	I2C_M_SETUP_Type	transfer_setup;

	volatile UNS_8		rdata[ 2 ];
	
	I2C_CARD_DETAILS_STRUCT		*icd; 

	// ----------

	// 2/11/2013 rmd : We allow the light card to use this function too. So allowable indexes
	// are 0..6 for a total of 7 different cards.
	if( pcard > STATION_CARD_COUNT )
	{
		DEBUG_STOP();
	}
	else
	{
		// ----------

		// 2/11/2013 rmd : We use this function for the lights card as well as the station cards.
		// Since the lights card is just another station card. And as such we need to set up for
		// which whats_installed structure we are working with.
		if( pcard < STATION_CARD_COUNT )
		{
			icd = &whats_installed.stations[ pcard ];
		}
		else
		{
			icd = &whats_installed.lights;
		}
		
		// ----------

		memset( &transfer_setup, 0x00, sizeof( transfer_setup ) );
		
		// ----------

		transfer_setup.sl_addr7bit = I2C_ADDRESS_STATION_CARD_0 + pcard;
		
		// ----------

		// 2/8/2013 rmd : NOTE by virtue of the memset, the tx length and tx_data ptr are NULL
		// already. And that is what we want for a read only operation. Also retransmissions is also
		// 0.
		
		// ----------

		// 2/8/2013 rmd : Read both bytes. The triacs state and the terminal block present.
		transfer_setup.rx_length = 2;

		transfer_setup.rx_data = (UNS_8*)&rdata;
	
		// ----------
		
		// 2/8/2013 rmd : This read only exchange is no more than 200usec.
		if( I2cTransferData( &transfer_setup, I2C_TRANSFER_INTERRUPT ) == SUCCESS )
		{
			if( !icd->card_present )
			{
				// 2/8/2013 rmd : If the card hasn't been seen as present acknowledge that. The index works
				// for the lights card too.
				pre_defined_alert_to_main( TPALERT_Triac_1_8_card_installed + (2*pcard) );

				// ----------

				icd->card_present = (true);
				
				// ----------

				*save_whats_installed = (true);
			}
			
			// 2/8/2013 rmd : If the bit is set it means the terminal is not present. Hardware makes
			// inverse logic.
			if( !B_IS_SET( rdata[ 1 ], TRIAC_TB_PRESENT_BIT ) )
			{
				// 2/8/2013 rmd : So the terminal is present. See if that news!
				if( !icd->tb_present )
				{
					// 2/8/2013 rmd : If the card hasn't been seen as present acknowledge that. The index works
					// for the lights card too.
					pre_defined_alert_to_main( TPALERT_Triac_1_8_tb_installed + (2*pcard) );
	
					// ----------
	
					icd->tb_present = (true);
					
					// ----------
	
					*save_whats_installed = (true);
				}
			}
			else
			{
				// 2/8/2013 rmd : So the terminal is not installed. See if that is news.
				if( icd->tb_present )
				{
					// 2/8/2013 rmd : If the card has been seen as present. The index works
					// for the lights card too.
					pre_defined_alert_to_main( TPALERT_Triac_1_8_tb_removed + (2*pcard) );
	
					// ----------
	
					icd->tb_present = (false);
					
					// ----------
	
					*save_whats_installed = (true);
				}
			}
		}
		else
		{
			// 2/8/2013 rmd : So the transaction failed. If the bus reset line is not active the card is
			// considered to be uninstalled.
			if( TPM_TRIAC_MR_IS_PASSIVE )
			{
				// 2/8/2013 rmd : The card is assumed to be not installed. See if that is news.
				if( icd->card_present )
				{
					// 2/8/2013 rmd : If the card has been seen as present acknowledge that. The index works
					// for the lights card too.
					pre_defined_alert_to_main( TPALERT_Triac_1_8_card_removed + (2*pcard) );
	
					// ----------
	
					icd->card_present = (false);
					
					// ----------
	
					*save_whats_installed = (true);
				}
				
				// 2/8/2013 rmd : You know, I don't think we have to mark the terminal as also not
				// installed. Missing one of the two is enough to stop attempts to turn on stations. And
				// what if the terminal is still actually present. An alert stating otherwise will just
				// raise questions. So leave the terminal setting as is. The terminal setting will therefore
				// only accurately reflect the terminal presence when its corresponding i2c card is in
				// place. And I say that's okay.
				//
				// 3/6/2013 rmd : AJ has commented on this behavior and I think he is right. Maybe they have
				// removed both. Is that what they normally do? I dunno. But AJ thought it best to mark the
				// terminal as have being removed as well. So that's what we'll do.
				if( icd->tb_present )
				{
					pre_defined_alert_to_main( TPALERT_Triac_1_8_tb_removed + (2*pcard) );
	
					// ----------
	
					icd->tb_present = (false);
					
					// ----------
	
					*save_whats_installed = (true);
				}

			}

		}  // transfer failed
	
	}  // pcard is within range
}

/* ---------------------------------------------------------- */
static void ping_poc_card( BOOL_32 *save_whats_installed )
{
	// 2/8/2013 rmd : Build up a message to ONLY read the state of the outputs and compare to
	// the last written state. If we get a response and it matches, if not already marked as
	// present we mark the card as present. In the case of the POC card we additionally check to
	// make sure it is in the list of pocs.

	I2C_M_SETUP_Type		transfer_setup;

	volatile UNS_8			rdata[ 2 ];
	
	// ----------

	memset( &transfer_setup, 0x00, sizeof( transfer_setup ) );
	
	// ----------

	transfer_setup.sl_addr7bit = I2C_ADDRESS_POC_CARD;
	
	// ----------

	// 2/8/2013 rmd : NOTE by virtue of the memset, the tx length and tx_data ptr are NULL
	// already. And that is what we want for a read only operation. Also retransmissions is also
	// 0.
	
	// ----------

	// 2/8/2013 rmd : For the POC the terminal present is in the first byte.
	transfer_setup.rx_length = 1;

	transfer_setup.rx_data = (UNS_8*)&rdata;

	// ----------
	
	// 2/8/2013 rmd : This read only exchange is no more than 200usec.
	if( I2cTransferData( &transfer_setup, I2C_TRANSFER_INTERRUPT ) == SUCCESS )
	{
		if( !whats_installed.poc.card_present )
		{
			pre_defined_alert_to_main( TPALERT_POC_card_installed );

			// ----------

			whats_installed.poc.card_present = (true);
			
			// ----------

			*save_whats_installed = (true);
		}
		
		// 2/8/2013 rmd : If the bit is set it means the terminal is not present. Hardware makes
		// inverse logic.
		if( !B_IS_SET( rdata[ 0 ], POC_TB_PRESENT_BIT ) )
		{
			// 2/8/2013 rmd : So the terminal is present. See if that news!
			if( !whats_installed.poc.tb_present )
			{
				pre_defined_alert_to_main( TPALERT_POC_tb_installed );

				// ----------

				whats_installed.poc.tb_present = (true);
				
				// ----------

				*save_whats_installed = (true);
			}
		}
		else
		{
			// 2/8/2013 rmd : So the terminal is not installed. See if that is news.
			if( whats_installed.poc.tb_present )
			{
				// 2/8/2013 rmd : If the card has been seen as present. The index works
				// for the lights card too.
				pre_defined_alert_to_main( TPALERT_POC_tb_removed );

				// ----------

				whats_installed.poc.tb_present = (false);
				
				// ----------

				*save_whats_installed = (true);
			}
		}
	}
	else
	{
		// 2/8/2013 rmd : So the transaction failed. If the bus reset line is not active the card is
		// considered to be uninstalled.
		if( TPM_TRIAC_MR_IS_PASSIVE )
		{
			// 2/8/2013 rmd : So the card is not installed. See if that is news.
			if( whats_installed.poc.card_present )
			{
				// 2/8/2013 rmd : If the card has been seen as present acknowledge that. The index works
				// for the lights card too.
				pre_defined_alert_to_main( TPALERT_POC_card_removed );

				// ----------

				whats_installed.poc.card_present = (false);
				
				// ----------

				*save_whats_installed = (true);
			}
			
			// 2/8/2013 rmd : You know, I don't think we have to mark the terminal as also not
			// installed. Missing one of the two is enough to stop attempts to turn on stations. And
			// what if the terminal is still actually present. An alert stating otherwise will just
			// raise questions. So leave the terminal setting as is. The terminal setting will therefore
			// only accurately reflect the terminal presence when its corresponding i2c card is in
			// place. And I say that's okay.
			//
			// 3/6/2013 rmd : AJ has commented on this behavior and I think he is right. Maybe they have
			// removed both. Is that what they normally do? I dunno. But AJ thought it best to mark the
			// terminal as have being removed as well. So that's what we'll do.
			if( whats_installed.poc.tb_present )
			{
				pre_defined_alert_to_main( TPALERT_POC_tb_removed );

				// ----------

				whats_installed.poc.tb_present = (false);
				
				// ----------

				*save_whats_installed = (true);
			}

		}

	}  // transfer failed

}

/* ---------------------------------------------------------- */
static void look_for_whats_installed( void )
{
	// 2/8/2013 rmd : Takes about 1.2ms total time to scan all the I2C cards and read the
	// GPIO. Varies somewhat based on which cards are present.

	BOOL_32		present_state, save_whats_installed;

	// ----------

    save_whats_installed = (false);
	
	// ----------

	UNS_32 i;

	for( i=0; i<STATION_CARD_COUNT; i++ )
	{
		// 2/8/2013 rmd : About 200usec each.
		ping_i2c_station_triac_card( i, &save_whats_installed );
	}

	// ----------

	// 2/11/2013 rmd : LIGHTS - we're going to take advantage of the hardware by pinging the
	// lights card as if it we an additional station triac card. WHICH IT IS. So we can simply
	// refer to it as index 6 (0..5 are the actual station cards and 6 is the lights card).
	ping_i2c_station_triac_card( 6, &save_whats_installed );

	// ----------

	ping_poc_card( &save_whats_installed );

	// ----------

	UNS_32	mp_states;

	// 2/5/2013 rmd : Read the port where the inputs are tied.
	mp_states = ((LPC_GPIO_TypeDef*)LPC_GPIO1_BASE)->FIOPIN;
	
	// ----------

	// Do Dash W signals

	present_state = ( mp_states & (1<<MP_DASH_W_CARD_BIT)) ? 1 : 0;
	
	if( whats_installed.weather_card_present != present_state )
	{
		save_whats_installed = (true);
		
		if( whats_installed.weather_card_present )
		{
			// 2/11/2013 rmd : Must mean it is out now.
			pre_defined_alert_to_main( TPALERT_Weather_card_removed );
		}
		else
		{
			// 2/11/2013 rmd : Must mean it is in now.
			pre_defined_alert_to_main( TPALERT_Weather_card_installed );
		}

		whats_installed.weather_card_present = present_state;
	}

	present_state = ( mp_states & (1<<MP_DASH_W_TB_BIT)) ? 1 : 0;

	if( whats_installed.weather_terminal_present != present_state )
	{
		save_whats_installed = (true);
		
		if( whats_installed.weather_terminal_present )
		{
			// 2/11/2013 rmd : Must mean it is out now.
			pre_defined_alert_to_main( TPALERT_Weather_tb_removed );
		}
		else
		{
			// 2/11/2013 rmd : Must mean it is in now.
			pre_defined_alert_to_main( TPALERT_Weather_tb_installed );
		}

		whats_installed.weather_terminal_present = present_state;
	}

	// ----------

	// Do Dash M card

	present_state = ( mp_states & (1<<MP_DASH_M_CARD_BIT)) ? 1 : 0;
	
	if( whats_installed.dash_m_card_present != present_state )
	{
		save_whats_installed = (true);
		
		if( whats_installed.dash_m_card_present )
		{
			// 2/11/2013 rmd : Must mean it is out now.
			pre_defined_alert_to_main( TPALERT_Dash_M_card_removed );
		}
		else
		{
			// 2/11/2013 rmd : Must mean it is in now.
			pre_defined_alert_to_main( TPALERT_Dash_M_card_installed );
		}

		whats_installed.dash_m_card_present = present_state;
	}

	present_state = ( mp_states & (1<<MP_DASH_M_TB_BIT)) ? 1 : 0;

	if( whats_installed.dash_m_terminal_present != present_state )
	{
		save_whats_installed = (true);
		
		if( whats_installed.dash_m_terminal_present )
		{
			// 2/11/2013 rmd : Must mean it is out now.
			pre_defined_alert_to_main( TPALERT_Dash_M_tb_removed );
		}
		else
		{
			// 2/11/2013 rmd : Must mean it is in now.
			pre_defined_alert_to_main( TPALERT_Dash_M_tb_installed );
		}

		whats_installed.dash_m_terminal_present = present_state;
	}

	present_state = ( mp_states & (1<<MP_DASH_M_TYPE_BIT)) ? 1 : 0;

	if( whats_installed.dash_m_card_type != present_state )
	{
		// 2/11/2013 rmd : This whole dash m card type thing is not clear to me. I don't even
		// remember where it came from. And it should be qualified with the card installed
		// indication. Since without the card installed it always swings low.
		save_whats_installed = (true);
		
		pre_defined_alert_to_main( TPALERT_Dash_M_card_type_changed );

		whats_installed.dash_m_card_type = present_state;
	}

	// ----------

	// Check on the Two-Wire Terminal

	present_state = ( mp_states & (1<<MP_2_WIRE_TB_BIT)) ? 1 : 0;
	
	if( whats_installed.two_wire_terminal_present != present_state )
	{
		save_whats_installed = (true);
		
		if( whats_installed.two_wire_terminal_present )
		{
			// 2/11/2013 rmd : Must mean it is out now.
			pre_defined_alert_to_main( TPALERT_2Wire_tb_removed );
		}
		else
		{
			// 2/11/2013 rmd : Must mean it is in now.
			pre_defined_alert_to_main( TPALERT_2Wire_tb_installed );
		}

		whats_installed.two_wire_terminal_present = present_state;
	}

	// ----------
	
	if( save_whats_installed )
	{
		// 2/8/2013 rmd : There's been a change.
		send_whats_installed_to_the_main_board = (true);
		
		// ----------

		// 2/7/2013 rmd : Save the main board serial number.
		EEPROM_MESSAGE_STRUCT	eeqm;
		
		eeqm.cmd = EE_ACTION_WRITE;
		
		eeqm.EE_address = EE_LOCATION_WHATS_INSTALLED;
		
		eeqm.RAM_address = (UNS_8*)&whats_installed;
		
		eeqm.length = EE_SIZE_WHATS_INSTALLED;
		
		if( xQueueSendToBack( g_xEEPromMsgQueue, &eeqm, portNO_DELAY ) != pdTRUE )
		{
			flag_error( BIT_TPME_queue_full );
		}
		
		// ----------
	}
}

/* ---------------------------------------------------------- */
static UNS_32 update_i2c_station_triac_outputs( UNS_32 pcard, UNS_8 ptriac_data )
{
	UNS_32	rv;

	I2C_M_SETUP_Type	transfer_setup;

	UNS_8	wdata;

	volatile UNS_8	triac_state_read;

	// ----------
	
	rv = ERROR;

	// ----------

	// Catch card value errors
	if( pcard < STATION_CARD_COUNT )
	{
		memset( &transfer_setup, 0x00, sizeof( transfer_setup ) );
		
		// ----------

		transfer_setup.sl_addr7bit = I2C_ADDRESS_STATION_CARD_0 + pcard;
		
		// ----------

		// 2/8/2013 rmd : NOTE by virtue of the memset, the retransmissions is already 0. Which
		// should be at 0 for us.

		// ----------

		// 2/8/2013 rmd : Complement the data due to the hardware. Low turns em ON.
		wdata = ~ptriac_data;
	
		transfer_setup.tx_data = (UNS_8 *)&wdata;

		transfer_setup.tx_length = 1;

		// ----------

		// Set up pointer to receive buffer
		transfer_setup.rx_data = (UNS_8*)&triac_state_read;
	
		// 2/8/2013 rmd : Not necessary to read the terminal present byte within this function. That
		// is handled where we look for what is installed.
		transfer_setup.rx_length = 1;
		
		// ----------

		rv = I2cTransferData( &transfer_setup, I2C_TRANSFER_INTERRUPT );
	
		if( rv == SUCCESS )
		{
			g_ic2_success_count += 1;

			// 2/8/2013 rmd : Need to invert to get into our high bit means station is ON format.
			triac_state_read = ~triac_state_read;
			
			if( triac_state_read == ptriac_data )
			{
				// 9/16/2013 rmd : Update the reflection of what triacs are ON. Taking MUTEX as the UNS_64
				// is shared with the comm_mngr task and this is not an atomic operation.
				xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );

				putByteIntoLongLong( &terminal_stations_that_are_ON, pcard, triac_state_read );

				xSemaphoreGive( g_xIrrigationMutex );
			}
			else
			{
				flag_error( BIT_TPME_the_i2c_states_dont_match_what_we_sent );
			}
		}
		else  // The I2C transfer failed!
		{
			// 2/8/2013 rmd : So the transaction failed. If the bus reset line is not active this counts
			// as an error.
			if( TPM_TRIAC_MR_IS_PASSIVE )
			{
				alert_message_to_tpmicro_pile( "STATION card I2C error" );

				//whats_installed.stations[ pcard ].i2c_error_count += 1;

				g_ic2_error_count += 1;
			}
		}
	
	}  // card out of range ... do nothing

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 update_i2c_lights_triac_outputs( UNS_8 ptriac_data )
{
	UNS_32	rv;

	I2C_M_SETUP_Type	transfer_setup;

	UNS_8	wdata;

	volatile UNS_8	triac_state_read;

	// ----------

	memset( &transfer_setup, 0x00, sizeof( transfer_setup ) );
	
	// ----------

	transfer_setup.sl_addr7bit = I2C_ADDRESS_LIGHTS_CARD;
	
	// ----------

	// 2/8/2013 rmd : NOTE by virtue of the memset, the retransmissions is already 0. Which
	// should be at 0 for us.

	// ----------

	// 2/8/2013 rmd : Complement the data due to the hardware. Low turns em ON.
	wdata = ~ptriac_data;

	transfer_setup.tx_data = (UNS_8 *)&wdata;

	transfer_setup.tx_length = 1;

	// ----------

	// Set up pointer to receive buffer
	transfer_setup.rx_data = (UNS_8*)&triac_state_read;

	// 2/8/2013 rmd : Not necessary to read the terminal present byte within this function. That
	// is handled where we look for what is installed.
	transfer_setup.rx_length = 1;
	
	// ----------

	rv = I2cTransferData( &transfer_setup, I2C_TRANSFER_INTERRUPT );

	if( rv == SUCCESS )
	{
		g_ic2_success_count += 1;

		// 2/8/2013 rmd : Need to invert to get into our high bit means station is ON format.
		triac_state_read = ~triac_state_read;
		
		if( triac_state_read == ptriac_data )
		{
			// 9/16/2013 rmd : Update the reflection of what triacs are ON. Taking MUTEX as the UNS_64
			// is shared with the comm_mngr task and this is not an atomic operation.
			xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );

			terminal_lights_that_are_ON = triac_state_read;

			xSemaphoreGive( g_xIrrigationMutex );
		}
		else
		{
			flag_error( BIT_TPME_the_i2c_states_dont_match_what_we_sent );
		}
	}
	else  // The I2C transfer failed!
	{
		// 2/8/2013 rmd : So the transaction failed. If the bus reset line is not active this counts
		// as an error.
		if( TPM_TRIAC_MR_IS_PASSIVE )
		{
			alert_message_to_tpmicro_pile( "LIGHTS card I2C error" );

			//whats_installed.lights.i2c_error_count += 1;

			g_ic2_error_count += 1;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static void update_i2c_poc_card( BOOL_32 ppump_on, BOOL_32 pmaster_valve_on )
{
	// 1/14/2015 rmd : Called frequently within the IRRIGATION TASK context. Once every 400ms.
	
	Status		status;

	I2C_M_SETUP_Type TransferSetup;

	UNS_8 wdata;

	UNS_8 rdata[2];

	UNS_8 TriacData = 0;

	// ----------

	// 4/11/2013 rmd : This function can be routinely called even though the hardware is not
	// present. And that is not an error. It the way the code works. Only if we think the
	// hardware is present do we proceed.
	if( whats_installed.poc.card_present && whats_installed.poc.tb_present )
	{
		// 11/6/2012 rmd : Get the 7-bit slave address for the card. It is stored and transferred as
		// an UNS_32.
		TransferSetup.sl_addr7bit = I2C_ADDRESS_POC_CARD;
	
		// ----------
		
		// Get the data to write to the card and complement it
		TriacData = (pmaster_valve_on ? 1 : 0) + ( (ppump_on ? 1 : 0) << 1 );
	
		wdata = ~TriacData;
	
		// ----------
		
		// Set up pointer to transmit data
		TransferSetup.tx_data = (UNS_8 *)&wdata;
	
		// Set up pointer to receive buffer
		TransferSetup.rx_data = (UNS_8 *)&rdata[0];
	
		// Set # retransmissions allowed
		TransferSetup.retransmissions_max = 0;
	
		// Must be CARD_TYPE_POC, fix-up output data to make input bits 1's
		wdata |= POC_WMASK;
	
		// set # bytes to transmit (after slave address)
		TransferSetup.tx_length = 1;
	
		// set # bytes to receive (always 1)
		TransferSetup.rx_length = 1;
	
		// Perform the I2C transfer for the card
		status = I2cTransferData( &TransferSetup, I2C_TRANSFER_INTERRUPT );
	
		if( status == SUCCESS )
		{
			g_ic2_success_count += 1;
			
			// Process BERMAD status bit (if bit is read as 1, then POC is set up for BERMAD
			terminal_poc.bermad_configuration = ( rdata[0] & POC_READ1_BERMAD_MASK ) ? 1 : 0;
	
            // 3/8/2016 mjb : Need to monitor this bit in debug to make sure it is toggling
			// Process switch open status bit (data bit 4 reads 0 when switch is open)
			terminal_poc.sw_open = ( rdata[0] & POC_READ1_SWC_MASK ) ? 0 : 1;

			// 3/8/2016 mjb : Now we can set the SW1 read status ready bit to true, this means we are 
			// ready to start including SW1 status in the message to main. 
            terminal_poc.sw_read_status_ready = (true);
	
			// ----------

			// 4/11/2013 rmd : Analyze the inverse logic of the mv and pump bits. If set the output is
			// OFF. When zero the output is ON.
			BOOL_32 pumpStateRead = ( rdata[0] & POC_READ1_PUMP_MASK ) ? 0 : 1;
	
			BOOL_32 mvStateRead =  ( rdata[0] & POC_READ1_MV_MASK ) ? 0 : 1;
	
			if( (mvStateRead != pmaster_valve_on) || (pumpStateRead != ppump_on) )
			{
				flag_error(BIT_TPME_the_i2c_states_dont_match_what_we_sent);
			}
			else
			{
				terminal_mv_is_ON = mvStateRead;

				terminal_pump_is_ON = pumpStateRead;
			}

			// ----------
		}
		else  // The I2C transfer failed!
		{
			// 2/8/2013 rmd : So the transaction failed. If the bus reset line is not active this counts
			// as an error.
			if( TPM_TRIAC_MR_IS_PASSIVE )
			{
				alert_message_to_tpmicro_pile( "POC card I2C error" );

				//whats_installed.poc.i2c_error_count += 1;

				g_ic2_error_count += 1;
			}
		}

	}  // if whats installed indicates they are present

}

/* ---------------------------------------------------------- */
static void look_for_station_turn_offs( BOOL_32 *triac_state_change )
{
	// ----------
	
	// 9/13/2013 rmd : Executed within irrigation task context ONLY. Sets all turn OFFs needed
	// in the stations next command. Remember ALL triac turn OFFs are completed before any turn
	// ON's take place.
	
	// ----------

	UNS_64	delta, pending_off;

	// ----------

	// 9/16/2013 rmd : Working with what triacs are ON variable. Taking MUTEX as the UNS_64 is
	// shared with the comm_mngr task and this is not an atomic operation.
	xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );
	
	// 9/13/2013 rmd : The bits set in delta indicate the station states that must change.
	// Wether that be change from ON to OFF or OFF to ON. If the bit is set there is a needed
	// action.
	delta = terminal_stations_to_be_turned_ON ^ terminal_stations_that_are_ON;

	// 9/13/2013 rmd : If we AND the delta with the stations presently ON we get bits set for
	// those that are ON that need to be OFF.
	pending_off = (delta & terminal_stations_that_are_ON);

	xSemaphoreGive( g_xIrrigationMutex );

	// ----------

	// 9/13/2013 rmd : Do not test if triac states have already changed. All OFFs occur
	// together. We do set the flag though if there is an OFF to perform. This flag inhibits any
	// turn ONs.
	if( pending_off != 0 )
	{
		// 9/13/2013 rmd : Leave ON what is ON. And turn OFF what needs to be turned OFF. No
		// staggering needed for turn offs. Also note the fact that there are turn OFF's to perform,
		// will prevent any turn ONs till 400ms later on the next pass through this function.
		terminal_stations_next_command &= (~pending_off);
		
		*triac_state_change = (true);
	}
}

/* ---------------------------------------------------------- */
static void look_for_station_turn_on( BOOL_32 *one_turned_ON_ptr )
{
	// ----------
	
	// 9/13/2013 rmd : Executed within irrigation task context ONLY. Sets all turn OFFs needed
	// at once. But staggers turn ONs to meet our turn ON staggering requirement.
	
	// ----------

	UNS_32	bit;

	UNS_64	delta, pending_on;

	// ----------

	// 9/13/2013 rmd : Only try to turn on a new station if triac states have not yet changed
	// from the other outputs such as Lights, MV, or Pump.
	if( !(*one_turned_ON_ptr) )
	{
		// ----------
	
		// 9/16/2013 rmd : Working with what triacs are ON variable. Taking MUTEX as the UNS_64 is
		// shared with the comm_mngr task and this is not an atomic operation.
		xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );
		
		// 9/13/2013 rmd : The bits set in delta indicate the station states that must change.
		// Wether that be change from ON to OFF or OFF to ON. If the bit is set there is a needed
		// action.
		delta = terminal_stations_to_be_turned_ON ^ terminal_stations_that_are_ON;
	
		// 9/13/2013 rmd : If we AND the delta with the inverse of those ON we get bits set for
		// those that are not yet ON and need to be turned ON.
		pending_on = (delta & (~terminal_stations_that_are_ON));
	
		xSemaphoreGive( g_xIrrigationMutex );

		// ----------

		// 9/13/2013 rmd : Only if there is a new one to turn ON.
		if( pending_on != 0 )
		{
			// Find the highest numbered triac that needs to be turned on
			bit = 0;
	
			// Keep looping until a set bit is found
			while( !B_IS_SET_64( pending_on, bit ) )
			{
				// keep looking
				bit++;
			}
	
			// Set the bit for this triac in the Triac_States_next_command
			terminal_stations_next_command |= ((UNS_64)1 << bit);
	
			terminal_just_turned_ON_station_number = bit;
			
			// ----------
			
			// 9/13/2013 rmd : Well for sure there is a new station coming ON. Indicate not to turn any
			// others ON. And cause us to check the current reading 400ms from now. And inhibits us from
			// sending the current measurement results to the main board.
			*one_turned_ON_ptr = (true);
			
			// 9/13/2013 rmd : And snatch a copy of the latest 100msec current number. Remember it has
			// been 400ms (at least) since the last turn ON or turn OFF.
			pre_turn_ON_100ma_measurement = adc_readings.field_current_short_avg_ma;

			// ----------
		}

	}  // output state has already changed
}

/* ---------------------------------------------------------- */
static void look_for_lights_turn_offs( BOOL_32 *triac_state_change )
{
	// ----------
	
	// 9/13/2013 rmd : Executed within irrigation task context ONLY. Sets all turn OFFs needed
	// in the ligths next command. Remember ALL triac turn OFFs are completed before any turn
	// ON's take place.
	
	// ----------

	UNS_32	delta, pending_off;

	// ----------

	// 9/16/2013 rmd : Working with what lights triacs are ON variable. Taking MUTEX as the
	// UNS_8 is shared with the comm_mngr task and this may not an atomic operation.
	xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );
	
	// 9/13/2013 rmd : The bits set in delta indicate the output states that must change.
	// Whether that be change from ON to OFF or OFF to ON. If the bit is set there is a
	// needed action.
	delta = terminal_lights_to_be_turned_ON ^ terminal_lights_that_are_ON;

	// 9/13/2013 rmd : If we AND the delta with the stations presently ON we set the bits for
	// those that are ON that need to be OFF.
	pending_off = (delta & terminal_lights_that_are_ON);

	xSemaphoreGive( g_xIrrigationMutex );

	// ----------

	// 9/13/2013 rmd : Do not test if triac states have already changed. All OFFs occur
	// together. We do set the flag though if there is an OFF to perform. This flag inhibits any
	// turn ONs.
	if( pending_off != 0 )
	{
		// 9/13/2013 rmd : Leave ON what is ON. And turn OFF what needs to be turned OFF. No
		// staggering needed for turn offs. Also note: the fact that there are turn OFF's to
		// perform, will prevent any turn ONs till 400ms later on the next pass through this
		// function.
		terminal_lights_next_command &= (~pending_off);
		
		*triac_state_change = (true);
	}
}

/* ---------------------------------------------------------- */
static void look_for_lights_turn_on( BOOL_32 *one_turned_ON_ptr )
{
	// ----------

	UNS_32	bit;

	// 9/13/2013 rmd : Even though only 4 bits are active I think it is better to stick with the
	// native size as opposed to using a byte.
	UNS_32	delta, pending_on;

	// ----------

	// 9/13/2013 rmd : Only try to turn on a new station if triac states have not yet changed
	// from the other outputs such as Lights, MV, or Pump.
	if( !(*one_turned_ON_ptr) )
	{
		// ----------
	
		// 9/16/2013 rmd : Working with what lights triacs are ON variable. Taking MUTEX as the
		// UNS_8 is shared with the comm_mngr task and this may not an atomic operation.
		xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );
		
		// 9/13/2013 rmd : The bits set in delta indicate the station states that must change.
		// Wether that be change from ON to OFF or OFF to ON. If the bit is set there is a needed
		// action.
		delta = terminal_lights_to_be_turned_ON ^ terminal_lights_that_are_ON;
	
		// 9/13/2013 rmd : If we AND the delta with the inverse of those ON we get bits set for
		// those that are not yet ON and need to be turned ON.
		pending_on = (delta & (~terminal_lights_that_are_ON));
	
		xSemaphoreGive( g_xIrrigationMutex );
	
		// ----------

		// 9/13/2013 rmd : Only if there is a new one to turn ON.
		if( pending_on != 0 )
		{
			// Find the highest numbered triac that needs to be turned on
			bit = 0;
	
			// Keep looping until a set bit is found
			while( !B_IS_SET( pending_on, bit ) )
			{
				// keep looking
				bit++;
			}
	
			// Set the bit for this triac in the Triac_States_next_command
			terminal_lights_next_command |= (1 << bit);
	
			terminal_just_turned_ON_light_number = bit;
			
			// ----------
			
			// 9/13/2013 rmd : Well for sure there is a new station coming ON. Indicate not to turn any
			// others ON. And cause us to check the current reading 400ms from now. And inhibits us from
			// sending the current measurement results to the main board.
			*one_turned_ON_ptr = (true);
			
			// 9/13/2013 rmd : And snatch a copy of the latest 100msec current number. Remember it has
			// been 400ms (at least) since the last turn ON or turn OFF.
			pre_turn_ON_100ma_measurement = adc_readings.field_current_short_avg_ma;

			// ----------
		}

	}  // output state has already changed
}

/* ---------------------------------------------------------- */
static void look_for_mv_turn_off( BOOL_32 *triac_state_change )
{
	// 9/13/2013 rmd : So if the mv is ON and is supposed to be OFF, turn it OFF and flag a
	// triac state change.
	if( terminal_mv_is_ON )
	{
		if( !terminal_mv_to_be_turned_ON )
		{
			terminal_mv_next_command = (false);
			
			*triac_state_change = (true);
		}
	}
}

/* ---------------------------------------------------------- */
static void look_for_mv_turn_on( BOOL_32 *one_turned_ON_ptr )
{
	if( !(*one_turned_ON_ptr) )
	{
		if( terminal_mv_to_be_turned_ON )
		{
			if( !terminal_mv_is_ON )
			{
				terminal_mv_next_command = (true);

				terminal_just_turned_ON_mv = (true);
				
				// ----------
				
				// 9/13/2013 rmd : Well for sure there is a new station coming ON. Indicate not to turn any
				// others ON. And cause us to check the current reading 400ms from now. And inhibits us from
				// sending the current measurement results to the main board.
				*one_turned_ON_ptr = (true);
				
				// 9/13/2013 rmd : And snatch a copy of the latest 100msec current number. Remember it has
				// been 400ms (at least) since the last turn ON or turn OFF.
				pre_turn_ON_100ma_measurement = adc_readings.field_current_short_avg_ma;
	
				// ----------
			}
		}
	}
}

/* ---------------------------------------------------------- */
static void look_for_pump_turn_off( BOOL_32 *triac_state_change )
{
	// 9/13/2013 rmd : So if the pump is ON and is supposed to be OFF, turn it OFF and flag a
	// triac state change.
	if( terminal_pump_is_ON )
	{
		if( !terminal_pump_to_be_turned_ON )
		{
			terminal_pump_next_command = (false);
			
			*triac_state_change = (true);
		}
	}
}

/* ---------------------------------------------------------- */
static void look_for_pump_turn_on( BOOL_32 *one_turned_ON_ptr )
{
	if( !(*one_turned_ON_ptr) )
	{
		if( terminal_pump_to_be_turned_ON )
		{
			if( !terminal_pump_is_ON )
			{
				terminal_pump_next_command = (true);

				terminal_just_turned_ON_pump = (true);
				
				// ----------
				
				// 9/13/2013 rmd : Well for sure there is a new station coming ON. Indicate not to turn any
				// others ON. And cause us to check the current reading 400ms from now. And inhibits us from
				// sending the current measurement results to the main board.
				*one_turned_ON_ptr = (true);
				
				// 9/13/2013 rmd : And snatch a copy of the latest 100msec current number. Remember it has
				// been 400ms (at least) since the last turn ON or turn OFF.
				pre_turn_ON_100ma_measurement = adc_readings.field_current_short_avg_ma;
	
				// ----------
			}
		}
	}
}

/* ---------------------------------------------------------- */
static void update_all_i2c_cards( BOOL_32 *one_turned_ON_ptr )
{
	// 9/13/2013 rmd : Called ONLY from the irrigation task at a maximum 400ms rate.
	// Accomplishes all the turn OFF's first in one 400ms time slice. Then accomplishes the turn
	// ON's one station at a time. Skips turn ON's if there are any turn OFF's so we get a good
	// pre-turn ON current measurement.
	
	// 10/2/2013 rmd : It is assumed that the caller has assurred one_turned_ON is (false) upon
	// function entry. If not no new outputs will be turned ON.
	
	// ----------

	UNS_32		card;

	UNS_8		TriacData;

	BOOL_32		got_some_turn_OFFs;

	// ----------

	// 9/13/2013 rmd : Indicate no new triacs have just turned ON.
	terminal_just_turned_ON_station_number = -1;

	terminal_just_turned_ON_light_number = -1;

	terminal_just_turned_ON_pump = (false);

	terminal_just_turned_ON_mv = (false);

	// ----------

	// 9/16/2013 rmd : Working with triacs that are ON variables. Taking MUTEX as the UNS_64 is
	// shared with the comm_mngr task and this is not an atomic operation.
	xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );
	
	// 9/13/2013 rmd : And start out the next command as a sort of feedback loop. The next
	// command is adjusted to reflect the desired state. And the station that are ON field is
	// read from the I2C card registers reflecting what is actually ON. They should always
	// match. And there is a test when the card chip register is set.
	terminal_stations_next_command = terminal_stations_that_are_ON;

	terminal_lights_next_command = terminal_lights_that_are_ON;
	
	terminal_mv_next_command = terminal_mv_is_ON;

	terminal_pump_next_command = terminal_pump_is_ON;
	
	xSemaphoreGive( g_xIrrigationMutex );

	// ----------

	got_some_turn_OFFs = (false);
	
	// ----------

	look_for_mv_turn_off( &got_some_turn_OFFs );

	look_for_pump_turn_off( &got_some_turn_OFFs );

	look_for_station_turn_offs( &got_some_turn_OFFs );
	
	look_for_lights_turn_offs( &got_some_turn_OFFs );

	// ----------

	// 10/2/2013 rmd : Turn OFF's go first without any turn ON's. If there were any turn OFF's
	// skip any potential turn ON's.
	if( !got_some_turn_OFFs )
	{
		// 2/14/2013 rmd : If the POC card is not installed do not attempt this turn ON. Because if
		// the main board is asking us to turn ON our resisdent POC and it is not actually installed
		// we will never achieve that turn ON. So every 400ms we will keep trying to turn ON that
		// resident MV or PMP. And never get past them. When the main board actually incorporates
		// the POC not being installed, it likely will not be asking us to enegize a 'missing' POC.
		// But for now this layer of logic allows us to proceed to turing on stations and skip over
		// the request to turn on a POC that we don't think is present. And I think it will be okay
		// to leave in the long run.
		if( whats_installed.poc.card_present && whats_installed.poc.tb_present )
		{
			look_for_mv_turn_on( one_turned_ON_ptr );
	
			look_for_pump_turn_on( one_turned_ON_ptr );
		}
	
		look_for_station_turn_on( one_turned_ON_ptr );
	
		look_for_lights_turn_on( one_turned_ON_ptr );
	}

	// ----------

	// 10/2/2013 rmd : NOTE - even though there may be NO changes we still re-write the triac
	// image to the triac drivers. I suppose this is a cautionary tatic to ensure the triacs ON
	// are the ones we think are ON. Though technically shouldn't be necessary.
	
	// ----------
	
	// 11/5/2014 rmd : This one has built in protection against writing to the card if it is not
	// present.
	update_i2c_poc_card( terminal_pump_next_command, terminal_mv_next_command );
	
	// ----------

	for( card=0; card<STATION_CARD_COUNT; card++ )
	{
		if( whats_installed.stations[ card ].card_present && whats_installed.stations[ card ].card_present )
		{
			// Get triac output state data from the cmdTriacStates variable
			TriacData = getByteFromLongLong( &terminal_stations_next_command, card );
	
			update_i2c_station_triac_outputs( card, TriacData );
		}
	}

	// ----------

	if( whats_installed.lights.card_present && whats_installed.lights.card_present )
	{
		update_i2c_lights_triac_outputs( terminal_lights_next_command );
	}

	// ----------

	// 2/20/2013 rmd : Some test code to turn on all outputs in a ripple pattern. Useful during
	// debug and perhaps production test.
	/*
	UNS_8	lsta;

	if( ostation < 7 )
	{
		ostation += 1;	
	}
	else
	{
		ostation = 0;
	}	

	for( card=0; card<STATION_CARD_COUNT; card++ )
	{
		lsta = 0;

		B_SET( lsta, ostation );

		update_i2c_station_triac_outputs( card, lsta );

		update_i2c_lights_triac_outputs( lsta );
	}

	if( ostation % 2 )
	{
		update_i2c_poc_card( (true), (false) );
	}
	else
	{
		update_i2c_poc_card( (false), (true) );
	}
	*/
	
	// ----------
}

/* ---------------------------------------------------------- */
extern void turn_off_all_cards( void )
{
	UNS_32	card;

	for( card=0; card<STATION_CARD_COUNT; card++ )
	{
		if( whats_installed.stations[ card ].card_present && whats_installed.stations[ card ].card_present )
		{
			update_i2c_station_triac_outputs( card, 0 );
		}
	}

	if( whats_installed.lights.card_present && whats_installed.lights.card_present )
	{
		update_i2c_lights_triac_outputs( 0 );
	}

	// 11/5/2014 rmd : This one has built in protection against writing to the card if it is not
	// present.
	update_i2c_poc_card( FALSE, FALSE );
}

/* ---------------------------------------------------------- */
extern void irri_timer_handler( xTimerHandle xTimer )
{
	// This is NOT an interrupt. But rather executed within the context of the timer task.

	// 9/19/2013 rmd : After being started 400ms later the timer expires and we post a tick to
	// the irrigation task to move it along.
	IRRIGATION_TASK_QUEUE_STRUCT	itqs;

	itqs.command = IRRI_TASK_CMD_400ms_tick;

	// 3/7/2013 rmd : Post a message to the irrigation queue. Posting to the queue MUST be
	// successful otherwise is really quite a problem for the short hunt state machine. I think
	// the whole irrigation task will actually freeze up!!! So wait for portMAX_DELAY.
	xQueueSendToBack( irrigation_task_queue, &itqs, portMAX_DELAY );
}

/* ---------------------------------------------------------- */

// 9/24/2013 rmd : When the post turn ON measurement is less than the pre turn ON
// measurement, must be this much less before we report it.
#define		THRESHOLD_LESS_THAN_PRE_MEASUREMENT			(10)

// 9/19/2013 rmd : If the reading changed by more than this amount it will be reported as an
// updated current for the output just turned ON. I've settled on 10ma for now. We'll see if
// at this level it becomes a nuisance.

#define		THRESHOLD_DELTA_TO_REPORT_LESS_THAN_100_MA			(10)
#define		THRESHOLD_DELTA_TO_REPORT_LESS_THAN_200_MA			(20)
#define		THRESHOLD_DELTA_TO_REPORT_LESS_THAN_OTHERWISE_MA	(30)

static void __record_current_draw_from_last_triac_turn_ON( void )
{
	BOOL_32		update_ee_current_draw_table, error;

	char		str_48[ 48 ];
	
	UNS_32		post_turn_on_measurement, detection_threshold;
	
	UNS_32		new_reading, ma_diff;
	
	// ----------	

	error = (false);
	
	// 9/13/2013 rmd : This function is to run ONLY if we just turned on a triac output.
	if( (terminal_just_turned_ON_station_number == -1) && (terminal_just_turned_ON_light_number == -1) && (!terminal_just_turned_ON_mv) && (!terminal_just_turned_ON_pump) )
	{
		error = (true);
		
		alert_message_to_tpmicro_pile( "Unexpected execution to measure current!" );
	}
	
	// ----------	

	if( !error )
	{
		// ----------	
	
		update_ee_current_draw_table = (false);
	
		// ----------	
	
		// 9/12/2013 rmd : Get a copy of the most recently available 100msec current reading. This
		// value is developed within the context of the comm_mngr task. This however is a safe read
		// as it is an ATOMIC read. And surely when it is written to the whole value is written
		// atomically as well.
		post_turn_on_measurement = adc_readings.field_current_short_avg_ma;
	
		// ----------
	
		// 9/16/2013 rmd : To satisfy compiler.
		new_reading = 0;

		if( post_turn_on_measurement < pre_turn_ON_100ma_measurement )
		{
			new_reading = (pre_turn_ON_100ma_measurement - post_turn_on_measurement);
	
			// 9/24/2013 rmd : Not sure what this really means, but for now we're going to flag this as
			// a NO CURRENT regardless of by how much it fell. If the fall is greater than say 30ma I
			// think we should additionally throw a special alert line so we see this is happening.
			// Cause it really shouldn't be I suppose.
			if( new_reading >= THRESHOLD_LESS_THAN_PRE_MEASUREMENT )
			{
				snprintf( str_48, sizeof(str_48), "Current Unexpectedly fell by %dma", new_reading );
	
				alert_message_to_tpmicro_pile( str_48 );
			}

			new_reading = 0;
		}
		else
		{
			// 9/24/2013 rmd : ELSE the post measurement is >= the pre measurement.

			new_reading = (post_turn_on_measurement - pre_turn_ON_100ma_measurement);

			// 9/23/2013 rmd : The new_reading is the difference between the current measurement prior
			// to turn ON and after turn ON. If the difference is less than the threshold call it 0.
			/*
			if( post_turn_on_measurement < 100 )
			{
				detection_threshold = THRESHOLD_DELTA_TO_REPORT_LESS_THAN_100_MA;
			}
			else
			if( post_turn_on_measurement < 200 )
			{
				detection_threshold = THRESHOLD_DELTA_TO_REPORT_LESS_THAN_200_MA;
			}
			else
			{
				detection_threshold = THRESHOLD_DELTA_TO_REPORT_LESS_THAN_OTHERWISE_MA;
			}
			*/
			
			// 10/3/2013 rmd : We have discovered the this alogrithm of saying the detection threshold
			// grows as the current grows is not needed. As a matter of fact it creates a problem when
			// at an existing high xfmr load we turn on an output with a light on it. It may only source
			// say 25mA but that is under our 30mA threshold so it reports as a no current. So for now
			// I'm removing the staged threshold stuff all together. I still think it is a good idea to
			// provide for a 10mA movement else call it 0.
			detection_threshold = THRESHOLD_DELTA_TO_REPORT_LESS_THAN_100_MA;

			if( new_reading < detection_threshold )
			{
				new_reading = 0;
			}
		}
		
		// ----------	
	
		// 9/12/2013 rmd : Now we have the change in current for the last event.

		char device_string[ 10 ];

		// 9/13/2013 rmd : Use INT_32 cause that is what the abs function is expecting for
		// arguments.
		INT_32	*prior_measurement_ptr;

		INT_32	output_number_for_display;

		if( terminal_just_turned_ON_station_number != -1 )
		{
			output_number_for_display = terminal_just_turned_ON_station_number+1;

			prior_measurement_ptr = (INT_32*)&output_current.stations[ terminal_just_turned_ON_station_number ];

			snprintf( device_string, 10, "Station" );
			
			// ----------
			
			// 9/25/2013 rmd : Prepare if there is a NO_CURRENT.
			scs.short_or_no_current_report.terminal_type = OUTPUT_TYPE_TERMINAL_STATION;

			scs.short_or_no_current_report.station_or_light_number_0 = terminal_just_turned_ON_station_number;

			// ----------
		}
		else
		if( terminal_just_turned_ON_light_number != -1 )
		{
			output_number_for_display = terminal_just_turned_ON_light_number+1;

			prior_measurement_ptr = (INT_32*)&output_current.lights[ terminal_just_turned_ON_light_number ];

			snprintf(device_string, 10, "Light");
			
			// ----------
			
			// 9/25/2013 rmd : Prepare if there is a NO_CURRENT.
			scs.short_or_no_current_report.terminal_type = OUTPUT_TYPE_TERMINAL_LIGHT;

			scs.short_or_no_current_report.station_or_light_number_0 = terminal_just_turned_ON_light_number;

			// ----------
		}
		else
		if( terminal_just_turned_ON_mv )
		{
			output_number_for_display = 1;

			prior_measurement_ptr = (INT_32*)&output_current.terminal_mv;

			snprintf(device_string, 10, "MV");
			
			// ----------
			
			// 9/25/2013 rmd : Prepare if there is a NO_CURRENT.
			scs.short_or_no_current_report.terminal_type = OUTPUT_TYPE_TERMINAL_MASTER_VALVE;

			scs.short_or_no_current_report.station_or_light_number_0 = 0;  // not important

			// ----------
		}
		else
		if( terminal_just_turned_ON_pump )
		{
			output_number_for_display = 1;

			prior_measurement_ptr = (INT_32*)&output_current.terminal_pump;

			snprintf(device_string, 10, "Pump");
			
			// ----------
			
			// 9/25/2013 rmd : Prepare if there is a NO_CURRENT.
			scs.short_or_no_current_report.terminal_type = OUTPUT_TYPE_TERMINAL_PUMP;

			scs.short_or_no_current_report.station_or_light_number_0 =  0;  // not important

			// ----------
		}
		else
		{
			error = (true);

			alert_message_to_tpmicro_pile( "what's ON changed!" );
		}
		
		// ----------

		if( !error )
		{
			// 10/3/2013 rmd : For debug only.
			//snprintf( str_48, sizeof(str_48), "%s %d, pre=%dmA, post=%dmA", device_string, output_number_for_display, pre_turn_ON_100ma_measurement, post_turn_on_measurement );
			//alert_message_to_tpmicro_pile( str_48 );

			// 9/12/2013 rmd : Address the special case of NO CURRENT.
			if( new_reading == 0 )
			{
				// ----------

				// 10/3/2013 rmd : Make a basic NO_CURRENT alert for the tp micro pile.
				snprintf( str_48, sizeof(str_48), "No Current on %s %d", device_string, output_number_for_display );
				alert_message_to_tpmicro_pile( str_48 );

				// ----------

				// 9/24/2013 rmd : We ALWAYS send the report to the master. Turn ON's inhibited by change
				// state. But only inhibited till the next token response is sent (the report along with
				// it). UNLIKE a short during which turn ON's are inhibited till the master ACK's the short
				// report.
				scs.short_or_no_current_report.result = SHORT_OR_NO_CURRENT_RESULT_no_current;
				
				scs.state = SHORT_STATE_send_short_or_no_current_to_main;
				
				// 9/27/2013 rmd : PLEASE REALIZE a no current puts a PAUSE in the turn ON's of additional
				// valves (if there are more to turn ON). This is because we use the same sequence wether it
				// is a short or a no current. Perhaps they should've been separated messaging processes but
				// they are not. The pause comes about because we change the state of the scs.state to
				// waiting for an ACK. And that change of state causes us to ignore station updates from the
				// main board and also not to even work on turning on the outputs not on that we already
				// know about.
				
				// ----------

				if( *prior_measurement_ptr != 0 )
				{
					// 9/13/2013 rmd : Update measured current for this station and alert the main board.
					snprintf( str_48, sizeof(str_48), "%s %d load changed to %dmA (was %dmA)", device_string, output_number_for_display, new_reading, *prior_measurement_ptr );
	
					alert_message_to_tpmicro_pile( str_48 );

					*prior_measurement_ptr = 0;
	
					update_ee_current_draw_table = (true);
				}
			}
			else
			{
				ma_diff = abs( *prior_measurement_ptr - new_reading );
		
				if( *prior_measurement_ptr < 100 )
				{
					detection_threshold = THRESHOLD_DELTA_TO_REPORT_LESS_THAN_100_MA;
				}
				else
				if( *prior_measurement_ptr < 200 )
				{
					detection_threshold = THRESHOLD_DELTA_TO_REPORT_LESS_THAN_200_MA;
				}
				else
				{
					detection_threshold = THRESHOLD_DELTA_TO_REPORT_LESS_THAN_OTHERWISE_MA;
				}

				// 9/23/2013 rmd : And did the new_reading as compared to the previous measurment change by
				// at least the THRESHOLD amount? If so update and report it.
				if( ma_diff >= detection_threshold )
				{
					snprintf( str_48, sizeof(str_48), "%s %d's load changed to %dmA (was %dmA)",device_string, output_number_for_display, new_reading, *prior_measurement_ptr );
	
					alert_message_to_tpmicro_pile( str_48 );
	
					*prior_measurement_ptr = new_reading;
	
					update_ee_current_draw_table = (true);
				}
			}
		
			if( update_ee_current_draw_table )
			{
				EEPROM_MESSAGE_STRUCT	eeqm;
			
				eeqm.cmd = EE_ACTION_WRITE;
			
				eeqm.EE_address = EE_LOCATION_OUTPUT_CURRENT;
			
				eeqm.RAM_address = (UNS_8*)&output_current;
			
				eeqm.length = EE_SIZE_OUTPUT_CURRENT;
			
				if( xQueueSendToBack( g_xEEPromMsgQueue, &eeqm, portNO_DELAY ) != pdTRUE )
				{
					flag_error( BIT_TPME_queue_full );
				}
			}
		}
	
	}  // of not an error
}

/* ---------------------------------------------------------- */
void exit_the_short_hunt( UNS_32 *number_of_measured_valve_currents_ptr, BOOL_32 *go_measure_the_current_for_the_output_just_turned_ON_ptr )
{
	// 9/17/2013 rmd : For one of two reasons we are done hunting for a short. Either we
	// sequenced through all the stations that were ON, or we suffered a short and were not
	// testing a station. DO NOT set the short control struct state within this function. Were
	// this function is called from has different state desires.

	// ----------

	// 9/16/2013 rmd : Clear the triac state of those ON. NOTE - during the short hunting the
	// image of those taht are ON is INVALID! And that is intentional. We do not need it to be
	// accurate and do not attempt to keep it accurate. BUT before we return to routine
	// irrigation we must clean them up. NOTE - Working with an UNS_64 which is shared between
	// the comm_mngr task and the irrigation_manager task so need to use MUTEX.
	xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );
	
	terminal_stations_that_are_ON = 0;
	terminal_lights_that_are_ON = 0;
	terminal_mv_is_ON = (false);
	terminal_pump_is_ON = (false);
	
	xSemaphoreGive( g_xIrrigationMutex );

	// ----------

	// 9/16/2013 rmd : The short interrupted a normal turn ON sequence. Reset or clear the
	// current measurement control variables. Including any sending to main. I feel best if the
	// whole turn-on process was started over clean.
	*number_of_measured_valve_currents_ptr = 0;

	*go_measure_the_current_for_the_output_just_turned_ON_ptr = (false);

	send_measured_currents_for_those_ON_to_main = (false);
	
	// ----------

	// 3/7/2013 rmd : Wipe the stations that the main board thinks it wants ON. During the short
	// hunting process additional messages may come in from the main board requesting the
	// stations be turned on (as the main board has no idea a short and the hunt is occuring).
	// Those messages were dropped as the short hunt was underway. When the next message arrives
	// these will be restored.
	terminal_stations_to_be_turned_ON = 0;
	terminal_lights_to_be_turned_ON = 0;
	terminal_mv_to_be_turned_ON = (false);
	terminal_pump_to_be_turned_ON = (false);
	
	// ----------
	
	// 9/28/2015 rmd : And set the message variables to force refresh of those to be ON before
	// processing.
	scs.station_q_message_arrived = (false);

	scs.station_q_message_arrived = (false);

	scs.station_q_message_arrived = (false);
}

/* ---------------------------------------------------------- */
void irrigation_task( void *pvParameters )
{
	UNS_32	*task_watchdog_time_stamp;
	
	IRRIGATION_TASK_QUEUE_STRUCT	itqs;

	UNS_32		iii;
	
	BOOL_32		found_one;
	
	BOOL_32		go_measure_the_current_for_the_output_just_turned_ON;
	
	UNS_32		number_of_measured_valve_currents;
	
	// ----------

	task_watchdog_time_stamp = ((TASK_TABLE_STRUCT*)pvParameters)->last_execution_time_stamp;
	
	// ----------

	// 9/16/2013 rmd : Because these don't initialize to our -1 values we do so here. And to be
	// complete include all 4 variables.
	terminal_just_turned_ON_station_number = -1;
	terminal_just_turned_ON_light_number = -1;
	terminal_just_turned_ON_mv = (false);
	terminal_just_turned_ON_pump = (false);
	
	// ----------

	// 4/12/2013 rmd : Initialize explicitly for clarity.

	go_measure_the_current_for_the_output_just_turned_ON = (false);

	number_of_measured_valve_currents = 0;
	
	send_measured_currents_for_those_ON_to_main = (false);
	
	// ----------

	// 2/8/2013 rmd : So on startup the I2C bus MR line is driven LOW active. Turning OFF all
	// the triacs and resetting all the registers on the triac cards. When we release MR we
	// still need to wait 300ms for the RESET condition to be over. First lets wait 50ms to
	// assure a long solid reset state on the MR line.
	vTaskDelay( MS_to_TICKS(50) );
	
	TPM_TRIAC_MR_PASSIVE;

	// 2/8/2013 rmd : Give time for the reset pulse from the supervisor chip to expire. Have
	// seen reset present for 270ms from the release of MR. So wait 350ms.
	vTaskDelay( MS_to_TICKS(350) );

	// 2/8/2013 rmd : I2C bus now available.

	// 9/13/2013 rmd : NOTE - while these delays are taking place at least one maybe more of the
	// IRRI_TASK_CMD_update_output_states queue messages are accumulating. They must be
	// surpressed. That is the function of the irrigation_task_available variable.

	// ----------

	// 3/7/2013 rmd : Always on power up we scan for whats installed. So we can send to the
	// master after we hear from him.
	look_for_whats_installed();
	
	// 3/7/2013 rmd : Whether there is a change or not update the main board as part of a TP
	// Micro restart. By definition we've decided the first message to the main board will
	// contain the whats installed info.
	send_whats_installed_to_the_main_board = (true);

	// ----------

	// 3/25/2013 rmd : There is some redundancy in EXPLICITLY requesting the wind settings on
	// our bootup. Because whent he CS3000 main board gets notice that we just booted he takes
	// it upon himself to send the wind settings. But this will cause no harm. And it completes
	// the circle of case to be handled guaranteeing the two boards stay in sync.
	weather_info.need_wind_settings_from_main_board = (true);

	// ----------

	// 3/8/2013 rmd : Because the creation of the irrigation task (this task) was tied to seeing
	// at least one packet to us from the main board, we can start this timer now. We surely
	// should see messages coming along on a routine basis.
	xTimerStart( last_msg_from_main_timer, portMAX_DELAY );

	// ----------

	// 9/13/2013 rmd : And now allow the RIT to generate the 400ms turn ON staggering queue
	// messages.
	scs.state = SHORT_STATE_regular_irrigation;

	// 9/19/2013 rmd : And start the timer so we are attempting to process any incoming
	// irrigation requests.
	xTimerStart( irrigation_task_timer, portMAX_DELAY );

	// ----------

	while( (true) )
	{
		// 7/31/2015 rmd : The task is moving. Acquire latest time stamp for task watchdog scheme.
		*task_watchdog_time_stamp = xTaskGetTickCount();
		
		// ----------
		
		if( xQueueReceive( irrigation_task_queue, &itqs, portMAX_DELAY ) )
		{
			switch( itqs.command )
			{
				// ----------
	
				// 2/11/2013 rmd : Once every 10 seconds look for new cards.
				case IRRI_TASK_CMD_look_for_whats_installed:
	
					// 9/16/2013 rmd : Only when we are not executing the short hunt process. Don't try to use
					// the i2c bus while a short hunt is going on. Remember shorts drive the bus reset active.
					if( scs.state == SHORT_STATE_regular_irrigation )
					{
						look_for_whats_installed();
					}
					
					// 5/8/2012 raf :  Now we need to consider the implications of i2c bus errors.  What if
					// comms went down on a card when it's triacs were engaged?  It would continue to power
					// them, and we would be none the wiser.  Surely we need to reset the i2c bus if we suspect
					// i2c comm error.  So we could check what the state of that card was when it last sent a
					// response, and then hit the reset line only if we had stations on, but I think a better
					// idea for now is to just be safe and bring the whole irri system down.  Maybe we only do
					// this after 'x' number of i2c errors?  For now im going with 4, and it will be stored in
					// the 'input bits' area of the peripheral cards. We will increase by 2 here, and decrement
					// by 1 on message success.
					break;
					
				// ----------
	
				case IRRI_TASK_CMD_terminal_based_stations_from_main:
	
					// 9/28/2015 rmd : If we are not in the midst of the short process update who is to be
					// turned ON.
					if( scs.state == SHORT_STATE_regular_irrigation )
					{
						// 4/12/2013 rmd : As the desired output state arrives from the main board, set the desired
						// state.
						terminal_stations_to_be_turned_ON = itqs.terminal_stations_to_be_turned_ON;

						// ----------
						
						// 9/28/2015 rmd : So okay a new message from main has arrived. So indicate that to allow
						// processing when all terminal output queue messages have arrived.	After the short process
						// we want to wait until those to be turned ON has been update. This is so we don't clear
						// the error_short_hunt_exhausted_count OR error_short_with_nothing_ON_count variables.
						scs.station_q_message_arrived = (true);
					}
					break;
	
				// ----------
	
				case IRRI_TASK_CMD_terminal_based_mv_and_pmp_from_main:
	
					// 9/28/2015 rmd : See explanation for this IF statement at the equivalent STATIONS queue
					// message.
					if( scs.state == SHORT_STATE_regular_irrigation )
					{
						// 4/12/2013 rmd : As the desired output state arrives from the main board, set the desired
						// state.
						terminal_mv_to_be_turned_ON = itqs.terminal_mv_to_be_turned_ON;
	
						terminal_pump_to_be_turned_ON = itqs.terminal_pump_to_be_turned_ON;

						// ----------
						
						// 9/28/2015 rmd : See station q message arrived comment above for more explanation.
						scs.poc_mv_q_message_arrived = (true);
					}
	
					break;
	
				// ----------
				
				case IRRI_TASK_CMD_terminal_based_lights_from_main:
	
					// 9/28/2015 rmd : See explanation for this IF statement at the equivalent STATIONS queue
					// message.
					if( scs.state == SHORT_STATE_regular_irrigation )
					{
						// 4/12/2013 rmd : As the desired output state arrives from the main board, set the desired
						// state.
						terminal_lights_to_be_turned_ON = itqs.terminal_lights_to_be_turned_ON;

						// ----------
						
						// 9/28/2015 rmd : See station q message arrived comment above for more explanation.
						scs.lights_q_message_arrived = (true);
					}
					break;
	
				// ----------
	
				case IRRI_TASK_CMD_400ms_tick:
	
					// 3/6/2013 rmd : If still playing out the short process forget the normal turn ON
					// process.
					if( scs.state == SHORT_STATE_regular_irrigation && scs.station_q_message_arrived && scs.poc_mv_q_message_arrived && scs.lights_q_message_arrived )
					{
						// 9/23/2013 rmd : Turns out this is needed. Take the case of a shorted triac into a shorted
						// solenoid. In other words a short with nothing ON. The short detection task keeps hitting
						// the MR reset line and rapid fires the SHORT_DETECTED message at our irrigation task
						// queue (our queue here). Soon enough the queue fills up and we begin to skip over posting
						// those messages BUT the adc task still hits the MR line. So now we have the MR line driven
						// low without a corresponding SHORT queue message. This code here allows us to get back at
						// business of irrigating (given the short subsides).
						if( TPM_TRIAC_MR_IS_ACTIVE )
						{
							TPM_TRIAC_MR_PASSIVE;
							
							// 2/8/2013 rmd : Give time for the reset pulse from the supervisor chip to expire. Have
							// seen reset present for 270ms from the release of MR. So wait 350ms.
							vTaskDelay( MS_to_TICKS(350) );
						
							// 2/8/2013 rmd : I2C bus now available.
						}
						
						// ----------
						
						// 9/13/2013 rmd : FIRST, if asked to, go check the current to find the current delta due to
						// the last turned ON triac.
						if( go_measure_the_current_for_the_output_just_turned_ON )
						{
							// 9/27/2013 rmd : A NO_CURRENT can change the scs.state within this next function call. Be
							// aware of such.
							__record_current_draw_from_last_triac_turn_ON();
							
							number_of_measured_valve_currents += 1;
							
							go_measure_the_current_for_the_output_just_turned_ON = (false);
						}
		
						// ----------
						
						// 1/9/2013 rmd : This is the main irrigation action.  It finds the differences between the
						// triacs we have on, and the commanded triac state, and turns them on 1 triac at a time.
						// NOTE - if the prior output turned ON current measurement produced a NO_CURRENT then we
						// shouldn't proceed UNTIL the NO_CURRENT has been ACK by the main board. If we we're to
						// turn ON another NO_CURRENT we have potentially overwritten the record before it was sent
						// to the main board (there is only ONE such record). Additionally if being held at the main
						// board (think of it as the IRRI side) and before a token arrives so that we can build a
						// token response, we turn ON another station with a short or no_current we also would
						// overwrite the anomoly information waiting to be carried to the master by the token
						// response.
						
						// 10/2/2013 rmd : ADDITIONALLY if we are waiting to send the measured currents for those
						// just turned ON effectively we want to freeze the state of those ON until that information
						// has been sent to the main board. For example suppose we completed our turn ON
						// instructions and we flag to send to the main board. But before we send those measured
						// currents we get new instructions that causes us to in fact turn ON an additional output.
						// SO we turn it ON, setting the record of those ON, but before we can perform the current
						// measurement (400ms after the turn ON) we send the currents. In that scenario we would
						// have sent the previously measured current for the output just turned ON.
						if( !send_measured_currents_for_those_ON_to_main )
						{
							// 9/25/2015 rmd : Perform some housekeeping on two SHORT tracking variables. If no outputs
							// are to be ON we should clear these variables so their counts don't carry over from
							// "output session" to "output session". Meaning if you run an output and get multiple
							// shorts that produce an unsuccesful hunt we should clear that 3 so that the next output to
							// turn ON doesn't inherit that count - as it shouldn't.
							if( (terminal_stations_to_be_turned_ON == 0) && !terminal_mv_to_be_turned_ON && !terminal_pump_to_be_turned_ON && (terminal_lights_to_be_turned_ON == 0) )
							{
								scs.error_short_hunt_exhausted_count = 0;
							}
							else
							{
								// 9/25/2015 rmd : This is the converse of the hunt exhausted count. And should be cleared
								// when we are set to turn something ON.
								scs.error_short_with_nothing_ON_count = 0;
							}
							
							// ----------
							
							update_all_i2c_cards( &go_measure_the_current_for_the_output_just_turned_ON );
							
							// 10/2/2013 rmd : If we did not turn ON any new valves during that last call (turn OFF's do
							// not count) then if we had been turning ON valves go ahead and send the results to the
							// main board.
							if( !go_measure_the_current_for_the_output_just_turned_ON && (number_of_measured_valve_currents > 0) )
							{
								send_measured_currents_for_those_ON_to_main = (true);
								
								number_of_measured_valve_currents = 0;
							}
						}
		
						// ----------
	
						/*
						char		str_32[ 32 ];
		
						// 9/23/2013 rmd : For debug this is very convenient. Shows the real-time current in the tp
						// micro alerts at a 2.5 hertz rate.
						snprintf( str_32, sizeof(str_32), "%u mA", adc_readings.field_current_short_avg_ma );
	
						alert_message_to_tpmicro_pile( str_32 );
						*/
						
						// ----------
					}
					else
					if( scs.state == SHORT_STATE_hunting )
					{
						// 9/18/2013 rmd : During the hunt, if we try the station with the short, the short occurs
						// and we end the hunt. However the hunt timer will fire one more time. When we see the
						// short and end the hunt we could stop the timer, in addition to this block, but I think
						// I'll just let the timer run down and fire. Doesn't hurt anything.
	
						// 3/7/2013 rmd : Always turn OFF all the outputs upon entry to this case. For each station
						// we hunt through we turn it ON within this case. If it causes a short we pass through the
						// short detected case above. If not the hunt timer expires and we arrive here. So it makes
						// sense in either case to turn off all outputs first.
						turn_off_all_cards();
		
						// ----------
						
						// 9/17/2013 rmd : Clear the output being tested.
						scs.station_being_tested_1 = 0;
						scs.light_being_tested_1 = 0;
						scs.mv_being_tested = (false);
						scs.pump_being_tested = (false);
						
						// ----------
		
						// 9/17/2013 rmd : If there are no more to test it is an error. We should have seen the
						// short by now!
						if( !scs.mv_test && !scs.pump_test && (scs.stations_to_test == 0) && (scs.lights_to_test == 0) )
						{
							// 9/24/2013 rmd : MUST limit how many times we allow this result. The reason? If the short
							// is the result of the cumulative current of the outputs involved but any single output
							// current isn't enough to trip the short protection THEN this process would be repeated
							// over and over again during the whole duration the outputs were ON. So we send a message
							// to the master. And when he sees this type of error removes the offending group from the
							// list. We could be more elegant and figure a way to lower the on-at-a-time for each of the
							// stations involved and then switch them all off, and let the foal_irri machine start
							// again.
	
							// 9/24/2013 rmd : TP Micro alert line to show occurance.
							alert_message_to_tpmicro_pile( "24VAC SHORT hunt exhausted - short not found!" );
	
							if( scs.error_short_hunt_exhausted_count < (SHORT_ERROR_LIMIT - 1) )
							{
								scs.error_short_hunt_exhausted_count += 1;
	
								// 9/28/2015 rmd : Move towards returning to regular irrigation.
								scs.state = SHORT_STATE_regular_irrigation;
							}
							else
							{
								// 9/24/2013 rmd : Get this report sent to the master. No turn ON's till completed without
								// an acknowledge. Hmmm. What will the master do? Lower the stations on-at-a-time in order
								// to reduce the current? Lower this boxes electrical limit? What if there is only a MV and
								// a PUMP on? Maybe the easiest thing to do is, if there is more than one station ON, flag
								// them to irrigate one at a time. If one station is ON to flag it as suspected hi current.
								// If no stations ON at this box to cancel irrigation.
								scs.short_or_no_current_report.result = SHORT_OR_NO_CURRENT_RESULT_short_but_hunt_was_unsuccessful;
								
								scs.state = SHORT_STATE_send_short_or_no_current_to_main;
							}
							
							// 9/17/2013 rmd : Calling this exit the short hunt function has the effect to start over
							// any ongoing normal turn ON sequence. The next message from the main board will recommence
							// the turn ON via the q_message_arrived variables.
							exit_the_short_hunt( &number_of_measured_valve_currents, &go_measure_the_current_for_the_output_just_turned_ON );
						}
						else
						if( scs.mv_test )
						{
							scs.mv_being_tested = (true);
							
							// 9/18/2013 rmd : And clear the need to test this output the next pass through.
							scs.mv_test = (false);
		
							// 3/6/2013 rmd : Turn ON the MV.
							update_i2c_poc_card( (false), (true) );
	
							alert_message_to_tpmicro_pile( "testing MV" );
						}
						else
						if( scs.pump_test )
						{
							scs.pump_being_tested = (true);
		
							// 9/18/2013 rmd : And clear the need to test this output the next pass through.
							scs.pump_test = (false);
		
							// 3/6/2013 rmd : Turn ON the PUMP.
							update_i2c_poc_card( (true), (false) );
	
							alert_message_to_tpmicro_pile( "testing PUMP" );
						}
						else
						if( scs.stations_to_test != 0 )
						{
							for( iii=0; iii<MAX_CONVENTIONAL_STATIONS_PER_BOX; iii++ )
							{
								if( B_IS_SET_64( scs.stations_to_test, iii ) )
								{
									// 9/18/2013 rmd : NOTE - 1 based output being tested. So we can use the 0 value as not
									// being tested!
									scs.station_being_tested_1 = (iii + 1);
									
									// 9/18/2013 rmd : And clear the need to test this output the next pass through.
									B_UNSET_64( scs.stations_to_test, iii );
		
									break;
								}
							}
		
							// ----------
							
							// 9/18/2013 rmd : Turn ON the selected output. No returned result error checking needed
							// here. And start the timer.
							update_i2c_station_triac_outputs( iii/8, (1 << (iii % 8)) );
	
	
							char	char_str_32[ 32 ];
							snprintf( char_str_32, 32, "Testing station %d", scs.station_being_tested_1 );
							alert_message_to_tpmicro_pile( char_str_32 );
	
						}
						else
						if( scs.lights_to_test != 0 )
						{
							for( iii=0; iii<MAX_LIGHTS_OUTPUTS_PER_BOX; iii++ )
							{
								if( B_IS_SET( scs.lights_to_test, iii ) )
								{
									// 9/18/2013 rmd : NOTE - 1 based output being tested. So we can use the 0 value as not
									// being tested!
									scs.light_being_tested_1 = (iii + 1);
									
									// 9/18/2013 rmd : And clear the need to test this output the next pass through.
									B_UNSET( scs.lights_to_test, iii );
									
									break;
								}
							}
		
							// 9/18/2013 rmd : Turn ON the selected output. No returned result error checking needed
							// here. And start the timer.
							update_i2c_lights_triac_outputs( (1 << iii) );
						}
						
						// ----------
	
					}  // of if we are hunting
					
					// ----------
					
					// 9/19/2013 rmd : In any case we always restart the timer.
					xTimerStart( irrigation_task_timer, portMAX_DELAY );
					
					// ----------
	
					break;
	
				// ----------
	
				case IRRI_TASK_CMD_short_detected:
	
					xTimerStop( irrigation_task_timer, portMAX_DELAY );
	
					if( scs.state == SHORT_STATE_hunting )
					{
						// 9/17/2013 rmd : If we are in the middle of the hunt and a short occurs we've found the
						// output that caused the short. At least we've found one output. But we will stop the hunt
						// on this one, flag the main baord so he'll remove from his irrigation lists, and resume
						// irrigation after the main board acknowledges this short.
	
						// 9/17/2013 rmd : A sanity check variable. So we can make sure state machine is acting
						// properly. An output is surely supposed to be ON!
						found_one = (false);
	
						if( scs.mv_being_tested )
						{
							found_one = (true);
	
							// 9/16/2013 rmd : Indicate it was the MV.
							scs.short_or_no_current_report.terminal_type = OUTPUT_TYPE_TERMINAL_MASTER_VALVE;
							
							alert_electrical_short_to_main( TPALERT_a_short_happened_on_this_master_valve, 1 );
						}
						else
						if( scs.pump_being_tested )
						{
							found_one = (true);
	
							// 9/16/2013 rmd : Indicate it was the PUMP.
							scs.short_or_no_current_report.terminal_type = OUTPUT_TYPE_TERMINAL_PUMP;
							
							alert_electrical_short_to_main( TPALERT_a_short_happened_on_this_pump, 1 );
						}
						else
						if( scs.station_being_tested_1 !=0 )
						{
							found_one = (true);
	
							// 9/16/2013 rmd : Indicate it was a STATION.
							scs.short_or_no_current_report.terminal_type = OUTPUT_TYPE_TERMINAL_STATION;
							
							scs.short_or_no_current_report.station_or_light_number_0 = (scs.station_being_tested_1 - 1);
	
							alert_electrical_short_to_main( TPALERT_short_on_this_station, scs.station_being_tested_1 );
						}
						else
						if( scs.light_being_tested_1 !=0 )
						{
							found_one = (true);
	
							// 9/16/2013 rmd : Indicate it was a LIGHTS OUTPUT.
							scs.short_or_no_current_report.terminal_type = OUTPUT_TYPE_TERMINAL_LIGHT;
	
							scs.short_or_no_current_report.station_or_light_number_0 = (scs.light_being_tested_1 - 1);
	
							alert_electrical_short_to_main( TPALERT_a_short_happened_on_this_light, scs.light_being_tested_1 );
						}
						
						// ----------
						
						if( found_one )
						{
							// 9/24/2013 rmd : Complete the report record by filling in the result type.
							scs.short_or_no_current_report.result = SHORT_OR_NO_CURRENT_RESULT_short_and_found;
							
							// 9/24/2013 rmd : Since we've had a successful hunt 0 the hunt exhausted error counter.
							scs.error_short_hunt_exhausted_count = 0;
	
							// 9/17/2013 rmd : Clean up the control variables.
							exit_the_short_hunt( &number_of_measured_valve_currents, &go_measure_the_current_for_the_output_just_turned_ON );
							
							// 9/24/2013 rmd : Get this report sent to the master. No turn ON's till completed with an
							// acknowledge.
							scs.state = SHORT_STATE_send_short_or_no_current_to_main;
						}
						else
						{
							// 9/23/2013 rmd : THIS is really an error and should never happen. We are in the midst of
							// the short hunting process and we've suffered a new short BUT no outputs are shown as ON.
							// Some kind of breakdown of the logic! Issue an alert line and return to normal irrigation.
							alert_message_to_tpmicro_pile( "24VAC SHORT during hunt - no outputs ON!" );
							
							// 9/17/2013 rmd : Clean up the control variables.
							exit_the_short_hunt( &number_of_measured_valve_currents, &go_measure_the_current_for_the_output_just_turned_ON );
	
							scs.state = SHORT_STATE_regular_irrigation;
						}
	
					}
					else
					if( scs.state == SHORT_STATE_regular_irrigation )
					{
						// ----------
	
						// 3/7/2013 rmd : Initialize the short hunting sequence.
						
						// ----------
	
						// 9/16/2013 rmd : Working with an UNS_64 which is shared between the comm_mngr task and the
						// irrigation_manager task (terminal_stations_that_are_ON) so need to use MUTEX.
						xSemaphoreTake( g_xIrrigationMutex, portMAX_DELAY );
	
						// 9/17/2013 rmd : If none are ON then there will be no hunting. Limit the number of error
						// messages. (MUTEX needed)
						if( !terminal_mv_is_ON && !terminal_pump_is_ON && (terminal_stations_that_are_ON == 0) && (terminal_lights_that_are_ON == 0) )
						{
							// 9/17/2013 rmd : Limit how many error messages we make. If this happened over and over
							// again would flood memory pool with requests. And flood alerts in main board.
							if( scs.error_short_with_nothing_ON_count < SHORT_ERROR_LIMIT )
							{
								scs.error_short_with_nothing_ON_count += 1;
							
								// 9/17/2013 rmd : We are not going to tell the main board about this with the short and ack
								// sequence. Issue an alert line though.
								alert_message_to_tpmicro_pile( "24VAC SHORT detected with nothing ON!" );
							}
	
							// 9/24/2013 rmd : Well HELL. There isn't really alot we can do about this. Apparently we
							// are getting a short though supposedly NOTHING is turned ON. Since we got the short the MR
							// line has been hit. Forcing OFF all outputs. All we can do is clean up our control
							// variables and continue on. We'll see if this ever becomes an issue.
							exit_the_short_hunt( &number_of_measured_valve_currents, &go_measure_the_current_for_the_output_just_turned_ON );
						}
						else
						{
							// ----------
		
							// 10/2/2013 rmd : (MUTEX needed)
							scs.mv_test = terminal_mv_is_ON;
							scs.pump_test = terminal_pump_is_ON;
							scs.stations_to_test = terminal_stations_that_are_ON;
							scs.lights_to_test = terminal_lights_that_are_ON;
		
							// ----------
		
							scs.state = SHORT_STATE_hunting;
							
							// ----------
							
						}
		
						xSemaphoreGive( g_xIrrigationMutex );
		
					}
					
					// ----------
	
					// 3/6/2013 rmd : Detection of a short activated the bus reset line. Clearing all i2c triac
					// cards and turning off all stations. Before going any further we release this state
					// allowing stations to once again be turned ON.
					TPM_TRIAC_MR_PASSIVE;
				
					// 2/8/2013 rmd : Give time for the reset pulse from the supervisor chip to expire. Have
					// seen reset present for 270ms from the release of MR. So wait 350ms.
					vTaskDelay( MS_to_TICKS(350) );
					
					// 2/8/2013 rmd : I2C bus now available.
				
					// ----------
	
					// 9/19/2013 rmd : Restart the timer. It was already running because that is how we just
					// turned a station ON. Then we saw the short. Probably rather quickly after turning ON. But
					// could be at any time while ON if someone manually made the short. Hmmm. Should we purge
					// the queue. Suppose after the short was detected the 400ms timer fired. There would
					// already be one of those commands on the queue. I think that possiblity just confuses the
					// world. I've decided EMPTY the queue. Really focused at getting rid of the 400ms timer CMD
					// in case that timer expired between the short and the short processing here within this
					// irrigation task. If it did we would pop on the first station to test without the 400ms
					// delay. Not that that is known to be bad. But I'd like each hunt to proceed from short
					// detection with the same timing each time.
					while( xQueueReceive( irrigation_task_queue, &itqs, 0 ) );
					
					xTimerStart( irrigation_task_timer, portMAX_DELAY );
					
					// ----------
	
					break;
	
	
			}  // of switch
	
		}  // of event rcvd
		
	}  // of task
	
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

