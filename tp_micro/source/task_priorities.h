/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_TASK_PRIORITIES_H
#define _INC_TASK_PRIORITIES_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/20/2013 rmd : This is a Calsense file used to gather all the task priorities together
// in one place. So there is no mistaking them.



// 9/20/2013 rmd : To start we know the IDLE task priority is 0. This define is not actually
// used but is here for reference.

#define		PRIORITY_IDLE_TASK					(0)

#define		PRIORITY_WDT_TASK_1					(1)

#define		PRIORITY_STARTUP_TASK_2				(2)

#define		PRIORITY_SERIAL_DRIVER_TASK_3		(3)

#define		PRIORITY_RING_BUFFER_SCAN_TASK_4	(4)

#define		PRIORITY_COMM_MNGR_TASK_5			(5)

#define		PRIORITY_IRRI_TASK_6				(6)

#define		PRIORITY_2WIRE_TASK_7				(7)

// 2/12/2013 rmd : With the exceptions of the TIMER task the ADC task is the highest
// priority task in the system. So that the A-D conversions are handled immediately ahead of
// any other task activity. Have seen this task use 636 bytes of stack space! Hence the 1000
// byte stack.
#define		PRIORITY_ADC_TASK_8					(8)

#define		PRIORITY_TIMER_TASK_9				(9)


// 9/20/2013 rmd : May set task priority value up to and including this value.
#define		PRIORITY_MAX_ALLOWED				(11)




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

