/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"wdt_and_powerfail.h"

#include	"tpmicro_comm_mngr.h"

#include	"intrinsics.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// 2/21/2014 rmd : To identify the event. Which is the reason for the shutdown and eventual
	// restart.
	UNS_32	event;
	
} SYSTEM_SHUTDOWN_EVENT_QUEUE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


RESTART_INFORMATION_STRUCT	restart_info __attribute__((section (".non_init")));


const char RESTART_INFO_VERIFY_STRING_PRE[] = "TPMICRO RESTART";

const char RESTART_INFO_VERIFY_STRING_POST[] = "OH A PWR FAILURE!";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void init_restart_info( void )
{
	char	str_64[ 64 ];
	
	// ----------
	
	// 7/24/2015 rmd : Is the string intact.
	if( (strncmp( restart_info.verify_string_pre, RESTART_INFO_VERIFY_STRING_PRE, strlen( RESTART_INFO_VERIFY_STRING_PRE ) ) != 0) ||
		(strncmp( restart_info.verify_string_post, RESTART_INFO_VERIFY_STRING_POST, strlen( RESTART_INFO_VERIFY_STRING_POST ) ) != 0)
	  )
	{
		alert_message_to_tpmicro_pile( "Restart_Info formatted" );

		// ----------

		// 9/25/2012 rmd : The following memset does indeed (of course) set all the UNS_32 numbers
		// to 0. And all _Bools to (false).
		memset( &restart_info, 0x00, sizeof(restart_info) );

		// 7/30/2015 rmd : And write the pre and post strings at the end of this function.
	}
	else
	{
		alert_message_to_tpmicro_pile( "Restart_Info valid" );

		// ----------

		if( restart_info.starvation_noted )
		{
			alert_message_to_tpmicro_pile( restart_info.starvation_string );
		}

		// ----------
		
		if( restart_info.exception_noted )
		{
			alert_message_to_tpmicro_pile( restart_info.exception_string );
		}

		// ----------

		if( restart_info.assert_noted )
		{
			alert_message_to_tpmicro_pile( restart_info.assert_string );
		}
		
	}
	
	// ----------
	
	snprintf( str_64, sizeof(str_64), "TPMicro Restart (" );
	
	// 7/29/2015 rmd : Need to wait to analyze the startup reason till we can send the alert
	// message to the main board.
	if( B_IS_SET( reset_source_reg, RSID_POR_BIT ) )
	{
		snprintf( str_64, sizeof(str_64), "%sPOWER FAIL)", str_64 );

		alert_message_to_tpmicro_pile( str_64 );
	}
	else
	if( B_IS_SET( reset_source_reg, RSID_BODR_BIT ) && !B_IS_SET( reset_source_reg, RSID_POR_BIT ) )
	{
		snprintf( str_64, sizeof(str_64), "%sBROWN OUT 2)", str_64 );

		alert_message_to_tpmicro_pile( str_64 );
	}
	else
	if( restart_info.brown_out_interrupt_occured && B_IS_SET( reset_source_reg, RSID_WDTR_BIT ) )
	{
		snprintf( str_64, sizeof(str_64), "%sBROWN OUT 1)", str_64 );

		alert_message_to_tpmicro_pile( str_64 );
	}
	else
	if( B_IS_SET( reset_source_reg, RSID_EXTR_BIT ) )
	{
		// 7/30/2015 rmd : This should never happen cause there is nothing driving the reset input
		// to the LPC1763. Well the JTAG does so sometimes we see this with the debugger attached.
		// But release code in the field should NEVER see this.
		snprintf( str_64, sizeof(str_64), "%sRESET INPUT!!!)", str_64 );

		alert_message_to_tpmicro_pile( str_64 );
	}
	else
	if( B_IS_SET( reset_source_reg, RSID_WDTR_BIT ) )
	{
		snprintf( str_64, sizeof(str_64), "%sWATCH_DOG TIMEOUT)", str_64 );
		
		alert_message_to_tpmicro_pile( str_64 );
	}

	// ----------
	
	// 7/29/2015 rmd : After processing always wipe the entire structure to get ready for the
	// next restart. Remember the restart structure is actually only used for exceptions and
	// some brown-outs. Most usual restarts (full power cycle) do not rely on any info in this
	// structure.
	memset( &restart_info, 0x00, sizeof(restart_info) );

	snprintf( restart_info.verify_string_pre, sizeof( restart_info.verify_string_pre ), "%s", RESTART_INFO_VERIFY_STRING_PRE );

	snprintf( restart_info.verify_string_post, sizeof( restart_info.verify_string_post ), "%s", RESTART_INFO_VERIFY_STRING_POST );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef DEBUG

	BOOL_32		alert_main_of_an_watchdog_timeout;
	
	void WDT_IRQHandler( void )
	{
		#ifdef WATCHDOG_TIMEOUT_FALLS_INTO_WHILE_FOREVER
		
			// 7/29/2015 rmd : ISR for watchdog timer. ONLY USED WHEN BUILT FOR DEBUG. In the RELEASE
			// build when the watchdog times out it goes directly to a hard chip reset. No
			// interrupt processing takes place.
			portENTER_CRITICAL();
		
			while(1);
			
		#else
		
			alert_main_of_an_watchdog_timeout = (true);
			
			// 1/27/2016 rmd : NOTE - once the watchdog interrupt occurs there is no way to clear it.
			// Meaning the interrupt will occur indefinately unless we disable the interrupt in the
			// NVIC.
			NVIC_DisableIRQ( WDT_IRQn );
			
		#endif
	}

#endif

/* ---------------------------------------------------------- */
void BOD_IRQHandler( void )
{
	// 7/29/2015 rmd : Disable interrupts. Turn off the two wire FETs (precautionary move). Then
	// reload the watchdog for the minimum time. And wait for watchdog timeout. There is NO EXIT
	// from this ISR. The watchdog hard reset does it. Remember in the release version there is
	// no watchdog isr - only a hard reset.

	// ----------
	
	// 7/29/2015 rmd : For the alert line upon restart.
	restart_info.brown_out_interrupt_occured = (true);
	
	// ----------
	
	// 7/29/2015 rmd : Using the built-in disable ALL priority configurable interrupts. This
	// disables all interrupts except for the NMI, Reset, and Hard Fault interrupts.
	__disable_interrupt();
	
	// ----------

	// 7/29/2015 rmd : Precautionary TWO-WIRE cable power down using brute force. After we
	// killed all interrupts we can go ahead and directly force this.
	TPM_2W_NEG_OFF;
	
	TPM_2W_POS_OFF;
	
	// ----------

	// 7/29/2015 rmd : Load a 300ms time out in the watchdog timer. Came up with this as this
	// would be plenty for a normal powerfail. Once the 3.3V on the TPMicro begins its decay
	// it's all over and done with in less than 100ms. So if the watchdog kicks in here it means
	// a type of brown out where the voltage never got down below the BOD Reset level. This is a
	// difficult phenomenon to duplicate and watch on a scope. But I have managed to see it a
	// few times.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDTC = 1350000;

	// 7/29/2015 rmd : Reload the watchdog timer with the new 300ms count. This is actually a
	// problematic step in that this brown out isr could have interrupted the kick_wdt function.
	// It's really not that bad however as if we broke the feed sequence the WDT responds with
	// an immediate hard reset. And that's where we are headed anyway.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0xAA;
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0x55;

	// ----------

	while(1);
}

/* ---------------------------------------------------------- */
void enable_brown_out_interrupt( void )
{
	// 7/29/2015 rmd : The brown out interrupt is only useful if the watchdog is running. The
	// intention is that when a power failure begins the brown out interrupt always fires. And
	// the brown out ISR will begin a shutdown by letting the watchdog expire.

	// ----------
	
	// 7/29/2015 rmd : Note the brown out detection circuitry is automatically enabled from a
	// hardware reset. Though for clarity sakes and to ensure we will enable here.
	PCON_BODRPM_BITBAND = 0;

	PCON_BOGD_BITBAND = 0;

	PCON_BORD_BITBAND = 0;
	
	// ----------
	
	NVIC_SetPriority( BOD_IRQn, BROWN_OUT_INT_PRIORITY );

	NVIC_EnableIRQ( BOD_IRQn );
}

/* ---------------------------------------------------------- */
void init_watchdog_hardware( void )
{
	// 7/28/2015 rmd : Start the WDT. The Watchdog CANNOT be stopped once it has been started.
	// Only a WD timeout provides the ability to stop the watchdog. Or a hardware reset.

	// 7/29/2015 rmd : The WDT peripheral is always powered. Don't need to enable its power.

	CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_WDT, CLKPWR_PCLKSEL_CCLK_DIV_4 );

	// ----------
	
	// 2/11/2014 rmd : Choose PCLK as the clock source.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDCLKSEL = 0x01;
	
	// 7/29/2015 rmd : And lock the clock source from further software changes.
	WDCLKSEL_WDLOCK_BITBAND = 1;
	
	// ----------
	
	// 2/11/2014 rmd : Load the timeout value. What we have is CCLK / 4 as the WDT peripheral
	// source (divided by 4 with the preceeding clk sel call). Then divided by 4 again by the
	// fixed WDT logic itself. So that would be 72MHz / 16 = 4.5MHz. So if we want a 10 second
	// timeout we load the counter with 45000000.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDTC = 45000000;

	// ----------
	
	// 7/29/2015 rmd : When in debug mode we only want to produce an interrupt when the WD
	// timeout occurs. Disable the hard reset else you lose the debugger upon the reset. In
	// release code we for-go the interrupt and go straight to a hard reset when the watchdog
	// times out.
	
	#ifdef DEBUG
	
		// 7/29/2015 rmd : Enable WDT.
		WDMOD_WDEN_BITBAND = 1;

		// 7/29/2015 rmd : With no hard reset.
		WDMOD_WDRESET_BITBAND = 0;

		// ----------
		
		NVIC_SetPriority( WDT_IRQn, WDT_INT_PRIORITY );
	
		NVIC_EnableIRQ( WDT_IRQn );
		
	#else

		// 7/29/2015 rmd : Enable WDT.
		WDMOD_WDEN_BITBAND = 1;

		// 7/29/2015 rmd : With a hard reset.
		WDMOD_WDRESET_BITBAND = 1;
		
	#endif
	
	// 7/29/2015 rmd : Perform the feed to latch in changes.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0xAA;
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0x55;
}

/* ---------------------------------------------------------- */
void WDT_kick_the_dog( void )
{
	// 7/29/2015 rmd : Feed the feed sequence to restart the watchdog timer.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0xAA;
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0x55;
}

/* ---------------------------------------------------------- */

// 10/23/2012 rmd : During debug you should always implement this function. Assert is your
// friend! And beyond that i.e. release code it is also reasonable to continue the use of
// assert. As long as it is not a nuisance.
//
// 7/30/2015 rmd : THERE IS A MAJOR CONCERN. If there is an ASSERT condition early in the
// boot process and it is repeated with each boot ... if the result of ASSERT is a reset via
// the watchdog then there isn't enough code run time to recieve and process messages from
// the main board to enter ISP for a code update. This effectively results in a stalemate
// situation. The TPMicro code is broken and cannot be updated. Not sure what the best
// course of action is. After some thought probably best that this assert function does not
// cause a restart but that the code continues on. And we attempt to write the alert lines
// within this function instead of in the init_restart_info function. And after even more
// thought had decided to also write an alert on the restart if any ASSERT occurred since
// the last reset. If multiple assert have occured they will overwrite each other of course.
void __assert( const char *__expression, const char *__filename, int __line )
{
	restart_info.assert_noted = (true);
	
	snprintf( restart_info.assert_string, sizeof(restart_info.assert_string), "ASSERT: %s, %s, %d (%s)", __expression,  CS_FILE_NAME( (char*)__filename ),  __line, pcTaskGetTaskName(NULL) );

	alert_message_to_tpmicro_pile( restart_info.assert_string );

	// ----------

	// 7/30/2015 rmd : See note just above this function why we commented the restart out.
	/*
	// 7/30/2015 rmd : Force an immediate watchdog hardware reset by botching the feed sequence.
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0xAA;
	((LPC_WDT_TypeDef*)LPC_WDT_BASE)->WDFEED = 0xAA;
	
	while( 1 );  // not needed but ...
	*/
}

/* ---------------------------------------------------------- */
extern void wdt_monitoring_task( void *pvParameters )
{
	static UNS_32	last_x_stamp, greatest_diff_ms;

	UNS_32		i;

	UNS_32		now_stamp, task_time_stamp;

	BOOL_32		care_for_the_dog;

	char		str_32[ 32 ];
	
	// ----------

	last_x_stamp  = xTaskGetTickCount();
	
	greatest_diff_ms = 0;
	
	// ----------
	
	while( (true) )
	{
		// 2/21/2014 rmd : This task has a priority of 1. And the only task lower is the IDLE task.
	
		// ----------
	
		vTaskDelay( MS_to_TICKS( 500 ) );
	
		// ----------
	
		// 3/12/2013 rmd : The tick count rate in this application is 1kHz. Which means it will roll
		// over each 48 some days. Unless the code is restarted within that period.
		now_stamp = xTaskGetTickCount();
	
		// ----------
	
		// 4/10/2013 rmd : In the case of roll-over lets not create an alarming alert line. So test
		// the stamp relative values make sense first.
		if( now_stamp > last_x_stamp )
		{
			if( (now_stamp - last_x_stamp) > MS_to_TICKS( greatest_diff_ms ) )
			{
				greatest_diff_ms = ((now_stamp - last_x_stamp) * (portTICK_RATE_MS) );
	
				snprintf( str_32, sizeof(str_32), "New Monitoring Delta: %u ms", greatest_diff_ms );

				alert_message_to_tpmicro_pile( str_32 );
			}
		}
		
		last_x_stamp = now_stamp;
		
		// ----------
	
		
		care_for_the_dog = (true);
	
		// ----------
	
		// 10/24/2012 rmd : Here is an interesting dilemma. This wdt task is the lowest priority
		// task. So when we grab the present tick count (x_stamp) it is quite possible another task
		// goes ready and executes itself till blocking (say a comm_mngr message comes in). Well
		// then that task has updated its task_last_execution_stamp. And it may be a newer number
		// (ie greater than our x_stamp value). Well that sure is assurance that the task has
		// executed. And we don't need to check that task. FURTHER more if we did check that task
		// the math all being unsigned would fail. For example 1211-1218 is a VERY big number. So we
		// MUST avoid even performing that math. How do I know all this? I HAVE SEEN IT HAPPEN!
		//
		// 10/25/2012 rmd : Further more what we have two options here. Suspend the scheduler so
		// that between "the test if the task_last_execution_stamp is less than our x_stamp" AND
		// "the actual delta comparison" task_least_execution_stamp cannot change. OR take a copy
		// and work with the copy. Taking a copy is an atomic operation so is safe. And to avoid
		// suspending the scheduler I've decided to take a copy of the tasks last x stamp and work
		// with that.
		
		// ----------

		// 7/31/2015 rmd : We check the tasks in the table up to but not including this task. WHY?
		// Because if this task isn't running indeed the WDT will time out. And what good does it do
		// to check ourselves. I mean if we aren't running how do we do that?
		for( i=0; i<(NUMBER_OF_TABLE_TASKS-1); i++ )
		{
			// 7/31/2015 rmd : Did we make the task?
			if( task_table[ i ].bCreateTask )
			{
				// 7/31/2015 rmd : Grab a copy so between our >= test and the delta calculation math if the
				// task runs and updates its time stamp we don't get bad math.
				task_time_stamp = task_last_execution_stamp[ i ];
				
				// 3/12/2013 rmd : Watch for the tick count roll-over (it happens every 248 some days) or
				// the phenomenon I mentioned above where the task runs between the capturing of the
				// now_stamp and this test here. Which does happen.
				if( now_stamp >= task_time_stamp )
				{
					// 7/31/2015 rmd : We just use a blanket 5 second limit. All tasks have to update their time
					// stamp at least in that time frame else the system is in error.
					if( (now_stamp - task_time_stamp) > MS_to_TICKS( 5000 ) )
					{
						// 7/31/2015 rmd : This alert may or may not make it to the main board depending on which
						// task is starved.
						snprintf( restart_info.starvation_string, sizeof(restart_info.starvation_string), "STARVED: %s, %ums", task_table[ i ].task_name, ((now_stamp - task_time_stamp) * portTICK_RATE_MS) );
						
						restart_info.starvation_noted = (true);
						
						alert_message_to_tpmicro_pile( restart_info.starvation_string );
						
						// ----------
						
						// 10/23/2012 rmd : Let the watchdog expire.
						care_for_the_dog = (false);
					}
	
				}
				else
				{
					// 3/12/2013 rmd : If the now_stamp is LESS THAN the task time stamp the OS tick count
					// MUST HAVE ROLLED over. This will happen once every 248 days. In this case just start the
					// task stamps over setting them equal to the present tick count. Missing this check once
					// each roll-over is okay. If the task is being starved that will show soon enough.
					//
					// Normally the task_last_execution_stamp is 'stamped' within the respective task itself
					// when the task unblocks. In this case we are reinitializing the last_execution_stamps.
					//
					// 2/21/2014 rmd : EVEN if we ended up here for the other phenomenon where the task ran
					// between taking the now_stamp and the mathematical test resetting the task time stamp like
					// this is just fine.
					task_last_execution_stamp[ i ] = now_stamp;
				}
				
			}

		}
		
		// ----------

		// 3/12/2013 rmd : And now cover the startup task which is not in the table.
		task_time_stamp = startup_task_last_execution_stamp;
		
		// 3/12/2013 rmd : Watch for the tick count roll-over. It happens every 248 some days.
		if( now_stamp >= task_time_stamp )
		{
			if( (now_stamp - task_time_stamp) > MS_to_TICKS( 5000 ) )
			{
				// 7/31/2015 rmd : This alert may or may not make it to the main board depending on which
				// task is starved.
				snprintf( restart_info.starvation_string, sizeof(restart_info.starvation_string), "STARVED: Startup, %ums", ((now_stamp - task_time_stamp) * portTICK_RATE_MS) );
				
				restart_info.starvation_noted = (true);
						
				alert_message_to_tpmicro_pile( restart_info.starvation_string );
				
				// ----------
				
				// 10/23/2012 rmd : Let the watchdog expire.
				care_for_the_dog = (false);
			}
		}
		else
		{
			startup_task_last_execution_stamp = now_stamp;
		}
		
		// ----------
	
		// 7/31/2015 rmd : And now for the whole point of all this.
		if( care_for_the_dog )
		{
			WDT_kick_the_dog();
		}
	
	}  // of the task itself
	
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
void pop_registers_from_fault_stack(unsigned int * hardfault_args)
{
unsigned int stacked_r0;
unsigned int stacked_r1;
unsigned int stacked_r2;
unsigned int stacked_r3;
unsigned int stacked_r12;
unsigned int stacked_lr;
unsigned int stacked_pc;
unsigned int stacked_psr;

	stacked_r0 = ((unsigned long) hardfault_args[0]);
	stacked_r1 = ((unsigned long) hardfault_args[1]);
	stacked_r2 = ((unsigned long) hardfault_args[2]);
	stacked_r3 = ((unsigned long) hardfault_args[3]);

	stacked_r12 = ((unsigned long) hardfault_args[4]);
	stacked_lr = ((unsigned long) hardfault_args[5]);
	stacked_pc = ((unsigned long) hardfault_args[6]);
	stacked_psr = ((unsigned long) hardfault_args[7]);

    // Inspect stacked_pc to locate the offending instruction.
	for( ;; );
}
*/

/* ---------------------------------------------------------- */
// This code just sets up everything ready, then jumps to a function called
// pop_registers_from_fault_stack - which is defined below.
/*
extern void data_abort_handler( void ) __attribute__ ((naked));
extern void data_abort_handler( void )
{
	__asm volatile
	(
		" tst lr, #4										\n"
		" ite eq											\n"
		" mrseq r0, msp										\n"
		" mrsne r0, psp										\n"
		" ldr r1, [r0, #24]									\n"
		" ldr r2, handler2_address_const					\n"
		" bx r2												\n"
		" handler2_address_const: .word pop_registers_from_fault_stack	\n"
	);
}
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if 0

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void undefined_instruction_handler(void) __attribute__ ((naked));
void undefined_instruction_handler(void)
{
	register UNS_32		*lnk_ptr;

	__asm__ __volatile__
	(
		"sub lr, lr, #8\n"
		"mov %0, lr" : "=r" (lnk_ptr)
	);
	
	// ----------

	// 10/23/2012 rmd : Though we could probably just go ahead and use the regular obtain time &
	// date function. And we could go ahead and write an alert line here. We really do not know
	// where in the code this exception occurred. For example if we were writing to the alert
	// pile and an exception occurred (even while in the middle of writing an alert a task
	// switch to another task that triggered the exception) then if we tried to write an alert
	// here we would likely crash the pile. The safest thing to do is record here and then on
	// the startup generate the appropriate alert lines.
	
	// ----------

	restart_info.exception_noted = (true);
	
	EXCEPTION_USE_ONLY_obtain_latest_time_and_date( &(restart_info.exception_td) );
	
	/* On data abort exception the LR points to PC+8 */
	// Exception at this address showing the instruction at that address.
	snprintf( restart_info.exception_description_string, sizeof(restart_info.exception_description_string), "Undef Instr at 0x%08lX 0x%08lX", lnk_ptr, *(lnk_ptr) );

	snprintf( restart_info.exception_current_task, sizeof(restart_info.exception_current_task), "%s", pcTaskGetTaskName(NULL) );
	
	// ----------

	// 10/22/2012 rmd : Endless ... this will freeze this task we were in (yes there is ALWAYS a
	// current task) and cause the wdt task to fire. If the watchdog is not running (hardware
	// uninitialized) I would assume the developer is using the debugger so when he pauses his
	// program he'll be at this line.
	for(;;);
}

/* ---------------------------------------------------------- */
void prefetch_abort_handler(void) __attribute__ ((naked));
void prefetch_abort_handler(void)
{
	register UNS_32		*lnk_ptr;

	__asm__ __volatile__
	(
		"sub lr, lr, #8\n"
		"mov %0, lr" : "=r" (lnk_ptr)
	);
	
	// ----------

	// 10/23/2012 rmd : Though we could probably just go ahead and use the regular obtain time &
	// date function. And we could go ahead and write an alert line here. We really do not know
	// where in the code this exception occurred. For example if we were writing to the alert
	// pile and an exception occurred (even while in the middle of writing an alert a task
	// switch to another task that triggered the exception) then if we tried to write an alert
	// here we would likely crash the pile. The safest thing to do is record here and then on
	// the startup generate the appropriate alert lines.
	
	// ----------

	restart_info.exception_noted = (true);
	
	EXCEPTION_USE_ONLY_obtain_latest_time_and_date( &(restart_info.exception_td) );
	
	/* On data abort exception the LR points to PC+8 */
	// Exception at this address showing the instruction at that address.
	snprintf( restart_info.exception_description_string, sizeof(restart_info.exception_description_string), "Prefetch Abort at 0x%08lX 0x%08lX", lnk_ptr, *(lnk_ptr) );

	snprintf( restart_info.exception_current_task, sizeof(restart_info.exception_current_task), "%s", pcTaskGetTaskName(NULL) );
	
	// ----------

	// 10/22/2012 rmd : Endless ... this will freeze this task we were in (yes there is ALWAYS a
	// current task) and cause the wdt task to fire. If the watchdog is not running (hardware
	// uninitialized) I would assume the developer is using the debugger so when he pauses his
	// program he'll be at this line.
	for(;;);
}

/* ---------------------------------------------------------- */
void data_abort_handler(void) __attribute__ ((naked));
void data_abort_handler(void)
{
	register UNS_32		*lnk_ptr;
	
	__asm__ __volatile__
	(
		"sub lr, lr, #8\n"
		"mov %0, lr" : "=r" (lnk_ptr)
	);
	
	// ----------

	// 10/23/2012 rmd : Though we could probably just go ahead and use the regular obtain time &
	// date function. And we could go ahead and write an alert line here. We really do not know
	// where in the code this exception occurred. For example if we were writing to the alert
	// pile and an exception occurred (even while in the middle of writing an alert a task
	// switch to another task that triggered the exception) then if we tried to write an alert
	// here we would likely crash the pile. The safest thing to do is record here and then on
	// the startup generate the appropriate alert lines.
	
	// ----------

	restart_info.exception_noted = (true);
	
	EXCEPTION_USE_ONLY_obtain_latest_time_and_date( &(restart_info.exception_td) );
	
	/* On data abort exception the LR points to PC+8 */
	// Exception at this address showing the instruction at that address.
	snprintf( restart_info.exception_description_string, sizeof(restart_info.exception_description_string), "Data Abort at 0x%08lX 0x%08lX", lnk_ptr, *(lnk_ptr) );

	snprintf( restart_info.exception_current_task, sizeof(restart_info.exception_current_task), "%s", pcTaskGetTaskName(NULL) );
	
	// ----------

	// 10/22/2012 rmd : Endless ... this will freeze this task we were in (yes there is ALWAYS a
	// current task) and cause the wdt task to fire. If the watchdog is not running (hardware
	// uninitialized) I would assume the developer is using the debugger so when he pauses his
	// program he'll be at this line.
	for(;;);
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

