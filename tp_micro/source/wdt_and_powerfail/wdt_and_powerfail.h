/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_WDT_AND_POWERFAIL_H
#define INC_WDT_AND_POWERFAIL_H


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct {
	
	// We use the pre and post strings during the startup to see if the battery backed SRAM
	// space for this structure has been configured. When unit is BORN it will not be equal to
	// the string we are looking for. And we will initialize the space.
	char				verify_string_pre[ 24 ];

	// ----------

	BOOL_32				brown_out_interrupt_occured;

	// ----------

	// 7/31/2015 rmd : Task starvation keeps a record. One reason is if the task that is starved
	// is involved in sending the alert message to the main board we will likely not see any
	// such alert.
	BOOL_32				starvation_noted;

	char				starvation_string[ 48 ];

	// ----------

	// 10/22/2012 rmd : For the data abort, undefined instruction, and prefetch abort handler
	// the variables in this section are filled out.
	BOOL_32				exception_noted;

	char				exception_string[ 48 ];

	// ----------

	// 10/22/2012 rmd : Support for the assert function.
	BOOL_32				assert_noted;

	char				assert_string[ 48 ];

	// ----------

	char				verify_string_post[ 24 ];

} RESTART_INFORMATION_STRUCT;


extern RESTART_INFORMATION_STRUCT	restart_info;

// ----------

extern BOOL_32		alert_main_of_an_watchdog_timeout;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_restart_info( void );



void enable_brown_out_interrupt( void );

void init_watchdog_hardware( void );



void WDT_kick_the_dog( void );


extern void wdt_monitoring_task( void *pvParameters );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

