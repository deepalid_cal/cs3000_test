/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"error_handler.h"

#include	"tpmicro_comm_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/10/2012 raf :  Stores all our errors in as flags in a bitfield.  Protected by the ErrorLogMutex mutex.
volatile ERROR_LOG_s g_error_log;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION We cant call the normal flag error function from an ISR because it uses
    freertos functions and non interrupt safe calls (mutex gets). So, what we need to do is
    queue the alert for our internal task to deal with later during its time slice.

	@PARAMETERS

		perror_bit: This is the bit we are going to set in the bitfield, error bits are defined in errorHandler.h

		*pxTaskWoken: needed for freertos, used to determine whether a tasks switch is required

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITIES(none)

	@EXECUTED From ISR's.

	@RETURN (none)
*/ 
extern void flag_error_from_isr( UNS_32 perror_bit, signed portBASE_TYPE *pxtask_woken )
{
	COMM_MNGR_QUEUE_STRUCT	int_msg;

	int_msg.command = COMM_MNGR_CMD_FLAG_THIS_ERROR;

	int_msg.data = perror_bit;

	xQueueSendToBackFromISR( comm_mngr_task_queue, &int_msg, pxtask_woken );
}


/* ---------------------------------------------------------- */
/**
	@DESCRIPTION 	Sets a passed bit in our error bitfield. Do not call from an ISR!!!

	@PARAMETERS

		perror_bit: This is the bit we are going to set in the bitfield, error bits are defined in errorHandler.h


	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITIES(none)

	@EXECUTED  All over the place!

	@RETURN (none)

	@ORIGINAL 8/15/2012 raf

	@REVISIONS
*/
extern void flag_error( UNS_32 perror_bit )
{
	// 8/10/2012 raf : Our operation here is not atomic, so we need to get a mutex before we can
	// edit this variable! Otherwise a task could call into this function, get interrupted by a
	// higher priority task, which then goes in here and sets a bit that the original task wont
	// know about!
	xSemaphoreTake( g_xErrorLogMutex, portMAX_DELAY );

	B_SET( g_error_log.errorBitField, perror_bit );

	xSemaphoreGive( g_xErrorLogMutex );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION  Checks if our error bitfield is non-zero.  Its debateable about whether this call is more efficient or overkill for what we are
    trying to accomplish, but its fine for now.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITIES(none)

	@EXECUTED 	Internal comms task during message building

	@RETURN  Boolean indicating TRUE if we have error bits set, FALSE if our bitfield == 0;

	@ORIGINAL 8/15/2012 raf

	@REVISIONS
*/

extern BOOL_32 do_we_have_any_errors_to_report_to_main()
{
	BOOL_32 rv;
	rv = FALSE;

	// 8/10/2012 raf :  we operate on the ERROR LOG non atomically from multiple tasks, so anywhere we use it we have to take and release a mutex!
	if( xSemaphoreTake( g_xErrorLogMutex, portMAX_DELAY ) == pdTRUE )
	{
		rv = (g_error_log.errorBitField != 0) ? TRUE : FALSE;

		xSemaphoreGive(g_xErrorLogMutex);
	}

	return rv;
}


