//////////////////////////////////////////////////////////////////////
//
//  File: TwoWireDrvr.h
//
//  Description:    Header file for Two Wire UART driver 
//
//  $Id: TwoWireDrvr.h,v 1.11 2012/02/17 21:51:35 dsquires Exp $
//
//  $Log: TwoWireDrvr.h,v $
//  Revision 1.11  2012/02/17 21:51:35  dsquires
//  Added new #defines to support the polled RXD pin scheme for detecting ACK pulse.
//
//  Revision 1.10  2011/12/14 23:03:30  dsquires
//  Changed port/pin assignments for DATA_MODE and PWR_MODE outputs. Also changed status queue size to 2 entries.
//
//  Revision 1.9  2011/12/13 20:54:23  dsquires
//  Removed externs for functions that did not need to be public.
//
//  Revision 1.8  2011/12/05 23:18:56  dsquires
//  Clarified/improved comment.
//
//  Revision 1.7  2011/12/02 00:06:46  dsquires
//  Work in progress.
//
//  Revision 1.6  2011/12/01 21:00:13  dsquires
//  Work in progress.
//
//  Revision 1.5  2011/11/30 23:56:41  dsquires
//  Work in progress.
//
//  Revision 1.4  2011/11/24 17:58:40  dsquires
//  Moved structure defs to uart.h.
//
//  Revision 1.3  2011/11/23 22:37:20  dsquires
//  Work in progress.
//
//  Revision 1.2  2011/11/21 23:34:56  dsquires
//  Work in progress.
//
//  Revision 1.1  2011/11/15 17:56:46  dsquires
//  New files for Two Wire communications.
//
//
//////////////////////////////////////////////////////////////////////

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef TWOWIRE_H_
#define TWOWIRE_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define MAX_2_WIRE_TX_MSG_LEN					(60)

#define MAX_DECODER_RESPONSE_LENGTH_IN_BYTES	(120)

// ----------

// 6/21/2013 rmd : Setting to 26 makes for a maximum decoder loopback RESPONSE length of 32
// bytes when one includes the packet header and CRC. And 32 is the design target packet
// size.
#define MAX_LOOPBACK_DATA_LENGTH				(26)

// ----------

// The Two Wire Communications normally use 1200 baud
#define BAUDRATE_2_WIRE_USART          1200

////////////////////////////////////////////////
// 2-Wire Master Controller Timing Parameters
////////////////////////////////////////////////

// Set the next #define if a FreeRTOS software timer is to be used for the no-reply timer
#define USE_FREERTOS_TIMER              1

#define FREERTOS_TIMER_PERIOD_TICKS     40  // 2.5 character times + 10 ms. at 1200 baud
#define FREERTOS_TIMER_RESTART_TICKS    10  // 1 character time at 1200 baud
#define FREERTOS_TIMER_xBLOCK_TIME       5  // Time to wait for timer start cmd

// This define is used for 1200 baud character delays
#define TWO_WIRE_MS_PER_CHAR				(9)  // Rounded up from 8.333 ms


// 5/7/2013 rmd : The intention is to dwell one whole character time at 1200 bps prior to
// commenceing with any true UART data on the two-wire bus. This is to allow the UART a
// clean out period after being hit with the received 60hz. (Time to send MARK when entering
// DATA MODE and before leaving DATA MODE.)
#define MARK_DLY_MS_BEFORE_ASYNC_DATA		(10)


//  # consecutive spaces needed to declare an ACK pulse detected
#define CONSECUTIVE_SPACES_FOR_ACK_DETECT	(7)


// Status messages for the small two_wire io response queue
#define TWO_WIRE_IO_CONTROL_QUEUE_RESP_RECEIVED		(0x19)	// Happens to be ASCII EM (End of Medium)

// Configures the maximum time (ms) allowed for an ACK pulse to be detected
#define TWO_WIRE_ACK_PULSE_TIMEOUT_MS   15

// Two-Wire I/O Request Types
typedef enum
{
  UNICAST_TYPE_e = 0,           // No response or async response expected
  MULTICAST_TYPE_e = 1,         // No response or ACK pulse response expected
  BROADCAST_TYPE_e = 2,         // No response expected
} IO_REQ_TYPE_e;

////////////////////////////////////
//  POSSIBLE RESULT STATUS CODES
////////////////////////////////////
#define TWO_WIRE_RESULT_ACK_DETECTED				(0x06)		// Happens to be ASCII ACK

#define TWO_WIRE_RESULT_NO_RESPONSE					(0x80)		

#define TWO_WIRE_RESULT_LENGTH_LESS_THAN_THREE		(0x81)		// Received an invalid (possibly short) response

#define TWO_WIRE_RESULT_RESP_BAD_CRC				(0x82)		// Message received but has bad CRC16

#define TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH	(0x83)		// Message received with good CRC16 but wrong length for the command sent

#define TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH		(0x90)		// Message received with good CRC16 and appropriate length for the command sent

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


// Function declarations
extern void perform_two_wire_io( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*TWOWIRE_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
