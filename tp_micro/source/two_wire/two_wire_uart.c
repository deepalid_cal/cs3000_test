/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"two_wire_uart.h"

#include	"two_wire_io.h"

#include	"ringbuffer.h"

#include	"two_wire_receive.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//-----------------------------------------------------------------------------
//   The UART Control Block for the two wire UART port
//-----------------------------------------------------------------------------
volatile TWO_WIRE_IO_CONTROL_STRUCT	two_wire_io_control;

/************************** PRIVATE FUNCTIONS *************************/

//-----------------------------------------------------------------------------
//  UART INTERRUPT HANDLER FOR TWO WIRE UART PORT
//-----------------------------------------------------------------------------

extern void UART1_IRQHandler( void )
{
	//UNS_32	tx_cnt;

	//UNS_32	receive_status;

	UNS_32	intsrc, tmp1;
	UNS_32	action;

	UNS_8 c;

	signed portBASE_TYPE	task_woken = pdFALSE;

	/* Determine the interrupt source */
	intsrc = ( ((LPC_UART1_TypeDef*)LPC_UART1)->IIR & UART_IIR_INTID_MASK );

	//-----------------------------------------------------------------------------
	//   Highest priority : Receive Line Status
	//-----------------------------------------------------------------------------
	if( intsrc == UART_IIR_INTID_RLS )
	{
		// 1/23/2013 rmd : In the two-wire implementation we do not have the line interrupt
		// enabled. And therefore should not get this case. However if we do get it reading the LSR
		// is the necessary means to clear the int. And so we'll at least do that. Check line
		// status. Reading clears the interrupt.
		tmp1 = ((LPC_UART1_TypeDef*)LPC_UART1)->LSR;

		// Mask out the Receive Ready and Transmit Holding empty status
		tmp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE | UART_LSR_BI | UART_LSR_RXFE);

		//-----------------------------------------------------------------------------
		//    tmp1 now holds these Rx error bits:
		//
		//    UART_LSR_OE   = Rx overrun error, 
		//    UART_LSR_PE   = parity error, 
		//    UART_LSR_FE   = framing error,
		//    UART_LSR_BI   = Break interrupt, 
		//    UART_LSR_RXFE = FIFO contains at least one UARTx RX error.
		//-----------------------------------------------------------------------------

		// We are only interested in the Break interrupt (ACK pulse detection)
		if( tmp1 & UART_LSR_BI )
		{
			// 1/23/2013 rmd : Not used in the two-wire code. We detect the ACK using gpio.
			/*
			receive_status = TWO_WIRE_QUEUE_ACK_PULSE_DETECTED; 

			// Send an event (msg) to the waiting task (two wire driver)
			xQueueSendFromISR( g_TwoWireUartCtl_s.two_wire_queue, &receive_status, &task_woken );
			*/
		}
		
	}

	//-----------------------------------------------------------------------------
	//   Next Highest Priority: Receive Data Available 
	//-----------------------------------------------------------------------------
	if( (intsrc == UART_IIR_INTID_RDA) || (intsrc == UART_IIR_INTID_CTI) )
	{
		// 1/23/2013 rmd : The FIFO trigger level has been reached so read till empty.
		while( U1LSR_RDR_BITBAND == 1 )
		{
			c = ( ((LPC_UART1_TypeDef*)LPC_UART1)->RBR & (0xFF) );  // it is only a byte so mask to be safe
			
			// Process the received character per two wire protocol
			action = two_wire_rx_from_isr( &c, &task_woken );

			// Make sure not to exceed the allowable response length configured
			if( (action == TWO_WIRE_RX_KEEP_CHAR) && (two_wire_io_control.actual_rsp_len < MAX_DECODER_RESPONSE_LENGTH_IN_BYTES) )
			{
				// Store the received byte in the UART's receive data buffer
				two_wire_io_control.rxbuf[ two_wire_io_control.actual_rsp_len++ ] = c;
			}
			
		}
	}

	// 1/23/2013 rmd : We hang at the task level and push out the message to the decoder. Sounds
	// horrible but not as bad as you might think.
	/*
	//-----------------------------------------------------------------------------
	//   Lowest Priority: Transmit Holding Register Empty
	//-----------------------------------------------------------------------------
	if(intsrc == UART_IIR_INTID_THRE)
	{
		////////////////////////
		// transmit function
		////////////////////////

		// Check first that there is at least one byte to send
		if( 0 != g_TwoWireUartCtl_s.TxByteCnt )
		{
			// Calculate how many bytes to put into Tx FIFO
			tx_cnt = g_TwoWireUartCtl_s.TxByteCnt >= UART_FIFO_DEPTH ? UART_FIFO_DEPTH : g_TwoWireUartCtl_s.TxByteCnt;

			// Load up the Tx FIFO
			while( tx_cnt-- )
			{
				// Fetch data from tx block and load next byte into Tx FIFO, bump index into tx data buffer
				UART_SendByte( UARTx, *g_TwoWireUartCtl_s.pTxData++ );

				// Decrement # command bytes remaining to send
				g_TwoWireUartCtl_s.TxByteCnt--;
			}
		}

		// Check if all data from this buffer has been sent
		if( 0 == g_TwoWireUartCtl_s.TxByteCnt )
		{
			// All data has been sent so disable the THRE interrupt
			UART_IntConfig( UARTx, UART_INTCFG_THRE, DISABLE );

			receive_status = TWO_WIRE_QUEUE_TX_COMPLETE;

			// Send an event (msg) to the waiting task (two wire driver) to let
			// it know we have sent the command message
			xQueueSendFromISR( g_TwoWireUartCtl_s.two_wire_queue, &receive_status, &task_woken );
		}
	}
	*/

	portEND_SWITCHING_ISR( task_woken );
}  

//-----------------------------------------------------------------------------
//   void TwoWireUARTInit( void  )
// 
//   This is the Two Wire UART initialization function. It is called the first
//   time that TwoWireUartStartIO() is called. Any errors result in a 
//   DEBUG_STOP.
//
//-----------------------------------------------------------------------------
extern void two_wire_uart_init( void  )
{
	UNS_32	tmp32;

	// ----------

	// 1/23/2013 rmd : Create a small queue primarily for signaling uart receiver events. Only
	// two events deep seem to be all that is needed. In addition to receiver events could be
	// used to signal the end of a transmission if transmissions were interrupt driven.
	// Presently however transmissions are task driven. No advantage seen to interrupt driven.
	two_wire_io_control.response_queue = xQueueCreate( 2, sizeof(UNS_32) );

	// Make sure we created the status queue
	if( two_wire_io_control.response_queue == NULL )
	{
		DEBUG_STOP();
	}

	// ----------

	// 1/22/2013 rmd : Make sure UART1 interrupt is disabled while we configure the uart.
	NVIC_DisableIRQ( UART1_IRQn );

	// ----------

	CLKPWR_ConfigPPWR (CLKPWR_PCONP_PCUART1, ENABLE);

	CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_UART1, CLKPWR_PCLKSEL_CCLK_DIV_4 );

	// ----------

	// 1/22/2013 rmd : Copied from NXP code this apparent attempt to fully reset the UART. It
	// would be nice if they had a reset bit in a register!
	
	// 1/22/2013 rmd : Enable transmitter and flush.
	U1TER_TXEN_BITBAND = 1;
	while( !(((LPC_UART1_TypeDef*)LPC_UART1)->LSR & UART_LSR_TEMT) );

	// 1/22/2013 rmd : Empty the receive buffer register.
	while (((LPC_UART1_TypeDef*)LPC_UART1)->LSR & UART_LSR_RDR)
	{
		tmp32 = ((LPC_UART1_TypeDef*)LPC_UART1)->RBR;
	}
	
	((LPC_UART1_TypeDef*)LPC_UART1)->FCR = 0;  // set to default state
	((LPC_UART1_TypeDef*)LPC_UART1)->TER = 0;  // set to default state
	((LPC_UART1_TypeDef*)LPC_UART1)->IER = 0;  // set to default state
	((LPC_UART1_TypeDef*)LPC_UART1)->LCR = 0;  // set to default state
	((LPC_UART1_TypeDef*)LPC_UART1)->ACR = 0;  // set to default state
	((LPC_UART1_TypeDef*)LPC_UART1)->MCR = 0;  // set to default state
	((LPC_UART1_TypeDef*)LPC_UART1)->RS485CTRL = 0;  // set to default state
	((LPC_UART1_TypeDef*)LPC_UART1)->RS485DLY = 0;  // set to default state
	((LPC_UART1_TypeDef*)LPC_UART1)->ADRMATCH = 0;  // set to default state

	//Dummy Reading to Clear Status
	tmp32 = ((LPC_UART1_TypeDef*)LPC_UART1)->MSR;
	tmp32 = ((LPC_UART1_TypeDef*)LPC_UART1)->LSR;

	// ----------

	// 1/22/2013 rmd : Set the divisors and leave DLAB access disabled. Which is where we want
	// it.
	if( uart_set_divisors( (LPC_UART_TypeDef*)LPC_UART1, 1200 ) != SUCCESS )
	{
		DEBUG_STOP();
	}

	// ----------

	// 1/22/2013 rmd : Make up the value to write into LCR to set the number of data bits and
	// stop bits. N-8-2
	tmp32 = UART_LCR_WLEN8;  // 8 data bits

	// 1/24/2013 rmd : In this application we must use 1 stop bit. I thought the number of stop
	// bits only affected the transmitter and the receiver always worked on ONE stop bit. For
	// this LPC17xx UART that is DEFINATELY NOT THE CASE. The number of stop bits controls both
	// the xmitter and rcvr. If you set it to TWO stop bits yeah the data transmits with two.
	// But the decoder responds with ONE stop bit data and the UART does not pick up that data.
	// VERY SURPRISING! But I very confident of what I'm saying here.

	((LPC_UART1_TypeDef *)LPC_UART1)->LCR = ((UNS_8)tmp32);

	// ----------

	// 1/22/2013 rmd : Disable and reset the fifos.
	U1FCR_FIFO_Enable_BITBAND = 1;
	U1FCR_Rx_FIFO_Reset_BITBAND = 1;  // self clearing bit
	U1FCR_Tx_FIFO_Reset_BITBAND = 1;  // self clearing bit

	// 1/22/2013 rmd : Make up the value to write into the FCR to enable the FIFOs and set the
	// receive trigger level to ONE byte. We cannot tolerate having any CTI interrupts in this
	// 2W application. They cause delays before responses sent to us are recognized. And thos
	// delays translate to more unpowered bus time the decoders have to endure. To the tune of
	// 40ms more. Unacceptable.
	((LPC_UART1_TypeDef *)LPC_UART1)->FCR = ( UART_FCR_FIFO_EN | UART_FCR_TRG_LEV0 );
	
	// ----------

	// 1/22/2013 rmd : Enable the transmitter for normal operation.
	U1TER_TXEN_BITBAND = 1;
	
	// ----------

	// 1/24/2013 rmd : Enable the receiver interrupt. But do not enable the NVIC. So as the 60
	// hz is merrily chugging along the uart will fill with junk data. The interrupt in the UART
	// will fire. The FIFO will fill and begin throwing away cahracters. All to no matter. We
	// ignore all of this. And when we want to receive data we flush the FIFO and clear the
	// pending NVIC interrupt and then enable the NVIC interrupt.
	U1IER_RBR_Interrupt_Enable_BITBAND = 1;
		
	// 1/22/2013 rmd : Note - transmissions are performed via polling. And we do not want the
	// UART isr executing. So leave the NVIC disabled. But we can at least set the priority.
	NVIC_SetPriority( UART1_IRQn, MX_UART_INT_PRIORITY );
}


//-----------------------------------------------------------------------------
//   void TwoWireUartStartIO( void )
//
//   This function initiates a two-wire command/response sequence using the 
//   information previously set up in the TWO_WIRE_UART_CTL_s TwoWireUartCtl_s
//   Structure members that must be set up before calling this function are:
//   a) req_type - the type of two-wire request
//   b) txbuf - must contain the command message
//   c) TxByteCnt - must be set to the # bytes to transmit from txbuf
//   d) max_rsp_len - must be set to the max. # bytes to be received
//   d) timeout_ms - must be set to the # milliseconds to allow for a response
//
//   Returns: pdTRUE if the IO was started without error(s),
//   			pdFALSE if an error occurred.
//   
//-----------------------------------------------------------------------------
/*
extern void TwoWireUartStartIO( void )
{
	UNS_16	fifo_bytes;

	UNS_8	txbyte;
	
	//-----------------------------------------------------------------------------
	//   Load up the UART Tx FIFO with as many data bytes as possible
	//-----------------------------------------------------------------------------
	fifo_bytes = 0;

	while( (fifo_bytes < UART_FIFO_DEPTH) && (fifo_bytes < g_TwoWireUartCtl_s.TxByteCnt) )
	{
		// Get the next byte of Tx data
		txbyte = *(g_TwoWireUartCtl_s.pTxData++);

		// Load a Tx byte into the Tx FIFO
		UART_SendByte( g_TwoWireUartCtl_s.UartCfg_s.UARTx, txbyte );

		fifo_bytes++;
	}

	// Adjust the Tx byte count by # bytes loaded
	g_TwoWireUartCtl_s.TxByteCnt -= fifo_bytes;

	// Re)enable the THRE interrupt
	UART_IntConfig( g_TwoWireUartCtl_s.UartCfg_s.UARTx, UART_INTCFG_THRE, ENABLE );

	// The I/O should begin now
}
*/

#define	W2_TX_FEED_DELAY_MS		(5)

extern void two_wire_send_message( void )
{
	UNS_32	iii, limit, num_to_feed;
	
	limit = 0;
	
	// 1/22/2013 rmd : Each count of limit represents 5ms. The buffer can hold 60 chars. And
	// each char xmits in 9 ms. But use 10 so the integer math works out. We could wait here
	// 600ms!!!! But we're not doing anything else anyway even if interrupt driven the task
	// would be waiting for the tx complete queue message.
	while( (two_wire_io_control.TxByteCnt > 0) && (limit < (MAX_2_WIRE_TX_MSG_LEN * 10 / W2_TX_FEED_DELAY_MS) ) )
	{
		if( U1LSR_THRE_BITBAND == 1 )
		{
			// 1/22/2013 rmd : So the THRE bit is set. Meaning the FIFO is empty. The fifo is 16 deep
			// but we'll just put in 8 bytes. That'll take about 70ms to send'em.
			num_to_feed = (two_wire_io_control.TxByteCnt >= 8) ? 8 : two_wire_io_control.TxByteCnt;

			for( iii=0; iii<num_to_feed; iii++ )
			{
				((LPC_UART1_TypeDef *)LPC_UART1)->THR = *(two_wire_io_control.pTxData++);
			}
			
			two_wire_io_control.TxByteCnt -= num_to_feed;
		}
		else
		{
			// 1/22/2013 rmd : Let the OS breathe. At 1200 baud 5ms is a bit more than one half CHAR
			// time.
			vTaskDelay( MS_to_TICKS( W2_TX_FEED_DELAY_MS ) );
		}
		
		limit += 1;
	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

