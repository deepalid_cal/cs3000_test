/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"two_wire_fet_control.h"

#include	"two_wire_uart.h"

#include	"tpmicro_comm_mngr.h"

#include	"adc.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/18/2013 rmd : Define the aliases for coding clarity.

#define 	TWO_WIRE_60_HERTZ_IRQn		(EINT1_IRQn)

#define 	TWO_WIRE_1200BAUD_IRQn		(EINT2_IRQn)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// 1/16/2013 rmd : This variable is only set within the functions of this file. It is not to
// be used otherwise. It is manipulated at both high priority interrupt level and at the
// task level. The protection against a corrupt read or write is that accesses are atomic.
// Being atomic there are no access protections. Even more so, since it is manipulated
// within ISRs that have a higher than FreeRTOS allowable priority we CANNOT use any OS
// protection tools.
//
// 11/5/2013 rmd : UPDATE - we really have a problem. The two-wire cable short detection
// scheme requires a measured current beyond the limit (excessive current that is) for 90
// consecutive 60 hertz cycles. That represents 1.5 seconds. During a decoder command
// exchange, the bus is turned off for a few 100ms to allow a decoder to respond. During
// this time we report effectively ZERO current. And the 90 consecutive 60 hertz cycle count
// is restarted. Well during the time when we do the 'periodic stat' command to every
// decoder we could have a shorted cable for a long time (many 10's of seconds) without
// tripping the short protection and shutting off the cable power. So we need the adc task
// to be able to refernce the cable state to know to not restart the count if the cable is
// off.
volatile UNS_32	__tw_bus_mode;


// 2/6/2013 rmd : This is used for our fuse blown detection. We are using the 60 hertz two
// wire bus interrupt to additionally count this varialbe. The same discussion about the
// __tw_bus_mode variable protection and accesses apply.
static volatile UNS_32	__60_hertz_count;


/* ---------------------------------------------------------- */
void EINT1_IRQHandler(void)
{
	// 11/12/2012 rmd : This interrupt is the handler for both the 60 hertz two-wire clock
	// input, EINT1. It is always enabled. And actually always runs. In all bus modes. Even when
	// the bus is OFF or in DATA_MODE.
	
	// ----------
	
	// 1/21/2013 rmd : In all modes except DATA_MODE we turn both fet's OFF. We skip DATA mode
	// because we want this interrupt to effectively NOT do anything while we are in data mode.
	// Remember this is the 60hz isr. Which always runs. By design!
	if( __tw_bus_mode == TW_BUS_MODE_DATA )
	{
	}
	else
	if( __tw_bus_mode == TW_BUS_MODE_TRANSITION_TO_DATA )
	{
		// 1/22/2013 rmd : While transitioning to DATA mode I noticed a small 'funny'. When we first
		// enter data mode we end up 10usec from now with the NEG fet ON. And the positive OFF. That
		// is the idle state for the uart data. Well if the neg fet is on now and we turn it off.
		// Then we end up turning it back on in 10usec. It probably doesn't hurt anything to do this
		// but just makes it look like the fet control isn't quite right. Like there is an oversite
		// or something. So while I KNOW what this is I'm fixing it. And the result of the logic is
		// if the neg fet is ON leave it ON, if it is OFF, leave it OFF. So do nothing to the neg
		// fet!
		TPM_2W_POS_OFF;
	}
	else
	{
		// 1/17/2013 rmd : Turn BOTH FETs OFF. That is what we do in this interrupt. And start the
		// 10usec timer. When the timer count matches the timer interrupt turns ON the appropriate
		// FET.
		TPM_2W_NEG_OFF;
	
		TPM_2W_POS_OFF;
	}	

	// ----------
	
	// 1/17/2013 rmd : ALWAYS keep the 60 hz interrupt going. Which edge did we just interrupt
	// on?
	if( EXTPOLAR_EXTPOLAR1_BITBAND == 0 )
	{
		// 1/17/2013 rmd : Okay has interrupted on the FALLING edge. So switch to the rising edge.
		EXTPOLAR_EXTPOLAR1_BITBAND = 1;
	}
	else
	{
		// 1/17/2013 rmd : Okay has interrupted on the RISING edge. So switch to the falling edge.
		EXTPOLAR_EXTPOLAR1_BITBAND = 0;
	}
	
	// ----------

	// 1/17/2013 rmd : Clear any pending interrupt cause we wrote to the POLARITY register. We
	// have to do this anyway before exiting this ISR. BUT after we change the polarity
	// register!!!! (See users manual register description.) Certainly I prefer to see this done
	// 'early' on in the isr to combat the 'HARDWARE WRITE CACHING PROBLEM' I have seen. See top
	// of main.c for more explanation. But we have to wait till after the polarity is set.
	EXTINT_EINT1_BITBAND = 1;
	
	// 1/17/2013 rmd : We do need to do this since we diddled the interrupt POLARITY register
	// and this is recommended. See user manual. But perhaps we don't have to actually do this
	// as we are IN THIS interrupt. I'm not sure when the pending interrupt is cleared in the
	// NVIC. Automatically when the ISR exits? I would think so. This is safe either way.
	NVIC_ClearPendingIRQ( EINT1_IRQn );
	
	// ----------

	// 1/17/2013 rmd : Start the 10 usec timer. But only for the power mode cases and if we are
	// transitioning to the data mode. The FETs remain OFF for the 10 usec period to give the
	// one that was ON time to fully shut OFF. The nature of FETs. Being a volatile the compiler
	// is going to make instructions to read the bus mode for each valuation test. This is okay
	// for a number of reason. The foremost being we are in the highest priority interrupt in
	// the system so nothing can interrupt this process. In other words we can't possible get
	// different results between the reads.
	if( (__tw_bus_mode == TW_BUS_MODE_TRANSITION_TO_POWER) || (__tw_bus_mode == TW_BUS_MODE_POWER) || (__tw_bus_mode == TW_BUS_MODE_TRANSITION_TO_DATA) )
	{
		// 1/17/2013 rmd : Zero the two counting registers.
		((LPC_TIM_TypeDef*)LPC_TIM1)->TC = 0;
		((LPC_TIM_TypeDef*)LPC_TIM1)->PRESCALE_COUNTER = 0;
		
		// 1/17/2013 rmd : The rest of the timer is setup. All that's left to do is enable it.
		T1TCR_Counter_Enable_BITBAND = 1;
	}

	// ----------

	if( __tw_bus_mode == TW_BUS_MODE_TRANSITION_TO_OFF )
	{
		// 1/21/2013 rmd : Signify the bus is OFF. Technically speaking there is still potentially
		// 10 usec till the fet that was on is off. But since we are not preparing to turn another
		// fet ON don't need to consider this.
		__tw_bus_mode = TW_BUS_MODE_OFF;
	}

	// ----------

	// 2/6/2013 rmd : Bump our blown fuse monitor.
	__60_hertz_count += 1;
	
	// ----------
}

/* ---------------------------------------------------------- */
void EINT2_IRQHandler(void)
{
	// 1/21/2013 rmd : The interrupt handler for the 1200 baud. The only mode to respond to is
	// DATA mode. For all other modes ignore this isr even happened. Remember the 60hz interrupt
	// never stops and is responsible for keeping the show going.
	//
	// 11/6/2013 rmd : UPDATE - ignore cable mode. If this interrupt happens ALWAYS turn OFF the
	// FET's and operate as if we are sending data. If the UART is sending data, well there
	// isn't a case I know of when we wouldn't want to turn OFF the cable and handle this
	// interrupt. Even in the face of a cable short (as the cable is already OFF this won't
	// hurt). BUT EVEN MORE IMPORTANTLY if the UART continues to put out data and this interrupt
	// happens we MUST the clear the interrupt with the EXTINT_EINT2_BITBAND statement. This IS
	// A REQUIREMENT else the code will crash! So do not check cable mode to decide to execute
	// this or not.

	// ----------
	
	// 1/17/2013 rmd : Turn BOTH FETs OFF. That is what we do in this interrupt. And start the
	// 10usec timer. When the timer count matches the timer interrupt turns ON the appropriate
	// FET.
	TPM_2W_NEG_OFF;

	TPM_2W_POS_OFF;
	
	// ----------

	if( EXTPOLAR_EXTPOLAR2_BITBAND == 0 )
	{
		// 1/17/2013 rmd : Okay has interrupted on the FALLING edge. So switch to the rising edge.
		EXTPOLAR_EXTPOLAR2_BITBAND = 1;
	}
	else
	{
		// 1/17/2013 rmd : Okay has interrupted on the RISING edge. So switch to the falling edge.
		EXTPOLAR_EXTPOLAR2_BITBAND = 0;
	}
	
	// ----------

	// 1/17/2013 rmd : Clear any pending interrupt cause we wrote to the POLARITY register. We
	// have to do this anyway before exiting this ISR. BUT after we change the polarity
	// register!!!! (See users manual register description.) Certainly I prefer to see this done
	// 'early' on in the isr to combat the 'HARDWARE WRITE CACHING PROBLEM' I have seen. See top
	// of main.c for more explanation. But we have to wait till after the polarity is set.
	EXTINT_EINT2_BITBAND = 1;
	
	// 1/17/2013 rmd : Maybe not needed as we are in this interrupt. I'm not sure when the
	// pending interrupt is cleared in the NVIC. Automatically when the ISR exits? I would think
	// so. But we diddled the interrupt POLARITY register and this is recommended. See user
	// manual.
	NVIC_ClearPendingIRQ( EINT2_IRQn );
	
	// ----------

	// 1/21/2013 rmd : Start the 10 usec timer.
	((LPC_TIM_TypeDef*)LPC_TIM1)->TC = 0;
	((LPC_TIM_TypeDef*)LPC_TIM1)->PRESCALE_COUNTER = 0;
	
	// 1/17/2013 rmd : The rest of the timer is setup. All that's left to do is enable it.
	T1TCR_Counter_Enable_BITBAND = 1;
	
}

/* ---------------------------------------------------------- */
void TIMER1_IRQHandler( void )
{
	// ----------

	// 1/18/2013 rmd : Clear the timer interrupt. The timer is configured so that this is the
	// only interrupt possible.
	T1IR_MR0_BITBAND = 1;
	
	// ----------

	if( (__tw_bus_mode == TW_BUS_MODE_TRANSITION_TO_OFF) || (__tw_bus_mode == TW_BUS_MODE_OFF) )
	{
		// 1/22/2013 rmd : This case should not happen. When transitioning to off or in off the tc
		// is not started. But cover it anyway for safety sakes.
		TPM_2W_NEG_OFF;

		TPM_2W_POS_OFF;

		__tw_bus_mode = TW_BUS_MODE_OFF;
	}
	else
	if( __tw_bus_mode == TW_BUS_MODE_TRANSITION_TO_DATA )
	{
		// 1/21/2013 rmd : We have to assume the user called while the UART was idle. And the user
		// is 'waiting' to reach data mode. An Idle UART at the micro is a high logic level. And the
		// hardware dictates this is to be a negative two wire bus. Being a high we then should be
		// setup to wait for the falling edge on the 1200 baud interrupt.
		TPM_2W_POS_OFF;
		TPM_2W_NEG_ON;
		
		EXTPOLAR_EXTPOLAR2_BITBAND = 0;  // looking for the falling edge
		
		// 1/21/2013 rmd : Per the user manual clear any pending after didling the polarity
		// register.
		EXTINT_EINT2_BITBAND = 1;
		NVIC_ClearPendingIRQ( EINT2_IRQn );
		
		__tw_bus_mode = TW_BUS_MODE_DATA;
	}
	else
	if( __tw_bus_mode == TW_BUS_MODE_DATA )
	{
		if( EXTPOLAR_EXTPOLAR2_BITBAND == 1 )
		{
			// 1/17/2013 rmd : If we are now set to interrupt on the next rising edge it means we are
			// here to respond to an isr on the FALLING edge. For the DATA mode a falling edge would
			// mean make the bus positive by tying to the positive cap.
			TPM_2W_POS_ON;
			TPM_2W_NEG_OFF; // it's alreay OFF but do it anyway.
		}
		else
		{
			TPM_2W_POS_OFF; // it's alreay OFF but do it anyway.
			TPM_2W_NEG_ON;
		}
	}
	else
	if( (__tw_bus_mode == TW_BUS_MODE_TRANSITION_TO_POWER) || (__tw_bus_mode == TW_BUS_MODE_POWER) )
	{
		if( EXTPOLAR_EXTPOLAR1_BITBAND == 1 )
		{
			// 1/17/2013 rmd : If we are now set to interrupt on the next rising edge it means we are
			// here to respond to an isr on the FALLING edge. Set the 2W bus appropriately keeping out
			// of phase with the cap charging. Meaning while the positive cap charges ties the bus to
			// the negative cap. And vice-a-versa.
			TPM_2W_POS_ON;
			TPM_2W_NEG_OFF; // it's alreay OFF but do it anyway.
		}
		else
		{
			TPM_2W_POS_OFF; // it's alreay OFF but do it anyway.
			TPM_2W_NEG_ON;
		}

		__tw_bus_mode = TW_BUS_MODE_POWER; // no matter what we are in this mode
	}

	// ----------
}

/* ---------------------------------------------------------- */
extern void setup_timer1( void )
{
	// 11/16/2012 rmd : Enable power to peripheral.
	CLKPWR_ConfigPPWR( CLKPWR_PCONP_PCTIM1 , ENABLE );
	
	// 11/16/2012 rmd : CCLOCK is 72MHz. So dividing by 4 gives us an 18MHz counter clock.
	CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_TIMER1, CLKPWR_PCLKSEL_CCLK_DIV_4 );
	
	// 11/16/2012 rmd : Timer is disabled from counting.
	((LPC_TIM_TypeDef*)LPC_TIM1)->TIMER_CONTROL_REG = 0;

	// 11/16/2012 rmd : Set such that TC is incremented when the prescale counter matches the
	// prescale register. And the prescale counter counts on each PCLK rising edge.
	((LPC_TIM_TypeDef*)LPC_TIM1)->COUNT_CONTROL_REG = 0x00000000;
	
	// 1/17/2013 rmd : These are the 2 registers that count.
	((LPC_TIM_TypeDef*)LPC_TIM1)->TC = 0;
	((LPC_TIM_TypeDef*)LPC_TIM1)->PRESCALE_COUNTER = 0;

	// 11/16/2012 rmd : With a match value of 0 the TC counts at the FULL CCLK rate.
	((LPC_TIM_TypeDef*)LPC_TIM1)->PRESCALE_MATCH_VALUE = 0;

	// 11/16/2012 rmd : Set to interrupt when the TC matches MR0. And stop.
	((LPC_TIM_TypeDef*)LPC_TIM1)->MATCH_CONTROL_REG = 5;

	((LPC_TIM_TypeDef*)LPC_TIM1)->MR0 = 180;  // 90 is 5usec, 180 is 10usec
	((LPC_TIM_TypeDef*)LPC_TIM1)->MR1 = 0;
	((LPC_TIM_TypeDef*)LPC_TIM1)->MR2 = 0;
	((LPC_TIM_TypeDef*)LPC_TIM1)->MR3 = 0;
	

	((LPC_TIM_TypeDef*)LPC_TIM1)->CAPTURE_CONTROL_REG = 0;

	((LPC_TIM_TypeDef*)LPC_TIM1)->EXTERNAL_MATCH_REG = 0;

	// 11/16/2012 rmd : Clear all the interrupts for throughness.
	((LPC_TIM_TypeDef*)LPC_TIM1)->INTERRUPT_REG = 0x0000003F;
	
	// 1/17/2013 rmd : Clear any pending in the NVIC just incase.
	NVIC_ClearPendingIRQ( TIMER1_IRQn );

	NVIC_SetPriority( TIMER1_IRQn, TWO_WIRE_INTERRUPT_PRIORITY_TIMER_1 );
	
	NVIC_EnableIRQ( TIMER1_IRQn );

	// 1/17/2013 rmd : All done. Note the timer is ready to go but NOT running yet.
}

/* ---------------------------------------------------------- */
extern void initialize_two_wire_fet_control( void )
{
	// 10/17/2013 rmd : Called only at startup. At the beginning of the 2W task.
	
	// ----------

	// 1/21/2013 rmd : Start with the bus off.
	__tw_bus_mode = TW_BUS_MODE_OFF;

	two_wire_bus_info.cable_is_powered = (false);

	two_wire_bus_info.cable_is_done_charging = (false);

	two_wire_bus_info.cable_percent_of_maximum_current = 0;

	// ----------

	two_wire_uart_init();

	setup_timer1();
	
	// ----------

	// 1/17/2013 rmd : Get the 60 hertz interrupt up and going.

	NVIC_DisableIRQ( TWO_WIRE_60_HERTZ_IRQn );
	
	NVIC_SetPriority( TWO_WIRE_60_HERTZ_IRQn, TWO_WIRE_INTERRUPT_PRIORITY_60_HZ );

	// 1/17/2013 rmd : Setup the Mode for edge sensitive.
	EXTMODE_EXTMODE1_BITBAND = 1;

	// 1/18/2013 rmd : Enable the 60 hertz signal to cause an interrupt on the next rising edge.
	// That happens to be the next positive going swing on the 24VAC line. No particular reason
	// to wait for the next rising. Could've just as well been the next falling.
	EXTPOLAR_EXTPOLAR1_BITBAND = 1;

	// 1/21/2013 rmd : Per the user manual clear any pending after didling the polarity
	// register.
	EXTINT_EINT1_BITBAND = 1;
	NVIC_ClearPendingIRQ( EINT1_IRQn );
	
	NVIC_EnableIRQ( TWO_WIRE_60_HERTZ_IRQn );

	// ----------

	// 1/17/2013 rmd : Get the 1220 baud interrupt up and enabled. After this it will fire if
	// there is data xmitted.

	NVIC_DisableIRQ( TWO_WIRE_1200BAUD_IRQn );
	
	NVIC_SetPriority( TWO_WIRE_1200BAUD_IRQn, TWO_WIRE_INTERRUPT_PRIORITY_1200_BAUD );

	// 1/17/2013 rmd : Setup the Mode for edge sensitive.
	EXTMODE_EXTMODE2_BITBAND = 1;

	// 1/21/2013 rmd : Setting this here is for clarity sakes. It really should matter cause we
	// have to pass through the 60hz isr in order to achieve data mode. And there we will set
	// the polarity approprately.
	EXTPOLAR_EXTPOLAR2_BITBAND = 0;

	// 1/21/2013 rmd : Per the user manual clear any pending after didling the polarity
	// register.
	EXTINT_EINT2_BITBAND = 1;
	NVIC_ClearPendingIRQ( EINT2_IRQn );

	NVIC_EnableIRQ( TWO_WIRE_1200BAUD_IRQn );
	
}

/* ---------------------------------------------------------- */
extern void during_io_switch_and_wait_for_bus_to_enter_power_mode( void )
{
	// 10/17/2013 rmd : Intended to be used during decoder I/O operations. Does not adjust the
	// bus status flags.
	
	// ----------

	UNS_32	ms_to_wait;

	// ----------
	
	// 11/4/2013 rmd : The adc readings have determined a cable fault. Do not allow cable to
	// transition to power mode.
	if( !adc_readings.two_wire_cable_excessive_current && !adc_readings.two_wire_terminal_over_heated )
	{
		// ----------
	
		__tw_bus_mode = TW_BUS_MODE_TRANSITION_TO_POWER;
		
		// ----------
	
		// 4/15/2013 rmd : And actually wait for the interrupts to occur and the bus to actually
		// switch to the POWER MODE.
	
		ms_to_wait = 0;
	
		while( __tw_bus_mode != TW_BUS_MODE_POWER )
		{
			vTaskDelay( 1 );
			
			ms_to_wait += 1;
			
			if( ms_to_wait == 20 )
			{
				// 1/21/2013 rmd : This is an error!
				__tw_bus_mode = TW_BUS_MODE_OFF;
				
				two_wire_bus_info.cable_is_powered = (false);  // should NEVER happen!
				
				flag_error( BIT_TPME_two_wire_bus_mode_error );
	
				break;
			}
		}

	}  // of if cable is healthy

}

/* ---------------------------------------------------------- */
extern void during_io_switch_and_wait_for_bus_to_enter_data_mode( void )
{
	// 10/17/2013 rmd : Intended to be used during decoder I/O operations. Does not adjust the
	// bus status flags.
	
	// ----------

	UNS_32	ms_to_wait;
	
	// ----------
	
	// 11/4/2013 rmd : The adc readings have determined a cable fault. Do not allow cable to
	// transition to data mode.
	if( !adc_readings.two_wire_cable_excessive_current && !adc_readings.two_wire_terminal_over_heated )
	{
		// ----------
	
		__tw_bus_mode = TW_BUS_MODE_TRANSITION_TO_DATA;
		
		// ----------
	
		// 4/15/2013 rmd : And actually wait for the interrupts to occur and the bus to actually
		// switch to the DATA MODE.
	
		ms_to_wait = 0;
	
		while( __tw_bus_mode != TW_BUS_MODE_DATA )
		{
			vTaskDelay( 1 );
			
			ms_to_wait += 1;
			
			if( ms_to_wait == 20 )
			{
				// 1/21/2013 rmd : This is an error! Let's just get out of here. Should embellish with an
				// error bit in the field sent to the main board.
				__tw_bus_mode = TW_BUS_MODE_OFF;
				
				flag_error( BIT_TPME_two_wire_bus_mode_error );
				
				two_wire_bus_info.cable_is_powered = (false);
	
				break;
			}
		}

	}  // of if cable is healthy

}

/* ---------------------------------------------------------- */
extern void during_io_turn_off_the_bus_NOW_and_setup_to_transition_to_off_mode( void )
{
	// 10/17/2013 rmd : Intended to be used during decoder I/O operations. Does not adjust the
	// bus status flags.
	
	// ----------

	// 1/29/2013 rmd : Set the mode first. Then turn off the fets. Generally when this funciton
	// is called we are in DATA mode and have just sent a message. And want to get the bus off
	// asap. If we turned the fets off first, and then one of the fet managing isrs fired,
	// before we set the mode to off, then likely one of the fets would come back on! So set
	// this first. THEN turn OFF the fets. This way they are sure to stay off.
	__tw_bus_mode = TW_BUS_MODE_TRANSITION_TO_OFF;

	// ----------

	// 1/21/2013 rmd : This is the only fet manipulation outside of the three isr's responsible
	// for their control. In this case we are transitioning to OFF and there can be no wrong
	// here in turning off both fets now. Furthermore after a message has been sent to a decoder
	// it is a requirement to float the bus (that is to turn it off). And to do so quickly. If
	// we did not pre-empt the turn off here, an indeterminent amount of time would pass til
	// they were snapped off. Up to 8ms (half 60hz cycle). So do it here.
	TPM_2W_NEG_OFF;
	
	TPM_2W_POS_OFF;

	// ----------

	// 1/21/2013 rmd : Done. Do not wait. The bus is off now cause we just turn off the fets.
	// And within a half cycle the 60 hz interrupt will fire. As we are set to transition to off
	// the isr really doesn't do anything to the fets. Sure it turns them off butthey are
	// already off. And we do not start the 10 usec timer.
}

/* ---------------------------------------------------------- */
extern void power_up_two_wire_cable( UNS_32 *ptask_watchdog_time_stamp_ptr, BOOL_32 pwait_till_charged )
{
	// 10/17/2013 rmd : May ONLY be executed within the context of the two_wire task. Do not use
	// during low level 2-W I/O operations. Note the need to update the task watchdog time
	// stamp.
	if( !two_wire_bus_info.cable_is_powered )
	{
		alert_message_to_tpmicro_pile( "2W bus power up." );
		
		// ----------
		
		// 10/17/2013 rmd : Set this flag now so we begin using the correct short magnitude and
		// duration in the adc post conversion analysis function (executed in the context of the
		// comm_mngr). Additionally, based upon this flag we postpone any decoder command
		// transactions until the cable has charged all those decoder caps.
		two_wire_bus_info.cable_is_done_charging = (false);
		
		// 11/6/2013 rmd : And indicate to start the short count anew. It is latched at the last
		// value while the cable was in POWER or DATA mode. If that terminated in a short the count
		// is already at one of the limits (charging or normal). If we left it there the short could
		// trip right away during cable charging as we wouldn't get our 2.5 second count up.
		clear_short_count = (true);
		
		// 10/18/2013 rmd : Yes it's not 'during_io'. But the use of this within this function is
		// THE way to put the cable into power mode. The point is the 'during_io' functions, all of
		// them, don't mess with the cable_is_powered and done_charging flags. We do that only
		// within the power_up and power_down functions.
		during_io_switch_and_wait_for_bus_to_enter_power_mode();
		
		// ----------
		
		two_wire_bus_info.cable_is_powered = (true);

		// 10/17/2013 rmd : Start the cable charge timer. To inhibit command exchanges and allow
		// higher current limits during the charge phase.
		xTimerStart( two_wire_bus_info.cable_charge_timer, portMAX_DELAY );
	}
	
	// ----------

	// 8/3/2015 rmd : In some cases we don't want to or need to wait.
	if( pwait_till_charged )
	{
		// 10/17/2013 rmd : Dwell until the timer fires and (in the context of the high priority
		// timer task) sets the charging complete indication.
		while( !two_wire_bus_info.cable_is_done_charging )
		{
			// 10/17/2013 rmd : Hey this is a long time. Like 6 Seconds! Let other tasks like the
			// comm_mngr continue to do its things. If not we'll have memory problems. For example the
			// ADC samples keep taking memory and being queue to the comm_mngr.
			vTaskDelay( MS_to_TICKS( 250 ) );
			
			// 8/3/2015 rmd : Because this function does not return for potentially a long time (a
			// minute for example) the task itself does not have the opportunity to update its watchdog
			// time stamp. So we have to do that here otherwise the superviosry watchdog task would flag
			// this task as starved and force a tpmicro restart.
			*ptask_watchdog_time_stamp_ptr = xTaskGetTickCount();
		}
	}
}

/* ---------------------------------------------------------- */
extern void power_down_two_wire_cable( void )
{
	// 10/17/2013 rmd : This function is to be used at more of the application level. Do not use
	// during low level 2-W I/O operations.

	if( two_wire_bus_info.cable_is_powered )
	{
		alert_message_to_tpmicro_pile( "2W bus power down." );
		
		during_io_turn_off_the_bus_NOW_and_setup_to_transition_to_off_mode();
		
		// ----------
	
		// 10/17/2013 rmd : Stop the cable charge timer and set the flags appropriately.
		xTimerStop( two_wire_bus_info.cable_charge_timer, portMAX_DELAY );
		
		two_wire_bus_info.cable_is_powered = (false);
	
		two_wire_bus_info.cable_is_done_charging = (false);
	}
}

/* ---------------------------------------------------------- */
extern void FUSE_one_hertz_blown_fuse_test( void )
{
	// 2/6/2013 rmd : ATOMIC read so safe. This function should be called once per second. I
	// just picked 48 for the threshold. I suppose at 48 it would work for 50 hertz power too.
	if( __60_hertz_count > 48 )
	{
		if( fuse_is_blown )
		{
			pre_defined_alert_to_main( TPALERT_field_power_fuse_okay );
			
			fuse_is_blown = (false);
		}
	}
	else
	{
		if( !fuse_is_blown )
		{
			pre_defined_alert_to_main( TPALERT_field_power_fuse_blown );
			
			fuse_is_blown = (true);
		}
	}

	// 3/7/2013 rmd : And reset our counter. ATOMIC write so safe.
	__60_hertz_count = 0;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

