//////////////////////////////////////////////////////////////////////
//
//  File: TwoWireMsgBldr.c
//
//  Description: Two Wire (command) message builder
//
//  $Id: TwoWireMsgBldr.c,v 1.5 2011/12/16 23:07:50 dsquires Exp $
//
//  $Log: TwoWireMsgBldr.c,v $
//  Revision 1.5  2011/12/16 23:07:50  dsquires
//  Added conditional inclusion of header files.
//
//  Revision 1.4  2011/12/14 23:05:29  dsquires
//  Added missing initialization of CRC value before building a new message.
//
//  Revision 1.3  2011/12/14 00:16:44  dsquires
//  Corrected byte ordering when inserting serial number into message.
//
//  Revision 1.2  2011/12/13 20:53:13  dsquires
//  Added two wire message building functions.
//
//  Revision 1.1  2011/11/15 17:56:46  dsquires
//  New files for Two Wire communications.
//
//////////////////////////////////////////////////////////////////////

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"two_wire_msg_builder.h"

#include	"two_wire_uart.h"

#include	"two_wire_task.h"

#include	"crc16.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/30/2013 rmd : Some of the smallest responses take only 60 ms. However realistically one
// would have to wait for some 100ms for this response to make a reasonalbe window for it to
// show within. I've seen Dave using 140ms here or there. And this seems like a reasonable
// catch all minimum dwell time to wait for a response when expecting one. Of course some
// response require much more time as they contain a bunch of data.
#define	DECODER_MINIMUM_RESPONSE_WAIT_MS	(140)


// This serial number will be used in multicast messages
#define DECODER_BROADCAST_SN				(0xfffff)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//////////////////
// PRIVATE DATA
//////////////////
static UNS_16 _crc16;
static UNS_16 _tx_byte_cnt;

///////////////////////
//  PRIVATE FUNCTIONS
///////////////////////
static UNS_8 _getLsByteVal16( UNS_16 val16 )
{
	return(UNS_8) ( val16 & 0x00ff );
}

static UNS_8 _getMsByteVal16( UNS_16 val16 )
{
	return(UNS_8) (val16 >>8);
}

static UNS_8 _getbyteVal32( UNS_32 byte, UNS_32 val32 )
{
	UNS_8	val8;

	val8 = (val32>>(byte<<3)) & 0x000000ff;

	return( val8 );
}

/////////////////////////////////////////////////////////////
//   AddByteToTxMsg(UNS_8 byte, BOOL_32 bIncludeInCRC)
//
//      Purpose: adds a data byte to the 2-wire transmit
//      TWM_buffer. If bIncludeInCRC is TRUE, then SYN byte-
//      stuffing is performed and the data byte is included
//      in the CRC16 calculation.
/////////////////////////////////////////////////////////////
static void AddByteToTxMsg( UNS_32 pdata, BOOL_32 bIncludeInCRC )
{
	UNS_8	lbyte;
	
	lbyte = (UNS_8)pdata;

	// first, put the byte into the transmit buffer, update
	// the byte counter.
	two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = lbyte;

	if( bIncludeInCRC )
	{
		// Include in CRC and handle byte-stuffing
		_crc16 = _update_crc16( _crc16, lbyte );

		// did we add a DLE?
		if( lbyte == DLE )
		{
			// Yes, byte-stuff a SYN character
			two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = SYN;
		}
	}
}

/////////////////////////////////////////////////////////
//      void completeTxMsg(void)
//
//      Purpose: adds the CRC16 value and the trailing
//      DEL EOT to TwoWireUartCtl_s.txbuf[].
/////////////////////////////////////////////////////////
static void completeTxMsg( void )
{
	// First put the CRC high byte into the transmit buffer, update
	// the byte counter.
	two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = _getMsByteVal16( _crc16 );

	// Check if byte-stuff needed
	if( _getMsByteVal16( _crc16 ) == DLE )
	{
		// Yes, we must insert a SYN char
		two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = SYN;
	}

	// Then put the CRC low byte into the transmit buffer, update
	// the byte counter.
	two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = _getLsByteVal16( _crc16 );

	// Check if byte-stuff needed
	if( _getLsByteVal16( _crc16 ) == DLE )
	{
		// Yes, we must insert a SYN char
		two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = SYN;
	}

	// Finally, add the trailing DLE EOT
	two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = DLE;
	two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = EOT;

	// Set the tx byte count in the control block
	two_wire_io_control.TxByteCnt = _tx_byte_cnt;
}

//-----------------------------------------------------------------------------
//  void newTxMsg( UNS_8 opdcode, UNS_32 serial_num, BOOL_32 bComplete )
//
//  This function starts building a new transmit message in
//  TwoWireUartCtl_s.txbuf[], using the specified opcode and serial number.
//
//  If bComplete is TRUE, then the message is completed.
//-----------------------------------------------------------------------------
static void newTxMsg( UNS_32 opcode, UNS_32 serial_num, BOOL_32 bComplete )
{
	UNS_8	val8;

	UNS_32	i;

	// ----------

	// 1/31/2013 rmd : So we know how to parse the response.
	two_wire_io_control.op_code = opcode;

	// 1/31/2013 rmd : The dis field is used when posting errors at the decoder level. Only the
	// UNICAST message type is to a specific decoder.
	if( two_wire_io_control.req_type == UNICAST_TYPE_e )
	{
		find_decoder( serial_num, (FIND_DECODER_STRUCT*)&(two_wire_io_control.fds) );
		
		// 1/31/2013 rmd : If the decoder cannot be found an error is racked up and the two pointers
		// within the structure will be NULL. Before any use of the pointers we must check for NULL.
	}
	else
	{
		two_wire_io_control.fds.info_ptr = NULL;

		two_wire_io_control.fds.list_ptr = NULL;
	}
	
	// 1/31/2013 rmd : Even if we can't find the decoder we proceed with the message.

	// ----------

	_tx_byte_cnt = 0;

	_crc16 = 0xffff;

	// ----------

	two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = DLE;
	two_wire_io_control.txbuf[ _tx_byte_cnt++ ] = SOH;

	// ----------


	// Add the opcode
	AddByteToTxMsg( opcode, TRUE );

	// ----------

	// Now, take care of the serial number. Since we transmit in big-endian (network) byte
	// order, we start with the most significant byte, byte #3.
	for( i=EEPROM_SN_ADDR_BYTES; i>0; i-- )
	{
		// Add the serial number bytes to the transmit buffer
		val8 = _getbyteVal32( (i-1), serial_num );

		AddByteToTxMsg( val8, TRUE );
	}

	// ----------

	if( bComplete )
	{
		// Go ahead and complete the message
		completeTxMsg();
	}

	// ----------
}

//-----------------------------------------------------------------------------
//  START OF PUBLIC MESSAGE BUILDING FUNCTIONS
//-----------------------------------------------------------------------------

extern void build_discovery_reset_msg( void )
{
	two_wire_io_control.req_type = BROADCAST_TYPE_e;
	
	// 1/30/2013 rmd : No need to set the response_timeout_ms for a multicast message. This
	// message type has no response at all.

	// 1/25/2013 rmd : This outgoing message is BROADCAST. And therefore is to sn 0xFFFFF.
	newTxMsg( OP_DISC_RST, DECODER_BROADCAST_SN, TRUE );
}

/* ---------------------------------------------------------- */
// 1/25/2013 rmd : Used during the discovery process.
extern void build_enquiry_msg( UNS_32 serial_num, UNS_32 mask_len )
{
	two_wire_io_control.req_type = MULTICAST_TYPE_e;
	
	// 1/30/2013 rmd : No need to set the response_timeout_ms for a multicast message. If there
	// is a response, this message type reponds with an ACK pulse.

	// Add the DLE SOH header, opcode, serial number
	newTxMsg( ENQ, serial_num, FALSE );

	// Now add the serial number mask length
	AddByteToTxMsg( mask_len, TRUE );

	// Finally, complete the message
	completeTxMsg();
}

/* ---------------------------------------------------------- */
extern void build_discovery_confirmation_msg( UNS_32 serial_num )
{
	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 1/30/2013 rmd : This response is about 85ms long. So we'll allow a 140ms window during
	// which it must show.
	two_wire_io_control.response_timeout_ms = DECODER_MINIMUM_RESPONSE_WAIT_MS;

	// Add the DLE SOH header, then op-code, serial number
	newTxMsg( OP_DISC_CONF, serial_num, TRUE );
}

/* ---------------------------------------------------------- */
//  void bldSolenoidControl( UNS_32 serial_num, UNS_8 solenoid, UNS_8 ctrl )
//
//  This function builds a solenoid control message.
//
//      UNS_8 solenoid :   solenoid # (numbered starting from 0)
//
//      BOOL_32	ctrl     :  (true) to turn ON, (false) OFF
//
extern void build_solenoid_control( UNS_32 serial_num, UNS_32 solenoid, BOOL_32 ctrl )
{
	UNS_8	b;

	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 1/30/2013 rmd : This response is about 65ms long. So we'll allow a 140ms window during
	// which it must show.
	two_wire_io_control.response_timeout_ms = DECODER_MINIMUM_RESPONSE_WAIT_MS;

	// Add the DLE SOH header
	newTxMsg( OP_SOL_CTL, serial_num, FALSE );

	// Create the value for the solenoid number field. A logical shift of 4.
	b = (solenoid << 4);

	// Now "or" in the control state value
	if( ctrl )
	{
		b |= SOL_CTL_CMD_SOL_ON;
	}

	// Add the data, include it in CRC16
	AddByteToTxMsg( b, TRUE );

	// Finally, complete the message
	completeTxMsg();
}

/* ---------------------------------------------------------- */
extern void build_flow_pulse_count_request_msg( UNS_32 serial_num )
{
	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 1/30/2013 rmd : This response is about 150ms long. So we'll allow a 200ms window during
	// which it must show.
	two_wire_io_control.response_timeout_ms = 200;

	// Add the DLE SOH header, then op-code, serial number. And complete the msg.
	newTxMsg( OP_GET_FLOW_CNTS, serial_num, TRUE );
}

/* ---------------------------------------------------------- */
extern void build_moisture_reading_request_msg( UNS_32 serial_num )
{
	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 1/30/2013 rmd : This response is long. The moisture sensor response string itself is 20
	// bytes. At 21 bytes payload this is a long response. The response itself takes about 250ms
	// to complete. So we'll allow 350ms to come in.
	two_wire_io_control.response_timeout_ms = 350;

	// Add the DLE SOH header, then op-code, serial number. And complete the msg.
	newTxMsg( OP_GET_MOISTURE_DATA, serial_num, TRUE );
}

/* ---------------------------------------------------------- */
extern void build_statistics_clear_msg( UNS_32 serial_num )
{
	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 1/30/2013 rmd : This response is about 60ms long. So we'll allow a 140ms window during
	// which it must show.
	two_wire_io_control.response_timeout_ms = DECODER_MINIMUM_RESPONSE_WAIT_MS;

	// Add the DLE SOH header, then op-code, serial number
	newTxMsg( OP_STATS_CLEAR, serial_num, TRUE );
}

/* ---------------------------------------------------------- */
extern void build_statistics_request_msg( UNS_32 serial_num )
{
	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 6/26/2013 rmd : This response takes a bit over 300ms to so allow 350ms to come in.
	two_wire_io_control.response_timeout_ms = 350;

	// Add the DLE SOH header, then op-code, serial number. And complete the msg.
	newTxMsg( OP_STATS_REQ, serial_num, TRUE );
}

/* ---------------------------------------------------------- */
extern void build_status1_request_msg( UNS_32 serial_num )
{
	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 1/30/2013 rmd : This response is about 80ms long. So we'll allow a 140ms window during
	// which it must show.
	two_wire_io_control.response_timeout_ms = DECODER_MINIMUM_RESPONSE_WAIT_MS;

	// Add the DLE SOH header, then op-code, serial number. And complete the msg.
	newTxMsg( OP_STAT1_REQ, serial_num, TRUE );
}

/* ---------------------------------------------------------- */
extern void build_status2_request_msg( UNS_32 serial_num )
{
	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 1/30/2013 rmd : This response is about 150ms long. So we'll allow a 200ms window during
	// which it must show.
	two_wire_io_control.response_timeout_ms = 200;

	// Add the DLE SOH header, then op-code, serial number
	newTxMsg( OP_STAT2_REQ, serial_num, TRUE );
}

/* ---------------------------------------------------------- */
extern void build_restore_user_parameters_to_defaults( UNS_32 serial_num )
{
	UNS_8	b;

	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 1/30/2013 rmd : This response is about 65ms long. So we'll allow a 140ms window during
	// which it must show.
	two_wire_io_control.response_timeout_ms = DECODER_MINIMUM_RESPONSE_WAIT_MS;

	// Add the DLE SOH header
	newTxMsg( OP_RSTR_DFLTS, serial_num, FALSE );

	// Create the value for the solenoid number field. A logical shift of 4.
	b = (EEP_COMMON_PARMS | EEP_SOL1_PARMS | EEP_SOL2_PARMS);

	// Add the data, include it in CRC16
	AddByteToTxMsg( b, TRUE );

	// Finally, complete the message
	completeTxMsg();
}

/* ---------------------------------------------------------- */
extern void build_set_serial_number_msg( UNS_32 pnew_serial_num )
{
	// ----------

	UNS_32	i, j;
	
	UNS_8	b;

	// ----------

	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 1/30/2013 rmd : This response is about 65ms long. So we'll allow a 140ms window during
	// which it must show.
	two_wire_io_control.response_timeout_ms = DECODER_MINIMUM_RESPONSE_WAIT_MS;

	// ----------

	// 6/17/2013 rmd : Kick off the message. Not ethe serial number is 0x000000. This is the
	// decoder serial number after first programming and the only serial numbered decoder that
	// will accept the command to program a new serial number.
	newTxMsg( OP_SET_SN_REQ, 0, FALSE );

	// ----------

	// 6/17/2013 rmd : We need to place the new serial number and its complement into the
	// message. 6 bytes all together.

	for( j=0; j<2; j++ )
	{
		// Now, take care of the serial number. Since we transmit in big-endian (network) byte
		// order, we start with the most significant byte, byte #3.
		for( i=EEPROM_SN_ADDR_BYTES; i>0; i-- )
		{
			// Add the serial number bytes to the transmit buffer
			b = _getbyteVal32( (i-1), pnew_serial_num );
	
			AddByteToTxMsg( b, TRUE );
		}

		pnew_serial_num = ~pnew_serial_num;
	}

	// ----------

	// Finally, complete the message
	completeTxMsg();
}

/* ---------------------------------------------------------- */
extern void build_data_loopback_msg( UNS_32 serial_num, UNS_8 *pData, UNS_32 byte_cnt )
{
	two_wire_io_control.req_type = UNICAST_TYPE_e;
	
	// 6/26/2013 rmd : Maximum total response is set for 32 bytes at approx 8ms per byte. So
	// allow 300ms for response to make it home.
	two_wire_io_control.response_timeout_ms = 300;

	// Add the DLE SOH header, then op-code, serial number
	newTxMsg( OP_DATA_LOOPBK, serial_num, FALSE );

	// ----------

	// Add the data bytes to the message
	while( byte_cnt-- )
	{
		// Add the data bytes, include it in CRC16
		AddByteToTxMsg( *pData++, TRUE );
	}

	// ----------

	// Finally, complete the message
	completeTxMsg();
}

//-----------------------------------------------------------------------------
//  void bldDecoderRstMsg( UNS_32 serial_num )
//
//  This function builds a decoder reset message
//-----------------------------------------------------------------------------
extern void bldDecoderRstMsg( UNS_32 serial_num )
{
	// Add the DLE SOH header, then op-code, serial number
	newTxMsg( OP_DECODER_RST, serial_num, TRUE );
}

//-----------------------------------------------------------------------------
//  void bldIdReqMsg( UNS_32 serial_num )
//
//  This function builds an ID REQUEST message
//-----------------------------------------------------------------------------
extern void bldIdReqMsg( UNS_32 serial_num )
{
	// Add the DLE SOH header, then op-code, serial number
	newTxMsg( OP_ID_REQ, serial_num, TRUE );
}

//-----------------------------------------------------------------------------
//  void bldPutParmsMsg( UNS_32 serial_num, UNS_8 region, UNS_8 *pParms, UNS_8 size )
//
//  This function builds a OP_PUT_PARMS_CMD message:
//
//      typedef struct
//      {
//          cmd_msg_hdr_s   hdr;        // common command message header
//          UNS_8              region;     // parameter region # to send
//
//      } OP_PUT_PARMS_CMD_s;
//
//-----------------------------------------------------------------------------
extern void bldPutParmsMsg( UNS_32 serial_num, UNS_8 region, UNS_8 *pParms, UNS_8 size )
{
	// Add the DLE SOH header, opcode, serial number
	newTxMsg( OP_PUT_PARMS, serial_num, FALSE );

	// Add the region (incl in CRC)
	AddByteToTxMsg( region, TRUE );

	while( size-- )
	{
		// Add the parameter bytes (incl in CRC)
		AddByteToTxMsg( *pParms++, TRUE );
	}

	// Finally, add the CRC16 and trailer (DLE EOT)
	completeTxMsg();
}

//-----------------------------------------------------------------------------
//  void bldGetParmsMsg( UNS_32 serial_num, UNS_8 region )
//
//  This function builds a OP_GET_PARMS opcode message:
//
//    typedef struct
//    {
//      cmd_msg_hdr_s	hdr;		// common command message header
//      UNS_8				region;		// parameter region # to send
//    } OP_GET_PARMS_CMD_s;
//
//-----------------------------------------------------------------------------
extern void bldGetParmsMsg( UNS_32 serial_num, UNS_8 region )
{
	// Add the DLE SOH header, opcode, serial number
	newTxMsg( OP_PUT_PARMS, serial_num, FALSE );

	// Add the region (incl in CRC)
	AddByteToTxMsg( region, TRUE );

	// Finally, add the CRC16 and trailer (DLE EOT)
	completeTxMsg();
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

