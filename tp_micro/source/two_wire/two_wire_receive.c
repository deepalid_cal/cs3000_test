/////////////////////////////////////////////////////////
//
//    File: TwoWireRx.c 
//
//    Description: Two Wire protocol receive parser
//
//    $Id: TwoWireRx.c,v 1.5 2011/12/16 23:07:50 dsquires Exp $
//    $Log:
/////////////////////////////////////////////////////////

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"two_wire_receive.h"

#include	"two_wire_io.h"

#include	"two_wire_uart.h"

#include	"crc16.h"

#include	"two_wire_task.h"

#include	"tpmicro_comm_mngr.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

///////////////////////////////////////////
//  SC State -  the serial communications
//              state machine state
///////////////////////////////////////////
static SC_STATES_e  sc_state;

///////////////////////////////////////////////////////
//  Initialize the serial communications state machine
///////////////////////////////////////////////////////
void tw_receive_init( void )
{
	sc_state = SC_RX_IDLE_e;

	// ----------
	
	// 1/25/2013 rmd : And initialize the receipt control.

	two_wire_io_control.response_result = TWO_WIRE_RESULT_NO_RESPONSE;

	two_wire_io_control.actual_rsp_len = 0;  // No bytes received
	
	// ----------
}

//////////////////////////////////////////////////////
//  This function checks the message CRC.
//  The last two bytes of the message are assumed to
//  be the transmitted CRC16 value
//////////////////////////////////////////////////////
BOOL_32 bMsgHasGoodCrc( UNS_8 *pMsg, UNS_8 MsgLen )
{
	UNS_16	crc16 = 0xffff;

	UNS_16	rx_crc;

	UNS_32	i;

	for( i = 0; i < MsgLen - 2; i++ )
	{
		crc16 = _update_crc16( crc16, *(pMsg+i) );
	}

	////////////////////////////////////////////////////////////////
	//  The 2-byte CRC16 value should start at pMsg + (MsgLen - 2)
	////////////////////////////////////////////////////////////////

	// Fetch the transmitted CRC16 value
	rx_crc = *(pMsg + MsgLen - 1) | (*(pMsg + MsgLen - 2)) << 8 ;

	// Compare it against the computed CRC16
	return( rx_crc == crc16 );
}


/////////////////////////////////////////////////////////////////////////
//  Serial Communications State Machine -
//
//  int TwoWireRx(UNS_8 *pRxData, long *pTaskWoken )
//
//  The USART interrupt service routine in serial.c calls this function
//  for every character received by the USART handling the Two Wire
//  ASYNC communications. The TwoWireRx function is passed a pointer
//  to the character just received by the USART. This function returns
//  status that tells the USART ISR whether to keep/store the character
//  or to discard it. This function can also change the char received
//  via the pointer pRxData.
//
//  This function also detects and indicates the reception of the 
//  end of a Two Wire response by sending an ASCII EM character to the
//  FreeRTOS queue having the queue handle xRxStatQueueHandle.
//
//  Returns:
//          TWO_WIRE_RX_DISCARD_CHAR - discard the character
//          TWO_WIRE_RX_KEEP_CHAR - keep/store the character
//
/////////////////////////////////////////////////////////////////////////
extern UNS_32 two_wire_rx_from_isr( UNS_8 *pRxData, signed portBASE_TYPE *pxHigherPriorityTaskWoken )
{
	UNS_32	receive_status;

	UNS_32	rv;


	// Default action
	rv = TWO_WIRE_RX_DISCARD_CHAR;

	switch( sc_state )
	{
		case SC_RX_IDLE_e:

			// if char avail, see if it's a DLE
			if( *pRxData == DLE)
			{
				sc_state = SC_RX_WAIT_STX_e;
			}
			break;

		case SC_RX_WAIT_STX_e:

			if( *pRxData == STX )
			{
				// Now get the message body
				sc_state = SC_RX_MSG_BODY_e;
			}
			else if( *pRxData != DLE )
			{
				sc_state = SC_RX_IDLE_e;
			}
			break;

		case SC_RX_MSG_BODY_e:

			// Was the character a DLE?
			if( *pRxData == DLE )
			{
				// yes, it was a DLE
				sc_state = SC_RX_MSG_BODY_DLE_e;
			}
			else
			{
				// then keep/store the message data byte
				rv = TWO_WIRE_RX_KEEP_CHAR;
			}
			break;

		case SC_RX_MSG_BODY_DLE_e:

			///////////////////////////////////////////////////////////////////////
			//    Last received char was a DLE (not stored or included in the CRC
			///////////////////////////////////////////////////////////////////////

			if( *pRxData == SYN )
			{
				/////////////////////////////////////////////////////
				// Unstuff: treat DLE SYN combo as just DLE received
				/////////////////////////////////////////////////////

				// Store a DLE (for DLE SYN received) - We have replaced the SYN with a DLE
				*pRxData = DLE;

				// keep/store this message data byte
				rv = TWO_WIRE_RX_KEEP_CHAR;

				// Go back to looking for a DLE
				sc_state = SC_RX_MSG_BODY_e;
			}
			else if( *pRxData == EOT )
			{
				/////////////////////////////////////////////////////////////////////
				//  Last received char was a DLE (not stored or included in the CRC
				/////////////////////////////////////////////////////////////////////

				// We got DLE EOT (message terminator)
				sc_state = SC_RX_MSG_COMPLETE_e;

				receive_status = TWO_WIRE_IO_CONTROL_QUEUE_RESP_RECEIVED;

				// Send an event (msg) to the waiting task (two wire driver)
				xQueueSendFromISR( two_wire_io_control.response_queue, &receive_status, pxHigherPriorityTaskWoken );
			}
			break;

		case SC_RX_MSG_COMPLETE_e:
			////////////////////////////////////////////////////////////////
			//    Here we just discard any additional received characters
			////////////////////////////////////////////////////////////////
			break;

		default:
			break;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern void move_response_data_to_appropriate_structure( void )
{
	UNS_32		iii;
	
	BOOL_32		data_checked_ok;
	
	FLOW_COUNT_RESP_s	fcrs;
	
	// ----------
						
	// 2/1/2013 rmd : The response includes the CRC16 at this point. So subtract it out.
	two_wire_io_control.actual_rsp_len -= sizeof(unsigned short);

	switch( two_wire_io_control.op_code )
	{
		// ----------
		
		case OP_DATA_LOOPBK:
			// 2/1/2013 rmd : For the address we sent the message to did we find him in the list? If so
			// can handle the response.
			if( two_wire_io_control.fds.info_ptr != NULL )
			{
				if( two_wire_io_control.actual_rsp_len == two_wire_io_control.lb_data_length )
				{
					data_checked_ok = (true);

					for( iii=0; iii<two_wire_io_control.lb_data_length; iii++ )
					{
						if( two_wire_io_control.rxbuf[ iii ] != two_wire_io_control.lb_data[ iii ] )
						{
							data_checked_ok = (false);
						}
					}
					
					if( data_checked_ok )
					{
						two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH;
					}
				}
				else
				{
					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
				}
			}	

			break;

		// ----------
		
		case OP_SET_SN_REQ:
			// 6/17/2013 rmd : Remember this was sent to address 0. And anytime we set serial numbers
			// the user must follow the activity by a discovery to formally 'find' the decoder. Because
			// it was sent to address 0 the info_ptr and list_ptr will assuredly be NULL. The response
			// is to be an ACK meaning successful.
			if( two_wire_io_control.actual_rsp_len == 1 )
			{
				if( two_wire_io_control.rxbuf[ 0 ] == ACK )
				{
					two_wire_io_control.response_result = TWO_WIRE_RESULT_ACK_DETECTED;
				}	
			}
			else
			{
				// 1/31/2013 rmd : This is an error. The response length is the wrong size.
				two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;

				two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
			}

			break;

		// ----------
		
		case OP_SOL_CTL:
			// 2/1/2013 rmd : For the address we sent the message to did we find him in the list? If so
			// can handle the response.
			if( two_wire_io_control.fds.info_ptr != NULL )
			{
				// 4/16/2013 rmd : The response to the solenoid command is a single status byte.
				if( two_wire_io_control.actual_rsp_len == 1 )
				{
					two_wire_io_control.fds.info_ptr->results_of_last_solenoid_control_command = two_wire_io_control.rxbuf[ 0 ];

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH;
				}
				else
				{
					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
				}
			}	
				
			break;

		// ----------

		case OP_GET_FLOW_CNTS:
			// 2/1/2013 rmd : For the address we sent the message to did we find him in the list? If so
			// can handle the response.
			if( two_wire_io_control.fds.info_ptr != NULL )
			{
				if( two_wire_io_control.actual_rsp_len == sizeof(FLOW_COUNT_RESP_s) )
				{
					// 5/21/2014 rmd : So we add the results to existing accumulated count. And update the 3
					// second value with new value. Remember the 3 second value is for the user to see (display)
					// the latest flow.
					memcpy( &fcrs, (void*)two_wire_io_control.rxbuf, sizeof(FLOW_COUNT_RESP_s) );
					
					// 5/21/2014 rmd : Because Squires sends multi-byte variables with bytes reversed to native
					// ARM.
					swap_32( fcrs.accumulated_ms );
					swap_32( fcrs.accumulated_pulses );
					swap_16( fcrs.latest_5_second_count );

					// ----------
					
					// 5/21/2014 rmd : Note something interesting here! The two-wire bus is actually OFF! If we
					// were to try and take the mutex and it wasn't available for a long time (10's to 200 some
					// ms - an error of some kind) the bus would discharge! Thought I'd note it and use the
					// follow construct allowing only 25ms for the MUTEX to become available.
					if( xSemaphoreTake( flow_count_MUTEX, MS_to_TICKS( 25 ) ) == pdTRUE )
					{
						// 6/5/2014 rmd : Only add to the time if there are accumulated pulses. Otherwise when we
						// go to calculate the gallons there could be a distortion (depending on the timing of the
						// token and more). Check against NULL for safety.
						if( two_wire_io_control.fds.info_ptr != NULL )
						{
							if( fcrs.accumulated_pulses > 0 )
							{
								two_wire_io_control.fds.info_ptr->latest_flow.accumulated_ms += fcrs.accumulated_ms;
		
								two_wire_io_control.fds.info_ptr->latest_flow.accumulated_pulses += fcrs.accumulated_pulses;
							}
								
							// 6/9/2014 rmd : And ALWAYS get the latest 5 second count from the decoder. If it is zero
							// we want to know about that. ZERO is a flow rate! A 0 flow rate and we neeed to show the
							// user that.
							two_wire_io_control.fds.info_ptr->latest_flow.latest_5_second_count = fcrs.latest_5_second_count;
		
							// ----------
							
							// 5/21/2014 rmd : We do have something to send.
							B_SET( two_wire_io_control.fds.info_ptr->send_to_main, SEND_TO_MAIN_BOARD_DECODER_FLOW_COUNT );
						}

						// ----------
						
						xSemaphoreGive( flow_count_MUTEX );
					}
					else
					{
						// 5/22/2014 rmd : So we know if this is happenening.
						alert_message_to_tpmicro_pile( "2-wire: flow mutex unavail!" );
					}
					
					// ----------

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH;
				}
				else
				{
					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
				}
			}	
				
			break;

		// ----------

		case OP_GET_MOISTURE_DATA:
		
			// 2/1/2013 rmd : For the address we sent the message to did we find him in the list? If so
			// can handle the response.
			if( two_wire_io_control.fds.info_ptr != NULL )
			{
				// 1/28/2016 rmd : NOTE - though the DECAGON sensor itself sends variable length responses
				// to the moisture decoder, the decoder always sends a fixed length response to the TPMicro.
				// And that is what we are receiving here. That fixed length response. Some of the content
				// is 0's. But that is okay.
				if( two_wire_io_control.actual_rsp_len == sizeof(MOISTURE_SENSOR_DECODER_RESPONSE) )
				{
					// 1/28/2016 rmd : Protect the moisture string. The comm_mngr task reads this string and we
					// set it. NOTE - something interesting here! The two-wire bus is actually OFF! If we were
					// to try and take the mutex and it wasn't available for a long time (10's to 200 some ms -
					// an error of some kind) the bus would discharge! Thought I'd note it and use the follow
					// construct allowing only 25ms for the MUTEX to become available.
					if( xSemaphoreTake( moisture_reading_MUTEX, MS_to_TICKS( 25 ) ) )
					{
						// 1/27/2016 rmd : Because of the timing between moisture sensor read frequency (once every
						// 10 minutes) and the message rate between the main board (1hz) and the setting of the
						// send_to_main bit we probably do not need a MUTEX. But we have one so am use it.
						memcpy( &(two_wire_io_control.fds.info_ptr->latest_moisture), (void*)two_wire_io_control.rxbuf, sizeof(MOISTURE_SENSOR_DECODER_RESPONSE) );
	
						// ----------
						
						B_SET( two_wire_io_control.fds.info_ptr->send_to_main, SEND_TO_MAIN_BOARD_DECODER_MOISTURE_READING );
						
						two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH;
						
						xSemaphoreGive( moisture_reading_MUTEX );
					}
					else
					{
						// 5/22/2014 rmd : So we know if this is happenening.
						alert_message_to_tpmicro_pile( "2-wire: moisture mutex unavail!" );
					}
				}
				else
				{
					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
				}
			}	
			break;
		
		// ----------
		
		case OP_STAT1_REQ:
			// 2/1/2013 rmd : For the address we sent the message to did we find him in the list? If so
			// can handle the response.
			if( two_wire_io_control.fds.info_ptr != NULL )
			{
				// 4/16/2013 rmd : The response to the solenoid command is a single status byte.
				if( two_wire_io_control.actual_rsp_len == sizeof(STAT1_REQ_RSP_s) )
				{
					memcpy( &two_wire_io_control.fds.info_ptr->stat2_response.sol1_status, (void*)two_wire_io_control.rxbuf, sizeof(STAT1_REQ_RSP_s) );

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH;
				}
				else
				{
					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
				}
			}	
				
			break;

		// ----------
		
		case OP_DISC_CONF:
			// 2/1/2013 rmd : For the address we sent the message to did we find him in the list? If so
			// can handle the response.
			if( (two_wire_io_control.fds.info_ptr != NULL) && (two_wire_io_control.fds.list_ptr != NULL) )
			{
				if( two_wire_io_control.actual_rsp_len == sizeof(ID_REQ_RESP_s) )
				{
					memcpy( &(two_wire_io_control.fds.list_ptr->id_info), (void*)two_wire_io_control.rxbuf, sizeof(ID_REQ_RESP_s) );
	
					swap_16( two_wire_io_control.fds.list_ptr->id_info.fw_vers );
					
					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH;
				}
				else
				{
					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
				}
			}	
			break;

		// ----------

		case OP_STATS_CLEAR:
			// 2/1/2013 rmd : For the address we sent the message to did we find him in the list? If so
			// can handle the response.
			if( two_wire_io_control.fds.info_ptr != NULL )
			{
				if( two_wire_io_control.actual_rsp_len == 1 )
				{
					if( two_wire_io_control.rxbuf[ 0 ] == ACK )
					{
						// 5/7/2013 rmd : Now clear our copy and update the main board too.
						memset( &(two_wire_io_control.fds.info_ptr->decoder_statistics), 0x00, sizeof(DECODER_STATS_s) );
						
						B_SET( two_wire_io_control.fds.info_ptr->send_to_main, SEND_TO_MAIN_BOARD_DECODER_STATISTICS );

						two_wire_io_control.response_result = TWO_WIRE_RESULT_ACK_DETECTED;
					}	
				}
				else
				{
					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;
	
					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
				}
			}	
			break;
			

		// ----------
		
		case OP_STATS_REQ:
			// 2/1/2013 rmd : For the address we sent the message to did we find him in the list? If so
			// can handle the response.
			if( two_wire_io_control.fds.info_ptr != NULL )
			{
				if( two_wire_io_control.actual_rsp_len == sizeof(DECODER_STATS_s) )
				{
					memcpy( &(two_wire_io_control.fds.info_ptr->decoder_statistics), (void*)two_wire_io_control.rxbuf, sizeof(DECODER_STATS_s) );

					// ----------
					
					// 6/13/2013 rmd : Because Squires sends multi-byte variables with bytes reversed to native
					// ARM.
					swap_16( two_wire_io_control.fds.info_ptr->decoder_statistics.temp_maximum );
					swap_16( two_wire_io_control.fds.info_ptr->decoder_statistics.temp_current );

					// ----------
					
					B_SET( two_wire_io_control.fds.info_ptr->send_to_main, SEND_TO_MAIN_BOARD_DECODER_STATISTICS );
					
					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH;
				}
				else
				{
					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
				}
			}	
			break;

		// ----------

		case OP_RSTR_DFLTS:
			// 2/1/2013 rmd : For the address we sent the message to did we find him in the list? If so
			// can handle the response.
			if( (two_wire_io_control.fds.info_ptr != NULL) && (two_wire_io_control.fds.list_ptr != NULL) )
			{
				// 4/16/2013 rmd : The response to the restore command is a single status byte.
				if( two_wire_io_control.actual_rsp_len == 1 )
				{
					// 6/6/2013 rmd : If non-zero is an error.
					if( two_wire_io_control.rxbuf[ 0 ] != 0 )
					{
						decoder_alert_message_to_tpmicro_pile( two_wire_io_control.fds.list_ptr->sn, "Error during RESTORE to defaults!" );
					}
					else
					{
						decoder_alert_message_to_tpmicro_pile( two_wire_io_control.fds.list_ptr->sn, "Sucessful RESTORE to defaults!" );

						two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH;
					}
				}
				else
				{
					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;

					two_wire_io_control.response_result = TWO_WIRE_RESULT_RECEIVED_UNEXPECTED_LENGTH;
				}
			}	
				
			break;
			
		// ----------
		
	}
	
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

