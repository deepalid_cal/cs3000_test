/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_TWO_WIRE_FET_CNTRL_H
#define INC_TWO_WIRE_FET_CNTRL_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	TW_BUS_MODE_TRANSITION_TO_OFF		(0)
#define	TW_BUS_MODE_OFF						(1)
#define	TW_BUS_MODE_TRANSITION_TO_POWER		(2)
#define	TW_BUS_MODE_POWER					(3)
#define	TW_BUS_MODE_TRANSITION_TO_DATA		(4)
#define	TW_BUS_MODE_DATA					(5)


extern volatile UNS_32	__tw_bus_mode;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void initialize_two_wire_fet_control( void );


extern void during_io_switch_and_wait_for_bus_to_enter_power_mode( void );

extern void during_io_switch_and_wait_for_bus_to_enter_data_mode( void );

extern void during_io_turn_off_the_bus_NOW_and_setup_to_transition_to_off_mode( void );


extern void power_up_two_wire_cable( UNS_32 *ptask_watchdog_time_stamp_ptr, BOOL_32 pwait_till_charged );

extern void power_down_two_wire_cable( void );


extern void FUSE_one_hertz_blown_fuse_test( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

