//////////////////////////////////////////////////////////////////////
//
//  File: TwoWireHndlr.h
//
//  Description: Header file for Two Wire Message handler
//
//  $Id: TwoWireHndlr.h,v 1.2 2011/12/13 20:53:43 dsquires Exp $
//
//  $Log: TwoWireHndlr.h,v $
//  Revision 1.2  2011/12/13 20:53:43  dsquires
//  Added trivial test code.
//
//  Revision 1.1  2011/11/15 17:56:46  dsquires
//  New files for Two Wire communications.
//
//////////////////////////////////////////////////////////////////////
/* ---------------------------------------------------------- */

#ifndef INC_TWO_WIRE_TASK_H_
#define INC_TWO_WIRE_TASK_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"

#include	"data_types.h"

#include	"eeprom.h"

#include	"protocolmsgs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/1/2013 rmd : How to size the queue. Well the main board may ask us to turn ON 6
// decoders. And maybe retrieve flow from 2 or 3 POC decoders. And read request current
// measurements from the 6 ON. I don't exactly know yet. But taking a stab at it with 20.
#define TWO_WIRE_TASK_QUEUE_SIZE	(20)

// ----------

#define TWO_WIRE_TASK_QUEUE_COMMAND_control_bus_power						(1)

#define TWO_WIRE_TASK_QUEUE_COMMAND_perform_discovery_process				(2)

#define TWO_WIRE_TASK_QUEUE_COMMAND_clear_statistics_at_all_decoders		(3)

#define TWO_WIRE_TASK_QUEUE_COMMAND_request_statistics_from_all_decoders	(4)

#define TWO_WIRE_TASK_QUEUE_COMMAND_start_all_decoder_loopback_test			(5)

#define TWO_WIRE_TASK_QUEUE_COMMAND_stop_all_decoder_loopback_test			(6)

#define TWO_WIRE_TASK_QUEUE_COMMAND_decoder_solenoid_operation				(7)

#define TWO_WIRE_TASK_QUEUE_COMMAND_receive_stations_ON_from_main			(8)

#define TWO_WIRE_TASK_QUEUE_COMMAND_receive_pocs_ON_from_main				(9)

#define TWO_WIRE_TASK_QUEUE_COMMAND_receive_lights_ON_from_main				(10)

#define TWO_WIRE_TASK_QUEUE_COMMAND_execute_commands_to_decoders			(11)

#define TWO_WIRE_TASK_QUEUE_COMMAND_set_serial_number						(12)

#define TWO_WIRE_TASK_QUEUE_COMMAND_end_special_activation_mode				(13)

#define TWO_WIRE_TASK_QUEUE_COMMAND_ack_cable_excessive_current				(14)

#define TWO_WIRE_TASK_QUEUE_COMMAND_ack_cable_overheated					(15)

// ----------

typedef struct
{
	UNS_32		command;
	
	union
	{
		UNS_8			*ucp;
		
		// 1/29/2013 rmd : Is (true) when request to turn ON.
		BOOL_32			bus_on_or_off;

		TWO_WIRE_DECODER_SOLENOID_OPERATION_STRUCT	decoder_operation;
	};
	
} TWO_WIRE_TASK_QUEUE_STRUCT;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// ----------

// 10/17/2013 rmd : I have observed more than 3 seconds needed to charge the cable when
// loaded with 80 decoders. So I've set this to 6. Remember during this time we inhibit
// command exchanges with the decoders and allow a higher cable current without tripping a
// short.
// 11/5/2013 rmd : UPDATE - I moved the terminal board capacitance BACK to two times 470uF
// on each leg of the cable. Previously I had 1470uF on each leg. So the time to charge a
// fully loaded cable has extended somewhat. It is more conservative to move this out to 8
// seconds.
#define		TWO_WIRE_CABLE_CHARGE_TIME_MS						(8*1000)


// 5/2/2013 rmd : The dwell time on the line from command/response sequence completion to
// the start of the next command. By allowing 300ms between commands we assure ample main
// caps recharge time after a response. It seems to be ample. But I'll tell you seeing the
// solenoid voltage flicker (via a light bulb on the decoder solenoid output) with each
// command response I am guarded against frequent commands. So I've moved the command rate
// to allow a 500ms dwell time between.
#define		TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS					(500)

// 6/5/2013 rmd : The turn ON command is very special. The decoder solenoid caps go through
// a charge sequence that can be as long as 22*100ms + 250ms = 2450ms. Then the TEST,
// PULL-IN, and HOLD phase are accomplished. They are completed within 100ms. So we wait
// 3000ms to know for sure the worst case turn on sequence has completed, before attempting
// the STAT1 command to check the turn ON status.
#define		TWO_WIRE_CABLE_MS_BETWEEN_AFTER_AN_ON_COMMAND		(3000)


#define		TWO_WIRE_PATH_MS_TILL_SHUTDOWN			(5*60*1000)

// ----------

// 5/1/2013 rmd : Once every 5 minutes we check that the decoder state matches the desired
// ON or OFF state. All decoders are checked.
//
// 9/3/2014 rmd : WOW. This was set at once every 5 minutes when we initially deployed to
// Bakersfield on 8/27/2014. I think once every 15 minutes sounds like a much more
// reasonable time frame. What this means is in the unlikely event (hopefully) that a
// decoder doesn't respond we may have an output erounously energized for this 15 minute
// period of time? I think however that is a very remote possiblity.
#define		DECODER_MS_BETWEEN_PERIODIC_STAT1		(15*60*1000)

// ----------

// 5/12/2014 rmd : How often shall we attempt to retrieve the flow sensor data from the POC
// decoders? Lets go with once every 10 seconds and see how that goes.
//
// 1/26/2016 rmd : The once every 10 seconds has worked out very well and has been in place
// now for about one and a half years. I think it is here to stay and is permanent.
#define		DECODER_MS_BETWEEN_FLOW_READING_COMMANDS	(10*1000)

// ----------

// 10/16/2013 rmd : When a decoder valve is turned ON for location purposes it is allowed to
// stay ON for one minute and then automatically turned OFF.
#define		SPECIAL_ACTIVATION_OUTPUT_ON_DURATION		(60*1000)

// ----------

#define		DECODER_STATUS_SEND_DISCOVERED_TO_MAIN_BOARD				(0)

// 4/29/2013 rmd : This occurs if we just can't get a response or an intelligent response
// from the decoder. We try 4 times and eventually flag the decoder a inoperative. The
// intent may be here not to attempt to use this decoder again till the next time the main
// board attempts to turn ON a station for this decoder. When sent for this current reason
// all stations using this decoder are removed from the irri and foal lists. Note - when the
// decoder was declared inoperative we are performing a preventive temporary 2-wire path
// shutdown.
#define		DECODER_STATUS_DECODER_INOPERATIVE							(1)

#define		DECODER_STATUS_SEND_DECODER_INOPERATIVE_TO_MAIN_BOARD		(2)

// ----------

// 4/15/2013 rmd : There is an array of the DECODER_LIST_STRUCT reflecting all discovered
// decoders the TP Micro knows about (learned through the discovery process). This array
// which primarily contains the decoder serial number and type is SAVED to the EE in the TP
// Micro. There is an accompanying array called decoder_info which is NOT saved to the EE.
// The order of the decoders is the same in both. As a matter of fact the index of a decoder
// is the same in both arrays.

typedef struct
{
	// ----------
	
	// 6/27/2013 rmd : These two are sent to the main board after the discovery process has
	// completed.
	ID_REQ_RESP_s			id_info;

	UNS_32					sn;

	// ----------

	// 6/13/2013 rmd : This is a count of the number of missing responses. Or incorrect response
	// lengths. This is not used during control command retries. Those are a counter by output
	// and are used when the voltage is too low or the solenoid output is shorted. This counter
	// is for flat out no response for example.
	UNS_8					communication_failures;

	// ----------
	
	// 6/27/2013 rmd : A status byte to help manage decoder state. And used to signal when
	// certain pieces of information need to be sent to the main board.
	UNS_8					decoder_status;

	// ----------

} TPMICRO_DECODER_LIST_STRUCT;

// ----------

// 5/12/2014 rmd : This bitfield and control is at the OUTPUT LEVEL. Not at the decoder
// level.

// 5/12/2014 rmd : This is the all important bit. Is the output supposed to be ON.
#define		OUTPUT_CONTROL_BIT_IS_ON						(0)

// 5/12/2014 rmd : Used for a logical alogorithm when determining if there has been a COS.
#define		OUTPUT_CONTROL_BIT_WAS_ON						(1)

// 5/12/2014 rmd : And is there a COS which dictates that we need to send a command to
// achieve the desired ON or OFF output state.
#define		OUTPUT_CONTROL_BIT_COS_DETECTED					(2)

#define		OUTPUT_CONTROL_BIT_SEND_ON_COMMAND				(3)

#define		OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND				(4)

// 5/13/2014 rmd : Send the STAT1 command. But for the purpose of verifying the last ON or
// OFF command was successful. If not will try again. The associated retry behavior is
// DIFFERENT than that associated with the PERIODIC stat1 request.
#define		OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1		(5)

#define		OUTPUT_CONTROL_BIT_not_yet_used_6				(6)

#define		OUTPUT_CONTROL_BIT_not_yet_used_7				(7)

// 5/13/2014 rmd : Clear the ON, OFF, and TRANSITIONAL status commands. As you add more
// commands this may need to be updated. This is used when a fault is detected to cease
// further command attempts. Important NOT to clear the IS_ON bit otherwise when the next
// messages from MAIN arrives the COS bit will get set and restart the ON or OFF attempt.
// Use is to AND with the inverse of this mask.
#define		OUTPUT_CONTROL_WIPE_ALL_COMMANDS_MASK			(0x38)
			
// ----------

// 5/12/2014 rmd : This bitfield and control is at the DECODER LEVEL. Not at the output
// level.

// 5/12/2014 rmd : The periodic stat is the lowest level command. Once every 5 minutes we
// query each decoder and compare status to desired output state. And correct if needed. The
// periodic stat requirement however is not a reason to keep the cable required.
//
// 1/26/2016 rmd : These are not necessarily defined in order of priority. They are just bit
// positions. The priority scheme of which command goes first is implemeted via the code
// loop in the execute_commands_to_decoders function.
#define		DECODER_CMDS_BIT_OBTAIN_PERIODIC_STAT1			(0)

#define		DECODER_CMDS_BIT_GET_FLOW_METER_READING			(1)

#define		DECODER_CMDS_BIT_GET_MOISTURE_READING			(2)

#define		DECODER_CMDS_BIT_not_yet_used_3					(3)

#define		DECODER_CMDS_BIT_not_yet_used_4					(4)

#define		DECODER_CMDS_BIT_not_yet_used_5					(5)

#define		DECODER_CMDS_BIT_not_yet_used_6					(6)

#define		DECODER_CMDS_BIT_not_yet_used_7					(7)

// 4/30/2013 rmd : When a short is detected or we have other command failure due to
// unresponsiveness or unexpected responses we want to wipe out all pending commands to
// supress any further communication attempts. Leave the FIRST bit intact otherwise as the
// main board sends in its output state request a COS (change of state) will be picked up
// and the process repeated.
#define		DECODER_CMDS_WIPE_PENDING_COMMANDS_MASK			(0x01)

// ----------

// 5/1/2013 rmd : Defines for the bit positions within the output_status byte.

#define		OUTPUT_STATUS_BIT_SHORTED							(0)

#define		OUTPUT_STATUS_BIT_SEND_SHORT_TO_MAIN				(1)

#define		OUTPUT_STATUS_BIT_VOLTAGE_TOO_LOW					(2)

#define		OUTPUT_STATUS_BIT_SEND_VOLTAGE_TOO_LOW_TO_MAIN		(3)

// ----------

typedef struct
{
	// ----------

	// 4/15/2013 rmd : A bit field that sets what state the solenoid is to be in (as told by the
	// main board message). And which command should be sent to the decoder. We only send the
	// command once. And then periodically verify state by requesting the solenoid state from
	// the decoder.
	UNS_8			control;
	
	// ----------

	// 4/29/2013 rmd : During the turn ON if the the decoder returns the FAILED PWM state 4
	// times in a row the solenoid is declared shorted.
	UNS_8			shorted_count;

	// 4/29/2013 rmd : During the turn ON if the the decoder returns the VOLT_TOO_LOW state 4
	// times in a row we declare a voltage problem.
	UNS_8			low_voltage_count;

	// ----------

	// 5/1/2013 rmd : A bit field which indicates if the output has been delared shorted, or we
	// received unexpected responses, and if the status needs to be sent to the main board. For
	// example in the case of a short.
	UNS_8			output_status;
	
	// ----------

// 5/1/2013 rmd : By packing we actually reduce the code !! and buy ourselves 1000 bytes of
// BSS SRAM space.
} __attribute__((packed)) OUTPUT_CONTROL_STRUCT;


// ----------
	
#define		SEND_TO_MAIN_BOARD_DECODER_STATISTICS			(0)

#define		SEND_TO_MAIN_BOARD_DECODER_STAT2_RESPONSE		(1)

#define		SEND_TO_MAIN_BOARD_DECODER_COMM_STATS			(2)

#define		SEND_TO_MAIN_BOARD_DECODER_FLOW_COUNT			(3)

#define		SEND_TO_MAIN_BOARD_DECODER_MOISTURE_READING		(4)

// ----------

// 4/29/2013 rmd : The dynamic array of decoder state and status since TPMicro code boot.
// Not saved to EE. Zeroed upon startup by C startup code. BE VERY AWARE OF THE IMPACT TO
// BSS SRAM SPACE WHEN ADDING TO THIS ARRAY!!!!!!!!! There are 128 of these.
typedef struct
{
	// ----------

	// 6/27/2013 rmd : The pending decoder commands that have been requested. These commands are
	// either routine periodic status commands or special user request commands. The requested
	// commands held in the OUTPUT_CONTROL_STRUCT take priority over these and those output
	// commands will execute first. Note: these commands are not solenoid output specific
	// commands, they are for example the loopback command, or the periodic STAT1 command.
	UNS_8						decoder_cmds;
	
	// ----------
	
	// 6/27/2013 rmd : These are bit fields that manage the individual solenoid control
	// commands. One for each output the decoder has.

	OUTPUT_CONTROL_STRUCT		output_A;

	OUTPUT_CONTROL_STRUCT		output_B;

	OUTPUT_CONTROL_STRUCT		output_C;

	OUTPUT_CONTROL_STRUCT		output_D;
	
	// ----------
	
	// 2/1/2013 rmd : Sent to the main board at the request of the main board.
	DECODER_STATS_s			decoder_statistics;

	// ----------
	
	// 2/1/2013 rmd : Sent to the main board after retrieval from a decoder.
	STAT2_REQ_RSP_s			stat2_response;

	// ----------

	FLOW_COUNT_RESP_s		latest_flow;

	// ----------
	
	MOISTURE_SENSOR_DECODER_RESPONSE	latest_moisture;

	// ----------
	
	// 2/1/2013 rmd : Kept at the TP Micro and sent with the above statistics. When we send we
	// send as many as will fit into the 512 byte max message. It may take several secodns to
	// update the data for all the decoders on the bus.
	TWO_WIRE_COMM_STATS_PER_DECODER_STRUCT	comm_stats;

	// ----------

	// 5/12/2014 rmd : Periodically, once every 15 minutes, we check on a decoders state. To
	// make sure it matches what we expect as far as outputs being ON or OFF. As new decoder
	// types come into existence we may use a different status command returning appropriate
	// health data for the decoder type.
	//
	// 1/26/2016 rmd : RANGE needs to be up to 15 minutes. Hence an UNS_32. (0 to 900,000)
	UNS_32					ms_since_last_periodic_status_check;								

	// 1/26/2016 rmd : We are concerned with structure size. The range on this however has to be
	// able to count up to say 15 minutes worth of ms (900,000). So we need the 4 byte size. One
	// could create another whole scheme where we count TENTHS of a second instead of 1000's.
	// And obtain several bytes here. But I'll pass on that for now.
	//
	// 1/26/2016 rmd : RANGE needs to be up to 15 minutes. Hence an UNS_32. (0 to 900,000)
	UNS_32					ms_since_last_moisture_reading_acquired;								

	// 1/26/2016 rmd : RANGE needs to be up to 10 seconds. Hence an UNS_16. (0 to 10,000)
	UNS_16					ms_since_last_flow_reading_acquired;								

	// ----------

	// 5/22/2014 rmd : A bit field used to flag data that needs to be sent to the main board.
	// This includes items like the decoder health statistics and the flow meter readings.
	UNS_8					send_to_main;

	// ----------

	// 4/26/2013 rmd : No matter what output the command was for the results are posted here for
	// evaluation immediately after the command was sent and the response received. To see which
	// command we should be sending next. Only valid for the last SOL_CTL command sent.
	UNS_8					results_of_last_solenoid_control_command;
	
	// ----------


// 5/1/2013 rmd : By packing we add a bit of code (less than 100 bytes) but buy ourselves
// 600 bytes of BSS SRAM space. The M3 / GCC combination does indeed generate assembly
// instructions to deal with the variable misalignments that may/will result from the use of
// this packed attribute. Therefore avoiding the expected 'hard fault' when access is made
// to any misaligned multi-byte variable!
} __attribute__((packed)) TPMICRO_DECODER_INFO_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 10/29/2013 rmd : The different states needed to manage the cable excessive current and
// solenoid output excessive and no current indications.
#define		TWO_WIRE_CABLE_STATUS_XMISSION_STATE_normal				(0)

#define		TWO_WIRE_CABLE_STATUS_XMISSION_STATE_signal_main		(1)

#define		TWO_WIRE_CABLE_STATUS_XMISSION_STATE_waiting_for_ack	(2)

// ----------

#define		DECODER_FAULT_STATE_normal			(1)

#define		DECODER_FAULT_STATE_discharging		(2)

// 1/27/2014 rmd : The cable will remain OFF for 15 seconds to ensure all decoders discharge
// and active solenoids close.
#define		DECODER_FAULT_CABLE_DISCHARGE_MS	(15000)

// ----------

typedef struct
{
	// ----------

	// 10/29/2013 rmd : NOTE - this entire structure is set to 0 on startup. Both by the C
	// startup code and then again explicitly in the beginning of the two_wire task. I did the
	// explicit init cause I feel better seeing such as a confirmation of the starting point.
	// Though technically it isn't necessary.

	// ----------

	// 4/15/2013 rmd : Is (true) when the two-wire cable is powered.
	BOOL_32			cable_is_powered;

	BOOL_32			cable_is_done_charging;
	
	xTimerHandle	cable_charge_timer;
	
	// ----------

	// 1/27/2014 rmd : When a decoder fault is detected (by missing message responses) the cable
	// is powered down as a preventative measure to ensure all solenoid outputs are indeed OFF.
	// I suppose this could have an undesired effect if a normally open MV was wired to a POC
	// decoder and there was a MLB in force. During the cable power down the MV would open! But
	// I think we need this mechanism to cover the instance when a decoder fails to respond.
	// Suppose the solenoid was active?
	//
	// 1/27/2014 rmd : NOTE - there is still a missing piece! We are not notifying the main
	// board of the decoder fault. So that the two respective outputs would be removed from the
	// irrigation list!!!!! TODO TODO
	UNS_32			decoder_fault_state;

	UNS_32			decoder_fault_remaining_discharge_ms;

	// ----------

	// 10/25/2013 rmd : A number based upon the cable excessive current limit and the present
	// FET temperature.
	UNS_32			cable_percent_of_maximum_current;
	
	// ----------

	// 10/29/2013 rmd : Initialized to 0 on startup. So that would be the normal xmission state.
	// for each. For the case of the short the flag is cleared when we receive the ACK from the
	// main board.
	UNS_32			cable_short_xmission_state;

	// 10/29/2013 rmd : Initialized to 0 on startup. So that would be the normal xmission state.
	// In the overheated case the ACK coming back doesn't clear overheated flag but does trigger
	// a wipe of all pending decoder commands. When the ACK comes back we know by then the
	// stations ON down the cable have been removed.
	UNS_32			cable_over_heated_xmission_state;

	// 11/13/2013 ajv : Initialized to 0 on startup (the normal transmission
	// state). When the overheated state changes back to cooled off, we need to
	// notify the mainboard so it can clear the FOAL and IRRI side flags (used
	// for display purposes and to prevent irrigation).
	UNS_32			cable_cooled_off_xmission_state;

		
	// 11/7/2013 rmd : This variable is not functionally required. But does force the cable
	// powered off due to excessive current alert line to be generated. Without this variable it
	// is a race between the 400ms command period and the message from the main board carrying
	// the cable short ACK - which would clear the excessive current. If cleared then when
	// powered down says so due to inactivity - not due to a short.
	BOOL_32			clear_excessive_current;

	// ----------

	// 10/15/2013 rmd : Support to activate a solenoid output. BYPASSING the normal list of
	// those ON from the main board official irri side irrigation list. This mechanism is use to
	// pop ON a valve before it has been assigned to a station. As an aid during setup to
	// visually locate the valve so the user may assign a station number that makes sense.
	BOOL_32			special_activation_mode;

	xTimerHandle	special_activation_timer;
	
	// ----------

	// 10/16/2013 rmd : A flag to supress incoming message processing from the main board. The
	// discovery process is a real beast. While it is underway we are only giving 20ms slots to
	// all the other tasks. And only during the transmission of commands to the decoders. There
	// are some other OS delays as well but those are even smaller. As a result I think certain
	// processes need to halt while this process is underway. And this variable controls that.
	// There are other two_wire network sequences which are also time consuming. For instance
	// acquiring statistics from all decoders. So we use this then too.
	BOOL_32			task_is_busy;
	
	// ----------

	// 10/16/2013 rmd : Discovery process details.
	BOOL_32			discovery_in_process;
	
	// 2/1/2013 rmd : As opposed to scanning for 'undiscovered' decoders. This means we reset
	// all decoders and cleared the entire list. And then went about learning all decoders out
	// there. This may be the only reliable way. Because a decoder added to the bus may have
	// already been discovered at some other time and told to shut up ie issued a discovery
	// confirmation command. This value is included with the list of serial numbers sent to the
	// main board. And when (true) instructs the main board to restart its list as opposed to
	// adding to its list.
	BOOL_32			full_discovery;

	UNS_32			discovered_count;
	
	BOOL_32			send_main_the_discovery_process_results;

	// ----------

} TWO_WIRE_BUS_INFO_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern TPMICRO_DECODER_LIST_STRUCT		decoder_list[ MAX_DECODERS_IN_A_BOX ];

extern TPMICRO_DECODER_INFO_STRUCT		decoder_info[ MAX_DECODERS_IN_A_BOX ];

extern TWO_WIRE_BUS_INFO_STRUCT			two_wire_bus_info;


typedef struct
{
	TPMICRO_DECODER_LIST_STRUCT		*list_ptr;

	TPMICRO_DECODER_INFO_STRUCT		*info_ptr;
	
} FIND_DECODER_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void find_decoder( UNS_32 pserial_num, FIND_DECODER_STRUCT *rv );


extern UNS_32 number_of_newly_discovered_decoder_serial_nums_to_send_to_the_mb( void );

extern UNS_32 number_of_decoder_statistics_to_send_to_the_mb( void );


extern void save_decoder_list_to_ee( void );


extern void TWO_WIRE_activity_timer_handler( xTimerHandle xTimer );

extern void TWO_WIRE_special_activation_timer_handler( xTimerHandle xTimer );

extern void TWO_WIRE_cable_charge_timer_handler( xTimerHandle xTimer );


extern void two_wire_task( void *pvParameters );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*TWOWIREHANDLER_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

