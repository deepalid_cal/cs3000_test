/////////////////////////////////////////////////////////
//
//    File: TwoWireRx.h 
//
//    Description:   header file for two wire receive 
//                   protocol parser
//
//    $Id: TwoWireRx.h,v 1.3 2011/12/01 21:00:13 dsquires Exp $
//    $Log:
/////////////////////////////////////////////////////////

#ifndef TWO_WIRE_RECEIVE_H_
#define TWO_WIRE_RECEIVE_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Return code to keep/store received character
#define TWO_WIRE_RX_KEEP_CHAR       1

// Return code to discard received character
#define TWO_WIRE_RX_DISCARD_CHAR    0

////////////////////////////////////////////
//
//  SERIAL COMM (SC) STATE MACHINE STATES
//
////////////////////////////////////////////
typedef enum
{
    ////////////////////
    // Receive states
    ////////////////////
    SC_RX_IDLE_e                = 0,    // Looking for a start of message (DLE)
    SC_RX_WAIT_STX_e            = 1,    // Got DLE, now looking for STX
    SC_RX_MSG_BODY_e            = 2,    // Got DLE SOH, now handling msg body (looking for DLE)
    SC_RX_MSG_BODY_DLE_e        = 3,    // Got DLE, now looking for SYN or EOT
    SC_RX_MSG_COMPLETE_e        = 4,    // Complete message received

} SC_STATES_e;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

void tw_receive_init( void );


extern BOOL_32 bMsgHasGoodCrc( UNS_8 *pMsg, UNS_8 MsgLen );


extern UNS_32 two_wire_rx_from_isr( UNS_8 *pRxData, signed portBASE_TYPE *pxHigherPriorityTaskWoken );


extern void move_response_data_to_appropriate_structure( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*TWOWIRERX_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

