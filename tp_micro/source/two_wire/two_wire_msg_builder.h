//////////////////////////////////////////////////////////////////////
//
//  File: TwoWireMsgBldr.h
//
//  Description: Header file for Two Wire (command) message builder
//
//  $Id: TwoWireMsgBldr.h,v 1.2 2011/12/13 20:53:13 dsquires Exp $
//
//  $Log: TwoWireMsgBldr.h,v $
//  Revision 1.2  2011/12/13 20:53:13  dsquires
//  Added two wire message building functions.
//
//  Revision 1.1  2011/11/15 17:56:46  dsquires
//  New files for Two Wire communications.
//
//
//////////////////////////////////////////////////////////////////////
/* ---------------------------------------------------------- */

#ifndef INC_TWO_WIRE_MSG_BUILDER_H_
#define INC_TWO_WIRE_MSG_BUILDER_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void build_discovery_reset_msg( void );

extern void build_enquiry_msg( UNS_32 serial_num, UNS_32 mask_len );

extern void build_discovery_confirmation_msg( UNS_32 serial_num );


extern void build_solenoid_control( UNS_32 serial_num, UNS_32 solenoid, BOOL_32 ctrl );

extern void build_flow_pulse_count_request_msg( UNS_32 serial_num );

extern void build_moisture_reading_request_msg( UNS_32 serial_num );


extern void build_statistics_clear_msg( UNS_32 serial_num );

extern void build_statistics_request_msg( UNS_32 serial_num );

extern void build_status1_request_msg( UNS_32 serial_num );

extern void build_status2_request_msg( UNS_32 serial_num );

extern void build_restore_user_parameters_to_defaults( UNS_32 serial_num );


extern void build_set_serial_number_msg( UNS_32 pnew_serial_num );


extern void build_data_loopback_msg( UNS_32 serial_num, UNS_8 *pData, UNS_32 byte_cnt );



// Function declarations
extern void bld_cur_meas_req_msg( UNS_32 serial_num, UNS_8 seqnum );

extern void bldDecoderRstMsg( UNS_32 serial_num );

extern void bldIdReqMsg( UNS_32 serial_num );

extern void bldPutParmsMsg( UNS_32 serial_num, UNS_8 region, UNS_8 *pParms, UNS_8 size );

extern void bldGetParmsMsg( UNS_32 serial_num, UNS_8 region );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

