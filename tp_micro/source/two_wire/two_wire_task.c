/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"two_wire_task.h"

#include	"two_wire_io.h"

#include	"two_wire_uart.h"

#include	"crc16.h"

#include	"two_wire_msg_builder.h"

#include	"two_wire_fet_control.h"

#include	"two_wire_discovery_process.h"

#include	"tpmicro_comm_mngr.h"

#include	"irrigation_manager.h"

#include	"eeprom_map.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	MAX_NUM_OF_DECODER_COMM_FAILURES		(4)

#define	MAX_NUM_OF_SHORTS_FOR_A_SHORT			(4)

#define	MAX_NUM_OF_LOW_VOLTAGE_ATTEMPTS			(4)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/16/2014 rmd : Just a diagnostic variable to understand program flow.
UNS_32	turn_ons_blocked;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/15/2013 rmd : The decoder info and list variables used to be one large array. But had
// to split them up to manage the size of what is being saved to the 8Kbyte TP Micro EE. The
// decoder_list variable is saved to the EE while the info is not.
TPMICRO_DECODER_LIST_STRUCT		decoder_list[ MAX_DECODERS_IN_A_BOX ];

// 5/12/2014 rmd : This variable, the decoder_info, is not stored in the EE. It is a SRAM
// variable and therefore by definition is fully zeroed on startup.
TPMICRO_DECODER_INFO_STRUCT		decoder_info[ MAX_DECODERS_IN_A_BOX ];


TWO_WIRE_BUS_INFO_STRUCT		two_wire_bus_info;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static const char * const decoder_sol_ctrl_responses[] =
{
"invalid output",
"op undefined",
"sol already ON",
"sol already OFF",
"low line voltage",
};

static const char * const decoder_sol_states[] =
{
"OFF",
"CAP CHG P",
"CAP CHG C",
"COIL TEST",
"PULL IN",
"HOLDING",
"SHORT"
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void find_decoder( UNS_32 pserial_num, FIND_DECODER_STRUCT *rv )
{
	// 1/31/2013 rmd : If the decoder cannot be found an alert message is sent and the two
	// pointers within the structure will be NULL. Before any use of the pointers we must check
	// for NULL.

	// ----------

	UNS_32		ddd;
	
	BOOL_32		found;
	
	found = (false);

	// ----------

	// 4/15/2013 rmd : So the caller knows not to use when these are NULL.
	rv->list_ptr = NULL;

	rv->info_ptr = NULL;

	// ----------

	// 2/5/2013 rmd : A serial number of 0 is invalid. Yet could fool us as that is the default
	// serial number in the array. But is just that the default. Not a decoder.
	if( pserial_num != 0 )
	{
		for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
		{
			if( decoder_list[ ddd ].sn == pserial_num )
			{
				rv->list_ptr = &decoder_list[ ddd ];

				rv->info_ptr = &decoder_info[ ddd ];
				
				found = (true);
				
				break;
			}
		}
	}

	if( !found )
	{
		alert_message_to_tpmicro_pile( "decoder not found" );
	}
}

/* ---------------------------------------------------------- */
extern UNS_32 number_of_newly_discovered_decoder_serial_nums_to_send_to_the_mb( void )
{
	UNS_32	ddd, rv;
	
	rv = 0;
	
	for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
	{
		if( B_IS_SET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_SEND_DISCOVERED_TO_MAIN_BOARD ) )
		{
			rv += 1;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 number_of_decoder_statistics_to_send_to_the_mb( void )
{
	UNS_32	ddd, rv;
	
	rv = 0;
	
	for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
	{
		if( B_IS_SET( decoder_info[ ddd ].send_to_main, SEND_TO_MAIN_BOARD_DECODER_STATISTICS ) )
		{
			rv += 1;
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern void save_decoder_list_to_ee( void )
{
	EEPROM_MESSAGE_STRUCT	eeqm;
	
	eeqm.cmd = EE_ACTION_WRITE;
	
	eeqm.EE_address = EE_LOCATION_DECODER_LIST;
	
	eeqm.RAM_address = (UNS_8*)&decoder_list;
	
	eeqm.length = EE_SIZE_DECODER_LIST;
	
	if( xQueueSendToBack( g_xEEPromMsgQueue, &eeqm, portNO_DELAY ) != pdTRUE )
	{
		flag_error( BIT_TPME_queue_full );
	}
}

/* ---------------------------------------------------------- */
static BOOL_32 __bump_and_test_if_solenoid_output_short_limit_reached( UNS_32 const pserial_number, UNS_32 const pwhich_solenoid, OUTPUT_CONTROL_STRUCT *poutput_control_ptr )
{
	BOOL_32	rv;
	
	rv = (false);

	if( poutput_control_ptr->shorted_count < MAX_NUM_OF_SHORTS_FOR_A_SHORT )
	{
		poutput_control_ptr->shorted_count += 1;
	}
	
	if( poutput_control_ptr->shorted_count >= MAX_NUM_OF_SHORTS_FOR_A_SHORT )
	{
		decoder_output_alert_message_to_tpmicro_pile( pserial_number, pwhich_solenoid, "output SHORTED" );
		
		// ----------

		// 4/30/2013 rmd : Don't change the first bit which determines if the output is to be on or
		// off. Else when the main board next sends its requested state a COS will be picked up and
		// we'll try to turn on the output again and repeat the short.
		poutput_control_ptr->control &= ~OUTPUT_CONTROL_WIPE_ALL_COMMANDS_MASK;

		// 4/30/2013 rmd : This output will not be part of the periodic state query until the next
		// main board requested COS is detected.
		B_SET( poutput_control_ptr->output_status, OUTPUT_STATUS_BIT_SHORTED );

		B_SET( poutput_control_ptr->output_status, OUTPUT_STATUS_BIT_SEND_SHORT_TO_MAIN );
		
		// ----------

		rv = (true);  // indicate yes past the limit and should stop trying
	}

	return( rv );
}	
	
/* ---------------------------------------------------------- */
static BOOL_32 bump_and_test_if_solenoid_voltage_too_low_limit_reached( UNS_32 pserial_number, OUTPUT_CONTROL_STRUCT *poutput_control_ptr )
{
	BOOL_32	rv;
	
	rv = (false);

	if( poutput_control_ptr->low_voltage_count < MAX_NUM_OF_LOW_VOLTAGE_ATTEMPTS )
	{
		poutput_control_ptr->low_voltage_count += 1;
	}
	
	if( poutput_control_ptr->low_voltage_count >= MAX_NUM_OF_LOW_VOLTAGE_ATTEMPTS )
	{
		decoder_alert_message_to_tpmicro_pile( pserial_number, "LOW CABLE VOLTAGE at this decoder" );
		
		// ----------

		// 4/30/2013 rmd : This output will not be part of the periodic state query until the next
		// main board requested COS is detected.
		B_SET( poutput_control_ptr->output_status, OUTPUT_STATUS_BIT_VOLTAGE_TOO_LOW );

		B_SET( poutput_control_ptr->output_status, OUTPUT_STATUS_BIT_SEND_VOLTAGE_TOO_LOW_TO_MAIN );
		
		// ----------

		rv = (true);  // indicate yes past the limit and should stop trying
	}

	return( rv );
}	
	
/* ---------------------------------------------------------- */
static BOOL_32 __bump_and_test_if_decoder_inoperative( UNS_32 pindex )
{
	BOOL_32	rv;
	
	rv = (false);

	// ----------

	if( decoder_list[ pindex ].communication_failures < MAX_NUM_OF_DECODER_COMM_FAILURES )
	{
		decoder_list[ pindex ].communication_failures += 1;
	}

	// ----------

	if( decoder_list[ pindex ].communication_failures >= MAX_NUM_OF_DECODER_COMM_FAILURES )
	{
		// ----------

		// 5/29/2013 rmd : Sanity check. The logic is such that we shouldn't be here if the decoder
		// is already inoperative. All the commands get cancelled and supressed.
		if( B_IS_SET( decoder_list[ pindex ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) )
		{
			decoder_alert_message_to_tpmicro_pile( decoder_list[ pindex ].sn, "decoder ALREADY inoperative!" );
		}
		else
		{
			// 5/29/2013 rmd : The expected message.
			decoder_alert_message_to_tpmicro_pile( decoder_list[ pindex ].sn, "decoder flagged INOPERATIVE" );
		}
		
		// ----------

		// 5/29/2013 rmd : Wipe all pending commands for this decoder. For ALL outputs. But leave
		// the ON/OFF state bit intact. So that we don't pick up a change of state and clear the
		// decoder inoperative bit when the next message from the main board comes carrying the
		// stations that are to be ON. Which if this failed while trying to turn ON would happen.
		// After this decoder is removed from the foal and irri lists we will subsequently pick up
		// that the decoder is to be OFF. And consider that (in the tp micro) as change of state
		// (COS). We do not however send commands to decoders flagged as INOP. So the OFF command
		// will be squashed when the attempt to send is made. And that is not an error.
		decoder_info[ pindex ].output_A.control &= ~OUTPUT_CONTROL_WIPE_ALL_COMMANDS_MASK;
		decoder_info[ pindex ].output_B.control &= ~OUTPUT_CONTROL_WIPE_ALL_COMMANDS_MASK;
		decoder_info[ pindex ].output_C.control &= ~OUTPUT_CONTROL_WIPE_ALL_COMMANDS_MASK;
		decoder_info[ pindex ].output_D.control &= ~OUTPUT_CONTROL_WIPE_ALL_COMMANDS_MASK;
		
		// 10/1/2014 rmd : And wipe any pending decoder level commands.
		decoder_info[ pindex ].decoder_cmds = 0x00;

		// ----------

		// 4/30/2013 rmd : This will cause a temporary shutdown of the 2W path in order to assure
		// ourselves this decoder is not sitting there with its a solenoid output operating.
		// Removing power will quench any active outputs.
		two_wire_bus_info.decoder_fault_state = DECODER_FAULT_STATE_discharging;

		two_wire_bus_info.decoder_fault_remaining_discharge_ms = DECODER_FAULT_CABLE_DISCHARGE_MS;

		// ----------

		B_SET( decoder_list[ pindex ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE );

		B_SET( decoder_list[ pindex ].decoder_status, DECODER_STATUS_SEND_DECODER_INOPERATIVE_TO_MAIN_BOARD );

		// ----------

		// 5/29/2013 rmd : So we'll save the INOPERATIVE state within the decoder list. Not sure why
		// right now but am doing so. I thought about what happens if the whole network of many
		// decoders is down, that we would post to the ee queue again and again. Once for each
		// decoder. Well that is indeed what happens but the postings are well spaced out. Perhaps 2
		// seconds apart. So the EE task has plenty of time to actually read the queue and perform
		// the save without the queue overflowing. And I'm not concerned about the reduntant saving
		// wearing out the EE. This error won't happen often (hopefully) and the EE is good for 1
		// million erase/write cycles!
		save_decoder_list_to_ee();

		// ----------

		rv = (true);  // indicate yes past the limit and should stop communicating to

		// ----------
	}

	return( rv );
}	
	
/* ---------------------------------------------------------- */
static void __wipe_all_pending_commands_and_states( void )
{
	UNS_32	ddd;
	
	for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
	{
		// 10/31/2013 rmd : This applies to ALL decoders on the cable. Regardless of decoder type.
		// This function is called as part of cable short or cable overheated processing.
		memset( &decoder_info[ ddd ].output_A, 0x00, sizeof(OUTPUT_CONTROL_STRUCT) );
		memset( &decoder_info[ ddd ].output_B, 0x00, sizeof(OUTPUT_CONTROL_STRUCT) );
		memset( &decoder_info[ ddd ].output_C, 0x00, sizeof(OUTPUT_CONTROL_STRUCT) );
		memset( &decoder_info[ ddd ].output_D, 0x00, sizeof(OUTPUT_CONTROL_STRUCT) );

		memset( &decoder_info[ ddd ].decoder_cmds, 0x00, sizeof(UNS_8) );
	}
}

/* ---------------------------------------------------------- */
static void process_periodic_stat1_results_for_an_output( UNS_32 pddd, UNS_32 pwhich_solenoid )
{
	char	str_32[ 32 ];
	
	UNS_8	received_status;
	
	OUTPUT_CONTROL_STRUCT	*ocs_ptr;

	// ----------

	if( pwhich_solenoid == DECODER_OUTPUT_A_BLACK )
	{
		received_status = decoder_info[ pddd ].stat2_response.sol1_status;
		
		ocs_ptr = &decoder_info[ pddd ].output_A;
	}
	else
	{
		received_status = decoder_info[ pddd ].stat2_response.sol2_status;
		
		ocs_ptr = &decoder_info[ pddd ].output_B;
	}

	if( B_IS_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_IS_ON ) )
	{
		// 6/13/2013 rmd : If we've already been though the turn on sequence and declared an error
		// then there is nothing to do. Note the error flags are cleared at the next COS detection.
		if( !B_IS_SET( ocs_ptr->output_status, OUTPUT_STATUS_BIT_SHORTED ) && !B_IS_SET( ocs_ptr->output_status, OUTPUT_STATUS_BIT_VOLTAGE_TOO_LOW ) )
		{
			// 6/13/2013 rmd : So there was no repetitive attempts to get the solenoid ON that resulted
			// in a short or low voltage declaration. So it should be HOLDING. If it is nothing to
			// report or do.
			if( received_status != SOL_HOLD_e )
			{
				// 8/15/2013 rmd : I believe the only way to arrive at this state is the solenoid cap
				// voltage was seen too low during the holding phase. That minimum is about 10V. Quite low.
				decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, "Periodic STAT1 output not HOLDING!" );
		
				// ----------
				
				// 8/15/2013 rmd : The next two lines can be commented out if they prove to be a nuisance.
				// But should RARELY show! Takes a very special combination of cable conditions to get here.
				snprintf( str_32, sizeof(str_32), "and PWM State is: %u", received_status );
				
				alert_message_to_tpmicro_pile( str_32 );
										
				// ----------
		
				// 5/2/2013 rmd : Unexpected state. We don't have a comm error (cause we executed the
				// periodic stat1) so the state should be HOLD. Try again via the COS mechanism.
				B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_COS_DETECTED );
			}
		}
		else
		{
			// 6/13/2013 rmd : Then I would expect the solenoid pwm state to be either OFF or EXCESSIVE
			// CURRENT. If not we should issue a change of state to cause us to try the ON command
			// again. This should never actually happen. It is an error but apparently the decoder is
			// still communicating.
			if( (received_status != SOL_OFF_e) && (received_status != SOL_EXCESSIVE_CURRENT_e) )
			{
				decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, "Solenoid Status UNEXPLAINED!" );

				B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_COS_DETECTED );
			}
		}
	
	}
	else
	{
		// 6/13/2013 rmd : THE OUTPUT IS TO BE OFF. Only 2 states are acceptable. OFF or EXCESSIVE
		// CURRENT. If in either of those two states let 'er lay. If not issue a COS to cause the
		// OFF command process to repeat.
		if( (received_status != SOL_OFF_e) && (received_status != SOL_EXCESSIVE_CURRENT_e) )
		{
			decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, "Periodic STAT1 output not OFF!" );
			
			B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_COS_DETECTED );
		}
	}
}

/* ---------------------------------------------------------- */
static void process_transitional_stat1_ON_results_for_an_output( UNS_32 pddd, UNS_32 pwhich_solenoid )
{
	char	str_32[ 32 ];
	
	UNS_8	received_status;
	
	OUTPUT_CONTROL_STRUCT	*ocs_ptr;

	// ----------

	if( pwhich_solenoid == DECODER_OUTPUT_A_BLACK )
	{
		received_status = decoder_info[ pddd ].stat2_response.sol1_status;
		
		ocs_ptr = &decoder_info[ pddd ].output_A;
	}
	else
	{
		received_status = decoder_info[ pddd ].stat2_response.sol2_status;
		
		ocs_ptr = &decoder_info[ pddd ].output_B;
	}

	// ----------
	
	if( received_status == SOL_HOLD_e )
	{
		decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, "ON command PWM hold state - OK" );
	}
	else
	{
		// 5/13/2014 rmd : Other than HOLD state is an error in the making.
		if( (received_status >= SOL_OFF_e) && (received_status <= SOL_EXCESSIVE_CURRENT_e) )
		{
			snprintf( str_32, sizeof(str_32), "STAT1: sol state %s", decoder_sol_states[ received_status ] );
			
			decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, str_32 );
		}
		else
		{
			decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, "STAT1: unk sol state!" );
		}
		
		// 4/30/2013 rmd : Did the status indicate an excessive current condition for the solenoid?
		if( received_status == SOL_EXCESSIVE_CURRENT_e )
		{
			if( !__bump_and_test_if_solenoid_output_short_limit_reached( decoder_list[ pddd ].sn, pwhich_solenoid, ocs_ptr ) )
			{
				// 6/13/2013 rmd : If we haven't reached the limit try again with the ON command. Remember
				// the OBTAIN_TRANSITIONAL_STAT1 bit is still set. And we leave it set so the ON command is
				// again followed by the STAT1 command.
				B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );
			}
		}
		else
		{
			// 6/13/2013 rmd : We should not see any other state here. I consider that a BUG. Shouldn't
			// happen given the code design. Because the other errors should've been caught with the
			// actual ON command. Not the TRANSITIONAL_STAT1.
			if( !__bump_and_test_if_decoder_inoperative( pddd ) )
			{
				// 6/13/2013 rmd : Well if we aren't at the limit try the ON command again.
				B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );
			}
		}
	}
}

/* ---------------------------------------------------------- */
static void process_transitional_stat1_OFF_results_for_an_output( UNS_32 pddd, UNS_32 pwhich_solenoid )
{
	char	str_32[ 32 ];
	
	UNS_8	received_status;
	
	OUTPUT_CONTROL_STRUCT	*ocs_ptr;

	// ----------

	if( pwhich_solenoid == DECODER_OUTPUT_A_BLACK )
	{
		received_status = decoder_info[ pddd ].stat2_response.sol1_status;
		
		ocs_ptr = &decoder_info[ pddd ].output_A;
	}
	else
	{
		received_status = decoder_info[ pddd ].stat2_response.sol2_status;
		
		ocs_ptr = &decoder_info[ pddd ].output_B;
	}

	// ----------

	if( received_status == SOL_OFF_e )
	{
		decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, "OFF command PWM off state - OK" );
	}
	else
	if( received_status == SOL_EXCESSIVE_CURRENT_e )
	{
		// 5/13/2014 rmd : This state is okay too. Because by definition in the decoder the output
		// is OFF.
		decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, "OFF command PWM shorted state - OK" );
	}
	else
	{
		// 5/1/2013 rmd : Notice the OFF and FAILED not included in this comparison.
		if( (received_status > SOL_OFF_e) && (received_status < SOL_EXCESSIVE_CURRENT_e) )
		{
			snprintf( str_32, sizeof(str_32), "STAT1 Command: sol state %s", decoder_sol_states[ received_status ] );
			
			decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, str_32 );
		}
		else
		{
			decoder_output_alert_message_to_tpmicro_pile( decoder_list[ pddd ].sn, pwhich_solenoid, "STAT1: sol state unknown!" );
		}
		
		// 6/13/2013 rmd : We should not see any other state here. I consider that a BUG. Shouldn't
		// happen given the code design.
		if( !__bump_and_test_if_decoder_inoperative( pddd ) )
		{
			// 6/13/2013 rmd : Well if we aren't at the limit, and apparently the solenoid may not be
			// OFF, try the OFF command sequence again. Eventually we'll hit the limit and stop (4
			// trys).
			B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND );
		}
	}
}

/* ---------------------------------------------------------- */
static void execute_stat1_command( UNS_32 const pddd )
{
	UNS_32	serial_number;
	
	serial_number = decoder_list[ pddd ].sn;
	
	// 5/2/2013 rmd : The routine status check could be performed on an inoperative decoder.
	if( B_IS_SET( decoder_list[ pddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) )
	{
		decoder_alert_message_to_tpmicro_pile( serial_number, "STAT1 attempt on inoperative decoder!" );
	}
	else
	{
		build_status1_request_msg( serial_number );
		
		// 1/29/2013 rmd : Make it happen.
		perform_two_wire_io();
		
		// 4/26/2013 rmd : If we are not in the correct state we'll attempt another turn ON/OFF
		// command - repeating just as if there was a change of state detected.
		if( two_wire_io_control.response_result == TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH )
		{
			if( B_IS_SET( decoder_info[ pddd ].output_A.control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 ) )
			{
				if( B_IS_SET( decoder_info[ pddd ].output_A.control, OUTPUT_CONTROL_BIT_IS_ON ) )
				{
					process_transitional_stat1_ON_results_for_an_output( pddd, DECODER_OUTPUT_A_BLACK );
				}
				else
				{
					process_transitional_stat1_OFF_results_for_an_output( pddd, DECODER_OUTPUT_A_BLACK );
				}

				// 5/13/2014 rmd : In any case, regardless of a positive result or negative result, the
				// transitional stat request bit is to be cleared. The design is such that these are the
				// HIGHEST priority command request. If the bit is set it will do it first. If we want to
				// issue another ON command this bit MUST be cleared. At the conclusion of the ON command we
				// set this bit to trigger the TRANSITIONAL STAT to execute as the following command.
				B_UNSET( decoder_info[ pddd ].output_A.control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 );
			}
			else
			if( B_IS_SET( decoder_info[ pddd ].output_B.control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 ) )
			{
				if( B_IS_SET( decoder_info[ pddd ].output_B.control, OUTPUT_CONTROL_BIT_IS_ON ) )
				{
					process_transitional_stat1_ON_results_for_an_output( pddd, DECODER_OUTPUT_B_ORANGE );
				}
				else
				{
					process_transitional_stat1_OFF_results_for_an_output( pddd, DECODER_OUTPUT_B_ORANGE );
				}

				B_UNSET( decoder_info[ pddd ].output_B.control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 );
			}
			else
			if( B_IS_SET( decoder_info[ pddd ].decoder_cmds, DECODER_CMDS_BIT_OBTAIN_PERIODIC_STAT1 ) )
			{
				process_periodic_stat1_results_for_an_output( pddd, DECODER_OUTPUT_A_BLACK );

				process_periodic_stat1_results_for_an_output( pddd, DECODER_OUTPUT_B_ORANGE );

				// 5/13/2014 rmd : Well we did it. And regardless of the results (positive or negative) we
				// clear this bit and restart the periodic status timer.
				B_UNSET( decoder_info[ pddd ].decoder_cmds, DECODER_CMDS_BIT_OBTAIN_PERIODIC_STAT1 );
				
				decoder_info[ pddd ].ms_since_last_periodic_status_check = 0;
			}
		}
		else
		{
			decoder_alert_message_to_tpmicro_pile( serial_number, "Periodic STAT1: missing response" );
			
			// 5/29/2013 rmd : So the periodic stat does not attempt again and again without success
			// bump the communication failure count and test if reached the limit.
			__bump_and_test_if_decoder_inoperative( pddd );
		}

	}  // of if not an error condition
}

/* ---------------------------------------------------------- */
static void execute_FLOW_command( UNS_32 const pddd )
{
	UNS_32	serial_number;
	
	serial_number = decoder_list[ pddd ].sn;
	
	// 5/2/2013 rmd : Are we trying this on an inoperative decoder?
	if( B_IS_SET( decoder_list[ pddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) )
	{
		decoder_alert_message_to_tpmicro_pile( serial_number, "FLOW attempt on inop decoder!" );
	}
	else
	{
		build_flow_pulse_count_request_msg( serial_number );
		
		// 1/29/2013 rmd : Make it happen.
		perform_two_wire_io();
		
		// 4/26/2013 rmd : If we are not in the correct state we'll attempt another turn ON/OFF
		// command - repeating just as if there was a change of state detected.
		if( two_wire_io_control.response_result == TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH )
		{
			B_UNSET( decoder_info[ pddd ].decoder_cmds, DECODER_CMDS_BIT_GET_FLOW_METER_READING );
		}
		else
		{
			decoder_alert_message_to_tpmicro_pile( serial_number, "Flow Count: missing response" );
			
			// 5/29/2013 rmd : So the command does not attempt again and again without success bump the
			// communication failure count and test if reached the limit.
			// 5/21/2014 rmd : NOTE - once it hits the limit here no way to clear the INOPERATIVE state
			// except by either a discovery or the POC receives a MV output COS (change of state).
			__bump_and_test_if_decoder_inoperative( pddd );
		}

	}  // of if not an error condition
}

/* ---------------------------------------------------------- */
static void execute_MOISTURE_command( UNS_32 const pddd )
{
	UNS_32	serial_number;
	
	serial_number = decoder_list[ pddd ].sn;
	
	// 5/2/2013 rmd : Are we trying this on an inoperative decoder?
	if( B_IS_SET( decoder_list[ pddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) )
	{
		decoder_alert_message_to_tpmicro_pile( serial_number, "MOIS attempt on inop decoder!" );
	}
	else
	{
		build_moisture_reading_request_msg( serial_number );
		
		// 1/29/2013 rmd : Make it happen.
		perform_two_wire_io();
		
		// 4/26/2013 rmd : If we are not in the correct state we'll attempt another turn ON/OFF
		// command - repeating just as if there was a change of state detected.
		if( two_wire_io_control.response_result == TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH )
		{
			B_UNSET( decoder_info[ pddd ].decoder_cmds, DECODER_CMDS_BIT_GET_MOISTURE_READING );
		}
		else
		{
			decoder_alert_message_to_tpmicro_pile( serial_number, "Moisture Reading: missing response" );
			
			// 2/22/2016 rmd : I think we should SUPPRESS the decoder inoperative flag for the case of a
			// missing moisture sensor reading. If we were to allow it to be set in this case what would
			// be the logic that would UNSET it? So I have decided that in this case it could not be set
			// for failures. We should however clear the command attempt. Otherwise it would try and try
			// and try. NOTE - over at the CS3000 main board the master check the timestamp of the most
			// recent moisture reading. And if too old will not use it. TOO OLD might be 30 minutes.
			//
			// COMMENT OUT THE FOLLOWING
			// __bump_and_test_if_decoder_inoperative( pddd );
			// AND REPLACE WITH CLEARING THE COMMAND BIT INSTEAD
			B_UNSET( decoder_info[ pddd ].decoder_cmds, DECODER_CMDS_BIT_GET_MOISTURE_READING );
		}

	}  // of if not an error condition
}

/* ---------------------------------------------------------- */
static void execute_ON_command( UNS_32 const pddd, UNS_32 const pwhich_solenoid )
{
	OUTPUT_CONTROL_STRUCT	*ocs_ptr;

	UNS_32					serial_number;
	
	// ----------
	
	serial_number = decoder_list[ pddd ].sn;

	if( pwhich_solenoid == DECODER_OUTPUT_A_BLACK )
	{
		ocs_ptr = &decoder_info[ pddd ].output_A;
	}
	else
	{
		ocs_ptr = &decoder_info[ pddd ].output_B;
	}
	
	// ----------

	if( !B_IS_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND ) )
	{
		decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "ON Command unexpdly not set!" );
	}
	else
	{
		if( B_IS_SET( ocs_ptr->output_status, OUTPUT_STATUS_BIT_SHORTED ) ||
							B_IS_SET( ocs_ptr->output_status, OUTPUT_STATUS_BIT_VOLTAGE_TOO_LOW ) ||
												B_IS_SET( decoder_list[ pddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) )
		{
			decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "ON rqst with error state!" );
			
			B_UNSET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );  // cancel the request!
		}
		else
		{
			build_solenoid_control( serial_number, pwhich_solenoid, (true) );
			
			// 1/29/2013 rmd : Make it happen.
			perform_two_wire_io();
			
			if( two_wire_io_control.response_result == TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH )
			{
				if( decoder_info[ pddd ].results_of_last_solenoid_control_command == RSP_STATUS_OK )
				{
					decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "ON command resp OK" );
					
					// 5/13/2014 rmd : Clear the ON command and set the request for the high priority
					// TRANSITIONAL STAT command. That bit cannot be set earlier otherwise the TRANSITIONAL STAT
					// command would go before the ON command.
					B_UNSET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );

					B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 );
				}
				else
				{
					if( decoder_info[ pddd ].results_of_last_solenoid_control_command == RSP_SOL_IS_ALREADY_ON )
					{
						decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "ON response already on" );

						// 5/13/2014 rmd : In this case we'll move ahead to the transitional stat command.
						B_UNSET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );

						B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 );
					}
					else
					if( decoder_info[ pddd ].results_of_last_solenoid_control_command == RSP_HBRG_VCAP_TOO_LOW )
					{
						decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "ON response voltage too low" );

						// 5/13/2014 rmd : If not past the limit leave the ON command bit set. And do not request
						// the transition stat. If it is past the limit clear the ON command bit to end the attempts
						// and again do not request the transitional stat.
						if( bump_and_test_if_solenoid_voltage_too_low_limit_reached( serial_number, ocs_ptr ) )
						{
							B_UNSET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );
						}
					}
					else
					{
						// 6/13/2013 rmd : All other response are unexpected and should not happen. Alert and bump
						// the decoder inoperative error so we have a termination mechanism. I consider errors that
						// fall through to here BUGS. 
						decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "UNEXPECTED response to ON command!" );

						__bump_and_test_if_decoder_inoperative( pddd );
						
						// 5/13/2014 rmd : NOTE - if not flagged as inoperative the ON command bit is still set and
						// will try again.
					}
				}
			}
			else
			{
				decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "ON command missing response" );
				
				__bump_and_test_if_decoder_inoperative( pddd );
				
				// 5/13/2014 rmd : NOTE - if not flagged as inoperative the ON command bit is still set and
				// will try again.
			}
		}  // of if not an error condition
	}  // of asking to send ON command
}

/* ---------------------------------------------------------- */
static void execute_OFF_command( UNS_32 const pddd, UNS_32 const pwhich_solenoid )
{
	UNS_32					serial_number;
	
	OUTPUT_CONTROL_STRUCT	*ocs_ptr;

	// ----------
	
	serial_number = decoder_list[ pddd ].sn;

	if( pwhich_solenoid == DECODER_OUTPUT_A_BLACK )
	{
		ocs_ptr = &decoder_info[ pddd ].output_A;
	}
	else
	{
		ocs_ptr = &decoder_info[ pddd ].output_B;
	}
	
	// ----------

	if( !B_IS_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND ) )
	{
		decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "OFF Command unexpdly not set!" );
	}
	else
	{
		// 6/13/2013 rmd : We will be sending the OFF command even if the output is shorted or the
		// voltage was too low to turn ON the output in the first place. However if marked as
		// INOPERATIVE we suppress the command. That would cause us to once again trigger the
		// decoder INOP process of sending to the main board. And that is not needed.
		if( B_IS_SET( decoder_list[ pddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) )
		{
			// 10/1/2014 rmd : Here is the scenario under which this happens. A decoder is asked to
			// energize one of its outputs. If the communication fails the decoder is flagged as INOP.
			// However the state (ON or OFF) is left intact (see __bump_and_test_if_decoder_inoperative
			// function). While the foal master is receiving and processing the INOP structure, working
			// towards removing the stations(s) from the lists, it likely will send another message to
			// the tpmicro asking for this valve to be ON. If we cleared the bit to be ON the tpmicro
			// would COS it and try to turn ON again. Well after it is finally removed from the IRRI
			// list the TPMICRO picks up that this valve is to be off and the COS bit is set. But we do
			// not clear the INOP setting. This is ONLY done when a new request to turn ON the valve
			// comes in. Otherwise every 15 minutes we would try a STAT1 command that may fail. So
			// anyway we end up here trying to process an OFF command for a decoder marked INOP. And
			// this is not an error. Just the way it is.
			decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "OFF to INOP dropped - okay." );
			
			B_UNSET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND );  // cancel the request!
		}
		else
		{
			build_solenoid_control( serial_number, pwhich_solenoid, (false) );
			
			// 1/29/2013 rmd : Make it happen.
			perform_two_wire_io();
			
			if( two_wire_io_control.response_result == TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH )
			{
				if( decoder_info[ pddd ].results_of_last_solenoid_control_command == RSP_STATUS_OK )
				{
					decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "OFF command resp OK" );
					
					// 5/13/2014 rmd : Clear the OFF command and set the request for the high priority
					// TRANSITIONAL STAT command. That bit cannot be set earlier otherwise the TRANSITIONAL STAT
					// command would go before the OFF command.
					B_UNSET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND );

					B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 );
				}
				else
				{
					if( decoder_info[ pddd ].results_of_last_solenoid_control_command == RSP_SOL_IS_ALREADY_OFF )
					{
						decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "OFF command already off" );

						// 5/13/2014 rmd : In this case we'll move ahead to the transitional stat command.
						B_UNSET( ocs_ptr->control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND );

						B_SET( ocs_ptr->control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 );
					}
					else
					{
						// 6/13/2013 rmd : All other response are unexpected and should not happen. Alert and bump
						// the decoder inoperative error so we have a termination mechanism. I consider errors that
						// fall through to here BUGS. By not clearing the bit the OFF command will try up to 4 times
						// total.
						decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "UNEXPECTED response to OFF command!" );

						__bump_and_test_if_decoder_inoperative( pddd );

						// 6/13/2013 rmd : The SEND_OFF_COMMAND bit is still set. So this command will be tried
						// again until the limit is reached if needed.
					}
				}
			}
			else
			{
				decoder_output_alert_message_to_tpmicro_pile( serial_number, pwhich_solenoid, "OFF command missing response" );
				
				__bump_and_test_if_decoder_inoperative( pddd );
				
				// 6/13/2013 rmd : The SEND_OFF_COMMAND bit is still set. So this command will be tried
				// again until the limit is reached if needed.
			}
		}  // of if not an error condition
	}  // of to send the OFF command
}

/* ---------------------------------------------------------- */
static void check_if_a_periodic_status_command_is_needed( UNS_32 const pdcheck )
{
	// 5/2/2013 rmd : If the decoder is inoperative or either of the outputs has a communication
	// problem this decoder does not participate in the periodic status check. And if the bus is
	// OFF don't do this ... cause the act of sending a status command will re-power the bus.
	// And we want it off unless we have valves to run or flow readings to get.
	if( two_wire_bus_info.cable_is_powered && !B_IS_SET( decoder_list[ pdcheck ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) )
	{
		// 5/1/2013 rmd : Assume no priority command will be picked up for this decoder. So bump the
		// last command sent count. This is to happen to EACH decoder in the list.
		if( decoder_info[ pdcheck ].ms_since_last_periodic_status_check < DECODER_MS_BETWEEN_PERIODIC_STAT1 )
		{
			decoder_info[ pdcheck ].ms_since_last_periodic_status_check += TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS;
		}
		
		// 5/1/2013 rmd : And if the count has been long enough set up to perform the periodic
		// STAT1.
		if( decoder_info[ pdcheck ].ms_since_last_periodic_status_check >= DECODER_MS_BETWEEN_PERIODIC_STAT1 )
		{
			// 5/1/2013 rmd : Status request returns the pwm state for both outputs and we will chack
			// that status against the state for both outputs.
			B_SET( decoder_info[ pdcheck ].decoder_cmds, DECODER_CMDS_BIT_OBTAIN_PERIODIC_STAT1 );
			
			// 5/2/2013 rmd : And start the count over again.
			decoder_info[ pdcheck ].ms_since_last_periodic_status_check = 0;
		}
	}
	else
	{
		// 5/2/2013 rmd : Clear the request for a periodic status in case one was left pending and
		// the bus just powered down. The pending command sequence would re-power the bus, only to
		// have it power back down again 300ms later upon next entry to this function. Note - only
		// the A output bit is used for the periodic status.
		B_UNSET( decoder_info[ pdcheck ].decoder_cmds, DECODER_CMDS_BIT_OBTAIN_PERIODIC_STAT1 );
		
		decoder_info[ pdcheck ].ms_since_last_periodic_status_check = 0;
	}
}

/* ---------------------------------------------------------- */
static void check_if_an_ON_OFF_command_is_needed( UNS_32 const pdcheck )
{
	// 5/12/2014 rmd : All decoders (two station, poc, and moisture) have an A output. Of course
	// the A output on a POC decoder is the MV (which is irrelevant to this logic).
	if( B_IS_SET( decoder_info[ pdcheck ].output_A.control, OUTPUT_CONTROL_BIT_COS_DETECTED ) )
	{
		if( B_IS_SET( decoder_info[ pdcheck ].output_A.control, OUTPUT_CONTROL_BIT_IS_ON ) )
		{
			B_SET( decoder_info[ pdcheck ].output_A.control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );
			B_UNSET( decoder_info[ pdcheck ].output_A.control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND );
		}
		else
		{
			B_UNSET( decoder_info[ pdcheck ].output_A.control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );
			B_SET( decoder_info[ pdcheck ].output_A.control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND );
		}
		
		// ----------

		// 4/26/2013 rmd : Okay the ON command or OFF command is latched in there. And will stay so
		// until the command is sucessfully sent. So clear the detected change of state bit.
		B_UNSET( decoder_info[ pdcheck ].output_A.control, OUTPUT_CONTROL_BIT_COS_DETECTED );
		
		// 5/13/2014 rmd : And MUST clear the high priority TRANSITION STAT bit else the transition
		// stat command would go before the ON command.
		B_UNSET( decoder_info[ pdcheck ].output_A.control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 );

		// ----------

		// 4/29/2013 rmd : And because we are beginning a COS command sequence reset the retry
		// counters.
		decoder_info[ pdcheck ].output_A.low_voltage_count = 0;

		decoder_info[ pdcheck ].output_A.shorted_count = 0;
		
		// 5/1/2013 rmd : Wipe all prior output problem states and/or the need to report such to the
		// main board.
		decoder_info[ pdcheck ].output_A.output_status = 0;
	}
	
	// ----------
	
	// 5/12/2014 rmd : When it comes to the B output do not include the POC decoders or moisture
	// decoders.
	if( (decoder_list[ pdcheck ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION) && B_IS_SET( decoder_info[ pdcheck ].output_B.control, OUTPUT_CONTROL_BIT_COS_DETECTED ) )
	{
		if( B_IS_SET( decoder_info[ pdcheck ].output_B.control, OUTPUT_CONTROL_BIT_IS_ON ) )
		{
			B_SET( decoder_info[ pdcheck ].output_B.control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );
			B_UNSET( decoder_info[ pdcheck ].output_B.control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND );
		}
		else
		{
			B_UNSET( decoder_info[ pdcheck ].output_B.control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND );
			B_SET( decoder_info[ pdcheck ].output_B.control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND );
		}
		
		// ----------

		// 4/26/2013 rmd : Okay the ON command or OFF command is latched in there. And will stay so
		// until the command is sucessfully sent. So clear the detected change of state bit.
		B_UNSET( decoder_info[ pdcheck ].output_B.control, OUTPUT_CONTROL_BIT_COS_DETECTED );
		
		// 5/13/2014 rmd : And MUST clear the high priority TRANSITION STAT bit else the transition
		// stat command would go before the ON command.
		B_UNSET( decoder_info[ pdcheck ].output_B.control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 );
		
		// ----------

		// 4/29/2013 rmd : And because we are beginning a COS command sequence reset the retry
		// counters.
		decoder_info[ pdcheck ].output_B.low_voltage_count = 0;

		decoder_info[ pdcheck ].output_B.shorted_count = 0;
		
		// 5/1/2013 rmd : Wipe all prior output problem states and/or the need to report such to the
		// main board.
		decoder_info[ pdcheck ].output_B.output_status = 0;
	}
}	

/* ---------------------------------------------------------- */
static void check_if_a_flow_reading_command_is_needed( UNS_32 const pdcheck )
{
	// 5/12/2014 rmd : Only for POC decoders. However if the decoder is flagged as inoperative
	// we should skip this.
	if( (decoder_list[ pdcheck ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC) && !B_IS_SET( decoder_list[ pdcheck ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) )
	{
		// 5/1/2013 rmd : Assume no priority command will be picked up for this decoder. So bump the
		// last command sent count. This is to happen to EACH decoder in the list.
		if( decoder_info[ pdcheck ].ms_since_last_flow_reading_acquired < DECODER_MS_BETWEEN_FLOW_READING_COMMANDS )
		{
			decoder_info[ pdcheck ].ms_since_last_flow_reading_acquired += TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS;
		}
		
		// 5/12/2014 rmd : Is it time?
		if( decoder_info[ pdcheck ].ms_since_last_flow_reading_acquired >= DECODER_MS_BETWEEN_FLOW_READING_COMMANDS )
		{
			// 5/1/2013 rmd : Status request returns the pwm state for both outputs and we will chack
			// that status against the state for both outputs.
			B_SET( decoder_info[ pdcheck ].decoder_cmds, DECODER_CMDS_BIT_GET_FLOW_METER_READING );
			
			// 5/2/2013 rmd : And start the count over again.
			decoder_info[ pdcheck ].ms_since_last_flow_reading_acquired = 0;
		}
	}
	else
	{
		// 5/12/2014 rmd : Shouldn't be set. Keep cleaned.
		B_UNSET( decoder_info[ pdcheck ].decoder_cmds, DECODER_CMDS_BIT_GET_FLOW_METER_READING );
		
		decoder_info[ pdcheck ].ms_since_last_flow_reading_acquired = 0;
	}
}

/* ---------------------------------------------------------- */
static void check_if_a_moisture_reading_command_is_needed( UNS_32 const pdcheck )
{
	// 5/12/2014 rmd : Only for POC decoders. However if the decoder is flagged as inoperative
	// we should skip this.
	if( (decoder_list[ pdcheck ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE) && !B_IS_SET( decoder_list[ pdcheck ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) )
	{
		// 5/1/2013 rmd : Assume no priority command will be picked up for this decoder. So bump the
		// last command sent count. This is to happen to EACH decoder in the list.
		if( decoder_info[ pdcheck ].ms_since_last_moisture_reading_acquired < TPMICRO_MS_BETWEEN_GETTING_MOISTURE_READING_FROM_DECODERS )
		{
			decoder_info[ pdcheck ].ms_since_last_moisture_reading_acquired += TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS;
		}
		
		// 5/12/2014 rmd : Is it time?
		if( decoder_info[ pdcheck ].ms_since_last_moisture_reading_acquired >= TPMICRO_MS_BETWEEN_GETTING_MOISTURE_READING_FROM_DECODERS )
		{
			// 5/1/2013 rmd : Status request returns the pwm state for both outputs and we will chack
			// that status against the state for both outputs.
			B_SET( decoder_info[ pdcheck ].decoder_cmds, DECODER_CMDS_BIT_GET_MOISTURE_READING );
			
			// 5/2/2013 rmd : And start the count over again.
			decoder_info[ pdcheck ].ms_since_last_moisture_reading_acquired = 0;
		}
	}
	else
	{
		// 5/12/2014 rmd : Shouldn't be set. Keep cleaned.
		B_UNSET( decoder_info[ pdcheck ].decoder_cmds, DECODER_CMDS_BIT_GET_MOISTURE_READING );
		
		decoder_info[ pdcheck ].ms_since_last_moisture_reading_acquired = 0;
	}
}

/* ---------------------------------------------------------- */
static void execute_commands_to_decoders( UNS_32 *ptask_watchdog_time_stamp_ptr )
{
	// 5/9/2014 rmd : Executed 500ms after completion of the last command. In the context of the
	// two-wire task.
	
	// ----------

	// 4/18/2013 rmd : All STATIC so we pick up where we left off.
	static UNS_32		ddd = 0;

	static UNS_32		doutput = DECODER_OUTPUT_A_BLACK;

	static UNS_32		remaining_ms_of_cable_power = 0;
	
	static BOOL_32		one_was_ON = (false);

	static BOOL_32		command_to_execute = (false);

	static BOOL_32		decoders_present_dictate_an_energized_cable = (false);

	UNS_32				dcheck;
	
	BOOL_32				found_an_OFF_command, found_an_ON_command, found_a_FLOW_command, found_a_TRANSITION_STAT_command, found_a_PERIODIC_STAT_command, found_a_MOISTURE_command;
	
	char				str_32[ 32 ];
	
	// 5/14/2013 rmd : A sanity test variable. We should never see the STAT1 more than 6 ON at a
	// time. If so we aren't issuing commands in the right order. That is OFF's before ON's.
	UNS_32				number_of_decoder_OUTPUTS_ON;

	// 6/4/2013 rmd : Certain commands take longer to complete. Therefore the next command must
	// be properly spaced from the prior command. The default spacing is 300ms. One that takes a
	// long time to complete is a solenoid turn on sequence. That takes 3 seconds.
	UNS_32				time_to_next_command_ms;

	// ----------

	// 6/4/2013 rmd : Set to the default 500ms.
	time_to_next_command_ms = TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS;
	
	// ----------

	// 4/17/2013 rmd : A MUTEX may not even be necessary since all manipulation and use of the
	// decoder_info solenoid control is performed within a single task. The 'two_wire_task'.
	if( xSemaphoreTake( decoder_control_MUTEX, 0 ) != pdTRUE )
	{
		turn_ons_blocked += 1;
		
		snprintf( str_32, 32, "Blocked again %u", turn_ons_blocked );
		
		alert_message_to_tpmicro_pile( str_32);

		// 4/17/2013 rmd : This construct is to see if we care ever held up by the taking of the
		// MUTEX. We should not ever be as only this task does the taking and giving. And bottom
		// line is we must have the MUTEX. So now the portMAX_DELAY attempt.
		xSemaphoreTake( decoder_control_MUTEX, portMAX_DELAY );
	}
	
	// ----------

	// 11/7/2013 rmd : If there is not an excessive current condition keep the flag to clear
	// that condition also cleared. So it is not ready to wipe the excessive current before the
	// ACK arrives.
	if( !adc_readings.two_wire_cable_excessive_current )
	{
		two_wire_bus_info.clear_excessive_current = (false);
	}
	
	// ----------

	if( adc_readings.two_wire_cable_excessive_current || adc_readings.two_wire_terminal_over_heated )
	{
		if( two_wire_bus_info.cable_is_powered )
		{
			if( adc_readings.two_wire_cable_excessive_current )
			{
				// 9/9/2013 rmd : Remember we already have an alert where the excessive current in detected.
				// This alert, and the others here could simply be replaced with a single cable powered down
				// alert with a specific reason. The reason is the prior alert line.
				alert_message_to_tpmicro_pile( "Two Wire Cable powered off : excessive current!" );
			}
			else
			if( adc_readings.two_wire_terminal_over_heated )
			{
				alert_message_to_tpmicro_pile( "Two Wire Cable powered off : terminal overheated!" );
			}
		}

		// 5/2/2013 rmd : Turn OFF the two wire bus. Power it down!
		power_down_two_wire_cable();

		// 10/30/2013 rmd : Ensure there are no residual pending decoder commands when the excessive
		// current or over_heated state is cleared.
		__wipe_all_pending_commands_and_states();
		
		// ----------

		// And reset the flag to clear.
		if( two_wire_bus_info.clear_excessive_current )
		{
			adc_readings.two_wire_cable_excessive_current = (false);
		}
	}
	else
	if( two_wire_bus_info.decoder_fault_state != DECODER_FAULT_STATE_normal )
	{
		// ----------

		// 5/2/2013 rmd : Turn OFF the two wire bus. Power it down!
		power_down_two_wire_cable();

		// 1/27/2014 rmd : HEY HEY TODO TODO!!! Uncomment this line when we are transmitting to the
		// main board the decoder INOP information. If we clear these states here WITHOUT removing
		// the stations from the irrigation list when the cable powers back ON the decoder will try
		// again. If it is really broken it will do it over and over again. And if there is a cable
		// open a whole BUNCH of them could do it over and over again.
		//
		// 1/28/2014 rmd : AHHH but NOTE - if we don't do this (clear the states that is) then when
		// the cable is free to re-power NOTHING will turn back. Because of the way the logic below
		// works. Someday this logic may be re-done but for now it is this way. Especially when we
		// add in the POC decoder which will cause the cable to be powered all the time and
		// therefore we should always be sending the periodic stat commands.
		__wipe_all_pending_commands_and_states();
		
		// ----------

		if( two_wire_bus_info.decoder_fault_remaining_discharge_ms >= TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS )
		{
			two_wire_bus_info.decoder_fault_remaining_discharge_ms -= TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS;
		}
		else
		{
			// 1/27/2014 rmd : Okay we're done. Back to normal so the cable can recharge and resume
			// commands as normal.
			two_wire_bus_info.decoder_fault_state = DECODER_FAULT_STATE_normal;
		}
	}
	else
	{
		// ----------
	
		// 4/26/2013 rmd : Pass through the decoder list looking for a COS bit set. This means we
		// should direct our attention to that decoder. We do so by requesting the appropriate
		// command be sent and by changing the ddd value to this decoder.
	
		// ----------
	
		// 5/2/2013 rmd : If one is ON we keep the cable powered for more time (another 5 minutes).
		// So when any station is ON we keep bumping ahead by 5 minutes. The end result is when the
		// station turns off the cable remains powered for another 5 minutes.
		if( one_was_ON || command_to_execute || decoders_present_dictate_an_energized_cable )
		{
			remaining_ms_of_cable_power = TWO_WIRE_PATH_MS_TILL_SHUTDOWN;
		}
		else
		if( remaining_ms_of_cable_power >= TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS )
		{
			// 5/17/2013 rmd : We subtract this amount because this is how often this function runs
			// (approximately!). I say approximately because recognize the wait after an ON command is
			// 3000ms. So that 3000ms period is counted as only 300ms the way this subtraction works.
			// But that is okay. The time to turn off is not critical.
			remaining_ms_of_cable_power -= TWO_WIRE_CABLE_MS_BETWEEN_COMMANDS;
		}
		else
		{
			remaining_ms_of_cable_power = 0;
		}
		
	
		if( remaining_ms_of_cable_power == 0 )
		{
			if( two_wire_bus_info.cable_is_powered )
			{
				alert_message_to_tpmicro_pile( "Two Wire powered off : inactivity" );
			}
	
			// 5/2/2013 rmd : Okay so we have counted down. Turn OFF the two wire bus. Power it down!
			power_down_two_wire_cable();
		}
	
		// ----------

		// 5/6/2013 rmd : We need to execute the OFF's first before the ON's. If we don't do this we
		// could have more valves ON than the electrical cable loading design supports!
		found_a_TRANSITION_STAT_command = (false);

		found_an_OFF_command = (false);

		found_an_ON_command = (false);

		found_a_FLOW_command = (false);

		found_a_MOISTURE_command = (false);

		found_a_PERIODIC_STAT_command = (false);
		

		command_to_execute = (false);
		
		one_was_ON = (false);
	
		decoders_present_dictate_an_energized_cable = (false);
		

		number_of_decoder_OUTPUTS_ON = 0;
		

		for( dcheck=0; dcheck<MAX_DECODERS_IN_A_BOX; dcheck++ )
		{
			// 5/9/2014 rmd : When we do a discovery the decoder list is zeroed and populated
			// sequentially as the decoders are found. I don't mean the serial numbers are in order
			// (though they are in some kind of order due to the discovery processs logic) - what I mean
			// is the list is populated starting at index 0. The first slot with a NULL serial number
			// indicates the end of the list.
			if( !decoder_list[ dcheck ].sn )
			{
				break;
			}
			
			// ----------

			// 5/16/2013 rmd : Our sanity check accumulator. To make sure commands are being issued in
			// the correct order. But make an exception for decoder that are marked as INOPERATIVE.
			// Check this scenario out: all working fine, we turn on an output, and then the decoder is
			// disconnected from the cable, now can no longer communicate so output status remains at
			// HOLD even though decoder effectively not present. So don't count these. Just leads to an
			// unecessary warning.
			//
			// 5/27/2015 rmd : Well ... as it turns out we are not doing a good job of strictly limiting
			// the number of solenoid outputs being turned ON. Technically a MV output should count as 1
			// of the 6 outputs but we are not limiting them that way. We only count station outputs in
			// the limit of 6. So only include station outputs here otherwise we'll see the "too many
			// on" alert.
			if( !B_IS_SET( decoder_list[ dcheck ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) && 
							((decoder_list[ dcheck ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION) || (decoder_list[ dcheck ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE)) )
			{
				if( decoder_info[ dcheck ].stat2_response.sol1_status == SOL_HOLD_e )
				{
					number_of_decoder_OUTPUTS_ON += 1;
				}

				if( decoder_info[ dcheck ].stat2_response.sol2_status == SOL_HOLD_e )
				{
					number_of_decoder_OUTPUTS_ON += 1;
				}
			}
			
			// ----------

			// 5/2/2013 rmd : If something is ON we allow 5 more minutes.
			if( B_IS_SET( decoder_info[ dcheck ].output_A.control, OUTPUT_CONTROL_BIT_IS_ON ) || B_IS_SET( decoder_info[ dcheck ].output_B.control, OUTPUT_CONTROL_BIT_IS_ON ) )
			{
				// 5/2/2013 rmd : If one is ON we keep the path powered for more time (another 5 minutes).
				// So when any station is ON we keep bumping ahead by 5 minutes. The end result is when the
				// station turns off the cable remains powered for another 5 minutes.
				one_was_ON = (true);
			}
			
			// 2/22/2016 rmd : Test if we have a POC or MOISTURE SENSOR decoder on the cable. If so need
			// to keep cable energized.
			if( !B_IS_SET( decoder_list[ dcheck ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE ) &&
					( (decoder_list[ dcheck ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE) || (decoder_list[ dcheck ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC) ) )
			{
				decoders_present_dictate_an_energized_cable = (true);
			}
			
			// ----------
				
			// 5/12/2014 rmd : Set the bits indicating the commands to send.
			
			check_if_a_periodic_status_command_is_needed( dcheck );

			check_if_an_ON_OFF_command_is_needed( dcheck );

			check_if_a_flow_reading_command_is_needed( dcheck );

			check_if_a_moisture_reading_command_is_needed( dcheck );
			
			// ----------

			// 5/12/2014 rmd : Now see if any commands need to be sent. Capturing the first OFF command
			// to send as the highest priority to do. Followed by any ON commands (if there are no OFF
			// commands to send). Followed by any FLOW commands. And finally if nothing else to do get
			// the status if its bit is set.
			
			// 5/12/2014 rmd : NOTE - this is carefully constructed such that all the OFF commands are
			// executed prior to any ON commands. Intentional so that we do not exceed cable IR drops
			// which can be significant at extreme cable lengths.
			
			// 1/26/2016 rmd : The TRANSITIONAL_STAT commands take palce after an ON or OFF command is
			// sent. They verify the on/off state transition took place properly. And are considered
			// part 2 of an ON or OFF command. So in a sense they are an ON or OFF command. And
			// therefore are a very high priority command.
			
			if( !found_a_TRANSITION_STAT_command )
			{
				if( B_IS_SET( decoder_info[ dcheck ].output_A.control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 ) )
				{
					// 5/12/2014 rmd : Flag that we've got one. So we don't pick up another TRANSITION STAT
					// command. Or any other command for that matter.
					found_a_TRANSITION_STAT_command = (true);

					// 5/12/2014 rmd : And capture the decoder to send the command to.
					ddd = dcheck;

					doutput = DECODER_OUTPUT_A_BLACK;
				}
			}

			if( !found_a_TRANSITION_STAT_command )
			{
				if( B_IS_SET( decoder_info[ dcheck ].output_B.control, OUTPUT_CONTROL_BIT_GET_TRANSITIONAL_STAT1 ) )
				{
					// 5/12/2014 rmd : Flag that we've got one. So we don't pick up another TRANSITION STAT
					// command. Or any other command for that matter.
					found_a_TRANSITION_STAT_command = (true);

					// 5/12/2014 rmd : And capture the decoder to send the command to.
					ddd = dcheck;

					doutput = DECODER_OUTPUT_B_ORANGE;
				}
			}

			// ----------

			if( !found_a_TRANSITION_STAT_command && !found_an_OFF_command )
			{
				if( B_IS_SET( decoder_info[ dcheck ].output_A.control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND ) )
				{
					// 5/12/2014 rmd : Flag that we've got one. So we don't pick up another OFF command. Or any
					// other command for that matter.
					found_an_OFF_command = (true);

					// 5/12/2014 rmd : And capture the decoder to send the command to.
					ddd = dcheck;

					doutput = DECODER_OUTPUT_A_BLACK;
				}
			}

			if( !found_a_TRANSITION_STAT_command && !found_an_OFF_command )
			{
				if( B_IS_SET( decoder_info[ dcheck ].output_B.control, OUTPUT_CONTROL_BIT_SEND_OFF_COMMAND ) )
				{
					// 5/12/2014 rmd : Flag that we've got one. So we don't pick up another OFF command. Or any
					// other command for that matter.
					found_an_OFF_command = (true);

					// 5/12/2014 rmd : And capture the decoder to send the command to.
					ddd = dcheck;

					doutput = DECODER_OUTPUT_B_ORANGE;
				}
			}

			// ----------

			if( !found_a_TRANSITION_STAT_command && !found_an_OFF_command && !found_an_ON_command )
			{
				if( B_IS_SET( decoder_info[ dcheck ].output_A.control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND ) )
				{
					// 5/12/2014 rmd : Flag to prevent another from being seen.
					found_an_ON_command = (true);

					// 5/12/2014 rmd : And capture the decoder to send the command to.
					ddd = dcheck;

					doutput = DECODER_OUTPUT_A_BLACK;
				}
			}

			if( !found_a_TRANSITION_STAT_command && !found_an_OFF_command && !found_an_ON_command )
			{
				if( B_IS_SET( decoder_info[ dcheck ].output_B.control, OUTPUT_CONTROL_BIT_SEND_ON_COMMAND ) )
				{
					// 5/12/2014 rmd : Flag to prevent another from being seen.
					found_an_ON_command = (true);

					// 5/12/2014 rmd : And capture the decoder to send the command to.
					ddd = dcheck;

					doutput = DECODER_OUTPUT_B_ORANGE;
				}
			}

			// ----------

			// 5/12/2014 rmd : The flow reading is a higher priority than the PERIODIC status command
			// and MOISTURE command. The reasoning being that it is only in the very rare (hopefully)
			// case that the status command reveals the decoder state doesn't match the desired state.
			// And getting the flow reading is way more important. The moisture command is not as
			// important as getting flow but more important than the background review of decoder
			// status.
			if( !found_a_TRANSITION_STAT_command && !found_an_OFF_command && !found_an_ON_command && !found_a_FLOW_command )
			{
				if( B_IS_SET( decoder_info[ dcheck ].decoder_cmds, DECODER_CMDS_BIT_GET_FLOW_METER_READING ) )
				{
					// 5/12/2014 rmd : Flag to prevent another from being seen.
					found_a_FLOW_command = (true);

					// 5/12/2014 rmd : Note the FLOW command is a decoder level command. No output to specify.
					
					// 5/12/2014 rmd : And capture the decoder to send the command to.
					ddd = dcheck;
				}
			}

			// ----------

			// 1/26/2016 rmd : The moisture command is not as important as getting flow but more
			// important than the background review of decoder status.
			if( !found_a_TRANSITION_STAT_command && !found_an_OFF_command && !found_an_ON_command && !found_a_FLOW_command && !found_a_MOISTURE_command )
			{
				if( B_IS_SET( decoder_info[ dcheck ].decoder_cmds, DECODER_CMDS_BIT_GET_MOISTURE_READING ) )
				{
					// 5/12/2014 rmd : Flag to prevent another from being seen.
					found_a_MOISTURE_command = (true);

					// 5/12/2014 rmd : Note the MOISTURE command is a decoder level command. No output to
					// specify.
					
					// 5/12/2014 rmd : And capture the decoder to send the command to.
					ddd = dcheck;
				}
			}

			// ----------

			if( !found_a_TRANSITION_STAT_command && !found_an_OFF_command && !found_an_ON_command && !found_a_FLOW_command && !found_a_MOISTURE_command && !found_a_PERIODIC_STAT_command )
			{
				if( B_IS_SET( decoder_info[ dcheck ].decoder_cmds, DECODER_CMDS_BIT_OBTAIN_PERIODIC_STAT1 ) )
				{
					// 5/12/2014 rmd : Flag to prevent another from being seen.
					found_a_PERIODIC_STAT_command = (true);

					// 5/12/2014 rmd : Note the STAT command is a decoder level command. No output to
					// specify.
					
					// 5/12/2014 rmd : And capture the decoder to send the command to.
					ddd = dcheck;
				}
			}

		}  // of for all possible decoders
		
		// ----------

		// 5/14/2013 rmd : A sanity test variable. We should never see the STAT1 indicate more than
		// 6 ON at a time. If so we aren't issuing commands in the right order - meaning the OFF's
		// aren't coming before the ON's.
		if( number_of_decoder_OUTPUTS_ON > MAX_NUMBER_OF_DECODER_BASED_OUTPUTS_ON_AT_A_TIME )
		{
			// 5/17/2013 rmd : Just alert. We need to re-work the code to fix this. How does this relate
			// to the electrical limit for the box?
			alert_message_to_tpmicro_pile( "Decoders show too many stations ON right now." );
		}
		
		// ----------

		// 5/12/2014 rmd : If we have a qualifying command power up the cable.
		if( found_a_TRANSITION_STAT_command || found_an_OFF_command || found_an_ON_command || found_a_FLOW_command || found_a_MOISTURE_command )
		{
			command_to_execute = (true);

			if( !two_wire_bus_info.cable_is_powered )
			{
				// 8/3/2015 rmd : And do not wait for cable to charge. We will pass back through here every
				// 500ms or so looking for commands to work on. And the cable will eventually become
				// charged.
				power_up_two_wire_cable( ptask_watchdog_time_stamp_ptr, (false) );
			}
		}

		// 2/22/2016 rmd : And get the cable turned ON if we have a moisture decoder or a flow meter
		// decoder. This is important so the moisture sensor decoder gets started on its one minute
		// timer (it needs to be powered for one minute before it acquires a moisture sensor
		// reading).
		if( decoders_present_dictate_an_energized_cable )
		{
			if( !two_wire_bus_info.cable_is_powered )
			{
				// 8/3/2015 rmd : And do not wait for cable to charge. We will pass back through here every
				// 500ms or so looking for commands to work on. And the cable will eventually become
				// charged.
				power_up_two_wire_cable( ptask_watchdog_time_stamp_ptr, (false) );
			}
		}

		// ----------
	
		// 10/17/2013 rmd : If not charging we are eligible to perform command exchanges with the
		// decoders.
		if( two_wire_bus_info.cable_is_powered && two_wire_bus_info.cable_is_done_charging )
		{
			// 5/13/2014 rmd : Execute ONE command using our defined priority scheme.

			if( found_a_TRANSITION_STAT_command )
			{
				execute_stat1_command( ddd );	
			}
			else
			if( found_an_OFF_command )
			{
				execute_OFF_command( ddd, doutput );
			}
			else
			if( found_an_ON_command )
			{
				execute_ON_command( ddd, doutput );

				// 6/4/2013 rmd : Note - due to the solenoid cap charging scheme within the decoder, it now
				// takes 2 plus seconds to complete a solenoid turn on sequence. The STAT command must not
				// attempt for 3 seconds following an ON command. Regardless of outcome wait 3 seconds
				// before next command. There are certain response conditions where this is not needed. But
				// doesn't hurt to wait the 3 seconds and keeps code uncomplicated here.
				time_to_next_command_ms = TWO_WIRE_CABLE_MS_BETWEEN_AFTER_AN_ON_COMMAND;
			}
			else
			if( found_a_FLOW_command )
			{
				execute_FLOW_command( ddd );
			}
			else
			if( found_a_MOISTURE_command )
			{
				execute_MOISTURE_command( ddd );
			}
			else
			if( found_a_PERIODIC_STAT_command )
			{
				execute_stat1_command( ddd );	
			}
			
		}  // of not charging
		
	}  // of not excessive current and not terminal overheated

	// ----------

	xSemaphoreGive( decoder_control_MUTEX );

	// ----------
	
	// 4/18/2013 rmd : Restart the activity timer with a custom period based upon the last
	// command executed.
	xTimerChangePeriod( two_wire_activity_timer, MS_to_TICKS( time_to_next_command_ms ), portMAX_DELAY );
}

/* ---------------------------------------------------------- */
static void __initialize_output_control_prior_to_incorporating_new_state( UNS_8 *poutput_control )
{
	// 4/19/2013 rmd : Goal is to detect if the new 'instructions' from the main board produces
	// any change of state for the outputs. So track what they were set at.
	if( B_IS_SET( *poutput_control, OUTPUT_CONTROL_BIT_IS_ON ) )
	{
		B_SET( *poutput_control, OUTPUT_CONTROL_BIT_WAS_ON );
	}
	else
	{
		B_UNSET( *poutput_control, OUTPUT_CONTROL_BIT_WAS_ON );
	}
	
	// 4/26/2013 rmd : And turn OFF so that stations that don't show in the message are set to
	// be OFF - like we want them.
	B_UNSET( *poutput_control, OUTPUT_CONTROL_BIT_IS_ON );
}

/* ---------------------------------------------------------- */
static void __check_against_how_it_was( UNS_8 *poutput_control )
{
	// 4/26/2013 rmd : IMPORTANT NOTE - only SET the COS (change of state) bit here. Do not
	// unset it. That takes place in the 4 Hz function that performs the communication with the
	// decoders. You see we could set the COS bit here and before the COS was recognized and
	// processed we receieved another 1 Hz message from the main board. And during this
	// comparison here if we UNSET the COS bit (because between the two msgs from main nothing
	// has changed) we would skip processing the change of state!
	if( B_IS_SET( *poutput_control, OUTPUT_CONTROL_BIT_IS_ON ) != B_IS_SET( *poutput_control, OUTPUT_CONTROL_BIT_WAS_ON ) )
	{
		B_SET( *poutput_control, OUTPUT_CONTROL_BIT_COS_DETECTED );
	}
}	

/* ---------------------------------------------------------- */
static void process_new_stations_ON_from_main( TWO_WIRE_TASK_QUEUE_STRUCT	*ptwq )
{
	IRRIGATION_TASK_QUEUE_STRUCT	itqs;
	
	UNS_8		number_of_stations_to_turn_ON;

	UNS_32		ddd, sss;

	char		str_32[ 32 ];
	
	UNS_8		*ucp;
	
	// ----------

	// 4/17/2013 rmd : A MUTEX may not even be necessary since all manipulation and use of the
	// decoder_info solenoid control is performed within a single task. The 'two_wire_task'.
	if( xSemaphoreTake( decoder_control_MUTEX, 0 ) != pdTRUE )
	{
		turn_ons_blocked += 1;
		
		snprintf( str_32, 32, "Blocked again %u", turn_ons_blocked );
		
		alert_message_to_tpmicro_pile( str_32);

		// 4/17/2013 rmd : This construct is to see if we care ever held up by the taking of the
		// MUTEX. We should not ever be as only this task does the taking and giving. And bottom
		// line is we must have the MUTEX. So now the portMAX_DELAY attempt.
		xSemaphoreTake( decoder_control_MUTEX, portMAX_DELAY );
	}

	// ----------

	// 9/27/2013 rmd : For the stations within the message that are terminal based we build up
	// this queue message.
	itqs.command = IRRI_TASK_CMD_terminal_based_stations_from_main;
	
	itqs.terminal_stations_to_be_turned_ON = 0;

	// ----------

	// 4/16/2013 rmd : Because we are only told about those ON, not being told about a decoder
	// implies it is to be OFF. So start with them all OFF. Tracking if we think we have to
	// command the solenoid to actually be OFF. Or if we are told to turn it ON was it already
	// ON. Go through the whole list of all possible decoders.
	for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
	{
		// 9/23/2015 rmd : If the array item is in use.
		if( decoder_list[ ddd ].sn )
		{
			if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION )
			{
				// 4/12/2016 rmd : For two station decoder both outputs.
				__initialize_output_control_prior_to_incorporating_new_state( &(decoder_info[ ddd ].output_A.control) );

				__initialize_output_control_prior_to_incorporating_new_state( &(decoder_info[ ddd ].output_B.control) );
			}
			else
			if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE )
			{
				// 4/12/2016 rmd : Moisture decoders only have output A.
				__initialize_output_control_prior_to_incorporating_new_state( &(decoder_info[ ddd ].output_A.control) );
			}
			
		}
	}
	
	// ----------
	
	ucp = ptwq->ucp;
	
	// 4/17/2013 rmd : If the pointer is NULL that is our indication NO STATIONS are ON. This is
	// valid and intentional.
	if( ucp == NULL )
	{
		number_of_stations_to_turn_ON = 0;
	}
	else
	{
		// 4/17/2013 rmd : So we are going to pull apart the message. The count of the number of ON
		// stations has already been checked so no need to range check how many we're pulling.
		number_of_stations_to_turn_ON = *ucp;
	
		ucp += sizeof( UNS_8 );
	}

	// ----------

	// 4/10/2013 rmd : The stations are delivered as a structure giving enough information to
	// know if they are terminal or decoder based. The number of station COULD BE ZERO which is
	// our indication that NO STATIONS ARE ON!!!
	for( sss=0; sss<number_of_stations_to_turn_ON; sss++ )
	{
		STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT	sos;
		
		memcpy( &sos, ucp, sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
		
		ucp += sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT);

		// 4/10/2013 rmd : Our definition of a terminal based station is that the decoder serial
		// number field has the DECODER_SERIAL_NUM_DEFAULT value.
		if( sos.decoder_serial_number == DECODER_SERIAL_NUM_DEFAULT )
		{
			// 4/10/2013 rmd : Okay its a TERMINAL based station. Is the hardware present?
			if( card_and_terminal_are_present_for_this_station( sos.terminal_number_0 ) )
			{
				B_SET_64( itqs.terminal_stations_to_be_turned_ON, sos.terminal_number_0 );
			}
			else
			{
				alert_message_to_tpmicro_pile( "Station ON rqst - HARDWARE NOT PRESENT" );
			}
		}
		else
		{
			// 4/10/2013 rmd : This 'station' is decoder based!
			FIND_DECODER_STRUCT		fds;
			
			find_decoder( sos.decoder_serial_number, &fds );
			
			if( (fds.list_ptr != NULL) && (fds.info_ptr != NULL) )
			{
				if( fds.list_ptr->id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION )
				{
					if( sos.decoder_output == DECODER_OUTPUT_A_BLACK )
					{
						B_SET( fds.info_ptr->output_A.control, OUTPUT_CONTROL_BIT_IS_ON );
					}
					else
					if( sos.decoder_output == DECODER_OUTPUT_B_ORANGE )
					{
						B_SET( fds.info_ptr->output_B.control, OUTPUT_CONTROL_BIT_IS_ON );
					}
					else
					{
						decoder_alert_message_to_tpmicro_pile( sos.decoder_serial_number, "sta dec output out of range" );
					}
				}
				else
				if( fds.list_ptr->id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE )
				{
					if( sos.decoder_output == DECODER_OUTPUT_A_BLACK )
					{
						B_SET( fds.info_ptr->output_A.control, OUTPUT_CONTROL_BIT_IS_ON );
					}
					else
					{
						decoder_alert_message_to_tpmicro_pile( sos.decoder_serial_number, "mois dec output out of range" );
					}
				}
				else
				{
					decoder_alert_message_to_tpmicro_pile( sos.decoder_serial_number, "not right kind of decoder!" );
				}
			}
			else
			{
				decoder_alert_message_to_tpmicro_pile( sos.decoder_serial_number, "station decoder to turn ON not found" );
			}
			
		}  // of its a decoder

	}  // of for each station to turn ON

	// ----------

	// 4/17/2013 rmd : If the comm_mngr allocated memory i.e. the pointer is not NULL release
	// it! Remember the pointer being NULL is our indication not to have any stations ON.
	if( ptwq->ucp != NULL )
	{
		FreeMemBlk( ptwq->ucp );
	}

	// ----------

	// 4/10/2013 rmd : For the CONVENTIONAL outputs, queue the command that was built up as the
	// message was parsed. Using NO_DELAY so we can see if we have a queue sizing problem.
	if( xQueueSendToBack( irrigation_task_queue, &itqs, portNO_DELAY ) != pdTRUE )
	{
		flag_error( BIT_TPME_queue_full );
	}

	// ----------

	// 4/26/2013 rmd : Now pass back through the decoder list looking for output change of state
	// to identify.
	for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
	{
		// 4/26/2013 rmd : Only check on decoders with a serial number.
		if( decoder_list[ ddd ].sn )
		{
			if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION )
			{
				__check_against_how_it_was( &(decoder_info[ ddd ].output_A.control) );
	
				__check_against_how_it_was( &(decoder_info[ ddd ].output_B.control) );
				
				// ----------
				
				// 5/29/2013 rmd : And when the main board signals us with a change of state for a decoder
				// output to be ON, if that decoder has been flagged as INOPERATIVE we'll clear that now.
				if( ( B_IS_SET(decoder_info[ ddd ].output_A.control, OUTPUT_CONTROL_BIT_IS_ON) && B_IS_SET(decoder_info[ ddd ].output_A.control, OUTPUT_CONTROL_BIT_COS_DETECTED) ) ||
					( B_IS_SET(decoder_info[ ddd ].output_B.control, OUTPUT_CONTROL_BIT_IS_ON) && B_IS_SET(decoder_info[ ddd ].output_B.control, OUTPUT_CONTROL_BIT_COS_DETECTED) ) )
				{
					if( B_IS_SET(decoder_list[ ddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE) )
					{
						// 6/7/2013 rmd : Always try again to operate the valve. The user may have fixed the
						// problem. Do not require user intervention to clear an inoperative flag. Automatically
						// tries again.
						decoder_alert_message_to_tpmicro_pile( decoder_list[ ddd ].sn, "Decoder inoperative state cleared" );
						
						// ----------
	
						// 6/25/2013 rmd : Clear the bit and the count.
						B_UNSET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE );
	
						decoder_list[ ddd ].communication_failures = 0;
	
						// ----------
	
						// 6/7/2013 rmd : Need to save to ee as the complement to the save we do when the flag is
						// set. Even if we didn't save the list at the place the flag was set, if the flag was set
						// and then a list save triggerred for another reason we get the same predicament. Every
						// time there is a power fail the list is read out with the flag set. The alternative is to
						// clear the flag when list is read out of EE.
						save_decoder_list_to_ee();
					}
					
				}

			}
			else
			if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__MOISTURE )
			{
				__check_against_how_it_was( &(decoder_info[ ddd ].output_A.control) );
	
				// ----------
				
				// 5/29/2013 rmd : And when the main board signals us with a change of state for a decoder
				// output to be ON, if that decoder has been flagged as INOPERATIVE we'll clear that now.
				if( B_IS_SET(decoder_info[ ddd ].output_A.control, OUTPUT_CONTROL_BIT_IS_ON) && B_IS_SET(decoder_info[ ddd ].output_A.control, OUTPUT_CONTROL_BIT_COS_DETECTED) ) 
				{
					if( B_IS_SET(decoder_list[ ddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE) )
					{
						// 6/7/2013 rmd : Always try again to operate the valve. The user may have fixed the
						// problem. Do not require user intervention to clear an inoperative flag. Automatically
						// tries again.
						decoder_alert_message_to_tpmicro_pile( decoder_list[ ddd ].sn, "Decoder inoperative state cleared" );
						
						// ----------
	
						// 6/25/2013 rmd : Clear the bit and the count.
						B_UNSET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE );
	
						decoder_list[ ddd ].communication_failures = 0;
	
						// ----------
	
						// 6/7/2013 rmd : Need to save to ee as the complement to the save we do when the flag is
						// set. Even if we didn't save the list at the place the flag was set, if the flag was set
						// and then a list save triggerred for another reason we get the same predicament. Every
						// time there is a power fail the list is read out with the flag set. The alternative is to
						// clear the flag when list is read out of EE.
						save_decoder_list_to_ee();
					}
					
				}

			}
			
			
		}  // of active in list

	}  // of for all possible decoders
	
	xSemaphoreGive( decoder_control_MUTEX );
}

/* ---------------------------------------------------------- */
static void process_new_pocs_ON_from_main( TWO_WIRE_TASK_QUEUE_STRUCT	*ptwq )
{
	IRRIGATION_TASK_QUEUE_STRUCT	itqs;
	
	UNS_8		number_of_pocs_to_turn_ON;

	UNS_32		ddd, ppp;

	char		str_32[ 32 ];
	
	UNS_8		*ucp;
	
	POC_ON_FOR_TPMICRO_STRUCT	pos;
	
	BOOL_32		already_found_terminal_poc;
		
	// ----------

	// 4/17/2013 rmd : A MUTEX may not even be necessary since all manipulation and use of the
	// decoder_info solenoid control is performed within a single task. The 'two_wire_task'.
	if( xSemaphoreTake( decoder_control_MUTEX, 0 ) != pdTRUE )
	{
		turn_ons_blocked += 1;
		
		snprintf( str_32, 32, "Blocked again %u", turn_ons_blocked );
		
		alert_message_to_tpmicro_pile( str_32);

		// 4/17/2013 rmd : This construct is to see if we care ever held up by the taking of the
		// MUTEX. We should not ever be as only this task does the taking and giving. And bottom
		// line is we must have the MUTEX. So now the portMAX_DELAY attempt.
		xSemaphoreTake( decoder_control_MUTEX, portMAX_DELAY );
	}

	// ----------

	// 9/27/2013 rmd : For the terminal based POC. If it is within the message from the
	// tpmicro. There can only be ONE. However we always send the command to the irrigation task
	// if the poc card and terminal are installed. So we default to not turning ON the mv and
	// pump.
	itqs.command = IRRI_TASK_CMD_terminal_based_mv_and_pmp_from_main;
	
	itqs.terminal_mv_to_be_turned_ON = (false);

	itqs.terminal_pump_to_be_turned_ON = (false);
	
	// ----------

	// 4/16/2013 rmd : Because we are only told about those ON, not being told about a decoder
	// implies it is to be OFF. So start with them all OFF. Tracking if we think we have to
	// command the solenoid to actually be OFF. Or if we are told to turn it ON was it already
	// ON. Go through the whole list of all possible decoders.
	for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
	{
		// 5/16/2014 rmd : Only manipulating the POC decoders.
		if( decoder_list[ ddd ].sn )
		{
			if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
			{
				__initialize_output_control_prior_to_incorporating_new_state( &(decoder_info[ ddd ].output_A.control) );
			}
		}
	}
	
	// ----------
	
	ucp = ptwq->ucp;
	
	// 4/17/2013 rmd : If the pointer is NULL that is our indication NO POCS are ON. This is
	// valid and intentional.
	if( ucp == NULL )
	{
		number_of_pocs_to_turn_ON = 0;
	}
	else
	{
		// 4/17/2013 rmd : So we are going to pull apart the message. The count of the number ON
		// has already been checked so no need to range check how many we're pulling.
		number_of_pocs_to_turn_ON = *ucp;
	
		ucp += sizeof( UNS_8 );
	}

	// ----------

	// 5/16/2014 rmd : Santiy check variable.
	already_found_terminal_poc = (false);

	// 4/10/2013 rmd : The pocs are delivered as a structure giving enough information to know
	// if they are terminal or decoder based. The number of pocs ON COULD BE ZERO which is our
	// indication that NONE ARE ON!!!
	for( ppp=0; ppp<number_of_pocs_to_turn_ON; ppp++ )
	{
		memcpy( &pos, ucp, sizeof(POC_ON_FOR_TPMICRO_STRUCT) );
		
		ucp += sizeof(POC_ON_FOR_TPMICRO_STRUCT);

		// 4/10/2013 rmd : Our definition of a terminal based station is that the decoder serial
		// number field has the DECODER_SERIAL_NUM_DEFAULT value.
		if( pos.decoder_serial_number == DECODER_SERIAL_NUM_DEFAULT )
		{
			if( !already_found_terminal_poc )
			{
				already_found_terminal_poc = (true);
				
				// 5/22/2014 rmd : This is a healthy check to make. If the main board is providing
				// instruction to turn ON - the hardware is supposed to be present.
				if( whats_installed.poc.card_present && whats_installed.poc.tb_present )
				{
					itqs.terminal_mv_to_be_turned_ON = B_IS_SET( pos.pump_and_mv_control, POC_CONTENT_TO_TP_MICRO_energize_mv );
	
					itqs.terminal_pump_to_be_turned_ON = B_IS_SET( pos.pump_and_mv_control, POC_CONTENT_TO_TP_MICRO_energize_pump );
					
					// 10/2/2015 rmd : If neither one then why was this POC sent to us?
					if( !itqs.terminal_mv_to_be_turned_ON && !itqs.terminal_pump_to_be_turned_ON )
					{
						alert_message_to_tpmicro_pile( "surprise: neither one set" );
					}
				}
				else
				{
					alert_message_to_tpmicro_pile( "terminal POC not present to turn ON" );
				}
			}
			else
			{
				// 5/16/2014 rmd : Sanity check.
				alert_message_to_tpmicro_pile( "terminal POC more than once?" );
			}
		}
		else
		{
			// 4/10/2013 rmd : This 'poc' is decoder based!
			FIND_DECODER_STRUCT		fds;
			
			find_decoder( pos.decoder_serial_number, &fds );
			
			if( (fds.list_ptr != NULL) && (fds.info_ptr != NULL) )
			{
				if( fds.list_ptr->id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
				{
					// 5/16/2014 rmd : The POC decoder only has ONE output choice. So if it is in this list then
					// we want to energize the MV output (output A). However still check the mv bit to be set.
					// We expect it to be! That is why this POC was delivered to us.
					if( B_IS_SET( pos.pump_and_mv_control, POC_CONTENT_TO_TP_MICRO_energize_mv ) )
					{
						B_SET( fds.info_ptr->output_A.control, OUTPUT_CONTROL_BIT_IS_ON );
					}
					else
					{
						alert_message_to_tpmicro_pile( "surprise: mv bit not set" );
					}
				}
				else
				{
					decoder_alert_message_to_tpmicro_pile( pos.decoder_serial_number, "should be a poc decoder!" );
				}
			}
			else
			{
				decoder_alert_message_to_tpmicro_pile( pos.decoder_serial_number, "poc decoder to turn ON not found" );
			}
			
		}  // of its a decoder

	}  // of for each poc to turn ON

	// ----------

	// 4/17/2013 rmd : If the comm_mngr allocated memory i.e. the pointer is not NULL release
	// it! Remember the pointer being NULL is our indication not to have any poc ON.
	if( ptwq->ucp != NULL )
	{
		FreeMemBlk( ptwq->ucp );
	}

	// ----------

	// 4/10/2013 rmd : For the CONVENTIONAL outputs, queue the command that was built up as the
	// message was parsed. Using NO_DELAY so we can see if we have a queue sizing problem. The
	// queue receiving this message will do the comparison to detect the output change of
	// states.
	if( xQueueSendToBack( irrigation_task_queue, &itqs, portNO_DELAY ) != pdTRUE )
	{
		flag_error( BIT_TPME_queue_full );
	}

	// ----------

	// 4/26/2013 rmd : Now pass back through the decoder list looking for output change of state
	// to identify.
	for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
	{
		// 4/26/2013 rmd : Only check on POC decoders with a serial number.
		if( decoder_list[ ddd ].sn )
		{
			if( decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC )
			{
				__check_against_how_it_was( &(decoder_info[ ddd ].output_A.control) );

				// ----------
				
				// 5/29/2013 rmd : And when the main board signals us with a change of state for a decoder
				// output to be ON, if that decoder has been flagged as INOPERATIVE we'll clear that setting
				// now. For the case of the POC decoder the COS bit will NEVER be set.
				if( B_IS_SET(decoder_info[ ddd ].output_A.control, OUTPUT_CONTROL_BIT_IS_ON) && B_IS_SET(decoder_info[ ddd ].output_A.control, OUTPUT_CONTROL_BIT_COS_DETECTED) )
				{
					if( B_IS_SET(decoder_list[ ddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE) )
					{
						// 6/7/2013 rmd : Always try again to operate the valve. The user may have fixed the
						// problem. Do not require user intervention to clear an inoperative flag. Automatically
						// tries again.
						decoder_alert_message_to_tpmicro_pile( decoder_list[ ddd ].sn, "Decoder inoperative state cleared" );
						
						// ----------
	
						// 6/25/2013 rmd : Clear the bit and the count.
						B_UNSET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE );
	
						decoder_list[ ddd ].communication_failures = 0;
	
						// ----------
	
						// 6/7/2013 rmd : Need to save to ee as the complement to the save we do when the flag is
						// set. Even if we didn't save the list at the place the flag was set, if the flag was set
						// and then a list save triggerred for another reason we get the same predicament. Every
						// time there is a power fail the list is read out with the flag set. The alternative is to
						// clear the flag when list is read out of EE.
						save_decoder_list_to_ee();
					}
					
				}

			}
			
		}  // of this is a station decoder

	}  // of for all possible decoders
	
	xSemaphoreGive( decoder_control_MUTEX );
}

/* ---------------------------------------------------------- */
static void process_new_lights_ON_from_main( TWO_WIRE_TASK_QUEUE_STRUCT	*ptwq )
{
	// 9/23/2015 rmd : This function does not allow for LIGHTS DECODERS. To add them refer to
	// the equivalent stations ON function. I left out lights decoders cause I think it is a
	// slim chance we will actually build such.
	
	// ----------
	
	IRRIGATION_TASK_QUEUE_STRUCT	itqs;
	
	UNS_8		number_of_lights_to_turn_ON;

	UNS_32		sss;

	UNS_8		*ucp;
	
	// ----------

	// 9/23/2015 rmd : All the lights within the message should be TERMINAL based and we build
	// up a queue message for the IRRI task accordingly.
	itqs.command = IRRI_TASK_CMD_terminal_based_lights_from_main;
	
	itqs.terminal_lights_to_be_turned_ON = 0;

	// ----------
	
	ucp = ptwq->ucp;
	
	// 4/17/2013 rmd : If the pointer is NULL that is our indication NO STATIONS are ON. This is
	// valid and intentional.
	if( ucp == NULL )
	{
		number_of_lights_to_turn_ON = 0;
	}
	else
	{
		// 4/17/2013 rmd : So we are going to pull apart the message. The count of the number of ON
		// stations has already been checked so no need to range check how many we're pulling.
		number_of_lights_to_turn_ON = *ucp;
	
		ucp += sizeof( UNS_8 );
	}

	// ----------

	// 4/10/2013 rmd : The lights are delivered as a structure giving enough information to know
	// if they are terminal or decoder based. The number of station COULD BE ZERO which is our
	// indication that NO STATIONS ARE ON!!!
	for( sss=0; sss<number_of_lights_to_turn_ON; sss++ )
	{
		STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT	sos;
		
		memcpy( &sos, ucp, sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT) );
		
		ucp += sizeof(STATION_OR_LIGHTS_ON_FOR_TPMICRO_STRUCT);

		// 4/10/2013 rmd : Our definition of a terminal based is that the decoder serial number
		// field has the DECODER_SERIAL_NUM_DEFAULT value.
		if( sos.decoder_serial_number == DECODER_SERIAL_NUM_DEFAULT )
		{
			// 4/10/2013 rmd : Okay its a TERMINAL based station. Is the hardware present?
			if( whats_installed.lights.card_present && whats_installed.lights.tb_present )
			{
				B_SET( itqs.terminal_lights_to_be_turned_ON, sos.terminal_number_0 );
			}
			else
			{
				alert_message_to_tpmicro_pile( "Lights ON rqst - HARDWARE NOT PRESENT" );
			}
		}
		else
		{
			alert_message_to_tpmicro_pile( "decoder based lights request!" );
		}

	}  // of for each to turn ON

	// ----------

	// 4/17/2013 rmd : If the comm_mngr allocated memory i.e. the pointer is not NULL release
	// it! Remember the pointer being NULL is our indication not to have any stations ON.
	if( ptwq->ucp != NULL )
	{
		FreeMemBlk( ptwq->ucp );
	}

	// ----------

	// 4/10/2013 rmd : For the CONVENTIONAL outputs, queue the command that was built up as the
	// message was parsed. Using NO_DELAY so we can see if we have a queue sizing problem.
	if( xQueueSendToBack( irrigation_task_queue, &itqs, portNO_DELAY ) != pdTRUE )
	{
		flag_error( BIT_TPME_queue_full );
	}
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_activity_timer_handler( xTimerHandle xTimer )
{
	// This is NOT an interrupt. But rather executed within the context of the timer task.
	
	// ----------
	
	// 4/18/2013 rmd : We post a queue message to the two_wire_task. Do not execute the
	// 'two-wire activity' directly of course. If we did we'd be executing it within the context
	// of the timer task. This is a 500ms timer. And is started at the completion of the prior
	// command, meaning response received or timed out.
	TWO_WIRE_TASK_QUEUE_STRUCT	twtqs;

	twtqs.command = TWO_WIRE_TASK_QUEUE_COMMAND_execute_commands_to_decoders;
	
	xQueueSendToBack( two_wire_task_queue, &twtqs, portMAX_DELAY );
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_special_activation_timer_handler( xTimerHandle xTimer )
{
	// This is NOT an interrupt. But rather executed within the context of the timer task.
	
	// ----------
	
	TWO_WIRE_TASK_QUEUE_STRUCT	twtqs;

	twtqs.command = TWO_WIRE_TASK_QUEUE_COMMAND_end_special_activation_mode;
	
	xQueueSendToBack( two_wire_task_queue, &twtqs, portMAX_DELAY );
	
	// ----------
}

/* ---------------------------------------------------------- */
extern void TWO_WIRE_cable_charge_timer_handler( xTimerHandle xTimer )
{
	// This is NOT an interrupt. But rather executed within the context of the timer task.
	
	// ----------
	
	alert_message_to_tpmicro_pile( "cable charge timer fired" );

	// 10/17/2013 rmd : DIRECTLY set the fact that the cable is charged (true). This is done
	// this way so the discovery process can pend on this cable_is_done_charging flag. And this
	// high priority timer task will set it (true) here. Making it available in the two_wire
	// task to read. I tried to do it via a queue message. But then would have to complicate the
	// invocation of the discovery proocess. And this works though a bit dirty feeling from a
	// variable encapsulation point of view. This marks the end of the cable charge period. And
	// allows command exchanges to begin with the decoders.
	two_wire_bus_info.cable_is_done_charging = (true);
	
	// ----------
}

/* ---------------------------------------------------------- */
static void exit_special_activation_mode( void )
{
	// 10/16/2013 rmd : Invoked from the two_wire task. Clear any activated solenoid outputs and
	// exit the mode.
	UNS_32	ddd;

	if( !two_wire_bus_info.special_activation_mode )
	{
		// 10/16/2013 rmd : Then there is nothing to do. The command is asking us to turn OFF the
		// output, but we are not even in the special activation mode so nothing to do!
		alert_message_to_tpmicro_pile( "SPECIAL ACT: rqst to exit unexpected" );
	}
	else
	{
		// 10/16/2013 rmd : So we are already in the special acitvation mode and the user wants us
		// to stop. Look through the list. Any valves ON should be turned OFF by setting the COS
		// bit. There should ONLY be 1 ON! By definition.
		for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
		{
			// 10/15/2013 rmd : Only for the active, station decoders in the list.
			if( (decoder_list[ ddd ].sn) && ((decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION) || (decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__POC)) )
			{
				// 10/16/2013 rmd : Don't need to test for a serial number or not. If they don't have a
				// serial number they won't have their commands processed anyway.
				if( B_IS_SET( decoder_info[ ddd ].output_A.control, OUTPUT_CONTROL_BIT_IS_ON ) )
				{
					B_UNSET( decoder_info[ ddd ].output_A.control, OUTPUT_CONTROL_BIT_IS_ON );

					B_SET( decoder_info[ ddd ].output_A.control, OUTPUT_CONTROL_BIT_COS_DETECTED );
				}
			
				// 5/13/2014 rmd : Its okay to test this even for the POC decoder. It shouldn't be set.
				if( B_IS_SET( decoder_info[ ddd ].output_B.control, OUTPUT_CONTROL_BIT_IS_ON ) )
				{
					B_UNSET( decoder_info[ ddd ].output_B.control, OUTPUT_CONTROL_BIT_IS_ON );

					B_SET( decoder_info[ ddd ].output_B.control, OUTPUT_CONTROL_BIT_COS_DETECTED );
				}
			}
		}
		
		// 10/16/2013 rmd : And exit the special activation mode.
		two_wire_bus_info.special_activation_mode = (false);
	}
}
	
/* ---------------------------------------------------------- */
void two_wire_task( void *pvParameters )
{
	UNS_32	*task_watchdog_time_stamp;

	TWO_WIRE_TASK_QUEUE_STRUCT	twtq;
	
	UNS_32	ddd;
					
	// ----------

	task_watchdog_time_stamp = ((TASK_TABLE_STRUCT*)pvParameters)->last_execution_time_stamp;
	
	// ----------

	// 5/12/2014 rmd : The compiler zeros the decoder_info array as part of the C startup code.
	// We are counting on that for example for the ms_since_last_periodic_status_check variable.
	
	// ----------

	// 1/18/2013 rmd : Crank up the FET gate control interrupts.
	initialize_two_wire_fet_control();

	// ----------
	
	// 4/18/2013 rmd : Start our 300ms 2 wire bus activity timer.
	xTimerReset( two_wire_activity_timer, portMAX_DELAY );
	
	// ----------
	
	// 1/27/2014 rmd : Well on power up (startup) the cable isn't powered and the decoder fault
	// state is to be normal.
	two_wire_bus_info.decoder_fault_state = DECODER_FAULT_STATE_normal;
		
	// ----------

	// 2/16/2016 rmd : Setup so that we take moisture readings shortly after power up. Roll
	// through the list of decoders looking for MOISTURE SENSOR decoders and set their last time
	// since a moisture reading appropriately to cause a reading to be taken now. This is to
	// support possible on-going irrigation by delivering to the main board the latest moisture
	// reading shortly after coming out of a power failure.
	for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
	{
		// 2/22/2016 rmd : Set so that 75 seconds after startup we get our FIRST moisture sensor
		// readings. (Set for whole list including non-moisture decoders - doesn't hurt anything to
		// do so.) Remember in the decoder the first moisture sensor reading is taken after 1
		// minute. So we should wait longer than a minute.
		decoder_info[ ddd ].ms_since_last_moisture_reading_acquired = (TPMICRO_MS_BETWEEN_GETTING_MOISTURE_READING_FROM_DECODERS - (75000));
	}
	
	// ----------
	
	while( (true) )
	{
		// 7/31/2015 rmd : The task is moving. Acquire latest time stamp for task watchdog scheme.
		*task_watchdog_time_stamp = xTaskGetTickCount();
		
		// ----------
		
		// 4/16/2013 rmd : At a minimum this queue is feed with each incoming message from main (to
		// deliver the new triac states). The is concern during long two-wire operation that this
		// queue will overflow. We pend 1 second so that we can time stamp the task as part of the
		// task watchdog scheme.
		if( xQueueReceive( two_wire_task_queue, &twtq, MS_to_TICKS( 1000 ) ) )
		{
			switch( twtq.command )
			{

				case TWO_WIRE_TASK_QUEUE_COMMAND_ack_cable_excessive_current:
	
					// 10/30/2013 rmd : Ensure there are no residual pending decoder commands. During the time
					// the overheated state was first detected and now (receipt of the confirmation from the
					// main board) the two-wire task will also clear the pending commands. However there is no
					// guarantee we got the 400ms execution period to elapse before we rcvd this ACK. So just
					// ensure by also posting to clear all pending commands.
					__wipe_all_pending_commands_and_states();
					
					// ----------
	
					// 11/7/2013 rmd : Set the flag to allow the excessive current to be cleared at the next
					// 400ms tick. This way we for sure flag the cable as being powered down due to the
					// excessive current. If we cleared the excessive current here, and the ACK beat the 400ms
					// queue message, well then we would alert powered down the cable due to inactivity. Not a
					// big deal but this cleans that up.
					two_wire_bus_info.clear_excessive_current = (true);
	
					// ----------
	
					break;
					
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_ack_cable_overheated:
	
					// 10/30/2013 rmd : Ensure there are no residual pending decoder commands. During the time
					// the overheated state was first detected and now (receipt of the confirmation from the
					// main board) the two-wire task will also clear the pending commands. However there is no
					// guarantee we got the 400ms execution period to elapse before we rcvd this ACK. So just
					// ensure by also posting to clear all pending commands.
					__wipe_all_pending_commands_and_states();
					
					// ----------
	
					// 11/6/2013 rmd : Remember the OVERHEATED state clears itself when the terminal cools off.
					// So we do not play any role in that here.
					
					// ----------
	
					break;
					
				// ----------
	
				// 4/22/2013 rmd : We attempt to execute another command to a decoder. Once each 500ms. The
				// timer is restarted after the decoder command exchange has completed.
				case TWO_WIRE_TASK_QUEUE_COMMAND_execute_commands_to_decoders:
	
					execute_commands_to_decoders( task_watchdog_time_stamp );
	
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_receive_stations_ON_from_main:
	
					// 10/16/2013 rmd : ONLY if have not hi-jacked the normal indication of which solenoid
					// outputs are to be energized should we process the indication from the main board.
					if( !two_wire_bus_info.special_activation_mode )
					{
						process_new_stations_ON_from_main( &twtq );
					}
					else
					{
						// 10/16/2013 rmd : BUT ahhh...we must release the memory associated with stations ON. If
						// there is any. Remember the pointer can be NULL - which is our indication not to have any
						// stations (conventional or decoder based) ON.
						if( twtq.ucp != NULL )
						{
							FreeMemBlk( twtq.ucp );
						}
					}
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_receive_pocs_ON_from_main:
	
					// 10/16/2013 rmd : ONLY if have not hi-jacked the normal indication of which solenoid
					// outputs are to be energized should we process the indication from the main board.
					if( !two_wire_bus_info.special_activation_mode )
					{
						process_new_pocs_ON_from_main( &twtq );
					}
					else
					{
						// 10/16/2013 rmd : BUT ahhh...we must release the memory associated with pocs ON. If there
						// is any. Remember the pointer can be NULL - which is our indication not to have any
						// pocs (conventional or decoder based) ON.
						if( twtq.ucp != NULL )
						{
							FreeMemBlk( twtq.ucp );
						}
					}
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_receive_lights_ON_from_main:
	
					// 10/16/2013 rmd : ONLY if have not hi-jacked the normal indication of which solenoid
					// outputs are to be energized should we process the indication from the main board.
					if( !two_wire_bus_info.special_activation_mode )
					{
						process_new_lights_ON_from_main( &twtq );
					}
					else
					{
						// 10/16/2013 rmd : BUT ahhh...we must release the memory associated with stations ON. If
						// there is any. Remember the pointer can be NULL - which is our indication not to have any
						// stations (conventional or decoder based) ON.
						if( twtq.ucp != NULL )
						{
							FreeMemBlk( twtq.ucp );
						}
					}
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_control_bus_power:
					if( twtq.bus_on_or_off == (true) )
					{
						// 1/29/2013 rmd : This can take up to one half of a 60hz power cycle - about 8.3ms. And do
						// not wait around for the cable charge timer to fire. Not sure why we don't wait except
						// that there is no real reason to wait. We're here just to enregize the cable. And that is
						// done.
						power_up_two_wire_cable( task_watchdog_time_stamp, (false) );	
					}
					else
					{
						// 1/29/2013 rmd : Returns immediately. The bus has been powered down!
						power_down_two_wire_cable();
					}
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_perform_discovery_process:
	
					perform_discovery( task_watchdog_time_stamp );
					
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_end_special_activation_mode:
				
					alert_message_to_tpmicro_pile( "special activation timer fired" );
	
					// 10/16/2013 rmd : The output was turned ON 1 minute ago. To prevent it from running for a
					// long time (till a power failure or user returns and turns OFF) exit this mode and resume
					// regular turn ON practice.
					exit_special_activation_mode();
					
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_decoder_solenoid_operation:
					
					if( !twtq.decoder_operation.on )
					{
						alert_message_to_tpmicro_pile( "locater service OFF" );
	
						// 10/16/2013 rmd : The end result is we are going to exit the special activation mode so we
						// don't need this timer running anymore.
						xTimerStop( two_wire_bus_info.special_activation_timer, portMAX_DELAY );
	
						exit_special_activation_mode();
					}
					else
					{
						alert_message_to_tpmicro_pile( "locater service ON" );
	
						// ----------
	
						// 10/16/2013 rmd : So the message is asking us to turn 1 ON. Make sure all others are OFF.
						// And turn ON the 1.
						for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
						{
							// 10/15/2013 rmd : Only allow the special solenoid activation for the active station
							// decoders in the list.
							if( (decoder_list[ ddd ].sn != NULL) && (decoder_list[ ddd ].id_info.decoder_type__tpmicro_type == DECODER__TPMICRO_TYPE__2_STATION) )
							{
								// 10/15/2013 rmd : For each decoder there are two outputs to work through.
								UNS_32	which_solenoid;
								
								for( which_solenoid=DECODER_OUTPUT_A_BLACK; which_solenoid<=DECODER_OUTPUT_B_ORANGE; which_solenoid++ )
								{
									// ----------
									
									UNS_8	*output_control_ptr;
									
									if( which_solenoid == DECODER_OUTPUT_A_BLACK )
									{
										output_control_ptr = &(decoder_info[ ddd ].output_A.control);
									}
									else
									{
										output_control_ptr = &(decoder_info[ ddd ].output_B.control);
									}
									
									// ----------
				
									// 10/15/2013 rmd : If this is the decoder and output the queue message is after set it
									// up.
									if( (decoder_list[ ddd ].sn == twtq.decoder_operation.sn) && (which_solenoid == twtq.decoder_operation.output))
									{
										// ----------
					
										// 10/16/2013 rmd : ENTER the special activation mode.
										two_wire_bus_info.special_activation_mode = (true);
					
										// ----------
	
										// 10/15/2013 rmd : Wether if it is presently ON or OFF doesn't matter, set the valve state
										// to OFF. As a matter of fact clear ALL the bits. Remember we suppress processing of
										// incoming commands from the main board to turn back ON/OFF outputs while in the special
										// activation mode.
										*output_control_ptr = 0;
		
										// 10/15/2013 rmd : Now set the desired state that the queue item delivered. And set the COS
										// bit to cause the command to be sent.
										B_SET( *output_control_ptr, OUTPUT_CONTROL_BIT_IS_ON );
		
										B_SET( *output_control_ptr, OUTPUT_CONTROL_BIT_COS_DETECTED );
	
										// ----------
	
										// 10/15/2013 rmd : Set the timer to 1 minute. When the timer expires it clears the special
										// activation mode, clears this bit in this output control, and sets the COS bit to cause
										// the turn OFF command to be issued to the decoder. If instructions come in from the main
										// board prior to issuing the OFF command, to turn ON the output, well then the end result
										// is it will remian ON.
										xTimerChangePeriod( two_wire_bus_info.special_activation_timer, MS_to_TICKS( SPECIAL_ACTIVATION_OUTPUT_ON_DURATION ), portMAX_DELAY );
	
										// ----------
										
										// 10/16/2013 rmd : And just in case clear the INOPERATIVE bit. If it happened to be set the
										// output would not come ON. The command would be effectively dropped.
										B_UNSET( decoder_list[ ddd ].decoder_status, DECODER_STATUS_DECODER_INOPERATIVE );
									}
									else
									{
										// 10/15/2013 rmd : If we've received this queue event, ordering us to use the special
										// activation to turn ON an output, I've decided all others ON are to be turned OFF. And
										// only the one output involved in the queue message is to be turned ON.
										if( B_IS_SET( *output_control_ptr, OUTPUT_CONTROL_BIT_IS_ON ) )
										{
											B_UNSET( *output_control_ptr, OUTPUT_CONTROL_BIT_IS_ON );
		
											B_SET( *output_control_ptr, OUTPUT_CONTROL_BIT_COS_DETECTED );
										}
									}
		
								}  // of for each output
		
							}  // of if active and a 2 station decoder
							
						}  // for all decoders
						
					}  // of if the message wants to turn 1 ON
					
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_set_serial_number:
					
					// 5/7/2013 rmd : And protect the two_wire queue from filling while this long activity takes
					// place.
					two_wire_bus_info.task_is_busy = (true);
					
					// ----------
				
					if( !two_wire_bus_info.cable_is_powered )
					{
						power_up_two_wire_cable( task_watchdog_time_stamp, (true) );
					}
				
					// ----------
	
					build_set_serial_number_msg( twtq.decoder_operation.sn );
	
					// 1/29/2013 rmd : Make it happen.
					perform_two_wire_io();
	
					// ----------
	
					if( two_wire_io_control.response_result == TWO_WIRE_RESULT_ACK_DETECTED )
					{
						decoder_alert_message_to_tpmicro_pile( twtq.decoder_operation.sn, "Successful : serial number set" );
					}
					else
					{
						decoder_alert_message_to_tpmicro_pile( twtq.decoder_operation.sn, "Error setting serial number" );
					}
	
					// ----------
				
					// 6/5/2013 rmd : Because this process is performed outside of the normal two-wire cable
					// power management scheme, when done the bus power is immediately turned OFF if there are
					// no other activities to do. It just feels better to give the decoders some dwell time with
					// the power ON.
					vTaskDelay( MS_to_TICKS(3000) );
					
					// ----------
				
					two_wire_bus_info.task_is_busy = (false);
					
					// ----------
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_clear_statistics_at_all_decoders:
					
					// 5/7/2013 rmd : And protect the two_wire queue from filling while this long activity takes
					// place.
					two_wire_bus_info.task_is_busy = (true);
					
					// ----------
				
					if( !two_wire_bus_info.cable_is_powered )
					{
						power_up_two_wire_cable( task_watchdog_time_stamp, (true) );
					}
				
					// ----------
	
					// 5/7/2013 rmd : Though this clearing activity is relatively quick there are two concerns.
					// First is attempting to send to the main board the updated statistics while we are in the
					// process of clearing them. And secondly if we did have 128 to do would that take TOO long?
					// Using the mutex deals with all that.
					xSemaphoreTake( decoder_statistics_MUTEX, portMAX_DELAY );
					
					// ----------
	
					for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
					{
						if( (decoder_list[ ddd ].sn != NULL) && (!adc_readings.two_wire_cable_excessive_current) && (!adc_readings.two_wire_terminal_over_heated) )
						{
							build_statistics_clear_msg( decoder_list[ ddd ].sn );
							
							perform_two_wire_io();
	
							// ----------
	
							vTaskDelay( MS_to_TICKS(100) );
	
							// ----------
	
							/*
							// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
							// just temporarily tacked on so we could restore the defaults - something we needed to do 
							// to force the new lower solenoid turn ON voltage limit into effect
	
							build_restore_user_parameters_to_defaults( decoder_list[ ddd ].sn );
							
							perform_two_wire_io();

							// 5/7/2013 rmd : Give line some re-charge time.
							vTaskDelay( MS_to_TICKS(100) );
							*/
							
							// ----------
							
							// 8/3/2015 rmd : See note in discovery process function about these discrete watchdog time
							// stamp updates. Basically this process freezes this task in the eyes of the watchdog
							// monitoring task so we have to update the task time stamp in several of these places.
							// Which is less than ideal but I don't feel like a reqrite of these processes right now.
							*task_watchdog_time_stamp = xTaskGetTickCount();
						}
						else
						{
							break;
						}
					}
					
					// ----------
	
					xSemaphoreGive( decoder_statistics_MUTEX );
					
					// ----------
				
					// 6/5/2013 rmd : Because this process is performed outside of the normal two-wire cable
					// power management scheme, when done the bus power is immediately turned OFF if there are
					// no other activities to do. Which usually there isn't. I'm concerned that the last decoder
					// may not have completed it's save to EE of the discovery confirmation state. Wait here
					// three seconds to ensure.
					vTaskDelay( MS_to_TICKS(3000) );
					
					// ----------
	
					two_wire_bus_info.task_is_busy = (false);
					
					// ----------
	
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_request_statistics_from_all_decoders:
					
					// 5/7/2013 rmd : And protect the two_wire queue from filling while this long activity takes
					// place.
					two_wire_bus_info.task_is_busy = (true);
					
					// ----------
				
					if( !two_wire_bus_info.cable_is_powered )
					{
						power_up_two_wire_cable( task_watchdog_time_stamp, (true) );
					}
				
					// ----------
	
					// 4/16/2013 rmd : Getting the statistics from all the decoders is a LONG WINDED activity.
					// During the whole process we hold the MUTEX which prevent the communications from
					// attempting to send the statistics results to the main board till all complete. The
					// communications task is very careful not to block on this MUTEX as we attempt the TAKE
					// with a 0 wait time. Here however we do BLOCK until the MUTEX becomes available.
					xSemaphoreTake( decoder_statistics_MUTEX, portMAX_DELAY );
	
					for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
					{
						if( (decoder_list[ ddd ].sn != NULL) && (!adc_readings.two_wire_cable_excessive_current) && (!adc_readings.two_wire_terminal_over_heated) )
						{
							build_statistics_request_msg( decoder_list[ ddd ].sn );
	
							// 1/31/2013 rmd : As part of the response will automatically mark results to be sent to the
							// main board.
							perform_two_wire_io();
	
							if( two_wire_io_control.response_result == TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH )
							{
								decoder_alert_message_to_tpmicro_pile( decoder_list[ ddd ].sn, "Successful STATS" );
							}
							else
							{
								decoder_alert_message_to_tpmicro_pile( decoder_list[ ddd ].sn, "Error getting full STATS" );
							}
							
							// 5/7/2013 rmd : Give line some re-charge time.
							vTaskDelay( MS_to_TICKS(300) );		

							// ----------
							
							// 8/3/2015 rmd : See note in discovery process function about these discrete watchdog time
							// stamp updates. Basically this process freezes this task in the eyes of the watchdog
							// monitoring task so we have to update the task time stamp in several of these places.
							// Which is less than ideal but I don't feel like a reqrite of these processes right now.
							*task_watchdog_time_stamp = xTaskGetTickCount();
						}
						else
						{
							break;
						}
					}
	
					xSemaphoreGive( decoder_statistics_MUTEX );
					
					// ----------
				
					// 6/5/2013 rmd : Because this process is performed outside of the normal two-wire cable
					// power management scheme, when done the bus power is immediately turned OFF if there are
					// no other activities to do. It just feels better to give the decoders some dwell time with
					// the power ON.
					vTaskDelay( MS_to_TICKS(3000) );
					
					// ----------
				
					two_wire_bus_info.task_is_busy = (false);
					
					// ----------
				
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_start_all_decoder_loopback_test:
					
					// 5/7/2013 rmd : And protect the two_wire queue from filling while this long activity takes
					// place.
					two_wire_bus_info.task_is_busy = (true);
					
					// ----------
				
					if( !two_wire_bus_info.cable_is_powered )
					{
						power_up_two_wire_cable( task_watchdog_time_stamp, (true) );
					}
				
					// ----------
					
					// 4/16/2013 rmd : Getting the statistics from all the decoders is a LONG WINDED activity.
					// During the whole process we hold the MUTEX which prevent the communications from
					// attempting to send the statistics results to the main board till all complete. The
					// communications task is very careful not to block on this MUTEX as we attempt the TAKE
					// with a 0 wait time. Here however we do BLOCK until the MUTEX becomes available.
					xSemaphoreTake( decoder_statistics_MUTEX, portMAX_DELAY );
	
					for( ddd=0; ddd<MAX_DECODERS_IN_A_BOX; ddd++ )
					{
						if( (decoder_list[ ddd ].sn != NULL) && (!adc_readings.two_wire_cable_excessive_current) && (!adc_readings.two_wire_terminal_over_heated) )
						{
							two_wire_io_control.lb_data_length = MAX_LOOPBACK_DATA_LENGTH;
	
							// 6/26/2013 rmd : Because the DLE character triggers byte stuffing in the outgoing and
							// response messages do not allow that character. It grows the message content considerably
							// - by almost twice.
							if( ddd == DLE )
							{
								memset( &two_wire_io_control.lb_data, (ddd+1), MAX_LOOPBACK_DATA_LENGTH );
							}
							else
							{
								memset( &two_wire_io_control.lb_data, ddd, MAX_LOOPBACK_DATA_LENGTH );
							}
							
							build_data_loopback_msg( decoder_list[ ddd ].sn, (UNS_8*)two_wire_io_control.lb_data, two_wire_io_control.lb_data_length );
							
							perform_two_wire_io();
	
							if( two_wire_io_control.response_result == TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH )
							{
								decoder_alert_message_to_tpmicro_pile( decoder_list[ ddd ].sn, "Loopback OK" );
							}
							else
							{
								decoder_alert_message_to_tpmicro_pile( decoder_list[ ddd ].sn, "Loopback FAILED" );
							}
							
							// 5/7/2013 rmd : Give line some re-charge time.
							vTaskDelay( MS_to_TICKS(500) );		

							// ----------
							
							// 8/3/2015 rmd : See note in discovery process function about these discrete watchdog time
							// stamp updates. Basically this process freezes this task in the eyes of the watchdog
							// monitoring task so we have to update the task time stamp in several of these places.
							// Which is less than ideal but I don't feel like a reqrite of these processes right now.
							*task_watchdog_time_stamp = xTaskGetTickCount();
						}
						else
						{
							break;
						}
					}
	
					xSemaphoreGive( decoder_statistics_MUTEX );
					
					// ----------
				
					// 6/5/2013 rmd : Because this process is performed outside of the normal two-wire cable
					// power management scheme, when done the bus power is immediately turned OFF if there are
					// no other activities to do. It just feels better to give the decoders some dwell time with
					// the power ON.
					vTaskDelay( MS_to_TICKS(3000) );
					
					// ----------
				
					two_wire_bus_info.task_is_busy = (false);
					
					// ----------
	
					break;
	
				// ----------
	
				case TWO_WIRE_TASK_QUEUE_COMMAND_stop_all_decoder_loopback_test:
					
					break;
	
				// ----------
	
			}  // of switch statement
	
		}  // of we got a queue event

	}  // of task while true
	
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
#if 0

	//Experimental task code to explore decoder bus timing. Very useful stuff man!
	
	#if 0
	INT_8	part_addr_bit;

	UNS_32	part_addr;

	UNS_8	mask_len;
	#endif
			
	UNS_8		test_byte;

	while( (true) )
	{
		// 4/16/2013 rmd : At a minimum this queue is feed with each incoming message from main (to
		// deliver the new triac states). The is concern during long two-wire operation that this
		// queue will overflow.
		xQueueReceive( two_wire_task_queue, &twtq, portMAX_DELAY );
		
		// ----------

		switch( twtq.command )
		{
			// ----------

			// 4/22/2013 rmd : We attempt to execute another command to a decoder 4 times each second
			// tied to a 250ms FreeRTOS timer. The timer is restarted after the decoder command exchange
			// has completed.
			case TWO_WIRE_TASK_QUEUE_COMMAND_activity_go_ahead:

				// ----------
			
				#if 0

				// 1/31/2013 rmd : This is a BROADCAST message to all decoders. There is no response.
				build_discovery_reset_msg();
			
				perform_two_wire_io();
			
				// ----------

				// initialize variables for discovery
				part_addr_bit = SN_HIGH_ADDR_BIT;
				
				part_addr = 0;
			
				//part_addr_mask = (1 << part_addr_bit);
			
				do
				{
					// Calculate mask length
					mask_len = SN_HIGH_ADDR_BIT - part_addr_bit + 1;
			
					/////////////////////
					// Send ENQ command
					/////////////////////
			
					// 1/31/2013 rmd : This is a MULTICAST message. The response is a ACK PULSE. Which may or
					// may not show up. It is not an error if the ack pulse is no show.
					build_enquiry_msg( part_addr, mask_len );
					
					perform_two_wire_io();
					
					/ *
					if( two_wire_io_control.response_result == TWO_WIRE_RESULT_ACK_DETECTED )
					{
						hit = (true);
					}
					else
					{
						// Complement (clear) last addr bit set and move on
						part_addr ^= (1 << part_addr_bit);
					}
			
					// Calculate next addr, mask
					part_addr_bit--;
			
    				part_addr_mask |= (part_addr_mask >> 1); 
					* /
					
					// ----------
					
					// 4/17/2013 rmd : While this may slow down the discovery process we need to give the other
					// tasks a chance at processing their queue messages. Else they will fill up.
					vTaskDelay( MS_to_TICKS(50) );		
			
					// ----------
					
				//} while( part_addr_bit >= 0 );
				} while( (true) );

				#endif


				#if 0

				do
				{

					//build_status1_request_msg( 0xDEAD2 );

					build_status1_request_msg( 0x0400 );
					
					perform_two_wire_io();
					
					// ----------
					
					// 4/17/2013 rmd : While this may slow down the discovery process we need to give the other
					// tasks a chance at processing their queue messages. Else they will fill up.
					vTaskDelay( MS_to_TICKS(50) );		
			
					// ----------
					
				} while( (true) );

				#endif
				

				// ----------
				// ----------

				//#if 0

				power_up_two_wire_cable();
				
				// ----------
				
			
				// ----------
				
				test_byte = 0x55;
			
				do
				{
					switch_and_wait_for_bus_to_enter_data_mode();
	
					vTaskDelay( MS_to_TICKS( MARK_DLY_MS_BEFORE_ASYNC_DATA ) );

					two_wire_io_control.pTxData = (UNS_8*)&test_byte;

					two_wire_io_control.TxByteCnt = 1;

					two_wire_send_message();
					
					while( U1LSR_TEMT_BITBAND == 0 );
					
					turn_off_the_bus_NOW_and_setup_to_transition_to_off_mode();
	
					vTaskDelay( MS_to_TICKS(30) );		

					switch_and_wait_for_bus_to_enter_power_mode();
					
					vTaskDelay( MS_to_TICKS(60) );		

				} while( (true) );

				//#endif

				break;

			// ----------

			case TWO_WIRE_TASK_QUEUE_COMMAND_irrigation_states_from_main:

				break;

			// ----------

			case TWO_WIRE_TASK_QUEUE_COMMAND_control_bus_power:
				break;

			// ----------

			case TWO_WIRE_TASK_QUEUE_COMMAND_perform_discovery_process:
				break;

			// ----------

			case TWO_WIRE_TASK_QUEUE_COMMAND_decoder_solenoid_operation:
				break;

			// ----------

			case TWO_WIRE_TASK_QUEUE_COMMAND_clear_statistics_at_all_decoders:
				break;

			// ----------

			case TWO_WIRE_TASK_QUEUE_COMMAND_request_statistics_from_all_decoders:
				break;

			// ----------

			case TWO_WIRE_TASK_QUEUE_COMMAND_start_all_decoder_loopback_test:
				break;

			// ----------

			case TWO_WIRE_TASK_QUEUE_COMMAND_stop_all_decoder_loopback_test:
				break;

			// ----------

		}


	}  // of task while true

#endif
*/


		/*
		vTaskDelay( 1000 );

		sn = 0xAAAAAAAA;

		// Let's build and send a OP_STAT_REQ command message
		bldStat1ReqMsg( sn );
		
		// Fill in required fields in the IO request block
		//g_TwoWireUartCtl_s.req_type     = UNICAST_TYPE_e;
		g_TwoWireUartCtl_s.req_type     = BROADCAST_TYPE_e;
		g_TwoWireUartCtl_s.max_rsp_len  = MAX_2_WIRE_RX_MSG_LEN;
		g_TwoWireUartCtl_s.timeout_ms   = 140;                    // A guess
		
		// Go perform the two wire serial I/O
		PerformTwoWireIo();
		*/
		
		//sn = 0xFACE1;        // Serial number for testing


		/*
		
		#if BUILD_STAT1_MSG != 0

			UNS_32 sn;

			// Let's build and send a OP_STAT_REQ command message
			sn = 0xDEAD2;

			//build_stat1_request_msg( sn );

			build_solenoid_control( sn, 0 , !solenoid_is_ON );
			solenoid_is_ON = !solenoid_is_ON;
			
			// Go perform the two wire serial I/O
			perform_two_wire_io();

		#endif
		*/
		
		/*
		#if BUILD_ENQ_MSG != 0

			perform_discovery();
			

			build_discovery_reset_msg(); 
		
			perform_two_wire_io();


			// Build an ENQ message with all 20 bits compared
			build_enquiry_msg( sn, 20 );
			
			// Go perform the two wire serial I/O
			perform_two_wire_io();
			


			// Now check the result
			// io_result = TwoWireUartCtl_s.result;
			
			vTaskDelay( MS_to_TICKS(100) );
			
			// Build an ENQ message with all 20 bits compared
			bldEnquireMsg( sn - 1, 20 );
			
			// Fill in required fields in the IO request block
			g_TwoWireUartCtl_s.req_type     = MULTICAST_TYPE_e;
			g_TwoWireUartCtl_s.max_rsp_len  = MAX_2_WIRE_RX_MSG_LEN;
			g_TwoWireUartCtl_s.timeout_ms   = 0;                    // Set by driver for MULTICAST messages
			
			// Go perform the two wire serial I/O
			perform_two_wire_io();
			
			// Now check the result
			//io_result = TwoWireUartCtl_s.result;
			

		#endif
		*/
				/*
				// 5/17/2013 rmd : To test the IAP 'disable interrupts to write to ee' bug we found in the 
				decoder we loop on the discovery commands. 
				
				two_wire_task_is_busy = (true);

				while( 1 )
				{
					TPM_LED2_ON;

					build_discovery_reset_msg(); 
				
					perform_two_wire_io();
		
					vTaskDelay( MS_to_TICKS(50) );		

					// ----------
					
					// Build an ENQ message with all 20 bits compared
					//build_enquiry_msg( 0x401, 20 );
					
					// Go perform the two wire serial I/O
					//perform_two_wire_io();
		
					//vTaskDelay( MS_to_TICKS(50) );		
					
					// ----------
					
					build_discovery_confirmation_msg( 0x401 );
					
					perform_two_wire_io();

					// ----------

					TPM_LED2_OFF;
					
					vTaskDelay( MS_to_TICKS(100) );		

					// ----------

					//vTaskDelay( MS_to_TICKS(400) );		
					//build_solenoid_control( 0x401, 0, (true) );
					//perform_two_wire_io();
					
					// ----------
				}

				two_wire_task_is_busy = (false);
				*/ 
		
