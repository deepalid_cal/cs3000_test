/* ---------------------------------------------------------- */

#ifndef INC_TWO_WIRE_UART_H_
#define INC_TWO_WIRE_UART_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"

#include	"x_uart.h"

#include	"two_wire_io.h"

#include	"two_wire_task.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//-----------------------------------------------------------------------------
//    This is the control structure for the 2-wire UART port
//-----------------------------------------------------------------------------
typedef struct
{
	IO_REQ_TYPE_e	req_type;							// I/O request type (enum)

	UNS_32			op_code;							// for tracking what to do with the response data
	
	// 1/31/2013 rmd : Used for error tracking during normal messaging. During BROADCAST and
	// MULTICAST messages the pointers will be NULL. Rightly so as those messages go to multiple
	// controllers.
	FIND_DECODER_STRUCT		fds;

	UNS_8			txbuf[ MAX_2_WIRE_TX_MSG_LEN ];		// transmit buffer

	UNS_8			*pTxData;							// Pointer to next byte to transmit

	UNS_8			TxByteCnt;							// Number of bytes left to transmit

	UNS_8			rxbuf[ MAX_DECODER_RESPONSE_LENGTH_IN_BYTES ];		// receive buffer

	UNS_8			actual_rsp_len;						// actual response length

	UNS_16			response_timeout_ms;				// response timeout (milliseconds)

	UNS_16			response_result;					// I/O result status

	xQueueHandle	response_queue;						// Handle of msg queue

	// 6/21/2013 rmd : Support for the decoder data loopback test.
	UNS_32			lb_data_length;
	
	UNS_8			lb_data[ MAX_LOOPBACK_DATA_LENGTH ];
	
} TWO_WIRE_IO_CONTROL_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern volatile TWO_WIRE_IO_CONTROL_STRUCT	two_wire_io_control;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void two_wire_uart_init( void );

extern void two_wire_send_message( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*TWOWIREUART_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

