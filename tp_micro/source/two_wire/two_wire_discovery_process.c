/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"two_wire_discovery_process.h"

#include	"two_wire_msg_builder.h"

#include	"two_wire_io.h"

#include	"two_wire_uart.h"

#include	"two_wire_task.h"

#include	"two_wire_fet_control.h"

#include	"tpmicro_comm_mngr.h"

#include	"eeprom_map.h"

#include	"adc.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Configure starting SN bit state
#define START_WITH_SN_BITS_SET  	(false)

// Here is the number of bits in the decoder serial number
#define DECODER_SN_BITS				(20)

// The highest order bit required for a serial number
#define SN_HIGH_ADDR_BIT			(DECODER_SN_BITS - 1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static BOOL_32 find_next_sn( UNS_32 *sn, BOOL_32 *pabort, BOOL_32 *pmax_number_found )
{
	// Returns TRUE if SN is found

	BOOL_32	hit;

	UNS_32	part_addr;

	UNS_32	part_addr_mask;

	INT_8	part_addr_bit;

	UNS_8	mask_len;

	// initialize variables for discovery
	part_addr_bit = SN_HIGH_ADDR_BIT;
	
	#if (START_WITH_SN_BITS_SET == (true))
		part_addr = (1<<part_addr_bit);
	#else
		part_addr = 0;
	#endif

	part_addr_mask = (1 << part_addr_bit);

	// Assume first that no decoder responds
	hit = (false);
	
	do
	{
		// Calculate mask length
		mask_len = SN_HIGH_ADDR_BIT - part_addr_bit + 1;

		/////////////////////
		// Send ENQ command
		/////////////////////

		// 1/31/2013 rmd : This is a MULTICAST message. The response is a ACK PULSE. Which may or
		// may not show up. It is not an error if the ack pulse is no show.
		build_enquiry_msg( part_addr, mask_len );
		
		perform_two_wire_io();
		
		if( two_wire_io_control.response_result == TWO_WIRE_RESULT_ACK_DETECTED )
		{
			hit = (true);
		}
		else
		{
			// Complement (clear) last addr bit set and move on
			part_addr ^= (1 << part_addr_bit);
		}

		// Calculate next addr, mask
		part_addr_bit--;

		part_addr_mask |= (part_addr_mask >> 1);

		// ----------
		
		// 4/17/2013 rmd : While this may slow down the discovery process we need to give the other
		// tasks a chance at processing their queue messages. Else they will fill up.
		vTaskDelay( MS_to_TICKS(20) );		

		// ----------
		
		// 11/6/2013 rmd : If a cable fault condition has occurred git out of here.
		if( adc_readings.two_wire_cable_excessive_current || adc_readings.two_wire_terminal_over_heated )
		{
			hit = (false);  // make sure its false

			break;
		}

	} while( (part_addr_bit >= 0) );

	if( hit )
	{
		// ----------

		// 1/31/2013 rmd : Let's pre-add to the list of discovered decoders. The discovery
		// confirmation command is SUPPOSED to work here. And the two-wire io process for such a
		// 'normal' command, if there are errors, counts on this sn being in the list. Else even
		// more errors are generated. So put it in now. If there is not an acceptable response to
		// this command we will remove it.
		decoder_list[ two_wire_bus_info.discovered_count ].sn = part_addr;

		two_wire_bus_info.discovered_count += 1;

		// ----------

		TPM_LED1_ON;
		
		// ----------

		// 4/9/2013 rmd : Issue the Discovery Confirmation message to decoder having this SN. Which
		// does two things. First is silences the decoder from continuing to participate in the
		// discovery process. And secondly it returns the decoder ID structure whihc contains the
		// decoder type. This structure is sent to the main board after the discovery process has
		// completed.
		build_discovery_confirmation_msg( part_addr );
		
		perform_two_wire_io();
		
		// Check if an valid response was received
		if( two_wire_io_control.response_result == TWO_WIRE_RESULT_RECEIVED_CORRECT_LENGTH )
		{
			// make assignment only if we got a hit
			*sn = part_addr;

			// 4/16/2013 rmd : So now the list item is ready for transmission to the main board.
			B_SET( two_wire_io_control.fds.list_ptr->decoder_status, DECODER_STATUS_SEND_DISCOVERED_TO_MAIN_BOARD );
		}
		else
		{
			decoder_alert_message_to_tpmicro_pile( part_addr, "Error at discovery confirmation" );
			
			// So we can see which serial num failed on.
			*sn = part_addr;
			
			// ----------
			
			// 1/31/2013 rmd : Back it out of the list. And do not send to main board. I suppose we
			// could leave it in the list but flag it as non-operational but choose not to.
			two_wire_bus_info.discovered_count -= 1;

			decoder_list[ two_wire_bus_info.discovered_count ].sn = NULL;
			
			B_UNSET( decoder_list[ two_wire_bus_info.discovered_count ].decoder_status, DECODER_STATUS_SEND_DISCOVERED_TO_MAIN_BOARD );
			
			// 4/29/2013 rmd : Don't need to say anything about decoder inoperative here. The serial
			// number is NULL so the decoder won't be used.
			
			// ----------
			
			*pabort = (true);
		}

		// ----------

		TPM_LED1_OFF;

		// ----------

		if( two_wire_bus_info.discovered_count >= MAX_DECODERS_IN_A_BOX )
		{
			*pmax_number_found = (true);
		}
	}

	return( hit );
}

/* ---------------------------------------------------------- */
extern void perform_discovery( UNS_32 *ptask_watchdog_time_stamp_ptr )
{
	UNS_32		sn;

	BOOL_32		abort, max_number_found;
	
	char		lstr_48[ 48 ];
	
	// ----------

	// 4/18/2013 rmd : This causes us to suspend processing of new incoming messages from the
	// main board. They cause all kinds of queue positing activity. Specifically for our own
	// two_wire_task queue. Which won't be processed until this process has completed. And that
	// may be up to 5 minutes. It seems we find a new decoder about every 4 seconds.
	two_wire_bus_info.task_is_busy = (true);
	
	// ----------

	// 4/16/2013 rmd : We must acquire this MUTEX to begin as we are zeroing out and rebuilding
	// the decoder list. Only the communication task could be holding this. And doesn't do so
	// long (long as compared to how long we are going to hold it during the discovery process!)
	// So we wait until it becomes available.
	xSemaphoreTake( decoder_list_MUTEX, portMAX_DELAY );

	// 4/16/2013 rmd : And guess what. We are also REDEFINING the decoder_info as well. So take
	// all the MUTEXES that protect that array.
	xSemaphoreTake( decoder_statistics_MUTEX, portMAX_DELAY );

	xSemaphoreTake( decoder_control_MUTEX, portMAX_DELAY );

	// ----------

	// 10/16/2013 rmd : Initialize the discovery process control variables. For now we only
	// perform a full discovery. Meaning we issue the discovery reset message so that all
	// decoders report in during the discovery process.

	// 5/22/2014 rmd : This flag signals when building a message to the main board to send
	// content about how many decoders were discovered so far.
	two_wire_bus_info.discovery_in_process = (true);
	
	two_wire_bus_info.full_discovery = (true);
	
	two_wire_bus_info.discovered_count = 0;

	two_wire_bus_info.send_main_the_discovery_process_results = (false);

	// ----------

	alert_message_to_tpmicro_pile( "START of decoder discovery process." );
	
	// ----------

	// 8/4/2015 rmd : Always clear the excessive current flag in preparation to start a
	// discovery process. Remember the overheat condition is self-clearing.
	adc_readings.two_wire_cable_excessive_current = (false);
	
	// 8/3/2015 rmd : Power up and wait for cable charge timer to fire.
	power_up_two_wire_cable( ptask_watchdog_time_stamp_ptr, (true) );
	
	// ----------

	// 1/31/2013 rmd : This is a BROADCAST message to all decoders. There is no response.
	build_discovery_reset_msg();

	perform_two_wire_io();

	// ----------

	// 1/31/2013 rmd : Since we just issued the discovery reset command, effectively there are
	// no known decoders on the bus. So we should zero out our decoder list and info
	// arrays.
	memset( &decoder_list, 0x00, sizeof(decoder_list) );

	memset( &decoder_info, 0x00, sizeof(decoder_info) );
	
	// ----------

	abort = (false);

	max_number_found = (false);

	// ----------

	// 8/3/2015 rmd : If there are no decoders on the cable we never make it to the update to
	// the time stamp inside of the while loop. Paste one in here to satisfy the watchdog
	// task.
	*ptask_watchdog_time_stamp_ptr = xTaskGetTickCount();

	// ----------

	// Continue while decoders are found and no COMM errors
	while( find_next_sn( &sn, &abort, &max_number_found ) )
	{
		if( abort )
		{
			// Display message to indicate no response to DISC CONF command
			snprintf( lstr_48, sizeof(lstr_48), "ERR Conf to S/N %06d (%02x)", sn, two_wire_io_control.response_result );

			alert_message_to_tpmicro_pile( lstr_48 );

			break;
		}
		else
		{
			snprintf( lstr_48, sizeof(lstr_48), "Decoder sn %06d found.", sn );
	
			alert_message_to_tpmicro_pile( lstr_48 );
		}

		// ----------

		if( max_number_found )
		{
			snprintf( lstr_48, sizeof(lstr_48), "DECODER COUNT AT FULL CAPACITY!!!" );
	
			alert_message_to_tpmicro_pile( lstr_48 );
			
			// 11/4/2014 rmd : And stop the discovery. That's all we an take.
			break;
		}

		// ----------
		
		// 4/17/2013 rmd : While this may slow down the discovery process we need to give the other
		// tasks a chance at processing their queue messages. Else they will fill up. And to let the
		// rest of the application at a lessor priority run. Remember the two_wire_task is a pretty
		// high priority. Like second or third from the top.
		vTaskDelay( MS_to_TICKS(100) );
		
		// 8/3/2015 rmd : Because this function does not return for potentially a long time (a
		// minute for example) the task itself does not have the opportunity to update its watchdog
		// time stamp. So we have to do that here otherwise the superviosry watchdog task would flag
		// this task as starved and force a tpmicro restart.
		*ptask_watchdog_time_stamp_ptr = xTaskGetTickCount();
	}

	// 5/22/2014 rmd : This flag signals when building a message to the main board to send
	// content about how many decoders were discovered so far.
	two_wire_bus_info.discovery_in_process = (false);

	// 10/16/2013 rmd : Okay send to the list to the main board.
	two_wire_bus_info.send_main_the_discovery_process_results = (true);
	
	// ----------

	xSemaphoreGive( decoder_control_MUTEX );

	xSemaphoreGive( decoder_statistics_MUTEX );

	// 4/16/2013 rmd : List has been developed.
	xSemaphoreGive( decoder_list_MUTEX );

	// ----------

	// 4/15/2013 rmd : Saving the decoder list now will save it with the send_this_discovered
	// flag set. Which is okay because when we read out the decoder_info on startup we make sure
	// to specifically clear this flag. That seems an easier approach than saving the
	// decoder_info after the communications task clears those flags.
	save_decoder_list_to_ee();

	// ----------

	if( two_wire_bus_info.discovered_count )
	{
		snprintf( lstr_48, sizeof(lstr_48), "END %u decoders total discovered.", two_wire_bus_info.discovered_count );

		alert_message_to_tpmicro_pile( lstr_48 );
	}
	else
	{
		alert_message_to_tpmicro_pile( "No decoders were discovered." );
	}
	
	// ----------

	// 6/5/2013 rmd : Because this process is performed outside of the normal two-wire cable
	// power management scheme, when done the bus power is immediately turned OFF if there are
	// no other activities to do. Which usually there isn't. I'm concerned that the last decoder
	// may not have completed it's save to EE of the discovery confirmation state. Wait here two
	// seconds to ensure.
	vTaskDelay( MS_to_TICKS(2000) );
	
	// ----------

	two_wire_bus_info.task_is_busy = (false);
	
	// ----------
}

/* ---------------------------------------------------------- */
extern void test_code( UNS_32 *ptask_watchdog_time_stamp_ptr )
{
	//UNS_32		sn;

	//BOOL_32		abort;
	
	//char		lstr_32[ 32 ];
	
	// ----------

	// 4/18/2013 rmd : This causes us to suspend processing of new incoming messages from the
	// main board. They cause all kinds of queue positing activity. Specifically for our own
	// two_wire_task queue. Which won't be processed until this process has completed. And that
	// may be up to 5 minutes. It seems we find a new decoder about every 4 seconds.
	two_wire_bus_info.task_is_busy = (true);
	
	// ----------

	// 10/16/2013 rmd : Initialize the discovery process control variables. For now we only
	// perform a full discovery. Meaning we issue the discovery reset message so that all
	// decoders report in during the discovery process.

	two_wire_bus_info.discovery_in_process = (true);
	
	two_wire_bus_info.full_discovery = (true);
	
	two_wire_bus_info.discovered_count = 0;

	two_wire_bus_info.send_main_the_discovery_process_results = (false);

	// ----------

	alert_message_to_tpmicro_pile( "START of decoder discovery process." );
	
	// ----------

	if( !two_wire_bus_info.cable_is_powered )
	{
		power_up_two_wire_cable( ptask_watchdog_time_stamp_ptr, (true) );
	}

	/*
	// ----------

	// 4/16/2013 rmd : We must acquire this MUTEX to begin as we are zeroing out and rebuilding
	// the decoder list. Only the communication task could be holding this. And doesn't do so
	// long (long as compared to how long we are going to hold it during the discovery process!)
	// So we wait until it becomes available.
	xSemaphoreTake( decoder_list_MUTEX, portMAX_DELAY );

	// 4/16/2013 rmd : And guess what. We are also REDEFINING the decoder_info as well. So take
	// all the MUTEXES that protect that array.
	xSemaphoreTake( decoder_statistics_MUTEX, portMAX_DELAY );

	xSemaphoreTake( decoder_control_MUTEX, portMAX_DELAY );

	*/
	// ----------


	UNS_32	iii;
	
	for( iii=0; iii<50; iii++ )
	{
		// 1/31/2013 rmd : This is a BROADCAST message to all decoders. There is no response.

		build_discovery_reset_msg();

		//build_statistics_request_msg( 1187 );

		perform_two_wire_io();


		/*
		if( adc_readings.two_wire_cable_excessive_current || adc_readings.two_wire_terminal_over_heated )
		{
			break;	
		}
		*/

		vTaskDelay( 20 );
	}

	// ----------
	/*
	
	// 1/31/2013 rmd : Since we just issued the discovery reset command, effectively there are
	// no known decoders on the bus. So we should zero out our decoder list and info
	// arrays.
	memset( &decoder_list, 0x00, sizeof(decoder_list) );

	memset( &decoder_info, 0x00, sizeof(decoder_info) );
	
	// ----------

	abort = (false);

	// ----------

	// Continue while decoders are found and no COMM errors
	while( find_next_sn( &sn, &abort ) )
	{
		if( abort )
		{
			// Display message to indicate no response to DISC CONF command
			snprintf( lstr_32, 32, "ERR Conf to S/N %06d (%02x)", sn, two_wire_io_control.response_result );

			alert_message_to_tpmicro_pile( lstr_32 );

			break;
		}
		else
		{
			snprintf( lstr_32, sizeof(lstr_32), "Decoder sn %06d found.", sn );
	
			alert_message_to_tpmicro_pile( lstr_32 );
		}

		// ----------
		
		// 4/17/2013 rmd : While this may slow down the discovery process we need to give the other
		// tasks a chance at processing their queue messages. Else they will fill up. And to let the
		// rest of the application at a lessor priority run. Remember the two_wire_task is a pretty
		// high priority. Like second or third from the top.
		vTaskDelay( MS_to_TICKS(100) );		

		// ----------
	}

	// ----------

	xSemaphoreGive( decoder_control_MUTEX );

	xSemaphoreGive( decoder_statistics_MUTEX );

	// 4/16/2013 rmd : List has been developed.
	xSemaphoreGive( decoder_list_MUTEX );


	// ----------

	// 4/15/2013 rmd : Saving the decoder list now will save it with the send_this_discovered
	// flag set. Which is okay because when we read out the decoder_info on startup we make sure
	// to specifically clear this flag. That seems an easier approach than saving the
	// decoder_info after the communications task clears those flags.
	save_decoder_list_to_ee();

	// ----------

	if( two_wire_bus_info.discovered_count )
	{
		snprintf( lstr_32, sizeof(lstr_32), "END %u decoders total discovered.", two_wire_bus_info.discovered_count );

		alert_message_to_tpmicro_pile( lstr_32 );
	}
	else
	{
		alert_message_to_tpmicro_pile( "No decoders were discovered." );
	}
	
	*/

	// ----------

	two_wire_bus_info.discovery_in_process = (false);

	// 10/16/2013 rmd : Okay send to the list to the main board.
	//              two_wire_bus_info.send_main_the_discovery_process_results = (true);
	
	// ----------

	// 6/5/2013 rmd : Because this process is performed outside of the normal two-wire cable
	// power management scheme, when done the bus power is immediately turned OFF if there are
	// no other activities to do. Which usually there isn't. I'm concerned that the last decoder
	// may not have completed it's save to EE of the discovery confirmation state. Wait here two
	// seconds to ensure.
	vTaskDelay( MS_to_TICKS(2000) );
	
	// ----------

	two_wire_bus_info.task_is_busy = (false);
	
	// ----------
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */