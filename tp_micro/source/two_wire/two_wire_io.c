/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"two_wire_io.h"

#include	"two_wire_uart.h"

#include	"two_wire_fet_control.h"

#include	"two_wire_receive.h"

#include	"tpmicro_comm_mngr.h"

#include	"tpmicro_alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/////////////////////////////////////////////////////////////////////
//    This function processes a Two-Wire I/O Request and
//    returns after the requested I/O has been completed.
//
//    The caller must first initialize most of the fields in the
//    global structure TwoWireUartCtl_s.
/////////////////////////////////////////////////////////////////////

// 1/27/2014 rmd : NOTE - this function must NOT BE optimized to preserve the timing of the
// stop bit. Follow the code and you'll see the section I'm talking about! Not optimizing
// is a 400 byte hit to code size but a necessity.
extern void __attribute__((optimize("O0"))) perform_two_wire_io( void )
{
	// 11/6/2013 rmd : Executed within the context of the two-wire task only. Safe to reference
	// both excessive_current and over_heated to 'back-out' of the process. Though it seems
	// completely unecessary to do so. Even though the bus may be OFF due to a detected cable
	// fault, this function will execute fine. I've tested this quite a bit.

	// ----------

	UNS_32		receive_status;

	BOOL_32		ack_detected;

	// ----------
	
	// 1/21/2013 rmd : Turn off power mode and switch into the data mode.
	during_io_switch_and_wait_for_bus_to_enter_data_mode();

	// ----------
	
	// Delay the configured time after entering DATA MODE before transmitting
	vTaskDelay( MS_to_TICKS( MARK_DLY_MS_BEFORE_ASYNC_DATA ) );

	// ----------
	
	// Point to start of command
	two_wire_io_control.pTxData = (UNS_8*)&two_wire_io_control.txbuf[ 0 ];

	// 1/22/2013 rmd : Send the complete msg. This function does not return till all the chars
	// have been sent! It could be awhile at 10ms per char! But that's okay. The alternative
	// would be to pend on a queue message and effectively the same thing would happen. We would
	// wait here till the tx was complete.
	two_wire_send_message();
	
	if( (two_wire_io_control.fds.info_ptr != NULL) && (two_wire_io_control.req_type == UNICAST_TYPE_e) )
	{
		two_wire_io_control.fds.info_ptr->comm_stats.unicast_msgs_sent += 1;	
	}
	
	// ----------

	TPM_LED3_ON;
	
	// ----------
	
	// 4/24/2013 rmd : Make the timing from the STOP bit till turning OFF the bus very
	// repeatable and tight. I could turn off interrupts but I have fear in doing that being it
	// is a relatively long time considering the baud rate. (Actually I tried it once and the
	// code froze. Not sure why and didn't track it down. Tried it again and the code didn't
	// freeze. However the interrupts are all quick usually posting to a queue. So with the
	// scheduler suspended we will be okay!
	//
	// 11/7/2013 rmd : UPDATE - Because the only two task priorites above us are the adc and the
	//timer task we do not need to block those from running. All other activities seem to only 
	//introduce maybe 100usec of jitter in getting the cable turn OFF. And thats fine. The ACK 
	//pulse doesn't show for maybe 2ms. And a response is out at about 3ms. So DO NOT SUSPEND 
	//ALL TASKS.
	
	// 1/21/2013 rmd : The last byte of the command is still going out, so we must delay one
	// character time at 1200 baud. That would be 8.33 ms. Or monitor the TEMT bit in the LSR
	// which does a pretty good job. But flips true, meaning empty, after only 420usec of the
	// stop bit has been transmitted. Wait for UART to go unbusy (THRE and TSR empty).
	while( U1LSR_TEMT_BITBAND == 0 );
	
	// ----------
	
	// 1/22/2014 rmd : And more delay to round out the stop bit!!! I discovered that decoders
	// would ocassionally miss commands. And tracked it down to the decoder UART was not picking
	// up the last byte of a command (0x04). And would therefore miss the whole command. It
	// seemed having 6 valves ON aggrevated this problem. And perhaps being at the end of a long
	// run also made things worse. I was able to easily reproduce without this delay using 6
	// valves ON at the end of 3000 feet of cable.
	//
	// 1/22/2014 rmd : A note on the values. A count of 800 seems to work. Therefore for added 
	// margin I am going to use 1250. This produces a good solid stop bit on the order of
	// 700usec. I think the key is to give the receiving UART a stop bit with at least a width
	// of half a bit time. At 1200 baud the bit time is 833usec. On another note, I have watched
	// extensively on the scope and this seems perfectly safe in regards to floating the bus
	// before the decoder begins his response.

	// 1/27/2014 rmd : NOTE - this is the code we DO NOT want the optimizer to touch. We achieve
	// this at the expense of not optimizing the whole function because our version of GCC does 
	// not support the #pragma GCC push_options / #pragma GCC optimize ("O0") / #pragma 
	// pop_options sequence.

	volatile UNS_32 cnt;
	
	cnt = 1250;

	while (cnt--);
	
	// ----------
	
	// 4/24/2013 rmd : Pending on the TEMT bit produces a STOP bit of around 420usec. Meaning it
	// isn't quite done yet because at 1200 baud the bit time is 833usec. The interesting thing
	// is the LPC based decoder doesn't seem to mind the missing half of the stop bit. BUT WHY.
	// One reason could be the sample point is half way. But at 420usec we are way too close for
	// comfort. THE REAL REASON is the receiver at the decoder. Even after the bus is floated
	// (not driven) the hysteresis of the receiver guarantees the stop bit continues. The
	// received state does not change just cause the bus has been floated. And this is very
	// good. And desireable.

	// ----------
	
	// 1/24/2013 rmd : An interesting note. After the data is sent the bus is inherently in the
	// negative position, meaning the neg fet is on. That's the idle state. When the decoder
	// takes over the bus the first thing it does is to also drive the bus negative. Matching
	// what we already have. The name of the game is however is that we have to get off the bus
	// (turn off our negative fet) BEFORE the decoder sends out it's first start bit of the
	// response. Turning off, before the decoder drives the bus, causes the negative rail to
	// jump up to gnd and then be driven back negative again by the decoder. Sometimes this
	// appears as just a small spike. That doesn't seem to hurt anything. That spike can be
	// eliminated by increasing the above delay to 6ms. But you may run the risk of encroaching
	// upon when the decoder drives the bus positive at the first start bit of the first byte of
	// the response. So I've left at 2 for now.
	
	// 1/25/2013 rmd : BUT when you look at the MULTICAST command that responds with the ACK
	// pulse the story is different. The decoder does not respond with a neg fet being driven.
	// So we definately have to be OFF the bus when the decoder responds (and it could be
	// multiple decoders). And it seems the decoders respond a bit more quickly. As tight as 3ms
	// after the START of the stop bit of the last command byte.
	
	// 4/24/2013 rmd : Note in the LPC CM0 based decoder the decoder begins to drive the bus
	// with a response within about 300usec. The ACK pulse appears within 200usec.

	// ----------
	
	// 1/21/2013 rmd : Turn off the bus. Float it so the decoder can respond. This function
	// returns immediately. It does not wait for a 60 hz crossing. And that is what we need
	// here. A response is coming!!
	during_io_turn_off_the_bus_NOW_and_setup_to_transition_to_off_mode();

	// ----------
	
	TPM_LED3_OFF;

	// ----------

	// 1/25/2013 rmd : For all commands prepare for a response. Even though one may not be
	// coming. Sets the response to the default NO_RESPONSE.
	tw_receive_init();

	// Flush the receive response queue.
	UNS_32	dummy;

	while( pdTRUE == xQueueReceive( two_wire_io_control.response_queue, &dummy, 0 ) );
	
	// ----------

	// Now proceed based upon what type of message was sent
	if( two_wire_io_control.req_type == MULTICAST_TYPE_e )
	{
		UNS_32	consecutive_spaces, samples;

		///////////////////////////////////////////////////////////////////////////////////////
		//    Here we expect no response or an ACK pulse to be received.
		//    An ACK pulse would (should) cause a MARK (1) - SPACE (0) - MARK (1) transition
		//    on the GPIO pin with the SPACE condition lasting for at least one character
		//    time at 1200 baud (8.333 ms).
		//////////////////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////////////////
		// For the ACK detection, we do not need to reconfigure the RXD pin into a generic GPIO
		// input. For the case of the LPC17xx we can always read the pin state using the FIOPIN
		// register not matter the pin assignment (there are a few exceptions like when assigned as
		// an A-D input).
		//////////////////////////////////////////////////////////////////////////////////////

		// 1/25/2013 rmd : For ACK detection we always only wait a total of 15ms. The ACK usually
		// shows within the first ms after the bus has been floated. And lasts approx 10ms. So
		// within 15ms its all over and done with or it ain't gonna show.
		samples = 15;

		// Clear # consecutive spaces seen during polling period
		consecutive_spaces = 0;

		ack_detected = (false);  // No ACK detected yet

		while( samples )
		{
			vTaskDelay( MS_to_TICKS( 1) );

			// Check the RXD pin input state (Note: 0 == spacing)
			if( (((LPC_GPIO_TypeDef*)LPC_GPIO2)->FIOPIN & _BIT(1)) == 0 )
			{
				//////////////////////////////////
				// We read a spacing line state
				//////////////////////////////////

				// Bump # consecutive spaces
				consecutive_spaces++;

				if( consecutive_spaces >= CONSECUTIVE_SPACES_FOR_ACK_DETECT )
				{
					ack_detected = (true);

					break;
				}
			}
			else
			{
				// Clear # consecutive spaces
				consecutive_spaces = 0;
			}

			samples--;
		}

		// 1/25/2013 rmd : It would be nice to sit it out here and wait for the ACK pulse to end.
		// However because of the hysteresis in the hardware when the ACK pulse ends the decoder
		// floats the bus. And so the RX input does not switch back to MARKING again. It stays at
		// SPACING. That's okay. We'll just wait out our 15ms. And after that it will be safe to
		// re-power the bus.
		
		// We will only execute this "while" if we detected an ACK pulse.
		while( samples-- )
		{
			// Wait until RXD pin goes high (marking) again
			vTaskDelay( MS_to_TICKS( 1 ) );
		}

		if( ack_detected )
		{
			// We detected an ACK. And by the nature of it could be from one or more decoders.
			two_wire_io_control.response_result = TWO_WIRE_RESULT_ACK_DETECTED;
		}
		
	}  // end if MULTICAST_TYPE_e
	else
	if( two_wire_io_control.req_type == UNICAST_TYPE_e )
	{
		// 1/23/2013 rmd : Hey we have to enable the UART interrupt. Carefully here. First clear out
		// any bytes in the RBR and toss em. Notice the unused attribute to supress the compiler
		// warning about setting a variable but not using it.
		UNS_32	throw_away __attribute__((__unused__));
		
		while( U1LSR_RDR_BITBAND == 1 )
		{
			throw_away = ((LPC_UART1_TypeDef*)LPC_UART1)->RBR;
		}
		
		NVIC_ClearPendingIRQ( UART1_IRQn );  // to be thorough
		
		NVIC_EnableIRQ( UART1_IRQn );
		
		// ----------

		// 1/25/2013 rmd : Check to make sure it is something reasonable. Otherwise imagine if it
		// was 10. We would wait only a short time. Quit. And re-power the bus while the decoder was
		// still xmitting a response. This shouldn't be needed but provides a level of
		// re-assurance.
		if( two_wire_io_control.response_timeout_ms <= 140 )
		{
			two_wire_io_control.response_timeout_ms = 140;
		}

		//////////////////////////////////////////////////////////////////////
		// Here we expect a standard 1200 baud ASYNC response or no response
		//////////////////////////////////////////////////////////////////////
		if( xQueueReceive( two_wire_io_control.response_queue, &receive_status, MS_to_TICKS( two_wire_io_control.response_timeout_ms ) ) )
		{
			// We got an event message from the two wire UART driver (could be zero length!)
			if( receive_status == TWO_WIRE_IO_CONTROL_QUEUE_RESP_RECEIVED )
			{
				////////////////////////////////////////////
				// Now check for possible error conditions
				////////////////////////////////////////////

				// Did we get ANY bytes within the protocol framing?
				if( two_wire_io_control.actual_rsp_len == 0 )
				{
					two_wire_io_control.response_result = TWO_WIRE_RESULT_NO_RESPONSE;

					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					if( two_wire_io_control.fds.info_ptr != NULL )
					{
						two_wire_io_control.fds.info_ptr->comm_stats.unicast_no_replies += 1;
					}
				}
				///////////////////////////////////////////////////////////
				// We got SOMETHING. But did we receive a valid response?
				// It has to be at least 3 characters (CRC16 + data)
				///////////////////////////////////////////////////////////
				else
				if( two_wire_io_control.actual_rsp_len < 3 )
				{
					////////////////////////////////////////////////////
					// This is a short/invalid response - a CRC16 check
					// cannot even be performed on this message
					////////////////////////////////////////////////////
					two_wire_io_control.response_result = TWO_WIRE_RESULT_LENGTH_LESS_THAN_THREE;

					// 1/31/2013 rmd : This is an error. The response length is the wrong size.
					if( two_wire_io_control.fds.info_ptr != NULL )
					{
						two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_length_errs += 1;
					}
				}
				else
				{
					///////////////////////////////////////////////////
					// OK, we can check the CRC16 for this message
					//////////////////////////////////////////////////
					if( bMsgHasGoodCrc( (UNS_8*)&two_wire_io_control.rxbuf, two_wire_io_control.actual_rsp_len ) )
					{
						// 4/16/2013 rmd : Go look at the data in detail and set RESULT_PASSED if all correct.
						move_response_data_to_appropriate_structure();
					}
					else
					{
						// The CRC16 was not correct
						two_wire_io_control.response_result = TWO_WIRE_RESULT_RESP_BAD_CRC;

						// 1/31/2013 rmd : This is an error. The response length is the wrong size.
						if( two_wire_io_control.fds.info_ptr != NULL )
						{
							two_wire_io_control.fds.info_ptr->comm_stats.unicast_response_crc_errs += 1;
						}
					}
				}

			}  // end if( uart_status == TWO_WIRE_RESP_RECEIVED )

		}
		else
		{
			alert_message_to_tpmicro_pile( "Result queue timed out" );
		}

		// 1/23/2013 rmd : Want to get back and turn off the RCVR interrupt. Before we turn the line
		// back on. And certainly before we send our next outgoing message.
		NVIC_DisableIRQ( UART1_IRQn );

		//////////////////////////
		//  IO REQUEST COMPLETED
		//////////////////////////
		
		// 1/24/2013 rmd : Now before proceeding which means before we move towards repowering the
		// bus we must make absolutely sure the decoder is off the bus. Dave drives the bus low for
		// a full 10ms before letting go and floating the bus. So I will wait 12ms before
		// re-powering. And at 12ms leaves a dead bus for at about 3ms. This is the timing for the
		// case of a UNICAST msg. Other message types require different timing.
		vTaskDelay( MS_to_TICKS( 12 ) );
		
	}  // end if( TwoWireUartCtl_s.req_type == UNICAST_TYPE_e )
	else
	if(  two_wire_io_control.req_type == BROADCAST_TYPE_e )
	{
		// Here we expect no response at all
	}

	// ----------
	
	// 1/21/2013 rmd : Turn power mode back on to bus.
	during_io_switch_and_wait_for_bus_to_enter_power_mode();

	// ----------
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

