/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"rit.h"

#include	"irrigation_manager.h"

#include	"gpiopinchg.h"

#include	"eeprom_map.h"

#include	"tpmicro_comm_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define	RIT_TIME_INTERVAL_MS	(200)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void RIT_IRQHandler()
{
	// ----------

	// 1/14/2013 rmd : The RIT interrupt occurs once each 200ms.

	// 1/10/2013 rmd : NOTE - THIS IS ISR CODE!!!! You are in an interrupt service handler. Keep
	// it as fast as possible. Use the appropriate OS calls ie ending with 'FromISR'.
	//
	// As coded this isr executes in a minimum of 2 usec to a maximum of 34 usec. It's like 2,
	// 11, 16, and 25 usec each second depending on the counts. And once per minute it takes 34
	// usec.
	
	// ----------

	static UNS_32	rit_1_sec_count = 0;

	static UNS_32	rit_10_sec_count = 0;

	static UNS_32	rit_1_min_count = 0;

	signed portBASE_TYPE	task_woken;

	COMM_MNGR_QUEUE_STRUCT	im;
		
	IRRIGATION_TASK_QUEUE_STRUCT	itqs;

	// ----------
	
	TPM_LED2_ON;

	// ----------

	task_woken = pdFALSE;

	// ----------

	// 1/10/2013 rmd : Clear the interrupt in the rit peripheral itself.
	((LPC_RIT_TypeDef*)LPC_RIT)->RICTRL |= RIT_CTRL_INTEN;
	
	// ----------
	
	rit_1_sec_count += 1;

	rit_10_sec_count += 1;

	rit_1_min_count += 1;

	// ----------

	// 1/14/2013 rmd : Trigger the 1 Hz processing.
	if( (rit_1_sec_count * RIT_TIME_INTERVAL_MS) >= 1000 )
	{
		rit_1_sec_count = 0;
		
		// ----------

		// 1/15/2013 rmd : Go capture the counts accumulated over the past second. Move copy of
		// counts to the task level accumulators variable. And zero out portions of the interrupt
		// level variable.
		process_counters_from_rit_isr();
	
		// ----------
	
		// 1/14/2015 rmd : Post the event which causes us to do the once per second processing NOW
		// post the
		im.command = COMM_MNGR_CMD_PERFORM_ONE_HERTZ_PROCESSING;
	
		if( xQueueSendToBackFromISR( comm_mngr_task_queue, &im, &task_woken ) != pdTRUE )
		{
			DEBUG_STOP();
			
			flag_error_from_isr( BIT_TPME_queue_full, &task_woken );
		}
	
	}

	// ----------

	if( (rit_10_sec_count * RIT_TIME_INTERVAL_MS) >= HOW_OFTEN_TO_UPDATE_WHATS_INSTALLED_MS )
	{
		rit_10_sec_count = 0;
		
		itqs.command = IRRI_TASK_CMD_look_for_whats_installed;

		if( xQueueSendToBackFromISR( irrigation_task_queue, &itqs, &task_woken ) != pdTRUE )
		{
			DEBUG_STOP();
			
			flag_error_from_isr( BIT_TPME_queue_full, &task_woken );
		}
	}

	// ----------

	// 1/14/2013 rmd : Trigger the once per minute processing.
	if( (rit_1_min_count * RIT_TIME_INTERVAL_MS) >= TP_MICRO_check_eeprom_mirror_ms )
	{
		rit_1_min_count = 0;
		
		// ----------

		EEPROM_MESSAGE_STRUCT	eeqm;
	
		eeqm.cmd = EE_ACTION_CHECK_MIRROR;
	
		eeqm.EE_address = 0;
	
		eeqm.RAM_address = (UNS_8*)NULL;

		eeqm.length = 0;

		if( xQueueSendToBackFromISR( g_xEEPromMsgQueue, &eeqm, &task_woken ) != pdTRUE )
		{
			DEBUG_STOP();
			
			flag_error_from_isr( BIT_TPME_queue_full, &task_woken );
		}
	}
	
	// ----------
	
	TPM_LED2_OFF;

	// ----------	

	portEND_SWITCHING_ISR( task_woken );

	// ----------
}

/* ---------------------------------------------------------- */
void init_RIT_For_Counters()
{   
	// 1/10/2013 rmd : Set up the RIT for 5Hz rate.
	
	// ----------

	// 1/10/2013 rmd : Enable power to peripheral.
	CLKPWR_ConfigPPWR( CLKPWR_PCONP_PCRIT , ENABLE );
	
	// 1/10/2013 rmd : CCLOCK is 72MHz. So dividing by 4 gives us an 18MHz counter clock if CCLK
	// is 72MHz. It gives us a 25MHz clock is CCLK is 100MHz.
	CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_RIT, CLKPWR_PCLKSEL_CCLK_DIV_4 );
	
	// ----------

	((LPC_RIT_TypeDef*)LPC_RIT)->RICTRL = 0;  // disable while we set it up

	// 1/10/2013 rmd : Bits set in the mask have the effect of ignoring them during the
	// compare. Maybe. Not exactly sure ... but setting this to zero causes proper operation.
	((LPC_RIT_TypeDef*)LPC_RIT)->RIMASK = 0;

	#ifdef FULL_SPEED

		// 1/10/2013 rmd : For the case CCLK = 100MHz. See CCLK_DIV notes above.
		((LPC_RIT_TypeDef*)LPC_RIT)->RICOMPVAL = ( ( 25000000 / 1000 ) * RIT_TIME_INTERVAL_MS );
		
	#else
	
		// 1/10/2013 rmd : For the case CCLK = 72MHz. See CCLK_DIV notes above.
		((LPC_RIT_TypeDef*)LPC_RIT)->RICOMPVAL = ( ( 18000000 / 1000 ) * RIT_TIME_INTERVAL_MS );

	#endif
	
	((LPC_RIT_TypeDef*)LPC_RIT)->RICOUNTER = 0x00;
	
	// ----------

	NVIC_SetPriority( RIT_IRQn, RIT_INT_PRIORITY );

	NVIC_EnableIRQ( RIT_IRQn );
	
	// ----------

	((LPC_RIT_TypeDef*)LPC_RIT)->RICTRL = RIT_CTRL_INTEN | RIT_CTRL_ENCLR | RIT_CTRL_ENBR | RIT_CTRL_TEN;
	
	// ----------
}
