/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"adc.h"

#include	"irrigation_manager.h"

#include	"tpmicro_comm_mngr.h"

#include	"two_wire_task.h"

#include	"two_wire_fet_control.h"

#include	"tpmicro_alerts.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

ADC_RESULTS_STRUCT		adc_readings;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		SAMPLES_USED_FOR_A_SHORT	(3)

typedef struct
{

	UNS_32		samples[ SAMPLES_USED_FOR_A_SHORT ];

	UNS_32		current_index;

} ADC_SHORT_DETECT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* File Description / Theory of operation

GENERAL -This file contains the main ADC task and the ADC interrupt callback.  The ADC
Callback is called whenever the done flag is set from our ADC reads. The SYSTICK (250us 
pulse rate) is responsbile for kicking off the ADC reads.  It arms the first channel every 
250us, after that the ADC callback is responsible for arming any additional channels it may 
need to read.  Currently it converts 4 channels in a cycle.  After the 4th conversion is 
complete, a semaphore is given to the ADC task.  The ADC task does a ATOMIC read on a global 
the ADC callback writes to.  It then converts the ADC reads into a raw count value, and 
stuffs it into an array.  After 50 (settable) readings have been stuffed, the ADC task 
packages the data into an internal message and sends it to the INTERNAL TASK for later 
processing to develop a 100ms and 1s average current reading.  The 100ms reading is used to 
determing individual station draws and the 1second is for display purposes for the main 
board.  The ADC task is also responsible for recognizing short condition and shutting of the 
I2C bus if it detects it. 

ADC ACCURACY - During testing it was noted that every so often (undeterministic time period)
the ADC read would fail and produce faulty results, but there was no indication of why this 
was happening, or that it was happening at all.  After switching between multiple methods of 
doing ADC reads (polling, burst mode, interrupt with burst, and finally single shot 
interrupt mode) it was determined that we should just live with the faulty readings and work 
around them.  We observed the the faulty readings never happened back to back, so for short 
detection we cant rely on a single reading(that wouldnt be very prudent anyways), also it 
was decided to throw out the lowest and highest result from every ADC package being sent for 
current calculation. 

SHORT DETECTION - Shorts are detected in this task by looking at the raw ADC readings, and
comparing them to a precalculated "over the limit" constant.  We determined this constant by 
making measurements and seeing where we would always have a short condition flagged. See the 
adc.h file for more info on how we determined it.  For a short event to be flagged, we 
require 3 successive readings above our "over the limit" const.  We then enter a short 
detection mode where our limit is lowered (to guarantee we will catch the offender).  After 
a short event its neccessary to delay a half AC wave cycle so that we dont accidently flag 
the same short as another separate event.  When we catch shorts, we reset the I2C Bus and 
then we send a message to the IRRIGATION TASK so it knows to hunt for the offender. 

FLOATING INPUTS - The adc channels for channels 0,2 and 3 (2-wire, 4-20ma, 5Vana) will float
if the terminal card for that device is disconnected. Always check the terminal block 
present bit before relying on the current readings. 

ADC CHANNELS -	CHANNEL 0 - 2 Wire Current
				CHANNEL 1 - Field Curent (current measured from stations, pumps, master valves, and lights.)
				CHANNEL 2 - POC 4-20ma Input
				CHANNEL 3 - POC 5V analog Input

*/
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/12/2013 rmd : Used to hold off the A-D conversions until 1 second after all tasks are
// up and running.
BOOL_32	commence_A_D_conversions;


// 8/8/2012 raf : incremented if we ever were to get our ADC control structure and adc
// interupts out of sync, cant really happen I dont think, but nice to check anyways.
volatile UNS_32 g_interrupt_error;

// --------------------------

// 9/20/2013 rmd : Used by adc ISR to determine which channel to convert
volatile ADC_CONTROL_s g_adc_control;

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Our ADC interrupt handler, its triggered when an adc conversion is
    finished. The conversion is kicked off by our systick every 250us. We store the results
    in a global array and give a semaphore when we have finished converting our 4th channel.
    Our task reads out of the global array as this interrupt is filling, but its OK because
    we only do atomic reads in the task.

	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITIES

	@EXECUTED

	@RETURN (none)

	@ORIGINAL 5/22/2012 raf

    @REVISIONS Removed LPC Function calls and replaced them with direct calls, cut
    processing time in half 4us -> 2us
*/
void ADC_IRQHandler( void )
{
	signed portBASE_TYPE	xTaskWoken = pdFALSE;		//this must be initialized to false

	// ----------

	UNS_32	temp_reg;

	temp_reg = ((LPC_ADC_TypeDef*)LPC_ADC_BASE)->ADSTAT;

	temp_reg &= 0xFFFF;	// 5/22/2012 raf :  get rid of the global flag.

	// ----------

	if( temp_reg != (1<<g_adc_control.current_channel) )
	{
		g_interrupt_error++;
	}

	if( g_adc_control.current_channel <= 3 )
	{
		// See if write_to has gone out of range.
		if( g_adc_control.write_to <= &(g_adc_control.results[ 3 ]) )
		{
			// 5/22/2012 raf :  this line is reading from the ADC results register and stuffing it into a results array we read from in the ADC task.
			*(g_adc_control.write_to) =  *(g_adc_control.read_from);

			g_adc_control.write_to += 1;

			g_adc_control.read_from += 1;
		}

		// ----------

		// Seems the best practice is to clear the conversion start bits. ADC_START_CONTINUOUS is the way to STOP the ADC when not in BURST mode.
		//ADC_StartCmd( LPC_ADC, ADC_START_CONTINUOUS );
		(((LPC_ADC_TypeDef*)LPC_ADC_BASE)->ADCR) &= ~ADC_CR_START_MASK;

		// And to disable the channel we just completed.
		//ADC_ChannelCmd( LPC_ADC, adc_control.current_channel, DISABLE );
		((LPC_ADC_TypeDef*)LPC_ADC_BASE)->ADCR &= ~ADC_CR_CH_SEL(g_adc_control.current_channel);

		// ----------

		// 6/18/2012 rmd : And bump to the next channel.
		g_adc_control.current_channel += 1;

		if( g_adc_control.current_channel < 4 )
		{
			// Arm the first channel.
			//ADC_ChannelCmd( LPC_ADC, adc_control.current_channel, ENABLE );
			((LPC_ADC_TypeDef*)LPC_ADC_BASE)->ADCR |= ADC_CR_CH_SEL(g_adc_control.current_channel);

			//ADC_StartCmd( LPC_ADC, ADC_START_NOW );
			((LPC_ADC_TypeDef*)LPC_ADC_BASE)->ADCR |= ADC_CR_START_MODE_SEL((uint32_t)ADC_START_NOW);

		}
		else
		{
			// Send your semaphore!
			xSemaphoreGiveFromISR( g_xAdcSema, &xTaskWoken );
		}

	}

	// ----------

	portEND_SWITCHING_ISR( xTaskWoken );

	// ----------
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION    				This is our ADC task loop.  This guy just waits for the ADC semaphore to be sent from the ADC ISR when channel 3
    								completes its conversion . We want to get in and out as quick as possible, because this task runs at, for now, the
    								highest priority. We also need to be sure that when we read the global that is stuffed with the latest ADC values,
    								its an atomic operation and only happens once.  If we don't do this, we could catch the data mid change.  Every
    								time NUMBER_OF_SAMPLES_TO_SEND_DOWN is equal to the number of recent samples, we send a record of that many
    								samples to the internal comms task to process into a current and then we reset our recent sample count.  This
    								calculation is used to report to main and to determine station deltas.
	@CALLER_MUTEX_REQUIREMENTS

	@MEMORY_RESPONSIBILITIES

	@EXECUTED

	@RETURN (none)

	@REVISIONS
*/

void adc_task( void *pvParameters )
{
	UNS_32	*task_watchdog_time_stamp;
	
	// 2 wire current local copy of data register
	UNS_32	adc_chan0;
	
	// Field Current local copy of data register
	UNS_32	adc_chan1;
	
	// POC 4-20ma Input local copy of data register
	UNS_32	adc_chan2;
	
	// POC 5V Analog Input local copy of data register
	UNS_32	adc_chan3;
	
	ADC_SHORT_DETECT_STRUCT		asd;
	
	ADC_RAW_SAMPLES_STRUCT		raw;
	
	UNS_32						raw_index;
	
	// ----------

	task_watchdog_time_stamp = ((TASK_TABLE_STRUCT*)pvParameters)->last_execution_time_stamp;
	
	// ----------
	
	g_interrupt_error = 0;

	// ----------
	
	memset( &asd, 0x00, sizeof(ADC_SHORT_DETECT_STRUCT) );
	
	memset( &raw, 0x00, sizeof(ADC_RAW_SAMPLES_STRUCT) );
	
	raw_index = 0;
	
	// ----------

	// Power on ADC and set to convert at a 40khz rate.
	ADC_Init( LPC_ADC, 160000 );

	// Disable global adc interrupts.
	ADC_IntConfig( LPC_ADC, ADC_ADGINTEN, 0 );

	ADC_IntConfig( LPC_ADC, ADC_ADINTEN0, 1 );
	ADC_IntConfig( LPC_ADC, ADC_ADINTEN1, 1 );
	ADC_IntConfig( LPC_ADC, ADC_ADINTEN2, 1 );
	ADC_IntConfig( LPC_ADC, ADC_ADINTEN3, 1 );

	NVIC_SetPriority( ADC_IRQn, ADC_INT_PRIORITY );

	NVIC_EnableIRQ( ADC_IRQn );

	// ----------

	while( (true) )
	{
		// 7/31/2015 rmd : For the watchdog task time stamp the last time we came back to the top of
		// the task. Meaning the last time we ran. Being this task runs at a furious rate we don't
		// have to worry about the portMAX_DELAY parameter in the semaphore pend that follows.
		*task_watchdog_time_stamp = xTaskGetTickCount();
		
		// ----------

		// 3/6/2013 rmd : This semaphore is GIVEN at a 4 KHz rate! Once every 250usec the convertor
		// starts a sequence of 4 conversions. After the 4th conversion is complete we give the
		// semaphore.
		xSemaphoreTake( g_xAdcSema, portMAX_DELAY );

		// ----------

		// 6/18/2012 : The first thing we do after a semaphore has been given to us is to take a
		// copy of the global results. Note atomic copy operations.
		adc_chan0 = g_adc_control.results[ 0 ];			// 2-wire Current

		adc_chan1 = g_adc_control.results[ 1 ];			// Field Current

		adc_chan2 = g_adc_control.results[ 2 ];			// 4-20 from POC

		adc_chan3 = g_adc_control.results[ 3 ];			// 5V from POC

		// ----------
		
		// 10/5/2012 : Check if our interrupt handler got messed up at all, if it did we are going
		// to disregard the results and wait for the next set.
		if( g_interrupt_error != 0 )
		{
			pre_defined_alert_to_main( TPALERT_functional_adc_error );

			g_interrupt_error = 0;

			DEBUG_STOP();

			continue;
		}

		// ----------

		// 11/7/2013 rmd : Using the timing of the LED's, this function executes in 2 usec!!! And
		// once every 16.6ms (a 60 hz period) this function takes 50 usec when it has to post the
		// readings to the comm_mngr task.
		//TPM_LED2_ON;
		
		// ----------

		// 6/18/2012 rmd : Taking a look at the done flag. As a sanity check to see each channel
		// converted. The done flag is reset in the peripheral register when we read the results in
		// the adc isr. 8/8/2012 raf :  We only need to send the alert once, so its ok to be else-if
		// If the data is bad, dump it and start over.
		if( (adc_chan0 & ADC_DR_DONE_FLAG) != ADC_DR_DONE_FLAG )
		{
			pre_defined_alert_to_main( TPALERT_functional_adc_error );
			continue;
		}
		else
		if( (adc_chan1 & ADC_DR_DONE_FLAG) != ADC_DR_DONE_FLAG )
		{
			pre_defined_alert_to_main( TPALERT_functional_adc_error );
			continue;
		}
		else
		if( (adc_chan2 & ADC_DR_DONE_FLAG) != ADC_DR_DONE_FLAG )
		{
			pre_defined_alert_to_main( TPALERT_functional_adc_error );
			continue;
		}
		else
		if( (adc_chan3 & ADC_DR_DONE_FLAG) != ADC_DR_DONE_FLAG )
		{
			pre_defined_alert_to_main( TPALERT_functional_adc_error );
			continue;
		}

		// ----------

		// 6/18/2012 rmd : Mask out to obtain the actual a-d reading from the register.
		adc_chan0 = ADC_DR_RESULT( adc_chan0 );

		adc_chan1 = ADC_DR_RESULT( adc_chan1 );

		adc_chan2 = ADC_DR_RESULT( adc_chan2 );

		adc_chan3 = ADC_DR_RESULT( adc_chan3 );

		// ----------

		// 3/6/2013 rmd : Increment the sample position. So actually the very first sample goes into
		// slot 1. But that is okay because we have circular buffer for the field current samples
		// used to determine if there is a short.
		asd.current_index += 1;

		if( asd.current_index >= SAMPLES_USED_FOR_A_SHORT )
		{
			asd.current_index = 0;
		}
		
		// 3/6/2013 rmd : And capture the field current reading.
		asd.samples[ asd.current_index ] = adc_chan1;
		
		// ----------

		UNS_32	current_limit;

		// 2/12/2013 rmd : If we are hunting for a short, we lower our bounds a bit in an attempt to
		// take out the anomoly that may come up if a short is initially detected at an amount
		// just over the trip point. I'm reasoning that the next time we measure it may not show
		// enough to trip again. So lower the trip point a bit.
		if( scs.state == SHORT_STATE_hunting )
		{
			current_limit = ADC_CURRENT_LIMIT_THRESHOLD_RAW_COUNT - ADC_CURRENT_LIMIT_REDUCTION_RAW_COUNT;
		}
		else
		{
			current_limit = ADC_CURRENT_LIMIT_THRESHOLD_RAW_COUNT;
		}

		// ----------

		//  		!!!!!!!!!!!!!SUPER IMPORTANT!!!!!!!!!!!
		//			Do NOT Put Breakpoints below or in any code that handles short protection, when testing
		//  		shorts...otherwise a short could stay active as long as it takes for the watchdog circuit to time out (400ms, i think)...

		// ----------
		
		// 3/6/2013 rmd : Criteria for a short is to exceed the three most recent current readings.
		// Using 3 readings was arrived at after likely much study. Unfortunetly novice boy did not
		// document any details of such.
		if( (asd.samples[ 0 ] >= current_limit) && (asd.samples[ 1 ] >= current_limit) && (asd.samples[ 2 ] >= current_limit) )
		{
			// ----------
		
			// 3/7/2013 rmd : Turn OFF all triac outputs using brute force.
			TPM_TRIAC_MR_ACTIVE;
			
			// ----------
		
			// 9/19/2013 rmd : Send message tot he FRONT of the queue. Has the potential effect of
			// causing any further station turn ON commands (via incoming message from main board) to be
			// dropped because we are going to get the hunting process under way immediately! Processing
			// a turn ON command after this short but before the hunt may produce some unaccounted for
			// results. Posting to the front of the queue supresses such.

			IRRIGATION_TASK_QUEUE_STRUCT	itqs;
		
			itqs.command = IRRI_TASK_CMD_short_detected;
		
			if( xQueueSendToFront( irrigation_task_queue, &itqs, portNO_DELAY ) != pdTRUE )
			{
				flag_error( BIT_TPME_queue_full );
			}
			
			// ----------
		
			// 3/7/2013 rmd : During this delay the triac(s) eventually turn off (turns off at the next
			// 0 crossing). And during this delay the ADC continues to run, sampling away and giving the
			// semaphore. So to prevent sending a second SHORT DETECTED queue message to the irrigation
			// task we delay here long enough so that when we finally go free the sample we first pick
			// up for the field power is 0 (or very close to 0). I chose 18ms to fully encompass 2 zero
			// crossings making sure the triac is OFF. Note: likely during this delay the irrigation
			// task will pick up the SHORT_DETECTED message and process it. That includes its very own
			// 350ms delay to let the I2C bus reset clear.
			vTaskDelay( MS_to_TICKS( 18 ) );

			// ----------
		}

		// ----------

		// 5/8/2013 rmd : Capture the samples for each channel into their respective buffer. We
		// collect every sample for the 5V and 4-20 but there is no buffer so we only deliver
		// the latest sample.
		raw.two_wire_current_samples[ raw_index ] = adc_chan0;

		raw.field_24VAC_current_sense[ raw_index ] = adc_chan1;
		
		// 6/22/2017 dks : The current values of both POC ADC channels are retained
		raw.poc_420_analog_sample = adc_chan2;

		raw.poc_5V_analog_sample = adc_chan3;

		// ----------

		raw_index += 1;
		
		if( raw_index >= AMPERAGE_SAMPLES_TO_COLLECT )
		{
			// 5/8/2013 rmd : The buffers are full!
			COMM_MNGR_QUEUE_STRUCT	cmqs;
			
			cmqs.command = COMM_MNGR_CMD_develop_adc_reading_averages;

			// 5/8/2013 rmd : Get a memory block to deleiver the samples.
			cmqs.dh.dptr = GetMemBlk( sizeof(ADC_RAW_SAMPLES_STRUCT) );

			// 3/13/2013 rmd : If the memory pool is empty, and returns a NULL, a flag has already been
			// set. Not necessary to alert the outside world from here.
			if( cmqs.dh.dptr != NULL )
			{
				memcpy( cmqs.dh.dptr, &raw, sizeof(ADC_RAW_SAMPLES_STRUCT) );

				if( xQueueSendToBack( comm_mngr_task_queue, &cmqs, portNO_DELAY ) != pdTRUE )
				{
					FreeMemBlk( cmqs.dh.dptr );

					DEBUG_STOP();
				}
			}
			
			// ----------
			
			// 5/8/2013 rmd : Start again of course.
			raw_index = 0;

		}  // of if enough sample collected to support post processing
		
		// ----------

		//TPM_LED2_OFF;
		
		// ----------

	}  // of task forever loop
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 9/19/2013 rmd : NOTE - this function that RECEIVES the 4khz samples collected over a 67
// sample peiord. That is approximately one 60 hz cycle. So therefore this function runs at
// a 60 hertz (approximately) rate. It is NOT called however from the adc task. We leave
// that task alone as a high priority task to run once every 250 usec. A very high
// rate. This function is instead invoked from the irrigation task.

// 5/13/2013 rmd : This value comes from the magnitude of the AC component of the signal.
// This would be the sample minus the current average. In any group of 50 samples we are
// guaranteed to get at least one peak. This is because the 50 sample cover 12.5 ms and that
// will include at least one 60 hertz peak. If we find the largest excursion in the sample
// group we can use that to determine if we are into a short condition. If it persists for
// one second flag the short. We have seen excursions in the 350 to 400 range. So take 75%
// of that as the short limit.

// 5/13/2013 rmd : Update. As the FET's heat up, their resistance increases and they pass
// less current, yet continue heating. A thermal run-away takes palce. So we should have
// different current thresholds by temperature. Let's just have two. A regular temperature
// and a high temperature. Remember the temperature reading drops as the temperature rises.

/*

As far as temps go:

Room Temp is about 1900

Gently Warmed is about 1720

Nice and warm is about 1100

Pretty Hot is about 700

Hot is about 530

Damn Hot is about 400

*/ 

// ----------

/*

Some notes about amp readings:

Regarding a 12 ohm load (which should trip a short)

 - at room temperature it measures a draw of about 230. Have seen as high as 250.

 - as the fets heat this number drops to about 180 as the temperature crosses through the
   HI_LO temp boundary (900).

 - at a temp of 600 the current is 130

 - at a temp of 500 it is 110


As a matter of fact, the amps versus temperature for a 12 ohm load is very close to the amps
measured with a dead short. A dead short produces only a bit higher readings. For example at 
a temperature of 600 the measured current is 160. At room temp produces amp reading of 270.


Regarding regular levels of current:

A 91 ohm resistor which produces about 380ma of current gives us a current reading of 55. I 
would consider this the high end of normal levels of current. Note that such a level of 
current will not overheat the fets nor trip the current magnitude (short) protection.

A 47 ohm resistor also does not overheat the fets nor trip the short protection. This causes
delivery of about 750ma down the cable. Makes a reading of about 90 at room temperature.

 
IN REFLECTION: I wish I had developed and tested with something more like a 47 ohm resistor 
instead of the 12 ohm that I used. I guess it doesn't hurt to allow that much current down 
the cable but now feels excessive. Perhaps the 3 current limits at the 3 temp ranges I have 
should be 160, 100, 75 (instead of the 220, 160, 110). If such a change is made the 120,000 
uF load startup would need to be re-tested.
 
*/ 

// ----------

// 9/12/2013 rmd : AS the temperature rises - temp reading falls - and so does the short
// limit amps. This is from the phenomenon where FET channel resistance increases with
// temperature.

// 9/11/2013 rmd : HOW TO SET THE SHORT_DRATION - Assuming this function runs at 60 hertz
// (it does run very close to that) we need 120 passes for seeing 2 consecutive seconds of a
// short. Remember when the cable first powers up it looks like a dead short until all the
// decoder caps have charged. We'll have to see if a cable with 70 decoders on it trips our
// short detection. UPDATE - with our 120,000 uF dummy test load indeed it trips the short
// if we don't allow time for the caps to charge. (All this capacitance is to simulate 70
// decoders each with 1640uF) At 2 seconds it seems to work fairly well but it is right on
// the edge of false tripping. So I'm going to make the short have to persist for 2.5
// seconds to trip.


// 9/12/2013 rmd : This is a succesful set of numbers. But I felt it allowed way more amps
// than needed. And I am afraid of a decoder failure that consumes a fair amount of current
// but not enough to trip the short protection that eventually overheats the transformer. So
// this is a delicate balance.

/*
#define		TEMP_BOUNDARY_1							(900) #define 
   GREATER_THAN_TEMP_BOUNDARY_1_AMPS		(220)

#define		TEMP_BOUNDARY_2							(600)
#define		BETWEEN_BOUNDARY_1_AND_2_AMPS			(160)

#define		LESS_THAN_BOUNDARY_2_AMPS				(110)

#define		SHORT_DURATION_THRESHOLD				(2.5 * 60)
*/


// 9/12/2013 rmd : As an alternate set of numbers I've reduced the allowed current but had
// to lengthen the charge time for the 120,000 uF test. I still don't know what are so
// called 'NORMAL' cable current limits when there are say 80 decoders present with 6 valves
// ON.

// 9/12/2013 rmd : !!!!! Another thought is we may want TWO sets of numbers. One for the
// first 6 seconds following a power up. And then a subsequent set for during normal
// operation!!!! This WOULD deal with the potential to overheat the transformer with a load
// that isn't enough to trip the short protection that we had to make high limits to deal
// with the start up. Essentially what you see below is a startup set of numbers. We should
// develop a second set that would be used after the 5 second mark. TODO do when we have
// real 100 decoders on a cable numbers.


/*
#define		TEMP_BOUNDARY_1							(900)
#define		GREATER_THAN_TEMP_BOUNDARY_1_AMPS		(145)

#define		TEMP_BOUNDARY_2							(600)
#define		BETWEEN_BOUNDARY_1_AND_2_AMPS			(85)

#define		LESS_THAN_BOUNDARY_2_AMPS				(65)

#define		SHORT_DURATION_THRESHOLD				(3 * 60)
*/


// 10/17/2013 rmd : UPDATE with 80 decoders on the cable. The above numbers (145, 85, 65)
// trip the short protection when the cable is first started. Saw repeated examples of the
// trip. The reported magnitude was between 150 and 190. Always at the low temp since we
// were inside in the lab.
//
// I've decided to formalize a bit more. There is only one set of TEMP boundaries. And one
// short duration. But 2 sets of trip values. The first set is used during the first 6
// seconds the cable is energized. The second set following the startup period.

#define		TEMP_BOUNDARY_1							(850)

#define		TEMP_BOUNDARY_2							(600)

// 11/4/2013 rmd : This was pushed out to 3 seconds as a way to allow an effective short to
// exist during the cable charging time (remember the dead capacitance of 70 decoders looks
// like a short for a second or two - until partially charged up). Well now that we have a
// second set of current limit numbers used during the cable charging phase, we can also
// have two of these durations. During cable charging we allow 2.5 seconds. And during
// regular operation set the criteria to 1.5 seconds. These numbers represent 60 hertz
// periods.

// 11/5/2013 rmd : UPDATE UPDATE - I warn you things are very complicated regarding cable
// short protection during the discovery process. First of all the two_wire task is a higher
// priority than the comm_mngr task. The comm_mngr task is the task POST processing the ADC
// readings to determine if a cable short condition exists. During the discovery process
// very little time is given to the lower level tasks. This could turn out to be a problem
// during periods of heavy serial traffic - but that has yet to be shown.
//
// ANYWAY after much playing here's where we're at. During cable charging we must allow the
// 'short' to exist for 2 and a half seconds without tripping. This criteria only is in
// effect during the first 6 seconds after the cable is first powered up.
//
// FOLLOWING the charging phase we tighten up the criteria dramatically. Because of the
// UPDATE paragraph explanation above. And furthermore in actuality we have no reason not to
// be so tight. We should not see sustained currents above the threshold for any duration.
// We have set it for 5 60 hertz cycles. And this manages to trip even during the discovery
// process which I think is our worst offender at starving the comm_mngr post_adc conversion
// processing.
#define		SHORT_DURATION_DURING_CHARGING				(150)
#define		SHORT_DURATION_DURING_REGULAR_OPERATION		(6)
#define		CABLE_MODE_TRANSITION_GRACE_PERIOD			(1)

// ----------

#define		CHARGING_LOW_TEMP_CURRENT_LIMIT			(260)
#define		CHARGING_MID_TEMP_CURRENT_LIMIT			(190)
#define		CHARGING_HIGH_TEMP_CURRENT_LIMIT		(130)

#define		STABLE_LOW_TEMP_CURRENT_LIMIT			(165)
#define		STABLE_MID_TEMP_CURRENT_LIMIT			(120)
#define		STABLE_HIGH_TEMP_CURRENT_LIMIT			(90)

// ----------

// 9/5/2013 rmd : This value started out at 0x380. On simply a hot day we would trip the
// overheated condition. Have rethought this value. It should be the drop dead damn we're
// hot emergency shut-down value. The dead short shut down is handled with the excursion
// from the average. This works quite well at lower temperatures. More testing is required
// at higher temperatures. Remember the nature of the FET's is to increase their channel
// resistance with temperature. And therefore heating climbs while current falls. Hence our
// need for the overheated algorithm combined with the over current algorithm. NOTE: With a
// 12 ohm load (which during development we made it not trip the short) eventually the FET's
// over heat and trip the temperature protection when set to 400.
#define		OVERHEATED_TRIP_POINT							(400)

// 9/11/2013 rmd : Oh I dunno. I just sort of picked this value. It's not cold. But plenty
// cooled down to try again in the face of an overheated condition. UPDATE - I've decided to
// just use the TEMP_BOUNDARY_1 defined above.
#define		OVERHEATED_RECOVERY								(TEMP_BOUNDARY_1)

/* ---------------------------------------------------------- */

// 9/12/2013 rmd : The variables used during development. To display the measured 2W FET
// temperature and current.

INT_32	g_latest_temp, g_lowest_temp;

INT_32	g_highest_temp = 0xFFFF;  // initialized to a very low temperature

INT_32	g_latest_amp, g_lowest_amp, g_highest_amp;

INT_32	g_current_limit, g_temp_range;


/* ---------------------------------------------------------- */


// 9/19/2013 rmd : This is the reading used once for each output turn ON. The average must
// be developed and available within less than 200ms. This fits the 400ms turn ON
// staggering. You see this adc process runs asynchronously to the irrigation process. So we
// have to guarantee we get one whole complete short_duration reading developed while the
// output is ON. And not end up working with a reading (average) that reflects part of the
// output OFF and part ON.

// 9/20/2013 rmd : We want to guarantee a full adc conversion of samples since a turn ON.
// That emans we can only average in a bit less than half of the station to station stagger
// time. That is now at 600ms. If we went with say 250ms worth or readings to develop our
// average that would be 15 60hz cycles worth.
#define		NUMBER_OF_60HZ_PERIODS_IN_SHORT_AVERAGE		(15)

// 11/6/2013 rmd : The short count MUST be reset when transitioning the cable from OFF to
// POWER as part of the INITIAL cable power up activity.

UNS_32		short_count;		// initial count of 0 is just fine

BOOL_32		clear_short_count;  // initial state of (false) is just fine

UNS_32		grace_period;		// initial count of 0 is just fine

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void develop_average_adc_readings_for_each_channel( ADC_RAW_SAMPLES_STRUCT *raw )
{
	// 9/11/2013 rmd : THIS FUNCTION IS EXECUTED WITHIN THE CONTEXT OF THE COMM_MNGR TASK. That
	// task is a lower priority than both the adc and the two_wire tasks. Additionally the
	// function runs at very close to 60 hertz rate (when the other tasks let it). The
	// fundamental reason it doesn't run at 60 hertz is because the adc sample rate is tied to
	// systick at 4kHz. And we attempt to collect one full 60 hertz period of samples. Well that
	// is not an integer number of sample. We settled at 67 sample. Which takes 16.75ms instead
	// of 16.66ms. I've decided this will produce very little error.

	static UNS_64	squared_sum_for_short_average = 0;

	static UNS_32	sample_count_for_short_avg = 0;


	double			Arms_calculation, multiplier;
	

	// 5/8/2013 rmd : A running exponential moving average of the two_wire adc samples. This is
	// the dc value of the waveform and represents the two_wire terminal FETs temperature. When
	// the code boots and starts, if the moving average is initially 0 (meaning VERY HOT), the
	// first pass we would trip the over heated condition. So start the moving average at a
	// known benign temperature.
	static float	two_wire_present_moving_avg = TEMP_BOUNDARY_1;
	
	static UNS_32	profile_amps_count[ 16 ] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	static UNS_32	profile_amps_highest[ 16 ] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

//	static UNS_32	short_count = 0;

//	static UNS_32	grace_period = 0;


	UNS_32 i;

	UNS_64	latest_field_current_squared_sum;

	UNS_32	sss, latest_total;

	float	latest_avg;
	
	UNS_32	lavg, short_duration_needed;

	UNS_32	lcurrent, biggest_excursion;
	
	char	str_48[ 48 ];
	
	UNS_32	low_temp_current_limit, mid_temp_current_limit, high_temp_current_limit;

	// Variables used for POC measurement sample processing (all initialized to zero by startup)
	
	// For accumulating 4-20 mA adc samples
	static UNS_32	poc_420_accum;
	
	// For accumulating 5V adc samples
	static UNS_32	poc_5v_accum;
	
	// For counting samples
	static UNS_32	current_samples_count_for_poc_channels;
	
	// ----------
	
	if( current_samples_count_for_poc_channels < POC_ADC_CHANNELS_SAMPLES_PER_READING )
	{
		// Add the most recent POC adc raw sample values to the accumulations
		poc_5v_accum += raw->poc_5V_analog_sample;
		poc_420_accum += raw->poc_420_analog_sample;
		
		// Increment the total # of POC samples now summed in poc_5v_accum and poc_420_accum variables
		current_samples_count_for_poc_channels++;
	}
	else
	{
		xSemaphoreTake( g_xHardwareProfileMutex, portMAX_DELAY );
		
		// 3/16/2018 rmd : Check if POC and its terminal board are installed. (Note - this line had
		// a bug in it - we were checking if the card was present twice - instead of if the card and
		// tb were present - found and corrected by dave squires on 10/16/2017 - years after initial
		// release)
		if( whats_installed.poc.card_present && whats_installed.poc.tb_present )
		{
			UNS_32 poc_420_avg;
			UNS_32 poc_tmp_mV;   // temp value for POC millivolts
			UNS_32 poc_5V_avg;
			UNS_32 poc_rem;      // Remainder from division
			
			// A POC card and its terminal board are installed
			
			//////////////////////
			// HANDLE 0-5V VALUE
			//////////////////////
			poc_5V_avg = ( poc_5v_accum / POC_ADC_CHANNELS_SAMPLES_PER_READING );
			
			// Get remainder for division
			poc_rem = poc_5v_accum % POC_ADC_CHANNELS_SAMPLES_PER_READING;
			
			// Round up quotient if needed
			if( poc_rem >= (POC_ADC_CHANNELS_SAMPLES_PER_READING / 2))
			{
				// Round up average ADC counts
				poc_5V_avg += 1;
			}
			
			// Scale ADC counts into millivolts
			poc_tmp_mV = (poc_5V_avg * POC_5V_MULT ) / POC_5V_DIV;
			
			// Get remainder for division above
			poc_rem = (poc_5V_avg * POC_5V_MULT ) % POC_5V_DIV;
			
			// Round up quotient if needed
			if( poc_rem >= ( POC_5V_DIV / 2))
			{
				// Round up scaling result
				poc_tmp_mV += 1;
			}
			adc_readings.POC_5V_analog_mvolts = poc_tmp_mV;
			
			////////////////////////
			// HANDLE 4-20 mA VALUE
			//////////////////////// 
			poc_420_avg = ( poc_420_accum / POC_ADC_CHANNELS_SAMPLES_PER_READING ); 
			
			// Get remainder for division
			poc_rem = poc_420_accum % POC_ADC_CHANNELS_SAMPLES_PER_READING;
			
			// Round up quotient if needed
			if( poc_rem >= (POC_ADC_CHANNELS_SAMPLES_PER_READING / 2))
			{
				// Round up average ADC counts
				poc_420_avg += 1;
			}
			
			// Scale ADC counts into microamps
			adc_readings.POC_420_analog_uamps = (poc_420_avg * POC_420_MULT ) / POC_420_DIV;
		}
		else
		{
			// A POC card and its terminal board are not installed
			adc_readings.POC_5V_analog_mvolts = 0;
			adc_readings.POC_420_analog_uamps = 0;
		}

		xSemaphoreGive( g_xHardwareProfileMutex );

		current_samples_count_for_poc_channels = 0;

		poc_5v_accum = 0;

		poc_420_accum = 0;
	}

	// ----------

	// 5/8/2013 rmd : So this buffer arrived with 13 samples of the two wire cable current.
	// Samples are taken each 1ms. So the buffer represents a 13ms time slice. Which will
	// include at least one 60hz peak.
	if( whats_installed.two_wire_terminal_present )
	{
		
		latest_total = 0;
		
		// 9/5/2013 rmd : Add up the 67 readings which represent one 60 hertz cycle.
		for( sss=0; sss<AMPERAGE_SAMPLES_TO_COLLECT; sss++ )
		{
			latest_total += raw->two_wire_current_samples[ sss ];
		}

		latest_avg = ( (float)latest_total / (float)AMPERAGE_SAMPLES_TO_COLLECT );

		// ----------
		
		biggest_excursion = 0;
		
		// 9/12/2013 rmd : These averages reflect the temperature.
		lavg = latest_avg;

		for( sss=0; sss<AMPERAGE_SAMPLES_TO_COLLECT; sss++ )
		{
			lcurrent =  abs( raw->two_wire_current_samples[ sss ] - lavg );
			
			// ----------

			// 5/13/2013 rmd : Collect the peak for short detection by magnitude. The biggest excursion
			// is the max amperage.
			if( lcurrent > biggest_excursion )
			{
				biggest_excursion = lcurrent;
			}
			
			// ----------
			
			// 5/17/2013 rmd : This whole profile stuff is debug code to see what the samples look like.
			// So far what I have seen is they are all <512. Even with a short I didn't see any lcurrent
			// values above 0x200.

			UNS_32	array_index;
			
			array_index = ( (lcurrent >> 6) & 0x0F );

			if( (array_index < (sizeof(profile_amps_count)/sizeof(UNS_32))) && (array_index < (sizeof(profile_amps_highest)/sizeof(UNS_32))) )
			{
				profile_amps_count[ array_index ] += 1;
	
				if( profile_amps_highest[ array_index ] < lcurrent )
				{
					profile_amps_highest[ array_index ] = lcurrent;
				}
			}
			else
			{
				alert_message_to_tpmicro_pile( "Unexpected 2W current magnitude" );
			}

		}
		
		// ----------

		g_latest_temp = lavg;

		g_latest_amp = biggest_excursion;
		
		// 9/11/2013 rmd : Remember as the temp rises the reading falls.
		if( g_latest_temp < g_highest_temp )
		{
			g_highest_temp = g_latest_temp;	
		}

		// 9/11/2013 rmd : Remember as the temp falls the reading rises.
		if( g_latest_temp > g_lowest_temp )
		{
			g_lowest_temp = g_latest_temp;	
		}

		if( g_latest_amp > g_highest_amp )
		{
			g_highest_amp = g_latest_amp;	
		}

		if( g_latest_amp < g_lowest_amp )
		{
			g_lowest_amp = g_latest_amp;	
		}
		
		// ----------

		if( two_wire_bus_info.cable_is_done_charging )
		{
			low_temp_current_limit = STABLE_LOW_TEMP_CURRENT_LIMIT;

			mid_temp_current_limit = STABLE_MID_TEMP_CURRENT_LIMIT;

			high_temp_current_limit = STABLE_HIGH_TEMP_CURRENT_LIMIT;
			
			short_duration_needed = SHORT_DURATION_DURING_REGULAR_OPERATION;
		}
		else
		{
			// 10/25/2013 rmd : NOTE! For now we have NOT seen the need to use the STARTUP set of
			// numbers. The concept was devised to allow a high current limit during the cable charge
			// period immediately following an initial powering of the cable. It seems the cable current
			// drops off enough within the SHORT_DURATION 3 second requirement to avoid tripping an
			// excessive current. This has been well tested with 70 decoders on 7000 feet of cable.
			
			// 11/4/2013 rmd : Ahhh. The note above was written with 1470uF on each leg of the 2W
			// TERMINAL card. This was an increase from Bill's original design of 2 x 470uF on each leg.
			// I (rmd) did this. As it turns out I think the extra capacitance endangers the FET's as
			// I've seen them destroyed while shorting the cable (during short testing). Though that
			// doesn't quite make sense as the 3.9 ohm resistors should save the FETs. Anyway I lowered
			// it back to Bill's design number. In doing so the cable charging of course takes longer
			// and therefore a higher measured current exists for the 3 second short duration. So during
			// the 6 second cable charge period switch in a higher set of numbers.
			low_temp_current_limit = CHARGING_LOW_TEMP_CURRENT_LIMIT;

			mid_temp_current_limit = CHARGING_MID_TEMP_CURRENT_LIMIT;

			high_temp_current_limit = CHARGING_HIGH_TEMP_CURRENT_LIMIT;
			
			short_duration_needed = SHORT_DURATION_DURING_CHARGING;
		}

		UNS_32	current_limit;
		
		// 5/13/2013 rmd : We use different temp ranges as the FET channel resistance increases
		// greatly with temperature. REMEMBER AS THE TEMP RISES THE READING DROP!
		if( lavg >= TEMP_BOUNDARY_1 )
		{
			current_limit = low_temp_current_limit;
			
			g_temp_range = 1;
		}
		else
		if( (lavg < TEMP_BOUNDARY_1) && (lavg >= TEMP_BOUNDARY_2) )
		{
			current_limit = mid_temp_current_limit;

			g_temp_range = 2;
		}
		else
		{
			current_limit = high_temp_current_limit;

			g_temp_range = 3;
		}
		
		g_current_limit = current_limit;

		// ----------
			
		// 11/5/2013 rmd : A note about the grace period and the short_count. During decoder command
		// exchanges the bus is turned OFF to allow the decoders to respond. During these times, the
		// bus is off that is, do not reset the short count. Otherwise during intense decoder I/O
		// (periodic stat), with period of the bus being OFF every say 400 ms, we will never count
		// up and detect the short. So a short could exist for as long as the rapid fire I/O
		// continues. I believe this scenario damaged the smaller FET on my two-wire terminal.
		//
		// Furthermore, as we are in the act of transitioning to the DATA or POWER mode we want to
		// allow a grace period to ensure we really are fully into the mode and not still in one of
		// the TRANSITION phases. To make sure the power in back ON. Without this I saw the short
		// not being picked up. This has become more complicated than I hoped but I think this
		// covers it.
		if( (__tw_bus_mode == TW_BUS_MODE_POWER) || (__tw_bus_mode == TW_BUS_MODE_DATA) )
		{
			// ----------
			
			// 11/6/2013 rmd : When we transition to cable from a true OFF state to POWER we must clear
			// the short count. BECAUSE the way the logic works here once we experience a short the
			// short count is latched until we are back in PWOER or DATA mode. This is intentional to
			// support the decoder I/O sequence which indeed puts the cable through OFF states during
			// which we do not want to reset the count to 0.
			if( clear_short_count )
			{
				short_count = 0;
				
				clear_short_count = (false);
			}

			// ----------

			if( grace_period > 0 )
			{
				grace_period -= 1;
			}
			else
			if( biggest_excursion > current_limit )
			{
				// 5/13/2013 rmd : Do we have a short?
	
				// ----------
	
				if( short_count < short_duration_needed )
				{
					short_count += 1;
				}
				
				if( short_count >= short_duration_needed )
				{
					// ----------
			
					// 11/4/2013 rmd : Take the step to turn OFF the cable now. Do not wait for the two-wire
					// task to get around to it. Some activities in that task take a long time and a look at the
					// cable health may not happen for a long time. For example during discovery.

					during_io_turn_off_the_bus_NOW_and_setup_to_transition_to_off_mode();
					
					// ----------
	
					if( !adc_readings.two_wire_cable_excessive_current )
					{
						// 9/11/2013 rmd : For evaluation of this detection algorithm I want to know was this a high
						// temp trip or a low temp trip.
						if( lavg >= TEMP_BOUNDARY_1 )
						{
							snprintf( str_48, sizeof(str_48), "2Wire Cable: High Current (%u) (low-temp)", biggest_excursion );
				
							alert_message_to_tpmicro_pile( str_48 );
						}
						else
						if( lavg >= TEMP_BOUNDARY_2 )
						{
							snprintf( str_48, sizeof(str_48), "2Wire Cable: High Current (%u) (mid-temp)", biggest_excursion );
				
							alert_message_to_tpmicro_pile( str_48 );
						}
						else
						{
							snprintf( str_48, sizeof(str_48), "2Wire Cable: High Current (%u) (hi-temp)", biggest_excursion );
				
							alert_message_to_tpmicro_pile( str_48 );
						}
						
						// ----------
	
						// 10/29/2013 rmd : Latch the cable short state. It can only be cleared by either a tp micro
						// restart, a main board restart, or by working through the intended communication process
						// where the message travels to the master and back to us at the tp micro. By the time that
						// happens the foal and irri machines have removed all the stations associated with this
						// cable.
						adc_readings.two_wire_cable_excessive_current = (true);
						
						two_wire_bus_info.cable_short_xmission_state = TWO_WIRE_CABLE_STATUS_XMISSION_STATE_signal_main;
						
						// ----------
					}
				}
			}
			else
			{
				short_count = 0;
			}
		}
		else
		{
			grace_period = CABLE_MODE_TRANSITION_GRACE_PERIOD;
		}
		
		// ----------

		// 10/25/2013 rmd : Calculate the cable current percentage of maximum.
		two_wire_bus_info.cable_percent_of_maximum_current = ( (biggest_excursion * 100) / current_limit );
		
		// ----------
		
		// 5/9/2013 rmd : This calculation takes about 7usec. CPU clock at 72Mhz. Timed on scope.
		two_wire_present_moving_avg = ( (latest_avg * 0.1) + (two_wire_present_moving_avg * 0.90) );
		
		// 5/9/2013 rmd : Always update to the latest average value.
		adc_readings.two_wire_present_avg_temperature = two_wire_present_moving_avg;

		// ----------
		
		// 5/10/2013 rmd : The other condition is just plain overheated. If the value is less than
		// 0x380 it is too hot.
		if( adc_readings.two_wire_present_avg_temperature < OVERHEATED_TRIP_POINT )
		{
			// ----------
	
			// 11/4/2013 rmd : Take the step to turn OFF the cable now. Do not wait for the two-wire
			// task to get around to it. Some activities in that task take a long time and a look at the
			// cable health may not happen for a long time. For example during discovery.
			during_io_turn_off_the_bus_NOW_and_setup_to_transition_to_off_mode();
			
			// ----------

			if( !adc_readings.two_wire_terminal_over_heated )
			{
				adc_readings.two_wire_terminal_over_heated = (true);

				// 10/29/2013 rmd : We need to send to main for two reasons. First to remove all the
				// stations registered to this cable from the foal and irri irrigation lists. And second if
				// approriate set flags to indicate why they aren't irrigating.
				two_wire_bus_info.cable_over_heated_xmission_state = TWO_WIRE_CABLE_STATUS_XMISSION_STATE_signal_main;
					
				alert_message_to_tpmicro_pile( "Two Wire Terminal: over heated!" );
			}
		}
		else
		if( adc_readings.two_wire_present_avg_temperature > OVERHEATED_RECOVERY )
		{
			// 5/13/2013 rmd : So it has cooled off quite a bit. Clear the condition.
			if( adc_readings.two_wire_terminal_over_heated )
			{
				// 10/29/2013 rmd : Don't allow the over heated state to CHANGE until the previously
				// determined overheated state has been properly disseminated throughout the network.
				// There may or may not be an issue allowing this to occur but it would be an out of
				// sequence event and that may cause a 'funny'. First of all I don't think the temperature
				// can physically cool down to the recovery level before the communications does it thing.
				// So this test should NEVER actually fail.
				if( two_wire_bus_info.cable_over_heated_xmission_state == TWO_WIRE_CABLE_STATUS_XMISSION_STATE_normal )
				{
					adc_readings.two_wire_terminal_over_heated = (false);
	
					// 11/13/2013 ajv : Set the flag to notify the mainboard of
					// the cool off so the IRRI-side flags can be cleared.
					two_wire_bus_info.cable_cooled_off_xmission_state = TWO_WIRE_CABLE_STATUS_XMISSION_STATE_signal_main;
	
					alert_message_to_tpmicro_pile( "Two Wire Terminal: has cooled down" );
				}	
				else
				{
					alert_message_to_tpmicro_pile( "Wow...cooled down very quickly?" );
				}
			}
		}	
		
	}
	else
	{
		// ----------

		// 10/18/2013 rmd : Remember the lower the number the HIGHER the temperature. If we set the
		// temperature moving avg to 0 that is consider VERY HOT. And when the terminal is first
		// installed will report an over-heated condition.
		two_wire_present_moving_avg = (2*OVERHEATED_RECOVERY);

		adc_readings.two_wire_present_avg_temperature = two_wire_present_moving_avg;

		// ----------

		// 10/18/2013 rmd : Card is not installed so clean house.
		short_count = 0;

		adc_readings.two_wire_terminal_over_heated = (false);

		adc_readings.two_wire_cable_excessive_current = (false);

		// ----------

		two_wire_bus_info.cable_percent_of_maximum_current = 0;

		// ----------
	}

	// ----------
	// ----------
	// ----------

	latest_field_current_squared_sum = 0;

	for( i=0; i<AMPERAGE_SAMPLES_TO_COLLECT; i++ )
	{
		latest_field_current_squared_sum += (raw->field_24VAC_current_sense[ i ] * raw->field_24VAC_current_sense[ i ] );
	}

	// ----------
	
	// 9/20/2013 rmd : Add sum and sample count to our running totals.
	
	squared_sum_for_short_average += latest_field_current_squared_sum;
	
	sample_count_for_short_avg += AMPERAGE_SAMPLES_TO_COLLECT;

	// ----------
	
	// 9/11/2013 rmd : Our collection buffer is suppose to be one 60 hertz cycle. So 6 of them
	// would be 100ms. This is approximately 100ms. But There is nothing really special about
	// the 100ms. It is somewhat related to getting the latest current number for the most
	// recently turned on. But not important to be a reading averaged over exactly 100ms. What's
	// important is that it is a reading developed over a recent period of time, less than 400ms
	// as that is the station turn ON stagger time, and over a period of time that is
	// approximately an integer number of 60 hertz cycles. Which our sample bin size (67) is
	// already geared to be.
	if( sample_count_for_short_avg >= (AMPERAGE_SAMPLES_TO_COLLECT * NUMBER_OF_60HZ_PERIODS_IN_SHORT_AVERAGE) )
	{
		//float	pre_convert __attribute((unused));
		//float	post_convert __attribute((unused));

		Arms_calculation = sqrt( (double)squared_sum_for_short_average / (double)sample_count_for_short_avg );


		//pre_convert = Arms_calculation;




		if( Arms_calculation < 50 )
		{
			multiplier = (1.1323);
		}
		else
		if( Arms_calculation < 100 )
		{
			multiplier = (1.0317);
		}
		else
		if( Arms_calculation < 200 )
		{
			multiplier = (0.97674);
		}
		else
		if( Arms_calculation < 300 )
		{
			multiplier = (0.95769);
		}
		else
		if( Arms_calculation < 400 )
		{
			multiplier = (0.94797);
		}
		else
		if( Arms_calculation < 500 )
		{
			multiplier = (0.95000);
		}
		else
		if( Arms_calculation < 600 )
		{
			multiplier = (0.9396);
		}
		else
		if( Arms_calculation < 700 )
		{
			multiplier = (0.940003);
		}
		else
		if( Arms_calculation < 800 )
		{
			multiplier = (0.93867);
		}
		else
		if( Arms_calculation < 900 )
		{
			multiplier = (0.91075);
		}
		else
		if( Arms_calculation < 1000 )
		{
			multiplier = (0.91937);
		}
		else
		if( Arms_calculation < 1200 )
		{
			multiplier = (0.92);
		}
		else
		if( Arms_calculation < 1300 )
		{
			multiplier = (0.9016);
		}
		else
		if( Arms_calculation < 1400 )
		{
			multiplier = (0.92308);
		}
		else
		if( Arms_calculation < 1600 )
		{
			multiplier = (0.926);
		}
		else
		if( Arms_calculation < 1800 )
		{
			multiplier = (0.931);
		}
		else
		if( Arms_calculation < 2000 )
		{
			multiplier = (0.935);
		}
		else
		if( Arms_calculation < 2200 )
		{
			multiplier = (0.93545);
		}
		else
		if( Arms_calculation < 2400 )
		{
			multiplier = (0.965);
		}
		else
		{
			multiplier = (0.965);
		}
		

		adc_readings.field_current_short_avg_ma = (Arms_calculation * multiplier);


		// 9/23/2013 rmd : Because of the 60 hz current bleed through the cap and resistor in series
		// around the triac terminals (150 ohm and .022uF) with all the outputs wired in we can see
		// several ma. We do not want to show this to the user as it just raises more questions than
		// its worth. I'm not going to supress it here however (with a statement such as less than
		// 10ma is ZERO). Let the UI hide it. This way we could show it on certain service screens
		// if we so choose.
		
		// ----------

		// 9/20/2013 rmd : Start again.
		squared_sum_for_short_average = 0;
		
		sample_count_for_short_avg = 0;
	}

	// ----------

	// 5/17/2013 rmd : Okay we're done readings the readings. Can free the mem.
	FreeMemBlk( raw );

	// ----------
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

