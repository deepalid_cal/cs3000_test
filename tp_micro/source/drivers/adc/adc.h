/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_ADC_H
#define INC_ADC_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//-----------------------------------------------------------------------------
//  The ADC reference voltage: 3.00 VDC +/- .5% from LM4040C30 shunt reference
//-----------------------------------------------------------------------------
#define ADC_REF_VOLTAGE								(3.00)

// The LPC176X has a 12-bit ADC
#define ADC_VALUE_BITS								(12)

// The LPC176x ADC conversion rate must be <= 200 KHz
#define ADC_CONVERSION_RATE_PER_SEC					(160000)

// 9/11/2013 rmd : This is based on the systick rate, which is 4000 times per second.
#define ADC_CONVERSIONS_PER_SECOND			(4000)

// ----------

// 6/23/2017 dks :  For the two POC ADC channels we will try processing readings about every 5 seconds
// Raw ADC measurements are available at a rate of approximately 60 Hz.
#define POC_ADC_CHANNELS_SAMPLES_PER_READING					(300)

// 6/23/2017 dks : Constants for scaling ADC counts into useful units for POC measurements
//
// For the 0-5V Scaling
#define POC_5V_MULT     (20481)     // See derivation in comments below
#define POC_5V_DIV      (16384)     // See derivation in comments below

// For the 4-20 mA Scaling
#define POC_420_MULT    (46154)     // See derivation in comments below
#define POC_420_DIV     (8192)      // See derivation in comments below

// 6/23/2017 dks : The two POC analog measurements are scaled as follows:
//
//       POC 4-20 mA : The UNS_32 final output value is in units of micro-Amperes (uA)
//
//       The scaling is done using fixed point math. One ADC count = 5.6340 uA
//       The scaling constants were calculated as follows:
//
//       The POC 130 ohm resistor R29 defines the I-to-V scaling. Full scale to the ADC from this
//       resistor will be 3V / 130 ohms = 23.0769230 mA or 23,0769.23 uA. This current corresponds to 4096 ADC counts.
//       So, 23,0769.23 uA / 4096 = 5.6340 uA / count.
//       5.6340 uA/count * 8192 = 46,153.72 uA/count but we round this up to 46154 uA/count. This is POC_420_MULT.
//       Because the current scaling factor has been multiplied by 8192, we must divide the product by 8192.
//       This divisor constant is POC_420_DIV.
//
//       Processing 4-20 mA ADC samples:
//
//       First, a number (POC_ADC_CHANNELS_SAMPLES_PER_READING) of raw ADC values are read and summed.
//       This accumulation of values is then divided by the number of samples. Call this avg_adc_iraw_value. 
//       Then this value is scaled per the algorithm described above:
//
//          output_current_in_uA = (avg_adc_iraw_value x POC_420_MULT) / POC_420_DIV 
//
//       -------------------------------------------------------------------------------------------------------------
//       POC 5V      : The UNS_32 final output value is in units of milli-Volts (mV)
//
//       The scaling is done using fixed point math. Here is how it is done:

//       One ADC count = 1.250073 mV (at 0-5V input) This is calculated as follows:
//       
//       The POC 0-5V circuit uses a simple resistive voltage divider followed by a unity gain op-amp stage.
//       The voltage divider output is given by Vout = R33/(R33 + R31) x Vinp. Since R31 = 4.7K and R33 = 6.65K the
//       "gain" of the divider is given by 6.65/(6.65 + 4.7) = .5859031.
//       At the input to the ADC an ADC count corresponds to 3.00V / 4096 count = .732421 mV / count.
//       However, since our POC divider attenuates the input, our effective ADC scale factor is:
//       .732421 mV / count * (1/.5859031) = 1.25007 mV / count.
//
//       Now for the fixed point scaling:

//       The effective adc scale factor is multiplied by 16384. 1.25007 mV / count x 16384 = 20,481.178. 
//       We round this to an integer constant of 20481. This is the #defined value POC_5V_MULT.
//       Now, since we've multiplied the scale factor by 16384, to get the proper final result from adc counts
//       we must multiply it times the number of ADC counts, then divide the product by 16384. 16384 is the value
//       of the #define POC_5V_DIV.
//
//       Processing POC 0-5V ADC samples:
//
//       First, a number (POC_ADC_CHANNELS_SAMPLES_PER_READING) of raw ADC values are read and summed.
//       This accumulation of values is then divided by the number of samples. Call this avg_adc_vraw_value. 
//       Then this value is scaled per the algorithm described above:
//
//          output_voltage_in_mV = (avg_adc_vraw_value x POC_5V_MULT) / POC_5V_DIV 
//
//       --------------------------------------------------------------------------------------------------------------

// 9/11/2013 rmd : In order to measure the two_wire current we have to subtract out the
// average of the readings in the buffer from each reading. The average represents the
// temperature. In order to get an accurate average value (which means an accurate
// temperature measurement) we need a full 60 hertz cycle. Because the current sine wave
// (the AC component) rides on top of the temperature (the DC component). The average value
// is the DC component and that is what we subtract from each reading to get the AC amperage
// reading. All amperage readings are 60 hertz waveforms. We might be able to also do it
// with half a cycle, or 1.5 cycles, but I think we'll work with a full 60 hertz cycle. Well
// as close as we can get. At a 4KHz reading rate a full 60 Hz cycle would be 67 readings.
#define	AMPERAGE_SAMPLES_TO_COLLECT				(67)
// ----------

// We want this to trip at 2.3Arms.  We are measuring all along the rectified AC wave form
// though, so we cant just use the peak value for 2.3Arms. (3.253Vpk) 5/31/2012 raf : This
// is a very tricky number to arrive at.  The waveform we are measuring is through a current
// sense transformer and through a rectifier and a DC offset added.  This number is also a
// raw ADC count, since we dont want to bother with the math yet. Also we have to keep in
// mind that we need 3 consecutive "hits" above this number in order to trip short
// protection.  So its all about how long the waveform is above this number.  The way the
// sampling lines up, the AC 60 hz waveform matches up to the 1ms tick every 3 cycles. If
// its >= 3ms, theres no issue as we will have that 3 hits every cycle. If its 2ms, it will
// never trip short protection. If its between 2 and 3,....   2 - 2 1/3  => no short
// protection (unless the first hit comes in that period!) 2 1/3 - 2 2/3 => short protection
// after 1-3 cycles 2 2/3 - 3 => short protection after 1-2 cycles. Because the wave form
// isnt a traditional AC current waveform, this number cant be reached by any simple math
// that im aware of. I tried a couple things, calculating the peak, and then finding the
// points 1ms away from the peak and using that number seems to be the best option though.

// For Example:   2.3Arms is the limit we want so first we find peak => 2.3/0.707 =
// 3.253Apk, now we want to find the value where short detection will kick in, which is 1
// 1/6 ms away from the peaks.  After doing simple sin calculaitons its found that this is
// 89.7% of the peak value.. For reference, heres a small chart
//										ms window		%Peak
//										2				93
//										2 1/3			89.7
//										3				84.4%

// 9/23/2013 rmd : Basically I just put a 2.4 amp load on and looked at the raw counts
// buffer. It seems to top out at around 3300. I choose therefore a trip level of 3200.
// Testing shows that at 3200 it trips just at around 2.3AMPS. I think right where we want
// it to be.
#define ADC_CURRENT_LIMIT_THRESHOLD_RAW_COUNT		(3230)



// 5/8/2013 rmd : In order to implement a hysteresis effect we reduce the limit when hunting
// for a short. So that a valve that 'just' tripped the limit will be picked up when trying
// to reproduce the short for the hunt. In case that valve first tripped by being JUST over
// the limit. A good number like 100 seems to work fine. Which is about a 3% reduction in
// the current needed to trip a short.
#define	ADC_CURRENT_LIMIT_REDUCTION_RAW_COUNT		(100)


//-----------------------------------------------------------------------------
//  This is the current transformer and op amp output scale factor - expressed
//  in volts per field power ampere.
//-----------------------------------------------------------------------------

// These configure the ADC input channel assignments
#define ADC_2_WIRE_CUR_CHANNEL						(0)
#define ADC_CURRENT_SENSE_CHANNEL					(1)
#define ADC_POC_4_20_MA_CHANNEL						(2)
#define ADC_POC_5V_CHANNEL							(3)


//////////////////////////////////////////////////////////////////////////
// The next two values (M & B) are used to compute Irms from Vrms using
// a "y = mx + b" linear approximation: I = m x Vrms + b
//
// It assumes a perfect ADC reference voltage as #defined below
// ///////////////////////////////////////////////////////////////////////


// 5/31/2012 raf :  We have made 4 different ranges for ADC values for when we factor and
// offset them.  The ranges are defined in ADC_RANGE_#_TOP. These numbers were obtained by
// just measuring at points along the range and using Excel to curve fit an equation.  The
// document is saved in the development folder.

//#define ADC_VOLTS_TO_AMPS_RANGE_ZERO_MULTIPLIER		(1.2799)
#define ADC_VOLTS_TO_AMPS_RANGE_ZERO_MULTIPLIER		(1.4047)
//#define ADC_VOLTS_TO_AMPS_RANGE_ZERO_OFFSET			(.004908)
#define ADC_VOLTS_TO_AMPS_RANGE_ZERO_OFFSET			(0.0)

//#define ADC_VOLTS_TO_AMPS_RANGE_ONE_MULTIPLIER		(1.2127)
#define ADC_VOLTS_TO_AMPS_RANGE_ONE_MULTIPLIER		(1.3137)
//#define ADC_VOLTS_TO_AMPS_RANGE_ONE_OFFSET			(.007349)
#define ADC_VOLTS_TO_AMPS_RANGE_ONE_OFFSET			(0.0)

//#define ADC_VOLTS_TO_AMPS_RANGE_TWO_MULTIPLIER		(1.2102)
#define ADC_VOLTS_TO_AMPS_RANGE_TWO_MULTIPLIER		(1.2358)
//#define ADC_VOLTS_TO_AMPS_RANGE_TWO_OFFSET			(.004862)
#define ADC_VOLTS_TO_AMPS_RANGE_TWO_OFFSET			(0.0)

#define ADC_VOLTS_TO_AMPS_RANGE_THREE_MULTIPLIER	(1.4311)
//#define ADC_VOLTS_TO_AMPS_RANGE_THREE_OFFSET		(-.20747)
#define ADC_VOLTS_TO_AMPS_RANGE_THREE_OFFSET		(0.0)

//-----------------------------------------------------------------------------
//  These defines are used for current calculations in the ADC task
//-----------------------------------------------------------------------------

#define ADC_VOLTS_PER_COUNT							(((float)ADC_REF_VOLTAGE) / ((float) (1 << ADC_VALUE_BITS)))


typedef struct
{
	volatile UNS_32		results[ 4 ];

	UNS_32				*read_from, *write_to;

	UNS_8				current_channel;

} ADC_CONTROL_s;

extern volatile ADC_CONTROL_s g_adc_control;



typedef struct
{
	UNS_32			two_wire_current_samples[ AMPERAGE_SAMPLES_TO_COLLECT ];
	
	UNS_32			field_24VAC_current_sense[ AMPERAGE_SAMPLES_TO_COLLECT ];
	
	UNS_32			poc_5V_analog_sample;

	UNS_32			poc_420_analog_sample;

} ADC_RAW_SAMPLES_STRUCT;

// ----------

typedef struct
{
	// 9/20/2013 rmd : This value is developed by average the current samples over 15 full 60hz
	// cycles. That is 250ms. It is always available for use and will be no more than 250ms
	// old.
	UNS_32	field_current_short_avg_ma;
	
	// 6/22/2017 dks :  Calculated by averaging and scaling POC_ADC_CHANNELS_SAMPLES_PER_READING samples from the POC 5v analog ADC input.  
	UNS_32	POC_5V_analog_mvolts;      // These are scaled millivolts
	
	// 6/22/2017 dks :  Calculated by averaging the last POC_ADC_CHANNELS_SAMPLES_PER_READING samples from the POC 4-20ma ADC input.  
	UNS_32	POC_420_analog_uamps;      // These are scaled microamps
	

	UNS_32	two_wire_cable_mamps;

	UNS_32	two_wire_present_avg_temperature;

	// 10/29/2013 rmd : Inherently initialized to (false) upon program start by the C startup
	// code.
	BOOL_32	two_wire_terminal_over_heated;
	
	// 10/29/2013 rmd : Inherently initialized to (false) upon program start by the C startup
	// code.
	BOOL_32	two_wire_cable_excessive_current;

} ADC_RESULTS_STRUCT;

// ----------

extern BOOL_32		clear_short_count;  // initial state of (false) is just fine

extern ADC_RESULTS_STRUCT		adc_readings;

// ----------

// 2/12/2013 rmd : Used to hold off the A-D conversions until 1 second after all tasks are
// up and running.
extern BOOL_32	commence_A_D_conversions;


extern volatile UNS_32 g_our_timer;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */



extern void adc_task( void *pvParameters );



extern void develop_average_adc_readings_for_each_channel( ADC_RAW_SAMPLES_STRUCT *raw );



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

