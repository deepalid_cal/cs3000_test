/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"i2cdrvr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//-----------------------------------------------------------------------------
//  I2cDrvr.c - I2C Driver for Calsense TP Micro
//-----------------------------------------------------------------------------

volatile BOOL_32 g_i2c_complete = FALSE;

volatile BOOL_32 g_i2c_failed;

/*********************************************************************//**
 * @brief       Main I2C0 interrupt handler sub-routine
 * @param[in]   None
 * @return      None
 **********************************************************************/
void I2C0_IRQHandler(void)
{
	// just call std int handler
	signed portBASE_TYPE    xTaskWoken = pdFALSE;		//this must be initialized to false

	g_i2c_failed = FALSE;

	I2C_MasterHandler(LPC_I2C0);

	if( I2C_MasterTransferComplete(LPC_I2C0) )
	{
		g_i2c_complete = TRUE;

		xSemaphoreGiveFromISR( g_xI2cSema, &xTaskWoken );
	}

	portEND_SWITCHING_ISR( xTaskWoken );
}

//-----------------------------------------------------------------------------
//  This function will configure the I2C0 port in Master mode, 400 KHz
//  It uses the open-drain port pins P0.27 (SDA0) and P0.28 (SCL0)
//-----------------------------------------------------------------------------
void initI2cDrvr(void)
{
	NVIC_SetPriority( I2C0_IRQn, I2C_INT_PRIORITY );

	// Initialize the I2C port 0 for 400K bps clock rate
	I2C_Init( LPC_I2C0, TP_I2C_CLOCKSPEED_HZ );

	// Enable I2C port 0
	I2C_Cmd( LPC_I2C0, ENABLE );
}

//-----------------------------------------------------------------------------
//  Wrapper function for I2C_MasterTransferData() - eliminates need to specify
//  the I2C port "LPC_I2C0" each time.
//
//  Note: Status is an enum defined in cmsis/lpc_types.h
//-----------------------------------------------------------------------------
Status I2cTransferData(I2C_M_SETUP_Type *TransferCfg, I2C_TRANSFER_OPT_Type Opt )
{
	Status rv = ERROR;

	UNS_32	time_out = MS_to_TICKS( 100 );

	g_i2c_complete = FALSE;

	// ----------
	
	I2C_MasterTransferData( LPC_I2C0, TransferCfg, Opt );

	// ----------
	
	//while ((g_i2c_complete == FALSE) && (time_out-- != 0));
	if( xSemaphoreTake( g_xI2cSema, time_out ) == pdTRUE )
	{
		//should we clock out  9 clock pulses if it fails to reset the i2c bus???? TODO:

		if( g_i2c_failed == FALSE )
		{
			rv = SUCCESS;
		}
	}

	// ----------
	
	if( rv != SUCCESS )
	{
		// 2/19/2013 rmd : I have seen errors that really lock up the I2C hardware/firmware state
		// machine. This is an attempt at clearing it up. But doesn't seem to help. Or hurt, so I
		// left in as it seems something like this should be taking place.
		I2C_Cmd( LPC_I2C0, DISABLE );

		vTaskDelay( MS_to_TICKS(5) );
		
		I2C_Cmd( LPC_I2C0, ENABLE );
	}

	return( rv );
}

