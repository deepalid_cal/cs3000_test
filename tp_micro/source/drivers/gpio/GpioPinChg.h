/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_GPIO_PIN_CHG_H_
#define INC_GPIO_PIN_CHG_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
		 
//-----------------------------------------------------------------------------
//    Port 0 bits used for pin change interrupts
//-----------------------------------------------------------------------------

// 1/15/2013 rmd : All the devices are set to generate an interrupt on their falling edge.
// In all cases it really doesn't matter ... rising edge or falling edge. As they each
// generate a pulse with both edges present. And what we are actually counting is the pulses
// by seeing one edge or the other.

#define	RAIN_BUCKET_P0_BIT		_BIT( 5 )

#define FLOW_METER_P0_BIT		_BIT( 6 )

#define WIND_GAGE_P0_BIT		_BIT( 7 )

#define	ET_GAGE_P0_BIT			_BIT( 9 )


typedef struct 
{
	// 1/16/2013 rmd : A very low rate 5ms wide pulse. One pulse per .01 inches of rain.
	UNS_32	RAIN_BUCKET_CNTS;

	// ----------
	
	// 1/12/2015 rmd : Used for the Badger Meter pulse flow meter. Generally in the range 0 to
	// 100 hertz. Pulse width varies somewhat with frequency but is around 4 to 5 ms wide.
	// Tested to maximum rate of 200 hz. For these traditional paddle wheel flow meters this
	// value is also used to calculate a rate used for the accumulators and flow checking.
	//
	// Also used for the Bermad flow meter pulse count. This value will directly drive the
	// accumulators when the flow meter is specified as a Bermad. This value is not involved in
	// the user displayed value or the flow checking value for the Bermad case.
	UNS_32	fm_pulse_count;

	// ----------
	
	// 1/13/2015 rmd : For the case of the Bermad we determine the flow rate by measuring the
	// time delta between pulses. Our minimum flow rate is 10 gpm. And our maximum is flow rate
	// is 1200 gpm. David Byma and I decided these values. This is regardless of the Bermad
	// valve size.

	// 1/13/2015 rmd : A value used only at the TPMicro. Used to develop the delta value below.
	// Initialize to 0 on program startup.
	UNS_32	fm_time_stamp_at_last_fm_pulse_4khz;

	// 1/13/2015 rmd : Initialize this value to 0 to indicate the delta is not valid. Meaning 0
	// flow rate as far as anyone receiving this information is concerned.
	UNS_32	fm_delta_between_last_two_fm_pulses_4khz;
	
	// 1/14/2015 rmd : If it is not valid, such as on startup or after it has not seen a pulse
	// for a long time - 60 seconds - we set this false. Which leads us to set the delta to 0.
	// And requires 2 pulses to get a new delta.
	UNS_32	fm_last_reading_valid;
	
	// 1/15/2015 rmd : As we get a pulse from the once per second accumulated structure we clear
	// this to 0. When we don't see a pulse we just let this keep counting.
	UNS_32	fm_seconds_since_last_pulse;
	
	// ----------
	
	// 1/16/2013 rmd : At 150mph wind we get 500 hertz pulse rate. Tested to a 1 kHz rate.
	UNS_32	WIND_GAGE_CNTS;
	
	// 2/20/2013 rmd : The ET-GAGE pulse is border line 1 to two seconds wide. I used to think
	// of it more like 5 seconds wide. And was going to poll it at a one hertz rate. Turns out
	// the pulse is occasionally less than 2 seconds wide. So being that close to one second
	// I'll just use interrupts to pick it up. Like the RAIN and WIND and FLOW.
	UNS_32	ET_GAGE_CNTS;
	
} PORT_ZERO_COUNTS_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern volatile UNS_32		fm_time_stamp_counter_4kHz;

extern volatile PORT_ZERO_COUNTS_STRUCT	accumulators_at_the_task_level;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_gpio_pin_change_interrupts( void );


extern void process_counters_from_rit_isr( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


/*
#define		_TPM_ET_GAGE_PIN			( _BIT(9) )

#define		TPM_ET_GAGE_IS_HIGH			( (LPC_GPIO0->FIOPIN & _TPM_ET_GAGE_PIN) == _TPM_ET_GAGE_PIN )

#define		TPM_ET_GAGE_IS_LOW			( (LPC_GPIO0->FIOPIN & _TPM_ET_GAGE_PIN) == 0 )
*/ 


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*GPIOPINCHG_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

