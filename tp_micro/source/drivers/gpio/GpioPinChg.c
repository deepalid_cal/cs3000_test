/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"GpioPinChg.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Define for GPIOINT register bits
#define IOINTSTATUS_P0_INT_PENDING_BIT	(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/14/2015 rmd : A firmware counter driven by the 4kHz sys_tick hardware counter
// interrupt. No need to concern with rollover as the math all works out when determining
// time between pulses as long as everything involved is unsigned 32 bit.
volatile UNS_32		fm_time_stamp_counter_4kHz;

// 1/15/2013 rmd : This variable is added to during the GPIO/EINT3 interrupt. It is then
// read and each counter cleared back to zero once per second in the RIT interrupt. Also in
// the RIT a copy is made into a second variable for use at the task level. A key component
// to this stragedy is that the RIT and GPIO interrupts do not nest. That is they are at the
// same interrupt priority level. That is the designed in protection that allows us to share
// this variable between the two interrupts.
volatile PORT_ZERO_COUNTS_STRUCT	accumulators_at_the_interrupt_level;

// 5/22/2014 rmd : Though almost surely insignificant, we use an intermediate variable to
// snatch a copy of the accumulated interrupt driven flow meter count (and other devices
// count) using the intermediate variable to suppress the one hertz task processing jitter.
// This variable is loaded at each one hertz RIT interrupt. And is available at the task
// level within a critical section.
volatile PORT_ZERO_COUNTS_STRUCT	accumulators_at_the_task_level;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void init_gpio_pin_change_interrupts( void )
{
	// 1/15/2013 rmd : Start with zeroed counts.
	memset( (void*)&accumulators_at_the_interrupt_level, 0x00, sizeof(accumulators_at_the_interrupt_level) );
	
	// ----------
	
	// 1/15/2013 rmd : Enable falling edge interrupts for all devices.
	((LPC_GPIOINT_TypeDef*)LPC_GPIOINT)->IO0IntEnF = ( RAIN_BUCKET_P0_BIT | FLOW_METER_P0_BIT | WIND_GAGE_P0_BIT | ET_GAGE_P0_BIT );
	
	// ----------
	
	NVIC_SetPriority( EINT3_IRQn, GPIO_PIN_CHANGE_INT_PRIORITY );

	// Enable GPIO interrupt at NVIC
	NVIC_EnableIRQ( EINT3_IRQn );
}

/* ---------------------------------------------------------- */
void EINT3_IRQHandler(void)
{
	// 11/12/2012 rmd : This interrupt is the handler for both the Port 0 and Port 2 gpio
	// interrupts. And the EXTI3 itself if enabled.

	// ----------

	UNS_32	fei;
	
	UNS_32	time_stamp;

	// ----------

	// 1/16/2013 rmd : See if there is any Port 0 interrupts. Should always be. As this is the
	// only reason this isr should fire.
	if( ((LPC_GPIOINT_TypeDef*)LPC_GPIOINT)->IntStatus & (1<<IOINTSTATUS_P0_INT_PENDING_BIT) )
	{
		// 1/16/2013 rmd : Capture falling edge interrupts present.
		fei = ((LPC_GPIOINT_TypeDef*)LPC_GPIOINT)->IO0IntStatF;

		if( fei )
		{
			// 1/15/2013 rmd : Then see who it is. Could be more than one device that caused the
			// interrupt.
			if( fei & RAIN_BUCKET_P0_BIT )
			{
				accumulators_at_the_interrupt_level.RAIN_BUCKET_CNTS += 1;
			}

			if( fei & FLOW_METER_P0_BIT )
			{
				// 1/14/2015 rmd : Add to the count.
				accumulators_at_the_interrupt_level.fm_pulse_count += 1;
				
				// ----------
				
				// 1/14/2015 rmd : Get a copy of the time stamp.
				time_stamp = fm_time_stamp_counter_4kHz;

				if( accumulators_at_the_interrupt_level.fm_last_reading_valid )
				{
					// 1/14/2015 rmd : Create the delta because we have two counts within the allowable time.
					// Remember this ALWAYS works even if the timer rolled over in between readings. See counter
					// roll-over test in main.c.
					accumulators_at_the_interrupt_level.fm_delta_between_last_two_fm_pulses_4khz = (time_stamp - accumulators_at_the_interrupt_level.fm_time_stamp_at_last_fm_pulse_4khz);
				}
				else
				{
					// 1/14/2015 rmd : We saw a reading. So set it true. And below time stamp it.
					accumulators_at_the_interrupt_level.fm_last_reading_valid = (true);
				}
				
				// 1/14/2015 rmd : And time stamp this pulse saving the time stamp.
				accumulators_at_the_interrupt_level.fm_time_stamp_at_last_fm_pulse_4khz = time_stamp;
			}

			if( fei & WIND_GAGE_P0_BIT )
			{
				accumulators_at_the_interrupt_level.WIND_GAGE_CNTS += 1;
			}

			if( fei & ET_GAGE_P0_BIT )
			{
				accumulators_at_the_interrupt_level.ET_GAGE_CNTS += 1;
			}
		}

		// 1/15/2013 rmd : Now reset the interrupts. But be sure to only OR this with the INTERRUPT
		// STATUS read. Otherwise, if you were to clear all the bits in the register, you could miss
		// an edge that comes in during the processing of this ISR.
		//((LPC_GPIOINT_TypeDef*)LPC_GPIOINT)->IO0IntClr = (RisingEdgeInts | FallingEdgeInts);  if there were rising edge 
		((LPC_GPIOINT_TypeDef*)LPC_GPIOINT)->IO0IntClr = fei;
	}
}

/*
// 1/7/2015 rmd : Example code showing way to handling both falling and rising edge // 
interrupts. Presently in the TPMicro they are all falling edge. 

static __interrupt void EINT3_Interrupt(void)
{
	unsigned long ulInterrupts;
	unsigned long ulBit;
	
	if (IOIntStatus & P0int)
	{ 
		// an interrupt from port 0 is pending
		ulInterrupts = IO0IntStatR; 
		// get rising edge interrupts on port 0
		ulInterrupts |= IO0IntStatF; 
		// also falling edge interrupts
		IO0IntClr = ulInterrupts; 
		// clear the ones which will be handled
		ulBit = 0x00000001;

		iInterrupt = 0;

		while (ulInterrupts >= ulBit)
		{ 
			// for each input change that has been detected on port 0
			if (ulInterrupts & ulBit)
			{ 
				// an enabled input has changed
				uDisable_Interrupt(); 
				// protect the call from interrupts
				(gpio_handlers_0[iInterrupt])(); 
				// call the application handler
				uEnable_Interrupt(); 
				// release
				ulInterrupts &= ~ulBit;
			}

			iInterrupt++;

			ulBit <<= 1;
		}
	}
}
*/

/* ---------------------------------------------------------- */
extern void process_counters_from_rit_isr( void )
{
	// 1/15/2013 rmd : Called at a one hertz rate from the RIT interrupt service routine.
	
	// 1/15/2013 rmd : Snatch a copy and zero 'em out. Remember both these variables have an
	// inherent protection mechanism because they are manipulated within ISR's at the same
	// priority level and therefore cannot nest. See their declarations for an explanation.
	accumulators_at_the_task_level = accumulators_at_the_interrupt_level;
	
	// 1/15/2015 rmd : Remember this is an ISR. Keep the code tight. Need to clear some of the
	// variables to start the one second accumulations over again.
	if( accumulators_at_the_interrupt_level.fm_pulse_count )
	{
		accumulators_at_the_interrupt_level.fm_pulse_count = 0;

		accumulators_at_the_interrupt_level.fm_seconds_since_last_pulse = 0;
	}
	else
	{
		accumulators_at_the_interrupt_level.fm_seconds_since_last_pulse += 1;

		// 1/15/2015 rmd : If it has been 2 minutes since the last pulse start the delta calculation
		// over again. The main board also sees this seconds_since_last_pulse count and can handle
		// this differently. For example could decide after 30 seconds to show the use a 0 flow
		// rate. Of course that would mean anything less than 20 gpm for the transducer that makes
		// one pulse per 10 gallons would show as 0 gpm to the user. And David Byma and I agreed we
		// would show down to 10 gpm for the case of the Bermad. Regardless of meter size. So that
		// means 60 seconds for the main board to wait before it decides to show 0 gpm to the user.
		if( accumulators_at_the_interrupt_level.fm_seconds_since_last_pulse > 120 )
		{
			accumulators_at_the_interrupt_level.fm_last_reading_valid = (false);	

			// 1/15/2015 rmd : This is CRITICAL to the behavior after the first pulse arrives after no
			// pulses for more than the 120 seconds. Set the delta to 0 so that the main board knows not
			// to use the delta even after the first pulse in a while shows up. We wait for the second
			// pulse within the 120 second window before once again calculating a new delta for the
			// world to use.
			accumulators_at_the_interrupt_level.fm_delta_between_last_two_fm_pulses_4khz = 0;	
		}
	}

	accumulators_at_the_interrupt_level.ET_GAGE_CNTS = 0;
	accumulators_at_the_interrupt_level.RAIN_BUCKET_CNTS = 0;
	accumulators_at_the_interrupt_level.WIND_GAGE_CNTS = 0;
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

