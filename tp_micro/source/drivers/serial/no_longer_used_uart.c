//-----------------------------------------------------------------------------
//  uart.c - uart task for LPC1768
//
// $Id: uart.c,v 1.33 2011/12/16 23:08:54 dsquires Exp $
// $Log: uart.c,v $
// Revision 1.33  2011/12/16 23:08:54  dsquires
// Work in progress. DMA support is coming.
//
// Revision 1.32  2011/12/16 20:06:26  dsquires
// Enabled RLS (receive line status) interrupt, but made ISR handler function for it
//  a NOP.
//
// Revision 1.31  2011/12/14 00:15:48  dsquires
// Changed UART1 baudrate to 1200 so Two Wire UART driver could be debugged.
//
// Revision 1.30  2011/12/10 00:24:31  dsquires
// Removed memset() made unnecessary (and wrong!) now that semaphores and queues are created in startup.c.
//
// Revision 1.29  2011/12/09 00:50:12  dsquires
// Moved queue and semaphore creation to startup.c.
//
// Revision 1.28  2011/11/28 23:07:21  dsquires
// Minor change.
//
// Revision 1.27  2011/11/25 18:41:06  dsquires
// Work in progress.
//
// Revision 1.26  2011/11/24 17:58:15  dsquires
// Revised to work with modified access to uart control structures.
//
// Revision 1.25  2011/11/23 22:35:57  dsquires
// Formatting only.
//
// Revision 1.24  2011/11/21 23:39:08  dsquires
// Work in progress.
//
// Revision 1.23  2011/11/19 21:29:47  dsquires
// Changed baudrates for testing.
//
// Revision 1.22  2011/11/16 23:50:02  dsquires
// Corrected typo due to copy/paste.
//
// Revision 1.21  2011/11/16 20:24:47  dsquires
// Now initialize receive ring buffer in serial task.
//
// Revision 1.20  2011/11/15 21:59:12  dsquires
// Revised UART_CTL_s to accomodate TP ring buffer structure and a new semaphore for
//  signaling rx data available.
//
// Revision 1.19  2011/11/04 22:30:36  dsquires
// Corrected comments.
//
// Revision 1.18  2011/11/04 22:27:10  dsquires
// Changed all UART baudrates to 115200.
//
// Revision 1.17  2011/11/03 20:37:58  dsquires
// Added default baudrate to UART configuration structure. Also added new functions to access UART driver.
//
// Revision 1.16  2011/11/01 21:21:53  dsquires
// Added config for UART3 and dummy table entry for UART2.
//
// Revision 1.15  2011/10/31 21:39:52  dsquires
// Work in progress.
//
//-----------------------------------------------------------------------------

// LPC CMSIS includes
#include "LPC17xx.h"
#include "lpc17xx_clkpwr.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_libcfg.h"
#include "lpc17xx_pinsel.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "task.h"

// Misc includes
#include "string.h"

// TP Micro includes
#include "tp_micro_data_types.h"
#include "startup.h"
#include "MemBlkMgr.h"
#include "RingBuffer.h"
#include "TwoWireDrvr.h"

#if UART_TX_DMA != 0
#include "x_uart.h"
#else
#include "uart.h"
#endif

#include "RxCommTask.h"
#include "test.h"

//-----------------------------------------------------------------------------
//    Configuration constants for each LPC176x UART Port, including the
//    UART used for the two wire serial communications.
//-----------------------------------------------------------------------------
static const UART_CFG_s  UartCfg_s[ TOTAL_UART_PORTS ] = 
{
  {   // UART0 - Main communications
      (LPC_UART_TypeDef *)LPC_UART0, UART0_IRQn, MX_UART_INT_PRIORITY,
      115200,                                                           // baudrate
      UART0_DRVR_ENA_PORT, UART0_DRVR_ENA_PIN                           // RS-485 Driver Enable
  },

  {   // UART1 - Two wire serial communications
      (LPC_UART_TypeDef *)LPC_UART1, UART1_IRQn, MX_UART_INT_PRIORITY,
      1200,                                                             // baudrate
      UART1_DRVR_ENA_PORT, UART1_DRVR_ENA_PIN                           // RS-485 Driver Enable
  },

  {   // UART2 - M1 communications (TTL I/O on JP1: P0.10 (TXD2) and P0.11 (RXD2)
      (LPC_UART_TypeDef *)LPC_UART2, UART2_IRQn, MX_UART_INT_PRIORITY,
      115200,                                                           // baudrate
      UART2_DRVR_ENA_PORT, UART2_DRVR_ENA_PIN                           // RS-485 Driver Enable (none)
  },

  {   // UART3 - M2 communications (TTL I/O on JP3 on IRD-LPC1768-DEV board)
      (LPC_UART_TypeDef *)LPC_UART3, UART3_IRQn, MX_UART_INT_PRIORITY,
      115200,                                                           // baudrate
      UART3_DRVR_ENA_PORT, UART3_DRVR_ENA_PIN                           // RS-485 Driver Enable (none)
  }
};

//-----------------------------------------------------------------------------
//    Uninitialized control structures for the LPC176X UART ports
//-----------------------------------------------------------------------------
volatile UART_CTL_s g_UartCtl_s[ MX_UART_PORTS ];

/************************** PRIVATE FUNCTIONS *************************/

/*********************************************************************//**
 * @brief	UART Line Status Error
 * @param[in]	bLSErrType	UART Line Status Error Type
 * @return	None
 **********************************************************************/
static void UART_IntErr(uint8_t bLSErrType)
{
    //  Line status errors cause this function to be called.
    //  Right now we do nothing, but maybe should bump an error counter?

    //  If this is not useful, perhaps we should not even enable the
    //  UART RLS interrupt?
}

//-----------------------------------------------------------------------------
//  COMMON UART INTERRUPT HANDLER FOR Mx UART PORTS (Main-to-TP Micro, M1, M2)
//-----------------------------------------------------------------------------
static void CommonUartISR( volatile UART_CTL_s *pUartCtl_s )
{
  LPC_UART_TypeDef *UARTx;
  uint32_t intsrc, tmp, tmp1;

  //UNS_8 led;
  UNS_16 tx_cnt;
  UNS_16 rx_cnt;
  UNS_8 c;
  long xTaskWoken = pdFALSE;		 //this must be initialized to false

  // Get pointer to UART register base
  UARTx = pUartCtl_s->UartCfg_s.UARTx;

  /* Determine the interrupt source */
  intsrc = UART_GetIntId( UARTx );

    tmp = intsrc & UART_IIR_INTID_MASK;

   //-----------------------------------------------------------------------------
   //    Highest priority : Receive Line Status
   //-----------------------------------------------------------------------------
   if (tmp == UART_IIR_INTID_RLS )
   {
      // Check line status
      tmp1 = UART_GetLineStatus((LPC_UART_TypeDef *)UARTx);
	
      // Mask out the Receive Ready and Transmit Holding empty status
      tmp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE \
                 | UART_LSR_BI | UART_LSR_RXFE);

      //-----------------------------------------------------------------------------
      //    tmp1 now holds these Rx error bits:
      //
      //    OE = Rx overrun error, PE = parity error, FE = framing error,
      //    BI = Break interrupt, RXFE = FIFO contains at least one UARTx
      //    RX error.
      //-----------------------------------------------------------------------------
      if (tmp1)
      {
         UART_IntErr(tmp1);
      }
   }

   //-----------------------------------------------------------------------------
   //    Next Highest Priority: Receive Data Available 
   //-----------------------------------------------------------------------------
   if (tmp == UART_IIR_INTID_RDA )
   {
      // Rx FIFO has reached its programmed threshold, so read UART_RX_FIFO_INT_THRESHOLD 
      // number of bytes into the ring buffer
      for( rx_cnt = 0; rx_cnt < UART_RX_FIFO_INT_THRESHOLD; rx_cnt++ )
      {
         // Read a byte from the Rx FIFO
         c = UART_ReceiveByte( UARTx );

         // Store the received byte in this UART's ring buffer
         RingBufferPut( &(pUartCtl_s->ringbuffer_s), c );
      }

      // Give the Rx data available semaphore
      xSemaphoreGiveFromISR( pUartCtl_s->xRxDataAvailSema, &xTaskWoken );
   }
   //-----------------------------------------------------------------------------
   //    Or it's an RX character time-out. There are one or more characters 
   //    in the Rx FIFO. We cannot tell how many so read just one character.
   //-----------------------------------------------------------------------------
   else if ( tmp == UART_IIR_INTID_CTI )
   {
      c = UART_ReceiveByte( UARTx );

        // Store the received byte in this UART's ring buffer
        RingBufferPut( &(pUartCtl_s->ringbuffer_s), c );

        // Give the Rx data available semaphore
        xSemaphoreGiveFromISR( pUartCtl_s->xRxDataAvailSema, &xTaskWoken );
   }

   //-----------------------------------------------------------------------------
   //    Lowest Priority: Transmit Holding Register Empty
   //-----------------------------------------------------------------------------
   if (tmp == UART_IIR_INTID_THRE)
   {
      ////////////////////////
      // transmit function
      ////////////////////////

      // Check first that there is at least one byte to send
      if( 0 != pUartCtl_s->TxByteCnt )
      {
         // Calculate how many bytes to put into Tx FIFO
         tx_cnt = pUartCtl_s->TxByteCnt >= UART_FIFO_DEPTH ? UART_FIFO_DEPTH : pUartCtl_s->TxByteCnt;

         // Load up the Tx FIFO
         while( tx_cnt-- )
         {
            // Fetch data from tx block and load next byte into Tx FIFO, bump pointer into data buffer
            UART_SendByte( UARTx, *pUartCtl_s->pTxData++ );

            // Decrement bytes remaining to send
            pUartCtl_s->TxByteCnt--;
         }
      }

      // Check if all data from this buffer has been sent
      if( 0 == pUartCtl_s->TxByteCnt )
      {
         // All data has been sent so disable the THRE interrupt
         UART_IntConfig( UARTx, UART_INTCFG_THRE, DISABLE );

         // Signal UART task to let it know we've sent the data
         xSemaphoreGiveFromISR( pUartCtl_s->xPacketSentSema, &xTaskWoken );
      }
   }

   portEND_SWITCHING_ISR( xTaskWoken );
}  

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
//-----------------------------------------------------------------------------
//  Interrupt handler for UART0 IRQ
//-----------------------------------------------------------------------------
void UART0_IRQHandler(void)
{
   // Turn on the Heartbeat LED
   //LPC_GPIO0->FIOSET = FDI_HBEAT_LED;

   //-----------------------------------------------------------------------------
   //   Call common Mx UART interrupt handler, passing it a pointer to its
   //   UART control structure.
   //-----------------------------------------------------------------------------
   CommonUartISR( &g_UartCtl_s[ 0 ] );

   // Turn off the heartbeat LED
   //LPC_GPIO0->FIOCLR = FDI_HBEAT_LED;
}

//-----------------------------------------------------------------------------
//  Interrupt handler for UART2 IRQ
//
//-----------------------------------------------------------------------------
void UART2_IRQHandler(void)
{
  //-----------------------------------------------------------------------------
  //   Call common Mx UART interrupt handler, passing it a pointer to its
  //   UART control structure.
  //-----------------------------------------------------------------------------
  CommonUartISR( &g_UartCtl_s[ 1 ] );
}

//-----------------------------------------------------------------------------
//  Interrupt handler for UART3 IRQ
//
//  Must fix up once UART3 working OK
//-----------------------------------------------------------------------------
void UART3_IRQHandler(void)
{
  //-----------------------------------------------------------------------------
  //   Call common Mx UART interrupt handler, passing it a pointer to its
  //   UART control structure.
  //-----------------------------------------------------------------------------
  CommonUartISR( &g_UartCtl_s[ 2 ] );
}

//-----------------------------------------------------------------------------
//    void UART_MxInit(  UNS_32 uart_number, _Bool bFirstTime )
//
//    This is the UART initialization function. uart_number is the ACTUAL
//    LPC176x UART number, 0 to 3.
//-----------------------------------------------------------------------------
static void UART_MxInit(  UNS_32 uart_number, _Bool bFirstTime )
{
   UART_CFG_Type UARTConfigStruct;           // CMSIS UART configuration Struct variable
   UART_FIFO_CFG_Type UARTFIFOConfigStruct;  // CMSIS UART FIFO configuration Struct variable
   volatile UART_CTL_s *pUartCtl_s;          // Ptr to this UART's control structure

   // Point to this UART's control structure
   pUartCtl_s = getUartCtl( uart_number );

   if( bFirstTime )
   {
      // Then fill in the port-specific configuration info
      pUartCtl_s->UartCfg_s = UartCfg_s[ uart_number ];
  
      // Get the default baudrate (this can be changed at runtime)
      pUartCtl_s->Baudrate = pUartCtl_s->UartCfg_s.Baudrate;
   }

   // Set the UARTx Peripheral clock to PCLK/4
   switch( uart_number )
   {
      case 0:   CLKPWR_SetPCLKDiv (CLKPWR_PCLKSEL_UART0, CLKPWR_PCLKSEL_CCLK_DIV_4);  break;
      case 1:   CLKPWR_SetPCLKDiv (CLKPWR_PCLKSEL_UART1, CLKPWR_PCLKSEL_CCLK_DIV_4);  break;
      case 2:   CLKPWR_SetPCLKDiv (CLKPWR_PCLKSEL_UART2, CLKPWR_PCLKSEL_CCLK_DIV_4);  break;
      case 3:   CLKPWR_SetPCLKDiv (CLKPWR_PCLKSEL_UART3, CLKPWR_PCLKSEL_CCLK_DIV_4);  break;
      default:  break;
   }

   // Initialize CMSIS configuration structure with defaults (9600, 8N1)
   UART_ConfigStructInit(&UARTConfigStruct);

   // Overwrite CMSIS default baudrate value
   UARTConfigStruct.Baud_rate = (UNS_32) pUartCtl_s->Baudrate;
 
    // Disable interrupts
   portENTER_CRITICAL();

   // Now initialize the UART using values in the CMSIS UARTConfigStruct
   UART_Init( pUartCtl_s->UartCfg_s.UARTx, &UARTConfigStruct);

   /* Initialize CMSIS FIFOConfigStruct to default state:
    * 				- FIFO_DMAMode    = DISABLE
    * 				- FIFO_Level      = UART_FIFO_TRGLEV0
    * 				- FIFO_ResetRxBuf = ENABLE
    * 				- FIFO_ResetTxBuf = ENABLE
    * 				- FIFO_State      = ENABLE
    */
   UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);

   // Set UART trigger level to allow 8 chars 
   UARTFIFOConfigStruct.FIFO_Level = UART_FIFO_TRGLEV2;

   // Initialize the FIFO for UART peripheral
   UART_FIFOConfig(pUartCtl_s->UartCfg_s.UARTx, &UARTFIFOConfigStruct);

   // Clear/initialize the receive data ring buffer to empty
   initRingBuffer( &pUartCtl_s->ringbuffer_s );

   // Enable RBR interrupt in UART
   UART_IntConfig(pUartCtl_s->UartCfg_s.UARTx, UART_INTCFG_RBR, ENABLE);

   // preemption = 1, sub-priority = 1 
   //NVIC_SetPriority(UART0_IRQn, ((0x01<<3)|0x01));

   if( bFirstTime )
   {
      // Set NVIC priority for this UART interrupt
      NVIC_SetPriority( pUartCtl_s->UartCfg_s.IRQn, pUartCtl_s->UartCfg_s.IrqPriority );

      // Enable NVIC Interrupt for this UART channel 
      NVIC_EnableIRQ(pUartCtl_s->UartCfg_s.IRQn);
   }

   // Enable USART transmitter
   UART_TxCmd(pUartCtl_s->UartCfg_s.UARTx, ENABLE);
  
   // Enable THRE interrupt in UART
   UART_IntConfig(pUartCtl_s->UartCfg_s.UARTx, UART_INTCFG_THRE, ENABLE);

   // Enable the Receive Line Status interrupt in the UART
   UART_IntConfig(pUartCtl_s->UartCfg_s.UARTx, UART_INTCFG_RLS, ENABLE );

   // Enable interrupts again
   portEXIT_CRITICAL();
}

//-----------------------------------------------------------------------------
//    Common FreeRTOS UART task for Main-to-TP, M1, and M2 UART ports
//-----------------------------------------------------------------------------
void vSerialTask( void *pvParameters )
{
  UNS_32 uart_number;
  volatile UART_CTL_s *pUartCtl_s;
  long lstatus;
  UNS_8 txbyte;

   //-----------------------------------------------------------------------------
   // The task parameter is the actual UART number 0, 2 or 3
   //----------------------------------------------------------------------------- 
   uart_number = (UNS_32) pvParameters;

   // Perform first-time initialization of UART and driver OS structures
   UART_MxInit( uart_number, TRUE );

   // Point to this UART's control structure
   pUartCtl_s = getUartCtl( uart_number );

   // Wait for UART channel to be enabled (via another system task)
   while( !pUartCtl_s->bEnaCmd )
   {
      vTaskDelay( MS_to_TICKS( UART_ENA_DIS_POLL_MS ) );
   }

   // Change driver state to ENABLED
   pUartCtl_s->bEnaState = TRUE;

   //-----------------------------------------------------------------------------
   //  Top of Mx UART task processing loop
   //-----------------------------------------------------------------------------
   while( TRUE )
   {
      //-----------------------------------------------------------------------------
      //  Check the Tx DATA_HANDLE queue for something to send
      //-----------------------------------------------------------------------------
      lstatus = xQueueReceive(   pUartCtl_s->xTxDataHandleQueue,     // queue handle
                                 (void *) &pUartCtl_s->DataHandle_s, // where to receive the message
                                 UART_TX_QUEUE_TIME_OUT_MS);         // read time-out (ms)

      //  Check the result status (TRUE indicates we read a DATA_HANDLE_s)
      if( pdTRUE == lstatus )
      {
         UNS_16 fifo_bytes;

         //-----------------------------------------------------------------------------
         //  A DATA_HANDLE was read from the queue. Set up to transmit the data block
         //-----------------------------------------------------------------------------
         pUartCtl_s->pTxData   = pUartCtl_s->DataHandle_s.dptr;      // pointer to the data
         pUartCtl_s->TxByteCnt = pUartCtl_s->DataHandle_s.dlen;      // # bytes left to send

         // The RS-485 Driver enable line (if applicable) is asserted
         if( pUartCtl_s->UartCfg_s.DrvrEnaPort != UART_DRVR_ENA_PORT_NONE )
         {
            GPIO_SetValue( pUartCtl_s->UartCfg_s.DrvrEnaPort, 1<<(pUartCtl_s->UartCfg_s.DrvrEnaPin) );
         }

         //-----------------------------------------------------------------------------
         //    Load up the UART Tx FIFO with as many data bytes as possible
         //-----------------------------------------------------------------------------
         fifo_bytes = 0;

         while( (fifo_bytes < UART_FIFO_DEPTH) && (fifo_bytes < pUartCtl_s->DataHandle_s.dlen) )
         {
            // Get the next byte of Tx data
            txbyte = *(pUartCtl_s->pTxData++);

            // Load a Tx byte into the Tx FIFO
            UART_SendByte(pUartCtl_s->UartCfg_s.UARTx, txbyte );

            fifo_bytes++;
         }

         // Adjust the Tx byte count by # bytes loaded
         pUartCtl_s->TxByteCnt -= fifo_bytes;

         // Re)enable the THRE interrupt
         UART_IntConfig(pUartCtl_s->UartCfg_s.UARTx, UART_INTCFG_THRE, ENABLE);

         //-----------------------------------------------------------------------------
         //    The data packet is now being sent..
         //-----------------------------------------------------------------------------

         //  Wait for transmit complete (UART ISR gives the xPacketSentSema)
         xSemaphoreTake( pUartCtl_s->xPacketSentSema, portMAX_DELAY );

         // Wait for UART to go unbusy (transmitter EMPTY)
         while( UNSET != UART_CheckBusy( pUartCtl_s->UartCfg_s.UARTx ) )
         {
            vTaskDelay( MS_to_TICKS( 1 ) );
         }

         // The RS-485 Driver enable line (if applicable) is reset here
         if( pUartCtl_s->UartCfg_s.DrvrEnaPort != UART_DRVR_ENA_PORT_NONE )
         {
            GPIO_ClearValue(  pUartCtl_s->UartCfg_s.DrvrEnaPort, 1<<(pUartCtl_s->UartCfg_s.DrvrEnaPin) );
         }

         //-----------------------------------------------------------------------------
         //    Now the data block can be freed
         //-----------------------------------------------------------------------------
         FreeMemBlk( pUartCtl_s->DataHandle_s.dptr );
      }
      else
      {
         // Nothing was read (the DATA HANDLE queue read timed out). Check the driver enable flag
         if( !pUartCtl_s->bEnaCmd )
         {
            //  The driver has been disabled, so disable the UART interrupts
            UART_DeInit( pUartCtl_s->UartCfg_s.UARTx );

            //  Reset the UART driver state
            pUartCtl_s->bEnaState = FALSE;

            // Then just wait for the driver to be enabled again..
            while( !pUartCtl_s->bEnaCmd )
            {
               vTaskDelay( MS_to_TICKS( UART_ENA_DIS_POLL_MS ) );
            }

            //-----------------------------------------------------------------------------
            //  The driver has been re-enabled
            //-----------------------------------------------------------------------------

            // Reinitialize the UART. The baudrate may have been changed
            UART_MxInit( uart_number, FALSE );
         }
      }
   }
}

//-----------------------------------------------------------------------------
//    UART_CFG_s *getUartCfg( UNS_32 uart )
//
//    This function returns a pointer to the UART configuration for the 
//    specified physical UART # ( 0 to 3 inclusive ). Providing this function
//    allows us to make the UART configuration structure static (private).
//-----------------------------------------------------------------------------
const UART_CFG_s *getUartCfg( UNS_32 uart )
{
   return ( &UartCfg_s[ uart ] );
}

//-----------------------------------------------------------------------------
//    UART_CTL_s *getUartCtl( UNS_32 uart )
//
//    This function returns a pointer to the UART_CTRL_s for the specified
//    UART number. UART 1 is not a valid parameter - it's used by the
//    special Two Wire serial driver.
//-----------------------------------------------------------------------------
volatile UART_CTL_s *getUartCtl( UNS_32 uart )
{
   volatile UART_CTL_s *pUartCtl_s;

   switch( uart )
   {
      case UPORT_MAIN:  pUartCtl_s = &g_UartCtl_s[ UART0_CTL_BLK_IDX ];   break;
      case UPORT_M1:    pUartCtl_s = &g_UartCtl_s[ UART2_CTL_BLK_IDX ];   break;
      case UPORT_M2:    pUartCtl_s = &g_UartCtl_s[ UART3_CTL_BLK_IDX ];   break;
      default:          DEBUG_STOP( UART + 4 );       break;
   }
   return pUartCtl_s;
}

//-----------------------------------------------------------------------------
//    _Bool UartState( UNS_32 uart )
//
//    This function returns the current driver state of the specified UART
//-----------------------------------------------------------------------------
_Bool UartState( UNS_32 uart )
{
   volatile UART_CTL_s *pUartCtl_s;

   pUartCtl_s = getUartCtl( uart );

   return pUartCtl_s->bEnaState;
}

//-----------------------------------------------------------------------------
//    void UartEnable( UNS_32 uart, UNS_32 baudrate)
//
//    This function is used to enable a UART driver. The baudrate parameter
//    can be set to zero to use the default baudrate for the port or it can
//    specify a non-zero baudrate.
//-----------------------------------------------------------------------------
void UartEnable( UNS_32 uart, UNS_32 baudrate)
{
   volatile UART_CTL_s *pUartCtl_s;

   pUartCtl_s = getUartCtl( uart );

   // Use specified baudrate or default baudrate
   pUartCtl_s->Baudrate = baudrate ? baudrate : pUartCtl_s->UartCfg_s.Baudrate;

   pUartCtl_s->bEnaCmd = TRUE;
}

//-----------------------------------------------------------------------------
//    void UartDisable( UNS_32 uart )
//
// This function disables the UART driver for the specified UART port
//-----------------------------------------------------------------------------
void UartDisable( UNS_32 uart )
{
   volatile UART_CTL_s *pUartCtl_s;

   pUartCtl_s = getUartCtl( uart );

   pUartCtl_s->bEnaCmd = FALSE;
}
