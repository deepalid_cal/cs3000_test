/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"x_uart.h"

#include	"lpc17xx_gpdma.h"

#include	"tpmicro_comm_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Flag to indicate if GPDMA controller has been initialized (interrupts, etc)
static BOOL_32 g_bGPDMA_Initialized = FALSE;

extern uint32_t converUSecToVal (uint32_t timernum, uint32_t usec);
extern uint32_t converPtrToTimeNum (LPC_TIM_TypeDef *TIMx);

//-----------------------------------------------------------------------------
//    Configuration constants for each LPC176x UART Port, including the
//    UART used for the two wire serial communications.
//    2012.03.06 raf  Had to redo this struct because I changed the definition in the header
//    file.  The new structure contains a DMA struct to better organize the data.  Also
//    added the timers that control the drive line to the DMA stuct for better code
//    organization.
//
//		4/27/2012 raf :  Did some baudrate testing on the rs485 lines.
//						 115200 -> 6000 ft worked well, 8000 ft nothing got through
//						57600	->	9000 ft worked well.
//-----------------------------------------------------------------------------
static const UART_CFG_s  uart_config[ MANAGED_UART_PORTS ] =
{
	{   // UART0 - Main communications
		.UARTx = (LPC_UART_TypeDef *)LPC_UART0, 					// Pointer to hardware address of UART
		.IRQn = UART0_IRQn,   										// The interrupt that is thrown when this UART recieves data
		.IrqPriority = MX_UART_INT_PRIORITY,  						// The Priority of the UART Interrupt, this is defined in FreeRTOSConfig.h

		._baud_rate = 115200,                                   	// baudrate the port operates at

		.stopBits = UART_STOPBIT_1,

		.bUsesDMA = (true),											// Used to set whether the port uses DMA or not, TODO: Re-Implement nonDMA Tx
		.DmaCfg_s.GPDMA_Conn = GPDMA_CONN_UART0_Tx,                 // The Device the DMA is communicating with TODO: Find out if this is redundant, can we remove this?
		.DmaCfg_s.GPDMA_Channel = UART0_DMA_CHAN,                   // GPDMA channel for transmit 0-9 Defined in x_uart.h

		.fifo_trigger_level = UART_FIFO_TRGLEV2,
		.fifo_trigger_count = 8,									// must set the trigger count to match the trigger level
		
		.rs485.manages_485_driver = (false)							// PORT_TP has no 485 driver.
	},

	/*
	{   // UART1 - Two wire serial communications
		.UARTx = (LPC_UART_TypeDef *)LPC_UART1,   					// Pointer to hardware address of UART
		.IRQn = UART1_IRQn,   										// The interrupt that is thrown when this UART recieves data
		.IrqPriority = MX_UART_INT_PRIORITY,  						// The Priority of the UART Interrupt, this is defined in FreeRTOSConfig.h
		._baud_rate = 1200,											// baudrate the port operates at
		.stopBits = UART_STOPBIT_2,

		.bUsesDMA = (false),

        .fifo_trigger_level = UART_FIFO_TRGLEV2,

		.rs485.manages_485_driver = (false)							// The 2 Wire UART does not have a 485 driver.
	},
	*/
	
	{   // UART2 - M1 communications (TTL I/O on JP1: P0.10 (TXD2) and P0.11 (RXD2)
		.UARTx = (LPC_UART_TypeDef *)LPC_UART2,   					// Pointer to hardware address of UART
		.IRQn = UART2_IRQn,   										// The interrupt that is thrown when this UART recieves data
		.IrqPriority = MX_UART_INT_PRIORITY,  						// The Priority of the UART Interrupt, this is defined in FreeRTOSConfig.h
		._baud_rate = DASH_M_PORT_BAUDRATE,							// baudrate the port operates at

		// 3/19/2013 rmd : Do not change to ONE stop bit without visiting the rs 485 driver time.
		// That calculation assumes 2 stop bits are in use.
		.stopBits = UART_STOPBIT_2,

		.bUsesDMA = (true),   										// Used to set whether the port uses DMA or not, TODO: Re-Implement nonDMA Tx
		.DmaCfg_s.GPDMA_Conn = GPDMA_CONN_UART2_Tx,                 // The Device the DMA is communicating with TODO: Find out if this is redundant, can we remove this?
		.DmaCfg_s.GPDMA_Channel = UART2_DMA_CHAN,                   // GPDMA channel for transmit 0-9 Defined in x_uart.h

		.fifo_trigger_level = UART_FIFO_TRGLEV2,
		.fifo_trigger_count = 8,									// must set the trigger count to match the trigger level
		
		.rs485.manages_485_driver = (true),							// M1 does indeed have a 485 driver.
		.rs485.timer_ptr = LPC_TIM2, 								// The timer used to drive the RS-485 Driver Enable Line Low
		.rs485.timer_IRQn = TIMER2_IRQn,							// The interrupt assoicated with this Timer
		.rs485.timer_IRQ_priority = TIMER2_INT_PRIORITY,			// This UARTs interrupt priority level, from FreeRtosConfig.h
		.rs485.driver_enable_port = UART2_DRVR_ENA_PORT,   	// RS-485 Driver Enable Port
		.rs485.driver_enable_pin = UART2_DRVR_ENA_PIN			// RS-485 Driver Enable Pin
	}

	/*
	{   // UART3 - M2 communications (TTL I/O on JP3)
		.UARTx = (LPC_UART_TypeDef *)LPC_UART3,   				    // Pointer to hardware address of UART
		.IRQn = UART3_IRQn,   										// The interrupt that is thrown when this UART recieves data
		.IrqPriority = MX_UART_INT_PRIORITY,  						// The Priority of the UART Interrupt, this is defined in FreeRTOSConfig.h
		._baud_rate = M_PORTS_BAUDRATE,								// baudrate the port operates at
		// 3/19/2013 rmd : Do not change to ONE stop bit without visiting the rs 485 driver time.
		// That calculation assumes 2 stop bits are in use.
		.stopBits = UART_STOPBIT_2,

		.bUsesDMA = (true), 										// Used to set whether the port uses DMA or not, TODO: Re-Implement nonDMA Tx
		.DmaCfg_s.GPDMA_Conn = GPDMA_CONN_UART3_Tx,                 // The Device the DMA is communicating with TODO: Find out if this is redundant, can we remove this?
		.DmaCfg_s.GPDMA_Channel = UART3_DMA_CHAN,                   // GPDMA channel for transmit 0-9 Defined in x_uart.h

        .fifo_trigger_level = UART_FIFO_TRGLEV0,

		.rs485.manages_485_driver = (true),							// M1 does indeed have a 485 driver.
		.rs485.timer_ptr = LPC_TIM3, 								// The timer used to drive the RS-485 Driver Enable Line Low
		.rs485.timer_IRQn = TIMER3_IRQn,							// The interrupt assoicated with this Timer
		.rs485.timer_IRQ_priority = TIMER3_INT_PRIORITY,			// This UARTs interrupt priority level, from FreeRtosConfig.h
		.rs485.driver_enable_port = UART3_DRVR_ENA_PORT,   	// RS-485 Driver Enable Port
		.rs485.driver_enable_pin = UART3_DRVR_ENA_PIN			// RS-485 Driver Enable Pin
	},
	*/
	
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

volatile UART_CTL_s uart_control[ MANAGED_UART_PORTS ];

/* ---------------------------------------------------------- */

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/

// GPDMA interrupt handler sub-routine. This task fires when 16-17 bytes are left to be
// transmitted out of the uart, I assume this is when the DMA transfer completes on the
// chip. Because we need to set the Drive line low after a finished RS485 transmission we
// enable a preloaded timer, that will interrupt to drive the line low.
void GPDMA_IRQHandler( void )
{
	uint32_t	ch;
	
	signed portBASE_TYPE	xTaskWoken;

	// ----------

	xTaskWoken = 0;

	// ----------

	// Scan UART channels for DMA interrupt pending
	for( ch=0; ch<MANAGED_UART_PORTS; ch++ )
	{
		//Check to make sure we arent scanning an invalid channel
		if( uart_config[ ch ].bUsesDMA )
		{
			//Check to see if we have a valid interrupt on this channel that needs to be serviced
			if( GPDMA_IntGetStatus( GPDMA_STAT_INT, uart_config[ ch ].DmaCfg_s.GPDMA_Channel ) )
			{
				// Check counter terminal status (normal data)
				if( GPDMA_IntGetStatus( GPDMA_STAT_INTTC, uart_config[ ch ].DmaCfg_s.GPDMA_Channel ) )
				{
					// Clear terminate counter Interrupt pending
					GPDMA_ClearIntPending( GPDMA_STATCLR_INTTC, uart_config[ ch ].DmaCfg_s.GPDMA_Channel );
				}

				// Check error terminal status
				if( GPDMA_IntGetStatus( GPDMA_STAT_INTERR, uart_config[ ch ].DmaCfg_s.GPDMA_Channel ) )
				{
					// Clear error counter Interrupt pending.
					GPDMA_ClearIntPending( GPDMA_STATCLR_INTERR, uart_config[ ch ].DmaCfg_s.GPDMA_Channel );

					flag_error_from_isr( BIT_TPME_DMA_error_on_uarts, &xTaskWoken );
				}

				//Disable the DMA Interrupt, it will be reenabled in the task when we have DMA data to send
				GPDMA_ChannelCmd( uart_config[ ch ].DmaCfg_s.GPDMA_Channel, DISABLE );
				
				// ----------
				
				if( uart_config[ ch ].rs485.manages_485_driver == (false) )
				{
					// 3/18/2013 rmd : If we are not managing a 485 driver enable then signal the task this
					// block is done! If we are the timer interrupt will signal the task when it fires.
					xSemaphoreGiveFromISR( uart_control[ ch ].xPacketSentSema, &xTaskWoken );
				}

			}

		}

	}

	portEND_SWITCHING_ISR( xTaskWoken );
}

/* ---------------------------------------------------------- */
void TIMER2_IRQHandler( void )
{
	// 1/30/2014 rmd : For the RS-485 transmission enable control for port M1 on UART_2.
	
	signed portBASE_TYPE	xTaskWoken;

	xTaskWoken = 0;

	// ----------

	// 1/18/2013 rmd : Clear the timer interrupt. The timer is configured so that this is the
	// only interrupt possible. And on match the timer has stopped. So this is all that is
	// needed as far as the timer hardware is concerned.
	T2IR_MR0_BITBAND = 1;
	
	// ----------

	if( uart_control[ UPORT_M1 ].rs485_driver_to_be_disabled )
	{
		// 3/18/2013 rmd : Turn the RS 485 driver off. Notice I'm using the constant configuration
		// array. Not the SRAM control array. Did so because more clear to me.
		GPIO_ClearValue( uart_config[ UPORT_M1 ].rs485.driver_enable_port, (1 << uart_config[ UPORT_M1 ].rs485.driver_enable_pin) );
	}

	// ----------

	// Signal UART task to let it know we've sent the data. Be careful of the stupid indexing
	// scheme.
	xSemaphoreGiveFromISR( uart_control[ UPORT_M1 ].xPacketSentSema, &xTaskWoken );

	// ----------

	portEND_SWITCHING_ISR( xTaskWoken );
}

/* ---------------------------------------------------------- */
/*
void TIMER3_IRQHandler( void )
{
	signed portBASE_TYPE	xTaskWoken;

	xTaskWoken = 0;

	// ----------

	// 1/18/2013 rmd : Clear the timer interrupt. The timer is configured so that this is the
	// only interrupt possible. And on match the timer has stopped. So this is all that is
	// needed as far as the timer hardware is concerned.
	T3IR_MR0_BITBAND = 1;
	
	// ----------

	if( uart_control[ 2 ].rs485_driver_to_be_disabled )
	{
		// 3/18/2013 rmd : Turn the RS 485 driver off. Notice I'm using the constant configuration
		// array. Not the SRAM control array. Did so because more clear to me.
		GPIO_ClearValue( uart_config[ 3 ].rs485.driver_enable_port, (1 << uart_config[ 3 ].rs485.driver_enable_pin) );
	}

	// ----------

	// Signal UART task to let it know we've sent the data. Becareful of the stupid indexing
	// scheme.
	xSemaphoreGiveFromISR( uart_control[ 2 ].xPacketSentSema, &xTaskWoken );

	// ----------

	portEND_SWITCHING_ISR( xTaskWoken );
}
*/

//-----------------------------------------------------------------------------
//  COMMON UART INTERRUPT HANDLER FOR Mx UART PORTS (Main-to-TP Micro, M1, M2)
//-----------------------------------------------------------------------------
static void CommonUartISR( UNS_32 pport, signed portBASE_TYPE *xTaskWoken )
{
	uint32_t			interruptType, interruptFlags;

	UNS_16				tx_cnt;

	UNS_16				rx_cnt;

	UNS_8				c;

	//get the status of the UIIR register and mask out the bytes that arent related to type of
	//received interrupt
	interruptType = UART_GetIntId( uart_config[ pport ].UARTx ) & UART_IIR_INTID_MASK;

	//-----------------------------------------------------------------------------
	//    Highest priority : Receive Line Status
	//-----------------------------------------------------------------------------
	if (interruptType == UART_IIR_INTID_RLS )
	{
		// Check line status
		interruptFlags = UART_GetLineStatus( uart_config[ pport ].UARTx );

		// Mask out the Receive Ready and Transmit Holding empty status
		interruptFlags &= ( UART_LSR_OE | UART_LSR_PE | UART_LSR_FE | UART_LSR_BI | UART_LSR_RXFE );

		//-----------------------------------------------------------------------------
		//    tmp1 now holds these Rx error bits:
		//
		//    OE = Rx overrun error, PE = parity error, FE = framing error,
		//    BI = Break interrupt, RXFE = FIFO contains at least one UARTx
		//    RX error.
		//-----------------------------------------------------------------------------
		if (interruptFlags == (TRUE))
		{
			// 4/8/2013 rmd : Flag the error.
			flag_error_from_isr( BIT_TPME_serial_transmission_had_serial_errors, xTaskWoken );
		}
	}

	//-----------------------------------------------------------------------------
	//    Next Highest Priority: Receive Data Available
	//-----------------------------------------------------------------------------
	if (interruptType == UART_IIR_INTID_RDA )
	{
		// 1/31/2014 rmd : Rx FIFO has reached its programmed threshold, so read the programmed
		// trigger level number of bytes into the ring buffer
		for( rx_cnt = 0; rx_cnt < uart_config[ pport ].fifo_trigger_count; rx_cnt++ )
		{
			// Read a byte from the Rx FIFO
			c = UART_ReceiveByte( uart_config[ pport ].UARTx );

			// Store the received byte in this UART's ring buffer
			RingBufferPut_ISR( &(uart_control[ pport ].ringbuffer_s), c );
		}

		// Give the Rx data available semaphore
		xSemaphoreGiveFromISR( uart_control[ pport ].xRxDataAvailSema, xTaskWoken );
	}
	//-----------------------------------------------------------------------------
	//    Or it's an RX character time-out. There are one or more characters
	//    in the Rx FIFO. We poll the FIFO for additional characters.
	//-----------------------------------------------------------------------------
	else
	if (interruptType == UART_IIR_INTID_CTI )
	{
		// 2012.02.28 rmd : We're reading the status register bit 0 which tells us if there is still
		// a byte in the RBR.
		while( (UART_GetLineStatus( uart_config[ pport ].UARTx ) & (UART_LSR_RDR) ) == (UART_LSR_RDR) )
		{
			// Read a byte from the Rx FIFO
			c = UART_ReceiveByte( uart_config[ pport ].UARTx );

			// Store the received byte in this UART's ring buffer
			RingBufferPut_ISR( &(uart_control[ pport ].ringbuffer_s), c );
		}

		// Give the Rx data available semaphore
		xSemaphoreGiveFromISR( uart_control[ pport ].xRxDataAvailSema, xTaskWoken );
	}

	//-----------------------------------------------------------------------------
	//    Lowest Priority: Transmit Holding Register Empty
	//-----------------------------------------------------------------------------
	if( interruptType == UART_IIR_INTID_THRE )
	{
		////////////////////////
		// transmit function   (Only non-DMA mode)
		////////////////////////

		// Check first that there is at least one byte to send
		if( uart_control[ pport ].TxByteCnt != 0 )
		{
			// Calculate how many bytes to put into Tx FIFO
			tx_cnt = uart_control[ pport ].TxByteCnt >= UART_FIFO_DEPTH ? UART_FIFO_DEPTH : uart_control[ pport ].TxByteCnt;

			// Load up the Tx FIFO
			while( tx_cnt-- )
			{
				// Fetch data from tx block and load next byte into Tx FIFO, bump pointer into data buffer
				UART_SendByte( uart_config[ pport ].UARTx, *uart_control[ pport ].pTxData++ );

				// Decrement bytes remaining to send
				uart_control[ pport ].TxByteCnt--;
			}
		}

		// Check if all data from this buffer has been sent
		if( 0 == uart_control[ pport ].TxByteCnt )
		{
			// All data has been sent so disable the THRE interrupt
			UART_IntConfig( uart_config[ pport ].UARTx, UART_INTCFG_THRE, DISABLE );

			// Signal UART task to let it know we've sent the data
			xSemaphoreGiveFromISR( uart_control[ pport ].xPacketSentSema, xTaskWoken );
		}
	}


}

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/

//-----------------------------------------------------------------------------
//  Interrupt handler for UART0 IRQ
//-----------------------------------------------------------------------------
void UART0_IRQHandler(void)
{
	signed portBASE_TYPE	xTaskWoken;
	
	// ----------
	
	xTaskWoken = 0;
	
	// ----------
	
	// 1/30/2014 rmd : Call common Mx UART interrupt handler, passing it the index into its
	// config and control structures.
	CommonUartISR( UPORT_MAIN, &xTaskWoken );
	
	// ----------
	
	portEND_SWITCHING_ISR( xTaskWoken );
}

//-----------------------------------------------------------------------------
//  Interrupt handler for UART2 IRQ
//-----------------------------------------------------------------------------
void UART2_IRQHandler(void)
{
	signed portBASE_TYPE	xTaskWoken;
	
	// ----------
	
	xTaskWoken = 0;
	
	// ----------
	
	// 1/30/2014 rmd : Call common Mx UART interrupt handler, passing it the index into its
	// config and control structures.
	CommonUartISR( UPORT_M1, &xTaskWoken );
	
	// ----------
	
	portEND_SWITCHING_ISR( xTaskWoken );
}

//-----------------------------------------------------------------------------
//  Interrupt handler for UART3 IRQ
//-----------------------------------------------------------------------------
/*
void UART3_IRQHandler( void )
{
	signed portBASE_TYPE	xTaskWoken;
	
	// ----------
	
	xTaskWoken = 0;
	
	// ----------
	
	// 3/19/2013 rmd : Note the error prone indexing scheme. Which should be rendered out.
	CommonUartISR( &uart_control[ 2 ], &xTaskWoken );
	
	// ----------
	
	portEND_SWITCHING_ISR( xTaskWoken );
}
*/

//-----------------------------------------------------------------------------
//    UART initialization function
//-----------------------------------------------------------------------------
extern void UART_MxInit( UNS_32 uart_number )
{
	// 1/30/2014 rmd : The parameter is either 0 or 1 in this application. And represents the
	// index into the config and control structures.

	UART_CFG_Type		UARTConfigStruct;		// CMSIS UART configuration Struct variable

	UART_FIFO_CFG_Type	UARTFIFOConfigStruct;	// CMSIS UART FIFO configuration Struct variable

	volatile UART_CTL_s		*pUartCtl;			// Ptr to this UART's control structure

	const UART_CFG_s		*pUartCfg;			// Ptr to this UART's config structure

	// ----------
	
	// Point to this UART's control structure
	pUartCtl = &uart_control[ uart_number ];

	// Then fill in the port-specific configuration info
	pUartCfg = &uart_config[ uart_number ];

	// ----------
	
	// Set the UARTx Peripheral clock to PCLK/4
	switch( uart_number )
	{
		case 0:   CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_UART0, CLKPWR_PCLKSEL_CCLK_DIV_4 );  break;
		case 1:   CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_UART1, CLKPWR_PCLKSEL_CCLK_DIV_4 );  break;
		case 2:   CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_UART2, CLKPWR_PCLKSEL_CCLK_DIV_4 );  break;
		case 3:   CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_UART3, CLKPWR_PCLKSEL_CCLK_DIV_4 );  break;
		default:  break;
	}

	// ----------
	
	// Disable interrupts
	portENTER_CRITICAL();

	// ----------
	
	UARTConfigStruct.Baud_rate = pUartCfg->_baud_rate;

	UARTConfigStruct.Stopbits =	 pUartCfg->stopBits;

	UARTConfigStruct.Databits = UART_DATABIT_8;

	UARTConfigStruct.Parity = UART_PARITY_NONE;

	// Now initialize the UART using values in the CMSIS UARTConfigStruct
	UART_Init( pUartCfg->UARTx, &UARTConfigStruct );

	// ----------
	
	/* Initialize CMSIS FIFOConfigStruct to default state:
	* 				- FIFO_DMAMode    = DISABLE
	* 				- FIFO_Level      = UART_FIFO_TRGLEV0
	* 				- FIFO_ResetRxBuf = ENABLE
	* 				- FIFO_ResetTxBuf = ENABLE
	* 				- FIFO_State      = ENABLE
	*/
	UART_FIFOConfigStructInit( &UARTFIFOConfigStruct );

	// Set UART trigger level to allow 8 chars
	UARTFIFOConfigStruct.FIFO_Level = pUartCfg->fifo_trigger_level;

	// Enable DMA mode in UART
	UARTFIFOConfigStruct.FIFO_DMAMode = ENABLE;

	// Initialize the FIFO for UART peripheral
	UART_FIFOConfig( pUartCfg->UARTx, &UARTFIFOConfigStruct );

	// ----------
	
	// Clear/initialize the receive data ring buffer to empty
	initRingBuffer( &pUartCtl->ringbuffer_s );

	// Enable UART Transmit
	UART_TxCmd( pUartCfg->UARTx, ENABLE );

	// Enable RBR interrupt in UART
	UART_IntConfig( pUartCfg->UARTx, UART_INTCFG_RBR, ENABLE );

	// Set NVIC priority for this UART interrupt
	NVIC_SetPriority( pUartCfg->IRQn, pUartCfg->IrqPriority );

	// Enable NVIC Interrupt for this UART channel
	NVIC_EnableIRQ( pUartCfg->IRQn );

	if( !g_bGPDMA_Initialized )
	{
		////////////////////////////////////////////////////
		// One-time GPDMA Interrupt configuration section
		////////////////////////////////////////////////////

		// Initialize GPDMA controller
		GPDMA_Init();

		// Set up GPDMA interrupt
		// Disable interrupt for DMA
		NVIC_DisableIRQ( DMA_IRQn );

		// preemption = 1, sub-priority = 1
		NVIC_SetPriority( DMA_IRQn, DMA_INT_PRIORITY );

		// Set flag so we don't initialize the GPDMA again
		g_bGPDMA_Initialized = TRUE;
	}

	// ----------
	
	// Enable USART transmitter
	UART_TxCmd( pUartCfg->UARTx, ENABLE );

	// Enable the Receive Line Status interrupt in the UART
	UART_IntConfig( pUartCfg->UARTx, UART_INTCFG_RLS, ENABLE );

	// ----------
	
	// Enable interrupts again
	portEXIT_CRITICAL();

	// ----------
	
	// 3/18/2013 rmd : If has 485 duties intialize the timer. End result is a timer, not
	// running, but all ready to load match register MR0 and start.
	if( pUartCfg->rs485.manages_485_driver )
	{
		if( pUartCfg->rs485.timer_ptr == LPC_TIM2 )
		{
			// 11/16/2012 rmd : Enable power to peripheral.
			CLKPWR_ConfigPPWR( CLKPWR_PCONP_PCTIM2 , ENABLE );
			
			// 11/16/2012 rmd : CCLOCK is 72MHz. So dividing by 4 gives us an 18MHz counter clock.
			CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_TIMER2, CLKPWR_PCLKSEL_CCLK_DIV_4 );
		}
		else
		if( pUartCfg->rs485.timer_ptr == LPC_TIM3 )
		{
			// 11/16/2012 rmd : Enable power to peripheral.
			CLKPWR_ConfigPPWR( CLKPWR_PCONP_PCTIM3 , ENABLE );
			
			// 11/16/2012 rmd : CCLOCK is 72MHz. So dividing by 4 gives us an 18MHz counter clock.
			CLKPWR_SetPCLKDiv( CLKPWR_PCLKSEL_TIMER3, CLKPWR_PCLKSEL_CCLK_DIV_4 );
		}
		else
		{
			DEBUG_STOP();
		}
		
		// ----------

		// 3/18/2013 rmd : For ease on the eyes.
		LPC_TIM_TypeDef		*ltimer;
		
		ltimer = pUartCfg->rs485.timer_ptr;
		
		// ----------

		// 3/18/2013 rmd : While reprogramming disable NVIC interrupt for this device.
		NVIC_DisableIRQ( pUartCfg->rs485.timer_IRQn );

		// 11/16/2012 rmd : Timer is disabled from counting.
		ltimer->TIMER_CONTROL_REG = 0;
	
		// 11/16/2012 rmd : Set such that TC is incremented when the prescale counter matches the
		// prescale register. And the prescale counter counts on each PCLK rising edge.
		ltimer->COUNT_CONTROL_REG = 0x00000000;
		
		// 1/17/2013 rmd : These are the 2 registers that count.
		ltimer->TC = 0;
		ltimer->PRESCALE_COUNTER = 0;
	
		// 11/16/2012 rmd : With a match value of 0 the TC counts at the FULL CCLK rate. The call we
		// are using is supposed to set up so that each TC tick is ONE usec. At 18MHz should come up
		// with a match value of 18.
		ltimer->PRESCALE_MATCH_VALUE = (converUSecToVal( converPtrToTimeNum(pUartCfg->rs485.timer_ptr), 1 ) - 1);
	
		// 11/16/2012 rmd : Set to interrupt when the TC matches MR0. And stop.
		ltimer->MATCH_CONTROL_REG = 5;
	
		ltimer->MR0 = 20;		// for this setup 20 is 20 usec
		ltimer->MR1 = 0;
		ltimer->MR2 = 0;
		ltimer->MR3 = 0;
		
	
		ltimer->CAPTURE_CONTROL_REG = 0;
	
		ltimer->EXTERNAL_MATCH_REG = 0;
	
		// 11/16/2012 rmd : Clear all the interrupts for throughness.
		ltimer->INTERRUPT_REG = 0x0000003F;
		
		// 1/17/2013 rmd : Clear any pending in the NVIC just incase.
		NVIC_ClearPendingIRQ( pUartCfg->rs485.timer_IRQn );
	
		NVIC_SetPriority( pUartCfg->rs485.timer_IRQn, pUartCfg->rs485.timer_IRQ_priority );
		
		NVIC_EnableIRQ( pUartCfg->rs485.timer_IRQn );
	
		// 3/18/2013 rmd : All done. Note the timer is ready to go but NOT running yet.
	}
}

/* ---------------------------------------------------------- */
extern void vSerialTask( void *pvParameters )
{
	UNS_32	*task_watchdog_time_stamp;
	
	GPDMA_Channel_CFG_Type	GPDMACfg;

	volatile UART_CTL_s		*pUartCtl;			// Ptr to this UART's control structure

	const UART_CFG_s		*pUartCfg;			// Ptr to this UART's config structure

	// ----------

	task_watchdog_time_stamp = ((TASK_TABLE_STRUCT*)pvParameters)->last_execution_time_stamp;
	
	// 7/31/2015 rmd : Pull the 0 or 1 index from the task table as the index into the control
	// structures.

	// Point to this UART's control structure
	pUartCtl = &uart_control[ (UNS_32)(((TASK_TABLE_STRUCT*)pvParameters)->task_info) ];

	// Then fill in the port-specific configuration info
	pUartCfg = &uart_config[ (UNS_32)(((TASK_TABLE_STRUCT*)pvParameters)->task_info) ];

	// Perform initialization of UART and driver OS structures and RS485 Driver line timer.
	UART_MxInit( (UNS_32)(((TASK_TABLE_STRUCT*)pvParameters)->task_info) );

	// ----------
	
	while( (true) )
	{
		// 7/31/2015 rmd : Task is moving. Grab latest time stamp for task watchdog scheme.
		*task_watchdog_time_stamp = xTaskGetTickCount();

		// ----------
		
		//-----------------------------------------------------------------------------
		//  Check the Tx DATA_HANDLE queue for something to send
		//-----------------------------------------------------------------------------
		if( xQueueReceive( pUartCtl->xTxDataHandleQueue, (void*)&pUartCtl->now_xmitting,  MS_to_TICKS( 1000 ) ) )
		{
			// 3/19/2013 rmd : Make sure semaphore is not available. Certainly on startup after it is
			// created it is available by default. And to ensure we know its state we can do this
			// always. It can't hurt. Notice we use NO_DELAY of course. The reason this is so important
			// and doing this came to mind is to ensure the RS485 driver envelope around the
			// transmission works properly. I wanted to make sure the semaphore somehow wasn't
			// available.
			xSemaphoreTake( pUartCtl->xPacketSentSema, portNO_DELAY );

			// ----------
			
			// 3/18/2013 rmd : If responsible for the RS-485 driver control turn on the driver now.
			if( pUartCfg->rs485.manages_485_driver )
			{
				GPIO_SetValue( pUartCfg->rs485.driver_enable_port, (1 << pUartCfg->rs485.driver_enable_pin ) );

				//----------

				// 3/19/2013 rmd : Set the timer up for 1ms of line drive time to charge the line. I don't
				// know for a fact this is needed but figure it to be good practice to drive the line for a
				// while before the start bit of the first byte. Without this pre-time the driver is on for
				// as little as 10usec before the first start bit. And that don't seem like long on what
				// could be an 8000 foot piece of cable! The time bounces around by 10's of usecs depedning
				// on the DMA start timing.
				pUartCtl->rs485_driver_to_be_disabled = (false);
				
				pUartCfg->rs485.timer_ptr->MR0 = 1000;
				
				// 1/17/2013 rmd : Zero the two counting registers.
				pUartCfg->rs485.timer_ptr->TC = 0;
				pUartCfg->rs485.timer_ptr->PRESCALE_COUNTER = 0;
				
				// 1/17/2013 rmd : The rest of the timer is setup. All that's left to do is enable it.
				pUartCfg->rs485.timer_ptr->TIMER_CONTROL_REG = 1;

				// 3/19/2013 rmd : Wait for the timer to complete.
				xSemaphoreTake( pUartCtl->xPacketSentSema, portMAX_DELAY );

				//----------

				// 3/18/2013 rmd : Perform integer math. Using the number of bytes we have to send time 11
				// bit per byte and the baud rate figure out how many usec the driver must stay enabled.
				// Remember the timer match register counts by micro seconds.
				//
				// 3/19/2013 rmd : And because of DMA jitter in when it has completed we add 1ms more driver
				// time. Which is of course 1000 usec. Have seen it bounce around as much as 300usec.
				pUartCtl->rs485_driver_to_be_disabled = (true);

				// 3/19/2013 rmd : Watch out for math overflow errors. Use long long variables to avoid. The
				// math uses 11 bits per byte which is accurate for the case of two stop bits and 8 data
				// bits.
				UNS_64	useconds;

				useconds = ( ( ((UNS_64)(pUartCtl->now_xmitting.dh.dlen)) * 11LL * 1000000LL ) / ((UNS_64)pUartCfg->_baud_rate) ) + 1000LL;
				
				pUartCfg->rs485.timer_ptr->MR0 = useconds;
				
				// 1/17/2013 rmd : Zero the two counting registers.
				pUartCfg->rs485.timer_ptr->TC = 0;
				pUartCfg->rs485.timer_ptr->PRESCALE_COUNTER = 0;
				
				// 1/17/2013 rmd : The rest of the timer is setup. All that's left to do is enable it.
				pUartCfg->rs485.timer_ptr->TIMER_CONTROL_REG = 1;
			}

			// ----------
			
			// 3/18/2013 rmd : All the serial ports managed by this driver use DMA driven
			// transmission.

			// Setup GPDMA channel
			GPDMACfg.ChannelNum = pUartCfg->DmaCfg_s.GPDMA_Channel;

			// pointer to the source data in memory (from the transmit block DATA_HANDLE_s)
			GPDMACfg.SrcMemAddr = (uint32_t) pUartCtl->now_xmitting.dh.dptr;

			// Destination memory - don't care
			GPDMACfg.DstMemAddr = 0;

			// DMA Transfer size (from the DATA_HANDLE_s)
			GPDMACfg.TransferSize =  pUartCtl->now_xmitting.dh.dlen;

			// Transfer width - don't care
			GPDMACfg.TransferWidth = 0;

			// Transfer type is memory to peripheral (uart tx)
			GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_M2P;

			// Source connection - don't care
			GPDMACfg.SrcConn = 0;

			// Destination connection (GPDMA_CONN_UARTn_Tx)
			GPDMACfg.DstConn = pUartCfg->DmaCfg_s.GPDMA_Conn;

			// Linker List Item - unused
			GPDMACfg.DMALLI = 0;

			// Setup channel with given parameters
			GPDMA_Setup( &GPDMACfg );

			// Enable interrupt for DMA
			NVIC_EnableIRQ( DMA_IRQn );

			// Enable GPDMA for the channel in defined in the struct
			GPDMA_ChannelCmd( pUartCfg->DmaCfg_s.GPDMA_Channel, ENABLE );

			// ----------

			// Packet now being sent. Wait for transmit complete. The DMA isr or the TIMER isr GIVES the
			// semaphore.
			xSemaphoreTake( pUartCtl->xPacketSentSema, portMAX_DELAY );

			// ----------

			// 3/13/2013 rmd : Free the memory block. If this is the last port (task) that is using this
			// block the free will be succesful. And we should signal the main board that we can accept
			// another block if needed. This is our software handshaking mechanism to 'pace' the packet
			// flow into the TP Micro. Since we are sending packets out the slower M1 and M2 ports, and
			// the main baord is firing data to us at 115200 baud, our resources here would be required
			// to buffer the packets till they have made their way out M1 and M2. Because of limited
			// memory we cannot do that. So the main board knows it must wait to hear from us that we
			// have finished with the previously sent packet. This is that signal.
			if( FreeMemBlk( pUartCtl->now_xmitting.dh.dptr ) )
			{
				if( pUartCtl->now_xmitting.give_main_goahead )
				{
					// 3/13/2013 rmd : Okay so the memory was actually freed. Tell the main board about this.
					construct_and_route_software_handshake_message_to_main_board();
				}
			}
			
			// ----------

			// 3/19/2013 rmd : Make sure the transmitter is fully empty before potentially starting the
			// next block. For the case of the MAIN port, to prevent reprogramming the DMA controller
			// before it is completely done, we must do this. For the case of M1 and M2 ports there is a
			// 485 driver timer that envelopes the transmission and set so that when timer ISR fires the
			// transmission is complete. However for M1 and M2 running through this code won't hurt.
			// When the bit is set the transmission has completed.
			while( !(pUartCfg->UARTx->LSR & UART_LSR_TEMT) )
			{
				vTaskDelay( 1 );
			}

		}  // of event rcvd  

	}  // of while forever

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

