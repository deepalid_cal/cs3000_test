//-----------------------------------------------------------------------------
//  uart.h - header file for LPC176X UART MX driver development
//-----------------------------------------------------------------------------
/* ---------------------------------------------------------- */

#ifndef INC_UART_H_
#define INC_UART_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// This sets the number of UART Ports supported by the LPC176x MX UART driver
#define MX_UART_PORTS               3

// This is used for the UART configuration array
#define TOTAL_UART_PORTS            4

// Symbolic port names and their physical UART ports
#define UPORT_MAIN                  0
#define UPORT_TWO_WIRE              1
#define UPORT_M1                    2
#define UPORT_M2                    3

// Indices of UART Control blocks by UART #
#define UART0_CTL_BLK_IDX           0
#define UART2_CTL_BLK_IDX           1
#define UART3_CTL_BLK_IDX           2

// This can be changed at runtime
#define MX_UART_DEFAULT_BAUDRATE  	19200

// Number of DATA_HANDLE_s entries for each UART Tx queue
#define MX_TX_QUEUE_DATA_HANDLES  	4       

//	Driver enable flag polling period when disabled (ms)
#define UART_ENA_DIS_POLL_MS		100

// 	Tx DATA_HANDLE queue read time-out (ms)
#define UART_TX_QUEUE_TIME_OUT_MS	100

// Size of UART Tx and Rx FIFOs
#define UART_FIFO_DEPTH             16

// This corresponds to RX Trigger level 2
#define UART_RX_FIFO_INT_THRESHOLD  8

//-----------------------------------------------------------------------------
//    Configuration of RS-485 Driver enable pins
//-----------------------------------------------------------------------------

// If the UART port does NOT have a driver enable, then set its
// UARTx_DRVR_ENA_PORT value to UART_DRVR_ENA_PORT_NONE
#define UART_DRVR_ENA_PORT_NONE     5

// UART0 DRIVER ENABLE: (None)
#define UART0_DRVR_ENA_PORT         UART_DRVR_ENA_PORT_NONE
#define UART0_DRVR_ENA_PIN          0

// UART1 DRIVER ENABLE: (None)
#define UART1_DRVR_ENA_PORT         UART_DRVR_ENA_PORT_NONE
#define UART1_DRVR_ENA_PIN          0

// UART2 DRIVER ENABLE: P1.24 goes to header J12_C on LPC1768 board
#define UART2_DRVR_ENA_PORT         1      
#define UART2_DRVR_ENA_PIN          24

// UART3 DRIVER ENABLE: P1.25 goes to header J12_C on LPC1768 board
#define UART3_DRVR_ENA_PORT         1        // P1.25 goes to header J12_c on LPC1768 board
#define UART3_DRVR_ENA_PIN          25

typedef struct
{
  LPC_UART_TypeDef *UARTx;       // Pointer to UART register struct
  IRQn_Type       IRQn;          // LPC interrupt #
  UNS_32             IrqPriority;   // NVIC interrupt priority (unshifted)
  UNS_32             Baudrate;      // Default port baudrate
  UNS_8              DrvrEnaPort;   // RS-485 port pin for driver enable (if applic.)
  UNS_8              DrvrEnaPin;    // RS-485 port pin for driver enable (0 if not used)   
} UART_CFG_s;

//-----------------------------------------------------------------------------
//    This is the configuration structure for the Main, M1, M2 serial ports
//-----------------------------------------------------------------------------
typedef struct
{
  UART_CFG_s        UartCfg_s;          // UART configuration structure
  UART_RINGBUFFER_s ringbuffer_s;       // UART ring buffer structure
  UNS_32               Baudrate;           // UART baudrate (bps)
  _Bool           bEnaCmd;            // Commanded state
  _Bool           bEnaState;          // Actual state
  UNS_8                *pTxData;           // Pointer to next byte to transmit
  UNS_16               TxByteCnt;          // Number of bytes left to transmit
  DATA_HANDLE_s     DataHandle_s;       // Active Data Handle structure (transmit)
  xSemaphoreHandle  xPacketSentSema;    // Handle of Tx Packet Sent semaphore
  xSemaphoreHandle  xRxDataAvailSema;   // Handle of Rx characters received semaphore
  xQueueHandle      xTxDataHandleQueue; // Handle of queue of DATA_HANDLE structures

} UART_CTL_s;

// Global Control structures for the LPC176X UART ports
extern volatile UART_CTL_s g_UartCtl_s[ MX_UART_PORTS ];

// Global function declarations
extern const UART_CFG_s *getUartCfg( UNS_32 uart );
extern volatile UART_CTL_s *getUartCtl( UNS_32 uart );
extern _Bool UartState( UNS_32 uart );
extern void UartEnable( UNS_32 uart, UNS_32 baudrate);
extern void UartDisable( UNS_32 uart );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*UART_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
