//-----------------------------------------------------------------------------
//  uart.h - header file for LPC176X UART MX driver development
//-----------------------------------------------------------------------------
/* ---------------------------------------------------------- */

#ifndef INC_XUART_H_
#define INC_XUART_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"

#include	"ringbuffer.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// This is used for the UART configuration and control arrays. Only needs to be big enough
// to support the actual number of UART peripherals that are managed by the serial driver.
// And in this application that is only TWO. The MAIN port and M1. The two-wire uart is
// independently managed. And channel M2 has been rendered obsolete.
#define MANAGED_UART_PORTS	2

// 1/30/2014 rmd : Symbolic port names and their index into the config and control
// arrays.
#define UPORT_MAIN			0

#define UPORT_M1			1

// --------

// 3/14/2013 rmd : We performed successful in house testing up to 8000 feet at 38400 baud.
// So I decided to drop to 19200 and hopefully make our line length spec 10,000 feet.
//
// 3/5/2014 rmd : I've reconsidered. Since the spec everyone has in their head is 5000 feet
// I feel comfortable using a 28800 baud rate. I fully believe that will work to 8000 feet
// in by far most all installations. And I'm going for a bit of performance here to support
// code download and even just the routine transmissions.
#define DASH_M_PORT_BAUDRATE		(28800)

// --------

// 11/15/2012 rmd : For each additional queued block we require the size of the DATA_HANDLE
// plus maybe some queue itself overhead. So not too much. The real implication is that the
// actual message memory for what is going to be transmitted (from the memory pool) is
// needed.

// 11/15/2012 rmd : For the main port we send a diagnostic message. One block per displayed
// line. As we can display 16 lines on the CS3000 display provide 16 blocks plus 2 for other
// messages that may get attempted.
#define UPORT_MAIN_MAX_QUEUED_XMIT_BLOCKS	(16)

// 11/15/2012 rmd : As blocks come in for transfer we queue them. If the incoming baud rate
// is higher than the 19.2K chain baud rate we need a buffering action. And this count
// provides it. NOTE : we've added a software flow control handshake between the main board
// and the tp micro card.
#define UPORT_M1_MAX_QUEUED_XMIT_BLOCKS		(6)

// --------

// Size of UART Tx and Rx FIFOs
#define UART_FIFO_DEPTH             (16)

//GPDMA Channels
#define UART0_DMA_CHAN				0
#define UART2_DMA_CHAN				1
//#define UART3_DMA_CHAN				2

//-----------------------------------------------------------------------------
//    Configuration of RS-485 Driver enable pins
//-----------------------------------------------------------------------------

// UART2 DRIVER ENABLE: P1.24 goes to header J12_C on LPC1768 board
#define UART2_DRVR_ENA_PORT         2
#define UART2_DRVR_ENA_PIN          7

// UART3 DRIVER ENABLE: P1.25 goes to header J12_C on LPC1768 board
//#define UART3_DRVR_ENA_PORT         0        // P1.25 goes to header J12_c on LPC1768 board
//#define UART3_DRVR_ENA_PIN          10


typedef struct
{
	BOOL_32					manages_485_driver;		// are we responsible or not?
	
	LPC_TIM_TypeDef			*timer_ptr;				// Pointer to the timer this struct uses to interrupt
	
	IRQn_Type				timer_IRQn;				// LPC interrupt #

	UNS_32					timer_IRQ_priority;		// The priority of this channels interrupt

	UNS_32					driver_enable_port;		// RS-485 port pin for driver enable (if applic.)

	UNS_32					driver_enable_pin;		// RS-485 port pin for driver enable (0 if not used)

} RS485_CONTROL_STRUCT;


typedef struct
{
	UNS_32			GPDMA_Conn;			// GPDMA connection number

	UNS_8			GPDMA_Channel;		// GPDMA channel for this UART

} DMA_CFG_s;
	

typedef struct
{
	LPC_UART_TypeDef		*UARTx;					// Pointer to UART register struct

	IRQn_Type				IRQn;					// LPC interrupt #

	UNS_32					IrqPriority;			// NVIC interrupt priority (unshifted)

	UNS_32					_baud_rate;				// Default port baudrate

	UART_STOPBIT_Type		stopBits;				// 1 or 2 stop bits.

	BOOL_32					bUsesDMA;				// Sets whether DMA is used on this connection

	DMA_CFG_s				DmaCfg_s;				// Contains the relevant DMA settings for this port

	UART_FITO_LEVEL_Type	fifo_trigger_level;

	UNS_32					fifo_trigger_count;

	RS485_CONTROL_STRUCT	rs485;					// RS-485 timer for driver enable line
	
} UART_CFG_s;
	


typedef struct
{

	DATA_HANDLE		dh;
	
	// 3/14/2013 rmd : When some packets complete their transmission a flow control packet is
	// sent to the main board. Giving the main board serial driver permission to send another
	// packet to the TP Micro. This is the flow control mechanism to manage resources within the
	// TP Micro.
	BOOL_32			give_main_goahead;

} XMIT_QUEUE_ITEM_STRUCT;



	//-----------------------------------------------------------------------------
	//    This is the configuration structure for the Main, M1, M2 serial ports
	//-----------------------------------------------------------------------------
typedef struct
{
	UART_RINGBUFFER_s		ringbuffer_s;			// UART ring buffer structure

	UNS_8					*pTxData;				// Pointer to next byte to transmit

	UNS_16					TxByteCnt;				// Number of bytes left to transmit

	UNS_32					TxLastSendTime;			// Amount of time it will take to transmit the data after DMA transfer complete interrupt

	XMIT_QUEUE_ITEM_STRUCT	now_xmitting;			// Active Data Handle structure (transmit)

	xSemaphoreHandle		xPacketSentSema;		// Handle of Tx Packet Sent semaphore

	xSemaphoreHandle		xRxDataAvailSema;		// Handle of Rx characters received semaphore

	xQueueHandle			xTxDataHandleQueue;		// Handle of queue of DATA_HANDLE structures
	
	// 3/19/2013 rmd : When we are pre-charging the cable for 1ms we use the timer. But in this
	// case we don't want the turn off the cable when the timer isr fires. When data transission
	// has completed and the timer isr fires we do want to turn the driver off. This variable
	// manages the behaviour.
	BOOL_32					rs485_driver_to_be_disabled;

} UART_CTL_s;
	
extern volatile UART_CTL_s uart_control[ MANAGED_UART_PORTS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void vSerialTask( void *pvParameters );
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

