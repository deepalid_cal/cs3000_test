/////////////////////////////////////////////
//
//  File: SpiDrvr.c
//
//  Description: - SPI driver for TP Micro
//
//  $Id: SpiDrvr.c,v 1.9 2011/12/29 20:52:12 dsquires Exp $
//
//  $Log: SpiDrvr.c,v $
//  Revision 1.9  2011/12/29 20:52:12  dsquires
//  Put status checking in data read and data write functions.
//
//  Revision 1.8  2011/12/28 22:33:45  dsquires
//  Reverted to wrappers for spi functions.
//
/////////////////////////////////////////////

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"spidrvr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// SSP Configuration structure variable (private)
static SSP_CFG_Type SSP_ConfigStruct;

//-----------------------------------------------------------------------------
//      void SpiSlaveSelEna( BOOL_32 bEnable )
//
//      This function controls the active low SSEL0 (slave select) signal
//      used to select the serial EEPROM chip for SPI operations.
//
//      bEnable == TRUE : 
//-----------------------------------------------------------------------------
void SpiSlaveSelEna( BOOL_32 bEnable )
{
    if( bEnable )
    {
        // Drive SSEL0 active low
        TPM_EEPROM_CHIP_SELECT_LOW;
    }
    else
    {
        // Drive SSEL0 inactive (high)
        TPM_EEPROM_CHIP_SELECT_HIGH;
    }
}

//-----------------------------------------------------------------------------
//  This function initializes the SSP0 port as a SPI mode master.
//  The MISO, MOSI, and SSEL pins are configured in drivers/pincfg/pincfg.c
//-----------------------------------------------------------------------------
void initSpiDrvr(void)
{
    //-----------------------------------------------------------------------------
    //  Initialize SSP_ConfigStruct with SPI defaults:
    //-----------------------------------------------------------------------------
    SSP_ConfigStructInit( &SSP_ConfigStruct );

    //
    //-----------------------------------------------------------------------------
    //
    //   Update: To avoid having to disable interrupts when performing a
    //           serial EEPROM page write cycle, the SSEL functionality is now
    //           achieved by driving the SSEL pin manually as a GPIO output.
    //
    //   We normally run the chip at 5 MHz using 8-bit data. 
    //-----------------------------------------------------------------------------
    SSP_ConfigStruct.Databit   = SSP0_DATASIZE_BITS;
    SSP_ConfigStruct.ClockRate = SSP0_CLOCKRATE_HZ;

    // Initialize SSP0 per SSP_ConfigStruct settings
    SSP_Init( LPC_SSP0, &SSP_ConfigStruct );

    // Enable the SSP0 peripheral
    SSP_Cmd( LPC_SSP0, ENABLE );
}

////////////////////////////////////////////////////////////////////////////
//
//  FlagStatus SpiGetStatus( UNS_32 FlagType )
//
//  This is a wrapper function for the SSP CMSIS function: SSP_GetStatus()
//
//  Purpose:	Checks whether the specified SSP status flag is set or not
//
//             FlagType	Type of flag to check status, should be one
// 							of following:
//							- SSP_STAT_TXFIFO_EMPTY: TX FIFO is empty
//							- SSP_STAT_TXFIFO_NOTFULL: TX FIFO is not full
//							- SSP_STAT_RXFIFO_NOTEMPTY: RX FIFO is not empty
//							- SSP_STAT_RXFIFO_FULL: RX FIFO is full
//							- SSP_STAT_BUSY: SSP peripheral is busy
//
//  returns:		New State of specified SSP status flag (SET or RESET),
//                defined in header file: lpc_types.h
////////////////////////////////////////////////////////////////////////////
FlagStatus SpiGetStatus( UNS_32 FlagType )
{
    return SSP_GetStatus(LPC_SSP0, FlagType);
}

//-----------------------------------------------------------------------------
//  Wrapper function to send a 8/16 bits of data over SPI port.
//  The size of data is set in SSP_ConfigStruct.Databit. See above.
//-----------------------------------------------------------------------------
void SpiSendData( UNS_16 data )
{
  SSP_DATA_SETUP_Type SSP_Data_Setup;

   // Send only when Tx FIFO is not full
   //while( SSP_GetStatus( LPC_SSP0, SSP_STAT_BUSY ) == SET );
   while( SSP_GetStatus( LPC_SSP0, SSP_STAT_TXFIFO_NOTFULL ) == UNSET );
   // Call NXP function in cmsis/lpc17xx_ssp.c
   //SSP_SendData( LPC_SSP0, data );
   
          SSP_Data_Setup.tx_data  = &data;
           SSP_Data_Setup.rx_data  = NULL;
           SSP_Data_Setup.length   = 1;

           // Perform a write to the STATUS register
           (void) SSP_ReadWrite( LPC_SSP0, &SSP_Data_Setup, SSP_TRANSFER_POLLING );
}

//-----------------------------------------------------------------------------
//  Wrapper function to receive 8/16 bits of data over SPI port.
//  The size of data is set in SSP_ConfigStruct.Databit. See above.
//
//  NOTE: A dummy write must be done before calling this function.
//-----------------------------------------------------------------------------
UNS_16 SpiReceiveData ( void )
{
  SSP_DATA_SETUP_Type SSP_Data_Setup;

   // The data register should be read only if the Rx FIFO is not empty
   //while( SSP_GetStatus( LPC_SSP0, SSP_STAT_BUSY ) == SET )
  // while( SSP_GetStatus(LPC_SSP0, SSP_STAT_RXFIFO_NOTEMPTY ) == UNSET ); 
   //while( SSP_GetStatus(
   //LPC_SSP0, SSP_STAT_ ) == SET ); 
  // return SSP_ReceiveData( LPC_SSP0 );
	UNS_16 rx_data;
          SSP_Data_Setup.tx_data  = NULL;
           SSP_Data_Setup.rx_data  = (void *) &rx_data;
           SSP_Data_Setup.length   = 1;

           // Perform a write to the STATUS registe
		   (void) SSP_ReadWrite( LPC_SSP0, &SSP_Data_Setup, SSP_TRANSFER_POLLING );
		   return rx_data;
}

//-----------------------------------------------------------------------------
//    void SpiFlushRxData( void )
//
//    This function flushes any receive data currently in the
//    SSP0 Rx data FIFO.
//-----------------------------------------------------------------------------
void SpiFlushRxData( void )
{
   //volatile UNS_8 black_hole;

   while( SpiGetStatus( SSP_STAT_RXFIFO_NOTEMPTY ) == SET  )
   {
      //black_hole = 
      SSP_ReceiveData( LPC_SSP0 );
   }
}