/////////////////////////////////////////////
//
//  File: SpiDrvr.h
//
//  Description: Header file for SPI Driver for TP Micro
//
//  $Id: SpiDrvr.h,v 1.6 2011/12/28 22:33:45 dsquires Exp $
//
//  $Log: SpiDrvr.h,v $
//  Revision 1.6  2011/12/28 22:33:45  dsquires
//  Reverted to wrappers for spi functions.
//
/////////////////////////////////////////////
/* ---------------------------------------------------------- */

#ifndef INC_SPI_DRVR_H
#define INC_SPI_DRVR_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//-----------------------------------------------------------------------------

// The Microchip 25LC640A serial EEPROM clock must be <= 5 MHz at Vcc = 3.3V.
//
// 5/16/2012 raf :  We lowered this down to 2 mhz, which results in a frequency of
// 1.785714mhz, so we settled on 1.8mhz for the define.  We lowered it from 5 because we
// observed that the data doesnt get processed much faster (if any) from 2mhz.  This is
// because a lot of the time it takes to write the data in the eechip we are sitting there
// and constantly polling if its done or not at 5mhz.

#define SSP0_CLOCKRATE_HZ     2500000

//-----------------------------------------------------------------------------
//    Then next #define must be set to SSP_DATABIT_8 or SSP_DATABIT_16
//-----------------------------------------------------------------------------

#define SSP0_DATASIZE_BITS    SSP_DATABIT_8

//-----------------------------------------------------------------------------
//   The next two #defines specify the port/pin for the SSEL- function
//-----------------------------------------------------------------------------

#define SSP0_SSEL0_PORT         0
#define SSP0_SSEL0_PIN          16

// ----------

extern void initSpiDrvr( void );
extern FlagStatus SpiGetStatus( UNS_32 FlagType );
extern void SpiSlaveSelEna( BOOL_32 bEnable );
extern void SpiSendData( UNS_16 data );
extern UNS_16 SpiReceiveData( void );
extern void SpiFlushRxData( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*SPIDRVR_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
