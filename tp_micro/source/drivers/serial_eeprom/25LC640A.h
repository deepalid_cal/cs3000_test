/////////////////////////////////////////////////////////
//
//    File: 25LC640A.h 
//
//    Description: Header file for 25LC640A serial
//                 EEPROM driver.
//
//    $Id: 25LC640A.h,v 1.5 2011/12/23 23:44:25 dsquires Exp $
//    $Log:
/////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
//    The next two #defines configure the LPC176x port/pin used to control
//    the 25LC640A WP- pin. This is an active low write protect input.
//-----------------------------------------------------------------------------
// On the TP Micro this will be P0.19. This is also OK on the IRD-LPC1768-DEV board
#define SEEP_nEEPROM_WP_PORT    0
#define SEEP_nEEPROM_WP_PIN     19

//-----------------------------------------------------------------------------
//    The 25LC640A is a 64 kbit part (8K x 8)
//-----------------------------------------------------------------------------
#define SEEP_CAPACITY_BYTES   (8 * 1024)
//-----------------------------------------------------------------------------
//    The SEEP_PAGE_SIZE_IN_BYTES is the maximum number of bytes that can
//    be written per write cycle.
//-----------------------------------------------------------------------------
#define SEEP_PAGE_SIZE_IN_BYTES  32

#define SEEP_PAGE_MASK  0x001f
//-----------------------------------------------------------------------------
//    Instruction codes for the 25LC640A device (from datasheet table 2-1)
//-----------------------------------------------------------------------------
#define SEEP_READ    0x03     // Read data from memory array beginning at selected address
#define SEEP_WRITE   0x02     // Write data to memory array beginning at selected address
#define SEEP_WRDI    0x04     // Reset the write enable latch (disable write operations)
#define SEEP_WREN    0x06     // Set the write enable latch (enable write operations)
#define SEEP_RDSR    0x05     // Read STATUS register
#define SEEP_WRSR    0x01     // Write STATUS register

//-----------------------------------------------------------------------------
//    Status register bit assignments (masks)
//-----------------------------------------------------------------------------
#define SEEP_STATUS_WPEN   0x80  // Write-Protect Enable
#define SEEP_STATUS_BP1    0x08  // Block protect 1
#define SEEP_STATUS_BP0    0x04  // Block protect 0
#define SEEP_STATUS_WEL    0x02  // Write enable latch state
#define SEEP_STATUS_WIP    0x01  // Write-In-Progress if set

// Function prototypes
extern void serial_eeprom_init(void);

extern void seep_write_protect_pin( BOOL_32 bEnable );

extern void seep_write_latch_ena( BOOL_32 bEnable );

extern void seep_rd( UNS_16 eep_addr, UNS_8 *pBuf, UNS_16 bytes );

extern void seep_wr( UNS_16 eep_addr, UNS_8 *pBuf, UNS_16 bytes );

extern UNS_8 seep_rd_status(void);

extern void seep_wait_if_WIP(void);

extern void seep_wr_status( UNS_8 data );


