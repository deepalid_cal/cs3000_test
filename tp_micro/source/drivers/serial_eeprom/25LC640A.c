/////////////////////////////////////////////////////////
//
//    File: 25LC640A.c
//
//    Description: Driver for Microchip 25LC640A serial
//                 EEPROM using SSP0 (SPI mode).
//
//    $Id: 25LC640A.c,v 1.10 2011/12/29 20:53:31 dsquires Exp $
//    $Log: 25LC640A.c,v $
//    Revision 1.10  2011/12/29 20:53:31  dsquires
//    Revised the way SSP status was checked in several places. Code now seems to execute
//    properly with non-debug (optimized) code.
//
//    Revision 1.9  2011/12/28 22:33:27  dsquires
//    Reverted to wrappers for spi functions.
//
//    Revision 1.7  2011/12/23 23:44:25  dsquires
//    The serial eeprom functions seem to be working now. Not yet tested with optimized
//    code. There could be timing problems.
//
//    Revision 1.6  2011/12/23 00:36:42  dsquires
//    Work in progress.
//
//    Revision 1.5  2011/12/22 21:05:38  dsquires
//    Work in progress.
//
//    Revision 1.4  2011/12/21 23:25:49  dsquires
//    Work in progress.
//
//    Revision 1.3  2011/12/16 23:06:16  dsquires
//    Work in progress.
//
//    Revision 1.2  2011/12/16 20:07:02  dsquires
//    Revised code to drive SSEL manually using GPIO output.
//
/////////////////////////////////////////////////////////
#include "tpmicro_common.h"

#include "SpiDrvr.h"

#include "25LC640A.h"

// Data transfer parameters for SSP
static SSP_DATA_SETUP_Type SSP_Data_Setup;

// SPI Data for Read Status
static UNS_8 rd_status_txdata[] = { SEEP_RDSR, 0 };

//-----------------------------------------------------------------------------
//    void seep_wait_if_WIP(void)
//
//    This function checks the SEEP status register to see if a WIP (Write in
//    Progress) condition is present. If so, the function waits until the WIP
//    status bit is reset.
//-----------------------------------------------------------------------------
void seep_wait_if_WIP(void)
{
	static UNS_32 maxCount = 0;
	static UNS_32 minCount = 0xff;
	UNS_32 currentCount = 0;
	while( seep_rd_status() & SEEP_STATUS_WIP )
	{
		currentCount++;
		//for(int i=0; i<2000; i++);
		//TODO:uncomment this lin
		vTaskDelay( MS_to_TICKS(1) );
	}
	maxCount = currentCount > maxCount ? currentCount : maxCount;
	minCount = currentCount < minCount ? currentCount : minCount;
}

//-----------------------------------------------------------------------------
//    Executing a WREN instruction successfully SETs the write enable latch.
//
//    These conditions RESET the write enable latch:
//    - Power-up
//    - WRDI instruction successfully executed
//    - WRSR instruction successfully executed
//    - WRITE instruction successfully executed. This can be a byte write,
//       page write, or status register write.
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//
//    void seep_wr_status( UNS_8 data )
//
//    This function writes the specified data to the SEEP status register.
//
//    Note 1: Only the WPEN, BP1, BP0 bits are writable.
//
//-----------------------------------------------------------------------------
void seep_wr_status( UNS_8 data )
{
	UNS_8 tx_data[2];
	UNS_8 rx_data[2];

	tx_data[0] = SEEP_WRSR;
	tx_data[1] = data;

	// Wait in case a write is already in progress
	seep_wait_if_WIP();

	//-----------------------------------------------------------------------------
	//    Set Write enable latch
	//-----------------------------------------------------------------------------
	seep_write_latch_ena( TRUE );

	SpiSlaveSelEna( TRUE );

	SSP_Data_Setup.tx_data  = &tx_data;
	SSP_Data_Setup.rx_data  = (void *) &rx_data;
	SSP_Data_Setup.length   = 2;

	// Perform a write to the STATUS register
	(void) SSP_ReadWrite( LPC_SSP0, &SSP_Data_Setup, SSP_TRANSFER_POLLING );

	SpiSlaveSelEna( FALSE );
}

//-----------------------------------------------------------------------------
//
//    UNS_8 seep_rd_status(void)
//
//    This function reads the SEEP status register.
//
//-----------------------------------------------------------------------------
UNS_8 seep_rd_status(void)
{
	UNS_8 seep_status[2];

	SSP_Data_Setup.tx_data  = &rd_status_txdata;
	SSP_Data_Setup.rx_data  = (void *) &seep_status;
	SSP_Data_Setup.length   = 2;

	SpiSlaveSelEna( TRUE );

	(void) SSP_ReadWrite( LPC_SSP0, &SSP_Data_Setup, SSP_TRANSFER_POLLING );

	SpiSlaveSelEna( FALSE );

	return(  seep_status[1] );
}

//-----------------------------------------------------------------------------
//    void seep_hw_write_protect( BOOL_32 bEnable )
//
//    This function controls the active low write protect signal that
//    is tied to the WP- pin on the Serial EEPROM.
//
//    bEnable == TRUE  : Write Protect active (no EEPROM writing is possible)
//    bEnable == FALSE ; Write Protect off (EEPROM writing is possible)
//-----------------------------------------------------------------------------
void seep_write_protect_pin( BOOL_32 bEnable )
{
	if( bEnable )
	{
		// Make sure WP- to the serial EEPROM chip is low (active)
		GPIO_ClearValue( SEEP_nEEPROM_WP_PORT, (1<<SEEP_nEEPROM_WP_PIN) );
	}
	else
	{
		// Set the WP- signal high (inactive)
		GPIO_SetValue( SEEP_nEEPROM_WP_PORT, (1<<SEEP_nEEPROM_WP_PIN) );
	}
}

//-----------------------------------------------------------------------------
//
//    void seep_init(void)
//
//    This function initializes the SSP0 port for SPI operation with the
//    Microchip 25LC640A serial EEPROM. It also configures the hardware
//    WP- line to the safe state and then enables it in the SEEP itself.
//
//    NOTE: This function also sets both SEEP block protection bits.
//-----------------------------------------------------------------------------
void serial_eeprom_init( void )
{
	UNS_8	data;

	// seep_wait_if_WIP();
	// Make sure SSEL- is deasserted
	SpiSlaveSelEna( FALSE );

	// Initialize the SSP0 driver
	initSpiDrvr();

	// ----------

	// 11/14/2012 rmd : Why write protect and then un-do? Not a big deal. Perhaps squires put
	// in. I leave it. Should this be first before SSP0 peripheral brought to life? You'd think
	// so.
	seep_write_protect_pin( TRUE );

	// 11/14/2012 rmd : This function called from the startup task. Therefore can take advantage
	// of the OS.
	vTaskDelay( MS_to_TICKS( 4 ) );

	// Temporarily allow writes (to status register)
	seep_write_protect_pin( FALSE );

	// ----------

	//-----------------------------------------------------------------------------
	//    Write the WPEN bit in the status register. This enables the WP- pin on
	//   the chip to function as a write protect. Also set both Block Protect bits.
	//-----------------------------------------------------------------------------
	data = SEEP_STATUS_WPEN | SEEP_STATUS_BP1 | SEEP_STATUS_BP0;

	// Wait if a write is already in progress (should not happen here
	seep_wait_if_WIP();

	// Write the status register
	seep_wr_status( data );

	// Wait for data to be sent (until TX FIFO is empty)
	while( SpiGetStatus( SSP_STAT_TXFIFO_EMPTY ) != SET );

	// Enable write protection
	seep_write_protect_pin( TRUE );
}

//-----------------------------------------------------------------------------
//    void seep_write_ena( BOOL_32 bEnable )
//
//    This function enables/disables the internal write enable latch.
//
//-----------------------------------------------------------------------------
void seep_write_latch_ena( BOOL_32 bEnable )
{
	UNS_8 data;

	// Set up the proper instruction
	data = bEnable ? SEEP_WREN : SEEP_WRDI;

	// Assert SEEP chip select
	SpiSlaveSelEna( TRUE );

	// Send instruction
	SpiSendData( data );

	// Wait for data to be sent
	while( SpiGetStatus( SSP_STAT_TXFIFO_EMPTY ) == UNSET );

	// Then flush Rx FIFO
	SpiFlushRxData();

	// Deassert SEEP chip select
	SpiSlaveSelEna( FALSE );
}

//-----------------------------------------------------------------------------
//    void seep_rd( UNS_16 eep_addr, UNS_8 *pBuf, UNS_16 cnt )
//
//    This function reads cnt # bytes from the specified EEPROM location
//    eep_addr into the buffer starting at pBuf.
//
//    NOTE: No check is made for EEPROM address wraparound!!
//    NOTE: YOU MUST GET A MUTEX WHEN ACCESSING THIS FUNCTION!!
//-----------------------------------------------------------------------------
void seep_rd( UNS_16 eep_addr, UNS_8 *pBuf, UNS_16 bytes )
{
	// 7/10/2012 raf :  if a write just happened, we need to wait until it is finished. Better to do this here than after a write, imo
	seep_wait_if_WIP();
	// Select SEEP chip select
	SpiSlaveSelEna( TRUE );

	// Send the 'READ from memory array' command
	SpiSendData( SEEP_READ );

	// Send the MS byte of the starting eeprom address
	SpiSendData( eep_addr >>8 );

	// Send the LS byte of the starting eeprom address
	SpiSendData( eep_addr & 0xff );

	// Wait for all Tx data to be sent
	while( SpiGetStatus( SSP_STAT_TXFIFO_EMPTY ) == UNSET );

	// Flush the Rx FIFO
	SpiFlushRxData();

	// Now fetch the requested data from the EEPROM
	while( bytes-- )
	{

		// dummy write to clock data into master
		//SpiSendData( 0xFF );

		// Read and store the data
		*pBuf++ = SpiReceiveData() & 0xff;

	}

	// Wait for SSP to go unbusy
	while( SpiGetStatus( SSP_STAT_BUSY ) == SET );

	// deselect SEEP chip select
	SpiSlaveSelEna( FALSE );
}

//-----------------------------------------------------------------------------
//    void seep_wr( UNS_16 eep_addr, UNS_8 *pBuf, UNS_16 bytes )
//
//    This function writes the specified number of bytes from a buffer to
//    the EEPROM, starting at the specified eeprom address.
//
//-----------------------------------------------------------------------------
void seep_wr( UNS_16 eep_addr, UNS_8 *pBuf, UNS_16 bytes )
{
	UNS_8 page_bytes_to_write;
	UNS_8 eeprom_addr_adj;

	// Turn off write protect: WP- signal set high
	seep_write_protect_pin( FALSE );

	// Set the write enable latch so we can write to the status register
	seep_write_latch_ena( TRUE );

	//-----------------------------------------------------------------------------
	//    Clear the two Block Protect bits in the status register, but keep
	//    The WPEN bit set.
	//-----------------------------------------------------------------------------
	seep_wr_status( SEEP_STATUS_WPEN );

	// Wait for status register write to complete (5 ms.)
	seep_wait_if_WIP();

	// Clear any left over data in the Rx FIFO
	SpiFlushRxData();

	while( bytes )
	{
		//-----------------------------------------------------------------------------
		//    Check SEEP status in case a prior EEPROM write is still in progress
		//
		//    Note: A self-timed SEEP page write takes about 5 ms.
		//-----------------------------------------------------------------------------
		seep_wait_if_WIP();

		// Set the write enable latch so we can write to the SEEP memory array
		seep_write_latch_ena( TRUE );

		//-----------------------------------------------------------------------------
		//    Calculate how many bytes we can actually write in this page
		//-----------------------------------------------------------------------------
		page_bytes_to_write = SEEP_PAGE_SIZE_IN_BYTES - (eep_addr & (SEEP_PAGE_SIZE_IN_BYTES - 1) );

		// Check if this is more than we need to write
		if( page_bytes_to_write > bytes )
		{
			// It is, so adjust # bytes to write in this page write
			page_bytes_to_write = bytes;
		}

		// Save this (used when calculating next page write eeprom address
		eeprom_addr_adj = page_bytes_to_write;

		// Calculate the number of bytes left after this page write
		bytes -= page_bytes_to_write;

		// Assert SEEP chip select
		SpiSlaveSelEna( TRUE );

		// Send the 'WRITE to memory array' command
		SpiSendData( SEEP_WRITE );
		//(void) SpiReceiveData();

		// Send the MS byte of the starting eeprom address
		SpiSendData( eep_addr >>8 );
		// (void) SpiReceiveData();

		// Send the LS byte of the starting eeprom address
		SpiSendData( eep_addr & 0xff );
		// (void) SpiReceiveData();

		//-----------------------------------------------------------------------------
		//    Write as many bytes as possible to the current page
		//-----------------------------------------------------------------------------
		while( page_bytes_to_write-- )
		{
			// Send a byte, advance buffer pointer
			SpiSendData( *pBuf++ );
			//   (void) SpiReceiveData();
		}
		// Wait for SSP to go unbusy
		while( SpiGetStatus( SSP_STAT_BUSY ) == SET );

		// Bring CS- high, triggering a page write
		SpiSlaveSelEna( FALSE );

		// update eeprom address for the next page write
		eep_addr += eeprom_addr_adj;
	}

	// Wait for the SEEP write to complete (5 ms.)

	seep_wait_if_WIP();


	//-----------------------------------------------------------------------------
	//    Write to status register to reenable the two Block Protect
	//    bits along with WPEN.
	//-----------------------------------------------------------------------------
	seep_wr_status( SEEP_STATUS_WPEN | SEEP_STATUS_BP1 | SEEP_STATUS_BP0 );

	// Wait for the SEEP write to complete (5 ms.)

	seep_wait_if_WIP();

	// Turn on hardware write protect again
	seep_write_protect_pin( TRUE );
}





