/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"pincfg.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//-----------------------------------------------------------------------------
//  TP Micro Pin Configuration Table
//-----------------------------------------------------------------------------

static const UNS_16 TP_Micro_Pin_Cfg_Table[ ] =
{
	//  For each entry use macro: SET_PIN_CFG(port, pin, funcnum, resmode, odmode)
	//
	//  Note: resmode and odmode can be zero if funcnum is not 0 (GPIO mode)

	//  pin 6: P0.26 ADC input 3
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_26, PINSEL_FUNC_1, PINSEL_PINMODE_TRISTATE,  0 ),

	//  pin 7: P0.25 ADC input 2
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_25, PINSEL_FUNC_1, PINSEL_PINMODE_TRISTATE,  0 ),

	//  pin 8: P0.24 ADC input 1
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_24, PINSEL_FUNC_1, PINSEL_PINMODE_TRISTATE,  0 ),

	//  pin 9: P0.23 ADC input 0
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_23, PINSEL_FUNC_1, PINSEL_PINMODE_TRISTATE,  0 ),

	// ----------
	
	// 1/15/2013 rmd : The two spare A-D inputs are being set to outputs with pull-down. I
	// believe the decision to make them as outputs was related to early trouble Mr. Fleury (a
	// novice) had getting the A-D to work consistently. I think the reasoning is faulty.
	// However I see no harm in making these as outputs as they are NC pins. So I am leaving
	// them as is.

	//  pin 20: P1.31 Future ADC input 5, configured as GPIO w/pull-up for now. This
	// configuration is the power-up default 5/24/2012 raf : changed it to digital low output.
	// 11/8/2012 rmd : YEAH but the novice didn't say why he did this! Does it have something to
	// do with him having problems with the readings on the other ADC channels?
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_31, (PINSEL_FUNC_0 | GPIO_OUTPUT), PINSEL_PINMODE_PULLDOWN, PINSEL_PINMODE_OPENDRAIN ),

	//  pin 21: P1.30 Future ADC input 4, configured as GPIO input w/pull-up for now
	//  This configuration is the power-up default
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_30, (PINSEL_FUNC_0 | GPIO_OUTPUT), PINSEL_PINMODE_PULLDOWN, PINSEL_PINMODE_OPENDRAIN ),

	// ----------
	
	//  pin 24: P0.28 I2C clock, SCL0 (open drain output)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_28, PINSEL_FUNC_1, 0, 0 ),

	//  pin 25: P0.27 I2C data, SDA0 (open drain output)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_27, PINSEL_FUNC_1, 0, 0 ),

	//  pin 32: P1.18 CAP1.0, Timer 1 Capture input ch 0 for nPOC_FM_micro signal
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_18, PINSEL_FUNC_3, 0, 0 ),

	// ----------
	
	// 1/15/2013 rmd : Even though the default is input with pull-up, we explicitly set this
	// next group, pins 33 through 38.
	
	//  pin 33: P1.19, GPIO Input with pull-up (the default), MP_DASH_W_CARD
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_19, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),

	//  pin 34: P1.20, GPIO Input with pull-up (the default), MP_DASH_W_TERMINAL
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_20, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),

	//  pin 35: P1.21, GPIO Input with pull-up (the default), MP_DASH_M_CARD
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_21, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),

	//  pin 36: P1.22, GPIO Input with pull-up (the default), MP_DASH_M_TERMINAL
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_22, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),

	//  pin 37: P1.23, GPIO Input with pull-up (the default), MP_DASH_M_TYPE (Which I ... rmd
	//  ... actually do not know what this is used for.)
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_23, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),

	//  pin 38: P1.24, GPIO Input with pull-up (the default), MP_TWO_WIRE_TERMINAL
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_24, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),
	
	// ----------
	
	//  pin 46: P0.0 UART3 TXD3
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_0, PINSEL_FUNC_2, 0, 0 ),

	//  pin 47: P0.1 UART3 RXD3
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_1, PINSEL_FUNC_2, 0, 0 ),

	// pin 48: P0.10 UART3 M2_CONTROL (output)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_10, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, 0 ),
	
	// ----------

	// pin 51: P2.12 TWO_WIRE_1200BAUD_TXD (input as EINT2), have decided pull-up or pull-down
	// doesn't matter which, so leave at default of pull-up.
	SET_PIN_CFG( PINSEL_PORT_2, PINSEL_PIN_12, PINSEL_FUNC_1, 0, 0 ),

	// pin 52: P2.11 TWO_WIRE_60_HERTZ (input as EINT1), have decided pull-up or pull-down
	// doesn't matter which, so leave at default of pull-up.
	SET_PIN_CFG( PINSEL_PORT_2, PINSEL_PIN_11, PINSEL_FUNC_1, 0, 0 ),
	
	// ----------
	
	// pin 56: P0.22 WDT_KICK (output), normal push-pull output (not open-drain)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_22, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, PINSEL_PINMODE_NORMAL ),

	// pin 57: P0.21 nTRIAC_BUS_RESET (output), normal push-pull output (not open-drain)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_21, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, PINSEL_PINMODE_NORMAL ),
	
	// ----------
	
	// pin 59: P0.19 nEEPROM_WP (output)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_19, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, 0 ),

	//  pin 60: P0.18 MOSI SSP (SPI) port 0
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_18, PINSEL_FUNC_2, 0, 0 ),

	//  pin 61: P0.17 MISO SSP (SPI) port 0
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_17, PINSEL_FUNC_2, 0, 0 ),

	//  pin 62: P0.15 SCK SPI port 0
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_15, PINSEL_FUNC_2, 0, 0 ),

	//  pin 63: P0.16 SSEL SPI port 0 (driven as GPIO output, not SSEL)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_16, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, 0 ),
	
	// ----------
	
	//  pin 64: P2.9 UART2 RXD2
	SET_PIN_CFG( PINSEL_PORT_2, PINSEL_PIN_9, PINSEL_FUNC_2, 0, 0 ),

	//  pin 65: P2.8 UART2 TXD2
	SET_PIN_CFG( PINSEL_PORT_2, PINSEL_PIN_8, PINSEL_FUNC_2, 0, 0 ),

	//  pin 66: P2.7 UART2 M1_CONTROL output
	SET_PIN_CFG( PINSEL_PORT_2, PINSEL_PIN_7, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, 0 ),
	
	// ----------

	//  pin 68: P2.5 TWO_WIRE_NEG_ON output, normal push-pull output (not open-drain)
	SET_PIN_CFG( PINSEL_PORT_2, PINSEL_PIN_5, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, PINSEL_PINMODE_NORMAL ),
	
	//  pin 69: P2.4 TWO_WIRE_POS_ON output, normal push-pull output (not open-drain)
	SET_PIN_CFG( PINSEL_PORT_2, PINSEL_PIN_4, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, PINSEL_PINMODE_NORMAL ),
	
	// 1/15/2013 rmd : NOTE - pin 73 P2.2, the FORCE_TO_MARK is not yet in use. And will only be
	// used if needed. It is believed the 2-wire RXD data can be surpressed by other means.
	// Perhaps disconnecting the pin from the UART switching back to a GPIO input.

	// ----------
	
	//  pin 74: P2.1 UART1 RXD1 (two wire serial)
	SET_PIN_CFG( PINSEL_PORT_2, PINSEL_PIN_1, PINSEL_FUNC_2, 0, 0 ),

	//  pin 75: P2.0 UART1 TXD1 (two wire serial)
	SET_PIN_CFG( PINSEL_PORT_2, PINSEL_PIN_0, PINSEL_FUNC_2, 0, 0 ),
	
	// ----------
	
	//////////////////////////////////////////////////////////////////////////
	//  pins 76 - 81 (P0.9 .. P0.4) are left as default inputs with pull-ups
	//////////////////////////////////////////////////////////////////////////
	
	// pin 76: P0.9 ETGAGE input, normally high so set with pull-up (the default from reset)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_9, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),
	
	// pin 78: P0.7 WIND input, pulse train, maybe better into a counter but oh well
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_7, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),
	
	// pin 79: P0.6 FLOW METER input, pulse train, max 200hz
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_6, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),
	
	// pin 80: P0.5 RAIN_BUCKET input, 5ms wide pulse
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_5, PINSEL_FUNC_0, PINSEL_PINMODE_PULLUP, 0 ),
	
	// 1/15/2013 rmd : pin 81 P0.4 is the 2 wire 60 hz clock input and is not yet used as this
	// also runs into EINT1 P2.11.
	
	// ----------
	
	//  pin 86: P1.17 DIAG_LED3 (output)
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_17, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, 0 ),

	//  pin 87: P1.16 DIAG_LED2 (output)
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_16, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, 0 ),

	//  pin 88: P1.15 DIAG_LED1 (output)
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_15, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, 0 ),

	//  pin 89: P1.14 DIAG_LED0 (output)
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_14, (PINSEL_FUNC_0 | GPIO_OUTPUT), 0, 0 ),

	//////////////////////////////////////////////////////////////////////////
	//  pins 90 - 93 (P1.10 .. P1.4) are left as default inputs with pull-ups
	//////////////////////////////////////////////////////////////////////////

	//  pin 94: P1.1 FREEZE_CLICK_micro (input) signal is normally low to the micro
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_1, PINSEL_FUNC_0, PINSEL_PINMODE_PULLDOWN, 0 ),

	//  pin 95: P1.0 RAIN_CLICK_micro (input) signal is normally low to the micro
	SET_PIN_CFG( PINSEL_PORT_1, PINSEL_PIN_0, PINSEL_FUNC_0, PINSEL_PINMODE_PULLDOWN, 0 ),



	//  pin 98: P0.2 UART0 TXD0 (Bootloader port)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_2, PINSEL_FUNC_1, 0, 0 ),

	//  pin 99: P0.3 UART0 RXD0 (Bootloader port)
	SET_PIN_CFG( PINSEL_PORT_0, PINSEL_PIN_3, PINSEL_FUNC_1, 0, 0 ),
};

//-----------------------------------------------------------------------------
//   End of TP Micro Pin Configuration Table
//-----------------------------------------------------------------------------


////////////////////////////////////////////////////////
//
//  The three set_xx functions were taken from the NXP
//  LPC17xx library.
//
// * Copyright(C) 2010, NXP Semiconductor
// * All rights reserved.
////////////////////////////////////////////////////////
static void set_PinFunc ( uint8_t portnum, uint8_t pinnum, uint8_t funcnum);
static void set_ResistorMode ( uint8_t portnum, uint8_t pinnum, uint8_t modenum);
static void set_OpenDrainMode( uint8_t portnum, uint8_t pinnum, uint8_t modenum);

/*********************************************************************//**
 * @brief 		Setup the pin selection function
 * @param[in]	portnum PORT number,
 * 				should be one of the following:
 * 				- PINSEL_PORT_0	: Port 0
 * 				- PINSEL_PORT_1	: Port 1
 * 				- PINSEL_PORT_2	: Port 2
 * 				- PINSEL_PORT_3	: Port 3
 *
 * @param[in]	pinnum	Pin number,
 * 				should be one of the following:
				- PINSEL_PIN_0 : Pin 0
				- PINSEL_PIN_1 : Pin 1
				- PINSEL_PIN_2 : Pin 2
				- PINSEL_PIN_3 : Pin 3
				- PINSEL_PIN_4 : Pin 4
				- PINSEL_PIN_5 : Pin 5
				- PINSEL_PIN_6 : Pin 6
				- PINSEL_PIN_7 : Pin 7
				- PINSEL_PIN_8 : Pin 8
				- PINSEL_PIN_9 : Pin 9
				- PINSEL_PIN_10 : Pin 10
				- PINSEL_PIN_11 : Pin 11
				- PINSEL_PIN_12 : Pin 12
				- PINSEL_PIN_13 : Pin 13
				- PINSEL_PIN_14 : Pin 14
				- PINSEL_PIN_15 : Pin 15
				- PINSEL_PIN_16 : Pin 16
				- PINSEL_PIN_17 : Pin 17
				- PINSEL_PIN_18 : Pin 18
				- PINSEL_PIN_19 : Pin 19
				- PINSEL_PIN_20 : Pin 20
				- PINSEL_PIN_21 : Pin 21
				- PINSEL_PIN_22 : Pin 22
				- PINSEL_PIN_23 : Pin 23
				- PINSEL_PIN_24 : Pin 24
				- PINSEL_PIN_25 : Pin 25
				- PINSEL_PIN_26 : Pin 26
				- PINSEL_PIN_27 : Pin 27
				- PINSEL_PIN_28 : Pin 28
				- PINSEL_PIN_29 : Pin 29
				- PINSEL_PIN_30 : Pin 30
				- PINSEL_PIN_31 : Pin 31

 * @param[in] 	funcnum Function number,
 * 				should be one of the following:
 *				- PINSEL_FUNC_0 : default function
 *				- PINSEL_FUNC_1 : first alternate function
 *				- PINSEL_FUNC_2 : second alternate function
 *				- PINSEL_FUNC_3 : third alternate function
 *
 * @return 		None
 **********************************************************************/
static void set_PinFunc ( uint8_t portnum, uint8_t pinnum, uint8_t funcnum)
{
	uint32_t pinnum_t = pinnum;
	uint32_t pinselreg_idx = 2 * portnum;
	uint32_t *pPinCon = (uint32_t *)&LPC_PINCON->PINSEL0;

	if( pinnum_t >= 16 )
	{
		pinnum_t -= 16;
		pinselreg_idx++;
	}
	*(uint32_t *)(pPinCon + pinselreg_idx) &= ~(0x03UL << (pinnum_t * 2));
	*(uint32_t *)(pPinCon + pinselreg_idx) |= ((uint32_t)funcnum) << (pinnum_t * 2);
}

/*********************************************************************//**
 * @brief 		Setup resistor mode for each pin
 * @param[in]	portnum PORT number,
 * 				should be one of the following:
 * 				- PINSEL_PORT_0	: Port 0
 * 				- PINSEL_PORT_1	: Port 1
 * 				- PINSEL_PORT_2	: Port 2
 * 				- PINSEL_PORT_3	: Port 3
 * @param[in]	pinnum	Pin number,
 * 				should be one of the following:
				- PINSEL_PIN_0 : Pin 0
				- PINSEL_PIN_1 : Pin 1
				- PINSEL_PIN_2 : Pin 2
				- PINSEL_PIN_3 : Pin 3
				- PINSEL_PIN_4 : Pin 4
				- PINSEL_PIN_5 : Pin 5
				- PINSEL_PIN_6 : Pin 6
				- PINSEL_PIN_7 : Pin 7
				- PINSEL_PIN_8 : Pin 8
				- PINSEL_PIN_9 : Pin 9
				- PINSEL_PIN_10 : Pin 10
				- PINSEL_PIN_11 : Pin 11
				- PINSEL_PIN_12 : Pin 12
				- PINSEL_PIN_13 : Pin 13
				- PINSEL_PIN_14 : Pin 14
				- PINSEL_PIN_15 : Pin 15
				- PINSEL_PIN_16 : Pin 16
				- PINSEL_PIN_17 : Pin 17
				- PINSEL_PIN_18 : Pin 18
				- PINSEL_PIN_19 : Pin 19
				- PINSEL_PIN_20 : Pin 20
				- PINSEL_PIN_21 : Pin 21
				- PINSEL_PIN_22 : Pin 22
				- PINSEL_PIN_23 : Pin 23
				- PINSEL_PIN_24 : Pin 24
				- PINSEL_PIN_25 : Pin 25
				- PINSEL_PIN_26 : Pin 26
				- PINSEL_PIN_27 : Pin 27
				- PINSEL_PIN_28 : Pin 28
				- PINSEL_PIN_29 : Pin 29
				- PINSEL_PIN_30 : Pin 30
				- PINSEL_PIN_31 : Pin 31

 * @param[in] 	modenum: Mode number,
 * 				should be one of the following:
				- PINSEL_PINMODE_PULLUP	: Internal pull-up resistor
				- PINSEL_PINMODE_TRISTATE : Tri-state
				- PINSEL_PINMODE_PULLDOWN : Internal pull-down resistor

 * @return 		None
 **********************************************************************/
void set_ResistorMode ( uint8_t portnum, uint8_t pinnum, uint8_t modenum)
{
	uint32_t pinnum_t = pinnum;
	uint32_t pinmodereg_idx = 2 * portnum;
	uint32_t *pPinCon = (uint32_t *)&LPC_PINCON->PINMODE0;

	if( pinnum_t >= 16 )
	{
		pinnum_t -= 16;
		pinmodereg_idx++ ;
	}

	*(uint32_t *)(pPinCon + pinmodereg_idx) &= ~(0x03UL << (pinnum_t * 2));
	*(uint32_t *)(pPinCon + pinmodereg_idx) |= ((uint32_t)modenum) << (pinnum_t * 2);
}

/*********************************************************************//**
 * @brief 		Setup Open drain mode for each pin
 * @param[in]	portnum PORT number,
 * 				should be one of the following:
 * 				- PINSEL_PORT_0	: Port 0
 * 				- PINSEL_PORT_1	: Port 1
 * 				- PINSEL_PORT_2	: Port 2
 * 				- PINSEL_PORT_3	: Port 3
 *
 * @param[in]	pinnum	Pin number,
 * 				should be one of the following:
				- PINSEL_PIN_0 : Pin 0
				- PINSEL_PIN_1 : Pin 1
				- PINSEL_PIN_2 : Pin 2
				- PINSEL_PIN_3 : Pin 3
				- PINSEL_PIN_4 : Pin 4
				- PINSEL_PIN_5 : Pin 5
				- PINSEL_PIN_6 : Pin 6
				- PINSEL_PIN_7 : Pin 7
				- PINSEL_PIN_8 : Pin 8
				- PINSEL_PIN_9 : Pin 9
				- PINSEL_PIN_10 : Pin 10
				- PINSEL_PIN_11 : Pin 11
				- PINSEL_PIN_12 : Pin 12
				- PINSEL_PIN_13 : Pin 13
				- PINSEL_PIN_14 : Pin 14
				- PINSEL_PIN_15 : Pin 15
				- PINSEL_PIN_16 : Pin 16
				- PINSEL_PIN_17 : Pin 17
				- PINSEL_PIN_18 : Pin 18
				- PINSEL_PIN_19 : Pin 19
				- PINSEL_PIN_20 : Pin 20
				- PINSEL_PIN_21 : Pin 21
				- PINSEL_PIN_22 : Pin 22
				- PINSEL_PIN_23 : Pin 23
				- PINSEL_PIN_24 : Pin 24
				- PINSEL_PIN_25 : Pin 25
				- PINSEL_PIN_26 : Pin 26
				- PINSEL_PIN_27 : Pin 27
				- PINSEL_PIN_28 : Pin 28
				- PINSEL_PIN_29 : Pin 29
				- PINSEL_PIN_30 : Pin 30
				- PINSEL_PIN_31 : Pin 31

 * @param[in]	modenum  Open drain mode number,
 * 				should be one of the following:
 * 				- PINSEL_PINMODE_NORMAL : Pin is in the normal (not open drain) mode
 * 				- PINSEL_PINMODE_OPENDRAIN : Pin is in the open drain mode
 *
 * @return 		None
 **********************************************************************/
void set_OpenDrainMode( uint8_t portnum, uint8_t pinnum, uint8_t modenum)
{
	uint32_t *pPinCon = (uint32_t *)&LPC_PINCON->PINMODE_OD0;

	if( modenum == PINSEL_PINMODE_OPENDRAIN )
	{
		*(uint32_t *)(pPinCon + portnum) |= (0x01UL << pinnum);
	}
	else
	{
		*(uint32_t *)(pPinCon + portnum) &= ~(0x01UL << pinnum);
	}
}

/////////////////////////////////////////////////////
//
// void Configure_LPC17xx_Pin( UNS_16 entry_data )
//
// Purpose: Configures a single LPC17xx Pin
//
////////////////////////////////////////////////////
void Configure_LPC17xx_Pin( UNS_16 entry_data )
{
	UNS_8 port;
	UNS_8 pin;
	UNS_8 funcnum;
	UNS_8 resmode;
	UNS_8 odmode;
	UNS_32 gpio_output_mask;

	// Extract the data fields for this entry
	pin     = ( entry_data >> pincfg_pin_shift )     & pincfg_pin_mask;
	port    = ( entry_data >> pincfg_port_shift )    & pincfg_port_mask;
	funcnum = ( entry_data >> pincfg_funcnum_shift ) & pincfg_funcnum_mask;
	resmode = ( entry_data >> pincfg_resmode_shift ) & pincfg_resmode_mask;
	odmode  = ( entry_data >> pincfg_odmode_shift  ) & pincfg_odmode_mask;

	// Now use NXP functions to set up the port.pin
	set_PinFunc ( port, pin, funcnum & 3 );	  // Mask off GPIO_OUTPUT bit here

	// ----------
	
	// See if we need to process this pin as a GPIO output

	if( funcnum == 0x04 )	// This is PINSEL_FUNC_0 | GPIO_OUTPUT attribute
	{
		// Create bit mask in correct position for the port
		gpio_output_mask = 1<<pin;

		GPIO_SetDir( port, gpio_output_mask, 1 );
	}

	// 1/9/2013 rmd : Note - if was an output doesn't set back to an input. At reset all pins
	// are inputs so use is restricted to following reset only.

	// ----------
	
	//-----------------------------------------
	//  Process other modes
	//-----------------------------------------

	// Process pull-up or pull-down settings for GPIO pin mode
	set_ResistorMode ( port, pin, resmode );

	// Process open drain setting for GPIO pin mode
	set_OpenDrainMode( port, pin, odmode );
}

//-------------------------------------------------------
//  void Configure_LPC17xx_Pins( void )
//
// Configures the pins of the LPC17xx as specified in
// the table TP_Micro_Pin_Cfg_Table, found in this file.
//-------------------------------------------------------
void PINCFG_configure_all_tpmicro_pins_using_table( void )
{
	UNS_32	entry;
	
	// ----------

	// 11/14/2012 rmd : Powerup IOCON, GPIO, and GPIO interrupts peripheral blocks.
	CLKPWR_ConfigPPWR( CLKPWR_PCONP_PCGPIO, ENABLE );
	
	// ----------

	// 11/14/2012 rmd : It's a free for all! A 0 in the MASK register enables control of that
	// bit for GPIO manipulation.
	((LPC_GPIO_TypeDef*)LPC_GPIO0)->FIOMASK = 0;
	((LPC_GPIO_TypeDef*)LPC_GPIO0)->FIOMASK = 0;
	((LPC_GPIO_TypeDef*)LPC_GPIO0)->FIOMASK = 0;
	((LPC_GPIO_TypeDef*)LPC_GPIO0)->FIOMASK = 0;

	// ----------
	// ----------
	// ----------
	// ----------

	// 1/17/2013 rmd : IMPORTANT NOTE - The GPIO pins come out of reset as inputs pulled high.
	// So they are weakly pulled high. And we count on that state for some pins that will
	// eventually become outputs. When making a pin an output if we want it high at the micro
	// pin, must set the output register NOW. Else it will glitch LOW when it is set as an
	// output. And be set high when we get around to doing so. By setting it here first there is
	// no glitch and it transitions from an input pulled high to an output driven high without
	// ever going LOW. This is important for a few signals. Two that come to mind are the FET
	// gate control outputs. We want them high at the micro pin as that keeps the FETs turned
	// OFF.
	
	// ----------
	
	// 1/17/2013 rmd : Ensure both 2W FETs are OFF to start. Which they should already be due to
	// the hardware itself. The pins power up (and come out of micro reset) as inputs with
	// on-chip pull-ups. Which after the inverter drives low signals to the 2W Terminal. Which
	// is the OFF state for the FETs.
	TPM_2W_NEG_OFF;

	TPM_2W_POS_OFF;

	// ----------
	
	// 1/9/2013 rmd : Drive MR input to WDT chip to cause all triac output OFF. This is released
	// when the irrigation task is ready to turn ON stations.
	TPM_TRIAC_MR_ACTIVE;

	// ----------

	// 1/9/2013 rmd : Drive CS pin to EEROM high. Deselecting the chip.
	TPM_EEPROM_CHIP_SELECT_HIGH;

	// ----------

	// 1/9/2013 rmd : Start with the WDT_KICK pin LOW. The heartbeat task that toggles it bumps
	// it HIGH then back LOW.
	TPM_TRIAC_WDT_KICK_LOW;

	// ----------

	// 1/18/2013 rmd : Their reset state is pulled high. But the LED's are not ON cause they are
	// not driven. When they are switched to outputs the lines will snap low - as we are setting
	// them low here.
	TPM_LED0_OFF;
	TPM_LED1_OFF;
	TPM_LED2_OFF;
	TPM_LED3_OFF;
	
	// ----------

	// 1/15/2013 rmd : The two spare A-D inputs were set to outputs with pull-down. And we make
	// sure they are driven low here. I believe the decision to make them as outputs was related
	// to early trouble novice Mr. Fleury had getting the A-D to work consistently. I think the
	// reasoning is faulty. However I see no harm in making these as outputs and setting them
	// low as they are NC pins. So I am leaving them as is.
	((LPC_GPIO_TypeDef*)LPC_GPIO1)->FIOCLR = _BIT( 30 );

	((LPC_GPIO_TypeDef*)LPC_GPIO1)->FIOCLR = _BIT( 31 );

	// ----------
	// ----------
	// ----------
	// ----------

	// 1/17/2013 rmd : Now configure them all.
	for( entry = 0; entry < N_ARRAY_ELEMENTS(TP_Micro_Pin_Cfg_Table); entry++ )
	{
		Configure_LPC17xx_Pin( TP_Micro_Pin_Cfg_Table[ entry ] );
	}

	// ----------
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

