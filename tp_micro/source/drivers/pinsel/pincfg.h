/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_PIN_CFG_H
#define INC_PIN_CFG_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"tpmicro_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// shift counts and field sizes for the PIN CONFIG macro data 
#define pincfg_pin_shift      (0)       // 0 bits
#define pincfg_pin_mask       (0x1f)    // 5 bits : (bit 0 to bit 4)

#define pincfg_port_shift     (5)       // 5 bits 
#define pincfg_port_mask      (0x7)     // 3 bits : (bit 5 to bit 7) ports can be 0 to 4

// We use 3 bits for the funcnum field because the extra bit is used to indicate
// when function 0 (GPIO) is to be an OUTPUT, not a (default) input.

#define pincfg_funcnum_shift  (8)       // 8 bits 
#define pincfg_funcnum_mask   (0x7)     // 3 bits : (bit 8 to bit 10)

#define pincfg_resmode_shift  (11)      // 11 bits
#define pincfg_resmode_mask   (0x3)     // 2 bits : (bit 11 to bit 12)

#define pincfg_odmode_shift   (13)      // 13 bits
#define pincfg_odmode_mask    (1)       // 1 bit : (bit 13)

// This can be bit-wise OR'd with PINSEL_FUNC_0 to specify a GPIO output

#define GPIO_OUTPUT           4

////////////////////////////////////////////////////////////////////////////////////////////////
//
//    MACRO: SET_PIN_CFG(port, pin, funcnum, resmode, odmode)
//
//      UNS_8 port     : the LPC176x port number (PINSEL_PORT_0 to PINSEL_PORT_3) 
//      UNS_8 pin      : the bit position in the port (PINSEL_PIN_0 to PINSEL_PIN_32)
//      UNS_8 funcnum  : the pin function number (PINSEL_FUNC_0 to PINSEL_FUNC_3)
//
// 5/24/2012 raf :  I dont think this following line is true.
//      The following parameters apply to GPIO mode only (PINSEL_FUNC_0):   
//
//      UNS_8 resmode  : the resistor mode (PINSEL_PINMODE_PULLUP, PINSEL_PINMODE_TRISTATE, or
//                                        {PINSEL_PINMODE_PULLDOWN, or PINSEL_RESMODE_NA)
//
//      UNS_8 odmode   : the open drain mode (PINSEL_PINMODE_NORMAL or PINSEL_PINMODE_OPENDRAIN)
//
//    For PINSEL_FUNC_0 you can bitwise OR in GPIO_OUTPUT to force the pin to be an output
//
//    This macro builds a 16-bit unsigned integer with fields containing the macro parameters.
//    The Configure_LPC17xx_Pins function unpacks these fields and calls NXP functions to 
//    set up the pin per the macro parameter values.
//
////////////////////////////////////////////////////////////////////////////////////////////////
#define SET_PIN_CFG(port, pin, funcnum, resmode, odmode)            \
  (                                                                 \
    ((UNS_16)(odmode & pincfg_odmode_mask)<<pincfg_odmode_shift )|     \
    ((UNS_16)(resmode & pincfg_resmode_mask)<<pincfg_resmode_shift )|  \
    ((UNS_16)(funcnum & pincfg_funcnum_mask)<<pincfg_funcnum_shift )|  \
    ((UNS_16)(port & pincfg_port_mask)<<pincfg_port_shift )|           \
    ((UNS_16)(pin & pincfg_pin_mask)<<pincfg_pin_shift)                \
  )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		_TPM_EEPROM_SELECT_PIN			( _BIT(16) )

#define		TPM_EEPROM_CHIP_SELECT_HIGH		( LPC_GPIO0->FIOSET = _TPM_EEPROM_SELECT_PIN )

#define		TPM_EEPROM_CHIP_SELECT_LOW		( LPC_GPIO0->FIOCLR = _TPM_EEPROM_SELECT_PIN )

/* ---------------------------------------------------------- */

#define		_TPM_LED0_D1_PIN		( _BIT(14) )

#define		TPM_LED0_ON				( LPC_GPIO1->FIOSET = _TPM_LED0_D1_PIN )

#define		TPM_LED0_OFF			( LPC_GPIO1->FIOCLR = _TPM_LED0_D1_PIN )

#define		TPM_LED0_IS_ON			( (LPC_GPIO1->FIOPIN & _TPM_LED0_D1_PIN) == _TPM_LED0_D1_PIN )

#define		TPM_LED0_IS_OFF			( (LPC_GPIO1->FIOPIN & _TPM_LED0_D1_PIN) == 0 )

#define		TPM_LED0_TOGGLE			( TPM_LED0_IS_ON ? TPM_LED0_OFF : TPM_LED0_ON )

/* ---------------------------------------------------------- */

#define		_TPM_LED1_D2_PIN		( _BIT(15) )

#define		TPM_LED1_ON				( LPC_GPIO1->FIOSET = _TPM_LED1_D2_PIN )

#define		TPM_LED1_OFF			( LPC_GPIO1->FIOCLR = _TPM_LED1_D2_PIN )

#define		TPM_LED1_IS_ON			( (LPC_GPIO1->FIOPIN & _TPM_LED1_D2_PIN) == _TPM_LED1_D2_PIN )

#define		TPM_LED1_IS_OFF			( (LPC_GPIO1->FIOPIN & _TPM_LED1_D2_PIN) == 0 )

#define		TPM_LED1_TOGGLE			( TPM_LED1_IS_ON ? TPM_LED1_OFF : TPM_LED1_ON )

/* ---------------------------------------------------------- */

#define		_TPM_LED2_D3_PIN		( _BIT(16) )

#define		TPM_LED2_ON				( LPC_GPIO1->FIOSET = _TPM_LED2_D3_PIN )

#define		TPM_LED2_OFF			( LPC_GPIO1->FIOCLR = _TPM_LED2_D3_PIN )

#define		TPM_LED2_IS_ON			( (LPC_GPIO1->FIOPIN & _TPM_LED2_D3_PIN) == _TPM_LED2_D3_PIN )

#define		TPM_LED2_IS_OFF			( (LPC_GPIO1->FIOPIN & _TPM_LED2_D3_PIN) == 0 )

#define		TPM_LED2_TOGGLE			( TPM_LED2_IS_ON ? TPM_LED2_OFF : TPM_LED2_ON )

/* ---------------------------------------------------------- */

#define		_TPM_LED3_D4_PIN		( _BIT(17) )

#define		TPM_LED3_ON				( LPC_GPIO1->FIOSET = _TPM_LED3_D4_PIN )

#define		TPM_LED3_OFF			( LPC_GPIO1->FIOCLR = _TPM_LED3_D4_PIN )

#define		TPM_LED3_IS_ON			( (LPC_GPIO1->FIOPIN & _TPM_LED3_D4_PIN) == _TPM_LED3_D4_PIN )

#define		TPM_LED3_IS_OFF			( (LPC_GPIO1->FIOPIN & _TPM_LED3_D4_PIN) == 0 )

#define		TPM_LED3_TOGGLE			( TPM_LED3_IS_ON ? TPM_LED3_OFF : TPM_LED3_ON )

/* ---------------------------------------------------------- */

#define		_TPM_TRIAC_WDT_KICK_PIN		( _BIT(22) )

#define		TPM_TRIAC_WDT_KICK_HIGH		( LPC_GPIO0->FIOSET = _TPM_TRIAC_WDT_KICK_PIN )

#define		TPM_TRIAC_WDT_KICK_LOW		( LPC_GPIO0->FIOCLR = _TPM_TRIAC_WDT_KICK_PIN )

/* ---------------------------------------------------------- */

// 1/9/2013 rmd : The triac master reset (MR) pin to the WDT circuit. When active (low)
// turns ALL triac outputs OFF.

#define		_TPM_TRIAC_MR_PIN				( _BIT(21) )

#define		TPM_TRIAC_MR_PASSIVE			( LPC_GPIO0->FIOSET = _TPM_TRIAC_MR_PIN )

#define		TPM_TRIAC_MR_ACTIVE				( LPC_GPIO0->FIOCLR = _TPM_TRIAC_MR_PIN )

#define		TPM_TRIAC_MR_IS_PASSIVE			( (LPC_GPIO0->FIOPIN & _TPM_TRIAC_MR_PIN) == _TPM_TRIAC_MR_PIN )

#define		TPM_TRIAC_MR_IS_ACTIVE			( (LPC_GPIO0->FIOPIN & _TPM_TRIAC_MR_PIN) == 0 )

/* ---------------------------------------------------------- */

#define		_TPM_RAIN_CLIK_PIN				( _BIT(0) )

#define		TPM_RAIN_CLIK_SHOWS_RAIN		( (LPC_GPIO1->FIOPIN & _TPM_RAIN_CLIK_PIN) == _TPM_RAIN_CLIK_PIN )

#define		TPM_RAIN_CLIK_DRY				( (LPC_GPIO1->FIOPIN & _TPM_RAIN_CLIK_PIN) == 0 )

/* ---------------------------------------------------------- */

#define		_TPM_FREEZE_CLIK_PIN			( _BIT(1) )

#define		TPM_FREEZE_CLIK_SHOWS_FREEZE	( (LPC_GPIO1->FIOPIN & _TPM_FREEZE_CLIK_PIN) == _TPM_FREEZE_CLIK_PIN )

#define		TPM_FREEZE_CLIK_WARM			( (LPC_GPIO1->FIOPIN & _TPM_FREEZE_CLIK_PIN) == 0 )

/* ---------------------------------------------------------- */

#define		_TPM_2W_NEG_ON_PIN				( _BIT(5) )

// 1/17/2013 rmd : The FET control pins leave the micro and pass through and inverter on
// their way to the FET gate control. At the 2W terminal a high level signal turns ON the
// FET. Low level keeps them OFF.
#define		TPM_2W_NEG_OFF					( LPC_GPIO2->FIOSET = _TPM_2W_NEG_ON_PIN )

#define		TPM_2W_NEG_ON					( LPC_GPIO2->FIOCLR = _TPM_2W_NEG_ON_PIN )

/* ---------------------------------------------------------- */

#define		_TPM_2W_POS_ON_PIN				( _BIT(4) )

// 1/17/2013 rmd : The FET control pins leave the micro and pass through and inverter on
// their way to the FET gate control. At the 2W terminal a high level signal turns ON the
// FET. Low level keeps them OFF.
#define		TPM_2W_POS_OFF					( LPC_GPIO2->FIOSET = _TPM_2W_POS_ON_PIN )

#define		TPM_2W_POS_ON					( LPC_GPIO2->FIOCLR = _TPM_2W_POS_ON_PIN )

/* ---------------------------------------------------------- */


extern void Configure_LPC17xx_Pin( UNS_16 entry_data );


void PINCFG_configure_all_tpmicro_pins_using_table( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*PINCFG_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

