/*  file = tpmicro_common.h                   2012.12.31 rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_TPMICRO_COMMON_H
#define _INC_TPMICRO_COMMON_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Use this as the function header.
// <<header start>>
/* ---------------------------------------------------------- */
/**
	@DESCRIPTION (none)
	
	@CALLER_MUTEX_REQUIREMENTS (none)
	
	@MEMORY_RESPONSIBILITES (none)
	
	@EXECUTED Executed within the context of the xxx task.
	
	@RETURN (none)
	
	@ORIGINAL 2012.09.14 rmd
	
	@REVISIONS (none)
*/
// <<header end>>

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
Convenient bit field definition that eliminates the need for a discrete union to set the 
size and then have to pass through to get at the items. Can reference them directly when 
using this structure like this:
 
NOTE: the minimum size using this construct seems to be 4 bytes.

typedef struct { 
    union {
		UNS_64	overall_size;

		struct
		{
			// NOTE: In GCC this is the Least Significant Bit (Little Endian)

			UNS_32		two_digit_number					: 2;

			BOOL_32		true_falsey							: 1;

			// NOTE: This is the MS bit in GCC.
		};
	};
} BIG_BIT_FIELD_STRUCT;

BIG_BIT_FIELD_STRUCT	bbf;
 
bbf.true_falsey
*/ 

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Use this macro to set delays directly in ms. Good for OS tick rates up to but not
// exceeding 1000Hz.
#define MS_to_TICKS( ms )	((ms)/portTICK_RATE_MS)   

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/16/2013 rmd : Use DMA to manage TX data to uart peripheral buffers.
#define UART_TX_DMA		(1)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2011.10.20 rmd : Set to 0 to render calls to assert to nothing. When set to a 1 there
// needs to be a function definition for __assert! 
#define		CALSENSE_USING_ASSERT	(1)

// 4/11/2012 rmd : I think this should the first include file. Because some of the low level
// code (FreeRTOS for example and maybe some of the NXP provided) uses assert. And we want
// to define its behaviour.
#include	"assert.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<stdio.h>

#include	<stdlib.h>

#include	<string.h>

#include	<stdarg.h>

#include	<stdint.h>

// Should this be included when using a floating point co-processor?
#include	<math.h>

#ifndef NO_DEBUG_PRINTF
	// Debug I/O support .. but does NOT redefine printf to the debug version debug_printf.
	// If you want to do that include __debug_stdio.h instead of __cross_studio.h
	#include <__cross_studio_io.h>
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#include	"lpc17xx_clkpwr.h"

#include	"lpc17xx_timer.h"

#include	"lpc17xx_pinsel.h"

#include	"lpc17xx_gpio.h"

#include	"lpc17xx_i2c.h"

#include	"lpc17xx_ssp.h"

#include	"lpc17xx_uart.h"

#include	"LPC17xx_rit.h"

#include	"LPC17xx_adc.h"

// ----------

// 1/21/2013 rmd : I had to pull these definitions from LPC1763.h and put them here because
// of naming conflicts if you attempt to include LPC1763.h.

#define EXTINT_EINT1_BITBAND (*(volatile unsigned *)0x43F82804)

#define EXTINT_EINT2_BITBAND (*(volatile unsigned *)0x43F82808)

#define EXTMODE_EXTMODE1_BITBAND (*(volatile unsigned *)0x43F82904)

#define EXTMODE_EXTMODE2_BITBAND (*(volatile unsigned *)0x43F82908)

#define EXTPOLAR_EXTPOLAR1_BITBAND (*(volatile unsigned *)0x43F82984)

#define EXTPOLAR_EXTPOLAR2_BITBAND (*(volatile unsigned *)0x43F82988)


#define T1TCR_Counter_Enable_BITBAND (*(volatile unsigned *)0x42100080)

#define T1IR_MR0_BITBAND (*(volatile unsigned *)0x42100000)


#define T2TCR_Counter_Enable_BITBAND (*(volatile unsigned *)0x43200080)

#define T2IR_MR0_BITBAND (*(volatile unsigned *)0x43200000)


#define T3TCR_Counter_Enable_BITBAND (*(volatile unsigned *)0x43280080)

#define T3IR_MR0_BITBAND (*(volatile unsigned *)0x43280000)


#define U1LSR_RDR_BITBAND (*(volatile unsigned *)0x42200280)

#define U1LSR_THRE_BITBAND (*(volatile unsigned *)0x42200294)

#define U1LSR_TEMT_BITBAND (*(volatile unsigned *)0x42200298)

#define U1FCR_FIFO_Enable_BITBAND (*(volatile unsigned *)0x42200100)

#define U1FCR_Rx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x42200104)

#define U1FCR_Tx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x42200108)

#define U1TER_TXEN_BITBAND (*(volatile unsigned *)0x4220061C)

#define U1IER_RBR_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x42200080)


#define WDCLKSEL_WDLOCK_BITBAND (*(volatile unsigned *)0x4200027C)

#define WDMOD_WDEN_BITBAND (*(volatile unsigned *)0x42000000)

#define WDMOD_WDRESET_BITBAND (*(volatile unsigned *)0x42000004)


#define PCON_BODRPM_BITBAND (*(volatile unsigned *)0x43F81808)

#define PCON_BOGD_BITBAND (*(volatile unsigned *)0x43F8180C)

#define PCON_BORD_BITBAND (*(volatile unsigned *)0x43F81810)


#define RSID_POR_BIT 0

#define RSID_EXTR_BIT 1

#define RSID_WDTR_BIT 2

#define RSID_BODR_BIT 3


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#include	"lpc_types.h"

#include	"bithacks.h"

#include	"general_picked_support.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/17/2013 rmd : Notes on timer use within this project.

/*

TIMER 0: Is being used for the OS Stats timer. To show task loading. This may have to be 
sacraficed someday for a real need. In the release code this is to be disabled..
 
TIMER 1: Is being used for the Two_Wire FET gate control signals. To manage the 10usec FET 
turn off time needed to avoid punch-through in the H-Bridge (both FETs ON at the same 
time).

TIMER 2: Is used to control the M1 RS-485 driver control.

TIMER 3: Is used to control the M2 RS-485 driver control.

SYSTICK: Provides the FreeRTOS OS tick. Which we actually run at 4kHz. And once each 4th 
interrupt invoke the OS. On each of the 4kHertz interrupts we send commands to the A-D 
hardware. So we can achieve the high sample rate needed for short detection.

RIT: The RIT is running at a 5 hertz rate. And is used as a general purpose messenging 
timer. Post queue messages every 400 ms to keep the irrigation going. Once per second for 
the 1 hertz activities. And perhaps more as needed.

*/ 


// 1/30/2014 rmd : Notes on the UARTS used within this project.

/*

UART 0:


UART 1:


UART 2:


UART 3:


*/ 



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef DEBUG

	#define DEBUG_STOP()  { portENTER_CRITICAL(); while(1); }

#else

	#define DEBUG_STOP()

#endif


#ifdef DEBUG

	// 1/27/2016 rmd : Apparently the watchdog time timeout even when the debugger is stopped.
	// We have two behaviors defined. One is if it timeouts out the code stops on a while
	// forever loop. This is useful so the developer can see that a watchdog occurred. A second
	// behavior is to instead send an alert line to the main board on a wdt timeout and not stop
	// the code.
	//
	// 1/27/2016 rmd : Comment out the following line to avoid the endless loop behavior.
	//#define	WATCHDOG_TIMEOUT_FALLS_INTO_WHILE_FOREVER 1
	
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"task_priorities.h"

// FreeRTOS ... FreeRTOS.h in turn brings in FreeRTOSConfig.h. */
#include	"FreeRTOS.h"

#include	"task.h"

#include	"semphr.h"

#include	"queue.h"

#include	"timers.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs3000_tpmicro_common.h"

#include	"utils.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CS_FILE_NAME( fname ) (RemovePathFromFileName( fname ))

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"packet_definitions.h"

// 1/16/2013 rmd : Contains all the queues and semaphores and more.
#include	"startup.h"

#include	"pincfg.h"

#include	"error_handler.h"

#include	"memblkmgr.h"

#include	"hardwaremanager.h"

#include	"ringbuffer.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

