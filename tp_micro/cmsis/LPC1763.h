// LPC1763 register and bit definitions.
//
// Copyright (c) 2012 Rowley Associates Limited.
//
// This file may be distributed under the terms of the License Agreement
// provided with this software.
//
// THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

#ifndef LPC1763_h
#define LPC1763_h


#define GPIO_BASE_ADDRESS 0x2009C000

#define FIO0DIR (*(volatile unsigned long *)0x2009C000)
#define FIO0DIR_OFFSET 0x0

#define FIO0DIR0 (*(volatile unsigned char *)0x2009C000)
#define FIO0DIR0_OFFSET 0x0

#define FIO0DIR1 (*(volatile unsigned char *)0x2009C001)
#define FIO0DIR1_OFFSET 0x1

#define FIO0DIR2 (*(volatile unsigned char *)0x2009C002)
#define FIO0DIR2_OFFSET 0x2

#define FIO0DIR3 (*(volatile unsigned char *)0x2009C003)
#define FIO0DIR3_OFFSET 0x3

#define FIO0DIRL (*(volatile unsigned short *)0x2009C000)
#define FIO0DIRL_OFFSET 0x0

#define FIO0DIRU (*(volatile unsigned short *)0x2009C002)
#define FIO0DIRH FIO0DIRU
#define FIO0DIRU_OFFSET 0x2
#define FIO0DIRH_OFFSET FIO0DIRU_OFFSET

#define FIO1DIR (*(volatile unsigned long *)0x2009C020)
#define FIO1DIR_OFFSET 0x20

#define FIO1DIR0 (*(volatile unsigned char *)0x2009C020)
#define FIO1DIR0_OFFSET 0x20

#define FIO1DIR1 (*(volatile unsigned char *)0x2009C021)
#define FIO1DIR1_OFFSET 0x21

#define FIO1DIR2 (*(volatile unsigned char *)0x2009C022)
#define FIO1DIR2_OFFSET 0x22

#define FIO1DIR3 (*(volatile unsigned char *)0x2009C023)
#define FIO1DIR3_OFFSET 0x23

#define FIO1DIRL (*(volatile unsigned short *)0x2009C020)
#define FIO1DIRL_OFFSET 0x20

#define FIO1DIRU (*(volatile unsigned short *)0x2009C022)
#define FIO1DIRH FIO1DIRU
#define FIO1DIRU_OFFSET 0x22
#define FIO1DIRH_OFFSET FIO1DIRU_OFFSET

#define FIO2DIR (*(volatile unsigned long *)0x2009C040)
#define FIO2DIR_OFFSET 0x40

#define FIO2DIR0 (*(volatile unsigned char *)0x2009C040)
#define FIO2DIR0_OFFSET 0x40

#define FIO2DIR1 (*(volatile unsigned char *)0x2009C041)
#define FIO2DIR1_OFFSET 0x41

#define FIO2DIR2 (*(volatile unsigned char *)0x2009C042)
#define FIO2DIR2_OFFSET 0x42

#define FIO2DIR3 (*(volatile unsigned char *)0x2009C043)
#define FIO2DIR3_OFFSET 0x43

#define FIO2DIRL (*(volatile unsigned short *)0x2009C040)
#define FIO2DIRL_OFFSET 0x40

#define FIO2DIRU (*(volatile unsigned short *)0x2009C042)
#define FIO2DIRH FIO2DIRU
#define FIO2DIRU_OFFSET 0x42
#define FIO2DIRH_OFFSET FIO2DIRU_OFFSET

#define FIO3DIR (*(volatile unsigned long *)0x2009C060)
#define FIO3DIR_OFFSET 0x60

#define FIO3DIR0 (*(volatile unsigned char *)0x2009C060)
#define FIO3DIR0_OFFSET 0x60

#define FIO3DIR1 (*(volatile unsigned char *)0x2009C061)
#define FIO3DIR1_OFFSET 0x61

#define FIO3DIR2 (*(volatile unsigned char *)0x2009C062)
#define FIO3DIR2_OFFSET 0x62

#define FIO3DIR3 (*(volatile unsigned char *)0x2009C063)
#define FIO3DIR3_OFFSET 0x63

#define FIO3DIRL (*(volatile unsigned short *)0x2009C060)
#define FIO3DIRL_OFFSET 0x60

#define FIO3DIRU (*(volatile unsigned short *)0x2009C062)
#define FIO3DIRH FIO3DIRU
#define FIO3DIRU_OFFSET 0x62
#define FIO3DIRH_OFFSET FIO3DIRU_OFFSET

#define FIO4DIR (*(volatile unsigned long *)0x2009C080)
#define FIO4DIR_OFFSET 0x80

#define FIO4DIR0 (*(volatile unsigned char *)0x2009C080)
#define FIO4DIR0_OFFSET 0x80

#define FIO4DIR1 (*(volatile unsigned char *)0x2009C081)
#define FIO4DIR1_OFFSET 0x81

#define FIO4DIR2 (*(volatile unsigned char *)0x2009C082)
#define FIO4DIR2_OFFSET 0x82

#define FIO4DIR3 (*(volatile unsigned char *)0x2009C083)
#define FIO4DIR3_OFFSET 0x83

#define FIO4DIRL (*(volatile unsigned short *)0x2009C080)
#define FIO4DIRL_OFFSET 0x80

#define FIO4DIRU (*(volatile unsigned short *)0x2009C082)
#define FIO4DIRH FIO4DIRU
#define FIO4DIRU_OFFSET 0x82
#define FIO4DIRH_OFFSET FIO4DIRU_OFFSET

#define FIO0MASK (*(volatile unsigned long *)0x2009C010)
#define FIO0MASK_OFFSET 0x10

#define FIO0MASK0 (*(volatile unsigned char *)0x2009C010)
#define FIO0MASK0_OFFSET 0x10

#define FIO0MASK1 (*(volatile unsigned char *)0x2009C011)
#define FIO0MASK1_OFFSET 0x11

#define FIO0MASK2 (*(volatile unsigned char *)0x2009C012)
#define FIO0MASK2_OFFSET 0x12

#define FIO0MASK3 (*(volatile unsigned char *)0x2009C013)
#define FIO0MASK3_OFFSET 0x13

#define FIO0MASKL (*(volatile unsigned short *)0x2009C010)
#define FIO0MASKL_OFFSET 0x10

#define FIO0MASKU (*(volatile unsigned short *)0x2009C012)
#define FIO0MASKH FIO0MASKU
#define FIO0MASKU_OFFSET 0x12
#define FIO0MASKH_OFFSET FIO0MASKU_OFFSET

#define FIO1MASK (*(volatile unsigned long *)0x2009C030)
#define FIO1MASK_OFFSET 0x30

#define FIO1MASK0 (*(volatile unsigned char *)0x2009C030)
#define FIO1MASK0_OFFSET 0x30

#define FIO1MASK1 (*(volatile unsigned char *)0x2009C031)
#define FIO1MASK1_OFFSET 0x31

#define FIO1MASK2 (*(volatile unsigned char *)0x2009C032)
#define FIO1MASK2_OFFSET 0x32

#define FIO1MASK3 (*(volatile unsigned char *)0x2009C033)
#define FIO1MASK3_OFFSET 0x33

#define FIO1MASKL (*(volatile unsigned short *)0x2009C030)
#define FIO1MASKL_OFFSET 0x30

#define FIO1MASKU (*(volatile unsigned short *)0x2009C032)
#define FIO1MASKH FIO1MASKU
#define FIO1MASKU_OFFSET 0x32
#define FIO1MASKH_OFFSET FIO1MASKU_OFFSET

#define FIO2MASK (*(volatile unsigned long *)0x2009C050)
#define FIO2MASK_OFFSET 0x50

#define FIO2MASK0 (*(volatile unsigned char *)0x2009C050)
#define FIO2MASK0_OFFSET 0x50

#define FIO2MASK1 (*(volatile unsigned char *)0x2009C051)
#define FIO2MASK1_OFFSET 0x51

#define FIO2MASK2 (*(volatile unsigned char *)0x2009C052)
#define FIO2MASK2_OFFSET 0x52

#define FIO2MASK3 (*(volatile unsigned char *)0x2009C053)
#define FIO2MASK3_OFFSET 0x53

#define FIO2MASKL (*(volatile unsigned short *)0x2009C050)
#define FIO2MASKL_OFFSET 0x50

#define FIO2MASKU (*(volatile unsigned short *)0x2009C052)
#define FIO2MASKH FIO2MASKU
#define FIO2MASKU_OFFSET 0x52
#define FIO2MASKH_OFFSET FIO2MASKU_OFFSET

#define FIO3MASK (*(volatile unsigned long *)0x2009C070)
#define FIO3MASK_OFFSET 0x70

#define FIO3MASK0 (*(volatile unsigned char *)0x2009C070)
#define FIO3MASK0_OFFSET 0x70

#define FIO3MASK1 (*(volatile unsigned char *)0x2009C071)
#define FIO3MASK1_OFFSET 0x71

#define FIO3MASK2 (*(volatile unsigned char *)0x2009C072)
#define FIO3MASK2_OFFSET 0x72

#define FIO3MASK3 (*(volatile unsigned char *)0x2009C073)
#define FIO3MASK3_OFFSET 0x73

#define FIO3MASKL (*(volatile unsigned short *)0x2009C070)
#define FIO3MASKL_OFFSET 0x70

#define FIO3MASKU (*(volatile unsigned short *)0x2009C072)
#define FIO3MASKH FIO3MASKU
#define FIO3MASKU_OFFSET 0x72
#define FIO3MASKH_OFFSET FIO3MASKU_OFFSET

#define FIO4MASK (*(volatile unsigned long *)0x2009C090)
#define FIO4MASK_OFFSET 0x90

#define FIO4MASK0 (*(volatile unsigned char *)0x2009C090)
#define FIO4MASK0_OFFSET 0x90

#define FIO4MASK1 (*(volatile unsigned char *)0x2009C091)
#define FIO4MASK1_OFFSET 0x91

#define FIO4MASK2 (*(volatile unsigned char *)0x2009C092)
#define FIO4MASK2_OFFSET 0x92

#define FIO4MASK3 (*(volatile unsigned char *)0x2009C093)
#define FIO4MASK3_OFFSET 0x93

#define FIO4MASKL (*(volatile unsigned short *)0x2009C090)
#define FIO4MASKL_OFFSET 0x90

#define FIO4MASKU (*(volatile unsigned short *)0x2009C092)
#define FIO4MASKH FIO4MASKU
#define FIO4MASKU_OFFSET 0x92
#define FIO4MASKH_OFFSET FIO4MASKU_OFFSET

#define FIO0PIN (*(volatile unsigned long *)0x2009C014)
#define FIO0PIN_OFFSET 0x14

#define FIO0PIN0 (*(volatile unsigned char *)0x2009C014)
#define FIO0PIN0_OFFSET 0x14

#define FIO0PIN1 (*(volatile unsigned char *)0x2009C015)
#define FIO0PIN1_OFFSET 0x15

#define FIO0PIN2 (*(volatile unsigned char *)0x2009C016)
#define FIO0PIN2_OFFSET 0x16

#define FIO0PIN3 (*(volatile unsigned char *)0x2009C017)
#define FIO0PIN3_OFFSET 0x17

#define FIO0PINL (*(volatile unsigned short *)0x2009C014)
#define FIO0PINL_OFFSET 0x14

#define FIO0PINU (*(volatile unsigned short *)0x2009C016)
#define FIO0PINH FIO0PINU
#define FIO0PINU_OFFSET 0x16
#define FIO0PINH_OFFSET FIO0PINU_OFFSET

#define FIO1PIN (*(volatile unsigned long *)0x2009C034)
#define FIO1PIN_OFFSET 0x34

#define FIO1PIN0 (*(volatile unsigned char *)0x2009C034)
#define FIO1PIN0_OFFSET 0x34

#define FIO1PIN1 (*(volatile unsigned char *)0x2009C035)
#define FIO1PIN1_OFFSET 0x35

#define FIO1PIN2 (*(volatile unsigned char *)0x2009C036)
#define FIO1PIN2_OFFSET 0x36

#define FIO1PIN3 (*(volatile unsigned char *)0x2009C037)
#define FIO1PIN3_OFFSET 0x37

#define FIO1PINL (*(volatile unsigned short *)0x2009C034)
#define FIO1PINL_OFFSET 0x34

#define FIO1PINU (*(volatile unsigned short *)0x2009C036)
#define FIO1PINH FIO1PINU
#define FIO1PINU_OFFSET 0x36
#define FIO1PINH_OFFSET FIO1PINU_OFFSET

#define FIO2PIN (*(volatile unsigned long *)0x2009C054)
#define FIO2PIN_OFFSET 0x54

#define FIO2PIN0 (*(volatile unsigned char *)0x2009C054)
#define FIO2PIN0_OFFSET 0x54

#define FIO2PIN1 (*(volatile unsigned char *)0x2009C055)
#define FIO2PIN1_OFFSET 0x55

#define FIO2PIN2 (*(volatile unsigned char *)0x2009C056)
#define FIO2PIN2_OFFSET 0x56

#define FIO2PIN3 (*(volatile unsigned char *)0x2009C057)
#define FIO2PIN3_OFFSET 0x57

#define FIO2PINL (*(volatile unsigned short *)0x2009C054)
#define FIO2PINL_OFFSET 0x54

#define FIO2PINU (*(volatile unsigned short *)0x2009C056)
#define FIO2PINH FIO2PINU
#define FIO2PINU_OFFSET 0x56
#define FIO2PINH_OFFSET FIO2PINU_OFFSET

#define FIO3PIN (*(volatile unsigned long *)0x2009C074)
#define FIO3PIN_OFFSET 0x74

#define FIO3PIN0 (*(volatile unsigned char *)0x2009C074)
#define FIO3PIN0_OFFSET 0x74

#define FIO3PIN1 (*(volatile unsigned char *)0x2009C075)
#define FIO3PIN1_OFFSET 0x75

#define FIO3PIN2 (*(volatile unsigned char *)0x2009C076)
#define FIO3PIN2_OFFSET 0x76

#define FIO3PIN3 (*(volatile unsigned char *)0x2009C077)
#define FIO3PIN3_OFFSET 0x77

#define FIO3PINL (*(volatile unsigned short *)0x2009C074)
#define FIO3PINL_OFFSET 0x74

#define FIO3PINU (*(volatile unsigned short *)0x2009C076)
#define FIO3PINH FIO3PINU
#define FIO3PINU_OFFSET 0x76
#define FIO3PINH_OFFSET FIO3PINU_OFFSET

#define FIO4PIN (*(volatile unsigned long *)0x2009C094)
#define FIO4PIN_OFFSET 0x94

#define FIO4PIN0 (*(volatile unsigned char *)0x2009C094)
#define FIO4PIN0_OFFSET 0x94

#define FIO4PIN1 (*(volatile unsigned char *)0x2009C095)
#define FIO4PIN1_OFFSET 0x95

#define FIO4PIN2 (*(volatile unsigned char *)0x2009C096)
#define FIO4PIN2_OFFSET 0x96

#define FIO4PIN3 (*(volatile unsigned char *)0x2009C097)
#define FIO4PIN3_OFFSET 0x97

#define FIO4PINL (*(volatile unsigned short *)0x2009C094)
#define FIO4PINL_OFFSET 0x94

#define FIO4PINU (*(volatile unsigned short *)0x2009C096)
#define FIO4PINH FIO4PINU
#define FIO4PINU_OFFSET 0x96
#define FIO4PINH_OFFSET FIO4PINU_OFFSET

#define FIO0SET (*(volatile unsigned long *)0x2009C018)
#define FIO0SET_OFFSET 0x18

#define FIO0SET0 (*(volatile unsigned char *)0x2009C018)
#define FIO0SET0_OFFSET 0x18

#define FIO0SET1 (*(volatile unsigned char *)0x2009C019)
#define FIO0SET1_OFFSET 0x19

#define FIO0SET2 (*(volatile unsigned char *)0x2009C01A)
#define FIO0SET2_OFFSET 0x1A

#define FIO0SET3 (*(volatile unsigned char *)0x2009C01B)
#define FIO0SET3_OFFSET 0x1B

#define FIO0SETL (*(volatile unsigned short *)0x2009C018)
#define FIO0SETL_OFFSET 0x18

#define FIO0SETU (*(volatile unsigned short *)0x2009C01A)
#define FIO0SETH FIO0SETU
#define FIO0SETU_OFFSET 0x1A
#define FIO0SETH_OFFSET FIO0SETU_OFFSET

#define FIO1SET (*(volatile unsigned long *)0x2009C038)
#define FIO1SET_OFFSET 0x38

#define FIO1SET0 (*(volatile unsigned char *)0x2009C038)
#define FIO1SET0_OFFSET 0x38

#define FIO1SET1 (*(volatile unsigned char *)0x2009C039)
#define FIO1SET1_OFFSET 0x39

#define FIO1SET2 (*(volatile unsigned char *)0x2009C03A)
#define FIO1SET2_OFFSET 0x3A

#define FIO1SET3 (*(volatile unsigned char *)0x2009C03B)
#define FIO1SET3_OFFSET 0x3B

#define FIO1SETL (*(volatile unsigned short *)0x2009C038)
#define FIO1SETL_OFFSET 0x38

#define FIO1SETU (*(volatile unsigned short *)0x2009C03A)
#define FIO1SETH FIO1SETU
#define FIO1SETU_OFFSET 0x3A
#define FIO1SETH_OFFSET FIO1SETU_OFFSET

#define FIO2SET (*(volatile unsigned long *)0x2009C058)
#define FIO2SET_OFFSET 0x58

#define FIO2SET0 (*(volatile unsigned char *)0x2009C058)
#define FIO2SET0_OFFSET 0x58

#define FIO2SET1 (*(volatile unsigned char *)0x2009C059)
#define FIO2SET1_OFFSET 0x59

#define FIO2SET2 (*(volatile unsigned char *)0x2009C05A)
#define FIO2SET2_OFFSET 0x5A

#define FIO2SET3 (*(volatile unsigned char *)0x2009C05B)
#define FIO2SET3_OFFSET 0x5B

#define FIO2SETL (*(volatile unsigned short *)0x2009C058)
#define FIO2SETL_OFFSET 0x58

#define FIO2SETU (*(volatile unsigned short *)0x2009C05A)
#define FIO2SETH FIO2SETU
#define FIO2SETU_OFFSET 0x5A
#define FIO2SETH_OFFSET FIO2SETU_OFFSET

#define FIO3SET (*(volatile unsigned long *)0x2009C078)
#define FIO3SET_OFFSET 0x78

#define FIO3SET0 (*(volatile unsigned char *)0x2009C078)
#define FIO3SET0_OFFSET 0x78

#define FIO3SET1 (*(volatile unsigned char *)0x2009C079)
#define FIO3SET1_OFFSET 0x79

#define FIO3SET2 (*(volatile unsigned char *)0x2009C07A)
#define FIO3SET2_OFFSET 0x7A

#define FIO3SET3 (*(volatile unsigned char *)0x2009C07B)
#define FIO3SET3_OFFSET 0x7B

#define FIO3SETL (*(volatile unsigned short *)0x2009C078)
#define FIO3SETL_OFFSET 0x78

#define FIO3SETU (*(volatile unsigned short *)0x2009C07A)
#define FIO3SETH FIO3SETU
#define FIO3SETU_OFFSET 0x7A
#define FIO3SETH_OFFSET FIO3SETU_OFFSET

#define FIO4SET (*(volatile unsigned long *)0x2009C098)
#define FIO4SET_OFFSET 0x98

#define FIO4SET0 (*(volatile unsigned char *)0x2009C098)
#define FIO4SET0_OFFSET 0x98

#define FIO4SET1 (*(volatile unsigned char *)0x2009C099)
#define FIO4SET1_OFFSET 0x99

#define FIO4SET2 (*(volatile unsigned char *)0x2009C09A)
#define FIO4SET2_OFFSET 0x9A

#define FIO4SET3 (*(volatile unsigned char *)0x2009C09B)
#define FIO4SET3_OFFSET 0x9B

#define FIO4SETL (*(volatile unsigned short *)0x2009C098)
#define FIO4SETL_OFFSET 0x98

#define FIO4SETU (*(volatile unsigned short *)0x2009C09A)
#define FIO4SETH FIO4SETU
#define FIO4SETU_OFFSET 0x9A
#define FIO4SETH_OFFSET FIO4SETU_OFFSET

#define FIO0CLR (*(volatile unsigned long *)0x2009C01C)
#define FIO0CLR_OFFSET 0x1C

#define FIO0CLR0 (*(volatile unsigned char *)0x2009C01C)
#define FIO0CLR0_OFFSET 0x1C

#define FIO0CLR1 (*(volatile unsigned char *)0x2009C01D)
#define FIO0CLR1_OFFSET 0x1D

#define FIO0CLR2 (*(volatile unsigned char *)0x2009C01E)
#define FIO0CLR2_OFFSET 0x1E

#define FIO0CLR3 (*(volatile unsigned char *)0x2009C01F)
#define FIO0CLR3_OFFSET 0x1F

#define FIO0CLRL (*(volatile unsigned short *)0x2009C01C)
#define FIO0CLRL_OFFSET 0x1C

#define FIO0CLRU (*(volatile unsigned short *)0x2009C01E)
#define FIO0CLRH FIO0CLRU
#define FIO0CLRU_OFFSET 0x1E
#define FIO0CLRH_OFFSET FIO0CLRU_OFFSET

#define FIO1CLR (*(volatile unsigned long *)0x2009C03C)
#define FIO1CLR_OFFSET 0x3C

#define FIO1CLR0 (*(volatile unsigned char *)0x2009C03C)
#define FIO1CLR0_OFFSET 0x3C

#define FIO1CLR1 (*(volatile unsigned char *)0x2009C03D)
#define FIO1CLR1_OFFSET 0x3D

#define FIO1CLR2 (*(volatile unsigned char *)0x2009C03E)
#define FIO1CLR2_OFFSET 0x3E

#define FIO1CLR3 (*(volatile unsigned char *)0x2009C03F)
#define FIO1CLR3_OFFSET 0x3F

#define FIO1CLRL (*(volatile unsigned short *)0x2009C03C)
#define FIO1CLRL_OFFSET 0x3C

#define FIO1CLRU (*(volatile unsigned short *)0x2009C03E)
#define FIO1CLRH FIO1CLRU
#define FIO1CLRU_OFFSET 0x3E
#define FIO1CLRH_OFFSET FIO1CLRU_OFFSET

#define FIO2CLR (*(volatile unsigned long *)0x2009C05C)
#define FIO2CLR_OFFSET 0x5C

#define FIO2CLR0 (*(volatile unsigned char *)0x2009C05C)
#define FIO2CLR0_OFFSET 0x5C

#define FIO2CLR1 (*(volatile unsigned char *)0x2009C05D)
#define FIO2CLR1_OFFSET 0x5D

#define FIO2CLR2 (*(volatile unsigned char *)0x2009C05E)
#define FIO2CLR2_OFFSET 0x5E

#define FIO2CLR3 (*(volatile unsigned char *)0x2009C05F)
#define FIO2CLR3_OFFSET 0x5F

#define FIO2CLRL (*(volatile unsigned short *)0x2009C05C)
#define FIO2CLRL_OFFSET 0x5C

#define FIO2CLRU (*(volatile unsigned short *)0x2009C05E)
#define FIO2CLRH FIO2CLRU
#define FIO2CLRU_OFFSET 0x5E
#define FIO2CLRH_OFFSET FIO2CLRU_OFFSET

#define FIO3CLR (*(volatile unsigned long *)0x2009C07C)
#define FIO3CLR_OFFSET 0x7C

#define FIO3CLR0 (*(volatile unsigned char *)0x2009C07C)
#define FIO3CLR0_OFFSET 0x7C

#define FIO3CLR1 (*(volatile unsigned char *)0x2009C07D)
#define FIO3CLR1_OFFSET 0x7D

#define FIO3CLR2 (*(volatile unsigned char *)0x2009C07E)
#define FIO3CLR2_OFFSET 0x7E

#define FIO3CLR3 (*(volatile unsigned char *)0x2009C07F)
#define FIO3CLR3_OFFSET 0x7F

#define FIO3CLRL (*(volatile unsigned short *)0x2009C07C)
#define FIO3CLRL_OFFSET 0x7C

#define FIO3CLRU (*(volatile unsigned short *)0x2009C07E)
#define FIO3CLRH FIO3CLRU
#define FIO3CLRU_OFFSET 0x7E
#define FIO3CLRH_OFFSET FIO3CLRU_OFFSET

#define FIO4CLR (*(volatile unsigned long *)0x2009C09C)
#define FIO4CLR_OFFSET 0x9C

#define FIO4CLR0 (*(volatile unsigned char *)0x2009C09C)
#define FIO4CLR0_OFFSET 0x9C

#define FIO4CLR1 (*(volatile unsigned char *)0x2009C09D)
#define FIO4CLR1_OFFSET 0x9D

#define FIO4CLR2 (*(volatile unsigned char *)0x2009C09E)
#define FIO4CLR2_OFFSET 0x9E

#define FIO4CLR3 (*(volatile unsigned char *)0x2009C09F)
#define FIO4CLR3_OFFSET 0x9F

#define FIO4CLRL (*(volatile unsigned short *)0x2009C09C)
#define FIO4CLRL_OFFSET 0x9C

#define FIO4CLRU (*(volatile unsigned short *)0x2009C09E)
#define FIO4CLRH FIO4CLRU
#define FIO4CLRU_OFFSET 0x9E
#define FIO4CLRH_OFFSET FIO4CLRU_OFFSET

#define WDT_BASE_ADDRESS 0x40000000

#define WDMOD (*(volatile unsigned long *)0x40000000)
#define WDMOD_OFFSET 0x0
#define WDMOD_WDEN_MASK 0x1
#define WDMOD_WDEN 0x1
#define WDMOD_WDEN_BITBAND_ADDRESS 0x42000000
#define WDMOD_WDEN_BITBAND (*(volatile unsigned *)0x42000000)
#define WDMOD_WDEN_BIT 0
#define WDMOD_WDRESET_MASK 0x2
#define WDMOD_WDRESET 0x2
#define WDMOD_WDRESET_BITBAND_ADDRESS 0x42000004
#define WDMOD_WDRESET_BITBAND (*(volatile unsigned *)0x42000004)
#define WDMOD_WDRESET_BIT 1
#define WDMOD_WDTOF_MASK 0x4
#define WDMOD_WDTOF 0x4
#define WDMOD_WDTOF_BITBAND_ADDRESS 0x42000008
#define WDMOD_WDTOF_BITBAND (*(volatile unsigned *)0x42000008)
#define WDMOD_WDTOF_BIT 2
#define WDMOD_WDINT_MASK 0x8
#define WDMOD_WDINT 0x8
#define WDMOD_WDINT_BITBAND_ADDRESS 0x4200000C
#define WDMOD_WDINT_BITBAND (*(volatile unsigned *)0x4200000C)
#define WDMOD_WDINT_BIT 3

#define WDTC (*(volatile unsigned long *)0x40000004)
#define WDTC_OFFSET 0x4

#define WDFEED (*(volatile unsigned long *)0x40000008)
#define WDFEED_OFFSET 0x8

#define WDTV (*(volatile unsigned long *)0x4000000C)
#define WDTV_OFFSET 0xC

#define WDCLKSEL (*(volatile unsigned long *)0x40000010)
#define WDCLKSEL_OFFSET 0x10
#define WDCLKSEL_WDSEL_MASK 0x3
#define WDCLKSEL_WDSEL_BIT 0
#define WDCLKSEL_WDLOCK_MASK 0x80000000
#define WDCLKSEL_WDLOCK 0x80000000
#define WDCLKSEL_WDLOCK_BITBAND_ADDRESS 0x4200027C
#define WDCLKSEL_WDLOCK_BITBAND (*(volatile unsigned *)0x4200027C)
#define WDCLKSEL_WDLOCK_BIT 31

#define TIMER0_BASE_ADDRESS 0x40004000

#define T0IR (*(volatile unsigned char *)0x40004000)
#define T0IR_OFFSET 0x0
#define T0IR_MR0_MASK 0x1
#define T0IR_MR0 0x1
#define T0IR_MR0_BITBAND_ADDRESS 0x42080000
#define T0IR_MR0_BITBAND (*(volatile unsigned *)0x42080000)
#define T0IR_MR0_BIT 0
#define T0IR_MR1_MASK 0x2
#define T0IR_MR1 0x2
#define T0IR_MR1_BITBAND_ADDRESS 0x42080004
#define T0IR_MR1_BITBAND (*(volatile unsigned *)0x42080004)
#define T0IR_MR1_BIT 1
#define T0IR_MR2_MASK 0x4
#define T0IR_MR2 0x4
#define T0IR_MR2_BITBAND_ADDRESS 0x42080008
#define T0IR_MR2_BITBAND (*(volatile unsigned *)0x42080008)
#define T0IR_MR2_BIT 2
#define T0IR_MR3_MASK 0x8
#define T0IR_MR3 0x8
#define T0IR_MR3_BITBAND_ADDRESS 0x4208000C
#define T0IR_MR3_BITBAND (*(volatile unsigned *)0x4208000C)
#define T0IR_MR3_BIT 3
#define T0IR_CR0_MASK 0x10
#define T0IR_CR0 0x10
#define T0IR_CR0_BITBAND_ADDRESS 0x42080010
#define T0IR_CR0_BITBAND (*(volatile unsigned *)0x42080010)
#define T0IR_CR0_BIT 4
#define T0IR_CR1_MASK 0x20
#define T0IR_CR1 0x20
#define T0IR_CR1_BITBAND_ADDRESS 0x42080014
#define T0IR_CR1_BITBAND (*(volatile unsigned *)0x42080014)
#define T0IR_CR1_BIT 5

#define T0TCR (*(volatile unsigned char *)0x40004004)
#define T0TCR_OFFSET 0x4
#define T0TCR_Counter_Enable_MASK 0x1
#define T0TCR_Counter_Enable 0x1
#define T0TCR_Counter_Enable_BITBAND_ADDRESS 0x42080080
#define T0TCR_Counter_Enable_BITBAND (*(volatile unsigned *)0x42080080)
#define T0TCR_Counter_Enable_BIT 0
#define T0TCR_Counter_Reset_MASK 0x2
#define T0TCR_Counter_Reset 0x2
#define T0TCR_Counter_Reset_BITBAND_ADDRESS 0x42080084
#define T0TCR_Counter_Reset_BITBAND (*(volatile unsigned *)0x42080084)
#define T0TCR_Counter_Reset_BIT 1

#define T0TC (*(volatile unsigned long *)0x40004008)
#define T0TC_OFFSET 0x8

#define T0PR (*(volatile unsigned long *)0x4000400C)
#define T0PR_OFFSET 0xC

#define T0PC (*(volatile unsigned long *)0x40004010)
#define T0PC_OFFSET 0x10

#define T0MCR (*(volatile unsigned short *)0x40004014)
#define T0MCR_OFFSET 0x14
#define T0MCR_MR0I_MASK 0x1
#define T0MCR_MR0I 0x1
#define T0MCR_MR0I_BITBAND_ADDRESS 0x42080280
#define T0MCR_MR0I_BITBAND (*(volatile unsigned *)0x42080280)
#define T0MCR_MR0I_BIT 0
#define T0MCR_MR0R_MASK 0x2
#define T0MCR_MR0R 0x2
#define T0MCR_MR0R_BITBAND_ADDRESS 0x42080284
#define T0MCR_MR0R_BITBAND (*(volatile unsigned *)0x42080284)
#define T0MCR_MR0R_BIT 1
#define T0MCR_MR0S_MASK 0x4
#define T0MCR_MR0S 0x4
#define T0MCR_MR0S_BITBAND_ADDRESS 0x42080288
#define T0MCR_MR0S_BITBAND (*(volatile unsigned *)0x42080288)
#define T0MCR_MR0S_BIT 2
#define T0MCR_MR1I_MASK 0x8
#define T0MCR_MR1I 0x8
#define T0MCR_MR1I_BITBAND_ADDRESS 0x4208028C
#define T0MCR_MR1I_BITBAND (*(volatile unsigned *)0x4208028C)
#define T0MCR_MR1I_BIT 3
#define T0MCR_MR1R_MASK 0x10
#define T0MCR_MR1R 0x10
#define T0MCR_MR1R_BITBAND_ADDRESS 0x42080290
#define T0MCR_MR1R_BITBAND (*(volatile unsigned *)0x42080290)
#define T0MCR_MR1R_BIT 4
#define T0MCR_MR1S_MASK 0x20
#define T0MCR_MR1S 0x20
#define T0MCR_MR1S_BITBAND_ADDRESS 0x42080294
#define T0MCR_MR1S_BITBAND (*(volatile unsigned *)0x42080294)
#define T0MCR_MR1S_BIT 5
#define T0MCR_MR2I_MASK 0x40
#define T0MCR_MR2I 0x40
#define T0MCR_MR2I_BITBAND_ADDRESS 0x42080298
#define T0MCR_MR2I_BITBAND (*(volatile unsigned *)0x42080298)
#define T0MCR_MR2I_BIT 6
#define T0MCR_MR2R_MASK 0x80
#define T0MCR_MR2R 0x80
#define T0MCR_MR2R_BITBAND_ADDRESS 0x4208029C
#define T0MCR_MR2R_BITBAND (*(volatile unsigned *)0x4208029C)
#define T0MCR_MR2R_BIT 7
#define T0MCR_MR2S_MASK 0x100
#define T0MCR_MR2S 0x100
#define T0MCR_MR2S_BITBAND_ADDRESS 0x420802A0
#define T0MCR_MR2S_BITBAND (*(volatile unsigned *)0x420802A0)
#define T0MCR_MR2S_BIT 8
#define T0MCR_MR3I_MASK 0x200
#define T0MCR_MR3I 0x200
#define T0MCR_MR3I_BITBAND_ADDRESS 0x420802A4
#define T0MCR_MR3I_BITBAND (*(volatile unsigned *)0x420802A4)
#define T0MCR_MR3I_BIT 9
#define T0MCR_MR3R_MASK 0x400
#define T0MCR_MR3R 0x400
#define T0MCR_MR3R_BITBAND_ADDRESS 0x420802A8
#define T0MCR_MR3R_BITBAND (*(volatile unsigned *)0x420802A8)
#define T0MCR_MR3R_BIT 10
#define T0MCR_MR3S_MASK 0x800
#define T0MCR_MR3S 0x800
#define T0MCR_MR3S_BITBAND_ADDRESS 0x420802AC
#define T0MCR_MR3S_BITBAND (*(volatile unsigned *)0x420802AC)
#define T0MCR_MR3S_BIT 11

#define T0MR0 (*(volatile unsigned long *)0x40004018)
#define T0MR0_OFFSET 0x18

#define T0MR1 (*(volatile unsigned long *)0x4000401C)
#define T0MR1_OFFSET 0x1C

#define T0MR2 (*(volatile unsigned long *)0x40004020)
#define T0MR2_OFFSET 0x20

#define T0MR3 (*(volatile unsigned long *)0x40004024)
#define T0MR3_OFFSET 0x24

#define T0CCR (*(volatile unsigned short *)0x40004028)
#define T0CCR_OFFSET 0x28
#define T0CCR_CAP0RE_MASK 0x1
#define T0CCR_CAP0RE 0x1
#define T0CCR_CAP0RE_BITBAND_ADDRESS 0x42080500
#define T0CCR_CAP0RE_BITBAND (*(volatile unsigned *)0x42080500)
#define T0CCR_CAP0RE_BIT 0
#define T0CCR_CAP0FE_MASK 0x2
#define T0CCR_CAP0FE 0x2
#define T0CCR_CAP0FE_BITBAND_ADDRESS 0x42080504
#define T0CCR_CAP0FE_BITBAND (*(volatile unsigned *)0x42080504)
#define T0CCR_CAP0FE_BIT 1
#define T0CCR_CAP0I_MASK 0x4
#define T0CCR_CAP0I 0x4
#define T0CCR_CAP0I_BITBAND_ADDRESS 0x42080508
#define T0CCR_CAP0I_BITBAND (*(volatile unsigned *)0x42080508)
#define T0CCR_CAP0I_BIT 2
#define T0CCR_CAP1RE_MASK 0x8
#define T0CCR_CAP1RE 0x8
#define T0CCR_CAP1RE_BITBAND_ADDRESS 0x4208050C
#define T0CCR_CAP1RE_BITBAND (*(volatile unsigned *)0x4208050C)
#define T0CCR_CAP1RE_BIT 3
#define T0CCR_CAP1FE_MASK 0x10
#define T0CCR_CAP1FE 0x10
#define T0CCR_CAP1FE_BITBAND_ADDRESS 0x42080510
#define T0CCR_CAP1FE_BITBAND (*(volatile unsigned *)0x42080510)
#define T0CCR_CAP1FE_BIT 4
#define T0CCR_CAP1I_MASK 0x20
#define T0CCR_CAP1I 0x20
#define T0CCR_CAP1I_BITBAND_ADDRESS 0x42080514
#define T0CCR_CAP1I_BITBAND (*(volatile unsigned *)0x42080514)
#define T0CCR_CAP1I_BIT 5

#define T0CR0 (*(volatile unsigned long *)0x4000402C)
#define T0CR0_OFFSET 0x2C

#define T0CR1 (*(volatile unsigned long *)0x40004030)
#define T0CR1_OFFSET 0x30

#define T0EMR (*(volatile unsigned short *)0x4000403C)
#define T0EMR_OFFSET 0x3C
#define T0EMR_EM0_MASK 0x1
#define T0EMR_EM0 0x1
#define T0EMR_EM0_BITBAND_ADDRESS 0x42080780
#define T0EMR_EM0_BITBAND (*(volatile unsigned *)0x42080780)
#define T0EMR_EM0_BIT 0
#define T0EMR_EM1_MASK 0x2
#define T0EMR_EM1 0x2
#define T0EMR_EM1_BITBAND_ADDRESS 0x42080784
#define T0EMR_EM1_BITBAND (*(volatile unsigned *)0x42080784)
#define T0EMR_EM1_BIT 1
#define T0EMR_EM2_MASK 0x4
#define T0EMR_EM2 0x4
#define T0EMR_EM2_BITBAND_ADDRESS 0x42080788
#define T0EMR_EM2_BITBAND (*(volatile unsigned *)0x42080788)
#define T0EMR_EM2_BIT 2
#define T0EMR_EM3_MASK 0x8
#define T0EMR_EM3 0x8
#define T0EMR_EM3_BITBAND_ADDRESS 0x4208078C
#define T0EMR_EM3_BITBAND (*(volatile unsigned *)0x4208078C)
#define T0EMR_EM3_BIT 3
#define T0EMR_EMC0_MASK 0x30
#define T0EMR_EMC0_BIT 4
#define T0EMR_EMC1_MASK 0xC0
#define T0EMR_EMC1_BIT 6
#define T0EMR_EMC2_MASK 0x300
#define T0EMR_EMC2_BIT 8
#define T0EMR_EMC3_MASK 0xC00
#define T0EMR_EMC3_BIT 10

#define T0CTCR (*(volatile unsigned long *)0x40004070)
#define T0CTCR_OFFSET 0x70
#define T0CTCR_Counter_Timer_Mode_MASK 0x3
#define T0CTCR_Counter_Timer_Mode_BIT 0
#define T0CTCR_Count_Input_Select_MASK 0xC
#define T0CTCR_Count_Input_Select_BIT 2

#define TIMER1_BASE_ADDRESS 0x40008000

#define T1IR (*(volatile unsigned char *)0x40008000)
#define T1IR_OFFSET 0x0
#define T1IR_MR0_MASK 0x1
#define T1IR_MR0 0x1
#define T1IR_MR0_BITBAND_ADDRESS 0x42100000
#define T1IR_MR0_BITBAND (*(volatile unsigned *)0x42100000)
#define T1IR_MR0_BIT 0
#define T1IR_MR1_MASK 0x2
#define T1IR_MR1 0x2
#define T1IR_MR1_BITBAND_ADDRESS 0x42100004
#define T1IR_MR1_BITBAND (*(volatile unsigned *)0x42100004)
#define T1IR_MR1_BIT 1
#define T1IR_MR2_MASK 0x4
#define T1IR_MR2 0x4
#define T1IR_MR2_BITBAND_ADDRESS 0x42100008
#define T1IR_MR2_BITBAND (*(volatile unsigned *)0x42100008)
#define T1IR_MR2_BIT 2
#define T1IR_MR3_MASK 0x8
#define T1IR_MR3 0x8
#define T1IR_MR3_BITBAND_ADDRESS 0x4210000C
#define T1IR_MR3_BITBAND (*(volatile unsigned *)0x4210000C)
#define T1IR_MR3_BIT 3
#define T1IR_CR0_MASK 0x10
#define T1IR_CR0 0x10
#define T1IR_CR0_BITBAND_ADDRESS 0x42100010
#define T1IR_CR0_BITBAND (*(volatile unsigned *)0x42100010)
#define T1IR_CR0_BIT 4
#define T1IR_CR1_MASK 0x20
#define T1IR_CR1 0x20
#define T1IR_CR1_BITBAND_ADDRESS 0x42100014
#define T1IR_CR1_BITBAND (*(volatile unsigned *)0x42100014)
#define T1IR_CR1_BIT 5

#define T1TCR (*(volatile unsigned char *)0x40008004)
#define T1TCR_OFFSET 0x4
#define T1TCR_Counter_Enable_MASK 0x1
#define T1TCR_Counter_Enable 0x1
#define T1TCR_Counter_Enable_BITBAND_ADDRESS 0x42100080
#define T1TCR_Counter_Enable_BITBAND (*(volatile unsigned *)0x42100080)
#define T1TCR_Counter_Enable_BIT 0
#define T1TCR_Counter_Reset_MASK 0x2
#define T1TCR_Counter_Reset 0x2
#define T1TCR_Counter_Reset_BITBAND_ADDRESS 0x42100084
#define T1TCR_Counter_Reset_BITBAND (*(volatile unsigned *)0x42100084)
#define T1TCR_Counter_Reset_BIT 1

#define T1TC (*(volatile unsigned long *)0x40008008)
#define T1TC_OFFSET 0x8

#define T1PR (*(volatile unsigned long *)0x4000800C)
#define T1PR_OFFSET 0xC

#define T1PC (*(volatile unsigned long *)0x40008010)
#define T1PC_OFFSET 0x10

#define T1MCR (*(volatile unsigned short *)0x40008014)
#define T1MCR_OFFSET 0x14
#define T1MCR_MR0I_MASK 0x1
#define T1MCR_MR0I 0x1
#define T1MCR_MR0I_BITBAND_ADDRESS 0x42100280
#define T1MCR_MR0I_BITBAND (*(volatile unsigned *)0x42100280)
#define T1MCR_MR0I_BIT 0
#define T1MCR_MR0R_MASK 0x2
#define T1MCR_MR0R 0x2
#define T1MCR_MR0R_BITBAND_ADDRESS 0x42100284
#define T1MCR_MR0R_BITBAND (*(volatile unsigned *)0x42100284)
#define T1MCR_MR0R_BIT 1
#define T1MCR_MR0S_MASK 0x4
#define T1MCR_MR0S 0x4
#define T1MCR_MR0S_BITBAND_ADDRESS 0x42100288
#define T1MCR_MR0S_BITBAND (*(volatile unsigned *)0x42100288)
#define T1MCR_MR0S_BIT 2
#define T1MCR_MR1I_MASK 0x8
#define T1MCR_MR1I 0x8
#define T1MCR_MR1I_BITBAND_ADDRESS 0x4210028C
#define T1MCR_MR1I_BITBAND (*(volatile unsigned *)0x4210028C)
#define T1MCR_MR1I_BIT 3
#define T1MCR_MR1R_MASK 0x10
#define T1MCR_MR1R 0x10
#define T1MCR_MR1R_BITBAND_ADDRESS 0x42100290
#define T1MCR_MR1R_BITBAND (*(volatile unsigned *)0x42100290)
#define T1MCR_MR1R_BIT 4
#define T1MCR_MR1S_MASK 0x20
#define T1MCR_MR1S 0x20
#define T1MCR_MR1S_BITBAND_ADDRESS 0x42100294
#define T1MCR_MR1S_BITBAND (*(volatile unsigned *)0x42100294)
#define T1MCR_MR1S_BIT 5
#define T1MCR_MR2I_MASK 0x40
#define T1MCR_MR2I 0x40
#define T1MCR_MR2I_BITBAND_ADDRESS 0x42100298
#define T1MCR_MR2I_BITBAND (*(volatile unsigned *)0x42100298)
#define T1MCR_MR2I_BIT 6
#define T1MCR_MR2R_MASK 0x80
#define T1MCR_MR2R 0x80
#define T1MCR_MR2R_BITBAND_ADDRESS 0x4210029C
#define T1MCR_MR2R_BITBAND (*(volatile unsigned *)0x4210029C)
#define T1MCR_MR2R_BIT 7
#define T1MCR_MR2S_MASK 0x100
#define T1MCR_MR2S 0x100
#define T1MCR_MR2S_BITBAND_ADDRESS 0x421002A0
#define T1MCR_MR2S_BITBAND (*(volatile unsigned *)0x421002A0)
#define T1MCR_MR2S_BIT 8
#define T1MCR_MR3I_MASK 0x200
#define T1MCR_MR3I 0x200
#define T1MCR_MR3I_BITBAND_ADDRESS 0x421002A4
#define T1MCR_MR3I_BITBAND (*(volatile unsigned *)0x421002A4)
#define T1MCR_MR3I_BIT 9
#define T1MCR_MR3R_MASK 0x400
#define T1MCR_MR3R 0x400
#define T1MCR_MR3R_BITBAND_ADDRESS 0x421002A8
#define T1MCR_MR3R_BITBAND (*(volatile unsigned *)0x421002A8)
#define T1MCR_MR3R_BIT 10
#define T1MCR_MR3S_MASK 0x800
#define T1MCR_MR3S 0x800
#define T1MCR_MR3S_BITBAND_ADDRESS 0x421002AC
#define T1MCR_MR3S_BITBAND (*(volatile unsigned *)0x421002AC)
#define T1MCR_MR3S_BIT 11

#define T1MR0 (*(volatile unsigned long *)0x40008018)
#define T1MR0_OFFSET 0x18

#define T1MR1 (*(volatile unsigned long *)0x4000801C)
#define T1MR1_OFFSET 0x1C

#define T1MR2 (*(volatile unsigned long *)0x40008020)
#define T1MR2_OFFSET 0x20

#define T1MR3 (*(volatile unsigned long *)0x40008024)
#define T1MR3_OFFSET 0x24

#define T1CCR (*(volatile unsigned short *)0x40008028)
#define T1CCR_OFFSET 0x28
#define T1CCR_CAP0RE_MASK 0x1
#define T1CCR_CAP0RE 0x1
#define T1CCR_CAP0RE_BITBAND_ADDRESS 0x42100500
#define T1CCR_CAP0RE_BITBAND (*(volatile unsigned *)0x42100500)
#define T1CCR_CAP0RE_BIT 0
#define T1CCR_CAP0FE_MASK 0x2
#define T1CCR_CAP0FE 0x2
#define T1CCR_CAP0FE_BITBAND_ADDRESS 0x42100504
#define T1CCR_CAP0FE_BITBAND (*(volatile unsigned *)0x42100504)
#define T1CCR_CAP0FE_BIT 1
#define T1CCR_CAP0I_MASK 0x4
#define T1CCR_CAP0I 0x4
#define T1CCR_CAP0I_BITBAND_ADDRESS 0x42100508
#define T1CCR_CAP0I_BITBAND (*(volatile unsigned *)0x42100508)
#define T1CCR_CAP0I_BIT 2
#define T1CCR_CAP1RE_MASK 0x8
#define T1CCR_CAP1RE 0x8
#define T1CCR_CAP1RE_BITBAND_ADDRESS 0x4210050C
#define T1CCR_CAP1RE_BITBAND (*(volatile unsigned *)0x4210050C)
#define T1CCR_CAP1RE_BIT 3
#define T1CCR_CAP1FE_MASK 0x10
#define T1CCR_CAP1FE 0x10
#define T1CCR_CAP1FE_BITBAND_ADDRESS 0x42100510
#define T1CCR_CAP1FE_BITBAND (*(volatile unsigned *)0x42100510)
#define T1CCR_CAP1FE_BIT 4
#define T1CCR_CAP1I_MASK 0x20
#define T1CCR_CAP1I 0x20
#define T1CCR_CAP1I_BITBAND_ADDRESS 0x42100514
#define T1CCR_CAP1I_BITBAND (*(volatile unsigned *)0x42100514)
#define T1CCR_CAP1I_BIT 5

#define T1CR0 (*(volatile unsigned long *)0x4000802C)
#define T1CR0_OFFSET 0x2C

#define T1CR1 (*(volatile unsigned long *)0x40008030)
#define T1CR1_OFFSET 0x30

#define T1EMR (*(volatile unsigned short *)0x4000803C)
#define T1EMR_OFFSET 0x3C
#define T1EMR_EM0_MASK 0x1
#define T1EMR_EM0 0x1
#define T1EMR_EM0_BITBAND_ADDRESS 0x42100780
#define T1EMR_EM0_BITBAND (*(volatile unsigned *)0x42100780)
#define T1EMR_EM0_BIT 0
#define T1EMR_EM1_MASK 0x2
#define T1EMR_EM1 0x2
#define T1EMR_EM1_BITBAND_ADDRESS 0x42100784
#define T1EMR_EM1_BITBAND (*(volatile unsigned *)0x42100784)
#define T1EMR_EM1_BIT 1
#define T1EMR_EM2_MASK 0x4
#define T1EMR_EM2 0x4
#define T1EMR_EM2_BITBAND_ADDRESS 0x42100788
#define T1EMR_EM2_BITBAND (*(volatile unsigned *)0x42100788)
#define T1EMR_EM2_BIT 2
#define T1EMR_EM3_MASK 0x8
#define T1EMR_EM3 0x8
#define T1EMR_EM3_BITBAND_ADDRESS 0x4210078C
#define T1EMR_EM3_BITBAND (*(volatile unsigned *)0x4210078C)
#define T1EMR_EM3_BIT 3
#define T1EMR_EMC0_MASK 0x30
#define T1EMR_EMC0_BIT 4
#define T1EMR_EMC1_MASK 0xC0
#define T1EMR_EMC1_BIT 6
#define T1EMR_EMC2_MASK 0x300
#define T1EMR_EMC2_BIT 8
#define T1EMR_EMC3_MASK 0xC00
#define T1EMR_EMC3_BIT 10

#define T1CTCR (*(volatile unsigned long *)0x40008070)
#define T1CTCR_OFFSET 0x70
#define T1CTCR_Counter_Timer_Mode_MASK 0x3
#define T1CTCR_Counter_Timer_Mode_BIT 0
#define T1CTCR_Count_Input_Select_MASK 0xC
#define T1CTCR_Count_Input_Select_BIT 2

#define UART0_BASE_ADDRESS 0x4000C000

#define U0RBR (*(volatile unsigned char *)0x4000C000)
#define U0RBR_OFFSET 0x0

#define U0THR (*(volatile unsigned char *)0x4000C000)
#define U0THR_OFFSET 0x0

#define U0DLL (*(volatile unsigned char *)0x4000C000)
#define U0DLL_OFFSET 0x0

#define U0DLM (*(volatile unsigned char *)0x4000C004)
#define U0DLM_OFFSET 0x4

#define U0IER (*(volatile unsigned long *)0x4000C004)
#define U0IER_OFFSET 0x4
#define U0IER_RBR_Interrupt_Enable_MASK 0x1
#define U0IER_RBR_Interrupt_Enable 0x1
#define U0IER_RBR_Interrupt_Enable_BITBAND_ADDRESS 0x42180080
#define U0IER_RBR_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x42180080)
#define U0IER_RBR_Interrupt_Enable_BIT 0
#define U0IER_THRE_Interrupt_Enable_MASK 0x2
#define U0IER_THRE_Interrupt_Enable 0x2
#define U0IER_THRE_Interrupt_Enable_BITBAND_ADDRESS 0x42180084
#define U0IER_THRE_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x42180084)
#define U0IER_THRE_Interrupt_Enable_BIT 1
#define U0IER_Rx_Line_Status_Interrupt_Enable_MASK 0x4
#define U0IER_Rx_Line_Status_Interrupt_Enable 0x4
#define U0IER_Rx_Line_Status_Interrupt_Enable_BITBAND_ADDRESS 0x42180088
#define U0IER_Rx_Line_Status_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x42180088)
#define U0IER_Rx_Line_Status_Interrupt_Enable_BIT 2
#define U0IER_ABEOIntEn_MASK 0x100
#define U0IER_ABEOIntEn 0x100
#define U0IER_ABEOIntEn_BITBAND_ADDRESS 0x421800A0
#define U0IER_ABEOIntEn_BITBAND (*(volatile unsigned *)0x421800A0)
#define U0IER_ABEOIntEn_BIT 8
#define U0IER_ABTOIntEn_MASK 0x200
#define U0IER_ABTOIntEn 0x200
#define U0IER_ABTOIntEn_BITBAND_ADDRESS 0x421800A4
#define U0IER_ABTOIntEn_BITBAND (*(volatile unsigned *)0x421800A4)
#define U0IER_ABTOIntEn_BIT 9

#define U0IIR (*(volatile unsigned long *)0x4000C008)
#define U0IIR_OFFSET 0x8
#define U0IIR_IntStatus_MASK 0x1
#define U0IIR_IntStatus 0x1
#define U0IIR_IntStatus_BITBAND_ADDRESS 0x42180100
#define U0IIR_IntStatus_BITBAND (*(volatile unsigned *)0x42180100)
#define U0IIR_IntStatus_BIT 0
#define U0IIR_IntId_MASK 0xE
#define U0IIR_IntId_BIT 1
#define U0IIR_FIFO_Enable_MASK 0xC0
#define U0IIR_FIFO_Enable_BIT 6
#define U0IIR_ABEOInt_MASK 0x100
#define U0IIR_ABEOInt 0x100
#define U0IIR_ABEOInt_BITBAND_ADDRESS 0x42180120
#define U0IIR_ABEOInt_BITBAND (*(volatile unsigned *)0x42180120)
#define U0IIR_ABEOInt_BIT 8
#define U0IIR_ABTOInt_MASK 0x200
#define U0IIR_ABTOInt 0x200
#define U0IIR_ABTOInt_BITBAND_ADDRESS 0x42180124
#define U0IIR_ABTOInt_BITBAND (*(volatile unsigned *)0x42180124)
#define U0IIR_ABTOInt_BIT 9

#define U0FCR (*(volatile unsigned char *)0x4000C008)
#define U0FCR_OFFSET 0x8
#define U0FCR_FIFO_Enable_MASK 0x1
#define U0FCR_FIFO_Enable 0x1
#define U0FCR_FIFO_Enable_BITBAND_ADDRESS 0x42180100
#define U0FCR_FIFO_Enable_BITBAND (*(volatile unsigned *)0x42180100)
#define U0FCR_FIFO_Enable_BIT 0
#define U0FCR_Rx_FIFO_Reset_MASK 0x2
#define U0FCR_Rx_FIFO_Reset 0x2
#define U0FCR_Rx_FIFO_Reset_BITBAND_ADDRESS 0x42180104
#define U0FCR_Rx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x42180104)
#define U0FCR_Rx_FIFO_Reset_BIT 1
#define U0FCR_Tx_FIFO_Reset_MASK 0x4
#define U0FCR_Tx_FIFO_Reset 0x4
#define U0FCR_Tx_FIFO_Reset_BITBAND_ADDRESS 0x42180108
#define U0FCR_Tx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x42180108)
#define U0FCR_Tx_FIFO_Reset_BIT 2
#define U0FCR_DMA_Mode_Select_MASK 0x8
#define U0FCR_DMA_Mode_Select 0x8
#define U0FCR_DMA_Mode_Select_BITBAND_ADDRESS 0x4218010C
#define U0FCR_DMA_Mode_Select_BITBAND (*(volatile unsigned *)0x4218010C)
#define U0FCR_DMA_Mode_Select_BIT 3
#define U0FCR_Rx_Trigger_Level_Select_MASK 0xC0
#define U0FCR_Rx_Trigger_Level_Select_BIT 6

#define U0LCR (*(volatile unsigned char *)0x4000C00C)
#define U0LCR_OFFSET 0xC
#define U0LCR_Word_Length_Select_MASK 0x3
#define U0LCR_Word_Length_Select_BIT 0
#define U0LCR_Stop_Bit_Select_MASK 0x4
#define U0LCR_Stop_Bit_Select 0x4
#define U0LCR_Stop_Bit_Select_BITBAND_ADDRESS 0x42180188
#define U0LCR_Stop_Bit_Select_BITBAND (*(volatile unsigned *)0x42180188)
#define U0LCR_Stop_Bit_Select_BIT 2
#define U0LCR_Parity_Enable_MASK 0x8
#define U0LCR_Parity_Enable 0x8
#define U0LCR_Parity_Enable_BITBAND_ADDRESS 0x4218018C
#define U0LCR_Parity_Enable_BITBAND (*(volatile unsigned *)0x4218018C)
#define U0LCR_Parity_Enable_BIT 3
#define U0LCR_Parity_Select_MASK 0x30
#define U0LCR_Parity_Select_BIT 4
#define U0LCR_Break_Control_MASK 0x40
#define U0LCR_Break_Control 0x40
#define U0LCR_Break_Control_BITBAND_ADDRESS 0x42180198
#define U0LCR_Break_Control_BITBAND (*(volatile unsigned *)0x42180198)
#define U0LCR_Break_Control_BIT 6
#define U0LCR_Divisor_Latch_Access_Bit_MASK 0x80
#define U0LCR_Divisor_Latch_Access_Bit 0x80
#define U0LCR_Divisor_Latch_Access_Bit_BITBAND_ADDRESS 0x4218019C
#define U0LCR_Divisor_Latch_Access_Bit_BITBAND (*(volatile unsigned *)0x4218019C)
#define U0LCR_Divisor_Latch_Access_Bit_BIT 7

#define U0LSR (*(volatile unsigned char *)0x4000C014)
#define U0LSR_OFFSET 0x14
#define U0LSR_RDR_MASK 0x1
#define U0LSR_RDR 0x1
#define U0LSR_RDR_BITBAND_ADDRESS 0x42180280
#define U0LSR_RDR_BITBAND (*(volatile unsigned *)0x42180280)
#define U0LSR_RDR_BIT 0
#define U0LSR_OE_MASK 0x2
#define U0LSR_OE 0x2
#define U0LSR_OE_BITBAND_ADDRESS 0x42180284
#define U0LSR_OE_BITBAND (*(volatile unsigned *)0x42180284)
#define U0LSR_OE_BIT 1
#define U0LSR_PE_MASK 0x4
#define U0LSR_PE 0x4
#define U0LSR_PE_BITBAND_ADDRESS 0x42180288
#define U0LSR_PE_BITBAND (*(volatile unsigned *)0x42180288)
#define U0LSR_PE_BIT 2
#define U0LSR_FE_MASK 0x8
#define U0LSR_FE 0x8
#define U0LSR_FE_BITBAND_ADDRESS 0x4218028C
#define U0LSR_FE_BITBAND (*(volatile unsigned *)0x4218028C)
#define U0LSR_FE_BIT 3
#define U0LSR_BI_MASK 0x10
#define U0LSR_BI 0x10
#define U0LSR_BI_BITBAND_ADDRESS 0x42180290
#define U0LSR_BI_BITBAND (*(volatile unsigned *)0x42180290)
#define U0LSR_BI_BIT 4
#define U0LSR_THRE_MASK 0x20
#define U0LSR_THRE 0x20
#define U0LSR_THRE_BITBAND_ADDRESS 0x42180294
#define U0LSR_THRE_BITBAND (*(volatile unsigned *)0x42180294)
#define U0LSR_THRE_BIT 5
#define U0LSR_TEMT_MASK 0x40
#define U0LSR_TEMT 0x40
#define U0LSR_TEMT_BITBAND_ADDRESS 0x42180298
#define U0LSR_TEMT_BITBAND (*(volatile unsigned *)0x42180298)
#define U0LSR_TEMT_BIT 6
#define U0LSR_RXFE_MASK 0x80
#define U0LSR_RXFE 0x80
#define U0LSR_RXFE_BITBAND_ADDRESS 0x4218029C
#define U0LSR_RXFE_BITBAND (*(volatile unsigned *)0x4218029C)
#define U0LSR_RXFE_BIT 7

#define U0SCR (*(volatile unsigned char *)0x4000C01C)
#define U0SCR_OFFSET 0x1C

#define U0ACR (*(volatile unsigned long *)0x4000C020)
#define U0ACR_OFFSET 0x20
#define U0ACR_Start_MASK 0x1
#define U0ACR_Start 0x1
#define U0ACR_Start_BITBAND_ADDRESS 0x42180400
#define U0ACR_Start_BITBAND (*(volatile unsigned *)0x42180400)
#define U0ACR_Start_BIT 0
#define U0ACR_Mode_MASK 0x2
#define U0ACR_Mode 0x2
#define U0ACR_Mode_BITBAND_ADDRESS 0x42180404
#define U0ACR_Mode_BITBAND (*(volatile unsigned *)0x42180404)
#define U0ACR_Mode_BIT 1
#define U0ACR_AutoRestart_MASK 0x4
#define U0ACR_AutoRestart 0x4
#define U0ACR_AutoRestart_BITBAND_ADDRESS 0x42180408
#define U0ACR_AutoRestart_BITBAND (*(volatile unsigned *)0x42180408)
#define U0ACR_AutoRestart_BIT 2
#define U0ACR_ABEOIntClr_MASK 0x100
#define U0ACR_ABEOIntClr 0x100
#define U0ACR_ABEOIntClr_BITBAND_ADDRESS 0x42180420
#define U0ACR_ABEOIntClr_BITBAND (*(volatile unsigned *)0x42180420)
#define U0ACR_ABEOIntClr_BIT 8
#define U0ACR_ABTOIntClr_MASK 0x200
#define U0ACR_ABTOIntClr 0x200
#define U0ACR_ABTOIntClr_BITBAND_ADDRESS 0x42180424
#define U0ACR_ABTOIntClr_BITBAND (*(volatile unsigned *)0x42180424)
#define U0ACR_ABTOIntClr_BIT 9

#define U0ICR (*(volatile unsigned long *)0x4000C024)
#define U0ICR_OFFSET 0x24
#define U0ICR_IrDAEn_MASK 0x1
#define U0ICR_IrDAEn 0x1
#define U0ICR_IrDAEn_BITBAND_ADDRESS 0x42180480
#define U0ICR_IrDAEn_BITBAND (*(volatile unsigned *)0x42180480)
#define U0ICR_IrDAEn_BIT 0
#define U0ICR_IrDAInv_MASK 0x2
#define U0ICR_IrDAInv 0x2
#define U0ICR_IrDAInv_BITBAND_ADDRESS 0x42180484
#define U0ICR_IrDAInv_BITBAND (*(volatile unsigned *)0x42180484)
#define U0ICR_IrDAInv_BIT 1
#define U0ICR_FixPulse_En_MASK 0x4
#define U0ICR_FixPulse_En 0x4
#define U0ICR_FixPulse_En_BITBAND_ADDRESS 0x42180488
#define U0ICR_FixPulse_En_BITBAND (*(volatile unsigned *)0x42180488)
#define U0ICR_FixPulse_En_BIT 2
#define U0ICR_PulseDiv_MASK 0x38
#define U0ICR_PulseDiv_BIT 3

#define U0FDR (*(volatile unsigned long *)0x4000C028)
#define U0FDR_OFFSET 0x28
#define U0FDR_DIVADDVAL_MASK 0xF
#define U0FDR_DIVADDVAL_BIT 0
#define U0FDR_MULVAL_MASK 0xF0
#define U0FDR_MULVAL_BIT 4

#define U0TER (*(volatile unsigned char *)0x4000C030)
#define U0TER_OFFSET 0x30
#define U0TER_TXEN_MASK 0x80
#define U0TER_TXEN 0x80
#define U0TER_TXEN_BITBAND_ADDRESS 0x4218061C
#define U0TER_TXEN_BITBAND (*(volatile unsigned *)0x4218061C)
#define U0TER_TXEN_BIT 7

#define U0FIFOLVL (*(volatile unsigned long *)0x4000C058)
#define U0FIFOLVL_OFFSET 0x58
#define U0FIFOLVL_RXFIFILVL_MASK 0xF
#define U0FIFOLVL_RXFIFILVL_BIT 0
#define U0FIFOLVL_TXFIFOLVL_MASK 0xF00
#define U0FIFOLVL_TXFIFOLVL_BIT 8

#define UART1_BASE_ADDRESS 0x40010000

#define U1RBR (*(volatile unsigned char *)0x40010000)
#define U1RBR_OFFSET 0x0

#define U1THR (*(volatile unsigned char *)0x40010000)
#define U1THR_OFFSET 0x0

#define U1DLL (*(volatile unsigned char *)0x40010000)
#define U1DLL_OFFSET 0x0

#define U1DLM (*(volatile unsigned char *)0x40010004)
#define U1DLM_OFFSET 0x4

#define U1IER (*(volatile unsigned long *)0x40010004)
#define U1IER_OFFSET 0x4
#define U1IER_RBR_Interrupt_Enable_MASK 0x1
#define U1IER_RBR_Interrupt_Enable 0x1
#define U1IER_RBR_Interrupt_Enable_BITBAND_ADDRESS 0x42200080
#define U1IER_RBR_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x42200080)
#define U1IER_RBR_Interrupt_Enable_BIT 0
#define U1IER_THRE_Interrupt_Enable_MASK 0x2
#define U1IER_THRE_Interrupt_Enable 0x2
#define U1IER_THRE_Interrupt_Enable_BITBAND_ADDRESS 0x42200084
#define U1IER_THRE_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x42200084)
#define U1IER_THRE_Interrupt_Enable_BIT 1
#define U1IER_Rx_Line_Status_Interrupt_Enable_MASK 0x4
#define U1IER_Rx_Line_Status_Interrupt_Enable 0x4
#define U1IER_Rx_Line_Status_Interrupt_Enable_BITBAND_ADDRESS 0x42200088
#define U1IER_Rx_Line_Status_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x42200088)
#define U1IER_Rx_Line_Status_Interrupt_Enable_BIT 2
#define U1IER_Modem_Status_Interrupt_Enable_MASK 0x8
#define U1IER_Modem_Status_Interrupt_Enable 0x8
#define U1IER_Modem_Status_Interrupt_Enable_BITBAND_ADDRESS 0x4220008C
#define U1IER_Modem_Status_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x4220008C)
#define U1IER_Modem_Status_Interrupt_Enable_BIT 3
#define U1IER_CTS_Interrupt_Enable_MASK 0x80
#define U1IER_CTS_Interrupt_Enable 0x80
#define U1IER_CTS_Interrupt_Enable_BITBAND_ADDRESS 0x4220009C
#define U1IER_CTS_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x4220009C)
#define U1IER_CTS_Interrupt_Enable_BIT 7
#define U1IER_ABEOIntEn_MASK 0x100
#define U1IER_ABEOIntEn 0x100
#define U1IER_ABEOIntEn_BITBAND_ADDRESS 0x422000A0
#define U1IER_ABEOIntEn_BITBAND (*(volatile unsigned *)0x422000A0)
#define U1IER_ABEOIntEn_BIT 8
#define U1IER_ABTOIntEn_MASK 0x200
#define U1IER_ABTOIntEn 0x200
#define U1IER_ABTOIntEn_BITBAND_ADDRESS 0x422000A4
#define U1IER_ABTOIntEn_BITBAND (*(volatile unsigned *)0x422000A4)
#define U1IER_ABTOIntEn_BIT 9

#define U1IIR (*(volatile unsigned long *)0x40010008)
#define U1IIR_OFFSET 0x8
#define U1IIR_IntStatus_MASK 0x1
#define U1IIR_IntStatus 0x1
#define U1IIR_IntStatus_BITBAND_ADDRESS 0x42200100
#define U1IIR_IntStatus_BITBAND (*(volatile unsigned *)0x42200100)
#define U1IIR_IntStatus_BIT 0
#define U1IIR_IntId_MASK 0xE
#define U1IIR_IntId_BIT 1
#define U1IIR_FIFO_Enable_MASK 0xC0
#define U1IIR_FIFO_Enable_BIT 6
#define U1IIR_ABEOInt_MASK 0x100
#define U1IIR_ABEOInt 0x100
#define U1IIR_ABEOInt_BITBAND_ADDRESS 0x42200120
#define U1IIR_ABEOInt_BITBAND (*(volatile unsigned *)0x42200120)
#define U1IIR_ABEOInt_BIT 8
#define U1IIR_ABTOInt_MASK 0x200
#define U1IIR_ABTOInt 0x200
#define U1IIR_ABTOInt_BITBAND_ADDRESS 0x42200124
#define U1IIR_ABTOInt_BITBAND (*(volatile unsigned *)0x42200124)
#define U1IIR_ABTOInt_BIT 9

#define U1FCR (*(volatile unsigned char *)0x40010008)
#define U1FCR_OFFSET 0x8
#define U1FCR_FIFO_Enable_MASK 0x1
#define U1FCR_FIFO_Enable 0x1
#define U1FCR_FIFO_Enable_BITBAND_ADDRESS 0x42200100
#define U1FCR_FIFO_Enable_BITBAND (*(volatile unsigned *)0x42200100)
#define U1FCR_FIFO_Enable_BIT 0
#define U1FCR_Rx_FIFO_Reset_MASK 0x2
#define U1FCR_Rx_FIFO_Reset 0x2
#define U1FCR_Rx_FIFO_Reset_BITBAND_ADDRESS 0x42200104
#define U1FCR_Rx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x42200104)
#define U1FCR_Rx_FIFO_Reset_BIT 1
#define U1FCR_Tx_FIFO_Reset_MASK 0x4
#define U1FCR_Tx_FIFO_Reset 0x4
#define U1FCR_Tx_FIFO_Reset_BITBAND_ADDRESS 0x42200108
#define U1FCR_Tx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x42200108)
#define U1FCR_Tx_FIFO_Reset_BIT 2
#define U1FCR_DMA_Mode_Select_MASK 0x8
#define U1FCR_DMA_Mode_Select 0x8
#define U1FCR_DMA_Mode_Select_BITBAND_ADDRESS 0x4220010C
#define U1FCR_DMA_Mode_Select_BITBAND (*(volatile unsigned *)0x4220010C)
#define U1FCR_DMA_Mode_Select_BIT 3
#define U1FCR_Rx_Trigger_Level_Select_MASK 0xC0
#define U1FCR_Rx_Trigger_Level_Select_BIT 6

#define U1LCR (*(volatile unsigned char *)0x4001000C)
#define U1LCR_OFFSET 0xC
#define U1LCR_Word_Length_Select_MASK 0x3
#define U1LCR_Word_Length_Select_BIT 0
#define U1LCR_Stop_Bit_Select_MASK 0x4
#define U1LCR_Stop_Bit_Select 0x4
#define U1LCR_Stop_Bit_Select_BITBAND_ADDRESS 0x42200188
#define U1LCR_Stop_Bit_Select_BITBAND (*(volatile unsigned *)0x42200188)
#define U1LCR_Stop_Bit_Select_BIT 2
#define U1LCR_Parity_Enable_MASK 0x8
#define U1LCR_Parity_Enable 0x8
#define U1LCR_Parity_Enable_BITBAND_ADDRESS 0x4220018C
#define U1LCR_Parity_Enable_BITBAND (*(volatile unsigned *)0x4220018C)
#define U1LCR_Parity_Enable_BIT 3
#define U1LCR_Parity_Select_MASK 0x30
#define U1LCR_Parity_Select_BIT 4
#define U1LCR_Break_Control_MASK 0x40
#define U1LCR_Break_Control 0x40
#define U1LCR_Break_Control_BITBAND_ADDRESS 0x42200198
#define U1LCR_Break_Control_BITBAND (*(volatile unsigned *)0x42200198)
#define U1LCR_Break_Control_BIT 6
#define U1LCR_Divisor_Latch_Access_Bit_MASK 0x80
#define U1LCR_Divisor_Latch_Access_Bit 0x80
#define U1LCR_Divisor_Latch_Access_Bit_BITBAND_ADDRESS 0x4220019C
#define U1LCR_Divisor_Latch_Access_Bit_BITBAND (*(volatile unsigned *)0x4220019C)
#define U1LCR_Divisor_Latch_Access_Bit_BIT 7

#define U1MCR (*(volatile unsigned char *)0x40010010)
#define U1MCR_OFFSET 0x10
#define U1MCR_DTR_Control_MASK 0x1
#define U1MCR_DTR_Control 0x1
#define U1MCR_DTR_Control_BITBAND_ADDRESS 0x42200200
#define U1MCR_DTR_Control_BITBAND (*(volatile unsigned *)0x42200200)
#define U1MCR_DTR_Control_BIT 0
#define U1MCR_RTS_Control_MASK 0x2
#define U1MCR_RTS_Control 0x2
#define U1MCR_RTS_Control_BITBAND_ADDRESS 0x42200204
#define U1MCR_RTS_Control_BITBAND (*(volatile unsigned *)0x42200204)
#define U1MCR_RTS_Control_BIT 1
#define U1MCR_Loopback_Mode_Select_MASK 0x10
#define U1MCR_Loopback_Mode_Select 0x10
#define U1MCR_Loopback_Mode_Select_BITBAND_ADDRESS 0x42200210
#define U1MCR_Loopback_Mode_Select_BITBAND (*(volatile unsigned *)0x42200210)
#define U1MCR_Loopback_Mode_Select_BIT 4
#define U1MCR_RTSen_MASK 0x40
#define U1MCR_RTSen 0x40
#define U1MCR_RTSen_BITBAND_ADDRESS 0x42200218
#define U1MCR_RTSen_BITBAND (*(volatile unsigned *)0x42200218)
#define U1MCR_RTSen_BIT 6
#define U1MCR_CTSen_MASK 0x80
#define U1MCR_CTSen 0x80
#define U1MCR_CTSen_BITBAND_ADDRESS 0x4220021C
#define U1MCR_CTSen_BITBAND (*(volatile unsigned *)0x4220021C)
#define U1MCR_CTSen_BIT 7

#define U1LSR (*(volatile unsigned char *)0x40010014)
#define U1LSR_OFFSET 0x14
#define U1LSR_RDR_MASK 0x1
#define U1LSR_RDR 0x1
#define U1LSR_RDR_BITBAND_ADDRESS 0x42200280
#define U1LSR_RDR_BITBAND (*(volatile unsigned *)0x42200280)
#define U1LSR_RDR_BIT 0
#define U1LSR_OE_MASK 0x2
#define U1LSR_OE 0x2
#define U1LSR_OE_BITBAND_ADDRESS 0x42200284
#define U1LSR_OE_BITBAND (*(volatile unsigned *)0x42200284)
#define U1LSR_OE_BIT 1
#define U1LSR_PE_MASK 0x4
#define U1LSR_PE 0x4
#define U1LSR_PE_BITBAND_ADDRESS 0x42200288
#define U1LSR_PE_BITBAND (*(volatile unsigned *)0x42200288)
#define U1LSR_PE_BIT 2
#define U1LSR_FE_MASK 0x8
#define U1LSR_FE 0x8
#define U1LSR_FE_BITBAND_ADDRESS 0x4220028C
#define U1LSR_FE_BITBAND (*(volatile unsigned *)0x4220028C)
#define U1LSR_FE_BIT 3
#define U1LSR_BI_MASK 0x10
#define U1LSR_BI 0x10
#define U1LSR_BI_BITBAND_ADDRESS 0x42200290
#define U1LSR_BI_BITBAND (*(volatile unsigned *)0x42200290)
#define U1LSR_BI_BIT 4
#define U1LSR_THRE_MASK 0x20
#define U1LSR_THRE 0x20
#define U1LSR_THRE_BITBAND_ADDRESS 0x42200294
#define U1LSR_THRE_BITBAND (*(volatile unsigned *)0x42200294)
#define U1LSR_THRE_BIT 5
#define U1LSR_TEMT_MASK 0x40
#define U1LSR_TEMT 0x40
#define U1LSR_TEMT_BITBAND_ADDRESS 0x42200298
#define U1LSR_TEMT_BITBAND (*(volatile unsigned *)0x42200298)
#define U1LSR_TEMT_BIT 6
#define U1LSR_RXFE_MASK 0x80
#define U1LSR_RXFE 0x80
#define U1LSR_RXFE_BITBAND_ADDRESS 0x4220029C
#define U1LSR_RXFE_BITBAND (*(volatile unsigned *)0x4220029C)
#define U1LSR_RXFE_BIT 7

#define U1MSR (*(volatile unsigned char *)0x40010018)
#define U1MSR_OFFSET 0x18
#define U1MSR_Delta_CTS_MASK 0x1
#define U1MSR_Delta_CTS 0x1
#define U1MSR_Delta_CTS_BITBAND_ADDRESS 0x42200300
#define U1MSR_Delta_CTS_BITBAND (*(volatile unsigned *)0x42200300)
#define U1MSR_Delta_CTS_BIT 0
#define U1MSR_Delta_DSR_MASK 0x2
#define U1MSR_Delta_DSR 0x2
#define U1MSR_Delta_DSR_BITBAND_ADDRESS 0x42200304
#define U1MSR_Delta_DSR_BITBAND (*(volatile unsigned *)0x42200304)
#define U1MSR_Delta_DSR_BIT 1
#define U1MSR_Trailing_Edge_RI_MASK 0x4
#define U1MSR_Trailing_Edge_RI 0x4
#define U1MSR_Trailing_Edge_RI_BITBAND_ADDRESS 0x42200308
#define U1MSR_Trailing_Edge_RI_BITBAND (*(volatile unsigned *)0x42200308)
#define U1MSR_Trailing_Edge_RI_BIT 2
#define U1MSR_Delta_DCD_MASK 0x8
#define U1MSR_Delta_DCD 0x8
#define U1MSR_Delta_DCD_BITBAND_ADDRESS 0x4220030C
#define U1MSR_Delta_DCD_BITBAND (*(volatile unsigned *)0x4220030C)
#define U1MSR_Delta_DCD_BIT 3
#define U1MSR_CTS_MASK 0x10
#define U1MSR_CTS 0x10
#define U1MSR_CTS_BITBAND_ADDRESS 0x42200310
#define U1MSR_CTS_BITBAND (*(volatile unsigned *)0x42200310)
#define U1MSR_CTS_BIT 4
#define U1MSR_DSR_MASK 0x20
#define U1MSR_DSR 0x20
#define U1MSR_DSR_BITBAND_ADDRESS 0x42200314
#define U1MSR_DSR_BITBAND (*(volatile unsigned *)0x42200314)
#define U1MSR_DSR_BIT 5
#define U1MSR_RI_MASK 0x40
#define U1MSR_RI 0x40
#define U1MSR_RI_BITBAND_ADDRESS 0x42200318
#define U1MSR_RI_BITBAND (*(volatile unsigned *)0x42200318)
#define U1MSR_RI_BIT 6
#define U1MSR_DCD_MASK 0x80
#define U1MSR_DCD 0x80
#define U1MSR_DCD_BITBAND_ADDRESS 0x4220031C
#define U1MSR_DCD_BITBAND (*(volatile unsigned *)0x4220031C)
#define U1MSR_DCD_BIT 7

#define U1SCR (*(volatile unsigned char *)0x4001001C)
#define U1SCR_OFFSET 0x1C

#define U1ACR (*(volatile unsigned long *)0x40010020)
#define U1ACR_OFFSET 0x20
#define U1ACR_Start_MASK 0x1
#define U1ACR_Start 0x1
#define U1ACR_Start_BITBAND_ADDRESS 0x42200400
#define U1ACR_Start_BITBAND (*(volatile unsigned *)0x42200400)
#define U1ACR_Start_BIT 0
#define U1ACR_Mode_MASK 0x2
#define U1ACR_Mode 0x2
#define U1ACR_Mode_BITBAND_ADDRESS 0x42200404
#define U1ACR_Mode_BITBAND (*(volatile unsigned *)0x42200404)
#define U1ACR_Mode_BIT 1
#define U1ACR_AutoRestart_MASK 0x4
#define U1ACR_AutoRestart 0x4
#define U1ACR_AutoRestart_BITBAND_ADDRESS 0x42200408
#define U1ACR_AutoRestart_BITBAND (*(volatile unsigned *)0x42200408)
#define U1ACR_AutoRestart_BIT 2
#define U1ACR_ABEOIntClr_MASK 0x100
#define U1ACR_ABEOIntClr 0x100
#define U1ACR_ABEOIntClr_BITBAND_ADDRESS 0x42200420
#define U1ACR_ABEOIntClr_BITBAND (*(volatile unsigned *)0x42200420)
#define U1ACR_ABEOIntClr_BIT 8
#define U1ACR_ABTOIntClr_MASK 0x200
#define U1ACR_ABTOIntClr 0x200
#define U1ACR_ABTOIntClr_BITBAND_ADDRESS 0x42200424
#define U1ACR_ABTOIntClr_BITBAND (*(volatile unsigned *)0x42200424)
#define U1ACR_ABTOIntClr_BIT 9

#define U1FDR (*(volatile unsigned long *)0x40010028)
#define U1FDR_OFFSET 0x28
#define U1FDR_DIVADDVAL_MASK 0xF
#define U1FDR_DIVADDVAL_BIT 0
#define U1FDR_MULVAL_MASK 0xF0
#define U1FDR_MULVAL_BIT 4

#define U1TER (*(volatile unsigned char *)0x40010030)
#define U1TER_OFFSET 0x30
#define U1TER_TXEN_MASK 0x80
#define U1TER_TXEN 0x80
#define U1TER_TXEN_BITBAND_ADDRESS 0x4220061C
#define U1TER_TXEN_BITBAND (*(volatile unsigned *)0x4220061C)
#define U1TER_TXEN_BIT 7

#define U1RS485CTRL (*(volatile unsigned long *)0x4001004C)
#define U1RS485CTRL_OFFSET 0x4C
#define U1RS485CTRL_NMMEN_MASK 0x1
#define U1RS485CTRL_NMMEN 0x1
#define U1RS485CTRL_NMMEN_BITBAND_ADDRESS 0x42200980
#define U1RS485CTRL_NMMEN_BITBAND (*(volatile unsigned *)0x42200980)
#define U1RS485CTRL_NMMEN_BIT 0
#define U1RS485CTRL_RXDIS_MASK 0x2
#define U1RS485CTRL_RXDIS 0x2
#define U1RS485CTRL_RXDIS_BITBAND_ADDRESS 0x42200984
#define U1RS485CTRL_RXDIS_BITBAND (*(volatile unsigned *)0x42200984)
#define U1RS485CTRL_RXDIS_BIT 1
#define U1RS485CTRL_AADEN_MASK 0x4
#define U1RS485CTRL_AADEN 0x4
#define U1RS485CTRL_AADEN_BITBAND_ADDRESS 0x42200988
#define U1RS485CTRL_AADEN_BITBAND (*(volatile unsigned *)0x42200988)
#define U1RS485CTRL_AADEN_BIT 2
#define U1RS485CTRL_SEL_MASK 0x8
#define U1RS485CTRL_SEL 0x8
#define U1RS485CTRL_SEL_BITBAND_ADDRESS 0x4220098C
#define U1RS485CTRL_SEL_BITBAND (*(volatile unsigned *)0x4220098C)
#define U1RS485CTRL_SEL_BIT 3
#define U1RS485CTRL_DCTRL_MASK 0x10
#define U1RS485CTRL_DCTRL 0x10
#define U1RS485CTRL_DCTRL_BITBAND_ADDRESS 0x42200990
#define U1RS485CTRL_DCTRL_BITBAND (*(volatile unsigned *)0x42200990)
#define U1RS485CTRL_DCTRL_BIT 4
#define U1RS485CTRL_OINV_MASK 0x20
#define U1RS485CTRL_OINV 0x20
#define U1RS485CTRL_OINV_BITBAND_ADDRESS 0x42200994
#define U1RS485CTRL_OINV_BITBAND (*(volatile unsigned *)0x42200994)
#define U1RS485CTRL_OINV_BIT 5

#define U1RS485ADRMATCH (*(volatile unsigned long *)0x40010050)
#define U1RS485ADRMATCH_OFFSET 0x50

#define U1RS485DLY (*(volatile unsigned long *)0x40010054)
#define U1RS485DLY_OFFSET 0x54

#define U1FIFOLVL (*(volatile unsigned long *)0x40010058)
#define U1FIFOLVL_OFFSET 0x58
#define U1FIFOLVL_RXFIFILVL_MASK 0xF
#define U1FIFOLVL_RXFIFILVL_BIT 0
#define U1FIFOLVL_TXFIFOLVL_MASK 0xF00
#define U1FIFOLVL_TXFIFOLVL_BIT 8

#define PWM1_BASE_ADDRESS 0x40018000

#define PWM1IR (*(volatile unsigned long *)0x40018000)
#define PWM1IR_OFFSET 0x0
#define PWM1IR_PWMMR0_Interrupt_MASK 0x1
#define PWM1IR_PWMMR0_Interrupt 0x1
#define PWM1IR_PWMMR0_Interrupt_BITBAND_ADDRESS 0x42300000
#define PWM1IR_PWMMR0_Interrupt_BITBAND (*(volatile unsigned *)0x42300000)
#define PWM1IR_PWMMR0_Interrupt_BIT 0
#define PWM1IR_PWMMR1_Interrupt_MASK 0x2
#define PWM1IR_PWMMR1_Interrupt 0x2
#define PWM1IR_PWMMR1_Interrupt_BITBAND_ADDRESS 0x42300004
#define PWM1IR_PWMMR1_Interrupt_BITBAND (*(volatile unsigned *)0x42300004)
#define PWM1IR_PWMMR1_Interrupt_BIT 1
#define PWM1IR_PWMMR2_Interrupt_MASK 0x4
#define PWM1IR_PWMMR2_Interrupt 0x4
#define PWM1IR_PWMMR2_Interrupt_BITBAND_ADDRESS 0x42300008
#define PWM1IR_PWMMR2_Interrupt_BITBAND (*(volatile unsigned *)0x42300008)
#define PWM1IR_PWMMR2_Interrupt_BIT 2
#define PWM1IR_PWMMR3_Interrupt_MASK 0x8
#define PWM1IR_PWMMR3_Interrupt 0x8
#define PWM1IR_PWMMR3_Interrupt_BITBAND_ADDRESS 0x4230000C
#define PWM1IR_PWMMR3_Interrupt_BITBAND (*(volatile unsigned *)0x4230000C)
#define PWM1IR_PWMMR3_Interrupt_BIT 3
#define PWM1IR_PWMCAP0_Interrupt_MASK 0x10
#define PWM1IR_PWMCAP0_Interrupt 0x10
#define PWM1IR_PWMCAP0_Interrupt_BITBAND_ADDRESS 0x42300010
#define PWM1IR_PWMCAP0_Interrupt_BITBAND (*(volatile unsigned *)0x42300010)
#define PWM1IR_PWMCAP0_Interrupt_BIT 4
#define PWM1IR_PWMCAP1_Interrupt_MASK 0x20
#define PWM1IR_PWMCAP1_Interrupt 0x20
#define PWM1IR_PWMCAP1_Interrupt_BITBAND_ADDRESS 0x42300014
#define PWM1IR_PWMCAP1_Interrupt_BITBAND (*(volatile unsigned *)0x42300014)
#define PWM1IR_PWMCAP1_Interrupt_BIT 5
#define PWM1IR_PWMMR4_Interrupt_MASK 0x100
#define PWM1IR_PWMMR4_Interrupt 0x100
#define PWM1IR_PWMMR4_Interrupt_BITBAND_ADDRESS 0x42300020
#define PWM1IR_PWMMR4_Interrupt_BITBAND (*(volatile unsigned *)0x42300020)
#define PWM1IR_PWMMR4_Interrupt_BIT 8
#define PWM1IR_PWMMR5_Interrupt_MASK 0x200
#define PWM1IR_PWMMR5_Interrupt 0x200
#define PWM1IR_PWMMR5_Interrupt_BITBAND_ADDRESS 0x42300024
#define PWM1IR_PWMMR5_Interrupt_BITBAND (*(volatile unsigned *)0x42300024)
#define PWM1IR_PWMMR5_Interrupt_BIT 9
#define PWM1IR_PWMMR6_Interrupt_MASK 0x400
#define PWM1IR_PWMMR6_Interrupt 0x400
#define PWM1IR_PWMMR6_Interrupt_BITBAND_ADDRESS 0x42300028
#define PWM1IR_PWMMR6_Interrupt_BITBAND (*(volatile unsigned *)0x42300028)
#define PWM1IR_PWMMR6_Interrupt_BIT 10

#define PWM1TCR (*(volatile unsigned long *)0x40018004)
#define PWM1TCR_OFFSET 0x4
#define PWM1TCR_Counter_Enable_MASK 0x1
#define PWM1TCR_Counter_Enable 0x1
#define PWM1TCR_Counter_Enable_BITBAND_ADDRESS 0x42300080
#define PWM1TCR_Counter_Enable_BITBAND (*(volatile unsigned *)0x42300080)
#define PWM1TCR_Counter_Enable_BIT 0
#define PWM1TCR_Counter_Reset_MASK 0x2
#define PWM1TCR_Counter_Reset 0x2
#define PWM1TCR_Counter_Reset_BITBAND_ADDRESS 0x42300084
#define PWM1TCR_Counter_Reset_BITBAND (*(volatile unsigned *)0x42300084)
#define PWM1TCR_Counter_Reset_BIT 1
#define PWM1TCR_PWM_Enable_MASK 0x8
#define PWM1TCR_PWM_Enable 0x8
#define PWM1TCR_PWM_Enable_BITBAND_ADDRESS 0x4230008C
#define PWM1TCR_PWM_Enable_BITBAND (*(volatile unsigned *)0x4230008C)
#define PWM1TCR_PWM_Enable_BIT 3

#define PWM1TC (*(volatile unsigned long *)0x40018008)
#define PWM1TC_OFFSET 0x8

#define PWM1PR (*(volatile unsigned long *)0x4001800C)
#define PWM1PR_OFFSET 0xC

#define PWM1PC (*(volatile unsigned long *)0x40018010)
#define PWM1PC_OFFSET 0x10

#define PWM1MCR (*(volatile unsigned long *)0x40018014)
#define PWM1MCR_OFFSET 0x14
#define PWM1MCR_PWMMR0I_MASK 0x1
#define PWM1MCR_PWMMR0I 0x1
#define PWM1MCR_PWMMR0I_BITBAND_ADDRESS 0x42300280
#define PWM1MCR_PWMMR0I_BITBAND (*(volatile unsigned *)0x42300280)
#define PWM1MCR_PWMMR0I_BIT 0
#define PWM1MCR_PWMMR0R_MASK 0x2
#define PWM1MCR_PWMMR0R 0x2
#define PWM1MCR_PWMMR0R_BITBAND_ADDRESS 0x42300284
#define PWM1MCR_PWMMR0R_BITBAND (*(volatile unsigned *)0x42300284)
#define PWM1MCR_PWMMR0R_BIT 1
#define PWM1MCR_PWMMR0S_MASK 0x4
#define PWM1MCR_PWMMR0S 0x4
#define PWM1MCR_PWMMR0S_BITBAND_ADDRESS 0x42300288
#define PWM1MCR_PWMMR0S_BITBAND (*(volatile unsigned *)0x42300288)
#define PWM1MCR_PWMMR0S_BIT 2
#define PWM1MCR_PWMMR1I_MASK 0x8
#define PWM1MCR_PWMMR1I 0x8
#define PWM1MCR_PWMMR1I_BITBAND_ADDRESS 0x4230028C
#define PWM1MCR_PWMMR1I_BITBAND (*(volatile unsigned *)0x4230028C)
#define PWM1MCR_PWMMR1I_BIT 3
#define PWM1MCR_PWMMR1R_MASK 0x10
#define PWM1MCR_PWMMR1R 0x10
#define PWM1MCR_PWMMR1R_BITBAND_ADDRESS 0x42300290
#define PWM1MCR_PWMMR1R_BITBAND (*(volatile unsigned *)0x42300290)
#define PWM1MCR_PWMMR1R_BIT 4
#define PWM1MCR_PWMMR1S_MASK 0x20
#define PWM1MCR_PWMMR1S 0x20
#define PWM1MCR_PWMMR1S_BITBAND_ADDRESS 0x42300294
#define PWM1MCR_PWMMR1S_BITBAND (*(volatile unsigned *)0x42300294)
#define PWM1MCR_PWMMR1S_BIT 5
#define PWM1MCR_PWMMR2I_MASK 0x40
#define PWM1MCR_PWMMR2I 0x40
#define PWM1MCR_PWMMR2I_BITBAND_ADDRESS 0x42300298
#define PWM1MCR_PWMMR2I_BITBAND (*(volatile unsigned *)0x42300298)
#define PWM1MCR_PWMMR2I_BIT 6
#define PWM1MCR_PWMMR2R_MASK 0x80
#define PWM1MCR_PWMMR2R 0x80
#define PWM1MCR_PWMMR2R_BITBAND_ADDRESS 0x4230029C
#define PWM1MCR_PWMMR2R_BITBAND (*(volatile unsigned *)0x4230029C)
#define PWM1MCR_PWMMR2R_BIT 7
#define PWM1MCR_PWMMR2S_MASK 0x100
#define PWM1MCR_PWMMR2S 0x100
#define PWM1MCR_PWMMR2S_BITBAND_ADDRESS 0x423002A0
#define PWM1MCR_PWMMR2S_BITBAND (*(volatile unsigned *)0x423002A0)
#define PWM1MCR_PWMMR2S_BIT 8
#define PWM1MCR_PWMMR3I_MASK 0x200
#define PWM1MCR_PWMMR3I 0x200
#define PWM1MCR_PWMMR3I_BITBAND_ADDRESS 0x423002A4
#define PWM1MCR_PWMMR3I_BITBAND (*(volatile unsigned *)0x423002A4)
#define PWM1MCR_PWMMR3I_BIT 9
#define PWM1MCR_PWMMR3R_MASK 0x400
#define PWM1MCR_PWMMR3R 0x400
#define PWM1MCR_PWMMR3R_BITBAND_ADDRESS 0x423002A8
#define PWM1MCR_PWMMR3R_BITBAND (*(volatile unsigned *)0x423002A8)
#define PWM1MCR_PWMMR3R_BIT 10
#define PWM1MCR_PWMMR3S_MASK 0x800
#define PWM1MCR_PWMMR3S 0x800
#define PWM1MCR_PWMMR3S_BITBAND_ADDRESS 0x423002AC
#define PWM1MCR_PWMMR3S_BITBAND (*(volatile unsigned *)0x423002AC)
#define PWM1MCR_PWMMR3S_BIT 11
#define PWM1MCR_PWMMR4I_MASK 0x1000
#define PWM1MCR_PWMMR4I 0x1000
#define PWM1MCR_PWMMR4I_BITBAND_ADDRESS 0x423002B0
#define PWM1MCR_PWMMR4I_BITBAND (*(volatile unsigned *)0x423002B0)
#define PWM1MCR_PWMMR4I_BIT 12
#define PWM1MCR_PWMMR4R_MASK 0x2000
#define PWM1MCR_PWMMR4R 0x2000
#define PWM1MCR_PWMMR4R_BITBAND_ADDRESS 0x423002B4
#define PWM1MCR_PWMMR4R_BITBAND (*(volatile unsigned *)0x423002B4)
#define PWM1MCR_PWMMR4R_BIT 13
#define PWM1MCR_PWMMR4S_MASK 0x4000
#define PWM1MCR_PWMMR4S 0x4000
#define PWM1MCR_PWMMR4S_BITBAND_ADDRESS 0x423002B8
#define PWM1MCR_PWMMR4S_BITBAND (*(volatile unsigned *)0x423002B8)
#define PWM1MCR_PWMMR4S_BIT 14
#define PWM1MCR_PWMMR5I_MASK 0x8000
#define PWM1MCR_PWMMR5I 0x8000
#define PWM1MCR_PWMMR5I_BITBAND_ADDRESS 0x423002BC
#define PWM1MCR_PWMMR5I_BITBAND (*(volatile unsigned *)0x423002BC)
#define PWM1MCR_PWMMR5I_BIT 15
#define PWM1MCR_PWMMR5R_MASK 0x10000
#define PWM1MCR_PWMMR5R 0x10000
#define PWM1MCR_PWMMR5R_BITBAND_ADDRESS 0x423002C0
#define PWM1MCR_PWMMR5R_BITBAND (*(volatile unsigned *)0x423002C0)
#define PWM1MCR_PWMMR5R_BIT 16
#define PWM1MCR_PWMMR5S_MASK 0x20000
#define PWM1MCR_PWMMR5S 0x20000
#define PWM1MCR_PWMMR5S_BITBAND_ADDRESS 0x423002C4
#define PWM1MCR_PWMMR5S_BITBAND (*(volatile unsigned *)0x423002C4)
#define PWM1MCR_PWMMR5S_BIT 17
#define PWM1MCR_PWMMR6I_MASK 0x40000
#define PWM1MCR_PWMMR6I 0x40000
#define PWM1MCR_PWMMR6I_BITBAND_ADDRESS 0x423002C8
#define PWM1MCR_PWMMR6I_BITBAND (*(volatile unsigned *)0x423002C8)
#define PWM1MCR_PWMMR6I_BIT 18
#define PWM1MCR_PWMMR6R_MASK 0x80000
#define PWM1MCR_PWMMR6R 0x80000
#define PWM1MCR_PWMMR6R_BITBAND_ADDRESS 0x423002CC
#define PWM1MCR_PWMMR6R_BITBAND (*(volatile unsigned *)0x423002CC)
#define PWM1MCR_PWMMR6R_BIT 19
#define PWM1MCR_PWMMR6S_MASK 0x100000
#define PWM1MCR_PWMMR6S 0x100000
#define PWM1MCR_PWMMR6S_BITBAND_ADDRESS 0x423002D0
#define PWM1MCR_PWMMR6S_BITBAND (*(volatile unsigned *)0x423002D0)
#define PWM1MCR_PWMMR6S_BIT 20

#define PWM1MR0 (*(volatile unsigned long *)0x40018018)
#define PWM1MR0_OFFSET 0x18

#define PWM1MR1 (*(volatile unsigned long *)0x4001801C)
#define PWM1MR1_OFFSET 0x1C

#define PWM1MR2 (*(volatile unsigned long *)0x40018020)
#define PWM1MR2_OFFSET 0x20

#define PWM1MR3 (*(volatile unsigned long *)0x40018024)
#define PWM1MR3_OFFSET 0x24

#define PWM1CCR (*(volatile unsigned long *)0x40018028)
#define PWM1CCR_OFFSET 0x28
#define PWM1CCR_Capture_on_CAPn_0_rising_edge_MASK 0x1
#define PWM1CCR_Capture_on_CAPn_0_rising_edge 0x1
#define PWM1CCR_Capture_on_CAPn_0_rising_edge_BITBAND_ADDRESS 0x42300500
#define PWM1CCR_Capture_on_CAPn_0_rising_edge_BITBAND (*(volatile unsigned *)0x42300500)
#define PWM1CCR_Capture_on_CAPn_0_rising_edge_BIT 0
#define PWM1CCR_Capture_on_CAPn_0_falling_edge_MASK 0x2
#define PWM1CCR_Capture_on_CAPn_0_falling_edge 0x2
#define PWM1CCR_Capture_on_CAPn_0_falling_edge_BITBAND_ADDRESS 0x42300504
#define PWM1CCR_Capture_on_CAPn_0_falling_edge_BITBAND (*(volatile unsigned *)0x42300504)
#define PWM1CCR_Capture_on_CAPn_0_falling_edge_BIT 1
#define PWM1CCR_Interrupt_on_CAPn_0_event_MASK 0x4
#define PWM1CCR_Interrupt_on_CAPn_0_event 0x4
#define PWM1CCR_Interrupt_on_CAPn_0_event_BITBAND_ADDRESS 0x42300508
#define PWM1CCR_Interrupt_on_CAPn_0_event_BITBAND (*(volatile unsigned *)0x42300508)
#define PWM1CCR_Interrupt_on_CAPn_0_event_BIT 2
#define PWM1CCR_Capture_on_CAPn_1_rising_edge_MASK 0x8
#define PWM1CCR_Capture_on_CAPn_1_rising_edge 0x8
#define PWM1CCR_Capture_on_CAPn_1_rising_edge_BITBAND_ADDRESS 0x4230050C
#define PWM1CCR_Capture_on_CAPn_1_rising_edge_BITBAND (*(volatile unsigned *)0x4230050C)
#define PWM1CCR_Capture_on_CAPn_1_rising_edge_BIT 3
#define PWM1CCR_Capture_on_CAPn_1_falling_edge_MASK 0x10
#define PWM1CCR_Capture_on_CAPn_1_falling_edge 0x10
#define PWM1CCR_Capture_on_CAPn_1_falling_edge_BITBAND_ADDRESS 0x42300510
#define PWM1CCR_Capture_on_CAPn_1_falling_edge_BITBAND (*(volatile unsigned *)0x42300510)
#define PWM1CCR_Capture_on_CAPn_1_falling_edge_BIT 4
#define PWM1CCR_Interrupt_on_CAPn_1_event_MASK 0x20
#define PWM1CCR_Interrupt_on_CAPn_1_event 0x20
#define PWM1CCR_Interrupt_on_CAPn_1_event_BITBAND_ADDRESS 0x42300514
#define PWM1CCR_Interrupt_on_CAPn_1_event_BITBAND (*(volatile unsigned *)0x42300514)
#define PWM1CCR_Interrupt_on_CAPn_1_event_BIT 5

#define PWM1CR0 (*(volatile unsigned long *)0x4001802C)
#define PWM1CR0_OFFSET 0x2C

#define PWM1CR1 (*(volatile unsigned long *)0x40018030)
#define PWM1CR1_OFFSET 0x30

#define PWM1CR2 (*(volatile unsigned long *)0x40018034)
#define PWM1CR2_OFFSET 0x34

#define PWM1CR3 (*(volatile unsigned long *)0x40018038)
#define PWM1CR3_OFFSET 0x38

#define PWM1MR4 (*(volatile unsigned long *)0x40018040)
#define PWM1MR4_OFFSET 0x40

#define PWM1MR5 (*(volatile unsigned long *)0x40018044)
#define PWM1MR5_OFFSET 0x44

#define PWM1MR6 (*(volatile unsigned long *)0x40018048)
#define PWM1MR6_OFFSET 0x48

#define PWM1PCR (*(volatile unsigned long *)0x4001804C)
#define PWM1PCR_OFFSET 0x4C
#define PWM1PCR_PWMSEL2_MASK 0x4
#define PWM1PCR_PWMSEL2 0x4
#define PWM1PCR_PWMSEL2_BITBAND_ADDRESS 0x42300988
#define PWM1PCR_PWMSEL2_BITBAND (*(volatile unsigned *)0x42300988)
#define PWM1PCR_PWMSEL2_BIT 2
#define PWM1PCR_PWMSEL3_MASK 0x8
#define PWM1PCR_PWMSEL3 0x8
#define PWM1PCR_PWMSEL3_BITBAND_ADDRESS 0x4230098C
#define PWM1PCR_PWMSEL3_BITBAND (*(volatile unsigned *)0x4230098C)
#define PWM1PCR_PWMSEL3_BIT 3
#define PWM1PCR_PWMSEL4_MASK 0x10
#define PWM1PCR_PWMSEL4 0x10
#define PWM1PCR_PWMSEL4_BITBAND_ADDRESS 0x42300990
#define PWM1PCR_PWMSEL4_BITBAND (*(volatile unsigned *)0x42300990)
#define PWM1PCR_PWMSEL4_BIT 4
#define PWM1PCR_PWMSEL5_MASK 0x20
#define PWM1PCR_PWMSEL5 0x20
#define PWM1PCR_PWMSEL5_BITBAND_ADDRESS 0x42300994
#define PWM1PCR_PWMSEL5_BITBAND (*(volatile unsigned *)0x42300994)
#define PWM1PCR_PWMSEL5_BIT 5
#define PWM1PCR_PWMSEL6_MASK 0x40
#define PWM1PCR_PWMSEL6 0x40
#define PWM1PCR_PWMSEL6_BITBAND_ADDRESS 0x42300998
#define PWM1PCR_PWMSEL6_BITBAND (*(volatile unsigned *)0x42300998)
#define PWM1PCR_PWMSEL6_BIT 6
#define PWM1PCR_PWMENA1_MASK 0x200
#define PWM1PCR_PWMENA1 0x200
#define PWM1PCR_PWMENA1_BITBAND_ADDRESS 0x423009A4
#define PWM1PCR_PWMENA1_BITBAND (*(volatile unsigned *)0x423009A4)
#define PWM1PCR_PWMENA1_BIT 9
#define PWM1PCR_PWMENA2_MASK 0x400
#define PWM1PCR_PWMENA2 0x400
#define PWM1PCR_PWMENA2_BITBAND_ADDRESS 0x423009A8
#define PWM1PCR_PWMENA2_BITBAND (*(volatile unsigned *)0x423009A8)
#define PWM1PCR_PWMENA2_BIT 10
#define PWM1PCR_PWMENA3_MASK 0x800
#define PWM1PCR_PWMENA3 0x800
#define PWM1PCR_PWMENA3_BITBAND_ADDRESS 0x423009AC
#define PWM1PCR_PWMENA3_BITBAND (*(volatile unsigned *)0x423009AC)
#define PWM1PCR_PWMENA3_BIT 11
#define PWM1PCR_PWMENA4_MASK 0x1000
#define PWM1PCR_PWMENA4 0x1000
#define PWM1PCR_PWMENA4_BITBAND_ADDRESS 0x423009B0
#define PWM1PCR_PWMENA4_BITBAND (*(volatile unsigned *)0x423009B0)
#define PWM1PCR_PWMENA4_BIT 12
#define PWM1PCR_PWMENA5_MASK 0x2000
#define PWM1PCR_PWMENA5 0x2000
#define PWM1PCR_PWMENA5_BITBAND_ADDRESS 0x423009B4
#define PWM1PCR_PWMENA5_BITBAND (*(volatile unsigned *)0x423009B4)
#define PWM1PCR_PWMENA5_BIT 13
#define PWM1PCR_PWMENA6_MASK 0x4000
#define PWM1PCR_PWMENA6 0x4000
#define PWM1PCR_PWMENA6_BITBAND_ADDRESS 0x423009B8
#define PWM1PCR_PWMENA6_BITBAND (*(volatile unsigned *)0x423009B8)
#define PWM1PCR_PWMENA6_BIT 14

#define PWM1LER (*(volatile unsigned long *)0x40018050)
#define PWM1LER_OFFSET 0x50
#define PWM1LER_Enable_PWM_Match_0_Latch_MASK 0x1
#define PWM1LER_Enable_PWM_Match_0_Latch 0x1
#define PWM1LER_Enable_PWM_Match_0_Latch_BITBAND_ADDRESS 0x42300A00
#define PWM1LER_Enable_PWM_Match_0_Latch_BITBAND (*(volatile unsigned *)0x42300A00)
#define PWM1LER_Enable_PWM_Match_0_Latch_BIT 0
#define PWM1LER_Enable_PWM_Match_1_Latch_MASK 0x2
#define PWM1LER_Enable_PWM_Match_1_Latch 0x2
#define PWM1LER_Enable_PWM_Match_1_Latch_BITBAND_ADDRESS 0x42300A04
#define PWM1LER_Enable_PWM_Match_1_Latch_BITBAND (*(volatile unsigned *)0x42300A04)
#define PWM1LER_Enable_PWM_Match_1_Latch_BIT 1
#define PWM1LER_Enable_PWM_Match_2_Latch_MASK 0x4
#define PWM1LER_Enable_PWM_Match_2_Latch 0x4
#define PWM1LER_Enable_PWM_Match_2_Latch_BITBAND_ADDRESS 0x42300A08
#define PWM1LER_Enable_PWM_Match_2_Latch_BITBAND (*(volatile unsigned *)0x42300A08)
#define PWM1LER_Enable_PWM_Match_2_Latch_BIT 2
#define PWM1LER_Enable_PWM_Match_3_Latch_MASK 0x8
#define PWM1LER_Enable_PWM_Match_3_Latch 0x8
#define PWM1LER_Enable_PWM_Match_3_Latch_BITBAND_ADDRESS 0x42300A0C
#define PWM1LER_Enable_PWM_Match_3_Latch_BITBAND (*(volatile unsigned *)0x42300A0C)
#define PWM1LER_Enable_PWM_Match_3_Latch_BIT 3
#define PWM1LER_Enable_PWM_Match_4_Latch_MASK 0x10
#define PWM1LER_Enable_PWM_Match_4_Latch 0x10
#define PWM1LER_Enable_PWM_Match_4_Latch_BITBAND_ADDRESS 0x42300A10
#define PWM1LER_Enable_PWM_Match_4_Latch_BITBAND (*(volatile unsigned *)0x42300A10)
#define PWM1LER_Enable_PWM_Match_4_Latch_BIT 4
#define PWM1LER_Enable_PWM_Match_5_Latch_MASK 0x20
#define PWM1LER_Enable_PWM_Match_5_Latch 0x20
#define PWM1LER_Enable_PWM_Match_5_Latch_BITBAND_ADDRESS 0x42300A14
#define PWM1LER_Enable_PWM_Match_5_Latch_BITBAND (*(volatile unsigned *)0x42300A14)
#define PWM1LER_Enable_PWM_Match_5_Latch_BIT 5
#define PWM1LER_Enable_PWM_Match_6_Latch_MASK 0x40
#define PWM1LER_Enable_PWM_Match_6_Latch 0x40
#define PWM1LER_Enable_PWM_Match_6_Latch_BITBAND_ADDRESS 0x42300A18
#define PWM1LER_Enable_PWM_Match_6_Latch_BITBAND (*(volatile unsigned *)0x42300A18)
#define PWM1LER_Enable_PWM_Match_6_Latch_BIT 6

#define PWM1CTCR (*(volatile unsigned long *)0x40018070)
#define PWM1CTCR_OFFSET 0x70
#define PWM1CTCR_Counter_Timer_Mode_MASK 0x3
#define PWM1CTCR_Counter_Timer_Mode_BIT 0
#define PWM1CTCR_Count_Input_Select_MASK 0xC
#define PWM1CTCR_Count_Input_Select_BIT 2

#define I2C0_BASE_ADDRESS 0x4001C000

#define I2C0CONSET (*(volatile unsigned char *)0x4001C000)
#define I2C0CONSET_OFFSET 0x0
#define I2C0CONSET_AA_MASK 0x4
#define I2C0CONSET_AA 0x4
#define I2C0CONSET_AA_BITBAND_ADDRESS 0x42380008
#define I2C0CONSET_AA_BITBAND (*(volatile unsigned *)0x42380008)
#define I2C0CONSET_AA_BIT 2
#define I2C0CONSET_SI_MASK 0x8
#define I2C0CONSET_SI 0x8
#define I2C0CONSET_SI_BITBAND_ADDRESS 0x4238000C
#define I2C0CONSET_SI_BITBAND (*(volatile unsigned *)0x4238000C)
#define I2C0CONSET_SI_BIT 3
#define I2C0CONSET_STO_MASK 0x10
#define I2C0CONSET_STO 0x10
#define I2C0CONSET_STO_BITBAND_ADDRESS 0x42380010
#define I2C0CONSET_STO_BITBAND (*(volatile unsigned *)0x42380010)
#define I2C0CONSET_STO_BIT 4
#define I2C0CONSET_STA_MASK 0x20
#define I2C0CONSET_STA 0x20
#define I2C0CONSET_STA_BITBAND_ADDRESS 0x42380014
#define I2C0CONSET_STA_BITBAND (*(volatile unsigned *)0x42380014)
#define I2C0CONSET_STA_BIT 5
#define I2C0CONSET_I2EN_MASK 0x40
#define I2C0CONSET_I2EN 0x40
#define I2C0CONSET_I2EN_BITBAND_ADDRESS 0x42380018
#define I2C0CONSET_I2EN_BITBAND (*(volatile unsigned *)0x42380018)
#define I2C0CONSET_I2EN_BIT 6

#define I2C0STAT (*(volatile unsigned char *)0x4001C004)
#define I2C0STAT_OFFSET 0x4
#define I2C0STAT_Status_MASK 0xF8
#define I2C0STAT_Status_BIT 3

#define I2C0DAT (*(volatile unsigned char *)0x4001C008)
#define I2C0DAT_OFFSET 0x8

#define I2C0ADR0 (*(volatile unsigned char *)0x4001C00C)
#define I2C0ADR0_OFFSET 0xC
#define I2C0ADR0_GC_MASK 0x1
#define I2C0ADR0_GC 0x1
#define I2C0ADR0_GC_BITBAND_ADDRESS 0x42380180
#define I2C0ADR0_GC_BITBAND (*(volatile unsigned *)0x42380180)
#define I2C0ADR0_GC_BIT 0
#define I2C0ADR0_Address_MASK 0xFE
#define I2C0ADR0_Address_BIT 1

#define I2C0SCLH (*(volatile unsigned short *)0x4001C010)
#define I2C0SCLH_OFFSET 0x10

#define I2C0SCLL (*(volatile unsigned short *)0x4001C014)
#define I2C0SCLL_OFFSET 0x14

#define I2C0CONCLR (*(volatile unsigned char *)0x4001C018)
#define I2C0CONCLR_OFFSET 0x18
#define I2C0CONCLR_AAC_MASK 0x4
#define I2C0CONCLR_AAC 0x4
#define I2C0CONCLR_AAC_BITBAND_ADDRESS 0x42380308
#define I2C0CONCLR_AAC_BITBAND (*(volatile unsigned *)0x42380308)
#define I2C0CONCLR_AAC_BIT 2
#define I2C0CONCLR_SIC_MASK 0x8
#define I2C0CONCLR_SIC 0x8
#define I2C0CONCLR_SIC_BITBAND_ADDRESS 0x4238030C
#define I2C0CONCLR_SIC_BITBAND (*(volatile unsigned *)0x4238030C)
#define I2C0CONCLR_SIC_BIT 3
#define I2C0CONCLR_STAC_MASK 0x20
#define I2C0CONCLR_STAC 0x20
#define I2C0CONCLR_STAC_BITBAND_ADDRESS 0x42380314
#define I2C0CONCLR_STAC_BITBAND (*(volatile unsigned *)0x42380314)
#define I2C0CONCLR_STAC_BIT 5
#define I2C0CONCLR_I2ENC_MASK 0x40
#define I2C0CONCLR_I2ENC 0x40
#define I2C0CONCLR_I2ENC_BITBAND_ADDRESS 0x42380318
#define I2C0CONCLR_I2ENC_BITBAND (*(volatile unsigned *)0x42380318)
#define I2C0CONCLR_I2ENC_BIT 6

#define I2C0MMCTRL (*(volatile unsigned char *)0x4001C01C)
#define I2C0MMCTRL_OFFSET 0x1C
#define I2C0MMCTRL_MM_ENA_MASK 0x1
#define I2C0MMCTRL_MM_ENA 0x1
#define I2C0MMCTRL_MM_ENA_BITBAND_ADDRESS 0x42380380
#define I2C0MMCTRL_MM_ENA_BITBAND (*(volatile unsigned *)0x42380380)
#define I2C0MMCTRL_MM_ENA_BIT 0
#define I2C0MMCTRL_ENA_SCL_MASK 0x2
#define I2C0MMCTRL_ENA_SCL 0x2
#define I2C0MMCTRL_ENA_SCL_BITBAND_ADDRESS 0x42380384
#define I2C0MMCTRL_ENA_SCL_BITBAND (*(volatile unsigned *)0x42380384)
#define I2C0MMCTRL_ENA_SCL_BIT 1
#define I2C0MMCTRL_MATCH_ALL_MASK 0x4
#define I2C0MMCTRL_MATCH_ALL 0x4
#define I2C0MMCTRL_MATCH_ALL_BITBAND_ADDRESS 0x42380388
#define I2C0MMCTRL_MATCH_ALL_BITBAND (*(volatile unsigned *)0x42380388)
#define I2C0MMCTRL_MATCH_ALL_BIT 2

#define I2C0ADR1 (*(volatile unsigned char *)0x4001C020)
#define I2C0ADR1_OFFSET 0x20
#define I2C0ADR1_GC_MASK 0x1
#define I2C0ADR1_GC 0x1
#define I2C0ADR1_GC_BITBAND_ADDRESS 0x42380400
#define I2C0ADR1_GC_BITBAND (*(volatile unsigned *)0x42380400)
#define I2C0ADR1_GC_BIT 0
#define I2C0ADR1_Address_MASK 0xFE
#define I2C0ADR1_Address_BIT 1

#define I2C0ADR2 (*(volatile unsigned char *)0x4001C024)
#define I2C0ADR2_OFFSET 0x24
#define I2C0ADR2_GC_MASK 0x1
#define I2C0ADR2_GC 0x1
#define I2C0ADR2_GC_BITBAND_ADDRESS 0x42380480
#define I2C0ADR2_GC_BITBAND (*(volatile unsigned *)0x42380480)
#define I2C0ADR2_GC_BIT 0
#define I2C0ADR2_Address_MASK 0xFE
#define I2C0ADR2_Address_BIT 1

#define I2C0ADR3 (*(volatile unsigned char *)0x4001C028)
#define I2C0ADR3_OFFSET 0x28
#define I2C0ADR3_GC_MASK 0x1
#define I2C0ADR3_GC 0x1
#define I2C0ADR3_GC_BITBAND_ADDRESS 0x42380500
#define I2C0ADR3_GC_BITBAND (*(volatile unsigned *)0x42380500)
#define I2C0ADR3_GC_BIT 0
#define I2C0ADR3_Address_MASK 0xFE
#define I2C0ADR3_Address_BIT 1

#define I2C0DATA_BUFFER (*(volatile unsigned char *)0x4001C02C)
#define I2C0DATA_BUFFER_OFFSET 0x2C

#define I2C0MASK0 (*(volatile unsigned long *)0x4001C030)
#define I2C0MASK0_OFFSET 0x30
#define I2C0MASK0_MASK_MASK 0xFE
#define I2C0MASK0_MASK_BIT 1

#define I2C0MASK1 (*(volatile unsigned long *)0x4001C034)
#define I2C0MASK1_OFFSET 0x34
#define I2C0MASK1_MASK_MASK 0xFE
#define I2C0MASK1_MASK_BIT 1

#define I2C0MASK2 (*(volatile unsigned long *)0x4001C038)
#define I2C0MASK2_OFFSET 0x38
#define I2C0MASK2_MASK_MASK 0xFE
#define I2C0MASK2_MASK_BIT 1

#define I2C0MASK3 (*(volatile unsigned long *)0x4001C03C)
#define I2C0MASK3_OFFSET 0x3C
#define I2C0MASK3_MASK_MASK 0xFE
#define I2C0MASK3_MASK_BIT 1

#define SPI0_BASE_ADDRESS 0x40020000

#define S0SPCR (*(volatile unsigned short *)0x40020000)
#define S0SPCR_OFFSET 0x0
#define S0SPCR_BitEnable_MASK 0x4
#define S0SPCR_BitEnable 0x4
#define S0SPCR_BitEnable_BITBAND_ADDRESS 0x42400008
#define S0SPCR_BitEnable_BITBAND (*(volatile unsigned *)0x42400008)
#define S0SPCR_BitEnable_BIT 2
#define S0SPCR_CPHA_MASK 0x8
#define S0SPCR_CPHA 0x8
#define S0SPCR_CPHA_BITBAND_ADDRESS 0x4240000C
#define S0SPCR_CPHA_BITBAND (*(volatile unsigned *)0x4240000C)
#define S0SPCR_CPHA_BIT 3
#define S0SPCR_CPOL_MASK 0x10
#define S0SPCR_CPOL 0x10
#define S0SPCR_CPOL_BITBAND_ADDRESS 0x42400010
#define S0SPCR_CPOL_BITBAND (*(volatile unsigned *)0x42400010)
#define S0SPCR_CPOL_BIT 4
#define S0SPCR_MSTR_MASK 0x20
#define S0SPCR_MSTR 0x20
#define S0SPCR_MSTR_BITBAND_ADDRESS 0x42400014
#define S0SPCR_MSTR_BITBAND (*(volatile unsigned *)0x42400014)
#define S0SPCR_MSTR_BIT 5
#define S0SPCR_LSBF_MASK 0x40
#define S0SPCR_LSBF 0x40
#define S0SPCR_LSBF_BITBAND_ADDRESS 0x42400018
#define S0SPCR_LSBF_BITBAND (*(volatile unsigned *)0x42400018)
#define S0SPCR_LSBF_BIT 6
#define S0SPCR_SPIE_MASK 0x80
#define S0SPCR_SPIE 0x80
#define S0SPCR_SPIE_BITBAND_ADDRESS 0x4240001C
#define S0SPCR_SPIE_BITBAND (*(volatile unsigned *)0x4240001C)
#define S0SPCR_SPIE_BIT 7
#define S0SPCR_BITS_MASK 0xF00
#define S0SPCR_BITS_BIT 8

#define S0SPSR (*(volatile unsigned char *)0x40020004)
#define S0SPSR_OFFSET 0x4
#define S0SPSR_ABRT_MASK 0x8
#define S0SPSR_ABRT 0x8
#define S0SPSR_ABRT_BITBAND_ADDRESS 0x4240008C
#define S0SPSR_ABRT_BITBAND (*(volatile unsigned *)0x4240008C)
#define S0SPSR_ABRT_BIT 3
#define S0SPSR_MODF_MASK 0x10
#define S0SPSR_MODF 0x10
#define S0SPSR_MODF_BITBAND_ADDRESS 0x42400090
#define S0SPSR_MODF_BITBAND (*(volatile unsigned *)0x42400090)
#define S0SPSR_MODF_BIT 4
#define S0SPSR_ROVR_MASK 0x20
#define S0SPSR_ROVR 0x20
#define S0SPSR_ROVR_BITBAND_ADDRESS 0x42400094
#define S0SPSR_ROVR_BITBAND (*(volatile unsigned *)0x42400094)
#define S0SPSR_ROVR_BIT 5
#define S0SPSR_WCOL_MASK 0x40
#define S0SPSR_WCOL 0x40
#define S0SPSR_WCOL_BITBAND_ADDRESS 0x42400098
#define S0SPSR_WCOL_BITBAND (*(volatile unsigned *)0x42400098)
#define S0SPSR_WCOL_BIT 6
#define S0SPSR_SPIF_MASK 0x80
#define S0SPSR_SPIF 0x80
#define S0SPSR_SPIF_BITBAND_ADDRESS 0x4240009C
#define S0SPSR_SPIF_BITBAND (*(volatile unsigned *)0x4240009C)
#define S0SPSR_SPIF_BIT 7

#define S0SPDR (*(volatile unsigned short *)0x40020008)
#define S0SPDR_OFFSET 0x8

#define S0SPCCR (*(volatile unsigned char *)0x4002000C)
#define S0SPCCR_OFFSET 0xC

#define S0SPINT (*(volatile unsigned char *)0x4002001C)
#define S0SPINT_OFFSET 0x1C

#define RTC_BASE_ADDRESS 0x40024000

#define ILR (*(volatile unsigned char *)0x40024000)
#define ILR_OFFSET 0x0
#define ILR_RTCCIF_MASK 0x1
#define ILR_RTCCIF 0x1
#define ILR_RTCCIF_BITBAND_ADDRESS 0x42480000
#define ILR_RTCCIF_BITBAND (*(volatile unsigned *)0x42480000)
#define ILR_RTCCIF_BIT 0
#define ILR_RTCALF_MASK 0x2
#define ILR_RTCALF 0x2
#define ILR_RTCALF_BITBAND_ADDRESS 0x42480004
#define ILR_RTCALF_BITBAND (*(volatile unsigned *)0x42480004)
#define ILR_RTCALF_BIT 1

#define CCR (*(volatile unsigned char *)0x40024008)
#define CCR_OFFSET 0x8
#define CCR_CLKEN_MASK 0x1
#define CCR_CLKEN 0x1
#define CCR_CLKEN_BITBAND_ADDRESS 0x42480100
#define CCR_CLKEN_BITBAND (*(volatile unsigned *)0x42480100)
#define CCR_CLKEN_BIT 0
#define CCR_CTCRST_MASK 0x2
#define CCR_CTCRST 0x2
#define CCR_CTCRST_BITBAND_ADDRESS 0x42480104
#define CCR_CTCRST_BITBAND (*(volatile unsigned *)0x42480104)
#define CCR_CTCRST_BIT 1
#define CCR_CCALEN_MASK 0x10
#define CCR_CCALEN 0x10
#define CCR_CCALEN_BITBAND_ADDRESS 0x42480110
#define CCR_CCALEN_BITBAND (*(volatile unsigned *)0x42480110)
#define CCR_CCALEN_BIT 4

#define CIIR (*(volatile unsigned char *)0x4002400C)
#define CIIR_OFFSET 0xC
#define CIIR_IMSEC_MASK 0x1
#define CIIR_IMSEC 0x1
#define CIIR_IMSEC_BITBAND_ADDRESS 0x42480180
#define CIIR_IMSEC_BITBAND (*(volatile unsigned *)0x42480180)
#define CIIR_IMSEC_BIT 0
#define CIIR_IMMIN_MASK 0x2
#define CIIR_IMMIN 0x2
#define CIIR_IMMIN_BITBAND_ADDRESS 0x42480184
#define CIIR_IMMIN_BITBAND (*(volatile unsigned *)0x42480184)
#define CIIR_IMMIN_BIT 1
#define CIIR_IMHOUR_MASK 0x4
#define CIIR_IMHOUR 0x4
#define CIIR_IMHOUR_BITBAND_ADDRESS 0x42480188
#define CIIR_IMHOUR_BITBAND (*(volatile unsigned *)0x42480188)
#define CIIR_IMHOUR_BIT 2
#define CIIR_IMDOM_MASK 0x8
#define CIIR_IMDOM 0x8
#define CIIR_IMDOM_BITBAND_ADDRESS 0x4248018C
#define CIIR_IMDOM_BITBAND (*(volatile unsigned *)0x4248018C)
#define CIIR_IMDOM_BIT 3
#define CIIR_IMDOW_MASK 0x10
#define CIIR_IMDOW 0x10
#define CIIR_IMDOW_BITBAND_ADDRESS 0x42480190
#define CIIR_IMDOW_BITBAND (*(volatile unsigned *)0x42480190)
#define CIIR_IMDOW_BIT 4
#define CIIR_IMDOY_MASK 0x20
#define CIIR_IMDOY 0x20
#define CIIR_IMDOY_BITBAND_ADDRESS 0x42480194
#define CIIR_IMDOY_BITBAND (*(volatile unsigned *)0x42480194)
#define CIIR_IMDOY_BIT 5
#define CIIR_IMMON_MASK 0x40
#define CIIR_IMMON 0x40
#define CIIR_IMMON_BITBAND_ADDRESS 0x42480198
#define CIIR_IMMON_BITBAND (*(volatile unsigned *)0x42480198)
#define CIIR_IMMON_BIT 6
#define CIIR_IMYEAR_MASK 0x80
#define CIIR_IMYEAR 0x80
#define CIIR_IMYEAR_BITBAND_ADDRESS 0x4248019C
#define CIIR_IMYEAR_BITBAND (*(volatile unsigned *)0x4248019C)
#define CIIR_IMYEAR_BIT 7

#define AMR (*(volatile unsigned char *)0x40024010)
#define AMR_OFFSET 0x10
#define AMR_AMRSEC_MASK 0x1
#define AMR_AMRSEC 0x1
#define AMR_AMRSEC_BITBAND_ADDRESS 0x42480200
#define AMR_AMRSEC_BITBAND (*(volatile unsigned *)0x42480200)
#define AMR_AMRSEC_BIT 0
#define AMR_AMRMIN_MASK 0x2
#define AMR_AMRMIN 0x2
#define AMR_AMRMIN_BITBAND_ADDRESS 0x42480204
#define AMR_AMRMIN_BITBAND (*(volatile unsigned *)0x42480204)
#define AMR_AMRMIN_BIT 1
#define AMR_AMRHOUR_MASK 0x4
#define AMR_AMRHOUR 0x4
#define AMR_AMRHOUR_BITBAND_ADDRESS 0x42480208
#define AMR_AMRHOUR_BITBAND (*(volatile unsigned *)0x42480208)
#define AMR_AMRHOUR_BIT 2
#define AMR_AMRDOM_MASK 0x8
#define AMR_AMRDOM 0x8
#define AMR_AMRDOM_BITBAND_ADDRESS 0x4248020C
#define AMR_AMRDOM_BITBAND (*(volatile unsigned *)0x4248020C)
#define AMR_AMRDOM_BIT 3
#define AMR_AMRDOW_MASK 0x10
#define AMR_AMRDOW 0x10
#define AMR_AMRDOW_BITBAND_ADDRESS 0x42480210
#define AMR_AMRDOW_BITBAND (*(volatile unsigned *)0x42480210)
#define AMR_AMRDOW_BIT 4
#define AMR_AMRDOY_MASK 0x20
#define AMR_AMRDOY 0x20
#define AMR_AMRDOY_BITBAND_ADDRESS 0x42480214
#define AMR_AMRDOY_BITBAND (*(volatile unsigned *)0x42480214)
#define AMR_AMRDOY_BIT 5
#define AMR_AMRMON_MASK 0x40
#define AMR_AMRMON 0x40
#define AMR_AMRMON_BITBAND_ADDRESS 0x42480218
#define AMR_AMRMON_BITBAND (*(volatile unsigned *)0x42480218)
#define AMR_AMRMON_BIT 6
#define AMR_AMRYEAR_MASK 0x80
#define AMR_AMRYEAR 0x80
#define AMR_AMRYEAR_BITBAND_ADDRESS 0x4248021C
#define AMR_AMRYEAR_BITBAND (*(volatile unsigned *)0x4248021C)
#define AMR_AMRYEAR_BIT 7

#define CTIME0 (*(volatile unsigned long *)0x40024014)
#define CTIME0_OFFSET 0x14
#define CTIME0_Seconds_MASK 0x3F
#define CTIME0_Seconds_BIT 0
#define CTIME0_Minutes_MASK 0x3F00
#define CTIME0_Minutes_BIT 8
#define CTIME0_Hours_MASK 0x1F0000
#define CTIME0_Hours_BIT 16
#define CTIME0_Day_of_Week_MASK 0x7000000
#define CTIME0_Day_of_Week_BIT 24

#define CTIME1 (*(volatile unsigned long *)0x40024018)
#define CTIME1_OFFSET 0x18
#define CTIME1_Day_of_Month_MASK 0x1F
#define CTIME1_Day_of_Month_BIT 0
#define CTIME1_Month_MASK 0xF00
#define CTIME1_Month_BIT 8
#define CTIME1_Year_MASK 0xFFF0000
#define CTIME1_Year_BIT 16

#define CTIME2 (*(volatile unsigned long *)0x4002401C)
#define CTIME2_OFFSET 0x1C
#define CTIME2_Day_of_Year_MASK 0xFFF
#define CTIME2_Day_of_Year_BIT 0

#define SEC (*(volatile unsigned long *)0x40024020)
#define SEC_OFFSET 0x20

#define MIN (*(volatile unsigned long *)0x40024024)
#define MIN_OFFSET 0x24

#define HOUR (*(volatile unsigned long *)0x40024028)
#define HOUR_OFFSET 0x28

#define DOM (*(volatile unsigned long *)0x4002402C)
#define DOM_OFFSET 0x2C

#define DOW (*(volatile unsigned long *)0x40024030)
#define DOW_OFFSET 0x30

#define DOY (*(volatile unsigned long *)0x40024034)
#define DOY_OFFSET 0x34

#define MONTH (*(volatile unsigned long *)0x40024038)
#define MONTH_OFFSET 0x38

#define YEAR (*(volatile unsigned long *)0x4002403C)
#define YEAR_OFFSET 0x3C

#define CALIBRATION (*(volatile unsigned long *)0x40024040)
#define CALIBRATION_OFFSET 0x40
#define CALIBRATION_CALVAL_MASK 0x1FFFF
#define CALIBRATION_CALVAL_BIT 0
#define CALIBRATION_CALDIR_MASK 0x20000
#define CALIBRATION_CALDIR 0x20000
#define CALIBRATION_CALDIR_BITBAND_ADDRESS 0x42480844
#define CALIBRATION_CALDIR_BITBAND (*(volatile unsigned *)0x42480844)
#define CALIBRATION_CALDIR_BIT 17

#define GPREG0 (*(volatile unsigned long *)0x40024044)
#define GPREG0_OFFSET 0x44

#define GPREG1 (*(volatile unsigned long *)0x40024048)
#define GPREG1_OFFSET 0x48

#define GPREG2 (*(volatile unsigned long *)0x4002404C)
#define GPREG2_OFFSET 0x4C

#define GPREG3 (*(volatile unsigned long *)0x40024050)
#define GPREG3_OFFSET 0x50

#define GPREG4 (*(volatile unsigned long *)0x40024054)
#define GPREG4_OFFSET 0x54

#define RTC_AUXEN (*(volatile unsigned char *)0x40024058)
#define RTC_AUXEN_OFFSET 0x58
#define RTC_AUXEN_RTC_OSCFEN_MASK 0x10
#define RTC_AUXEN_RTC_OSCFEN 0x10
#define RTC_AUXEN_RTC_OSCFEN_BITBAND_ADDRESS 0x42480B10
#define RTC_AUXEN_RTC_OSCFEN_BITBAND (*(volatile unsigned *)0x42480B10)
#define RTC_AUXEN_RTC_OSCFEN_BIT 4

#define RTC_AUX (*(volatile unsigned char *)0x4002405C)
#define RTC_AUX_OFFSET 0x5C
#define RTC_AUX_RTC_OSCF_MASK 0x10
#define RTC_AUX_RTC_OSCF 0x10
#define RTC_AUX_RTC_OSCF_BITBAND_ADDRESS 0x42480B90
#define RTC_AUX_RTC_OSCF_BITBAND (*(volatile unsigned *)0x42480B90)
#define RTC_AUX_RTC_OSCF_BIT 4

#define ALSEC (*(volatile unsigned long *)0x40024060)
#define ALSEC_OFFSET 0x60

#define ALMIN (*(volatile unsigned long *)0x40024064)
#define ALMIN_OFFSET 0x64

#define ALHOUR (*(volatile unsigned long *)0x40024068)
#define ALHOUR_OFFSET 0x68

#define ALDOM (*(volatile unsigned long *)0x4002406C)
#define ALDOM_OFFSET 0x6C

#define ALDOW (*(volatile unsigned long *)0x40024070)
#define ALDOW_OFFSET 0x70

#define ALDOY (*(volatile unsigned long *)0x40024074)
#define ALDOY_OFFSET 0x74

#define ALMON (*(volatile unsigned long *)0x40024078)
#define ALMON_OFFSET 0x78

#define ALYEAR (*(volatile unsigned long *)0x4002407C)
#define ALYEAR_OFFSET 0x7C

#define GPIO_INT_BASE_ADDRESS 0x40028000

#define IOIntStatus (*(volatile unsigned long *)0x40028080)
#define IOIntStatus_OFFSET 0x80
#define IOIntStatus_P0Int_MASK 0x1
#define IOIntStatus_P0Int 0x1
#define IOIntStatus_P0Int_BITBAND_ADDRESS 0x42501000
#define IOIntStatus_P0Int_BITBAND (*(volatile unsigned *)0x42501000)
#define IOIntStatus_P0Int_BIT 0
#define IOIntStatus_P2Int_MASK 0x4
#define IOIntStatus_P2Int 0x4
#define IOIntStatus_P2Int_BITBAND_ADDRESS 0x42501008
#define IOIntStatus_P2Int_BITBAND (*(volatile unsigned *)0x42501008)
#define IOIntStatus_P2Int_BIT 2

#define IO0IntStatR (*(volatile unsigned long *)0x40028084)
#define IO0IntStatR_OFFSET 0x84

#define IO0IntStatF (*(volatile unsigned long *)0x40028088)
#define IO0IntStatF_OFFSET 0x88

#define IO0IntClr (*(volatile unsigned long *)0x4002808C)
#define IO0IntClr_OFFSET 0x8C

#define IO0IntEnR (*(volatile unsigned long *)0x40028090)
#define IO0IntEnR_OFFSET 0x90

#define IO0IntEnF (*(volatile unsigned long *)0x40028094)
#define IO0IntEnF_OFFSET 0x94

#define IO2IntStatR (*(volatile unsigned long *)0x400280A4)
#define IO2IntStatR_OFFSET 0xA4

#define IO2IntStatF (*(volatile unsigned long *)0x400280A8)
#define IO2IntStatF_OFFSET 0xA8

#define IO2IntClr (*(volatile unsigned long *)0x400280AC)
#define IO2IntClr_OFFSET 0xAC

#define IO2IntEnR (*(volatile unsigned long *)0x400280B0)
#define IO2IntEnR_OFFSET 0xB0

#define IO2IntEnF (*(volatile unsigned long *)0x400280B4)
#define IO2IntEnF_OFFSET 0xB4

#define PCB_BASE_ADDRESS 0x4002C000

#define PINSEL0 (*(volatile unsigned long *)0x4002C000)
#define PINSEL0_OFFSET 0x0
#define PINSEL0_P0_0_MASK 0x3
#define PINSEL0_P0_0_BIT 0
#define PINSEL0_P0_1_MASK 0xC
#define PINSEL0_P0_1_BIT 2
#define PINSEL0_P0_2_MASK 0x30
#define PINSEL0_P0_2_BIT 4
#define PINSEL0_P0_3_MASK 0xC0
#define PINSEL0_P0_3_BIT 6
#define PINSEL0_P0_4_MASK 0x300
#define PINSEL0_P0_4_BIT 8
#define PINSEL0_P0_5_MASK 0xC00
#define PINSEL0_P0_5_BIT 10
#define PINSEL0_P0_6_MASK 0x3000
#define PINSEL0_P0_6_BIT 12
#define PINSEL0_P0_7_MASK 0xC000
#define PINSEL0_P0_7_BIT 14
#define PINSEL0_P0_8_MASK 0x30000
#define PINSEL0_P0_8_BIT 16
#define PINSEL0_P0_9_MASK 0xC0000
#define PINSEL0_P0_9_BIT 18
#define PINSEL0_P0_10_MASK 0x300000
#define PINSEL0_P0_10_BIT 20
#define PINSEL0_P0_11_MASK 0xC00000
#define PINSEL0_P0_11_BIT 22
#define PINSEL0_P0_15_MASK 0xC0000000
#define PINSEL0_P0_15_BIT 30

#define PINSEL1 (*(volatile unsigned long *)0x4002C004)
#define PINSEL1_OFFSET 0x4
#define PINSEL1_P0_16_MASK 0x3
#define PINSEL1_P0_16_BIT 0
#define PINSEL1_P0_17_MASK 0xC
#define PINSEL1_P0_17_BIT 2
#define PINSEL1_P0_18_MASK 0x30
#define PINSEL1_P0_18_BIT 4
#define PINSEL1_P0_19_MASK 0xC0
#define PINSEL1_P0_19_BIT 6
#define PINSEL1_P0_20_MASK 0x300
#define PINSEL1_P0_20_BIT 8
#define PINSEL1_P0_21_MASK 0xC00
#define PINSEL1_P0_21_BIT 10
#define PINSEL1_P0_22_MASK 0x3000
#define PINSEL1_P0_22_BIT 12
#define PINSEL1_P0_23_MASK 0xC000
#define PINSEL1_P0_23_BIT 14
#define PINSEL1_P0_24_MASK 0x30000
#define PINSEL1_P0_24_BIT 16
#define PINSEL1_P0_25_MASK 0xC0000
#define PINSEL1_P0_25_BIT 18
#define PINSEL1_P0_26_MASK 0x300000
#define PINSEL1_P0_26_BIT 20
#define PINSEL1_P0_27_MASK 0xC00000
#define PINSEL1_P0_27_BIT 22
#define PINSEL1_P0_28_MASK 0x3000000
#define PINSEL1_P0_28_BIT 24
#define PINSEL1_P0_29_MASK 0xC000000
#define PINSEL1_P0_29_BIT 26
#define PINSEL1_P0_30_MASK 0x30000000
#define PINSEL1_P0_30_BIT 28

#define PINSEL2 (*(volatile unsigned long *)0x4002C008)
#define PINSEL2_OFFSET 0x8
#define PINSEL2_P1_0_MASK 0x3
#define PINSEL2_P1_0_BIT 0
#define PINSEL2_P1_1_MASK 0xC
#define PINSEL2_P1_1_BIT 2
#define PINSEL2_P1_4_MASK 0x300
#define PINSEL2_P1_4_BIT 8
#define PINSEL2_P1_8_MASK 0x30000
#define PINSEL2_P1_8_BIT 16
#define PINSEL2_P1_9_MASK 0xC0000
#define PINSEL2_P1_9_BIT 18
#define PINSEL2_P1_10_MASK 0x300000
#define PINSEL2_P1_10_BIT 20
#define PINSEL2_P1_14_MASK 0x30000000
#define PINSEL2_P1_14_BIT 28
#define PINSEL2_P1_15_MASK 0xC0000000
#define PINSEL2_P1_15_BIT 30

#define PINSEL3 (*(volatile unsigned long *)0x4002C00C)
#define PINSEL3_OFFSET 0xC
#define PINSEL3_P1_16_MASK 0x3
#define PINSEL3_P1_16_BIT 0
#define PINSEL3_P1_17_MASK 0xC
#define PINSEL3_P1_17_BIT 2
#define PINSEL3_P1_18_MASK 0x30
#define PINSEL3_P1_18_BIT 4
#define PINSEL3_P1_19_MASK 0xC0
#define PINSEL3_P1_19_BIT 6
#define PINSEL3_P1_20_MASK 0x300
#define PINSEL3_P1_20_BIT 8
#define PINSEL3_P1_21_MASK 0xC00
#define PINSEL3_P1_21_BIT 10
#define PINSEL3_P1_22_MASK 0x3000
#define PINSEL3_P1_22_BIT 12
#define PINSEL3_P1_23_MASK 0xC000
#define PINSEL3_P1_23_BIT 14
#define PINSEL3_P1_24_MASK 0x30000
#define PINSEL3_P1_24_BIT 16
#define PINSEL3_P1_25_MASK 0xC0000
#define PINSEL3_P1_25_BIT 18
#define PINSEL3_P1_26_MASK 0x300000
#define PINSEL3_P1_26_BIT 20
#define PINSEL3_P1_27_MASK 0xC00000
#define PINSEL3_P1_27_BIT 22
#define PINSEL3_P1_28_MASK 0x3000000
#define PINSEL3_P1_28_BIT 24
#define PINSEL3_P1_29_MASK 0xC000000
#define PINSEL3_P1_29_BIT 26
#define PINSEL3_P1_30_MASK 0x30000000
#define PINSEL3_P1_30_BIT 28
#define PINSEL3_P1_31_MASK 0xC0000000
#define PINSEL3_P1_31_BIT 30

#define PINSEL4 (*(volatile unsigned long *)0x4002C010)
#define PINSEL4_OFFSET 0x10
#define PINSEL4_P2_0_MASK 0x3
#define PINSEL4_P2_0_BIT 0
#define PINSEL4_P2_1_MASK 0xC
#define PINSEL4_P2_1_BIT 2
#define PINSEL4_P2_2_MASK 0x30
#define PINSEL4_P2_2_BIT 4
#define PINSEL4_P2_3_MASK 0xC0
#define PINSEL4_P2_3_BIT 6
#define PINSEL4_P2_4_MASK 0x300
#define PINSEL4_P2_4_BIT 8
#define PINSEL4_P2_5_MASK 0xC00
#define PINSEL4_P2_5_BIT 10
#define PINSEL4_P2_6_MASK 0x3000
#define PINSEL4_P2_6_BIT 12
#define PINSEL4_P2_7_MASK 0xC000
#define PINSEL4_P2_7_BIT 14
#define PINSEL4_P2_8_MASK 0x30000
#define PINSEL4_P2_8_BIT 16
#define PINSEL4_P2_9_MASK 0xC0000
#define PINSEL4_P2_9_BIT 18
#define PINSEL4_P2_10_MASK 0x300000
#define PINSEL4_P2_10_BIT 20
#define PINSEL4_P2_11_MASK 0xC00000
#define PINSEL4_P2_11_BIT 22
#define PINSEL4_P2_12_MASK 0x3000000
#define PINSEL4_P2_12_BIT 24
#define PINSEL4_P2_13_MASK 0xC000000
#define PINSEL4_P2_13_BIT 26

#define PINSEL5 (*(volatile unsigned long *)0x4002C014)
#define PINSEL5_OFFSET 0x14

#define PINSEL6 (*(volatile unsigned long *)0x4002C018)
#define PINSEL6_OFFSET 0x18

#define PINSEL7 (*(volatile unsigned long *)0x4002C01C)
#define PINSEL7_OFFSET 0x1C
#define PINSEL7_P3_25_MASK 0xC0000
#define PINSEL7_P3_25_BIT 18
#define PINSEL7_P3_26_MASK 0x300000
#define PINSEL7_P3_26_BIT 20

#define PINSEL8 (*(volatile unsigned long *)0x4002C020)
#define PINSEL8_OFFSET 0x20

#define PINSEL9 (*(volatile unsigned long *)0x4002C024)
#define PINSEL9_OFFSET 0x24
#define PINSEL9_P4_28_MASK 0x3000000
#define PINSEL9_P4_28_BIT 24
#define PINSEL9_P4_29_MASK 0xC000000
#define PINSEL9_P4_29_BIT 26

#define PINSEL10 (*(volatile unsigned long *)0x4002C028)
#define PINSEL10_OFFSET 0x28
#define PINSEL10_GPIO_TRACE_MASK 0x8
#define PINSEL10_GPIO_TRACE 0x8
#define PINSEL10_GPIO_TRACE_BITBAND_ADDRESS 0x4258050C
#define PINSEL10_GPIO_TRACE_BITBAND (*(volatile unsigned *)0x4258050C)
#define PINSEL10_GPIO_TRACE_BIT 3

#define PINMODE0 (*(volatile unsigned long *)0x4002C040)
#define PINMODE0_OFFSET 0x40

#define PINMODE1 (*(volatile unsigned long *)0x4002C044)
#define PINMODE1_OFFSET 0x44

#define PINMODE2 (*(volatile unsigned long *)0x4002C048)
#define PINMODE2_OFFSET 0x48

#define PINMODE3 (*(volatile unsigned long *)0x4002C04C)
#define PINMODE3_OFFSET 0x4C

#define PINMODE4 (*(volatile unsigned long *)0x4002C050)
#define PINMODE4_OFFSET 0x50

#define PINMODE5 (*(volatile unsigned long *)0x4002C054)
#define PINMODE5_OFFSET 0x54

#define PINMODE6 (*(volatile unsigned long *)0x4002C058)
#define PINMODE6_OFFSET 0x58

#define PINMODE7 (*(volatile unsigned long *)0x4002C05C)
#define PINMODE7_OFFSET 0x5C

#define PINMODE8 (*(volatile unsigned long *)0x4002C060)
#define PINMODE8_OFFSET 0x60

#define PINMODE9 (*(volatile unsigned long *)0x4002C064)
#define PINMODE9_OFFSET 0x64

#define PINMODE_OD0 (*(volatile unsigned long *)0x4002C068)
#define PINMODE_OD0_OFFSET 0x68
#define PINMODE_OD0_P0_0OD_MASK 0x1
#define PINMODE_OD0_P0_0OD 0x1
#define PINMODE_OD0_P0_0OD_BITBAND_ADDRESS 0x42580D00
#define PINMODE_OD0_P0_0OD_BITBAND (*(volatile unsigned *)0x42580D00)
#define PINMODE_OD0_P0_0OD_BIT 0
#define PINMODE_OD0_P0_1OD_MASK 0x2
#define PINMODE_OD0_P0_1OD 0x2
#define PINMODE_OD0_P0_1OD_BITBAND_ADDRESS 0x42580D04
#define PINMODE_OD0_P0_1OD_BITBAND (*(volatile unsigned *)0x42580D04)
#define PINMODE_OD0_P0_1OD_BIT 1
#define PINMODE_OD0_P0_2OD_MASK 0x4
#define PINMODE_OD0_P0_2OD 0x4
#define PINMODE_OD0_P0_2OD_BITBAND_ADDRESS 0x42580D08
#define PINMODE_OD0_P0_2OD_BITBAND (*(volatile unsigned *)0x42580D08)
#define PINMODE_OD0_P0_2OD_BIT 2
#define PINMODE_OD0_P0_3OD_MASK 0x8
#define PINMODE_OD0_P0_3OD 0x8
#define PINMODE_OD0_P0_3OD_BITBAND_ADDRESS 0x42580D0C
#define PINMODE_OD0_P0_3OD_BITBAND (*(volatile unsigned *)0x42580D0C)
#define PINMODE_OD0_P0_3OD_BIT 3
#define PINMODE_OD0_P0_4OD_MASK 0x10
#define PINMODE_OD0_P0_4OD 0x10
#define PINMODE_OD0_P0_4OD_BITBAND_ADDRESS 0x42580D10
#define PINMODE_OD0_P0_4OD_BITBAND (*(volatile unsigned *)0x42580D10)
#define PINMODE_OD0_P0_4OD_BIT 4
#define PINMODE_OD0_P0_5OD_MASK 0x20
#define PINMODE_OD0_P0_5OD 0x20
#define PINMODE_OD0_P0_5OD_BITBAND_ADDRESS 0x42580D14
#define PINMODE_OD0_P0_5OD_BITBAND (*(volatile unsigned *)0x42580D14)
#define PINMODE_OD0_P0_5OD_BIT 5
#define PINMODE_OD0_P0_6OD_MASK 0x40
#define PINMODE_OD0_P0_6OD 0x40
#define PINMODE_OD0_P0_6OD_BITBAND_ADDRESS 0x42580D18
#define PINMODE_OD0_P0_6OD_BITBAND (*(volatile unsigned *)0x42580D18)
#define PINMODE_OD0_P0_6OD_BIT 6
#define PINMODE_OD0_P0_7OD_MASK 0x80
#define PINMODE_OD0_P0_7OD 0x80
#define PINMODE_OD0_P0_7OD_BITBAND_ADDRESS 0x42580D1C
#define PINMODE_OD0_P0_7OD_BITBAND (*(volatile unsigned *)0x42580D1C)
#define PINMODE_OD0_P0_7OD_BIT 7
#define PINMODE_OD0_P0_8OD_MASK 0x100
#define PINMODE_OD0_P0_8OD 0x100
#define PINMODE_OD0_P0_8OD_BITBAND_ADDRESS 0x42580D20
#define PINMODE_OD0_P0_8OD_BITBAND (*(volatile unsigned *)0x42580D20)
#define PINMODE_OD0_P0_8OD_BIT 8
#define PINMODE_OD0_P0_9OD_MASK 0x200
#define PINMODE_OD0_P0_9OD 0x200
#define PINMODE_OD0_P0_9OD_BITBAND_ADDRESS 0x42580D24
#define PINMODE_OD0_P0_9OD_BITBAND (*(volatile unsigned *)0x42580D24)
#define PINMODE_OD0_P0_9OD_BIT 9
#define PINMODE_OD0_P0_10OD_MASK 0x400
#define PINMODE_OD0_P0_10OD 0x400
#define PINMODE_OD0_P0_10OD_BITBAND_ADDRESS 0x42580D28
#define PINMODE_OD0_P0_10OD_BITBAND (*(volatile unsigned *)0x42580D28)
#define PINMODE_OD0_P0_10OD_BIT 10
#define PINMODE_OD0_P0_11OD_MASK 0x800
#define PINMODE_OD0_P0_11OD 0x800
#define PINMODE_OD0_P0_11OD_BITBAND_ADDRESS 0x42580D2C
#define PINMODE_OD0_P0_11OD_BITBAND (*(volatile unsigned *)0x42580D2C)
#define PINMODE_OD0_P0_11OD_BIT 11
#define PINMODE_OD0_P0_12OD_MASK 0x1000
#define PINMODE_OD0_P0_12OD 0x1000
#define PINMODE_OD0_P0_12OD_BITBAND_ADDRESS 0x42580D30
#define PINMODE_OD0_P0_12OD_BITBAND (*(volatile unsigned *)0x42580D30)
#define PINMODE_OD0_P0_12OD_BIT 12
#define PINMODE_OD0_P0_13OD_MASK 0x2000
#define PINMODE_OD0_P0_13OD 0x2000
#define PINMODE_OD0_P0_13OD_BITBAND_ADDRESS 0x42580D34
#define PINMODE_OD0_P0_13OD_BITBAND (*(volatile unsigned *)0x42580D34)
#define PINMODE_OD0_P0_13OD_BIT 13
#define PINMODE_OD0_P0_14OD_MASK 0x4000
#define PINMODE_OD0_P0_14OD 0x4000
#define PINMODE_OD0_P0_14OD_BITBAND_ADDRESS 0x42580D38
#define PINMODE_OD0_P0_14OD_BITBAND (*(volatile unsigned *)0x42580D38)
#define PINMODE_OD0_P0_14OD_BIT 14
#define PINMODE_OD0_P0_15OD_MASK 0x8000
#define PINMODE_OD0_P0_15OD 0x8000
#define PINMODE_OD0_P0_15OD_BITBAND_ADDRESS 0x42580D3C
#define PINMODE_OD0_P0_15OD_BITBAND (*(volatile unsigned *)0x42580D3C)
#define PINMODE_OD0_P0_15OD_BIT 15
#define PINMODE_OD0_P0_16OD_MASK 0x10000
#define PINMODE_OD0_P0_16OD 0x10000
#define PINMODE_OD0_P0_16OD_BITBAND_ADDRESS 0x42580D40
#define PINMODE_OD0_P0_16OD_BITBAND (*(volatile unsigned *)0x42580D40)
#define PINMODE_OD0_P0_16OD_BIT 16
#define PINMODE_OD0_P0_17OD_MASK 0x20000
#define PINMODE_OD0_P0_17OD 0x20000
#define PINMODE_OD0_P0_17OD_BITBAND_ADDRESS 0x42580D44
#define PINMODE_OD0_P0_17OD_BITBAND (*(volatile unsigned *)0x42580D44)
#define PINMODE_OD0_P0_17OD_BIT 17
#define PINMODE_OD0_P0_18OD_MASK 0x40000
#define PINMODE_OD0_P0_18OD 0x40000
#define PINMODE_OD0_P0_18OD_BITBAND_ADDRESS 0x42580D48
#define PINMODE_OD0_P0_18OD_BITBAND (*(volatile unsigned *)0x42580D48)
#define PINMODE_OD0_P0_18OD_BIT 18
#define PINMODE_OD0_P0_19OD_MASK 0x80000
#define PINMODE_OD0_P0_19OD 0x80000
#define PINMODE_OD0_P0_19OD_BITBAND_ADDRESS 0x42580D4C
#define PINMODE_OD0_P0_19OD_BITBAND (*(volatile unsigned *)0x42580D4C)
#define PINMODE_OD0_P0_19OD_BIT 19
#define PINMODE_OD0_P0_20OD_MASK 0x100000
#define PINMODE_OD0_P0_20OD 0x100000
#define PINMODE_OD0_P0_20OD_BITBAND_ADDRESS 0x42580D50
#define PINMODE_OD0_P0_20OD_BITBAND (*(volatile unsigned *)0x42580D50)
#define PINMODE_OD0_P0_20OD_BIT 20
#define PINMODE_OD0_P0_21OD_MASK 0x200000
#define PINMODE_OD0_P0_21OD 0x200000
#define PINMODE_OD0_P0_21OD_BITBAND_ADDRESS 0x42580D54
#define PINMODE_OD0_P0_21OD_BITBAND (*(volatile unsigned *)0x42580D54)
#define PINMODE_OD0_P0_21OD_BIT 21
#define PINMODE_OD0_P0_22OD_MASK 0x400000
#define PINMODE_OD0_P0_22OD 0x400000
#define PINMODE_OD0_P0_22OD_BITBAND_ADDRESS 0x42580D58
#define PINMODE_OD0_P0_22OD_BITBAND (*(volatile unsigned *)0x42580D58)
#define PINMODE_OD0_P0_22OD_BIT 22
#define PINMODE_OD0_P0_23OD_MASK 0x800000
#define PINMODE_OD0_P0_23OD 0x800000
#define PINMODE_OD0_P0_23OD_BITBAND_ADDRESS 0x42580D5C
#define PINMODE_OD0_P0_23OD_BITBAND (*(volatile unsigned *)0x42580D5C)
#define PINMODE_OD0_P0_23OD_BIT 23
#define PINMODE_OD0_P0_24OD_MASK 0x1000000
#define PINMODE_OD0_P0_24OD 0x1000000
#define PINMODE_OD0_P0_24OD_BITBAND_ADDRESS 0x42580D60
#define PINMODE_OD0_P0_24OD_BITBAND (*(volatile unsigned *)0x42580D60)
#define PINMODE_OD0_P0_24OD_BIT 24
#define PINMODE_OD0_P0_25OD_MASK 0x2000000
#define PINMODE_OD0_P0_25OD 0x2000000
#define PINMODE_OD0_P0_25OD_BITBAND_ADDRESS 0x42580D64
#define PINMODE_OD0_P0_25OD_BITBAND (*(volatile unsigned *)0x42580D64)
#define PINMODE_OD0_P0_25OD_BIT 25
#define PINMODE_OD0_P0_26OD_MASK 0x4000000
#define PINMODE_OD0_P0_26OD 0x4000000
#define PINMODE_OD0_P0_26OD_BITBAND_ADDRESS 0x42580D68
#define PINMODE_OD0_P0_26OD_BITBAND (*(volatile unsigned *)0x42580D68)
#define PINMODE_OD0_P0_26OD_BIT 26
#define PINMODE_OD0_P0_27OD_MASK 0x8000000
#define PINMODE_OD0_P0_27OD 0x8000000
#define PINMODE_OD0_P0_27OD_BITBAND_ADDRESS 0x42580D6C
#define PINMODE_OD0_P0_27OD_BITBAND (*(volatile unsigned *)0x42580D6C)
#define PINMODE_OD0_P0_27OD_BIT 27
#define PINMODE_OD0_P0_28OD_MASK 0x10000000
#define PINMODE_OD0_P0_28OD 0x10000000
#define PINMODE_OD0_P0_28OD_BITBAND_ADDRESS 0x42580D70
#define PINMODE_OD0_P0_28OD_BITBAND (*(volatile unsigned *)0x42580D70)
#define PINMODE_OD0_P0_28OD_BIT 28
#define PINMODE_OD0_P0_29OD_MASK 0x20000000
#define PINMODE_OD0_P0_29OD 0x20000000
#define PINMODE_OD0_P0_29OD_BITBAND_ADDRESS 0x42580D74
#define PINMODE_OD0_P0_29OD_BITBAND (*(volatile unsigned *)0x42580D74)
#define PINMODE_OD0_P0_29OD_BIT 29
#define PINMODE_OD0_P0_30OD_MASK 0x40000000
#define PINMODE_OD0_P0_30OD 0x40000000
#define PINMODE_OD0_P0_30OD_BITBAND_ADDRESS 0x42580D78
#define PINMODE_OD0_P0_30OD_BITBAND (*(volatile unsigned *)0x42580D78)
#define PINMODE_OD0_P0_30OD_BIT 30
#define PINMODE_OD0_P0_31OD_MASK 0x80000000
#define PINMODE_OD0_P0_31OD 0x80000000
#define PINMODE_OD0_P0_31OD_BITBAND_ADDRESS 0x42580D7C
#define PINMODE_OD0_P0_31OD_BITBAND (*(volatile unsigned *)0x42580D7C)
#define PINMODE_OD0_P0_31OD_BIT 31

#define PINMODE_OD1 (*(volatile unsigned long *)0x4002C06C)
#define PINMODE_OD1_OFFSET 0x6C
#define PINMODE_OD1_P1_0OD_MASK 0x1
#define PINMODE_OD1_P1_0OD 0x1
#define PINMODE_OD1_P1_0OD_BITBAND_ADDRESS 0x42580D80
#define PINMODE_OD1_P1_0OD_BITBAND (*(volatile unsigned *)0x42580D80)
#define PINMODE_OD1_P1_0OD_BIT 0
#define PINMODE_OD1_P1_1OD_MASK 0x2
#define PINMODE_OD1_P1_1OD 0x2
#define PINMODE_OD1_P1_1OD_BITBAND_ADDRESS 0x42580D84
#define PINMODE_OD1_P1_1OD_BITBAND (*(volatile unsigned *)0x42580D84)
#define PINMODE_OD1_P1_1OD_BIT 1
#define PINMODE_OD1_P1_2OD_MASK 0x4
#define PINMODE_OD1_P1_2OD 0x4
#define PINMODE_OD1_P1_2OD_BITBAND_ADDRESS 0x42580D88
#define PINMODE_OD1_P1_2OD_BITBAND (*(volatile unsigned *)0x42580D88)
#define PINMODE_OD1_P1_2OD_BIT 2
#define PINMODE_OD1_P1_3OD_MASK 0x8
#define PINMODE_OD1_P1_3OD 0x8
#define PINMODE_OD1_P1_3OD_BITBAND_ADDRESS 0x42580D8C
#define PINMODE_OD1_P1_3OD_BITBAND (*(volatile unsigned *)0x42580D8C)
#define PINMODE_OD1_P1_3OD_BIT 3
#define PINMODE_OD1_P1_4OD_MASK 0x10
#define PINMODE_OD1_P1_4OD 0x10
#define PINMODE_OD1_P1_4OD_BITBAND_ADDRESS 0x42580D90
#define PINMODE_OD1_P1_4OD_BITBAND (*(volatile unsigned *)0x42580D90)
#define PINMODE_OD1_P1_4OD_BIT 4
#define PINMODE_OD1_P1_5OD_MASK 0x20
#define PINMODE_OD1_P1_5OD 0x20
#define PINMODE_OD1_P1_5OD_BITBAND_ADDRESS 0x42580D94
#define PINMODE_OD1_P1_5OD_BITBAND (*(volatile unsigned *)0x42580D94)
#define PINMODE_OD1_P1_5OD_BIT 5
#define PINMODE_OD1_P1_6OD_MASK 0x40
#define PINMODE_OD1_P1_6OD 0x40
#define PINMODE_OD1_P1_6OD_BITBAND_ADDRESS 0x42580D98
#define PINMODE_OD1_P1_6OD_BITBAND (*(volatile unsigned *)0x42580D98)
#define PINMODE_OD1_P1_6OD_BIT 6
#define PINMODE_OD1_P1_7OD_MASK 0x80
#define PINMODE_OD1_P1_7OD 0x80
#define PINMODE_OD1_P1_7OD_BITBAND_ADDRESS 0x42580D9C
#define PINMODE_OD1_P1_7OD_BITBAND (*(volatile unsigned *)0x42580D9C)
#define PINMODE_OD1_P1_7OD_BIT 7
#define PINMODE_OD1_P1_8OD_MASK 0x100
#define PINMODE_OD1_P1_8OD 0x100
#define PINMODE_OD1_P1_8OD_BITBAND_ADDRESS 0x42580DA0
#define PINMODE_OD1_P1_8OD_BITBAND (*(volatile unsigned *)0x42580DA0)
#define PINMODE_OD1_P1_8OD_BIT 8
#define PINMODE_OD1_P1_9OD_MASK 0x200
#define PINMODE_OD1_P1_9OD 0x200
#define PINMODE_OD1_P1_9OD_BITBAND_ADDRESS 0x42580DA4
#define PINMODE_OD1_P1_9OD_BITBAND (*(volatile unsigned *)0x42580DA4)
#define PINMODE_OD1_P1_9OD_BIT 9
#define PINMODE_OD1_P1_10OD_MASK 0x400
#define PINMODE_OD1_P1_10OD 0x400
#define PINMODE_OD1_P1_10OD_BITBAND_ADDRESS 0x42580DA8
#define PINMODE_OD1_P1_10OD_BITBAND (*(volatile unsigned *)0x42580DA8)
#define PINMODE_OD1_P1_10OD_BIT 10
#define PINMODE_OD1_P1_11OD_MASK 0x800
#define PINMODE_OD1_P1_11OD 0x800
#define PINMODE_OD1_P1_11OD_BITBAND_ADDRESS 0x42580DAC
#define PINMODE_OD1_P1_11OD_BITBAND (*(volatile unsigned *)0x42580DAC)
#define PINMODE_OD1_P1_11OD_BIT 11
#define PINMODE_OD1_P1_12OD_MASK 0x1000
#define PINMODE_OD1_P1_12OD 0x1000
#define PINMODE_OD1_P1_12OD_BITBAND_ADDRESS 0x42580DB0
#define PINMODE_OD1_P1_12OD_BITBAND (*(volatile unsigned *)0x42580DB0)
#define PINMODE_OD1_P1_12OD_BIT 12
#define PINMODE_OD1_P1_13OD_MASK 0x2000
#define PINMODE_OD1_P1_13OD 0x2000
#define PINMODE_OD1_P1_13OD_BITBAND_ADDRESS 0x42580DB4
#define PINMODE_OD1_P1_13OD_BITBAND (*(volatile unsigned *)0x42580DB4)
#define PINMODE_OD1_P1_13OD_BIT 13
#define PINMODE_OD1_P1_14OD_MASK 0x4000
#define PINMODE_OD1_P1_14OD 0x4000
#define PINMODE_OD1_P1_14OD_BITBAND_ADDRESS 0x42580DB8
#define PINMODE_OD1_P1_14OD_BITBAND (*(volatile unsigned *)0x42580DB8)
#define PINMODE_OD1_P1_14OD_BIT 14
#define PINMODE_OD1_P1_15OD_MASK 0x8000
#define PINMODE_OD1_P1_15OD 0x8000
#define PINMODE_OD1_P1_15OD_BITBAND_ADDRESS 0x42580DBC
#define PINMODE_OD1_P1_15OD_BITBAND (*(volatile unsigned *)0x42580DBC)
#define PINMODE_OD1_P1_15OD_BIT 15
#define PINMODE_OD1_P1_16OD_MASK 0x10000
#define PINMODE_OD1_P1_16OD 0x10000
#define PINMODE_OD1_P1_16OD_BITBAND_ADDRESS 0x42580DC0
#define PINMODE_OD1_P1_16OD_BITBAND (*(volatile unsigned *)0x42580DC0)
#define PINMODE_OD1_P1_16OD_BIT 16
#define PINMODE_OD1_P1_17OD_MASK 0x20000
#define PINMODE_OD1_P1_17OD 0x20000
#define PINMODE_OD1_P1_17OD_BITBAND_ADDRESS 0x42580DC4
#define PINMODE_OD1_P1_17OD_BITBAND (*(volatile unsigned *)0x42580DC4)
#define PINMODE_OD1_P1_17OD_BIT 17
#define PINMODE_OD1_P1_18OD_MASK 0x40000
#define PINMODE_OD1_P1_18OD 0x40000
#define PINMODE_OD1_P1_18OD_BITBAND_ADDRESS 0x42580DC8
#define PINMODE_OD1_P1_18OD_BITBAND (*(volatile unsigned *)0x42580DC8)
#define PINMODE_OD1_P1_18OD_BIT 18
#define PINMODE_OD1_P1_19OD_MASK 0x80000
#define PINMODE_OD1_P1_19OD 0x80000
#define PINMODE_OD1_P1_19OD_BITBAND_ADDRESS 0x42580DCC
#define PINMODE_OD1_P1_19OD_BITBAND (*(volatile unsigned *)0x42580DCC)
#define PINMODE_OD1_P1_19OD_BIT 19
#define PINMODE_OD1_P1_20OD_MASK 0x100000
#define PINMODE_OD1_P1_20OD 0x100000
#define PINMODE_OD1_P1_20OD_BITBAND_ADDRESS 0x42580DD0
#define PINMODE_OD1_P1_20OD_BITBAND (*(volatile unsigned *)0x42580DD0)
#define PINMODE_OD1_P1_20OD_BIT 20
#define PINMODE_OD1_P1_21OD_MASK 0x200000
#define PINMODE_OD1_P1_21OD 0x200000
#define PINMODE_OD1_P1_21OD_BITBAND_ADDRESS 0x42580DD4
#define PINMODE_OD1_P1_21OD_BITBAND (*(volatile unsigned *)0x42580DD4)
#define PINMODE_OD1_P1_21OD_BIT 21
#define PINMODE_OD1_P1_22OD_MASK 0x400000
#define PINMODE_OD1_P1_22OD 0x400000
#define PINMODE_OD1_P1_22OD_BITBAND_ADDRESS 0x42580DD8
#define PINMODE_OD1_P1_22OD_BITBAND (*(volatile unsigned *)0x42580DD8)
#define PINMODE_OD1_P1_22OD_BIT 22
#define PINMODE_OD1_P1_23OD_MASK 0x800000
#define PINMODE_OD1_P1_23OD 0x800000
#define PINMODE_OD1_P1_23OD_BITBAND_ADDRESS 0x42580DDC
#define PINMODE_OD1_P1_23OD_BITBAND (*(volatile unsigned *)0x42580DDC)
#define PINMODE_OD1_P1_23OD_BIT 23
#define PINMODE_OD1_P1_24OD_MASK 0x1000000
#define PINMODE_OD1_P1_24OD 0x1000000
#define PINMODE_OD1_P1_24OD_BITBAND_ADDRESS 0x42580DE0
#define PINMODE_OD1_P1_24OD_BITBAND (*(volatile unsigned *)0x42580DE0)
#define PINMODE_OD1_P1_24OD_BIT 24
#define PINMODE_OD1_P1_25OD_MASK 0x2000000
#define PINMODE_OD1_P1_25OD 0x2000000
#define PINMODE_OD1_P1_25OD_BITBAND_ADDRESS 0x42580DE4
#define PINMODE_OD1_P1_25OD_BITBAND (*(volatile unsigned *)0x42580DE4)
#define PINMODE_OD1_P1_25OD_BIT 25
#define PINMODE_OD1_P1_26OD_MASK 0x4000000
#define PINMODE_OD1_P1_26OD 0x4000000
#define PINMODE_OD1_P1_26OD_BITBAND_ADDRESS 0x42580DE8
#define PINMODE_OD1_P1_26OD_BITBAND (*(volatile unsigned *)0x42580DE8)
#define PINMODE_OD1_P1_26OD_BIT 26
#define PINMODE_OD1_P1_27OD_MASK 0x8000000
#define PINMODE_OD1_P1_27OD 0x8000000
#define PINMODE_OD1_P1_27OD_BITBAND_ADDRESS 0x42580DEC
#define PINMODE_OD1_P1_27OD_BITBAND (*(volatile unsigned *)0x42580DEC)
#define PINMODE_OD1_P1_27OD_BIT 27
#define PINMODE_OD1_P1_28OD_MASK 0x10000000
#define PINMODE_OD1_P1_28OD 0x10000000
#define PINMODE_OD1_P1_28OD_BITBAND_ADDRESS 0x42580DF0
#define PINMODE_OD1_P1_28OD_BITBAND (*(volatile unsigned *)0x42580DF0)
#define PINMODE_OD1_P1_28OD_BIT 28
#define PINMODE_OD1_P1_29OD_MASK 0x20000000
#define PINMODE_OD1_P1_29OD 0x20000000
#define PINMODE_OD1_P1_29OD_BITBAND_ADDRESS 0x42580DF4
#define PINMODE_OD1_P1_29OD_BITBAND (*(volatile unsigned *)0x42580DF4)
#define PINMODE_OD1_P1_29OD_BIT 29
#define PINMODE_OD1_P1_30OD_MASK 0x40000000
#define PINMODE_OD1_P1_30OD 0x40000000
#define PINMODE_OD1_P1_30OD_BITBAND_ADDRESS 0x42580DF8
#define PINMODE_OD1_P1_30OD_BITBAND (*(volatile unsigned *)0x42580DF8)
#define PINMODE_OD1_P1_30OD_BIT 30
#define PINMODE_OD1_P1_31OD_MASK 0x80000000
#define PINMODE_OD1_P1_31OD 0x80000000
#define PINMODE_OD1_P1_31OD_BITBAND_ADDRESS 0x42580DFC
#define PINMODE_OD1_P1_31OD_BITBAND (*(volatile unsigned *)0x42580DFC)
#define PINMODE_OD1_P1_31OD_BIT 31

#define PINMODE_OD2 (*(volatile unsigned long *)0x4002C070)
#define PINMODE_OD2_OFFSET 0x70
#define PINMODE_OD2_P2_0OD_MASK 0x1
#define PINMODE_OD2_P2_0OD 0x1
#define PINMODE_OD2_P2_0OD_BITBAND_ADDRESS 0x42580E00
#define PINMODE_OD2_P2_0OD_BITBAND (*(volatile unsigned *)0x42580E00)
#define PINMODE_OD2_P2_0OD_BIT 0
#define PINMODE_OD2_P2_1OD_MASK 0x2
#define PINMODE_OD2_P2_1OD 0x2
#define PINMODE_OD2_P2_1OD_BITBAND_ADDRESS 0x42580E04
#define PINMODE_OD2_P2_1OD_BITBAND (*(volatile unsigned *)0x42580E04)
#define PINMODE_OD2_P2_1OD_BIT 1
#define PINMODE_OD2_P2_2OD_MASK 0x4
#define PINMODE_OD2_P2_2OD 0x4
#define PINMODE_OD2_P2_2OD_BITBAND_ADDRESS 0x42580E08
#define PINMODE_OD2_P2_2OD_BITBAND (*(volatile unsigned *)0x42580E08)
#define PINMODE_OD2_P2_2OD_BIT 2
#define PINMODE_OD2_P2_3OD_MASK 0x8
#define PINMODE_OD2_P2_3OD 0x8
#define PINMODE_OD2_P2_3OD_BITBAND_ADDRESS 0x42580E0C
#define PINMODE_OD2_P2_3OD_BITBAND (*(volatile unsigned *)0x42580E0C)
#define PINMODE_OD2_P2_3OD_BIT 3
#define PINMODE_OD2_P2_4OD_MASK 0x10
#define PINMODE_OD2_P2_4OD 0x10
#define PINMODE_OD2_P2_4OD_BITBAND_ADDRESS 0x42580E10
#define PINMODE_OD2_P2_4OD_BITBAND (*(volatile unsigned *)0x42580E10)
#define PINMODE_OD2_P2_4OD_BIT 4
#define PINMODE_OD2_P2_5OD_MASK 0x20
#define PINMODE_OD2_P2_5OD 0x20
#define PINMODE_OD2_P2_5OD_BITBAND_ADDRESS 0x42580E14
#define PINMODE_OD2_P2_5OD_BITBAND (*(volatile unsigned *)0x42580E14)
#define PINMODE_OD2_P2_5OD_BIT 5
#define PINMODE_OD2_P2_6OD_MASK 0x40
#define PINMODE_OD2_P2_6OD 0x40
#define PINMODE_OD2_P2_6OD_BITBAND_ADDRESS 0x42580E18
#define PINMODE_OD2_P2_6OD_BITBAND (*(volatile unsigned *)0x42580E18)
#define PINMODE_OD2_P2_6OD_BIT 6
#define PINMODE_OD2_P2_7OD_MASK 0x80
#define PINMODE_OD2_P2_7OD 0x80
#define PINMODE_OD2_P2_7OD_BITBAND_ADDRESS 0x42580E1C
#define PINMODE_OD2_P2_7OD_BITBAND (*(volatile unsigned *)0x42580E1C)
#define PINMODE_OD2_P2_7OD_BIT 7
#define PINMODE_OD2_P2_8OD_MASK 0x100
#define PINMODE_OD2_P2_8OD 0x100
#define PINMODE_OD2_P2_8OD_BITBAND_ADDRESS 0x42580E20
#define PINMODE_OD2_P2_8OD_BITBAND (*(volatile unsigned *)0x42580E20)
#define PINMODE_OD2_P2_8OD_BIT 8
#define PINMODE_OD2_P2_9OD_MASK 0x200
#define PINMODE_OD2_P2_9OD 0x200
#define PINMODE_OD2_P2_9OD_BITBAND_ADDRESS 0x42580E24
#define PINMODE_OD2_P2_9OD_BITBAND (*(volatile unsigned *)0x42580E24)
#define PINMODE_OD2_P2_9OD_BIT 9
#define PINMODE_OD2_P2_10OD_MASK 0x400
#define PINMODE_OD2_P2_10OD 0x400
#define PINMODE_OD2_P2_10OD_BITBAND_ADDRESS 0x42580E28
#define PINMODE_OD2_P2_10OD_BITBAND (*(volatile unsigned *)0x42580E28)
#define PINMODE_OD2_P2_10OD_BIT 10
#define PINMODE_OD2_P2_11OD_MASK 0x800
#define PINMODE_OD2_P2_11OD 0x800
#define PINMODE_OD2_P2_11OD_BITBAND_ADDRESS 0x42580E2C
#define PINMODE_OD2_P2_11OD_BITBAND (*(volatile unsigned *)0x42580E2C)
#define PINMODE_OD2_P2_11OD_BIT 11
#define PINMODE_OD2_P2_12OD_MASK 0x1000
#define PINMODE_OD2_P2_12OD 0x1000
#define PINMODE_OD2_P2_12OD_BITBAND_ADDRESS 0x42580E30
#define PINMODE_OD2_P2_12OD_BITBAND (*(volatile unsigned *)0x42580E30)
#define PINMODE_OD2_P2_12OD_BIT 12
#define PINMODE_OD2_P2_13OD_MASK 0x2000
#define PINMODE_OD2_P2_13OD 0x2000
#define PINMODE_OD2_P2_13OD_BITBAND_ADDRESS 0x42580E34
#define PINMODE_OD2_P2_13OD_BITBAND (*(volatile unsigned *)0x42580E34)
#define PINMODE_OD2_P2_13OD_BIT 13
#define PINMODE_OD2_P2_14OD_MASK 0x4000
#define PINMODE_OD2_P2_14OD 0x4000
#define PINMODE_OD2_P2_14OD_BITBAND_ADDRESS 0x42580E38
#define PINMODE_OD2_P2_14OD_BITBAND (*(volatile unsigned *)0x42580E38)
#define PINMODE_OD2_P2_14OD_BIT 14
#define PINMODE_OD2_P2_15OD_MASK 0x8000
#define PINMODE_OD2_P2_15OD 0x8000
#define PINMODE_OD2_P2_15OD_BITBAND_ADDRESS 0x42580E3C
#define PINMODE_OD2_P2_15OD_BITBAND (*(volatile unsigned *)0x42580E3C)
#define PINMODE_OD2_P2_15OD_BIT 15
#define PINMODE_OD2_P2_16OD_MASK 0x10000
#define PINMODE_OD2_P2_16OD 0x10000
#define PINMODE_OD2_P2_16OD_BITBAND_ADDRESS 0x42580E40
#define PINMODE_OD2_P2_16OD_BITBAND (*(volatile unsigned *)0x42580E40)
#define PINMODE_OD2_P2_16OD_BIT 16
#define PINMODE_OD2_P2_17OD_MASK 0x20000
#define PINMODE_OD2_P2_17OD 0x20000
#define PINMODE_OD2_P2_17OD_BITBAND_ADDRESS 0x42580E44
#define PINMODE_OD2_P2_17OD_BITBAND (*(volatile unsigned *)0x42580E44)
#define PINMODE_OD2_P2_17OD_BIT 17
#define PINMODE_OD2_P2_18OD_MASK 0x40000
#define PINMODE_OD2_P2_18OD 0x40000
#define PINMODE_OD2_P2_18OD_BITBAND_ADDRESS 0x42580E48
#define PINMODE_OD2_P2_18OD_BITBAND (*(volatile unsigned *)0x42580E48)
#define PINMODE_OD2_P2_18OD_BIT 18
#define PINMODE_OD2_P2_19OD_MASK 0x80000
#define PINMODE_OD2_P2_19OD 0x80000
#define PINMODE_OD2_P2_19OD_BITBAND_ADDRESS 0x42580E4C
#define PINMODE_OD2_P2_19OD_BITBAND (*(volatile unsigned *)0x42580E4C)
#define PINMODE_OD2_P2_19OD_BIT 19
#define PINMODE_OD2_P2_20OD_MASK 0x100000
#define PINMODE_OD2_P2_20OD 0x100000
#define PINMODE_OD2_P2_20OD_BITBAND_ADDRESS 0x42580E50
#define PINMODE_OD2_P2_20OD_BITBAND (*(volatile unsigned *)0x42580E50)
#define PINMODE_OD2_P2_20OD_BIT 20
#define PINMODE_OD2_P2_21OD_MASK 0x200000
#define PINMODE_OD2_P2_21OD 0x200000
#define PINMODE_OD2_P2_21OD_BITBAND_ADDRESS 0x42580E54
#define PINMODE_OD2_P2_21OD_BITBAND (*(volatile unsigned *)0x42580E54)
#define PINMODE_OD2_P2_21OD_BIT 21
#define PINMODE_OD2_P2_22OD_MASK 0x400000
#define PINMODE_OD2_P2_22OD 0x400000
#define PINMODE_OD2_P2_22OD_BITBAND_ADDRESS 0x42580E58
#define PINMODE_OD2_P2_22OD_BITBAND (*(volatile unsigned *)0x42580E58)
#define PINMODE_OD2_P2_22OD_BIT 22
#define PINMODE_OD2_P2_23OD_MASK 0x800000
#define PINMODE_OD2_P2_23OD 0x800000
#define PINMODE_OD2_P2_23OD_BITBAND_ADDRESS 0x42580E5C
#define PINMODE_OD2_P2_23OD_BITBAND (*(volatile unsigned *)0x42580E5C)
#define PINMODE_OD2_P2_23OD_BIT 23
#define PINMODE_OD2_P2_24OD_MASK 0x1000000
#define PINMODE_OD2_P2_24OD 0x1000000
#define PINMODE_OD2_P2_24OD_BITBAND_ADDRESS 0x42580E60
#define PINMODE_OD2_P2_24OD_BITBAND (*(volatile unsigned *)0x42580E60)
#define PINMODE_OD2_P2_24OD_BIT 24
#define PINMODE_OD2_P2_25OD_MASK 0x2000000
#define PINMODE_OD2_P2_25OD 0x2000000
#define PINMODE_OD2_P2_25OD_BITBAND_ADDRESS 0x42580E64
#define PINMODE_OD2_P2_25OD_BITBAND (*(volatile unsigned *)0x42580E64)
#define PINMODE_OD2_P2_25OD_BIT 25
#define PINMODE_OD2_P2_26OD_MASK 0x4000000
#define PINMODE_OD2_P2_26OD 0x4000000
#define PINMODE_OD2_P2_26OD_BITBAND_ADDRESS 0x42580E68
#define PINMODE_OD2_P2_26OD_BITBAND (*(volatile unsigned *)0x42580E68)
#define PINMODE_OD2_P2_26OD_BIT 26
#define PINMODE_OD2_P2_27OD_MASK 0x8000000
#define PINMODE_OD2_P2_27OD 0x8000000
#define PINMODE_OD2_P2_27OD_BITBAND_ADDRESS 0x42580E6C
#define PINMODE_OD2_P2_27OD_BITBAND (*(volatile unsigned *)0x42580E6C)
#define PINMODE_OD2_P2_27OD_BIT 27
#define PINMODE_OD2_P2_28OD_MASK 0x10000000
#define PINMODE_OD2_P2_28OD 0x10000000
#define PINMODE_OD2_P2_28OD_BITBAND_ADDRESS 0x42580E70
#define PINMODE_OD2_P2_28OD_BITBAND (*(volatile unsigned *)0x42580E70)
#define PINMODE_OD2_P2_28OD_BIT 28
#define PINMODE_OD2_P2_29OD_MASK 0x20000000
#define PINMODE_OD2_P2_29OD 0x20000000
#define PINMODE_OD2_P2_29OD_BITBAND_ADDRESS 0x42580E74
#define PINMODE_OD2_P2_29OD_BITBAND (*(volatile unsigned *)0x42580E74)
#define PINMODE_OD2_P2_29OD_BIT 29
#define PINMODE_OD2_P2_30OD_MASK 0x40000000
#define PINMODE_OD2_P2_30OD 0x40000000
#define PINMODE_OD2_P2_30OD_BITBAND_ADDRESS 0x42580E78
#define PINMODE_OD2_P2_30OD_BITBAND (*(volatile unsigned *)0x42580E78)
#define PINMODE_OD2_P2_30OD_BIT 30
#define PINMODE_OD2_P2_31OD_MASK 0x80000000
#define PINMODE_OD2_P2_31OD 0x80000000
#define PINMODE_OD2_P2_31OD_BITBAND_ADDRESS 0x42580E7C
#define PINMODE_OD2_P2_31OD_BITBAND (*(volatile unsigned *)0x42580E7C)
#define PINMODE_OD2_P2_31OD_BIT 31

#define PINMODE_OD3 (*(volatile unsigned long *)0x4002C074)
#define PINMODE_OD3_OFFSET 0x74
#define PINMODE_OD3_P3_0OD_MASK 0x1
#define PINMODE_OD3_P3_0OD 0x1
#define PINMODE_OD3_P3_0OD_BITBAND_ADDRESS 0x42580E80
#define PINMODE_OD3_P3_0OD_BITBAND (*(volatile unsigned *)0x42580E80)
#define PINMODE_OD3_P3_0OD_BIT 0
#define PINMODE_OD3_P3_1OD_MASK 0x2
#define PINMODE_OD3_P3_1OD 0x2
#define PINMODE_OD3_P3_1OD_BITBAND_ADDRESS 0x42580E84
#define PINMODE_OD3_P3_1OD_BITBAND (*(volatile unsigned *)0x42580E84)
#define PINMODE_OD3_P3_1OD_BIT 1
#define PINMODE_OD3_P3_2OD_MASK 0x4
#define PINMODE_OD3_P3_2OD 0x4
#define PINMODE_OD3_P3_2OD_BITBAND_ADDRESS 0x42580E88
#define PINMODE_OD3_P3_2OD_BITBAND (*(volatile unsigned *)0x42580E88)
#define PINMODE_OD3_P3_2OD_BIT 2
#define PINMODE_OD3_P3_3OD_MASK 0x8
#define PINMODE_OD3_P3_3OD 0x8
#define PINMODE_OD3_P3_3OD_BITBAND_ADDRESS 0x42580E8C
#define PINMODE_OD3_P3_3OD_BITBAND (*(volatile unsigned *)0x42580E8C)
#define PINMODE_OD3_P3_3OD_BIT 3
#define PINMODE_OD3_P3_4OD_MASK 0x10
#define PINMODE_OD3_P3_4OD 0x10
#define PINMODE_OD3_P3_4OD_BITBAND_ADDRESS 0x42580E90
#define PINMODE_OD3_P3_4OD_BITBAND (*(volatile unsigned *)0x42580E90)
#define PINMODE_OD3_P3_4OD_BIT 4
#define PINMODE_OD3_P3_5OD_MASK 0x20
#define PINMODE_OD3_P3_5OD 0x20
#define PINMODE_OD3_P3_5OD_BITBAND_ADDRESS 0x42580E94
#define PINMODE_OD3_P3_5OD_BITBAND (*(volatile unsigned *)0x42580E94)
#define PINMODE_OD3_P3_5OD_BIT 5
#define PINMODE_OD3_P3_6OD_MASK 0x40
#define PINMODE_OD3_P3_6OD 0x40
#define PINMODE_OD3_P3_6OD_BITBAND_ADDRESS 0x42580E98
#define PINMODE_OD3_P3_6OD_BITBAND (*(volatile unsigned *)0x42580E98)
#define PINMODE_OD3_P3_6OD_BIT 6
#define PINMODE_OD3_P3_7OD_MASK 0x80
#define PINMODE_OD3_P3_7OD 0x80
#define PINMODE_OD3_P3_7OD_BITBAND_ADDRESS 0x42580E9C
#define PINMODE_OD3_P3_7OD_BITBAND (*(volatile unsigned *)0x42580E9C)
#define PINMODE_OD3_P3_7OD_BIT 7
#define PINMODE_OD3_P3_8OD_MASK 0x100
#define PINMODE_OD3_P3_8OD 0x100
#define PINMODE_OD3_P3_8OD_BITBAND_ADDRESS 0x42580EA0
#define PINMODE_OD3_P3_8OD_BITBAND (*(volatile unsigned *)0x42580EA0)
#define PINMODE_OD3_P3_8OD_BIT 8
#define PINMODE_OD3_P3_9OD_MASK 0x200
#define PINMODE_OD3_P3_9OD 0x200
#define PINMODE_OD3_P3_9OD_BITBAND_ADDRESS 0x42580EA4
#define PINMODE_OD3_P3_9OD_BITBAND (*(volatile unsigned *)0x42580EA4)
#define PINMODE_OD3_P3_9OD_BIT 9
#define PINMODE_OD3_P3_10OD_MASK 0x400
#define PINMODE_OD3_P3_10OD 0x400
#define PINMODE_OD3_P3_10OD_BITBAND_ADDRESS 0x42580EA8
#define PINMODE_OD3_P3_10OD_BITBAND (*(volatile unsigned *)0x42580EA8)
#define PINMODE_OD3_P3_10OD_BIT 10
#define PINMODE_OD3_P3_11OD_MASK 0x800
#define PINMODE_OD3_P3_11OD 0x800
#define PINMODE_OD3_P3_11OD_BITBAND_ADDRESS 0x42580EAC
#define PINMODE_OD3_P3_11OD_BITBAND (*(volatile unsigned *)0x42580EAC)
#define PINMODE_OD3_P3_11OD_BIT 11
#define PINMODE_OD3_P3_12OD_MASK 0x1000
#define PINMODE_OD3_P3_12OD 0x1000
#define PINMODE_OD3_P3_12OD_BITBAND_ADDRESS 0x42580EB0
#define PINMODE_OD3_P3_12OD_BITBAND (*(volatile unsigned *)0x42580EB0)
#define PINMODE_OD3_P3_12OD_BIT 12
#define PINMODE_OD3_P3_13OD_MASK 0x2000
#define PINMODE_OD3_P3_13OD 0x2000
#define PINMODE_OD3_P3_13OD_BITBAND_ADDRESS 0x42580EB4
#define PINMODE_OD3_P3_13OD_BITBAND (*(volatile unsigned *)0x42580EB4)
#define PINMODE_OD3_P3_13OD_BIT 13
#define PINMODE_OD3_P3_14OD_MASK 0x4000
#define PINMODE_OD3_P3_14OD 0x4000
#define PINMODE_OD3_P3_14OD_BITBAND_ADDRESS 0x42580EB8
#define PINMODE_OD3_P3_14OD_BITBAND (*(volatile unsigned *)0x42580EB8)
#define PINMODE_OD3_P3_14OD_BIT 14
#define PINMODE_OD3_P3_15OD_MASK 0x8000
#define PINMODE_OD3_P3_15OD 0x8000
#define PINMODE_OD3_P3_15OD_BITBAND_ADDRESS 0x42580EBC
#define PINMODE_OD3_P3_15OD_BITBAND (*(volatile unsigned *)0x42580EBC)
#define PINMODE_OD3_P3_15OD_BIT 15
#define PINMODE_OD3_P3_16OD_MASK 0x10000
#define PINMODE_OD3_P3_16OD 0x10000
#define PINMODE_OD3_P3_16OD_BITBAND_ADDRESS 0x42580EC0
#define PINMODE_OD3_P3_16OD_BITBAND (*(volatile unsigned *)0x42580EC0)
#define PINMODE_OD3_P3_16OD_BIT 16
#define PINMODE_OD3_P3_17OD_MASK 0x20000
#define PINMODE_OD3_P3_17OD 0x20000
#define PINMODE_OD3_P3_17OD_BITBAND_ADDRESS 0x42580EC4
#define PINMODE_OD3_P3_17OD_BITBAND (*(volatile unsigned *)0x42580EC4)
#define PINMODE_OD3_P3_17OD_BIT 17
#define PINMODE_OD3_P3_18OD_MASK 0x40000
#define PINMODE_OD3_P3_18OD 0x40000
#define PINMODE_OD3_P3_18OD_BITBAND_ADDRESS 0x42580EC8
#define PINMODE_OD3_P3_18OD_BITBAND (*(volatile unsigned *)0x42580EC8)
#define PINMODE_OD3_P3_18OD_BIT 18
#define PINMODE_OD3_P3_19OD_MASK 0x80000
#define PINMODE_OD3_P3_19OD 0x80000
#define PINMODE_OD3_P3_19OD_BITBAND_ADDRESS 0x42580ECC
#define PINMODE_OD3_P3_19OD_BITBAND (*(volatile unsigned *)0x42580ECC)
#define PINMODE_OD3_P3_19OD_BIT 19
#define PINMODE_OD3_P3_20OD_MASK 0x100000
#define PINMODE_OD3_P3_20OD 0x100000
#define PINMODE_OD3_P3_20OD_BITBAND_ADDRESS 0x42580ED0
#define PINMODE_OD3_P3_20OD_BITBAND (*(volatile unsigned *)0x42580ED0)
#define PINMODE_OD3_P3_20OD_BIT 20
#define PINMODE_OD3_P3_21OD_MASK 0x200000
#define PINMODE_OD3_P3_21OD 0x200000
#define PINMODE_OD3_P3_21OD_BITBAND_ADDRESS 0x42580ED4
#define PINMODE_OD3_P3_21OD_BITBAND (*(volatile unsigned *)0x42580ED4)
#define PINMODE_OD3_P3_21OD_BIT 21
#define PINMODE_OD3_P3_22OD_MASK 0x400000
#define PINMODE_OD3_P3_22OD 0x400000
#define PINMODE_OD3_P3_22OD_BITBAND_ADDRESS 0x42580ED8
#define PINMODE_OD3_P3_22OD_BITBAND (*(volatile unsigned *)0x42580ED8)
#define PINMODE_OD3_P3_22OD_BIT 22
#define PINMODE_OD3_P3_23OD_MASK 0x800000
#define PINMODE_OD3_P3_23OD 0x800000
#define PINMODE_OD3_P3_23OD_BITBAND_ADDRESS 0x42580EDC
#define PINMODE_OD3_P3_23OD_BITBAND (*(volatile unsigned *)0x42580EDC)
#define PINMODE_OD3_P3_23OD_BIT 23
#define PINMODE_OD3_P3_24OD_MASK 0x1000000
#define PINMODE_OD3_P3_24OD 0x1000000
#define PINMODE_OD3_P3_24OD_BITBAND_ADDRESS 0x42580EE0
#define PINMODE_OD3_P3_24OD_BITBAND (*(volatile unsigned *)0x42580EE0)
#define PINMODE_OD3_P3_24OD_BIT 24
#define PINMODE_OD3_P3_25OD_MASK 0x2000000
#define PINMODE_OD3_P3_25OD 0x2000000
#define PINMODE_OD3_P3_25OD_BITBAND_ADDRESS 0x42580EE4
#define PINMODE_OD3_P3_25OD_BITBAND (*(volatile unsigned *)0x42580EE4)
#define PINMODE_OD3_P3_25OD_BIT 25
#define PINMODE_OD3_P3_26OD_MASK 0x4000000
#define PINMODE_OD3_P3_26OD 0x4000000
#define PINMODE_OD3_P3_26OD_BITBAND_ADDRESS 0x42580EE8
#define PINMODE_OD3_P3_26OD_BITBAND (*(volatile unsigned *)0x42580EE8)
#define PINMODE_OD3_P3_26OD_BIT 26
#define PINMODE_OD3_P3_27OD_MASK 0x8000000
#define PINMODE_OD3_P3_27OD 0x8000000
#define PINMODE_OD3_P3_27OD_BITBAND_ADDRESS 0x42580EEC
#define PINMODE_OD3_P3_27OD_BITBAND (*(volatile unsigned *)0x42580EEC)
#define PINMODE_OD3_P3_27OD_BIT 27
#define PINMODE_OD3_P3_28OD_MASK 0x10000000
#define PINMODE_OD3_P3_28OD 0x10000000
#define PINMODE_OD3_P3_28OD_BITBAND_ADDRESS 0x42580EF0
#define PINMODE_OD3_P3_28OD_BITBAND (*(volatile unsigned *)0x42580EF0)
#define PINMODE_OD3_P3_28OD_BIT 28
#define PINMODE_OD3_P3_29OD_MASK 0x20000000
#define PINMODE_OD3_P3_29OD 0x20000000
#define PINMODE_OD3_P3_29OD_BITBAND_ADDRESS 0x42580EF4
#define PINMODE_OD3_P3_29OD_BITBAND (*(volatile unsigned *)0x42580EF4)
#define PINMODE_OD3_P3_29OD_BIT 29
#define PINMODE_OD3_P3_30OD_MASK 0x40000000
#define PINMODE_OD3_P3_30OD 0x40000000
#define PINMODE_OD3_P3_30OD_BITBAND_ADDRESS 0x42580EF8
#define PINMODE_OD3_P3_30OD_BITBAND (*(volatile unsigned *)0x42580EF8)
#define PINMODE_OD3_P3_30OD_BIT 30
#define PINMODE_OD3_P3_31OD_MASK 0x80000000
#define PINMODE_OD3_P3_31OD 0x80000000
#define PINMODE_OD3_P3_31OD_BITBAND_ADDRESS 0x42580EFC
#define PINMODE_OD3_P3_31OD_BITBAND (*(volatile unsigned *)0x42580EFC)
#define PINMODE_OD3_P3_31OD_BIT 31

#define PINMODE_OD4 (*(volatile unsigned long *)0x4002C078)
#define PINMODE_OD4_OFFSET 0x78
#define PINMODE_OD4_P4_0OD_MASK 0x1
#define PINMODE_OD4_P4_0OD 0x1
#define PINMODE_OD4_P4_0OD_BITBAND_ADDRESS 0x42580F00
#define PINMODE_OD4_P4_0OD_BITBAND (*(volatile unsigned *)0x42580F00)
#define PINMODE_OD4_P4_0OD_BIT 0
#define PINMODE_OD4_P4_1OD_MASK 0x2
#define PINMODE_OD4_P4_1OD 0x2
#define PINMODE_OD4_P4_1OD_BITBAND_ADDRESS 0x42580F04
#define PINMODE_OD4_P4_1OD_BITBAND (*(volatile unsigned *)0x42580F04)
#define PINMODE_OD4_P4_1OD_BIT 1
#define PINMODE_OD4_P4_2OD_MASK 0x4
#define PINMODE_OD4_P4_2OD 0x4
#define PINMODE_OD4_P4_2OD_BITBAND_ADDRESS 0x42580F08
#define PINMODE_OD4_P4_2OD_BITBAND (*(volatile unsigned *)0x42580F08)
#define PINMODE_OD4_P4_2OD_BIT 2
#define PINMODE_OD4_P4_3OD_MASK 0x8
#define PINMODE_OD4_P4_3OD 0x8
#define PINMODE_OD4_P4_3OD_BITBAND_ADDRESS 0x42580F0C
#define PINMODE_OD4_P4_3OD_BITBAND (*(volatile unsigned *)0x42580F0C)
#define PINMODE_OD4_P4_3OD_BIT 3
#define PINMODE_OD4_P4_4OD_MASK 0x10
#define PINMODE_OD4_P4_4OD 0x10
#define PINMODE_OD4_P4_4OD_BITBAND_ADDRESS 0x42580F10
#define PINMODE_OD4_P4_4OD_BITBAND (*(volatile unsigned *)0x42580F10)
#define PINMODE_OD4_P4_4OD_BIT 4
#define PINMODE_OD4_P4_5OD_MASK 0x20
#define PINMODE_OD4_P4_5OD 0x20
#define PINMODE_OD4_P4_5OD_BITBAND_ADDRESS 0x42580F14
#define PINMODE_OD4_P4_5OD_BITBAND (*(volatile unsigned *)0x42580F14)
#define PINMODE_OD4_P4_5OD_BIT 5
#define PINMODE_OD4_P4_6OD_MASK 0x40
#define PINMODE_OD4_P4_6OD 0x40
#define PINMODE_OD4_P4_6OD_BITBAND_ADDRESS 0x42580F18
#define PINMODE_OD4_P4_6OD_BITBAND (*(volatile unsigned *)0x42580F18)
#define PINMODE_OD4_P4_6OD_BIT 6
#define PINMODE_OD4_P4_7OD_MASK 0x80
#define PINMODE_OD4_P4_7OD 0x80
#define PINMODE_OD4_P4_7OD_BITBAND_ADDRESS 0x42580F1C
#define PINMODE_OD4_P4_7OD_BITBAND (*(volatile unsigned *)0x42580F1C)
#define PINMODE_OD4_P4_7OD_BIT 7
#define PINMODE_OD4_P4_8OD_MASK 0x100
#define PINMODE_OD4_P4_8OD 0x100
#define PINMODE_OD4_P4_8OD_BITBAND_ADDRESS 0x42580F20
#define PINMODE_OD4_P4_8OD_BITBAND (*(volatile unsigned *)0x42580F20)
#define PINMODE_OD4_P4_8OD_BIT 8
#define PINMODE_OD4_P4_9OD_MASK 0x200
#define PINMODE_OD4_P4_9OD 0x200
#define PINMODE_OD4_P4_9OD_BITBAND_ADDRESS 0x42580F24
#define PINMODE_OD4_P4_9OD_BITBAND (*(volatile unsigned *)0x42580F24)
#define PINMODE_OD4_P4_9OD_BIT 9
#define PINMODE_OD4_P4_10OD_MASK 0x400
#define PINMODE_OD4_P4_10OD 0x400
#define PINMODE_OD4_P4_10OD_BITBAND_ADDRESS 0x42580F28
#define PINMODE_OD4_P4_10OD_BITBAND (*(volatile unsigned *)0x42580F28)
#define PINMODE_OD4_P4_10OD_BIT 10
#define PINMODE_OD4_P4_11OD_MASK 0x800
#define PINMODE_OD4_P4_11OD 0x800
#define PINMODE_OD4_P4_11OD_BITBAND_ADDRESS 0x42580F2C
#define PINMODE_OD4_P4_11OD_BITBAND (*(volatile unsigned *)0x42580F2C)
#define PINMODE_OD4_P4_11OD_BIT 11
#define PINMODE_OD4_P4_12OD_MASK 0x1000
#define PINMODE_OD4_P4_12OD 0x1000
#define PINMODE_OD4_P4_12OD_BITBAND_ADDRESS 0x42580F30
#define PINMODE_OD4_P4_12OD_BITBAND (*(volatile unsigned *)0x42580F30)
#define PINMODE_OD4_P4_12OD_BIT 12
#define PINMODE_OD4_P4_13OD_MASK 0x2000
#define PINMODE_OD4_P4_13OD 0x2000
#define PINMODE_OD4_P4_13OD_BITBAND_ADDRESS 0x42580F34
#define PINMODE_OD4_P4_13OD_BITBAND (*(volatile unsigned *)0x42580F34)
#define PINMODE_OD4_P4_13OD_BIT 13
#define PINMODE_OD4_P4_14OD_MASK 0x4000
#define PINMODE_OD4_P4_14OD 0x4000
#define PINMODE_OD4_P4_14OD_BITBAND_ADDRESS 0x42580F38
#define PINMODE_OD4_P4_14OD_BITBAND (*(volatile unsigned *)0x42580F38)
#define PINMODE_OD4_P4_14OD_BIT 14
#define PINMODE_OD4_P4_15OD_MASK 0x8000
#define PINMODE_OD4_P4_15OD 0x8000
#define PINMODE_OD4_P4_15OD_BITBAND_ADDRESS 0x42580F3C
#define PINMODE_OD4_P4_15OD_BITBAND (*(volatile unsigned *)0x42580F3C)
#define PINMODE_OD4_P4_15OD_BIT 15
#define PINMODE_OD4_P4_16OD_MASK 0x10000
#define PINMODE_OD4_P4_16OD 0x10000
#define PINMODE_OD4_P4_16OD_BITBAND_ADDRESS 0x42580F40
#define PINMODE_OD4_P4_16OD_BITBAND (*(volatile unsigned *)0x42580F40)
#define PINMODE_OD4_P4_16OD_BIT 16
#define PINMODE_OD4_P4_17OD_MASK 0x20000
#define PINMODE_OD4_P4_17OD 0x20000
#define PINMODE_OD4_P4_17OD_BITBAND_ADDRESS 0x42580F44
#define PINMODE_OD4_P4_17OD_BITBAND (*(volatile unsigned *)0x42580F44)
#define PINMODE_OD4_P4_17OD_BIT 17
#define PINMODE_OD4_P4_18OD_MASK 0x40000
#define PINMODE_OD4_P4_18OD 0x40000
#define PINMODE_OD4_P4_18OD_BITBAND_ADDRESS 0x42580F48
#define PINMODE_OD4_P4_18OD_BITBAND (*(volatile unsigned *)0x42580F48)
#define PINMODE_OD4_P4_18OD_BIT 18
#define PINMODE_OD4_P4_19OD_MASK 0x80000
#define PINMODE_OD4_P4_19OD 0x80000
#define PINMODE_OD4_P4_19OD_BITBAND_ADDRESS 0x42580F4C
#define PINMODE_OD4_P4_19OD_BITBAND (*(volatile unsigned *)0x42580F4C)
#define PINMODE_OD4_P4_19OD_BIT 19
#define PINMODE_OD4_P4_20OD_MASK 0x100000
#define PINMODE_OD4_P4_20OD 0x100000
#define PINMODE_OD4_P4_20OD_BITBAND_ADDRESS 0x42580F50
#define PINMODE_OD4_P4_20OD_BITBAND (*(volatile unsigned *)0x42580F50)
#define PINMODE_OD4_P4_20OD_BIT 20
#define PINMODE_OD4_P4_21OD_MASK 0x200000
#define PINMODE_OD4_P4_21OD 0x200000
#define PINMODE_OD4_P4_21OD_BITBAND_ADDRESS 0x42580F54
#define PINMODE_OD4_P4_21OD_BITBAND (*(volatile unsigned *)0x42580F54)
#define PINMODE_OD4_P4_21OD_BIT 21
#define PINMODE_OD4_P4_22OD_MASK 0x400000
#define PINMODE_OD4_P4_22OD 0x400000
#define PINMODE_OD4_P4_22OD_BITBAND_ADDRESS 0x42580F58
#define PINMODE_OD4_P4_22OD_BITBAND (*(volatile unsigned *)0x42580F58)
#define PINMODE_OD4_P4_22OD_BIT 22
#define PINMODE_OD4_P4_23OD_MASK 0x800000
#define PINMODE_OD4_P4_23OD 0x800000
#define PINMODE_OD4_P4_23OD_BITBAND_ADDRESS 0x42580F5C
#define PINMODE_OD4_P4_23OD_BITBAND (*(volatile unsigned *)0x42580F5C)
#define PINMODE_OD4_P4_23OD_BIT 23
#define PINMODE_OD4_P4_24OD_MASK 0x1000000
#define PINMODE_OD4_P4_24OD 0x1000000
#define PINMODE_OD4_P4_24OD_BITBAND_ADDRESS 0x42580F60
#define PINMODE_OD4_P4_24OD_BITBAND (*(volatile unsigned *)0x42580F60)
#define PINMODE_OD4_P4_24OD_BIT 24
#define PINMODE_OD4_P4_25OD_MASK 0x2000000
#define PINMODE_OD4_P4_25OD 0x2000000
#define PINMODE_OD4_P4_25OD_BITBAND_ADDRESS 0x42580F64
#define PINMODE_OD4_P4_25OD_BITBAND (*(volatile unsigned *)0x42580F64)
#define PINMODE_OD4_P4_25OD_BIT 25
#define PINMODE_OD4_P4_26OD_MASK 0x4000000
#define PINMODE_OD4_P4_26OD 0x4000000
#define PINMODE_OD4_P4_26OD_BITBAND_ADDRESS 0x42580F68
#define PINMODE_OD4_P4_26OD_BITBAND (*(volatile unsigned *)0x42580F68)
#define PINMODE_OD4_P4_26OD_BIT 26
#define PINMODE_OD4_P4_27OD_MASK 0x8000000
#define PINMODE_OD4_P4_27OD 0x8000000
#define PINMODE_OD4_P4_27OD_BITBAND_ADDRESS 0x42580F6C
#define PINMODE_OD4_P4_27OD_BITBAND (*(volatile unsigned *)0x42580F6C)
#define PINMODE_OD4_P4_27OD_BIT 27
#define PINMODE_OD4_P4_28OD_MASK 0x10000000
#define PINMODE_OD4_P4_28OD 0x10000000
#define PINMODE_OD4_P4_28OD_BITBAND_ADDRESS 0x42580F70
#define PINMODE_OD4_P4_28OD_BITBAND (*(volatile unsigned *)0x42580F70)
#define PINMODE_OD4_P4_28OD_BIT 28
#define PINMODE_OD4_P4_29OD_MASK 0x20000000
#define PINMODE_OD4_P4_29OD 0x20000000
#define PINMODE_OD4_P4_29OD_BITBAND_ADDRESS 0x42580F74
#define PINMODE_OD4_P4_29OD_BITBAND (*(volatile unsigned *)0x42580F74)
#define PINMODE_OD4_P4_29OD_BIT 29
#define PINMODE_OD4_P4_30OD_MASK 0x40000000
#define PINMODE_OD4_P4_30OD 0x40000000
#define PINMODE_OD4_P4_30OD_BITBAND_ADDRESS 0x42580F78
#define PINMODE_OD4_P4_30OD_BITBAND (*(volatile unsigned *)0x42580F78)
#define PINMODE_OD4_P4_30OD_BIT 30
#define PINMODE_OD4_P4_31OD_MASK 0x80000000
#define PINMODE_OD4_P4_31OD 0x80000000
#define PINMODE_OD4_P4_31OD_BITBAND_ADDRESS 0x42580F7C
#define PINMODE_OD4_P4_31OD_BITBAND (*(volatile unsigned *)0x42580F7C)
#define PINMODE_OD4_P4_31OD_BIT 31

#define I2CPADCFG (*(volatile unsigned long *)0x4002C07C)
#define I2CPADCFG_OFFSET 0x7C
#define I2CPADCFG_SDADRV0_MASK 0x1
#define I2CPADCFG_SDADRV0 0x1
#define I2CPADCFG_SDADRV0_BITBAND_ADDRESS 0x42580F80
#define I2CPADCFG_SDADRV0_BITBAND (*(volatile unsigned *)0x42580F80)
#define I2CPADCFG_SDADRV0_BIT 0
#define I2CPADCFG_SDAI2C0_MASK 0x2
#define I2CPADCFG_SDAI2C0 0x2
#define I2CPADCFG_SDAI2C0_BITBAND_ADDRESS 0x42580F84
#define I2CPADCFG_SDAI2C0_BITBAND (*(volatile unsigned *)0x42580F84)
#define I2CPADCFG_SDAI2C0_BIT 1
#define I2CPADCFG_SCLDRV0_MASK 0x4
#define I2CPADCFG_SCLDRV0 0x4
#define I2CPADCFG_SCLDRV0_BITBAND_ADDRESS 0x42580F88
#define I2CPADCFG_SCLDRV0_BITBAND (*(volatile unsigned *)0x42580F88)
#define I2CPADCFG_SCLDRV0_BIT 2
#define I2CPADCFG_SCLI2C0_MASK 0x8
#define I2CPADCFG_SCLI2C0 0x8
#define I2CPADCFG_SCLI2C0_BITBAND_ADDRESS 0x42580F8C
#define I2CPADCFG_SCLI2C0_BITBAND (*(volatile unsigned *)0x42580F8C)
#define I2CPADCFG_SCLI2C0_BIT 3

#define SSP1_BASE_ADDRESS 0x40030000

#define SSP1CR0 (*(volatile unsigned long *)0x40030000)
#define SSP1CR0_OFFSET 0x0
#define SSP1CR0_SCR_MASK 0xFF00
#define SSP1CR0_SCR_BIT 8
#define SSP1CR0_CPHA_MASK 0x80
#define SSP1CR0_CPHA 0x80
#define SSP1CR0_CPHA_BITBAND_ADDRESS 0x4260001C
#define SSP1CR0_CPHA_BITBAND (*(volatile unsigned *)0x4260001C)
#define SSP1CR0_CPHA_BIT 7
#define SSP1CR0_CPOL_MASK 0x40
#define SSP1CR0_CPOL 0x40
#define SSP1CR0_CPOL_BITBAND_ADDRESS 0x42600018
#define SSP1CR0_CPOL_BITBAND (*(volatile unsigned *)0x42600018)
#define SSP1CR0_CPOL_BIT 6
#define SSP1CR0_FRF_MASK 0x30
#define SSP1CR0_FRF_BIT 4
#define SSP1CR0_DSS_MASK 0xF
#define SSP1CR0_DSS_BIT 0

#define SSP1CR1 (*(volatile unsigned long *)0x40030004)
#define SSP1CR1_OFFSET 0x4
#define SSP1CR1_SOD_MASK 0x8
#define SSP1CR1_SOD 0x8
#define SSP1CR1_SOD_BITBAND_ADDRESS 0x4260008C
#define SSP1CR1_SOD_BITBAND (*(volatile unsigned *)0x4260008C)
#define SSP1CR1_SOD_BIT 3
#define SSP1CR1_MS_MASK 0x4
#define SSP1CR1_MS 0x4
#define SSP1CR1_MS_BITBAND_ADDRESS 0x42600088
#define SSP1CR1_MS_BITBAND (*(volatile unsigned *)0x42600088)
#define SSP1CR1_MS_BIT 2
#define SSP1CR1_SSE_MASK 0x2
#define SSP1CR1_SSE 0x2
#define SSP1CR1_SSE_BITBAND_ADDRESS 0x42600084
#define SSP1CR1_SSE_BITBAND (*(volatile unsigned *)0x42600084)
#define SSP1CR1_SSE_BIT 1
#define SSP1CR1_LBM_MASK 0x1
#define SSP1CR1_LBM 0x1
#define SSP1CR1_LBM_BITBAND_ADDRESS 0x42600080
#define SSP1CR1_LBM_BITBAND (*(volatile unsigned *)0x42600080)
#define SSP1CR1_LBM_BIT 0

#define SSP1DR (*(volatile unsigned long *)0x40030008)
#define SSP1DR_OFFSET 0x8

#define SSP1SR (*(volatile unsigned long *)0x4003000C)
#define SSP1SR_OFFSET 0xC
#define SSP1SR_BSY_MASK 0x10
#define SSP1SR_BSY 0x10
#define SSP1SR_BSY_BITBAND_ADDRESS 0x42600190
#define SSP1SR_BSY_BITBAND (*(volatile unsigned *)0x42600190)
#define SSP1SR_BSY_BIT 4
#define SSP1SR_RFF_MASK 0x8
#define SSP1SR_RFF 0x8
#define SSP1SR_RFF_BITBAND_ADDRESS 0x4260018C
#define SSP1SR_RFF_BITBAND (*(volatile unsigned *)0x4260018C)
#define SSP1SR_RFF_BIT 3
#define SSP1SR_RNE_MASK 0x4
#define SSP1SR_RNE 0x4
#define SSP1SR_RNE_BITBAND_ADDRESS 0x42600188
#define SSP1SR_RNE_BITBAND (*(volatile unsigned *)0x42600188)
#define SSP1SR_RNE_BIT 2
#define SSP1SR_TNF_MASK 0x2
#define SSP1SR_TNF 0x2
#define SSP1SR_TNF_BITBAND_ADDRESS 0x42600184
#define SSP1SR_TNF_BITBAND (*(volatile unsigned *)0x42600184)
#define SSP1SR_TNF_BIT 1
#define SSP1SR_TFE_MASK 0x1
#define SSP1SR_TFE 0x1
#define SSP1SR_TFE_BITBAND_ADDRESS 0x42600180
#define SSP1SR_TFE_BITBAND (*(volatile unsigned *)0x42600180)
#define SSP1SR_TFE_BIT 0

#define SSP1CPSR (*(volatile unsigned long *)0x40030010)
#define SSP1CPSR_OFFSET 0x10
#define SSP1CPSR_CPSDVSR_MASK 0xFF
#define SSP1CPSR_CPSDVSR_BIT 0

#define SSP1IMSC (*(volatile unsigned long *)0x40030014)
#define SSP1IMSC_OFFSET 0x14
#define SSP1IMSC_TXIM_MASK 0x8
#define SSP1IMSC_TXIM 0x8
#define SSP1IMSC_TXIM_BITBAND_ADDRESS 0x4260028C
#define SSP1IMSC_TXIM_BITBAND (*(volatile unsigned *)0x4260028C)
#define SSP1IMSC_TXIM_BIT 3
#define SSP1IMSC_RXIM_MASK 0x4
#define SSP1IMSC_RXIM 0x4
#define SSP1IMSC_RXIM_BITBAND_ADDRESS 0x42600288
#define SSP1IMSC_RXIM_BITBAND (*(volatile unsigned *)0x42600288)
#define SSP1IMSC_RXIM_BIT 2
#define SSP1IMSC_RTIM_MASK 0x2
#define SSP1IMSC_RTIM 0x2
#define SSP1IMSC_RTIM_BITBAND_ADDRESS 0x42600284
#define SSP1IMSC_RTIM_BITBAND (*(volatile unsigned *)0x42600284)
#define SSP1IMSC_RTIM_BIT 1
#define SSP1IMSC_RORIM_MASK 0x1
#define SSP1IMSC_RORIM 0x1
#define SSP1IMSC_RORIM_BITBAND_ADDRESS 0x42600280
#define SSP1IMSC_RORIM_BITBAND (*(volatile unsigned *)0x42600280)
#define SSP1IMSC_RORIM_BIT 0

#define SSP1RIS (*(volatile unsigned long *)0x40030018)
#define SSP1RIS_OFFSET 0x18
#define SSP1RIS_TXRIS_MASK 0x8
#define SSP1RIS_TXRIS 0x8
#define SSP1RIS_TXRIS_BITBAND_ADDRESS 0x4260030C
#define SSP1RIS_TXRIS_BITBAND (*(volatile unsigned *)0x4260030C)
#define SSP1RIS_TXRIS_BIT 3
#define SSP1RIS_RXRIS_MASK 0x4
#define SSP1RIS_RXRIS 0x4
#define SSP1RIS_RXRIS_BITBAND_ADDRESS 0x42600308
#define SSP1RIS_RXRIS_BITBAND (*(volatile unsigned *)0x42600308)
#define SSP1RIS_RXRIS_BIT 2
#define SSP1RIS_RTRIS_MASK 0x2
#define SSP1RIS_RTRIS 0x2
#define SSP1RIS_RTRIS_BITBAND_ADDRESS 0x42600304
#define SSP1RIS_RTRIS_BITBAND (*(volatile unsigned *)0x42600304)
#define SSP1RIS_RTRIS_BIT 1
#define SSP1RIS_RORRIS_MASK 0x1
#define SSP1RIS_RORRIS 0x1
#define SSP1RIS_RORRIS_BITBAND_ADDRESS 0x42600300
#define SSP1RIS_RORRIS_BITBAND (*(volatile unsigned *)0x42600300)
#define SSP1RIS_RORRIS_BIT 0

#define SSP1MIS (*(volatile unsigned long *)0x4003001C)
#define SSP1MIS_OFFSET 0x1C
#define SSP1MIS_TXMIS_MASK 0x8
#define SSP1MIS_TXMIS 0x8
#define SSP1MIS_TXMIS_BITBAND_ADDRESS 0x4260038C
#define SSP1MIS_TXMIS_BITBAND (*(volatile unsigned *)0x4260038C)
#define SSP1MIS_TXMIS_BIT 3
#define SSP1MIS_RXMIS_MASK 0x4
#define SSP1MIS_RXMIS 0x4
#define SSP1MIS_RXMIS_BITBAND_ADDRESS 0x42600388
#define SSP1MIS_RXMIS_BITBAND (*(volatile unsigned *)0x42600388)
#define SSP1MIS_RXMIS_BIT 2
#define SSP1MIS_RTMIS_MASK 0x2
#define SSP1MIS_RTMIS 0x2
#define SSP1MIS_RTMIS_BITBAND_ADDRESS 0x42600384
#define SSP1MIS_RTMIS_BITBAND (*(volatile unsigned *)0x42600384)
#define SSP1MIS_RTMIS_BIT 1
#define SSP1MIS_RORMIS_MASK 0x1
#define SSP1MIS_RORMIS 0x1
#define SSP1MIS_RORMIS_BITBAND_ADDRESS 0x42600380
#define SSP1MIS_RORMIS_BITBAND (*(volatile unsigned *)0x42600380)
#define SSP1MIS_RORMIS_BIT 0

#define SSP1ICR (*(volatile unsigned long *)0x40030020)
#define SSP1ICR_OFFSET 0x20
#define SSP1ICR_RTIC_MASK 0x2
#define SSP1ICR_RTIC 0x2
#define SSP1ICR_RTIC_BITBAND_ADDRESS 0x42600404
#define SSP1ICR_RTIC_BITBAND (*(volatile unsigned *)0x42600404)
#define SSP1ICR_RTIC_BIT 1
#define SSP1ICR_RORIC_MASK 0x1
#define SSP1ICR_RORIC 0x1
#define SSP1ICR_RORIC_BITBAND_ADDRESS 0x42600400
#define SSP1ICR_RORIC_BITBAND (*(volatile unsigned *)0x42600400)
#define SSP1ICR_RORIC_BIT 0

#define SSP1DMACR (*(volatile unsigned long *)0x40030024)
#define SSP1DMACR_OFFSET 0x24
#define SSP1DMACR_RXDMAE_MASK 0x1
#define SSP1DMACR_RXDMAE 0x1
#define SSP1DMACR_RXDMAE_BITBAND_ADDRESS 0x42600480
#define SSP1DMACR_RXDMAE_BITBAND (*(volatile unsigned *)0x42600480)
#define SSP1DMACR_RXDMAE_BIT 0
#define SSP1DMACR_TXDMAE_MASK 0x2
#define SSP1DMACR_TXDMAE 0x2
#define SSP1DMACR_TXDMAE_BITBAND_ADDRESS 0x42600484
#define SSP1DMACR_TXDMAE_BITBAND (*(volatile unsigned *)0x42600484)
#define SSP1DMACR_TXDMAE_BIT 1

#define AD0_BASE_ADDRESS 0x40034000

#define AD0CR (*(volatile unsigned *)0x40034000)
#define AD0CR_OFFSET 0x0
#define AD0CR_SEL_MASK 0xFF
#define AD0CR_SEL_BIT 0
#define AD0CR_CLKDIV_MASK 0xFF00
#define AD0CR_CLKDIV_BIT 8
#define AD0CR_BURST_MASK 0x10000
#define AD0CR_BURST 0x10000
#define AD0CR_BURST_BITBAND_ADDRESS 0x42680040
#define AD0CR_BURST_BITBAND (*(volatile unsigned *)0x42680040)
#define AD0CR_BURST_BIT 16
#define AD0CR_PDN_MASK 0x200000
#define AD0CR_PDN 0x200000
#define AD0CR_PDN_BITBAND_ADDRESS 0x42680054
#define AD0CR_PDN_BITBAND (*(volatile unsigned *)0x42680054)
#define AD0CR_PDN_BIT 21
#define AD0CR_START_MASK 0x7000000
#define AD0CR_START_BIT 24
#define AD0CR_EDGE_MASK 0x8000000
#define AD0CR_EDGE 0x8000000
#define AD0CR_EDGE_BITBAND_ADDRESS 0x4268006C
#define AD0CR_EDGE_BITBAND (*(volatile unsigned *)0x4268006C)
#define AD0CR_EDGE_BIT 27

#define AD0GDR (*(volatile unsigned *)0x40034004)
#define AD0GDR_OFFSET 0x4
#define AD0GDR_RESULT_MASK 0xFFF0
#define AD0GDR_RESULT_BIT 4
#define AD0GDR_CHN_MASK 0x7000000
#define AD0GDR_CHN_BIT 24
#define AD0GDR_OVERUN_MASK 0x40000000
#define AD0GDR_OVERUN 0x40000000
#define AD0GDR_OVERUN_BITBAND_ADDRESS 0x426800F8
#define AD0GDR_OVERUN_BITBAND (*(volatile unsigned *)0x426800F8)
#define AD0GDR_OVERUN_BIT 30
#define AD0GDR_DONE_MASK 0x80000000
#define AD0GDR_DONE 0x80000000
#define AD0GDR_DONE_BITBAND_ADDRESS 0x426800FC
#define AD0GDR_DONE_BITBAND (*(volatile unsigned *)0x426800FC)
#define AD0GDR_DONE_BIT 31

#define AD0INTEN (*(volatile unsigned *)0x4003400C)
#define AD0INTEN_OFFSET 0xC
#define AD0INTEN_ADINTEN0_MASK 0x1
#define AD0INTEN_ADINTEN0 0x1
#define AD0INTEN_ADINTEN0_BITBAND_ADDRESS 0x42680180
#define AD0INTEN_ADINTEN0_BITBAND (*(volatile unsigned *)0x42680180)
#define AD0INTEN_ADINTEN0_BIT 0
#define AD0INTEN_ADINTEN1_MASK 0x2
#define AD0INTEN_ADINTEN1 0x2
#define AD0INTEN_ADINTEN1_BITBAND_ADDRESS 0x42680184
#define AD0INTEN_ADINTEN1_BITBAND (*(volatile unsigned *)0x42680184)
#define AD0INTEN_ADINTEN1_BIT 1
#define AD0INTEN_ADINTEN2_MASK 0x4
#define AD0INTEN_ADINTEN2 0x4
#define AD0INTEN_ADINTEN2_BITBAND_ADDRESS 0x42680188
#define AD0INTEN_ADINTEN2_BITBAND (*(volatile unsigned *)0x42680188)
#define AD0INTEN_ADINTEN2_BIT 2
#define AD0INTEN_ADINTEN3_MASK 0x8
#define AD0INTEN_ADINTEN3 0x8
#define AD0INTEN_ADINTEN3_BITBAND_ADDRESS 0x4268018C
#define AD0INTEN_ADINTEN3_BITBAND (*(volatile unsigned *)0x4268018C)
#define AD0INTEN_ADINTEN3_BIT 3
#define AD0INTEN_ADINTEN4_MASK 0x10
#define AD0INTEN_ADINTEN4 0x10
#define AD0INTEN_ADINTEN4_BITBAND_ADDRESS 0x42680190
#define AD0INTEN_ADINTEN4_BITBAND (*(volatile unsigned *)0x42680190)
#define AD0INTEN_ADINTEN4_BIT 4
#define AD0INTEN_ADINTEN5_MASK 0x20
#define AD0INTEN_ADINTEN5 0x20
#define AD0INTEN_ADINTEN5_BITBAND_ADDRESS 0x42680194
#define AD0INTEN_ADINTEN5_BITBAND (*(volatile unsigned *)0x42680194)
#define AD0INTEN_ADINTEN5_BIT 5
#define AD0INTEN_ADINTEN6_MASK 0x40
#define AD0INTEN_ADINTEN6 0x40
#define AD0INTEN_ADINTEN6_BITBAND_ADDRESS 0x42680198
#define AD0INTEN_ADINTEN6_BITBAND (*(volatile unsigned *)0x42680198)
#define AD0INTEN_ADINTEN6_BIT 6
#define AD0INTEN_ADINTEN7_MASK 0x80
#define AD0INTEN_ADINTEN7 0x80
#define AD0INTEN_ADINTEN7_BITBAND_ADDRESS 0x4268019C
#define AD0INTEN_ADINTEN7_BITBAND (*(volatile unsigned *)0x4268019C)
#define AD0INTEN_ADINTEN7_BIT 7
#define AD0INTEN_ADGINTEN_MASK 0x100
#define AD0INTEN_ADGINTEN 0x100
#define AD0INTEN_ADGINTEN_BITBAND_ADDRESS 0x426801A0
#define AD0INTEN_ADGINTEN_BITBAND (*(volatile unsigned *)0x426801A0)
#define AD0INTEN_ADGINTEN_BIT 8

#define AD0DR0 (*(volatile unsigned *)0x40034010)
#define AD0DR0_OFFSET 0x10
#define AD0DR0_RESULT_MASK 0xFFF0
#define AD0DR0_RESULT_BIT 4
#define AD0DR0_OVERRUN_MASK 0x40000000
#define AD0DR0_OVERRUN 0x40000000
#define AD0DR0_OVERRUN_BITBAND_ADDRESS 0x42680278
#define AD0DR0_OVERRUN_BITBAND (*(volatile unsigned *)0x42680278)
#define AD0DR0_OVERRUN_BIT 30
#define AD0DR0_DONE_MASK 0x80000000
#define AD0DR0_DONE 0x80000000
#define AD0DR0_DONE_BITBAND_ADDRESS 0x4268027C
#define AD0DR0_DONE_BITBAND (*(volatile unsigned *)0x4268027C)
#define AD0DR0_DONE_BIT 31

#define AD0DR1 (*(volatile unsigned *)0x40034014)
#define AD0DR1_OFFSET 0x14
#define AD0DR1_RESULT_MASK 0xFFF0
#define AD0DR1_RESULT_BIT 4
#define AD0DR1_OVERRUN_MASK 0x40000000
#define AD0DR1_OVERRUN 0x40000000
#define AD0DR1_OVERRUN_BITBAND_ADDRESS 0x426802F8
#define AD0DR1_OVERRUN_BITBAND (*(volatile unsigned *)0x426802F8)
#define AD0DR1_OVERRUN_BIT 30
#define AD0DR1_DONE_MASK 0x80000000
#define AD0DR1_DONE 0x80000000
#define AD0DR1_DONE_BITBAND_ADDRESS 0x426802FC
#define AD0DR1_DONE_BITBAND (*(volatile unsigned *)0x426802FC)
#define AD0DR1_DONE_BIT 31

#define AD0DR2 (*(volatile unsigned *)0x40034018)
#define AD0DR2_OFFSET 0x18
#define AD0DR2_RESULT_MASK 0xFFF0
#define AD0DR2_RESULT_BIT 4
#define AD0DR2_OVERRUN_MASK 0x40000000
#define AD0DR2_OVERRUN 0x40000000
#define AD0DR2_OVERRUN_BITBAND_ADDRESS 0x42680378
#define AD0DR2_OVERRUN_BITBAND (*(volatile unsigned *)0x42680378)
#define AD0DR2_OVERRUN_BIT 30
#define AD0DR2_DONE_MASK 0x80000000
#define AD0DR2_DONE 0x80000000
#define AD0DR2_DONE_BITBAND_ADDRESS 0x4268037C
#define AD0DR2_DONE_BITBAND (*(volatile unsigned *)0x4268037C)
#define AD0DR2_DONE_BIT 31

#define AD0DR3 (*(volatile unsigned *)0x4003401C)
#define AD0DR3_OFFSET 0x1C
#define AD0DR3_RESULT_MASK 0xFFF0
#define AD0DR3_RESULT_BIT 4
#define AD0DR3_OVERRUN_MASK 0x40000000
#define AD0DR3_OVERRUN 0x40000000
#define AD0DR3_OVERRUN_BITBAND_ADDRESS 0x426803F8
#define AD0DR3_OVERRUN_BITBAND (*(volatile unsigned *)0x426803F8)
#define AD0DR3_OVERRUN_BIT 30
#define AD0DR3_DONE_MASK 0x80000000
#define AD0DR3_DONE 0x80000000
#define AD0DR3_DONE_BITBAND_ADDRESS 0x426803FC
#define AD0DR3_DONE_BITBAND (*(volatile unsigned *)0x426803FC)
#define AD0DR3_DONE_BIT 31

#define AD0DR4 (*(volatile unsigned *)0x40034020)
#define AD0DR4_OFFSET 0x20
#define AD0DR4_RESULT_MASK 0xFFF0
#define AD0DR4_RESULT_BIT 4
#define AD0DR4_OVERRUN_MASK 0x40000000
#define AD0DR4_OVERRUN 0x40000000
#define AD0DR4_OVERRUN_BITBAND_ADDRESS 0x42680478
#define AD0DR4_OVERRUN_BITBAND (*(volatile unsigned *)0x42680478)
#define AD0DR4_OVERRUN_BIT 30
#define AD0DR4_DONE_MASK 0x80000000
#define AD0DR4_DONE 0x80000000
#define AD0DR4_DONE_BITBAND_ADDRESS 0x4268047C
#define AD0DR4_DONE_BITBAND (*(volatile unsigned *)0x4268047C)
#define AD0DR4_DONE_BIT 31

#define AD0DR5 (*(volatile unsigned *)0x40034024)
#define AD0DR5_OFFSET 0x24
#define AD0DR5_RESULT_MASK 0xFFF0
#define AD0DR5_RESULT_BIT 4
#define AD0DR5_OVERRUN_MASK 0x40000000
#define AD0DR5_OVERRUN 0x40000000
#define AD0DR5_OVERRUN_BITBAND_ADDRESS 0x426804F8
#define AD0DR5_OVERRUN_BITBAND (*(volatile unsigned *)0x426804F8)
#define AD0DR5_OVERRUN_BIT 30
#define AD0DR5_DONE_MASK 0x80000000
#define AD0DR5_DONE 0x80000000
#define AD0DR5_DONE_BITBAND_ADDRESS 0x426804FC
#define AD0DR5_DONE_BITBAND (*(volatile unsigned *)0x426804FC)
#define AD0DR5_DONE_BIT 31

#define AD0DR6 (*(volatile unsigned *)0x40034028)
#define AD0DR6_OFFSET 0x28
#define AD0DR6_RESULT_MASK 0xFFF0
#define AD0DR6_RESULT_BIT 4
#define AD0DR6_OVERRUN_MASK 0x40000000
#define AD0DR6_OVERRUN 0x40000000
#define AD0DR6_OVERRUN_BITBAND_ADDRESS 0x42680578
#define AD0DR6_OVERRUN_BITBAND (*(volatile unsigned *)0x42680578)
#define AD0DR6_OVERRUN_BIT 30
#define AD0DR6_DONE_MASK 0x80000000
#define AD0DR6_DONE 0x80000000
#define AD0DR6_DONE_BITBAND_ADDRESS 0x4268057C
#define AD0DR6_DONE_BITBAND (*(volatile unsigned *)0x4268057C)
#define AD0DR6_DONE_BIT 31

#define AD0DR7 (*(volatile unsigned *)0x4003402C)
#define AD0DR7_OFFSET 0x2C
#define AD0DR7_RESULT_MASK 0xFFF0
#define AD0DR7_RESULT_BIT 4
#define AD0DR7_OVERRUN_MASK 0x40000000
#define AD0DR7_OVERRUN 0x40000000
#define AD0DR7_OVERRUN_BITBAND_ADDRESS 0x426805F8
#define AD0DR7_OVERRUN_BITBAND (*(volatile unsigned *)0x426805F8)
#define AD0DR7_OVERRUN_BIT 30
#define AD0DR7_DONE_MASK 0x80000000
#define AD0DR7_DONE 0x80000000
#define AD0DR7_DONE_BITBAND_ADDRESS 0x426805FC
#define AD0DR7_DONE_BITBAND (*(volatile unsigned *)0x426805FC)
#define AD0DR7_DONE_BIT 31

#define AD0STAT (*(volatile unsigned *)0x40034030)
#define AD0STAT_OFFSET 0x30
#define AD0STAT_DONE0_MASK 0x1
#define AD0STAT_DONE0 0x1
#define AD0STAT_DONE0_BITBAND_ADDRESS 0x42680600
#define AD0STAT_DONE0_BITBAND (*(volatile unsigned *)0x42680600)
#define AD0STAT_DONE0_BIT 0
#define AD0STAT_DONE1_MASK 0x2
#define AD0STAT_DONE1 0x2
#define AD0STAT_DONE1_BITBAND_ADDRESS 0x42680604
#define AD0STAT_DONE1_BITBAND (*(volatile unsigned *)0x42680604)
#define AD0STAT_DONE1_BIT 1
#define AD0STAT_DONE2_MASK 0x4
#define AD0STAT_DONE2 0x4
#define AD0STAT_DONE2_BITBAND_ADDRESS 0x42680608
#define AD0STAT_DONE2_BITBAND (*(volatile unsigned *)0x42680608)
#define AD0STAT_DONE2_BIT 2
#define AD0STAT_DONE3_MASK 0x8
#define AD0STAT_DONE3 0x8
#define AD0STAT_DONE3_BITBAND_ADDRESS 0x4268060C
#define AD0STAT_DONE3_BITBAND (*(volatile unsigned *)0x4268060C)
#define AD0STAT_DONE3_BIT 3
#define AD0STAT_DONE4_MASK 0x10
#define AD0STAT_DONE4 0x10
#define AD0STAT_DONE4_BITBAND_ADDRESS 0x42680610
#define AD0STAT_DONE4_BITBAND (*(volatile unsigned *)0x42680610)
#define AD0STAT_DONE4_BIT 4
#define AD0STAT_DONE5_MASK 0x20
#define AD0STAT_DONE5 0x20
#define AD0STAT_DONE5_BITBAND_ADDRESS 0x42680614
#define AD0STAT_DONE5_BITBAND (*(volatile unsigned *)0x42680614)
#define AD0STAT_DONE5_BIT 5
#define AD0STAT_DONE6_MASK 0x40
#define AD0STAT_DONE6 0x40
#define AD0STAT_DONE6_BITBAND_ADDRESS 0x42680618
#define AD0STAT_DONE6_BITBAND (*(volatile unsigned *)0x42680618)
#define AD0STAT_DONE6_BIT 6
#define AD0STAT_DONE7_MASK 0x80
#define AD0STAT_DONE7 0x80
#define AD0STAT_DONE7_BITBAND_ADDRESS 0x4268061C
#define AD0STAT_DONE7_BITBAND (*(volatile unsigned *)0x4268061C)
#define AD0STAT_DONE7_BIT 7
#define AD0STAT_OVERRUN0_MASK 0x100
#define AD0STAT_OVERRUN0 0x100
#define AD0STAT_OVERRUN0_BITBAND_ADDRESS 0x42680620
#define AD0STAT_OVERRUN0_BITBAND (*(volatile unsigned *)0x42680620)
#define AD0STAT_OVERRUN0_BIT 8
#define AD0STAT_OVERRUN1_MASK 0x200
#define AD0STAT_OVERRUN1 0x200
#define AD0STAT_OVERRUN1_BITBAND_ADDRESS 0x42680624
#define AD0STAT_OVERRUN1_BITBAND (*(volatile unsigned *)0x42680624)
#define AD0STAT_OVERRUN1_BIT 9
#define AD0STAT_OVERRUN2_MASK 0x400
#define AD0STAT_OVERRUN2 0x400
#define AD0STAT_OVERRUN2_BITBAND_ADDRESS 0x42680628
#define AD0STAT_OVERRUN2_BITBAND (*(volatile unsigned *)0x42680628)
#define AD0STAT_OVERRUN2_BIT 10
#define AD0STAT_OVERRUN3_MASK 0x800
#define AD0STAT_OVERRUN3 0x800
#define AD0STAT_OVERRUN3_BITBAND_ADDRESS 0x4268062C
#define AD0STAT_OVERRUN3_BITBAND (*(volatile unsigned *)0x4268062C)
#define AD0STAT_OVERRUN3_BIT 11
#define AD0STAT_OVERRUN4_MASK 0x1000
#define AD0STAT_OVERRUN4 0x1000
#define AD0STAT_OVERRUN4_BITBAND_ADDRESS 0x42680630
#define AD0STAT_OVERRUN4_BITBAND (*(volatile unsigned *)0x42680630)
#define AD0STAT_OVERRUN4_BIT 12
#define AD0STAT_OVERRUN5_MASK 0x2000
#define AD0STAT_OVERRUN5 0x2000
#define AD0STAT_OVERRUN5_BITBAND_ADDRESS 0x42680634
#define AD0STAT_OVERRUN5_BITBAND (*(volatile unsigned *)0x42680634)
#define AD0STAT_OVERRUN5_BIT 13
#define AD0STAT_OVERRUN6_MASK 0x4000
#define AD0STAT_OVERRUN6 0x4000
#define AD0STAT_OVERRUN6_BITBAND_ADDRESS 0x42680638
#define AD0STAT_OVERRUN6_BITBAND (*(volatile unsigned *)0x42680638)
#define AD0STAT_OVERRUN6_BIT 14
#define AD0STAT_OVERRUN7_MASK 0x8000
#define AD0STAT_OVERRUN7 0x8000
#define AD0STAT_OVERRUN7_BITBAND_ADDRESS 0x4268063C
#define AD0STAT_OVERRUN7_BITBAND (*(volatile unsigned *)0x4268063C)
#define AD0STAT_OVERRUN7_BIT 15
#define AD0STAT_ADINT_MASK 0x10000
#define AD0STAT_ADINT 0x10000
#define AD0STAT_ADINT_BITBAND_ADDRESS 0x42680640
#define AD0STAT_ADINT_BITBAND (*(volatile unsigned *)0x42680640)
#define AD0STAT_ADINT_BIT 16

#define AD0TRM (*(volatile unsigned *)0x40034034)
#define AD0TRM_OFFSET 0x34
#define AD0TRM_ADCOFFS_MASK 0xF0
#define AD0TRM_ADCOFFS_BIT 4
#define AD0TRM_TRIM_MASK 0xF00
#define AD0TRM_TRIM_BIT 8

#define I2C1_BASE_ADDRESS 0x4005C000

#define I2C1CONSET (*(volatile unsigned char *)0x4005C000)
#define I2C1CONSET_OFFSET 0x0
#define I2C1CONSET_AA_MASK 0x4
#define I2C1CONSET_AA 0x4
#define I2C1CONSET_AA_BITBAND_ADDRESS 0x42B80008
#define I2C1CONSET_AA_BITBAND (*(volatile unsigned *)0x42B80008)
#define I2C1CONSET_AA_BIT 2
#define I2C1CONSET_SI_MASK 0x8
#define I2C1CONSET_SI 0x8
#define I2C1CONSET_SI_BITBAND_ADDRESS 0x42B8000C
#define I2C1CONSET_SI_BITBAND (*(volatile unsigned *)0x42B8000C)
#define I2C1CONSET_SI_BIT 3
#define I2C1CONSET_STO_MASK 0x10
#define I2C1CONSET_STO 0x10
#define I2C1CONSET_STO_BITBAND_ADDRESS 0x42B80010
#define I2C1CONSET_STO_BITBAND (*(volatile unsigned *)0x42B80010)
#define I2C1CONSET_STO_BIT 4
#define I2C1CONSET_STA_MASK 0x20
#define I2C1CONSET_STA 0x20
#define I2C1CONSET_STA_BITBAND_ADDRESS 0x42B80014
#define I2C1CONSET_STA_BITBAND (*(volatile unsigned *)0x42B80014)
#define I2C1CONSET_STA_BIT 5
#define I2C1CONSET_I2EN_MASK 0x40
#define I2C1CONSET_I2EN 0x40
#define I2C1CONSET_I2EN_BITBAND_ADDRESS 0x42B80018
#define I2C1CONSET_I2EN_BITBAND (*(volatile unsigned *)0x42B80018)
#define I2C1CONSET_I2EN_BIT 6

#define I2C1STAT (*(volatile unsigned char *)0x4005C004)
#define I2C1STAT_OFFSET 0x4
#define I2C1STAT_Status_MASK 0xF8
#define I2C1STAT_Status_BIT 3

#define I2C1DAT (*(volatile unsigned char *)0x4005C008)
#define I2C1DAT_OFFSET 0x8

#define I2C1ADR0 (*(volatile unsigned char *)0x4005C00C)
#define I2C1ADR0_OFFSET 0xC
#define I2C1ADR0_GC_MASK 0x1
#define I2C1ADR0_GC 0x1
#define I2C1ADR0_GC_BITBAND_ADDRESS 0x42B80180
#define I2C1ADR0_GC_BITBAND (*(volatile unsigned *)0x42B80180)
#define I2C1ADR0_GC_BIT 0
#define I2C1ADR0_Address_MASK 0xFE
#define I2C1ADR0_Address_BIT 1

#define I2C1SCLH (*(volatile unsigned short *)0x4005C010)
#define I2C1SCLH_OFFSET 0x10

#define I2C1SCLL (*(volatile unsigned short *)0x4005C014)
#define I2C1SCLL_OFFSET 0x14

#define I2C1CONCLR (*(volatile unsigned char *)0x4005C018)
#define I2C1CONCLR_OFFSET 0x18
#define I2C1CONCLR_AAC_MASK 0x4
#define I2C1CONCLR_AAC 0x4
#define I2C1CONCLR_AAC_BITBAND_ADDRESS 0x42B80308
#define I2C1CONCLR_AAC_BITBAND (*(volatile unsigned *)0x42B80308)
#define I2C1CONCLR_AAC_BIT 2
#define I2C1CONCLR_SIC_MASK 0x8
#define I2C1CONCLR_SIC 0x8
#define I2C1CONCLR_SIC_BITBAND_ADDRESS 0x42B8030C
#define I2C1CONCLR_SIC_BITBAND (*(volatile unsigned *)0x42B8030C)
#define I2C1CONCLR_SIC_BIT 3
#define I2C1CONCLR_STAC_MASK 0x20
#define I2C1CONCLR_STAC 0x20
#define I2C1CONCLR_STAC_BITBAND_ADDRESS 0x42B80314
#define I2C1CONCLR_STAC_BITBAND (*(volatile unsigned *)0x42B80314)
#define I2C1CONCLR_STAC_BIT 5
#define I2C1CONCLR_I2ENC_MASK 0x40
#define I2C1CONCLR_I2ENC 0x40
#define I2C1CONCLR_I2ENC_BITBAND_ADDRESS 0x42B80318
#define I2C1CONCLR_I2ENC_BITBAND (*(volatile unsigned *)0x42B80318)
#define I2C1CONCLR_I2ENC_BIT 6

#define I2C1MMCTRL (*(volatile unsigned char *)0x4005C01C)
#define I2C1MMCTRL_OFFSET 0x1C
#define I2C1MMCTRL_MM_ENA_MASK 0x1
#define I2C1MMCTRL_MM_ENA 0x1
#define I2C1MMCTRL_MM_ENA_BITBAND_ADDRESS 0x42B80380
#define I2C1MMCTRL_MM_ENA_BITBAND (*(volatile unsigned *)0x42B80380)
#define I2C1MMCTRL_MM_ENA_BIT 0
#define I2C1MMCTRL_ENA_SCL_MASK 0x2
#define I2C1MMCTRL_ENA_SCL 0x2
#define I2C1MMCTRL_ENA_SCL_BITBAND_ADDRESS 0x42B80384
#define I2C1MMCTRL_ENA_SCL_BITBAND (*(volatile unsigned *)0x42B80384)
#define I2C1MMCTRL_ENA_SCL_BIT 1
#define I2C1MMCTRL_MATCH_ALL_MASK 0x4
#define I2C1MMCTRL_MATCH_ALL 0x4
#define I2C1MMCTRL_MATCH_ALL_BITBAND_ADDRESS 0x42B80388
#define I2C1MMCTRL_MATCH_ALL_BITBAND (*(volatile unsigned *)0x42B80388)
#define I2C1MMCTRL_MATCH_ALL_BIT 2

#define I2C1ADR1 (*(volatile unsigned char *)0x4005C020)
#define I2C1ADR1_OFFSET 0x20
#define I2C1ADR1_GC_MASK 0x1
#define I2C1ADR1_GC 0x1
#define I2C1ADR1_GC_BITBAND_ADDRESS 0x42B80400
#define I2C1ADR1_GC_BITBAND (*(volatile unsigned *)0x42B80400)
#define I2C1ADR1_GC_BIT 0
#define I2C1ADR1_Address_MASK 0xFE
#define I2C1ADR1_Address_BIT 1

#define I2C1ADR2 (*(volatile unsigned char *)0x4005C024)
#define I2C1ADR2_OFFSET 0x24
#define I2C1ADR2_GC_MASK 0x1
#define I2C1ADR2_GC 0x1
#define I2C1ADR2_GC_BITBAND_ADDRESS 0x42B80480
#define I2C1ADR2_GC_BITBAND (*(volatile unsigned *)0x42B80480)
#define I2C1ADR2_GC_BIT 0
#define I2C1ADR2_Address_MASK 0xFE
#define I2C1ADR2_Address_BIT 1

#define I2C1ADR3 (*(volatile unsigned char *)0x4005C028)
#define I2C1ADR3_OFFSET 0x28
#define I2C1ADR3_GC_MASK 0x1
#define I2C1ADR3_GC 0x1
#define I2C1ADR3_GC_BITBAND_ADDRESS 0x42B80500
#define I2C1ADR3_GC_BITBAND (*(volatile unsigned *)0x42B80500)
#define I2C1ADR3_GC_BIT 0
#define I2C1ADR3_Address_MASK 0xFE
#define I2C1ADR3_Address_BIT 1

#define I2C1DATA_BUFFER (*(volatile unsigned char *)0x4005C02C)
#define I2C1DATA_BUFFER_OFFSET 0x2C

#define I2C1MASK0 (*(volatile unsigned long *)0x4005C030)
#define I2C1MASK0_OFFSET 0x30
#define I2C1MASK0_MASK_MASK 0xFE
#define I2C1MASK0_MASK_BIT 1

#define I2C1MASK1 (*(volatile unsigned long *)0x4005C034)
#define I2C1MASK1_OFFSET 0x34
#define I2C1MASK1_MASK_MASK 0xFE
#define I2C1MASK1_MASK_BIT 1

#define I2C1MASK2 (*(volatile unsigned long *)0x4005C038)
#define I2C1MASK2_OFFSET 0x38
#define I2C1MASK2_MASK_MASK 0xFE
#define I2C1MASK2_MASK_BIT 1

#define I2C1MASK3 (*(volatile unsigned long *)0x4005C03C)
#define I2C1MASK3_OFFSET 0x3C
#define I2C1MASK3_MASK_MASK 0xFE
#define I2C1MASK3_MASK_BIT 1

#define SSP0_BASE_ADDRESS 0x40088000

#define SSP0CR0 (*(volatile unsigned long *)0x40088000)
#define SSP0CR0_OFFSET 0x0
#define SSP0CR0_SCR_MASK 0xFF00
#define SSP0CR0_SCR_BIT 8
#define SSP0CR0_CPHA_MASK 0x80
#define SSP0CR0_CPHA 0x80
#define SSP0CR0_CPHA_BITBAND_ADDRESS 0x4310001C
#define SSP0CR0_CPHA_BITBAND (*(volatile unsigned *)0x4310001C)
#define SSP0CR0_CPHA_BIT 7
#define SSP0CR0_CPOL_MASK 0x40
#define SSP0CR0_CPOL 0x40
#define SSP0CR0_CPOL_BITBAND_ADDRESS 0x43100018
#define SSP0CR0_CPOL_BITBAND (*(volatile unsigned *)0x43100018)
#define SSP0CR0_CPOL_BIT 6
#define SSP0CR0_FRF_MASK 0x30
#define SSP0CR0_FRF_BIT 4
#define SSP0CR0_DSS_MASK 0xF
#define SSP0CR0_DSS_BIT 0

#define SSP0CR1 (*(volatile unsigned long *)0x40088004)
#define SSP0CR1_OFFSET 0x4
#define SSP0CR1_SOD_MASK 0x8
#define SSP0CR1_SOD 0x8
#define SSP0CR1_SOD_BITBAND_ADDRESS 0x4310008C
#define SSP0CR1_SOD_BITBAND (*(volatile unsigned *)0x4310008C)
#define SSP0CR1_SOD_BIT 3
#define SSP0CR1_MS_MASK 0x4
#define SSP0CR1_MS 0x4
#define SSP0CR1_MS_BITBAND_ADDRESS 0x43100088
#define SSP0CR1_MS_BITBAND (*(volatile unsigned *)0x43100088)
#define SSP0CR1_MS_BIT 2
#define SSP0CR1_SSE_MASK 0x2
#define SSP0CR1_SSE 0x2
#define SSP0CR1_SSE_BITBAND_ADDRESS 0x43100084
#define SSP0CR1_SSE_BITBAND (*(volatile unsigned *)0x43100084)
#define SSP0CR1_SSE_BIT 1
#define SSP0CR1_LBM_MASK 0x1
#define SSP0CR1_LBM 0x1
#define SSP0CR1_LBM_BITBAND_ADDRESS 0x43100080
#define SSP0CR1_LBM_BITBAND (*(volatile unsigned *)0x43100080)
#define SSP0CR1_LBM_BIT 0

#define SSP0DR (*(volatile unsigned long *)0x40088008)
#define SSP0DR_OFFSET 0x8

#define SSP0SR (*(volatile unsigned long *)0x4008800C)
#define SSP0SR_OFFSET 0xC
#define SSP0SR_BSY_MASK 0x10
#define SSP0SR_BSY 0x10
#define SSP0SR_BSY_BITBAND_ADDRESS 0x43100190
#define SSP0SR_BSY_BITBAND (*(volatile unsigned *)0x43100190)
#define SSP0SR_BSY_BIT 4
#define SSP0SR_RFF_MASK 0x8
#define SSP0SR_RFF 0x8
#define SSP0SR_RFF_BITBAND_ADDRESS 0x4310018C
#define SSP0SR_RFF_BITBAND (*(volatile unsigned *)0x4310018C)
#define SSP0SR_RFF_BIT 3
#define SSP0SR_RNE_MASK 0x4
#define SSP0SR_RNE 0x4
#define SSP0SR_RNE_BITBAND_ADDRESS 0x43100188
#define SSP0SR_RNE_BITBAND (*(volatile unsigned *)0x43100188)
#define SSP0SR_RNE_BIT 2
#define SSP0SR_TNF_MASK 0x2
#define SSP0SR_TNF 0x2
#define SSP0SR_TNF_BITBAND_ADDRESS 0x43100184
#define SSP0SR_TNF_BITBAND (*(volatile unsigned *)0x43100184)
#define SSP0SR_TNF_BIT 1
#define SSP0SR_TFE_MASK 0x1
#define SSP0SR_TFE 0x1
#define SSP0SR_TFE_BITBAND_ADDRESS 0x43100180
#define SSP0SR_TFE_BITBAND (*(volatile unsigned *)0x43100180)
#define SSP0SR_TFE_BIT 0

#define SSP0CPSR (*(volatile unsigned long *)0x40088010)
#define SSP0CPSR_OFFSET 0x10
#define SSP0CPSR_CPSDVSR_MASK 0xFF
#define SSP0CPSR_CPSDVSR_BIT 0

#define SSP0IMSC (*(volatile unsigned long *)0x40088014)
#define SSP0IMSC_OFFSET 0x14
#define SSP0IMSC_TXIM_MASK 0x8
#define SSP0IMSC_TXIM 0x8
#define SSP0IMSC_TXIM_BITBAND_ADDRESS 0x4310028C
#define SSP0IMSC_TXIM_BITBAND (*(volatile unsigned *)0x4310028C)
#define SSP0IMSC_TXIM_BIT 3
#define SSP0IMSC_RXIM_MASK 0x4
#define SSP0IMSC_RXIM 0x4
#define SSP0IMSC_RXIM_BITBAND_ADDRESS 0x43100288
#define SSP0IMSC_RXIM_BITBAND (*(volatile unsigned *)0x43100288)
#define SSP0IMSC_RXIM_BIT 2
#define SSP0IMSC_RTIM_MASK 0x2
#define SSP0IMSC_RTIM 0x2
#define SSP0IMSC_RTIM_BITBAND_ADDRESS 0x43100284
#define SSP0IMSC_RTIM_BITBAND (*(volatile unsigned *)0x43100284)
#define SSP0IMSC_RTIM_BIT 1
#define SSP0IMSC_RORIM_MASK 0x1
#define SSP0IMSC_RORIM 0x1
#define SSP0IMSC_RORIM_BITBAND_ADDRESS 0x43100280
#define SSP0IMSC_RORIM_BITBAND (*(volatile unsigned *)0x43100280)
#define SSP0IMSC_RORIM_BIT 0

#define SSP0RIS (*(volatile unsigned long *)0x40088018)
#define SSP0RIS_OFFSET 0x18
#define SSP0RIS_TXRIS_MASK 0x8
#define SSP0RIS_TXRIS 0x8
#define SSP0RIS_TXRIS_BITBAND_ADDRESS 0x4310030C
#define SSP0RIS_TXRIS_BITBAND (*(volatile unsigned *)0x4310030C)
#define SSP0RIS_TXRIS_BIT 3
#define SSP0RIS_RXRIS_MASK 0x4
#define SSP0RIS_RXRIS 0x4
#define SSP0RIS_RXRIS_BITBAND_ADDRESS 0x43100308
#define SSP0RIS_RXRIS_BITBAND (*(volatile unsigned *)0x43100308)
#define SSP0RIS_RXRIS_BIT 2
#define SSP0RIS_RTRIS_MASK 0x2
#define SSP0RIS_RTRIS 0x2
#define SSP0RIS_RTRIS_BITBAND_ADDRESS 0x43100304
#define SSP0RIS_RTRIS_BITBAND (*(volatile unsigned *)0x43100304)
#define SSP0RIS_RTRIS_BIT 1
#define SSP0RIS_RORRIS_MASK 0x1
#define SSP0RIS_RORRIS 0x1
#define SSP0RIS_RORRIS_BITBAND_ADDRESS 0x43100300
#define SSP0RIS_RORRIS_BITBAND (*(volatile unsigned *)0x43100300)
#define SSP0RIS_RORRIS_BIT 0

#define SSP0MIS (*(volatile unsigned long *)0x4008801C)
#define SSP0MIS_OFFSET 0x1C
#define SSP0MIS_TXMIS_MASK 0x8
#define SSP0MIS_TXMIS 0x8
#define SSP0MIS_TXMIS_BITBAND_ADDRESS 0x4310038C
#define SSP0MIS_TXMIS_BITBAND (*(volatile unsigned *)0x4310038C)
#define SSP0MIS_TXMIS_BIT 3
#define SSP0MIS_RXMIS_MASK 0x4
#define SSP0MIS_RXMIS 0x4
#define SSP0MIS_RXMIS_BITBAND_ADDRESS 0x43100388
#define SSP0MIS_RXMIS_BITBAND (*(volatile unsigned *)0x43100388)
#define SSP0MIS_RXMIS_BIT 2
#define SSP0MIS_RTMIS_MASK 0x2
#define SSP0MIS_RTMIS 0x2
#define SSP0MIS_RTMIS_BITBAND_ADDRESS 0x43100384
#define SSP0MIS_RTMIS_BITBAND (*(volatile unsigned *)0x43100384)
#define SSP0MIS_RTMIS_BIT 1
#define SSP0MIS_RORMIS_MASK 0x1
#define SSP0MIS_RORMIS 0x1
#define SSP0MIS_RORMIS_BITBAND_ADDRESS 0x43100380
#define SSP0MIS_RORMIS_BITBAND (*(volatile unsigned *)0x43100380)
#define SSP0MIS_RORMIS_BIT 0

#define SSP0ICR (*(volatile unsigned long *)0x40088020)
#define SSP0ICR_OFFSET 0x20
#define SSP0ICR_RTIC_MASK 0x2
#define SSP0ICR_RTIC 0x2
#define SSP0ICR_RTIC_BITBAND_ADDRESS 0x43100404
#define SSP0ICR_RTIC_BITBAND (*(volatile unsigned *)0x43100404)
#define SSP0ICR_RTIC_BIT 1
#define SSP0ICR_RORIC_MASK 0x1
#define SSP0ICR_RORIC 0x1
#define SSP0ICR_RORIC_BITBAND_ADDRESS 0x43100400
#define SSP0ICR_RORIC_BITBAND (*(volatile unsigned *)0x43100400)
#define SSP0ICR_RORIC_BIT 0

#define SSP0DMACR (*(volatile unsigned long *)0x40088024)
#define SSP0DMACR_OFFSET 0x24
#define SSP0DMACR_RXDMAE_MASK 0x1
#define SSP0DMACR_RXDMAE 0x1
#define SSP0DMACR_RXDMAE_BITBAND_ADDRESS 0x43100480
#define SSP0DMACR_RXDMAE_BITBAND (*(volatile unsigned *)0x43100480)
#define SSP0DMACR_RXDMAE_BIT 0
#define SSP0DMACR_TXDMAE_MASK 0x2
#define SSP0DMACR_TXDMAE 0x2
#define SSP0DMACR_TXDMAE_BITBAND_ADDRESS 0x43100484
#define SSP0DMACR_TXDMAE_BITBAND (*(volatile unsigned *)0x43100484)
#define SSP0DMACR_TXDMAE_BIT 1

#define DAC_BASE_ADDRESS 0x4008C000

#define DACR (*(volatile unsigned long *)0x4008C000)
#define DACR_OFFSET 0x0
#define DACR_VALUE_MASK 0xFFC0
#define DACR_VALUE_BIT 6
#define DACR_BIAS_MASK 0x10000
#define DACR_BIAS 0x10000
#define DACR_BIAS_BITBAND_ADDRESS 0x43180040
#define DACR_BIAS_BITBAND (*(volatile unsigned *)0x43180040)
#define DACR_BIAS_BIT 16

#define DACCTRL (*(volatile unsigned long *)0x4008C004)
#define DACCTRL_OFFSET 0x4
#define DACCTRL_INT_DMA_REQ_MASK 0x1
#define DACCTRL_INT_DMA_REQ 0x1
#define DACCTRL_INT_DMA_REQ_BITBAND_ADDRESS 0x43180080
#define DACCTRL_INT_DMA_REQ_BITBAND (*(volatile unsigned *)0x43180080)
#define DACCTRL_INT_DMA_REQ_BIT 0
#define DACCTRL_DBLBUF_ENA_MASK 0x2
#define DACCTRL_DBLBUF_ENA 0x2
#define DACCTRL_DBLBUF_ENA_BITBAND_ADDRESS 0x43180084
#define DACCTRL_DBLBUF_ENA_BITBAND (*(volatile unsigned *)0x43180084)
#define DACCTRL_DBLBUF_ENA_BIT 1
#define DACCTRL_CNT_ENA_MASK 0x4
#define DACCTRL_CNT_ENA 0x4
#define DACCTRL_CNT_ENA_BITBAND_ADDRESS 0x43180088
#define DACCTRL_CNT_ENA_BITBAND (*(volatile unsigned *)0x43180088)
#define DACCTRL_CNT_ENA_BIT 2
#define DACCTRL_DMA_ENA_MASK 0x8
#define DACCTRL_DMA_ENA 0x8
#define DACCTRL_DMA_ENA_BITBAND_ADDRESS 0x4318008C
#define DACCTRL_DMA_ENA_BITBAND (*(volatile unsigned *)0x4318008C)
#define DACCTRL_DMA_ENA_BIT 3

#define DACCNTVAL (*(volatile unsigned long *)0x4008C008)
#define DACCNTVAL_OFFSET 0x8
#define DACCNTVAL_VALUE_MASK 0xFFFF
#define DACCNTVAL_VALUE_BIT 0

#define TIMER2_BASE_ADDRESS 0x40090000

#define T2IR (*(volatile unsigned char *)0x40090000)
#define T2IR_OFFSET 0x0
#define T2IR_MR0_MASK 0x1
#define T2IR_MR0 0x1
#define T2IR_MR0_BITBAND_ADDRESS 0x43200000
#define T2IR_MR0_BITBAND (*(volatile unsigned *)0x43200000)
#define T2IR_MR0_BIT 0
#define T2IR_MR1_MASK 0x2
#define T2IR_MR1 0x2
#define T2IR_MR1_BITBAND_ADDRESS 0x43200004
#define T2IR_MR1_BITBAND (*(volatile unsigned *)0x43200004)
#define T2IR_MR1_BIT 1
#define T2IR_MR2_MASK 0x4
#define T2IR_MR2 0x4
#define T2IR_MR2_BITBAND_ADDRESS 0x43200008
#define T2IR_MR2_BITBAND (*(volatile unsigned *)0x43200008)
#define T2IR_MR2_BIT 2
#define T2IR_MR3_MASK 0x8
#define T2IR_MR3 0x8
#define T2IR_MR3_BITBAND_ADDRESS 0x4320000C
#define T2IR_MR3_BITBAND (*(volatile unsigned *)0x4320000C)
#define T2IR_MR3_BIT 3
#define T2IR_CR0_MASK 0x10
#define T2IR_CR0 0x10
#define T2IR_CR0_BITBAND_ADDRESS 0x43200010
#define T2IR_CR0_BITBAND (*(volatile unsigned *)0x43200010)
#define T2IR_CR0_BIT 4
#define T2IR_CR1_MASK 0x20
#define T2IR_CR1 0x20
#define T2IR_CR1_BITBAND_ADDRESS 0x43200014
#define T2IR_CR1_BITBAND (*(volatile unsigned *)0x43200014)
#define T2IR_CR1_BIT 5

#define T2TCR (*(volatile unsigned char *)0x40090004)
#define T2TCR_OFFSET 0x4
#define T2TCR_Counter_Enable_MASK 0x1
#define T2TCR_Counter_Enable 0x1
#define T2TCR_Counter_Enable_BITBAND_ADDRESS 0x43200080
#define T2TCR_Counter_Enable_BITBAND (*(volatile unsigned *)0x43200080)
#define T2TCR_Counter_Enable_BIT 0
#define T2TCR_Counter_Reset_MASK 0x2
#define T2TCR_Counter_Reset 0x2
#define T2TCR_Counter_Reset_BITBAND_ADDRESS 0x43200084
#define T2TCR_Counter_Reset_BITBAND (*(volatile unsigned *)0x43200084)
#define T2TCR_Counter_Reset_BIT 1

#define T2TC (*(volatile unsigned long *)0x40090008)
#define T2TC_OFFSET 0x8

#define T2PR (*(volatile unsigned long *)0x4009000C)
#define T2PR_OFFSET 0xC

#define T2PC (*(volatile unsigned long *)0x40090010)
#define T2PC_OFFSET 0x10

#define T2MCR (*(volatile unsigned short *)0x40090014)
#define T2MCR_OFFSET 0x14
#define T2MCR_MR0I_MASK 0x1
#define T2MCR_MR0I 0x1
#define T2MCR_MR0I_BITBAND_ADDRESS 0x43200280
#define T2MCR_MR0I_BITBAND (*(volatile unsigned *)0x43200280)
#define T2MCR_MR0I_BIT 0
#define T2MCR_MR0R_MASK 0x2
#define T2MCR_MR0R 0x2
#define T2MCR_MR0R_BITBAND_ADDRESS 0x43200284
#define T2MCR_MR0R_BITBAND (*(volatile unsigned *)0x43200284)
#define T2MCR_MR0R_BIT 1
#define T2MCR_MR0S_MASK 0x4
#define T2MCR_MR0S 0x4
#define T2MCR_MR0S_BITBAND_ADDRESS 0x43200288
#define T2MCR_MR0S_BITBAND (*(volatile unsigned *)0x43200288)
#define T2MCR_MR0S_BIT 2
#define T2MCR_MR1I_MASK 0x8
#define T2MCR_MR1I 0x8
#define T2MCR_MR1I_BITBAND_ADDRESS 0x4320028C
#define T2MCR_MR1I_BITBAND (*(volatile unsigned *)0x4320028C)
#define T2MCR_MR1I_BIT 3
#define T2MCR_MR1R_MASK 0x10
#define T2MCR_MR1R 0x10
#define T2MCR_MR1R_BITBAND_ADDRESS 0x43200290
#define T2MCR_MR1R_BITBAND (*(volatile unsigned *)0x43200290)
#define T2MCR_MR1R_BIT 4
#define T2MCR_MR1S_MASK 0x20
#define T2MCR_MR1S 0x20
#define T2MCR_MR1S_BITBAND_ADDRESS 0x43200294
#define T2MCR_MR1S_BITBAND (*(volatile unsigned *)0x43200294)
#define T2MCR_MR1S_BIT 5
#define T2MCR_MR2I_MASK 0x40
#define T2MCR_MR2I 0x40
#define T2MCR_MR2I_BITBAND_ADDRESS 0x43200298
#define T2MCR_MR2I_BITBAND (*(volatile unsigned *)0x43200298)
#define T2MCR_MR2I_BIT 6
#define T2MCR_MR2R_MASK 0x80
#define T2MCR_MR2R 0x80
#define T2MCR_MR2R_BITBAND_ADDRESS 0x4320029C
#define T2MCR_MR2R_BITBAND (*(volatile unsigned *)0x4320029C)
#define T2MCR_MR2R_BIT 7
#define T2MCR_MR2S_MASK 0x100
#define T2MCR_MR2S 0x100
#define T2MCR_MR2S_BITBAND_ADDRESS 0x432002A0
#define T2MCR_MR2S_BITBAND (*(volatile unsigned *)0x432002A0)
#define T2MCR_MR2S_BIT 8
#define T2MCR_MR3I_MASK 0x200
#define T2MCR_MR3I 0x200
#define T2MCR_MR3I_BITBAND_ADDRESS 0x432002A4
#define T2MCR_MR3I_BITBAND (*(volatile unsigned *)0x432002A4)
#define T2MCR_MR3I_BIT 9
#define T2MCR_MR3R_MASK 0x400
#define T2MCR_MR3R 0x400
#define T2MCR_MR3R_BITBAND_ADDRESS 0x432002A8
#define T2MCR_MR3R_BITBAND (*(volatile unsigned *)0x432002A8)
#define T2MCR_MR3R_BIT 10
#define T2MCR_MR3S_MASK 0x800
#define T2MCR_MR3S 0x800
#define T2MCR_MR3S_BITBAND_ADDRESS 0x432002AC
#define T2MCR_MR3S_BITBAND (*(volatile unsigned *)0x432002AC)
#define T2MCR_MR3S_BIT 11

#define T2MR0 (*(volatile unsigned long *)0x40090018)
#define T2MR0_OFFSET 0x18

#define T2MR1 (*(volatile unsigned long *)0x4009001C)
#define T2MR1_OFFSET 0x1C

#define T2MR2 (*(volatile unsigned long *)0x40090020)
#define T2MR2_OFFSET 0x20

#define T2MR3 (*(volatile unsigned long *)0x40090024)
#define T2MR3_OFFSET 0x24

#define T2CCR (*(volatile unsigned short *)0x40090028)
#define T2CCR_OFFSET 0x28
#define T2CCR_CAP0RE_MASK 0x1
#define T2CCR_CAP0RE 0x1
#define T2CCR_CAP0RE_BITBAND_ADDRESS 0x43200500
#define T2CCR_CAP0RE_BITBAND (*(volatile unsigned *)0x43200500)
#define T2CCR_CAP0RE_BIT 0
#define T2CCR_CAP0FE_MASK 0x2
#define T2CCR_CAP0FE 0x2
#define T2CCR_CAP0FE_BITBAND_ADDRESS 0x43200504
#define T2CCR_CAP0FE_BITBAND (*(volatile unsigned *)0x43200504)
#define T2CCR_CAP0FE_BIT 1
#define T2CCR_CAP0I_MASK 0x4
#define T2CCR_CAP0I 0x4
#define T2CCR_CAP0I_BITBAND_ADDRESS 0x43200508
#define T2CCR_CAP0I_BITBAND (*(volatile unsigned *)0x43200508)
#define T2CCR_CAP0I_BIT 2
#define T2CCR_CAP1RE_MASK 0x8
#define T2CCR_CAP1RE 0x8
#define T2CCR_CAP1RE_BITBAND_ADDRESS 0x4320050C
#define T2CCR_CAP1RE_BITBAND (*(volatile unsigned *)0x4320050C)
#define T2CCR_CAP1RE_BIT 3
#define T2CCR_CAP1FE_MASK 0x10
#define T2CCR_CAP1FE 0x10
#define T2CCR_CAP1FE_BITBAND_ADDRESS 0x43200510
#define T2CCR_CAP1FE_BITBAND (*(volatile unsigned *)0x43200510)
#define T2CCR_CAP1FE_BIT 4
#define T2CCR_CAP1I_MASK 0x20
#define T2CCR_CAP1I 0x20
#define T2CCR_CAP1I_BITBAND_ADDRESS 0x43200514
#define T2CCR_CAP1I_BITBAND (*(volatile unsigned *)0x43200514)
#define T2CCR_CAP1I_BIT 5

#define T2CR0 (*(volatile unsigned long *)0x4009002C)
#define T2CR0_OFFSET 0x2C

#define T2CR1 (*(volatile unsigned long *)0x40090030)
#define T2CR1_OFFSET 0x30

#define T2EMR (*(volatile unsigned short *)0x4009003C)
#define T2EMR_OFFSET 0x3C
#define T2EMR_EM0_MASK 0x1
#define T2EMR_EM0 0x1
#define T2EMR_EM0_BITBAND_ADDRESS 0x43200780
#define T2EMR_EM0_BITBAND (*(volatile unsigned *)0x43200780)
#define T2EMR_EM0_BIT 0
#define T2EMR_EM1_MASK 0x2
#define T2EMR_EM1 0x2
#define T2EMR_EM1_BITBAND_ADDRESS 0x43200784
#define T2EMR_EM1_BITBAND (*(volatile unsigned *)0x43200784)
#define T2EMR_EM1_BIT 1
#define T2EMR_EM2_MASK 0x4
#define T2EMR_EM2 0x4
#define T2EMR_EM2_BITBAND_ADDRESS 0x43200788
#define T2EMR_EM2_BITBAND (*(volatile unsigned *)0x43200788)
#define T2EMR_EM2_BIT 2
#define T2EMR_EM3_MASK 0x8
#define T2EMR_EM3 0x8
#define T2EMR_EM3_BITBAND_ADDRESS 0x4320078C
#define T2EMR_EM3_BITBAND (*(volatile unsigned *)0x4320078C)
#define T2EMR_EM3_BIT 3
#define T2EMR_EMC0_MASK 0x30
#define T2EMR_EMC0_BIT 4
#define T2EMR_EMC1_MASK 0xC0
#define T2EMR_EMC1_BIT 6
#define T2EMR_EMC2_MASK 0x300
#define T2EMR_EMC2_BIT 8
#define T2EMR_EMC3_MASK 0xC00
#define T2EMR_EMC3_BIT 10

#define T2CTCR (*(volatile unsigned long *)0x40090070)
#define T2CTCR_OFFSET 0x70
#define T2CTCR_Counter_Timer_Mode_MASK 0x3
#define T2CTCR_Counter_Timer_Mode_BIT 0
#define T2CTCR_Count_Input_Select_MASK 0xC
#define T2CTCR_Count_Input_Select_BIT 2

#define TIMER3_BASE_ADDRESS 0x40094000

#define T3IR (*(volatile unsigned char *)0x40094000)
#define T3IR_OFFSET 0x0
#define T3IR_MR0_MASK 0x1
#define T3IR_MR0 0x1
#define T3IR_MR0_BITBAND_ADDRESS 0x43280000
#define T3IR_MR0_BITBAND (*(volatile unsigned *)0x43280000)
#define T3IR_MR0_BIT 0
#define T3IR_MR1_MASK 0x2
#define T3IR_MR1 0x2
#define T3IR_MR1_BITBAND_ADDRESS 0x43280004
#define T3IR_MR1_BITBAND (*(volatile unsigned *)0x43280004)
#define T3IR_MR1_BIT 1
#define T3IR_MR2_MASK 0x4
#define T3IR_MR2 0x4
#define T3IR_MR2_BITBAND_ADDRESS 0x43280008
#define T3IR_MR2_BITBAND (*(volatile unsigned *)0x43280008)
#define T3IR_MR2_BIT 2
#define T3IR_MR3_MASK 0x8
#define T3IR_MR3 0x8
#define T3IR_MR3_BITBAND_ADDRESS 0x4328000C
#define T3IR_MR3_BITBAND (*(volatile unsigned *)0x4328000C)
#define T3IR_MR3_BIT 3
#define T3IR_CR0_MASK 0x10
#define T3IR_CR0 0x10
#define T3IR_CR0_BITBAND_ADDRESS 0x43280010
#define T3IR_CR0_BITBAND (*(volatile unsigned *)0x43280010)
#define T3IR_CR0_BIT 4
#define T3IR_CR1_MASK 0x20
#define T3IR_CR1 0x20
#define T3IR_CR1_BITBAND_ADDRESS 0x43280014
#define T3IR_CR1_BITBAND (*(volatile unsigned *)0x43280014)
#define T3IR_CR1_BIT 5

#define T3TCR (*(volatile unsigned char *)0x40094004)
#define T3TCR_OFFSET 0x4
#define T3TCR_Counter_Enable_MASK 0x1
#define T3TCR_Counter_Enable 0x1
#define T3TCR_Counter_Enable_BITBAND_ADDRESS 0x43280080
#define T3TCR_Counter_Enable_BITBAND (*(volatile unsigned *)0x43280080)
#define T3TCR_Counter_Enable_BIT 0
#define T3TCR_Counter_Reset_MASK 0x2
#define T3TCR_Counter_Reset 0x2
#define T3TCR_Counter_Reset_BITBAND_ADDRESS 0x43280084
#define T3TCR_Counter_Reset_BITBAND (*(volatile unsigned *)0x43280084)
#define T3TCR_Counter_Reset_BIT 1

#define T3TC (*(volatile unsigned long *)0x40094008)
#define T3TC_OFFSET 0x8

#define T3PR (*(volatile unsigned long *)0x4009400C)
#define T3PR_OFFSET 0xC

#define T3PC (*(volatile unsigned long *)0x40094010)
#define T3PC_OFFSET 0x10

#define T3MCR (*(volatile unsigned short *)0x40094014)
#define T3MCR_OFFSET 0x14
#define T3MCR_MR0I_MASK 0x1
#define T3MCR_MR0I 0x1
#define T3MCR_MR0I_BITBAND_ADDRESS 0x43280280
#define T3MCR_MR0I_BITBAND (*(volatile unsigned *)0x43280280)
#define T3MCR_MR0I_BIT 0
#define T3MCR_MR0R_MASK 0x2
#define T3MCR_MR0R 0x2
#define T3MCR_MR0R_BITBAND_ADDRESS 0x43280284
#define T3MCR_MR0R_BITBAND (*(volatile unsigned *)0x43280284)
#define T3MCR_MR0R_BIT 1
#define T3MCR_MR0S_MASK 0x4
#define T3MCR_MR0S 0x4
#define T3MCR_MR0S_BITBAND_ADDRESS 0x43280288
#define T3MCR_MR0S_BITBAND (*(volatile unsigned *)0x43280288)
#define T3MCR_MR0S_BIT 2
#define T3MCR_MR1I_MASK 0x8
#define T3MCR_MR1I 0x8
#define T3MCR_MR1I_BITBAND_ADDRESS 0x4328028C
#define T3MCR_MR1I_BITBAND (*(volatile unsigned *)0x4328028C)
#define T3MCR_MR1I_BIT 3
#define T3MCR_MR1R_MASK 0x10
#define T3MCR_MR1R 0x10
#define T3MCR_MR1R_BITBAND_ADDRESS 0x43280290
#define T3MCR_MR1R_BITBAND (*(volatile unsigned *)0x43280290)
#define T3MCR_MR1R_BIT 4
#define T3MCR_MR1S_MASK 0x20
#define T3MCR_MR1S 0x20
#define T3MCR_MR1S_BITBAND_ADDRESS 0x43280294
#define T3MCR_MR1S_BITBAND (*(volatile unsigned *)0x43280294)
#define T3MCR_MR1S_BIT 5
#define T3MCR_MR2I_MASK 0x40
#define T3MCR_MR2I 0x40
#define T3MCR_MR2I_BITBAND_ADDRESS 0x43280298
#define T3MCR_MR2I_BITBAND (*(volatile unsigned *)0x43280298)
#define T3MCR_MR2I_BIT 6
#define T3MCR_MR2R_MASK 0x80
#define T3MCR_MR2R 0x80
#define T3MCR_MR2R_BITBAND_ADDRESS 0x4328029C
#define T3MCR_MR2R_BITBAND (*(volatile unsigned *)0x4328029C)
#define T3MCR_MR2R_BIT 7
#define T3MCR_MR2S_MASK 0x100
#define T3MCR_MR2S 0x100
#define T3MCR_MR2S_BITBAND_ADDRESS 0x432802A0
#define T3MCR_MR2S_BITBAND (*(volatile unsigned *)0x432802A0)
#define T3MCR_MR2S_BIT 8
#define T3MCR_MR3I_MASK 0x200
#define T3MCR_MR3I 0x200
#define T3MCR_MR3I_BITBAND_ADDRESS 0x432802A4
#define T3MCR_MR3I_BITBAND (*(volatile unsigned *)0x432802A4)
#define T3MCR_MR3I_BIT 9
#define T3MCR_MR3R_MASK 0x400
#define T3MCR_MR3R 0x400
#define T3MCR_MR3R_BITBAND_ADDRESS 0x432802A8
#define T3MCR_MR3R_BITBAND (*(volatile unsigned *)0x432802A8)
#define T3MCR_MR3R_BIT 10
#define T3MCR_MR3S_MASK 0x800
#define T3MCR_MR3S 0x800
#define T3MCR_MR3S_BITBAND_ADDRESS 0x432802AC
#define T3MCR_MR3S_BITBAND (*(volatile unsigned *)0x432802AC)
#define T3MCR_MR3S_BIT 11

#define T3MR0 (*(volatile unsigned long *)0x40094018)
#define T3MR0_OFFSET 0x18

#define T3MR1 (*(volatile unsigned long *)0x4009401C)
#define T3MR1_OFFSET 0x1C

#define T3MR2 (*(volatile unsigned long *)0x40094020)
#define T3MR2_OFFSET 0x20

#define T3MR3 (*(volatile unsigned long *)0x40094024)
#define T3MR3_OFFSET 0x24

#define T3CCR (*(volatile unsigned short *)0x40094028)
#define T3CCR_OFFSET 0x28
#define T3CCR_CAP0RE_MASK 0x1
#define T3CCR_CAP0RE 0x1
#define T3CCR_CAP0RE_BITBAND_ADDRESS 0x43280500
#define T3CCR_CAP0RE_BITBAND (*(volatile unsigned *)0x43280500)
#define T3CCR_CAP0RE_BIT 0
#define T3CCR_CAP0FE_MASK 0x2
#define T3CCR_CAP0FE 0x2
#define T3CCR_CAP0FE_BITBAND_ADDRESS 0x43280504
#define T3CCR_CAP0FE_BITBAND (*(volatile unsigned *)0x43280504)
#define T3CCR_CAP0FE_BIT 1
#define T3CCR_CAP0I_MASK 0x4
#define T3CCR_CAP0I 0x4
#define T3CCR_CAP0I_BITBAND_ADDRESS 0x43280508
#define T3CCR_CAP0I_BITBAND (*(volatile unsigned *)0x43280508)
#define T3CCR_CAP0I_BIT 2
#define T3CCR_CAP1RE_MASK 0x8
#define T3CCR_CAP1RE 0x8
#define T3CCR_CAP1RE_BITBAND_ADDRESS 0x4328050C
#define T3CCR_CAP1RE_BITBAND (*(volatile unsigned *)0x4328050C)
#define T3CCR_CAP1RE_BIT 3
#define T3CCR_CAP1FE_MASK 0x10
#define T3CCR_CAP1FE 0x10
#define T3CCR_CAP1FE_BITBAND_ADDRESS 0x43280510
#define T3CCR_CAP1FE_BITBAND (*(volatile unsigned *)0x43280510)
#define T3CCR_CAP1FE_BIT 4
#define T3CCR_CAP1I_MASK 0x20
#define T3CCR_CAP1I 0x20
#define T3CCR_CAP1I_BITBAND_ADDRESS 0x43280514
#define T3CCR_CAP1I_BITBAND (*(volatile unsigned *)0x43280514)
#define T3CCR_CAP1I_BIT 5

#define T3CR0 (*(volatile unsigned long *)0x4009402C)
#define T3CR0_OFFSET 0x2C

#define T3CR1 (*(volatile unsigned long *)0x40094030)
#define T3CR1_OFFSET 0x30

#define T3EMR (*(volatile unsigned short *)0x4009403C)
#define T3EMR_OFFSET 0x3C
#define T3EMR_EM0_MASK 0x1
#define T3EMR_EM0 0x1
#define T3EMR_EM0_BITBAND_ADDRESS 0x43280780
#define T3EMR_EM0_BITBAND (*(volatile unsigned *)0x43280780)
#define T3EMR_EM0_BIT 0
#define T3EMR_EM1_MASK 0x2
#define T3EMR_EM1 0x2
#define T3EMR_EM1_BITBAND_ADDRESS 0x43280784
#define T3EMR_EM1_BITBAND (*(volatile unsigned *)0x43280784)
#define T3EMR_EM1_BIT 1
#define T3EMR_EM2_MASK 0x4
#define T3EMR_EM2 0x4
#define T3EMR_EM2_BITBAND_ADDRESS 0x43280788
#define T3EMR_EM2_BITBAND (*(volatile unsigned *)0x43280788)
#define T3EMR_EM2_BIT 2
#define T3EMR_EM3_MASK 0x8
#define T3EMR_EM3 0x8
#define T3EMR_EM3_BITBAND_ADDRESS 0x4328078C
#define T3EMR_EM3_BITBAND (*(volatile unsigned *)0x4328078C)
#define T3EMR_EM3_BIT 3
#define T3EMR_EMC0_MASK 0x30
#define T3EMR_EMC0_BIT 4
#define T3EMR_EMC1_MASK 0xC0
#define T3EMR_EMC1_BIT 6
#define T3EMR_EMC2_MASK 0x300
#define T3EMR_EMC2_BIT 8
#define T3EMR_EMC3_MASK 0xC00
#define T3EMR_EMC3_BIT 10

#define T3CTCR (*(volatile unsigned long *)0x40094070)
#define T3CTCR_OFFSET 0x70
#define T3CTCR_Counter_Timer_Mode_MASK 0x3
#define T3CTCR_Counter_Timer_Mode_BIT 0
#define T3CTCR_Count_Input_Select_MASK 0xC
#define T3CTCR_Count_Input_Select_BIT 2

#define UART2_BASE_ADDRESS 0x40098000

#define U2RBR (*(volatile unsigned char *)0x40098000)
#define U2RBR_OFFSET 0x0

#define U2THR (*(volatile unsigned char *)0x40098000)
#define U2THR_OFFSET 0x0

#define U2DLL (*(volatile unsigned char *)0x40098000)
#define U2DLL_OFFSET 0x0

#define U2DLM (*(volatile unsigned char *)0x40098004)
#define U2DLM_OFFSET 0x4

#define U2IER (*(volatile unsigned long *)0x40098004)
#define U2IER_OFFSET 0x4
#define U2IER_RBR_Interrupt_Enable_MASK 0x1
#define U2IER_RBR_Interrupt_Enable 0x1
#define U2IER_RBR_Interrupt_Enable_BITBAND_ADDRESS 0x43300080
#define U2IER_RBR_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x43300080)
#define U2IER_RBR_Interrupt_Enable_BIT 0
#define U2IER_THRE_Interrupt_Enable_MASK 0x2
#define U2IER_THRE_Interrupt_Enable 0x2
#define U2IER_THRE_Interrupt_Enable_BITBAND_ADDRESS 0x43300084
#define U2IER_THRE_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x43300084)
#define U2IER_THRE_Interrupt_Enable_BIT 1
#define U2IER_Rx_Line_Status_Interrupt_Enable_MASK 0x4
#define U2IER_Rx_Line_Status_Interrupt_Enable 0x4
#define U2IER_Rx_Line_Status_Interrupt_Enable_BITBAND_ADDRESS 0x43300088
#define U2IER_Rx_Line_Status_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x43300088)
#define U2IER_Rx_Line_Status_Interrupt_Enable_BIT 2
#define U2IER_ABEOIntEn_MASK 0x100
#define U2IER_ABEOIntEn 0x100
#define U2IER_ABEOIntEn_BITBAND_ADDRESS 0x433000A0
#define U2IER_ABEOIntEn_BITBAND (*(volatile unsigned *)0x433000A0)
#define U2IER_ABEOIntEn_BIT 8
#define U2IER_ABTOIntEn_MASK 0x200
#define U2IER_ABTOIntEn 0x200
#define U2IER_ABTOIntEn_BITBAND_ADDRESS 0x433000A4
#define U2IER_ABTOIntEn_BITBAND (*(volatile unsigned *)0x433000A4)
#define U2IER_ABTOIntEn_BIT 9

#define U2IIR (*(volatile unsigned long *)0x40098008)
#define U2IIR_OFFSET 0x8
#define U2IIR_IntStatus_MASK 0x1
#define U2IIR_IntStatus 0x1
#define U2IIR_IntStatus_BITBAND_ADDRESS 0x43300100
#define U2IIR_IntStatus_BITBAND (*(volatile unsigned *)0x43300100)
#define U2IIR_IntStatus_BIT 0
#define U2IIR_IntId_MASK 0xE
#define U2IIR_IntId_BIT 1
#define U2IIR_FIFO_Enable_MASK 0xC0
#define U2IIR_FIFO_Enable_BIT 6
#define U2IIR_ABEOInt_MASK 0x100
#define U2IIR_ABEOInt 0x100
#define U2IIR_ABEOInt_BITBAND_ADDRESS 0x43300120
#define U2IIR_ABEOInt_BITBAND (*(volatile unsigned *)0x43300120)
#define U2IIR_ABEOInt_BIT 8
#define U2IIR_ABTOInt_MASK 0x200
#define U2IIR_ABTOInt 0x200
#define U2IIR_ABTOInt_BITBAND_ADDRESS 0x43300124
#define U2IIR_ABTOInt_BITBAND (*(volatile unsigned *)0x43300124)
#define U2IIR_ABTOInt_BIT 9

#define U2FCR (*(volatile unsigned char *)0x40098008)
#define U2FCR_OFFSET 0x8
#define U2FCR_FIFO_Enable_MASK 0x1
#define U2FCR_FIFO_Enable 0x1
#define U2FCR_FIFO_Enable_BITBAND_ADDRESS 0x43300100
#define U2FCR_FIFO_Enable_BITBAND (*(volatile unsigned *)0x43300100)
#define U2FCR_FIFO_Enable_BIT 0
#define U2FCR_Rx_FIFO_Reset_MASK 0x2
#define U2FCR_Rx_FIFO_Reset 0x2
#define U2FCR_Rx_FIFO_Reset_BITBAND_ADDRESS 0x43300104
#define U2FCR_Rx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x43300104)
#define U2FCR_Rx_FIFO_Reset_BIT 1
#define U2FCR_Tx_FIFO_Reset_MASK 0x4
#define U2FCR_Tx_FIFO_Reset 0x4
#define U2FCR_Tx_FIFO_Reset_BITBAND_ADDRESS 0x43300108
#define U2FCR_Tx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x43300108)
#define U2FCR_Tx_FIFO_Reset_BIT 2
#define U2FCR_DMA_Mode_Select_MASK 0x8
#define U2FCR_DMA_Mode_Select 0x8
#define U2FCR_DMA_Mode_Select_BITBAND_ADDRESS 0x4330010C
#define U2FCR_DMA_Mode_Select_BITBAND (*(volatile unsigned *)0x4330010C)
#define U2FCR_DMA_Mode_Select_BIT 3
#define U2FCR_Rx_Trigger_Level_Select_MASK 0xC0
#define U2FCR_Rx_Trigger_Level_Select_BIT 6

#define U2LCR (*(volatile unsigned char *)0x4009800C)
#define U2LCR_OFFSET 0xC
#define U2LCR_Word_Length_Select_MASK 0x3
#define U2LCR_Word_Length_Select_BIT 0
#define U2LCR_Stop_Bit_Select_MASK 0x4
#define U2LCR_Stop_Bit_Select 0x4
#define U2LCR_Stop_Bit_Select_BITBAND_ADDRESS 0x43300188
#define U2LCR_Stop_Bit_Select_BITBAND (*(volatile unsigned *)0x43300188)
#define U2LCR_Stop_Bit_Select_BIT 2
#define U2LCR_Parity_Enable_MASK 0x8
#define U2LCR_Parity_Enable 0x8
#define U2LCR_Parity_Enable_BITBAND_ADDRESS 0x4330018C
#define U2LCR_Parity_Enable_BITBAND (*(volatile unsigned *)0x4330018C)
#define U2LCR_Parity_Enable_BIT 3
#define U2LCR_Parity_Select_MASK 0x30
#define U2LCR_Parity_Select_BIT 4
#define U2LCR_Break_Control_MASK 0x40
#define U2LCR_Break_Control 0x40
#define U2LCR_Break_Control_BITBAND_ADDRESS 0x43300198
#define U2LCR_Break_Control_BITBAND (*(volatile unsigned *)0x43300198)
#define U2LCR_Break_Control_BIT 6
#define U2LCR_Divisor_Latch_Access_Bit_MASK 0x80
#define U2LCR_Divisor_Latch_Access_Bit 0x80
#define U2LCR_Divisor_Latch_Access_Bit_BITBAND_ADDRESS 0x4330019C
#define U2LCR_Divisor_Latch_Access_Bit_BITBAND (*(volatile unsigned *)0x4330019C)
#define U2LCR_Divisor_Latch_Access_Bit_BIT 7

#define U2LSR (*(volatile unsigned char *)0x40098014)
#define U2LSR_OFFSET 0x14
#define U2LSR_RDR_MASK 0x1
#define U2LSR_RDR 0x1
#define U2LSR_RDR_BITBAND_ADDRESS 0x43300280
#define U2LSR_RDR_BITBAND (*(volatile unsigned *)0x43300280)
#define U2LSR_RDR_BIT 0
#define U2LSR_OE_MASK 0x2
#define U2LSR_OE 0x2
#define U2LSR_OE_BITBAND_ADDRESS 0x43300284
#define U2LSR_OE_BITBAND (*(volatile unsigned *)0x43300284)
#define U2LSR_OE_BIT 1
#define U2LSR_PE_MASK 0x4
#define U2LSR_PE 0x4
#define U2LSR_PE_BITBAND_ADDRESS 0x43300288
#define U2LSR_PE_BITBAND (*(volatile unsigned *)0x43300288)
#define U2LSR_PE_BIT 2
#define U2LSR_FE_MASK 0x8
#define U2LSR_FE 0x8
#define U2LSR_FE_BITBAND_ADDRESS 0x4330028C
#define U2LSR_FE_BITBAND (*(volatile unsigned *)0x4330028C)
#define U2LSR_FE_BIT 3
#define U2LSR_BI_MASK 0x10
#define U2LSR_BI 0x10
#define U2LSR_BI_BITBAND_ADDRESS 0x43300290
#define U2LSR_BI_BITBAND (*(volatile unsigned *)0x43300290)
#define U2LSR_BI_BIT 4
#define U2LSR_THRE_MASK 0x20
#define U2LSR_THRE 0x20
#define U2LSR_THRE_BITBAND_ADDRESS 0x43300294
#define U2LSR_THRE_BITBAND (*(volatile unsigned *)0x43300294)
#define U2LSR_THRE_BIT 5
#define U2LSR_TEMT_MASK 0x40
#define U2LSR_TEMT 0x40
#define U2LSR_TEMT_BITBAND_ADDRESS 0x43300298
#define U2LSR_TEMT_BITBAND (*(volatile unsigned *)0x43300298)
#define U2LSR_TEMT_BIT 6
#define U2LSR_RXFE_MASK 0x80
#define U2LSR_RXFE 0x80
#define U2LSR_RXFE_BITBAND_ADDRESS 0x4330029C
#define U2LSR_RXFE_BITBAND (*(volatile unsigned *)0x4330029C)
#define U2LSR_RXFE_BIT 7

#define U2SCR (*(volatile unsigned char *)0x4009801C)
#define U2SCR_OFFSET 0x1C

#define U2ACR (*(volatile unsigned long *)0x40098020)
#define U2ACR_OFFSET 0x20
#define U2ACR_Start_MASK 0x1
#define U2ACR_Start 0x1
#define U2ACR_Start_BITBAND_ADDRESS 0x43300400
#define U2ACR_Start_BITBAND (*(volatile unsigned *)0x43300400)
#define U2ACR_Start_BIT 0
#define U2ACR_Mode_MASK 0x2
#define U2ACR_Mode 0x2
#define U2ACR_Mode_BITBAND_ADDRESS 0x43300404
#define U2ACR_Mode_BITBAND (*(volatile unsigned *)0x43300404)
#define U2ACR_Mode_BIT 1
#define U2ACR_AutoRestart_MASK 0x4
#define U2ACR_AutoRestart 0x4
#define U2ACR_AutoRestart_BITBAND_ADDRESS 0x43300408
#define U2ACR_AutoRestart_BITBAND (*(volatile unsigned *)0x43300408)
#define U2ACR_AutoRestart_BIT 2
#define U2ACR_ABEOIntClr_MASK 0x100
#define U2ACR_ABEOIntClr 0x100
#define U2ACR_ABEOIntClr_BITBAND_ADDRESS 0x43300420
#define U2ACR_ABEOIntClr_BITBAND (*(volatile unsigned *)0x43300420)
#define U2ACR_ABEOIntClr_BIT 8
#define U2ACR_ABTOIntClr_MASK 0x200
#define U2ACR_ABTOIntClr 0x200
#define U2ACR_ABTOIntClr_BITBAND_ADDRESS 0x43300424
#define U2ACR_ABTOIntClr_BITBAND (*(volatile unsigned *)0x43300424)
#define U2ACR_ABTOIntClr_BIT 9

#define U2ICR (*(volatile unsigned long *)0x40098024)
#define U2ICR_OFFSET 0x24
#define U2ICR_IrDAEn_MASK 0x1
#define U2ICR_IrDAEn 0x1
#define U2ICR_IrDAEn_BITBAND_ADDRESS 0x43300480
#define U2ICR_IrDAEn_BITBAND (*(volatile unsigned *)0x43300480)
#define U2ICR_IrDAEn_BIT 0
#define U2ICR_IrDAInv_MASK 0x2
#define U2ICR_IrDAInv 0x2
#define U2ICR_IrDAInv_BITBAND_ADDRESS 0x43300484
#define U2ICR_IrDAInv_BITBAND (*(volatile unsigned *)0x43300484)
#define U2ICR_IrDAInv_BIT 1
#define U2ICR_FixPulse_En_MASK 0x4
#define U2ICR_FixPulse_En 0x4
#define U2ICR_FixPulse_En_BITBAND_ADDRESS 0x43300488
#define U2ICR_FixPulse_En_BITBAND (*(volatile unsigned *)0x43300488)
#define U2ICR_FixPulse_En_BIT 2
#define U2ICR_PulseDiv_MASK 0x38
#define U2ICR_PulseDiv_BIT 3

#define U2FDR (*(volatile unsigned long *)0x40098028)
#define U2FDR_OFFSET 0x28
#define U2FDR_DIVADDVAL_MASK 0xF
#define U2FDR_DIVADDVAL_BIT 0
#define U2FDR_MULVAL_MASK 0xF0
#define U2FDR_MULVAL_BIT 4

#define U2TER (*(volatile unsigned char *)0x40098030)
#define U2TER_OFFSET 0x30
#define U2TER_TXEN_MASK 0x80
#define U2TER_TXEN 0x80
#define U2TER_TXEN_BITBAND_ADDRESS 0x4330061C
#define U2TER_TXEN_BITBAND (*(volatile unsigned *)0x4330061C)
#define U2TER_TXEN_BIT 7

#define U2FIFOLVL (*(volatile unsigned long *)0x40098058)
#define U2FIFOLVL_OFFSET 0x58
#define U2FIFOLVL_RXFIFILVL_MASK 0xF
#define U2FIFOLVL_RXFIFILVL_BIT 0
#define U2FIFOLVL_TXFIFOLVL_MASK 0xF00
#define U2FIFOLVL_TXFIFOLVL_BIT 8

#define UART3_BASE_ADDRESS 0x4009C000

#define U3RBR (*(volatile unsigned char *)0x4009C000)
#define U3RBR_OFFSET 0x0

#define U3THR (*(volatile unsigned char *)0x4009C000)
#define U3THR_OFFSET 0x0

#define U3DLL (*(volatile unsigned char *)0x4009C000)
#define U3DLL_OFFSET 0x0

#define U3DLM (*(volatile unsigned char *)0x4009C004)
#define U3DLM_OFFSET 0x4

#define U3IER (*(volatile unsigned long *)0x4009C004)
#define U3IER_OFFSET 0x4
#define U3IER_RBR_Interrupt_Enable_MASK 0x1
#define U3IER_RBR_Interrupt_Enable 0x1
#define U3IER_RBR_Interrupt_Enable_BITBAND_ADDRESS 0x43380080
#define U3IER_RBR_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x43380080)
#define U3IER_RBR_Interrupt_Enable_BIT 0
#define U3IER_THRE_Interrupt_Enable_MASK 0x2
#define U3IER_THRE_Interrupt_Enable 0x2
#define U3IER_THRE_Interrupt_Enable_BITBAND_ADDRESS 0x43380084
#define U3IER_THRE_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x43380084)
#define U3IER_THRE_Interrupt_Enable_BIT 1
#define U3IER_Rx_Line_Status_Interrupt_Enable_MASK 0x4
#define U3IER_Rx_Line_Status_Interrupt_Enable 0x4
#define U3IER_Rx_Line_Status_Interrupt_Enable_BITBAND_ADDRESS 0x43380088
#define U3IER_Rx_Line_Status_Interrupt_Enable_BITBAND (*(volatile unsigned *)0x43380088)
#define U3IER_Rx_Line_Status_Interrupt_Enable_BIT 2
#define U3IER_ABEOIntEn_MASK 0x100
#define U3IER_ABEOIntEn 0x100
#define U3IER_ABEOIntEn_BITBAND_ADDRESS 0x433800A0
#define U3IER_ABEOIntEn_BITBAND (*(volatile unsigned *)0x433800A0)
#define U3IER_ABEOIntEn_BIT 8
#define U3IER_ABTOIntEn_MASK 0x200
#define U3IER_ABTOIntEn 0x200
#define U3IER_ABTOIntEn_BITBAND_ADDRESS 0x433800A4
#define U3IER_ABTOIntEn_BITBAND (*(volatile unsigned *)0x433800A4)
#define U3IER_ABTOIntEn_BIT 9

#define U3IIR (*(volatile unsigned long *)0x4009C008)
#define U3IIR_OFFSET 0x8
#define U3IIR_IntStatus_MASK 0x1
#define U3IIR_IntStatus 0x1
#define U3IIR_IntStatus_BITBAND_ADDRESS 0x43380100
#define U3IIR_IntStatus_BITBAND (*(volatile unsigned *)0x43380100)
#define U3IIR_IntStatus_BIT 0
#define U3IIR_IntId_MASK 0xE
#define U3IIR_IntId_BIT 1
#define U3IIR_FIFO_Enable_MASK 0xC0
#define U3IIR_FIFO_Enable_BIT 6
#define U3IIR_ABEOInt_MASK 0x100
#define U3IIR_ABEOInt 0x100
#define U3IIR_ABEOInt_BITBAND_ADDRESS 0x43380120
#define U3IIR_ABEOInt_BITBAND (*(volatile unsigned *)0x43380120)
#define U3IIR_ABEOInt_BIT 8
#define U3IIR_ABTOInt_MASK 0x200
#define U3IIR_ABTOInt 0x200
#define U3IIR_ABTOInt_BITBAND_ADDRESS 0x43380124
#define U3IIR_ABTOInt_BITBAND (*(volatile unsigned *)0x43380124)
#define U3IIR_ABTOInt_BIT 9

#define U3FCR (*(volatile unsigned char *)0x4009C008)
#define U3FCR_OFFSET 0x8
#define U3FCR_FIFO_Enable_MASK 0x1
#define U3FCR_FIFO_Enable 0x1
#define U3FCR_FIFO_Enable_BITBAND_ADDRESS 0x43380100
#define U3FCR_FIFO_Enable_BITBAND (*(volatile unsigned *)0x43380100)
#define U3FCR_FIFO_Enable_BIT 0
#define U3FCR_Rx_FIFO_Reset_MASK 0x2
#define U3FCR_Rx_FIFO_Reset 0x2
#define U3FCR_Rx_FIFO_Reset_BITBAND_ADDRESS 0x43380104
#define U3FCR_Rx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x43380104)
#define U3FCR_Rx_FIFO_Reset_BIT 1
#define U3FCR_Tx_FIFO_Reset_MASK 0x4
#define U3FCR_Tx_FIFO_Reset 0x4
#define U3FCR_Tx_FIFO_Reset_BITBAND_ADDRESS 0x43380108
#define U3FCR_Tx_FIFO_Reset_BITBAND (*(volatile unsigned *)0x43380108)
#define U3FCR_Tx_FIFO_Reset_BIT 2
#define U3FCR_DMA_Mode_Select_MASK 0x8
#define U3FCR_DMA_Mode_Select 0x8
#define U3FCR_DMA_Mode_Select_BITBAND_ADDRESS 0x4338010C
#define U3FCR_DMA_Mode_Select_BITBAND (*(volatile unsigned *)0x4338010C)
#define U3FCR_DMA_Mode_Select_BIT 3
#define U3FCR_Rx_Trigger_Level_Select_MASK 0xC0
#define U3FCR_Rx_Trigger_Level_Select_BIT 6

#define U3LCR (*(volatile unsigned char *)0x4009C00C)
#define U3LCR_OFFSET 0xC
#define U3LCR_Word_Length_Select_MASK 0x3
#define U3LCR_Word_Length_Select_BIT 0
#define U3LCR_Stop_Bit_Select_MASK 0x4
#define U3LCR_Stop_Bit_Select 0x4
#define U3LCR_Stop_Bit_Select_BITBAND_ADDRESS 0x43380188
#define U3LCR_Stop_Bit_Select_BITBAND (*(volatile unsigned *)0x43380188)
#define U3LCR_Stop_Bit_Select_BIT 2
#define U3LCR_Parity_Enable_MASK 0x8
#define U3LCR_Parity_Enable 0x8
#define U3LCR_Parity_Enable_BITBAND_ADDRESS 0x4338018C
#define U3LCR_Parity_Enable_BITBAND (*(volatile unsigned *)0x4338018C)
#define U3LCR_Parity_Enable_BIT 3
#define U3LCR_Parity_Select_MASK 0x30
#define U3LCR_Parity_Select_BIT 4
#define U3LCR_Break_Control_MASK 0x40
#define U3LCR_Break_Control 0x40
#define U3LCR_Break_Control_BITBAND_ADDRESS 0x43380198
#define U3LCR_Break_Control_BITBAND (*(volatile unsigned *)0x43380198)
#define U3LCR_Break_Control_BIT 6
#define U3LCR_Divisor_Latch_Access_Bit_MASK 0x80
#define U3LCR_Divisor_Latch_Access_Bit 0x80
#define U3LCR_Divisor_Latch_Access_Bit_BITBAND_ADDRESS 0x4338019C
#define U3LCR_Divisor_Latch_Access_Bit_BITBAND (*(volatile unsigned *)0x4338019C)
#define U3LCR_Divisor_Latch_Access_Bit_BIT 7

#define U3LSR (*(volatile unsigned char *)0x4009C014)
#define U3LSR_OFFSET 0x14
#define U3LSR_RDR_MASK 0x1
#define U3LSR_RDR 0x1
#define U3LSR_RDR_BITBAND_ADDRESS 0x43380280
#define U3LSR_RDR_BITBAND (*(volatile unsigned *)0x43380280)
#define U3LSR_RDR_BIT 0
#define U3LSR_OE_MASK 0x2
#define U3LSR_OE 0x2
#define U3LSR_OE_BITBAND_ADDRESS 0x43380284
#define U3LSR_OE_BITBAND (*(volatile unsigned *)0x43380284)
#define U3LSR_OE_BIT 1
#define U3LSR_PE_MASK 0x4
#define U3LSR_PE 0x4
#define U3LSR_PE_BITBAND_ADDRESS 0x43380288
#define U3LSR_PE_BITBAND (*(volatile unsigned *)0x43380288)
#define U3LSR_PE_BIT 2
#define U3LSR_FE_MASK 0x8
#define U3LSR_FE 0x8
#define U3LSR_FE_BITBAND_ADDRESS 0x4338028C
#define U3LSR_FE_BITBAND (*(volatile unsigned *)0x4338028C)
#define U3LSR_FE_BIT 3
#define U3LSR_BI_MASK 0x10
#define U3LSR_BI 0x10
#define U3LSR_BI_BITBAND_ADDRESS 0x43380290
#define U3LSR_BI_BITBAND (*(volatile unsigned *)0x43380290)
#define U3LSR_BI_BIT 4
#define U3LSR_THRE_MASK 0x20
#define U3LSR_THRE 0x20
#define U3LSR_THRE_BITBAND_ADDRESS 0x43380294
#define U3LSR_THRE_BITBAND (*(volatile unsigned *)0x43380294)
#define U3LSR_THRE_BIT 5
#define U3LSR_TEMT_MASK 0x40
#define U3LSR_TEMT 0x40
#define U3LSR_TEMT_BITBAND_ADDRESS 0x43380298
#define U3LSR_TEMT_BITBAND (*(volatile unsigned *)0x43380298)
#define U3LSR_TEMT_BIT 6
#define U3LSR_RXFE_MASK 0x80
#define U3LSR_RXFE 0x80
#define U3LSR_RXFE_BITBAND_ADDRESS 0x4338029C
#define U3LSR_RXFE_BITBAND (*(volatile unsigned *)0x4338029C)
#define U3LSR_RXFE_BIT 7

#define U3SCR (*(volatile unsigned char *)0x4009C01C)
#define U3SCR_OFFSET 0x1C

#define U3ACR (*(volatile unsigned long *)0x4009C020)
#define U3ACR_OFFSET 0x20
#define U3ACR_Start_MASK 0x1
#define U3ACR_Start 0x1
#define U3ACR_Start_BITBAND_ADDRESS 0x43380400
#define U3ACR_Start_BITBAND (*(volatile unsigned *)0x43380400)
#define U3ACR_Start_BIT 0
#define U3ACR_Mode_MASK 0x2
#define U3ACR_Mode 0x2
#define U3ACR_Mode_BITBAND_ADDRESS 0x43380404
#define U3ACR_Mode_BITBAND (*(volatile unsigned *)0x43380404)
#define U3ACR_Mode_BIT 1
#define U3ACR_AutoRestart_MASK 0x4
#define U3ACR_AutoRestart 0x4
#define U3ACR_AutoRestart_BITBAND_ADDRESS 0x43380408
#define U3ACR_AutoRestart_BITBAND (*(volatile unsigned *)0x43380408)
#define U3ACR_AutoRestart_BIT 2
#define U3ACR_ABEOIntClr_MASK 0x100
#define U3ACR_ABEOIntClr 0x100
#define U3ACR_ABEOIntClr_BITBAND_ADDRESS 0x43380420
#define U3ACR_ABEOIntClr_BITBAND (*(volatile unsigned *)0x43380420)
#define U3ACR_ABEOIntClr_BIT 8
#define U3ACR_ABTOIntClr_MASK 0x200
#define U3ACR_ABTOIntClr 0x200
#define U3ACR_ABTOIntClr_BITBAND_ADDRESS 0x43380424
#define U3ACR_ABTOIntClr_BITBAND (*(volatile unsigned *)0x43380424)
#define U3ACR_ABTOIntClr_BIT 9

#define U3ICR (*(volatile unsigned long *)0x4009C024)
#define U3ICR_OFFSET 0x24
#define U3ICR_IrDAEn_MASK 0x1
#define U3ICR_IrDAEn 0x1
#define U3ICR_IrDAEn_BITBAND_ADDRESS 0x43380480
#define U3ICR_IrDAEn_BITBAND (*(volatile unsigned *)0x43380480)
#define U3ICR_IrDAEn_BIT 0
#define U3ICR_IrDAInv_MASK 0x2
#define U3ICR_IrDAInv 0x2
#define U3ICR_IrDAInv_BITBAND_ADDRESS 0x43380484
#define U3ICR_IrDAInv_BITBAND (*(volatile unsigned *)0x43380484)
#define U3ICR_IrDAInv_BIT 1
#define U3ICR_FixPulse_En_MASK 0x4
#define U3ICR_FixPulse_En 0x4
#define U3ICR_FixPulse_En_BITBAND_ADDRESS 0x43380488
#define U3ICR_FixPulse_En_BITBAND (*(volatile unsigned *)0x43380488)
#define U3ICR_FixPulse_En_BIT 2
#define U3ICR_PulseDiv_MASK 0x38
#define U3ICR_PulseDiv_BIT 3

#define U3FDR (*(volatile unsigned long *)0x4009C028)
#define U3FDR_OFFSET 0x28
#define U3FDR_DIVADDVAL_MASK 0xF
#define U3FDR_DIVADDVAL_BIT 0
#define U3FDR_MULVAL_MASK 0xF0
#define U3FDR_MULVAL_BIT 4

#define U3TER (*(volatile unsigned char *)0x4009C030)
#define U3TER_OFFSET 0x30
#define U3TER_TXEN_MASK 0x80
#define U3TER_TXEN 0x80
#define U3TER_TXEN_BITBAND_ADDRESS 0x4338061C
#define U3TER_TXEN_BITBAND (*(volatile unsigned *)0x4338061C)
#define U3TER_TXEN_BIT 7

#define U3FIFOLVL (*(volatile unsigned long *)0x4009C058)
#define U3FIFOLVL_OFFSET 0x58
#define U3FIFOLVL_RXFIFILVL_MASK 0xF
#define U3FIFOLVL_RXFIFILVL_BIT 0
#define U3FIFOLVL_TXFIFOLVL_MASK 0xF00
#define U3FIFOLVL_TXFIFOLVL_BIT 8

#define I2C2_BASE_ADDRESS 0x400A0000

#define I2C2CONSET (*(volatile unsigned char *)0x400A0000)
#define I2C2CONSET_OFFSET 0x0
#define I2C2CONSET_AA_MASK 0x4
#define I2C2CONSET_AA 0x4
#define I2C2CONSET_AA_BITBAND_ADDRESS 0x43400008
#define I2C2CONSET_AA_BITBAND (*(volatile unsigned *)0x43400008)
#define I2C2CONSET_AA_BIT 2
#define I2C2CONSET_SI_MASK 0x8
#define I2C2CONSET_SI 0x8
#define I2C2CONSET_SI_BITBAND_ADDRESS 0x4340000C
#define I2C2CONSET_SI_BITBAND (*(volatile unsigned *)0x4340000C)
#define I2C2CONSET_SI_BIT 3
#define I2C2CONSET_STO_MASK 0x10
#define I2C2CONSET_STO 0x10
#define I2C2CONSET_STO_BITBAND_ADDRESS 0x43400010
#define I2C2CONSET_STO_BITBAND (*(volatile unsigned *)0x43400010)
#define I2C2CONSET_STO_BIT 4
#define I2C2CONSET_STA_MASK 0x20
#define I2C2CONSET_STA 0x20
#define I2C2CONSET_STA_BITBAND_ADDRESS 0x43400014
#define I2C2CONSET_STA_BITBAND (*(volatile unsigned *)0x43400014)
#define I2C2CONSET_STA_BIT 5
#define I2C2CONSET_I2EN_MASK 0x40
#define I2C2CONSET_I2EN 0x40
#define I2C2CONSET_I2EN_BITBAND_ADDRESS 0x43400018
#define I2C2CONSET_I2EN_BITBAND (*(volatile unsigned *)0x43400018)
#define I2C2CONSET_I2EN_BIT 6

#define I2C2STAT (*(volatile unsigned char *)0x400A0004)
#define I2C2STAT_OFFSET 0x4
#define I2C2STAT_Status_MASK 0xF8
#define I2C2STAT_Status_BIT 3

#define I2C2DAT (*(volatile unsigned char *)0x400A0008)
#define I2C2DAT_OFFSET 0x8

#define I2C2ADR0 (*(volatile unsigned char *)0x400A000C)
#define I2C2ADR0_OFFSET 0xC
#define I2C2ADR0_GC_MASK 0x1
#define I2C2ADR0_GC 0x1
#define I2C2ADR0_GC_BITBAND_ADDRESS 0x43400180
#define I2C2ADR0_GC_BITBAND (*(volatile unsigned *)0x43400180)
#define I2C2ADR0_GC_BIT 0
#define I2C2ADR0_Address_MASK 0xFE
#define I2C2ADR0_Address_BIT 1

#define I2C2SCLH (*(volatile unsigned short *)0x400A0010)
#define I2C2SCLH_OFFSET 0x10

#define I2C2SCLL (*(volatile unsigned short *)0x400A0014)
#define I2C2SCLL_OFFSET 0x14

#define I2C2CONCLR (*(volatile unsigned char *)0x400A0018)
#define I2C2CONCLR_OFFSET 0x18
#define I2C2CONCLR_AAC_MASK 0x4
#define I2C2CONCLR_AAC 0x4
#define I2C2CONCLR_AAC_BITBAND_ADDRESS 0x43400308
#define I2C2CONCLR_AAC_BITBAND (*(volatile unsigned *)0x43400308)
#define I2C2CONCLR_AAC_BIT 2
#define I2C2CONCLR_SIC_MASK 0x8
#define I2C2CONCLR_SIC 0x8
#define I2C2CONCLR_SIC_BITBAND_ADDRESS 0x4340030C
#define I2C2CONCLR_SIC_BITBAND (*(volatile unsigned *)0x4340030C)
#define I2C2CONCLR_SIC_BIT 3
#define I2C2CONCLR_STAC_MASK 0x20
#define I2C2CONCLR_STAC 0x20
#define I2C2CONCLR_STAC_BITBAND_ADDRESS 0x43400314
#define I2C2CONCLR_STAC_BITBAND (*(volatile unsigned *)0x43400314)
#define I2C2CONCLR_STAC_BIT 5
#define I2C2CONCLR_I2ENC_MASK 0x40
#define I2C2CONCLR_I2ENC 0x40
#define I2C2CONCLR_I2ENC_BITBAND_ADDRESS 0x43400318
#define I2C2CONCLR_I2ENC_BITBAND (*(volatile unsigned *)0x43400318)
#define I2C2CONCLR_I2ENC_BIT 6

#define I2C2MMCTRL (*(volatile unsigned char *)0x400A001C)
#define I2C2MMCTRL_OFFSET 0x1C
#define I2C2MMCTRL_MM_ENA_MASK 0x1
#define I2C2MMCTRL_MM_ENA 0x1
#define I2C2MMCTRL_MM_ENA_BITBAND_ADDRESS 0x43400380
#define I2C2MMCTRL_MM_ENA_BITBAND (*(volatile unsigned *)0x43400380)
#define I2C2MMCTRL_MM_ENA_BIT 0
#define I2C2MMCTRL_ENA_SCL_MASK 0x2
#define I2C2MMCTRL_ENA_SCL 0x2
#define I2C2MMCTRL_ENA_SCL_BITBAND_ADDRESS 0x43400384
#define I2C2MMCTRL_ENA_SCL_BITBAND (*(volatile unsigned *)0x43400384)
#define I2C2MMCTRL_ENA_SCL_BIT 1
#define I2C2MMCTRL_MATCH_ALL_MASK 0x4
#define I2C2MMCTRL_MATCH_ALL 0x4
#define I2C2MMCTRL_MATCH_ALL_BITBAND_ADDRESS 0x43400388
#define I2C2MMCTRL_MATCH_ALL_BITBAND (*(volatile unsigned *)0x43400388)
#define I2C2MMCTRL_MATCH_ALL_BIT 2

#define I2C2ADR1 (*(volatile unsigned char *)0x400A0020)
#define I2C2ADR1_OFFSET 0x20
#define I2C2ADR1_GC_MASK 0x1
#define I2C2ADR1_GC 0x1
#define I2C2ADR1_GC_BITBAND_ADDRESS 0x43400400
#define I2C2ADR1_GC_BITBAND (*(volatile unsigned *)0x43400400)
#define I2C2ADR1_GC_BIT 0
#define I2C2ADR1_Address_MASK 0xFE
#define I2C2ADR1_Address_BIT 1

#define I2C2ADR2 (*(volatile unsigned char *)0x400A0024)
#define I2C2ADR2_OFFSET 0x24
#define I2C2ADR2_GC_MASK 0x1
#define I2C2ADR2_GC 0x1
#define I2C2ADR2_GC_BITBAND_ADDRESS 0x43400480
#define I2C2ADR2_GC_BITBAND (*(volatile unsigned *)0x43400480)
#define I2C2ADR2_GC_BIT 0
#define I2C2ADR2_Address_MASK 0xFE
#define I2C2ADR2_Address_BIT 1

#define I2C2ADR3 (*(volatile unsigned char *)0x400A0028)
#define I2C2ADR3_OFFSET 0x28
#define I2C2ADR3_GC_MASK 0x1
#define I2C2ADR3_GC 0x1
#define I2C2ADR3_GC_BITBAND_ADDRESS 0x43400500
#define I2C2ADR3_GC_BITBAND (*(volatile unsigned *)0x43400500)
#define I2C2ADR3_GC_BIT 0
#define I2C2ADR3_Address_MASK 0xFE
#define I2C2ADR3_Address_BIT 1

#define I2C2DATA_BUFFER (*(volatile unsigned char *)0x400A002C)
#define I2C2DATA_BUFFER_OFFSET 0x2C

#define I2C2MASK0 (*(volatile unsigned long *)0x400A0030)
#define I2C2MASK0_OFFSET 0x30
#define I2C2MASK0_MASK_MASK 0xFE
#define I2C2MASK0_MASK_BIT 1

#define I2C2MASK1 (*(volatile unsigned long *)0x400A0034)
#define I2C2MASK1_OFFSET 0x34
#define I2C2MASK1_MASK_MASK 0xFE
#define I2C2MASK1_MASK_BIT 1

#define I2C2MASK2 (*(volatile unsigned long *)0x400A0038)
#define I2C2MASK2_OFFSET 0x38
#define I2C2MASK2_MASK_MASK 0xFE
#define I2C2MASK2_MASK_BIT 1

#define I2C2MASK3 (*(volatile unsigned long *)0x400A003C)
#define I2C2MASK3_OFFSET 0x3C
#define I2C2MASK3_MASK_MASK 0xFE
#define I2C2MASK3_MASK_BIT 1

#define I2S_BASE_ADDRESS 0x400A8000

#define I2SDAO (*(volatile unsigned long *)0x400A8000)
#define I2SDAO_OFFSET 0x0
#define I2SDAO_wordwidth_MASK 0x3
#define I2SDAO_wordwidth_BIT 0
#define I2SDAO_mono_MASK 0x4
#define I2SDAO_mono 0x4
#define I2SDAO_mono_BITBAND_ADDRESS 0x43500008
#define I2SDAO_mono_BITBAND (*(volatile unsigned *)0x43500008)
#define I2SDAO_mono_BIT 2
#define I2SDAO_stop_MASK 0x8
#define I2SDAO_stop 0x8
#define I2SDAO_stop_BITBAND_ADDRESS 0x4350000C
#define I2SDAO_stop_BITBAND (*(volatile unsigned *)0x4350000C)
#define I2SDAO_stop_BIT 3
#define I2SDAO_reset_MASK 0x10
#define I2SDAO_reset 0x10
#define I2SDAO_reset_BITBAND_ADDRESS 0x43500010
#define I2SDAO_reset_BITBAND (*(volatile unsigned *)0x43500010)
#define I2SDAO_reset_BIT 4
#define I2SDAO_ws_sel_MASK 0x20
#define I2SDAO_ws_sel 0x20
#define I2SDAO_ws_sel_BITBAND_ADDRESS 0x43500014
#define I2SDAO_ws_sel_BITBAND (*(volatile unsigned *)0x43500014)
#define I2SDAO_ws_sel_BIT 5
#define I2SDAO_ws_halfperiod_MASK 0x7FC0
#define I2SDAO_ws_halfperiod_BIT 6
#define I2SDAO_mute_MASK 0x8000
#define I2SDAO_mute 0x8000
#define I2SDAO_mute_BITBAND_ADDRESS 0x4350003C
#define I2SDAO_mute_BITBAND (*(volatile unsigned *)0x4350003C)
#define I2SDAO_mute_BIT 15

#define I2SDAI (*(volatile unsigned long *)0x400A8004)
#define I2SDAI_OFFSET 0x4
#define I2SDAI_wordwidth_MASK 0x3
#define I2SDAI_wordwidth_BIT 0
#define I2SDAI_mono_MASK 0x4
#define I2SDAI_mono 0x4
#define I2SDAI_mono_BITBAND_ADDRESS 0x43500088
#define I2SDAI_mono_BITBAND (*(volatile unsigned *)0x43500088)
#define I2SDAI_mono_BIT 2
#define I2SDAI_stop_MASK 0x8
#define I2SDAI_stop 0x8
#define I2SDAI_stop_BITBAND_ADDRESS 0x4350008C
#define I2SDAI_stop_BITBAND (*(volatile unsigned *)0x4350008C)
#define I2SDAI_stop_BIT 3
#define I2SDAI_reset_MASK 0x10
#define I2SDAI_reset 0x10
#define I2SDAI_reset_BITBAND_ADDRESS 0x43500090
#define I2SDAI_reset_BITBAND (*(volatile unsigned *)0x43500090)
#define I2SDAI_reset_BIT 4
#define I2SDAI_ws_sel_MASK 0x20
#define I2SDAI_ws_sel 0x20
#define I2SDAI_ws_sel_BITBAND_ADDRESS 0x43500094
#define I2SDAI_ws_sel_BITBAND (*(volatile unsigned *)0x43500094)
#define I2SDAI_ws_sel_BIT 5
#define I2SDAI_ws_halfperiod_MASK 0x7FC0
#define I2SDAI_ws_halfperiod_BIT 6

#define I2STXFIFO (*(volatile unsigned long *)0x400A8008)
#define I2STXFIFO_OFFSET 0x8

#define I2SRXFIFO (*(volatile unsigned long *)0x400A800C)
#define I2SRXFIFO_OFFSET 0xC

#define I2SSTATE (*(volatile unsigned long *)0x400A8010)
#define I2SSTATE_OFFSET 0x10
#define I2SSTATE_irq_MASK 0x1
#define I2SSTATE_irq 0x1
#define I2SSTATE_irq_BITBAND_ADDRESS 0x43500200
#define I2SSTATE_irq_BITBAND (*(volatile unsigned *)0x43500200)
#define I2SSTATE_irq_BIT 0
#define I2SSTATE_dmareq1_MASK 0x2
#define I2SSTATE_dmareq1 0x2
#define I2SSTATE_dmareq1_BITBAND_ADDRESS 0x43500204
#define I2SSTATE_dmareq1_BITBAND (*(volatile unsigned *)0x43500204)
#define I2SSTATE_dmareq1_BIT 1
#define I2SSTATE_dmareq2_MASK 0x4
#define I2SSTATE_dmareq2 0x4
#define I2SSTATE_dmareq2_BITBAND_ADDRESS 0x43500208
#define I2SSTATE_dmareq2_BITBAND (*(volatile unsigned *)0x43500208)
#define I2SSTATE_dmareq2_BIT 2
#define I2SSTATE_rx_level_MASK 0xF00
#define I2SSTATE_rx_level_BIT 8
#define I2SSTATE_tx_level_MASK 0xF0000
#define I2SSTATE_tx_level_BIT 16

#define I2SDMA1 (*(volatile unsigned long *)0x400A8014)
#define I2SDMA1_OFFSET 0x14
#define I2SDMA1_rx_dma1_enable_MASK 0x1
#define I2SDMA1_rx_dma1_enable 0x1
#define I2SDMA1_rx_dma1_enable_BITBAND_ADDRESS 0x43500280
#define I2SDMA1_rx_dma1_enable_BITBAND (*(volatile unsigned *)0x43500280)
#define I2SDMA1_rx_dma1_enable_BIT 0
#define I2SDMA1_tx_dma1_enable_MASK 0x2
#define I2SDMA1_tx_dma1_enable 0x2
#define I2SDMA1_tx_dma1_enable_BITBAND_ADDRESS 0x43500284
#define I2SDMA1_tx_dma1_enable_BITBAND (*(volatile unsigned *)0x43500284)
#define I2SDMA1_tx_dma1_enable_BIT 1
#define I2SDMA1_rx_depth_dma1_MASK 0xF00
#define I2SDMA1_rx_depth_dma1_BIT 8
#define I2SDMA1_tx_depth_dma1_MASK 0xF0000
#define I2SDMA1_tx_depth_dma1_BIT 16

#define I2SDMA2 (*(volatile unsigned long *)0x400A8018)
#define I2SDMA2_OFFSET 0x18
#define I2SDMA2_rx_dma2_enable_MASK 0x1
#define I2SDMA2_rx_dma2_enable 0x1
#define I2SDMA2_rx_dma2_enable_BITBAND_ADDRESS 0x43500300
#define I2SDMA2_rx_dma2_enable_BITBAND (*(volatile unsigned *)0x43500300)
#define I2SDMA2_rx_dma2_enable_BIT 0
#define I2SDMA2_tx_dma2_enable_MASK 0x2
#define I2SDMA2_tx_dma2_enable 0x2
#define I2SDMA2_tx_dma2_enable_BITBAND_ADDRESS 0x43500304
#define I2SDMA2_tx_dma2_enable_BITBAND (*(volatile unsigned *)0x43500304)
#define I2SDMA2_tx_dma2_enable_BIT 1
#define I2SDMA2_rx_depth_dma2_MASK 0xF00
#define I2SDMA2_rx_depth_dma2_BIT 8
#define I2SDMA2_tx_depth_dma2_MASK 0xF0000
#define I2SDMA2_tx_depth_dma2_BIT 16

#define I2SIRQ (*(volatile unsigned long *)0x400A801C)
#define I2SIRQ_OFFSET 0x1C
#define I2SIRQ_rx_irq_enable_MASK 0x1
#define I2SIRQ_rx_irq_enable 0x1
#define I2SIRQ_rx_irq_enable_BITBAND_ADDRESS 0x43500380
#define I2SIRQ_rx_irq_enable_BITBAND (*(volatile unsigned *)0x43500380)
#define I2SIRQ_rx_irq_enable_BIT 0
#define I2SIRQ_tx_irq_enable_MASK 0x2
#define I2SIRQ_tx_irq_enable 0x2
#define I2SIRQ_tx_irq_enable_BITBAND_ADDRESS 0x43500384
#define I2SIRQ_tx_irq_enable_BITBAND (*(volatile unsigned *)0x43500384)
#define I2SIRQ_tx_irq_enable_BIT 1
#define I2SIRQ_rx_depth_irq_MASK 0xF00
#define I2SIRQ_rx_depth_irq_BIT 8
#define I2SIRQ_tx_depth_irq_MASK 0xF0000
#define I2SIRQ_tx_depth_irq_BIT 16

#define I2STXRATE (*(volatile unsigned long *)0x400A8020)
#define I2STXRATE_OFFSET 0x20
#define I2STXRATE_Y_divider_MASK 0xFF
#define I2STXRATE_Y_divider_BIT 0
#define I2STXRATE_X_divider_MASK 0xFF00
#define I2STXRATE_X_divider_BIT 8

#define I2SRXRATE (*(volatile unsigned long *)0x400A8024)
#define I2SRXRATE_OFFSET 0x24
#define I2SRXRATE_Y_divider_MASK 0xFF
#define I2SRXRATE_Y_divider_BIT 0
#define I2SRXRATE_X_divider_MASK 0xFF00
#define I2SRXRATE_X_divider_BIT 8

#define I2STXBITRATE (*(volatile unsigned long *)0x400A8028)
#define I2STXBITRATE_OFFSET 0x28
#define I2STXBITRATE_tx_bitrate_MASK 0x3F
#define I2STXBITRATE_tx_bitrate_BIT 0

#define I2SRXBITRATE (*(volatile unsigned long *)0x400A802C)
#define I2SRXBITRATE_OFFSET 0x2C
#define I2SRXBITRATE_rx_bitrate_MASK 0x3F
#define I2SRXBITRATE_rx_bitrate_BIT 0

#define I2STXMODE (*(volatile unsigned long *)0x400A8030)
#define I2STXMODE_OFFSET 0x30
#define I2STXMODE_TXCLKSEL_MASK 0x3
#define I2STXMODE_TXCLKSEL_BIT 0
#define I2STXMODE_TX4PIN_MASK 0x4
#define I2STXMODE_TX4PIN 0x4
#define I2STXMODE_TX4PIN_BITBAND_ADDRESS 0x43500608
#define I2STXMODE_TX4PIN_BITBAND (*(volatile unsigned *)0x43500608)
#define I2STXMODE_TX4PIN_BIT 2
#define I2STXMODE_TXMCENA_MASK 0x8
#define I2STXMODE_TXMCENA 0x8
#define I2STXMODE_TXMCENA_BITBAND_ADDRESS 0x4350060C
#define I2STXMODE_TXMCENA_BITBAND (*(volatile unsigned *)0x4350060C)
#define I2STXMODE_TXMCENA_BIT 3

#define I2SRXMODE (*(volatile unsigned long *)0x400A8034)
#define I2SRXMODE_OFFSET 0x34
#define I2SRXMODE_RXCLKSEL_MASK 0x3
#define I2SRXMODE_RXCLKSEL_BIT 0
#define I2SRXMODE_RX4PIN_MASK 0x4
#define I2SRXMODE_RX4PIN 0x4
#define I2SRXMODE_RX4PIN_BITBAND_ADDRESS 0x43500688
#define I2SRXMODE_RX4PIN_BITBAND (*(volatile unsigned *)0x43500688)
#define I2SRXMODE_RX4PIN_BIT 2
#define I2SRXMODE_RXMCENA_MASK 0x8
#define I2SRXMODE_RXMCENA 0x8
#define I2SRXMODE_RXMCENA_BITBAND_ADDRESS 0x4350068C
#define I2SRXMODE_RXMCENA_BITBAND (*(volatile unsigned *)0x4350068C)
#define I2SRXMODE_RXMCENA_BIT 3

#define RIT_BASE_ADDRESS 0x400B0000

#define RICOMPVAL (*(volatile unsigned long *)0x400B0000)
#define RICOMPVAL_OFFSET 0x0

#define RIMASK (*(volatile unsigned long *)0x400B0004)
#define RIMASK_OFFSET 0x4

#define RICTRL (*(volatile unsigned long *)0x400B0008)
#define RICTRL_OFFSET 0x8
#define RICTRL_RITINT_MASK 0x1
#define RICTRL_RITINT 0x1
#define RICTRL_RITINT_BITBAND_ADDRESS 0x43600100
#define RICTRL_RITINT_BITBAND (*(volatile unsigned *)0x43600100)
#define RICTRL_RITINT_BIT 0
#define RICTRL_RITENCLR_MASK 0x2
#define RICTRL_RITENCLR 0x2
#define RICTRL_RITENCLR_BITBAND_ADDRESS 0x43600104
#define RICTRL_RITENCLR_BITBAND (*(volatile unsigned *)0x43600104)
#define RICTRL_RITENCLR_BIT 1
#define RICTRL_RITENBR_MASK 0x4
#define RICTRL_RITENBR 0x4
#define RICTRL_RITENBR_BITBAND_ADDRESS 0x43600108
#define RICTRL_RITENBR_BITBAND (*(volatile unsigned *)0x43600108)
#define RICTRL_RITENBR_BIT 2
#define RICTRL_RITEN_MASK 0x8
#define RICTRL_RITEN 0x8
#define RICTRL_RITEN_BITBAND_ADDRESS 0x4360010C
#define RICTRL_RITEN_BITBAND (*(volatile unsigned *)0x4360010C)
#define RICTRL_RITEN_BIT 3

#define RICOUNTER (*(volatile unsigned long *)0x400B000C)
#define RICOUNTER_OFFSET 0xC

#define MCPWM_BASE_ADDRESS 0x400B8000

#define MCCON (*(volatile unsigned long *)0x400B8000)
#define MCCON_OFFSET 0x0
#define MCCON_RUN0_MASK 0x1
#define MCCON_RUN0 0x1
#define MCCON_RUN0_BITBAND_ADDRESS 0x43700000
#define MCCON_RUN0_BITBAND (*(volatile unsigned *)0x43700000)
#define MCCON_RUN0_BIT 0
#define MCCON_CENTER0_MASK 0x2
#define MCCON_CENTER0 0x2
#define MCCON_CENTER0_BITBAND_ADDRESS 0x43700004
#define MCCON_CENTER0_BITBAND (*(volatile unsigned *)0x43700004)
#define MCCON_CENTER0_BIT 1
#define MCCON_POLA0_MASK 0x4
#define MCCON_POLA0 0x4
#define MCCON_POLA0_BITBAND_ADDRESS 0x43700008
#define MCCON_POLA0_BITBAND (*(volatile unsigned *)0x43700008)
#define MCCON_POLA0_BIT 2
#define MCCON_DTE0_MASK 0x8
#define MCCON_DTE0 0x8
#define MCCON_DTE0_BITBAND_ADDRESS 0x4370000C
#define MCCON_DTE0_BITBAND (*(volatile unsigned *)0x4370000C)
#define MCCON_DTE0_BIT 3
#define MCCON_DISUP0_MASK 0x10
#define MCCON_DISUP0 0x10
#define MCCON_DISUP0_BITBAND_ADDRESS 0x43700010
#define MCCON_DISUP0_BITBAND (*(volatile unsigned *)0x43700010)
#define MCCON_DISUP0_BIT 4
#define MCCON_RUN1_MASK 0x100
#define MCCON_RUN1 0x100
#define MCCON_RUN1_BITBAND_ADDRESS 0x43700020
#define MCCON_RUN1_BITBAND (*(volatile unsigned *)0x43700020)
#define MCCON_RUN1_BIT 8
#define MCCON_CENTER1_MASK 0x200
#define MCCON_CENTER1 0x200
#define MCCON_CENTER1_BITBAND_ADDRESS 0x43700024
#define MCCON_CENTER1_BITBAND (*(volatile unsigned *)0x43700024)
#define MCCON_CENTER1_BIT 9
#define MCCON_POLA1_MASK 0x400
#define MCCON_POLA1 0x400
#define MCCON_POLA1_BITBAND_ADDRESS 0x43700028
#define MCCON_POLA1_BITBAND (*(volatile unsigned *)0x43700028)
#define MCCON_POLA1_BIT 10
#define MCCON_DTE1_MASK 0x800
#define MCCON_DTE1 0x800
#define MCCON_DTE1_BITBAND_ADDRESS 0x4370002C
#define MCCON_DTE1_BITBAND (*(volatile unsigned *)0x4370002C)
#define MCCON_DTE1_BIT 11
#define MCCON_DISUP1_MASK 0x1000
#define MCCON_DISUP1 0x1000
#define MCCON_DISUP1_BITBAND_ADDRESS 0x43700030
#define MCCON_DISUP1_BITBAND (*(volatile unsigned *)0x43700030)
#define MCCON_DISUP1_BIT 12
#define MCCON_RUN2_MASK 0x10000
#define MCCON_RUN2 0x10000
#define MCCON_RUN2_BITBAND_ADDRESS 0x43700040
#define MCCON_RUN2_BITBAND (*(volatile unsigned *)0x43700040)
#define MCCON_RUN2_BIT 16
#define MCCON_CENTER2_MASK 0x20000
#define MCCON_CENTER2 0x20000
#define MCCON_CENTER2_BITBAND_ADDRESS 0x43700044
#define MCCON_CENTER2_BITBAND (*(volatile unsigned *)0x43700044)
#define MCCON_CENTER2_BIT 17
#define MCCON_POLA2_MASK 0x40000
#define MCCON_POLA2 0x40000
#define MCCON_POLA2_BITBAND_ADDRESS 0x43700048
#define MCCON_POLA2_BITBAND (*(volatile unsigned *)0x43700048)
#define MCCON_POLA2_BIT 18
#define MCCON_DTE2_MASK 0x80000
#define MCCON_DTE2 0x80000
#define MCCON_DTE2_BITBAND_ADDRESS 0x4370004C
#define MCCON_DTE2_BITBAND (*(volatile unsigned *)0x4370004C)
#define MCCON_DTE2_BIT 19
#define MCCON_DISUP2_MASK 0x100000
#define MCCON_DISUP2 0x100000
#define MCCON_DISUP2_BITBAND_ADDRESS 0x43700050
#define MCCON_DISUP2_BITBAND (*(volatile unsigned *)0x43700050)
#define MCCON_DISUP2_BIT 20
#define MCCON_INVBDC_MASK 0x20000000
#define MCCON_INVBDC 0x20000000
#define MCCON_INVBDC_BITBAND_ADDRESS 0x43700074
#define MCCON_INVBDC_BITBAND (*(volatile unsigned *)0x43700074)
#define MCCON_INVBDC_BIT 29
#define MCCON_ACMODE_MASK 0x40000000
#define MCCON_ACMODE 0x40000000
#define MCCON_ACMODE_BITBAND_ADDRESS 0x43700078
#define MCCON_ACMODE_BITBAND (*(volatile unsigned *)0x43700078)
#define MCCON_ACMODE_BIT 30
#define MCCON_DCMODE_MASK 0x80000000
#define MCCON_DCMODE 0x80000000
#define MCCON_DCMODE_BITBAND_ADDRESS 0x4370007C
#define MCCON_DCMODE_BITBAND (*(volatile unsigned *)0x4370007C)
#define MCCON_DCMODE_BIT 31

#define MCCON_SET (*(volatile unsigned long *)0x400B8004)
#define MCCON_SET_OFFSET 0x4
#define MCCON_SET_RUN0_MASK 0x1
#define MCCON_SET_RUN0 0x1
#define MCCON_SET_RUN0_BITBAND_ADDRESS 0x43700080
#define MCCON_SET_RUN0_BITBAND (*(volatile unsigned *)0x43700080)
#define MCCON_SET_RUN0_BIT 0
#define MCCON_SET_CENTER0_MASK 0x2
#define MCCON_SET_CENTER0 0x2
#define MCCON_SET_CENTER0_BITBAND_ADDRESS 0x43700084
#define MCCON_SET_CENTER0_BITBAND (*(volatile unsigned *)0x43700084)
#define MCCON_SET_CENTER0_BIT 1
#define MCCON_SET_POLA0_MASK 0x4
#define MCCON_SET_POLA0 0x4
#define MCCON_SET_POLA0_BITBAND_ADDRESS 0x43700088
#define MCCON_SET_POLA0_BITBAND (*(volatile unsigned *)0x43700088)
#define MCCON_SET_POLA0_BIT 2
#define MCCON_SET_DTE0_MASK 0x8
#define MCCON_SET_DTE0 0x8
#define MCCON_SET_DTE0_BITBAND_ADDRESS 0x4370008C
#define MCCON_SET_DTE0_BITBAND (*(volatile unsigned *)0x4370008C)
#define MCCON_SET_DTE0_BIT 3
#define MCCON_SET_DISUP0_MASK 0x10
#define MCCON_SET_DISUP0 0x10
#define MCCON_SET_DISUP0_BITBAND_ADDRESS 0x43700090
#define MCCON_SET_DISUP0_BITBAND (*(volatile unsigned *)0x43700090)
#define MCCON_SET_DISUP0_BIT 4
#define MCCON_SET_RUN1_MASK 0x100
#define MCCON_SET_RUN1 0x100
#define MCCON_SET_RUN1_BITBAND_ADDRESS 0x437000A0
#define MCCON_SET_RUN1_BITBAND (*(volatile unsigned *)0x437000A0)
#define MCCON_SET_RUN1_BIT 8
#define MCCON_SET_CENTER1_MASK 0x200
#define MCCON_SET_CENTER1 0x200
#define MCCON_SET_CENTER1_BITBAND_ADDRESS 0x437000A4
#define MCCON_SET_CENTER1_BITBAND (*(volatile unsigned *)0x437000A4)
#define MCCON_SET_CENTER1_BIT 9
#define MCCON_SET_POLA1_MASK 0x400
#define MCCON_SET_POLA1 0x400
#define MCCON_SET_POLA1_BITBAND_ADDRESS 0x437000A8
#define MCCON_SET_POLA1_BITBAND (*(volatile unsigned *)0x437000A8)
#define MCCON_SET_POLA1_BIT 10
#define MCCON_SET_DTE1_MASK 0x800
#define MCCON_SET_DTE1 0x800
#define MCCON_SET_DTE1_BITBAND_ADDRESS 0x437000AC
#define MCCON_SET_DTE1_BITBAND (*(volatile unsigned *)0x437000AC)
#define MCCON_SET_DTE1_BIT 11
#define MCCON_SET_DISUP1_MASK 0x1000
#define MCCON_SET_DISUP1 0x1000
#define MCCON_SET_DISUP1_BITBAND_ADDRESS 0x437000B0
#define MCCON_SET_DISUP1_BITBAND (*(volatile unsigned *)0x437000B0)
#define MCCON_SET_DISUP1_BIT 12
#define MCCON_SET_RUN2_MASK 0x10000
#define MCCON_SET_RUN2 0x10000
#define MCCON_SET_RUN2_BITBAND_ADDRESS 0x437000C0
#define MCCON_SET_RUN2_BITBAND (*(volatile unsigned *)0x437000C0)
#define MCCON_SET_RUN2_BIT 16
#define MCCON_SET_CENTER2_MASK 0x20000
#define MCCON_SET_CENTER2 0x20000
#define MCCON_SET_CENTER2_BITBAND_ADDRESS 0x437000C4
#define MCCON_SET_CENTER2_BITBAND (*(volatile unsigned *)0x437000C4)
#define MCCON_SET_CENTER2_BIT 17
#define MCCON_SET_POLA2_MASK 0x40000
#define MCCON_SET_POLA2 0x40000
#define MCCON_SET_POLA2_BITBAND_ADDRESS 0x437000C8
#define MCCON_SET_POLA2_BITBAND (*(volatile unsigned *)0x437000C8)
#define MCCON_SET_POLA2_BIT 18
#define MCCON_SET_DTE2_MASK 0x80000
#define MCCON_SET_DTE2 0x80000
#define MCCON_SET_DTE2_BITBAND_ADDRESS 0x437000CC
#define MCCON_SET_DTE2_BITBAND (*(volatile unsigned *)0x437000CC)
#define MCCON_SET_DTE2_BIT 19
#define MCCON_SET_DISUP2_MASK 0x100000
#define MCCON_SET_DISUP2 0x100000
#define MCCON_SET_DISUP2_BITBAND_ADDRESS 0x437000D0
#define MCCON_SET_DISUP2_BITBAND (*(volatile unsigned *)0x437000D0)
#define MCCON_SET_DISUP2_BIT 20
#define MCCON_SET_INVBDC_MASK 0x20000000
#define MCCON_SET_INVBDC 0x20000000
#define MCCON_SET_INVBDC_BITBAND_ADDRESS 0x437000F4
#define MCCON_SET_INVBDC_BITBAND (*(volatile unsigned *)0x437000F4)
#define MCCON_SET_INVBDC_BIT 29
#define MCCON_SET_ACMODE_MASK 0x40000000
#define MCCON_SET_ACMODE 0x40000000
#define MCCON_SET_ACMODE_BITBAND_ADDRESS 0x437000F8
#define MCCON_SET_ACMODE_BITBAND (*(volatile unsigned *)0x437000F8)
#define MCCON_SET_ACMODE_BIT 30
#define MCCON_SET_DCMODE_MASK 0x80000000
#define MCCON_SET_DCMODE 0x80000000
#define MCCON_SET_DCMODE_BITBAND_ADDRESS 0x437000FC
#define MCCON_SET_DCMODE_BITBAND (*(volatile unsigned *)0x437000FC)
#define MCCON_SET_DCMODE_BIT 31

#define MCCON_CLR (*(volatile unsigned long *)0x400B8008)
#define MCCON_CLR_OFFSET 0x8
#define MCCON_CLR_RUN0_MASK 0x1
#define MCCON_CLR_RUN0 0x1
#define MCCON_CLR_RUN0_BITBAND_ADDRESS 0x43700100
#define MCCON_CLR_RUN0_BITBAND (*(volatile unsigned *)0x43700100)
#define MCCON_CLR_RUN0_BIT 0
#define MCCON_CLR_CENTER0_MASK 0x2
#define MCCON_CLR_CENTER0 0x2
#define MCCON_CLR_CENTER0_BITBAND_ADDRESS 0x43700104
#define MCCON_CLR_CENTER0_BITBAND (*(volatile unsigned *)0x43700104)
#define MCCON_CLR_CENTER0_BIT 1
#define MCCON_CLR_POLA0_MASK 0x4
#define MCCON_CLR_POLA0 0x4
#define MCCON_CLR_POLA0_BITBAND_ADDRESS 0x43700108
#define MCCON_CLR_POLA0_BITBAND (*(volatile unsigned *)0x43700108)
#define MCCON_CLR_POLA0_BIT 2
#define MCCON_CLR_DTE0_MASK 0x8
#define MCCON_CLR_DTE0 0x8
#define MCCON_CLR_DTE0_BITBAND_ADDRESS 0x4370010C
#define MCCON_CLR_DTE0_BITBAND (*(volatile unsigned *)0x4370010C)
#define MCCON_CLR_DTE0_BIT 3
#define MCCON_CLR_DISUP0_MASK 0x10
#define MCCON_CLR_DISUP0 0x10
#define MCCON_CLR_DISUP0_BITBAND_ADDRESS 0x43700110
#define MCCON_CLR_DISUP0_BITBAND (*(volatile unsigned *)0x43700110)
#define MCCON_CLR_DISUP0_BIT 4
#define MCCON_CLR_RUN1_MASK 0x100
#define MCCON_CLR_RUN1 0x100
#define MCCON_CLR_RUN1_BITBAND_ADDRESS 0x43700120
#define MCCON_CLR_RUN1_BITBAND (*(volatile unsigned *)0x43700120)
#define MCCON_CLR_RUN1_BIT 8
#define MCCON_CLR_CENTER1_MASK 0x200
#define MCCON_CLR_CENTER1 0x200
#define MCCON_CLR_CENTER1_BITBAND_ADDRESS 0x43700124
#define MCCON_CLR_CENTER1_BITBAND (*(volatile unsigned *)0x43700124)
#define MCCON_CLR_CENTER1_BIT 9
#define MCCON_CLR_POLA1_MASK 0x400
#define MCCON_CLR_POLA1 0x400
#define MCCON_CLR_POLA1_BITBAND_ADDRESS 0x43700128
#define MCCON_CLR_POLA1_BITBAND (*(volatile unsigned *)0x43700128)
#define MCCON_CLR_POLA1_BIT 10
#define MCCON_CLR_DTE1_MASK 0x800
#define MCCON_CLR_DTE1 0x800
#define MCCON_CLR_DTE1_BITBAND_ADDRESS 0x4370012C
#define MCCON_CLR_DTE1_BITBAND (*(volatile unsigned *)0x4370012C)
#define MCCON_CLR_DTE1_BIT 11
#define MCCON_CLR_DISUP1_MASK 0x1000
#define MCCON_CLR_DISUP1 0x1000
#define MCCON_CLR_DISUP1_BITBAND_ADDRESS 0x43700130
#define MCCON_CLR_DISUP1_BITBAND (*(volatile unsigned *)0x43700130)
#define MCCON_CLR_DISUP1_BIT 12
#define MCCON_CLR_RUN2_MASK 0x10000
#define MCCON_CLR_RUN2 0x10000
#define MCCON_CLR_RUN2_BITBAND_ADDRESS 0x43700140
#define MCCON_CLR_RUN2_BITBAND (*(volatile unsigned *)0x43700140)
#define MCCON_CLR_RUN2_BIT 16
#define MCCON_CLR_CENTER2_MASK 0x20000
#define MCCON_CLR_CENTER2 0x20000
#define MCCON_CLR_CENTER2_BITBAND_ADDRESS 0x43700144
#define MCCON_CLR_CENTER2_BITBAND (*(volatile unsigned *)0x43700144)
#define MCCON_CLR_CENTER2_BIT 17
#define MCCON_CLR_POLA2_MASK 0x40000
#define MCCON_CLR_POLA2 0x40000
#define MCCON_CLR_POLA2_BITBAND_ADDRESS 0x43700148
#define MCCON_CLR_POLA2_BITBAND (*(volatile unsigned *)0x43700148)
#define MCCON_CLR_POLA2_BIT 18
#define MCCON_CLR_DTE2_MASK 0x80000
#define MCCON_CLR_DTE2 0x80000
#define MCCON_CLR_DTE2_BITBAND_ADDRESS 0x4370014C
#define MCCON_CLR_DTE2_BITBAND (*(volatile unsigned *)0x4370014C)
#define MCCON_CLR_DTE2_BIT 19
#define MCCON_CLR_DISUP2_MASK 0x100000
#define MCCON_CLR_DISUP2 0x100000
#define MCCON_CLR_DISUP2_BITBAND_ADDRESS 0x43700150
#define MCCON_CLR_DISUP2_BITBAND (*(volatile unsigned *)0x43700150)
#define MCCON_CLR_DISUP2_BIT 20
#define MCCON_CLR_INVBDC_MASK 0x20000000
#define MCCON_CLR_INVBDC 0x20000000
#define MCCON_CLR_INVBDC_BITBAND_ADDRESS 0x43700174
#define MCCON_CLR_INVBDC_BITBAND (*(volatile unsigned *)0x43700174)
#define MCCON_CLR_INVBDC_BIT 29
#define MCCON_CLR_ACMODE_MASK 0x40000000
#define MCCON_CLR_ACMODE 0x40000000
#define MCCON_CLR_ACMODE_BITBAND_ADDRESS 0x43700178
#define MCCON_CLR_ACMODE_BITBAND (*(volatile unsigned *)0x43700178)
#define MCCON_CLR_ACMODE_BIT 30
#define MCCON_CLR_DCMODE_MASK 0x80000000
#define MCCON_CLR_DCMODE 0x80000000
#define MCCON_CLR_DCMODE_BITBAND_ADDRESS 0x4370017C
#define MCCON_CLR_DCMODE_BITBAND (*(volatile unsigned *)0x4370017C)
#define MCCON_CLR_DCMODE_BIT 31

#define MCCAPCON (*(volatile unsigned long *)0x400B800C)
#define MCCAPCON_OFFSET 0xC
#define MCCAPCON_CAP0MCI0_RE_MASK 0x1
#define MCCAPCON_CAP0MCI0_RE 0x1
#define MCCAPCON_CAP0MCI0_RE_BITBAND_ADDRESS 0x43700180
#define MCCAPCON_CAP0MCI0_RE_BITBAND (*(volatile unsigned *)0x43700180)
#define MCCAPCON_CAP0MCI0_RE_BIT 0
#define MCCAPCON_CAP0MCI0_FE_MASK 0x2
#define MCCAPCON_CAP0MCI0_FE 0x2
#define MCCAPCON_CAP0MCI0_FE_BITBAND_ADDRESS 0x43700184
#define MCCAPCON_CAP0MCI0_FE_BITBAND (*(volatile unsigned *)0x43700184)
#define MCCAPCON_CAP0MCI0_FE_BIT 1
#define MCCAPCON_CAP0MCI1_RE_MASK 0x4
#define MCCAPCON_CAP0MCI1_RE 0x4
#define MCCAPCON_CAP0MCI1_RE_BITBAND_ADDRESS 0x43700188
#define MCCAPCON_CAP0MCI1_RE_BITBAND (*(volatile unsigned *)0x43700188)
#define MCCAPCON_CAP0MCI1_RE_BIT 2
#define MCCAPCON_CAP0MCI1_FE_MASK 0x8
#define MCCAPCON_CAP0MCI1_FE 0x8
#define MCCAPCON_CAP0MCI1_FE_BITBAND_ADDRESS 0x4370018C
#define MCCAPCON_CAP0MCI1_FE_BITBAND (*(volatile unsigned *)0x4370018C)
#define MCCAPCON_CAP0MCI1_FE_BIT 3
#define MCCAPCON_CAP0MCI2_RE_MASK 0x10
#define MCCAPCON_CAP0MCI2_RE 0x10
#define MCCAPCON_CAP0MCI2_RE_BITBAND_ADDRESS 0x43700190
#define MCCAPCON_CAP0MCI2_RE_BITBAND (*(volatile unsigned *)0x43700190)
#define MCCAPCON_CAP0MCI2_RE_BIT 4
#define MCCAPCON_CAP0MCI2_FE_MASK 0x20
#define MCCAPCON_CAP0MCI2_FE 0x20
#define MCCAPCON_CAP0MCI2_FE_BITBAND_ADDRESS 0x43700194
#define MCCAPCON_CAP0MCI2_FE_BITBAND (*(volatile unsigned *)0x43700194)
#define MCCAPCON_CAP0MCI2_FE_BIT 5
#define MCCAPCON_CAP1MCI0_RE_MASK 0x40
#define MCCAPCON_CAP1MCI0_RE 0x40
#define MCCAPCON_CAP1MCI0_RE_BITBAND_ADDRESS 0x43700198
#define MCCAPCON_CAP1MCI0_RE_BITBAND (*(volatile unsigned *)0x43700198)
#define MCCAPCON_CAP1MCI0_RE_BIT 6
#define MCCAPCON_CAP1MCI0_FE_MASK 0x80
#define MCCAPCON_CAP1MCI0_FE 0x80
#define MCCAPCON_CAP1MCI0_FE_BITBAND_ADDRESS 0x4370019C
#define MCCAPCON_CAP1MCI0_FE_BITBAND (*(volatile unsigned *)0x4370019C)
#define MCCAPCON_CAP1MCI0_FE_BIT 7
#define MCCAPCON_CAP1MCI1_RE_MASK 0x100
#define MCCAPCON_CAP1MCI1_RE 0x100
#define MCCAPCON_CAP1MCI1_RE_BITBAND_ADDRESS 0x437001A0
#define MCCAPCON_CAP1MCI1_RE_BITBAND (*(volatile unsigned *)0x437001A0)
#define MCCAPCON_CAP1MCI1_RE_BIT 8
#define MCCAPCON_CAP1MCI1_FE_MASK 0x200
#define MCCAPCON_CAP1MCI1_FE 0x200
#define MCCAPCON_CAP1MCI1_FE_BITBAND_ADDRESS 0x437001A4
#define MCCAPCON_CAP1MCI1_FE_BITBAND (*(volatile unsigned *)0x437001A4)
#define MCCAPCON_CAP1MCI1_FE_BIT 9
#define MCCAPCON_CAP1MCI2_RE_MASK 0x400
#define MCCAPCON_CAP1MCI2_RE 0x400
#define MCCAPCON_CAP1MCI2_RE_BITBAND_ADDRESS 0x437001A8
#define MCCAPCON_CAP1MCI2_RE_BITBAND (*(volatile unsigned *)0x437001A8)
#define MCCAPCON_CAP1MCI2_RE_BIT 10
#define MCCAPCON_CAP1MCI2_FE_MASK 0x800
#define MCCAPCON_CAP1MCI2_FE 0x800
#define MCCAPCON_CAP1MCI2_FE_BITBAND_ADDRESS 0x437001AC
#define MCCAPCON_CAP1MCI2_FE_BITBAND (*(volatile unsigned *)0x437001AC)
#define MCCAPCON_CAP1MCI2_FE_BIT 11
#define MCCAPCON_CAP2MCI0_RE_MASK 0x1000
#define MCCAPCON_CAP2MCI0_RE 0x1000
#define MCCAPCON_CAP2MCI0_RE_BITBAND_ADDRESS 0x437001B0
#define MCCAPCON_CAP2MCI0_RE_BITBAND (*(volatile unsigned *)0x437001B0)
#define MCCAPCON_CAP2MCI0_RE_BIT 12
#define MCCAPCON_CAP2MCI0_FE_MASK 0x2000
#define MCCAPCON_CAP2MCI0_FE 0x2000
#define MCCAPCON_CAP2MCI0_FE_BITBAND_ADDRESS 0x437001B4
#define MCCAPCON_CAP2MCI0_FE_BITBAND (*(volatile unsigned *)0x437001B4)
#define MCCAPCON_CAP2MCI0_FE_BIT 13
#define MCCAPCON_CAP2MCI1_RE_MASK 0x4000
#define MCCAPCON_CAP2MCI1_RE 0x4000
#define MCCAPCON_CAP2MCI1_RE_BITBAND_ADDRESS 0x437001B8
#define MCCAPCON_CAP2MCI1_RE_BITBAND (*(volatile unsigned *)0x437001B8)
#define MCCAPCON_CAP2MCI1_RE_BIT 14
#define MCCAPCON_CAP2MCI1_FE_MASK 0x8000
#define MCCAPCON_CAP2MCI1_FE 0x8000
#define MCCAPCON_CAP2MCI1_FE_BITBAND_ADDRESS 0x437001BC
#define MCCAPCON_CAP2MCI1_FE_BITBAND (*(volatile unsigned *)0x437001BC)
#define MCCAPCON_CAP2MCI1_FE_BIT 15
#define MCCAPCON_CAP2MCI2_RE_MASK 0x10000
#define MCCAPCON_CAP2MCI2_RE 0x10000
#define MCCAPCON_CAP2MCI2_RE_BITBAND_ADDRESS 0x437001C0
#define MCCAPCON_CAP2MCI2_RE_BITBAND (*(volatile unsigned *)0x437001C0)
#define MCCAPCON_CAP2MCI2_RE_BIT 16
#define MCCAPCON_CAP2MCI2_FE_MASK 0x20000
#define MCCAPCON_CAP2MCI2_FE 0x20000
#define MCCAPCON_CAP2MCI2_FE_BITBAND_ADDRESS 0x437001C4
#define MCCAPCON_CAP2MCI2_FE_BITBAND (*(volatile unsigned *)0x437001C4)
#define MCCAPCON_CAP2MCI2_FE_BIT 17
#define MCCAPCON_RT0_MASK 0x40000
#define MCCAPCON_RT0 0x40000
#define MCCAPCON_RT0_BITBAND_ADDRESS 0x437001C8
#define MCCAPCON_RT0_BITBAND (*(volatile unsigned *)0x437001C8)
#define MCCAPCON_RT0_BIT 18
#define MCCAPCON_RT1_MASK 0x80000
#define MCCAPCON_RT1 0x80000
#define MCCAPCON_RT1_BITBAND_ADDRESS 0x437001CC
#define MCCAPCON_RT1_BITBAND (*(volatile unsigned *)0x437001CC)
#define MCCAPCON_RT1_BIT 19
#define MCCAPCON_RT2_MASK 0x100000
#define MCCAPCON_RT2 0x100000
#define MCCAPCON_RT2_BITBAND_ADDRESS 0x437001D0
#define MCCAPCON_RT2_BITBAND (*(volatile unsigned *)0x437001D0)
#define MCCAPCON_RT2_BIT 20
#define MCCAPCON_HNFCAP0_MASK 0x200000
#define MCCAPCON_HNFCAP0 0x200000
#define MCCAPCON_HNFCAP0_BITBAND_ADDRESS 0x437001D4
#define MCCAPCON_HNFCAP0_BITBAND (*(volatile unsigned *)0x437001D4)
#define MCCAPCON_HNFCAP0_BIT 21
#define MCCAPCON_HNFCAP1_MASK 0x400000
#define MCCAPCON_HNFCAP1 0x400000
#define MCCAPCON_HNFCAP1_BITBAND_ADDRESS 0x437001D8
#define MCCAPCON_HNFCAP1_BITBAND (*(volatile unsigned *)0x437001D8)
#define MCCAPCON_HNFCAP1_BIT 22
#define MCCAPCON_HNFCAP2_MASK 0x800000
#define MCCAPCON_HNFCAP2 0x800000
#define MCCAPCON_HNFCAP2_BITBAND_ADDRESS 0x437001DC
#define MCCAPCON_HNFCAP2_BITBAND (*(volatile unsigned *)0x437001DC)
#define MCCAPCON_HNFCAP2_BIT 23

#define MCCAPCON_SET (*(volatile unsigned long *)0x400B8010)
#define MCCAPCON_SET_OFFSET 0x10
#define MCCAPCON_SET_CAP0MCI0_RE_MASK 0x1
#define MCCAPCON_SET_CAP0MCI0_RE 0x1
#define MCCAPCON_SET_CAP0MCI0_RE_BITBAND_ADDRESS 0x43700200
#define MCCAPCON_SET_CAP0MCI0_RE_BITBAND (*(volatile unsigned *)0x43700200)
#define MCCAPCON_SET_CAP0MCI0_RE_BIT 0
#define MCCAPCON_SET_CAP0MCI0_FE_MASK 0x2
#define MCCAPCON_SET_CAP0MCI0_FE 0x2
#define MCCAPCON_SET_CAP0MCI0_FE_BITBAND_ADDRESS 0x43700204
#define MCCAPCON_SET_CAP0MCI0_FE_BITBAND (*(volatile unsigned *)0x43700204)
#define MCCAPCON_SET_CAP0MCI0_FE_BIT 1
#define MCCAPCON_SET_CAP0MCI1_RE_MASK 0x4
#define MCCAPCON_SET_CAP0MCI1_RE 0x4
#define MCCAPCON_SET_CAP0MCI1_RE_BITBAND_ADDRESS 0x43700208
#define MCCAPCON_SET_CAP0MCI1_RE_BITBAND (*(volatile unsigned *)0x43700208)
#define MCCAPCON_SET_CAP0MCI1_RE_BIT 2
#define MCCAPCON_SET_CAP0MCI1_FE_MASK 0x8
#define MCCAPCON_SET_CAP0MCI1_FE 0x8
#define MCCAPCON_SET_CAP0MCI1_FE_BITBAND_ADDRESS 0x4370020C
#define MCCAPCON_SET_CAP0MCI1_FE_BITBAND (*(volatile unsigned *)0x4370020C)
#define MCCAPCON_SET_CAP0MCI1_FE_BIT 3
#define MCCAPCON_SET_CAP0MCI2_RE_MASK 0x10
#define MCCAPCON_SET_CAP0MCI2_RE 0x10
#define MCCAPCON_SET_CAP0MCI2_RE_BITBAND_ADDRESS 0x43700210
#define MCCAPCON_SET_CAP0MCI2_RE_BITBAND (*(volatile unsigned *)0x43700210)
#define MCCAPCON_SET_CAP0MCI2_RE_BIT 4
#define MCCAPCON_SET_CAP0MCI2_FE_MASK 0x20
#define MCCAPCON_SET_CAP0MCI2_FE 0x20
#define MCCAPCON_SET_CAP0MCI2_FE_BITBAND_ADDRESS 0x43700214
#define MCCAPCON_SET_CAP0MCI2_FE_BITBAND (*(volatile unsigned *)0x43700214)
#define MCCAPCON_SET_CAP0MCI2_FE_BIT 5
#define MCCAPCON_SET_CAP1MCI0_RE_MASK 0x40
#define MCCAPCON_SET_CAP1MCI0_RE 0x40
#define MCCAPCON_SET_CAP1MCI0_RE_BITBAND_ADDRESS 0x43700218
#define MCCAPCON_SET_CAP1MCI0_RE_BITBAND (*(volatile unsigned *)0x43700218)
#define MCCAPCON_SET_CAP1MCI0_RE_BIT 6
#define MCCAPCON_SET_CAP1MCI0_FE_MASK 0x80
#define MCCAPCON_SET_CAP1MCI0_FE 0x80
#define MCCAPCON_SET_CAP1MCI0_FE_BITBAND_ADDRESS 0x4370021C
#define MCCAPCON_SET_CAP1MCI0_FE_BITBAND (*(volatile unsigned *)0x4370021C)
#define MCCAPCON_SET_CAP1MCI0_FE_BIT 7
#define MCCAPCON_SET_CAP1MCI1_RE_MASK 0x100
#define MCCAPCON_SET_CAP1MCI1_RE 0x100
#define MCCAPCON_SET_CAP1MCI1_RE_BITBAND_ADDRESS 0x43700220
#define MCCAPCON_SET_CAP1MCI1_RE_BITBAND (*(volatile unsigned *)0x43700220)
#define MCCAPCON_SET_CAP1MCI1_RE_BIT 8
#define MCCAPCON_SET_CAP1MCI1_FE_MASK 0x200
#define MCCAPCON_SET_CAP1MCI1_FE 0x200
#define MCCAPCON_SET_CAP1MCI1_FE_BITBAND_ADDRESS 0x43700224
#define MCCAPCON_SET_CAP1MCI1_FE_BITBAND (*(volatile unsigned *)0x43700224)
#define MCCAPCON_SET_CAP1MCI1_FE_BIT 9
#define MCCAPCON_SET_CAP1MCI2_RE_MASK 0x400
#define MCCAPCON_SET_CAP1MCI2_RE 0x400
#define MCCAPCON_SET_CAP1MCI2_RE_BITBAND_ADDRESS 0x43700228
#define MCCAPCON_SET_CAP1MCI2_RE_BITBAND (*(volatile unsigned *)0x43700228)
#define MCCAPCON_SET_CAP1MCI2_RE_BIT 10
#define MCCAPCON_SET_CAP1MCI2_FE_MASK 0x800
#define MCCAPCON_SET_CAP1MCI2_FE 0x800
#define MCCAPCON_SET_CAP1MCI2_FE_BITBAND_ADDRESS 0x4370022C
#define MCCAPCON_SET_CAP1MCI2_FE_BITBAND (*(volatile unsigned *)0x4370022C)
#define MCCAPCON_SET_CAP1MCI2_FE_BIT 11
#define MCCAPCON_SET_CAP2MCI0_RE_MASK 0x1000
#define MCCAPCON_SET_CAP2MCI0_RE 0x1000
#define MCCAPCON_SET_CAP2MCI0_RE_BITBAND_ADDRESS 0x43700230
#define MCCAPCON_SET_CAP2MCI0_RE_BITBAND (*(volatile unsigned *)0x43700230)
#define MCCAPCON_SET_CAP2MCI0_RE_BIT 12
#define MCCAPCON_SET_CAP2MCI0_FE_MASK 0x2000
#define MCCAPCON_SET_CAP2MCI0_FE 0x2000
#define MCCAPCON_SET_CAP2MCI0_FE_BITBAND_ADDRESS 0x43700234
#define MCCAPCON_SET_CAP2MCI0_FE_BITBAND (*(volatile unsigned *)0x43700234)
#define MCCAPCON_SET_CAP2MCI0_FE_BIT 13
#define MCCAPCON_SET_CAP2MCI1_RE_MASK 0x4000
#define MCCAPCON_SET_CAP2MCI1_RE 0x4000
#define MCCAPCON_SET_CAP2MCI1_RE_BITBAND_ADDRESS 0x43700238
#define MCCAPCON_SET_CAP2MCI1_RE_BITBAND (*(volatile unsigned *)0x43700238)
#define MCCAPCON_SET_CAP2MCI1_RE_BIT 14
#define MCCAPCON_SET_CAP2MCI1_FE_MASK 0x8000
#define MCCAPCON_SET_CAP2MCI1_FE 0x8000
#define MCCAPCON_SET_CAP2MCI1_FE_BITBAND_ADDRESS 0x4370023C
#define MCCAPCON_SET_CAP2MCI1_FE_BITBAND (*(volatile unsigned *)0x4370023C)
#define MCCAPCON_SET_CAP2MCI1_FE_BIT 15
#define MCCAPCON_SET_CAP2MCI2_RE_MASK 0x10000
#define MCCAPCON_SET_CAP2MCI2_RE 0x10000
#define MCCAPCON_SET_CAP2MCI2_RE_BITBAND_ADDRESS 0x43700240
#define MCCAPCON_SET_CAP2MCI2_RE_BITBAND (*(volatile unsigned *)0x43700240)
#define MCCAPCON_SET_CAP2MCI2_RE_BIT 16
#define MCCAPCON_SET_CAP2MCI2_FE_MASK 0x20000
#define MCCAPCON_SET_CAP2MCI2_FE 0x20000
#define MCCAPCON_SET_CAP2MCI2_FE_BITBAND_ADDRESS 0x43700244
#define MCCAPCON_SET_CAP2MCI2_FE_BITBAND (*(volatile unsigned *)0x43700244)
#define MCCAPCON_SET_CAP2MCI2_FE_BIT 17
#define MCCAPCON_SET_RT0_MASK 0x40000
#define MCCAPCON_SET_RT0 0x40000
#define MCCAPCON_SET_RT0_BITBAND_ADDRESS 0x43700248
#define MCCAPCON_SET_RT0_BITBAND (*(volatile unsigned *)0x43700248)
#define MCCAPCON_SET_RT0_BIT 18
#define MCCAPCON_SET_RT1_MASK 0x80000
#define MCCAPCON_SET_RT1 0x80000
#define MCCAPCON_SET_RT1_BITBAND_ADDRESS 0x4370024C
#define MCCAPCON_SET_RT1_BITBAND (*(volatile unsigned *)0x4370024C)
#define MCCAPCON_SET_RT1_BIT 19
#define MCCAPCON_SET_RT2_MASK 0x100000
#define MCCAPCON_SET_RT2 0x100000
#define MCCAPCON_SET_RT2_BITBAND_ADDRESS 0x43700250
#define MCCAPCON_SET_RT2_BITBAND (*(volatile unsigned *)0x43700250)
#define MCCAPCON_SET_RT2_BIT 20
#define MCCAPCON_SET_HNFCAP0_MASK 0x200000
#define MCCAPCON_SET_HNFCAP0 0x200000
#define MCCAPCON_SET_HNFCAP0_BITBAND_ADDRESS 0x43700254
#define MCCAPCON_SET_HNFCAP0_BITBAND (*(volatile unsigned *)0x43700254)
#define MCCAPCON_SET_HNFCAP0_BIT 21
#define MCCAPCON_SET_HNFCAP1_MASK 0x400000
#define MCCAPCON_SET_HNFCAP1 0x400000
#define MCCAPCON_SET_HNFCAP1_BITBAND_ADDRESS 0x43700258
#define MCCAPCON_SET_HNFCAP1_BITBAND (*(volatile unsigned *)0x43700258)
#define MCCAPCON_SET_HNFCAP1_BIT 22
#define MCCAPCON_SET_HNFCAP2_MASK 0x800000
#define MCCAPCON_SET_HNFCAP2 0x800000
#define MCCAPCON_SET_HNFCAP2_BITBAND_ADDRESS 0x4370025C
#define MCCAPCON_SET_HNFCAP2_BITBAND (*(volatile unsigned *)0x4370025C)
#define MCCAPCON_SET_HNFCAP2_BIT 23

#define MCCAPCON_CLR (*(volatile unsigned long *)0x400B8014)
#define MCCAPCON_CLR_OFFSET 0x14
#define MCCAPCON_CLR_CAP0MCI0_RE_MASK 0x1
#define MCCAPCON_CLR_CAP0MCI0_RE 0x1
#define MCCAPCON_CLR_CAP0MCI0_RE_BITBAND_ADDRESS 0x43700280
#define MCCAPCON_CLR_CAP0MCI0_RE_BITBAND (*(volatile unsigned *)0x43700280)
#define MCCAPCON_CLR_CAP0MCI0_RE_BIT 0
#define MCCAPCON_CLR_CAP0MCI0_FE_MASK 0x2
#define MCCAPCON_CLR_CAP0MCI0_FE 0x2
#define MCCAPCON_CLR_CAP0MCI0_FE_BITBAND_ADDRESS 0x43700284
#define MCCAPCON_CLR_CAP0MCI0_FE_BITBAND (*(volatile unsigned *)0x43700284)
#define MCCAPCON_CLR_CAP0MCI0_FE_BIT 1
#define MCCAPCON_CLR_CAP0MCI1_RE_MASK 0x4
#define MCCAPCON_CLR_CAP0MCI1_RE 0x4
#define MCCAPCON_CLR_CAP0MCI1_RE_BITBAND_ADDRESS 0x43700288
#define MCCAPCON_CLR_CAP0MCI1_RE_BITBAND (*(volatile unsigned *)0x43700288)
#define MCCAPCON_CLR_CAP0MCI1_RE_BIT 2
#define MCCAPCON_CLR_CAP0MCI1_FE_MASK 0x8
#define MCCAPCON_CLR_CAP0MCI1_FE 0x8
#define MCCAPCON_CLR_CAP0MCI1_FE_BITBAND_ADDRESS 0x4370028C
#define MCCAPCON_CLR_CAP0MCI1_FE_BITBAND (*(volatile unsigned *)0x4370028C)
#define MCCAPCON_CLR_CAP0MCI1_FE_BIT 3
#define MCCAPCON_CLR_CAP0MCI2_RE_MASK 0x10
#define MCCAPCON_CLR_CAP0MCI2_RE 0x10
#define MCCAPCON_CLR_CAP0MCI2_RE_BITBAND_ADDRESS 0x43700290
#define MCCAPCON_CLR_CAP0MCI2_RE_BITBAND (*(volatile unsigned *)0x43700290)
#define MCCAPCON_CLR_CAP0MCI2_RE_BIT 4
#define MCCAPCON_CLR_CAP0MCI2_FE_MASK 0x20
#define MCCAPCON_CLR_CAP0MCI2_FE 0x20
#define MCCAPCON_CLR_CAP0MCI2_FE_BITBAND_ADDRESS 0x43700294
#define MCCAPCON_CLR_CAP0MCI2_FE_BITBAND (*(volatile unsigned *)0x43700294)
#define MCCAPCON_CLR_CAP0MCI2_FE_BIT 5
#define MCCAPCON_CLR_CAP1MCI0_RE_MASK 0x40
#define MCCAPCON_CLR_CAP1MCI0_RE 0x40
#define MCCAPCON_CLR_CAP1MCI0_RE_BITBAND_ADDRESS 0x43700298
#define MCCAPCON_CLR_CAP1MCI0_RE_BITBAND (*(volatile unsigned *)0x43700298)
#define MCCAPCON_CLR_CAP1MCI0_RE_BIT 6
#define MCCAPCON_CLR_CAP1MCI0_FE_MASK 0x80
#define MCCAPCON_CLR_CAP1MCI0_FE 0x80
#define MCCAPCON_CLR_CAP1MCI0_FE_BITBAND_ADDRESS 0x4370029C
#define MCCAPCON_CLR_CAP1MCI0_FE_BITBAND (*(volatile unsigned *)0x4370029C)
#define MCCAPCON_CLR_CAP1MCI0_FE_BIT 7
#define MCCAPCON_CLR_CAP1MCI1_RE_MASK 0x100
#define MCCAPCON_CLR_CAP1MCI1_RE 0x100
#define MCCAPCON_CLR_CAP1MCI1_RE_BITBAND_ADDRESS 0x437002A0
#define MCCAPCON_CLR_CAP1MCI1_RE_BITBAND (*(volatile unsigned *)0x437002A0)
#define MCCAPCON_CLR_CAP1MCI1_RE_BIT 8
#define MCCAPCON_CLR_CAP1MCI1_FE_MASK 0x200
#define MCCAPCON_CLR_CAP1MCI1_FE 0x200
#define MCCAPCON_CLR_CAP1MCI1_FE_BITBAND_ADDRESS 0x437002A4
#define MCCAPCON_CLR_CAP1MCI1_FE_BITBAND (*(volatile unsigned *)0x437002A4)
#define MCCAPCON_CLR_CAP1MCI1_FE_BIT 9
#define MCCAPCON_CLR_CAP1MCI2_RE_MASK 0x400
#define MCCAPCON_CLR_CAP1MCI2_RE 0x400
#define MCCAPCON_CLR_CAP1MCI2_RE_BITBAND_ADDRESS 0x437002A8
#define MCCAPCON_CLR_CAP1MCI2_RE_BITBAND (*(volatile unsigned *)0x437002A8)
#define MCCAPCON_CLR_CAP1MCI2_RE_BIT 10
#define MCCAPCON_CLR_CAP1MCI2_FE_MASK 0x800
#define MCCAPCON_CLR_CAP1MCI2_FE 0x800
#define MCCAPCON_CLR_CAP1MCI2_FE_BITBAND_ADDRESS 0x437002AC
#define MCCAPCON_CLR_CAP1MCI2_FE_BITBAND (*(volatile unsigned *)0x437002AC)
#define MCCAPCON_CLR_CAP1MCI2_FE_BIT 11
#define MCCAPCON_CLR_CAP2MCI0_RE_MASK 0x1000
#define MCCAPCON_CLR_CAP2MCI0_RE 0x1000
#define MCCAPCON_CLR_CAP2MCI0_RE_BITBAND_ADDRESS 0x437002B0
#define MCCAPCON_CLR_CAP2MCI0_RE_BITBAND (*(volatile unsigned *)0x437002B0)
#define MCCAPCON_CLR_CAP2MCI0_RE_BIT 12
#define MCCAPCON_CLR_CAP2MCI0_FE_MASK 0x2000
#define MCCAPCON_CLR_CAP2MCI0_FE 0x2000
#define MCCAPCON_CLR_CAP2MCI0_FE_BITBAND_ADDRESS 0x437002B4
#define MCCAPCON_CLR_CAP2MCI0_FE_BITBAND (*(volatile unsigned *)0x437002B4)
#define MCCAPCON_CLR_CAP2MCI0_FE_BIT 13
#define MCCAPCON_CLR_CAP2MCI1_RE_MASK 0x4000
#define MCCAPCON_CLR_CAP2MCI1_RE 0x4000
#define MCCAPCON_CLR_CAP2MCI1_RE_BITBAND_ADDRESS 0x437002B8
#define MCCAPCON_CLR_CAP2MCI1_RE_BITBAND (*(volatile unsigned *)0x437002B8)
#define MCCAPCON_CLR_CAP2MCI1_RE_BIT 14
#define MCCAPCON_CLR_CAP2MCI1_FE_MASK 0x8000
#define MCCAPCON_CLR_CAP2MCI1_FE 0x8000
#define MCCAPCON_CLR_CAP2MCI1_FE_BITBAND_ADDRESS 0x437002BC
#define MCCAPCON_CLR_CAP2MCI1_FE_BITBAND (*(volatile unsigned *)0x437002BC)
#define MCCAPCON_CLR_CAP2MCI1_FE_BIT 15
#define MCCAPCON_CLR_CAP2MCI2_RE_MASK 0x10000
#define MCCAPCON_CLR_CAP2MCI2_RE 0x10000
#define MCCAPCON_CLR_CAP2MCI2_RE_BITBAND_ADDRESS 0x437002C0
#define MCCAPCON_CLR_CAP2MCI2_RE_BITBAND (*(volatile unsigned *)0x437002C0)
#define MCCAPCON_CLR_CAP2MCI2_RE_BIT 16
#define MCCAPCON_CLR_CAP2MCI2_FE_MASK 0x20000
#define MCCAPCON_CLR_CAP2MCI2_FE 0x20000
#define MCCAPCON_CLR_CAP2MCI2_FE_BITBAND_ADDRESS 0x437002C4
#define MCCAPCON_CLR_CAP2MCI2_FE_BITBAND (*(volatile unsigned *)0x437002C4)
#define MCCAPCON_CLR_CAP2MCI2_FE_BIT 17
#define MCCAPCON_CLR_RT0_MASK 0x40000
#define MCCAPCON_CLR_RT0 0x40000
#define MCCAPCON_CLR_RT0_BITBAND_ADDRESS 0x437002C8
#define MCCAPCON_CLR_RT0_BITBAND (*(volatile unsigned *)0x437002C8)
#define MCCAPCON_CLR_RT0_BIT 18
#define MCCAPCON_CLR_RT1_MASK 0x80000
#define MCCAPCON_CLR_RT1 0x80000
#define MCCAPCON_CLR_RT1_BITBAND_ADDRESS 0x437002CC
#define MCCAPCON_CLR_RT1_BITBAND (*(volatile unsigned *)0x437002CC)
#define MCCAPCON_CLR_RT1_BIT 19
#define MCCAPCON_CLR_RT2_MASK 0x100000
#define MCCAPCON_CLR_RT2 0x100000
#define MCCAPCON_CLR_RT2_BITBAND_ADDRESS 0x437002D0
#define MCCAPCON_CLR_RT2_BITBAND (*(volatile unsigned *)0x437002D0)
#define MCCAPCON_CLR_RT2_BIT 20
#define MCCAPCON_CLR_HNFCAP0_MASK 0x200000
#define MCCAPCON_CLR_HNFCAP0 0x200000
#define MCCAPCON_CLR_HNFCAP0_BITBAND_ADDRESS 0x437002D4
#define MCCAPCON_CLR_HNFCAP0_BITBAND (*(volatile unsigned *)0x437002D4)
#define MCCAPCON_CLR_HNFCAP0_BIT 21
#define MCCAPCON_CLR_HNFCAP1_MASK 0x400000
#define MCCAPCON_CLR_HNFCAP1 0x400000
#define MCCAPCON_CLR_HNFCAP1_BITBAND_ADDRESS 0x437002D8
#define MCCAPCON_CLR_HNFCAP1_BITBAND (*(volatile unsigned *)0x437002D8)
#define MCCAPCON_CLR_HNFCAP1_BIT 22
#define MCCAPCON_CLR_HNFCAP2_MASK 0x800000
#define MCCAPCON_CLR_HNFCAP2 0x800000
#define MCCAPCON_CLR_HNFCAP2_BITBAND_ADDRESS 0x437002DC
#define MCCAPCON_CLR_HNFCAP2_BITBAND (*(volatile unsigned *)0x437002DC)
#define MCCAPCON_CLR_HNFCAP2_BIT 23

#define MCTC0 (*(volatile unsigned long *)0x400B8018)
#define MCTC0_OFFSET 0x18

#define MCTC1 (*(volatile unsigned long *)0x400B801C)
#define MCTC1_OFFSET 0x1C

#define MCTC2 (*(volatile unsigned long *)0x400B8020)
#define MCTC2_OFFSET 0x20

#define MCLIM0 (*(volatile unsigned long *)0x400B8024)
#define MCLIM0_OFFSET 0x24

#define MCLIM1 (*(volatile unsigned long *)0x400B8028)
#define MCLIM1_OFFSET 0x28

#define MCLIM2 (*(volatile unsigned long *)0x400B802C)
#define MCLIM2_OFFSET 0x2C

#define MCMAT0 (*(volatile unsigned long *)0x400B8030)
#define MCMAT0_OFFSET 0x30

#define MCMAT1 (*(volatile unsigned long *)0x400B8034)
#define MCMAT1_OFFSET 0x34

#define MCMAT2 (*(volatile unsigned long *)0x400B8038)
#define MCMAT2_OFFSET 0x38

#define MCDT (*(volatile unsigned long *)0x400B803C)
#define MCDT_OFFSET 0x3C
#define MCDT_DT0_MASK 0x3FF
#define MCDT_DT0_BIT 0
#define MCDT_DT1_MASK 0xFFC00
#define MCDT_DT1_BIT 10
#define MCDT_DT2_MASK 0x3FF00000
#define MCDT_DT2_BIT 20

#define MCCP (*(volatile unsigned long *)0x400B8040)
#define MCCP_OFFSET 0x40
#define MCCP_CCPA0_MASK 0x1
#define MCCP_CCPA0 0x1
#define MCCP_CCPA0_BITBAND_ADDRESS 0x43700800
#define MCCP_CCPA0_BITBAND (*(volatile unsigned *)0x43700800)
#define MCCP_CCPA0_BIT 0
#define MCCP_CCPB0_MASK 0x2
#define MCCP_CCPB0 0x2
#define MCCP_CCPB0_BITBAND_ADDRESS 0x43700804
#define MCCP_CCPB0_BITBAND (*(volatile unsigned *)0x43700804)
#define MCCP_CCPB0_BIT 1
#define MCCP_CCPA1_MASK 0x4
#define MCCP_CCPA1 0x4
#define MCCP_CCPA1_BITBAND_ADDRESS 0x43700808
#define MCCP_CCPA1_BITBAND (*(volatile unsigned *)0x43700808)
#define MCCP_CCPA1_BIT 2
#define MCCP_CCPB1_MASK 0x8
#define MCCP_CCPB1 0x8
#define MCCP_CCPB1_BITBAND_ADDRESS 0x4370080C
#define MCCP_CCPB1_BITBAND (*(volatile unsigned *)0x4370080C)
#define MCCP_CCPB1_BIT 3
#define MCCP_CCPA2_MASK 0x10
#define MCCP_CCPA2 0x10
#define MCCP_CCPA2_BITBAND_ADDRESS 0x43700810
#define MCCP_CCPA2_BITBAND (*(volatile unsigned *)0x43700810)
#define MCCP_CCPA2_BIT 4
#define MCCP_CCPB2_MASK 0x20
#define MCCP_CCPB2 0x20
#define MCCP_CCPB2_BITBAND_ADDRESS 0x43700814
#define MCCP_CCPB2_BITBAND (*(volatile unsigned *)0x43700814)
#define MCCP_CCPB2_BIT 5

#define MCCAP0 (*(volatile unsigned long *)0x400B8044)
#define MCCAP0_OFFSET 0x44

#define MCCAP1 (*(volatile unsigned long *)0x400B8048)
#define MCCAP1_OFFSET 0x48

#define MCCAP2 (*(volatile unsigned long *)0x400B804C)
#define MCCAP2_OFFSET 0x4C

#define MCINTEN (*(volatile unsigned long *)0x400B8050)
#define MCINTEN_OFFSET 0x50
#define MCINTEN_ILIM0_MASK 0x1
#define MCINTEN_ILIM0 0x1
#define MCINTEN_ILIM0_BITBAND_ADDRESS 0x43700A00
#define MCINTEN_ILIM0_BITBAND (*(volatile unsigned *)0x43700A00)
#define MCINTEN_ILIM0_BIT 0
#define MCINTEN_IMAT0_MASK 0x2
#define MCINTEN_IMAT0 0x2
#define MCINTEN_IMAT0_BITBAND_ADDRESS 0x43700A04
#define MCINTEN_IMAT0_BITBAND (*(volatile unsigned *)0x43700A04)
#define MCINTEN_IMAT0_BIT 1
#define MCINTEN_ICAP0_MASK 0x4
#define MCINTEN_ICAP0 0x4
#define MCINTEN_ICAP0_BITBAND_ADDRESS 0x43700A08
#define MCINTEN_ICAP0_BITBAND (*(volatile unsigned *)0x43700A08)
#define MCINTEN_ICAP0_BIT 2
#define MCINTEN_ILIM1_MASK 0x10
#define MCINTEN_ILIM1 0x10
#define MCINTEN_ILIM1_BITBAND_ADDRESS 0x43700A10
#define MCINTEN_ILIM1_BITBAND (*(volatile unsigned *)0x43700A10)
#define MCINTEN_ILIM1_BIT 4
#define MCINTEN_IMAT1_MASK 0x20
#define MCINTEN_IMAT1 0x20
#define MCINTEN_IMAT1_BITBAND_ADDRESS 0x43700A14
#define MCINTEN_IMAT1_BITBAND (*(volatile unsigned *)0x43700A14)
#define MCINTEN_IMAT1_BIT 5
#define MCINTEN_ICAP1_MASK 0x40
#define MCINTEN_ICAP1 0x40
#define MCINTEN_ICAP1_BITBAND_ADDRESS 0x43700A18
#define MCINTEN_ICAP1_BITBAND (*(volatile unsigned *)0x43700A18)
#define MCINTEN_ICAP1_BIT 6
#define MCINTEN_ILIM2_MASK 0x100
#define MCINTEN_ILIM2 0x100
#define MCINTEN_ILIM2_BITBAND_ADDRESS 0x43700A20
#define MCINTEN_ILIM2_BITBAND (*(volatile unsigned *)0x43700A20)
#define MCINTEN_ILIM2_BIT 8
#define MCINTEN_IMAT2_MASK 0x200
#define MCINTEN_IMAT2 0x200
#define MCINTEN_IMAT2_BITBAND_ADDRESS 0x43700A24
#define MCINTEN_IMAT2_BITBAND (*(volatile unsigned *)0x43700A24)
#define MCINTEN_IMAT2_BIT 9
#define MCINTEN_ICAP2_MASK 0x400
#define MCINTEN_ICAP2 0x400
#define MCINTEN_ICAP2_BITBAND_ADDRESS 0x43700A28
#define MCINTEN_ICAP2_BITBAND (*(volatile unsigned *)0x43700A28)
#define MCINTEN_ICAP2_BIT 10
#define MCINTEN_ABORT_MASK 0x8000
#define MCINTEN_ABORT 0x8000
#define MCINTEN_ABORT_BITBAND_ADDRESS 0x43700A3C
#define MCINTEN_ABORT_BITBAND (*(volatile unsigned *)0x43700A3C)
#define MCINTEN_ABORT_BIT 15

#define MCINTEN_SET (*(volatile unsigned long *)0x400B8054)
#define MCINTEN_SET_OFFSET 0x54
#define MCINTEN_SET_ILIM0_MASK 0x1
#define MCINTEN_SET_ILIM0 0x1
#define MCINTEN_SET_ILIM0_BITBAND_ADDRESS 0x43700A80
#define MCINTEN_SET_ILIM0_BITBAND (*(volatile unsigned *)0x43700A80)
#define MCINTEN_SET_ILIM0_BIT 0
#define MCINTEN_SET_IMAT0_MASK 0x2
#define MCINTEN_SET_IMAT0 0x2
#define MCINTEN_SET_IMAT0_BITBAND_ADDRESS 0x43700A84
#define MCINTEN_SET_IMAT0_BITBAND (*(volatile unsigned *)0x43700A84)
#define MCINTEN_SET_IMAT0_BIT 1
#define MCINTEN_SET_ICAP0_MASK 0x4
#define MCINTEN_SET_ICAP0 0x4
#define MCINTEN_SET_ICAP0_BITBAND_ADDRESS 0x43700A88
#define MCINTEN_SET_ICAP0_BITBAND (*(volatile unsigned *)0x43700A88)
#define MCINTEN_SET_ICAP0_BIT 2
#define MCINTEN_SET_ILIM1_MASK 0x10
#define MCINTEN_SET_ILIM1 0x10
#define MCINTEN_SET_ILIM1_BITBAND_ADDRESS 0x43700A90
#define MCINTEN_SET_ILIM1_BITBAND (*(volatile unsigned *)0x43700A90)
#define MCINTEN_SET_ILIM1_BIT 4
#define MCINTEN_SET_IMAT1_MASK 0x20
#define MCINTEN_SET_IMAT1 0x20
#define MCINTEN_SET_IMAT1_BITBAND_ADDRESS 0x43700A94
#define MCINTEN_SET_IMAT1_BITBAND (*(volatile unsigned *)0x43700A94)
#define MCINTEN_SET_IMAT1_BIT 5
#define MCINTEN_SET_ICAP1_MASK 0x40
#define MCINTEN_SET_ICAP1 0x40
#define MCINTEN_SET_ICAP1_BITBAND_ADDRESS 0x43700A98
#define MCINTEN_SET_ICAP1_BITBAND (*(volatile unsigned *)0x43700A98)
#define MCINTEN_SET_ICAP1_BIT 6
#define MCINTEN_SET_ILIM2_MASK 0x100
#define MCINTEN_SET_ILIM2 0x100
#define MCINTEN_SET_ILIM2_BITBAND_ADDRESS 0x43700AA0
#define MCINTEN_SET_ILIM2_BITBAND (*(volatile unsigned *)0x43700AA0)
#define MCINTEN_SET_ILIM2_BIT 8
#define MCINTEN_SET_IMAT2_MASK 0x200
#define MCINTEN_SET_IMAT2 0x200
#define MCINTEN_SET_IMAT2_BITBAND_ADDRESS 0x43700AA4
#define MCINTEN_SET_IMAT2_BITBAND (*(volatile unsigned *)0x43700AA4)
#define MCINTEN_SET_IMAT2_BIT 9
#define MCINTEN_SET_ICAP2_MASK 0x400
#define MCINTEN_SET_ICAP2 0x400
#define MCINTEN_SET_ICAP2_BITBAND_ADDRESS 0x43700AA8
#define MCINTEN_SET_ICAP2_BITBAND (*(volatile unsigned *)0x43700AA8)
#define MCINTEN_SET_ICAP2_BIT 10
#define MCINTEN_SET_ABORT_MASK 0x8000
#define MCINTEN_SET_ABORT 0x8000
#define MCINTEN_SET_ABORT_BITBAND_ADDRESS 0x43700ABC
#define MCINTEN_SET_ABORT_BITBAND (*(volatile unsigned *)0x43700ABC)
#define MCINTEN_SET_ABORT_BIT 15

#define MCINTEN_CLR (*(volatile unsigned long *)0x400B8058)
#define MCINTEN_CLR_OFFSET 0x58
#define MCINTEN_CLR_ILIM0_MASK 0x1
#define MCINTEN_CLR_ILIM0 0x1
#define MCINTEN_CLR_ILIM0_BITBAND_ADDRESS 0x43700B00
#define MCINTEN_CLR_ILIM0_BITBAND (*(volatile unsigned *)0x43700B00)
#define MCINTEN_CLR_ILIM0_BIT 0
#define MCINTEN_CLR_IMAT0_MASK 0x2
#define MCINTEN_CLR_IMAT0 0x2
#define MCINTEN_CLR_IMAT0_BITBAND_ADDRESS 0x43700B04
#define MCINTEN_CLR_IMAT0_BITBAND (*(volatile unsigned *)0x43700B04)
#define MCINTEN_CLR_IMAT0_BIT 1
#define MCINTEN_CLR_ICAP0_MASK 0x4
#define MCINTEN_CLR_ICAP0 0x4
#define MCINTEN_CLR_ICAP0_BITBAND_ADDRESS 0x43700B08
#define MCINTEN_CLR_ICAP0_BITBAND (*(volatile unsigned *)0x43700B08)
#define MCINTEN_CLR_ICAP0_BIT 2
#define MCINTEN_CLR_ILIM1_MASK 0x10
#define MCINTEN_CLR_ILIM1 0x10
#define MCINTEN_CLR_ILIM1_BITBAND_ADDRESS 0x43700B10
#define MCINTEN_CLR_ILIM1_BITBAND (*(volatile unsigned *)0x43700B10)
#define MCINTEN_CLR_ILIM1_BIT 4
#define MCINTEN_CLR_IMAT1_MASK 0x20
#define MCINTEN_CLR_IMAT1 0x20
#define MCINTEN_CLR_IMAT1_BITBAND_ADDRESS 0x43700B14
#define MCINTEN_CLR_IMAT1_BITBAND (*(volatile unsigned *)0x43700B14)
#define MCINTEN_CLR_IMAT1_BIT 5
#define MCINTEN_CLR_ICAP1_MASK 0x40
#define MCINTEN_CLR_ICAP1 0x40
#define MCINTEN_CLR_ICAP1_BITBAND_ADDRESS 0x43700B18
#define MCINTEN_CLR_ICAP1_BITBAND (*(volatile unsigned *)0x43700B18)
#define MCINTEN_CLR_ICAP1_BIT 6
#define MCINTEN_CLR_ILIM2_MASK 0x100
#define MCINTEN_CLR_ILIM2 0x100
#define MCINTEN_CLR_ILIM2_BITBAND_ADDRESS 0x43700B20
#define MCINTEN_CLR_ILIM2_BITBAND (*(volatile unsigned *)0x43700B20)
#define MCINTEN_CLR_ILIM2_BIT 8
#define MCINTEN_CLR_IMAT2_MASK 0x200
#define MCINTEN_CLR_IMAT2 0x200
#define MCINTEN_CLR_IMAT2_BITBAND_ADDRESS 0x43700B24
#define MCINTEN_CLR_IMAT2_BITBAND (*(volatile unsigned *)0x43700B24)
#define MCINTEN_CLR_IMAT2_BIT 9
#define MCINTEN_CLR_ICAP2_MASK 0x400
#define MCINTEN_CLR_ICAP2 0x400
#define MCINTEN_CLR_ICAP2_BITBAND_ADDRESS 0x43700B28
#define MCINTEN_CLR_ICAP2_BITBAND (*(volatile unsigned *)0x43700B28)
#define MCINTEN_CLR_ICAP2_BIT 10
#define MCINTEN_CLR_ABORT_MASK 0x8000
#define MCINTEN_CLR_ABORT 0x8000
#define MCINTEN_CLR_ABORT_BITBAND_ADDRESS 0x43700B3C
#define MCINTEN_CLR_ABORT_BITBAND (*(volatile unsigned *)0x43700B3C)
#define MCINTEN_CLR_ABORT_BIT 15

#define MCCNTCON (*(volatile unsigned long *)0x400B805C)
#define MCCNTCON_OFFSET 0x5C
#define MCCNTCON_TC0MCI0_RE_MASK 0x1
#define MCCNTCON_TC0MCI0_RE 0x1
#define MCCNTCON_TC0MCI0_RE_BITBAND_ADDRESS 0x43700B80
#define MCCNTCON_TC0MCI0_RE_BITBAND (*(volatile unsigned *)0x43700B80)
#define MCCNTCON_TC0MCI0_RE_BIT 0
#define MCCNTCON_TC0MCI0_FE_MASK 0x2
#define MCCNTCON_TC0MCI0_FE 0x2
#define MCCNTCON_TC0MCI0_FE_BITBAND_ADDRESS 0x43700B84
#define MCCNTCON_TC0MCI0_FE_BITBAND (*(volatile unsigned *)0x43700B84)
#define MCCNTCON_TC0MCI0_FE_BIT 1
#define MCCNTCON_TC0MCI1_RE_MASK 0x4
#define MCCNTCON_TC0MCI1_RE 0x4
#define MCCNTCON_TC0MCI1_RE_BITBAND_ADDRESS 0x43700B88
#define MCCNTCON_TC0MCI1_RE_BITBAND (*(volatile unsigned *)0x43700B88)
#define MCCNTCON_TC0MCI1_RE_BIT 2
#define MCCNTCON_TC0MCI1_FE_MASK 0x8
#define MCCNTCON_TC0MCI1_FE 0x8
#define MCCNTCON_TC0MCI1_FE_BITBAND_ADDRESS 0x43700B8C
#define MCCNTCON_TC0MCI1_FE_BITBAND (*(volatile unsigned *)0x43700B8C)
#define MCCNTCON_TC0MCI1_FE_BIT 3
#define MCCNTCON_TC0MCI2_RE_MASK 0x10
#define MCCNTCON_TC0MCI2_RE 0x10
#define MCCNTCON_TC0MCI2_RE_BITBAND_ADDRESS 0x43700B90
#define MCCNTCON_TC0MCI2_RE_BITBAND (*(volatile unsigned *)0x43700B90)
#define MCCNTCON_TC0MCI2_RE_BIT 4
#define MCCNTCON_TC0MCI2_FE_MASK 0x20
#define MCCNTCON_TC0MCI2_FE 0x20
#define MCCNTCON_TC0MCI2_FE_BITBAND_ADDRESS 0x43700B94
#define MCCNTCON_TC0MCI2_FE_BITBAND (*(volatile unsigned *)0x43700B94)
#define MCCNTCON_TC0MCI2_FE_BIT 5
#define MCCNTCON_TC1MCI0_RE_MASK 0x40
#define MCCNTCON_TC1MCI0_RE 0x40
#define MCCNTCON_TC1MCI0_RE_BITBAND_ADDRESS 0x43700B98
#define MCCNTCON_TC1MCI0_RE_BITBAND (*(volatile unsigned *)0x43700B98)
#define MCCNTCON_TC1MCI0_RE_BIT 6
#define MCCNTCON_TC1MCI0_FE_MASK 0x80
#define MCCNTCON_TC1MCI0_FE 0x80
#define MCCNTCON_TC1MCI0_FE_BITBAND_ADDRESS 0x43700B9C
#define MCCNTCON_TC1MCI0_FE_BITBAND (*(volatile unsigned *)0x43700B9C)
#define MCCNTCON_TC1MCI0_FE_BIT 7
#define MCCNTCON_TC1MCI1_RE_MASK 0x100
#define MCCNTCON_TC1MCI1_RE 0x100
#define MCCNTCON_TC1MCI1_RE_BITBAND_ADDRESS 0x43700BA0
#define MCCNTCON_TC1MCI1_RE_BITBAND (*(volatile unsigned *)0x43700BA0)
#define MCCNTCON_TC1MCI1_RE_BIT 8
#define MCCNTCON_TC1MCI1_FE_MASK 0x200
#define MCCNTCON_TC1MCI1_FE 0x200
#define MCCNTCON_TC1MCI1_FE_BITBAND_ADDRESS 0x43700BA4
#define MCCNTCON_TC1MCI1_FE_BITBAND (*(volatile unsigned *)0x43700BA4)
#define MCCNTCON_TC1MCI1_FE_BIT 9
#define MCCNTCON_TC1MCI2_RE_MASK 0x400
#define MCCNTCON_TC1MCI2_RE 0x400
#define MCCNTCON_TC1MCI2_RE_BITBAND_ADDRESS 0x43700BA8
#define MCCNTCON_TC1MCI2_RE_BITBAND (*(volatile unsigned *)0x43700BA8)
#define MCCNTCON_TC1MCI2_RE_BIT 10
#define MCCNTCON_TC1MCI2_FE_MASK 0x800
#define MCCNTCON_TC1MCI2_FE 0x800
#define MCCNTCON_TC1MCI2_FE_BITBAND_ADDRESS 0x43700BAC
#define MCCNTCON_TC1MCI2_FE_BITBAND (*(volatile unsigned *)0x43700BAC)
#define MCCNTCON_TC1MCI2_FE_BIT 11
#define MCCNTCON_TC2MCI0_RE_MASK 0x1000
#define MCCNTCON_TC2MCI0_RE 0x1000
#define MCCNTCON_TC2MCI0_RE_BITBAND_ADDRESS 0x43700BB0
#define MCCNTCON_TC2MCI0_RE_BITBAND (*(volatile unsigned *)0x43700BB0)
#define MCCNTCON_TC2MCI0_RE_BIT 12
#define MCCNTCON_TC2MCI0_FE_MASK 0x2000
#define MCCNTCON_TC2MCI0_FE 0x2000
#define MCCNTCON_TC2MCI0_FE_BITBAND_ADDRESS 0x43700BB4
#define MCCNTCON_TC2MCI0_FE_BITBAND (*(volatile unsigned *)0x43700BB4)
#define MCCNTCON_TC2MCI0_FE_BIT 13
#define MCCNTCON_TC2MCI1_RE_MASK 0x4000
#define MCCNTCON_TC2MCI1_RE 0x4000
#define MCCNTCON_TC2MCI1_RE_BITBAND_ADDRESS 0x43700BB8
#define MCCNTCON_TC2MCI1_RE_BITBAND (*(volatile unsigned *)0x43700BB8)
#define MCCNTCON_TC2MCI1_RE_BIT 14
#define MCCNTCON_TC2MCI1_FE_MASK 0x8000
#define MCCNTCON_TC2MCI1_FE 0x8000
#define MCCNTCON_TC2MCI1_FE_BITBAND_ADDRESS 0x43700BBC
#define MCCNTCON_TC2MCI1_FE_BITBAND (*(volatile unsigned *)0x43700BBC)
#define MCCNTCON_TC2MCI1_FE_BIT 15
#define MCCNTCON_TC2MCI2_RE_MASK 0x10000
#define MCCNTCON_TC2MCI2_RE 0x10000
#define MCCNTCON_TC2MCI2_RE_BITBAND_ADDRESS 0x43700BC0
#define MCCNTCON_TC2MCI2_RE_BITBAND (*(volatile unsigned *)0x43700BC0)
#define MCCNTCON_TC2MCI2_RE_BIT 16
#define MCCNTCON_TC2MCI2_FE_MASK 0x20000
#define MCCNTCON_TC2MCI2_FE 0x20000
#define MCCNTCON_TC2MCI2_FE_BITBAND_ADDRESS 0x43700BC4
#define MCCNTCON_TC2MCI2_FE_BITBAND (*(volatile unsigned *)0x43700BC4)
#define MCCNTCON_TC2MCI2_FE_BIT 17
#define MCCNTCON_CNTR0_MASK 0x20000000
#define MCCNTCON_CNTR0 0x20000000
#define MCCNTCON_CNTR0_BITBAND_ADDRESS 0x43700BF4
#define MCCNTCON_CNTR0_BITBAND (*(volatile unsigned *)0x43700BF4)
#define MCCNTCON_CNTR0_BIT 29
#define MCCNTCON_CNTR1_MASK 0x40000000
#define MCCNTCON_CNTR1 0x40000000
#define MCCNTCON_CNTR1_BITBAND_ADDRESS 0x43700BF8
#define MCCNTCON_CNTR1_BITBAND (*(volatile unsigned *)0x43700BF8)
#define MCCNTCON_CNTR1_BIT 30
#define MCCNTCON_CNTR2_MASK 0x80000000
#define MCCNTCON_CNTR2 0x80000000
#define MCCNTCON_CNTR2_BITBAND_ADDRESS 0x43700BFC
#define MCCNTCON_CNTR2_BITBAND (*(volatile unsigned *)0x43700BFC)
#define MCCNTCON_CNTR2_BIT 31

#define MCCNTCON_SET (*(volatile unsigned long *)0x400B8060)
#define MCCNTCON_SET_OFFSET 0x60
#define MCCNTCON_SET_TC0MCI0_RE_MASK 0x1
#define MCCNTCON_SET_TC0MCI0_RE 0x1
#define MCCNTCON_SET_TC0MCI0_RE_BITBAND_ADDRESS 0x43700C00
#define MCCNTCON_SET_TC0MCI0_RE_BITBAND (*(volatile unsigned *)0x43700C00)
#define MCCNTCON_SET_TC0MCI0_RE_BIT 0
#define MCCNTCON_SET_TC0MCI0_FE_MASK 0x2
#define MCCNTCON_SET_TC0MCI0_FE 0x2
#define MCCNTCON_SET_TC0MCI0_FE_BITBAND_ADDRESS 0x43700C04
#define MCCNTCON_SET_TC0MCI0_FE_BITBAND (*(volatile unsigned *)0x43700C04)
#define MCCNTCON_SET_TC0MCI0_FE_BIT 1
#define MCCNTCON_SET_TC0MCI1_RE_MASK 0x4
#define MCCNTCON_SET_TC0MCI1_RE 0x4
#define MCCNTCON_SET_TC0MCI1_RE_BITBAND_ADDRESS 0x43700C08
#define MCCNTCON_SET_TC0MCI1_RE_BITBAND (*(volatile unsigned *)0x43700C08)
#define MCCNTCON_SET_TC0MCI1_RE_BIT 2
#define MCCNTCON_SET_TC0MCI1_FE_MASK 0x8
#define MCCNTCON_SET_TC0MCI1_FE 0x8
#define MCCNTCON_SET_TC0MCI1_FE_BITBAND_ADDRESS 0x43700C0C
#define MCCNTCON_SET_TC0MCI1_FE_BITBAND (*(volatile unsigned *)0x43700C0C)
#define MCCNTCON_SET_TC0MCI1_FE_BIT 3
#define MCCNTCON_SET_TC0MCI2_RE_MASK 0x10
#define MCCNTCON_SET_TC0MCI2_RE 0x10
#define MCCNTCON_SET_TC0MCI2_RE_BITBAND_ADDRESS 0x43700C10
#define MCCNTCON_SET_TC0MCI2_RE_BITBAND (*(volatile unsigned *)0x43700C10)
#define MCCNTCON_SET_TC0MCI2_RE_BIT 4
#define MCCNTCON_SET_TC0MCI2_FE_MASK 0x20
#define MCCNTCON_SET_TC0MCI2_FE 0x20
#define MCCNTCON_SET_TC0MCI2_FE_BITBAND_ADDRESS 0x43700C14
#define MCCNTCON_SET_TC0MCI2_FE_BITBAND (*(volatile unsigned *)0x43700C14)
#define MCCNTCON_SET_TC0MCI2_FE_BIT 5
#define MCCNTCON_SET_TC1MCI0_RE_MASK 0x40
#define MCCNTCON_SET_TC1MCI0_RE 0x40
#define MCCNTCON_SET_TC1MCI0_RE_BITBAND_ADDRESS 0x43700C18
#define MCCNTCON_SET_TC1MCI0_RE_BITBAND (*(volatile unsigned *)0x43700C18)
#define MCCNTCON_SET_TC1MCI0_RE_BIT 6
#define MCCNTCON_SET_TC1MCI0_FE_MASK 0x80
#define MCCNTCON_SET_TC1MCI0_FE 0x80
#define MCCNTCON_SET_TC1MCI0_FE_BITBAND_ADDRESS 0x43700C1C
#define MCCNTCON_SET_TC1MCI0_FE_BITBAND (*(volatile unsigned *)0x43700C1C)
#define MCCNTCON_SET_TC1MCI0_FE_BIT 7
#define MCCNTCON_SET_TC1MCI1_RE_MASK 0x100
#define MCCNTCON_SET_TC1MCI1_RE 0x100
#define MCCNTCON_SET_TC1MCI1_RE_BITBAND_ADDRESS 0x43700C20
#define MCCNTCON_SET_TC1MCI1_RE_BITBAND (*(volatile unsigned *)0x43700C20)
#define MCCNTCON_SET_TC1MCI1_RE_BIT 8
#define MCCNTCON_SET_TC1MCI1_FE_MASK 0x200
#define MCCNTCON_SET_TC1MCI1_FE 0x200
#define MCCNTCON_SET_TC1MCI1_FE_BITBAND_ADDRESS 0x43700C24
#define MCCNTCON_SET_TC1MCI1_FE_BITBAND (*(volatile unsigned *)0x43700C24)
#define MCCNTCON_SET_TC1MCI1_FE_BIT 9
#define MCCNTCON_SET_TC1MCI2_RE_MASK 0x400
#define MCCNTCON_SET_TC1MCI2_RE 0x400
#define MCCNTCON_SET_TC1MCI2_RE_BITBAND_ADDRESS 0x43700C28
#define MCCNTCON_SET_TC1MCI2_RE_BITBAND (*(volatile unsigned *)0x43700C28)
#define MCCNTCON_SET_TC1MCI2_RE_BIT 10
#define MCCNTCON_SET_TC1MCI2_FE_MASK 0x800
#define MCCNTCON_SET_TC1MCI2_FE 0x800
#define MCCNTCON_SET_TC1MCI2_FE_BITBAND_ADDRESS 0x43700C2C
#define MCCNTCON_SET_TC1MCI2_FE_BITBAND (*(volatile unsigned *)0x43700C2C)
#define MCCNTCON_SET_TC1MCI2_FE_BIT 11
#define MCCNTCON_SET_TC2MCI0_RE_MASK 0x1000
#define MCCNTCON_SET_TC2MCI0_RE 0x1000
#define MCCNTCON_SET_TC2MCI0_RE_BITBAND_ADDRESS 0x43700C30
#define MCCNTCON_SET_TC2MCI0_RE_BITBAND (*(volatile unsigned *)0x43700C30)
#define MCCNTCON_SET_TC2MCI0_RE_BIT 12
#define MCCNTCON_SET_TC2MCI0_FE_MASK 0x2000
#define MCCNTCON_SET_TC2MCI0_FE 0x2000
#define MCCNTCON_SET_TC2MCI0_FE_BITBAND_ADDRESS 0x43700C34
#define MCCNTCON_SET_TC2MCI0_FE_BITBAND (*(volatile unsigned *)0x43700C34)
#define MCCNTCON_SET_TC2MCI0_FE_BIT 13
#define MCCNTCON_SET_TC2MCI1_RE_MASK 0x4000
#define MCCNTCON_SET_TC2MCI1_RE 0x4000
#define MCCNTCON_SET_TC2MCI1_RE_BITBAND_ADDRESS 0x43700C38
#define MCCNTCON_SET_TC2MCI1_RE_BITBAND (*(volatile unsigned *)0x43700C38)
#define MCCNTCON_SET_TC2MCI1_RE_BIT 14
#define MCCNTCON_SET_TC2MCI1_FE_MASK 0x8000
#define MCCNTCON_SET_TC2MCI1_FE 0x8000
#define MCCNTCON_SET_TC2MCI1_FE_BITBAND_ADDRESS 0x43700C3C
#define MCCNTCON_SET_TC2MCI1_FE_BITBAND (*(volatile unsigned *)0x43700C3C)
#define MCCNTCON_SET_TC2MCI1_FE_BIT 15
#define MCCNTCON_SET_TC2MCI2_RE_MASK 0x10000
#define MCCNTCON_SET_TC2MCI2_RE 0x10000
#define MCCNTCON_SET_TC2MCI2_RE_BITBAND_ADDRESS 0x43700C40
#define MCCNTCON_SET_TC2MCI2_RE_BITBAND (*(volatile unsigned *)0x43700C40)
#define MCCNTCON_SET_TC2MCI2_RE_BIT 16
#define MCCNTCON_SET_TC2MCI2_FE_MASK 0x20000
#define MCCNTCON_SET_TC2MCI2_FE 0x20000
#define MCCNTCON_SET_TC2MCI2_FE_BITBAND_ADDRESS 0x43700C44
#define MCCNTCON_SET_TC2MCI2_FE_BITBAND (*(volatile unsigned *)0x43700C44)
#define MCCNTCON_SET_TC2MCI2_FE_BIT 17
#define MCCNTCON_SET_CNTR0_MASK 0x20000000
#define MCCNTCON_SET_CNTR0 0x20000000
#define MCCNTCON_SET_CNTR0_BITBAND_ADDRESS 0x43700C74
#define MCCNTCON_SET_CNTR0_BITBAND (*(volatile unsigned *)0x43700C74)
#define MCCNTCON_SET_CNTR0_BIT 29
#define MCCNTCON_SET_CNTR1_MASK 0x40000000
#define MCCNTCON_SET_CNTR1 0x40000000
#define MCCNTCON_SET_CNTR1_BITBAND_ADDRESS 0x43700C78
#define MCCNTCON_SET_CNTR1_BITBAND (*(volatile unsigned *)0x43700C78)
#define MCCNTCON_SET_CNTR1_BIT 30
#define MCCNTCON_SET_CNTR2_MASK 0x80000000
#define MCCNTCON_SET_CNTR2 0x80000000
#define MCCNTCON_SET_CNTR2_BITBAND_ADDRESS 0x43700C7C
#define MCCNTCON_SET_CNTR2_BITBAND (*(volatile unsigned *)0x43700C7C)
#define MCCNTCON_SET_CNTR2_BIT 31

#define MCCNTCON_CLR (*(volatile unsigned long *)0x400B8064)
#define MCCNTCON_CLR_OFFSET 0x64
#define MCCNTCON_CLR_TC0MCI0_RE_MASK 0x1
#define MCCNTCON_CLR_TC0MCI0_RE 0x1
#define MCCNTCON_CLR_TC0MCI0_RE_BITBAND_ADDRESS 0x43700C80
#define MCCNTCON_CLR_TC0MCI0_RE_BITBAND (*(volatile unsigned *)0x43700C80)
#define MCCNTCON_CLR_TC0MCI0_RE_BIT 0
#define MCCNTCON_CLR_TC0MCI0_FE_MASK 0x2
#define MCCNTCON_CLR_TC0MCI0_FE 0x2
#define MCCNTCON_CLR_TC0MCI0_FE_BITBAND_ADDRESS 0x43700C84
#define MCCNTCON_CLR_TC0MCI0_FE_BITBAND (*(volatile unsigned *)0x43700C84)
#define MCCNTCON_CLR_TC0MCI0_FE_BIT 1
#define MCCNTCON_CLR_TC0MCI1_RE_MASK 0x4
#define MCCNTCON_CLR_TC0MCI1_RE 0x4
#define MCCNTCON_CLR_TC0MCI1_RE_BITBAND_ADDRESS 0x43700C88
#define MCCNTCON_CLR_TC0MCI1_RE_BITBAND (*(volatile unsigned *)0x43700C88)
#define MCCNTCON_CLR_TC0MCI1_RE_BIT 2
#define MCCNTCON_CLR_TC0MCI1_FE_MASK 0x8
#define MCCNTCON_CLR_TC0MCI1_FE 0x8
#define MCCNTCON_CLR_TC0MCI1_FE_BITBAND_ADDRESS 0x43700C8C
#define MCCNTCON_CLR_TC0MCI1_FE_BITBAND (*(volatile unsigned *)0x43700C8C)
#define MCCNTCON_CLR_TC0MCI1_FE_BIT 3
#define MCCNTCON_CLR_TC0MCI2_RE_MASK 0x10
#define MCCNTCON_CLR_TC0MCI2_RE 0x10
#define MCCNTCON_CLR_TC0MCI2_RE_BITBAND_ADDRESS 0x43700C90
#define MCCNTCON_CLR_TC0MCI2_RE_BITBAND (*(volatile unsigned *)0x43700C90)
#define MCCNTCON_CLR_TC0MCI2_RE_BIT 4
#define MCCNTCON_CLR_TC0MCI2_FE_MASK 0x20
#define MCCNTCON_CLR_TC0MCI2_FE 0x20
#define MCCNTCON_CLR_TC0MCI2_FE_BITBAND_ADDRESS 0x43700C94
#define MCCNTCON_CLR_TC0MCI2_FE_BITBAND (*(volatile unsigned *)0x43700C94)
#define MCCNTCON_CLR_TC0MCI2_FE_BIT 5
#define MCCNTCON_CLR_TC1MCI0_RE_MASK 0x40
#define MCCNTCON_CLR_TC1MCI0_RE 0x40
#define MCCNTCON_CLR_TC1MCI0_RE_BITBAND_ADDRESS 0x43700C98
#define MCCNTCON_CLR_TC1MCI0_RE_BITBAND (*(volatile unsigned *)0x43700C98)
#define MCCNTCON_CLR_TC1MCI0_RE_BIT 6
#define MCCNTCON_CLR_TC1MCI0_FE_MASK 0x80
#define MCCNTCON_CLR_TC1MCI0_FE 0x80
#define MCCNTCON_CLR_TC1MCI0_FE_BITBAND_ADDRESS 0x43700C9C
#define MCCNTCON_CLR_TC1MCI0_FE_BITBAND (*(volatile unsigned *)0x43700C9C)
#define MCCNTCON_CLR_TC1MCI0_FE_BIT 7
#define MCCNTCON_CLR_TC1MCI1_RE_MASK 0x100
#define MCCNTCON_CLR_TC1MCI1_RE 0x100
#define MCCNTCON_CLR_TC1MCI1_RE_BITBAND_ADDRESS 0x43700CA0
#define MCCNTCON_CLR_TC1MCI1_RE_BITBAND (*(volatile unsigned *)0x43700CA0)
#define MCCNTCON_CLR_TC1MCI1_RE_BIT 8
#define MCCNTCON_CLR_TC1MCI1_FE_MASK 0x200
#define MCCNTCON_CLR_TC1MCI1_FE 0x200
#define MCCNTCON_CLR_TC1MCI1_FE_BITBAND_ADDRESS 0x43700CA4
#define MCCNTCON_CLR_TC1MCI1_FE_BITBAND (*(volatile unsigned *)0x43700CA4)
#define MCCNTCON_CLR_TC1MCI1_FE_BIT 9
#define MCCNTCON_CLR_TC1MCI2_RE_MASK 0x400
#define MCCNTCON_CLR_TC1MCI2_RE 0x400
#define MCCNTCON_CLR_TC1MCI2_RE_BITBAND_ADDRESS 0x43700CA8
#define MCCNTCON_CLR_TC1MCI2_RE_BITBAND (*(volatile unsigned *)0x43700CA8)
#define MCCNTCON_CLR_TC1MCI2_RE_BIT 10
#define MCCNTCON_CLR_TC1MCI2_FE_MASK 0x800
#define MCCNTCON_CLR_TC1MCI2_FE 0x800
#define MCCNTCON_CLR_TC1MCI2_FE_BITBAND_ADDRESS 0x43700CAC
#define MCCNTCON_CLR_TC1MCI2_FE_BITBAND (*(volatile unsigned *)0x43700CAC)
#define MCCNTCON_CLR_TC1MCI2_FE_BIT 11
#define MCCNTCON_CLR_TC2MCI0_RE_MASK 0x1000
#define MCCNTCON_CLR_TC2MCI0_RE 0x1000
#define MCCNTCON_CLR_TC2MCI0_RE_BITBAND_ADDRESS 0x43700CB0
#define MCCNTCON_CLR_TC2MCI0_RE_BITBAND (*(volatile unsigned *)0x43700CB0)
#define MCCNTCON_CLR_TC2MCI0_RE_BIT 12
#define MCCNTCON_CLR_TC2MCI0_FE_MASK 0x2000
#define MCCNTCON_CLR_TC2MCI0_FE 0x2000
#define MCCNTCON_CLR_TC2MCI0_FE_BITBAND_ADDRESS 0x43700CB4
#define MCCNTCON_CLR_TC2MCI0_FE_BITBAND (*(volatile unsigned *)0x43700CB4)
#define MCCNTCON_CLR_TC2MCI0_FE_BIT 13
#define MCCNTCON_CLR_TC2MCI1_RE_MASK 0x4000
#define MCCNTCON_CLR_TC2MCI1_RE 0x4000
#define MCCNTCON_CLR_TC2MCI1_RE_BITBAND_ADDRESS 0x43700CB8
#define MCCNTCON_CLR_TC2MCI1_RE_BITBAND (*(volatile unsigned *)0x43700CB8)
#define MCCNTCON_CLR_TC2MCI1_RE_BIT 14
#define MCCNTCON_CLR_TC2MCI1_FE_MASK 0x8000
#define MCCNTCON_CLR_TC2MCI1_FE 0x8000
#define MCCNTCON_CLR_TC2MCI1_FE_BITBAND_ADDRESS 0x43700CBC
#define MCCNTCON_CLR_TC2MCI1_FE_BITBAND (*(volatile unsigned *)0x43700CBC)
#define MCCNTCON_CLR_TC2MCI1_FE_BIT 15
#define MCCNTCON_CLR_TC2MCI2_RE_MASK 0x10000
#define MCCNTCON_CLR_TC2MCI2_RE 0x10000
#define MCCNTCON_CLR_TC2MCI2_RE_BITBAND_ADDRESS 0x43700CC0
#define MCCNTCON_CLR_TC2MCI2_RE_BITBAND (*(volatile unsigned *)0x43700CC0)
#define MCCNTCON_CLR_TC2MCI2_RE_BIT 16
#define MCCNTCON_CLR_TC2MCI2_FE_MASK 0x20000
#define MCCNTCON_CLR_TC2MCI2_FE 0x20000
#define MCCNTCON_CLR_TC2MCI2_FE_BITBAND_ADDRESS 0x43700CC4
#define MCCNTCON_CLR_TC2MCI2_FE_BITBAND (*(volatile unsigned *)0x43700CC4)
#define MCCNTCON_CLR_TC2MCI2_FE_BIT 17
#define MCCNTCON_CLR_CNTR0_MASK 0x20000000
#define MCCNTCON_CLR_CNTR0 0x20000000
#define MCCNTCON_CLR_CNTR0_BITBAND_ADDRESS 0x43700CF4
#define MCCNTCON_CLR_CNTR0_BITBAND (*(volatile unsigned *)0x43700CF4)
#define MCCNTCON_CLR_CNTR0_BIT 29
#define MCCNTCON_CLR_CNTR1_MASK 0x40000000
#define MCCNTCON_CLR_CNTR1 0x40000000
#define MCCNTCON_CLR_CNTR1_BITBAND_ADDRESS 0x43700CF8
#define MCCNTCON_CLR_CNTR1_BITBAND (*(volatile unsigned *)0x43700CF8)
#define MCCNTCON_CLR_CNTR1_BIT 30
#define MCCNTCON_CLR_CNTR2_MASK 0x80000000
#define MCCNTCON_CLR_CNTR2 0x80000000
#define MCCNTCON_CLR_CNTR2_BITBAND_ADDRESS 0x43700CFC
#define MCCNTCON_CLR_CNTR2_BITBAND (*(volatile unsigned *)0x43700CFC)
#define MCCNTCON_CLR_CNTR2_BIT 31

#define MCINTF (*(volatile unsigned long *)0x400B8068)
#define MCINTF_OFFSET 0x68
#define MCINTF_ILIM0_MASK 0x1
#define MCINTF_ILIM0 0x1
#define MCINTF_ILIM0_BITBAND_ADDRESS 0x43700D00
#define MCINTF_ILIM0_BITBAND (*(volatile unsigned *)0x43700D00)
#define MCINTF_ILIM0_BIT 0
#define MCINTF_IMAT0_MASK 0x2
#define MCINTF_IMAT0 0x2
#define MCINTF_IMAT0_BITBAND_ADDRESS 0x43700D04
#define MCINTF_IMAT0_BITBAND (*(volatile unsigned *)0x43700D04)
#define MCINTF_IMAT0_BIT 1
#define MCINTF_ICAP0_MASK 0x4
#define MCINTF_ICAP0 0x4
#define MCINTF_ICAP0_BITBAND_ADDRESS 0x43700D08
#define MCINTF_ICAP0_BITBAND (*(volatile unsigned *)0x43700D08)
#define MCINTF_ICAP0_BIT 2
#define MCINTF_ILIM1_MASK 0x10
#define MCINTF_ILIM1 0x10
#define MCINTF_ILIM1_BITBAND_ADDRESS 0x43700D10
#define MCINTF_ILIM1_BITBAND (*(volatile unsigned *)0x43700D10)
#define MCINTF_ILIM1_BIT 4
#define MCINTF_IMAT1_MASK 0x20
#define MCINTF_IMAT1 0x20
#define MCINTF_IMAT1_BITBAND_ADDRESS 0x43700D14
#define MCINTF_IMAT1_BITBAND (*(volatile unsigned *)0x43700D14)
#define MCINTF_IMAT1_BIT 5
#define MCINTF_ICAP1_MASK 0x40
#define MCINTF_ICAP1 0x40
#define MCINTF_ICAP1_BITBAND_ADDRESS 0x43700D18
#define MCINTF_ICAP1_BITBAND (*(volatile unsigned *)0x43700D18)
#define MCINTF_ICAP1_BIT 6
#define MCINTF_ILIM2_MASK 0x100
#define MCINTF_ILIM2 0x100
#define MCINTF_ILIM2_BITBAND_ADDRESS 0x43700D20
#define MCINTF_ILIM2_BITBAND (*(volatile unsigned *)0x43700D20)
#define MCINTF_ILIM2_BIT 8
#define MCINTF_IMAT2_MASK 0x200
#define MCINTF_IMAT2 0x200
#define MCINTF_IMAT2_BITBAND_ADDRESS 0x43700D24
#define MCINTF_IMAT2_BITBAND (*(volatile unsigned *)0x43700D24)
#define MCINTF_IMAT2_BIT 9
#define MCINTF_ICAP2_MASK 0x400
#define MCINTF_ICAP2 0x400
#define MCINTF_ICAP2_BITBAND_ADDRESS 0x43700D28
#define MCINTF_ICAP2_BITBAND (*(volatile unsigned *)0x43700D28)
#define MCINTF_ICAP2_BIT 10
#define MCINTF_ABORT_MASK 0x8000
#define MCINTF_ABORT 0x8000
#define MCINTF_ABORT_BITBAND_ADDRESS 0x43700D3C
#define MCINTF_ABORT_BITBAND (*(volatile unsigned *)0x43700D3C)
#define MCINTF_ABORT_BIT 15

#define MCINTF_SET (*(volatile unsigned long *)0x400B806C)
#define MCINTF_SET_OFFSET 0x6C
#define MCINTF_SET_ILIM0_MASK 0x1
#define MCINTF_SET_ILIM0 0x1
#define MCINTF_SET_ILIM0_BITBAND_ADDRESS 0x43700D80
#define MCINTF_SET_ILIM0_BITBAND (*(volatile unsigned *)0x43700D80)
#define MCINTF_SET_ILIM0_BIT 0
#define MCINTF_SET_IMAT0_MASK 0x2
#define MCINTF_SET_IMAT0 0x2
#define MCINTF_SET_IMAT0_BITBAND_ADDRESS 0x43700D84
#define MCINTF_SET_IMAT0_BITBAND (*(volatile unsigned *)0x43700D84)
#define MCINTF_SET_IMAT0_BIT 1
#define MCINTF_SET_ICAP0_MASK 0x4
#define MCINTF_SET_ICAP0 0x4
#define MCINTF_SET_ICAP0_BITBAND_ADDRESS 0x43700D88
#define MCINTF_SET_ICAP0_BITBAND (*(volatile unsigned *)0x43700D88)
#define MCINTF_SET_ICAP0_BIT 2
#define MCINTF_SET_ILIM1_MASK 0x10
#define MCINTF_SET_ILIM1 0x10
#define MCINTF_SET_ILIM1_BITBAND_ADDRESS 0x43700D90
#define MCINTF_SET_ILIM1_BITBAND (*(volatile unsigned *)0x43700D90)
#define MCINTF_SET_ILIM1_BIT 4
#define MCINTF_SET_IMAT1_MASK 0x20
#define MCINTF_SET_IMAT1 0x20
#define MCINTF_SET_IMAT1_BITBAND_ADDRESS 0x43700D94
#define MCINTF_SET_IMAT1_BITBAND (*(volatile unsigned *)0x43700D94)
#define MCINTF_SET_IMAT1_BIT 5
#define MCINTF_SET_ICAP1_MASK 0x40
#define MCINTF_SET_ICAP1 0x40
#define MCINTF_SET_ICAP1_BITBAND_ADDRESS 0x43700D98
#define MCINTF_SET_ICAP1_BITBAND (*(volatile unsigned *)0x43700D98)
#define MCINTF_SET_ICAP1_BIT 6
#define MCINTF_SET_ILIM2_MASK 0x100
#define MCINTF_SET_ILIM2 0x100
#define MCINTF_SET_ILIM2_BITBAND_ADDRESS 0x43700DA0
#define MCINTF_SET_ILIM2_BITBAND (*(volatile unsigned *)0x43700DA0)
#define MCINTF_SET_ILIM2_BIT 8
#define MCINTF_SET_IMAT2_MASK 0x200
#define MCINTF_SET_IMAT2 0x200
#define MCINTF_SET_IMAT2_BITBAND_ADDRESS 0x43700DA4
#define MCINTF_SET_IMAT2_BITBAND (*(volatile unsigned *)0x43700DA4)
#define MCINTF_SET_IMAT2_BIT 9
#define MCINTF_SET_ICAP2_MASK 0x400
#define MCINTF_SET_ICAP2 0x400
#define MCINTF_SET_ICAP2_BITBAND_ADDRESS 0x43700DA8
#define MCINTF_SET_ICAP2_BITBAND (*(volatile unsigned *)0x43700DA8)
#define MCINTF_SET_ICAP2_BIT 10
#define MCINTF_SET_ABORT_MASK 0x8000
#define MCINTF_SET_ABORT 0x8000
#define MCINTF_SET_ABORT_BITBAND_ADDRESS 0x43700DBC
#define MCINTF_SET_ABORT_BITBAND (*(volatile unsigned *)0x43700DBC)
#define MCINTF_SET_ABORT_BIT 15

#define MCINTF_CLR (*(volatile unsigned long *)0x400B8070)
#define MCINTF_CLR_OFFSET 0x70
#define MCINTF_CLR_ILIM0_MASK 0x1
#define MCINTF_CLR_ILIM0 0x1
#define MCINTF_CLR_ILIM0_BITBAND_ADDRESS 0x43700E00
#define MCINTF_CLR_ILIM0_BITBAND (*(volatile unsigned *)0x43700E00)
#define MCINTF_CLR_ILIM0_BIT 0
#define MCINTF_CLR_IMAT0_MASK 0x2
#define MCINTF_CLR_IMAT0 0x2
#define MCINTF_CLR_IMAT0_BITBAND_ADDRESS 0x43700E04
#define MCINTF_CLR_IMAT0_BITBAND (*(volatile unsigned *)0x43700E04)
#define MCINTF_CLR_IMAT0_BIT 1
#define MCINTF_CLR_ICAP0_MASK 0x4
#define MCINTF_CLR_ICAP0 0x4
#define MCINTF_CLR_ICAP0_BITBAND_ADDRESS 0x43700E08
#define MCINTF_CLR_ICAP0_BITBAND (*(volatile unsigned *)0x43700E08)
#define MCINTF_CLR_ICAP0_BIT 2
#define MCINTF_CLR_ILIM1_MASK 0x10
#define MCINTF_CLR_ILIM1 0x10
#define MCINTF_CLR_ILIM1_BITBAND_ADDRESS 0x43700E10
#define MCINTF_CLR_ILIM1_BITBAND (*(volatile unsigned *)0x43700E10)
#define MCINTF_CLR_ILIM1_BIT 4
#define MCINTF_CLR_IMAT1_MASK 0x20
#define MCINTF_CLR_IMAT1 0x20
#define MCINTF_CLR_IMAT1_BITBAND_ADDRESS 0x43700E14
#define MCINTF_CLR_IMAT1_BITBAND (*(volatile unsigned *)0x43700E14)
#define MCINTF_CLR_IMAT1_BIT 5
#define MCINTF_CLR_ICAP1_MASK 0x40
#define MCINTF_CLR_ICAP1 0x40
#define MCINTF_CLR_ICAP1_BITBAND_ADDRESS 0x43700E18
#define MCINTF_CLR_ICAP1_BITBAND (*(volatile unsigned *)0x43700E18)
#define MCINTF_CLR_ICAP1_BIT 6
#define MCINTF_CLR_ILIM2_MASK 0x100
#define MCINTF_CLR_ILIM2 0x100
#define MCINTF_CLR_ILIM2_BITBAND_ADDRESS 0x43700E20
#define MCINTF_CLR_ILIM2_BITBAND (*(volatile unsigned *)0x43700E20)
#define MCINTF_CLR_ILIM2_BIT 8
#define MCINTF_CLR_IMAT2_MASK 0x200
#define MCINTF_CLR_IMAT2 0x200
#define MCINTF_CLR_IMAT2_BITBAND_ADDRESS 0x43700E24
#define MCINTF_CLR_IMAT2_BITBAND (*(volatile unsigned *)0x43700E24)
#define MCINTF_CLR_IMAT2_BIT 9
#define MCINTF_CLR_ICAP2_MASK 0x400
#define MCINTF_CLR_ICAP2 0x400
#define MCINTF_CLR_ICAP2_BITBAND_ADDRESS 0x43700E28
#define MCINTF_CLR_ICAP2_BITBAND (*(volatile unsigned *)0x43700E28)
#define MCINTF_CLR_ICAP2_BIT 10
#define MCINTF_CLR_ABORT_MASK 0x8000
#define MCINTF_CLR_ABORT 0x8000
#define MCINTF_CLR_ABORT_BITBAND_ADDRESS 0x43700E3C
#define MCINTF_CLR_ABORT_BITBAND (*(volatile unsigned *)0x43700E3C)
#define MCINTF_CLR_ABORT_BIT 15

#define MCCAP_CLR (*(volatile unsigned long *)0x400B8074)
#define MCCAP_CLR_OFFSET 0x74
#define MCCAP_CLR_CAP_CLR0_MASK 0x1
#define MCCAP_CLR_CAP_CLR0 0x1
#define MCCAP_CLR_CAP_CLR0_BITBAND_ADDRESS 0x43700E80
#define MCCAP_CLR_CAP_CLR0_BITBAND (*(volatile unsigned *)0x43700E80)
#define MCCAP_CLR_CAP_CLR0_BIT 0
#define MCCAP_CLR_CAP_CLR1_MASK 0x2
#define MCCAP_CLR_CAP_CLR1 0x2
#define MCCAP_CLR_CAP_CLR1_BITBAND_ADDRESS 0x43700E84
#define MCCAP_CLR_CAP_CLR1_BITBAND (*(volatile unsigned *)0x43700E84)
#define MCCAP_CLR_CAP_CLR1_BIT 1
#define MCCAP_CLR_CAP_CLR2_MASK 0x4
#define MCCAP_CLR_CAP_CLR2 0x4
#define MCCAP_CLR_CAP_CLR2_BITBAND_ADDRESS 0x43700E88
#define MCCAP_CLR_CAP_CLR2_BITBAND (*(volatile unsigned *)0x43700E88)
#define MCCAP_CLR_CAP_CLR2_BIT 2

#define QEI_BASE_ADDRESS 0x400BC000

#define QEICON (*(volatile unsigned long *)0x400BC000)
#define QEICON_OFFSET 0x0
#define QEICON_RESP_MASK 0x1
#define QEICON_RESP 0x1
#define QEICON_RESP_BITBAND_ADDRESS 0x43780000
#define QEICON_RESP_BITBAND (*(volatile unsigned *)0x43780000)
#define QEICON_RESP_BIT 0
#define QEICON_RESPI_MASK 0x2
#define QEICON_RESPI 0x2
#define QEICON_RESPI_BITBAND_ADDRESS 0x43780004
#define QEICON_RESPI_BITBAND (*(volatile unsigned *)0x43780004)
#define QEICON_RESPI_BIT 1
#define QEICON_RESV_MASK 0x4
#define QEICON_RESV 0x4
#define QEICON_RESV_BITBAND_ADDRESS 0x43780008
#define QEICON_RESV_BITBAND (*(volatile unsigned *)0x43780008)
#define QEICON_RESV_BIT 2
#define QEICON_RESi_MASK 0x8
#define QEICON_RESi 0x8
#define QEICON_RESi_BITBAND_ADDRESS 0x4378000C
#define QEICON_RESi_BITBAND (*(volatile unsigned *)0x4378000C)
#define QEICON_RESi_BIT 3

#define QEISTAT (*(volatile unsigned long *)0x400BC004)
#define QEISTAT_OFFSET 0x4
#define QEISTAT_DIR_MASK 0x1
#define QEISTAT_DIR 0x1
#define QEISTAT_DIR_BITBAND_ADDRESS 0x43780080
#define QEISTAT_DIR_BITBAND (*(volatile unsigned *)0x43780080)
#define QEISTAT_DIR_BIT 0

#define QEICONF (*(volatile unsigned long *)0x400BC008)
#define QEICONF_OFFSET 0x8
#define QEICONF_DIRINV_MASK 0x1
#define QEICONF_DIRINV 0x1
#define QEICONF_DIRINV_BITBAND_ADDRESS 0x43780100
#define QEICONF_DIRINV_BITBAND (*(volatile unsigned *)0x43780100)
#define QEICONF_DIRINV_BIT 0
#define QEICONF_SIGMODE_MASK 0x2
#define QEICONF_SIGMODE 0x2
#define QEICONF_SIGMODE_BITBAND_ADDRESS 0x43780104
#define QEICONF_SIGMODE_BITBAND (*(volatile unsigned *)0x43780104)
#define QEICONF_SIGMODE_BIT 1
#define QEICONF_CAPMODE_MASK 0x4
#define QEICONF_CAPMODE 0x4
#define QEICONF_CAPMODE_BITBAND_ADDRESS 0x43780108
#define QEICONF_CAPMODE_BITBAND (*(volatile unsigned *)0x43780108)
#define QEICONF_CAPMODE_BIT 2
#define QEICONF_INVINX_MASK 0x8
#define QEICONF_INVINX 0x8
#define QEICONF_INVINX_BITBAND_ADDRESS 0x4378010C
#define QEICONF_INVINX_BITBAND (*(volatile unsigned *)0x4378010C)
#define QEICONF_INVINX_BIT 3

#define QEIPOS (*(volatile unsigned long *)0x400BC00C)
#define QEIPOS_OFFSET 0xC

#define QEIMAXPOS (*(volatile unsigned long *)0x400BC010)
#define QEIMAXPOS_OFFSET 0x10

#define CMPOS0 (*(volatile unsigned long *)0x400BC014)
#define CMPOS0_OFFSET 0x14

#define CMPOS1 (*(volatile unsigned long *)0x400BC018)
#define CMPOS1_OFFSET 0x18

#define CMPOS2 (*(volatile unsigned long *)0x400BC01C)
#define CMPOS2_OFFSET 0x1C

#define INXCNT (*(volatile unsigned long *)0x400BC020)
#define INXCNT_OFFSET 0x20

#define INXCMP (*(volatile unsigned long *)0x400BC024)
#define INXCMP_OFFSET 0x24

#define FILTER (*(volatile unsigned long *)0x400BC03C)
#define FILTER_OFFSET 0x3C

#define QEILOAD (*(volatile unsigned long *)0x400BC028)
#define QEILOAD_OFFSET 0x28

#define QEITIME (*(volatile unsigned long *)0x400BC02C)
#define QEITIME_OFFSET 0x2C

#define QEIVEL (*(volatile unsigned long *)0x400BC030)
#define QEIVEL_OFFSET 0x30

#define QEICAP (*(volatile unsigned long *)0x400BC034)
#define QEICAP_OFFSET 0x34

#define VELCOMP (*(volatile unsigned long *)0x400BC038)
#define VELCOMP_OFFSET 0x38

#define QEIIEC (*(volatile unsigned long *)0x400BCFD8)
#define QEIIEC_OFFSET 0xFD8
#define QEIIEC_INX_EN_MASK 0x1
#define QEIIEC_INX_EN 0x1
#define QEIIEC_INX_EN_BITBAND_ADDRESS 0x4379FB00
#define QEIIEC_INX_EN_BITBAND (*(volatile unsigned *)0x4379FB00)
#define QEIIEC_INX_EN_BIT 0
#define QEIIEC_TIM_EN_MASK 0x2
#define QEIIEC_TIM_EN 0x2
#define QEIIEC_TIM_EN_BITBAND_ADDRESS 0x4379FB04
#define QEIIEC_TIM_EN_BITBAND (*(volatile unsigned *)0x4379FB04)
#define QEIIEC_TIM_EN_BIT 1
#define QEIIEC_VELC_EN_MASK 0x4
#define QEIIEC_VELC_EN 0x4
#define QEIIEC_VELC_EN_BITBAND_ADDRESS 0x4379FB08
#define QEIIEC_VELC_EN_BITBAND (*(volatile unsigned *)0x4379FB08)
#define QEIIEC_VELC_EN_BIT 2
#define QEIIEC_DIR_EN_MASK 0x8
#define QEIIEC_DIR_EN 0x8
#define QEIIEC_DIR_EN_BITBAND_ADDRESS 0x4379FB0C
#define QEIIEC_DIR_EN_BITBAND (*(volatile unsigned *)0x4379FB0C)
#define QEIIEC_DIR_EN_BIT 3
#define QEIIEC_ERR_EN_MASK 0x10
#define QEIIEC_ERR_EN 0x10
#define QEIIEC_ERR_EN_BITBAND_ADDRESS 0x4379FB10
#define QEIIEC_ERR_EN_BITBAND (*(volatile unsigned *)0x4379FB10)
#define QEIIEC_ERR_EN_BIT 4
#define QEIIEC_ENCLK_EN_MASK 0x20
#define QEIIEC_ENCLK_EN 0x20
#define QEIIEC_ENCLK_EN_BITBAND_ADDRESS 0x4379FB14
#define QEIIEC_ENCLK_EN_BITBAND (*(volatile unsigned *)0x4379FB14)
#define QEIIEC_ENCLK_EN_BIT 5
#define QEIIEC_POS0_EN_MASK 0x40
#define QEIIEC_POS0_EN 0x40
#define QEIIEC_POS0_EN_BITBAND_ADDRESS 0x4379FB18
#define QEIIEC_POS0_EN_BITBAND (*(volatile unsigned *)0x4379FB18)
#define QEIIEC_POS0_EN_BIT 6
#define QEIIEC_POS1_EN_MASK 0x80
#define QEIIEC_POS1_EN 0x80
#define QEIIEC_POS1_EN_BITBAND_ADDRESS 0x4379FB1C
#define QEIIEC_POS1_EN_BITBAND (*(volatile unsigned *)0x4379FB1C)
#define QEIIEC_POS1_EN_BIT 7
#define QEIIEC_POS2_EN_MASK 0x100
#define QEIIEC_POS2_EN 0x100
#define QEIIEC_POS2_EN_BITBAND_ADDRESS 0x4379FB20
#define QEIIEC_POS2_EN_BITBAND (*(volatile unsigned *)0x4379FB20)
#define QEIIEC_POS2_EN_BIT 8
#define QEIIEC_POS0REV_Int_MASK 0x400
#define QEIIEC_POS0REV_Int 0x400
#define QEIIEC_POS0REV_Int_BITBAND_ADDRESS 0x4379FB28
#define QEIIEC_POS0REV_Int_BITBAND (*(volatile unsigned *)0x4379FB28)
#define QEIIEC_POS0REV_Int_BIT 10
#define QEIIEC_POS1REV_Int_MASK 0x800
#define QEIIEC_POS1REV_Int 0x800
#define QEIIEC_POS1REV_Int_BITBAND_ADDRESS 0x4379FB2C
#define QEIIEC_POS1REV_Int_BITBAND (*(volatile unsigned *)0x4379FB2C)
#define QEIIEC_POS1REV_Int_BIT 11
#define QEIIEC_POS2REV_Int_MASK 0x1000
#define QEIIEC_POS2REV_Int 0x1000
#define QEIIEC_POS2REV_Int_BITBAND_ADDRESS 0x4379FB30
#define QEIIEC_POS2REV_Int_BITBAND (*(volatile unsigned *)0x4379FB30)
#define QEIIEC_POS2REV_Int_BIT 12
#define QEIIEC_REV_Int_MASK 0x200
#define QEIIEC_REV_Int 0x200
#define QEIIEC_REV_Int_BITBAND_ADDRESS 0x4379FB24
#define QEIIEC_REV_Int_BITBAND (*(volatile unsigned *)0x4379FB24)
#define QEIIEC_REV_Int_BIT 9

#define QEIIES (*(volatile unsigned long *)0x400BCFDC)
#define QEIIES_OFFSET 0xFDC
#define QEIIES_INX_EN_MASK 0x1
#define QEIIES_INX_EN 0x1
#define QEIIES_INX_EN_BITBAND_ADDRESS 0x4379FB80
#define QEIIES_INX_EN_BITBAND (*(volatile unsigned *)0x4379FB80)
#define QEIIES_INX_EN_BIT 0
#define QEIIES_TIM_EN_MASK 0x2
#define QEIIES_TIM_EN 0x2
#define QEIIES_TIM_EN_BITBAND_ADDRESS 0x4379FB84
#define QEIIES_TIM_EN_BITBAND (*(volatile unsigned *)0x4379FB84)
#define QEIIES_TIM_EN_BIT 1
#define QEIIES_VELC_EN_MASK 0x4
#define QEIIES_VELC_EN 0x4
#define QEIIES_VELC_EN_BITBAND_ADDRESS 0x4379FB88
#define QEIIES_VELC_EN_BITBAND (*(volatile unsigned *)0x4379FB88)
#define QEIIES_VELC_EN_BIT 2
#define QEIIES_DIR_EN_MASK 0x8
#define QEIIES_DIR_EN 0x8
#define QEIIES_DIR_EN_BITBAND_ADDRESS 0x4379FB8C
#define QEIIES_DIR_EN_BITBAND (*(volatile unsigned *)0x4379FB8C)
#define QEIIES_DIR_EN_BIT 3
#define QEIIES_ERR_EN_MASK 0x10
#define QEIIES_ERR_EN 0x10
#define QEIIES_ERR_EN_BITBAND_ADDRESS 0x4379FB90
#define QEIIES_ERR_EN_BITBAND (*(volatile unsigned *)0x4379FB90)
#define QEIIES_ERR_EN_BIT 4
#define QEIIES_ENCLK_EN_MASK 0x20
#define QEIIES_ENCLK_EN 0x20
#define QEIIES_ENCLK_EN_BITBAND_ADDRESS 0x4379FB94
#define QEIIES_ENCLK_EN_BITBAND (*(volatile unsigned *)0x4379FB94)
#define QEIIES_ENCLK_EN_BIT 5
#define QEIIES_POS0_EN_MASK 0x40
#define QEIIES_POS0_EN 0x40
#define QEIIES_POS0_EN_BITBAND_ADDRESS 0x4379FB98
#define QEIIES_POS0_EN_BITBAND (*(volatile unsigned *)0x4379FB98)
#define QEIIES_POS0_EN_BIT 6
#define QEIIES_POS1_EN_MASK 0x80
#define QEIIES_POS1_EN 0x80
#define QEIIES_POS1_EN_BITBAND_ADDRESS 0x4379FB9C
#define QEIIES_POS1_EN_BITBAND (*(volatile unsigned *)0x4379FB9C)
#define QEIIES_POS1_EN_BIT 7
#define QEIIES_POS2_EN_MASK 0x100
#define QEIIES_POS2_EN 0x100
#define QEIIES_POS2_EN_BITBAND_ADDRESS 0x4379FBA0
#define QEIIES_POS2_EN_BITBAND (*(volatile unsigned *)0x4379FBA0)
#define QEIIES_POS2_EN_BIT 8
#define QEIIES_POS0REV_Int_MASK 0x400
#define QEIIES_POS0REV_Int 0x400
#define QEIIES_POS0REV_Int_BITBAND_ADDRESS 0x4379FBA8
#define QEIIES_POS0REV_Int_BITBAND (*(volatile unsigned *)0x4379FBA8)
#define QEIIES_POS0REV_Int_BIT 10
#define QEIIES_POS1REV_Int_MASK 0x800
#define QEIIES_POS1REV_Int 0x800
#define QEIIES_POS1REV_Int_BITBAND_ADDRESS 0x4379FBAC
#define QEIIES_POS1REV_Int_BITBAND (*(volatile unsigned *)0x4379FBAC)
#define QEIIES_POS1REV_Int_BIT 11
#define QEIIES_POS2REV_Int_MASK 0x1000
#define QEIIES_POS2REV_Int 0x1000
#define QEIIES_POS2REV_Int_BITBAND_ADDRESS 0x4379FBB0
#define QEIIES_POS2REV_Int_BITBAND (*(volatile unsigned *)0x4379FBB0)
#define QEIIES_POS2REV_Int_BIT 12
#define QEIIES_REV_Int_MASK 0x200
#define QEIIES_REV_Int 0x200
#define QEIIES_REV_Int_BITBAND_ADDRESS 0x4379FBA4
#define QEIIES_REV_Int_BITBAND (*(volatile unsigned *)0x4379FBA4)
#define QEIIES_REV_Int_BIT 9

#define QEIINTSTAT (*(volatile unsigned long *)0x400BCFE0)
#define QEIINTSTAT_OFFSET 0xFE0
#define QEIINTSTAT_INX_Int_MASK 0x1
#define QEIINTSTAT_INX_Int 0x1
#define QEIINTSTAT_INX_Int_BITBAND_ADDRESS 0x4379FC00
#define QEIINTSTAT_INX_Int_BITBAND (*(volatile unsigned *)0x4379FC00)
#define QEIINTSTAT_INX_Int_BIT 0
#define QEIINTSTAT_TIM_Int_MASK 0x2
#define QEIINTSTAT_TIM_Int 0x2
#define QEIINTSTAT_TIM_Int_BITBAND_ADDRESS 0x4379FC04
#define QEIINTSTAT_TIM_Int_BITBAND (*(volatile unsigned *)0x4379FC04)
#define QEIINTSTAT_TIM_Int_BIT 1
#define QEIINTSTAT_VELC_Int_MASK 0x4
#define QEIINTSTAT_VELC_Int 0x4
#define QEIINTSTAT_VELC_Int_BITBAND_ADDRESS 0x4379FC08
#define QEIINTSTAT_VELC_Int_BITBAND (*(volatile unsigned *)0x4379FC08)
#define QEIINTSTAT_VELC_Int_BIT 2
#define QEIINTSTAT_DIR_Int_MASK 0x8
#define QEIINTSTAT_DIR_Int 0x8
#define QEIINTSTAT_DIR_Int_BITBAND_ADDRESS 0x4379FC0C
#define QEIINTSTAT_DIR_Int_BITBAND (*(volatile unsigned *)0x4379FC0C)
#define QEIINTSTAT_DIR_Int_BIT 3
#define QEIINTSTAT_ERR_Int_MASK 0x10
#define QEIINTSTAT_ERR_Int 0x10
#define QEIINTSTAT_ERR_Int_BITBAND_ADDRESS 0x4379FC10
#define QEIINTSTAT_ERR_Int_BITBAND (*(volatile unsigned *)0x4379FC10)
#define QEIINTSTAT_ERR_Int_BIT 4
#define QEIINTSTAT_ENCLK_Int_MASK 0x20
#define QEIINTSTAT_ENCLK_Int 0x20
#define QEIINTSTAT_ENCLK_Int_BITBAND_ADDRESS 0x4379FC14
#define QEIINTSTAT_ENCLK_Int_BITBAND (*(volatile unsigned *)0x4379FC14)
#define QEIINTSTAT_ENCLK_Int_BIT 5
#define QEIINTSTAT_POS0_Int_MASK 0x40
#define QEIINTSTAT_POS0_Int 0x40
#define QEIINTSTAT_POS0_Int_BITBAND_ADDRESS 0x4379FC18
#define QEIINTSTAT_POS0_Int_BITBAND (*(volatile unsigned *)0x4379FC18)
#define QEIINTSTAT_POS0_Int_BIT 6
#define QEIINTSTAT_POS1_Int_MASK 0x80
#define QEIINTSTAT_POS1_Int 0x80
#define QEIINTSTAT_POS1_Int_BITBAND_ADDRESS 0x4379FC1C
#define QEIINTSTAT_POS1_Int_BITBAND (*(volatile unsigned *)0x4379FC1C)
#define QEIINTSTAT_POS1_Int_BIT 7
#define QEIINTSTAT_POS2_Int_MASK 0x100
#define QEIINTSTAT_POS2_Int 0x100
#define QEIINTSTAT_POS2_Int_BITBAND_ADDRESS 0x4379FC20
#define QEIINTSTAT_POS2_Int_BITBAND (*(volatile unsigned *)0x4379FC20)
#define QEIINTSTAT_POS2_Int_BIT 8
#define QEIINTSTAT_POS0REV_Int_MASK 0x400
#define QEIINTSTAT_POS0REV_Int 0x400
#define QEIINTSTAT_POS0REV_Int_BITBAND_ADDRESS 0x4379FC28
#define QEIINTSTAT_POS0REV_Int_BITBAND (*(volatile unsigned *)0x4379FC28)
#define QEIINTSTAT_POS0REV_Int_BIT 10
#define QEIINTSTAT_POS1REV_Int_MASK 0x800
#define QEIINTSTAT_POS1REV_Int 0x800
#define QEIINTSTAT_POS1REV_Int_BITBAND_ADDRESS 0x4379FC2C
#define QEIINTSTAT_POS1REV_Int_BITBAND (*(volatile unsigned *)0x4379FC2C)
#define QEIINTSTAT_POS1REV_Int_BIT 11
#define QEIINTSTAT_POS2REV_Int_MASK 0x1000
#define QEIINTSTAT_POS2REV_Int 0x1000
#define QEIINTSTAT_POS2REV_Int_BITBAND_ADDRESS 0x4379FC30
#define QEIINTSTAT_POS2REV_Int_BITBAND (*(volatile unsigned *)0x4379FC30)
#define QEIINTSTAT_POS2REV_Int_BIT 12
#define QEIINTSTAT_REV_Int_MASK 0x200
#define QEIINTSTAT_REV_Int 0x200
#define QEIINTSTAT_REV_Int_BITBAND_ADDRESS 0x4379FC24
#define QEIINTSTAT_REV_Int_BITBAND (*(volatile unsigned *)0x4379FC24)
#define QEIINTSTAT_REV_Int_BIT 9

#define QEIIE (*(volatile unsigned long *)0x400BCFE4)
#define QEIIE_OFFSET 0xFE4
#define QEIIE_INX_Int_MASK 0x1
#define QEIIE_INX_Int 0x1
#define QEIIE_INX_Int_BITBAND_ADDRESS 0x4379FC80
#define QEIIE_INX_Int_BITBAND (*(volatile unsigned *)0x4379FC80)
#define QEIIE_INX_Int_BIT 0
#define QEIIE_TIM_Int_MASK 0x2
#define QEIIE_TIM_Int 0x2
#define QEIIE_TIM_Int_BITBAND_ADDRESS 0x4379FC84
#define QEIIE_TIM_Int_BITBAND (*(volatile unsigned *)0x4379FC84)
#define QEIIE_TIM_Int_BIT 1
#define QEIIE_VELC_Int_MASK 0x4
#define QEIIE_VELC_Int 0x4
#define QEIIE_VELC_Int_BITBAND_ADDRESS 0x4379FC88
#define QEIIE_VELC_Int_BITBAND (*(volatile unsigned *)0x4379FC88)
#define QEIIE_VELC_Int_BIT 2
#define QEIIE_DIR_Int_MASK 0x8
#define QEIIE_DIR_Int 0x8
#define QEIIE_DIR_Int_BITBAND_ADDRESS 0x4379FC8C
#define QEIIE_DIR_Int_BITBAND (*(volatile unsigned *)0x4379FC8C)
#define QEIIE_DIR_Int_BIT 3
#define QEIIE_ERR_Int_MASK 0x10
#define QEIIE_ERR_Int 0x10
#define QEIIE_ERR_Int_BITBAND_ADDRESS 0x4379FC90
#define QEIIE_ERR_Int_BITBAND (*(volatile unsigned *)0x4379FC90)
#define QEIIE_ERR_Int_BIT 4
#define QEIIE_ENCLK_Int_MASK 0x20
#define QEIIE_ENCLK_Int 0x20
#define QEIIE_ENCLK_Int_BITBAND_ADDRESS 0x4379FC94
#define QEIIE_ENCLK_Int_BITBAND (*(volatile unsigned *)0x4379FC94)
#define QEIIE_ENCLK_Int_BIT 5
#define QEIIE_POS0_Int_MASK 0x40
#define QEIIE_POS0_Int 0x40
#define QEIIE_POS0_Int_BITBAND_ADDRESS 0x4379FC98
#define QEIIE_POS0_Int_BITBAND (*(volatile unsigned *)0x4379FC98)
#define QEIIE_POS0_Int_BIT 6
#define QEIIE_POS1_Int_MASK 0x80
#define QEIIE_POS1_Int 0x80
#define QEIIE_POS1_Int_BITBAND_ADDRESS 0x4379FC9C
#define QEIIE_POS1_Int_BITBAND (*(volatile unsigned *)0x4379FC9C)
#define QEIIE_POS1_Int_BIT 7
#define QEIIE_POS2_Int_MASK 0x100
#define QEIIE_POS2_Int 0x100
#define QEIIE_POS2_Int_BITBAND_ADDRESS 0x4379FCA0
#define QEIIE_POS2_Int_BITBAND (*(volatile unsigned *)0x4379FCA0)
#define QEIIE_POS2_Int_BIT 8
#define QEIIE_POS0REV_Int_MASK 0x400
#define QEIIE_POS0REV_Int 0x400
#define QEIIE_POS0REV_Int_BITBAND_ADDRESS 0x4379FCA8
#define QEIIE_POS0REV_Int_BITBAND (*(volatile unsigned *)0x4379FCA8)
#define QEIIE_POS0REV_Int_BIT 10
#define QEIIE_POS1REV_Int_MASK 0x800
#define QEIIE_POS1REV_Int 0x800
#define QEIIE_POS1REV_Int_BITBAND_ADDRESS 0x4379FCAC
#define QEIIE_POS1REV_Int_BITBAND (*(volatile unsigned *)0x4379FCAC)
#define QEIIE_POS1REV_Int_BIT 11
#define QEIIE_POS2REV_Int_MASK 0x1000
#define QEIIE_POS2REV_Int 0x1000
#define QEIIE_POS2REV_Int_BITBAND_ADDRESS 0x4379FCB0
#define QEIIE_POS2REV_Int_BITBAND (*(volatile unsigned *)0x4379FCB0)
#define QEIIE_POS2REV_Int_BIT 12
#define QEIIE_REV_Int_MASK 0x200
#define QEIIE_REV_Int 0x200
#define QEIIE_REV_Int_BITBAND_ADDRESS 0x4379FCA4
#define QEIIE_REV_Int_BITBAND (*(volatile unsigned *)0x4379FCA4)
#define QEIIE_REV_Int_BIT 9

#define QEICLR (*(volatile unsigned long *)0x400BCFE8)
#define QEICLR_OFFSET 0xFE8
#define QEICLR_INX_Int_MASK 0x1
#define QEICLR_INX_Int 0x1
#define QEICLR_INX_Int_BITBAND_ADDRESS 0x4379FD00
#define QEICLR_INX_Int_BITBAND (*(volatile unsigned *)0x4379FD00)
#define QEICLR_INX_Int_BIT 0
#define QEICLR_TIM_Int_MASK 0x2
#define QEICLR_TIM_Int 0x2
#define QEICLR_TIM_Int_BITBAND_ADDRESS 0x4379FD04
#define QEICLR_TIM_Int_BITBAND (*(volatile unsigned *)0x4379FD04)
#define QEICLR_TIM_Int_BIT 1
#define QEICLR_VELC_Int_MASK 0x4
#define QEICLR_VELC_Int 0x4
#define QEICLR_VELC_Int_BITBAND_ADDRESS 0x4379FD08
#define QEICLR_VELC_Int_BITBAND (*(volatile unsigned *)0x4379FD08)
#define QEICLR_VELC_Int_BIT 2
#define QEICLR_DIR_Int_MASK 0x8
#define QEICLR_DIR_Int 0x8
#define QEICLR_DIR_Int_BITBAND_ADDRESS 0x4379FD0C
#define QEICLR_DIR_Int_BITBAND (*(volatile unsigned *)0x4379FD0C)
#define QEICLR_DIR_Int_BIT 3
#define QEICLR_ERR_Int_MASK 0x10
#define QEICLR_ERR_Int 0x10
#define QEICLR_ERR_Int_BITBAND_ADDRESS 0x4379FD10
#define QEICLR_ERR_Int_BITBAND (*(volatile unsigned *)0x4379FD10)
#define QEICLR_ERR_Int_BIT 4
#define QEICLR_ENCLK_Int_MASK 0x20
#define QEICLR_ENCLK_Int 0x20
#define QEICLR_ENCLK_Int_BITBAND_ADDRESS 0x4379FD14
#define QEICLR_ENCLK_Int_BITBAND (*(volatile unsigned *)0x4379FD14)
#define QEICLR_ENCLK_Int_BIT 5
#define QEICLR_POS0_Int_MASK 0x40
#define QEICLR_POS0_Int 0x40
#define QEICLR_POS0_Int_BITBAND_ADDRESS 0x4379FD18
#define QEICLR_POS0_Int_BITBAND (*(volatile unsigned *)0x4379FD18)
#define QEICLR_POS0_Int_BIT 6
#define QEICLR_POS1_Int_MASK 0x80
#define QEICLR_POS1_Int 0x80
#define QEICLR_POS1_Int_BITBAND_ADDRESS 0x4379FD1C
#define QEICLR_POS1_Int_BITBAND (*(volatile unsigned *)0x4379FD1C)
#define QEICLR_POS1_Int_BIT 7
#define QEICLR_POS2_Int_MASK 0x100
#define QEICLR_POS2_Int 0x100
#define QEICLR_POS2_Int_BITBAND_ADDRESS 0x4379FD20
#define QEICLR_POS2_Int_BITBAND (*(volatile unsigned *)0x4379FD20)
#define QEICLR_POS2_Int_BIT 8
#define QEICLR_POS0REV_Int_MASK 0x400
#define QEICLR_POS0REV_Int 0x400
#define QEICLR_POS0REV_Int_BITBAND_ADDRESS 0x4379FD28
#define QEICLR_POS0REV_Int_BITBAND (*(volatile unsigned *)0x4379FD28)
#define QEICLR_POS0REV_Int_BIT 10
#define QEICLR_POS1REV_Int_MASK 0x800
#define QEICLR_POS1REV_Int 0x800
#define QEICLR_POS1REV_Int_BITBAND_ADDRESS 0x4379FD2C
#define QEICLR_POS1REV_Int_BITBAND (*(volatile unsigned *)0x4379FD2C)
#define QEICLR_POS1REV_Int_BIT 11
#define QEICLR_POS2REV_Int_MASK 0x1000
#define QEICLR_POS2REV_Int 0x1000
#define QEICLR_POS2REV_Int_BITBAND_ADDRESS 0x4379FD30
#define QEICLR_POS2REV_Int_BITBAND (*(volatile unsigned *)0x4379FD30)
#define QEICLR_POS2REV_Int_BIT 12
#define QEICLR_REV_Int_MASK 0x200
#define QEICLR_REV_Int 0x200
#define QEICLR_REV_Int_BITBAND_ADDRESS 0x4379FD24
#define QEICLR_REV_Int_BITBAND (*(volatile unsigned *)0x4379FD24)
#define QEICLR_REV_Int_BIT 9

#define QEISET (*(volatile unsigned long *)0x400BCFEC)
#define QEISET_OFFSET 0xFEC
#define QEISET_INX_Int_MASK 0x1
#define QEISET_INX_Int 0x1
#define QEISET_INX_Int_BITBAND_ADDRESS 0x4379FD80
#define QEISET_INX_Int_BITBAND (*(volatile unsigned *)0x4379FD80)
#define QEISET_INX_Int_BIT 0
#define QEISET_TIM_Int_MASK 0x2
#define QEISET_TIM_Int 0x2
#define QEISET_TIM_Int_BITBAND_ADDRESS 0x4379FD84
#define QEISET_TIM_Int_BITBAND (*(volatile unsigned *)0x4379FD84)
#define QEISET_TIM_Int_BIT 1
#define QEISET_VELC_Int_MASK 0x4
#define QEISET_VELC_Int 0x4
#define QEISET_VELC_Int_BITBAND_ADDRESS 0x4379FD88
#define QEISET_VELC_Int_BITBAND (*(volatile unsigned *)0x4379FD88)
#define QEISET_VELC_Int_BIT 2
#define QEISET_DIR_Int_MASK 0x8
#define QEISET_DIR_Int 0x8
#define QEISET_DIR_Int_BITBAND_ADDRESS 0x4379FD8C
#define QEISET_DIR_Int_BITBAND (*(volatile unsigned *)0x4379FD8C)
#define QEISET_DIR_Int_BIT 3
#define QEISET_ERR_Int_MASK 0x10
#define QEISET_ERR_Int 0x10
#define QEISET_ERR_Int_BITBAND_ADDRESS 0x4379FD90
#define QEISET_ERR_Int_BITBAND (*(volatile unsigned *)0x4379FD90)
#define QEISET_ERR_Int_BIT 4
#define QEISET_ENCLK_Int_MASK 0x20
#define QEISET_ENCLK_Int 0x20
#define QEISET_ENCLK_Int_BITBAND_ADDRESS 0x4379FD94
#define QEISET_ENCLK_Int_BITBAND (*(volatile unsigned *)0x4379FD94)
#define QEISET_ENCLK_Int_BIT 5
#define QEISET_POS0_Int_MASK 0x40
#define QEISET_POS0_Int 0x40
#define QEISET_POS0_Int_BITBAND_ADDRESS 0x4379FD98
#define QEISET_POS0_Int_BITBAND (*(volatile unsigned *)0x4379FD98)
#define QEISET_POS0_Int_BIT 6
#define QEISET_POS1_Int_MASK 0x80
#define QEISET_POS1_Int 0x80
#define QEISET_POS1_Int_BITBAND_ADDRESS 0x4379FD9C
#define QEISET_POS1_Int_BITBAND (*(volatile unsigned *)0x4379FD9C)
#define QEISET_POS1_Int_BIT 7
#define QEISET_POS2_Int_MASK 0x100
#define QEISET_POS2_Int 0x100
#define QEISET_POS2_Int_BITBAND_ADDRESS 0x4379FDA0
#define QEISET_POS2_Int_BITBAND (*(volatile unsigned *)0x4379FDA0)
#define QEISET_POS2_Int_BIT 8
#define QEISET_POS0REV_Int_MASK 0x400
#define QEISET_POS0REV_Int 0x400
#define QEISET_POS0REV_Int_BITBAND_ADDRESS 0x4379FDA8
#define QEISET_POS0REV_Int_BITBAND (*(volatile unsigned *)0x4379FDA8)
#define QEISET_POS0REV_Int_BIT 10
#define QEISET_POS1REV_Int_MASK 0x800
#define QEISET_POS1REV_Int 0x800
#define QEISET_POS1REV_Int_BITBAND_ADDRESS 0x4379FDAC
#define QEISET_POS1REV_Int_BITBAND (*(volatile unsigned *)0x4379FDAC)
#define QEISET_POS1REV_Int_BIT 11
#define QEISET_POS2REV_Int_MASK 0x1000
#define QEISET_POS2REV_Int 0x1000
#define QEISET_POS2REV_Int_BITBAND_ADDRESS 0x4379FDB0
#define QEISET_POS2REV_Int_BITBAND (*(volatile unsigned *)0x4379FDB0)
#define QEISET_POS2REV_Int_BIT 12
#define QEISET_REV_Int_MASK 0x200
#define QEISET_REV_Int 0x200
#define QEISET_REV_Int_BITBAND_ADDRESS 0x4379FDA4
#define QEISET_REV_Int_BITBAND (*(volatile unsigned *)0x4379FDA4)
#define QEISET_REV_Int_BIT 9

#define SC_BASE_ADDRESS 0x400FC000

#define FLASHCFG (*(volatile unsigned long *)0x400FC000)
#define FLASHCFG_OFFSET 0x0
#define FLASHCFG_FLASHTIM_MASK 0xF000
#define FLASHCFG_FLASHTIM_BIT 12

#define CLKSRCSEL (*(volatile unsigned long *)0x400FC10C)
#define CLKSRCSEL_OFFSET 0x10C
#define CLKSRCSEL_CLKSRC_MASK 0x3
#define CLKSRCSEL_CLKSRC_BIT 0

#define PLL0CON (*(volatile unsigned long *)0x400FC080)
#define PLL0CON_OFFSET 0x80
#define PLL0CON_PLLE0_MASK 0x1
#define PLL0CON_PLLE0 0x1
#define PLL0CON_PLLE0_BITBAND_ADDRESS 0x43F81000
#define PLL0CON_PLLE0_BITBAND (*(volatile unsigned *)0x43F81000)
#define PLL0CON_PLLE0_BIT 0
#define PLL0CON_PLLC0_MASK 0x2
#define PLL0CON_PLLC0 0x2
#define PLL0CON_PLLC0_BITBAND_ADDRESS 0x43F81004
#define PLL0CON_PLLC0_BITBAND (*(volatile unsigned *)0x43F81004)
#define PLL0CON_PLLC0_BIT 1

#define PLL0CFG (*(volatile unsigned long *)0x400FC084)
#define PLL0CFG_OFFSET 0x84
#define PLL0CFG_MSEL0_MASK 0x7FFF
#define PLL0CFG_MSEL0_BIT 0
#define PLL0CFG_NSEL0_MASK 0xFF0000
#define PLL0CFG_NSEL0_BIT 16

#define PLL0STAT (*(volatile unsigned long *)0x400FC088)
#define PLL0STAT_OFFSET 0x88
#define PLL0STAT_MSEL0_MASK 0x7FFF
#define PLL0STAT_MSEL0_BIT 0
#define PLL0STAT_NSEL0_MASK 0xFF0000
#define PLL0STAT_NSEL0_BIT 16
#define PLL0STAT_PLLE0_STAT_MASK 0x1000000
#define PLL0STAT_PLLE0_STAT 0x1000000
#define PLL0STAT_PLLE0_STAT_BITBAND_ADDRESS 0x43F81160
#define PLL0STAT_PLLE0_STAT_BITBAND (*(volatile unsigned *)0x43F81160)
#define PLL0STAT_PLLE0_STAT_BIT 24
#define PLL0STAT_PLLC0_STAT_MASK 0x2000000
#define PLL0STAT_PLLC0_STAT 0x2000000
#define PLL0STAT_PLLC0_STAT_BITBAND_ADDRESS 0x43F81164
#define PLL0STAT_PLLC0_STAT_BITBAND (*(volatile unsigned *)0x43F81164)
#define PLL0STAT_PLLC0_STAT_BIT 25
#define PLL0STAT_PLOCK0_MASK 0x4000000
#define PLL0STAT_PLOCK0 0x4000000
#define PLL0STAT_PLOCK0_BITBAND_ADDRESS 0x43F81168
#define PLL0STAT_PLOCK0_BITBAND (*(volatile unsigned *)0x43F81168)
#define PLL0STAT_PLOCK0_BIT 26

#define PLL0FEED (*(volatile unsigned long *)0x400FC08C)
#define PLL0FEED_OFFSET 0x8C

#define PLL1CON (*(volatile unsigned long *)0x400FC0A0)
#define PLL1CON_OFFSET 0xA0
#define PLL1CON_PLLE1_MASK 0x1
#define PLL1CON_PLLE1 0x1
#define PLL1CON_PLLE1_BITBAND_ADDRESS 0x43F81400
#define PLL1CON_PLLE1_BITBAND (*(volatile unsigned *)0x43F81400)
#define PLL1CON_PLLE1_BIT 0
#define PLL1CON_PLLC1_MASK 0x2
#define PLL1CON_PLLC1 0x2
#define PLL1CON_PLLC1_BITBAND_ADDRESS 0x43F81404
#define PLL1CON_PLLC1_BITBAND (*(volatile unsigned *)0x43F81404)
#define PLL1CON_PLLC1_BIT 1

#define PLL1CFG (*(volatile unsigned long *)0x400FC0A4)
#define PLL1CFG_OFFSET 0xA4
#define PLL1CFG_MSEL1_MASK 0x1F
#define PLL1CFG_MSEL1_BIT 0
#define PLL1CFG_PSEL1_MASK 0x60
#define PLL1CFG_PSEL1_BIT 5

#define PLL1STAT (*(volatile unsigned long *)0x400FC0A8)
#define PLL1STAT_OFFSET 0xA8
#define PLL1STAT_MSEL1_MASK 0x1F
#define PLL1STAT_MSEL1_BIT 0
#define PLL1STAT_PSEL1_MASK 0x60
#define PLL1STAT_PSEL1_BIT 5
#define PLL1STAT_PLLE1_STAT_MASK 0x100
#define PLL1STAT_PLLE1_STAT 0x100
#define PLL1STAT_PLLE1_STAT_BITBAND_ADDRESS 0x43F81520
#define PLL1STAT_PLLE1_STAT_BITBAND (*(volatile unsigned *)0x43F81520)
#define PLL1STAT_PLLE1_STAT_BIT 8
#define PLL1STAT_PLLC1_STAT_MASK 0x200
#define PLL1STAT_PLLC1_STAT 0x200
#define PLL1STAT_PLLC1_STAT_BITBAND_ADDRESS 0x43F81524
#define PLL1STAT_PLLC1_STAT_BITBAND (*(volatile unsigned *)0x43F81524)
#define PLL1STAT_PLLC1_STAT_BIT 9
#define PLL1STAT_PLOCK1_MASK 0x400
#define PLL1STAT_PLOCK1 0x400
#define PLL1STAT_PLOCK1_BITBAND_ADDRESS 0x43F81528
#define PLL1STAT_PLOCK1_BITBAND (*(volatile unsigned *)0x43F81528)
#define PLL1STAT_PLOCK1_BIT 10

#define PLL1FEED (*(volatile unsigned long *)0x400FC0AC)
#define PLL1FEED_OFFSET 0xAC

#define CCLKCFG (*(volatile unsigned long *)0x400FC104)
#define CCLKCFG_OFFSET 0x104
#define CCLKCFG_CCLKSEL_MASK 0xFF
#define CCLKCFG_CCLKSEL_BIT 0

#define USBCLKCFG (*(volatile unsigned long *)0x400FC108)
#define USBCLKCFG_OFFSET 0x108
#define USBCLKCFG_USBSEL_MASK 0xF
#define USBCLKCFG_USBSEL_BIT 0

#define IRCTRIM (*(volatile unsigned long *)0x400FC1A4)
#define IRCTRIM_OFFSET 0x1A4
#define IRCTRIM_IRCtrim_MASK 0xFF
#define IRCTRIM_IRCtrim_BIT 0

#define PCLKSEL0 (*(volatile unsigned long *)0x400FC1A8)
#define PCLKSEL0_OFFSET 0x1A8
#define PCLKSEL0_PCLK_WDT_MASK 0x3
#define PCLKSEL0_PCLK_WDT_BIT 0
#define PCLKSEL0_PCLK_TIMER0_MASK 0xC
#define PCLKSEL0_PCLK_TIMER0_BIT 2
#define PCLKSEL0_PCLK_TIMER1_MASK 0x30
#define PCLKSEL0_PCLK_TIMER1_BIT 4
#define PCLKSEL0_PCLK_UART0_MASK 0xC0
#define PCLKSEL0_PCLK_UART0_BIT 6
#define PCLKSEL0_PCLK_UART1_MASK 0x300
#define PCLKSEL0_PCLK_UART1_BIT 8
#define PCLKSEL0_PCLK_PWM1_MASK 0x3000
#define PCLKSEL0_PCLK_PWM1_BIT 12
#define PCLKSEL0_PCLK_I2C0_MASK 0xC000
#define PCLKSEL0_PCLK_I2C0_BIT 14
#define PCLKSEL0_PCLK_SPI_MASK 0x30000
#define PCLKSEL0_PCLK_SPI_BIT 16
#define PCLKSEL0_PCLK_SSP1_MASK 0x300000
#define PCLKSEL0_PCLK_SSP1_BIT 20
#define PCLKSEL0_PCLK_DAC_MASK 0xC00000
#define PCLKSEL0_PCLK_DAC_BIT 22
#define PCLKSEL0_PCLK_ADC_MASK 0x3000000
#define PCLKSEL0_PCLK_ADC_BIT 24
#define PCLKSEL0_PCLK_CAN1_MASK 0xC000000
#define PCLKSEL0_PCLK_CAN1_BIT 26
#define PCLKSEL0_PCLK_CAN2_MASK 0x30000000
#define PCLKSEL0_PCLK_CAN2_BIT 28
#define PCLKSEL0_PCLK_ACF_MASK 0xC0000000
#define PCLKSEL0_PCLK_ACF_BIT 30

#define PCLKSEL1 (*(volatile unsigned long *)0x400FC1AC)
#define PCLKSEL1_OFFSET 0x1AC
#define PCLKSEL1_PCLK_QEI_MASK 0x3
#define PCLKSEL1_PCLK_QEI_BIT 0
#define PCLKSEL1_PCLK_GPIOINT_MASK 0xC
#define PCLKSEL1_PCLK_GPIOINT_BIT 2
#define PCLKSEL1_PCLK_PCB_MASK 0x30
#define PCLKSEL1_PCLK_PCB_BIT 4
#define PCLKSEL1_PCLK_I2C1_MASK 0xC0
#define PCLKSEL1_PCLK_I2C1_BIT 6
#define PCLKSEL1_PCLK_SSP0_MASK 0xC00
#define PCLKSEL1_PCLK_SSP0_BIT 10
#define PCLKSEL1_PCLK_TIMER2_MASK 0x3000
#define PCLKSEL1_PCLK_TIMER2_BIT 12
#define PCLKSEL1_PCLK_TIMER3_MASK 0xC000
#define PCLKSEL1_PCLK_TIMER3_BIT 14
#define PCLKSEL1_PCLK_UART2_MASK 0x30000
#define PCLKSEL1_PCLK_UART2_BIT 16
#define PCLKSEL1_PCLK_UART3_MASK 0xC0000
#define PCLKSEL1_PCLK_UART3_BIT 18
#define PCLKSEL1_PCLK_I2C2_MASK 0x300000
#define PCLKSEL1_PCLK_I2C2_BIT 20
#define PCLKSEL1_PCLK_I2S_MASK 0xC00000
#define PCLKSEL1_PCLK_I2S_BIT 22
#define PCLKSEL1_PCLK_RIT_MASK 0xC000000
#define PCLKSEL1_PCLK_RIT_BIT 26
#define PCLKSEL1_PCLK_SYSCON_MASK 0x30000000
#define PCLKSEL1_PCLK_SYSCON_BIT 28
#define PCLKSEL1_PCLK_MC_MASK 0xC0000000
#define PCLKSEL1_PCLK_MC_BIT 30

#define PCON (*(volatile unsigned long *)0x400FC0C0)
#define PCON_OFFSET 0xC0
#define PCON_PM0_MASK 0x1
#define PCON_PM0 0x1
#define PCON_PM0_BITBAND_ADDRESS 0x43F81800
#define PCON_PM0_BITBAND (*(volatile unsigned *)0x43F81800)
#define PCON_PM0_BIT 0
#define PCON_PM1_MASK 0x2
#define PCON_PM1 0x2
#define PCON_PM1_BITBAND_ADDRESS 0x43F81804
#define PCON_PM1_BITBAND (*(volatile unsigned *)0x43F81804)
#define PCON_PM1_BIT 1
#define PCON_BODRPM_MASK 0x4
#define PCON_BODRPM 0x4
#define PCON_BODRPM_BITBAND_ADDRESS 0x43F81808
#define PCON_BODRPM_BITBAND (*(volatile unsigned *)0x43F81808)
#define PCON_BODRPM_BIT 2
#define PCON_BOGD_MASK 0x8
#define PCON_BOGD 0x8
#define PCON_BOGD_BITBAND_ADDRESS 0x43F8180C
#define PCON_BOGD_BITBAND (*(volatile unsigned *)0x43F8180C)
#define PCON_BOGD_BIT 3
#define PCON_BORD_MASK 0x10
#define PCON_BORD 0x10
#define PCON_BORD_BITBAND_ADDRESS 0x43F81810
#define PCON_BORD_BITBAND (*(volatile unsigned *)0x43F81810)
#define PCON_BORD_BIT 4
#define PCON_SMFLAG_MASK 0x100
#define PCON_SMFLAG 0x100
#define PCON_SMFLAG_BITBAND_ADDRESS 0x43F81820
#define PCON_SMFLAG_BITBAND (*(volatile unsigned *)0x43F81820)
#define PCON_SMFLAG_BIT 8
#define PCON_DSFLAG_MASK 0x200
#define PCON_DSFLAG 0x200
#define PCON_DSFLAG_BITBAND_ADDRESS 0x43F81824
#define PCON_DSFLAG_BITBAND (*(volatile unsigned *)0x43F81824)
#define PCON_DSFLAG_BIT 9
#define PCON_PDFLAG_MASK 0x400
#define PCON_PDFLAG 0x400
#define PCON_PDFLAG_BITBAND_ADDRESS 0x43F81828
#define PCON_PDFLAG_BITBAND (*(volatile unsigned *)0x43F81828)
#define PCON_PDFLAG_BIT 10
#define PCON_DPDFLAG_MASK 0x800
#define PCON_DPDFLAG 0x800
#define PCON_DPDFLAG_BITBAND_ADDRESS 0x43F8182C
#define PCON_DPDFLAG_BITBAND (*(volatile unsigned *)0x43F8182C)
#define PCON_DPDFLAG_BIT 11

#define PCONP (*(volatile unsigned long *)0x400FC0C4)
#define PCONP_OFFSET 0xC4
#define PCONP_PCTIM0_MASK 0x2
#define PCONP_PCTIM0 0x2
#define PCONP_PCTIM0_BITBAND_ADDRESS 0x43F81884
#define PCONP_PCTIM0_BITBAND (*(volatile unsigned *)0x43F81884)
#define PCONP_PCTIM0_BIT 1
#define PCONP_PCTIM1_MASK 0x4
#define PCONP_PCTIM1 0x4
#define PCONP_PCTIM1_BITBAND_ADDRESS 0x43F81888
#define PCONP_PCTIM1_BITBAND (*(volatile unsigned *)0x43F81888)
#define PCONP_PCTIM1_BIT 2
#define PCONP_PCUART0_MASK 0x8
#define PCONP_PCUART0 0x8
#define PCONP_PCUART0_BITBAND_ADDRESS 0x43F8188C
#define PCONP_PCUART0_BITBAND (*(volatile unsigned *)0x43F8188C)
#define PCONP_PCUART0_BIT 3
#define PCONP_PCUART1_MASK 0x10
#define PCONP_PCUART1 0x10
#define PCONP_PCUART1_BITBAND_ADDRESS 0x43F81890
#define PCONP_PCUART1_BITBAND (*(volatile unsigned *)0x43F81890)
#define PCONP_PCUART1_BIT 4
#define PCONP_PCPWM1_MASK 0x40
#define PCONP_PCPWM1 0x40
#define PCONP_PCPWM1_BITBAND_ADDRESS 0x43F81898
#define PCONP_PCPWM1_BITBAND (*(volatile unsigned *)0x43F81898)
#define PCONP_PCPWM1_BIT 6
#define PCONP_PCI2C0_MASK 0x80
#define PCONP_PCI2C0 0x80
#define PCONP_PCI2C0_BITBAND_ADDRESS 0x43F8189C
#define PCONP_PCI2C0_BITBAND (*(volatile unsigned *)0x43F8189C)
#define PCONP_PCI2C0_BIT 7
#define PCONP_PCSPI_MASK 0x100
#define PCONP_PCSPI 0x100
#define PCONP_PCSPI_BITBAND_ADDRESS 0x43F818A0
#define PCONP_PCSPI_BITBAND (*(volatile unsigned *)0x43F818A0)
#define PCONP_PCSPI_BIT 8
#define PCONP_PCRTC_MASK 0x200
#define PCONP_PCRTC 0x200
#define PCONP_PCRTC_BITBAND_ADDRESS 0x43F818A4
#define PCONP_PCRTC_BITBAND (*(volatile unsigned *)0x43F818A4)
#define PCONP_PCRTC_BIT 9
#define PCONP_PCSSP1_MASK 0x400
#define PCONP_PCSSP1 0x400
#define PCONP_PCSSP1_BITBAND_ADDRESS 0x43F818A8
#define PCONP_PCSSP1_BITBAND (*(volatile unsigned *)0x43F818A8)
#define PCONP_PCSSP1_BIT 10
#define PCONP_PCADC_MASK 0x1000
#define PCONP_PCADC 0x1000
#define PCONP_PCADC_BITBAND_ADDRESS 0x43F818B0
#define PCONP_PCADC_BITBAND (*(volatile unsigned *)0x43F818B0)
#define PCONP_PCADC_BIT 12
#define PCONP_PCCAN1_MASK 0x2000
#define PCONP_PCCAN1 0x2000
#define PCONP_PCCAN1_BITBAND_ADDRESS 0x43F818B4
#define PCONP_PCCAN1_BITBAND (*(volatile unsigned *)0x43F818B4)
#define PCONP_PCCAN1_BIT 13
#define PCONP_PCCAN2_MASK 0x4000
#define PCONP_PCCAN2 0x4000
#define PCONP_PCCAN2_BITBAND_ADDRESS 0x43F818B8
#define PCONP_PCCAN2_BITBAND (*(volatile unsigned *)0x43F818B8)
#define PCONP_PCCAN2_BIT 14
#define PCONP_PCRIT_MASK 0x10000
#define PCONP_PCRIT 0x10000
#define PCONP_PCRIT_BITBAND_ADDRESS 0x43F818C0
#define PCONP_PCRIT_BITBAND (*(volatile unsigned *)0x43F818C0)
#define PCONP_PCRIT_BIT 16
#define PCONP_PCMCPWM_MASK 0x20000
#define PCONP_PCMCPWM 0x20000
#define PCONP_PCMCPWM_BITBAND_ADDRESS 0x43F818C4
#define PCONP_PCMCPWM_BITBAND (*(volatile unsigned *)0x43F818C4)
#define PCONP_PCMCPWM_BIT 17
#define PCONP_PCQEI_MASK 0x40000
#define PCONP_PCQEI 0x40000
#define PCONP_PCQEI_BITBAND_ADDRESS 0x43F818C8
#define PCONP_PCQEI_BITBAND (*(volatile unsigned *)0x43F818C8)
#define PCONP_PCQEI_BIT 18
#define PCONP_PCI2C1_MASK 0x80000
#define PCONP_PCI2C1 0x80000
#define PCONP_PCI2C1_BITBAND_ADDRESS 0x43F818CC
#define PCONP_PCI2C1_BITBAND (*(volatile unsigned *)0x43F818CC)
#define PCONP_PCI2C1_BIT 19
#define PCONP_PCSSP0_MASK 0x200000
#define PCONP_PCSSP0 0x200000
#define PCONP_PCSSP0_BITBAND_ADDRESS 0x43F818D4
#define PCONP_PCSSP0_BITBAND (*(volatile unsigned *)0x43F818D4)
#define PCONP_PCSSP0_BIT 21
#define PCONP_PCTIM2_MASK 0x400000
#define PCONP_PCTIM2 0x400000
#define PCONP_PCTIM2_BITBAND_ADDRESS 0x43F818D8
#define PCONP_PCTIM2_BITBAND (*(volatile unsigned *)0x43F818D8)
#define PCONP_PCTIM2_BIT 22
#define PCONP_PCTIM3_MASK 0x800000
#define PCONP_PCTIM3 0x800000
#define PCONP_PCTIM3_BITBAND_ADDRESS 0x43F818DC
#define PCONP_PCTIM3_BITBAND (*(volatile unsigned *)0x43F818DC)
#define PCONP_PCTIM3_BIT 23
#define PCONP_PCUART2_MASK 0x1000000
#define PCONP_PCUART2 0x1000000
#define PCONP_PCUART2_BITBAND_ADDRESS 0x43F818E0
#define PCONP_PCUART2_BITBAND (*(volatile unsigned *)0x43F818E0)
#define PCONP_PCUART2_BIT 24
#define PCONP_PCUART3_MASK 0x2000000
#define PCONP_PCUART3 0x2000000
#define PCONP_PCUART3_BITBAND_ADDRESS 0x43F818E4
#define PCONP_PCUART3_BITBAND (*(volatile unsigned *)0x43F818E4)
#define PCONP_PCUART3_BIT 25
#define PCONP_PCI2C2_MASK 0x4000000
#define PCONP_PCI2C2 0x4000000
#define PCONP_PCI2C2_BITBAND_ADDRESS 0x43F818E8
#define PCONP_PCI2C2_BITBAND (*(volatile unsigned *)0x43F818E8)
#define PCONP_PCI2C2_BIT 26
#define PCONP_PCI2S_MASK 0x8000000
#define PCONP_PCI2S 0x8000000
#define PCONP_PCI2S_BITBAND_ADDRESS 0x43F818EC
#define PCONP_PCI2S_BITBAND (*(volatile unsigned *)0x43F818EC)
#define PCONP_PCI2S_BIT 27
#define PCONP_PCGPDMA_MASK 0x20000000
#define PCONP_PCGPDMA 0x20000000
#define PCONP_PCGPDMA_BITBAND_ADDRESS 0x43F818F4
#define PCONP_PCGPDMA_BITBAND (*(volatile unsigned *)0x43F818F4)
#define PCONP_PCGPDMA_BIT 29
#define PCONP_PCENET_MASK 0x40000000
#define PCONP_PCENET 0x40000000
#define PCONP_PCENET_BITBAND_ADDRESS 0x43F818F8
#define PCONP_PCENET_BITBAND (*(volatile unsigned *)0x43F818F8)
#define PCONP_PCENET_BIT 30
#define PCONP_PCUSB_MASK 0x80000000
#define PCONP_PCUSB 0x80000000
#define PCONP_PCUSB_BITBAND_ADDRESS 0x43F818FC
#define PCONP_PCUSB_BITBAND (*(volatile unsigned *)0x43F818FC)
#define PCONP_PCUSB_BIT 31

#define CLKOUTCFG (*(volatile unsigned long *)0x400FC1C8)
#define CLKOUTCFG_OFFSET 0x1C8
#define CLKOUTCFG_CLKOUTSEL_MASK 0xF
#define CLKOUTCFG_CLKOUTSEL_BIT 0
#define CLKOUTCFG_CLKOUTDIV_MASK 0xF0
#define CLKOUTCFG_CLKOUTDIV_BIT 4
#define CLKOUTCFG_CLKOUT_EN_MASK 0x100
#define CLKOUTCFG_CLKOUT_EN 0x100
#define CLKOUTCFG_CLKOUT_EN_BITBAND_ADDRESS 0x43F83920
#define CLKOUTCFG_CLKOUT_EN_BITBAND (*(volatile unsigned *)0x43F83920)
#define CLKOUTCFG_CLKOUT_EN_BIT 8
#define CLKOUTCFG_CLKOUT_ACT_MASK 0x200
#define CLKOUTCFG_CLKOUT_ACT 0x200
#define CLKOUTCFG_CLKOUT_ACT_BITBAND_ADDRESS 0x43F83924
#define CLKOUTCFG_CLKOUT_ACT_BITBAND (*(volatile unsigned *)0x43F83924)
#define CLKOUTCFG_CLKOUT_ACT_BIT 9

#define CANSLEEPCLR (*(volatile unsigned long *)0x400FC110)
#define CANSLEEPCLR_OFFSET 0x110
#define CANSLEEPCLR_CAN1SLEEP_MASK 0x2
#define CANSLEEPCLR_CAN1SLEEP 0x2
#define CANSLEEPCLR_CAN1SLEEP_BITBAND_ADDRESS 0x43F82204
#define CANSLEEPCLR_CAN1SLEEP_BITBAND (*(volatile unsigned *)0x43F82204)
#define CANSLEEPCLR_CAN1SLEEP_BIT 1
#define CANSLEEPCLR_CAN2SLEEP_MASK 0x4
#define CANSLEEPCLR_CAN2SLEEP 0x4
#define CANSLEEPCLR_CAN2SLEEP_BITBAND_ADDRESS 0x43F82208
#define CANSLEEPCLR_CAN2SLEEP_BITBAND (*(volatile unsigned *)0x43F82208)
#define CANSLEEPCLR_CAN2SLEEP_BIT 2

#define CANWAKEFLAGS (*(volatile unsigned long *)0x400FC114)
#define CANWAKEFLAGS_OFFSET 0x114
#define CANWAKEFLAGS_CAN1WAKE_MASK 0x2
#define CANWAKEFLAGS_CAN1WAKE 0x2
#define CANWAKEFLAGS_CAN1WAKE_BITBAND_ADDRESS 0x43F82284
#define CANWAKEFLAGS_CAN1WAKE_BITBAND (*(volatile unsigned *)0x43F82284)
#define CANWAKEFLAGS_CAN1WAKE_BIT 1
#define CANWAKEFLAGS_CAN2WAKE_MASK 0x4
#define CANWAKEFLAGS_CAN2WAKE 0x4
#define CANWAKEFLAGS_CAN2WAKE_BITBAND_ADDRESS 0x43F82288
#define CANWAKEFLAGS_CAN2WAKE_BITBAND (*(volatile unsigned *)0x43F82288)
#define CANWAKEFLAGS_CAN2WAKE_BIT 2

#define EXTINT (*(volatile unsigned char *)0x400FC140)
#define EXTINT_OFFSET 0x140
#define EXTINT_EINT0_MASK 0x1
#define EXTINT_EINT0 0x1
#define EXTINT_EINT0_BITBAND_ADDRESS 0x43F82800
#define EXTINT_EINT0_BITBAND (*(volatile unsigned *)0x43F82800)
#define EXTINT_EINT0_BIT 0
#define EXTINT_EINT1_MASK 0x2
#define EXTINT_EINT1 0x2
#define EXTINT_EINT1_BITBAND_ADDRESS 0x43F82804
#define EXTINT_EINT1_BITBAND (*(volatile unsigned *)0x43F82804)
#define EXTINT_EINT1_BIT 1
#define EXTINT_EINT2_MASK 0x4
#define EXTINT_EINT2 0x4
#define EXTINT_EINT2_BITBAND_ADDRESS 0x43F82808
#define EXTINT_EINT2_BITBAND (*(volatile unsigned *)0x43F82808)
#define EXTINT_EINT2_BIT 2
#define EXTINT_EINT3_MASK 0x8
#define EXTINT_EINT3 0x8
#define EXTINT_EINT3_BITBAND_ADDRESS 0x43F8280C
#define EXTINT_EINT3_BITBAND (*(volatile unsigned *)0x43F8280C)
#define EXTINT_EINT3_BIT 3

#define EXTMODE (*(volatile unsigned char *)0x400FC148)
#define EXTMODE_OFFSET 0x148
#define EXTMODE_EXTMODE0_MASK 0x1
#define EXTMODE_EXTMODE0 0x1
#define EXTMODE_EXTMODE0_BITBAND_ADDRESS 0x43F82900
#define EXTMODE_EXTMODE0_BITBAND (*(volatile unsigned *)0x43F82900)
#define EXTMODE_EXTMODE0_BIT 0
#define EXTMODE_EXTMODE1_MASK 0x2
#define EXTMODE_EXTMODE1 0x2
#define EXTMODE_EXTMODE1_BITBAND_ADDRESS 0x43F82904
#define EXTMODE_EXTMODE1_BITBAND (*(volatile unsigned *)0x43F82904)
#define EXTMODE_EXTMODE1_BIT 1
#define EXTMODE_EXTMODE2_MASK 0x4
#define EXTMODE_EXTMODE2 0x4
#define EXTMODE_EXTMODE2_BITBAND_ADDRESS 0x43F82908
#define EXTMODE_EXTMODE2_BITBAND (*(volatile unsigned *)0x43F82908)
#define EXTMODE_EXTMODE2_BIT 2
#define EXTMODE_EXTMODE3_MASK 0x8
#define EXTMODE_EXTMODE3 0x8
#define EXTMODE_EXTMODE3_BITBAND_ADDRESS 0x43F8290C
#define EXTMODE_EXTMODE3_BITBAND (*(volatile unsigned *)0x43F8290C)
#define EXTMODE_EXTMODE3_BIT 3

#define EXTPOLAR (*(volatile unsigned char *)0x400FC14C)
#define EXTPOLAR_OFFSET 0x14C
#define EXTPOLAR_EXTPOLAR0_MASK 0x1
#define EXTPOLAR_EXTPOLAR0 0x1
#define EXTPOLAR_EXTPOLAR0_BITBAND_ADDRESS 0x43F82980
#define EXTPOLAR_EXTPOLAR0_BITBAND (*(volatile unsigned *)0x43F82980)
#define EXTPOLAR_EXTPOLAR0_BIT 0
#define EXTPOLAR_EXTPOLAR1_MASK 0x2
#define EXTPOLAR_EXTPOLAR1 0x2
#define EXTPOLAR_EXTPOLAR1_BITBAND_ADDRESS 0x43F82984
#define EXTPOLAR_EXTPOLAR1_BITBAND (*(volatile unsigned *)0x43F82984)
#define EXTPOLAR_EXTPOLAR1_BIT 1
#define EXTPOLAR_EXTPOLAR2_MASK 0x4
#define EXTPOLAR_EXTPOLAR2 0x4
#define EXTPOLAR_EXTPOLAR2_BITBAND_ADDRESS 0x43F82988
#define EXTPOLAR_EXTPOLAR2_BITBAND (*(volatile unsigned *)0x43F82988)
#define EXTPOLAR_EXTPOLAR2_BIT 2
#define EXTPOLAR_EXTPOLAR3_MASK 0x8
#define EXTPOLAR_EXTPOLAR3 0x8
#define EXTPOLAR_EXTPOLAR3_BITBAND_ADDRESS 0x43F8298C
#define EXTPOLAR_EXTPOLAR3_BITBAND (*(volatile unsigned *)0x43F8298C)
#define EXTPOLAR_EXTPOLAR3_BIT 3

#define RSID (*(volatile unsigned char *)0x400FC180)
#define RSID_OFFSET 0x180
#define RSID_POR_MASK 0x1
#define RSID_POR 0x1
#define RSID_POR_BITBAND_ADDRESS 0x43F83000
#define RSID_POR_BITBAND (*(volatile unsigned *)0x43F83000)
#define RSID_POR_BIT 0
#define RSID_EXTR_MASK 0x2
#define RSID_EXTR 0x2
#define RSID_EXTR_BITBAND_ADDRESS 0x43F83004
#define RSID_EXTR_BITBAND (*(volatile unsigned *)0x43F83004)
#define RSID_EXTR_BIT 1
#define RSID_WDTR_MASK 0x4
#define RSID_WDTR 0x4
#define RSID_WDTR_BITBAND_ADDRESS 0x43F83008
#define RSID_WDTR_BITBAND (*(volatile unsigned *)0x43F83008)
#define RSID_WDTR_BIT 2
#define RSID_BODR_MASK 0x8
#define RSID_BODR 0x8
#define RSID_BODR_BITBAND_ADDRESS 0x43F8300C
#define RSID_BODR_BITBAND (*(volatile unsigned *)0x43F8300C)
#define RSID_BODR_BIT 3

#define SCS (*(volatile unsigned char *)0x400FC1A0)
#define SCS_OFFSET 0x1A0
#define SCS_OSCRANGE_MASK 0x10
#define SCS_OSCRANGE 0x10
#define SCS_OSCRANGE_BITBAND_ADDRESS 0x43F83410
#define SCS_OSCRANGE_BITBAND (*(volatile unsigned *)0x43F83410)
#define SCS_OSCRANGE_BIT 4
#define SCS_OSCEN_MASK 0x20
#define SCS_OSCEN 0x20
#define SCS_OSCEN_BITBAND_ADDRESS 0x43F83414
#define SCS_OSCEN_BITBAND (*(volatile unsigned *)0x43F83414)
#define SCS_OSCEN_BIT 5
#define SCS_OSCSTAT_MASK 0x40
#define SCS_OSCSTAT 0x40
#define SCS_OSCSTAT_BITBAND_ADDRESS 0x43F83418
#define SCS_OSCSTAT_BITBAND (*(volatile unsigned *)0x43F83418)
#define SCS_OSCSTAT_BIT 6

#define USBIntSt (*(volatile unsigned long *)0x400FC1C0)
#define USBIntSt_OFFSET 0x1C0
#define USBIntSt_USB_INT_REQ_LP_MASK 0x1
#define USBIntSt_USB_INT_REQ_LP 0x1
#define USBIntSt_USB_INT_REQ_LP_BITBAND_ADDRESS 0x43F83800
#define USBIntSt_USB_INT_REQ_LP_BITBAND (*(volatile unsigned *)0x43F83800)
#define USBIntSt_USB_INT_REQ_LP_BIT 0
#define USBIntSt_USB_INT_REQ_HP_MASK 0x2
#define USBIntSt_USB_INT_REQ_HP 0x2
#define USBIntSt_USB_INT_REQ_HP_BITBAND_ADDRESS 0x43F83804
#define USBIntSt_USB_INT_REQ_HP_BITBAND (*(volatile unsigned *)0x43F83804)
#define USBIntSt_USB_INT_REQ_HP_BIT 1
#define USBIntSt_USB_INT_REQ_DMA_MASK 0x4
#define USBIntSt_USB_INT_REQ_DMA 0x4
#define USBIntSt_USB_INT_REQ_DMA_BITBAND_ADDRESS 0x43F83808
#define USBIntSt_USB_INT_REQ_DMA_BITBAND (*(volatile unsigned *)0x43F83808)
#define USBIntSt_USB_INT_REQ_DMA_BIT 2
#define USBIntSt_USB_NEED_CLK_MASK 0x100
#define USBIntSt_USB_NEED_CLK 0x100
#define USBIntSt_USB_NEED_CLK_BITBAND_ADDRESS 0x43F83820
#define USBIntSt_USB_NEED_CLK_BITBAND (*(volatile unsigned *)0x43F83820)
#define USBIntSt_USB_NEED_CLK_BIT 8
#define USBIntSt_EN_USB_INTS_MASK 0x80000000
#define USBIntSt_EN_USB_INTS 0x80000000
#define USBIntSt_EN_USB_INTS_BITBAND_ADDRESS 0x43F8387C
#define USBIntSt_EN_USB_INTS_BITBAND (*(volatile unsigned *)0x43F8387C)
#define USBIntSt_EN_USB_INTS_BIT 31

#define GPDMA_BASE_ADDRESS 0x50004000

#define DMACIntStat (*(volatile unsigned *)0x50004000)
#define DMACIntStat_OFFSET 0x0
#define DMACIntStat_IntStat_MASK 0xFF
#define DMACIntStat_IntStat_BIT 0

#define DMACIntTCStat (*(volatile unsigned *)0x50004004)
#define DMACIntTCStat_OFFSET 0x4
#define DMACIntTCStat_IntTCStat_MASK 0xFF
#define DMACIntTCStat_IntTCStat_BIT 0

#define DMACIntTCClear (*(volatile unsigned *)0x50004008)
#define DMACIntTCClear_OFFSET 0x8
#define DMACIntTCClear_IntTCClear_MASK 0xFF
#define DMACIntTCClear_IntTCClear_BIT 0

#define DMACIntErrStat (*(volatile unsigned *)0x5000400C)
#define DMACIntErrStat_OFFSET 0xC
#define DMACIntErrStat_IntErrStat_MASK 0xFF
#define DMACIntErrStat_IntErrStat_BIT 0

#define DMACIntErrClr (*(volatile unsigned *)0x50004010)
#define DMACIntErrClr_OFFSET 0x10
#define DMACIntErrClr_IntErrClr_MASK 0xFF
#define DMACIntErrClr_IntErrClr_BIT 0

#define DMACRawIntTCStat (*(volatile unsigned *)0x50004014)
#define DMACRawIntTCStat_OFFSET 0x14
#define DMACRawIntTCStat_RawIntTCStat_MASK 0xFF
#define DMACRawIntTCStat_RawIntTCStat_BIT 0

#define DMACRawIntErrStat (*(volatile unsigned *)0x50004018)
#define DMACRawIntErrStat_OFFSET 0x18
#define DMACRawIntErrStat_RawIntErrStat_MASK 0xFF
#define DMACRawIntErrStat_RawIntErrStat_BIT 0

#define DMACEnbldChns (*(volatile unsigned *)0x5000401C)
#define DMACEnbldChns_OFFSET 0x1C
#define DMACEnbldChns_EnabledChannels_MASK 0xFF
#define DMACEnbldChns_EnabledChannels_BIT 0

#define DMACSoftBReq (*(volatile unsigned *)0x50004020)
#define DMACSoftBReq_OFFSET 0x20
#define DMACSoftBReq_SoftBReq_MASK 0xFFFF
#define DMACSoftBReq_SoftBReq_BIT 0

#define DMACSoftSReq (*(volatile unsigned *)0x50004024)
#define DMACSoftSReq_OFFSET 0x24
#define DMACSoftSReq_SoftSReq_MASK 0xFFFF
#define DMACSoftSReq_SoftSReq_BIT 0

#define DMACSoftLBReq (*(volatile unsigned *)0x50004028)
#define DMACSoftLBReq_OFFSET 0x28
#define DMACSoftLBReq_SoftLBReq_MASK 0xFFFF
#define DMACSoftLBReq_SoftLBReq_BIT 0

#define DMACSoftLSReq (*(volatile unsigned *)0x5000402C)
#define DMACSoftLSReq_OFFSET 0x2C
#define DMACSoftLSReq_SoftLSReq_MASK 0xFFFF
#define DMACSoftLSReq_SoftLSReq_BIT 0

#define DMACConfig (*(volatile unsigned *)0x50004030)
#define DMACConfig_OFFSET 0x30
#define DMACConfig_E_MASK 0x1
#define DMACConfig_E 0x1
#define DMACConfig_E_BIT 0
#define DMACConfig_M_MASK 0x2
#define DMACConfig_M 0x2
#define DMACConfig_M_BIT 1

#define DMACSync (*(volatile unsigned *)0x50004034)
#define DMACSync_OFFSET 0x34
#define DMACSync_DMACSync_MASK 0xFFFF
#define DMACSync_DMACSync_BIT 0

#define DMACC0SrcAddr (*(volatile unsigned *)0x50004100)
#define DMACC0SrcAddr_OFFSET 0x100

#define DMACC1SrcAddr (*(volatile unsigned *)0x50004120)
#define DMACC1SrcAddr_OFFSET 0x120

#define DMACC2SrcAddr (*(volatile unsigned *)0x50004140)
#define DMACC2SrcAddr_OFFSET 0x140

#define DMACC3SrcAddr (*(volatile unsigned *)0x50004160)
#define DMACC3SrcAddr_OFFSET 0x160

#define DMACC4SrcAddr (*(volatile unsigned *)0x50004180)
#define DMACC4SrcAddr_OFFSET 0x180

#define DMACC5SrcAddr (*(volatile unsigned *)0x500041A0)
#define DMACC5SrcAddr_OFFSET 0x1A0

#define DMACC6SrcAddr (*(volatile unsigned *)0x500041C0)
#define DMACC6SrcAddr_OFFSET 0x1C0

#define DMACC7SrcAddr (*(volatile unsigned *)0x500041E0)
#define DMACC7SrcAddr_OFFSET 0x1E0

#define DMACC0DestAddr (*(volatile unsigned *)0x50004104)
#define DMACC0DestAddr_OFFSET 0x104

#define DMACC1DestAddr (*(volatile unsigned *)0x50004124)
#define DMACC1DestAddr_OFFSET 0x124

#define DMACC2DestAddr (*(volatile unsigned *)0x50004144)
#define DMACC2DestAddr_OFFSET 0x144

#define DMACC3DestAddr (*(volatile unsigned *)0x50004164)
#define DMACC3DestAddr_OFFSET 0x164

#define DMACC4DestAddr (*(volatile unsigned *)0x50004184)
#define DMACC4DestAddr_OFFSET 0x184

#define DMACC5DestAddr (*(volatile unsigned *)0x500041A4)
#define DMACC5DestAddr_OFFSET 0x1A4

#define DMACC6DestAddr (*(volatile unsigned *)0x500041C4)
#define DMACC6DestAddr_OFFSET 0x1C4

#define DMACC7DestAddr (*(volatile unsigned *)0x500041E4)
#define DMACC7DestAddr_OFFSET 0x1E4

#define DMACC0LLI (*(volatile unsigned *)0x50004108)
#define DMACC0LLI_OFFSET 0x108
#define DMACC0LLI_LLI_MASK 0xFFFFFFFC
#define DMACC0LLI_LLI_BIT 2

#define DMACC1LLI (*(volatile unsigned *)0x50004128)
#define DMACC1LLI_OFFSET 0x128
#define DMACC1LLI_LLI_MASK 0xFFFFFFFC
#define DMACC1LLI_LLI_BIT 2

#define DMACC2LLI (*(volatile unsigned *)0x50004148)
#define DMACC2LLI_OFFSET 0x148
#define DMACC2LLI_LLI_MASK 0xFFFFFFFC
#define DMACC2LLI_LLI_BIT 2

#define DMACC3LLI (*(volatile unsigned *)0x50004168)
#define DMACC3LLI_OFFSET 0x168
#define DMACC3LLI_LLI_MASK 0xFFFFFFFC
#define DMACC3LLI_LLI_BIT 2

#define DMACC4LLI (*(volatile unsigned *)0x50004188)
#define DMACC4LLI_OFFSET 0x188
#define DMACC4LLI_LLI_MASK 0xFFFFFFFC
#define DMACC4LLI_LLI_BIT 2

#define DMACC5LLI (*(volatile unsigned *)0x500041A8)
#define DMACC5LLI_OFFSET 0x1A8
#define DMACC5LLI_LLI_MASK 0xFFFFFFFC
#define DMACC5LLI_LLI_BIT 2

#define DMACC6LLI (*(volatile unsigned *)0x500041C8)
#define DMACC6LLI_OFFSET 0x1C8
#define DMACC6LLI_LLI_MASK 0xFFFFFFFC
#define DMACC6LLI_LLI_BIT 2

#define DMACC7LLI (*(volatile unsigned *)0x500041E8)
#define DMACC7LLI_OFFSET 0x1E8
#define DMACC7LLI_LLI_MASK 0xFFFFFFFC
#define DMACC7LLI_LLI_BIT 2

#define DMACC0Control (*(volatile unsigned *)0x5000410C)
#define DMACC0Control_OFFSET 0x10C
#define DMACC0Control_TransferSize_MASK 0xFFF
#define DMACC0Control_TransferSize_BIT 0
#define DMACC0Control_SBSize_MASK 0x7000
#define DMACC0Control_SBSize_BIT 12
#define DMACC0Control_DBSize_MASK 0x38000
#define DMACC0Control_DBSize_BIT 15
#define DMACC0Control_SWidth_MASK 0x1C0000
#define DMACC0Control_SWidth_BIT 18
#define DMACC0Control_DWidth_MASK 0xE00000
#define DMACC0Control_DWidth_BIT 21
#define DMACC0Control_SI_MASK 0x4000000
#define DMACC0Control_SI 0x4000000
#define DMACC0Control_SI_BIT 26
#define DMACC0Control_DI_MASK 0x8000000
#define DMACC0Control_DI 0x8000000
#define DMACC0Control_DI_BIT 27
#define DMACC0Control_Prot1_MASK 0x10000000
#define DMACC0Control_Prot1 0x10000000
#define DMACC0Control_Prot1_BIT 28
#define DMACC0Control_Prot2_MASK 0x20000000
#define DMACC0Control_Prot2 0x20000000
#define DMACC0Control_Prot2_BIT 29
#define DMACC0Control_Prot3_MASK 0x40000000
#define DMACC0Control_Prot3 0x40000000
#define DMACC0Control_Prot3_BIT 30
#define DMACC0Control_I_MASK 0x80000000
#define DMACC0Control_I 0x80000000
#define DMACC0Control_I_BIT 31

#define DMACC1Control (*(volatile unsigned *)0x5000412C)
#define DMACC1Control_OFFSET 0x12C
#define DMACC1Control_TransferSize_MASK 0xFFF
#define DMACC1Control_TransferSize_BIT 0
#define DMACC1Control_SBSize_MASK 0x7000
#define DMACC1Control_SBSize_BIT 12
#define DMACC1Control_DBSize_MASK 0x38000
#define DMACC1Control_DBSize_BIT 15
#define DMACC1Control_SWidth_MASK 0x1C0000
#define DMACC1Control_SWidth_BIT 18
#define DMACC1Control_DWidth_MASK 0xE00000
#define DMACC1Control_DWidth_BIT 21
#define DMACC1Control_SI_MASK 0x4000000
#define DMACC1Control_SI 0x4000000
#define DMACC1Control_SI_BIT 26
#define DMACC1Control_DI_MASK 0x8000000
#define DMACC1Control_DI 0x8000000
#define DMACC1Control_DI_BIT 27
#define DMACC1Control_Prot1_MASK 0x10000000
#define DMACC1Control_Prot1 0x10000000
#define DMACC1Control_Prot1_BIT 28
#define DMACC1Control_Prot2_MASK 0x20000000
#define DMACC1Control_Prot2 0x20000000
#define DMACC1Control_Prot2_BIT 29
#define DMACC1Control_Prot3_MASK 0x40000000
#define DMACC1Control_Prot3 0x40000000
#define DMACC1Control_Prot3_BIT 30
#define DMACC1Control_I_MASK 0x80000000
#define DMACC1Control_I 0x80000000
#define DMACC1Control_I_BIT 31

#define DMACC2Control (*(volatile unsigned *)0x5000414C)
#define DMACC2Control_OFFSET 0x14C
#define DMACC2Control_TransferSize_MASK 0xFFF
#define DMACC2Control_TransferSize_BIT 0
#define DMACC2Control_SBSize_MASK 0x7000
#define DMACC2Control_SBSize_BIT 12
#define DMACC2Control_DBSize_MASK 0x38000
#define DMACC2Control_DBSize_BIT 15
#define DMACC2Control_SWidth_MASK 0x1C0000
#define DMACC2Control_SWidth_BIT 18
#define DMACC2Control_DWidth_MASK 0xE00000
#define DMACC2Control_DWidth_BIT 21
#define DMACC2Control_SI_MASK 0x4000000
#define DMACC2Control_SI 0x4000000
#define DMACC2Control_SI_BIT 26
#define DMACC2Control_DI_MASK 0x8000000
#define DMACC2Control_DI 0x8000000
#define DMACC2Control_DI_BIT 27
#define DMACC2Control_Prot1_MASK 0x10000000
#define DMACC2Control_Prot1 0x10000000
#define DMACC2Control_Prot1_BIT 28
#define DMACC2Control_Prot2_MASK 0x20000000
#define DMACC2Control_Prot2 0x20000000
#define DMACC2Control_Prot2_BIT 29
#define DMACC2Control_Prot3_MASK 0x40000000
#define DMACC2Control_Prot3 0x40000000
#define DMACC2Control_Prot3_BIT 30
#define DMACC2Control_I_MASK 0x80000000
#define DMACC2Control_I 0x80000000
#define DMACC2Control_I_BIT 31

#define DMACC3Control (*(volatile unsigned *)0x5000416C)
#define DMACC3Control_OFFSET 0x16C
#define DMACC3Control_TransferSize_MASK 0xFFF
#define DMACC3Control_TransferSize_BIT 0
#define DMACC3Control_SBSize_MASK 0x7000
#define DMACC3Control_SBSize_BIT 12
#define DMACC3Control_DBSize_MASK 0x38000
#define DMACC3Control_DBSize_BIT 15
#define DMACC3Control_SWidth_MASK 0x1C0000
#define DMACC3Control_SWidth_BIT 18
#define DMACC3Control_DWidth_MASK 0xE00000
#define DMACC3Control_DWidth_BIT 21
#define DMACC3Control_SI_MASK 0x4000000
#define DMACC3Control_SI 0x4000000
#define DMACC3Control_SI_BIT 26
#define DMACC3Control_DI_MASK 0x8000000
#define DMACC3Control_DI 0x8000000
#define DMACC3Control_DI_BIT 27
#define DMACC3Control_Prot1_MASK 0x10000000
#define DMACC3Control_Prot1 0x10000000
#define DMACC3Control_Prot1_BIT 28
#define DMACC3Control_Prot2_MASK 0x20000000
#define DMACC3Control_Prot2 0x20000000
#define DMACC3Control_Prot2_BIT 29
#define DMACC3Control_Prot3_MASK 0x40000000
#define DMACC3Control_Prot3 0x40000000
#define DMACC3Control_Prot3_BIT 30
#define DMACC3Control_I_MASK 0x80000000
#define DMACC3Control_I 0x80000000
#define DMACC3Control_I_BIT 31

#define DMACC4Control (*(volatile unsigned *)0x5000418C)
#define DMACC4Control_OFFSET 0x18C
#define DMACC4Control_TransferSize_MASK 0xFFF
#define DMACC4Control_TransferSize_BIT 0
#define DMACC4Control_SBSize_MASK 0x7000
#define DMACC4Control_SBSize_BIT 12
#define DMACC4Control_DBSize_MASK 0x38000
#define DMACC4Control_DBSize_BIT 15
#define DMACC4Control_SWidth_MASK 0x1C0000
#define DMACC4Control_SWidth_BIT 18
#define DMACC4Control_DWidth_MASK 0xE00000
#define DMACC4Control_DWidth_BIT 21
#define DMACC4Control_SI_MASK 0x4000000
#define DMACC4Control_SI 0x4000000
#define DMACC4Control_SI_BIT 26
#define DMACC4Control_DI_MASK 0x8000000
#define DMACC4Control_DI 0x8000000
#define DMACC4Control_DI_BIT 27
#define DMACC4Control_Prot1_MASK 0x10000000
#define DMACC4Control_Prot1 0x10000000
#define DMACC4Control_Prot1_BIT 28
#define DMACC4Control_Prot2_MASK 0x20000000
#define DMACC4Control_Prot2 0x20000000
#define DMACC4Control_Prot2_BIT 29
#define DMACC4Control_Prot3_MASK 0x40000000
#define DMACC4Control_Prot3 0x40000000
#define DMACC4Control_Prot3_BIT 30
#define DMACC4Control_I_MASK 0x80000000
#define DMACC4Control_I 0x80000000
#define DMACC4Control_I_BIT 31

#define DMACC5Control (*(volatile unsigned *)0x500041AC)
#define DMACC5Control_OFFSET 0x1AC
#define DMACC5Control_TransferSize_MASK 0xFFF
#define DMACC5Control_TransferSize_BIT 0
#define DMACC5Control_SBSize_MASK 0x7000
#define DMACC5Control_SBSize_BIT 12
#define DMACC5Control_DBSize_MASK 0x38000
#define DMACC5Control_DBSize_BIT 15
#define DMACC5Control_SWidth_MASK 0x1C0000
#define DMACC5Control_SWidth_BIT 18
#define DMACC5Control_DWidth_MASK 0xE00000
#define DMACC5Control_DWidth_BIT 21
#define DMACC5Control_SI_MASK 0x4000000
#define DMACC5Control_SI 0x4000000
#define DMACC5Control_SI_BIT 26
#define DMACC5Control_DI_MASK 0x8000000
#define DMACC5Control_DI 0x8000000
#define DMACC5Control_DI_BIT 27
#define DMACC5Control_Prot1_MASK 0x10000000
#define DMACC5Control_Prot1 0x10000000
#define DMACC5Control_Prot1_BIT 28
#define DMACC5Control_Prot2_MASK 0x20000000
#define DMACC5Control_Prot2 0x20000000
#define DMACC5Control_Prot2_BIT 29
#define DMACC5Control_Prot3_MASK 0x40000000
#define DMACC5Control_Prot3 0x40000000
#define DMACC5Control_Prot3_BIT 30
#define DMACC5Control_I_MASK 0x80000000
#define DMACC5Control_I 0x80000000
#define DMACC5Control_I_BIT 31

#define DMACC6Control (*(volatile unsigned *)0x500041CC)
#define DMACC6Control_OFFSET 0x1CC
#define DMACC6Control_TransferSize_MASK 0xFFF
#define DMACC6Control_TransferSize_BIT 0
#define DMACC6Control_SBSize_MASK 0x7000
#define DMACC6Control_SBSize_BIT 12
#define DMACC6Control_DBSize_MASK 0x38000
#define DMACC6Control_DBSize_BIT 15
#define DMACC6Control_SWidth_MASK 0x1C0000
#define DMACC6Control_SWidth_BIT 18
#define DMACC6Control_DWidth_MASK 0xE00000
#define DMACC6Control_DWidth_BIT 21
#define DMACC6Control_SI_MASK 0x4000000
#define DMACC6Control_SI 0x4000000
#define DMACC6Control_SI_BIT 26
#define DMACC6Control_DI_MASK 0x8000000
#define DMACC6Control_DI 0x8000000
#define DMACC6Control_DI_BIT 27
#define DMACC6Control_Prot1_MASK 0x10000000
#define DMACC6Control_Prot1 0x10000000
#define DMACC6Control_Prot1_BIT 28
#define DMACC6Control_Prot2_MASK 0x20000000
#define DMACC6Control_Prot2 0x20000000
#define DMACC6Control_Prot2_BIT 29
#define DMACC6Control_Prot3_MASK 0x40000000
#define DMACC6Control_Prot3 0x40000000
#define DMACC6Control_Prot3_BIT 30
#define DMACC6Control_I_MASK 0x80000000
#define DMACC6Control_I 0x80000000
#define DMACC6Control_I_BIT 31

#define DMACC7Control (*(volatile unsigned *)0x500041EC)
#define DMACC7Control_OFFSET 0x1EC
#define DMACC7Control_TransferSize_MASK 0xFFF
#define DMACC7Control_TransferSize_BIT 0
#define DMACC7Control_SBSize_MASK 0x7000
#define DMACC7Control_SBSize_BIT 12
#define DMACC7Control_DBSize_MASK 0x38000
#define DMACC7Control_DBSize_BIT 15
#define DMACC7Control_SWidth_MASK 0x1C0000
#define DMACC7Control_SWidth_BIT 18
#define DMACC7Control_DWidth_MASK 0xE00000
#define DMACC7Control_DWidth_BIT 21
#define DMACC7Control_SI_MASK 0x4000000
#define DMACC7Control_SI 0x4000000
#define DMACC7Control_SI_BIT 26
#define DMACC7Control_DI_MASK 0x8000000
#define DMACC7Control_DI 0x8000000
#define DMACC7Control_DI_BIT 27
#define DMACC7Control_Prot1_MASK 0x10000000
#define DMACC7Control_Prot1 0x10000000
#define DMACC7Control_Prot1_BIT 28
#define DMACC7Control_Prot2_MASK 0x20000000
#define DMACC7Control_Prot2 0x20000000
#define DMACC7Control_Prot2_BIT 29
#define DMACC7Control_Prot3_MASK 0x40000000
#define DMACC7Control_Prot3 0x40000000
#define DMACC7Control_Prot3_BIT 30
#define DMACC7Control_I_MASK 0x80000000
#define DMACC7Control_I 0x80000000
#define DMACC7Control_I_BIT 31

#define DMACC0Config (*(volatile unsigned *)0x50004110)
#define DMACC0Config_OFFSET 0x110
#define DMACC0Config_E_MASK 0x1
#define DMACC0Config_E 0x1
#define DMACC0Config_E_BIT 0
#define DMACC0Config_SrcPeripheral_MASK 0x3E
#define DMACC0Config_SrcPeripheral_BIT 1
#define DMACC0Config_DestPeripheral_MASK 0x7C0
#define DMACC0Config_DestPeripheral_BIT 6
#define DMACC0Config_TransferType_MASK 0x3800
#define DMACC0Config_TransferType_BIT 11
#define DMACC0Config_IE_MASK 0x4000
#define DMACC0Config_IE 0x4000
#define DMACC0Config_IE_BIT 14
#define DMACC0Config_ITC_MASK 0x8000
#define DMACC0Config_ITC 0x8000
#define DMACC0Config_ITC_BIT 15
#define DMACC0Config_L_MASK 0x10000
#define DMACC0Config_L 0x10000
#define DMACC0Config_L_BIT 16
#define DMACC0Config_A_MASK 0x20000
#define DMACC0Config_A 0x20000
#define DMACC0Config_A_BIT 17
#define DMACC0Config_H_MASK 0x40000
#define DMACC0Config_H 0x40000
#define DMACC0Config_H_BIT 18

#define DMACC1Config (*(volatile unsigned *)0x50004130)
#define DMACC1Config_OFFSET 0x130
#define DMACC1Config_E_MASK 0x1
#define DMACC1Config_E 0x1
#define DMACC1Config_E_BIT 0
#define DMACC1Config_SrcPeripheral_MASK 0x3E
#define DMACC1Config_SrcPeripheral_BIT 1
#define DMACC1Config_DestPeripheral_MASK 0x7C0
#define DMACC1Config_DestPeripheral_BIT 6
#define DMACC1Config_TransferType_MASK 0x3800
#define DMACC1Config_TransferType_BIT 11
#define DMACC1Config_IE_MASK 0x4000
#define DMACC1Config_IE 0x4000
#define DMACC1Config_IE_BIT 14
#define DMACC1Config_ITC_MASK 0x8000
#define DMACC1Config_ITC 0x8000
#define DMACC1Config_ITC_BIT 15
#define DMACC1Config_L_MASK 0x10000
#define DMACC1Config_L 0x10000
#define DMACC1Config_L_BIT 16
#define DMACC1Config_A_MASK 0x20000
#define DMACC1Config_A 0x20000
#define DMACC1Config_A_BIT 17
#define DMACC1Config_H_MASK 0x40000
#define DMACC1Config_H 0x40000
#define DMACC1Config_H_BIT 18

#define DMACC2Config (*(volatile unsigned *)0x50004150)
#define DMACC2Config_OFFSET 0x150
#define DMACC2Config_E_MASK 0x1
#define DMACC2Config_E 0x1
#define DMACC2Config_E_BIT 0
#define DMACC2Config_SrcPeripheral_MASK 0x3E
#define DMACC2Config_SrcPeripheral_BIT 1
#define DMACC2Config_DestPeripheral_MASK 0x7C0
#define DMACC2Config_DestPeripheral_BIT 6
#define DMACC2Config_TransferType_MASK 0x3800
#define DMACC2Config_TransferType_BIT 11
#define DMACC2Config_IE_MASK 0x4000
#define DMACC2Config_IE 0x4000
#define DMACC2Config_IE_BIT 14
#define DMACC2Config_ITC_MASK 0x8000
#define DMACC2Config_ITC 0x8000
#define DMACC2Config_ITC_BIT 15
#define DMACC2Config_L_MASK 0x10000
#define DMACC2Config_L 0x10000
#define DMACC2Config_L_BIT 16
#define DMACC2Config_A_MASK 0x20000
#define DMACC2Config_A 0x20000
#define DMACC2Config_A_BIT 17
#define DMACC2Config_H_MASK 0x40000
#define DMACC2Config_H 0x40000
#define DMACC2Config_H_BIT 18

#define DMACC3Config (*(volatile unsigned *)0x50004170)
#define DMACC3Config_OFFSET 0x170
#define DMACC3Config_E_MASK 0x1
#define DMACC3Config_E 0x1
#define DMACC3Config_E_BIT 0
#define DMACC3Config_SrcPeripheral_MASK 0x3E
#define DMACC3Config_SrcPeripheral_BIT 1
#define DMACC3Config_DestPeripheral_MASK 0x7C0
#define DMACC3Config_DestPeripheral_BIT 6
#define DMACC3Config_TransferType_MASK 0x3800
#define DMACC3Config_TransferType_BIT 11
#define DMACC3Config_IE_MASK 0x4000
#define DMACC3Config_IE 0x4000
#define DMACC3Config_IE_BIT 14
#define DMACC3Config_ITC_MASK 0x8000
#define DMACC3Config_ITC 0x8000
#define DMACC3Config_ITC_BIT 15
#define DMACC3Config_L_MASK 0x10000
#define DMACC3Config_L 0x10000
#define DMACC3Config_L_BIT 16
#define DMACC3Config_A_MASK 0x20000
#define DMACC3Config_A 0x20000
#define DMACC3Config_A_BIT 17
#define DMACC3Config_H_MASK 0x40000
#define DMACC3Config_H 0x40000
#define DMACC3Config_H_BIT 18

#define DMACC4Config (*(volatile unsigned *)0x50004190)
#define DMACC4Config_OFFSET 0x190
#define DMACC4Config_E_MASK 0x1
#define DMACC4Config_E 0x1
#define DMACC4Config_E_BIT 0
#define DMACC4Config_SrcPeripheral_MASK 0x3E
#define DMACC4Config_SrcPeripheral_BIT 1
#define DMACC4Config_DestPeripheral_MASK 0x7C0
#define DMACC4Config_DestPeripheral_BIT 6
#define DMACC4Config_TransferType_MASK 0x3800
#define DMACC4Config_TransferType_BIT 11
#define DMACC4Config_IE_MASK 0x4000
#define DMACC4Config_IE 0x4000
#define DMACC4Config_IE_BIT 14
#define DMACC4Config_ITC_MASK 0x8000
#define DMACC4Config_ITC 0x8000
#define DMACC4Config_ITC_BIT 15
#define DMACC4Config_L_MASK 0x10000
#define DMACC4Config_L 0x10000
#define DMACC4Config_L_BIT 16
#define DMACC4Config_A_MASK 0x20000
#define DMACC4Config_A 0x20000
#define DMACC4Config_A_BIT 17
#define DMACC4Config_H_MASK 0x40000
#define DMACC4Config_H 0x40000
#define DMACC4Config_H_BIT 18

#define DMACC5Config (*(volatile unsigned *)0x500041B0)
#define DMACC5Config_OFFSET 0x1B0
#define DMACC5Config_E_MASK 0x1
#define DMACC5Config_E 0x1
#define DMACC5Config_E_BIT 0
#define DMACC5Config_SrcPeripheral_MASK 0x3E
#define DMACC5Config_SrcPeripheral_BIT 1
#define DMACC5Config_DestPeripheral_MASK 0x7C0
#define DMACC5Config_DestPeripheral_BIT 6
#define DMACC5Config_TransferType_MASK 0x3800
#define DMACC5Config_TransferType_BIT 11
#define DMACC5Config_IE_MASK 0x4000
#define DMACC5Config_IE 0x4000
#define DMACC5Config_IE_BIT 14
#define DMACC5Config_ITC_MASK 0x8000
#define DMACC5Config_ITC 0x8000
#define DMACC5Config_ITC_BIT 15
#define DMACC5Config_L_MASK 0x10000
#define DMACC5Config_L 0x10000
#define DMACC5Config_L_BIT 16
#define DMACC5Config_A_MASK 0x20000
#define DMACC5Config_A 0x20000
#define DMACC5Config_A_BIT 17
#define DMACC5Config_H_MASK 0x40000
#define DMACC5Config_H 0x40000
#define DMACC5Config_H_BIT 18

#define DMACC6Config (*(volatile unsigned *)0x500041D0)
#define DMACC6Config_OFFSET 0x1D0
#define DMACC6Config_E_MASK 0x1
#define DMACC6Config_E 0x1
#define DMACC6Config_E_BIT 0
#define DMACC6Config_SrcPeripheral_MASK 0x3E
#define DMACC6Config_SrcPeripheral_BIT 1
#define DMACC6Config_DestPeripheral_MASK 0x7C0
#define DMACC6Config_DestPeripheral_BIT 6
#define DMACC6Config_TransferType_MASK 0x3800
#define DMACC6Config_TransferType_BIT 11
#define DMACC6Config_IE_MASK 0x4000
#define DMACC6Config_IE 0x4000
#define DMACC6Config_IE_BIT 14
#define DMACC6Config_ITC_MASK 0x8000
#define DMACC6Config_ITC 0x8000
#define DMACC6Config_ITC_BIT 15
#define DMACC6Config_L_MASK 0x10000
#define DMACC6Config_L 0x10000
#define DMACC6Config_L_BIT 16
#define DMACC6Config_A_MASK 0x20000
#define DMACC6Config_A 0x20000
#define DMACC6Config_A_BIT 17
#define DMACC6Config_H_MASK 0x40000
#define DMACC6Config_H 0x40000
#define DMACC6Config_H_BIT 18

#define DMACC7Config (*(volatile unsigned *)0x500041F0)
#define DMACC7Config_OFFSET 0x1F0
#define DMACC7Config_E_MASK 0x1
#define DMACC7Config_E 0x1
#define DMACC7Config_E_BIT 0
#define DMACC7Config_SrcPeripheral_MASK 0x3E
#define DMACC7Config_SrcPeripheral_BIT 1
#define DMACC7Config_DestPeripheral_MASK 0x7C0
#define DMACC7Config_DestPeripheral_BIT 6
#define DMACC7Config_TransferType_MASK 0x3800
#define DMACC7Config_TransferType_BIT 11
#define DMACC7Config_IE_MASK 0x4000
#define DMACC7Config_IE 0x4000
#define DMACC7Config_IE_BIT 14
#define DMACC7Config_ITC_MASK 0x8000
#define DMACC7Config_ITC 0x8000
#define DMACC7Config_ITC_BIT 15
#define DMACC7Config_L_MASK 0x10000
#define DMACC7Config_L 0x10000
#define DMACC7Config_L_BIT 16
#define DMACC7Config_A_MASK 0x20000
#define DMACC7Config_A 0x20000
#define DMACC7Config_A_BIT 17
#define DMACC7Config_H_MASK 0x40000
#define DMACC7Config_H 0x40000
#define DMACC7Config_H_BIT 18

#define SCnSCB_BASE_ADDRESS 0xE000E000

#define InterruptType_ICTR (*(volatile unsigned *)0xE000E004)
#define Interrupt_Control_Type InterruptType_ICTR
#define InterruptType_ICTR_OFFSET 0x4
#define Interrupt_Control_Type_OFFSET InterruptType_ICTR_OFFSET
#define InterruptType_ICTR_INTLINESNUM_MASK 0x1F
#define Interrupt_Control_Type_INTLINESNUM_MASK InterruptType_ICTR_INTLINESNUM_MASK
#define InterruptType_ICTR_INTLINESNUM_BIT 0
#define Interrupt_Control_Type_INTLINESNUM_BIT InterruptType_ICTR_INTLINESNUM_BIT

#define SCB_ACTLR (*(volatile unsigned *)0xE000E008)
#define SCB_ACTLR_OFFSET 0x8
#define SCB_ACTLR_DISMCYCINT_MASK 0x1
#define SCB_ACTLR_DISMCYCINT 0x1
#define SCB_ACTLR_DISMCYCINT_BIT 0
#define SCB_ACTLR_DISDEFWBUF_MASK 0x2
#define SCB_ACTLR_DISDEFWBUF 0x2
#define SCB_ACTLR_DISDEFWBUF_BIT 1
#define SCB_ACTLR_DISFOLD_MASK 0x4
#define SCB_ACTLR_DISFOLD 0x4
#define SCB_ACTLR_DISFOLD_BIT 2

#define SysTick_BASE_ADDRESS 0xE000E010

#define SysTick_CTRL (*(volatile unsigned *)0xE000E010)
#define SysTick_Control_And_Status SysTick_CTRL
#define SysTick_CTRL_OFFSET 0x0
#define SysTick_Control_And_Status_OFFSET SysTick_CTRL_OFFSET
#define SysTick_CTRL_COUNTFLAG_MASK 0x10000
#define SysTick_Control_And_Status_COUNTFLAG_MASK SysTick_CTRL_COUNTFLAG_MASK
#define SysTick_CTRL_COUNTFLAG 0x10000
#define SysTick_Control_And_Status_COUNTFLAG SysTick_CTRL_COUNTFLAG
#define SysTick_CTRL_COUNTFLAG_BIT 16
#define SysTick_Control_And_Status_COUNTFLAG_BIT SysTick_CTRL_COUNTFLAG_BIT
#define SysTick_CTRL_CLKSOURCE_MASK 0x4
#define SysTick_Control_And_Status_CLKSOURCE_MASK SysTick_CTRL_CLKSOURCE_MASK
#define SysTick_CTRL_CLKSOURCE 0x4
#define SysTick_Control_And_Status_CLKSOURCE SysTick_CTRL_CLKSOURCE
#define SysTick_CTRL_CLKSOURCE_BIT 2
#define SysTick_Control_And_Status_CLKSOURCE_BIT SysTick_CTRL_CLKSOURCE_BIT
#define SysTick_CTRL_TICKINT_MASK 0x2
#define SysTick_Control_And_Status_TICKINT_MASK SysTick_CTRL_TICKINT_MASK
#define SysTick_CTRL_TICKINT 0x2
#define SysTick_Control_And_Status_TICKINT SysTick_CTRL_TICKINT
#define SysTick_CTRL_TICKINT_BIT 1
#define SysTick_Control_And_Status_TICKINT_BIT SysTick_CTRL_TICKINT_BIT
#define SysTick_CTRL_ENABLE_MASK 0x1
#define SysTick_Control_And_Status_ENABLE_MASK SysTick_CTRL_ENABLE_MASK
#define SysTick_CTRL_ENABLE 0x1
#define SysTick_Control_And_Status_ENABLE SysTick_CTRL_ENABLE
#define SysTick_CTRL_ENABLE_BIT 0
#define SysTick_Control_And_Status_ENABLE_BIT SysTick_CTRL_ENABLE_BIT

#define SysTick_LOAD (*(volatile unsigned *)0xE000E014)
#define SysTick_Reload_Value SysTick_LOAD
#define SysTick_LOAD_OFFSET 0x4
#define SysTick_Reload_Value_OFFSET SysTick_LOAD_OFFSET
#define SysTick_LOAD_RELOAD_MASK 0xFFFFFF
#define SysTick_Reload_Value_RELOAD_MASK SysTick_LOAD_RELOAD_MASK
#define SysTick_LOAD_RELOAD_BIT 0
#define SysTick_Reload_Value_RELOAD_BIT SysTick_LOAD_RELOAD_BIT

#define SysTick_VAL (*(volatile unsigned *)0xE000E018)
#define SysTick_Current_Value SysTick_VAL
#define SysTick_VAL_OFFSET 0x8
#define SysTick_Current_Value_OFFSET SysTick_VAL_OFFSET
#define SysTick_VAL_CURRENT_MASK 0xFFFFFF
#define SysTick_Current_Value_CURRENT_MASK SysTick_VAL_CURRENT_MASK
#define SysTick_VAL_CURRENT_BIT 0
#define SysTick_Current_Value_CURRENT_BIT SysTick_VAL_CURRENT_BIT

#define SysTick_CALIB (*(volatile unsigned *)0xE000E01C)
#define SysTick_Calibration_Value SysTick_CALIB
#define SysTick_CALIB_OFFSET 0xC
#define SysTick_Calibration_Value_OFFSET SysTick_CALIB_OFFSET
#define SysTick_CALIB_NOREF_MASK 0x80000000
#define SysTick_Calibration_Value_NOREF_MASK SysTick_CALIB_NOREF_MASK
#define SysTick_CALIB_NOREF 0x80000000
#define SysTick_Calibration_Value_NOREF SysTick_CALIB_NOREF
#define SysTick_CALIB_NOREF_BIT 31
#define SysTick_Calibration_Value_NOREF_BIT SysTick_CALIB_NOREF_BIT
#define SysTick_CALIB_SKEW_MASK 0x40000000
#define SysTick_Calibration_Value_SKEW_MASK SysTick_CALIB_SKEW_MASK
#define SysTick_CALIB_SKEW 0x40000000
#define SysTick_Calibration_Value_SKEW SysTick_CALIB_SKEW
#define SysTick_CALIB_SKEW_BIT 30
#define SysTick_Calibration_Value_SKEW_BIT SysTick_CALIB_SKEW_BIT
#define SysTick_CALIB_TENMS_MASK 0xFFFFFF
#define SysTick_Calibration_Value_TENMS_MASK SysTick_CALIB_TENMS_MASK
#define SysTick_CALIB_TENMS_BIT 0
#define SysTick_Calibration_Value_TENMS_BIT SysTick_CALIB_TENMS_BIT

#define NVIC_BASE_ADDRESS 0xE000E100

#define NVIC_ISER0 (*(volatile unsigned *)0xE000E100)
#define Irq_0_to_31_Set_Enable NVIC_ISER0
#define NVIC_ISER0_OFFSET 0x0
#define Irq_0_to_31_Set_Enable_OFFSET NVIC_ISER0_OFFSET

#define NVIC_ISER1 (*(volatile unsigned *)0xE000E104)
#define Irq_32_to_63_Set_Enable NVIC_ISER1
#define NVIC_ISER1_OFFSET 0x4
#define Irq_32_to_63_Set_Enable_OFFSET NVIC_ISER1_OFFSET

#define NVIC_ICER0 (*(volatile unsigned *)0xE000E180)
#define Irq_0_to_31_Clear_Enable NVIC_ICER0
#define NVIC_ICER0_OFFSET 0x80
#define Irq_0_to_31_Clear_Enable_OFFSET NVIC_ICER0_OFFSET

#define NVIC_ICER1 (*(volatile unsigned *)0xE000E184)
#define Irq_32_to_63_Clear_Enable NVIC_ICER1
#define NVIC_ICER1_OFFSET 0x84
#define Irq_32_to_63_Clear_Enable_OFFSET NVIC_ICER1_OFFSET

#define NVIC_ISPR0 (*(volatile unsigned *)0xE000E200)
#define Irq_0_to_31_Set_Pending NVIC_ISPR0
#define NVIC_ISPR0_OFFSET 0x100
#define Irq_0_to_31_Set_Pending_OFFSET NVIC_ISPR0_OFFSET

#define NVIC_ISPR1 (*(volatile unsigned *)0xE000E204)
#define Irq_32_to_63_Set_Pending NVIC_ISPR1
#define NVIC_ISPR1_OFFSET 0x104
#define Irq_32_to_63_Set_Pending_OFFSET NVIC_ISPR1_OFFSET

#define NVIC_ICPR0 (*(volatile unsigned *)0xE000E280)
#define Irq_0_to_31_Clear_Pending NVIC_ICPR0
#define NVIC_ICPR0_OFFSET 0x180
#define Irq_0_to_31_Clear_Pending_OFFSET NVIC_ICPR0_OFFSET

#define NVIC_ICPR1 (*(volatile unsigned *)0xE000E284)
#define Irq_32_to_63_Clear_Pending NVIC_ICPR1
#define NVIC_ICPR1_OFFSET 0x184
#define Irq_32_to_63_Clear_Pending_OFFSET NVIC_ICPR1_OFFSET

#define NVIC_IABR0 (*(volatile unsigned *)0xE000E300)
#define Irq_0_to_31_Active_Bit NVIC_IABR0
#define NVIC_IABR0_OFFSET 0x200
#define Irq_0_to_31_Active_Bit_OFFSET NVIC_IABR0_OFFSET

#define NVIC_IABR1 (*(volatile unsigned *)0xE000E304)
#define Irq_32_to_63_Active_Bit NVIC_IABR1
#define NVIC_IABR1_OFFSET 0x204
#define Irq_32_to_63_Active_Bit_OFFSET NVIC_IABR1_OFFSET

#define NVIC_IPR0 (*(volatile unsigned *)0xE000E400)
#define Irq_0_to_3_Priority NVIC_IPR0
#define NVIC_IPR0_OFFSET 0x300
#define Irq_0_to_3_Priority_OFFSET NVIC_IPR0_OFFSET
#define NVIC_IPR0_PRI_0_MASK 0xFF
#define Irq_0_to_3_Priority_PRI_0_MASK NVIC_IPR0_PRI_0_MASK
#define NVIC_IPR0_PRI_0_BIT 0
#define Irq_0_to_3_Priority_PRI_0_BIT NVIC_IPR0_PRI_0_BIT
#define NVIC_IPR0_PRI_1_MASK 0xFF00
#define Irq_0_to_3_Priority_PRI_1_MASK NVIC_IPR0_PRI_1_MASK
#define NVIC_IPR0_PRI_1_BIT 8
#define Irq_0_to_3_Priority_PRI_1_BIT NVIC_IPR0_PRI_1_BIT
#define NVIC_IPR0_PRI_2_MASK 0xFF0000
#define Irq_0_to_3_Priority_PRI_2_MASK NVIC_IPR0_PRI_2_MASK
#define NVIC_IPR0_PRI_2_BIT 16
#define Irq_0_to_3_Priority_PRI_2_BIT NVIC_IPR0_PRI_2_BIT
#define NVIC_IPR0_PRI_3_MASK 0xFF000000
#define Irq_0_to_3_Priority_PRI_3_MASK NVIC_IPR0_PRI_3_MASK
#define NVIC_IPR0_PRI_3_BIT 24
#define Irq_0_to_3_Priority_PRI_3_BIT NVIC_IPR0_PRI_3_BIT

#define NVIC_IPR1 (*(volatile unsigned *)0xE000E404)
#define Irq_4_to_7_Priority NVIC_IPR1
#define NVIC_IPR1_OFFSET 0x304
#define Irq_4_to_7_Priority_OFFSET NVIC_IPR1_OFFSET
#define NVIC_IPR1_PRI_4_MASK 0xFF
#define Irq_4_to_7_Priority_PRI_4_MASK NVIC_IPR1_PRI_4_MASK
#define NVIC_IPR1_PRI_4_BIT 0
#define Irq_4_to_7_Priority_PRI_4_BIT NVIC_IPR1_PRI_4_BIT
#define NVIC_IPR1_PRI_5_MASK 0xFF00
#define Irq_4_to_7_Priority_PRI_5_MASK NVIC_IPR1_PRI_5_MASK
#define NVIC_IPR1_PRI_5_BIT 8
#define Irq_4_to_7_Priority_PRI_5_BIT NVIC_IPR1_PRI_5_BIT
#define NVIC_IPR1_PRI_6_MASK 0xFF0000
#define Irq_4_to_7_Priority_PRI_6_MASK NVIC_IPR1_PRI_6_MASK
#define NVIC_IPR1_PRI_6_BIT 16
#define Irq_4_to_7_Priority_PRI_6_BIT NVIC_IPR1_PRI_6_BIT
#define NVIC_IPR1_PRI_7_MASK 0xFF000000
#define Irq_4_to_7_Priority_PRI_7_MASK NVIC_IPR1_PRI_7_MASK
#define NVIC_IPR1_PRI_7_BIT 24
#define Irq_4_to_7_Priority_PRI_7_BIT NVIC_IPR1_PRI_7_BIT

#define NVIC_IPR2 (*(volatile unsigned *)0xE000E408)
#define Irq_8_to_11_Priority NVIC_IPR2
#define NVIC_IPR2_OFFSET 0x308
#define Irq_8_to_11_Priority_OFFSET NVIC_IPR2_OFFSET
#define NVIC_IPR2_PRI_8_MASK 0xFF
#define Irq_8_to_11_Priority_PRI_8_MASK NVIC_IPR2_PRI_8_MASK
#define NVIC_IPR2_PRI_8_BIT 0
#define Irq_8_to_11_Priority_PRI_8_BIT NVIC_IPR2_PRI_8_BIT
#define NVIC_IPR2_PRI_9_MASK 0xFF00
#define Irq_8_to_11_Priority_PRI_9_MASK NVIC_IPR2_PRI_9_MASK
#define NVIC_IPR2_PRI_9_BIT 8
#define Irq_8_to_11_Priority_PRI_9_BIT NVIC_IPR2_PRI_9_BIT
#define NVIC_IPR2_PRI_10_MASK 0xFF0000
#define Irq_8_to_11_Priority_PRI_10_MASK NVIC_IPR2_PRI_10_MASK
#define NVIC_IPR2_PRI_10_BIT 16
#define Irq_8_to_11_Priority_PRI_10_BIT NVIC_IPR2_PRI_10_BIT
#define NVIC_IPR2_PRI_11_MASK 0xFF000000
#define Irq_8_to_11_Priority_PRI_11_MASK NVIC_IPR2_PRI_11_MASK
#define NVIC_IPR2_PRI_11_BIT 24
#define Irq_8_to_11_Priority_PRI_11_BIT NVIC_IPR2_PRI_11_BIT

#define NVIC_IPR3 (*(volatile unsigned *)0xE000E40C)
#define Irq_12_to_15_Priority NVIC_IPR3
#define NVIC_IPR3_OFFSET 0x30C
#define Irq_12_to_15_Priority_OFFSET NVIC_IPR3_OFFSET
#define NVIC_IPR3_PRI_12_MASK 0xFF
#define Irq_12_to_15_Priority_PRI_12_MASK NVIC_IPR3_PRI_12_MASK
#define NVIC_IPR3_PRI_12_BIT 0
#define Irq_12_to_15_Priority_PRI_12_BIT NVIC_IPR3_PRI_12_BIT
#define NVIC_IPR3_PRI_13_MASK 0xFF00
#define Irq_12_to_15_Priority_PRI_13_MASK NVIC_IPR3_PRI_13_MASK
#define NVIC_IPR3_PRI_13_BIT 8
#define Irq_12_to_15_Priority_PRI_13_BIT NVIC_IPR3_PRI_13_BIT
#define NVIC_IPR3_PRI_14_MASK 0xFF0000
#define Irq_12_to_15_Priority_PRI_14_MASK NVIC_IPR3_PRI_14_MASK
#define NVIC_IPR3_PRI_14_BIT 16
#define Irq_12_to_15_Priority_PRI_14_BIT NVIC_IPR3_PRI_14_BIT
#define NVIC_IPR3_PRI_15_MASK 0xFF000000
#define Irq_12_to_15_Priority_PRI_15_MASK NVIC_IPR3_PRI_15_MASK
#define NVIC_IPR3_PRI_15_BIT 24
#define Irq_12_to_15_Priority_PRI_15_BIT NVIC_IPR3_PRI_15_BIT

#define NVIC_IPR4 (*(volatile unsigned *)0xE000E410)
#define Irq_16_to_19_Priority NVIC_IPR4
#define NVIC_IPR4_OFFSET 0x310
#define Irq_16_to_19_Priority_OFFSET NVIC_IPR4_OFFSET
#define NVIC_IPR4_PRI_16_MASK 0xFF
#define Irq_16_to_19_Priority_PRI_16_MASK NVIC_IPR4_PRI_16_MASK
#define NVIC_IPR4_PRI_16_BIT 0
#define Irq_16_to_19_Priority_PRI_16_BIT NVIC_IPR4_PRI_16_BIT
#define NVIC_IPR4_PRI_17_MASK 0xFF00
#define Irq_16_to_19_Priority_PRI_17_MASK NVIC_IPR4_PRI_17_MASK
#define NVIC_IPR4_PRI_17_BIT 8
#define Irq_16_to_19_Priority_PRI_17_BIT NVIC_IPR4_PRI_17_BIT
#define NVIC_IPR4_PRI_18_MASK 0xFF0000
#define Irq_16_to_19_Priority_PRI_18_MASK NVIC_IPR4_PRI_18_MASK
#define NVIC_IPR4_PRI_18_BIT 16
#define Irq_16_to_19_Priority_PRI_18_BIT NVIC_IPR4_PRI_18_BIT
#define NVIC_IPR4_PRI_19_MASK 0xFF000000
#define Irq_16_to_19_Priority_PRI_19_MASK NVIC_IPR4_PRI_19_MASK
#define NVIC_IPR4_PRI_19_BIT 24
#define Irq_16_to_19_Priority_PRI_19_BIT NVIC_IPR4_PRI_19_BIT

#define NVIC_IPR5 (*(volatile unsigned *)0xE000E414)
#define Irq_20_to_23_Priority NVIC_IPR5
#define NVIC_IPR5_OFFSET 0x314
#define Irq_20_to_23_Priority_OFFSET NVIC_IPR5_OFFSET
#define NVIC_IPR5_PRI_20_MASK 0xFF
#define Irq_20_to_23_Priority_PRI_20_MASK NVIC_IPR5_PRI_20_MASK
#define NVIC_IPR5_PRI_20_BIT 0
#define Irq_20_to_23_Priority_PRI_20_BIT NVIC_IPR5_PRI_20_BIT
#define NVIC_IPR5_PRI_21_MASK 0xFF00
#define Irq_20_to_23_Priority_PRI_21_MASK NVIC_IPR5_PRI_21_MASK
#define NVIC_IPR5_PRI_21_BIT 8
#define Irq_20_to_23_Priority_PRI_21_BIT NVIC_IPR5_PRI_21_BIT
#define NVIC_IPR5_PRI_22_MASK 0xFF0000
#define Irq_20_to_23_Priority_PRI_22_MASK NVIC_IPR5_PRI_22_MASK
#define NVIC_IPR5_PRI_22_BIT 16
#define Irq_20_to_23_Priority_PRI_22_BIT NVIC_IPR5_PRI_22_BIT
#define NVIC_IPR5_PRI_23_MASK 0xFF000000
#define Irq_20_to_23_Priority_PRI_23_MASK NVIC_IPR5_PRI_23_MASK
#define NVIC_IPR5_PRI_23_BIT 24
#define Irq_20_to_23_Priority_PRI_23_BIT NVIC_IPR5_PRI_23_BIT

#define NVIC_IPR6 (*(volatile unsigned *)0xE000E418)
#define Irq_24_to_27_Priority NVIC_IPR6
#define NVIC_IPR6_OFFSET 0x318
#define Irq_24_to_27_Priority_OFFSET NVIC_IPR6_OFFSET
#define NVIC_IPR6_PRI_24_MASK 0xFF
#define Irq_24_to_27_Priority_PRI_24_MASK NVIC_IPR6_PRI_24_MASK
#define NVIC_IPR6_PRI_24_BIT 0
#define Irq_24_to_27_Priority_PRI_24_BIT NVIC_IPR6_PRI_24_BIT
#define NVIC_IPR6_PRI_25_MASK 0xFF00
#define Irq_24_to_27_Priority_PRI_25_MASK NVIC_IPR6_PRI_25_MASK
#define NVIC_IPR6_PRI_25_BIT 8
#define Irq_24_to_27_Priority_PRI_25_BIT NVIC_IPR6_PRI_25_BIT
#define NVIC_IPR6_PRI_26_MASK 0xFF0000
#define Irq_24_to_27_Priority_PRI_26_MASK NVIC_IPR6_PRI_26_MASK
#define NVIC_IPR6_PRI_26_BIT 16
#define Irq_24_to_27_Priority_PRI_26_BIT NVIC_IPR6_PRI_26_BIT
#define NVIC_IPR6_PRI_27_MASK 0xFF000000
#define Irq_24_to_27_Priority_PRI_27_MASK NVIC_IPR6_PRI_27_MASK
#define NVIC_IPR6_PRI_27_BIT 24
#define Irq_24_to_27_Priority_PRI_27_BIT NVIC_IPR6_PRI_27_BIT

#define NVIC_IPR7 (*(volatile unsigned *)0xE000E41C)
#define Irq_28_to_31_Priority NVIC_IPR7
#define NVIC_IPR7_OFFSET 0x31C
#define Irq_28_to_31_Priority_OFFSET NVIC_IPR7_OFFSET
#define NVIC_IPR7_PRI_28_MASK 0xFF
#define Irq_28_to_31_Priority_PRI_28_MASK NVIC_IPR7_PRI_28_MASK
#define NVIC_IPR7_PRI_28_BIT 0
#define Irq_28_to_31_Priority_PRI_28_BIT NVIC_IPR7_PRI_28_BIT
#define NVIC_IPR7_PRI_29_MASK 0xFF00
#define Irq_28_to_31_Priority_PRI_29_MASK NVIC_IPR7_PRI_29_MASK
#define NVIC_IPR7_PRI_29_BIT 8
#define Irq_28_to_31_Priority_PRI_29_BIT NVIC_IPR7_PRI_29_BIT
#define NVIC_IPR7_PRI_30_MASK 0xFF0000
#define Irq_28_to_31_Priority_PRI_30_MASK NVIC_IPR7_PRI_30_MASK
#define NVIC_IPR7_PRI_30_BIT 16
#define Irq_28_to_31_Priority_PRI_30_BIT NVIC_IPR7_PRI_30_BIT
#define NVIC_IPR7_PRI_31_MASK 0xFF000000
#define Irq_28_to_31_Priority_PRI_31_MASK NVIC_IPR7_PRI_31_MASK
#define NVIC_IPR7_PRI_31_BIT 24
#define Irq_28_to_31_Priority_PRI_31_BIT NVIC_IPR7_PRI_31_BIT

#define NVIC_IPR8 (*(volatile unsigned *)0xE000E420)
#define Irq_32_to_35_Priority NVIC_IPR8
#define NVIC_IPR8_OFFSET 0x320
#define Irq_32_to_35_Priority_OFFSET NVIC_IPR8_OFFSET
#define NVIC_IPR8_PRI_32_MASK 0xFF
#define Irq_32_to_35_Priority_PRI_32_MASK NVIC_IPR8_PRI_32_MASK
#define NVIC_IPR8_PRI_32_BIT 0
#define Irq_32_to_35_Priority_PRI_32_BIT NVIC_IPR8_PRI_32_BIT
#define NVIC_IPR8_PRI_33_MASK 0xFF00
#define Irq_32_to_35_Priority_PRI_33_MASK NVIC_IPR8_PRI_33_MASK
#define NVIC_IPR8_PRI_33_BIT 8
#define Irq_32_to_35_Priority_PRI_33_BIT NVIC_IPR8_PRI_33_BIT
#define NVIC_IPR8_PRI_34_MASK 0xFF0000
#define Irq_32_to_35_Priority_PRI_34_MASK NVIC_IPR8_PRI_34_MASK
#define NVIC_IPR8_PRI_34_BIT 16
#define Irq_32_to_35_Priority_PRI_34_BIT NVIC_IPR8_PRI_34_BIT
#define NVIC_IPR8_PRI_35_MASK 0xFF000000
#define Irq_32_to_35_Priority_PRI_35_MASK NVIC_IPR8_PRI_35_MASK
#define NVIC_IPR8_PRI_35_BIT 24
#define Irq_32_to_35_Priority_PRI_35_BIT NVIC_IPR8_PRI_35_BIT

#define NVIC_IP0 (*(volatile unsigned char *)0xE000E400)
#define NVIC_IP0_OFFSET 0x300

#define NVIC_IP1 (*(volatile unsigned char *)0xE000E401)
#define NVIC_IP1_OFFSET 0x301

#define NVIC_IP2 (*(volatile unsigned char *)0xE000E402)
#define NVIC_IP2_OFFSET 0x302

#define NVIC_IP3 (*(volatile unsigned char *)0xE000E403)
#define NVIC_IP3_OFFSET 0x303

#define NVIC_IP4 (*(volatile unsigned char *)0xE000E404)
#define NVIC_IP4_OFFSET 0x304

#define NVIC_IP5 (*(volatile unsigned char *)0xE000E405)
#define NVIC_IP5_OFFSET 0x305

#define NVIC_IP6 (*(volatile unsigned char *)0xE000E406)
#define NVIC_IP6_OFFSET 0x306

#define NVIC_IP7 (*(volatile unsigned char *)0xE000E407)
#define NVIC_IP7_OFFSET 0x307

#define NVIC_IP8 (*(volatile unsigned char *)0xE000E408)
#define NVIC_IP8_OFFSET 0x308

#define NVIC_IP9 (*(volatile unsigned char *)0xE000E409)
#define NVIC_IP9_OFFSET 0x309

#define NVIC_IP10 (*(volatile unsigned char *)0xE000E40A)
#define NVIC_IP10_OFFSET 0x30A

#define NVIC_IP11 (*(volatile unsigned char *)0xE000E40B)
#define NVIC_IP11_OFFSET 0x30B

#define NVIC_IP12 (*(volatile unsigned char *)0xE000E40C)
#define NVIC_IP12_OFFSET 0x30C

#define NVIC_IP13 (*(volatile unsigned char *)0xE000E40D)
#define NVIC_IP13_OFFSET 0x30D

#define NVIC_IP14 (*(volatile unsigned char *)0xE000E40E)
#define NVIC_IP14_OFFSET 0x30E

#define NVIC_IP15 (*(volatile unsigned char *)0xE000E40F)
#define NVIC_IP15_OFFSET 0x30F

#define NVIC_IP16 (*(volatile unsigned char *)0xE000E410)
#define NVIC_IP16_OFFSET 0x310

#define NVIC_IP17 (*(volatile unsigned char *)0xE000E411)
#define NVIC_IP17_OFFSET 0x311

#define NVIC_IP18 (*(volatile unsigned char *)0xE000E412)
#define NVIC_IP18_OFFSET 0x312

#define NVIC_IP19 (*(volatile unsigned char *)0xE000E413)
#define NVIC_IP19_OFFSET 0x313

#define NVIC_IP20 (*(volatile unsigned char *)0xE000E414)
#define NVIC_IP20_OFFSET 0x314

#define NVIC_IP21 (*(volatile unsigned char *)0xE000E415)
#define NVIC_IP21_OFFSET 0x315

#define NVIC_IP22 (*(volatile unsigned char *)0xE000E416)
#define NVIC_IP22_OFFSET 0x316

#define NVIC_IP23 (*(volatile unsigned char *)0xE000E417)
#define NVIC_IP23_OFFSET 0x317

#define NVIC_IP24 (*(volatile unsigned char *)0xE000E418)
#define NVIC_IP24_OFFSET 0x318

#define NVIC_IP25 (*(volatile unsigned char *)0xE000E419)
#define NVIC_IP25_OFFSET 0x319

#define NVIC_IP26 (*(volatile unsigned char *)0xE000E41A)
#define NVIC_IP26_OFFSET 0x31A

#define NVIC_IP27 (*(volatile unsigned char *)0xE000E41B)
#define NVIC_IP27_OFFSET 0x31B

#define NVIC_IP28 (*(volatile unsigned char *)0xE000E41C)
#define NVIC_IP28_OFFSET 0x31C

#define NVIC_IP29 (*(volatile unsigned char *)0xE000E41D)
#define NVIC_IP29_OFFSET 0x31D

#define NVIC_IP30 (*(volatile unsigned char *)0xE000E41E)
#define NVIC_IP30_OFFSET 0x31E

#define NVIC_IP31 (*(volatile unsigned char *)0xE000E41F)
#define NVIC_IP31_OFFSET 0x31F

#define NVIC_IP32 (*(volatile unsigned char *)0xE000E420)
#define NVIC_IP32_OFFSET 0x320

#define NVIC_IP33 (*(volatile unsigned char *)0xE000E421)
#define NVIC_IP33_OFFSET 0x321

#define NVIC_IP34 (*(volatile unsigned char *)0xE000E422)
#define NVIC_IP34_OFFSET 0x322

#define NVIC_STIR (*(volatile unsigned *)0xE000EF00)
#define SCB_SW_TRIG NVIC_STIR
#define NVIC_STIR_OFFSET 0xE00
#define SCB_SW_TRIG_OFFSET NVIC_STIR_OFFSET
#define NVIC_STIR_INTID_MASK 0x3FF
#define SCB_SW_TRIG_INTID_MASK NVIC_STIR_INTID_MASK
#define NVIC_STIR_INTID_BIT 0
#define SCB_SW_TRIG_INTID_BIT NVIC_STIR_INTID_BIT

#define SCB_BASE_ADDRESS 0xE000ED00

#define SCB_CPUID (*(volatile unsigned *)0xE000ED00)
#define CPUID_Base SCB_CPUID
#define SCB_CPUID_OFFSET 0x0
#define CPUID_Base_OFFSET SCB_CPUID_OFFSET
#define SCB_CPUID_IMPLEMENTER_MASK 0xFF000000
#define CPUID_Base_IMPLEMENTER_MASK SCB_CPUID_IMPLEMENTER_MASK
#define SCB_CPUID_IMPLEMENTER_BIT 24
#define CPUID_Base_IMPLEMENTER_BIT SCB_CPUID_IMPLEMENTER_BIT
#define SCB_CPUID_VARIANT_MASK 0xF00000
#define CPUID_Base_VARIANT_MASK SCB_CPUID_VARIANT_MASK
#define SCB_CPUID_VARIANT_BIT 20
#define CPUID_Base_VARIANT_BIT SCB_CPUID_VARIANT_BIT
#define SCB_CPUID_PARTNO_MASK 0xFFF0
#define CPUID_Base_PARTNO_MASK SCB_CPUID_PARTNO_MASK
#define SCB_CPUID_PARTNO_BIT 4
#define CPUID_Base_PARTNO_BIT SCB_CPUID_PARTNO_BIT
#define SCB_CPUID_REVISION_MASK 0xF
#define CPUID_Base_REVISION_MASK SCB_CPUID_REVISION_MASK
#define SCB_CPUID_REVISION_BIT 0
#define CPUID_Base_REVISION_BIT SCB_CPUID_REVISION_BIT

#define SCB_ICSR (*(volatile unsigned *)0xE000ED04)
#define Interrupt_Control_State SCB_ICSR
#define SCB_ICSR_OFFSET 0x4
#define Interrupt_Control_State_OFFSET SCB_ICSR_OFFSET
#define SCB_ICSR_NMIPENDSET_MASK 0x80000000
#define Interrupt_Control_State_NMIPENDSET_MASK SCB_ICSR_NMIPENDSET_MASK
#define SCB_ICSR_NMIPENDSET 0x80000000
#define Interrupt_Control_State_NMIPENDSET SCB_ICSR_NMIPENDSET
#define SCB_ICSR_NMIPENDSET_BIT 31
#define Interrupt_Control_State_NMIPENDSET_BIT SCB_ICSR_NMIPENDSET_BIT
#define SCB_ICSR_PENDSVSET_MASK 0x10000000
#define Interrupt_Control_State_PENDSVSET_MASK SCB_ICSR_PENDSVSET_MASK
#define SCB_ICSR_PENDSVSET 0x10000000
#define Interrupt_Control_State_PENDSVSET SCB_ICSR_PENDSVSET
#define SCB_ICSR_PENDSVSET_BIT 28
#define Interrupt_Control_State_PENDSVSET_BIT SCB_ICSR_PENDSVSET_BIT
#define SCB_ICSR_PENDSVCLR_MASK 0x8000000
#define Interrupt_Control_State_PENDSVCLR_MASK SCB_ICSR_PENDSVCLR_MASK
#define SCB_ICSR_PENDSVCLR 0x8000000
#define Interrupt_Control_State_PENDSVCLR SCB_ICSR_PENDSVCLR
#define SCB_ICSR_PENDSVCLR_BIT 27
#define Interrupt_Control_State_PENDSVCLR_BIT SCB_ICSR_PENDSVCLR_BIT
#define SCB_ICSR_PENDSTSET_MASK 0x4000000
#define Interrupt_Control_State_PENDSTSET_MASK SCB_ICSR_PENDSTSET_MASK
#define SCB_ICSR_PENDSTSET 0x4000000
#define Interrupt_Control_State_PENDSTSET SCB_ICSR_PENDSTSET
#define SCB_ICSR_PENDSTSET_BIT 26
#define Interrupt_Control_State_PENDSTSET_BIT SCB_ICSR_PENDSTSET_BIT
#define SCB_ICSR_PENDSTCLR_MASK 0x2000000
#define Interrupt_Control_State_PENDSTCLR_MASK SCB_ICSR_PENDSTCLR_MASK
#define SCB_ICSR_PENDSTCLR 0x2000000
#define Interrupt_Control_State_PENDSTCLR SCB_ICSR_PENDSTCLR
#define SCB_ICSR_PENDSTCLR_BIT 25
#define Interrupt_Control_State_PENDSTCLR_BIT SCB_ICSR_PENDSTCLR_BIT
#define SCB_ICSR_ISRPREEMPT_MASK 0x800000
#define Interrupt_Control_State_ISRPREEMPT_MASK SCB_ICSR_ISRPREEMPT_MASK
#define SCB_ICSR_ISRPREEMPT 0x800000
#define Interrupt_Control_State_ISRPREEMPT SCB_ICSR_ISRPREEMPT
#define SCB_ICSR_ISRPREEMPT_BIT 23
#define Interrupt_Control_State_ISRPREEMPT_BIT SCB_ICSR_ISRPREEMPT_BIT
#define SCB_ICSR_ISRPENDING_MASK 0x400000
#define Interrupt_Control_State_ISRPENDING_MASK SCB_ICSR_ISRPENDING_MASK
#define SCB_ICSR_ISRPENDING 0x400000
#define Interrupt_Control_State_ISRPENDING SCB_ICSR_ISRPENDING
#define SCB_ICSR_ISRPENDING_BIT 22
#define Interrupt_Control_State_ISRPENDING_BIT SCB_ICSR_ISRPENDING_BIT
#define SCB_ICSR_VECTPENDING_MASK 0x1FF000
#define Interrupt_Control_State_VECTPENDING_MASK SCB_ICSR_VECTPENDING_MASK
#define SCB_ICSR_VECTPENDING_BIT 12
#define Interrupt_Control_State_VECTPENDING_BIT SCB_ICSR_VECTPENDING_BIT
#define SCB_ICSR_RETTOBASE_MASK 0x800
#define Interrupt_Control_State_RETTOBASE_MASK SCB_ICSR_RETTOBASE_MASK
#define SCB_ICSR_RETTOBASE 0x800
#define Interrupt_Control_State_RETTOBASE SCB_ICSR_RETTOBASE
#define SCB_ICSR_RETTOBASE_BIT 11
#define Interrupt_Control_State_RETTOBASE_BIT SCB_ICSR_RETTOBASE_BIT
#define SCB_ICSR_VECTACTIVE_MASK 0x1FF
#define Interrupt_Control_State_VECTACTIVE_MASK SCB_ICSR_VECTACTIVE_MASK
#define SCB_ICSR_VECTACTIVE_BIT 0
#define Interrupt_Control_State_VECTACTIVE_BIT SCB_ICSR_VECTACTIVE_BIT

#define SCB_VTOR (*(volatile unsigned *)0xE000ED08)
#define Vector_Table_Offset SCB_VTOR
#define SCB_VTOR_OFFSET 0x8
#define Vector_Table_Offset_OFFSET SCB_VTOR_OFFSET
#define SCB_VTOR_TBLBASE_MASK 0x20000000
#define Vector_Table_Offset_TBLBASE_MASK SCB_VTOR_TBLBASE_MASK
#define SCB_VTOR_TBLBASE 0x20000000
#define Vector_Table_Offset_TBLBASE SCB_VTOR_TBLBASE
#define SCB_VTOR_TBLBASE_BIT 29
#define Vector_Table_Offset_TBLBASE_BIT SCB_VTOR_TBLBASE_BIT
#define SCB_VTOR_TBLOFF_MASK 0x1FFFFF80
#define Vector_Table_Offset_TBLOFF_MASK SCB_VTOR_TBLOFF_MASK
#define SCB_VTOR_TBLOFF_BIT 7
#define Vector_Table_Offset_TBLOFF_BIT SCB_VTOR_TBLOFF_BIT

#define SCB_AIRCR (*(volatile unsigned *)0xE000ED0C)
#define Application_Interrupt_and_Reset_Control SCB_AIRCR
#define SCB_AIRCR_OFFSET 0xC
#define Application_Interrupt_and_Reset_Control_OFFSET SCB_AIRCR_OFFSET
#define SCB_AIRCR_VECTKEY_MASK 0xFFFF0000
#define Application_Interrupt_and_Reset_Control_VECTKEY_MASK SCB_AIRCR_VECTKEY_MASK
#define SCB_AIRCR_VECTKEY_BIT 16
#define Application_Interrupt_and_Reset_Control_VECTKEY_BIT SCB_AIRCR_VECTKEY_BIT
#define SCB_AIRCR_ENDIANESS_MASK 0x8000
#define Application_Interrupt_and_Reset_Control_ENDIANESS_MASK SCB_AIRCR_ENDIANESS_MASK
#define SCB_AIRCR_ENDIANESS 0x8000
#define Application_Interrupt_and_Reset_Control_ENDIANESS SCB_AIRCR_ENDIANESS
#define SCB_AIRCR_ENDIANESS_BIT 15
#define Application_Interrupt_and_Reset_Control_ENDIANESS_BIT SCB_AIRCR_ENDIANESS_BIT
#define SCB_AIRCR_PRIGROUP_MASK 0x700
#define Application_Interrupt_and_Reset_Control_PRIGROUP_MASK SCB_AIRCR_PRIGROUP_MASK
#define SCB_AIRCR_PRIGROUP_BIT 8
#define Application_Interrupt_and_Reset_Control_PRIGROUP_BIT SCB_AIRCR_PRIGROUP_BIT
#define SCB_AIRCR_SYSRESETREQ_MASK 0x4
#define Application_Interrupt_and_Reset_Control_SYSRESETREQ_MASK SCB_AIRCR_SYSRESETREQ_MASK
#define SCB_AIRCR_SYSRESETREQ 0x4
#define Application_Interrupt_and_Reset_Control_SYSRESETREQ SCB_AIRCR_SYSRESETREQ
#define SCB_AIRCR_SYSRESETREQ_BIT 2
#define Application_Interrupt_and_Reset_Control_SYSRESETREQ_BIT SCB_AIRCR_SYSRESETREQ_BIT
#define SCB_AIRCR_VECTCLRACTIVE_MASK 0x2
#define Application_Interrupt_and_Reset_Control_VECTCLRACTIVE_MASK SCB_AIRCR_VECTCLRACTIVE_MASK
#define SCB_AIRCR_VECTCLRACTIVE 0x2
#define Application_Interrupt_and_Reset_Control_VECTCLRACTIVE SCB_AIRCR_VECTCLRACTIVE
#define SCB_AIRCR_VECTCLRACTIVE_BIT 1
#define Application_Interrupt_and_Reset_Control_VECTCLRACTIVE_BIT SCB_AIRCR_VECTCLRACTIVE_BIT
#define SCB_AIRCR_VECTRESET_MASK 0x1
#define Application_Interrupt_and_Reset_Control_VECTRESET_MASK SCB_AIRCR_VECTRESET_MASK
#define SCB_AIRCR_VECTRESET 0x1
#define Application_Interrupt_and_Reset_Control_VECTRESET SCB_AIRCR_VECTRESET
#define SCB_AIRCR_VECTRESET_BIT 0
#define Application_Interrupt_and_Reset_Control_VECTRESET_BIT SCB_AIRCR_VECTRESET_BIT

#define SCB_SCR (*(volatile unsigned *)0xE000ED10)
#define System_Control SCB_SCR
#define SCB_SCR_OFFSET 0x10
#define System_Control_OFFSET SCB_SCR_OFFSET
#define SCB_SCR_SEVONPEND_MASK 0x10
#define System_Control_SEVONPEND_MASK SCB_SCR_SEVONPEND_MASK
#define SCB_SCR_SEVONPEND 0x10
#define System_Control_SEVONPEND SCB_SCR_SEVONPEND
#define SCB_SCR_SEVONPEND_BIT 4
#define System_Control_SEVONPEND_BIT SCB_SCR_SEVONPEND_BIT
#define SCB_SCR_SLEEPDEEP_MASK 0x4
#define System_Control_SLEEPDEEP_MASK SCB_SCR_SLEEPDEEP_MASK
#define SCB_SCR_SLEEPDEEP 0x4
#define System_Control_SLEEPDEEP SCB_SCR_SLEEPDEEP
#define SCB_SCR_SLEEPDEEP_BIT 2
#define System_Control_SLEEPDEEP_BIT SCB_SCR_SLEEPDEEP_BIT
#define SCB_SCR_SLEEPONEXIT_MASK 0x2
#define System_Control_SLEEPONEXIT_MASK SCB_SCR_SLEEPONEXIT_MASK
#define SCB_SCR_SLEEPONEXIT 0x2
#define System_Control_SLEEPONEXIT SCB_SCR_SLEEPONEXIT
#define SCB_SCR_SLEEPONEXIT_BIT 1
#define System_Control_SLEEPONEXIT_BIT SCB_SCR_SLEEPONEXIT_BIT

#define SCB_CCR (*(volatile unsigned *)0xE000ED14)
#define Configuration_Control SCB_CCR
#define SCB_CCR_OFFSET 0x14
#define Configuration_Control_OFFSET SCB_CCR_OFFSET
#define SCB_CCR_STKALIGN_MASK 0x200
#define Configuration_Control_STKALIGN_MASK SCB_CCR_STKALIGN_MASK
#define SCB_CCR_STKALIGN 0x200
#define Configuration_Control_STKALIGN SCB_CCR_STKALIGN
#define SCB_CCR_STKALIGN_BIT 9
#define Configuration_Control_STKALIGN_BIT SCB_CCR_STKALIGN_BIT
#define SCB_CCR_BFHFNMIGN_MASK 0x100
#define Configuration_Control_BFHFNMIGN_MASK SCB_CCR_BFHFNMIGN_MASK
#define SCB_CCR_BFHFNMIGN 0x100
#define Configuration_Control_BFHFNMIGN SCB_CCR_BFHFNMIGN
#define SCB_CCR_BFHFNMIGN_BIT 8
#define Configuration_Control_BFHFNMIGN_BIT SCB_CCR_BFHFNMIGN_BIT
#define SCB_CCR_DIV_0_TRP_MASK 0x10
#define Configuration_Control_DIV_0_TRP_MASK SCB_CCR_DIV_0_TRP_MASK
#define SCB_CCR_DIV_0_TRP 0x10
#define Configuration_Control_DIV_0_TRP SCB_CCR_DIV_0_TRP
#define SCB_CCR_DIV_0_TRP_BIT 4
#define Configuration_Control_DIV_0_TRP_BIT SCB_CCR_DIV_0_TRP_BIT
#define SCB_CCR_UNALIGN_TRP_MASK 0x8
#define Configuration_Control_UNALIGN_TRP_MASK SCB_CCR_UNALIGN_TRP_MASK
#define SCB_CCR_UNALIGN_TRP 0x8
#define Configuration_Control_UNALIGN_TRP SCB_CCR_UNALIGN_TRP
#define SCB_CCR_UNALIGN_TRP_BIT 3
#define Configuration_Control_UNALIGN_TRP_BIT SCB_CCR_UNALIGN_TRP_BIT
#define SCB_CCR_USERSETMPEND_MASK 0x2
#define Configuration_Control_USERSETMPEND_MASK SCB_CCR_USERSETMPEND_MASK
#define SCB_CCR_USERSETMPEND 0x2
#define Configuration_Control_USERSETMPEND SCB_CCR_USERSETMPEND
#define SCB_CCR_USERSETMPEND_BIT 1
#define Configuration_Control_USERSETMPEND_BIT SCB_CCR_USERSETMPEND_BIT
#define SCB_CCR_NONBASETHRDENA_MASK 0x1
#define Configuration_Control_NONBASETHRDENA_MASK SCB_CCR_NONBASETHRDENA_MASK
#define SCB_CCR_NONBASETHRDENA 0x1
#define Configuration_Control_NONBASETHRDENA SCB_CCR_NONBASETHRDENA
#define SCB_CCR_NONBASETHRDENA_BIT 0
#define Configuration_Control_NONBASETHRDENA_BIT SCB_CCR_NONBASETHRDENA_BIT

#define SCB_SHPR1 (*(volatile unsigned *)0xE000ED18)
#define System_Handlers_4_7_Priority SCB_SHPR1
#define SCB_SHPR1_OFFSET 0x18
#define System_Handlers_4_7_Priority_OFFSET SCB_SHPR1_OFFSET
#define SCB_SHPR1_PRI_4_MASK 0xFF
#define System_Handlers_4_7_Priority_PRI_4_MASK SCB_SHPR1_PRI_4_MASK
#define SCB_SHPR1_PRI_4_BIT 0
#define System_Handlers_4_7_Priority_PRI_4_BIT SCB_SHPR1_PRI_4_BIT
#define SCB_SHPR1_PRI_5_MASK 0xFF00
#define System_Handlers_4_7_Priority_PRI_5_MASK SCB_SHPR1_PRI_5_MASK
#define SCB_SHPR1_PRI_5_BIT 8
#define System_Handlers_4_7_Priority_PRI_5_BIT SCB_SHPR1_PRI_5_BIT
#define SCB_SHPR1_PRI_6_MASK 0xFF0000
#define System_Handlers_4_7_Priority_PRI_6_MASK SCB_SHPR1_PRI_6_MASK
#define SCB_SHPR1_PRI_6_BIT 16
#define System_Handlers_4_7_Priority_PRI_6_BIT SCB_SHPR1_PRI_6_BIT
#define SCB_SHPR1_PRI_7_MASK 0xFF000000
#define System_Handlers_4_7_Priority_PRI_7_MASK SCB_SHPR1_PRI_7_MASK
#define SCB_SHPR1_PRI_7_BIT 24
#define System_Handlers_4_7_Priority_PRI_7_BIT SCB_SHPR1_PRI_7_BIT

#define SCB_SHPR2 (*(volatile unsigned *)0xE000ED1C)
#define System_Handlers_8_11_Priority SCB_SHPR2
#define SCB_SHPR2_OFFSET 0x1C
#define System_Handlers_8_11_Priority_OFFSET SCB_SHPR2_OFFSET
#define SCB_SHPR2_PRI_8_MASK 0xFF
#define System_Handlers_8_11_Priority_PRI_8_MASK SCB_SHPR2_PRI_8_MASK
#define SCB_SHPR2_PRI_8_BIT 0
#define System_Handlers_8_11_Priority_PRI_8_BIT SCB_SHPR2_PRI_8_BIT
#define SCB_SHPR2_PRI_9_MASK 0xFF00
#define System_Handlers_8_11_Priority_PRI_9_MASK SCB_SHPR2_PRI_9_MASK
#define SCB_SHPR2_PRI_9_BIT 8
#define System_Handlers_8_11_Priority_PRI_9_BIT SCB_SHPR2_PRI_9_BIT
#define SCB_SHPR2_PRI_10_MASK 0xFF0000
#define System_Handlers_8_11_Priority_PRI_10_MASK SCB_SHPR2_PRI_10_MASK
#define SCB_SHPR2_PRI_10_BIT 16
#define System_Handlers_8_11_Priority_PRI_10_BIT SCB_SHPR2_PRI_10_BIT
#define SCB_SHPR2_PRI_11_MASK 0xFF000000
#define System_Handlers_8_11_Priority_PRI_11_MASK SCB_SHPR2_PRI_11_MASK
#define SCB_SHPR2_PRI_11_BIT 24
#define System_Handlers_8_11_Priority_PRI_11_BIT SCB_SHPR2_PRI_11_BIT

#define SCB_SHPR3 (*(volatile unsigned *)0xE000ED20)
#define System_Handlers_12_15_Priority SCB_SHPR3
#define SCB_SHPR3_OFFSET 0x20
#define System_Handlers_12_15_Priority_OFFSET SCB_SHPR3_OFFSET
#define SCB_SHPR3_PRI_12_MASK 0xFF
#define System_Handlers_12_15_Priority_PRI_12_MASK SCB_SHPR3_PRI_12_MASK
#define SCB_SHPR3_PRI_12_BIT 0
#define System_Handlers_12_15_Priority_PRI_12_BIT SCB_SHPR3_PRI_12_BIT
#define SCB_SHPR3_PRI_13_MASK 0xFF00
#define System_Handlers_12_15_Priority_PRI_13_MASK SCB_SHPR3_PRI_13_MASK
#define SCB_SHPR3_PRI_13_BIT 8
#define System_Handlers_12_15_Priority_PRI_13_BIT SCB_SHPR3_PRI_13_BIT
#define SCB_SHPR3_PRI_14_MASK 0xFF0000
#define System_Handlers_12_15_Priority_PRI_14_MASK SCB_SHPR3_PRI_14_MASK
#define SCB_SHPR3_PRI_14_BIT 16
#define System_Handlers_12_15_Priority_PRI_14_BIT SCB_SHPR3_PRI_14_BIT
#define SCB_SHPR3_PRI_15_MASK 0xFF000000
#define System_Handlers_12_15_Priority_PRI_15_MASK SCB_SHPR3_PRI_15_MASK
#define SCB_SHPR3_PRI_15_BIT 24
#define System_Handlers_12_15_Priority_PRI_15_BIT SCB_SHPR3_PRI_15_BIT

#define SCB_SHP0 (*(volatile unsigned char *)0xE000ED18)
#define SCB_SHP0_OFFSET 0x18

#define SCB_SHP1 (*(volatile unsigned char *)0xE000ED19)
#define SCB_SHP1_OFFSET 0x19

#define SCB_SHP2 (*(volatile unsigned char *)0xE000ED1A)
#define SCB_SHP2_OFFSET 0x1A

#define SCB_SHP3 (*(volatile unsigned char *)0xE000ED1B)
#define SCB_SHP3_OFFSET 0x1B

#define SCB_SHP4 (*(volatile unsigned char *)0xE000ED1C)
#define SCB_SHP4_OFFSET 0x1C

#define SCB_SHP5 (*(volatile unsigned char *)0xE000ED1D)
#define SCB_SHP5_OFFSET 0x1D

#define SCB_SHP6 (*(volatile unsigned char *)0xE000ED1E)
#define SCB_SHP6_OFFSET 0x1E

#define SCB_SHP7 (*(volatile unsigned char *)0xE000ED1F)
#define SCB_SHP7_OFFSET 0x1F

#define SCB_SHP8 (*(volatile unsigned char *)0xE000ED20)
#define SCB_SHP8_OFFSET 0x20

#define SCB_SHP9 (*(volatile unsigned char *)0xE000ED21)
#define SCB_SHP9_OFFSET 0x21

#define SCB_SHP10 (*(volatile unsigned char *)0xE000ED22)
#define SCB_SHP10_OFFSET 0x22

#define SCB_SHP11 (*(volatile unsigned char *)0xE000ED23)
#define SCB_SHP11_OFFSET 0x23

#define SCB_SHCRS (*(volatile unsigned *)0xE000ED24)
#define System_Handler_Control_and_State SCB_SHCRS
#define SCB_SHCRS_OFFSET 0x24
#define System_Handler_Control_and_State_OFFSET SCB_SHCRS_OFFSET
#define SCB_SHCRS_USGFAULTENA_MASK 0x40000
#define System_Handler_Control_and_State_USGFAULTENA_MASK SCB_SHCRS_USGFAULTENA_MASK
#define SCB_SHCRS_USGFAULTENA 0x40000
#define System_Handler_Control_and_State_USGFAULTENA SCB_SHCRS_USGFAULTENA
#define SCB_SHCRS_USGFAULTENA_BIT 18
#define System_Handler_Control_and_State_USGFAULTENA_BIT SCB_SHCRS_USGFAULTENA_BIT
#define SCB_SHCRS_BUSFAULTENA_MASK 0x20000
#define System_Handler_Control_and_State_BUSFAULTENA_MASK SCB_SHCRS_BUSFAULTENA_MASK
#define SCB_SHCRS_BUSFAULTENA 0x20000
#define System_Handler_Control_and_State_BUSFAULTENA SCB_SHCRS_BUSFAULTENA
#define SCB_SHCRS_BUSFAULTENA_BIT 17
#define System_Handler_Control_and_State_BUSFAULTENA_BIT SCB_SHCRS_BUSFAULTENA_BIT
#define SCB_SHCRS_MEMFAULTENA_MASK 0x10000
#define System_Handler_Control_and_State_MEMFAULTENA_MASK SCB_SHCRS_MEMFAULTENA_MASK
#define SCB_SHCRS_MEMFAULTENA 0x10000
#define System_Handler_Control_and_State_MEMFAULTENA SCB_SHCRS_MEMFAULTENA
#define SCB_SHCRS_MEMFAULTENA_BIT 16
#define System_Handler_Control_and_State_MEMFAULTENA_BIT SCB_SHCRS_MEMFAULTENA_BIT
#define SCB_SHCRS_SVCALLPENDED_MASK 0x8000
#define System_Handler_Control_and_State_SVCALLPENDED_MASK SCB_SHCRS_SVCALLPENDED_MASK
#define SCB_SHCRS_SVCALLPENDED 0x8000
#define System_Handler_Control_and_State_SVCALLPENDED SCB_SHCRS_SVCALLPENDED
#define SCB_SHCRS_SVCALLPENDED_BIT 15
#define System_Handler_Control_and_State_SVCALLPENDED_BIT SCB_SHCRS_SVCALLPENDED_BIT
#define SCB_SHCRS_BUSFAULTPENDED_MASK 0x4000
#define System_Handler_Control_and_State_BUSFAULTPENDED_MASK SCB_SHCRS_BUSFAULTPENDED_MASK
#define SCB_SHCRS_BUSFAULTPENDED 0x4000
#define System_Handler_Control_and_State_BUSFAULTPENDED SCB_SHCRS_BUSFAULTPENDED
#define SCB_SHCRS_BUSFAULTPENDED_BIT 14
#define System_Handler_Control_and_State_BUSFAULTPENDED_BIT SCB_SHCRS_BUSFAULTPENDED_BIT
#define SCB_SHCRS_MEMFAULTPENDED_MASK 0x2000
#define System_Handler_Control_and_State_MEMFAULTPENDED_MASK SCB_SHCRS_MEMFAULTPENDED_MASK
#define SCB_SHCRS_MEMFAULTPENDED 0x2000
#define System_Handler_Control_and_State_MEMFAULTPENDED SCB_SHCRS_MEMFAULTPENDED
#define SCB_SHCRS_MEMFAULTPENDED_BIT 13
#define System_Handler_Control_and_State_MEMFAULTPENDED_BIT SCB_SHCRS_MEMFAULTPENDED_BIT
#define SCB_SHCRS_USGFAULTPENDED_MASK 0x1000
#define System_Handler_Control_and_State_USGFAULTPENDED_MASK SCB_SHCRS_USGFAULTPENDED_MASK
#define SCB_SHCRS_USGFAULTPENDED 0x1000
#define System_Handler_Control_and_State_USGFAULTPENDED SCB_SHCRS_USGFAULTPENDED
#define SCB_SHCRS_USGFAULTPENDED_BIT 12
#define System_Handler_Control_and_State_USGFAULTPENDED_BIT SCB_SHCRS_USGFAULTPENDED_BIT
#define SCB_SHCRS_SYSTICKACT_MASK 0x800
#define System_Handler_Control_and_State_SYSTICKACT_MASK SCB_SHCRS_SYSTICKACT_MASK
#define SCB_SHCRS_SYSTICKACT 0x800
#define System_Handler_Control_and_State_SYSTICKACT SCB_SHCRS_SYSTICKACT
#define SCB_SHCRS_SYSTICKACT_BIT 11
#define System_Handler_Control_and_State_SYSTICKACT_BIT SCB_SHCRS_SYSTICKACT_BIT
#define SCB_SHCRS_PENDSVACT_MASK 0x400
#define System_Handler_Control_and_State_PENDSVACT_MASK SCB_SHCRS_PENDSVACT_MASK
#define SCB_SHCRS_PENDSVACT 0x400
#define System_Handler_Control_and_State_PENDSVACT SCB_SHCRS_PENDSVACT
#define SCB_SHCRS_PENDSVACT_BIT 10
#define System_Handler_Control_and_State_PENDSVACT_BIT SCB_SHCRS_PENDSVACT_BIT
#define SCB_SHCRS_MONITORACT_MASK 0x100
#define System_Handler_Control_and_State_MONITORACT_MASK SCB_SHCRS_MONITORACT_MASK
#define SCB_SHCRS_MONITORACT 0x100
#define System_Handler_Control_and_State_MONITORACT SCB_SHCRS_MONITORACT
#define SCB_SHCRS_MONITORACT_BIT 8
#define System_Handler_Control_and_State_MONITORACT_BIT SCB_SHCRS_MONITORACT_BIT
#define SCB_SHCRS_SVCALLACT_MASK 0x80
#define System_Handler_Control_and_State_SVCALLACT_MASK SCB_SHCRS_SVCALLACT_MASK
#define SCB_SHCRS_SVCALLACT 0x80
#define System_Handler_Control_and_State_SVCALLACT SCB_SHCRS_SVCALLACT
#define SCB_SHCRS_SVCALLACT_BIT 7
#define System_Handler_Control_and_State_SVCALLACT_BIT SCB_SHCRS_SVCALLACT_BIT
#define SCB_SHCRS_USGFAULTACT_MASK 0x8
#define System_Handler_Control_and_State_USGFAULTACT_MASK SCB_SHCRS_USGFAULTACT_MASK
#define SCB_SHCRS_USGFAULTACT 0x8
#define System_Handler_Control_and_State_USGFAULTACT SCB_SHCRS_USGFAULTACT
#define SCB_SHCRS_USGFAULTACT_BIT 3
#define System_Handler_Control_and_State_USGFAULTACT_BIT SCB_SHCRS_USGFAULTACT_BIT
#define SCB_SHCRS_BUSFAULTACT_MASK 0x2
#define System_Handler_Control_and_State_BUSFAULTACT_MASK SCB_SHCRS_BUSFAULTACT_MASK
#define SCB_SHCRS_BUSFAULTACT 0x2
#define System_Handler_Control_and_State_BUSFAULTACT SCB_SHCRS_BUSFAULTACT
#define SCB_SHCRS_BUSFAULTACT_BIT 1
#define System_Handler_Control_and_State_BUSFAULTACT_BIT SCB_SHCRS_BUSFAULTACT_BIT
#define SCB_SHCRS_MEMFAULTACT_MASK 0x1
#define System_Handler_Control_and_State_MEMFAULTACT_MASK SCB_SHCRS_MEMFAULTACT_MASK
#define SCB_SHCRS_MEMFAULTACT 0x1
#define System_Handler_Control_and_State_MEMFAULTACT SCB_SHCRS_MEMFAULTACT
#define SCB_SHCRS_MEMFAULTACT_BIT 0
#define System_Handler_Control_and_State_MEMFAULTACT_BIT SCB_SHCRS_MEMFAULTACT_BIT

#define SCB_MMSR (*(volatile unsigned char *)0xE000ED28)
#define Memory_Manage_Fault_Status SCB_MMSR
#define SCB_MMSR_OFFSET 0x28
#define Memory_Manage_Fault_Status_OFFSET SCB_MMSR_OFFSET
#define SCB_MMSR_MMARVALID_MASK 0x80
#define Memory_Manage_Fault_Status_MMARVALID_MASK SCB_MMSR_MMARVALID_MASK
#define SCB_MMSR_MMARVALID 0x80
#define Memory_Manage_Fault_Status_MMARVALID SCB_MMSR_MMARVALID
#define SCB_MMSR_MMARVALID_BIT 7
#define Memory_Manage_Fault_Status_MMARVALID_BIT SCB_MMSR_MMARVALID_BIT
#define SCB_MMSR_MSTKERR_MASK 0x10
#define Memory_Manage_Fault_Status_MSTKERR_MASK SCB_MMSR_MSTKERR_MASK
#define SCB_MMSR_MSTKERR 0x10
#define Memory_Manage_Fault_Status_MSTKERR SCB_MMSR_MSTKERR
#define SCB_MMSR_MSTKERR_BIT 4
#define Memory_Manage_Fault_Status_MSTKERR_BIT SCB_MMSR_MSTKERR_BIT
#define SCB_MMSR_MUNSTKERR_MASK 0x8
#define Memory_Manage_Fault_Status_MUNSTKERR_MASK SCB_MMSR_MUNSTKERR_MASK
#define SCB_MMSR_MUNSTKERR 0x8
#define Memory_Manage_Fault_Status_MUNSTKERR SCB_MMSR_MUNSTKERR
#define SCB_MMSR_MUNSTKERR_BIT 3
#define Memory_Manage_Fault_Status_MUNSTKERR_BIT SCB_MMSR_MUNSTKERR_BIT
#define SCB_MMSR_DACCVIOL_MASK 0x2
#define Memory_Manage_Fault_Status_DACCVIOL_MASK SCB_MMSR_DACCVIOL_MASK
#define SCB_MMSR_DACCVIOL 0x2
#define Memory_Manage_Fault_Status_DACCVIOL SCB_MMSR_DACCVIOL
#define SCB_MMSR_DACCVIOL_BIT 1
#define Memory_Manage_Fault_Status_DACCVIOL_BIT SCB_MMSR_DACCVIOL_BIT
#define SCB_MMSR_IACCVIOL_MASK 0x1
#define Memory_Manage_Fault_Status_IACCVIOL_MASK SCB_MMSR_IACCVIOL_MASK
#define SCB_MMSR_IACCVIOL 0x1
#define Memory_Manage_Fault_Status_IACCVIOL SCB_MMSR_IACCVIOL
#define SCB_MMSR_IACCVIOL_BIT 0
#define Memory_Manage_Fault_Status_IACCVIOL_BIT SCB_MMSR_IACCVIOL_BIT

#define SCB_BFSR (*(volatile unsigned char *)0xE000ED29)
#define Bus_Fault_Status SCB_BFSR
#define SCB_BFSR_OFFSET 0x29
#define Bus_Fault_Status_OFFSET SCB_BFSR_OFFSET
#define SCB_BFSR_BFARVALID_MASK 0x80
#define Bus_Fault_Status_BFARVALID_MASK SCB_BFSR_BFARVALID_MASK
#define SCB_BFSR_BFARVALID 0x80
#define Bus_Fault_Status_BFARVALID SCB_BFSR_BFARVALID
#define SCB_BFSR_BFARVALID_BIT 7
#define Bus_Fault_Status_BFARVALID_BIT SCB_BFSR_BFARVALID_BIT
#define SCB_BFSR_STKERR_MASK 0x10
#define Bus_Fault_Status_STKERR_MASK SCB_BFSR_STKERR_MASK
#define SCB_BFSR_STKERR 0x10
#define Bus_Fault_Status_STKERR SCB_BFSR_STKERR
#define SCB_BFSR_STKERR_BIT 4
#define Bus_Fault_Status_STKERR_BIT SCB_BFSR_STKERR_BIT
#define SCB_BFSR_UNSTKERR_MASK 0x8
#define Bus_Fault_Status_UNSTKERR_MASK SCB_BFSR_UNSTKERR_MASK
#define SCB_BFSR_UNSTKERR 0x8
#define Bus_Fault_Status_UNSTKERR SCB_BFSR_UNSTKERR
#define SCB_BFSR_UNSTKERR_BIT 3
#define Bus_Fault_Status_UNSTKERR_BIT SCB_BFSR_UNSTKERR_BIT
#define SCB_BFSR_IMPRECISERR_MASK 0x4
#define Bus_Fault_Status_IMPRECISERR_MASK SCB_BFSR_IMPRECISERR_MASK
#define SCB_BFSR_IMPRECISERR 0x4
#define Bus_Fault_Status_IMPRECISERR SCB_BFSR_IMPRECISERR
#define SCB_BFSR_IMPRECISERR_BIT 2
#define Bus_Fault_Status_IMPRECISERR_BIT SCB_BFSR_IMPRECISERR_BIT
#define SCB_BFSR_PRECISERR_MASK 0x2
#define Bus_Fault_Status_PRECISERR_MASK SCB_BFSR_PRECISERR_MASK
#define SCB_BFSR_PRECISERR 0x2
#define Bus_Fault_Status_PRECISERR SCB_BFSR_PRECISERR
#define SCB_BFSR_PRECISERR_BIT 1
#define Bus_Fault_Status_PRECISERR_BIT SCB_BFSR_PRECISERR_BIT
#define SCB_BFSR_IBUSERR_MASK 0x1
#define Bus_Fault_Status_IBUSERR_MASK SCB_BFSR_IBUSERR_MASK
#define SCB_BFSR_IBUSERR 0x1
#define Bus_Fault_Status_IBUSERR SCB_BFSR_IBUSERR
#define SCB_BFSR_IBUSERR_BIT 0
#define Bus_Fault_Status_IBUSERR_BIT SCB_BFSR_IBUSERR_BIT

#define SCB_UFSR (*(volatile unsigned short *)0xE000ED2A)
#define Usage_Fault_Status SCB_UFSR
#define SCB_UFSR_OFFSET 0x2A
#define Usage_Fault_Status_OFFSET SCB_UFSR_OFFSET
#define SCB_UFSR_DIVBYZERO_MASK 0x200
#define Usage_Fault_Status_DIVBYZERO_MASK SCB_UFSR_DIVBYZERO_MASK
#define SCB_UFSR_DIVBYZERO 0x200
#define Usage_Fault_Status_DIVBYZERO SCB_UFSR_DIVBYZERO
#define SCB_UFSR_DIVBYZERO_BIT 9
#define Usage_Fault_Status_DIVBYZERO_BIT SCB_UFSR_DIVBYZERO_BIT
#define SCB_UFSR_UNALIGNED_MASK 0x100
#define Usage_Fault_Status_UNALIGNED_MASK SCB_UFSR_UNALIGNED_MASK
#define SCB_UFSR_UNALIGNED 0x100
#define Usage_Fault_Status_UNALIGNED SCB_UFSR_UNALIGNED
#define SCB_UFSR_UNALIGNED_BIT 8
#define Usage_Fault_Status_UNALIGNED_BIT SCB_UFSR_UNALIGNED_BIT
#define SCB_UFSR_NOCP_MASK 0x8
#define Usage_Fault_Status_NOCP_MASK SCB_UFSR_NOCP_MASK
#define SCB_UFSR_NOCP 0x8
#define Usage_Fault_Status_NOCP SCB_UFSR_NOCP
#define SCB_UFSR_NOCP_BIT 3
#define Usage_Fault_Status_NOCP_BIT SCB_UFSR_NOCP_BIT
#define SCB_UFSR_INVPC_MASK 0x4
#define Usage_Fault_Status_INVPC_MASK SCB_UFSR_INVPC_MASK
#define SCB_UFSR_INVPC 0x4
#define Usage_Fault_Status_INVPC SCB_UFSR_INVPC
#define SCB_UFSR_INVPC_BIT 2
#define Usage_Fault_Status_INVPC_BIT SCB_UFSR_INVPC_BIT
#define SCB_UFSR_INVSTATE_MASK 0x2
#define Usage_Fault_Status_INVSTATE_MASK SCB_UFSR_INVSTATE_MASK
#define SCB_UFSR_INVSTATE 0x2
#define Usage_Fault_Status_INVSTATE SCB_UFSR_INVSTATE
#define SCB_UFSR_INVSTATE_BIT 1
#define Usage_Fault_Status_INVSTATE_BIT SCB_UFSR_INVSTATE_BIT
#define SCB_UFSR_UNDEFINSTR_MASK 0x1
#define Usage_Fault_Status_UNDEFINSTR_MASK SCB_UFSR_UNDEFINSTR_MASK
#define SCB_UFSR_UNDEFINSTR 0x1
#define Usage_Fault_Status_UNDEFINSTR SCB_UFSR_UNDEFINSTR
#define SCB_UFSR_UNDEFINSTR_BIT 0
#define Usage_Fault_Status_UNDEFINSTR_BIT SCB_UFSR_UNDEFINSTR_BIT

#define SCB_HFSR (*(volatile unsigned *)0xE000ED2C)
#define Hard_Fault_Status SCB_HFSR
#define SCB_HFSR_OFFSET 0x2C
#define Hard_Fault_Status_OFFSET SCB_HFSR_OFFSET
#define SCB_HFSR_DEBUGEVT_MASK 0x80000000
#define Hard_Fault_Status_DEBUGEVT_MASK SCB_HFSR_DEBUGEVT_MASK
#define SCB_HFSR_DEBUGEVT 0x80000000
#define Hard_Fault_Status_DEBUGEVT SCB_HFSR_DEBUGEVT
#define SCB_HFSR_DEBUGEVT_BIT 31
#define Hard_Fault_Status_DEBUGEVT_BIT SCB_HFSR_DEBUGEVT_BIT
#define SCB_HFSR_FORCED_MASK 0x40000000
#define Hard_Fault_Status_FORCED_MASK SCB_HFSR_FORCED_MASK
#define SCB_HFSR_FORCED 0x40000000
#define Hard_Fault_Status_FORCED SCB_HFSR_FORCED
#define SCB_HFSR_FORCED_BIT 30
#define Hard_Fault_Status_FORCED_BIT SCB_HFSR_FORCED_BIT
#define SCB_HFSR_VECTTBL_MASK 0x2
#define Hard_Fault_Status_VECTTBL_MASK SCB_HFSR_VECTTBL_MASK
#define SCB_HFSR_VECTTBL 0x2
#define Hard_Fault_Status_VECTTBL SCB_HFSR_VECTTBL
#define SCB_HFSR_VECTTBL_BIT 1
#define Hard_Fault_Status_VECTTBL_BIT SCB_HFSR_VECTTBL_BIT

#define SCB_DFSR (*(volatile unsigned *)0xE000ED30)
#define Debug_Fault_Status SCB_DFSR
#define SCB_DFSR_OFFSET 0x30
#define Debug_Fault_Status_OFFSET SCB_DFSR_OFFSET
#define SCB_DFSR_EXTERNAL_MASK 0x10
#define Debug_Fault_Status_EXTERNAL_MASK SCB_DFSR_EXTERNAL_MASK
#define SCB_DFSR_EXTERNAL 0x10
#define Debug_Fault_Status_EXTERNAL SCB_DFSR_EXTERNAL
#define SCB_DFSR_EXTERNAL_BIT 4
#define Debug_Fault_Status_EXTERNAL_BIT SCB_DFSR_EXTERNAL_BIT
#define SCB_DFSR_VCATCH_MASK 0x8
#define Debug_Fault_Status_VCATCH_MASK SCB_DFSR_VCATCH_MASK
#define SCB_DFSR_VCATCH 0x8
#define Debug_Fault_Status_VCATCH SCB_DFSR_VCATCH
#define SCB_DFSR_VCATCH_BIT 3
#define Debug_Fault_Status_VCATCH_BIT SCB_DFSR_VCATCH_BIT
#define SCB_DFSR_DWTTRAP_MASK 0x4
#define Debug_Fault_Status_DWTTRAP_MASK SCB_DFSR_DWTTRAP_MASK
#define SCB_DFSR_DWTTRAP 0x4
#define Debug_Fault_Status_DWTTRAP SCB_DFSR_DWTTRAP
#define SCB_DFSR_DWTTRAP_BIT 2
#define Debug_Fault_Status_DWTTRAP_BIT SCB_DFSR_DWTTRAP_BIT
#define SCB_DFSR_BKPT_MASK 0x2
#define Debug_Fault_Status_BKPT_MASK SCB_DFSR_BKPT_MASK
#define SCB_DFSR_BKPT 0x2
#define Debug_Fault_Status_BKPT SCB_DFSR_BKPT
#define SCB_DFSR_BKPT_BIT 1
#define Debug_Fault_Status_BKPT_BIT SCB_DFSR_BKPT_BIT
#define SCB_DFSR_HALTED_MASK 0x1
#define Debug_Fault_Status_HALTED_MASK SCB_DFSR_HALTED_MASK
#define SCB_DFSR_HALTED 0x1
#define Debug_Fault_Status_HALTED SCB_DFSR_HALTED
#define SCB_DFSR_HALTED_BIT 0
#define Debug_Fault_Status_HALTED_BIT SCB_DFSR_HALTED_BIT

#define SCB_MMFAR (*(volatile unsigned *)0xE000ED34)
#define Memory_Manage_Fault_Address SCB_MMFAR
#define SCB_MMFAR_OFFSET 0x34
#define Memory_Manage_Fault_Address_OFFSET SCB_MMFAR_OFFSET

#define SCB_BFAR (*(volatile unsigned *)0xE000ED38)
#define Bus_Fault_Address SCB_BFAR
#define SCB_BFAR_OFFSET 0x38
#define Bus_Fault_Address_OFFSET SCB_BFAR_OFFSET

#define SCB_AFSR (*(volatile unsigned *)0xE000ED3C)
#define Auxillary_Fault_Address SCB_AFSR
#define SCB_AFSR_OFFSET 0x3C
#define Auxillary_Fault_Address_OFFSET SCB_AFSR_OFFSET

#define SCB_PFR0 (*(volatile unsigned *)0xE000ED40)
#define PFR0 SCB_PFR0
#define SCB_PFR0_OFFSET 0x40
#define PFR0_OFFSET SCB_PFR0_OFFSET

#define SCB_PFR1 (*(volatile unsigned *)0xE000ED44)
#define PFR1 SCB_PFR1
#define SCB_PFR1_OFFSET 0x44
#define PFR1_OFFSET SCB_PFR1_OFFSET

#define SCB_DFR0 (*(volatile unsigned *)0xE000ED48)
#define DFR0 SCB_DFR0
#define SCB_DFR0_OFFSET 0x48
#define DFR0_OFFSET SCB_DFR0_OFFSET

#define SCB_AFR0 (*(volatile unsigned *)0xE000ED4C)
#define AFR0 SCB_AFR0
#define SCB_AFR0_OFFSET 0x4C
#define AFR0_OFFSET SCB_AFR0_OFFSET

#define SCB_MMFR0 (*(volatile unsigned *)0xE000ED50)
#define MMFR0 SCB_MMFR0
#define SCB_MMFR0_OFFSET 0x50
#define MMFR0_OFFSET SCB_MMFR0_OFFSET

#define SCB_MMFR1 (*(volatile unsigned *)0xE000ED54)
#define MMFR1 SCB_MMFR1
#define SCB_MMFR1_OFFSET 0x54
#define MMFR1_OFFSET SCB_MMFR1_OFFSET

#define SCB_MMFR2 (*(volatile unsigned *)0xE000ED58)
#define MMFR2 SCB_MMFR2
#define SCB_MMFR2_OFFSET 0x58
#define MMFR2_OFFSET SCB_MMFR2_OFFSET

#define SCB_MMFR3 (*(volatile unsigned *)0xE000ED5C)
#define MMFR3 SCB_MMFR3
#define SCB_MMFR3_OFFSET 0x5C
#define MMFR3_OFFSET SCB_MMFR3_OFFSET

#define SCB_ISAR0 (*(volatile unsigned *)0xE000ED60)
#define ISAR0 SCB_ISAR0
#define SCB_ISAR0_OFFSET 0x60
#define ISAR0_OFFSET SCB_ISAR0_OFFSET

#define SCB_ISAR1 (*(volatile unsigned *)0xE000ED64)
#define ISAR1 SCB_ISAR1
#define SCB_ISAR1_OFFSET 0x64
#define ISAR1_OFFSET SCB_ISAR1_OFFSET

#define SCB_ISAR2 (*(volatile unsigned *)0xE000ED68)
#define ISAR2 SCB_ISAR2
#define SCB_ISAR2_OFFSET 0x68
#define ISAR2_OFFSET SCB_ISAR2_OFFSET

#define SCB_ISAR3 (*(volatile unsigned *)0xE000ED6C)
#define ISAR3 SCB_ISAR3
#define SCB_ISAR3_OFFSET 0x6C
#define ISAR3_OFFSET SCB_ISAR3_OFFSET

#define SCB_ISAR4 (*(volatile unsigned *)0xE000ED70)
#define ISAR4 SCB_ISAR4
#define SCB_ISAR4_OFFSET 0x70
#define ISAR4_OFFSET SCB_ISAR4_OFFSET

#define SCB_CPACR (*(volatile unsigned *)0xE000ED88)
#define SCB_CPACR_OFFSET 0x88
#define SCB_CPACR_CP0_MASK 0x3
#define SCB_CPACR_CP0_BIT 0
#define SCB_CPACR_CP1_MASK 0xC
#define SCB_CPACR_CP1_BIT 2
#define SCB_CPACR_CP2_MASK 0x30
#define SCB_CPACR_CP2_BIT 4
#define SCB_CPACR_CP3_MASK 0xC0
#define SCB_CPACR_CP3_BIT 6
#define SCB_CPACR_CP4_MASK 0x300
#define SCB_CPACR_CP4_BIT 8
#define SCB_CPACR_CP5_MASK 0xC00
#define SCB_CPACR_CP5_BIT 10
#define SCB_CPACR_CP6_MASK 0x3000
#define SCB_CPACR_CP6_BIT 12
#define SCB_CPACR_CP7_MASK 0xC000
#define SCB_CPACR_CP7_BIT 14
#define SCB_CPACR_CP10_MASK 0x300000
#define SCB_CPACR_CP10_BIT 20
#define SCB_CPACR_CP11_MASK 0xC00000
#define SCB_CPACR_CP11_BIT 22

#define MPU_BASE_ADDRESS 0xE000ED90

#define MPU_TYPE (*(volatile unsigned *)0xE000ED90)
#define MPU_Type MPU_TYPE
#define MPU_TYPE_OFFSET 0x0
#define MPU_Type_OFFSET MPU_TYPE_OFFSET
#define MPU_TYPE_IREGION_MASK 0xFF0000
#define MPU_Type_IREGION_MASK MPU_TYPE_IREGION_MASK
#define MPU_TYPE_IREGION_BIT 16
#define MPU_Type_IREGION_BIT MPU_TYPE_IREGION_BIT
#define MPU_TYPE_DREGION_MASK 0xFF00
#define MPU_Type_DREGION_MASK MPU_TYPE_DREGION_MASK
#define MPU_TYPE_DREGION_BIT 8
#define MPU_Type_DREGION_BIT MPU_TYPE_DREGION_BIT
#define MPU_TYPE_SEPARATE_MASK 0xFF
#define MPU_Type_SEPARATE_MASK MPU_TYPE_SEPARATE_MASK
#define MPU_TYPE_SEPARATE_BIT 0
#define MPU_Type_SEPARATE_BIT MPU_TYPE_SEPARATE_BIT

#define MPU_CTRL (*(volatile unsigned *)0xE000ED94)
#define MPU_Control MPU_CTRL
#define MPU_CTRL_OFFSET 0x4
#define MPU_Control_OFFSET MPU_CTRL_OFFSET
#define MPU_CTRL_PRIVDEFENA_MASK 0x4
#define MPU_Control_PRIVDEFENA_MASK MPU_CTRL_PRIVDEFENA_MASK
#define MPU_CTRL_PRIVDEFENA 0x4
#define MPU_Control_PRIVDEFENA MPU_CTRL_PRIVDEFENA
#define MPU_CTRL_PRIVDEFENA_BIT 2
#define MPU_Control_PRIVDEFENA_BIT MPU_CTRL_PRIVDEFENA_BIT
#define MPU_CTRL_HFNMIENA_MASK 0x2
#define MPU_Control_HFNMIENA_MASK MPU_CTRL_HFNMIENA_MASK
#define MPU_CTRL_HFNMIENA 0x2
#define MPU_Control_HFNMIENA MPU_CTRL_HFNMIENA
#define MPU_CTRL_HFNMIENA_BIT 1
#define MPU_Control_HFNMIENA_BIT MPU_CTRL_HFNMIENA_BIT
#define MPU_CTRL_ENABLE_MASK 0x1
#define MPU_Control_ENABLE_MASK MPU_CTRL_ENABLE_MASK
#define MPU_CTRL_ENABLE 0x1
#define MPU_Control_ENABLE MPU_CTRL_ENABLE
#define MPU_CTRL_ENABLE_BIT 0
#define MPU_Control_ENABLE_BIT MPU_CTRL_ENABLE_BIT

#define MPU_RNR (*(volatile unsigned *)0xE000ED98)
#define MPU_Region_Number MPU_RNR
#define MPU_RNR_OFFSET 0x8
#define MPU_Region_Number_OFFSET MPU_RNR_OFFSET
#define MPU_RNR_REGION_MASK 0xFF
#define MPU_Region_Number_REGION_MASK MPU_RNR_REGION_MASK
#define MPU_RNR_REGION_BIT 0
#define MPU_Region_Number_REGION_BIT MPU_RNR_REGION_BIT

#define MPU_RBAR (*(volatile unsigned *)0xE000ED9C)
#define MPU_Region_Base_Address MPU_RBAR
#define MPU_RBAR_OFFSET 0xC
#define MPU_Region_Base_Address_OFFSET MPU_RBAR_OFFSET
#define MPU_RBAR_ADDR_MASK 0xFFFFFFE0
#define MPU_Region_Base_Address_ADDR_MASK MPU_RBAR_ADDR_MASK
#define MPU_RBAR_ADDR_BIT 5
#define MPU_Region_Base_Address_ADDR_BIT MPU_RBAR_ADDR_BIT
#define MPU_RBAR_VALID_MASK 0x10
#define MPU_Region_Base_Address_VALID_MASK MPU_RBAR_VALID_MASK
#define MPU_RBAR_VALID 0x10
#define MPU_Region_Base_Address_VALID MPU_RBAR_VALID
#define MPU_RBAR_VALID_BIT 4
#define MPU_Region_Base_Address_VALID_BIT MPU_RBAR_VALID_BIT
#define MPU_RBAR_REGION_MASK 0xF
#define MPU_Region_Base_Address_REGION_MASK MPU_RBAR_REGION_MASK
#define MPU_RBAR_REGION_BIT 0
#define MPU_Region_Base_Address_REGION_BIT MPU_RBAR_REGION_BIT

#define MPU_RASR (*(volatile unsigned *)0xE000EDA0)
#define MPU_Attribute_and_Size MPU_RASR
#define MPU_RASR_OFFSET 0x10
#define MPU_Attribute_and_Size_OFFSET MPU_RASR_OFFSET
#define MPU_RASR_XN_MASK 0x10000000
#define MPU_Attribute_and_Size_XN_MASK MPU_RASR_XN_MASK
#define MPU_RASR_XN 0x10000000
#define MPU_Attribute_and_Size_XN MPU_RASR_XN
#define MPU_RASR_XN_BIT 28
#define MPU_Attribute_and_Size_XN_BIT MPU_RASR_XN_BIT
#define MPU_RASR_AP_MASK 0x7000000
#define MPU_Attribute_and_Size_AP_MASK MPU_RASR_AP_MASK
#define MPU_RASR_AP_BIT 24
#define MPU_Attribute_and_Size_AP_BIT MPU_RASR_AP_BIT
#define MPU_RASR_TEX_MASK 0x380000
#define MPU_Attribute_and_Size_TEX_MASK MPU_RASR_TEX_MASK
#define MPU_RASR_TEX_BIT 19
#define MPU_Attribute_and_Size_TEX_BIT MPU_RASR_TEX_BIT
#define MPU_RASR_S_MASK 0x40000
#define MPU_Attribute_and_Size_S_MASK MPU_RASR_S_MASK
#define MPU_RASR_S 0x40000
#define MPU_Attribute_and_Size_S MPU_RASR_S
#define MPU_RASR_S_BIT 18
#define MPU_Attribute_and_Size_S_BIT MPU_RASR_S_BIT
#define MPU_RASR_C_MASK 0x20000
#define MPU_Attribute_and_Size_C_MASK MPU_RASR_C_MASK
#define MPU_RASR_C 0x20000
#define MPU_Attribute_and_Size_C MPU_RASR_C
#define MPU_RASR_C_BIT 17
#define MPU_Attribute_and_Size_C_BIT MPU_RASR_C_BIT
#define MPU_RASR_B_MASK 0x10000
#define MPU_Attribute_and_Size_B_MASK MPU_RASR_B_MASK
#define MPU_RASR_B 0x10000
#define MPU_Attribute_and_Size_B MPU_RASR_B
#define MPU_RASR_B_BIT 16
#define MPU_Attribute_and_Size_B_BIT MPU_RASR_B_BIT
#define MPU_RASR_SRD_MASK 0xFF00
#define MPU_Attribute_and_Size_SRD_MASK MPU_RASR_SRD_MASK
#define MPU_RASR_SRD_BIT 8
#define MPU_Attribute_and_Size_SRD_BIT MPU_RASR_SRD_BIT
#define MPU_RASR_SIZE_MASK 0x3E
#define MPU_Attribute_and_Size_SIZE_MASK MPU_RASR_SIZE_MASK
#define MPU_RASR_SIZE_BIT 1
#define MPU_Attribute_and_Size_SIZE_BIT MPU_RASR_SIZE_BIT
#define MPU_RASR_ENABLE_MASK 0x1
#define MPU_Attribute_and_Size_ENABLE_MASK MPU_RASR_ENABLE_MASK
#define MPU_RASR_ENABLE 0x1
#define MPU_Attribute_and_Size_ENABLE MPU_RASR_ENABLE
#define MPU_RASR_ENABLE_BIT 0
#define MPU_Attribute_and_Size_ENABLE_BIT MPU_RASR_ENABLE_BIT

#define CoreDebug_BASE_ADDRESS 0xE000EDF0

#define DHCSR (*(volatile unsigned *)0xE000EDF0)
#define DHCSR_OFFSET 0x0

#define DCRSR (*(volatile unsigned *)0xE000EDF4)
#define DCRSR_OFFSET 0x4

#define DCRDR (*(volatile unsigned *)0xE000EDF8)
#define DCRDR_OFFSET 0x8

#define DEMCR (*(volatile unsigned *)0xE000EDFC)
#define DEMCR_OFFSET 0xC

#define ID_space_BASE_ADDRESS 0xE000EFD0

#define PID4 (*(volatile unsigned char *)0xE000EFD0)
#define PID4_OFFSET 0x0

#define PID5 (*(volatile unsigned char *)0xE000EFD4)
#define PID5_OFFSET 0x4

#define PID6 (*(volatile unsigned char *)0xE000EFD8)
#define PID6_OFFSET 0x8

#define PID7 (*(volatile unsigned char *)0xE000EFDC)
#define PID7_OFFSET 0xC

#define PID0 (*(volatile unsigned char *)0xE000EFE0)
#define PID0_OFFSET 0x10

#define PID1 (*(volatile unsigned char *)0xE000EFE4)
#define PID1_OFFSET 0x14

#define PID2 (*(volatile unsigned char *)0xE000EFE8)
#define PID2_OFFSET 0x18

#define PID3 (*(volatile unsigned char *)0xE000EFEC)
#define PID3_OFFSET 0x1C

#define CID0 (*(volatile unsigned char *)0xE000EFF0)
#define CID0_OFFSET 0x20

#define CID1 (*(volatile unsigned char *)0xE000EFF4)
#define CID1_OFFSET 0x24

#define CID2 (*(volatile unsigned char *)0xE000EFF8)
#define CID2_OFFSET 0x28

#define CID3 (*(volatile unsigned char *)0xE000EFFC)
#define CID3_OFFSET 0x2C

#endif
