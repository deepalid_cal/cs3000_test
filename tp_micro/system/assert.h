/*  
	CALSENSE modified assert.h

    2011.10.20 rmd : I didn't like the dependancy of the use of assert on the NDEBUG define.
    So I replaced that with our own define to control the generation of a functional assert
    or not.
*/


// Rowley C Compiler, runtime support.
//
// Copyright (c) 2001, 2002, 2010 Rowley Associates Limited.
//
// This file may be distributed under the terms of the License Agreement
// provided with this software.
//
// THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

// Can include <assert.h> multiple times, so this doesn't use the standard
// #ifndef/#define idiom.

#ifndef INC_ASSERT_H

	#define INC_ASSERT_H
	
	// ----------
	
	// 8/20/2014 rmd : Are we evalutaing assert or not. I would say YES. As long as possible. I
	// suppose down the road after release it is traditional to turn OFF. However I would have
	// to see a real reason to do so.
	#define		CALSENSE_USING_ASSERT	(1)

	// ----------

	#ifdef __cplusplus
	extern "C" {
	#endif
	
		/*! \brief User defined behaviour for the assert macro \ingroup Functions \synopsis
		
		 \desc There is no default implementation of \b \this. Keeping \b \this 
		  out of the library means that you can can customize its behaviour without rebuilding 
		  the library. You must implement this function where \a expression is the stringized
		  expression, \a filename is the filename of the source file and \a line
		  is the linenumber of the failed assertion.
		*/
		#if ( CALSENSE_USING_ASSERT == 1 )
		
			extern void __assert(const char *__expression, const char *__filename, int __line);
		
			#define assert( e ) ( ( e ) ? (void)0 : __assert( #e, __FILE__, __LINE__ ) )
		
		#else
		
			#define assert(e) ((void)0)
		
		#endif
	
	#ifdef __cplusplus
	}
	#endif

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

