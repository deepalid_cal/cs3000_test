///////////////////////////////////////////////////////////
//
//	File:    eeprom.c
//
// Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
///////////////////////////////////////////////////////////
#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"

///////////////////////////////////////////////////////
//	These are located in the FLASH memory, not EEPROM
///////////////////////////////////////////////////////
const FLASH_DEFAULTS_s Flash_Defaults = 
{
	// DECODER_COMMON_PARMS_s DfltCommonParms
	{
		DECODER_COMMON_PARM_INITS
	},

	// DECODER_PARMS_s DfltSolxParms
	{
		DECODER_PARM_INITS
	},
};



