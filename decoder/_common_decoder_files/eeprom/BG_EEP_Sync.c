/////////////////////////////////////////////////////////////
//
//	File:    BG_EEP_Sync.c
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
//
// Purpose: This function handles the synchronization of 
//          variables in RAM with their non-volatile copies
//          in EEPROM.
/////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>

#include "LPC1000.h"
#include "LPC11Exx.h"

#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "ProtocolMsgs.h"
#include "sol_pwm.h"
#include "SerialComm.h"
#include "BG_EEP_Sync.h"
#include "iap.h"
#include "eep.h"

#include "gpio.h"          // temp for GPIO_CLRP1 macro for debugging
#include "sys_timers.h"    // temp for debugging crash

// FreeRTOS includes
#include "FreeRTOS.h"
#include "task.h"

//#include "stddef.h"     // for offsetof (happens to be #included by FreeRTOS.h)

/////////////////////////////////////////////
//	Private data used only within this file
/////////////////////////////////////////////

////////////////////////////////////////////////
//  Data for the region being synced is copied
//	here  before starting any writes to EEPROM.
////////////////////////////////////////////////
static SRAM_SNAPSHOT_BUFFER_u snapshot_buffer;

////////////////////////////////////////////////
//	Data table for the SRAM/EEPROM regions that
// require synchronization. These tables are
// also used by WriteParametersToEEPROM()
// in source/util/eep.c
////////////////////////////////////////////////
const BG_SYNC_s bg_sync_s[ BG_REGIONS_CNT_e ] =
{
	{
      // Common Parameters
      (U8 *) &Flash_Defaults.DfltCommonParms,   // Pointer to defaults in Flash
		(U8 *) &CommonParms,                      // Pointer to source (address in SRAM)           
      EEPROM_COMMON_PARMS_OFFSET,               // Byte offset to destination region in EEPROM
		sizeof( DECODER_COMMON_PARMS_s ),         // region size in bytes;
      EEPROM_CRC16_COMMON_PARMS_OFFSET,         // byte offset to CRC16 in EEPROM
	},

	{
      // Solenoid 1 parameters
      (U8 *) &Flash_Defaults.DfltSolxParms,     // Pointer to defaults in Flash
		(U8 *) &Sol1Parms,                        // Pointer to source (address in SRAM)
      EEPROM_SOL1_PARMS_OFFSET,                 // Byte offset to destination region in EEPROM
		sizeof( DECODER_PARMS_s ),                // region size in bytes;
      EEPROM_CRC16_SOL1_PARMS_OFFSET,           // byte offset to CRC16 in EEPROM
	},

	{
      // Solenoid 2 parameters
      (U8 *) &Flash_Defaults.DfltSolxParms,     // Pointer to defaults in Flash
		(U8 *) &Sol2Parms,                        // Pointer to source (address in SRAM)
      EEPROM_SOL2_PARMS_OFFSET,                 // Byte offset to destination region in EEPROM
		sizeof( DECODER_PARMS_s ),                // region size in bytes;
      EEPROM_CRC16_SOL2_PARMS_OFFSET,           // byte offset to CRC16 in EEPROM
	},

	{
      // Statistics counters
      (U8 *) NULL,                              // Pointer to defaults in Flash
		(U8 *) &Decoder_stats,                    // Pointer to source (address in SRAM)
      EEPROM_STATS_OFFSET,                      // Byte offset to destination region in EEPROM
		sizeof( DECODER_STATS_s ),                // region size in bytes;
      EEPROM_CRC16_STATS_OFFSET,                // byte offset to CRC16 in EEPROM
	}
};

/////////////////////////////////////////////////////////////////
//
//    static void BG_EEP_Sync( BG_TASKS_e index )
//
//    This function handles RAM-to_EEPROM data synchronization
//    for a single region (specified by index).
//
//    WARNING: Because a static snapshot_buffer is used, this
//    function is not reentrant.
//
/////////////////////////////////////////////////////////////////
static void BG_EEP_Sync( BG_REGIONS_e index )
{
   U32 status;
  	U16 eep_crc;
   U16 region_crc16;

	// First take a snapshot of the SRAM region to check
   portENTER_CRITICAL();
	memcpy((void *) &snapshot_buffer, bg_sync_s[ index ].pSRAM, bg_sync_s[ index ].size );  
   portEXIT_CRITICAL();

	// Calculate the CRC16 for the region we just snapshot
	region_crc16 = crc16_sram_region( (U8 *) &snapshot_buffer, bg_sync_s[ index ].size );

	// See if this CRC16 matches the CRC16 in EEPROM for the region
   status = EEPROM_Read( (U8 *) &eep_crc, ((U32) bg_sync_s[ index ].CRC16), sizeof( eep_crc ) );

   if( status != CMD_SUCCESS)
   {
      // FATAL ERROR TRAP
      portENTER_CRITICAL();
      while(1);
   }

	if(  region_crc16 != eep_crc )
	{
      /////////////////////////////////////////////////////////
		// The CRC values differ: a sync operation is required.
		/////////////////////////////////////////////////////////
      status =  WriteParametersToEEPROM( index, FALSE );

      if( status != CMD_SUCCESS)
      {
         // FATAL ERROR TRAP
         portENTER_CRITICAL();
         while(1);
      }
   }
}

/////////////////////////////////////////////////////////////
//
//    void process_bg_sync( U8 sync_region )
//
//    U8 sync_region - Region # (0 to 3) to sync
//
//    0:            Common Parameters
//    1:            Solenoid 1 Parameters
//    2:            Solenoid 2 Parameters
//    3:            Statistics Counters
//
//    This function handles a sync request message.
//
/////////////////////////////////////////////////////////////
void process_bg_sync( U8 sync_region )
{
   if( sync_region < BG_REGIONS_CNT_e )
   {
      // Disable PWM timer interrupts while syncing
      enablePWM( FALSE );

      // Valid region so sync this reqion
      BG_EEP_Sync( (BG_REGIONS_e) sync_region );

      // Re-enable PWM interrupt now
      enablePWM( TRUE );
   }
}

///////////////////////////////////////////////////////////////
//
// const BG_SYNC_s *get_bg_sync_info( U8 region)
//
// This function returns a pointer to the background sync
// structure for the specified region.
//
// If an invalid region is specified, NULL is returned.
//
// This function is used by the command message handler for 
// PUT PARMS in SerialMsgProc.c
///////////////////////////////////////////////////////////////
const BG_SYNC_s *get_bg_sync_info( U8 region)
{
   const BG_SYNC_s *pInfo;

   pInfo = (BG_SYNC_s *) NULL;

   if( region <= STATS_REGION )  // Range check
   {
      pInfo = &bg_sync_s[ region ];
   }

   return pInfo;
}
