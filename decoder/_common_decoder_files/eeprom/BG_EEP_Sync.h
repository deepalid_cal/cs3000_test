/////////////////////////////////////////////////////////////
//
//	File:    BG_EEP_Sync.h
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
/////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
//	These are the logical indices for the SRAM items that
//	require SYNCing to EEPROM.
//
// NOTE: These indices can be found in ProtocolMsgs.h
//////////////////////////////////////////////////////////
typedef enum
{
	BG_COMMON_PARMS_e = COMMON_PARM_REGION,   // 0
	BG_SOL1_PARMS_e	= SOL1_PARM_REGION,     // 1
	BG_SOL2_PARMS_e	= SOL2_PARM_REGION,     // 2
	BG_STATS_e        = STATS_REGION,         // 3

	//	The number of regions
	BG_REGIONS_CNT_e	  = 4

} BG_REGIONS_e;


typedef struct
{
   U8 *FlashDefaults;   // Pointer to defaults in Flash
	U8 *pSRAM;           // Pointer to source (address in SRAM)
	U32 EEPROM;          // Byte offset to destination region in EEPROM
	U32 size;            // region size in bytes;
	U32 CRC16;           // byte offset to CRC16 in EEPROM

} BG_SYNC_s;

////////////////////////////////////////////////////
//	SNAPSHOT SRAM BUFFER
//
//	A union is used to provide a buffer in SRAM
//	that's large enough to receive a copy of any
//  of the parameter or statistics structures.
////////////////////////////////////////////////////
typedef union
{
	DECODER_COMMON_PARMS_s  u1;
	DECODER_PARMS_s			u2;
	DECODER_STATS_s			u3;

} SRAM_SNAPSHOT_BUFFER_u;

// Extern declaration
extern const BG_SYNC_s bg_sync_s[ BG_REGIONS_CNT_e ];

// Function prototypes
extern const BG_SYNC_s *get_bg_sync_info( U8 region);
extern void process_bg_sync( U8 sync_region );

