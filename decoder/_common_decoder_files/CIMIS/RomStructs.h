////////////////////////////////////////////////////////////////
//
//    File: RomStructs.h
//
//    Header file for using the ROM-based functions API
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
//    LPC_1XXX_ROM_LOC is the address of the pointer to the 
//    base address of the ROM Driver Table.
//
//    Figure 8 in UM10518 Rev 2 is misleading; 0x1fff2004 is
//    the address of the pointer to the Power API Table, not
//    to the ROM Driver Table.
////////////////////////////////////////////////////////////////

typedef struct _PWRD
{
   void (*set_pll)(unsigned int cmd[], unsigned int resp[]);
   void (*set_power)(unsigned int cmd[], unsigned int resp[]);
} PWRD;

typedef struct
{ 
   int quot;
   int rem;
} idiv_return;

typedef struct
{
   unsigned quot;
   unsigned rem;
} uidiv_return;

typedef struct
{
   /* Signed integer division */
   int(*sidiv) (int numerator, int denominator);

   /* Unsigned integer division */
   unsigned (*uidiv) (unsigned numerator, unsigned denominator);

   /* Signed integer division with remainder */
   idiv_return (*sidivmod) (int numerator, int denominator);
   
   /* Unsigned integer division with remainder */
   uidiv_return (*uidivmod)(unsigned numerator, unsigned denominator);

} LPC_ROM_DIV_STRUCT;

// ROM table structure
typedef struct _ROM
{
   const unsigned p_dev1;
   const unsigned p_dev2;
   const unsigned p_dev3;
   const PWRD *pPWRD;
   const LPC_ROM_DIV_STRUCT * pROMDiv;
   const unsigned p_dev4;
   const unsigned p_dev5;
   const unsigned p_dev6;
} ROM;


/* set_pll mode options */
#define CPU_FREQ_EQU 0
#define CPU_FREQ_LTE 1
#define CPU_FREQ_GTE 2
#define CPU_FREQ_APPROX 3

/* set_pll result0 options */
#define PLL_CMD_SUCCESS 0
#define PLL_INVALID_FREQ 1
#define PLL_INVALID_MODE 2
#define PLL_FREQ_NOT_FOUND 3

/* set_power mode options */
#define PWR_DEFAULT 0
#define PWR_CPU_PERFORMANCE 1
#define PWR_EFFICIENCY 2
#define PWR_LOW_CURRENT 3

/* set_power result0 options */
#define PWR_CMD_SUCCESS 0
#define PWR_INVALID_FREQ 1
#define PWR_INVALID_MODE 2

