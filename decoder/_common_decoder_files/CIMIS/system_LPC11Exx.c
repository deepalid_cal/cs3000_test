/******************************************************************************
 *
 ****************************************************************************
 * THIS FILE WAS MODIFIED BY DAVID SQUIRES on March 2013 to correct problems
 * with startup for the Flash Release build of the Calsense_LPC11E14_Decoder
 *
 * ENABLE_WDT must be defined for the watchdog timer code to be active!
 ****************************************************************************
 * @file     system_LPC11Exx.c
 * @purpose  CMSIS Cortex-M0 Device Peripheral Access Layer Source File
 *           for the NXP LPC11Exx Device Series
 * @version  V1.0
 * @date     31. January 2012
 *
 * @note
 * Copyright (C) 2009-2010 ARM Limited. All rights reserved.
 *
 * @par
 * ARM Limited (ARM) is supplying this software for use with Cortex-M 
 * processor based microcontrollers.  This file can be freely distributed 
 * within development tools that are supporting such ARM based processors. 
 *
 * @par
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * ARM SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 ******************************************************************************/
#include <stdint.h>
#include "LPC11Exx.h"

#include "FreeRTOS.h"   // For portENTER_CRITICAL and portEXIT_CRITICAL

/*
//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------
*/

/*--------------------- Clock Configuration ----------------------------------
//
// <e> Clock Configuration
//   <h> System Oscillator Control Register (SYSOSCCTRL)
//     <o1.0>      BYPASS: System Oscillator Bypass Enable
//                     <i> If enabled then PLL input (sys_osc_clk) is fed
//                     <i> directly from XTALIN and XTALOUT pins.
//     <o1.9>      FREQRANGE: System Oscillator Frequency Range
//                     <i> Determines frequency range for Low-power oscillator.
//                   <0=> 1 - 20 MHz
//                   <1=> 15 - 25 MHz
//   </h>
//
//   <h> Watchdog Oscillator Control Register (WDTOSCCTRL)
//     <o2.0..4>   DIVSEL: Select Divider for Fclkana
//                     <i> wdt_osc_clk = Fclkana/ (2 � (1 + DIVSEL))
//                   <0-31>
//     <o2.5..8>   FREQSEL: Select Watchdog Oscillator Analog Output Frequency (Fclkana)
//                   <0=> Undefined
//                   <1=> 0.5 MHz
//                   <2=> 0.8 MHz
//                   <3=> 1.1 MHz
//                   <4=> 1.4 MHz
//                   <5=> 1.6 MHz
//                   <6=> 1.8 MHz
//                   <7=> 2.0 MHz
//                   <8=> 2.2 MHz
//                   <9=> 2.4 MHz
//                   <10=> 2.6 MHz
//                   <11=> 2.7 MHz
//                   <12=> 2.9 MHz
//                   <13=> 3.1 MHz
//                   <14=> 3.2 MHz
//                   <15=> 3.4 MHz
//   </h>
//
//   <h> System PLL Control Register (SYSPLLCTRL)
//                   <i> F_clkout = M * F_clkin = F_CCO / (2 * P)
//                   <i> F_clkin must be in the range of  10 MHz to  25 MHz
//                   <i> F_CCO   must be in the range of 156 MHz to 320 MHz
//     <o3.0..4>   MSEL: Feedback Divider Selection
//                     <i> M = MSEL + 1
//                   <0-31>
//     <o3.5..6>   PSEL: Post Divider Selection
//                   <0=> P = 1
//                   <1=> P = 2
//                   <2=> P = 4
//                   <3=> P = 8
//   </h>
//
//   <h> System PLL Clock Source Select Register (SYSPLLCLKSEL)
//     <o4.0..1>   SEL: System PLL Clock Source
//                   <0=> IRC Oscillator
//                   <1=> System Oscillator
//                   <2=> Reserved
//                   <3=> Reserved
//   </h>
//
//   <h> Main Clock Source Select Register (MAINCLKSEL)
//     <o5.0..1>   SEL: Clock Source for Main Clock
//                   <0=> IRC Oscillator
//                   <1=> Input Clock to System PLL
//                   <2=> WDT Oscillator
//                   <3=> System PLL Clock Out
//   </h>
//
//   <h> System AHB Clock Divider Register (SYSAHBCLKDIV)
//     <o6.0..7>   DIV: System AHB Clock Divider
//                     <i> Divides main clock to provide system clock to core, memories, and peripherals.
//                     <i> 0 = is disabled
//                   <0-255>
//   </h>
// </e>
*/
///////////////////////////////////////////////////////////////////
//  The clock/PLL setup has a problem with VCC applied slowly, so
//  for the CALSENSE decoder applications, set NOP_COUNT to 1000
///////////////////////////////////////////////////////////////////
#define CLOCK_SETUP           1
#define NOP_COUNT             1000           // Was 200 in ARM original code
#define SYSOSCCTRL_Val        0x00000000     // Reset: 0x000 (Osc not bypassed, 1-20 MHz freq range
#define WDTOSCCTRL_Val        0x00000000     // Reset: 0x000
#define SYSPLLCTRL_Val        0x00000023     // Reset: 0x000
#define SYSPLLCLKSEL_Val      0x00000001     // Reset: 0x000, 1 = Crystal Osc (SYSOSC)

// This selects PLL input as main clock source
#define MAINCLKSEL_Val        0x00000001     // Reset: 0x000, 1 = PLL input

// A divider of 1 is selected 
#define SYSAHBCLKDIV_Val      0x00000001     // Reset: 0x001

/*
//-------- <<< end of configuration section >>> ------------------------------
*/

/*----------------------------------------------------------------------------
  Check the register settings
 *----------------------------------------------------------------------------*/
#define CHECK_RANGE(val, min, max)                ((val < min) || (val > max))
#define CHECK_RSVD(val, mask)                     (val & mask)

/* Clock Configuration -------------------------------------------------------*/
#if (CHECK_RSVD((SYSOSCCTRL_Val),  ~0x00000003))
   #error "SYSOSCCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((WDTOSCCTRL_Val),  ~0x000001FF))
   #error "WDTOSCCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RANGE((SYSPLLCLKSEL_Val), 0, 2))
   #error "SYSPLLCLKSEL: Value out of range!"
#endif

#if (CHECK_RSVD((SYSPLLCTRL_Val),  ~0x000001FF))
   #error "SYSPLLCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((MAINCLKSEL_Val),  ~0x00000003))
   #error "MAINCLKSEL: Invalid values of reserved bits!"
#endif

#if (CHECK_RANGE((SYSAHBCLKDIV_Val), 0, 255))
   #error "SYSAHBCLKDIV: Value out of range!"
#endif

/*----------------------------------------------------------------------------
  DEFINES
 *----------------------------------------------------------------------------*/
    
/*----------------------------------------------------------------------------
  Define clocks
 *----------------------------------------------------------------------------*/
#define __XTAL            (12000000UL)    /* Oscillator frequency             */
#define __SYS_OSC_CLK     (    __XTAL)    /* Main oscillator frequency        */
#define __IRC_OSC_CLK     (12000000UL)    /* Internal RC oscillator frequency */


#define __FREQSEL   ((WDTOSCCTRL_Val >> 5) & 0x0F)
#define __DIVSEL   (((WDTOSCCTRL_Val & 0x1F) << 1) + 2)

#if (CLOCK_SETUP)                         /* Clock Setup              */
  #if  (__FREQSEL ==  0)
    #define __WDT_OSC_CLK        ( 0)                  /* undefined */
  #elif (__FREQSEL ==  1)
    #define __WDT_OSC_CLK        ( 500000 / __DIVSEL)
  #elif (__FREQSEL ==  2)
    #define __WDT_OSC_CLK        ( 800000 / __DIVSEL)
  #elif (__FREQSEL ==  3)
    #define __WDT_OSC_CLK        (1100000 / __DIVSEL)
  #elif (__FREQSEL ==  4)
    #define __WDT_OSC_CLK        (1400000 / __DIVSEL)
  #elif (__FREQSEL ==  5)
    #define __WDT_OSC_CLK        (1600000 / __DIVSEL)
  #elif (__FREQSEL ==  6)
    #define __WDT_OSC_CLK        (1800000 / __DIVSEL)
  #elif (__FREQSEL ==  7)
    #define __WDT_OSC_CLK        (2000000 / __DIVSEL)
  #elif (__FREQSEL ==  8)
    #define __WDT_OSC_CLK        (2200000 / __DIVSEL)
  #elif (__FREQSEL ==  9)
    #define __WDT_OSC_CLK        (2400000 / __DIVSEL)
  #elif (__FREQSEL == 10)
    #define __WDT_OSC_CLK        (2600000 / __DIVSEL)
  #elif (__FREQSEL == 11)
    #define __WDT_OSC_CLK        (2700000 / __DIVSEL)
  #elif (__FREQSEL == 12)
    #define __WDT_OSC_CLK        (2900000 / __DIVSEL)
  #elif (__FREQSEL == 13)
    #define __WDT_OSC_CLK        (3100000 / __DIVSEL)
  #elif (__FREQSEL == 14)
    #define __WDT_OSC_CLK        (3200000 / __DIVSEL)
  #else
    #define __WDT_OSC_CLK        (3400000 / __DIVSEL)
  #endif

  /* sys_pllclkin calculation */
  #if   ((SYSPLLCLKSEL_Val & 0x03) == 0)
    #define __SYS_PLLCLKIN           (__IRC_OSC_CLK)
  #elif ((SYSPLLCLKSEL_Val & 0x03) == 1)
    #define __SYS_PLLCLKIN           (__SYS_OSC_CLK)
  #else
    #define __SYS_PLLCLKIN           (0)
  #endif

  #define  __SYS_PLLCLKOUT         (__SYS_PLLCLKIN * ((SYSPLLCTRL_Val & 0x01F) + 1))

  /* main clock calculation */
  #if   ((MAINCLKSEL_Val & 0x03) == 0)
    #define __MAIN_CLOCK             (__IRC_OSC_CLK)
  #elif ((MAINCLKSEL_Val & 0x03) == 1)
    #define __MAIN_CLOCK             (__SYS_PLLCLKIN)
  #elif ((MAINCLKSEL_Val & 0x03) == 2)
    #if (__FREQSEL ==  0)
      #error "MAINCLKSEL: WDT Oscillator selected but FREQSEL is undefined!"
    #else
      #define __MAIN_CLOCK           (__WDT_OSC_CLK)
    #endif
  #elif ((MAINCLKSEL_Val & 0x03) == 3)
    #define __MAIN_CLOCK             (__SYS_PLLCLKOUT)
  #else
    #define __MAIN_CLOCK             (0)
  #endif

  #define __SYSTEM_CLOCK             (__MAIN_CLOCK / SYSAHBCLKDIV_Val)         

#else
  #define __SYSTEM_CLOCK             (__IRC_OSC_CLK)
#endif  // CLOCK_SETUP 


/*----------------------------------------------------------------------------
  Clock Variable definitions
 *----------------------------------------------------------------------------*/
uint32_t SystemCoreClock = __SYSTEM_CLOCK;/*!< System Clock Frequency (Core Clock)*/


/*----------------------------------------------------------------------------
  Clock functions
 *----------------------------------------------------------------------------*/
void SystemCoreClockUpdate (void)            /* Get Core Clock Frequency      */
{
  uint32_t wdt_osc = 0;

  /* Determine clock frequency according to clock register values             */
  switch ((LPC_SYSCON->WDTOSCCTRL >> 5) & 0x0F) {
    case 0:  wdt_osc =       0; break;
    case 1:  wdt_osc =  500000; break;
    case 2:  wdt_osc =  800000; break;
    case 3:  wdt_osc = 1100000; break;
    case 4:  wdt_osc = 1400000; break;
    case 5:  wdt_osc = 1600000; break;
    case 6:  wdt_osc = 1800000; break;
    case 7:  wdt_osc = 2000000; break;
    case 8:  wdt_osc = 2200000; break;
    case 9:  wdt_osc = 2400000; break;
    case 10: wdt_osc = 2600000; break;
    case 11: wdt_osc = 2700000; break;
    case 12: wdt_osc = 2900000; break;
    case 13: wdt_osc = 3100000; break;
    case 14: wdt_osc = 3200000; break;
    case 15: wdt_osc = 3400000; break;
  }
  wdt_osc /= ((LPC_SYSCON->WDTOSCCTRL & 0x1F) << 1) + 2;
 
  switch (LPC_SYSCON->MAINCLKSEL & 0x03) {
    case 0:                             /* Internal RC oscillator             */
      SystemCoreClock = __IRC_OSC_CLK;
      break;
    case 1:                             /* Input Clock to System PLL          */
      switch (LPC_SYSCON->SYSPLLCLKSEL & 0x03) {
          case 0:                       /* Internal RC oscillator             */
            SystemCoreClock = __IRC_OSC_CLK;
            break;
          case 1:                       /* System oscillator                  */
            SystemCoreClock = __SYS_OSC_CLK;
            break;
          case 2:                       /* Reserved                           */
          case 3:                       /* Reserved                           */
            SystemCoreClock = 0;
            break;
      }
      break;
    case 2:                             /* WDT Oscillator                     */
      SystemCoreClock = wdt_osc;
      break;
    case 3:                             /* System PLL Clock Out               */
      switch (LPC_SYSCON->SYSPLLCLKSEL & 0x03) {
          case 0:                       /* Internal RC oscillator             */
            if (LPC_SYSCON->SYSPLLCTRL & 0x180) {
              SystemCoreClock = __IRC_OSC_CLK;
            } else {
              SystemCoreClock = __IRC_OSC_CLK * ((LPC_SYSCON->SYSPLLCTRL & 0x01F) + 1);
            }
            break;
          case 1:                       /* System oscillator                  */
            if (LPC_SYSCON->SYSPLLCTRL & 0x180) {
              SystemCoreClock = __SYS_OSC_CLK;
            } else {
              SystemCoreClock = __SYS_OSC_CLK * ((LPC_SYSCON->SYSPLLCTRL & 0x01F) + 1);
            }
            break;
          case 2:                       /* Reserved                           */
          case 3:                       /* Reserved                           */
            SystemCoreClock = 0;
            break;
      }
      break;
  }

  SystemCoreClock /= LPC_SYSCON->SYSAHBCLKDIV;  

}


/////////////////////////////////////////////
// Define bits in SYSCON PDRUNCFG Register
/////////////////////////////////////////////
#define IRCOUT_PD          (1<<0)         // IRC osc. output power-down
#define IRC_PD             (1<<1)         // IRC osc. power-down
#define FLASH_PD           (1<<2)         // Flash power-down
#define BOD_PD             (1<<3)         // BOD power-down
#define ADC_PD             (1<<4)         // ADC power-down
#define SYSOSC_PD          (1<<5)         // System (crystal) osc. power-down
#define WDTOSC_PD          (1<<6)         // WDT osc. power-down
#define SYSPLL_PD          (1<<7)         // System PLL power-down

#define PDRUNCFG_RESERVED  (0x0000ed00)   // bits that must be written as ones

/////////////////////////////////////
// BODCTRL Register Field Values
/////////////////////////////////////
#define BODRSTLEV(n)         (n)          // BOD Reset level thresholds: see user manual Table 28
#define BODINTVAL(n)         (n<<2)       // BOD interrupt level threshold: see user manual Table 28
#define BODRSTENA(n)         (n<<4)       // BOD Reset enable: 1: Yes, 0: No

/////////////////////////////////////////////
// Define bits for the WDTOSCCTRL register
//
// Settings produce WDTOSC clock of 9375 Hz,
// or period of about 107 microseconds. 
// WARNING: FREQUENCY TOLERANCE IS +/- 40%! 
/////////////////////////////////////////////

// Provides divide by 64: 2 x (divsel+1)
#define WDT_DIVSEL         (0x1f)

// Sets Fclkana to 0.6 MHz +/- 40%
#define WDT_FREQSEL        (1<<5)

//////////////////////////////////////////////
// Define bits for the WWDT CLKSEL register
//////////////////////////////////////////////
#define CLKSEL_IRC         (0x00)
#define CLKSEL_WDOSC       (0x01)
#define CLKSEL_LOCK        (1<<31)

//////////////////////////////////////////////
// Define bits for the WWDT MOD register
//////////////////////////////////////////////
#define MOD_WDEN           1
#define MOD_WDRESET        (1<<1)
#define MOD_WDTOF          (1<<2)
#define MOD_WDINT          (1<<3)
#define MOD_WDPROTECT      (1<<4)
#define MOD_LOCK           (1<<5)

/////////////////////////////////////////////
// Function to feed the Watchdog Timer
// to prevent a time-out and RESET.
/////////////////////////////////////////////
void Feed_Watchdog_Timer( void )
{
#ifdef ENABLE_WDT
   portENTER_CRITICAL();
   LPC_WWDT->FEED = 0xAA;
   LPC_WWDT->FEED = 0x55;
   portEXIT_CRITICAL();
#endif
}

#ifdef WDT_INTS_BUT_NO_RESET
/////////////////////////////////////////////
// WDT interrupt handler
/////////////////////////////////////////////
void WDT_IRQHandler(void)
{
  portENTER_CRITICAL();
  while(1);
}
#endif

/**
 * Initialize the system
 *
 * @param  none
 * @return none
 *
 * @brief  Setup the microcontroller system.
 *         Initialize the System.
 */
//////////////////////////////////////////////////////////////
//
//    void SystemInit(void)
//
//    Goal - to enable the BOD and WDT as soon as possible
//           using the IRC oscillator (which is already
//           running).
//
//    Outline:
//
//    1. Power up peripherals we need (PDRUNCFG register)
//    2. Enable PCLKs for peripherals we need
//    3. Enable the BOD
//    4. Select the WDT oscillator as the WWDT clock source
//    5. Set up the WDT oscillator for 9375 Hz (+/- 40%)
//
///////////////////////////////////////////////////////////////
void SystemInit( void )
{
   volatile uint32_t i;
   uint32_t val32;

   // Extra delay here probably not necessary - dks
   for (i = 0; i < NOP_COUNT; i++) __NOP();

   // 1. Power-up BOD, SYSOSC, ADC, WDTOSC. Leave IRC and IRCOUT powered up for now..
   val32 = PDRUNCFG_RESERVED & ~( BOD_PD | ADC_PD | SYSOSC_PD | WDTOSC_PD );
   LPC_SYSCON->PDRUNCFG = val32;

   /////////////////////////////////////////////////////////////////////////////
   //
   // 2. Now we will set up clocks for devices we use:
   //
   // NOTE: IF RAM1 is needed, also OR-in (1<<26) to enable its clock
   ////////////////////////////////////////////////////////////////////////////
   // Start with SYS, ROM, RAM0, FLASHREG, FLASHARRAY (but no I2C)
   val32 = 0x1f; 

   // Or in bits to enable clocks for GPIO(6), CT16B0(7), CT16B1(8),
   // USART(12), ADC(13), WWDT(15), IOCON(16), PINT(19), RAM1(26)
   val32 |= ( (1<<6) | (1<<7) | (1<<8) | (1<<12) | (1<<13) | (1<<15) | (1<<16) | (1<<19) | (1<<26) );

   // Write to the control register
   LPC_SYSCON->SYSAHBCLKCTRL = val32;

   ////////////////////////////////////////////////////////////////
   //    3. Set up the BOD Control Register:
   //
   //    BOD Reset Level 3:      Assert @2.63V, de-assert @2.71V
   //    BOD Interrupt level 3:  Assert @2.80V, de-assert @2.90V
   //    BOD Interrupt:          disabled
   ////////////////////////////////////////////////////////////////

   // Disable the BOD interrupt
   NVIC_DisableIRQ( BOD_IRQn );

   // Enable the BOD thresholds for interrupt and enable the BOD reset
   LPC_SYSCON->BODCTRL = ( BODRSTLEV(3) | BODINTVAL(3) | BODRSTENA(1) );

   ///////////////////////////////////
   // Brown-out detection is ACTIVE
   ///////////////////////////////////
   
   ////////////////////////////////////////////////////////////////
   // 4. Set the WWDT clock source to be the WDT oscillator (WDOSC)
   ////////////////////////////////////////////////////////////////
   LPC_WWDT->CLKSEL = CLKSEL_WDOSC | CLKSEL_LOCK;

   ////////////////////////////////////////////////////////////////
   // 5. Set up the WDT oscillator: Fclkana = 0.6 MHz, DIVSEL = 31
   //    Yields .6 MHz / 64 = 9375 Hz (period of 107 us.)
   ////////////////////////////////////////////////////////////////
   LPC_SYSCON->WDTOSCCTRL = WDT_DIVSEL | WDT_FREQSEL;

   ////////////////////////////////////////////////////////////////
   // 6. Set up the Watchdog Timer Constant register to establish 
   //    the WDT period of about 2 seconds. We allow 40% margin.
   //    Period = 2.8 seconds / 107 E-6 seconds = 26,168 counts.
   //    But there is a fixed x 4 prescaler so we need just
   //    26,168 / 4 = 6,542 counts. We'll round up to 6600 counts
   ////////////////////////////////////////////////////////////////
   LPC_WWDT->TC = 6600;

   ////////////////////////////////////////////////////////////////
   // Set up the Watchdog Timer operating mode
   ////////////////////////////////////////////////////////////////
#ifdef ENABLE_WDT

#ifdef WDT_INTS_BUT_NO_RESET

   // Disable the WWDT interrupt
   NVIC_DisableIRQ( WDT_IRQn );

   // This is the NVIC interrupt priority for the WDT interrupt
   // 0 = highest, 3 = lowest
   NVIC_SetPriority( WDT_IRQn, 0 );

   // Set up WDT to generate an interrupt but no hardware reset
   LPC_WWDT->MOD = ( MOD_WDEN | MOD_LOCK );

   // Set the warning interrupt to occur 1023 counts before the WDT period expires
   LPC_WWDT->WARNINT = 1023;

   // Enable the WDT interrupt
   NVIC_EnableIRQ( WDT_IRQn );

#else
   // Set up WDT to generate hardware reset but no interrupt
   LPC_WWDT->MOD = ( MOD_WDEN | MOD_WDRESET | MOD_LOCK );
#endif

   // Start the Watchdog timer by "feeding" it.
   Feed_Watchdog_Timer();

#endif

   //////////////////////////////////
   // THE WATCHDOG TIMER IS RUNNING
   //////////////////////////////////

   //////////////////////////////////////////////////////////////////////////////
   // The SYSCON SYSOSCCTRL is left with the power-on reset default value of 0.
   // The system oscillator is NOT bypassed, sys osc freq range: 1-20 MHz 
   //////////////////////////////////////////////////////////////////////////////

   // Set the PCLK divider to 1 (no division; PCLK is 12 MHz)
   LPC_SYSCON->SYSAHBCLKDIV  = SYSAHBCLKDIV_Val;

   ////////////////////////////////////////////////////////////////////
   // Configure the FLASHCFG register to optimize Flash access timing
   // for the relatively slow (12 MHz) system clock. Clear both
   // FLASHTIM field bits 1:0 to set 1 system clock for Flash access.
   //
   // NOTE: This is acceptable for a system clock up to 20 MHz
   ////////////////////////////////////////////////////////////////////
   LPC_FLASHCTRL->FLASHCFG = LPC_FLASHCTRL->FLASHCFG & 0xfffffffc;
  
   ///////////////////////////////////////////////
   // Switch over to use the crystal oscillator
   ///////////////////////////////////////////////
   LPC_SYSCON->SYSPLLCLKSEL  = 0x01;               /* Set SYSOSC as input to PLL */
   LPC_SYSCON->SYSPLLCLKUEN  = 0x01;               /* Update MCLK Clock Source */
   LPC_SYSCON->SYSPLLCLKUEN  = 0x00;               /* Update MCLK Clock Source */
   LPC_SYSCON->SYSPLLCLKUEN  = 0x01;               /* Update MCLK Clock Source */

   LPC_SYSCON->MAINCLKSEL    = 0x01;     /* Select PLL Clock Input  */
   LPC_SYSCON->MAINCLKUEN    = 0x01;               /* Update MCLK Clock Source */
   LPC_SYSCON->MAINCLKUEN    = 0x00;               /* Toggle Update Register   */
   LPC_SYSCON->MAINCLKUEN    = 0x01;

   // Note: Nowhere does the user manual show bit 0 as a status indicator for update complete! - dks
   // while (!(LPC_SYSCON->MAINCLKUEN & 0x01));       /* Wait Until Updated       */

   // Brief delay
   for (i = 0; i < NOP_COUNT; i++) __NOP();

   // Now we're going to power down:
   // a. The IRC oscillator and IRC output (bits 0, 1)
   // b. the SYSPLL since we're not using it (bit 7)
   // COMMENTED OUT - dks 02/19/2015
   // LPC_SYSCON->PDRUNCFG |= ((1<<0) | (1<<1) | (1<<7) );      // Set bits 0, 1, 7

   // Power down SYSPLL but keep IRC Osc and IRC output powered up. Why?? This allows
   // CLKOUT to work. Why this is necessary is not clear yet. dks 02/19/2015 
   // Note: IRC and IRC Output are powered down in main() init_clkout().
   LPC_SYSCON->PDRUNCFG |= ( (1<<7) );                
}
