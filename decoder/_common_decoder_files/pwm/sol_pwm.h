////////////////////////////////////////////////////////////////
//
//	File:	sol_pwm.h
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////
//	Mode flag states passed to solenoid OK
//  functions to indicate current PWM mode.
////////////////////////////////////////////
#define SOL_PWM_TEST_MODE		0
#define SOL_PWM_HOLD_MODE		2

/////////////////////////////
//	PWM Calculation defines
/////////////////////////////

//	The PWM duty cycles are programmable in 8 steps (0 to 7) 
//	over the solenoid capacitor voltage range of 13.75 VDC to
//	40.0 VDC. Solenoid voltage >= 40 VDC will use step 7,
// and solenoid voltage <= 13.75 will use step 0.
//
// 40.0 VDC = 661 ADC counts, 13.75 VDC = 227 ADC counts
// The difference between these is 26.25 VDC = 434 ADC counts
// Each PWM table step is for a 3.75VDC change in H-bridge voltage.
// Each 3.75V step is 62 ADC counts.

//	This is the number of ADC counts = 13.75 VDC
#define PWM_DUTY_CYCLE_ADC_OFFSET         227

// This is the number of ADC counts per duty cycle step (434/7 = 62)
#define PWM_ADC_DUTY_CYCLE_STEP_SIZE      62

// Macros for LPC11E14 (for connecting and disconnecting solenoid cap to H-Bridge)
#define SET_CAPCON_SOL(sol)   GPIO_SETP1 = CAPCON_SOL_MASK[ sol ]
#define CLR_CAPCON_SOL(sol)   GPIO_CLRP1 = CAPCON_SOL_MASK[ sol ]

// Macros for setting the MAT0 (sol 1) and MAT1 (sol 2) compare registers
#define SET_SOL1_PWM_CNTS(cnt16) CT16B0_MR0=((U32) cnt16)
#define SET_SOL2_PWM_CNTS(cnt16) CT16B0_MR1=((U32) cnt16)

#define SET_SOL_PWM_CNTS(sol,cnt16) \
   if( sol )                        \
   {                                \
      CT16B0_MR1 = ((U32) cnt16);   \
   }                                \
   else                             \
   {                                \
      CT16B0_MR0 = ((U32) cnt16);   \
   }

// Macro for LPC11E14
#define SET_SOL_PWM_PD(cnt16)    CT16B0_MR3 = (U32) cnt16

// Macro to compute COMPARE counts to provide a specific duty cycle.
// "percent" is the target duty cycle, "top" is the count for PWM TOP
#define PWM_DUTY_CYCLE_CNTS(percent,top)	(percent*top)/100

//////////////////////////////////////////
//	Solenoid current-related structures
//////////////////////////////////////////

typedef enum
{
	CUR_MEAS_INACTIVE_e = 0,
	CUR_MEAS_IN_PROG_e = 1,
	CUR_MEAS_READY_e = 2

} CUR_MEAS_STATE_e;

/////////////////////////////////////////////////////////
//	These flags are written by task(s) and are read by
//	the ISR processing functions.
/////////////////////////////////////////////////////////
extern volatile BOOL bSolEna[ NUM_SOLENOIDS ];

//////////////////////////////////////////////////////////////////
// These flags are set by the SerialMsgProc.c and acted upon once
// the response to a solenoid control command has been sent.
//////////////////////////////////////////////////////////////////
extern volatile BOOL bSolEnaTmp[ NUM_SOLENOIDS ];

/////////////////////////////////////////////////////////
//	Current (state machine) states of the two solenoids
/////////////////////////////////////////////////////////
extern volatile SOL_PWM_STATES_e SolPwmState_e[ NUM_SOLENOIDS ];

///////////////////////////////////////////////
//	H-Bridge capacitor voltages in ADC counts
///////////////////////////////////////////////
extern volatile U16 H_BridgeAdcCnts;

/////////////////////////////////////////////////////////
//	Solenoid Cap Delta V in ADC counts
/////////////////////////////////////////////////////////
extern volatile U16 SolCapDeltaVAdcCnts[ NUM_SOLENOIDS ];

/////////////////////////////////////////////////////////
//	Solenoid Duty cycles
/////////////////////////////////////////////////////////
extern volatile U8 SolDutyCycle[ NUM_SOLENOIDS ];

// This value is written in SerialMsgProc.c and read in sol_pwm.c
extern volatile U8 CurMeasSeqnum;

extern volatile BOOL bStartCurMeas[ NUM_SOLENOIDS ];

extern volatile CUR_MEAS_STATE_e CurMeasState_e[ NUM_SOLENOIDS ];

// Completed measurements are stored in these for reporting
extern volatile SOL_CUR_MEAS_s SolCurMeasSnapshot[ NUM_SOLENOIDS ];

// Global Function Prototypes
extern U16 adc_convert( U8 channel );
extern void enablePWM( BOOL enable );
extern void initPWM(void);
