////////////////////////////////////////////////////////////////
//
//	File:	h-bridge-tx.c
//
//	Author: David K. Squires
//			Squires Engineering, Inc.
//			for Calsense, Carlsbad, CA
//
////////////////////////////////////////////////////////////////
#include "LPC1000.h"
#include "LPC11Exx.h"

#include "data_types.h"
#include "eeprom.h"
#include "gpio.h"
#include "Calsense_2-Wire_Decoder.h"
#include "h-bridge_tx.h"
#include "sys_timers.h"
#include "uart.h"

// FreeRTOS includes
#include "FreeRTOS.h"
#include "task.h"

#include "SerialComm.h"

/////////////////////////////////////////////////////////////////////////////////////
//
//	2-Wire state transition look-up table - this is a lookup table used to 
// determine the proper MOSFET gate control transitions to create for a 
// specific "current line state" and "next line state" transition.
//
//	WARNING: The values for SPA, MRK and TRI must be 0, 1, and 2 or else 
//          this lookup table will need to be reorganized accordingly.
//
//	NOTE:    The lookup table is organized with ROWS corresponding to
//          the value of the "current line state" and COLUMNS 
//          corresponding to the value of the "next line state."
//
//  						                          === rows ===       ==== cols ====
//
/////////////////////////////////////////////////////////////////////////////////////
static const line_state_xforms_s XformLookup[ MAX_LINE_STATES ][ MAX_LINE_STATES ]= 
{
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//      ======================================== NEXT STATE ==========================================
//
// ===  ==============================  =============================	==============================
// Cur  -------------- SPA -----------  ------------ MRK ------------   -------------- TRI -----------  
// ===  ==============================  =============================	==============================

// SPA			t0          t1                  t0          t1                  t0          t1
	{
			{	SPA_SPA_T0, SPA_SPA_T1	},	{	SPA_MRK_T0, SPA_MRK_T1	},	{	SPA_TRI_T0, SPA_TRI_T1	},
	},

// MRK
	{
			{ 	MRK_SPA_T0, MRK_SPA_T1	},	{	MRK_MRK_T0, MRK_MRK_T1	},	{	MRK_TRI_T0, MRK_TRI_T1	},
	},

// TRI	
	{
			{	TRI_SPA_T0,	TRI_SPA_T1	},	{	TRI_MRK_T0, TRI_MRK_T1	},	{	TRI_TRI_T0, TRI_TRI_T1	},	
	},
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//		Function: void HBridgeOut( U8 cur, U8 next)
//
//		Description:
//
//			This function performs the H-Bridge gate I/O required to accomplish the specified change
//			to the 2-wire network output. The input parameters "cur" and "next" are the current and
//			next 2-wire network states. These are specified using the defined values MRK, SPA, and TRI,
//			which correspond to marking, spacing, or tri-state line conditions respectively.
//
//		Operations performed:
//
// 		1. PORT0 = ((PORT0 & and_mask) | or_mask);	// using t0 and_mask and or_mask values
//			2. delay 40 uS.                              // uses software delay loop
//			3. PORT0 = ((PORT0D & and_mask) | or_mask);	// using t1 and_mask and or_mask values
//
// 		Note:
//
//			This function assumes it's called at time T0, does the first operation, then the
// 		40 uS delay, followed by the second operation before returning.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////
void HBridgeOut(U8 cur, U8 next)
{
   *GPIO_PORT0_PTR = (*GPIO_PORT0_PTR & ((U32) XformLookup[cur][next].xform_t0.and_mask)) | ((U32) XformLookup[cur][next].xform_t0.or_mask);

   // Delay approximately 40 microseconds
#if ( USE_SW_TIMER_FOR_H_BRIDGE_TIMING == 1 )

   // Use software delay loop bracketed by portBEGIN_CRITICAL/portEND_CRITICAL
   sw_delay( SW_DELAY_CNT_FOR_40_US );
#else

   // Use hardware timer CT16B1 for the delay
   us_timer( 40 );
#endif

	*GPIO_PORT0_PTR = (*GPIO_PORT0_PTR & ((U32) XformLookup[cur][next].xform_t1.and_mask)) | ((U32) XformLookup[cur][next].xform_t1.or_mask);
}

////////////////////////////////////////////////
//    Pin change interrupt service routine 
//    for the USART TXD signal
//
//    P1.27 Pin change interrupt handler
//
//    Note: 1 = MARK, 0 = SPACE (USART Output),
//          Idle line state is marking.
////////////////////////////////////////////////
void FLEX_INT0_IRQHandler(void)
{
   U32 IST_Value;

   // Read interrupt status (keep only status for pin int #0)
   IST_Value = LPC_GPIO_PIN_INT->IST & 1;

   // We drive the H-Bridge MOSFET gates only when transmitting
   if( (IST_Value & 1) && (sc_state == SC_TX_SENDING_MSG_e) )
   {
      if( LPC_GPIO_PIN_INT->RISE )
      {
         // Output H-Bridge: Space-to-Mark transition
         HBridgeOut( SPA, MRK );
      }
      else if( LPC_GPIO_PIN_INT->FALL )
      {
         // Output H-Bridge: Mark-to-Space transition
         HBridgeOut( MRK, SPA );
      }
   }

   // Clear pending interrupts
   LPC_GPIO_PIN_INT->IST = IST_Value;
}

///////////////////////////////////////////////////
//
//	void send_ACK_pulse_response(void) -
//
//	Purpose: 
//
//	This function generates an ACK pulse response
//  on the 2-wire network (in response to an ENQ
//	 command message or current meas. request).
//
///////////////////////////////////////////////////
void send_ACK_pulse_response(void)
{
#if SIM_ENABLE == 0
   /////////////////////////////////////////
	//	First generate a tri-state to SPACE 
	//	transition on the 2-wire network.
	/////////////////////////////////////////

   // Disable interrupts
	portENTER_CRITICAL();

   HBridgeOut( TRI,SPA );

   // Enable interrupts now
  	portEXIT_CRITICAL();

	////////////////////////////////////////////////////
	//	Maintain the SPACING (+V) condition for the
	//	configured ACK pulse time (an EEPROM parameter)
	////////////////////////////////////////////////////
	vTaskDelay( MS_to_TICKS( ACK_PULSE_MS ) );

	////////////////////////////////////
	//	Then return the 2-wire network 
	//	to the tri-state condition
	////////////////////////////////////
   // Disable interrupts
	portENTER_CRITICAL();

	HBridgeOut( SPA, TRI );

   // Enable interrupts now
  	portEXIT_CRITICAL();

#else
   ////////////////////////////////////////////////////////
   // For testing with the EVK1100 and a direct TXD and 
   // RXD connections to the LPC11E14, this code is used.
   ////////////////////////////////////////////////////////
   UsartBreakControl( TRUE );

   ////////////////////////////////////////////////////
	//	Maintain the SPACING (+V) condition for the
	//	configured ACK pulse time (an EEPROM parameter)
	////////////////////////////////////////////////////
	vTaskDelay( MS_to_TICKS( CommonParms.AckPulse_ms ) );

   UsartBreakControl( FALSE );

#endif
}

