//////////////////////////////////////////////////////
//
//	File:	h-bridge-tx.h
//
//	Author: David K. Squires
//			Squires Engineering, Inc.
//			for Calsense, Carlsbad, CA
//	
//////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// Data structures for 2-wire line state management
/////////////////////////////////////////////////////
//
//	The two-wire line state can transition between
//	combinations of these three conditions:
//  Tri-state (T), Marking (M), and Spacing (S)
//
///////////////////////////////////////////////////////
// The port_xform_s structure defines the bit changes
// to be performed on the MOSFET gate control port (PORT0)
// to accomplish a specific combination of MOSFET 
// output changes.
//
// Changes to port pins are effected by applying two
// binary operations to the pin states:
//
//	1. Apply a binary AND mask value to the present port pin states
//	2. Apply a binary OR mask value to the result of step 1.
//
// Thus, the actions performed are:
//
//		PORT0 = ((PORT0 & and_mask) | or_mask);
//
///////////////////////////////////////////////////////

typedef struct
{
	U32 and_mask;
	U32 or_mask;
} port_xform_s;

/////////////////////////////////////////////////////////////////////////////////////
//    For a given combination of 2-wire before/after line states, a pair of xforms
//    define the PORT0 bit manipulations to be performed at the start of the
//    833 uS bit time (using xform_t0), and then 40 uS later (using xform_t1).
/////////////////////////////////////////////////////////////////////////////////////
typedef struct
{

	port_xform_s	xform_t0;	// xform for bit start time
	port_xform_s	xform_t1;	// xform for bit start time + 40 uS 

} line_state_xforms_s;


/////////////////////////////////////////////////////
// Indices for handling two-wire state transitions
//
// We assign SPA = 0 and MRK = 1 to match USART
// TXD pin states.
/////////////////////////////////////////////////////
#define SPA                0		// Spacing line state = positive voltage
#define MRK                1		// Marking line state = negative voltage
#define TRI                2		// Tri-state = floating 

#define MAX_LINE_STATES		3		// Used to size state management structures

// Mask of other PORT0 bits not used for H-Bridge
#define NON_HBRG_MASK   ( VSOL1 | VSOL2 | VHBRG | VTHERM | P0_1  | \
                          P0_2  | P0_4  | P0_5  | P0_9   | P0_16 | \
                          P0_16 | P0_17 | P0_18 | P0_19  | P0_20 | \
                          P0_21 | P0_22 | P0_23 )

// This and-or transform turns off all MOSFET gates but
// leaves other PORT0 output bits undisturbed
#define ALL_HBRG_FETS_OFF	{ NON_HBRG_MASK, 0 }

// All MOSFETs off
#define TRI_TRI_T0			ALL_HBRG_FETS_OFF
#define TRI_TRI_T1			ALL_HBRG_FETS_OFF

// Turn on LT and RB       AND            OR
#define TRI_MRK_T0			{ NON_HBRG_MASK,(HBRG_LT | HBRG_RB) }

// Leave LT, RB on			AND                                    OR
#define TRI_MRK_T1			{ (NON_HBRG_MASK | HBRG_LT | HBRG_RB), 0 }

// Turn on RT, LB
#define TRI_SPA_T0			ALL_HBRG_FETS_OFF

// Turn on RT, LB 
#define TRI_SPA_T1			{ NON_HBRG_MASK,(HBRG_RT | HBRG_LB) }

// LT, RB on
#define MRK_MRK_T0			{ NON_HBRG_MASK,(HBRG_LT | HBRG_RB) }
#define MRK_MRK_T1			{ NON_HBRG_MASK,(HBRG_LT | HBRG_RB) }

// Turn off all HBRG FETs
#define MRK_TRI_T0			ALL_HBRG_FETS_OFF

// Turn off all HBRG FETs
#define MRK_TRI_T1			ALL_HBRG_FETS_OFF

// Leave RB on, turn off others
#define MRK_SPA_T0			{ (NON_HBRG_MASK | HBRG_RB), 0 }

// Turn on LB, RT, others off
#define MRK_SPA_T1			{ NON_HBRG_MASK,(HBRG_LB | HBRG_RT) }

// Turn on LB, RT
#define SPA_SPA_T0			{ NON_HBRG_MASK,(HBRG_RT | HBRG_LB) }
#define SPA_SPA_T1			{ NON_HBRG_MASK,(HBRG_RT | HBRG_LB) }

// Turn off all HBRG FETs
#define SPA_TRI_T0			ALL_HBRG_FETS_OFF
#define SPA_TRI_T1			ALL_HBRG_FETS_OFF

// Keep LB on 
#define SPA_MRK_T0			{ (NON_HBRG_MASK | HBRG_LB), 0 }

// Turn on LB and RB others off 
#define SPA_MRK_T1			{ NON_HBRG_MASK, HBRG_LT | HBRG_RB } 

// Define names for H-Bridge signal change points in time.
// These are used as array index values.
#define	T0						0
#define	T1						1

// Function Prototypes
extern void FLEX_INT0_IRQHandler(void);
extern void HBridgeOut(U8 cur, U8 next);
extern void send_ACK_pulse_response(void);
