/////////////////////////////////////////////////////
//
//    void sw_delay(U32 value)
//
//    This function creates a delay using software. Interrupts are disabled for the duration
//    of the software delay.
//
//    NOTE: This source file has its properties set in CrossWorks so that it's optimized for
//    size and debug level is set to 1. No longer TRUE - see function header below.
//
/////////////////////////////////////////////////////
#include "FreeRTOS.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "sys_timers.h"


// 1/29/2014 rmd : Before use verify timing. This function is presently not used in the
// decoder projects but remain in the code for reference.
void __attribute__((optimize("O0"))) sw_delay(U32 value)
{
   volatile U32 counter;

   portENTER_CRITICAL();
   counter = value;
   while(counter--);
   portEXIT_CRITICAL();
}

