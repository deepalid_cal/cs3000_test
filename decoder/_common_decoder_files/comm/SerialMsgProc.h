//////////////////////////////////////////////////////
//
//
//	File:	SerialMsgProc.h
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
//////////////////////////////////////////////////////

// Function Prototypes
extern void process_sc_msg(void);

