////////////////////////////////////////////////////////////////
//
//	File:	SerialComm.c
//
//
//	Purpose: This module handles the physical I/O for the
//          USART and the 2-wire H-Bridge hardware.
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>

#include "LPC1000.h"
#include "LPC11Exx.h"
#include "system_LPC11Exx.h"

#include "data_types.h"
#include "crc16.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "ProtocolMsgs.h"
#include "sol_pwm.h"
#include "SerialComm.h"
#include "h-bridge_tx.h"
#include "SerialMsgProc.h"

// FreeRTOS includes
#include "FreeRTOS.h"
#include "task.h"

#include "uart.h"

///////////////////////
//	SC Message Buffer 
///////////////////////
U8 sc_msg_bfr[ MAX_SC_MSG_LEN ];
U8 sc_msglen;

///////////////////////////////////////////////////
//	STATIC DATA BELOW HERE - not globally visible
///////////////////////////////////////////////////

///////////////////////
//	SC Tx Ring Buffer
///////////////////////
static SC_TX_RING_BFR_s sc_tx_ring_bfr;

///////////////////////
//	SC Rx Ring Buffer
///////////////////////
static SC_RX_RING_BFR_s sc_rx_ring_bfr;

///////////////////////////////////////////
//	SC State - 	the serial communications 
//				state machine state
///////////////////////////////////////////
SC_STATES_e	sc_state;

// Rx Message CRC16
static U16 crc16;
static U16 rx_crc;

////////////////////////////////////////////
//	Changes sc_state to send the ACK pulse
////////////////////////////////////////////
void send_ack_pulse(void)
{
	sc_state = SC_TX_SENDING_ACK_PULSE_e;
}

U8 chars_loaded;

//////////////////////////////////////////////
//	Changes sc_state to send all data in the 
//	transmit ring buffer to the USART.
//////////////////////////////////////////////
void start_2_wire_transmit(void)
{
	sc_state = SC_TX_MARKING_PRE_e;
   chars_loaded = 0;
}

//////////////////////////////////////////////////////
//	Checks if data avail in ring buffer to transmit
//
//	Returns: TRUE (data avail), FALSE (no data avail)
//////////////////////////////////////////////////////
static BOOL bTxRingBfrDataAvail(void)
{
	BOOL avail;

	avail = sc_tx_ring_bfr.head != sc_tx_ring_bfr.tail ? TRUE : FALSE;
	
	return avail;
}

/////////////////////////////////////////////////////////
// Returns the next available byte to transmit.
// Must call only if bTxRingBfrDataAvail() returns TRUE
/////////////////////////////////////////////////////////
static U8 getTxRingBfrData(void)
{
	U8 c;

	// get next avail tx data byte
	c = sc_tx_ring_bfr.buf[ sc_tx_ring_bfr.head++ ];

	// check for ptr wraparound
	if( sc_tx_ring_bfr.head >= TX_MSG_BUFFER_SIZE_BYTES )
	{
		sc_tx_ring_bfr.head = 0;
	}

	return c;
}

// Add a byte of response message data to the Tx ring buffer
void putTxRingBfrData( U8 data )
{
	// Store the data to be sent
	sc_tx_ring_bfr.buf[ sc_tx_ring_bfr.tail++ ] = data;

	// Check/handle wraparound
	if( sc_tx_ring_bfr.tail >= TX_MSG_BUFFER_SIZE_BYTES )
	{
		sc_tx_ring_bfr.tail = 0;
	}
}

//////////////////////////////////////////////////
//
//    void reset_usart_rx(void)
//
//    This function momentarily disables then re-
//    enables the USART receiver. This clears any
//    received data or USART rx error flags.
//
//////////////////////////////////////////////////
static void reset_usart_rx(void)
{
}

/////////////////////////////////////////////
//  Gets oldest rx data out of ring buffer
//	 and stores at *bfr.
//
//	Returns SC_EMPTY if buffer underrun,
//	else returns 0
//////////////////////////////////////////
static int getRxRingBfrData(U8 *bfr)
{
	int c;

	// check for empty ring buffer
	if( sc_rx_ring_bfr.tail == sc_rx_ring_bfr.head )
	{
		// It's empty
		c = SC_EMPTY;
	}
	else
	{
		// get the oldest data
		*bfr = sc_rx_ring_bfr.buf[ sc_rx_ring_bfr.head++ ];

		// check/handle wraparound
		if( sc_rx_ring_bfr.head >= RX_MSG_BUFFER_SIZE_BYTES )
		{
			sc_rx_ring_bfr.head = 0;
		}

		c = 0;
	}

	return c;
}

//////////////////////////////////////
//
//	void putRxData(U8 c)
//
//	Stores character in ring buffer
//
/////////////////////////////////////
void putRxData(U8 c)
{
	// store the data
	sc_rx_ring_bfr.buf[ sc_rx_ring_bfr.tail++ ] = c;
	
	// check/handle wraparound
	if( sc_rx_ring_bfr.tail >= RX_MSG_BUFFER_SIZE_BYTES )
	{
		sc_rx_ring_bfr.tail = 0;
	}
}

///////////////////////////////////////////////
//	Initialize the serial communications task
///////////////////////////////////////////////
void SerialCommInit(void)
{
	// initialize rx ring buffer as empty
	memset( (void *) &sc_rx_ring_bfr.buf[0], 0, RX_MSG_BUFFER_SIZE_BYTES);
	sc_rx_ring_bfr.head = 0;
	sc_rx_ring_bfr.tail = 0;

   sc_tx_ring_bfr.head = 0;
	sc_tx_ring_bfr.tail = 0;

	sc_msglen = 0;

	sc_state = SC_RX_IDLE_e;
}

///////////////////////////////////////////////
//	Serial Communications Task - polls USART
//	for I/O when sending or receiving.
///////////////////////////////////////////////
void SerialCommTask( void *pvParameters )
{
	U8 c;
	U8 i;

   // Initialize UART for 1200 8-N-1 
   UARTInit( 1200 );

   while(1)
   {
      // Feed the WDT to prevent timeout and RESET
      Feed_Watchdog_Timer();

      switch( sc_state )
      {
         case SC_RX_IDLE_e:

            // if char avail, see if it's a DLE
            if( bRxDataAvail() && ((c = getRxChar()) == DLE) )
            {
               sc_state = SC_RX_WAIT_SOH_e;
            }
            break;

         case SC_RX_WAIT_SOH_e:

            // if char avail, see if it's a SOH
            if( bRxDataAvail() )
            {
               // Get the data
               c = getRxChar();

               if ( c == SOH )
               {
                  ////////////////////////////////
                  //	DLE SOH header was received
                  ////////////////////////////////
                  sc_msglen = 0;		// reset message length
					
                  // Now get the message body
                  sc_state = SC_RX_MSG_BODY_e;
               }
               else if ( c != DLE )
               {
                  sc_state = SC_RX_IDLE_e;
               }
            }
            break;

         case SC_RX_MSG_BODY_e:

            // if char avail, get it 
            if( bRxDataAvail() )
            {
               ///////////////////
               //	Get the data.
               ///////////////////
               c = getRxChar();

               // Was the character a DLE?
               if( c == DLE )
               {
                  // yes, it was a DLE
                  sc_state = SC_RX_MSG_BODY_DLE_e;
               }
               else
               {
                  if( sc_msglen < MAX_SC_MSG_LEN )
                  {
                     // then store the message data byte
                     putRxData(c);

                     // bump the length
                     sc_msglen++;
                  }
                  else
                  {
                     // Message must be invalid since it's too long
                     Decoder_stats.rx_long_msgs++;

                     ////////////////////////////////////////////
                     //	Reinitialize to receive other messages
                     ////////////////////////////////////////////
                     SerialCommInit();
                  }
               }
            }
            break;

         case SC_RX_MSG_BODY_DLE_e:
		
            ///////////////////////////////////
            //	Previous rx char was DLE (not 
            //	stored or included in the CRC
            ///////////////////////////////////

            // if char avail, get it 
            if( bRxDataAvail() )
            {
               ///////////////////
               //	Get the data.
               ///////////////////
               c = getRxChar();

               if( c == SYN )
               {
                  /////////////////////////////////////////////////////
                  // Unstuff: treat DLE SYN combo as just DLE received
                  /////////////////////////////////////////////////////

                  if( sc_msglen < MAX_SC_MSG_LEN )
                  {
                     // Store a DLE (for DLE SYN received)
                     putRxData(DLE);

                     // bump the length
                     sc_msglen++;

                     // Go back to looking for a DLE
                     sc_state = SC_RX_MSG_BODY_e;
                  }
                  else
                  {
                     // Message must be invalid since it's too long!
                     Decoder_stats.rx_long_msgs++;


                     ////////////////////////////////////////////
                     //	Reinitialize to receive other messages
                     ////////////////////////////////////////////
                     SerialCommInit();
                  }
               }
               else if ( c == EOT )
               {
                  ///////////////////////////////////
                  //	Previous rx char was DLE (not 
                  //	stored or included in the CRC
                  ///////////////////////////////////

                  // We got DLE EOT (message terminator)
                  sc_state = SC_RX_MSG_PROC_e;
               }
            }
            break;

         case SC_RX_MSG_PROC_e:

            ////////////////////////////////////////////////////////////////////////////
            //	We should now have a message body (including the 16-bit CRC16 received)
            //	in the ring buffer starting at the head pointer buffer location.
            ////////////////////////////////////////////////////////////////////////////

            /////////////////////////////////////////////////////////////
            //	Move the message from the ring buffer into the linear 
            // 	message processing buffer to make its processing easier.
            /////////////////////////////////////////////////////////////
            for( i = 0; i < sc_msglen; i++ )
            {
               if( (getRxRingBfrData( &sc_msg_bfr[i])) == SC_EMPTY)
               {
                  Fatal_Error( SC_RING_BUFFER_ERROR );	// no return
               }
            }
			
            Decoder_stats.rx_msgs++;

            //////////////////////////////////////////////
            // 	Calculate the CRC16 on the message bytes
            //	up to the transmitted CRC16 bytes.
            //////////////////////////////////////////////
            crc16 = 0xffff;		

            for( i = 0; i < sc_msglen - 2; i++ )
            {
               crc16 = _crc16_update( crc16, sc_msg_bfr[i] );
            }

            ///////////////////////////////////////////////////////////////////////
            //	The message of length "sc_msglen" is now stored in sc_msg_bfr
            //  The 2-byte CRC16 value should start at sc_msg_bfr[ sc_msglen - 2 ]
            ///////////////////////////////////////////////////////////////////////

            // Fetch the transmitted CRC16 value
            rx_crc = sc_msg_bfr[ sc_msglen - 1 ] | (sc_msg_bfr[ sc_msglen - 2 ] << 8);

            // Compare it against the computed CRC16
            if( rx_crc == crc16 )
            {
               ///////////////////////////////////////////////////////////////
               // The received message has a valid CRC16 and is OK to decode
               ///////////////////////////////////////////////////////////////
               process_sc_msg();
            }
            else
            {
               // Bad crc16
               Decoder_stats.rx_crc_errs++;

               ////////////////////////////////////////////
               //	Reinitialize to receive other messages
               ////////////////////////////////////////////
               SerialCommInit();
            }
            break;

         ///////////////////////////////////////////////////////////////////
         //	Transmit states
         //
         //	Note that the response message data in the tx ring buffer has 
         //	already had byte-stuffing performed and the transmit state
         //	machine can simply transmit all tx ring buffer data bytes
         //	verbatim.
         ///////////////////////////////////////////////////////////////////

         case SC_TX_MARKING_PRE_e:

            //////////////////////////////////////////////////
            //	H-Bridge assumed to be in tri-state (off)
            //	mode when entering this state.
            //
            //	Purpose: Put 2-wire in MARKING state prior 
            //	to sending data
            //////////////////////////////////////////////////

            HBridgeOut( TRI, MRK );    // Make Tri-state to Mark transition
		
            // Allow MARK for a short time (configured by #define in Calsense_2_Wire_Decoder.h)
            vTaskDelay( MS_to_TICKS( TX_MARKING_LINE_DURATION_MS ) );

            // then allow USART to transmit data in tx ring buffer
            sc_state = SC_TX_SENDING_MSG_e;
            break;

         case SC_TX_SENDING_MSG_e:	// USART data is being sent
			
            // is there data in the Tx ring buffer to send?
            if( bTxRingBfrDataAvail() )
            {
               // There is ring buffer data to transmit. But is the USART ready to transmit?
               if( bUsartTxRdy() )
               {
                  // Yes, get the data byte to send
                  c = getTxRingBfrData();

                  // And send it
                  UsartTx(c);
                  chars_loaded++;
               }
            }
            else
            {
               // Is the USART done transmitting?
               if( bUsartTxEmpty() )
               {
                  // Go to state to handle return to tri-state on network
                  sc_state = SC_TX_MARKING_POST_e;
               }
            }
            break;

         case SC_TX_SENDING_ACK_PULSE_e:	
			
            //////////////////////////////////////////
            //	Send an ACK Pulse using the H-Bridge
            //////////////////////////////////////////

            // Call H-Bridge output functions to handle ACK pulse
            send_ACK_pulse_response();

            ///////////////////////////////
            //	Return to receive mode
            ///////////////////////////////
            SerialCommInit();
            break;

         case SC_TX_MARKING_POST_e:	// 2-wire at MARKING state after USART data sent
					
            /////////////////////////////////////////////////////////////////////
            //	Return the 2-wire network to tri-state after a brief delay
            // The delay is configured by #define in Calsense_2_Wire_Decoder.h)
            /////////////////////////////////////////////////////////////////////
            vTaskDelay( MS_to_TICKS( TX_MARKING_LINE_DURATION_MS ) );
            HBridgeOut( MRK, TRI );

            if( bDecoderResetPending == FALSE )
            {
               // Normal action
               sc_state = SC_SOL_TURN_ON_e;
            }
            else
            {  
               // We are handling a DECODER_RST command
               vTaskDelay( MS_to_TICKS(50) );

               // Turn off interrupts
               portENTER_CRITICAL();

               ///////////////////////////////////////////////////////////
               // Reset the processor using the LPC11E14 Application
               // Interrupt and Reset Control Register (AIRCR), in the
               // System Control Block (SCB).
               // NOTE: There is no CMSIS support for this.
               ///////////////////////////////////////////////////////////
               *(SCB_AIRCR_REG_PTR) = (AIRCR_VECTKEY | AIRCR_SYSRESET_REQ);
            }
            break;

         case SC_SOL_TURN_ON_e:		// Check/handle solenoid turn-on
			
            if( bSolEnaTmp[ SOL1 ] )
            {
               bSolEna[ SOL1 ]    = TRUE;	
               bSolEnaTmp[ SOL1 ] = FALSE;
            }
            else if( bSolEnaTmp[ SOL2 ] )
            {
               bSolEna[ SOL2 ]    = TRUE;	
               bSolEnaTmp[ SOL2 ] = FALSE;
            }
			
            ///////////////////////////////
            //	Return to receive mode
            ///////////////////////////////

            // Normal action performed
            SerialCommInit();
            break;

         default:
            break;
      }

      // Allow round robin task switching if PREEMPTION is not enabled
      vTaskDelay( MS_to_TICKS( 1 ) );
   }
}