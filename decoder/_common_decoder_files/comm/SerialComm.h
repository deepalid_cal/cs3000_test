////////////////////////////////////////////////////////////////
//
//	File:	SerialComm.h
//
// Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////
//
//	SERIAL COMM (SC) STATE MACHINE STATES
//
////////////////////////////////////////////
typedef enum
{
	////////////////////
	// Receive states
	////////////////////
	SC_RX_IDLE_e				= 0,	// Looking for a start of message (DLE)
	SC_RX_WAIT_SOH_e			= 1,	// Got DLE, now looking for SOH
	SC_RX_MSG_BODY_e			= 2,	// Got DLE SOH, now handling msg body (looking for DLE)
	SC_RX_MSG_BODY_DLE_e		= 3,	// Got DLE, now looking for SYN or EOT
	SC_RX_MSG_PROC_e			= 4,	// Check the message (CRC check/decode)

	/////////////////////
	//	Transmit states
	/////////////////////
	SC_TX_MARKING_PRE_e			= 5,	// 2-wire at MARKING state prior to sending data
	SC_TX_SENDING_MSG_e			= 6,	// USART data is being sent
	SC_TX_SENDING_ACK_PULSE_e	= 7,	// ACK pulse for ENQuire message is being sent
	SC_TX_MARKING_POST_e       = 8,	// 2-wire at MARKING state after USART data or ACK sent
	SC_SOL_TURN_ON_e           = 9,	// Process solenoid turn on command

} SC_STATES_e;

// Rx ring buffer size (must hold at least MAX_SC_MSG_LEN)
#define RX_MSG_BUFFER_SIZE_BYTES	100

// Rx message processing buffer size
#define MAX_SC_MSG_LEN				80

//////////////////////////////////////////////
//	SC Receive message ring buffer structure
//////////////////////////////////////////////
typedef struct
{

	U8 buf[ RX_MSG_BUFFER_SIZE_BYTES ];
	U8 head;		// index for data removal
	U8 tail;		// index for data insertion

} SC_RX_RING_BFR_s;

// Tx ring buffer size 
#define TX_MSG_BUFFER_SIZE_BYTES	128

//////////////////////////////////////////////
//	SC Receive message ring buffer structure
//////////////////////////////////////////////
typedef struct
{

	U8 buf[ TX_MSG_BUFFER_SIZE_BYTES ];
	U8 head;		// index for data removal
	U8 tail;		// index for data insertion

} SC_TX_RING_BFR_s;

// Empty ring buffer status indicator
#define SC_EMPTY		TRUE

////////////////////////
//	SC GLOBAL VARS
////////////////////////
extern U8 sc_msg_bfr[ MAX_SC_MSG_LEN ];
extern U8 sc_msglen;
extern SC_STATES_e sc_state;

// Function Prototypes
extern void putTxRingBfrData( U8 data );
extern void send_ack_pulse(void);
extern void start_2_wire_transmit(void);
extern void SerialCommInit(void);
extern void init_usart( void );
extern void SerialCommTask( void *pvParameters );

