/////////////////////////////////////////////
//
//    File: adc.c
//
//    Functions for LPC11E14 ADC
//
////////////////////////////////////////////
#include "LPC11Exx.h"
#include "LPC1000.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"

#include "adc.h"

// The setup value for the ADC control register
static uint32_t adc_cr;

///////////////////////////////////////////////
//
//    void init_adc( uint32_t Pclk_Hz )
//
//    Pclk: CPU clock speed in Hz
//
//    This function initializes the ADC for 
//    10-bit conversions. The ADC is clocked
//    at 4 MHz.
///////////////////////////////////////////////
void init_adc( uint32_t Pclk_Hz )
{
   uint32_t div;

   div = 0;

   // Find PCLK divisor that makes ADC clock < 4.5 MHz
   while( Pclk_Hz / (div + 1) >= 4500000 )
   {
      div++;
   }

   // Initialize ADC for 10-bit conversions
   adc_cr = CR_CLKDIV(div) | CR_11_CLKS_10_BITS;

   LPC_ADC->CR = adc_cr;

   // Allow any ADC conversion complete to set DONE bit
   LPC_ADC->INTEN = 1<<8;     // Set ADGINTEN bit
}

////////////////////////////////////////////////////
//
//	uint32_t read_adc_ch(uint32_t ch)
//
//	ch:   ADC channel # (0 to 3)
//
//	This function does an ADC conversion on the
//  specified ADC channel and returns the result.
//
//	A conversion takes 11 clock cycles @ 4 MHz
//	(2.75 microseconds)
// 
//	This function was modified on 08/15/2013
//	to optimize the function. A change to the test 
// for DONE was needed to keep from reading the
// result of a previous conversion.

// 8/1/2014 rmd : This indeed is the finalized adc function Dave and I came up with after
// much experimentation. It is DAVE2 as referenced at the end of this file.

////////////////////////////////////////////////////
uint32_t read_adc_ch(uint32_t ch)
{
   uint32_t gdr_value;

   // Clear start and select the ADC channel
   LPC_ADC->CR = adc_cr | CR_SEL_CH( ch ) ;

   // Start the conversion
   LPC_ADC->CR = adc_cr | CR_SEL_CH( ch ) | CR_START_NOW;

   // Wait for DONE bit in global data register
   do
   {
      gdr_value = LPC_ADC->GDR;
   } while( !(gdr_value & GDR_DONE ));

   return( GDR_V_VREF(gdr_value) );
}

// ----------

/*
#if (ADC_FUNC == ADC_FUNC_DAVE0)
#warning OLD BAD ADC CODE IS BEING COMPILED
////////////////////////////////////////////////////
//
//    uint32_t read_adc_ch(uint32_t ch)
//
//    ch:   ADC channel # (0 to 3)
//
//    This function does an ADC conversion on the
//    specified ADC channel and returns the result.
//
//    A conversion takes 11 clock cycles @ 4 MHz
//    (2.75 microseconds)
////////////////////////////////////////////////////
uint32_t read_adc_ch(uint32_t ch)
{
   uint32_t gdr_value;
   uint32_t cr_value;

   // Clear START and SEL bits
   LPC_ADC->CR = adc_cr;

   cr_value = (CR_SEL_CH( ch ) | CR_START_NOW);

   // Select the channel and start the conversion
   LPC_ADC->CR = cr_value;

   // Wait for ADINT bit to be set
   while( !(LPC_ADC->STAT & (1<<16) ) );

   // Read the GDR register
   gdr_value = LPC_ADC->GDR;
   
   return( GDR_V_VREF(gdr_value) );
}
#elif (ADC_FUNC == ADC_FUNC_DAVE1)
#warning Using revised ADC function

////////////////////////////////////////////////////
//
//    uint32_t read_adc_ch(uint32_t ch)
//
//    ch:   ADC channel # (0 to 3)
//
//    This function does an ADC conversion on the
//    specified ADC channel and returns the result.
//
//    A conversion takes 11 clock cycles @ 4 MHz
//    (2.75 microseconds)
////////////////////////////////////////////////////
uint32_t read_adc_ch(uint32_t ch)
{
   uint32_t gdr_value;

   // Clear START and SEL bits
   LPC_ADC->CR = adc_cr;

   // Select the ADC channel
   LPC_ADC->CR = adc_cr | CR_SEL_CH( ch ) ;

   // Start the conversion
   LPC_ADC->CR = adc_cr | CR_SEL_CH( ch ) | CR_START_NOW;

   // Wait for ADINT bit to be set
   while( !(LPC_ADC->STAT & (1<<16) ) );

   // Read the GDR register
   gdr_value = LPC_ADC->GDR;
   
   return( GDR_V_VREF(gdr_value) );
}
#elif (ADC_FUNC == ADC_FUNC_DAVE2)
#warning Dave2 Optimized ADC function is being used.
////////////////////////////////////////////////////
//
//	uint32_t read_adc_ch(uint32_t ch)
//
//	ch:   ADC channel # (0 to 3)
//
//	This function does an ADC conversion on the
//  specified ADC channel and returns the result.
//
//	A conversion takes 11 clock cycles @ 4 MHz
//	(2.75 microseconds)
// 
//	This function was modified on 08/15/2013
//	to optimize the function. A change to the test 
// for DONE was needed to keep from reading the
// result of a previous conversion.
////////////////////////////////////////////////////
uint32_t read_adc_ch(uint32_t ch)
{
   uint32_t gdr_value;

   // Clear start and select the ADC channel
   LPC_ADC->CR = adc_cr | CR_SEL_CH( ch ) ;

   // Start the conversion
   LPC_ADC->CR = adc_cr | CR_SEL_CH( ch ) | CR_START_NOW;

   // Wait for DONE bit in global data register
   do
   {
      gdr_value = LPC_ADC->GDR;
   } while( !(gdr_value & GDR_DONE ));

   return( GDR_V_VREF(gdr_value) );
}
#elif (ADC_FUNC == ADC_FUNC_BOB)
#warning BOB ADC Code used

uint32_t read_adc_ch(uint32_t ch)
{
	// START OF BOBS CODE.
	
	uint32_t	result;

	// 8/14/2013 rmd : Clear previous channel select.
	adc_cr &= 0xFFFFFF00;
	
	// 8/14/2013 rmd : Select the next channel.
	adc_cr |= CR_SEL_CH( ch );
	
	// 8/14/2013 rmd : Now write it to the register. The START bit is not set at this point in
	// adc_cr. Which matches the start of the CR register start bit. As we cleared it at the
	// conclusion of the prevous conversion.
	LPC_ADC->CR = adc_cr;
	
	LPC_ADC->CR = (adc_cr | CR_START_NOW);

	do
	{
		// 8/14/2013 rmd : Read result and completion status from the specific channel data
		// register.
		result = LPC_ADC->DR[ ch ];

	} while( !(result & 0x80000000) );


	// 8/14/2013 rmd : Stop the AD. Leave channel MUX at last setting. This also sets CR up for
	// the next time we cycle through this function and change the channel. The START bit is
	// already cleared.
	LPC_ADC->CR &= 0xF8FFFFFF;
	
	return( GDR_V_VREF(result) );
}
#endif

*/


