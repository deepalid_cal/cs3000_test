/////////////////////////////////////////////
//
//    File: adc.h
//
//    Header file for LPC11E14 ADC
//
////////////////////////////////////////////

////////////////////////////////////////////////////////
// ADC Control register (CR) bit assignments and masks
////////////////////////////////////////////////////////

// SEL field: channel selection, bits 7:0
#define CR_SEL_CH(ch)   (1<<ch)
#define CR_SEL_CH_0     0x01
#define CR_SEL_CH_1     0x02
#define CR_SEL_CH_2     0x04
#define CR_SEL_CH_3     0x08
#define CR_SEL_CH_4     0x10
#define CR_SEL_CH_5     0x20
#define CR_SEL_CH_6     0x40
#define CR_SEL_CH_7     0x80

// The APB clock (PCLK) is divided by n+1 to produce the ADC clock.
// The ADC clock must be <= 4.5 MHz, so a good value for n is 2 when
// a 12 MHz PCLK is used. That produces an ADC clock of 4 MHz.
// CLKDIV field: bits 15:8 (7-bits)
#define  CR_CLKDIV(n)      (n<<8)

// BURST field: Selects Burst mode, bit 16
#define CR_BURST_MODE      (1<<16)

// CLKS field: Clocks and accuracy selection, bits 19:17
#define CR_11_CLKS_10_BITS (0)
#define CR_10_CLKS_9_BITS  (1<<17)
#define CR_9_CLKS_8_BITS   (2<<17)
#define CR_8_CLKS_7_BITS   (3<<17)
#define CR_7_CLKS_6_BITS   (4<<17)
#define CR_6_CLKS_5_BITS   (5<<17)
#define CR_5_CLKS_4_BITS   (6<<17)
#define CR_4_CLKS_3_BITS   (7<<17)

// START field, bits 26:24
#define CR_NO_START        (0)
#define CR_START_NOW       (1<<24)

///////////////////////////////
// Global Data Register (GDR)
///////////////////////////////

// Returns the V_VREF ADC reading from a 32-bit value read from the GDR
#define GDR_V_VREF(gdr_val)   ((gdr_val>>6) & 0x3ff)
#define GDR_CHN(gdr_val)      ((gdr_val>>24) & 0x7)

// Bit masks
#define GDR_OVERRUN           (1<<30)
#define GDR_DONE              (1<<31)

// Function declarations
extern void init_adc( uint32_t Pclk_Hz );
extern uint32_t read_adc_ch(uint32_t ch);