/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_DECODER_EEPROM_H
#define _INC_DECODER_EEPROM_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/////////////////////////////////////////////////////
//
//	File: eeprom.h
//
//	Author: David K. Squires
//			Squires Engineering, Inc.
//			for Calsense, Carlsbad, CA
//
/////////////////////////////////////////////////////

/////////////////////////////////////////////////
//	DEFAULT PARAMETER VALUES ARE ASSIGNED/SET
//	USING THE #DEFINES BELOW HERE 
/////////////////////////////////////////////////

// Solenoid PWM Duty Cycles to use
#define SOL_STNDBY_DUTY_CYCLE       20

// The duty cycle used for the test PWM cycle
#define SOL_TEST_DUTY_CYCLE         15

// SolMaxHoldDC
#define SOL_HOLD_MAX_DUTY_CYCLE		30

// SolMinHoldDC
#define SOL_HOLD_MIN_DUTY_CYCLE		10

// Number of timer counts for Solenoid PWM period of 4 mS (250 Hz)
// Do not change this: this sets PWM cycle period.
#define PWM_DIV64_250_HZ_TOP        500

////////////////////////////////////////////////////////////////
//
//	ADC input scaling:
//
//	ADC input voltage divider: 100K over 5.62K = .05321 which
//	means (3.3V/1024)/.05321 = 60.56 mV per count.
//
// Historical note: The ATmega168PA design had 58.6 mV per count.
////////////////////////////////////////////////////////////////

// ----------

// MIN VDC for pull-in mode is 25.0 VDC = 412 ADC counts
//#define PULL_IN_MODE_MIN_CAP_ADC_CNTS			412

// 6/6/2013 rmd : As you can see Dave had this set to 412 counts or about 25V. At the end of
// 10,000 feet of cable the 6th valve may or may not turn ON as the working solenoid cap
// voltage drops to right around 22.5 volts. I'm going to lower this limit to roughly 22
// volts (363 counts). The risk in doing so is that we think the solenoid has pulled in and
// the valve has opened when in fact the voltage was not enough. However all the valves I've
// tested so far all pull-in at this voltage (Rainbird, Hunter, Irritrol, Toro).
// Additionally I have tested at the end of 150 feet of solenoid wire with the 6th valve ON
// at the end of 9000 feet of wire. I know this valve is right at the PULL IN voltage limit
// specified here. Sometimes it passes , sometimes not. And the solenoid sucessfully pulled
// in each time I've tried (several dozen times).
//
// 10/18/2013 rmd : I have now lowered this to 20VDC (325 counts). We were having trouble
// meeting spec. with a fully loaded cable (70 decoders, 7000 feet, with 6 generic Irritrol
// solenoids at the end of the cable). The 6th solenoid mostly won't attempt a turn ON.
// Doesn't get past the pull-in cap voltage test. At a setting of 325 it does turn ON.
#define PULL_IN_MODE_MIN_CAP_ADC_CNTS			325

// ----------

// MIN VDC for hold mode is 10.0 VDC = 165 ADC counts
#define HOLD_MODE_MIN_CAP_ADC_CNTS				165

// ADC delta V Limits for bad solenoid checks
#define HOLD_MODE_BAD_DELTA_V_LIMIT_CNTS		13			// Was 10

// ----------

//////////////////////////////////////////
//	Solenoid Capacitor Charge time - ms.
//////////////////////////////////////////

// 6/7/2013 rmd : Note - this is assigned to an 8-bit value and therefore cannot exceed 255.
#define SOL_CAP_CHG_TIME_MS						250     // was 150

// ----------

/////////////////////////////////////////
//	Time allowed for solenoid to pull in
/////////////////////////////////////////
#define SOL_PULL_IN_TIME_MS						50

///////////////////////////////////////////////////////
// Solenoid Duty Cycle Lookup Table defines
//
//	Tables 0 - 2 are standard, table 3 is programmable
///////////////////////////////////////////////////////

// The number of intervals (steps) in each table
#define PWM_TABLE_STEPS                      8

// The number of Standard Lookup tables
#define N_PWM_TABLES                         3

#define DEFAULT_PWM_LOOKUP_TABLE_NUM         0

//////////////////////////////////////////
//	Solenoid current-related structures
//////////////////////////////////////////

//	The # of PWM cycles to allow the solenoid cap to recharge after 
//	performing a current measurement
#define PWM_CYCLES_FOR_SOL_CAP_CHG           4

//	The min. dV to use for a solenoid current measurement
// This corresponds to a dV of 6.45 volts
#define SOL_CUR_MEAS_MIN_DV_ADC_CNTS         106

////////////////////////////////////////////////////////////////////////////////
//	The max. duty cycle percent accumulation for a solenoid current measurement
//	NOTE: Every 100 counts is one PWM period = 4.00 ms., so this
//	total elapsed time is 2000/100 x 4 ms. = 80 ms.
////////////////////////////////////////////////////////////////////////////////
#define SOL_CUR_MEAS_MAX_DUTY_CYCLE_SUM		2000

/////////////////////////////////////////
//	DECODER SERIAL NUMBER
//
//  Must be at least 20 bits to support
//  1 million addressable decoders.
/////////////////////////////////////////
#define EEPROM_SN_ADDR_BYTES                 3

typedef union
{
	U32 sn32;
	U8 sn8[ EEPROM_SN_ADDR_BYTES ];
} SER_NUM_u;

typedef struct
{
   SER_NUM_u ser_num_u;       // 20-bit serial number plus 4-pad bits
   SER_NUM_u ser_num_u_not;   // Complement of serial number 
} SER_NUM_s;

//	Initial fault state of a solenoid
#define SOL_FAILURE_STATUS		FALSE

//////////////////////////////////
//	DECODER STATISTICS COUNTERS
//////////////////////////////////
typedef struct
{
   U16 temp_maximum;
   U16 temp_current;
   U8 por_resets;
	U8 wdt_resets;
	U8 bod_resets;
	U8 sol_1_ucos;
	U8 sol_2_ucos;
	U8 eep_crc_err_com_parms;
	U8 eep_crc_err_sol1_parms;
	U8 eep_crc_err_sol2_parms;
	U8 eep_crc_err_stats;
	U8 rx_msgs;
	U8 rx_long_msgs;
	U8 rx_crc_errs;
	U8 rx_disc_rst_msgs;
	U8 rx_enq_msgs;
	U8 rx_disc_conf_msgs;
	U8 rx_id_req_msgs;
	U8 rx_dec_rst_msgs;
	U8 rx_data_lpbk_msgs;
	U8 rx_put_parms_msgs;
	U8 rx_get_parms_msgs;
	U8 rx_sol_cur_meas_req_msgs;
	U8 rx_stat_req_msgs;
	U8 rx_stats_req_msgs;
	U8 rx_sol_ctl_msgs;
	U8 tx_acks_sent;

} __attribute__ (( packed)) DECODER_STATS_s;

///////////////////////////////////////////////////
//	EEPROM DATA NOT SUBJECT TO FIELD-MODIFICATION
///////////////////////////////////////////////////

/////////////////////////////////////////////////////
//	EEPROM PARAMETERS SUBJECT TO FIELD-MODIFICATION
//
//	IMPORTANT: KEEP U16 vars on 16-bit alignment
/////////////////////////////////////////////////////

////////////////////////////////////
//	These parameters are common to 
//	both solenoid channels
////////////////////////////////////
typedef struct
{
	U16	PwmTopCnts;										// # TC1 counts for PWM cycle
	U16	SolCurMeasMaxDCSum;							// PWM duty cycle accum value beyond which cur. meas. is complete
	U8 	SolCapChgPwmCyc;								// # PWM cycles to allow for solenoid cap to recharge after cur. meas.
	U8		SolCurMeasMinDvAdcCnts;						// Min ADC counts for dV for solenoid current measurement
	BOOL	bDiscovered;									// Discovered Flag (non-volatile)
	U8		SolCapChgTime_ms;								// Solenoid cap. charge time in ms.
	U8 	SolStndbyDC;									// Standby duty cycle %
	U8		SolTblLookupDC[ N_PWM_TABLES ][ PWM_TABLE_STEPS ];	// Standard solenoid PWM duty cycle lookup tables #0 - #2

} __attribute__ (( packed)) DECODER_COMMON_PARMS_s;

///////////////////////////////////////////
//	These parameters can be independently
//	set for each solenoid channel
///////////////////////////////////////////
typedef struct
{
	U16	MinHbrgCapPullInAdcCnts;            // Min. ADC cnts for H-bridge cap (pull-in mode)
	U16	MinSolCapHoldAdcCnts;               // Min. ADC cnts for sol cap (hold mode)
	U16	HoldBadDeltaVcntsMax;               // Max. ADC count delta (hold)
	BOOL	bSolFailed;                         // Solenoid is in failed state
	U8 	SolTestDC;                          // Test duty cycle %
	U8		SolMaxHoldDC;                       // Max. Hold duty cycle %
	U8		SolMinHoldDC;                       // Min. Hold duty cycle %
	U8		SolPullInTime_ms;                   // Solenoid pull-in time - ms.
	U8		ActiveSolPwmLookupTable;            // Solenoid PWM duty cycle lookup to use
	U8		SolTblLookupDC[ PWM_TABLE_STEPS ];	// Custom PWM DC lookup table (table #3)

} __attribute__ (( packed)) DECODER_PARMS_s;

////////////////////////////////////////////
//	OVERALL STRUCTURE OF EEPROM CONTENTS
////////////////////////////////////////////

typedef struct
{
   // Two copies of the decoder's unique 20-bit Serial Number and check word
   SER_NUM_s SerialNumber[2];

	//////////////////////////////////////////////
	// These are read into RAM at every power-up
	//////////////////////////////////////////////
	DECODER_COMMON_PARMS_s	CommonParms;		// Field modifiable
	DECODER_PARMS_s			Sol1Parms;			// Field modifiable
	DECODER_PARMS_s			Sol2Parms;			// Field modifiable
	DECODER_STATS_s			Decoder_stats;		// Updated while operating

	U16 					Crc16_common_parms;     // Updated in field
	U16 					Crc16_sol1_parms;       // Updated in field
	U16 					Crc16_sol2_parms;       // Updated in field
	U16					Crc16_stats;            // Updated in field

} EEPROM_CONTENTS_s;

///////////////////////////////////////////////
//	These defaults are stored in FLASH Memory
///////////////////////////////////////////////
typedef struct
{
	//////////////////////////////////////////////
	//	These are read only if default values
	//	need to be reestablished for one or more
	//	field-modifiable parameters
	//////////////////////////////////////////////

	DECODER_COMMON_PARMS_s	DfltCommonParms;	// Not field modifiable
	DECODER_PARMS_s			DfltSolxParms;		// Not field modifiable

} FLASH_DEFAULTS_s;

// Bob's example PWM duty cycle values		0	1	2	3	4	5	6	7
#define DEFAULT_PWM_DC_TABLE_INITS		{	28, 25, 23, 20, 17, 15, 12, 10 }

////////////////////////////////////////////////
//	The DECODER_COMMON_PARMS_s structure is
//	initialized with this macro so that we can
//	be sure it's done identically for both
//  the eeprom_modifiable_parms and the 
//	eeprom_default_decoder_common_parms.
////////////////////////////////////////////////
#define DECODER_COMMON_PARM_INITS	\
                                    \
		PWM_DIV64_250_HZ_TOP, 					/*	U16	PWM_Top_Cnts;			*/	\
		SOL_CUR_MEAS_MAX_DUTY_CYCLE_SUM,		/*	U16	SolCurMeasMaxDCSum;	*/	\
		PWM_CYCLES_FOR_SOL_CAP_CHG,			/*	U8	SolCapChgPwmCyc;			*/	\
		SOL_CUR_MEAS_MIN_DV_ADC_CNTS, 		/*	U8	SolCurMeasMinDvAdcCnts;	*/	\
		FALSE,                              /*	BOOL bDiscovered;				*/	\
		SOL_CAP_CHG_TIME_MS,                /* U8	SolCapChgTime_ms;       */ \
		SOL_STNDBY_DUTY_CYCLE,					/*	U8 	SolStndbyDC;			*/ \
		{                                                                    \
         DEFAULT_PWM_DC_TABLE_INITS,		/* SolTblLookupDC[ 0 ] [ x ]	*/	\
         DEFAULT_PWM_DC_TABLE_INITS,		/* SolTblLookupDC[ 1 ] [ x ]	*/	\
         DEFAULT_PWM_DC_TABLE_INITS,		/* SolTblLookupDC[ 2 ] [ x ]	*/	\
		}

////////////////////////////////////////////////
//	The DECODER_PARMS_s structures are
//	initialized with this macro so that we can
//	be sure they are done identically for both
//  the eeprom_modifiable_parms and the 
//	eeprom_default_decoder_parms.
////////////////////////////////////////////////
#define DECODER_PARM_INITS	\
                                                                                       \
		PULL_IN_MODE_MIN_CAP_ADC_CNTS,			/*	U16	MinHbrgCapPullInAdcCnts;		*/	\
		HOLD_MODE_MIN_CAP_ADC_CNTS,				/*	U16	MinSolCapHoldAdcCnts;			*/	\
		HOLD_MODE_BAD_DELTA_V_LIMIT_CNTS,		/*	U16	HoldBadDeltaVcntsMax;			*/	\
		SOL_FAILURE_STATUS,                    /* BOOL  bSolFailed;                   */	\
		SOL_TEST_DUTY_CYCLE,                   /*	U8 	SolTestDC;							*/	\
		SOL_HOLD_MAX_DUTY_CYCLE,               /*	U8    SolMaxHoldDC;                 */	\
		SOL_HOLD_MIN_DUTY_CYCLE,               /*	U8    SolMinHoldDC;						*/	\
		SOL_PULL_IN_TIME_MS,                   /*	U8    SolPullInTime_ms;					*/	\
		DEFAULT_PWM_LOOKUP_TABLE_NUM, 			/*	U8    ActiveSolPwmLookupTable;		*/	\
		DEFAULT_PWM_DC_TABLE_INITS             /* U8    SolTblLookupDC[ PWM_TABLE_STEPS ]; 	*/

///////////////////////////////////////////
//	EEPROM ADDRESSES for reading/writing
//	specific EEPROM data structures
///////////////////////////////////////////
#define EEPROM_SER_NUMS_OFFSET      offsetof(EEPROM_CONTENTS_s, SerialNumber[0])
#define EEPROM_COMMON_PARMS_OFFSET	offsetof(EEPROM_CONTENTS_s, CommonParms)
#define EEPROM_SOL1_PARMS_OFFSET    offsetof(EEPROM_CONTENTS_s, Sol1Parms)
#define EEPROM_SOL2_PARMS_OFFSET    offsetof(EEPROM_CONTENTS_s, Sol2Parms)
#define EEPROM_STATS_OFFSET			offsetof(EEPROM_CONTENTS_s, Decoder_stats)

/////////////////////////////////////////
// EEPROM addresses of the CRC16 values
/////////////////////////////////////////
#define EEPROM_CRC16_COMMON_PARMS_OFFSET 	offsetof(EEPROM_CONTENTS_s, Crc16_common_parms)
#define EEPROM_CRC16_SOL1_PARMS_OFFSET		offsetof(EEPROM_CONTENTS_s, Crc16_sol1_parms)
#define EEPROM_CRC16_SOL2_PARMS_OFFSET		offsetof(EEPROM_CONTENTS_s, Crc16_sol2_parms)
#define EEPROM_CRC16_STATS_OFFSET 			offsetof(EEPROM_CONTENTS_s, Crc16_stats)

extern const FLASH_DEFAULTS_s Flash_Defaults;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
