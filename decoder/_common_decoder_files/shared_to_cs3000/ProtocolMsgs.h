/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_PROTOCOL_MSGS_H
#define _INC_PROTOCOL_MSGS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

////////////////////////////////////////////////
//
//	File:	ProtocolMsgs.h
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
////////////////////////////////////////////////



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"data_types.h"

#include	"eeprom.h"


/////////////////////////////////////////////////////////
//
//	Basic command message structure to the decoder:
//
//	DLE SOH OP8 ADDR24 DATA[n] CRC16   DLE EOT
//
//	OP8:	Opcode   (8-bits)
//	ADDR24:	Address (24-bits), 20 bits are used for the decoder serial number
//	DATA[n]	Data (0-n bytes) Message-dependent length
//	CRC16:	CRC16   (16-bits) x^16 + x^15 + x^2 + 1
//
//	The CRC calculation includes OP8 through the last byte of 
//	DATA[n]. These bytes are considered as THE MESSAGE BODY and 
//	are defined with message structures in this header file. The DLE 
//	SOH (or DLE STX) header bytes and CRC16 DLE EOT trailer bytes are
// understood to be present in all cases when messages are sent/received
// over the 2-Wire network.
//
//	The protocol uses SYN character 0x16) stuffing instead of the more 
//	common NUL (0x00) character stuffing.
//
//	ABOUT THE ADDR24 FIELD:
//
//	The ADDR24 field maps to serial numbers of decoders. Each decoder
//	is assigned a unique 20-bit serial number at the factory during
//	production. Although it is anticipated that only 20 bits are needed
//	for serial numbers during the lifetime of the 2-wire products, the
//	protocol, master controller, and decoder actually support a 24-bit
//	serial number.
//
//	ABOUT COMMAND ADDRESSING MODES:
//
//	Commands from the 2-wire master controller can be MULTICAST, BROADCAST,
//	or UNICAST, depending upon which decoders the master controller 
//	intends to act upon the command.
//
//	When a command is to be MULTICAST, or received and acted upon by
//	multiple decoders on the network at once, the ADDR24 bytes are accompanied
//	by a mask bit count field which indicates that an n-bit address field
//	mask is to be applied by the decoder when comparing its serial number
//	to that in the received command. The n-bit mask bit count is defined to 
//	start at serial number bit A20 and additional mask bits are successively
//	less in significance. A response to an MULTICAST commands is limited to 
//	an ACK pulse, which is a short (several milliseconds) positive voltage 
//	pulse on the 2-wire network. Multiple decoders can send an ACK pulse 
//	at the same time without signal contention on the 2-wire network.
//	MULTICAST commands are used only during the decoder discovery process.
//
//	When a command is to be BROADCAST, i.e. addressed to and to be acted upon 
//	by all decoders on the network, special opcodes are used. In these
//	cases decoders receiving such commands will ignore the ADDR24 field altogether
//	and execute the command. Decoders must not respond to BROADCAST commands,
//	due to the risk of response polarity contention on the 2-wire network.
//	Note: In BROADCAST commands, the ADDR24 field will be set to 0xffffff;
//
//	When a command is to be UNICAST, i.e. addressed to and to be acted upon by
//	only a single, specific decoder, there is no mask bit count field included 
//	in the message. In this case, every decoder on the network compares the 
//	ADD24 field in the received message against ALL of the bits of its 
//	unique serial number. Only if a decoder matches all serial number bits
//	shall execute the command and return a response if required (command 
//	dependent).
//
//	ABOUT BYTE ORDERING:
//
//	Multi-byte fields within messages are sent using network (i.e. big-endian)
//	byte-order. That is, values larger than a single byte are sent in the
//	order of most significant byte to least significant byte. The decoder
//	MCU is a little-endian architecture and therefore performs byte-swapping
//	as needed when receiving and transmitting multi-byte fields over the
//	2-Wire network.
//
//	ABOUT BYTE STUFFING AND CRC16 CALCULATIONS:
//
//	On transmission, within the body of a message, when a DLE character is 
//	encountered, a DLE SYN pair is  sent instead. The added SYN is not 
//	included in the calculation of the transmitted CRC16 value.
//
//	For reception, first a DLE SOH sequence must be detected to indicate the 
//	start of a message. Then whenever a DLE SYN pair is received, it is replaced 
//	with single DLE. Any SYN removed is not included in the CRC16 calculation.
//	The message body is terminated with the character sequence: DLE EOT.
//  
///////////////////////////////////////////////////////////////////////////////
//
//	NOTE: In the C structures that appear for message structures in this file, 
//	please note that the requisite leading DLE SOH (or DLE STX for responses) 
//	and trailing CRC16 DLE EOT are not included since they are understood to 
//	always be present.
//
//	This means that response message structures show only the data between 
//	the leading DLE STX and trailing CRC16 DLE EOT
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////
//	Some ASCII characters used by the protocol
////////////////////////////////////////////////
#define DLE				0x10			// Used for all message headers and trailers
#define SOH				0x01			// Used by the 2-wire controller for command message headers
#define STX				0x02			// Used by decoder for response message headers

#define EOT				0x04
#define SYN				0x16
#define ENQ				0x05
#define ACK				0x06
#define NAK				0x15

/////////////////////////////////////////
//	DECODER COMMAND OPCODES
/////////////////////////////////////////

// BROADCAST COMMANDS
#define OP_DISC_RST			0xd0		// Request for all decoders to reset their "discovered" status.

// MULTICAST COMMANDS
#define	OP_ENQ				ENQ			// Discovery phase command for one or more addressed decoders to respond with a special ACK

// UNICAST COMMANDS
#define OP_SOL_CTL			0x11		// Controls a solenoid
#define OP_CUR_MEAS_REQ		0x12		// Requests a current measurement be taken of solenoid currents
#define OP_DATA_LOOPBK		0x30		// Echoes a block of arbitrary data (for testing)
#define OP_PUT_PARMS		0x31		// Sends a set of parameters to the decoder
#define OP_GET_PARMS		0x32		// Retrieves a set of parameters from the decoder
#define OP_STATS_CLEAR		0x33		// Clears statistics values in the decoder
#define OP_SET_SN_REQ		0x34		// CALSENSE PRODUCTION: Assigns a serial number to a decoder

// New message types added for the POC DECODER
#define OP_GET_FLOW_CNTS	0x35		// Get flow sensor pulse counts 

// New message type added for the MOISTURE SENSOR DECODER
#define OP_GET_MOISTURE_DATA	0x36		// Get data from Decagon 5TM/5TE moisture sensor

#define OP_STAT1_REQ		0x53			// Request for decoder status: includes states of solenoids, etc.
#define OP_STAT2_REQ		0x54			// Request for decoder status and solenoid current data
#define OP_STATS_REQ		0x60			// Request for decoder statistics counters
#define OP_ID_REQ			0xdd			// Request for a single decoder to send its identifying information (decoder type, etc.)
#define OP_DISC_CONF		0xdc			// Confirmation message from master that the addressed decoder SN has been discovered and registered
#define OP_DECODER_RST		0xfe			// Forces a reset of the decoder
#define OP_RSTR_DFLTS		0xff			// Restores field-modifiable parameters with factory defaults

// COMMAND MESSAGE SIZE LIMITS

// This applies to OP_DATA_LOOPBK
#define MAX_DATA_BYTES_TO_LOOP			32		

////////////////////////////////
//	COMMAND / RESPONSE SUMMARY:
////////////////////////////////

//	================	=========		=================================
//	OPCODE				CMD TYPE       RESPONSE
//	================	=========		=================================
//	OP_DISC_RST			BROADCAST		none
//	OP_ENQ				MULTICAST		none or ACK (pulse)
// OP_CUR_MEAS_REQ	UNICAST			none or ACK (pulse)
//	OP_ID_REQ			UNICAST			ID response
//	OP_DISC_CONF		UNICAST			ID response
//	OP_STAT1_REQ		UNICAST			basic decoder status
//	OP_STAT2_REQ		UNICAST			decoder status with solenoid current data
//  OP_STATS_REQ		UNICAST			decoder statistics
//	OP_SOL_CTL			UNICAST			command status
//	OP_DECODER_RST		UNICAST			ACK (async message)
//	OP_DATA_LOOPBK		UNICAST			The data byte(s) sent
//	OP_PUT_PARMS		UNICAST			ACK (async message)
//	OP_GET_PARMS		UNICAST			The requested set of parameters
//	OP_CLEAR_STATS		UNICAST			ACK (async message)
//  OP_RSTR_DFLTS     	UNICAST         status byte: bit(s) set if errors restoring region(s)
//  OP_GET_FLOW_CNTS  	UNICAST         32-bit flow sensor counts and 16-bit flow sensor counts
// OP_GET_MOISTURE   UNICAST        Status byte plus sensor data (if valid)

//////////////////////////////////////////////////
//	DECODER RESPONSE STATUS CODES
//
//	These are used in one or more response types.
//////////////////////////////////////////////////
#define RSP_STATUS_OK		0x00		// The command was executed successfully
#define RSP_INVALID_CMD		0xff		// Invalid command data field(s) were detected 

///////////////////////////////////////////////////////////////
//	COMMON 2-WIRE CMD MESSAGE HEADER
//
//  Many commands use only this structure and a unique opcode.
///////////////////////////////////////////////////////////////
typedef struct
{
	U8 opcode;
	U8 sn8[ EEPROM_SN_ADDR_BYTES ];		// Decoder serial number/address
} cmd_msg_hdr_s;

/////////////////////////////////////////////////////////////////////////
//	Command used to request that solenoid current measurements be taken
//
//	The only valid response to this message is an ACK pulse
/////////////////////////////////////////////////////////////////////////
typedef struct
{
	cmd_msg_hdr_s hdr;
	U8 seqnum;

} SOL_CUR_MEAS_REQ_s;

////////////////////////////////////////
//	SIMPLE RESPONSE STRUCTURE
//
//	Many response messages are simple
// and contain only a single byte of 
//	data between the leading DLE STX
//	and trailing CRC16 DLE EOT message
//	bytes. Those responses have a simple
//	structure shown below.
/////////////////////////////////////////
typedef struct
{
	U8  data;

} SIMPLE_RSP_s;

/////////////////////////////////////////////////////////
//	ENQUIRE MESSAGE - used during the Discovery process
/////////////////////////////////////////////////////////
typedef struct
{
	cmd_msg_hdr_s   hdr;		// common 2-wire message header
	U8  mask;						// Address mask (# bits)

} ENQ_MSG_s;

////////////////////////////////////////
//	ENQUIRE MESSAGE RESPONSE
////////////////////////////////////////

// The response to an ENQUIRE message is either:
// a) no response
// b) an ACK (positive) pulse on the 2-wire network for
//	   some number of milliseconds.

////////////////////////////////////////


///////////////////////////////////////////
//	OP_DISC_RST - DISCOVERY RESET MESSAGE
///////////////////////////////////////////

// The command uses the cmd_msg_hdr_s with
//	an opcode field value of OP_DISC_RST

////////////////////////////////////////
//	DISCOVERY RESET RESPONSE
////////////////////////////////////////

//	There is no response to the OP_DISC_RST
//	command message because all decoders
//	on the network will decode and execute 
//	this command concurrently.

///////////////////////////////////////////////////////////////////////
//	SOLENOID CONTROL MESSAGE
//
//	This message can be used to turn on or turn off either solenoid #1
//	or solenoid #2.
//
//	It can also be used to clear the FAULT status for a solenoid. 
///////////////////////////////////////////////////////////////////////

// Mask for solenoid (0 to n) in command's data byte
#define SOL_DATA_SOL_MASK        0xf0

#define SOL_DATA_OP_MASK         0x0f

// Operation field values
#define SOL_CTL_CMD_SOL_OFF      0x00
#define SOL_CTL_CMD_SOL_ON       0x01

typedef struct
{
	cmd_msg_hdr_s   hdr;	// common message header
	U8  data;				// command control data

} SOL_CTL_MSG_s;

////////////////////////////////////////
//	SOLENOID CONTROL MESSAGE RESPONSE
//
//	DLE STX STAT8 CRC16 DEL EOT
////////////////////////////////////////

// Status return codes 

// Invalid solenoid number
#define RSP_SOL_NUMBER_INVALID            0xf0

// Undefined operation specified
#define RSP_SOL_OP_UNDEFINED              0xf1

// Solenoid is already in target state (on)
#define RSP_SOL_IS_ALREADY_ON             0xf2

// Solenoid is already in target state (off)
#define RSP_SOL_IS_ALREADY_OFF            0xf3

// The H-bridge cap voltage is too low to actuate the solenoid
#define RSP_HBRG_VCAP_TOO_LOW             0xf4

////////////////////////////////////////
//	STATUS REQUEST COMMAND
////////////////////////////////////////

//	The status request command uses the 
//	cmd_msg_hdr_s with an opcode value
//	of OP_STAT_REQ.

////////////////////////////////////////
//	RESPONSE to OP_STATx_REQ opcode CMDS
////////////////////////////////////////

//	Status response sys_flag bits
#define SYS_FLAG_POR				0x01	// Pwr-on-reset: appears in first status response after a reset (reported once)
#define SYS_FLAG_DISCOVERED	0x02	// Reflects state of bDiscovered flag (non-volatile)
#define SYS_FLAG_BOR				0x04	// Brown-out reset: appears in first status response after a reset (reported once)
#define SYS_FLAG_WDR				0x08	// WDT-reset: appears in first status response after a reset (reported once)

// These bits are also reported just once
#define SYS_FLAG_EEPROM_CRC_ERR		0x40	// An EEPROM CRC error caused parameters to be restored with defaults
#define SYS_FLAG_SOLENOID_UCOS		0x80	// Uncommanded Change-of-state: an energized solenoid was turned off due to low 2-Wire voltage

#define SYS_FLAGS_REPORTED_ONCE_MASK	(SYS_FLAG_POR|SYS_FLAG_BOR|SYS_FLAG_WDR|SYS_FLAG_EEPROM_CRC_ERR|SYS_FLAG_SOLENOID_UCOS)

// The structure of each channel's solenoid current measurement variables
typedef struct
{
	U8  seqnum;					// measurement sequence #
	U8  dv_adc_cnts;			// total delta V, adc counts
	U16 duty_cycle_acc;			// Duty cycle accumulation

} SOL_CUR_MEAS_s;

///////////////////////////////////////
//
//	Solenoid PWM State Machine States
//
///////////////////////////////////////
typedef enum
{

	SOL_OFF_e             = 0,	  // The solenoid is OFF
	SOL_CAP_CHG_P_e       = 1,	// Charge solenoid cap using 1 ms. pulses every 100 ms.
	SOL_CAP_CHG_C_e       = 2,	  // Charge solenoid cap continuously - solenoid PWM not turned on yet
	SOL_TEST_e            = 3,	  // Single PWM cycle at SOL_TEST_DUTY_CYCLE for solenoid current test
	SOL_PULL_IN_e         = 4,	  // Fixed duty cycle for solenoid pull-in (for SOL_PULL_IN_TIME_MS)
	SOL_HOLD_e            = 5,	  // Variable duty cycle for solenoid holding mode
	SOL_EXCESSIVE_CURRENT_e           = 6,	  // A solenoid failure has been detected

} SOL_PWM_STATES_e;

/////////////////////////////////////
//	Response to OP_STAT1_REQ opcode
/////////////////////////////////////
typedef struct
{
	U8  sol1_status;			// State machine state of solenoid #1
	U8  sol2_status;			// State machine state of solenoid #1
	U8  sys_flags;				// state of decoder system flags

} __attribute__((packed)) STAT1_REQ_RSP_s;

/////////////////////////////////////
//	Response to OP_STAT2_REQ opcode
/////////////////////////////////////
typedef struct
{
	SOL_CUR_MEAS_s sol1_cur_s;	// solenoid 1 seqnum, dV and dt data
	SOL_CUR_MEAS_s sol2_cur_s;	// solenoid 2 seqnum, dV and dt data
	U8  sol1_status;			// State machine state of solenoid #1
	U8  sol2_status;			// State machine state of solenoid #1
	U8  sys_flags;				// state of decoder system flags

} __attribute__((packed)) STAT2_REQ_RSP_s;

#define STATUS_RESP_DUTY_CYCLE_MASK	0xfc00	// Duty cycle (6 MS bits for 0 to 63% duty cycle)
#define STATUS_RESP_DELTA_V_MASK    0x03ff   // Delta V mask for ADC counts

//	region field values
#define COMMON_PARM_REGION	0		// common parameter region
#define SOL1_PARM_REGION	1		// solenoid 1 parameter region
#define SOL2_PARM_REGION	2		// solenoid 2 parameter region
#define STATS_REGION        3       // statistics counters region 

///////////////////////////////////////////////
//	Command structure for OP_GET_PARMS
//
//	Note: The region numbers (0, 1, 2) are 
//	#defined above for OP_WR_PARMS.
///////////////////////////////////////////////
typedef struct
{
	cmd_msg_hdr_s   hdr;		// common command message header
	U8              region;		// parameter region # to send

} OP_GET_PARMS_CMD_s;

///////////////////////////////////////////////
//	Command structure for OP_PUT_PARMS
//
//	Note: The region numbers (0, 1, 2) are 
//	#defined above for OP_WR_PARMS. Note that
// 4 is NOT a valid region to put.
//	The parameter data immediately follows the
//	OP_PUT_PARMS_CMD_s header.
///////////////////////////////////////////////
typedef struct
{
	cmd_msg_hdr_s   hdr;		// common command message header
	U8              region;		// parameter region # to send

} OP_PUT_PARMS_CMD_s;



///////////////////////////////////////////////
//	OP_ID_REQ opcode message
///////////////////////////////////////////////


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/16/2014 rmd : The POC type definitions. Which are a bit tricky but a good understanding
// of when to use them is needed. The TPMICRO related types are used between the decoder and
// main board when main board is building messages to the tpmicro and in the tpmicro code
// itself. There is NO bypass at that level.
//
// The FILE_POC_TYPE defines are the higher level poc type defines. And as you can see
// include a BYPASS type.
//
// They are intentionally valued differently to help identify misuse which may be picked up
// via a range check.

// 5/19/2014 rmd : Note in the decoder the type variable is a single byte. So the values
// used here must not exceed 8-bits and be less than or equal to 0xff.

// 5/19/2014 rmd : Remember at the physical POC level there are only two types of POC. The
// terminal based poc or a decoder based poc. The BYPASS poc is a 'construction' that exists
// at the user 'file' level.

// ----------

#define	POC__TPMICRO_TYPE__TERMINAL				(0x07)

#define	POC__TPMICRO_TYPE__DECODER				(0x08)

#define	POC__TPMICRO_TYPE__MIN					(POC__TPMICRO_TYPE__TERMINAL)
#define	POC__TPMICRO_TYPE__MAX					(POC__TPMICRO_TYPE__DECODER)
#define	POC__TPMICRO_TYPE__DEFAULT				(POC__TPMICRO_TYPE__TERMINAL)

// ----------

// 5/19/2014 rmd : These defines are used in the decoder code itself. And are reported to
// the tpmicro as part of the discovery process in the ID_REQ_RESP. Presently there are only
// two decoders - a 2-station decoder and a poc decoder.
//
// 1/25/2016 rmd : And now there is a third. The moisture decoder.

// 5/19/2014 rmd : This value is VERY CAREFULLY choosen for backwards compatibility to
// existing decoders already programmed and deployed. The original 2-station tyedef is 0x02.
#define	DECODER__TPMICRO_TYPE__2_STATION		(0x02)

#define	DECODER__TPMICRO_TYPE__POC				(POC__TPMICRO_TYPE__DECODER)

#define	DECODER__TPMICRO_TYPE__MOISTURE			(0x03)

// ----------

// 5/19/2014 rmd : In the file the POCs have a new type possible. The BYPASS type. Which is
// made up of up to three POC decoders.

#define	POC__FILE_TYPE__TERMINAL				(0x0a)

#define	POC__FILE_TYPE__DECODER_SINGLE			(0x0b)

#define	POC__FILE_TYPE__DECODER_BYPASS			(0x0c)


#define	POC__FILE_TYPE__MIN						(POC__FILE_TYPE__TERMINAL)
#define	POC__FILE_TYPE__MAX						(POC__FILE_TYPE__DECODER_BYPASS)
#define	POC__FILE_TYPE__DEFAULT					(POC__FILE_TYPE__TERMINAL)

// ----------

// 5/20/2014 ajv : Additional decoder types would add on here. The values choosen MUST not
// be re-uses of any of the other TPMICRO_TYPE or FILE_TYPE family of values you see here.
// In other words 0x02, 0x03, 0x07, 0x08, 0x0a, 0x0b, and 0x0c are already taken. Pick new
// values between 0x01 and 0xff.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

///////////////////////////////////////////////
//	Structure for OP_ID_REQ opcode response
///////////////////////////////////////////////

typedef struct
{
	U8      decoder_type__tpmicro_type;		// Decode type code

	U8      decoder_subtype;				// subtype code

	U16     fw_vers;						// Firmware version

} ID_REQ_RESP_s;


///////////////////////////////////////////////////////////
// Structure for an OP_SET_SN_REQ opcode command message
//
// RESPONSE: Normal response is a simple ACK message
///////////////////////////////////////////////////////////
typedef struct
{
	cmd_msg_hdr_s   hdr;								// Opcode: OP_SET_SN_REQ, SN must be 0x00000!

	U8              sn8[ EEPROM_SN_ADDR_BYTES ];		// serial number to be assigned (high 4 bits are always zero)

	U8              sn8NOT[ EEPROM_SN_ADDR_BYTES ];		// 1's complement of serial number to be assigned (high 4 bits are always 1)

} OP_SET_SN_REQ_s;

///////////////////////////////////////////////
// Structure of an OP_RSTR_DFLTS command
///////////////////////////////////////////////

// regions bit assignments - also apply to SYNC request message
#define EEP_COMMON_PARMS   0x01     // Common parameters
#define EEP_SOL1_PARMS     0x02     // Solenoid 1 parameters
#define EEP_SOL2_PARMS     0x04     // Solenoid 2 parameters

typedef struct
{
	cmd_msg_hdr_s   hdr;		// common command message header

	U8              regions;	// Bitmask of one or more parameter regions to restore

} OP_RSTR_DFLTS_CMD_s;

// The response to the OP_RSTR_DFLTS is a single status byte
// indicating the success of the requested restores. This status byte
// uses the same bit assignments as does the regions field in the command.
// A region that was successfully restored with defaults will have a zero
// bit in its associated bit in the status byte of the response. If an 
// error occurred with the restore operation of a particular region, a one 
// bit will be present instead in the status bit for that region.

///////////////////////////////////////////////////
//	Structure for OP_GET_FLOW_CNTS opcode response
///////////////////////////////////////////////////
typedef struct
{
	// 1/5/2015 rmd : The two 'accumulated' values are used to drive the accumulators.
	U32		accumulated_ms;			// Time (ms.) since last request

	U32		accumulated_pulses;		// Flow sensor counts since last request

	// 5/22/2014 rmd : In the decoder we track the flow over the last 5 seconds as a way of
	// providing the average flow rate over the MOST recent 5 seconds for the user to see. This
	// is done as a way of delivering a visual rate for the user that shows what the average
	// rate is during a recent 5 second window. Instead of during a recent 10 second window. Not
	// sure just how much benefit this is. And surely the 5 second can only give a 'better'
	// reading than the 10 second in the presence of a changing flow rate. But this is the way
	// it is done.
	U16		latest_5_second_count;	// Flow sensor counts over last 5 second window

	// ----------
	
	// 8/3/2015 rmd : NOTE - when we add the BERMAD variable make sure to zero them in the
	// function that handles that for the exisiting variables when we are sending a cable
	// excessive current to the main board!!!!!
	
} __attribute__((packed)) FLOW_COUNT_RESP_s;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

//////////////////////////////////////////////////
// Structure for OP_GET_MOISTURE opcode response
//////////////////////////////////////////////////

// Maximum sensor data length including trailing NULL is 20 bytes (0x14 bytes)
#define MAX_MOISTURE_DATA_STRLEN			(20)

// 1/27/2016 rmd : Status byte bit settings and result length mask. A meaningless INTERNAL
// decoder use only bit. Not useful outside of the decoder.
#define MOISTURE_DATA_STATUS_NONE			(0x80)

// 2/22/2016 rmd : A valuable bit setting. Used to determine if any sensor reading attempt
// failed since the last time the TPMicro requested the sensor data. It doesn't mean the
// sensor data delivered from the TPMicro isn't valid - if there are length bits the reading
// is valid - this bit means one of the reading attempts failed. REMEMBER the decoder reads
// the sensor once per minute while the TPMicro gathers that reading once every 10 minutes.
// So on average there are 10 sensor readings taken for each delivery to the
// TPMicro.
#define MOISTURE_DATA_STATUS_INVALID		(0x40)

// 1/27/2016 rmd : The LAST data received was valid. This is really an INTERNAL decoder
// variable used to determine if the decoder should update the data waiting to be delivered
// to the TPMicro. Not useful on the CS3000 side.
#define MOISTURE_DATA_STATUS_LAST_READING_VALID		(0x20)

// 2/19/2016 rmd : The status byte that is delivered to the TPMicro along with the moisture
// sensor data string contains 5 bits (the lower 5-bits) that reflects the length of the
// string delivered from the sensor. If the length is 0 there was NEVER a valid response
// received from the sensor since the last time the TPMicro asked the decoder for the
// sensors data. The CS3000 should query these bits and if non-zero there is a valid
// reading.
#define MOISTURE_STATUS_DATA_STRLEN_MASK	(0x1f)

typedef struct
{
	// 1/27/2016 rmd : The status byte.
	uint8_t	status;
	
	uint8_t	response_string[MAX_MOISTURE_DATA_STRLEN];
	
} __attribute__((packed)) MOISTURE_SENSOR_DECODER_RESPONSE;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

