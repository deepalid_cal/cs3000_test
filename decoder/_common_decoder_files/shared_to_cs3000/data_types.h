///////////////////////////////////////////////////////////////
//
//    File: data_types.h
//
//    Top level data types for Calsense 2-wire decoders
//
///////////////////////////////////////////////////////////////

// Unsigned integer types
typedef uint32_t  U32;
typedef uint16_t  U16;
typedef uint8_t   U8;
typedef uint8_t   BOOL;

// Signed integer types
typedef int32_t   S32;
typedef int16_t   S16;
typedef int8_t    S8;

