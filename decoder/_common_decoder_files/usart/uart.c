/****************************************************************************
 *   $Id:: uart.c 8224 2011-10-07 00:50:05Z usb00423                        $
 *   Project: NXP LPC11Exx UART example
 *					   
 *   Description:
 *     This file contains UART code example which include UART 
 *     initialization, UART interrupt handler, and related APIs for 
 *     UART access.
 *
 ****************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
 * Permission to use, copy, modify, and distribute this software and its 
 * documentation is hereby granted, under NXP Semiconductors' 
 * relevant copyright in the software, without fee, provided that it 
 * is used in conjunction with NXP Semiconductors microcontrollers.  This 
 * copyright, permission, and disclaimer notice must appear in all copies of 
 * this code.
****************************************************************************/
#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "uart.h"

#include "gpio.h"    // while debugging

/***********************************************************************
 *
 * Function: uart_set_divisors
 *
 * Purpose: Determines best dividers to get a target clock rate
 *
 * Processing:
 *     See function.
 *
 * Parameters:
 *     UARTClk    : UART clock
 *     baudrate   : Desired UART baud rate
 *
 * Outputs:
 *	  baudrate : Sets the estimated buadrate value in DLL, DLM, and FDR.
 *
 * Returns: Error status.
 *
 * Notes: None
 *
 **********************************************************************/
U32 uart_set_divisors( U32 UARTClk, U32 baudrate )
{
   U32 uClk;
   U32 calcBaudrate = 0;
   U32 temp = 0;

   U32 mulFracDiv, dividerAddFracDiv;
   U32 diviser = 0 ;
   U32 mulFracDivOptimal = 1;
   U32 dividerAddOptimal = 0;
   U32 diviserOptimal = 0;

   U32 relativeError = 0;
   U32 relativeOptimalError = 100000;

   /* get UART block clock */
   uClk = UARTClk >> 4; /* div by 16 */

   /* In the Uart IP block, baud rate is calculated using FDR and DLL-DLM registers
   * The formula is :
   * BaudRate= uClk * (mulFracDiv/(mulFracDiv+dividerAddFracDiv) / (16 * (DLL)
   * It involves floating point calculations. That's the reason the formulae are adjusted with
   * Multiply and divide method.*/
   /* The value of mulFracDiv and dividerAddFracDiv should comply to the following expressions:
   * 0 < mulFracDiv <= 15, 0 <= dividerAddFracDiv <= 15 */
   for (mulFracDiv = 1; mulFracDiv <= 15; mulFracDiv++)
   {
      for (dividerAddFracDiv = 0; dividerAddFracDiv <= 15; dividerAddFracDiv++)
      {
         temp = (mulFracDiv * uClk) / ((mulFracDiv + dividerAddFracDiv));
         diviser = temp / baudrate;
         if ((temp % baudrate) > (baudrate / 2))
         {
            diviser++;
         }

         if (diviser > 2 && diviser < 65536)
         {
            calcBaudrate = temp / diviser;

            if (calcBaudrate <= baudrate)
               relativeError = baudrate - calcBaudrate;
            else
               relativeError = calcBaudrate - baudrate;

            if ((relativeError < relativeOptimalError))
            {
               mulFracDivOptimal = mulFracDiv ;
               dividerAddOptimal = dividerAddFracDiv;
               diviserOptimal = diviser;
               relativeOptimalError = relativeError;
               if (relativeError == 0)
                  break;
            }
         } /* End of if */
    } /* end of inner for loop */

    if (relativeError == 0)
      break;
   } /* end of outer for loop  */

   if (relativeOptimalError < (baudrate / 30))
   {
      /* Set the `Divisor Latch Access Bit` and enable so the DLL/DLM access*/
      /* Initialise the `Divisor latch LSB` and `Divisor latch MSB` registers */
      LPC_USART->DLM = (diviserOptimal >> 8) & 0xFF;
      LPC_USART->DLL = diviserOptimal & 0xFF;

      /* Initialise the Fractional Divider Register */
      LPC_USART->FDR = ((mulFracDivOptimal & 0xF) << 4) | (dividerAddOptimal & 0xF);
      return( TRUE );
   }

   return ( FALSE );
}

/*****************************************************************************
** Function name:		UARTInit
**
** Descriptions:		Initialize UART0 port, setup pin select,
**                   clock, parity, stop bits, FIFO, etc.
**
** parameters:			UART baudrate
** Returned value:	None
** 
*****************************************************************************/
void UARTInit( U32 baudrate )
{
  U32 Fdiv;
  volatile U32 regVal;

   // Configure RXD and TXD pins for 2-wire decoder
   LPC_IOCON->PIO1_26 &= ~0x07;  /* UART I/O config */
   LPC_IOCON->PIO1_26 |= 0x02;   /* UART RXD on P1.26 */
   LPC_IOCON->PIO1_27 &= ~0x07;	/* UART I/O config */
   LPC_IOCON->PIO1_27 |= 0x02;   /* UART TXD on P1.27 */

   /* Enable UART clock */
   LPC_SYSCON->SYSAHBCLKCTRL |= (1<<12);
   LPC_SYSCON->UARTCLKDIV = 0x1;  /* divided by 1 */

   LPC_USART->LCR = 0x83;         /* 8 bits, no Parity, 1 Stop bit */

   Fdiv = ((SystemCoreClock/LPC_SYSCON->UARTCLKDIV)/16)/baudrate ;	/*baud rate */
   LPC_USART->DLM = Fdiv / 256;							
   LPC_USART->DLL = Fdiv % 256;
	LPC_USART->FDR = 0x10;		/* Default */

   LPC_USART->LCR = 0x03;         /* 8-bit data, no parity, DLAB = 0 */

   // FIFO Trigger levels: 1 character
   LPC_USART->FCR = 0x07;         /* Enable and reset TX and RX FIFO. */

   /* Read to clear the line status. */
   regVal = LPC_USART->LSR;

   /* Ensure a clean start, no data in either TX or RX FIFO. */
   while (( LPC_USART->LSR & (LSR_THRE|LSR_TEMT)) != (LSR_THRE|LSR_TEMT) );
   while ( LPC_USART->LSR & LSR_RDR )
   {
      regVal = LPC_USART->RBR;	/* Dump data from RX FIFO */
   }
 
   return;
}

//////////////////////////////////////////////////
//
//	BOOL bRxDataAvail(void)
//
//	Returns TRUE if a character is ready,
//	otherwise returns FALSE.
//
//////////////////////////////////////////////////
BOOL bRxDataAvail( void )
{
   BOOL bRDA;
   U8 Dummy;
   U8 LSRValue;

   // Read line status register
   LSRValue = LPC_USART->LSR;

   /* Receive Line Status */
   if (LSRValue & (LSR_OE | LSR_PE | LSR_FE | LSR_RXFE | LSR_BI))
   {
      /* There are errors or break interrupt */
      /* Reading LSR will clear the interrupt */
      Dummy = LPC_USART->RBR;    /* Dummy read on RX to clear 
                                      interrupt, then bail out */

      // Re-read line status register
      LSRValue = LPC_USART->LSR;      
   }

   bRDA = LSRValue & LSR_RDR ? TRUE : FALSE;
   return bRDA;
}

//////////////////////////////////////////
//
//	U8 getRxChar(void)
//
//	Returns the character in the USART
//	receive buffer register
//////////////////////////////////////////
U8 getRxChar( void )
{
   return LPC_USART->RBR;
}

//////////////////////////////////
//	Outputs a single serial port 
//	character to the USART
//////////////////////////////////
void UsartTx( U8 data )
{
	// Put data into UART THR (sends data)
   LPC_USART->THR = data;
}

/////////////////////////////////////////////////
//	Checks for USART Tx buffer ready
//
//	Returns: TRUE (OK to send)
//				FALSE (not ready to send)
/////////////////////////////////////////////////
BOOL bUsartTxRdy( void )
{
   BOOL bTxRdy;

   bTxRdy = LPC_USART->LSR & LSR_THRE ? TRUE : FALSE;

	return bTxRdy;
}

///////////////////////////////////////////////////////
//	Checks for USART Tx buffer empty (all data sent)
//
//	Returns: TRUE (all data sent), FALSE (more to go)
///////////////////////////////////////////////////////
BOOL bUsartTxEmpty( void )
{
   BOOL temt;

   temt = LPC_USART->LSR & LSR_TEMT ? TRUE : FALSE;

   return  temt;
}

#if SIM_ENABLE == 1
//////////////////////////////////////////////////////////////////
//
// void UsartBreakControl( BOOL bEnable )
//
// This function controls USART break transmission:
//
// bEnable == TRUE   : Enable break (USART TXD forced to logic 0)
// bEnable == FALSE  : Disable break transmission
//
// WARNING: THIS FUNCTION IS TO BE USED ONLY FOR SIMULATIONS,
// NOT FOR OPERATION ON A REAL 2-WIRE NETWORK!!
//////////////////////////////////////////////////////////////////
void UsartBreakControl( BOOL bEnable )
{
   U32 LCR_value;

   LCR_value = bEnable ? (LCR_BC | LCR_8_DATA_BITS) : LCR_8_DATA_BITS;

   LPC_USART->LCR = LCR_value;
}
#endif
/******************************************************************************
**                            End Of File
******************************************************************************/
