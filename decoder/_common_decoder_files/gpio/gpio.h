///////////////////////////////////////////////////////////
//
//    File: gpio.h
//
//    Header file for GPIO I/O Functions
//
///////////////////////////////////////////////////////////

// Base address of port 0 pin registers
#define GPIO_PIN_REG_BASE_ADDR   0x50001000

// Address offset to start of port 1 pin registers
#define GPIO_PIN_REG_PORT_OFFS   0x80

// Address offset for port pins
#define GPIO_PIN_REG_BIT_OFFS    4

// This macro computes the GPIO pin register address for the specified port, bit
#define GPIO_PIN_REG_ADDR(port, bit)   (GPIO_PIN_REG_BASE_ADDR+(port * GPIO_PIN_REG_PORT_OFFS)+(bit * GPIO_PIN_REG_BIT_OFFS))

// Address of port 0 pin direction register
#define GPIO_PORT_DIR_REG_BASE_ADDR 0x50002000
#define GPIO_P0_DIR_REG_BASE_ADDR   GPIO_PORT_DIR_REG_BASE_ADDR

// Address of port 1 pin direction register
#define GPIO_P1_DIR_REG_BASE_ADDR   0x50002004

// Address offset to port 1 pin direction register
#define GPIO_PORT_DIR_REG_PORT_OFFS    4

// Macros for accessing the pin direction registers
#define GPIO_DIRP0  (*((U32 *)GPIO_P0_DIR_REG_BASE_ADDR))
#define GPIO_DIRP1  (*((U32 *)GPIO_P1_DIR_REG_BASE_ADDR))

#define GPIO_PIN_DIR_OUTPUT         1        
#define GPIO_PIN_DIR_INPUT          0        // default (reset) value

// Macros for accessing the GPIO port SET and port CLR registers
#define GPIO_SETP0   (*((U32 *)0x50002200))
#define GPIO_SETP1   (*((U32 *)0x50002204))
#define GPIO_CLRP0   (*((U32 *)0x50002280))
#define GPIO_CLRP1   (*((U32 *)0x50002284))

////////////////////////////////////////////////////
// The base address of the GPIO pin configuration
// registers is LPC_IOCON_BASE
//
// There is one 32-bit register for each port/bit
///////////////////////////////////////////////////

// This macro calculates the address of the I/O configuration register for a port/bit
#define IOCON_ADDR(port, bit)       (LPC_IOCON_BASE+(port * 0x60)+(bit * 4))

////////////////////////////////////////////////////////////////////////
// Macros for setting fields within an I/O configuration register
// Not all fields are present for all pins - see data sheet sec. 7.4.1
////////////////////////////////////////////////////////////////////////
#define _FUNC(n)  (n)      // pin function (3-bit field), 0 = GPIO
#define _MODE(n)  (n<<3)   // mode (2-bit field): 0=no PU/PD, 1=PD, 2=PU, 3=Repeater mode
#define _HYS(n)   (n<<5)   // hysteresis (1-bit field): 1 = yes, 0 = no
#define _INV(n)   (n<<6)   // invert input (1-bit field): 1 = yes, 0 = no
#define _OD(n)    (n<<10)  // Open-drain mode (1-bit field): 1 = enable, 0 = disable

// This macro must be used for ADC pins that are being configured as GPIO inputs
// or outputs. The states of such configured GPIO pins cannot be read correctly unless 
// _DIG(1) is specified as the option value when configuring the pin in gpio.c.
#define _DIG(n)   (n<<7)   // Digital mode = 1, analog input = 0

#define GPIO_PORT0_PTR  ( (U32 *) 0x50002100 )
#define GPIO_PORT1_PTR  ( (U32 *) 0x50002104 )

extern void SetGpioPinValue( U32 port, U32 bit, U32 state );
extern U32 GetGpioPinValue( U32 port, U32 bit );

extern void CfgPinFunc( U32 port, U32 bit, U32 cfg_value );

extern void InitPinAsGpioOutput( U32 port, U32 bit, U32 initial_state, U32 options );
extern void InitPinAsGpioInput( U32 port, U32 bit, U32 options );
extern void InitPinAsAltFunc( U32 port, U32 bit, U32 func, U32 options );

// Initialize all decoder pins
extern void init_pins(void);
