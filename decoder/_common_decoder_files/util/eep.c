#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "ProtocolMsgs.h"
#include "iap.h"
#include "crc16.h"
#include "BG_EEP_Sync.h"

#include "stddef.h"  // For offsetof 
#include "string.h"  // For memcpy. memset

////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    U32 WriteParametersToEEPROM( U32 region_num, BOOL bLoadDefaultsFirst )
//
//    If bLoadDefaultsFirst is TRUE, this function restores default values to the RAM before 
//    writing the parameters in RAM out to EEPROM and updating the EEPROM CRC value.
//
//    In the case of the statistics, the default values used are all zeros (via memset)
//
//    Returns:    CMD_SUCCESS (0) of successful, otherwise a non-zero value
//
//    WARNING: Interrupts will be disabled during the EEPROM writes.
////////////////////////////////////////////////////////////////////////////////////////////////////////
U32 WriteParametersToEEPROM( U32 region_num, BOOL bLoadDefaultsFirst )
{
   U8 *RAM_Addr;
   U32 EEPROM_Addr;
   U8 *FlashDefaults;
   U32 byte_cnt;
   U32 CrcOffsetInEEPROM;
   U16 crc16;
   U32 status;

   if( region_num <= STATS_REGION )
   {
      // Initialize variable for the region 
      FlashDefaults     = bg_sync_s[ region_num ].FlashDefaults;
      RAM_Addr          = bg_sync_s[ region_num ].pSRAM;
      byte_cnt          = bg_sync_s[ region_num ].size;
      EEPROM_Addr       = bg_sync_s[ region_num ].EEPROM;
      CrcOffsetInEEPROM = bg_sync_s[ region_num ].CRC16;

      if( bLoadDefaultsFirst )
      {
         /////////////////////////////////////////////////////
         //    Default values are to be loaded into RAM before
         //    writing the RAM values out to EEPROM
         /////////////////////////////////////////////////////
         if( FlashDefaults )
         {
            /////////////////////////////////////////////////
            // Copy default parameters from Flash into RAM
            /////////////////////////////////////////////////
            memcpy( (void *) RAM_Addr, (void *) FlashDefaults, byte_cnt );
         }
         else
         {
            //////////////////////////////
            // Handle statistics region
            //////////////////////////////

            /////////////////////////////////////////////////////////
            // Zero the parameters in RAM for statistics, except for
            // temp_maximum, which is initialized to 0xffff.
            /////////////////////////////////////////////////////////
            ClearStatistics();   // This function is in main.c
         }
      }

      ///////////////////////////////////////////////////////////
      // Write the specified parameter values in RAM to EEPROM
      ///////////////////////////////////////////////////////////
      status = EEPROM_Write( EEPROM_Addr, RAM_Addr, byte_cnt );

      // Calculate the CRC-16 value for this memory region
      crc16 = crc16_sram_region( (U8 *) RAM_Addr, byte_cnt );

      // Save the CRC-16 value for this region in EEPROM
      status |= EEPROM_Write( CrcOffsetInEEPROM, (U8 *) &crc16, sizeof(crc16) );
   }
   else
   {  
      status = 0xff;
   }

   return status;
}

/////////////////////////////////////////////////////////////////////////////
//
//    U32 WriteSerialNumbersToEEPROM( SER_NUM_u serial_num_u )
//
//    This function writes two copies of the specified serial number and
//    two copies of its ones complement to EEPROM.
//
//    Returns:    CMD_SUCCESS (0) of successful, otherwise a non-zero value
//
//    WARNING: Interrupts will be disabled during the EEPROM writes.
/////////////////////////////////////////////////////////////////////////////
U32 WriteSerialNumbersToEEPROM( SER_NUM_u serial_number_u )
{
   U32 status;
   SER_NUM_s SerialNumber[2];    // values read from EEPROM during initialization

   // First initialize the serial number structures in RAM
   SerialNumber[0].ser_num_u.sn32     =  serial_number_u.sn32;
   SerialNumber[0].ser_num_u_not.sn32 = ~serial_number_u.sn32;
   SerialNumber[1].ser_num_u.sn32     =  serial_number_u.sn32;
   SerialNumber[1].ser_num_u_not.sn32 = ~serial_number_u.sn32;

   // Then write these out to the EEPROM
   status = EEPROM_Write( EEPROM_SER_NUMS_OFFSET, (U8 *) &SerialNumber, sizeof(SerialNumber) );

   return status;
}
