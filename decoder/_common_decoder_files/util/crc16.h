/////////////////////////////////////////////////////////
//
//    File: crc16.h
//
//    Description: header file for CRC16 calculation 
//
/////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
//  The crc16 accumulator must be initialized with this value
///////////////////////////////////////////////////////////////
#define CRC16_INIT_VAL          0xffff

// Function Prototype
extern U16 _crc16_update(U16 _crc, U8 _data);
extern U16 crc16_eeprom_region( U32 offset, U32 size );
extern U16 crc16_sram_region( U8 *pRegion, U32 size );

