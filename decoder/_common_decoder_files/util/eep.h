/////////////////////////////////////////////////////////////
//
//    File: eep.h
//
//   Header file for EEPROM utility functions
//
/////////////////////////////////////////////////////////////
extern U32 WriteParametersToEEPROM( U32 region_num, BOOL bLoadDefaultsFirst );
extern U32 WriteSerialNumbersToEEPROM( SER_NUM_u serial_num_u );