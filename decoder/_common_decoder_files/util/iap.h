///////////////////////////////////////////////////////////
//
//	File:    iap.h
//
//	Purpose: Header file for IAP I/O functions
//
// Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
///////////////////////////////////////////////////////////

// Address of IAP Entry
#define IAP_ADDRESS 0x1FFF1FF1

// IAP Functions need system clock in KHz units
#define CCLK_KHZ  (CPU_XTAL_CLK_HZ/1000)

// IAP Command Codes
#define PREP_SECTOR_FOR_WRITE    50
#define COPY_RAM_TO_FLASH        51
#define ERASE_SECTORS            52
#define BLANK_CHECK_SECTORS      53
#define READ_PART_ID             54
#define READ_BOOT_CODE_VERSION   55
#define COMPARE                  56
#define REINVOKE_ISP             57
#define READ_UID                 58
#define EEPROM_WRITE             61
#define EEPROM_READ              62

// IAP Status Codes
#define CMD_SUCCESS              0     // Command is executed successfully.
#define INVALID_COMMAND          1     // Invalid command.
#define SRC_ADDR_ERROR           2     // Source address is not on a word boundary.
#define DST_ADDR_ERROR           3     // Destination address is not on a correct boundary.
#define SRC_ADDR_NOT_MAPPED      4     // Source address is not mapped in the memory map.
#define DST_ADDR_NOT_MAPPED      5     // Destination address is not mapped in the memory map.
#define COUNT_ERROR              6     // Byte count is not multiple of 4 or is not a permitted value.
#define INVALID_SECTOR           7     // Sector number is invalid.
#define SECTOR_NOT_BLANK         8     // Sector is not blank.
#define SECTOR_NOT_PREPARED      9     // Command to prepare sector for write operation was not executed.
#define COMPARE_ERROR            10    // Source and destination data is not same.
#define BUSY                     11    // Flash programming hardware interface is busy.

#define MUTEX_ERROR              12    // Added for Calsense IAP implementation

// Size of the UID (bytes)
#define UID_SIZE_BYTES           16    // Size of the Unique ID (UID) in bytes

// Function prototypes
extern void iap_init(void);
extern U32 Reinvoke_ISP(void);
extern U32 Read_UID(U32 *pUID);
extern U32 EEPROM_Write( U32 EEPROM_Addr, U8 *RAM_Addr, U32 byte_cnt );
extern U32 EEPROM_Read( U8 *RAM_Addr, U32 EEPROM_Addr, U32 byte_cnt );
