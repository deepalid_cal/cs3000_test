////////////////////////////////////////////////////////////////////////
//
//	File:    iap.c
//
//	Purpose: IAP I/O functions
//
// Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
// NOTES:   1. The flash memory is not accessible during a write or 
//          erase operation. IAP commands, which result in a flash 
//          write/erase operation, use 32 bytes of space in the 
//          top portion of the ?? on-chip RAM for execution.
//
//          2. The IAP functions themselves neither disable nor enable 
//			interrupts,	however the iap_entry function disables 
//			interrupts prior to IAP calls, and re-enables them 
//			upon return from them.
//
///////////////////////////////////////////////////////////////////////
#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "iap.h"

#include "string.h"  // For memcpy

#include "FreeRTOS.h"
#include "semphr.h"

#include "gpio.h"    // TEMP for debugging: GPIO_SETP1 and GPIO_CLRP1 macros

// Mutex for IAP Access
static xSemaphoreHandle xIAP_Mutex;

// IAP I/O tables (visible only in this file)
static unsigned long param_table[5];    // Command parameter table
static unsigned long result_table[5];   // Command result table

// This function performs the actual IAP function call per the conventions 
// spelled out in section 18.13 of the NXP LPC11Exx User Manual Rev. 2
static void iap_entry( unsigned long param_tab[], unsigned long result_tab[] )
{
   // Declare the iap function
   void (*iap)( unsigned long[], unsigned long[] );

   // Set up the function pointer
   iap = (void (*)( unsigned long[],unsigned long[] ) ) IAP_ADDRESS;

   // Call the IAP function
   portENTER_CRITICAL();

   iap( param_tab, result_tab );

   portEXIT_CRITICAL();   
}

/////////////////////////////////////////////////
// 
//    void iap_init(void)
//
//    This function creates the FreeRTOS mutex
//    for guarding the LPC11E14 IAP access.
//
//    Since all IAP calls use common parameter
//    arrays, the mutex guards access to them.
//
//    Does not return if mutex cannot be created
/////////////////////////////////////////////////
void iap_init(void)
{
   // Create the mutex for guarding EEPROM I/O
   xIAP_Mutex = xSemaphoreCreateMutex();

   if( xIAP_Mutex == NULL )
   {  
      // if unable to create the mutex
      Fatal_Error( IAP_MUTEX_CREATE_ERROR );
   }
}

///////////////////////////////////////////////////////////////
//
//    U32 Reinvoke_ISP(void)
//
//    Description:
//
//    This command is used to invoke the bootloader in ISP mode.
//    It maps boot vectors, sets PCLK = CCLK, configures UART 
//    pins RXD and TXD, resets counter/timer CT32B1 and resets
//    the U0FDR (see Table 166). This command may be used when 
//    a valid user program is present in the internal flash 
//    memory and the PIO0_1 pin is not accessible to force 
//    the ISP mode.
//
//    There is no return from this function unless there is
//    an error (which can only be MUTEX_ERROR.)
///////////////////////////////////////////////////////////////
U32 Reinvoke_ISP(void)
{
   // Get the mutex for EEPROM access
   if( pdTRUE == xSemaphoreTake( xIAP_Mutex, portMAX_DELAY ) )
   {
      // Need to set up main stack pointer here

      // Parameter for Reinvoke ISP
      param_table[0] = REINVOKE_ISP;             // Command code
      iap_entry( param_table, result_table );

      // Execution continues in the ISP
   }
   return ( MUTEX_ERROR );
}

//////////////////////////////////////////////////////////////
//
//    U32 Read_UID(U32 *pUID)
//
//    U32 *pUID: Pointer to buffer to hold the 16-byte UID
//
//    Description: This command is used to read the unique ID.
//
//    Possible Value: CMD_SUCCESS
//////////////////////////////////////////////////////////////
U32 Read_UID(U32 *pUID)
{
   U32 status;

   status = MUTEX_ERROR;

   // Get the mutex for EEPROM access
   if( pdTRUE == xSemaphoreTake( xIAP_Mutex, portMAX_DELAY ) )
   {
      // Parameter for Read UID
      param_table[0] = READ_UID;             // Command code
      iap_entry( param_table, result_table );

      // Copy UID from result_table[1..4] into caller's buffer
      memcpy( (void *) pUID, (void *) &result_table[1], UID_SIZE_BYTES );

      status = result_table[0];

      // Release the mutex for IAP access
      xSemaphoreGive( xIAP_Mutex );
   }

   // Return status
   return status;
}

///////////////////////////////////////////////////////////////////////////////////////
//
//    U32 EEPROM_Write( U8 *EEPROM_Addr, U8 *RAM_Addr, U32 byte_cnt )
//
//    Description:
//
//    This function copies data from the RAM address to the EEPROM 
//    address.
//
//    U32 EEPROM_Addr:  Starting address in EEPROM
//    U8 *RAM_Addr:     Starting address in SRAM
//    U32 byte_cnt:     Number of bytes to copy
//
//    Return Value:     Can be CMD_SUCCESS, SRC_ADDR_NOT_MAPPED,
//                      or DST_ADDR_NOT_MAPPED
//
//    NOTE:    The top 64 bytes of the 4 KB EEPROM memory are
//             reserved and cannot be written to.
///////////////////////////////////////////////////////////////////////////////////////
U32 EEPROM_Write( U32 EEPROM_Addr, U8 *RAM_Addr, U32 byte_cnt )
{
   U32 status;

   status = MUTEX_ERROR;

   // Get the mutex for EEPROM access
   if( pdTRUE == xSemaphoreTake( xIAP_Mutex, portMAX_DELAY ) )
   {
      // Parameters for Write EEPROM 
      param_table[0] = EEPROM_WRITE;         // Command code
      param_table[1] = EEPROM_Addr;          // EEPROM address (destination)
      param_table[2] = (U32) RAM_Addr;       // RAM address (source)
      param_table[3] = byte_cnt;             // # bytes to write to EEPROM
      param_table[4] = CCLK_KHZ;             // system clock in KHz 

      iap_entry( param_table, result_table );

      status = result_table[0];

      // Release the mutex for IAP access
      xSemaphoreGive( xIAP_Mutex );
   } 

   // Return status
   return status;
}

//////////////////////////////////////////////////////////////////////////////////////
//
//    U32 EEPROM_Read( U8 *RAM_Addr, U8 *EEPROM_Addr, U32 byte_cnt )
//
//    Description:
//
//    This function copies data from the EEPROM address to the RAM 
//    address.
//
//    U8 *RAM_Addr:     Starting address in SRAM (destination)
//    U32 EEPROM_Addr:  Starting address in EEPROM (source)
//    U32 byte_cnt:     Number of bytes to copy
//
//    Return Value:     Can be CMD_SUCCESS, SRC_ADDR_NOT_MAPPED,
//                      or DST_ADDR_NOT_MAPPED
//
//    NOTE:    The top 64 bytes of the 4 KB EEPROM memory are
//             reserved and cannot be written to. So reading them
//             is probably pointless and not a good idea.
//////////////////////////////////////////////////////////////////////
U32 EEPROM_Read( U8 *RAM_Addr, U32 EEPROM_Addr, U32 byte_cnt )
{
   U32 status;

   status = MUTEX_ERROR;

   // Get the mutex for EEPROM access
   if( pdTRUE == xSemaphoreTake( xIAP_Mutex, portMAX_DELAY ) )
   {
      // Parameters for Read EEPROM
      param_table[0] = EEPROM_READ;          // Command code
      param_table[1] = EEPROM_Addr;          // EEPROM address (source)
      param_table[2] = (U32) RAM_Addr;       // RAM address (destination)
      param_table[3] = byte_cnt;             // # bytes to read from EEPROM
      param_table[4] = CCLK_KHZ;             // system clock in KHz 

      iap_entry( param_table, result_table );

      status = result_table[0];

      // Release the mutex for EEPROM access
      xSemaphoreGive( xIAP_Mutex );
   }

   // Return status
   return status;
}
