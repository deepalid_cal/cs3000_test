///////////////////////////////////////////////////////////
//
//    File: crc16.c 
//
//    Description:

//    CRC16 calculations for 2-Wire messages and for memory
//    region validity checks.
//
//    All of these functions are reentrant.
///////////////////////////////////////////////////////////

#include "LPC1000.h"
#include "LPC11Exx.h"

#include "data_types.h"
#include "crc16.h"
#include "iap.h"

// FreeRTOS includes
#include "FreeRTOS.h"

//////////////////////////////////////////
//  Polynomial is: x^16 + x^15 + x^2 + 1
//////////////////////////////////////////
#define CRC16_POLY_MASK         0xa001

U16 _crc16_update(U16 _crc, U8 _data)
{
   int i;

   _crc ^= _data;
   for( i = 0; i < 8; i++ )
   {
      _crc = (_crc & 1) ? ((_crc >> 1) ^ CRC16_POLY_MASK) : (_crc >> 1);
   }
   return _crc;
}

///////////////////////////////////////////////////////
//
//    U16 crc16_eeprom_region( U32 offset, U32 size )
//
//    Calculates the CRC16 over an EEPROM region
//
//    U32 offset: starting address in EEPROM
//    U32 size:   size in bytes
///////////////////////////////////////////////////////
U16 crc16_eeprom_region( U32 offset, U32 size )
{
	U16 crc16;
	U32 cnt;
	U8 eep_data;
   U32 status;

	crc16 = 0xffff;	/// initialize CRC16 value

	for( cnt = 0; cnt < size; cnt++ )
	{
		///////////////////////////////////////////////////////////
		// get data from EEPROM at EEPROM address = offset + cnt
      //
      // An error is a problem
		///////////////////////////////////////////////////////////
      status = EEPROM_Read( (U8 *) &eep_data, (offset + cnt), 1 );

      if( status != CMD_SUCCESS )
      {
         // FATAL ERROR
         portENTER_CRITICAL();
         while(1);
      }

		crc16 = _crc16_update( crc16, eep_data);
	}

	return crc16;
}

//////////////////////////////////////////////////////////
//
//    U16 crc16_sram_region( U8 *pRegion, U32 size )
//
//    Calculates CRC16 over a range of SRAM
//
//    U8 *pRegion;      // ptr to start of SRAM
//    U32 size;         // size in bytes
//
//////////////////////////////////////////////////////////
U16 crc16_sram_region( U8 *pRegion, U32 size )
{
	U16 crc16;

	crc16 = 0xffff;	/// initialize CRC16 value

	while( size-- )
	{
		crc16 = _crc16_update( crc16, *pRegion++ );
	}

	return crc16;
}


