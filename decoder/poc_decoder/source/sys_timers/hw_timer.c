//////////////////////////////////////////////////////////////////////
//
//    File: hw_timer.c
//
//    Timer functions for CT16B1 and CT32B0
//
//    SYNOPSIS:
//
//    CT16B1:  is used to time the H-Bridge phases when transmitting
//             serial data.
//    CT32B0:  is used to count flow sensor pulses (using MAT0 input)
//
//////////////////////////////////////////////////////////////////////
#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "sys_timers.h"
#include "gpio.h"
#include "FreeRTOS.h"

/////////////////////////////////
//  Set up a hardware timer for
//  timing in microseconds.
//  Use CT16B1 for this function
/////////////////////////////////
void init_us_timer(void)
{
   // Stop the Timer Counter and Prescale Counter
   LPC_CT16B1->TCR = 0;       // reset CEN (bit 0)

   // Configure Match Control Register to clear TCR[0] when MR0 matches it.
   // Also clear TC when MR0 matches it.
   LPC_CT16B1->MCR = (1<<1) | (1<<2);  // Set MR0R and MR0S

   ///////////////////////////////////////////////
   // Set prescale register to 11 (divide by 12)
   // to generate a 1 MHz clock to CT16B1.
   ///////////////////////////////////////////////
   LPC_CT16B1->PR = 11;

   // Set Count Control Register for timer mode
   LPC_CT16B1->CTCR = 0;   // Note: Same as reset value
}


/////////////////////////////////////////////////
//
//    void us_timer( U16 delay_us )
//
/////////////////////////////////////////////////
void us_timer( U16 delay_us )
{
   portENTER_CRITICAL();

   // Adjust for inherent delays in function
   if( delay_us >= 15 ) delay_us -= 14;

   // NOTE: These first two statements may not be necessary

   // Stop the Timer Counter and Prescale Counter
   LPC_CT16B1->TCR = 0;    // reset CEN (bit 0)

   // Clear the TC register
   LPC_CT16B1->TC = 0;

   // Load MR0 with # microseconds
   *(MR1_ADDR_PTR(0)) = delay_us;

   // Enable the Timer Counter and Prescale Counter
   LPC_CT16B1->TCR = 1;    // Set CEN (bit 0)

   // Wait for match 0 (time up) by waiting for TCR bit 0 (CEN) to be cleared
   while( (LPC_CT16B1->TCR & 1) );

   portEXIT_CRITICAL();
}

///////////////////////////////////
//  Set up 16-bit timer 0 for PWM
///////////////////////////////////
void init_pwm(void)
{
   U32 EMR_Value;

   // Disable the CT16B0 interrupt
   NVIC_DisableIRQ( TIMER_16_0_IRQn );

   // Enable PCLK to CT16B0
   LPC_SYSCON->SYSAHBCLKCTRL |= (1<<7);   // CT16B0 is bit 7

   ///////////////////////////////////////////////
   // Set prescale register to 95 (divide by 96)
   // to generate a 125 KHz clock to CT16B0.
   ///////////////////////////////////////////////
   LPC_CT16B0->PR = 95;

   // Set Count Control Register for timer mode
   LPC_CT16B0->CTCR = 0;   // Note: Same as reset value

   // Set PWM control register to Non-PWM mode - the PWM
   // outputs are controlled by code in the CT16B0 ISR (interrupt #16)
   LPC_CT16B0->PWMC = 0;   // Note: Same as reset value

   // Set MR0 for 25% (1 ms)
   *(MR0_ADDR_PTR(0)) = 125;

   // Set MR1 for 75% (3 ms)
   *(MR0_ADDR_PTR(1)) = 375;

   // Set MR3 for 1 PWM cycle (4 ms.) = 500
   *(MR0_ADDR_PTR(3)) = 500;

   //////////////////////////////////////////////////////
   // Set up MCR such that:
   // 1. An interrupt is generated when TC == MR0
   // 2. An interrupt is generated when TC == MR1
   // 3. An interrupt is generated when TC == MR3
   // 4. The TC is reset when TC == MR3
   //
   // All events cause interrupt #16 (CT16B0 interrupt)
   //////////////////////////////////////////////////////
   LPC_CT16B0->MCR = ( (1<<0) | (1<<3) | (1<<9) | (1<<10) );    

   // Write the setup value to the EMR
   LPC_CT16B0->EMR = EMR_Value;

   // Enable the Timer Counter and Prescale Counter
   LPC_CT16B0->TCR = 1;    // Set CEN (bit 0)

   // Set SYSTICK interrupt priority to 3 (lowest)
   NVIC_SetPriority( SysTick_IRQn, 3 );

   // Set CT16B0 interrupt priority to 0 (highest) 
   NVIC_SetPriority( TIMER_16_0_IRQn, 0 );

   /* Enable the CT16B0 Interrupt */
   NVIC_EnableIRQ( TIMER_16_0_IRQn );
}

/////////////////////////////////////////////////////////////////
//
//    void init_flow_sensor_pulse_counter(void)
//
//    This function initializes CT32B0 to count flow sensor
//    pulses on the rising edges of the CT32B0 CAP0 input.
//    This is P0.17 (pin 45) on the LPC11E14.
//
//    This function enables the CT32B0 counter to count.
//
///////////////////////////////////////////////////////////////////
void init_flow_sensor_pulse_counter(void)
{
   portENTER_CRITICAL();

   // Enable PCLK to CT32B0 - it's off after SystemInit() returns
   // This operation was added here to avoid having this in
   // the shared CMSIS/system_LPC11Exx.c file. Enabling the 32-bit
   // counter uses some additional power.

   // Or in bit 9 to enable PCLK to CT32B0( bit 9)
   // Write to the system clock control register
   LPC_SYSCON->SYSAHBCLKCTRL |= (1<<9);

   portEXIT_CRITICAL();

   LPC_CT32B0->TCR = CT32B0_TCR_CRST;      // Reset TC and prescaler
   LPC_CT32B0->TCR = 0;                    // Remove reset command

   // Set up CT32B0 Capture Control Register (CCR)
   LPC_CT32B0->CCR = 0;    // Happens to be the same as reset (default) value

   // Set up External Match Register (EMR)
   LPC_CT32B0->EMR = 0;    // Happens to be the same as reset (default) value

   // Set up the CT32B0 Count Control Register (CTCR) to count on the 
   // rising edge of the CAP0 (P0.17) input. The CAP0 input is driven by FS_PULSE-.
   LPC_CT32B0->CTCR = CT32B0_CTCR_CTM_CTR_RE | CT32B0_CTCR_CIS_CAP0;

   // Enable the timer/counter so it's ready to count
   LPC_CT32B0->TCR = CT32B0_TCR_CEN;
}
