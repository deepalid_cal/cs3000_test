////////////////////////////////////////////////////////////////////
//
//	File:	SerMsgProc.c
//
// NOTE: This source file is for the POC Decoder!
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//	
///////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>

#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "crc16.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "SerialMsgProc.h"
#include "ProtocolMsgs.h"
#include "iap.h"
#include "eep.h"
#include "sol_pwm.h"
#include "SerialComm.h"
#include "BG_EEP_Sync.h"
#include "h-bridge_tx.h"
#include "FreeRTOS.h"
#include "task.h"
#include "flow.h"
//#include "stddef.h"     // for offsetof (happens to be #included by FreeRTOS.h)

// 32-bit mask with only A20 set
#define A20_BIT_MASK	0x00100000

// Transmit CRC: Used by all message handlers
static U16 txcrc;

/////////////////////////////////////////////////////////
//	Returns a 32-bit compare mask when given a compare 
//	bit count. Compare mask bits are set starting with 
//	bit 20 and work toward lesser significant bits.
//
//	The compare mask is used to process the ENQ command
//	used during the decoder discovery process.
//
//	A mask value of zero is valid and causes a mask
//	containing all zero bits to be returned.
/////////////////////////////////////////////////////////
static U32 calc_compare_mask(U8 cnt)
{
	U32 seed = A20_BIT_MASK;
	U32 mask = 0x00000000;

	while( cnt-- )
	{
		seed = seed>>1;
		mask |= seed;
	}

	return mask;
}

////////////////////////////////////////////////////////////
//	This function posts an 8-bit value to the transmit
//	ring buffer, adding a SYN byte if needed. The crc value 
//  is updated. 
////////////////////////////////////////////////////////////
static void queue_val8( U8 val8 )
{
	// Put data in ring buffer, update CRC
	putTxRingBfrData( val8 );
	txcrc = _crc16_update( txcrc, val8 );

	// SYN-pad if needed
	if( val8 == DLE )
	{
		// Add a SYN (not included in CRC)
		putTxRingBfrData( SYN );
	}
}

////////////////////////////////////////////////////////////
//	This function posts a 16-bit (little-endian) value 
//	to the transmit ring buffer, adding SYN bytes if needed.
//	The txcrc value is updated. 
////////////////////////////////////////////////////////////
static void queue_val16( U16 val16 )
{
	U16 upd_crc16;

	// First add high byte
	queue_val8( HIBYTE( val16 ) );

	// Then add low byte
	queue_val8( LOBYTE( val16 ) );
}

///////////////////////////////////////////////////////
//	Queues a 16-bit CRC16 (little-endian) value into 
//	transmit ring buffer, handling byte-stuffing as 
//	needed. The value is not included in the CRC16 
//	calculation.
///////////////////////////////////////////////////////
static void queue_crc16( void )
{
	U8 c;

	c = HIBYTE( txcrc );        

	// put the high byte into the tx ring buffer
	putTxRingBfrData( c );

	// Was it a DLE?
	if( c == DLE )
	{
		// stuff a SYN char
		putTxRingBfrData( SYN );
	}

	c = LOBYTE( txcrc );

	// put the low byte into the tx ring buffer
	putTxRingBfrData( c );

	// Was it a DLE?
	if( c == DLE )
	{
		// stuff a SYN char
		putTxRingBfrData( SYN );
	}
}

/////////////////////////////////////////
//	This function queues the leading
//	DLE and STX bytes into the transmit
//	ring buffer and initializes the
// transmit CRC to 0xffff.
/////////////////////////////////////////
static void queue_header(void)
{
	putTxRingBfrData( DLE );
	putTxRingBfrData( STX );
	txcrc = 0xffff;
}

/////////////////////////////////////////////////
//	This function queues the tx CRC16
//	value into the tx ring buffer. The txcrc
//	value must be in little-endian byte 
// order
/////////////////////////////////////////////////
static void queue_trailer( void )
{
	// Queue up the CRC16 bytes (little-endian)
	queue_crc16( );

	// Queue up the terminating DLE EOT
	putTxRingBfrData( DLE );
	putTxRingBfrData( EOT );
}

///////////////////////////////////////////////////////////
//	Queue up a simple response: DLE STX val CRC16 DLE EOT
///////////////////////////////////////////////////////////
static void queue_simple_response(U8 val)
{
	queue_header();

	// store the parameter value
	putTxRingBfrData( val );

	// Calc CRC16 on "val" 
	txcrc = _crc16_update( txcrc, val );

	// Was the parameter a DLE?
	if( val == DLE )
	{
		// Yes, then stuff a SYN
		putTxRingBfrData( SYN );
	}

	// queue up the trailer
	queue_trailer( );
}

//////////////////////////////////////////////////////////////////////////
//
// Function:   U8 ValidateSolCtl( U8 sol, U8 op, 
//                               DECODER_PARMS_s *pParms)
//
// This function checks the validity of the solenoid
// control command data and returns the appropriate
// status code. See ProtocolMsgs.h for the error codes
//
//    U8 sol:  solenoid (0 to n-1)
//    U8 op:   operation code (see ProtocolMsgs.h)
//    DECODER_PARMS_s *pParms: pointer to solenoid's parameters in RAM
//
// If no errors, the return value is: RSP_STATUS_OK (0)
//
// Note: The calling function checks the solenoid # and operation
//       code for validity. That's why it's not done in this
//       function.
/////////////////////////////////////////////////////////////////////////
U8 ValidateSolCtl( U8 sol, U8 op, DECODER_PARMS_s *pParms )
{
	U8 stat8;

	stat8 = RSP_STATUS_OK;

	// Check target state against current solenoid state
	// See if trying to turn on a solenoid that's already On
	if( ( op == SOL_CTL_CMD_SOL_ON ) && ( SolPwmState_e[ sol ] == SOL_HOLD_e ) )
	{
		// Oops, solenoid is already On
		stat8 = RSP_SOL_IS_ALREADY_ON;
	}
	else if( ( op == SOL_CTL_CMD_SOL_OFF ) && ( SolPwmState_e[ sol ] == SOL_OFF_e ) )
	{
		// Oops, solenoid is already Off
		stat8 = RSP_SOL_IS_ALREADY_OFF;
	}

	return stat8;
}

////////////////////////////////////////////////////////////
//
//	Function: void process_sc_message(void)
//
//	Purpose: Processes the serial comm message that
//          was just received.
//
//	Notes:	
//
//	1.	When this function is called, a message 
// 	framed by DLE SOH and DLE EOT and having a valid
//		CRC16 has been received. Bytes within the pairs
//		of framing characters have been placed within 
//		buffer 'sc_msg_bfr,' starting at index 0.
//
//	2. The number of bytes between DLE SOH and DLE EOT
//		is in global variable sc_msglen. This message length
//		thus includes the two bytes of the received CRC16.
//
//	3.	Because the solenoid PWM operations occur solely 
//		within interrupt service routines, it is very
//		important to minimize the time interrupts are
//		disabled within this function or any functions
//		called by this function.
//
////////////////////////////////////////////////////////////
void process_sc_msg(void)
{
	cmd_msg_hdr_s *pHdr;		// ptr to common message header structure
	U32 compare_mask;
	ENQ_MSG_s *pEnqMsg;
	union 
	{
		U32 u32;
		U8 sn8[ EEPROM_SN_ADDR_BYTES ];
	} cmp_sn;

	pHdr = (cmd_msg_hdr_s *) sc_msg_bfr;

	////////////////////////////////////
	//	CHECK FOR A BROADCAST MESSAGE
	////////////////////////////////////

	if( pHdr->opcode == OP_DISC_RST )
	{
		//////////////////////////////////
		//	Reset the decoder DISCOVERED
		//	flag in RAM and EEPROM
		//////////////////////////////////
		Decoder_stats.rx_disc_rst_msgs++;

		// Let BG tasks sync this to EEPROM
		CommonParms.bDiscovered = FALSE;

		// Clear bit in system flags too
		sys_flags &= ~SYS_FLAG_DISCOVERED;

		// This causes Common Parms to be sync'd to EEPROM
		SendSyncRequest( COMMON_PARM_REGION );

		//////////////////////////////////////////////////
		//	No response is sent because all decoders
		//	on the network are being addressed/commanded
		//////////////////////////////////////////////////
		SerialCommInit();	// Prepare to receive again
	}

	else

		/////////////////////////////////////
		//	CHECK FOR MULTICAST MESSAGES
		/////////////////////////////////////

		if( pHdr->opcode == OP_ENQ )
	{
		Decoder_stats.rx_enq_msgs++;

		///////////////////////////////////////////////////////////////////
		// Only process an ENQ if we have NOT been discovered (confirmed)
		///////////////////////////////////////////////////////////////////
		if( !CommonParms.bDiscovered )
		{
			//////////////////////////////
			//	Process the ENQ message
			//////////////////////////////
			pEnqMsg = (ENQ_MSG_s *) pHdr;

			// We will compare using 32-bit unsigned long operations
			cmp_sn.u32 = 0;

			////////////////////////////////////////////
			//	Get the SN bytes to compare against. 
			//	Note that we swap the byte order here
			//  when doing the comparison.
			///////////////////////////////////////////
			cmp_sn.sn8[ 0 ] = pEnqMsg->hdr.sn8[ 2 ];
			cmp_sn.sn8[ 1 ] = pEnqMsg->hdr.sn8[ 1 ];
			cmp_sn.sn8[ 2 ] = pEnqMsg->hdr.sn8[ 0 ];

			// Get the compare mask to use
			compare_mask = calc_compare_mask( pEnqMsg->mask );

			if( (cmp_sn.u32 & compare_mask) == (Serial_Number.sn32 & compare_mask) )
			{
				////////////////////////////////////////////////
				//	Generate an ACK pulse for the ENQ response.
				//	This uses the sc state machine to do the 
				//  sequencing of state changes to the 2-wire
				//	network lines.
				//
				//	Note: We do not follow "send_ack_pulse()"
				//  with "SerialCommInit()" because this is
				//  done ultimately by the sc state machine
				//  itself.
				////////////////////////////////////////////////
				send_ack_pulse();

				// bump stats counter
				Decoder_stats.tx_acks_sent++;
			}
			else
			{
				SerialCommInit();	// Prepare to receive again
			}
		}
		else
		{
			SerialCommInit();	// Prepare to receive again
		}
	}
	else
	{
		///////////////////////////////////////////////////////////
		//	PROCESS UNICAST MESSAGES:
		//
		//	Check the received serial number/address against ours
		//	configured in EEPROM. Here we swap the compare byte 
		//	order to compensate for the endianess difference:
		// network is big-endian, LPC11E14 is little-endian.
		//////////////////////////////////////////////////////////
		if( pHdr->sn8[ 0 ] == Serial_Number.sn8[ 2 ] &&
			pHdr->sn8[ 1 ] == Serial_Number.sn8[ 1 ] &&
			pHdr->sn8[ 2 ] == Serial_Number.sn8[ 0 ] )
		{
			//////////////////////////////////////////////////////
			//	THE UNICAST MESSAGE IS ADDRESSED TO THIS DECODER
			//////////////////////////////////////////////////////

			switch( pHdr->opcode )
			{
				case OP_DISC_CONF:

					Decoder_stats.rx_disc_conf_msgs++;

					//////////////////////////////////
					//	Set the decoder DISCOVERED
					//	flag in RAM and EEPROM
					//////////////////////////////////

					// Let BG task sync this to EEPROM
					CommonParms.bDiscovered = TRUE;

					// Set bit in system flags too
					sys_flags |= SYS_FLAG_DISCOVERED;

					// This causes Common Parms to be sync'd to EEPROM
					SendSyncRequest( COMMON_PARM_REGION );

					//////////////////////////////////////////////////////
					//	Intentionally fall through to send same response
					//	as that for ID REQUEST message
					//////////////////////////////////////////////////////

				case OP_ID_REQ:
					{
						// This check is required to avoid counting OP_DISC_CONF msgs twice
						if( pHdr->opcode == OP_ID_REQ )
						{
							Decoder_stats.rx_id_req_msgs++;
						}

						//////////////////////////////////////////////////////////////////////////////
						//	Queue up ID response: DLE STX DEC_TYPE DEC_SUBTYPE FW_VERS CRC16 DLE EOT
						//////////////////////////////////////////////////////////////////////////////

						// Queue up the header
						queue_header();

						// Put decoder type in ring buffer, update CRC
						queue_val8( CoreData.decoder_type );

						// Put decoder subtype in ring buffer, update CRC
						// NOTE: For now the subtype is always zero
						queue_val8( 0 );

						// Put the firmware version into the ring buffer, update CRC
						queue_val16 ( CoreData.fw_vers );

						// Queue up the CRC16 and DLE EOT bytes
						queue_trailer();

						// Then start the 2-wire transmit 
						start_2_wire_transmit();
					}
					break;


				case OP_DATA_LOOPBK:
					{
						U8 bytes_to_loop;
						U8 *pData;

						// Bump the statistics counter
						Decoder_stats.rx_data_lpbk_msgs++;

						// Point to first message byte past header (cmd_msg_hdr_s)
						pData = (U8 *) (pHdr+1);

						// Compute how many bytes are there (allow 2 for CRC16)
						bytes_to_loop = sc_msglen - sizeof(cmd_msg_hdr_s) - 2;

						/////////////////////////////////////////////////////////
						//	Queue up ID response: DLE STX DATA.. CRC16 DLE EOT
						/////////////////////////////////////////////////////////

						// Queue up the header
						queue_header();

						while( bytes_to_loop-- )
						{
							// Put next data byte in ring buffer, update CRC
							queue_val8( *pData++ );
						}

						// Queue up the CRC16 and DLE EOT bytes
						queue_trailer();

						// Then start the 2-wire transmit 
						start_2_wire_transmit();
					}
					break;

				case OP_PUT_PARMS:
					{
						U8 status_code;
						U32 bytes_to_copy;
						OP_PUT_PARMS_CMD_s *pPutParmsMsg;
						U8 *pParmData;
						const BG_SYNC_s *pInfo;
						U32 put_data_size;
						U8 region;

						// bump the statistics counter
						Decoder_stats.rx_put_parms_msgs++;

						// See if there are any active solenoids
						if( bSolenoidsActive() )
						{
							// one or both solenoids are active

							// Command both solenoids OFF to allow sync to happen
							bSolEna[ SOL1 ] = FALSE;
							bSolEna[ SOL2 ] = FALSE;

							// Allow time for PWM state machine to process OFF commands
							vTaskDelay( MS_to_TICKS( 10 ) );
						}

						// Request sync of statistics out to EEPROM
						SendSyncRequest( STATS_REGION );

						// Set up pointer to the command header structure
						pPutParmsMsg = (OP_PUT_PARMS_CMD_s *) pHdr;

						// Get the region #
						region = pPutParmsMsg->region;

						// Get pointer to structure with information about the region
						pInfo = get_bg_sync_info( region );

						// Get pointer to parameters in SRAM
						pParmData = pInfo->pSRAM;

						// Get size of parameter region
						bytes_to_copy = pInfo->size;

						// Calculate # bytes to copy from command message (allow 2 bytes for CRC16)
						put_data_size = sc_msglen - sizeof( OP_PUT_PARMS_CMD_s ) - 2;

						// Now bump the pointer so it points to the start of the parameter data
						pPutParmsMsg++;

						/////////////////////////////////////////////////////////////////////
						//	Only update parameters if a) the # bytes to copy is nonzero,
						//	b) a valid region was specified and c) the amount of parameter 
						//	data provided with the command matches the parameter region size
						/////////////////////////////////////////////////////////////////////
						if( bytes_to_copy && (put_data_size == bytes_to_copy ) )
						{
							// Valid region specified
							status_code = ACK;
						}
						else
						{
							// Invalid region specified
							status_code = NAK;
						}
						queue_simple_response( status_code );

						// Then start the 2-wire transmit 
						start_2_wire_transmit();

						if( status_code == ACK )
						{
							////////////////////////////////////////////
							// It's OK to perform the parameter update
							////////////////////////////////////////////
							portENTER_CRITICAL();
							memcpy( pParmData, pPutParmsMsg, bytes_to_copy );
							portEXIT_CRITICAL();

							// Make sure EEPROM gets sync'd for this region.
							// It won't happen until the ACK or NAK response gets sent
							SendSyncRequest( region );
						}
					}
					break;

				case OP_GET_PARMS:
					{
						U8 *pParmData;
						U8 bytes_to_send;
						OP_GET_PARMS_CMD_s *pGetParmsMsg;

						// bump the statistics counter
						Decoder_stats.rx_get_parms_msgs++;

						// Set up pointer to request structure
						pGetParmsMsg = (OP_GET_PARMS_CMD_s *) pHdr;

						bytes_to_send = 0;

						// Then check the region requested for validity
						if( pGetParmsMsg->region == COMMON_PARM_REGION )
						{
							pParmData = (U8 *) &CommonParms;
							bytes_to_send = sizeof(DECODER_COMMON_PARMS_s);
						}
						else if( pGetParmsMsg->region == SOL1_PARM_REGION )
						{
							pParmData = (U8 *) &Sol1Parms;
							bytes_to_send = sizeof(DECODER_PARMS_s);
						}
						else if( pGetParmsMsg->region == SOL2_PARM_REGION )
						{
							pParmData = (U8 *) &Sol2Parms;
							bytes_to_send = sizeof(DECODER_PARMS_s);
						}

						if( bytes_to_send )
						{
							// Queue up the header
							queue_header();

							// Queue up the parameter data
							while( bytes_to_send-- )
							{
								// Put next data byte in ring buffer, update CRC
								queue_val8( *pParmData++ );
							}

							// Queue up the CRC16 and DLE EOT bytes
							queue_trailer();
						}
						else
						{
							////////////////////////////////////////////////////
							//	Send error response: DLE STX NAK CRC16 DLE EOT
							////////////////////////////////////////////////////
							queue_simple_response( NAK );
						}

						// Then start the 2-wire transmit 
						start_2_wire_transmit();
					}
					break;

				case OP_STATS_CLEAR:

					/////////////////////////////////////////////////
					//	Send response: DLE STX ACK CRC16 DLE EOT
					/////////////////////////////////////////////////
					queue_simple_response( ACK );

					// Then start the 2-wire transmit 
					start_2_wire_transmit();

					// Clear stats in RAM and then sync them to EEPROM
					ClearStatistics();

					SendSyncRequest( STATS_REGION );
					break;

				case OP_DECODER_RST:

					///////////////////////////
					//	DECODER RESET COMMAND
					///////////////////////////

					// We need to force an EEPROM write to see these
					Decoder_stats.rx_dec_rst_msgs++;

					// See if there are any active solenoids
					if( bSolenoidsActive() )
					{
						// one or both solenoids are active

						// Command both solenoids OFF to allow sync to happen
						bSolEna[ SOL1 ] = FALSE;
						bSolEna[ SOL2 ] = FALSE;

						// Allow time for PWM state machine to process OFF commands
						vTaskDelay( MS_to_TICKS( 10 ) );
					}

					//////////////////////////////////////////////
					//	Send response: DLE STX ACK CRC16 DLE EOT
					//////////////////////////////////////////////
					queue_simple_response( ACK );

					// Then start the 2-wire transmit 
					start_2_wire_transmit();

					vTaskDelay(5);

					// Write statistics out to EEPROM
					SendSyncRequest( STATS_REGION );

					// Force a processor reset after response is transmitted
					// The reset will be delayed to allow the deferred SYNC
					// to occur.
					bDecoderResetPending = TRUE;
					break;

				case OP_STAT1_REQ:		// Fall-through intentionally
				case OP_STAT2_REQ:
					{
						U8 stat1;
						U8 stat2;
						U8 sol;
						SOL_CUR_MEAS_s temp_s;

						// Both status requests are counted by a single statistics counter
						Decoder_stats.rx_stat_req_msgs++;

						// Queue up the header
						queue_header();

						// Get the two solenoid PWM state machine states
						stat1 = SolPwmState_e[ SOL1 ];
						stat2 = SolPwmState_e[ SOL2 ];

						//////////////////////////////////////////////////////
						//	Format solenoid current values into the response
						//////////////////////////////////////////////////////

						// See if we are sending verbose status back
						if( pHdr->opcode == OP_STAT2_REQ )
						{
							/////////////////////////////////////
							// Yes we are: send STAT2_REQ_RSP_s	
							/////////////////////////////////////					
							for( sol = SOL1; sol <= SOL2; sol++ )
							{
								if( CurMeasState_e[ sol ] != CUR_MEAS_IN_PROG_e )
								{
									//////////////////////////////////////////////////////////////////////////
									//	We do this because we don't know when PWM ISR may update the current
									//	snapshot structure and we must be sure that we get coherent values.
									//////////////////////////////////////////////////////////////////////////
									portENTER_CRITICAL();
									temp_s = SolCurMeasSnapshot[ sol ];
									portEXIT_CRITICAL();
								}
								else
								{
									// Report values (all 1's) that mean NOT_READY
									temp_s.duty_cycle_acc = 0xffff;
									temp_s.dv_adc_cnts = 0xff;
									temp_s.seqnum = 0xff;
								}

								// This may not be the best way to send this structure
								queue_val8( temp_s.seqnum );
								queue_val8( temp_s.dv_adc_cnts );
								queue_val16( temp_s.duty_cycle_acc );
							}
						}

						// Put solenoid 1 state in ring buffer, update CRC
						queue_val8( stat1 );

						// Put solenoid 2 state in ring buffer, update CRC
						queue_val8( stat2 );

						// Put sys_flags in ring buffer, update CRC

						//	Note: we don't disable interrupts when reading sys_flags because all of its
						//	bits are updated atomically
						queue_val8( sys_flags );

						// Queue up the CRC16 DLE EOT bytes
						queue_trailer();

						// Then start the 2-wire transmit 
						start_2_wire_transmit();

						// Now clear the reset cause status bits in sys_flags
						sys_flags &= ~( SYS_FLAGS_REPORTED_ONCE_MASK);
					}
					break;

				case OP_SOL_CTL:
					{
						U8 op;					   // operation code
						U8 sol;					   // solenoid # (0 to n-1)
						U8 stat8;				   // return status
						U16 HbridgeCapAdcCnts;	   // H-Bridge voltage in ADC counts
						DECODER_PARMS_s *pParms;   // pointer to solenoid's parameters in RAM

						// Bump # of solenoid control messages in statistics
						Decoder_stats.rx_sol_ctl_msgs++;

						// Get the operation code from the command message data byte
						op = ( ( (SOL_CTL_MSG_s *) &sc_msg_bfr)->data ) & SOL_DATA_OP_MASK;

						// Get the solenoid number field from the command message data byte
						sol = (((SOL_CTL_MSG_s *) &sc_msg_bfr)->data & SOL_DATA_SOL_MASK)>>4;

						// Check for an invalid solenoid number
						// Only the first solenoid is "real" and controllable, hence the "- 1"
						if( sol >= NUM_SOLENOIDS - 1 )
						{
							// An invalid solenoid number was specified
							stat8 = RSP_SOL_NUMBER_INVALID;
						}
						else
						{
							// Initialize pointer to that solenoid's parameters
							pParms = (DECODER_PARMS_s *) &Sol1Parms;

							// Call function to check for possible errors
							stat8 = ValidateSolCtl( sol, op, pParms );
						}

						// Were no errors detected by ValidateSolCtl()?
						if( stat8 == RSP_STATUS_OK )
						{
							///////////////////////
							// NO ERRORS DETECTED
							///////////////////////
							switch( op )
							{
								case SOL_CTL_CMD_SOL_ON:

									//////////////////////////////////////////////////////////
									// SOLENOID TURN-ON OPERATION
									//	Handle turn-on requests by setting a flag that defers
									// the turn-on until the serial response has been sent
									//////////////////////////////////////////////////////////

									////////////////////////////////////////////////////////////////////
									//	Check that the H-bridge cap voltage meets the minimum required
									////////////////////////////////////////////////////////////////////

									// First read the current H-Bridge capacitor voltage (ADC counts).
									HbridgeCapAdcCnts = H_BridgeAdcCnts;

									// Are we above the minimum voltage for pull-in?
									if( HbridgeCapAdcCnts < pParms->MinHbrgCapPullInAdcCnts )
									{
										// NO. The H-bridge cap voltage is too low!
										stat8 = RSP_HBRG_VCAP_TOO_LOW;
									}
									else
									{
										/////////////////////////////////////////
										// The H-bridge capacitor voltage is OK
										/////////////////////////////////////////

										// So turn solenoid on (deferred)
										bSolEnaTmp[ sol ] = TRUE;
									}
									break;

								case SOL_CTL_CMD_SOL_OFF:

									////////////////////////////////
									// SOLENOID TURN-OFF OPERATION
									////////////////////////////////

									// Handle solenoid offs immediately
									bSolEna[ sol ] = FALSE;
									break;


								default: 
									// An undefined operation code was specified
									stat8 = RSP_SOL_OP_UNDEFINED;
									break;

							}  // end switch( data)

						} // end if( stat8 = RSP_STATUS_OK )

						////////////////////////////////////////////////////
						//	Queue up response: DLE STX STAT8 CRC16 DLE EOT
						////////////////////////////////////////////////////
						queue_simple_response( stat8 );

						// Then start the 2-wire transmit 
						start_2_wire_transmit();
					}
					break;

				case OP_STATS_REQ:

					//////////////////////
					//	Report Statistics 
					//////////////////////
					{
						U16 nByteStats;	// # statistics counters to report
						U8 *pCounter;	  // Pointer to a statistics counter
						U8 counter;

						// Bump statistics counter
						Decoder_stats.rx_stats_req_msgs++;

						// Queue up the header
						queue_header();

						// Handle the two 16-bit values first
						queue_val16 ( Decoder_stats.temp_maximum );
						queue_val16 ( Decoder_stats.temp_current );

						// Calculate # 8-bit statistics values to send. There are two
						// 16-bit values at the beginning of the structure that we don't count
						nByteStats = sizeof( DECODER_STATS_s ) - 4;

						// Point to start of 8-bit statistics in RAM
						pCounter = (U8 *) &Decoder_stats.por_resets;

						while( nByteStats-- )
						{
							/////////////////////////////////////////////
							//	Put the next statistics counter
							//	value into the ring buffer, update CRC.
							/////////////////////////////////////////////
							counter = *pCounter++;
							queue_val8 ( counter );
						}

						// Queue up the CRC16 DLE EOT bytes
						queue_trailer();

						// Then start the 2-wire transmit 
						start_2_wire_transmit();
					}
					break;

				case OP_GET_FLOW_CNTS:

					//////////////////////////////////
					// Get flow sensor pulse counts 
					//////////////////////////////////
					{
						U32		accumulated_ms;

						U32		accumulated_pulses;

						U16		latest_5_second_count;

						// Get elapsed time (milliseconds) and both flow sensor counts
						accumulated_ms = getFlowCounts( &accumulated_pulses, &latest_5_second_count );

						// Queue up the header
						queue_header();

						// ----------
						
						// Queue up the MS bytes of the 32-bit OS ticks count
						queue_val16( (U16)(accumulated_ms >>16) );

						// Queue up the LS bytes of the 32-bit OS tick count
						queue_val16( (U16)(accumulated_ms & 0xffff) );

						// ----------

						// Queue up the MS bytes of the 32-bit pulse count
						queue_val16( (U16)(accumulated_pulses >>16) );

						// Queue up the LS bytes of the 32-bit pulse count
						queue_val16( (U16)(accumulated_pulses & 0xffff) );

						// ----------
						
						// Queue up the 16-bit count for 5 second window
						queue_val16( latest_5_second_count );

						// Queue up the CRC16 DLE EOT bytes
						queue_trailer();

						// Then start the 2-wire transmit 
						start_2_wire_transmit();
					}
					break;

				case OP_RSTR_DFLTS:

					/////////////////////////////////////////
					// Restore [parameter] Defaults Command
					/////////////////////////////////////////
					{
						OP_RSTR_DFLTS_CMD_s *pMsg;
						U8 status8;

						// Set pointer to the command
						pMsg = (OP_RSTR_DFLTS_CMD_s *) &sc_msg_bfr;
						status8 = 0;

						// See if there are any active solenoids
						if( bSolenoidsActive() )
						{
							// one or both solenoids are active

							// Command both solenoids OFF to allow sync to happen
							bSolEna[ SOL1 ] = FALSE;
							bSolEna[ SOL2 ] = FALSE;

							// Allow time for PWM state machine to process OFF commands
							vTaskDelay( MS_to_TICKS( 10 ) );
						}

						// Now disable solenoid PWM outputs and interrupt
//                  enablePWM( FALSE );

						// Restore regions specified in the regions field
						if( pMsg->regions & EEP_COMMON_PARMS )
						{
							if( WriteParametersToEEPROM( COMMON_PARM_REGION, TRUE ) != CMD_SUCCESS )
							{
								status8 |= EEP_COMMON_PARMS;
							}
						}

						if( pMsg->regions & EEP_SOL1_PARMS )
						{
							if( WriteParametersToEEPROM( SOL1_PARM_REGION, TRUE ) != CMD_SUCCESS )
							{
								status8 |= EEP_SOL1_PARMS;
							}
						}

						if( pMsg->regions & EEP_SOL2_PARMS )
						{
							if( WriteParametersToEEPROM( SOL2_PARM_REGION, TRUE ) != CMD_SUCCESS )
							{
								status8 |= EEP_SOL2_PARMS;
							}
						}

						////////////////////////////////////////////////////
						//	Queue up response: DLE STX status8 CRC16 DLE EOT
						////////////////////////////////////////////////////
						queue_simple_response( status8 );

						// Then start the 2-wire transmit 
						start_2_wire_transmit();
					}
					break;

				case OP_SET_SN_REQ:

					//////////////////////////////
					//	Set Serial Number Command
					//////////////////////////////
					{
						OP_SET_SN_REQ_s *pMsg;

						pMsg   = (OP_SET_SN_REQ_s *) &sc_msg_bfr;

						//////////////////////////////////////////////////////////////////////////////
						// Check our current serial number: must be zero or we ignore this command.
						// If our SN is zero then check validity of new SN fields by comparing the 
						// complement of each byte with its ones complement. Each compare must 
						// pass.
						//////////////////////////////////////////////////////////////////////////////
						if(   ( Serial_Number.sn32 != 0x00000 )          ||
							  ( (U8) ~(pMsg->sn8[0]) != pMsg->sn8NOT[0]) ||
							  ( (U8) ~(pMsg->sn8[1]) != pMsg->sn8NOT[1]) ||
							  ( (U8) ~(pMsg->sn8[2]) != pMsg->sn8NOT[2]) )
						{
							/////////////////////////////////////////////////
							// Invalid command: ignore it and do not reply 
							/////////////////////////////////////////////////
							SerialCommInit();	 // Prepare to receive again
						}
						else
						{
							// See if there are any active solenoids
							if( bSolenoidsActive() )
							{
								// one or both solenoids are active

								// Command both solenoids OFF to allow sync to happen
								bSolEna[ SOL1 ] = FALSE;
								bSolEna[ SOL2 ] = FALSE;

								// Allow time for PWM state machine to process OFF commands
								vTaskDelay( MS_to_TICKS( 10 ) );
							}

							// Now disable solenoid PWM outputs and interrupt
//                     enablePWM( FALSE );

							//////////////////////////////////////////
							// This is a valid OP_SET_SN_REQ command
							//////////////////////////////////////////

							// Store the new serial number in our global RAM variable,
							// while adjusting for the big endian byte order
							Serial_Number.sn8[0] =  pMsg->sn8[2];
							Serial_Number.sn8[1] =  pMsg->sn8[1];
							Serial_Number.sn8[2] =  pMsg->sn8[0];

							// Write two copies of the new serial number to EEPROM
							// This is done synchronously with the direct call to the EEPROM function.
							if( WriteSerialNumbersToEEPROM( Serial_Number ) == 0 )
							{
								// The serial number was successfully written into EEPROM

								////////////////////////////////////////////////////
								//	Queue up response: DLE STX STAT8 CRC16 DLE EOT
								////////////////////////////////////////////////////
								queue_simple_response( ACK );

								// Then start the 2-wire transmit 
								start_2_wire_transmit();
							}
							else
							{
								// Do not reply
								SerialCommInit();	 // Prepare to receive again
							}

							// Re-enable the PWM interrupts
//                     enablePWM( TRUE );
						}
					}
					break;

				default:
					break;
			}
		}
		else
		{
			///////////////////////////////////////
			// UNICAST ADDRESS DID NOT MATCH OURS
			///////////////////////////////////////

			SerialCommInit();	// Prepare to receive again
		}
	}

	return;
}
