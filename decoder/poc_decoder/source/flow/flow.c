//////////////////////////////////////////////////////////////////////////
//
//	File:    flow.c
//
//          Calsense FM Series Flow Sensor support
//
// NOTE:    For LPC11E14 Microprocessor
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
// Synopsis:   The FM series flow sensor outputs a 5 ms wide pulse (logic 0) 
//             to the microprocessor's P0.22 pin. This pin is configured
//             as an input that generates an interrupt on each rising edge.
//
//             The handler for this interrupt is FLEX_INT1_IRQHandler and
//             is located in this file.
//
//////////////////////////////////////////////////////////////////////////
#include "LPC11Exx.h"
#include "LPC1000.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "gpio.h"
#include "sys_timers.h"
#include "flow.h"

// FreeRTOS includes
#include "FreeRTOS.h"
#include "task.h"

//////////////////////////////////////////////////////////////////////////////////////
//
//  DATA STRUCTURES
//
// These can all be made static (private) once the flow sensor code has been debugged)
//////////////////////////////////////////////////////////////////////////////////////

// Index (0..2) into FS_Sample[] where next one second pulse count is to be stored
static U32		pulse_1hz_index;

// The sample ring buffer for three one-second pulse counts
static U32		pulse_1hz_history[ ONE_SEC_FLOW_SAMPLES ];

// Used to calculate # pulses in last second
static U32		last_1hz_pulse_count;

// OS Tick time of last request
static U32		last_tick_count;

// 5/20/2014 rmd : Last register value read
static U32		last_pulse_count;


///////////////////////////////////////////////////////////////////////////////
//
//    U32 getFlowCounts( U32 *pCount32, U16 *pCounts3Sec )
//    
//    This function returns the number of milliseconds since the last request
//    was made, and via pointers, the number of flow sensor pulses (U32) that 
//    have been counted since this function was last called, and the number 
//    of pulses (U16) counted in the last 3 second running average window.
//
///////////////////////////////////////////////////////////////////////////////

extern U32 getFlowCounts( U32 *paccumulated_pulses, U16 *platest_5_second_count )
{
	static U32	FirstTime = TRUE;

	U32			pulse_count_now;

	U32			tick_count_now;

	U32			accumulated_ms;

	U32			latest_5_count;

	//////////////////////////////////////////////////////////
	// Set up initial OS tick count and ensure zero counts 
	// are reported the first time flow counts are requested
	//////////////////////////////////////////////////////////
	if( FirstTime )
	{
		// Remember that we have done this
		FirstTime = FALSE;

		// ----------
		
		portENTER_CRITICAL();

		// ----------
		
		// Initialize OS tick values
		tick_count_now = xTaskGetTickCount();
		
		last_tick_count = tick_count_now;

		// ----------
		
		// Read the initial counter value
		pulse_count_now = ((LPC_CTxxB0_Type*)LPC_CT32B0)->TC;
		
		last_pulse_count = pulse_count_now;
		
		// ----------

		// 6/9/2014 rmd : First time through - why not 0? Zero it is.
		latest_5_count = 0;
		
		// ----------
		
		portEXIT_CRITICAL();
	}
	else
	{
		portENTER_CRITICAL();

		// ----------
		
		// Get the current FreeRTOS tick count
		tick_count_now = xTaskGetTickCount();

		// ----------

		// then read the current flow sensor count
		pulse_count_now = ((LPC_CTxxB0_Type*)LPC_CT32B0)->TC;
		
		// ----------
		
		// Total the one second counters
		latest_5_count =	pulse_1hz_history[ 0 ] +
							pulse_1hz_history[ 1 ] +
							pulse_1hz_history[ 2 ] +
							pulse_1hz_history[ 3 ] +
							pulse_1hz_history[ 4 ];
	
		// ----------
		
		portEXIT_CRITICAL();
	}

	// ----------
	
	// 5/20/2014 rmd : Calculate the flow pulse counts since last requested to. What about
	// counter overflow? Well according to several internet postings even if the counter rolled
	// over the math will always work given the counter is 32 bits, the native micro int size is
	// 32 bits, we are using 32 bit variables, and the counter only rolled over once. All of
	// those conditions are met. So the subtraction always yields the number of pulses that
	// occurred since we last ran through this function! So directly store 32-bit flow sensor
	// pulse counts since last request.
	*paccumulated_pulses = (pulse_count_now - last_pulse_count);

	// Calculate number of OS ticks since last request
	accumulated_ms = (tick_count_now - last_tick_count);

	// ----------

	// Store 5 second count value
	*platest_5_second_count = (U16)(latest_5_count & 0xffff);

	// ----------
	
	// Record last count reported
	last_pulse_count = pulse_count_now;

	// Records OS tick count for next time
	last_tick_count = tick_count_now;
	
	// ----------
	
	// Return OS millisecond ticks since function last called
	return( accumulated_ms );
}

//////////////////////////////////////////////////////////////
//
//    void FlowSensorTask( void *pvParameters )
//
//    This FreeRTOS task handles the one second processing
//    of the flow sensor pulses.
//////////////////////////////////////////////////////////////
void FlowSensorTask( void *pvParameters )
{
	U32				pulse_count_now;

	portTickType	xLastWakeTime;

	U32				tmp_32;

	U16				tmp_16;
	
	// ----------
	
	// Initialize flow sensor data structures
	pulse_1hz_index = 0;
	
	pulse_1hz_history[ 0 ] = 0;
	pulse_1hz_history[ 1 ] = 0;
	pulse_1hz_history[ 2 ] = 0;
	pulse_1hz_history[ 3 ] = 0;
	pulse_1hz_history[ 4 ] = 0;
	
	last_1hz_pulse_count = 0;
	
	// ----------
	
	// Initialize CT32B0 to count rising edges of its CAP0 (FS_PULSE-) input
	init_flow_sensor_pulse_counter();

	// 5/21/2014 rmd : To initialize tracking variables run through first time here on startup.
	getFlowCounts( &tmp_32, &tmp_16 );

	// ----------
	
	// Initialize the xLastWakeTime variable with the current time.
	xLastWakeTime = xTaskGetTickCount();

	// ----------
	
	////////////////////////////////////////////////////////////
	// Perform flow sensor counter processing every 1000 ticks.
	////////////////////////////////////////////////////////////
	for( ;; )
	{
		// Wait for the next cycle.
		vTaskDelayUntil( &xLastWakeTime, FS_ONE_SEC_PERIOD_OS_TICKS );

		////////////////////////////////////////////
		// BEGIN ONE SECOND PULSE COUNT PROCESSING
		////////////////////////////////////////////

		// Read the current flow sensor count
		pulse_count_now = ((LPC_CTxxB0_Type*)LPC_CT32B0)->TC;

		// Calculate and save newest one second pulse count. Assumes the TC started out at 0 after
		// init function. Which it does.
		pulse_1hz_history[ pulse_1hz_index ] = (pulse_count_now - last_1hz_pulse_count);
		
		// Prepare for next count period
		last_1hz_pulse_count = pulse_count_now;

		// ----------
		
		// Calculate new storage index
		pulse_1hz_index++;

		if( pulse_1hz_index >= ONE_SEC_FLOW_SAMPLES )
		{
			pulse_1hz_index = 0;
		}

		//////////////////////////////////////////////
		// END OF ONE SECOND PULSE COUNT PROCESSING
		//////////////////////////////////////////////
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

