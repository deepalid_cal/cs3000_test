//////////////////////////////////////////////////////////////////////////
//
//	File:    flow.c
//
//          Calsense FM Series Flow Sensor support
//
// NOTE:    For LPC11E14 Microprocessor
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
// Synopsis:   The FM series flow sensor outputs a 5 ms wide pulse (logic 0) 
//             to the microprocessor's P0.22 pin. This pin is configured
//             as an input that generates an interrupt on each rising edge.
//
//             The handler for this interrupt is FLEX_INT1_IRQHandler and
//             is located in this file.
//
//////////////////////////////////////////////////////////////////////////

// ----------


// 1/29/2014 rmd : NOTE - this file is for REFERENCE ONLY. No longer part of the POC_DECODER
// build. It may be useful in the future as a pin change interrupt example.


// ----------

#include "LPC11Exx.h"
#include "LPC1000.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "gpio.h"
#include "flow.h"

// FreeRTOS includes
#include "FreeRTOS.h"
#include "task.h"

//////////////////////////////////////////////////
//
//  DATA STRUCTURES
//
// These can all be made static (private) 
// once the flow sensor code has been debugged)
//////////////////////////////////////////////////

// Index (0..2) into FS_Sample[] where next one second pulse count is to be stored
U8 FsIndex;

// The sample ring buffer for three one-second pulse counts
U32 FsSampleCnt[ ONE_SEC_FLOW_SAMPLES ];

U32 LastReportedCnt;    // Count last reported to TP Micro
U32 LastOneSecCnt;      // Used to calculate # pulses in last second
U32 LiveFsPulseCnt;     // Live FS pulse counter

//////////////////////////////////////////////////////////////////////////
//
//    U32 getFsPulsesSinceLastAsked(void)
//    
//    This function returns the number of flow sensor pulses that have
//    been counted since the last time this function was called.
//
//////////////////////////////////////////////////////////////////////////
U32 getFsPulsesSinceLastAsked(void)
{
   U32 count;

   portENTER_CRITICAL();

   // Calculate new count since last asked
   count = LiveFsPulseCnt - LastReportedCnt;

   // Record last count reported
   LastReportedCnt = LiveFsPulseCnt;

   portEXIT_CRITICAL();

   return count;
}

//////////////////////////////////////////////////////////////////////////
//
//    U16 getThreeSecondPulseCount(void)
//    
//    This function returns the number of flow sensor pulses that have
//    been counted over the most recent three second counting window.
//
//////////////////////////////////////////////////////////////////////////
U16 getThreeSecondPulseCount(void)
{
   U32 count;

   portENTER_CRITICAL();

   // Total the one second counters
   count = FsSampleCnt[ 0 ] + FsSampleCnt[ 1 ] + FsSampleCnt[ 2 ];

   portEXIT_CRITICAL();

   // Truncate to 16-bits (maximum count should only be around 600)
   return (U16) (count & 0xffff);
}

//////////////////////////////////////////////////////////////////////////
//
//    void initFlowSensorPinInterrupt(void)
//    
//    This function initializes the P0.22 pin change interrupt for
//    counting the FS_PULSE- signal pulses. A FLEX_INT1_IRQn interrupt is 
//    generated each rising edge of the FS_PULSE- signal and these
//    interrupts are counted to keep track of water flow.
//
//    The interrupt handler for the FLEX_INT1_IRQn is function  
//    FLEX_INT1_IRQHandler() and is in this file.
//
//////////////////////////////////////////////////////////////////////////
void initFlowSensorPinInterrupt(void)
{
   NVIC_DisableIRQ( FLEX_INT1_IRQn );

   // Select P0.17 for pin interrupt selection 1
   LPC_SYSCON->PINTSEL[ FS_PULSE_PINT_SEL ] = INTPIN_P0_17;

   // Enable rising edge interrupts for P0.22 (FS_PULSE- signal)
   LPC_GPIO_PIN_INT->SIENR = 1<<FS_PULSE_PINT_SEL;

   // Clear rising edge interrupt detection for pin interrupt 1 (FS_PULSE-)
   LPC_GPIO_PIN_INT->RISE = 1<<FS_PULSE_PINT_SEL;

   // Set P0.22 pin-change interrupt priority to 3 (lowest) 
   // This is configured in Calsense_2_Wire_decoder.h
   NVIC_SetPriority( FLEX_INT1_IRQn, FS_PULSE_PIN_CHANGE_NVIC_INT_PRIORITY );

   // Initialize flow sensor data structures
   FsIndex          = 0;
   FsSampleCnt[ 0 ] = 0L;
   FsSampleCnt[ 1 ] = 0L;
   FsSampleCnt[ 2 ] = 0L;
   LastReportedCnt  = 0L;
   LiveFsPulseCnt   = 0L;
   LastOneSecCnt    = 0L;

   // Enable the P0.22 pin change interrupt
   NVIC_EnableIRQ( FLEX_INT1_IRQn );
}

//////////////////////////////////////
//    Interrupt Service Routine for
//    Pin Interrupt Selection 1
//////////////////////////////////////
void FLEX_INT1_IRQHandler(void)
{
   // Check for rising edge detected on pin interrupt 1
   if( LPC_GPIO_PIN_INT->RISE & ( 1<<FS_PULSE_PINT_SEL ) )
   {
      // Increment the flow sensor pulse accumulator (live pulse counter)
      LiveFsPulseCnt++;

      // Clear pending interrupt for pin interrupt 1
      LPC_GPIO_PIN_INT->IST = ( 1<<FS_PULSE_PINT_SEL );
   }
}

//////////////////////////////////////////////////////////////
//
//    void FlowSensorTask( void *pvParameters )
//
//    This FreeRTOS task handles the one second processing
//    of the flow sensor pulses.
//////////////////////////////////////////////////////////////
void FlowSensorTask( void *pvParameters )
{
U32 count;

 portTickType xLastWakeTime;
 const portTickType xFrequency = FS_ONE_SEC_PERIOD_OS_TICKS;

   // Initialize the pin change interrupt for the FS_PULSE- signal
   initFlowSensorPinInterrupt();

   // Initialize the xLastWakeTime variable with the current time.
   xLastWakeTime = xTaskGetTickCount();

   ////////////////////////////////////////////////////////////
   // Perform flow sensor counter processing every 1000 ticks.
   ////////////////////////////////////////////////////////////
   for( ;; )
   {
      // Wait for the next cycle.
      vTaskDelayUntil( &xLastWakeTime, xFrequency );

      ////////////////////////////////////////////
      // BEGIN ONE SECOND PULSE COUNT PROCESSING
      ////////////////////////////////////////////
      portENTER_CRITICAL();

      // Calculate newest one second pulse count
      count = LiveFsPulseCnt - LastOneSecCnt;

      // Store it
      FsSampleCnt[ FsIndex ] = count;

      // Prepare for next count period
      LastOneSecCnt = LiveFsPulseCnt;

      portEXIT_CRITICAL();

      // Calculate new storage index
      FsIndex++;
      if( FsIndex >= ONE_SEC_FLOW_SAMPLES )
      {
         FsIndex = 0;
      }

      //////////////////////////////////////////////
      // END OF ONE SECOND PULSE COUNT PROCESSING
      //////////////////////////////////////////////
   }
}

