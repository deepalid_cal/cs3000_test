//////////////////////////////////////////////////////////////////////////
//
//	File:    flow.h
//
//          Flow Sensor support header file
//
// NOTE:    For LPC11E14 Microprocessor
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
//////////////////////////////////////////////////////////////////////////

// The # FreeRTOS ticks for each one second count period
#define FS_ONE_SEC_PERIOD_OS_TICKS		MS_to_TICKS(1000)

// The number of one second pulse sample periods used for a flow measurement
#define ONE_SEC_FLOW_SAMPLES			(5)


// ----------

extern void FlowSensorTask( void *pvParameters );

extern U32 getFlowCounts( U32 *paccumulated_pulses, U16 *platest_5_second_count );

extern U32 getFsPulsesSinceLastAsked(void);

extern U16 getThreeSecondPulseCount(void);


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

