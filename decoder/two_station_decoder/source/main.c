#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "ProtocolMsgs.h"
#include "RomStructs.h"       // ROM functions header file
#include "gpio.h"
#include "uart.h"
#include "adc.h"
#include "SerialComm.h"
#include "sys_timers.h"
#include "sol_pwm.h"
#include "iap.h"
#include "crc16.h"
#include "eep.h"
#include "BG_EEP_Sync.h"

// FreeRTOS includes
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

// Misc. includes
#include "string.h"     
//#include "stddef.h"     // for offsetof (happens to be #included by FreeRTOS.h)
#include "stdio.h"

//////////////////////////////////////////////////////////////////////////
//    GLOBAL DECODER VARIABLES
//////////////////////////////////////////////////////////////////////////

// TEMP FOR DEBUGGING
const char  *pFunctionName;      // TEMP
U32 line_num;                    // TEMP

//////////////////////////////////////////////////////////////////////////
//    Decoder Core Data (FLASH) - these are constants
//////////////////////////////////////////////////////////////////////////
const DECODER_CORE_DATA_s CoreData =
{
   DECODER_TYPE_CODE,
   DECODER_FW_VERSION
};

//////////////////////////////////////////////////////////////////////////
//    The Decoder Serial Number (20-bits)
//////////////////////////////////////////////////////////////////////////
SER_NUM_u Serial_Number;

//////////////////////////////////////////////////////////////////////////
//    ACTIVE PARAMETERS 
//
//    These are field-modifiable via commands over the 2-Wire network
//////////////////////////////////////////////////////////////////////////
volatile DECODER_COMMON_PARMS_s  CommonParms ALIGN4;      // field modifiable
volatile DECODER_PARMS_s         Sol1Parms ALIGN4;        // field modifiable
volatile DECODER_PARMS_s         Sol2Parms ALIGN4;        // field modifiable

//////////////////////////////////////////////////////////////////////////
//	1 KHz timers for solenoid state machines and the task timer.
//	These are declared "volatile" since they're decremented in an ISR.
// In this implementation, the FreeRTOS tick hook is used for this.
//////////////////////////////////////////////////////////////////////////
volatile U8 sol_sm_timer[ NUM_SOLENOIDS ] = { 0, 0, };

//////////////////////////////////////////////////////////////////////////
//    System Flags (8-bits) - reported in both status responses
//
//    See file ProtocolMsgs.h for the System Flags bit definitions
//////////////////////////////////////////////////////////////////////////
volatile U8 sys_flags = 0;

//////////////////////////////////////////////////////////////////////////
//    SYSRSTSTAT register value at startup - this is used during the
//    initialization to determine what caused the last reset.
//////////////////////////////////////////////////////////////////////////
U8 sysrststat;

//////////////////////////////////////////////////////////////////////////
//    Flag for Decoder Reset - if this flag is set by message processing 
//    in the process_sc_msg() function, a microprocessor reset is 
//    generated once an async message response has been sent.
//////////////////////////////////////////////////////////////////////////
volatile BOOL bDecoderResetPending = FALSE;

//////////////////////////////////////////////////////////////////////////
//    The RAM-resident decoder statistics counters
//
//    These are written to EEPROM periodically if any have changed (see 
//    define SYNC_STATS_INTERVAL_MINUTES in Calsense_2-Wire_Decoder.h).
//
//    The statistics are also written to EEPROM if the last reset was due
//    to a system anomaly ( such as BOR, WDT ) or a POR.
//////////////////////////////////////////////////////////////////////////
volatile DECODER_STATS_s Decoder_stats ALIGN4;	

//////////////////////////////////////////////////////////////////////////
//    Queue handle for the Background Sync request queue.
//////////////////////////////////////////////////////////////////////////
xQueueHandle xBG_Sync_Req_Queue;

//////////////////////////////////////////////////////////////////////////
//
//    Temperature measurement-related variables
//
//    BOOL bTempAvgReady - Set within pwm.c when a new average temp
//                         measurement is available to be used.
//                         Cleared within process_temp() when the new
//                         measurement is processed.
//                      
//    U16 TempAvgCnts   -  the averaged temp value, in ADC counts
//
//////////////////////////////////////////////////////////////////////////

// Flag that indicates a new average temperature value is ready
volatile BOOL bTempAvgReady;

// The temperature value (averaged over 1024 ADC readings)
volatile U16 TempAvgCnts;

// RAM-to-EEPROM syncs that cannot be done immediately are
// recorded in this variable. Each bit # represents a region
// that needs a sync operation. These are performed in the
// sync processing loop in the SysTask.
static U8 deferred_sync_requests;

//////////////////////////////////////////////////////////////////////////
//    LPC11E14-based Decoder I/O Configuration - for reference
//
//    NOTE: This pin configuration is set up in gpio.c.
//////////////////////////////////////////////////////////////////////////

//   PIN    SIGNAL      PORT.BIT    DESCRIPTION
//   ===    ========    =======     ========================
//    14    HBRG_RT     P0.3        H-Bridge MOSFET Gate drive 
//    22    HBRG_RB     P0.6        H-Bridge MOSFET Gate drive
//    23    HBRG_LT     P0.7        H-Bridge MOSFET Gate drive
//    27    HBRG_LB     P0.8        H-Bridge MOSFET Gate drive
//
//    32    VSOL1       P0.11       Solenoid 1 voltage
//    33    VSOL2       P0.12       Solenoid 2 voltage
//    34    VHBRG       P0.13       H-Bridge voltage
//    35    VTHERM      P0.14       Thermistor voltage
//    
//    36    PWM_SOL1    P1.13       CT16B0_MAT0
//    37    PWM_SOL2    P1.14       CT16B0_MAT1
//
//    18    CAPCON_SOL1 P1.23       Connects SOL1 capacitor for charging
//    21    CAPCON_SOL2 P1.24       Connects SOL2 capacitor for charging
//
//    11    RXD         P1.26       UART RXD
//    12    TXD/TP1     P1.27       UART TXD (Test point 1)
//
//    1     TP2         P1.25       Test point 2
//    9     TP3         P0.20       Test point 3
//    17    TP4         P0.21       Test point 4
//    24    TP5         P1.28       Test point 5
//    30    TP6         P0.22       Test point 6
//    42    TP7         P0.23       Test point 7

//////////////////////////////////////////////////////////////////////////
//    Variables used by the Power Profiles driver
//////////////////////////////////////////////////////////////////////////

// Address of the internal ROM of the LPC11E14. LPC_1XXX_ROM_LOC is #defined in LPC11Exx.h
static ROM **rom = (ROM **) LPC_1XXX_ROM_LOC;    

// Power profiles command/input parameters
static unsigned int command[4];
     
// Power profiles output/result parameters
static unsigned int result[2];

//////////////////////////////////////////////////////////////////////////
//    Fatal error trap - useful mainly during firmware development 
//////////////////////////////////////////////////////////////////////////
void Fatal_Error(U32 error)
{
	volatile int err;

	err = error;

	taskDISABLE_INTERRUPTS();
	while(1);
}

void ClearStatistics(void)
{
   memset( (void *) &Decoder_stats, 0, sizeof( Decoder_stats ) );
   Decoder_stats.temp_maximum = TEMP_MIN_READING_CNTS;
}

///////////////////////////////////////////////////////////////////
//
//    BOOL bSolenoidsActive(void)
//
//    This function returns TRUE unless both solenoids are either
//    in the OFF state or the FAILED state.
///////////////////////////////////////////////////////////////////
BOOL bSolenoidsActive(void)
{
   BOOL bActive;

   bActive = ( ( SolPwmState_e[0] != SOL_OFF_e ) && ( SolPwmState_e[0] != SOL_EXCESSIVE_CURRENT_e ) ) ||
             ( ( SolPwmState_e[1] != SOL_OFF_e ) && ( SolPwmState_e[1] != SOL_EXCESSIVE_CURRENT_e ) );
   
   return (bActive);
}

//////////////////////////////////////////////////////////////////////////
//
//    void process_temp(void)
//
//    This function is called from a task to process the latest
//    temperature average computed by the PWM ISR.
//
//    Processing Outline:
//
//    1. Check the bTempAvgReady flag to see if a new temp average is
//       available.
//    2. If the bTempAvgReady flag is FALSE, exit the function, otherwise
//       continue at step 3.
//    3. Begin critical section 
//    4. Clear the bTempAvgReady flag.
//    5. Update the current temperature value in the RAM-based statistics
//    6. If the current temperature count value represents a higher
//       temperature than the current "high water mark" temperature, update the
//       maximum temperature measured value with the current temperature value.
//    7. End critical section
//
/////////////////////////////////////////////////////////////////////////////////
void process_temp(void)
{

   // Process temperature if a new average is available from the PWM ISR
   if( bTempAvgReady == TRUE )
   {
      // A new average temperature measurement is available
      portENTER_CRITICAL();

      // Clear the measurement ready flag
      bTempAvgReady = FALSE;

      // Update the current temperature in statistics
      Decoder_stats.temp_current = TempAvgCnts;

      // If current temp is higher then present temp_maximum,, update temp_maximum
      if( ( Decoder_stats.temp_maximum == 0 ) || ( TempAvgCnts < Decoder_stats.temp_maximum ) )
      {
         Decoder_stats.temp_maximum = TempAvgCnts;
      }

      portEXIT_CRITICAL();
   }
}

//////////////////////////////////////////////////////////////////////////
// 
//    void SendSyncRequest( U8 region_number )
//
//    U8 region_number:  0 to 3
//
//    This function is called to request that a RAM-resident region of
//    structures be written to EEPROM.
//
//    This function is called by process_sc_msg() function or a PWM state
//    machine if a parameter region in RAM needs to be written to EEPROM.
//
//    This function sends a single byte message to the background sync
//    request queue. This FreeRTOS queue is serviced by the System Task 
//    once its initialization duties are complete.
//////////////////////////////////////////////////////////////////////////
void SendSyncRequest( U8 region_number )
{
   portBASE_TYPE status;

   if( xQueueSend( xBG_Sync_Req_Queue, (void *) &region_number, 0 ) != pdTRUE )
   {
      Fatal_Error( SEND_SYNC_Q_SEND_ERROR );
   }
}

//////////////////////////////////////////////////////////////////////////
//
//    void vApplicationTickHook(void)
//
//    This function is called at a 1 KHz rate by the SYSTICK ISR and is 
//    used to maintain the software timers used by the PWM state machines.
//    If also handles the timing of the statistics sync to EEPROM.
//////////////////////////////////////////////////////////////////////////
void vApplicationTickHook(void)
{
   static U32 bg_sync_timer;
   static U8 const bg_sync_cmd = STATS_REGION;
   portBASE_TYPE status;

	//////////////////////////////////////
	// Decrement active (non-zero) timers
	//////////////////////////////////////
	if( sol_sm_timer[ SOL1 ] ) sol_sm_timer[ SOL1 ]--;
	if( sol_sm_timer[ SOL2 ] ) sol_sm_timer[ SOL2 ]--;

   // Increment the background sync timer
   bg_sync_timer++;

   if( bg_sync_timer >= SYNC_STATS_INTERVAL_1_KHZ_TICKS )
   {
      // Send SYNC STATS message to the background sync request queue
      status = xQueueSendFromISR( xBG_Sync_Req_Queue, (void *) &bg_sync_cmd, NULL );   // Is NULL OK here?

      bg_sync_timer = 0;   // reset the software timer
   }
}

//////////////////////////////////////////////////////////////////////////
//
//    void init_txd_edge_int(void)
//    
//    This function initializes the P1.27 pin change interrupt for
//    monitoring the TXD output pin. A FLEX_INT0_IRQn interrupt is 
//    generated for both rising and falling edges of the TXD signal and 
//    these are used to initiate the H-Bridge MOSFET gate drive signal 
//    changes.
//
//    The H-Bridge gate drive code is table driven and can be found in
//    source/h-bridge_tx/h-bridge_tx.c.
//
//    The interrupt handler for the FLEX_INT0_IRQn is function  
//    FLEX_INT0_IRQHandler() and is in h-bridge_tx.c.
//
//////////////////////////////////////////////////////////////////////////
void init_txd_edge_int(void)
{
   NVIC_DisableIRQ( FLEX_INT0_IRQn );

   // Select P1.27 for pin interrupt selection 0
   LPC_SYSCON->PINTSEL[0] = INTPIN_P1_27;

   // Enable interrupts for both edges of P1.27
   LPC_GPIO_PIN_INT->ISEL    = 0;   // Sets pin interrupt mode to edge sensitive
   LPC_GPIO_PIN_INT->IENR    = 1;   // Enable rising edge interrupt
   LPC_GPIO_PIN_INT->SIENR   = 1;   // Write to enable rising edge interrupt
   LPC_GPIO_PIN_INT->IENF    = 1;   // Enable falling edge interrupt
   LPC_GPIO_PIN_INT->SIENF   = 1;   // Write to enable falling edge interrupt

   // Set P1.27 pin-change interrupt priority to 0 (highest) 
   // This is configured in Calsense_2_Wire_decoder.h
   NVIC_SetPriority( FLEX_INT0_IRQn, TXD_PIN_CHANGE_NVIC_INT_PRIORITY );

   // Enable the pin change interrupt
   NVIC_EnableIRQ( FLEX_INT0_IRQn );
}

//////////////////////////////////////////////////////////////////////////
//
//    void hw_init1(void)
//
//    This performs hardware initialization of the micro pins and 
//    peripherals. None of this initialization relies upon values
//    read from EEPROM.
//////////////////////////////////////////////////////////////////////////
void hw_init1(void)
{
   // Force CMSIS to determine actual clock rate and store it
   // The UARTInit() function relies upon this having been done.
   SystemCoreClockUpdate();

   ////////////////////////////////////////////////////////////
   // Low-current mode PWR_LOW_CURRENT selected when running
   // the set_power routine in the power profiles.
   ////////////////////////////////////////////////////////////
   //    call set_power() 
   command[0] = 12;                 // main clock (MHz) = 12 MHz
   command[1] = PWR_LOW_CURRENT;    // select profile for low current
   command[2] = 12;                 // system clock (MHz) = 12 MHz

   // result[0] = status code
   (*rom)->pPWRD->set_power( command, result );

   if( result[0] != PWR_CMD_SUCCESS )
   {
      // Should never get here
      Fatal_Error( SET_POWER_ERROR );
   }

   // Initialize all of the pin functions
   init_pins();

   // Initialize the TXD pin change interrupt (both edges gen. int.)
   init_txd_edge_int();

#if ( USE_SW_TIMER_FOR_H_BRIDGE_TIMING == 0 )

   // Initialize the hardware timer (used for H-Bridge 40 us. delay)
   init_us_timer();
#endif

   // initialize ADC
   init_adc( CPU_XTAL_CLK_HZ );   // Arg is PCLK in Hz
}

//////////////////////////////////////////////////////////////////////////
//
//    void hw_init2(void)
//
//    This function initializes the PWM hardware and state machine.
//    This initialization needs configuration/parameter values that have
//    been read from EEPROM.
//////////////////////////////////////////////////////////////////////////
void hw_init2(void)
{
   initPWM();
}

//////////////////////////////////////////////////////////////////////////
//
//    void DebugTask( void *pvParameters )
//
//    This FreeRTOS task is intended to be used only during development
//
//////////////////////////////////////////////////////////////////////////
#ifdef DEBUG
static S8 mybuf[160];

void DebugTask( void *pvParameters )
{
   // Display the configured serial number one time
   sprintf( mybuf, "Configured Serial Number is: 0x%05X\n\r\n\r", Serial_Number.sn32);
   debug_printf( mybuf );
   vTaskDelay(MS_to_TICKS(5000) );

   while(1)
   {
      // Show FreeRTOS Task information
      vTaskList( mybuf );
      debug_printf( mybuf );

      // Short delay
      vTaskDelay(MS_to_TICKS(2000) );
   }
}
#endif

//////////////////////////////////////////////////////////////////////////
// Static Data used by SysTask
//////////////////////////////////////////////////////////////////////////
static U8 sync_req_msg;
static SER_NUM_s SerialNumber[2];    // values read from EEPROM during initialization

void SystemTask( void *pvParameters )
{
   portBASE_TYPE stat;           /// FreeRTOS status (long)
   U32 status;
   void *p;
   U16 crc16;

   // Turn off interrupts during system initialization
	portENTER_CRITICAL();

   ///////////////////////////////////////////////////////////////////////
   //    THIS CODE IS TO BE ENABLED ONLY DURING TESTING!!!!
   ///////////////////////////////////////////////////////////////////////
#if FORCE_SN_TO_ZERO != 0
#warning the #define FORCE_SN_TO_ZERO is enabled!

   Serial_Number.sn32 = 0;
   status = WriteSerialNumbersToEEPROM( Serial_Number );

#endif

   // Read the two serial number structures from EEPROM
   stat = EEPROM_Read( (U8 *) &SerialNumber, EEPROM_SER_NUMS_OFFSET, sizeof(SerialNumber) );

   // See if first serial number passes validity check
   if( SerialNumber[0].ser_num_u.sn32 == ~SerialNumber[0].ser_num_u_not.sn32 )
   {
      // First serial number in EEPROM looks valid, so let's use it (store in RAM variable)
      Serial_Number.sn32 = SerialNumber[0].ser_num_u.sn32;
   }
   else if( SerialNumber[1].ser_num_u.sn32 == ~SerialNumber[1].ser_num_u_not.sn32 )
   {
      // First serial number in EEPROM was not valid, but
      // second serial number in EEPROM looks valid, so let's use it (store in RAM variable)
      Serial_Number.sn32 = SerialNumber[1].ser_num_u.sn32;

      // Write this serial number out to EEPROM to fix first serial number value
      SerialNumber[0].ser_num_u.sn32     = SerialNumber[1].ser_num_u.sn32;
      SerialNumber[0].ser_num_u_not.sn32 = SerialNumber[1].ser_num_u_not.sn32;
      status = EEPROM_Write( EEPROM_SER_NUMS_OFFSET, (U8 *) &SerialNumber, sizeof(SerialNumber[0]) );
   }
   else
   {
      //////////////////////////////////////////////////////////////////
      // Neither serial number structure in EEPROM was valid. This is
      // usually just because it's the first time the program has run.
      /////////////////////////////////////////////////////////////////

      // Set serial number to zero for initial programming at Calsense
      Serial_Number.sn32 = 0;
      
      // Initialize the two copies of the serial number in EEPROM (with check bits)
      status = WriteSerialNumbersToEEPROM( Serial_Number );

      ////////////////////////////////////////////////////////
      // Copy default common parameters from Flash into RAM
      // and write them into EEPROM
      ////////////////////////////////////////////////////////
      status = WriteParametersToEEPROM( COMMON_PARM_REGION, TRUE );

      ///////////////////////////////////////////////////////////
      // Copy default solenoid 1 parameters from Flash into RAM
      // and write them into EEPROM
      ///////////////////////////////////////////////////////////
      status = WriteParametersToEEPROM( SOL1_PARM_REGION, TRUE );

      ///////////////////////////////////////////////////////////
      // Copy default solenoid 2 parameters from Flash into RAM
      // and write them into EEPROM
      ///////////////////////////////////////////////////////////
      status = WriteParametersToEEPROM( SOL2_PARM_REGION, TRUE );

      /////////////////////////////////////////////////////////
      // Write cleared statistics counter values to EEPROM
      // and write them into EEPROM
      /////////////////////////////////////////////////////////
      status = WriteParametersToEEPROM( STATS_REGION, TRUE );
   }

   ///////////////////////////////////////////////////////////////////
   // Perform normal start-up checks of the EEPROM region CRC values
   //
   // Any errors will be counted by the statistics in RAM then written
   // back out to EEPROM at the end of this start-up code if the
   // sysflags indicates any errors have occurred.
   ///////////////////////////////////////////////////////////////////

   // Get the CRC for the statistics counters in EEPROM
	status = EEPROM_Read( (U8 *) &crc16, EEPROM_CRC16_STATS_OFFSET, sizeof(crc16) );

   // Is the CRC valid?
   if( crc16 == crc16_eeprom_region( EEPROM_STATS_OFFSET, sizeof(Decoder_stats) ) )
   {
      // The CRC is valid so go ahead and load the statistics into RAM
      status = EEPROM_Read( (U8 *) &Decoder_stats, EEPROM_STATS_OFFSET, sizeof(Decoder_stats) );
   }
   else
   {
      // Note the CRC error
		sys_flags |= SYS_FLAG_EEPROM_CRC_ERR;

      // Clear statistics counters in RAM
      memset((void *) &Decoder_stats, 0, sizeof(Decoder_stats) );

      // Bump stats counter for EEPROM CRC ERROR
		Decoder_stats.eep_crc_err_stats++;
   }

   // Get the CRC for the common parameters in EEPROM
	status = EEPROM_Read( (U8 *) &crc16, EEPROM_CRC16_COMMON_PARMS_OFFSET, sizeof(crc16) );

   // Is the CRC valid?
   if( crc16 == crc16_eeprom_region( EEPROM_COMMON_PARMS_OFFSET, sizeof(CommonParms) ) )
   {
      // The CRC is valid so go ahead and load the common parameters into RAM
      status = EEPROM_Read( (U8 *) &CommonParms, EEPROM_COMMON_PARMS_OFFSET, sizeof(CommonParms) );
   }
   else
   {
		// Note the CRC error 
		sys_flags |= SYS_FLAG_EEPROM_CRC_ERR;

		// Bump stats counter for EEPROM COM PARMS CRC ERROR
		Decoder_stats.eep_crc_err_com_parms++;

      //////////////////////////////////////////////////////////////////////////
      // Restore default common parameters from Flash into RAM and into EEPROM
      //////////////////////////////////////////////////////////////////////////
      status = WriteParametersToEEPROM( COMMON_PARM_REGION, TRUE );
   }
   
   // Check to see if this decoder has been discovered
   if( CommonParms.bDiscovered == TRUE )
   {
      // Yes, so indicate this in the system flags
      sys_flags |= SYS_FLAG_DISCOVERED;
   }

   // Get the CRC for solenoid 1 parameters in EEPROM
	status = EEPROM_Read( (U8 *) &crc16, EEPROM_CRC16_SOL1_PARMS_OFFSET, sizeof(crc16) );

   // Is the CRC valid?
   if( crc16 == crc16_eeprom_region( EEPROM_SOL1_PARMS_OFFSET, sizeof(Sol1Parms) ) )
   {
      // The CRC is valid so go ahead and load the sol 1 parameters into RAM
      status = EEPROM_Read( (U8 *) &Sol1Parms, EEPROM_SOL1_PARMS_OFFSET, sizeof(Sol1Parms) );
   }
   else
   {
		// Note the CRC error 
		sys_flags |= SYS_FLAG_EEPROM_CRC_ERR;

		// Bump stats counter for EEPROM SOL1 PARMS CRC ERROR
		Decoder_stats.eep_crc_err_sol1_parms++;

      //////////////////////////////////////////////////////////////////////////
      // Restore default sol 1 parameters from Flash into RAM and into EEPROM
      //////////////////////////////////////////////////////////////////////////
      status = WriteParametersToEEPROM( SOL1_PARM_REGION, TRUE );
   }

   // Get the CRC for solenoid 2 parameters in EEPROM
	status = EEPROM_Read( (U8 *) &crc16, EEPROM_CRC16_SOL2_PARMS_OFFSET, sizeof(crc16) );

   // Is the CRC valid?
   if( crc16 == crc16_eeprom_region( EEPROM_SOL2_PARMS_OFFSET, sizeof(Sol2Parms) ) )
   {
      // The CRC is valid so go ahead and load the sol 2 parameters into RAM
      status = EEPROM_Read( (U8 *) &Sol2Parms, EEPROM_SOL2_PARMS_OFFSET, sizeof(Sol2Parms) );
   }
   else
   {
		// Note the CRC error 
		sys_flags |= SYS_FLAG_EEPROM_CRC_ERR;

		// Bump stats counter for EEPROM SOL2 PARMS CRC ERROR
		Decoder_stats.eep_crc_err_sol2_parms++;

      //////////////////////////////////////////////////////////////////////////
      // Restore default sol 2 parameters from Flash into RAM and into EEPROM
      //////////////////////////////////////////////////////////////////////////
      status = WriteParametersToEEPROM( SOL2_PARM_REGION, TRUE );
   }

   ////////////////////////////////////////
   // Update statistics for reset causes
   ////////////////////////////////////////
   if( sysrststat & SYSRSTSTAT_POR )
   {
      // Power-on reset (POR)
      Decoder_stats.por_resets++;
      sys_flags |= SYS_FLAG_POR;
   }
   else if( sysrststat & SYSRSTSTAT_WDT )
   {
      // WDT reset (WDR)
      Decoder_stats.wdt_resets++;
      sys_flags |= SYS_FLAG_WDR;
   }
   else if( sysrststat & SYSRSTSTAT_BOD )
   {
      // BOD reset (BOR)
      Decoder_stats.bod_resets++;
      sys_flags |= SYS_FLAG_BOR;
   }

   // If the last reset was caused by a POR, BOR, or WDR, then write the statistics out to EEPROM
   if( sys_flags & (  SYS_FLAG_POR | SYS_FLAG_BOR | SYS_FLAG_WDR ) )
   {
      status = WriteParametersToEEPROM( STATS_REGION, FALSE );
   }

   // Initialize the PWM hardware (PWM init needs values read from EEPROM)
   hw_init2();

   // Create the serial comm task
   stat = xTaskCreate(  SerialCommTask,
                        (signed char *) "SerComm",
                        configMINIMAL_STACK_SIZE + 64,   // was + 0
                        NULL,
                        configMAX_PRIORITIES - 2,  //  Priority
                        NULL);
   
   if( stat != pdPASS )
   {
      // Error creating task
      Fatal_Error( SER_COMM_TASK_CREATE_ERROR );
   }

#ifdef DEBUG
   // Create the debug information task
   stat = xTaskCreate(  DebugTask,
                        (signed char *) "DbgTask",
                        configMINIMAL_STACK_SIZE + 0,
                        NULL,
                        configMAX_PRIORITIES - 2,  //  Priority
                        NULL);
   
   if( stat != pdPASS )
   {
      // Error creating task
      Fatal_Error( DEBUG_TASK_CREATE_ERROR );
   }
#endif

   ///////////////////////////
   // Enable interrupts now
   // ////////////////////////
  	portEXIT_CRITICAL();

   ////////////////////////////////////
   //    BACKGROUND PROCESSING LOOP
   ////////////////////////////////////

   while(1)
   {
      // This variable contains a bit (1<<region) for each region that needs 
      // a sync but that cannot be done because of PWM or TXD activities.
      U8 cnt;

      // Wait here to receive a Background Sync request message
      stat = xQueueReceive( xBG_Sync_Req_Queue, (void *) &sync_req_msg, MS_to_TICKS(100) );

      if( stat == pdTRUE )
      {
         ///////////////////////////////////////
         // We received a sync request message
         ///////////////////////////////////////

         // See if sync not possible at this time because solenoids 
         // active or because we're transmitting on the 2-Wire network
         if( bSolenoidsActive() || (sc_state >= SC_TX_MARKING_PRE_e ) )
         {
            // This sync request must be deferred. Set the bit for the region #
            deferred_sync_requests |= ( 1<< sync_req_msg );
         }
         else
         {
            // It's OK to sync right now.
            // Call function in BG_EEP_Sync.c to handle sync
            process_bg_sync( sync_req_msg );
         }
      }
      else
      {
         //////////////////////////////////////////////
         // No sync message in the queue at this time
         //////////////////////////////////////////////

         // See if sync is possible at this time..
         if( !bSolenoidsActive() && ( (sc_state < SC_TX_MARKING_PRE_e ) || (sc_state == SC_TX_MARKING_POST_e ) ) )
         {
            // Sync is possible now so check all four
            // regions and sync any that need it.
            for( cnt = 0; cnt <= STATS_REGION; cnt++ )
            {
               if( deferred_sync_requests & (1<<cnt) )
               {
                  // Sync region == cnt
                  process_bg_sync( cnt );

                  // Reset the deferred sync bit for this region
                  deferred_sync_requests &= ~(1<<cnt);
               }
            }
         }

         // Handle temperature statistics
         process_temp();
      }
   }
}


////////////////////////////////////////////////////////////
//
//    void main(void)
//
//    The decoder starts here after SystemInit() and the 
//    C startup code in thumb_crt0.s have executed.
////////////////////////////////////////////////////////////
void main(void)
{
   portBASE_TYPE status;   // FreeRTOS API function return status

   /////////////////////////////////////////////////////////////////////
   // The LPC11E14 is running at 12 MHz from the crystal at this point. 
   // See SystemInit() in system_LPC11Exx.c
   /////////////////////////////////////////////////////////////////////

   // Clear system flags initially
   sys_flags = 0;

   // Read system reset status and save it
   sysrststat = LPC_SYSCON->SYSRSTSTAT & 0x1f;

   // Then reset the SYSRSTSTAT status bits
   LPC_SYSCON->SYSRSTSTAT = sysrststat;

   /////////////////////////////////////////////////////////////////////
   // Initialize hardware that doesn't need values read from EEPROM.
   // hw_init() configures all micro pins for the decoder application.
   /////////////////////////////////////////////////////////////////////
   hw_init1();

   /////////////////////////////////////////////////////////////////////
   // NOTE: hw_init1() returns with the LPC11E14 in a power-optimized 
   // configuration for unused peripherals and pins.
   /////////////////////////////////////////////////////////////////////
    
   // This creates the mutex for EEPROM access. 
   // It does not return if an error occurs (but this should never happen)
   iap_init();    

   // Create the queue for the background sync requests
   if( ( xBG_Sync_Req_Queue = xQueueCreate( MAX_BG_SYNC_REQ_MSGS, 1 ) ) == 0 )
   {
      // Error creating queue
      Fatal_Error( SYNC_REQ_Q_CREATE_ERROR );
   }

   ///////////////////////////////////////////////////////////////////////
   // Create the system task - it first handles startup duties
   // and then it handles background sync-to-EEPROM processing.
   ///////////////////////////////////////////////////////////////////////
   status = xTaskCreate(   SystemTask,
                           (signed char *) "SysTask",
                           configMINIMAL_STACK_SIZE + 64,      // Allow extra 128 bytes for IAP
                           NULL,
                           configMAX_PRIORITIES - 2,  //  Priority
                           NULL);
   
   if( status != pdPASS )
   {
      // Error creating task
      Fatal_Error( SYSTEM_TASK_CREATE_ERROR );
   }

   // Start the FreeRTOS scheduler
   vTaskStartScheduler();

   //  Will only get here if there was insufficient memory to create the idle
   //  task.  The idle task is created within vTaskStartScheduler(). 
   Fatal_Error( SCHEDULER_START_MEM_ERROR );
}

//////////////////////////////////////////////////////////////////////////
//
//    void vApplicationStackOverflowHook( xTaskHandle pxTask, signed char *pcTaskName )
//
//    This function is called if FreeRTOS detects that a task has 
//    overflowed its allocated stack.
//////////////////////////////////////////////////////////////////////////
void vApplicationStackOverflowHook( xTaskHandle pxTask, signed char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time stack overflow checking is performed if configCHECK_FOR_STACK_OVERFLOW is defined 
   to 1 or 2.  This hook function is called if a stack overflow is detected. */
   Fatal_Error( APP_STACK_OVF_HOOK_ERROR );
}


void vApplicationMallocFailedHook( void )
{
   Fatal_Error( APP_MALLOC_FAILED_ERROR );
}
