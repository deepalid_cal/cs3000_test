//////////////////////////////////////////////////////////
//
//    File: sys_timers.h
//
//    Header file for CT16Bx timer functions
//
//////////////////////////////////////////////////////////

// Macros for timer/counter register access
#define MR_REG_OFFSET   0x00000004
#define MR0_BASE_ADDR   0x4000c018
#define MR1_BASE_ADDR   0x40010018
#define MR0_ADDR_PTR(n) ((U32 *)(MR0_BASE_ADDR+(n<<2)))
#define MR1_ADDR_PTR(n) ((U32 *)(MR1_BASE_ADDR+(n<<2)))

// CT16B0 Timer counter register
#define CT16B0_TC       (*((U32 *) 0x4000c008))

// CT16B0 Match registers
#define CT16B0_MR0      (*((U32 *) 0x4000c018))
#define CT16B0_MR1      (*((U32 *) 0x4000c01c))
#define CT16B0_MR2      (*((U32 *) 0x4000c020))
#define CT16B0_MR3      (*((U32 *) 0x4000c024))

// Some constants for the sw_delay function
#define SW_DELAY_CNT_FOR_40_US      20

// Function Prototypes
extern void init_us_timer(void);
extern void us_timer( U16 delay_us );
extern void init_pwm(void);

// For the software delay
extern void sw_delay(U32 value);
