@echo off

rem Create the directory to store the binary in. If it the directory already
rem exists, nothing happens.
mkdir "C:\CS3000\fw_upgrade\bin\moisture_decoder\"

rem Copy the binary from the Release folder to the newly created directory, over-
rem writing the file if it already exists.
rem NOTE: The %~dp0 indicates the folder in which this batch file resides.
copy %~dp0"\THUMB Flash Release\moisture_decoder.bin" "C:\CS3000\fw_upgrade\bin\moisture_decoder\moisture_decoder.bin" /y

rem Copy the solution file from the to the newly created directory, overwriting
rem the file if it already exists. The solution file is necessary when using the
rem CrossLoad command-line tool to load code into a Two-Wire decoder because it
rem specifies information about the hardware configuration.
copy %~dp0"\moisture_decoder.hzp" "C:\CS3000\fw_upgrade\bin\moisture_decoder\moisture_decoder.hzp" /y

rem Start the CS3000 Firmware Upgrade Utility. Note the use of the Start command.
rem This executes the application in a separate window, immediately returning to
rem the batch file, effectively returning control back to Rowley CrossStudio when
rem this batch file is executed as part of the compilation process.
rem NOTE: The command will not execute properly if the quotes ("") between start
rem and the application are removed.
start "" "C:\CS3000\fw_upgrade\bin\FW_Upgrade.exe"
