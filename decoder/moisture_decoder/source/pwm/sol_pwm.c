////////////////////////////////////////////////////////////////
//
//	File:	sol_pwm.c
//
//	Author: David K. Squires
//			Squires Engineering, Inc.
//			for Calsense, Carlsbad, CA
//
////////////////////////////////////////////////////////////////
#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "eeprom.h"
#include "gpio.h"
#include "adc.h"
#include "sys_timers.h"  
#include "Calsense_2-Wire_Decoder.h"
#include "ProtocolMsgs.h"
#include "sol_pwm.h"
#include "SerialComm.h"

#include "FreeRTOS.h"

// Forward references
static void Timer_Ovf( volatile DECODER_PARMS_s *pParms, U8 sol );
static void Timer_Compx( volatile DECODER_PARMS_s *pParms, U8 sol );

////////////////////////////
//	Globally visible data
////////////////////////////
volatile BOOL bSolEnaTmp[ NUM_SOLENOIDS ] = { 0, 0 };

/////////////////////////////////////////////////////////
//	These flags are written by task(s) and are read by
//	the ISR processing functions.
/////////////////////////////////////////////////////////
volatile BOOL bSolEna[ NUM_SOLENOIDS ] = { 0, 0 };

/////////////////////////////////////////////////////////
//	Current (state machine) states of the two solenoids
/////////////////////////////////////////////////////////
volatile SOL_PWM_STATES_e SolPwmState_e[ NUM_SOLENOIDS ] = { SOL_OFF_e, SOL_OFF_e };

/////////////////////////////////////////////////////////
//	Solenoid Cap Delta V in ADC counts
/////////////////////////////////////////////////////////
volatile U16 SolCapDeltaVAdcCnts[ NUM_SOLENOIDS ] = { 0, 0 };

//////////////////////////////////////////////
//	H-Bridge capacitor voltage in ADC counts
//////////////////////////////////////////////
volatile U16 H_BridgeAdcCnts = 0;

//////////////////////////////////////////////
//	Solenoid current measurement variables
//////////////////////////////////////////////
volatile BOOL bStartCurMeas[ NUM_SOLENOIDS ] = { FALSE, FALSE };

volatile CUR_MEAS_STATE_e CurMeasState_e[ NUM_SOLENOIDS ] = { CUR_MEAS_INACTIVE_e, CUR_MEAS_INACTIVE_e };

volatile U8 CurMeasSeqnum = 0;

// The dV and dt values are accumulated in these
volatile U16 StartingVSolCapAdc[ NUM_SOLENOIDS ];
volatile SOL_CUR_MEAS_s SolCurMeas[ NUM_SOLENOIDS ];

// Completed measurements are stored in these for reporting
volatile SOL_CUR_MEAS_s SolCurMeasSnapshot[ NUM_SOLENOIDS ];

/////////////////////////////////
//	Private (static) variables
/////////////////////////////////

/////////////////////////////////////////////
//	ADC readings of VSOLx - written in ISRs
/////////////////////////////////////////////
static volatile U16 Vsol[ NUM_SOLENOIDS ] = { 0, 0, };

/////////////////////////////////////////////
//	PWM Turn-on latency in TC counts
/////////////////////////////////////////////
static volatile U16 SolPwmOnLatencyCnts[ NUM_SOLENOIDS ] = { 0, 0 };

///////////////////////////////////////////////////
//	Pointer to PWM Duty Cycle Loopkup table to use
///////////////////////////////////////////////////
static volatile U8 *Sol1PwmDC;
static volatile U8 *Sol2PwmDC;

/////////////////////////////////////////////////
//	Lookup table of PWM-related I/O port masks
/////////////////////////////////////////////////
static U32 PWM_SOL_MASK[ NUM_SOLENOIDS ]    = { PWM_SOL1_MASK, 		PWM_SOL2_MASK 		};
static U32 CAPCON_SOL_MASK[ NUM_SOLENOIDS ] = { CAPCON_SOL1_MASK, 	CAPCON_SOL2_MASK	};

////////////////////////////////////////
// Temperature Measurement Variables
////////////////////////////////////////

// PWM Cycle counter for timing temp processing in PWM code
static U16 temp_pwm_cycles;

// The accumulator for averaging temp readings
static U32 sum_of_temp_readings;

///////////////////////////////////////////////////////////
//
//	Function: void enablePWM( BOOL enable )
//
//	This function safely disables or re-enables the 
// solenoid PWM system.
//
///////////////////////////////////////////////////////////
void enablePWM( BOOL enable )
{
   U32 IR_Value;

   portENTER_CRITICAL();

   if( enable == TRUE )
   {
      // Read pending interrupt sources in CT16B0
      IR_Value = LPC_CT16B0->IR;

      // Clear all match interrupts
      LPC_CT16B0->IR = IR_Value;

      // Re-enable the CT16B0 interrupt
      NVIC_EnableIRQ( TIMER_16_0_IRQn );
   }
   else
   {
      // Turn off both PWM outputs
      GPIO_CLRP1 = PWM_SOL1_MASK | PWM_SOL2_MASK;

      // Disable the CT16B0 interrupt
      NVIC_DisableIRQ( TIMER_16_0_IRQn );
   }

   portEXIT_CRITICAL();
}

//////////////////////////////////////////////////////////
//
//    void SetMR2(void)
//
//    This function sets CT16B0->MR2 for an interval that
//    is slightly longer than the maximum duty cycle 
//    configured for either solenoid channel.
//
//////////////////////////////////////////////////////////
static void SetMR2(void)
{
   U8 DC;
   U16 counts;

   // Find greater of the two configured max duty cycle values
   DC = Sol1Parms.SolMaxHoldDC > Sol2Parms.SolMaxHoldDC ? Sol1Parms.SolMaxHoldDC + 1 : Sol2Parms.SolMaxHoldDC + 1;

   // Here we calculate the counts to use for MR2
   counts = PWM_DUTY_CYCLE_CNTS( DC, PWM_DIV64_250_HZ_TOP );

   // Now we write MR2
   LPC_CT16B0->MR2 = counts;
}

/////////////////////////////////////////////////////////////////////////
//
//    Function: void initPWM(void)
//    
//    Purpose: Initializes timer CT16B0 in counter mode to generate PWM timing
//
//    The SOL0 on time is set by the value in MR0
// 	The SOL1 on time is set by the value in MR1
//    The PWM period is set by the value in MR3,
//
//    A common interrupt is generated when any of the match events
//    occur. The IRQ is TIMER_16_0_IRQn.
//
/////////////////////////////////////////////////////////////////////////
void initPWM(void)
{
   U16 cnts;

   // Disable the CT16B0 interrupt
   NVIC_DisableIRQ( TIMER_16_0_IRQn );

   ///////////////////////////////////////////////
   // Set prescale register to 95 (divide by 96)
   // to generate a 125 KHz clock to CT16B0.
   ///////////////////////////////////////////////
   LPC_CT16B0->PR = 95;

   // Set Count Control Register for timer mode
   LPC_CT16B0->CTCR = 0;   // Note: Same as reset value

   // Set PWM control register to Non-PWM mode - the PWM
   // outputs are controlled by code in the CT16B0 ISR (interrupt #16)
   LPC_CT16B0->PWMC = 0;   // Note: Same as reset value

   // Set the Timer/Counter 1 period to 4 ms (250 Hz PWM cycle rate)
	SET_SOL_PWM_PD(CommonParms.PwmTopCnts);	

	///////////////////////////////////////////////////////////
	//	Set the COMPx registers for 50% duty cycle initially.
	//	This puts the COMPx interrupts in the middle of the 
	//	PWM cycle.
	///////////////////////////////////////////////////////////
   cnts = PWM_DUTY_CYCLE_CNTS(CommonParms.SolStndbyDC, CommonParms.PwmTopCnts);

	SET_SOL1_PWM_CNTS(cnts);	// set sol 1 PWM to 50% D.C.
	SET_SOL2_PWM_CNTS(cnts);	// set sol 2 PWM to 50% D.C.

	//////////////////////////////////////////////////////////
	//	Initialize both PWM state machines, allowing
   // for possible failed solenoid(s).
	/////////////////////////////////////////////////////////
	SolPwmState_e[ SOL1 ] = Sol1Parms.bSolFailed ? SOL_EXCESSIVE_CURRENT_e : SOL_OFF_e;
	SolPwmState_e[ SOL2 ] = Sol2Parms.bSolFailed ? SOL_EXCESSIVE_CURRENT_e : SOL_OFF_e;

   //////////////////////////////////////////////////////////////////
   //
   // The MCR is set up such that:
   // 1. An interrupt is generated when TC == MR0 - like TIM1_COMPA
   //    This event means "Solenoid 1 ON time is complete."
   // 2. An interrupt is generated when TC == MR1 - like TIM1_COMPB
   //    This event means "Solenoid 2 ON time is complete."
   // 3. An interrupt is generated when TC == MR2 
   //    This event means both PWM ON times are complete and
   //    the non time-critical processing can be done.
   // 4. An interrupt is generated when TC == MR3 - like TIM1_OVF
   //    This event marks the end of each PWM period.
   // 5. The TC is reset when TC == MR3
   //
   // All events cause common interrupt #16 (CT16B0 interrupt)
   //////////////////////////////////////////////////////////////////
   //                      1.       2.       3.       4.        5.
   //LPC_CT16B0->MCR = ( (1<<0) | (1<<3) | (1<<6) | (1<<9) | (1<<10) );

   // For the POC Decoder we do not enable 3) the MR1 interrupt 
   //                    1.       2.       4.        5.
   LPC_CT16B0->MCR = ( (1<<0) | (1<<6) | (1<<9) | (1<<10) );

   // Write the reset value to the EMR
   LPC_CT16B0->EMR = 0;

   // Enable the Timer Counter and Prescale Counter
   LPC_CT16B0->TCR = 1;    // Set CEN (bit 0)

   // Set up MR2 with a suitable interval
   SetMR2();

   // Set SYSTICK interrupt priority to 3 (lowest)
   // This is configured in Calsense_2_Wire_Decoder.h   
   NVIC_SetPriority( SysTick_IRQn, SYSTICK_IRQ_NVIC_INT_PRIORITY );

   // Set CT16B0 interrupt priority to 1 (second highest) 
   // This is configured in Calsense_2_Wire_Decoder.h
   NVIC_SetPriority( TIMER_16_0_IRQn, PWM_CT16B0_NVIC_INT_PRIORITY );

   /* Enable the CT16B0 Interrupt */
   NVIC_EnableIRQ( TIMER_16_0_IRQn );

   // Clear temp reading accumulator
   sum_of_temp_readings = 0;
}


////////////////////////////////////////////////
//    ISR for CT16B0 (for PWM)
//
//    This interrupt occurs for:
//    TC == MR0 (Match 0) SOL1 output turn-off
//    TC == MR1 (Match 1) SOL2 output turn-off
//    TC == MR2 (Match 2) SOL1, SOL2 processing
//    TC == MR3 Start/End of PWM period
////////////////////////////////////////////////
void TIMER16_0_IRQHandler(void)
{
   U32 IR_Value;
  
   // Read pending interrupt sources in CT16B0
   IR_Value = LPC_CT16B0->IR;

   if( IR_Value & (1<<0) ) // Match channel 0?
   {
      // Turn off sol 1 PWM output
      GPIO_CLRP1 = PWM_SOL_MASK[ SOL1 ];
   }

   if( IR_Value & (1<<2) ) // Match channel 2?
   {
      // Call common PWM compare function for solenoid 1
      Timer_Compx( &Sol1Parms, SOL1 );
   }

   if( IR_Value & (1<<3) ) // Match channel 3 (End/Start of PWM cycle)?
   {
      // THIS IS THE TIMER_OVF EVENT (BEGIN/END of PWM Cycle)

      // Handle Solenoid 1
      Timer_Ovf( &Sol1Parms, SOL1 );
   }

   ////////////////////////////////////
   // Handle temperature measurements
   ////////////////////////////////////

   // Measure thermistor voltage, add counts to accumulator
   sum_of_temp_readings += (U32) adc_convert( ADMUX_CH_VOLT_THERM );

   // Check to see if it's time to calculate the average
   if( ++temp_pwm_cycles == PWM_PERIODS_PER_TEMP_AVGS )
   {
      // Yes, it's time to provide the temp reading average
      // Note: Use a divisor that's a power of 2 (it will be done by shifting.)
      TempAvgCnts = (U16) ( sum_of_temp_readings / PWM_PERIODS_PER_TEMP_AVGS );
      bTempAvgReady = TRUE;
      sum_of_temp_readings = 0;  // Clear accumulator
      temp_pwm_cycles = 0;       // Reset our PWM cycle counter
   }

   // Clear all match interrupts
   LPC_CT16B0->IR = IR_Value;
}

///////////////////////////////////////////////
//	Returns TRUE if OK to start or continue a 
//	current measurement, else returns FALSE
///////////////////////////////////////////////
BOOL bValidTimeForCurrentMeasurement( U8 sol )
{
	BOOL bOK;
	
	// See if we are transmitting or other channel's solenoid is being activated
	if( ( sc_state >= SC_TX_MARKING_PRE_e ) || (
		( SolPwmState_e[ sol ^ 1 ] > SOL_OFF_e ) &&
	   ( SolPwmState_e[ sol ^ 1 ] < SOL_HOLD_e ) ) )
	{
		bOK = FALSE;
	}
	else
	{
		bOK = TRUE;
	}

	return bOK;
}

////////////////////////////////////////////////////////////////////
//
//	BOOL bCurrentMeasurementIsComplete( U8 sol )
//
//	Purpose: checks to see if criteria are met for a 
//	complete current measurement. Criteria are:
//
//		a) Solenoid cap delta V >= SOL_CUR_MEAS_MIN_DV_ADC_CNTS (6.45V)
//			or
//		b) total cap discharge time >= SOL_CUR_MEAS_MAX_DUTY_CYCLE_SUM
//			This 80 ms. of solenoid ON time accumulated.
//
///////////////////////////////////////////////////////////////////
BOOL bCurrentMeasurementIsComplete( U8 sol )
{
	U16 dVAdcCnts;
	BOOL bDone;
	
	bDone = FALSE;
	
	// Check if solenoid cap discharging (what we want to see)
	// if( Vsol[ sol ] <= StartingVSolCapAdc[ sol ])
    if( CurMeasState_e[ sol ] == CUR_MEAS_IN_PROG_e )
	{
		////////////////////////////////////////////////////
		//	Cap is discharging (good.) Compute solenoid cap 
		//	voltage drop since start of measurement.
		////////////////////////////////////////////////////
		dVAdcCnts = StartingVSolCapAdc[ sol ] - Vsol[ sol ];
		
		if( ( dVAdcCnts >= CommonParms.SolCurMeasMinDvAdcCnts ) ||
			( SolCurMeas[ sol ].duty_cycle_acc >= CommonParms.SolCurMeasMaxDCSum) )
		{
			bDone = TRUE;
		}
	}
	return bDone;
}

///////////////////////////////////////////////
//
//	Performs and returns an ADC measurement -
//
//	Approximate ADC conversion times:
//
//    11 * ( 1 / 4 MHz) = about 3 microseconds
//
///////////////////////////////////////////////
#if SIM_ENABLE == 1
#warning ADC input simulation is enabled with SIM_ENABLE set!
U16 adc_convert(U8 channel)
{
	U16 val;

	switch( channel )
	{
		case ADMUX_CH_VOLT_SOL1:
		case ADMUX_CH_VOLT_SOL2:

			// Simulate 31.5 VDC for solenoid cap voltages
			val = 520;
			break;

		case ADMUX_CH_VOLT_HBRG:

			// Simulate 32.0 VDC for H-Bridge voltage
			val = 528;
			break;

      case ADMUX_CH_VOLT_THERM:

         // Simulate a thermistor at 20 deg. C
         // Must be <= 0x3ff (i.e. 10-bit ADC)
         val = 0x0123;
         break;

		default:
			val = 0;
			break;
	}

	return val;
}

#else

U16 adc_convert(U8 channel)
{
	U16 val;

	// Select the ADC input channel
   val = read_adc_ch( channel );

	return val;
}
#endif

////////////////////////////////////////////////////////////
//
//	This function returns the PWM counts to use for
//	the solenoid PWM solenoid hold mode based upon the
//	specified capacitor voltage (passed as ADC counts).
//
//	The PWM counts returned have been adjusted by the
//	PWM On time latency, which is measured within the
//	timer OVF ISR just before each channel's solenoid 
//	PWM pin is enabled (i.e. the start of that
//	solenoid channel's PWM On time).
//
//	It also returns the duty cycle via the pointer pDutyCycle
//
//	Algorithm: index = (adc_cnts / 64); 
//
//	Indices > 7 are forced to 7
//	Indices < 3 are forced to 0
//	otherwise index = index - 3;
//
////////////////////////////////////////////////////////////
U16 GetPwmHoldModeCnts( volatile DECODER_PARMS_s *pParms, U8 *pDutyCycle, U16 vcap)
{
	U16 DC;
	U16 latency_adj;

	/////////////////////////////////////////////////
	//	Convert the solenoid capacitor ADC value to
	//	the 3.75 V step number to use as index
	/////////////////////////////////////////////////
	if( vcap >= PWM_DUTY_CYCLE_ADC_OFFSET )
	{
		vcap = (vcap - PWM_DUTY_CYCLE_ADC_OFFSET ) / PWM_ADC_DUTY_CYCLE_STEP_SIZE;
	}
	else
	{
		// Voltage below PWM modulation limit, so use index = 0
		vcap = 0;
	}

	////////////////////////////////////////////////////
	//	Calculate lookup table index; do range checking
	////////////////////////////////////////////////////
	if( vcap > 7 ) 
	{
		// Limit index for high values
		vcap = 7;
	}

	// Get the PWM duty cycle from the selected duty cycle lookup table
	if( pParms == &Sol1Parms )
	{
		DC = *( Sol1PwmDC + vcap );
	}		
	else
	{
		DC = *( Sol2PwmDC + vcap );
	}

	///////////////////////////////////////////
	//  Apply limit parameters for duty cycle
	//
	//	THIS PROTECTS AGAINST BAD DUTY CYCLES
	//	IN THE DUTY CYCLE LOOKUP TABLES BUT
	//	PERHAPS COULD BE REMOVED?
	///////////////////////////////////////////
	if( DC > pParms->SolMaxHoldDC )
	{
		// Use the maximum allowed duty cycle
		DC = pParms->SolMaxHoldDC;
	}
	else if( DC < pParms->SolMinHoldDC )
	{
		// Use the minimum allowed duty cycle
		DC = pParms->SolMinHoldDC;
	}

	// Save the duty cycle
	*pDutyCycle = DC;
		
	///////////////////////////////////////////
	//	Apply PWM turn-on latency compensation
	///////////////////////////////////////////
	latency_adj = ( pParms == &Sol1Parms ) ? SolPwmOnLatencyCnts[ SOL1 ] : SolPwmOnLatencyCnts[ SOL2 ];

	return ( PWM_DUTY_CYCLE_CNTS(DC,CommonParms.PwmTopCnts)) + latency_adj;
}

/////////////////////////////////////////////////////////////////////////
//
//	This function returns a TRUE (GOOD) or FALSE (BAD) status 
//	based upon the specified PWM duty cycle and the capacitor voltages 
//	before and after (a single PWM cycle).
//
/////////////////////////////////////////////////////////////////////////
BOOL isSolenoidOk( volatile DECODER_PARMS_s *pParms, U8 pwm_mode, U16 delta_v)
{
	BOOL status;
	U8 sol;

	status = TRUE;

	// Set "sol" to the other solenoid channel (0/1)
	sol = pParms == &Sol1Parms ? 1 : 0 ;

	///////////////////////////////////////////////////
	//	Perform this check only if the other solenoid 
	//	channel is NOT being activated at this time
	///////////////////////////////////////////////////
	if( ( SolPwmState_e[ sol ] == SOL_OFF_e ) || 
		( SolPwmState_e[ sol ] > SOL_PULL_IN_e ) )
	{
		// check for possible negative delta V and treat as zero volt delta
		if( delta_v > 1023 )
		{
			delta_v = 0;
		}

		if( pwm_mode == SOL_PWM_HOLD_MODE )
		{
			/////////////////////
			//	HOLD MODE CHECK
			/////////////////////
			if( delta_v > pParms->HoldBadDeltaVcntsMax )
			{
				status = FALSE;
			}
		}
		else // assumes the solenoid mode is test mode
		{
			////////////////////
			//	TEST MODE CHECK
			////////////////////
		
			//	presently uses same criteria as hold mode
			if( delta_v > pParms->HoldBadDeltaVcntsMax )
			{
				status = FALSE;
			}
		}
	}

	return status;
}

/////////////////////////////////////////////////////
//	TIMER_OVF ISR STATE MACHINE FOR SOLENOIDS
//
// This is called at the start/end of each PWM cycle
/////////////////////////////////////////////////////
static void Timer_Ovf( volatile DECODER_PARMS_s *pParms, U8 sol )
{
static U8 PwmCyclesForSolCapChg[ NUM_SOLENOIDS ];	// PWM cycle counter to recharge sol cap
static U8 SolCapConPulseCnt[ NUM_SOLENOIDS ];      // Solenoid cap charge pulse counter
U16 vsol_cap;  // Used for sol cap pulse charging scheme

   ///////////////////////////////////////////
	//	Get/save current H-bridge cap voltage
	///////////////////////////////////////////
	H_BridgeAdcCnts = adc_convert( ADMUX_CH_VOLT_HBRG );

	switch( SolPwmState_e[ sol ] )
	{
		case SOL_OFF_e:

			/////////////////////////////////////////////
			//	check for turn on request for solenoid 
			/////////////////////////////////////////////
			if( bSolEna[ sol ] )
			{
            // Set channel's match register for 1 ms. pulse
            SET_SOL_PWM_CNTS(sol, SOL_CAP_CHG_PULSE_TMR_CNTS );

            // Set up MR2 again (the max duty cycle parm may have changed)
            SetMR2();

				// Set up pointer to PWM duty cycle lookup table
				if( sol == SOL1 )
				{
					if( Sol1Parms.ActiveSolPwmLookupTable < N_PWM_TABLES )
					{
						// Point to one of the standard PWM duty cycle tables
						Sol1PwmDC = &CommonParms.SolTblLookupDC[ Sol1Parms.ActiveSolPwmLookupTable ][0];
					}
					else
					{
						// Point to the user-programmable PWM duty cycle table
						Sol1PwmDC = &Sol1Parms.SolTblLookupDC[ Sol1Parms.ActiveSolPwmLookupTable ];
					}
				}
				else
				{
					if( Sol2Parms.ActiveSolPwmLookupTable < N_PWM_TABLES )
					{
						// Point to one of the standard PWM duty cycle tables
						Sol2PwmDC = &CommonParms.SolTblLookupDC[ Sol2Parms.ActiveSolPwmLookupTable ][0];
					}
					else
					{
						// Point to the user-programmable PWM duty cycle table
						Sol2PwmDC = &Sol2Parms.SolTblLookupDC[ Sol2Parms.ActiveSolPwmLookupTable ];
					}
				}

            // Clear solenoid cap connection pulse counter
            SolCapConPulseCnt[ sol ] = 0;

            // Update PWM state
            SolPwmState_e[ sol ] = SOL_CAP_CHG_P_e;
            
			}
			break;

      case SOL_CAP_CHG_P_e:
         
         // See if it's time to pulse the solenoid cap connection
         if( sol_sm_timer[ sol ] == 0 )
         {
            // Set "connect solenoid cap" to H-Bridge
            GPIO_SETP1 = CAPCON_SOL_MASK[ sol ];

            // Count the solenoid cap charge pulse
            SolCapConPulseCnt[ sol ]++;

            // Set timer for 100 ms.
            sol_sm_timer[ sol ] = SOL_CAP_CHG_PULSE_PERIOD_MS;
         }
         else
         {
            // See if the solenoid caps have charged enough in pulse mode

            // Check if solenoid caps' voltage is close to H-Bridge voltage
            vsol_cap = adc_convert( ADMUX_CH_VOLT_SOL1 + sol );

            if(  ( ( vsol_cap >= H_BridgeAdcCnts ) || ( ( H_BridgeAdcCnts - vsol_cap ) <= SOL_CAP_H_BRG_CHARGE_DELTA_ADC_CNTS ) ) 
               || ( SolCapConPulseCnt[ sol ] >= MAX_SOL_CAP_CON_PULSES ) )
            {
               // connect solenoid cap to H-Bridge
               GPIO_SETP1 = CAPCON_SOL_MASK[ sol ];

               // Set timer to allow solenoid cap to charge
               sol_sm_timer[ sol ] = CommonParms.SolCapChgTime_ms;

               // update state to charge continuously
               SolPwmState_e[ sol ] = SOL_CAP_CHG_C_e;
            }
         }
         break;

		case SOL_CAP_CHG_C_e:
			////////////////
			//	do nothing
			////////////////
         break;

		case SOL_TEST_e:

			// Take ADC reading VOLTAGE_SOLx, save it
			Vsol[ sol ] = adc_convert( ADMUX_CH_VOLT_SOL1 + sol );

         // Now turn on SOL PWM output for this channel
         GPIO_SETP1 = PWM_SOL_MASK[ sol ];
			break;

  		case SOL_PULL_IN_e:

			// Take ADC reading VOLTAGE_SOLx, save it
			Vsol[ sol ] = adc_convert( ADMUX_CH_VOLT_SOL1 + sol );

         // Turn on SOL PWM output for this channel
         GPIO_SETP1 = PWM_SOL_MASK[ sol ];
			break;

  		case SOL_HOLD_e:

         // LPC11E14 code to disconnect solenoid cap from H-Bridge
         GPIO_CLRP1 = CAPCON_SOL_MASK[ sol ];

			// Take ADC reading VOLTAGE_SOLx, save it
			Vsol[ sol ] = adc_convert( ADMUX_CH_VOLT_SOL1 + sol );
			
			// Measure PWM ON interval latency for solenoid
			SolPwmOnLatencyCnts[ sol ] = CT16B0_TC;

         // Turn on SOL PWM output for this channel
         GPIO_SETP1 = PWM_SOL_MASK[ sol ];

			///////////////////////////////////////////////////////////////////
			// CURRENT MEASUREMENT CODE STARTS HERE SO IT WON'T MESS UP TIMING
			///////////////////////////////////////////////////////////////////
			
			//////////////////////////////////////////////////////////
			//	See if a current measurement is in progress 
			//	( a non-zero solenoid cap starting voltage tells us)
			//////////////////////////////////////////////////////////
			if ( StartingVSolCapAdc[ sol ] )
			{
				//////////////////////////////////////////
				//	A current measurement is in progress
				//////////////////////////////////////////
				
				// Has the solenoid cap voltage dropped enough 
				//	or has enough time elapsed for a measurement?
				if( bCurrentMeasurementIsComplete( sol ) )
				{
					//////////////////////////////////////////
               // Yes -
					//	Compute delta V for this measurement, 
					//	store it, masking it to 8 bits.
					//////////////////////////////////////////
					SolCurMeas[ sol ].dv_adc_cnts = ( StartingVSolCapAdc[ sol ] - Vsol[ sol ] ) & 0x00ff;

					// The measurement is done, copy vars to snapshot struct
					SolCurMeasSnapshot[ sol ] = SolCurMeas[ sol ];

					// Indicate that the current measurement is ready
					CurMeasState_e[ sol ] = CUR_MEAS_READY_e;

					// show no measurement in progress now
					StartingVSolCapAdc[ sol ] = 0;

					// Reset the start flag too
					bStartCurMeas[ sol ] = FALSE;

					// Set PWM cycle counter for timing solenoid cap recharging
					PwmCyclesForSolCapChg[ sol ] = CommonParms.SolCapChgPwmCyc;
				}
					// See if it's OK to continue measuring
				else if( FALSE == bValidTimeForCurrentMeasurement( sol ) )	
				{
					// No, we must abort the measurement in progress:
					// Show no measurement in progress
					StartingVSolCapAdc[ sol ] = 0;
					SolCurMeas[ sol ].duty_cycle_acc = 0;

					// Change the current measurement state
					CurMeasState_e[ sol ] = CUR_MEAS_INACTIVE_e;
				}
			}
			else
			{
				////////////////////////////////////////////////////////////
				//	No current measurement is in progress.
				//	Check to see if it's OK to start a current measurement
				////////////////////////////////////////////////////////////
            if( PwmCyclesForSolCapChg[ sol ] != 0 )   // Counter is active
            {
               // Decrement it
               --PwmCyclesForSolCapChg[ sol ];
            }

				if ( bStartCurMeas[ sol ] && bValidTimeForCurrentMeasurement( sol ) && 
					( PwmCyclesForSolCapChg[ sol ] == 0 ) )
				{
					////////////////////////////////////////////////
					//	It's OK to start a new current measurement
					////////////////////////////////////////////////
					
					// Record starting solenoid cap voltage
					StartingVSolCapAdc[ sol ] = Vsol[ sol ];
					
					// Clear the duty cycle accumulator
					SolCurMeas[ sol ].duty_cycle_acc = 0;

					// Record the sequence number for this measurement
					SolCurMeas[ sol ].seqnum = CurMeasSeqnum;

					// Change the current measurement state
					CurMeasState_e[ sol ] = CUR_MEAS_IN_PROG_e;
				}
			}
			break;

  		case SOL_EXCESSIVE_CURRENT_e:
			break;

		default:
			break;
	}
}

//////////////////////////////////////////////////
//	Common solenoid function for TIMER_COMPx ISR
//
// THIS IS A MATCH 0 or MATCH 1 compare for the
// LPC11E14 CT16B0 Timer/Counter that occurs
// when the channel's PWM on time is complete.
//////////////////////////////////////////////////
static void Timer_Compx( volatile DECODER_PARMS_s *pParms, U8 sol )
{
	U16 delta_v;
	U8 duty_cycle;
	U16 v2;
	U16 pwm_cnts;
   U8 sync_region;
   BOOL bFault;

   /////////////////////////////////////////////
   // NOTE: The PWM outputs are turned off 
   // in the MR0 and MR1 ISRs
   /////////////////////////////////////////////

	/////////////////////////////////////////////
	//	Then process according to the PWM state
	/////////////////////////////////////////////
	switch( SolPwmState_e[ sol ] )
	{
		case SOL_OFF_e:

			// Force current measurement to zero
			SolCurMeasSnapshot[ sol ].duty_cycle_acc = SolCurMeasSnapshot[ sol ].dv_adc_cnts = 0;
			break;

		case SOL_CAP_CHG_P_e:
				
         // disconnect solenoid cap from HBRG
			CLR_CAPCON_SOL(sol);
         break;

		case SOL_CAP_CHG_C_e:

			// Check for charge time complete
			if( sol_sm_timer[ sol ] == 0 )
			{
				// disconnect solenoid cap from HBRG
				CLR_CAPCON_SOL(sol);

				// Take ADC reading VOLTAGE_SOLx, save it
				Vsol[ sol ] = adc_convert( ADMUX_CH_VOLT_SOL1 + sol );

				// Calc PWM counts for test duty cycle
				pwm_cnts = PWM_DUTY_CYCLE_CNTS(pParms->SolTestDC, CommonParms.PwmTopCnts);

				// Then set the PWM compare register
				SET_SOL_PWM_CNTS(sol,pwm_cnts);

				// Change state
				SolPwmState_e[ sol ] = SOL_TEST_e;
			}
			break;

		case SOL_TEST_e:

			// Measure VOLTAGE_SOLx after a single PWM cycle
			v2 = adc_convert( ADMUX_CH_VOLT_SOL1 + sol );

			delta_v = Vsol[ sol ] - v2;

			// Check if the solenoid is OK			
			if( isSolenoidOk( pParms, SOL_PWM_TEST_MODE, delta_v ) )
			{
				// Solenoid is OK, reconnect solenoid cap
				SET_CAPCON_SOL(sol);

            //////////////////////////////////////////////////////////
            // If this solenoid is currently flagged as faulted and
            // we've turned it on without a problem, we defer clearing
            // its fault status until it passes the solenoid check in
            // the HOLD state.
            //////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////
            // To achieve a 100% duty cycle, the match register is
            // set to a value that cannot occur since it is greater
            // than the PWM period count set in match register 3.
            //////////////////////////////////////////////////////////
            pwm_cnts = CommonParms.PwmTopCnts + 1;

				// Then set the PWM compare register
				SET_SOL_PWM_CNTS(sol,pwm_cnts);

				// Set the timer for the pull-in time
				sol_sm_timer[ sol ] = pParms->SolPullInTime_ms;

				// Change State to PULL_IN
				SolPwmState_e[ sol ] = SOL_PULL_IN_e;
			}
			else
			{
				/////////////////////
				//	Solenoid is BAD 
				/////////////////////
				
				// We leave the solenoid PWM output OFF and
				// the solenoid capacitor disconnected.
				SET_SOL_PWM_CNTS(sol,PWM_DUTY_CYCLE_CNTS(CommonParms.SolStndbyDC,
									 CommonParms.PwmTopCnts));

            // A solenoid failure has been detected
            bSolEna[ sol ] = FALSE;
			
			SolPwmState_e[ sol ] = SOL_EXCESSIVE_CURRENT_e;

            // If the solenoid wasn't already marked as FAULTED, do it now
            if( pParms->bSolFailed == FALSE )
            {
               pParms->bSolFailed = TRUE;

               // Send SYNC request for a solenoid parameter region so that
               // the solenoid fault state is updated in EEPROM
               sync_region = sol ? SOL2_PARM_REGION : SOL1_PARM_REGION;

               SendSyncRequest( sync_region );
            }
         }
			break;

  		case SOL_PULL_IN_e:
			
			// Has the solenoid pull-in timer expired?
			if( sol_sm_timer[ sol ] == 0 )
			{
				//////////////////////////////
				//	Yes, pull-in is complete
				//////////////////////////////

				// Calculate PWM compare counts for current voltage
				pwm_cnts = GetPwmHoldModeCnts(	pParms, &duty_cycle, Vsol[ sol ] );
					 
				// Write to PWM compare register
				SET_SOL_PWM_CNTS(sol,pwm_cnts);

				// Set state to HOLD mode
				SolPwmState_e[ sol ] = SOL_HOLD_e;

            // Initiate current measurement for this solenoid
            bStartCurMeas[ sol ] = TRUE;
			}
			break;			

  		case SOL_HOLD_e:
			
			// Measure VOLTAGE_SOLx after a single PWM cycle
			v2 = adc_convert( ADMUX_CH_VOLT_SOL1 + sol );

			delta_v = Vsol[ sol ] - v2;

			// Save delta V 
			SolCapDeltaVAdcCnts[ sol ] = delta_v;

			////////////////////////////
			// Check if solenoid is OK
			////////////////////////////
			if( isSolenoidOk( pParms, SOL_PWM_HOLD_MODE, delta_v ) )
			{
				////////////////////
				//	Solenoid is OK, 
				////////////////////

            bFault = FALSE;

				// Check for solenoid turn-off or low solenoid cap voltage
				if( ( bSolEna[ sol ] == 0 ) || (v2 < pParms->MinSolCapHoldAdcCnts ) )
				{
					// Leave compare register at standby duty cycle
					// Write to compare register
					SET_SOL_PWM_CNTS(sol,PWM_DUTY_CYCLE_CNTS(	CommonParms.SolStndbyDC,
																CommonParms.PwmTopCnts));

					//////////////////////////////////////////////
					//	Note we leave solenoid cap. disconnected
					//////////////////////////////////////////////

					// Change state back to OFF
					SolPwmState_e[ sol ] = SOL_OFF_e;

					// Do special processing if this is an uncommanded COS
					if( v2 < pParms->MinSolCapHoldAdcCnts )
					{
						// Reset the solenoid enable flag
						bSolEna[ sol ] = FALSE;

						// Set UCOS bit in system flags
						sys_flags |= SYS_FLAG_SOLENOID_UCOS;

						// Bump stats counter for this event
						if( sol == SOL1 )
						{
							Decoder_stats.sol_1_ucos++;
						}
						else
						{
							Decoder_stats.sol_2_ucos++;
						}
					}
				}
				else
				{
					///////////////////////////////////
					//	Keep solenoid on in hold mode
					///////////////////////////////////
				
					// reconnect solenoid cap if we're not doing a current measurement
					if( 0 == StartingVSolCapAdc[ sol ] )
					{
						SET_CAPCON_SOL(sol);
					}
					
					/////////////////////////////////////////////
					//	Calculate PWM counts for current voltage.
					//	This was the STARTING solenoid capacitor 
					//	voltage for the last PWM cycle.
					///////////////////////////////////////////// 
					pwm_cnts = GetPwmHoldModeCnts( pParms, &duty_cycle, Vsol[ sol ] );
										  
					// Write to compare register
					SET_SOL_PWM_CNTS(sol,pwm_cnts);

					// Update duty cycle accum if current measurement is active
					if( StartingVSolCapAdc[ sol ] )
					{
						SolCurMeas[ sol ].duty_cycle_acc += duty_cycle;
					}
				}
			}
			else
			{
				/////////////////////////
				// The Solenoid is BAD
				/////////////////////////

            bFault = TRUE; // Updated fault flag state for this solenoid channel

				// Leave compare register at standby duty cycle
				// Write to compare register
				SET_SOL_PWM_CNTS(sol,PWM_DUTY_CYCLE_CNTS(	CommonParms.SolStndbyDC,
															CommonParms.PwmTopCnts));
				
				// leave the solenoid cap disconnected

            // Clear the solenoid enable for the failed channel.
            // We must do this BEFORE setting the FAILED state
            bSolEna[ sol ] = FALSE;

            // Set its PWM state to FAILED
				SolPwmState_e[ sol ] = SOL_EXCESSIVE_CURRENT_e;
			}

         ///////////////////////////////////////////////////////////////
         // If the solenoid was previously faulted and is OK now, then 
         // clear the fault status and perform a sync.
         // If the solenoid is bad now and wasn't already,
         // flag the solenoid as faulted and perform a sync.
         //////////////////////////////////////////////////////////////
         if( bFault != pParms->bSolFailed )
         {
            // Update fault flag in RAM parms
            pParms->bSolFailed = bFault;

            // Send SYNC request for a solenoid parameter region so that
            // the solenoid fault state is updated in EEPROM
            sync_region = sol ? SOL2_PARM_REGION : SOL1_PARM_REGION;
            SendSyncRequest( sync_region );
         }
			break;

  		case SOL_EXCESSIVE_CURRENT_e:

			///////////////////////////////////////////////
         // If a solenoid ON command is issued
         // while a solenoid is in the failed state,
         // an attempt is made to operate the solenoid.
         ///////////////////////////////////////////////
         if( bSolEna[ sol ] == TRUE )
         {
            // Change Solenoid state to OFF
				SolPwmState_e[ sol ] = SOL_OFF_e;
			}
			break;

		default:
			break;
	}
}

