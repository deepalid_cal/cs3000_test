//////////////////////////////////////////////////////////////////////////
//
//	File:    Calsense_2-Wire_Decoder.h
//
// NOTE:    For LPC11E14 Microprocessor
//
//	Author:  David K. Squires
//          Squires Engineering, Inc.
//          for Calsense, Carlsbad, CA
//
//////////////////////////////////////////////////////////////////////////

// Macro to align a variable or structure on an 4-byte boundary
#define ALIGN4  __attribute__((aligned(4)))

//////////////////////////////////////////////////////////////////////////
// CRITICAL CONFIGURATION DEFINES - 
//////////////////////////////////////////////////////////////////////////

// Set to 1 to force the decoder SN to zero. This is done only for development
// testing and production in the event that a decoder's serial number is assigned 
// incorrectly and must be changed.
#define FORCE_SN_TO_ZERO                  0     // Must be 0 for production

// Set to 1 to fake ADC values and use UART break for generating ACK pulse. This is
// for direct UART connections (i.e. Calsense LPC11E14 Eval board used with EVK1100)
#define SIM_ENABLE                        0     // Must be 0 for production

// Set this define to 1 to use a software delay for the 40 us H-Bridge delay.
// Set this define to 0 to use hardware timer CT16B1 for the 40 us. H-Bridge delay
#define USE_SW_TIMER_FOR_H_BRIDGE_TIMING  0     // Can be 0 or 1 for production (TBD)

///////////////////////////
//  INTERRUPT PRIORITIES
///////////////////////////

// This is the NVIC interrupt priority for the TXD pin-change interrupt
// 0 = highest, 3 = lowest
// This interrupt must set to the highest priority, otherwise interrupt latency will
// cause bit timing errors in the decoder serial data transmissions.
#define TXD_PIN_CHANGE_NVIC_INT_PRIORITY  0

// This is the NVIC interrupt priority for the PWM interrupts
// 0 = highest, 3 = lowest
// This interrupt is used to drive the PWM state machine to control solenoid PWM.
// Its priority should be set lower than the TXD PIN CHANGE INTERRUPT but higher than
// less timing-critical interrupts (like the SYSTICK or Flow sensor pulse detection
// interrupts).
#define PWM_CT16B0_NVIC_INT_PRIORITY      1

// This is the NVIC interrupt priority for the SYSTICK interrupt
// 0 = highest, 3 = lowest
// This interrupt is generated by the CPU internal SYSTICK hardware and is used to generate 
// the 1 KHz FreeRTOS tick. It affects FreeRTOS timing functionality. Its priority should be set
// lower than the TXD PIN CHANGE and PWM interrupts to keep the 1 KHz system ticks from
// causing unnecessary latency in processing those other interrupts.
#define SYSTICK_IRQ_NVIC_INT_PRIORITY     3

// This is the NVIC interrupt priority for FS_PULSE- pin changes on P0.22
// 0 = highest, 3 = lowest
// This interrupt priority can be set at or lower than that of the SYSTICK. 
#define FS_PULSE_PIN_CHANGE_NVIC_INT_PRIORITY   3

//////////////////////////////////////////////////////////
//  HARD-CODED VALUES FOR SOLENOID CAPACITOR CHARGING
//////////////////////////////////////////////////////////

// # PWM Timer counts for solenoid cap charge pulse (125 counts = 1 ms.)
#define SOL_CAP_CHG_PULSE_TMR_CNTS           125

// PWM charge pulse period in ms.
#define SOL_CAP_CHG_PULSE_PERIOD_MS          100

// The maximum number of solenoid cap charge pulses allowed
#define MAX_SOL_CAP_CON_PULSES               22

// Solenoid Cap Voltage delta from H-Bridge voltage -
// When this is reached the solenoid cap pulse mode charging is 
// terminated.
// It's expressed as ADC counts BELOW the H-Bridge voltage.
// Note: Each ADC count is 60.56 mV.
#define SOL_CAP_H_BRG_CHARGE_DELTA_ADC_CNTS  17       // This is about 1.0 Volts

////////////////////////////////////
//	Duration of the ACK positive 
//	voltage pulse, in milliseconds
////////////////////////////////////
#define ACK_PULSE_MS                         10

///////////////////////////////////////////
//	Marking line  duration before and 
//	after data sent by USART
///////////////////////////////////////////
#define TX_MARKING_LINE_DURATION_MS             10

///////////////// END OF CRITICAL CONFIGURATION DEFINES //////////////////

// Boolean states
#define FALSE	0
#define TRUE	(!FALSE)

#ifndef NDEBUG
	#include <cross_studio_io.h>
#endif


/////////////////////////////////////////
//	THIS DECODER'S TYPE CODE
//
// Type codes are #defined in header
// file: ProtocolMsgs.h
////////////////////////////////////////

#define DECODER_TYPE_CODE			(DECODER__TPMICRO_TYPE__MOISTURE)

////////////////////////////////////////
//	FIRMWARE VERSION 
//
//	Hex format: 0xMMmm
// MM = major version number
// mm = minor version number
//
//	Use hex digits as BCD numbers.
//
//	Example: V01.00 is encoded as 0x0100
/////////////////////////////////////////
#define DECODER_FW_VERSION			(0x0100)

//////////////////////////////////////////////////////////////
// Configure the number of stations (solenoid outputs) here.
// Valid values are 1 and 2
//////////////////////////////////////////////////////////////
#define NUM_SOLENOIDS			2

// Values to use as array indices for solenoid variables and structures
#define SOL1						0
#define SOL2						1

// Define ADCMUX selection values for ADC channels
#define ADMUX_CH_VOLT_SOL1		0		// P0.11 AD0 for reading solenoid 1 voltage
#define ADMUX_CH_VOLT_SOL2		1		// P0.12 AD1 for reading solenoid 2 voltage
#define ADMUX_CH_VOLT_HBRG		2		// P0.13 AD2 for reading HBRG voltage
#define ADMUX_CH_VOLT_THERM   3     // P0.14 AD3 for reading thermistor voltage

//////////////////////////////////////////////////
// 32-bit bitmask values for Port 0 pin functions
//////////////////////////////////////////////////
#define HBRG_RT					(1<<3)      // P0.3  GPIO output
#define HBRG_RB					(1<<6)      // P0.6  GPIO output
#define HBRG_LT					(1<<7)      // P0.7  GPIO output
#define HBRG_LB					(1<<8)      // P0.8  GPIO output
#define VSOL1                 (1<<11)     // P0.11 AD0 input
#define VSOL2                 (1<<12)     // P0.12 AD1 input
#define VHBRG                 (1<<13)     // P0.13 AD2 input
#define VTHERM                (1<<14)     // P0.14 AD3 input

//////////////////////////////////////////////////
// 32-bit bitmask values for unused port 0 pins
// These are used in h-bridge_tx.h
//////////////////////////////////////////////////
#define P0_1                  (1<<1)      // P0.1
#define P0_2                  (1<<2)      // P0.2
#define P0_4                  (1<<4)      // P0.4
#define P0_5                  (1<<5)      // P0.5
#define P0_9                  (1<<9)      // P0.9
#define P0_16                 (1<<16)     // P0.16
#define P0_17                 (1<<17)     // P0.17
#define P0_18                 (1<<18)     // P0.18
#define P0_19                 (1<<19)     // P0.19
#define P0_20                 (1<<20)     // P0.20
#define P0_21                 (1<<21)     // P0.21
#define P0_22                 (1<<22)     // P0.22
#define P0_23                 (1<<23)     // P0.23

//////////////////////////////////////////////////
// 32-bit bitmask values for port 1 pin functions
//////////////////////////////////////////////////
#define PWM_SOL1_MASK			(1<<13)     // P1.13 CT16B0_MAT0 PWM output for solenoid 1 (GPIO)
#define PWM_SOL2_MASK			(1<<14)     // P1.14 CT16B0_MAT1 PWM output for solenoid 2 (GPIO)
#define CAPCON_SOL1_MASK		(1<<23)     // P1.23 connects solenoid 1 to its capacitor
#define CAPCON_SOL2_MASK		(1<<24)     // P1.24 connects solenoid 2 to its capacitor
#define USART_RXD             (1<<26)     // P1.26 UART RXD
#define USART_TXD             (1<<27)     // P1.27 UART TXD

/////////////////////////////////////
// System Control Block Registers
/////////////////////////////////////
#define SCB_AIRCR_REG_PTR     ((U32 *)0xe000ed0c)  // Application Interrupt and Reset Control Register address ptr.

// This is the VECTKEY value that must be written (for bits 31:16)
#define AIRCR_VECTKEY         (0x05fa<<16)      // Writes to AIRCR must have this value. otherwise they're ignored
#define AIRCR_SYSRESET_REQ    (1<<2)            // Setting this bit requests a system level reset

// SYSRSTSTAT bit assignments
#define SYSRSTSTAT_POR        (1<<0)            // Power on reset
#define SYSRSTSTAT_EXTRST     (1<<1)            // External reset
#define SYSRSTSTAT_WDT        (1<<2)            // WDT reset
#define SYSRSTSTAT_BOD        (1<<3)            // BOD reset
#define SYSRSTSTAT_SYSRST     (1<<4)            // System (commanded) reset

///////////////////////////////
// Defines used for operations
///////////////////////////////

// The CPU crystal frequency
#define CPU_XTAL_CLK_HZ             12000000    // 12 MHz

// USART-related #defines
#define USART_BAUDRATE              1200			// The serial port will be 1200 baud, 8-N-1

// Macros for getting high/low byte of a 16-bit value
#define HIBYTE(u16)                 (u16>>8)
#define LOBYTE(u16)                 (u16&0xff)

// Useful macro for FreeRTOS functions
#define MS_to_TICKS( ms )           ((ms)/portTICK_RATE_MS) 

//////////////////////////////////////////////////////////////////////////
//
//    SYNC_STATS_INTERVAL_MINUTES
//
//    This configures the time (in minutes) for the statistics sync
//    period. 
//
//    All of the other SRAM regions are synchronized to EEPROM on demand 
//    by calling SendSyncRequest() with a region number to be 
//    synchronized. The region numbers are #defined in ProtocolMsgs.h
//
//    A 64 minute sync period allows 12 years before reaching the minimum
//    EEPROM erase/write cycle life.
//
//    A 79 minute sync period allows 15 years before reaching the minimum
//    EEPROM erase/write cycle life.
//////////////////////////////////////////////////////////////////////////

// 5/20/2014 rmd : I actually don't consider writing the STATS to the EE a terribly
// important requirement. At least when saying the decoder will only last 12 years because
// we wore out the flash. The decoders may last that long in the ground and I sure don't
// want saving the STATS to be the limiting factor. By setting this to once every two hours
// we push the EE lifetime to 20+ years.
#define SYNC_STATS_INTERVAL_MINUTES			(60 * 2)

// Constants derived from the above define
#define SYNC_STATS_INTERVAL_SECS			(SYNC_STATS_INTERVAL_MINUTES * 60)

#define SYNC_STATS_INTERVAL_1_KHZ_TICKS		(SYNC_STATS_INTERVAL_SECS * 1000)

// ----------

// This is the max number of request messages the sync request queue can hold
#define MAX_BG_SYNC_REQ_MSGS		(8)

//////////////////////////////////////////////////////////////////////////
//    Temperature Measurement Rate Configuration
//
//    PWM_PERIODS_BETWEEN_TEMP_AVGS:
//
//    The number of 4 ms. PWM periods per thermistor reading average.
//
//    The thermistor is sampled by the ADC every PWM period and these
//    readings are accumulated. When the configured number of PWM 
//    cycles have elapsed the sum of the ADC readings is divided by the
//    number of readings to provide a smoothed average value for
//    temperature.
//
//    NOTE: PWM_PERIODS_BETWEEN_TEMP_AVGS should be set to a power of 2
//    to avoid a performance-costly integer division to calculate the
//    average.
//////////////////////////////////////////////////////////////////////////
//
//    A value of 1024 provides a new averaged temp reading about every 4.1 seconds
#define PWM_PERIODS_PER_TEMP_AVGS   1024
#define TEMP_MIN_READING_CNTS       0x3ff

//////////////////////////////////////////////////////////////////////////
//    Pin Interrupt number for P1.20 from user manual Table 32 for 
//    pin number-to-PININT number mapping:
//
//    P0.0 to P0.23 : INTPIN  0 to INTPIN 23
//    P1.0 to P1.31 : INTPIN 24 to INTPIN 55
//////////////////////////////////////////////////////////////////////////

// This is the USART TXD pin (generates pin change interrupts)
#define INTPIN_P1_27                51       

// This is the pin connected to FS_PULSE- signal (generates pin change interrupts)
#define INTPIN_P0_17                17       

//////////////////////////////////////////////////////////////////////////
//	FATAL ERROR CODE parameters for Fatal_Error() function
//////////////////////////////////////////////////////////////////////////
#define SER_COMM_TASK_CREATE_ERROR     1
#define DEBUG_TASK_CREATE_ERROR        2
#define SYNC_REQ_Q_CREATE_ERROR        3
#define SYSTEM_TASK_CREATE_ERROR       4
#define SCHEDULER_START_MEM_ERROR      5
#define IAP_MUTEX_CREATE_ERROR         6
#define APP_STACK_OVF_HOOK_ERROR       7
#define APP_MALLOC_FAILED_ERROR        8
#define SEND_SYNC_Q_SEND_ERROR         9
#define SC_RING_BUFFER_ERROR           10
#define SET_POWER_ERROR                11
#define FLOW_SENSOR_TASK_CREATE_ERROR  12
#define MOISTURE_SENSOR_TASK_CREATE_ERROR    13
#define MOISTURE_SENSOR_MUTEX_CREATE_ERROR   14

typedef struct
{
	U16 decoder_type;	 // Decoder type code
	U16 fw_vers;		 // Firmware version
} DECODER_CORE_DATA_s;

///////////////////////////////
// Global Function Prototypes
///////////////////////////////
extern void Fatal_Error(U32 error);

/////////////////////
// GLOBAL VARIABLES
/////////////////////
extern SER_NUM_u Serial_Number;
extern const DECODER_CORE_DATA_s    CoreData;			 // Not field modifiable
extern volatile DECODER_STATS_s Decoder_stats;			// Modified in various places
extern volatile BOOL bDecoderResetPending;				// Set in SerialMsgProc.c

///////////////////////
//	ACTIVE PARAMETERS 
///////////////////////
extern volatile DECODER_COMMON_PARMS_s  CommonParms;	// Field modifiable
extern volatile DECODER_PARMS_s         Sol1Parms;		// Field modifiable
extern volatile DECODER_PARMS_s         Sol2Parms;		// Field modifiable

////////////////////////////////////////////////
//	System Flags - reported in status response
////////////////////////////////////////////////
extern volatile U8 sys_flags;

///////////////////////////////////////////////////////////////////////
//	1 KHz timers for solenoid state machines and the task timer.
//	These are declared "volatile" since they're decremented in an ISR.
///////////////////////////////////////////////////////////////////////
extern volatile U8 sol_sm_timer[ NUM_SOLENOIDS ];
extern volatile U8 task_timer;
extern volatile U8 bg_proc_timer;

/////////////////////////////////
// Temperature Variables
/////////////////////////////////
extern volatile BOOL bTempAvgReady;	   // Needed for pwm.c
extern volatile U16 TempAvgCnts;	   // Needed for pwm.c

// Function Declarations (prototypes)
extern BOOL bSolenoidsActive(void);
extern void ClearStatistics(void);
extern U16 crc16_sram_region( U8 *pRegion, U32 size );
extern void SendSyncRequest( U8 region_number );
