//////////////////////////////////////////////////////////
//
//    File: sys_timers.h
//
//    Header file for CT16Bx timer functions
//
//////////////////////////////////////////////////////////

// Macros for timer/counter register access
#define MR_REG_OFFSET   0x00000004
#define MR0_BASE_ADDR   0x4000c018
#define MR1_BASE_ADDR   0x40010018
#define MR0_ADDR_PTR(n) ((U32 *)(MR0_BASE_ADDR+(n<<2)))
#define MR1_ADDR_PTR(n) ((U32 *)(MR1_BASE_ADDR+(n<<2)))

// CT16B0 Timer counter register
#define CT16B0_TC       (*((U32 *) 0x4000c008))

// CT16B0 Match registers
#define CT16B0_MR0      (*((U32 *) 0x4000c018))
#define CT16B0_MR1      (*((U32 *) 0x4000c01c))
#define CT16B0_MR2      (*((U32 *) 0x4000c020))
#define CT16B0_MR3      (*((U32 *) 0x4000c024))

// Some constants for the sw_delay function
#define SW_DELAY_CNT_FOR_40_US      20

//////////////////////////////
// 32-bit CT32B0 defines
//////////////////////////////

// Register defines for CT32B0 TCR (Timer Control Register, addr: 0x40014004)
#define CT32B0_TCR_CEN           1        // Timer/Prescale counter enable (1 = enable)
#define CT32B0_TCR_CRST          (1<<1)   // Reset timer/prescaler: 1 = reset, 0 = count

// Register bit defines for CT32B0 CCR (addr: 0x40014028)
#define CT32B0_CCR_CAP0RE        0     // Load CR0 with TC on CAP0 rising edge
#define CT32B0_CCR_CAP0FE        1     // Load CR0 with TC on CAP0 falling edge
#define CT32B0_CCR_CAP0I         2     // Interrupt on CT32B0_CAP0 event
#define CT32B0_CCR_CAP1RE        6     // Load CR0 with TC on CAP1 rising edge
#define CT32B0_CCR_CAP1FE        7     // Load CR0 with TC on CAP1 falling edge
#define CT32B0_CCR_CAP1I         8     // Interrupt on CT32B0_CAP1 event

////////////////////////////////////////////////////////////////////
// Register field value defines for CT32B0 CTCR (addr: 0x40014070)
////////////////////////////////////////////////////////////////////

// CTM Field bits 1:0
#define CT32B0_CTCR_CTM_TMR_MODE 0  // Timer mode: count every PCLK edge
#define CT32B0_CTCR_CTM_CTR_RE   1  // Ctr mode: TC incremented on Rising Edge of selected CAP input
#define CT32B0_CTCR_CTM_CTR_FE   2  // Ctr mode: TC incremented on Falling Edge of selected CAP input
#define CT32B0_CTCR_CTM_CTR_BE   3  // Ctr mode: TC incremented on both edges of selected CAP input

// CIS Field bits 3:2
#define CT32B0_CTCR_CIS_CAP0     (0<<2)   // Count using CAP0 as clock
#define CT32B0_CTCR_CIS_RES      (1<<2)   // Reserved value - do not use
#define CT32B0_CTCR_CIS_CAP1     (2<<2)   // Count using CAP1 as clock

// ENCC Field bit 4
#define CT32B0_ENCC              (1<<4)   // Enables clearing of timer and prescaler upon selected capture event

// SEICC Field bits 7:5
#define CT32B0_SEICC_CAP0_RE     (0<<5)   // Rising edge of CAP0 clears timer
#define CT32B0_SEICC_CAP0_FE     (1<<5)   // Falling edge of CAP0 clears timer
#define CT32B0_SEICC_CAP1_RE     (4<<5)   // Rising edge of CAP1 clears timer
#define CT32B0_SEICC_CAP1_FE     (5<<5)   // Falling edge of CAP1 clears timer

// Function Prototypes
extern void init_us_timer(void);
extern void us_timer( U16 delay_us );
extern void init_pwm(void);

extern void init_flow_sensor_pulse_counter(void);

// For the software delay
extern void sw_delay(U32 value);
