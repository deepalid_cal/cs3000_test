//////////////////////////////////////////////////////////////////////////
//
//	File:    sspx.c
//
//          Support for LPC11E14 SSPx port in SPI mode
//
//	Author: David K. Squires
//          Fredonia Mountain Technologies, LLC
//          for Calsense, Carlsbad, CA
//
// Synopsis:   
//
// Note: Configure the SSP channel # (0 or 1) in sspx.h
//
//////////////////////////////////////////////////////////////////////////
#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "gpio.h"
#include "sspx.h"

// Pointer to base of SSPx registers
LPC_SSPx_Type *SSPxRegBasePtr;

////////////////////////////////////////////////
//
//    void SSPx_Select_Slave(BOOL state)
//
//    BOOL state;
//
//    Operation:
//    SSEL- driven active low if state is TRUE
//    SSEL- driven inactive high if state is FALSE
////////////////////////////////////////////////
void SSPx_Select_Slave(BOOL state)
{
   U32 value;

   value = state == TRUE ? 0 : 1;
   SetGpioPinValue( SSEL_GPIO_PORT, SSEL_GPIO_BIT, value );
}

////////////////////////////////////////////////////
//
//    void sspx_purge_rx_fifo( void )
//
//    This function purges all data from the sspx
//    RX FIFO
///////////////////////////////////////////////////
void sspx_purge_rx_fifo( void )
{
   U8 blackhole;

   while( ( SSPxRegBasePtr->SR & SR_RNE ) == SR_RNE )
   {
      // Read data register (ignore)
      blackhole = SSPxRegBasePtr->DR;
   }
}

////////////////////////////////////////////////
//
//    void init_sspx( U32 clk_hz )
//
//    Purpose: Initializes the SPI port at
//             the specified clock rate.
//
//    clk_hz: SPI clock speed in Hz
//
///////////////////////////////////////////////
void init_sspx( U32 clk_hz )
{
   U32 mask32;
   U32 SSPx_Clk_Div;

   // deassert peripheral reset for the chosen SSP channel
#if SSP_CH == 0
   mask32 = 1<<0;    // bit 0
#else
   mask32 = 1<<2;    // bit 2
#endif
   LPC_SYSCON->PRESETCTRL |= mask32;

   // Enable clock for SSPx
#if SSP_CH == 0
   mask32 = 1<<11;   // bit 11
#else
   mask32 = 1<<18;   // bit 18
#endif
   LPC_SYSCON->SYSAHBCLKCTRL |= mask32;

   // Set up the SSPx clock divider
   SSPx_Clk_Div = CPU_XTAL_CLK_HZ / clk_hz;

#if SSP_CH == 0
   LPC_SYSCON->SSP0CLKDIV = SSPx_Clk_Div;
#else
   LPC_SYSCON->SSP1CLKDIV = SSPx_Clk_Div;
#endif

   // Set up pointer to SSPx register base address
#if SSP_CH == 0
   SSPxRegBasePtr = LPC_SSP0; 
#else
   SSPxRegBasePtr = LPC_SSP1;
#endif

   // Set up SSP Clock Prescale Register
   SSPxRegBasePtr->CPSR = 2;

   // Set up control Register 0
   SSPxRegBasePtr->CR0 = CR0_DSS | CR0_DSS | CR0_FRM_FMT | 
                        CR0_CPOL | CR0_CPHA | CR0_SCR;

   // Set up control register 1
   SSPxRegBasePtr->CR1 = CR1_LBM | CR1_SSE | CR1_MS | CR1_SOD;

   // Clear out Rx FIFO
   sspx_purge_rx_fifo();
}


///////////////////////////////////////////////////
//
//    void sspx_tx( U8 *pData, U8 bytes )
//
//    U8 *pData - pointer to data buffer
//    U8 bytes  - # of bytes to transmit
//
//    Synopsis: Transmits a data buffer to the 
//             SPI slave
///////////////////////////////////////////////////
void sspx_tx( U8 *pData, U8 bytes )
{
   if( bytes )
   {
      // Wait for SPI controller to not be busy
      while( (SSPxRegBasePtr->SR & SR_BSY ) == SR_BSY );

      // Assert SSEL- signal
      SSPx_Select_Slave( TRUE );

      // LOAD DATA TO BE SENT TO SLAVE
      while( bytes )
      {
         // Check if TX FIFO is not full
         if(( SSPxRegBasePtr->SR & SR_TNF ) == SR_TNF )
         {
            // Tx FIFO not full: Load a data byte into the TX FIFO
            SSPxRegBasePtr->DR = *pData++;
            bytes--;
         }
      }

      // All data loaded. Now wait for SPI controller to go not busy
      while( (SSPxRegBasePtr->SR & SR_BSY ) == SR_BSY );

      // Deassert SSEL- signal
      SSPx_Select_Slave( FALSE );
   }
}

///////////////////////////////////////////////////
//
//    void sspx_tx( SSPX_IO_s *pIO_struct )
//
//    Synopsis: Transmits/Receives data buffers 
//    to/from the SPI slave
///////////////////////////////////////////////////
void sspx_io( SSPX_IO_CTL_s *pControl_Block )
{
   U8 dummy_write_cnt;

   // Wait for SPI controller to not be busy
   while( (SSPxRegBasePtr->SR & SR_BSY ) == SR_BSY );

   // Assert UART SSEL- signal
   SSPx_Select_Slave( TRUE );

   // See if anything to transmit
   if( pControl_Block->TxCnt )
   {
      // LOAD DATA TO BE SENT TO SLAVE
      while( pControl_Block->TxCnt )
      {
         // Check if TX FIFO is not full
         if(( SSPxRegBasePtr->SR & SR_TNF ) == SR_TNF )
         {
            // Tx FIFO not full: Load a tx data byte into the TX FIFO
            SSPxRegBasePtr->DR = *pControl_Block->TxBfr++;
            pControl_Block->TxCnt--;
         }
      }
   }

   // See if any data to be received
   if( pControl_Block->RxCnt )
   {
      // Get expected # receive data bytes
      dummy_write_cnt = pControl_Block->RxCnt;

      // Clear out SSPx receive FIFO
      sspx_purge_rx_fifo();

      // Write dummy bytes out to clock data into Rx FIFO
      while( dummy_write_cnt )
      {
         // Check if TX FIFO is not full
         if(( SSPxRegBasePtr->SR & SR_TNF ) == SR_TNF )
         {
            // Tx FIFO not full: Load a tx data byte into the TX FIFO
            SSPxRegBasePtr->DR = DUMMY_WRITE_DATA;
            dummy_write_cnt--;
         }
      }

      // Read data from Rx FIFO into Rx Buffer
      while( pControl_Block->RxCnt )
      {
         // Check if Rx FIFO is not empty
         if(( SSPxRegBasePtr->SR & SR_RNE ) == SR_RNE )
         {
            // Pull a data byte from the RX FIFO
            *pControl_Block->RxBfr++ =  SSPxRegBasePtr->DR;
            pControl_Block->RxCnt--;
         }
      }

   }
      
   // All I/O completed. Now wait for SPI controller to go not busy
   while( (SSPxRegBasePtr->SR & SR_BSY ) == SR_BSY );

   // Deassert SSEL- signal
   SSPx_Select_Slave( FALSE );
 
}
