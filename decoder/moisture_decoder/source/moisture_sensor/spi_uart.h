///////////////////////////////////////////////////////////
//
//    File: spi_uart.h
//
//    Header file for Exar SPI Uart I/O Functions
//
//    Fredonia Mountain Technologies, LLC
//
///////////////////////////////////////////////////////////

// Configure GPIO port/bit that monitors UART IRQ- output
#define SPI_UART_IRQ_PORT     0
#define SPI_UART_IRQ_BIT      21

// Configure GPIO port/bit for driving the SPI UART RESET-
// input. The SPI UART reset input is active low.
#define SPI_UART_RESET_PORT   0
#define SPI_UART_RESET_BIT    23

// Configure special character to detect
#define SPI_UART_SPEC_CHAR   0x0a   // Linefeed indicates end of ASCII data string

// SPI UART Registers
#define SPI_UART_RHR    0x00        // R/O LCR[7] = 0
#define SPI_UART_THR    0x00        // W/O LCR[7] = 0
#define SPI_UART_DLL    0x00        // R/W LCR[7] = 1, LCR .NE. 0xBF
#define SPI_UART_DLM    0x01        // R/W LCR[7] = 1, LCR .NE. 0xBF
#define SPI_UART_DLD    0x02        // R/W LCR[7] = 1, LCR .NE. 0xBF, EFR[4] = 1
#define SPI_UART_IER    0x01        // R/W LCR[7] = 0
#define SPI_UART_ISR    0x02        // R/O LCR[7] = 0
#define SPI_UART_FCR    0x02        // W/O LCR[7] = 0 
#define SPI_UART_LCR    0x03        // R/W
#define SPI_UART_MCR    0x04        // R/W LCR .NE. 0xBF
#define SPI_UART_LSR    0x05        // R/O LCR .NE. 0xBF
#define SPI_UART_MSR    0x06        // R/O
#define SPI_UART_SPR    0x07        // R/W
#define SPI_UART_TCR    0x06        // R/W
#define SPI_UART_TLR    0x07        // R/W
#define SPI_UART_TXLVL  0x08        // R/O
#define SPI_UART_RXLVL  0x09        // R/O
#define SPI_UART_EFCR   0x0f        // R/W LCR[7] = 0

#define SPI_UART_EFR    0x02        // R/W LCR = 0xBF
#define SPI_UART_XON1   0x04        // R/W LCR = 0xBF
#define SPI_UART_XON2   0x05        // R/W LCR = 0xBF
#define SPI_UART_XOFF1  0x06        // R/W LCR = 0xBF
#define SPI_UART_XOFF2  0x07        // R/W LCR = 0xBF

// Function Prototypes
extern void SpiUartReset( BOOL state );
extern void init_SpiUart(void);
extern void SpiFlushRxData( void );
extern U8 SpiUartIo( BOOL bRead, U8 Reg, U8 *pBuffer, U8 bytes );
extern void SpiWrReg( U8 reg, U8 data );
extern U8 SpiRdReg( U8 reg );