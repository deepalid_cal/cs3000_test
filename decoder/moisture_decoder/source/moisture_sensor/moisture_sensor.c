//////////////////////////////////////////////////////////////////////////
//
//	File:   moisture_sensor.c
//
//          Calsense Moisture Sensor support
//
// NOTE:    For LPC11E14 Microprocessor
//
//	Author:  David K. Squires
//          Fredonia Mountain Technologies, LLC
//          for Calsense, Carlsbad, CA
//
// Synopsis:   
//
//////////////////////////////////////////////////////////////////////////
#include "LPC11Exx.h"
#include "LPC1000.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "ProtocolMsgs.h"

#include "sspx.h"
#include "spi_uart.h"
#include "gpio.h"

// FreeRTOS includes
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "moisture_sensor.h"
#include "string.h"

//////////////////////////////////////////////////////////////////////////////////////
//
//  DATA STRUCTURES
//
//////////////////////////////////////////////////////////////////////////////////////

// Mutex handle for moisture sensor data/status access
xSemaphoreHandle xMoistureDataSemaphore;

//////////////////////
//  Global Variables
//////////////////////
// Updated by Sensor Task, read and cleared by SerialMsgProc.
volatile U8 SensorStatus;

// Updated by Sensor Task, read by SerialMsgProc.
volatile U8 SensorData[ MAX_MOISTURE_DATA_STRLEN ];

// Internal task variables
volatile U8 tempSensorData[ MAX_MOISTURE_DATA_STRLEN ];

static portTickType xLastWakeTime;
static const portTickType xFrequency = MOISTURE_SENSOR_MEAS_PERIOD_MSECS;
static portTickType xSensorPwrupTime;	// Time that sensor was powered up
static portTickType xSystemTimeNow;
static portTickType TicksSinceSensorPwrup;

// Moisture Sensor Measurement State
volatile MSENS_STATES_e Msense_State_e;

// Variables for testing only
volatile U32 ms_cycles;
volatile U32 ms_errors;

//////////////////////////////////////////////////////
//
//    void CreateMoistureSensorDataMutex( void )
//
//    Synopsis: Creates the FreeRTOS mutex semaphore
//    that ensures mutually exclusive access to the
//    moisture sensor data/status.
//
//    Notes:
//
//    1. Called from main() during initialization
//    2. Will not return if mutex cannot be created
//////////////////////////////////////////////////////
void CreateMoistureSensorDataMutex( void )
{
	xMoistureDataSemaphore = xSemaphoreCreateMutex();

	if( xMoistureDataSemaphore == NULL )
	{
		Fatal_Error( MOISTURE_SENSOR_MUTEX_CREATE_ERROR );
	}
}

// Wrappers for FreeRTOS mutex access

// For gaining accessing to Moisture Sensor data/status.
// Blocks if other task already "owns" the mutex.
void TakeMoistureSensorDataMutex( void )
{
	xSemaphoreTake( xMoistureDataSemaphore, portMAX_DELAY );
}

// For giving up the mutex
void GiveMoistureSensorDataMutex( void )
{
	xSemaphoreGive( xMoistureDataSemaphore );
}

/////////////////////////////////////////////////
//
//    BOOL ReadSensorData( void )
//
//    Synopsis:
//
//    Reads sensor measurement data into data
//    buffer SensorData[].
//
//    Note: All data is read from the UART Rx FIFO.
//
//    Returns: TRUE if success
//             FALSE if failure
////////////////////////////////////////////////
BOOL ReadSensorData( void )
{
	BOOL bResult;
	U8 data8;
	U8 bytes;
	U8 i;
	U32 sum;

	bytes = 0;
	bResult = FALSE;

	while( ( SpiRdReg( SPI_UART_LSR ) & 1 )  )
	{
		// All UART rx data is read but not necessarily used/stored
		data8 = SpiRdReg( SPI_UART_RHR );

		if( !bResult && bytes < MAX_MOISTURE_DATA_STRLEN )
		{
			tempSensorData[ bytes ] = data8;
			bytes++;

			if( data8 == 0x0a )
			{
				// Append NULL
				tempSensorData[ bytes ] = '\0';
				bResult = TRUE;
			}
		}
	}

	// Verify DDI checksum too
	sum = 0;
	i = 0;

	while(tempSensorData[ i ] != 0x0d )
	{
		sum += tempSensorData[ i++ ];
	}

	// Include the carriage return
	sum += tempSensorData[ i++ ]; 

	// and include one byte past first carriage return
	sum += tempSensorData[ i++ ]; 

	// Compute checksum, make printable character
	sum = ( sum % 64 ) + ' ';

	if( sum != tempSensorData[ i ] )
	{
		bResult = FALSE;
	}

#ifdef DEBUG
	if( bResult )
	{
		debug_printf("Good\n");
	}
	else
	{
		debug_printf("Bad!\n");
	}
#endif
	return bResult;
}

//////////////////////////////////////////////////////////////
//
//    void MoistureSensorTask( void *pvParameters )
//
//    This FreeRTOS task handles the cycling of the moisture
//    sensor power and the capture and its sensor data.
//////////////////////////////////////////////////////////////
void MoistureSensorTask( void *pvParameters )
{
	U8 data8;
	U8 IoResult;

	// Initialize SPI port
	init_sspx( 4000000 );

	// Initialize SPI UART for 1200 baud 8-N-1 data format
	init_SpiUart();

	// For testing only
	ms_cycles = 0;
	ms_errors = 0;

	// Initialize moisture sensor data structures
	Msense_State_e = MSENS_WAIT_TO_MEASURE_e;

	// Initialize the xLastWakeTime variable with the current time.
	xLastWakeTime = xTaskGetTickCount();

	//////////////////////////////////////
	// Perform moisture sensor processing 
	//////////////////////////////////////
	for( ;; )
	{
		switch( Msense_State_e )
		{
			case MSENS_WAIT_TO_MEASURE_e:

				// Wait for the next cycle.
				vTaskDelayUntil( &xLastWakeTime, xFrequency );

				// Time's up: time to take a moisture measurement

				// Set the default status for this cycle
				IoResult = MOISTURE_DATA_STATUS_NONE;

				// Record time that sensor was powered up
				xSensorPwrupTime = xTaskGetTickCount();

				// Count the cycle
				ms_cycles++;

				// Enable LT8300 to power up the moisture sensor
				SetGpioPinValue( SENSOR_PWR_ENA_PORT, SENSOR_PWR_ENA_BIT, 1 );

				Msense_State_e = MSENS_WAIT_FOR_MARKING_e;

				// Fall through intentionally

			case MSENS_WAIT_FOR_MARKING_e:

				////////////////////////////////
				//    Wait for TXD to go high
				////////////////////////////////
				if( (data8 = GetGpioPinValue( SENSOR_TXD_MONITOR_PORT, SENSOR_TXD_MONITOR_BIT ) ) == 1 )
				{
					// Sensor TXD line is high

					// Enable UART Rx (LCR[7] must be 0)
					SpiWrReg( SPI_UART_EFCR, 0x00 );

					// Now go wait for the sensor data
					Msense_State_e = MSENS_WAIT_FOR_DATA_e;
				}
				else
				{
					// Sensor TXD line is low

					// Check time since sensor was powered up
					TicksSinceSensorPwrup = xTaskGetTickCount() - xSensorPwrupTime;

					if( TicksSinceSensorPwrup >= MOISTURE_SENSOR_DATA_TIMEOUT_MS )
					{
						// Sensor did not send data in time
						Msense_State_e = MSENS_CYCLE_END_e;
					}
				}
				break;

			case MSENS_WAIT_FOR_DATA_e:

				// Check if SPI UART IRQ- has gone low
				if( GetGpioPinValue( SPI_UART_IRQ_PORT, SPI_UART_IRQ_BIT ) == 0 )
				{
					// Yes, the UART IRQ- line is low

					// Read the interrupt status
					data8 = SpiRdReg( SPI_UART_ISR );

					// Check for Special character detected (bit 4 set)
					if( ( data8 & 0x3f ) == 1<<4 )
					{
						// Go read the data that the sensor sent..
						if( ReadSensorData() == TRUE )
						{
							// If the data has a valid checksum
							IoResult = MOISTURE_DATA_STATUS_LAST_READING_VALID;
						}
						else
						{
							// We did not receive valid sensor data
							IoResult = MOISTURE_DATA_STATUS_INVALID;
							ms_errors++;
						}
					}

					Msense_State_e = MSENS_CYCLE_END_e;
				}
				else
				{
					// Check time since sensor was powered up
					TicksSinceSensorPwrup = xTaskGetTickCount() - xSensorPwrupTime;

					if( TicksSinceSensorPwrup >= MOISTURE_SENSOR_DATA_TIMEOUT_MS )
					{
						// Sensor did not send data in time
						Msense_State_e = MSENS_CYCLE_END_e;
					}
					else
					{
						// Wait about a character time
						vTaskDelay( MS_to_TICKS( 9 ) );
					}
				}
				break;

			case MSENS_CYCLE_END_e:

				/////////////////////////////////////////////////////
				//
				//    Sensor cycle end clean-up. Arrive here with 
				//    IoResult set as follows:
				//
				//    MOISTURE_STATUS_LAST_DATA_VALID
				//    MOISTURE_STATUS_NO_DATA:
				//    MOISTURE_STATUS_LAST_DATA_VALID:
				// 
				/////////////////////////////////////////////////////

				init_SpiUart();

				// Disable LT8300 to power down the moisture sensor
				SetGpioPinValue( SENSOR_PWR_ENA_PORT, SENSOR_PWR_ENA_BIT, 0 );

				// Update data/status for serial message processor

				// Get access to the shared variables
				TakeMoistureSensorDataMutex();

				// Or-in latest I/O status
				//
				// 1/27/2016 rmd : The status byte accumulates the 3 possible results. And could have all 3
				// bits set at the same time. This can takes place when the TPMicro reads the decoder at a
				// less frequent rate than the decoder reads the moisture sensor. In other words the status
				// reflect what happened during the last x number of sensor readings. If the VALID bit is
				// set then the result string is guaranteed to hold good data. It just may not be the data
				// from the latest moisture sensor read attempt. But that is OK!
				SensorStatus |= IoResult;

				// If good data was received, update the SensorData array
				if( IoResult == MOISTURE_DATA_STATUS_LAST_READING_VALID )
				{
					memcpy( (char *) SensorData, (char *) tempSensorData, MAX_MOISTURE_DATA_STRLEN );

					// 1/27/2016 rmd : Update sensor data length field to reflect the actual string length of
					// this last GOOD reading.
					SensorStatus &= ~MOISTURE_STATUS_DATA_STRLEN_MASK;
					SensorStatus |= strlen( (char *) SensorData );
				}
				else
				{
					// No valid data obtained this cycle:

					// We don't overwrite the current SensorData[] data. Its data length
					// will be cleared by the SerialMsgProc when it's reported to
					// the TP Micro.

					// Clear status bit for 'last reading good'
					//
					// 1/27/2016 rmd : We use the length bits to 'know' if during the period since the TPMicro
					// last obtained the reading there was at least ONE good reading. This status bit indicates
					// only if the LAST reading attempt produced a valid result. Which is actually a fairly
					// useless and confusing thing. This whole LAST READING VALID bit is really only an
					// internally used bit (internal to the decoder).
					SensorStatus &= ~MOISTURE_DATA_STATUS_LAST_READING_VALID;
				}

				/////////////////////////////////////////////////////////////////
				// Show I/O result and sensor status (as reported to TP Micro)
				/////////////////////////////////////////////////////////////////
#ifdef DEBUG
				debug_printf("IoS: 0x%02x SS: 0x%02x\n", IoResult, SensorStatus );
#endif
				// Release the mutex for the shared data area
				GiveMoistureSensorDataMutex();

				// Reinitialize state machine for the next measurement
				Msense_State_e = MSENS_WAIT_TO_MEASURE_e;
				break;
		}
	}
	//////////////////////////////////////////////
	// END OF SENSOR MEASUREMENT PROCESSING
	//////////////////////////////////////////////
}
