//////////////////////////////////////////////////////////////////////////
//
//	File:    moisture_sensor.h
//
//          Calsense Moisture Sensor support
//
//  NOTE:   For LPC11E14 Microprocessor
//
//	Author:  David K. Squires
//          Fredonia Mountain Technologies, LLC
//          for Calsense, Carlsbad, CA
//
// Synopsis: Periodically powers up the attached Decagon moisture sensor
//           and obtains moisture sensor data, then powers down the 
//           sensor until the next measurement is needed.
//////////////////////////////////////////////////////////////////////////

// Measure moisture once per 60 seconds
#define MOISTURE_SENSOR_MEAS_PERIOD_MSECS   MS_to_TICKS(60000)

// Configure time to allow for sensor to output data (ms)
#define MOISTURE_SENSOR_DATA_TIMEOUT_MS    MS_to_TICKS(300)

// Moisture Sensor Task State Machine States
typedef enum
{
   MSENS_WAIT_TO_MEASURE_e    = 0,
   MSENS_WAIT_FOR_MARKING_e   = 1,
   MSENS_WAIT_FOR_DATA_e      = 2,
   MSENS_CYCLE_END_e          = 3,
} MSENS_STATES_e;

// Configure GPIO port/pin for LT8300 Switcher Power Enable (Active High)
#define SENSOR_PWR_ENA_PORT      0
#define SENSOR_PWR_ENA_BIT       20

// Configure GPIO port/pin for sensor TXD state monitor
#define SENSOR_TXD_MONITOR_PORT  0
#define SENSOR_TXD_MONITOR_BIT   22

// Configure delay between sensor power application and sensor TXD line monitoring
#define SENSOR_PWRUP_TXD_MONITOR_DELAY_MS     15

// Function prototypes
extern void CreateMoistureSensorDataMutex( void );
extern void TakeMoistureSensorDataMutex();
extern void GiveMoistureSensorDataMutex();
extern void MoistureSensorTask( void *pvParameters );

////////////////
// Global data
////////////////

/////////////////////////////////
//  Moisture Sensor Data/Status
/////////////////////////////////
extern volatile U8 SensorData[ MAX_MOISTURE_DATA_STRLEN ];
extern volatile U8 SensorStatus;

