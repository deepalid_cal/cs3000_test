//////////////////////////////////////////////////////////////////////////
//
//	File:    spi_uart.c
//
//          Support for Exar SPI/I2C UART in SPI mode
//
// NOTE:    For Exar XR2011M series UARTs
//
//	Author:  David K. Squires
//          Fredonia Mountain Technologies, LLC
//          for Calsense, Carlsbad, CA
//
// Synopsis:   
//
//////////////////////////////////////////////////////////////////////////
#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "eeprom.h"
#include "Calsense_2-Wire_Decoder.h"
#include "gpio.h"
#include "sspx.h"
#include "spi_uart.h"

#define UART_TX_BUFFER_SIZE   2
#define UART_RX_BUFFER_SIZE   32

/////////////////////////////////
//    UART Variables
/////////////////////////////////

// UART SSPx IO Control Block
SSPX_IO_CTL_s Uart_Sspx_Io_Ctl;

// SSPx Transmit and Receive buffers
U8 UartTxBuffer[ UART_TX_BUFFER_SIZE ];
U8 UartRxBuffer[ UART_RX_BUFFER_SIZE ];

//////////////////////////////////////////////////////////////
//
//    void SpiUartReset( BOOL state )
//
//    BOOL state;
//
//       state = TRUE:     RESET- is driven active low
//       state = FALSE:    RESET- is driven inactive high
//
//    Synopsis:
//
//    This function controls the SPI UART RESET- input line.
//////////////////////////////////////////////////////////////
void SpiUartReset( BOOL state )
{
   U8 pinval;

   pinval = state ? 0 : 1;

   SetGpioPinValue( SPI_UART_RESET_PORT, SPI_UART_RESET_BIT, pinval );
}

///////////////////////////////////////////////////
//
//    void SpiWrReg( U8 reg, U8 data )
//
//    U8 reg;     UART register to be written
//    U8 data;    Data to be written
///////////////////////////////////////////////////
void SpiWrReg( U8 reg, U8 data )
{
   // Initialize control block for I/O
   Uart_Sspx_Io_Ctl.TxBfr = UartTxBuffer;
   Uart_Sspx_Io_Ctl.TxCnt = 2;
   Uart_Sspx_Io_Ctl.RxBfr = UartRxBuffer;
   Uart_Sspx_Io_Ctl.RxCnt = 0;

   UartTxBuffer[0] = reg << 3;   // Data to transmit to UART
   UartTxBuffer[1] = data;

   // Perform I/O
   sspx_io( &Uart_Sspx_Io_Ctl );
}

///////////////////////////////////////////////////
//
//    U8 SpiRdReg( U8 reg )
//
//    U8 reg;     Register to read from
//
//    Returns:    Data read from the register
//
///////////////////////////////////////////////////
U8 SpiRdReg( U8 reg )
{
   // Initialize control block for I/O
   Uart_Sspx_Io_Ctl.TxBfr = UartTxBuffer;
   Uart_Sspx_Io_Ctl.TxCnt = 1;
   Uart_Sspx_Io_Ctl.RxBfr = UartRxBuffer;
   Uart_Sspx_Io_Ctl.RxCnt = 1;

   // Set Read bit and shift in the register to read
   UartTxBuffer[0] = 0x80 | reg << 3;   // Data to transmit to UART

   // Perform I/O
   sspx_io( &Uart_Sspx_Io_Ctl );

   // Fetch value read from receive buffer
   return (UartRxBuffer[0] );
}

/////////////////////////////////////////////////////////////
//
//    void SpiFlushRxData( void )
//
//    Synopsis: Flushes data from the UART Rx FIFO
//
/////////////////////////////////////////////////////////////
void SpiFlushRxData( void )
{
   volatile U8 data8;

   SpiWrReg( SPI_UART_FCR, 0x03 );     // Reset Rx FIFO

   // Read Rx data until no more rx data ready
   while( SpiRdReg( SPI_UART_LSR ) & 1 )
   {
      // Input data from Rx Holding Register
      data8 = SpiRdReg( SPI_UART_RHR );
   }
}

////////////////////////////////////////////////////////////
//
//    void init_SpiUart(void)
//
//    Synopsis: 
//
//    Initializes the SPI UART for receive only using
//    1200 baud, 8-N-1 data format.
//
//    NOTES:
//
//    The UART expects to receive a 96 KHz clock on the
//    XTAL1 input (pin 7). This signal originates from the
//    CLKOUT (pin 4) of the LPC11E14 and is 12 MHz / 125.
//
//    The UART DLM/DLL further divides the 96 KHz external
//    clock by 5 to provide the UART a 19.2 KHz clock. This
//    is 16X the 1200 baud operating rate to allow for 16X
//    sampling rate.
//
//    The RESET- input is driven from the LPC11E14 P0.23 pin.
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
void init_SpiUart(void)
{
   volatile U8 data8;

   // Reset the UART using its RESET- input line
   SpiUartReset( TRUE );
   SpiUartReset( FALSE );

   // Select DLl/DLM by setting LCR[7]
   SpiWrReg( SPI_UART_LCR, 0x83 );

   // Set DLL = 0x05;
   SpiWrReg( SPI_UART_DLL, 0x05 );

   // Set DLM = 0x00;
   SpiWrReg( SPI_UART_DLM, 0x00 );

   /////////////////////////
   // Set DLD 
   /////////////////////////

   // Set LCR[7] = 1
   SpiWrReg( SPI_UART_LCR, 0xbf );     // For selecting EFR

   // Set EFR[4] = 1
   SpiWrReg( SPI_UART_EFR, 1<<4 );

   // Load DLD (DlD[5], DLD[4] Both 0 for 16X Sampling!)
   SpiWrReg( SPI_UART_DLD, 0x00 );

   // DONE WITH DLD

   // Initiialize LCR
   SpiWrReg( SPI_UART_LCR, 0x03 );     // Configure N-8-1 No parity

   // Initialize FCR
   // Enable FIFOs with 8 bytes present (Rx FIFO) or available (Tx FIFO)
   SpiWrReg( SPI_UART_FCR, 0x03 );     // Reset Rx FIFO

   // Initialize MCR to set prescaler to divide by one
   SpiWrReg( SPI_UART_MCR, 0x00 );

   // Initialize EFR and XOFF-2 to allow special char (linefeed) to
   // be detected.
   SpiWrReg( SPI_UART_LCR, 0xbf );

   // Load the special character into XOFF-2
   SpiWrReg( SPI_UART_XOFF2, SPI_UART_SPEC_CHAR );

   // Set LCR to normal values again
   SpiWrReg( SPI_UART_LCR, 0x03 );     // Configure N-8-1 No parity

   // Disable Rx (LCR[7] must be 0)
   // Rx is enabled once the sensor TXD line goes high (marking)
   SpiWrReg( SPI_UART_EFCR, 0x02 );

   /////////////////////////////////////////////
   // Enable interrupt for special char detect
   /////////////////////////////////////////////

   // First set EFR[5] and EFR[4]
   SpiWrReg( SPI_UART_LCR, 0xbf );
   SpiWrReg( SPI_UART_EFR, ( 1<< 5 | 1<<4 ) );

   // Then set IER[5]
   SpiWrReg( SPI_UART_LCR, 0x03 );     // Must make LCR[7] = 0
   data8 = SpiRdReg( SPI_UART_IER );
   SpiWrReg( SPI_UART_IER, ( data8 | 1<<5 ) );

   // Purge Rx FIFO
   SpiFlushRxData();
}


