///////////////////////////////////////////////////////////
//
//    File: sspx.h
//
//    Header file for LPC11Exx SSPx I/O Functions
//
//    Fredonia Mountain Technologies, LLC
//
///////////////////////////////////////////////////////////

// Specify which SSPx port to use (0 or 1)
#define SSP_CH		1

// SPI clock speed in Hz
#define SSPx_CLK_HZ	4000000     // 4 MHz

// Configure GPIO for Slave Select
#define SSEL_GPIO_PORT     1
#define SSEL_GPIO_BIT      19

// Dummy write data value
#define DUMMY_WRITE_DATA   0xAF

///////////////////////////////
// SSPX I/O CONTROL STRUCTURE
///////////////////////////////
typedef struct
{
   U8 *TxBfr;
   U8 TxCnt;
   U8 *RxBfr;
   U8 RxCnt;

} SSPX_IO_CTL_s;


// CR0 Fields
#define CR0_DSS            0x7<<0   // 8-bits
#define CR0_FRM_FMT        0x0<<4   // SPI
#define CR0_CPOL           0x0<<6   // Clock idles low
#define CR0_CPHA           0x0<<7   // Clock data on rising edge
#define CR0_SCR            0x0<<8   // Prescaler (0 value = x1)

// CR1 Fields
#define CR1_LBM            0x0<<0   // Loop Back mode disabled
#define CR1_SSE            0x1<<1   // SPI controller enabled
#define CR1_MS             0x0<<2   // SPI Master mode enabled
#define CR1_SOD            0x0<<3   // Slave output disable (don't care in master mode)

// Status register (SR) bits
#define SR_TFE             0x1<<0   // Transmit FIFO Empty
#define SR_TNF             0x1<<1   // Transmit FIFO Not full
#define SR_RNE             0x1<<2   // Receive FIFO Not Empty
#define SR_RFF             0x1<<3   // Receive FIFO Full
#define SR_BSY             0x1<<4   // SPI Controller Busy


// Function prototypes
extern void init_sspx( U32 clk_hz );
extern void sspx_purge_rx_fifo( void );
extern void SSPx_Select_Slave(BOOL state);
extern void sspx_io( SSPX_IO_CTL_s *pControl_Block );
extern void sspx_tx( U8 *pData, U8 bytes );