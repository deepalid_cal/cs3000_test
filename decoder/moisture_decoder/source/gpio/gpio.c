////////////////////////////////////////////////////////
//
//    File: gpio.c
//
//    GPIO I/O functions
//
////////////////////////////////////////////////////////
#ifndef NDEBUG
#include <cross_studio_io.h>
#endif

#include "LPC1000.h"
#include "LPC11Exx.h"
#include "data_types.h"
#include "gpio.h"

//////////////////////////////////////////////////////////////////////////////
//
//    void SetGpioPinValue(U32 port, U32 bit, U32 state)
//
//    Sets or clears a port pin based upon state
//
//    port:    The GPIO port, either 0 or 1
//    bit:     The bit within the port, 0 to 31
//    state    The state to which the port pin is driven. 0 = low, 1 = high
//
//////////////////////////////////////////////////////////////////////////////
void SetGpioPinValue(U32 port, U32 bit, U32 state)
{
   U32 *pPinReg;

   pPinReg = (U32 *) GPIO_PIN_REG_ADDR(port, bit);

   *pPinReg = state;
}

//////////////////////////////////////////////////////////////////////////////
//
//    U32 GetGpioPinValue(U32 port, U32 bit)
//
//    Returns a port pin state (valid for inputs or outputs)
//
//    port:    The GPIO port, either 0 or 1
//    bit:     The bit within the port, 0 to 31
//
//    Returns: 0 (pin is low), 1 (pin is high)
//
//////////////////////////////////////////////////////////////////////////////
U32 GetGpioPinValue(U32 port, U32 bit)
{
   U32 *pPinReg;

   pPinReg = (U32 *) GPIO_PIN_REG_ADDR(port, bit);

   return (*pPinReg & 1);
}

///////////////////////////////////////////////////////////////////////////
//
//    void CfgPinFunc( U32 port, U32 bit, U32 cfg_value )
//
//    Initializes the pin configuration register for a port pin.
//
//    U32 port :     0 or 1
//    U32 bit  :     0..31 (the bit within the port)
//    U32 cfg_value: The value to be written to the pin's configuration 
//                   register
//
///////////////////////////////////////////////////////////////////////////
void CfgPinFunc( U32 port, U32 bit, U32 cfg_value )
{
   U32 *pCfgReg;

   // Calculate address of pin configuration register
   pCfgReg = (U32 *) IOCON_ADDR(port, bit);

   // Configure the pin
   *pCfgReg = cfg_value;
}

//////////////////////////////////////////////////////////////////////////
//
//    void InitPinAsAltFunc( U32 port, U32 bit, U32 func, U32 options )
//
//    Initializes a port/bit as a GPIO input
//
//    func: 1 - n (depends upon port/bit)
//
//    Options: any bitwise-or'd combination of:
//
//             _MODE(n), _HYS(n), _INV(n), _OD(n)
//
//    See gpio.h for valid macro arguments.
//////////////////////////////////////////////////////////////////////////
void InitPinAsAltFunc( U32 port, U32 bit, U32 func, U32 options )
{
   U32 cfg_value;

   // load value to write to I/O configuration register
   cfg_value = func | options;

   // Now handle pin options
   CfgPinFunc(port, bit, cfg_value );
}

//////////////////////////////////////////////////////////////////////////
//
//    void InitPinAsGpioInput( U32 port, U32 bit, U32 options )
//
//    Initializes a port/bit as a GPIO input
//
//    Options: any bitwise-or'd combination of:
//
//             _MODE(n), _HYS(n), _INV(n), _OD(n), _DIG(n)
//
//    See gpio.h for valid macro arguments.
//////////////////////////////////////////////////////////////////////////
void InitPinAsGpioInput( U32 port, U32 bit, U32 options )
{
   U32 *pDirReg;
   U32 pin_mask;

   // Calculate pointer to the pin direction register for the port
   pDirReg = (U32 *) ( GPIO_PORT_DIR_REG_BASE_ADDR + ( port * GPIO_PORT_DIR_REG_PORT_OFFS ) );

   // Calculate pin bit mask
   pin_mask = 1<<bit;

   // Set up pin as an input
   *pDirReg &= (~pin_mask);

   // Now handle pin options
   CfgPinFunc(port, bit, options);
}

//////////////////////////////////////////////////////////////////////////////////////
//
//    void InitPinAsGpioOutput( U32 port, U32 bit, U32 initial_state, U32 options )
//
//    Initializes a port/bit as a GPIO output
//
//    initial_state: 0 = set pin low, non-zero = set pin high
//
//    Options: any bitwise-or'd combination of:
//
//             _MODE(n), _HYS(n), _INV(n), _OD(n)
//
//    See gpio.h for valid macro arguments.
//////////////////////////////////////////////////////////////////////////
void InitPinAsGpioOutput( U32 port, U32 bit, U32 initial_state, U32 options )
{
   U32 *pDirReg;
   U32 pin_mask;
   U32 *pWordPinReg;

   // Write to I/O configuration register for this pin (GPIO)
   CfgPinFunc(port, bit, options);

   // Calculate pointer to the pin direction register for the port
   pDirReg = (U32 *) ( GPIO_PORT_DIR_REG_BASE_ADDR + ( port * GPIO_PORT_DIR_REG_PORT_OFFS ) );

   // Calculate pin bit mask
   pin_mask = 1<<bit;

   // Set up pin as an output (GPIO)
   *pDirReg |= pin_mask;

   // Calculate address of word pin register
   pWordPinReg = (U32 *) GPIO_PIN_REG_ADDR(port, bit);

   // Set the initial pin output state
   *pWordPinReg = initial_state;
}

///////////////////////////////////////////////////
//   Decoder I/O Configuration
///////////////////////////////////////////////////

//    PIN   SIGNAL      PORT.BIT    DESCRIPTION
//    ===   ========    =======     ========================
//     14   HBRG_RT     P0.3        H-Bridge MOSFET Gate drive 
//     22   HBRG_RB     P0.6        H-Bridge MOSFET Gate drive
//     23   HBRG_LT     P0.7        H-Bridge MOSFET Gate drive
//     27   HBRG_LB     P0.8        H-Bridge MOSFET Gate drive
//
//     32   VSOL1       P0.11       Solenoid 1 voltage
//     33   VSOL2       P0.12       Solenoid 2 voltage
//     34   VHBRG       P0.13       H-Bridge voltage
//     35   VTHERM      P0.14       Thermistor voltage
//    
//     36   PWM_SOL1    P1.13       CT16B0_MAT0
//     37   PWM_SOL2    P1.14       CT16B0_MAT1
//
//     18   CAPCON_SOL1 P1.23       Connects SOL1 capacitor for charging
//     21   CAPCON_SOL2 P1.24       Connects SOL2 capacitor for charging
//
//     11   RXD         P1.26       UART RXD
//     12   TXD         P1.27       UART TXD
//
//     45   FS_PULSE-   P0.17       Flow sensor input (digital)
//
//     1    TP2         P1.25       Decoder test point pad
//
//     2    MS_SSEL1-      P1.19    SSEL- to Moisture Sensor SPI UART
//     4    MS_CLK_96_KHZ  P0.1     96 KHz CLKOUT to SPI UART (initialized in init_clkout() for moisture sensor)
//     
//     9    MS_5V_ISO_ON   P0.20    CPU output to turn on +5V isolated power for moisture sensor
//     13   MS_SCK1        P1.20    SPI SCK to moisture mensor SPI UART 
//     17   MS_IRQ-        P0.21    IRQ output from moisture mensor SPI UART. Polled by CPU.
//     24   TP5            P1.28    Decoder test point pad
//     26   MS_MISO1       P1.21    Moisture sensor SPI UART slave data output to CPU
//     30   MS_RXD         P0.22    Moisture sensor SPI UART RXD output to CPU
//     38   MS_MOSI1       P1.22    Moisture sensor SPI UART slave data input from CPU
//     42   MS_UART_RST-   P0.23    Moisture sensor SPI UART reset input (driven from CPU GPIO)
//


///////////////////////////////////////////////////
//
//    init_pins(void)
//
//    Initializes the LPC11E14 pins to match the  
//    2-wire decoder configuration shown above.
//
///////////////////////////////////////////////////
void init_pins(void)
{
   // 3                P0.0         RESET- pin

   // 32   VSOL1       P0.11       Solenoid 1 voltage
   InitPinAsAltFunc( 0, 11, 2, 0 );    // ADC input AD0

   // 33   VSOL2       P0.12       Solenoid 2 voltage
   InitPinAsAltFunc( 0, 12, 2, 0 );    // ADC input AD1

   // 34   VHBRG       P0.13       H-Bridge voltage
   InitPinAsAltFunc( 0, 13, 2, 0 );    // ADC input AD2

   // 35   VTHERM      P0.14       Thermistor voltage                            
   InitPinAsAltFunc( 0, 14, 2, 0 );    // ADC input AD3

   // 14   HBRG_RT     P0.3        H-Bridge MOSFET Gate drive 
   InitPinAsGpioOutput( 0, 3, 0, 0 );  // GPIO output, initial state = 0

   // 22   HBRG_RB     P0.6        H-Bridge MOSFET Gate drive
   InitPinAsGpioOutput( 0, 6, 0, 0 );  // GPIO output, initial state = 0 (was 1?)

   // 23   HBRG_LT     P0.7        H-Bridge MOSFET Gate drive
   InitPinAsGpioOutput( 0, 7, 0, 0 );  // GPIO output, initial state = 0 (was 1?)

   // 27   HBRG_LB     P0.8        H-Bridge MOSFET Gate drive
   InitPinAsGpioOutput( 0, 8, 0, 0 );  // GPIO output, initial state = 0

   // 18   CAPCON_SOL1 P1.23       Connects SOL1 capacitor for charging
   InitPinAsGpioOutput( 1, 23, 0, 0 );  // GPIO output, initial state = 0

   // 21   CAPCON_SOL2 P1.24       Connects SOL2 capacitor for charging
   InitPinAsGpioOutput( 1, 24, 0, 0 );  // GPIO output, initial state = 0

   //////////////////////////////////////////////////
   // NOTE: PWM MODE IS NOT USED FOR MAT0 and MAT1!
   //////////////////////////////////////////////////

   // 36   PWM_SOL1    P1.13       CT16B0_MAT0
   InitPinAsGpioOutput( 1, 13, 0, 0 );    // GPIO output, initial state = 0

   // 37   PWM_SOL2    P1.14       CT16B0_MAT1
   InitPinAsGpioOutput( 1, 14, 0, 0 );    // GPIO output, initial state = 0
   
   ////////// END OF PWM OUTPUT INITIALIZATIONS ////
   
   // 11   RXD         P1.26       UART RXD
   InitPinAsAltFunc( 1, 26, 2, 0 );    // func = RXD

   // 12   TXD         P1.27       UART TXD
   InitPinAsAltFunc( 1, 27, 2, 0 );    // func = TXD

   // 45   TEST POINT
   InitPinAsGpioOutput( 0, 17, 0, 0 );    // func = GPIO output (low)

   //////////////////////////////////////////////////////////////
   // Finally to conserve power, all unused pins are configured
   // as GPIO outputs, 0 state, with no pull-up/pull-down
   //
   // Listed in increasing pin number order
   //////////////////////////////////////////////////////////////
   // Pin   Port.bit
   // 1:    P1.25 Test point 2
   InitPinAsGpioOutput( 1, 25, 0, 0 );

   // 2:    P1.19  MOISTURE SENS: SSEL1 (Must use GPIO output)
   InitPinAsGpioOutput( 1, 19, 1, 0 );    // initial state = high

   // 4:    P0.1
   // This pin is initialized in init_clkout() for MOISTURE SENS

   // 9:    P0.20  MOISTURE SENS: 5V_ISO_ON
   InitPinAsGpioOutput( 0, 20, 0, 0 );    // Initial state: low

   // 10:   P0.2
   InitPinAsGpioOutput( 0, 2, 0, 0 );

   // 13:   P1.20  MOISTURE SENS: SCK1     
   InitPinAsAltFunc( 1, 20, 2, 0 );

   // 15:   P0.4
   InitPinAsGpioOutput( 0, 4, 0, 0 );

   // 16:   P0.5
   InitPinAsGpioOutput( 0, 5, 0, 0 );

   // 17:   P0.21 MOISTURE SENS: UART IRQ-
   InitPinAsGpioInput( 0, 21, 0 );

   // 24:   P1.28 Test point 5
   InitPinAsGpioOutput( 1, 28, 0, 0 );

   // 25:   P1.31
   InitPinAsGpioOutput( 1, 31, 0, 0 );

   // 26:   P1.21 MOISTURE SENS: MISO1
   InitPinAsAltFunc( 1, 21, 2, 0 );

    // 28:   P0.9
   InitPinAsGpioOutput( 0, 9, 0, 0 );

   // 30:   P0.22 MOISTURE SENS: RXD input (idles high)
   InitPinAsGpioInput( 0, 22, _DIG(1) );

   // 31:   P1.29
   InitPinAsGpioOutput( 1, 29, 0, 0 );

   // 38:   P1.22 MOISTURE SENS: MOSI1
   InitPinAsAltFunc( 1, 22, 2, 0 );

   // 39:   P0.15  SWDIO (FOR SWD INTERFACE)
   InitPinAsAltFunc( 0, 15, 0, _DIG(1));        // SWDIO, Digital functional mode

   // 40:   P0.16  Unused pin, configured as GPIO output (low)
   InitPinAsGpioOutput( 0, 16, 0, _DIG(0) );    // Digital functional mode

   // 42:   P0.23 MOISTURE SENS: Output UART_RST-
   InitPinAsGpioOutput( 0, 23, 1, _DIG(1) );    // Initial state: high

   // 43:   P1.15
   InitPinAsGpioOutput( 1, 15, 0, 0 );

   // 46:   P0.18
   InitPinAsGpioOutput( 0, 18, 0, 0 );

   // 47:   P0.19
   InitPinAsGpioOutput( 0, 19, 0, 0 );

   // 48:   P1.16
   InitPinAsGpioOutput( 1, 16, 0, 0 );
}
