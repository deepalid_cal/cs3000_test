/******************************* KRNLLIST.C ********************************
*
* 1998 rmd
* This file contains the kernel entry point routines for list management.
* Under Calsense 1999 SMT Board
*
* 01.15.2009 rmd
* Now updated for the UC3A FreeRTOS environment.
*
* 2011.06.01 rmd
* Updated for the LPC3250 and GCC environment.
* 
* 
****************************************************************************/

/**** Include Files ****/

#include	"ks_common.h"




// -----------------------------------------
// -----------------------------------------



/*************************** List Functions *******************************/

/*--------------------------------------------------------------------------
  Name:           ListInit()

  Description:    This routine initializes a list header.

  Parameters:     listhdr           pointer to the list header structure.
				  offset            offset in bytes of the DLINK structure
									in elements on this list.

  Return Values:  None
 -------------------------------------------------------------------------*/
void nm_ListInit( MIST_LIST_HDR_TYPE_PTR plisthdr, int offset ) {

	plisthdr->phead = plisthdr->ptail = NULL;
	plisthdr->count = 0;
	plisthdr->offset = offset;

	#if defined(LIST_DEBUG) && LIST_DEBUG
		plisthdr->InUse = FALSE;
	#endif

} /* End ListInit */


/**** List Insertion Functions ****/

/*--------------------------------------------------------------------------
  Name:           ListInsert()

  Description:    This routine inserts an element before another element
				  on the list.  If old_elem is NULL it will insert at the
				  end of the list.

  Parameters:     listhdr           pointer to the list header structure.
				  new_elem          pointer to element to add
				  old_elem          pointer to existing element in list, or
									NULL.

  Return Values:  SUCCESS           new element was inserted.
				  INVALID_PARAMETER old_elem was not on the list specified.
 -------------------------------------------------------------------------*/
MIST_RET_CODE_TYPE nm_ListInsert (MIST_LIST_HDR_TYPE_PTR plisthdr, void *new_elem, void *old_elem) {

char				*new, *old, *prev;
MIST_DLINK_TYPE_PTR	dlinknew, dlinkold, dlinkprev;
MIST_RET_CODE_TYPE	rv;


	if ( !plisthdr || !new_elem ) {

		rv = MIST_INVALID_PARAMETER;        /* Safety check */

	} else {

		#if (LIST_DEBUG == 1)

			if ( plisthdr->InUse == TRUE ) {
			
				Alert_Message( "KRNLLIST.C List Insert re-entered!" );
				
				FREEZE_and_RESTART_APP;
			}

			plisthdr->InUse = TRUE;
		#endif
		
		if ( plisthdr->count == 0 ) {            /* Check for empty list */
			/* Empty list.  Add first element */
			plisthdr->phead = plisthdr->ptail = new_elem;
			plisthdr->count = 1;
	
			new = (char *)new_elem;             /* Get DLINK of new element */
			dlinknew = (MIST_DLINK_TYPE_PTR) (new + plisthdr->offset);
	
			dlinknew->pNext = dlinknew->pPrev = NULL;
			dlinknew->pListHdr = plisthdr;           /* Point back to list header */
	
			#if defined(LIST_DEBUG) && (LIST_DEBUG==1)
				plisthdr->InUse = FALSE;
			#endif
	
			rv = MIST_SUCCESS;
	
		} else {
		
			/* Get pointer to existing element */
			if ( old_elem == NULL ) {               /* Check for append to tail of list */
				old = (char *)plisthdr->ptail;     /* Existing element is tail */
			}
			else {
				old = (char *)old_elem;          /* Existing element was passed */
			}
			/* Get DLINKs of existing and new elements */
			dlinkold = (MIST_DLINK_TYPE_PTR) (old + plisthdr->offset);
			if ( dlinkold->pListHdr != plisthdr ) {      /* Make sure element points to listhdr */
		
				#if defined(LIST_DEBUG) && LIST_DEBUG
					plisthdr->InUse = FALSE;
				#endif
		
				rv = MIST_UNABLE;
		
			} else {
			
				new = (char *)new_elem;             /* Get DLINK of new element */
				dlinknew = (MIST_DLINK_TYPE_PTR) (new + plisthdr->offset);
			
				dlinknew->pListHdr = plisthdr;           /* Point back to list header */
			
				plisthdr->count++;                   /* Increment element count */
			
				/* Set up the new linkages */
				if ( old_elem == NULL ) {
					/* Append new element to tail of list */
			
					dlinkold->pNext = new_elem;      /* Set new links */
					dlinknew->pNext = NULL;
					dlinknew->pPrev = plisthdr->ptail;
			
					plisthdr->ptail = new_elem;        /* New tail */
				}
				else if ( old_elem == plisthdr->phead ) {
					/* Insert new element at head of list */
			
					dlinkold->pPrev = new_elem;      /* Set new links */
					dlinknew->pNext = old_elem;
					dlinknew->pPrev = NULL;
			
					plisthdr->phead = new_elem;        /* Update list head */
				}
				else {
					/* Insert new element before old element */
			
					prev = (char *)dlinkold->pPrev;  /* Get DLINK of previous element */
					dlinkprev = (MIST_DLINK_TYPE_PTR)(prev + plisthdr->offset);
			
					dlinkold->pPrev = new_elem;      /* Set new links */
					dlinkprev->pNext = new_elem;
					dlinknew->pNext = old_elem;
					dlinknew->pPrev = (void *)prev;
				}
			
			
				#if defined(LIST_DEBUG) && LIST_DEBUG
					plisthdr->InUse = FALSE;
				#endif
			
				rv = MIST_SUCCESS;
			
			
			}
		
		}
	}

	return( rv );

} /* End ListInsert */

/*--------------------------------------------------------------------------
  Name:           ListInsertHead()

  Description:    This routine inserts an element at the head of a list.

  Parameters:     listhdr           pointer to the list header structure.
				  new_elem          pointer to element to add

  Return Values:  None
 -------------------------------------------------------------------------*/
MIST_RET_CODE_TYPE nm_ListInsertHead (MIST_LIST_HDR_TYPE_PTR listhdr, void *new_elem) {

MIST_RET_CODE_TYPE	rv;

	//need to suspend all here to protect the head

	rv = nm_ListInsert (listhdr, new_elem, listhdr->phead);

	return( rv );

} /* End ListInsertHead */

/*--------------------------------------------------------------------------
  Name:           ListInsertTail()

  Description:    This routine inserts an element at the tail of a list.

  Parameters:     listhdr           pointer to the list header structure.
				  new_elem          pointer to element to add

  Return Values:  None
 -------------------------------------------------------------------------*/
MIST_RET_CODE_TYPE nm_ListInsertTail (MIST_LIST_HDR_TYPE_PTR listhdr, void *new_elem) {

MIST_RET_CODE_TYPE  rv;

	//need to suspend all here to protect the head

	rv = nm_ListInsert( listhdr, new_elem, NULL );

	return( rv );
}


/**** List Removal Functions ****/

/*--------------------------------------------------------------------------
  Name:           ListRemove()

  Description:    This routine removes an element from a list.

  Parameters:     listhdr           pointer to the list header structure.
				  element           pointer to element to remove.

  Return Values:  SUCCESS           Element was removed.
				  INVALID_PARAMETER Element was not on the list.
 -------------------------------------------------------------------------*/
#if defined(LIST_DEBUG) && LIST_DEBUG

MIST_RET_CODE_TYPE nm_ListRemove_debug( MIST_LIST_HDR_TYPE_PTR listhdr, void *element, char *file, int line ) {

#else    
	
MIST_RET_CODE_TYPE nm_ListRemove( MIST_LIST_HDR_TYPE_PTR listhdr, void *element ) {
	
#endif
	
	char *temp;

	MIST_DLINK_TYPE_PTR dlink, dlinkprev, dlinknext;

	MIST_RET_CODE_TYPE rv;

	if ( !listhdr || !element ) {    /* Safety check */

		//Alert_Message_va( "List Remove NULL : %s, %d", RemovePathFromFileName(file), line );
		
		rv = MIST_INVALID_PARAMETER;

	} else {

		#if defined(LIST_DEBUG) && LIST_DEBUG

			if ( listhdr->InUse ) {
		
				//Alert_Message_va( "List Remove RE-ENTERED : %s, %d", RemovePathFromFileName(file), line );
		
				FREEZE_and_RESTART_APP;
			}
		
			listhdr->InUse = TRUE;

		#endif
	
	
		/* Get DLINK of element to remove */
		temp = (char *)element;
		dlink = (MIST_DLINK_TYPE_PTR)( temp + listhdr->offset);
	
		if ( dlink->pListHdr != listhdr ) {         /* Verify element is on list specified */
	
			#if defined(LIST_DEBUG) && LIST_DEBUG

				listhdr->InUse = FALSE;
		
				//Alert_Message_va( "Not ON List : %s, %d", RemovePathFromFileName(file), line );

			#endif

			rv = MIST_UNABLE;

		} else {
		
			dlink->pListHdr = NULL;                 /* Take off list */
		
		
			/* Adjust existing links accordingly */
			if ( dlink->pNext ) {
				temp = (char *)dlink->pNext;     /* Get DLINK of next element */
				dlinknext = (MIST_DLINK_TYPE_PTR)(temp + listhdr->offset);
		
				dlinknext->pPrev = dlink->pPrev; /* Adjust next element's backward link */
			}
			else {
				/* Element was at the tail of the list */
				listhdr->ptail = dlink->pPrev;
			}
		
			if ( dlink->pPrev ) {
				temp = (char *)dlink->pPrev;     /* Get DLINK of previous element */
				dlinkprev = (MIST_DLINK_TYPE_PTR)(temp + listhdr->offset);
		
				dlinkprev->pNext = dlink->pNext; /* Adjust prev element's forward link */
			}
			else {
				/* Element was at the head of the list */
				listhdr->phead = dlink->pNext;
			}
		
			dlink->pNext = dlink->pPrev = NULL; /* No longer linked */
			dlink->pListHdr = NULL;
			listhdr->count--;                   /* One less element on list */
		
			#if defined(LIST_DEBUG) && LIST_DEBUG

				listhdr->InUse = FALSE;

			#endif

			rv = MIST_SUCCESS;

		}

	}

	return( rv );
}

/*--------------------------------------------------------------------------
  Name:           ListRemoveHead()

  Description:    This routine REMOVES and returns the element at the head
				  of a list.

  Parameters:     listhdr           pointer to the list header structure.

  Return Values:  pointer to head element if not an empty list.
				  NULL if list was empty.
 -------------------------------------------------------------------------*/
#if defined(LIST_DEBUG) && LIST_DEBUG

void *nm_ListRemoveHead_debug( MIST_LIST_HDR_TYPE_PTR listhdr, char *file, int line )

#else

void *nm_ListRemoveHead( MIST_LIST_HDR_TYPE_PTR listhdr )

#endif
{
	void	*rv;

	if( !listhdr )
	{
		// Safety check
		//Alert_Message_va( "ListRemoveHead : NULL list ptr : %s, %d", RemovePathFromFileName(file), line );
		
		rv = NULL;

	}
	else
	{
		rv = listhdr->phead;
	
		if( rv )
		{
			#if defined(LIST_DEBUG) && LIST_DEBUG
	
				if ( nm_ListRemove_debug( listhdr, rv, file, line ) != MIST_SUCCESS )
				{
					//Alert_Message_va( "ListRemoveHead : FAILED : %s, %d", RemovePathFromFileName(file), line );
					
					// Return a FAILED indication so we don't keep iterating on a potentially broken list.
					rv = NULL;
				}
	
			#else
	
				nm_ListRemove( listhdr, rv );
			
			#endif
	
		}
	}

	return( rv );
}

/*--------------------------------------------------------------------------
  Name:           ListRemoveTail()

  Description:    This routine REMOVES and returns the element at the tail
				  of a list.

  Parameters:     listhdr           pointer to the list header structure.

  Return Values:  pointer to tail element if not an empty list.
				  NULL if list was empty.
 -------------------------------------------------------------------------*/
#if defined(LIST_DEBUG) && LIST_DEBUG

void *nm_ListRemoveTail_debug( MIST_LIST_HDR_TYPE_PTR listhdr, char *file, int line ) {

#else

void *ListRemoveTail( MIST_LIST_HDR_TYPE_PTR listhdr ) {

#endif
	
	void	*rv;

	if ( listhdr == NULL ) {                 /* Safety check */
		
		//Alert_Message_va( "ListRemoveTail : NULL List Ptr : %s, %d", RemovePathFromFileName(file), line );
		
		rv = NULL;

	} else {

		rv = listhdr->ptail;
	
		if ( rv ) {
	
			#if defined(LIST_DEBUG) && LIST_DEBUG
	
				if ( nm_ListRemove_debug( listhdr, rv, file, line ) != MIST_SUCCESS )  {
					
					//Alert_Message_va( "ListRemoveTail : FAILED : %s, %d", RemovePathFromFileName(file), line );
					
				}
	
			#else
	
				ListRemove( listhdr, rv );
	
			#endif
	
		}

	}

	return( rv );
}

/**** List Traversal Functions ****/

/*--------------------------------------------------------------------------
  Name:           ListGetFirst()

  Description:    This routine returns the element at the head
				  of a list.  It does NOT remove it from the list.

  Parameters:     listhdr           pointer to the list header structure.

  Return Values:  pointer to head element if not an empty list.
				  NULL if list was empty.
 -------------------------------------------------------------------------*/
void *nm_ListGetFirst (MIST_LIST_HDR_TYPE_PTR listhdr) {

void	*rv;

	if ( !listhdr ) {  /* Safety check */

		rv = NULL;

	} else {

		rv = listhdr->phead;

	}

	return( rv );

} /* End ListGetFirst */

/*--------------------------------------------------------------------------
  Name:           ListGetLast()

  Description:    This routine returns the element at the tail
				  of a list.  It does NOT remove it from the list.

  Parameters:     listhdr           pointer to the list header structure.

  Return Values:  pointer to tail element if not an empty list.
				  NULL if list was empty.
 -------------------------------------------------------------------------*/
void *nm_ListGetLast (MIST_LIST_HDR_TYPE_PTR listhdr) {

void	*rv;

	if ( !listhdr ) {  /* Safety check */

		rv = NULL;

	} else {

		rv = listhdr->ptail;

	}

	return( rv );

} /* End ListGetLast */

/*--------------------------------------------------------------------------
  Name:           ListGetNext()

  Description:    This routine returns the next element in a list.
				  It does NOT remove it from the list.

  Parameters:     listhdr           pointer to the list header structure.
				  element           pointer to existing element in list

  Return Values:  pointer to next element in list.
 -------------------------------------------------------------------------*/
void *nm_ListGetNext( MIST_LIST_HDR_TYPE_PTR listhdr, void *element ) {

char				*temp;
MIST_DLINK_TYPE_PTR	dlink;
void 				*rv;

	if ( !listhdr || !element ) {          /* Safety check */

		rv = NULL;

	} else {

		/* Get DLINK of current element */
		temp = (char *)element;
		dlink = (MIST_DLINK_TYPE_PTR)(temp + listhdr->offset);
	
		if ( dlink->pListHdr != listhdr ) {  /* Verify element is on list specified */
	
			rv = NULL;

		} else {
		
			rv = dlink->pNext;      /* Return pointer to next element */

		}

	}

	return( rv );              /* Return pointer to next element */

} /* End ListGetNext */

/*--------------------------------------------------------------------------
  Name:           ListGetPrev()

  Description:    This routine returns the previous element in a list.
				  It does NOT remove it from the list.

  Parameters:     listhdr           pointer to the list header structure.
				  element           pointer to existing element in list

  Return Values:  pointer to previous element in list.
 -------------------------------------------------------------------------*/
void *nm_ListGetPrev( MIST_LIST_HDR_TYPE_PTR listhdr, void *element )
{
	char				*temp;

	MIST_DLINK_TYPE_PTR	dlink;

	void 				*rv;

	if( !listhdr || !element )  // Safety check
	{
		rv = NULL;
	}
	else
	{
		/* Get DLINK of current element */
		temp = (char *)element;

		dlink = (MIST_DLINK_TYPE_PTR)(temp + listhdr->offset);
	
		if( dlink->pListHdr != listhdr )  /* Verify element is on list specified */
		{
			rv = NULL;
		}
		else
		{
			rv = dlink->pPrev;     /* Return pointer to previous element */
		}
	}

	return( rv );              /* Return pointer to previous element */

}

/*--------------------------------------------------------------------------
  Name:           OnList()

  Description:    This routine returns TRUE if the element pointed to by
				  element is on the list pointed to by listhdr.

  Parameters:     listhdr           pointer to the list header structure.
				  element           pointer to element

  Return Values:  BOOL
 -------------------------------------------------------------------------*/
_Bool nm_OnList( const MIST_LIST_HDR_TYPE_PTR listhdr, const void *const element )
{
	char					*temp;

	MIST_DLINK_TYPE_PTR		dlink;

	_Bool					rv;

	if( (!listhdr) || (!element) )  // Safety check
	{
		rv = (false);
	}
	else
	{
		// Get DLINK of current element
		temp = (char *)element;
		
		dlink = (MIST_DLINK_TYPE_PTR)(temp + listhdr->offset);
	
		rv = (dlink->pListHdr == listhdr);  // Return TRUE if on the list, otherwise return FALSE.
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

