/*  file = mem_partioning.h  03.05.2003  rmd  */
/* ---------------------------------- */

#ifndef _INC_MEM_PARTIONING_H
#define _INC_MEM_PARTIONING_H


/* ---------------------------------- */
/* ---------------------------------- */


/* ---------------------------------- */
/* ---------------------------------- */

// Max. number of memory partitions ... MUST be >= 2
// keep outside of the decision to play fixed block partitions or not cause this define
// is used in mem.c to define the statistical arrays regarding allocations


#define OS_MEM_MAX_PART	5

typedef struct {

	unsigned long	block_size;

	unsigned long	block_count;
	
} PARTITION_DEFINITION;


extern const PARTITION_DEFINITION partitions[ OS_MEM_MAX_PART ];


/* ---------------------------------- */
/* ---------------------------------- */


#define OS_MEM_NO_ERR			0000
#define OS_MEM_NO_FREE_BLKS		0110
#define OS_MEM_INVALID_BLKS		0111
#define OS_MEM_INVALID_SIZE		0112
#define OS_MEM_FULL				0113


/* ---------------------------------- */
/* ---------------------------------- */

typedef unsigned char  INT8U;                    /* Unsigned  8 bit quantity                           */
typedef signed   char  INT8S;                    /* Signed    8 bit quantity                           */
typedef unsigned short INT16U;                   /* Unsigned 16 bit quantity                           */
typedef signed   short INT16S;                   /* Signed   16 bit quantity                           */
typedef unsigned int   INT32U;                   /* Unsigned 32 bit quantity                           */
typedef signed   int   INT32S;                   /* Signed   32 bit quantity                           */


/* ---------------------------------- */
/* ---------------------------------- */

/*
*********************************************************************************************************
*                                     MEMORY PARTITION DATA STRUCTURES
*********************************************************************************************************
*/

typedef struct {                       /* MEMORY CONTROL BLOCK                                         */
    void   *OSMemAddr;                 /* Pointer to beginning of memory partition                     */
    void   *OSMemFreeList;             /* Pointer to the beginning of the free list of memory blocks   */
    INT32U  OSMemBlkSize;              /* Size (in bytes) of each block of memory                      */
    INT32U  OSMemNBlks;                /* Total number of blocks in this partition                     */
    INT32U  OSMemNFree;                /* Number of memory blocks remaining in this partition          */
} OS_MEM;

extern OS_MEM OSMemTbl[ OS_MEM_MAX_PART ];	/* Storage for memory partition manager             */


typedef struct {
    void   *OSAddr;                    /* Pointer to the beginning address of the memory partition     */
    void   *OSFreeList;                /* Pointer to the beginning of the free list of memory blocks   */
    INT32U  OSBlkSize;                 /* Size (in bytes) of each memory block                         */
    INT32U  OSNBlks;                   /* Total number of blocks in the partition                      */
    INT32U  OSNFree;                   /* Number of memory blocks free                                 */
    INT32U  OSNUsed;                   /* Number of memory blocks used                                 */
} OS_MEM_DATA;





/*
*********************************************************************************************************
*                                           MEMORY MANAGEMENT
*********************************************************************************************************
*/


void *OSMemGet(OS_MEM *pmem, INT8U *err);

INT8U OSMemPut( unsigned short pindex, void *pblk );

void init_mem_partitioning( void );


/* ---------------------------------- */
/* ---------------------------------- */


#endif


/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */

