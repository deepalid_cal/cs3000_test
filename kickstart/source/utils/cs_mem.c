/* +++Date last modified: 05-Jul-1997 */
/*
**  This is a copyrighted work which is functionally identical to work
**  originally published in Micro Cornucopia magazine (issue #52, March-April,
**  1990) and is freely licensed by the author, Walter Bright, for any use.
*/

/*_ mem.c   Fri Jan 26 1990   Modified by: Walter Bright */
/* Memory management package                    */


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#include "ks_common.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

	// 2009.03.13 RMD
	// FOR NOW WE'LL LEAVE THIS OUT...BESIDES I THINK THERE ARE OTHER WAYS TO
	// CHECK THE STACK. BESIDES IN THE FREERTOS ENVIRONMENT THERE ARE MULTIPLE
	// STACKS
	
	// The stack overflow test array...if this array gets stepped on by the
	// stack we'll know about it by testing for that
	
	//#pragma ghs section bss=".s_check"
	
	//char stack_test_space[ 16 ];
	
	//#pragma ghs section

/* ----------------------------------- */
/* ----------------------------------- */


#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
#if defined(MEM_SHOW_ALLOCS_AND_FREES) && MEM_SHOW_ALLOCS_AND_FREES

static BOOL record_mem_action = FALSE;

#endif
#endif


unsigned long mem_inited = 0;	// != 0 if initialized

unsigned long mem_count;		// # of allocs that haven't been free'd


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// PART OF FOR MEM_DEBUG 

#define BEFOREVAL 0x12345678  /* value to detect underrun   */
#define AFTERVAL  0x87654321  /* value to detect overrun    */


unsigned long mem_maxalloc;     /* max # of bytes allocated         */
unsigned long mem_numalloc;     /* current # of bytes allocated           */


// variables for examining what the allocation by size distribution looks like
//
unsigned short mem_current_allocations_of[ OS_MEM_MAX_PART ];
unsigned short mem_max_allocations_of[ OS_MEM_MAX_PART ];


// PART OF FOR MEM_DEBUG 

/* The following should be selected to give maximum probability that    */
/* pointers loaded with these values will cause an obvious crash. On    */
/* Unix machines, a large value will cause a segment fault.       */
/* MALLOCVAL is the value to set malloc'd data to.                */

#define MALLOCVAL   0xDD
#define BADVAL      0xEE


// PART OF FOR MEM_DEBUG 

// this variable is so we can get at the start of the linked list of allocations
// compiler intializes with NULL pointers but we will also explicitly do so in init_mem
//
MEM_DEBUG_STRUCT mem_alloclist;


// Convert from a void *to a mem_debug struct.

#define mem_ptrtodl(p)  ((MEM_DEBUG_STRUCT*) ((char *)p - MEM_SIZE_OF_DEBUG_HEADER))

// Convert from a mem_debug struct to a mem_ptr.

#define mem_dltoptr(dl) ((void *) &((dl)->data[0]))


static unsigned short in_mem_malloc;  // a static to check for re-entrancy


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void init_mem_debug( void ) {

unsigned short i;

    if ( mem_inited == 0 ) {

		// as a precaution ... enter critical section ... this function only called once early during
		// startup so shouldn't hurt a thing
		//
		calsense_mem_ENTER_CRITICAL();

		mem_count = 0;

		// the debug stuff
		mem_numalloc = 0;
		mem_maxalloc = 0;
		mem_alloclist.next = NULL;
		
		// 08.05.05 rmd FIX corrected bug here ... was 20 not OS_MEM_MAX_PART
		for ( i=0; i<OS_MEM_MAX_PART; i++ )  {
			
			mem_current_allocations_of[ i ] = 0;
			
			mem_max_allocations_of[ i ] = 0;

		}
		// end the debug stuff


		// don't want to do this more than once - but at least once
		//
		//for ( i=0; i<16; i++ ) {
		//
		//	stack_test_space[ i ] = i;
		//	
		//}

        mem_inited++;

		calsense_mem_EXIT_CRITICAL();

	} else {
    	
		// CANNOT make any alert lines as the call to create the ALERT line wants to use the alerts pile MUTEX which
		// has not been created YET because this is part of initializing the memory that the MUTEX uses.
		//
		//Alert_Error( "calling init_mem > once", FALSE );

		while( TRUE )
		{
			
		}

	}

}

/* ---------------------------------------------------------- */
void *mem_calloc_debug( size_t n, char *fil, unsigned short lin ) {

MEM_DEBUG_STRUCT *dl;

unsigned long alloc_size;

unsigned short i;

char str_128[ 128 ];

unsigned char err;

unsigned long ul;

void *rv;


	calsense_mem_ENTER_CRITICAL();

    // if its not an even number of bytes need to up the count by one
    // this is to avoid the address error we would get when we write the
    // AFTER_VAL - if we do it directly... using a memcpy would avoid this
	// problem and is surely the beat way to handle this. We would also 
	// get an error on the read but we never get that far.
	while ( ( n % 4) != 0 ) n++;

	alloc_size = MEM_SIZE_OF_DEBUG_HEADER + n + sizeof(AFTERVAL);
	
	dl = NULL; 	// establish the no memory condition
	
	// based on the size, we know which partition to get the block from
	// however we want to ripple up to the next level if that partition is full
	//
	for ( i=0; i<OS_MEM_MAX_PART; i++ ) {
	
		if ( partitions[ i ].block_size >= alloc_size ) {
		
			dl = (MEM_DEBUG_STRUCT*) OSMemGet( &OSMemTbl[ i ], &err );
			
			if ( err == OS_MEM_NO_FREE_BLKS )
			{
				snprintf( str_128, sizeof(str_128), "Out of memory (%d) : %s %d", i, fil, lin );
				Alert_Message( str_128 );
			}
			else
			{
				// we have to guard against a partition running out of blocks...if that
				// were to happen the code is written such that we would actually try to
				// find a block in the next block sized partition...if it does we need to
				// change the alloc_size to reflect this as we assign alloc_size to the
				// dl->nbytes which is used to determine which partition to return the block
				// to and also we use alloc_size when tracking the mem_current_allocations_of.
				//
				// its okay to just update alloc_size all the time
				//
				alloc_size = partitions[ i ].block_size;  // the ACTUAL alloc size we got  
				
				// update the number of allocations by size
				//
				mem_current_allocations_of[ i ] ++;
						
				if ( mem_current_allocations_of[ i ] > mem_max_allocations_of[ i ] ) {
				
					mem_max_allocations_of[ i ] = mem_current_allocations_of[ i ];
				
				}
				
				// we got our assignment ...get out of the loop
				//
				break;
			} 
		
		}
		
	}

    
	
	if ( dl == NULL ) {

        // mem_exception();  we replace this with our own

		if ( alloc_size > partitions[ OS_MEM_MAX_PART - 1 ].block_size )
		{
			snprintf( str_128, sizeof(str_128), "Giant Allocation: %ld bytes %s %d", alloc_size, fil, lin );
			Alert_Message( str_128 );
		}
		else
		{
			snprintf( str_128, sizeof(str_128), "No memory: %ld used in %ld allocs %s %d", mem_numalloc, mem_count, fil, lin );
			Alert_Message( str_128 );
		}
		

		// the following code will never get exectured cause of the restart in the Alert_error
		// lines - but leave the code in case someone changes TRUE to FALSE not realizing
        //
		rv = NULL;

    } else {
	
	
		// some of the following assignments would generate an ADDRESS ERROR in an external 32-bit
		// bus environment if the block sizes in the partition were not a multiple of the
		// external bus size...i suppose
		//
		dl->file = fil;
		
		dl->line = lin;
	
		dl->nbytes = alloc_size;
		
		dl->beforeval = BEFOREVAL;
	
	
		
		// memcpy is one way to deal with odd number of bytes requested
		// but i choose to round up the requested amount to an even number of bytes instead...rmd
		//
		ul = AFTERVAL;
		memcpy( &dl->data[ alloc_size - MEM_SIZE_OF_DEBUG_HEADER - sizeof(AFTERVAL) ], &ul, sizeof( ul ) );
		
	
		// Add dl to start of allocation list
		dl->next = mem_alloclist.next;
		dl->prev = &mem_alloclist;
	
		mem_alloclist.next = dl;
	
		//if ( dl->next != NULL ) (MEM_DEBUG_STRUCT*)(((MEM_DEBUG_STRUCT*)dl->next)->prev) = dl;
		if ( dl->next != NULL ) ((MEM_DEBUG_STRUCT*)dl->next)->prev = dl;
	
	
		mem_count++;
	
		mem_numalloc += dl->nbytes;
	
		if ( mem_numalloc > mem_maxalloc ) mem_maxalloc = mem_numalloc;
	
		rv = mem_dltoptr(dl);

		// remove this all together ... just for debug
		#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
		#if defined(MEM_SHOW_ALLOCS_AND_FREES) && MEM_SHOW_ALLOCS_AND_FREES
			if( record_mem_action == TRUE )
			{
				snprintf( str_128, sizeof(str_128), "malloc: %ld bytes, %s %d", alloc_size, fil, lin );
				Alert_Message( str_128 );
			}	
		#endif
		#endif

    }

	calsense_mem_EXIT_CRITICAL();

	return( rv );

}

/* ---------------------------------------------------------- */
void *mem_malloc_debug( size_t n, char *fil, unsigned short lin ) {

void	*rv;

char str_128[ 128 ];

	calsense_mem_ENTER_CRITICAL();

    // check for re-entrancy
    //
    if ( in_mem_malloc ) {
	
		// do not use Alert_Error cause we want to send the passed in file name and line number to the
		// alert error...if we used Alert_Error we would never see who was calling when the error occured
		snprintf( str_128, sizeof(str_128), "ALREADY IN MEM_MALLOC: %s %d", fil, lin );
		Alert_Message( str_128 );

		// the following code won't get executed cause the alert lines cause a restart ... but don't remove it

        rv = NULL;

    } else {
	
		in_mem_malloc = 1;

        rv = mem_calloc_debug( n, fil, lin );
	
		if ( rv != NULL ) {
			
			memset( rv, MALLOCVAL, n );
	
		}
	
        in_mem_malloc = 0;
	}

	calsense_mem_EXIT_CRITICAL();

    return( rv );

}

/* ---------------------------------------------------------- */
void mem_free_debug( void *ptr, char *fil, unsigned short lin ) {

MEM_DEBUG_STRUCT *dl;

unsigned short i;

unsigned long after_val;

unsigned char err;

char str_64[ 64 ];

#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
#if defined(MEM_SHOW_ALLOCS_AND_FREES) && MEM_SHOW_ALLOCS_AND_FREES

char	str_32[ 32 ];

#endif
#endif


	calsense_mem_ENTER_CRITICAL();

    if ( ptr == NULL ) {

		#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
		    Alert_error_debug( "MEM_FREE: freeing null ptr", TRUE, fil, lin );
		#else
		    Alert_error_normal( "MEM_FREE: freeing null ptr", TRUE );
		#endif

    }

    if ( mem_count <= 0 ) {

		#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
		    Alert_error_debug( "MEM_FREE : freeing with no count", TRUE, fil, lin );
		#else
		    Alert_error_normal( "MEM_FREE : freeing with no count", TRUE );
		#endif

    }

    dl = mem_ptrtodl( ptr );

    if ( dl->beforeval != BEFOREVAL ) {

		#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
		    Alert_error_debug( "MEM_FREE : pointer under run", TRUE, fil, lin );
		#else
		    Alert_error_normal( "MEM_FREE : pointer under run", TRUE );
		#endif

    }

	memcpy( &after_val, &dl->data[ dl->nbytes - MEM_SIZE_OF_DEBUG_HEADER - sizeof(AFTERVAL) ], sizeof( unsigned long ) );
	
    if ( after_val != AFTERVAL ) {
	
		#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
		
			Alert_error_debug( "MEM_FREE : pointer over run", TRUE, fil, lin );
			
		#else
		
			Alert_error_normal( "MEM_FREE : pointer over run", TRUE );
			
		#endif
		
    }
	
    if ( dl->nbytes > mem_numalloc ) {

		#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
	        Alert_error_debug( "MEM_FREE : no more to release", TRUE, fil, lin );
		#else
	        Alert_error_normal( "MEM_FREE : no more to release", TRUE );
		#endif

    }

	//
	// AFTER RELEASE VIA OSMemPut the data in the dl stucture is no longer guaranteed
	// to be intact...we stick a pointer in there...therfore these operations must
	// be performed before returning the block to the pool of available blocks
	//
	
	// TODO : remove this all together ... just for debug
	#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
	#if defined(MEM_SHOW_ALLOCS_AND_FREES) && MEM_SHOW_ALLOCS_AND_FREES
		if( record_mem_action == TRUE )
		{
			sprintf( str_32, "mem_free: %ld", dl->nbytes );
			Alert_error_debug( str_32, FALSE, fil, lin );
		}
	#endif
	#endif


    // Remove dl from linked list
	//
    if ( dl->prev )	 {
    	
		((MEM_DEBUG_STRUCT*)dl->prev)->next = dl->next;

    }

    if ( dl->next ) {
    	
        ((MEM_DEBUG_STRUCT*)dl->next)->prev = dl->prev;

    }

	// update the number of allocations by size
	//
	for ( i=0; i<OS_MEM_MAX_PART; i++ ) {
	
		if ( dl->nbytes <= partitions[ i ].block_size ) {
		
			mem_current_allocations_of[ i ] --;
			
			break;
			
		}
		
	}

    mem_numalloc -= dl->nbytes;
	
    mem_count--;

    // Stomp on the freed storage to help detect references after the storage was freed.
	// This is very tricky to do when in combination with the fixed memory block partition
	// scheme...remember the freed memory is now part of a linked list of =available blocks...
	// if we stomp on the pointer in the beginning of the block we'll blow up
	//
	// Conclusion is we can stomp on it but only the user data area (plus the AFTERVAL area)
	//
	memset( &dl->data, BADVAL, dl->nbytes - MEM_SIZE_OF_DEBUG_HEADER );
	
	//
	// END OF OPERATIONS THAT COUNT ON dl to be intact
	//
	
	// NOW ACTUALLY RETURN THE BLOCK TO THE POOL OF FREE BLOCKS OR FREE IT

	// based on the size, we know which partition to return the block to
	// this assumes this is where this block came from
	//
	for ( i=0; i<OS_MEM_MAX_PART; i++ ) {
	
		if ( dl->nbytes <= partitions[ i ].block_size ) {
		
			err = OSMemPut( i, (void *)dl );
			
			if ( err == OS_MEM_NO_ERR ) {
			
				break;

			} else {
				
				sprintf( str_64, "cannot return block of size %ld", dl->nbytes );
			
				#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
			        Alert_error_debug( str_64, TRUE, fil, lin );
				#else
			        Alert_error_normal( str_64, TRUE );
				#endif

			}
			
		}
		
	}



	calsense_mem_EXIT_CRITICAL();

}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

