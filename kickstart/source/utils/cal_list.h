// FILE = CAL_LIST.H

#ifndef _INC_CAL_LIST_H
#define _INC_CAL_LIST_H

/******************* Includes *****************/



/************** List Structures ***************/

/**** List Header Structure ****/

typedef struct
{
	void				*phead;		// Pointer to head of list

	void				*ptail;		// Pointer to tail of list

	UNS_32				count;		// Count of elements in list

	UNS_32				offset;		// Offset of the DLINK structure in each element.
	
	#if defined(LIST_DEBUG) && LIST_DEBUG
		BOOLEAN			InUse;		// Flag to check for re-entrancy.
	#endif
	
} MIST_LIST_HDR_TYPE, *MIST_LIST_HDR_TYPE_PTR;


/**** Data Link Structure ****/
typedef struct
{
	void				*pPrev;			// Pointer to previous data element

	void				*pNext;			// Pointer to next data element

	MIST_LIST_HDR_TYPE	*pListHdr;		// Pointer to list header the element is on

} MIST_DLINK_TYPE, *MIST_DLINK_TYPE_PTR;

/**** List Functions ****/

void nm_ListInit( MIST_LIST_HDR_TYPE_PTR plisthdr, int offset );

MIST_RET_CODE_TYPE nm_ListInsert( MIST_LIST_HDR_TYPE_PTR plisthdr, void *new_elem, void *old_elem );

MIST_RET_CODE_TYPE nm_ListInsertHead( MIST_LIST_HDR_TYPE_PTR plisthdr, void *new_elem );

MIST_RET_CODE_TYPE nm_ListInsertTail( MIST_LIST_HDR_TYPE_PTR plisthdr, void *new_elem );





#if defined(LIST_DEBUG) && LIST_DEBUG

// Define macros to replace call with debug call in application file.

#define nm_ListRemove(p,e)     nm_ListRemove_debug( (p), (e), __FILE__, __LINE__ )

#define nm_ListRemoveHead(p)   nm_ListRemoveHead_debug( (p), __FILE__, __LINE__ )

#define nm_ListRemoveTail(p)   nm_ListRemoveTail_debug( (p), __FILE__, __LINE__ )


MIST_RET_CODE_TYPE nm_ListRemove_debug( MIST_LIST_HDR_TYPE_PTR plisthdr, void *element, char *file, int line );

void *nm_ListRemoveHead_debug( MIST_LIST_HDR_TYPE_PTR plisthdr, char *file, int line );

void *nm_ListRemoveTail_debug( MIST_LIST_HDR_TYPE_PTR plisthdr, char *file, int line );


#else



MIST_RET_CODE_TYPE nm_ListRemove( MIST_LIST_HDR_TYPE_PTR plisthdr, void *element );

void *nm_ListRemoveHead( MIST_LIST_HDR_TYPE_PTR plisthdr );

void *nm_ListRemoveTail( MIST_LIST_HDR_TYPE_PTR plisthdr );


#endif  // of if making LIST_DEBUG version





void *nm_ListGetFirst( MIST_LIST_HDR_TYPE_PTR plisthdr );

void *nm_ListGetLast( MIST_LIST_HDR_TYPE_PTR plisthdr );

void *nm_ListGetNext( MIST_LIST_HDR_TYPE_PTR plisthdr, void *element );

void *nm_ListGetPrev( MIST_LIST_HDR_TYPE_PTR plisthdr, void *element );


_Bool nm_OnList( const MIST_LIST_HDR_TYPE_PTR listhdr, const void *const element );



#define nm_ListGetCount(listhdr) ((listhdr)->count)


#endif /* _INC_CAL_LIST_H */

