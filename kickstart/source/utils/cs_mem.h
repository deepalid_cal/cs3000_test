/* +++Date last modified: 05-Jul-1997 */
/*
**  This is a copyrighted work which is functionally identical to work
**  originally published in Micro Cornucopia magazine (issue #52, March-April,
**  1990) and is freely licensed by the author, Walter Bright, for any use.
*/

/*_ mem.h   Fri May 26 1989   Modified by: Walter Bright */
/* Copyright 1986-1988 by Northwest Software    */
/* All Rights Reserved                    */
/* Written by Walter Bright               */




/* ---------------------------------- */
/* ---------------------------------- */

/*  file = mem.h  03.05.2003  rmd       */
/*  and again updated by rmd 03.16.2009 */

/* ---------------------------------- */

#ifndef _INC_MEM_H
#define _INC_MEM_H

/* ---------------------------------- */
/* ---------------------------------- */

/* ---------------------------------- */
/* ---------------------------------- */

/* ---------------------------------- */
/* ---------------------------------- */
/*
 * Memory management routines.
 *
 * Compiling:
 *
 *    #define MEM_DEBUG 1 when compiling to enable extended debugging
 *    features.
 *
 * Features always enabled:
 *
 *    o mem_init() is called at startup, and mem_term() at
 *      close, which checks to see that the number of alloc's is
 *      the same as the number of free's.
 *    o Behavior on out-of-memory conditions can be controlled
 *      via mem_setexception().
 *
 * Extended debugging features:
 *
 *    o Enabled by #define MEM_DEBUG 1 when compiling.
 *    o Check values are inserted before and after the alloc'ed data
 *      to detect pointer underruns and overruns.
 *    o Free'd pointers are checked against alloc'ed pointers.
 *    o Free'd storage is cleared to smoke out references to free'd data.
 *    o Realloc'd pointers are always changed, and the previous storage
 *      is cleared, to detect erroneous dependencies on the previous
 *      pointer.
 *    o The routine mem_checkptr() is provided to check an alloc'ed
 *      pointer.
 */




#if defined(ALERT_ERROR_FILE_NAMES) && ALERT_ERROR_FILE_NAMES
#if defined(MEM_SHOW_ALLOCS_AND_FREES) && MEM_SHOW_ALLOCS_AND_FREES

extern BOOLEAN	record_mem_action = FALSE;

#endif
#endif

/* Create a list of all alloc'ed pointers, retaining info about where   */
/* each alloc came from. This is a real memory and speed hog, but who   */
/* cares when you've got obscure pointer bugs.                    */

typedef struct  {

	void *next;					// next in list			
	void *prev;					// previous value in list
	char *file;					// filename of where allocated
	unsigned long line;			// line number of where allocated
	unsigned long nbytes;		// size of the allocation
	unsigned long beforeval;	// detect underrun of data

	// items before this point are considered the header

	char data[ 4 ];				// the data actually allocated

} MEM_DEBUG_STRUCT;


#define MEM_SIZE_OF_DEBUG_HEADER (sizeof( MEM_DEBUG_STRUCT ) - 4)



// variables for examining what the allocation by size distribution looks like
//
extern unsigned long mem_count;			// # of allocs that haven't been free'd
extern unsigned long mem_maxalloc;     /* max # of bytes allocated		*/
extern unsigned long mem_numalloc;     /* current # of bytes allocated	*/

extern unsigned short mem_current_allocations_of[ OS_MEM_MAX_PART ];
extern unsigned short mem_max_allocations_of[ OS_MEM_MAX_PART ];






#define init_mem()			init_mem_debug()

#define mem_malloc(u)		mem_malloc_debug((u),__FILE__,__LINE__)
#define mem_free(p)			mem_free_debug((p),__FILE__,__LINE__)


void init_mem_debug( void );

void *mem_malloc_debug( size_t n, char *fil, unsigned short lin );
void mem_free_debug( void *ptr, char *fil, unsigned short lin );

//void mem_test_for_stack_overrun( void );

#endif /* MEM_H */
