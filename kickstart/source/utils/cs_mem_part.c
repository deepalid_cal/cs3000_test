/*  file = mem_partioning.c 03/04/2003   rmd  */
/* ---------------------------------- */
/* ---------------------------------- */


#include "ks_common.h"

#include "crc.h"

#include "packet_definitions.h"




/* ---------------------------------- */
/* ---------------------------------- */

// define structure of the partitions...used when setting up the partitions and throughout
// the process of using and returning a block of memory...currently uses 343,392 bytes of heap
// the nice thing we know is that is the absolute bound...this progam uses that much heap...period.
//
// 362,336 BYTES total bytes of heap represented here
// 
// As for 608.e, we're using about 350K outside of the heap. This leaves us with well over 250K available
//
// MUST have a minimum of 2 blocks per partition else won't create
//
// WARNING: All block SIZES must be an even multiple of 4 bytes for the Atmel UC3A/B architecture!! FOR ALL ARCHITECTURES
//          I WOULD CONSIDER THIS NORMAL PRACTICE.
//
// NOTE: if you want a request to use a particular partition remember the overhead of the memory scheme
//		 is 28 bytes ... so add 32 to the partition block size and you'll see the expected results
//
const PARTITION_DEFINITION partitions[ OS_MEM_MAX_PART ] =
{
	//   SIZE   QUANTITY  
	
	// During the kickstart memory tests there are alot of fragments of strings handed to the
	// serial driver. With 64 blocks we ran out of memory. So I set to 128.
	{	 144,	64 },
	

	{	 288,	8 },


	// This is for the FreeRTOS stacks.And this for the bigger stacked freeRTOS tasks one might
	// create ... this needs to be watched carefully to craft the partition sizes and number of
	// blocks to fit the application
	{	2112,	6 },	
	
	// For the received message packets. +32 to support the mem partioning overhead.
	{	SYSTEM_WIDE_LARGEST_AMBLE_TO_AMBLE_PACKET_SIZE + 32, 4 },  
	
	// For flash storage operations.
	{	4160,	4 },	


	// NOTE: YOU MUST HAVE AT LEAST 2 BLKS PER PARTITION. THERE IS A CATCH IN THE INIT CODE FOR THIS. I DID NOT TRACK DOWN THE REASON WHY.

};

/* ---------------------------------- */
/* ---------------------------------- */
// CALSENSE CODE





/* ---------------------------------- */
/* ---------------------------------- */

/*
*********************************************************************************************************
*                                                uC/OS-II
*                                          The Real-Time Kernel
*                                            MEMORY MANAGEMENT
*
*                        (c) Copyright 1992-1998, Jean J. Labrosse, Plantation, FL
*                                           All Rights Reserved
*
*                                                  V2.00
*
* File : OS_MEM.C
* By   : Jean J. Labrosse
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                         LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/

OS_MEM OSMemTbl[ OS_MEM_MAX_PART ];		/* Storage for memory partition manager             */

/*
*********************************************************************************************************
*                                        CREATE A MEMORY PARTITION
*
* Description : Create a fixed-sized memory partition that will be managed by uC/OS-II.
*
* Arguments   : addr     is the starting address of the memory partition
*
*               nblks    is the number of memory blocks to create from the partition.
*
*               blksize  is the size (in bytes) of each block in the memory partition.
*
*               err      is a pointer to a variable containing an error message which will be set by
*                        this function to either:
*             
*                        OS_NO_ERR            if the memory partition has been created correctly.
*                        OS_MEM_INVALID_PART  no free partitions available
*                        OS_MEM_INVALID_BLKS  user specified an invalid number of blocks (must be >= 2)
*                        OS_MEM_INVALID_SIZE  user specified an invalid block size 
*                                             (must be greater than the size of a pointer)
* Returns    : != (OS_MEM *)0  is the partition was created 
*              == (OS_MEM *)0  if the partition was not created because of invalid arguments or, no
*                              free partition is available.
*********************************************************************************************************
*/

OS_MEM *OSMemCreate( unsigned short pindex, void *addr, INT32U nblks, INT32U blksize, INT8U *err ) {

OS_MEM  *pmem;
INT8U   *pblk;
void   **plink;
unsigned long i;

    if (nblks < 2) {                                  /* Must have at least 2 blocks per partition      */
        *err = OS_MEM_INVALID_BLKS;
        return ((OS_MEM *)0);
    }
    if (blksize < sizeof(void *)) {                   /* Must contain space for at least a pointer      */
        *err = OS_MEM_INVALID_SIZE;
        return ((OS_MEM *)0);
    }
    
	calsense_mem_ENTER_CRITICAL();

	plink = (void **)addr;                            /* Create linked list of free memory blocks      */
    pblk  = (INT8U *)addr + blksize;
    for (i = 0; i < (nblks - 1); i++) {
        *plink = (void *)pblk;
        plink  = (void **)pblk;
        pblk   = pblk + blksize;
    }
    *plink = (void *)0;                               /* Last memory block points to NULL              */
    


	pmem = &OSMemTbl[ pindex ];

	pmem->OSMemAddr     = addr;                       /* Store start address of memory partition       */
    pmem->OSMemFreeList = addr;                       /* Initialize pointer to pool of free blocks     */
    pmem->OSMemNFree    = nblks;                      /* Store number of free blocks in MCB            */
    pmem->OSMemNBlks    = nblks;
    pmem->OSMemBlkSize  = blksize;      /* Store block size of each memory blocks        */
    

	calsense_mem_EXIT_CRITICAL();

	*err = OS_MEM_NO_ERR;
	
    return ( pmem ); 

}

/*
*********************************************************************************************************
*                                          GET A MEMORY BLOCK
*
* Description : Get a memory block from a partition
*
* Arguments   : pmem    is a pointer to the memory partition control block
*
*               err     is a pointer to a variable containing an error message which will be set by this
*                       function to either:
*
*                       OS_NO_ERR           if the memory partition has been created correctly.
*                       OS_MEM_NO_FREE_BLKS if there are no more free memory blocks to allocate to caller
*
* Returns     : A pointer to a memory block if no error is detected
*               A pointer to NULL if an error is detected
*********************************************************************************************************
*/

void *OSMemGet( OS_MEM *pmem, INT8U *err ) {

void *rv;
    
	calsense_mem_ENTER_CRITICAL();

    if (pmem->OSMemNFree > 0) {                     /* See if there are any free memory blocks       */

        rv = pmem->OSMemFreeList;					/* Yes, point to next free memory block          */

        pmem->OSMemFreeList = *(void **)rv;			/*      Adjust pointer to new free list          */

        pmem->OSMemNFree--;                         /*      One less memory block in this partition  */

        *err = OS_MEM_NO_ERR;                           /*      No error                                 */

    } else {

	    rv = NULL;									/*      Return NULL pointer to caller            */
		
		*err = OS_MEM_NO_FREE_BLKS;                 /* No,  Notify caller of empty memory partition  */


		/* for debug
		DisplayLEDOn();
		ClearDisplay();
		for ( i=0; i<OS_MEM_MAX_PART; i++ ) {
			
			DFP( SLO_4+(i*40), " %6d, %3d, %3d ", OSMemTbl[ i ].OSMemBlkSize, OSMemTbl[ i ].OSMemNBlks, OSMemTbl[ i ].OSMemNFree );

		}
		
	    init_kbd( FALSE );
		WaitTillKey();
		*/
		
    }

	calsense_mem_EXIT_CRITICAL();

	return( rv );
	
}

/*
*********************************************************************************************************
*                                         RELEASE A MEMORY BLOCK
*
* Description : Returns a memory block to a partition
*
* Arguments   : pmem    is a pointer to the memory partition control block
*
*               pblk    is a pointer to the memory block being released.
*
* Returns     : OS_NO_ERR         if the memory block was inserted into the partition
*               OS_MEM_FULL       if you are returning a memory block to an already FULL memory partition
*                                 (You freed more blocks than you allocated!)
*********************************************************************************************************
*/

INT8U OSMemPut( unsigned short pindex, void *pblk ) {

INT8U rv;
OS_MEM *pmem;

//MEM_DEBUG_STRUCT *pdl;
//static unsigned long lcounter;
    
	calsense_mem_ENTER_CRITICAL();

//	lcounter++;


/*
	pdl = (MEM_DEBUG_STRUCT*)pblk;

	DFP( SLO_1, "been here %d times", lcounter );
	
	DFP( SLO_2, "index passed=%d, memory size passed=%d", pindex, pdl->nbytes );
	
	for ( i=0; i<OS_MEM_MAX_PART; i++ ) {
		
		DFP( SLO_4+(i*40), " %6d, %3d, %3d ", OSMemTbl[ i ].OSMemBlkSize, OSMemTbl[ i ].OSMemNBlks, OSMemTbl[ i ].OSMemNFree );

	}
	
	WaitTillKey();
*/



	pmem = &OSMemTbl[ pindex ];


    if (pmem->OSMemNFree >= pmem->OSMemNBlks) {  	 /* Make sure all blocks not already returned          */


/*
		mem_show_memstats();
		
		
		DFP( SLO_1, "block size is %d    block size is %d  ", pmem->OSMemBlkSize, pdl->nbytes );
		DFP( SLO_2, " total blocks %d         ", pmem->OSMemNBlks );
		DFP( SLO_3, "  free blocks %d         ", pmem->OSMemNFree );
				
		WaitTillKey();
			
*/

		rv = OS_MEM_FULL;
		       
    } else {
    	
	    *(void **)pblk = pmem->OSMemFreeList;   /* Insert released block into free block list         */
	    
		pmem->OSMemFreeList = pblk;
	    
		pmem->OSMemNFree++;                          /* One more memory block in this partition            */

    	rv = OS_MEM_NO_ERR;	        					 /* Notify caller that memory block was released       */
		
	}

	calsense_mem_EXIT_CRITICAL();

	return( rv );
	
}

/*
*********************************************************************************************************
*                                    INITIALIZE MEMORY PARTITION MANAGER
*
* Description : This function is called by uC/OS-II to initialize the memory partition manager.  Your
*               application MUST NOT call this function.
*
* Arguments   : none
*
* Returns     : none
*********************************************************************************************************
*/

void init_mem_partitioning( void ) {
	
unsigned short i;

unsigned char err;

unsigned char *ucp;

OS_MEM  *pmem;


	#if ( OS_MEM_MAX_PART < 2 )
	
		do not comment out these comments
		compiler message...no code goes here...this prevents compiling with number of partitions < 2
		
		the number of partitions must be >= 2 ... not sure why though??? (historically not an issue as we have many more partitions)
		
	#endif
	
	
	// Calsense added code...for our application we want to go ahead and grab the partitions from the heap
	// using malloc...then using "OSMemCreate()" we enable each of the fixed block partitions for use...our
	// scheme is to have "mem_malloc" end up using a block from an appropiate sized partition...the nice
	// thing is that when we go to free the block the mem_malloc_debug "knows" from which partition the
	// block came from and can therefore return it correctly...this seems to say that in order to fix the
	// fragmentation problem (what the partioning is all about) we must employ some form of the mem_debug
	// header in the assigned block to make it easy to know where it came from...the most direct would be to
	// keep the pointer to OSMemTbl memory control block the block was from...that pointer could of course
	// get destroyed in a memory underrun problem...but MEM_DEBUG will tell us about those
	//
	//
    
	// don't really know why we turn off interrupts here but this function should only be called ONCE early
	// during the startup process so doing so as a precaution shouldn't hurt
	//
	calsense_mem_ENTER_CRITICAL();  

	for ( i=0; i<OS_MEM_MAX_PART; i++ ) {
	
		ucp = malloc( partitions[ i ].block_count * partitions[ i ].block_size );
		
		pmem = OSMemCreate( i, ucp, partitions[ i ].block_count, partitions[ i ].block_size, &err );
		
		if ( (ucp == NULL) || (pmem == NULL) ) {
		
			// CANNOT make any alert lines as the call to create the ALERT line wants to use the alerts pile MUTEX which
			// by design uses memory from the partition pool which we have not yet made...that's what we are here doing!
			//
			//Alert_Error( "partition not created", TRUE );

			for(;;);

        }
		
	    
		//for debug show_mem_parts( i );
		
		/*
		init_lcd();
	    //SetContrastToMidScale();
		DisplayLEDOn();
		
		DFP( SLO_2, "partition %d", i );
		DFP( SLO_4, "block size  = %5d", pmem->OSMemBlkSize );
		DFP( SLO_5, "block count = %5d", pmem->OSMemNBlks );
		DFP( SLO_6, "available   = %5d", pmem->OSMemNFree );
		
		for ( www=0; www<0x30000; www++ ) {
			
			BangWDT();
			
		}
		*/
		
    }
		
	calsense_mem_EXIT_CRITICAL();

	// now all the partitions have been created and are available for use by mem_malloc_debug and mem_free_debug
	
}

/*
*********************************************************************************************************
*                                          QUERY MEMORY PARTITION
*
* Description : This function is used to determine the number of free memory blocks and the number of 
*               used memory blocks from a memory partition.
*
* Arguments   : pmem    is a pointer to the memory partition control block
*
*               pdata   is a pointer to a structure that will contain information about the memory
*                       partition.
*
* Returns     : OS_NO_ERR         Always returns no error.
*********************************************************************************************************
*/
/*

not used so comment out


INT8U OSMemQuery( OS_MEM *pmem, OS_MEM_DATA *pdata ) {

unsigned short SavedISRLevel;

    SavedISRLevel = ISRLevel;
    if ( ISRLevel < INT_MEM_BLACKOUT ) SetISRLevel( INT_MEM_BLACKOUT );

    pdata->OSAddr     = pmem->OSMemAddr;
    pdata->OSFreeList = pmem->OSMemFreeList;
    pdata->OSBlkSize  = pmem->OSMemBlkSize;
    pdata->OSNBlks    = pmem->OSMemNBlks;
    pdata->OSNFree    = pmem->OSMemNFree;

	SetISRLevel( SavedISRLevel );  // back to where it was

    pdata->OSNUsed    = pdata->OSNBlks - pdata->OSNFree;

    return( OS_NO_ERR );                         
}

*/

/*
void show_mem_parts( unsigned short where ) {

unsigned short j;
	
    init_lcd();
	init_lcdbias_hardware();  // sets the bias to mid_scale
	DisplayLEDOn();

	ClearDisplay();

	DFP( SLO_1, "%d     ", where );

	for ( j=0; j<OS_MEM_MAX_PART; j++ ) {
		
		DFP( SLO_4+(j*40), "%d: %6d, %3d, %3d ", j, 
												 OSMemTbl[ j ].OSMemBlkSize, 
												 OSMemTbl[ j ].OSMemNBlks, 
												 OSMemTbl[ j ].OSMemNFree );

	}
	
    init_kbd( FALSE );
	WaitTillKey();
	
	for ( j=0; j<20; j++ ) WaitOneMS();  // effective debounce

}		
*/		

/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */
/* ---------------------------------- */
