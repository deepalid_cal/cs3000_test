/* file = main.c                             01.11.2011  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"ks_common.h"

#include	"led_utils.h"

#include	"lpc32xx_intc_driver.h"

#include	"lpc32xx_wdt.h"

#include	"lpc32xx_timer_driver.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"lpc32xx_gpio_driver.h"

#include	"misc_config.h"

#include	"common_funcs.h"

#include	"s1l_memtests.h"

#include	"memtest.h"

#include	"s1l_sys_inf.h"

#include	"app_startup.h"

#include	"flash_storage.h"

#include	"spi_flash_driver.h"

#include	"lpc_arm922t_cp15_driver.h"




/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
void disable_dcache( void )
{
	void dcache_flush( void );  // defined in LPC3200_Startup.s
	dcache_flush();

	cp15_set_dcache( 0 );
}

/*-----------------------------------------------------------*/
void enable_dcache( void )
{
	cp15_invalidate_cache();

	cp15_set_dcache( 1 );
}

/*-----------------------------------------------------------*/
// 2012.03.01 rmd : After much trial and error, it seems this is the best way to force the
// code to jump to the new program. Be aware if you have the caches and mmu enabled you
// should flush the data cache, disable both the i and d caches, and disable the mmu. Also
// note prior to this call I think you should disable interrupts.

#define jump_cs3000()													\
{																		\
	__asm volatile (													\
																		\
    "mov r2, #0x80000000									\n\t"		\
    "mov lr, pc												\n\t"		\
    "bx r2													\n\t"		\
	);																	\
}

/* ---------------------------------------------------------- */
static void powerfail_isr( void )
{
	// ----------
	// 10/19/2012 rmd : Because we have decided it is more robust to set this interrupt up as a
	// level triggered interrupt and the hardware cannot be cleared of the LOW condition that
	// caused this interrupt, we MUST DISABLE the INTERRUPT otherwise we would spend all of our
	// time in the interrupt.
	SIC2_ER &= ~( SIC2_ER_GPI_01_MASK );

	// ----------
	// 10/18/2012 rmd : See the CS3000 project for the full implication of setting this (true).
	// But basically it boils down to we are going to back out of and prevent further flash work
	// (if any is under way). We are also going to let the WDT expire. Which should occur in as
	// little as 1 second but no more than 2.
	restart_info.SHUTDOWN_impending = (true);
}

/* ---------------------------------------------------------- */
static void init_powerfail_isr( void )
{
	xDisable_ISR( GPI_01_INT );

	xSetISR_Vector( GPI_01_INT, INTERRUPT_PRIORITY_powerfail_warning, ISR_TRIGGER_LOW_LEVEL, powerfail_isr, NULL );

	xEnable_ISR( GPI_01_INT );
}

/*-----------------------------------------------------------*/
static void init_the_watchdog( void )
{
	// 10/15/2012 rmd : First before anything enable the clock to the WDT peripheral block.
	// Otherwise I don't think even writes to the registers will work.
	clkpwr_clk_en_dis( CLKPWR_WDOG_CLK, 1 );

	// ----------
	
	WDT_REGS_T	*wdt;
	
	wdt = ((WDT_REGS_T*)WDTIM_BASE);
	
	// ----------

	// 10/16/2012 rmd : Now initialize the WDT counter. ha

	// 10/15/2012 rmd : Going to initialize the WDT hardware. Coming out of hardware RESET is
	// the only way we're executing this code here (normally ... I suppose anything is possible
	// with an error or other anomaly).
	
	// 10/15/2012 rmd : Following reset the wdt is supposed to be disabled. HOWEVER the data
	// sheet is not clear about this. In the data sheet the attained reset value for the enable
	// bit is strangely blank. So we make a point to disable here.
	wdt->main_control = 0;
	
	// 10/18/2012 rmd : In the kickstart program we want to set the WDT expiration period long
	// enough that the main code image can be read without expiring. This takes somthing like 5
	// seconds. So set to 10 seconds to make sure.
	UNS_32	clock_rate;
	
	clock_rate = clkpwr_get_base_clock_rate( CLKPWR_PERIPH_CLK );

	wdt->match_value = ( 10 * clock_rate );
	
	// 10/16/2012 rmd : Set up to generate both the internal chip reset and an external reset.
	// Also set for the WDT to stop on match? It seems it may continue to be active AFTER a
	// match detect? Hmmm. I don't think we want that. Will we have a race between the startup
	// getting back to this point in the code and another WDT timeout? I don't think so because
	// there are other bits I am sure are not setup making the WDT non-operational post reset.
	// Even post a WDT reset. So these bits (bits 0,1, and 2 that STOP, RESET, and generate an
	// INT) must be more geared for when this peripheral block is used as a general purpose
	// timer.
	wdt->match_control = ( WDT_M_RES2 | WDT_STOP_ON_MATCH );

	// 10/15/2012 rmd : The maximum value in this register is 0xFFFF. With a 13MHz PERIPHERAL
	// CLOCK the widest reset pulse we get is 5.04ms. Note: This register must be written to
	// else the WDT block will not operate.
	wdt->reset_pulse_length = 0xFFFF;

	// 10/15/2012 rmd : Set up so that the match output is active upon a match.
	wdt->external_match_control = ( 0x20 );
	
	// 10/15/2012 rmd : Start the WDT up. Allowing the debugger to pause the count. So that when
	// the debugger takes a hold we don't generate a watchdog initiated reset.
	wdt->main_control = (WDT_PAUSE_ENABLE | WDT_COUNT_ENABLE);
	
}

/*-----------------------------------------------------------*/

#ifdef DEBUG
	#define SECONDS_WITHOUT_ACTIVITY_BEFORE_RESTART	(3000)
#else
	#define SECONDS_WITHOUT_ACTIVITY_BEFORE_RESTART	(20)
#endif	

/*-----------------------------------------------------------*/
static void tend_to_the_watchdog( void )
{
	// ----------
	
	WDT_REGS_T	*wdt;
	
	wdt = ((WDT_REGS_T*)WDTIM_BASE);
	
	// ----------

	// 10/18/2012 rmd : As long as we have haven't seen a power failure and data has been
	// arriving stick with it. Otherwise we are going to let the WDT expire.
	if( (restart_info.SHUTDOWN_impending == (false)) && (restart_info.seconds_running < (restart_info.time_stamp_when_last_packet_arrived + SECONDS_WITHOUT_ACTIVITY_BEFORE_RESTART)) )
	{
		// 10/16/2012 rmd : Restarts the count to avoid a match.
		wdt->counter_value = 0;
	}
	else
	{
		// 10/18/2012 rmd : Well see this come on for about 10 seconds before the WDT strikes!
		CS_LED_RED_ON;
	}

}

/*-----------------------------------------------------------*/
void MAIN_look_for_app_task( void *pvParameters )
{
	BOOL_32		watchdog_and_powerfail_isr_started;
	
	/*
	UNS_32	iters, hexaddr, bytes, width, tstnum;

	hexaddr = 0x80000000;	// This is EMC SDRAM.
	bytes = 0x800000;		// 8 MBytes
	width = 4;				// 32-bit wide chip
	tstnum = MTST_ALL;
	iters = 1;
	// memtst [hex address][bytes to test][1, 2, or 4 bytes][0(all) - 5][iterations]
	memory_test( hexaddr, bytes, width, tstnum, iters);
	
	hexaddr = 0x80000000;	// This is EMC SDRAM.
	bytes = 0x800000;		// 8 MBytes
	width = 2;				// 32-bit wide chip
	tstnum = MTST_ALL;
	iters = 1;
	// memtst [hex address][bytes to test][1, 2, or 4 bytes][0(all) - 5][iterations]
	memory_test( hexaddr, bytes, width, tstnum, iters);
	
	hexaddr = 0x80000000;	// This is EMC SDRAM.
	bytes = 0x800000;		// 8 MBytes
	width = 1;				// 32-bit wide chip
	tstnum = MTST_ALL;
	iters = 1;
	// memtst [hex address][bytes to test][1, 2, or 4 bytes][0(all) - 5][iterations]
	memory_test( hexaddr, bytes, width, tstnum, iters);
	*/
	

	// ----------------------------------------
	// Initialize these. Normally done in the WDT task. But we don't have one in the kickstart
	// app. And these are embedded throughout the FLASH code so this is the easiest way to deal
	// with them.
	restart_info.SHUTDOWN_impending = (false);

	restart_info.we_were_reading_spi_flash_data = (false);

	restart_info.we_were_writing_spi_flash_data = (false);

	restart_info.seconds_running = 0;

	restart_info.time_stamp_when_last_packet_arrived = 0;
	
	// ----------------------------------------

	// 4/22/2014 rmd : Not yet started so flag accordingly.
	watchdog_and_powerfail_isr_started = (false);

	// ----------------------------------------
	// Startup the flash file system so we can begin reading files and establish structures and
	// list intializations. This code is here because it must be inside a task and executed
	// after the OS starts as it releys upon MUTEXES and delays.
	init_FLASH_DRIVE_SSP_channel( FLASH_INDEX_0_BOOTCODE_AND_EXE );  // hardware: serial channel
	init_FLASH_DRIVE_SSP_channel( FLASH_INDEX_1_GENERAL_STORAGE );  // hardware: serial channel

	init_FLASH_DRIVE_file_system( FLASH_INDEX_0_BOOTCODE_AND_EXE, FALSE );  // See if file system is valid. Do not force an initialization.
	init_FLASH_DRIVE_file_system( FLASH_INDEX_1_GENERAL_STORAGE, FALSE );  // See if file system is valid. Do not force an initialization.

	// ----------------------------------------

	// 10/18/2012 rmd : The action of the SERVICE pin is interesting. Both the jumper and an
	// impending pwr failure cause the SERVICE input to go low. On a normal unattended power up
	// the service pin is high. This causes us to check the file system and attempt to load the
	// CS3000 app to run. In the following I'll describe all the startup cases:
	/*
	    Case 1: Normal startup. Jumper is out. Power is good. Service input is high. Kick start
	    looks for the CS3000 app. If file not found we proceed with the normal kickstart
	    functionality. And that has incorporated into it a watchdog functionality that says if
	    we haven't seen a packet in the last 30 seconds we restart. Also if we subsequently see
	    bad power we restart.
		
	    Case 2: On a what was to be a normal startup the power flucuates. However never goes
	    away completely. If the impending pwr fail line out of the maxim chip tripped we could
	    end up running the THIS kick start code with NO WAY OUT except a power failure. And this
	    could happen in the field!!! The packet watching functionality provides the escape.
	*/
	

	// 10/18/2012 rmd : If the jumper isn't in, then the SERVICE pin input will be high and
	// we'll go to the file system and try to find the main app. If not found we proceed to the
	// regular look for packet kickstart functionality.
	if( SERVICE_N_is_HIGH )
	{
		// 4/22/2014 rmd : A normal unattended to start. Suppose somehow the file is corrupt and we
		// attempt to run it. Should have a watchdog! The jumper is out so we can fire up both the
		// power fail isr and the watchdog.
		watchdog_and_powerfail_isr_started = (true);

		// 4/22/2014 rmd : Set to 10 second period to allow enough time for the file read and code
		// startup.
		init_the_watchdog();
			
		init_powerfail_isr();

		// ----------

		// Okay find the CS3000_APP_FILENAME file.
		if( FLASH_STORAGE_find_and_read_latest_version_file( FLASH_INDEX_0_BOOTCODE_AND_EXE, CS3000_APP_FILENAME, (void*)0x80000000 ) == (true) )
		{
			// Okay we found and wrote a file to the location. Go run it!

			// 2012.03.01 rmd : I think this is needed. We do not want an interrupt to occur right when
			// we reach the 0x80000000 code. The vector table will send us into this code in the on-chip
			// SRAM. And that would be a problem I'd bet.
			portDISABLE_INTERRUPTS();
	
			// 2012.03.01 rmd : Because we have included the NO_CACHE_ENABLE and NO_ICACHE_ENABLE
			// pre-processor definitions we DO NOT have to got through this chache and mmu disable
			// sequence of 5 function calls.
			// #include	"lpc_arm922t_cp15_driver.h"
			//
			// void dcache_flush( void );  // prototype for function defined in LPC3200_Startup.s
			//
			// dcache_flush(); cp15_invalidate_cache(); cp15_set_dcache( 0 ); cp15_set_icache( 0 ); cp15_set_mmu( 0 );
	
			jump_cs3000();
	
			/* alternate method to jump to a program
			PFV execa = (PFV)RECEIVED_PACKET_STORAGE_START;
			execa();
			*/
		}
	}

	// ----------------------------------------

	// 4/22/2014 rmd : We discovered a VERY obscure yet somewhat deadly bug. If code
	// distribution packets are arriving (at that oh so fast rate that they do) during the boot
	// process of the kickstart program AND the kickstart sees a valid code file per the above
	// code, a condition develops where the unit sits unbooted in a corrupt state. One way I
	// discovered to help prevent this condition is to not create the THREE serial port related
	// tasks till after we've decided we need them. Afterall if the kickstart sees a valid code
	// file in the flash file system it will boot that and never needs to be concerned with the
	// serial ports. So that is what you see going on here.
	//
	// The other way I suspect we could bail out of the locked up condition would be to start
	// the WDT ealier. I think I'll do that too!

	Task_Table[ 0 ].bCreateTask = (true);

	Task_Table[ 1 ].bCreateTask = (true);

	Task_Table[ 2 ].bCreateTask = (true);

	Task_Table[ 3 ].bCreateTask = (false);
	
	process_task_table();
	
	// ----------------------------------------

	// 2/5/2014 rmd : So we when debugging know we made it past the jumper test.
	Alert_Message( "Remove Jumper.\r\n" );


	// 2/11/2015 rmd : Moving from 4.1 to 5.0 was when we redefined the flash file DIR_ENTRY
	// structure. To accomodate our main app file REVISION scheme. The dir_entry location of the
	// file revision was changed.
	//
	// 4/29/2016 rmd : Moving from 5.0 to 6.0. Major low level spi flash driver re-do to include
	// device identification and subsequent ability to handle both the 25VF and 26VF Microchip
	// flash parts.
	term_dat_out( "\r\r\nCalsense Kickstart : Ver 6.3 rmd\r\n" );

	term_dat_out( "\r\nYou may remove the jumper now.\r\n" );

	term_dat_out( "\r\nCS3000>" );

	// ----------------------------------------

	_Bool	is_on;	

	is_on = (false);
	
	for( ;; )
	{
		// ----------
		
		// 10/18/2012 rmd : I'm alive.
		if( is_on )
		{
			CS_LED_GREEN_OFF;	
		}
		else
		{
			CS_LED_GREEN_ON;	
		}
		
		is_on = !is_on;
		
		// ----------
		
		restart_info.seconds_running += 1;
		
		// 10/18/2012 rmd : We wait 4 seconds so the guy has time to remove the service jumper
		// before we fire up the wdt and enable the pwr fail isr. Remember the pwr fail isr can be
		// tripped by the SERVICE jumper itself (a hardware quirk)! Only tend to the WDT after it
		// has been initialized!
		if( (restart_info.seconds_running > 4) || watchdog_and_powerfail_isr_started )
		{
			tend_to_the_watchdog();
		}
		else
		if( restart_info.seconds_running == 4 )
		{
			if( !watchdog_and_powerfail_isr_started )
			{
				init_the_watchdog();
				
				init_powerfail_isr();
			}
		}
		
		// ----------

		vTaskDelay( MS_to_TICKS( 1000 ) );

		// ----------

	}
	
}


#ifndef NO_DEBUG_PRINTF

xSemaphoreHandle	debugio_MUTEX;

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
int main( void )
{

	/*
	volatile UNS_32	clk_rate;

	clk_rate = clkpwr_get_base_clock_rate( CLKPWR_ARM_CLK );

	clk_rate = clkpwr_get_base_clock_rate( CLKPWR_HCLK );

	clk_rate = clkpwr_get_base_clock_rate( CLKPWR_PERIPH_CLK );
	*/
	

	#if( USE_CALSENSE_MEMORY_MANAGER == 1 )
		// Create the memory partitions
		//
		// 03.27.2009 rmd ... The memory MUST be initialized BEFORE any freeRTOS calls because we have
		// directed the freeRTOS heap calls to use our heap management system. There is at least one mutex
		// made early on (the one for the SPI Dataflash parts).
		//
		// There are NO alert lines attempted during this intialization! The pile has not yet been checked and initialized.
		// We have protection against such if tried though.
		init_mem_partitioning();

		init_mem();  // simply prepares memory use stats variables
	#endif

	// ----------------------------------------


	// 2012.02.28 rmd : All sys_up does is setup and start a 10ms timer. Which isn't used for
	// anything. So comment out.
	// sys_up();

	
	#ifndef NO_DEBUG_PRINTF

		debugio_MUTEX = xSemaphoreCreateMutex();

	#endif


	before_scheduler_runs_CreateSystemTasks();

	Alert_Message( "Tasks Created.\r\n" );

	// Start the OpenRTOS scheduler
	vTaskStartScheduler( );


	return( 0 );
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
void vConfigureTimerForRunTimeStats( void )
{
	TIMER_CNTR_REGS_T	*ltimer;

	ltimer = FreeRTOS_RUN_TIME_STATS_TIMER;


	clkpwr_clk_en_dis( FreeRTOS_RUN_TIME_STATS_TIMER_CLK_EN, 1 );

	// Setup default timer state as standard timer mode, timer disabled and all match and counters disabled.
	ltimer->tcr = 0;  // timer control register - disabled
	ltimer->ctcr = TIMER_CNTR_SET_MODE(TIMER_CNTR_CTCR_TIMER_MODE);  // timer mode
	ltimer->ccr = 0;  // capture control register fully disabled
	ltimer->emr = (TIMER_CNTR_EMR_EMC_SET(0, TIMER_CNTR_EMR_NOTHING) | TIMER_CNTR_EMR_EMC_SET(1, TIMER_CNTR_EMR_NOTHING) | TIMER_CNTR_EMR_EMC_SET(2, TIMER_CNTR_EMR_NOTHING) | TIMER_CNTR_EMR_EMC_SET(3, TIMER_CNTR_EMR_NOTHING));

	// Clear pending interrupts and reset counts.
	ltimer->tc = 0;  // the count register itself
	ltimer->pc = 0;  // prescale counter register

	ltimer->mr[ 0 ] = 0;  // Zero all the match registers.
	ltimer->mr[ 1 ] = 0;
	ltimer->mr[ 2 ] = 0;
	ltimer->mr[ 3 ] = 0;

	ltimer->pr = 650;		// Set to count the main counter at 20kHz.
	ltimer->mcr = 0x00;		// Just count.

	ltimer->ir = 0xFF;  // Clear ALL pending interrupts.

	ltimer->tcr = TIMER_CNTR_TCR_EN;  // Enable the timer/counter.
}

unsigned long return_run_timer_counter_value( void )
{
	TIMER_CNTR_REGS_T	*ltimer;
	
	ltimer = FreeRTOS_RUN_TIME_STATS_TIMER;
	
	return( ltimer->tc );
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
// When NDEBUG is defined as in the release code a call to assert in the user code will not result in an eventual call to this
// function. The original assert is compiled as a null. When NDEBUG is not defined assert is defined and if the value asserted
// is FALSE this function is invoked. You can make it do what you want. Some of the provided code from other parties has assert
// statements embedded throughout. And therefore during debug you should always implement this function. Assert is your friend!
#ifndef NDEBUG
void __assert( const char *__expression, const char *__filename, int __line )
{
	char	s[ 256 ];
	
	snprintf( s,  sizeof(s), "ASSERT: %s, %s, line=%d", __expression,  __filename,  __line );

	/*
	if( GuiLib_okay_to_use == TRUE )
	{
		// ASSERT should write directly to the screen. Then attempt the alert message. Usually when __assert is executed we have ahd
		// a serious error. And neeed to get some message of some kind to the test personnel. REMEMBER for release code there is no
		// assert. It compiles to nothing! So this is only effective during development. And of course we are violating the whole
		// FDTO rules. That is we must queue any GUI request for the display task to process. But hey this is a last ditch attempt.
		GuiLib_DrawStr( 0, 10,							// at beginning of top line : for ANSI7 y=10 is the correct value

						GuiFont_ANSI7,					// the font number, 0 is the default font

						0,								// only ENGLISH so far

						s,								// the string to display

						GuiLib_ALIGN_LEFT,				// GuiLib_ALIGN_LEFT or GuiLib_ALIGN_CENTER or GuiLib_ALIGN_RIGHT

						GuiLib_PS_ON,					// proportional writing: GuiLib_PS_NOCHANGE, GuiLib_PS_OFF, GuiLib_PS_ON, GuiLib_PS_NUM

						GuiLib_TRANSPARENT_OFF,         // background box: GuiLib_TRANSPARENT_OFF, GuiLib_TRANSPARENT_ON

						GuiLib_UNDERLINE_OFF,			// underlining: GuiLib_UNDERLINE_OFF, GuiLib_UNDERLINE_ON

						320,							// GuiConst_INT16S BackBoxSizeX,   if this is >0 the transparent setting is ignored
						10,								// GuiConst_INT16S BackBoxSizeY1,
						4,								// GuiConst_INT16S BackBoxSizeY2,
						0,								// GuiConst_INT8U BackBorderPixels,   do a bitwise OR GuiLib_BBP_LEFT | GuiLib_BBP_RIGHT | GuiLib_BBP_TOP | GuiLib_BBP_BOTTOM
						COLOR_BLACK,					// GuiConst_INTCOLOR ForeColor,
						COLOR_WHITE						// GuiConst_INTCOLOR BackColor
					  );
	}
	
	// And attempt an alert line! If the pile is valid.
	if( aps.apile_ready_to_use_uc == TRUE )
	{
		Alert_Message( s );
	}
	*/
	
	// HALT!!!!  TODO set some battery backed bits to indicate something or another.
	for( ;; );
}
#endif

/*-----------------------------------------------------------*/
/*
 * Application stack overflow hook function.
 */
void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
/* This function will get called if a task overflows its stack. */
//xTaskHandle prvTaskHandle;
//signed char *scTaskName;

    for(;;)
    {
		//prvTaskHandle = pxTask;
		//scTaskName = pcTaskName;
    }
}
/*-----------------------------------------------------------*/

/*
 * sample tick hook function that is called every tick - see macro definition
 * configUSE_TICK_HOOK
 */
//void vApplicationTickHook( void )
//{
  /* Can be used as specified in the OpenRTOS user manual.  */
//}
/*-----------------------------------------------------------*/

/*
 * sample idle hook function - see macro definition configUSE_IDLE_HOOK
 */
//void vApplicationIdleHook( void )
//{
  /* Can be used as specified in the OpenRTOS user manual.  */
//}

/*-----------------------------------------------------------*/
void vApplicationMallocFailedHook( void )
{
	for(;;);
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
#ifndef NO_DEBUG_PRINTF

	void Alert_Message( char *pmessage )
	{
		debug_printf( pmessage );
		debug_printf( "\r\n" );
	}
	
#else

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

