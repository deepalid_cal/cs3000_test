/* file = app_startup.h                      10.09.2009  rmd  */
/* ---------------------------------------------------------- */

#ifndef INC_APP_STARTUP_H_
#define INC_APP_STARTUP_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"ks_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern xSemaphoreHandle	Flash_0_MUTEX, Flash_1_MUTEX;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Structure for a task creation table entry
typedef struct 
{
    BOOL				bCreateTask;		// TRUE if this entry is to be processed
	pdTASK_CODE			pTaskFunc;			// The task function
	const char * const	TaskName;			// The task name, maximum 16 chars. incl. NULL
	unsigned short		StackDepth;			// The depth of the stack in 32-bit words
	void 				*pParameters;		// Parameters passed to task
	unsigned int		Priority;			// Task priority
	xTaskHandle			pTaskHandle;		// ptr to handle for new task 

} TASK_ENTRY_STRUCT;
	
#define NUMBER_OF_TABLE_TASKS	(4)

extern TASK_ENTRY_STRUCT Task_Table[ NUMBER_OF_TABLE_TASKS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern void process_task_table( void );

extern void before_scheduler_runs_CreateSystemTasks( void );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*STARTUP_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

