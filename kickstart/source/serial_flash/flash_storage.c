// file = flash_storage.c                    03.28.2011  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"flash_storage.h"

#include	"df_storage_mngr.h"

#include	"app_startup.h"

#include	"spi_flash_driver.h"

#include	"lpc32xx_gpio_driver.h"

#include	"app_startup.h"

#include	"packet_definitions.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define SHOW_FILE_ACTIVITY_ALERTS


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Queue commands

#define	FLASH_STORAGE_COMMAND_write_data_to_flash_file			(22)

#define	FLASH_STORAGE_COMMAND_delete_older_versions				(33)

#define	FLASH_STORAGE_COMMAND_delete_all_files_with_this_name	(44)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
unsigned long DFLASH_STORAGE_find_latest_version_and_latest_edit_of_a_filename_NM( UNS_32 flash_index, DF_DIR_ENTRY_s *pdir_entry )
{

// The _NM means NO MUTEX activity within this function there for the caller must be responsible for the use of the DATA_FLASH mutex.
//
// This function returns the dir index and fully loads the pfull_dir_entry structure. The pdir_entry union is valid ONLY if (rv_ul != DF_DIR_ENTRY_NOT_FOUND).
// AND it is important to note that pdir_entry is NOT touched if no matches are found.

	DF_DIR_ENTRY_s			*lsearch_dir_entry;
	
	DF_DIR_ENTRY_s			*lresults_dir_entry;
	
	unsigned long			files_found_ul, dir_index_ul;
	
	unsigned long			rstatus_ul, rv_ul;
	
	//char					str_128[ 128 ];

	// THE CALLER IS RESPONSIBLE FOR TAKING AND RELEASING THE DATA FLASH MUTEX

	// NOTE: Upon entry to the function the pfull_dir_entry contains the filename. Really nothing else.
	// We snatch that and then allow the DfFindDirEntry_NM to load lresults_full_dir_entry if it finds a file
	lsearch_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	// grab the file name we are looking for
	strlcpy( (char*)&lsearch_dir_entry->Filename_c, (char*)&pdir_entry->Filename_c, sizeof(lsearch_dir_entry->Filename_c) );

	lresults_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );  // we do not need to 0 out the lresults_full_dir_entry structure ... we use the return value from DfFindDirEntry_NM to tell us if it is valid

	// set up so the comparison works
	pdir_entry->code_image_date_or_file_contents_revision = 0;  // by our rule no file is allowed this version number...so this can be viewed as no file yet found

	rv_ul = DF_DIR_ENTRY_NOT_FOUND;  // to indicate no file found

	files_found_ul = 0;  // if we only find one latest version file we are done with the hunt ... also used as a debug aid

	dir_index_ul = 0;  // start with the first directory entry

	while( TRUE )
	{
		rstatus_ul = DfFindDirEntry_NM( flash_index, lsearch_dir_entry, lresults_dir_entry, DF_COMPARISON_MASK_FILENAME, dir_index_ul );

		if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
		{
			files_found_ul += 1;  // inc how many files we have found

			// A matching directory entry was found. found_dir_entry is filled in with the complete directory entry for the item.  Capture the newest version found.
			//
			if( lresults_dir_entry->code_image_date_or_file_contents_revision >= pdir_entry->code_image_date_or_file_contents_revision )  // the >= is a debug thing to write the string showing what files we find ... only needs to be > ... but the >= doesn't hurt
			{
				*pdir_entry = *lresults_dir_entry;  // copy the latest possible full directory candidate

				rv_ul = rstatus_ul;  // and capture the directory index for this entry

				//snprintf( str_128, sizeof(str_128), "V%ld %s: found version %lx, edit %ld, %ld bytes", files_found_ul, lresults_full_dir_entry->dir.Filename_c, lresults_full_dir_entry->dir.version_ul, lresults_full_dir_entry->dir.edit_count_ul, lresults_full_dir_entry->dir.ItemSize_ul );
				//Alert_Message( str_128 );
			}

			// if we just returned the last possible directory entry then we can't go any further
			if( rstatus_ul == (ssp_define[ flash_index ].max_directory_entries - 1) )
			{
				break;  // we're all done  ... last of all possible directory entries
			}
			else
			{
				dir_index_ul = rstatus_ul + 1;  // To resume search at the next entry
			}

		}
		else
		{
			break;  // we're all done  ... no more files found
		}

	}

	// now find the latest edit_count_ul file ... but only if we found more than one file ... most of the time we will only find one and it must be the one we're after
	//
	if( files_found_ul > 1 )
	{
		// okay we should have the latest version now in pdir_entry ... so put a copy into the search_dir_entry to update it for the coming search
		lsearch_dir_entry->code_image_date_or_file_contents_revision = pdir_entry->code_image_date_or_file_contents_revision;

		// and reset the edit_count_ul value in the results as it is not necessarily the largest edit value...that's what we're finding next
		pdir_entry->edit_count_ul = 0;

		rv_ul = DF_DIR_ENTRY_NOT_FOUND;  // reset the return value to again indicate no file found

		dir_index_ul = 0;  // start AGAIN at the first directory entry

		while( TRUE )
		{
			// now use the filename and the version number as the match criteria
			rstatus_ul = DfFindDirEntry_NM( flash_index, lsearch_dir_entry, lresults_dir_entry, DF_COMPARISON_MASK_FILENAME | DF_COMPARISON_MASK_DATE_OR_REVISION, dir_index_ul );

			if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
			{
				// A matching directory entry was found. found_dir_entry is filled in with the complete directory entry for the item.  Capture the newest edit_count_ul found.
				//
				if( lresults_dir_entry->edit_count_ul >= pdir_entry->edit_count_ul )  // the >= is a debug thing to write the string showing what files we find ... only needs to be > ... but the >= doesn't hurt
				{
					*pdir_entry = *lresults_dir_entry;  // copy the latest possible full directory candidate

					rv_ul = rstatus_ul;  // and capture the directory index for this entry

					//snprintf( str_128, sizeof(str_128), "E %s: found version %lx, edit %ld, %ld bytes", lresults_full_dir_entry->dir.Filename_c, lresults_full_dir_entry->dir.version_ul, lresults_full_dir_entry->dir.edit_count_ul, lresults_full_dir_entry->dir.ItemSize_ul );
					//Alert_Message( str_128 );
				}

				// if we just returned the last possible directory entry then we can't go any further
				if( rstatus_ul == (ssp_define[ flash_index ].max_directory_entries-1) )
				{
					break;  // we're all done  ... last of all possible directory entries
				}
				else
				{
					dir_index_ul = rstatus_ul + 1;  // To resume search at the next entry
				}

			}
			else
			{
				break;  // we're all done  ... no more files found
			}

		}
	
	}

	mem_free( lsearch_dir_entry );  // don't forget to free this!

	mem_free( lresults_dir_entry );  // don't forget to free this!

	// sanity check
	if( (files_found_ul > 0) && (pdir_entry->edit_count_ul == 0) )
	{
		// Do not make such alert lines if the reason we got funny results is the impending power failure.
		if( restart_info.SHUTDOWN_impending != TRUE )
		{
			Alert_Message( "DF: condition should not exist." );
		}

	}

	return( rv_ul );
}

/* ---------------------------------------------------------- */
_Bool FLASH_STORAGE_find_and_read_latest_version_file( UNS_32 flash_index, const char *pfile_name, void *pto_where )
{
	_Bool	rv;
	
	rv = (false);
	

	DF_DIR_ENTRY_s		*lfound_dir_entry;
	
	unsigned long		rstatus_ul;
	
	lfound_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );  // MEMORY ALLOCATION
	
	strlcpy( lfound_dir_entry->Filename_c, pfile_name, sizeof(lfound_dir_entry->Filename_c) );
	
	// START of data flash operations...we don't want the data flash to change until we are done doing our operations
	DfTakeStorageMgrMutex( flash_index, portMAX_DELAY );
	
	// NO NEED to initialize anything else in the lfound_full_dir_entry for the following - only the filename
	rstatus_ul = DFLASH_STORAGE_find_latest_version_and_latest_edit_of_a_filename_NM( flash_index, lfound_dir_entry );
	
	if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
	{
		#ifdef SHOW_FILE_ACTIVITY_ALERTS
			char	str_96[ 96 ];
			snprintf( str_96, sizeof(str_96), "%s: found version %ld, edit %ld, %ld bytes", lfound_dir_entry->Filename_c, lfound_dir_entry->code_image_date_or_file_contents_revision, lfound_dir_entry->edit_count_ul, lfound_dir_entry->ItemSize_ul );
			Alert_Message( str_96 );
		#endif

		// 11/13/2012 rmd : Test if sucessful. Else we will think the file data occupies the memory
		// space the caller provided. And obviously something is wrong and the data cannot be
		// trusted.
		if( DfReadFileFromDF_NM( flash_index, lfound_dir_entry, pto_where ) == DF_RESULT_SUCCESSFUL )
		{
			// Indicate we found and read out a file.
			rv = (true);
		}
		
	}
	
	// END of data flash operations...return the mutex
	DfGiveStorageMgrMutex( flash_index );
	
	mem_free( lfound_dir_entry );  // release the memory we allocated in the beginning of the function
	
	return( rv );
}

/* ---------------------------------------------------------- */
void __find_older_files_and_delete_NM( UNS_32 flash_index, const char *pfile_name, unsigned long pversion_ul, unsigned long pedit_count_ul )
{
    // Loop through the directory entries and delete all files that do not match the submitted key fields.
	// This function manages the data flash mutex at this level. The caller of this function does not need to concern themselves with the mutex.

	DF_DIR_ENTRY_s						*lsearch_dir_entry;
	
	DF_DIR_ENTRY_s						*lresults_dir_entry;
	
	unsigned long						dir_index_ul, rstatus_ul, files_deleted_ul;
	

	lsearch_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	lresults_dir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

    // load up the search directory entry with the filename, the latest version, and the latest edit ... to be used to decide to delete or not
	strlcpy( (char*)&lsearch_dir_entry->Filename_c, (char*)pfile_name, sizeof(lsearch_dir_entry->Filename_c) );
	lsearch_dir_entry->code_image_date_or_file_contents_revision = pversion_ul;
	lsearch_dir_entry->edit_count_ul = pedit_count_ul;

	files_deleted_ul = 0;
    dir_index_ul = 0;  // start at the beginning

	while( TRUE )
	{
		rstatus_ul = DfFindDirEntry_NM( flash_index, lsearch_dir_entry, lresults_dir_entry, DF_COMPARISON_MASK_FILENAME, dir_index_ul );

		if( rstatus_ul != DF_DIR_ENTRY_NOT_FOUND )
		{
			// okay we found an entry that matches in filename only so far...test against the latest VERSION and EDIT_COUNT...if no match then delete the file
			if( DfDirEntryCompare( lsearch_dir_entry, lresults_dir_entry, DF_COMPARISON_MASK_DATE_OR_REVISION | DF_COMPARISON_MASK_EDIT_COUNT ) != 0 )
			{
				// NO MATCH...delete

				#ifdef SHOW_FILE_ACTIVITY_ALERTS
					char	str_96[ 96 ];
					snprintf( str_96, sizeof(str_96), "DELETING: Chip %d, %s version %ld, edit %ld, %ld bytes", flash_index, lresults_dir_entry->Filename_c, lresults_dir_entry->code_image_date_or_file_contents_revision, lresults_dir_entry->edit_count_ul, lresults_dir_entry->ItemSize_ul );
					Alert_Message( str_96 );
				#endif
				
				DfDelFileFromDF_NM( flash_index, rstatus_ul, lresults_dir_entry );

				files_deleted_ul += 1;
			}

			// if we just returned the last possible directory entry then we can't go any further
			if( rstatus_ul == (ssp_define[ flash_index ].max_directory_entries - 1) )
			{
				break;  // we're all done  ... last of all possible directory entries
			}
			else
			{
				dir_index_ul = rstatus_ul + 1;  // To resume search at the next entry
			}
		}
		else
		{
			break;  // we're all done  ... no more files found
		}
	}

	if( files_deleted_ul == 0 )
	{
		#ifdef SHOW_FILE_ACTIVITY_ALERTS
			char	str_96[ 96 ];
			snprintf( str_96, sizeof(str_96), "Old File Version Delete: Chip %d, %s, no old verions found.", flash_index, lsearch_dir_entry->Filename_c );
			Alert_Message( str_96 );
		#endif
	}

	mem_free( lsearch_dir_entry );

	mem_free( lresults_dir_entry );
}

/* ---------------------------------------------------------- */
void FLASH_STORAGE_write_data_to_flash_file( UNS_32 flash_index, FILE_SAVE_STRUCT *pfss )
{
	// This function manages the data flash mutex at this level. The caller of this function does not need to concern themselves with the mutex.

	// This function is called potentially the very first time a variable is stored (ie no file exists) and during routine file updates.

	DF_DIR_ENTRY_s		*ldir_entry;
	
	// ----------

	// 2/6/2014 rmd : Get memory for a directory entry. And clean it.
	ldir_entry = mem_malloc( sizeof(DF_DIR_ENTRY_s) );

	memset( ldir_entry, 0x00, sizeof(DF_DIR_ENTRY_s) );

	// ----------

	ldir_entry->code_image_date_or_file_contents_revision = convert_code_image_date_to_version_number( pfss->file_buffer_ptr, pfss->const_file_name_ptr );

	ldir_entry->edit_count_ul = convert_code_image_time_to_edit_count( pfss->file_buffer_ptr, pfss->const_file_name_ptr );

	// 2/7/2014 rmd : If either are 0 we have a problem in the conversion.
	if( (ldir_entry->code_image_date_or_file_contents_revision == 0) || (ldir_entry->edit_count_ul == 0) )
	{
		Alert_Message( "FILE: code revision conversion problem" );	
	}

	// ----------

	DfTakeStorageMgrMutex( flash_index, portMAX_DELAY );  // freeze the data flash at its current state

	// ----------

	// For the hunt for the latest version and latest edit just provide the filename.
	strlcpy( (char*)&(ldir_entry->Filename_c), pfss->const_file_name_ptr, sizeof(ldir_entry->Filename_c) );

	ldir_entry->ItemSize_ul = pfss->file_size;

	if( DfWriteFileToDF_NM( flash_index, ldir_entry, pfss->file_buffer_ptr ) == DF_DUPL_ITEM_ALREADY_EXISTS )
	{
		Alert_Message(  "FLASH: file should not already exist!" );  // this should NEVER happen as we just incremented the edit_count_ul!!!!!
	}
	else
	{
		#ifdef SHOW_FILE_ACTIVITY_ALERTS
			char	str_96[ 96 ];
			snprintf( str_96, sizeof(str_96), "WROTE: Chip %d, %s version %ld, edit %ld, %ld bytes", flash_index, ldir_entry->Filename_c, ldir_entry->code_image_date_or_file_contents_revision, ldir_entry->edit_count_ul, ldir_entry->ItemSize_ul );
			Alert_Message( str_96 );
		#endif
	}
	
	// Now start the flash cleaning function.   
	__find_older_files_and_delete_NM( flash_index, ldir_entry->Filename_c, ldir_entry->code_image_date_or_file_contents_revision, ldir_entry->edit_count_ul );

	DfGiveStorageMgrMutex( flash_index );

	mem_free( ldir_entry );  // don't free this memory till we are done with the data contained within it
}
		
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

