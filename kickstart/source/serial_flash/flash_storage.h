/* file = flash_storage.h  10.12.2009  rmd  */
/* ---------------------------------------------------------- */

#ifndef INC_FLASH_STORAGE_H_
#define INC_FLASH_STORAGE_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"ks_common.h"

#include	"df_storage_mngr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// These defines represent each of the TWO DIFFERENT flash chips in our LPC3250 design.

#define	FLASH_INDEX_0_BOOTCODE_AND_EXE	0

#define	FLASH_INDEX_1_GENERAL_STORAGE	1

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	// We provide a pointer to the CONST filename. As opposed to the queue carrying a copy of
	// the name.
	const char 		*const_file_name_ptr;

	void			*file_buffer_ptr;

	UNS_32			file_size;
	
} FILE_SAVE_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
// Function prototypes

_Bool FLASH_STORAGE_find_and_read_latest_version_file( UNS_32 flash_index, const char *pfile_name, void *pto_where );


void FLASH_STORAGE_write_data_to_flash_file( UNS_32 flash_index, FILE_SAVE_STRUCT *pfss );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /* INC_FLASH_STORAGE_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

