/*  file = spi_flash_driver.c                 02.09.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*   spi_flash_driver.c:	For the LPC32x0 on-chip SSP SPI device. Crafted for an SST25VF032B serial
 *							flash. Though may also work with Winbond W25Q32BV parts. Though this has
 *							not yet been tested. Certainly the SST AAI program instruction doesn't work
 *							with the Winbond devices.
 */


// 8/20/2014 rmd : A NOTE ON OPTIMIZATION: I found that the flash_storage FOLDER in the
// Crossworks project has optimization set to level 1. As a common setting for all builds. I
// couldn't remember why this was done. But after some poking around I see testing I did
// during the spi writes in the spi_flash_driver source that refers to the speed
// improvements attained. I think it may have been a left over that this was actually saved
// this way. But it has likely been this way a long time. And I feel it best that the flash
// file reads and writes have their timing remain consistent from DEBUG to RELEASE. So I am
// leaving the crossworks property in place.
//
// 4/25/2016 rmd : When we added the DEVICE IDENTIFICATION functionality I went back and
// re-visited this subject. I tested the code with optimization set to both level 0 and
// level 1. The level 1 results in a small time saving but about 5K of code space. Remember
// most of the time associated with a write or read is from the spi hardware itself and the
// sector erase time (20ms just for that). It seems wise to take the code size saving and
// slight efficiency improvement you get with the optimization at level 1. I have no
// interest in pushing the envelope here are trying say a level 3 setting and so have done
// NO tests with that.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<string.h>

#include	"spi_flash_driver.h"

#include	"lpc3250_chip.h"

#include	"lpc32xx_ssp_driver.h"

#include	"lpc32xx_gpio_driver.h"

#include	"df_storage_mngr.h"

#include	"df_storage_internals.h"

#include	"flash_storage.h"

#include	"gpio_setup.h"

//#include	"wdt_and_powerfail.h"

#include	"app_startup.h"

//#include	"alerts.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2012.02.23 rmd : Provide our own definition of the power fail structure as we do not
// include the standard wdt version. Easier to include this than to change the code.
RESTART_INFORMATION_STRUCT	restart_info;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//#include "l3k_boot.h"                       // boot code content

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Where we keep an image of the Master DF record during operations. It is of course read out of the flash and put here. One for each file system.
MASTER_DF_RECORD_s		flash_0_mr_buffer, flash_1_mr_buffer;

// ----------

// 4/21/2016 rmd : The following three devices use the same code. When we added the issi25cq
// part we did indeed have to change the interfacing code. But the change was minor and
// could be used for both devices with negligible performance impact to the 25vf device. We
// really don't have a 'generic device' as we will only build with the 25vf and 25cq parts.
#define		FLASH_DEV_ID_generic_device				(0)
#define		FLASH_DEV_ID_microchip_sst25vf032b		(1)
#define		FLASH_DEV_ID_issi_is25cq032				(2)

// 4/21/2016 rmd : This SST26VF part IS different. The status register is now two registers.
// And the bits having different meanings. So locking and unlocking the device is all
// together different. This is the device where we made the move to include the formal
// device ID functionality up front as part of the init function.
#define		FLASH_DEV_ID_microchip_sst26vf032b		(3)

#define		FLASH_NUM_OF_TESTED_DEVICES				(4)

// ----------

// 4/21/2016 rmd : Using the flash index as the index into this array.
static UNS_32	flash_device_id[ 2 ];
	

#define		MANUFACTURER_ID_issi		(0x7F)
#define		MANUFACTURER_ID_sst			(0xbF)
 
#define		MEMORY_TYPE_issi			(0x9d)
#define		MEMORY_TYPE_sst_25vf		(0x25)
#define		MEMORY_TYPE_sst_26vf		(0x26)

#define		MEMORY_CAPACITY_issi		(0x46)
#define		MEMORY_CAPACITY_sst_25vf	(0x4a)
#define		MEMORY_CAPACITY_sst_26vf	(0x42)


typedef struct
{
	// 4/21/2016 rmd : When you issue the JEDEC READ ID command you read 3 bytes which define
	// the Manufacturer, The Memory Type, and The Memory Capacity.
	UNS_32	manufacturer_id;

	UNS_32	memory_type;

	UNS_32	memory_capacity;

} JEDEC_ID_STRUCT;

const JEDEC_ID_STRUCT	known_JEDEC_READ_ID_results[ FLASH_NUM_OF_TESTED_DEVICES ] =
{
	[ 0 ]={	.manufacturer_id = 0,
			.memory_type = 0,
			.memory_capacity = 0
		  },

	[ FLASH_DEV_ID_microchip_sst25vf032b ] =	{	.manufacturer_id = MANUFACTURER_ID_sst,
													.memory_type = MEMORY_TYPE_sst_25vf,
													.memory_capacity = MEMORY_CAPACITY_sst_25vf
												},

	[ FLASH_DEV_ID_issi_is25cq032 ] =			{	.manufacturer_id = MANUFACTURER_ID_issi,
													.memory_type = MEMORY_TYPE_issi,
													.memory_capacity = MEMORY_CAPACITY_issi
												},

	[ FLASH_DEV_ID_microchip_sst26vf032b ] =	{	.manufacturer_id = MANUFACTURER_ID_sst,
													.memory_type = MEMORY_TYPE_sst_26vf,
													.memory_capacity = MEMORY_CAPACITY_sst_26vf
												}
};

// ----------

const SSP_BASE_STRUCT		ssp_define[ 2 ] = 
{
	[ 0 ]={	.write_protect = WP_FLASH_0_PIN,
			.chip_select_bit =  P3_STATE_GPIO(5),
		    .miso_mask = P3_INP_STATE_GPI_25_MASK,
			.ssp_base = SSP0,
//			.task_queue_ptr = &FLASH_STORAGE_task_queue_0,
			.flash_mutex_ptr = &Flash_0_MUTEX,
			.mr_struct_ptr = &flash_0_mr_buffer,
			.master_record_page = DF_MASTER_RECORD_PAGE_0,
			.first_directory_page = DF_FIRST_DIR_PAGE_0,
			.last_directory_page = DF_LAST_DIR_PAGE_0,
			.first_data_cluster = DF_FIRST_DATA_CLUSTER_0,
			.key_string = DF_FLASH_MAGIC_STR_0,
			.max_directory_entries = DF_MAX_DIR_ENTRIES_0
		  },

	[ 1 ]={	.write_protect = WP_FLASH_1_PIN,
			.chip_select_bit =  P3_STATE_GPIO(4),
			.miso_mask = P3_INP_STATE_GPI_27_MASK,
			.ssp_base = SSP1,
//			.task_queue_ptr = &FLASH_STORAGE_task_queue_1,
			.flash_mutex_ptr = &Flash_1_MUTEX,
			.mr_struct_ptr = &flash_1_mr_buffer,
			.master_record_page = DF_MASTER_RECORD_PAGE_1,
			.first_directory_page = DF_FIRST_DIR_PAGE_1,
			.last_directory_page = DF_LAST_DIR_PAGE_1,
			.first_data_cluster = DF_FIRST_DATA_CLUSTER_1,
			.key_string = DF_FLASH_MAGIC_STR_1,
			.max_directory_entries = DF_MAX_DIR_ENTRIES_1
		  }
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/22/2016 rmd : Some LPC3250 pin macros based upon OUR board design.

#define WRITE_PROTECT_LOW		(P3_OUTP_CLR = ssp_define[ flash_index ].write_protect)
#define WRITE_PROTECT_HIGH		(P3_OUTP_SET = ssp_define[ flash_index ].write_protect)

#define CHIP_SELECT_LOW		(GPIO->p3_outp_clr = ssp_define[ flash_index ].chip_select_bit)
#define CHIP_SELECT_HIGH	(GPIO->p3_outp_set = ssp_define[ flash_index ].chip_select_bit)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 4/22/2016 rmd : Some defines for the LPC3250 on-chip SPI peripheral.

#define SSP_DR	(pssp->data)
#define SSP_SR	(pssp->sr)

#define SSP_SR_RNE_MASK		(0x04)
#define	SSP_SR_BSY_MASK		(0x10)
	
#define ENABLE_TRANSMISSION		((pssp->cr1) = 0x02)
#define DISABLE_TRANSMISSION	((pssp->cr1) = 0x00)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//SPI Flash memory commands; see the datasheet for details
#define CMD_RD					(0x03)
#define CMD_HIGH_SPEED_READ		(0x0B)

#define CMD_WRDI				(0x04)
#define CMD_WREN				(0x06)
#define CMD_EWSR				(0x50)
#define CMD_WRDIS				(0x04)

#define CMD_RDSR				(0x05)

#define CMD_WRSR				(0x01)

#define CMD_4K_SECTOR_ERASE		(0x20)

#define CMD_CHER				(0xC7)

#define CMD_BYTE_PROGRAMMING	(0x02)

#define CMD_WBPR				(0x42)
#define CMD_ULBPR				(0x98)

#define CMD_JEDEC_READ_ID		(0x9F)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define DEVICE_BUSY_MASK	0x01
#define DEVICE_BUSY			0x01

#define DEVICE_WEL_MASK		0x02
#define DEVICE_WEL			0x02

// ----------

// 4/22/2016 rmd : These block protection defines are only used with the SST25VF and IS25CQ
// devices.
#define	DEVICE_BLOCK_PROTECTION_MASK	0xBC

// 1/10/2017 rmd : In the beginning, when i first wrote this flash driver for the 25VF
// devices i did not make use of the status register HARDWARE lock down feature. Upon
// revision to support the 26VF devices I added that capability. BUT to maintain backwards
// compatibility with the older code base we are not using it for the 25VF devices. We are
// about to stop shipping them anyhow and haven't had any issues not using it. But I surely
// locked down the 26VF devices. Though they do that in a different way.
#define	DEVICE_ALL_BLOCKS_UNPROTECTED_NO_LOCK_DOWN	0x00
#define	DEVICE_ALL_BLOCKS_PROTECTED_NO_LOCK_DOWN	0x1C

// 1/10/2017 rmd : For reference only. DO NOT USE. Unless you clearly understand the
// ramifications. There is no going back to old code once you set this bit. I think (the
// datasheet says the lock down bit is cleared at power up so maybe it can be worked but i'm
// not implementing in the interest of time).
#define	DEVICE_ALL_BLOCKS_UNPROTECTED_WITH_LOCK_DOWN	0x80
#define	DEVICE_ALL_BLOCKS_PROTECTED_WITH_LOCK_DOWN		0x9C

// ----------

// 1/12/2017 rmd : If you set this to a 1, you introduce a backwards incompatibility. The
// older production code may not be ble to write to the 25VF flash devices because this
// define introduces a level of status register lock down not implemented in the older
// production code. As a matter of fact, not ever implemented for the 25VF devices. So
// DO NOT set this define to a 1. Unless YOU know WTF you are doing.
#define	USE_HARDWARE_STATUS_REGISTER_LOCKING	(0)

// ----------

// 4/22/2016 rmd : These status register defines are only used with the SST26VF device.
#define SST26VF_SR_info_mask					0x3C
#define SST26VF_SR_write_suspend_erase			0x04
#define SST26VF_SR_write_suspend_program		0x08
#define SST26VF_SR_write_protection_lockdown	0x10
#define SST26VF_SR_security_id_locked			0x20

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	SPI_FLASH_WAIT_TILL_IDLE	TRUE
#define	SPI_FLASH_DONT_WAIT			FALSE

#define	SPI_FLASH_SEND_ADDR			TRUE
#define	SPI_FLASH_DONT_SEND_ADDR	FALSE

#define	SPI_FLASH_SEND_DATA8		TRUE
#define	SPI_FLASH_DONT_SEND_DATA8	FALSE

#define	SPI_FLASH_SEND_DATA16		TRUE
#define	SPI_FLASH_DONT_SEND_DATA16	FALSE

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//volatile unsigned long int dummy,i,j,rx_cnt,tx_cnt;
//unsigned char tx_array[256];


// 2012.01.19 rmd : TODO what is this - consider eliminating after study.
static INT_32 sspid;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// some prototypes needed for the code

void send_command_to_flash_device( UNS_32 flash_index, BOOL_32 pwait_till_idle, UNS_32 pcommand, UNS_32 paddress, BOOL_32 psend_address, UNS_8 pdata_8, BOOL_32 psend_data_8, UNS_16 pdata_16, BOOL_32 psend_data_16 );

BOOL_32 lock_flash_device( UNS_32 flash_index );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
extern void identify_the_device( UNS_32 flash_index )
{
	// 4/21/2016 rmd : Using the JEDEC READ_ID command learn the manufacturer and device. See if
	// we know about this device and the set the flash device appropriately so that all
	// operations know how to proceed.
	
	// ----------
	
	volatile JEDEC_ID_STRUCT	lid;
	
	volatile SSP_REGS_T			*pssp;
	
	UNS_32						iii;
	
	BOOL_32						recognized;
	
	// ----------
	
	pssp = ssp_define[ flash_index ].ssp_base;  // acquire the ssp base addr for use in the macros

	// ----------
	
	SSP_DR  = CMD_JEDEC_READ_ID;	// prepare Read Status Register1 command
	SSP_DR  = 0x00;					// and the dummy to cause the read
	SSP_DR  = 0x00;					// and the dummy to cause the read
	SSP_DR  = 0x00;					// and the dummy to cause the read
	
	CHIP_SELECT_LOW;			// Drive chip select low.

	ENABLE_TRANSMISSION;		// Let the SSP xmit the data in the FIFO. 
	
	while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );  // Use the SSP hardware to determine when xmission is done.
	
	DISABLE_TRANSMISSION;		// Disable the SSP

	CHIP_SELECT_HIGH;			// Drive chip select high.

	lid.manufacturer_id = SSP_DR;  // dummy read to get one out of the way
	lid.manufacturer_id = SSP_DR;
	lid.memory_type = SSP_DR;
	lid.memory_capacity = SSP_DR;
	
	// ----------
	
	// 4/21/2016 rmd : Run through our known responses.
	recognized = (false);
	
	// 4/21/2016 rmd : Don't start at index 0. That's not a device and I don't want to somehow
	// fall into 'recognizing' that device.
	for( iii=1; iii<FLASH_NUM_OF_TESTED_DEVICES; iii++ )
	{
		if( memcmp( &(known_JEDEC_READ_ID_results[ iii ]), (void*)&lid, sizeof(JEDEC_ID_STRUCT) ) == 0 )
		{
			flash_device_id[ flash_index ] = iii;
			
			recognized = (true);
			
			// 4/21/2016 rmd : And we are done so git.
			break;
		}
	}
	
	if( !recognized )
	{
		Alert_Message_va( "Flash part not recognized %d: %01X, %01X, %01X", flash_index, lid.manufacturer_id, lid.memory_type, lid.memory_capacity );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define DF_WAITING_FOR_IDLE_USE_TASK_DELAY		TRUE

#define DF_WAITING_FOR_IDLE_DONT_DELAY			FALSE

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void wait_for_flash_device_to_be_idle( UNS_32 flash_index, UNS_32 use_delay )
{
	// 4/21/2016 rmd : Wait till device is ready to properly accept a new command. We do so by
	// watching the BUSY bit in the status register. Reads the status register using the
	// standard CMD_RDSR command. This function works across ALL known devices as of this
	// writing.
	
	// ----------
	
	UNS_32	lloop_count;

	volatile UNS_32	ldummy;

	volatile SSP_REGS_T *pssp;

	// ----------

	pssp = ssp_define[ flash_index ].ssp_base;  // acquire the ssp base addr for use in the macros

	// ----------
	
	// 4/28/2016 rmd : Don't know what's in the SPI rcv FIFO so flush it. Otherwise you may
	// NEEDLESSLY execute some 1 tick OS delays in the loop below while trying to get the FIFO
	// to actually deliver the status register!
	while((SSP_SR & SSP_SR_RNE_MASK) == SSP_SR_RNE) ldummy = SSP_DR;

	// ----------
	
	SSP_DR  = CMD_RDSR;			// prepare Read Status Register1 command
	SSP_DR  = 0x00;				// and the dummy to cause the read of the status register

	CHIP_SELECT_LOW;			// Drive chip select low.

	ENABLE_TRANSMISSION;		// Let the SSP xmit the data in the FIFO. 

	while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );  // Use the SSP hardware to determine when xmission is done.

	ldummy = SSP_DR;
	ldummy = SSP_DR;  // Read the RX FIFO twice the second read being the status register.

	lloop_count = 0;

	while( (ldummy & DEVICE_BUSY_MASK) == DEVICE_BUSY )
	{
		// I have seen the device get into a state that says it is BUSY for ever. Even restarting
		// the program does not help. I think I need a way to reset the device when power is not
		// removed from our controller. Am I going to need a power switch to these guys? Oh boy.
		// What I think is much more likely is that we need to send the CMD_WRDIS command followed
		// by the CMD_DISABLE_SO_AS_BUSY_OUTPUT command. If the AAI write sequence was interrupted
		// abnormally (debugger?...brownout???) but the device power is not removed the part is
		// stuck in the AAI write mode. And forever busy.
		//
		// WELL THAT SEEMS TO BE EXACTLY THE CASE. If I put a break point in the middle of the AAI
		// write sequence and then restart the program the device is FOREVER busy. Stuck in the AAI
		// mode. And worse yet if we try to boot the system (without removing power) the LPC3250
		// attempt to read the SPI flash_0 will fail. And the boot will be unsuccessful.

		// ENDLESS LOOP??? Just rely on the WDT to save us. But then we have no indication of why
		// the WDT went off. hmmmm.
		// 
		// Gee how do you select the length of time to OSDelay here? Well I observed it took 16ms at
		// room temp on the part I happened to be using to do the sector erase. Using 4 ms delay
		// results in 20ms wait. Using 5ms delay also results in 20ms wait. Using 3 ms delay results
		// in 18ms delay. So the delay costs us about 2ms. The great benefit is of course the OS is
		// free to take care of other tasks.

		// ----------
		
		lloop_count++;

		// ----------
		
		if( use_delay == DF_WAITING_FOR_IDLE_USE_TASK_DELAY )
		{
			if( (lloop_count % 100) == 0 )
			{
				// So we've been here for a long time (like 500 ms). At least throw an Alert_Message. And
				// let the Watchdog Task get us out.
				Alert_Message_va( "FLASH: always busy! %u", lloop_count );  // Should NEVER happen!
			}
	
			// 4/18/2016 rmd : For the OS delay function to work we must ask to wait at least 1 tick.
			// This means 0 to 5 ms. But in practice we see a rather consistent 2ms. Must have to do
			// with when the flash task is invoked relative to the OS tick.
			vTaskDelay( 1 );
		}

		// ----------
		
		SSP_DR  = 0x00;		// Load the dummy to cause the read of the status register.

		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );

		ldummy = SSP_DR;	// read the retrieved status register
	}

	DISABLE_TRANSMISSION;		// Disable the SSP

	CHIP_SELECT_HIGH;			// Drive chip select high.
}

/* ---------------------------------------------------------- */
void init_FLASH_DRIVE_SSP_channel( UNS_32 flash_index )
{
	// 4/21/2016 rmd : The serial flash part data sheet(s) generally specify some delay from
	// power up to before attempting the first exchange with the flash part. They don't specify
	// much time at all. Less than 1ms is what I see. Because of where this function is called
	// within the startup process we are guaranteed many times this time. Espcially since we are
	// waiting for the RTC to cross its first one second tick.
	
	// ----------
	
	SSP_CONFIG_T sspcfg;

	// Make all the settings needed.
	sspcfg.databits				= 8;
	sspcfg.mode					= SSP_CR0_FRF_SPI;
	sspcfg.highclk_spi_frames	= TRUE;
	sspcfg.usesecond_clk_spi	= TRUE;

	// There is not as much to gain as you might think by raising the clock rate ever higher. For example a sequence of reads and writes
	// to accomplish several file operations takes 120ms with a 6MHz clock. With a 24MHz clock that same sequence takes 104ms. Yes a gain.
	// But at 12MHz that sequence takes 108ms. So doubling the clock rate only bought us a 4ms improvement on a 100ms operation. The bulk
	// of the time is spent waiting for the flash device to accomplish the sector erases and writes.
	// 
	// On the CS3000 board the SPI Flash fails when the SSP clock is set to 48MHz. At 24MHz it works. Where is the threshold? I don't know
	// and based on the prior discussion I have decided to run the SPI flash with a 12MHz clock. Performance will be fine. It will have to be ehh?
	// sspcfg.ssp_clk				= 12000000;  // results in an approx 13MHz clock
	// SEE BELOW
	// 
	// WELL after discovering that we could not use the AAI programming mode (system non-bootable issue), we recoded the write function using
	// a byte by byte write. This more than doubled the write times. At 12MHz clock it now takes 85ms to write 4096 bytes. At 24MHz clock it
	// takes 75ms to write the same 4096 bytes. So I;ve decided to use the 24MHz clock setting.
	sspcfg.ssp_clk				= 24000000;  // results in an approx 26MHz clock

	sspcfg.master_mode			= TRUE;

	// Initialize the SSP channel
	sspid = ssp_open( ssp_define[ flash_index ].ssp_base, (INT_32)&sspcfg );
	
	// ----------
	
	identify_the_device( flash_index );
	
	// ----------
	
	lock_flash_device( flash_index );  // If a brown-out or watchdog hit, it is possible the device is not locked down!
}

/* ---------------------------------------------------------- */
void send_command_to_flash_device( UNS_32 flash_index, BOOL_32 pwait_till_idle, UNS_32 pcommand, UNS_32 paddress, BOOL_32 psend_address, UNS_8 pdata_8, BOOL_32 psend_data_8, UNS_16 pdata_16, BOOL_32 psend_data_16 )
{
	// This function is used to send many different commands. The LPC3250 SSP channel has an 8 frame FIFO. Becareful not to exceed.

	volatile UNS_32	ldummy;

	SSP_REGS_T *pssp;

	pssp = ssp_define[ flash_index ].ssp_base;  // acquire the ssp base addr for use in the macros

	// ----------
	
	if ( pwait_till_idle == (true) )
	{
		// 4/29/2016 rmd : DO NOT use the TASK_DELAY parameter while waiting for idle. If you do
		// this the byte by byte write timing will be GREATLY lengthened.
		wait_for_flash_device_to_be_idle( flash_index, DF_WAITING_FOR_IDLE_DONT_DELAY );  // Make sure FLASH is ready to accept a command.
	}

	// ----------
	
	SSP_DR = pcommand;		// Load the TX FIFO with the Write Enable command.

	if( psend_address == (true) )
	{
		SSP_DR  = ((paddress & 0x00FF0000) >> 16);	// MSByte of the address
		SSP_DR  = ((paddress & 0x0000FF00) >> 8);	// Then middle byte
		SSP_DR  =  (paddress & 0x000000FF);			// LSByte last
	}

	// We could check here if we are being asked to send both the 8 and the 16 but you know
	// either the caller has it correct or the activity will fail. Besides I'm not sure what
	// we could do about it. Writing an Alert line could be a mess. As it may happen a bunch.
	// Anyway...give 8 precedence.
	if( psend_data_8 == (true) )
	{
		SSP_DR  = pdata_8;
	}
	else
	if( psend_data_16 == (true) )
	{
		SSP_DR  = (pdata_16 & 0x00FF);			// LSByte first
		SSP_DR  = ((pdata_16 & 0xFF00) >> 8);	// MSByte next
	}
	
	CHIP_SELECT_LOW;		// Drive chip select low.
	ENABLE_TRANSMISSION;	// Let the SSP xmit the data in the FIFO.

	// Use the SSP hardware to determine when xmission is done.
	while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );

	DISABLE_TRANSMISSION;	// Disable the SSP
	CHIP_SELECT_HIGH;		// Drive chip select high.

	// !!! After any command the datasheet calls for CS to remain HIGH for 100ns at 25MHz and 50ns at
	// >25MHz clock speeds. To digest the command I presume? How do we assure that? Well thankfully
	// the following RX FIFO flushing will give us at least that much. Even with optimized code.

	// Flush the RX FIFO.
	while((SSP_SR & SSP_SR_RNE_MASK) == SSP_SR_RNE) ldummy = SSP_DR;
}

/* ---------------------------------------------------------- */
BOOL_32 flash_device_is_write_enabled( UNS_32 flash_index )
{
	// 4/21/2016 rmd : This function reads the status register using the standard CMD_RDSR
	// command. This function works across ALL known devices as of this writing.
	
	// ----------
	
	volatile UNS_32	ldummy;

	SSP_REGS_T *pssp;

	pssp = ssp_define[ flash_index ].ssp_base;  // acquire the ssp base addr for use in the macros

	// 4/28/2016 rmd : Don't know what's in the SPI rcv FIFO so flush it.
	while((SSP_SR & SSP_SR_RNE_MASK) == SSP_SR_RNE) ldummy = SSP_DR;

	SSP_DR  = CMD_RDSR;			// prepare Read Status Register1 command
	SSP_DR  = 0x00;				// and the dummy to cause the read of the status register
	
	CHIP_SELECT_LOW;			// Drive chip select low.
	ENABLE_TRANSMISSION;		// Let the SSP xmit the data in the FIFO. 
	
	while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );  // Use the SSP hardware to determine when xmission is done.
	
	DISABLE_TRANSMISSION;		// Disable the SSP
	CHIP_SELECT_HIGH;			// Drive chip select high.

	ldummy = SSP_DR;
	ldummy = SSP_DR;	// Read the RX FIFO twice the second read being the status register.
	
	return( (ldummy & DEVICE_WEL_MASK) == DEVICE_WEL );		// return TRUE if write enabled
}

/* ---------------------------------------------------------- */
BOOL_32 unlock_and_write_enable_flash_device( UNS_32 flash_index )
{
	// Prepare SPI Flash for erase/programming. Use the EWSR command and set the BPL bits all to 0. To unlock
	// all the sectors for erase.
	BOOL_32	rv;

	UNS_32	next_command;
	
	volatile UNS_32	ldummy;

	volatile SSP_REGS_T *pssp;
	
	// ----------

	pssp = ssp_define[ flash_index ].ssp_base;  // acquire the ssp base addr for use in the macros

	rv = FALSE;

	// ----------
	
	// 4/21/2016 rmd : Here is where the devices diverge.
	if( flash_device_id[ flash_index ] == FLASH_DEV_ID_microchip_sst26vf032b )
	{
		// 4/28/2016 rmd : In preparation for reading the status register flush the rcv FIFO.
		while((SSP_SR & SSP_SR_RNE_MASK) == SSP_SR_RNE) ldummy = SSP_DR;
	
		// 4/22/2016 rmd : FIRST read the status register. And investigate for an abnormal state.
		SSP_DR  = CMD_RDSR;
		SSP_DR  = 0x00;				// and the dummy to cause the read of the status register
	
		CHIP_SELECT_LOW;			// Drive chip select low.
		ENABLE_TRANSMISSION;		// Let the SSP xmit the data in the FIFO. 
	
		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );  // Use the SSP hardware to determine when xmission is done.
	
		DISABLE_TRANSMISSION;	// Let the SSP xmit the data in the FIFO. 
		CHIP_SELECT_HIGH;		// Drive chip select low.
	
		ldummy = SSP_DR;
		ldummy = SSP_DR;  // Read the RX FIFO twice the second read being the status register.

		// 4/22/2016 rmd : Check upfront are any of the bits set that we don't expect. Time saving
		// move.
		if( (ldummy & SST26VF_SR_info_mask) != 0 )
		{
			if( (ldummy & SST26VF_SR_write_suspend_erase) == SST26VF_SR_write_suspend_erase )
			{
				Alert_Message( "SST26 WSE set!" );
			}

			if( (ldummy & SST26VF_SR_write_suspend_program) == SST26VF_SR_write_suspend_program )
			{
				Alert_Message( "SST26 WSP set!" );
			}

			if( (ldummy & SST26VF_SR_write_protection_lockdown) == SST26VF_SR_write_protection_lockdown )
			{
				Alert_Message( "SST26 WPLD set!" );
			}

			if( (ldummy & SST26VF_SR_security_id_locked) == SST26VF_SR_security_id_locked )
			{
				Alert_Message( "SST26 SEC set!" );
			}
		}
		
		// ----------
		
		wait_for_flash_device_to_be_idle( flash_index, DF_WAITING_FOR_IDLE_USE_TASK_DELAY );


		// 4/29/2016 rmd : WRITE ENABLE the device.
		SSP_DR  = CMD_WREN;
	
		CHIP_SELECT_LOW;			// Drive chip select low.
		ENABLE_TRANSMISSION;		// Let the SSP xmit the data in the FIFO. 
	
		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );  // Use the SSP hardware to determine when xmission is done.
	
		DISABLE_TRANSMISSION;
		CHIP_SELECT_HIGH;			// Drive chip select low.

		// ----------
		
		// 4/22/2016 rmd : With the device WRITE ENABLED send the command to unlock the whole
		// device. I believe we need to set the WP# input high in order to execute this command as
		// it seems to me it changes the Block Protection Register.
		WRITE_PROTECT_HIGH;

		SSP_DR  = CMD_ULBPR;
	
		CHIP_SELECT_LOW;			// Drive chip select low.
		ENABLE_TRANSMISSION;		// Let the SSP xmit the data in the FIFO.
	
		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );  // Use the SSP hardware to determine when xmission is done.
	
		DISABLE_TRANSMISSION;
		CHIP_SELECT_HIGH;			// Drive chip select low.
	
		WRITE_PROTECT_LOW;
		
		// 4/28/2016 rmd : There is little to no delay here but wait for the device to become
		// officially idle.
		wait_for_flash_device_to_be_idle( flash_index, DF_WAITING_FOR_IDLE_USE_TASK_DELAY );

		// ----------

		// 4/22/2016 rmd : Now that we have completed UNLOCKING the device. Set it to WRITE ENABLED
		// again as the function is supposed to return with the device WEL bit set.
		send_command_to_flash_device( flash_index, SPI_FLASH_WAIT_TILL_IDLE,  CMD_WREN,  0,  SPI_FLASH_DONT_SEND_ADDR,  0,  SPI_FLASH_DONT_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );

		if ( flash_device_is_write_enabled( flash_index ) == TRUE )
		{
			rv = (true);
		}
		else
		{
			Alert_Message( "FLASH: not WEL" );  // unexpected state
		}
	}
	else
	{
		// 4/21/2016 rmd : Right now the SST25VF and the IS25CQ are the only two parts that should
		// fall into this section (given that is all we would ever build). For these parts there is
		// a single status register that we write to.

		// ----------
		
		// 4/15/2016 rmd : Prepare for a WRSR command with a status register write enable command.
		// Beaware the IS25 device DOES NOT support the CMD_EWSR command like the SST25VF device
		// does.
		if( flash_device_id[ flash_index ] == FLASH_DEV_ID_microchip_sst25vf032b )
		{
			next_command = CMD_EWSR;
		}
		else
		{
			next_command = CMD_WREN;
		}

		// 1/10/2017 rmd : This function to send the command ends by driving CS back high. Which is
		// a REQUIREMENT before the WRSR command is sent.
		send_command_to_flash_device( flash_index, SPI_FLASH_WAIT_TILL_IDLE, next_command,  0,  SPI_FLASH_DONT_SEND_ADDR,  0,  SPI_FLASH_DONT_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );
		
		// ----------
		
		// 4/22/2016 rmd : With the device write enabled we can now unlock the device by writing to
		// the status register.
		
		SSP_DR  = CMD_WRSR;

		#if USE_HARDWARE_STATUS_REGISTER_LOCKING
			SSP_DR  = DEVICE_ALL_BLOCKS_UNPROTECTED_WITH_LOCK_DOWN;
		#else
			SSP_DR  = DEVICE_ALL_BLOCKS_UNPROTECTED_NO_LOCK_DOWN;
		#endif

		// 4/21/2016 rmd : Since we are setting the high order bit in the status register we must
		// override that by making the WP input high.
		WRITE_PROTECT_HIGH;
	
		CHIP_SELECT_LOW;		// Drive chip select low.
		ENABLE_TRANSMISSION;	// Let the SSP xmit the data in the FIFO. 
	
		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );	// Use the SSP hardware to determine when xmission is done.
	
		DISABLE_TRANSMISSION;
		CHIP_SELECT_HIGH;		// Drive chip select low.
	
		// 4/21/2016 rmd : Re-enable write protection on the status register.
		WRITE_PROTECT_LOW;
	
		// ----------
		
		// 4/19/2016 rmd : On the ISSI 25CQ032 part this appear to be about 6ms for the status
		// register command to process and the device to go idle. One the SST25VF part there is NO
		// wait. The very first read of the status register shows the device is not busy.
		wait_for_flash_device_to_be_idle( flash_index, DF_WAITING_FOR_IDLE_USE_TASK_DELAY );
		
		// ----------
		
		// 4/28/2016 rmd : In preparation for reading the status register flush the rcv FIFO.
		while((SSP_SR & SSP_SR_RNE_MASK) == SSP_SR_RNE) ldummy = SSP_DR;

		// READ the status register
		SSP_DR  = CMD_RDSR;			// prepare Read Status Register1 command
		SSP_DR  = 0x00;				// and the dummy to cause the read of the status register
	
		CHIP_SELECT_LOW;			// Drive chip select low.
		ENABLE_TRANSMISSION;		// Let the SSP xmit the data in the FIFO. 
	
		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );  // Use the SSP hardware to determine when xmission is done.
	
		DISABLE_TRANSMISSION;
		CHIP_SELECT_HIGH;		// Drive chip select low.
	
		ldummy = SSP_DR;
		ldummy = SSP_DR;  // Read the RX FIFO twice the second read being the status register.
	
		// ----------
		
		#if USE_HARDWARE_STATUS_REGISTER_LOCKING
			if( (ldummy & DEVICE_BLOCK_PROTECTION_MASK) == DEVICE_ALL_BLOCKS_UNPROTECTED_WITH_LOCK_DOWN )
		#else
			if( (ldummy & DEVICE_BLOCK_PROTECTION_MASK) == DEVICE_ALL_BLOCKS_UNPROTECTED_NO_LOCK_DOWN )
		#endif
		{
			// This is good. We wanted to unlock the device. Now write enable it so we can actually erase or write data to it.
			send_command_to_flash_device( flash_index, SPI_FLASH_WAIT_TILL_IDLE,  CMD_WREN,  0,  SPI_FLASH_DONT_SEND_ADDR,  0,  SPI_FLASH_DONT_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );
	
			if ( flash_device_is_write_enabled( flash_index ) == TRUE )
			{
				rv = (true);
			}
			else
			{
				Alert_Message( "FLASH: not WEL" );  // unexpected state
			}
		}
		else
		{
			Alert_Message( "FLASH: blocks not unlocked." );  // unexpected state
		}
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
BOOL_32 lock_flash_device( UNS_32 flash_index )
{
	// 4/15/2016 rmd : Coded using GENERIC serial flash commands. As opposed to using commands
	// unique to the Microchip SST25VF032B part (specifically the EWSR command). As such we now
	// must include manipulation of the WP line. When the SRWD/BPL bit is set to a 1, the ONLY
	// way to write to the status register (WRSR command) is to raise the WP input to the
	// device.
	//
	// So this function will execute a WREN command to prepare for the WRSR command. Next we
	// will issue the WRSR command but also make sure the WP line is driven HIGH during the WRSR
	// command clocking and especially before CE is raised (raising the CE triggers the
	// execution of the WRSR command).
	//
	// The end result should be that the device is locked down via BP2, BP1, BP0 all set. And
	// the SRWD bit is also set.
	//
	// Old measurement: This function takes 10.5usec to execute.
	
	BOOL_32	rv;
	
	UNS_32	iii, next_command;
	
	volatile UNS_32	ldummy;

	volatile SSP_REGS_T *pssp;

	// 4/29/2016 rmd : These bytes are what is written into the BPR. That register is 80 bits
	// wide (for the 32Mbit device). The data is ordered with the MSBit first. Note the 0x55's
	// upfront in the array. These are to NOT SET the read protection bits. If we prevent
	// reading in our scheme we cannot query the file system to check for file existence. And
	// even cannot see if the flash part has been 'formatted'. Of course we could incorporate
	// this read protection feature but code changes would be needed to unprotect prior to
	// reads. And read protection is not necessary for us.
	const UNS_8	bpr_bytes[ 10 ] = {	0x55, 0x55, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	
	// ----------
	
	pssp = ssp_define[ flash_index ].ssp_base;  // acquire the ssp base addr for use in the macros

	rv = (false);

	// ----------
	
	// 4/21/2016 rmd : Here is where the devices diverge.
	if( flash_device_id[ flash_index ] == FLASH_DEV_ID_microchip_sst26vf032b )
	{
		send_command_to_flash_device( flash_index, SPI_FLASH_WAIT_TILL_IDLE,  CMD_WREN,  0,  SPI_FLASH_DONT_SEND_ADDR,  0,  SPI_FLASH_DONT_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );

		// ----------
		
		// WRITE to the configuration register
		SSP_DR  = CMD_WRSR;		// command
		SSP_DR  = 0x00;			// this is a dummy byte - the SR is read-only
		SSP_DR  = 0x80;			// enable WPEN in the configuration register
		
		WRITE_PROTECT_HIGH;
		
		CHIP_SELECT_LOW;		// Drive chip select low.
		ENABLE_TRANSMISSION;	// Let the SSP xmit the data in the FIFO. 
	
		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );	// Use the SSP hardware to determine when xmission is done.
	
		DISABLE_TRANSMISSION;
		CHIP_SELECT_HIGH;		// Drive chip select high.
	
		WRITE_PROTECT_LOW;
	
		// ----------
		
		wait_for_flash_device_to_be_idle( flash_index, DF_WAITING_FOR_IDLE_USE_TASK_DELAY );
		
		// ----------
		
		// 4/25/2016 rmd : Now write to the Block Protection Register to lock down all the sectors.
		// Which requires a WREN command prior to the WBPR command.
		send_command_to_flash_device( flash_index, SPI_FLASH_WAIT_TILL_IDLE,  CMD_WREN,  0,  SPI_FLASH_DONT_SEND_ADDR,  0,  SPI_FLASH_DONT_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );


		SSP_DR  = CMD_WBPR;		// command
		
		WRITE_PROTECT_HIGH;
		
		CHIP_SELECT_LOW;		// Drive chip select low.
		ENABLE_TRANSMISSION;	// Let the SSP xmit the data in the FIFO. 
	
		// 4/25/2016 rmd : Write the 80-bits worth of data to the block protection register. The
		// data sheet talks about 18 data cycles but the BPR bit definition table defines only 80
		// bits. I think the 18 must be in reference to larger flash devices. So we will only write
		// the 10 bytes worth.
		for( iii=0; iii<10; iii++ )
		{
			SSP_DR  = bpr_bytes[ iii ];

			// 4/28/2016 rmd : The SPI hardware only has an 8 bit FIFO so must wait for xmission to
			// comnplete given we are sending 10 bytes.
			while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );
		}
		
		DISABLE_TRANSMISSION;
		CHIP_SELECT_HIGH;		// Drive chip select high.
	
		WRITE_PROTECT_LOW;

		// 4/27/2016 rmd : And wait for the device to be done locking down.
		wait_for_flash_device_to_be_idle( flash_index, DF_WAITING_FOR_IDLE_USE_TASK_DELAY );
	}
	else
	{
		// 4/25/2016 rmd : So BOTH the SST25VF032B and the IS25CQ032 devices have the block
		// protection bits in the STATUS REGISTER. And function the same when it comes to the
		// lockdown feature.
		
		// ----------
		
		// 4/15/2016 rmd : Prepare for a WRSR command with a status register write enable command.
		// Beaware the IS25 device DOES NOT support the CMD_EWSR command like the SST25VF device
		// does.
		if( flash_device_id[ flash_index ] == FLASH_DEV_ID_microchip_sst25vf032b )
		{
			next_command = CMD_EWSR;
		}
		else
		{
			next_command = CMD_WREN;
		}

		// 1/10/2017 rmd : This function to send the command ends by driving CS back high. Which is
		// a REQUIREMENT before the WRSR command is sent.
		send_command_to_flash_device( flash_index, SPI_FLASH_WAIT_TILL_IDLE, next_command,  0,  SPI_FLASH_DONT_SEND_ADDR,  0,  SPI_FLASH_DONT_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );
		
		// ----------
		
		// WRITE to the status register
		SSP_DR  = CMD_WRSR;		// command to WRITE STATUS REGISTER

		#if USE_HARDWARE_STATUS_REGISTER_LOCKING
			SSP_DR  = DEVICE_ALL_BLOCKS_UNPROTECTED_WITH_LOCK_DOWN;
		#else
			SSP_DR  = DEVICE_ALL_BLOCKS_UNPROTECTED_NO_LOCK_DOWN;
		#endif

		// 1/10/2017 rmd : Allows writing to the STATUS REGISTER regardless of the state of the BPL
		// bit in the status register.
		WRITE_PROTECT_HIGH;

		CHIP_SELECT_LOW;		// Drive chip select low.
		ENABLE_TRANSMISSION;	// Let the SSP xmit the data in the FIFO. 
	
		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );	// Use the SSP hardware to determine when xmission is done.
	
		DISABLE_TRANSMISSION;
		CHIP_SELECT_HIGH;		// Drive chip select high.
	
		WRITE_PROTECT_LOW;
	
		// ----------
		
		// 4/19/2016 rmd : On the ISSI 25CQ032 part this appear to be about 6ms for the status
		// register command to process and the device to go idle. For the MICROCHIP 25VF032B part
		// this delay is rendered to 1us because the very first read of the status register
		// following the WRSR command shows the part to be idle. It is ready IMMEDIATELY. A notable
		// difference between the two parts.
		wait_for_flash_device_to_be_idle( flash_index, DF_WAITING_FOR_IDLE_USE_TASK_DELAY );
		
		// ----------
		
		// 4/28/2016 rmd : In preparation for reading the status register flush the rcv FIFO so we
		// know where we are at.
		while((SSP_SR & SSP_SR_RNE_MASK) == SSP_SR_RNE) ldummy = SSP_DR;

		// READ the status register
		SSP_DR  = CMD_RDSR;			// prepare Read Status Register1 command
		SSP_DR  = 0x00;				// and the dummy to cause the read of the status register
	
		CHIP_SELECT_LOW;			// Drive chip select low.
		ENABLE_TRANSMISSION;		// Let the SSP xmit the data in the FIFO. 
	
		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );  // Use the SSP hardware to determine when xmission is done.
	
		DISABLE_TRANSMISSION;
		CHIP_SELECT_HIGH;		// Drive chip select high.
	
		ldummy = SSP_DR;
		ldummy = SSP_DR;  // Read the RX FIFO twice the second read being the status register.
	
		// ----------
		
		#if USE_HARDWARE_STATUS_REGISTER_LOCKING
			if( (ldummy & DEVICE_BLOCK_PROTECTION_MASK) == DEVICE_ALL_BLOCKS_UNPROTECTED_WITH_LOCK_DOWN )
		#else
			if( (ldummy & DEVICE_BLOCK_PROTECTION_MASK) == DEVICE_ALL_BLOCKS_UNPROTECTED_NO_LOCK_DOWN )
		#endif
		{
			// This is good. We wanted to lock the device. And it's done.
			rv = (true);
		}
		else
		{
			Alert_Message( "FLASH: blocks not sucessfully locked." );  // unexpected state
		}
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
UNS_32 SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming( UNS_32 flash_index, UNS_32 paddress, DATA_HANDLE pdh )
{
	// It is assumed upon entry that the data from the 4K sector to be erased has been previously read out of that
	// sector. All of it. As when we do the erase it is gone. So it follows that pdh contains a pointer to all the
	// data for that entire sector.
	// 
	// RETURNS the status. If there is an impending powerfail the status indicates the write has failed.
	// 
	// NOTES:	This function takes 85ms to program a full 4096 byte page (when using a 12MHz clock). Not bad compared to the AAI method which leads to the NON-BOOTABLE issue.
	//  		When using a 24MHz clock the same 4096 bytes takes 75ms. (When the code is optimized to Level 1 this becomes 70ms.) Enough of an improvement I've decided to
	// 			use the 24MHz clock.
	//
	// NOTES:	The byte programming method is safe. Cannot end up in a NON-BOOTABLE system condition. Though it takes longer than the AAI method.

	UNS_32	rv;

	UNS_32	bytes_remaining;

	UNS_32	to_program_now;

	volatile UNS_32	ldummy;

	UNS_8	*ucp;

	SSP_REGS_T *pssp;

	// ----------
	
	rv = DF_RESULT_SUCCESSFUL;
	
	// ----------
	
	pssp = ssp_define[ flash_index ].ssp_base;  // acquire the ssp base addr for use in the macros
	
	// ----------
	
	if( restart_info.SHUTDOWN_impending == TRUE )
	{
		rv = DF_IMPENDING_POWER_FAIL_ABORT;
	}
	else
	if( pdh.dlen == 0 )
	{
		Alert_Message( "FLASH: trying to write zero bytes!" );
		
		rv = DF_TOO_FEW_BYTES_TO_WRITE;
	}
	else
	if( pdh.dlen > DF_PAGE_SIZE_IN_BYTES )
	{
		Alert_Message( "FLASH: trying to program too many bytes!" );

		rv = DF_TOO_MANY_BYTES_TO_WRITE;
	}
	else
	{
		// The write_enable_flash_device function takes 20usec to execute. Regarding the powerfail it's best to just let it execute. Then
		// check afterwards if we've got a powerfail indication. And use that to decide to erase the sector or not. Cause that sector erase
		// takes 18ms!
		//CS_LED_YELLOW_ON;
				
		if ( unlock_and_write_enable_flash_device( flash_index ) )
		{
			//CS_LED_YELLOW_OFF;
				
			if( restart_info.SHUTDOWN_impending == FALSE )
			{
				// Now write enabled and ready to accept the sector erase command. 18ms!!!!
				send_command_to_flash_device( flash_index, SPI_FLASH_WAIT_TILL_IDLE,  CMD_4K_SECTOR_ERASE,  paddress,  SPI_FLASH_SEND_ADDR,  0,  SPI_FLASH_DONT_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );
		
				// THIS IS A LONG WAIT! Like 18ms I measured on the scope. The datasheet states 25ms max.
				// This can be the bulk of the time spent while writing - that is erasing the sector. The
				// time to write to the freshly erased sector depends on how many bytes there are to write.
				// As an example 180 bytes for the file directory entry takes about 1.1ms to write. Or about
				// 6usec per byte. And that should multiple up to about 25ms for an entire 4192 byte sector.
				//
				// 4/19/2016 rmd : An UPDATE on this wait. Yes for the Microchip 25VF032B part indeed the
				// wait is about 20ms. And the data sheet states a maximum of 25ms. But I've been testing
				// another part. The ISSI IS25CQ032 and it's wait here is CONSIDERABLY longer. I measure
				// about 100ms on the scope. And their data sheet quotes up to 450ms maximum. And this shows
				// when writing files. A small single block file write moves from about 100ms total
				// (includes the file write, directory update, and subsequent previous edit deletion) to
				// 500ms. Other examples are the STATION HISTORY file goes from 4 seconds to 6 seconds. The
				// STATION REPORT DATA goes from 9 seconds to 12 seconds. All file writes are significantly
				// longer with the ISSI 25CQ part.
				//
				// 4/19/2016 rmd : Do recognize that this sector erase delay is the biggest delay
				// contribution of any write activity.
				wait_for_flash_device_to_be_idle( flash_index, DF_WAITING_FOR_IDLE_USE_TASK_DELAY );
		
				// ----------
				
				ucp = pdh.dptr;  // start addr

				bytes_remaining = pdh.dlen;

				// ----------

				// 4/21/2016 rmd : Here is where the devices diverge.

				if( (flash_device_id[ flash_index ] == FLASH_DEV_ID_issi_is25cq032) || (flash_device_id[ flash_index ] == FLASH_DEV_ID_microchip_sst26vf032b) )
				{
					// 4/29/2016 rmd : BY THE WAY ... the write throughput for these devices is very close to
					// 100K bytes per second. Because of file system directory entry writes smaller files are
					// less. Bigger files are more.
					
					// 4/22/2016 rmd : These devices use the same 0x02 programming command BUT that command
					// programs up to 256 bytes per [CE LOW-0x02 command-CE HIGH] cycle.
					while( (bytes_remaining > 0) && (restart_info.SHUTDOWN_impending == FALSE) )
					{
						if( bytes_remaining > 256 )
						{
							to_program_now = 256;	
						}
						else
						{
							to_program_now = bytes_remaining;	
						}
						
						bytes_remaining -= to_program_now;
						
						// ----------
						
						// 4/22/2016 rmd : About the wait here. Notice we include the WAIT_TILL_IDLE request before
						// we issue the command. Well after the SECTOR ERASE we don't expect to wait of course as we
						// already waited and indeed this command takes about 5 usec to execute. HOWEVER after we
						// have written in up to the 256 bytes and we loop around to here because there is more to
						// write the wait is a bit more than 1ms on the scope. The SST26VF data sheet states up to
						// 1.5ms maximum is possible.
						send_command_to_flash_device( flash_index, SPI_FLASH_WAIT_TILL_IDLE,  CMD_WREN,  0,  SPI_FLASH_DONT_SEND_ADDR,  0,  SPI_FLASH_DONT_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );
	
						// ----------
						
						SSP_DR  = CMD_BYTE_PROGRAMMING;		// command to PROGRAM A PAGE

						SSP_DR  = ((paddress & 0x00FF0000) >> 16);	// MSByte of the address
						SSP_DR  = ((paddress & 0x0000FF00) >> 8);	// Then middle byte
						SSP_DR  =  (paddress & 0x000000FF);			// LSByte last
						
						// 4/22/2016 rmd : To get ready for the next block UPDATE the next address NOW before we
						// loose the value of the to_program_now variable.
						paddress += to_program_now;
						
						// ----------
						
						CHIP_SELECT_LOW;		// Drive chip select low.
						ENABLE_TRANSMISSION;	// Let the SSP xmit the data in the FIFO. 
					
						while( to_program_now > 0 )
						{
							SSP_DR  = *ucp;  // get the first byte - we have at least one
							ucp += 1;
							to_program_now -= 1;
												
							while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );	// Use the SSP hardware to determine when xmission is done.
						}
						
						// 4/22/2016 rmd : DONE.
						DISABLE_TRANSMISSION;
						CHIP_SELECT_HIGH;		// Drive chip select high.
					
						// ----------
						
						// 4/22/2016 rmd : Housekeeping step. Flush the RX FIFO.
						while((SSP_SR & SSP_SR_RNE_MASK) == SSP_SR_RNE) ldummy = SSP_DR;
					}
				}
				else
				{
					// 4/22/2016 rmd : The SST25VF032B device falls through to here. It has ONLY a byte by byte
					// programming scheme (we do not like the AAI mode).
					
					// 4/29/2016 rmd : BY THE WAY ... the write throughput for these devices is very close to
					// 56K bytes per second. Because of file system directory entry writes smaller files are
					// less. Bigger files are more.
					
					while( (bytes_remaining > 0) && (restart_info.SHUTDOWN_impending == FALSE) )
					{
						// 4/25/2016 rmd : In practice this byte-by-byte programming loop programs one byte each
						// 13usec for the SST25VF device.
						
						// To send this command really only takes about 2usec but we are waiting for the byte programming to finish.
						send_command_to_flash_device( flash_index, SPI_FLASH_WAIT_TILL_IDLE,  CMD_WREN,  0,  SPI_FLASH_DONT_SEND_ADDR,  0,  SPI_FLASH_DONT_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );
	
						// program a byte ... takes about 13usec for each byte
						send_command_to_flash_device( flash_index, SPI_FLASH_DONT_WAIT,  CMD_BYTE_PROGRAMMING,  paddress,  SPI_FLASH_SEND_ADDR,  *ucp,  SPI_FLASH_SEND_DATA8,  0,  SPI_FLASH_DONT_SEND_DATA16 );
						
						bytes_remaining--;
						
						ucp++;
						
						paddress++;
					}
				}
			}
			else
			{
				// We put ourselves into WRITE ENABLE mode but then detected an impending power fail. MUST take ourselves out of the mode. Because if there
				// isn't really a power failure (brown out) and we restart the program the device will STILL be write enabled. And it may not work? Even
				// without restarting we need to re-lock the device. And that occurs after all these elses. Note we are NOT exiting the AAI mode here.
				// Just resetting the WEL bit in the status register. And either the CMD_WRDIS or CMD_WRSR commands will do that (as well as others).
				rv = DF_IMPENDING_POWER_FAIL_ABORT;
			}
		}
		else
		{
			Alert_Message( "FLASH: device wouldn't write enable." );

			rv = DF_FUNCTIONAL_ERROR;
		}

		//CS_LED_RED_ON;
						
		// We are done writing this page. So re-lock the device. This ALSO disables the write! Like a CMD_WRDIS would do. Because writing to the status register does that.
		lock_flash_device( flash_index );
		
		//CS_LED_RED_OFF;
						
		if( flash_device_is_write_enabled( flash_index ) == (true) )
		{
			Alert_Message( "Still WRITE enabled!");
		}
		
		// So now if the power fail went TRUE since we started the write set this flag to indicate that.
		if( restart_info.SHUTDOWN_impending == TRUE ) {
			// This probably indicates some data was not saved. Or worse. The flash file system is now corrupt.
			restart_info.we_were_writing_spi_flash_data = TRUE;  
		}
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
UNS_32 SPI_FLASH_fast_read_as_much_as_needed_to_buffer( UNS_32 flash_index, UNS_32 paddress, DATA_HANDLE pdh )
{
	// This function can read as much as you need. And even across 4K sector boundaries. We need to implement the
	// read using step by step approach. Cause the CS line must remian low throughout and the send_command function
	// will drive it back high. And because of that we have no worries like with the AAI mode in conjunction with
	// a power fail.
	// 
	// NOTES:	With a 26MHz clock and non-optimized code it takes 2.3ms to read 4096 bytes. When the code is optimized
	//			to level 1 it takes 2ms.
	//
	// NOTES:	Regarding power fail. In view of the short possible time we are in this function (2ms max) we are just
	// 			going to let it complete the read. Then prevent any further reads from starting.

	UNS_32	iii, rv;
	
	volatile UNS_32	ldummy;

	SSP_REGS_T *pssp;
	
	rv = DF_RESULT_SUCCESSFUL;

	if( restart_info.SHUTDOWN_impending == TRUE )
	{
		rv = DF_IMPENDING_POWER_FAIL_ABORT;
	}
	else
	{
		pssp = ssp_define[ flash_index ].ssp_base;  // acquire the ssp base addr for use in the macros
		
		wait_for_flash_device_to_be_idle( flash_index, DF_WAITING_FOR_IDLE_DONT_DELAY );  // Make sure FLASH is ready to accept a command.
	
		SSP_DR = CMD_HIGH_SPEED_READ;					// Load the TX FIFO with the read command.
	
		SSP_DR  = ((paddress & 0x00FF0000) >> 16);		// MSByte of the address
		SSP_DR  = ((paddress & 0x0000FF00) >> 8);		// Then middle byte
		SSP_DR  =  (paddress & 0x000000FF);				// LSByte last
	
		SSP_DR  = 0x00;									// and send a dummy byte as required
	
		CHIP_SELECT_LOW;		// Drive chip select low.
		ENABLE_TRANSMISSION;	// Let the SSP xmit the data in the FIFO. 
	
		// Use the SSP hardware to determine when xmission is done.
		while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );
	
		// Flush the RX FIFO.
		while((SSP_SR & SSP_SR_RNE_MASK) == SSP_SR_RNE) ldummy = SSP_DR;
	
		while( pdh.dlen > 0 )
		{
			// Take advantage of the SSP 8 byte FIFO.
			for( iii=0; ((iii<8) && (pdh.dlen>0)); iii++ )
			{
				SSP_DR  = 0x00;  // send a dummy byte to cause the read
				pdh.dlen -= 1;
			}
	
			// Use the SSP hardware to determine when xmission is done.
			while( (SSP_SR & SSP_SR_BSY_MASK) == SSP_SR_BSY );
	
			while( iii>0 )
			{
				*(pdh.dptr++) = SSP_DR;
				iii -= 1;
			}
		}
		
		DISABLE_TRANSMISSION;	// Disable the SSP
		CHIP_SELECT_HIGH;		// Drive chip select high.

		// So now if the power fail went TRUE since we started the read set this flag to indicate that.
		if( restart_info.SHUTDOWN_impending == TRUE ) {
			// This may not be super precisely accurate about are we ACTUALLY reading but at this time it is more a curiousity issue than anything
			// if we were reading from the flash when an impending power fail was detected.
			restart_info.we_were_reading_spi_flash_data = TRUE;  
		}
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

