//	
//  Description: KICKSTART Top Level header file 
//
///////////////////////////////////////////////////////////////////////////////
#ifndef INC_KS_COMMON_H_
#define INC_KS_COMMON_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
INFORMATION ON CLASSIC SECTION NAMES
You will see these named in the linker output map but it's not obvious what they are.
This is a partial guide to the major section names you'll seems.

SECTION			LOCATION		COMMENT
.text			FLASH			code
.rodata			FLASH			initialized constants
.data			INTRAM			initialized variables (by setting them equal to something in their declaration)
.bss			INTRAM			initialized to 0 by compiler on startup (stands for BLOCK STARTING WITH SYMBOL)
*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// LPC3250 NOTE
// Only supported in GCC 4.4 or later. This is what we need to allow us to turn off the optimization on only a file.
// This would be used on all files. For the file we don't want optimization on we would set to another level. If we use
// this we should remove the optimization in the config.mk??
// 
// EXAMPLE: control level in a file with pragma
// #pragma GCC optimize ( "Os" )
//
// EXAMPLE: Function by function optimization attribute syntax. Needs GCC 4.4.4 or greater.
// __attribute__((__optimize__("0"))) void Function_Name( void )
//{
//}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


	//P3_OUTP_SET = P3_STATE_GPO( 14 );  // SCOPE MARKER - drives the marker LOW 


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/29/2014 rmd : This define is used primarily to change the behaviour of shared CS3000
// main_app code. To craft it more appropiately for the Kickstart. As opposed to holding
// full duplicates of the code in the kickstart directories. I think it is not a good idea
// to broadly use the #ifdef compiler directive as it makes the code cluttered and hard to
// read. So as we reach a point of over-use one must decide to finally include of copy of
// the code into the kickstart directories.
//
// NOTE - this is defined as a CROSSWORKS preprocessor definition.
//
//#define		KICKSTART_CODE_GENERATION

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Important defines that control low level behaviour of the memory partitioning code, the list code, and alert subsystem.

// provides malloc debugging capabilities found in mem.c and mem.h - test
// dynamically allocated memory for overruns, underruns, free the same memory more than
// once and tries to bring out any use of memory already freed by filling it
//
// Change it to a 0 OR comment out the whole line (either way works)
#define MEM_DEBUG 0

#define LIST_DEBUG 1

// The following switch will grow the compiled code by some 5000 bytes! It causes the file names to be
// embedded into the code every time Alert_Error is called. The file name includes the entire DOS path -
// which we strip when we store it but the compiler puts it in! Doing your development in a "close to the
// root directory" help control the overhead for this run time convienience.
//
// Change it to a 0 OR comment out the whole line (either way works)
#define ALERT_ERROR_FILE_NAMES 0


// This one must have ALERT_ERROR_FILE_NAMES defined as well...it generates alert lines at each malloc and free call.
#define MEM_SHOW_ALLOCS_AND_FREES 0


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// these defines used to be in the config.mk make file ... I felt more comfortable seeing them
// here in cs_common.h ... this way when I encounter them in the source my SlickEdit editor knows
// about them

// IN THE KICKSTART ENVIRONMENT WE RUN WITH JUST THE STANDARD malloc AND free SO SET THIS TO A 0

#define	USE_CALSENSE_MEMORY_MANAGER		1


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// these defines used to be in the config.mk make file ... I felt more comfortable seeing them
// here in cs_common.h ... this way when I encounter them in the source my SlickEdit editor knows
// about them

#define	USE_CALSENSE_GPIO_INIT	1

#define	FREERTOS_USED			1									

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


// This define is to handle TRULY FATAL errors that are detected. TODO not yet defined!!!!!!!!!!!

#define FREEZE_and_RESTART_APP for(;;);


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<stdio.h>

#include	<stdlib.h>

#include	<string.h>

#include	<stdarg.h>

#include	<stdint.h>

#include	<assert.h>


#ifndef NO_DEBUG_PRINTF
	// Debug I/O support .. but does NOT redefine printf to the debug version debug_printf.
	// If you want to do that include __debug_stdio.h instead of __cross_studio.h
	#include <__cross_studio_io.h>
#endif




#include	"lpc32xx_chip.h"

#include	"lpc3250_chip.h"

#include	"lpc3200_intc.h"



#include	"lpc_types.h"

#include	"general_picked_support.h"


// FreeRTOS ... FreeRTOS.h in turn brings in FreeRTOSConfig.h. */
#include	"FreeRTOS.h"

#include	"task.h"

#include	"semphr.h"

#include	"queue.h"




#include	"gpio_setup.h"


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

size_t strlcpy(char *dst, const char *src, size_t siz);

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Data types
typedef unsigned char   uchar;
typedef unsigned short  ushort;
typedef unsigned long   ulong;
typedef unsigned short WORD;
typedef uchar BOOL;
typedef uchar BOOLEAN;
typedef WORD	MIST_RET_CODE_TYPE;

// This next typedef will probably go away. It's commented out for now.
// typedef ulong   MIST_FSM_ID_TYPE;

#define MIST_SUCCESS				0x0000
#define MIST_RESOURCE_IN_USE		0x0003
#define MIST_RESOURCE_BUSY			0x0004
#define MIST_RESOURCE_FULL			0x0005
#define MIST_INVALID_PARAMETER		0x0006
#define MIST_INVALID_HANDLE			0x000C
#define MIST_UNABLE					0x0012
#define MIST_RESOURCE_NOT_ENABLED	0x001E

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* Calsense-specific */
#include	"cs_mem_part.h"

#include	"cs_mem.h"

#include	"cal_list.h"


/* ---------------------------------------------------------- */

// In the kickstart when we build the RELEASE version we might as well just NULL out the whole ALert_Message and Alert_Error functions.

#define Alert_error_normal( a, b ) Alert_Error( a, b )

#define RemovePathFromFileName( name ) name

#define Alert_Error( a,  b ) Alert_Message( a )


#ifdef NO_DEBUG_PRINTF

	//#define Alert_Message( a ) for(;;)

	#define Alert_Message( a )

	#define Alert_Message_va( a, ... )

	#define debug_printf( a, ... )

#else

	void Alert_Message( char *pmessage ); 
		
	#define Alert_Message_va( a, ... )
	
#endif



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// useful when making delays or setting queuue wait times to something meaningful

#define MS_to_TICKS( ms )		((ms)/portTICK_RATE_MS)   

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*              CRITICAL SECTION HANDLING                     */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// THERE IS A CONCERN: The following statement can be found in the documentation "FreeRTOS API
// functions should not be called while the scheduler is suspended." To address that read the
// following: RE: vTaskSuspendAll By: Richard (richardbarry ) - 2009-01-22 23:24 There is no problem
// with interrupts attempting to yield while the scheduler is suspended, or interrupts reading and
// writing to queues/semaphores. The scheduler takes care of these situations. If an interrupts
// attempts to yield then the yield is held pending until the scheduler is resumed. Pending yields
// are actually performed within xTaskResumeAll().

// We suspend the scheduler to protect the memory pool from corruption that would occur when multiple tasks are trying to
// take or give a block. There are TWO rules to follow:
//
// 1. While the scheduler is suspended there may be no calls to most FreeRTOS API functions. For example taking or giving a
// mutex. The fallout of this is NO alert lines within the suspended sections. We have done a good job to minimize those
// sections and they contain no alert lines.
//
// 2. ISR's may not call mem_malloc or mem_free. That would cause re-entrancy problems which could certainly lead to
// corruption. Don't do it.

#define calsense_mem_ENTER_CRITICAL() vTaskSuspendAll()

#define calsense_mem_EXIT_CRITICAL() xTaskResumeAll()

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// LPC3250 INTERRUPT PRIORITIES
//
// Interrupt priorities are manifested in software. There is no prioritization mechanism within the hardware. With the
// exception that an FIQ is a higher priority than an IRQ. More on that later. Interrupt do not nest.
// 
// The lower the priority number the higher the priority of processing. Range 0 .. MAX_INTERRUPTS.
// But using MAX_INTERRUPTS as a priority level is questionable as it brings in the FIQ story.
//
// MUST BE A UNIQUE PRIORITY LEVEL FOR EACH GUY. THE LOWER THE NUMBER THE HIGHER THE PRIORITY.

#define INTERRUPT_PRIORITY_powerfail_warning	0

#define INTERRUPT_PRIORITY_speaker_timer		1

#define INTERRUPT_PRIORITY_free_rtos_tick		2

#define INTERRUPT_PRIORITY_keypad_scanner		3

#define INTERRUPT_PRIORITY_usb_dev_hp			4
#define INTERRUPT_PRIORITY_usb_dev_lp			5

#define INTERRUPT_PRIORITY_uart_1				6
#define INTERRUPT_PRIORITY_uart_3				7
#define INTERRUPT_PRIORITY_uart_4				8
#define INTERRUPT_PRIORITY_uart_5				9
#define INTERRUPT_PRIORITY_uart_6				10

#define INTERRUPT_PRIORITY_utility_mstimer		11

// Though the interrupt is setup we don't really take any action when the int occurs.
#define INTERRUPT_PRIORITY_usb_bd				12

#define INTERRUPT_PRIORITY_dac_i2c_channel		13

#define INTERRUPT_PRIORITY_epson_i2c_channel	14

#define INTERRUPT_PRIORITY_epson_1hz			15


#define INTERRUPT_PRIORITY_port_a_cts			16

#define INTERRUPT_PRIORITY_port_b_cts			17

#define INTERRUPT_PRIORITY_port_rre_cts			18


/*

Table to use when setting the TRIGGER_TYPE

LOW LEVEL		APR=0	ATR=0

HIGH LEVEL		APR=1	ATR=0

RISING EDGE		APR=1	ATR=1

FALLING EDGE	APR=0	ATR=1

*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*               TIMERS USED THROUGHTOUT SYSTEM               */
/* ---------------------------------------------------------- */

#define FreeRTOS_TICK_TIMER						TIMER_CNTR5
#define FreeRTOS_TICK_INT						Timer5_INT

#define SPEAKER_TIMER							TIMER_CNTR4
#define SPEAKER_TIMER_INT						Timer4_Mcpwm_INT
#define SPEAKER_TIMER_CLK_EN					CLKPWR_TIMER4_CLK

#define FreeRTOS_RUN_TIME_STATS_TIMER			TIMER_CNTR3
#define FreeRTOS_RUN_TIME_STATS_TIMER_CLK_EN	CLKPWR_TIMER3_CLK

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

