// file = app_startup.c                      01.24.2011  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"app_startup.h"

#include	"serport_drvr.h"

#include	"rcvd_data.h"

#include	"packet_drvr.h"

#include	"packet_drvr.h"

#include	"spi_flash_driver.h"

#include	"flash_storage.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Because we don't have a main.h proto this here.
void MAIN_look_for_app_task( void *pvParameters );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Our plan is to ALWAYS swap out the flop registers for all task context switches. The alternative would be to pick and choose which tasks
// are floating point cable and which are not. I find that error prone. The reason I prefer to make all tasks FLOP capable is it would be too
// easy years from now for someone to ADD a floating point operation to a task that did not have it's FLOP register array set up. And this
// error could go undetected for a long time. Until there was context switch in the middle of the FLOP operation. Then the math could go very
// wrong if the task it switched into had a FLOP register storage array.


// The DFP context switch registers for the internal to the RTOS timer task. Timer callback functions may never do any
// floating point operations but with these the stage is set to do so.
unsigned long timer_task_FLOP_registers[ portNO_FLOP_REGISTERS_TO_SAVE ];

// Buffers into which the flop registers will be saved.  There is a buffer for each task created.  Zeroing out this array is the normal and
// safe option as this will cause the task to start with all zeros in its flop context.
static unsigned long task_table_FLOP_registers[ NUMBER_OF_TABLE_TASKS ][ portNO_FLOP_REGISTERS_TO_SAVE ];


// We need a task handle when setting the pointer to the flop registers for the task just created. Not all tasks need a handle in the task table. So
// instead of providing a unique handle to each task use this general purpose one. It is only good immediately after the task has been created. AND we
// have to make sure when that happens the scheduler isn't running or ELSE the function making all the tasks could be preempted and the may become
// invalid. This is remote but certainly possible. So call the handle bebecome qwe have a hanlde  

static xTaskHandle			general_purpose_FLOP_task_handle;

// And provide a task handle for the internally generated timer task (internal to the OS that is).
xTaskHandle		timer_task_handle;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

xSemaphoreHandle	Flash_0_MUTEX, Flash_1_MUTEX;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//////////////////////////////////////////////////////////////////////////////
//
// SYSTEM TASK TABLE
//
// Notes:
//
//   1. Setting the first column value to 1 or 0 enables or 
//      disables creation of that task.
//
//   2. For many tasks, the Task Parameter is the UART port used for
//      output or managed by the task. See some specific cases below.
//
//   3. To enable the serial port driver for UPORT_RRE you must
//      enable two tasks:
//        a) "SerDrvr RRE" with a task parameter set to 3 (uart port)
//        b) "SpiUartIRQ" (the task parameter is not used and can be NULL)
//
//      Additionally, you will need to enable a task, such as "SerDrvrTest" that 
//      has its task parameter set to use UPORT_RRE.
//
//   4. To enable the USB CDC driver for UPORT_USB, you must 
//      enable three tasks:
//
//        a) "SerDrvr USB" with a task parameter set to 4 (the uart port number)
//        b) "USB Enum" (the task parameter is not used and can be NULL)
//        c) "USB Dev CDC" (the task parameter is not used and can be NULL)
//
//////////////////////////////////////////////////////////////////////////////
TASK_ENTRY_STRUCT Task_Table[ NUMBER_OF_TABLE_TASKS ] =
{

	// 4/22/2014 rmd : NOTE - We discovered a VERY obscure yet deadly bug. If code distribution
	// packets are arriving (at that oh so fast rate that they do) during the boot process of
	// the kickstart program AND the kickstart sees a valid code file per the above code, a
	// condition develops where the unit sits unbooted in a corrupt state. One way I discovered
	// to help prevent this condition is to not create the THREE serial port related tasks till
	// after we've decided we need them. Afterall if the kickstart sees a valid code file in the
	// flash file system it will boot that and never needs to be concerned with the serial
	// ports. So that is what you see going on here with the first three tasks DISABLED. That is
	// intentional.


//	SORT THE TASK ORDER IN THIS TABLE BY PRIORITY
//												             															
//																														
//  CREATE 		TASK FUNCTION    			TASK NAME 				STACK DEPTH (32-BIT WORDS)				TASK PARAM.				TASKPRIORITY         				PTR TO TASK HANDLE
//											(31 chars + NULL)                                                               		[(configMAX_PRIORITIES-1) is the
//                  	                                                                                                    		 highest possible priority]
//

	// STACK:
	//
	// PRIORITY:
	//
	// DISABLED TILL AFTER FILE TEST IS MADE!
	{   0, 		ring_buffer_analysis_task,				"Ring Buf RRE",      	(512),				(void *)UPORT_RRE, 			tskIDLE_PRIORITY + 2, 		&general_purpose_FLOP_task_handle},


	// STACK:
	//
	// PRIORITY:
	//
	// DISABLED TILL AFTER FILE TEST IS MADE!
	{	0, 		vSerDrvrTask, 	 						"SerDrvr RRE",    		(512),				(void *)UPORT_RRE,			tskIDLE_PRIORITY + 2, 		&general_purpose_FLOP_task_handle},


	// STACK:
	//
	// PRIORITY:
	//
	// DISABLED TILL AFTER FILE TEST IS MADE!
	{   0, 		PACKET_rcvr_task,						"Packet Process",      	(512),							NULL, 			tskIDLE_PRIORITY + 1, 		&general_purpose_FLOP_task_handle},
	

	// STACK:
	//
	// PRIORITY:
	//
	// ENABLED NOW!
	{   1, 		MAIN_look_for_app_task,					"Main app task",  		(512),							NULL, 			tskIDLE_PRIORITY + 1, 		&general_purpose_FLOP_task_handle}

};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void CreateSystemSemaphores( void )
{
	vSemaphoreCreateBinary( rcvd_data_binary_semaphore[ UPORT_RRE ] );

	Flash_0_MUTEX = xSemaphoreCreateMutex();

	Flash_1_MUTEX = xSemaphoreCreateMutex();
}

/* ---------------------------------------------------------- */
void CreateSystemQueues( void )
{
	// Create an event queue for each serial port driver instance. In the kickstart we only have
	// one part we care about.
	serial_drvr.SerportDrvrEventQHandle = xQueueCreate( SERPORT_DRVR_EVENT_QSIZE, sizeof( SERPORT_EVENT_DEFS ) );

	// Create the incoming packet processing queue. How big does it need to be?
	KS_IN_event_queue = xQueueCreate( 20, sizeof( KS_INBOUND_PACKET_QUEUE_STRUCT ) );
}

/* ---------------------------------------------------------- */
extern void process_task_table( void )
{
	UNS_32	n;
	
	TASK_ENTRY_STRUCT	*pTaskEntry;

	pTaskEntry = (TASK_ENTRY_STRUCT*)&Task_Table;

	for( n=0; n<sizeof(Task_Table)/sizeof(TASK_ENTRY_STRUCT); n++, pTaskEntry++ )
	{
		if( n >= NUMBER_OF_TABLE_TASKS)
		{
			// IF YOUR CODE GETS TO HERE YOU HAVE TOO MANY TASKS IN THE TABLE. NEED TO INCREASE THE MAXIMUM_NUMBER_OF_TABLE_TASKS
			for(;;);
		}

		if( pTaskEntry->pTaskHandle == NULL )
		{
			// IF YOUR CODE GETS TO HERE YOU DIDN'T SET THE TASK HANDLE IN THE TABLE - AND YOU WON'T GET THE DESIRED RESULTS FROM THE FLOP REGISTER SWAP   
			for(;;);
		}

		if( pTaskEntry->bCreateTask == TRUE )
		{
			// Create the task for this entry
			xTaskCreate( pTaskEntry->pTaskFunc,
						 (signed char *) pTaskEntry->TaskName,
						 pTaskEntry->StackDepth,
						 pTaskEntry->pParameters,
						 pTaskEntry->Priority,
						 pTaskEntry->pTaskHandle );

			// BE AWARE ... the task handle set in the TASK TABLE is a general purpose handle. Unless the task requires specific use of it's own handle. When using the
			// general purpose task handle a task cannot expect to use that handle later. Unless we specifically define one for that task.
			vTaskSetApplicationTaskTag( pTaskEntry->pTaskHandle, (void*)&task_table_FLOP_registers[ n ] );
		}
		else
		{
			continue;	// Skip this entry
		}
	}
}
	
/* ---------------------------------------------------------- */
extern void before_scheduler_runs_CreateSystemTasks( void )
{
	CreateSystemSemaphores();  // just a temporary way to get the queue created for the serial port task

	CreateSystemQueues();  // just a temporary way to get the queue created for the serial port task


	// Buffers into which the flop registers will be saved.  There is a buffer for each task created.  Zeroing out this
	// array is the normal and safe option as this will cause the task to start with all zeros in its flop context.
	memset( &task_table_FLOP_registers, 0x00, sizeof( task_table_FLOP_registers ) );


	process_task_table();
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

