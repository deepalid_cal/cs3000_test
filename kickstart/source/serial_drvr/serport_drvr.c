/*  file = serport_drvr.c                     02.09.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"serport_drvr.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

SERPORT_DRVR_TASK_VARS_s serial_drvr;

// These semaphores signal the ring buffer task to go ahead and take a look as some data has come in
xSemaphoreHandle		rcvd_data_binary_semaphore[ UPORT_MAX_PORTS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void postSerportDrvrEvent( UPORTS pport, SERPORT_EVENT_DEFS pevent )
{
	//	WARNING: THIS FUNCTION MUST NOT BE CALLED FROM AN INTERRUPT!

	// post the event using the queue handle for this serial port driver
	if( xQueueSendToBack( serial_drvr.SerportDrvrEventQHandle, &pevent, 0 ) != pdPASS )  // do not wait for queue space ... flag an error
	{
		Alert_Message( "SerPort queue overflow!" );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Local function prototypes
static void serdrvr_idle(int port, SERPORT_EVENT_DEFS event);				
static void serdrvr_tx_blk_in_prog(int port, SERPORT_EVENT_DEFS event);				

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void vSerDrvrTask( void *pvParameters )
{
	xQueueHandle		EvtQHandle;
	
	int					port;
	
	SERPORT_EVENT_DEFS	event;
	

	// passed parameter is the serial port number
	port = (int) pvParameters;
	
	standard_uart_init( UPORT_RRE );

	// A NOTE. In this application we only call this function once. At the startup of the task. We never re-initialize our serial ports. If you did
	// though and the xmit list was populated the following ListInit would lead to a memory leak. One should first clean the list, removing and
	// releasing the appropriate memory allocations. Then call this function if you wish.
	nm_ListInit( &(xmit_cntrl[ port ].xlist), offsetof( XMIT_CONTROL_LIST_ITEM, dl ) );  

	// Go directly to IDLE. The port is initialized and waiting for action.
	serial_drvr.SerportTaskState = UPORT_STATE_IDLE;

	// Get handle to our port's Event queue
	EvtQHandle = serial_drvr.SerportDrvrEventQHandle;
	
	while( EvtQHandle )  // Must be non-zero to be valid
	{
        if( xQueueReceive( EvtQHandle, &event, portMAX_DELAY ) )
		{

			//			CTS DISCOVERY
			//
			// It turns out the CTS COS via an interrupt is not too good when playing around with CTS manually...say
			// using a switch. The switch bounce actually can cause us to end up with the WRONG cts state. So the input
			// signal is at a level that should be considered "NOT OK" (FALSE) yet the SerDrvrVars_s[port].UartCtlLine_s.cts
			// value is TRUE. This means the read of the signal level was fooled by the switch bounce. And I've seen it
			// on the scope.
			//
			// Now in real life with a device hanging on the port I don't expect this to be a problem when a device
			// is wired in but it still is a bit unnerving.
			//
			// This so far only seems to be a problem with the RRE port which has the CTS signal fed in through a GPIO pin.
			// 
			// The UC3A on-board uarts do not exhibit the problem as I can see. But that may be a matter of timing. It
			// seems to reason that there is a CTS input waveform pattern that could do the same thing.
			//
			// So one solution is to read the actual level of CTS at various places outside the interrupts that the change of
			// states generate. I'm not in love with doing this. It seems it defeats the purpose of the interrupt.
			//
			// I'll tell you what I'm going to do. Because I don't think this will happen when devices are hooked up I've
			// decided not to do anything about this. But we'll keep our eyes out.
			//
			// I did however turn on the input glitch filter on the gpio cts input pin for UPORT_RRE.

			switch( serial_drvr.SerportTaskState )
			{
				/*
				case UPORT_STATE_DISABLED:
					serdrvr_disabled( port, event );
					break;
				*/
					
				case UPORT_STATE_IDLE:
					serdrvr_idle( port, event );				
					break;
					
				case UPORT_STATE_TX_IN_PROG:
					serdrvr_tx_blk_in_prog( port, event );				
					break;
					
				/*
				case UPORT_STATE_TX_CTS_WAIT:
					serdrvr_cts_wait( port, event );				
					break;
				*/
						
				default:
					break;
			}

        } 
		else
		{

			// Should not get here
			Alert_Message( "xQueueReceive error" );

        }

    }

    // Should never get here
	Alert_Message( "Event queue handle not valid!" );

}

/* ---------------------------------------------------------- */
static void serdrvr_idle( int port, SERPORT_EVENT_DEFS event)
{	

XMIT_CONTROL_LIST_ITEM *llist_item;
	
//char str_64[ 64 ];

	switch( event )
	{
		case UPEV_NEW_TX_BLK_AVAIL:
			// Check to see if a block is currently being sent..
			if( xmit_cntrl[ port ].now_xmitting == NULL )
			{
				// No, we are idle, so set up to transmit the block at the head of the list.
				// Get (but do not remove) the item at the head of the transmit list.
				llist_item = nm_ListGetFirst( &xmit_cntrl[ port ].xlist );

				// Make sure there's something to send
				if( llist_item != NULL )
				{
					//sprintf( str_64, "IDLE, CTS OK, blk avail event, %d", pListItem->Length );
					//Alert_Message( str_64 );

					xmit_cntrl[port].now_xmitting = llist_item;

					serial_drvr.SerportTaskState = UPORT_STATE_TX_IN_PROG;

					kick_off_a_block_by_feeding_the_uart( port );
				}
				else
				{
					// !!!!!! May want to uncomment. I only commentted out during Kickstart development because
					// single stepping made it show once in a while.
					// Alert_Message( "IDLE, CTS OK, blk avail event, NO BLOCK" );
				}
			}
			else
			{
				// A block is already being transmitted?
				Alert_Message( "During IDLE: transmission in progress!!" );
				
				// Change state - TODO: review this.
				serial_drvr.SerportTaskState = UPORT_STATE_TX_IN_PROG;
			}
			break;
	
			
	}  // of switch

}

/* ---------------------------------------------------------- */
static void serdrvr_tx_blk_in_prog( int port, SERPORT_EVENT_DEFS event)
{

	XMIT_CONTROL_LIST_ITEM	*llist_item, *lremoved_item;

	switch( event )
	{

		case UPEV_NEW_TX_BLK_AVAIL:	// We can ignore this event in this state
			break;
	
		case UPEV_TX_BLK_SENT:
			// Depending on the size of the blocks and the baud rate used (the nature of the transmissions) the TX_BLK_SENT pulled off the queue may
			// NOT be for the block we are currently transmitting. It would be for a block sent previously. BEFORE this one. So check that the length
			// is really 0 too.
			//
			// Check to make sure a block was being sent ... and it has completed.
			if( (xmit_cntrl[ port ].now_xmitting != NULL) && (xmit_cntrl[ port ].now_xmitting->Length == 0) )
			{
				/*
				if( XmitCtl_s[port].now_xmitting->Length != 0 )
				{
					Alert_Message( "done but not done" );
				}
				*/
				
				// Remove this block from the transmit list
				lremoved_item = nm_ListRemoveHead( &xmit_cntrl[port].xlist );
				
				// do sanity check, then free the block
				if( lremoved_item == xmit_cntrl[ port ].now_xmitting )
				{
					mem_free( lremoved_item->OriginalPtr );
					mem_free( lremoved_item );
				}
				else
				{
					Alert_Message( "SERDRVR: list discrepancy" );
				}
				
				// Indicate no block being transmitted
				xmit_cntrl[ port ].now_xmitting = NULL;
				
				// Is another block available to be sent?
				llist_item = nm_ListGetFirst( &xmit_cntrl[port].xlist );

				if( llist_item != NULL )
				{
					//sprintf( str_64, "XMITTING, another blk avail, %d", pListItem->Length );
					//Alert_Message( str_64 );

					// Yes, we've got another to send. So set up to transmit it
					xmit_cntrl[ port ].now_xmitting = llist_item;
					
					// unmask tx interrupt
					kick_off_a_block_by_feeding_the_uart( port );
				}
				else
				{
					// Change state
					serial_drvr.SerportTaskState = UPORT_STATE_IDLE;	
				}
			}
			break;

	}

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

