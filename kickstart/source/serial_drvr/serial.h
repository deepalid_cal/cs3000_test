/*  file = serial.h                           02.09.2010  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef INC_SERIAL_SERIAL_H
#define INC_SERIAL_SERIAL_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"ks_common.h"

#include	"crc.h"

#include	"packet_definitions.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The Calsense application uses a USB channel and 4 UART channels. We would be using the 4 standard UART's (3,4,5,6) but
// UART 4's TX line is consumed by the LCD controller. So we use one of the so called high speed UART's for the interface
// to the TP board. We use UART 1 for that.

#define NOT_USED_UPORT_TP		0			// LPC3250 UART 1	(a high speed 14X Clock UART)
#define NOT_USED_UPORT_A		1			// LPC3250 UART 3	(a standard 16X Clock UART)
#define NOT_USED_UPORT_B		2			// LPC3250 UART 6	(a standard 16X Clock UART)
#define UPORT_RRE   			3			// LPC3250 UART 5	(a standard 16X Clock UART)
#define NOT_USED_UPORT_USB		4			// USB CDC device	(USB stack)

#define UPORT_MAX_PORTS		(NOT_USED_UPORT_USB+1)

#define UPORT_ALL_FLOW_SENSE_PORTS	98	// this is used during the foalsense scan to ask the ping to go out all eligible ports
#define UPORT_INVALID				99	// this is used during the foal scan as a default setting for which port a controller is found on


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2/6/2014 rmd : We add 1024 to give a time margin in the form of buffer space. For the
// code to find the packet, extract the packet, crc the packet, and queue the packet. BEFORE
// the ongoing data stream wraps around and hits the preamble of the packet we're trying to
// find and pull out. The application does work with this margin as small as 256 bytes. So
// at 1024 we should have plenty of margin.
#define RING_BUFFER_SIZE	(SYSTEM_WIDE_LARGEST_AMBLE_TO_AMBLE_PACKET_SIZE + 1024)


// This limit is checked when adding TX data to a serial port. When working with the low level USB loopback test the
// incoming block (say 8K) is broken into the 64 byte chunks due to the end point definition. Now this loop back mode
// is not normally used but when it is the amount you can send will be limited by this number of 64 byte blocks.
#define MAX_QUEUED_BLOCKS	48

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
//
// UART_CTL_LINE_STATES_s - structure that holds current control line states
//
// These are all Active high (nonzero value means the signal is ACTIVE)
//
typedef struct
{
	unsigned char 	cts;			// Clear-to-send (input)
	unsigned char 	dsr;			// Data set ready (input)
	unsigned char 	ri;				// Ring indicator (input)
	unsigned char 	cd;				// Carrier Detect (input)
	unsigned char 	rts;			// RTS (output)
	unsigned char	dtr;			// DTR (output)
	unsigned char   reset;			// Reset (output)
} UART_CTL_LINE_STATE_s;

/* ---------------------------------------------------------- */
//
//  Set of configurable flags used to report the control
//  line states at the communications device connector.
//  For an input to the Main card, the line state read by the hardware
//  is XOR'd with the flag. Similarly, output control line states are
//  also XOR'd with their corresponding flag when reported.
//
//  So, a value here of zero causes no inversion, a value of
//  0xff causes a logical inversion (NOT).
//
typedef struct
{
	unsigned char 	cts;			// Clear-to-send (input)
	unsigned char 	dsr;			// Data set ready (input)
	unsigned char 	ri;				// Ring indicator (input)
	unsigned char 	cd;				// Carrier Detect (input)
	unsigned char 	rts;			// RTS (output)
	unsigned char	dtr;			// DTR (output)
	unsigned char   reset;			// Reset (output)

} UART_CTL_LINE_XORS_s;

/* ---------------------------------------------------------- */
typedef struct
{

	// ring_index is where we start examining for 500 series packets from
	unsigned short	packet_index;
	
	// 500 series packet hunting support
	unsigned short	ringhead1;
	unsigned short	ringhead2;
	unsigned short	ringhead3;
	unsigned short	ringhead4;
	
	unsigned short	datastart;
	
	unsigned short	packetlength;
	
	// AS WE ADD TO THIS STRUCTURE THE VARS NEED TO BE ADDED TO THE PACKET HUNT MODE ENABLE FUNCTION

} PACKET_HUNT_S;

/* ---------------------------------------------------------- */
typedef struct
{
	
	// data_index is where we start collecting the data to xfer from
	unsigned short	data_index;

	BOOLEAN			transfer_from_this_port_to_TP;
	BOOLEAN			transfer_from_this_port_to_A;
	BOOLEAN			transfer_from_this_port_to_B;
	BOOLEAN			transfer_from_this_port_to_RRE;
	BOOLEAN			transfer_from_this_port_to_USB;

	// AS WE ADD TO THIS STRUCTURE THE VARS NEED TO BE ADDED TO THE DATA HUNT MODE ENABLE FUNCTION

} DATA_HUNT_S;

/* ---------------------------------------------------------- */
typedef struct
{
	
	// string_index is where we start examining for the string
	unsigned short	string_index;

	// AS WE ADD TO THIS STRUCTURE THE VARS NEED TO BE ADDED TO THE STRINGS HUNT MODE ENABLE FUNCTION

} STRING_HUNT_S;

/* ---------------------------------------------------------- */
//
// UART_RING_BUFFER_s - UART Ring Buffer data structure
//                      One instance for each UART port
typedef struct
{
	unsigned char				ringbuffer[ RING_BUFFER_SIZE ];	// the actual ring buffer

	// ring_tail is where the next incoming byte from the UART is placed into the ring buffer
	volatile unsigned short		ringtail;						

	BOOLEAN						hunt_for_packets;
	BOOLEAN						hunt_for_data;
	BOOLEAN						hunt_for_strings;

    // variables used during the transport PACKET hunt
	PACKET_HUNT_S				ph;

	// variables used during port to port data transfer (used only during test)
	DATA_HUNT_S					dh;

	// variables used during string hunting during say menu driven radio programming or AT commands to some device
	STRING_HUNT_S				sh;

	volatile BOOLEAN			ph_tail_caught_index;		// set TRUE when this happens - (set within an interrupt - except for the USB channel)
	volatile BOOLEAN			dh_tail_caught_index;		// set TRUE when this happens - (set within an interrupt - except for the USB channel)
	volatile BOOLEAN			sh_tail_caught_index;		// set TRUE when this happens - (set within an interrupt - except for the USB channel)

} UART_RING_BUFFER_s;

/* ---------------------------------------------------------- */

typedef struct
{

	unsigned short	errors_overrun;
    
    unsigned short	errors_parity;
    
    unsigned short	errors_frame;

    unsigned short	errors_break;
    
    unsigned short	errors_fifo;

	unsigned long	total_rcvd_bytes;

	unsigned long	total_xmitted_bytes;

} UART_STATS_STRUCT;

/* ---------------------------------------------------------- */

typedef struct
{
    MIST_DLINK_TYPE		dl;				// for list control
    unsigned char		*From;
    unsigned char		*OriginalPtr;	// used when releasing the memory
    unsigned short		Length;

} XMIT_CONTROL_LIST_ITEM;

typedef struct
{
	MIST_LIST_HDR_TYPE		xlist;

	XMIT_CONTROL_LIST_ITEM	*now_xmitting;

} XMIT_CONTROL_STRUCT_s;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern char* const port_names[ UPORT_MAX_PORTS ];  // = { "Port TP", "Port A", "Port B", "Port RRE", "Port USB" };

extern XMIT_CONTROL_STRUCT_s xmit_cntrl[ UPORT_MAX_PORTS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

void kick_off_a_block_by_feeding_the_uart( UPORTS pport );

void uart_enable_tx_int_and_pump_a_byte_to_the_uart( UPORTS pport );


void standard_uart_init( UPORTS pport );


void AddCopyOfBlockToXmitList(UPORTS port, DATA_HANDLE pdh );



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

