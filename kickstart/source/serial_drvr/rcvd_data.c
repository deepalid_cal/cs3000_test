/*  file = rcvd_data.c                        2009.06.23  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"rcvd_data.h"

#include	"serial.h"

#include	"serport_drvr.h"

#include	"packet_drvr.h"

#include	"packet_definitions.h"

#include	"crc.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void RCVD_DATA_enable_packethunting( UART_RING_BUFFER_s *pptr_ring_buffer )
{
    // WHAT ABOUT TURING OFF INTERRUPT DURING THIS??? well its an atomic operation to reset the tail ... and the rest of em
	// well could the rcvd_data task run while we were doing this...I guess so cause this is likely called by another
	// task setting up the port for use
	taskENTER_CRITICAL();

	pptr_ring_buffer->ph.packet_index = 0;
	pptr_ring_buffer->ph.ringhead1 = 0;
	pptr_ring_buffer->ph.ringhead2 = 0;
	pptr_ring_buffer->ph.ringhead3 = 0;
	pptr_ring_buffer->ph.ringhead4 = 0;
	pptr_ring_buffer->ph.datastart = 0;
	pptr_ring_buffer->ph.packetlength = 0;

	// so no matter what mode we are entering we 0 the tail...we rarely change modes and it is acceptable that
	// other communications that may be going on at the time of a mode change won't survive
	pptr_ring_buffer->ringtail = 0;

	pptr_ring_buffer->hunt_for_packets = TRUE;

	taskEXIT_CRITICAL();

}

/* ---------------------------------------------------------- */
void check_ring_buffer_for_CalsenseTPL_packet( unsigned short pport )
{
	UART_RING_BUFFER_s		*ptr_ring_buffer_s;		// for convienance
	
	UNS_16					itc1, itc2, itc3, itc4;
	
	UNS_8					*tptr;
	
	UNS_16					plen, psri;
	
	DATA_HANDLE				dh;



	ptr_ring_buffer_s = &serial_drvr.UartRingBuffer_s;

    // loop around looking for either the pre or post amble until we strike the tail
    do {

		itc1 = ptr_ring_buffer_s->ph.packet_index;
        if ( itc1 == ptr_ring_buffer_s->ringtail ) return;

        itc2 = itc1+1; if ( itc2 == RING_BUFFER_SIZE ) itc2=0;
        if ( itc2 == ptr_ring_buffer_s->ringtail ) return;

        itc3 = itc2+1; if ( itc3 == RING_BUFFER_SIZE ) itc3=0;
        if ( itc3 == ptr_ring_buffer_s->ringtail ) return;

        itc4 = itc3+1; if ( itc4 == RING_BUFFER_SIZE ) itc4=0;
        if ( itc4 == ptr_ring_buffer_s->ringtail ) return;

        //    sprintf(buffer,"       _1=%02X _2=%02X _3=%02X _4=%02X \0",preamble._1,preamble._2,preamble._3,preamble._4);
        //    DFromPtr(buffer,0);
        //    sprintf(buffer,"       i1=%02X i2=%02X i3=%02X i4=%02X \0",itc1,itc2,itc3,itc4);
        //    DFromPtr(buffer,40);
        //    sprintf(buffer,"       b1=%02X b2=%02X b3=%02X b4=%02X \0",B_ringbuffer[itc1],B_ringbuffer[itc2],B_ringbuffer[itc3],B_ringbuffer[itc4]);
        //    DFromPtr(buffer,80);
        //    sprintf(buffer,"       index=%04d tail=%04d len=%04d         \0",B_ringindex,B_ringtail,B_packetlength);
        //    DFromPtr(buffer,120);
        //    WaitTillKey(1);

        if ( ptr_ring_buffer_s->ringbuffer[ itc1 ] == preamble._1 ) {
            if ( ptr_ring_buffer_s->ringbuffer[ itc2 ] == preamble._2 ) {
                if ( ptr_ring_buffer_s->ringbuffer[ itc3 ] == preamble._3 ) {
                    if ( ptr_ring_buffer_s->ringbuffer[ itc4 ] == preamble._4 ) {

                        ptr_ring_buffer_s->ph.ringhead1 = itc1;    /* head points to the preamble */
                        ptr_ring_buffer_s->ph.ringhead2 = itc2;
                        ptr_ring_buffer_s->ph.ringhead3 = itc3;
                        ptr_ring_buffer_s->ph.ringhead4 = itc4;

                        ptr_ring_buffer_s->ph.packetlength = 0;

                        ptr_ring_buffer_s->ph.datastart = itc4 + 1;
                        
						if ( ptr_ring_buffer_s->ph.datastart == RING_BUFFER_SIZE ) ptr_ring_buffer_s->ph.datastart = 0;

                        //            WaitTillKeyText("found complete pre-amble in B \0");
                    }
                }
            }
        }


        if ( ptr_ring_buffer_s->ringbuffer[ itc1 ] == postamble._1 ) {
            if ( ptr_ring_buffer_s->ringbuffer[ itc2 ] == postamble._2 ) {
                if ( ptr_ring_buffer_s->ringbuffer[ itc3 ] == postamble._3 ) {
                    if ( ptr_ring_buffer_s->ringbuffer[ itc4 ] == postamble._4 ) {


                        // check the preamble is still there. if not move on
						if ( (ptr_ring_buffer_s->ringbuffer[ ptr_ring_buffer_s->ph.ringhead1 ] == preamble._1) &&
                             (ptr_ring_buffer_s->ringbuffer[ ptr_ring_buffer_s->ph.ringhead2 ] == preamble._2) &&
                             (ptr_ring_buffer_s->ringbuffer[ ptr_ring_buffer_s->ph.ringhead3 ] == preamble._3) &&
                             (ptr_ring_buffer_s->ringbuffer[ ptr_ring_buffer_s->ph.ringhead4 ] == preamble._4) ) {


							//WaitTillKeyText("PRE-AMBLE STILL GOOD in B            \0");


							// 2/5/2014 rmd : Adjust so the length reflects the full packet length without the ambles.
							ptr_ring_buffer_s->ph.packetlength -= 4;

							if ( ptr_ring_buffer_s->ph.packetlength > (CS3000_MAX_AMBLE_TO_AMBLE_PACKET_SIZE - (2*sizeof(AMBLE_TYPE)) ) )
							{
								Alert_Message( "Packet too big" );
                            }
							else
							{
								// now we have a pre and post amble identified testthe CRC

                                /* allocate memory on the heap for this packet. When
                                 * transferred to IM using PacketToTransport the
                                 * transport layer then becomes responsible for releasing
                                 * this memory
                                 */

								dh.dlen = ptr_ring_buffer_s->ph.packetlength;

								dh.dptr = mem_malloc( dh.dlen );

                                /* get a copy of the memory block ptr (packet start) */
                                tptr = dh.dptr;

                                psri = ptr_ring_buffer_s->ph.datastart;    /* packet start ring index */

                                /* plen is just for debugging and can be done
                                 * away with when working
                                 */
                                plen = 0;
                                do {

									*tptr++ = ptr_ring_buffer_s->ringbuffer[ psri++ ];

                                    if( psri == RING_BUFFER_SIZE ) psri=0;

                                    plen++;

                                }while( psri!=itc1 );


                                if( plen != ptr_ring_buffer_s->ph.packetlength )
								{
									
									Alert_Message( "Packet lengths don't match" );
                                
								}
								else
								{
									// this is it!
									receive_packet( dh );
								}

								// 2009.07.27 rmd ... RTOS environment
								// Do not free the allocated packet memory here. All we have done is posted
								// a queue event which carries the data handle. A copy of the packet has not
								// yet been taken.

                            }  /* of packet size makes sense */

                        }  /* of if the preamble is still there */

                        // reset these to guarantee new packet start must be found
						ptr_ring_buffer_s->ph.ringhead1 = itc1;
						ptr_ring_buffer_s->ph.ringhead2 = itc1;
						ptr_ring_buffer_s->ph.ringhead3 = itc1;
						ptr_ring_buffer_s->ph.ringhead4 = itc1;

                    }  /* post amble check 4 */
                }  /* post amble check 3 */
            }  /* post amble check 2 */
        }  /* post amble check 1 */

        // each loop move index along
		if ( ++ptr_ring_buffer_s->ph.packet_index == RING_BUFFER_SIZE ) {

            ptr_ring_buffer_s->ph.packet_index = 0;

        }

		ptr_ring_buffer_s->ph.packetlength++;  // each loop makes the packet 1 bigger

    } while( TRUE );  // of do while loop

}

/* ---------------------------------------------------------- */
unsigned short __bytes_available_to_process( unsigned short process_end_us, unsigned short process_start_us, unsigned short pbuffer_size_us ) {

unsigned short rv;

	// this returns the number of new bytes in the ring buffer that have not yet been processed
	//
	// remember the tail points to a location the does NOT YET contain a byte to xfer
	// 			the start points to a location that DOES contain a byte to xfer
	//
	if ( process_start_us == process_end_us ) {
	
		rv = 0;
	
	} else
	if ( process_start_us < process_end_us ) {
	
		rv = process_end_us - process_start_us;
		
	} else {
		
		// in the presence of a wrap around
		//
		rv = (pbuffer_size_us - process_start_us) + process_end_us;
		
	}
	
	return( rv );
	
}

/* ---------------------------------------------------------- */
void ring_buffer_analysis_task( void *pvParameters )
{
    //  A FreeRTOS task TO SCAN THE RING BUFFER.

	int port;
	
	volatile UART_RING_BUFFER_s		*ptr_ring_buffer_s;		// for convience - easier code to read
	
	//char str_64[ 64 ];

    // passed parameter is really the serial port number
	port = (int) pvParameters;

	ptr_ring_buffer_s = &serial_drvr.UartRingBuffer_s;  // create a ptr to the ring buffer controlling struct

	// Zero the ENTIRE ring buffer structure.
	memset( (void*)ptr_ring_buffer_s, 0, sizeof(UART_RING_BUFFER_s) );


	RCVD_DATA_enable_packethunting( (UART_RING_BUFFER_s*) ptr_ring_buffer_s );


    // Take the semaphore once since it's created signaled
	xSemaphoreTake( rcvd_data_binary_semaphore[ port ], 0 );
	
	while( TRUE )
	{

		// Block waiting forever for the rcvd_data semaphore to become available. */
		if( xSemaphoreTake( rcvd_data_binary_semaphore[ port ], portMAX_DELAY ) == pdTRUE )
		{
			// don't need to test operational mode when looking if buffer as that is considered when setting the error
			if( ptr_ring_buffer_s->ph_tail_caught_index == TRUE )
			{

				ptr_ring_buffer_s->ph_tail_caught_index = FALSE;

				/*
				snprintf( str_64, sizeof(str_64), "%s : PH Tail caught INDEX", port_names[ port ] );
                Alert_Message( str_64 );
				*/
				for(;;);
				//debug_printf( "%s : PH Tail caught INDEX", port_names[ port ] );
            }


			//	HUNT FOR PACKETS
            if( ptr_ring_buffer_s->hunt_for_packets == TRUE )
			{

				check_ring_buffer_for_CalsenseTPL_packet( port );

            }

			// we can do both at the same time if needed - hunt for 'packets' and 'data to xfer' that is

			//
			//	ON THE DATA HUNT
			//
			/*
			if( ptr_ring_buffer_s->hunt_for_data == TRUE )
            {

				if( port == UPORT_RRE )
				{

					//xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_A );
					xfer_bytes_to_another_port( port, ptr_ring_buffer_s, UPORT_B );

                }

				// now this is an interesting delay...we are here because data has been rcvd...for each
				// byte we get a semaphore so we won't miss any data BUT if we keep checking for the transfer
				// to the other serial port we will rack up too many blocks on the serial port driver so
				// this just allows some bytes to build up before we compose another block to xfer
				//
				// too long however and could overrun the ring buffer
				//
				//vTaskDelay( MS_to_TICKS( 100 ) );

            }  // of if hunting for data
			*/

		}  // of waiting for the semaphore
		
	}  // of the while forever of the task

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

