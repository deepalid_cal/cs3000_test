/*  file = serial.c                           02.09.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"serial.h"

#include	"lpc32xx_uart.h"

#include	"lpc32xx_uart_driver.h"

#include	"lpc32xx_hsuart.h"

#include	"serport_drvr.h"

#include	"lpc32xx_intc_driver.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"lpc32xx_gpio_driver.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


/* Constants to setup and access the USART. */
#define serINVALID_QUEUE                  ( ( xQueueHandle ) 0 )
#define serHANDLE                         ( ( xComPortHandle ) 1 )
#define serNO_BLOCK                       ( ( portTickType ) 0 )

typedef struct
{
	void*			addr;

	unsigned long	mic_isr_number;
} UART_DEFS;

UART_DEFS const uinfo[ UPORT_MAX_PORTS ] =
{
	{ UART1,	IIR1_INT },		// UPORT_TP
	{ UART3,	IIR3_INT },		// UPORT_A
	{ UART6,	IIR6_INT },		// UPORT_B
	{ UART5,	IIR5_INT },		// UPORT_RRE
	{ NULL,		0 }				// UPORT_USB
};



char* const port_names[ UPORT_MAX_PORTS ] = { "Port TP", "Port A", "Port B", "Port RRE", "Port USB" };

// The transmit control structures. One for each serial port
XMIT_CONTROL_STRUCT_s xmit_cntrl[ UPORT_MAX_PORTS ];

UNS_32	rcvd_bytes;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void standard_uart_int_handler( UPORTS puport, UART_REGS_T *puart_base )
{
	// THIS is Jeopardy. It is also an ISR. (And not the TV show.)

	portBASE_TYPE			xTaskWoken;
	
	volatile UNS_32			jjj, iir_ul, llsr, lmsr, bytes_to_read_ul;
	
	UART_RING_BUFFER_s		*ptr_ring_buffer_s;		// for convenience - easier code to read

	SERPORT_EVENT_DEFS		event;

	XMIT_CONTROL_LIST_ITEM	*pListItem;
	

	// initialize if a higher priority task than us has been woken
	xTaskWoken = pdFALSE;

	// Determine the interrupt source
	iir_ul = (puart_base->iir_fcr & UART_IIR_INTSRC_MASK);
	
	// ONLY handle one level of priority PER INTERRUPT. I tried to loop reading the iir until it produced a 0x01. To no avail. Kept reading a 0x00.
	// Apparently not meant to work that way. The NXP code only handles one interrupt at a time. Then leaves the isr. So we will too.
	switch( iir_ul )
	{
		case UART_IIR_INTSRC_RXLINE:
			// The highest priority interrupt. Shows all the line errors. We keep stats on such. Read the LSR to clear.
			llsr = puart_base->lsr;
			break;

		case UART_IIR_INTSRC_RDA:
		case UART_IIR_INTSRC_CTI:
			// These are the 2nd highest priority interrupt. And means the rcv FIFO crossed it's trigger level OR some data arrived but not enough bytes
			// to reach the trigger level. And then the data stopped. In any case read the number of bytes in the FIFO.
			bytes_to_read_ul = (puart_base->rxlev & 0x7F);

			ptr_ring_buffer_s = &serial_drvr.UartRingBuffer_s;

			for( jjj=0; jjj<bytes_to_read_ul; jjj++ )
			{
				ptr_ring_buffer_s->ringbuffer[ ptr_ring_buffer_s->ringtail ] = puart_base->dll_fifo;

				ptr_ring_buffer_s->ringtail++;
				if( ptr_ring_buffer_s->ringtail == RING_BUFFER_SIZE )
				{
					ptr_ring_buffer_s->ringtail = 0x00;
				}

				rcvd_bytes++;
				
				// If the tail ever catches up to index we've over run the buffer so either make it bigger or check it more often. ONLY test
				// for the mode we are active in cause for the inactive modes the tail will always eventually catch the index. And Latch
				// the response if ever TRUE.
				if ( (ptr_ring_buffer_s->ph_tail_caught_index == FALSE) && (ptr_ring_buffer_s->hunt_for_packets == TRUE) )
				{
					ptr_ring_buffer_s->ph_tail_caught_index = (ptr_ring_buffer_s->ringtail == ptr_ring_buffer_s->ph.packet_index);
				}

			}

			// signal that data has arrived
			xSemaphoreGiveFromISR( rcvd_data_binary_semaphore[ puport ], &xTaskWoken );
			break;

		case UART_IIR_INTSRC_THRE:
			// This is the third level priority interrupt. And was caused by the THR becoming empty. Are there any more characters to transmit?

			// See if there's an active block
			pListItem = xmit_cntrl[ puport ].now_xmitting;

			if( pListItem != NULL )
			{
				// NOTE: CTS cannot go FALSE once inside the loop loading the TX FIFO. Well it can but we won't notice that quite yet. That is because
				// we are already within the ISR. And another interrupt is needed to set it FALSE.
				if( pListItem->Length > 0 )
				{
					// It seems the UART_LSR_THRE bit is practically worthless. By writing ONE byte in the TX FIFO the bit goes to a 0. Meaning there
					// is something to transmit. The is NO absolute indication of where you are in the TX FIFO. We have to assume that we are at 8 bytes.
					// Our trigger level. To be safe we will only write in 32 bytes. This also comes into play with CTS. As the receiving device must be
					// able to tolerate up to 32 bytes after it signals us to stop. We may need to make this a variable that controls how many we load.

					// push in up to 32 bytes more if available
					for( jjj=0; jjj<32; jjj++ )
					{
						if( pListItem->Length > 0 )
						{
							// write one byte to the FIFO
							puart_base->dll_fifo = *pListItem->From;

							pListItem->From++;

							pListItem->Length--;
						}
						else
						{
							break;  // from the for loop
						}
					}

					// Check if we are DONE with this block
					if( pListItem->Length == 0 )
					{
						// Post an EV_TX_BLK_SENT event to the serial driver for this port
						event = UPEV_TX_BLK_SENT;

						xQueueSendToBackFromISR( serial_drvr.SerportDrvrEventQHandle, &event, &xTaskWoken );
					}
				}  // of if there are bytes left
			}  // of if we are currently xmitting a block
			break;

		case UART_IIR_MODEM_STS:
			// The lowest priority interrupt. The Modem Status Register. But oh so important to us. CTS is in here!
			lmsr = puart_base->modem_status;
			break;

	}  // of the switch
		
	if( xTaskWoken == pdTRUE )
	{
		// This sets us up so that when we return from the interrupt we return to the highest priority task now ready.
		portYIELD_FROM_ISR();	
	}
}

/* ---------------------------------------------------------------- */
void uart5_isr( void )
{
	standard_uart_int_handler( UPORT_RRE, UART5 );
}

/* ---------------------------------------------------------- */
void standard_uart_init( UPORTS pport )
{
	UART_CFG_T		uart_cfg;

	UART_CONTROL_T	uart_setup;

	volatile UNS_32	tmp_ul;


	// This FUNCTION should ONLY be called for UPORT_A, UPORT_B, and UPORT_RRE. They are the standard uarts.
	// This FUNCTION is only valid for the LPC3250 UART's 3 through 6.
	if( pport == UPORT_RRE )
	{

		// Make sure interrupts are OFF for the channel in the MIC, enable the CLK, assign the interrupt vector
		xDisable_ISR( uinfo[ pport ].mic_isr_number );

		if( pport == UPORT_RRE )	// UPORT_RRE is on UART 5 55555555555555555555555555555
		{
			// set uart_cfg->uartnum for use in later functions
			uart_cfg.regptr = UART5;
			uart_cfg.uartnum = 2;  // for UART3: uartnum=0, UART4: uartnum=1, UART5: uartnum=2, UART6: uartnum=3

			clkpwr_clk_en_dis( CLKPWR_UART5_CLK, 1 );

			// assign the isr vector - DOES NOT ENABLE THE INTERRUPT
			xSetISR_Vector( IIR5_INT, INTERRUPT_PRIORITY_uart_5, ISR_TRIGGER_HIGH_LEVEL, uart5_isr, NULL );
		}


		// UART baud rate generator isn't used, so just set it to divider by 1
		uart_cfg.regptr->lcr |= UART_LCR_DIVLATCH_EN;
		uart_cfg.regptr->dll_fifo = 1;	// write to the DLL register
		uart_cfg.regptr->dlm_ier = 0;	// write to the DLM register
		uart_cfg.regptr->lcr &= ~UART_LCR_DIVLATCH_EN;


		//// TODO: we need to translate out ConfigTable data to the NXP structures used internally. For now just fake it.

		//uart_setup.baud_rate = 38400;
		uart_setup.baud_rate = 115200;
		//uart_setup.baud_rate = 57600;
		uart_setup.parity = UART_PAR_NONE;
		uart_setup.stopbits = 1;
		uart_setup.databits = 8;
		
		// SET THE baud rate, parity, stop bits, databits AND sets the clock mode to AUTOMATIC CLOCKING
		uart_setup_trans_mode( &uart_cfg, &uart_setup );


		uart_cfg.regptr->lcr &= ~UART_LCR_DIVLATCH_EN;  // make SURE we are pointed to the rbr and thr


		// BE CAREFUL of the iir_fcr register. It is listed as a WRITE ONLY. Reading this location actually will read the IIR.
        // set up fifo levels
		uart_cfg.regptr->iir_fcr = ( UART_FCR_RXFIFO_TL16 | UART_FCR_TXFIFO_TL8 );
        // enable the fifos
		uart_cfg.regptr->iir_fcr = ( UART_FCR_RXFIFO_TL16 | UART_FCR_TXFIFO_TL8 | UART_FCR_FIFO_CTRL | UART_FCR_FIFO_EN );
        // flush the fifos
		uart_cfg.regptr->iir_fcr = ( UART_FCR_RXFIFO_TL16 | UART_FCR_TXFIFO_TL8 | UART_FCR_FIFO_CTRL | UART_FCR_FIFO_EN | UART_FCR_TXFIFO_FLUSH | UART_FCR_RXFIFO_FLUSH );


		// Enable the appropriate interrupts for the UART in question. Remember the MIC interrupt for this UART device has been disabled so no interrupts
		// to the core will will actually occur. They could be made pending within the UART itself however?
		if( pport == UPORT_RRE )
		{
			uart_cfg.regptr->dlm_ier = ( UART_IER_RXLINE_STS | UART_IER_RDA | UART_IER_THRE );
		}

		// Clear any pending interrupts - ITS A GOOD IDEA. It is recommended to ALWAYS read the RBR after the FIFOs are flushed. (from manual)
		tmp_ul = uart_cfg.regptr->iir_fcr;
		tmp_ul = uart_cfg.regptr->dll_fifo;
		tmp_ul = uart_cfg.regptr->lsr;
		tmp_ul = uart_cfg.regptr->modem_status;


		// ENABLE INTERRUPT IN THE MIC FOR THIS CHANNEL
		xEnable_ISR( uinfo[ pport ].mic_isr_number );
	}
	else
	{
		Alert_Message( "UART INIT: port out of range." );
	}
}

/* ---------------------------------------------------------- */
void kick_off_a_block_by_feeding_the_uart( UPORTS pport )
{
	// This is probably one of the most likely spots in the code of the serial driver for something to go wrong. We are going to push a byte
	// from the currently transmitting block into the UART. We must guard against a UART interrupt have had occuring between when this function
	// was called and now that it is executing. Disable interrupts, check for uart space, and block validity.

	UART_REGS_T				*uart_ptr;

	XMIT_CONTROL_LIST_ITEM	*list_item_ptr; 

	SERPORT_EVENT_DEFS		levent;

	UNS_32					jjj;
	
	
	if( pport == UPORT_RRE )
	{
		uart_ptr = (UART_REGS_T*)uinfo[ pport ].addr;

		// the now_xmitting pointer is ONLY changed elsewhere within the task...not at the interrupt level ... so therefore is safe to reference here
		list_item_ptr = xmit_cntrl[ pport ].now_xmitting;

		if( list_item_ptr != NULL )
		{
			// protect the xmit control block from a UART TX interrupt occuring (which it could) and changing the block state
			xDisable_ISR( uinfo[ pport ].mic_isr_number );

			// The length could actually be zero at this point...how?...a TX interrupt occurred (from the previous block) between when the NEW now_xmitting
			// ptr was set and before we disabled interrupts here. And the block was 32 bytes or less. So it was consumed in one interrupt. That's all okay.
			// But we had best check here. So we don't unnecessarily issue another TX_BLK_SENT event to the queue.
			if( list_item_ptr->Length > 0 )
			{
				// What is the state of the THR FIFO. It is actually unknown. But what is the worst case. Worst case being that there is already data in it.
				// Our ISR trigger level interrupt fires at 8 bytes. And the ISR loads 32 bytes maximum into the FIFO. If that wrapped up a block. And the
				// driver processed that queue message and started this next block. All very quickly. We could be here with 40 bytes in the FIFO already!
				// A low baud rate would allow this to occur.
				//
				// We cannot use the THRE bit in the LSR register to tell us much. It goes LOW when there is a SINGLE byte in the FIFO. So in the blind we
				// fill the FIFO. We need to load enough data to guarantee we push the FIFO over the trigger point of 8 bytes. Otherwise we won't get our
				// TX interrupt to load the rest of a block. We have to cross that threshold. So I would say load 16 bytes maximum. That would push us to
				// a maximum of 56 bytes in the FIFO.
				// 
				// What about bunches of small packets queued up in the driver? Suppose we had 14 byte packets in the driver. TEN of them. Each one of them
				// would load in its entirety within this function. And issue the queue event to cause the next block to load. 10 14 
				// 52. UNLESS the driver was loaded with lots of small packets to send. Then we could process them fast enough such that we kept loading
				// them into the FIFO. One after another. As we in this function are issueing the queue event that says we are done with the previous block.
				// And that could cause us to OVERFLOW the FIFO. Being that we never really know where we are in the TX FIFO. The lower the baud rate the more
				// feasible this becomes.
				// 
				// A couple of things fall out of the scenarios just described. FIRST of all we need protection here against a UART interrupt occuring. Hence the
				// xDisable_ISR function surrounding our work here. A UART TX interrupt could occur as we may push the FIFO over the trigger and it may fall down
				// below just as we are executing this code. And the interrupt of course is diddling the same variables we are here. And SECONDLY we should wait
				// for the UART transmitter to be empty before starting the next block. To prevent the FIFO overflow. This delay would only occur before we prepare
				// to put the first bytes of the new packet into the UART. This will perform just fine. And may actually give us a small (and I mean
				// small) but detectable delay between packets. Waiting like that will prevent us from overflowing the TX FIFO in the small packet scenario..

				// WAIT till transmitter is DONE ... wait for both bits to be set (even though supposively you only need to wait for the TEMT bit)
				while( (uart_ptr->lsr & (UART_LSR_THRE | UART_LSR_TEMT)) != (UART_LSR_THRE | UART_LSR_TEMT) );

                // Well the transmitter is empty. We could push in 32 bytes. But I think I'll only do up to 16 maximum.
				// With the transmitter empty it actually takes 9 BYTES to cause the TX interrupt to fire. I think this is cause the first one goes into
				// the TX shift register immediately. Then the rest into the FIFO up to our trigger point of 8.
				for( jjj=0; jjj<16; jjj++ )
				{
					if( list_item_ptr->Length > 0 )
					{
						// write one byte to the FIFO
						uart_ptr->dll_fifo = *(list_item_ptr->From);

						list_item_ptr->From++;

						list_item_ptr->Length--;
					}
					else
					{
						break;  // from the for loop
					}
				}

				// Check if we are DONE with this block
				if( list_item_ptr->Length == 0 )
				{
					// Get the ISR back alive. Though actually not critical. If the TX interrupt happens (cause we crossed the threshold) no action will occur as the
					// block is at 0 length already.
					xEnable_ISR( uinfo[ pport ].mic_isr_number );

					// Post an EV_TX_BLK_SENT event to the serial driver for this port. We MUST do this in this function here and now. BECAUSE THERE MAY NOT BE
					// A TX INTERRUPT GENERATED. That's right. If we did not load enough bytes to cross the FIFO interrupt threshold (8 bytes) a TX interrupt will
					// not occur! And therefore it is up to us to signal to the driver (ourselves) that the block has been sent.
					levent = UPEV_TX_BLK_SENT;

					postSerportDrvrEvent( pport,  levent );
				}
				else
				{
					xEnable_ISR( uinfo[ pport ].mic_isr_number );
				}
			}
			else
			{
				xEnable_ISR( uinfo[ pport ].mic_isr_number );
			}
		}
	}
	else
	{
		Alert_Message( "UART KICK: port out of range." );
	}

}

/* ---------------------------------------------------------- */
void AddCopyOfBlockToXmitList( UPORTS pport, DATA_HANDLE pdh )
{
	XMIT_CONTROL_LIST_ITEM	*newlistitem_ptr;
	
	char					str_64[ 64 ];

	// Check if serial port driver is in a state able to accept new tx data blocks.
	// We do not add more tx data to its list if it's in the TX_DISABLED state.
	if( serial_drvr.SerportTaskState != UPORT_STATE_DISABLED )
	{
		// DO NOT allow 0 byte blocks onto the list ... why do so ... and what bad behaviors would this bring out?
		if( pdh.dlen == 0 )
		{
			snprintf( str_64, sizeof(str_64), "%s: trying to add a 0 byte block", port_names[ pport ] );
			Alert_Message( str_64 );
		}
		else
		// though there is no real limit (the heap partitions are the limit)...this is a sanity check
		if( xmit_cntrl[ pport ].xlist.count >= MAX_QUEUED_BLOCKS )
		{
			snprintf( str_64, sizeof(str_64), "%s: xmit list too long", port_names[ pport ] );
			Alert_Message( str_64 );
		}
		else
		{
			newlistitem_ptr = mem_malloc( sizeof( XMIT_CONTROL_LIST_ITEM ) );

			// allocates a new block for the data to xmit
            newlistitem_ptr->From = mem_malloc( pdh.dlen );
			newlistitem_ptr->OriginalPtr = newlistitem_ptr->From;
			newlistitem_ptr->Length = pdh.dlen;

			memcpy( newlistitem_ptr->From, pdh.dptr, pdh.dlen );

			// now add it to the end of the list
			nm_ListInsertTail( &xmit_cntrl[ pport ].xlist, newlistitem_ptr );

			// Post event to the serial driver event queue
			postSerportDrvrEvent( pport, UPEV_NEW_TX_BLK_AVAIL );
		}
	}
}

/* ---------------------------------------------------------- */
void term_dat_out( char *pstring )
{
	DATA_HANDLE dh;
	
	// Create and post some data to transmit
	dh.dptr = (unsigned char *) pstring;
	dh.dlen = strlen( (char*)pstring );
	
	AddCopyOfBlockToXmitList( UPORT_RRE, dh);  // remember this is for the program loader
}

void term_dat_out_crlf( UNS_8 *pstring )
{
	char	str_64[ 64 ];

	snprintf( str_64, sizeof(str_64), "%s\r\n", pstring );
	term_dat_out( str_64 );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

