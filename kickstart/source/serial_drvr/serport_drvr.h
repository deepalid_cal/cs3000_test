/*  file = serport_drvr.h                     02.09.2010  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef SERPORT_DRVR_H_
#define SERPORT_DRVR_H_


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"serial.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Serial port driver task priority
#define SERPORT_DRVR_TASK_PRI			(tskIDLE_PRIORITY + 1)

// Serial port driver task stack size
#define SERPORT_DRVR_TASK_STACK_SIZE	(configMINIMAL_STACK_SIZE+100)

// Serial port driver event queue size (# messages). At times we could queue alot of blocks. And each block queued adds an
// event to the queue. Plus the TX interrupt adds an event. So add 10 extra.
#define SERPORT_DRVR_EVENT_QSIZE		(MAX_QUEUED_BLOCKS + 10)

// CTS Time-out period - guards against CTS staying inactive too long (forever)
#define SERPORT_DRVR_CTS_TIMEOUT_MS		5000

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Serial Port Driver Event IDs
#define UPEV_NEW_TX_BLK_AVAIL		1
#define UPEV_TX_BLK_SENT			2
#define UPEV_CTS_COS				3
#define UPEV_CONFIG_REQ				4
#define UPEV_DISABLE_PORT			5
#define UPEV_CTS_TMR_EXPIRED		6

#define SERPORT_EVENT_DEFS			UNS_32

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Serial port driver task states ... it is important that the TX_DISABLED state
// is 0 on startup. We count on that so we can test that a port has at least been
// intialized once before we service an interrupt that could sneak in during startup.
// This is related to the spi uart right now but could be important for any port channel.
// DON'T CHANGE THESE VALUES.

#define UPORT_STATE_DISABLED		0
#define UPORT_STATE_IDLE			1
#define UPORT_STATE_TX_IN_PROG		2
#define UPORT_STATE_TX_CTS_WAIT		3

#define TX_MAX_STATES				4


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

//  This structure is used for each serial port driver instance (task)
typedef struct
{
	xQueueHandle						SerportDrvrEventQHandle;					// event queue handle

	UNS_32								SerportTaskState;							// Serial driver task state

	UART_RING_BUFFER_s					UartRingBuffer_s;							// Ring buffer structure

} SERPORT_DRVR_TASK_VARS_s;


// counting on this to be zeroed on startup ... which it is ... that's good so the task state starts out
// in the TX_DISABLED state
extern SERPORT_DRVR_TASK_VARS_s serial_drvr;

// These semaphores signal the ring buffer task to go ahead and take a look as some data has come in
extern xSemaphoreHandle rcvd_data_binary_semaphore[ UPORT_MAX_PORTS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Function prototypes

void vSerDrvrTask( void *pvParameters );

void postSerportDrvrEvent( UPORTS pport, SERPORT_EVENT_DEFS pevent );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*SERPORT_DRVR_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

