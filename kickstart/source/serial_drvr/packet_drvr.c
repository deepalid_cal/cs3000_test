/*  file = packet_drvr.c                      2011.03.07  rmd */


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"packet_drvr.h"

#include	"crc.h"

#include	"serial.h"

#include	"spi_flash_driver.h"

#include	"flash_storage.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

xQueueHandle	KS_IN_event_queue;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void Build_Up_And_Send_A_Simple_Command_Response( UNS_32 presponse_cmd )
{
	UNS_32					response_size;
	
	DATA_HANDLE				ldh;

	TO_COMMSERVER_MESSAGE_HEADER	tcsmh;

	UNS_8					*ucp;

	UNS_8					*start_of_crc;

	// ----------

	response_size = sizeof( AMBLE_TYPE ) +
					sizeof( TO_COMMSERVER_MESSAGE_HEADER ) +
					sizeof( CRC_TYPE ) +
					sizeof( AMBLE_TYPE );

	ldh.dptr = mem_malloc( response_size );
	ldh.dlen = response_size;

	ucp = ldh.dptr;
	
	*ucp++ = preamble._1;
	*ucp++ = preamble._2;
	*ucp++ = preamble._3;
	*ucp++ = preamble._4;

	// ----------

	start_of_crc = ucp;

	// ----------

	memset( &tcsmh, 0x00, sizeof(TO_COMMSERVER_MESSAGE_HEADER) );

	// 2/5/2014 rmd : This IS the response.
	tcsmh.mid = presponse_cmd;

	tcsmh.length = response_size;
	
	// 2/5/2014 rmd : In the kickstart world the from serial number and network id are
	// irrelevant.
	
	memcpy( ucp, &tcsmh, sizeof(TO_COMMSERVER_MESSAGE_HEADER) );
	
	// 11/13/2012 rmd : Bump ucp over the payload. Which is only the packet header in this case.
	// After bumping over payload points to where the crc goes.
	ucp += sizeof( TO_COMMSERVER_MESSAGE_HEADER );

	// ----------

	// 11/6/2012 rmd : Calculate the CRC. Which is returned in BIG ENDIAN format. Which is what
	// we need (see note in crc calc function). Then copy to ucp.
	CRC_BASE_TYPE	lcrc;
	
	lcrc = CRC_calculate_32bit_big_endian( start_of_crc, sizeof(TO_COMMSERVER_MESSAGE_HEADER) );
	
	memcpy( ucp, &lcrc, sizeof(CRC_BASE_TYPE) );
	
	// 11/13/2012 rmd : Adjust to now point to where the post amble goes.
	ucp += sizeof(CRC_BASE_TYPE);

	// ----------
	
	*ucp++ = postamble._1;
	*ucp++ = postamble._2;
	*ucp++ = postamble._3;
	*ucp   = postamble._4;

	// ----------
	
	// Packet is built. Send it!
	AddCopyOfBlockToXmitList( UPORT_RRE, ldh );

	// Free the memory used.
	mem_free( ldh.dptr );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2012.02.24 rmd : We are going to choose a spot half way through the SDRAM to start
// receiving the downloaded application code. All downloaded applications will be received
// and stored here.
#define		RECEIVED_PACKET_STORAGE_START	(EMC_DYCS0_BASE)
//#define		RECEIVED_PACKET_STORAGE_START	(EMC_DYCS0_BASE + 0x400000)

// 2012.02.24 rmd : As we prepare the blocks to write to the flash we use this temporary
// storage area. In this kickstart application this array falls into the on-chip SRAM memory
// area.
UNS_8		fspot[  4096 ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
void write_bootloader_to_spi_flash( UNS_32 psize )
{
	// Using the low level spi flash driver functions jam out the bootloader in binary form directly to the spi flash.
	// CODE ASSUMES WE ARE WRITING MORE THAN 4K!!!

	DATA_HANDLE		ldh;

	UNS_32			flash_addr;
	
	// Ptr to where the received data is stored.
	unsigned char	*where_from;
	
	// Always starts here.
	where_from = ((void*)RECEIVED_PACKET_STORAGE_START);	

	// For the first page we need to write the special stuff.
	fspot[0] = 0xDF;
	fspot[1] = 0x9B;
	fspot[2] = 0x57;
	fspot[3] = 0x13;
	fspot[4] = (UNS_8) ((psize >> 0) & 0xFF);
	fspot[5] = (UNS_8) ((psize >> 8) & 0xFF);
	fspot[6] = (UNS_8) ((psize >> 16) & 0xFF);
	fspot[7] = (UNS_8) ((psize >> 24) & 0xFF);

	memcpy( &fspot[8], where_from, 4088 );

	ldh.dlen = 4096;

	ldh.dptr = &fspot[0];

	Alert_Message( "Writing First bootloader packet...\r\n" );

    SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming( FLASH_INDEX_0_BOOTCODE_AND_EXE, 0, ldh );

	psize -= 4088;

	where_from += 4088;

    flash_addr = 4096;

	while( psize > 0 )
	{
		if( psize >= 4096 )
		{
			ldh.dlen = 4096;

			ldh.dptr = where_from;

			Alert_Message( "Writing full bootloader packet...\r\n" );

			SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming( FLASH_INDEX_0_BOOTCODE_AND_EXE, flash_addr, ldh );

            psize -= 4096;

			flash_addr += 4096;

			where_from += 4096;
		}
		else
		{
			ldh.dlen = psize;

			ldh.dptr = where_from;

			Alert_Message( "Writing last bootloader packet...\r\n" );

			SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming( FLASH_INDEX_0_BOOTCODE_AND_EXE, flash_addr, ldh );

			psize = 0;  // all done!
		}

	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	MSG_INITIALIZATION_STRUCT	imis;
	
	UNS_32		next_packet_due_in;

	UNS_32		bytes_rcvd_so_far;

	UNS_8		*where_next_payload_goes;
	
} KS_MESSAGE_CONTROL_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void _check_crc_and_process_inbound_message( KS_MESSAGE_CONTROL_STRUCT *pksmcs )
{
	// 2/5/2014 rmd : Got to check the CRC.
	CRC_BASE_TYPE	lcrc;
	
	FILE_SAVE_STRUCT	lfss;
			
	// ----------

	lcrc = CRC_calculate_32bit_big_endian( (UNS_8*)RECEIVED_PACKET_STORAGE_START, pksmcs->imis.expected_size );
	
	if( lcrc == pksmcs->imis.expected_crc )
	{
		if( pksmcs->imis.mid == MID_CODE_DISTRIBUTION_kickstart_data_packet )
		{
			// And do what???? Write the byte count to the data flash. Not within the file system
			// however. We need to write the straight binary starting at page 0. But don't forget to
			// preceed the data with the 8 bytes that is the tag and size indicators for the resisdent
			// bootstrap program.
			write_bootloader_to_spi_flash( pksmcs->imis.expected_size );
	
			// Message is complete and saved. Send a positive response after the save. This is a small
			// file so the save is quick. On the order of 1 second or so.
			Build_Up_And_Send_A_Simple_Command_Response( MID_CODE_DISTRIBUTION_kickstart_full_receipt_ack );
		}
		else
		if( pksmcs->imis.mid == MID_CODE_DISTRIBUTION_main_data_packet )
		{
			lfss.const_file_name_ptr = CS3000_APP_FILENAME;
	
			lfss.file_size = pksmcs->imis.expected_size;
			
			lfss.file_buffer_ptr = ((void*)RECEIVED_PACKET_STORAGE_START);
			
			// 2012.02.29 rmd : The write to the flash file system for a 500K app takes approximately
			// 10 seconds. And this function will not return till the write has finished and we've
			// scoured and deleted from the file system the older version.
			FLASH_STORAGE_write_data_to_flash_file( FLASH_INDEX_0_BOOTCODE_AND_EXE, &lfss );
	
			// 2012.02.29 rmd : File has been saved to FLASH. Send a positive response. When saving the
			// CS3000_APP that can take up to 15 seconds. Sending this response AFTER the save has
			// completed allows the bootloader program to know it is safe to begin sending another file.
			Build_Up_And_Send_A_Simple_Command_Response( MID_CODE_DISTRIBUTION_main_full_receipt_ack );
		}
		else
		if( pksmcs->imis.mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet )
		{
			lfss.const_file_name_ptr = TPMICRO_APP_FILENAME;
	
			lfss.file_size = pksmcs->imis.expected_size;
			
			lfss.file_buffer_ptr = ((void*)RECEIVED_PACKET_STORAGE_START);  // Start at the beginning.
			
			// 2012.02.29 rmd : The write to the flash file system for a 500K app takes approximately
			// 10 seconds. And this function will not return till the write has finished and we've
			// scoured and deleted from the file system the older version.
			FLASH_STORAGE_write_data_to_flash_file( FLASH_INDEX_0_BOOTCODE_AND_EXE, &lfss );
	
			// 2012.02.29 rmd : File has been saved to FLASH. Send a positive response. When saving the
			// CS3000_APP that can take up to 15 seconds. Sending this response AFTER the save has
			// completed allows the bootloader program to know it is safe to begin sending another file.
			Build_Up_And_Send_A_Simple_Command_Response( MID_CODE_DISTRIBUTION_tpmicro_full_receipt_ack );
		}
	}
	else
	{
		Alert_Message( "Full Message CRC Failed!\r\n" );
	}
}

/* ---------------------------------------------------------- */
void PACKET_rcvr_task( void *pvParameters )
{
	KS_INBOUND_PACKET_QUEUE_STRUCT		kipqs;

	FROM_COMMSERVER_PACKET_TOP_HEADER	fcpth;

	KS_MESSAGE_CONTROL_STRUCT			ksmcs;

	MSG_DATA_PACKET_STRUCT				imdps;

	UNS_8								*ucp_from;

	UNS_32								payload_bytes;
	
	// ----------
	
	// 2/5/2014 rmd : Clear to force new initialization packet.
	memset( &ksmcs, 0x00, sizeof(KS_MESSAGE_CONTROL_STRUCT) );

	while( (TRUE) )
	{
		if( xQueueReceive( KS_IN_event_queue, &kipqs, portMAX_DELAY ) )
		{
			switch( kipqs.event )
			{
				case KS_IN_EVENT_here_is_an_inbound_packet:

					// ----------

					// 10/18/2012 rmd : Stamp so we know not to let the WDT timeout.
					restart_info.time_stamp_when_last_packet_arrived = restart_info.seconds_running;
					
					// ----------

					// 2/5/2014 rmd : Load the header.
					memcpy( &fcpth, kipqs.dh.dptr, sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) );
					
					// 2/5/2014 rmd : And point to the start of the packet specific content.
					ucp_from = kipqs.dh.dptr + sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER);
					
					// ----------

					if( (fcpth.PID.__routing_class == MSG_CLASS_2000e_CS3000_FLAG) && (fcpth.cs3000_msg_class == MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION) )
					{
						if( (fcpth.mid == MID_CODE_DISTRIBUTION_kickstart_init_packet) || (fcpth.mid == MID_CODE_DISTRIBUTION_main_init_packet) || (fcpth.mid == MID_CODE_DISTRIBUTION_tpmicro_init_packet) )
						{
							// 2/5/2014 rmd : Initialize for an incoming message.
							memcpy( &ksmcs.imis, ucp_from, sizeof(MSG_INITIALIZATION_STRUCT) );
							
							// ----------
							
							// 4/22/2014 rmd : Now we have to do a funny thing here. If we want to test when the data
							// packets arrive that we are getting packets for the same mid associated with this init
							// packet, we MUST flip the recorded mid. As that is what will be coming.
							if( fcpth.mid == MID_CODE_DISTRIBUTION_kickstart_init_packet )
							{
								// 4/23/2014 rmd : Respond to the program that sent us the originator packet.
								Build_Up_And_Send_A_Simple_Command_Response( MID_CODE_DISTRIBUTION_kickstart_init_ack );

								// 4/23/2014 rmd : Flip mid for coming data packets verification test.
								ksmcs.imis.mid = MID_CODE_DISTRIBUTION_kickstart_data_packet;
							}
							else
							if( fcpth.mid == MID_CODE_DISTRIBUTION_main_init_packet )
							{
								// 4/23/2014 rmd : Respond to the program that sent us the originator packet.
								Build_Up_And_Send_A_Simple_Command_Response( MID_CODE_DISTRIBUTION_main_init_ack );

								// 4/23/2014 rmd : Flip mid for coming data packets verification test.
								ksmcs.imis.mid = MID_CODE_DISTRIBUTION_main_data_packet;
							}
							else
							if( fcpth.mid == MID_CODE_DISTRIBUTION_tpmicro_init_packet )
							{
								// 4/23/2014 rmd : Respond to the program that sent us the originator packet.
								Build_Up_And_Send_A_Simple_Command_Response( MID_CODE_DISTRIBUTION_tpmicro_init_ack );

								// 4/23/2014 rmd : Flip mid for coming data packets verification test.
								ksmcs.imis.mid = MID_CODE_DISTRIBUTION_tpmicro_data_packet;
							}
							
							// ----------
							
							// 2/5/2014 rmd : The kickstart should only be receiving the kickstart code, the main code,
							// and the tp micro code messages. So all messages are indeed multiple packet messages. And
							// those messages do not contain further content within this INITIALIZATION packet. So
							// cleanup the other counters within the control struct and that's it.
							ksmcs.bytes_rcvd_so_far = 0;
	
							ksmcs.next_packet_due_in = 1;
							
							// 2/5/2014 rmd : Start storage from the start of SDRAM as intended.
							ksmcs.where_next_payload_goes = ((void*)RECEIVED_PACKET_STORAGE_START);
							
							// ----------
							
							if( kipqs.dh.dlen != (sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) + sizeof(MSG_INITIALIZATION_STRUCT) + sizeof(CRC_TYPE)) )
							{
								Alert_Message( "Unexpected Packet Size!\r\n" );
							}
						}
						else
						if( (fcpth.mid == MID_CODE_DISTRIBUTION_kickstart_data_packet) || (fcpth.mid == MID_CODE_DISTRIBUTION_main_data_packet) || (fcpth.mid == MID_CODE_DISTRIBUTION_tpmicro_data_packet) )
						{
							// 2/5/2014 rmd : Pull the packet header off.
							memcpy( &imdps, ucp_from, sizeof(MSG_DATA_PACKET_STRUCT) );
	
							// 2/5/2014 rmd : Advance the pointer to the payload.
							ucp_from += sizeof(MSG_DATA_PACKET_STRUCT);
							
							// 2/5/2014 rmd : Figure the payload amount.
							payload_bytes = ( kipqs.dh.dlen - sizeof(FROM_COMMSERVER_PACKET_TOP_HEADER) - sizeof(MSG_DATA_PACKET_STRUCT) - sizeof(CRC_TYPE) );
	
							if( (imdps.packet_number == ksmcs.next_packet_due_in) && (imdps.mid == ksmcs.imis.mid) )
							{
								// 2/5/2014 rmd : Looks like we're expecting this packet. So store it.
								memcpy( ksmcs.where_next_payload_goes, ucp_from, payload_bytes );
								
								// 2/5/2014 rmd : And update the byte rcvd accumulator.
								ksmcs.bytes_rcvd_so_far += payload_bytes;
								
								// ----------
	
								// 2/5/2014 rmd : If message has arrived go process.
								if( (ksmcs.imis.expected_packets == ksmcs.next_packet_due_in) && (ksmcs.imis.expected_size == ksmcs.bytes_rcvd_so_far) )
								{
									_check_crc_and_process_inbound_message( &ksmcs );
	
									// 2/5/2014 rmd : Clear to force new initialization packet.
									memset( &ksmcs, 0x00, sizeof(KS_MESSAGE_CONTROL_STRUCT) );
								}
								else
								{
									// 2/5/2014 rmd : Wasn't the last packet. More to come. Update accumulators to recieve more.
									ksmcs.next_packet_due_in += 1;
									
									ksmcs.where_next_payload_goes += payload_bytes;
								}
								
							}
							else
							{
								Alert_Message( "Unexpected Packet Number or MID.\r\n" );
	
								// 2/5/2014 rmd : Clear to force new initialization packet.
								memset( &ksmcs, 0x00, sizeof(KS_MESSAGE_CONTROL_STRUCT) );
							}
	
						}
						
					}
					else
					{
						Alert_Message( "Unexpected Packet Class.\r\n" );

						// 2/5/2014 rmd : Clear to force new initialization packet.
						memset( &ksmcs, 0x00, sizeof(KS_MESSAGE_CONTROL_STRUCT) );
					}

					mem_free( kipqs.dh.dptr );
					
					break;
			}

		}  // of a sucessful queue item is available

	}  // of forever task loop





}

/* ---------------------------------------------------------- */

UNS_32	_packets_rcvd, _bad_crcs_rcvd;

/* ---------------------------------------------------------- */
void receive_packet( DATA_HANDLE pdh )
{
	// 2/5/2014 rmd : Note - packet delivered without ambles. Length includes all headers,
	// payload, and CRC.
	KS_INBOUND_PACKET_QUEUE_STRUCT		kipqs;

	_packets_rcvd += 1;

	if( CRCIsOK( pdh ) == (true) )
	{
		kipqs.event = KS_IN_EVENT_here_is_an_inbound_packet;

		kipqs.dh = pdh;

		if( xQueueSend( KS_IN_event_queue, &kipqs, 0 ) != pdPASS )
		{
			mem_free( pdh.dptr );

			Alert_Message( "PACKET queue full\r\n" );
		}
	}
	else
	{
		_bad_crcs_rcvd += 1;

		mem_free( pdh.dptr );

		Alert_Message( "CRC failed\r\n" );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

