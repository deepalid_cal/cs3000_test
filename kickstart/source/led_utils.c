/* file = led_utils.c                        01.11.2011  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"led_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* LED 1(D15) bit mask  */
#define  partstPHYCORE3250_LED1_MASK	( 0x00000002 )

/* LED 2(D18) bit mask */
#define  partstPHYCORE3250_LED2_MASK	( 0x00004000 )

/* LED state registers. */
#define P3_OUTP_STATE_REG               ( *( volatile unsigned* ) 0x4002800C )
#define P3_OUTP_CLR_REG                 ( *( volatile unsigned* ) 0x40028008 )
#define P3_OUTP_SET_REG                 ( *( volatile unsigned* ) 0x40028004 )
#define MAX_USER_LED_NUMBER             ( 2 )
/*-----------------------------------------------------------
 * Simple parallel port IO routines.
 *-----------------------------------------------------------*/

void vTestSetLED( unsigned portBASE_TYPE uxLED, signed portBASE_TYPE xValue )
{
unsigned long ulLedMask = 0;

    if( uxLED <= MAX_USER_LED_NUMBER )
    {
        if( uxLED == 1 )
        {
            ulLedMask = partstPHYCORE3250_LED1_MASK;
        }
        else if( uxLED == 2 )
        {
            ulLedMask = partstPHYCORE3250_LED2_MASK;
        }
    
        /* Set of clear the output. */
        if( xValue )
        {
            P3_OUTP_CLR_REG |= ulLedMask;
        }
        else
        {
            P3_OUTP_SET_REG |= ulLedMask;
        }
    }
}
/*-----------------------------------------------------------*/

void vTestToggleLED( unsigned portBASE_TYPE uxLED )
{
unsigned long ulLedMask = 0;

    if( uxLED <= MAX_USER_LED_NUMBER )
    {
        if( uxLED == 1 )
        {
            ulLedMask = partstPHYCORE3250_LED1_MASK;
        }
        else if( uxLED == 2 )
        {
            ulLedMask = partstPHYCORE3250_LED2_MASK;
        }
    
        /* If this bit is already set, clear it, and visa versa. */
        if( P3_OUTP_STATE_REG & ulLedMask )
        {
            P3_OUTP_CLR_REG |= ulLedMask;
        }
        else
        {
            P3_OUTP_SET_REG |= ulLedMask;
        }
    }
}
/*-----------------------------------------------------------*/

unsigned portBASE_TYPE uxTestGetLED( unsigned portBASE_TYPE uxLED )
{
unsigned long ulLedMask = 0;

    if( uxLED <= MAX_USER_LED_NUMBER )
    {
        if( uxLED == 1 )
        {
            ulLedMask = partstPHYCORE3250_LED1_MASK;
        }
        else if( uxLED == 2 )
        {
            ulLedMask = partstPHYCORE3250_LED2_MASK;
        }
    }

	return ( P3_OUTP_STATE_REG & ulLedMask );
}
/*-----------------------------------------------------------*/

