@echo off

rem Create the directories to store the binary in. If it the directories already
rem exists, nothing happens. Since the kickstart.bin file is used for both the
rem Kickstart and the Bootloader (Stage 1), handle both at the same time.
mkdir "C:\CS3000\fw_upgrade\bin\kickstart\"
mkdir "C:\CS3000\fw_upgrade\bin\bootloader\"

rem Copy the binary from the Release folder to the newly created directories, 
rem overwriting the files if it already exists.
rem NOTE: The %~dp0 indicates the folder in which this batch file resides.
copy %~dp0"\ARM Release\kickstart.bin" "C:\CS3000\fw_upgrade\bin\kickstart\kickstart.bin" /y
copy %~dp0"\ARM Release\kickstart.bin" "C:\CS3000\fw_upgrade\bin\bootloader\kickstart.bin" /y

rem Start the CS3000 Firmware Upgrade Utility. Note the use of the Start command.
rem This executes the application in a separate window, immediately returning to
rem the batch file, effectively returning control back to Rowley CrossStudio when
rem this batch file is executed as part of the compilation process.
rem NOTE: The command will not execute properly if the quotes ("") between start
rem and the application are removed.
start "" "C:\CS3000\fw_upgrade\bin\FW_Upgrade.exe"
