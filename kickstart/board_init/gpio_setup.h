/* file = gpio_setup.h                        04.08.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_GPIO_SETUP_H
#define _INC_GPIO_SETUP_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"ks_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2011.08.10 rmd : I had to move the serial definitions here to compile. Getting some crazy circular references between
// TPL_OUT and SERIAL.H. This solved it. Functional but not pretty!

// The Calsense LPC3250 application uses a USB channel and 4 UART channels. We would be using the 4 standard UART's
// (3,4,5,6) but UART 4's TX line is consumed by the LCD controller. So we use one of the so called high speed UART's for
// the interface to the TP board. We use UART 1 for that.

// DO NOT change the order of these around unless you investigate thouroughly.

#define UPORT_TP						0			// LPC3250 UART 1	(a high speed 14X Clock UART)
#define UPORT_A							1			// LPC3250 UART 3	(a standard 16X Clock UART)
#define UPORT_B							2			// LPC3250 UART 6	(a standard 16X Clock UART)
#define UPORT_RRE   					3			// LPC3250 UART 5	(a standard 16X Clock UART)
#define UPORT_USB						4			// USB CDC device	(USB stack)
#define UPORT_VIRTUAL_M1				5			// RS-485 port on TP board
#define UPORT_VIRTUAL_M2				6			// RS-485 port on TP board

#define UPORT_TOTAL_PHYSICAL_PORTS		(UPORT_USB + 1)
#define UPORT_TOTAL_SYSTEM_PORTS		(UPORT_VIRTUAL_M2 + 1)

#define UPORTS							UNS_32

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// By mistake, in the schematic, we have TWO chip select for the U12 Flash device.
// When we do the rev we'll get rid of one of them. For now we'll name it with the ERROR in its name.
// And leave it as an INPUT during the setup so we won't disturb the functionality of the TRUE SEL_FLASH_1.
#define		CS_ERROR_NOT_USED_P00	P0_GPOP0_I2SRXCLK1

#define		CS_LED_RED				P0_GPOP1_I2SRXWS1
#define		CS_LED_YELLOW			P0_GPOP2_I2SRXSDA0
#define		CS_LED_GREEN			P0_GPOP3_I2SRXCLK0

#define		CS_NOT_USED_P04			P0_GPOP4_I2SRXWS0
#define		CS_NOT_USED_P05			P0_GPOP5_I2STXSDA0

// Two outputs from the LPC3250 as inputs to the MAX13345 USB transceiver.
#define		USB_ENUM				P0_GPOP6_I2STXCLK0
#define		USB_SUSPEND				P0_GPOP7_I2STXWS0



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		PORT_A_RESET_PIN		_BIT(P1_MUX_SET_P1_23_BIT)

#define		PORT_A_RESET_LOW		(P1_OUTP_CLR = PORT_A_RESET_PIN)
#define		PORT_A_RESET_HIGH		(P1_OUTP_SET = PORT_A_RESET_PIN)



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		PORT_A_RTS_PIN			_BIT( P3_OUTP_SET_GPO_23_BIT )
// NOTE: Port A DTR signal will not be routed to GPO_20 till Rev G of the pcb.
#define		PORT_A_DTR_PIN			_BIT( P3_OUTP_SET_GPO_20_BIT )

#define		WP_FLASH_1_PIN			_BIT( P3_OUTP_SET_GPO_17_BIT )

#define		LCD_BACKLIGHT_PIN		_BIT( P3_OUTP_SET_GPO_14_BIT )

#define		PORT_RRE_CONFIG_PIN		_BIT( P3_OUTP_SET_GPO_11_BIT )

#define		PORT_RRE_SLEEP_PIN		_BIT( P3_OUTP_SET_GPO_09_BIT )
#define		PORT_RRE_RESET_PIN		_BIT( P3_OUTP_SET_GPO_08_BIT )

#define		PORT_B_RESET_PIN		_BIT( P3_OUTP_SET_GPO_06_BIT )

#define		WP_FLASH_0_PIN			_BIT( P3_OUTP_SET_GPO_05_BIT )

#define		PORT_B_DTR_PIN			_BIT( P3_OUTP_SET_GPO_04_BIT )
#define		PORT_B_RTS_PIN			_BIT( P3_OUTP_SET_GPO_01_BIT )



#define	PORT_A_DTR_HIGH				(P3_OUTP_SET = PORT_A_DTR_PIN)
#define	PORT_A_DTR_LOW				(P3_OUTP_CLR = PORT_A_DTR_PIN)
#define	PORT_A_DTR_is_HIGH			((P3_OUTP_STATE & PORT_A_DTR_PIN) == PORT_A_DTR_PIN)
#define	PORT_A_DTR_is_LOW			((P3_OUTP_STATE & PORT_A_DTR_PIN) == 0)

#define	PORT_A_RTS_HIGH				(P3_OUTP_SET = PORT_A_RTS_PIN)
#define	PORT_A_RTS_LOW				(P3_OUTP_CLR = PORT_A_RTS_PIN)
#define	PORT_A_RTS_is_HIGH			((P3_OUTP_STATE & PORT_A_RTS_PIN) == PORT_A_RTS_PIN)
#define	PORT_A_RTS_is_LOW			((P3_OUTP_STATE & PORT_A_RTS_PIN) == 0)

#define	LCD_BACKLIGHT_ON			(P3_OUTP_SET = LCD_BACKLIGHT_PIN)
#define	LCD_BACKLIGHT_OFF			(P3_OUTP_CLR = LCD_BACKLIGHT_PIN)
#define	LCD_BACKLIGHT_is_ON			((P3_OUTP_STATE & LCD_BACKLIGHT_PIN) == LCD_BACKLIGHT_PIN)
#define	LCD_BACKLIGHT_is_OFF		((P3_OUTP_STATE & LCD_BACKLIGHT_PIN) == 0)

#define	PORT_RRE_CONFIG_HIGH		(P3_OUTP_SET = PORT_RRE_CONFIG_PIN)
#define	PORT_RRE_CONFIG_LOW			(P3_OUTP_CLR = PORT_RRE_CONFIG_PIN)
#define	PORT_RRE_CONFIG_is_HIGH		((P3_OUTP_STATE & PORT_RRE_CONFIG_PIN) == PORT_RRE_CONFIG_PIN)
#define	PORT_RRE_CONFIG_is_LOW		((P3_OUTP_STATE & PORT_RRE_CONFIG_PIN) == 0)

#define	PORT_RRE_SLEEP_HIGH			(P3_OUTP_SET = PORT_RRE_SLEEP_PIN)
#define	PORT_RRE_SLEEP_LOW			(P3_OUTP_CLR = PORT_RRE_SLEEP_PIN)
#define	PORT_RRE_SLEEP_is_HIGH		((P3_OUTP_STATE & PORT_RRE_SLEEP_PIN) == PORT_RRE_SLEEP_PIN)
#define	PORT_RRE_SLEEP_is_LOW		((P3_OUTP_STATE & PORT_RRE_SLEEP_PIN) == 0)

#define	PORT_RRE_RESET_HIGH			(P3_OUTP_SET = PORT_RRE_RESET_PIN)
#define	PORT_RRE_RESET_LOW			(P3_OUTP_CLR = PORT_RRE_RESET_PIN)
#define	PORT_RRE_RESET_is_HIGH		((P3_OUTP_STATE & PORT_RRE_RESET_PIN) == PORT_RRE_RESET_PIN)
#define	PORT_RRE_RESET_is_LOW		((P3_OUTP_STATE & PORT_RRE_RESET_PIN) == 0)

#define	PORT_B_RESET_HIGH			(P3_OUTP_SET = PORT_B_RESET_PIN)
#define	PORT_B_RESET_LOW			(P3_OUTP_CLR = PORT_B_RESET_PIN)
#define	PORT_B_RESET_is_HIGH		((P3_OUTP_STATE & PORT_B_RESET_PIN) == PORT_B_RESET_PIN)
#define	PORT_B_RESET_is_LOW			((P3_OUTP_STATE & PORT_B_RESET_PIN) == 0)

#define	PORT_B_DTR_HIGH				(P3_OUTP_SET = PORT_B_DTR_PIN)
#define	PORT_B_DTR_LOW				(P3_OUTP_CLR = PORT_B_DTR_PIN)
#define	PORT_B_DTR_is_HIGH			((P3_OUTP_STATE & PORT_B_DTR_PIN) == PORT_B_DTR_PIN)
#define	PORT_B_DTR_is_LOW			((P3_OUTP_STATE & PORT_B_DTR_PIN) == 0)

#define	PORT_B_RTS_HIGH				(P3_OUTP_SET = PORT_B_RTS_PIN)
#define	PORT_B_RTS_LOW				(P3_OUTP_CLR = PORT_B_RTS_PIN)
#define	PORT_B_RTS_is_HIGH			((P3_OUTP_STATE & PORT_B_RTS_PIN) == PORT_B_RTS_PIN)
#define	PORT_B_RTS_is_LOW			((P3_OUTP_STATE & PORT_B_RTS_PIN) == 0)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define	PORT_A_RI_PIN			P3_IN_STATE_GPI_28_U3RI

#define	PORT_RRE_CD_PIN			P3_IN_STATE_U7_RX_GPI_23

#define	PORT_RRE_CTS_PIN		P3_IN_STATE_U7_HCTS_GPI_22

#define	EPSON_FOUT				P3_IN_STATE_GPI_19_U4RX

#define	PORT_A_CTS_PIN			P3_IN_STATE_U2_HCTS

#define	EPSON_INT_B				P3_IN_STATE_GPI_07

#define	USB_DETECT_PIN			P3_IN_STATE_GPI_06

#define	PORT_A_CD_PIN			P3_IN_STATE_GPI_05

#define	EPSON_INT_A				P3_IN_STATE_GPI_04

#define	PORT_B_RI_PIN			P3_IN_STATE_GPI_03

#define	PORT_B_CD_PIN			P3_IN_STATE_GPI_02

#define	POWER_FAIL_INT			P3_IN_STATE_GPI_01

#define	PORT_B_CTS_PIN			P3_IN_STATE_GPI_00


#define	PORT_A_CTS_is_HIGH		((P3_INP_STATE & PORT_A_CTS_PIN) == PORT_A_CTS_PIN)
#define	PORT_A_CTS_is_LOW		((P3_INP_STATE & PORT_A_CTS_PIN) == 0)

#define	PORT_A_CD_is_HIGH		((P3_INP_STATE & PORT_A_CD_PIN) == PORT_A_CD_PIN)
#define	PORT_A_CD_is_LOW		((P3_INP_STATE & PORT_A_CD_PIN) == 0)

#define	PORT_A_RI_is_HIGH		((P3_INP_STATE & PORT_A_RI_PIN) == PORT_A_RI_PIN)
#define	PORT_A_RI_is_LOW		((P3_INP_STATE & PORT_A_RI_PIN) == 0)

#define	PORT_B_CTS_is_HIGH		((P3_INP_STATE & PORT_B_CTS_PIN) == PORT_B_CTS_PIN)
#define	PORT_B_CTS_is_LOW		((P3_INP_STATE & PORT_B_CTS_PIN) == 0)

#define	PORT_B_CD_is_HIGH		((P3_INP_STATE & PORT_B_CD_PIN) == PORT_B_CD_PIN)
#define	PORT_B_CD_is_LOW		((P3_INP_STATE & PORT_B_CD_PIN) == 0)

#define	PORT_B_RI_is_HIGH		((P3_INP_STATE & PORT_B_RI_PIN) == PORT_B_RI_PIN)
#define	PORT_B_RI_is_LOW		((P3_INP_STATE & PORT_B_RI_PIN) == 0)

#define	PORT_RRE_CTS_is_HIGH	((P3_INP_STATE & PORT_RRE_CTS_PIN) == PORT_RRE_CTS_PIN)
#define	PORT_RRE_CTS_is_LOW		((P3_INP_STATE & PORT_RRE_CTS_PIN) == 0)

#define	PORT_RRE_CD_is_HIGH		((P3_INP_STATE & PORT_RRE_CD_PIN) == PORT_RRE_CD_PIN)
#define	PORT_RRE_CD_is_LOW		((P3_INP_STATE & PORT_RRE_CD_PIN) == 0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define CS_LED_RED_ON		(P0_OUTP_SET = CS_LED_RED)
#define CS_LED_RED_OFF		(P0_OUTP_CLR = CS_LED_RED)

#define CS_LED_YELLOW_ON	(P0_OUTP_SET = CS_LED_YELLOW)
#define CS_LED_YELLOW_OFF	(P0_OUTP_CLR = CS_LED_YELLOW)

#define CS_LED_GREEN_ON		(P0_OUTP_SET = CS_LED_GREEN)
#define CS_LED_GREEN_OFF	(P0_OUTP_CLR = CS_LED_GREEN)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	SERVICE_N_is_HIGH	((P3_INP_STATE & P3_IN_STATE_GPI_01) == P3_IN_STATE_GPI_01)
#define	SERVICE_N_is_LOW	((P3_INP_STATE & P3_IN_STATE_GPI_01) == 0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

void gpio_setup( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

