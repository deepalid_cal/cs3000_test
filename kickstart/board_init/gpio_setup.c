/***********************************************************************
 * $Id:: gpio_setup.c 3376 2010-05-05 22:28:09Z usb10132               $
 *
 * Project: GPIO and MUX code
 *
 * Description:
 *     Provides MUX and GPIO setup for the board
 *
 ***********************************************************************
 * Software that is described herein is for illustrative purposes only  
 * which provides customers with programming information regarding the  
 * products. This software is supplied "AS IS" without any warranties.  
 * NXP Semiconductors assumes no responsibility or liability for the 
 * use of the software, conveys no license or title under any patent, 
 * copyright, or mask work right to the product. NXP Semiconductors 
 * reserves the right to make changes in the software without 
 * notification. NXP Semiconductors also make no representation or 
 * warranty that such application will be suitable for the specified 
 * use without further testing or modification. 
 **********************************************************************/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"gpio_setup.h"

#include	"lpc32xx_gpio.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


/***********************************************************************
 *
 * Function: gpio_setup
 *
 * Purpose: Setup GPIO and MUX states
 *
 * Processing:
 *     See function.
 *
 * Parameters: None
 *
 * Outputs: None
 *
 * Returns: Nothing
 *
 * Notes: Changes these as needed.
 *
 **********************************************************************/
void gpio_setup( void )
{
	GPIO_REGS_T		*Lgpio;
	
	Lgpio = (GPIO_REGS_T*)GPIO_BASE;


	// --------------------------------------
	// PORT 0

	// Configure the following all as GPIO as opposed to an I2S function.
	Lgpio->p0_mux_clr = ( CS_ERROR_NOT_USED_P00 | CS_LED_RED |CS_LED_YELLOW | CS_LED_GREEN | CS_NOT_USED_P04 | CS_NOT_USED_P05 | USB_ENUM | USB_SUSPEND );

	// Make the extra nSEL_FLASH_1 chip select an input...which it already is following a reset.
	Lgpio->p0_dir_clr = ( CS_ERROR_NOT_USED_P00 );

	// Make the following outputs ... ALL of them except P0.0 which is to be left as an input!
	Lgpio->p0_dir_set = ( CS_LED_RED |CS_LED_YELLOW | CS_LED_GREEN | CS_NOT_USED_P04 | CS_NOT_USED_P05 | USB_ENUM | USB_SUSPEND );

	// Set the FLASH CS high! And the GREEN led.
	Lgpio->p0_outp_set =( CS_LED_GREEN );

	// All others are LOW. USB_SUSPEND low for normal tranceiver operation. And the USB_ENUM line low to disconnect the pull-up from D+ to VTRM.
	Lgpio->p0_outp_clr = ( CS_LED_RED |CS_LED_YELLOW | CS_NOT_USED_P04 | CS_NOT_USED_P05 | USB_SUSPEND | USB_ENUM );

	// --------------------------------------
	// PORT 1

	// Muxing for address bus. Set first for the full 24-bit address bus on EMC_A[0..23]. Then configure for PortA_Reset.
	PORT_A_RESET_LOW;  // set to be low
	Lgpio->p1_dir_set = ( PORT_A_RESET_PIN );  // set to be an output
	
	Lgpio->p1_mux_clr = P1_ALL;  // all A0-A23 as address outputs
	Lgpio->p1_mux_set = ( PORT_A_RESET_PIN );  // and finally make PORT_A_RESET as a GPIO line
	
	// --------------------------------------------
	// PORT 2

	// THE FOLLOWING STRETCH OF CODE HAS BEEN DOUBLE AND TRIPLE CHECKED. IT LOOKS LIKE AN ERROR AS WE BOUNCE BACK AND FORTH BETWEEN THE
	// P2_MUX AND P2_DIR AND P3_OUTP_SET REGISTERS. BUT THIS IS THE WAY GPIO_00 THROUGH GPIO_05 ARE HANDLED.
	// 
	// GPIO_00		(not used in this design - has an external to chip pull down on it)  so make sure it is configured as an input
	// GPIO_01		(not used in this design - has an external to chip pull down on it)  so make sure it is configured as an input
	// GPIO_02		Configured as ROW_OUT_6
	// GPIO_03		Configured as ROW_OUT_7
	// GPIO_04		Configured as nSEL_FLASH_1
	// GPIO_05		Configured as nSEL_FLASH_0
	//
	// Configure these two pins as KEYSCAN ROWS 6 & 7
	Lgpio->p2_mux_set = (P2_GPIO03_KEYROW7 | P2_GPIO02_KEYROW6);

	// Make GPIO_05/SSEL0 as a GPIO pin (GPIO_05)...nSEL_FLASH_0
	// Make GPIO_04/SSEL1 as a GPIO pin (GPIO_04)...nSEL_FLASH_1
	// Connect Data 19 .. 31 to the SDRAM controller
	// Configure GPO_21 as GPO_21. However this is OVERRIDEN by the LCD controller and becomes LCDVD3
	Lgpio->p2_mux_clr = (P2_GPIO05_SSEL0 | P2_GPIO04_SSEL1 | P2_SDRAMD19D31_GPIO | P2_GPO21_U4TX );
	
	// IMPORTANT to set the desired level PRIOR to making them outputs. Otherwise the default following reset is a LOW. And these are chip selects.
	// So for about the 100ns it takes to set them HIGH after making them an output the CS line will be driven LOW! Set them HIGH first. Then switch
	// them to outputs to avoid the glitch.
	Lgpio->p3_outp_set = (P3_STATE_GPIO(4) | P3_STATE_GPIO(5));  // Drive GPIO_04 and 5 both high to deselect both serial flash devices.

	Lgpio->p2_dir_set = (P2_DIR_GPIO(4) | P2_DIR_GPIO(5));  // Set up GPIO_04 and 5 as outputs. This is the SSEL0 and SSEL1 lines (both as GPIO).

	Lgpio->p2_dir_clr = (P2_DIR_GPIO(0) | P2_DIR_GPIO(1));  // Set up GPIO_00 and 1 as inputs. (Not necessary to do this as this is the state upon RESET.)


	// Setup the initial state on a variety of output pins. They are outputs following a RESET. Make them all LOW. Which they are already (reset state).
	Lgpio->p3_outp_clr = ( WP_FLASH_1_PIN | LCD_BACKLIGHT_PIN | PORT_RRE_CONFIG_PIN | PORT_RRE_SLEEP_PIN | PORT_RRE_RESET_PIN | PORT_B_RESET_PIN | WP_FLASH_0_PIN | PORT_B_DTR_PIN | PORT_B_RTS_PIN );  



	// --------------------------------------
	// P_MUX port
	
	
	/* P_MUX_CLR register definition and setting in this design
	(MS_DIO0) | MAT0.0				no connect in this design - input on power up and pulled down
	(MS_DIO1) | MAT0.1				no connect in this design - input on power up and pulled down
	(MS_DIO2) | MAT0.2				no connect in this design - input on power up and pulled down
	(MS_DIO3) | MAT0.3				no connect in this design - input on power up and pulled down

	U7_TX / MAT1.1 | (LCDVD[11])	no connect in this design - pin left open - is an output H on power up

	SPI1_CLK / SCK0					configure as SCK0 by setting bit 12 in the SET register
	SPI1_DATIN / MISO0				configure as MISO0 by setting bit 10 in the SET register
	SPI1_DATIO / MOSI0				configure as MOSI0 by setting bit 9 in the SET register

	SPI2_CLK / SCK1					configure as SCK1 by setting bit 8 in the SET register
	SPI2_DATIN / MISO1				configure as MISO1 by setting bit 6 in the SET register
	SPI2_DATIO / MOSI1				configure as MOSI1 by setting bit 5 in the SET register

	I2S1TX_WS / CAP3.0				no connect in this design - input on power up and pulled down
	I2S1TX_CLK / MAT3.0				no connect in this design - may be an ouput set LOW on power up (and pulled down)
	I2S1TX_SDA / MAT3.1				no connect in this design - input on power up and pulled down
	*/

	// Set up SSP0 & SSP1 channels (Synchronous Serial Port).  That's all we have to do to this register.
	Lgpio->p_mux_set = (P_SPI1DATAIO_SSP0_MOSI | P_SPI1DATAIN_SSP0_MISO | P_SPI1CLK_SCK0 | P_SPI2DATAIO_MOSI1 | P_SPI2DATAIN_MISO1 | P_SPI2CLK_SCK1 );
	

	// --------------------------------------
	// P3_MUX_SET and P3_MUX_CLR

	/*
	GPO_18 / MC0A | (LCDLP)				in this design this becomes the LCDLP line to the LCD panel, no action necessary here
	GPO_16 / MC0B | (LCDENAB/LCDM)		no connect in this design - but the LCD does have control and is an output LOW on power up, no action necessary here
	GPO_15 / MC1A | (LCDFP)				in this design this becomes the LCDFP line to the LCD panel, no action necessary here
	GPO_13 / MC1B | (LCDDCLK)			in this design this becomes the LCDCLK line to the LCD panel, no action necessary here
	GPO_12 / MC2A | (LCDLE)				no connect in this design - but the LCD does have control and is an output LOW on power up, no action necessary here
	GPO_10 / MC2B | (LCDPWR)			in this design this becomes the LCDPWR line to the LCD panel, no action necessary here
	GPO_02 / MAT1.0 | (LCDVD[0])		in this design this becomes the LCDDVD_0 line to the LCD panel, no action necessary here

	And that is the end of the P3_MUX_CLR/SET registers. NO action is necessary. The LCD initialization takes precedence and sets all the lines listed above. 
	*/
}


