/***********************************************************************
 * $Id:: board_config.h 3388 2010-05-06 00:17:50Z usb10132             $
 *
 * Project: Board support package configuration options
 *
 ***********************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
 **********************************************************************/

#ifndef BOARD_CONFIG_H
#define BOARD_CONFIG_H


/* External static memory timings used for chip select 0 (see the users
   guide for what these values do). Optimizing these values will help
   with NOR program and boot speed. These should be programmed to work
   with the selected bus (HCLK) speed. */
#define EMCSTATICWAITWEN_CLKS  0xF
#define EMCSTATICWAITOEN_CLKS  0xF
#define EMCSTATICWAITRD_CLKS   0x1F
#define EMCSTATICWAITPAGE_CLKS 0x1F
#define EMCSTATICWAITWR_CLKS   0x1F
#define EMCSTATICWAITTURN_CLKS 0xF


#endif

