/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/


/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the ARM9 port.
 *
 * Components that can be compiled to either ARM or THUMB mode are
 * contained in this file.  The ISR routines, which can only be compiled
 * to ARM mode are contained in portISR.c.
 *----------------------------------------------------------*/


#include	"ks_common.h"


/* NXP LPC23xx library includes. */
#include	"lpc32xx_intc_driver.h"

#include	"lpc32xx_timer_driver.h"




/*-----------------------------------------------------------*/
/* Constants required to setup the task context. */

/* System mode, ARM mode, interrupts enabled. */
#define portINITIAL_SPSR							( ( portSTACK_TYPE ) 0x1F )
#define portTHUMB_MODE_BIT							( ( portSTACK_TYPE ) 0x20 )
#define portINSTRUCTION_SIZE                        ( ( portSTACK_TYPE ) 4 )
#define portNO_CRITICAL_SECTION_NESTING             ( ( portSTACK_TYPE ) 0 )
#define portTHUMB_MODE_BIT							( ( portSTACK_TYPE ) 0x20 )

/* Timer 0 Registers addresses. */
/*
#define portT0IR                                    ( *( volatile unsigned char* ) 0x40044000 )
#define portT0TCR                                   ( *( volatile unsigned char* ) 0x40044004 )
#define portT0PR                                    ( *( volatile unsigned long* ) 0x4004400C )
#define portT0MCR                                   ( *( volatile unsigned short* ) 0x40044014 )
#define portT0MR0                                   ( *( volatile unsigned long* ) 0x40044018 )
#define portTIMCLK_CTRL1                            ( *( volatile unsigned* ) 0x400040C0 )
*/

/* Timer 0 configuration parameters. */
/*
#define portTICK_TIMER_CLOCK_ENABLE  				( 0x00000004 )
#define portRESET_TIMER_COUNT  						( 0x02 )
#define portPRESCALE_DIVIDER						( 13 )
#define portHCLKDIV_CTRL                            ( 0x40004040 )
#define portPERIPH_CLK_DIVIDER						( ( portHCLKDIV_CTRL >> 2 ) & 0x1F ) 
#define portENABLE_MATCH0_INT 						( 0x01 )
#define portRESET_COUNT_ON_MATCH 					( 0x02 )
#define portCLEAR_PENDING_INT 						( 0x01 )
#define portENABLE_TIMER 							( 0x01 )
*/

/* ---------------------------------------------------------- */

// 01.27.2011 rmd
// For use in optimizing the macros that save and restore the FPU registers. If there isn't going to be
// a context switch then don't need those operations. Opted to keep the original Wittenstein code. And not attempt my
// optimization technique.

//xTaskHandle		xTaskThatWasRunning;

/*-----------------------------------------------------------*/

/* Setup the Timer 0 to generate the tick interrupts. */
static void prvSetupTimerInterrupt( void );

/*-----------------------------------------------------------*/

/*
 * Initialise the stack of a task to look exactly as if a call to
 * portSAVE_CONTEXT had been called.
 *
 * See header file for description.
 */
portSTACK_TYPE *pxPortInitialiseStack( portSTACK_TYPE *pxTopOfStack, pdTASK_CODE pxCode, void *pvParameters )
{
portSTACK_TYPE *pxOriginalTOS;

	pxOriginalTOS = pxTopOfStack;

	// 2011.06.24  rmd
	// Add this decrement to keep the stack on an 8 byte boundary at the conclusion of this function. That is important per
	// Wittenstein and there is an assert test of such in the code where this function is called from. This works in conjunction
	// with the portBYTE_ALIGNMENT setting.
	pxTopOfStack--;

	/* Setup the initial stack of the task.  The stack is set exactly as
	expected by the portRESTORE_CONTEXT() macro. */

	/* First on the stack is the return address - which in this case is the
	start of the task.  The offset is added to make the return address appear
	as it would within an IRQ ISR. */
	*pxTopOfStack = ( portSTACK_TYPE ) pxCode + portINSTRUCTION_SIZE;
	pxTopOfStack--;

	*pxTopOfStack = (portSTACK_TYPE ) 0xaaaaaaaa; /* R14 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) pxOriginalTOS; /* Stack used when task starts goes in R13. */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x12121212;	/* R12 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x11111111;	/* R11 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x10101010;	/* R10 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x09090909;	/* R9 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x08080808;	/* R8 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x07070707;	/* R7 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x06060606;	/* R6 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x05050505;	/* R5 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x04040404;	/* R4 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x03030303;	/* R3 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x02020202;	/* R2 */
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x01010101;	/* R1 */
	pxTopOfStack--;

	/* When the task starts is will expect to find the function parameter in
	R0. */
	*pxTopOfStack = ( portSTACK_TYPE ) pvParameters; /* R0 */
	pxTopOfStack--;

	/* The status register is set for system mode, with interrupts enabled. */
	*pxTopOfStack = ( portSTACK_TYPE ) portINITIAL_SPSR;

	if( ( ( unsigned long ) pxCode & 0x01UL ) != 0x00 )
	{
		/* We want the task to start in thumb mode. */
		*pxTopOfStack |= portTHUMB_MODE_BIT;
	}

	pxTopOfStack--;

	/* Some optimisation levels use the stack differently to others.  This
	 means the interrupt flags cannot always be stored on the stack and will
	 instead be stored in a variable, which is then saved as part of the
	 tasks context. */
	*pxTopOfStack = portNO_CRITICAL_SECTION_NESTING;

	return pxTopOfStack;
}
/*-----------------------------------------------------------*/

portBASE_TYPE xPortStartScheduler(void)
{
extern void vPortISRStartFirstTask( void );

	/* Start the timer that generates the tick ISR.  Interrupts are disabled
	here already. */
	prvSetupTimerInterrupt();

	/* Start the first task. */
	vPortISRStartFirstTask();	

	/* Should not get here! */
	return 0;
}
/*-----------------------------------------------------------*/

void vPortEndScheduler( void )
{
	/* It is unlikely that the ARM port will require this function as there
	is nothing to return to.  */
}
/*-----------------------------------------------------------*/

/*
 * Setup the scheduler timer to generate the tick interrupts at the required
 * frequency.
 */
static void prvSetupTimerInterrupt( void )
{
	INT_32				ltimer;
	TMR_PSCALE_SETUP_T	pscale;
	TMR_MATCH_SETUP_T	msetup;


    // The ISR installed depends on whether the preemptive or cooperative scheduler is being used.
	//
	// The openRTOS guys first had the interrupt set as an ISR_TRIGGER_NEGATIVE_EDGE. According to the LPC32x0 data sheet
	// this is wrong. It should be set as as LOW LEVEL interrupt. We may have seen some misbehaviors when set as an -ve edge
	// trigger. But not clear if so.
	// 
	#if configUSE_PREEMPTION == 1
	{
		extern void ( vPreemptiveTick )( void );
		xSetISR_Vector( FreeRTOS_TICK_INT, INTERRUPT_PRIORITY_free_rtos_tick, ISR_TRIGGER_LOW_LEVEL, vPreemptiveTick, 0 );
	}
	#else
	{
		extern void ( vNonPreemptiveTick )( void );
		xSetISR_Vector( FreeRTOS_TICK_INT, INTERRUPT_PRIORITY_free_rtos_tick, ISR_TRIGGER_LOW_LEVEL, vNonPreemptiveTick, 0 );
	}
	#endif



	#if 0
	THIS IS WITTENSTEIN CODE

	/* Enable the timer 0 clock. */
	portTIMCLK_CTRL1 = portTICK_TIMER_CLOCK_ENABLE;

	/* Reset the Timer 0.  */
	portT0TCR = portRESET_TIMER_COUNT; 

	/* Enable Timer 0 in interrupt controller. */
    portMIC_ER |= portENABLE_MIC_TIMER0; 

    /* Set the prescaler value for 1 micro second. */
    //portT0PR = portPRESCALE_DIVIDER;

    /* Set the Match 0 register for 1 millisecond timer tick. ( 1000 * 1us ) = 1ms. */
    //portT0MR0 = configCPU_CLOCK_HZ / portPERIPH_CLK_DIVIDER  / portPRESCALE_DIVIDER / configTICK_RATE_HZ;

	// set the prescaler to 0
	//portT0PR = 0;
	// so no PCLK is the input clock at 13MHz...so count up to 1 one thousand of that (for 1ms of counting)
	//portT0MR0 = 13000;

	portT0PR = 0;
	portT0MR0 = 13000;

    /* Enable interrupt and reset on MR0. */
    portT0MCR |= portENABLE_MATCH0_INT | portRESET_COUNT_ON_MATCH; 
    
    /* Clear pending interrupts. */
    portT0IR = portCLEAR_PENDING_INT;

    /* Enable Timer 0.  */
    portT0TCR = portENABLE_TIMER;
     
	#endif




	/* Open timers - this will enable the clocks for all timers when
	   match control, match output, and capture control functions
	   disabled. Default clock will be internal. */
	ltimer = timer_open( FreeRTOS_TICK_TIMER, 0 );  // actually returns a pointer to an array which now hold the the the base register address value

	/******************************************************************/
	/* Setup timer 0 for a 10Hz match rate                            */

	/* Use a prescale count time of 100uS                             */
	pscale.ps_tick_val = 0; /* Use ps_us_val value */
	pscale.ps_us_val = 0; /* 100uS */
	timer_ioctl( ltimer, TMR_SETUP_PSCALE, (INT_32) &pscale);

	/* Use a match count value of 1000 (1000 * 100uS = 100mS (10Hz))  */
	msetup.timer_num = 0; /* Use match register set 0 (of 0..3) */
	msetup.use_match_int = TRUE; /* Generate match interrupt on match */
	msetup.stop_on_match = FALSE; /* Do not stop timer on match */
	msetup.reset_on_match = TRUE; /* Reset timer counter on match */

	// 2012.01.26 rmd : Since we have set up the pre-scale registers to be 0 and we have a 13MHz
	// counter clock rate a match value of 13000 will produce an OS tick rate of 1kHz. That's
	// way faster than needed. I think all the periperals are interrupt driven and post
	// semaphores which cause an immediate context switch. So why does this need an agressive
	// rate. I like something on the order of 200Hz. Heck it could probably even be 50Hz.
	// Hmmmm. What to make it. Make it 100Hz. NOTE: when this is changed you must change
	// configTICK_RATE_HZ define in FreeRTOSConfig.h.
	//
	// UPDATE : I have noted in the spi flash driver there is a 3 ms delay. With a 100hz tick
	// rate we will end up with a 10ms delay. That particular code was tested with a 5ms delay.
	// So I've decided to use that. That is a 200Hz tick (5ms) rate. Also noted a delay in the
	// Epson I2C code. It was 1ms. But making it 5 should be safe. At least I'll have alot of
	// test time on it.
	msetup.match_tick_val = 65000;

	timer_ioctl( ltimer, TMR_SETUP_MATCH, (INT_32) &msetup);

	/* Clear any latched timer 0 interrupts and enable match
	 interrupt */
	timer_ioctl( ltimer, TMR_CLEAR_INTS,
				(TIMER_CNTR_MTCH_BIT(0) | TIMER_CNTR_MTCH_BIT(1) |
				 TIMER_CNTR_MTCH_BIT(2) | TIMER_CNTR_MTCH_BIT(3) |
				 TIMER_CNTR_CAPT_BIT(0) | TIMER_CNTR_CAPT_BIT(1) |
				 TIMER_CNTR_CAPT_BIT(2) | TIMER_CNTR_CAPT_BIT(3)));

	timer_ioctl( ltimer, TMR_ENABLE, 1);
	/******************************************************************/

	
	xEnable_ISR( FreeRTOS_TICK_INT );

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


