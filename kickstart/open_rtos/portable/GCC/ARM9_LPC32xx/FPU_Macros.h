/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/

/* provide an alternative and more meaningful name to use when defining the
application task tag register bank. */
#define vTaskSetApplicationTaskTag( xTaskHandle, xFlopRegisterBank ) vTaskUsesFLOP( xTaskHandle, xFlopRegisterBank )


/* ORIGINAL WITTENSTEIN MACROS. The CALSENSE version ONLY saves the VFP REGISTERS IF A CONTEXT SWITCH ACTUALLY OCCURRED */
/* When switching out a task, if the task tag contains a buffer address then
save the flop context into the buffer. */
#define traceTASK_SWITCHED_OUT()											\
	if( pxCurrentTCB->pxTaskTag != NULL )									\
	{																		\
		extern void vPortSaveVFPRegisters( void * );						\
		vPortSaveVFPRegisters( ( void * ) ( pxCurrentTCB->pxTaskTag ) );	\
	}

/* When switching in a task, if the task tag contains a buffer address then
load the flop context from the buffer. */
#define traceTASK_SWITCHED_IN()												\
	if( pxCurrentTCB->pxTaskTag != NULL )									\
	{																		\
		extern void vPortRestoreVFPRegisters( void * );						\
		vPortRestoreVFPRegisters( ( void * ) ( pxCurrentTCB->pxTaskTag ) );	\
	}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef UNTESTED_CALSENSE_MACROS
   THE UNTESTED EVER SO SLIGHTLY MORE EFFICIENT CALSENSE WAY. But this macro can be called from within an ISR
   (via portYIELD_FROM_ISR) so I got nervous and decided not to do it this way. Due to the use of the global
	xTaskThatWasRunning variable.

// defined in port.c
extern void*	xTaskThatWasRunning;

/* When switching out a task, if the task tag contains a buffer address then
save the flop context into the buffer. */
#define traceTASK_SWITCHED_OUT()											\
	xTaskThatWasRunning = pxCurrentTCB;


/* When switching in a task, if the task tag contains a buffer address then
load the flop context from the buffer. */
#define traceTASK_SWITCHED_IN()																\
	if( pxCurrentTCB != (tskTCB*)xTaskThatWasRunning )										\
    {                                                                                   	\
		/* A context switch has occurred, do something. */									\
		if( ((tskTCB*)xTaskThatWasRunning)->pxTaskTag != NULL )								\
		{																					\
			extern void vPortSaveVFPRegisters( void * );									\
			vPortSaveVFPRegisters( (void*)( ((tskTCB*)xTaskThatWasRunning)->pxTaskTag ) );	\
		}																					\
		if( pxCurrentTCB->pxTaskTag != NULL )												\
		{																					\
			extern void vPortRestoreVFPRegisters( void * );									\
			vPortRestoreVFPRegisters( ( void * ) ( pxCurrentTCB->pxTaskTag ) );				\
		}																					\
	}
end of calsense way
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

