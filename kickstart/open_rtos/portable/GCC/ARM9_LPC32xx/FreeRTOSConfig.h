/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/

#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE. 
 *
 * See http://www.freertos.org/a00110.html.
 *----------------------------------------------------------*/


// 2010.10.20 rmd : Calsense has their own copy of assert.h within our project directories.
// Include with "filename" instead of <filename>. Though I think it is actually okay as the
// compiler first searches our specified includes and then the defined system directories.
#include	"assert.h"


#define configASSERT(e) assert(e)


#define configUSE_PREEMPTION				1

#define configUSE_IDLE_HOOK         		0
#define configUSE_TICK_HOOK         		0
#define configUSE_MALLOC_FAILED_HOOK		1


#define configCPU_CLOCK_HZ          		( ( unsigned long ) 208000000 )	
#define configTICK_RATE_HZ          		( ( portTickType ) 200 )

#define configMAX_PRIORITIES				16

//#define configMINIMAL_STACK_SIZE			( ( unsigned short ) 120 )
#define configMINIMAL_STACK_SIZE			( ( unsigned short ) 256 )

#define configTOTAL_HEAP_SIZE				( 0 ) /* This parameter is not used when heap_3 is included in a project. */

#define configMAX_TASK_NAME_LEN				( 16 )

#define configUSE_TRACE_FACILITY			0

#define configUSE_16_BIT_TICKS				0

#define configIDLE_SHOULD_YIELD				1

#define configUSE_VFP						1
#define configUSE_DOUBLE_PRECISION_VFP   	1
#define configUSE_APPLICATION_TASK_TAG		1

#define configCHECK_FOR_STACK_OVERFLOW		2

#define configUSE_MUTEXES               	1
#define configUSE_RECURSIVE_MUTEXES     	1

#define configQUEUE_REGISTRY_SIZE			10


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern unsigned long my_tick_count;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define configGENERATE_RUN_TIME_STATS		0

extern void vConfigureTimerForRunTimeStats( void );

#define portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() vConfigureTimerForRunTimeStats()

unsigned long return_run_timer_counter_value( void );

#define portGET_RUN_TIME_COUNTER_VALUE()	return_run_timer_counter_value()

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* Co-routine definitions. */
#define configUSE_CO_ROUTINES 				0
#define configMAX_CO_ROUTINE_PRIORITIES 	( 2 )


/* Set the following definitions to 1 to include the API function, or zero
to exclude the API function. */

#define INCLUDE_vTaskPrioritySet            1

#define INCLUDE_uxTaskPriorityGet           1

#define INCLUDE_vTaskDelete                 1

#define INCLUDE_vTaskCleanUpResources       1

#define INCLUDE_vTaskSuspend                1

#define INCLUDE_vTaskDelayUntil             1

#define INCLUDE_vTaskDelay                  1

#define INCLUDE_uxTaskGetStackHighWaterMark	1

#define INCLUDE_xTaskGetCurrentTaskHandle	1

#if configUSE_VFP == 1
	/* Include the header that define the traceTASK_SWITCHED_IN() and
	traceTASK_SWITCHED_OUT() macros to save and restore the floating
	point registers for tasks that have requested this behaviour. */
	#include "FPU_Macros.h"
#endif


// configUSE_TIMERS
// Set to 1 to include timer functionality. The timer service task will be automatically created as the scheduler starts when configUSE_TIMERS
// is set to 1.  
#define configUSE_TIMERS	0

// configTIMER_TASK_PRIORITY
// Sets the priority of the timer service task. Like all tasks, the timer service task can run at any priority between 0 and
// ( configMAX_PRIORITIES - 1 ). 
// This value needs to be chosen carefully to meet the requirements of the application. For example, if the timer service
// task is made the highest priority task in the system, then commands sent to the timer service task (when a timer API function is called)
// and expired timers will both get processed immediately. Conversely, if the timer service task is given a low priority, then commands sent
// to the timer service task and expired timers will not be processed until the timer service task is the highest priority task that is able
// to run. It is worth noting here however, that timer expiry times are calculated relative to when a command is sent, and not relative to
// when a command is processed.
//
// 2011.08.09 rmd : NOTE - the application design counts on the timer task queue processing to be a very high priority task.
// HIGHER than any task using the timer service task API. This gives us that assurance that timers are started when we ask
// them to be and STOPPED as well when we put in the request. We want that behavior.
#define configTIMER_TASK_PRIORITY	12

// configTIMER_QUEUE_LENGTH
// This sets the maximum number of unprocessed commands that the timer command queue can hold at any one time. 
// Reasons the timer command queue might fill up include: 
// Making multiple timer API function calls before the scheduler has been started, and therefore before the timer service task has been created. 
// Making multiple (interrupt safe) timer API function calls from an interrupt service routine (ISR). 
// Making multiple timer API function calls from a task that has a priority above that of the timer service task. 
#define configTIMER_QUEUE_LENGTH	20

// configTIMER_TASK_STACK_DEPTH  
// Sets the size of the stack (in words, not bytes) allocated to the timer service task. Timer callback functions execute in the context
// of the timer service task. The stack requirement of the timer service task therefore depends on the stack requirements of the timer
// callback functions.
#define configTIMER_TASK_STACK_DEPTH	(0x2000/4)



#endif
