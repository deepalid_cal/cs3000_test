/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/

/* ----------------------------------------------------------
 * Components that can be compiled to either ARM or THUMB mode are
 * contained in port.c  The ISR routines, which can only be compiled
 * to ARM mode, are contained in this file.
 *----------------------------------------------------------*/


/* Scheduler includes. */
#include	"FreeRTOS.h"

#include	"task.h"

#include	"lpc32xx_timer.h"

#include	"ks_common.h"

// in order to blink the led for some scope timing tests
//#include	"partest.h"




/* Constants required to handle critical sections. */
#define portNO_CRITICAL_NESTING		( ( unsigned long ) 0 )

/* ulCriticalNesting will get set to zero when the first task starts.  It
cannot be initialised to 0 as this will cause interrupts to be enabled
during the kernel initialisation process. */
volatile unsigned long ulCriticalNesting = 9999UL;

/* ---------------------------------------------------------- */

/*
 * ISR to handle manual context switches (from a call to taskYIELD()).
 */
void vPortYieldProcessor( void ) __attribute__((naked));

/*
 * The scheduler can only be started from ARM mode, hence the inclusion of this
 * function here.
 */
void vPortISRStartFirstTask( void );

/* ---------------------------------------------------------- */

void vPortISRStartFirstTask( void )
{
	/* Simply start the scheduler.  This is included here as it can only be
	called from ARM mode. */
	portRESTORE_CONTEXT();
}
/*-----------------------------------------------------------*/

/*
 * Called by portYIELD() or taskYIELD() to manually force a context switch.
 *
 * When a context switch is performed from the task level the saved task
 * context is made to look as if it occurred from within the tick ISR.  This
 * way the same restore context function can be used when restoring the context
 * saved from the ISR or that saved from a call to vPortYieldProcessor.
 */
void vPortYieldProcessor( void )
{
	/* Within an IRQ ISR the link register has an offset from the true return
	address, but an SWI ISR does not.  Add the offset manually so the same
	ISR return code can be used in both cases. */
	__asm volatile ( "ADD		LR, LR, #4" );

	/* Perform the context switch.  First save the context of the current task. */
	portSAVE_CONTEXT();

	/* Find the highest priority task that is ready to run. */
	__asm volatile( "bl			vTaskSwitchContext" );

	/* Restore the context of the new task. */
	portRESTORE_CONTEXT( );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


unsigned long my_tick_count;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/*
 * The ISR used for the scheduler tick depends on whether the cooperative or
 * the preemptive scheduler is being used.
 */
#if configUSE_PREEMPTION == 0

	/* Tick interrupt handler when the co-operative scheduler is being used. */
	void vNonPreemptiveTick( void );
	void vNonPreemptiveTick( void )
	{
		// Clear ALL sources of interrupt in the timer. It should be that only MATCH CHANNEL 0 is the only interrupt set.
		// But clear them all as this timer is not used by any other process (has no secondary use).
		FreeRTOS_TICK_TIMER->ir = 0xFF;

		/* increment the tick. */
        vTaskIncrementTick();
	}

#else

	/* Tick interrupt handler when the pre-emptive scheduler is being used. */
	void vPreemptiveTick( void );
	void vPreemptiveTick( void )
	{
		// Clear ALL sources of interrupt in the timer. It should be that only MATCH CHANNEL 0 is the only interrupt set.
		// But clear them all as this timer is not used by any other process (has no secondary use).
		FreeRTOS_TICK_TIMER->ir = 0xFF;

		// 2011.12.01 rmd : So we can track time even when the scheduler is SUSPENDED. Because the
		// regular OS tick count does not increment when the scheduler is suspended.
		++my_tick_count;

		/*
		// To measure OS tick rate.
		#include "gpio_setup.h"
		if( P0_OUTP_STATE & CS_LED_RED )
		{
			CS_LED_RED_OFF;	
		}
		else
		{
			CS_LED_RED_ON;
		}
		*/
		

		/* Increment the RTOS tick count, then look for the highest priority task that is ready to run. */
		vTaskIncrementTick( );
		vTaskSwitchContext( );        
	}

#endif
/*-----------------------------------------------------------*/

/*
 * The interrupt management utilities can only be called from ARM mode.  When
 * THUMB_INTERWORK is defined the utilities are defined as functions here to
 * ensure a switch to ARM mode.  When THUMB_INTERWORK is not defined then
 * the utilities are defined as macros in portmacro.h - as per other ports.
 */
#ifdef THUMB_INTERWORK

	void vPortDisableInterruptsFromThumb( void ) __attribute__ ((naked));
	void vPortEnableInterruptsFromThumb( void ) __attribute__ ((naked));

	void vPortDisableInterruptsFromThumb( void )
	{
		__asm volatile (
			"STMDB	SP!, {R0}		\n\t"	/* Push R0.									*/
			"MRS	R0, CPSR		\n\t"	/* Get CPSR.								*/
			"ORR	R0, R0, #0xC0	\n\t"	/* Disable IRQ, FIQ.						*/
			"MSR	CPSR, R0		\n\t"	/* Write back modified value.				*/
			"LDMIA	SP!, {R0}		\n\t"	/* Pop R0.									*/
			"BLX	R14" );					/* Return back to thumb.					*/
	}

	/*-----------------------------------------------------------*/
	void vPortEnableInterruptsFromThumb( void )
	{
		__asm volatile (
			"STMDB	SP!, {R0}		\n\t"	/* Push R0.									*/
			"MRS	R0, CPSR		\n\t"	/* Get CPSR.								*/
			"BIC	R0, R0, #0xC0	\n\t"	/* Enable IRQ, FIQ.							*/
			"MSR	CPSR, R0		\n\t"	/* Write back modified value.				*/
			"LDMIA	SP!, {R0}		\n\t"	/* Pop R0.									*/
			"BLX	R14" );					/* Return back to thumb.					*/
	}
	/*-----------------------------------------------------------*/

#endif /* THUMB_INTERWORK */

/* The code generated by the GCC compiler uses the stack in different ways at
different optimisation levels.  The interrupt flags can therefore not always
be saved to the stack.  Instead the critical section nesting level is stored
in a variable, which is then saved as part of the stack context. */
void vPortEnterCritical( void )
{
	portDISABLE_INTERRUPTS();

	/* Now interrupts are disabled ulCriticalNesting can be accessed
	directly.  Increment ulCriticalNesting to keep a count of how many times
	portENTER_CRITICAL() has been called. */
	ulCriticalNesting++;
}
/*-----------------------------------------------------------*/
void vPortExitCritical( void )
{
	if( ulCriticalNesting > portNO_CRITICAL_NESTING )
	{
		/* Decrement the nesting count as we are leaving a critical section. */
		ulCriticalNesting--;

		/* If the nesting level has reached zero then interrupts should be
		re-enabled. */
		if( ulCriticalNesting == portNO_CRITICAL_NESTING )
		{
			portENABLE_INTERRUPTS( );
		}
	}
}
/*-----------------------------------------------------------*/

#if configUSE_VFP == 1
	/*
	 * Subroutine is called to save floating point registers when a task
	 * using vTaskSetApplicationTaskTag(), context is switched.
	 * Macro traceTASK_SWITCHED_OUT is used to invoke this routine.
	 */
	void vPortSaveVFPRegisters( void * pvTaskTag ) __attribute__ ((naked));
	void vPortSaveVFPRegisters( void * pvTaskTag )
	{
		#if configUSE_DOUBLE_PRECISION_VFP == 1
		{
			__asm volatile
			(
				/* Save Flop registers. */
				"FSTD	D0, [ r0, #+0 * 8 ]         \n\t"
				"FSTD	D1, [ r0, #+1 * 8 ]         \n\t"
				"FSTD	D2, [ r0, #+2 * 8 ]         \n\t"
				"FSTD	D3, [ r0, #+3 * 8 ]         \n\t"
				"FSTD	D4, [ r0, #+4 * 8 ]         \n\t"
				"FSTD	D5, [ r0, #+5 * 8 ]         \n\t"
				"FSTD	D6, [ r0, #+6 * 8 ]         \n\t"
				"FSTD	D7, [ r0, #+7 * 8 ]         \n\t"
				"FSTD	D8, [ r0, #+8 * 8 ]         \n\t"
				"FSTD	D9, [ r0, #+9 * 8 ]         \n\t"
				"FSTD	D10, [ r0, #+10 * 8 ]       \n\t"
				"FSTD	D11, [ r0, #+11 * 8 ]       \n\t"
				"FSTD	D12, [ r0, #+12 * 8 ]       \n\t"
				"FSTD	D13, [ r0, #+13 * 8 ]       \n\t"
				"FSTD	D14, [ r0, #+14 * 8 ]       \n\t"
				"FSTD	D15, [ r0, #+15 * 8 ]       \n\t"
				"FMRX	R2, FPSCR					\n\t"
				"str    R2, [ r0, #+16 * 8 ]        \n\t"
				"BLX R14"
			);
		}
		#else
		{
			__asm volatile
			(
				/* Save Flop registers. */
				"FSTS	S0, [ r0, #+0 * 4 ]         \n\t"
				"FSTS	S1, [ r0, #+1 * 4 ]         \n\t"
				"FSTS	S2, [ r0, #+2 * 4 ]         \n\t"
				"FSTS	S3, [ r0, #+3 * 4 ]         \n\t"
				"FSTS	S4, [ r0, #+4 * 4 ]         \n\t"
				"FSTS	S5, [ r0, #+5 * 4 ]         \n\t"
				"FSTS	S6, [ r0, #+6 * 4 ]         \n\t"
				"FSTS	S7, [ r0, #+7 * 4 ]         \n\t"
				"FSTS	S8, [ r0, #+8 * 4 ]         \n\t"
				"FSTS	S9, [ r0, #+9 * 4 ]         \n\t"
				"FSTS	S10, [ r0, #+10 * 4 ]       \n\t"
				"FSTS	S11, [ r0, #+11 * 4 ]       \n\t"
				"FSTS	S12, [ r0, #+12 * 4 ]       \n\t"
				"FSTS	S13, [ r0, #+13 * 4 ]       \n\t"
				"FSTS	S14, [ r0, #+14 * 4 ]       \n\t"
				"FSTS	S15, [ r0, #+15 * 4 ]       \n\t"
				"FSTS	S16, [ r0, #+16 * 4 ]       \n\t"
				"FSTS	S17, [ r0, #+17 * 4 ]       \n\t"
				"FSTS	S18, [ r0, #+18 * 4 ]       \n\t"
				"FSTS	S19, [ r0, #+19 * 4 ]       \n\t"
				"FSTS	S20, [ r0, #+20 * 4 ]       \n\t"
				"FSTS	S21, [ r0, #+21 * 4 ]       \n\t"
				"FSTS	S22, [ r0, #+22 * 4 ]       \n\t"
				"FSTS	S23, [ r0, #+23 * 4 ]       \n\t"
				"FSTS	S24, [ r0, #+24 * 4 ]       \n\t"
				"FSTS	S25, [ r0, #+25 * 4 ]       \n\t"
				"FSTS	S26, [ r0, #+26 * 4 ]       \n\t"
				"FSTS	S27, [ r0, #+27 * 4 ]       \n\t"
				"FSTS	S28, [ r0, #+28 * 4 ]       \n\t"
				"FSTS	S29, [ r0, #+29 * 4 ]       \n\t"
				"FSTS	S30, [ r0, #+30 * 4 ]       \n\t"
				"FSTS	S31, [ r0, #+31 * 4 ]       \n\t"
				"FMRX	R2, FPSCR					\n\t"
				"str    R2, [ r0, #+16 * 8 ]        \n\t"
				"BLX R14"
			);
		}
		#endif /* configUSE_DOUBLE_PRECISION_VFP */

		/* To remove compiler warning about unused parameter. */
		( void ) pvTaskTag;
	}

	/*-----------------------------------------------------------*/
	/*
	 * Subroutine is called to restore floating point registers when a task
	 * using vTaskSetApplicationTaskTag(), context is switched.
	 * Macro traceTASK_SWITCHED_IN is used to invoke this routine.
	 */
	void vPortRestoreVFPRegisters( void * pvTaskTag ) __attribute__ ((naked));
	void vPortRestoreVFPRegisters( void * pvTaskTag )
	{
		#if configUSE_DOUBLE_PRECISION_VFP == 1
		{
			__asm volatile
			(
				/* Restore flop registers. */
				"LDR 	R2, [ r0, #+16 * 8 ]        \n\t"
				"FLDD	D0, [ r0, #+0 * 8 ]         \n\t"
				"FLDD	D1, [ r0, #+1 * 8 ]         \n\t"
				"FLDD	D2, [ r0, #+2 * 8 ]         \n\t"
				"FLDD	D3, [ r0, #+3 * 8 ]         \n\t"
				"FLDD	D4, [ r0, #+4 * 8 ]         \n\t"
				"FLDD	D5, [ r0, #+5 * 8 ]         \n\t"
				"FLDD	D6, [ r0, #+6 * 8 ]         \n\t"
				"FLDD	D7, [ r0, #+7 * 8 ]         \n\t"
				"FLDD	D8, [ r0, #+8 * 8 ]         \n\t"
				"FLDD	D9, [ r0, #+9 * 8 ]         \n\t"
				"FLDD	D10, [ r0, #+10 * 8 ]       \n\t"
				"FLDD	D11, [ r0, #+11 * 8 ]       \n\t"
				"FLDD	D12, [ r0, #+12 * 8 ]       \n\t"
				"FLDD	D13, [ r0, #+13 * 8 ]       \n\t"
				"FLDD	D14, [ r0, #+14 * 8 ]       \n\t"
				"FLDD	D15, [ r0, #+15 * 8 ]       \n\t"
				"FMXR 	FPSCR, R2					\n\t"
				"BLX R14"
			);
		}
		#else
		{
			__asm volatile
			(
				/* Restore flop registers. */
				"LDR 	R2, [ r0, #+16 * 8 ]        \n\t"
				/* Restore flop registers. */
				"FLDS	S0, [ r0, #+0 * 4 ]         \n\t"
				"FLDS	S1, [ r0, #+1 * 4 ]         \n\t"
				"FLDS	S2, [ r0, #+2 * 4 ]         \n\t"
				"FLDS	S3, [ r0, #+3 * 4 ]         \n\t"
				"FLDS	S4, [ r0, #+4 * 4 ]         \n\t"
				"FLDS	S5, [ r0, #+5 * 4 ]         \n\t"
				"FLDS	S6, [ r0, #+6 * 4 ]         \n\t"
				"FLDS	S7, [ r0, #+7 * 4 ]         \n\t"
				"FLDS	S8, [ r0, #+8 * 4 ]         \n\t"
				"FLDS	S9, [ r0, #+9 * 4 ]         \n\t"
				"FLDS	S10, [ r0, #+10 * 4 ]       \n\t"
				"FLDS	S11, [ r0, #+11 * 4 ]       \n\t"
				"FLDS	S12, [ r0, #+12 * 4 ]       \n\t"
				"FLDS	S13, [ r0, #+13 * 4 ]       \n\t"
				"FLDS	S14, [ r0, #+14 * 4 ]       \n\t"
				"FLDS	S15, [ r0, #+15 * 4 ]       \n\t"
				"FLDS	S16, [ r0, #+16 * 4 ]       \n\t"
				"FLDS	S17, [ r0, #+17 * 4 ]       \n\t"
				"FLDS	S18, [ r0, #+18 * 4 ]       \n\t"
				"FLDS	S19, [ r0, #+19 * 4 ]       \n\t"
				"FLDS	S20, [ r0, #+20 * 4 ]       \n\t"
				"FLDS	S21, [ r0, #+21 * 4 ]       \n\t"
				"FLDS	S22, [ r0, #+22 * 4 ]       \n\t"
				"FLDS	S23, [ r0, #+23 * 4 ]       \n\t"
				"FLDS	S24, [ r0, #+24 * 4 ]       \n\t"
				"FLDS	S25, [ r0, #+25 * 4 ]       \n\t"
				"FLDS	S26, [ r0, #+26 * 4 ]       \n\t"
				"FLDS	S27, [ r0, #+27 * 4 ]       \n\t"
				"FLDS	S28, [ r0, #+28 * 4 ]       \n\t"
				"FLDS	S29, [ r0, #+29 * 4 ]       \n\t"
				"FLDS	S30, [ r0, #+30 * 4 ]       \n\t"
				"FLDS	S31, [ r0, #+31 * 4 ]       \n\t"
				"FMXR 	FPSCR, R2					\n\t"
				"BLX R14"
			);
		}
		#endif /* configUSE_DOUBLE_PRECISION_VFP */

		/* To remove compiler warning about unused parameter. */
		( void ) pvTaskTag;
	}

#endif
