/*  file = lib_reent.c                         02.09.2011 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<__libc.h>




#ifndef NO_DEBUG_PRINTF

extern void *debugio_MUTEX;

#include	"ks_common.h"

#endif





/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/*
typedef enum
{
  CTL_LIBC_MUTEX_HEAP = (1<<0),
  CTL_LIBC_MUTEX_PRINTF = (1<<1),
  CTL_LIBC_MUTEX_SCANF = (1<<2),
  CTL_LIBC_MUTEX_DEBUG_IO = (1<<3)
}
CTL_LIBC_MUTEX_BITS;

CTL_EVENT_SET_t ctl_libc_mutex = CTL_LIBC_MUTEX_HEAP | CTL_LIBC_MUTEX_PRINTF | CTL_LIBC_MUTEX_SCANF | CTL_LIBC_MUTEX_DEBUG_IO;
*/

static int errno;

int* __errno(void)
{
/*
  if (ctl_task_executing)
    return &(ctl_task_executing->thread_errno);
  else
    return &errno;
*/
	return( &errno );
}

void 
__heap_lock(void)
{
/*
  if (ctl_task_executing)
    ctl_events_wait(CTL_EVENT_WAIT_ANY_EVENTS_WITH_AUTO_CLEAR, &ctl_libc_mutex, CTL_LIBC_MUTEX_HEAP, 0, 0);
*/
}

void 
__heap_unlock(void)
{
/*
  if (ctl_task_executing)
    ctl_events_set_clear(&ctl_libc_mutex, CTL_LIBC_MUTEX_HEAP, 0);
*/
}

void 
__printf_lock(void)
{
/*
  if (ctl_task_executing)
    ctl_events_wait(CTL_EVENT_WAIT_ANY_EVENTS_WITH_AUTO_CLEAR, &ctl_libc_mutex, CTL_LIBC_MUTEX_PRINTF, 0, 0);
*/
}

void 
__printf_unlock(void)
{
/*
  if (ctl_task_executing)
    ctl_events_set_clear(&ctl_libc_mutex, CTL_LIBC_MUTEX_PRINTF, 0);
*/
}

void 
__scanf_lock(void)
{
/*
  if (ctl_task_executing)
    ctl_events_wait(CTL_EVENT_WAIT_ANY_EVENTS_WITH_AUTO_CLEAR, &ctl_libc_mutex, CTL_LIBC_MUTEX_SCANF, 0, 0);
*/
}

void 
__scanf_unlock(void)
{
/*
  if (ctl_task_executing)
    ctl_events_set_clear(&ctl_libc_mutex, CTL_LIBC_MUTEX_SCANF, 0);
*/
}

//static unsigned char debug_io_lock_previous_priority;


#ifndef NO_DEBUG_PRINTF

	// Consider ALSO switching the task trying to do the debugio activity to the highest priority
	// to prevent debugio host timeout. During the unlock restore the task to its original priority.

	void __debug_io_lock(void)
	{
		if ( debugio_MUTEX != NULL )
		{
			xSemaphoreTake( debugio_MUTEX, portMAX_DELAY );

			// should boost calling task priority to max priority to avoid debugio timeout
		}
	}
	
	void __debug_io_unlock(void)
	{
		if ( debugio_MUTEX != NULL )
		{
			xSemaphoreGive( debugio_MUTEX );
		}
	}
	
#else

	void __debug_io_lock(void)
	{
	}

	void __debug_io_unlock(void)
	{
	}
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

