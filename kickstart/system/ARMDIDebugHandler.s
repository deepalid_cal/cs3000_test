/*****************************************************************************
 * Copyright (c) 2001, 2002 Rowley Associates Limited.                       *
 *                                                                           *
 * This file may be distributed under the terms of the License Agreement     *
 * provided with this software.                                              *
 *                                                                           *
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE   *
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. *
 *****************************************************************************/

/*****************************************************************************
 *                           Preprocessor Definitions
 *                           ------------------------
 *
 * __DOWNLOADABLE_DEBUG_HANDLER__
 *
 *   Builds the debug handler so that it can be downloaded anywhere in memory.
 *   Note that this shouldn't be defined for any debug handler that will
 *   be linked into the final application.
 *
 * __TARGET_SUPPORTS_MONITOR_MODE__
 *
 *   Should be defined if target supports monitor mode.
 *
 * __TARGET_HAS_MMU__
 *
 *   Should be defined if target has an MMU.
 *
 *****************************************************************************/

#include "ARMDIDebugHandler.h"

#define PSR_MODE_User   0x00000010
#define PSR_MODE_FIQ    0x00000011
#define PSR_MODE_IRQ    0x00000012
#define PSR_MODE_SVC    0x00000013
#define PSR_MODE_Abort  0x00000017
#define PSR_MODE_Undef  0x0000001B
#define PSR_MODE_System 0x0000001F
#define PSR_MODE_mask   0x0000001F

#define PSR_F_bit       (1 << 6)
#define PSR_I_bit       (1 << 7)

#ifndef __DOWNLOADABLE_DEBUG_HANDLER__
  .global dbg_pabort_handler
  .global dbg_dabort_handler
  .global dbg_poll
#endif

  .section .init, "ax"
  .arm

.macro tx_poll reg
  mrc 14, 0, \reg, c0, c0
  tst \reg, #0x02
.endm

.macro tx_wait reg
1:
  tx_poll \reg
  bne 1b
.endm

.macro tx_write reg
  mcr 14, 0, \reg, c1, c0
.endm

.macro tx reg
.ifc "\reg", "r7"
.err
.endif
  tx_wait r7
  tx_write \reg
.endm

.macro rx_poll reg
  mrc 14, 0, \reg, c0, c0
  tst \reg, #0x01
.endm

.macro rx_wait reg
1:
  rx_poll \reg
  beq 1b
.endm

.macro rx_read reg
  mrc 14, 0, \reg, c1,  c0
.endm

.macro rx reg
1:
  rx_wait \reg
  rx_read \reg
.endm

#ifndef __DOWNLOADABLE_DEBUG_HANDLER__

dbg_pabort_handler:
  /* Store CPU State */
  ldr r13, =dbg_registers_start
  stmia r13!, { r0-r12 }

  mov r0, #ARMDI_DEBUG_HANDLER_STOP_CAUSE_PABORT

  b debug_handler

dbg_dabort_handler:
  /* If debug handler has re-entered, return to the next instruction */
  mrs r13, spsr
  and r13, r13, #PSR_MODE_mask
  cmp r13, #PSR_MODE_Abort
  subeqs pc, r14, #4

  /* Store CPU State */
  ldr r13, =dbg_registers_start
  stmia r13!, { r0-r12 }

  mov r0, #ARMDI_DEBUG_HANDLER_STOP_CAUSE_DABORT

debug_handler:
  sub r10, lr, #4     /* Store the PC for later */
  mrs r11, spsr       /* Store the cpsr for later */
  mrs r12, cpsr       /* Store the actual cpsr for later */

#ifdef __TARGET_SUPPORTS_MONITOR_MODE__
  /* Check if stop is due to breakpoint or watchdog */
  mrc 14, 0, r1, c2, c0
  tst r1, #1
  beq 1f
  mov r0, #ARMDI_DEBUG_HANDLER_STOP_CAUSE_BREAKPOINT
  mov r1, #0
  mcr 14, 0, r1, c2, c0
1:
#endif

  ldr r1, =dbg_stopped
  str r0, [r1]
#endif
  
  ldr r0, =ARMDI_DEBUG_HANDLER_MAGIC_NUMBER
  bl tx_r0

serve_loop:
  sub lr, pc, #8 // Return to this instruction
serve:
  rx r0
  add pc, pc, r0, lsl #2
  b serve_loop
  b peek_uint32
  b peek_uint16
  b peek_uint8
  b poke_uint32
  b poke_uint16
  b poke_uint8
  b crc32_uint8
  b execute_instruction
  b terminate
  b call_function
#ifndef __DOWNLOADABLE_DEBUG_HANDLER__
  b get_info
  b store_state
  b restore_state
#endif

/*
 * ARMDI_DEBUG_HANDLER_CMD_TERMINATE
 */
terminate:

#ifndef __DOWNLOADABLE_DEBUG_HANDLER__

  ldr r1, =dbg_stopped
  ldr r0, =0
  str r0, [r1]

  mov lr, r10         /* Setup return address to be the pc */
  ldmdb r13!, { r0-r12 }
  subs pc, lr, #0     /* Exit from debug exeception */

#else

  mov r0, #1
  mov r9, lr
  bl tx_r0
  mov pc, r9

#endif

/*
 * ARMDI_DEBUG_HANDLER_CMD_POKE_UINT32
 */
poke_uint32:
  mov r9, lr
  bl rx_r0           /* Get address */
  bl rx_r1           /* Get length */
poke_uint32_loop:
  bl rx_r2
#ifdef __TARGET_HAS_MMU__
  and r3, r0, #~0x1f
#endif
  str r2, [r0], #4
#ifdef __TARGET_HAS_MMU__     
  mcr p15, 0, r3, c7, c10, 1 // Clean DCache single entry using MVA
  mcr p15, 0, r3, c7, c5, 1  // Invalidate ICache single entry using MVA
#endif
  subs r1, r1, #1
  bne poke_uint32_loop
  mov pc, r9
  
/*
 * ARMDI_DEBUG_HANDLER_CMD_POKE_UINT16
 */
poke_uint16:
  mov r9, lr
  bl rx_r0           /* Get address */
  bl rx_r1           /* Get length */
poke_uint16_loop:
  bl rx_r2
#ifdef __TARGET_HAS_MMU__
  and r3, r0, #~0x1f
#endif
  strh r2, [r0], #2
#ifdef __TARGET_HAS_MMU__     
  mcr p15, 0, r3, c7, c10, 1 // Clean DCache single entry using MVA
  mcr p15, 0, r3, c7, c5, 1  // Invalidate ICache single entry using MVA
#endif
  subs r1, r1, #1
  moveq pc, r9
  mov r2, r2, lsr #16
#ifdef __TARGET_HAS_MMU__
  and r3, r0, #~0x1f
#endif
  strh r2, [r0], #2
#ifdef __TARGET_HAS_MMU__     
  mcr p15, 0, r3, c7, c10, 1 // Clean DCache single entry using MVA
  mcr p15, 0, r3, c7, c5, 1  // Invalidate ICache single entry using MVA
#endif
  subs r1, r1, #1
  moveq pc, r9
  b poke_uint16_loop

/*
 * ARMDI_DEBUG_HANDLER_CMD_POKE_UINT8
 */
poke_uint8:
  mov r9, lr
  bl rx_r0           /* Get address */
  bl rx_r1           /* Get length */
poke_uint8_loop:
  bl rx_r2
  strb r2, [r0], #1
  subs r1, r1, #1
  moveq pc, r9
  mov r2, r2, lsr #8
  strb r2, [r0], #1
  subs r1, r1, #1
  moveq pc, r9
  mov r2, r2, lsr #8
  strb r2, [r0], #1
  subs r1, r1, #1
  moveq pc, r9
  mov r2, r2, lsr #8
  strb r2, [r0], #1
  subs r1, r1, #1
  moveq pc, r9
  b poke_uint8_loop

/*
 * ARMDI_DEBUG_HANDLER_CMD_PEEK_UINT32
 */
peek_uint32:
  mov r9, lr
  bl rx_r0           /* Get address */
  bl rx_r1           /* Get length */
peek_uint32_loop:
  ldr r2, [r0], #4
  bl tx_r2
  subs r1, r1, #1
  moveq pc, r9
  b peek_uint32_loop

/*
 * ARMDI_DEBUG_HANDLER_CMD_PEEK_UINT16
 */
peek_uint16:
  mov r9, lr
  bl rx_r0           /* Get address */
  bl rx_r1           /* Get length */
peek_uint16_loop:
  ldrh r2, [r0], #2
  subs r1, r1, #1
  ldrneh r3, [r0], #2
  orrne r2, r2, r3, lsl #16
  subnes r1, r1, #1
  beq peek_uint16_finish
  bl tx_r2
  b peek_uint16_loop
peek_uint16_finish:
  bl tx_r2
  mov pc, r9

/*
 * ARMDI_DEBUG_HANDLER_CMD_PEEK_UINT8
 */
peek_uint8:
  mov r9, lr
  bl rx_r0           /* Get address */
  bl rx_r1           /* Get length */
peek_uint8_loop:
  ldrb r2, [r0], #1
  subs r1, r1, #1
  ldrneb r3, [r0], #1
  orrne r2, r2, r3, lsl #8
  subnes r1, r1, #1
  ldrneb r3, [r0], #1
  orrne r2, r2, r3, lsl #16
  subnes r1, r1, #1
  ldrneb r3, [r0], #1
  orrne r2, r2, r3, lsl #24
  subnes r1, r1, #1
  beq peek_uint8_finish
  bl tx_r2
  b peek_uint8_loop
peek_uint8_finish:
  bl tx_r2
  mov pc, r9

/*
 * ARMDI_DEBUG_HANDLER_CMD_CRC32_UINT8
 */
crc32_uint8:
  mov r9, lr
  bl rx_r0           /* Get address */
  bl rx_r1           /* Get length  */
  bl rx_r2           /* CRC32 Checksum */
  rx r8              /* CRC32 Polynomial */
  mov r5, #0
1:
  ldrb r3, [r0], #1
  mov r3, r3, lsl #24
  mov r4, #8
2:
  movs r3, r3, lsl #1
  adc r6, r5, r5
  movs r2, r2, lsl #1
  sbcs r6, r6, r5
  eoreq r2, r2, r8
  subs r4, r4, #1
  bne 2b
  subs r1, r1, #1
  bne 1b
  mvn r0, r2 /* Return checksum XOR 0xFFFFFFFF */
  bl tx_r0
  mov pc, r9

/*
 * ARMDI_DEBUG_HANDLER_CMD_EXECUTE_INSTRUCTION
 */
execute_instruction:
  mov r9, lr
  bl rx_r2 /* Get control */
  bl rx_r0 /* Get instruction */
  mov r3, r0
  tst r2, #0x00000003
  blne rx_r0 /* Get R0 */
  tst r2, #0x00000002
  blne rx_r1 /* Get R1 */

#ifdef __DOWNLOADABLE_DEBUG_HANDLER__
  str r3, [pc, #instruction - . - 8] /* Store the instruction */
  nop /* prefetching may mean that the instruction: has been fetched */
  nop
  nop
  nop
  nop
instruction:
  nop /* Space for instruction */
#else
#if defined(__thumb2__)
#error FIXME: implement
#else
  ldr r4, =0xE1A0F00E /* mov pc, lr */
#endif
  ldr r5, =dbg_execute_instruction_data_end
  stmfd r5!, { r3, r4 }
#ifdef __TARGET_HAS_MMU__    
  and r3, r5, #~0x1f
  mcr p15, 0, r3, c7, c10, 1 // Clean DCache single entry using MVA
  mcr p15, 0, r3, c7, c5, 1  // Invalidate ICache single entry using MVA
#endif
  mov lr, pc
  mov pc, r5
#endif

  tst r2, #0x00000300
  blne tx_r0 /* Send r0 */
  tst r2, #0x00000200
  blne tx_r1 /* Send r1 */
  mov pc, r9

/*
 * ARMDI_DEBUG_HANDLER_CMD_CALL_FUNCTION
 */
call_function:
  mov r9, lr
  bl rx_r0
  mov r5, r0
  bl rx_r1
  bl rx_r2
  bl rx_r0
  mov r3, r0
  bl rx_r0
  mov sp, r0
  bl rx_r0
  mov r4, r0
  mov r0, r5
  mov lr, pc
  mov pc, r4
  bl tx_r0
  bl tx_r1
  mov pc, r9

#ifndef __DOWNLOADABLE_DEBUG_HANDLER__

/*
 * ARMDI_DEBUG_HANDLER_CMD_GET_INFO
 */
get_info:
  mov r9, lr
  ldr r0, =dbg_registers_start
  bl tx_r0
  ldr r0, =dbg_poll
  bl tx_r0
  mov pc, r9

/*
 * ARMDI_DEBUG_HANDLER_CMD_STORE_STATE
 */
store_state:
  mov r9, lr

  mov r1, r13
  bic r2, r12, #PSR_MODE_mask

  /* Store system mode registers */
  orr r0, r2, #PSR_MODE_System
  msr cpsr_c, r0
  stmia r1!, { r13-r14 }
  stmia r1!, { r10-r11 }

  /* Store fiq mode registers */
  orr r0, r2, #PSR_MODE_FIQ
  msr cpsr_c, r0
  stmia r1!, {r8-r14}
  mrs r0, spsr
  stmia r1!, {r0}

  /* Store svc mode registers */
  orr r0, r2, #PSR_MODE_SVC
  msr cpsr_c, r0
  stmia r1!, {r13-r14}
  mrs r0, spsr
  stmia r1!, {r0}

  /* Store abt mode registers */
  orr r0, r2, #PSR_MODE_Abort
  msr cpsr_c, r0
  ldr r13, =dbg_stopped 
  ldr r13, [r13] /* Store stopped cause in r13_abt */
  stmia r1!, {r13-r14}
  mrs r0, spsr
  stmia r1!, {r0}

  /* Store irq mode registers */
  orr r0, r2, #PSR_MODE_IRQ
  msr cpsr_c, r0
  stmia r1!, {r13-r14}
  mrs r0, spsr
  stmia r1!, {r0}

  /* Store und mode registers */
  orr r0, r2, #PSR_MODE_Undef
  msr cpsr_c, r0
  stmia r1!, {r13-r14}
  mrs r0, spsr
  stmia r1!, {r0}

  /* Get aborted mode */
  and r0, r11, #PSR_MODE_mask

  mov r1, r12
  /* If not stopped in IRQ mode, re-enable IRQ interrupts */
  cmp r0, #PSR_MODE_IRQ
  orreq r1, r1, #PSR_I_bit
  bicne r1, r1, #PSR_I_bit

  /* If stopped in FIQ mode, disable FIQ interrupts */
  cmp r0, #PSR_MODE_FIQ
  orreq r1, r1, #PSR_F_bit
  bicne r1, r1, #PSR_F_bit

  /* Restore initial mode at start of handler */
  msr cpsr_c, r1
  mov pc, r9

/*
 * ARMDI_DEBUG_HANDLER_CMD_RESTORE_STATE
 */
restore_state:
  mov r9, lr

  ldr r1, =dbg_registers_end  
  bic r2, r12, #PSR_MODE_mask

  /* Restore und mode registers */
  orr r0, r2, #PSR_MODE_Undef
  msr cpsr_c, r0
  ldmdb r1!, { r0 }
  msr spsr_cxsf, r0
  ldmdb r1!, { r13-r14 }

  /* Restore irq mode registers */
  orr r0, r2, #PSR_MODE_IRQ
  msr cpsr_c, r0
  ldmdb r1!, { r0 }
  msr spsr_cxsf, r0
  ldmdb r1!, { r13-r14 }

  /* Restore abt mode registers */
  orr r0, r2, #PSR_MODE_Abort
  msr cpsr_c, r0
  ldmdb r1!, { r0 }
  msr spsr_cxsf, r0
  ldmdb r1!, { r13-r14 }

  /* Restore svc mode registers */
  orr r0, r2, #PSR_MODE_SVC
  msr cpsr_c, r0
  ldmdb r1!, { r0 }
  msr spsr_cxsf, r0
  ldmdb r1!, { r13-r14 }

  /* Restore fiq mode registers */
  orr r0, r2, #PSR_MODE_FIQ
  msr cpsr_c, r0
  ldmdb r1!, { r0 }
  msr spsr_cxsf, r0
  ldmdb r1!, { r8-r14 }

  /* Restore system mode registers */
  orr r0, r2, #PSR_MODE_System
  msr cpsr_c, r0
  ldmdb r1!, { r10-r11 } /* r11 = cpsr, r10 = pc */
  ldmdb r1!, { r13-r14 }

  /* Restore the common registers */
  msr cpsr_c, r12     /* Restore initial mode at start of handler */
  msr spsr_cxsf, r11  /* Setup spsr to be the cpsr */
  mov r13, r1

  mov pc, r9

dbg_poll:
  /* Return if debugger is already stopped */
  ldr r1, =dbg_stopped
  ldr r0, [r1]
  cmp r0, #0
  bxne lr

  /* Return if there is nothing to do */
  rx_poll r0
  bxeq lr

  /* Store register state onto debug handler stack */
  mov r0, sp
  ldr sp, =dbg_stack_end
  stmfd sp!, { r0, r4-r9, lr }

  /* Handle debugger commands */
1:
  mov lr, pc
  ldr pc, =serve
  rx_poll r0
  bne 1b

  /* Restore register state from debug handler stack */
  ldmfd sp!, { r0, r4-r9, lr }
  mov sp, r0
  bx lr

#endif

tx_r0:
  tx r0
  mov pc, lr

tx_r1:
  tx r1
  mov pc, lr

tx_r2:
  tx r2
  mov pc, lr

rx_r0:
  rx r0
  mov pc, lr

rx_r1:
  rx r1
  mov pc, lr

rx_r2:
  rx r2
  mov pc, lr

  .data
  .arm

#ifndef __DOWNLOADABLE_DEBUG_HANDLER__

dbg_execute_instruction_data_start:
  .fill 2, 4, 0
dbg_execute_instruction_data_end:

dbg_registers_start:
  .fill 37, 4, 0
dbg_registers_end:

dbg_stopped:
  .word 0

dbg_stack_start:
  .fill 10, 4, 0
dbg_stack_end:


#endif
