/*****************************************************************************
 * Copyright (c) 2009 Rowley Associates Limited.                             *
 *                                                                           *
 * This file may be distributed under the terms of the License Agreement     *
 * provided with this software.                                              *
 *                                                                           *
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE   *
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. *
 *
 *                           Function
 *                           --------
 * Defines the reset, exception and interrupt handler behaviour. The reset 
 * behaviour is controlled by a set of preprocessor definitions.
 *
 *                           Preprocessor Definitions
 *                           ------------------------
 * STARTUP_FROM_RESET
 *
 *   If defined, the program will startup from power-on/reset. If not defined
 *   the program will just loop endlessly from power-on/reset.
 *
 *   This definition is not defined by default on this target because the
 *   debugger is unable to reset this target and maintain control of it over the
 *   JTAG interface. The advantage of doing this is that it allows the debugger
 *   to reset the CPU and run programs from a known reset CPU state on each run.
 *   It also acts as a safety net if you accidently download a program in FLASH
 *   that crashes and prevents the debugger from taking control over JTAG
 *   rendering the target unusable over JTAG. The obvious disadvantage of doing
 *   this is that your application will not startup without the debugger.
 *
 *   We advise that on this target you keep STARTUP_FROM_RESET undefined whilst
 *   you are developing and only define STARTUP_FROM_RESET when development is
 *   complete.
 *
 * __FLASH_BUILD
 *
 * Executable will boot from NOR flash and will call the user supplied function 
 * init_sdram_pll() to set up the sdram. Note that this function cannot use static
 * data, it can however use stack allocated data.
 *
 * __FLASH_WIDTH
 *
 * Specify the byte width of the NOR flash memory for a __FLASH_BUILD.
 *
 * NO_DBG_HANDLER
 *
 * When specified a debug handler isn't used for the abort vectors.
 *
 * NO_SRAM_VECTORS
 *
 * When specified exeception vectors are not copied to IRAM start.
 *
 * NO_CACHE_ENABLE
 * 
 * When specified the I and D caches are not enabled.
 *
 * NO_ICACHE_ENABLE
 *
 *   If not defined (and NO_CACHE_ENABLE not defined), the I cache is enabled.
 *
 *
 *                           Linker Symbols
 *                           --------------
 *
 * __reserved_mmu_start__
 *
 * The start of the MMU Translation Table Base (TTB)
 *
 * __IRAM_segment_start__, __IRAM_segment_end__
 *
 * The start and end addresses of IRAM.
 *
 * __SDRAM_segment_start__, __SDRAM_segment_end__
 *
 * The start and end addresses of SDRAM
 *
 * __FLASH_segment_start__, __FLASH_segment_start__
 *
 * The start and end address of NOR FLASH memory
 *
 *****************************************************************************/


  .section .vectors, "ax"		/* specify that the following code goes into the .vectors section */
  .code 32						/* specify the ARM instruction set */
  .align 0						
  .global _vectors
  .global reset_handler
  .global dcache_flush

  .extern vPortYieldProcessor
  .extern xIRQ_Handler
  .extern xFIQ_Handler


#ifdef __FLASH_BUILD
#if __FLASH_WIDTH==1
  .word 0x13579BD0
#elif __FLASH_WIDTH==2
  .word 0x13579BD1
#else
  .word 0x13579BD2
#endif
#endif


/*****************************************************************************
 *                                                                           *
 * Exception Vectors                                                         *
 *                                                                           *
 *****************************************************************************/
_vectors:
#ifdef STARTUP_FROM_RESET
  ldr pc, [pc, #reset_handler_address - . - 8]  /* reset */
#else
  b .                                           /* reset - infinite loop */
#endif
  ldr pc, [pc, #undef_handler_address - . - 8]
  ldr pc, [pc, #swi_handler_address - . - 8]
  ldr pc, [pc, #pabort_handler_address - . - 8]
  ldr pc, [pc, #dabort_handler_address - . - 8]
  nop
  ldr pc, [pc, #irq_handler_address - . - 8]
  ldr pc, [pc, #fiq_handler_address - . - 8]

reset_handler_address:
  .word reset_handler
undef_handler_address:
  .word undef_handler
swi_handler_address:
  .word swi_handler
#ifndef NO_DBG_HANDLER
pabort_handler_address:
  .word dbg_pabort_handler
dabort_handler_address:
  .word dbg_dabort_handler
#else
pabort_handler_address:
  .word pabort_handler
dabort_handler_address:
  .word dabort_handler
#endif
irq_handler_address:
  .word irq_handler
fiq_handler_address:
  .word fiq_handler


  .balign 64, 0
  //.ascii "CS3000 "
  .ascii __DATE__
  .ascii " @ "
  .ascii __TIME__
  .balign 32, 0



  .section .init, "ax"			/* specify that the following code goes into the .init section */
  .code 32						/* specify the ARM instruction set */
  .align 0

/******************************************************************************
 *                                                                            *
 * Default exception handlers                                                 *
 *                                                                            *
 ******************************************************************************/
reset_handler:

  // get a temporary stack pointer in place so we can make function calls.
  ldr sp, =__IRAM_segment_end__

  // Let's get the SDRAM initialized. This leads us to the NXP SDRAM init code. Which is flexible
  // and can be setup to initialize the CALSENSE SDRAM parts.
  bl board_init

  /* Map IRAM to address zero */
  ldr r0, =0x40004014
  ldr r1, =1
  str r1, [r0]

#ifndef NO_SRAM_VECTORS
  /* Copy exception vectors into IRAM */
  ldr r0, =__IRAM_segment_start__
  ldr r1, =_vectors
  ldmia r1!, {r2-r9}
  stmia r0!, {r2-r9}
  ldmia r1!, {r2-r8}
  stmia r0!, {r2-r8}
#endif


	// 2012.03.01 rmd : NOTE - in the kickstart application we have decided NOT to enable the
	// caches. Either one of them. The two defines that manage this are in the Crossworks
	// project. NO_CACHE_ENABLE and NO_ICACHE_ENABLE. With the caches enable we have the added
	// complication of flushing and disabling them prior to a jump to a new program. Which
	// afterall is what this program is all about. Jumping to the true app.
	
#ifndef NO_CACHE_ENABLE
  /* Set the tranlation table base address */
  ldr r0, =__reserved_mmu_start__
  mcr p15, 0, r0, c2, c0, 0          /* Write to TTB register */

  /* Setup the domain access control so accesses are not checked */
  ldr r0, =0xFFFFFFFF
  mcr p15, 0, r0, c3, c0, 0          /* Write to domain access control register */

  /* Create translation table */
  ldr r0, =__reserved_mmu_start__
  bl libarm_mmu_flat_initialise_level_1_table

#ifdef __FLASH_BUILD
  /* Make FLASH cacheable */
  ldr r0, =__reserved_mmu_start__
  ldr r1, =__FLASH_segment_start__
  ldr r2, =__FLASH_segment_end__ 
  sub r2, r2, r1
  cmp r2, #0x00100000
  movle r2, #0x00100000
  ldr r4, =libarm_mmu_flat_set_level_1_cacheable_region
  mov lr, pc
  bx r4
#endif

  /* Make the IRAM cacheable */
  ldr r0, =__reserved_mmu_start__
  ldr r1, =__IRAM_segment_start__ 
  ldr r2, =__IRAM_segment_end__ 
  sub r2, r2, r1
  cmp r2, #0x00100000
  movle r2, #0x00100000
  bl libarm_mmu_flat_set_level_1_cacheable_region

  /* Make the SDRAM cacheable */
  ldr r0, =__reserved_mmu_start__
  ldr r1, =__SDRAM_segment_start__ 
  ldr r2, =__SDRAM_segment_end__
  sub r2, r2, r1
  bl libarm_mmu_flat_set_level_1_cacheable_region

  /* Enable the MMU and caches */
  mrc p15, 0, r0, c1, c0, 0          /* Read MMU control register */
  orr r0, r0, #0x00001000            /* Enable ICache */
  orr r0, r0, #0x00000007            /* Enable DCache, MMU and alignment fault */
  mcr p15, 0, r0, c1, c0, 0          /* Write MMU control register */
  nop
  nop
#elif !defined(NO_ICACHE_ENABLE)
  mrc p15, 0, r0, c1, c0, 0          /* Read MMU control register */
  orr r0, r0, #0x00001000            /* Enable ICache */ 
  mcr p15, 0, r0, c1, c0, 0          /* Write MMU control register */
  nop
  nop
#endif

  // enable VFP
  mov r0, #0x40000000
  fmxr fpexc, r0
  // set VFP to RunFast mode
  mov r0, #(1<<24)|(1<<25)
  fmxr fpscr, r0
	  
  /****************************************************************************
   * Jump to the default C runtime startup code.                              *
   ****************************************************************************/
  b _start



dcache_flush:
  MRC  p15, 0, r15, c7, c10, 3
  BNE  dcache_flush
  MCR  p15, 0, r0, c7, c10, 4		/* drain write buffer */
  MOV  pc, lr


/******************************************************************************
 *                                                                            *
 * Default exception handlers                                                 *
 * These are declared weak symbols so they can be redefined in user code.     * 
 *                                                                            *
 ******************************************************************************/
  .text

undef_handler:
  b undef_handler
  
swi_handler:
  b vPortYieldProcessor
  
pabort_handler:
  b pabort_handler
  
dabort_handler:
  b dabort_handler
  
irq_handler:
  b xIRQ_Handler
  
fiq_handler:
  b xFIQ_Handler

/*  .weak undef_handler, vPortYieldProcessor, pabort_handler, dabort_handler, xIRQ_Handler, xFIQ_Handler*/

