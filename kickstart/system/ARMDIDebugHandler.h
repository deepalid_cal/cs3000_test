/*****************************************************************************
 * Copyright (c) 2006 Rowley Associates Limited.                             *
 *                                                                           *
 * This file may be distributed under the terms of the License Agreement     *
 * provided with this software.                                              *
 *                                                                           *
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE   *
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. *
 *****************************************************************************/

#ifndef ARMDIDebugHandler_h
#define ARMDIDebugHandler_h

#define ARMDI_DEBUG_HANDLER_CMD_PEEK_UINT32         0
#define ARMDI_DEBUG_HANDLER_CMD_PEEK_UINT16         1
#define ARMDI_DEBUG_HANDLER_CMD_PEEK_UINT8          2
#define ARMDI_DEBUG_HANDLER_CMD_POKE_UINT32         3
#define ARMDI_DEBUG_HANDLER_CMD_POKE_UINT16         4
#define ARMDI_DEBUG_HANDLER_CMD_POKE_UINT8          5
#define ARMDI_DEBUG_HANDLER_CMD_CRC32_UINT8         6
#define ARMDI_DEBUG_HANDLER_CMD_EXECUTE_INSTRUCTION 7
#define ARMDI_DEBUG_HANDLER_CMD_TERMINATE           8
#define ARMDI_DEBUG_HANDLER_CMD_CALL_FUNCTION       9
#define ARMDI_DEBUG_HANDLER_CMD_GET_INFO            10
#define ARMDI_DEBUG_HANDLER_CMD_STORE_STATE         11
#define ARMDI_DEBUG_HANDLER_CMD_RESTORE_STATE       12

#define ARMDI_DEBUG_HANDLER_STOP_CAUSE_PABORT       1
#define ARMDI_DEBUG_HANDLER_STOP_CAUSE_DABORT       2
#define ARMDI_DEBUG_HANDLER_STOP_CAUSE_BREAKPOINT   3      

#define ARMDI_DEBUG_HANDLER_MAGIC_NUMBER 0xF3A729C2

#endif

