/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/

#include "FreeRTOS.h"
#include "LPC32xx_intc_driver.h"

static struct
{ 
    ISR_FUNCTION pISR_Function;
    unsigned int ulISRNumber;
} xISRVectorTable[ MAX_INTERRUPTS ];

/*-----------------------------------------------------------*/
/*
 * Function to execute the highest priority ISR handler.
 */
void prvExecuteHighestPriorityISR( void )
{
unsigned ulISRindex;
    for( ulISRindex = 0; ulISRindex < MAX_INTERRUPTS; ulISRindex++ )
    {
        unsigned long ulISRIndex = xISRVectorTable[ ulISRindex ].ulISRNumber;
        if( ulISRIndex )
        {      
            /* Main interrupt controller */
            if (ulISRIndex < 32) 
            {
                if (MIC_SR & (1<<ulISRIndex))
                {
                    xISRVectorTable[ ulISRindex ].pISR_Function( );
                    /* clear edge sensitive interrupts */
                    if ( MIC_ATR & (1 << ulISRIndex ) )
                        MIC_RSR |= (1 << ulISRIndex );
                        break;
                }
            }
            /* Sub interrupt controller 1 */
            else if( ulISRIndex < 64 ) 
            {
                ulISRIndex -= 32;
                if( ( ( MIC_SR & ( 1 << 0 ) ) || ( MIC_SR & ( 1 << 30 ) ) ) && ( SIC1_SR & ( 1 << ulISRIndex ) ) )
                {
                    xISRVectorTable[ ulISRindex ].pISR_Function( );
                    if( SIC1_ATR & ( 1 << ulISRIndex ) )
                        SIC1_RSR |= ( 1 << ulISRIndex );
                        break;
                }
            }
            /* Sub interrupt controller 2 */
            else 
            {
                ulISRIndex -= 64;
                if( ( ( MIC_SR & ( 1 << 1 ) ) || ( MIC_SR & ( 1 << 31 ) ) ) && ( SIC2_SR & ( 1 << ulISRIndex ) ) )
                {
                    xISRVectorTable[ ulISRindex ].pISR_Function( );
                    if( SIC2_ATR & ( 1 << ulISRIndex ) )
                        SIC2_RSR |= ( 1 << ulISRIndex );
                        break;
                }
            }
        }
    }
}
/*-----------------------------------------------------------*/
/*
 * The global IRQ handler function.
 */
void xIRQ_Handler( void ) __attribute__ ((naked));
void xIRQ_Handler( void )
{
	portSAVE_CONTEXT( );
	__asm volatile( "bl   prvExecuteHighestPriorityISR" );
	portRESTORE_CONTEXT( );
}
/*-----------------------------------------------------------*/

/*
 * The global FIQ Handler function.
 */
void xFIQ_Handler( void ) __attribute__ ((naked));
void xFIQ_Handler( void )
{
	portSAVE_CONTEXT( );
    __asm volatile( "bl   prvExecuteHighestPriorityISR" );
	portRESTORE_CONTEXT( );
}
/*-----------------------------------------------------------*/

/*
 * Function to set the ISR handler and its properties in vector table.
 */
int xSetISR_Vector( unsigned long ulISRNumber, unsigned long ulPriority, ISR_TRIGGER_TYPE eTriggerType, ISR_FUNCTION pISR_Function, ISR_FUNCTION *pOldISR_Function )
{
unsigned long ulISRindex;
unsigned long ulFiq = ulPriority / MAX_INTERRUPTS;
volatile unsigned *ulAPR, *ulATR, *ulITR;

    ulPriority %= MAX_INTERRUPTS;
    
    switch( ulISRNumber )
    {
        case Ethernet_INT:
        case DMA_INT:
        case MSTIMER_INT:
        case IIR1_INT:
        case IIR2_INT:
        case IIR7_INT:
        case I2S1_INT:
        case I2S0_INT:
        case SSP1_INT:
        case SSP0_INT:
        case Timer3_INT:
        case Timer2_INT:
        case Timer1_INT:
        case Timer0_INT:
        case SD0_INT:
        case LCD_INT:
        case SD1_INT:
        case FLASH_INT:
        case IIR6_INT:
        case IIR5_INT:
        case IIR4_INT:
        case IIR3_INT:
        case WATCH_INT:
        case HSTIMER_INT:
        case Timer5_INT:
        case Timer4_Mcpwm_INT:
        case USB_i2c_INT:
        case USB_dev_hp_INT:
        case USB_dev_lp_INT:
        case USB_dev_dma_INT:
        case USB_host_INT:
        case USB_otg_atx_INT:
        case USB_otg_timer_INT:
        case SOFTWARE_INT:
        case SPI1_INT:
        case KEY_IRQ_INT:
        case RTC_INT:
        case I2C_1_INT:
        case I2C_2_INT:
        case PLL397_INT:
        case PLLHCLK_INT:
        case PLLUSB_INT:
        case SPI2_INT:
        case TS_AUX_INT:
        case TS_IRQ_INT:
        case TS_P_INT:
        case GPI_28_INT:
        case JTAG_COMM_RX_INT:
        case JTAG_COMM_TX_INT:
        case SYSCLK_mux_INT:
        case GPI_06_INT:
        case GPI_05_INT:
        case GPI_04_INT:
        case GPI_03_INT:
        case GPI_02_INT:
        case GPI_01_INT:
        case GPI_00_INT:
        case SPI1_DATIN_INT:
        case U5_RX_INT:
        case SDIO_INT:
        case GPI_07_INT:
        case U7_HCTS_INT:
        case GPI_19_INT:
        case GPI_09_INT:
        case GPI_08_INT:
        case Pn_GPIO_INT:
        case U2_HCTS_INT:
        case SPI2_DATIN_INT:
        case GPIO_05_INT:
        case GPIO_04_INT:
        case GPIO_03_INT:
        case GPIO_02_INT:
        case GPIO_01_INT:
        case GPIO_00_INT:
        break;
        
        default:        
            return 0;
    }

    /* Reset old ISR */
    if( pOldISR_Function )
    {
        for( ulISRindex = 0; ulISRindex < MAX_INTERRUPTS; ulISRindex++ )
        {
            if( xISRVectorTable[ ulISRindex ].ulISRNumber == ulISRNumber )
            {
                *pOldISR_Function = xISRVectorTable[ ulISRindex ].pISR_Function;
                xISRVectorTable[ ulISRindex ].ulISRNumber = 0;
                break;
            }
        } 
    }

    /* Set Vector number and ISR handler */
    xISRVectorTable[ ulPriority ].ulISRNumber = ulISRNumber;
    xISRVectorTable[ ulPriority ].pISR_Function = pISR_Function;

    /* MIC */
    if( ulISRNumber < 32 ) 
    {     
        ulAPR = &MIC_APR;
        ulATR = &MIC_ATR;
        ulITR = &MIC_ITR;
    }
    /* Sub interrupt controller 1 */
    else if( ulISRNumber < 64 ) 
    {      
        ulAPR = &SIC1_APR;
        ulATR = &SIC1_ATR;
        ulITR = &SIC1_ITR;
        ulISRNumber -= 32;
        /* Enable SIC1 irq and ulFiq */
        MIC_ER |= (1<<0)|(1<<30); 
        /* SIC1 ulFiq generates MIC ulFiq */
        MIC_ITR |= (1<<30); 
        /* Active low */
        MIC_APR &= ~(1<<0); 
        /* Level */
        MIC_ATR &= ~(1<<0); 
    }
    /* Sub interrupt controller 2 */
    else 
    {      
        ulAPR = &SIC2_APR;
        ulATR = &SIC2_ATR;
        ulITR = &SIC2_ITR;
        ulISRNumber -= 64;
        /* Enable SIC2 irq and ulFiq  */
        MIC_ER |= ( 1 << 1 ) | ( 1 << 31 ); 
        /* SIC2 ulFiq generates MIC ulFiq  */
        MIC_ITR |= ( 1 << 31 ); 
        /* Active low  */
        MIC_APR &= ~( 1 << 1 ); 
        /* Level  */
        MIC_ATR &= ~( 1 << 1 ); 
    }
    switch( eTriggerType )
    {
        case ISR_TRIGGER_FIXED:
        case ISR_TRIGGER_LOW_LEVEL:
            *ulAPR &= ~( 1 << ulISRNumber );
            *ulATR &= ~( 1 << ulISRNumber );
        break;
        case ISR_TRIGGER_HIGH_LEVEL:
            *ulAPR |= ( 1 << ulISRNumber );
            *ulATR &= ~( 1 << ulISRNumber );
        break;
        case ISR_TRIGGER_NEGATIVE_EDGE:
            *ulAPR &= ~( 1 << ulISRNumber );
            *ulATR |= ( 1 << ulISRNumber );
        break;
        case ISR_TRIGGER_POSITIVE_EDGE:
            *ulAPR |= ( 1 << ulISRNumber );
            *ulATR |= ( 1 << ulISRNumber );
        break;
        case ISR_TRIGGER_DUAL_EDGE:
             return 0;
    }  
    
    if( ulFiq )
    {
        *ulITR |= ( 1 << ulISRNumber );
    }
    else
    {
        *ulITR &= ~( 1 << ulISRNumber );
    }
    
    return 1;
}
/*-----------------------------------------------------------*/

/*
 * Function to enable ISR in interrupt controller.
 */
int xEnable_ISR( unsigned long ulISRNumber )
{
volatile unsigned *ulER;
    /* MIC  */
    if( ulISRNumber < 32 ) 
    {
        ulER = &MIC_ER;
    }
    /* SIC1 */
    else if( ulISRNumber < 64 ) 
    {
        ulER = &SIC1_ER;
        ulISRNumber -= 32;
    }
    /* SIC2    */
    else 
    {
        ulER = &SIC2_ER;
        ulISRNumber -= 64;
    }

    *ulER |= ( 1 << ulISRNumber );
    
    return 0;
}
/*-----------------------------------------------------------*/

/*
 * Function to disable ISR in interrupt controller.
 */
int xDisable_ISR( unsigned long ulISRNumber )
{
volatile unsigned *ulER;
    /* MIC  */
    if( ulISRNumber < 32 ) 
    {
        ulER = &MIC_ER;
    }
    /* SIC1 */
    else if( ulISRNumber < 64 ) 
    {
        ulER = &SIC1_ER;
        ulISRNumber -= 32;
    }
    /* SIC2    */
    else 
    {
        ulER = &SIC2_ER;
        ulISRNumber -= 64;
    }

    *ulER &= ~( 1 << ulISRNumber );
    
    return 0;
}
/*-----------------------------------------------------------*/



