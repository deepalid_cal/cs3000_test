/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/

#ifndef LPC32XX_INTC_DRIVER_H
#define LPC32XX_INTC_DRIVER_H

#include "LPC3200_intc.h"

/* Enumeration of interrupt trigger types */
typedef enum
{
  ISR_TRIGGER_FIXED,
  ISR_TRIGGER_LOW_LEVEL,
  ISR_TRIGGER_HIGH_LEVEL,
  ISR_TRIGGER_NEGATIVE_EDGE,
  ISR_TRIGGER_POSITIVE_EDGE,
  ISR_TRIGGER_DUAL_EDGE
} 
ISR_TRIGGER_TYPE;

typedef void (*ISR_FUNCTION)(void);
/*-----------------------------------------------------------*/

/*
 * Function to set the ISR handler and its properties in vector table.
 */
int xSetISR_Vector( unsigned long ulISRNumber, unsigned long ulPriority, ISR_TRIGGER_TYPE eTriggerType, ISR_FUNCTION pISR_Function, ISR_FUNCTION *pOldISR_Function );

/*-----------------------------------------------------------*/

/*
 * Function to enable ISR in interrupt controller.
 */
int xEnable_ISR( unsigned long ulISRNumber );
/*-----------------------------------------------------------*/

/*
 * Function to disable ISR in interrupt controller.
 */
int xDisable_ISR( unsigned long ulISRNumber );
/*-----------------------------------------------------------*/

#endif

