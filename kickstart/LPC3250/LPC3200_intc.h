/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/

#ifndef LPC3200_INT_H
#define LPC3200_INT_H

#include "LPC3250_chip.h"

/* interrupt numbers  */
#define Ethernet_INT 				( 29 )
#define DMA_INT 					( 28 )
#define MSTIMER_INT 				( 27 )
#define IIR1_INT 					( 26 )
#define IIR2_INT                    ( 25 )
#define IIR7_INT                    ( 24 )
#define I2S1_INT                    ( 23 )
#define I2S0_INT                    ( 22 )
#define SSP1_INT                    ( 21 )
#define SSP0_INT                    ( 20 )
#define Timer3_INT                  ( 19 )
#define Timer2_INT                  ( 18 )
#define Timer1_INT                  ( 17 )
#define Timer0_INT                  ( 16 )
#define SD0_INT                     ( 15 )
#define LCD_INT                     ( 14 )
#define SD1_INT                     ( 13 ) 
#define FLASH_INT                   ( 11 )
#define IIR6_INT                    ( 10 )
#define IIR5_INT                    ( 9 )
#define IIR4_INT                    ( 8 )
#define IIR3_INT                    ( 7 )
#define WATCH_INT                   ( 6 )
#define HSTIMER_INT                 ( 5 )
#define Timer5_INT                  ( 4 )
#define Timer4_Mcpwm_INT			( 3 )
/* SIC1 */
#define USB_i2c_INT                 ( 31+32 )
#define USB_dev_hp_INT              ( 30+32 )
#define USB_dev_lp_INT              ( 29+32 )
#define USB_dev_dma_INT             ( 28+32 )
#define USB_host_INT                ( 27+32 )
#define USB_otg_atx_INT             ( 26+32 )
#define USB_otg_timer_INT           ( 25+32 )
#define SOFTWARE_INT                ( 24+32 )
#define SPI1_INT                    ( 23+32 )
#define KEY_IRQ_INT                 ( 22+32 )
#define RTC_INT                     ( 20+32 )
#define I2C_1_INT                   ( 19+32 )
#define I2C_2_INT                   ( 18+32 )
#define PLL397_INT                  ( 17+32 )
#define PLLHCLK_INT                 ( 14+32 )
#define PLLUSB_INT                  ( 13+32 )
#define SPI2_INT                    ( 12+32 )
#define TS_AUX_INT                  ( 8+32 )
#define TS_IRQ_INT                  ( 7+32 )
#define TS_P_INT                    ( 6+32 )
#define GPI_28_INT                  ( 4+32 )
#define JTAG_COMM_RX_INT            ( 2+32 )
#define JTAG_COMM_TX_INT            ( 1+32 )
/* SIC2 */
#define SYSCLK_mux_INT              ( 31+64 )
#define GPI_06_INT                  ( 28+64 )
#define GPI_05_INT                  ( 27+64 )
#define GPI_04_INT                  ( 26+64 )
#define GPI_03_INT                  ( 25+64 )
#define GPI_02_INT                  ( 24+64 )
#define GPI_01_INT                  ( 23+64 )
#define GPI_00_INT                  ( 22+64 )
#define SPI1_DATIN_INT              ( 20+64 )
#define U5_RX_INT                   ( 19+64 )
#define SDIO_INT                    ( 18+64 )
#define GPI_07_INT                  ( 15+64 )
#define U7_HCTS_INT                 ( 12+64 )
#define GPI_19_INT                  ( 11+64 )
#define GPI_09_INT                  ( 10+64 )
#define GPI_08_INT                  ( 9+64 )
#define Pn_GPIO_INT                 ( 8+64 )
#define U2_HCTS_INT                 ( 7+64 )
#define SPI2_DATIN_INT              ( 6+64 )
#define GPIO_05_INT                 ( 5+64 )
#define GPIO_04_INT                 ( 4+64 )
#define GPIO_03_INT                 ( 3+64 )
#define GPIO_02_INT                 ( 2+64 )
#define GPIO_01_INT                 ( 1+64 )
#define GPIO_00_INT                 ( 0+64 )

#define MAX_INTERRUPTS              ( 72 ) 
#define FIQ(N)                      ( N+MAX_INTERRUPTS )

#endif
