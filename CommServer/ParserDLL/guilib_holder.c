//
// just used to get the main copy of GuiLib.c into this project
//
#ifdef _MSC_VER
#pragma pack(1)
#define __attribute__(A) /* do nothing */
#define _CRT_SECURE_NO_WARNINGS
#define snprintf _snprintf
#endif
#include <lcd_init.c>
#include <GuiDisplay.c>
#include <GuiFont.c>
#pragma warning ( disable : 4267 4244 4047 )
#include <GuiLib.c>
#pragma warning ( default : 4267 4244 4047 )
#include <GuiStruct.c>
#include <GuiVar.c>