// ParserDLL.cpp : Defines the exported functions for the DLL application.
//
#include "stdafx.h"
#include "ParserDLL.h"
#pragma pack(1)
#define __attribute__(A) /* do nothing */
#using <mscorlib.dll>
#include <msclr\auto_gcroot.h>


#undef TRUE		// so no TRUE FALSE conflict from lpc_types.h
#undef FALSE	// ditto
#include <packet_definitions.h>
#include <cent_comm.h>
#include <flow_recorder.h>
#include <report_data.h>
typedef void BY_SYSTEM_RECORD;	// to avoid needing battery_backed_vars.h 
#include <system_report_data.h>
typedef void BY_POC_RECORD;		// ditto
#include <poc_report_data.h>
#include <weather_tables.h>		// for ET_TABLE and RAIN_TABLE structs
#include <moisture_sensors.h>	// for MOISTURE_SENSOR_RECORDER_RECORD structs
#include <budget_report_data.h>		// for budget structs

//
// sent (with header) by controller to server to
// check for firmware update using MID_TO_COMMSERVER_CHECK_FOR_UPDATES
//
typedef struct
{
	UNS_16 MainCodeDate;
	UNS_32 MainCodeTime;
	UNS_16 TPCodeDate;
	UNS_32 TPCodeTime;
} CHECK_FOR_UPDATES;

//
// need to manually copy NLog.dll in use at c# code/project where it is managed with nuget to 
// this project so below "using" statement can pick it up.
//
#using "NLog.dll"

using namespace NLog;
using namespace NLog::Config;
using namespace NLog::Targets;


double Add(double a, double b)
{
	return a + b;
}

unsigned int Dummy(DUMMY_STRUCT * pp,
	const unsigned int why,
	unsigned char *dest,
	const unsigned int sz,
	unsigned int idx)
{
	if (pp->footer[0] == 'e')pp->footer[0] = 'E';

	dest[0] = 0xc7;

	if (pp->header[0] == '1')return(1);
	return(0);
}

unsigned int Dummy2(char *arg1, char *arg2, int *arg3)
{
	*arg3 = 0;
	if (strncmp(arg1, "ABC", 3) == 0)*arg3 = 33;
	if (strncmp(arg2, "DEF", 3) == 0)*arg3 = 44;

	return(-1);
}

#define AMBLES_AND_CRC_SIZE 12
int MaxSizeMessage(int mid)
{
	int ret = -1;
	Logger^ _logger;

	_logger = LogManager::GetLogger("C++MaxSizeMessage");

	switch (mid){
	case MID_TO_COMMSERVER_REGISTRATION_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Registration message");
		ret = sizeof(COMM_SERVER_REGISTRATION_STRUCT);
		break;
	case MID_TO_COMMSERVER_FLOW_RECORDING_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Flow Recording message");
		// UNS_32 is system_id up front in message
		ret = sizeof(UNS_32)+(sizeof(CS3000_FLOW_RECORDER_RECORD)*(FLOW_RECORDER_MAX_RECORDS));
		break;
	case MID_TO_COMMSERVER_CHECK_FOR_UPDATES_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Firmware Update message");
		ret = sizeof(CHECK_FOR_UPDATES); 
		break;
	case MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Verify Firmware message");
		ret = sizeof(CHECK_FOR_UPDATES);
		break;
	case MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Verify Firmware message");
		ret = sizeof(CHECK_FOR_UPDATES);
		break;
	case MID_CODE_DISTRIBUTION_main_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Code Distribution Init Ack");
		ret = 0;
		break;
	case MID_CODE_DISTRIBUTION_main_full_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Code Distribution Full Receipt Ack");
		ret = 0;
		break;
	case MID_CODE_DISTRIBUTION_tpmicro_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size TP Code Distribution Init Ack");
		ret = 0;
		break;
	case MID_CODE_DISTRIBUTION_tpmicro_full_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size TP Code Distribution Full Receipt Ack");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_ENGINEERING_ALERTS_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Engineering Alerts message");
		ret = (8*1024);		// 8K worth of alert data allowed
		break;
	case MID_TO_COMMSERVER_STATION_HISTORY_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Station History message");
		ret = (STATION_HISTORY_MAX_RECORDS*sizeof(STATION_HISTORY_RECORD));
		break;
	case MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Station Report message");
		ret = (STATION_REPORT_DATA_MAX_RECORDS * sizeof(STATION_REPORT_DATA_RECORD));
		break;
	case MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size System Report message");
		ret = (SYSTEM_REPORT_RECORDS_TO_KEEP * sizeof(SYSTEM_REPORT_RECORD));
		break;
	case MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size POC Report message");
		ret = (POC_REPORT_RECORDS_TO_KEEP * sizeof(POC_REPORT_RECORD));
		break;

	case MID_TO_COMMSERVER_WEATHER_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Weather Report message");
		ret = 1 + sizeof(ET_TABLE_ENTRY) + sizeof(RAIN_TABLE_ENTRY);	// both et and rain structs present
		break;

	case MID_TO_COMMSERVER_RAIN_INDICATION_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Rain Indication message");
		ret = 2;	// 16-bit date
		break;

	case MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Rain Shutdown ACK");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_mobile_station_ON_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Mobile ON ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_station_OFF_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Mobile OFF ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Weather Data ACK");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_status_screen_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Status Screen data");
		ret = DESIRED_PACKET_PAYLOAD;
		break;

	case MID_TO_COMMSERVER_send_a_status_screen_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Status Screen request ACK");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size swap factory reset to new panel ACK");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size swap factory reset to old panel ACK");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_set_all_bits_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Set all Bits request ACK");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size check for Lights Report message");
		ret = (LIGHTS_REPORT_DATA_MAX_RECORDS * sizeof(LIGHTS_REPORT_RECORD));
		break;
	case MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size check for ET and Rain table message");
		ret = ET_RAIN_DAYS_IN_TABLE * (sizeof(ET_TABLE_ENTRY)+sizeof(RAIN_TABLE_ENTRY));	// both et and rain structs present
		break;
	case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size check for Budgets Report message");
		ret = (BUDGET_REPORT_RECORDS_TO_KEEP * sizeof(BUDGET_REPORT_RECORD));
		break;
    case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2:
        _logger->Log( NLog::LogLevel::Info, "Max size check for Budgets Report message" );
        ret = (BUDGET_REPORT_RECORDS_TO_KEEP * (sizeof( BUDGET_REPORT_RECORD ) + sizeof(UNS_32)));
        break;
	case MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size check for Moisture Sensor Report message");
		ret = (MOISTURE_SENSOR_RECORDER_MAX_RECORDS * sizeof(MOISTURE_SENSOR_RECORDER_RECORD));
		break;


#ifdef ORIGINAL_CODE
	case MID_TO_COMMSERVER_RAIN_SHUTDOWN_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Rain Shutdown message");
		ret = 2;	// 16-bit date
		break;

	case MID_TO_COMMSERVER_RAIN_POLLING_REQUEST_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Rain Polling message");
		ret = 2;
		break;

	case MID_TO_COMMSERVER_RAIN_POLLING_SHUTDOWN_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Rain Polling Init Ack message");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_RAIN_POLLING_SHUTDOWN_data_full_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Rain Polling Full Recpt Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_WEATHER_REQUEST_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Check for Weather init message");
		ret = 1;
		break;
	case MID_TO_COMMSERVER_WEATHER_DATA_data_full_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Check for Weather Full Recpt Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_WEATHER_DATA_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Check for init Ack message");
		ret = 0;
		break;
#endif
	case MID_TO_COMMSERVER_PROGRAM_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Check for Program Data message");
		ret = (1024 * 1024);	// this is a guess. TBD
		break;
	case MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size Check for Program Data Request message");
		ret = 1;		// one byte 
		break;
	case MID_TO_COMMSERVER_PROGRAM_DATA_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Check for Program Data Init Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_PROGRAM_DATA_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Check for Program Data Full Receipt Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_force_registration_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Check for force registration Data Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_enable_or_disable_FL_option_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Enable/Disable FL option ACK");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_mobile_MVOR_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size MVOR option ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_light_cmd_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Light On/Off ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_turn_controller_on_off_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Scheduled Irrigation On/Off ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_now_days_all_stations_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Now Days all Stations ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_now_days_by_group_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Now Days by Group ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_now_days_by_box_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Now Days by Box ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_enable_or_disable_HUB_option_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Enabled/Disable Hub ACK");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_mobile_now_days_by_station_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Now Days by Station ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_clear_MLB_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Clear Mainline Break ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_stop_all_irrigation_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Stop all irrigation Break ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_stop_irrigation_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size Stop irrigation Break ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_check_for_updates_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size check for forcing to check update Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_keep_alive:
		_logger->Log(NLog::LogLevel::Info, "Max size check for keepalive");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_no_more_messages_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size check for hub No More Messages");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_hub_is_busy_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Max size check for hub Busy");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_please_send_me_the_hub_list:
		_logger->Log(NLog::LogLevel::Info, "Max size csend hub list");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_here_is_the_hub_list_ack:
		_logger->Log(NLog::LogLevel::Info, "Max size check for hub list Ack message");
		ret = 0;
		break;
	default:
		_logger->Log(NLog::LogLevel::Warn, "UNKNOWN MID");
		break;
	}
	return(ret);
}

int MinSizeMessage(int mid)
{
	int ret = -1;
	Logger^ _logger;

	_logger = LogManager::GetLogger("C++MinSizeMessage");

	switch (mid){
	case MID_TO_COMMSERVER_REGISTRATION_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Registration message");
		ret = sizeof(COMM_SERVER_REGISTRATION_STRUCT)- (16*MAX_CHAIN_LENGTH);	// version 3 is smaller than version 4 by 16 bytes per controller
		break;
	case MID_TO_COMMSERVER_FLOW_RECORDING_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Flow Recording message");
		// UNS_32 is system_id up front in message
		ret = sizeof(UNS_32)+(sizeof(CS3000_FLOW_RECORDER_RECORD)*(1));	// 1 is minimum number of records
		break;
	case MID_TO_COMMSERVER_CHECK_FOR_UPDATES_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Firmware Update message");
		ret = sizeof(CHECK_FOR_UPDATES);
		break;
	case MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Verify Firmware message");
		ret = sizeof(CHECK_FOR_UPDATES);
		break;
	case MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Verify Firmware message");
		ret = sizeof(CHECK_FOR_UPDATES);
		break;
	case MID_CODE_DISTRIBUTION_main_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Code Distribution Init Ack");
		ret = 0;
		break;
	case MID_CODE_DISTRIBUTION_main_full_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Code Distribution Full Receipt Ack");
		ret = 0;
		break;
	case MID_CODE_DISTRIBUTION_tpmicro_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size TP Code Distribution Init Ack");
		ret = 0;
		break;
	case MID_CODE_DISTRIBUTION_tpmicro_full_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size TP Code Distribution Full Receipt Ack");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_ENGINEERING_ALERTS_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Engineering Alerts message");
		ret = 8;		// smallest single alert 8 bytes
		break;
	case MID_TO_COMMSERVER_STATION_HISTORY_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Station History message");
		ret = (1 * sizeof(STATION_HISTORY_RECORD));
		break;
	case MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Station Report message");
		ret = (1 * sizeof(STATION_REPORT_DATA_RECORD));
		break;
	case MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size System Report message");
		ret = (1 * sizeof(SYSTEM_REPORT_RECORD));
		break;
	case MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size POC Report message");
		ret = (1 * sizeof(POC_REPORT_RECORD));
		break;
	case MID_TO_COMMSERVER_WEATHER_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Weather Report message");
		ret = 1;	// neither bit set means no rain or et struct
		break;
	case MID_TO_COMMSERVER_RAIN_INDICATION_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Rain Indication message");
		ret = 2;	// 16-bit date
		break;
	case MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Rain Shutdown ACK");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_mobile_station_ON_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Mobile ON ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_station_OFF_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Mobile OFF ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Weather Data ACK");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_status_screen_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Status Screen data");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_send_a_status_screen_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Status Screen request ACK");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack:
		_logger->Log(NLog::LogLevel::Info, "Min swap factory reset to new panel ACK");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack:
		_logger->Log(NLog::LogLevel::Info, "Min swap factory reset to old panel ACK");
		ret = 0;
		break;

	case MID_TO_COMMSERVER_set_all_bits_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Set all Bits request ACK");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size check for Lights Report message");
		ret = (1 * sizeof(LIGHTS_REPORT_RECORD));
		break;
	case MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size check for ET and Rain table message");
		ret = 1 + sizeof(ET_TABLE_ENTRY)+sizeof(RAIN_TABLE_ENTRY);	// both et and rain structs present
		break;
	case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size check for Budgets Report message");
		ret = (1 * sizeof(BUDGET_REPORT_RECORD));
		break;
    case MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2:
        _logger->Log( NLog::LogLevel::Info, "Min size check for Budgets Report message2 " );
        // minimum size is the sizeof(system part) + sizeof(num_poc) + sizeof(date), where num_poc = 0
        ret = sizeof( SYSTEM_BUDGET_REPORT_RECORD ) + sizeof(UNS_32) + sizeof(UNS_32);
        break;
	case MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size check for Moisture Sensor Report message");
		ret = (1 * sizeof(MOISTURE_SENSOR_RECORDER_RECORD));
        break;

#ifdef ORIGINAL_CODE
	case MID_TO_COMMSERVER_RAIN_SHUTDOWN_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Rain Shutdown message");
		ret = 2;	// 16-bit date
		break;
	case MID_TO_COMMSERVER_RAIN_POLLING_REQUEST_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Rain Polling message");
		ret = 2;
		break;
	case MID_TO_COMMSERVER_RAIN_POLLING_SHUTDOWN_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Rain Polling Init Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_RAIN_POLLING_SHUTDOWN_data_full_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Rain Polling Full Recpt Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_WEATHER_REQUEST_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Check for Weather init message");
		ret = 1;
		break;
	case MID_TO_COMMSERVER_WEATHER_DATA_data_full_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Check for Weather Full Recpt Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_WEATHER_DATA_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Check for init Ack message");
		ret = 0;
		break;
#endif
	case MID_TO_COMMSERVER_PROGRAM_DATA_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Check for Program Data message");
		ret = 0;	// this is a guess TBD
		break;
	case MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size Check for Program Data Request message");
		ret = 1;		// one byte 
		break;
	case MID_TO_COMMSERVER_PROGRAM_DATA_init_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Check for Program Data Init Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_PROGRAM_DATA_receipt_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Check for Program Data Full Receipt Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_force_registration_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Check for force registration Data Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_enable_or_disable_FL_option_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Enable/Disable FL option ACK");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_mobile_MVOR_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size MVOR option ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_light_cmd_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Light On/Off ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_turn_controller_on_off_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Scheduled Irrigation On/Off ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_now_days_all_stations_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Now Days all Stations ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_now_days_by_group_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Now Days by Group ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_mobile_now_days_by_box_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Now Days by Box ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_enable_or_disable_HUB_option_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Enable/Disable Hub option ACK");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_mobile_now_days_by_station_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Now Days by Station ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_clear_MLB_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Clear Mainline Break ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_stop_all_irrigation_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Stop all irrigation ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_stop_irrigation_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size Stop irrigation ACK");
		ret = 4;
		break;
	case MID_TO_COMMSERVER_check_for_updates_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size check for forcing to check update Ack message");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_keep_alive:
		_logger->Log(NLog::LogLevel::Info, "Min size check for keepalive");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_no_more_messages_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size check for hub No More Mesages");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_hub_is_busy_init_packet:
		_logger->Log(NLog::LogLevel::Info, "Min size check for hub Busy");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_please_send_me_the_hub_list:
		_logger->Log(NLog::LogLevel::Info, "Min size csend hub list");
		ret = 0;
		break;
	case MID_TO_COMMSERVER_here_is_the_hub_list_ack:
		_logger->Log(NLog::LogLevel::Info, "Min size check for hub list Ack message");
		ret = 0;
		break;
	default:
		_logger->Log(NLog::LogLevel::Warn, "UNKNOWN MID");
		break;
	}
	return(ret);
}

//
// return a 1 to indicate an "ACK" structure message
//
int AckOrNotMessage(int mid)
{
	int ret = 0;
	Logger^ _logger;

	_logger = LogManager::GetLogger("C++AckOrNotMessage");

	switch (mid){
	case MID_CODE_DISTRIBUTION_main_init_ack:
	case MID_CODE_DISTRIBUTION_main_full_receipt_ack:
	case MID_CODE_DISTRIBUTION_tpmicro_init_ack:
	case MID_CODE_DISTRIBUTION_tpmicro_full_receipt_ack:
	case MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack:
	case MID_TO_COMMSERVER_mobile_station_ON_ack:
	case MID_TO_COMMSERVER_mobile_station_OFF_ack:
	case MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack:
	case MID_TO_COMMSERVER_PROGRAM_DATA_init_ack:
	case MID_TO_COMMSERVER_PROGRAM_DATA_receipt_ack:
	case MID_TO_COMMSERVER_send_a_status_screen_ack:
	case MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack:
	case MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack:
	case MID_TO_COMMSERVER_set_all_bits_ack:
	case MID_TO_COMMSERVER_force_registration_ack:
	case MID_TO_COMMSERVER_enable_or_disable_FL_option_ack:
	case MID_TO_COMMSERVER_mobile_MVOR_ack:
	case MID_TO_COMMSERVER_mobile_light_cmd_ack:
	case MID_TO_COMMSERVER_mobile_turn_controller_on_off_ack:
	case MID_TO_COMMSERVER_mobile_now_days_all_stations_ack:
	case MID_TO_COMMSERVER_mobile_now_days_by_group_ack:
	case MID_TO_COMMSERVER_mobile_now_days_by_box_ack:
	case MID_TO_COMMSERVER_enable_or_disable_HUB_option_ack:
	case MID_TO_COMMSERVER_mobile_now_days_by_station_ack:
	case MID_TO_COMMSERVER_clear_MLB_ack:
	case MID_TO_COMMSERVER_stop_all_irrigation_ack:
	case MID_TO_COMMSERVER_stop_irrigation_ack:
	case MID_TO_COMMSERVER_check_for_updates_ack:
	case MID_TO_COMMSERVER_keep_alive:
	case MID_TO_COMMSERVER_here_is_the_hub_list_ack:
	case MID_TO_COMMSERVER_pause_cancel_code_distribution_ack:
		ret = 1;
		break;
	default:
		break;
	}
	return(ret);
}


size_t
strlcpy(char *dst, const char *src, size_t siz)
{
	char *d = dst;
	const char *s = src;
	size_t n = siz;

	/* Copy as many bytes as will fit */
	if (n != 0) {
		while (--n != 0) {
			if ((*d++ = *s++) == '\0')
				break;
		}
	}

	/* Not enough room in dst, add NUL and traverse rest of src */
	if (n == 0) {
		if (siz != 0)
			*d = '\0';		/* NUL-terminate dst */
		while (*s++)
			;
	}

	return(s - src - 1);	/* count does not include NUL */
}
