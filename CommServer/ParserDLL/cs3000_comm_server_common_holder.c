//
// just used to get the main copy of cs3000_comm_server_common.c into this project
//
#ifdef _MSC_VER
#pragma pack(1)
#define __attribute__(A) /* do nothing */
#define _CRT_SECURE_NO_WARNINGS
#define snprintf _snprintf
#endif
#include <cs3000_comm_server_common.c>