//
// just used to get the main copy of pdata_changes.c into this project
//
#ifdef _MSC_VER
#pragma pack(1)
#define __attribute__(A) /* do nothing */
#define _CRT_SECURE_NO_WARNINGS
#define snprintf _snprintf
#endif
#include <group_base_file.c>
#include <shared_configuration_network.c>
#include <shared_stations.c>
#include <shared_weather_control.c>
#include <shared_manual_programs.c>
#include <shared_irrigation_system.c>
#include <shared_poc.c>
#include <shared_station_groups.c>
#include <shared_lights.c>
#include <shared_moisture_sensors.c>
#include <shared_walk_thru.c>
#include <pdata_changes.c>