//
// just used to get the main copy of report_data.c and associated files into this project
//
#ifdef _MSC_VER
#pragma pack(1)
#define __attribute__(A) /* do nothing */
#define _CRT_SECURE_NO_WARNINGS
#define snprintf _snprintf
#endif

// Required to support legacy (ET2000 and ET2000e controllers)
#include <legacy_common.c>
#include <legacy_report_data.c>
#include <legacy_cmos.c>
#include <legacy_pdata.c>
#include <legacy_manual.c>
#include <legacy_lights.c>
#include <legacy_alerts.c>
#include <legacy_change_lines_600.c>
#include <legacy_change_lines_500.c>
