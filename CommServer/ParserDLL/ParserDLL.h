#pragma once

typedef struct
{
	char header[10];
	int body1;
	int body2;
	char footer[8];
}DUMMY_STRUCT;

extern "C" __declspec(dllexport) double Add(double a, double b);
extern "C" __declspec(dllexport) unsigned int Dummy(DUMMY_STRUCT *const pp,
	const unsigned int why,
	unsigned char *dest,
	const unsigned int sz,
	unsigned int idx);
extern "C" __declspec(dllexport) unsigned int Dummy2(char * arg1,
	char *arg2,
	int * arg3);
extern "C" __declspec(dllexport) int MaxSizeMessage(int mid);
extern "C" __declspec(dllexport) int MinSizeMessage(int mid);
extern "C" __declspec(dllexport) int AckOrNotMessage(int mid);
extern "C" __declspec(dllexport) unsigned int nm_ALERT_PARSING_parse_alert_and_return_length(unsigned int paid, unsigned char * ppile, unsigned int pparse_why, unsigned char *pdest_ptr, unsigned int pallowable_size, unsigned int pindex);
// all below unsigned ints are BOOL_32's
extern "C" __declspec(dllexport) unsigned int ALERTS_alert_is_visible_to_the_user(const unsigned int paid, const unsigned int pinclude_alerts, const unsigned int pinclude_changes);
