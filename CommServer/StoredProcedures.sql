CREATE PROCEDURE PrepareCS3000ProgramData
   ( 
      UserId Integer,
      NetworkId Integer,
      status Integer OUTPUT,
      error VARCHAR ( 4000 ) OUTPUT
   ) 
BEGIN 

DECLARE @UserId INTEGER;
DECLARE @NetworkId INTEGER;
DECLARE @status INTEGER;
DECLARE @error String;

DECLARE @BoxIndex INTEGER;
DECLARE @BoxesLength INTEGER;

SET @UserId  = (SELECT UserId FROM __input);
SET @NetworkId  = (SELECT NetworkId FROM __input);

SET @status=1;

@BoxIndex = 0;

TRY
  
   -- Delete old records
   DELETE FROM Temp_CS3000Networks WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Systems WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Controllers WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000ManualPrograms WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000POC WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Stations WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Weather WHERE NetworkID=@NetworkId AND UserId = @UserId;
  
   -- Network table
   INSERT INTO Temp_CS3000Networks SELECT c.*,@UserId UserId,0 EventType,'' Columns,now() CreatedDate,0 Status FROM CS3000Networks c WHERE c.NetworkID=@NetworkId;
   -- copy controllers names into CS3000Network table
   @BoxesLength = (SELECT COUNT(ControllerID) FROM CS3000Controllers WHERE NetworkID=@NetworkID);
   WHILE @BoxIndex<@BoxesLength DO
             EXECUTE IMMEDIATE 'UPDATE TEMP_CS3000Networks SET BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) +
             ' = (SELECT Name FROM CS3000Controllers WHERE BoxIndex=' + CAST (@BoxIndex AS SQL_VARCHAR) +
             ' AND NetworkID=' + CAST (@NetworkId AS SQL_VARCHAR) +
             ') WHERE NetworkID = ' + CAST (@NetworkId AS SQL_VARCHAR) +
             ' AND UserID=' + CAST (@UserId AS SQL_VARCHAR);
             @BoxIndex = @BoxIndex + 1;
   END WHILE;
  
   -- System table
   INSERT INTO Temp_CS3000Systems SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000Systems c WHERE c.NetworkID=@NetworkId;

   -- Controller table
   INSERT INTO Temp_CS3000Controllers SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000Controllers c WHERE c.NetworkID=@NetworkId; 

   -- ManualPrograms table
   INSERT INTO Temp_CS3000ManualPrograms SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000ManualPrograms c WHERE c.NetworkID=@NetworkId;

   -- POC table
   INSERT INTO Temp_CS3000POC SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000POC c WHERE c.NetworkID=@NetworkId;

   -- Station table
   INSERT INTO Temp_CS3000Stations SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000Stations c WHERE c.NetworkID=@NetworkId;

   -- Group Station table
   INSERT INTO Temp_CS3000StationGroups SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000StationGroups c WHERE c.NetworkID=@NetworkId;

   -- Weather table
   INSERT INTO Temp_CS3000Weather SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000Weather c WHERE c.NetworkID=@NetworkId;

   -- End
CATCH ADS_SCRIPT_EXCEPTION
      -- Delete records
   DELETE FROM Temp_CS3000Networks WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Systems WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Controllers WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000ManualPrograms WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000POC WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Stations WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Weather WHERE NetworkID=@NetworkId AND UserId = @UserId;
   SET @status=0;
   SET @error = __errtext;
END TRY;


INSERT INTO __output SELECT @status,@error FROM system.iota;






END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'PrepareCS3000ProgramData', 
   'COMMENT', 
   '');

CREATE PROCEDURE UpdateCS3000ProgramData
   ( 
      UserId Integer,
      NetworkId Integer,
      status Integer OUTPUT,
      error VARCHAR ( 4000 ) OUTPUT
   ) 
BEGIN 

--
-- STORED PROCEDURE
--     UpdateCS3000ProgramData
--
-- DESCRIPTION
--     Update CS3000 original program data tables from the temporary tables.
--
-- PARAMETERS
--     :UserId
--         The user id we are pulling his records from the temporary tables.
--       :NetworkId
--             The network id we are updating.
-- 
-- RETURN VALUE
--     Status
--                 0 - failed, 1 - succeeded.
--     Error
--                The exception message if the Status was 0.
-- 

DECLARE @TaskId INTEGER;
DECLARE @UserId INTEGER;
DECLARE @NetworkId INTEGER;
DECLARE @status INTEGER;
DECLARE @error String;
DECLARE @Columns String;
DECLARE sqlStmt String;
DECLARE UserIdStr VARCHAR(20);
DECLARE NetworkIDStr VARCHAR (20);
DECLARE NewGID INTEGER;
DECLARE @BoxIndex INTEGER;
DECLARE @BoxesLength INTEGER;

DECLARE nt  CURSOR AS SELECT * FROM Temp_CS3000Networks WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType = 1;
DECLARE sys CURSOR AS SELECT * FROM Temp_CS3000Systems WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType in (1,2) ORDER BY SystemID;
DECLARE mp  CURSOR AS SELECT * FROM Temp_CS3000ManualPrograms WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType in (1,2) ORDER BY ProgramID;
DECLARE poc CURSOR AS SELECT * FROM Temp_CS3000POC WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType = 1;
DECLARE wt  CURSOR AS SELECT * FROM Temp_CS3000Weather WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType = 1;
DECLARE gp  CURSOR AS SELECT * FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType in (1,2) ORDER BY StationGroupID;
DECLARE st  CURSOR AS SELECT * FROM Temp_CS3000Stations WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType = 1;

SET @UserId  = (SELECT UserId FROM __input);
SET @NetworkId  = (SELECT NetworkId FROM __input);
SET UserIdStr = CAST (@UserID AS SQL_VARCHAR);
SET NetworkIDStr = CAST (@NetworkId AS SQL_VARCHAR);

SET @status=1;
SET @error='';

BEGIN TRAN;

-- sanity check that we have records in the temporary tables for the user & network
IF ((SELECT COUNT(*) FROM Temp_CS3000Weather WHERE NetworkID=@NetworkId AND UserId = @UserId)=0 OR
    (SELECT COUNT(*) FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkId AND UserId = @UserId)=0) THEN
     SET @status=0;
     SET @error = 'No records found';
ENDIF;
  
--------------------------------------------------------
--                      Network Table                         --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN nt;

WHILE FETCH nt DO

        @Columns = (LTRIM(RTRIM(nt.Columns)));
       
        sqlStmt = 'UPDATE CS3000Networks SET ' +
              (SELECT SplitColumns( @Columns ) FROM system.iota) +
              ' FROM Temp_CS3000Networks t ' +
              ' INNER JOIN CS3000Networks p ON p.NetworkID=t.NetworkID ' + 
              'WHERE t.NetworkID=' + NetworkIDStr + ' AND t.UserID=' + UserIdStr;
       
      EXECUTE IMMEDIATE SQlStmt;
     
   -- Update BoxName(s) into CS3000Controllers & ControllerName
   @BoxIndex = 0;
  @BoxesLength = (SELECT COUNT(ControllerID) FROM CS3000Controllers WHERE NetworkID=@NetworkID);
   WHILE @BoxIndex<@BoxesLength DO
             SET NewGID = (SELECT ControllerID FROM CS3000Controllers WHERE NetworkID = @NetworkID AND BoxIndex =@BoxIndex);
             
             EXECUTE IMMEDIATE 'UPDATE CS3000Controllers SET Name =' +
                                       ' ISNULL((SELECT BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) +
                                       ' FROM Temp_CS3000Networks WHERE NetworkID = ' + NetworkIDStr +
                                       ' AND UserID=' + UserIdStr + '),'''')' +
                                       ' WHERE ControllerID=' + CAST (NewGID AS SQL_VARCHAR);
                                      
             EXECUTE IMMEDIATE 'UPDATE Controllers SET ControllerName =' +
                                       ' ISNULL((SELECT BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) +
                                       ' FROM Temp_CS3000Networks WHERE NetworkID = ' + NetworkIDStr +
                                       ' AND UserID=' + UserIdStr + '),'''')' +
                                       ' WHERE ControllerID=' + CAST (NewGID AS SQL_VARCHAR);
                                                     
             @BoxIndex = @BoxIndex + 1;
   END WHILE;
  
END WHILE;  

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
FINALLY
   CLOSE nt;
END TRY;

ENDIF;

--------------------------------------------------------
--                            System Table                          --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN sys;

WHILE FETCH sys DO

        @Columns = (LTRIM(RTRIM(sys.Columns)));

        IF sys.EventType = 1 THEN -- Update
       
        sqlStmt = 'UPDATE CS3000Systems SET ' +
              (SELECT SplitColumns( @Columns ) FROM system.iota) +
              ' FROM Temp_CS3000Systems t ' +
              ' INNER JOIN CS3000Systems p ON p.SystemID = t.SystemID AND p.NetworkID=t.NetworkID ' + 
              'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.SystemID = ' + CAST (sys.SystemID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
       
        EXECUTE IMMEDIATE SQlStmt;
       
        ELSE -- Insert
       
        NewGID = (SELECT MAX(SystemGID)+1 FROM CS3000Systems WHERE NetworkID = @NetworkID);
        sqlStmt = 'INSERT INTO CS3000Systems ' +
                      '(SystemGID,' + @Columns + ') SELECT ' +
                        CAST (NewGID AS SQL_VARCHAR) + ',' +
                        @Columns +
                        ' FROM Temp_CS3000Systems WHERE NetworkID=' + NetworkIDStr + ' AND SystemID = ' + CAST (sys.SystemID AS SQL_VARCHAR) + ' AND UserID=' + UserIdStr;
     
        EXECUTE IMMEDIATE SQlStmt;
     
        -- Update the new GID in both CS3000POC & CS3000Stations tables.
        UPDATE Temp_CS3000POC SET SystemGID = NewGID WHERE SystemGID = sys.SystemGID AND NetworkID = @NetworkID AND UserID=@UserID;
       
        UPDATE Temp_CS3000Stations SET SystemID = NewGID WHERE SystemID = sys.SystemGID AND NetworkID = @NetworkID AND UserID=@UserID;
       
        ENDIF;
       
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
FINALLY
   CLOSE sys;
END TRY;

ENDIF;

--------------------------------------------------------
--            Manual Programs Table                           --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN mp;

WHILE FETCH mp DO

        @Columns = (LTRIM(RTRIM(mp.Columns)));

        IF mp.EventType = 1 THEN -- Update
       
        sqlStmt = 'UPDATE CS3000ManualPrograms SET ' +
              (SELECT SplitColumns( @Columns ) FROM system.iota) +
              ' FROM Temp_CS3000ManualPrograms t ' +
              ' INNER JOIN CS3000ManualPrograms p ON p.ProgramID = t.ProgramID AND p.NetworkID=t.NetworkID ' + 
              'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.ProgramID = ' + CAST (mp.ProgramID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
       
        EXECUTE IMMEDIATE SQlStmt;
       
        ELSE -- Insert
       
        NewGID = (SELECT MAX(ProgramGID)+1 FROM CS3000ManualPrograms WHERE NetworkID = @NetworkID);
        sqlStmt = 'INSERT INTO CS3000ManualPrograms ' +
                      '(ProgramGID,' + @Columns + ') SELECT ' +
                        CAST (NewGID AS SQL_VARCHAR) + ',' +
                        @Columns +
                        ' FROM Temp_CS3000ManualPrograms WHERE NetworkID=' + NetworkIDStr + ' AND ProgramID = ' + CAST (mp.ProgramID AS SQL_VARCHAR) + ' AND UserID=' + UserIdStr;
       
        EXECUTE IMMEDIATE SQlStmt;
       
        -- Update Manual Program GID in Stations table
        UPDATE Temp_CS3000Stations SET ManualProgramA_ID = NewGID WHERE ManualProgramA_ID = mp.ProgramGID AND NetworkID = @NetworkID AND UserID=@UserID;
       
        UPDATE Temp_CS3000Stations SET ManualProgramB_ID = NewGID WHERE ManualProgramB_ID = mp.ProgramGID AND NetworkID = @NetworkID AND UserID=@UserID;
        
        ENDIF;
       
        
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
FINALLY
   CLOSE mp;
END TRY;

ENDIF;

--------------------------------------------------------
--                        POC Table                           --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN poc;

WHILE FETCH poc DO

        @Columns = (LTRIM(RTRIM(poc.Columns)));
       
        sqlStmt = 'UPDATE CS3000POC SET ' +
              (SELECT SplitColumns( @Columns ) FROM system.iota) +
              ' FROM Temp_CS3000POC t ' +
              ' INNER JOIN CS3000POC p ON p.POC_ID = t.POC_ID AND p.NetworkID=t.NetworkID ' + 
              'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.POC_ID = ' + CAST (poc.POC_ID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
       
        EXECUTE IMMEDIATE SQlStmt;
       
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
FINALLY
   CLOSE poc;
END TRY;

ENDIF;

--------------------------------------------------------
--                      Weather Table                         --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN wt;

WHILE FETCH wt DO

        @Columns = (LTRIM(RTRIM(wt.Columns)));
       
        sqlStmt = 'UPDATE CS3000Weather SET ' +
              (SELECT SplitColumns( @Columns ) FROM system.iota) +
              ' FROM Temp_CS3000Weather t ' +
              ' INNER JOIN CS3000Weather p ON p.NetworkID=t.NetworkID ' + 
              'WHERE t.NetworkID=' + NetworkIDStr + ' AND t.UserID=' + UserIdStr;
       
        EXECUTE IMMEDIATE SQlStmt;
       
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
FINALLY
   CLOSE wt;
END TRY;

ENDIF;

--------------------------------------------------------
--                Station Groups Table                        --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN gp;

WHILE FETCH gp DO

        @Columns = (LTRIM(RTRIM(gp.Columns)));

        IF gp.EventType = 1 THEN -- Update
       
        sqlStmt = 'UPDATE CS3000StationGroups SET ' +
              (SELECT SplitColumns( @Columns ) FROM system.iota) +
              ' FROM Temp_CS3000StationGroups t ' +
              ' INNER JOIN CS3000StationGroups p ON p.StationGroupID = t.StationGroupID AND p.NetworkID=t.NetworkID ' + 
              'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.StationGroupID = ' + CAST (gp.StationGroupID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
       
        EXECUTE IMMEDIATE SQlStmt;
       
        ELSE -- Insert
       
        NewGID = (SELECT MAX(StationGroupGID)+1 FROM CS3000StationGroups WHERE NetworkID = @NetworkID);
        sqlStmt = 'INSERT INTO CS3000StationGroups ' +
                      '(StationGroupGID,' + @Columns + ') SELECT ' +
                        CAST (NewGID AS SQL_VARCHAR) + ',' +
                        @Columns +
                        ' FROM Temp_CS3000StationGroups WHERE NetworkID=' + NetworkIDStr + ' AND StationGroupGID = ' + CAST (gp.StationGroupGID AS SQL_VARCHAR) + ' AND UserID=' + UserIdStr;
       
        EXECUTE IMMEDIATE SQlStmt;
       
        -- Update Station Group GID in Stations table
        UPDATE Temp_CS3000Stations SET StationGroupID = NewGID WHERE StationGroupID = gp.StationGroupGID AND NetworkID = @NetworkID AND UserID=@UserID;
        
        ENDIF;
       
        
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
FINALLY
   CLOSE gp;
END TRY;

ENDIF;

--------------------------------------------------------
--                      Stations Table                        --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN st;

WHILE FETCH st DO

        @Columns = (LTRIM(RTRIM(st.Columns)));
       
        sqlStmt = 'UPDATE CS3000Stations SET ' +
              (SELECT SplitColumns( @Columns ) FROM system.iota) +
              ' FROM Temp_CS3000Stations t ' +
              ' INNER JOIN CS3000Stations p ON p.StationID = t.StationID AND p.NetworkID=t.NetworkID ' + 
              'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.StationID = ' + CAST (st.StationID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
       
        EXECUTE IMMEDIATE SQlStmt;
       
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
FINALLY
   CLOSE st;
END TRY;

ENDIF;


IF @Status=1 THEN
   COMMIT WORK;
   -- Delete records
   DELETE FROM Temp_CS3000Networks WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Systems WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Controllers WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000ManualPrograms WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000POC WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Stations WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Weather WHERE NetworkID=@NetworkId AND UserId = @UserId;
ENDIF;

INSERT INTO __output SELECT @status,@error FROM system.iota;



END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'UpdateCS3000ProgramData', 
   'COMMENT', 
   '');

CREATE PROCEDURE CompleteCS3000ProgramData
   ( 
      NetworkId Integer
   ) 
BEGIN 

DECLARE NewGID INTEGER;
DECLARE @BoxIndex INTEGER;
DECLARE @BoxesLength INTEGER;
DECLARE @NetworkId INTEGER;
DECLARE NetworkIDStr VARCHAR (20);

SET @NetworkId  = (SELECT NetworkId FROM __input);

BEGIN TRAN;
  
TRY
  
   @NetworkId=140;
   @BoxIndex = 0;
   SET NetworkIDStr = CAST (@NetworkId AS SQL_VARCHAR);

   @BoxesLength = (SELECT COUNT(ControllerID) FROM CS3000Controllers WHERE NetworkID=@NetworkID);

   WHILE @BoxIndex<@BoxesLength DO
             SET NewGID = (SELECT ControllerID FROM CS3000Controllers WHERE NetworkID = @NetworkID AND BoxIndex =@BoxIndex);
            
             EXECUTE IMMEDIATE 'UPDATE CS3000Controllers SET Name =' +
                                       ' ISNULL((SELECT BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) +
                                       ' FROM CS3000Networks WHERE NetworkID = ' + NetworkIDStr + ' ),'''')' +
                                       ' WHERE ControllerID=' + CAST (NewGID AS SQL_VARCHAR);
                                      
             EXECUTE IMMEDIATE 'UPDATE Controllers SET ControllerName =' +
                                       ' ISNULL((SELECT BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) +
                                       ' FROM CS3000Networks WHERE NetworkID = ' + NetworkIDStr + '),'''')' +
                                       ' WHERE ControllerID=' + CAST (NewGID AS SQL_VARCHAR);
                                                      
             @BoxIndex = @BoxIndex + 1;
   END WHILE;
  
   // Update NotifyNewDataArrives field in Temp_CS3000Networks to 2 for the given network id
   // to notify all the users in temp tables that a new data has arrived
   UPDATE Temp_CS3000Networks SET NotifyNewDataArrives=2 WHERE NetworkID=@NetworkID;
  
CATCH ADS_SCRIPT_EXCEPTION

FINALLY
COMMIT WORK;

END TRY;



END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'CompleteCS3000ProgramData', 
   'COMMENT', 
   '');

