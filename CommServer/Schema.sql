CREATE TABLE CS3000WeatherData ( 
      NetworkID Integer,
      WeatherTimeStamp TimeStamp,
      ETValue Integer,
      ETStatus Integer,
      RainValue Integer,
      RainStatus Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000WeatherData',
   'CS3000WeatherData.adi',
   'WDNETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000WeatherDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000WeatherDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000WeatherDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000WeatherDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000WeatherDatafail');

CREATE TABLE CS3000Alerts ( 
      NetworkID Integer,
      AlertTimestamp TimeStamp,
      AlertDiagCode Integer,
      Description Char( 256 ),
      UserVisible Logical) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Alerts', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Alertsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Alerts', 
      'AlertTimestamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Alertsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Alerts', 
      'AlertDiagCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Alertsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Alerts', 
      'Description', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Alertsfail' ); 

CREATE TABLE Temp_CS3000Controllers ( 
      ControllerID Integer,
      NetworkID Integer,
      Name Char( 50 ),
      SerialNumber Integer,
      FLOption Logical,
      SSEOption Logical,
      SSE_DOption Logical,
      ElectricalStationsOnLimit Short,
      FOAL_MinutesBetweenRescanAttempts Integer,
      OM_OriginatorRetries Integer,
      OM_SecondsForStatus_FOAL Integer,
      OM_MinutesToExist Integer,
      TestSeconds Integer,
      LastAssignedDecoderSN Integer,
      WeatherOption Logical,
      HasRainBucket Logical,
      HasETgage Logical,
      HasWindGage Logical,
      WeatherCard Logical,
      WeatherTerminal Logical,
      DashM_Card Logical,
      DashM_Terminal Logical,
      DashM_CardType Logical,
      TwoWireTerminal Logical,
      PortA_DeviceIndex Integer,
      PortB_DeviceIndex Integer,
      BoxIndex Integer,
      HasRainSwitch Logical,
      HasFreezeSwitch Logical,
      UserId Integer,
      EventType Integer,
      Columns Char( 600 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

CREATE TABLE CS3000SystemRecord ( 
      SystemID Integer,
      NetworkID Integer,
      StartTimeStamp TimeStamp,
      EndTimeStamp TimeStamp,
      RainfallTotal Double( 0 ),
      RRSeconds Integer,
      RRGallons Double( 0 ),
      TestSeconds Integer,
      TestGallons Double( 0 ),
      WalkThruSeconds Integer,
      WalkThruGallons Double( 0 ),
      ManualSeconds Integer,
      ManualGallons Double( 0 ),
      ManualProgramSeconds Integer,
      ManualProgramGallons Double( 0 ),
      ProgramedIrrigSeconds Integer,
      ProgramedIrrigGallons Double( 0 ),
      NonControllerSeconds Integer,
      NonControllerGallons Double( 0 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000SystemRecord',
   'CS3000SystemRecord.adi',
   'SYSTEMID',
   'SystemID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000SystemRecordfail');

CREATE TABLE Temp_CS3000ManualPrograms ( 
      ProgramID Integer,
      NetworkID Integer,
      ProgramGID Integer,
      Name Char( 50 ),
      Deleted Logical,
      StartTime1 Integer,
      StartTime2 Integer,
      StartTime3 Integer,
      StartTime4 Integer,
      StartTime5 Integer,
      StartTime6 Integer,
      Day1 Logical,
      Day2 Logical,
      Day3 Logical,
      Day4 Logical,
      Day5 Logical,
      Day6 Logical,
      Day7 Logical,
      StartDate Date,
      StopDate Date,
      RunTime Integer,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

CREATE TABLE CS3000StationHistory ( 
      StartTimeStamp TimeStamp,
      StationID Integer,
      SystemID Integer,
      StationGroupID Integer,
      ControllerSerialNumber Integer,
      FirstCycleStartTimeStamp TimeStamp,
      LastCycleEndTimeStamp TimeStamp,
      SecondsIrrigated Integer,
      LastMeasuredCurrent Integer,
      GallonsIrrigated Double( 0 ),
      TotalRequestedMinutes Short,
      LeftOverAtStart Short,
      LeftOverAtConclusion Short,
      RainAtStartBefore Short,
      RainAtStartAfter Short,
      ActualFlowCheckShare Short,
      HiLimitFlowCheckShare Short,
      LoLimitFlowCheckShare Short,
      NumberOfRepeats Short,
      NetworkID Integer,
      BoxIndex Integer,
      FlowDataStamped Logical,
      ControllerTurnedOff Logical,
      HitStopTime Logical,
      StopKeyPressed Logical,
      CurrentShort Logical,
      CurrentNone Logical,
      CurrentLow Logical,
      CurrentHigh Logical,
      TailendsModifiedCycle Logical,
      TailendsZeroedIrrig Logical,
      FlowLow Logical,
      FlowHigh Logical,
      FlowNeverChecked Logical,
      NoWaterByManual Logical,
      NoWaterByCalendar Logical,
      MlbPrevented Logical,
      MvorClosed Logical,
      RainPrevented Logical,
      RainReducedIrrig Logical,
      RainTable Logical,
      SwitchRain Logical,
      SwitchFreeze Logical,
      WindConditions Logical,
      MoisCauseCycleSkip Logical,
      MoisMaxWaterDay Logical,
      PocShort Logical,
      MowDay Logical,
      TwoWireCable Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationHistory',
   'CS3000StationHistory.adi',
   'STATIONID',
   'StationID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'FlowDataStamped', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'ControllerTurnedOff', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'HitStopTime', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'StopKeyPressed', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'CurrentShort', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'CurrentNone', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'CurrentLow', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'CurrentHigh', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'TailendsModifiedCycle', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'TailendsZeroedIrrig', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'FlowLow', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'FlowHigh', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'FlowNeverChecked', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'NoWaterByManual', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'NoWaterByCalendar', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MlbPrevented', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MvorClosed', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'RainPrevented', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'RainReducedIrrig', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'RainTable', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'SwitchRain', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'SwitchFreeze', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'WindConditions', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MoisCauseCycleSkip', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MoisMaxWaterDay', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'PocShort', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MowDay', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'TwoWireCable', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

CREATE TABLE Temp_CS3000Networks ( 
      NetworkID Integer,
      MasterTimeStamp TimeStamp,
      CompanyID Integer,
      NetworkName Char( 50 ),
      Deleted Logical,
      LastCommunicated TimeStamp,
      MainboardForceFirmware CIChar( 255 ),
      TPMicroForceFirmware CIChar( 255 ),
      StartOfIrrigationDay Integer,
      ScheduledIrrigationIsOff Logical,
      TimeZone Integer,
      DlsInUse Logical,
      DlsSpringMonth Integer,
      DlsSpringMinDay Integer,
      DlsSpringMaxDay Integer,
      DlsFallBackMonth Integer,
      DlsFallBackMinDay Integer,
      DlsFallBackMaxDay Integer,
      RainPollingEnabled Logical,
      SendFlowRecording Logical,
      BoxName1 Char( 50 ),
      BoxName2 Char( 50 ),
      BoxName3 Char( 50 ),
      BoxName4 Char( 50 ),
      BoxName5 Char( 50 ),
      BoxName6 Char( 50 ),
      BoxName7 Char( 50 ),
      BoxName8 Char( 50 ),
      BoxName9 Char( 50 ),
      BoxName10 Char( 50 ),
      BoxName11 Char( 50 ),
      BoxName12 Char( 50 ),
      ElectricalStationOnLimit1 Integer,
      ElectricalStationOnLimit2 Integer,
      ElectricalStationOnLimit3 Integer,
      ElectricalStationOnLimit4 Integer,
      ElectricalStationOnLimit5 Integer,
      ElectricalStationOnLimit6 Integer,
      ElectricalStationOnLimit7 Integer,
      ElectricalStationOnLimit8 Integer,
      ElectricalStationOnLimit9 Integer,
      ElectricalStationOnLimit10 Integer,
      ElectricalStationOnLimit11 Integer,
      ElectricalStationOnLimit12 Integer,
      MasterSerialNumber Integer,
      ETSharingNetworkID Integer,
      RainSharingNetworkID Integer,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 ),
      CreatedDate TimeStamp,
      Status Integer,
      NotifyNewDataArrives Integer) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Temp_CS3000Networks', 
      'Status', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'Temp_CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Temp_CS3000Networks', 
      'NotifyNewDataArrives', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'Temp_CS3000Networksfail' ); 

CREATE TABLE CS3000StationDataRecord ( 
      StationID Integer,
      ControllerID Integer,
      StationGroupID Integer,
      ProgramedIrrigSeconds Integer,
      ProgramedIrrigGallons Double( 0 ),
      ManualProgramSeconds Integer,
      ManualProgramGallons Double( 0 ),
      ManualSeconds Integer,
      ManualGallons Double( 0 ),
      WalkThruSeconds Integer,
      WalkThruGallons Double( 0 ),
      TestSeconds Integer,
      TestGallons Double( 0 ),
      RRSeconds Integer,
      RRGallons Double( 0 ),
      TimeStamp TimeStamp,
      BoxIndex Integer,
      NetworkID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationDataRecord',
   'CS3000StationDataRecord.adi',
   'STATIONID',
   'StationID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

CREATE TABLE CS3000POCRecord ( 
      POC_ID Integer,
      NetworkID Integer,
      StartTimeStamp TimeStamp,
      GallonsTotal Double( 0 ),
      SecondsTotal Integer,
      IdleGallons Double( 0 ),
      IdleSeconds Integer,
      MVORGallons Double( 0 ),
      MVORSeconds Integer,
      IrriGallons Double( 0 ),
      IrriSeconds Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000POCRecord',
   'CS3000POCRecord.adi',
   'POC_ID',
   'POC_ID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POCRecord', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCRecordfail' ); 

CREATE TABLE Temp_CS3000POC ( 
      POC_ID Integer,
      SystemGID Integer,
      POC_GID Integer,
      Name Char( 50 ),
      Deleted Logical,
      BoxIndex Integer,
      TypeOfPOC Short,
      DecoderSerialNumber1 Integer,
      DecoderSerialNumber2 Integer,
      DecoderSerialNumber3 Integer,
      POC_CardConnected Logical,
      POC_TerminalConnected Logical,
      POC_Usage Integer,
      BudgetOption Short,
      BudgetPercentOfET Double( 0 ),
      BudgetMonthly1 Double( 0 ),
      BudgetMonthly2 Double( 0 ),
      BudgetMonthly3 Double( 0 ),
      BudgetMonthly4 Double( 0 ),
      BudgetMonthly5 Double( 0 ),
      BudgetMonthly6 Double( 0 ),
      BudgetMonthly7 Double( 0 ),
      BudgetMonthly8 Double( 0 ),
      BudgetMonthly9 Double( 0 ),
      BudgetMonthly10 Double( 0 ),
      BudgetMonthly11 Double( 0 ),
      BudgetMonthly12 Double( 0 ),
      MasterValveType1 Short,
      MasterValveType2 Short,
      MasterValveType3 Short,
      FlowMeterChoice1 Short,
      FlowMeterK_Value1 Integer,
      FlowMeterOffset_Value1 Integer,
      FlowMeterChoice2 Short,
      FlowMeterK_Value2 Integer,
      FlowMeterOffset_Value2 Integer,
      FlowMeterChoice3 Short,
      FlowMeterK_Value3 Integer,
      FlowMeterOffset_Value3 Integer,
      NetworkID Integer,
      NumberOfLevels Integer,
      FlowMeterReedSwitch1 Integer,
      FlowMeterReedSwitch2 Integer,
      FlowMeterReedSwitch3 Integer,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000POCfail');

CREATE TABLE CS3000ManualPrograms ( 
      ProgramID AutoInc,
      NetworkID Integer,
      ProgramGID Integer,
      Name Char( 50 ),
      Deleted Logical,
      StartTime1 Integer,
      StartTime2 Integer,
      StartTime3 Integer,
      StartTime4 Integer,
      StartTime5 Integer,
      StartTime6 Integer,
      Day1 Logical,
      Day2 Logical,
      Day3 Logical,
      Day4 Logical,
      Day5 Logical,
      Day6 Logical,
      Day7 Logical,
      StartDate Date,
      StopDate Date,
      RunTime Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000ManualPrograms',
   'CS3000ManualPrograms.adi',
   'PROGRAMID',
   'ProgramID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000ManualPrograms',
   'CS3000ManualPrograms.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Primary_Key', 
   'PROGRAMID', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'ProgramID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'ProgramGID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day1', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day2', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day3', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day4', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day5', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day6', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day7', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

CREATE TABLE CS3000POC ( 
      POC_ID AutoInc,
      SystemGID Integer,
      POC_GID Integer,
      Name Char( 50 ),
      Deleted Logical,
      BoxIndex Integer,
      TypeOfPOC Short,
      DecoderSerialNumber1 Integer,
      DecoderSerialNumber2 Integer,
      DecoderSerialNumber3 Integer,
      POC_CardConnected Logical,
      POC_TerminalConnected Logical,
      POC_Usage Integer,
      BudgetOption Short,
      BudgetPercentOfET Double( 0 ),
      BudgetMonthly1 Double( 0 ),
      BudgetMonthly2 Double( 0 ),
      BudgetMonthly3 Double( 0 ),
      BudgetMonthly4 Double( 0 ),
      BudgetMonthly5 Double( 0 ),
      BudgetMonthly6 Double( 0 ),
      BudgetMonthly7 Double( 0 ),
      BudgetMonthly8 Double( 0 ),
      BudgetMonthly9 Double( 0 ),
      BudgetMonthly10 Double( 0 ),
      BudgetMonthly11 Double( 0 ),
      BudgetMonthly12 Double( 0 ),
      MasterValveType1 Short,
      MasterValveType2 Short,
      MasterValveType3 Short,
      FlowMeterChoice1 Short,
      FlowMeterK_Value1 Integer,
      FlowMeterOffset_Value1 Integer,
      FlowMeterChoice2 Short,
      FlowMeterK_Value2 Integer,
      FlowMeterOffset_Value2 Integer,
      FlowMeterChoice3 Short,
      FlowMeterK_Value3 Integer,
      FlowMeterOffset_Value3 Integer,
      NetworkID Integer,
      NumberOfLevels Integer,
      FlowMeterReedSwitch1 Integer,
      FlowMeterReedSwitch2 Integer,
      FlowMeterReedSwitch3 Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000POC',
   'CS3000POC.adi',
   'POC_ID',
   'POC_ID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000POC',
   'CS3000POC.adi',
   'SYSTEMID',
   'SystemGID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Primary_Key', 
   'POC_ID', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_ID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'SystemGID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_GID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_CardConnected', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_CardConnected', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_TerminalConnected', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_TerminalConnected', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'FlowMeterReedSwitch1', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'FlowMeterReedSwitch2', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'FlowMeterReedSwitch3', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'CS3000POCfail' ); 

CREATE TABLE CS3000Controllers ( 
      ControllerID Integer,
      NetworkID Integer,
      Name Char( 50 ),
      SerialNumber Integer,
      FLOption Logical,
      SSEOption Logical,
      SSE_DOption Logical,
      ElectricalStationsOnLimit Short,
      FOAL_MinutesBetweenRescanAttempts Integer,
      OM_OriginatorRetries Integer,
      OM_SecondsForStatus_FOAL Integer,
      OM_MinutesToExist Integer,
      TestSeconds Integer,
      LastAssignedDecoderSN Integer,
      WeatherOption Logical,
      HasRainBucket Logical,
      HasETgage Logical,
      HasWindGage Logical,
      WeatherCard Logical,
      WeatherTerminal Logical,
      DashM_Card Logical,
      DashM_Terminal Logical,
      DashM_CardType Logical,
      TwoWireTerminal Logical,
      PortA_DeviceIndex Integer,
      PortB_DeviceIndex Integer,
      BoxIndex Integer,
      HasRainSwitch Logical,
      HasFreezeSwitch Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Controllers',
   'CS3000Controllers.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Controllers',
   'CS3000Controllers.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'SerialNumber', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'FLOption', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'SSEOption', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'SSEOption', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'SSE_DOption', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'WeatherOption', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasRainBucket', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasETgage', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasWindGage', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'WeatherCard', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'WeatherTerminal', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'DashM_Card', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'DashM_Terminal', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'DashM_CardType', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'TwoWireTerminal', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'BoxIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasRainSwitch', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasFreezeSwitch', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

CREATE TABLE CS3000Cards ( 
      CardID AutoInc,
      Slot Integer,
      CardPresent Logical,
      TBPresent Logical,
      CardErrors Integer,
      CardType Logical,
      ControllerSN Integer,
      NetworkID Integer,
      BoxIndex Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Cards',
   'CS3000Cards.adi',
   'CARDID',
   'CardID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Cards',
   'CS3000Cards.adi',
   'SLOT',
   'Slot',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Cards',
   'CS3000Cards.adi',
   'CONTROLLERSN',
   'ControllerSN',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Cardsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Cardsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Cardsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Cardsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Cardsfail');

CREATE TABLE CS3000FlowRecordingTable ( 
      NetworkID Integer,
      SystemGID Integer,
      FlowTimeStamp TimeStamp,
      ControllerSerialNum Integer,
      Station Integer,
      ExpectedFlow Integer,
      DeratedExpectedFlow Integer,
      HiLimit Integer,
      LoLimit Integer,
      PortionOfActual Integer,
      Flag Integer,
      Controller Integer) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

CREATE TABLE ControllerSites ( 
      SiteID AutoInc,
      CompanyID Integer,
      SiteName Char( 50 ),
      Deleted Logical,
      UserID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ControllerSites',
   'ControllerSites.adi',
   'SITEID',
   'SiteID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ControllerSites',
   'ControllerSites.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_Primary_Key', 
   'SITEID', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'SiteID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'CompanyID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'SiteName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

CREATE TABLE Controllers ( 
      ControllerID AutoInc,
      SiteID Integer,
      ControllerName Char( 50 ),
      RadioGroupID Integer,
      ConnectionType Short,
      Installed TimeStamp,
      LastCommunicated TimeStamp,
      Address Char( 3 ),
      BaudRate Short,
      Model Short,
      PhoneNumber Char( 25 ),
      StationsInUse Short,
      SoftwareVersion Char( 56 ),
      AlertCode Short,
      Notes Memo,
      LightsDescription1 Char( 40 ),
      LightsDescription2 Char( 40 ),
      LightsDescription3 Char( 40 ),
      LightsDescription4 Char( 40 ),
      Communicate Logical,
      WeatherShutdown Short,
      ControllerPicture Char( 150 ),
      AutoRetrieveReports Logical,
      Deleted Logical,
      UserID Integer,
      FOAL Logical,
      TimeoutAdj Short,
      CommServer Char( 50 ),
      UseCTS Logical,
      WeatherStation Short,
      ControllerImage Blob,
      ImageType Char( 50 ),
      SRInvolved Logical,
      TrainingController Logical,
      LRCommType Short,
      Latitude Char( 20 ),
      Longitude Char( 20 ),
      ETSharingControllerID Integer,
      RainSharingControllerID Integer,
      TimeZone CIChar( 50 ),
      CompanyID Integer,
      TP_Version Char( 48 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Controllers',
   'Controllers.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Controllers',
   'Controllers.adi',
   'SITEID',
   'SiteID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Controllers',
   'Controllers.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_Primary_Key', 
   'CONTROLLERID', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'ControllerName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'ConnectionType', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Installed', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Communicate', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Communicate', 'Field_Default_Value', 
      'True', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'AutoRetrieveReports', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'SRInvolved', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'SRInvolved', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'CompanyID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

CREATE TABLE CS3000RainShutdown ( 
      NetworkID Integer,
      TransmissionTime TimeStamp,
      ShutdownDate Date) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000RainShutdown',
   'CS3000RainShutdown.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000RainShutdown', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000RainShutdownfail' ); 

CREATE TABLE CS3000Networks ( 
      NetworkID Integer,
      MasterTimeStamp TimeStamp,
      CompanyID Integer,
      NetworkName Char( 50 ),
      Deleted Logical,
      LastCommunicated TimeStamp,
      MainboardForceFirmware CIChar( 255 ),
      TPMicroForceFirmware CIChar( 255 ),
      StartOfIrrigationDay Integer,
      ScheduledIrrigationIsOff Logical,
      TimeZone Integer,
      DlsInUse Logical,
      DlsSpringMonth Integer,
      DlsSpringMinDay Integer,
      DlsSpringMaxDay Integer,
      DlsFallBackMonth Integer,
      DlsFallBackMinDay Integer,
      DlsFallBackMaxDay Integer,
      RainPollingEnabled Logical,
      SendFlowRecording Logical,
      BoxName1 Char( 50 ),
      BoxName2 Char( 50 ),
      BoxName3 Char( 50 ),
      BoxName4 Char( 50 ),
      BoxName5 Char( 50 ),
      BoxName6 Char( 50 ),
      BoxName7 Char( 50 ),
      BoxName8 Char( 50 ),
      BoxName9 Char( 50 ),
      BoxName10 Char( 50 ),
      BoxName11 Char( 50 ),
      BoxName12 Char( 50 ),
      ElectricalStationOnLimit1 Integer,
      ElectricalStationOnLimit2 Integer,
      ElectricalStationOnLimit3 Integer,
      ElectricalStationOnLimit4 Integer,
      ElectricalStationOnLimit5 Integer,
      ElectricalStationOnLimit6 Integer,
      ElectricalStationOnLimit7 Integer,
      ElectricalStationOnLimit8 Integer,
      ElectricalStationOnLimit9 Integer,
      ElectricalStationOnLimit10 Integer,
      ElectricalStationOnLimit11 Integer,
      ElectricalStationOnLimit12 Integer,
      MasterSerialNumber Integer,
      ETSharingNetworkID Integer,
      RainSharingNetworkID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Networks',
   'CS3000Networks.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Networks',
   'CS3000Networks.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Primary_Key', 
   'NETWORKID', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'NetworkID', 'Comment', 
      'CS3000 Network ID', 'APPEND_FAIL', 'CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'ScheduledIrrigationIsOff', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'TimeZone', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'DlsInUse', 'Field_Default_Value', 
      'true', 'APPEND_FAIL', 'CS3000Networksfail' ); 

CREATE TABLE CS3000StationGroups ( 
      StationGroupID AutoInc,
      NetworkID Integer,
      StationGroupGID Integer,
      Name Char( 50 ),
      Deleted Logical,
      PlantType Short,
      HeadType Short,
      PrecipRate Double( 0 ),
      SoilType Short,
      SlopePercentage Double( 0 ),
      Exposure Short,
      UsableRainPercentage Double( 0 ),
      PriorityLevel Integer,
      PercentAdjust Double( 0 ),
      StartDate Date,
      EndDate Date,
      Enabled Logical,
      ScheduleType Integer,
      BeginDate Date,
      IrrigateOn29Or31 Logical,
      MowDay Integer,
      LastRan TimeStamp,
      TailEndsThrowAway Integer,
      TailEndsMakeAdjustment Integer,
      MaximumLeftover Integer,
      ET_Mode Integer,
      UseET_Averaging Logical,
      RainInUse Logical,
      MaximumAccumulation Integer,
      WindInUse Logical,
      HighFlowAction Integer,
      LowFlowAction Integer,
      On_At_A_TimeInGroup Double( 0 ),
      On_At_A_TimeInSystem Double( 0 ),
      PresentlyOnGroupCount Integer,
      PumpInUse Logical,
      LineFillTime Integer,
      ValveCloseTime Integer,
      AcquireExpecteds Logical,
      CropCoefficient1 Double( 0 ),
      CropCoefficient2 Double( 0 ),
      CropCoefficient3 Double( 0 ),
      CropCoefficient4 Double( 0 ),
      CropCoefficient5 Double( 0 ),
      CropCoefficient6 Double( 0 ),
      CropCoefficient7 Double( 0 ),
      CropCoefficient8 Double( 0 ),
      CropCoefficient9 Double( 0 ),
      CropCoefficient10 Double( 0 ),
      CropCoefficient11 Double( 0 ),
      CropCoefficient12 Double( 0 ),
      WaterDaysSun Logical,
      WaterDaysMon Logical,
      WaterDaysTue Logical,
      WaterDaysWed Logical,
      WaterDaysThu Logical,
      WaterDaysFri Logical,
      WaterDaysSat Logical,
      StartTime Integer,
      StopTime Integer,
      AllowableDepletion Integer,
      AvailableWater Integer,
      RootDepth Integer,
      SpeciesFactor Integer,
      DensityFactor Integer,
      MicroclimateFactor Integer,
      SoilIntakeRate Integer,
      AllowableSurfaceAccum Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationGroups',
   'CS3000StationGroups.adi',
   'STATIONGROUPID',
   'StationGroupID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationGroups',
   'CS3000StationGroups.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Primary_Key', 
   'STATIONGROUPID', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'StationGroupID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'StationGroupGID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'Enabled', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'Enabled', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'IrrigateOn29Or31', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'IrrigateOn29Or31', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'UseET_Averaging', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'UseET_Averaging', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'RainInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'RainInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WindInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WindInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'PumpInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'PumpInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'AcquireExpecteds', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'AcquireExpecteds', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysSun', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysSun', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysMon', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysMon', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysTue', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysTue', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysWed', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysWed', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysThu', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysThu', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysFri', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysFri', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysSat', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysSat', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

CREATE TABLE CS3000Stations ( 
      StationID AutoInc,
      NetworkID Integer,
      Name Char( 50 ),
      StationNumber Integer,
      Deleted Logical,
      CardConnected Logical,
      InUse Logical,
      DecoderSerialNumber Integer,
      DecoderOutput Integer,
      TotalMinutesRun Double( 0 ),
      CycleMinutes Double( 0 ),
      SoakMinutes Double( 0 ),
      ET_Factor Integer,
      ExpectedFlowRate Integer,
      DistributionUniformity Integer,
      NoWaterDays Integer,
      ManualProgramA Integer,
      ManualProgramB Integer,
      StationGroupID Integer,
      SystemID Integer,
      ControllerID Integer,
      ManualProgramA_ID Integer,
      ManualProgramB_ID Integer,
      BoxIndex Integer,
      MoistureBalance Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Stations',
   'CS3000Stations.adi',
   'STATIONID',
   'StationID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Stations',
   'CS3000Stations.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Primary_Key', 
   'STATIONID', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'StationID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'StationNumber', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'CardConnected', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'InUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

CREATE TABLE CS3000Systems ( 
      SystemID AutoInc,
      NetworkID Integer,
      SystemGID Integer,
      SystemName Char( 50 ),
      Deleted Logical,
      UsedForIrrig Logical,
      CapacityInUse Logical,
      CapacityWithPump Integer,
      CapacityWithoutPump Integer,
      MLBDuringIrrig Integer,
      MLBDuringMVOR Integer,
      MLBAllOtherTimes Integer,
      FlowCheckingInUse Logical,
      DerateAllowedToLock Logical,
      DerateSlotSize Integer,
      DerateMaxStationOn Integer,
      DerateNumberOfSlots Integer,
      DerateNumberOfStation Integer,
      DerateNumberOfCell Integer,
      FlowCheckingRange1 Integer,
      FlowCheckingRange2 Integer,
      FlowCheckingRange3 Integer,
      FlowTolerancePlus1 Integer,
      FlowToleranceMinus1 Integer,
      FlowTolerancePlus2 Integer,
      FlowToleranceMinus2 Integer,
      FlowTolerancePlus3 Integer,
      FlowToleranceMinus3 Integer,
      FlowTolerancePlus4 Integer,
      FlowToleranceMinus4 Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Systems',
   'CS3000Systems.adi',
   'SYSTEMID',
   'SystemID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Systems',
   'CS3000Systems.adi',
   'NETWORDID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Primary_Key', 
   'SYSTEMID', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'SystemID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'UsedForIrrig', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'UsedForIrrig', 'Comment', 
      'Used for irrigation', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'CapacityInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'CapacityWithPump', 'Field_Default_Value', 
      '200', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'CapacityWithoutPump', 'Field_Default_Value', 
      '200', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'MLBDuringIrrig', 'Field_Default_Value', 
      '400', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'MLBDuringMVOR', 'Field_Default_Value', 
      '150', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'MLBAllOtherTimes', 'Field_Default_Value', 
      '150', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowCheckingInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'DerateAllowedToLock', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'DerateNumberOfSlots', 'Comment', 
      'Derate table number of gpm slots', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'DerateNumberOfStation', 'Comment', 
      'Derate table before checking number of cell station cycles', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'DerateNumberOfCell', 'Comment', 
      'Derate table before checking number of cell iterations', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowCheckingRange1', 'Field_Default_Value', 
      '30', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowCheckingRange2', 'Field_Default_Value', 
      '65', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowCheckingRange3', 'Field_Default_Value', 
      '100', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowTolerancePlus1', 'Field_Default_Value', 
      '5', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowToleranceMinus1', 'Field_Default_Value', 
      '5', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowTolerancePlus2', 'Field_Default_Value', 
      '10', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowToleranceMinus2', 'Field_Default_Value', 
      '10', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowTolerancePlus3', 'Field_Default_Value', 
      '10', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowToleranceMinus3', 'Field_Default_Value', 
      '10', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowTolerancePlus4', 'Field_Default_Value', 
      '15', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowToleranceMinus4', 'Field_Default_Value', 
      '15', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

CREATE TABLE CS3000Weather ( 
      NetworkID Integer,
      ET_RainTableRollTime Time,
      RainBucket_isInUse Logical,
      RainBucketMaxHourly Integer,
      RainBucketMinInches Integer,
      RainBucketMax24Hour Integer,
      ETG_isInUse Logical,
      ETG_LogPulses Logical,
      ETG_HistCapPercent Integer,
      ETG_HistMinPercent Integer,
      WindGage_isInUse Logical,
      WindPauseMPG Integer,
      WindPauseMinutes Integer,
      WindResumeMPG Integer,
      WindResumeMinutes Integer,
      RainSwitch_IsInUse Logical,
      FreezeSwitch_isInUse Logical,
      UseYourOwnET Logical,
      UserDefinedState Char( 50 ),
      UserDefinedCity Char( 50 ),
      UserDefinedCounty Char( 50 ),
      ETUserMonth1 Integer,
      ETUserMonth2 Integer,
      ETUserMonth3 Integer,
      ETUserMonth4 Integer,
      ETUserMonth5 Integer,
      ETUserMonth6 Integer,
      ETUserMonth7 Integer,
      ETUserMonth8 Integer,
      ETUserMonth9 Integer,
      ETUserMonth10 Integer,
      ETUserMonth11 Integer,
      ETUserMonth12 Integer,
      ET_StateIndex Integer,
      ET_CityIndex Integer,
      ET_CountyIndex Integer,
      ETGageBoxIndex Integer,
      WindGageBoxIndex Integer,
      RainBucketBoxIndex Integer,
      HasRainSwitch1 Logical,
      HasRainSwitch2 Logical,
      HasRainSwitch3 Logical,
      HasRainSwitch4 Logical,
      HasRainSwitch5 Logical,
      HasRainSwitch6 Logical,
      HasRainSwitch7 Logical,
      HasRainSwitch8 Logical,
      HasRainSwitch9 Logical,
      HasRainSwitch10 Logical,
      HasRainSwitch11 Logical,
      HasRainSwitch12 Logical,
      HasFreezeSwitch1 Logical,
      HasFreezeSwitch2 Logical,
      HasFreezeSwitch3 Logical,
      HasFreezeSwitch4 Logical,
      HasFreezeSwitch5 Logical,
      HasFreezeSwitch6 Logical,
      HasFreezeSwitch7 Logical,
      HasFreezeSwitch8 Logical,
      HasFreezeSwitch9 Logical,
      HasFreezeSwitch10 Logical,
      HasFreezeSwitch11 Logical,
      HasFreezeSwitch12 Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Weather',
   'CS3000Weather.adi',
   'CONTROLLERID',
   'NetworkID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Primary_Key', 
   'CONTROLLERID', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'RainBucket_isInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'RainBucket_isInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'ETG_isInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'ETG_isInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'ETG_LogPulses', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'ETG_LogPulses', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'WindGage_isInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'WindGage_isInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'RainSwitch_IsInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'RainSwitch_IsInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'FreezeSwitch_isInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'FreezeSwitch_isInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch1', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch2', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch3', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch4', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch5', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch6', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch7', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch8', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch9', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch10', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch11', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch12', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch1', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch2', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch3', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch4', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch5', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch6', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch7', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch8', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch9', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch10', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch11', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch12', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

CREATE TABLE Temp_CS3000StationGroups ( 
      StationGroupID Integer,
      NetworkID Integer,
      StationGroupGID Integer,
      Name Char( 50 ),
      Deleted Logical,
      PlantType Short,
      HeadType Short,
      PrecipRate Double( 0 ),
      SoilType Short,
      SlopePercentage Double( 0 ),
      Exposure Short,
      UsableRainPercentage Double( 0 ),
      PriorityLevel Integer,
      PercentAdjust Double( 0 ),
      StartDate Date,
      EndDate Date,
      Enabled Logical,
      ScheduleType Integer,
      BeginDate Date,
      IrrigateOn29Or31 Logical,
      MowDay Integer,
      LastRan TimeStamp,
      TailEndsThrowAway Integer,
      TailEndsMakeAdjustment Integer,
      MaximumLeftover Integer,
      ET_Mode Integer,
      UseET_Averaging Logical,
      RainInUse Logical,
      MaximumAccumulation Integer,
      WindInUse Logical,
      HighFlowAction Integer,
      LowFlowAction Integer,
      On_At_A_TimeInGroup Double( 0 ),
      On_At_A_TimeInSystem Double( 0 ),
      PresentlyOnGroupCount Integer,
      PumpInUse Logical,
      LineFillTime Integer,
      ValveCloseTime Integer,
      AcquireExpecteds Logical,
      CropCoefficient1 Double( 0 ),
      CropCoefficient2 Double( 0 ),
      CropCoefficient3 Double( 0 ),
      CropCoefficient4 Double( 0 ),
      CropCoefficient5 Double( 0 ),
      CropCoefficient6 Double( 0 ),
      CropCoefficient7 Double( 0 ),
      CropCoefficient8 Double( 0 ),
      CropCoefficient9 Double( 0 ),
      CropCoefficient10 Double( 0 ),
      CropCoefficient11 Double( 0 ),
      CropCoefficient12 Double( 0 ),
      WaterDaysSun Logical,
      WaterDaysMon Logical,
      WaterDaysTue Logical,
      WaterDaysWed Logical,
      WaterDaysThu Logical,
      WaterDaysFri Logical,
      WaterDaysSat Logical,
      StartTime Integer,
      StopTime Integer,
      AllowableDepletion Integer,
      AvailableWater Integer,
      RootDepth Integer,
      SpeciesFactor Integer,
      DensityFactor Integer,
      MicroclimateFactor Integer,
      SoilIntakeRate Integer,
      AllowableSurfaceAccum Integer,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Temp_CS3000StationGroups', 
      'Deleted', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail' ); 

CREATE TABLE Temp_CS3000Stations ( 
      StationID Integer,
      NetworkID Integer,
      Name Char( 50 ),
      StationNumber Integer,
      Deleted Logical,
      CardConnected Logical,
      InUse Logical,
      DecoderSerialNumber Integer,
      DecoderOutput Integer,
      TotalMinutesRun Double( 0 ),
      CycleMinutes Double( 0 ),
      SoakMinutes Double( 0 ),
      ET_Factor Integer,
      ExpectedFlowRate Integer,
      DistributionUniformity Integer,
      NoWaterDays Integer,
      ManualProgramA Integer,
      ManualProgramB Integer,
      StationGroupID Integer,
      SystemID Integer,
      ControllerID Integer,
      ManualProgramA_ID Integer,
      ManualProgramB_ID Integer,
      BoxIndex Integer,
      MoistureBalance Integer,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

CREATE TABLE Temp_CS3000Systems ( 
      SystemID Integer,
      NetworkID Integer,
      SystemGID Integer,
      SystemName Char( 50 ),
      Deleted Logical,
      UsedForIrrig Logical,
      CapacityInUse Logical,
      CapacityWithPump Integer,
      CapacityWithoutPump Integer,
      MLBDuringIrrig Integer,
      MLBDuringMVOR Integer,
      MLBAllOtherTimes Integer,
      FlowCheckingInUse Logical,
      DerateAllowedToLock Logical,
      DerateSlotSize Integer,
      DerateMaxStationOn Integer,
      DerateNumberOfSlots Integer,
      DerateNumberOfStation Integer,
      DerateNumberOfCell Integer,
      FlowCheckingRange1 Integer,
      FlowCheckingRange2 Integer,
      FlowCheckingRange3 Integer,
      FlowTolerancePlus1 Integer,
      FlowToleranceMinus1 Integer,
      FlowTolerancePlus2 Integer,
      FlowToleranceMinus2 Integer,
      FlowTolerancePlus3 Integer,
      FlowToleranceMinus3 Integer,
      FlowTolerancePlus4 Integer,
      FlowToleranceMinus4 Integer,
      UserId Integer,
      EventType Integer,
      Columns Char( 600 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

CREATE TABLE Temp_CS3000Weather ( 
      NetworkID Integer,
      ET_RainTableRollTime Time,
      RainBucket_isInUse Logical,
      RainBucketMaxHourly Integer,
      RainBucketMinInches Integer,
      RainBucketMax24Hour Integer,
      ETG_isInUse Logical,
      ETG_LogPulses Logical,
      ETG_HistCapPercent Integer,
      ETG_HistMinPercent Integer,
      WindGage_isInUse Logical,
      WindPauseMPG Integer,
      WindPauseMinutes Integer,
      WindResumeMPG Integer,
      WindResumeMinutes Integer,
      RainSwitch_IsInUse Logical,
      FreezeSwitch_isInUse Logical,
      UseYourOwnET Logical,
      UserDefinedState Char( 50 ),
      UserDefinedCity Char( 50 ),
      UserDefinedCounty Char( 50 ),
      ETUserMonth1 Integer,
      ETUserMonth2 Integer,
      ETUserMonth3 Integer,
      ETUserMonth4 Integer,
      ETUserMonth5 Integer,
      ETUserMonth6 Integer,
      ETUserMonth7 Integer,
      ETUserMonth8 Integer,
      ETUserMonth9 Integer,
      ETUserMonth10 Integer,
      ETUserMonth11 Integer,
      ETUserMonth12 Integer,
      ET_StateIndex Integer,
      ET_CityIndex Integer,
      ET_CountyIndex Integer,
      ETGageBoxIndex Integer,
      WindGageBoxIndex Integer,
      RainBucketBoxIndex Integer,
      HasRainSwitch1 Logical,
      HasRainSwitch2 Logical,
      HasRainSwitch3 Logical,
      HasRainSwitch4 Logical,
      HasRainSwitch5 Logical,
      HasRainSwitch6 Logical,
      HasRainSwitch7 Logical,
      HasRainSwitch8 Logical,
      HasRainSwitch9 Logical,
      HasRainSwitch10 Logical,
      HasRainSwitch11 Logical,
      HasRainSwitch12 Logical,
      HasFreezeSwitch1 Logical,
      HasFreezeSwitch2 Logical,
      HasFreezeSwitch3 Logical,
      HasFreezeSwitch4 Logical,
      HasFreezeSwitch5 Logical,
      HasFreezeSwitch6 Logical,
      HasFreezeSwitch7 Logical,
      HasFreezeSwitch8 Logical,
      HasFreezeSwitch9 Logical,
      HasFreezeSwitch10 Logical,
      HasFreezeSwitch11 Logical,
      HasFreezeSwitch12 Logical,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

CREATE TABLE CS3000CommServerSettings ( 
      SetControllersClock Time,
      SendWeatherData Time) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

CREATE TABLE IHSFY_jobs ( 
      Idx AutoInc,
      Command_id Integer,
      Network_id Integer,
      User_id Integer,
      Date_created TimeStamp,
      Status Integer,
      Processing_start TimeStamp,
      Param1 Integer,
      Param2 Integer,
      Param3 Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'IHSFY_jobs',
   'IHSFY_jobs.adi',
   'NETWORK_ID',
   'Network_id',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_jobs', 
      'Network_id', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_jobsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_jobs', 
      'Date_created', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_jobsfail' ); 

CREATE TABLE IHSFY_notifications ( 
      Idx Integer,
      Command_id Integer,
      Network_id Integer,
      User_id Integer,
      Date_created TimeStamp,
      Status Integer,
      Param1 Integer,
      Param2 Integer,
      Param3 Integer,
      Param4 Blob) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'IHSFY_notifications',
   'IHSFY_notifications.adi',
   'NETWORK_ID',
   'Network_id',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_notifications', 
      'Command_id', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_notificationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_notifications', 
      'Network_id', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_notificationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_notifications', 
      'Status', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_notificationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_notifications', 
      'Status', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'IHSFY_notificationsfail' ); 

