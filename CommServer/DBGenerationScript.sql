CREATE DATABASE "output.add" ENCRYPT False
EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Version_Major',
        '2'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Version_Minor',
        '2'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Default_Table_Path',
        '.\'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Temp_Table_Path',
        '.\'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Log_In_Required',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Verify_Access_Rights',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Encrypt_Table_Password',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Encrypt_New_Table',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Enable_Internet',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Internet_Security_Level',
        '2'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Max_Failed_Attempts',
        '5'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Logins_Disabled',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Logins_Disabled_Msg',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Comment',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'User_Defined_Prop',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'FTS_Delimiters',
        
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'FTS_Noise_Words',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'FTS_Drop_Chars',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'FTS_Conditional_Chars',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Encrypt_Indexes',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Query_Log_Table',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Encrypt_Communication',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Disable_DLL_Caching',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Triggers_Disabled',
        'False'
      );

CREATE TABLE Companies ( 
      CompanyID AutoInc,
      Name CIChar( 80 ),
      Company_Address CIChar( 50 ),
      Company_Address2 CIChar( 50 ),
      Company_City CIChar( 50 ),
      Company_State CIChar( 2 ),
      Company_ZipCode CIChar( 10 ),
      Deleted Logical,
      Company_TimeZone CIChar( 50 ),
      CompanyTypes Short,
      Notes Memo,
      NotesStartDate TimeStamp,
      NotesEndDate TimeStamp,
      EmailNotes Logical,
      WebServiceKey Char( 35 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Companies',
   'Companies.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Companies',
   'Companies.adi',
   'NAME',
   'Name',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Companies', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Companiesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Companies', 
   'Table_Primary_Key', 
   'COMPANYID', 'APPEND_FAIL', 'Companiesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Companies', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Companiesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Companies', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'Companiesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Companies', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Companiesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Companies', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Companiesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Companies', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Companiesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Companies', 
      'CompanyID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Companiesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Companies', 
      'Name', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Companiesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Companies', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Companiesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Companies', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Companiesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Companies', 
      'Company_TimeZone', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Companiesfail' ); 

CREATE TABLE Usrs ( 
      UserID AutoInc,
      UserTypes Short,
      Usrnm Char( 80 ),
      Psswrd Char( 254 ),
      User_Name Char( 50 ),
      User_Company Char( 50 ),
      User_Address Char( 50 ),
      User_Address2 Char( 50 ),
      User_City Char( 50 ),
      User_State Char( 2 ),
      User_ZipCode Char( 10 ),
      User_Phone Char( 15 ),
      User_Extention Char( 10 ),
      User_Debug Logical,
      User_SendPData Logical,
      User_SavePData Logical,
      User_GetLights Logical,
      User_EditController Logical,
      User_AddControllers Logical,
      User_DeleteControllers Logical,
      User_AccessPasswords Logical,
      User_ClearMLB Logical,
      User_ClearHO Logical,
      User_SendNoW Logical,
      User_MVOR Logical,
      User_OnOff Logical,
      User_SetTimeDate Logical,
      User_ManualProgram Logical,
      User_Mobile Logical,
      CompanyID Integer,
      Deleted Logical,
      ReportSelection Char( 3000 ),
      Dashboard_DateRange Char( 18 ),
      Dashboard_Viewing Char( 14 ),
      EmailAlerts Logical,
      LastEmailDate TimeStamp,
      NotifyWindow Short,
      DefaultSortDirection Logical,
      LandingPage Short,
      TempId Char( 50 ),
      TempIdDateRequested TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Usrs',
   'Usrs.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Usrs',
   'Usrs.adi',
   'USERID',
   'UserID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Usrs',
   'Usrs.adi',
   'USRNM',
   'Usrnm',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Usrs',
   'Usrs.adi',
   'USERTYPES',
   'UserTypes',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Usrs', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Usrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Usrs', 
   'Table_Primary_Key', 
   'USERID', 'APPEND_FAIL', 'Usrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Usrs', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Usrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Usrs', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Usrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Usrs', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Usrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Usrs', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Usrsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'UserTypes', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'Usrnm', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'Psswrd', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_Debug', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_Debug', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_SendPData', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_SavePData', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_GetLights', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_EditController', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_AddControllers', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_DeleteControllers', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_AccessPasswords', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_ClearMLB', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_ClearHO', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_SendNoW', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_MVOR', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_OnOff', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_SetTimeDate', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_ManualProgram', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'User_Mobile', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'CompanyID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'Dashboard_Viewing', 'Field_Default_Value', 
      'aa', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'EmailAlerts', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'NotifyWindow', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'DefaultSortDirection', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'LandingPage', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'TempId', 'Comment', 
      'Temp id for reguest reset password', 'APPEND_FAIL', 'Usrsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Usrs', 
      'TempIdDateRequested', 'Comment', 
      'Time stamp for requestion new temp id to reset password', 'APPEND_FAIL', 'Usrsfail' ); 

CREATE TABLE CS3000ET_RainRecord ( 
      NetworkID Integer,
      WeatherDate Date,
      ETValue Integer,
      ETStatus Integer,
      RainValue Integer,
      RainStatus Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000ET_RainRecord',
   'CS3000ET_RainRecord.adi',
   'WDNETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000ET_RainRecord',
   'CS3000ET_RainRecord.adi',
   'WEATHERDATE',
   'WeatherDate',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ET_RainRecord', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000ET_RainRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ET_RainRecord', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000ET_RainRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ET_RainRecord', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000ET_RainRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ET_RainRecord', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000ET_RainRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ET_RainRecord', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000ET_RainRecordfail');

CREATE TABLE AlrtFltrGrp ( 
      GroupID AutoInc,
      FltrName Char( 50 ),
      UserID Integer,
      Deleted Logical,
      DefaultFltr Logical,
      DefaultGIS Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AlrtFltrGrp',
   'AlrtFltrGrp.adi',
   'PRIMARY_KEY',
   'UserID;GroupID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlrtFltrGrp', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'AlrtFltrGrpfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlrtFltrGrp', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'AlrtFltrGrpfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlrtFltrGrp', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AlrtFltrGrpfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlrtFltrGrp', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AlrtFltrGrpfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlrtFltrGrp', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'AlrtFltrGrpfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlrtFltrGrp', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'AlrtFltrGrpfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlrtFltrGrp', 
      'GroupID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlrtFltrGrpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlrtFltrGrp', 
      'FltrName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlrtFltrGrpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlrtFltrGrp', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlrtFltrGrpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlrtFltrGrp', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlrtFltrGrpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlrtFltrGrp', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'AlrtFltrGrpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlrtFltrGrp', 
      'DefaultFltr', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlrtFltrGrpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlrtFltrGrp', 
      'DefaultFltr', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'AlrtFltrGrpfail' ); 

CREATE TABLE ControllerSites ( 
      SiteID AutoInc,
      CompanyID Integer,
      SiteName Char( 150 ),
      Deleted Logical,
      UserID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ControllerSites',
   'ControllerSites.adi',
   'SITEID',
   'SiteID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ControllerSites',
   'ControllerSites.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_Primary_Key', 
   'SITEID', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ControllerSites', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'ControllerSitesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'SiteID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'CompanyID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'SiteName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ControllerSites', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'ControllerSitesfail' ); 

CREATE TABLE Controllers ( 
      ControllerID AutoInc,
      SiteID Integer,
      ControllerName Char( 50 ),
      RadioGroupID Integer,
      ConnectionType Short,
      Installed TimeStamp,
      LastCommunicated TimeStamp,
      Address Char( 3 ),
      BaudRate Short,
      Model Short,
      PhoneNumber Char( 25 ),
      StationsInUse Short,
      SoftwareVersion Char( 64 ),
      AlertCode Short,
      Notes Memo,
      LightsDescription1 Char( 40 ),
      LightsDescription2 Char( 40 ),
      LightsDescription3 Char( 40 ),
      LightsDescription4 Char( 40 ),
      Communicate Logical,
      WeatherShutdown Short,
      ControllerPicture Char( 150 ),
      AutoRetrieveReports Logical,
      Deleted Logical,
      UserID Integer,
      FOAL Logical,
      TimeoutAdj Short,
      CommServer Char( 50 ),
      UseCTS Logical,
      WeatherStation Short,
      ControllerImage Blob,
      ImageType Char( 50 ),
      SRInvolved Logical,
      TrainingController Logical,
      LRCommType Short,
      Latitude Char( 20 ),
      Longitude Char( 20 ),
      ETSharingControllerID Integer,
      RainSharingControllerID Integer,
      TimeZone CIChar( 50 ),
      CompanyID Integer,
      TP_Version Char( 48 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Controllers',
   'Controllers.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Controllers',
   'Controllers.adi',
   'SITEID',
   'SiteID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Controllers',
   'Controllers.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_Primary_Key', 
   'CONTROLLERID', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Controllers', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Controllersfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'ControllerName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'ConnectionType', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Installed', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Communicate', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Communicate', 'Field_Default_Value', 
      'True', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'AutoRetrieveReports', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'SRInvolved', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'SRInvolved', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Controllers', 
      'CompanyID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Controllersfail' ); 

CREATE TRIGGER UpdateRadioGroupID
   ON Controllers
   AFTER 
   INSERT 
BEGIN 
UPDATE Controllers SET RadioGroupID = 1000000000 + (CompanyID * 10000) 
WHERE Model < 7 -- only ET2000's
AND ControllerID = (SELECT ControllerId FROM __new);

END 
   NO MEMOS 
   PRIORITY 1;

CREATE TABLE CommTypes ( 
      CTIndex Integer,
      CommunicationsType Char( 50 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CommTypes',
   'CommTypes.adi',
   'CTINDEX',
   'CTIndex',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommTypes', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'CommTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommTypes', 
   'Table_Primary_Key', 
   'CTINDEX', 'APPEND_FAIL', 'CommTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommTypes', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CommTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommTypes', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CommTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommTypes', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CommTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommTypes', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CommTypesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CommTypes', 
      'CTIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CommTypesfail' ); 

CREATE TABLE Models ( 
      ModelIndex AutoInc,
      Model Char( 25 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Models',
   'Models.adi',
   'MODELINDEX',
   'ModelIndex',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Models', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Modelsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Models', 
   'Table_Primary_Key', 
   'MODELINDEX', 'APPEND_FAIL', 'Modelsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Models', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Modelsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Models', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Modelsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Models', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Modelsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Models', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Modelsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Models', 
      'ModelIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Modelsfail' ); 

CREATE TABLE AlertFiltering ( 
      UserID Integer,
      AlertFilter Integer,
      AlertState Logical,
      AlertStyle Short,
      AlertFilterGroupID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AlertFiltering',
   'AlertFiltering.adi',
   'PRIMARY_KEY',
   'UserID;AlertFilterGroupID;AlertFilter',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AlertFiltering',
   'AlertFiltering.adi',
   'FK_ALRTFLTRGRP',
   'UserID;AlertFilterGroupID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AlertFiltering',
   'AlertFiltering.adi',
   'ALERTFILTER',
   'AlertFilter',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFiltering', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'AlertFilteringfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFiltering', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'AlertFilteringfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFiltering', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AlertFilteringfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFiltering', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AlertFilteringfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFiltering', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'AlertFilteringfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFiltering', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'AlertFilteringfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFiltering', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlertFilteringfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFiltering', 
      'AlertFilter', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlertFilteringfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFiltering', 
      'AlertState', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlertFilteringfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFiltering', 
      'AlertState', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'AlertFilteringfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFiltering', 
      'AlertStyle', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlertFilteringfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFiltering', 
      'AlertFilterGroupID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlertFilteringfail' ); 

CREATE TABLE AlertFilterOptions ( 
      FilterID Integer,
      ParentID Integer,
      Display Char( 100 ),
      Sort Integer,
      Source Integer,
      Style Short) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AlertFilterOptions',
   'AlertFilterOptions.adi',
   'FILTERID',
   'FilterID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AlertFilterOptions',
   'AlertFilterOptions.adi',
   'PARENTID',
   'ParentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AlertFilterOptions',
   'AlertFilterOptions.adi',
   'SORT',
   'Sort',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptions', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'AlertFilterOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptions', 
   'Table_Primary_Key', 
   'FILTERID', 'APPEND_FAIL', 'AlertFilterOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptions', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AlertFilterOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptions', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AlertFilterOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptions', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'AlertFilterOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptions', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'AlertFilterOptionsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFilterOptions', 
      'FilterID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlertFilterOptionsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFilterOptions', 
      'Display', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlertFilterOptionsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFilterOptions', 
      'Style', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'AlertFilterOptionsfail' ); 

CREATE TABLE UsrTypes ( 
      UserTypeID Integer,
      Description CIChar( 50 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'UsrTypes',
   'USRTYPES.ADI',
   'USERTYPEID',
   'UserTypeID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'UsrTypes', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'UsrTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'UsrTypes', 
   'Table_Primary_Key', 
   'USERTYPEID', 'APPEND_FAIL', 'UsrTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'UsrTypes', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'UsrTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'UsrTypes', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'UsrTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'UsrTypes', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'UsrTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'UsrTypes', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'UsrTypesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'UsrTypes', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'UsrTypesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'UsrTypes', 
      'UserTypeID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'UsrTypesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'UsrTypes', 
      'Description', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'UsrTypesfail' ); 

CREATE TABLE GeneralHistory ( 
      HistoryID AutoInc,
      HistoryTimeStamp TimeStamp,
      ControllerID Integer,
      TaskID Integer,
      CommServer Char( 50 ),
      UserID Integer,
      HistoryErrorCode Short,
      SpeedCommunications Logical,
      EndTimeStamp TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'GeneralHistory',
   'GeneralHistory.adi',
   'HISTORYID',
   'HistoryID',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'GeneralHistory',
   'GeneralHistory.adi',
   'HISTORYTIMESTAMP',
   'HistoryTimeStamp',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'GeneralHistory',
   'GeneralHistory.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'GeneralHistory',
   'GeneralHistory.adi',
   'TASKID',
   'TaskID',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'GeneralHistory',
   'GeneralHistory.adi',
   'USERID',
   'UserID',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'GeneralHistory', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'GeneralHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'GeneralHistory', 
   'Table_Default_Index', 
   'HistoryTimeStamp', 'APPEND_FAIL', 'GeneralHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'GeneralHistory', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'GeneralHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'GeneralHistory', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'GeneralHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'GeneralHistory', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'GeneralHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'GeneralHistory', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'GeneralHistoryfail');

CREATE TABLE History ( 
      HistoryID AutoInc,
      HistoryTimeStamp TimeStamp,
      Controller Char( 50 ),
      HistoryErrorMsg Char( 100 ),
      HistoryFunction Char( 50 ),
      Communications Char( 25 ),
      HistoryRoms Char( 15 ),
      HistoryModel Char( 15 ),
      HistorySuccess Logical,
      HistoryErrorCode Short,
      UserID Integer,
      ControllerID Integer,
      CommServer Char( 50 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'History',
   'History.adi',
   'HISTORYID',
   'HistoryID',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'History',
   'History.adi',
   'HISTORYTIMESTAMP',
   'HistoryTimeStamp',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'History',
   'History.adi',
   'USERID',
   'UserID',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'History',
   'History.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'History', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Historyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'History', 
   'Table_Default_Index', 
   'HistoryID', 'APPEND_FAIL', 'Historyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'History', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Historyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'History', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Historyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'History', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Historyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'History', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Historyfail');

CREATE TABLE Alerts ( 
      ControllerID Integer,
      AlertTimeStamp TimeStamp,
      AlertTimeStampIndex Integer,
      AlertDiagCode Integer,
      AlertData Raw( 200 ),
      RetrievalDate Date,
      AlertStyle Short,
      DuplicateIndex Short,
      Series600 Short) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'ALERTTIMESTAMP',
   'AlertTimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'ALERTTIMESTAMPINDEX',
   'AlertTimeStampIndex',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'ALERTDIAGCODE',
   'AlertDiagCode',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'RETRIEVALDATE',
   'RetrievalDate',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'ALERTSTYLE',
   'AlertStyle',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'DUPLICATEINDEX',
   'DuplicateIndex',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'SERIES600',
   'Series600',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Alerts', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Alerts', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Alerts', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Alerts', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Alerts', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Alertsfail');

CREATE TABLE TaskData ( 
      TaskID AutoInc,
      TaskName Char( 50 ),
      UserID Integer,
      CompanyID Integer,
      RefUnique CIChar( 50 ),
      RefTimeZone CIChar( 50 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskData',
   'TaskData.adi',
   'USERID',
   'UserID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskData',
   'TaskData.adi',
   'TASKID',
   'TaskID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskData',
   'TaskData.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskData', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'TaskDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskData', 
   'Table_Primary_Key', 
   'TASKID', 'APPEND_FAIL', 'TaskDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'TaskDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'TaskDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'TaskDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'TaskDatafail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskData', 
      'TaskID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskData', 
      'TaskName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskData', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskData', 
      'CompanyID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskDatafail' ); 

CREATE TABLE TaskExclusion ( 
      TaskTime Time,
      TaskDate Date,
      TaskID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskExclusion',
   'TaskExclusion.adi',
   'TASKID',
   'TaskID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskExclusion',
   'TaskExclusion.adi',
   'PRIMARY_KEY',
   'TaskDate;TaskTime;TaskID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskExclusion', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'TaskExclusionfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskExclusion', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'TaskExclusionfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskExclusion', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'TaskExclusionfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskExclusion', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'TaskExclusionfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskExclusion', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'TaskExclusionfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskExclusion', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'TaskExclusionfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskExclusion', 
      'TaskTime', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskExclusionfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskExclusion', 
      'TaskDate', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskExclusionfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskExclusion', 
      'TaskID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskExclusionfail' ); 

CREATE TABLE TaskOptions ( 
      OptionID Integer,
      TaskID Integer,
      ControllerID Integer,
      Options Char( 50 ),
      ServerID Integer,
      CompletionTimestamp TimeStamp,
      TaskIndex AutoInc) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskOptions',
   'TaskOptions.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskOptions',
   'TaskOptions.adi',
   'PRIMARY_KEY',
   'TaskID;ControllerID;OptionID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskOptions',
   'TaskOptions.adi',
   'TASKID',
   'TaskID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskOptions',
   'TaskOptions.adi',
   'OPTIONID',
   'OptionID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskOptions', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'TaskOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskOptions', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'TaskOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskOptions', 
   'Table_Default_Index', 
   'ControllerID', 'APPEND_FAIL', 'TaskOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskOptions', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'TaskOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskOptions', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'TaskOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskOptions', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'TaskOptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskOptions', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'TaskOptionsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskOptions', 
      'OptionID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskOptionsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskOptions', 
      'TaskID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskOptionsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskOptions', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskOptionsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskOptions', 
      'TaskIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskOptionsfail' ); 

CREATE TABLE TaskSchedule ( 
      StartDate Date,
      ScheduleTime Time,
      EndTimeStamp TimeStamp,
      Repeating Integer,
      TaskID Integer,
      UserID Integer,
      TimeZone CIChar( 50 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskSchedule',
   'TaskSchedule.adi',
   'USERID',
   'UserID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskSchedule',
   'TaskSchedule.adi',
   'START_DATE_TIME',
   'StartDate;ScheduleTime',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskSchedule',
   'TaskSchedule.adi',
   'PRIMARY_KEY',
   'TaskID;StartDate;ScheduleTime;Repeating',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskSchedule',
   'TaskSchedule.adi',
   'TASKID',
   'TaskID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskSchedule', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'TaskSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskSchedule', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'TaskSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskSchedule', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'TaskSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskSchedule', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'TaskSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskSchedule', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'TaskSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskSchedule', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'TaskSchedulefail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskSchedule', 
      'StartDate', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskSchedulefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskSchedule', 
      'ScheduleTime', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskSchedulefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskSchedule', 
      'Repeating', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskSchedulefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskSchedule', 
      'TaskID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskSchedulefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskSchedule', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskSchedulefail' ); 

CREATE TABLE ETComparing ( 
      UseETComparing Logical,
      JanMinET Double( 0 ),
      JanShareMin Logical,
      FebMinET Double( 0 ),
      FebShareMin Logical,
      MarMinET Double( 0 ),
      MarShareMin Logical,
      AprMinET Double( 0 ),
      AprShareMin Logical,
      MayMinET Double( 0 ),
      MayShareMin Logical,
      JuneMinET Double( 0 ),
      JuneShareMin Logical,
      JulyMinET Double( 0 ),
      JulyShareMin Logical,
      AugMinET Double( 0 ),
      AugShareMin Logical,
      SepMinET Double( 0 ),
      SepShareMin Logical,
      OctMinET Double( 0 ),
      OctShareMin Logical,
      NovMinET Double( 0 ),
      NovShareMin Logical,
      DecMinET Double( 0 ),
      DecShareMin Logical,
      Latitude Char( 20 ),
      Longitude Char( 20 ),
      UseSystemWideLatAndLong Logical,
      ChangesEnabled Logical) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETComparing', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'ETComparingfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETComparing', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ETComparingfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETComparing', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ETComparingfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETComparing', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ETComparingfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETComparing', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'ETComparingfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ETComparing', 
      'ChangesEnabled', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ETComparingfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ETComparing', 
      'ChangesEnabled', 'Field_Default_Value', 
      'Yes', 'APPEND_FAIL', 'ETComparingfail' ); 

CREATE TABLE Fnctns ( 
      OptionID AutoInc,
      OptionDescription Char( 50 ),
      MinimumLevel Short,
      DialogIndex Short) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Fnctns',
   'Fnctns.adi',
   'OPTIONID',
   'OptionID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Fnctns', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Fnctnsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Fnctns', 
   'Table_Primary_Key', 
   'OPTIONID', 'APPEND_FAIL', 'Fnctnsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Fnctns', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Fnctnsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Fnctns', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Fnctnsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Fnctns', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Fnctnsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Fnctns', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Fnctnsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Fnctns', 
      'OptionID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Fnctnsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Fnctns', 
      'OptionDescription', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Fnctnsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Fnctns', 
      'MinimumLevel', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Fnctnsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Fnctns', 
      'DialogIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Fnctnsfail' ); 

CREATE TABLE CMOS ( 
      ControllerID Integer,
      Version Short,
      CMOSSize Short,
      A_BaudRate Short,
      Protocol Short,
      Comm_Type Short,
      Number_Of_Stations Short,
      KeyPad Short,
      LightsEnabled Logical,
      GInterface Logical,
      RBInterface Logical,
      FInterface Logical,
      DemoAllOptions Logical,
      SizeOfRam Short,
      UseSOutDelay Logical,
      NumOfRings Short,
      DTMF_Installed Logical,
      WGInterface Logical,
      UseCTS Logical,
      BuzzerType Short,
      Reserved1 Logical,
      Reserved2 Logical,
      Reserved3 Logical,
      B_BaudRate Short,
      BoardRevision Short,
      Debuging Integer,
      BadChecksum Logical,
      BadVersion Logical,
      OM_Originator_Retries Short,
      OM_Seconds_for_Status_FOAL Short,
      OM_Minutes_To_Exist Short,
      FOAL_MinutesBetweenRescanAttempts Short,
      COMM_Address Char( 3 ),
      NormalLevelsA Short,
      NormalLevelsB Short,
      NormalLevelsC Short,
      NormalLevelsD Short,
      C_BaudRate Short,
      D_BaudRate Short,
      StationsAllowed Short,
      GpmSlotSize Short,
      MaxStationsOn Short,
      NumberOfGpmSlots Short,
      NumberOfStationCycles Short,
      CellIterations Short,
      CentralMajorVersion Short,
      CentralMinorVersion Short,
      CentralReleaseVersion Short,
      CentralBuildVersion Short,
      ControllerSerialNumber Integer,
      CommTypeA Short,
      CommTypeB Short,
      CommTypeC Short,
      CommTypeD Short,
      RRe_Enabled Logical,
      ThisControllerIsAHub Short,
      PortASRIsAMaster Logical,
      PortBSRIsAMaster Logical,
      HysteresisPercentage Short,
      Spacer Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CMOS',
   'CMOS.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CMOS', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'CMOSfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CMOS', 
   'Table_Primary_Key', 
   'CONTROLLERID', 'APPEND_FAIL', 'CMOSfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CMOS', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CMOSfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CMOS', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CMOSfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CMOS', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CMOSfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CMOS', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CMOSfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CMOS', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CMOSfail' ); 

CREATE TABLE CommWght ( 
      CommType Integer,
      CommWeight Integer,
      ServerID Integer,
      DWWeight Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CommWght',
   'CommWght.adi',
   'COMMTYPE',
   'CommType',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CommWght',
   'CommWght.adi',
   'SERVERID',
   'ServerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommWght', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'CommWghtfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommWght', 
   'Table_Default_Index', 
   'CommType', 'APPEND_FAIL', 'CommWghtfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommWght', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CommWghtfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommWght', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CommWghtfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommWght', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CommWghtfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommWght', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CommWghtfail');

CREATE TABLE DailyData ( 
      ControllerID Integer,
      StartTimeStamp TimeStamp,
      EndTimeStamp TimeStamp,
      HistoricalETNumber Double( 0 ),
      GageET Double( 0 ),
      GageETStatus Integer,
      TotalRain Double( 0 ),
      UsableRainStatus Integer,
      UsableRainRain Double( 0 ),
      Budget Double( 0 ),
      TestSeconds Integer,
      TestGallons Double( 0 ),
      ManualSeconds Integer,
      ManualGallons Double( 0 ),
      RRSeconds Integer,
      RRGallons Double( 0 ),
      NonControllerSeconds Integer,
      NonControllerGallons Double( 0 ),
      ScheduledSeconds Integer,
      ScheduledGallons Double( 0 ),
      FOALSystemGallons Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'DailyData',
   'DailyData.adi',
   'PRIMARY_KEY',
   'ControllerID;StartTimeStamp',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'DailyData',
   'DailyData.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'DailyData', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'DailyDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'DailyData', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'DailyDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'DailyData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'DailyDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'DailyData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'DailyDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'DailyData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'DailyDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'DailyData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'DailyDatafail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'DailyData', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'DailyDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'DailyData', 
      'StartTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'DailyDatafail' ); 

CREATE TABLE PastSchedule ( 
      ScheduleTimeStamp TimeStamp,
      TaskID Integer,
      UserID Integer,
      CommServerDescription Char( 50 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'PastSchedule',
   'PastSchedule.adi',
   'TASKID',
   'TaskID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'PastSchedule',
   'PastSchedule.adi',
   'PRIMARY_KEY',
   'ScheduleTimeStamp;TaskID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'PastSchedule',
   'PastSchedule.adi',
   'USERID',
   'UserID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'PastSchedule', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'PastSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PastSchedule', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'PastSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PastSchedule', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'PastSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PastSchedule', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'PastSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PastSchedule', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'PastSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PastSchedule', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'PastSchedulefail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'PastSchedule', 
      'ScheduleTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'PastSchedulefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'PastSchedule', 
      'TaskID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'PastSchedulefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'PastSchedule', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'PastSchedulefail' ); 

CREATE TABLE POCReportData ( 
      ControllerID Integer,
      POCID Short,
      StartTimeStamp TimeStamp,
      EndTimeStamp TimeStamp,
      Name Char( 40 ),
      IdleGallons Integer,
      IdleSeconds Integer,
      UseGallons Integer,
      UseSeconds Integer,
      IrriGallons Integer,
      IrriSeconds Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'POCReportData',
   'POCReportData.adi',
   'POCID',
   'POCID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'POCReportData',
   'POCReportData.adi',
   'STARTTIMESTAMP',
   'StartTimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'POCReportData',
   'POCReportData.adi',
   'PRIMARY_KEY',
   'ControllerID;POCID;StartTimeStamp',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCReportData', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'POCReportDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCReportData', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'POCReportDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCReportData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'POCReportDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCReportData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'POCReportDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCReportData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'POCReportDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCReportData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'POCReportDatafail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCReportData', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCReportDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCReportData', 
      'POCID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCReportDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCReportData', 
      'StartTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCReportDatafail' ); 

CREATE TABLE AlertFilterOptionDetails ( 
      AlertID Integer,
      OptionFilterID Integer,
      VariableName CIChar( 50 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AlertFilterOptionDetails',
   'AlertFilterOptionDetails.adi',
   'ALERTID',
   'AlertID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AlertFilterOptionDetails',
   'AlertFilterOptionDetails.adi',
   'OPTIONFILTERID',
   'OptionFilterID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptionDetails', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptionDetails', 
   'Table_Primary_Key', 
   'ALERTID', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptionDetails', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptionDetails', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptionDetails', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptionDetails', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AlertFilterOptionDetails', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFilterOptionDetails', 
      'AlertID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFilterOptionDetails', 
      'AlertID', 'Comment', 
      'ID of Alert as sent by controller', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFilterOptionDetails', 
      'OptionFilterID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AlertFilterOptionDetails', 
      'OptionFilterID', 'Comment', 
      'Group for selecting filters', 'APPEND_FAIL', 'AlertFilterOptionDetailsfail' ); 

CREATE TABLE Assignment ( 
      UserID Integer,
      ControllerID Integer,
      GatheredDate Date,
      Viewed Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Assignment',
   'Assignment.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Assignment',
   'Assignment.adi',
   'PRIMARY_KEY',
   'UserID;ControllerID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Assignment', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Assignmentfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Assignment', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'Assignmentfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Assignment', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Assignmentfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Assignment', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Assignmentfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Assignment', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Assignmentfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Assignment', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Assignmentfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Assignment', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Assignmentfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Assignment', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Assignmentfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Assignment', 
      'GatheredDate', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Assignmentfail' ); 

CREATE TABLE BulkPgmData1 ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      ETOption Logical,
      ETCountyIndex Integer,
      ETCityIndex Integer,
      RunWeek Integer,
      PumpUsage Short,
      FlowMeter Short,
      TypeOfMV Short,
      LearnGPMs Logical,
      IrrigMLB Double( 0 ),
      NonIrrigMLB Double( 0 ),
      MainLineBreak Logical,
      FullYear Logical,
      Password1 Short,
      Password2 Short,
      Password3 Short,
      Password Char( 3 ),
      MasterUnit Logical,
      MasterMLB Double( 0 ),
      ControllerOff Logical,
      CommAddr Char( 5 ),
      LightsDef Raw( 152 ),
      RunTempProg Logical,
      RunTempTill TimeStamp,
      UseBudget Logical,
      BudgetOption Short,
      PercentOfETo Double( 0 ),
      UseDailyET Logical,
      DailyETDate TimeStamp,
      UsingAnETGage Logical,
      ComBaudRate Integer,
      WS_InUse Logical,
      WS_Pause Logical,
      WS_P_Speed Double( 0 ),
      WS_R_Speed Double( 0 ),
      WS_P_Time Integer,
      WS_R_Time Integer,
      Jumpers_Demonstrator Logical,
      Jumpers_UsingPager Logical,
      Jumpers_FM_FM Logical,
      Jumpers_FM_ET Logical,
      Jumpers_RG_ET Logical,
      Jumpers_WS_ET Logical,
      Jumpers_D1 Logical,
      Jumpers_D2 Logical,
      Jumpers_D3 Logical,
      Jumpers_D4 Logical,
      LogETPulses Logical,
      DLSavings Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'BulkPgmData1',
   'BulkPgmData1.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData1', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'BulkPgmData1fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData1', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'BulkPgmData1fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData1', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'BulkPgmData1fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData1', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'BulkPgmData1fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData1', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'BulkPgmData1fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData1', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'BulkPgmData1fail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData1', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'BulkPgmData1fail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData1', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'BulkPgmData1fail' ); 

CREATE TABLE BulkPgmData2 ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      RainSwitchInUse Logical,
      EnableHOTime Logical,
      HOTime Time,
      RainInfo_InUse Logical,
      RainInfo_HookedUp Logical,
      RainInfo_MaxHourlyRate Double( 0 ),
      RainInfo_Maximum Double( 0 ),
      RainInfo_Minimum Double( 0 ),
      RainInfo_Status Short,
      RainInfo_Dummy2 Logical,
      RainInfo_RBTableDate Date,
      RainInfo_Dummy4 Integer,
      CentralRadio Char( 16 ),
      XFlowMeter_1 Short,
      XFlowMeter_2 Short,
      MaxFlowNumber Double( 0 ),
      StationOrder Logical,
      ManCycleSoak Logical,
      DailyETUse12 Logical,
      AllowNegHO_1 Logical,
      AllowNegHO_2 Logical,
      AllowNegHO_3 Logical,
      AllowNegHO_4 Logical,
      AllowNegHO_5 Logical,
      AllowNegHO_6 Logical,
      AllowNegHO_7 Logical,
      MaxInchesOfRainHO Double( 0 ),
      FME_K_1 Double( 0 ),
      FME_K_2 Double( 0 ),
      FME_K_3 Double( 0 ),
      FME_O_1 Double( 0 ),
      FME_O_2 Double( 0 ),
      FME_O_3 Double( 0 ),
      FM_Connected Logical,
      FMEnterOwn Logical,
      VariableCCInUse Logical,
      ReportsRollTimeEnabled Logical,
      ReportsRollTime Time,
      LogAddress Integer,
      DiagAddress Integer,
      ActiveStations Short,
      EESize Integer,
      ControllerDateTime TimeStamp,
      CentralDateTime TimeStamp,
      SWVersion Char( 15 ),
      CommunicationsType Integer,
      ExistsOnController Logical,
      UsePassword Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'BulkPgmData2',
   'BulkPgmData2.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData2', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'BulkPgmData2fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData2', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'BulkPgmData2fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData2', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'BulkPgmData2fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData2', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'BulkPgmData2fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData2', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'BulkPgmData2fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData2', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'BulkPgmData2fail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData2', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'BulkPgmData2fail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData2', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'BulkPgmData2fail' ); 

CREATE TABLE BulkPgmData3 ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      IncludeTime Time,
      UseETAveraging_1 Logical,
      UseETAveraging_2 Logical,
      UseETAveraging_3 Logical,
      UseETAveraging_4 Logical,
      UseETAveraging_5 Logical,
      UseETAveraging_6 Logical,
      UseETAveraging_7 Logical,
      DTMF_Mode1Addr Raw( 5 ),
      DTMF_Mode2Addr Raw( 2 ),
      BacklightState Logical,
      MSInUse Logical,
      DTMFChannel1 Integer,
      DTMFChannel2 Integer,
      TrackEstimatedUsage Logical,
      Deleted Logical,
      UseYourOwnNumbers Logical,
      POAFS Logical,
      NOCIC Integer,
      HighFlowMargin Short,
      LowFlowMargin Short,
      Capacity_NonPump Short,
      IrrigatingByCapacity Logical,
      TestingFlow Logical,
      FlowRange1Top Integer,
      FlowRange2Top Integer,
      FlowRange3Top Integer,
      FlowTolerance1Plus Integer,
      FlowTolerance1Minus Integer,
      FlowTolerance2Plus Integer,
      FlowTolerance2Minus Integer,
      FlowTolerance3Plus Integer,
      FlowTolerance3Minus Integer,
      FlowTolerance4Plus Integer,
      FlowTolerance4Minus Integer,
      TailEndsThrowAwayUpperbound Integer,
      TailEndsMakeAdjustmentUpperbound Integer,
      IrriElectricalStationLimit Integer,
      FlowLength Integer,
      ChainDown Logical,
      FlowMeterAvailableElsewhere Logical,
      MasterHasIrrigateByCapacity Logical,
      MasterHasFlowChecking Logical,
      FlowInfoValid Logical,
      ProgramPrioritiesEnabled Logical,
      UserDefinedETState Char( 16 ),
      UserDefinedETCounty Char( 16 ),
      UserDefinedETCity Char( 16 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'BulkPgmData3',
   'BulkPgmData3.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData3', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'BulkPgmData3fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData3', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'BulkPgmData3fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData3', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'BulkPgmData3fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData3', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'BulkPgmData3fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData3', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'BulkPgmData3fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData3', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'BulkPgmData3fail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData3', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'BulkPgmData3fail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData3', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'BulkPgmData3fail' ); 

CREATE TABLE BulkPgmData4 ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      CreatedByRReInterface Logical,
      PTagsEditableAtController Logical,
      RReChannelChangeMode Short,
      RReControllerChannel Short,
      RReUserChannel Short,
      DMCInUse Logical,
      DMCLevels Short,
      DMCMaxFlowRateLevel1 Short,
      DMCMaxFlowRateLevel2 Short,
      MLBMVOR Short,
      MaximumHoldoverFactor Short,
      SICFunctionalityName Short,
      SICContactType Short,
      SICIrrigationToAffect Short,
      SICActionToTake Short,
      SICSwitchName Char( 24 ),
      YearlyBudget Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'BulkPgmData4',
   'BulkPgmData4.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData4', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'BulkPgmData4fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData4', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'BulkPgmData4fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData4', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'BulkPgmData4fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData4', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'BulkPgmData4fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData4', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'BulkPgmData4fail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'BulkPgmData4', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'BulkPgmData4fail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData4', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'BulkPgmData4fail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData4', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'BulkPgmData4fail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData4', 
      'YearlyBudget', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'BulkPgmData4fail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'BulkPgmData4', 
      'YearlyBudget', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'BulkPgmData4fail' ); 

CREATE TABLE MonthlyPrgmData ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      DataMonth Short,
      Budgets Double( 0 ),
      ETNumber Double( 0 ),
      FTimesMax Double( 0 ),
      VariableCCs Double( 0 ),
      Schedule Short,
      SX_ProgramA Time,
      SX_ProgramAEnabled Logical,
      SX_ProgramB Time,
      SX_ProgramBEnabled Logical,
      SX_ProgramC Time,
      SX_ProgramCEnabled Logical,
      SX_ProgramD Time,
      SX_ProgramDEnabled Logical,
      SX_ProgramE Time,
      SX_ProgramEEnabled Logical,
      SX_ProgramD1 Time,
      SX_ProgramD1Enabled Logical,
      SX_ProgramD2 Time,
      SX_ProgramD2Enabled Logical,
      UserETNumbers Double( 0 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'MonthlyPrgmData',
   'MonthlyPrgmData.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp;DataMonth',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthlyPrgmData', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'MonthlyPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthlyPrgmData', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'MonthlyPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthlyPrgmData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'MonthlyPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthlyPrgmData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'MonthlyPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthlyPrgmData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'MonthlyPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthlyPrgmData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'MonthlyPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'MonthlyPrgmData', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'MonthlyPrgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'MonthlyPrgmData', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'MonthlyPrgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'MonthlyPrgmData', 
      'DataMonth', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'MonthlyPrgmDatafail' ); 

CREATE TABLE POCPrgmData ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      POCID Short,
      POC_Name Char( 40 ),
      PartOfIrrigation Logical,
      AllowIrriDuringMLB Logical,
      MVType Short,
      FMInUse Logical,
      FMUseKAndO Logical,
      MLBDuringIdle Integer,
      MLBDuringUse Integer,
      MLBDuringIrri Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'POCPrgmData',
   'POCPrgmData.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp;POCID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCPrgmData', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'POCPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCPrgmData', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'POCPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCPrgmData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'POCPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCPrgmData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'POCPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCPrgmData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'POCPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCPrgmData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'POCPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCPrgmData', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCPrgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCPrgmData', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCPrgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCPrgmData', 
      'POCID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCPrgmDatafail' ); 

CREATE TABLE POCSchedule ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      POCID Short,
      POCDay Short,
      OpenTimeAEnabled Logical,
      OpenTimeA Time,
      DurationA Integer,
      OpenTimeBEnabled Logical,
      OpenTimeB Time,
      DurationB Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'POCSchedule',
   'POCSchedule.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp;POCID;POCDay',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCSchedule', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'POCSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCSchedule', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'POCSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCSchedule', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'POCSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCSchedule', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'POCSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCSchedule', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'POCSchedulefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'POCSchedule', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'POCSchedulefail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCSchedule', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCSchedulefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCSchedule', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCSchedulefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCSchedule', 
      'POCID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCSchedulefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'POCSchedule', 
      'POCDay', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'POCSchedulefail' ); 

CREATE TABLE ProgramPrgmData ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      DataProgram Short,
      NoFlowShutOff Short,
      PumpByProgram Logical,
      TripPercent Double( 0 ),
      ValveTime Integer,
      UseVariableCC Logical,
      OverFlowShutOff Short,
      VariableCCByProg_Jan Double( 0 ),
      VariableCCByProg_Feb Double( 0 ),
      VariableCCByProg_Mar Double( 0 ),
      VariableCCByProg_Apr Double( 0 ),
      VariableCCByProg_May Double( 0 ),
      VariableCCByProg_June Double( 0 ),
      VariableCCByProg_July Double( 0 ),
      VariableCCByProg_Aug Double( 0 ),
      VariableCCByProg_Sep Double( 0 ),
      VariableCCByProg_Oct Double( 0 ),
      VariableCCByProg_Nov Double( 0 ),
      VariableCCByProg_Dec Double( 0 ),
      PTags Short,
      MSByProg Logical,
      DailyETByProg Logical,
      HoldOverEnabledByProg Logical,
      HoldOverByProg Time,
      ValvesOnByProgram Integer,
      SetExpected Logical,
      ValveCloseTime Integer,
      ValvesOnInSystem Integer,
      WindInUse Logical,
      ProgramPriority Short,
      UserTag Char( 20 ),
      PercentAdjust1 Integer,
      PercentAdjustNumberOfDays1 Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ProgramPrgmData',
   'ProgramPrgmData.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp;DataProgram',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ProgramPrgmData', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'ProgramPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ProgramPrgmData', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'ProgramPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ProgramPrgmData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ProgramPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ProgramPrgmData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ProgramPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ProgramPrgmData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ProgramPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ProgramPrgmData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'ProgramPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ProgramPrgmData', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ProgramPrgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ProgramPrgmData', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ProgramPrgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ProgramPrgmData', 
      'DataProgram', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ProgramPrgmDatafail' ); 

CREATE TABLE StationPgmData ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      StationNumber Short,
      StationInUse Logical,
      StationInfo_Size Integer,
      StationInfo_Rate Integer,
      LorL Double( 0 ),
      FlowStatus Short,
      IStatus Short,
      NoWaterDays Integer,
      NoWaterToday Logical,
      EditSVar_Temp_PTime Double( 0 ),
      EditSVar_Temp_HTime Short,
      EditSVar_Temp_CTime Short,
      EditSVar_Jan_PTime Double( 0 ),
      EditSVar_Jan_HTime Short,
      EditSVar_Jan_CTime Short,
      EditSVar_Feb_PTime Double( 0 ),
      EditSVar_Feb_HTime Short,
      EditSVar_Feb_CTime Short,
      EditSVar_Mar_PTime Double( 0 ),
      EditSVar_Mar_HTime Short,
      EditSVar_Mar_CTime Short,
      EditSVar_Apr_PTime Double( 0 ),
      EditSVar_Apr_HTime Short,
      EditSVar_Apr_CTime Short,
      EditSVar_May_PTime Double( 0 ),
      EditSVar_May_HTime Short,
      EditSVar_May_CTime Short,
      EditSVar_June_PTime Double( 0 ),
      EditSVar_June_HTime Short,
      EditSVar_June_CTime Short,
      EditSVar_July_PTime Double( 0 ),
      EditSVar_July_HTime Short,
      EditSVar_July_CTime Short,
      EditSVar_Aug_PTime Double( 0 ),
      EditSVar_Aug_HTime Short,
      EditSVar_Aug_CTime Short,
      EditSVar_Sep_PTime Double( 0 ),
      EditSVar_Sep_HTime Short,
      EditSVar_Sep_CTime Short,
      EditSVar_Oct_PTime Double( 0 ),
      EditSVar_Oct_HTime Short,
      EditSVar_Oct_CTime Short,
      EditSVar_Nov_PTime Double( 0 ),
      EditSVar_Nov_HTime Short,
      EditSVar_Nov_CTime Short,
      EditSVar_Dec_PTime Double( 0 ),
      EditSVar_Dec_HTime Short,
      EditSVar_Dec_CTime Short,
      PAssign_Temp Short,
      PAssign_Norm Short,
      Name Char( 40 ),
      DailyETFactor Double( 0 ),
      LowFlowLimits Double( 0 ),
      MSSensorAssignment Short,
      MSSetPoint Short,
      MSReading Short,
      MSReadingStatus Short,
      MSMaxWaterDays Short) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'StationPgmData',
   'StationPgmData.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp;StationNumber',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'StationPgmData', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'StationPgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StationPgmData', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'StationPgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StationPgmData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'StationPgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StationPgmData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'StationPgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StationPgmData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'StationPgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StationPgmData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'StationPgmDatafail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StationPgmData', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StationPgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StationPgmData', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StationPgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StationPgmData', 
      'StationNumber', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StationPgmDatafail' ); 

CREATE TABLE TagNames ( 
      ControllerID Integer,
      PDataTimeStamp TimeStamp,
      TagIndex Short,
      TagNames Char( 20 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'TagNames',
   'TagNames.adi',
   'PRIMARY_KEY',
   'ControllerID;PDataTimeStamp;TagIndex',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'TagNames', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'TagNamesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TagNames', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'TagNamesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TagNames', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'TagNamesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TagNames', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'TagNamesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TagNames', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'TagNamesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TagNames', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'TagNamesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TagNames', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TagNamesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TagNames', 
      'PDataTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TagNamesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TagNames', 
      'TagIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TagNamesfail' ); 

CREATE TABLE TwentyEightDayPrgmData ( 
      ControllerID Integer,
      PgmTimeStamp TimeStamp,
      DataDate Short,
      DailyET_ET Double( 0 ),
      DailyET_Status Short,
      RainTable_Status Short,
      RainTable_Pad Short,
      RainTable_Rain Double( 0 ),
      WDay_ProgA_Temp Logical,
      WDay_ProgB_Temp Logical,
      WDay_ProgC_Temp Logical,
      WDay_ProgD_Temp Logical,
      WDay_ProgE_Temp Logical,
      WDay_ProgD1_Temp Logical,
      WDay_ProgD2_Temp Logical,
      WDay_ProgA_Jan Logical,
      WDay_ProgB_Jan Logical,
      WDay_ProgC_Jan Logical,
      WDay_ProgD_Jan Logical,
      WDay_ProgE_Jan Logical,
      WDay_ProgD1_Jan Logical,
      WDay_ProgD2_Jan Logical,
      WDay_ProgA_Feb Logical,
      WDay_ProgB_Feb Logical,
      WDay_ProgC_Feb Logical,
      WDay_ProgD_Feb Logical,
      WDay_ProgE_Feb Logical,
      WDay_ProgD1_Feb Logical,
      WDay_ProgD2_Feb Logical,
      WDay_ProgA_Mar Logical,
      WDay_ProgB_Mar Logical,
      WDay_ProgC_Mar Logical,
      WDay_ProgD_Mar Logical,
      WDay_ProgE_Mar Logical,
      WDay_ProgD1_Mar Logical,
      WDay_ProgD2_Mar Logical,
      WDay_ProgA_Apr Logical,
      WDay_ProgB_Apr Logical,
      WDay_ProgC_Apr Logical,
      WDay_ProgD_Apr Logical,
      WDay_ProgE_Apr Logical,
      WDay_ProgD1_Apr Logical,
      WDay_ProgD2_Apr Logical,
      WDay_ProgA_May Logical,
      WDay_ProgB_May Logical,
      WDay_ProgC_May Logical,
      WDay_ProgD_May Logical,
      WDay_ProgE_May Logical,
      WDay_ProgD1_May Logical,
      WDay_ProgD2_May Logical,
      WDay_ProgA_June Logical,
      WDay_ProgB_June Logical,
      WDay_ProgC_June Logical,
      WDay_ProgD_June Logical,
      WDay_ProgE_June Logical,
      WDay_ProgD1_June Logical,
      WDay_ProgD2_June Logical,
      WDay_ProgA_July Logical,
      WDay_ProgB_July Logical,
      WDay_ProgC_July Logical,
      WDay_ProgD_July Logical,
      WDay_ProgE_July Logical,
      WDay_ProgD1_July Logical,
      WDay_ProgD2_July Logical,
      WDay_ProgA_Aug Logical,
      WDay_ProgB_Aug Logical,
      WDay_ProgC_Aug Logical,
      WDay_ProgD_Aug Logical,
      WDay_ProgE_Aug Logical,
      WDay_ProgD1_Aug Logical,
      WDay_ProgD2_Aug Logical,
      WDay_ProgA_Sep Logical,
      WDay_ProgB_Sep Logical,
      WDay_ProgC_Sep Logical,
      WDay_ProgD_Sep Logical,
      WDay_ProgE_Sep Logical,
      WDay_ProgD1_Sep Logical,
      WDay_ProgD2_Sep Logical,
      WDay_ProgA_Oct Logical,
      WDay_ProgB_Oct Logical,
      WDay_ProgC_Oct Logical,
      WDay_ProgD_Oct Logical,
      WDay_ProgE_Oct Logical,
      WDay_ProgD1_Oct Logical,
      WDay_ProgD2_Oct Logical,
      WDay_ProgA_Nov Logical,
      WDay_ProgB_Nov Logical,
      WDay_ProgC_Nov Logical,
      WDay_ProgD_Nov Logical,
      WDay_ProgE_Nov Logical,
      WDay_ProgD1_Nov Logical,
      WDay_ProgD2_Nov Logical,
      WDay_ProgA_Dec Logical,
      WDay_ProgB_Dec Logical,
      WDay_ProgC_Dec Logical,
      WDay_ProgD_Dec Logical,
      WDay_ProgE_Dec Logical,
      WDay_ProgD1_Dec Logical,
      WDay_ProgD2_Dec Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'TwentyEightDayPrgmData',
   'TwentyEightDayPrgmData.adi',
   'PRIMARY_KEY',
   'ControllerID;PgmTimeStamp;DataDate',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'TwentyEightDayPrgmData', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'TwentyEightDayPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TwentyEightDayPrgmData', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'TwentyEightDayPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TwentyEightDayPrgmData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'TwentyEightDayPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TwentyEightDayPrgmData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'TwentyEightDayPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TwentyEightDayPrgmData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'TwentyEightDayPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TwentyEightDayPrgmData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'TwentyEightDayPrgmDatafail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TwentyEightDayPrgmData', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TwentyEightDayPrgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TwentyEightDayPrgmData', 
      'PgmTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TwentyEightDayPrgmDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TwentyEightDayPrgmData', 
      'DataDate', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TwentyEightDayPrgmDatafail' ); 

CREATE TABLE City ( 
      CityIndex Integer,
      CountyIndex Integer,
      CityName Char( 50 ),
      JanET Double( 0 ),
      FebET Double( 0 ),
      MarET Double( 0 ),
      AprilET Double( 0 ),
      MayET Double( 0 ),
      JuneET Double( 0 ),
      JulyET Double( 0 ),
      AugET Double( 0 ),
      SeptET Double( 0 ),
      OctET Double( 0 ),
      NovET Double( 0 ),
      DecET Double( 0 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'City',
   'City.adi',
   'CITYINDEX',
   'CityIndex',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'City',
   'City.adi',
   'COUNTYINDEX',
   'CountyIndex',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'City', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Cityfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'City', 
   'Table_Primary_Key', 
   'CITYINDEX', 'APPEND_FAIL', 'Cityfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'City', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Cityfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'City', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Cityfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'City', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Cityfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'City', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Cityfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'City', 
      'CityIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Cityfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'City', 
      'CountyIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Cityfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'City', 
      'CityName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Cityfail' ); 

CREATE TABLE County ( 
      CountyIndex Integer,
      CountyName Char( 20 ),
      StateIndex Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'County',
   'County.adi',
   'COUNTYINDEX',
   'CountyIndex',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'County',
   'County.adi',
   'STATEINDEX',
   'StateIndex',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'County', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Countyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'County', 
   'Table_Primary_Key', 
   'COUNTYINDEX', 'APPEND_FAIL', 'Countyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'County', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Countyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'County', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Countyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'County', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Countyfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'County', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Countyfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'County', 
      'CountyIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Countyfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'County', 
      'CountyName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Countyfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'County', 
      'StateIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Countyfail' ); 

CREATE TABLE PrgmTags ( 
      PrgmTagIndex AutoInc,
      PrgmTagName Char( 20 ),
      CompanyID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'PrgmTags',
   'PrgmTags.adi',
   'PRGMTAGINDEX',
   'PrgmTagIndex',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'PrgmTags',
   'PrgmTags.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'PrgmTags', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'PrgmTagsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PrgmTags', 
   'Table_Primary_Key', 
   'PRGMTAGINDEX', 'APPEND_FAIL', 'PrgmTagsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PrgmTags', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'PrgmTagsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PrgmTags', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'PrgmTagsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PrgmTags', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'PrgmTagsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PrgmTags', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'PrgmTagsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'PrgmTags', 
      'PrgmTagIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'PrgmTagsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'PrgmTags', 
      'PrgmTagName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'PrgmTagsfail' ); 

CREATE TABLE States ( 
      StateIndex Integer,
      StateName Char( 20 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'States',
   'States.adi',
   'STATEINDEX',
   'StateIndex',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'States', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Statesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'States', 
   'Table_Primary_Key', 
   'STATEINDEX', 'APPEND_FAIL', 'Statesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'States', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Statesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'States', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Statesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'States', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Statesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'States', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Statesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'States', 
      'StateIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Statesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'States', 
      'StateName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Statesfail' ); 

CREATE TABLE MonthET ( 
      ControllerID Integer,
      ETMonth Short,
      ETData Double( 0 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'MonthET',
   'MonthET.adi',
   'PRIMARY_KEY',
   'ControllerID;ETMonth',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthET', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'MonthETfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthET', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'MonthETfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthET', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'MonthETfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthET', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'MonthETfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthET', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'MonthETfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'MonthET', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'MonthETfail');

CREATE TABLE StatusMessages ( 
      DateTime TimeStamp,
      CompanyID Integer,
      CategoryID CIChar( 3 ),
      Expiration TimeStamp,
      Heading CIChar( 400 ),
      Contents Memo,
      Enabled Logical,
      ControllerID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'StatusMessages',
   'StatusMessages.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'StatusMessages',
   'StatusMessages.adi',
   'CATEGORYID',
   'CategoryID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'StatusMessages',
   'StatusMessages.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'StatusMessages',
   'StatusMessages.adi',
   'SEARCH_INDEX',
   'CompanyID;Expiration',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessages', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'StatusMessagesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessages', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'StatusMessagesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessages', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'StatusMessagesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessages', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'StatusMessagesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessages', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'StatusMessagesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessages', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'StatusMessagesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessages', 
      'DateTime', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StatusMessagesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessages', 
      'DateTime', 'Field_Default_Value', 
      'CURRENT_TIMESTAMP()', 'APPEND_FAIL', 'StatusMessagesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessages', 
      'CategoryID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StatusMessagesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessages', 
      'Heading', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StatusMessagesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessages', 
      'Contents', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StatusMessagesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessages', 
      'Enabled', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StatusMessagesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessages', 
      'Enabled', 'Field_Default_Value', 
      'True', 'APPEND_FAIL', 'StatusMessagesfail' ); 

CREATE TABLE StatusMessageCategories ( 
      ID CIChar( 3 ),
      Description CIChar( 50 ),
      CssClass CIChar( 50 ),
      Priority Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'StatusMessageCategories',
   'StatusMessageCategories.adi',
   'ID',
   'ID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessageCategories', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'StatusMessageCategoriesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessageCategories', 
   'Table_Primary_Key', 
   'ID', 'APPEND_FAIL', 'StatusMessageCategoriesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessageCategories', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'StatusMessageCategoriesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessageCategories', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'StatusMessageCategoriesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessageCategories', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'StatusMessageCategoriesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessageCategories', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'StatusMessageCategoriesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusMessageCategories', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'StatusMessageCategoriesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessageCategories', 
      'ID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StatusMessageCategoriesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessageCategories', 
      'Description', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StatusMessageCategoriesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessageCategories', 
      'CssClass', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StatusMessageCategoriesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'StatusMessageCategories', 
      'Priority', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'StatusMessageCategoriesfail' ); 

CREATE TABLE RainShare ( 
      ControllerID Integer,
      RainDate Date,
      Rain Double( 0 ),
      RainStatus Short) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'RainShare',
   'RainShare.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'RainShare',
   'RainShare.adi',
   'RAINDATE',
   'RainDate',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'RainShare', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'RainSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'RainShare', 
   'Table_Default_Index', 
   'ControllerID', 'APPEND_FAIL', 'RainSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'RainShare', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'RainSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'RainShare', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'RainSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'RainShare', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'RainSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'RainShare', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'RainSharefail');

CREATE TABLE ETShare ( 
      ControllerID Integer,
      ETDate Date,
      ET Double( 0 ),
      ETStatus Short) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ETShare',
   'ETShare.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ETShare',
   'ETShare.adi',
   'ETDATE',
   'ETDate',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETShare', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'ETSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETShare', 
   'Table_Default_Index', 
   'ControllerID', 'APPEND_FAIL', 'ETSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETShare', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ETSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETShare', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ETSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETShare', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ETSharefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ETShare', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'ETSharefail');

CREATE TABLE AssignmentSites ( 
      UserID Integer,
      SiteID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AssignmentSites',
   'AssignmentSites.adi',
   'SITEID',
   'SiteID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AssignmentSites',
   'AssignmentSites.adi',
   'PRIMARY_KEY',
   'UserID;SiteID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AssignmentSites',
   'AssignmentSites.adi',
   'USERID',
   'UserID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentSites', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'AssignmentSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentSites', 
   'Table_Primary_Key', 
   'PRIMARY_KEY', 'APPEND_FAIL', 'AssignmentSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentSites', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AssignmentSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentSites', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'AssignmentSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentSites', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AssignmentSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentSites', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'AssignmentSitesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentSites', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'AssignmentSitesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AssignmentSites', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AssignmentSitesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AssignmentSites', 
      'SiteID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AssignmentSitesfail' ); 

CREATE TABLE ActivityLog ( 
      UserID Integer,
      ActivityTimeStamp TimeStamp,
      Activity1 Char( 250 ),
      Activity2 Char( 250 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ActivityLog',
   'ActivityLog.adi',
   'SORT_UT',
   'UserID;ActivityTimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ActivityLog', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'ActivityLogfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ActivityLog', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ActivityLogfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ActivityLog', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ActivityLogfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ActivityLog', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ActivityLogfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ActivityLog', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'ActivityLogfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ActivityLog', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ActivityLogfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ActivityLog', 
      'ActivityTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ActivityLogfail' ); 

CREATE TABLE ComSrvrs ( 
      IPAddress Char( 20 ),
      IPPort Integer,
      CommName Char( 50 ),
      ModemAvailable Logical,
      ATAvailable Logical,
      MASCAvailable Logical,
      HardWireAvailable Logical,
      Communicating Logical,
      LastStarted TimeStamp,
      ServerID AutoInc,
      CurrentController Char( 50 ),
      LastActivity Char( 50 ),
      ControllerRetrys Short,
      CommandRetrys Short,
      EthernetAvailable Logical,
      NumberOfControllers Integer,
      Online Logical,
      Version Double( 0 ),
      HostName Char( 100 ),
      GPRSAvailable Logical,
      FiberOpticModemAvailable Logical,
      InternetAvailable Logical,
      ConnectedViaInternet Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ComSrvrs',
   'ComSrvrs.adi',
   'IPADDRESS',
   'IPAddress',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ComSrvrs',
   'ComSrvrs.adi',
   'IPPORT',
   'IPPort',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ComSrvrs',
   'ComSrvrs.adi',
   'MODEMAVAILABLE',
   'ModemAvailable',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ComSrvrs',
   'ComSrvrs.adi',
   'COMMUNICATING',
   'Communicating',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ComSrvrs',
   'ComSrvrs.adi',
   'ONLINE',
   'Online',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ComSrvrs',
   'ComSrvrs.adi',
   'HOSTNAME',
   'HostName',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ComSrvrs',
   'ComSrvrs.adi',
   'INTERNETAVAILABLE',
   'InternetAvailable',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ComSrvrs',
   'ComSrvrs.adi',
   'CONNECTEDVIAINTERNET',
   'ConnectedViaInternet',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ComSrvrs',
   'ComSrvrs.adi',
   'NUMBEROFCONTROLLERS_LASTSTARTED',
   'NumberOfControllers;LastStarted',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ComSrvrs', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'ComSrvrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ComSrvrs', 
   'Table_Default_Index', 
   'IPADDRESS', 'APPEND_FAIL', 'ComSrvrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ComSrvrs', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ComSrvrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ComSrvrs', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ComSrvrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ComSrvrs', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ComSrvrsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ComSrvrs', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'ComSrvrsfail');

CREATE TABLE FlowRec ( 
      ControllerID Integer,
      FlowTimeStamp TimeStamp,
      Controller Char( 1 ),
      Station Short,
      Expected Short,
      DeratedExpected Short,
      HiLimit Short,
      LoLimit Short,
      PortionOfActual Short,
      Flag Short,
      FlowData Raw( 20 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'FlowRec',
   'FlowRec.adi',
   'FLOWTIMESTAMP',
   'FlowTimeStamp',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'FlowRec',
   'FlowRec.adi',
   'SORT',
   'ControllerID;FlowTimeStamp;Controller;Station',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'FlowRec',
   'FlowRec.adi',
   'SORT2',
   'ControllerID;Controller;Station;FlowTimeStamp',
   '',
   2,
   8192,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'FlowRec', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'FlowRecfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'FlowRec', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'FlowRecfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'FlowRec', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'FlowRecfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'FlowRec', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'FlowRecfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'FlowRec', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'FlowRecfail');

CREATE TABLE Polling ( 
      PollingEnabled Logical,
      PollOnSunday Logical,
      PollOnMonday Logical,
      PollOnTuesday Logical,
      PollOnWednesday Logical,
      PollOnThursday Logical,
      PollOnFriday Logical,
      PollOnSaturday Logical,
      PollStart Time,
      PollStop Time,
      PollFrequency Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Polling',
   'Polling.adi',
   'POLLINGENABLED',
   'PollingEnabled',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Polling', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Pollingfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Polling', 
   'Table_Default_Index', 
   'PollingEnabled', 'APPEND_FAIL', 'Pollingfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Polling', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Pollingfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Polling', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Pollingfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Polling', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Pollingfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Polling', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Pollingfail');

CREATE TABLE PollingControllers ( 
      ControllerID Integer,
      CompletionTime TimeStamp,
      PollingTaskID Integer,
      ShutdownTaskID Integer,
      UserID Integer,
      TableDate Date) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'PollingControllers',
   'PollingControllers.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'PollingControllers',
   'PollingControllers.adi',
   'COMPLETIONTIME',
   'CompletionTime',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'PollingControllers', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'PollingControllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PollingControllers', 
   'Table_Default_Index', 
   'ControllerID', 'APPEND_FAIL', 'PollingControllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PollingControllers', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'PollingControllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PollingControllers', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'PollingControllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PollingControllers', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'PollingControllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PollingControllers', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'PollingControllersfail');

CREATE TABLE Widgets ( 
      UserID Integer,
      WidgetName Char( 32 ),
      Size Short,
      WidgetOrder Short,
      Title Char( 50 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Widgets',
   'Widgets.adi',
   'USERID',
   'UserID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Widgets', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'Widgetsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Widgets', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Widgetsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Widgets', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'Widgetsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Widgets', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Widgetsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Widgets', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Widgetsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Widgets', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Widgetsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Widgets', 
      'UserID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Widgetsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Widgets', 
      'WidgetName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Widgetsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Widgets', 
      'Size', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Widgetsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Widgets', 
      'WidgetOrder', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Widgetsfail' ); 

CREATE TABLE TaskStatus ( 
      TaskId AutoInc,
      DateCreated TimeStamp,
      CreatedBy Integer,
      ControllerID Integer,
      ControllerName Char( 255 ),
      TaskCommand Short,
      Status Char( 50 ),
      HasViewed Logical,
      arg1 Integer,
      arg2 Integer,
      Log Char( 1500 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskStatus',
   'TaskStatus.adi',
   'TASKID',
   'TaskId',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskStatus',
   'TaskStatus.adi',
   'DATECREATED',
   'DateCreated',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'TaskStatus',
   'TaskStatus.adi',
   'CREATEDBY',
   'CreatedBy',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskStatus', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', 'TaskStatusfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskStatus', 
   'Table_Primary_Key', 
   'TASKID', 'APPEND_FAIL', 'TaskStatusfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskStatus', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'TaskStatusfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskStatus', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'TaskStatusfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskStatus', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'TaskStatusfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskStatus', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'TaskStatusfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TaskStatus', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'TaskStatusfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'TaskId', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'DateCreated', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'DateCreated', 'Field_Default_Value', 
      'getDate()', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'CreatedBy', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'ControllerName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'TaskCommand', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'Status', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'Status', 'Field_Default_Value', 
      'Waiting', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'HasViewed', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'TaskStatusfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'TaskStatus', 
      'HasViewed', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'TaskStatusfail' ); 

CREATE TABLE AssignmentCompanies ( 
      ContractorID Integer,
      CompanyID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentCompanies', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'AssignmentCompaniesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentCompanies', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AssignmentCompaniesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentCompanies', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'AssignmentCompaniesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentCompanies', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AssignmentCompaniesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentCompanies', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'AssignmentCompaniesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignmentCompanies', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'AssignmentCompaniesfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AssignmentCompanies', 
      'ContractorID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AssignmentCompaniesfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'AssignmentCompanies', 
      'CompanyID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'AssignmentCompaniesfail' ); 

CREATE TABLE CS3000Alerts ( 
      NetworkID Integer,
      AlertTimestamp TimeStamp,
      AlertDiagCode Integer,
      Description Char( 256 ),
      UserVisible Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Alerts',
   'CS3000Alerts.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Alerts',
   'CS3000Alerts.adi',
   'ALERTTIMESTAMP',
   'AlertTimestamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Alerts',
   'CS3000Alerts.adi',
   'ALERTDIAGCODE',
   'AlertDiagCode',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Alerts', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Alertsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Alerts', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Alertsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Alerts', 
      'AlertTimestamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Alertsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Alerts', 
      'AlertDiagCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Alertsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Alerts', 
      'Description', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Alertsfail' ); 

CREATE TABLE CS3000Cards ( 
      CardID AutoInc,
      Slot Integer,
      CardPresent Logical,
      TBPresent Logical,
      CardErrors Integer,
      CardType Logical,
      ControllerSN Integer,
      NetworkID Integer,
      BoxIndex Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Cards',
   'CS3000Cards.adi',
   'CARDID',
   'CardID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Cards',
   'CS3000Cards.adi',
   'SLOT',
   'Slot',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Cards',
   'CS3000Cards.adi',
   'CONTROLLERSN',
   'ControllerSN',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Cardsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Cardsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Cardsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Cardsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Cards', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Cardsfail');

CREATE TABLE CS3000Controllers ( 
      ControllerID Integer,
      NetworkID Integer,
      Name Char( 50 ),
      SerialNumber Integer,
      FLOption Logical,
      SSEOption Logical,
      SSE_DOption Logical,
      ElectricalStationsOnLimit Short,
      FOAL_MinutesBetweenRescanAttempts Integer,
      OM_OriginatorRetries Integer,
      OM_SecondsForStatus_FOAL Integer,
      OM_MinutesToExist Integer,
      TestSeconds Integer,
      LastAssignedDecoderSN Integer,
      WeatherOption Logical,
      HasRainBucket Logical,
      HasETgage Logical,
      HasWindGage Logical,
      WeatherCard Logical,
      WeatherTerminal Logical,
      DashM_Card Logical,
      DashM_Terminal Logical,
      DashM_CardType Logical,
      TwoWireTerminal Logical,
      PortA_DeviceIndex Integer,
      PortB_DeviceIndex Integer,
      BoxIndex Integer,
      HasRainSwitch Logical,
      HasFreezeSwitch Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Controllers',
   'CS3000Controllers.adi',
   'CONTROLLERID',
   'ControllerID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Controllers',
   'CS3000Controllers.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Controllers', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'ControllerID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'SerialNumber', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'FLOption', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'SSEOption', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'SSEOption', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'SSE_DOption', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'WeatherOption', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasRainBucket', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasETgage', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasWindGage', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'WeatherCard', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'WeatherTerminal', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'DashM_Card', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'DashM_Terminal', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'DashM_CardType', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'TwoWireTerminal', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'BoxIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasRainSwitch', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Controllers', 
      'HasFreezeSwitch', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Controllersfail' ); 

CREATE TABLE CS3000FlowRecordingTable ( 
      NetworkID Integer,
      SystemGID Integer,
      FlowTimeStamp TimeStamp,
      Controller Integer,
      Station Integer,
      ExpectedFlow Integer,
      DeratedExpectedFlow Integer,
      HiLimit Integer,
      LoLimit Integer,
      PortionOfActual Integer,
      Flag Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000FlowRecordingTable',
   'CS3000FlowRecordingTable.adi',
   'FLOWTIMESTAMP',
   'FlowTimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000FlowRecordingTable',
   'CS3000FlowRecordingTable.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000FlowRecordingTable',
   'CS3000FlowRecordingTable.adi',
   'SYSTEMGID',
   'SystemGID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000FlowRecordingTable',
   'CS3000FlowRecordingTable.adi',
   'CONTROLLER',
   'Controller',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000FlowRecordingTable',
   'CS3000FlowRecordingTable.adi',
   'STATION',
   'Station',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000FlowRecordingTable', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000FlowRecordingTablefail');

CREATE TABLE CS3000ManualPrograms ( 
      ProgramID AutoInc,
      NetworkID Integer,
      ProgramGID Integer,
      Name Char( 50 ),
      Deleted Logical,
      StartTime1 Integer,
      StartTime2 Integer,
      StartTime3 Integer,
      StartTime4 Integer,
      StartTime5 Integer,
      StartTime6 Integer,
      Day1 Logical,
      Day2 Logical,
      Day3 Logical,
      Day4 Logical,
      Day5 Logical,
      Day6 Logical,
      Day7 Logical,
      StartDate Date,
      StopDate Date,
      RunTime Integer,
      InUse Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000ManualPrograms',
   'CS3000ManualPrograms.adi',
   'PROGRAMID',
   'ProgramID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000ManualPrograms',
   'CS3000ManualPrograms.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Primary_Key', 
   'PROGRAMID', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000ManualPrograms', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'ProgramID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'ProgramGID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day1', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day2', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day3', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day4', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day5', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day6', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000ManualPrograms', 
      'Day7', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000ManualProgramsfail' ); 

CREATE TABLE CS3000Networks ( 
      NetworkID Integer,
      MasterTimeStamp TimeStamp,
      CompanyID Integer,
      NetworkName Char( 50 ),
      Deleted Logical,
      LastCommunicated TimeStamp,
      MainboardForceFirmware CIChar( 255 ),
      TPMicroForceFirmware CIChar( 255 ),
      StartOfIrrigationDay Integer,
      ScheduledIrrigationIsOff Logical,
      TimeZone Integer,
      DlsInUse Logical,
      DlsSpringMonth Integer,
      DlsSpringMinDay Integer,
      DlsSpringMaxDay Integer,
      DlsFallBackMonth Integer,
      DlsFallBackMinDay Integer,
      DlsFallBackMaxDay Integer,
      RainPollingEnabled Logical,
      SendFlowRecording Logical,
      BoxName1 Char( 50 ),
      BoxName2 Char( 50 ),
      BoxName3 Char( 50 ),
      BoxName4 Char( 50 ),
      BoxName5 Char( 50 ),
      BoxName6 Char( 50 ),
      BoxName7 Char( 50 ),
      BoxName8 Char( 50 ),
      BoxName9 Char( 50 ),
      BoxName10 Char( 50 ),
      BoxName11 Char( 50 ),
      BoxName12 Char( 50 ),
      ElectricalStationOnLimit1 Integer,
      ElectricalStationOnLimit2 Integer,
      ElectricalStationOnLimit3 Integer,
      ElectricalStationOnLimit4 Integer,
      ElectricalStationOnLimit5 Integer,
      ElectricalStationOnLimit6 Integer,
      ElectricalStationOnLimit7 Integer,
      ElectricalStationOnLimit8 Integer,
      ElectricalStationOnLimit9 Integer,
      ElectricalStationOnLimit10 Integer,
      ElectricalStationOnLimit11 Integer,
      ElectricalStationOnLimit12 Integer,
      MasterSerialNumber Integer,
      ETSharingNetworkID Integer,
      RainSharingNetworkID Integer,
      RR_ScreenData Blob,
      RR_ScreenLastUpdated TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Networks',
   'CS3000Networks.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Networks',
   'CS3000Networks.adi',
   'COMPANYID',
   'CompanyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Primary_Key', 
   'NETWORKID', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Networks', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'NetworkID', 'Comment', 
      'CS3000 Network ID', 'APPEND_FAIL', 'CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Networksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Networks', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Networksfail' ); 

CREATE TABLE CS3000POC ( 
      POC_ID AutoInc,
      SystemGID Integer,
      POC_GID Integer,
      Name Char( 50 ),
      Deleted Logical,
      BoxIndex Integer,
      TypeOfPOC Short,
      DecoderSerialNumber1 Integer,
      DecoderSerialNumber2 Integer,
      DecoderSerialNumber3 Integer,
      POC_CardConnected Logical,
      POC_TerminalConnected Logical,
      POC_Usage Integer,
      BudgetOption Short,
      BudgetPercentOfET Double( 0 ),
      BudgetMonthly1 Double( 0 ),
      BudgetMonthly2 Double( 0 ),
      BudgetMonthly3 Double( 0 ),
      BudgetMonthly4 Double( 0 ),
      BudgetMonthly5 Double( 0 ),
      BudgetMonthly6 Double( 0 ),
      BudgetMonthly7 Double( 0 ),
      BudgetMonthly8 Double( 0 ),
      BudgetMonthly9 Double( 0 ),
      BudgetMonthly10 Double( 0 ),
      BudgetMonthly11 Double( 0 ),
      BudgetMonthly12 Double( 0 ),
      MasterValveType1 Short,
      MasterValveType2 Short,
      MasterValveType3 Short,
      FlowMeterChoice1 Short,
      FlowMeterK_Value1 Integer,
      FlowMeterOffset_Value1 Integer,
      FlowMeterChoice2 Short,
      FlowMeterK_Value2 Integer,
      FlowMeterOffset_Value2 Integer,
      FlowMeterChoice3 Short,
      FlowMeterK_Value3 Integer,
      FlowMeterOffset_Value3 Integer,
      NetworkID Integer,
      NumberOfLevels Integer,
      FlowMeterReedSwitch1 Integer,
      FlowMeterReedSwitch2 Integer,
      FlowMeterReedSwitch3 Integer,
      BudgetGallonsEntryOption Integer,
      BudgetCalcPercentOfET Integer,
      BudgetAutoAdjustUsingLiveET Logical,
      BudgetClosePOCAtBudget Integer,
      BudgetGallons1 Integer,
      BudgetGallons2 Integer,
      BudgetGallons3 Integer,
      BudgetGallons4 Integer,
      BudgetGallons5 Integer,
      BudgetGallons6 Integer,
      BudgetGallons7 Integer,
      BudgetGallons8 Integer,
      BudgetGallons9 Integer,
      BudgetGallons10 Integer,
      BudgetGallons11 Integer,
      BudgetGallons12 Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000POC',
   'CS3000POC.adi',
   'POC_ID',
   'POC_ID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000POC',
   'CS3000POC.adi',
   'SYSTEMID',
   'SystemGID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Primary_Key', 
   'POC_ID', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POC', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_ID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'SystemGID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_GID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_CardConnected', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_CardConnected', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_TerminalConnected', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'POC_TerminalConnected', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'FlowMeterReedSwitch1', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'FlowMeterReedSwitch2', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'CS3000POCfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POC', 
      'FlowMeterReedSwitch3', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'CS3000POCfail' ); 

CREATE TABLE CS3000POCRecord ( 
      NetworkID Integer,
      POC_ID Integer,
      StartTimeStamp TimeStamp,
      GallonsTotal Double( 0 ),
      SecondsTotal Integer,
      IdleGallons Double( 0 ),
      IdleSeconds Integer,
      MVORGallons Double( 0 ),
      MVORSeconds Integer,
      IrriGallons Double( 0 ),
      IrriSeconds Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000POCRecord',
   'CS3000POCRecord.adi',
   'POC_ID',
   'POC_ID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000POCRecord',
   'CS3000POCRecord.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000POCRecord',
   'CS3000POCRecord.adi',
   'STARTTIMESTAMP',
   'StartTimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000POCRecord', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000POCRecordfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POCRecord', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCRecordfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POCRecord', 
      'POC_ID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCRecordfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000POCRecord', 
      'StartTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000POCRecordfail' ); 

CREATE TABLE CS3000RainShutdown ( 
      NetworkID Integer,
      TransmissionTime TimeStamp,
      ShutdownDate Date) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000RainShutdown',
   'CS3000RainShutdown.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000RainShutdown', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000RainShutdownfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000RainShutdown', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000RainShutdownfail' ); 

CREATE TABLE CS3000StationDataRecord ( 
      StationID Integer,
      BoxIndex Integer,
      StationGroupID Integer,
      ProgramedIrrigSeconds Integer,
      ProgramedIrrigGallons Double( 0 ),
      ManualProgramSeconds Integer,
      ManualProgramGallons Double( 0 ),
      ManualSeconds Integer,
      ManualGallons Double( 0 ),
      WalkThruSeconds Integer,
      WalkThruGallons Double( 0 ),
      TestSeconds Integer,
      TestGallons Double( 0 ),
      RRSeconds Integer,
      RRGallons Double( 0 ),
      TimeStamp TimeStamp,
      NetworkID Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationDataRecord',
   'CS3000StationDataRecord.adi',
   'STATIONID',
   'StationID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationDataRecord',
   'CS3000StationDataRecord.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationDataRecord',
   'CS3000StationDataRecord.adi',
   'BOXINDEX',
   'BoxIndex',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationDataRecord',
   'CS3000StationDataRecord.adi',
   'TIMESTAMP',
   'TimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationDataRecord', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationDataRecord', 
      'StationID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationDataRecord', 
      'BoxIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationDataRecord', 
      'TimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationDataRecordfail' ); 

CREATE TABLE CS3000StationGroups ( 
      StationGroupID AutoInc,
      NetworkID Integer,
      StationGroupGID Integer,
      Name Char( 50 ),
      Deleted Logical,
      PlantType Short,
      HeadType Short,
      PrecipRate Double( 0 ),
      SoilType Short,
      SlopePercentage Double( 0 ),
      Exposure Short,
      UsableRainPercentage Double( 0 ),
      PriorityLevel Integer,
      PercentAdjust Double( 0 ),
      StartDate Date,
      EndDate Date,
      Enabled Logical,
      ScheduleType Integer,
      BeginDate Date,
      IrrigateOn29Or31 Logical,
      MowDay Integer,
      LastRan TimeStamp,
      TailEndsThrowAway Integer,
      TailEndsMakeAdjustment Integer,
      MaximumLeftover Integer,
      ET_Mode Integer,
      UseET_Averaging Logical,
      RainInUse Logical,
      MaximumAccumulation Integer,
      WindInUse Logical,
      HighFlowAction Integer,
      LowFlowAction Integer,
      On_At_A_TimeInGroup Double( 0 ),
      On_At_A_TimeInSystem Double( 0 ),
      PresentlyOnGroupCount Integer,
      PumpInUse Logical,
      LineFillTime Integer,
      ValveCloseTime Integer,
      AcquireExpecteds Logical,
      CropCoefficient1 Double( 0 ),
      CropCoefficient2 Double( 0 ),
      CropCoefficient3 Double( 0 ),
      CropCoefficient4 Double( 0 ),
      CropCoefficient5 Double( 0 ),
      CropCoefficient6 Double( 0 ),
      CropCoefficient7 Double( 0 ),
      CropCoefficient8 Double( 0 ),
      CropCoefficient9 Double( 0 ),
      CropCoefficient10 Double( 0 ),
      CropCoefficient11 Double( 0 ),
      CropCoefficient12 Double( 0 ),
      WaterDaysSun Logical,
      WaterDaysMon Logical,
      WaterDaysTue Logical,
      WaterDaysWed Logical,
      WaterDaysThu Logical,
      WaterDaysFri Logical,
      WaterDaysSat Logical,
      StartTime Integer,
      StopTime Integer,
      AllowableDepletion Integer,
      AvailableWater Integer,
      RootDepth Integer,
      SpeciesFactor Integer,
      DensityFactor Integer,
      MicroclimateFactor Integer,
      SoilIntakeRate Integer,
      AllowableSurfaceAccum Integer,
      BudgetReductionCap Integer,
      SystemID Integer,
      InUse Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationGroups',
   'CS3000StationGroups.adi',
   'STATIONGROUPID',
   'StationGroupID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationGroups',
   'CS3000StationGroups.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Primary_Key', 
   'STATIONGROUPID', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationGroups', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'StationGroupID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'StationGroupGID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'Enabled', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'Enabled', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'IrrigateOn29Or31', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'IrrigateOn29Or31', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'UseET_Averaging', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'UseET_Averaging', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'RainInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'RainInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WindInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WindInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'PumpInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'PumpInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'AcquireExpecteds', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'AcquireExpecteds', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysSun', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysSun', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysMon', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysMon', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysTue', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysTue', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysWed', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysWed', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysThu', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysThu', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysFri', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysFri', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysSat', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationGroups', 
      'WaterDaysSat', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationGroupsfail' ); 

CREATE TABLE CS3000StationHistory ( 
      StartTimeStamp TimeStamp,
      StationID Integer,
      SystemID Integer,
      StationGroupID Integer,
      FirstCycleStartTimeStamp TimeStamp,
      LastCycleEndTimeStamp TimeStamp,
      SecondsIrrigated Integer,
      LastMeasuredCurrent Integer,
      GallonsIrrigated Double( 0 ),
      TotalRequestedMinutes Short,
      RainAtStartBefore Short,
      RainAtStartAfter Short,
      ActualFlowCheckShare Short,
      HiLimitFlowCheckShare Short,
      LoLimitFlowCheckShare Short,
      NumberOfRepeats Short,
      FlowDataStamped Logical,
      ControllerTurnedOff Logical,
      HitStopTime Logical,
      StopKeyPressed Logical,
      CurrentShort Logical,
      CurrentNone Logical,
      CurrentLow Logical,
      CurrentHigh Logical,
      TailendsModifiedCycle Logical,
      TailendsZeroedIrrig Logical,
      FlowLow Logical,
      FlowHigh Logical,
      FlowNeverChecked Logical,
      NoWaterByManual Logical,
      NoWaterByCalendar Logical,
      MlbPrevented Logical,
      MvorClosed Logical,
      RainPrevented Logical,
      RainReducedIrrig Logical,
      RainTable Logical,
      SwitchRain Logical,
      SwitchFreeze Logical,
      WindConditions Logical,
      MoisCauseCycleSkip Logical,
      MoisMaxWaterDay Logical,
      PocShort Logical,
      MowDay Logical,
      TwoWireCable Logical,
      NetworkID Integer,
      BoxIndex Integer,
      RipValid Logical,
      twStationDecoder Logical,
      twPOCDecoder Logical,
      MoistureBalancePerc Integer,
      WaterSenseMinutes Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationHistory',
   'CS3000StationHistory.adi',
   'STATIONID',
   'StationID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationHistory',
   'CS3000StationHistory.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationHistory',
   'CS3000StationHistory.adi',
   'STARTTIMESTAMP',
   'StartTimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000StationHistory',
   'CS3000StationHistory.adi',
   'BOXINDEX',
   'BoxIndex',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000StationHistory', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000StationHistoryfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'StartTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'StationID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'SystemID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'FlowDataStamped', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'ControllerTurnedOff', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'HitStopTime', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'StopKeyPressed', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'CurrentShort', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'CurrentNone', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'CurrentLow', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'CurrentHigh', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'TailendsModifiedCycle', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'TailendsZeroedIrrig', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'FlowLow', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'FlowHigh', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'FlowNeverChecked', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'NoWaterByManual', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'NoWaterByCalendar', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MlbPrevented', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MvorClosed', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'RainPrevented', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'RainReducedIrrig', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'RainTable', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'SwitchRain', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'SwitchFreeze', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'WindConditions', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MoisCauseCycleSkip', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MoisMaxWaterDay', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'PocShort', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'MowDay', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'TwoWireCable', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'RipValid', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'twStationDecoder', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000StationHistory', 
      'twPOCDecoder', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000StationHistoryfail' ); 

CREATE TABLE CS3000Stations ( 
      StationID AutoInc,
      NetworkID Integer,
      Name Char( 50 ),
      StationNumber Integer,
      Deleted Logical,
      CardConnected Logical,
      InUse Logical,
      DecoderSerialNumber Integer,
      DecoderOutput Integer,
      TotalMinutesRun Double( 0 ),
      CycleMinutes Double( 0 ),
      SoakMinutes Double( 0 ),
      ET_Factor Integer,
      ExpectedFlowRate Integer,
      DistributionUniformity Integer,
      NoWaterDays Integer,
      ManualProgramA Integer,
      ManualProgramB Integer,
      StationGroupID Integer,
      SystemID Integer,
      ControllerID Integer,
      ManualProgramA_ID Integer,
      ManualProgramB_ID Integer,
      BoxIndex Integer,
      MoistureBalance Double( 3 ),
      SquareFootage Integer,
      RR_Status Integer,
      RR_SecondsLeft Integer,
      RR_StartDateTime TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Stations',
   'CS3000Stations.adi',
   'STATIONID',
   'StationID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Stations',
   'CS3000Stations.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Stations',
   'CS3000Stations.adi',
   'BOXINDEX',
   'BoxIndex',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Primary_Key', 
   'STATIONID', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Stations', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'StationID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'StationNumber', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'CardConnected', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'InUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'RR_Status', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Stations', 
      'RR_SecondsLeft', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'CS3000Stationsfail' ); 

CREATE TABLE CS3000SystemRecord ( 
      NetworkID Integer,
      SystemID Integer,
      StartTimeStamp TimeStamp,
      RainfallTotal Double( 0 ),
      RRSeconds Integer,
      RRGallons Double( 0 ),
      TestSeconds Integer,
      TestGallons Double( 0 ),
      WalkThruSeconds Integer,
      WalkThruGallons Double( 0 ),
      ManualSeconds Integer,
      ManualGallons Double( 0 ),
      ManualProgramSeconds Integer,
      ManualProgramGallons Double( 0 ),
      ProgramedIrrigSeconds Integer,
      ProgramedIrrigGallons Double( 0 ),
      NonControllerSeconds Integer,
      NonControllerGallons Double( 0 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000SystemRecord',
   'CS3000SystemRecord.adi',
   'SYSTEMID',
   'SystemID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000SystemRecord',
   'CS3000SystemRecord.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000SystemRecord',
   'CS3000SystemRecord.adi',
   'STARTTIMESTAMP',
   'StartTimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000SystemRecord', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000SystemRecordfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000SystemRecord', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000SystemRecordfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000SystemRecord', 
      'SystemID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000SystemRecordfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000SystemRecord', 
      'StartTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000SystemRecordfail' ); 

CREATE TABLE CS3000Systems ( 
      SystemID AutoInc,
      NetworkID Integer,
      SystemGID Integer,
      SystemName Char( 50 ),
      Deleted Logical,
      UsedForIrrig Logical,
      CapacityInUse Logical,
      CapacityWithPump Integer,
      CapacityWithoutPump Integer,
      MLBDuringIrrig Integer,
      MLBDuringMVOR Integer,
      MLBAllOtherTimes Integer,
      FlowCheckingInUse Logical,
      DerateAllowedToLock Logical,
      DerateSlotSize Integer,
      DerateMaxStationOn Integer,
      DerateNumberOfSlots Integer,
      DerateNumberOfStation Integer,
      DerateNumberOfCell Integer,
      FlowCheckingRange1 Integer,
      FlowCheckingRange2 Integer,
      FlowCheckingRange3 Integer,
      FlowTolerancePlus1 Integer,
      FlowToleranceMinus1 Integer,
      FlowTolerancePlus2 Integer,
      FlowToleranceMinus2 Integer,
      FlowTolerancePlus3 Integer,
      FlowToleranceMinus3 Integer,
      FlowTolerancePlus4 Integer,
      FlowToleranceMinus4 Integer,
      MVORScheduleOpen0 Integer,
      MVORScheduleClose0 Integer,
      MVORScheduleOpen1 Integer,
      MVORScheduleClose1 Integer,
      MVORScheduleOpen2 Integer,
      MVORScheduleClose2 Integer,
      MVORScheduleOpen3 Integer,
      MVORScheduleClose3 Integer,
      MVORScheduleOpen4 Integer,
      MVORScheduleClose4 Integer,
      MVORScheduleOpen5 Integer,
      MVORScheduleClose5 Integer,
      MVORScheduleOpen6 Integer,
      MVORScheduleClose6 Integer,
      InUse Logical,
      BudgetUseChoice Integer,
      BudgetMeterReadTime Integer,
      BudgetAnnualPeriods Integer,
      BudgetPeriodStart1 Integer,
      BudgetPeriodStart2 Integer,
      BudgetPeriodStart3 Integer,
      BudgetPeriodStart4 Integer,
      BudgetPeriodStart5 Integer,
      BudgetPeriodStart6 Integer,
      BudgetPeriodStart7 Integer,
      BudgetPeriodStart8 Integer,
      BudgetPeriodStart9 Integer,
      BudgetPeriodStart10 Integer,
      BudgetPeriodStart11 Integer,
      BudgetPeriodStart12 Integer,
      BudgetPeriodEnd1 Integer,
      BudgetPeriodEnd2 Integer,
      BudgetPeriodEnd3 Integer,
      BudgetPeriodEnd4 Integer,
      BudgetPeriodEnd5 Integer,
      BudgetPeriodEnd6 Integer,
      BudgetPeriodEnd7 Integer,
      BudgetPeriodEnd8 Integer,
      BudgetPeriodEnd9 Integer,
      BudgetPeriodEnd10 Integer,
      BudgetPeriodEnd11 Integer,
      BudgetPeriodEnd12 Integer,
      BudgetHaltIrrigationAtBudget Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Systems',
   'CS3000Systems.adi',
   'SYSTEMID',
   'SystemID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Systems',
   'CS3000Systems.adi',
   'NETWORDID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Primary_Key', 
   'SYSTEMID', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Systems', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'SystemID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'Deleted', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'Deleted', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'UsedForIrrig', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'UsedForIrrig', 'Comment', 
      'Used for irrigation', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'CapacityInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'CapacityWithPump', 'Field_Default_Value', 
      '200', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'CapacityWithoutPump', 'Field_Default_Value', 
      '200', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'MLBDuringIrrig', 'Field_Default_Value', 
      '400', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'MLBDuringMVOR', 'Field_Default_Value', 
      '150', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'MLBAllOtherTimes', 'Field_Default_Value', 
      '150', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowCheckingInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'DerateAllowedToLock', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'DerateNumberOfSlots', 'Comment', 
      'Derate table number of gpm slots', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'DerateNumberOfStation', 'Comment', 
      'Derate table before checking number of cell station cycles', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'DerateNumberOfCell', 'Comment', 
      'Derate table before checking number of cell iterations', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowCheckingRange1', 'Field_Default_Value', 
      '30', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowCheckingRange2', 'Field_Default_Value', 
      '65', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowCheckingRange3', 'Field_Default_Value', 
      '100', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowTolerancePlus1', 'Field_Default_Value', 
      '5', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowToleranceMinus1', 'Field_Default_Value', 
      '5', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowTolerancePlus2', 'Field_Default_Value', 
      '10', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowToleranceMinus2', 'Field_Default_Value', 
      '10', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowTolerancePlus3', 'Field_Default_Value', 
      '10', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowToleranceMinus3', 'Field_Default_Value', 
      '10', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowTolerancePlus4', 'Field_Default_Value', 
      '15', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Systems', 
      'FlowToleranceMinus4', 'Field_Default_Value', 
      '15', 'APPEND_FAIL', 'CS3000Systemsfail' ); 

CREATE TABLE CS3000Weather ( 
      NetworkID Integer,
      ET_RainTableRollTime Time,
      RainBucket_isInUse Logical,
      RainBucketMaxHourly Integer,
      RainBucketMinInches Integer,
      RainBucketMax24Hour Integer,
      ETG_isInUse Logical,
      ETG_LogPulses Logical,
      ETG_HistCapPercent Integer,
      ETG_HistMinPercent Integer,
      WindGage_isInUse Logical,
      WindPauseMPG Integer,
      WindPauseMinutes Integer,
      WindResumeMPG Integer,
      WindResumeMinutes Integer,
      RainSwitch_IsInUse Logical,
      FreezeSwitch_isInUse Logical,
      UseYourOwnET Logical,
      UserDefinedState Char( 50 ),
      UserDefinedCity Char( 50 ),
      UserDefinedCounty Char( 50 ),
      ETUserMonth1 Integer,
      ETUserMonth2 Integer,
      ETUserMonth3 Integer,
      ETUserMonth4 Integer,
      ETUserMonth5 Integer,
      ETUserMonth6 Integer,
      ETUserMonth7 Integer,
      ETUserMonth8 Integer,
      ETUserMonth9 Integer,
      ETUserMonth10 Integer,
      ETUserMonth11 Integer,
      ETUserMonth12 Integer,
      ET_StateIndex Integer,
      ET_CityIndex Integer,
      ET_CountyIndex Integer,
      ETGageBoxIndex Integer,
      WindGageBoxIndex Integer,
      RainBucketBoxIndex Integer,
      HasRainSwitch1 Logical,
      HasRainSwitch2 Logical,
      HasRainSwitch3 Logical,
      HasRainSwitch4 Logical,
      HasRainSwitch5 Logical,
      HasRainSwitch6 Logical,
      HasRainSwitch7 Logical,
      HasRainSwitch8 Logical,
      HasRainSwitch9 Logical,
      HasRainSwitch10 Logical,
      HasRainSwitch11 Logical,
      HasRainSwitch12 Logical,
      HasFreezeSwitch1 Logical,
      HasFreezeSwitch2 Logical,
      HasFreezeSwitch3 Logical,
      HasFreezeSwitch4 Logical,
      HasFreezeSwitch5 Logical,
      HasFreezeSwitch6 Logical,
      HasFreezeSwitch7 Logical,
      HasFreezeSwitch8 Logical,
      HasFreezeSwitch9 Logical,
      HasFreezeSwitch10 Logical,
      HasFreezeSwitch11 Logical,
      HasFreezeSwitch12 Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Weather',
   'CS3000Weather.adi',
   'CONTROLLERID',
   'NetworkID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Primary_Key', 
   'CONTROLLERID', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Weather', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'RainBucket_isInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'RainBucket_isInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'ETG_isInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'ETG_isInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'ETG_LogPulses', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'ETG_LogPulses', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'WindGage_isInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'WindGage_isInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'RainSwitch_IsInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'RainSwitch_IsInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'FreezeSwitch_isInUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'FreezeSwitch_isInUse', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch1', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch2', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch3', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch4', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch5', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch6', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch7', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch8', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch9', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch10', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch11', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasRainSwitch12', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch1', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch2', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch3', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch4', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch5', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch6', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch7', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch8', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch9', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch10', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch11', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Weather', 
      'HasFreezeSwitch12', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'CS3000Weatherfail' ); 

CREATE TABLE CS3000WeatherData ( 
      NetworkID Integer,
      WeatherTimeStamp TimeStamp,
      ETValue Integer,
      ETStatus Integer,
      RainValue Integer,
      RainStatus Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000WeatherData',
   'CS3000WeatherData.adi',
   'WDNETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000WeatherDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000WeatherDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000WeatherDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000WeatherDatafail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000WeatherData', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000WeatherDatafail');

CREATE TABLE Temp_CS3000Controllers ( 
      ControllerID Integer,
      NetworkID Integer,
      Name Char( 50 ),
      SerialNumber Integer,
      FLOption Logical,
      SSEOption Logical,
      SSE_DOption Logical,
      ElectricalStationsOnLimit Short,
      FOAL_MinutesBetweenRescanAttempts Integer,
      OM_OriginatorRetries Integer,
      OM_SecondsForStatus_FOAL Integer,
      OM_MinutesToExist Integer,
      TestSeconds Integer,
      LastAssignedDecoderSN Integer,
      WeatherOption Logical,
      HasRainBucket Logical,
      HasETgage Logical,
      HasWindGage Logical,
      WeatherCard Logical,
      WeatherTerminal Logical,
      DashM_Card Logical,
      DashM_Terminal Logical,
      DashM_CardType Logical,
      TwoWireTerminal Logical,
      PortA_DeviceIndex Integer,
      PortB_DeviceIndex Integer,
      BoxIndex Integer,
      HasRainSwitch Logical,
      HasFreezeSwitch Logical,
      UserId Integer,
      EventType Integer,
      Columns Char( 600 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Controllers', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Controllersfail');

CREATE TABLE Temp_CS3000ManualPrograms ( 
      ProgramID Integer,
      NetworkID Integer,
      ProgramGID Integer,
      Name Char( 50 ),
      Deleted Logical,
      StartTime1 Integer,
      StartTime2 Integer,
      StartTime3 Integer,
      StartTime4 Integer,
      StartTime5 Integer,
      StartTime6 Integer,
      Day1 Logical,
      Day2 Logical,
      Day3 Logical,
      Day4 Logical,
      Day5 Logical,
      Day6 Logical,
      Day7 Logical,
      StartDate Date,
      StopDate Date,
      RunTime Integer,
      InUse Logical,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000ManualPrograms', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000ManualProgramsfail');

CREATE TABLE Temp_CS3000Networks ( 
      NetworkID Integer,
      MasterTimeStamp TimeStamp,
      CompanyID Integer,
      NetworkName Char( 50 ),
      Deleted Logical,
      LastCommunicated TimeStamp,
      MainboardForceFirmware CIChar( 255 ),
      TPMicroForceFirmware CIChar( 255 ),
      StartOfIrrigationDay Integer,
      ScheduledIrrigationIsOff Logical,
      TimeZone Integer,
      DlsInUse Logical,
      DlsSpringMonth Integer,
      DlsSpringMinDay Integer,
      DlsSpringMaxDay Integer,
      DlsFallBackMonth Integer,
      DlsFallBackMinDay Integer,
      DlsFallBackMaxDay Integer,
      RainPollingEnabled Logical,
      SendFlowRecording Logical,
      BoxName1 Char( 50 ),
      BoxName2 Char( 50 ),
      BoxName3 Char( 50 ),
      BoxName4 Char( 50 ),
      BoxName5 Char( 50 ),
      BoxName6 Char( 50 ),
      BoxName7 Char( 50 ),
      BoxName8 Char( 50 ),
      BoxName9 Char( 50 ),
      BoxName10 Char( 50 ),
      BoxName11 Char( 50 ),
      BoxName12 Char( 50 ),
      ElectricalStationOnLimit1 Integer,
      ElectricalStationOnLimit2 Integer,
      ElectricalStationOnLimit3 Integer,
      ElectricalStationOnLimit4 Integer,
      ElectricalStationOnLimit5 Integer,
      ElectricalStationOnLimit6 Integer,
      ElectricalStationOnLimit7 Integer,
      ElectricalStationOnLimit8 Integer,
      ElectricalStationOnLimit9 Integer,
      ElectricalStationOnLimit10 Integer,
      ElectricalStationOnLimit11 Integer,
      ElectricalStationOnLimit12 Integer,
      MasterSerialNumber Integer,
      ETSharingNetworkID Integer,
      RainSharingNetworkID Integer,
      RR_ScreenData Blob,
      RR_ScreenLastUpdated TimeStamp,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 ),
      CreatedDate TimeStamp,
      Status Integer,
      NotifyNewDataArrives Integer) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Networks', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Networksfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Temp_CS3000Networks', 
      'Status', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'Temp_CS3000Networksfail' ); 

CREATE TABLE Temp_CS3000POC ( 
      POC_ID Integer,
      SystemGID Integer,
      POC_GID Integer,
      Name Char( 50 ),
      Deleted Logical,
      BoxIndex Integer,
      TypeOfPOC Short,
      DecoderSerialNumber1 Integer,
      DecoderSerialNumber2 Integer,
      DecoderSerialNumber3 Integer,
      POC_CardConnected Logical,
      POC_TerminalConnected Logical,
      POC_Usage Integer,
      BudgetOption Short,
      BudgetPercentOfET Double( 0 ),
      BudgetMonthly1 Double( 0 ),
      BudgetMonthly2 Double( 0 ),
      BudgetMonthly3 Double( 0 ),
      BudgetMonthly4 Double( 0 ),
      BudgetMonthly5 Double( 0 ),
      BudgetMonthly6 Double( 0 ),
      BudgetMonthly7 Double( 0 ),
      BudgetMonthly8 Double( 0 ),
      BudgetMonthly9 Double( 0 ),
      BudgetMonthly10 Double( 0 ),
      BudgetMonthly11 Double( 0 ),
      BudgetMonthly12 Double( 0 ),
      MasterValveType1 Short,
      MasterValveType2 Short,
      MasterValveType3 Short,
      FlowMeterChoice1 Short,
      FlowMeterK_Value1 Integer,
      FlowMeterOffset_Value1 Integer,
      FlowMeterChoice2 Short,
      FlowMeterK_Value2 Integer,
      FlowMeterOffset_Value2 Integer,
      FlowMeterChoice3 Short,
      FlowMeterK_Value3 Integer,
      FlowMeterOffset_Value3 Integer,
      NetworkID Integer,
      NumberOfLevels Integer,
      FlowMeterReedSwitch1 Integer,
      FlowMeterReedSwitch2 Integer,
      FlowMeterReedSwitch3 Integer,
      BudgetGallonsEntryOption Integer,
      BudgetCalcPercentOfET Integer,
      BudgetAutoAdjustUsingLiveET Logical,
      BudgetClosePOCAtBudget Integer,
      BudgetGallons1 Integer,
      BudgetGallons2 Integer,
      BudgetGallons3 Integer,
      BudgetGallons4 Integer,
      BudgetGallons5 Integer,
      BudgetGallons6 Integer,
      BudgetGallons7 Integer,
      BudgetGallons8 Integer,
      BudgetGallons9 Integer,
      BudgetGallons10 Integer,
      BudgetGallons11 Integer,
      BudgetGallons12 Integer,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000POCfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000POC', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000POCfail');

CREATE TABLE Temp_CS3000StationGroups ( 
      StationGroupID Integer,
      NetworkID Integer,
      StationGroupGID Integer,
      Name Char( 50 ),
      Deleted Logical,
      PlantType Short,
      HeadType Short,
      PrecipRate Double( 0 ),
      SoilType Short,
      SlopePercentage Double( 0 ),
      Exposure Short,
      UsableRainPercentage Double( 0 ),
      PriorityLevel Integer,
      PercentAdjust Double( 0 ),
      StartDate Date,
      EndDate Date,
      Enabled Logical,
      ScheduleType Integer,
      BeginDate Date,
      IrrigateOn29Or31 Logical,
      MowDay Integer,
      LastRan TimeStamp,
      TailEndsThrowAway Integer,
      TailEndsMakeAdjustment Integer,
      MaximumLeftover Integer,
      ET_Mode Integer,
      UseET_Averaging Logical,
      RainInUse Logical,
      MaximumAccumulation Integer,
      WindInUse Logical,
      HighFlowAction Integer,
      LowFlowAction Integer,
      On_At_A_TimeInGroup Double( 0 ),
      On_At_A_TimeInSystem Double( 0 ),
      PresentlyOnGroupCount Integer,
      PumpInUse Logical,
      LineFillTime Integer,
      ValveCloseTime Integer,
      AcquireExpecteds Logical,
      CropCoefficient1 Double( 0 ),
      CropCoefficient2 Double( 0 ),
      CropCoefficient3 Double( 0 ),
      CropCoefficient4 Double( 0 ),
      CropCoefficient5 Double( 0 ),
      CropCoefficient6 Double( 0 ),
      CropCoefficient7 Double( 0 ),
      CropCoefficient8 Double( 0 ),
      CropCoefficient9 Double( 0 ),
      CropCoefficient10 Double( 0 ),
      CropCoefficient11 Double( 0 ),
      CropCoefficient12 Double( 0 ),
      WaterDaysSun Logical,
      WaterDaysMon Logical,
      WaterDaysTue Logical,
      WaterDaysWed Logical,
      WaterDaysThu Logical,
      WaterDaysFri Logical,
      WaterDaysSat Logical,
      StartTime Integer,
      StopTime Integer,
      AllowableDepletion Integer,
      AvailableWater Integer,
      RootDepth Integer,
      SpeciesFactor Integer,
      DensityFactor Integer,
      MicroclimateFactor Integer,
      SoilIntakeRate Integer,
      AllowableSurfaceAccum Integer,
      BudgetReductionCap Integer,
      SystemID Integer,
      InUse Logical,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000StationGroups', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Temp_CS3000StationGroups', 
      'Deleted', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'Temp_CS3000StationGroupsfail' ); 

CREATE TABLE Temp_CS3000Stations ( 
      StationID Integer,
      NetworkID Integer,
      Name Char( 50 ),
      StationNumber Integer,
      Deleted Logical,
      CardConnected Logical,
      InUse Logical,
      DecoderSerialNumber Integer,
      DecoderOutput Integer,
      TotalMinutesRun Double( 0 ),
      CycleMinutes Double( 0 ),
      SoakMinutes Double( 0 ),
      ET_Factor Integer,
      ExpectedFlowRate Integer,
      DistributionUniformity Integer,
      NoWaterDays Integer,
      ManualProgramA Integer,
      ManualProgramB Integer,
      StationGroupID Integer,
      SystemID Integer,
      ControllerID Integer,
      ManualProgramA_ID Integer,
      ManualProgramB_ID Integer,
      BoxIndex Integer,
      MoistureBalance Double( 0 ),
      SquareFootage Integer,
      RR_Status Integer,
      RR_SecondsLeft Integer,
      RR_StartDateTime TimeStamp,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Stations', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Stationsfail');

CREATE TABLE Temp_CS3000Systems ( 
      SystemID Integer,
      NetworkID Integer,
      SystemGID Integer,
      SystemName Char( 50 ),
      Deleted Logical,
      UsedForIrrig Logical,
      CapacityInUse Logical,
      CapacityWithPump Integer,
      CapacityWithoutPump Integer,
      MLBDuringIrrig Integer,
      MLBDuringMVOR Integer,
      MLBAllOtherTimes Integer,
      FlowCheckingInUse Logical,
      DerateAllowedToLock Logical,
      DerateSlotSize Integer,
      DerateMaxStationOn Integer,
      DerateNumberOfSlots Integer,
      DerateNumberOfStation Integer,
      DerateNumberOfCell Integer,
      FlowCheckingRange1 Integer,
      FlowCheckingRange2 Integer,
      FlowCheckingRange3 Integer,
      FlowTolerancePlus1 Integer,
      FlowToleranceMinus1 Integer,
      FlowTolerancePlus2 Integer,
      FlowToleranceMinus2 Integer,
      FlowTolerancePlus3 Integer,
      FlowToleranceMinus3 Integer,
      FlowTolerancePlus4 Integer,
      FlowToleranceMinus4 Integer,
      MVORScheduleOpen0 Integer,
      MVORScheduleClose0 Integer,
      MVORScheduleOpen1 Integer,
      MVORScheduleClose1 Integer,
      MVORScheduleOpen2 Integer,
      MVORScheduleClose2 Integer,
      MVORScheduleOpen3 Integer,
      MVORScheduleClose3 Integer,
      MVORScheduleOpen4 Integer,
      MVORScheduleClose4 Integer,
      MVORScheduleOpen5 Integer,
      MVORScheduleClose5 Integer,
      MVORScheduleOpen6 Integer,
      MVORScheduleClose6 Integer,
      InUse Logical,
      BudgetUseChoice Integer,
      BudgetMeterReadTime Integer,
      BudgetAnnualPeriods Integer,
      BudgetPeriodStart1 Integer,
      BudgetPeriodStart2 Integer,
      BudgetPeriodStart3 Integer,
      BudgetPeriodStart4 Integer,
      BudgetPeriodStart5 Integer,
      BudgetPeriodStart6 Integer,
      BudgetPeriodStart7 Integer,
      BudgetPeriodStart8 Integer,
      BudgetPeriodStart9 Integer,
      BudgetPeriodStart10 Integer,
      BudgetPeriodStart11 Integer,
      BudgetPeriodStart12 Integer,
      BudgetPeriodEnd1 Integer,
      BudgetPeriodEnd2 Integer,
      BudgetPeriodEnd3 Integer,
      BudgetPeriodEnd4 Integer,
      BudgetPeriodEnd5 Integer,
      BudgetPeriodEnd6 Integer,
      BudgetPeriodEnd7 Integer,
      BudgetPeriodEnd8 Integer,
      BudgetPeriodEnd9 Integer,
      BudgetPeriodEnd10 Integer,
      BudgetPeriodEnd11 Integer,
      BudgetPeriodEnd12 Integer,
      BudgetHaltIrrigationAtBudget Logical,
      UserId Integer,
      EventType Integer,
      Columns Char( 1600 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Systems', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Systemsfail');

CREATE TABLE Temp_CS3000Weather ( 
      NetworkID Integer,
      ET_RainTableRollTime Time,
      RainBucket_isInUse Logical,
      RainBucketMaxHourly Integer,
      RainBucketMinInches Integer,
      RainBucketMax24Hour Integer,
      ETG_isInUse Logical,
      ETG_LogPulses Logical,
      ETG_HistCapPercent Integer,
      ETG_HistMinPercent Integer,
      WindGage_isInUse Logical,
      WindPauseMPG Integer,
      WindPauseMinutes Integer,
      WindResumeMPG Integer,
      WindResumeMinutes Integer,
      RainSwitch_IsInUse Logical,
      FreezeSwitch_isInUse Logical,
      UseYourOwnET Logical,
      UserDefinedState Char( 50 ),
      UserDefinedCity Char( 50 ),
      UserDefinedCounty Char( 50 ),
      ETUserMonth1 Integer,
      ETUserMonth2 Integer,
      ETUserMonth3 Integer,
      ETUserMonth4 Integer,
      ETUserMonth5 Integer,
      ETUserMonth6 Integer,
      ETUserMonth7 Integer,
      ETUserMonth8 Integer,
      ETUserMonth9 Integer,
      ETUserMonth10 Integer,
      ETUserMonth11 Integer,
      ETUserMonth12 Integer,
      ET_StateIndex Integer,
      ET_CityIndex Integer,
      ET_CountyIndex Integer,
      ETGageBoxIndex Integer,
      WindGageBoxIndex Integer,
      RainBucketBoxIndex Integer,
      HasRainSwitch1 Logical,
      HasRainSwitch2 Logical,
      HasRainSwitch3 Logical,
      HasRainSwitch4 Logical,
      HasRainSwitch5 Logical,
      HasRainSwitch6 Logical,
      HasRainSwitch7 Logical,
      HasRainSwitch8 Logical,
      HasRainSwitch9 Logical,
      HasRainSwitch10 Logical,
      HasRainSwitch11 Logical,
      HasRainSwitch12 Logical,
      HasFreezeSwitch1 Logical,
      HasFreezeSwitch2 Logical,
      HasFreezeSwitch3 Logical,
      HasFreezeSwitch4 Logical,
      HasFreezeSwitch5 Logical,
      HasFreezeSwitch6 Logical,
      HasFreezeSwitch7 Logical,
      HasFreezeSwitch8 Logical,
      HasFreezeSwitch9 Logical,
      HasFreezeSwitch10 Logical,
      HasFreezeSwitch11 Logical,
      HasFreezeSwitch12 Logical,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Weather', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Weatherfail');

CREATE TABLE CS3000CommServerSettings ( 
      SetControllersClock Time,
      SendWeatherData Time) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000CommServerSettings', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000CommServerSettingsfail');

CREATE TABLE IHSFY_jobs ( 
      Idx AutoInc,
      Command_id Integer,
      Network_id Integer,
      User_id Integer,
      Date_created TimeStamp,
      Status Integer,
      Processing_start TimeStamp,
      Param1 Integer,
      Param2 Integer,
      Param3 Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'IHSFY_jobs',
   'IHSFY_jobs.adi',
   'NETWORK_ID',
   'Network_id',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_jobs', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'IHSFY_jobsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_jobs', 
      'Network_id', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_jobsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_jobs', 
      'Date_created', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_jobsfail' ); 

CREATE TABLE IHSFY_notifications ( 
      Idx Integer,
      Command_id Integer,
      Network_id Integer,
      User_id Integer,
      Date_created TimeStamp,
      Status Integer,
      Param1 Integer,
      Param2 Integer,
      Param3 Integer,
      Param4 Blob) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'IHSFY_notifications',
   'IHSFY_notifications.adi',
   'NETWORK_ID',
   'Network_id',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'IHSFY_notifications', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'IHSFY_notificationsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_notifications', 
      'Command_id', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_notificationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_notifications', 
      'Network_id', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_notificationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_notifications', 
      'Status', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'IHSFY_notificationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'IHSFY_notifications', 
      'Status', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'IHSFY_notificationsfail' ); 

CREATE TABLE Manual ( 
      ControllerId Integer,
      MpTimeStamp TimeStamp,
      DataStatus Integer,
      MP1_Name Char( 26 ),
      MP1_Sch1 Logical,
      MP1_Sch2 Logical,
      MP1_Sch3 Logical,
      MP1_Sch4 Logical,
      MP1_Sch5 Logical,
      MP1_Sch6 Logical,
      MP1_Sch7 Logical,
      MP1_StartTime1 Integer,
      MP1_StartTime2 Integer,
      MP1_StartTime3 Integer,
      MP1_StartTime4 Integer,
      MP1_StartTime5 Integer,
      MP1_StartTime6 Integer,
      MP1_StartDate TimeStamp,
      MP1_StopDate TimeStamp,
      MP1_UseHO Logical,
      MP1_SuspendProgrammed Logical,
      MP2_Name Char( 26 ),
      MP2_Sch1 Logical,
      MP2_Sch2 Logical,
      MP2_Sch3 Logical,
      MP2_Sch4 Logical,
      MP2_Sch5 Logical,
      MP2_Sch6 Logical,
      MP2_Sch7 Logical,
      MP2_StartTime1 Integer,
      MP2_StartTime2 Integer,
      MP2_StartTime3 Integer,
      MP2_StartTime4 Integer,
      MP2_StartTime5 Integer,
      MP2_StartTime6 Integer,
      MP2_StartDate TimeStamp,
      MP2_StopDate TimeStamp,
      MP2_UseHO Logical,
      MP2_SuspendProgrammed Logical,
      MVOR_Open_Sch1 Integer,
      MVOR_Close_Sch1 Integer,
      MVOR_Open_Sch2 Integer,
      MVOR_Close_Sch2 Integer,
      MVOR_Open_Sch3 Integer,
      MVOR_Close_Sch3 Integer,
      MVOR_Open_Sch4 Integer,
      MVOR_Close_Sch4 Integer,
      MVOR_Open_Sch5 Integer,
      MVOR_Close_Sch5 Integer,
      MVOR_Open_Sch6 Integer,
      MVOR_Close_Sch6 Integer,
      MVOR_Open_Sch7 Integer,
      MVOR_Close_Sch7 Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Manual',
   'Manual.adi',
   'CONTROLLERID',
   'ControllerId',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Manual', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Manualfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Manual', 
   'Table_Primary_Key', 
   'CONTROLLERID', 'APPEND_FAIL', 'Manualfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Manual', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Manualfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Manual', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'Manualfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Manual', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Manualfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Manual', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Manualfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Manual', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Manualfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'ControllerId', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MpTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'DataStatus', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'DataStatus', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_Sch1', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_Sch1', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_Sch2', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_Sch3', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_Sch4', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_Sch5', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_Sch6', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_Sch7', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_StartTime1', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_StartTime2', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_StartTime3', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_StartTime4', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_StartTime5', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP1_StartTime6', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_Sch1', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_Sch2', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_Sch3', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_Sch4', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_Sch5', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_Sch6', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_Sch7', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_StartTime1', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_StartTime2', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_StartTime3', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_StartTime4', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_StartTime5', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MP2_StartTime6', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Open_Sch1', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Close_Sch1', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Open_Sch2', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Close_Sch2', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Open_Sch3', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Close_Sch3', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Open_Sch4', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Close_Sch4', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Open_Sch5', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Close_Sch5', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Open_Sch6', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Close_Sch6', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Open_Sch7', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Manual', 
      'MVOR_Close_Sch7', 'Field_Default_Value', 
      '86400', 'APPEND_FAIL', 'Manualfail' ); 

CREATE TABLE ManualStations ( 
      ControllerId Integer,
      MpTimeStamp TimeStamp,
      StationNumber Integer,
      InUse Logical,
      MP1_Seconds Integer,
      MP2_Seconds Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ManualStations',
   'ManualStations.adi',
   'CONTROLLERID',
   'ControllerId',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ManualStations',
   'ManualStations.adi',
   'STATIONNUMBER',
   'StationNumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ManualStations', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ManualStationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ManualStations', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ManualStationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ManualStations', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'ManualStationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ManualStations', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ManualStationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ManualStations', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ManualStationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ManualStations', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'ManualStationsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ManualStations', 
      'ControllerId', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ManualStationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ManualStations', 
      'MpTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ManualStationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ManualStations', 
      'StationNumber', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ManualStationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ManualStations', 
      'InUse', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ManualStationsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ManualStations', 
      'InUse', 'Field_Default_Value', 
      'false', 'APPEND_FAIL', 'ManualStationsfail' ); 

CREATE TABLE Lights ( 
      ControllerId Integer,
      LightTimeStamp TimeStamp,
      LightData Blob) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Lights',
   'Lights.adi',
   'CONTROLLERID',
   'ControllerId',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Lights', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Lights', 
   'Table_Primary_Key', 
   'CONTROLLERID', 'APPEND_FAIL', 'Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Lights', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Lights', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Lights', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Lights', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Lights', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Lightsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Lights', 
      'ControllerId', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Lightsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Lights', 
      'LightTimeStamp', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Lightsfail' ); 

CREATE TABLE CS3000Lights ( 
      LightID AutoInc,
      NetworkID Integer,
      BoxIndex Integer,
      Name Char( 48 ),
      OutputIndex Integer,
      CardConnected Logical,
      Day00_StartTime1 Integer,
      Day00_StartTime2 Integer,
      Day00_StopTime1 Integer,
      Day00_StopTime2 Integer,
      Day01_StartTime1 Integer,
      Day01_StartTime2 Integer,
      Day01_StopTime1 Integer,
      Day01_StopTime2 Integer,
      Day02_StartTime1 Integer,
      Day02_StartTime2 Integer,
      Day02_StopTime1 Integer,
      Day02_StopTime2 Integer,
      Day03_StartTime1 Integer,
      Day03_StartTime2 Integer,
      Day03_StopTime1 Integer,
      Day03_StopTime2 Integer,
      Day04_StartTime1 Integer,
      Day04_StartTime2 Integer,
      Day04_StopTime1 Integer,
      Day04_StopTime2 Integer,
      Day05_StartTime1 Integer,
      Day05_StartTime2 Integer,
      Day05_StopTime1 Integer,
      Day05_StopTime2 Integer,
      Day06_StartTime1 Integer,
      Day06_StartTime2 Integer,
      Day06_StopTime1 Integer,
      Day06_StopTime2 Integer,
      Day07_StartTime1 Integer,
      Day07_StartTime2 Integer,
      Day07_StopTime1 Integer,
      Day07_StopTime2 Integer,
      Day08_StartTime1 Integer,
      Day08_StartTime2 Integer,
      Day08_StopTime1 Integer,
      Day08_StopTime2 Integer,
      Day09_StartTime1 Integer,
      Day09_StartTime2 Integer,
      Day09_StopTime1 Integer,
      Day09_StopTime2 Integer,
      Day10_StartTime1 Integer,
      Day10_StartTime2 Integer,
      Day10_StopTime1 Integer,
      Day10_StopTime2 Integer,
      Day11_StartTime1 Integer,
      Day11_StartTime2 Integer,
      Day11_StopTime1 Integer,
      Day11_StopTime2 Integer,
      Day12_StartTime1 Integer,
      Day12_StartTime2 Integer,
      Day12_StopTime1 Integer,
      Day12_StopTime2 Integer,
      Day13_StartTime1 Integer,
      Day13_StartTime2 Integer,
      Day13_StopTime1 Integer,
      Day13_StopTime2 Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Lights',
   'CS3000Lights.adi',
   'LIGHTID',
   'LightID',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Lights',
   'CS3000Lights.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000Lights',
   'CS3000Lights.adi',
   'BOXINDEX',
   'BoxIndex',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Lights', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Lights', 
   'Table_Primary_Key', 
   'LIGHTID', 'APPEND_FAIL', 'CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Lights', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Lights', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Lights', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Lights', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000Lights', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Lights', 
      'LightID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Lightsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Lights', 
      'NetworkID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Lightsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CS3000Lights', 
      'BoxIndex', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'CS3000Lightsfail' ); 

CREATE TABLE CS3000LightsRecord ( 
      NetworkID Integer,
      BoxIndex Integer,
      OutputID Integer,
      StartTimeStamp TimeStamp,
      ProgrammedSeconds Integer,
      ManualSeconds Integer,
      NumberOfRepeats Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000LightsRecord',
   'CS3000LightsRecord.adi',
   'NETWORKID',
   'NetworkID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CS3000LightsRecord',
   'CS3000LightsRecord.adi',
   'BOXINDEX',
   'BoxIndex',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000LightsRecord', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CS3000LightsRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000LightsRecord', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CS3000LightsRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000LightsRecord', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'CS3000LightsRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000LightsRecord', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CS3000LightsRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000LightsRecord', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'CS3000LightsRecordfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CS3000LightsRecord', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'CS3000LightsRecordfail');

CREATE TABLE Temp_CS3000Lights ( 
      LightID Integer,
      NetworkID Integer,
      BoxIndex Integer,
      Name Char( 48 ),
      OutputIndex Integer,
      CardConnected Logical,
      Day00_StartTime1 Integer,
      Day00_StartTime2 Integer,
      Day00_StopTime1 Integer,
      Day00_StopTime2 Integer,
      Day01_StartTime1 Integer,
      Day01_StartTime2 Integer,
      Day01_StopTime1 Integer,
      Day01_StopTime2 Integer,
      Day02_StartTime1 Integer,
      Day02_StartTime2 Integer,
      Day02_StopTime1 Integer,
      Day02_StopTime2 Integer,
      Day03_StartTime1 Integer,
      Day03_StartTime2 Integer,
      Day03_StopTime1 Integer,
      Day03_StopTime2 Integer,
      Day04_StartTime1 Integer,
      Day04_StartTime2 Integer,
      Day04_StopTime1 Integer,
      Day04_StopTime2 Integer,
      Day05_StartTime1 Integer,
      Day05_StartTime2 Integer,
      Day05_StopTime1 Integer,
      Day05_StopTime2 Integer,
      Day06_StartTime1 Integer,
      Day06_StartTime2 Integer,
      Day06_StopTime1 Integer,
      Day06_StopTime2 Integer,
      Day07_StartTime1 Integer,
      Day07_StartTime2 Integer,
      Day07_StopTime1 Integer,
      Day07_StopTime2 Integer,
      Day08_StartTime1 Integer,
      Day08_StartTime2 Integer,
      Day08_StopTime1 Integer,
      Day08_StopTime2 Integer,
      Day09_StartTime1 Integer,
      Day09_StartTime2 Integer,
      Day09_StopTime1 Integer,
      Day09_StopTime2 Integer,
      Day10_StartTime1 Integer,
      Day10_StartTime2 Integer,
      Day10_StopTime1 Integer,
      Day10_StopTime2 Integer,
      Day11_StartTime1 Integer,
      Day11_StartTime2 Integer,
      Day11_StopTime1 Integer,
      Day11_StopTime2 Integer,
      Day12_StartTime1 Integer,
      Day12_StartTime2 Integer,
      Day12_StopTime1 Integer,
      Day12_StopTime2 Integer,
      Day13_StartTime1 Integer,
      Day13_StartTime2 Integer,
      Day13_StopTime1 Integer,
      Day13_StopTime2 Integer,
      UserId Integer,
      EventType Integer,
      Columns Char( 1000 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Lights', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Lights', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Temp_CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Lights', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Lights', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Lightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Temp_CS3000Lights', 
   'Table_WEB_delta', 
   'False', 'APPEND_FAIL', 'Temp_CS3000Lightsfail');

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_Usrs_Companies',
     'Companies', 
     'Usrs', 
     'COMPANYID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_Controllers_Sites',
     'ControllerSites', 
     'Controllers', 
     'SITEID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_AlertFiltering_Group',
     'AlrtFltrGrp', 
     'AlertFiltering', 
     'FK_ALRTFLTRGRP', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_AlertFilterOptions_Parent',
     'AlertFilterOptions', 
     'AlertFilterOptions', 
     'PARENTID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_AlertFiltering_Options',
     'AlertFilterOptions', 
     'AlertFiltering', 
     'ALERTFILTER', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_TaskData_Company',
     'Companies', 
     'TaskData', 
     'COMPANYID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_TaskExclusion_Task',
     'TaskData', 
     'TaskExclusion', 
     'TASKID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_TaskOptions_Task',
     'TaskData', 
     'TaskOptions', 
     'TASKID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_TaskSchedule_Task',
     'TaskData', 
     'TaskSchedule', 
     'TASKID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_TaskOptions_Controller',
     'Controllers', 
     'TaskOptions', 
     'CONTROLLERID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_Fnctns_TaskOptions',
     'Fnctns', 
     'TaskOptions', 
     'OPTIONID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_CMOS_Controller',
     'Controllers', 
     'CMOS', 
     'CONTROLLERID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_DailyData_Controller',
     'Controllers', 
     'DailyData', 
     'CONTROLLERID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_Usrs_Types',
     'UsrTypes', 
     'Usrs', 
     'USERTYPES', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_AlertFilterOptionDetails_Options',
     'AlertFilterOptions', 
     'AlertFilterOptionDetails', 
     'OPTIONFILTERID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_Cities_Counties',
     'County', 
     'City', 
     'COUNTYINDEX', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_Counties_States',
     'States', 
     'County', 
     'STATEINDEX', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_ControllerSites_Assignments',
     'ControllerSites', 
     'AssignmentSites', 
     'SITEID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'RI_Usrs_SiteAssignments',
     'Usrs', 
     'AssignmentSites', 
     'USERID', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_CreateLink ( 
     'GPRS',
     '\\lyell\Databases\GPRSBilling\GPRS Billing.add',
     TRUE,
     TRUE,
     FALSE,
     'AdsSys',
     ''/* YOUR PASSWORD GOES HERE */ );
EXECUTE PROCEDURE 
  sp_CreateLink ( 
     'Archive',
     '\\lyell\Databases\Web\TablesArchive\TablesArchive.add',
     TRUE,
     TRUE,
     FALSE,
     'adssys',
     ''/* YOUR PASSWORD GOES HERE */ );
CREATE VIEW 
   GprsExpirationDatesView
AS 
   SELECT AO.AccountID [AccountID], 
      '1' + A.PhoneNumber [PhoneNumber], 
       MAX(O.ServiceStartDate+PT.ExpirationDays) AS [ExpirationDate], 
       MAX(O.ServiceStartDate+PT.Warning1Days) AS [Warning1Date], 
       MAX(O.ServiceStartDate+PT.Warning2Days) AS [Warning2Date], 
       Max(A.ForceNotification) as ForceNotification
FROM   GPRS.tblCustomers C, GPRS.tblOrders O, GPRS.tblPlanTypes PT, 
       GPRS.tblAccountOrders AO, GPRS.tblJobLocations J, GPRS.tblAccounts A 
WHERE  C.CustomerID = O.CustomerID AND
	   O.JobLocationID = J.JobLocationID AND
	   O.ServiceStartDate IS NOT NULL AND
	   O.PlanID = PT.PlanID AND
	   O.OrderID = AO.OrderID AND
       A.AccountID = AO.AccountID
GROUP BY AO.AccountID, A.PhoneNumber
UNION 
SELECT AO.AccountID [AccountID],
       A.IPAddress [PhoneNumber], 
       MAX(O.ServiceStartDate+PT.ExpirationDays) AS [ExpirationDate], 
       MAX(O.ServiceStartDate+PT.Warning1Days) AS [Warning1Date], 
       MAX(O.ServiceStartDate+PT.Warning2Days) AS [Warning2Date], 
       Max(A.ForceNotification) as ForceNotification
FROM   GPRS.tblCustomers C, GPRS.tblOrders O, GPRS.tblPlanTypes PT, 
       GPRS.tblAccountOrders AO, GPRS.tblJobLocations J, GPRS.tblAccounts A 
WHERE  C.CustomerID = O.CustomerID AND
	   O.JobLocationID = J.JobLocationID AND
	   O.ServiceStartDate IS NOT NULL AND
	   O.PlanID = PT.PlanID AND
	   O.OrderID = AO.OrderID AND
       A.AccountID = AO.AccountID 
GROUP BY AO.AccountID, A.IPAddress
having A.IPAddress <> ''
;

CREATE VIEW 
   NewGPRSExpirationDatesView
AS 
   SELECT (CASE WHEN A.IPAddress<>'' THEN A.IPAddress COLLATE ads_default_cs ELSE '1' + A.PhoneNumber END) [PhoneNumber],
	   MAX(O.ServiceStartDate+PT.ExpirationDays) AS [ExpirationDate], 
	   MAX(O.ServiceStartDate+PT.Warning1Days) AS [Warning1Date], 
	   MAX(O.ServiceStartDate+PT.Warning2Days) AS [Warning2Date]
	   FROM GPRS.tblAccounts A
	   LEFT OUTER JOIN GPRS.tblAccountOrders AO ON A.AccountID=AO.AccountID
	   LEFT OUTER JOIN GPRS.tblOrders O ON O.OrderID=AO.OrderID
	   LEFT OUTER JOIN GPRS.tblPlanTypes PT ON O.PlanID = PT.PlanID
	   WHERE O.ActiveOrder = true
	   GROUP BY [PhoneNumber];

CREATE VIEW 
   CommunicationStatistics
AS 
   SELECT p.CompanyID,MAX(Name) Name,
SUM ( IIF(TimeDiff=0,NumOfController,0)) Today,
SUM ( IIF(TimeDiff=1,NumOfController,0)) Yesterday,
SUM ( IIF(TimeDiff>1 AND TimeDiff<8,NumOfController,0)) ThreeDays,
SUM ( IIF(TimeDiff>=8,NumOfController,0)) Week,
SUM ( IIF(TimeDiff IS NULL,NumOfController,0)) Never
FROM Companies p  
JOIN (
	 SELECT p.CompanyID,Count(ControllerID) NumOfController, TIMESTAMPDIFF( SQL_TSI_DAY, LastCommunicated, NOW() ) TimeDiff
	 FROM ControllerSites s 
	 JOIN Companies p on s.companyID = p.CompanyID
	 JOIN Controllers c ON s.SiteID =c.SiteID AND c.Deleted = false
	 GROUP BY p.CompanyID,TIMESTAMPDIFF( SQL_TSI_DAY, LastCommunicated, NOW() )
	 ) t ON t.CompanyID=p.CompanyID
GROUP BY p.CompanyID HAVING (Yesterday+ThreeDays+Week+Never)>0;

CREATE VIEW 
   ControllersDetails
AS 
   SELECT s.CompanyID, p.Name, s.SiteName,s.SiteID,C.ControllerName,C.ControllerID, LastCommunicated,
	   ISNULL(c.Model,0) Model, ControllerSerialNumber ,c.SoftwareVersion,m.Version,Communicate,
	   --x.ExpirationDate, x.Warning1Date, x.Warning2Date, 
	   -- ET2000e Options
	   ISNULL(c.StationsInUse,0) StationsInUse,
	   IsNULL(CommTypeA,0) CommTypeA,
	   IsNULL(CommTypeB,0) CommTypeB, 
	   IsNULL(GInterface,false) GInterface,
	   IsNULL(RBInterface,false) RBInterface,
	   IsNULL(FInterface,false) FInterface, 
	   IsNULL(WGInterface,false) AS WGInterface,
	   IsNULL(LightsEnabled,false) AS LightsEnabled,
	   IsNULL(DemoAllOptions,false) AS DemoAllOptions,
	   IsNULL(DTMF_Installed,false) AS DTMF_Installed,
	   IsNULL(RRe_Enabled,false) AS RRe_Enabled, 
	   IsNULL(GInterface,false) AS GInterface,
	   -- CS3000 Options
	   ISNULL(PortA_DeviceIndex,0) PortA_DeviceIndex,
       ISNULL(PortB_DeviceIndex,0) PortB_DeviceIndex,
	   ISNULL(DashM_Card AND DashM_Terminal,false) DashMOption,
	   ISNULL(WeatherCard AND WeatherTerminal,false) WeatherOption,
	   ISNULL(TwoWireTerminal,false) TwoWireOption,
	   ISNULL(FLOption,false) FLOption,
	   ISNULL(SSEOption,false) SSEOption,
	   ISNULL(SSE_DOption,false) SSE_DOption,
	   ISNULL(StationOption,0) StationOption,
	   ISNULL(POCoption,false) POCOption,
	   ISNULL(LightOption,false) LightOption,
	   cs.SerialNumber
FROM ControllerSites s 
JOIN Companies p on s.companyID = p.CompanyID 
LEFT JOIN Controllers c ON s.SiteID =c.SiteID AND c.Deleted = false 
LEFT OUTER JOIN cmos m ON c.controllerid=m.controllerid
LEFT OUTER JOIN CS3000Controllers cs ON cs.ControllerID = c.ControllerID
LEFT OUTER JOIN 
		   		(
				  SELECT NetworkID,BoxIndex, 
				  SUM(
				   		 (CASE WHEN Slot = 0 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 1 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 2 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 3 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 4 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 5 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) )
						  AS StationOption
						  ,MAX(IIF(Slot=80 AND CardPresent=true AND TBPresent=True,true,false)) AS POCoption 
						  ,MAX(IIF(Slot=90 AND CardPresent=true AND TBPresent=True,true,false)) AS LightOption
				   FROM CS3000Cards
				   GROUP BY NetworkID,BoxIndex
				) cso ON cs.NetworkID = cso.NetworkID AND cs.BoxIndex = cso.BoxIndex
--LEFT OUTER JOIN NewGprsExpirationDatesView x ON c.PhoneNumber = x.PhoneNumber 
WHERE c.Deleted = false AND s.Deleted = false        
--ORDER BY s.siteName, C.ControllerName;






CREATE VIEW 
   ControllersSetupReport
AS 
   SELECT s.CompanyID, ISNULL(p.Name,'[ UNALLOCATED ]') Name, s.SiteName,s.SiteID,C.ControllerName,C.ControllerID, IIF(ISNULL(c.Model,0)=7,nt.LastCommunicated, c.LastCommunicated) LastCommunicated,
	   ISNULL(c.Model,0) Model, ControllerSerialNumber ,c.SoftwareVersion,m.Version,
	   x.ExpirationDate, x.Warning1Date, x.Warning2Date, c.Address,IFNULL(cs.BoxIndex,0) BoxIndex,c.PhoneNumber,CT.CommunicationsType,c.Communicate,MasterSerialNumber,
	   -- ET2000e Options
	   ISNULL(c.StationsInUse,0) StationsInUse,
	   IsNULL(CommTypeA,0) CommTypeA,
	   IsNULL(CommTypeB,0) CommTypeB, 
	   IsNULL(GInterface,false) GInterface,
	   IsNULL(RBInterface,false) RBInterface,
	   IsNULL(FInterface,false) FInterface, 
	   IsNULL(WGInterface,false) AS WGInterface,
	   IsNULL(LightsEnabled,false) AS LightsEnabled,
	   IsNULL(DemoAllOptions,false) AS DemoAllOptions,
	   IsNULL(DTMF_Installed,false) AS DTMF_Installed,
	   IsNULL(RRe_Enabled,false) AS RRe_Enabled, 
	   IsNULL(GInterface,false) AS GInterface,
	   -- CS3000 Options
	   ISNULL(PortA_DeviceIndex,0) PortA_DeviceIndex,
       ISNULL(PortB_DeviceIndex,0) PortB_DeviceIndex,
	   ISNULL(DashM_Card AND DashM_Terminal,false) DashMOption,
	   ISNULL(WeatherCard AND WeatherTerminal,false) WeatherOption,
	   ISNULL(TwoWireTerminal,false) TwoWireOption,
	   ISNULL(FLOption,false) FLOption,
	   ISNULL(SSEOption,false) SSEOption,
	   ISNULL(SSE_DOption,false) SSE_DOption,
	   ISNULL(StationOption,0) StationOption,
	   ISNULL(POCoption,false) POCOption,
	   ISNULL(LightOption,false) LightOption,
        cs.SerialNumber
	   
FROM ControllerSites s 
LEFT OUTER JOIN Companies p on s.companyID = p.CompanyID 
LEFT JOIN Controllers c ON s.SiteID =c.SiteID AND c.Deleted = false 
LEFT OUTER JOIN CommTypes CT on C.ConnectionType=CT.CTIndex
LEFT OUTER JOIN cmos m ON c.controllerid=m.controllerid
LEFT OUTER JOIN CS3000Controllers cs ON cs.ControllerID = c.ControllerID
LEFT OUTER JOIN CS3000Networks nt ON cs.NetworkID = nt.NetworkID
LEFT OUTER JOIN 
		   		(
				  SELECT NetworkID,BoxIndex,
				  SUM(
				   		 (CASE WHEN Slot = 0 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 1 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 2 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 3 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 4 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) +
						 (CASE WHEN Slot = 5 AND CardPresent=true AND TBPresent=True THEN 8 ELSE 0 END) )
						  AS StationOption
						  ,MAX(IIF(Slot=80 AND CardPresent=true AND TBPresent=True,true,false)) AS POCoption 
						  ,MAX(IIF(Slot=90 AND CardPresent=true AND TBPresent=True,true,false)) AS LightOption
				   FROM CS3000Cards
				   GROUP BY NetworkID,BoxIndex
				) cso ON cs.NetworkID = cso.NetworkID AND cs.BoxIndex = cso.BoxIndex
LEFT OUTER JOIN NewGprsExpirationDatesView x ON c.PhoneNumber = x.PhoneNumber 
WHERE c.Deleted = false AND s.Deleted = false
--ORDER BY s.siteName, C.ControllerName;









CREATE FUNCTION SplitColumns 
   ( 
   @Columns VARCHAR ( 5000 )
   )
   RETURNS VARCHAR ( 5000 )
BEGIN

    DECLARE @Pos    Integer;
    DECLARE @OldPos Integer;
	DECLARE @Query VARCHAR (5000);
	DECLARE @Field VARCHAR (30);
	
	SET @Query = '';
	
	IF RIGHT(@Columns, 1) <> ',' THEN
	  @Columns = @Columns + ',';
  	END IF;
	   
    SET  @Pos    = 1;
         @OldPos = 1;
		 
 	WHILE @Pos < LENGTH(@Columns) DO
       SET @Pos = LOCATE(',', @Columns, @OldPos);
	   @Field = LTRIM(RTRIM(SUBSTRING(@Columns, @OldPos, @Pos - @OldPos)));
	   @Query = @Query + @Field + '=t.' + @Field + ',';
	   SET @OldPos = @Pos + 1;
    END WHILE;
	
	IF RIGHT(@Query, 1) = ',' THEN
	  @Query = SUBSTRING(@Query,1 , LENGTH(@Query)-1);
  	END IF;
	
	RETURN @Query;



END;
CREATE PROCEDURE DeletePData
   ( 
      nid Integer
   ) 
BEGIN 
DECLARE @nid INTEGER;

SET @nid  = (SELECT nid FROM __input);

-- delete PData
DELETE FROM CS3000Systems WHERE NetworkID=@nid;
DELETE FROM CS3000ManualPrograms WHERE NetworkID=@nid;
DELETE FROM CS3000StationGroups WHERE NetworkID=@nid;
DELETE FROM CS3000POC WHERE NetworkID=@nid;
DELETE FROM CS3000Lights WHERE NetworkID=@nid;
DELETE FROM CS3000Stations WHERE NetworkID=@nid;
DELETE FROM CS3000Weather WHERE NetworkID=@nid;
DELETE FROM CS3000Lights WHERE NetworkID=@nid;

-- delete temp PData
DELETE FROM Temp_CS3000Networks WHERE NetworkID=@nid AND UserId<>-1; // -1 swapping user
DELETE FROM Temp_CS3000Controllers WHERE NetworkID=@nid AND UserId<>-1;
DELETE FROM Temp_CS3000Systems WHERE NetworkID=@nid AND UserId<>-1;
DELETE FROM Temp_CS3000ManualPrograms WHERE NetworkID=@nid AND UserId<>-1;
DELETE FROM Temp_CS3000StationGroups WHERE NetworkID=@nid AND UserId<>-1;
DELETE FROM Temp_CS3000POC WHERE NetworkID=@nid AND UserId<>-1;
DELETE FROM Temp_CS3000Lights WHERE NetworkID=@nid AND UserId<>-1;
DELETE FROM Temp_CS3000Stations WHERE NetworkID=@nid AND UserId<>-1;
DELETE FROM Temp_CS3000Weather WHERE NetworkID=@nid AND UserId<>-1;
DELETE FROM Temp_CS3000Lights WHERE NetworkID=@nid AND UserId<>-1;





END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'DeletePData', 
   'COMMENT', 
   '');

CREATE PROCEDURE PanelSwapping
   ( 
      UserId Integer,
      OldNetworkID Integer,
      NewNetworkID Integer,
      NewSN Integer,
      OldSN Integer,
      Status Integer OUTPUT
   ) 
BEGIN 
DECLARE @OldNetworkID INTEGER;
DECLARE @NewNetworkID INTEGER;
DECLARE @OldSN INTEGER;
DECLARE @NewSN INTEGER;
DECLARE @UserId INTEGER;
DECLARE @status INTEGER;

SET @UserId  = (SELECT UserId FROM __input);
SET @OldNetworkID  = (SELECT OldNetworkID FROM __input);
SET @NewNetworkID  = (SELECT NewNetworkID FROM __input);
SET @OldSN  = (SELECT OldSN FROM __input);
SET @NewSN  = (SELECT NewSN FROM __input);

SET @status=1;

BEGIN TRAN;

TRY

-- Swap Serial Number
UPDATE CS3000Networks SET MasterSerialNumber=@NewSN WHERE NetworkID=@OldNetworkID;
UPDATE CS3000Networks SET MasterSerialNumber=@OldSN WHERE NetworkID=@NewNetworkID;

-- mark old site name as inactive
UPDATE ControllerSites SET SiteName= IIF(CONTAINS(SiteName,' -Inactive'), SiteName,CONCAT(TRIM(SiteName),' -Inactive')) WHERE SiteID=@NewNetworkID;

-- make sure we don't have any program data ready to be sent to the controller
UPDATE Temp_CS3000Networks SET Status=0 WHERE NetworkID=@OldNetworkID;

-- Prepare Temp tables
--IF NOT EXISTS(SELECT 1 FROM Temp_CS3000Networks WHERE UserId=@UserId AND NetworkID=@OldNetworkID) THEN
--EXECUTE PROCEDURE PrepareCS3000ProgramData (@UserId,@OldNetworkId);
--ENDIF;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   ROLLBACK;
END TRY;

IF @Status=1 THEN 
	 COMMIT WORK;
ENDIF;

INSERT INTO __output SELECT @status FROM system.iota;









END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'PanelSwapping', 
   'COMMENT', 
   '');

CREATE PROCEDURE ArchiveActivityLog
   ( 
      DateArchive TIMESTAMP,
      status Integer OUTPUT,
      iCount Integer OUTPUT
   ) 
BEGIN 

DECLARE @DateArchive TimeStamp;
DECLARE @status INTEGER;
DECLARE @iCount INTEGER;

@DateArchive  = (SELECT DateArchive FROM __input);

SET @status=1;  

BEGIN TRAN;

@iCount = (SELECT COUNT(*) FROM ActivityLog WHERE ActivityTimeStamp<@DateArchive);

TRY
	  INSERT INTO Archive.ActivityLog SELECT * FROM ActivityLog WHERE ActivityTimeStamp<@DateArchive;
      DELETE FROM ActivityLog WHERE ActivityTimeStamp<@DateArchive;
CATCH ADS_SCRIPT_EXCEPTION
 	  @status=0;
	  ROLLBACK;
END TRY;

IF (@status=1) THEN
   COMMIT WORK;
ENDIF;

INSERT INTO __output SELECT @status,@iCount FROM system.iota;











END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'ArchiveActivityLog', 
   'COMMENT', 
   '');

CREATE PROCEDURE ArchiveAlerts
   ( 
      DateArchive TIMESTAMP,
      status Integer OUTPUT,
      iCount Integer OUTPUT
   ) 
BEGIN 

DECLARE @DateArchive TimeStamp;
DECLARE @status INTEGER;
DECLARE @iCount INTEGER;

@DateArchive  = (SELECT DateArchive FROM __input);

SET @status=1; 

BEGIN TRAN;

    @iCount = (SELECT COUNT(*) FROM Alerts WHERE AlertTimeStamp<@DateArchive);
	
	TRY
	   INSERT INTO Archive.Alerts SELECT * FROM Alerts WHERE AlertTimeStamp<@DateArchive;
       DELETE FROM Alerts WHERE AlertTimeStamp<@DateArchive;
	CATCH ADS_SCRIPT_EXCEPTION
 	  @status=0;
	  ROLLBACK;
	END TRY;

IF (@status=1) THEN
   COMMIT WORK;
ENDIF;

INSERT INTO __output SELECT @status,@iCount FROM system.iota;






END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'ArchiveAlerts', 
   'COMMENT', 
   '');

CREATE PROCEDURE ArchiveHistory
   ( 
      DateArchive TIMESTAMP,
      status Integer OUTPUT,
      iCount Integer OUTPUT
   ) 
BEGIN 

DECLARE @DateArchive TimeStamp;
DECLARE @status INTEGER;
DECLARE @iCount INTEGER;

@DateArchive  = (SELECT DateArchive FROM __input);

SET @status=1; 

BEGIN TRAN;

    @iCount = (SELECT COUNT(*) FROM History WHERE HistoryTimeStamp<@DateArchive);
	
	TRY
	   INSERT INTO Archive.History SELECT * FROM History WHERE HistoryTimeStamp<@DateArchive;
       DELETE FROM History WHERE HistoryTimeStamp<@DateArchive;
	CATCH ADS_SCRIPT_EXCEPTION
 	  @status=0;
	  ROLLBACK;
	END TRY;

IF (@status=1) THEN
   COMMIT WORK;
ENDIF;

INSERT INTO __output SELECT @status,@iCount FROM system.iota;



END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'ArchiveHistory', 
   'COMMENT', 
   '');

CREATE PROCEDURE ArchiveGeneralHistory
   ( 
      DateArchive TIMESTAMP,
      status Integer OUTPUT,
      iCount Integer OUTPUT
   ) 
BEGIN 

DECLARE @DateArchive TimeStamp;
DECLARE @status INTEGER;
DECLARE @iCount INTEGER;

@DateArchive  = (SELECT DateArchive FROM __input);

SET @status=1;

BEGIN TRAN;

@iCount = (SELECT COUNT(*) FROM GeneralHistory WHERE HistoryTimeStamp<@DateArchive);

TRY
	   INSERT INTO Archive.GeneralHistory SELECT * FROM GeneralHistory WHERE HistoryTimeStamp<@DateArchive;
       DELETE FROM GeneralHistory WHERE HistoryTimeStamp<@DateArchive;
CATCH ADS_SCRIPT_EXCEPTION
 	  @status=0;
	  ROLLBACK;
END TRY;

IF (@status=1) THEN
   COMMIT WORK;
ENDIF;

INSERT INTO __output SELECT @status,@iCount FROM system.iota;






END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'ArchiveGeneralHistory', 
   'COMMENT', 
   '');

CREATE PROCEDURE DeleteTempCS3000ProgramData
   ( 
      UserId Integer,
      NetworkId Integer
   ) 
BEGIN 

DECLARE @UserId INTEGER;
DECLARE @NetworkId INTEGER;

SET @UserId  = (SELECT UserId FROM __input);
SET @NetworkId  = (SELECT NetworkId FROM __input);

TRY

DELETE FROM Temp_CS3000Networks WHERE NetworkID=@NetworkId AND UserId = @UserId;
DELETE FROM Temp_CS3000Systems WHERE NetworkID=@NetworkId AND UserId = @UserId;
DELETE FROM Temp_CS3000Controllers WHERE NetworkID=@NetworkId AND UserId = @UserId;
DELETE FROM Temp_CS3000ManualPrograms WHERE NetworkID=@NetworkId AND UserId = @UserId;
DELETE FROM Temp_CS3000POC WHERE NetworkID=@NetworkId AND UserId = @UserId;
DELETE FROM Temp_CS3000Stations WHERE NetworkID=@NetworkId AND UserId = @UserId;
DELETE FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkId AND UserId = @UserId; 
DELETE FROM Temp_CS3000Weather WHERE NetworkID=@NetworkId AND UserId = @UserId;
DELETE FROM Temp_CS3000Lights WHERE NetworkID=@NetworkId AND UserId = @UserId;

CATCH ADS_SCRIPT_EXCEPTION
 ROLLBACK;
END TRY;





END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'DeleteTempCS3000ProgramData', 
   'COMMENT', 
   '');

CREATE PROCEDURE DeleteUnusedCS3000ProgramData
   ( 
   ) 
BEGIN 

DECLARE nt CURSOR AS SELECT NetworkID,UserId FROM Temp_CS3000Networks WHERE Status<>2 AND CreatedDate<TIMESTAMPADD( SQL_TSI_DAY,-1, CURRENT_TIMESTAMP() );

OPEN nt;

WHILE FETCH nt DO

EXECUTE PROCEDURE DeleteTempCS3000ProgramData(nt.UserId,nt.NetworkId);

END WHILE;

CLOSE nt;









END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'DeleteUnusedCS3000ProgramData', 
   'COMMENT', 
   '');

CREATE PROCEDURE locate_network
   ( 
      networkid Integer,
      serialnumber Integer,
      companyid Integer,
      status Integer OUTPUT
   ) 
BEGIN 
DECLARE @networkid  	 INTEGER;
DECLARE @serialnumber 	 INTEGER;
DECLARE @companyid 	 	 INTEGER;
DECLARE @status 		 INTEGER;

-- Get values from input
@networkid  = 	  (SELECT networkid 	  FROM __input);
@serialnumber  =  (SELECT serialnumber 	  FROM __input);
@companyid  =  	  (SELECT companyid 	  FROM __input);

	 SET @status=0;
	 
	 IF EXISTS( SELECT 1 FROM CS3000Networks n 
	 		    JOIN CS3000Controllers c ON n.NetworkId=c.NetworkId
				JOIN ControllerSites s ON n.NetworkID = s.SiteID
				WHERE n.NetworkID=@networkid
				AND c.SerialNumber=@serialnumber
				AND s.CompanyID=-1)
	 THEN

	 	 		BEGIN TRAN;
       TRY
				UPDATE CS3000Networks  SET CompanyID=@companyid WHERE NetworkID=@networkid AND CompanyID=-1;				
				UPDATE ControllerSites SET CompanyID=@companyid WHERE SiteID=@networkid AND CompanyID=-1;
                UPDATE Controllers     SET CompanyID=@companyid WHERE SiteID=@networkid AND CompanyID=-1;
				SET @status = 1;
				COMMIT WORK;
       CATCH ALL
                ROLLBACK;
       END TRY;
	 ENDIF;
	 
	 INSERT INTO __output SELECT @status FROM system.iota;





END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'locate_network', 
   'COMMENT', 
   '');

CREATE PROCEDURE PrepareCS3000ProgramData
   ( 
      UserId Integer,
      NetworkId Integer,
      status Integer OUTPUT,
      error VARCHAR ( 4000 ) OUTPUT
   ) 
BEGIN 

DECLARE @UserId INTEGER;
DECLARE @NetworkId INTEGER;
DECLARE @status INTEGER;
DECLARE @error String;

DECLARE @BoxIndex INTEGER;
DECLARE @BoxesLength INTEGER;

SET @UserId  = (SELECT UserId FROM __input);
SET @NetworkId  = (SELECT NetworkId FROM __input);

SET @status=1;

@BoxIndex = 0;

TRY
   
   -- Delete old records
   DELETE FROM Temp_CS3000Networks WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Systems WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Controllers WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000ManualPrograms WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000POC WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Stations WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkId AND UserId = @UserId; 
   DELETE FROM Temp_CS3000Weather WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Lights WHERE NetworkID=@NetworkId AND UserId = @UserId;

    BEGIN TRAN;   

   -- Network table
   INSERT INTO Temp_CS3000Networks SELECT c.*,@UserId UserId,0 EventType,'' Columns,now() CreatedDate,0 Status,0 NotifyNewDataArrives FROM CS3000Networks c WHERE c.NetworkID=@NetworkId;
   -- copy controllers names into CS3000Network table
   @BoxesLength = (SELECT COUNT(ControllerID) FROM CS3000Controllers WHERE NetworkID=@NetworkID);
   WHILE @BoxIndex<@BoxesLength DO
   		 EXECUTE IMMEDIATE 'UPDATE TEMP_CS3000Networks SET BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) + 
		 ' = (SELECT Name FROM CS3000Controllers WHERE BoxIndex=' + CAST (@BoxIndex AS SQL_VARCHAR) + 
		 ' AND NetworkID=' + CAST (@NetworkId AS SQL_VARCHAR) + 
		 ') WHERE NetworkID = ' + CAST (@NetworkId AS SQL_VARCHAR) + 
		 ' AND UserID=' + CAST (@UserId AS SQL_VARCHAR);
   		 @BoxIndex = @BoxIndex + 1;
   END WHILE;
   
   -- System table
   INSERT INTO Temp_CS3000Systems SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000Systems c WHERE c.NetworkID=@NetworkId;

   -- Controller table
   INSERT INTO Temp_CS3000Controllers SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000Controllers c WHERE c.NetworkID=@NetworkId;  

   -- ManualPrograms table
   INSERT INTO Temp_CS3000ManualPrograms SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000ManualPrograms c WHERE c.NetworkID=@NetworkId;

   -- POC table
   INSERT INTO Temp_CS3000POC SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000POC c WHERE c.NetworkID=@NetworkId;

   -- Station table
   INSERT INTO Temp_CS3000Stations SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000Stations c WHERE c.NetworkID=@NetworkId; --AND CardConnected=true;

   -- Group Station table
   INSERT INTO Temp_CS3000StationGroups SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000StationGroups c WHERE c.NetworkID=@NetworkId;

   -- Weather table
   INSERT INTO Temp_CS3000Weather SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000Weather c WHERE c.NetworkID=@NetworkId;

   -- Lights table
   INSERT INTO Temp_CS3000Lights SELECT c.*,@UserId UserId,0 EventType,'' Columns FROM CS3000Lights c WHERE c.NetworkID=@NetworkId;

   IF ((SELECT COUNT(*) FROM Temp_CS3000Weather WHERE NetworkID=@NetworkId AND UserId = @UserId)=0 OR
       (SELECT COUNT(*) FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkId AND UserId = @UserId)=0 OR
       (SELECT COUNT(*) FROM Temp_CS3000Stations WHERE NetworkID=@NetworkId)=0 OR
       (SELECT COUNT(*) FROM Temp_CS3000Systems WHERE NetworkID=@NetworkId AND UserId = @UserId)=0 OR
       (SELECT COUNT(*) FROM Temp_CS3000POC WHERE NetworkID=@NetworkId AND UserId = @UserId)=0 OR
       (SELECT COUNT(*) FROM Temp_CS3000Controllers WHERE NetworkID=@NetworkId AND UserId = @UserId)=0 OR
       (SELECT COUNT(*) FROM Temp_CS3000Lights WHERE NetworkID=@NetworkId AND UserId = @UserId)=0 OR
       (SELECT COUNT(*) FROM Temp_CS3000ManualPrograms WHERE NetworkID=@NetworkId AND UserId = @UserId)=0) THEN
     SET @status=0;
     SET @error = 'No records found';
   ENDIF;
    
   -- End
CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   SET @error = __errtext;
   ROLLBACK;
END TRY;

IF @Status=1 THEN 
	 COMMIT WORK;
ENDIF;

INSERT INTO __output SELECT @status,@error FROM system.iota;































END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'PrepareCS3000ProgramData', 
   'COMMENT', 
   '');

CREATE PROCEDURE UpdateCS3000ProgramData
   ( 
      UserId Integer,
      NetworkId Integer,
      status Integer OUTPUT,
      error VARCHAR ( 4000 ) OUTPUT
   ) 
BEGIN 

--
-- STORED PROCEDURE
--     UpdateCS3000ProgramData
--
-- DESCRIPTION
--     Update CS3000 original program data tables from the temporary tables.
--
-- PARAMETERS
--     :UserId
--         The user id we are pulling his records from the temporary tables.
--	   :NetworkId 
--	   	   The network id we are updating. 
-- 
-- RETURN VALUE
--     Status
--	   		 0 - failed, 1 - succeeded.
--     Error
--	   		The exception message if the Status was 0.
-- 

DECLARE @TaskId INTEGER;
DECLARE @UserId INTEGER;
DECLARE @NetworkId INTEGER;
DECLARE @status INTEGER;
DECLARE @error String; 
DECLARE @Columns String;
DECLARE sqlStmt String;
DECLARE UserIdStr VARCHAR(20);
DECLARE NetworkIDStr VARCHAR (20); 
DECLARE NewGID INTEGER;
DECLARE @BoxIndex INTEGER;
DECLARE @BoxesLength INTEGER;

DECLARE nt  CURSOR AS SELECT * FROM Temp_CS3000Networks WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType = 1;
DECLARE sys CURSOR AS SELECT * FROM Temp_CS3000Systems WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType in (1,2) ORDER BY SystemID;
DECLARE mp  CURSOR AS SELECT * FROM Temp_CS3000ManualPrograms WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType in (1,2) ORDER BY ProgramID;
DECLARE poc CURSOR AS SELECT * FROM Temp_CS3000POC WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType = 1;
DECLARE wt  CURSOR AS SELECT * FROM Temp_CS3000Weather WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType = 1;
DECLARE gp  CURSOR AS SELECT * FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType in (1,2) ORDER BY StationGroupID;
DECLARE st  CURSOR AS SELECT * FROM Temp_CS3000Stations WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType = 1;
DECLARE lg  CURSOR AS SELECT * FROM Temp_CS3000Lights WHERE NetworkID=@NetworkID AND UserID = @UserID AND EventType = 1;

SET @UserId  = (SELECT UserId FROM __input);
SET @NetworkId  = (SELECT NetworkId FROM __input);
SET UserIdStr = CAST (@UserID AS SQL_VARCHAR);
SET NetworkIDStr = CAST (@NetworkId AS SQL_VARCHAR);

SET @status=1;
SET @error='';

-- sanity check that we have records in the temporary tables for the user & network
IF ((SELECT COUNT(*) FROM Temp_CS3000Weather WHERE NetworkID=@NetworkId AND UserId = @UserId)=0 OR
    (SELECT COUNT(*) FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkId AND UserId = @UserId)=0) THEN
     SET @status=0;
     SET @error = 'No records found';
ENDIF;

   
--------------------------------------------------------
--                      Network Table 				  --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN nt;

WHILE FETCH nt DO

	  @Columns = (LTRIM(RTRIM(nt.Columns)));
	  
	  sqlStmt = 'UPDATE CS3000Networks SET ' +
		  (SELECT SplitColumns( @Columns ) FROM system.iota) +
		  ' FROM Temp_CS3000Networks t ' +
		  ' INNER JOIN CS3000Networks p ON p.NetworkID=t.NetworkID ' +  
		  'WHERE t.NetworkID=' + NetworkIDStr + ' AND t.UserID=' + UserIdStr;
	  
	EXECUTE IMMEDIATE SQlStmt;
      
   -- Update BoxName(s) into CS3000Controllers & ControllerName
   @BoxIndex = 0;
   @BoxesLength = 12;
   WHILE @BoxIndex<@BoxesLength DO
   		 SET NewGID = (SELECT ControllerID FROM CS3000Controllers WHERE NetworkID = @NetworkID AND BoxIndex =@BoxIndex);
   		 IF NewGID IS NOT NULL THEN
		 EXECUTE IMMEDIATE 'UPDATE CS3000Controllers SET Name =' + 
		 		   		   ' ISNULL((SELECT BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) + 
		 		   		   ' FROM Temp_CS3000Networks WHERE NetworkID = ' + NetworkIDStr + 
		 				   ' AND UserID=' + UserIdStr + '),'''')' +
						   ' WHERE ControllerID=' + CAST (NewGID AS SQL_VARCHAR);
						   
		 EXECUTE IMMEDIATE 'UPDATE Controllers SET ControllerName =' + 
		 		   		   ' ISNULL((SELECT BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) + 
		 		   		   ' FROM Temp_CS3000Networks WHERE NetworkID = ' + NetworkIDStr + 
		 				   ' AND UserID=' + UserIdStr + '),'''')' +
						   ' WHERE ControllerID=' + CAST (NewGID AS SQL_VARCHAR);
		  ENDIF;				
   		 @BoxIndex = @BoxIndex + 1;
   END WHILE;
   
END WHILE;   

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
   RAISE;
FINALLY
   CLOSE nt;
END TRY;

ENDIF;

--------------------------------------------------------
--            			System Table 				  --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN sys;

WHILE FETCH sys DO

	  @Columns = (LTRIM(RTRIM(sys.Columns)));

	  IF sys.EventType = 1 THEN -- Update
	  
	  sqlStmt = 'UPDATE CS3000Systems SET ' +
		  (SELECT SplitColumns( @Columns ) FROM system.iota) +
		  ' FROM Temp_CS3000Systems t ' +
		  ' INNER JOIN CS3000Systems p ON p.SystemID = t.SystemID AND p.NetworkID=t.NetworkID ' +  
		  'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.SystemID = ' + CAST (sys.SystemID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
	  
	  EXECUTE IMMEDIATE SQlStmt;
	  
	  ELSE -- Insert
	  
	  NewGID = (SELECT MAX(SystemGID)+1 FROM CS3000Systems WHERE NetworkID = @NetworkID);
	  sqlStmt = 'INSERT INTO CS3000Systems ' + 
	  		    '(SystemGID,' + @Columns + ') SELECT ' +
				CAST (NewGID AS SQL_VARCHAR) + ',' + 
				@Columns +
				' FROM Temp_CS3000Systems WHERE NetworkID=' + NetworkIDStr + ' AND SystemID = ' + CAST (sys.SystemID AS SQL_VARCHAR) + ' AND UserID=' + UserIdStr;
	
	  EXECUTE IMMEDIATE SQlStmt;
	 
	  -- Update the new GID in both CS3000POC & CS3000Stations tables.
	  UPDATE Temp_CS3000POC SET SystemGID = NewGID WHERE SystemGID = sys.SystemGID AND NetworkID = @NetworkID AND UserID=@UserID;
	  
	  UPDATE Temp_CS3000StationGroups SET SystemID = NewGID WHERE SystemID = sys.SystemGID AND NetworkID = @NetworkID AND UserID=@UserID;
	  
	  ENDIF;
	  
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
   RAISE;
FINALLY
   CLOSE sys;
END TRY;

ENDIF;

--------------------------------------------------------
--            Manual Programs Table 				  --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN mp;

WHILE FETCH mp DO

	  @Columns = (LTRIM(RTRIM(mp.Columns)));

	  IF mp.EventType = 1 THEN -- Update
	  
	  sqlStmt = 'UPDATE CS3000ManualPrograms SET ' +
		  (SELECT SplitColumns( @Columns ) FROM system.iota) +
		  ' FROM Temp_CS3000ManualPrograms t ' +
		  ' INNER JOIN CS3000ManualPrograms p ON p.ProgramID = t.ProgramID AND p.NetworkID=t.NetworkID ' +  
		  'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.ProgramID = ' + CAST (mp.ProgramID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
	  
	  EXECUTE IMMEDIATE SQlStmt;
	  
	  ELSE -- Insert
	  
	  NewGID = (SELECT MAX(ProgramGID)+1 FROM CS3000ManualPrograms WHERE NetworkID = @NetworkID);
	  sqlStmt = 'INSERT INTO CS3000ManualPrograms ' + 
	  		    '(ProgramGID,' + @Columns + ') SELECT ' +
				CAST (NewGID AS SQL_VARCHAR) + ',' + 
				@Columns +
				' FROM Temp_CS3000ManualPrograms WHERE NetworkID=' + NetworkIDStr + ' AND ProgramID = ' + CAST (mp.ProgramID AS SQL_VARCHAR) + ' AND UserID=' + UserIdStr;
	  
	  EXECUTE IMMEDIATE SQlStmt;
	  
	  -- Update Manual Program GID in Stations table
	  UPDATE Temp_CS3000Stations SET ManualProgramA_ID = NewGID WHERE ManualProgramA_ID = mp.ProgramGID AND NetworkID = @NetworkID AND UserID=@UserID;
	  
	  UPDATE Temp_CS3000Stations SET ManualProgramB_ID = NewGID WHERE ManualProgramB_ID = mp.ProgramGID AND NetworkID = @NetworkID AND UserID=@UserID;
	   
	  ENDIF;
	  
	  
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
   RAISE;
FINALLY
   CLOSE mp;
END TRY;

ENDIF;

--------------------------------------------------------
--                        POC Table 				  --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN poc;

WHILE FETCH poc DO

	  @Columns = (LTRIM(RTRIM(poc.Columns)));
	  
	  sqlStmt = 'UPDATE CS3000POC SET ' +
		  (SELECT SplitColumns( @Columns ) FROM system.iota) +
		  ' FROM Temp_CS3000POC t ' +
		  ' INNER JOIN CS3000POC p ON p.POC_ID = t.POC_ID AND p.NetworkID=t.NetworkID ' +  
		  'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.POC_ID = ' + CAST (poc.POC_ID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
	  
	  EXECUTE IMMEDIATE SQlStmt;
	  
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
   RAISE;
FINALLY
   CLOSE poc;
END TRY;

ENDIF;

--------------------------------------------------------
--                      Weather Table 				  --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN wt;

WHILE FETCH wt DO

	  @Columns = (LTRIM(RTRIM(wt.Columns)));
	  
	  sqlStmt = 'UPDATE CS3000Weather SET ' +
		  (SELECT SplitColumns( @Columns ) FROM system.iota) +
		  ' FROM Temp_CS3000Weather t ' +
		  ' INNER JOIN CS3000Weather p ON p.NetworkID=t.NetworkID ' +  
		  'WHERE t.NetworkID=' + NetworkIDStr + ' AND t.UserID=' + UserIdStr;
	  
	  EXECUTE IMMEDIATE SQlStmt;
	  
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
   RAISE;
FINALLY
   CLOSE wt;
END TRY;

ENDIF;

--------------------------------------------------------
--            	Station Groups Table 				  --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN gp;

WHILE FETCH gp DO

	  @Columns = (LTRIM(RTRIM(gp.Columns)));

	  IF gp.EventType = 1 THEN -- Update
	  
	  sqlStmt = 'UPDATE CS3000StationGroups SET ' +
		  (SELECT SplitColumns( @Columns ) FROM system.iota) +
		  ' FROM Temp_CS3000StationGroups t ' +
		  ' INNER JOIN CS3000StationGroups p ON p.StationGroupID = t.StationGroupID AND p.NetworkID=t.NetworkID ' +  
		  'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.StationGroupID = ' + CAST (gp.StationGroupID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
	  
	  EXECUTE IMMEDIATE SQlStmt;
	  
	  ELSE -- Insert
	  
	  NewGID = (SELECT MAX(StationGroupGID)+1 FROM CS3000StationGroups WHERE NetworkID = @NetworkID);
	  sqlStmt = 'INSERT INTO CS3000StationGroups ' + 
	  		    '(StationGroupGID,' + @Columns + ') SELECT ' +
				CAST (NewGID AS SQL_VARCHAR) + ',' + 
				@Columns +
				' FROM Temp_CS3000StationGroups WHERE NetworkID=' + NetworkIDStr + ' AND StationGroupGID = ' + CAST (gp.StationGroupGID AS SQL_VARCHAR) + ' AND UserID=' + UserIdStr;
	  
	  EXECUTE IMMEDIATE SQlStmt;
	  
	  -- Update Station Group GID in Stations table
	  UPDATE Temp_CS3000Stations SET StationGroupID = NewGID WHERE StationGroupID = gp.StationGroupGID AND NetworkID = @NetworkID AND UserID=@UserID;
	   
	  ENDIF;
	  
	  
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
   RAISE;
FINALLY
   CLOSE gp;
END TRY;

ENDIF;

--------------------------------------------------------
--                      Stations Table 				  --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN st;

WHILE FETCH st DO

	  @Columns = (LTRIM(RTRIM(st.Columns)));
	  
	  sqlStmt = 'UPDATE CS3000Stations SET ' +
		  (SELECT SplitColumns( @Columns ) FROM system.iota) +
		  ' FROM Temp_CS3000Stations t ' +
		  ' INNER JOIN CS3000Stations p ON p.StationID = t.StationID AND p.NetworkID=t.NetworkID ' +  
		  'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.StationID = ' + CAST (st.StationID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
	  
	  EXECUTE IMMEDIATE SQlStmt;
	  
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
   RAISE;
FINALLY
   CLOSE st;
END TRY;

ENDIF;


--------------------------------------------------------
--                      Lights Table 				  --
--------------------------------------------------------
IF @Status=1 THEN

TRY

OPEN lg;

WHILE FETCH lg DO

	  @Columns = (LTRIM(RTRIM(lg.Columns)));
	  
	  sqlStmt = 'UPDATE CS3000Lights SET ' +
		  (SELECT SplitColumns( @Columns ) FROM system.iota) +
		  ' FROM Temp_CS3000Lights t ' +
		  ' INNER JOIN CS3000Lights p ON p.LightID = t.LightID AND p.NetworkID=t.NetworkID ' +  
		  'WHERE t.NetworkID=' + NetworkIDStr + ' AND p.LightID = ' + CAST (lg.LightID AS SQL_VARCHAR) + ' AND t.UserID=' + UserIdStr;
	  
	  EXECUTE IMMEDIATE SQlStmt;
	  
END WHILE;

CATCH ADS_SCRIPT_EXCEPTION
   SET @status=0;
   @error = __errtext;
   RAISE;
FINALLY
   CLOSE lg;
END TRY;

ENDIF;


IF @Status=1 THEN 

   -- Delete records
   DELETE FROM Temp_CS3000Networks WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Systems WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Controllers WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000ManualPrograms WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000POC WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Stations WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000StationGroups WHERE NetworkID=@NetworkId AND UserId = @UserId; 
   DELETE FROM Temp_CS3000Weather WHERE NetworkID=@NetworkId AND UserId = @UserId;
   DELETE FROM Temp_CS3000Lights WHERE NetworkID=@NetworkId AND UserId = @UserId;
   
   // Update NotifyNewDataArrives field in Temp_CS3000Networks to 2 for the given network id 
   // to notify all the users in temp tables that a new data has arrived
   UPDATE Temp_CS3000Networks SET NotifyNewDataArrives=2 WHERE NetworkID=@NetworkId;

   INSERT INTO TaskStatus (DateCreated, CreatedBy, ControllerID,ControllerName,TaskCommand,Status)
                   VALUES (CURRENT_TIMESTAMP(), @UserId, @NetworkId, 
				           (SELECT SiteName FROM ControllerSites WHERE SiteID=@NetworkId), 187,'Succeeded'); -- 187 SendCS3000ProgramDataChanges

ENDIF;

INSERT INTO __output SELECT @status,@error FROM system.iota;





































END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'UpdateCS3000ProgramData', 
   'COMMENT', 
   '');

CREATE PROCEDURE CompleteCS3000ProgramData
   ( 
      NetworkId Integer
   ) 
BEGIN 

DECLARE NewGID INTEGER;
DECLARE @BoxIndex INTEGER;
DECLARE @BoxesLength INTEGER;
DECLARE @NetworkId INTEGER;
DECLARE NetworkIDStr VARCHAR (20);

SET @NetworkId  = (SELECT NetworkId FROM __input);
   
TRY
   
   @BoxIndex = 0;
   SET NetworkIDStr = CAST (@NetworkId AS SQL_VARCHAR);

   @BoxesLength = 12;

   WHILE @BoxIndex<@BoxesLength DO
             SET NewGID = (SELECT ControllerID FROM CS3000Controllers WHERE NetworkID = @NetworkID AND BoxIndex =@BoxIndex);
             
			 IF NewGID IS NOT NULL THEN
                EXECUTE IMMEDIATE 'UPDATE CS3000Controllers SET Name =' + 
                                          ' ISNULL((SELECT BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) + 
                                          ' FROM CS3000Networks WHERE NetworkID = ' + NetworkIDStr + ' ),'''')' +
                                          ' WHERE ControllerID=' + CAST (NewGID AS SQL_VARCHAR);
                                       
                EXECUTE IMMEDIATE 'UPDATE Controllers SET ControllerName =' + 
                                         ' ISNULL((SELECT BoxName' + CAST (@BoxIndex+1 AS SQL_VARCHAR) + 
                                         ' FROM CS3000Networks WHERE NetworkID = ' + NetworkIDStr + '),'''')' +
                                         ' WHERE ControllerID=' + CAST (NewGID AS SQL_VARCHAR);
			ENDIF;		                                   
            
			@BoxIndex = @BoxIndex + 1;
			  
   END WHILE;
   
   // Update NotifyNewDataArrives field in Temp_CS3000Networks to 2 for the given network id 
   // to notify all the users in temp tables that a new data has arrived
   UPDATE Temp_CS3000Networks SET NotifyNewDataArrives=2 WHERE NetworkID=@NetworkID;
   
CATCH ADS_SCRIPT_EXCEPTION

END TRY;











END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'CompleteCS3000ProgramData', 
   'COMMENT', 
   '');

CREATE PROCEDURE Add_IHSFY_Command
   ( 
      Command_ID Integer,
      Network_id Integer,
      User_id Integer,
      param1 Integer,
      param2 Integer,
      param3 Integer,
      job_id Integer OUTPUT,
      status Integer OUTPUT
   ) 
BEGIN 
DECLARE @Command_ID  	 INTEGER;
DECLARE @Network_id 	 INTEGER;
DECLARE @User_id 	 	 INTEGER;
DECLARE @param1 	 	 INTEGER;
DECLARE @param2 	 	 INTEGER;
DECLARE @param3 	 	 INTEGER;
DECLARE @status 		 INTEGER;
DECLARE @job_id 		 INTEGER;

-- Get values from input
@Command_ID  = 	  (SELECT Command_ID 	  FROM __input);
@Network_id  =    (SELECT Network_id 	  FROM __input);
@User_id  =  	  (SELECT User_id 	  	  FROM __input);
@param1  =  	  (SELECT param1 	  	  FROM __input);
@param2  =  	  (SELECT param2 	  	  FROM __input);
@param3  =  	  (SELECT param3 	  	  FROM __input);

		SET @status=0;
        SET @job_id = 0;
	 
	BEGIN TRAN;
	
	TRY
	   INSERT INTO IHSFY_jobs(Command_ID, Network_id, User_id, Date_created, Status, param1, param2, param3)
	 		   		   VALUES (@Command_ID, @Network_id, @User_id, CURRENT_TIMESTAMP(), 0, @param1, @param2, @param3); // (0) new, needs to be processed
	
    SET @job_id = (SELECT LastAutoInc( STATEMENT ) FROM system.iota);
	
	  // For turn station on, MID_FROM_COMMSERVER_mobile_station_ON (100)
	  // Status => (4) Turn Station ON
      // param1 => box index
      // param2 => station number
      // param3 => for how long in seconds
	   IF @Command_ID=100 THEN 							 
	    	UPDATE CS3000Stations SET RR_Status=4,RR_SecondsLeft=@param3, RR_StartDateTime = CURRENT_TIMESTAMP()
			WHERE StationNumber=@param2 AND BoxIndex=@param1 AND NetworkID=@network_id;

      // For turn station off, MID_FROM_COMMSERVER_mobile_station_OFF (102)
	  // Status => (3) Turn Station OFF
      // param1 => box index
      // param2 => station number
	   ELSEIF @Command_ID=102 THEN 							 
	    	UPDATE CS3000Stations SET RR_Status=3
			WHERE StationNumber=@param2 AND BoxIndex=@param1 AND NetworkID=@network_id;
	   ENDIF;

     
	   
	   SET @status = 1;
	   COMMIT WORK;
	CATCH ALL
	   SET @status=0;
	   ROLLBACK;
	END TRY;

	 
	 INSERT INTO __output SELECT @status,@job_id FROM system.iota;

















END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'Add_IHSFY_Command', 
   'COMMENT', 
   '');

EXECUTE PROCEDURE 
  sp_CreateUser ( 
     'Calsense',
     ''/* YOUR PASSWORD GOES HERE */,
     '' );
EXECUTE PROCEDURE 
  sp_CreateGroup ( 
     'CC4 User',
     '' );
EXECUTE PROCEDURE 
   sp_AddUserToGroup 
   ( 'Calsense',
   'CC4 User' );

GRANT Inherit ON  [Companies]  TO  [Calsense];
GRANT Inherit ON  [Usrs]  TO  [Calsense];
GRANT Inherit ON  [CS3000ET_RainRecord]  TO  [Calsense];
GRANT Inherit ON  [AlrtFltrGrp]  TO  [Calsense];
GRANT Inherit ON  [ControllerSites]  TO  [Calsense];
GRANT Inherit ON  [Controllers]  TO  [Calsense];
GRANT Inherit ON  [CommTypes]  TO  [Calsense];
GRANT Inherit ON  [Models]  TO  [Calsense];
GRANT Inherit ON  [AlertFiltering]  TO  [Calsense];
GRANT Inherit ON  [AlertFilterOptions]  TO  [Calsense];
GRANT Inherit ON  [UsrTypes]  TO  [Calsense];
GRANT Inherit ON  [GeneralHistory]  TO  [Calsense];
GRANT Inherit ON  [History]  TO  [Calsense];
GRANT Inherit ON  [Alerts]  TO  [Calsense];
GRANT Inherit ON  [TaskData]  TO  [Calsense];
GRANT Inherit ON  [TaskExclusion]  TO  [Calsense];
GRANT Inherit ON  [TaskOptions]  TO  [Calsense];
GRANT Inherit ON  [TaskSchedule]  TO  [Calsense];
GRANT Inherit ON  [ETComparing]  TO  [Calsense];
GRANT Inherit ON  [Fnctns]  TO  [Calsense];
GRANT Inherit ON  [CMOS]  TO  [Calsense];
GRANT Inherit ON  [CommWght]  TO  [Calsense];
GRANT Inherit ON  [DailyData]  TO  [Calsense];
GRANT Inherit ON  [PastSchedule]  TO  [Calsense];
GRANT Inherit ON  [POCReportData]  TO  [Calsense];
GRANT Inherit ON  [AlertFilterOptionDetails]  TO  [Calsense];
GRANT Inherit ON  [Assignment]  TO  [Calsense];
GRANT Inherit ON  [BulkPgmData1]  TO  [Calsense];
GRANT Inherit ON  [BulkPgmData2]  TO  [Calsense];
GRANT Inherit ON  [BulkPgmData3]  TO  [Calsense];
GRANT Inherit ON  [BulkPgmData4]  TO  [Calsense];
GRANT Inherit ON  [MonthlyPrgmData]  TO  [Calsense];
GRANT Inherit ON  [POCPrgmData]  TO  [Calsense];
GRANT Inherit ON  [POCSchedule]  TO  [Calsense];
GRANT Inherit ON  [ProgramPrgmData]  TO  [Calsense];
GRANT Inherit ON  [StationPgmData]  TO  [Calsense];
GRANT Inherit ON  [TagNames]  TO  [Calsense];
GRANT Inherit ON  [TwentyEightDayPrgmData]  TO  [Calsense];
GRANT Inherit ON  [City]  TO  [Calsense];
GRANT Inherit ON  [County]  TO  [Calsense];
GRANT Inherit ON  [PrgmTags]  TO  [Calsense];
GRANT Inherit ON  [States]  TO  [Calsense];
GRANT Inherit ON  [MonthET]  TO  [Calsense];
GRANT Inherit ON  [StatusMessages]  TO  [Calsense];
GRANT Inherit ON  [StatusMessageCategories]  TO  [Calsense];
GRANT Inherit ON  [RainShare]  TO  [Calsense];
GRANT Inherit ON  [ETShare]  TO  [Calsense];
GRANT Inherit ON  [AssignmentSites]  TO  [Calsense];
GRANT Inherit ON  [ActivityLog]  TO  [Calsense];
GRANT Inherit ON  [ComSrvrs]  TO  [Calsense];
GRANT Inherit ON  [FlowRec]  TO  [Calsense];
GRANT Inherit ON  [Polling]  TO  [Calsense];
GRANT Inherit ON  [PollingControllers]  TO  [Calsense];
GRANT Inherit ON  [Widgets]  TO  [Calsense];
GRANT Inherit ON  [TaskStatus]  TO  [Calsense];
GRANT Inherit ON  [AssignmentCompanies]  TO  [Calsense];
GRANT Inherit ON  [CS3000Alerts]  TO  [Calsense];
GRANT Inherit ON  [CS3000Cards]  TO  [Calsense];
GRANT Inherit ON  [CS3000Controllers]  TO  [Calsense];
GRANT Inherit ON  [CS3000FlowRecordingTable]  TO  [Calsense];
GRANT Inherit ON  [CS3000ManualPrograms]  TO  [Calsense];
GRANT Inherit ON  [CS3000Networks]  TO  [Calsense];
GRANT Inherit ON  [CS3000POC]  TO  [Calsense];
GRANT Inherit ON  [CS3000POCRecord]  TO  [Calsense];
GRANT Inherit ON  [CS3000RainShutdown]  TO  [Calsense];
GRANT Inherit ON  [CS3000StationDataRecord]  TO  [Calsense];
GRANT Inherit ON  [CS3000StationGroups]  TO  [Calsense];
GRANT Inherit ON  [CS3000StationHistory]  TO  [Calsense];
GRANT Inherit ON  [CS3000Stations]  TO  [Calsense];
GRANT Inherit ON  [CS3000SystemRecord]  TO  [Calsense];
GRANT Inherit ON  [CS3000Systems]  TO  [Calsense];
GRANT Inherit ON  [CS3000Weather]  TO  [Calsense];
GRANT Inherit ON  [CS3000WeatherData]  TO  [Calsense];
GRANT Inherit ON  [Temp_CS3000Controllers]  TO  [Calsense];
GRANT Inherit ON  [Temp_CS3000ManualPrograms]  TO  [Calsense];
GRANT Inherit ON  [Temp_CS3000Networks]  TO  [Calsense];
GRANT Inherit ON  [Temp_CS3000POC]  TO  [Calsense];
GRANT Inherit ON  [Temp_CS3000StationGroups]  TO  [Calsense];
GRANT Inherit ON  [Temp_CS3000Stations]  TO  [Calsense];
GRANT Inherit ON  [Temp_CS3000Systems]  TO  [Calsense];
GRANT Inherit ON  [Temp_CS3000Weather]  TO  [Calsense];
GRANT Inherit ON  [CS3000CommServerSettings]  TO  [Calsense];
GRANT Inherit ON  [IHSFY_jobs]  TO  [Calsense];
GRANT Inherit ON  [IHSFY_notifications]  TO  [Calsense];
GRANT Inherit ON  [Manual]  TO  [Calsense];
GRANT Inherit ON  [ManualStations]  TO  [Calsense];
GRANT Inherit ON  [Lights]  TO  [Calsense];
GRANT Inherit ON  [CS3000Lights]  TO  [Calsense];
GRANT Inherit ON  [CS3000LightsRecord]  TO  [Calsense];
GRANT Inherit ON  [Temp_CS3000Lights]  TO  [Calsense];
GRANT Inherit ON  [GprsExpirationDatesView]  TO  [Calsense];
GRANT Inherit ON  [NewGPRSExpirationDatesView]  TO  [Calsense];
GRANT Inherit ON  [CommunicationStatistics]  TO  [Calsense];
GRANT Inherit ON  [ControllersDetails]  TO  [Calsense];
GRANT Inherit ON  [ControllersSetupReport]  TO  [Calsense];
GRANT Inherit ON  [DeletePData]  TO  [Calsense];
GRANT Inherit ON  [PanelSwapping]  TO  [Calsense];
GRANT Inherit ON  [ArchiveActivityLog]  TO  [Calsense];
GRANT Inherit ON  [ArchiveAlerts]  TO  [Calsense];
GRANT Inherit ON  [ArchiveHistory]  TO  [Calsense];
GRANT Inherit ON  [ArchiveGeneralHistory]  TO  [Calsense];
GRANT Inherit ON  [DeleteTempCS3000ProgramData]  TO  [Calsense];
GRANT Inherit ON  [DeleteUnusedCS3000ProgramData]  TO  [Calsense];
GRANT Inherit ON  [locate_network]  TO  [Calsense];
GRANT Inherit ON  [PrepareCS3000ProgramData]  TO  [Calsense];
GRANT Inherit ON  [UpdateCS3000ProgramData]  TO  [Calsense];
GRANT Inherit ON  [CompleteCS3000ProgramData]  TO  [Calsense];
GRANT Inherit ON  [Add_IHSFY_Command]  TO  [Calsense];
GRANT Inherit ON  [SplitColumns]  TO  [Calsense];
GRANT Inherit ON  [GPRS]  TO  [Calsense];
GRANT Inherit ON  [Archive]  TO  [Calsense];

GRANT Inherit ON  [CC4 User]  TO  [Calsense];
EXECUTE PROCEDURE sp_ModifyPermission( 11, NULL, NULL, 'Calsense', 8, TRUE );
EXECUTE PROCEDURE sp_ModifyPermission( 11, NULL, NULL, 'Calsense', 2147483904, FALSE );

GRANT  Inherit Create TABLE TO  [Calsense];
GRANT  Inherit Create VIEW TO  [Calsense];
GRANT  Inherit Create USER TO  [Calsense];
GRANT  Inherit Create USER GROUP TO  [Calsense];
GRANT  Inherit Create PROCEDURE TO  [Calsense];
GRANT  Inherit Create FUNCTION TO  [Calsense];
GRANT  Inherit Create PACKAGE TO  [Calsense];
GRANT  Inherit Create LINK TO  [Calsense];
GRANT  Inherit Create PUBLICATION TO  [Calsense];
GRANT  Inherit Create SUBSCRIPTION TO  [Calsense];
GRANT Select ON  [Companies]  TO  [CC4 User];
GRANT Update ON  [Companies]  TO  [CC4 User];
GRANT Insert ON  [Companies]  TO  [CC4 User];
GRANT Delete ON  [Companies]  TO  [CC4 User];
GRANT Alter ON  [Companies]  TO  [CC4 User];
GRANT Drop ON  [Companies]  TO  [CC4 User];
GRANT Select ON  [Usrs]  TO  [CC4 User];
GRANT Update ON  [Usrs]  TO  [CC4 User];
GRANT Insert ON  [Usrs]  TO  [CC4 User];
GRANT Delete ON  [Usrs]  TO  [CC4 User];
GRANT Alter ON  [Usrs]  TO  [CC4 User];
GRANT Drop ON  [Usrs]  TO  [CC4 User];
GRANT Select ON  [AlrtFltrGrp]  TO  [CC4 User];
GRANT Update ON  [AlrtFltrGrp]  TO  [CC4 User];
GRANT Insert ON  [AlrtFltrGrp]  TO  [CC4 User];
GRANT Delete ON  [AlrtFltrGrp]  TO  [CC4 User];
GRANT Alter ON  [AlrtFltrGrp]  TO  [CC4 User];
GRANT Drop ON  [AlrtFltrGrp]  TO  [CC4 User];
GRANT Select ON  [ControllerSites]  TO  [CC4 User];
GRANT Update ON  [ControllerSites]  TO  [CC4 User];
GRANT Insert ON  [ControllerSites]  TO  [CC4 User];
GRANT Delete ON  [ControllerSites]  TO  [CC4 User];
GRANT Alter ON  [ControllerSites]  TO  [CC4 User];
GRANT Drop ON  [ControllerSites]  TO  [CC4 User];
GRANT Select ON  [Controllers]  TO  [CC4 User];
GRANT Update ON  [Controllers]  TO  [CC4 User];
GRANT Insert ON  [Controllers]  TO  [CC4 User];
GRANT Delete ON  [Controllers]  TO  [CC4 User];
GRANT Alter ON  [Controllers]  TO  [CC4 User];
GRANT Drop ON  [Controllers]  TO  [CC4 User];
GRANT Select ON  [CommTypes]  TO  [CC4 User];
GRANT Update ON  [CommTypes]  TO  [CC4 User];
GRANT Insert ON  [CommTypes]  TO  [CC4 User];
GRANT Delete ON  [CommTypes]  TO  [CC4 User];
GRANT Alter ON  [CommTypes]  TO  [CC4 User];
GRANT Drop ON  [CommTypes]  TO  [CC4 User];
GRANT Select ON  [Models]  TO  [CC4 User];
GRANT Update ON  [Models]  TO  [CC4 User];
GRANT Insert ON  [Models]  TO  [CC4 User];
GRANT Delete ON  [Models]  TO  [CC4 User];
GRANT Alter ON  [Models]  TO  [CC4 User];
GRANT Drop ON  [Models]  TO  [CC4 User];
GRANT Select ON  [AlertFiltering]  TO  [CC4 User];
GRANT Update ON  [AlertFiltering]  TO  [CC4 User];
GRANT Insert ON  [AlertFiltering]  TO  [CC4 User];
GRANT Delete ON  [AlertFiltering]  TO  [CC4 User];
GRANT Alter ON  [AlertFiltering]  TO  [CC4 User];
GRANT Drop ON  [AlertFiltering]  TO  [CC4 User];
GRANT Select ON  [AlertFilterOptions]  TO  [CC4 User];
GRANT Update ON  [AlertFilterOptions]  TO  [CC4 User];
GRANT Insert ON  [AlertFilterOptions]  TO  [CC4 User];
GRANT Delete ON  [AlertFilterOptions]  TO  [CC4 User];
GRANT Alter ON  [AlertFilterOptions]  TO  [CC4 User];
GRANT Drop ON  [AlertFilterOptions]  TO  [CC4 User];
GRANT Select ON  [UsrTypes]  TO  [CC4 User];
GRANT Update ON  [UsrTypes]  TO  [CC4 User];
GRANT Insert ON  [UsrTypes]  TO  [CC4 User];
GRANT Delete ON  [UsrTypes]  TO  [CC4 User];
GRANT Alter ON  [UsrTypes]  TO  [CC4 User];
GRANT Drop ON  [UsrTypes]  TO  [CC4 User];
GRANT Select ON  [GeneralHistory]  TO  [CC4 User];
GRANT Update ON  [GeneralHistory]  TO  [CC4 User];
GRANT Insert ON  [GeneralHistory]  TO  [CC4 User];
GRANT Delete ON  [GeneralHistory]  TO  [CC4 User];
GRANT Alter ON  [GeneralHistory]  TO  [CC4 User];
GRANT Drop ON  [GeneralHistory]  TO  [CC4 User];
GRANT Select ON  [History]  TO  [CC4 User];
GRANT Update ON  [History]  TO  [CC4 User];
GRANT Insert ON  [History]  TO  [CC4 User];
GRANT Delete ON  [History]  TO  [CC4 User];
GRANT Alter ON  [History]  TO  [CC4 User];
GRANT Drop ON  [History]  TO  [CC4 User];
GRANT Select ON  [TaskData]  TO  [CC4 User];
GRANT Update ON  [TaskData]  TO  [CC4 User];
GRANT Insert ON  [TaskData]  TO  [CC4 User];
GRANT Delete ON  [TaskData]  TO  [CC4 User];
GRANT Alter ON  [TaskData]  TO  [CC4 User];
GRANT Drop ON  [TaskData]  TO  [CC4 User];
GRANT Select ON  [TaskExclusion]  TO  [CC4 User];
GRANT Update ON  [TaskExclusion]  TO  [CC4 User];
GRANT Insert ON  [TaskExclusion]  TO  [CC4 User];
GRANT Delete ON  [TaskExclusion]  TO  [CC4 User];
GRANT Alter ON  [TaskExclusion]  TO  [CC4 User];
GRANT Drop ON  [TaskExclusion]  TO  [CC4 User];
GRANT Select ON  [TaskOptions]  TO  [CC4 User];
GRANT Update ON  [TaskOptions]  TO  [CC4 User];
GRANT Insert ON  [TaskOptions]  TO  [CC4 User];
GRANT Delete ON  [TaskOptions]  TO  [CC4 User];
GRANT Alter ON  [TaskOptions]  TO  [CC4 User];
GRANT Drop ON  [TaskOptions]  TO  [CC4 User];
GRANT Select ON  [TaskSchedule]  TO  [CC4 User];
GRANT Update ON  [TaskSchedule]  TO  [CC4 User];
GRANT Insert ON  [TaskSchedule]  TO  [CC4 User];
GRANT Delete ON  [TaskSchedule]  TO  [CC4 User];
GRANT Alter ON  [TaskSchedule]  TO  [CC4 User];
GRANT Drop ON  [TaskSchedule]  TO  [CC4 User];
GRANT Select ON  [ETComparing]  TO  [CC4 User];
GRANT Update ON  [ETComparing]  TO  [CC4 User];
GRANT Insert ON  [ETComparing]  TO  [CC4 User];
GRANT Delete ON  [ETComparing]  TO  [CC4 User];
GRANT Alter ON  [ETComparing]  TO  [CC4 User];
GRANT Drop ON  [ETComparing]  TO  [CC4 User];
GRANT Select ON  [Fnctns]  TO  [CC4 User];
GRANT Update ON  [Fnctns]  TO  [CC4 User];
GRANT Insert ON  [Fnctns]  TO  [CC4 User];
GRANT Delete ON  [Fnctns]  TO  [CC4 User];
GRANT Alter ON  [Fnctns]  TO  [CC4 User];
GRANT Drop ON  [Fnctns]  TO  [CC4 User];
GRANT Select ON  [CMOS]  TO  [CC4 User];
GRANT Update ON  [CMOS]  TO  [CC4 User];
GRANT Insert ON  [CMOS]  TO  [CC4 User];
GRANT Delete ON  [CMOS]  TO  [CC4 User];
GRANT Alter ON  [CMOS]  TO  [CC4 User];
GRANT Drop ON  [CMOS]  TO  [CC4 User];
GRANT Select ON  [CommWght]  TO  [CC4 User];
GRANT Update ON  [CommWght]  TO  [CC4 User];
GRANT Insert ON  [CommWght]  TO  [CC4 User];
GRANT Delete ON  [CommWght]  TO  [CC4 User];
GRANT Alter ON  [CommWght]  TO  [CC4 User];
GRANT Drop ON  [CommWght]  TO  [CC4 User];
GRANT Select ON  [DailyData]  TO  [CC4 User];
GRANT Update ON  [DailyData]  TO  [CC4 User];
GRANT Insert ON  [DailyData]  TO  [CC4 User];
GRANT Delete ON  [DailyData]  TO  [CC4 User];
GRANT Alter ON  [DailyData]  TO  [CC4 User];
GRANT Drop ON  [DailyData]  TO  [CC4 User];
GRANT Select ON  [PastSchedule]  TO  [CC4 User];
GRANT Update ON  [PastSchedule]  TO  [CC4 User];
GRANT Insert ON  [PastSchedule]  TO  [CC4 User];
GRANT Delete ON  [PastSchedule]  TO  [CC4 User];
GRANT Alter ON  [PastSchedule]  TO  [CC4 User];
GRANT Drop ON  [PastSchedule]  TO  [CC4 User];
GRANT Select ON  [POCReportData]  TO  [CC4 User];
GRANT Update ON  [POCReportData]  TO  [CC4 User];
GRANT Insert ON  [POCReportData]  TO  [CC4 User];
GRANT Delete ON  [POCReportData]  TO  [CC4 User];
GRANT Alter ON  [POCReportData]  TO  [CC4 User];
GRANT Drop ON  [POCReportData]  TO  [CC4 User];
GRANT Select ON  [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Update ON  [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Insert ON  [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Delete ON  [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Alter ON  [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Drop ON  [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Select ON  [Assignment]  TO  [CC4 User];
GRANT Update ON  [Assignment]  TO  [CC4 User];
GRANT Insert ON  [Assignment]  TO  [CC4 User];
GRANT Delete ON  [Assignment]  TO  [CC4 User];
GRANT Alter ON  [Assignment]  TO  [CC4 User];
GRANT Drop ON  [Assignment]  TO  [CC4 User];
GRANT Select ON  [BulkPgmData1]  TO  [CC4 User];
GRANT Update ON  [BulkPgmData1]  TO  [CC4 User];
GRANT Insert ON  [BulkPgmData1]  TO  [CC4 User];
GRANT Delete ON  [BulkPgmData1]  TO  [CC4 User];
GRANT Alter ON  [BulkPgmData1]  TO  [CC4 User];
GRANT Drop ON  [BulkPgmData1]  TO  [CC4 User];
GRANT Select ON  [BulkPgmData2]  TO  [CC4 User];
GRANT Update ON  [BulkPgmData2]  TO  [CC4 User];
GRANT Insert ON  [BulkPgmData2]  TO  [CC4 User];
GRANT Delete ON  [BulkPgmData2]  TO  [CC4 User];
GRANT Alter ON  [BulkPgmData2]  TO  [CC4 User];
GRANT Drop ON  [BulkPgmData2]  TO  [CC4 User];
GRANT Select ON  [BulkPgmData3]  TO  [CC4 User];
GRANT Update ON  [BulkPgmData3]  TO  [CC4 User];
GRANT Insert ON  [BulkPgmData3]  TO  [CC4 User];
GRANT Delete ON  [BulkPgmData3]  TO  [CC4 User];
GRANT Alter ON  [BulkPgmData3]  TO  [CC4 User];
GRANT Drop ON  [BulkPgmData3]  TO  [CC4 User];
GRANT Select ON  [BulkPgmData4]  TO  [CC4 User];
GRANT Update ON  [BulkPgmData4]  TO  [CC4 User];
GRANT Insert ON  [BulkPgmData4]  TO  [CC4 User];
GRANT Delete ON  [BulkPgmData4]  TO  [CC4 User];
GRANT Alter ON  [BulkPgmData4]  TO  [CC4 User];
GRANT Drop ON  [BulkPgmData4]  TO  [CC4 User];
GRANT Select ON  [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update ON  [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert ON  [MonthlyPrgmData]  TO  [CC4 User];
GRANT Delete ON  [MonthlyPrgmData]  TO  [CC4 User];
GRANT Alter ON  [MonthlyPrgmData]  TO  [CC4 User];
GRANT Drop ON  [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select ON  [POCPrgmData]  TO  [CC4 User];
GRANT Update ON  [POCPrgmData]  TO  [CC4 User];
GRANT Insert ON  [POCPrgmData]  TO  [CC4 User];
GRANT Delete ON  [POCPrgmData]  TO  [CC4 User];
GRANT Alter ON  [POCPrgmData]  TO  [CC4 User];
GRANT Drop ON  [POCPrgmData]  TO  [CC4 User];
GRANT Select ON  [POCSchedule]  TO  [CC4 User];
GRANT Update ON  [POCSchedule]  TO  [CC4 User];
GRANT Insert ON  [POCSchedule]  TO  [CC4 User];
GRANT Delete ON  [POCSchedule]  TO  [CC4 User];
GRANT Alter ON  [POCSchedule]  TO  [CC4 User];
GRANT Drop ON  [POCSchedule]  TO  [CC4 User];
GRANT Select ON  [ProgramPrgmData]  TO  [CC4 User];
GRANT Update ON  [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert ON  [ProgramPrgmData]  TO  [CC4 User];
GRANT Delete ON  [ProgramPrgmData]  TO  [CC4 User];
GRANT Alter ON  [ProgramPrgmData]  TO  [CC4 User];
GRANT Drop ON  [ProgramPrgmData]  TO  [CC4 User];
GRANT Select ON  [StationPgmData]  TO  [CC4 User];
GRANT Update ON  [StationPgmData]  TO  [CC4 User];
GRANT Insert ON  [StationPgmData]  TO  [CC4 User];
GRANT Delete ON  [StationPgmData]  TO  [CC4 User];
GRANT Alter ON  [StationPgmData]  TO  [CC4 User];
GRANT Drop ON  [StationPgmData]  TO  [CC4 User];
GRANT Select ON  [TagNames]  TO  [CC4 User];
GRANT Update ON  [TagNames]  TO  [CC4 User];
GRANT Insert ON  [TagNames]  TO  [CC4 User];
GRANT Delete ON  [TagNames]  TO  [CC4 User];
GRANT Alter ON  [TagNames]  TO  [CC4 User];
GRANT Drop ON  [TagNames]  TO  [CC4 User];
GRANT Select ON  [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update ON  [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert ON  [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Delete ON  [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Alter ON  [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Drop ON  [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select ON  [City]  TO  [CC4 User];
GRANT Update ON  [City]  TO  [CC4 User];
GRANT Insert ON  [City]  TO  [CC4 User];
GRANT Delete ON  [City]  TO  [CC4 User];
GRANT Alter ON  [City]  TO  [CC4 User];
GRANT Drop ON  [City]  TO  [CC4 User];
GRANT Select ON  [County]  TO  [CC4 User];
GRANT Update ON  [County]  TO  [CC4 User];
GRANT Insert ON  [County]  TO  [CC4 User];
GRANT Delete ON  [County]  TO  [CC4 User];
GRANT Alter ON  [County]  TO  [CC4 User];
GRANT Drop ON  [County]  TO  [CC4 User];
GRANT Select ON  [PrgmTags]  TO  [CC4 User];
GRANT Update ON  [PrgmTags]  TO  [CC4 User];
GRANT Insert ON  [PrgmTags]  TO  [CC4 User];
GRANT Delete ON  [PrgmTags]  TO  [CC4 User];
GRANT Alter ON  [PrgmTags]  TO  [CC4 User];
GRANT Drop ON  [PrgmTags]  TO  [CC4 User];
GRANT Select ON  [States]  TO  [CC4 User];
GRANT Update ON  [States]  TO  [CC4 User];
GRANT Insert ON  [States]  TO  [CC4 User];
GRANT Delete ON  [States]  TO  [CC4 User];
GRANT Alter ON  [States]  TO  [CC4 User];
GRANT Drop ON  [States]  TO  [CC4 User];
GRANT Select ON  [MonthET]  TO  [CC4 User];
GRANT Update ON  [MonthET]  TO  [CC4 User];
GRANT Insert ON  [MonthET]  TO  [CC4 User];
GRANT Delete ON  [MonthET]  TO  [CC4 User];
GRANT Alter ON  [MonthET]  TO  [CC4 User];
GRANT Drop ON  [MonthET]  TO  [CC4 User];
GRANT Select ON  [StatusMessages]  TO  [CC4 User];
GRANT Update ON  [StatusMessages]  TO  [CC4 User];
GRANT Insert ON  [StatusMessages]  TO  [CC4 User];
GRANT Delete ON  [StatusMessages]  TO  [CC4 User];
GRANT Alter ON  [StatusMessages]  TO  [CC4 User];
GRANT Drop ON  [StatusMessages]  TO  [CC4 User];
GRANT Select ON  [StatusMessageCategories]  TO  [CC4 User];
GRANT Update ON  [StatusMessageCategories]  TO  [CC4 User];
GRANT Insert ON  [StatusMessageCategories]  TO  [CC4 User];
GRANT Delete ON  [StatusMessageCategories]  TO  [CC4 User];
GRANT Alter ON  [StatusMessageCategories]  TO  [CC4 User];
GRANT Drop ON  [StatusMessageCategories]  TO  [CC4 User];
GRANT Select ON  [RainShare]  TO  [CC4 User];
GRANT Update ON  [RainShare]  TO  [CC4 User];
GRANT Insert ON  [RainShare]  TO  [CC4 User];
GRANT Delete ON  [RainShare]  TO  [CC4 User];
GRANT Alter ON  [RainShare]  TO  [CC4 User];
GRANT Drop ON  [RainShare]  TO  [CC4 User];
GRANT Select ON  [ETShare]  TO  [CC4 User];
GRANT Update ON  [ETShare]  TO  [CC4 User];
GRANT Insert ON  [ETShare]  TO  [CC4 User];
GRANT Delete ON  [ETShare]  TO  [CC4 User];
GRANT Alter ON  [ETShare]  TO  [CC4 User];
GRANT Drop ON  [ETShare]  TO  [CC4 User];
GRANT Select ON  [AssignmentSites]  TO  [CC4 User];
GRANT Update ON  [AssignmentSites]  TO  [CC4 User];
GRANT Insert ON  [AssignmentSites]  TO  [CC4 User];
GRANT Delete ON  [AssignmentSites]  TO  [CC4 User];
GRANT Alter ON  [AssignmentSites]  TO  [CC4 User];
GRANT Drop ON  [AssignmentSites]  TO  [CC4 User];
GRANT Select ON  [ActivityLog]  TO  [CC4 User];
GRANT Update ON  [ActivityLog]  TO  [CC4 User];
GRANT Insert ON  [ActivityLog]  TO  [CC4 User];
GRANT Delete ON  [ActivityLog]  TO  [CC4 User];
GRANT Alter ON  [ActivityLog]  TO  [CC4 User];
GRANT Drop ON  [ActivityLog]  TO  [CC4 User];
GRANT Select ON  [ComSrvrs]  TO  [CC4 User];
GRANT Update ON  [ComSrvrs]  TO  [CC4 User];
GRANT Insert ON  [ComSrvrs]  TO  [CC4 User];
GRANT Delete ON  [ComSrvrs]  TO  [CC4 User];
GRANT Alter ON  [ComSrvrs]  TO  [CC4 User];
GRANT Drop ON  [ComSrvrs]  TO  [CC4 User];
GRANT Select ON  [FlowRec]  TO  [CC4 User];
GRANT Update ON  [FlowRec]  TO  [CC4 User];
GRANT Insert ON  [FlowRec]  TO  [CC4 User];
GRANT Delete ON  [FlowRec]  TO  [CC4 User];
GRANT Alter ON  [FlowRec]  TO  [CC4 User];
GRANT Drop ON  [FlowRec]  TO  [CC4 User];
GRANT Select ON  [Polling]  TO  [CC4 User];
GRANT Update ON  [Polling]  TO  [CC4 User];
GRANT Insert ON  [Polling]  TO  [CC4 User];
GRANT Delete ON  [Polling]  TO  [CC4 User];
GRANT Alter ON  [Polling]  TO  [CC4 User];
GRANT Drop ON  [Polling]  TO  [CC4 User];
GRANT Select ON  [PollingControllers]  TO  [CC4 User];
GRANT Update ON  [PollingControllers]  TO  [CC4 User];
GRANT Insert ON  [PollingControllers]  TO  [CC4 User];
GRANT Delete ON  [PollingControllers]  TO  [CC4 User];
GRANT Alter ON  [PollingControllers]  TO  [CC4 User];
GRANT Drop ON  [PollingControllers]  TO  [CC4 User];
GRANT Select( [CompanyID] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [CompanyID] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [CompanyID] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [Name] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [Name] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [Name] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [Company_Address] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [Company_Address] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [Company_Address] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [Company_Address2] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [Company_Address2] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [Company_Address2] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [Company_City] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [Company_City] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [Company_City] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [Company_State] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [Company_State] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [Company_State] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [Company_ZipCode] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [Company_ZipCode] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [Company_ZipCode] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [Deleted] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [Deleted] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [Deleted] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [Company_TimeZone] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [Company_TimeZone] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [Company_TimeZone] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [CompanyTypes] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [CompanyTypes] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [CompanyTypes] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [Notes] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [Notes] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [Notes] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [NotesStartDate] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [NotesStartDate] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [NotesStartDate] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [NotesEndDate] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [NotesEndDate] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [NotesEndDate] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [EmailNotes] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [EmailNotes] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [EmailNotes] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [WebServiceKey] ) ON [Companies]  TO  [CC4 User];
GRANT Update( [WebServiceKey] ) ON [Companies]  TO  [CC4 User];
GRANT Insert( [WebServiceKey] ) ON [Companies]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [UserTypes] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [UserTypes] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [UserTypes] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [Usrnm] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [Usrnm] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [Usrnm] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [Psswrd] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [Psswrd] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [Psswrd] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_Name] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_Name] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_Name] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_Company] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_Company] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_Company] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_Address] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_Address] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_Address] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_Address2] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_Address2] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_Address2] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_City] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_City] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_City] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_State] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_State] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_State] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_ZipCode] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_ZipCode] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_ZipCode] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_Phone] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_Phone] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_Phone] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_Extention] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_Extention] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_Extention] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_Debug] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_Debug] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_Debug] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_SendPData] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_SendPData] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_SendPData] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_SavePData] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_SavePData] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_SavePData] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_GetLights] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_GetLights] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_GetLights] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_EditController] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_EditController] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_EditController] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_AddControllers] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_AddControllers] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_AddControllers] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_DeleteControllers] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_DeleteControllers] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_DeleteControllers] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_AccessPasswords] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_AccessPasswords] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_AccessPasswords] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_ClearMLB] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_ClearMLB] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_ClearMLB] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_ClearHO] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_ClearHO] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_ClearHO] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_SendNoW] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_SendNoW] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_SendNoW] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_MVOR] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_MVOR] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_MVOR] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_OnOff] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_OnOff] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_OnOff] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_SetTimeDate] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_SetTimeDate] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_SetTimeDate] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_ManualProgram] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_ManualProgram] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_ManualProgram] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [User_Mobile] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [User_Mobile] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [User_Mobile] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [CompanyID] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [CompanyID] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [CompanyID] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [Deleted] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [Deleted] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [Deleted] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [ReportSelection] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [ReportSelection] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [ReportSelection] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [Dashboard_DateRange] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [Dashboard_DateRange] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [Dashboard_DateRange] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [Dashboard_Viewing] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [Dashboard_Viewing] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [Dashboard_Viewing] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [EmailAlerts] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [EmailAlerts] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [EmailAlerts] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [LastEmailDate] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [LastEmailDate] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [LastEmailDate] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [NotifyWindow] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [NotifyWindow] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [NotifyWindow] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [DefaultSortDirection] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [DefaultSortDirection] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [DefaultSortDirection] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [LandingPage] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [LandingPage] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [LandingPage] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [TempId] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [TempId] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [TempId] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [TempIdDateRequested] ) ON [Usrs]  TO  [CC4 User];
GRANT Update( [TempIdDateRequested] ) ON [Usrs]  TO  [CC4 User];
GRANT Insert( [TempIdDateRequested] ) ON [Usrs]  TO  [CC4 User];
GRANT Select( [GroupID] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Update( [GroupID] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Insert( [GroupID] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Select( [FltrName] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Update( [FltrName] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Insert( [FltrName] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Select( [Deleted] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Update( [Deleted] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Insert( [Deleted] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Select( [DefaultFltr] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Update( [DefaultFltr] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Insert( [DefaultFltr] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Select( [DefaultGIS] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Update( [DefaultGIS] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Insert( [DefaultGIS] ) ON [AlrtFltrGrp]  TO  [CC4 User];
GRANT Select( [SiteID] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Update( [SiteID] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Insert( [SiteID] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Select( [CompanyID] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Update( [CompanyID] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Insert( [CompanyID] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Select( [SiteName] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Update( [SiteName] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Insert( [SiteName] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Select( [Deleted] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Update( [Deleted] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Insert( [Deleted] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [ControllerSites]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [SiteID] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [SiteID] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [SiteID] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [ControllerName] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [ControllerName] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [ControllerName] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [RadioGroupID] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [RadioGroupID] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [RadioGroupID] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [ConnectionType] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [ConnectionType] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [ConnectionType] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [Installed] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [Installed] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [Installed] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [LastCommunicated] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [LastCommunicated] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [LastCommunicated] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [Address] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [Address] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [Address] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [BaudRate] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [BaudRate] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [BaudRate] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [Model] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [Model] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [Model] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [PhoneNumber] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [PhoneNumber] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [PhoneNumber] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [StationsInUse] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [StationsInUse] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [StationsInUse] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [SoftwareVersion] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [SoftwareVersion] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [SoftwareVersion] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [AlertCode] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [AlertCode] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [AlertCode] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [Notes] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [Notes] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [Notes] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [LightsDescription1] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [LightsDescription1] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [LightsDescription1] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [LightsDescription2] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [LightsDescription2] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [LightsDescription2] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [LightsDescription3] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [LightsDescription3] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [LightsDescription3] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [LightsDescription4] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [LightsDescription4] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [LightsDescription4] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [Communicate] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [Communicate] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [Communicate] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [WeatherShutdown] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [WeatherShutdown] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [WeatherShutdown] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [ControllerPicture] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [ControllerPicture] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [ControllerPicture] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [AutoRetrieveReports] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [AutoRetrieveReports] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [AutoRetrieveReports] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [Deleted] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [Deleted] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [Deleted] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [FOAL] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [FOAL] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [FOAL] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [TimeoutAdj] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [TimeoutAdj] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [TimeoutAdj] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [CommServer] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [CommServer] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [CommServer] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [UseCTS] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [UseCTS] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [UseCTS] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [WeatherStation] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [WeatherStation] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [WeatherStation] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [ControllerImage] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [ControllerImage] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [ControllerImage] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [ImageType] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [ImageType] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [ImageType] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [SRInvolved] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [SRInvolved] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [SRInvolved] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [TrainingController] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [TrainingController] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [TrainingController] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [LRCommType] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [LRCommType] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [LRCommType] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [Latitude] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [Latitude] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [Latitude] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [Longitude] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [Longitude] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [Longitude] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [ETSharingControllerID] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [ETSharingControllerID] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [ETSharingControllerID] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [RainSharingControllerID] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [RainSharingControllerID] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [RainSharingControllerID] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [TimeZone] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [TimeZone] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [TimeZone] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [CompanyID] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [CompanyID] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [CompanyID] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [TP_Version] ) ON [Controllers]  TO  [CC4 User];
GRANT Update( [TP_Version] ) ON [Controllers]  TO  [CC4 User];
GRANT Insert( [TP_Version] ) ON [Controllers]  TO  [CC4 User];
GRANT Select( [CTIndex] ) ON [CommTypes]  TO  [CC4 User];
GRANT Update( [CTIndex] ) ON [CommTypes]  TO  [CC4 User];
GRANT Insert( [CTIndex] ) ON [CommTypes]  TO  [CC4 User];
GRANT Select( [CommunicationsType] ) ON [CommTypes]  TO  [CC4 User];
GRANT Update( [CommunicationsType] ) ON [CommTypes]  TO  [CC4 User];
GRANT Insert( [CommunicationsType] ) ON [CommTypes]  TO  [CC4 User];
GRANT Select( [ModelIndex] ) ON [Models]  TO  [CC4 User];
GRANT Update( [ModelIndex] ) ON [Models]  TO  [CC4 User];
GRANT Insert( [ModelIndex] ) ON [Models]  TO  [CC4 User];
GRANT Select( [Model] ) ON [Models]  TO  [CC4 User];
GRANT Update( [Model] ) ON [Models]  TO  [CC4 User];
GRANT Insert( [Model] ) ON [Models]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Select( [AlertFilter] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Update( [AlertFilter] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Insert( [AlertFilter] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Select( [AlertState] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Update( [AlertState] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Insert( [AlertState] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Select( [AlertStyle] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Update( [AlertStyle] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Insert( [AlertStyle] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Select( [AlertFilterGroupID] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Update( [AlertFilterGroupID] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Insert( [AlertFilterGroupID] ) ON [AlertFiltering]  TO  [CC4 User];
GRANT Select( [FilterID] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Update( [FilterID] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Insert( [FilterID] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Select( [ParentID] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Update( [ParentID] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Insert( [ParentID] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Select( [Display] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Update( [Display] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Insert( [Display] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Select( [Sort] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Update( [Sort] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Insert( [Sort] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Select( [Source] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Update( [Source] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Insert( [Source] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Select( [Style] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Update( [Style] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Insert( [Style] ) ON [AlertFilterOptions]  TO  [CC4 User];
GRANT Select( [UserTypeID] ) ON [UsrTypes]  TO  [CC4 User];
GRANT Update( [UserTypeID] ) ON [UsrTypes]  TO  [CC4 User];
GRANT Insert( [UserTypeID] ) ON [UsrTypes]  TO  [CC4 User];
GRANT Select( [Description] ) ON [UsrTypes]  TO  [CC4 User];
GRANT Update( [Description] ) ON [UsrTypes]  TO  [CC4 User];
GRANT Insert( [Description] ) ON [UsrTypes]  TO  [CC4 User];
GRANT Select( [HistoryID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Update( [HistoryID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Insert( [HistoryID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Select( [HistoryTimeStamp] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Update( [HistoryTimeStamp] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Insert( [HistoryTimeStamp] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Select( [TaskID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Update( [TaskID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Insert( [TaskID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Select( [CommServer] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Update( [CommServer] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Insert( [CommServer] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Select( [HistoryErrorCode] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Update( [HistoryErrorCode] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Insert( [HistoryErrorCode] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Select( [SpeedCommunications] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Update( [SpeedCommunications] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Insert( [SpeedCommunications] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Select( [EndTimeStamp] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Update( [EndTimeStamp] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Insert( [EndTimeStamp] ) ON [GeneralHistory]  TO  [CC4 User];
GRANT Select( [HistoryID] ) ON [History]  TO  [CC4 User];
GRANT Update( [HistoryID] ) ON [History]  TO  [CC4 User];
GRANT Insert( [HistoryID] ) ON [History]  TO  [CC4 User];
GRANT Select( [HistoryTimeStamp] ) ON [History]  TO  [CC4 User];
GRANT Update( [HistoryTimeStamp] ) ON [History]  TO  [CC4 User];
GRANT Insert( [HistoryTimeStamp] ) ON [History]  TO  [CC4 User];
GRANT Select( [Controller] ) ON [History]  TO  [CC4 User];
GRANT Update( [Controller] ) ON [History]  TO  [CC4 User];
GRANT Insert( [Controller] ) ON [History]  TO  [CC4 User];
GRANT Select( [HistoryErrorMsg] ) ON [History]  TO  [CC4 User];
GRANT Update( [HistoryErrorMsg] ) ON [History]  TO  [CC4 User];
GRANT Insert( [HistoryErrorMsg] ) ON [History]  TO  [CC4 User];
GRANT Select( [HistoryFunction] ) ON [History]  TO  [CC4 User];
GRANT Update( [HistoryFunction] ) ON [History]  TO  [CC4 User];
GRANT Insert( [HistoryFunction] ) ON [History]  TO  [CC4 User];
GRANT Select( [Communications] ) ON [History]  TO  [CC4 User];
GRANT Update( [Communications] ) ON [History]  TO  [CC4 User];
GRANT Insert( [Communications] ) ON [History]  TO  [CC4 User];
GRANT Select( [HistoryRoms] ) ON [History]  TO  [CC4 User];
GRANT Update( [HistoryRoms] ) ON [History]  TO  [CC4 User];
GRANT Insert( [HistoryRoms] ) ON [History]  TO  [CC4 User];
GRANT Select( [HistoryModel] ) ON [History]  TO  [CC4 User];
GRANT Update( [HistoryModel] ) ON [History]  TO  [CC4 User];
GRANT Insert( [HistoryModel] ) ON [History]  TO  [CC4 User];
GRANT Select( [HistorySuccess] ) ON [History]  TO  [CC4 User];
GRANT Update( [HistorySuccess] ) ON [History]  TO  [CC4 User];
GRANT Insert( [HistorySuccess] ) ON [History]  TO  [CC4 User];
GRANT Select( [HistoryErrorCode] ) ON [History]  TO  [CC4 User];
GRANT Update( [HistoryErrorCode] ) ON [History]  TO  [CC4 User];
GRANT Insert( [HistoryErrorCode] ) ON [History]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [History]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [History]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [History]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [History]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [History]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [History]  TO  [CC4 User];
GRANT Select( [CommServer] ) ON [History]  TO  [CC4 User];
GRANT Update( [CommServer] ) ON [History]  TO  [CC4 User];
GRANT Insert( [CommServer] ) ON [History]  TO  [CC4 User];
GRANT Select( [TaskID] ) ON [TaskData]  TO  [CC4 User];
GRANT Update( [TaskID] ) ON [TaskData]  TO  [CC4 User];
GRANT Insert( [TaskID] ) ON [TaskData]  TO  [CC4 User];
GRANT Select( [TaskName] ) ON [TaskData]  TO  [CC4 User];
GRANT Update( [TaskName] ) ON [TaskData]  TO  [CC4 User];
GRANT Insert( [TaskName] ) ON [TaskData]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [TaskData]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [TaskData]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [TaskData]  TO  [CC4 User];
GRANT Select( [CompanyID] ) ON [TaskData]  TO  [CC4 User];
GRANT Update( [CompanyID] ) ON [TaskData]  TO  [CC4 User];
GRANT Insert( [CompanyID] ) ON [TaskData]  TO  [CC4 User];
GRANT Select( [RefUnique] ) ON [TaskData]  TO  [CC4 User];
GRANT Update( [RefUnique] ) ON [TaskData]  TO  [CC4 User];
GRANT Insert( [RefUnique] ) ON [TaskData]  TO  [CC4 User];
GRANT Select( [RefTimeZone] ) ON [TaskData]  TO  [CC4 User];
GRANT Update( [RefTimeZone] ) ON [TaskData]  TO  [CC4 User];
GRANT Insert( [RefTimeZone] ) ON [TaskData]  TO  [CC4 User];
GRANT Select( [TaskTime] ) ON [TaskExclusion]  TO  [CC4 User];
GRANT Update( [TaskTime] ) ON [TaskExclusion]  TO  [CC4 User];
GRANT Insert( [TaskTime] ) ON [TaskExclusion]  TO  [CC4 User];
GRANT Select( [TaskDate] ) ON [TaskExclusion]  TO  [CC4 User];
GRANT Update( [TaskDate] ) ON [TaskExclusion]  TO  [CC4 User];
GRANT Insert( [TaskDate] ) ON [TaskExclusion]  TO  [CC4 User];
GRANT Select( [TaskID] ) ON [TaskExclusion]  TO  [CC4 User];
GRANT Update( [TaskID] ) ON [TaskExclusion]  TO  [CC4 User];
GRANT Insert( [TaskID] ) ON [TaskExclusion]  TO  [CC4 User];
GRANT Select( [OptionID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Update( [OptionID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Insert( [OptionID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Select( [TaskID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Update( [TaskID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Insert( [TaskID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Select( [Options] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Update( [Options] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Insert( [Options] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Select( [ServerID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Update( [ServerID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Insert( [ServerID] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Select( [CompletionTimestamp] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Update( [CompletionTimestamp] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Insert( [CompletionTimestamp] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Select( [TaskIndex] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Update( [TaskIndex] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Insert( [TaskIndex] ) ON [TaskOptions]  TO  [CC4 User];
GRANT Select( [StartDate] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Update( [StartDate] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Insert( [StartDate] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Select( [ScheduleTime] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Update( [ScheduleTime] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Insert( [ScheduleTime] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Select( [EndTimeStamp] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Update( [EndTimeStamp] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Insert( [EndTimeStamp] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Select( [Repeating] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Update( [Repeating] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Insert( [Repeating] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Select( [TaskID] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Update( [TaskID] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Insert( [TaskID] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Select( [TimeZone] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Update( [TimeZone] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Insert( [TimeZone] ) ON [TaskSchedule]  TO  [CC4 User];
GRANT Select( [UseETComparing] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [UseETComparing] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [UseETComparing] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [JanMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [JanMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [JanMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [JanShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [JanShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [JanShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [FebMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [FebMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [FebMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [FebShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [FebShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [FebShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [MarMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [MarMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [MarMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [MarShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [MarShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [MarShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [AprMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [AprMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [AprMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [AprShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [AprShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [AprShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [MayMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [MayMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [MayMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [MayShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [MayShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [MayShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [JuneMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [JuneMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [JuneMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [JuneShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [JuneShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [JuneShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [JulyMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [JulyMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [JulyMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [JulyShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [JulyShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [JulyShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [AugMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [AugMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [AugMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [AugShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [AugShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [AugShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [SepMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [SepMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [SepMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [SepShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [SepShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [SepShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [OctMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [OctMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [OctMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [OctShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [OctShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [OctShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [NovMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [NovMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [NovMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [NovShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [NovShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [NovShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [DecMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [DecMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [DecMinET] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [DecShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [DecShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [DecShareMin] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [Latitude] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [Latitude] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [Latitude] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [Longitude] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [Longitude] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [Longitude] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [UseSystemWideLatAndLong] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [UseSystemWideLatAndLong] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [UseSystemWideLatAndLong] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [ChangesEnabled] ) ON [ETComparing]  TO  [CC4 User];
GRANT Update( [ChangesEnabled] ) ON [ETComparing]  TO  [CC4 User];
GRANT Insert( [ChangesEnabled] ) ON [ETComparing]  TO  [CC4 User];
GRANT Select( [OptionID] ) ON [Fnctns]  TO  [CC4 User];
GRANT Update( [OptionID] ) ON [Fnctns]  TO  [CC4 User];
GRANT Insert( [OptionID] ) ON [Fnctns]  TO  [CC4 User];
GRANT Select( [OptionDescription] ) ON [Fnctns]  TO  [CC4 User];
GRANT Update( [OptionDescription] ) ON [Fnctns]  TO  [CC4 User];
GRANT Insert( [OptionDescription] ) ON [Fnctns]  TO  [CC4 User];
GRANT Select( [MinimumLevel] ) ON [Fnctns]  TO  [CC4 User];
GRANT Update( [MinimumLevel] ) ON [Fnctns]  TO  [CC4 User];
GRANT Insert( [MinimumLevel] ) ON [Fnctns]  TO  [CC4 User];
GRANT Select( [DialogIndex] ) ON [Fnctns]  TO  [CC4 User];
GRANT Update( [DialogIndex] ) ON [Fnctns]  TO  [CC4 User];
GRANT Insert( [DialogIndex] ) ON [Fnctns]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [Version] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [Version] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [Version] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CMOSSize] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CMOSSize] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CMOSSize] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [A_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [A_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [A_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [Protocol] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [Protocol] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [Protocol] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [Comm_Type] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [Comm_Type] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [Comm_Type] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [Number_Of_Stations] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [Number_Of_Stations] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [Number_Of_Stations] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [KeyPad] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [KeyPad] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [KeyPad] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [LightsEnabled] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [LightsEnabled] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [LightsEnabled] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [GInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [GInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [GInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [RBInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [RBInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [RBInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [FInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [FInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [FInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [DemoAllOptions] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [DemoAllOptions] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [DemoAllOptions] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [SizeOfRam] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [SizeOfRam] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [SizeOfRam] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [UseSOutDelay] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [UseSOutDelay] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [UseSOutDelay] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [NumOfRings] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [NumOfRings] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [NumOfRings] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [DTMF_Installed] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [DTMF_Installed] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [DTMF_Installed] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [WGInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [WGInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [WGInterface] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [UseCTS] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [UseCTS] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [UseCTS] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [BuzzerType] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [BuzzerType] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [BuzzerType] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [Reserved1] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [Reserved1] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [Reserved1] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [Reserved2] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [Reserved2] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [Reserved2] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [Reserved3] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [Reserved3] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [Reserved3] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [B_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [B_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [B_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [BoardRevision] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [BoardRevision] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [BoardRevision] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [Debuging] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [Debuging] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [Debuging] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [BadChecksum] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [BadChecksum] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [BadChecksum] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [BadVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [BadVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [BadVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [OM_Originator_Retries] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [OM_Originator_Retries] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [OM_Originator_Retries] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [OM_Seconds_for_Status_FOAL] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [OM_Seconds_for_Status_FOAL] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [OM_Seconds_for_Status_FOAL] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [OM_Minutes_To_Exist] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [OM_Minutes_To_Exist] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [OM_Minutes_To_Exist] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [FOAL_MinutesBetweenRescanAttempts] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [FOAL_MinutesBetweenRescanAttempts] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [FOAL_MinutesBetweenRescanAttempts] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [COMM_Address] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [COMM_Address] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [COMM_Address] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [NormalLevelsA] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [NormalLevelsA] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [NormalLevelsA] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [NormalLevelsB] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [NormalLevelsB] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [NormalLevelsB] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [NormalLevelsC] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [NormalLevelsC] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [NormalLevelsC] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [NormalLevelsD] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [NormalLevelsD] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [NormalLevelsD] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [C_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [C_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [C_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [D_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [D_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [D_BaudRate] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [StationsAllowed] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [StationsAllowed] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [StationsAllowed] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [GpmSlotSize] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [GpmSlotSize] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [GpmSlotSize] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [MaxStationsOn] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [MaxStationsOn] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [MaxStationsOn] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [NumberOfGpmSlots] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [NumberOfGpmSlots] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [NumberOfGpmSlots] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [NumberOfStationCycles] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [NumberOfStationCycles] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [NumberOfStationCycles] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CellIterations] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CellIterations] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CellIterations] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CentralMajorVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CentralMajorVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CentralMajorVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CentralMinorVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CentralMinorVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CentralMinorVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CentralReleaseVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CentralReleaseVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CentralReleaseVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CentralBuildVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CentralBuildVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CentralBuildVersion] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [ControllerSerialNumber] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [ControllerSerialNumber] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [ControllerSerialNumber] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CommTypeA] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CommTypeA] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CommTypeA] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CommTypeB] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CommTypeB] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CommTypeB] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CommTypeC] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CommTypeC] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CommTypeC] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CommTypeD] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [CommTypeD] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [CommTypeD] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [RRe_Enabled] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [RRe_Enabled] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [RRe_Enabled] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [ThisControllerIsAHub] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [ThisControllerIsAHub] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [ThisControllerIsAHub] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [PortASRIsAMaster] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [PortASRIsAMaster] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [PortASRIsAMaster] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [PortBSRIsAMaster] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [PortBSRIsAMaster] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [PortBSRIsAMaster] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [HysteresisPercentage] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [HysteresisPercentage] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [HysteresisPercentage] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [Spacer] ) ON [CMOS]  TO  [CC4 User];
GRANT Update( [Spacer] ) ON [CMOS]  TO  [CC4 User];
GRANT Insert( [Spacer] ) ON [CMOS]  TO  [CC4 User];
GRANT Select( [CommType] ) ON [CommWght]  TO  [CC4 User];
GRANT Update( [CommType] ) ON [CommWght]  TO  [CC4 User];
GRANT Insert( [CommType] ) ON [CommWght]  TO  [CC4 User];
GRANT Select( [CommWeight] ) ON [CommWght]  TO  [CC4 User];
GRANT Update( [CommWeight] ) ON [CommWght]  TO  [CC4 User];
GRANT Insert( [CommWeight] ) ON [CommWght]  TO  [CC4 User];
GRANT Select( [ServerID] ) ON [CommWght]  TO  [CC4 User];
GRANT Update( [ServerID] ) ON [CommWght]  TO  [CC4 User];
GRANT Insert( [ServerID] ) ON [CommWght]  TO  [CC4 User];
GRANT Select( [DWWeight] ) ON [CommWght]  TO  [CC4 User];
GRANT Update( [DWWeight] ) ON [CommWght]  TO  [CC4 User];
GRANT Insert( [DWWeight] ) ON [CommWght]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [StartTimeStamp] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [StartTimeStamp] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [StartTimeStamp] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [EndTimeStamp] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [EndTimeStamp] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [EndTimeStamp] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [HistoricalETNumber] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [HistoricalETNumber] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [HistoricalETNumber] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [GageET] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [GageET] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [GageET] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [GageETStatus] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [GageETStatus] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [GageETStatus] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [TotalRain] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [TotalRain] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [TotalRain] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [UsableRainStatus] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [UsableRainStatus] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [UsableRainStatus] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [UsableRainRain] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [UsableRainRain] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [UsableRainRain] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [Budget] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [Budget] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [Budget] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [TestSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [TestSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [TestSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [TestGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [TestGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [TestGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [ManualSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [ManualSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [ManualSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [ManualGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [ManualGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [ManualGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [RRSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [RRSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [RRSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [RRGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [RRGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [RRGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [NonControllerSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [NonControllerSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [NonControllerSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [NonControllerGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [NonControllerGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [NonControllerGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [ScheduledSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [ScheduledSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [ScheduledSeconds] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [ScheduledGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [ScheduledGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [ScheduledGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [FOALSystemGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Update( [FOALSystemGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Insert( [FOALSystemGallons] ) ON [DailyData]  TO  [CC4 User];
GRANT Select( [ScheduleTimeStamp] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Update( [ScheduleTimeStamp] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Insert( [ScheduleTimeStamp] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Select( [TaskID] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Update( [TaskID] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Insert( [TaskID] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Select( [CommServerDescription] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Update( [CommServerDescription] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Insert( [CommServerDescription] ) ON [PastSchedule]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [POCID] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [POCID] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [POCID] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [StartTimeStamp] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [StartTimeStamp] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [StartTimeStamp] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [EndTimeStamp] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [EndTimeStamp] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [EndTimeStamp] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [Name] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [Name] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [Name] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [IdleGallons] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [IdleGallons] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [IdleGallons] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [IdleSeconds] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [IdleSeconds] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [IdleSeconds] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [UseGallons] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [UseGallons] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [UseGallons] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [UseSeconds] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [UseSeconds] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [UseSeconds] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [IrriGallons] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [IrriGallons] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [IrriGallons] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [IrriSeconds] ) ON [POCReportData]  TO  [CC4 User];
GRANT Update( [IrriSeconds] ) ON [POCReportData]  TO  [CC4 User];
GRANT Insert( [IrriSeconds] ) ON [POCReportData]  TO  [CC4 User];
GRANT Select( [AlertID] ) ON [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Update( [AlertID] ) ON [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Insert( [AlertID] ) ON [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Select( [OptionFilterID] ) ON [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Update( [OptionFilterID] ) ON [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Insert( [OptionFilterID] ) ON [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Select( [VariableName] ) ON [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Update( [VariableName] ) ON [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Insert( [VariableName] ) ON [AlertFilterOptionDetails]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [Assignment]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [Assignment]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [Assignment]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [Assignment]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [Assignment]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [Assignment]  TO  [CC4 User];
GRANT Select( [GatheredDate] ) ON [Assignment]  TO  [CC4 User];
GRANT Update( [GatheredDate] ) ON [Assignment]  TO  [CC4 User];
GRANT Insert( [GatheredDate] ) ON [Assignment]  TO  [CC4 User];
GRANT Select( [Viewed] ) ON [Assignment]  TO  [CC4 User];
GRANT Update( [Viewed] ) ON [Assignment]  TO  [CC4 User];
GRANT Insert( [Viewed] ) ON [Assignment]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [ETOption] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [ETOption] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [ETOption] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [ETCountyIndex] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [ETCountyIndex] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [ETCountyIndex] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [ETCityIndex] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [ETCityIndex] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [ETCityIndex] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [RunWeek] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [RunWeek] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [RunWeek] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [PumpUsage] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [PumpUsage] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [PumpUsage] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [FlowMeter] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [FlowMeter] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [FlowMeter] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [TypeOfMV] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [TypeOfMV] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [TypeOfMV] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [LearnGPMs] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [LearnGPMs] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [LearnGPMs] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [IrrigMLB] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [IrrigMLB] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [IrrigMLB] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [NonIrrigMLB] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [NonIrrigMLB] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [NonIrrigMLB] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [MainLineBreak] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [MainLineBreak] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [MainLineBreak] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [FullYear] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [FullYear] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [FullYear] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Password1] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Password1] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Password1] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Password2] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Password2] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Password2] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Password3] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Password3] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Password3] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Password] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Password] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Password] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [MasterUnit] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [MasterUnit] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [MasterUnit] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [MasterMLB] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [MasterMLB] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [MasterMLB] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [ControllerOff] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [ControllerOff] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [ControllerOff] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [CommAddr] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [CommAddr] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [CommAddr] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [LightsDef] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [LightsDef] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [LightsDef] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [RunTempProg] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [RunTempProg] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [RunTempProg] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [RunTempTill] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [RunTempTill] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [RunTempTill] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [UseBudget] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [UseBudget] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [UseBudget] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [BudgetOption] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [BudgetOption] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [BudgetOption] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [PercentOfETo] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [PercentOfETo] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [PercentOfETo] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [UseDailyET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [UseDailyET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [UseDailyET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [DailyETDate] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [DailyETDate] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [DailyETDate] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [UsingAnETGage] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [UsingAnETGage] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [UsingAnETGage] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [ComBaudRate] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [ComBaudRate] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [ComBaudRate] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [WS_InUse] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [WS_InUse] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [WS_InUse] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [WS_Pause] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [WS_Pause] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [WS_Pause] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [WS_P_Speed] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [WS_P_Speed] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [WS_P_Speed] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [WS_R_Speed] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [WS_R_Speed] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [WS_R_Speed] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [WS_P_Time] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [WS_P_Time] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [WS_P_Time] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [WS_R_Time] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [WS_R_Time] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [WS_R_Time] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_Demonstrator] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_Demonstrator] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_Demonstrator] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_UsingPager] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_UsingPager] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_UsingPager] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_FM_FM] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_FM_FM] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_FM_FM] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_FM_ET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_FM_ET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_FM_ET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_RG_ET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_RG_ET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_RG_ET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_WS_ET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_WS_ET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_WS_ET] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_D1] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_D1] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_D1] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_D2] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_D2] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_D2] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_D3] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_D3] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_D3] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [Jumpers_D4] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [Jumpers_D4] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [Jumpers_D4] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [LogETPulses] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [LogETPulses] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [LogETPulses] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [DLSavings] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Update( [DLSavings] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Insert( [DLSavings] ) ON [BulkPgmData1]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainSwitchInUse] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainSwitchInUse] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainSwitchInUse] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [EnableHOTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [EnableHOTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [EnableHOTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [HOTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [HOTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [HOTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainInfo_InUse] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainInfo_InUse] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainInfo_InUse] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainInfo_HookedUp] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainInfo_HookedUp] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainInfo_HookedUp] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainInfo_MaxHourlyRate] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainInfo_MaxHourlyRate] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainInfo_MaxHourlyRate] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainInfo_Maximum] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainInfo_Maximum] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainInfo_Maximum] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainInfo_Minimum] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainInfo_Minimum] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainInfo_Minimum] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainInfo_Status] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainInfo_Status] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainInfo_Status] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainInfo_Dummy2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainInfo_Dummy2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainInfo_Dummy2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainInfo_RBTableDate] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainInfo_RBTableDate] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainInfo_RBTableDate] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [RainInfo_Dummy4] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [RainInfo_Dummy4] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [RainInfo_Dummy4] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [CentralRadio] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [CentralRadio] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [CentralRadio] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [XFlowMeter_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [XFlowMeter_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [XFlowMeter_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [XFlowMeter_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [XFlowMeter_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [XFlowMeter_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [MaxFlowNumber] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [MaxFlowNumber] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [MaxFlowNumber] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [StationOrder] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [StationOrder] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [StationOrder] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [ManCycleSoak] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [ManCycleSoak] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [ManCycleSoak] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [DailyETUse12] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [DailyETUse12] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [DailyETUse12] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [AllowNegHO_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [AllowNegHO_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [AllowNegHO_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [AllowNegHO_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [AllowNegHO_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [AllowNegHO_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [AllowNegHO_3] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [AllowNegHO_3] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [AllowNegHO_3] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [AllowNegHO_4] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [AllowNegHO_4] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [AllowNegHO_4] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [AllowNegHO_5] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [AllowNegHO_5] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [AllowNegHO_5] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [AllowNegHO_6] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [AllowNegHO_6] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [AllowNegHO_6] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [AllowNegHO_7] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [AllowNegHO_7] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [AllowNegHO_7] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [MaxInchesOfRainHO] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [MaxInchesOfRainHO] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [MaxInchesOfRainHO] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [FME_K_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [FME_K_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [FME_K_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [FME_K_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [FME_K_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [FME_K_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [FME_K_3] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [FME_K_3] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [FME_K_3] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [FME_O_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [FME_O_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [FME_O_1] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [FME_O_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [FME_O_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [FME_O_2] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [FME_O_3] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [FME_O_3] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [FME_O_3] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [FM_Connected] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [FM_Connected] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [FM_Connected] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [FMEnterOwn] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [FMEnterOwn] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [FMEnterOwn] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [VariableCCInUse] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [VariableCCInUse] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [VariableCCInUse] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [ReportsRollTimeEnabled] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [ReportsRollTimeEnabled] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [ReportsRollTimeEnabled] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [ReportsRollTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [ReportsRollTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [ReportsRollTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [LogAddress] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [LogAddress] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [LogAddress] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [DiagAddress] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [DiagAddress] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [DiagAddress] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [ActiveStations] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [ActiveStations] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [ActiveStations] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [EESize] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [EESize] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [EESize] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [ControllerDateTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [ControllerDateTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [ControllerDateTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [CentralDateTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [CentralDateTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [CentralDateTime] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [SWVersion] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [SWVersion] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [SWVersion] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [CommunicationsType] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [CommunicationsType] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [CommunicationsType] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [ExistsOnController] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [ExistsOnController] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [ExistsOnController] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [UsePassword] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Update( [UsePassword] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Insert( [UsePassword] ) ON [BulkPgmData2]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [IncludeTime] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [IncludeTime] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [IncludeTime] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UseETAveraging_1] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UseETAveraging_1] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UseETAveraging_1] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UseETAveraging_2] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UseETAveraging_2] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UseETAveraging_2] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UseETAveraging_3] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UseETAveraging_3] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UseETAveraging_3] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UseETAveraging_4] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UseETAveraging_4] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UseETAveraging_4] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UseETAveraging_5] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UseETAveraging_5] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UseETAveraging_5] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UseETAveraging_6] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UseETAveraging_6] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UseETAveraging_6] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UseETAveraging_7] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UseETAveraging_7] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UseETAveraging_7] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [DTMF_Mode1Addr] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [DTMF_Mode1Addr] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [DTMF_Mode1Addr] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [DTMF_Mode2Addr] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [DTMF_Mode2Addr] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [DTMF_Mode2Addr] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [BacklightState] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [BacklightState] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [BacklightState] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [MSInUse] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [MSInUse] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [MSInUse] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [DTMFChannel1] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [DTMFChannel1] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [DTMFChannel1] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [DTMFChannel2] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [DTMFChannel2] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [DTMFChannel2] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [TrackEstimatedUsage] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [TrackEstimatedUsage] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [TrackEstimatedUsage] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [Deleted] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [Deleted] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [Deleted] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UseYourOwnNumbers] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UseYourOwnNumbers] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UseYourOwnNumbers] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [POAFS] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [POAFS] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [POAFS] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [NOCIC] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [NOCIC] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [NOCIC] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [HighFlowMargin] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [HighFlowMargin] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [HighFlowMargin] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [LowFlowMargin] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [LowFlowMargin] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [LowFlowMargin] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [Capacity_NonPump] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [Capacity_NonPump] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [Capacity_NonPump] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [IrrigatingByCapacity] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [IrrigatingByCapacity] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [IrrigatingByCapacity] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [TestingFlow] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [TestingFlow] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [TestingFlow] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowRange1Top] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowRange1Top] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowRange1Top] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowRange2Top] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowRange2Top] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowRange2Top] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowRange3Top] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowRange3Top] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowRange3Top] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowTolerance1Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowTolerance1Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowTolerance1Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowTolerance1Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowTolerance1Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowTolerance1Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowTolerance2Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowTolerance2Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowTolerance2Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowTolerance2Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowTolerance2Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowTolerance2Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowTolerance3Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowTolerance3Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowTolerance3Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowTolerance3Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowTolerance3Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowTolerance3Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowTolerance4Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowTolerance4Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowTolerance4Plus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowTolerance4Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowTolerance4Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowTolerance4Minus] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [TailEndsThrowAwayUpperbound] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [TailEndsThrowAwayUpperbound] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [TailEndsThrowAwayUpperbound] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [TailEndsMakeAdjustmentUpperbound] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [TailEndsMakeAdjustmentUpperbound] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [TailEndsMakeAdjustmentUpperbound] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [IrriElectricalStationLimit] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [IrriElectricalStationLimit] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [IrriElectricalStationLimit] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowLength] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowLength] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowLength] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [ChainDown] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [ChainDown] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [ChainDown] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowMeterAvailableElsewhere] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowMeterAvailableElsewhere] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowMeterAvailableElsewhere] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [MasterHasIrrigateByCapacity] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [MasterHasIrrigateByCapacity] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [MasterHasIrrigateByCapacity] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [MasterHasFlowChecking] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [MasterHasFlowChecking] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [MasterHasFlowChecking] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [FlowInfoValid] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [FlowInfoValid] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [FlowInfoValid] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [ProgramPrioritiesEnabled] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [ProgramPrioritiesEnabled] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [ProgramPrioritiesEnabled] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UserDefinedETState] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UserDefinedETState] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UserDefinedETState] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UserDefinedETCounty] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UserDefinedETCounty] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UserDefinedETCounty] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [UserDefinedETCity] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Update( [UserDefinedETCity] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Insert( [UserDefinedETCity] ) ON [BulkPgmData3]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [CreatedByRReInterface] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [CreatedByRReInterface] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [CreatedByRReInterface] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [PTagsEditableAtController] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [PTagsEditableAtController] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [PTagsEditableAtController] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [RReChannelChangeMode] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [RReChannelChangeMode] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [RReChannelChangeMode] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [RReControllerChannel] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [RReControllerChannel] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [RReControllerChannel] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [RReUserChannel] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [RReUserChannel] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [RReUserChannel] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [DMCInUse] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [DMCInUse] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [DMCInUse] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [DMCLevels] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [DMCLevels] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [DMCLevels] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [DMCMaxFlowRateLevel1] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [DMCMaxFlowRateLevel1] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [DMCMaxFlowRateLevel1] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [DMCMaxFlowRateLevel2] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [DMCMaxFlowRateLevel2] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [DMCMaxFlowRateLevel2] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [MLBMVOR] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [MLBMVOR] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [MLBMVOR] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [MaximumHoldoverFactor] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [MaximumHoldoverFactor] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [MaximumHoldoverFactor] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [SICFunctionalityName] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [SICFunctionalityName] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [SICFunctionalityName] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [SICContactType] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [SICContactType] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [SICContactType] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [SICIrrigationToAffect] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [SICIrrigationToAffect] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [SICIrrigationToAffect] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [SICActionToTake] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [SICActionToTake] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [SICActionToTake] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [SICSwitchName] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [SICSwitchName] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [SICSwitchName] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [YearlyBudget] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Update( [YearlyBudget] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Insert( [YearlyBudget] ) ON [BulkPgmData4]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [DataMonth] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [DataMonth] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [DataMonth] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [Budgets] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [Budgets] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [Budgets] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [ETNumber] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [ETNumber] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [ETNumber] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [FTimesMax] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [FTimesMax] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [FTimesMax] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCs] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCs] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCs] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [Schedule] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [Schedule] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [Schedule] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramA] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramA] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramA] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramAEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramAEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramAEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramB] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramB] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramB] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramBEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramBEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramBEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramC] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramC] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramC] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramCEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramCEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramCEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramD] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramD] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramD] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramDEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramDEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramDEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramE] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramE] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramE] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramEEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramEEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramEEnabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramD1] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramD1] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramD1] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramD1Enabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramD1Enabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramD1Enabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramD2] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramD2] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramD2] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [SX_ProgramD2Enabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [SX_ProgramD2Enabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [SX_ProgramD2Enabled] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [UserETNumbers] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Update( [UserETNumbers] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Insert( [UserETNumbers] ) ON [MonthlyPrgmData]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [POCID] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [POCID] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [POCID] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [POC_Name] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [POC_Name] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [POC_Name] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [PartOfIrrigation] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [PartOfIrrigation] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [PartOfIrrigation] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [AllowIrriDuringMLB] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [AllowIrriDuringMLB] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [AllowIrriDuringMLB] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [MVType] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [MVType] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [MVType] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [FMInUse] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [FMInUse] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [FMInUse] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [FMUseKAndO] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [FMUseKAndO] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [FMUseKAndO] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [MLBDuringIdle] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [MLBDuringIdle] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [MLBDuringIdle] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [MLBDuringUse] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [MLBDuringUse] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [MLBDuringUse] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [MLBDuringIrri] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Update( [MLBDuringIrri] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Insert( [MLBDuringIrri] ) ON [POCPrgmData]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [POCID] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [POCID] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [POCID] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [POCDay] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [POCDay] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [POCDay] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [OpenTimeAEnabled] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [OpenTimeAEnabled] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [OpenTimeAEnabled] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [OpenTimeA] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [OpenTimeA] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [OpenTimeA] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [DurationA] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [DurationA] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [DurationA] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [OpenTimeBEnabled] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [OpenTimeBEnabled] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [OpenTimeBEnabled] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [OpenTimeB] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [OpenTimeB] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [OpenTimeB] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [DurationB] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Update( [DurationB] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Insert( [DurationB] ) ON [POCSchedule]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [DataProgram] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [DataProgram] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [DataProgram] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [NoFlowShutOff] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [NoFlowShutOff] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [NoFlowShutOff] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [PumpByProgram] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [PumpByProgram] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [PumpByProgram] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [TripPercent] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [TripPercent] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [TripPercent] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [ValveTime] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [ValveTime] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [ValveTime] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [UseVariableCC] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [UseVariableCC] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [UseVariableCC] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [OverFlowShutOff] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [OverFlowShutOff] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [OverFlowShutOff] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_Jan] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_Jan] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_Jan] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_Feb] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_Feb] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_Feb] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_Mar] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_Mar] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_Mar] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_Apr] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_Apr] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_Apr] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_May] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_May] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_May] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_June] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_June] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_June] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_July] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_July] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_July] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_Aug] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_Aug] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_Aug] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_Sep] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_Sep] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_Sep] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_Oct] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_Oct] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_Oct] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_Nov] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_Nov] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_Nov] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [VariableCCByProg_Dec] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [VariableCCByProg_Dec] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [VariableCCByProg_Dec] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [PTags] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [PTags] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [PTags] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [MSByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [MSByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [MSByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [DailyETByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [DailyETByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [DailyETByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [HoldOverEnabledByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [HoldOverEnabledByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [HoldOverEnabledByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [HoldOverByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [HoldOverByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [HoldOverByProg] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [ValvesOnByProgram] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [ValvesOnByProgram] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [ValvesOnByProgram] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [SetExpected] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [SetExpected] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [SetExpected] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [ValveCloseTime] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [ValveCloseTime] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [ValveCloseTime] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [ValvesOnInSystem] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [ValvesOnInSystem] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [ValvesOnInSystem] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [WindInUse] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [WindInUse] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [WindInUse] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [ProgramPriority] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [ProgramPriority] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [ProgramPriority] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [UserTag] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [UserTag] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [UserTag] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [PercentAdjust1] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [PercentAdjust1] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [PercentAdjust1] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [PercentAdjustNumberOfDays1] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Update( [PercentAdjustNumberOfDays1] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Insert( [PercentAdjustNumberOfDays1] ) ON [ProgramPrgmData]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [StationNumber] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [StationNumber] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [StationNumber] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [StationInUse] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [StationInUse] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [StationInUse] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [StationInfo_Size] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [StationInfo_Size] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [StationInfo_Size] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [StationInfo_Rate] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [StationInfo_Rate] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [StationInfo_Rate] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [LorL] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [LorL] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [LorL] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [FlowStatus] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [FlowStatus] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [FlowStatus] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [IStatus] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [IStatus] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [IStatus] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [NoWaterDays] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [NoWaterDays] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [NoWaterDays] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [NoWaterToday] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [NoWaterToday] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [NoWaterToday] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Temp_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Temp_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Temp_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Temp_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Temp_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Temp_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Temp_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Temp_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Temp_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Jan_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Jan_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Jan_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Jan_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Jan_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Jan_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Jan_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Jan_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Jan_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Feb_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Feb_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Feb_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Feb_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Feb_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Feb_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Feb_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Feb_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Feb_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Mar_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Mar_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Mar_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Mar_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Mar_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Mar_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Mar_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Mar_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Mar_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Apr_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Apr_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Apr_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Apr_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Apr_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Apr_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Apr_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Apr_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Apr_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_May_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_May_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_May_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_May_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_May_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_May_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_May_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_May_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_May_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_June_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_June_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_June_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_June_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_June_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_June_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_June_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_June_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_June_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_July_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_July_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_July_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_July_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_July_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_July_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_July_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_July_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_July_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Aug_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Aug_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Aug_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Aug_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Aug_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Aug_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Aug_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Aug_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Aug_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Sep_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Sep_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Sep_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Sep_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Sep_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Sep_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Sep_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Sep_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Sep_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Oct_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Oct_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Oct_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Oct_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Oct_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Oct_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Oct_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Oct_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Oct_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Nov_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Nov_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Nov_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Nov_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Nov_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Nov_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Nov_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Nov_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Nov_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Dec_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Dec_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Dec_PTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Dec_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Dec_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Dec_HTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [EditSVar_Dec_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [EditSVar_Dec_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [EditSVar_Dec_CTime] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [PAssign_Temp] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [PAssign_Temp] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [PAssign_Temp] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [PAssign_Norm] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [PAssign_Norm] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [PAssign_Norm] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [Name] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [Name] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [Name] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [DailyETFactor] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [DailyETFactor] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [DailyETFactor] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [LowFlowLimits] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [LowFlowLimits] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [LowFlowLimits] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [MSSensorAssignment] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [MSSensorAssignment] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [MSSensorAssignment] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [MSSetPoint] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [MSSetPoint] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [MSSetPoint] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [MSReading] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [MSReading] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [MSReading] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [MSReadingStatus] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [MSReadingStatus] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [MSReadingStatus] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [MSMaxWaterDays] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Update( [MSMaxWaterDays] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Insert( [MSMaxWaterDays] ) ON [StationPgmData]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [TagNames]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [TagNames]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [TagNames]  TO  [CC4 User];
GRANT Select( [PDataTimeStamp] ) ON [TagNames]  TO  [CC4 User];
GRANT Update( [PDataTimeStamp] ) ON [TagNames]  TO  [CC4 User];
GRANT Insert( [PDataTimeStamp] ) ON [TagNames]  TO  [CC4 User];
GRANT Select( [TagIndex] ) ON [TagNames]  TO  [CC4 User];
GRANT Update( [TagIndex] ) ON [TagNames]  TO  [CC4 User];
GRANT Insert( [TagIndex] ) ON [TagNames]  TO  [CC4 User];
GRANT Select( [TagNames] ) ON [TagNames]  TO  [CC4 User];
GRANT Update( [TagNames] ) ON [TagNames]  TO  [CC4 User];
GRANT Insert( [TagNames] ) ON [TagNames]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [PgmTimeStamp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [PgmTimeStamp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [PgmTimeStamp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [DataDate] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [DataDate] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [DataDate] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [DailyET_ET] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [DailyET_ET] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [DailyET_ET] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [DailyET_Status] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [DailyET_Status] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [DailyET_Status] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [RainTable_Status] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [RainTable_Status] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [RainTable_Status] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [RainTable_Pad] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [RainTable_Pad] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [RainTable_Pad] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [RainTable_Rain] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [RainTable_Rain] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [RainTable_Rain] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Temp] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Jan] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Feb] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Mar] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Apr] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_May] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_June] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_July] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Aug] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Sep] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Oct] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Nov] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgA_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgA_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgA_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgB_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgB_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgB_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgC_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgC_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgC_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgE_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgE_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgE_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD1_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD1_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD1_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [WDay_ProgD2_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Update( [WDay_ProgD2_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Insert( [WDay_ProgD2_Dec] ) ON [TwentyEightDayPrgmData]  TO  [CC4 User];
GRANT Select( [CityIndex] ) ON [City]  TO  [CC4 User];
GRANT Update( [CityIndex] ) ON [City]  TO  [CC4 User];
GRANT Insert( [CityIndex] ) ON [City]  TO  [CC4 User];
GRANT Select( [CountyIndex] ) ON [City]  TO  [CC4 User];
GRANT Update( [CountyIndex] ) ON [City]  TO  [CC4 User];
GRANT Insert( [CountyIndex] ) ON [City]  TO  [CC4 User];
GRANT Select( [CityName] ) ON [City]  TO  [CC4 User];
GRANT Update( [CityName] ) ON [City]  TO  [CC4 User];
GRANT Insert( [CityName] ) ON [City]  TO  [CC4 User];
GRANT Select( [JanET] ) ON [City]  TO  [CC4 User];
GRANT Update( [JanET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [JanET] ) ON [City]  TO  [CC4 User];
GRANT Select( [FebET] ) ON [City]  TO  [CC4 User];
GRANT Update( [FebET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [FebET] ) ON [City]  TO  [CC4 User];
GRANT Select( [MarET] ) ON [City]  TO  [CC4 User];
GRANT Update( [MarET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [MarET] ) ON [City]  TO  [CC4 User];
GRANT Select( [AprilET] ) ON [City]  TO  [CC4 User];
GRANT Update( [AprilET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [AprilET] ) ON [City]  TO  [CC4 User];
GRANT Select( [MayET] ) ON [City]  TO  [CC4 User];
GRANT Update( [MayET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [MayET] ) ON [City]  TO  [CC4 User];
GRANT Select( [JuneET] ) ON [City]  TO  [CC4 User];
GRANT Update( [JuneET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [JuneET] ) ON [City]  TO  [CC4 User];
GRANT Select( [JulyET] ) ON [City]  TO  [CC4 User];
GRANT Update( [JulyET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [JulyET] ) ON [City]  TO  [CC4 User];
GRANT Select( [AugET] ) ON [City]  TO  [CC4 User];
GRANT Update( [AugET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [AugET] ) ON [City]  TO  [CC4 User];
GRANT Select( [SeptET] ) ON [City]  TO  [CC4 User];
GRANT Update( [SeptET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [SeptET] ) ON [City]  TO  [CC4 User];
GRANT Select( [OctET] ) ON [City]  TO  [CC4 User];
GRANT Update( [OctET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [OctET] ) ON [City]  TO  [CC4 User];
GRANT Select( [NovET] ) ON [City]  TO  [CC4 User];
GRANT Update( [NovET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [NovET] ) ON [City]  TO  [CC4 User];
GRANT Select( [DecET] ) ON [City]  TO  [CC4 User];
GRANT Update( [DecET] ) ON [City]  TO  [CC4 User];
GRANT Insert( [DecET] ) ON [City]  TO  [CC4 User];
GRANT Select( [CountyIndex] ) ON [County]  TO  [CC4 User];
GRANT Update( [CountyIndex] ) ON [County]  TO  [CC4 User];
GRANT Insert( [CountyIndex] ) ON [County]  TO  [CC4 User];
GRANT Select( [CountyName] ) ON [County]  TO  [CC4 User];
GRANT Update( [CountyName] ) ON [County]  TO  [CC4 User];
GRANT Insert( [CountyName] ) ON [County]  TO  [CC4 User];
GRANT Select( [StateIndex] ) ON [County]  TO  [CC4 User];
GRANT Update( [StateIndex] ) ON [County]  TO  [CC4 User];
GRANT Insert( [StateIndex] ) ON [County]  TO  [CC4 User];
GRANT Select( [PrgmTagIndex] ) ON [PrgmTags]  TO  [CC4 User];
GRANT Update( [PrgmTagIndex] ) ON [PrgmTags]  TO  [CC4 User];
GRANT Insert( [PrgmTagIndex] ) ON [PrgmTags]  TO  [CC4 User];
GRANT Select( [PrgmTagName] ) ON [PrgmTags]  TO  [CC4 User];
GRANT Update( [PrgmTagName] ) ON [PrgmTags]  TO  [CC4 User];
GRANT Insert( [PrgmTagName] ) ON [PrgmTags]  TO  [CC4 User];
GRANT Select( [CompanyID] ) ON [PrgmTags]  TO  [CC4 User];
GRANT Update( [CompanyID] ) ON [PrgmTags]  TO  [CC4 User];
GRANT Insert( [CompanyID] ) ON [PrgmTags]  TO  [CC4 User];
GRANT Select( [StateIndex] ) ON [States]  TO  [CC4 User];
GRANT Update( [StateIndex] ) ON [States]  TO  [CC4 User];
GRANT Insert( [StateIndex] ) ON [States]  TO  [CC4 User];
GRANT Select( [StateName] ) ON [States]  TO  [CC4 User];
GRANT Update( [StateName] ) ON [States]  TO  [CC4 User];
GRANT Insert( [StateName] ) ON [States]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [MonthET]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [MonthET]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [MonthET]  TO  [CC4 User];
GRANT Select( [ETMonth] ) ON [MonthET]  TO  [CC4 User];
GRANT Update( [ETMonth] ) ON [MonthET]  TO  [CC4 User];
GRANT Insert( [ETMonth] ) ON [MonthET]  TO  [CC4 User];
GRANT Select( [ETData] ) ON [MonthET]  TO  [CC4 User];
GRANT Update( [ETData] ) ON [MonthET]  TO  [CC4 User];
GRANT Insert( [ETData] ) ON [MonthET]  TO  [CC4 User];
GRANT Select( [DateTime] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Update( [DateTime] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Insert( [DateTime] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Select( [CompanyID] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Update( [CompanyID] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Insert( [CompanyID] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Select( [CategoryID] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Update( [CategoryID] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Insert( [CategoryID] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Select( [Expiration] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Update( [Expiration] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Insert( [Expiration] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Select( [Heading] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Update( [Heading] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Insert( [Heading] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Select( [Contents] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Update( [Contents] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Insert( [Contents] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Select( [Enabled] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Update( [Enabled] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Insert( [Enabled] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [StatusMessages]  TO  [CC4 User];
GRANT Select( [ID] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Update( [ID] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Insert( [ID] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Select( [Description] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Update( [Description] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Insert( [Description] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Select( [CssClass] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Update( [CssClass] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Insert( [CssClass] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Select( [Priority] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Update( [Priority] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Insert( [Priority] ) ON [StatusMessageCategories]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [RainShare]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [RainShare]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [RainShare]  TO  [CC4 User];
GRANT Select( [RainDate] ) ON [RainShare]  TO  [CC4 User];
GRANT Update( [RainDate] ) ON [RainShare]  TO  [CC4 User];
GRANT Insert( [RainDate] ) ON [RainShare]  TO  [CC4 User];
GRANT Select( [Rain] ) ON [RainShare]  TO  [CC4 User];
GRANT Update( [Rain] ) ON [RainShare]  TO  [CC4 User];
GRANT Insert( [Rain] ) ON [RainShare]  TO  [CC4 User];
GRANT Select( [RainStatus] ) ON [RainShare]  TO  [CC4 User];
GRANT Update( [RainStatus] ) ON [RainShare]  TO  [CC4 User];
GRANT Insert( [RainStatus] ) ON [RainShare]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [ETShare]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [ETShare]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [ETShare]  TO  [CC4 User];
GRANT Select( [ETDate] ) ON [ETShare]  TO  [CC4 User];
GRANT Update( [ETDate] ) ON [ETShare]  TO  [CC4 User];
GRANT Insert( [ETDate] ) ON [ETShare]  TO  [CC4 User];
GRANT Select( [ET] ) ON [ETShare]  TO  [CC4 User];
GRANT Update( [ET] ) ON [ETShare]  TO  [CC4 User];
GRANT Insert( [ET] ) ON [ETShare]  TO  [CC4 User];
GRANT Select( [ETStatus] ) ON [ETShare]  TO  [CC4 User];
GRANT Update( [ETStatus] ) ON [ETShare]  TO  [CC4 User];
GRANT Insert( [ETStatus] ) ON [ETShare]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [AssignmentSites]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [AssignmentSites]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [AssignmentSites]  TO  [CC4 User];
GRANT Select( [SiteID] ) ON [AssignmentSites]  TO  [CC4 User];
GRANT Update( [SiteID] ) ON [AssignmentSites]  TO  [CC4 User];
GRANT Insert( [SiteID] ) ON [AssignmentSites]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Select( [ActivityTimeStamp] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Update( [ActivityTimeStamp] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Insert( [ActivityTimeStamp] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Select( [Activity1] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Update( [Activity1] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Insert( [Activity1] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Select( [Activity2] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Update( [Activity2] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Insert( [Activity2] ) ON [ActivityLog]  TO  [CC4 User];
GRANT Select( [IPAddress] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [IPAddress] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [IPAddress] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [IPPort] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [IPPort] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [IPPort] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [CommName] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [CommName] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [CommName] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [ModemAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [ModemAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [ModemAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [ATAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [ATAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [ATAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [MASCAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [MASCAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [MASCAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [HardWireAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [HardWireAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [HardWireAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [Communicating] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [Communicating] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [Communicating] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [LastStarted] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [LastStarted] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [LastStarted] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [ServerID] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [ServerID] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [ServerID] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [CurrentController] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [CurrentController] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [CurrentController] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [LastActivity] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [LastActivity] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [LastActivity] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [ControllerRetrys] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [ControllerRetrys] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [ControllerRetrys] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [CommandRetrys] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [CommandRetrys] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [CommandRetrys] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [EthernetAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [EthernetAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [EthernetAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [NumberOfControllers] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [NumberOfControllers] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [NumberOfControllers] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [Online] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [Online] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [Online] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [Version] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [Version] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [Version] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [HostName] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [HostName] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [HostName] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [GPRSAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [GPRSAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [GPRSAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [FiberOpticModemAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [FiberOpticModemAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [FiberOpticModemAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [InternetAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [InternetAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [InternetAvailable] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [ConnectedViaInternet] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Update( [ConnectedViaInternet] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Insert( [ConnectedViaInternet] ) ON [ComSrvrs]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [FlowTimeStamp] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [FlowTimeStamp] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [FlowTimeStamp] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [Controller] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [Controller] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [Controller] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [Station] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [Station] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [Station] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [Expected] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [Expected] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [Expected] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [DeratedExpected] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [DeratedExpected] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [DeratedExpected] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [HiLimit] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [HiLimit] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [HiLimit] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [LoLimit] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [LoLimit] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [LoLimit] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [PortionOfActual] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [PortionOfActual] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [PortionOfActual] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [Flag] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [Flag] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [Flag] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [FlowData] ) ON [FlowRec]  TO  [CC4 User];
GRANT Update( [FlowData] ) ON [FlowRec]  TO  [CC4 User];
GRANT Insert( [FlowData] ) ON [FlowRec]  TO  [CC4 User];
GRANT Select( [PollingEnabled] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollingEnabled] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollingEnabled] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollOnSunday] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollOnSunday] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollOnSunday] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollOnMonday] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollOnMonday] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollOnMonday] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollOnTuesday] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollOnTuesday] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollOnTuesday] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollOnWednesday] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollOnWednesday] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollOnWednesday] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollOnThursday] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollOnThursday] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollOnThursday] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollOnFriday] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollOnFriday] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollOnFriday] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollOnSaturday] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollOnSaturday] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollOnSaturday] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollStart] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollStart] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollStart] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollStop] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollStop] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollStop] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [PollFrequency] ) ON [Polling]  TO  [CC4 User];
GRANT Update( [PollFrequency] ) ON [Polling]  TO  [CC4 User];
GRANT Insert( [PollFrequency] ) ON [Polling]  TO  [CC4 User];
GRANT Select( [ControllerID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Update( [ControllerID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Insert( [ControllerID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Select( [CompletionTime] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Update( [CompletionTime] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Insert( [CompletionTime] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Select( [PollingTaskID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Update( [PollingTaskID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Insert( [PollingTaskID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Select( [ShutdownTaskID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Update( [ShutdownTaskID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Insert( [ShutdownTaskID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Select( [UserID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Update( [UserID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Insert( [UserID] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Select( [TableDate] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Update( [TableDate] ) ON [PollingControllers]  TO  [CC4 User];
GRANT Insert( [TableDate] ) ON [PollingControllers]  TO  [CC4 User];
EXECUTE PROCEDURE sp_ModifyPermission( 11, NULL, NULL, 'CC4 User', 2147483904, FALSE );

GRANT Select ON  [Companies]  TO  [DB:Public];
GRANT Update ON  [Companies]  TO  [DB:Public];
GRANT Insert ON  [Companies]  TO  [DB:Public];
GRANT Delete ON  [Companies]  TO  [DB:Public];
GRANT Select ON  [Usrs]  TO  [DB:Public];
GRANT Update ON  [Usrs]  TO  [DB:Public];
GRANT Insert ON  [Usrs]  TO  [DB:Public];
GRANT Delete ON  [Usrs]  TO  [DB:Public];
GRANT Select ON  [AlrtFltrGrp]  TO  [DB:Public];
GRANT Update ON  [AlrtFltrGrp]  TO  [DB:Public];
GRANT Insert ON  [AlrtFltrGrp]  TO  [DB:Public];
GRANT Delete ON  [AlrtFltrGrp]  TO  [DB:Public];
GRANT Select ON  [ControllerSites]  TO  [DB:Public];
GRANT Update ON  [ControllerSites]  TO  [DB:Public];
GRANT Insert ON  [ControllerSites]  TO  [DB:Public];
GRANT Delete ON  [ControllerSites]  TO  [DB:Public];
GRANT Select ON  [Controllers]  TO  [DB:Public];
GRANT Update ON  [Controllers]  TO  [DB:Public];
GRANT Insert ON  [Controllers]  TO  [DB:Public];
GRANT Delete ON  [Controllers]  TO  [DB:Public];
GRANT Select ON  [CommTypes]  TO  [DB:Public];
GRANT Update ON  [CommTypes]  TO  [DB:Public];
GRANT Insert ON  [CommTypes]  TO  [DB:Public];
GRANT Delete ON  [CommTypes]  TO  [DB:Public];
GRANT Select ON  [Models]  TO  [DB:Public];
GRANT Update ON  [Models]  TO  [DB:Public];
GRANT Insert ON  [Models]  TO  [DB:Public];
GRANT Delete ON  [Models]  TO  [DB:Public];
GRANT Select ON  [AlertFiltering]  TO  [DB:Public];
GRANT Update ON  [AlertFiltering]  TO  [DB:Public];
GRANT Insert ON  [AlertFiltering]  TO  [DB:Public];
GRANT Delete ON  [AlertFiltering]  TO  [DB:Public];
GRANT Select ON  [AlertFilterOptions]  TO  [DB:Public];
GRANT Update ON  [AlertFilterOptions]  TO  [DB:Public];
GRANT Insert ON  [AlertFilterOptions]  TO  [DB:Public];
GRANT Delete ON  [AlertFilterOptions]  TO  [DB:Public];
GRANT Select ON  [GeneralHistory]  TO  [DB:Public];
GRANT Update ON  [GeneralHistory]  TO  [DB:Public];
GRANT Insert ON  [GeneralHistory]  TO  [DB:Public];
GRANT Delete ON  [GeneralHistory]  TO  [DB:Public];
GRANT Select ON  [History]  TO  [DB:Public];
GRANT Update ON  [History]  TO  [DB:Public];
GRANT Insert ON  [History]  TO  [DB:Public];
GRANT Delete ON  [History]  TO  [DB:Public];
GRANT Select ON  [TaskData]  TO  [DB:Public];
GRANT Update ON  [TaskData]  TO  [DB:Public];
GRANT Insert ON  [TaskData]  TO  [DB:Public];
GRANT Delete ON  [TaskData]  TO  [DB:Public];
GRANT Select ON  [TaskExclusion]  TO  [DB:Public];
GRANT Update ON  [TaskExclusion]  TO  [DB:Public];
GRANT Insert ON  [TaskExclusion]  TO  [DB:Public];
GRANT Delete ON  [TaskExclusion]  TO  [DB:Public];
GRANT Select ON  [TaskOptions]  TO  [DB:Public];
GRANT Update ON  [TaskOptions]  TO  [DB:Public];
GRANT Insert ON  [TaskOptions]  TO  [DB:Public];
GRANT Delete ON  [TaskOptions]  TO  [DB:Public];
GRANT Select ON  [TaskSchedule]  TO  [DB:Public];
GRANT Update ON  [TaskSchedule]  TO  [DB:Public];
GRANT Insert ON  [TaskSchedule]  TO  [DB:Public];
GRANT Delete ON  [TaskSchedule]  TO  [DB:Public];
GRANT Select( [CompanyID] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [CompanyID] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [CompanyID] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [Name] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [Name] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [Name] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [Company_Address] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [Company_Address] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [Company_Address] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [Company_Address2] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [Company_Address2] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [Company_Address2] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [Company_City] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [Company_City] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [Company_City] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [Company_State] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [Company_State] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [Company_State] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [Company_ZipCode] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [Company_ZipCode] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [Company_ZipCode] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [Deleted] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [Deleted] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [Deleted] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [Company_TimeZone] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [Company_TimeZone] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [Company_TimeZone] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [CompanyTypes] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [CompanyTypes] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [CompanyTypes] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [Notes] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [Notes] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [Notes] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [NotesStartDate] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [NotesStartDate] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [NotesStartDate] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [NotesEndDate] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [NotesEndDate] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [NotesEndDate] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [EmailNotes] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [EmailNotes] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [EmailNotes] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [WebServiceKey] ) ON [Companies]  TO  [DB:Public];
GRANT Update( [WebServiceKey] ) ON [Companies]  TO  [DB:Public];
GRANT Insert( [WebServiceKey] ) ON [Companies]  TO  [DB:Public];
GRANT Select( [UserID] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [UserID] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [UserID] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [UserTypes] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [UserTypes] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [UserTypes] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [Usrnm] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [Usrnm] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [Usrnm] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [Psswrd] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [Psswrd] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [Psswrd] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_Name] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_Name] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_Name] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_Company] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_Company] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_Company] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_Address] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_Address] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_Address] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_Address2] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_Address2] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_Address2] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_City] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_City] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_City] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_State] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_State] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_State] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_ZipCode] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_ZipCode] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_ZipCode] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_Phone] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_Phone] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_Phone] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_Extention] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_Extention] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_Extention] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_Debug] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_Debug] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_Debug] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_SendPData] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_SendPData] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_SendPData] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_SavePData] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_SavePData] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_SavePData] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_GetLights] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_GetLights] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_GetLights] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_EditController] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_EditController] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_EditController] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_AddControllers] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_AddControllers] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_AddControllers] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_DeleteControllers] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_DeleteControllers] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_DeleteControllers] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_AccessPasswords] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_AccessPasswords] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_AccessPasswords] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_ClearMLB] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_ClearMLB] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_ClearMLB] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_ClearHO] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_ClearHO] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_ClearHO] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_SendNoW] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_SendNoW] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_SendNoW] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_MVOR] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_MVOR] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_MVOR] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_OnOff] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_OnOff] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_OnOff] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_SetTimeDate] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_SetTimeDate] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_SetTimeDate] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_ManualProgram] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_ManualProgram] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_ManualProgram] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [User_Mobile] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [User_Mobile] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [User_Mobile] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [CompanyID] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [CompanyID] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [CompanyID] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [Deleted] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [Deleted] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [Deleted] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [ReportSelection] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [ReportSelection] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [ReportSelection] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [Dashboard_DateRange] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [Dashboard_DateRange] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [Dashboard_DateRange] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [Dashboard_Viewing] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [Dashboard_Viewing] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [Dashboard_Viewing] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [EmailAlerts] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [EmailAlerts] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [EmailAlerts] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [LastEmailDate] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [LastEmailDate] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [LastEmailDate] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [NotifyWindow] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [NotifyWindow] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [NotifyWindow] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [DefaultSortDirection] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [DefaultSortDirection] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [DefaultSortDirection] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [LandingPage] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [LandingPage] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [LandingPage] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [TempId] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [TempId] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [TempId] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [TempIdDateRequested] ) ON [Usrs]  TO  [DB:Public];
GRANT Update( [TempIdDateRequested] ) ON [Usrs]  TO  [DB:Public];
GRANT Insert( [TempIdDateRequested] ) ON [Usrs]  TO  [DB:Public];
GRANT Select( [GroupID] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Update( [GroupID] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Insert( [GroupID] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Select( [FltrName] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Update( [FltrName] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Insert( [FltrName] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Select( [UserID] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Update( [UserID] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Insert( [UserID] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Select( [Deleted] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Update( [Deleted] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Insert( [Deleted] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Select( [DefaultFltr] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Update( [DefaultFltr] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Insert( [DefaultFltr] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Select( [DefaultGIS] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Update( [DefaultGIS] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Insert( [DefaultGIS] ) ON [AlrtFltrGrp]  TO  [DB:Public];
GRANT Select( [SiteID] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Update( [SiteID] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Insert( [SiteID] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Select( [CompanyID] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Update( [CompanyID] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Insert( [CompanyID] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Select( [SiteName] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Update( [SiteName] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Insert( [SiteName] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Select( [Deleted] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Update( [Deleted] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Insert( [Deleted] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Select( [UserID] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Update( [UserID] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Insert( [UserID] ) ON [ControllerSites]  TO  [DB:Public];
GRANT Select( [ControllerID] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [ControllerID] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [ControllerID] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [SiteID] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [SiteID] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [SiteID] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [ControllerName] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [ControllerName] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [ControllerName] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [RadioGroupID] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [RadioGroupID] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [RadioGroupID] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [ConnectionType] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [ConnectionType] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [ConnectionType] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [Installed] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [Installed] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [Installed] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [LastCommunicated] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [LastCommunicated] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [LastCommunicated] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [Address] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [Address] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [Address] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [BaudRate] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [BaudRate] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [BaudRate] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [Model] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [Model] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [Model] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [PhoneNumber] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [PhoneNumber] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [PhoneNumber] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [StationsInUse] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [StationsInUse] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [StationsInUse] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [SoftwareVersion] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [SoftwareVersion] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [SoftwareVersion] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [AlertCode] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [AlertCode] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [AlertCode] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [Notes] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [Notes] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [Notes] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [LightsDescription1] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [LightsDescription1] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [LightsDescription1] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [LightsDescription2] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [LightsDescription2] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [LightsDescription2] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [LightsDescription3] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [LightsDescription3] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [LightsDescription3] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [LightsDescription4] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [LightsDescription4] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [LightsDescription4] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [Communicate] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [Communicate] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [Communicate] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [WeatherShutdown] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [WeatherShutdown] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [WeatherShutdown] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [ControllerPicture] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [ControllerPicture] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [ControllerPicture] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [AutoRetrieveReports] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [AutoRetrieveReports] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [AutoRetrieveReports] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [Deleted] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [Deleted] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [Deleted] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [UserID] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [UserID] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [UserID] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [FOAL] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [FOAL] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [FOAL] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [TimeoutAdj] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [TimeoutAdj] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [TimeoutAdj] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [CommServer] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [CommServer] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [CommServer] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [UseCTS] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [UseCTS] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [UseCTS] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [WeatherStation] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [WeatherStation] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [WeatherStation] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [ControllerImage] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [ControllerImage] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [ControllerImage] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [ImageType] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [ImageType] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [ImageType] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [SRInvolved] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [SRInvolved] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [SRInvolved] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [TrainingController] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [TrainingController] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [TrainingController] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [LRCommType] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [LRCommType] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [LRCommType] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [Latitude] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [Latitude] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [Latitude] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [Longitude] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [Longitude] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [Longitude] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [ETSharingControllerID] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [ETSharingControllerID] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [ETSharingControllerID] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [RainSharingControllerID] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [RainSharingControllerID] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [RainSharingControllerID] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [TimeZone] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [TimeZone] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [TimeZone] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [CompanyID] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [CompanyID] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [CompanyID] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [TP_Version] ) ON [Controllers]  TO  [DB:Public];
GRANT Update( [TP_Version] ) ON [Controllers]  TO  [DB:Public];
GRANT Insert( [TP_Version] ) ON [Controllers]  TO  [DB:Public];
GRANT Select( [CTIndex] ) ON [CommTypes]  TO  [DB:Public];
GRANT Update( [CTIndex] ) ON [CommTypes]  TO  [DB:Public];
GRANT Insert( [CTIndex] ) ON [CommTypes]  TO  [DB:Public];
GRANT Select( [CommunicationsType] ) ON [CommTypes]  TO  [DB:Public];
GRANT Update( [CommunicationsType] ) ON [CommTypes]  TO  [DB:Public];
GRANT Insert( [CommunicationsType] ) ON [CommTypes]  TO  [DB:Public];
GRANT Select( [ModelIndex] ) ON [Models]  TO  [DB:Public];
GRANT Update( [ModelIndex] ) ON [Models]  TO  [DB:Public];
GRANT Insert( [ModelIndex] ) ON [Models]  TO  [DB:Public];
GRANT Select( [Model] ) ON [Models]  TO  [DB:Public];
GRANT Update( [Model] ) ON [Models]  TO  [DB:Public];
GRANT Insert( [Model] ) ON [Models]  TO  [DB:Public];
GRANT Select( [UserID] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Update( [UserID] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Insert( [UserID] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Select( [AlertFilter] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Update( [AlertFilter] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Insert( [AlertFilter] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Select( [AlertState] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Update( [AlertState] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Insert( [AlertState] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Select( [AlertStyle] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Update( [AlertStyle] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Insert( [AlertStyle] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Select( [AlertFilterGroupID] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Update( [AlertFilterGroupID] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Insert( [AlertFilterGroupID] ) ON [AlertFiltering]  TO  [DB:Public];
GRANT Select( [FilterID] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Update( [FilterID] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Insert( [FilterID] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Select( [ParentID] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Update( [ParentID] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Insert( [ParentID] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Select( [Display] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Update( [Display] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Insert( [Display] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Select( [Sort] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Update( [Sort] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Insert( [Sort] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Select( [Source] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Update( [Source] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Insert( [Source] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Select( [Style] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Update( [Style] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Insert( [Style] ) ON [AlertFilterOptions]  TO  [DB:Public];
GRANT Select( [HistoryID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Update( [HistoryID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Insert( [HistoryID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Select( [HistoryTimeStamp] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Update( [HistoryTimeStamp] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Insert( [HistoryTimeStamp] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Select( [ControllerID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Update( [ControllerID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Insert( [ControllerID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Select( [TaskID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Update( [TaskID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Insert( [TaskID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Select( [CommServer] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Update( [CommServer] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Insert( [CommServer] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Select( [UserID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Update( [UserID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Insert( [UserID] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Select( [HistoryErrorCode] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Update( [HistoryErrorCode] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Insert( [HistoryErrorCode] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Select( [SpeedCommunications] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Update( [SpeedCommunications] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Insert( [SpeedCommunications] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Select( [EndTimeStamp] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Update( [EndTimeStamp] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Insert( [EndTimeStamp] ) ON [GeneralHistory]  TO  [DB:Public];
GRANT Select( [HistoryID] ) ON [History]  TO  [DB:Public];
GRANT Update( [HistoryID] ) ON [History]  TO  [DB:Public];
GRANT Insert( [HistoryID] ) ON [History]  TO  [DB:Public];
GRANT Select( [HistoryTimeStamp] ) ON [History]  TO  [DB:Public];
GRANT Update( [HistoryTimeStamp] ) ON [History]  TO  [DB:Public];
GRANT Insert( [HistoryTimeStamp] ) ON [History]  TO  [DB:Public];
GRANT Select( [Controller] ) ON [History]  TO  [DB:Public];
GRANT Update( [Controller] ) ON [History]  TO  [DB:Public];
GRANT Insert( [Controller] ) ON [History]  TO  [DB:Public];
GRANT Select( [HistoryErrorMsg] ) ON [History]  TO  [DB:Public];
GRANT Update( [HistoryErrorMsg] ) ON [History]  TO  [DB:Public];
GRANT Insert( [HistoryErrorMsg] ) ON [History]  TO  [DB:Public];
GRANT Select( [HistoryFunction] ) ON [History]  TO  [DB:Public];
GRANT Update( [HistoryFunction] ) ON [History]  TO  [DB:Public];
GRANT Insert( [HistoryFunction] ) ON [History]  TO  [DB:Public];
GRANT Select( [Communications] ) ON [History]  TO  [DB:Public];
GRANT Update( [Communications] ) ON [History]  TO  [DB:Public];
GRANT Insert( [Communications] ) ON [History]  TO  [DB:Public];
GRANT Select( [HistoryRoms] ) ON [History]  TO  [DB:Public];
GRANT Update( [HistoryRoms] ) ON [History]  TO  [DB:Public];
GRANT Insert( [HistoryRoms] ) ON [History]  TO  [DB:Public];
GRANT Select( [HistoryModel] ) ON [History]  TO  [DB:Public];
GRANT Update( [HistoryModel] ) ON [History]  TO  [DB:Public];
GRANT Insert( [HistoryModel] ) ON [History]  TO  [DB:Public];
GRANT Select( [HistorySuccess] ) ON [History]  TO  [DB:Public];
GRANT Update( [HistorySuccess] ) ON [History]  TO  [DB:Public];
GRANT Insert( [HistorySuccess] ) ON [History]  TO  [DB:Public];
GRANT Select( [HistoryErrorCode] ) ON [History]  TO  [DB:Public];
GRANT Update( [HistoryErrorCode] ) ON [History]  TO  [DB:Public];
GRANT Insert( [HistoryErrorCode] ) ON [History]  TO  [DB:Public];
GRANT Select( [UserID] ) ON [History]  TO  [DB:Public];
GRANT Update( [UserID] ) ON [History]  TO  [DB:Public];
GRANT Insert( [UserID] ) ON [History]  TO  [DB:Public];
GRANT Select( [ControllerID] ) ON [History]  TO  [DB:Public];
GRANT Update( [ControllerID] ) ON [History]  TO  [DB:Public];
GRANT Insert( [ControllerID] ) ON [History]  TO  [DB:Public];
GRANT Select( [CommServer] ) ON [History]  TO  [DB:Public];
GRANT Update( [CommServer] ) ON [History]  TO  [DB:Public];
GRANT Insert( [CommServer] ) ON [History]  TO  [DB:Public];
GRANT Select( [TaskID] ) ON [TaskData]  TO  [DB:Public];
GRANT Update( [TaskID] ) ON [TaskData]  TO  [DB:Public];
GRANT Insert( [TaskID] ) ON [TaskData]  TO  [DB:Public];
GRANT Select( [TaskName] ) ON [TaskData]  TO  [DB:Public];
GRANT Update( [TaskName] ) ON [TaskData]  TO  [DB:Public];
GRANT Insert( [TaskName] ) ON [TaskData]  TO  [DB:Public];
GRANT Select( [UserID] ) ON [TaskData]  TO  [DB:Public];
GRANT Update( [UserID] ) ON [TaskData]  TO  [DB:Public];
GRANT Insert( [UserID] ) ON [TaskData]  TO  [DB:Public];
GRANT Select( [CompanyID] ) ON [TaskData]  TO  [DB:Public];
GRANT Update( [CompanyID] ) ON [TaskData]  TO  [DB:Public];
GRANT Insert( [CompanyID] ) ON [TaskData]  TO  [DB:Public];
GRANT Select( [RefUnique] ) ON [TaskData]  TO  [DB:Public];
GRANT Update( [RefUnique] ) ON [TaskData]  TO  [DB:Public];
GRANT Insert( [RefUnique] ) ON [TaskData]  TO  [DB:Public];
GRANT Select( [RefTimeZone] ) ON [TaskData]  TO  [DB:Public];
GRANT Update( [RefTimeZone] ) ON [TaskData]  TO  [DB:Public];
GRANT Insert( [RefTimeZone] ) ON [TaskData]  TO  [DB:Public];
GRANT Select( [TaskTime] ) ON [TaskExclusion]  TO  [DB:Public];
GRANT Update( [TaskTime] ) ON [TaskExclusion]  TO  [DB:Public];
GRANT Insert( [TaskTime] ) ON [TaskExclusion]  TO  [DB:Public];
GRANT Select( [TaskDate] ) ON [TaskExclusion]  TO  [DB:Public];
GRANT Update( [TaskDate] ) ON [TaskExclusion]  TO  [DB:Public];
GRANT Insert( [TaskDate] ) ON [TaskExclusion]  TO  [DB:Public];
GRANT Select( [TaskID] ) ON [TaskExclusion]  TO  [DB:Public];
GRANT Update( [TaskID] ) ON [TaskExclusion]  TO  [DB:Public];
GRANT Insert( [TaskID] ) ON [TaskExclusion]  TO  [DB:Public];
GRANT Select( [OptionID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Update( [OptionID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Insert( [OptionID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Select( [TaskID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Update( [TaskID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Insert( [TaskID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Select( [ControllerID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Update( [ControllerID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Insert( [ControllerID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Select( [Options] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Update( [Options] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Insert( [Options] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Select( [ServerID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Update( [ServerID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Insert( [ServerID] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Select( [CompletionTimestamp] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Update( [CompletionTimestamp] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Insert( [CompletionTimestamp] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Select( [TaskIndex] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Update( [TaskIndex] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Insert( [TaskIndex] ) ON [TaskOptions]  TO  [DB:Public];
GRANT Select( [StartDate] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Update( [StartDate] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Insert( [StartDate] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Select( [ScheduleTime] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Update( [ScheduleTime] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Insert( [ScheduleTime] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Select( [EndTimeStamp] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Update( [EndTimeStamp] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Insert( [EndTimeStamp] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Select( [Repeating] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Update( [Repeating] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Insert( [Repeating] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Select( [TaskID] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Update( [TaskID] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Insert( [TaskID] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Select( [UserID] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Update( [UserID] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Insert( [UserID] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Select( [TimeZone] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Update( [TimeZone] ) ON [TaskSchedule]  TO  [DB:Public];
GRANT Insert( [TimeZone] ) ON [TaskSchedule]  TO  [DB:Public];
EXECUTE PROCEDURE sp_ModifyPermission( 11, NULL, NULL, 'DB:Public', 2147483904, FALSE );

