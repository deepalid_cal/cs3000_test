/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	<string.h>

#include	"legacy_common.h"

#include	"sql_merge.h"

#include	"cal_string.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* 
 *   8-11-2016 @wjb: method returns the position of the last occurrence of a specified value in a   	.
 *  				 string This method returns -1 if the value to search for never occurs  			.
 */
extern INT_32 lastIndexOf( char *pstr, char *psubstr, const UNS_32 pstr_len )
{
	UNS_32	lindex_into_str;

	UNS_32	lsubstr_len;

	UNS_8	*begin;

	UNS_8	*pattern;

	// ----------

	lindex_into_str = 0;

	// ----------

	while( lindex_into_str < pstr_len )
	{
		lsubstr_len = 0;

		begin = pstr;

		pattern = psubstr;

		// ----------

		// If first character of substring matches, check for whole string
		while( (*pstr) && (*pattern) && (*pstr == *pattern) )
		{
			pstr++;

			pattern++;

			lsubstr_len++;
		}

		// ----------

		// If complete substring matches, return starting address 
		if( !*pattern )
		{
			return( lindex_into_str + lsubstr_len );
		}

		// ----------

		pstr = (begin + 1);	// Increment main string 

		lindex_into_str++;
	}

	return( -1 );	// If the substring wasn't found, return -1

	// 8/17/2016 ajv : The code below uses strnstr which may be more efficient than the above
	// code. However, the above code has already tested and works so leave this commented out
	// for now.
	#if 0

		INT_32 index;

		UNS_8 *found;

		// ----------

		index = -1;

		// ----------

		found = strnstr( pstr, psubstr, pstr_len );

		if( found )
		{
			index = found - pstr;
		}

		// ----------

		return( index );

	#endif
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
// reverse 16-bit byte order to go from big-endian to little-endian and vice versa
extern UNS_16 LEGACY_COMMON_swap_endianness_uint16( const UNS_16 pvalue )
{
    return (UNS_16)((pvalue & 0xFFU) << 8 | (pvalue & 0xFF00U) >> 8);
}

/* ---------------------------------------------------------- */
// reverse 16-bit byte order to go from big-endian to little-endian and vice versa
extern INT_16 LEGACY_COMMON_swap_endianness_int16( const INT_16 pvalue )
{
    return (INT_16)((pvalue & 0xFFU) << 8 | (pvalue & 0xFF00U) >> 8);
}

/* ---------------------------------------------------------- */
// reverse 32-bit byte order to go from big-endian to little-endian and vice versa
extern UNS_32 LEGACY_COMMON_swap_endianness_uint32( const UNS_32 pvalue )
{
    return (UNS_32)((pvalue & 0x000000FFU) << 24 | (pvalue & 0x0000FF00U) <<  8 |
					(pvalue & 0x00FF0000U) >>  8 | (pvalue & 0xFF000000U) >> 24);
}

/* ---------------------------------------------------------- */
// reverse 64-bit byte order to go from big-endian to little-endian and vice versa
extern UNS_64 LEGACY_COMMON_swap_endianness_uint64( const UNS_64 pvalue )
{
    return (UNS_64)((pvalue & 0x00000000000000FFUL) << 56 | (pvalue & 0x000000000000FF00UL) << 40 |
					(pvalue & 0x0000000000FF0000UL) << 24 | (pvalue & 0x00000000FF000000UL) <<  8 |
					(pvalue & 0x000000FF00000000UL) >>  8 | (pvalue & 0x0000FF0000000000UL) >> 24 |
					(pvalue & 0x00FF000000000000UL) >> 40 | (pvalue & 0xFF00000000000000UL) >> 56);
}

/* ---------------------------------------------------------- */
extern DATE_TIME LEGACY_COMMON_swap_endianness_datetime( const DATE_TIME pvalue )
{
	DATE_TIME	lvalue;

	DATE_TIME	svalue;

	// ----------

	// @wjb 8/9/2016 : The DATE_TIME value passed-in in to this function is stored in the CS3000
	// DATE_TIME structure (UNS_32 T, UNS_16 D) which is different from ET2000 DATE_TIME (UNS_16
	// D, UNS_32 T) so we need to extract the date and time.
	svalue.T = (UNS_32)( (pvalue.D & 0x000000FFU) << 16 | (pvalue.D & 0x0000FF00U) << 16 |
						 (pvalue.T & 0xFF000000U) >> 16 | (pvalue.T & 0x00FF0000U) >> 16 );

	svalue.D = (UNS_16)( (pvalue.T & 0x000000FFU) | (pvalue.T & 0x0000FF00U) );

	// ----------

	lvalue.D = LEGACY_COMMON_swap_endianness_uint16( svalue.D );

	lvalue.T = LEGACY_COMMON_swap_endianness_uint32( svalue.T );

	// ----------

    return( lvalue );
}

/* ---------------------------------------------------------- */

// @WB: we do especial extract for ET & Rain value from scratch object after the value was swapped as INT32
// value came as => (2 bytes ET value) + (1 byte ET status) + (1 byte dummy)
extern UNS_16 LEGACY_COMMON_extract_et_rain( const UNS_32 pvalue )
{
	return (LEGACY_COMMON_swap_endianness_uint16((UNS_16)((UNS_32)((pvalue & 0x00FF0000U) >> 8 | (pvalue & 0xFF000000U) >> 24))));
}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern UNS_32 ALERTS_pull_legacy_object_off_pile( ALERTS_PILE_STRUCT *const ppile, void *pdest, UNS_32 *pindex_ptr, const UNS_32 psize_of_object )
{
	UNS_16	lswapped_uint16;

	UNS_32	lswapped_uint32;

	UNS_64	lswapped_uint64;

	// ----------

	ALERTS_pull_object_off_pile(ppile, pdest, pindex_ptr, psize_of_object);

	// ----------

	switch( psize_of_object )
	{
		case sizeof(UNS_16):
			memcpy( &lswapped_uint16, pdest, psize_of_object );
			lswapped_uint16 = LEGACY_COMMON_swap_endianness_uint16( lswapped_uint16 );
			memcpy( pdest, &lswapped_uint16, psize_of_object );
			break;

		case sizeof(UNS_32):
			memcpy( &lswapped_uint32, pdest, psize_of_object );
			lswapped_uint32 = LEGACY_COMMON_swap_endianness_uint32( lswapped_uint32 );
			memcpy( pdest, &lswapped_uint32, psize_of_object );
			break;

		case sizeof(UNS_64):
			memcpy( &lswapped_uint64, pdest, psize_of_object );
			lswapped_uint64 = LEGACY_COMMON_swap_endianness_uint64( lswapped_uint64 );
			memcpy( pdest, &lswapped_uint64, psize_of_object );
			break;

		case sizeof(DATE_TIME):
			// 8/17/2016 ajv : Don't do anything for DATE_TIME objects. These need to be handled using
			// the LEGACY_COMMON_swap_endianness_datetime function because we need to convert them to
			// the CS3000 DATE_TIME format before swapping them.
			break;
	}

	return( psize_of_object );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern UNS_32 LEGACY_COMMON_extract_uint8( UNS_8 **pucp, UNS_8 *pvalue )
{
	memcpy( pvalue, *pucp, sizeof(UNS_8) );
	*pucp += sizeof(UNS_8);

	return( sizeof(UNS_8) );
}

/* ---------------------------------------------------------- */
extern UNS_32 LEGACY_COMMON_extract_uint16( UNS_8 **pucp, UNS_16 *pvalue )
{
	memcpy( pvalue, *pucp, sizeof(UNS_16) );
	*pucp += sizeof(UNS_16);

	*pvalue = LEGACY_COMMON_swap_endianness_uint16( *pvalue );

	return( sizeof(UNS_16) );
}

/* ---------------------------------------------------------- */
extern UNS_32 LEGACY_COMMON_extract_uint32( UNS_8 **pucp, UNS_32 *pvalue )
{
	memcpy( pvalue, *pucp, sizeof(UNS_32) );
	*pucp += sizeof(UNS_32);

	*pvalue = LEGACY_COMMON_swap_endianness_uint32( *pvalue );

	return( sizeof(UNS_32) );
}

/* ---------------------------------------------------------- */
extern UNS_32 LEGACY_COMMON_extract_string( UNS_8 **pucp, char *pvalue, const UNS_32 pstrlen )
{
	memset( pvalue, 0x00, pstrlen );

	memcpy( pvalue, *pucp, pstrlen );
	*pucp += pstrlen;

	return( pstrlen );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern UNS_32 LEGACY_COMMON_extract_and_store_uint8(	UNS_8 **pucp,
														char *pfield_name,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str )
{
	UNS_8	lval;

	// ----------

	LEGACY_COMMON_extract_uint8( pucp, &lval );

	SQL_MERGE_build_upsert_uint32( lval, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	return( sizeof(UNS_8) );
}

/* ---------------------------------------------------------- */
extern UNS_32 LEGACY_COMMON_extract_and_store_uint16(	UNS_8 **pucp,
														char *pfield_name,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str )
{
	UNS_16	lval;

	// ----------

	LEGACY_COMMON_extract_uint16( pucp, &lval );

	SQL_MERGE_build_upsert_uint32( lval, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	return( sizeof(UNS_16) );
}

/* ---------------------------------------------------------- */
extern UNS_32 LEGACY_COMMON_extract_and_store_uint32(	UNS_8 **pucp,
														char *pfield_name,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str )
{
	UNS_32	lval;

	// ----------

	LEGACY_COMMON_extract_uint32( pucp, &lval );

	SQL_MERGE_build_upsert_uint32( lval, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	return( sizeof(UNS_32) );
}

/* ---------------------------------------------------------- */
extern UNS_32 LEGACY_COMMON_extract_and_store_string(	UNS_8 **pucp,
														const UNS_32 pstrlen,
														char *pfield_name,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str )
{
	char	lstr_48[ 48 ];

	// ----------

	memset( lstr_48, 0x00, 48 );

	// ----------

	LEGACY_COMMON_extract_string( pucp, lstr_48, pstrlen );

	SQL_MERGE_build_upsert_string( lstr_48, pstrlen, SQL_MERGE_include_field_name_in_insert_clause, pfield_name, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	return( pstrlen );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

