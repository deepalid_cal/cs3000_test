/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"legacy_cmos.h"

#include	"legacy_common.h"

// Required for SQL functions
#include	"sql_merge.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static UNS_32 extract_and_store_500_series_cmos_data(	UNS_8 **pucp,
														const UNS_32 pcontroller_id,
														DATE_TIME llast_time_cmos_was_retrieved,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf )
{
	UNS_32	rv;

	// ----------

	rv = 0;

	// ----------

	// 1/25/2016 ajv : Make sure the strings are fully initialized to NULL since we rely on this
	// to determine whether the string is empty or not.
	memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	// ----------

	SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "Version", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "CMOSSize", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "A_BaudRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "Protocol", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "Comm_Type", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "Number_Of_Stations", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "KeyPad", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "LightsEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "GInterface", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "RBInterface", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "FInterface", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "DemoAllOptions", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "SizeOfRam", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "UseSOutDelay", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "NumOfRings", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "DTMF_Installed", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "WGInterface", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "UseCTS", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "BuzzerType", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "Reserved1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "Reserved2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "Reserved3", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "B_BaudRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "BoardRevision", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "Debuging", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "OM_Originator_Retries", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	
	// nlu_OM_Response_Retries
	*pucp += sizeof(UNS_16);
	rv += sizeof(UNS_16);

	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "OM_Seconds_for_Status_FOAL", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "OM_Minutes_To_Exist", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "FOAL_MinutesBetweenRescanAttempts", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// nlu_Comm_Mngr_App_Timer_Seconds
	*pucp += sizeof(UNS_16);
	rv += sizeof(UNS_16);

	rv += LEGACY_COMMON_extract_and_store_string( pucp, 3, "COMM_Address", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// dummy_1
	*pucp += sizeof(UNS_8);
	rv += sizeof(UNS_8);

	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NormalLevelsA", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NormalLevelsB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NormalLevelsC", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NormalLevelsD", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "C_BaudRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "D_BaudRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "StationsAllowed", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "GpmSlotSize", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "MaxStationsOn", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NumberOfGpmSlots", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NumberOfStationCycles", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "CellIterations", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CentralMajorVersion", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CentralMinorVersion", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CentralReleaseVersion", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CentralBuildVersion", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "ControllerSerialNumber", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	SQL_MERGE_build_upsert_timestamp(llast_time_cmos_was_retrieved, SQL_MERGE_include_field_name_in_insert_clause, "RetrievalTimestamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);

	// ----------

	SQL_MERGE_build_search_condition_uint32( "ControllerID", pcontroller_id, pSQL_search_condition_str );

	SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( "CMOS", pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 extract_and_store_600_series_cmos_data(	UNS_8 **pucp,
														const UNS_32 pcontroller_id,
														DATE_TIME llast_time_cmos_was_retrieved,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf )
{
	UNS_32	rv;

	UNS_8 PortASRIsAMaster;
	UNS_8 PortBSRIsAMaster;

	// ----------

	rv = 0;

	PortASRIsAMaster = 0;
	PortBSRIsAMaster = 0;

	// ----------

	// 1/25/2016 ajv : Make sure the strings are fully initialized to NULL since we rely on this
	// to determine whether the string is empty or not.
	memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	// ----------

	SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "Version", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "CMOSSize", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "ControllerSerialNumber", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "StationsAllowed", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CommTypeA", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CommTypeB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CommTypeC", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CommTypeD", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "A_BaudRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "B_BaudRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "C_BaudRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "D_BaudRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NormalLevelsA", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NormalLevelsB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NormalLevelsC", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NormalLevelsD", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "ThisControllerIsAHub", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "FInterface", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "GInterface", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "RBInterface", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "WGInterface", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "DemoAllOptions", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "LightsEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "RRe_Enabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "FOAL_MinutesBetweenRescanAttempts", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_string( pucp, 3, "COMM_Address", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "NumOfRings", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CentralMajorVersion", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CentralMinorVersion", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CentralReleaseVersion", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint8( pucp, "CentralBuildVersion", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "OM_Originator_Retries", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "OM_Seconds_for_Status_FOAL", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "OM_Minutes_To_Exist", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "GpmSlotSize", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "MaxStationsOn", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NumberOfGpmSlots", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "NumberOfStationCycles", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "CellIterations", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "Debuging", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// PortASRIsAMaster & PortASRIsAMaster
	rv += LEGACY_COMMON_extract_uint8(pucp, &PortASRIsAMaster);
	rv += LEGACY_COMMON_extract_uint8(pucp, &PortBSRIsAMaster);

	SQL_MERGE_build_upsert_bool((PortASRIsAMaster>0 ? 1 : 0), SQL_MERGE_include_field_name_in_insert_clause, "PortASRIsAMaster", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
	SQL_MERGE_build_upsert_bool((PortBSRIsAMaster>0 ? 1 : 0), SQL_MERGE_include_field_name_in_insert_clause, "PortBSRIsAMaster", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);

	rv += LEGACY_COMMON_extract_and_store_uint16( pucp, "HysteresisPercentage", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	rv += LEGACY_COMMON_extract_and_store_uint32( pucp, "Spacer", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	
	// ----------
	
	SQL_MERGE_build_upsert_timestamp(llast_time_cmos_was_retrieved, SQL_MERGE_include_field_name_in_insert_clause, "RetrievalTimestamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);

	// ----------

	SQL_MERGE_build_search_condition_uint32( "ControllerID", pcontroller_id, pSQL_search_condition_str );

	SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements( "CMOS", pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */

static void update_controller_table_from_cmos(	const UNS_32 pcontroller_id,
												char *software_version,
												char *pSQL_statements,
												char *pSQL_single_merge_statement_buf)
{
	memset(pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT);

	snprintf(pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
		"UPDATE Controllers SET StationsInUse=(SELECT TOP 1 StationsAllowed FROM CMOS WHERE ControllerID=%u),Model=CONVERT(LEFT('%s',1),SQL_INTEGER),SoftwareVersion='%s',LastCommunicated=CURRENT_TIMESTAMP() WHERE ControllerID=%u;",
		pcontroller_id, software_version, software_version, pcontroller_id);

	strlcat(pSQL_statements, pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS);
}

/* ---------------------------------------------------------- */
DLLEXPORT UNS_32 LEGACY_CMOS_extract_and_store_data(	UNS_8 *pucp,
														const UNS_32 pcontroller_id,
														const UNS_8 pis_a_600_series,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf )
{
	UNS_32	lcmos_ver;

	UNS_32  rv;

	char	lrom_date[ ET2000e_ROM_VERSION_STRING_LENGTH + 1]; // add 1 for null terminating

	DATE_TIME	llast_time_cmos_was_retrieved;

	// ----------

	rv = 0;

	// ----------

	memset( pSQL_statements, 0x00, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	// ----------

	// 6/20/2016 ajv : For now, skip over the last time CMOS was retrieved to see whether that
	// overcomes the parsing error Jay is seeing. We may need to store this somewhere as well.
	LEGACY_COMMON_extract_uint16( &pucp, &llast_time_cmos_was_retrieved.D );
	LEGACY_COMMON_extract_uint32( &pucp, &llast_time_cmos_was_retrieved.T );

	// 6/20/2016 ajv : For now, skip over the ROM version (e.g., 610.m) to see whether that
	// overcomes the parsing error Jay is seeing. We may need to store this somewhere as well.
	LEGACY_COMMON_extract_string( &pucp, lrom_date, ET2000e_ROM_VERSION_STRING_LENGTH );

	// 6/20/2016 wjb : make sure to terminate the string
	lrom_date[ET2000e_ROM_VERSION_STRING_LENGTH] = '\0';

	// ----------

	// 1/25/2016 ajv : Since we're retrieving the latest CMOS, we can't rely on the passed-in
	// CMOS version. Therefore, since the version occupies the first two-bytes of the message
	// body, extract that now. However, DO NOT increment pucp as we need to parse out the
	// version again in the extract routines so it can be stored in the database.
	lcmos_ver = 0;
	memcpy( &lcmos_ver, pucp, sizeof(UNS_16) );
	lcmos_ver = LEGACY_COMMON_swap_endianness_uint16( lcmos_ver );
	
	// 1/25/2016 ajv : We require that the controller be running the latest firmware version so
	// we don't have to have unique parsing for each possible firmware version. The same
	// applies to CMOS.

	// 1/25/2016 ajv : The current ET2000e CMOS version is 1312 and the latest ET2000 is 1210.
	// 9/19/2016 wjb : Update the 600 version to 1309.
	if( lcmos_ver >= 0x1309 )
	{
		rv += extract_and_store_600_series_cmos_data(&pucp, pcontroller_id, llast_time_cmos_was_retrieved, pSQL_statements, pSQL_search_condition_str, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf);

		// 8/16/2016 wjb : update controllers table "Software Version, model,.." from cmos
		update_controller_table_from_cmos(pcontroller_id, lrom_date, pSQL_statements, pSQL_single_merge_statement_buf);

	}
	else if( (lcmos_ver < 0x1300) && (lcmos_ver >= 0x1210) )
	{
		rv += extract_and_store_500_series_cmos_data(&pucp, pcontroller_id, llast_time_cmos_was_retrieved, pSQL_statements, pSQL_search_condition_str, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf);

		// 8/16/2016 wjb : update controllers table "Software Version, model,.." from cmos
		update_controller_table_from_cmos(pcontroller_id, lrom_date, pSQL_statements, pSQL_single_merge_statement_buf);
	}
	else
	{
		// 1/25/2016 ajv : The CMOS version cannot be parsed. The return value of this function will
		// therefore be a 0 which is an indication to the CommServer that the parsing could not be
		// completed.
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

