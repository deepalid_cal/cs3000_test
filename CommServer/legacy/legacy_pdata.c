/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"legacy_pdata.h"

#include	"legacy_common.h"

// Required for B_IS_SET routine
#include	"bithacks.h"

// Required for SQL functions
#include	"sql_merge.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if 0

BulkPgmData1
	CommAddr
	LightsDef

BulkPgmData2
	ActiveStations
	EESize
	ControllerDateTime
	CentralDateTime
	SWVersion
	CommunicationsType

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define MAX_SCC_LENGTH				(17)

#define	PTAG_LENGTH					(20)

#define NUM_OF_PREDEFINED_PTAGS		(20)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static char const *const prog_string[ PROG_LAST ] =
{
	"A",
	"B",
	"C",
	"D",
	"E",
	"D1",
	"D2"
};

static char const *const month_strings[ MONTHS_IN_A_YEAR ] =
{
	"Jan",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"June",
	"July",
	"Aug",
	"Sep",
	"Oct",
	"Nov",
	"Dec"
};
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_8 FlowMeter;
	UNS_16 IrrigMLB;
	UNS_16 NonIrrigMLB;
	UNS_8 TypeOfMV;
	UNS_8 ControllerOff;
	UNS_16 ETCountyIndex;
	UNS_16 ETCityIndex;
	UNS_8 UsingAnETGage;
	UNS_8 LogETPulses;
	UNS_16 RunWeek;
	UNS_8 DLSavings;
	UNS_8 UseBudget;
	UNS_8 BudgetOption;
	UNS_16 PercentOfETo_100u;
	UNS_8 WS_InUse;
	UNS_8 WS_Pause;
	UNS_16 WS_P_Speed;
	UNS_16 WS_R_Speed;
	UNS_16 WS_P_Time;
	UNS_16 WS_R_Time;

} BulkPgmData1Fields;

// ----------

typedef struct
{
	UNS_8 FM_Connected;
	UNS_8 FMEnterOwn;
	UNS_8 XFlowMeter_1;
	UNS_8 XFlowMeter_2;
	UNS_32 FME_K_1_100000u;
	UNS_32 FME_K_2_100000u;
	UNS_32 FME_K_3_100000u;
	UNS_32 FME_O_1_100000u;
	UNS_32 FME_O_2_100000u;
	UNS_32 FME_O_3_100000u;
	UNS_16 MaxFlowNumber;
	UNS_8 VariableCCInUse;
	UNS_8 ManCycleSoak;
	UNS_8 DailyETUse12;
	UNS_8 RainSwitchInUse;
	UNS_16 RainInfo_Minimum_100u;
	UNS_16 RainInfo_MaxHourlyRate_100u;
	UNS_16 RainInfo_Maximum_100u;
	UNS_16 MaxInchesOfRainHO_100u;
	UNS_8 AllowNegHO_1;
	UNS_8 AllowNegHO_2;
	UNS_8 AllowNegHO_3;
	UNS_8 AllowNegHO_4;
	UNS_8 AllowNegHO_5;
	UNS_8 AllowNegHO_6;
	UNS_8 AllowNegHO_7;

} BulkPgmData2Fields;

// ----------

typedef struct
{
	UNS_32 IncludeTime;
	UNS_8 UseETAveraging_1;
	UNS_8 UseETAveraging_2;
	UNS_8 UseETAveraging_3;
	UNS_8 UseETAveraging_4;
	UNS_8 UseETAveraging_5;
	UNS_8 UseETAveraging_6;
	UNS_8 UseETAveraging_7;
	UNS_8 TrackEstimatedUsage;
	UNS_8 UseYourOwnNumbers;
	char DTMF_Mode1Addr[ 5 ];
	char DTMF_Mode2Addr[ 2 ];
	UNS_16 DTMFChannel1;
	UNS_16 DTMFChannel2;
	UNS_8 MSInUse;
	UNS_8 POAFS;
	UNS_16 NOCIC;
	UNS_16 HighFlowMargin;
	UNS_16 LowFlowMargin;
	UNS_16 Capacity_NonPump;
	UNS_8 IrrigatingByCapacity;
	UNS_8 TestingFlow;
	UNS_16 FlowRange1Top;
	UNS_16 FlowRange2Top;
	UNS_16 FlowRange3Top;
	UNS_16 FlowTolerance1Plus;
	UNS_16 FlowTolerance1Minus;
	UNS_16 FlowTolerance2Plus;
	UNS_16 FlowTolerance2Minus;
	UNS_16 FlowTolerance3Plus;
	UNS_16 FlowTolerance3Minus;
	UNS_16 FlowTolerance4Plus;
	UNS_16 FlowTolerance4Minus;
	UNS_16 TailEndsThrowAwayUpperbound;
	UNS_16 TailEndsMakeAdjustmentUpperbound;
	UNS_16 IrriElectricalStationLimit;
	UNS_16 FlowLength;
	UNS_8 ChainDown;
	UNS_8 FlowMeterAvailableElsewhere;
	UNS_8 MasterHasIrrigateByCapacity;
	UNS_8 MasterHasFlowChecking;
	UNS_8 ProgramPrioritiesEnabled;
	char UserDefinedETState[ MAX_SCC_LENGTH-1 ];
	char UserDefinedETCounty[ MAX_SCC_LENGTH-1 ];
	char UserDefinedETCity[ MAX_SCC_LENGTH-1 ];

} BulkPgmData3Fields;

// ----------

typedef struct
{
	UNS_8 PTagsEditableAtController;
	UNS_8 RReChannelChangeMode;
	UNS_16 RReControllerChannel;
	UNS_16 RReUserChannel;
	UNS_8 DMCInUse;
	UNS_8 DMCLevels;
	UNS_16 DMCMaxFlowRateLevel1;
	UNS_16 DMCMaxFlowRateLevel2;
	UNS_16 MLBMVOR;
	UNS_8  MaximumHoldoverFactor;
	UNS_8 SICFunctionalityName;
	UNS_8 SICContactType;
	UNS_8 SICIrrigationToAffect;
	UNS_8 SICActionToTake;
	char SICSwitchName[ 24 ];

} BulkPgmData4Fields;

// ----------

typedef struct
{
	UNS_8 Schedule;
	UNS_32 SX_ProgramA;
	UNS_32 SX_ProgramB;
	UNS_32 SX_ProgramC;
	UNS_32 SX_ProgramD;
	UNS_32 SX_ProgramE;
	UNS_32 SX_ProgramD1;
	UNS_32 SX_ProgramD2;
	UNS_32 Budgets;
	UNS_16 UserETNumbers_100u;
	UNS_16 FTimesMax_100u;

} MonthlyPrgmDataFields;

// ----------

typedef struct
{
	char POC_Name[ 42 ];
	UNS_8 PartOfIrrigation;
	UNS_8 AllowIrriDuringMLB;
	UNS_8 MVType;
	UNS_8 FMInUse;
	UNS_8 FMUseKAndO;
	UNS_16 MLBDuringIdle;
	UNS_16 MLBDuringUse;
	UNS_16 MLBDuringIrri;

} POCPrgmDataFields;

// ----------

typedef struct
{
	UNS_32 OpenTimeA;
	UNS_32 DurationA;
	UNS_32 OpenTimeB;
	UNS_32 DurationB;

} POCScheduleFields;

// ----------

typedef struct
{
	UNS_16 ValveTime;
	UNS_8 NoFlowShutOff;
	UNS_8 OverFlowShutOff;
	UNS_8 PumpByProgram;
	UNS_8 DailyETByProg;
	UNS_16 VariableCCByProg_Jan_100u;
	UNS_16 VariableCCByProg_Feb_100u;
	UNS_16 VariableCCByProg_Mar_100u;
	UNS_16 VariableCCByProg_Apr_100u;
	UNS_16 VariableCCByProg_May_100u;
	UNS_16 VariableCCByProg_June_100u;
	UNS_16 VariableCCByProg_July_100u;
	UNS_16 VariableCCByProg_Aug_100u;
	UNS_16 VariableCCByProg_Sep_100u;
	UNS_16 VariableCCByProg_Oct_100u;
	UNS_16 VariableCCByProg_Nov_100u;
	UNS_16 VariableCCByProg_Dec_100u;
	UNS_32 HoldOverByProg;
	UNS_16 PTags;
	UNS_8 MSByProg;
	UNS_16 ValvesOnByProgram;
	UNS_8 SetExpected;
	UNS_16 ValveCloseTime;
	UNS_16 ValvesOnInSystem;
	UNS_8 WindInUse;
	UNS_16 ProgramPriority;
	char UserTag[ PTAG_LENGTH ];
	UNS_16 PercentAdjust1_100u;
	UNS_16 PercentAdjustNumberOfDays1;

} ProgramPrgmDataFields;

// ----------

typedef struct
{
	UNS_16 DailyETFactor_100u;
	UNS_8 StationInUse;
	UNS_16 StationInfo_Rate;
	UNS_16 StationInfo_Size;
	UNS_16 NoWaterDays;
	UNS_16 EditSVar_Jan_PTime_100u;
	UNS_8 EditSVar_Jan_HTime_5;
	UNS_8 EditSVar_Jan_CTime;
	UNS_16 EditSVar_Feb_PTime_100u;
	UNS_8 EditSVar_Feb_HTime_5;
	UNS_8 EditSVar_Feb_CTime;
	UNS_16 EditSVar_Mar_PTime_100u;
	UNS_8 EditSVar_Mar_HTime_5;
	UNS_8 EditSVar_Mar_CTime;
	UNS_16 EditSVar_Apr_PTime_100u;
	UNS_8 EditSVar_Apr_HTime_5;
	UNS_8 EditSVar_Apr_CTime;
	UNS_16 EditSVar_May_PTime_100u;
	UNS_8 EditSVar_May_HTime_5;
	UNS_8 EditSVar_May_CTime;
	UNS_16 EditSVar_June_PTime_100u;
	UNS_8 EditSVar_June_HTime_5;
	UNS_8 EditSVar_June_CTime;
	UNS_16 EditSVar_July_PTime_100u;
	UNS_8 EditSVar_July_HTime_5;
	UNS_8 EditSVar_July_CTime;
	UNS_16 EditSVar_Aug_PTime_100u;
	UNS_8 EditSVar_Aug_HTime_5;
	UNS_8 EditSVar_Aug_CTime;
	UNS_16 EditSVar_Sep_PTime_100u;
	UNS_8 EditSVar_Sep_HTime_5;
	UNS_8 EditSVar_Sep_CTime;
	UNS_16 EditSVar_Oct_PTime_100u;
	UNS_8 EditSVar_Oct_HTime_5;
	UNS_8 EditSVar_Oct_CTime;
	UNS_16 EditSVar_Nov_PTime_100u;
	UNS_8 EditSVar_Nov_HTime_5;
	UNS_8 EditSVar_Nov_CTime;
	UNS_16 EditSVar_Dec_PTime_100u;
	UNS_8 EditSVar_Dec_HTime_5;
	UNS_8 EditSVar_Dec_CTime;
	UNS_8 PAssign_Norm;
	char Name[ STATION_DESCRIPTION_LENGTH ];
	UNS_8 MSSensorAssignment;
	UNS_8 MSSetPoint;
	UNS_8 MSMaxWaterDays;
	UNS_8 FlowStatus;
	UNS_8 IStatus;
	UNS_8 MSReading;
	UNS_8 MSReadingStatus;

} StationPgmDataFields;

// ----------

typedef struct
{
	char TagNames[ PTAG_LENGTH ];

} TagNamesFields;

// ----------

typedef struct
{
	UNS_16 DailyET_ET_10000u;
	UNS_8 DailyET_Status;
	UNS_8 DailyET_Pad;
	UNS_16 RainTable_Rain_100u;
	UNS_8 RainTable_Status;
	UNS_8 RainTable_Pad;

} TwentyEightDayPrgmDataFields;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void store_bulkpgmdata1(	const BulkPgmData1Fields pdata,
								const UNS_32 pcontroller_id,
								const BOOL_32 pis_a_600_series,
								char *pSQL_statements,
								char *pSQL_update_str,
								char *pSQL_insert_fields_str,
								char *pSQL_insert_values_str,
								char *pSQL_single_merge_statement_buf )
{
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

	// ----------

	SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "ETOption", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_uint32( pdata.ETCountyIndex, SQL_MERGE_include_field_name_in_insert_clause, "ETCountyIndex", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.ETCityIndex, SQL_MERGE_include_field_name_in_insert_clause, "ETCityIndex", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.RunWeek, SQL_MERGE_include_field_name_in_insert_clause, "RunWeek", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "PumpUsage", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_uint32( pdata.FlowMeter, SQL_MERGE_include_field_name_in_insert_clause, "FlowMeter", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.TypeOfMV, SQL_MERGE_include_field_name_in_insert_clause, "TypeOfMV", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "LearnGPMs", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_uint32( pdata.IrrigMLB, SQL_MERGE_include_field_name_in_insert_clause, "IrrigMLB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.NonIrrigMLB, SQL_MERGE_include_field_name_in_insert_clause, "NonIrrigMLB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "MainLineBreak", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "FullYear", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( 49, SQL_MERGE_include_field_name_in_insert_clause, "Password1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( 50, SQL_MERGE_include_field_name_in_insert_clause, "Password2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( 51, SQL_MERGE_include_field_name_in_insert_clause, "Password3", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( 123, SQL_MERGE_include_field_name_in_insert_clause, "Password", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "MasterUnit", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "MasterMLB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_bool( pdata.ControllerOff, SQL_MERGE_include_field_name_in_insert_clause, "ControllerOff", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	//CommAddr
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "RunTempProg", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "RunTempTill", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_bool( pdata.UseBudget, SQL_MERGE_include_field_name_in_insert_clause, "UseBudget", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.BudgetOption, SQL_MERGE_include_field_name_in_insert_clause, "BudgetOption", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.PercentOfETo_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "PercentOfETo", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "UseDailyET", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "DailyETDate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.UsingAnETGage, SQL_MERGE_include_field_name_in_insert_clause, "UsingAnETGage", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "ComBaudRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_bool( pdata.WS_InUse, SQL_MERGE_include_field_name_in_insert_clause, "WS_InUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.WS_Pause, SQL_MERGE_include_field_name_in_insert_clause, "WS_Pause", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.WS_P_Speed, SQL_MERGE_include_field_name_in_insert_clause, "WS_P_Speed", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.WS_R_Speed, SQL_MERGE_include_field_name_in_insert_clause, "WS_R_Speed", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.WS_P_Time, SQL_MERGE_include_field_name_in_insert_clause, "WS_P_Time", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.WS_R_Time, SQL_MERGE_include_field_name_in_insert_clause, "WS_R_Time", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_Demonstrator", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_UsingPager", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_FM_FM", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_FM_ET", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_RG_ET", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_WS_ET", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_D1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_D2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_D3", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Jumpers_D4", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_bool( pdata.LogETPulses, SQL_MERGE_include_field_name_in_insert_clause, "LogETPulses", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.DLSavings, SQL_MERGE_include_field_name_in_insert_clause, "DLSavings", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements("BulkPgmData1", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
}

/* ---------------------------------------------------------- */
static void store_bulkpgmdata2(	const BulkPgmData2Fields pdata,
								const UNS_32 pcontroller_id,
								const BOOL_32 pis_a_600_series,
								char *pSQL_statements,
								char *pSQL_update_str,
								char *pSQL_insert_fields_str,
								char *pSQL_insert_values_str,
								char *pSQL_single_merge_statement_buf )
{
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

	// ----------

	SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.RainSwitchInUse, SQL_MERGE_include_field_name_in_insert_clause, "RainSwitchInUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "EnableHOTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "HOTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "RainInfo_InUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "RainInfo_HookedUp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_float( (pdata.RainInfo_MaxHourlyRate_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "RainInfo_MaxHourlyRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.RainInfo_Maximum_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "RainInfo_Maximum", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.RainInfo_Minimum_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "RainInfo_Minimum", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "RainInfo_Status", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "RainInfo_Dummy2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "RainInfo_RBTableDate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "RainInfo_Dummy4", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_string( "", 1, SQL_MERGE_include_field_name_in_insert_clause, "CentralRadio", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_uint32( pdata.XFlowMeter_1, SQL_MERGE_include_field_name_in_insert_clause, "XFlowMeter_1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.XFlowMeter_2, SQL_MERGE_include_field_name_in_insert_clause, "XFlowMeter_2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.MaxFlowNumber, SQL_MERGE_include_field_name_in_insert_clause, "MaxFlowNumber", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "StationOrder", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_bool( pdata.ManCycleSoak, SQL_MERGE_include_field_name_in_insert_clause, "ManCycleSoak", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.DailyETUse12, SQL_MERGE_include_field_name_in_insert_clause, "DailyETUse12", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.AllowNegHO_1, SQL_MERGE_include_field_name_in_insert_clause, "AllowNegHO_1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.AllowNegHO_2, SQL_MERGE_include_field_name_in_insert_clause, "AllowNegHO_2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.AllowNegHO_3, SQL_MERGE_include_field_name_in_insert_clause, "AllowNegHO_3", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.AllowNegHO_4, SQL_MERGE_include_field_name_in_insert_clause, "AllowNegHO_4", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.AllowNegHO_5, SQL_MERGE_include_field_name_in_insert_clause, "AllowNegHO_5", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.AllowNegHO_6, SQL_MERGE_include_field_name_in_insert_clause, "AllowNegHO_6", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.AllowNegHO_7, SQL_MERGE_include_field_name_in_insert_clause, "AllowNegHO_7", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.MaxInchesOfRainHO_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "MaxInchesOfRainHO", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.FME_K_1_100000u / 100000.0F), SQL_MERGE_include_field_name_in_insert_clause, "FME_K_1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.FME_K_2_100000u / 100000.0F), SQL_MERGE_include_field_name_in_insert_clause, "FME_K_2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.FME_K_3_100000u / 100000.0F), SQL_MERGE_include_field_name_in_insert_clause, "FME_K_3", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.FME_O_1_100000u / 100000.0F), SQL_MERGE_include_field_name_in_insert_clause, "FME_O_1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.FME_O_2_100000u / 100000.0F), SQL_MERGE_include_field_name_in_insert_clause, "FME_O_2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_float( (pdata.FME_O_3_100000u / 100000.0F), SQL_MERGE_include_field_name_in_insert_clause, "FME_O_3", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.FM_Connected, SQL_MERGE_include_field_name_in_insert_clause, "FM_Connected", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.FMEnterOwn, SQL_MERGE_include_field_name_in_insert_clause, "FMEnterOwn", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.VariableCCInUse, SQL_MERGE_include_field_name_in_insert_clause, "VariableCCInUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "ReportsRollTimeEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "ReportsRollTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "LogAddress", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "DiagAddress", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	// Active Stations
	// EESize
	// ControllerDateTime
	// CentralDateTime
	// SWVersion
	// CommunicationsType
#if 0
	SQL_MERGE_build_upsert_bool( (true), SQL_MERGE_include_field_name_in_insert_clause, "ExistsOnController", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "UsePassword", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif

	// ----------

	SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements( "BulkPgmData2", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );
}

/* ---------------------------------------------------------- */
static void store_bulkpgmdata3(	const BulkPgmData3Fields pdata,
								const UNS_32 pcontroller_id,
								const BOOL_32 pis_a_600_series,
								char *pSQL_statements,
								char *pSQL_update_str,
								char *pSQL_insert_fields_str,
								char *pSQL_insert_values_str,
								char *pSQL_single_merge_statement_buf )
{
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

	// ----------

	SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_time( pdata.IncludeTime, SQL_MERGE_include_field_name_in_insert_clause, "IncludeTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.UseETAveraging_1, SQL_MERGE_include_field_name_in_insert_clause, "UseETAveraging_1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.UseETAveraging_2, SQL_MERGE_include_field_name_in_insert_clause, "UseETAveraging_2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.UseETAveraging_3, SQL_MERGE_include_field_name_in_insert_clause, "UseETAveraging_3", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.UseETAveraging_4, SQL_MERGE_include_field_name_in_insert_clause, "UseETAveraging_4", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.UseETAveraging_5, SQL_MERGE_include_field_name_in_insert_clause, "UseETAveraging_5", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.UseETAveraging_6, SQL_MERGE_include_field_name_in_insert_clause, "UseETAveraging_6", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.UseETAveraging_7, SQL_MERGE_include_field_name_in_insert_clause, "UseETAveraging_7", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	if( pis_a_600_series )
	{
		// 1/28/2016 ajv : The 600 series no longer uses these fields, so set them to the default.
		SQL_MERGE_build_upsert_raw( "", sizeof(pdata.DTMF_Mode1Addr), SQL_MERGE_include_field_name_in_insert_clause, "DTMF_Mode1Addr", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_raw( "", sizeof(pdata.DTMF_Mode2Addr), SQL_MERGE_include_field_name_in_insert_clause, "DTMF_Mode2Addr", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}
	else
	{
		SQL_MERGE_build_upsert_raw( pdata.DTMF_Mode1Addr, sizeof(pdata.DTMF_Mode1Addr), SQL_MERGE_include_field_name_in_insert_clause, "DTMF_Mode1Addr", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_raw( pdata.DTMF_Mode2Addr, sizeof(pdata.DTMF_Mode2Addr), SQL_MERGE_include_field_name_in_insert_clause, "DTMF_Mode2Addr", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}

#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "BacklightState", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_bool( pdata.MSInUse, SQL_MERGE_include_field_name_in_insert_clause, "MSInUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	if( pis_a_600_series )
	{
		// 1/28/2016 ajv : The 600 series no longer uses these fields, so set them to the default.
		SQL_MERGE_build_upsert_uint32( 5, SQL_MERGE_include_field_name_in_insert_clause, "DTMFChannel1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 5, SQL_MERGE_include_field_name_in_insert_clause, "DTMFChannel2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}
	else
	{
		SQL_MERGE_build_upsert_uint32( pdata.DTMFChannel1, SQL_MERGE_include_field_name_in_insert_clause, "DTMFChannel1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.DTMFChannel2, SQL_MERGE_include_field_name_in_insert_clause, "DTMFChannel2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}

	SQL_MERGE_build_upsert_bool( pdata.TrackEstimatedUsage, SQL_MERGE_include_field_name_in_insert_clause, "TrackEstimatedUsage", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "Deleted", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	SQL_MERGE_build_upsert_bool( pdata.UseYourOwnNumbers, SQL_MERGE_include_field_name_in_insert_clause, "UseYourOwnNumbers", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.POAFS, SQL_MERGE_include_field_name_in_insert_clause, "POAFS", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.NOCIC, SQL_MERGE_include_field_name_in_insert_clause, "NOCIC", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.HighFlowMargin, SQL_MERGE_include_field_name_in_insert_clause, "HighFlowMargin", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.LowFlowMargin, SQL_MERGE_include_field_name_in_insert_clause, "LowFlowMargin", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.Capacity_NonPump, SQL_MERGE_include_field_name_in_insert_clause, "Capacity_NonPump", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.IrrigatingByCapacity, SQL_MERGE_include_field_name_in_insert_clause, "IrrigatingByCapacity", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.TestingFlow, SQL_MERGE_include_field_name_in_insert_clause, "TestingFlow", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowRange1Top, SQL_MERGE_include_field_name_in_insert_clause, "FlowRange1Top", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowRange2Top, SQL_MERGE_include_field_name_in_insert_clause, "FlowRange2Top", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowRange3Top, SQL_MERGE_include_field_name_in_insert_clause, "FlowRange3Top", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowTolerance1Plus, SQL_MERGE_include_field_name_in_insert_clause, "FlowTolerance1Plus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowTolerance1Minus, SQL_MERGE_include_field_name_in_insert_clause, "FlowTolerance1Minus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowTolerance2Plus, SQL_MERGE_include_field_name_in_insert_clause, "FlowTolerance2Plus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowTolerance2Minus, SQL_MERGE_include_field_name_in_insert_clause, "FlowTolerance2Minus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowTolerance3Plus, SQL_MERGE_include_field_name_in_insert_clause, "FlowTolerance3Plus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowTolerance3Minus, SQL_MERGE_include_field_name_in_insert_clause, "FlowTolerance3Minus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowTolerance4Plus, SQL_MERGE_include_field_name_in_insert_clause, "FlowTolerance4Plus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowTolerance4Minus, SQL_MERGE_include_field_name_in_insert_clause, "FlowTolerance4Minus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.TailEndsThrowAwayUpperbound, SQL_MERGE_include_field_name_in_insert_clause, "TailEndsThrowAwayUpperbound", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.TailEndsMakeAdjustmentUpperbound, SQL_MERGE_include_field_name_in_insert_clause, "TailEndsMakeAdjustmentUpperbound", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.IrriElectricalStationLimit, SQL_MERGE_include_field_name_in_insert_clause, "IrriElectricalStationLimit", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.FlowLength, SQL_MERGE_include_field_name_in_insert_clause, "FlowLength", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.ChainDown, SQL_MERGE_include_field_name_in_insert_clause, "ChainDown", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.FlowMeterAvailableElsewhere, SQL_MERGE_include_field_name_in_insert_clause, "FlowMeterAvailableElsewhere", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.MasterHasIrrigateByCapacity, SQL_MERGE_include_field_name_in_insert_clause, "MasterHasIrrigateByCapacity", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.MasterHasFlowChecking, SQL_MERGE_include_field_name_in_insert_clause, "MasterHasFlowChecking", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (true), SQL_MERGE_include_field_name_in_insert_clause, "FlowInfoValid", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif

	if( pis_a_600_series )
	{
		SQL_MERGE_build_upsert_bool( pdata.ProgramPrioritiesEnabled, SQL_MERGE_include_field_name_in_insert_clause, "ProgramPrioritiesEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( pdata.UserDefinedETState, sizeof(pdata.UserDefinedETState), SQL_MERGE_include_field_name_in_insert_clause, "UserDefinedETState", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( pdata.UserDefinedETCounty, sizeof(pdata.UserDefinedETCounty), SQL_MERGE_include_field_name_in_insert_clause, "UserDefinedETCounty", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( pdata.UserDefinedETCity, sizeof(pdata.UserDefinedETCity), SQL_MERGE_include_field_name_in_insert_clause, "UserDefinedETCity", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}
	else
	{
		// 1/28/2016 ajv : The 500 series doesn't know anything about these fields, so set them to
		// the default.
		SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "ProgramPrioritiesEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( "", sizeof(pdata.UserDefinedETState), SQL_MERGE_include_field_name_in_insert_clause, "UserDefinedETState", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( "", sizeof(pdata.UserDefinedETCounty), SQL_MERGE_include_field_name_in_insert_clause, "UserDefinedETCounty", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( "", sizeof(pdata.UserDefinedETCity), SQL_MERGE_include_field_name_in_insert_clause, "UserDefinedETCity", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}

	// ----------

	SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements("BulkPgmData3", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
}

/* ---------------------------------------------------------- */
static void store_bulkpgmdata4(	const BulkPgmData4Fields pdata,
								const UNS_32 pcontroller_id,
								const BOOL_32 pis_a_600_series,
								char *pSQL_statements,
								char *pSQL_update_str,
								char *pSQL_insert_fields_str,
								char *pSQL_insert_values_str,
								char *pSQL_single_merge_statement_buf )
{
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

	// ----------

	SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
	SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "CreatedByRReInterface", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
	if( pis_a_600_series )
	{
		SQL_MERGE_build_upsert_bool( pdata.PTagsEditableAtController, SQL_MERGE_include_field_name_in_insert_clause, "PTagsEditableAtController", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.RReChannelChangeMode, SQL_MERGE_include_field_name_in_insert_clause, "RReChannelChangeMode", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.RReControllerChannel, SQL_MERGE_include_field_name_in_insert_clause, "RReControllerChannel", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.RReUserChannel, SQL_MERGE_include_field_name_in_insert_clause, "RReUserChannel", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( pdata.DMCInUse, SQL_MERGE_include_field_name_in_insert_clause, "DMCInUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.DMCLevels, SQL_MERGE_include_field_name_in_insert_clause, "DMCLevels", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.DMCMaxFlowRateLevel1, SQL_MERGE_include_field_name_in_insert_clause, "DMCMaxFlowRateLevel1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.DMCMaxFlowRateLevel2, SQL_MERGE_include_field_name_in_insert_clause, "DMCMaxFlowRateLevel2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.MLBMVOR, SQL_MERGE_include_field_name_in_insert_clause, "MLBMVOR", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.MaximumHoldoverFactor, SQL_MERGE_include_field_name_in_insert_clause, "MaximumHoldoverFactor", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.SICFunctionalityName, SQL_MERGE_include_field_name_in_insert_clause, "SICFunctionalityName", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.SICContactType, SQL_MERGE_include_field_name_in_insert_clause, "SICContactType", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.SICIrrigationToAffect, SQL_MERGE_include_field_name_in_insert_clause, "SICIrrigationToAffect", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata.SICActionToTake, SQL_MERGE_include_field_name_in_insert_clause, "SICActionToTake", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( pdata.SICSwitchName, sizeof(pdata.SICSwitchName), SQL_MERGE_include_field_name_in_insert_clause, "SICSwitchName", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}
	else
	{
		// 1/28/2016 ajv : The 500 series doesn't know anything about these fields, so set them to
		// the default.
		SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "PTagsEditableAtController", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 16, SQL_MERGE_include_field_name_in_insert_clause, "RReChannelChangeMode", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 2, SQL_MERGE_include_field_name_in_insert_clause, "RReControllerChannel", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 1, SQL_MERGE_include_field_name_in_insert_clause, "RReUserChannel", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "DMCInUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 2, SQL_MERGE_include_field_name_in_insert_clause, "DMCLevels", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "DMCMaxFlowRateLevel1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "DMCMaxFlowRateLevel2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 150, SQL_MERGE_include_field_name_in_insert_clause, "MLBMVOR", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 1, SQL_MERGE_include_field_name_in_insert_clause, "MaximumHoldoverFactor", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 17, SQL_MERGE_include_field_name_in_insert_clause, "SICFunctionalityName", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 34, SQL_MERGE_include_field_name_in_insert_clause, "SICContactType", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 50, SQL_MERGE_include_field_name_in_insert_clause, "SICIrrigationToAffect", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( 65, SQL_MERGE_include_field_name_in_insert_clause, "SICActionToTake", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( "Custom Switch", sizeof(pdata.SICSwitchName), SQL_MERGE_include_field_name_in_insert_clause, "SICSwitchName", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}
#if 0
	SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "YearlyBudget", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif

	// ----------

	SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements( "BulkPgmData4", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf );
}

/* ---------------------------------------------------------- */
static void store_monthlyprgmdata(	const MonthlyPrgmDataFields pdata[],
									const UNS_32 pcontroller_id,
									const BOOL_32 pis_a_600_series,
									char *pSQL_statements,
									char *pSQL_update_str,
									char *pSQL_insert_fields_str,
									char *pSQL_insert_values_str,
									char *pSQL_single_merge_statement_buf )
{
	UNS_32	m;

	// ----------

	for( m = 0; m < MONTHS_IN_A_YEAR; ++m )
	{
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

		// ----------

		SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( (m + 1), SQL_MERGE_include_field_name_in_insert_clause, "DataMonth", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ m ].Budgets, SQL_MERGE_include_field_name_in_insert_clause, "Budgets", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
		SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "ETNumber", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
		SQL_MERGE_build_upsert_float( (pdata [ m ].FTimesMax_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "FTimesMax", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
		SQL_MERGE_build_upsert_float( 0.0F, SQL_MERGE_include_field_name_in_insert_clause, "VariableCCs", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
		SQL_MERGE_build_upsert_uint32( pdata[ m ].Schedule, SQL_MERGE_include_field_name_in_insert_clause, "Schedule", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_time( pdata[ m ].SX_ProgramA, SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramA", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (pdata[ m ].SX_ProgramA != 86400), SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramAEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_time( pdata[ m ].SX_ProgramB, SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (pdata[ m ].SX_ProgramB != 86400), SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramBEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_time( pdata[ m ].SX_ProgramC, SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramC", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (pdata[ m ].SX_ProgramC != 86400), SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramCEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_time( pdata[ m ].SX_ProgramD, SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramD", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (pdata[ m ].SX_ProgramD != 86400), SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramDEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_time( pdata[ m ].SX_ProgramE, SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramE", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (pdata[ m ].SX_ProgramE != 86400), SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramEEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_time( pdata[ m ].SX_ProgramD1, SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramD1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (pdata[ m ].SX_ProgramD1 != 86400), SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramD1Enabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_time( pdata[ m ].SX_ProgramD2, SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramD2", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (pdata[ m ].SX_ProgramD2 != 86400), SQL_MERGE_include_field_name_in_insert_clause, "SX_ProgramD2Enabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ m ].UserETNumbers_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "UserETNumbers", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		// ----------

		SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements("MonthlyPrgmData", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
	}
}

/* ---------------------------------------------------------- */
static void store_pocprgmdata(	const POCPrgmDataFields pdata[],
								const UNS_32 pcontroller_id,
								const BOOL_32 pis_a_600_series,
								char *pSQL_statements,
								char *pSQL_update_str,
								char *pSQL_insert_fields_str,
								char *pSQL_insert_values_str,
								char *pSQL_single_merge_statement_buf )
{
	UNS_32	p;

	// ----------

	for( p = 0; p < 3; ++p )
	{
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

		// ----------

		SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( p, SQL_MERGE_include_field_name_in_insert_clause, "POCID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		if( pis_a_600_series )
		{
			SQL_MERGE_build_upsert_string( pdata[ p ].POC_Name, sizeof(pdata[ p ].POC_Name), SQL_MERGE_include_field_name_in_insert_clause, "POC_Name", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_bool( pdata[ p ].PartOfIrrigation, SQL_MERGE_include_field_name_in_insert_clause, "PartOfIrrigation", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_bool( pdata[ p ].AllowIrriDuringMLB, SQL_MERGE_include_field_name_in_insert_clause, "AllowIrriDuringMLB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( pdata[ p ].MVType, SQL_MERGE_include_field_name_in_insert_clause, "MVType", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_bool( pdata[ p ].FMInUse, SQL_MERGE_include_field_name_in_insert_clause, "FMInUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_bool( pdata[ p ].FMUseKAndO, SQL_MERGE_include_field_name_in_insert_clause, "FMUseKAndO", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( pdata[ p ].MLBDuringIdle, SQL_MERGE_include_field_name_in_insert_clause, "MLBDuringIdle", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( pdata[ p ].MLBDuringUse, SQL_MERGE_include_field_name_in_insert_clause, "MLBDuringUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( pdata[ p ].MLBDuringIrri, SQL_MERGE_include_field_name_in_insert_clause, "MLBDuringIrri", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		}
		else
		{
			// 1/28/2016 ajv : The 500 series doesn't know anything about these fields, so set them to
			// the default.
			SQL_MERGE_build_upsert_string( "", sizeof(pdata[ p ].POC_Name), SQL_MERGE_include_field_name_in_insert_clause, "POC_Name", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_bool( (true), SQL_MERGE_include_field_name_in_insert_clause, "PartOfIrrigation", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "AllowIrriDuringMLB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( 1, SQL_MERGE_include_field_name_in_insert_clause, "MVType", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "FMInUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "FMUseKAndO", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( 200, SQL_MERGE_include_field_name_in_insert_clause, "MLBDuringIdle", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( 200, SQL_MERGE_include_field_name_in_insert_clause, "MLBDuringUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( 400, SQL_MERGE_include_field_name_in_insert_clause, "MLBDuringIrri", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		}

		// ----------

		SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements("POCPrgmData", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
	}
}

/* ---------------------------------------------------------- */
static void store_pocschedule(	const POCScheduleFields pdata[][DAYS_IN_A_WEEK],
								const UNS_32 pcontroller_id,
								const BOOL_32 pis_a_600_series,
								char *pSQL_statements,
								char *pSQL_update_str,
								char *pSQL_insert_fields_str,
								char *pSQL_insert_values_str,
								char *pSQL_single_merge_statement_buf )
{
	UNS_32	p, d;

	// ----------

	for( p = 0; p < 3; ++p )
	{
		for( d = 0; d < DAYS_IN_A_WEEK; ++d )
		{
			memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

			// ----------

			SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( p, SQL_MERGE_include_field_name_in_insert_clause, "POCID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( d, SQL_MERGE_include_field_name_in_insert_clause, "POCDay", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			if( pis_a_600_series )
			{
				SQL_MERGE_build_upsert_bool( (pdata[ p ][ d ].OpenTimeA < 86400), SQL_MERGE_include_field_name_in_insert_clause, "OpenTimeAEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_time( pdata[ p ][ d ].OpenTimeA, SQL_MERGE_include_field_name_in_insert_clause, "OpenTimeA", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_uint32( pdata[ p ][ d ].DurationA, SQL_MERGE_include_field_name_in_insert_clause, "DurationA", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_bool( (pdata[ p ][ d ].OpenTimeB < 86400), SQL_MERGE_include_field_name_in_insert_clause, "OpenTimeBEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_time( pdata[ p ][ d ].OpenTimeB, SQL_MERGE_include_field_name_in_insert_clause, "OpenTimeB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_uint32( pdata[ p ][ d ].DurationB, SQL_MERGE_include_field_name_in_insert_clause, "DurationB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			}
			else
			{
				// 1/28/2016 ajv : The 500 series doesn't know anything about these fields, so set them to
				// the default.
				SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "OpenTimeAEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_time( 86400, SQL_MERGE_include_field_name_in_insert_clause, "OpenTimeA", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_uint32( 3600, SQL_MERGE_include_field_name_in_insert_clause, "DurationA", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "OpenTimeBEnabled", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_time( 86400, SQL_MERGE_include_field_name_in_insert_clause, "OpenTimeB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
				SQL_MERGE_build_upsert_uint32( 3600, SQL_MERGE_include_field_name_in_insert_clause, "DurationB", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			}

			// ----------

			SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements("POCSchedule", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
		}
	}
}

/* ---------------------------------------------------------- */
static void store_programprgmdata(	const ProgramPrgmDataFields pdata[],
									const UNS_32 pcontroller_id,
									const BOOL_32 pis_a_600_series,
									char *pSQL_statements,
									char *pSQL_update_str,
									char *pSQL_insert_fields_str,
									char *pSQL_insert_values_str,
									char *pSQL_single_merge_statement_buf )
{
	UNS_32	p;

	// ----------

	for( p = 0; p < PROG_LAST; ++p )
	{
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

		// ----------

		SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( (p + 1), SQL_MERGE_include_field_name_in_insert_clause, "DataProgram", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ p ].NoFlowShutOff, SQL_MERGE_include_field_name_in_insert_clause, "NoFlowShutOff", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( pdata[ p ].PumpByProgram, SQL_MERGE_include_field_name_in_insert_clause, "PumpByProgram", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
		SQL_MERGE_build_upsert_float( (0.15F), SQL_MERGE_include_field_name_in_insert_clause, "TripPercent", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
		SQL_MERGE_build_upsert_uint32( pdata[ p ].ValveTime, SQL_MERGE_include_field_name_in_insert_clause, "ValveTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
		SQL_MERGE_build_upsert_bool( (false), SQL_MERGE_include_field_name_in_insert_clause, "UseVariableCC", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
		SQL_MERGE_build_upsert_uint32( pdata[ p ].OverFlowShutOff, SQL_MERGE_include_field_name_in_insert_clause, "OverFlowShutOff", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_Jan_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_Jan", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_Feb_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_Feb", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_Mar_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_Mar", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_Apr_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_Apr", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_May_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_May", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_June_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_June", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_July_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_July", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_Aug_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_Aug", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_Sep_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_Sep", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_Oct_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_Oct", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_Nov_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_Nov", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ p ].VariableCCByProg_Dec_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "VariableCCByProg_Dec", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ p ].PTags, SQL_MERGE_include_field_name_in_insert_clause, "PTags", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ p ].MSByProg, SQL_MERGE_include_field_name_in_insert_clause, "MSByProg", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( pdata[ p ].DailyETByProg, SQL_MERGE_include_field_name_in_insert_clause, "DailyETByProg", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (pdata[ p ].HoldOverByProg < 86400), SQL_MERGE_include_field_name_in_insert_clause, "HoldOverEnabledByProg", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_time( pdata[ p ].HoldOverByProg, SQL_MERGE_include_field_name_in_insert_clause, "HoldOverByProg", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ p ].ValvesOnByProgram, SQL_MERGE_include_field_name_in_insert_clause, "ValvesOnByProgram", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( pdata[ p ].SetExpected, SQL_MERGE_include_field_name_in_insert_clause, "SetExpected", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ p ].ValveCloseTime, SQL_MERGE_include_field_name_in_insert_clause, "ValveCloseTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ p ].ValvesOnInSystem, SQL_MERGE_include_field_name_in_insert_clause, "ValvesOnInSystem", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( pdata[ p ].WindInUse, SQL_MERGE_include_field_name_in_insert_clause, "WindInUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		if( pis_a_600_series )
		{
			// @WB: Some controllers sent Program Priority with "0" value which we don't support. ( 1- low, 2-Medium and 3-High)
			SQL_MERGE_build_upsert_uint32((pdata[ p ].ProgramPriority < 1 ? 3 : pdata[p].ProgramPriority), SQL_MERGE_include_field_name_in_insert_clause, "ProgramPriority", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
			SQL_MERGE_build_upsert_string( pdata[ p ].UserTag, sizeof(pdata[ p ].UserTag), SQL_MERGE_include_field_name_in_insert_clause, "UserTag", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_float( (pdata[ p ].PercentAdjust1_100u), SQL_MERGE_include_field_name_in_insert_clause, "PercentAdjust1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( pdata[ p ].PercentAdjustNumberOfDays1, SQL_MERGE_include_field_name_in_insert_clause, "PercentAdjustNumberOfDays1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		}
		else
		{
			// 1/28/2016 ajv : The 500 series doesn't know anything about these fields, so set them to
			// the default.
			SQL_MERGE_build_upsert_uint32( 3, SQL_MERGE_include_field_name_in_insert_clause, "ProgramPriority", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_string( "....................", sizeof(pdata[ p ].UserTag), SQL_MERGE_include_field_name_in_insert_clause, "UserTag", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_float( 100, SQL_MERGE_include_field_name_in_insert_clause, "PercentAdjust1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "PercentAdjustNumberOfDays1", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		}

		// ----------

		SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements("ProgramPrgmData", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
	}
}

/* ---------------------------------------------------------- */
static void store_stationpgmdata(	const StationPgmDataFields pdata[],
									const UNS_32 pcontroller_id,
									const BOOL_32 pis_a_600_series,
									char *pSQL_statements,
									char *pSQL_update_str,
									char *pSQL_insert_fields_str,
									char *pSQL_insert_values_str,
									char *pSQL_single_merge_statement_buf )
{
	// 1/28/2016 ajv : Since the 500 and 600 series have a different number of maximum stations
	// (40 and 48, respectively), we're defining a local variable to ensure we only look through
	// the station data for the correct number of stations.
	UNS_32	lmax_stations;

	UNS_32	s;

	// ----------

	if( pis_a_600_series )
	{
		lmax_stations = MAX_STATIONS__600_SERIES;
	}
	else
	{
		lmax_stations = MAX_STATIONS__500_SERIES;
	}

	// ----------

	for( s = 0; s < lmax_stations; ++s )
	{
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

		// ----------

		SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( (s + 1), SQL_MERGE_include_field_name_in_insert_clause, "StationNumber", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( pdata[ s ].StationInUse, SQL_MERGE_include_field_name_in_insert_clause, "StationInUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].StationInfo_Size, SQL_MERGE_include_field_name_in_insert_clause, "StationInfo_Size", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].StationInfo_Rate, SQL_MERGE_include_field_name_in_insert_clause, "StationInfo_Rate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
		SQL_MERGE_build_upsert_uint32( 700, SQL_MERGE_include_field_name_in_insert_clause, "LorL", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
		SQL_MERGE_build_upsert_uint32( pdata[ s ].FlowStatus, SQL_MERGE_include_field_name_in_insert_clause, "FlowStatus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].IStatus, SQL_MERGE_include_field_name_in_insert_clause, "IStatus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].NoWaterDays, SQL_MERGE_include_field_name_in_insert_clause, "NoWaterDays", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_bool( (pdata[ s ].NoWaterDays > 0), SQL_MERGE_include_field_name_in_insert_clause, "NoWaterToday", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_Jan_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Jan_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_Jan_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Jan_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_Jan_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Jan_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_Feb_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Feb_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_Feb_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Feb_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_Feb_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Feb_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_Mar_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Mar_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_Mar_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Mar_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_Mar_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Mar_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_Apr_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Apr_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_Apr_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Apr_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_Apr_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Apr_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_May_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_May_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_May_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_May_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_May_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_May_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_June_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_June_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_June_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_June_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_June_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_June_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_July_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_July_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_July_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_July_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_July_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_July_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_Aug_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Aug_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_Aug_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Aug_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_Aug_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Aug_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_Sep_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Sep_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_Sep_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Sep_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_Sep_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Sep_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_Oct_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Oct_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_Oct_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Oct_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_Oct_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Oct_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_Nov_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Nov_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_Nov_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Nov_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_Nov_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Nov_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float((pdata[s].EditSVar_Dec_PTime_100u / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Dec_PTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
		SQL_MERGE_build_upsert_uint32( (pdata[ s ].EditSVar_Dec_HTime_5 * 5), SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Dec_HTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].EditSVar_Dec_CTime, SQL_MERGE_include_field_name_in_insert_clause, "EditSVar_Dec_CTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].PAssign_Norm, SQL_MERGE_include_field_name_in_insert_clause, "PAssign_Norm", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( pdata[ s ].Name, sizeof(pdata[ s ].Name), SQL_MERGE_include_field_name_in_insert_clause, "Name", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ s ].DailyETFactor_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "DailyETFactor", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#if 0
		SQL_MERGE_build_upsert_uint32( 0, SQL_MERGE_include_field_name_in_insert_clause, "LowFlowLimits", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
#endif
		SQL_MERGE_build_upsert_uint32( pdata[ s ].MSSensorAssignment, SQL_MERGE_include_field_name_in_insert_clause, "MSSensorAssignment", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].MSSetPoint, SQL_MERGE_include_field_name_in_insert_clause, "MSSetPoint", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].MSReading, SQL_MERGE_include_field_name_in_insert_clause, "MSReading", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].MSReadingStatus, SQL_MERGE_include_field_name_in_insert_clause, "MSReadingStatus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].MSMaxWaterDays, SQL_MERGE_include_field_name_in_insert_clause, "MSMaxWaterDays", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		// ----------

		SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements("StationPgmData", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
	}
}

/* ---------------------------------------------------------- */
static void store_tagnames(	const TagNamesFields pdata[],
							const UNS_32 pcontroller_id,
							const BOOL_32 pis_a_600_series,
							char *pSQL_statements,
							char *pSQL_update_str,
							char *pSQL_insert_fields_str,
							char *pSQL_insert_values_str,
							char *pSQL_single_merge_statement_buf )
{
	UNS_32	t;

	// ----------

	for( t = 0; t < NUM_OF_PREDEFINED_PTAGS; ++t )
	{
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

		// ----------

		SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PDataTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( (t + 1), SQL_MERGE_include_field_name_in_insert_clause, "TagIndex", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_string( pdata[ t ].TagNames, sizeof(pdata[ t ].TagNames), SQL_MERGE_include_field_name_in_insert_clause, "TagNames", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		// ----------

		SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements("TagNames", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
	}
}

/* ---------------------------------------------------------- */
static void store_twentyeightdayprgmdata(	const TwentyEightDayPrgmDataFields pdata[],
											UNS_16 WDay_Prog[][ PROG_LAST ][ 2 ],
											const UNS_32 pcontroller_id,
											const BOOL_32 pis_a_600_series,
											char *pSQL_statements,
											char *pSQL_update_str,
											char *pSQL_insert_fields_str,
											char *pSQL_insert_values_str,
											char *pSQL_single_merge_statement_buf )
{
	char	str_20[ 20 ];

	UNS_32	d, m, p;

	// ----------

	for( d = 0; d < 28; ++d )
	{
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

		// ----------

		SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "PgmTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( (d + 1), SQL_MERGE_include_field_name_in_insert_clause, "DataDate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ d ].DailyET_ET_10000u / 10000.0F), SQL_MERGE_include_field_name_in_insert_clause, "DailyET_ET", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ d ].DailyET_Status, SQL_MERGE_include_field_name_in_insert_clause, "DailyET_Status", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ d ].RainTable_Status, SQL_MERGE_include_field_name_in_insert_clause, "RainTable_Status", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ d ].RainTable_Pad, SQL_MERGE_include_field_name_in_insert_clause, "RainTable_Pad", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_float( (pdata[ d ].RainTable_Rain_100u / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "RainTable_Rain", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		// ----------

		for( m = 0; m < MONTHS_IN_A_YEAR; ++m )
		{
			for( p = 0; p < PROG_LAST; ++p )
			{
				snprintf( str_20, sizeof(str_20), "WDay_Prog%s_%s", prog_string[ p ], month_strings[ m ] );

				SQL_MERGE_build_upsert_bool( B_IS_SET(WDay_Prog[ m ][ p ][ ( d<14 ? 0 : 1 ) ], (d<14 ? d : (d - 14)) ), SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
			}
		}

		// ----------

		SQL_MERGE_build_legacy_pdata_insert_and_append_to_SQL_statements("TwentyEightDayPrgmData", SQL_MERGE_include_field_name_in_insert_clause, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
	}
}

/* ---------------------------------------------------------- */
DLLEXPORT UNS_32 LEGACY_PDATA_extract_and_store_data(	UNS_8 *pucp,
														const UNS_32 pcontroller_id,
														const BOOL_32 pis_a_600_series,
														char *pSQL_statements,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf )
{
	BulkPgmData1Fields	B1;

	BulkPgmData2Fields	B2;

	BulkPgmData3Fields	B3;

	BulkPgmData4Fields	B4;

	MonthlyPrgmDataFields M[ MONTHS_IN_A_YEAR ];

	POCPrgmDataFields POCP[ 3 ];

	POCScheduleFields POCS[ 3 ][ DAYS_IN_A_WEEK ];

	ProgramPrgmDataFields P[ PROG_LAST ];

	StationPgmDataFields S[ MAX_STATIONS__600_SERIES ];

	TagNamesFields T[ NUM_OF_PREDEFINED_PTAGS ];

	TwentyEightDayPrgmDataFields TW[ 28 ];

	UNS_16 WDay_Prog[ MONTHS_IN_A_YEAR ][ PROG_LAST ][ 2 ];		// 1/28/2016 ajv : We use 28-bits of the available 32-bits.

	// 1/28/2016 ajv : Since the 500 and 600 series have a different number of maximum stations
	// (40 and 48, respectively), we're defining a local variable to ensure we only look through
	// the station data for the correct number of stations.
	UNS_32	lmax_stations;

	UNS_32  rv;

	UNS_32	i, j;

	UNS_8	temp_uns8;

	UNS_16	temp_uns16;

	// ----------

	rv = 0;

	// ----------

	memset( pSQL_statements, 0x00, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

	// ----------

	if( pis_a_600_series )
	{
		lmax_stations = MAX_STATIONS__600_SERIES;
	}
	else
	{
		lmax_stations = MAX_STATIONS__500_SERIES;
	}

	// ----------

	// 1/28/2016 ajv : The data includes a short for the length of the PDATA_SYSINFO_STRUCT
	// struct and then the struct itself.
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowLength );

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.ChainDown );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.FlowMeterAvailableElsewhere );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.MasterHasIrrigateByCapacity );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.MasterHasFlowChecking );

	// ----------

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.FM_Connected );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.FMEnterOwn );

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.FlowMeter );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.XFlowMeter_1 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.XFlowMeter_2 );

	rv += LEGACY_COMMON_extract_uint32( &pucp, &B2.FME_K_1_100000u );
	rv += LEGACY_COMMON_extract_uint32( &pucp, &B2.FME_K_2_100000u );
	rv += LEGACY_COMMON_extract_uint32( &pucp, &B2.FME_K_3_100000u );
	rv += LEGACY_COMMON_extract_uint32( &pucp, &B2.FME_O_1_100000u );
	rv += LEGACY_COMMON_extract_uint32( &pucp, &B2.FME_O_2_100000u );
	rv += LEGACY_COMMON_extract_uint32( &pucp, &B2.FME_O_3_100000u );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].ValveTime );
	}

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &P[ i ].NoFlowShutOff );
	}

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &P[ i ].OverFlowShutOff );
	}

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.IrrigMLB );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.NonIrrigMLB );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B2.MaxFlowNumber );

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.TypeOfMV );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &P[ i ].PumpByProgram );
	}

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.ControllerOff );

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.UseYourOwnNumbers );

	for( i = 0; i < MONTHS_IN_A_YEAR; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &M[ i ].UserETNumbers_100u );
	}

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.ETCountyIndex );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.ETCityIndex );

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.UsingAnETGage );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.LogETPulses );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &P[ i ].DailyETByProg );
	}

	rv += LEGACY_COMMON_extract_uint32( &pucp, &B3.IncludeTime );

	// 1/28/2016 ajv : The Daily ET Percent of Historical Cap is stored in the FTimesMax field
	// for each month. So, extract it from the message and then copy it to the remaining 11
	// monthly fields.
	rv += LEGACY_COMMON_extract_uint16( &pucp, &M[ 0 ].FTimesMax_100u );
	for( i = 1; i < MONTHS_IN_A_YEAR; ++i )
	{
		M[ i ].FTimesMax_100u = M[ 0 ].FTimesMax_100u;
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].DailyETFactor_100u );
	}

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.UseETAveraging_1 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.UseETAveraging_2 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.UseETAveraging_3 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.UseETAveraging_4 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.UseETAveraging_5 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.UseETAveraging_6 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.UseETAveraging_7 );

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.VariableCCInUse );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_Jan_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_Feb_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_Mar_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_Apr_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_May_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_June_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_July_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_Aug_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_Sep_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_Oct_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_Nov_100u );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].VariableCCByProg_Dec_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].StationInUse );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].StationInfo_Rate );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].StationInfo_Size );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].NoWaterDays );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_Jan_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Jan_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Jan_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_Feb_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Feb_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Feb_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_Mar_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Mar_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Mar_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_Apr_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Apr_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Apr_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_May_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_May_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_May_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_June_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_June_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_June_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_July_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_July_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_July_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_Aug_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Aug_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Aug_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_Sep_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Sep_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Sep_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_Oct_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Oct_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Oct_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_Nov_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Nov_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Nov_CTime );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &S[ i ].EditSVar_Dec_PTime_100u );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Dec_HTime_5 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].EditSVar_Dec_CTime );
	}

	for( i = 0; i < MONTHS_IN_A_YEAR; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &M[ i ].Schedule );

		// 2/3/2016 ajv : Move past padding after schedule
		LEGACY_COMMON_extract_uint8( &pucp, &temp_uns8 );

		for( j = 0; j < PROG_LAST; ++j )
		{
			rv += LEGACY_COMMON_extract_uint16( &pucp, &WDay_Prog[ i ][ j ][ 0 ] );
			rv += LEGACY_COMMON_extract_uint16( &pucp, &WDay_Prog[ i ][ j ][ 1 ] );
		}
		rv += LEGACY_COMMON_extract_uint32( &pucp, &M[ i ].SX_ProgramA );
		rv += LEGACY_COMMON_extract_uint32( &pucp, &M[ i ].SX_ProgramB );
		rv += LEGACY_COMMON_extract_uint32( &pucp, &M[ i ].SX_ProgramC );
		rv += LEGACY_COMMON_extract_uint32( &pucp, &M[ i ].SX_ProgramD );
		rv += LEGACY_COMMON_extract_uint32( &pucp, &M[ i ].SX_ProgramE );
		rv += LEGACY_COMMON_extract_uint32( &pucp, &M[ i ].SX_ProgramD1 );
		rv += LEGACY_COMMON_extract_uint32( &pucp, &M[ i ].SX_ProgramD2 );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].PAssign_Norm );
	}

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.TrackEstimatedUsage );

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.RunWeek );

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.DLSavings );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.ManCycleSoak );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint32( &pucp, &P[ i ].HoldOverByProg );
	}

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.DailyETUse12 );

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.UseBudget );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.BudgetOption );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.PercentOfETo_100u );

	for( i = 0; i < MONTHS_IN_A_YEAR; ++i )
	{
		rv += LEGACY_COMMON_extract_uint32( &pucp, &M[ i ].Budgets );
	}

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.RainSwitchInUse );

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B2.RainInfo_Minimum_100u );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B2.RainInfo_MaxHourlyRate_100u );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B2.RainInfo_Maximum_100u );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B2.MaxInchesOfRainHO_100u );

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.AllowNegHO_1 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.AllowNegHO_2 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.AllowNegHO_3 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.AllowNegHO_4 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.AllowNegHO_5 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.AllowNegHO_6 );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B2.AllowNegHO_7 );

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_string( &pucp, S[ i ].Name, STATION_DESCRIPTION_LENGTH );
	}

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].PTags );
	}

	for( i = 0; i < NUM_OF_PREDEFINED_PTAGS; ++i )
	{
		rv += LEGACY_COMMON_extract_string( &pucp, T[ i ].TagNames, PTAG_LENGTH );
	}

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.WS_InUse );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B1.WS_Pause );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.WS_P_Speed );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.WS_R_Speed );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.WS_P_Time );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B1.WS_R_Time );

	if( !pis_a_600_series )
	{
		rv += LEGACY_COMMON_extract_string( &pucp, B3.DTMF_Mode1Addr, 5 );
		rv += LEGACY_COMMON_extract_string( &pucp, B3.DTMF_Mode2Addr, 2 );

		rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.DTMFChannel1 );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.DTMFChannel2 );
	}

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.MSInUse );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &P[ i ].MSByProg );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].MSSensorAssignment );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].MSSetPoint );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].MSMaxWaterDays );
	}

	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.POAFS );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.NOCIC );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].ValvesOnByProgram );
	}

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.HighFlowMargin );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.LowFlowMargin );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &P[ i ].SetExpected );
	}

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.Capacity_NonPump );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.IrrigatingByCapacity );
	rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.TestingFlow );

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowRange1Top );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowRange2Top );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowRange3Top );

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowTolerance1Plus );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowTolerance1Minus );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowTolerance2Plus );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowTolerance2Minus );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowTolerance3Plus );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowTolerance3Minus );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowTolerance4Plus );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.FlowTolerance4Minus );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].ValveCloseTime );
	}

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].ValvesOnInSystem );
	}

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.TailEndsThrowAwayUpperbound );
	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.TailEndsMakeAdjustmentUpperbound );

	rv += LEGACY_COMMON_extract_uint16( &pucp, &B3.IrriElectricalStationLimit );

	for( i = 0; i < PROG_LAST; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &P[ i ].WindInUse );
	}

	if( pis_a_600_series )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &B3.ProgramPrioritiesEnabled );

		for( i = 0; i < PROG_LAST; ++i )
		{
			rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].ProgramPriority );
		}

		rv += LEGACY_COMMON_extract_string( &pucp, B3.UserDefinedETState, (MAX_SCC_LENGTH -1) );
		rv += LEGACY_COMMON_extract_string( &pucp, B3.UserDefinedETCounty, (MAX_SCC_LENGTH -1) );
		rv += LEGACY_COMMON_extract_string( &pucp, B3.UserDefinedETCity, (MAX_SCC_LENGTH -1) );

		for( i = 0; i < 3; ++i )
		{
			rv += LEGACY_COMMON_extract_string( &pucp, POCP[ i ].POC_Name, 42 );
			rv += LEGACY_COMMON_extract_uint8( &pucp, &POCP[ i ].PartOfIrrigation );
			rv += LEGACY_COMMON_extract_uint8( &pucp, &POCP[ i ].AllowIrriDuringMLB );
			rv += LEGACY_COMMON_extract_uint8( &pucp, &POCP[ i ].MVType );
			rv += LEGACY_COMMON_extract_uint8( &pucp, &POCP[ i ].FMInUse );
			rv += LEGACY_COMMON_extract_uint8( &pucp, &POCP[ i ].FMUseKAndO );
			rv += LEGACY_COMMON_extract_uint16( &pucp, &POCP[ i ].MLBDuringIdle );
			rv += LEGACY_COMMON_extract_uint16( &pucp, &POCP[ i ].MLBDuringUse );
			rv += LEGACY_COMMON_extract_uint16( &pucp, &POCP[ i ].MLBDuringIrri );

			for( j = 0; j < DAYS_IN_A_WEEK; ++j )
			{
				rv += LEGACY_COMMON_extract_uint32( &pucp, &POCS[ i ][ j ].OpenTimeA );
				rv += LEGACY_COMMON_extract_uint32( &pucp, &POCS[ i ][ j ].DurationA );
				rv += LEGACY_COMMON_extract_uint32( &pucp, &POCS[ i ][ j ].OpenTimeB );
				rv += LEGACY_COMMON_extract_uint32( &pucp, &POCS[ i ][ j ].DurationB );
			}
		}

		for( i = 0; i < PROG_LAST; ++i )
		{
			rv += LEGACY_COMMON_extract_string( &pucp, P[ i ].UserTag, PTAG_LENGTH );
		}

		rv += LEGACY_COMMON_extract_uint8( &pucp, &B4.PTagsEditableAtController );

		for( i = 0; i < PROG_LAST; ++i )
		{
			rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].PercentAdjust1_100u );
		}

		for( i = 0; i < PROG_LAST; ++i )
		{
			rv += LEGACY_COMMON_extract_uint16( &pucp, &P[ i ].PercentAdjustNumberOfDays1 );
		}

		rv += LEGACY_COMMON_extract_uint8( &pucp, &B4.RReChannelChangeMode );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &B4.RReControllerChannel );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &B4.RReUserChannel );

		rv += LEGACY_COMMON_extract_uint8( &pucp, &B4.DMCInUse );
		rv += LEGACY_COMMON_extract_uint8( &pucp, &B4.DMCLevels );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &B4.DMCMaxFlowRateLevel1 );
		rv += LEGACY_COMMON_extract_uint16( &pucp, &B4.DMCMaxFlowRateLevel2 );

		rv += LEGACY_COMMON_extract_uint16( &pucp, &B4.MLBMVOR );

		rv += LEGACY_COMMON_extract_uint8( &pucp, &B4.MaximumHoldoverFactor );

		rv += LEGACY_COMMON_extract_uint8( &pucp, &B4.SICFunctionalityName );
		rv += LEGACY_COMMON_extract_uint8( &pucp, &B4.SICContactType );
		rv += LEGACY_COMMON_extract_uint8( &pucp, &B4.SICIrrigationToAffect );
		rv += LEGACY_COMMON_extract_uint8( &pucp, &B4.SICActionToTake );
		rv += LEGACY_COMMON_extract_string( &pucp, B4.SICSwitchName, 24 );
	}

	// ----------

	// ETTable.TableDate - not stored
	LEGACY_COMMON_extract_uint16( &pucp, &temp_uns16 );

	for( i = 0; i < 28; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &TW[ i ].DailyET_ET_10000u );
		rv += LEGACY_COMMON_extract_uint8( &pucp, &TW[ i ].DailyET_Status );
		rv += LEGACY_COMMON_extract_uint8( &pucp, &TW[ i ].DailyET_Pad );
	}

	// ----------

	// RainTable.TableDate - not stored
	LEGACY_COMMON_extract_uint16( &pucp, &temp_uns16 );

	for( i = 0; i < 28; ++i )
	{
		rv += LEGACY_COMMON_extract_uint16( &pucp, &TW[ i ].RainTable_Rain_100u );
		rv += LEGACY_COMMON_extract_uint8( &pucp, &TW[ i ].RainTable_Status );
		rv += LEGACY_COMMON_extract_uint8( &pucp, &TW[ i ].RainTable_Pad );
	}

	// ----------

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].FlowStatus );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].IStatus );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].MSReading );
	}

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &S[ i ].MSReadingStatus );
	}

	// ----------

	// add timestamp variable definition "@DT" and set it to the current date & time
	snprintf(pSQL_statements, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS, "DECLARE @DT TImestamp;SET @DT = CURRENT_TIMESTAMP();");

	store_bulkpgmdata1( B1, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_bulkpgmdata2( B2, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_bulkpgmdata3( B3, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_bulkpgmdata4( B4, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_monthlyprgmdata( M, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_pocprgmdata( POCP, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_pocschedule( POCS, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_programprgmdata( P, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_stationpgmdata( S, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_tagnames( T, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_twentyeightdayprgmdata( TW, WDay_Prog, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

