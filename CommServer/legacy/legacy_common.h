/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_LEGACY_COMMON_H
#define _INC_LEGACY_COMMON_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs3000_comm_server_common.h"

#include	"cal_td_utils.h"

#include	"alert_parsing.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	BOOLEAN	unsigned char

// ----------

#define	MAX_STATIONS__600_SERIES		(48)
#define	MAX_STATIONS__500_SERIES		(40)

// ----------

#define POCS_PER_CONTROLLER				(3)

// ----------

#define MAX_SCC_LENGTH					(17)

// ----------

#define	STATION_DESCRIPTION_LENGTH		(40)

// ----------

#define	ET2000e_ROM_VERSION_STRING_LENGTH	(12)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// ***** CENTRAL COMMAND MID's 1000 through 1999	**********************************

#define MID_PING_FROM_PC 1000
#define MID_PING_FROM_PC_RESP 1001

#define MID_UNKNOWN_COMMAND_PC_TO_CONTROLLER_RESP 1999

#define MID_CMOS_TO_PC 1002
#define MID_CMOS_TO_PC_RESP 1003

#define MID_CMOS_TO_CONTROLLER 1004
#define MID_CMOS_TO_CONTROLLER_RESP 1005

#define MID_PDATA_TO_PC 1006
#define MID_PDATA_TO_PC_RESP 1007

#define MID_PDATA_TO_CONTROLLER 1008
#define MID_PDATA_TO_CONTROLLER_RESP 1009

#define MID_ALERTS_TO_PC 1010
#define MID_ALERTS_TO_PC_RESP 1011

#define MID_SETTIMEDATE 1012
#define MID_SETTIMEDATE_SUCCESS_RESP 1013
#define MID_SETTIMEDATE_FAILED_RESP 1014

#define MID_CLEARMLB 1016
#define MID_CLEARMLB_SUCCESS_RESP 1017
#define MID_CLEARMLB_FAILED_RESP 1018
#define MID_CLEARMLB_RESP 1019

#define MID_SETHO_ALL_STATIONS 1020
#define MID_SETHO_BY_PROG 1021
#define MID_SETHO_BY_STATION 1022
#define MID_SETHO_RESP 1023

#define MID_SETNOWDAYS_ALL_STATIONS 1024
#define MID_SETNOWDAYS_BY_PROG 1025
#define MID_SETNOWDAYS_BY_STATION 1026
#define MID_SETNOWDAYS_OK_RESP 1027

#define MID_TURNON 1028
#define MID_TURNON_RESP 1029
#define MID_TURNOFF 1030
#define MID_TURNOFF_RESP 1031

#define MID_PASSWORDS_RESET 1032
#define MID_PASSWORDS_RESET_RESP 1033

#define MID_PASSWORDS_TO_PC 1034
#define MID_PASSWORDS_TO_PC_RESP 1035

#define MID_PASSWORDS_TO_CONTROLLER 1036
#define MID_PASSWORDS_TO_CONTROLLER_RESP 1037

#define MID_PASSWORDS_UNLK 1038
#define MID_PASSWORDS_UNLK_RESP 1039

#define MID_SHARING_GET_ET 1040
#define MID_SHARING_GET_ET_RESP_OK 1041
#define MID_SHARING_GET_ET_RESP_NOT_A_G 1042
#define MID_SHARING_GET_ET_RESP_GAGE_NOT_IN_USE 1043

#define MID_SHARING_GET_RAIN 1044
#define MID_SHARING_GET_RAIN_RESP_OK 1045
#define MID_SHARING_GET_RAIN_RESP_NOT_AN_RB 1046

#define MID_SHARING_ETRAIN_TO_CONTROLLER 1048
#define MID_SHARING_ETRAIN_TO_CONTROLLER_RESP_OK 1049
#define MID_SHARING_ETRAIN_TO_CONTROLLER_RESP_ERROR 1050

#define MID_SHARING_RAIN_POLL 1060
#define MID_SHARING_RAIN_POLL_RESP_NOT_RB 1061
#define MID_SHARING_RAIN_POLL_RESP_NO_RAIN 1062
#define MID_SHARING_RAIN_POLL_RESP_RAIN 1063

#define MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER 1070
#define MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER_RESP_OK 1071
#define MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER_DATES_DONT_MATCH 1072

#define MID_ENABLE_FOAL_DEBUG_RECORD 1100

#define MID_SEND_FLOW_RECORDER_RECORDS_TO_PC 1110
#define MID_SEND_FLOW_RECORDER_RECORDS_TO_PC_RESP 1111

#define MID_SETNOWDAYS_NUMBER_OF_DAYS_OUT_OF_RANGE_RESP 1120  // if requested to set >31 days of no water
#define MID_SETNOWDAYS_STATION_NUMBER_OUT_OF_RANGE_RESP 1121  // if station number is not in the range 0 to 39
#define MID_SETNOWDAYS_PROG_ASS_OUT_OF_RANGE_RESP 1122		 // if prog ass is not in the range 0 to 6

#define MID_LOGLINES_CONTROLLER_TO_PC 1200
#define MID_LOGLINES_CONTROLLER_TO_PC_RESP 1201

#define MID_CONTROLLER_REPORT_DATA_TO_PC 1210
#define MID_CONTROLLER_REPORT_DATA_TO_PC_RESP 1211

#define MID_LEGACY_STATION_REPORT_DATA_TO_PC 1220
#define MID_LEGACY_STATION_REPORT_DATA_TO_PC_RESP 1221

#define MID_MVOR_TO_CONTROLLER 1230
#define MID_MVOR_TO_CONTROLLER_RESP 1230

#define MID_LIGHTS_TO_PC 1240
#define MID_LIGHTS_TO_PC_RESP 1241

#define MID_LIGHTS_TO_CONTROLLER 1242
#define MID_LIGHTS_TO_CONTROLLER_RESP 1243

#define MID_MANUALSTUFF_TO_PC 1250
#define MID_MANUALSTUFF_TO_PC_RESP 1251
#define MID_MANUALSTUFF_TO_PC_NODATA 1252

#define MID_MANUALSTUFF_TO_CONTROLLER 1255

#define MID_MANUALSTUFF_TO_CONTROLLER_RESP 1256  // all OK
#define MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK01 1257
#define MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK02 1258
#define MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK04 1260
#define MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK08 1264
#define MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK10 1265

#define MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER 1400
#define MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER_RESP_START 1401  // responds w/ stations in use array
#define MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER_RESP_STOP 1402  // simple response...no accompanying data

#define MID_DIRECTACCESS_SCREEN_UPDATE_TO_PC 1403

#define MID_DIRECTACCESS_STARTTIMER_TO_CONTROLLER 1405

#define MID_DIRECTACCESS_CHANGESCREEN_TO_CONTROLLER 1406

#define MID_DIRECTACCESS_STOPKEY_TO_CONTROLLER 1407

#define MID_DIRECTACCESS_TEST_STATION_TO_CONTROLLER 1408
#define MID_DIRECTACCESS_TEST_PROG_TO_CONTROLLER 1409
#define MID_DIRECTACCESS_TEST_ALL_TO_CONTROLLER 1410

#define MID_DIRECTACCESS_MANUAL_STATION_TO_CONTROLLER 1411
#define MID_DIRECTACCESS_MANUAL_PROG_TO_CONTROLLER 1412
#define MID_DIRECTACCESS_MANUAL_ALL_TO_CONTROLLER 1413

#define MID_DIRECTACCESS_TURN_ON_TO_CONTROLLER 1414
#define MID_DIRECTACCESS_TURN_OFF_TO_CONTROLLER 1415

#define MID_DIRECTACCESS_GOTO_STATION_TO_CONTROLLER 1416

#define MID_DIRECTACCESS_MVOR_TO_CONTROLLER 1417

#define MID_DIRECTACCESS_REFRESH_REQUEST_TO_CONTROLLER 1418

//#define nlu_MID_TEST_PACKET_TO_CONTROLLER_NO_ECHO 1440  // this one currently not used cause for Rodger the lack

#define MID_TEST_PACKET_TO_CONTROLLER_TO_ECHO 1441
#define MID_TEST_PACKET_TO_CONTROLLER_TO_ECHO_RESP 1442

#define MID_DERATE_TABLE_TO_PC 1444
#define MID_DERATE_TABLE_TO_PC_RESP 1445

#define MID_DERATE_TABLE_TO_CONTROLLER 1446
#define MID_DERATE_TABLE_TO_CONTROLLER_RESP 1447

#define MID_TEST_PACKET_BETWEEN_CONTROLLERS_TO_ECHO 1450
#define MID_TEST_PACKET_BETWEEN_CONTROLLERS_TO_ECHO_RESP 1451

#define MID_SETNOWDAYS_BY_PTAG 1452
#define MID_SETNOWDAYS_BY_PTAG_RESP 1453

#define MID_SETSTARTTIME_BY_PTAG 1455
#define MID_SETSTARTTIME_BY_PTAG_RESP 1456
#define MID_SETSTARTTIME_TIME_OUT_OF_RANGE_RESP 1457
#define MID_SETSTARTTIME_MONTH_OUT_OF_RANGE_RESP 1458
#define MID_SETSTARTTIME_12_MONTH_CUR_MONTH_CHANGED_RESP 1459

#define MID_SETSTOPTIME_BY_PTAG 1460
#define MID_SETSTOPTIME_BY_PTAG_RESP 1461
#define MID_SETSTOPTIME_TIME_OUT_OF_RANGE_RESP 1462

#define MID_SETHO_BY_PTAG 1465
#define MID_SETHO_BY_PTAG_RESP 1466
#define MID_SETHO_HO_OUT_OF_RANGE_RESP 1467

#define MID_PERCENTADJUST_BY_PTAG 1470
#define MID_PERCENTADJUST_BY_PTAG_RESP 1471
#define MID_PERCENTADJUST_PERCENTAGE_OUT_OF_RANGE_RESP 1472
#define MID_PERCENTADJUST_DAYS_OUT_OF_RANGE_RESP 1473

#define MID_PTAG_DOES_NOT_EXIST_RESP 1475

#define MID_PERCENTADJUST_BY_PROG 1480
#define MID_PERCENTADJUST_BY_PROG_RESP 1481
#define MID_PERCENTADJUST_ALL_PROGS 1482
#define MID_PERCENTADJUST_ALL_PROGS_RESP 1483
#define MID_PERCENTADJUST_PROG_OUT_OF_RANGE_RESP 1484

#define MID_LIGHTS_COMMAND_FROM_PC	1485
#define MID_LIGHTS_COMMAND_FROM_PC_GOOD_RESP 1486
#define MID_LIGHTS_COMMAND_FROM_PC_BAD_RESP 1487
#define MID_LIGHTS_COMMAND_FROM_PC_BAD_TIME_RESP 1488

#define MID_SEND_DMC_RECORDER_RECORDS_TO_PC 1490
#define MID_SEND_DMC_RECORDER_RECORDS_TO_PC_RESP 1491

#define MID_PDATA_CHANGES_TO_PC 1500
#define MID_PDATA_CHANGES_TO_PC_RESP 1501

#define MID_PDATA_CHANGES_TO_CONTROLLER 1505
#define MID_PDATA_CHANGES_TO_CONTROLLER_RESP 1506

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern INT_32 lastIndexOf( char *pstr, char *psubstr, const UNS_32 pstr_len );

// ----------

extern DATE_TIME LEGACY_COMMON_swap_legacy_to_CS3000_datetime( const DATE_TIME pvalue, DATE_TIME *new_value );

// ----------

extern UNS_32 ALERTS_pull_legacy_object_off_pile( ALERTS_PILE_STRUCT *const ppile, void *pdest, UNS_32 *pindex_ptr, const UNS_32 psize_of_object );

// ----------

extern UNS_16 LEGACY_COMMON_extract_et_rain( const UNS_32 pvalue );

// ----------

extern UNS_16 LEGACY_COMMON_swap_endianness_uint16( const UNS_16 pvalue );

extern INT_16 LEGACY_COMMON_swap_endianness_int16( const INT_16 pvalue );

extern UNS_32 LEGACY_COMMON_swap_endianness_uint32( const UNS_32 pvalue );

extern DATE_TIME LEGACY_COMMON_swap_endianness_datetime( const DATE_TIME pvalue );

// ----------

extern UNS_32 LEGACY_COMMON_extract_uint8( UNS_8 **pucp, UNS_8 *pvalue );

extern UNS_32 LEGACY_COMMON_extract_uint16( UNS_8 **pucp, UNS_16 *pvalue );

extern UNS_32 LEGACY_COMMON_extract_uint32( UNS_8 **pucp, UNS_32 *pvalue );

extern UNS_32 LEGACY_COMMON_extract_string(UNS_8 **pucp, char *pvalue, const UNS_32 pstrlen);

// ----------

extern UNS_32 LEGACY_COMMON_extract_and_store_uint8(	UNS_8 **pucp,
														char *pfield_name,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str );

extern UNS_32 LEGACY_COMMON_extract_and_store_uint16(	UNS_8 **pucp,
														char *pfield_name,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str );

extern UNS_32 LEGACY_COMMON_extract_and_store_uint32(	UNS_8 **pucp,
														char *pfield_name,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str );

extern UNS_32 LEGACY_COMMON_extract_and_store_string(	UNS_8 **pucp,
														const UNS_32 pstrlen,
														char *pfield_name,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _INC_LEGACY_COMMON_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

