/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"legacy_cmos.h"

#include	"legacy_common.h"

// Required for SQL functions
#include	"sql_merge.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	LEGACY_MANUAL_START_TIMES_PER_MANUAL_PROG	(9)

#define	LEGACY_MANUAL_START_TIMES_PER_MANUAL_PROG_IN_DATABASE	(6)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	char	MP1_Name[ 26 ];
	UNS_8	MP1_Sch[ DAYS_IN_A_WEEK ];
	UNS_8	MP1_CompilerSpacer;
	UNS_32	MP1_StartTime[ LEGACY_MANUAL_START_TIMES_PER_MANUAL_PROG ];
	UNS_16	MP1_StartDate;
	UNS_16	MP1_StopDate;
	UNS_8	MP1_UseHO;
	UNS_8	MP1_SuspendProgrammed;

	char	MP2_Name[ 26 ];
	UNS_8	MP2_Sch[ DAYS_IN_A_WEEK ];
	UNS_8	MP2_CompilerSpacer;
	UNS_32	MP2_StartTime[ LEGACY_MANUAL_START_TIMES_PER_MANUAL_PROG ];
	UNS_16	MP2_StartDate;
	UNS_16	MP2_StopDate;
	UNS_8	MP2_UseHO;
	UNS_8	MP2_SuspendProgrammed;

	UNS_16	WT_RunForSeconds;

	UNS_8	HO_InUse;
	UNS_8	HO_Dummy1;
	UNS_32	HO_StartTime;
	UNS_32	HO_StopTime;
	UNS_8	HO_Days[ DAYS_IN_A_WEEK ];
	UNS_8	HO_Dummy2;
	 
	UNS_32	MVOR_FirstOpen_Sch[ DAYS_IN_A_WEEK ];
	UNS_32	MVOR_FirstClose_Sch[ DAYS_IN_A_WEEK ];
	UNS_32	MVOR_SecondOpen_Sch[ DAYS_IN_A_WEEK ];
	UNS_32	MVOR_SecondClose_Sch[ DAYS_IN_A_WEEK ];

} ManualFields;

// ----------

typedef struct
{

	UNS_16	StationNumber;
	UNS_8	InUse;
	UNS_16	MP1_Seconds;
	UNS_16	MP2_Seconds;
	UNS_16	WT_Order;

} ManualStationFields;

/* ---------------------------------------------------------- */
static void store_manual(	const ManualFields pdata,
							const UNS_32 pcontroller_id,
							const BOOL_32 pis_a_600_series,
							char *pSQL_statements,
							char *pSQL_search_condition_str,
							char *pSQL_update_str,
							char *pSQL_insert_fields_str,
							char *pSQL_insert_values_str,
							char *pSQL_single_merge_statement_buf )
{
	char	str_20[ 20 ];

	UNS_32	i;

	// ----------

	memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

	// ----------

	SQL_MERGE_build_search_condition_uint32( "ControllerID", pcontroller_id, pSQL_search_condition_str );
	SQL_MERGE_build_search_condition_sql_cursor_var("MpTimestamp", "@DT", pSQL_search_condition_str);

	// ----------

	SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "MpTimestamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	SQL_MERGE_build_upsert_string( pdata.MP1_Name, sizeof(pdata.MP1_Name), SQL_MERGE_include_field_name_in_insert_clause, "MP1_Name", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	for( i = 0; i < DAYS_IN_A_WEEK; ++i )
	{
		snprintf( str_20, sizeof(str_20), "MP1_Sch%d", (i + 1) );

		SQL_MERGE_build_upsert_bool( pdata.MP1_Sch[ i ], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}

	for (i = 0; i < LEGACY_MANUAL_START_TIMES_PER_MANUAL_PROG_IN_DATABASE; ++i)
	{
		snprintf( str_20, sizeof(str_20), "MP1_StartTime%d", (i + 1) );

		SQL_MERGE_build_upsert_bool( pdata.MP1_StartTime[ i ], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}

	SQL_MERGE_build_upsert_date( pdata.MP1_StartDate, SQL_MERGE_include_field_name_in_insert_clause, "MP1_StartDate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_date( pdata.MP1_StopDate, SQL_MERGE_include_field_name_in_insert_clause, "MP1_StopDate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	SQL_MERGE_build_upsert_bool( pdata.MP1_UseHO, SQL_MERGE_include_field_name_in_insert_clause, "MP1_UseHO", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.MP1_SuspendProgrammed, SQL_MERGE_include_field_name_in_insert_clause, "MP1_SuspendProgrammed", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	SQL_MERGE_build_upsert_string( pdata.MP2_Name, sizeof(pdata.MP2_Name), SQL_MERGE_include_field_name_in_insert_clause, "MP2_Name", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	for( i = 0; i < DAYS_IN_A_WEEK; ++i )
	{
		snprintf( str_20, sizeof(str_20), "MP2_Sch%d", (i + 1) );

		SQL_MERGE_build_upsert_bool( pdata.MP2_Sch[ i ], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}

	for (i = 0; i < LEGACY_MANUAL_START_TIMES_PER_MANUAL_PROG_IN_DATABASE; ++i)
	{
		snprintf( str_20, sizeof(str_20), "MP2_StartTime%d", (i + 1) );

		SQL_MERGE_build_upsert_bool( pdata.MP2_StartTime[ i ], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}

	SQL_MERGE_build_upsert_date( pdata.MP2_StartDate, SQL_MERGE_include_field_name_in_insert_clause, "MP2_StartDate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_date( pdata.MP2_StopDate, SQL_MERGE_include_field_name_in_insert_clause, "MP2_StopDate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	SQL_MERGE_build_upsert_bool( pdata.MP2_UseHO, SQL_MERGE_include_field_name_in_insert_clause, "MP2_UseHO", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_bool( pdata.MP2_SuspendProgrammed, SQL_MERGE_include_field_name_in_insert_clause, "MP2_SuspendProgrammed", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	// ----------

	SQL_MERGE_build_upsert_uint32( pdata.WT_RunForSeconds, SQL_MERGE_include_field_name_in_insert_clause, "WT_RunForSeconds", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	
	// ----------

	SQL_MERGE_build_upsert_bool( pdata.HO_InUse, SQL_MERGE_include_field_name_in_insert_clause, "HO_InUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	SQL_MERGE_build_upsert_uint32( pdata.HO_StartTime, SQL_MERGE_include_field_name_in_insert_clause, "HO_StartTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	SQL_MERGE_build_upsert_uint32( pdata.HO_StopTime, SQL_MERGE_include_field_name_in_insert_clause, "HO_StopTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

	for( i = 0; i < DAYS_IN_A_WEEK; ++i )
	{
		snprintf( str_20, sizeof(str_20), "HO_Days%d", (i + 1) );

		SQL_MERGE_build_upsert_bool( pdata.HO_Days[ i ], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
	}

	// ----------

	for( i = 0; i < DAYS_IN_A_WEEK; ++i )
	{
		snprintf( str_20, sizeof(str_20), "MVOR_Open_Sch%d", (i + 1) );
		SQL_MERGE_build_upsert_uint32(pdata.MVOR_FirstOpen_Sch[i], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);

		snprintf( str_20, sizeof(str_20), "MVOR_Close_Sch%d", (i + 1) );
		SQL_MERGE_build_upsert_uint32(pdata.MVOR_FirstClose_Sch[i], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);

		snprintf( str_20, sizeof(str_20), "MVOR_2ndOpen_Sch%d", (i + 1) );
		SQL_MERGE_build_upsert_uint32(pdata.MVOR_SecondOpen_Sch[i], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);

		snprintf( str_20, sizeof(str_20), "MVOR_2ndClose_Sch%d", (i + 1) );
		SQL_MERGE_build_upsert_uint32(pdata.MVOR_SecondClose_Sch[i], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
	}

	// ----------

	SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements("Manual", pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);

}

/* ---------------------------------------------------------- */
static void store_manual_stations(	const ManualStationFields pdata[],
									const UNS_32 pcontroller_id,
									const BOOL_32 pis_a_600_series,
									char *pSQL_statements,
									char *pSQL_search_condition_str,
									char *pSQL_update_str,
									char *pSQL_insert_fields_str,
									char *pSQL_insert_values_str,
									char *pSQL_single_merge_statement_buf )
{
	// 1/28/2016 ajv : Since the 500 and 600 series have a different number of maximum stations
	// (40 and 48, respectively), we're defining a local variable to ensure we only look through
	// the station data for the correct number of stations.
	UNS_32	lmax_stations;

	UNS_32	s;

	// ----------

	if( pis_a_600_series )
	{
		lmax_stations = MAX_STATIONS__600_SERIES;
	}
	else
	{
		lmax_stations = MAX_STATIONS__500_SERIES;
	}

	// ----------

	for( s = 0; s < lmax_stations; ++s )
	{

		memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

		// ----------

		SQL_MERGE_build_search_condition_uint32( "ControllerID", pcontroller_id, pSQL_search_condition_str );
		SQL_MERGE_build_search_condition_sql_cursor_var("MpTimestamp", "@DT", pSQL_search_condition_str);
		SQL_MERGE_build_search_condition_uint32( "StationNumber", s, pSQL_search_condition_str );

		// ----------

		SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "MpTimestamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( s, SQL_MERGE_include_field_name_in_insert_clause, "StationNumber", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		SQL_MERGE_build_upsert_uint32( pdata[ s ].InUse, SQL_MERGE_include_field_name_in_insert_clause, "InUse", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		SQL_MERGE_build_upsert_uint32( pdata[ s ].MP1_Seconds, SQL_MERGE_include_field_name_in_insert_clause, "MP1_Seconds", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( pdata[ s ].MP2_Seconds, SQL_MERGE_include_field_name_in_insert_clause, "MP2_Seconds", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		SQL_MERGE_build_upsert_uint32( pdata[ s ].WT_Order, SQL_MERGE_include_field_name_in_insert_clause, "WT_Order", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		// ----------

		SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements("ManualStations", pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);

	}
}

/* ---------------------------------------------------------- */
DLLEXPORT UNS_32 LEGACY_MANUAL_extract_and_store_data(	UNS_8 *pucp,
														const UNS_32 pcontroller_id,
														const UNS_8 pis_a_600_series,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf )
{
	ManualFields		M;

	ManualStationFields	MS[ 48 ];

	// 1/28/2016 ajv : Since the 500 and 600 series have a different number of maximum stations
	// (40 and 48, respectively), we're defining a local variable to ensure we only look through
	// the station data for the correct number of stations.
	UNS_32	lmax_stations;

	UNS_32	i;

	UNS_32  rv;

	UNS_8	temp_uns8;

	UNS_16	temp_uns16;

	// ----------

	rv = 0;

	// ----------

	memset( pSQL_statements, 0x00, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	// ----------

	if( pis_a_600_series )
	{
		lmax_stations = MAX_STATIONS__600_SERIES;
	}
	else
	{
		lmax_stations = MAX_STATIONS__500_SERIES;
	}

	// ----------

	for( i = 0; i < lmax_stations; ++i )
	{
		rv += LEGACY_COMMON_extract_uint8( &pucp, &MS[ i ].InUse );
	}

	if( (*pucp & 0x01) == 0x01 )	// Manual Program 1
	{
		// 2/3/2016 ajv : Move past the piece identifier
		LEGACY_COMMON_extract_uint8( &pucp, &temp_uns8 );

		// 2/3/2016 ajv : Move past the length bytes
		LEGACY_COMMON_extract_uint16( &pucp, &temp_uns16 );
		
		LEGACY_COMMON_extract_string( &pucp, M.MP1_Name, sizeof(M.MP1_Name) );

		for( i = 0; i < DAYS_IN_A_WEEK; ++i )
		{
			LEGACY_COMMON_extract_uint8( &pucp, &M.MP1_Sch[ i ] );
		}

		LEGACY_COMMON_extract_uint8( &pucp, &M.MP1_CompilerSpacer );

		for( i = 0; i < LEGACY_MANUAL_START_TIMES_PER_MANUAL_PROG; ++i )
		{
			LEGACY_COMMON_extract_uint32( &pucp, &M.MP1_StartTime[ i ] );
		}

		LEGACY_COMMON_extract_uint16( &pucp, &M.MP1_StartDate );
		LEGACY_COMMON_extract_uint16( &pucp, &M.MP1_StopDate );

		LEGACY_COMMON_extract_uint8( &pucp, &M.MP1_UseHO );
		LEGACY_COMMON_extract_uint8( &pucp, &M.MP1_SuspendProgrammed );

		for( i = 0; i < lmax_stations; ++i )
		{
			LEGACY_COMMON_extract_uint16( &pucp, &MS[ i ].MP1_Seconds );
		}
	}

	if( (*pucp & 0x02) == 0x02 )	// Manual Program 2
	{
		// 2/3/2016 ajv : Move past the piece identifier
		LEGACY_COMMON_extract_uint8( &pucp, &temp_uns8 );

		// 2/3/2016 ajv : Move past the length bytes
		LEGACY_COMMON_extract_uint16( &pucp, &temp_uns16 );

		LEGACY_COMMON_extract_string( &pucp, M.MP2_Name, sizeof(M.MP2_Name) );

		for( i = 0; i < DAYS_IN_A_WEEK; ++i )
		{
			LEGACY_COMMON_extract_uint8( &pucp, &M.MP2_Sch[ i ] );
		}

		LEGACY_COMMON_extract_uint8( &pucp, &M.MP2_CompilerSpacer );

		for( i = 0; i < LEGACY_MANUAL_START_TIMES_PER_MANUAL_PROG; ++i )
		{
			LEGACY_COMMON_extract_uint32( &pucp, &M.MP2_StartTime[ i ] );
		}

		LEGACY_COMMON_extract_uint16( &pucp, &M.MP2_StartDate );
		LEGACY_COMMON_extract_uint16( &pucp, &M.MP2_StopDate );

		LEGACY_COMMON_extract_uint8( &pucp, &M.MP2_UseHO );
		LEGACY_COMMON_extract_uint8( &pucp, &M.MP2_SuspendProgrammed );

		for( i = 0; i < lmax_stations; ++i )
		{
			LEGACY_COMMON_extract_uint16( &pucp, &MS[ i ].MP2_Seconds );
		}
	}

	if( (*pucp & 0x04) == 0x04 )	// Walk-Thru
	{
		// 2/3/2016 ajv : Move past the piece identifier
		LEGACY_COMMON_extract_uint8( &pucp, &temp_uns8 );

		// 2/3/2016 ajv : Move past the length bytes
		LEGACY_COMMON_extract_uint16( &pucp, &temp_uns16 );

		LEGACY_COMMON_extract_uint16( &pucp, &M.WT_RunForSeconds );

		for( i = 0; i < lmax_stations; ++i )
		{
			LEGACY_COMMON_extract_uint16( &pucp, &MS[ i ].WT_Order );
		}
	}

	if( (*pucp & 0x08) == 0x08 )	// Scheduled Use of Hold-Over
	{
		// 2/3/2016 ajv : Move past the piece identifier
		LEGACY_COMMON_extract_uint8( &pucp, &temp_uns8 );

		// 2/3/2016 ajv : Move past the length bytes
		LEGACY_COMMON_extract_uint16( &pucp, &temp_uns16 );

		LEGACY_COMMON_extract_uint8( &pucp, &M.HO_InUse );
		LEGACY_COMMON_extract_uint8( &pucp, &M.HO_Dummy1 );

		LEGACY_COMMON_extract_uint32( &pucp, &M.HO_StartTime );
		LEGACY_COMMON_extract_uint32( &pucp, &M.HO_StopTime );

		for( i = 0; i < DAYS_IN_A_WEEK; ++i )
		{
			LEGACY_COMMON_extract_uint8( &pucp, &M.HO_Days[ i ] );
		}

		LEGACY_COMMON_extract_uint8( &pucp, &M.HO_Dummy2 );
	}

	if( (*pucp & 0x10) == 0x10 )	// Master Valve Override
	{
		// 2/3/2016 ajv : Move past the piece identifier
		LEGACY_COMMON_extract_uint8( &pucp, &temp_uns8 );

		// 2/3/2016 ajv : Move past the length bytes
		LEGACY_COMMON_extract_uint16( &pucp, &temp_uns16 );

		for( i = 0; i < DAYS_IN_A_WEEK; ++i )
		{
			LEGACY_COMMON_extract_uint32( &pucp, &M.MVOR_FirstOpen_Sch[ i ] );
		}

		for (i = 0; i < DAYS_IN_A_WEEK; ++i)
		{
			LEGACY_COMMON_extract_uint32(&pucp, &M.MVOR_FirstClose_Sch[i]);
		}

		for (i = 0; i < DAYS_IN_A_WEEK; ++i)
		{
			LEGACY_COMMON_extract_uint32(&pucp, &M.MVOR_SecondOpen_Sch[i]);
		}

		for (i = 0; i < DAYS_IN_A_WEEK; ++i)
		{
			LEGACY_COMMON_extract_uint32(&pucp, &M.MVOR_SecondClose_Sch[i]);
		}
	}

	// ----------

	// add timestamp variable definition "@DT" and set it to the current date & time
	snprintf(pSQL_statements, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS, "DECLARE @DT TImestamp;SET @DT = CURRENT_TIMESTAMP();");
	
	store_manual( M, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_search_condition_str, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );
	store_manual_stations( MS, pcontroller_id, pis_a_600_series, pSQL_statements, pSQL_search_condition_str, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

