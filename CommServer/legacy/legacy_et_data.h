/* ---------------------------------------------------------- */

#ifndef _INC_LEGACY_ET_DATA_H
#define _INC_LEGACY_ET_DATA_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs3000_comm_server_common.h"

#include	"legacy_common.h"

#include	"cal_td_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define LEGACY_NO_OF_STATES		(13)

#define LEGACY_NO_OF_COUNTIES	(65)

#define LEGACY_NO_OF_CITIES		(154)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	char county[ MAX_SCC_LENGTH ];

	unsigned short stateIndex;

} COUNTY_INFO;

// ----------

typedef struct
{
	char city[ MAX_SCC_LENGTH ];

	unsigned short countyIndex;

	unsigned short ET_us_100u[ MONTHS_IN_A_YEAR ];

} CITY_INFO;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern const char States[ LEGACY_NO_OF_STATES ][ MAX_SCC_LENGTH ];

extern const COUNTY_INFO Counties[ LEGACY_NO_OF_COUNTIES ];

extern const CITY_INFO ET_PredefinedData[ LEGACY_NO_OF_CITIES ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
