/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_LEGACY_ALERTS
#define _INC_LEGACY_ALERTS

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs3000_comm_server_common.h"

#include	"alert_parsing.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define ENGLISH     (0)
#define SPANISH     (1)

#define Language	(ENGLISH)

// ----------

#define LEGACY_DATESTR_include_year		(100)
#define LEGACY_DATESTR_short_year		(200)
#define LEGACY_DATESTR_show_dow			(300)

#define LEGACY_DATESTR_include_year_not	(150)
#define LEGACY_DATESTR_short_year_not	(250)
#define LEGACY_DATESTR_show_dow_not		(350)

// ----------

#define EEWRITE_REASON_12MONTH_MASK					0x8000

#define EEWRITE_REASON_COMMLINK						0x0001
#define EEWRITE_REASON_KEYPAD					 	0x0002
#define EEWRITE_REASON_ZEROED_DERATE_TABLE		 	0x0003
#define EEWRITE_REASON_NEWROMS						0x0004
#define EEWRITE_REASON_CHECKSUM_UPDATE				0x0005
#define EEWRITE_REASON_NORMAL_DERATE_UPDATE			0x0006
#define EEWRITE_REASON_AUTO_ADJUSTED			 	0x0007
#define EEWRITE_REASON_EXPECTED_GPM_SET			  	0x0008
#define EEWRITE_REASON_HIT_A_STARTTIME				0x0009
#define EEWRITE_REASON_MOISTURE_SENSOR_READ			0x000A
#define EEWRITE_REASON_SHARING_WEATHER				0x000B
#define EEWRITE_REASON_ITS_RAINING					0x000C
#define EEWRITE_REASON_DAY_CHANGE					0x000D
#define EEWRITE_REASON_MONTH_CHANGE					0x000E
#define EEWRITE_REASON_NORMAL_STARTUP				0x0010
#define EEWRITE_REASON_START_OF_ET_DAY				0x0011
#define EEWRITE_REASON_NEW_CITY						0x0012
#define EEWRITE_REASON_SET_BY_CMOS					0x0013
#define EEWRITE_REASON_DONT_SHOW_A_REASON			0x0014
#define EEWRITE_REASON_FCC_EXPECTED_CHANGED			0x0015
#define EEWRITE_REASON_FCC_RAN_A_CYCLE   			0x0016
#define EEWRITE_REASON_CMOS_VERSION_UPDATE			0x0017
#define EEWRITE_REASON_CMOS_FAILED_OR_NEW			0x0018
#define EEWRITE_REASON_RRE_REMOTE					0x001A

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef enum
{
	LEGACY_AID_USER_RESET = 1,
	LEGACY_AID_EE_FAILED = 2,
	LEGACY_AID_SRAM_FAILED = 3,
	LEGACY_AID_POWER_FAIL = 4,
	LEGACY_AID_ROM_UPDATE = 5,
	LEGACY_AID_ROM_VERSION_UNRECOGNIZABLE = 6,
	LEGACY_AID_UPDATED_CMOS = 7,
	LEGACY_AID_CMOS_FAILED = 8,
	LEGACY_AID_PROGRAM_RESTART = 9,
	LEGACY_AID_EXCEPTION = 10,
	LEGACY_AID_ERROR = 11,
	LEGACY_AID_UNIT_COMMUNICATED = 100,
	LEGACY_AID_COMM_COMMAND_RCVD = 101,
	LEGACY_AID_MLB_BREAK = 200,
	LEGACY_AID_MLB_REMINDER = 201,
	LEGACY_AID_MLB_CLEAR_REQUEST_KEYPAD = 202,
	LEGACY_AID_MLB_CLEAR_REQUEST_COMMLINK = 203,
	LEGACY_AID_MLB_CLEARED = 204,
	LEGACY_AID_SHORT_STATION = 210,
	LEGACY_AID_SHORT_LIGHT = 211,
	LEGACY_AID_SHORT_UNKNOWN_REASON = 214,
	LEGACY_AID_OPEN_STATION = 215,
	LEGACY_AID_SHORT_MV = 216,
	LEGACY_AID_SHORT_PUMP = 217,
	LEGACY_AID_SHORT_COMBINATION = 218,
	LEGACY_AID_FLOW_NOT_CHECKED_WITH_REASONS = 221,
	LEGACY_AID_FLOW_NOT_CHECKED_NO_REASONS = 222,
	LEGACY_AID_FLOW_CHECK_PASSED = 225,
	LEGACY_AID_FLOW_CHECK_LEARNING = 226,
	LEGACY_AID_FINAL_HIGHFLOW = 232,
	LEGACY_AID_FIRST_HIGHFLOW = 233,
	LEGACY_AID_FINAL_LOWFLOW = 242,
	LEGACY_AID_FIRST_LOWFLOW = 243,
	LEGACY_AID_STARTTIME_SKIPPED = 300,
	LEGACY_AID_IRRIGATION_IS_OFF_REMINDER = 303,
	LEGACY_AID_IRRIGATION_TURNED_OFF_WITH_REASON = 306,
	LEGACY_AID_IRRIGATION_TURNED_ON_WITH_REASON = 307,
	LEGACY_AID_CLOCKSET_KEYPAD = 310,
	LEGACY_AID_CLOCKSET_COMMLINK = 311,
	LEGACY_AID_CLOCKSET_DAYLIGHTSAVINGS = 312,
	LEGACY_AID_OVER_BUDGET = 350,
	LEGACY_AID_HIT_STOPTIME = 355,
	LEGACY_AID_SETHO_BYSTATION = 356,
	LEGACY_AID_SETHO_BYPROG = 357,
	LEGACY_AID_SETHO_ALLSTATIONS = 358,
	LEGACY_AID_SETNOWDAYS_BYSTATION = 359,
	LEGACY_AID_SETNOWDAYS_BYPROG = 360,
	LEGACY_AID_SETNOWDAYS_ALLSTATIONS = 361,
	LEGACY_AID_PASSWORDS_RESET_ROMVERSION = 370,
	LEGACY_AID_PASSWORDS_RESET_CMOS = 371,
	LEGACY_AID_PASSWORDS_RESET_COMMLINK = 372,
	LEGACY_AID_PASSWORDS_RECEIVED_FROM_PC = 375,
	LEGACY_AID_PASSWORDS_SENT_TO_PC = 376,
	LEGACY_AID_PASSWORDS_UNLOCKED = 377,
	LEGACY_AID_PASSWORDS_LOGIN = 378,
	LEGACY_AID_PASSWORDS_LOGOUT_KEYPAD_TIMEOUT = 379,
	LEGACY_AID_PASSWORDS_LOGOUT_USER_LOGGED_OUT = 380,
	LEGACY_AID_PASSWORDS_CANCEL_UNLOCK_KEYPAD_TIMEOUT = 381,
	LEGACY_AID_PASSWORDS_CANCEL_UNLOCK_USER_CANCELLED = 382,
	LEGACY_AID_SYSTEM_CAPACITY_AUTOMATICALLY_ADJUSTED_UP = 400,
	LEGACY_AID_MVOR_REQUESTED = 420,
	LEGACY_AID_MVOR_STARTED = 421,
	LEGACY_AID_MVOR_CANCELLED = 422,
	LEGACY_AID_MVOR_STOPPED = 423,
	LEGACY_AID_MVOR_OPEN_REQUEST_IGNORED = 424,
	LEGACY_AID_ENTERED_FORCED = 430,
	LEGACY_AID_LEAVING_FORCED = 431,
	LEGACY_AID_STARTING_RESCAN = 432,
	LEGACY_AID_RAIN_SWITCH_ACTIVE = 440,
	LEGACY_AID_RAIN_SWITCH_INACTIVE = 441,
	LEGACY_AID_MANUALHO_SKIPPED = 450,
	LEGACY_AID_MANUALPROG_SKIPPED = 451,
	LEGACY_AID_EEWRITE_IN_PROCESS = 460,
	LEGACY_AID_ETGAGE_FIRST_ZERO = 461,
	LEGACY_AID_ETGAGE_SECOND_OR_MORE_ZEROS = 462,
	LEGACY_AID_ETGAGE_PERCENT_FULL_EDITED = 463,
	LEGACY_AID_ETGAGE_PULSE = 465,
	LEGACY_AID_ETGAGE_RUNAWAY = 466,
	nlu_LEGACY_AID_ETGAGE_ZEROPULSES = 467,
	LEGACY_AID_ETGAGE_PERCENT_FULL_WARNING = 468,
	LEGACY_AID_RAIN_AFFECTING_IRRIGATION = 470,
	LEGACY_AID_RAIN_24HOUR_TOTAL = 471,
	nlu_LEGACY_AID_STOP_KEY = 475,
	LEGACY_AID_STOP_KEY_WITH_REASON = 476,
	LEGACY_AID_ETTABLE_LOADED = 480,
	LEGACY_AID_HOLDOVER_CHANGED = 482,
	LEGACY_AID_FUSE_REPLACED = 485,
	LEGACY_AID_FUSE_BLOWN = 486,
	LEGACY_AID_FUSE_REMINDER = 487,
	LEGACY_AID_PING_SWREV_ERROR = 490,
	LEGACY_AID_PING_COMMVER_ERROR = 491,
	LEGACY_AID_PROGRAMMED_IRRIGATION_RUNNING_AT_STARTTIME = 492,
	LEGACY_AID_PROGRAMMED_IRRIGATION_STARTED = 493,
	LEGACY_AID_MANUAL_PROGRAM_IRRIGATING = 495,
	LEGACY_AID_MANUAL_HOLDOVER_IRRIGATING = 496,
	LEGACY_AID_MANUAL_SEQUENCE_IRRIGATING = 497,
	LEGACY_AID_IRRIGATION_ENDED = 510,
	LEGACY_AID_DTMF_ACTIVATED = 512,
	LEGACY_AID_DTMF_DEACTIVATED = 513,
	LEGACY_AID_DTMF_STATION_ON = 514,		// carries station number as data
	LEGACY_AID_DTMF_STATION_OFF = 515,		// carries station number as data
	LEGACY_AID_TEST_STATION_STARTED_WITH_REASON = 520,
	LEGACY_AID_TEST_PROGRAM_STARTED_WITH_REASON = 521,
	LEGACY_AID_TEST_ALL_STARTED_WITH_REASON = 522,
	LEGACY_AID_OLD_MANUAL_STATION_STARTED_WITH_REASON = 523,
	LEGACY_AID_OLD_MANUAL_PROGRAM_STARTED_WITH_REASON = 524,
	LEGACY_AID_OLD_MANUAL_ALL_STARTED_WITH_REASON = 525,
	LEGACY_AID_DIRECT_ACCESS_ENTERED = 530,
	LEGACY_AID_DIRECT_ACCESS_EXITED = 531,
	LEGACY_AID_DIRECT_ACCESS_TIMEDOUT = 532,
	LEGACY_AID_CTS_TIMEOUT_A = 540,
	LEGACY_AID_CTS_TIMEOUT_B = 541,
	LEGACY_AID_CTS_TIMEOUT_C = 542,
	LEGACY_AID_CTS_TIMEOUT_D = 543,
	LEGACY_AID_CH1794_FAILED_INIT = 545,
	LEGACY_AID_ETTABLE_SUBSTITUTION = 550,
	LEGACY_AID_ETTABLE_LIMITED_ENTRY = 551,
	LEGACY_AID_MOISTURE_SENSOR_READING = 560,
	LEGACY_AID_MOISTURE_SLAVE_ON_DIFFERENT_PROGRAM = 561,
	LEGACY_AID_MOISTURE_MASTER_TERMINATED = 562,
	LEGACY_AID_MOISTURE_SLAVE_TERMINATED = 563,
	LEGACY_AID_LIGHTS_STATION_ON = 565,
	LEGACY_AID_LIGHTS_STATION_OFF = 566,
	LEGACY_AID_WIND_PAUSE = 567,
	LEGACY_AID_WIND_RESUME = 568,
	LEGACY_AID_IRRIGATION_PAUSED = 569,
	LEGACY_AID_IRRIGATION_RESUMED = 570,
	LEGACY_AID_POC_MLB_BREAK = 600,
	LEGACY_AID_POC_MLB_REMINDER = 601,
	LEGACY_AID_POC_MLB_CLEARED_KEYPAD = 602,
	LEGACY_AID_POC_MLB_CLEARED_COMMLINK = 603,
	LEGACY_AID_POC_MV_SHORT = 604,
	LEGACY_AID_POC_STARTED_USE = 605,
	LEGACY_AID_POC_HIT_ENDTIME = 606,
	LEGACY_AID_POC_STOPPED_USE = 607,
	LEGACY_AID_POC_MLB_CLEARED_SHORT = 608,
	LEGACY_AID_POC_MV_SHORT_DURING_MLB = 609,
	LEGACY_AID_USERDEFINED_PROGRAMTAG_CHANGED = 620,
	LEGACY_AID_PROGRAMTAG_CHANGED = 621,
	LEGACY_AID_PERCENTADJUST_ALL_PROGS = 630,
	LEGACY_AID_PERCENTADJUST_BY_PROG = 631,
	LEGACY_AID_RRE_PASSWORD_LOGIN = 632,
	LEGACY_AID_SIC_STATE_CHANGED = 635,

	// ----------

	// this next one is not actually an alert but an equate
	// for use in the parsing case statement - its helpful
	//
	LEGACY_AID_CHANGE_INDEX_START_OF_NON_PDATA_VARS     = 0xA000,

	// ----------

	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_open_sun      = 0xB380,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_close_sun     = 0xB381,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_open_sun     = 0xB382,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_close_sun    = 0xB383,

	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_open_mon      = 0xB384,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_close_mon     = 0xB385,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_open_mon     = 0xB386,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_close_mon    = 0xB387,

	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_open_tue      = 0xB388,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_close_tue     = 0xB389,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_open_tue     = 0xB38A,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_close_tue    = 0xB38B,

	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_open_wed      = 0xB38C,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_close_wed     = 0xB38D,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_open_wed     = 0xB38E,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_close_wed    = 0xB38F,

	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_open_thu      = 0xB390,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_close_thu     = 0xB391,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_open_thu     = 0xB392,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_close_thu    = 0xB393,

	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_open_fri      = 0xB394,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_close_fri     = 0xB395,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_open_fri     = 0xB396,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_close_fri    = 0xB397,

	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_open_sat      = 0xB398,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_first_close_sat     = 0xB399,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_open_sat     = 0xB39A,
	LEGACY_AID_CHANGE_MVOR_SCHEDULE_second_close_sat    = 0xB39B,

	// ----------

	// lights has a total of (4 * 57) + 1 AID's which is 229 of 'em
	// 57 for each of the 4 stations followed by 1 for the date
	//
	LEGACY_AID_CHANGE_LIGHTS_sta0_text              = 0xB400,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day0_on0          = 0xB401,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day0_on1          = 0xB402,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day0_off0         = 0xB403,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day0_off1         = 0xB404,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day1_on0          = 0xB405,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day1_on1          = 0xB406,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day1_off0         = 0xB407,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day1_off1         = 0xB408,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day2_on0          = 0xB409,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day2_on1          = 0xB40A,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day2_off0         = 0xB40B,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day2_off1         = 0xB40C,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day3_on0          = 0xB40D,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day3_on1          = 0xB40E,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day3_off0         = 0xB40F,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day3_off1         = 0xB410,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day4_on0          = 0xB411,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day4_on1          = 0xB412,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day4_off0         = 0xB413,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day4_off1         = 0xB414,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day5_on0          = 0xB415,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day5_on1          = 0xB416,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day5_off0         = 0xB417,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day5_off1         = 0xB418,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day6_on0          = 0xB419,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day6_on1          = 0xB41A,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day6_off0         = 0xB41B,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day6_off1         = 0xB41C,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day7_on0          = 0xB41D,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day7_on1          = 0xB41E,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day7_off0         = 0xB41F,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day7_off1         = 0xB420,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day8_on0          = 0xB421,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day8_on1          = 0xB422,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day8_off0         = 0xB423,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day8_off1         = 0xB424,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day9_on0          = 0xB425,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day9_on1          = 0xB426,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day9_off0         = 0xB427,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day9_off1         = 0xB428,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day10_on0     = 0xB429,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day10_on1     = 0xB42A,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day10_off0        = 0xB42B,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day10_off1        = 0xB42C,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day11_on0     = 0xB42D,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day11_on1     = 0xB42E,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day11_off0        = 0xB42F,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day11_off1        = 0xB430,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day12_on0     = 0xB431,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day12_on1     = 0xB432,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day12_off0        = 0xB433,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day12_off1        = 0xB434,

	LEGACY_AID_CHANGE_LIGHTS_sta0_day13_on0     = 0xB435,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day13_on1     = 0xB436,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day13_off0        = 0xB437,
	LEGACY_AID_CHANGE_LIGHTS_sta0_day13_off1        = 0xB438,

	LEGACY_AID_CHANGE_LIGHTS_sta1_text              = 0xB439,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day0_on0          = 0xB43A,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day0_on1          = 0xB43B,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day0_off0     = 0xB43C,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day0_off1     = 0xB43D,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day1_on0          = 0xB43E,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day1_on1          = 0xB43F,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day1_off0     = 0xB440,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day1_off1     = 0xB441,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day2_on0          = 0xB442,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day2_on1          = 0xB443,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day2_off0     = 0xB444,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day2_off1     = 0xB445,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day3_on0          = 0xB446,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day3_on1          = 0xB447,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day3_off0     = 0xB448,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day3_off1     = 0xB449,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day4_on0          = 0xB44A,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day4_on1          = 0xB44B,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day4_off0     = 0xB44C,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day4_off1     = 0xB44D,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day5_on0          = 0xB44E,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day5_on1          = 0xB44F,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day5_off0     = 0xB450,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day5_off1     = 0xB451,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day6_on0          = 0xB452,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day6_on1          = 0xB453,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day6_off0     = 0xB454,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day6_off1     = 0xB455,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day7_on0          = 0xB456,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day7_on1          = 0xB457,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day7_off0     = 0xB458,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day7_off1     = 0xB459,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day8_on0          = 0xB45A,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day8_on1          = 0xB45B,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day8_off0     = 0xB45C,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day8_off1     = 0xB45D,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day9_on0          = 0xB45E,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day9_on1          = 0xB45F,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day9_off0     = 0xB460,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day9_off1     = 0xB461,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day10_on0     = 0xB462,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day10_on1     = 0xB463,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day10_off0        = 0xB464,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day10_off1        = 0xB465,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day11_on0     = 0xB466,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day11_on1     = 0xB467,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day11_off0        = 0xB468,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day11_off1        = 0xB469,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day12_on0     = 0xB46A,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day12_on1     = 0xB46B,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day12_off0        = 0xB46C,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day12_off1        = 0xB46D,

	LEGACY_AID_CHANGE_LIGHTS_sta1_day13_on0     = 0xB46E,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day13_on1     = 0xB46F,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day13_off0        = 0xB470,
	LEGACY_AID_CHANGE_LIGHTS_sta1_day13_off1        = 0xB471,

	LEGACY_AID_CHANGE_LIGHTS_sta2_text              = 0xB472,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day0_on0          = 0xB473,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day0_on1          = 0xB474,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day0_off0     = 0xB475,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day0_off1     = 0xB476,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day1_on0          = 0xB477,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day1_on1          = 0xB478,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day1_off0     = 0xB479,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day1_off1     = 0xB47A,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day2_on0          = 0xB47B,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day2_on1          = 0xB47C,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day2_off0     = 0xB47D,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day2_off1     = 0xB47E,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day3_on0          = 0xB47F,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day3_on1          = 0xB480,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day3_off0     = 0xB481,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day3_off1     = 0xB482,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day4_on0          = 0xB483,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day4_on1          = 0xB484,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day4_off0     = 0xB485,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day4_off1     = 0xB486,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day5_on0          = 0xB487,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day5_on1          = 0xB488,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day5_off0     = 0xB489,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day5_off1     = 0xB48A,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day6_on0          = 0xB48B,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day6_on1          = 0xB48C,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day6_off0     = 0xB48D,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day6_off1     = 0xB48E,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day7_on0          = 0xB48F,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day7_on1          = 0xB490,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day7_off0     = 0xB491,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day7_off1     = 0xB492,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day8_on0          = 0xB493,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day8_on1          = 0xB494,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day8_off0     = 0xB495,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day8_off1     = 0xB496,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day9_on0          = 0xB497,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day9_on1          = 0xB498,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day9_off0     = 0xB499,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day9_off1     = 0xB49A,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day10_on0     = 0xB49B,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day10_on1     = 0xB49C,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day10_off0        = 0xB49D,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day10_off1        = 0xB49E,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day11_on0     = 0xB49F,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day11_on1     = 0xB4A0,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day11_off0        = 0xB4A1,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day11_off1        = 0xB4A2,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day12_on0     = 0xB4A3,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day12_on1     = 0xB4A4,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day12_off0        = 0xB4A5,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day12_off1        = 0xB4A6,

	LEGACY_AID_CHANGE_LIGHTS_sta2_day13_on0     = 0xB4A7,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day13_on1     = 0xB4A8,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day13_off0        = 0xB4A9,
	LEGACY_AID_CHANGE_LIGHTS_sta2_day13_off1        = 0xB4AA,

	LEGACY_AID_CHANGE_LIGHTS_sta3_text              = 0xB4AB,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day0_on0          = 0xB4AC,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day0_on1          = 0xB4AD,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day0_off0     = 0xB4AE,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day0_off1     = 0xB4AF,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day1_on0          = 0xB4B0,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day1_on1          = 0xB4B1,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day1_off0     = 0xB4B2,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day1_off1     = 0xB4B3,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day2_on0          = 0xB4B4,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day2_on1          = 0xB4B5,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day2_off0     = 0xB4B6,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day2_off1     = 0xB4B7,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day3_on0          = 0xB4B8,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day3_on1          = 0xB4B9,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day3_off0     = 0xB4BA,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day3_off1     = 0xB4BB,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day4_on0          = 0xB4BC,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day4_on1          = 0xB4BD,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day4_off0     = 0xB4BE,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day4_off1     = 0xB4BF,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day5_on0          = 0xB4C0,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day5_on1          = 0xB4C1,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day5_off0     = 0xB4C2,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day5_off1     = 0xB4C3,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day6_on0          = 0xB4C4,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day6_on1          = 0xB4C5,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day6_off0     = 0xB4C6,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day6_off1     = 0xB4C7,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day7_on0          = 0xB4C8,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day7_on1          = 0xB4C9,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day7_off0     = 0xB4CA,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day7_off1     = 0xB4CB,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day8_on0          = 0xB4CC,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day8_on1          = 0xB4CD,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day8_off0     = 0xB4CE,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day8_off1     = 0xB4CF,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day9_on0          = 0xB4D0,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day9_on1          = 0xB4D1,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day9_off0     = 0xB4D2,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day9_off1     = 0xB4D3,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day10_on0     = 0xB4D4,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day10_on1     = 0xB4D5,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day10_off0        = 0xB4D6,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day10_off1        = 0xB4D7,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day11_on0     = 0xB4D8,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day11_on1     = 0xB4D9,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day11_off0        = 0xB4DA,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day11_off1        = 0xB4DB,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day12_on0     = 0xB4DC,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day12_on1     = 0xB4DD,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day12_off0        = 0xB4DE,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day12_off1        = 0xB4DF,

	LEGACY_AID_CHANGE_LIGHTS_sta3_day13_on0     = 0xB4E0,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day13_on1     = 0xB4E1,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day13_off0        = 0xB4E2,
	LEGACY_AID_CHANGE_LIGHTS_sta3_day13_off1        = 0xB4E3,

	LEGACY_AID_CHANGE_LIGHTS_date                   = 0xB4E4,

	// ----------

	LEGACY_AID_CHANGE_ETTABLE_date                  = 0xB500,
	LEGACY_AID_CHANGE_ETTABLE__0                    = 0xB501,
	LEGACY_AID_CHANGE_ETTABLE__1                    = 0xB502,
	LEGACY_AID_CHANGE_ETTABLE__2                    = 0xB503,
	LEGACY_AID_CHANGE_ETTABLE__3                    = 0xB504,
	LEGACY_AID_CHANGE_ETTABLE__4                    = 0xB505,
	LEGACY_AID_CHANGE_ETTABLE__5                    = 0xB506,
	LEGACY_AID_CHANGE_ETTABLE__6                    = 0xB507,
	LEGACY_AID_CHANGE_ETTABLE__7                    = 0xB508,
	LEGACY_AID_CHANGE_ETTABLE__8                    = 0xB509,
	LEGACY_AID_CHANGE_ETTABLE__9                    = 0xB50A,
	LEGACY_AID_CHANGE_ETTABLE_10                    = 0xB50B,
	LEGACY_AID_CHANGE_ETTABLE_11                    = 0xB50C,
	LEGACY_AID_CHANGE_ETTABLE_12                    = 0xB50D,
	LEGACY_AID_CHANGE_ETTABLE_13                    = 0xB50E,
	LEGACY_AID_CHANGE_ETTABLE_14                    = 0xB50F,
	LEGACY_AID_CHANGE_ETTABLE_15                    = 0xB510,
	LEGACY_AID_CHANGE_ETTABLE_16                    = 0xB511,
	LEGACY_AID_CHANGE_ETTABLE_17                    = 0xB512,
	LEGACY_AID_CHANGE_ETTABLE_18                    = 0xB513,
	LEGACY_AID_CHANGE_ETTABLE_19                    = 0xB514,
	LEGACY_AID_CHANGE_ETTABLE_20                    = 0xB515,
	LEGACY_AID_CHANGE_ETTABLE_21                    = 0xB516,
	LEGACY_AID_CHANGE_ETTABLE_22                    = 0xB517,
	LEGACY_AID_CHANGE_ETTABLE_23                    = 0xB518,
	LEGACY_AID_CHANGE_ETTABLE_24                    = 0xB519,
	LEGACY_AID_CHANGE_ETTABLE_25                    = 0xB51A,
	LEGACY_AID_CHANGE_ETTABLE_26                    = 0xB51B,
	LEGACY_AID_CHANGE_ETTABLE_27                    = 0xB51C,

	LEGACY_AID_CHANGE_RAINTABLE_date                = 0xB600,
	LEGACY_AID_CHANGE_RAINTABLE__0                  = 0xB601,
	LEGACY_AID_CHANGE_RAINTABLE__1                  = 0xB602,
	LEGACY_AID_CHANGE_RAINTABLE__2                  = 0xB603,
	LEGACY_AID_CHANGE_RAINTABLE__3                  = 0xB604,
	LEGACY_AID_CHANGE_RAINTABLE__4                  = 0xB605,
	LEGACY_AID_CHANGE_RAINTABLE__5                  = 0xB606,
	LEGACY_AID_CHANGE_RAINTABLE__6                  = 0xB607,
	LEGACY_AID_CHANGE_RAINTABLE__7                  = 0xB608,
	LEGACY_AID_CHANGE_RAINTABLE__8                  = 0xB609,
	LEGACY_AID_CHANGE_RAINTABLE__9                  = 0xB60A,
	LEGACY_AID_CHANGE_RAINTABLE_10                  = 0xB60B,
	LEGACY_AID_CHANGE_RAINTABLE_11                  = 0xB60C,
	LEGACY_AID_CHANGE_RAINTABLE_12                  = 0xB60D,
	LEGACY_AID_CHANGE_RAINTABLE_13                  = 0xB60E,
	LEGACY_AID_CHANGE_RAINTABLE_14                  = 0xB60F,
	LEGACY_AID_CHANGE_RAINTABLE_15                  = 0xB610,
	LEGACY_AID_CHANGE_RAINTABLE_16                  = 0xB611,
	LEGACY_AID_CHANGE_RAINTABLE_17                  = 0xB612,
	LEGACY_AID_CHANGE_RAINTABLE_18                  = 0xB613,
	LEGACY_AID_CHANGE_RAINTABLE_19                  = 0xB614,
	LEGACY_AID_CHANGE_RAINTABLE_20                  = 0xB615,
	LEGACY_AID_CHANGE_RAINTABLE_21                  = 0xB616,
	LEGACY_AID_CHANGE_RAINTABLE_22                  = 0xB617,
	LEGACY_AID_CHANGE_RAINTABLE_23                  = 0xB618,
	LEGACY_AID_CHANGE_RAINTABLE_24                  = 0xB619,
	LEGACY_AID_CHANGE_RAINTABLE_25                  = 0xB61A,
	LEGACY_AID_CHANGE_RAINTABLE_26                  = 0xB61B,
	LEGACY_AID_CHANGE_RAINTABLE_27                  = 0xB61C,


	// ----------

	// NOTE: B700 through B7FF are reserved and in use by the original
	//       500 series code running on the 2000/2003 boards. They are
	//       used there for the SMSPROGs 1 & 2. In the ET2000e we have
	//       moved the SMSPROGs to B900 and BA00 respectively.

	// ----------

	LEGACY_AID_CHANGE_SMSSEQ_runfor             = 0xB800,
	LEGACY_AID_CHANGE_SMSSEQ_station_0              = 0xB801,

	// the 40 aid's for the stations are in here (0x28 = 40 decimal)
	LEGACY_AID_CHANGE_SMSSEQ_station_39         = (LEGACY_AID_CHANGE_SMSSEQ_station_0 + MAX_STATIONS__500_SERIES - 1),

	// the 48 aid's for the stations are in here
	LEGACY_AID_CHANGE_SMSSEQ_station_47         = (LEGACY_AID_CHANGE_SMSSEQ_station_0 + MAX_STATIONS__600_SERIES - 1),

	// ----------

	LEGACY_AID_CHANGE_SMSHO_inuse                   = 0xB880,
	LEGACY_AID_CHANGE_SMSHO_starttime               = 0xB881,
	LEGACY_AID_CHANGE_SMSHO_stoptime                = 0xB882,

	LEGACY_AID_CHANGE_SMSHO_sun                 = 0xB883,
	LEGACY_AID_CHANGE_SMSHO_mon                 = 0xB884,
	LEGACY_AID_CHANGE_SMSHO_tue                 = 0xB885,
	LEGACY_AID_CHANGE_SMSHO_wed                 = 0xB886,
	LEGACY_AID_CHANGE_SMSHO_thu                 = 0xB887,
	LEGACY_AID_CHANGE_SMSHO_fri                 = 0xB888,
	LEGACY_AID_CHANGE_SMSHO_sat                 = 0xB889,

	// ----------

	LEGACY_AID_CHANGE_SMSPROG1_name             = 0xB900,

	LEGACY_AID_CHANGE_SMSPROG1_sun                  = 0xB901,
	LEGACY_AID_CHANGE_SMSPROG1_mon                  = 0xB902,
	LEGACY_AID_CHANGE_SMSPROG1_tue                  = 0xB903,
	LEGACY_AID_CHANGE_SMSPROG1_wed                  = 0xB904,
	LEGACY_AID_CHANGE_SMSPROG1_thu                  = 0xB905,
	LEGACY_AID_CHANGE_SMSPROG1_fri                  = 0xB906,
	LEGACY_AID_CHANGE_SMSPROG1_sat                  = 0xB907,

	LEGACY_AID_CHANGE_SMSPROG1_sx1                  = 0xB908,
	LEGACY_AID_CHANGE_SMSPROG1_sx2                  = 0xB909,
	LEGACY_AID_CHANGE_SMSPROG1_sx3                  = 0xB90A,
	LEGACY_AID_CHANGE_SMSPROG1_sx4                  = 0xB90B,
	LEGACY_AID_CHANGE_SMSPROG1_sx5                  = 0xB90C,
	LEGACY_AID_CHANGE_SMSPROG1_sx6                  = 0xB90D,
	LEGACY_AID_CHANGE_SMSPROG1_sx7                  = 0xB90E,
	LEGACY_AID_CHANGE_SMSPROG1_sx8                  = 0xB90F,
	LEGACY_AID_CHANGE_SMSPROG1_sx9                  = 0xB910,

	LEGACY_AID_CHANGE_SMSPROG1_startdate            = 0xB911,
	LEGACY_AID_CHANGE_SMSPROG1_stopdate         = 0xB912,

	LEGACY_AID_CHANGE_SMSPROG1_useHO                = 0xB913,
	LEGACY_AID_CHANGE_SMSPROG1_suspend_programmed   = 0xB914,

	LEGACY_AID_CHANGE_SMSPROG1_seconds_0            = 0xB915,

	// In here would go the MAX_STATIONS AID's one for each station - but we don't need to
	// explicitly show each one cause we index off of seconds_0.
	// Show the last one for the range when parsing alerts however.
	LEGACY_AID_CHANGE_SMSPROG1_seconds_39           = (LEGACY_AID_CHANGE_SMSPROG1_seconds_0 + MAX_STATIONS__500_SERIES - 1),

	// In here would go the MAX_STATIONS AID's one for each station - but we don't need to
	// explicitly show each one cause we index off of seconds_0.
	// Show the last one for the range when parsing alerts however.
	LEGACY_AID_CHANGE_SMSPROG1_seconds_47           = (LEGACY_AID_CHANGE_SMSPROG1_seconds_0 + MAX_STATIONS__600_SERIES - 1),

	// ----------

	// the PARSING depends on PROG_2 to be sequentially following PROG_1 with no
	// interruptions in the numbering of the AID's.

	// ----------

	LEGACY_AID_CHANGE_SMSPROG2_name             = 0xBA00,

	LEGACY_AID_CHANGE_SMSPROG2_sun                  = 0xBA01,
	LEGACY_AID_CHANGE_SMSPROG2_mon                  = 0xBA02,
	LEGACY_AID_CHANGE_SMSPROG2_tue                  = 0xBA03,
	LEGACY_AID_CHANGE_SMSPROG2_wed                  = 0xBA04,
	LEGACY_AID_CHANGE_SMSPROG2_thu                  = 0xBA05,
	LEGACY_AID_CHANGE_SMSPROG2_fri                  = 0xBA06,
	LEGACY_AID_CHANGE_SMSPROG2_sat                  = 0xBA07,

	LEGACY_AID_CHANGE_SMSPROG2_sx1                  = 0xBA08,
	LEGACY_AID_CHANGE_SMSPROG2_sx2                  = 0xBA09,
	LEGACY_AID_CHANGE_SMSPROG2_sx3                  = 0xBA0A,
	LEGACY_AID_CHANGE_SMSPROG2_sx4                  = 0xBA0B,
	LEGACY_AID_CHANGE_SMSPROG2_sx5                  = 0xBA0C,
	LEGACY_AID_CHANGE_SMSPROG2_sx6                  = 0xBA0D,
	LEGACY_AID_CHANGE_SMSPROG2_sx7                  = 0xBA0E,
	LEGACY_AID_CHANGE_SMSPROG2_sx8                  = 0xBA0F,
	LEGACY_AID_CHANGE_SMSPROG2_sx9                  = 0xBA10,

	LEGACY_AID_CHANGE_SMSPROG2_startdate            = 0xBA11,
	LEGACY_AID_CHANGE_SMSPROG2_stopdate         = 0xBA12,

	LEGACY_AID_CHANGE_SMSPROG2_useHO                = 0xBA13,
	LEGACY_AID_CHANGE_SMSPROG2_suspend_programmed   = 0xBA14,

	LEGACY_AID_CHANGE_SMSPROG2_seconds_0            = 0xBA15,

	// In here would go the 40 AID's one for each station - but we don't need to
	// explicitly show each one cause we index off of seconds_0.
	// Show the last one for the range when parsing alerts however.
	LEGACY_AID_CHANGE_SMSPROG2_seconds_39           = (LEGACY_AID_CHANGE_SMSPROG2_seconds_0 + MAX_STATIONS__500_SERIES - 1),

	// In here would go the 40 AID's one for each station - but we don't need to
	// explicitly show each one cause we index off of seconds_0.
	// Show the last one for the range when parsing alerts however.
	LEGACY_AID_CHANGE_SMSPROG2_seconds_47           = (LEGACY_AID_CHANGE_SMSPROG2_seconds_0 + MAX_STATIONS__600_SERIES - 1),

	// ----------

	// Specially located variables stored in the EE. Nothing special about starting at 0xBF00.
	// If we bang into 0xC000 just start a new group a 0xBE00. That will be ok.
	LEGACY_AID_CHANGE_CHECKSUM = 0xBF00,

	LEGACY_AID_CHANGE_ROMDATE = 0xBF01,

	LEGACY_AID_CHANGE_ACCESSPROFILE = 0xBF02,

	LEGACY_AID_CHANGE_LOGINCODES = 0xBF03,

	LEGACY_AID_CHANGE_HISTORY24 = 0xBF04,

	LEGACY_AID_CHANGE_HISTORY24_DATE = 0xBF05,

	LEGACY_AID_CHANGE_DERATE_TABLE = 0xBF06,

	// now 40 lines so we can see which station we are updating for
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__0 = 0xBF10,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__1 = 0xBF11,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__2 = 0xBF12,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__3 = 0xBF13,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__4 = 0xBF14,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__5 = 0xBF15,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__6 = 0xBF16,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__7 = 0xBF17,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__8 = 0xBF18,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT__9 = 0xBF19,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_10 = 0xBF1A,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_11 = 0xBF1B,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_12 = 0xBF1C,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_13 = 0xBF1D,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_14 = 0xBF1E,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_15 = 0xBF1F,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_16 = 0xBF20,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_17 = 0xBF21,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_18 = 0xBF22,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_19 = 0xBF23,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_20 = 0xBF24,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_21 = 0xBF25,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_22 = 0xBF26,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_23 = 0xBF27,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_24 = 0xBF28,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_25 = 0xBF29,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_26 = 0xBF2A,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_27 = 0xBF2B,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_28 = 0xBF2C,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_29 = 0xBF2D,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_30 = 0xBF2E,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_31 = 0xBF2F,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_32 = 0xBF30,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_33 = 0xBF31,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_34 = 0xBF32,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_35 = 0xBF33,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_36 = 0xBF34,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_37 = 0xBF35,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_38 = 0xBF36,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_39 = 0xBF37,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_40 = 0xBF38,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_41 = 0xBF39,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_42 = 0xBF3A,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_43 = 0xBF3B,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_44 = 0xBF3C,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_45 = 0xBF3D,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_46 = 0xBF3E,
	LEGACY_AID_CHANGE_IRRI_FLOW_CHECK_COUNT_47 = 0xBF3F,

	// one for the whole table
	LEGACY_AID_CHANGE_DERATE_UPDATE_COUNT = 0xBF40,

	// The base change line for indexed variables from which all PData change lines are created.
	// What we will actually do is add the EE index to this base to form the AID. There will be ranges
	// of alert ID's for certain variables which represent station-by-station information or
	// program-by-program information.
	LEGACY_AID_CHANGE_LINE_BASE = 0xC000,

	// AID's from 0xC000 through 0xFFFF (16384 of 'em) are taken by the change lines. DO NOT USE THEM.

	// ----------

	ALERT_ID_SIZE_FORCER = 0xFFFF	// forces the compiler to make vars of this type 2 bytes

} ALERT_ID;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

char A_NoYes[ 2 ][ 4 ];
char S_A_NoYes[ 2 ][ 4 ];

// ----------

char A_OffOn[ 2 ][ 4 ];
char S_A_OffOn[ 2 ][ 9 ];

// ----------

char A_PlusMinus[ 2 ][ 8 ];

// ----------

char lights_time_str16[ 4 ][ 16 ];
char s_lights_time_str16[ 4 ][ 28 ];

// ----------

char inuse_str[ 2 ][ 12 ];
char s_inuse_str[ 2 ][ 12 ];

// ----------

char referenceET_str[ 2 ][ 24 ];
char s_referenceET_str[ 2 ][ 24 ];

// ----------

char budgetoption_str[ 3 ][ 24 ];
char s_budgetoption_str[ 3 ][ 24 ];

// ----------

char prioritylevels_str[ 3 ][ 7 ];
char s_prioritylevels_str[ 3 ][ 7 ];

// ----------

const char E_ActionStrs[ 3 ][ 16 ];
const char S_ActionStrs[ 3 ][ 16 ];

// ----------

const char E_CapDayStringsShort[ 7 ][ 4 ];
const char S_CapDayStringsShort[ 7 ][ 4 ];

// ----------

const char FlowMeterStrings[ 13 ][ 9 ];

// ----------

char MVType_Str_20[ 4 ][ 24 ];

// ----------

const char  E_MonthStringsShort[ 13 ][ 4 ];
const char  S_MonthStringsShort[ 13 ][ 4 ];

// ----------

const char  E_MonthStringsShortCAPS[ 13 ][ 4 ];
const char  S_MonthStringsShortCAPS[ 13 ][ 4 ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern char *valveson_str( char *pstr, const UNS_32 pstr_size, const unsigned short pvalveson );

extern char *Change_MonthString_16( char *pstr, const UNS_32 pstr_size, const unsigned short preason, const unsigned short index0_11 );

extern char *Change_ReasonString_32( char *pstr, const UNS_32 pstr_size, unsigned short preason );

extern char *ShaveRightPad_63max( char *pbuf, const UNS_32 pbuf_size, char *pstr );

// ----------

DLLEXPORT UNS_32 LEGACY_ALERTS_parse_alert_and_return_length( const UNS_32 paid,
															  ALERTS_PILE_STRUCT *const ppile,
															  const UNS_32 pparse_why,
															  char *pdest_ptr,
															  const UNS_32 pallowable_size,
															  UNS_32 pindex,
															  const UNS_8 pis_a_600_series );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _INC_LEGACY_ALERTS

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

