/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_LEGACY_REPORT_DATA
#define _INC_LEGACY_REPORT_DATA

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs3000_comm_server_common.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

DLLEXPORT UNS_32 LEGACY_REPORTS_extract_and_store_data(	const UNS_32 pMID,
														UNS_8 *pucp,
														const UNS_32 pdata_len,
														const UNS_32 pcontroller_id,
														const UNS_8 pis_a_600_series,
														const BOOL_32 phas_a_dash_f_interface,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf
												   );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _INC_LEGACY_REPORT_DATA

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

