/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"legacy_lights.h"

#include	"legacy_common.h"

// Required for SQL functions
#include	"sql_merge.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		LEGACY_LIGHTS_DAYS_SCHEDULE		(14)

#define		LEGACY_LIGHTS_MAX_OUTPUTS		(4)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	char	name[ 40 ];

	UNS_32	times[ LEGACY_LIGHTS_DAYS_SCHEDULE ][ 4 ];

	UNS_16	date;

} LightsFields;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void store_lights(	const LightsFields pdata[],
							const UNS_32 pcontroller_id,
							char *pSQL_statements,
							char *pSQL_search_condition_str,
							char *pSQL_update_str,
							char *pSQL_insert_fields_str,
							char *pSQL_insert_values_str,
							char *pSQL_single_merge_statement_buf )
{
	char	str_20[ 20 ];

	UNS_32	i, d;

	// ----------

	for( i = 0; i < LEGACY_LIGHTS_MAX_OUTPUTS; ++i )
	{
		memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
		memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );

		// ----------

		SQL_MERGE_build_search_condition_uint32( "ControllerID", pcontroller_id, pSQL_search_condition_str );
		SQL_MERGE_build_search_condition_sql_cursor_var("LightTimestamp", "@DT", pSQL_search_condition_str);
		SQL_MERGE_build_search_condition_uint32( "OutputIndex", (i + 1), pSQL_search_condition_str );

		// ----------

		SQL_MERGE_build_upsert_uint32( pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_sql_cursor_var( "@DT", SQL_MERGE_include_field_name_in_insert_clause, "LightTimestamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		SQL_MERGE_build_upsert_uint32( (i + 1), SQL_MERGE_include_field_name_in_insert_clause, "OutputIndex", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		SQL_MERGE_build_upsert_string( pdata[ i ].name, sizeof(pdata[ i ].name), SQL_MERGE_include_field_name_in_insert_clause, "Name", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		for( d = 0; d < LEGACY_LIGHTS_DAYS_SCHEDULE; ++d )
		{
			snprintf( str_20, sizeof(str_20), "Day%02dStartTime1", d );
			SQL_MERGE_build_upsert_uint32( pdata[ i ].times[ d ][ 0 ], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			snprintf( str_20, sizeof(str_20), "Day%02dStartTime2", d );
			SQL_MERGE_build_upsert_uint32( pdata[ i ].times[ d ][ 1 ], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			snprintf( str_20, sizeof(str_20), "Day%02dStopTime1", d );
			SQL_MERGE_build_upsert_uint32( pdata[ i ].times[ d ][ 2 ], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

			snprintf( str_20, sizeof(str_20), "Day%02dStopTime2", d );
			SQL_MERGE_build_upsert_uint32( pdata[ i ].times[ d ][ 3 ], SQL_MERGE_include_field_name_in_insert_clause, str_20, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );
		}

		SQL_MERGE_build_upsert_date( pdata[ i ].date, SQL_MERGE_include_field_name_in_insert_clause, "LightDate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str );

		// ----------

		SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements("Lights", pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);

	}
}

/* ---------------------------------------------------------- */
DLLEXPORT UNS_32 LEGACY_LIGHTS_extract_and_store_data(	UNS_8 *pucp,
														const UNS_32 pcontroller_id,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf )
{
	LightsFields	L[ LEGACY_LIGHTS_MAX_OUTPUTS ];

	UNS_32	i, d, t;

	UNS_32  rv;

	// ----------

	rv = 0;

	// ----------

	// 1/25/2016 ajv : Make sure the strings are fully initialized to NULL since we rely on this
	// to determine whether the string is empty or not.
	memset( pSQL_statements, 0x00, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	// ----------

	for( i = 0; i < LEGACY_LIGHTS_MAX_OUTPUTS; ++i )
	{
		
		LEGACY_COMMON_extract_string(&pucp, L[i].name, 40);

		rv += 40;

		for( d = 0; d < LEGACY_LIGHTS_DAYS_SCHEDULE; ++d )
		{
			for( t = 0; t < 4; ++t )
			{
				LEGACY_COMMON_extract_uint32( &pucp, &L[ i ].times[ d ][ t ] );

				rv += sizeof ( UNS_32 );
			}
		}
	}

	// 2/3/2016 ajv : Although we have four independent light outputs, we only have one date.
	// Therefore, grab it but assign it to all four lights.
	LEGACY_COMMON_extract_uint16( &pucp, &L[ 0 ].date );

	rv += sizeof(UNS_16);

	for( i = 1; i < LEGACY_LIGHTS_MAX_OUTPUTS; ++i )
	{
		L[ i ].date = L[ 0 ].date;

		rv += sizeof ( UNS_16 );
	}

	// ----------

	// add timestamp variable definition "@DT" and set it to the current date & time
	snprintf(pSQL_statements, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS, "DECLARE @DT TImestamp;SET @DT = CURRENT_TIMESTAMP();");
	
	store_lights( L, pcontroller_id, pSQL_statements, pSQL_search_condition_str, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_single_merge_statement_buf );

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

